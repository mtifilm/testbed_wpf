//---------------------------------------------------------------------------

#ifndef ColorLabelH
#define ColorLabelH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TColorLabel : public TLabel
{
private:
protected:
	virtual void __fastcall Paint(void);
public:
	__fastcall TColorLabel(TComponent* Owner);
__published:

};
//---------------------------------------------------------------------------
#endif
