//---------------------------------------------------------------------------

#ifndef MtiComponentTestH
#define MtiComponentTestH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "Knob.h"
#include "VDoubleEdit.h"
#include "VTimeCodeEdit.h"
#include "MTIBusy.h"
#include "GrayComboBox.h"
#include "HistoryMenu.h"
#include <Vcl.Menus.hpp>
#include "MTIDBMeter.h"
#include <Vcl.ExtCtrls.hpp>
#include "MTIProgressBar.h"
#include "TriTimecode.h"
#include "VTimeCodeHistory.h"
#include "TimelineTrackBar.h"
#include "PopupComboBox.h"
#include "IconListBox.h"
#include <Vcl.ImgList.hpp>
#include <Vcl.Grids.hpp>
#include "ColorPanel.h"
#include <Vcl.Buttons.hpp>
#include "ColorLabel.h"
#include <System.ImageList.hpp>
#include "ColorSplitter.h"
#include "cspin.h"
//---------------------------------------------------------------------------
class TForm3 : public TForm
{
__published:	// IDE-managed Components
	TKnob *Knob1;
	VDoubleEdit *VDoubleEdit1;
	VTimeCodeEdit *VTimeCodeEdit1;
	TMTIBusy *MTIBusy1;
	TButton *StartBusyButton;
	TGrayComboBox *GrayComboBox1;
	TMTIDBMeter *MTIDBMeter1;
	TTimer *Timer1;
	TMTIProgressBar *MTIProgressBar1;
	TTriTimecode *TriTimecode1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *Label8;
	VTimeCodeHistory *VTimeCodeHistory1;
	TLabel *Label9;
	TLabel *Label10;
	TPopupComboBox *PopupComboBox1;
	TLabel *Label11;
	TIconListBox *IconListBox1;
	TImageList *ImageList1;
	TPopupMenu *PopupMenu1;
	TMenuItem *Copy2;
	TMenuItem *Cut3;
	TMenuItem *Paste1;
	TLabel *Label12;
	TTimelineTrackBar *TimelineTrackBar1;
	TColorPanel *ColorPanel1;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TComboBox *TextColorComboBox;
	TColorLabel *VideoStorageLabel;
	TColorSplitter *ColorSplitter1;
   TButton *Button1;
	void __fastcall Knob1Change(TObject *Sender);
	void __fastcall VDoubleEdit1DoubleChange(TObject *Sender);
	void __fastcall StartBusyButtonClick(TObject *Sender);
	void __fastcall Exit1Click(TObject *Sender);
	void __fastcall GrayComboBox1Change(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall TextColorComboBoxChange(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm3(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif
