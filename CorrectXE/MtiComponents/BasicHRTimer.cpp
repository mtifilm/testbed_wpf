//---------------------------------------------------------------------------

#pragma hdrstop

#include "BasicHRTimer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

/*
    File Name:  HRTimer.cpp
    Create:  March 2, 2000
    Release 1.00

    This provides a High Resolution Timer for windows.  The accuracy
    depends upon the machine but for windows is better than 100
    microseconds and for sgi and linux on intel the same.

                                        -John Mertus@Brown.EDU
*/

//---------------------------------------------------------------------------
#include "BasicHRTimer.h"
#include <sstream>
#include <time.h>

#ifndef _MSC_VER
#include <algorithm>
using std::min;
using std::max;
#endif

#include <windows.h>
#include <dos.h>

//---------------------------------------------------------------------------


 /*-----------------------Constructor----------------JAM--Mar 2000----*/

  CHRTimer::CHRTimer()

 /* This just finds if the hardware supports the timer and finds the  */
 /* clock freq                                                        */
 /*                                                                   */
 /*********************************************************************/
{
   LARGE_INTEGER l;
   QueryPerformanceFrequency(&l);
   m_dClockFreq = l.QuadPart;

   wTimerRes = -1;   // initialize millisecond resolution
   hWaitableTimer = NULL;  // handle to waitable timer

   hWaitableTimer = CreateWaitableTimer (NULL, TRUE, "WaitableHRTimer");
   if (hWaitableTimer != NULL)
    {
     // get the capabilities of this computer
     TIMECAPS tc;
     if (timeGetDevCaps (&tc, sizeof(TIMECAPS)) == TIMERR_NOERROR)
      {
       #define TARGET_RESOLUTION 1   // attempt to use a 1 millisec timer
       UINT wTmp;
       wTmp = min (max (tc.wPeriodMin, (UINT)TARGET_RESOLUTION), tc.wPeriodMax);

       // set the resolution of the timer
       if (timeBeginPeriod (wTmp) == TIMERR_NOERROR)
        {
         // successfully set the multi media timer
         wTimerRes = wTmp;
        }
      }
	}

   Start();
}

CHRTimer::~CHRTimer ()
{
  // destroy the Waitable timer
  CloseHandle (hWaitableTimer);
  hWaitableTimer = NULL;
}

 /*-----------------------Start-----------------------JAM--Mar 2000---*/

     void CHRTimer::Start()

 /* Call this to zero the timer.  This just gets the count value      */
 /* and stores it for later.                                          */
 /*                                                                   */
 /*********************************************************************/
{
   LARGE_INTEGER l;
   QueryPerformanceCounter(&l);
   m_dStartCount = l.QuadPart;
}

/*-------------------------Read--------------------JAM--Mar 2000----*/

   double CHRTimer::Read(bool reset)

/* This returns the current count in milliseconds.  StartTimer must  */
/* be called first                                                   */
/*                                                                   */
/*********************************************************************/
{
  LARGE_INTEGER llCurrentCount;
  QueryPerformanceCounter(&llCurrentCount);
  double dCurrentCount = double(llCurrentCount.QuadPart) - m_dStartCount;
  if (reset)
     m_dStartCount = llCurrentCount.QuadPart;

  return(1000.0 * dCurrentCount / m_dClockFreq);
}



/*-----------------------ReadAsString----------------JAM--Mar 2000----*/

  string CHRTimer::ReadAsString()

/* This returns the current count in milliseconds.  StartTimer must  */
/* be called first                                                   */
/*                                                                   */
/*********************************************************************/
{
  std::ostringstream os;
  os << Read();
  return(os.str());
}

/*-----------------------Delay----------------JAM--Mar 2000----*/

  void CHRTimer::Delay(float fDelay)

//  Delays at least fDelay milliseconds.
//  Delay can be fractional, up to the accuracy of the time
//
//********************************************************************
{
  double st = Read();
  while ((Read() - st) < fDelay);
}

/*-----------------------Sleep----------------JAM--Mar 2000----*/

  void CHRTimer::Sleep(int iDelay)

//
//  This function uses the Multi Media timer to get close to the
//  desired delay.
//
//********************************************************************
{
  int iRet = -1;

  if (hWaitableTimer)
   {
    // the MultiMedia timer has resolution of wTimeRes.  Set the timer
    // to expire in iDelay milliseconds
    if ((int)wTimerRes != -1)
     {
      LARGE_INTEGER liDueTime;
      liDueTime.QuadPart = -iDelay * 10000;   // convert to 100 nanosec.
		// negative means relative wait.

      if (SetWaitableTimer (hWaitableTimer, &liDueTime, 0, NULL, NULL, 0))
       {
        // success, wait for event
        if (WaitForSingleObject (hWaitableTimer, INFINITE) == WAIT_OBJECT_0)
         {
          // success
          iRet = 0;
         }
       }
     }
   }

  if (iRet)
   {
    // something went wrong, no choice but to use SleepMS()
    SleepMS (iDelay);
   }
}

/*-----------------------DelayNS--------------JAM--Mar 2000----*/

  void CHRTimer::DelayNS(float fDelay)

//  Delays at least fDelay milliseconds.  -- NON-SPINNING version
//  Delay can be fractional, up to the accuracy of the time
//
//  This function uses the Multi Media timer to get close to the
//  desired delay.  Then is spins while waiting for the final few
//  micro seconds.
//
//********************************************************************
{
  double st = Read();

  if (hWaitableTimer)
   {
    // the MultiMedia timer has resolution of wTimeRes.  Set the timer
    // to expire in (fDelay - wTimerRes) milliseconds
    int iNonSpinningMS = (int)fDelay - wTimerRes;
    if ((int)wTimerRes != -1 && iNonSpinningMS > 0)
     {
      LARGE_INTEGER liDueTime;
      liDueTime.QuadPart = -iNonSpinningMS * 10000;   // convert to 100 nanosec.
		// negative means relative wait.

      if (SetWaitableTimer (hWaitableTimer, &liDueTime, 0, NULL, NULL, 0))
       {
        // success, wait for event
        WaitForSingleObject (hWaitableTimer, INFINITE);
       }
     }
   }

  // spin the CPU waiting for remaining time
  while ((Read() - st) < fDelay);
}

//---------------------SleepMS--------------------John Mertus----Feb 2002---*/

  void SleepMS(int ms)

//   This rouitine sleeps for ms Millisecond.  Unlike delay, which does
//  not release the cpu, this does.
//
//****************************************************************************
{
  Sleep(ms);
}

