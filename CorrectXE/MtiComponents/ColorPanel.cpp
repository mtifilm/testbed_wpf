//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "ColorPanel.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TColorPanel *)
{
	new TColorPanel(NULL);
}
//---------------------------------------------------------------------------
__fastcall TColorPanel::TColorPanel(TComponent* Owner)
	: TPanel(Owner), fOnPaint(NULL)
{
}

//---------------------Paint----------------------John Mertus----Sep 2013-----

  void __fastcall TColorPanel::Paint(void)

//   This is called whenever Windows repaints.
//  If _InvalidateLeds is set, Rebuild all the LEDS
// Otherwise just create the main bitmap from the Text, Boarder and LEDS
// and Draw Mainbitmap to control
//
//****************************************************************************
{
   // TODO: Obey ParentColor
   Canvas->Brush->Color = this->Color;

   if (this->ParentBackground)
   {
      Canvas->Brush->Style = bsClear;
   }
   else
   {
	  Canvas->Brush->Style = bsSolid;
   }

  //// Canvas->FillRect(this->ClientRect);

   TRect Rect = ClientRect;
   WORD SavedAlign;

   Canvas->Font = this->Font;

   TSize textSize = Canvas->TextExtent(this->Caption);
   int verticalCenter = (Rect.Top + Margins->Top + Rect.Bottom - Margins->Bottom)/2
		 - textSize.Height / 2;

	switch (this->Alignment)
	{
	case taCenter:
		SavedAlign = SetTextAlign(Canvas->Handle, TA_CENTER);
		Canvas->TextRect(Rect, (Rect.Right + Rect.Left) / 2, verticalCenter ,
			Caption);
		SetTextAlign(Canvas->Handle, SavedAlign);
		break;

	case taLeftJustify:
		SavedAlign = SetTextAlign(Canvas->Handle, TA_LEFT);
		Canvas->TextRect(Rect, Margins->Left, verticalCenter,
			Caption);
		SetTextAlign(Canvas->Handle, SavedAlign);
		break;

	case taRightJustify:
		SavedAlign = SetTextAlign(Canvas->Handle, TA_RIGHT);
		Canvas->TextRect(Rect, Rect.Right - Margins->Right, verticalCenter,
			Caption);
		SetTextAlign(Canvas->Handle, SavedAlign);
		break;
	}

	if (fOnPaint != NULL)
	{
	   fOnPaint(this);
	}
}

//---------------------------------------------------------------------------
namespace Colorpanel
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TColorPanel)};
		RegisterComponents(L"MTI", classes, 0);
	}
}
//---------------------------------------------------------------------------
