//---------------------------------------------------------------------------

#ifndef MTIProgressBarH
#define MTIProgressBarH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
#ifndef _TProgressDirection_
#define  _TProgressDirection_
enum TProgressDirection { pdLeftToRight, pdRightToLeft, pdBottomToTop, pdTopToBottom };
#endif

enum TBarColorStyle { cs1Color, cs2Colors, cs3Colors };

class PACKAGE TMTIProgressBar : public TGraphicControl
{
private:
	Graphics::TBitmap* MainBitmap;
	Graphics::TColor fBorderColor1;
	Graphics::TColor fBorderColor2;
	Graphics::TColor fBackgroundColor;
	int fPosition;
	Graphics::TBitmap* fBarBitmap;
	int fMin;
	int fMax;
	bool fShowBorder;
	TProgressDirection fDirection;
	bool fShowPosText;
	AnsiString fPosTextSuffix;
	AnsiString fPosTextPrefix;
	TBarColorStyle fBarColorStyle;
	Graphics::TColor fBarColor1;
	Graphics::TColor fBarColor2;
	Graphics::TColor fBarColor3;
	double fBarValue1;
	double fBarValue2;
	bool fRegenerateBitmap;
	Graphics::TBitmap* TiledBarBitmap;
	void __fastcall PaintBorder(void);
	void __fastcall PaintPosText(void);
	void __fastcall SetBorderColor1(const Graphics::TColor Value);
	void __fastcall SetBorderColor2(const Graphics::TColor Value);
	void __fastcall SetBarBitmap(const Graphics::TBitmap* Value);
	void __fastcall SetBackgroundColor(const Graphics::TColor Value);
	void __fastcall SetPosition(const int Value);
	void __fastcall SetMax(const int Value);
	void __fastcall SetMin(const int Value);
	void __fastcall SetShowBorder(const bool Value);
	void __fastcall PaintBar(bool RegenerateBitmap);
	void __fastcall TileBitmap(Graphics::TBitmap* TiledBitmap, Graphics::TBitmap* &DestBitmap);
	void __fastcall SetDirection(const TProgressDirection Value);
	void __fastcall SetBarColor1(const Graphics::TColor Value);
	void __fastcall SetBarColor2(const Graphics::TColor Value);
	void __fastcall SetBarColor3(const Graphics::TColor Value);
	void __fastcall SetBarValue1(const double Value);
	void __fastcall SetBarValue2(const double Value);
	void __fastcall SetShowPosText(const bool Value);
	void __fastcall SetPosTextSuffix(const AnsiString Value);
	HIDESBASE MESSAGE void __fastcall CMFontChanged(Messages::TMessage &Message);
	void __fastcall SetPosTextPrefix(const AnsiString Value);
	Graphics::TColor __fastcall CalcColorIndex(Graphics::TColor StartColor, Graphics::TColor EndColor,
		int Steps, int ColorIndex);
	void __fastcall SetBarColorStyle(const TBarColorStyle Value);
	void __fastcall DrawColorBlending(void);

protected:
	virtual void __fastcall Paint(void);

public:
	__fastcall TMTIProgressBar(TComponent* AOwner);
	__fastcall virtual ~TMTIProgressBar(void);

__published:
	__property Graphics::TColor BackgroundColor = {read=fBackgroundColor, write=SetBackgroundColor, default=0
		};
	__property Graphics::TBitmap* BarBitmap = {read=fBarBitmap, write=SetBarBitmap};
	__property Graphics::TColor BarColor1 = {read=fBarColor1, write=SetBarColor1, default=32768};
	__property Graphics::TColor BarColor2 = {read=fBarColor2, write=SetBarColor2, default=65535};
	__property Graphics::TColor BarColor3 = {read=fBarColor3, write=SetBarColor3, default=255};
	__property double BarValue1 = {read=fBarValue1, write=SetBarValue1, default=50};
	__property double BarValue2 = {read=fBarValue2, write=SetBarValue2, default=80};
	__property TBarColorStyle BarColorStyle = {read=fBarColorStyle, write=SetBarColorStyle, default=2};

	__property Graphics::TColor BorderColor1 = {read=fBorderColor1, write=SetBorderColor1, default=-2147483632
		};
	__property Graphics::TColor BorderColor2 = {read=fBorderColor2, write=SetBorderColor2, default=-2147483628
		};
	__property TProgressDirection Direction = {read=fDirection, write=SetDirection, default=0};
	__property int Max = {read=fMax, write=SetMax, default=100};
	__property int Min = {read=fMin, write=SetMin, default=0};
	__property int Position = {read=fPosition, write=SetPosition, default=0};
	__property AnsiString PosTextPrefix = {read=fPosTextPrefix, write=SetPosTextPrefix};
	__property AnsiString PosTextSuffix = {read=fPosTextSuffix, write=SetPosTextSuffix};
	__property bool ShowBorder = {read=fShowBorder, write=SetShowBorder, default=1};
	__property bool ShowPosText = {read=fShowPosText, write=SetShowPosText, default=1};
	__property Align ;
	__property Anchors ;
	__property Font ;
	__property ParentShowHint ;
	__property ShowHint ;
	__property Visible ;
	__property OnClick ;
	__property OnDblClick ;
	__property OnMouseDown ;
	__property OnMouseMove ;
	__property OnMouseUp ;
};
//---------------------------------------------------------------------------
#endif


