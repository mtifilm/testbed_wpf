//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "MTIBusy.h"
#pragma resource "*.res"
#pragma package(smart_init)
//#define  DLLFileName  "C:\\Program Files\\Borland\\CBuilder6\\Projects\\Bpl\\dclusr60.bpl"
#define  DLLFileName  "MTIPackage.bpl"
#pragma resource "BusyArrows.res"
#include "IniFile.h"

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TMTIBusy *)
{
        new TMTIBusy(NULL);
}
//---------------------------------------------------------------------------
//--------------------TMTIBusy--------------------John Mertus----Sep 2003-----

  __fastcall TMTIBusy::TMTIBusy(TComponent* Owner)

//   Constructor, set the width and height and constraints
//   Create the animation and load it
//
//****************************************************************************
        : TCustomControl(Owner)
{
   Height = 32;
   Width = 32;
   Constraints->MaxHeight = Height;
   Constraints->MinHeight = Height;
   Constraints->MaxWidth = Width;
   Constraints->MinWidth = Width;
#ifdef _WIN64
   Animate = new TAnimate(this);
   Animate->Parent = this;
   Animate->Left = 0;
   Animate->Top = 0;
   Animate->Transparent = true;
#endif
   _ArrowType = batLarge;
   Load();

}
   //---------------------------------------------------------------------------
namespace Mtibusy
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TMTIBusy)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------

//----------------------Load----------------------John Mertus----Sep 2003-----

  void TMTIBusy::Load(void)

//  THis load the animation from the resource BusyArrowsAVI
//
//****************************************************************************
{
#ifndef _WIN64
    return;
#endif

    HINSTANCE hDll;
    char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
    String sResource;

//    hDll = LoadLibrary("MTIPackage.bpl");

//
//  The following should work but does not seem to hence the LoadLibrary
//    hDll = (void *)::FindClassHInstance(__classid(TMTIBusy));
    if (ArrowType == batSmall)
     {
	   sResource = "SmallBusyArrows";
       Height = 16;
       Width = 16;
     }
    else
     {
        sResource = "BusyArrows";
        Height = 32;
		Width = 32;
     }

    // Try to load it from the normal file
    //
	try
      {
#ifdef _WIN64
		String s = ExtractFilePath(Application->ExeName) + sResource + ".avi";
		Animate->FileName = s;
		if (Animate->Open)
          {
             FreeLibrary(hDll);
             Constraints->MaxHeight = Height;
             Constraints->MinHeight = Height;
             Constraints->MaxWidth = Width;
             Constraints->MinWidth = Width;
             return;
          }
#endif
      }
    catch (...)
      {
      }

//    // Try to load from resource
//    try
//     {
//       // Get resource
//       TResourceStream *ts = new TResourceStream((int)hDll, sResource, L"AVI");
//
//       // Create temp file and save to it
//       GetTempPath(MAX_PATH, DirBuf);
//       GetTempFileName(DirBuf, "tmp", 0, FileBuf);
//       ts->SaveToFile(FileBuf);
//       Animate->FileName = FileBuf;
//       DeleteFile(FileBuf);
//       delete ts;
//     }
//    catch (...)
//     {
//     }

//   FreeLibrary(hDll);

// Should be able to do this but doesn't work
//   Height = Animate->Height;
//   Width = Animate->Width;
   Constraints->MaxHeight = Height;
   Constraints->MinHeight = Height;
   Constraints->MaxWidth = Width;
   Constraints->MinWidth = Width;

}

//-------------------SetBusy----------------------John Mertus----Sep 2003-----

      void __fastcall TMTIBusy::SetBusy(const bool Value)

//  Usual setter of busy (active) parameter
//
//****************************************************************************
{
#ifdef _WIN64
    Animate->Active = Value;
#endif
}

//-------------------SetArrowType-----------------John Mertus----Sep 2003-----

      void __fastcall TMTIBusy::SetArrowType(const BusyArrowType Value)

//  Usual setter of busy (active) parameter
//
//****************************************************************************
{
   if (Value == _ArrowType) return;
   _ArrowType = Value;
   Load();
}

//-------------------GetBusy----------------------John Mertus----Sep 2003-----

      bool __fastcall TMTIBusy::GetBusy(void)

//  Usual getter of busy (active) parameter
//
//****************************************************************************
{
   return false; //Animate->Active;
}

//---------------------Paint---------------------John Mertus----Sep 2003-----

  	void __fastcall TMTIBusy::Paint(void)

//  A paint stops animination, so restart it if it was
// started.
//
//****************************************************************************
{
  if (Busy)
    {
       Busy = false;
       Busy = true;
    }
}

