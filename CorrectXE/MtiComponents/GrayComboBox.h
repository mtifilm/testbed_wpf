//---------------------------------------------------------------------------

#ifndef GrayComboBoxH
#define GrayComboBoxH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TGrayComboBox : public TComboBox
{
private:
   TDrawItemEvent qOnDrawItem;   // New ondraw event
   TNotifyEvent   qOnChange;     // What happens on change

   int qOldItemIndex;            // Where to revert if changed to gray

   // Local when an item is drawn
   void  __fastcall BoxDrawItemEvent(Vcl::Controls::TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State);

   // Local when an item changes
   void __fastcall OnComboBoxChangeCB(TObject *Sender);
   bool __fastcall GetItemEnabled(int Index);
   void __fastcall SetItemEnabled(int Index, bool Value);

protected:
public:
   __fastcall TGrayComboBox(TComponent* Owner);
   __property bool ItemEnabled[int Index] = {read=GetItemEnabled, write=SetItemEnabled};

__published:
   __property TDrawItemEvent OnDrawItem = {read=qOnDrawItem, write=qOnDrawItem};
   __property TNotifyEvent OnChange = {read=qOnChange, write=qOnChange};

};
//---------------------------------------------------------------------------
#endif
