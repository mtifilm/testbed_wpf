//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MTITrackBar.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TMTITrackBar *)
{
        new TMTITrackBar(NULL);
}
//---------------------------------------------------------------------------
__fastcall TMTITrackBar::TMTITrackBar(TComponent* Owner)
        : TTrackBar(Owner)
{
}
//---------------------------------------------------------------------------
namespace Mtitrackbar
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TMTITrackBar)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------
 