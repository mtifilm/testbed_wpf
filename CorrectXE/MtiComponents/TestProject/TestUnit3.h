//---------------------------------------------------------------------------

#ifndef TestUnit3H
#define TestUnit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MTIBusy.h"
#include "AdvancedEditPanel.h"
//---------------------------------------------------------------------------
class TForm5 : public TForm
{
__published:	// IDE-managed Components
	TButton *StartBusyButton;
	TMTIBusy *MTIBusy1;
	TLabel *Label3;
	TAdvancedEditPanel *AdvancedEditPanel1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm5(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm5 *Form5;
//---------------------------------------------------------------------------
#endif
