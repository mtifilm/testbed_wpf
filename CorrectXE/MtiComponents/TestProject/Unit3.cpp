//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"
#include "filesweeper.h"
#include "MTIio.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VDoubleEdit"
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
    this->Caption = String(GetFileName(GetApplicationName()).c_str());

   #ifdef _DEBUG
	 this->Caption = this->Caption + "-Debug ";
   #else
   	 this->Caption = this->Caption + "-Release ";
   #endif

   if (sizeof(int *) == 8)
   {
	  this->Caption += " 64 Bits";
   }
   else
   {
	  this->Caption = this->Caption + " 32 Bits";
   }

   	MEMORYSTATUSEX status;
	status.dwLength = sizeof(status);
	GlobalMemoryStatusEx(&status);
	ostringstream os;
	os << " " << min(status.ullAvailVirtual, status.ullAvailPhys) /1024.0/1024.0/1024.0 << " GB physical available";
	AnsiString tmpStr = AnsiString(os.str().c_str());

   this->Caption = this->Caption + tmpStr;;
}
//---------------------------------------------------------------------------
