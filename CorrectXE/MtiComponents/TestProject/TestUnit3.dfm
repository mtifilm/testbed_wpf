object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 433
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 24
    Top = 8
    Width = 58
    Height = 13
    Caption = 'Busy Cursor'
  end
  object StartBusyButton: TButton
    Left = 16
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
  end
  object MTIBusy1: TMTIBusy
    Left = 32
    Top = 27
    Width = 32
    Height = 32
  end
  object AdvancedEditPanel1: TAdvancedEditPanel
    Left = 104
    Top = 232
    Width = 337
    Height = 129
    Caption = 'AdvancedEditPanel1'
  end
end
