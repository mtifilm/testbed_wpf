/*
   File Name:  TriTimecode.cpp
   Created: Feb 3, 2001 by John Mertus
   Version 1.00

   This is a component that produces three timecode
      in
      out
      duration

   And does all the proper updating
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TriTimecode.h"
#pragma package(smart_init)
#pragma resource "*.res"
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TTriTimecode *)
{
        new TTriTimecode(NULL);
}
//---------------------------------------------------------------------------
// MakeCompatibleTC is used to assure that two timecodes have the same
// flags and fps; if they don't it will convert, trying to preserve the
// ASCII view.
//

static inline void EnsureCompatibleTC(const CTimecode &SourceTC,
                                      CTimecode &TargetTC)
{
   if (SourceTC.getFlags() != TargetTC.getFlags()
     || SourceTC.getFramesPerSecond() != TargetTC.getFramesPerSecond())
     {
        // Note this constructor works right, even if FPS == 0
        CTimecode NewTC(TargetTC.getHours(),
                        TargetTC.getMinutes(),
                        TargetTC.getSeconds(),
                        TargetTC.getFrames(),
                        SourceTC.getFlags(),
                        SourceTC.getFramesPerSecond());

        TargetTC = NewTC;
     }
}
//--------------Constructor--------------------------John Mertus---Feb 2001---

  __fastcall TTriTimecode::TTriTimecode(TComponent* Owner)
        : TCustomPanel(Owner)

//
//  Construct the display
//
//****************************************************************************
//---------------------------------------------------------------------------
{
    // Compute the size of the display
    // Note owner must be know at this point
    if (Parent == NULL) Parent = (TWinControl *)Owner;
    Visible = true;
    jDurationValid = true;

    //
    // Create the time code edits
    //
    InTC = new VTimeCodeEdit(this);
    InTC->Parent = this;
    InTC->OnTimecodeChange = InTimeCodeTimecodeChange;

    OutTC = new VTimeCodeEdit(this);
    OutTC->Parent = this;
    OutTC->OnTimecodeChange = OutTimeCodeTimecodeChange;

    DurationTC = new VTimeCodeEdit(this);
    DurationTC->tcP.setDropFrame(false);
    DurationTC->Parent = this;
    DurationTC->OnTimecodeChange = DurationTimeCodeTimecodeChange;
    //
    //  Now create all the labels
    //
    jInLabel = new TLabel(this);
    jInLabel->Parent = this;
    jInLabel->Alignment = taRightJustify;
    jInLabel->Caption = "&In";
    jInLabel->FocusControl = InTC;

    jOutLabel = new TLabel(this);
    jOutLabel->Parent = this;
    jInLabel->Alignment = taRightJustify;
    jOutLabel->Caption = "&Out";
    jOutLabel->FocusControl = OutTC;

    jDurationLabel = new TLabel(this);
    jDurationLabel->Parent = this;
    jDurationLabel->Alignment = taRightJustify;
    jDurationLabel->Caption = "&Duration";
    jDurationLabel->FocusControl = DurationTC;

    ComputeSizeAndWidth();
    IgnoreChangeCB = false;
    RO_SPROPERTY_INIT(DurationValid, jDurationValid);
    SET_CBHOOK(DurationValidChange, DurationValid);
    jFirstPaint = true;

    jDurationDominant = false;
    jAtomicUpdate = 0;
}

//-------------Paint-----------------------------John Mertus---Feb 2001---

   void __fastcall TTriTimecode::Paint(void)
//
//  Only used at design time to not allow the resize of the box
//  and to change a change in the font.
//
//*************************************************************************
{
   if (ComponentState.Contains(csDesigning) || jFirstPaint)
     {
       ComputeSizeAndWidth();
       jFirstPaint = false;
     }
   TCustomPanel::Paint();
}

//-------------ComputeSizeAndWidth-----------------John Mertus---Feb 2001---

    void TTriTimecode::ComputeSizeAndWidth(void)

//
//  This computes the proper size and width of the display
//
//*************************************************************************
{
    Caption = "";                                               // Never a caption
    Canvas->Font->Assign(Font);

    int h = Canvas->TextHeight("M");
    int w = Canvas->TextWidth("Duration");
    int Spacing = Canvas->TextWidth("M");
    int SmallSpacing = 0.75*Spacing;
    int TopSpace = Canvas->TextHeight('m');

    InTC->Width = Canvas->TextWidth("99:99:99:99 ") + 6;         // Nothing will be bigger
    OutTC->Width = InTC->Width;
    DurationTC->Width = InTC->Width;

    //
    // The width is
    //   Spacing + LabelWidth + Small space + VTimeCodeEdit->Width + Spacing
    Width = Spacing + w + SmallSpacing + InTC->Width + TopSpace;
    Height = 2*TopSpace + 3*InTC->Height + 6;
    //
    // Now set all the positions
    //
    InTC->Top = TopSpace;
    InTC->Left = Width - TopSpace - InTC->Width;

    OutTC->Top = InTC->Top + InTC->Height + 2;   // Just a little spacing
    OutTC->Left = InTC->Left;

    DurationTC->Top = OutTC->Top + OutTC->Height + 4;
    DurationTC->Left = InTC->Left;

    jInLabel->Left = InTC->Left - SmallSpacing - jInLabel->Width;
    jInLabel->Top = TopSpace  + (InTC->Height - h)/2 - 1;

    jOutLabel->Left =  OutTC->Left - SmallSpacing - jOutLabel->Width;
    jOutLabel->Top = jInLabel->Top + OutTC->Height + 2;

    jDurationLabel->Left = DurationTC->Left - SmallSpacing - jDurationLabel->Width;
    jDurationLabel->Top = jOutLabel->Top + DurationTC->Height + 4;

}

//----------------InTimeCodeTimecodeChange--------Mike Braca---Nov 2003---

  void __fastcall TTriTimecode::InTimeCodeTimecodeChange(TObject *Sender)

//
//  This is called whenever the in time code changes.  If the duration
//  is marked as dominant (vs the out TC) we move the out TC to preserve
//  the duration, then join common TC change code.
//
//****************************************************************************
{
    bool HandledChange(false);

    if (!(jAtomicUpdate++))
      {
        HandledChange = true;

        // Fix the new In value if necessary
        EnforceLimits();

        // Fix the Out TC if necessary, possibly moving it if we are
        // trying to preserve the Duration
        if (jDurationDominant)
          {
            // Crush the existing Out TC to ensure compatibility with In TC
            // CAREFUL! (TC - int) doesn't work, but (TC + (-int)) does
            OutTC->tcP = InTC->tcP + DurationTC->tcP.getAbsoluteTime()
                        + (jOutInclusive? -1 : 0);
          }
        else
          {
            EnsureCompatibleTC(InTC->tcP, OutTC->tcP);
          }

        // Common code
        HandleTimecodeChange();
      }
    --jAtomicUpdate;

    if (HandledChange && DurationValid)
      {
        // Timecode changed callback
        if ((fOnTCChange != NULL) && (!IgnoreChangeCB)) fOnTCChange(this);
      }
}

//----------------OutTimeCodeTimecodeChange--------Mike Braca---Nov 2003---

  void __fastcall TTriTimecode::OutTimeCodeTimecodeChange(TObject *Sender)

//
//  This is called whenever the out time code changes.  We set the
//  DurationDominant flag to false to indicate that the out TC should
//  be preserved in case of the in TC changing. Then we join common
//  processing code with the in TC.
//
//****************************************************************************
{
    bool HandledChange(false);

    if (!(jAtomicUpdate++))
      {
        HandledChange = true;

        // Limit the new Out value if necessary
        EnforceLimits();

        // Make sure the InTC properties are compatible
        EnsureCompatibleTC(OutTC->tcP, InTC->tcP);

        // Common code
        HandleTimecodeChange();

        // Now Out-TC-dominant
        jDurationDominant = false;
      }
    --jAtomicUpdate;

    if (HandledChange && DurationValid)
      {
        // Timecode changed callback
        if ((fOnTCChange != NULL) && (!IgnoreChangeCB)) fOnTCChange(this);
      }
}

//----------------HandleTimecodeChange--------John Mertus---Feb 2001---

  void TTriTimecode::HandleTimecodeChange()

//
//  This is called whenever the in or out time code changes.  The result
//  is a new duration is computed.  If the duration is not valid, the
//  label is set to flashing.
//
//****************************************************************************
{
    EnforceLimits();

    int Frames = OutTC->tcP.getAbsoluteTime() - InTC->tcP.getAbsoluteTime();

    SetDurationValid(Frames >= 0);
    if (!DurationValid) return;

    if (jOutInclusive) ++Frames;

    if (Frames != DurationTC->tcP.getAbsoluteTime()
      || InTC->tcP.getFlags() != DurationTC->tcP.getFlags()
      || InTC->tcP.getFramesPerSecond() != DurationTC->tcP.getFramesPerSecond())
      {
        // Set duration in absolute time, making sure the timecode
        // is compatible with the In TC (except never drop frame);
        // also careful to only change the control once to avoid
        // multiple callbacks
        CTimecode newDuration(0, 0, 0, Frames,
                              InTC->tcP.getFlags() &~ DROPFRAME,
                              InTC->tcP.getFramesPerSecond());
        DurationTC->tcP = newDuration;
      }
}

//--------------DurationTimeCodeTimecodeChange--------John Mertus---Feb 2001---

   void __fastcall TTriTimecode::DurationTimeCodeTimecodeChange(TObject *Sender)

//
//  This handles the change in the Duration, it adds in to the out.
//
//****************************************************************************
{
    bool HandledChange(false);

    if (!(jAtomicUpdate++))
      {
        HandledChange = true;
        jDurationDominant = true;

       // Convert back from absolute time to real time
       int Frames = InTC->tcP.getAbsoluteTime()
                    + DurationTC->tcP.getAbsoluteTime();

       if (jOutInclusive) Frames -= 1;

       OutTC->tcP.setAbsoluteTime(Frames);
       EnforceLimits();

       if (Frames != OutTC->tcP.getAbsoluteTime())
         {
          // Out was clipped -- try moving the In to preserve duration
          Frames = OutTC->tcP.getAbsoluteTime()
                 - DurationTC->tcP.getAbsoluteTime();
          if (jOutInclusive) Frames += 1;

          InTC->tcP.setAbsoluteTime(Frames);
          EnforceLimits();

          if (InTC->tcP.getAbsoluteTime() != Frames)
            {
             // Bad news... duration will have to change
             jDurationDominant = false;
             HandleTimecodeChange();
            }
         }
      }
    --jAtomicUpdate;

    if (HandledChange)
      {
        // Timecode changed callback
        if ((fOnTCChange != NULL) && (!IgnoreChangeCB)) fOnTCChange(this);
      }
}

//--------------DurationValidChange------------------John Mertus---Feb 2001---

    void TTriTimecode::DurationValidChange(void *p)

//
//  Come here on change in validity
//  The property is called here.
//
//****************************************************************************
{
   if (DurationValid)
      DisableHighlight();
   else
      EnableHighlight();

   if ((fOnInvalidChange != NULL) && (!IgnoreChangeCB)) fOnInvalidChange(this);
}


//--------------EnableHighlight-----------------------John Mertus---Feb 2001---

    void TTriTimecode::EnableHighlight(void)

//
//  Call to highlight, e.g, show a mistake
//
//****************************************************************************
{
   DurationTC->Color = clRed;
}

//--------------DisableHighlight----------------------John Mertus---Feb 2001---

    void TTriTimecode::DisableHighlight(void)

//
//  Disable showing of the the mistake
//
//****************************************************************************
{
   DurationTC->Color = InTC->Color;
}

//--------------SetDurationValid--------------------John Mertus---Feb 2001---

    void TTriTimecode::SetDurationValid(bool Value)

//
//  Internal routine for setting the DurationValid
//
//****************************************************************************
{
   if ((Value == jDurationValid) && (!DurationValid.GetInvalidState())) return;
   jDurationValid = Value;
   if (!jDurationValid)
     DurationTC->Invalidate();
   DurationValid.Notify();
}

//--------------EnforceLimits--------------------Mike Braca---Nov 2003---

    void TTriTimecode::EnforceLimits(void)

//
//  Internal routine for optionally enforcing min/max. Note: this only
//  fixes the In and Out timecodes, caller must then compute the Duration.
//
//****************************************************************************
{
   if (jAutoLimit)
     {
        int InFrame = InTC->tcP.getAbsoluteTime();
        int OutFrame = OutTC->tcP.getAbsoluteTime();
        int OutOffset = (jOutInclusive? 0 : 1);

        // Short circuit: both limits are negative
        //                or both are positive and min > max
        if ((jLimitMin < 0 && jLimitMax < 0) ||
            (jLimitMin >= 0 && jLimitMax >= 0 &&
                (jOutInclusive? (jLimitMax < jLimitMin)
                              : (jLimitMax <= jLimitMin))))
          {
            return;
          }

        // Limit InFrame
        if (jLimitMin >= 0 && InFrame < jLimitMin) InFrame = jLimitMin;
        if (jLimitMax >= 0 && InFrame > jLimitMax) InFrame = jLimitMax;

        // Limit OutFrame; the min is related to the InFrame
        if (InFrame >= 0)
          {
            int MinOutFrame = InFrame + OutOffset;
            if (OutFrame < MinOutFrame) OutFrame = MinOutFrame;
          }
        if (jLimitMax >= 0)
          {
            int MaxOutFrame = jLimitMax + OutOffset;
            if (OutFrame > MaxOutFrame) OutFrame = MaxOutFrame;
          }

        // Update the controls only if they changed
        // (avoid unneeded "TC changed" callbacks)
        if (InFrame != InTC->tcP.getAbsoluteTime())
          {
            InTC->tcP.setAbsoluteTime(InFrame);
          }
        if (OutFrame != OutTC->tcP.getAbsoluteTime())
          {
            OutTC->tcP.setAbsoluteTime(OutFrame);
          }
     }
}

//---------------------------------------------------------------------------
namespace Tritimecode
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TTriTimecode)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------
