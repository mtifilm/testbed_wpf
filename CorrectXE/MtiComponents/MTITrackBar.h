//---------------------------------------------------------------------------

#ifndef MTITrackBarH
#define MTITrackBarH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TMTITrackBar : public TTrackBar
{
private:
protected:
public:
        __fastcall TMTITrackBar(TComponent* Owner);
__published:
  	__property OnMouseDown;
  	__property OnMouseUp;
        __property OnMouseMove;
};

//---------------------------------------------------------------------------
#endif
