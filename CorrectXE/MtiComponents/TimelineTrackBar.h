//---------------------------------------------------------------------------

#ifndef TimelineTrackBarH
#define TimelineTrackBarH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <comctrls.hpp>
#include "IniFile.h"
//---------------------------------------------------------------------------

// These are the special styles of the trackbar
enum TTrackStyle {trUserDrawn,trFlat,trRaised,trLowered} ;
enum TThumbStyle {tsUserDrawn,tsRectangle,tsRaisedRectangle,tsLoweredRectangle};
enum TActiveDrag {adNONE, adTHUMB, adCURSOR, adBOTH};

extern PACKAGE CIniAssociations TrackStyleAssociations;
extern PACKAGE CIniAssociations ThumbStyleAssociations;
extern PACKAGE CIniAssociations ActiveDragAssociations;

enum TThumbResizingFcn {RESIZING_NOT, RESIZING_LEFT, RESIZING_RIGHT};
enum TCursorStyle {CS_SOLID, CS_DASHED_1_2_1, CS_DASHED_2_1_2, CS_3_WIDE, CS_5_WIDE};

typedef void __fastcall (__closure *TDragEvent)(TObject *Sender, int Value);

// This is a common cursor class for the timeline that knows how to
// draw itself
// Setters should be defined, but until it stabilizes, we don't care
class PACKAGE CTimelineCursor
{
   public:
   CTimelineCursor(TColor tcDefaultColor);
   virtual ~CTimelineCursor(void);

   virtual void Draw(int x, int y, const TRect &Boundary, Graphics::TBitmap* Bitmap);
   virtual void Draw(int x, int y, const TRect &Boundary, TCanvas *Canvas);
   virtual void Draw(int x, int y, Graphics::TBitmap* Bitmap);

   TColor Color;
   TColor DefaultColor;
   double Position;
   string Label;
   TCursorStyle Style;
   int FrameWidth;    // if > 0, shows a dashed line and end of frame
};

class PACKAGE CTimelineCursors : public  vector<CTimelineCursor>
{
  public:
	CTimelineCursors(void);
	void ActiveCursor(int Value);
	int ActiveCursor(void);
  private:

    int _ActiveCursor;
};

class PACKAGE TTimelineTrackBar : public TCustomControl
{
private:
    double _Max;
    double _Min;
    double _ThumbPosition;
    double _SmallChange;
    double _LargeChange;
    int    _TrackSize;
    bool   _ShowThumb;
    bool   _ShowThumbDetent;
    bool   _EnableThumbResize;
    bool   _ShowCursors;
    bool   _ShowHandles;
    bool   _ShowMarks;
    int    _NumberOfCursors;
    TActiveDrag _ActiveDrag;
    TActiveDrag _CurrentActiveDrag;
    TColor _Color;
    bool   _ShowFocus;
    bool   _EnableWheel;

    int _LeftHandles;
    int _RightHandles;
    int _MarkInFrame;       // < 0 if no mark
    int _MarkOutFrame;      // < 0 if no mark
    
    TTrackBarOrientation _Orientation;
    TTrackStyle _TrackStyle;

    double _Range;
    int _MinThumbSize;
    int _ThumbSize;
    int _TrackOffset;
    int _BorderWidth;

    TThumbStyle _ThumbStyle;

    TColor _ThumbColor;
    TColor _ThumbRangeColor;
    TColor _TrackColor;
    TColor _DefaultCursorColor;
    bool _ShowThumbRange;

    bool _Tracking;
    TThumbResizingFcn _ThumbResizing;
    TKeyEvent    _OnKeyDown;

    TNotifyEvent _OnChange;
    TNotifyEvent _OnExternalThumbChange;

    TDragEvent   _OnCursorChange;
    TDragEvent   _OnExternalCursorChange;
    TNotifyEvent _OnRangeChange;
    TNotifyEvent _OnPaint;
    TDragEvent   _SpecialDragEvent;
    bool         _JumpOnLeftDown;


    double _ExPosition;
    bool   _UseExPostion;
    bool   _IgnoreExPosition;

//    FOnDrawTrack:TDrawTrackEvent;
//    FOnDrawThumb:TDrawThumbEvent;

    TRect _RawTrackRect;
    TRect _TrackRect;

    int _UsablePixels;
    int _Centering;
    int _OldThumbX;
    int _OldThumbY;
    int _ThumbHalfSize;
    int _SpecialDragX;

	double _FrameWidth;
    
    TRect _ThumbRect;
    TRect _VThumbRect;     // Virtual, i.e., same as _ThumbRect except
                           // when thumb is under end points

    Graphics::TBitmap *_ThumbBmp;
    Graphics::TBitmap *_TrackBmp;
    Graphics::TBitmap *_BackGroundBmp;
    Graphics::TBitmap *_DitherBmp;

    // Properties getters and setters
	void __fastcall SetMax(double Value);
	void __fastcall SetMin(double Value);
	void __fastcall SetThumbPosition(double Value);
	void __fastcall SetOrientation(TTrackBarOrientation Value);
	void __fastcall SetTrackSize(int Value);
	void __fastcall SetTrackStyle(TTrackStyle Value);
	void __fastcall SetTrackColor(TColor Value);
	void __fastcall SetBorderWidth(int Value);

	void __fastcall SetThumbStyle(TThumbStyle Value);
	void __fastcall SetThumbSize(double Value);
	void __fastcall SetMinThumbSize(int Value);

	void __fastcall SetThumbColor(TColor Value);
	void __fastcall SetThumbRangeColor(TColor Value);

	void __fastcall SetShowCursors(bool Value);
	void __fastcall SetActiveDrag(TActiveDrag Value);
	void __fastcall SetNumberOfCursors(int Value);
	void __fastcall SetDefaultCursorColor(TColor Value);

	void __fastcall SetShowHandles(bool Value);
	void __fastcall SetLeftHandles(int Value);
	void __fastcall SetRightHandles(int Value);
	void __fastcall SetShowMarks(bool Value);
	void __fastcall SetMarkInFrame(int Value);
    void __fastcall SetMarkOutFrame(int Value);

    void __fastcall ThumbMouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
    void __fastcall ThumbMouseMove(Classes::TShiftState Shift, int X, int Y);
    void __fastcall CursorMouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
    void __fastcall CursorMouseMove(Classes::TShiftState Shift, int X, int Y);

    void __fastcall SetShowFocus(bool Value);
    
    void PositionInternal(double Value);
    void PositionCommon(double Value);

    double CursorPositionCommon(double Value, int nCur);
    double ThumbPositionInternal(double Value);
    double ThumbPositionCommon(double Value);

    bool _SpecialDrag;

    CTimelineCursors _Cursors;

protected:
    TTimer *ExtTimer;
    void __fastcall ExtTimerTimer(TObject *Sender);

    // Messages
    void __fastcall CMEnabledChanged(TMessage &Message);
    void __fastcall WMEraseBkgnd(TMessage &Message);
    void __fastcall WMActivateChange(TMessage &Message);
    void __fastcall WMGetDlgCode(TWMGetDlgCode &Message);
    void __fastcall WMSize(TWMSize &Message);
    void __fastcall CMMouseLeave(TMessage &Message);

    BEGIN_MESSAGE_MAP
       VCL_MESSAGE_HANDLER(WM_GETDLGCODE, TWMGetDlgCode, WMGetDlgCode)
       VCL_MESSAGE_HANDLER(WM_SIZE, TWMSize, WMSize)
       VCL_MESSAGE_HANDLER(CM_EXIT, TMessage, WMActivateChange)
       VCL_MESSAGE_HANDLER(CM_ENTER, TMessage, WMActivateChange)
       VCL_MESSAGE_HANDLER(WM_ACTIVATE, TMessage, WMActivateChange)
       VCL_MESSAGE_HANDLER(WM_ERASEBKGND , TMessage, WMEraseBkgnd)
       VCL_MESSAGE_HANDLER(CM_ENABLEDCHANGED, TMessage, WMActivateChange)
       VCL_MESSAGE_HANDLER(CM_MOUSELEAVE, TMessage, CMMouseLeave)

       //       VCL_MESSAGE_HANDLER(CM_CTL3DCHANGED, TMessage, CMCtl3DChanged)
    END_MESSAGE_MAP(TCustomControl)

    DYNAMIC void __fastcall MouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
    DYNAMIC void __fastcall MouseMove(Classes::TShiftState Shift, int X, int Y);
    DYNAMIC void __fastcall MouseUp(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
    DYNAMIC bool __fastcall DoMouseWheel(TShiftState Shift, int WheelDelta, const TPoint &MousePos);

	void __fastcall Paint(void);

    void UpdateTrackData(void);
    virtual void UpdateTrack(void);
    void UpdateDitherBitmap(void);

    void UpdateThumbData(void);
    virtual void UpdateThumb(void);

    void UpdateCursors(void);
    void UpdateMarks(void);
    void UpdateHandles(void);

    virtual void DrawTrack(void);
    virtual void DrawThumb(int X, int Y);

    void  __fastcall SetRange(double Value);
	void  __fastcall SetShowThumbRange(bool Value);

    DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
    DYNAMIC void __fastcall KeyUp(Word &Key, Classes::TShiftState Shift);

    void __fastcall SetColor(TColor Value);
    TColor __fastcall GetColor(void);
    bool __fastcall SetShowThumb(bool Value);
    bool __fastcall SetThumbDetent(bool Value);

    // Necessary to handle a bug in Borland
    void __fastcall DefineProperties(TFiler *Filer);
    void __fastcall MinimumWriterProc(TWriter *Writer);
    void __fastcall MaximumWriterProc(TWriter *Writer);

    void __fastcall SetActiveCursor(int Value);
    int __fastcall  GetActiveCursor(void);

public:
    __fastcall TTimelineTrackBar(TComponent* Owner);
    __fastcall ~TTimelineTrackBar(void);

    void   Range(double Value);
    double Range(void);

    void   ShowThumbRange(bool Value);
    bool   ShowThumbRange(void);

    void   PositionOfThumb(double Value);
    double PositionOfThumb(void);

    void   Position(double Value);
    double Position(void);
    int    IPosition(void);

    // A pointer to the list of cursors
    // This should reall be a class
    CTimelineCursors *Cursors(void);

    double CursorPosition(double Value);
    double CursorPosition(void);
    double CursorPosition(double Value, int nCur);
    double CursorPosition(int ad);
    double CursorPositionInternal(double Value, int nCur);
    int    ICursorPosition(void);

    void   Min(double Value);
    double Min(void);

    void   Max(double Value);
    double Max(void);

    void  Background(Graphics::TBitmap *Value);
    Graphics::TBitmap *Background(void);

    double RangeMin(void);
    double RangeMax(void);

    bool IsDragging(void);

    TRect TrackRect(void) const;

    int PositionInPixels(void);
    int PositionInPixels(double Value);
    double PositionFromPixels(int Value);

    void ScrollToFrame(double Frame);  // scroll to show frame, not necessarily centered

    virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.

__published:
      __property Align;
      __property Anchors;
      __property Left;
      __property Top;
      __property Width;
      __property Height;
      __property Visible;
      __property Enabled;
      __property Font;
      __property ParentFont;
      __property ParentColor;
      __property ParentShowHint;
      __property PopupMenu;
      __property TabOrder;
      __property TabStop;
      __property ShowHint;
      __property Hint;
      __property Cursor;
      __property Ctl3D;
      __property DragCursor;
      __property DragMode;
      __property HelpContext;
      __property OnMouseMove;
      __property OnMouseDown;
      __property OnMouseUp;
      __property OnKeyPress;
      __property OnKeyUp;

	__property double ThumbRange = {read = _Range, write = SetRange, default = 0};
	__property double ThumbPosition = {read = _ThumbPosition, write = SetThumbPosition};
	__property double Maximum = {read = _Max, write = SetMax, default = 10};
	__property double Minimum = {read = _Min, write = SetMin, default = -10};
    __property bool   EnableThumbRange = {read = _ShowThumbRange, write = SetShowThumbRange, default=true};
    __property bool   EnableThumbResize = {read = _EnableThumbResize, write = _EnableThumbResize, default=true};

    __property int MinThumbSize = {read = _MinThumbSize, write = SetMinThumbSize, default = 17};
    __property TTrackStyle TrackStyle = {read = _TrackStyle, write = SetTrackStyle, default = trLowered};
    __property TThumbStyle ThumbStyle = {read = _ThumbStyle, write = SetThumbStyle, default = tsRaisedRectangle};
    __property TColor TrackColor = {read = _TrackColor, write = SetTrackColor, default = clWhite};
    __property TColor ThumbColor = {read = _ThumbColor, write = SetThumbColor, default = clBtnFace};
    __property TColor Color = {read = GetColor, write = SetColor, default = clBtnFace};
    __property TColor ThumbRangeColor = {read = _ThumbRangeColor, write = SetThumbRangeColor, default = clBtnFace};
    __property TNotifyEvent OnPaint = {read = _OnPaint, write=_OnPaint};
    __property TNotifyEvent OnRangeChange = {read = _OnRangeChange, write=_OnRangeChange};
    __property TNotifyEvent OnChange = {read = _OnChange, write=_OnChange};
    __property TNotifyEvent OnExternalThumbChange = {read = _OnExternalThumbChange, write=_OnExternalThumbChange};
    __property TDragEvent OnSpecialDrag = {read = _SpecialDragEvent, write=_SpecialDragEvent};
    __property TDragEvent OnCursorChange = {read = _OnCursorChange, write=_OnCursorChange};
    __property TDragEvent OnExternalCursorChange = {read = _OnExternalCursorChange, write=_OnExternalCursorChange};
    __property TKeyEvent OnKeyDown = {read = _OnKeyDown, write=_OnKeyDown};
	__property bool ShowThumb = {read = _ShowThumb, write = SetShowThumb, default=true};
	__property bool ShowThumbDetent = {read = _ShowThumbDetent, write = SetThumbDetent, default=true};
	__property bool ShowCursors = {read = _ShowCursors, write = SetShowCursors,  default=false};
    __property bool ShowHandles = {read = _ShowHandles, write = SetShowHandles,  default=false};
    __property bool ShowMarks = {read = _ShowMarks, write = SetShowMarks,  default=false};
    __property bool JumpOnLeftDown = {read = _JumpOnLeftDown, write = _JumpOnLeftDown, default=false};
    __property int NumberOfCursors = {read = _NumberOfCursors, write = SetNumberOfCursors, default=0};
	__property TActiveDrag ActiveDrag = {read = _ActiveDrag, write = SetActiveDrag, default=adTHUMB};
	__property TColor DefaultCursorColor = {read = _DefaultCursorColor, write = SetDefaultCursorColor, default=clBlue};
	__property int ActiveCursor = {read = GetActiveCursor, write = SetActiveCursor, default=0};
    __property int BorderWidth = {read = _BorderWidth, write=SetBorderWidth, default=6};
    __property bool ShowFocus = {read = _ShowFocus, write=SetShowFocus, default=true};
    __property bool EnableWheel = {read = _EnableWheel, write=_EnableWheel, default=false};
    __property int LeftHandles = {read = _LeftHandles, write = SetLeftHandles, default=0};
    __property int RightHandles = {read = _RightHandles, write = SetRightHandles, default=0};
    __property int MarkInFrame = {read = _MarkInFrame, write = SetMarkInFrame, default=-1};
    __property int MarkOutFrame = {read = _MarkOutFrame, write = SetMarkOutFrame, default=-1};

};


//---------------------------------------------------------------------------
#endif
