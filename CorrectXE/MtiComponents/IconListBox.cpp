//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "IconListBox.h"
#pragma package(smart_init)
#pragma resource "*.res"
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TIconListBox *)
{
        new TIconListBox(NULL);
}
//---------------------------------------------------------------------------
__fastcall TIconListBox::TIconListBox(TComponent* Owner)
        : TCustomListBox(Owner)
{
    Style = lbOwnerDrawFixed;
    OnDrawItem = ListBoxDrawItem;
}

void __fastcall TIconListBox::ListBoxDrawItem(TWinControl *Control, int Index, const TRect &Rect, TOwnerDrawState State)
{
  Graphics::TBitmap *pBitmap; // temporary variable for item�s bitmap
  int Offset = 2;   // default text offset width
  int VOffset = 0;  // Default vertical offser

  // note that we draw on the listbox�s canvas, not on the form
  TCanvas *pCanvas = ((TListBox *)Control)->Canvas;
  pCanvas->FillRect(Rect); // clear the rectangle
  ObjectData *od = ((ObjectData *)((TListBox *)Control)->Items->Objects[Index]);

  // If no data, just ignore bitmap
  if (od)
    pBitmap = od->Bitmap;
  else
    pBitmap = NULL;

  AnsiString s = ((TListBox *)Control)->Items->Strings[Index];

  if (pBitmap)
  {
//    pCanvas->BrushCopy(Bounds(Rect.Left + Offset, Rect.Top, pBitmap->Width, pBitmap->Height), pBitmap, Bounds(0, 0, pBitmap->Width, pBitmap->Height), pBitmap->Canvas->Pixels[0][0]); // render bitmap
    pCanvas->Draw(Rect.Left + Offset, Rect.Top, pBitmap);
    Offset += pBitmap->Width + 4;   // add four pixels between bitmap and text
    VOffset = (pBitmap->Height - pCanvas->TextHeight(s))/2;
  }
  // display the text
  pCanvas->TextOut(Rect.Left + Offset, Rect.Top+VOffset, s);
}

   void __fastcall TIconListBox::SetImages(Imglist::TCustomImageList* Value)
{
   FImages = Value;
   ItemHeight = FImages->Height;
}

//---------------------SetBitmap-------------John Mertus--March 2001------

    void __fastcall TIconListBox::SetBitmap(Graphics::TBitmap *bm, int Index)


// The Object of the list box is broken into data/bitmap, this
// checks to see if the index exists in the list box.  If so, it
// add the bitmap to the listbox.
//
//*********************************************************************
{
    // Ignore out of bounds
    if (Items->Count <= 0) return;
    if (Index >= Items->Count) return;

    // Create an object if not yet defined
    if (Items->Objects[Index] == NULL)
      {
        Items->Objects[Index] = new ObjectData;
      }

    // Replace the bitmap which is owned by the icon list
    ObjectData *od = (ObjectData *)(Items->Objects[Index]);

    if (od->Bitmap != NULL) delete od->Bitmap;
    od->Bitmap = new Graphics::TBitmap;
    od->Bitmap->Assign(bm);
};

//---------------------SetData-------------John Mertus--March 2001------

    void __fastcall TIconListBox::SetData(void *op, int Index)


// The Object of the list box is broken into data/bitmap, this
// checks to see if the index exists in the list box.  If so, it
// add the data to the listbox.
//
//*********************************************************************
{
    // Ignore out of bounds
    if (Items->Count <= 0) return;
    if (Index >= Items->Count) return;

    // Create an object if not yet defined
    if (Items->Objects[Index] == NULL)
      {
        Items->Objects[Index] = new ObjectData;
      }

    // Replace the bitmap which is owned by the icon list
    ObjectData *od = (ObjectData *)(Items->Objects[Index]);
    od->p = op;
};

//---------------------GetData------------John Mertus--March 2001------

      void * __fastcall TIconListBox::GetData(int Index)

// The Object of the list box is broken into data/bitmap, this returns
// the data.
//
//*********************************************************************
{
    if (Index < 0) return(NULL);
    if (Index >= Items->Count) return(NULL);
    
    // Create an object if not yet defined
    if (Items->Objects[Index] == NULL)
      {
        return(NULL);
      }

    // Replace the bitmap which is owned by the icon list
    ObjectData *od = (ObjectData *)(Items->Objects[Index]);
    return(od->p);
};

//---------------------GetBitmap-------------John Mertus--March 2001------

      Graphics::TBitmap *__fastcall TIconListBox::GetBitmap(int Index)

// The Object of the list box is broken into data/bitmap, this returns
// the bitmap pointer.
//
//*********************************************************************
{
    // Create an object if not yet defined
    if (Items->Objects[Index] == NULL)
      {
        return(NULL);
      }

    // Return a pointer to the bitmap
    ObjectData *od = (ObjectData *)(Items->Objects[Index]);
    return(od->Bitmap);
};

//---------------------Assign------------------John Mertus--March 2001------

   void __fastcall TIconListBox::Assign(TPersistent * Source)


// This assigns a the iconlist box to the other list box
// All data is assigned but no ownership
//
//*********************************************************************
{
   TIconListBox *ILB = (TIconListBox *)Source;   // Inheritence bullshit

   Clear();    // Erase all

   Items->Assign(ILB->Items);        // Copy over all the data
   ItemHeight = ILB->ItemHeight;     // Make the heights the same
   Font->Assign(ILB->Font);
}

//---------------------------------------------------------------------------
namespace Iconlistbox
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TIconListBox)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------
