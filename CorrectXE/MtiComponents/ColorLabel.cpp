//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "ColorLabel.h"
#include "IniFile.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TColorLabel *)
{
	new TColorLabel(NULL);
}
//---------------------------------------------------------------------------
__fastcall TColorLabel::TColorLabel(TComponent* Owner)
	: TLabel(Owner)
{
}

//---------------------Paint----------------------John Mertus----Sep 2003-----

  void __fastcall TColorLabel::Paint(void)

//   This is called whenever Windows repaints.
// Otherwise just create the main bitmap from the Text, Boarder and LEDS
// and Draw Mainbitmap to control
//
//****************************************************************************
{
	// Set color TODO: Obey ParentColor
	Canvas->Brush->Color = this->Color;
   Canvas->Brush->Style = this->Transparent ? bsClear : bsSolid;

   // Use the canvas font.
   Canvas->Font = Font;

   // Clipping rectangle is the entire control.
   TRect Rect = TRect(0, 0, Width, Height);

   int textPositionX;
   int textPositionY = AlignWithMargins ? Margins->Top : 0;
   UINT textAlignment;
	switch (this->Alignment)
	{
   default:
	case taLeftJustify:
      textAlignment = TA_LEFT;
      textPositionX = AlignWithMargins ? Margins->Left : 0;
		break;

	case taCenter:
      textAlignment = TA_CENTER;
      textPositionX = AlignWithMargins ? ((Margins->Left + Margins->Right) / 2) : (Width / 2);
		break;

	case taRightJustify:
		textAlignment = TA_RIGHT;
      textPositionX = Width - (AlignWithMargins ? Margins->Right : 0);
		break;
	}

   WORD SavedAlign = SetTextAlign(Canvas->Handle, textAlignment);
   Canvas->TextRect(Rect, textPositionX, textPositionY, Caption);
   SetTextAlign(Canvas->Handle, SavedAlign);
}
//---------------------------------------------------------------------------
namespace Colorlabel
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TColorLabel)};
		RegisterComponents(L"MTI", classes, 0);
	}
}
//---------------------------------------------------------------------------
