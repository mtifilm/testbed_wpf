//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MTIProgressBar.h"
#pragma resource "*.res"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TMTIProgressBar *)
{
        new TMTIProgressBar(NULL);
}
//---------------------------------------------------------------------------
__fastcall TMTIProgressBar::TMTIProgressBar(TComponent* Owner)
        : TGraphicControl(Owner)
{
  Width = 200;
  Height = 20;

  fBorderColor1 = clBtnShadow;
  fBorderColor2 = clBtnHighlight;
  fBackgroundColor = clBtnFace;
  fBarBitmap = new Graphics::TBitmap();
  fBarColor1 = clGreen;
  fBarColor2 = clYellow;
  fBarColor3 = clRed;
  fBarColorStyle = cs3Colors;
  fPosition = 0;
  fMin = 0;
  fMax = 100;
  fShowBorder = true;
  fDirection = pdLeftToRight;
  fShowPosText = true;
  fPosTextSuffix = "%";
  fPosTextPrefix = "";
  fRegenerateBitmap = true;

  MainBitmap = new Graphics::TBitmap();
  TiledBarBitmap = new Graphics::TBitmap();
}

// TMTIProgressBar


__fastcall TMTIProgressBar::~TMTIProgressBar(void)
{
  delete MainBitmap;
  delete fBarBitmap;
  delete TiledBarBitmap;
}

void __fastcall TMTIProgressBar::Paint(void)
{
  TGraphicControl::Paint();

  MainBitmap->Width = Width;
  MainBitmap->Height = Height;

  if (!ComponentState.Contains(csReading)) PaintBar(fRegenerateBitmap);

  if (fShowBorder) PaintBorder();

  if (fShowPosText) PaintPosText();

  Canvas->Draw(0, 0, MainBitmap);
}

void __fastcall TMTIProgressBar::PaintBar(bool RegenerateBitmap)
{
  int BarLength, BarPixelLength, EmptySpaceLength;
  int AreaTop, AreaBottom, AreaLeft, AreaRight;

  if ((fBarBitmap != NULL) && (!fBarBitmap->Empty))
  {
    if (RegenerateBitmap)
    {
      TiledBarBitmap->Height = Height;
      TiledBarBitmap->Width = Width;
      TileBitmap(fBarBitmap, TiledBarBitmap);
    }
	MainBitmap->Canvas->Draw(0, 0, TiledBarBitmap);
  }
  else if (fBarColorStyle == cs1Color)
  {
    MainBitmap->Canvas->Brush->Color = fBarColor1;
    MainBitmap->Canvas->FillRect(Rect(0, 0, Width, Height));
  }
  else if ((fBarColorStyle == cs2Colors) || (fBarColorStyle == cs3Colors))
  {
    if (RegenerateBitmap)
    {
      TiledBarBitmap->Height = Height;
      TiledBarBitmap->Width = Width;
      DrawColorBlending();
    }
    MainBitmap->Canvas->Draw(0, 0, TiledBarBitmap);
  }

  if ((fDirection == pdLeftToRight) || (fDirection == pdRightToLeft))
  {
    if (fShowBorder)
      BarPixelLength = Width - 2;
    else
      BarPixelLength = Width;
  }
  else
  {
    if (fShowBorder)
      BarPixelLength = Height - 2;
    else
      BarPixelLength = Height;
  }

  if (fShowBorder)
  {
    AreaTop = 1;
    AreaLeft = 1;
    AreaBottom = Height - 1;
    AreaRight = Width - 1;
  }
  else
  {
    AreaTop = 0;
    AreaLeft = 0;
    AreaBottom = Height;
    AreaRight = Width;
  }

  if (fPosition > Min)
    BarLength = ((fPosition-fMin) / (float)abs(fMax-fMin)) * BarPixelLength + 0.5;
  else
    BarLength = 0;
  EmptySpaceLength = BarPixelLength - BarLength;

  MainBitmap->Canvas->Brush->Color = fBackgroundColor;
  if (fDirection == pdLeftToRight)
    MainBitmap->Canvas->FillRect(Rect(AreaRight-EmptySpaceLength, AreaTop, AreaRight, AreaBottom));
  else if (fDirection == pdRightToLeft)
    MainBitmap->Canvas->FillRect(Rect(AreaLeft, AreaTop, AreaLeft+EmptySpaceLength, AreaBottom));
  else if (fDirection == pdTopToBottom)
    MainBitmap->Canvas->FillRect(Rect(AreaLeft, AreaBottom-EmptySpaceLength, AreaRight, AreaBottom));
  else if (fDirection == pdBottomToTop)
    MainBitmap->Canvas->FillRect(Rect(AreaLeft, AreaTop, AreaRight, AreaTop+EmptySpaceLength));
}

	void __fastcall TMTIProgressBar::DrawColorBlending(void)
{
  int IndexCount, MaxWidth, MaxHeight, StartPoint;
  TColor FirstColor, SecondColor;
  if (fBarColorStyle == cs2Colors)
  {
    MaxWidth = TiledBarBitmap->Width;
    MaxHeight = TiledBarBitmap->Height;
  }
  else
  {
    MaxWidth = TiledBarBitmap->Width/2;
    MaxHeight = TiledBarBitmap->Height/2;
  }

  StartPoint = 1;
  if ((fDirection == pdLeftToRight) || (fDirection == pdRightToLeft))
  {
    if (fDirection == pdLeftToRight)
    {
      FirstColor = fBarColor1;
      SecondColor = fBarColor2;
    }
    else
    {
      if (fBarColorStyle == cs2Colors)
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor1;
      }
      else
      {
        FirstColor = fBarColor3;
        SecondColor = fBarColor2;
      }
    }
    for (IndexCount = StartPoint; IndexCount <= MaxWidth; IndexCount++)
    {
      TiledBarBitmap->Canvas->Pen->Color = CalcColorIndex(FirstColor, SecondColor, MaxWidth, IndexCount);
      TiledBarBitmap->Canvas->MoveTo(IndexCount-1, 0);
      TiledBarBitmap->Canvas->LineTo(IndexCount-1, TiledBarBitmap->Height);
    }
    if (fBarColorStyle == cs3Colors)
    {
      if (fDirection == pdLeftToRight)
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor3;
      }
      else
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor1;
      }
      for (IndexCount = MaxWidth+1; IndexCount <= TiledBarBitmap->Width; IndexCount++)
	  {
		TiledBarBitmap->Canvas->Pen->Color = CalcColorIndex(FirstColor, SecondColor, TiledBarBitmap->Width-MaxWidth, IndexCount);
		TiledBarBitmap->Canvas->MoveTo(IndexCount-1, 0);
        TiledBarBitmap->Canvas->LineTo(IndexCount-1, TiledBarBitmap->Height);
      }
    }
  }
  else //if ((fDirection in [pdTopToBottom, pdBottomToTop])
  {
    if (fDirection == pdTopToBottom)
    {
      FirstColor = fBarColor1;
      SecondColor = fBarColor2;
    }
    else
    {
      if (fBarColorStyle == cs2Colors)
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor1;
      }
      else
      {
        FirstColor = fBarColor3;
        SecondColor = fBarColor2;
      }
    }
    for (IndexCount = StartPoint; IndexCount <= MaxHeight; IndexCount++)
    {
      TiledBarBitmap->Canvas->Pen->Color = CalcColorIndex(FirstColor, SecondColor, MaxHeight, IndexCount);
      TiledBarBitmap->Canvas->MoveTo(0, IndexCount-1);
      TiledBarBitmap->Canvas->LineTo(TiledBarBitmap->Width, IndexCount-1);
    }
    if (fBarColorStyle == cs3Colors)
    {
      if (fDirection == pdTopToBottom)
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor3;
      }
      else
      {
        FirstColor = fBarColor2;
        SecondColor = fBarColor1;
      }
      for (IndexCount = MaxHeight+1; IndexCount <= TiledBarBitmap->Height; IndexCount++)
      {
        TiledBarBitmap->Canvas->Pen->Color = CalcColorIndex(FirstColor, SecondColor, TiledBarBitmap->Height-MaxHeight, IndexCount-MaxHeight);
        TiledBarBitmap->Canvas->MoveTo(0, IndexCount-1);
        TiledBarBitmap->Canvas->LineTo(TiledBarBitmap->Width, IndexCount-1);
      }
    }
  }
    fRegenerateBitmap = false;
}

	void __fastcall TMTIProgressBar::TileBitmap(Graphics::TBitmap* TiledBitmap, Graphics::TBitmap* &DestBitmap)
{
  int NoOfImagesX, NoOfImagesY, ix, iy, XPos, YPos;
  NoOfImagesX = (Width/TiledBitmap->Width) + 1;
  NoOfImagesY = (Height/TiledBitmap->Height) + 1;
  XPos = 0;
  YPos = 0;
  for (iy = 1; iy <= NoOfImagesY; iy++)
  {
    for (ix = 1; ix <= NoOfImagesX; ix++)
    {
      DestBitmap->Canvas->Draw(XPos, YPos, TiledBitmap);
      XPos = XPos + TiledBitmap->Width;
    }
    YPos = YPos + TiledBitmap->Height;
    XPos = 0;
  }
}

	void __fastcall TMTIProgressBar::PaintPosText(void)
{
  String Text;
  int TextPosX, TextPosY;

  Text = fPosTextPrefix + IntToStr(fPosition) + fPosTextSuffix;
  TextPosX = (Width/2) - (MainBitmap->Canvas->TextWidth(Text)/2);
  TextPosY = (Height/2) - (MainBitmap->Canvas->TextHeight(Text)/2);
  MainBitmap->Canvas->Pen->Color = clBlack;
  MainBitmap->Canvas->Brush->Style = bsClear;
  MainBitmap->Canvas->TextOut(TextPosX, TextPosY, Text);
}

	void __fastcall TMTIProgressBar::PaintBorder(void)
{
    MainBitmap->Canvas->Pen->Color = fBorderColor1;
    MainBitmap->Canvas->MoveTo(Width-1, 0);
    MainBitmap->Canvas->LineTo(0, 0);
    MainBitmap->Canvas->LineTo(0, Height);
    MainBitmap->Canvas->Pen->Color = fBorderColor2;
    MainBitmap->Canvas->MoveTo(1, Height-1);
    MainBitmap->Canvas->LineTo(Width-1, Height-1);
    MainBitmap->Canvas->LineTo(Width-1, 0);
}

  Graphics::TColor __fastcall TMTIProgressBar::CalcColorIndex(Graphics::TColor StartColor, Graphics::TColor EndColor,
		int Steps, int ColorIndex)

{
  TColor Result;
  if ((ColorIndex < 1) || (ColorIndex > 2*Steps)) throw;
//    raise ERangeError.Create('ColorIndex can''t be less than 1 or greater than ' + IntToStr(Steps));
  switch (fDirection)
    {
	  case pdLeftToRight:
	  case pdBottomToTop:
	  case pdTopToBottom:
		  if ((ColorIndex < fBarValue1*2*Steps)) Result = fBarColor1;
		  else if ((ColorIndex < fBarValue2*2*Steps)) Result = fBarColor2;
		  else Result = fBarColor3;
	  break;

	  case pdRightToLeft:
		  if ((2*Steps - ColorIndex+1) < (fBarValue1*2*Steps)) Result = fBarColor1;
		  else if ((2*Steps - ColorIndex+1) < (fBarValue2*2*Steps)) Result = fBarColor2;
		  else Result = fBarColor3;
		  break;
    }
  return Result;
}

  void __fastcall TMTIProgressBar::SetBarBitmap(const Graphics::TBitmap* Value)
{
//TBD  fBarBitmap->Assign(Value);
  Paint();
}

  void __fastcall TMTIProgressBar::SetBackgroundColor(const Graphics::TColor Value)
{
  fBackgroundColor = Value;
  Paint();
}

   void __fastcall TMTIProgressBar::SetBorderColor1(const Graphics::TColor Value)
{
  fBorderColor1 = Value;
  Paint();
}

   void __fastcall TMTIProgressBar::SetBorderColor2(const Graphics::TColor Value)
{
  fBorderColor2 = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetPosition(const int Value)
{
  if ((fPosition != Value) && (Value <= fMax) && (Value >= fMin))
  {
	fPosition = Value;
	Paint();
  }
}

  void __fastcall TMTIProgressBar::SetMax(const int Value)
{
  fMax = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetMin(const int Value)
{
  fMin = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetShowBorder(const bool Value)
{
  fShowBorder = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetDirection(const TProgressDirection Value)
{
  fDirection = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetBarColor1(const Graphics::TColor Value)
{
  fBarColor1 = Value;
  fRegenerateBitmap = true;
  Paint();
}

  void __fastcall TMTIProgressBar::SetBarColor2(const Graphics::TColor Value)
{
  fBarColor2 = Value;
  fRegenerateBitmap = true;
  Paint();
}

   void __fastcall TMTIProgressBar::SetBarColor3(const Graphics::TColor Value)
{
  fBarColor3 = Value;
  fRegenerateBitmap = true;
  Paint();
}

   void __fastcall TMTIProgressBar::SetShowPosText(const bool Value)
{
  fShowPosText = Value;
  Paint();
}

   void __fastcall TMTIProgressBar::SetPosTextSuffix(const AnsiString Value)
{
  fPosTextSuffix = Value;
  Paint();
}

   void __fastcall TMTIProgressBar::SetPosTextPrefix(const AnsiString Value)
{
  fPosTextPrefix = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::CMFontChanged(Messages::TMessage &Message)
{
  MainBitmap->Canvas->Font->Assign(Font);
  Paint();
}

  void __fastcall TMTIProgressBar::SetBarColorStyle(const TBarColorStyle Value)
{
  fBarColorStyle = Value;
  Paint();
}

  void __fastcall TMTIProgressBar::SetBarValue1(const double Value)
{
  fBarValue1 = Value;
  fRegenerateBitmap = true;
  Paint();
}

   void __fastcall TMTIProgressBar::SetBarValue2(const double Value)
{
  fBarValue2 = Value;
  fRegenerateBitmap = true;
  Paint();
}

//---------------------------------------------------------------------------
namespace Mtiprogressbar
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TMTIProgressBar)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------

