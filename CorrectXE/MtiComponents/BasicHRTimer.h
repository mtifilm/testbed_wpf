//---------------------------------------------------------------------------

#ifndef BasicHRTimerH
#define BasicHRTimerH
//---------------------------------------------------------------------------

/*
  This defines a high resolution timer.  There are two important
  member functions
      Start    Which start counting
      Read     Which returns the current count in milliseconds;

*/

#include <string>
#include <windows.h>

class CHRTimer
 {
   public:
	 CHRTimer(void);        // Construct it, does a start
	~CHRTimer(void);
	 double Read(bool reset=false);
	 void Start(void);
	 string ReadAsString(void);
	 void Delay(float fDelay);     // Delays for fDelay milliseconds
								   // can do sub milliseconds
	 void DelayNS(float fDelay);   // Non-spinning version of Delay
								   // can do sub milliseconds
	 void Sleep (int iDelay);    // Sleeps for the specified number
				   // of milliseconds

   private:
     double m_dClockFreq;   // The HR clock frequency
	 double m_dStartCount;  // The start fills this in
	 UINT wTimerRes;     // resolution of the Windows Multi-media timer
	 HANDLE hWaitableTimer; // handle to waitable timer
 };
//---------------------------------------------------------------------------

void SleepMS(int ms);
#endif
