//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GrayComboBox.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TGrayComboBox *)
{
        new TGrayComboBox(NULL);
}
//---------------------------------------------------------------------------
__fastcall TGrayComboBox::TGrayComboBox(TComponent* Owner)
		: TComboBox(Owner)
{
	this->Style = TComboBoxStyle::csOwnerDrawFixed;
	TComboBox::OnDrawItem = BoxDrawItemEvent;
////	TComboBox::OnChange = OnComboBoxChangeCB;
	qOldItemIndex = -1;
}

void __fastcall  TGrayComboBox::BoxDrawItemEvent(TWinControl* Control, int Index, const System::Types::TRect &Rect, Winapi::Windows::TOwnerDrawState State)
{
    // Should never happen, but just to be safe
	if (Index < 0) return;

    // eliminate artifacts
    Canvas->FillRect(Rect);

    // check to see if Index is our "disabled" item
    if (Items->Objects[Index] != NULL)
    {
        // If this is not an Edit box; e.g., a drop down menu, grey it
        if (!State.Contains(odComboBoxEdit))
          {
            // gray out it's text
            Canvas->Font->Color = clGray;

            // If selected, we swap colors
            if (State.Contains(odSelected))
            {
                Canvas->Brush->Color = clGray;
                Canvas->Font->Color = clBlack;
                Canvas->FillRect(Rect);
            }
          }
        else
          {
            // Write old text back out
            if ((qOldItemIndex >= 0) && (qOldItemIndex < Items->Count))
              Canvas->TextOut(Rect.Left, Rect.Top, Items->Strings[qOldItemIndex]);
            return;
          }
    }
    else
      {
        // Non disabled state
        if (State.Contains(odSelected))
          Canvas->Font->Color = clWhite;
        else
          Canvas->Font->Color = clBlack;
      }

    // Now display the text
      Canvas->TextOut(Rect.Left, Rect.Top, Items->Strings[Index]);
}

void __fastcall TGrayComboBox::OnComboBoxChangeCB(TObject *Sender)
{
   if (ItemIndex < 0) return;

   if (Items->Objects[ItemIndex] != NULL)
   {
       DroppedDown = true;
       ItemIndex = qOldItemIndex;
       return;
   }

   qOldItemIndex = ItemIndex;

   if (this->qOnChange != NULL)
   {
      this->qOnChange(Sender);
   }
}

   bool __fastcall TGrayComboBox::GetItemEnabled(int Index)
   {
     if ((Index >= 0) && (Index < Items->Count))
       return(Items->Objects[Index] == NULL);
     return(true);

   }
   void __fastcall TGrayComboBox::SetItemEnabled(int Index, bool Value)
   {
	 if ((Index >= 0) && (Index < Items->Count))
	 {
	   if (Value)
		 Items->Objects[Index] = NULL;
	   else
		 Items->Objects[Index] = (TComponent *)1;
	 }
   }

//---------------------------------------------------------------------------
namespace Graycombobox
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TGrayComboBox)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------


