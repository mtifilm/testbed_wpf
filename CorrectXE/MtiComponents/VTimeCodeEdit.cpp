//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "VTimeCodeEdit.h"
#include <clipbrd.hpp>
#include "MTIio.h"

#pragma resource "*.res"
#pragma package(smart_init)
//
//  The logic for a VTimeCode is
//     VTimeCode is a visual component and has a PTimecode tcP, attached
//     to the property.
//      Changing this tc updates the display, or changing the display
//     update tcP
//     When the tcP changes, the tcP.notify() is called and the
//     Onchanged handler is called.
//
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(VTimeCodeEdit *)
{
      new VTimeCodeEdit(NULL);
}

//---------------------------------------------------------------------------
__fastcall VTimeCodeEdit::VTimeCodeEdit(TComponent* Owner)
        : TCustomEdit(Owner), tcP(DROPFRAME,24)
{
   Width = 72;
   AutoSelect = true;
   Ctl3D = false;
   ParentCtl3D = false;

   _OnEnterKey = NULL;
   _OnEscapeKey = NULL;

   // Common to VTimecodeEdit and VTimecodeHistory
   tcP = "1 0 0 0";
   _AllowVTRCopy = false;   // Default changed because VTR no longer supported
   _InternalHint = true;
   _OnExit = NULL;

   // Do the key processing
   OnKeyPress = VKeyPress;

   _PopupMenu = NULL;
   CreatePopUp();

   // What happens on change
   UpdateDisplay();
   SET_CBHOOK(TCChanged, tcP);
   tcP.InvalidateData();

   _oldTimecode = CTimecode::NOT_SET;
}

//---------------------------------------------------------------------------
namespace Vtimecodeedit
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(VTimeCodeEdit)};
                 RegisterComponents("MTI", classes, 0);
        }
}

//---------------------VKeyPress----------------------John Mertus---Jan 2001---

   void __fastcall VTimeCodeEdit::VKeyPress(TObject *Sender, wchar_t &Key)

//  Allow only certain keys
//
//******************************************************************************

{
#define Ctrl_A ('A' - 'A' + 1)
#define Ctrl_C ('C' - 'A' + 1)
#define Ctrl_V ('V' - 'A' + 1)
#define Ctrl_X ('X' - 'A' + 1)
#define Ctrl_Z ('Z' - 'A' + 1)

   // All these keys are processed normally
  if (isdigit(Key))  return;
  if (Key == ':')    return;
  if (Key == '\b')   return;
  if (Key == ' ')    return;
  if (Key == '+')    return;
  if (Key == '-')    return;
  if (Key == '.')    return;
  if (Key == Ctrl_A) return;
  if (Key == Ctrl_C) return;
  if (Key == Ctrl_V) return;
  if (Key == Ctrl_X) return;
  if (Key == Ctrl_Z) return;

  // Process Escape
  if (Key == 0x1B)
    {
      Key = 0;
      UpdateDisplay();
      if (_OnEscapeKey != NULL)
       {
         _OnEscapeKey(Sender);
          SelectAll();
       }
      return;
    }

  // Return processes the data
  if (Key == '\r')
    {
      Key = 0;
      tcP.InhibitCallbacks();               // Force an change call
      VExit(Sender);                        // Parse it
      tcP.Notify();
      tcP.UnInhibitCallbacks();            // But make it only one
      if (_OnEnterKey != NULL)
       {
         _OnEnterKey(Sender);
          SelectAll();
       }
      return;
    }

  Key = 0;
  Beep();
}

//---------------------VFocusExit---------------------John Mertus---Jan 2004---

    void __fastcall VTimeCodeEdit::VFocusExit(TObject *Sender)

//  Called when the focus is lost on the control
//
//******************************************************************************
{
    VExit(Sender);
    if (_OnExit != NULL) _OnExit(Sender);
}

//---------------------VExit-------------------------John Mertus---Jan 2001---

	void __fastcall VTimeCodeEdit::VExit(TObject *Sender)

//  Called when the focus is lost on the component
//
//******************************************************************************
{
//	if (!tcP.ConformTimeASCII(WCharToStdString(Text.c_str())))
//	  _MTIErrorDialog(Handle, theError.getFmtError());
    tcP.ConformTimeASCII(WCharToStdString(Text.c_str()));

    UpdateDisplay();
}

//------------------SetAllowVTRCopy--------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::SetAllowVTRCopy(const bool Value)

//  This setter turns of the internal hint automatically
//
//****************************************************************************
{
   if (Value == _AllowVTRCopy) return;
   _AllowVTRCopy = Value;
   CreatePopUp();
}

//-------------------SetTheHint-------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::SetTheHint(const String Value)

//  This setter turns of the internal hint automatically
//
//****************************************************************************
{
   TCustomEdit::Hint = Value;
   _InternalHint = false;
}

//-------------------GetTheHint-------------------John Mertus----Oct 2003-----

     String __fastcall VTimeCodeEdit::GetTheHint(void)

//  Usual getter since properties cannot be nested inside properties
//
//****************************************************************************
{
   return TCustomEdit::Hint;
}

//--------------CreateHint-------------------------John Mertus---Jan 2001---

   void VTimeCodeEdit::CreateHint(void)
//
//  This creates the hint for the system
//
//******************************************************************************
{
   // Don't create the hint if external
   if (!_InternalHint) return;

   // Don't create it if designing
   if (ComponentState.Contains(csDesigning)) return;

   String str = (String)tcP.getFramesPerSecond() + " FPS, ";
   if (tcP.isDropFrame()) str = str + "Drop Frame"; else str = str + "Non-Drop Frame";
   if (tcP.isColorFrame()) str = str + ", Color Frame";
   if (tcP.isHalfFrame()) str = str + ", Half Frame";
   if (tcP.is720P()) str = str + ", 720P";
   str = str + ", Frames: " + (String)tcP.Frame;

   if (VTRList.NumberOfVTRs() > 0)
     {
		int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
        VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
        if (VTRInfo != NULL)
          str = str + "\r" + VTRInfo->Name;
        VTRList.ReleaseVTR(nV);
     }

   TCustomEdit::Hint = str;
}

//------------------WmHint----------------------------John Mertus---Jan 2001---

      void __fastcall VTimeCodeEdit::WmHint(TMessage &Message)

//  This is called when a hint is wanted, generate it
//
//******************************************************************************

{
//   String s = Hint;
   CreateHint();
//   String s2 = Hint;
//   if (Hint != s)
//     {
//	   ///	Application->CancelHint();
//     }
}


//--------------UpdateDisplay-------------------------John Mertus---Jan 2001---

   void VTimeCodeEdit::UpdateDisplay(void)
//
//  This just takes the data member and displays it
//
//******************************************************************************
{
   Text = (tcP.getUserPreferredStyleString()).c_str();
}

//-------------------TCChanged--------------------John Mertus----Oct 2003-----

  void VTimeCodeEdit::TCChanged(void *p)

//  Come here when timecode has changed, update the display
//  and the MRU list.
//
//****************************************************************************
{
   // Need to always re-display because the timecode STYLE might have changed!
   UpdateDisplay();

   if (tcP == _oldTimecode)
   {
      return;
   }

   _oldTimecode = tcP;

   if (_OnTimecodeChange != NULL)
   {
      _OnTimecodeChange(this);
   }
}

//------------------ReadSettings------------------John Mertus----Oct 2003-----

  bool VTimeCodeEdit::ReadSettings(CIniFile *ini, const string &section)

//  Usual read setting property
//
//****************************************************************************
{
   return true;
}

//-----------------WriteSettings------------------John Mertus----Oct 2003-----

  bool VTimeCodeEdit::WriteSettings(CIniFile *ini, const string &section)

//  Usual write setting property
//
//****************************************************************************
{
   return true;
}

//----------------CopyFromVTR--------------------John Mertus-----Sep 2003---

   bool VTimeCodeEdit::CopyFromVTR(int vtr)

//  Call this to copy a timecode from a VTR to the component
//   vtr is the number of the vtr, only DEFAULT_VTR for now
//   Return is TRUE if it copies
//   false if no VTR available
//
//***************************************************************************
{
     // See if any VTRs exist
     if (VTRList.NumberOfVTRs() == 0) return false;

	 int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
     VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
     if (VTRInfo == NULL) return false;

     PTimecode ptc(VTRInfo->tcVTR);
     VTRList.ReleaseVTR(nV);

     tcP.setTime(ptc);
     return true;
}

//-----------------CueToVTR-----------------------John Mertus-----Sep 2003---

   bool VTimeCodeEdit::CueToVTR(int vtr)

//  Call this to copy a timecode code to the VTR
//   vtr is the number of the vtr, only DEFAULT_VTR for now
//   Return is TRUE if it copies
//   false if no VTR available
//
//***************************************************************************
{
     // See if any VTRs exist
     if (VTRList.NumberOfVTRs() == 0) return false;

	 int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
     VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
     if (VTRInfo == NULL) return false;
     VTRSharedMemory_Cue(VTRInfo, this->tcP);
     VTRList.ReleaseVTR(nV);

     return true;
}


//----------------PopupCopyFromVTR----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupCopyFromVTR(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   CopyFromVTR();
}

//-----------------PopupCopyToVTR-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupCopyToVTR(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   CueToVTR();
}

//-------------------PopupCopy--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupCopy(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_COPY, 0, (NativeInt)0);
}
//-------------------PopupPaste-------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupPaste(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_PASTE, 0, (NativeInt)0);
}
//--------------------PopupCut--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupCut(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_CUT, 0, (NativeInt)0);
}

//-------------------PopupUndo--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupUndo(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
//    COMBOBOXINFO cbi;
//    cbi.cbSize = sizeof(cbi);
//    GetComboBoxInfo(Handle, &cbi);
//    SendMessage(cbi.hwndItem, WM_UNDO, 0, 0);

    HWND hWndChild = GetWindow(Handle, GW_CHILD);
    SendMessage(hWndChild, WM_UNDO, 0, 0);
}

//-----------------PopupSelectAll-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupSelectAll(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   SelectAll();
}

//----------------PopupVTRChosen---------------John Mertus----Oct 2003-----

   void __fastcall  VTimeCodeEdit::PopupVTRChosen(TObject *Sender)

//  This sets the default vtr to the sender
//
//****************************************************************************
{
    TMenuItem *mi = (TMenuItem *)Sender;
    VTRList.DefaultIndex(mi->Tag);
}

//----------------CreateSelectPopup---------------John Mertus----Oct 2003-----

     void VTimeCodeEdit::CreateSelectPopup(void)

//  Builds the menu for the VTR choices
//
//****************************************************************************
{
  if (VTRList.DefaultIndex() < 0)
    {
      // This forces a default vtr to exist
      VTRList.SelectVTR();
      VTRList.ReleaseVTR();

    }
  // Build the vtrs
  if (SelectVTRMenuItem->Enabled)
   {
      // Clear all the tags
      SelectVTRMenuItem->Clear();

      // Loop through all the possible names
      for (int i=0; i < VTRList.NameIndexList()->Max; i++)
         {
           // See if there is a VTR out there
           VTRSharedMemory *vsm = VTRList.SelectVTR(i);
           if (vsm != NULL)
             {
               TMenuItem *mi = new TMenuItem(_PopupMenu);
               mi->Caption = vsm->Name;
               mi->OnClick = PopupVTRChosen;
               // Create the Name
               mi->Tag = i;
               mi->Checked = (i == VTRList.DefaultIndex());
               SelectVTRMenuItem->Add(mi);
               VTRList.ReleaseVTR(i);
             }
         }
    }

}

//-------------CopyFromVTRAvailable---------------John Mertus----Oct 2003-----

     bool VTimeCodeEdit::CopyFromVTRAvailable(void)

//  Just return if the VTimecode paste is available
//
//****************************************************************************
{
    bool Result = (VTRList.NumberOfVTRs() > 0);
    if (AttachToName != "")
	  Result =  Result && (VTRList.IndexByAttachToID(StringToStdString(AttachToName)) >= 0);

    return Result;
}

//----------------CueFromVTRAvailable-------------John Mertus----Oct 2003-----

     bool VTimeCodeEdit::CueFromVTRAvailable(void)

//  Just return if the VTimecode cue function is available
//
//****************************************************************************
{
    bool Result = (VTRList.NumberOfVTRs() > 0);
    if (AttachToName != "")
      Result =  Result && (VTRList.IndexByAttachToID(StringToStdString(AttachToName)) >= 0);

    return Result;
}


//-------------------PopupClick-------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupClick(TObject *Sender)

//  Enable/disable the different parts of the menu system
//
//****************************************************************************
{
   VExit(NULL);
   UndoMenuItem->Enabled = (!ReadOnly);
   CutMenuItem->Enabled = (SelLength > 0) && (!ReadOnly);
   CopyMenuItem->Enabled = (SelLength > 0);
   PasteMenuItem->Enabled = Clipboard()->HasFormat(CF_TEXT) && (!ReadOnly);

   // These don't exist if we can't copy
   if (_AllowVTRCopy)
     {
       // See if this VTimecode is restricted to one VTR
       CopyToVTRMenuItem->Enabled = CopyFromVTRAvailable();
       SelectVTRMenuItem->Enabled = CopyToVTRMenuItem->Enabled && (AttachToName == "");
       CopyFromVTRMenuItem->Enabled = CopyToVTRMenuItem->Enabled && (!ReadOnly);
       if (SelectVTRMenuItem->Enabled) CreateSelectPopup();

     }

   SelectAllMenuItem->Enabled = (SelLength != Text.Length());
   SetFocus();

}

//----------------PopupProperties-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeEdit::PopupProperties(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
    ////_MTIInformationDialog(Handle, tcP.DumpString());
}

//------------------CreatePopUp-------------------John Mertus----Oct 2003-----

  void VTimeCodeEdit::CreatePopUp(void)

//  This creates the popup menu, only needs to be done once
//
//****************************************************************************
{
   // Create the popup menu
   delete _PopupMenu;

   _PopupMenu = new TPopupMenu(this);
   _PopupMenu->OnPopup = PopupClick;

   UndoMenuItem = new TMenuItem(_PopupMenu);
   UndoMenuItem->Caption = "Undo";
   UndoMenuItem->ShortCut = ShortCut(Word('Z'), TShiftState() << ssCtrl);
   UndoMenuItem->OnClick = PopupUndo;
   _PopupMenu->Items->Add(UndoMenuItem);

   TMenuItem *mi = new TMenuItem(_PopupMenu);
   mi->Caption = "-";
   _PopupMenu->Items->Add(mi);

   CutMenuItem = new TMenuItem(_PopupMenu);
   CutMenuItem->Caption = "Cut";
   CutMenuItem->ShortCut = ShortCut(Word('X'), TShiftState() << ssCtrl);
   CutMenuItem->OnClick = PopupCut;
   _PopupMenu->Items->Add(CutMenuItem);

   CopyMenuItem = new TMenuItem(_PopupMenu);
   CopyMenuItem->Caption = "Copy";
   CopyMenuItem->ShortCut = ShortCut(Word('C'), TShiftState() << ssCtrl);
   CopyMenuItem->OnClick = PopupCopy;
   _PopupMenu->Items->Add(CopyMenuItem);

   PasteMenuItem = new TMenuItem(_PopupMenu);
   PasteMenuItem->Caption = "Paste";
   PasteMenuItem->ShortCut = ShortCut(Word('V'), TShiftState() << ssCtrl);
   PasteMenuItem->OnClick = PopupPaste;
   _PopupMenu->Items->Add(PasteMenuItem);

   mi = new TMenuItem(_PopupMenu);
   mi->Caption = "-";
   _PopupMenu->Items->Add(mi);

   SelectAllMenuItem = new TMenuItem(_PopupMenu);
   SelectAllMenuItem->Caption = "Select All";
   SelectAllMenuItem->ShortCut = ShortCut(Word('A'), TShiftState() << ssCtrl);
   SelectAllMenuItem->OnClick = PopupSelectAll;
   _PopupMenu->Items->Add(SelectAllMenuItem);

   if (_AllowVTRCopy)
     {

       mi = new TMenuItem(_PopupMenu);
       mi->Caption = "-";
       _PopupMenu->Items->Add(mi);

       CopyFromVTRMenuItem = new TMenuItem(_PopupMenu);
       CopyFromVTRMenuItem->Caption = "Paste from VTR";\
       CopyFromVTRMenuItem->OnClick = PopupCopyFromVTR;
       CopyFromVTRMenuItem->ShortCut = ShortCut(Word('V'), TShiftState() << ssCtrl << ssShift);
       _PopupMenu->Items->Add(CopyFromVTRMenuItem);

       CopyToVTRMenuItem = new TMenuItem(_PopupMenu);
       CopyToVTRMenuItem->Caption = "Cue VTR";
       CopyToVTRMenuItem->OnClick = PopupCopyToVTR;
       CopyToVTRMenuItem->ShortCut = ShortCut(Word('Q'), TShiftState() << ssCtrl << ssShift);
       _PopupMenu->Items->Add(CopyToVTRMenuItem);

       SelectVTRMenuItem = new TMenuItem(_PopupMenu);
       SelectVTRMenuItem->Caption = "Select VTR";
       _PopupMenu->Items->Add(SelectVTRMenuItem);

     }

//   mi = new TMenuItem(_PopupMenu);
//   mi->Caption = "-";
//   _PopupMenu->Items->Add(mi);
//
//   PropertiesMenuItem = new TMenuItem(_PopupMenu);
//   PropertiesMenuItem->Caption = "Properties...";
//   PropertiesMenuItem->OnClick = PopupProperties;
//   _PopupMenu->Items->Add(PropertiesMenuItem);

   PopupMenu = _PopupMenu;
}

//---------------------VEnter-----------------------John Mertus---Jan 2001---

	void __fastcall VTimeCodeEdit::VEnter(TObject *Sender)

//  Called when the focus is lost on the component
//
//******************************************************************************
{
    PostMessage(Handle, EM_SETSEL, 0, -1);
    if (_OnEnter != NULL) _OnEnter(Sender);
}

//---------------------DoEnter-----------------------John Mertus---Apr 2013---

	void __fastcall VTimeCodeEdit::DoEnter(void)

//  Called when enter with focus.
//  Required because 64 bits doesn't allow OnEnter to be set.
//
//*****************************************************************************
{
		this->VEnter(this);
}

//---------------------DoExit------------------------John Mertus---Apr 2013---

	void __fastcall VTimeCodeEdit::DoExit(void)

//  Called when focus is lost
//  Required because 64 bits doesn't allow OnExit to be set.
//
//*****************************************************************************
{
		this->VFocusExit(this);
}
//----------------------------------------------------------------------------

// Awful hack to let us recives notifications from global CBHook in CTimecode
#include "SharedMapForComponents.h"
void VTimeCodeEdit::SetSharedMapMap(void *map)
{
   SharedMapForComponents::setMap(map);
}
//----------------------------------------------------------------------------

