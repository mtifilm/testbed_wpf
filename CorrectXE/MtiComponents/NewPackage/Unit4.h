//---------------------------------------------------------------------------

#ifndef Unit4H
#define Unit4H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MTIBusy.h"
#include <Vcl.Grids.hpp>
#include "Knob.h"
#include "VDoubleEdit.h"
#include "VTimeCodeEdit.h"
#include "VTimeCodeHistory.h"
#include "GrayComboBox.h"
#include "MTIDBMeter.h"
#include "MTIProgressBar.h"
#include "PopupComboBox.h"
#include <Vcl.ExtCtrls.hpp>
#include "IconListBox.h"
//---------------------------------------------------------------------------
class TForm4 : public TForm
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TForm4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm4 *Form4;
//---------------------------------------------------------------------------
#endif
