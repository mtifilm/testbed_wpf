//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <sstream>
#include <System.IOUtils.hpp>

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VDoubleEdit"
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
	String fileName = TPath::GetFileName(Application->ExeName);
	this->Caption = fileName;

   #ifdef _DEBUG
	 this->Caption = this->Caption + ": Debug ";
   #else
   	 this->Caption = this->Caption + ": Release ";
   #endif

   if (sizeof(int *) == 8)
   {
	  this->Caption += " 64 Bits";
   }
   else
   {
	  this->Caption = this->Caption + " 32 Bits";
   }

   	MEMORYSTATUSEX status;
	status.dwLength = sizeof(status);
	GlobalMemoryStatusEx(&status);
	std::ostringstream os;
	os << " " << std::min(status.ullAvailVirtual, status.ullAvailPhys) /1024.0/1024.0/1024.0 << " GB physical available";
	AnsiString tmpStr = AnsiString(os.str().c_str());

   this->Caption = this->Caption + tmpStr;;
}
//---------------------------------------------------------------------------
