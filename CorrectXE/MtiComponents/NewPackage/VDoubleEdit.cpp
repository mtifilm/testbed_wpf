//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "VDoubleEdit.h"
#pragma resource "*.res"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(VDoubleEdit *)
{
        new VDoubleEdit(NULL);
}
//---------------------------------------------------------------------------
__fastcall VDoubleEdit::VDoubleEdit(TComponent* Owner) : TCustomEdit(Owner)
{
   // Set the defaults
   AutoSelect = true;
   qDecimals = 2;
   jdValue = 0;

   // What happens on change
//   PROPERTY_INIT(dValue, GetdValue, SetdValue);
//   SET_CBHOOK(ValueChangeCB, dValue);

   // Set the callbacks
   OnKeyPress = VKeyPress;
   OnExit = VExit;

   UpdateDisplay();
}

//---------------------------------------------------------------------------

void __fastcall VDoubleEdit::VKeyPress(TObject *Sender, WideChar &Key)
{
  //
  //  Pass all number keys
  //
  if (isdigit(Key)) return;
  //
  // We must pass - and .
  //
  if ((Key == '-') || (Key == '.')) return;

  if (Key == '\r')
    {
      Key = 0;
      VExit(Sender);
      return;
    }
  //
  // Note: we must pass editing keys as well as numbers
  if (Key == '\b') return;

  // Reject all other keys
  Beep();
  Key = 0;
  return;
}

//---------------------------------------------------------------------------

void __fastcall VDoubleEdit::VExit(TObject *Sender)
{
    if (Text == OldText) return;
    double f = StrToFloat(Text);
    dValue = f;
   if (jDoubleChange != NULL) jDoubleChange(this);
}

//---------------------------------------------------------------------------
   void __fastcall VDoubleEdit::SetdValue(double Value)
{
   if (jdValue == Value) return;
   jdValue = Value;
   UpdateDisplay();
}

//---------------------------------------------------------------------------
   double __fastcall VDoubleEdit::GetdValue(void)
{
   return jdValue;
}
//---------------------------------------------------------------------------
   void __fastcall VDoubleEdit::SetDecimals(int Value)
{
   if (qDecimals == Value) return;
   qDecimals = Value;
   UpdateDisplay();
}

//---------------------------------------------------------------------------

//   void VDoubleEdit::ValueChangeCB(void *p)
//{
//   UpdateDisplay();
//   if (jDoubleChange != NULL) jDoubleChange(this);
//}

//--------------UpdateDisplay-------------------------John Mertus---Jan 2001---

   void VDoubleEdit::UpdateDisplay(void)
//
//  This just takes the data memeber and displays it
//
//******************************************************************************
{
  // Dumb way to convert to a string
    AnsiString fmt = "%0." + IntToStr(qDecimals) + "f";
    Text = Format(fmt,ARRAYOFCONST((jdValue)));
    OldText = Text;
}

//---------------------------------------------------------------------------
namespace Vdoubleedit
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(VDoubleEdit)};
                 RegisterComponents("MTI", classes, 0);
        }
}

//---------------------------------------------------------------------------
