//---------------------------------------------------------------------------

#ifndef MTIBusyH
#define MTIBusyH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ComCtrls.hpp>

enum BusyArrowType {batSmall, batLarge};

//---------------------------------------------------------------------------
class PACKAGE TMTIBusy : public TCustomControl
{
private:
    void Load(void);

    // Unknown why this fails in 32 bit mode
#ifdef _WIN64
    TAnimate *Animate;
#endif
    BusyArrowType _ArrowType;

    void __fastcall SetArrowType(const BusyArrowType Value);

    void __fastcall SetBusy(const bool Value);
    bool __fastcall GetBusy(void);

protected:
    virtual void __fastcall Paint(void);
public:
     __fastcall TMTIBusy(TComponent* Owner);

    __published:
     __property bool Busy = {read=GetBusy, write=SetBusy, default=false};
     __property BusyArrowType ArrowType = {read=_ArrowType, write=SetArrowType, default=batLarge};
     __property Anchors;

};
//---------------------------------------------------------------------------
#endif
