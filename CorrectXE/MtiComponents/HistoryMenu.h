//---------------------------------------------------------------------------

#ifndef HistoryMenuH
#define HistoryMenuH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include "HistoryList.h"
#include "PropX.h"
typedef void __fastcall (__closure *THistoryClickEvent)(String Str);
//---------------------------------------------------------------------------
class PACKAGE THistoryMenu : public TMainMenu
{
private:
   int  fiBaseIndex;            // Index of last menu item
   int  fiPosition;             // Which main menu item to addto
   int  fiFromBottom;           // Index of menu from bottom
   int  fiActualFromBottom;     // Actual from bottom (fudge if menu is too small)
   int  fiNumberAdded;          // Number of files added

   bool fbMenuValid;            // True if menu has been rebuild
   void __fastcall LoadFile(TObject *Sender);
   void AddItem(int i, String Name);
   DEFINE_CBHOOK(PCBHistoryChanged, THistoryMenu);
   THistoryClickEvent fOnHistoryClick;

public:
   __fastcall THistoryMenu(TComponent* Owner);
   __fastcall ~THistoryMenu(void);
   void RebuildMenu(void);
   void Add(const string &str);
   CHistoryList   HistoryList;
   TPopupMenu     *OpenPopup;

__published:
   __property THistoryClickEvent OnHistoryClick = {read=fOnHistoryClick, write=fOnHistoryClick};
   __property int FromBottom = {read=fiFromBottom, write=fiFromBottom};
};
//---------------------------------------------------------------------------
#endif
