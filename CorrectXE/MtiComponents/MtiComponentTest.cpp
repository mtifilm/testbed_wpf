//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MtiComponentTest.h"
#include "BasicHRTimer.h"
#include <sstream>
#include <System.IOUtils.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Knob"
#pragma link "VDoubleEdit"
#pragma link "VTimeCodeEdit"
#pragma link "MTIBusy"
#pragma link "ColorPanel"
#pragma link "ColorLabel"
#pragma link "ColorSplitter"
//#pragma link "cspin"
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
	String fileName = TPath::GetFileName(Application->ExeName);
	this->Caption = fileName;

   #ifdef _DEBUG
	 this->Caption = this->Caption + ": Debug ";
   #else
   	 this->Caption = this->Caption + ": Release ";
   #endif

   if (sizeof(int *) == 8)
   {
	  this->Caption += " 64 Bits";
   }
   else
   {
	  this->Caption = this->Caption + " 32 Bits";
   }

   	MEMORYSTATUSEX status;
	status.dwLength = sizeof(status);
	GlobalMemoryStatusEx(&status);
	std::ostringstream os;
	os << " " << std::min(status.ullAvailVirtual, status.ullAvailPhys) /1024.0/1024.0/1024.0 << " GB physical available";
	AnsiString tmpStr = AnsiString(os.str().c_str());

   this->Caption = this->Caption + tmpStr;

   // Setup gray combo box
   this->GrayComboBox1->ItemEnabled[1] = false;
   this->GrayComboBox1->ItemEnabled[2] = false;

   	// Icon list box
	for (int i = 0; i < 8; i++)
	{
		ObjectData *objectData = new ObjectData();
		objectData->Bitmap = new TBitmap();
		ImageList1->GetBitmap(i, objectData->Bitmap);
		this->IconListBox1->Items->Objects[i] = objectData;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Knob1Change(TObject *Sender)
{
	this->VTimeCodeEdit1->tcP.Frame =  this->Knob1->Position;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::VDoubleEdit1DoubleChange(TObject *Sender)
{
	this->Knob1->Position  = (int)this->VDoubleEdit1->dValue;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::StartBusyButtonClick(TObject *Sender)
{
	this->MTIBusy1->Busy = !this->MTIBusy1->Busy;
	this->Timer1->Enabled = this->MTIBusy1->Busy;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Exit1Click(TObject *Sender)
{
   this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GrayComboBox1Change(TObject *Sender)
{
   switch (this->GrayComboBox1->ItemIndex)
   {
   case 0:
	   this->MTIDBMeter1->LedType = ltLedFlatRect;
		break;

   case 3:
	   this->MTIDBMeter1->LedType = ltLed3DRound;
	   break;

   case 4:
	   this->MTIDBMeter1->LedType = ltLed3DLargeRound;
	   break;

   case 5:
	   this->MTIDBMeter1->LedType = ltLed3DRect;
	   break;

   default:
	   throw "Illegal value";
   }
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Timer1Timer(TObject *Sender)
{
	  double r = ((double) rand() / (RAND_MAX));
	  this->MTIDBMeter1->Position = r * this->MTIDBMeter1->Max;
	  this->MTIProgressBar1->Position = this->MTIProgressBar1->Position + 2;
	  if (this->MTIProgressBar1->Position >= 100)
	  {
         this->MTIProgressBar1->Position = 0;
	  }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


void __fastcall TForm3::BitBtn1Click(TObject *Sender)
{
  CHRTimer HRT;
  Sleep(100);
  string s = HRT.ReadAsString();
  VideoStorageLabel->Caption = String(s.c_str());
}

//---------------------------------------------------------------------------

void __fastcall TForm3::BitBtn2Click(TObject *Sender)
{
   this->BitBtn2->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::TextColorComboBoxChange(TObject *Sender)
{
	 switch (TextColorComboBox->ItemIndex)
	  {
	case 0:
		 ColorPanel1->Font->Color = clYellow;
		 break;

	case 1:
		 ColorPanel1->Font->Color = clBlack;
		 break;

	case 2:
		 ColorPanel1->Font->Color = clGreen;
		 break;

	case 3:
		 ColorPanel1->Font->Color = clBlue;
		 break;
	 default:
		break;
	 }

	VideoStorageLabel->Font->Color = ColorPanel1->Font->Color;

}
//---------------------------------------------------------------------------

void BadFunction()
{
   vector<int> t(10);
   auto a = t.at(33);
}
void __fastcall TForm3::Button1Click(TObject *Sender)
{
    try
    {
       BadFunction();
    }
    catch (exception &ex)
    {
       auto s = String(ex.what());
       ShowMessage(s);
    }
}
//---------------------------------------------------------------------------

