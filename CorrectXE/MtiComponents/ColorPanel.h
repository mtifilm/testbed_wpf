//---------------------------------------------------------------------------

#ifndef ColorPanelH
#define ColorPanelH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TColorPanel : public TPanel
{
private:
	Classes::TNotifyEvent fOnPaint;

protected:
	virtual void __fastcall Paint(void);

public:
	__fastcall TColorPanel(TComponent* Owner);
__published:

	__property Classes::TNotifyEvent OnChange = {read=fOnPaint, write=fOnPaint};
};
//---------------------------------------------------------------------------
#endif
