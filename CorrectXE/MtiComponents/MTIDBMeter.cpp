//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MTIDBMeter.h"
#pragma resource "*.res"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TMTIDBMeter *)
{
        new TMTIDBMeter(NULL);
}

//------------------TMTIDBMeter-------------------John Mertus----Sep 2003-----

  __fastcall TMTIDBMeter::TMTIDBMeter(TComponent* Owner)
        : TCustomControl(Owner)

//  Usual Constructor
//
//****************************************************************************
{

  Width = 200;
  Height = 20;

  Color = clBtnFace;
  _LedSize = 4;
  _NewLedSize = _LedSize;
  _NewWidth = Width;
  _LedSpacing = 2;
  fBorderColor1 = clBtnShadow;
  fBorderColor2 = clBtnHighlight;
  fBackgroundColor = Color;
  _ColorInBounds = clGreen;
  _ColorWarning = clYellow;
  _ColorClip = (TColor)RGB(255,0,0);
  _WarningLevel = 80;
  _Position = 0;
  _Min = 0;
  _Max = 100;
  _LastMax = 0;
  _Clipped = false;
  _ShowBorder = true;
  _ShowClipLed = true;
  _ShowPeak = true;
  _ClipTimer = new TTimer(this);
  _PeakTimer = new TTimer(this);
  _ClipTimer->Interval = 5000;
  _PeakTimer->Interval = 1000;
  _ClipTimer->OnTimer = ClipTimeCallback;
  _PeakTimer->OnTimer = PeakTimeCallback;
  _ClipTimer->Enabled = false;
  _PeakTimer->Enabled = false;
  _ShowUnlitLeds = false;

  _Direction = pdLeftToRight;
  _LedType = ltLedFlatRect;

  fShowPosText = true;
  fPosTextSuffix = "%";
  fPosTextPrefix = "";
  RegenerateBitmap = true;

  MainBitmap = new Graphics::TBitmap();
  TiledBarBitmap = new Graphics::TBitmap();
  ClipBitmap = new Graphics::TBitmap();
  ClipBitmapUnlit = new Graphics::TBitmap();
  WarningBitmap = new Graphics::TBitmap();
  WarningBitmapUnlit = new Graphics::TBitmap();
  InBoundsBitmap = new Graphics::TBitmap();
  InBoundsBitmapUnlit = new Graphics::TBitmap();
  BlankBitmap = new Graphics::TBitmap();

  ControlStyle << csOpaque;
  _InvalidateLeds = true;
  }



//------------------TMTIDBMeter-------------------John Mertus----Sep 2003-----

  __fastcall TMTIDBMeter::~TMTIDBMeter(void)

//  Usual destructor, destory any bitmaps
//
//****************************************************************************
{
  delete MainBitmap;
  delete TiledBarBitmap;
  delete ClipBitmap;
  delete ClipBitmapUnlit;
  delete WarningBitmap;
  delete WarningBitmapUnlit;
  delete InBoundsBitmap;
  delete InBoundsBitmapUnlit;
  delete BlankBitmap;
}

//---------------------Paint----------------------John Mertus----Sep 2003-----

  void __fastcall TMTIDBMeter::Paint(void)

//   This is called whenever Windows repaints.
//  If _InvalidateLeds is set, Rebuild all the LEDS
// Otherwise just create the main bitmap from the Text, Boarder and LEDS
// and Draw Mainbitmap to control
//
//****************************************************************************
{
  TCustomControl::Paint();

  if (_InvalidateLeds) Rebuild();

  if (!ComponentState.Contains(csReading))
    {
      PaintBar(RegenerateBitmap);
    }

  if (ShowBorder) PaintBorder();
  if (fShowPosText) PaintPosText();

  Canvas->Draw(0, 0, MainBitmap);
}

//--------------------PaintBar--------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::PaintBar(bool RegenerateBitmap)

//  This Redraws all the LEDS to the MainBitmap
//
//****************************************************************************
{
  if (RegenerateBitmap)
    {
      DrawLeds();
    }

  MainBitmap->Canvas->Draw(_TiledArea.left, _TiledArea.top, TiledBarBitmap);
}

//------------------PixelFromPos------------------John Mertus----Sep 2003-----

   int  TMTIDBMeter::PixelFromPos(double Pos)

//  Given a Position value, find the pixel in the LED list  
//
//****************************************************************************
{
    if (_Max == _Min) return 0;
	return ((Pos-_Min) / std::fabs(_Max-_Min)) * LogicalSize() + 0.5;
}

//------------------PosFromPixel------------------John Mertus----Sep 2003-----

      double TMTIDBMeter::PosFromPixel(int Pixel)

//  Given a pixel, find the value from the LEDS
//
//****************************************************************************
{
	return (Pixel*std::fabs(_Max-_Min)/LogicalSize()) + _Min;
}

//------------------PaintPosText------------------John Mertus----Sep 2003-----

  	void __fastcall TMTIDBMeter::PaintPosText(void)

//  Draw the value of the Position on the mainbitmap
//
//****************************************************************************
{
  String Text;
  int TextPosX, TextPosY;

  Text = fPosTextPrefix + IntToStr((int)_Position) + fPosTextSuffix;
  TextPosX = (Width/2) - (MainBitmap->Canvas->TextWidth(Text)/2);
  TextPosY = (Height/2) - (MainBitmap->Canvas->TextHeight(Text)/2);

  MainBitmap->Canvas->Brush->Style = bsClear;
  MainBitmap->Canvas->TextOut(TextPosX, TextPosY, Text);
}

//------------------PaintBorder-------------------John Mertus----Sep 2003-----

  	void __fastcall TMTIDBMeter::PaintBorder(void)

//  Create a border if so desired.
//
//****************************************************************************
{
    int W = _TiledArea.Width()+1;
    int H = _TiledArea.Height()+1;

    MainBitmap->Canvas->Pen->Color = fBorderColor1;
    MainBitmap->Canvas->MoveTo(W, 0);
    MainBitmap->Canvas->LineTo(0, 0);
    MainBitmap->Canvas->LineTo(0, H);
    MainBitmap->Canvas->Pen->Color = fBorderColor2;
    MainBitmap->Canvas->MoveTo(1, H);
    MainBitmap->Canvas->LineTo(W, H);
    MainBitmap->Canvas->LineTo(W, 0);
}

//---------------SetBackgroundColor---------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetBackgroundColor(const Graphics::TColor Value)

//  Changes the background color
//
//****************************************************************************
{
  fBackgroundColor = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//----------------SetBorderColor1-----------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetBorderColor1(const Graphics::TColor Value)

// Changes the Boarder color of the upper left
//
//****************************************************************************
{
  fBorderColor1 = Value;
  Paint();
}

//----------------SetBorderColor2-----------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetBorderColor2(const Graphics::TColor Value)

//  Changes the boarder color of the upper right
// 
//****************************************************************************
{
  fBorderColor2 = Value;
  Paint();
}

//------------------SetPosition-------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetPosition(double Value)

//  Call this to change which LED to light
//
//****************************************************************************
{
  // Now set the peak position
  if (Value > _LastMax)
    {
       _LastMax = Value;
       _PeakTimer->Interval = _PeakTimer->Interval;
       _PeakTimer->Enabled = ShowPeak && (PeakTime > 0);
    }

  // If the position exceeds the maximum, set the clipping
  if (Value > _Max)
    {
       Clipped(true);
       Value = _Max;
    }
  else
    {
      _ClipTimer->Enabled = Clipped() && (ClipTime > 0);
      _ClipTimer->Interval = _ClipTimer->Interval;
    }

  if (Value < _Min) Value = _Min;

  if (_Position == Value) return;

  _Position = Value;
  Paint();
}

//---------------------SetMax---------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetMax(const double Value)

//  Set the Max value of the LED
//
//****************************************************************************
{
  _Max = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//---------------------SetMin---------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetMin(const double Value)

//  Set the Min value of the LEDs
//
//****************************************************************************
{
  _Min = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//-----------------SetShowBorder------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetShowBorder(const bool Value)

// Set if the board should be shown
// 
//****************************************************************************
{
  if (_ShowBorder == Value) return;
  _ShowBorder = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//----------------SetShowUnlitLeds----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetShowUnlitLeds(const bool Value)

//  Set if the Unlit LEDS should be displayed
//
//****************************************************************************
{
  if (_ShowUnlitLeds == Value) return;
  _ShowUnlitLeds = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//-----------------SetShowClipLed-----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetShowClipLed(bool Value)

//  Set if the clip LED should be shown
//
//****************************************************************************
{
  if (_ShowClipLed == Value) return;
  _ShowClipLed = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//------------------SetShowPeak-------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetShowPeak(bool Value)

//  Set if the Peak LED should be displayed
//
//****************************************************************************
{
  if (_ShowPeak == Value) return;
  _ShowPeak = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//-------------------IsVertical-------------------John Mertus----Sep 2003-----

    bool TMTIDBMeter::IsVertical(void)

//  Returns true if the meter is in a vertical position
//
//****************************************************************************
{
  return  (_Direction == pdTopToBottom) || (_Direction == pdBottomToTop);
}

//-----------------CreateFlatLed------------------John Mertus----Sep 2003-----

    bool TMTIDBMeter::CreateFlatLed(TColor cl, Graphics::TBitmap *bm)

//  Build a flat LED
//    cl is the color
//    bm the bitmap to store it
//    return is true if it succeeds, false otherwise
//
//****************************************************************************
{
  if (IsVertical())
    {
       bm->Width = _TiledArea.Width();
       bm->Height = LedSize;
    }
  else
    {
       bm->Height = _TiledArea.Height();
       bm->Width = LedSize;
    }

  bm->Canvas->Brush->Color = cl;
  bm->Canvas->FillRect(Rect(0,0, bm->Width, bm->Height));

  return true;
}

//----------------------Less----------------------John Mertus----Sep 2003-----

   TColor Less(const TColor cl)

//  Creates a dimmer color out of a color
//
//****************************************************************************
{
   double S = .5;
   return (TColor)RGB(S*GetRValue(cl), S*GetGValue(cl), S*GetBValue(cl));
}

//------------------BuildLedList------------------John Mertus----Sep 2003-----

    void TMTIDBMeter::BuildLedList(void)

//  Major internal program, this builds the LED list from the colors and
// sizes, very costly, so call rarely.
//
//****************************************************************************
{
  Leds.clear();

  //  See if we are going in different directions
  switch (LedType)
    {
      case ltLedFlatRect:
         CreateFlatLed(ColorClip, ClipBitmap);
         CreateFlatLed(Less(ColorClip), ClipBitmapUnlit);
         CreateFlatLed(ColorWarning, WarningBitmap);
         CreateFlatLed(Less(ColorWarning), WarningBitmapUnlit);
         CreateFlatLed(ColorInBounds, InBoundsBitmap);
         CreateFlatLed(Less(ColorInBounds), InBoundsBitmapUnlit);
        break;

      case ltLed3DRect:                  
         CreateLedFromResource(ColorClip, "RECTLED", ClipBitmap, !IsVertical());
         CreateLedFromResource(Less(ColorClip), "RECTLED", ClipBitmapUnlit, !IsVertical());
         CreateLedFromResource(ColorWarning, "RECTLED", WarningBitmap, !IsVertical());
         CreateLedFromResource(Less(ColorWarning), "RECTLED", WarningBitmapUnlit, !IsVertical());
         CreateLedFromResource(ColorInBounds, "RECTLED", InBoundsBitmap, !IsVertical());
         CreateLedFromResource(Less(ColorInBounds), "RECTLED", InBoundsBitmapUnlit, !IsVertical());
        break;

      case ltLed3DRound:
         CreateLedFromResource(ColorClip, "SMALLROUND", ClipBitmap);
         CreateLedFromResource(Less(ColorClip), "SMALLROUND", ClipBitmapUnlit);
         CreateLedFromResource(ColorWarning, "SMALLROUND", WarningBitmap);
         CreateLedFromResource(Less(ColorWarning), "SMALLROUND", WarningBitmapUnlit);
         CreateLedFromResource(ColorInBounds, "SMALLROUND", InBoundsBitmap);
         CreateLedFromResource(Less(ColorInBounds), "SMALLROUND", InBoundsBitmapUnlit);
        break;

      case ltLed3DLargeRound:
         CreateLedFromResource(ColorClip, "ROUNDLED", ClipBitmap);
         CreateLedFromResource(Less(ColorClip), "ROUNDLED", ClipBitmapUnlit);
         CreateLedFromResource(ColorWarning, "ROUNDLED", WarningBitmap);
         CreateLedFromResource(Less(ColorWarning), "ROUNDLED", WarningBitmapUnlit);
         CreateLedFromResource(ColorInBounds, "ROUNDLED", InBoundsBitmap);
         CreateLedFromResource(Less(ColorInBounds), "ROUNDLED", InBoundsBitmapUnlit);
        break;
    }

  BlankBitmap->Assign(ClipBitmap);
  BlankBitmap->Canvas->Brush->Color = BackgroundColor;
  BlankBitmap->Canvas->FillRect(TRect(0,0,BlankBitmap->Width, BlankBitmap->Height));
  
  for (int i = 0; i < NumberOfLeds(); i ++)
   {
       TLed *led;

       // Decide on the color
       int ledBottom = i*(_LedSize + _LedSpacing);
       TColor cl;
       double LedPos = PosFromPixel(ledBottom + _LedSize/2);
       if (LedPos >= _WarningLevel)
          led = new TLed(WarningBitmap, WarningBitmapUnlit, BlankBitmap);
       else
          led = new TLed(InBoundsBitmap, InBoundsBitmapUnlit, BlankBitmap);
       Leds.push_back(*led);
       delete led;
   }

  if (ShowClipLed)
    {
       TLed *led = new TLed(ClipBitmap, ClipBitmapUnlit, BlankBitmap);
       Leds.push_back(*led);
       delete led;
    }

  if (IsVertical())
   {
     _NewLedSize = Leds[0].Height();
     _NewWidth = Leds[0].Width();
   }
  else
   {
     _NewLedSize = Leds[0].Width();
     _NewWidth = Leds[0].Height();
   }
  _InvalidateLeds = false;
}

//-------------------SetLedType-------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetLedType(const TLedType Value)

//  Changes the LED type
//
//****************************************************************************
{
  if (_LedType == Value) return;

  _LedType = Value;
  Rebuild();
  LedSize = _NewLedSize;
}

//------------------SetDirection------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetDirection(const TProgressDirection Value)

//  Change the Meter Direction
//
//****************************************************************************
{
  if (_Direction == Value) return;

  //  See if we are going in different directions
  switch (Value)
    {
    case pdLeftToRight:
    case pdRightToLeft:
        if (Width < Height) SetBounds(Left, Top, Height, Width);
      break;

    case pdTopToBottom:
    case pdBottomToTop:
        if (Width > Height) SetBounds(Left, Top, Height, Width);
      break;
    }
  _Direction = Value;
  Rebuild();
  LedSize = _NewLedSize;
}

//----------------SetColorInBounds----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetColorInBounds(const Graphics::TColor Value)

//  Set the color of the LEDs that represent inbound
//
//****************************************************************************
{
  if (_ColorInBounds == Value) return;
  _ColorInBounds = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//----------------SetColorWarning-----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetColorWarning(const Graphics::TColor Value)

//  SEt the color of the LEDs that represents the warning
//
//****************************************************************************
{
  if (_ColorWarning == Value) return;
  _ColorWarning = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//------------------SetColorClip------------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetColorClip(const Graphics::TColor Value)

//  Set the color of clip LED
//
//****************************************************************************
{
  if (_ColorClip == Value) return;
  _ColorClip = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//-----------------SetShowPosText-----------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetShowPosText(const bool Value)

//  Usual set of Show Position Text
//
//****************************************************************************
{
  fShowPosText = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//----------------SetPosTextSuffix----------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetPosTextSuffix(const AnsiString Value)

//  Usual set of Text Suffix
//
//****************************************************************************
{
  fPosTextSuffix = Value;
  Paint();
}

//----------------SetPosTextPrefix----------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetPosTextPrefix(const AnsiString Value)

//   Usual set of Text position
//
//****************************************************************************
{
  if (fPosTextPrefix == Value) return;

  fPosTextPrefix = Value;
  Paint();
}

//-----------------CMFontChanged------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::CMFontChanged(Messages::TMessage &Message)

//  Processes font changes
//
//****************************************************************************
{
  _InvalidateLeds = true;
  Invalidate();
}

//---------------------WMSize---------------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::WMSize(Messages::TWMSize &Message)

//   Processes size changes
//
//****************************************************************************
{
  _InvalidateLeds = true;
  Invalidate();
}


//----------------SetWarningLevel-----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::SetWarningLevel(const double Value)

//  Usual set of the warning value
//
//****************************************************************************
{
  if (_WarningLevel == Value) return;
  _WarningLevel = Value;
  _InvalidateLeds = true;
  Invalidate();
}

//-------------------LedFromPos-------------------John Mertus----Sep 2003-----

     int TMTIDBMeter::LedFromPos(double Pos)

//  Returns the LED give its position
//
//****************************************************************************
{
   if (Pos > _Max) return NumberOfLeds();
   if (Pos == _Max) return NumberOfLeds()-1;
   if (Pos <= _Min) return -1;
   int L = (LogicalSize()*(Pos - _Min)/std::fabs(_Max-_Min) - LedSize/2)/LedFullSize() - .5;
   return L;
}



//--------------------DrawLeds--------------------John Mertus----Sep 2003-----

    void TMTIDBMeter::DrawLeds(void)

//   Draws all the LEDs
//
//****************************************************************************
{
   // if we draw an LED depends upon the direction
  for (int i = 0; i < NumberOfLeds(); i ++)
   {
       // Decide on the color
       int ledBottom = i*(_LedSize + _LedSpacing);
       TColor cl;
       double LedPos = PosFromPixel(ledBottom + _LedSize/2);
       if (LedPos <= _Position)
         {
           DrawLed(i, llsLit);
         }
       else
       // See if unlit LED's are wanted
        if (ShowUnlitLeds)
           DrawLed(i, llsUnlit);
        else
           DrawLed(i, llsBlank);
   }

  // Display the clip LED
  if (ShowClipLed)
  {
	if (Clipped())
	{
	  DrawLed(NumberOfLeds(), llsLit);
	}
	else
	{
	  if (ShowUnlitLeds)
	  {
		DrawLed(NumberOfLeds(), llsUnlit);
	  }
	  else
	  {
		DrawLed(NumberOfLeds(), llsBlank);
      }
	}
  }

  if (ShowPeak) DrawLed(LedFromPos(_LastMax), true);
}

//---------------------------------------------------------------------------
namespace Mtidbmeter
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TMTIDBMeter)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------


//--------------------DrawLed---------------------John Mertus----Sep 2003-----

    void TMTIDBMeter::DrawLed(int n, bool lit)

//  Draws LED at position n either on or off
//
//****************************************************************************
{
   if (lit) DrawLed(n, llsLit); else DrawLed(n, llsUnlit);
}

//--------------------DrawLed---------------------John Mertus----Sep 2003-----

  void TMTIDBMeter::DrawLed(int n, TLedState lit)

//  Draws LED at position n with state lit.  Notice this is optimized
//  so only if the LED has changes is it redrawn.
//
//****************************************************************************
{
    // Don't process leds out of range
    if (n < 0) return;

    int ledBottom = n*(_LedSize + _LedSpacing);
    TRect fRect;

   // This draws the n-th LED
     switch (_Direction)
       {
          case pdBottomToTop:
            fRect = Rect(0, PhysicalSize() - ledBottom - LedSize, Width, PhysicalSize() - ledBottom);
            break;

          case pdTopToBottom:
            fRect = Rect(0, ledBottom, Width, ledBottom+_LedSize);
            break;

          case pdLeftToRight:
            fRect = Rect(ledBottom, 0, ledBottom+_LedSize, Height);
            break;

          case pdRightToLeft:
            fRect = Rect(PhysicalSize() - ledBottom - LedSize, 0, PhysicalSize() - ledBottom, Height);
            break;
       }

   // Check in bounds
   if (n < (int)Leds.size())
    if (Leds[n].GetLed(lit) != NULL)
     {
       if (Leds[n].Lit() != lit)
         TiledBarBitmap->Canvas->Draw(fRect.left, fRect.top, Leds[n].GetLed(lit));
       Leds[n].Lit(lit);
     }
}

//-------------------SetLedSize-------------------John Mertus----Sep 2003-----

          void __fastcall TMTIDBMeter::SetLedSize(int Size)

//    Set the size of the LED, may not change the actual size
//
//****************************************************************************
{
  if (Size == _LedSize) return;
  _LedSize = Size;
  Rebuild();
  LedSize = _NewLedSize;
}

//-----------------SetLedSpacing------------------John Mertus----Sep 2003-----

          void __fastcall TMTIDBMeter::SetLedSpacing(int Spacing)

//  Sets the spacing of an LED
//
//****************************************************************************
{
  if (Spacing == _LedSpacing) return;
  _LedSpacing = Spacing;
  Height = Height;
  _InvalidateLeds = true;
  Invalidate();
}

//------------------LedFullSize-------------------John Mertus----Sep 2003-----

  int TMTIDBMeter::LedFullSize(void)

//  Returns the size of and LED and its spacing
//
//****************************************************************************
{
  return LedSize + LedSpacing;
}

//------------------PhysicalSize------------------John Mertus----Sep 2003-----

  int TMTIDBMeter::PhysicalSize(void)

//  Return the size of the display which includes the clip LED
//
//****************************************************************************
{
  int V;
  if (IsVertical()) V = _TiledArea.Height(); else V = _TiledArea.Width();
  if (LedFullSize() <= 0) return V;
  if (V < LedFullSize()) V = LedFullSize();
  return (int)((V - LedSize)/LedFullSize())*LedFullSize() +  LedSize;
}

//------------------LogicalSize-------------------John Mertus----Sep 2003-----

  int TMTIDBMeter::LogicalSize(void)

//  Returns the size of the LED bar without the clip LED;
//
//****************************************************************************
{
  int V;
  return NumberOfLeds()*LedFullSize() - LedSpacing;
}

//------------------NumberOfLeds------------------John Mertus----Sep 2003-----

  int TMTIDBMeter::NumberOfLeds(void)

//  Return the number of LEDs
//
//****************************************************************************
{
  if (LedFullSize() <= 0) return 1;
  int L = (PhysicalSize() + LedSpacing)/LedFullSize();
  if (ShowClipLed) L--;
  return L;
}

//-----------------RecomputeSizes-----------------John Mertus----Sep 2003-----

    void TMTIDBMeter::RecomputeSizes(void)

//  Recompute all the sizes given the LEDs and spacings
//
//****************************************************************************

//  This finds the size of the _TitedArea according to
//  the direction, control size, led size and spacing
//  boarder and clip led
{
  int aLeft, aTop;
  int L, W;

  // Cannot compute this if we have not been set
  if (LedFullSize() <= 0) return;

  // L is the logical length of the bar, W the logical Width
  if (IsVertical())
    {
      L = Height;
      W = Width;
    }
  else
    {
      L = Width;
      W = Height;
    }

  // Adjust size for boarders
  if (ShowBorder)
    {
      L = L-2;
      W = W-2;
      aLeft = 1;
      aTop = 1;
    }
  else
    {
      aLeft = 0;
      aTop = 0;
    }

  // Now change the Length for the total number of LED possible
  L = (int)((L - LedSize)/LedFullSize())*LedFullSize() +  LedSize;

  if (IsVertical())
    _TiledArea = TRect(aLeft, aTop, aLeft + W, aTop + L);
  else
    _TiledArea = TRect(aLeft, aTop, aLeft + L, aTop + W);
}

//--------------------Clipped---------------------John Mertus----Sep 2003-----

  bool TMTIDBMeter::Clipped(void)

//  Return sthe clip value
//
//****************************************************************************
{
  return _Clipped;
}

//--------------------Clipped---------------------John Mertus----Sep 2003-----

  bool TMTIDBMeter::Clipped(bool On)

//  Internal routine to set the clip value, it turns on the clip
//
//****************************************************************************
{
  _Clipped = On;
  _ClipTimer->Enabled = false;
  Paint();
  return _Clipped;
}

//----------------ClipTimeCallback----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::ClipTimeCallback(TObject* Sender)

//   Internal routine to turn off the clip led
//
//****************************************************************************
{
  _Clipped = false;
  _ClipTimer->Enabled = false;
  Paint();
}

//----------------PeakTimeCallback----------------John Mertus----Sep 2003-----

    void __fastcall TMTIDBMeter::PeakTimeCallback(TObject* Sender)

//
//
//****************************************************************************
{
  _LastMax = _Position;
  Paint();
}

//------------------GetClipTime-------------------John Mertus----Sep 2003-----

    int __fastcall TMTIDBMeter::GetClipTime(void)

//  Return sthe clip timer value
//
//****************************************************************************
{
   return _ClipTimer->Interval;
}

//------------------GetPeakTime-------------------John Mertus----Sep 2003-----

     int __fastcall TMTIDBMeter::GetPeakTime(void)

//  Returns the peak timer value
//
//****************************************************************************
{
   return _PeakTimer->Interval;
}

//------------------SetClipTime-------------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetClipTime(int Value)

//   Set the clip timer value
//
//****************************************************************************
{
  if (Value < 0) Value = 0;
  if (Value == (int)_ClipTimer->Interval) return;
  _ClipTimer->Interval = Value;
}

//------------------SetPeakTime-------------------John Mertus----Sep 2003-----

     void __fastcall TMTIDBMeter::SetPeakTime(int Value)

//   Set the Peak timer value
//
//****************************************************************************
{
  if (Value == (int)_PeakTimer->Interval) return;
  _PeakTimer->Interval = Value;
}

//*****************************TLED*****************************************

//----------------------TLed----------------------John Mertus----Sep 2003-----

      TLed::TLed(Graphics::TBitmap *abmLit, Graphics::TBitmap *abmUnlit, Graphics::TBitmap *abmBlank)

//  Construct an LED, and LED has three states and the bitmaps that represent
// these states are passed into it.
//
//****************************************************************************
{
  bmLit = abmLit;
  bmUnlit = abmUnlit;
  bmBlank = abmBlank;
  _Lit = llsBlank;

}

//-------------------------Lit-------------------------John Mertus----Sep 2003-----

   TLedState TLed::Lit(void)

//  Overrides the current state of the LED
//
//****************************************************************************
 {
   return _Lit;
 }

//----------------------Lit-----------------------John Mertus----Sep 2003-----

     TLedState TLed::Lit(const TLedState bLit)

//  Returns the last requested LED state
//
//****************************************************************************
 {
   _Lit = bLit;
   return _Lit;
 }


   TLed::~TLed(void)
 {
 }

//---------------------GetLed---------------------John Mertus----Sep 2003-----

   Graphics::TBitmap *TLed::GetLed(TLedState bLit)

//  Gets the bitmap associated with the led with state bLit
//   bLit is the "color" of the bitmap.
//
//****************************************************************************
 {
    switch (bLit)
      {
        case llsBlank:
          return bmBlank;

        case llsLit:
          return bmLit;

        case llsUnlit:
          return bmUnlit;
      }

    // Store the state for later
    _Lit = bLit;
    return NULL;
 }

//---------------------Height---------------------John Mertus----Sep 2003-----

     int TLed::Height(void)

//  Returns the height of the bitmaps
//
//****************************************************************************
{
   if (bmLit == NULL) return 0;
   return bmLit->Height;
}
   int TLed::Width(void)
{
   if (bmLit == NULL) return 0;
   return bmLit->Width;
}


//--------------------Average---------------------John Mertus----Feb 2003-----

	void AverageBitmap(Graphics::TBitmap *bmIn, Graphics::TBitmap *bmOut, int nBkSz)

//  Generic routine that takes the input bitmap and averages
//  nBkSz blocks to produce the Output bitmap.
//
//   Input and output bitmaps must be 24 bits.
//   Output bitmap must be 1/nBkSz the size of the input one
//
//****************************************************************************
{
    // Now average all the values in an SxS square
    Byte *ptr;
    for (int y = 0; y < bmIn->Height; y+=nBkSz)
    {
      for (int x = 0; x < 3*bmIn->Width; x+=3*nBkSz)
        {
           int r=0;
           int g=0;
           int b=0;
           for (int row=0; row < nBkSz; row++)
             {
               ptr = (Byte *)bmIn->ScanLine[y+row] + x;
               for (int col=0; col < 3*nBkSz; col+=3)
                 {
                   r += ptr[col];
                   g += ptr[col+1];
                   b += ptr[col+2];
                 }
             }
           ptr = (Byte *)bmOut->ScanLine[y/nBkSz] + x/nBkSz;
           ptr[0] = r/(nBkSz*nBkSz);
           ptr[1] = g/(nBkSz*nBkSz);
           ptr[2] = b/(nBkSz*nBkSz);
        }
    }
}

//---------------------------------------------------------------------------

//-------------CreateLedFromResource--------------John Mertus----Sep 2003-----

    bool TMTIDBMeter::CreateLedFromResource(TColor cl, String sResource, Graphics::TBitmap *&bmOut, bool bTranspose)

//  This loads a bitmap from the resourse and colors it
//   cl is the color
//   sResourse is the name of the resource
//   bmOut is the bitmap to create
//    if bmOut is NULL, the bitmap is created
//   bTranspose is true if the bitmap is to be transposed
//
//  Return is true if success, false if the resource does not exist
//
//****************************************************************************
{
   // Get the desired Resource
   Graphics::TBitmap *bm = new Graphics::TBitmap;
   bm->LoadFromResourceName((int)HInstance, sResource);

   //  Create a copy
   Graphics::TBitmap *bmIn = new Graphics::TBitmap;
   bmIn->PixelFormat = pf24bit;

   if (!bTranspose)
     {
       bmIn->Width = bm->Width;
       bmIn->Height = bm->Height;
     }
   else
     {
       bmIn->Width = bm->Height;
       bmIn->Height = bm->Width;
     }

   // Build the copy
   TColor bgColor = bm->Canvas->Pixels[0][0];
   TColor mixColor = bm->Canvas->Pixels[1][0];
   TColor fgColor = bm->Canvas->Pixels[2][0];
   for (int r=0; r < bm->Height; r++)
    {
      for (int c=0; c < bm->Width; c++)
      {
        TColor color = bm->Canvas->Pixels[c][r];
        if (color == bgColor)
           color = clBtnFace;
        else if (color == fgColor)
           color = cl;
        else if (color == mixColor)
           color = (TColor)RGB(.5*GetRValue(cl), .5*GetGValue(cl), .5*GetBValue(cl));

        if (!bTranspose)
          bmIn->Canvas->Pixels[c][r] = color;
        else
          bmIn->Canvas->Pixels[r][c] = color;

      }
    }
   bmIn->Canvas->Pixels[1][0] = bgColor;
   bmIn->Canvas->Pixels[2][0] = bgColor;

   // Create the result
   if (bmOut == NULL) bmOut = new Graphics::TBitmap;
   bmOut->PixelFormat = pf24bit;

   // Antialias the result
   bmOut->Width = bmIn->Width/4;
   bmOut->Height = bmIn->Height/4;
   AverageBitmap(bmIn, bmOut, 4);

   // Clean up
   delete bm;
   delete bmIn;
   return true;
}

//--------------------Rebuild---------------------John Mertus----Sep 2003-----

  void TMTIDBMeter::Rebuild(void)

//  Call when the entire control needs to be rebuilt
//
//****************************************************************************
{
  MainBitmap->Width = Width;
  MainBitmap->Height = Height;
  RecomputeSizes();
  BuildLedList();

   // Build the canvas
  MainBitmap->Canvas->Brush->Color = fBackgroundColor;
  MainBitmap->Canvas->FillRect(Rect(0, 0, Width, Height));
  TiledBarBitmap->Height = _TiledArea.Height();
  TiledBarBitmap->Width = _TiledArea.Width();
  TiledBarBitmap->Canvas->Brush->Color = fBackgroundColor;
  TiledBarBitmap->Canvas->FillRect(Rect(0, 0, Width, Height));
  _InvalidateLeds = false;

  Paint();
}
