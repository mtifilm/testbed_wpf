/*
   Program name VDoubleEdit.h
   Created Jan 2001 by John Mertus

   This simple component is just a visual interface to an edit box which shows a double number.
   To use, drop on form
     Use Value to Read/Write Value
     Use Decimals to show how many decimals are displayed.

*/
//---------------------------------------------------------------------------

#ifndef _VDoubleEditH
#define _VDoubleEditH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <StdCtrls.hpp>
#include "PropX.h"
//---------------------------------------------------------------------------
class PACKAGE VDoubleEdit : public TCustomEdit
{
private:
   TNotifyEvent jDoubleChange;
   int qDecimals;                                            // Number of Decimals to Display
   double jdValue;                                            // Stored Value
   String OldText;                                            // Old text value


//
// Usual setters and getters
//   void __fastcall SetValue(double Value);                   // write method
   void __fastcall SetDecimals(int Value);                   // write method
   void __fastcall VKeyPress(TObject *Sender, WideChar &Key);
   void __fastcall SetdValue(double Value);                   // write method
   double __fastcall GetdValue(void);                   // read method

   DEFINE_CBHOOK(ValueChangeCB, VDoubleEdit);
   protected:

   public:
     __property double dValue = {read=jdValue, write=SetdValue};
     void UpdateDisplay(void);

     __fastcall VDoubleEdit(TComponent* Owner);
    void __fastcall VExit(TObject *Sender);

__published:
//    __property double Value = {read=dValue, write=SetValue};
    __property int Decimals = {read=qDecimals, write=SetDecimals, default=2};
    __property TNotifyEvent OnDoubleChange = {read=jDoubleChange, write=jDoubleChange};

};
//---------------------------------------------------------------------------
#endif
