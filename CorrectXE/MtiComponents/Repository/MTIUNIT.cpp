//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "MTIUNIT.h"
#include "IniFile.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

string *TMTIForm::IniSectionPrefix = NULL;

TMTIForm *MTIForm;
//---------------------------------------------------------------------------
__fastcall TMTIForm::~TMTIForm(void)
{
    // This does not appear to do anything but because
    // the SDL MUST be initialized for libaries this
    // does the initialization until the bug is solved

//    int iIn;
//    xistringstream is("1");
//    is >> iIn;

    //delete IniFileName;          hmmm these get used after this!!
    //delete DefaultIniSection;    hmmm these get used after this!!
}

__fastcall TMTIForm::TMTIForm(TComponent* Owner)
        : TForm(Owner)
{
  //  Save value to restore later
  OldTop = Top;
  OldLeft = Left;
  OldWidth = Width;
  OldHeight = Height;

  // Create the ini file in the application directory
  IniFileName = new string;
  DefaultIniSection = new string;
  *IniFileName = CPMPIniFileName();
  *DefaultIniSection =  StringToStdString(Name);
  fSaveOnExit = false;
//
// Warning: The visible and StayOnTop may not be always valid
// TBD: Fix the above
//
    OldVisible = Visible;
    OldStayOnTop = FormStyle;
/*
  CIniFile *ini = CreateIniFile(GetIniFileName());
  if (ini != NULL)
   {
     ReadSettings(ini, *DefaultIniSection);
     ReadSAP(ini, *DefaultIniSection);
     DeleteIniFile(ini);
   }
*/
  SaveWhenExit.Parent = this;
}

//---------------------------------------------------------------------------


//----------------SaveSAP---------------------------John Mertus-----Jan 2001---

   bool TMTIForm::SaveSAP(EIniDataType eid, CIniFile *ini)

//  This writes to an ini file.
//
//    eid is what data to use as the source of restoration
//        See header for the values
//    ini is an ini file, possibly NULL, to save the data.
//    If null, the ini file associated with eid is opened.
//
//    ini ONLY exists to optimize reading/writing the ini files.
//
//    return is true if data is successfully saved.
//
//******************************************************************************
{
  // Design data is not saved in this version
  if (eid == CM_DESIGNED_DATA)
    {
      return(true);
    }

  // Open the ini file if necessary
  bool CloseIni = false;
  if (ini == NULL)
    {
      ini = CreateIniFile(GetIniFileName(eid));
      if (ini == NULL) return(false);
      CloseIni = true;
    }
//
// Notice this MUST be done before the ShowWindow
// otherwise fVisible will always be true!
//
  if (this != Application->MainForm)
    ini->WriteInteger(GetIniSection(), "Visible", Visible);
//
//  Restore the window to its non-full screen size
//
  if (Visible) WindowState = wsNormal;
//
//  Save the values, must be done AFTer the ShowWindow
//
   auto inis = GetIniSection();
  ini->WriteInteger(GetIniSection(), "Left", Left);
  ini->WriteInteger(GetIniSection(), "Top", Top);
  ini->WriteInteger(GetIniSection(), "Width", Width);
  ini->WriteInteger(GetIniSection(), "Height", Height);
  ini->WriteInteger(GetIniSection(), "StayOnTop", FormStyle);

//
//  Now call the user routine
//
  WriteSAP(ini, GetIniSection());

//
// Close up the ini file if necessary
//
  if (CloseIni)
    return DeleteIniFile(ini);
  else
    return true;
}

//--------------------RestoreSAP---------------------John Mertus-----Jan 2001---

   bool TMTIForm::RestoreSAP(EIniDataType eid, CIniFile *ini)

//  This reads the ini file for only the SAP (Size and Position) Settings
//  This includes stayontop and visibility if not main form
//    eid is what data to use as the source of restoration
//        See header for the values
//
//    return is true if data is read.
//
//******************************************************************************
{
  // Design data goes back to original start data
  if (eid == CM_DESIGNED_DATA)
    {
      SetBounds(OldLeft, OldTop, OldWidth, OldHeight);
      if (this != Application->MainForm) Visible = OldVisible;
      FormStyle = OldStayOnTop;
      return ReadDesignSAP();
    }

  // Open the ini file if necessary
  bool CloseIni = false;
  if (ini == NULL)
    {
      ini = CreateIniFile(GetIniFileName(eid));
      if (ini == NULL) return(false);
      CloseIni = true;
    }

//
//  Check to see if the section exists, if it doesn't don't
// waste time reading it
  if (ini->SectionExists(GetIniSection()))
    {
      //
      //  Read the SAP values
      int L = ini->ReadInteger(GetIniSection(), "Left", Left);
      int T = ini->ReadInteger(GetIniSection(), "Top", Top);
      int W, H;
      if (BorderStyle == bsSingle || BorderStyle == bsDialog
          || BorderStyle == bsToolWindow)
         {
         // Do not restore the width and height for non-resizeable forms
         W = Width;
         H = Height;
         }
      else
         {
         W = ini->ReadInteger(GetIniSection(), "Width", Width);
         H = ini->ReadInteger(GetIniSection(), "Height", Height);
			}

		SetBounds(L, T, W, H);

// Only set visible if not the main form
// Mote, fVisible must always be set along with visible because
// when the program closes, before the SAP is saved, the form is made invisible
//
      if (this != Application->MainForm)
        {
          fVisible = ini->ReadInteger(GetIniSection(), "Visible", Visible);
          if (fVisible)
            Show();
          else
            Hide();
		  }
////      FormStyle = (TFormStyle)ini->ReadInteger(GetIniSection(), "StayOnTop", FormStyle);
		SetBounds(L, T, W, H);
	 }


//
//  Now call the user routine
//
  ReadSAP(ini, GetIniSection());

  // Make sure the window is visible on one of the monitors!
  FixWindowPosition();

  if (CloseIni)
    return DeleteIniFile(ini);
  else
    return true;
}



//---------------------------------------------------------------------------

//-----------------RestoreSettings-----------------John Mertus-----Jan 2001---

   bool TMTIForm::RestoreSettings(EIniDataType eid, CIniFile *ini)

//  This reads the ini file for the non-SAP Settings
//    eid is what data to use as the source of restoration
//        See header for the values
//
//    return is true if data is read.
//
//******************************************************************************
{
  // Design data goes back to original start data
  if (eid == CM_DESIGNED_DATA) return ReadDesignSettings();

  // Open the ini file if necessary
  bool CloseIni = false;
  if (ini == NULL)
    {
      ini = CreateIniFile(GetIniFileName(eid));
      if (ini == NULL) return(false);
      CloseIni = true;
    }

  // Now read the settings
  ReadSettings(ini, GetIniSection());

  if (CloseIni)
    return DeleteIniFile(ini);
  else
    return true;
}

//--------------------SaveSettings-----------------John Mertus-----Jan 2001---

	bool TMTIForm::SaveSettings(EIniDataType eid, CIniFile *ini)

//  This save the Settings to the ini file for the non-SAP Settings
//    eid is what data to use as the source of restoration
//        See header for the values
//
//    return is true if data is read.
//
//******************************************************************************
{
  // Design data does NOTHING
  if (eid == CM_DESIGNED_DATA) return true;

  // Open the ini file if necessary
  bool CloseIni = false;
  if (ini == NULL)
    {
      ini = CreateIniFile(GetIniFileName(eid));
      if (ini == NULL) return(false);
      CloseIni = true;
    }

  // Write the settings out
  WriteSettings(ini, GetIniSection());

  if (CloseIni)
    return DeleteIniFile(ini);
  else
    return true;
}

//----------------RestoreAllSettings--------------John Mertus-----Jan 2001---

   bool TMTIForm::RestoreAllSettings(EIniDataType eid)

//  This restores the form size back to default
//
//******************************************************************************
{
  CIniFile *ini = CreateIniFile(GetIniFileName(eid));
  if (ini == NULL) return(false);

  bool bResult = true;
  for (int i = 0; i < Screen->FormCount; i++)
     bResult = bResult && PostMessage(Screen->Forms[i]->Handle, CW_RESTORE_SETTINGS, eid, (long)ini);

  // We must process all messages, BEFORE deleting the ini file
  Application->ProcessMessages();
  DeleteIniFile(ini);
  return(bResult);
}

//-------------------SaveAllSettings---------------John Mertus-----Jan 2001---

	bool TMTIForm::SaveAllSettings(EIniDataType eid)

//  This Saves all the form Settings.
//
//******************************************************************************
{
  CIniFile *ini = CreateIniFile(GetIniFileName(eid));
  if (ini == NULL) return(false);

  bool bResult = true;
  for (int i = 0; i < Screen->FormCount; i++)
  {
//	  bResult = bResult && SendMessage(Screen->Forms[i]->Handle, CW_SAVE_SETTINGS, eid, (intptr_t)ini);
		auto N = Screen->Forms[i]->Name;
		auto form = dynamic_cast<TMTIForm *>(Screen->Forms[i]);
		if (form != nullptr)
		{
			form->SaveSettings(eid, ini);
		}
  }

  // We must process all messages, BEFORE deleting the ini file
  Application->ProcessMessages();
  DeleteIniFile(ini);
  return(bResult);
}

//-------------------SaveAllProperties---------------John Mertus-----Jan 2002---

   bool TMTIForm::SaveAllProperties(EIniDataType eid)

//  This Saves all the forms SAP and Settings.
//
//******************************************************************************
{
   return SaveAllSAP(eid) && SaveAllSettings(eid);
}

//-------------------RestoreAllProperties---------------John Mertus-----Jan 2002---

   bool TMTIForm::RestoreAllProperties(EIniDataType eid)

//  This Saves all the forms SAP and Settings.
//
//******************************************************************************
{
   return RestoreAllSAP(eid) && RestoreAllSettings(eid);
}
//--------------------RestoreAllSAP------------------John Mertus-----Jan 2002---

   bool TMTIForm::RestoreAllSAP(EIniDataType eid)

//  This restores all the SAP (Size and Position) Settings
//
//******************************************************************************
{
  bool bResult = true;
  for (int i = 0; i < Screen->FormCount; i++)
     bResult = bResult && PostMessage(Screen->Forms[i]->Handle, CW_RESTORE_SAP, eid, 0);
  RestoreZOrder(eid);
  Application->ProcessMessages();
  return(bResult);
}

//--------------------SaveAllSAP------------------John Mertus-----Jan 2002---

   bool TMTIForm::SaveAllSAP(EIniDataType eid)

//  This restores all the SAP (Size and Position) Settings
//
//******************************************************************************
{
  bool bResult = true;
  for (int i = 0; i < Screen->FormCount; i++)
  {
//	  bResult = bResult && SendMessage(Screen->Forms[i]->Handle, CW_SAVE_SAP, eid, 0);
		auto N = Screen->Forms[i]->Name;
		auto form = dynamic_cast<TMTIForm *>(Screen->Forms[i]);
		if (form != nullptr)
		{
			form->SaveSAP(eid, nullptr);
		}
	}

  SaveZOrder(eid);
  Application->ProcessMessages();
  return(bResult);
}

//----------------CMRestoreSAP----------------------John Mertus-----Jan 2001---

    bool __fastcall TMTIForm::CMRestoreSAP(TMessage &Message)


//  This handles the window messages and then calls the RestoreForm
//  Used so that other forms can reset this form
//
//******************************************************************************
{
  return RestoreSAP((EIniDataType)Message.WParam, (CIniFile *)Message.LParam);
}

//----------------CMRestoreSettings------------------John Mertus-----Jan 2001---

    bool __fastcall TMTIForm::CMRestoreSettings(TMessage &Message)


//  This handles the window messages and then call Read Settings
//  Used so that other forms can reset this form
//
//******************************************************************************
{
  return RestoreSettings((EIniDataType)Message.WParam, (CIniFile *)Message.LParam);
}

//----------------CMSaveSAP----------------------John Mertus-----Jan 2001---

    bool __fastcall TMTIForm::CMSaveSAP(TMessage &Message)


//  This handles the window messages and then calls the RestoreForm
//  Used so that other forms can reset this form
//
//******************************************************************************
{
  return SaveSAP((EIniDataType)Message.WParam, (CIniFile *)Message.LParam);
}

//----------------CMSaveSettings------------------John Mertus-----Jan 2001---

    bool __fastcall TMTIForm::CMSaveSettings(TMessage &Message)


//  This handles the window messages and then call Read Settings
//  Used so that other forms can reset this form
//
//******************************************************************************
{
  return SaveSettings((EIniDataType)Message.WParam, (CIniFile *)Message.LParam);
}

//----------------CMOneTimeStartup------------------John Mertus-----Jan 2001---

    void __fastcall TMTIForm::CMStartForm(TMessage &Message)


//  This starts up the form
//
//******************************************************************************
{
  StartForm();
}

void __fastcall TMTIForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    if (fSaveOnExit)
      {
         SaveSettings(CM_STARTUP_DATA);
         SaveSAP(CM_STARTUP_DATA);
      }
}
//---------------------------------------------------------------------------

//----------------Show----------------------------John Mertus-----Jan 2001---

void __fastcall TMTIForm::Show(void)

//  Shows the state of the form
//
//  Note: the key here is that fVisible is correct on show
//
//***********************************************************************
{
	FixWindowPosition();
	WindowState = wsNormal;
	fVisible = true;
	TForm::Show();
}

//----------------------Hide----------------------John Mertus-----Jan 2001---

	  void __fastcall TMTIForm::Hide(void)

//  Shows the state of the form
//
//  Note: the key here is that fVisible is correct on show
//
//***********************************************************************
{
   fVisible = false;
   TForm::Hide();
}

//---------------------------------------------------------------------------


//-------------------SaveZOrder---------------------John Mertus-----Jan 2001---

    bool TMTIForm::SaveZOrder(EIniDataType eid)

//  This saves the z-order to the ini file under section ZOrder
//  return is true if zorder is saved
//
//******************************************************************************

{
  StringList ZList;

  // The UserData and the CurrentData come from same file but different
  // Ini files, not the default is current data

  HWND wnd = Screen->ActiveForm->Handle ;
  TWinControl *tf = FindControl(wnd);
  while (wnd != NULL) 
    {
       if (tf != NULL) if (tf->Visible) ZList.push_back( StringToStdString(tf->Name));
       wnd = GetNextWindow(wnd, GW_HWNDNEXT);
       tf = FindControl(wnd);
    }

// Write the section
  CIniFile *ini = CreateIniFile(GetIniFileName(eid));
  if (ini == NULL) return(false);
  ini->WriteStringList("ZOrder", "Z", &ZList);
  return(DeleteIniFile(ini));
}
//---------------------------------------------------------------------------



//-------------------RestoreZOrder-------------------John Mertus-----Jan 2001---

    bool TMTIForm::RestoreZOrder(EIniDataType eid)

//  This restores the Z order from the eid list.
//
//    eid is what data to use as the source of restoration
//        See header for the values
//
//  return is true if all forms were restored
//
//******************************************************************************
{
   StringList ZList;

   CIniFile *ini = CreateIniFile(GetIniFileName(eid));
   if (ini == NULL) return false;

   ini->ReadStringList("ZOrder", "Z", &ZList);
   // Create any forms that were deferred
   for (unsigned int i=0; i < ZList.size(); i++)
     {
        if (ini->ReadBool(ZList[i], "Visible", false))
           CreateDeferredForm(ZList[i]);
     }

   if (!DeleteIniFile(ini)) return false;


   // Process the z-order in reverse, sending the bottommost to the top then
   // the next one etc.  THis results in the bottommost being at the bottom
   for (int i=ZList.size()-1; i >= 0; i--)
    {
        // Match the name of a form with the list.
        // Note: names are unique, there is at most one match.
        for (int j=0; j < Screen->FormCount-1; j++)
          {
             TForm *tf = Screen->Forms[j];
             if (tf->Name == (String)ZList[i].c_str())
               {
                 // Match, take the handle and insert at the top
                 SetWindowPos(tf->Handle, HWND_TOP,
                    tf->Left, tf->Top, tf->Width, tf->Height,
                    SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
               }
          }
     }
   return true;
}
//---------------------------------------------------------------------------

//-------------------GetIniFileName-------------------John Mertus-----Jan 2001---

    string TMTIForm::GetIniFileName(EIniDataType eid)

//  This returns the file name associated with the save data type
//  eid is what to save
//    CM_USER_DATA use the default filename + "1"
//  otherwise use just the default filename.
//
//******************************************************************************
{
  if (eid == CM_USER_DATA) return *IniFileName+"1";
  return *IniFileName;
}
    void TMTIForm::StartProgram(void)
{
    RestoreAllSAP(CM_STARTUP_DATA);
    RestoreAllSettings(CM_STARTUP_DATA);
    StartAllForms();
}
//---------------------------------------------------------------------------

//-------------------SetIniSectionPrefix-------------------------------------

   /* static */ void TMTIForm::SetIniSectionPrefix(const string &newPrefix)
{
   if (IniSectionPrefix == NULL)
      IniSectionPrefix = new string;

   *IniSectionPrefix = newPrefix;
}
//---------------------------------------------------------------------------

//-------------------GetIniSectionPrefix-------------------------------------

   /* static */ string TMTIForm::GetIniSectionPrefix(void)
{
   string retVal;

   if (IniSectionPrefix != NULL)
      retVal = *IniSectionPrefix;

   return retVal;
}
//---------------------------------------------------------------------------

//-------------------GetIniSection-------------------------------------------

    string TMTIForm::GetIniSection(void)
{
   string iniSection = GetIniSectionPrefix();

   if (!iniSection.empty())
      iniSection += "\\";

   iniSection += *DefaultIniSection;

   return iniSection;
}
//---------------------------------------------------------------------------

//-------------------StartForm----------------------John Mertus-----Jan 2002---

    void TMTIForm::StartForm(void)

// This dummy form is called after ALL forms are constructed and ready to go
//
//******************************************************************************
{
}

//-------------------StartAllForms------------------John Mertus-----Jan 2001---

   void TMTIForm::StartAllForms(void)

//  This restores the form size back to default
//
//******************************************************************************
{
  for (int i = 0; i < Screen->FormCount; i++)
     PostMessage(Screen->Forms[i]->Handle, CW_START_FORM, 0, 0);

  // We must process all messages
  Application->ProcessMessages();
  return;
}

//-------------------SaveProperties------------------John Mertus-----Jan 2001---

   bool TMTIForm::SaveProperties(EIniDataType eid, CIniFile *ini)

//  This convience functions saves both the SAF and preferences settings
//
//******************************************************************************
{
	bool bResult = SaveSettings(eid, ini);
	return SaveSAP(eid, ini) && bResult;
}

//------------------RestoreProperties------------------John Mertus-----Jan 2001---

    bool TMTIForm::RestoreProperties(EIniDataType eid, CIniFile *ini)

//  This convience functions restores both the SAF and preferences settings
//
//******************************************************************************
{
   bool bResult = RestoreSettings(eid, ini);
   return RestoreSAP(eid, ini) && bResult;
}

//----------------------WMClose----------------------John Mertus-----Jan 2002---

   void __fastcall TMTIForm::WMClose(TMessage &Message)

//  This changes the state of the fVisible variable and passes on the message
//
//******************************************************************************
{
  fVisible = false;
  TForm::Dispatch(&Message);
}


//---------------CreateDeferredForm---------------John Mertus----Sep 2002-----

     bool TMTIForm::CreateDeferredForm(const string &Name)

//  This creates a deferred form with name Name.
//
//****************************************************************************
{
   return false;
}

//-------------------WMInitMenu-------------------John Mertus----Sep 2002-----

      void __fastcall TMTIForm::WMInitMenu(TMessage &Message)

//
//
//****************************************************************************
{
     BeforeMainMenuDropDown();
}

// *****************************OVERRIDE THESE*******************************

//-------------BeforeMainMenuDropDown-------------John Mertus----Sep 2002-----

     void TMTIForm::BeforeMainMenuDropDown(void)

//
//
//****************************************************************************
{
   return;
}


//-------------------ReadSAP-------------------------John Mertus-----Jan 2002---

   bool TMTIForm::ReadSAP(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   return true;
}

//-------------------------WriteSAP-----------------John Mertus-----Jan 2002---

   bool TMTIForm::WriteSAP(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  return true;
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TMTIForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  return true;
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

   bool TMTIForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  return true;
}
//---------------ReadDesignSettings---------------John Mertus----Sep 2002-----

      bool TMTIForm::ReadDesignSettings(void)

//  This restorese the settings to their original values.
//
//****************************************************************************
{
  return true;
}
//-----------------ReadDesignSAP------------------John Mertus----Sep 2002-----

		bool TMTIForm::ReadDesignSAP(void)

//  This restorese the size and position settings to their original values.
//
//****************************************************************************
{
     return true;
}

//****************************************************************************
void TMTIForm::FixWindowPosition()
{
	TRect windowRect;
	windowRect.Left = Left;
	windowRect.Top = Top;
	windowRect.Right = Left + Width - 1;
	windowRect.Bottom = Top + Height - 1;

	// We want to be able to see at least 1/4 of the window, so we are going to
	// check the upper left corner and the center of the window. If less than
	// 1/4 of the window is visible, try to make the entire window visible.
	// Check the center first.
	TPoint center;
	center.X = Left + Width / 2;
	center.Y = Top + Height / 2;
	Vcl::Forms::TMonitor *monitor = Screen->MonitorFromPoint(center);

	// Center X vs. right edge of closest monitor.
	if (center.X > (monitor->Left + monitor->Width))
	{
		// Can see less than half the width - pull the whole window back in.
		Left = monitor->Left + monitor->Width - Width;
	}

	// Center Y vs. bottom edge of closest monitor.
	if (center.Y > (monitor->Top + monitor->Height))
	{
		// Can see less than half the height - pull the whole window back in.
		Top = monitor->Top + monitor->Height - Height;
	}

	// Now check the upper left corner.
	TPoint upperLeft;
	upperLeft.X = Left;
	upperLeft.Y = Top;
	monitor = Screen->MonitorFromPoint(upperLeft);
	const int arbitraryDesiredDistanceFromEdge = 100;

	// Make sure the left edge of the window isn't too close to the right edge of the monitor.
	if (upperLeft.X > (monitor->Left + monitor->Width - arbitraryDesiredDistanceFromEdge))
	{
		Left = monitor->Left + monitor->Width - Width;
	}

	// Check upper-left Y vs top edge of closest monitor.
	if (upperLeft.Y < monitor->Top)
	{
		Top = monitor->Top;
	}

	// Check upper-left X vs left edge of closest monitor.
	if (upperLeft.X < monitor->Left)
	{
		Left = monitor->Left;
	}
}

//****************************************************************************
void __fastcall TMTIForm::FormShow(TObject *Sender)
{
	FixWindowPosition();
}
//---------------------------------------------------------------------------

