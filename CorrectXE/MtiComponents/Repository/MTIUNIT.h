/*
  File Name: MTIUNIT.h
  Created:  Jan 2001 by John Mertus

  This unit provides the basis of all window navigator units.  It provides for
  automatic saving of
      size and position
      visibility
      stay on top
  The state (iconized or maximized) is not saved. These are called SAP for
  Size And Position properties.

  Read/Write Properties are virtual functions, the base read/write saves the
  above properties to the section *IniSection.  The upper class should extend
  this function to its data and be sure to call the base class.

  However the SAP and other properties are not always units.   For this reason,
  there are Save/Restore SAP function that only reads the SAP properties.

  MTIForm responds to certain messages
     CM_RESTOREPROPERTIES      This does a ReadSettings
     CM_SAVEPROPERTIES     This does a WriteSettings
     CM_RESTORESAP          This does a ReadSAP (Size and position)
     CM_SAVESAP             This does a WriteSAP (Size and position)


  These can take the arguements, which may not make sense in all cases,
     CM_STARTUP_DATA   -  Data read at startup and writen at shutdown
     CM_USER_DATA     -  Data read explicity by user
     CM_DESIGNED_DATA  -  The designed data.

  Note:  The Read/Write settings function are old and will be replaced by
  Restore/Save properties functions but are left for backward compatibility.

  To start a new program up, in the constructor, call
  SendMessage(Handle, CW_START_PROGRAM, 0, 0);

*/
//---------------------------------------------------------------------------

#ifndef MTIUNITH
#define MTIUNITH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "IniFile.h"

//---------------------------------------------------------------------------
#define CW_BASE                 (WM_APP + 234)
#define CW_START_PROGRAM        (CW_BASE + 0)
#define CW_START_FORM           (CW_BASE + 1)
#define CW_RESTORE_SETTINGS     (CW_BASE + 2)
#define CW_SAVE_SETTINGS        (CW_BASE + 3)
#define CW_RESTORE_SAP          (CW_BASE + 4)
#define CW_SAVE_SAP             (CW_BASE + 5)

//    eid is what data to use as the source of restoration
//       CM_DESIGNED_DATA  Use the designed data
//       CM_STARTUP_DATA Use data that is saved/restored at end/start of program
//       CM_USER_DATA    Use data that the user specifies explicity
//
enum EIniDataType {CM_USER_DATA, CM_DESIGNED_DATA, CM_STARTUP_DATA};

class Stupid;   // Used for autosave

class TMTIForm : public TForm
{
__published:	// IDE-managed Components
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall FormShow(TObject *Sender);

private:	// User declarations

    // Storage for restore
    int OldTop, OldLeft, OldWidth, OldHeight;
    bool OldVisible;
    TFormStyle OldStayOnTop;
    bool SaveZOrder(EIniDataType eid);
    bool RestoreZOrder(EIniDataType eid);
//
// This is a kludge that allows all subwindows to be saved when
// destroyed.  It works because BEFORE the class is destroyed, the
// Saver class is destroyed (automatically), this calls the save
//
class CSaveWhenExit
  {
    public:
       TMTIForm *Parent;
      CSaveWhenExit(void)
        {
          Parent = NULL;
        }

     ~CSaveWhenExit(void)
        {
          if (Parent == NULL) return;
          if (Parent->SaveOnExit)
            {
              Parent->SaveSettings();
              Parent->SaveSAP();
            }
        }
  };

  CSaveWhenExit SaveWhenExit;

 private:

    static string  *IniSectionPrefix;       // Shared amngst all child forms


 protected:
    string  *IniFileName;                   // Storage for the inifile
    string  *DefaultIniSection;             // Do not make implicit constructor!!!!
    bool fSaveOnExit;
    bool fReadOnCreate;
    bool __fastcall CMRestoreSAP(TMessage &Message);
    bool __fastcall CMRestoreSettings(TMessage &Message);
    bool __fastcall CMSaveSAP(TMessage &Message);
    bool __fastcall CMSaveSettings(TMessage &Message);
    void __fastcall CMStartForm(TMessage &Message);
    void __fastcall CMStartProgram(TMessage &Message) { StartProgram(); }
    void __fastcall WMClose(TMessage &Message);
    void __fastcall WMInitMenu(TMessage &Message);

    BEGIN_MESSAGE_MAP
      VCL_MESSAGE_HANDLER(CW_RESTORE_SAP, TMessage, CMRestoreSAP)
      VCL_MESSAGE_HANDLER(CW_RESTORE_SETTINGS, TMessage, CMRestoreSettings)
      VCL_MESSAGE_HANDLER(CW_SAVE_SAP, TMessage, CMSaveSAP)
      VCL_MESSAGE_HANDLER(CW_SAVE_SETTINGS, TMessage, CMSaveSettings)
      VCL_MESSAGE_HANDLER(CW_START_FORM, TMessage, CMStartForm)
      VCL_MESSAGE_HANDLER(CW_START_PROGRAM, TMessage, CMStartProgram)
      VCL_MESSAGE_HANDLER(WM_CLOSE, TMessage, WMClose)
      VCL_MESSAGE_HANDLER(WM_INITMENU, TMessage, WMInitMenu)
    END_MESSAGE_MAP(TForm)


    bool SaveAllSAP(EIniDataType eid=CM_STARTUP_DATA);                    // Saves all forms size and positions
    bool RestoreAllSAP(EIniDataType eid=CM_STARTUP_DATA);                 // Restores all forms size and positions
    bool SaveAllSettings(EIniDataType eid=CM_STARTUP_DATA);                // Saves all forms settings
    bool RestoreAllSettings(EIniDataType eid=CM_STARTUP_DATA);             // Restores all the settings
    bool SaveAllProperties(EIniDataType eid=CM_STARTUP_DATA);
    bool RestoreAllProperties(EIniDataType eid=CM_STARTUP_DATA);

	 void FixWindowPosition();

    void StartAllForms(void);                                 // Runs startup
    void StartProgram(void);
    virtual void BeforeMainMenuDropDown(void);

    virtual bool CreateDeferredForm(const string &Name);

public:		// User declarations
    bool fVisible;       // Keep track of visibility ourselfs
    __fastcall TMTIForm(TComponent* Owner);
    __fastcall ~TMTIForm(void);

    HIDESBASE void __fastcall Hide(void);
    HIDESBASE void __fastcall Show(void);

    bool SaveSAP(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);          // Save the size and position
    bool RestoreSAP(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);       // Restore the size and position
    bool SaveSettings(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);     // Save the settings
    bool RestoreSettings(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);  // Restores the settings
    bool SaveProperties(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);
    bool RestoreProperties(EIniDataType eid=CM_STARTUP_DATA, CIniFile *ini = NULL);

    virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
    virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.
    virtual bool ReadDesignSettings(void);
    virtual bool ReadDesignSAP(void);
    virtual void StartForm(void);

    virtual string GetIniFileName(EIniDataType eid = CM_STARTUP_DATA);
    virtual string GetIniSection(void);
    static void SetIniSectionPrefix(const string &newPrefix);
    static string GetIniSectionPrefix(void);

__published:
    __property bool SaveOnExit = {read=fSaveOnExit, write=fSaveOnExit};
};


//---------------------------------------------------------------------------
extern PACKAGE TMTIForm *MTIForm;
//---------------------------------------------------------------------------
#endif
