//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "About.h"
#include "DllSupport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAboutForm *AboutForm;
//---------------------------------------------------------------------------
__fastcall TAboutForm::TAboutForm(TComponent* Owner)
        : TForm(Owner)
{
  char FileName[256];
  GetModuleFileName( NULL,FileName, sizeof (FileName) );
  Version->Caption = (AnsiString)"Version " + GetDLLVersion(FileName).c_str();
  Date->Caption = GetFileDateString(FileName).c_str();
  Copyright->Caption = GetDLLCopyright(FileName).c_str();
  Comments->Caption = GetDLLInfo(FileName, "Comments").c_str();
}
//---------------------------------------------------------------------------
