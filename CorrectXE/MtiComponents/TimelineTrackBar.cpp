//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include <math.h>
#include <algorithm>

#include "TimelineTrackBar.h"
#pragma package(smart_init)
#pragma resource "*.res"
#include "IniFile.h"
#include "HRTimer.h"
#include "MTIio.h"

#define ROOM_FOR_CURSOR_ON_LAST_FRAME 2  // pixels
#define clWindowBackground (TColor)0x00222222

#define clTrackStartShadow (TColor)0x00161616
#define clTrackShadow (TColor)0x00080808
#define clTrackHighlight (TColor)0x00575457

#define clThumbHighlight (TColor)0x00575757
#define clThumbShadow (TColor)0x00161616

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TTimelineTrackBar *)
{
        new TTimelineTrackBar(NULL);
}

static const SIniAssociation TTrackStyleAssociationsData[] =
  {
        {"trUserDrawn", trUserDrawn},
	{"trFlat", trFlat},
	{"trRaised", trRaised},
	{"trLowered", trLowered},
        {"", -1}
  };

static const SIniAssociation TThumbStyleAssociationsData[] =
  {
        {"tsUserDrawn", tsUserDrawn},
	{"tsRectangle", tsRectangle},
	{"tsRaisedRectangle", tsRaisedRectangle},
	{"tsLoweredRectangle", tsLoweredRectangle},
        {"", -1}
   };

static const SIniAssociation TActiveDragAssociationsData[] =
  {
        {"adNone", adNONE},
        {"adThumb", adTHUMB},
        {"adCursor_0", adCURSOR},
        {"adBoth", adBOTH},
        {"", -1}
   };

CIniAssociations TrackStyleAssociations(TTrackStyleAssociationsData, "tr");
CIniAssociations ThumbStyleAssociations(TThumbStyleAssociationsData, "ts");
CIniAssociations ActiveDragAssociations(TActiveDragAssociationsData, "ad");

//---------------TTimelineTrackBar----------------John Mertus----May 2003-----

  __fastcall TTimelineTrackBar::TTimelineTrackBar(TComponent* Owner)
        : TCustomControl(Owner)

//  Usual constructor, set the defaults
//
//****************************************************************************
{
   _Max = 10;
   _Min = -10;
   _ThumbPosition = 0;
   _SmallChange = 1;
   _LargeChange = 2;
   _Orientation = trHorizontal;
   _TrackSize = 20;
   _TrackStyle = trLowered;
   _TrackColor = (TColor)0x00222222;
   _ThumbStyle = tsRaisedRectangle;
   _ThumbColor = (TColor)0x00333333;
   _EnableWheel = false;
   _Color = (TColor)0x00333333;
   _SpecialDrag = false;
   _SpecialDragX = -10000;
   _ActiveDrag = adTHUMB;
   _CurrentActiveDrag = adTHUMB;
   _NumberOfCursors = 0;
   _ShowCursors = false;
   _ShowFocus = true;
   _DefaultCursorColor = clBlue;
   _EnableThumbResize = true;
   _BorderWidth = 6;
   _JumpOnLeftDown = false;
   _ShowHandles = false;
   _LeftHandles = 0;
   _RightHandles = 0;
   _ShowMarks = false;
   _MarkInFrame = -1;
   _MarkOutFrame = -1;

//  TColor C = ColorToRGB(_ThumbColor);
//  _ThumbRangeColor = RGB(GetRValue(C)*.9, GetGValue(C)*.9, GetBValue(C)*.9);
   _ThumbRangeColor = (TColor)0x00222222;
   _Tracking = false;
   _IgnoreExPosition = false;

   _ThumbResizing = RESIZING_NOT;

   _MinThumbSize = 17;
   Width = 450;
   Height = 50;
   TabStop = True;
   _OldThumbX = MaxInt;
   _OldThumbY = MaxInt;
   _TrackBmp = new Graphics::TBitmap;
   _ThumbBmp = new Graphics::TBitmap;
   _BackGroundBmp = NULL;
   _DitherBmp = new Graphics::TBitmap;
   _DitherBmp->Width = 8;
   _DitherBmp->Height = 8;
   _UseExPostion = false;
   _ShowThumb = true;
   _ShowThumbDetent = true;

   _Range = 0;
   _ShowThumbRange = true;

   UpdateTrackData();
   UpdateThumbData();
   UpdateDitherBitmap();

   _FrameWidth = _UsablePixels;  // computed in UpdateThumbData()

   // Timer to control when update is done after mouse release
   ExtTimer = new TTimer(this);
   ExtTimer->Enabled = false;
   ExtTimer->Interval = 175;
   ExtTimer->OnTimer = ExtTimerTimer;

   _OnKeyDown = NULL;
   _OnChange = NULL;
   _OnRangeChange = NULL;
   _OnExternalThumbChange = NULL;
   _OnExternalCursorChange = NULL;
   _OnPaint = NULL;
   _OnCursorChange = NULL;

   Anchors << akRight;
   ControlStyle << csOpaque;
}

//---------------~TTimelineTrackBar---------------John Mertus----May 2003-----

    __fastcall TTimelineTrackBar::~TTimelineTrackBar(void)

//  Usual destructor
//
//****************************************************************************
{
  delete _ThumbBmp;
  _ThumbBmp = NULL;
  delete _TrackBmp;
  _TrackBmp = NULL;
  delete _DitherBmp;
  _DitherBmp = NULL;
}

//---------------------------------------------------------------------------
namespace Timelinetrackbar
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TTimelineTrackBar)};
                 RegisterComponents("MTI", classes, 0);
        }
}

//-------------------SetColor--------------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetColor(TColor Value)

//  This controls the background color
//
//****************************************************************************
{
	if (Value == _Color) return;
    _Color = Value;
    UpdateTrackData();
    Invalidate();
}

//----------------SetBorderWidth-----------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetBorderWidth(int Value)

//  This controls the width of the border
//
//****************************************************************************
{
    if (Value == _BorderWidth) return;
    _BorderWidth = Value;
    UpdateTrackData();
    UpdateThumbData();
    Invalidate();
}

//-------------------GetColor--------------------John Mertus----Jan 2004-----

      TColor  __fastcall TTimelineTrackBar::GetColor(void)

//  This returns the background color
//
//****************************************************************************
{
   return _Color;
}

//-----------------SetShowCursors------------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetShowCursors(bool Value)

//  This controls if the cursors should be displayed or not
//
//****************************************************************************
{
    if (Value == _ShowCursors) return;
    _ShowCursors = Value;
    Invalidate();
}

//-----------------SetShowHandles------------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetShowHandles(bool Value)

//  This controls if the handles should be displayed or not
//
//****************************************************************************
{
    if (Value == _ShowHandles) return;
    _ShowHandles = Value;
    Invalidate();
}

//-----------------SetShowMarks------------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetShowMarks(bool Value)

//  This controls if the marks should be displayed or not
//
//****************************************************************************
{
    if (Value == _ShowMarks) return;
    _ShowMarks = Value;
    Invalidate();
}

//-----------------SetShowFocus------------------John Mertus----Feb 2004-----

      void  __fastcall TTimelineTrackBar::SetShowFocus(bool Value)

//  This controls if the focus rectangle should be shown or not
//
//****************************************************************************
{
    if (Value == _ShowFocus) return;
    _ShowFocus = Value;
    Repaint();
}

//-------------SetNumberOfCursors---------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetNumberOfCursors(int Value)

//  This controls the number of cursors
//
//****************************************************************************
{
    if (Value < 0) Value = 0;
    if (Value == _NumberOfCursors) return;

    //  Delete if too big
    while (_Cursors.size() > (unsigned)Value) _Cursors.pop_back();

	CTimelineCursor tbc(DefaultCursorColor);
	for (unsigned i=_Cursors.size(); i < (unsigned)Value; i++)
       _Cursors.push_back(tbc);

    _NumberOfCursors = Value;
    Invalidate();
}

//---------SetDefaultCursorColor-----------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetDefaultCursorColor(TColor Value)

//  This controls the color of the cursors
//
//****************************************************************************
{
	if (Value == _DefaultCursorColor) return;
    _DefaultCursorColor = Value;
    for (unsigned i=0; i <_Cursors.size(); i++)
       _Cursors[i].DefaultColor = _DefaultCursorColor;

    Invalidate();
}

//-----------------SetActiveDrag-------------------John Mertus----Jan 2004-----

      void  __fastcall TTimelineTrackBar::SetActiveDrag(TActiveDrag Value)

//  This controls if the lines are shown for the range when the
//  range is smaller than the thumb size.
//
//****************************************************************************
{
    if (Value == _ActiveDrag) return;
    _ActiveDrag = Value;
    // _CurrentActiveDrag must never be set to adBOTH
    if (_ActiveDrag != adBOTH)
        _CurrentActiveDrag = _ActiveDrag;
    else
        _CurrentActiveDrag = adTHUMB;
    Invalidate();
}

//-----------------SetShowThumbRange---------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::SetShowThumbRange(bool Value)

//  This controls if the lines are shown for the range when the
//  range is smaller than the thumb size.
//
//****************************************************************************
{
    ShowThumbRange(Value);
}


//-----------------ShowThumbRange-----------------John Mertus----May 2003-----

      void   TTimelineTrackBar::ShowThumbRange(bool Value)

//  This controls if the lines are shown for the range when the
//  range is smaller than the thumb size.
//
//****************************************************************************
{
   if (_ShowThumbRange == Value) return;
   _ShowThumbRange = Value;
   UpdateThumbData();
   UpdateTrackData();
   Invalidate();
}

//----------------ShowThumbRange-----------------John Mertus----May 2003-----

      bool   TTimelineTrackBar::ShowThumbRange(void)

//  Return the value of ShowRange
//
//****************************************************************************
{
   return _ShowThumbRange;
}

//---------------SetShowThumb---------------------Mike Braca----Nov 2004-----

      bool __fastcall TTimelineTrackBar::SetShowThumb(bool Value)

//  Property interface to ShowThumb property
//
//****************************************************************************
{
    if (_ShowThumb == Value) return _ShowThumb;
    _ShowThumb = Value;
    UpdateThumbData();
    Invalidate();
    return _ShowThumb;
}

//---------------SetThumbDetent------------------John Mertus----Jan 2004-----

      bool __fastcall TTimelineTrackBar::SetThumbDetent(bool Value)

//  Property interface to ShowThumbDetent property
//
//****************************************************************************
{
    if (_ShowThumbDetent == Value) return _ShowThumbDetent;
    _ShowThumbDetent = Value;
    UpdateThumbData();
    Invalidate();
    return _ShowThumbDetent;
}

//-------------SetLeftHandles---------------Mike Braca----Nov 2004-----

      void  __fastcall TTimelineTrackBar::SetLeftHandles(int Value)

//  This controls the number of handle frames on the left side
//
//****************************************************************************
{
    if (Value < 0) Value = 0;
    if (Value == _LeftHandles) return;
    _LeftHandles = Value;
    Invalidate();
}

//-------------SetRightHandles---------------Mike Braca----Nov 2004-----

      void  __fastcall TTimelineTrackBar::SetRightHandles(int Value)

//  This controls the number of handle frames on the right side
//
//****************************************************************************
{
    if (Value < 0) Value = 0;
    if (Value == _RightHandles) return;
    _RightHandles = Value;
    Invalidate();
}

//-------------SetMarkInFrame---------------Mike Braca----Nov 2004-----

      void  __fastcall TTimelineTrackBar::SetMarkInFrame(int Value)

//  This controls the position of the 'In' mark; -1 indicates no mark
//
//****************************************************************************
{
    if (Value == _MarkInFrame) return;
    _MarkInFrame = Value;
    Invalidate();
}

//-------------SetMarkOutFrame---------------Mike Braca----Nov 2004-----

	  void  __fastcall TTimelineTrackBar::SetMarkOutFrame(int Value)

//  This controls the position of the 'Out' mark; -1 indicates no mark
//
//****************************************************************************
{
    if (Value == _MarkOutFrame) return;
    _MarkOutFrame = Value;
    Invalidate();
}

//--------------------SetRange--------------------John Mertus----May 2003-----

	  void __fastcall TTimelineTrackBar::SetRange(double Value)

//  Property interface to Range
//
//****************************************************************************
{
    Range(Value);
}

//---------------------Range----------------------John Mertus----May 2003-----

      void   TTimelineTrackBar::Range(double Value)

//  Sets the range.  The range is a non-negative number that is smaller
// than or equal to Max - Min.
//  Set the RangeMin and RangeMax before setting this.
//
//****************************************************************************
{
   if (Value == _Range) return;
   _Range = Value;
   //
   // Change small/and large changes
   ThumbPositionCommon(_ThumbPosition);
   UpdateThumbData();
   if (_OnRangeChange != NULL) _OnRangeChange(this);
   Repaint();
}

//---------------------Range----------------------John Mertus----May 2003-----

      double   TTimelineTrackBar::Range(void)

//  Just return the range size
//
//****************************************************************************
{
   double Result = _Range;
   if (Result < 0) Result = 0;
   if (Result > fabs(_Max - _Min + 1)) Result = fabs(_Max - _Min + 1);
   return Result;
}

//----------------PositionOfThumb-----------------John Mertus----May 2003-----

      void   TTimelineTrackBar::PositionOfThumb(double Value)

//  This is the external interface into the set program.  If the thumb
// is dragging, the values are ignored until release
//
//****************************************************************************
{
   // Hmm this should only be necessary for Position() ??!?
   //if (_IgnoreExPosition)
    //{
       //_ExPosition = Value;
       //_UseExPostion = true;
       //return;
    //}

   int OldPos = ThumbPosition; //Value;
   ThumbPositionCommon(Value);
   if ((ThumbPosition != OldPos) && (_OnExternalThumbChange != NULL))
     _OnExternalThumbChange(this);
}

//--------------ThumbPositionCommon--------------John Mertus----May 2003-----

	  double TTimelineTrackBar::ThumbPositionCommon(double Value)

//  This sets the position of the track bar.  The position must be inbetween
// the Min and Max.
//
//****************************************************************************
{
   // Allow only minimum and maximum values
   if (Value < _Min) Value = _Min;
   if (Value > _Max) Value = _Max;

	   double mn = _Min + (_Range / 2.0);
	   double mx = _Max - (_Range / 2.0);
	   if (Value < mn) Value = mn;
	   if (Value > mx) Value = mx;

   if (Value == _ThumbPosition) return _ThumbPosition;
   _ThumbPosition = Value;
   Repaint();
   return _ThumbPosition;
}

//----------------PositionOfThumb-----------------John Mertus----May 2003-----

      double TTimelineTrackBar::PositionOfThumb(void)

//  Just return the position
//
//****************************************************************************
{
   double Result = _ThumbPosition;
   if (_ThumbPosition < Min()) Result = Min();
   else if (_ThumbPosition > Max()) Result = Max();
   return Result;
}

//-------------CursorPositionCommon--------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPositionCommon(double Value, int nCur)

//  This sets the position of the cursor nCur.  The position must be inbetween
// the Min and Max.
// UPDATE: We no longer enforce the min/max because it isn't right -
//         Control dailies can send in positions out of range and simply
//         doesn't want any cursor drawn. Instead I'll enforce that the
//         position must be non-negative.
// ??? I wonder what this is supposed to return... why return the value just
//     set? Instead I think it should return tho old value maybe?
//
//****************************************************************************
{
   // Allow only minimum and maximum values
   //if (Value < _Min) Value = _Min;
   //if (Value > _Max) Value = _Max;
   // Allow only non-negative instead.
   if (Value < 0) {
      return Value;     // this is considered an error condition
   }

   if (nCur < 0) return _Min-1;
   if (nCur >= (int)_Cursors.size()) return _Min-1;
   if (Value == _Cursors[nCur].Position) return Value;
   _Cursors[nCur].Position = Value;
   Repaint();
   return CursorPosition(nCur);
}

//------------CursorPositionInternal-------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPositionInternal(double Value, int nCur)

//  This sets the position of a particular cursor.
//
//****************************************************************************
{
   // Allow only minimum and maximum values
   CursorPositionCommon(Value, nCur);
   if (_OnCursorChange != NULL) _OnCursorChange(this, nCur);
   return CursorPosition(nCur);
}

//--------------CursorPosition--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPosition(void)

//  Just return the position of the active cursor
//
//****************************************************************************
{
   return CursorPosition(ActiveCursor);
}

//--------------------CursorPosition--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPosition(int nCur)

//  Return the position of a particular cursor
//
//****************************************************************************
{
   if (nCur < 0) return _Min-1 ;
   if (nCur >= (int)_Cursors.size()) return _Min-1;
   return _Cursors[nCur].Position;
}

//--------------------CursorPosition--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPosition(double Value)

//  Set the position of the active cursor
//
//****************************************************************************
{
   return CursorPositionCommon(Value, ActiveCursor);
}

//----------------CursorPosition------------------John Mertus----May 2003-----

      double TTimelineTrackBar::CursorPosition(double Value, int nCur)

//  Just return the position of the active cursor
//
//****************************************************************************
{
    int OldPosition = CursorPosition(nCur);
    if (Value == OldPosition) return OldPosition;
    CursorPositionCommon(Value, nCur);
    if ((CursorPosition(nCur) != OldPosition) && (_OnExternalCursorChange != NULL)) _OnExternalCursorChange(this, nCur);
    return CursorPosition(nCur);
}

//-------------------IPosition--------------------John Mertus----May 2003-----

      int TTimelineTrackBar::IPosition(void)

//  Returns the position as an integer.
//
//****************************************************************************
{
   return floor(Position() + .5);
}

//--------------------RangeMin--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::RangeMin(void)

//  This returns the current value of the Range Minimum
//
//****************************************************************************
{
    // Note: Since ThumbPositionis in range and Range >= 0, we only have this check
    double Result = ThumbPosition - Range()/2;
    if (Result < _Min) Result = _Min;
    return Result;
}

//--------------------RangeMax--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::RangeMax(void)

//  This returns the current value of the Range Maximum
//  RangeMax is the highest range from the track bar
//
//****************************************************************************
{
    // Note: Since ThumbPosition is in range and Range >= 0, we only have this check
    double Result = ThumbPosition + Range()/2;
    if (Result > _Max) Result = _Max;
    return Result;
}


//----------------------Max-----------------------John Mertus----May 2003-----

  double TTimelineTrackBar::Max(void)

//  Return the maximum setting.  The Max setting must be >= the Min
//
//****************************************************************************
{
  return _Max;
}

//----------------------Min-----------------------John Mertus----May 2003-----

  double TTimelineTrackBar::Min(void)

//  Return the minimum setting.  The Max setting must be >= the Min
//
//****************************************************************************
{
  return _Min;
}

//----------------------Max-----------------------John Mertus----May 2003-----

  void TTimelineTrackBar::Max(double Value)

//  Set the maximum, if the min is greater than this maximum, Min is set
// to the Max.  The range may be also adjusted
//
//****************************************************************************
{
  if (Value == _Max) return;

  int OldPos = ThumbPosition;              // We need to check for a change
  int OldRange = Range();               // Ditto

  _Max = Value;

  if (_Min > _Max) _Min = _Max;

  if ((ThumbPosition != OldPos) && (_OnChange != NULL)) _OnChange(this);
  if ((Range() != OldRange) && (_OnRangeChange != NULL)) _OnRangeChange(this);

  UpdateThumbData();
  Invalidate();
}


//----------------------Min-----------------------John Mertus----May 2003-----

  void TTimelineTrackBar::Min(double Value)

//  Set the minimum, if the Max is less than this mimimum, the Max is
// to the Min.
//
//****************************************************************************
{
  if (Value == _Min) return;

  int OldPos = ThumbPosition;              // We need to check for a change
  int OldRange = Range();               // Ditto

  _Min = Value;

  if (_Max < _Min) _Max = _Min;

  if ((ThumbPosition != OldPos) && (_OnChange != NULL)) _OnChange(this);
  if ((Range() != OldRange) && (_OnRangeChange != NULL)) _OnRangeChange(this);

  UpdateThumbData();
  Invalidate();
}

//---------------------SetMax---------------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::SetMax(double Value)

//  Property interface to Max
//
//****************************************************************************
{
  Max(Value);
}


//---------------------SetMin---------------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::SetMin(double Value)

//  Property interface to Min
//
//****************************************************************************
{
  Min(Value);
}

//------------------Background--------------------John Mertus----May 2003-----

    void   TTimelineTrackBar::Background(Graphics::TBitmap *Value)

//  This sets a background bitmap.  Even if the same bitmap, a redraw
//  is always done.
//
//****************************************************************************
{
   _BackGroundBmp = Value;
   UpdateTrackData();
   UpdateThumbData();
   UpdateDitherBitmap();
   Invalidate();
}

//------------------Background--------------------John Mertus----May 2003-----

    Graphics::TBitmap *TTimelineTrackBar::Background(void)

//  This returns the background bitmap
//
//****************************************************************************
{
   return _BackGroundBmp;
}

//----------------SetMinThumbSize-----------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::SetMinThumbSize(int Value)

//  This sets the thumb size in PIXELS.
//
//****************************************************************************
{
  if (Value < 0) Value = -Value;
  if (Value == _MinThumbSize) return;
  _MinThumbSize = Value;
  UpdateThumbData();
  Invalidate();
}

//----------------UpdateTrackData-----------------John Mertus----May 2003-----

  void TTimelineTrackBar::UpdateTrackData(void)

//  This creates all the information needed to generate the track
//
//****************************************************************************
{
  if ( _Orientation == trHorizontal)
  {
////    Height = 20; // KLUDGE, should be removed
    _TrackSize = Height - 2*_BorderWidth;
    _RawTrackRect.Top = (Height - _TrackSize)/2;
    _RawTrackRect.Bottom = (Height + _TrackSize)/2;
	_UsablePixels = Width - 2*_RawTrackRect.Top - 1;
	if (_UsablePixels < 0) _UsablePixels = 0;
    _RawTrackRect.Left = (Width - _UsablePixels)/2;
    _RawTrackRect.Right = Width - _RawTrackRect.Left;
  }
  else
  {
    _RawTrackRect.Left = (Width/2)-(_TrackSize/2);
    _RawTrackRect.Right = (Width/2)+(_TrackSize/2);
    _UsablePixels = Height - 2*_RawTrackRect.Left;
    _RawTrackRect.Top = (Height - _UsablePixels)/2;
    _RawTrackRect.Bottom = Height - _RawTrackRect.Top;
  }

 _TrackBmp->Width = Width;
 _TrackBmp->Height = Height;

    // Draw the entire rectangle
 _TrackBmp->Canvas->Brush->Color = Color;
 _TrackBmp->Canvas->FillRect(TRect(0,0, Width, Height));

 // Now update the usable area
 _TrackRect = _RawTrackRect;
 if ((_TrackStyle == trRaised) || (_TrackStyle == trLowered))
   {
      _TrackRect.Left += 2;
      _TrackRect.Bottom += -2;
      _TrackRect.Top += 2;
      _TrackRect.Right += -2;
   }
}

//---------------UpdateDitherBitmap---------------John Mertus----May 2003-----

      void  TTimelineTrackBar::UpdateDitherBitmap(void)

//   Create a dither bitmap
//
//****************************************************************************
{
  TColor C;
  if (ColorToRGB(_TrackColor) == clSilver)
   C = clGray;
  else
   C = clSilver;

  _DitherBmp->Canvas->Brush->Color = _TrackColor;
  _DitherBmp->Canvas->FillRect(Rect(0,0, _DitherBmp->Width, _DitherBmp->Height));
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      if (((i+j) % 2) != 0)
        _DitherBmp->Canvas->Pixels[i][j] = C;
}

//-----------------SetTrackStyle------------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::SetTrackStyle(TTrackStyle Value)

//  Property interface into track style
//
//****************************************************************************
{
   if (_TrackStyle == Value) return;
   _TrackStyle = Value;
   UpdateTrackData();
   UpdateThumbData();
   Invalidate();
}

//-----------------SetThumbStyle------------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::SetThumbStyle(TThumbStyle Value)

//  Property interface into Thumb style
//
//****************************************************************************
{
   if (_ThumbStyle == Value) return;
   _ThumbStyle = Value;
   UpdateThumbData();
   Invalidate();
}

//-----------------SetTrackColor------------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::SetTrackColor(TColor Value)

//  Property interface into track color
//
//****************************************************************************
{
   if (_TrackColor == Value) return;
   _TrackColor = Value;
   UpdateDitherBitmap();
   Invalidate();
}

//----------------UpdateThumbData-----------------John Mertus----May 2003-----

	  void TTimelineTrackBar::UpdateThumbData(void)

//  This generates the thumb data when the properties change
//
//****************************************************************************
{
  // Find the thumb size, if no range, then make it the entire control
  if (_Max != _Min)
    {
      // OK, pay attention because this is a little hairy. We ant the cursor
	  // to be approximately flush right in the trackbar when it's on the
      // last frame in the range. This means that when we calculate the thumb
	  // width, we purposely use one less than the number of frames and divide
      // that into the wiodth of the trackbar minus the amount of room we want
      // to leave for the cursor to show that it is on the last frame.
	  _FrameWidth = (_UsablePixels - ROOM_FOR_CURSOR_ON_LAST_FRAME)/(_Max-_Min);
	  _ThumbSize = std::max(_MinThumbSize, (int) (_FrameWidth * Range() + 0.5) + 1);
	}
  else
    {
	  _FrameWidth = _UsablePixels;
      _ThumbSize = _UsablePixels;
    }

  // Just check if makes sense to do a thumb
  if (_MinThumbSize > _UsablePixels) return;

	_ThumbHalfSize = _ThumbSize/4;
    if (_Orientation == trHorizontal)
      {
		_ThumbBmp->Width = _ThumbSize;
        if (_ThumbStyle == tsRectangle)
          {
            if (_TrackStyle == trFlat)
               _ThumbBmp->Height = _RawTrackRect.Height()-0;
            else
               _ThumbBmp->Height = _RawTrackRect.Height()-2;
          }
        else
		  _ThumbBmp->Height = _RawTrackRect.Height()-1;
      }
    else
      {
        _ThumbBmp->Height = _ThumbSize;
        if (_ThumbStyle == tsRectangle)
          {
            if (_TrackStyle == trFlat)
               _ThumbBmp->Width = _RawTrackRect.Height()-0;
            else
               _ThumbBmp->Width = _RawTrackRect.Height()-2;
          }
        else
		  _ThumbBmp->Width = _RawTrackRect.Width()-1;
      };

    _ThumbBmp->Canvas->Brush->Color = _ThumbColor;

    _ThumbBmp->Canvas->Brush->Style = bsSolid;
    _ThumbBmp->Canvas->FillRect(Rect(0,0,_ThumbBmp->Width,_ThumbBmp->Height));


	if (_ThumbStyle == tsRectangle)
      {
		_ThumbBmp->Canvas->Pen->Color = _ThumbColor;
		_ThumbBmp->Canvas->Rectangle(0,0, _ThumbBmp->Width, _ThumbBmp->Height);
      }
    else
      {
        if  (_ThumbStyle == tsRaisedRectangle)
		  _ThumbBmp->Canvas->Pen->Color = clThumbHighlight;
        else
		  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;

        _ThumbBmp->Canvas->MoveTo(0,_ThumbBmp->Height-1);
        _ThumbBmp->Canvas->LineTo(0,0);
        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width-1,0);

        if (_ThumbStyle == tsRaisedRectangle)
		  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;
        else
		  _ThumbBmp->Canvas->Pen->Color = clThumbHighlight;

        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width-1,_ThumbBmp->Height-1);
        _ThumbBmp->Canvas->LineTo(0,_ThumbBmp->Height-1);

        if (_ThumbStyle == tsRaisedRectangle)
		  _ThumbBmp->Canvas->Pen->Color = clThumbHighlight;
        else
		  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;

        _ThumbBmp->Canvas->MoveTo(1,_ThumbBmp->Height-2);
        _ThumbBmp->Canvas->LineTo(1,1);
        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width-2,1);

        if (_ThumbStyle == tsRaisedRectangle)
		  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;
        else
          _ThumbBmp->Canvas->Pen->Color = clThumbHighlight;

        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width-2,_ThumbBmp->Height-2);
        _ThumbBmp->Canvas->LineTo(1,_ThumbBmp->Height-2);
      }

    if (_Orientation == trHorizontal)
      {
        //  Fill in the range
        TRect TempRect(0,1, _ThumbBmp->Width/2-1, _ThumbBmp->Height-2);
        int L = _FrameWidth * Range() / 2 + .5;
        if (L < _ThumbBmp->Width/2)
           {

              if (_ShowThumbRange)
                {
                  int FlatFudge = 1;
                  if (_ThumbStyle == tsRectangle)
                    {
                       FlatFudge = 0;
                       TempRect.Top -= 1;
                       TempRect.Bottom += 2;
                    }
                  TempRect.Right = FlatFudge;
                  TempRect.Left = (_ThumbBmp->Width+1)/2 - L;
                  _ThumbBmp->Canvas->Brush->Color = _ThumbRangeColor;
                  _ThumbBmp->Canvas->FillRect(TempRect);
				  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;
                  _ThumbBmp->Canvas->MoveTo(TempRect.Left,FlatFudge);
                  _ThumbBmp->Canvas->LineTo(TempRect.Left,_ThumbBmp->Height-FlatFudge);

                  TempRect.Right = (_ThumbBmp->Width+1)/2 + L - 1;
                  TempRect.Left = _ThumbBmp->Width - FlatFudge;
                  _ThumbBmp->Canvas->FillRect(TempRect);

				  _ThumbBmp->Canvas->Pen->Color = clThumbShadow;
                  _ThumbBmp->Canvas->MoveTo(TempRect.Right,1);
                  _ThumbBmp->Canvas->LineTo(TempRect.Right,_ThumbBmp->Height-FlatFudge);
                }                  
           }


        // Draw the Center line
		if (ShowThumbDetent)
          {
			_ThumbBmp->Canvas->Pen->Color = clThumbHighlight;
            _ThumbBmp->Canvas->MoveTo((_ThumbBmp->Width)/2+1,0);
            _ThumbBmp->Canvas->LineTo((_ThumbBmp->Width)/2+1,_ThumbBmp->Height);

			_ThumbBmp->Canvas->Pen->Color = clThumbShadow;
            _ThumbBmp->Canvas->MoveTo((_ThumbBmp->Width)/2,0);
            _ThumbBmp->Canvas->LineTo((_ThumbBmp->Width)/2,_ThumbBmp->Height);
          }

      }
    else
      {
		_ThumbBmp->Canvas->Pen->Color = clThumbHighlight;
        _ThumbBmp->Canvas->MoveTo(0, _ThumbBmp->Height/2);
        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width, _ThumbBmp->Height/2);

		_ThumbBmp->Canvas->Pen->Color = clThumbShadow;
        _ThumbBmp->Canvas->MoveTo(0,_ThumbBmp->Height/2-1);
        _ThumbBmp->Canvas->LineTo(_ThumbBmp->Width, _ThumbBmp->Height/2-1);
      }

}

//------------------SetThumbPosition-------------------John Mertus----May 2003-----

     void __fastcall TTimelineTrackBar::SetThumbPosition(double Value)

//  Property interface to the Position value
//
//****************************************************************************
{
   PositionOfThumb(Value);
}

//--------------------KeyUp---------------------John Mertus----May 2003-----

     void __fastcall TTimelineTrackBar::KeyUp(Word &Key, Classes::TShiftState Shift)

//  This processes all the Key up events
//
//****************************************************************************
{
  switch (Key)
   {
      case 'G':
        Cursor = crDefault;
        _SpecialDrag = false;
      break;
   }
}

//--------------------Position--------------------John Mertus----May 2003-----

      void   TTimelineTrackBar::Position(double Value)

//  This is the external interface into the set program.  If the action
// is dragging, the values are ignored until release
//
//****************************************************************************
{
   if (_IgnoreExPosition)
    {
       _ExPosition = Value;
       _UseExPostion = true;
       return;
    }

   PositionCommon(Value);
}

//----------------PositionCommon-----------------John Mertus----May 2003-----

      void   TTimelineTrackBar::PositionCommon(double Value)

//  This sets the position of the track bar.  The position must be inbetween
// the Min and Max.
//
//****************************************************************************
{
   switch (_ActiveDrag)
     {
        case adTHUMB:
          ThumbPositionCommon(Value);
          break;

        case adCURSOR:
        case adBOTH:
          CursorPositionCommon(Value, ActiveCursor);
          break;

        default:
          return;
     }
 }

//--------------ThumbPositionInternal------------John Mertus----May 2003-----

      double TTimelineTrackBar::ThumbPositionInternal(double Value)

//  This sets the position of the track bar.  The position must be inbetween
// the Min and Max.
//
//****************************************************************************
{
   // Allow only minimum and maximum values
   ThumbPositionCommon(Value);
   if (_OnChange != NULL) _OnChange(this);
   return ThumbPosition;
}


//--------------------PositionInternal------------John Mertus----May 2003-----

      void   TTimelineTrackBar::PositionInternal(double Value)

//  This sets the position of the track bar.  The position must be inbetween
// the Min and Max.
//
//****************************************************************************
{
   switch (_ActiveDrag)
     {
        case adTHUMB:
          ThumbPositionInternal(Value);
          break;

        case adCURSOR:
        case adBOTH:
          CursorPositionInternal(Value, ActiveCursor);
          break;

        default:
          return;
     }
}

//--------------------Position--------------------John Mertus----May 2003-----

      double TTimelineTrackBar::Position(void)

//  Just return the position
//
//****************************************************************************
{
   switch (_ActiveDrag)
     {
        case adTHUMB:
          return ThumbPosition;

        case adCURSOR:
          return CursorPosition();

        case adNONE:
          return ThumbPosition;

        case adBOTH:
          return CursorPosition();
     }

  return _Min-1;
}

//--------------------ScrollToFrame----------------Mike Braca----Dec 2004-----

    void TTimelineTrackBar::ScrollToFrame(double Frame)

// scroll to show frame if the frame is not already in view. We won't
// necessarily centering the frame if we need to actually scroll. In fact we
// will center except to keep the entire thumb visible. For example, if
// the frame to scroll to is frame 0, we will left-flush the thumb at zero,
// not center it.
//
//****************************************************************************
{
  if (_Orientation == trHorizontal)
    {
      int X = PositionInPixels(Frame);

      // only move if the new position is outside of thumb extent
      if (X < (_ThumbPosition - _ThumbSize/2) || X > (_ThumbPosition + _ThumbSize/2))
        {
          // special case flush left or right on the track
          if (X < _ThumbSize/2)
            {
              X = _ThumbSize/2;
            }
          else if ((X + _ThumbSize/2) > (_RawTrackRect.Right - _RawTrackRect.Left))
            {
              X = _RawTrackRect.Right - _RawTrackRect.Left - _ThumbSize/2;
            }
          // now scroll
          Frame = PositionFromPixels(X);
        }
    }
  else
    {
      // QQQ Someday, do  Vertical
    }

   ThumbPositionInternal(Frame);
}

//--------------------KeyDown---------------------John Mertus----May 2003-----

     void __fastcall TTimelineTrackBar::KeyDown(Word &Key, Classes::TShiftState Shift)

//  This processes all the Key presses
//
//****************************************************************************
{
  // Preview the key
  if (_OnKeyDown != NULL) _OnKeyDown(this, Key, Shift);

  switch (Key)
   {
    case  VK_LEFT:
       PositionInternal(Position() - _SmallChange);
       break;

    case VK_RIGHT:
       PositionInternal(Position() + _SmallChange);
       break;

    case VK_PRIOR:
       PositionInternal(Position() + _LargeChange);
       break;

    case VK_NEXT:
       PositionInternal(Position() - _LargeChange);
       break;

    case VK_END:
       PositionInternal(Max());
       break;

    case VK_HOME:
       PositionInternal(Min());
       break;

    case 'G':
        Cursor = crDrag;
        _SpecialDrag = true;
        break;

  }
  TCustomControl::KeyDown(Key,Shift);
}

//------------------WMGetDlgCode------------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::WMGetDlgCode(TWMGetDlgCode &Message)

//  Just tell windows we want to process arrow keys.
//
//****************************************************************************
{
  //We want to receive arrow key codes
  Message.Result = DLGC_WANTARROWS;
}

//----------------WMActivateChange----------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::WMActivateChange(TMessage &Message)

//  Whenever a windows property change (enable, focus) this is called
//
//****************************************************************************
{
  Invalidate();
}

//------------------WMEraseBkgnd------------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::WMEraseBkgnd(TMessage &Message)

//  Ignore all background erases
//
//****************************************************************************
{
  Message.Msg = 1;
}


//---------------------Paint----------------------John Mertus----May 2003-----

  void __fastcall TTimelineTrackBar::Paint(void)

//  The major routine that redraws the control when needed
//
//****************************************************************************
{
  if (_OnPaint != NULL)
  {
	  _OnPaint(this);
  }

  _OldThumbX = MaxInt;
  _OldThumbY = MaxInt;
  UpdateTrack();            // Create the TrackBmp
  if (Enabled)
    {
      UpdateThumb();            // This adds thumb to TrackBmp
      UpdateHandles();          // This shades the handles area
      UpdateCursors();
      UpdateMarks();            // This adds marks to TrackBmp
	}

  Canvas->Draw(0,0, _TrackBmp);
}

//-------------------DrawTrack--------------------John Mertus----May 2003-----

  void TTimelineTrackBar::DrawTrack(void)

//  Interface into user drawing
//
//****************************************************************************
{
}

//-------------------DrawThumb--------------------John Mertus----May 2003-----

  void TTimelineTrackBar::DrawThumb(int X, int Y)

//  Interface into user drawing
//
//****************************************************************************
{
}

//------------------UpdateCursors-----------------John Mertus----Jan 2004-----

  void TTimelineTrackBar::UpdateCursors(void)

//  Called to rebuild the cursors
//
//  Modified by K. Tolksdorf 6/21/05 to draw cursor 0 LAST. Needed for
//  cueing TimelineTrackBar4 in LoggerUnit.cpp. Hope no side effects..
//
//****************************************************************************
{
   if (!_ShowCursors) return;

   for (int i = _Cursors.size() - 1; i >= 0; i--)
     {
        // Set the color, override the default
        TColor cl = _Cursors[i].Color;
        if (cl == clNone) cl = DefaultCursorColor;
        _TrackBmp->Canvas->Pen->Color = cl;

        int x = PositionInPixels(_Cursors[i].Position) + _TrackRect.Left;
        _Cursors[i].Draw(x, 0, _TrackRect, _TrackBmp);
     }
}

namespace {
//------------------DrawInMark-----------------Mike Braca----Nov 2004-----

  void DrawInMark(TCanvas *canvas, const TRect &trackRect,
                  int offsetX, int vertSpace)

//  Called to draw an In mark at offsetX from the left side of the track rect;
//  may be to the left of the track rect (negative offsetX)
//
//****************************************************************************
{
  int x = trackRect.Left + offsetX;
  canvas->MoveTo(x+2, trackRect.Top + vertSpace);
  canvas->LineTo(x-1, trackRect.Top + vertSpace);
  canvas->LineTo(x-1, trackRect.Bottom - vertSpace);
  canvas->LineTo(x+3, trackRect.Bottom - vertSpace);
  canvas->MoveTo(x+2, trackRect.Top + vertSpace + 1);
  canvas->LineTo(x  , trackRect.Top + vertSpace + 1);
  canvas->LineTo(x  , trackRect.Bottom - vertSpace - 1);
  canvas->LineTo(x+3, trackRect.Bottom - vertSpace - 1);
}

//------------------DrawOutMark-----------------Mike Braca----Nov 2004-----

  void DrawOutMark(TCanvas *canvas, const TRect &trackRect,
                   int offsetX, int vertSpace)

//  Called to draw an Out mark at offsetX from the left side of the track rect;
//  may be to the right of the track rect (offsetX > track rect right)
//
//****************************************************************************
{
  int x = trackRect.Left + offsetX;
  canvas->MoveTo(x-2, trackRect.Top + vertSpace);
  canvas->LineTo(x+1, trackRect.Top + vertSpace);
  canvas->LineTo(x+1, trackRect.Bottom - vertSpace);
  canvas->LineTo(x-3, trackRect.Bottom - vertSpace);
  canvas->MoveTo(x-2, trackRect.Top + vertSpace + 1);
  canvas->LineTo(x  , trackRect.Top + vertSpace + 1);
  canvas->LineTo(x  , trackRect.Bottom - vertSpace - 1);
  canvas->LineTo(x-3, trackRect.Bottom - vertSpace - 1);
}

} // end of local namespace

//------------------UpdateMarks-----------------Mike Braca----Nov 2004-----

  void TTimelineTrackBar::UpdateMarks(void)

//  Called to draw the marks
//
//****************************************************************************
{
  if (!_ShowMarks) return;

  const int vertSpace = 3;
  const int firstFrameMarkOffset = -5;
  const int lastFrameMarkOffset = 4;

  // erase old marks outside of track if any (mark on first/last frame)
  _TrackBmp->Canvas->Pen->Color = _Color;

  DrawInMark(_TrackBmp->Canvas, _TrackRect, firstFrameMarkOffset, vertSpace);
  DrawOutMark(_TrackBmp->Canvas, _TrackRect,
              (_TrackRect.Right - _TrackRect.Left) + lastFrameMarkOffset,
              vertSpace);

  _TrackBmp->Canvas->Pen->Color = clBlack;

  if (_MarkInFrame == 0)
   {
    DrawInMark(_TrackBmp->Canvas, _TrackRect, firstFrameMarkOffset, vertSpace);
   }
  else if (_MarkInFrame > 0)
   {
    int offsetX = PositionInPixels(_MarkInFrame);
    if ((offsetX >= 0) && (offsetX <= (_TrackRect.Right - _TrackRect.Left)))
     {
      DrawInMark(_TrackBmp->Canvas, _TrackRect, offsetX, vertSpace);
     }
   }
   
  if (_MarkOutFrame >= 0)
   {
    int offsetX = PositionInPixels(_MarkOutFrame);
    if (offsetX > 0)
     {
      if (offsetX  > (_TrackRect.Right - _TrackRect.Left))
       {
        offsetX = (_TrackRect.Right - _TrackRect.Left) + lastFrameMarkOffset;
       }
      DrawOutMark(_TrackBmp->Canvas, _TrackRect, offsetX, vertSpace);
     }
   }
}

//------------------UpdateHandles-----------------Mike Braca----Nov 2004-----

  void TTimelineTrackBar::UpdateHandles(void)

//  Called to shade the handles; do after UpdateThumb() if you want to shade
//  it, too
//
//****************************************************************************
{
  // This is a tad kludgy - we key both sets of handles off whether the
  // left handles are visible
  if ((!_ShowHandles) || _LeftHandles <= _Min || _Max <= _Min) return;

  Graphics::TBitmap *maskBmp = new Graphics::TBitmap;
  Graphics::TBitmap *paintBmp = new Graphics::TBitmap;
  Graphics::TBitmap *workBmp = new Graphics::TBitmap;
  int widthInPixels;
  TRect sourceRect, destRect;

  maskBmp->Width = 8;
  maskBmp->Height = 8;
  maskBmp->Canvas->Brush->Color = clBlack;
  maskBmp->Canvas->FillRect(Rect(0, 0, 8, 8));
  paintBmp->Width = 8;
  paintBmp->Height = 8;
  paintBmp->Canvas->Brush->Color = clRed;
  paintBmp->Canvas->FillRect(Rect(0, 0, 8, 8));
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      if (((i+j) % 2) != 0)
       {
        maskBmp->Canvas->Pixels[i][j] = clWhite;
		paintBmp->Canvas->Pixels[i][j] = clBlack;
       }

  workBmp->Width = PositionInPixels(_LeftHandles);
  workBmp->Height = _TrackRect.Bottom + 1 - _TrackRect.Top;
  sourceRect = TRect(0, 0, workBmp->Width, workBmp->Height);
  destRect = TRect(_TrackRect.Left, _TrackRect.Top,
                   _TrackRect.Left + workBmp->Width, _TrackRect.Bottom + 1);

  workBmp->Canvas->Brush->Bitmap = maskBmp;
  workBmp->Canvas->FillRect(sourceRect);
  _TrackBmp->Canvas->CopyMode = cmSrcAnd;
  _TrackBmp->Canvas->CopyRect(destRect, workBmp->Canvas, sourceRect);

  workBmp->Canvas->Brush->Bitmap = paintBmp;
  workBmp->Canvas->FillRect(sourceRect);
  _TrackBmp->Canvas->CopyMode = cmSrcPaint;
  _TrackBmp->Canvas->CopyRect(destRect, workBmp->Canvas, sourceRect);

  if (_RightHandles > 0 && _RightHandles < _Max)
   {
    workBmp->Width = PositionInPixels(_RightHandles);
    workBmp->Height = _TrackRect.Bottom + 1 - _TrackRect.Top;
    destRect = TRect(_TrackRect.Right + 1 - workBmp->Width, _TrackRect.Top,
                     _TrackRect.Right + 1,  _TrackRect.Bottom + 1);
    sourceRect = TRect(0, 0, workBmp->Width, workBmp->Height);

    workBmp->Canvas->Brush->Bitmap = maskBmp;
    workBmp->Canvas->FillRect(sourceRect);
    _TrackBmp->Canvas->CopyMode = cmSrcAnd;
    _TrackBmp->Canvas->CopyRect(destRect, workBmp->Canvas, sourceRect);

    workBmp->Canvas->Brush->Bitmap = paintBmp;
    workBmp->Canvas->FillRect(sourceRect);
    _TrackBmp->Canvas->CopyMode = cmSrcPaint;
    _TrackBmp->Canvas->CopyRect(destRect, workBmp->Canvas, sourceRect);
   }
  _TrackBmp->Canvas->CopyMode = cmSrcCopy;
  delete workBmp;
  delete maskBmp;
  delete paintBmp;
}

//------------------UpdateTrack-------------------John Mertus----May 2003-----

  void TTimelineTrackBar::UpdateTrack(void)

//  Called to rebuild the track portion of the control
//
//****************************************************************************
{
  if (_TrackStyle == trUserDrawn)
	DrawTrack();
  else
   {
	 _TrackBmp->Canvas->Pen->Color = clBlack;      //xxx
     if (Enabled)
      {
        _TrackBmp->Canvas->Brush->Style = bsSolid;
        _TrackBmp->Canvas->Brush->Color = _TrackColor;
      }
    else
      {
		_TrackBmp->Canvas->Brush->Bitmap = _DitherBmp;
      }

    // Draw the entire rectangle
    _TrackBmp->Canvas->FillRect(_RawTrackRect);
	_TrackBmp->Canvas->Brush->Bitmap = NULL;

    if (_BackGroundBmp != NULL)
       _TrackBmp->Canvas->CopyRect(_TrackRect, _BackGroundBmp->Canvas,
            TRect(0,0, _BackGroundBmp->Width, _BackGroundBmp->Height));

	if ((_TrackStyle == trRaised) || (_TrackStyle == trLowered))
	 {
	  if (_TrackStyle == trRaised)
		_TrackBmp->Canvas->Pen->Color = clWhite;
	  else
		_TrackBmp->Canvas->Pen->Color = clTrackStartShadow;
	  _TrackBmp->Canvas->MoveTo(_RawTrackRect.Left,_RawTrackRect.Bottom);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Left,_RawTrackRect.Top);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Right,_RawTrackRect.Top);

	  if (_TrackStyle == trRaised)
		_TrackBmp->Canvas->Pen->Color = clBlack;
	  else
		_TrackBmp->Canvas->Pen->Color = clTrackHighlight;
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Right,_RawTrackRect.Bottom);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Left,_RawTrackRect.Bottom);

	  if (_TrackStyle == trRaised)
		_TrackBmp->Canvas->Pen->Color = clTrackHighlight;
	  else
		_TrackBmp->Canvas->Pen->Color = clTrackStartShadow;
	  _TrackBmp->Canvas->MoveTo(_RawTrackRect.Left+1,_RawTrackRect.Bottom-1);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Left+1,_RawTrackRect.Top+1);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Right-1,_RawTrackRect.Top+1);

	  if (_TrackStyle == trRaised)
        _TrackBmp->Canvas->Brush->Color = _TrackColor;
	  else
		 _TrackBmp->Canvas->Pen->Color = clTrackHighlight;

	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Right-1,_RawTrackRect.Bottom-1);
	  _TrackBmp->Canvas->LineTo(_RawTrackRect.Left+1,_RawTrackRect.Bottom-1);

	}

	// Draw the focused rectangle
	  _TrackBmp->Canvas->Brush->Color = Color;
	  _TrackBmp->Canvas->Brush->Style = bsSolid;
	  _TrackBmp->Canvas->FrameRect(ClientRect);
	  _TrackBmp->Canvas->Pen->Color = clWhite;
	  if (ShowFocus && (BorderWidth >= 2) && Focused())
	   {
		 _TrackBmp->Canvas->DrawFocusRect(ClientRect);
	   }

  }
}

//------------------UpdateThumb-------------------John Mertus----May 2003-----

  void TTimelineTrackBar::UpdateThumb(void)

//  Called to rebuild the Thumb data
//
//****************************************************************************
{
  // Check the on-off switch
  if (!_ShowThumb) return;

  int X,Y,TempX,TempY;
  TRect TempRect;

  if (_Orientation == trHorizontal)
    {
      if (_Max != _Min)
        X = _FrameWidth * (ThumbPosition - _Min) + _RawTrackRect.Left - _ThumbSize/2 + .5;
      else
        X = _RawTrackRect.Left;

      // This check is not really needed
      if (X > _UsablePixels) X = _UsablePixels;

      Y = (Height/2)-(_ThumbBmp->Height/2);
    }
   else
    {
      X = (Width/2)-(_ThumbBmp->Width/2);
      if (_Max != _Min)
        Y = _FrameWidth * (_Max+1-ThumbPosition) + _RawTrackRect.Top - _ThumbSize/2 + .5;
      else
        Y = _RawTrackRect.Top;
     }

  _OldThumbX = X;
  _OldThumbY = Y;

  if (_ThumbStyle == tsUserDrawn)
   {
     DrawThumb(X,Y);
     return;
   }

  Graphics::TBitmap *WorkBmp = new Graphics::TBitmap;

  WorkBmp->Width = _ThumbBmp->Width;
  WorkBmp->Height = _ThumbBmp->Height;
  WorkBmp->Canvas->Draw(0,0,_ThumbBmp);

   // Only copy part if the thumbbar overlaps the edges
   // Note:  Because rectangle is smaller than the trackbar, we don't have
   // to worry about the other end
   TRect DestRect(X, Y, X + _ThumbBmp->Width, Y+_ThumbBmp->Height);
   TRect SourceRect(0, 0, _ThumbBmp->Width, _ThumbBmp->Height);
   _VThumbRect = DestRect;

   if (_Orientation == trHorizontal)
     {
       if (_RawTrackRect.Left >= DestRect.Left)
         {
           if  (_ThumbStyle == tsRectangle)
             DestRect.Left = _TrackRect.Left;
           else
             DestRect.Left = _TrackRect.Left+1;
           SourceRect.Left = DestRect.Left - X;
         }

       if (_RawTrackRect.Right < DestRect.Right)
         {
           DestRect.Right = _TrackRect.Right;
           SourceRect.right = DestRect.Right - X;
         }
     }
   else
    {
       if (_RawTrackRect.Bottom >= DestRect.Bottom)
		 {
           if  (_ThumbStyle == tsRectangle)
             DestRect.Bottom = _TrackRect.Bottom;
           else
             DestRect.Bottom = _TrackRect.Bottom+1;

           SourceRect.Bottom = DestRect.Bottom - Y;
         }

       if (_RawTrackRect.Top < DestRect.Top)
         {
           DestRect.Top = _TrackRect.Top;
           SourceRect.Top = DestRect.Top - Y;
         }

    }

  // Copy the track background to the thumb
  if ((_BackGroundBmp != NULL) && (ThumbStyle == tsRectangle))
   {
      TRect tmpRect = SourceRect;
      // CopyRect doesn't create a bitmap
      // make right size using CopyRect and draw transparent using draw
      Graphics::TBitmap *bmTmp = new Graphics::TBitmap;
      bmTmp->Assign(WorkBmp);
      bmTmp->Canvas->CopyRect(SourceRect, _TrackBmp->Canvas, DestRect);

      bmTmp->TransparentColor = _TrackColor;
      bmTmp->TransparentMode = tmFixed;
      bmTmp->Transparent = true;
      WorkBmp->Canvas->Draw(0, 0, bmTmp);
      delete bmTmp;

   }

   // Create the bitmap
   _TrackBmp->Canvas->CopyRect(DestRect, WorkBmp->Canvas, SourceRect);
   delete WorkBmp;

   _ThumbRect = DestRect;
}

//-------------------IsDragging-------------------John Mertus----May 2003-----

    bool  TTimelineTrackBar::IsDragging(void)

//  Returns true if the user is dragging the control. This controls if
//  the Position command should result in a thumb change.
//
//*****************************************************************************
{
   return _Tracking || (_ThumbResizing != RESIZING_NOT);
}

//-----------------ThumbMouseDown------------------John Mertus----May 2003-----

    void __fastcall TTimelineTrackBar::ThumbMouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Process the mouse down events for the thumb
//
//****************************************************************************
{
  if (!_ShowThumb) return;

  if ((_ThumbResizing == RESIZING_NOT)  && EnableThumbResize)
   {
      if ((X >= (_VThumbRect.Left - 3)) && (X <= _VThumbRect.Left + 3))
       {
        if (X > (_RawTrackRect.Left))
          {
            _ThumbResizing = RESIZING_LEFT;
            _IgnoreExPosition = true;
            return;
          }
       }

      if ((X >= (_VThumbRect.Right - 3)) && (X <= _VThumbRect.Right + 3))
       {
        if (X < (_RawTrackRect.Right))
          {
            _ThumbResizing = RESIZING_RIGHT;
            _IgnoreExPosition = true;
            return;
          }
       }
   }

  if (PtInRect(_VThumbRect,Point(X,Y)))
    {
       if (!_Tracking)
         {
           _Tracking = true;
           // Subtract off the position of the timeline in pixels
           // from the mouse position, adjust for the relative offsets
           _TrackOffset = X - PositionInPixels(PositionOfThumb()) - _RawTrackRect.Left;
           _IgnoreExPosition = true;
         }
    }
  else // clicked outside of thumb
   {
      // Ugly, Ugly code, left mouse down can either be a jump
      // or a move
      if (JumpOnLeftDown) // OK to JUMP even if something else is tracking
        {
          if (!_Tracking)
            {
              _TrackOffset = 0;
              _Tracking = true;
            }
          if (_Orientation == trHorizontal)
            {
              X = X - _TrackRect.Left - _TrackOffset;
              // special hack: when both cursor and thumb are jumping,
              // we don't need to center the thumb at the jump point,
              // so prefer to show as much of the clip as possible
              if (_CurrentActiveDrag == adBOTH &&
                  _ThumbSize)
                {
                  if ((X - _ThumbSize/2) < _RawTrackRect.Left)
                    {
                      X = _RawTrackRect.Left + _ThumbSize/2;
                    }
                  else if ((X + _ThumbSize/2) > _RawTrackRect.Right)
                    {
                      X = _RawTrackRect.Right - _ThumbSize/2;
                    }
                }
              ThumbPositionInternal(PositionFromPixels(X));
            }
          else
            {
              Y = Y + _TrackRect.Top - _TrackOffset;
              ThumbPositionInternal(PositionFromPixels(Y));
           }
        }
      else if (!_Tracking)
        {
           if (_Orientation == trHorizontal)
             {
               if ((X > _ThumbRect.Right) && (X <= _RawTrackRect.Right))
                  ThumbPositionInternal(PositionOfThumb() + _LargeChange);
               else if ((X < _ThumbRect.Left) && (X >= _RawTrackRect.Left))
                  ThumbPositionInternal(PositionOfThumb() - _LargeChange);
              }
           else
             {
               if ((Y > _ThumbRect.Bottom) && (Y <= _RawTrackRect.Bottom))
                  ThumbPositionInternal(PositionOfThumb() - _LargeChange);
               else if ((Y < _ThumbRect.Top) && (Y >= _RawTrackRect.top))
                  ThumbPositionInternal(PositionOfThumb() + _LargeChange);
             }
        }

      return;
   }
}

//-----------------CursorMouseDown-----------------John Mertus----May 2003-----

    void __fastcall TTimelineTrackBar::CursorMouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Process the mouse down events when a cursor move is wanted
//
//****************************************************************************
{
  if (!_Tracking)
    {
      if ((ActiveCursor < 0) || (ActiveCursor >= (int)_Cursors.size())) return;

      int n = PositionInPixels(_Cursors[ActiveCursor].Position);
      TRect Rect(n + _RawTrackRect.Left - 4, _TrackRect.Top, n + _RawTrackRect.Left + 4, _TrackRect.Bottom);
      if (PtInRect(Rect,Point(X,Y)))
        {
          if (!_Tracking)
            {
              _Tracking = true;
              // Subtract off the position of the timeline in pixels
              // from the mouse position, adjust for the relative offsets
              _TrackOffset = X - n - _RawTrackRect.Left;
              _IgnoreExPosition = true;
            }
        }
      else if ((_ActiveDrag == adCURSOR) ||
                ((_ActiveDrag == adBOTH) && _JumpOnLeftDown))
        {
          if (!_Tracking)
            {
              _TrackOffset = 0;
              _Tracking = true;
            }
          if (_Orientation == trHorizontal)
            {
              X = X - _TrackRect.Left;    // offset is 0
              CursorPositionInternal(PositionFromPixels(X), ActiveCursor);
            }
          else
            {
              Y = Y + _TrackRect.Top;     // offset is 0
              CursorPositionInternal(PositionFromPixels(Y), ActiveCursor);
            }
       }

      return;
   }

}

//-------------------MouseDown--------------------John Mertus----May 2003-----

    void __fastcall TTimelineTrackBar::MouseDown(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Process the mouse down events
//
//****************************************************************************
{
  // Do nothing if we are not enabled
  if (!Enabled) return;

////JAM  TCustomControl::MouseDown(Button,Shift,X,Y);
  SetFocus();

  // See if this is a special drag
  if (_SpecialDrag)
   {
     _SpecialDragX = X;
     return;
   }

  // Process the middle mouse button
  if (Button == mbMiddle)
    {
       _TrackOffset =  -_RawTrackRect.Left;
       if (_Orientation == trHorizontal)
          {
             X = X - _RawTrackRect.Left - _TrackOffset;
             PositionInternal(PositionFromPixels(X));
          }
       else
          {
             Y = Y + _RawTrackRect.Top - _TrackOffset;
             PositionInternal(PositionFromPixels(Y));
          }
       _Tracking = true;
       return;
    }

  // Process the left button
  if (Button == mbLeft && _ActiveDrag != adNONE)
    {
      // When dragging BOTH is selected, prefer to drag the cursor if the
      // click was close to it, else drag the thumb.
      if ((_ActiveDrag == adBOTH) && (X > _RawTrackRect.Left))
        {
          if (((size_t) ActiveCursor < _Cursors.size())
                  && (X >= PositionInPixels(_Cursors[ActiveCursor].Position) + _TrackRect.Left -4)
                  && (X <= PositionInPixels(_Cursors[ActiveCursor].Position) + _TrackRect.Left +4)
                 )
            {
              _CurrentActiveDrag = adCURSOR;
            }
          else if (_ShowThumb && (X >= _VThumbRect.Left) && (X <= _VThumbRect.Right))
            {
              _CurrentActiveDrag = adTHUMB;
            }
          else
            {
              _CurrentActiveDrag = adBOTH;    // if jumping them both!
            }
         }

       // Check the drag
       switch (_CurrentActiveDrag)
         {
            default:
              break;                 // Do nothing if no dragging

            case adTHUMB:            // Drag the thumb
              ThumbMouseDown(Button, Shift, X, Y);
              break;

            case adCURSOR:           // All other cursors
              CursorMouseDown(Button, Shift, X, Y);
              break;

            case adBOTH:             // Drag everything
              CursorMouseDown(Button, Shift, X, Y);  // DO THIS FIRST! QQQ
              ThumbMouseDown(Button, Shift, X, Y);
              _CurrentActiveDrag = adCURSOR;  // never leave it as adBOTH
              break;
         }
    }
}

//---------------CursorMouseMove-------------------John Mertus----May 2003-----

   void __fastcall TTimelineTrackBar::CursorMouseMove(Classes::TShiftState Shift, int X, int Y)

//  Process the Mouse movement events fro the cursor
//
//****************************************************************************
{
  if ((ActiveCursor < 0) || (ActiveCursor >= (int)_Cursors.size())) return;

  if (_Tracking)
    {
      if (_Orientation == trHorizontal)
        {
           X = X - _TrackRect.Left - _TrackOffset;
           CursorPositionInternal(PositionFromPixels(X), ActiveCursor);
        }
      else
        {
           Y = Y + _TrackRect.Top - _TrackOffset;
           CursorPositionInternal(PositionFromPixels(Y), ActiveCursor);
        }
    }

}

//---------------ThumbMouseMove-------------------John Mertus----May 2003-----

   void __fastcall TTimelineTrackBar::ThumbMouseMove(Classes::TShiftState Shift, int X, int Y)

//  Process the Mouse movement events
//
//****************************************************************************
{
  if (_Tracking)
   {
     if (_Orientation == trHorizontal)
       {
         X = X - _RawTrackRect.Left - _TrackOffset;
         ThumbPositionInternal(PositionFromPixels(X));
       }
     else
       {
         Y = Y + _RawTrackRect.Top - _TrackOffset;
         ThumbPositionInternal(PositionFromPixels(Y));
       }
     return;
   }

  if (_ThumbResizing != RESIZING_NOT)
    {
       X = X - _RawTrackRect.Left;
       double value = PositionFromPixels(X);
       double mov;

       switch (_ThumbResizing)
        {
          case RESIZING_LEFT:
            mov = -value + RangeMin();
            if (Shift.Contains(ssShift))
                ThumbPositionInternal(PositionOfThumb() - mov/2);
            Range(Range() + mov);
            break;

          case RESIZING_RIGHT:
            mov = -value + RangeMax();
            if (Shift.Contains(ssShift))
                ThumbPositionInternal(PositionOfThumb() - mov/2);
            Range(Range() - mov);
			break;

		  default:
		    break;
		}

       return;
    }

  // Otherwise set the cursor to the proper ones
  if ((X >= (_VThumbRect.Left -3)) && (X <= (_VThumbRect.Left + 3)) && (X > (_RawTrackRect.Left)) && EnableThumbResize)
    {
       Cursor = crHSplit;
       return;
    }

  if ((X >= (_VThumbRect.Right - 3)) && (X <= (_VThumbRect.Right + 3)) && (X < (_RawTrackRect.Right)) && EnableThumbResize)
    {
	   Cursor = crHSplit;
	   return;
    }

  Cursor = crDefault;
}

//-------------------MouseMove--------------------John Mertus----May 2003-----

   void __fastcall TTimelineTrackBar::MouseMove(Classes::TShiftState Shift, int X, int Y)

//  Process the Mouse movement events
//
//****************************************************************************
{
////JAM  TCustomControl::MouseMove(Shift,X,Y);

  // See if this is a special drag
  if (_SpecialDrag)
   {
     if (_SpecialDragX <= -1000) _SpecialDragX = X;
     if (_SpecialDragEvent != NULL) _SpecialDragEvent(this, X - _SpecialDragX);
     return;
   }

  // Nothing to do if _Max == _Min
  if (_Max == _Min) return;

  // Check if we are dragging something
  // Check the drag
  switch (_CurrentActiveDrag)
    {
       case adNONE:
         break;                 // Do nothing if no dragging

       case adTHUMB:            // Drage the thumb
         ThumbMouseMove(Shift, X, Y);
         break;

       default:                 // All other cursors
         CursorMouseMove(Shift, X, Y);
         break;
     }
}

//--------------------MouseUp---------------------John Mertus----May 2003-----

   void __fastcall TTimelineTrackBar::MouseUp(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Process the Mouse up event
//
//****************************************************************************
{
////JAM  TCustomControl::MouseUp(Button,Shift,X,Y);

  // See if this is a special drag
  if (_SpecialDrag)
   {
     _SpecialDragX = -10000;
     if (_SpecialDragEvent != NULL) _SpecialDragEvent(this, 0);
     return;
   }

  if (Button == mbLeft)
    {
        _Tracking = false;
        _ThumbResizing = RESIZING_NOT;
        ExtTimer->Enabled = _IgnoreExPosition;
    }

  if (Button == mbMiddle)
    {
       _Tracking = false;
    }
}

//---------------------WMSize---------------------John Mertus----May 2003-----

    void __fastcall TTimelineTrackBar::WMSize(TWMSize &Message)

//  Called when the size changes
//
//****************************************************************************
{
  UpdateTrackData();
  UpdateThumbData();
}

//------------------DoMouseWheel------------------John Mertus----May 2003-----

  bool __fastcall TTimelineTrackBar::DoMouseWheel(TShiftState Shift,
      int WheelDelta, const TPoint &MousePos)

//  Process the mouse wheel commands
//
//****************************************************************************
{
  // Do Nothing if wheel is not enabled
  if (!EnableWheel) return false;
  
  // Do nothing on a degenerate trackbar
  if (_Max == _Min) return true;

  PositionInternal(Position() + _LargeChange*WheelDelta/WHEEL_DELTA/_FrameWidth);
  return true;
}

//-----------------SetThumbColor------------------John Mertus----May 2003-----

	 void __fastcall TTimelineTrackBar::SetThumbColor(TColor Value)

//  Change the thumb color
//
//****************************************************************************
{
   if (_ThumbColor == Value) return;
   _ThumbColor = Value;
   UpdateThumbData();
   Invalidate();
}

//---------------SetThumbRangeColor---------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::SetThumbRangeColor(TColor Value)

//  Change the thumb range color
//
//****************************************************************************
{
   if (_ThumbRangeColor == Value) return;
   _ThumbRangeColor = Value;
   UpdateThumbData();
   UpdateTrackData();
   Invalidate();
}

//---------------ExtTimerTimer-------------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::ExtTimerTimer(TObject *Sender)

//  Does an update if needed
//
//****************************************************************************
{
    _IgnoreExPosition = false;
    //  Replace this with a timer
    if (_UseExPostion)
     {
       _UseExPostion = false;
       // mbraca - 20041202 - I don't know what problem this timer is
       // trying to solve, but it sure messes up cursor dragging because
       // the cursor often warps to some random position after you're done
       // dragging it... So I commented out the following:
       //PositionInternal(_ExPosition);
     }
    ExtTimer->Enabled = false;
}

//------------------CMMouseLeave------------------John Mertus----May 2003-----

      void __fastcall TTimelineTrackBar::CMMouseLeave(TMessage &Message)

//  Does an update if needed
//
//****************************************************************************
{
  if (!IsDragging()) Cursor = crDefault;
}

//---------------PositionFromPixels---------------John Mertus----May 2003-----

      double TTimelineTrackBar::PositionFromPixels(int Pos)

//  Returns the track position in value from the pixel position
//    0 returns _Min
//    _UsuablePixels returns _Max
//
//****************************************************************************
{
  return  Pos/_FrameWidth + _Min;
}

//---------------PositionInPixels---------------John Mertus----May 2003-----

      int TTimelineTrackBar::PositionInPixels(void)

//  Returns the track position in Pixels
//    The leftmost position return 0
//    The Rightmost Postion return _UsablePixels
//
//****************************************************************************
{
  if (_Max == _Min) return 0;

  return  (Position() - _Min) * _FrameWidth;   // truncate?
}

//---------------PositionInPixels---------------John Mertus----May 2003-----

      int TTimelineTrackBar::PositionInPixels(double Value)

//  Returns the track position in Pixels
//    The leftmost position return 0
//    The Rightmost Postion return _UsuablePixels
//
//****************************************************************************
{
  if (_Max == _Min) return 0;

  return  (Value - _Min) * _FrameWidth;   // truncate ?
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

   bool TTimelineTrackBar::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes tthe settings for the timeline
//   ini is the ini file
//   IniSection is the section to write, the name of the trackbar
//   will be appended to this name
//
//  Return should be true
//
//******************************************************************************
{
   String name = ".";
   name = name + Name;
   string Section = IniSection + WCharToStdString(name.c_str());
// The PACKAGE export for XE<n> appears not to work correctly, so these are commente dout
// some solution is needed if these values are ever going to be changed from defaults
//   ini->WriteAssociation(ThumbStyleAssociations, Section, "ThumbStyle", ThumbStyle);
//   ini->WriteAssociation(TrackStyleAssociations, Section, "TrackStyle", TrackStyle);
//   ini->WriteAssociation(ActiveDragAssociations, Section, "ActiveDrag", ActiveDrag);
   ini->WriteInteger(Section, "ThumbColor", ThumbColor);
   ini->WriteInteger(Section, "TrackColor", TrackColor);
   ini->WriteInteger(Section, "ThumbRangeColor", ThumbRangeColor);
   ini->WriteInteger(Section, "DefaultCursorColor", DefaultCursorColor);
   ini->WriteInteger(Section, "ActiveCursor", ActiveCursor);

   // Write out cursor information
   // In reality the cursor should write its own self
   IntegerList ilColor;
   for (unsigned i=0; i < _Cursors.size(); i++)
     ilColor.push_back(_Cursors[i].Color);
   ini->WriteIntegerList(Section,"CursorColor", ilColor);
   
   ini->WriteBool(Section, "EnableThumbRange", EnableThumbRange);
   ini->WriteBool(Section, "ShowThumb", ShowThumb);
   ini->WriteBool(Section, "ShowThumbDetent", ShowThumbDetent);
   ini->WriteBool(Section, "ShowCursors", ShowCursors);
   return true;
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TTimelineTrackBar::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the settings for the timeline
//   ini is the ini file
//   IniSection is the section to write, the name of the trackbar
//   will be appended to this name
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
	return true;
//   String name = ".";
//   name = name + Name;
//   string Section = IniSection + WCharToStdString(name.c_str());
//   ThumbStyle = (TThumbStyle)ini->ReadAssociation(ThumbStyleAssociations, Section, "ThumbStyle", ThumbStyle);
//   TrackStyle = (TTrackStyle)ini->ReadAssociation(TrackStyleAssociations, Section, "TrackStyle", TrackStyle);
//   ActiveDrag = (TActiveDrag)ini->ReadAssociation(ActiveDragAssociations, Section, "ActiveDrag", ActiveDrag);
//   if (ActiveDrag != adBOTH)
//	 _CurrentActiveDrag = ActiveDrag;
//   else
//	 _CurrentActiveDrag = adTHUMB;
//
//   ThumbColor = (TColor)ini->ReadInteger(Section, "ThumbColor", ThumbColor);
//   TrackColor = (TColor)ini->ReadInteger(Section, "TrackColor", TrackColor);
//   ThumbRangeColor = (TColor)ini->ReadInteger(Section, "ThumbRangeColor", ThumbRangeColor);
//   DefaultCursorColor = (TColor)ini->ReadInteger(Section, "DefaultCursorColor", DefaultCursorColor);
//   ActiveCursor = ini->ReadInteger(Section, "ActiveCursor", ActiveCursor);
//
//   // Read the cursor list
//   IntegerList ilColor;
//   ilColor = ini->ReadIntegerList(Section,"CursorColor");
//   NumberOfCursors = ilColor.size();
//   for (unsigned i=0; i < ilColor.size(); i++)
//	 _Cursors[i].Color = (TColor)ilColor[i];
//
//   EnableThumbRange = ini->ReadBool(Section, "EnableThumbRange", EnableThumbRange);
//   ShowThumb = ini->ReadBool(Section, "ShowThumb", ShowThumb);
//   ShowThumbDetent = ini->ReadBool(Section, "ShowThumbDetent", ShowThumbDetent);
//   ShowCursors = ini->ReadBool(Section, "ShowCursors", ShowCursors);
//   return true;
}

//-------------------TrackRect--------------------John Mertus----Jan 2004-----

    TRect TTimelineTrackBar::TrackRect(void) const

//  returns the size of the track rectangle
//
//****************************************************************************
{
    return _TrackRect;
}

//---------------SetActiveCursor------------------John Mertus----Jan 2004-----

    void __fastcall TTimelineTrackBar::SetActiveCursor(int Value)

// Sets the active cursor
//
//****************************************************************************
{
   _Cursors.ActiveCursor(Value);
}

//---------------GetActiveCursor------------------John Mertus----Jan 2004-----

    int __fastcall TTimelineTrackBar::GetActiveCursor(void)

// Returns the active cursor
//
//****************************************************************************
{
   return _Cursors.ActiveCursor();
}

//---------------DefineProperties-----------------John Mertus----Jan 2004-----

   void __fastcall TTimelineTrackBar::DefineProperties(TFiler *Filer)

//  Because of a design bug in Borland, the Double properties can only have
//  default of 0, this is not good enough for us, so override them
//
//****************************************************************************

{
  // before we do anything, let the base class define its properties.
  TCustomControl::DefineProperties(Filer);
  Filer->DefineProperty("Minimum",NULL,MinimumWriterProc, true);
  Filer->DefineProperty("Maximum",NULL,MaximumWriterProc, true);
}

//------------MinimumWriterProc------------------John Mertus----Jan 2004-----

   void __fastcall TTimelineTrackBar::MinimumWriterProc(TWriter *Writer)

//  Helper function, this writes the minimum property value
//
//****************************************************************************
{
    Writer->WriteFloat(_Min);
}

//-----------------Cursors------------------------John Mertus----Jan 2004-----

    CTimelineCursors *TTimelineTrackBar::Cursors(void)

//  Just gets the address of the cursors for the timeline
//
//****************************************************************************
{
   return &_Cursors;
}

//------------MaximumWriterProc------------------John Mertus----Jan 2004-----

   void __fastcall TTimelineTrackBar::MaximumWriterProc(TWriter *Writer)

//  Helper function, this writes the maximum property value
//
//****************************************************************************
{
    Writer->WriteFloat(_Max);
}

//---------------Constructor------------------John Mertus----Jan 2004-----

   CTimelineCursor::CTimelineCursor(TColor tsDefaultColor)

//  Create the cursor.
//
//****************************************************************************
{
   Color = clNone;
   Position = 0;
   DefaultColor = tsDefaultColor;
   Style = CS_SOLID;
   FrameWidth = 0;
   // Label, being a string, is automatically is set to ""
}

//-----------------Destructor------------------John Mertus----Jan 2004-----

   CTimelineCursor::~CTimelineCursor(void)

//  Normal Destructor
//
//****************************************************************************
{
}

//--------------------Draw----------------------John Mertus----Feb 2004-----

   void CTimelineCursor::Draw(int x, int y, const TRect &Boundary, TCanvas *Canvas)

//  This is a very crude drawing method
//    x, y represent where to start the drawing
//    Boundary represents a rectangle that limits the drawing
//    Bitmap is the bitmap and how to draw
//
//   Both boundary and X,Y can be interperted by the Draw program to
//   draw the simple.
//
//****************************************************************************
{
  // For now, the only symbol is a line of one pixel
  // with label attached to lower right
  // Should save colors and drawing information but
  // we don't care for now.
    TColor penColor = DefaultColor;
    if (Color != clNone)
      penColor = Color;
    Canvas->Pen->Color = penColor;

    int iNumElem, iStart;

    // short-circuit if cursor totally clipped
    int iRightBound = Boundary.Right + ((Style == CS_3_WIDE)? 2 : 0);
    int iLeftBound = Boundary.Left - ((Style == CS_3_WIDE)? 2 : 0);
    if ((x < iLeftBound) || (x > iRightBound)) return;

    switch (Style)
     {
      case CS_DASHED_1_2_1:
        // this makes lines that look like:  "-  -"
	// pixels.  

        iNumElem = (Boundary.Bottom - Boundary.Top) / 4;
        iStart = ( (Boundary.Bottom - Boundary.Top) - (iNumElem * 4) ) / 2;
        iStart += Boundary.Top;
        for (int iElem = 0; iElem < iNumElem; iElem++)
         {
          // first pixel
          Canvas->MoveTo (x, iStart);
          Canvas->LineTo (x, iStart+1);
 
          // last pixel
          Canvas->MoveTo (x, iStart+3);
          Canvas->LineTo (x, iStart+4);

          iStart += 4;
         }
      break;
      case CS_DASHED_2_1_2:
	// this makes lines that look like:  "-- --"
        iNumElem = (Boundary.Bottom - Boundary.Top) / 5;
        iStart = ( (Boundary.Bottom - Boundary.Top) - (iNumElem * 5) ) / 2;
        iStart += Boundary.Top;
        for (int iElem = 0; iElem < iNumElem; iElem++)
         {
          // first two pixels
          Canvas->MoveTo (x, iStart);
          Canvas->LineTo (x, iStart+2);

          // last two pixels
          Canvas->MoveTo (x, iStart+3);
          Canvas->LineTo (x, iStart+5);

          iStart += 5;
		 }
	  break;
	  case CS_3_WIDE:
		// three pixels wide, line-space-line; we try a cheap way to make
		// the lines look translucent - should figure out how to do it right
		Canvas->Pen->Mode = pmMask;
		Canvas->Pen->Color = clTrackShadow;
		// clip to the boundary rectangle
		if (x >= (Boundary.Left + 1))
		  {
			Canvas->MoveTo(x - 1,Boundary.Top);
			Canvas->LineTo(x - 1,Boundary.Bottom + 1);   // bottom is inclusive
		  }
		if (x <= (Boundary.Right - 1))
		  {
			Canvas->MoveTo(x + 1,Boundary.Top);
			Canvas->LineTo(x + 1,Boundary.Bottom + 1);
		  }

			Canvas->Pen->Mode = pmMerge;
			Canvas->Pen->Color = penColor;
		if (x >= (Boundary.Left + 1))
		  {
			Canvas->MoveTo(x - 1,Boundary.Top);
			Canvas->LineTo(x - 1,Boundary.Bottom + 1);
		  }
		if (x <= (Boundary.Right - 1))
		  {
			Canvas->MoveTo(x + 1,Boundary.Top);
			Canvas->LineTo(x + 1,Boundary.Bottom + 1);
		  }

		Canvas->Pen->Mode = pmCopy;
	  break;

	  case CS_5_WIDE:
		// five pixels wide, 2line-space-2line; we try a cheap way to make
		// the lines look translucent - should figure out how to do it right
		Canvas->Pen->Mode = pmMask;
		// clip to the boundary rectangle
		if (x >= (Boundary.Left + 2))
		  {
			Canvas->MoveTo(x - 2,Boundary.Top);
			Canvas->LineTo(x - 2,Boundary.Bottom + 1);   // bottom is inclusive
		  }
		if (x >= (Boundary.Left + 1))
		  {
			Canvas->MoveTo(x - 1,Boundary.Top);
			Canvas->LineTo(x - 1,Boundary.Bottom + 1);   // bottom is inclusive
		  }
		if (x <= (Boundary.Right - 1))
		  {
			Canvas->MoveTo(x + 1,Boundary.Top);
			Canvas->LineTo(x + 1,Boundary.Bottom + 1);
		  }

		if (x <= (Boundary.Right - 2))
		  {
			Canvas->MoveTo(x + 2,Boundary.Top);
			Canvas->LineTo(x + 2,Boundary.Bottom + 1);
		  }

		Canvas->Pen->Mode = pmMerge;
		Canvas->Pen->Color = penColor;

		if (x >= (Boundary.Left + 1))
		  {
			Canvas->MoveTo(x - 1,Boundary.Top);
			Canvas->LineTo(x - 1,Boundary.Bottom + 1);
		  }
		if (x <= (Boundary.Right - 1))
		  {
			Canvas->MoveTo(x + 1,Boundary.Top);
			Canvas->LineTo(x + 1,Boundary.Bottom + 1);
		  }

		if (x >= (Boundary.Left + 2))
		  {
			Canvas->MoveTo(x - 2,Boundary.Top);
			Canvas->LineTo(x - 2,Boundary.Bottom + 1);
		  }
		if (x <= (Boundary.Right - 2))
		  {
			Canvas->MoveTo(x + 2,Boundary.Top);
			Canvas->LineTo(x + 2,Boundary.Bottom + 1);
		  }
		Canvas->Pen->Mode = pmCopy;
	  break;
      case CS_SOLID:
      default:
        Canvas->MoveTo(x,Boundary.Top);
        Canvas->LineTo(x,Boundary.Bottom);
     }

#if 0 // doesn't quite work yet
     if (FrameWidth > 4)        // sanity
       {
	   // tShow end of frame with a 2-2 dashed line:  "--  "
         int x2 = x + FrameWidth - 1;
         iNumElem = (Boundary.Bottom - Boundary.Top) / 4;
         iStart = ( (Boundary.Bottom - Boundary.Top) - (iNumElem * 4) ) / 2;
         iStart += Boundary.Top;
         for (int iElem = 0; iElem < iNumElem; iElem++)
           {
             // draw two pixels
             Canvas->MoveTo (x2, iStart);
             Canvas->LineTo (x2, iStart+2);

             iStart += 4;
           }
       }
#endif

    // Draw the Label
    if (Label != "")
      {
         TBrushStyle bs = Canvas->Brush->Style;
         Canvas->Brush->Style = bsClear;
         Canvas->TextOut(x+2, Boundary.Bottom - Canvas->TextHeight(Label.c_str()) - 1, Label.c_str());
         Canvas->Brush->Style = bs;
      }
}

//--------------------Draw----------------------John Mertus----Feb 2004-----

   void CTimelineCursor::Draw(int x, int y, const TRect &Boundary, Graphics::TBitmap* Bitmap)

//  This is a very crude drawing method
//    x, y represent where to start the drawing
//    Boundary represents a rectangle that limits the drawing
//    Bitmap is the bitmap and how to draw
//
//   Both boundary and X,Y can be interperted by the Draw program to
//   draw the simple.
//
//****************************************************************************
{
    Draw(x, y, Boundary, Bitmap->Canvas);
}

//--------------------Draw----------------------John Mertus----Feb 2004-----

   void CTimelineCursor::Draw(int x, int y,  Graphics::TBitmap* Bitmap)

//  This calls the other draw with the entire rectangle
//
//****************************************************************************
{
   TRect Boundary(0,0,Bitmap->Width, Bitmap->Height);
   Draw(x, y, Boundary, Bitmap);
}

//--------------------ActiveCursor-----------------John Mertus----Apr 2013-----

	CTimelineCursors::CTimelineCursors(void)

//  Moved to code, this controls the active cursor value
//
//****************************************************************************
{
   _ActiveCursor = 0;
}

//--------------------ActiveCursor-----------------John Mertus----Apr 2013-----

	void CTimelineCursors::ActiveCursor(int Value)

//  Moved to code, this controls the active cursor value
//
//****************************************************************************
{
   _ActiveCursor = Value;
}

//-------------------ActiveCursor------------------John Mertus----Apr 2013-----

	int CTimelineCursors::ActiveCursor(void)

//  Moved to code, this controls the active cursor value
//
//****************************************************************************
{
	return _ActiveCursor;
}
