//---------------------------------------------------------------------------

#ifndef VTimeCodeHistoryH
#define VTimeCodeHistoryH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <Mask.hpp>
#include <StdCtrls.hpp>
#include "pTimecode.h"
#include "IniFile.h"
#include "SharedMem.h"

#define DEFAULT_VTR -1

//---------------------------------------------------------------------------
class PACKAGE VTimeCodeHistory : public TCustomComboBox
{
private:

   // Common to VTimecodeHistory and VTimecodeEdit

   TNotifyEvent _OnTimecodeChange;
   TNotifyEvent _OnExit;
   TNotifyEvent _OnEnter;
   TNotifyEvent _OnEnterKey;

   DEFINE_CBHOOK(TCChanged, VTimeCodeHistory);
   void CreateHint(void);
   bool _AllowVTRCopy;
   bool _InternalHint;
   String _AttachToName;

   TPopupMenu *_PopupMenu;

   void __fastcall setAllowNegative(bool Value) {tcP.setAllowNegative(Value);}
   bool __fastcall getAllowNegative(void) { return tcP.getAllowNegative(); }
   void __fastcall setSign(int Value) { tcP.setSign(Value);}
   int  __fastcall getSign(void) {return tcP.getSign();}

   void __fastcall SetTheHint(const String Value);
   String __fastcall GetTheHint(void);

   void __fastcall SetAllowVTRCopy(const bool Value);

   void CreatePopUp(void);
   void CreateSelectPopup(void);

   // PopupMenu callbacks
   void __fastcall PopupCopyFromVTR(TObject *Sender);
   void __fastcall PopupCopyToVTR(TObject *Sender);
   void __fastcall PopupCopy(TObject *Sender);
   void __fastcall PopupCut(TObject *Sender);
   void __fastcall PopupPaste(TObject *Sender);
   void __fastcall PopupUndo(TObject *Sender);
   void __fastcall PopupProperties(TObject *Sender);
   void __fastcall PopupSelectAll(TObject *Sender);
   void __fastcall PopupVTRChosen(TObject *Sender);
   void __fastcall PopupClick(TObject *Sender);
   void __fastcall VFocusExit(TObject *Sender);
   void __fastcall VFocusEnter(TObject *Sender);

   // just for reference
   TMenuItem *CopyFromVTRMenuItem;
   TMenuItem *CopyToVTRMenuItem;
   TMenuItem *CutMenuItem;
   TMenuItem *CopyMenuItem;
   TMenuItem *PasteMenuItem;
   TMenuItem *UndoMenuItem;
   TMenuItem *SelectAllMenuItem;
   TMenuItem *PropertiesMenuItem;
   TMenuItem *SelectVTRMenuItem;
   CVTRSharedMemoryList  VTRList;
   AnsiString strOrigText;

   // Special to Combbox only
   void __fastcall DropDownCallback(TObject *Sender);
   void __fastcall SelectCallback(TObject *Sender);
   void __fastcall SetMaxSizeMRU(int Value);
   int  __fastcall GetMaxSizeMRU(void);
   bool _VTRCopyShown;
   bool _ShowVTRCopy;
   bool _ReadOnly;
   bool _EnterOnESC;

protected:
	void DYNAMIC __fastcall DoEnter(void);
	void DYNAMIC __fastcall DoExit(void);

public:
   // Special to Combobox
   __fastcall virtual VTimeCodeHistory(Classes::TComponent* AOwner);
   CMRU mru;

   void *getEditHandle ();

   // Common to VTimecodeHistory and VTimecodeEdit
   PTimecode tcP;
   void UpdateDisplay(void);

   void __fastcall VKeyPress(TObject *Sender, WideChar &Key);
   void __fastcall VExit(TObject *Sender);

   bool CopyFromVTR(int vtr = DEFAULT_VTR);
   bool CopyFromVTRAvailable(void);
   bool CueToVTR(int vtr = DEFAULT_VTR);
   bool CueFromVTRAvailable(void);

    // I/O operators
   bool ReadSettings(CIniFile *ini, const string &section);
   bool WriteSettings(CIniFile *ini, const string &section);

   __property bool InternalHint = {read=_InternalHint, write=_InternalHint, default=true};
   __property int Sign = {read=getSign, write=setSign, default=1};

__published:

  // These are the properties we wish to expose to the designer
   // Common to VTimecodeHistory and VTimecodeEdit

  __property Anchors;
  __property BevelEdges;
  __property BevelInner;
  __property BevelKind;
  __property BevelOuter;
  __property Color;
  __property Constraints;
  __property Cursor;
  __property DragCursor;
  __property DragKind;
  __property DragMode;
  __property Enabled;
  __property Font;
  __property Height;
  __property HelpContext;
  __property HelpKeyword;
  __property HelpType;
  __property Left;
  __property ParentColor;
  __property ParentFont;
  __property ParentShowHint;
  __property ShowHint;
  __property TabOrder;
  __property TabStop;
  __property Tag;
  __property Top;
  __property Visible;
  __property Width;

  // Overridden and additional properties
  __property bool AllowNegative = {read=getAllowNegative, write=setAllowNegative, default=false};
  __property bool AllowVTRCopy = {read=_AllowVTRCopy, write=SetAllowVTRCopy, default=true};
  __property AnsiString Hint = {read=GetTheHint, write=SetTheHint};

  __property TNotifyEvent OnTimecodeChange = {read=_OnTimecodeChange, write=_OnTimecodeChange};
  __property TNotifyEvent OnExit = {read=_OnExit, write=_OnExit};
  __property TNotifyEvent OnEnter = {read=_OnEnter, write=_OnEnter};
  __property TNotifyEvent OnEnterKey = {read=_OnEnterKey, write=_OnEnterKey};
  __property String AttachToName = {read=_AttachToName, write=_AttachToName};

  __property OnClick;
  __property OnDblClick;
  __property OnDragDrop;
  __property OnDragOver;
  __property OnEndDock;
  __property OnEndDrag;
  __property OnStartDock;
  __property OnStartDrag;

  // Specific to VTimecodeHistory
  __property bool ShowVTRCopy = {read=_ShowVTRCopy, write=_ShowVTRCopy, default=false};
  __property int DropDownCount = {read=GetMaxSizeMRU, write=SetMaxSizeMRU, default=10};
  __property ItemIndex;
  __property bool ReadOnly = {read=_ReadOnly, write=_ReadOnly, default=false};
  __property bool EnterOnESC = {read=_EnterOnESC, write=_EnterOnESC, default=false};

};

//---------------------------------------------------------------------------
#endif
