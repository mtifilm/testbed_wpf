//---------------------------------------------------------------------------

#ifndef ColorSplitterH
#define ColorSplitterH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TColorSplitter : public TSplitter
{
private:
protected:
////	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);

public:
   __fastcall TColorSplitter(TComponent* Owner);
__published:
};
//---------------------------------------------------------------------------
#endif
