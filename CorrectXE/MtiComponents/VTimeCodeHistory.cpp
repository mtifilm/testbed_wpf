//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "VTimeCodeHistory.h"
#include <clipbrd.hpp>

#pragma resource "*.res"
#pragma package(smart_init)
//
//  The logic for a VTimeCode is
//     VTimeCode is a visual component and has a PTimecode tcP, attached
//     to the property.
//      Changing this tc updates the display, or changing the display
//     update tcP
//     When the tcP changes, the tcP.notify() is called and the
//     Onchanged handler is called.
//
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

#include "SharedMem.h"

#define FROM_VTR_STRING "VTR Timecode"
#define CUE_VTR_STRING "Cue to Timecode"

static inline void ValidCtrCheck(VTimeCodeHistory *)
{
      new VTimeCodeHistory(NULL);
}

//---------------------------------------------------------------------------
__fastcall VTimeCodeHistory::VTimeCodeHistory(TComponent* Owner)
        : TCustomComboBox(Owner), tcP(DROPFRAME,24)
{
   Width = 85;
   Ctl3D = false;
   ParentCtl3D = false;
   _ReadOnly = false;
   _EnterOnESC = false;
   _ShowVTRCopy = false;
   DropDownCount = 5;
   OnDropDown = DropDownCallback;
   OnSelect = SelectCallback;

   // Common to VTimecodeHistory and VTimecodeEdit
   tcP = "1 0 0 0";
   _AllowVTRCopy = true;
   _InternalHint = true;
   _OnExit = NULL;
   _OnEnter = NULL;
   _OnEnterKey = NULL;

   // Do the key processing
   OnKeyPress = VKeyPress;

   _PopupMenu = NULL;
   CreatePopUp();

   // What happens on change
   UpdateDisplay();
   SET_CBHOOK(TCChanged, tcP);
   tcP.InvalidateData();
}

//---------------------------------------------------------------------------
namespace Vtimecodehistory
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(VTimeCodeHistory)};
                 RegisterComponents("MTI", classes, 0);
        }
}

//---------------------VKeyPress----------------------John Mertus---Jan 2001---


   void __fastcall VTimeCodeHistory::VKeyPress(TObject *Sender, WideChar &Key)

//  Allow only certain keys
//
//******************************************************************************
{
  // Drop all keys on write only
  if (_ReadOnly)
    {
       Key = 0;
       return;
    }

  // All these keys are processed normally
  if (isdigit(Key)) return;
  if (Key == ':') return;
  if (Key == '\b') return;
  if (Key == ' ') return;
  if (Key == '+') return;
  if (Key == '-') return;
  if (Key == '.') return;
  // Process Escape
  if (Key == 0x1B)
    {
      Text = strOrigText;
      if (_EnterOnESC)
       {
        Key = '\r';
       }
      else
       {
        SelectAll ();
        return;
       }
    }

  // Return processes the data
  if (Key == '\r')
    {
      Key = 0;
      tcP.InhibitCallbacks();               // Force an change call
      VExit(Sender);                        // Parse it
      tcP.Notify();
      tcP.UnInhibitCallbacks();            // But make it only one
      if (_OnEnterKey != NULL)
       {
         SelLength = 0;
         _OnEnterKey(Sender);
       }
      return;
    }

  Key = 0;
  Beep();
}

//---------------------VFocusEnter---------------------John Mertus---Jan 2004---

    void __fastcall VTimeCodeHistory::VFocusEnter(TObject *Sender)

//  Called when the focus is placed on the control
//
//******************************************************************************
{
  strOrigText = Text;
  if (_OnEnter != NULL) _OnEnter(Sender);
}

//---------------------VFocusExit---------------------John Mertus---Jan 2004---

    void __fastcall VTimeCodeHistory::VFocusExit(TObject *Sender)

//  Called when the focus is lost on the control
//
//******************************************************************************
{
    VExit(Sender);
    if (_OnExit != NULL) _OnExit(Sender);
}

//---------------------VExit-------------------------John Mertus---Jan 2001---

    void __fastcall VTimeCodeHistory::VExit(TObject *Sender)

//  Called when the focus is lost on the component
//
//******************************************************************************
{
    if (!tcP.ConformTimeASCII(StringToStdString(Text)))
      ; ////_MTIErrorDialog(Handle, theError.getFmtError());
    else
      mru.push(tcP);

    UpdateDisplay();
}

//------------------SetAllowVTRCopy--------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::SetAllowVTRCopy(const bool Value)

//  This setter turns of the internal hint automatically
//
//****************************************************************************
{
   if (Value == _AllowVTRCopy) return;
   _AllowVTRCopy = Value;
   CreatePopUp();
}

//-------------------SetTheHint-------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::SetTheHint(const String Value)

//  This setter turns of the internal hint automatically
//
//****************************************************************************
{
   TCustomComboBox::Hint = Value;
   _InternalHint = false;
}

//-------------------GetTheHint-------------------John Mertus----Oct 2003-----

     String __fastcall VTimeCodeHistory::GetTheHint(void)

//  Usual getter since properties cannot be nested inside properties
//
//****************************************************************************
{
   return TCustomComboBox::Hint;
}

//--------------CreateHint-------------------------John Mertus---Jan 2001---

   void VTimeCodeHistory::CreateHint(void)
//
//  This creates the hint for the system
//
//******************************************************************************
{
   // Don't create the hint if external
   if (!_InternalHint) return;

   // Don't create it if designing
   if (ComponentState.Contains(csDesigning)) return;

   String str = (String)tcP.getFramesPerSecond() + " FPS, ";
   if (tcP.isDropFrame()) str = str + "Drop Frame"; else str = str + "Non-Drop Frame";
   if (tcP.isColorFrame()) str = str + ", Color Frame";
   if (tcP.isHalfFrame()) str = str + ", Half Frame";
   if (tcP.is720P()) str = str + ", 720P";
   str = str + ", Frames: " + (String)tcP.Frame;

   if (VTRList.NumberOfVTRs() > 0)
     {
        int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
        VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
        if (VTRInfo != NULL)
          str = str + "\r" + VTRInfo->Name;
        VTRList.ReleaseVTR(nV);
     }

   TCustomComboBox::Hint = str;
}

//--------------UpdateDisplay-------------------------John Mertus---Jan 2001---

   void VTimeCodeHistory::UpdateDisplay(void)
//
//  This just takes the data memeber and displays it
//
//******************************************************************************
{
    CreateHint();
    Text = ((string)tcP).c_str();
}

void *VTimeCodeHistory::getEditHandle()
{
  return FEditHandle;
}

//-------------------TCChanged--------------------John Mertus----Oct 2003-----

  void VTimeCodeHistory::TCChanged(void *p)

//  Come here when timecode has changed, update the display
//  and the MRU list.
//
//****************************************************************************
{
  UpdateDisplay();
  if (_OnTimecodeChange != NULL) _OnTimecodeChange(this);
}

//------------------ReadSettings------------------John Mertus----Oct 2003-----

  bool VTimeCodeHistory::ReadSettings(CIniFile *ini, const string &section)

//  Usual read setting property
//
//****************************************************************************
{
   return mru.ReadSettings(ini, section, StringToStdString(Name+"MRU"));
}

//-----------------WriteSettings------------------John Mertus----Oct 2003-----

  bool VTimeCodeHistory::WriteSettings(CIniFile *ini, const string &section)

//  Usual write setting property
//
//****************************************************************************
{
   return mru.WriteSettings(ini, section, StringToStdString(Name+"MRU"));
}


//----------------CopyFromVTR--------------------John Mertus-----Sep 2003---

   bool VTimeCodeHistory::CopyFromVTR(int vtr)

//  Call this to copy a timecode from a VTR to the component
//   vtr is the number of the vtr, only DEFAULT_VTR for now
//   Return is TRUE if it copies
//   false if no VTR available
//
//***************************************************************************
{
     // See if any VTRs exist
     if (VTRList.NumberOfVTRs() == 0) return false;

     int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
     VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
     if (VTRInfo == NULL) return false;

     PTimecode ptc(VTRInfo->tcVTR);
     VTRList.ReleaseVTR(nV);

     tcP.setTime(ptc);
     mru.push(tcP);
     if (_VTRCopyShown)
       {
         DropDownCallback(this);
         ItemIndex = 1;
       }
     return true;
}

//----------------CueToVTR-----------------------John Mertus-----Sep 2003---

   bool VTimeCodeHistory::CueToVTR(int vtr)

//  Call this to copy a timecode code to the VTR
//   vtr is the number of the vtr, only DEFAULT_VTR for now
//   Return is TRUE if it copies
//   false if no VTR available
//
//***************************************************************************
{
     // See if any VTRs exist
     if (VTRList.NumberOfVTRs() == 0) return false;

     int nV = VTRList.IndexByAttachToID(StringToStdString(AttachToName));
     VTRSharedMemory *VTRInfo = VTRList.SelectVTR(nV);
     if (VTRInfo == NULL) return false;
     VTRSharedMemory_Cue(VTRInfo, this->tcP);
     VTRList.ReleaseVTR(nV);

     return true;
}


//----------------PopupCopyFromVTR----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupCopyFromVTR(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   CopyFromVTR();
}

//-----------------PopupCopyToVTR-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupCopyToVTR(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   CueToVTR();
}

//-------------------PopupCopy--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupCopy(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_COPY, 0, (NativeInt)0);
}

//-------------------PopupPaste-------------------John Mertus----Oct 2003-----

	 void __fastcall VTimeCodeHistory::PopupPaste(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_PASTE, 0, (NativeInt)0);
}
//--------------------PopupCut--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupCut(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   Perform(WM_CUT, 0, (NativeInt)0);
}

//-------------------PopupUndo--------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupUndo(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
//    COMBOBOXINFO cbi;
//    cbi.cbSize = sizeof(cbi);
//    GetComboBoxInfo(Handle, &cbi);
//    SendMessage(cbi.hwndItem, WM_UNDO, 0, 0);

    HWND hWndChild = GetWindow(Handle, GW_CHILD);
    SendMessage(hWndChild, WM_UNDO, 0, 0);
}

//-----------------PopupSelectAll-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupSelectAll(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
   SelectAll();
}

//----------------PopupVTRChosen---------------John Mertus----Oct 2003-----

   void __fastcall  VTimeCodeHistory::PopupVTRChosen(TObject *Sender)

//  This sets the default vtr to the sender
//
//****************************************************************************
{
    TMenuItem *mi = (TMenuItem *)Sender;
    VTRList.DefaultIndex(mi->Tag);
}

//----------------CreateSelectPopup---------------John Mertus----Oct 2003-----

     void VTimeCodeHistory::CreateSelectPopup(void)

//  Builds the menu for the VTR choices
//
//****************************************************************************
{
  if (VTRList.DefaultIndex() < 0)
    {
      // This forces a default vtr to exist
      VTRList.SelectVTR();
      VTRList.ReleaseVTR();

    }
  // Build the vtrs
  if (SelectVTRMenuItem->Enabled)
   {
      // Clear all the tags
      SelectVTRMenuItem->Clear();

      // Loop through all the possible names
      for (int i=0; i < VTRList.NameIndexList()->Max; i++)
         {
           // See if there is a VTR out there
           VTRSharedMemory *vsm = VTRList.SelectVTR(i);
           if (vsm != NULL)
             {
               TMenuItem *mi = new TMenuItem(_PopupMenu);
               mi->Caption = vsm->Name;
               mi->OnClick = PopupVTRChosen;
               // Create the Name
               mi->Tag = i;
               mi->Checked = (i == VTRList.DefaultIndex());
               SelectVTRMenuItem->Add(mi);
               VTRList.ReleaseVTR(i);
             }
         }
    }

}

//-------------CopyFromVTRAvailable---------------John Mertus----Oct 2003-----

     bool VTimeCodeHistory::CopyFromVTRAvailable(void)

//  Just return if the VTimecode paste is available
//
//****************************************************************************
{
    bool Result = (VTRList.NumberOfVTRs() > 0);
    if (AttachToName != "")
      Result =  Result && (VTRList.IndexByAttachToID(StringToStdString(AttachToName)) >= 0);

    return Result;
}

//----------------CueFromVTRAvailable-------------John Mertus----Oct 2003-----

     bool VTimeCodeHistory::CueFromVTRAvailable(void)

//  Just return if the VTimecode cue function is available
//
//****************************************************************************
{
    bool Result = (VTRList.NumberOfVTRs() > 0);
    if (AttachToName != "")
      Result =  Result && (VTRList.IndexByAttachToID(StringToStdString(AttachToName)) >= 0);

    return Result;
}

//-------------------PopupClick-------------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupClick(TObject *Sender)

//  Enable/disable the different parts of the menu system
//
//****************************************************************************
{
   VExit(NULL);
   UndoMenuItem->Enabled = (!ReadOnly);
   CutMenuItem->Enabled = (SelLength > 0) && (!ReadOnly);
   CopyMenuItem->Enabled = (SelLength > 0);
   PasteMenuItem->Enabled = Clipboard()->HasFormat(CF_TEXT) && (!ReadOnly);

   // These don't exist if we can't copy
   if (_AllowVTRCopy)
     {
       // See if this VTimecode is restricted to one VTR
       bool bA = true;
       if (AttachToName != "") bA = (VTRList.IndexByAttachToID(StringToStdString(AttachToName)) >= 0);

       CopyToVTRMenuItem->Enabled = (VTRList.NumberOfVTRs() > 0) && bA;
       SelectVTRMenuItem->Enabled = CopyToVTRMenuItem->Enabled && (AttachToName == "");
       CopyFromVTRMenuItem->Enabled = CopyToVTRMenuItem->Enabled && (!ReadOnly);
       if (SelectVTRMenuItem->Enabled) CreateSelectPopup();
     }

   SelectAllMenuItem->Enabled = (SelLength != Text.Length());
   SetFocus();

}

//----------------PopupProperties-----------------John Mertus----Oct 2003-----

     void __fastcall VTimeCodeHistory::PopupProperties(TObject *Sender)

//  Just translate the message down, must be an easier way
//
//****************************************************************************
{
    ////_MTIInformationDialog(Handle, tcP.DumpString());
}

//------------------CreatePopUp-------------------John Mertus----Oct 2003-----

  void VTimeCodeHistory::CreatePopUp(void)

//  This creates the popup menu, only needs to be done once
//
//****************************************************************************
{
   // Create the popup menu
   delete _PopupMenu;

   _PopupMenu = new TPopupMenu(this);
   _PopupMenu->OnPopup = PopupClick;

   UndoMenuItem = new TMenuItem(_PopupMenu);
   UndoMenuItem->Caption = "Undo";
   UndoMenuItem->ShortCut = ShortCut(Word('Z'), TShiftState() << ssCtrl);
   UndoMenuItem->OnClick = PopupUndo;
   _PopupMenu->Items->Add(UndoMenuItem);

   TMenuItem *mi = new TMenuItem(_PopupMenu);
   mi->Caption = "-";
   _PopupMenu->Items->Add(mi);

   CutMenuItem = new TMenuItem(_PopupMenu);
   CutMenuItem->Caption = "Cut";
   CutMenuItem->ShortCut = ShortCut(Word('X'), TShiftState() << ssCtrl);
   CutMenuItem->OnClick = PopupCut;
   _PopupMenu->Items->Add(CutMenuItem);

   CopyMenuItem = new TMenuItem(_PopupMenu);
   CopyMenuItem->Caption = "Copy";
   CopyMenuItem->ShortCut = ShortCut(Word('C'), TShiftState() << ssCtrl);
   CopyMenuItem->OnClick = PopupCopy;
   _PopupMenu->Items->Add(CopyMenuItem);

   PasteMenuItem = new TMenuItem(_PopupMenu);
   PasteMenuItem->Caption = "Paste";
   PasteMenuItem->ShortCut = ShortCut(Word('V'), TShiftState() << ssCtrl);
   PasteMenuItem->OnClick = PopupPaste;
   _PopupMenu->Items->Add(PasteMenuItem);

   mi = new TMenuItem(_PopupMenu);
   mi->Caption = "-";
   _PopupMenu->Items->Add(mi);

   SelectAllMenuItem = new TMenuItem(_PopupMenu);
   SelectAllMenuItem->Caption = "Select All";
   SelectAllMenuItem->ShortCut = ShortCut(Word('A'), TShiftState() << ssCtrl);
   SelectAllMenuItem->OnClick = PopupSelectAll;
   _PopupMenu->Items->Add(SelectAllMenuItem);

   if (_AllowVTRCopy)
     {

       mi = new TMenuItem(_PopupMenu);
       mi->Caption = "-";
       _PopupMenu->Items->Add(mi);

       CopyFromVTRMenuItem = new TMenuItem(_PopupMenu);
       CopyFromVTRMenuItem->Caption = "Paste from VTR";
       CopyFromVTRMenuItem->OnClick = PopupCopyFromVTR;
       CopyFromVTRMenuItem->ShortCut = ShortCut(Word('V'), TShiftState() << ssCtrl << ssShift);
       _PopupMenu->Items->Add(CopyFromVTRMenuItem);

       CopyToVTRMenuItem = new TMenuItem(_PopupMenu);
       CopyToVTRMenuItem->Caption = "Cue VTR";
       CopyToVTRMenuItem->OnClick = PopupCopyToVTR;
       CopyToVTRMenuItem->ShortCut = ShortCut(Word('Q'), TShiftState() << ssCtrl << ssShift);
       _PopupMenu->Items->Add(CopyToVTRMenuItem);

       SelectVTRMenuItem = new TMenuItem(_PopupMenu);
       SelectVTRMenuItem->Caption = "Select VTR";
       _PopupMenu->Items->Add(SelectVTRMenuItem);

     }

   mi = new TMenuItem(_PopupMenu);
   mi->Caption = "-";
   _PopupMenu->Items->Add(mi);

   PropertiesMenuItem = new TMenuItem(_PopupMenu);
   PropertiesMenuItem->Caption = "Properties...";
   PropertiesMenuItem->OnClick = PopupProperties;
   _PopupMenu->Items->Add(PropertiesMenuItem);

   PopupMenu = _PopupMenu;
}

//********************Specific to Combobox procedures

//----------------DropDownCallback----------------John Mertus----Oct 2003-----

  void __fastcall VTimeCodeHistory::DropDownCallback(TObject *Sender)

//  Call before the dropdown, add the VTR string if necessary
//
//****************************************************************************
{
   Items->Clear();

   _VTRCopyShown = _ShowVTRCopy && (VTRList.NumberOfVTRs() > 0) && _AllowVTRCopy;
   if (_VTRCopyShown) Items->Add(FROM_VTR_STRING);
   for (unsigned i=0; i < mru.size(); i++)
     Items->Add(mru[i].c_str());
}

//----------------SelectCallback----------------John Mertus----Oct 2003-----

   void __fastcall VTimeCodeHistory::SelectCallback(TObject *Sender)

//  Called when the user selects an item
//
//****************************************************************************
{
   if (Text == FROM_VTR_STRING)
     {
        CopyFromVTR();
        return;
     }
   VExit(Sender);
}

//-----------------GetMaxSizeMRU------------------John Mertus----Oct 2003-----

  int __fastcall VTimeCodeHistory::GetMaxSizeMRU(void)

//  Return the drop down list size
//
//****************************************************************************
{
  return mru.MaxSize();
}

//-----------------SetMaxSizeMRU------------------John Mertus----Oct 2003-----

  void __fastcall VTimeCodeHistory::SetMaxSizeMRU(int Value)

//  Set the drop down list size
//
//****************************************************************************
{
  if (Value <= 0) return;
  if (DropDownCount != Value)
  {
     DropDownCount = mru.MaxSize(Value);
  }

}

//---------------------DoEnter-----------------------John Mertus---Apr 2013---

	void __fastcall VTimeCodeHistory::DoEnter(void)

//  Called when enter with focus.
//  Required because 64 bits doesn't allow OnEnter to be set.
//
//*****************************************************************************
{
		this->VFocusEnter(this);
}

//---------------------DoExit------------------------John Mertus---Apr 2013---

	void __fastcall VTimeCodeHistory::DoExit(void)

//  Called when focus is lost
//  Required because 64 bits doesn't allow OnExit to be set.
//
//*****************************************************************************
{
		this->VFocusExit(this);
}
