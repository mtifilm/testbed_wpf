//---------------------------------------------------------------------------

#ifndef TestComponentUnitH
#define TestComponentUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MTIBusy.h"
#include "HistoryMenu.h"
#include <Vcl.Menus.hpp>
#include "GrayComboBox.h"
#include "MTIDBMeter.h"
#include <Vcl.ExtCtrls.hpp>
#include "VTimeCodeEdit.h"
#include "MTIProgressBar.h"
#include "MTITrackBar.h"
#include <Vcl.ComCtrls.hpp>
#include "PopupComboBox.h"
#include "TimelineTrackBar.h"
#include "IconListBox.h"
#include <Vcl.ImgList.hpp>
#include "VTimeCodeHistory.h"
#include "TriTimecode.h"
#include "VDoubleEdit.h"
#include <Vcl.Grids.hpp>
#include "Knob.h"

//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TMTIBusy *MtiBusy;
	TButton *Button1;
	THistoryMenu *HistoryMenu1;
	TGrayComboBox *GrayComboBox1;
	TMTIDBMeter *MTIDBMeter1;
	TTimer *Timer1;
	VTimeCodeEdit *VTimeCodeEdit1;
	TMTITrackBar *MTITrackBar1;
	TMTIProgressBar *MTIProgressBar1;
	TPopupComboBox *PopupComboBox1;
	TMainMenu *MainMenu1;
	TMenuItem *File1;
	TMenuItem *Open1;
	TMenuItem *Close1;
	TMenuItem *Exit1;
	TMenuItem *EditMenu1;
	TMenuItem *Cut1;
	TMenuItem *Cut2;
	TMenuItem *Copy1;
	TPopupMenu *PopupMenu1;
	TMenuItem *Copy2;
	TMenuItem *Cut3;
	TMenuItem *Paste1;
	TTimelineTrackBar *TimelineTrackBar1;
	TIconListBox *IconListBox1;
	TImageList *ImageList1;
	VTimeCodeHistory *VTimeCodeHistory1;
	TTriTimecode *TriTimecode1;
	VDoubleEdit *VDoubleEdit1;
	TVideoStoreUsage *VideoStoreUsage1;
	TKnob *Knob1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall GrayComboBox1Change(TObject *Sender);
	void __fastcall MTITrackBar1Change(TObject *Sender);
	void __fastcall Knob1Change(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
