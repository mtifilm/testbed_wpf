//---------------------------------------------------------------------------

#ifndef IconListBoxH
#define IconListBoxH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
class ObjectData : public TObject
{
  public:
    Graphics::TBitmap *Bitmap;
    void *p;
    ObjectData(void)
      {
         Bitmap = NULL;
         p = NULL;
      };

};


class PACKAGE TIconListBox : public TCustomListBox
{
private:
   Imglist::TCustomImageList* FImages;
   void __fastcall SetImages(Imglist::TCustomImageList* Value);

protected:
public:
	  __fastcall TIconListBox(TComponent* Owner);

	  void __fastcall ListBoxDrawItem(TWinControl *Control, int Index, const TRect &Rect, TOwnerDrawState State);
      void __fastcall SetBitmap(Graphics::TBitmap *bm, int Index);
      Graphics::TBitmap * __fastcall GetBitmap(int Index);
      void __fastcall SetData(void *op, int Index);
      void * __fastcall GetData(int Index);
      virtual void __fastcall Assign(TPersistent *Source);

__published:
	__property Align ;
	__property Anchors ;
	__property BiDiMode ;
	__property BorderStyle ;
	__property Color ;
	__property Columns ;
	__property Constraints ;
	__property Ctl3D ;
	__property DragCursor ;
	__property DragKind ;
	__property DragMode ;
	__property Enabled ;
	__property ExtendedSelect ;
	__property Font ;
	__property ImeMode ;
	__property ImeName ;
	__property IntegralHeight ;
	__property ItemHeight ;
	__property Items ;
	__property MultiSelect ;
	__property ParentBiDiMode ;
	__property ParentColor ;
	__property ParentCtl3D ;
	__property ParentFont ;
	__property ParentShowHint ;
	__property PopupMenu ;
	__property ShowHint ;
	__property Sorted ;
	__property TabOrder ;
	__property TabStop ;
	__property TabWidth ;
	__property Visible ;
	__property OnClick ;
	__property OnContextPopup ;
	__property OnDblClick ;
	__property OnDragDrop ;
	__property OnDragOver ;
	__property OnEndDock ;
	__property OnEndDrag ;
	__property OnEnter ;
	__property OnExit ;
	__property OnKeyDown ;
	__property OnKeyPress ;
	__property OnKeyUp ;
	__property OnMeasureItem ;
	__property OnMouseDown ;
	__property OnMouseMove ;
	__property OnMouseUp ;
	__property OnStartDock ;
	__property OnStartDrag ;

};
//---------------------------------------------------------------------------
#endif
