//---------------------------------------------------------------------------

#ifndef KnobH
#define KnobH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class PACKAGE TKnob : public TCustomControl
{
private:
	Graphics::TColor fLineColor;
	int fMin;
	int fMax;
	int fPosition;
	Stdctrls::TLabel *fPositionLabel;
	bool fSpringLoaded;
	Word fPageSize;
	int fDiameter;
	int fRadius;
	int fSteps;
        int fDimpleSize;
	float fAngleInterval;
	int fAngle;
	int fMouseAngle;
        bool fFastDraw;
	bool fDragging;
	TPoint fIndicatorCenterPosition;
	Classes::TNotifyEvent fOnChange;
	void __fastcall SetMin(const int NewMinValue);
	void __fastcall SetMax(const int NewMaxValue);
	void __fastcall SetPosition(const int NewPosition);
	void __fastcall SetParams(int APosition, int AMin, int AMax);
	void __fastcall SetSteps(void);
	void __fastcall CalcAngle(void);
	int __fastcall CalcPosition(const int TheAngle);
	void __fastcall SetPositionLabel(TLabel *NewLabel);
	void __fastcall ShowPosition(const int ThePosition);
	void __fastcall SetSpringLoaded(const bool Sprung);
	void __fastcall SetLineColor(Graphics::TColor NewColor);
	MESSAGE void __fastcall WMCreate(Messages::TWMCreate &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Messages::TWMSize &Message);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Messages::TWMSetFocus &Message);
	HIDESBASE MESSAGE void __fastcall WMKillFocus(Messages::TWMKillFocus &Message);
	MESSAGE void __fastcall WMGetDlgCode(Messages::TWMNoParams &Message);
	HIDESBASE MESSAGE void __fastcall CMMouseLeave(Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMVisibleChanged(Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall CMEnabledChanged(Messages::TMessage &Message);

	MESSAGE void __fastcall CM_ParentColorChanged(Messages::TMessage &Msg);
	MESSAGE void __fastcall CM_TextChanged(Messages::TMessage &Msg);
        Graphics::TBitmap *IndicatorBM;            // Bitmap for indicator
        Graphics::TBitmap *bm;                     // Raw image
        void __fastcall SetDimpleSize(int Value);
        void __fastcall SetFastDraw(bool Value);

        void PaintKnob(void);
        void CreateIndicator(void);
        void CreateIndicatorFast(void);
        void CreateIndicatorSlow(void);
	void CreateKnob(void);
	void CreateState(void);

protected:
        BEGIN_MESSAGE_MAP
           MESSAGE_HANDLER(CM_PARENTCOLORCHANGED, TMessage, CM_ParentColorChanged)
           MESSAGE_HANDLER(CM_TEXTCHANGED, TMessage, CM_TextChanged)
           MESSAGE_HANDLER(CM_ENABLEDCHANGED, TMessage, CMEnabledChanged)
           MESSAGE_HANDLER(CM_VISIBLECHANGED, TMessage, CMVisibleChanged)
           MESSAGE_HANDLER(CM_MOUSELEAVE, TMessage, CMMouseLeave)
           MESSAGE_HANDLER(WM_SETFOCUS, TWMSetFocus, WMSetFocus)
           MESSAGE_HANDLER(WM_CREATE, TWMCreate, WMCreate)
           MESSAGE_HANDLER(WM_KILLFOCUS, TWMKillFocus, WMKillFocus)
           MESSAGE_HANDLER(WM_GETDLGCODE, TWMNoParams, WMGetDlgCode)
           MESSAGE_HANDLER(WM_SIZE, TWMSize, WMSize)
        END_MESSAGE_MAP(TCustomControl)

	virtual void __fastcall Paint(void);
	void PaintIndicator(void);
	void DrawIndicator(void);
	DYNAMIC void __fastcall MouseUp(Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int
		Y);
	DYNAMIC void __fastcall MouseDown(Controls::TMouseButton Button, Classes::TShiftState Shift, int X,
		int Y);
	DYNAMIC void __fastcall MouseMove(Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
	virtual void __fastcall Notification(Classes::TComponent* AComponent, Classes::TOperation Operation
		);

public:
  __fastcall TKnob(TComponent* Owner);

__published:
	__property ParentShowHint ;
	__property ShowHint ;
	__property int Max = {read=fMax, write=SetMax, default=127};
	__property int Min = {read=fMin, write=SetMin, default=0};
        __property int DimpleSize = {read=fDimpleSize, write=SetDimpleSize, default=10};
        __property bool FastDraw = {read=fFastDraw, write=SetFastDraw, default=false};
	__property Caption ;
	__property Graphics::TColor LineColor = {read=fLineColor, write=SetLineColor, default=-2147483646};

	__property int Position = {read=fPosition, write=SetPosition, default=0};
	__property Word PageSize = {read=fPageSize, write=fPageSize, default=10};
	__property Stdctrls::TLabel* PositionLabel = {read=fPositionLabel, write=SetPositionLabel};
	__property bool SpringLoaded = {read=fSpringLoaded, write=SetSpringLoaded, default=0};
	__property TabStop ;
	__property TabOrder ;
	__property PopupMenu ;
	__property Visible ;
	__property Enabled ;
	__property Classes::TNotifyEvent OnChange = {read=fOnChange, write=fOnChange};
	__property OnClick ;
	__property OnEnter ;
	__property OnExit ;
	__property OnKeyDown ;
	__property OnKeyPress ;
	__property OnKeyUp ;
	__property OnMouseDown ;
	__property OnMouseMove ;
	__property OnMouseUp ;

public:
/*
	#pragma option push -w-inl
	// TWinControl.CreateParented
   inline __fastcall TKnob(HWND ParentWindow) : Controls::TCustomControl(
		ParentWindow) { }
	#pragma option pop
*/
};
//---------------------------------------------------------------------------
#endif
 