//---------------------------------------------------------------------------

#ifndef MTIDBMeterH
#define MTIDBMeterH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <vector>

//---------------------------------------------------------------------------
#ifndef _TProgressDirection_
#define _TProgressDirection_
enum TProgressDirection { pdLeftToRight, pdRightToLeft, pdBottomToTop, pdTopToBottom };
#endif
enum TLedType {ltLedFlatRect, ltLed3DRect, ltLed3DRound, ltLed3DLargeRound};

// This is the LED class, each LED has a lit and unlit color
// The DBMeter builds a list of LED and then lights and unlights them

enum TLedState {llsBlank, llsLit, llsUnlit};
class TLed
{
public:
    TLed(Graphics::TBitmap *abmLit, Graphics::TBitmap *abmUnlit, Graphics::TBitmap *abmBlank);
   ~TLed(void);
   Graphics::TBitmap *GetLed(TLedState bLit);
   int Height(void);
   int Width(void);
   TLedState Lit(void);
   TLedState Lit(const TLedState bLit);

private:
   Graphics::TBitmap *bmLit;
   Graphics::TBitmap *bmUnlit;
   Graphics::TBitmap *bmBlank;
   TLedState _Lit;
};

class PACKAGE TMTIDBMeter : public TCustomControl
{
private:
	Graphics::TBitmap* MainBitmap;
	Graphics::TColor fBorderColor1;
	double f1;
	Graphics::TColor fBorderColor2;
		double f2;
	Graphics::TColor fBackgroundColor;
	double f3;
	double _Position;
	double _Min;
	double _Max;
	bool _ShowBorder;
    bool _ShowClipLed;
    bool _ShowPeak;
    bool _ShowUnlitLeds;
	TLedType _LedType;
	TProgressDirection _Direction;
	bool fShowPosText;
	AnsiString fPosTextSuffix;
	AnsiString fPosTextPrefix;
	Graphics::TColor _ColorInBounds;
	Graphics::TColor _ColorWarning;
	Graphics::TColor _ColorClip;

    TTimer *_ClipTimer;
    TTimer *_PeakTimer;

	double _WarningLevel;
	bool RegenerateBitmap;
	bool _InvalidateLeds;
        Graphics::TBitmap* TiledBarBitmap;
	void __fastcall PaintBorder(void);
	void __fastcall PaintPosText(void);
	void __fastcall SetBorderColor1(const Graphics::TColor Value);
	void __fastcall SetBorderColor2(const Graphics::TColor Value);
	void __fastcall SetBackgroundColor(const Graphics::TColor Value);
	void __fastcall SetPosition(double Value);
	void __fastcall SetMax(const double Value);
	void __fastcall SetMin(const double Value);
	void __fastcall SetShowBorder(const bool Value);
	void __fastcall SetShowClipLed(const bool Value);
	void __fastcall SetShowUnlitLeds(const bool Value);
	void __fastcall SetShowPeak(const bool Value);
	void __fastcall PaintBar(bool RegenerateBitmap);
	void __fastcall SetDirection(const TProgressDirection Value);
	void __fastcall SetLedType(const TLedType Value);
	void __fastcall SetColorInBounds(const Graphics::TColor Value);
	void __fastcall SetColorClip(const Graphics::TColor Value);
	void __fastcall SetColorWarning(const Graphics::TColor Value);
	void __fastcall SetWarningLevel(const double Value);
	void __fastcall SetShowPosText(const bool Value);
	void __fastcall SetPosTextSuffix(const AnsiString Value);
	void __fastcall SetClipTime(int Value);
	void __fastcall SetPeakTime(int Value);

	HIDESBASE MESSAGE void __fastcall CMFontChanged(Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMSize(Messages::TWMSize &Message);

	void __fastcall SetPosTextPrefix(const AnsiString Value);

    // This is the LED drawing part
	void DrawLed(int n, TLedState lit);
    void DrawLed(int n, bool lit);
    void DrawLeds(void);

    int  PixelFromPos(double Pos);
    double  PosFromPixel(int Pixel);
    void __fastcall SetLedSize(int Size);
    void __fastcall SetLedSpacing(int Spacing);
    TRect _TiledArea;
    double _LastMax;

	int LogicalSize(void);
	int PhysicalSize(void);
    int NumberOfLeds(void);
    void RecomputeSizes(void);

    int __fastcall GetClipTime(void);
    int __fastcall GetPeakTime(void);
    void __fastcall ClipTimeCallback(TObject* Sender);
    void __fastcall PeakTimeCallback(TObject* Sender);
    int LedFromPos(double Pos);
    std::vector<TLed> Leds;
    void BuildLedList(void);

	Graphics::TBitmap *ClipBitmap;
	Graphics::TBitmap *ClipBitmapUnlit;
	Graphics::TBitmap *WarningBitmap;
	Graphics::TBitmap *WarningBitmapUnlit;
	Graphics::TBitmap *InBoundsBitmap;
	Graphics::TBitmap *InBoundsBitmapUnlit;
	Graphics::TBitmap *BlankBitmap;

    bool CreateLedFromResource(TColor cl, String sResource, Graphics::TBitmap *&bm, bool bTranspose = false);
    bool CreateFlatLed(TColor cl, Graphics::TBitmap *bm);

    int _NewLedSize;
    int _NewWidth;

protected:
	virtual void __fastcall Paint(void);
    int  _KeeperValue;
    int  _KeepCount;
    int _LedSize;
    int _BarPixelLength;
    int _LedSpacing;
    int _Clipped;
    BEGIN_MESSAGE_MAP
       MESSAGE_HANDLER(WM_SIZE, TWMSize, WMSize)
    END_MESSAGE_MAP(TCustomControl)

public:
	__fastcall TMTIDBMeter(TComponent* AOwner);
	__fastcall virtual ~TMTIDBMeter(void);
        bool IsVertical(void);
        int LedFullSize(void);
        bool Clipped(bool On);
        bool Clipped(void);
        void Rebuild(void);

__published:
	__property Graphics::TColor BackgroundColor = {read=fBackgroundColor, write=SetBackgroundColor, default=0
		};
	__property Graphics::TColor ColorInBounds = {read=_ColorInBounds, write=SetColorInBounds, default=clGreen};
	__property Graphics::TColor ColorWarning = {read=_ColorWarning, write=SetColorWarning, default=clYellow};
	__property Graphics::TColor ColorClip = {read=_ColorClip, write=SetColorClip, default=255};
	__property bool ShowUnlitLeds = {read=_ShowUnlitLeds, write=SetShowUnlitLeds, default=false};
	__property double WarningLevel = {read=_WarningLevel, write=SetWarningLevel, default=80};

	__property Graphics::TColor BorderColor1 = {read=fBorderColor1, write=SetBorderColor1, default=-2147483632
		};
	__property Graphics::TColor BorderColor2 = {read=fBorderColor2, write=SetBorderColor2, default=-2147483628
		};
	__property TProgressDirection Direction = {read=_Direction, write=SetDirection, default=0};
	__property TLedType LedType = {read=_LedType, write=SetLedType, default=0};
	__property double Max = {read=_Max, write=SetMax, default=100};
	__property double Min = {read=_Min, write=SetMin, default=0};
	__property double Position = {read=_Position, write=SetPosition, default=0};
	__property AnsiString PosTextPrefix = {read=fPosTextPrefix, write=SetPosTextPrefix};
	__property AnsiString PosTextSuffix = {read=fPosTextSuffix, write=SetPosTextSuffix};
	__property bool ShowBorder = {read=_ShowBorder, write=SetShowBorder, default=1};
	__property bool ShowClipLed = {read=_ShowClipLed, write=SetShowClipLed, default=1};
	__property bool ShowPosText = {read=fShowPosText, write=SetShowPosText, default=1};
	__property bool ShowPeak = {read=_ShowPeak, write=SetShowPeak, default=1};
    __property int LedSize  = {read=_LedSize, write=SetLedSize, default=4};
    __property int LedSpacing  = {read=_LedSpacing, write=SetLedSpacing, default=2};
    __property int ClipTime = {read=GetClipTime, write=SetClipTime, default = 5000};
    __property int PeakTime = {read=GetPeakTime, write=SetPeakTime, default = 1000};
	__property Align ;
	__property Anchors ;
	__property Font ;
	__property ParentShowHint ;
	__property ShowHint ;
	__property Visible ;
	__property OnClick ;
	__property OnDblClick ;
    __property OnResize;
};
//---------------------------------------------------------------------------
#endif


