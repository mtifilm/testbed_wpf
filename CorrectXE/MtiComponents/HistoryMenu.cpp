//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HistoryMenu.h"
#include "IniFile.h"
#include "MTIio.h"
#pragma package(smart_init)
#pragma resource "*.res"
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(THistoryMenu *)
{
        new THistoryMenu(NULL);
}
//---------------------------------------------------------------------------
__fastcall THistoryMenu::THistoryMenu(TComponent* Owner)
        : TMainMenu(Owner)
{
    fiBaseIndex = -1;           // We are not yet set
    fiNumberAdded = 0;          // Number of items + Seperator
    fiPosition = 0;             // What menu to write
    fiActualFromBottom = 2;     // Actual from bottom (fudge if menu is too small)
    fiFromBottom = 2;           // Where the history goes, measured from
                                // The bottom, usual separator + exit
    HistoryList.IniFileName = CPMPIniFileName();
    SET_CBHOOK(PCBHistoryChanged, HistoryList);
    HistoryList.ReadProperties();
}

__fastcall THistoryMenu::~THistoryMenu(void)
{
    HistoryList.WriteProperties();
}

//---------------------------------------------------------------------------
namespace Historymenu
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(THistoryMenu)};
                 RegisterComponents("MTI", classes, 0);
        }
}

//------------------LoadFile------------------John Mertus-----Sept 01---

   void __fastcall THistoryMenu::LoadFile(TObject *Sender)

//  This is called when a click occures on the menu
//
//**********************************************************************
{
  TMenuItem *MenuItem = (TMenuItem *)Sender;
  String s = (MenuItem->Caption); //.SubString(MenuItem->Caption.Pos(" ")+1,MenuItem->Caption.Length())).c_str();
  if (fOnHistoryClick != NULL) fOnHistoryClick(s);
  HistoryList.Selected = HistoryList.Index(WCharToStdString(s.c_str()));
  HistoryList.PCBOpen.Notify();
}

//------------PCBHistoryChanged---------------John Mertus-----Sept 01---

   void THistoryMenu::PCBHistoryChanged(void *Data)

//  This is called whenever something happens to the history list
//
//**********************************************************************
{
   fbMenuValid = false;
   RebuildMenu();
}

//------------------RebuildMenu-------------John Mertus-----Sept 01---

   void THistoryMenu::RebuildMenu(void)

//  This rebuilds the entire menu from the HistoryList
//
//**********************************************************************
{
  // Check to see if there is a menu
  if (Items == NULL) return;
  if (Items->Count <= fiPosition) return;

  // If nothing loaded, find where to write the menu items
  if (fiBaseIndex < 0)
    {
       // Where to write the files
       fiBaseIndex = Items->Items[fiPosition]->Count - fiFromBottom;
       fiActualFromBottom = fiFromBottom;
       // Correct if too small
       if (fiBaseIndex < 0)
         {
           fiActualFromBottom = Items->Items[fiPosition]->Count;
           fiBaseIndex = 0;
         }
    }
  else
    {
      // Delete everything
      for (int i = fiNumberAdded + fiBaseIndex - 1; i >= fiBaseIndex; i--)
           Items->Items[fiPosition]->Delete(i);
    }

  // Just exit if history list has size 0
  if (HistoryList.size() == 0) return;
  
  fiNumberAdded = 0;
  // Add a seperator, first check if there is another one
  if (fiBaseIndex > 0)
    if (Items->Items[fiPosition]->Caption != "-")
      {
        TMenuItem *MenuItem = new TMenuItem(this);
        MenuItem->Caption = "-";
        Items->Items[fiPosition]->Insert(fiBaseIndex,MenuItem);
        fiNumberAdded = 1;       // Seperator has been added
      }

  // The first time through, create the popup menu
  if (OpenPopup == NULL)
    {
       OpenPopup = new TPopupMenu(this);
       OpenPopup->Name = "FilePopup";
       OpenPopup->AutoPopup = true;
    }
  else
    OpenPopup->Items->Clear();

  // Add all items
  for (unsigned i = 0; i < HistoryList.size(); i++)
    AddItem(i, HistoryList[i].c_str());

  fbMenuValid = true;     // Menu has been rebuilt
}

//------------------AddItem------------------John Mertus-----Sept 01---

   void THistoryMenu::AddItem(int i, String s)

//  This adds string s to menu list with index i
//
//**********************************************************************
{
   TMenuItem *MenuItem = new TMenuItem(this);
   String Hot;

   // Create the right hot letter in front of the name
   if (i < 9)
     Hot = i+1;
   else
     Hot = char(int('A') + i - 9);

   MenuItem->Caption = "&" + Hot + " " + s;
   MenuItem->OnClick = LoadFile;
   MenuItem->Tag = i;
   int n = Items->Items[fiPosition]->Count - fiActualFromBottom;
   Items->Items[fiPosition]->Insert(n,MenuItem);
   fiNumberAdded++;                                    // Another has been added

   // Add the popup menu
   TMenuItem *MenuItem2 = new TMenuItem(OpenPopup);
   MenuItem2->Caption = MenuItem->Caption;
   MenuItem2->OnClick = LoadFile;
   MenuItem2->Tag = i;
   OpenPopup->Items->Add(MenuItem2);
}

//------------------Add--------------------John Mertus-----Sept 01---

   void THistoryMenu::Add(const string &str)

//  This adds string s to the history list
//
//**********************************************************************
{
   HistoryList.Add(str);
}


