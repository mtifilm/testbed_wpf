#ifndef SharedMapForComponentsH
#define SharedMapForComponentsH
#define SharedMapH             // wtf ??

#include "corecodelibint.h"
#include <map>
#include <string>
using std::map;
using std::string;

class MTI_CORECODELIB_API SharedMapForComponents
{
   static map<string, void *> *TheMap;

public:
   static void setMap(void *newMap);
   static void add(const string &key, void *value);
   static void remove(const string &key);
   static void *retrieve(const string &key);

private:

   SharedMapForComponents() {};
};

#endif
