//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "Knob.h"
#pragma package(smart_init)
#pragma resource "*.res"
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//


#define  MAX_ANGLE 270
#define  MIN_ANGLE 0

static inline void ValidCtrCheck(TKnob *)
{
  new TKnob(NULL);
}

#define ANTI_A_SZ 4

//--------------------TKnob---------------------John Mertus----Aug 2002-----

__fastcall TKnob::TKnob(TComponent* Owner)
  : TCustomControl(Owner)

//  Constructor, build everything and set defaults.
//
//****************************************************************************
{
  Width = 40;
  Height = 40;
  fRadius = 20;
  fMin = -128;
  fMax= 127;
  fPosition = 0;
  fPageSize = 10;
  fDimpleSize = 10;
  fSpringLoaded = false;
  fLineColor = clActiveCaption;
  fFastDraw = false;

  TabStop = true;
  ControlStyle << csOpaque;

  bm = new Graphics::TBitmap();
  bm->PixelFormat = pf24bit;
  IndicatorBM = new Graphics::TBitmap();
  IndicatorBM->PixelFormat = pf24bit;
}

//--------------------Average---------------------John Mertus----Feb 2003-----

    void Average(Graphics::TBitmap *bmIn, Graphics::TBitmap *bmOut, int nBkSz)

//  Generic routine that takes the input bitmap and averages
//  nBkSz blocks to produce the Output bitmap.
//
//   Input and output bitmaps must be 24 bits.
//   Output bitmap must be 1/nBkSz the size of the input one
//
//****************************************************************************
{
    // Now average all the values in an SxS square
    Byte *ptr;
    for (int y = 0; y < bmIn->Height; y+=nBkSz)
    {
      for (int x = 0; x < 3*bmIn->Width; x+=3*nBkSz)
        {
           int r=0;
           int g=0;
           int b=0;
           for (int row=0; row < nBkSz; row++)
             {
               ptr = (Byte *)bmIn->ScanLine[y+row] + x;
               for (int col=0; col < 3*nBkSz; col+=3)
                 {
                   r += ptr[col];
                   g += ptr[col+1];
                   b += ptr[col+2];
                 }
             }
           ptr = (Byte *)bmOut->ScanLine[y/nBkSz] + x/nBkSz;
           ptr[0] = r/(nBkSz*nBkSz);
           ptr[1] = g/(nBkSz*nBkSz);
           ptr[2] = b/(nBkSz*nBkSz);
        }
    }
}

//-------------------PaintKnob--------------------John Mertus----Feb 2003-----

    void TKnob::PaintKnob(void)

//  This takes the created knob which is both knob and state
//  and draws it to canvas.
//  The Indicator is NOT drawn.
//
//****************************************************************************
{
    Graphics::TBitmap *abm;           // Anti-alised image

    abm = new Graphics::TBitmap();
    abm->PixelFormat = pf24bit;
    abm->Height = Height;
    abm->Width = Width;

    Average(bm, abm, ANTI_A_SZ);
    Canvas->Draw(0,0, abm);
    delete abm;
}

//----------------SetPositionLabel----------------John Mertus----Feb 2003-----

  void __fastcall TKnob::SetPositionLabel(TLabel* NewLabel)

//  Changes the position lable
//
//****************************************************************************
{
  if (fPositionLabel != NewLabel) fPositionLabel = NewLabel;
  if (fPositionLabel != NULL) ShowPosition(Position);
}

//------------------Notification------------------John Mertus----Feb 2003-----

  void __fastcall TKnob::Notification(Classes::TComponent* AComponent, Classes::TOperation Operation)

// If the PositionLabel is removed then point PositionLabel to NULL
//
//****************************************************************************
{
  TCustomControl::Notification(AComponent, Operation);
  if ((AComponent == fPositionLabel) && (Operation == opRemove))
    PositionLabel= NULL;
}

//------------------CM_TextChanged----------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::CM_TextChanged(Messages::TMessage &Msg)

//If the caption changes) re-draw on Position Label as suffix
//
//****************************************************************************
{
  ShowPosition(Position);
}

//------------------ShowPosition------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::ShowPosition(const int ThePosition)

//Display the Position value in an associated PositionLabel control
//
//****************************************************************************
{
  if (fPositionLabel != NULL)
    fPositionLabel->Caption = Format("%d " + Caption, ARRAYOFCONST((ThePosition)));
}

//--------------------WMCreate--------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::WMCreate(Messages::TWMCreate &Message)

//  This happens on create
//
//****************************************************************************
{
  SetSteps();
  CalcAngle();
}

//---------------------WMSize---------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::WMSize(Messages::TWMSize &Message)

//  This happens on resize, force a square.
//
//****************************************************************************
{
  int H;
  if ((Height > fDiameter) || (Width > fDiameter)) //growing
    H = (((Height) > (Width)) ? (Height) : (Width));
  else if ((Height < fDiameter) || (Width < fDiameter)) //shrinking
    H = (((Height) < (Width)) ? (Height) : (Width));
  else
    H = fDiameter;

  SetBounds(Left,Top,H,H);
  fDiameter = H;
  fRadius = H/2;

  // Create a new bitmap
  TCustomControl::Dispatch(&Message);
}

//-------------------WMSetFocus-------------------John Mertus----Feb 2003-----

    void __fastcall TKnob::WMSetFocus(Messages::TWMSetFocus &Message)

//  Change of focus, redraw the state
//
//****************************************************************************
{
  CreateState();
  Paint();
  TCustomControl::Dispatch(&Message);
}

//------------------WMKillFocus-------------------John Mertus----Feb 2003-----

    void __fastcall TKnob::WMKillFocus(Messages::TWMKillFocus &Message)

//   Focus has been dropped, redraw the state
//
//****************************************************************************
{
  fDragging = false; //Release dragging flag
  if (SpringLoaded) Position = 0;
  CreateState(); //Paint as non-focused
  Paint();
  TCustomControl::Dispatch(&Message);
}

//--------------------MouseUp---------------------John Mertus----Feb 2003-----

     void __fastcall TKnob::MouseUp(Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Turn off the dragging and spring back if necessary
//
//****************************************************************************
{
  int NewPosition;
  if (fDragging)
    {
     if (SpringLoaded)
       {
         Position = 0;
       }
     else
       {
         NewPosition = CalcPosition(fMouseAngle);
         if (Position != NewPosition) Position = NewPosition;
       }
     fDragging = False;
    }

////  TCustomControl::MouseUp(Button,Shift,X,Y);
 }

//-------------------MouseDown--------------------John Mertus----Feb 2003-----

    void __fastcall TKnob::MouseDown(Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y)

//  Start the dragging
//
//****************************************************************************
{
  HRGN RegionHandle;
  if (Button == mbLeft)
   {
	 TRect rect = ClientRect;
	 RegionHandle = CreateEllipticRgnIndirect(&rect);
	 if (RegionHandle != nullptr)
       if (PtInRegion(RegionHandle, X,Y))
         try
          {
			SetFocus();
            fDragging = True;
            Position = CalcPosition(fMouseAngle);
            PaintIndicator();
          }
        catch ( ... ) {}

        //Ensure the created region is deleted
        DeleteObject(RegionHandle);
    }
////  TCustomControl::MouseDown(Button,Shift,X,Y);
}

//-------------------MouseMove--------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::MouseMove(Classes::TShiftState Shift, int X, int Y)

//  Mouse position has changed, move the mouse
//
//****************************************************************************
{
  int NewPosition;

  if (X == (Width/2))
    {
      if (Y > (Height/2))
         fMouseAngle = 270; //Correctly 270, but 240 reflects real world knob
      else
        fMouseAngle = 90; //rotation behaviour
    }
  else
    {
      fMouseAngle = atan(((Height/2.0)-Y)/(X-(Width/2.0)))*180/3.14159 + 0.5;
      if (X < (Width/2)) fMouseAngle = (fMouseAngle + 540)%360;
    }

  //Limit movement to between 240 clockwise through zero to 300 degrees
//  if (fMouseAngle <= 360) and (fMouseAngle > 360)) fMouseAngle = 360;
  if (fMouseAngle < -90) fMouseAngle = -90;

  //Where would this set Position to, if (clicked?
  NewPosition = CalcPosition(fMouseAngle);

  //if the knob is being dragged and the position has changed) update
  // Position to this new position
  if (fDragging && (fPosition != NewPosition))
   {
      Position = NewPosition;
     //Show the Angle on a label (if (associated)
      ShowPosition(NewPosition);
   }

 //Enable default OnMouseMove processing
////  TCustomControl::MouseMove(Shift,X,Y);
}

//------------------CMMouseLeave------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::CMMouseLeave(Messages::TMessage &Message)

//  Reset the PositionLabel caption on mouse-exit
//
//****************************************************************************
{
  ShowPosition(Position);
  TCustomControl::Dispatch(&Message);
}

//----------------CMEnabledChanged----------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::CMEnabledChanged(Messages::TMessage &Message)

//  Change of state has happened
//
//****************************************************************************
{
  //Force the knob to re-paint its new state
  Paint();
  TCustomControl::Dispatch(&Message);
}

//----------------CMVisibleChanged----------------John Mertus----Feb 2003-----

    void __fastcall TKnob::CMVisibleChanged(Messages::TMessage &Message)

//  Change of visibality, the label is considered part of the control
//
//****************************************************************************
{
  //Show or hide the position Label in sync with the Knob
  if (PositionLabel != NULL) PositionLabel->Visible = Visible;
  TCustomControl::Dispatch(&Message);
}

//----------------CM_ParentColorChanged------------John Mertus----Feb 2003-----

    void __fastcall TKnob::CM_ParentColorChanged(Messages::TMessage &Message)

//  Color has changed, redraw everything.
//
//****************************************************************************
{
  bm->Canvas->Brush->Color = ((TWinControl *)Parent)->Brush->Color;
  bm->Canvas->FillRect(ClientRect);
  CreateKnob();
  CreateState();
  Paint();
  TCustomControl::Dispatch(&Message);
}

//----------------SetSpringLoaded-----------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::SetSpringLoaded(const bool Sprung)

//  Internal procedure to set the spring state
//
//****************************************************************************
{
  if (fSpringLoaded != Sprung) fSpringLoaded = Sprung;
  if (Sprung) Position = 0;
}

//------------------SetPosition-------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::SetPosition(const int NewPosition)

//  Internal procedure to set the position and update the display
//
//****************************************************************************
{
  if (Position == NewPosition)
  {
	return;
  }

  SetParams(NewPosition, fMin, fMax);
}

//---------------------SetMin---------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::SetMin(const int NewMinValue)

//  Internal procedure to set the minimum
//
//****************************************************************************
{
  SetParams(fPosition, NewMinValue, fMax);
}

//---------------------SetMax---------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::SetMax(const int NewMaxValue)

//  Internal procedure to set the maximum
//
//****************************************************************************
{
  SetParams(fPosition, fMin, NewMaxValue);
}

//-------------------SetParams--------------------John Mertus----Feb 2003-----

     void __fastcall TKnob::SetParams(int APosition, int AMin, int AMax)

//  All the real work of position and min, max changes are done here.
//
//****************************************************************************
{
  if ((fMin != AMin)) //Min has Changed
  {
    fMin = AMin;
    SetSteps();     //updates fSteps and fAngleInterval
  }

  if (fMax != AMax)
  {
    fMax = AMax;
    SetSteps();     //updates fSteps and fAngleInterval
  }

  if (fAngleInterval == 0)
   {
      APosition = AMin;
   }
  else
    {
      while (APosition < AMin) APosition += (AMax - AMin);
      while (APosition > AMax) APosition -= (AMax - AMin);
    }

  if (fPosition == APosition) return;

  fPosition = APosition;

  CalcAngle();                //Set fAngle
  PaintIndicator();           //And paint the new indicator position
  ShowPosition(Position);   //Update the PositionLabel caption

  //Fire the OnChange event if (not in Designing state
  if ((fOnChange != NULL) && (!ComponentState.Contains(csDesigning)))
    fOnChange(this);
}

//--------------------SetSteps--------------------John Mertus----Feb 2003-----

  void __fastcall TKnob::SetSteps(void)

//  Internal procedure need to set the angle change.
//
//****************************************************************************
{
  fSteps = fMax - fMin;
  if (fSteps == 0)
   {
     fAngleInterval= 0;
    }
  else
    {
      fAngleInterval = 360.0 / fSteps;
      fSteps = abs(fSteps);
    }
}

//-------------------CalcAngle--------------------John Mertus----Feb 2003-----

  void __fastcall TKnob::CalcAngle(void)

//  Calculate fAngle based on fMin, fPosition and fAngleInterval
//
//****************************************************************************
{
  fAngle = 270 - ((fPosition - fMin) * fAngleInterval + 0.5);
}

//Calculate fPosition based on fMin, fMax, Angle parameter and fAngleInterval
//------------------CalcPosition------------------John Mertus----Feb 2003-----

  int __fastcall TKnob::CalcPosition(const int NewAngle)

//  Calculates the new position based upon the the angle
//
//****************************************************************************
{
  int Result;
  if (fAngleInterval == 0)
    Result = fMin;
  else
    Result = fMin + ((270 - NewAngle) / fAngleInterval);

  return Result;
}

//------------------SetLineColor------------------John Mertus----Feb 2003-----

    void __fastcall TKnob::SetLineColor(Graphics::TColor NewColor)

//  Set the colour of the indicator
//
//****************************************************************************
{
  if ((fLineColor != NewColor) && (NewColor != clBtnFace))
  {
    fLineColor = NewColor;
    Invalidate();
  }
}

//-------------------CreateKnob-------------------John Mertus----Feb 2003-----

  void TKnob::CreateKnob(void)

//  Does all the hard work of creating the bitmap for the new knob
//
//****************************************************************************
{
    // Reset the position of the indicator to nothing
    fIndicatorCenterPosition.x = Height/2;
    fIndicatorCenterPosition.y = Width/2;

    bm->Height = ANTI_A_SZ*Width;
    bm->Width = ANTI_A_SZ*Width;

    //Draws a circular clBtnFace coloured knob-face
    //regardless of the Parents Canvas->Brush color
    bm->Canvas->Pen->Color = clBtnFace;
    bm->Canvas->Brush->Color = clBtnFace;
    bm->Canvas->Brush->Style = bsSolid;
    bm->Canvas->Rectangle(0,0,bm->Width, bm->Height);

    //Draw the circular patch - 2 pixels is the width of the highlight
    // & shadow arcs, as drawn below. Test showed that drawing it this small
    // reduced flicker by not obliterating the arcs before they're re-drawn
    bm->Canvas->Ellipse(2*ANTI_A_SZ, 2*ANTI_A_SZ, bm->Width-2*ANTI_A_SZ, bm->Height-2*ANTI_A_SZ);

    //Draw the upper highlighted arc
    bm->Canvas->Pen->Width = 2*ANTI_A_SZ;
    bm->Canvas->Pen->Color = clBtnHighlight;

    bm->Canvas->Arc((ClientRect.Left+2)*ANTI_A_SZ,
                     (ClientRect.Top+2)*ANTI_A_SZ,
                     ClientRect.Right*ANTI_A_SZ, ClientRect.Bottom*ANTI_A_SZ,
                     ClientRect.Right*ANTI_A_SZ,ClientRect.Top*ANTI_A_SZ,
                     ClientRect.Left*ANTI_A_SZ,ClientRect.Bottom*ANTI_A_SZ);

    //Draw the lower shadowed arc
    bm->Canvas->Pen->Color = clBtnShadow;
    bm->Canvas->Arc((ClientRect.Left+1)*ANTI_A_SZ,
                (ClientRect.Top+1)*ANTI_A_SZ,
                (ClientRect.Right-1)*ANTI_A_SZ,
                (ClientRect.Bottom-1)*ANTI_A_SZ,
                ClientRect.Left*ANTI_A_SZ,
                ClientRect.Bottom*ANTI_A_SZ,
                ClientRect.Right*ANTI_A_SZ,
                ClientRect.Top*ANTI_A_SZ);

    CreateIndicator();
}

//------------------CreateState-------------------John Mertus----Feb 2003-----

     void TKnob::CreateState(void)

//  This creates the proper state of the indicator depending upon the
// the focused and enabled functions
//
//****************************************************************************
{
    if (bm == NULL) return;
    bm->Canvas->Pen->Width = ANTI_A_SZ;
    bm->Canvas->Brush->Style = bsClear;

    if (Enabled) bm->Canvas->Pen->Color = clBlack;
    else bm->Canvas->Pen->Color = clBtnShadow;

    bm->Canvas->Ellipse(0, 0, ClientRect.Width()*ANTI_A_SZ, ClientRect.Height()*ANTI_A_SZ);

    if (Focused())
       bm->Canvas->Pen->Color = clBlack;
    else
       bm->Canvas->Pen->Color = clBtnShadow;


    bm->Canvas->Arc(ClientRect.Left*ANTI_A_SZ,ClientRect.Top*ANTI_A_SZ,ClientRect.Right*ANTI_A_SZ,
                ClientRect.Bottom*ANTI_A_SZ,ClientRect.Right*ANTI_A_SZ,ClientRect.Top*ANTI_A_SZ,ClientRect.Left*ANTI_A_SZ,ClientRect.Bottom*ANTI_A_SZ);
    if (PositionLabel != NULL) PositionLabel->Enabled = Enabled;

}

void TKnob::CreateIndicatorSlow(void)
{
    Graphics::TBitmap *abm = new Graphics::TBitmap();
    abm->PixelFormat = pf24bit;
    abm->Height = (fDimpleSize+1)*ANTI_A_SZ;
    abm->Width =  (fDimpleSize+1)*ANTI_A_SZ;

    // Clear the bitmap
    abm->Canvas->Pen->Color = clBtnFace;
    abm->Canvas->Brush->Color = clBtnFace;
    abm->Canvas->Brush->Style = bsSolid;
    abm->Canvas->Pen->Width = ANTI_A_SZ;
    abm->Canvas->Rectangle(0,0,abm->Width, abm->Height);

    if ((Enabled))
      abm->Canvas->Brush->Color = fLineColor;
    else
      abm->Canvas->Brush->Color = clHighlight;

    float AngleInRadians = (fAngle)*3.14159/180;
    float CosAngle = cos(AngleInRadians);
    float SinAngle = sin(AngleInRadians);
    int x1 = (fRadius + (fRadius-fDimpleSize-3)*CosAngle)*ANTI_A_SZ + .5;
    int y1 = (fRadius - (fRadius-fDimpleSize-3)*SinAngle)*ANTI_A_SZ + .5;

    int x = (fRadius + (fRadius-fDimpleSize-3)*CosAngle) + .5;
    int y = (fRadius - (fRadius-fDimpleSize-3)*SinAngle) + .5;

    x1 = x1 - x*ANTI_A_SZ + 2;     // Don't understand the 2!
    y1 = y1 - y*ANTI_A_SZ + 2;

    // End points
    int x2 = x1+fDimpleSize*ANTI_A_SZ;
    int y2 = y1+fDimpleSize*ANTI_A_SZ;

    // Draw the filled in circle
    abm->Canvas->Ellipse(x1, y1, x2, y2);

    //Draw the upper highlighted arc
    abm->Canvas->Pen->Color = clBtnHighlight;
    abm->Canvas->Arc(x1,y1, x2,y2, x1, y2, x2, y1);

    //Draw the lower shadowed arc
    abm->Canvas->Pen->Color = clBtnShadow;
    abm->Canvas->Arc(x1,y1, x2, y2, x2, y1, x1, y2);

    // Now average all the values in an ANTI_A_SZ x ANTI_A_SZ square
    IndicatorBM->Width = fDimpleSize+1;
    IndicatorBM->Height = fDimpleSize+1;
    Average(abm, IndicatorBM, ANTI_A_SZ);
    delete abm;
}

void TKnob::CreateIndicatorFast(void)
{
    Graphics::TBitmap *abm = new Graphics::TBitmap();
    abm->PixelFormat = pf24bit;
    abm->Height = fDimpleSize*ANTI_A_SZ;
    abm->Width = fDimpleSize*ANTI_A_SZ;

    // Clear the bitmap
    abm->Canvas->Pen->Color = clBtnFace;
    abm->Canvas->Brush->Color = clBtnFace;
    abm->Canvas->Brush->Style = bsSolid;
    abm->Canvas->Pen->Width = ANTI_A_SZ;
    abm->Canvas->Rectangle(0,0,abm->Width, abm->Height);

    if ((Enabled))
      abm->Canvas->Brush->Color = fLineColor;
    else
      abm->Canvas->Brush->Color = clHighlight;

    // Draw the filled in circle
    abm->Canvas->Ellipse(0, 0, abm->Width, abm->Height);


    //Draw the upper highlighted arc
    abm->Canvas->Pen->Color = clBtnHighlight;
    abm->Canvas->Arc(0,0, abm->Width, abm->Height,
        0, abm->Height, abm->Width, 0);

    //Draw the lower shadowed arc
    abm->Canvas->Pen->Color = clBtnShadow;
    abm->Canvas->Arc(0,0, abm->Width, abm->Height,
        abm->Width, 0, 0, abm->Height);

    // Now average all the values in an ANTI_A_SZ x ANTI_A_SZ square
    IndicatorBM->Width = fDimpleSize;
    IndicatorBM->Height = fDimpleSize;
    Average(abm, IndicatorBM, ANTI_A_SZ);
    delete abm;
}

//-----------------PaintIndicator-----------------John Mertus----Feb 2003-----

  void TKnob::PaintIndicator(void)

//  This displays the indicator on the canvas
//
//****************************************************************************
{
    if (!fFastDraw) CreateIndicator();
    DrawIndicator();
}

//----------------CreateIndicator-----------------John Mertus----Feb 2003-----

  void TKnob::CreateIndicator(void)

//  Create the proper type of indicator.
//
//****************************************************************************
{
  if (fFastDraw)
    CreateIndicatorFast();
  else
    CreateIndicatorSlow();
}

//-----------------DrawIndicator------------------John Mertus----Feb 2003-----

  void TKnob::DrawIndicator(void)

//  This does the actual drawing of the indicator on the bitmap then
// copies the bitmap to the canvas
//
//****************************************************************************
{
  float AngleInRadians, CosAngle, SinAngle;
  int x1, y1;

  AngleInRadians = (fAngle)*3.14159/180;
  CosAngle = cos(AngleInRadians);
  SinAngle = sin(AngleInRadians);

  //Erase Indicator
  Canvas->Brush->Color = clBtnFace;
  Canvas->Pen->Color = clBtnFace;
  Canvas->Pen->Width = ANTI_A_SZ;

  x1 = fIndicatorCenterPosition.x - fDimpleSize/2;
  y1 = fIndicatorCenterPosition.y - fDimpleSize/2;
  Canvas->Rectangle(x1, y1, x1+fDimpleSize, y1+fDimpleSize);

  //Calculating the new Indicator positions
  fIndicatorCenterPosition.x = (fRadius + (fRadius-fDimpleSize-3)*CosAngle) + .5;
  fIndicatorCenterPosition.y = (fRadius - (fRadius-fDimpleSize-3)*SinAngle) + .5;

  x1 = fIndicatorCenterPosition.x - fDimpleSize/2 - 1;
  y1 = fIndicatorCenterPosition.y - fDimpleSize/2 - 1;

  Canvas->Draw(x1 , y1, IndicatorBM);
}

//---------------------Paint----------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::Paint(void)

//  Recreate the knob, state and paint it all
//
//****************************************************************************
{
  CreateKnob();
  CreateState();
  PaintKnob();
  PaintIndicator();
}

//------------------WMGetDlgCode------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::WMGetDlgCode(Messages::TWMNoParams &Message)

//  This tells the control we want the arrow keys
//
//****************************************************************************
{
  TCustomControl::Dispatch(&Message);
  Message.Result = DLGC_WANTARROWS;
}

//-----------------SetDimpleSize------------------John Mertus----Feb 2003-----

          void __fastcall TKnob::SetDimpleSize(int Value)

//  Internal routine to change the dimple size, this redraws all.
//
//****************************************************************************
{
   if (Value < 4) Value = 4;
   if (Value > .45*Height) Value = .45*Height;
   if (Value == fDimpleSize) return;
   fDimpleSize = Value;
   Paint();
}

//------------------SetFastDraw-------------------John Mertus----Feb 2003-----

          void __fastcall TKnob::SetFastDraw(bool Value)

//  Internal to change to fast or slow draw state
//
//****************************************************************************
{
   if (Value == fFastDraw) return;
   fFastDraw = Value;
   Paint();
}

//--------------------KeyDown---------------------John Mertus----Feb 2003-----

  	void __fastcall TKnob::KeyDown(Word &Key, Classes::TShiftState Shift)

//   This is the key event handler
//
//****************************************************************************
{
  TCustomControl::KeyDown(Key, Shift);
  switch (Key)
  {
    case VK_PRIOR :
      Position = fPosition + fPageSize;
      break;

    case VK_NEXT  :
      Position = fPosition - fPageSize;
      break;

    case VK_END   :
      Position = fMax;
      break;

    case VK_HOME  :
      Position = fMin;
      break;

    case VK_LEFT  :
      Position = fPosition -1;
      break;

    case VK_UP    :
      Position = fPosition +1;
      break;

    case VK_RIGHT :
      Position = fPosition +1;
      break;

    case VK_DOWN  :
      Position = fPosition -1;
      break;
  }
}

//---------------------------------------------------------------------------
namespace Knob
{
  void __fastcall PACKAGE Register()
  {
     TComponentClass classes[1] = {__classid(TKnob)};
     RegisterComponents("MTI", classes, 0);
  }
}
//---------------------------------------------------------------------------
