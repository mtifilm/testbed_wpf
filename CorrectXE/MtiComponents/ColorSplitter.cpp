//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "ColorSplitter.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TColorSplitter *)
{
   new TColorSplitter(NULL);
}
//---------------------------------------------------------------------------
__fastcall TColorSplitter::TColorSplitter(TComponent* Owner)
   : TSplitter(Owner)
{
}

//void __fastcall TColorSplitter::MouseMove(System::Classes::TShiftState Shift, int X, int Y)
//{
//   Screen->Cursor = crVSplit;
//   MouseMove(Shift, X, Y);
//}

//---------------------------------------------------------------------------
namespace Colorsplitter
{
   void __fastcall PACKAGE Register()
   {
       TComponentClass classes[1] = {__classid(TColorSplitter)};
       RegisterComponents(L"MTI", classes, 0);
   }
}
//---------------------------------------------------------------------------
