#include "SharedMapForComponents.h"

map<string, void *> *SharedMapForComponents::TheMap;

void SharedMapForComponents::setMap(void *newMap)
{
   TheMap = reinterpret_cast<map<string, void *> *>(newMap);
}

#ifndef _WIN64
#define nullptr 0
#endif

void SharedMapForComponents::add(const string &key, void *value)
{
   if (TheMap == nullptr)
   {
      return;
   }

   (*TheMap)[key] = value;
}

void SharedMapForComponents::remove(const string &key)
{
   if (TheMap == nullptr)
   {
      return;
   }

   (*TheMap)[key] = nullptr;
}

void *SharedMapForComponents::retrieve(const string &key)
{
   if (TheMap == nullptr)
   {
      return nullptr;
   }

   return (*TheMap)[key];
}


