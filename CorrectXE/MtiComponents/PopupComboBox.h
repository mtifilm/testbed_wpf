//---------------------------------------------------------------------------

#ifndef PopupComboBoxH
#define PopupComboBoxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

class TMultiEdit : public TEdit
{
  private:
   void __fastcall CreateParams(TCreateParams &Params);
  public:
     __fastcall TMultiEdit(TComponent* Owner);
};

class PACKAGE TPopupComboBox : public TCustomPanel
{
private:
        TMultiEdit *Edit;
        TSpeedButton *DropDownButton;
        void SetSize(void);
        int m_iOldWidth;
        int m_iOldHeight;
        TPopupMenu *m_DropDownMenu;

        void __fastcall SetColor(TColor Value) {Edit->Color = Value; }
        TColor __fastcall GetColor(void) { return Edit->Color; }

        void __fastcall SetTabStop(bool Value) {Edit->TabStop = Value; }
        bool  __fastcall GetTabStop(void) { return Edit->TabStop; }

        void __fastcall SetTabOrder(TTabOrder Value) {Edit->TabOrder = Value; }
        TTabOrder  __fastcall GetTTabOrder(void) { return Edit->TabOrder; }

        void __fastcall SetText(AnsiString Value) {Edit->Text = Value; }
        AnsiString __fastcall GetText(void) { return Edit->Text; }

        void __fastcall SetPWidth(int Value) {Edit->Width = Value; }
        int __fastcall GetPWidth(void) { return Edit->Width; }

        void __fastcall SetReadOnly(bool Value) {Edit->ReadOnly = Value; }
        bool __fastcall GetReadOnly(void) { return Edit->ReadOnly; }

        void __fastcall SetEnabled(bool Value) {Edit->Enabled = Value; DropDownButton->Enabled=Value;}
        bool __fastcall GetEnabled(void) { return Edit->Enabled; }

        void __fastcall SetOnChange(Classes::TNotifyEvent Value) {Edit->OnChange = Value; }
        Classes::TNotifyEvent __fastcall GetOnChange(void) { return Edit->OnChange; }

        void __fastcall SetOnEnter(Classes::TNotifyEvent Value) {Edit->OnEnter = Value; }
        Classes::TNotifyEvent __fastcall GetOnEnter(void) { return Edit->OnEnter; }

        void __fastcall SetOnExit(Classes::TNotifyEvent Value) {Edit->OnExit = Value; }
        Classes::TNotifyEvent __fastcall GetOnExit(void) { return Edit->OnExit; }

        void __fastcall SetOnClick(Classes::TNotifyEvent Value) {Edit->OnClick = Value; }
        Classes::TNotifyEvent __fastcall GetOnClick(void) { return Edit->OnClick; }

        void __fastcall SetOnDblClick(Classes::TNotifyEvent Value) {Edit->OnDblClick = Value; }
        Classes::TNotifyEvent __fastcall GetOnDblClick(void) { return Edit->OnDblClick; }

        void __fastcall SetOnKeyDown(TKeyEvent Value) {Edit->OnKeyDown = Value; }
        TKeyEvent  __fastcall GetOnKeyDown(void) { return Edit->OnKeyDown; }

        void __fastcall SetOnKeyUp(TKeyEvent Value) {Edit->OnKeyUp = Value; }
        TKeyEvent  __fastcall GetOnKeyUp(void) { return Edit->OnKeyUp; }

        void __fastcall SetOnKeyPress(TKeyPressEvent Value) {Edit->OnKeyPress = Value; }
        TKeyPressEvent  __fastcall GetOnKeyPress(void) { return Edit->OnKeyPress; }

        void __fastcall SetOnContextPopup(TContextPopupEvent Value) {Edit->OnContextPopup = Value; }
        TContextPopupEvent  __fastcall GetOnContextPopup(void) { return Edit->OnContextPopup; }

        TPopupMenu * __fastcall GetDropDownMenu(void) { return m_DropDownMenu; }
        void __fastcall SetDropDownMenu(TPopupMenu *Value);

        TPopupMenu * __fastcall GetPopup(void) { return Edit->PopupMenu; }
        void __fastcall SetPopup(TPopupMenu *Value) {Edit->PopupMenu = Value ;}

        // Internal callbacks
        void __fastcall DropDownClick(TObject *Sender);
        void __fastcall DropDownMenuClick(TObject *Sender);
        void SetMenuItemClickRecursive(TMenuItem *mi);
        void __fastcall SetEditRect(void);

protected:
        virtual void __fastcall CreateWnd(void);
        virtual void __fastcall Paint(void);
public:
        __fastcall TPopupComboBox(TComponent* Owner);
__published:
	__property Anchors ;
	__property BiDiMode ;
	__property Color = {read=GetColor, write=SetColor, default=clWindow};
	__property Constraints ;
	__property Cursor ;
	__property DragCursor ;
	__property DragKind ;
	__property DragMode ; 
	__property Enabled = {read=GetEnabled, write=SetEnabled, default=true};
	__property Font ;
	__property ParentBiDiMode ;
	__property ParentColor ;
	__property ParentCtl3D ;
	__property ParentFont ;
	__property ParentShowHint ;
	__property TPopupMenu *DropDownMenu = {read=GetDropDownMenu, write=SetDropDownMenu};
	__property PopupMenu = {read=GetPopup, write=SetPopup};
        __property bool ReadOnly = {read=GetReadOnly, write=SetReadOnly, default=false};
	__property ShowHint ;
	__property TabOrder = {read=GetTabOrder, write=SetTabOrder};
	__property TabStop = {read=GetTabStop, write=SetTabStop, default=true};
	__property AnsiString Text = {read=GetText, write=SetText};
	__property Visible ;
//        __property Width = {read=GetPWidth, write=SetPWidth};

        // Callbacks
	__property Classes::TNotifyEvent OnChange = {read=GetOnChange, write=SetOnChange};
	__property OnClick = {read=GetOnClick, write=SetOnClick};
	__property OnContextPopup = {read=GetOnContextPopup, write=SetOnContextPopup};
	__property OnDblClick = {read=GetOnDblClick, write=SetOnDblClick};
	__property OnDragDrop ;
	__property OnDragOver ;
	__property OnEndDock ;
	__property OnEndDrag ;
	__property OnEnter = {read=GetOnEnter, write=SetOnEnter};
	__property OnExit = {read=GetOnEnter, write=SetOnExit};
	__property OnKeyDown = {read=GetOnKeyDown, write=SetOnKeyDown};
	__property OnKeyPress = {read=GetOnKeyPress, write=SetOnKeyPress};
	__property OnKeyUp = {read=GetOnKeyUp, write=SetOnKeyUp};
	__property OnStartDock ;
	__property OnStartDrag ;
};
//---------------------------------------------------------------------------
#endif
