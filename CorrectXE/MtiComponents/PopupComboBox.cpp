//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PopupComboBox.h"
#pragma resource "*.res"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TPopupComboBox *)
{
        new TPopupComboBox(NULL);
}
//---------------------------------------------------------------------------



//-----------------Constructor--------------------John Mertus----Sep 2002-----

  __fastcall TPopupComboBox::TPopupComboBox(TComponent* Owner)
        : TCustomPanel(Owner)

//  Normal constructor.
//
//****************************************************************************
{
        TCustomPanel::TabOrder = false;
        
        Edit = new TMultiEdit(this);
        Edit->Parent = this;
        Edit->ParentShowHint = true;           // Hints are set at upper level only

        DropDownButton = new TSpeedButton(Edit);
        DropDownButton->Parent = Edit;
        //
        // Set the fixed parameters
        BevelInner = bvNone;
        BevelOuter = bvNone;
        Width = 145;
        Height = 21;

        Edit->Color = clWindow;
        Edit->Text = "";
        DropDownButton->OnClick = DropDownClick;
        DropDownButton->Glyph->LoadFromResourceName((int)HInstance, "DropDown");
        DropDownButton->Cursor = crArrow;

        AutoSize = false;
        TabStop = true;
        // The true size is set when the window is created
}

//-------------------CreateWnd--------------------John Mertus----Sep 2002-----

  void  __fastcall TPopupComboBox::CreateWnd(void)

//  Fonts don't become valid until right before window is created.
//  So comput the real size here.
//
//****************************************************************************
{
   TCustomPanel::CreateWnd();
   SetSize();
}


//-------------Paint-----------------------------John Mertus---Feb 2001---

   void __fastcall TPopupComboBox::Paint(void)
//
//  Used at design time change size when font changes or
//  at runtime when width changes.
//
//*************************************************************************
{
  // Recompute on size change
  if ((m_iOldWidth != Width) || (m_iOldHeight != Height) || ComponentState.Contains(csDesigning))
     {
       SetSize();
       m_iOldWidth = Width;
       m_iOldHeight = Height;
     }
   TCustomPanel::Paint();
}

//---------------------------------------------------------------------------

//--------------------SetSize---------------------John Mertus----Sep 2002-----

    void TPopupComboBox::SetSize(void)

//  This computes the size of the form.  Size depends upon a few things,
//  The height is related to the Text Font
//  The Width is the Width of the Entire panel and the Edit Box is expanded
//  to fit this.
//
//****************************************************************************
{
    // First find the total width of the display
    Canvas->Font->Assign(Font);
    Edit->Font->Assign(Font);

    Height = Canvas->TextHeight("M") + 8;
    Edit->Height = Height;
    Edit->Width = Width;

    DropDownButton->Width = 16;
    DropDownButton->Left = Width - DropDownButton->Width - 4;
    DropDownButton->Height = Height - 4;

    TRect Loc;
    SendMessage(Edit->Handle, EM_GETRECT, 0, long(&Loc));
    Loc.Bottom = Edit->ClientHeight + 1;  // +1 is workaround for windows paint bug
    Loc.Right = Edit->ClientWidth - DropDownButton->Width - 2;
    Loc.Top = 0;
    Loc.Left = 0;
    SendMessage(Edit->Handle, EM_SETRECTNP, 0, long(&Loc));
}
//---------------DropDownMenuClick----------------John Mertus----Sep 2002-----

     void __fastcall TPopupComboBox::DropDownMenuClick(TObject *Sender)

//  This is what happens when a drop down menu item is selected.
//  Note:  Indirectly this will call the OnChange method
//
//****************************************************************************
{
   TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
   if (menuItem == NULL) return;

   Edit->Text = menuItem->Caption;
}

//-----------SetMenuItemClickRecursive------------John Mertus----Sep 2002-----

    void TPopupComboBox::SetMenuItemClickRecursive(TMenuItem *mi)

//  This decends down into a menu list and attaches a click to the item
//
//****************************************************************************
{
  if (mi->Count == 0) mi->OnClick = DropDownMenuClick;
  for (int i=0; i < mi->Count; i++)
    SetMenuItemClickRecursive(mi->Items[i]);
}

//----------------SetDropDownMenu-----------------John Mertus----Sep 2002-----

    void __fastcall TPopupComboBox::SetDropDownMenu(TPopupMenu *Value)

//  This is called to set which menu to use for the dropdown
//  Value is the popup menu to use
//
//  Note:  This attaches a click to the menu items y change of the menu
// requires this to be called.
//
//****************************************************************************
{
   m_DropDownMenu = Value;

   if (m_DropDownMenu == 0)
   {
        return;
   }

   // If no menu, just exit
   if (m_DropDownMenu->ComponentCount <= 0) return;

   SetMenuItemClickRecursive((TMenuItem *)(m_DropDownMenu->Components[0]));
}

//-----------------DropDownClick------------------John Mertus----Sep 2002-----

     void __fastcall TPopupComboBox::DropDownClick(TObject *Sender)

//  This pops up the dropdown menu
// 
//****************************************************************************
{
  if (DropDownMenu != NULL)
    {
      TPoint p(Width, Height);
      TPoint sp = ClientToScreen(p);
      DropDownMenu->Popup(sp.x, sp.y);
    }
}

namespace Popupcombobox
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TPopupComboBox)};
                 RegisterComponents("MTI", classes, 0);
        }
}
//---------------------------------------------------------------------------

void __fastcall TMultiEdit::CreateParams(TCreateParams &Params)
{
    TEdit::CreateParams(Params);
    Params.Style = Params.Style | ES_MULTILINE | WS_CLIPCHILDREN;
}
  __fastcall TMultiEdit::TMultiEdit(TComponent* Owner)
        : TEdit(Owner)
{
}

