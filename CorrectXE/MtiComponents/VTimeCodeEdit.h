//---------------------------------------------------------------------------

#ifndef VTimeCodeEditH
#define VTimeCodeEditH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <Mask.hpp>
#include <StdCtrls.hpp>
#include "pTimecode.h"
#include "SharedMem.h"

#define DEFAULT_VTR -1

//---------------------------------------------------------------------------
class PACKAGE VTimeCodeEdit : public TCustomEdit
{
private:
   TNotifyEvent _OnEnter;

   // Common to VTimecodeHistory and VTimecodeEdit

   TNotifyEvent _OnTimecodeChange;
   TNotifyEvent _OnExit;
   TNotifyEvent _OnEnterKey;
   TNotifyEvent _OnEscapeKey;

   DEFINE_CBHOOK(TCChanged, VTimeCodeEdit);
   void CreateHint(void);
   bool _AllowVTRCopy;
   bool _InternalHint;
   String _AttachToName;

   TPopupMenu *_PopupMenu;

   void __fastcall setAllowNegative(bool Value) {tcP.setAllowNegative(Value);}
   bool __fastcall getAllowNegative(void) { return tcP.getAllowNegative(); }
   void __fastcall setSign(int Value) { tcP.setSign(Value);}
   int  __fastcall getSign(void) {return tcP.getSign();}

   void __fastcall SetTheHint(const String Value);
   String __fastcall GetTheHint(void);

   void __fastcall SetAllowVTRCopy(const bool Value);

   void CreatePopUp(void);
   void CreateSelectPopup(void);

   // PopupMenu callbacks
   void __fastcall PopupCopyFromVTR(TObject *Sender);
   void __fastcall PopupCopyToVTR(TObject *Sender);
   void __fastcall PopupCopy(TObject *Sender);
   void __fastcall PopupCut(TObject *Sender);
   void __fastcall PopupPaste(TObject *Sender);
   void __fastcall PopupUndo(TObject *Sender);
   void __fastcall PopupProperties(TObject *Sender);
   void __fastcall PopupSelectAll(TObject *Sender);
   void __fastcall PopupVTRChosen(TObject *Sender);
   void __fastcall PopupClick(TObject *Sender);
   void __fastcall VFocusExit(TObject *Sender);

   // just for reference
   TMenuItem *CopyFromVTRMenuItem;
   TMenuItem *CopyToVTRMenuItem;
   TMenuItem *CutMenuItem;
   TMenuItem *CopyMenuItem;
   TMenuItem *PasteMenuItem;
   TMenuItem *UndoMenuItem;
   TMenuItem *SelectAllMenuItem;
   TMenuItem *PropertiesMenuItem;
   TMenuItem *SelectVTRMenuItem;
   CVTRSharedMemoryList  VTRList;

protected:
    void __fastcall WmHint(TMessage &Message);
	void DYNAMIC __fastcall DoEnter(void);
	void DYNAMIC __fastcall DoExit(void);

BEGIN_MESSAGE_MAP
   MESSAGE_HANDLER(CM_HINTSHOW, TMessage, WmHint)
END_MESSAGE_MAP(TCustomEdit)

public:
   // Specific to VTimecodeEdit
   __fastcall VTimeCodeEdit(TComponent* Owner);
   void __fastcall VEnter(TObject *Sender);

   // Common to VTimecodeHistory and VTimecodeEdit
   PTimecode tcP;
   CTimecode _oldTimecode;
   void UpdateDisplay(void);

   void __fastcall VKeyPress(TObject *Sender, wchar_t &Key);
   void __fastcall VExit(TObject *Sender);

   bool CopyFromVTR(int vtr = DEFAULT_VTR);
   bool CopyFromVTRAvailable(void);
   bool CueToVTR(int vtr = DEFAULT_VTR);
   bool CueFromVTRAvailable(void);

    // I/O operators
   bool ReadSettings(CIniFile *ini, const string &section);
   bool WriteSettings(CIniFile *ini, const string &section);

   __property bool InternalHint = {read=_InternalHint, write=_InternalHint, default=true};
   __property int Sign = {read=getSign, write=setSign, default=1};

   // Awful hack to let us recives notifications from global CBHook in CTimecode
   void SetSharedMapMap(void *map);

__published:

   // Common to VTimecodeHistory and VTimecodeEdit

  __property AutoSelect;
  __property Anchors;
  __property BevelEdges;
  __property BevelInner;
  __property BevelKind;
  __property BevelOuter;
  __property Color;
  __property Constraints;
  __property Cursor;
  __property DragCursor;
  __property DragKind;
  __property DragMode;
  __property Enabled;
  __property Font;
  __property Height;
  __property HelpContext;
  __property HelpKeyword;
  __property HelpType;
  __property Left;
  __property ParentColor;
  __property ParentFont;
  __property ParentShowHint;
  __property ShowHint;
  __property TabOrder;
  __property TabStop;
  __property Tag;
  __property Top;
  __property Visible;
  __property Width;

  __property ReadOnly;

  // Overridden and additional properties
  __property bool AllowNegative = {read=getAllowNegative, write=setAllowNegative, default=false};
  __property bool AllowVTRCopy = {read=_AllowVTRCopy, write=SetAllowVTRCopy, default=true};
  __property AnsiString Hint = {read=GetTheHint, write=SetTheHint};

  __property TNotifyEvent OnDropFrame = {read=_OnTimecodeChange, write=_OnTimecodeChange};
  __property TNotifyEvent OnTimecodeChange = {read=_OnTimecodeChange, write=_OnTimecodeChange};
  __property TNotifyEvent OnExit = {read=_OnExit, write=_OnExit};
  __property TNotifyEvent OnEnterKey = {read=_OnEnterKey, write=_OnEnterKey};
  __property TNotifyEvent OnEscapeKey = {read=_OnEscapeKey, write=_OnEscapeKey};
  __property String AttachToName = {read=_AttachToName, write=_AttachToName};

  __property OnClick;
  __property OnDblClick;
  __property OnDragDrop;
  __property OnDragOver;
  __property OnEndDock;
  __property OnEndDrag;
  __property OnStartDock;
  __property OnStartDrag;

  // Different than VTimecodeHistroy
  __property TNotifyEvent OnEnter = {read=_OnEnter, write=_OnEnter};

};
//---------------------------------------------------------------------------
#endif
