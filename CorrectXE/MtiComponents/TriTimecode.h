/*
   File Name:  TriTimecode.h
   Created: Feb 3, 2001 by John Mertus
   Version 1.00

   This is a component that produces three timecode
      in
      out
      duration

   And does all the proper updating
*/

//---------------------------------------------------------------------------

#ifndef TriTimecodeH
#define TriTimecodeH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "VTimeCodeEdit.h"
//---------------------------------------------------------------------------
class PACKAGE TTriTimecode : public TCustomPanel
{
private:
   bool jDurationValid;

   TLabel *jInLabel;
   TLabel *jOutLabel;
   TLabel *jDurationLabel;
   bool    jFirstPaint;
   VTimeCodeEdit *InTC;
   VTimeCodeEdit *OutTC;
   VTimeCodeEdit *DurationTC;
   bool    jOutInclusive;
   bool    jAutoLimit;
   int     jLimitMin;
   int     jLimitMax;
   bool    jDurationDominant;
   int     jAtomicUpdate;

   TNotifyEvent fOnTCChange;
   TNotifyEvent fOnInvalidChange;

   void __fastcall InTimeCodeTimecodeChange(TObject *Sender);
   void __fastcall OutTimeCodeTimecodeChange(TObject *Sender);
   void __fastcall DurationTimeCodeTimecodeChange(TObject *Sender);
   void EnableHighlight(void);
   void DisableHighlight(void);

   PTimecode *GetIn(void) {return(&InTC->tcP);};
   void SetIn(PTimecode *Value) {InTC->tcP.Assign(*Value); InTC->UpdateDisplay();};

   PTimecode *GetOut(void) {return(&OutTC->tcP);};
   void SetOut(PTimecode *Value) {OutTC->tcP.Assign(*Value); OutTC->UpdateDisplay();};

   PTimecode *GetDuration(void) {return(&DurationTC->tcP);};
   void SetDuration(PTimecode *Value) {DurationTC->tcP.Assign(*Value); DurationTC->UpdateDisplay();};

   __fastcall bool GetOutInclusive(void) {return jOutInclusive;};
   __fastcall void SetOutInclusive(bool flag) {jOutInclusive = flag;};

   __fastcall String GetInCaption(void) {return(jInLabel->Caption);};
   __fastcall void SetInCaption(String Value) {jInLabel->Caption = Value;};

   __fastcall String GetOutCaption(void) {return(jOutLabel->Caption);};
   __fastcall void SetOutCaption(String Value) {jOutLabel->Caption = Value;};

   __fastcall String GetDurationCaption(void) {return(jDurationLabel->Caption);};
   __fastcall void SetDurationCaption(String Value) {jDurationLabel->Caption = Value;};

   __fastcall bool GetAutoLimit(void) {return jAutoLimit;};
   __fastcall void SetAutoLimit(bool flag) {jAutoLimit = flag;};

   __fastcall int GetLimitMin(void) {return jLimitMin;};
   __fastcall void SetLimitMin(int frameNumber) {jLimitMin = frameNumber;};

   __fastcall int GetLimitMax(void) {return jLimitMax;};
   __fastcall void SetLimitMax(int frameNumber) {jLimitMax = frameNumber;};

   DEFINE_CBHOOK(DurationValidChange, TTriTimecode);

protected:
   void HandleTimecodeChange();
   void SetDurationValid(bool Value);
   void EnforceLimits(void);

public:
   bool IgnoreChangeCB;     // True means the OnTChange is ignored

   RO_SPROPERTY(bool, DurationValid, jDurationValid, TTriTimecode);    // Duration valid property

   __fastcall TTriTimecode(TComponent* Owner);

   //  So user cannot resize
   void ComputeSizeAndWidth(void);
   virtual void __fastcall Paint(void);

  __property PTimecode *In = {read=GetIn, write=SetIn};
  __property PTimecode *Out = {read=GetOut, write=SetOut};
  __property PTimecode *Duration = {read=GetDuration, write=SetDuration};

__published:

  __property BevelInner;
  __property BevelOuter;
  __property Color;
  __property Font;
  __property String InCaption = {read=GetInCaption, write=SetInCaption};
  __property String OutCaption = {read=GetOutCaption, write=SetOutCaption};
  __property String DurationCaption = {read=GetDurationCaption, write=SetDurationCaption};
  __property bool OutInclusive = {write=SetOutInclusive, read=GetOutInclusive};
  __property bool AutoLimit = {write=SetAutoLimit, read=GetAutoLimit};
  __property int LimitMin = {write=SetLimitMin, read=GetLimitMin};
  __property int LimitMax = {write=SetLimitMax, read=GetLimitMax};

// Events
  __property TNotifyEvent OnTCChange = {read=fOnTCChange, write=fOnTCChange};
  __property TNotifyEvent OnInvalidChange = {read=fOnInvalidChange, write=fOnInvalidChange};

};
//---------------------------------------------------------------------------
#endif
