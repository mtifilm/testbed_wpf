//---------------------------------------------------------------------------

#ifndef RefTimeLineFrameH
#define RefTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>

#include "machine.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFRefTimeLine : public TFBaseTimeLine
{
__published:	// IDE-managed Components
        void __fastcall DataDisplayDblClick(TObject *Sender);


private:	// User declarations
		 virtual void ComputeBitmap(void);

	int _oldHeightBm = -1;
	int _oldWidthBm = -1;
	double _oldBeginFrame = -1.0;
	double _oldEndFrame = -1.0;
	double _oldMinFrame = -1.0;
	double _oldMaxFrame = -1.0;
	double _oldMidFrame = -1.0;
	double _oldNumFrames = -1.0;
	double _oldNoteResWidthInPixels = -1.0;

public:		// User declarations

        __fastcall TFRefTimeLine(TComponent* Owner);
                
};
//---------------------------------------------------------------------------
extern PACKAGE TFRefTimeLine *FRefTimeLine;
//---------------------------------------------------------------------------
#endif

