//---------------------------------------------------------------------------


#ifndef MasterTimeLineFrameH
#define MasterTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include "pTimecode.h"
#include "BookmarkTimeLineFrame.h"
#include "EventTimeLineFrame.h"
#include "RefTimeLineFrame.h"
#include "AudioTimeLineFrame.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include "TimelineTrackBar.h"
#include "TimeLine.h"
#include "GroupTimeLineFrame.h"
#include "AnimateTimeLineFrame.h"
#include <System.ImageList.hpp>


#define RESTART_MESSAGE  (WM_APP + 334)
#define MAX_AUDIO_CHANNELS 8

#define TIMELINE_HEIGHT_AUDIO_TRACK 36
#define TIMELINE_HEIGHT_EVENT_TRACK 36

typedef void __fastcall (__closure *TTimelineChanged)(TObject *Sender, double Frame);

//
// This defines the timelines by number
// WARNING, the GUI part of the system use these as hardcoded numbers
// Do NOT change the number unless you change
//    TAG index of the popup menu
//
enum  TIMELINE_TYPE
{tlTYPE_NONE=0,
tlTYPE_AUDIO,
tlTYPE_CADENCE,
tlTYPE_CUT,
tlTYPE_ANIMATE, // was tlTYPE_MARK,
tlTYPE_PANDS,
tlTYPE_LOGGER_PICTURE,
tlTYPE_LOGGER_AUDIO,
tlTYPE_SCENE_CUT,
tlTYPE_MARK, // was tlTYPE_REF,
tlTYPE_REF  // was tlTYPE_ANIMATE
};

class CTimeLineDefs
{
   public:
     CTimeLineDefs(TIMELINE_TYPE et, long d=0);
     CTimeLineDefs *Realize(void);

     TIMELINE_TYPE  EventType;           // Event number of timeline
     long Data;
     string Name;
     TFBaseTimeLine *TimeLine;


};

class CTimeLineDefsList : public  vector<CTimeLineDefs *>
{
  public:
     CTimeLineDefs *FindTimeLineDefs(TIMELINE_TYPE et, int d=0);
     CTimeLineDefs *FindTimeLineDefs(TFBaseTimeLine *btl);
     CTimeLineDefs *FindTimeLineDefs(const string &strName);
     bool Move(CTimeLineDefs *tl, CTimeLineDefsList &tlList, int Pos=-1);
     bool Move(TFBaseTimeLine *btl, CTimeLineDefsList &tlList, int Pos=-1);
     bool Move(TIMELINE_TYPE et, int d, CTimeLineDefsList &tlList, int Pos=-1);
     void MoveAllFrom(CTimeLineDefsList &tlList);
};

//---------------------------------------------------------------------------
class TFMasterTimeLine : public TFrame
{
__published:	// IDE-managed Components
        TImageList *WhatIsImageList;
        TPopupMenu *TimeLinePopup;
        TMenuItem *AudioMenuItem;
        TMenuItem *Ch1MenuItem;
        TMenuItem *Ch2MenuItem;
        TMenuItem *Ch6MenuItem;
        TMenuItem *Ch5MenuItem;
        TMenuItem *Ch4MenuItem;
        TMenuItem *Ch3MenuItem;
        TMenuItem *Ch8MenuItem;

        TMenuItem *Channel62;
        TMenuItem *N1;
        TMenuItem *ShowHideMenuItem;
        TMenuItem *HandlesMenuItem;
        //------------------------------

        TPanel *TrackBarPanel;
        TTimelineTrackBar *ResizeTrackBar;
        TSplitter *TrackBarSplitter;
        TTimelineTrackBar *TrackBar;
        TMenuItem *PictureEvents1;
        TMenuItem *AudioEvents1;
        TMenuItem *PictureEvent1;
        TMenuItem *Cuts1;
    TMenuItem *ReferenceMenuItem;
    TMenuItem *TimecodeMenuItem;
        TMenuItem *MarksMenuItem;
        TFGroupTimeLine *GroupTimeLine;
        TMenuItem *N2;
        TMenuItem *ShowAllAudio1;
        TMenuItem *HideAudioTracks1;
   TMenuItem *MaskAnimationMenuItem;
        void __fastcall TrackBarChange(TObject *Sender);
        void __fastcall TrackBarRangeChange(TObject *Sender);
        void __fastcall FrameResize(TObject *Sender);
        void __fastcall TimeLinePopupPopup(TObject *Sender);
        void __fastcall ReadMenuItemClick(TObject *Sender);
        void __fastcall HandlesMenuItemClick(TObject *Sender);
        void __fastcall FrameMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
        void __fastcall ResizeTrackBarChange(TObject *Sender);
        void __fastcall TrackBarKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall TrackBarCursorChange(TObject *Sender, int Value);
        void __fastcall ShowAllAudio1Click(TObject *Sender);
        void __fastcall HideAudioTracks1Click(TObject *Sender);
      	////void __fastcall GroupTimeLineResize(TObject *Sender);
        //------------------------------------------------------------------

private:	// User declarations

        TTimelineChanged _TimelineChanged;
        TTimelineChanged _OnPrevNextChange;
        TTimelineChanged _AudioSlipChanged;

        int   _CurrentFrame;
        bool  _ShowHandleData;
        int   _InhibitCallback;
		bool  _Scrolling;
        double _NonScrollingRange;
        bool _TrackBarInitTrigger;
        string _OldClipName;
		int _OldFramingSelection;
        
		double   _StartHandleCount;
        double   _EndHandleCount;
        bool  _AllowHandles;
        double   _AudioSlipIncrement;
        bool  _TrackBarAtTop;
        bool  _FirstResize;
        int _GroupTimeLineTop;
        int _SpaceGroupToTrackBar;
        int _BottomSpace;
		bool  _AutoSize;
		TColor _InitialResizeTrackColor;


        void CurrentFrameInternal(double Value);
        void ThumbPositionInternal(double Value);
		void BuildTimeLines(const CTimeLineDefsList &TimeLineDefsList);

		CTimeLineCommonData  _CommonData;
		CTimeLine            *_RawTimeLine;

        // These define the what time lines exist and what to use
        CTimeLineDefsList   ReservedTimeLines;
        CTimeLineDefsList   CurrentTimeLines;
        CTimeLineDefsList   DesignTimeLines;
        bool InitIt;

        DEFINE_CBHOOK(CommonDataChanged, TFMasterTimeLine);

        TNotifyEvent _OnChange;
        TNotifyEvent _OnSelectChange;
        TNotifyEvent _OnTrackRebuildChange;
        TMenuEventChange _OnMenuEventChange;

        double _ExPosition;
        bool   _UseExPostion;
        TDragEvent _OnScrub;
        int    _ResizePoint;
        int    _OldStartFrame;
        int    _OldStopFrame;

        void   SendNotificationIfVisibleFrameRangeChanged();

        void __fastcall SpecialDrag(TObject *Sender, int Value);

        double LinearWarpPtoV(double Value);
        double LinearWarpVtoP(double Value);
        double BiLinearWarpPtoV(double Value);
        double BiLinearWarpVtoP(double Value);

        double ResizeTrackBarWarpPtoV(double Value);
        double ResizeTrackBarWarpVtoP(double Value);

        void SetOnMenuEventChange(TMenuEventChange ChangeEvent);

        // These refer to the actual height of the object with all
        //   tracks shown.
        int groupTimeLineRequiredHeight;
        int masterTimeLineRequiredHeight;

        // This is to force the audio pop-up menu for the timeline to
        //   be hidden.
        bool showEmptyMenuF;

protected:
        void __fastcall WMShow(TMessage &Message);

        BEGIN_MESSAGE_MAP
          VCL_MESSAGE_HANDLER(WM_PAINT, TMessage, WMShow)
        END_MESSAGE_MAP(TFrame)


public:		// User declarations

        __fastcall TFMasterTimeLine(TComponent* Owner);
		__fastcall ~TFMasterTimeLine();
        double AudioSlip(double Value);
        double AudioSlipForceCB(double Value);
		double AudioSlip(void);
		void SetAutoRefresh(bool value);

        TTrackStyle   TrackStyle(TTrackStyle Value);
        TThumbStyle   ThumbStyle(TThumbStyle Value);
        TTrackStyle   TrackStyle(void);
        TThumbStyle   ThumbStyle(void);
        TColor   ThumbColor(TColor Value);
        TColor   TrackColor(TColor Value);
        TColor   ThumbColor(void);
        TColor   TrackColor(void);
        bool   ShowThumbDetent(bool Value);
        bool   ShowThumbDetent(void);

        void   Min(double Value);
        double Min(void);

        void   Max(double Value);
        double Max(void);

        void   NonScrollingRange(double Value);
        double NonScrollingRange(void);

        bool   TrackBarAtTop(bool Value);
        bool   TrackBarAtTop(void);

        void AllowHandles(bool Value);
        bool AllowHandles(void);

        bool DrawCenterLine(void);
        void DrawCenterLine(bool Value);

        double  CurrentFrame(void);
        void CurrentFrame(double Value);
        void CurrentFrameForceChangeCB(double Value);

        double  ThumbPosition(void);
        void  ThumbPosition(double Value);
        void ThumbFrameForceChangeCB(double Value);

        bool Scrolling(void);
        void Scrolling(bool Value);

        double  AudioSlipIncrement(void);
        double  AudioSlipIncrement(double Value);

        void FineDisplayBoundary(int Value);
        int  FineDisplayBoundary(void);

        MTI_INT32  TotalFrames(void);
        void TotalFrames(MTI_INT32 Value);

        CTimeLine *TimeLine(void);
        void TimeLine(CTimeLine *Value);

        void SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface);

        CBHook VisibleFrameRangeHasChanged;

        //Little Method added by Kea
        CTimeLineDefs *getLine(const string &findme, bool inCurrentTL);
        //--------------------------

        double  StartFrame(void);
        double  StopFrame(void);
        double CursorPosition(double Value);
        double CursorPosition(void);
        double CursorPosition(double Value, int nCur);
        double CursorPosition(int nCur);

        double  Frames(void);
        void Frames(double Value);

        void GetVisibleFrameIndexRange(int &inIndex, int &exclusiveOutIndex);

        void Select (enum TIMELINE_TYPE iType);

        void RedrawDisplay(void);
        void UpdateDisplay(void);
        __property TTimelineChanged TimelineChanged = {read=_TimelineChanged, write=_TimelineChanged};
        __property TTimelineChanged AudioSlipChanged = {read=_AudioSlipChanged, write=_AudioSlipChanged};
        __property TDragEvent OnScrub = {read=_OnScrub, write=_OnScrub};
        __property TTimelineChanged OnPrevNextChange = {read=_OnPrevNextChange, write=_OnPrevNextChange};

        void SetChangeEvent(TNotifyEvent Callback);
        TNotifyEvent GetChangeEvent(void);

        void SetAudioSlipChangeEvent(TTimelineChanged Callback);
        TTimelineChanged GetAudioSlipChangeEvent(void);

        vector<double> DebugIndividualTimes(void);

        virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool ReadDesignSettings(void);
        virtual bool ReadDesignSAP(void);

        // Expose some of the Timeline to the user
        bool SetClipParameters (const string &strClipNameArg,
		    int iVideoSelect=VIDEO_SELECT_NORMAL,
            int iAudioSelect=0,
            int iFramingSelect=FRAMING_SELECT_VIDEO);

        __property TNotifyEvent OnSelectChange = {read=_OnSelectChange, write=_OnSelectChange};
        __property TNotifyEvent OnTrackRebuildChange = {read=_OnTrackRebuildChange, write=_OnTrackRebuildChange};
        __property TMenuEventChange OnMenuEventChange = {read=_OnMenuEventChange, write=SetOnMenuEventChange};


        int NumberOfCurrentTimeLines(void);
        TFBaseTimeLine *CurrentTimeLine(int idx);

        int NumberOfSelectedTimeLines(void);
        TFBaseTimeLine *SelectedTimeLine(int idx);

        void ResizeTimeLines(void);

        double ResizePoint(double Value);
        double ResizePoint(void);

        void UpdateMenus(void);

        // BRIDGE CODE - renamed CurrentThumbFrame to be ThumbPosition()
        inline double CurrentThumbFrame(void) { return ThumbPosition(); };
        inline void CurrentThumbFrame(double Value) { ThumbPosition(Value); };
        //

        int GetMasterTimeLineRequiredHeight(void);
        int GetGroupTimeLineRequiredHeight(void);
        void SetGroupTimeLineHeight(int height);
        void SetTrackBarTop(int top);

        bool SetShowEmptyMenu(bool b);
        bool GetShowEmptyMenu(void);

        void setResizeTrackBarSoftEnable(bool onOff);
        bool getResizeTrackBarSoftEnable();
        void setTrackBarSoftEnable(bool onOff);
        bool getTrackBarSoftEnable();

};

//---------------------------------------------------------------------------
extern PACKAGE TFMasterTimeLine *FMasterTimeLine;
//---------------------------------------------------------------------------
#endif
