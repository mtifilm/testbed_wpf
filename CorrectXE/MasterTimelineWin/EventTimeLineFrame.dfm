inherited FEventTimeLine: TFEventTimeLine
  Height = 37
  ExplicitHeight = 37
  DesignSize = (
    498
    37)
  inherited Bevel1: TBevel
    Height = 35
    ExplicitHeight = 35
  end
  inherited NextButton: TSpeedButton
    Height = 33
    ExplicitHeight = 33
  end
  inherited DataDisplay: TPaintBox
    Height = 35
    ExplicitHeight = 35
  end
  inherited Bevel2: TBevel
    Height = 34
    ExplicitHeight = 34
  end
  inherited PreviousButton: TSpeedButton
    Height = 33
    ExplicitHeight = 33
  end
  inherited SelectButton: TSpeedButton
    Height = 32
    ExplicitHeight = 32
  end
  inherited BaseTimelinePopupMenu: TPopupMenu
    Left = 200
    Top = 8
    inherited N1: TMenuItem
      Visible = False
    end
    inherited N2: TMenuItem [6]
    end
    inherited N5: TMenuItem [12]
    end
    inherited ImportBookmarkEventsMenuItem: TMenuItem [13]
    end
    inherited ExportBookmarkEventsMenuItem: TMenuItem [14]
    end
  end
end
