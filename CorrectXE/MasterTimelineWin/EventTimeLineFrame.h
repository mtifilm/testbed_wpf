//---------------------------------------------------------------------------

#ifndef EventTimeLineFrameH
#define EventTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>

#include "machine.h"
#include <Dialogs.hpp>

#include <map>

//---------------------------------------------------------------------------
class TFEventTimeLine : public TFBaseTimeLine
{
__published:	// IDE-managed Components
        void __fastcall DataDisplayDblClick(TObject *Sender);
   void __fastcall AddColorBreakMenuItemClick(TObject *Sender);
   void __fastcall DropColorBreakMenuItemClick(TObject *Sender);

private:	// User declarations
       virtual void ComputeBitmap(void);

       bool _TicksAreCached;
       std::map<int, int> _TickMarkCache;
       std::map<int, AnsiString> _TickTcCache;
       int _TickCacheHeightBm;
       int _TickCacheWidthBm;
       int _TickCacheStartFrame;
       int _TickCacheEndFrame;

public:		// User declarations

      __fastcall TFEventTimeLine(TComponent* Owner);
      virtual void UpdateButtons(void);

      virtual void DrawEventSymbol(
               Graphics::TBitmap *bitmap,
               MTI_UINT64 event,
               int iXPos,
               int iFrame,
               int iFrameWidth);
};
//---------------------------------------------------------------------------
extern PACKAGE TFEventTimeLine *FEventTimeLine;
//---------------------------------------------------------------------------
#endif

