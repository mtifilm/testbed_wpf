//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "EventTimeLineFrame.h"
#include <math.h>    // for ceil() and floor()
#include <stdlib.h>


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma resource "*.dfm"
TFEventTimeLine *FEventTimeLine;
//---------------------------------------------------------------------------
__fastcall TFEventTimeLine::TFEventTimeLine(TComponent* Owner)
        : TFBaseTimeLine(Owner)
{
    EventMask = EVENT_NONE;                 // No Event
    _TicksAreCached = false;

    // QQQ - Need to figure out a better way to do this!
    Height = 21;
    DataDisplay->Color = TColor(0xFFFAF0);
}

//---------------------------------------------------------------------------

// For drawing TC reference lines
#define BIG_TICK 0.75
#define MEDIUM_TICK 0.33
#define SHORT_TICK 0.20
//---------------------------------------------------------------------------

//---------------ComputeBitmap------------------John Mertus----Apr 2003---

	 void TFEventTimeLine::ComputeBitmap(void)

// This draws the data into the frames
//
//****************************************************************************
{
  ////TRACE_3(errout << "<x>x<x>x<x>x<x> ENTER TFEventTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");

  TFBaseTimeLine::ComputeBitmap();

  // Do nothing if we have not been set up
  if (!HaveTimeLine()) return;
  if (Frames() < 2) return;

  // Save some typing
  TCanvas *DataCanvas = _OffScreenBitmap->Canvas;
  int HeightBm = _OffScreenBitmap->Height;
  int WidthBm = _OffScreenBitmap->Width;

   // Find the min/max where to look
   MTI_UINT64 tmp = MFEventMask;
   int MaxBit;
   for (MaxBit = 1; MaxBit < 65; MaxBit++)
     {
        tmp = tmp/2;
        if (tmp == 0) break;
     }
   tmp = MFEventMask;
   int MinBit;
   for (MinBit = 0; MinBit < 64; MinBit++)
     {
       if ((tmp & 1) != 0) break;
       tmp = tmp/2;
     }

   int iSF = StartFrame();
   int iEnd = StartFrame() + Frames();
   int iFrameWidth = WidthBm / Frames();

   // Keep in range
   if (iEnd > (int) Max() + 1) iEnd = Max() + 1;
   if (iSF < (int) Min()) iSF = Min();

   if (_TicksAreCached
   && (_TickCacheHeightBm != HeightBm
       || _TickCacheWidthBm != WidthBm
       || _TickCacheStartFrame != iSF
       || _TickCacheEndFrame != iEnd))
     {
       _TicksAreCached = false;
     }

   //====================== MARKS ===========================================
    int iIn = CommonData()->MarkIn();
    int iOut = CommonData()->MarkOut();
    if (iIn >= 0 && iOut > iIn)
      {
        // WTF!!!??! Could this be any stoopider?
        int xOffset = 0;
        if ((AdjustedMidFrame() - Min()) < Frames()/2)
          {
            xOffset = (int) floor((WidthBm)*(Frames()/2. - AdjustedMidFrame()
                                    + Min() /* + 0.5 */)  /((float)Frames())) - 1;
          }

        if (iIn >= iSF)
          {
            if (iIn >= iEnd)
               iIn = iEnd - 1;

            int x1 = xOffset - 1;
            int x2 = WidthBm * (iIn - iSF) / float(Frames()) - 1 + xOffset;
            if (x2 <= x1) x2 = x1 + 1;   // At least one pixel wide!

            TColor saveBrushColor = DataCanvas->Brush->Color;
            DataCanvas->Brush->Color = (TColor) 0x00E0E0E0;
            DataCanvas->Brush->Style = bsSolid;
            DataCanvas->FillRect(TRect(x1, 0, x2, HeightBm));
            DataCanvas->Brush->Color = saveBrushColor;
          }

        if (iOut < iEnd)
          {
            if (iOut < iSF)
               iOut = iSF;

            int x1 = WidthBm * (iOut - iSF) / float(Frames()) - 1 + xOffset;
            int x2 = WidthBm * (Max() + 1 - iSF) / float(Frames()) - 1 + xOffset;
            if (x2 <= x1) x2 = x1 + 1;   // At least one pixel wide!
            TColor saveBrushColor = DataCanvas->Brush->Color;
            DataCanvas->Brush->Color = (TColor) 0x00E0E0E0;
            DataCanvas->Brush->Style = bsSolid;
            DataCanvas->FillRect(TRect(x1, 0, x2, HeightBm));
            DataCanvas->Brush->Color = saveBrushColor;
          }
      }
   //====================== END MARKS ====================================

   // =============== Frame Cache Hack======================--==============
   if (ShowFrameCache())
     {
       const IntegerList &cachedFrameList = CommonData()->CachedFrameList();
	   for (int j = 0; j < (int)cachedFrameList.size();)
         {
           int runFirst = cachedFrameList[j];
           int runLast = runFirst;
           for (j = j + 1;
                (j < (int)cachedFrameList.size()) && (cachedFrameList[j] == runLast + 1);
                ++j)
             {
               runLast = cachedFrameList[j];
             }

           if (runFirst < iSF) runFirst = iSF;
           if (runLast >= iEnd) runLast = iEnd - 1;
           if (runLast < runFirst) runLast = runFirst;

           int x1 = DataDisplay->Width * (runFirst - StartFrame()) / float(Frames()) - 1;
           int x2 = DataDisplay->Width * (runLast + 1 - StartFrame()) / float(Frames()) - 1;
           if (x2 <= x1) x2 = x1 + 1;   // At least one pixel wide!
           TColor saveBrushColor = DataCanvas->Brush->Color;
           DataCanvas->Brush->Color = clMoneyGreen;
           DataCanvas->Brush->Style = bsSolid;
           DataCanvas->FillRect(TRect(x1, 0, x2, HeightBm/3));
           DataCanvas->Brush->Color = saveBrushColor;
         }
     }
   // =============== End Frame Cache Hack==================================

   //====================== TICKS ===========================================
   if (ShowTicks() && !_TicksAreCached)
     {
       _TicksAreCached = true;
       _TickMarkCache.clear();
       _TickTcCache.clear();
       _TickCacheHeightBm = HeightBm;
       _TickCacheWidthBm = WidthBm;
       _TickCacheStartFrame = iSF;
       _TickCacheEndFrame = iEnd;

       double BeginFrame = StartFrame();
       double EndFrame = StartFrame() + Frames();

       // Keep in range
       if (BeginFrame < Min())
         {
           BeginFrame = Min();
         }

       if (EndFrame > Max() + 1)
         {
           EndFrame = Max() + 1;
         }

       for (double frame = BeginFrame; frame < EndFrame; frame += 1.0)
         {
           if (frame < 0)
             {
               continue;
             }

           float iTickFrac = 0.;
           int x = WidthBm * (frame - BeginFrame) / float(Frames()) - 1;
           AnsiString note;
           int noteResWidthInPixels = DataCanvas->TextWidth("99:99:99:99.XX");
           double pixelsPerFrame = WidthBm / Frames();
           int noteResWidthInFrames = (int) ceil(noteResWidthInPixels / pixelsPerFrame);
           int frameOffset = (int) frame;
           int xOffset = 0;
           int pow2 = 1;
           int exp = 0;

           if ((AdjustedMidFrame() - Min()) < Frames()/2)
             {
               xOffset = (int) floor(WidthBm * (Frames() / 2.0 - AdjustedMidFrame() + Min()) / ((float) Frames())) - 1;
             }

           x += xOffset;

           for (exp = 0; pow2 < noteResWidthInFrames; ++exp)
             {
               pow2 <<= 1;
             }

           if ((frameOffset % pow2) == 0)
             {
               iTickFrac = BIG_TICK;
               note = GetVideoTC((int) frame).c_str();

               // Trim blanks off front of non-timecode timecodes
               // Careful!! Frickin' AnsiString indexes are 1-based!!!
               // For some reason, linker can't find AnsiString::SubString in XE5upd2
               while (note.Length() > 1 && AnsiString((AnsiStringToStdString(note).substr(1, 1)).c_str()) == " ")
                 note = AnsiString((AnsiStringToStdString(note).substr(2, note.Length() - 1)).c_str());
             }
           else
             {
               while ((pow2 >>= 1) > 1)
                 {
                   if (((frameOffset % pow2) == 0) && ((pixelsPerFrame * pow2) > 10.0))
                     {
                       iTickFrac = (pow2 > 2)? MEDIUM_TICK : SHORT_TICK;
                       break;
                     }
                   exp -= 1;
                 }
               if ((pow2 == 1) && (pixelsPerFrame > 10.0))
                 {
                   iTickFrac = SHORT_TICK;
                 }
             }

           int tickHeight = (int) (iTickFrac * HeightBm);
           std::map<int, int>::iterator iter = _TickMarkCache.find(x);
           if (iter == _TickMarkCache.end() || iter->second < tickHeight)
             {
                _TickMarkCache[x] = tickHeight;
             }

           if (note != "")
             {
               _TickTcCache[x + 3] = note;
             }
         }
     }

   if (ShowTicks())
     {
       DataCanvas->Pen->Color = (TColor) 0xB8B8B8;
       DataCanvas->Font->Color = (TColor) 0xB8B8B8;

       for (std::map<int, int>::iterator iter = _TickMarkCache.begin();
            iter != _TickMarkCache.end();
            ++iter)
         {
           const int &x = iter->first;
           int &tickHeight = iter->second;
           DataCanvas->MoveTo(x, HeightBm - tickHeight);
           DataCanvas->LineTo(x, HeightBm);
         }

       DataCanvas->Brush->Style = bsClear;
       for (std::map<int, AnsiString>::iterator iter = _TickTcCache.begin();
            iter != _TickTcCache.end();
            ++iter)
         {
           const int &x = iter->first;
           AnsiString &tc = iter->second;
           DataCanvas->TextOut(x, 0, tc);
         }

       DataCanvas->Brush->Style = bsSolid;
     }
   //====================== END TICKS ====================================

   // Multiframe event
   int i = -1;
   while (i < iEnd)
     {
       CMultiFrameEvent mfe = GetNextMultiFrameEvent (i, MFEventMask);
	   if (mfe.iFrame < 0) break;
	   i = std::max(mfe.iFrame, (int)Min());
	   if ((i < iEnd) || ((i + mfe.iNFrame) > iSF))
         {
            // Kludge: EventMaskShift needs to be done another way
            // This is wasteful timewise
            MTI_UINT64 Probe = 1;
            for (int j=0; j < MinBit; j++) Probe = 2*Probe;
            for (MTI_UINT64 j = MinBit; j < MaxBit; j++)
              {
                 if ((Probe & mfe.ullFlag) != 0)
                   {
                      int x = DataDisplay->Width*(i - StartFrame())/float(Frames()) - 1;
                      int x2 = DataDisplay->Width*(i + mfe.iNFrame - StartFrame())/float(Frames()) - 1;
                      DataCanvas->Brush->Color = (TColor)mfe.uiColor;
                      DataCanvas->FillRect(TRect(x, 0, x2, HeightBm));

                      if (mfe.bUnderlineAnnotation)
                       {
                        DataCanvas->Font->Style = TFontStyles() << fsUnderline;
                       }
                      else
                       {
                        DataCanvas->Font->Style = TFontStyles();
                       }
                      DataCanvas->Font->Color = (TColor)mfe.uiAnnotationColor;
                      // Draw the annotation
                      String s = mfe.strAnnotation.c_str();
                      int w = DataCanvas->TextWidth(s);
                      if (w < (x2-x))
                        {
                           DataCanvas->TextOut(((x2+x) - w)/2, (HeightBm - DataCanvas->TextHeight(s))/2, s);
                        }
                      else
                        {
                         s = mfe.strAnnotationShort.c_str();
                         w = DataCanvas->TextWidth(s);
                         if (w < (x2-x))
                          {
                             DataCanvas->TextOut(((x2+x) - w)/2, (HeightBm - DataCanvas->TextHeight(s))/2, s);
                          }
                        }
                   }
                 Probe = 2*Probe;
              }
         }

     }


    // Find the min/max where to look
   tmp = EventMask;
   for (MaxBit = 1; MaxBit < 65; MaxBit++)
     {
        tmp = tmp/2;
        if (tmp == 0) break;
     }
   tmp = EventMask;
   for (MinBit = 0; MinBit < 64; MinBit++)
     {
       if ((tmp & 1) != 0) break;
       tmp = tmp/2;
     }


     //sets the offset for special cases in delete event.

  // Single frame event
   for (MTI_INT32 i = iSF; i < iEnd;)
     {
       // Check the current event, after this, the event will always succeed
       MTI_TIMELINE_EVENT event = GetEventFlag(i) & EventMask;
       if (event != EVENT_NONE)
          {
            int x =DataDisplay->Width*(i-StartFrame())/float(Frames())-1;
            DrawEventSymbol (_OffScreenBitmap, event, x, i, iFrameWidth);
          }

       // Proceed to next event
       int iNext = GetNextEventFrameIndex(i, EventMask);
       // If no event, exit
       if (iNext == i) i = iEnd;
       else i = iNext;
     }

  ////TRACE_3(errout << "<x>x<x>x<x>x<x> EXIT TFEventTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");
}

void TFEventTimeLine::DrawEventSymbol(Graphics::TBitmap *bitmap,
		MTI_UINT64 llEvent, int iXPos, int iFrame, int iFrameWidth)
{
  // this function draws all symbols described by llEvent at position
  // iXPos in bitmap

  // Save some typing
  TCanvas *canvas = bitmap->Canvas;
  int iHeight = bitmap->Height;


    
  // the audio events
  if (llEvent & (EVENT_AUD_LTC_BREAK | EVENT_AUD_BOUNDARY))
   {
    // line from top to bottom
    canvas->Pen->Color = clRed;
    canvas->MoveTo(iXPos, 0);
    canvas->LineTo(iXPos, iHeight);
   }

  if (llEvent & EVENT_AUD_CLAP)
   {
    // line from top to 25%
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 0);
    canvas->LineTo(iXPos, iHeight/4);
   }

  if (llEvent & EVENT_AUD_TRIM_IN)
   {
    int iSize = iHeight / 4;
    if (iSize > 15)
      iSize = 15;

    // line on bottom
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 9*iHeight/10);
    canvas->LineTo(iXPos+iSize, 9*iHeight/10);

    // line on side
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 9*iHeight/10 - iSize);
    canvas->LineTo(iXPos, 9*iHeight/10);

   }

  // Draw cut line
  if(llEvent & EVENT_CUT_MASK)
   {
    // Darkish green
    canvas->Pen->Color = TColor(0x008000);
    canvas->MoveTo(iXPos, 0);
    canvas->LineTo(iXPos, iHeight);
    if (iFrameWidth >= 3)
    {
       canvas->MoveTo(iXPos+1, 0);
       canvas->LineTo(iXPos+1, iHeight);
    }
    if (iFrameWidth >= 4)
    {
       canvas->MoveTo(iXPos+2, 0);
       canvas->LineTo(iXPos+2, iHeight);
    }
   }

  if (llEvent & EVENT_BOOKMARK)
   {
    canvas->Pen->Color = clMaroon;
    canvas->MoveTo(iXPos, 0);
    canvas->LineTo(iXPos, iHeight);
   }


  if (llEvent & EVENT_AUD_TRIM_OUT)
   {
    int iSize = iHeight / 4;
    if (iSize > 15)
      iSize = 15;

    // line on bottom
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 9*iHeight/10);
    canvas->LineTo(iXPos-iSize, 9*iHeight/10);

    // line on side
   canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 9*iHeight/10);
   canvas->LineTo(iXPos, 9*iHeight/10 - iSize);
   }

  if (llEvent & EVENT_AUD_CLAP_OTHER)
   {
    // line from 90% to bottom
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 9*iHeight/10);
    canvas->LineTo(iXPos, iHeight);
   }


  // the picture events
  if (llEvent & EVENT_PIC_BOUNDARY)
   {
    // line from top to bottom
    canvas->Pen->Color = clRed;
    canvas->MoveTo(iXPos, 0);
    canvas->LineTo(iXPos, iHeight);
   }

  if (llEvent & EVENT_PIC_CLAP)
   {
    // line from 75% to bottom
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, 3*iHeight/4);
    canvas->LineTo(iXPos, iHeight);
   }

  if (llEvent & EVENT_PIC_TRIM_IN)
   {
    int iSize = iHeight / 4;
    if (iSize > 15)
      iSize = 15;

    // line on top
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, iHeight/10);
    canvas->LineTo(iXPos+iSize, iHeight/10);

    // line on side
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, iHeight/10);
    canvas->LineTo(iXPos, iHeight/10 + iSize);
   }

  if (llEvent & EVENT_PIC_TRIM_OUT)
   {
    int iSize = iHeight / 4;
    if (iSize > 15)
      iSize = 15;

    // line on top
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, iHeight/10);
    canvas->LineTo(iXPos-iSize, iHeight/10);

    // line on side
    canvas->Pen->Color = clBlack;
    canvas->MoveTo(iXPos, iHeight/10);
    canvas->LineTo(iXPos, iHeight/10 + iSize);
   }
}

void __fastcall TFEventTimeLine::DataDisplayDblClick(TObject *Sender)
{

  if (DblClickCallback)
   {
    DblClickCallback (Sender);

    // ignore mouse down
    bDblClickIgnoreMouse = true;
   }
}

// ---------------------------------------------------------------------------

void TFEventTimeLine::UpdateButtons(void)
{
   // TO be used if the menu changes from the base
   TFBaseTimeLine::UpdateButtons();

//   int i = ceil(_CommonData->CursorPosition() + OffsetFrame());
//   if (i >= 0)
//   {
//      if (EventMask & EVENT_CB_BREAK)
//      {
//         DropEventMenuItem->Enabled = ((GetEventFlag(i) & EVENT_CB_BREAK) != 0);
//         AddEventMenuItem->Enabled =  ((GetEventFlag(i) & EventMask) != 0);
//      }
//      else
//      {
//         DropEventMenuItem->Enabled = false;
//         AddEventMenuItem->Enabled = false;
//      }
//   }
}

// ---------------------------------------------------------------------------

void __fastcall TFEventTimeLine::AddColorBreakMenuItemClick(TObject *Sender)
{
   int frame = ceil(_CommonData->CursorPosition() + OffsetFrame());
   HandleAddEventRequest(frame, EVENT_CB_BREAK);
}
// ---------------------------------------------------------------------------


void __fastcall TFEventTimeLine::DropColorBreakMenuItemClick(TObject *Sender)
{
   int frame = ceil(_CommonData->CursorPosition() + OffsetFrame());
   HandleAddEventRequest(frame, EVENT_CB_BREAK);
}

