//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BookmarkTimeLineFrame.h"

#include "BookmarkManager.h"
#include "CSV.h"
#include "JobManager.h"
#include "mti_event.h"
#include "MTIDialogs.h"
#include "ShowModalDialog.h"

#include <stdlib.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma link "BaseTimeLineFrame"
#pragma resource "*.dfm"
TBookmarkTimeLine *BookmarkTimeLine;
//---------------------------------------------------------------------------
__fastcall TBookmarkTimeLine::TBookmarkTimeLine(TComponent* Owner)
   : TFEventTimeLine(Owner)
{
}
//---------------------------------------------------------------------------

void TBookmarkTimeLine::GoToPreviousEvent(void)
{
   int frameIndex = GoToPreviousEventInternal();

   BookmarkManager bookmarkManager;
   bookmarkManager.SelectBookmarkAt(frameIndex);
}
//---------------------------------------------------------------------------

void TBookmarkTimeLine::GoToNextEvent(void)
{
   int frameIndex = GoToNextEventInternal();

   BookmarkManager bookmarkManager;

   // Workaround for a bug: sometimes 0 is not really a bookmark
   if (frameIndex == 0 && !bookmarkManager.DoesFrameHaveBookmarkInfo(0))
   {
      frameIndex = GoToNextEventInternal();
   }

   bookmarkManager.SelectBookmarkAt(frameIndex);
}
//---------------------------------------------------------------------------

void TBookmarkTimeLine::ImportBookmarkEvents()
{
   if (!HaveTimeLine())
      return;

   bool deleteOldBookmarks = true;
   BookmarkManager bookmarkManager;
   if (bookmarkManager.DoAnyFramesHaveBookmarkInfo())
   {
      MTIostringstream os;
      os << "This clip already has bookmarks events. Do you want    " << endl
         << "to delete the existing events before importing?" << endl;
      switch (_MTICancelConfirmationDialog(os.str()))
      {
         case MTI_DLG_OK:
            break;

         case MTI_DLG_NO:
            deleteOldBookmarks = false;
            break;

         case MTI_DLG_CANCEL:
            return;
      }
   }

   // Ask the job manager to suggest where "reports" are stored.
   JobManager jobManager;
   string suggestedFolder   = jobManager.GetJobFolderSubfolderPath(JobManager::reportsType);
   string suggestedFilename = "*.csv";
   ImportBookmarkEventsOpenDialog->InitialDir = suggestedFolder.c_str();
   ImportBookmarkEventsOpenDialog->FileName   = suggestedFilename.c_str();

   bool retVal = ImportBookmarkEventsOpenDialog->Execute();
   if (!retVal)
   {
      return; // cancelled
   }

   string filename = StringToStdString(ImportBookmarkEventsOpenDialog->FileName);
   if (filename.empty())
   {
      return;
   }

   CsvReader csvReader(filename);

   // First row of the CSV file MUST BE the headers!
   stringlist headers;
   int result = csvReader.GetRow(headers);
   if (result != CSV_SUCCESS)
   {
      MTIostringstream ostr;
      ostr << "Can't open file " << filename << " for reading    ";
      TRACE_0(errout << "ERROR: BookmarkTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   // OK, we're committed, so get rid of old bookmarks if indicated.
   if (deleteOldBookmarks)
   {
      DeleteAllEvents(DefaultEvent);
   }

   stringlist row;
   int inRangeCount = 0;
   int outOfRangeCount = 0;
   int invalidCount = 0;
   while (true)
   {
      int result = csvReader.GetRow(row);
      if (result == CSV_FILE_EOF)
      {
         break;
      }
      else if (result != CSV_SUCCESS)
      {
         MTIostringstream ostr;
         ostr << "Got an error while reading file " << filename;
         TRACE_0(errout << "ERROR: BookmarkTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         break;
      }

      BookmarkInfo *bookmarkInfo = bookmarkManager.CreateBookmarkInfo(headers, row);
      switch (bookmarkManager.AddBookmark(*bookmarkInfo))
      {
         case BOOKMARK_ADDED_SUCCESSFULLY:
            ++inRangeCount;
            break;

         case BOOKMARKINFO_OUT_OF_RANGE:
            ++outOfRangeCount;
            break;

         case BOOKMARKINFO_NOT_VALID:
         default:
            ++invalidCount;
            break;
      }

      delete bookmarkInfo;
      bookmarkInfo = NULL;
   }

   RedrawDisplay();

   if (invalidCount)
   {
      string qualifier = ((inRangeCount + outOfRangeCount) > 0)
                         ? string("some")
                         : string("any");
      _MTIWarningDialog(Handle, "Timecode was not found for " + qualifier + " events!    ");
   }

   if (outOfRangeCount)
   {
      string qualifier = ((inRangeCount + invalidCount) > 0)
                         ? string("Some")
                         : string("All");
      _MTIWarningDialog(Handle, qualifier + " event timecodes were outside the range for this clip!   ");
   }
}
//---------------------------------------------------------------------------

void TBookmarkTimeLine::ExportBookmarkEvents(void)
{
   if (!HaveTimeLine())
      return;

   // Ask the job manager to suggest where "reports" are stored.
   JobManager jobManager;
   string suggestedFolder   = jobManager.GetJobFolderSubfolderPath(JobManager::reportsType);
   string suggestedFilename = jobManager.GetJobFolderFileSaveName(suggestedFolder, "bm", true, "csv");
   ExportBookmarkEventsSaveDialog->InitialDir = suggestedFolder.c_str();
   ExportBookmarkEventsSaveDialog->FileName   = suggestedFilename.c_str();

   bool retVal = ExportBookmarkEventsSaveDialog->Execute();
   if (!retVal)
   {
      return; // cancelled
   }

   string filename = StringToStdString(ExportBookmarkEventsSaveDialog->FileName);
   if (filename.empty())
   {
      return;
   }

   MTI_UINT64 EventFlag = EventMask; // all visible marks for this timeline

   CsvWriter csvWriter(filename);

   BookmarkManager bookmarkManager;
   stringlist row;
   BookmarkInfo::GetCsvHeaderFields(row);
   int result = csvWriter.PutRow(row);
   if (result != CSV_SUCCESS)
   {
      MTIostringstream ostr;
      ostr << "Can't open file " << filename << " for writing    ";
      TRACE_0(errout << "ERROR: BookmarkTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   int nextFrame = -1;
   int oldFrame = -1; // make sure we look at frame 0!

   while (oldFrame < (nextFrame = GetNextEventFrameIndex(oldFrame, EventFlag)))
   {
      oldFrame = nextFrame;

      BookmarkManager bookmarkManager;
      BookmarkInfo bookmarkInfo;
      result = bookmarkManager.GetBookmarkInfo(nextFrame, bookmarkInfo);
      if (result != 0)
      {
         // QQQ The one at frame index zero mey be bogus!
         if (nextFrame == 0)
         {
            continue;
         }

         MTIostringstream ostr;
         ostr << "Can't get bookmark at frame " << nextFrame << " from file " << filename << " (error code " << retVal << ")";
         TRACE_0(errout << "ERROR: BookmarkTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }

      bookmarkInfo.GetCsvRowFields(row);
      result = csvWriter.PutRow(row);
      if (result != CSV_SUCCESS)
      {
         MTIostringstream ostr;
         ostr << "Can't write to file " << filename << "    ";
         TRACE_0(errout << "ERROR: BookmarkTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }
   }
}
//---------------------------------------------------------------------------
//
// In the Bookmark MEGAHACK-O-RAMA, we use an INI FILE in the master clip to
// store bookmark events. No events are ever written to the time track!!
//
bool TBookmarkTimeLine::HaveTimeLine()
{
   return TimeLine() != NULL;
}

int TBookmarkTimeLine::GetTotalFrameCount()
{
   return TimeLine()->getTotalFrameCount();
}

string TBookmarkTimeLine::GetVideoTC(int frameIndex)
{
   return TimeLine()->getVideoTC(frameIndex);
}

int TBookmarkTimeLine::GetVideoFrameIndex(CTimecode timecode)
{
   return TimeLine()->getVideoFrameIndex(timecode);
}

int TBookmarkTimeLine::GetPreviousEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask)
{
   if ((eventMask & EVENT_BOOKMARK) == 0)
   {
      return currentFrameIndex;
   }

   BookmarkManager bookmarkManager;
   return bookmarkManager.GetPreviousBookmarkFrameIndex(currentFrameIndex);
}

int TBookmarkTimeLine::GetNextEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask)
{
   if ((eventMask & EVENT_BOOKMARK) == 0)
   {
      return currentFrameIndex;
   }

   BookmarkManager bookmarkManager;
   return bookmarkManager.GetNextBookmarkFrameIndex(currentFrameIndex);
}

MTI_TIMELINE_EVENT TBookmarkTimeLine::GetEventFlag(int frameIndex)
{
   BookmarkManager bookmarkManager;
   return bookmarkManager.DoesFrameHaveBookmarkInfo(frameIndex)
                          ? EVENT_BOOKMARK
                          : EVENT_NONE;
}

void TBookmarkTimeLine::DeleteEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag)
{
   BookmarkManager bookmarkManager;
   bookmarkManager.DeleteBookmark(frameIndex);
}

void TBookmarkTimeLine::AddEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag)
{
   BookmarkManager bookmarkManager;
   bookmarkManager.AddBookmark(frameIndex);
}

CMultiFrameEvent TBookmarkTimeLine::GetNextMultiFrameEvent(int frameIndex, MTI_TIMELINE_EVENT mfEventMask)
{
   return TimeLine()->getNextMultiFrameEvent(frameIndex, mfEventMask);
}

void TBookmarkTimeLine::DrawEventSymbol(Graphics::TBitmap *bitmap,
		MTI_UINT64 llEvent, int iXPos, int iFrame, int iFrameWidth)
{
   TCanvas *canvas = bitmap->Canvas;
   int iHeight = bitmap->Height;
   int rectOffset = 1;

   canvas->Pen->Color = clMaroon;
   canvas->MoveTo(iXPos, 0);
   canvas->LineTo(iXPos, iHeight);

   if (iFrameWidth >= 3)
   {
      canvas->MoveTo(iXPos + 1, 0);
      canvas->LineTo(iXPos + 1, iHeight);
      ++rectOffset;
   }

   if (iFrameWidth >= 4)
   {
      canvas->MoveTo(iXPos + 2, 0);
      canvas->LineTo(iXPos + 2, iHeight);
      ++rectOffset;
   }

   BookmarkManager bookmarkManager;
   int selectedFrame = bookmarkManager.GetSelectedBookmarkFrameIndex();
   if (selectedFrame != iFrame)
   {
      return;
   }

   BookmarkInfo bookmarkInfo;
   int retVal = bookmarkManager.GetBookmarkInfo(iFrame, bookmarkInfo);
   if (retVal != 0)
   {
      return;
   }

   int duration = (bookmarkInfo.OutTimecode - bookmarkInfo.InTimecode);
   if (duration < 1)
   {
      return;
   }

   if ((iFrame + duration - StartFrame()) >= Frames())
   {
      duration = (Frames() + StartFrame()) - iFrame - 1;
   }

   int x1 = DataDisplay->Width * (iFrame - StartFrame()) / float(Frames()) + rectOffset - 1;
   int x2 = DataDisplay->Width * (iFrame + duration + 1 - StartFrame()) / float(Frames()) - 1;
   if (x2 <= x1)
   {
      return;
   }

   TColor saveBrushColor = canvas->Brush->Color;
   canvas->Brush->Color = TColor(0x00D0C0C8);
   canvas->Brush->Style = bsSolid;
   canvas->FillRect(TRect(x1, 0, x2, iHeight));
   canvas->Brush->Color = saveBrushColor;
}
//---------------------------------------------------------------------------//---------------------------------------------------------------------------
