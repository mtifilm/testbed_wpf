//---------------------------------------------------------------------------
/*
   How drawing works.
      All timelines are drawn to an offscreen bitmap (OffScreenBitmap).
      The paint function draws this bitmap onto the canvas.

      This bitmap width and height are each two pixels smaller than the display width and height.
      The extra pixels are used to draw the frame around the display.

      The derived classes need to define a function ComputeBitmap.

      When this function is called, the derived class's responsibility is
      to generate the display from its own data, the size of the bitmap.


      HOW THE DISPLAY UPDATES.

      Externally, the Redraw invalids the display and sets the
      RecomputeFlag.

      When the repaint is done, this flag is checked,
      if true, the ComputeBitmap function is called.

      The bitmap is then displayed on the screen.
      Note: This means that multiple changes in the waveform display can happen
      in a loop and only after the message loop has control, is the bitmap
      recomputed.

      TO FORCE AN UPDATE
        Call repaint, you only need to call it once for the mastertimeline, e.g,
         MasterTimeLine->Repaint();
*/


#ifndef BaseTimeLineFrameH
#define BaseTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "IniFile.h"
#include "TimeLine.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include "HRTimer.h"
#include "TimelineTrackBar.h"
#include <Dialogs.hpp>

#include <vector>
using std::vector;

// Enum for different actions
enum EMenuEventAction
{
  EVENT_ACTION_INVALID = 0,     // Usual invalid actions
  EVENT_ACTION_ADD = 1,         // Add a new event
  EVENT_ACTION_DELETE = 2,      // Delete a new event
  EVENT_ACTION_NEXT = 3,        // Goto Next event
  EVENT_ACTION_PREVIOUS = 4     // Goto Previous Event
};

typedef void __fastcall (__closure *TMenuEventChange)(TObject *Sender, EMenuEventAction Action, MTI_UINT64 &EventFlag, int &Frame, bool &Continue);

// This quick and dirty class stops any windows
// refreshes

class TFlickerPanel : public TPanel
{
protected:
        void __fastcall WMEraseBkgnd(TMessage &Message);
        BEGIN_MESSAGE_MAP
          VCL_MESSAGE_HANDLER(WM_ERASEBKGND , TMessage, WMEraseBkgnd)
        END_MESSAGE_MAP(TPanel)
public:
   __fastcall TFlickerPanel(TComponent* Owner);

};
//
//  This is the common data class for the global values
//  The data lives in the Master class
//  The base class has access to these
//
class TFBaseTimeLine;

class CTimeLineCommonData : public CBHook
{
  private:
    TFBaseTimeLine  *_ActiveTL;      // Which timeline is active
    double  _MidFrame;               // Frame to which timelines are centered
    double  _Frames;                 // Frames to display
    double  _Min;                    // The first VISIBLE timeline frame
    double  _Max;                    // The last VISIBLE timeline frame + 1
    double  _MarkIn;                 // The Mark In frame
    double  _MarkOut;                // The Mark Out frame
    bool    _DrawCenterLine;         // ** DEPRECATED **
    CTimelineCursors *_Cursors;
    int     *ActiveCursor;
	 int    _FineDisplayBoundary;
    IntegerList _CachedFramesList;   // List of indices of cached frames

  public:
    CTimeLine  *_TimeLine;
    TMessage   Message;                  // Optional Message
	double     dValue;                   // Optional double value on Message

	bool AutoPaint;						// Suppress the Paint message

    void HideMe(TFBaseTimeLine *btl);
    void AddMe(TFBaseTimeLine *btl);
    void RecomputeAllDisplays(void);

    // Access functions
    TFBaseTimeLine  *ActiveTL(void);
    void ActiveTL(TFBaseTimeLine *tl);

    double Frames(void);
    void Frames(double Value);

    double MidFrame(void);
    void MidFrame(double Value);

    bool DrawCenterLine(void);
    void DrawCenterLine(bool Value);

    double Max(void);
    void Max(double Value);

    double Min(void);
    void Min(double Value);

    double MarkIn(void);
    void MarkIn(double Value);

    double MarkOut(void);
    void MarkOut(double Value);

    void Cursors(CTimelineCursors *cur);
    CTimelineCursors * Cursors(void);

	CTimeLineCommonData(void);
    void Changed(void);

    int   FineDisplayBoundary(void);
    void  FineDisplayBoundary(int Value);

    void CursorPosition(double Value);
    double CursorPosition(void);

    const IntegerList &CachedFrameList(void);
    void CachedFrameList(const IntegerList &List);

    void NextPrevClick(TFBaseTimeLine *tl, int iFrame);
};

//---------------------------------------------------------------------------
class TFBaseTimeLine : public TFrame
{
__published:	// IDE-managed Components
        TBevel *Bevel1;
        TSpeedButton *NextButton;
        TPaintBox *DataDisplay;
        TBevel *Bevel2;
        TSpeedButton *PreviousButton;
   TPopupMenu *BaseTimelinePopupMenu;
        TMenuItem *NextEventMenuItem;
        TMenuItem *PreviousEventMenuItem;
        TMenuItem *Hide;
        TSpeedButton *SelectButton;
        TMenuItem *DropEventMenuItem;
        TMenuItem *AddEventMenuItem;
        TMenuItem *N1;
        TMenuItem *N2;
   TMenuItem *ImportCutListMenuItem;
   TMenuItem *ExportCutListSubMenu;
        TMenuItem *ClearAllMarksMenuItem;
        TMenuItem *RescanforSceneCutsMenuItem;
        TOpenDialog *ImportTimecodesOpenDialog;
        TSaveDialog *ExportTimecodesSaveDialog;
        TMenuItem *N3;
    TMenuItem *TicksMenuItem;
    TMenuItem *FrameCacheMenuItem;
    TMenuItem *N4;
   TMenuItem *ExportBookmarkEventsMenuItem;
   TMenuItem *ImportBookmarkEventsMenuItem;
   TMenuItem *BrowseToReportsFolderMenuItem;
   TMenuItem *N5;
   TMenuItem *ImportEDLMenuItem;
   TMenuItem *N6;
   TMenuItem *ExportCutListAsTimecodesMenuItem;
   TMenuItem *ExportCutListAsFrameNumbersMenuItem;
   TMenuItem *N7;
   TMenuItem *ExportEdlSubMenu;
   TMenuItem *ExportEdlAsTimecodesMenuItem;
   TMenuItem *ExportEdlAsFrameNumbersMenuItem;

        void __fastcall DrawCursorsOnCanvas();
		void __fastcall DataDisplayPaint(TObject *Sender);
        void __fastcall MaybeDataDisplayPaint(TObject *Sender);
        void __fastcall FrameResize(TObject *Sender);
        void __fastcall SelectButtonClick(TObject *Sender);
        void __fastcall PreviousButtonClick(TObject *Sender);
        void __fastcall NextButtonClick(TObject *Sender);
        void __fastcall DataDisplayMouseUp(TObject *Sender,
        TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall HideClick(TObject *Sender);
        void __fastcall FrameMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall FrameMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall DataDisplayDblClick(TObject *Sender);
        void __fastcall DropEventMenuItemClick(TObject *Sender);
        void __fastcall AddEventMenuItemClick(TObject *Sender);
        void __fastcall BaseTimelinePopupMenuPopup(TObject *Sender);
        void __fastcall ClearAllMarksMenuItemClick(TObject *Sender);
        void __fastcall RescanforSceneCutsMenuItemClick(TObject *Sender);
        void __fastcall ImportCutListMenuItemClick(TObject *Sender);
    void __fastcall TicksMenuItemClick(TObject *Sender);
    void __fastcall FrameCacheMenuItemClick(TObject *Sender);
   void __fastcall ExportBookmarkEventsMenuItemClick(TObject *Sender);
   void __fastcall ImportBookmarkEventsMenuItemClick(TObject *Sender);
   void __fastcall BrowseToReportsFolderMenuItemClick(TObject *Sender);
   void __fastcall ImportEDLMenuItemClick(TObject *Sender);
   void __fastcall ExportCutListAsTimecodesMenuItemClick(TObject *Sender);
   void __fastcall ExportCutListAsFrameNumbersMenuItemClick(TObject *Sender);
   void __fastcall ExportEdlAsTimecodesMenuItemClick(TObject *Sender);
   void __fastcall ExportEdlAsFrameNumbersMenuItemClick(TObject *Sender);
private:	// User declarations
        // Bitmap

        double _OffsetFrame;
        CTimeLineCommonData  _LocalCommonData;      // In case common data is not wanted

        TColor  BackGroundColor;
		  bool _Tracking;

        TFlickerPanel *HidePanel;

        TMenuEventChange _OnMenuEventChange;
        CHRTimer HRT;

        DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);

        void ExportEdl(bool asTimecodes);
        void ExportCutList(bool asTimecodes);

protected:
        virtual void UpdateButtons(void);
        CTimeLineCommonData  *_CommonData;

        bool bDblClickIgnoreMouse;
        void (*DblClickCallback) (TObject *Sender);
        virtual void ComputeBitmap(void);
        Graphics::TBitmap    *_OffScreenBitmap;
        bool                  _RecomputeFlag;
        bool                  _ShowTicks;
        bool                  _ShowFrameCache;

        void __fastcall WMGetDlgCode(TWMGetDlgCode &Message);
        BEGIN_MESSAGE_MAP
          VCL_MESSAGE_HANDLER(WM_GETDLGCODE, TWMGetDlgCode, WMGetDlgCode)
        END_MESSAGE_MAP(TFrame)

        int GoToPreviousEventInternal(void);
        int GoToNextEventInternal(void);

        virtual void ExportBookmarkEvents();
        virtual void ImportBookmarkEvents();

        // Overridable Timeline stuff for Bookmark MEGAHACK-O-RAMA.
        virtual bool HaveTimeLine();
        virtual int GetTotalFrameCount();
        virtual string GetVideoTC(int frameIndex);
        virtual int GetVideoFrameIndex(CTimecode timecode);
        virtual int GetPreviousEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask);
        virtual int GetNextEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask);
        virtual MTI_TIMELINE_EVENT GetEventFlag(int frameIndex);
        virtual void DeleteEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag);
        virtual void AddEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag);
        virtual CMultiFrameEvent GetNextMultiFrameEvent(int frameIndex, MTI_TIMELINE_EVENT mfEventMask);

        // Uggh, not virtual because it needs to use private _OnMenuEventChange. QQQ
        void DeleteAllEvents(MTI_TIMELINE_EVENT eventFlag);

        void UseTimelineToAddEvent(MTI_TIMELINE_EVENT EventFlag);
        void UseTimelineToDropEvent(MTI_TIMELINE_EVENT EventFlag);

public:		// User declarations
        vector<TColor> SymbolColor;
        void CommonComputeBitmap(void);

        // TEMPORARY until Events are defined
        // [NOT SURE WHAT THAT MEANS!?!]
        MTI_TIMELINE_EVENT  EventMask;
        MTI_TIMELINE_EVENT  MFEventMask;
        MTI_TIMELINE_EVENT  DefaultEvent;
        int Channel;
         //  End of temporary

        //Kea added this.------------------------------
        bool isActiveTimeline(TFBaseTimeLine* tl);
        //----------------------------------------------

        void OnMenuEventChange(TMenuEventChange ChangeEvent);
        TMenuEventChange OnMenuEventChange(void);

        void HandleDeleteEventRequest(int frame, MTI_TIMELINE_EVENT eventMask);
        void HandleAddEventRequest(int frame, MTI_TIMELINE_EVENT eventMask);

        __fastcall TFBaseTimeLine(TComponent* Owner);
        __fastcall ~TFBaseTimeLine(void);

        bool  DrawCenterLine(bool Value);
        bool  DrawCenterLine(void);

        CTimeLine *TimeLine(void);
        void TimeLine(CTimeLine *Value);

        double Frames(void);
        void   Frames(double Value);

        double StartFrame(void);
        double OffsetFrame(void);
        void   OffsetFrame(double Value);

        double NominalMidFrame(void);         // The center frame that was set
        void   NominalMidFrame(double Value);
        double AdjustedMidFrame(void);        // includes the Offset

        void  CommonData(CTimeLineCommonData  *_Common);
        CTimeLineCommonData  *CommonData(void);
        double TotalFrames(void);

        // Make the commondata interface cleaner
        double Max(void);
        void Max(double Value);

        double Min(void);
        void Min(double Value);

        bool ShowTicks();
        void ShowTicks(bool Value);

        bool ShowFrameCache();
        void ShowFrameCache(bool Value);

        virtual void RedrawDisplay(void);
        virtual void DrawCursors(TCanvas *Canvas);

        // Debugging timing values
        double PaintTime;
        double BuildBitmapTime;
        double BlitBitmapTime;
        int    Builds;
        int    Paints;
        void setDblClickCallback (void (*DblClickCallback)(TObject *Sender));
        
        virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool ReadDesignSettings(void);
        virtual bool ReadDesignSAP(void);

        // Overridable Prev/Next Button Click handlers
        virtual void GoToPreviousEvent(void);
        virtual void GoToNextEvent(void);
};

//---------------------------------------------------------------------------
extern PACKAGE TFBaseTimeLine *FBaseTimeLine;
//---------------------------------------------------------------------------
#endif
