//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MasterTimelineTesterUnit.h"
#include "HRTimer.h"
#include "BinManagerGUIUnit.h"
#include "MTIWinInterface.h"
#include <math.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma link "EventTimeLineFrame"
#pragma link "AudioTimeLineFrame"
#pragma link "MasterTimeLineFrame"
//#pragma link "CSPIN"
#pragma link "MTIUNIT"
#pragma link "HistoryMenu"
#pragma link "MTIDBMeter"
#pragma link "MTITrackBar"
#pragma link "VTimeCodeEdit"
#pragma link "TimelineTrackBar"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TMTIForm(Owner)
{
     // Create the symbolic drop down boxes
     TrackStyleCBox->Items->Clear();
     for (int i=0; i < TrackStyleAssociations.Size(); i++)
       TrackStyleCBox->Items->Add(TrackStyleAssociations.DisplayNameByIndex(i).c_str());

     ThumbStyleCBox->Items->Clear();
     for (int i=0; i < ThumbStyleAssociations.Size(); i++)
       ThumbStyleCBox->Items->Add(ThumbStyleAssociations.DisplayNameByIndex(i).c_str());

     ActiveDragComboBox->Items->Clear();
     for (int i=0; i < ActiveDragAssociations.Size(); i++)
       ActiveDragComboBox->Items->Add(ActiveDragAssociations.DisplayNameByIndex(i).c_str());
     ActiveDragComboBox->ItemIndex = 1;

     MasterTimeLine->OnPrevNextChange = OnPrevNextChange;
     TrackBitmap = new Graphics::TBitmap();
     SavedBitmap = new Graphics::TBitmap();
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
     MasterTimeLine->CurrentThumbFrame(0);
     MasterTimeLine->SetAudioSlipChangeEvent(AudioSlipChange);
     MasterTimeLine->OnScrub = OnScrub;
     AtTopCBox->Checked = MasterTimeLine->TrackBarAtTop();
     // Load all the persistent information
     HistoryMenu->RebuildMenu();

     CIniFile *ini = new CIniFile(GetIniFileName());
     RestoreOnStartupMenuItem->Checked = ini->ReadBool(GetIniSection(), "RestoreOnStartup", RestoreOnStartupMenuItem->Checked);
     DeleteIniFile(ini);

     if (RestoreOnStartupMenuItem->Checked)
        RestoreAllProperties();

     // Load the history
     while (!HistoryMenu->HistoryList.empty())
      {
        if (LoadClip(HistoryMenu->HistoryList[0])) break;
        HistoryMenu->HistoryList.Delete(0);
     }

     MasterTimeLine->OnSelectChange = OnSelectClick;
     MasterTimeLine->OnMenuEventChange = MenuEventChange;

     LoadMainIcons();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
   ostringstream os;
   vector <double> Result = MasterTimeLine->DebugIndividualTimes();

   TimeMemo->Clear();

   TimeMemo->Lines->Add("  Build,      Blit,   Paint,  Builds,  Paints");
   for (unsigned i =0; i < Result.size();)
    {
       os.str("");
       os << std::fixed << std::setw(6) << std::setprecision(3) << Result[i++];
       os << "   "  << Result[i++];
       os << "   " << Result[i++];
       os << "   " << Result[i++];
       os << ", " << Result[i++];
       TimeMemo->Lines->Add(os.str().c_str());
    }

}
//---------------------------------------------------------------------------




void __fastcall TMainForm::PositionEditChange(TObject *Sender)
{
  MasterTimeLine->CurrentThumbFrame(PositionEdit->Value);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::CSpinEdit1Change(TObject *Sender)
{
     MasterTimeLine->AudioSlip(.25*CSpinEdit1->Value);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::MasterTimeLineResize(TObject *Sender)
{
  ClientHeight = MasterTimeLine->Top + MasterTimeLine->Height;
  MasterTimeLine->FrameResize(Sender);
}
//---------------------------------------------------------------------------

//-------------------ReadSAP-------------------------John Mertus-----Jan 2002---

   bool TMainForm::ReadSAP(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   return MasterTimeLine->ReadSAP(ini, MasterTimeLine->Name.c_str());
}

//-------------------------WriteSAP-----------------John Mertus-----Jan 2002---

   bool TMainForm::WriteSAP(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   return MasterTimeLine->WriteSAP(ini, MasterTimeLine->Name.c_str());
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TMainForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   SaveonExitMenuItem->Checked = ini->ReadBool(IniSection, "SaveOnExit", SaveonExitMenuItem->Checked);
   RestoreOnStartupMenuItem->Checked = ini->ReadBool(IniSection, "RestoreOnStartup", RestoreOnStartupMenuItem->Checked);

   MasterTimeLine->ReadSettings(ini, MasterTimeLine->Name.c_str());

   ThumbStyleCBox->ItemIndex = ThumbStyleAssociations.Index(MasterTimeLine->ThumbStyle());
   ActiveDragComboBox->ItemIndex = ActiveDragAssociations.Index(MasterTimeLine->TrackBar->ActiveDrag);
   TrackStyleCBox->ItemIndex = TrackStyleAssociations.Index(MasterTimeLine->TrackStyle());
   ShowDetentCBox->Checked = MasterTimeLine->ShowThumbDetent();
   ShowThumbRangeCBox->Checked = MasterTimeLine->TrackBar->EnableThumbRange;
   ShowCBox->Checked = MasterTimeLine->TrackBar->ShowCursors;
   CursorsSpinEdit->Value = MasterTimeLine->TrackBar->NumberOfCursors;

   ThumbColorBox->Selected = (TColor)MasterTimeLine->ThumbColor();
   TrackColorBox->Selected = (TColor)MasterTimeLine->TrackColor();
   CursorColorBox->Selected = (TColor)MasterTimeLine->TrackBar->DefaultCursorColor;
   return true;
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

   bool TMainForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   // Normal read/writes
  ini->WriteBool(IniSection, "SaveOnExit", SaveonExitMenuItem->Checked);
  ini->WriteBool(IniSection, "RestoreOnStartup", RestoreOnStartupMenuItem->Checked);

   return MasterTimeLine->WriteSettings(ini, MasterTimeLine->Name.c_str());
}
//---------------ReadDesignSettings---------------John Mertus----Sep 2002-----

      bool TMainForm::ReadDesignSettings(void)

//  This restorese the settings to their original values.
//
//****************************************************************************
{
   SaveonExitMenuItem->Checked = true;
   return MasterTimeLine->ReadDesignSettings();
}
//-----------------ReadDesignSAP------------------John Mertus----Sep 2002-----

      bool TMainForm::ReadDesignSAP(void)

//  This restorese the size and position settings to their original values.
//
//****************************************************************************
{
   return MasterTimeLine->ReadDesignSAP();
}

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{

  if (SaveonExitMenuItem->Checked) SaveAllProperties();
  CIniFile *ini = new CIniFile(GetIniFileName());
  ini->WriteBool(GetIniSection(), "SaveOnExit", SaveonExitMenuItem->Checked);
  ini->WriteBool(GetIniSection(), "RestoreOnStartup", RestoreOnStartupMenuItem->Checked);
  DeleteIniFile(ini);

  delete TrackBitmap;
  delete SavedBitmap;

}
//---------------------------------------------------------------------------



void __fastcall TMainForm::DefaultPositions1Click(TObject *Sender)
{
   RestoreAllSAP(CM_DESIGNED_DATA);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UserPositions1Click(TObject *Sender)
{
   RestoreAllSAP(CM_STARTUP_DATA);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UserPositions2Click(TObject *Sender)
{
   RestoreAllSAP(CM_USER_DATA);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UserPositions3Click(TObject *Sender)
{
   SaveAllSAP(CM_USER_DATA);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StartupPositions1Click(TObject *Sender)
{
   SaveAllSAP(CM_STARTUP_DATA);
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::BrowserButtonClick(TObject *Sender)
{
  // 3rd argument is false for fsNormal Bin Manager
  ShowBinManagerGUI(false, BM_MODE_NORMAL);
  if (ShowModalDialog(BinManagerForm) == mrOk)
    {
      StringList sl = BinManagerForm->SelectedClipNames();
      if (!sl.empty())
        {
           LoadClip(sl[0]);
        }
    }

}
//---------------------------------------------------------------------------

bool TMainForm::LoadClip(const string &ClipName)
{
   int Framing;
   if (FrameRadioGroup->ItemIndex == 1)
     Framing = FRAMING_SELECT_VIDEO;
   else
     Framing = FRAMING_SELECT_FILM;

//   delete MasterTimeLine->TimeLine()->getClip();

   if (!MasterTimeLine->SetClipParameters(ClipName, VIDEO_SELECT_NORMAL, 0,Framing))
     {
       ShowMessage(theError.getFmtError().c_str());
       return false;
     }

     HistoryMenu->Add(ClipName);
     ClipEdit->Text = ClipName.c_str();
     return true;
}



void __fastcall TMainForm::HistoryMenuHistoryClick(AnsiString Str)
{
   LoadClip(Str.c_str());
}
//---------------------------------------------------------------------------


   void _fastcall TMainForm::OnAccelerate(TObject *Sender, double Speed)
{
  ostringstream os;
  os << setw(3) << Speed;
//  AccelLabel->Caption = os.str().c_str();
}

   void _fastcall TMainForm::AudioSlipChange(TObject *Sender, double Slip)
{
  ostringstream os;
  os << setw(3) << Slip;
//  AccelLabel->Caption = os.str().c_str();
}

   void _fastcall TMainForm::OnScrub(TObject *Sender, int Slip)
{
  ostringstream os;
  os << setw(3) << Slip;
//  AccelLabel->Caption = os.str().c_str();
}



void __fastcall TMainForm::ClipEditKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if (Key == VK_RETURN)
      {
         LoadClip(ClipEdit->Text.c_str());
      }
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::FrameRadioGroupClick(TObject *Sender)
{
  auto cpClip = MasterTimeLine->TimeLine()->getClip();
  if (cpClip == NULL) return;
  LoadClip(cpClip->getClipFileNameWithExt());
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AtTopCBoxClick(TObject *Sender)
{
   MasterTimeLine->TrackBarAtTop(AtTopCBox->Checked);
}
//---------------------------------------------------------------------------     h

void __fastcall TMainForm::ThumbStyleCBoxChange(TObject *Sender)
{
   int n;
   if (ThumbStyleAssociations.ValueByDisplayName(ThumbStyleCBox->Text.c_str(), n))
      MasterTimeLine->ThumbStyle((TThumbStyle)n);
   else
      ShowMessage("Unknown Thumb style");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TrackStyleCBoxChange(TObject *Sender)
{
   int n;
   if (TrackStyleAssociations.ValueByDisplayName(TrackStyleCBox->Text.c_str(), n))
      MasterTimeLine->TrackStyle((TTrackStyle)n);
   else
      ShowMessage("Unknown Thumb style");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TrackColorBoxChange(TObject *Sender)
{
   MasterTimeLine->TrackColor(TrackColorBox->Selected);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ThumbColorBoxChange(TObject *Sender)
{
   MasterTimeLine->ThumbColor(ThumbColorBox->Selected);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ShowDetentCBoxClick(TObject *Sender)
{
    MasterTimeLine->ShowThumbDetent(ShowDetentCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitmapButtonClick(TObject *Sender)
{
    if (MasterTimeLine->TrackBar->Background() != NULL)
      {
         MasterTimeLine->TrackBar->Background(NULL);
         BitmapButton->Caption = "Add Bitmap";
         return;
      }

    BuildABitmap();
    AnimateTimer->Enabled = false;
    BitmapButton->Caption = "Remove";
    MasterTimeLine->TrackBar->Background(TrackBitmap);
}

void __fastcall TMainForm::BuildABitmap(void)
{
    int Width = MasterTimeLine->TrackBar->TrackRect().Width();
    int Height = MasterTimeLine->TrackBar->TrackRect().Height();
    TCanvas *Canvas = TrackBitmap->Canvas;  // To save typing
    TrackBitmap->Width = Width;
    TrackBitmap->Height = Height;

    // Create the bitmap anyway one wishes
    // Create the correct size and background color
    Canvas->Brush->Color = MasterTimeLine->TrackColor();
    Canvas->FillRect(TRect(0,0, Width, Height));
    Canvas->Brush->Color = MasterTimeLine->TrackColor();
    Canvas->Pen->Color = clNavy;

    Canvas->MoveTo(0,0);
    Canvas->LineTo(Width-1,0);
    Canvas->MoveTo(0,Height-1);
    Canvas->LineTo(Width-1,Height-1);
    
    // Put some tick marks on it
    for (int w=0; w < Width; w=w+15)
      {
         int l = Height/5;
         if ((w%75) == 0) l = Height/3;
         Canvas->MoveTo(w,0);
         Canvas->LineTo(w,l);
         Canvas->MoveTo(w,Height-1);
         Canvas->LineTo(w,Height-l-1);
      }                          

    // DO NOT DELETE TrackBitmap
    // The trackbar uses it, not a copy
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ShowThumbRangeCBoxClick(TObject *Sender)
{
   MasterTimeLine->TrackBar->EnableThumbRange = ShowThumbRangeCBox->Checked;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::AnimateButtonClick(TObject *Sender)
{
     AnimateTimer->Enabled = !AnimateTimer->Enabled;
     if (AnimateTimer->Enabled)
       {
         BuildABitmap();
         // Set the bitmap
         SavedBitmap->Assign(TrackBitmap);
         MasterTimeLine->TrackBar->Background(TrackBitmap);
         AniTimer.Start();
       }
}
//---------------------------------------------------------------------------

#define ANI_PIXELS_SECOND 100

void __fastcall TMainForm::AnimateTimerTimer(TObject *Sender)
{

     Graphics::TBitmap *TmpBitmap = new Graphics::TBitmap;

     TrackBitmap->Assign(SavedBitmap);   // Get virgin copy
     ImageList->GetBitmap(0,TmpBitmap);

     TmpBitmap->TransparentMode = tmAuto;
     TmpBitmap->Transparent = true;
     int AniX = (int)(AniTimer.Read()*ANI_PIXELS_SECOND/1000) % TrackBitmap->Width;
     TrackBitmap->Canvas->Draw(AniX, 3, TmpBitmap);

     // An invalidate redraws the entire bitmap
     // So is more expensive than moving the thumb or cursor
     MasterTimeLine->TrackBar->Invalidate();
     delete TmpBitmap;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::CursorsSpinEditChange(TObject *Sender)
{
     MasterTimeLine->TrackBar->NumberOfCursors = CursorsSpinEdit->Value;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ShowCBoxClick(TObject *Sender)
{
     MasterTimeLine->TrackBar->ShowCursors = ShowCBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ActiveDragComboBoxChange(TObject *Sender)
{
   int n;
   if (ActiveDragAssociations.ValueByDisplayName(ActiveDragComboBox->Text.c_str(), n))
      MasterTimeLine->TrackBar->ActiveDrag = (TActiveDrag)n;
   else
      ShowMessage("Unknown Drag style");
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::CursorSpinEditChange(TObject *Sender)
{
   MasterTimeLine->CursorPosition((double)CursorSpinEdit->Value);
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::MasterTimeLineTrackBarRangeChange(
      TObject *Sender)
{
  MasterTimeLine->TrackBarRangeChange(Sender);
  RangeChangeSEdit->Value = MasterTimeLine->TrackBar->Range();

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MasterTimeLineTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  MasterTimeLine->TrackBarKeyDown(Sender, Key, Shift);
}

//---------------------------------------------------------------------------


void __fastcall TMainForm::MasterTimeLineTrackBarChange(TObject *Sender)
{
  MasterTimeLine->TrackBarChange(Sender);
  PositionEdit->Value = MasterTimeLine->CurrentThumbFrame();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ScrollingCBoxClick(TObject *Sender)
{
     MasterTimeLine->Scrolling(ScrollingCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::OnSelectClick(TObject *Sender)
{
   TFBaseTimeLine *btl = (TFBaseTimeLine *)Sender;
   if (btl == NULL)
     {
       SelectedLabel->Caption = "Selected: None";
       return;
     }
   SelectedLabel->Caption = "Selected: "  + btl->Name;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MasterTimeLineTrackBarCursorChange(
      TObject *Sender, int Value)
{
  MasterTimeLine->TrackBarCursorChange(Sender, Value);
  CursorSpinEdit->Value = MasterTimeLine->CursorPosition();
}
//---------------------------------------------------------------------------

   void _fastcall TMainForm::OnPrevNextChange(TObject *Sender, double Frame)
{
   TFBaseTimeLine *btl = (TFBaseTimeLine *)Sender;
   // Should NEVER be NULL
   if (btl == NULL)
     {
        throw;
     }

   PrevNextLabel->Caption = "Timeline: " + btl->Name + ",  value " + Frame;
}



void __fastcall TMainForm::FormShow(TObject *Sender)
{

     MasterTimeLine->ResizeTimeLines();
        
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button2Click(TObject *Sender)
{
//    for (int i=0; i < 10000; i++)
//      {
//        MasterTimeLine->CursorPosition(500 + i);
//      }
        MasterTimeLine->CursorPosition(MasterTimeLine->CursorPosition()+1);

    int Width = TestTrackBar->TrackRect().Width();
    int Height = TestTrackBar->TrackRect().Height();
    TrackBitmap->Width = Width;
    TrackBitmap->Height = Height;
    TCanvas *Canvas = TrackBitmap->Canvas;  // To save typing

    TRect Rect(0,0, Width, Height);
    int N = 1000;
    MTI_INT16 *Data = new MTI_INT16[N];
    for (int i=0; i < N; i++)
       Data[i] = (N-i)/(double)N * (sin(3.14159/10*i) * 30000.);
    BuildWaveForm(Rect, Canvas,  Data, N, 1.0);
    delete [] Data;
    TestTrackBar->Background(TrackBitmap);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RangeChangeSEditChange(TObject *Sender)
{
   MasterTimeLine->TrackBar->Range(RangeChangeSEdit->Value);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MenuEventChange(TObject *Sender, EMenuEventAction Action, MTI_UINT64 &EventFlag, int &Frame, bool &Continue)
{
   TFEventTimeLine *etl = (TFEventTimeLine *)Sender;
   ostringstream os;
   os << "Event change on " << etl->Name.c_str() << endl;\
   switch (Action)
    {
        case EVENT_ACTION_INVALID:
           os << "Action is not defined";
           break;

        case EVENT_ACTION_ADD:
           os << "Add action";
           break;

        case EVENT_ACTION_DELETE:
           os << "Delete action";
           break;

        case EVENT_ACTION_NEXT:
           os << "Next action";
           break;

        case EVENT_ACTION_PREVIOUS:
           os << "Previous action";
           break;
    }

   os << " on frame " << Frame << endl;
   os << "Default event 0x" << hex << setw(16) << setfill('0') << EventFlag;
   _MTIInformationDialog(Handle, os.str());
}


