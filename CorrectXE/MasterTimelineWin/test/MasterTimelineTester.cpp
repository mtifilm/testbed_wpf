//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("MasterTimelineTesterUnit.cpp", MainForm);
USEFORM("..\BaseTimeLineFrame.cpp", FBaseTimeLine); /* TFrame: File Type */
USEFORM("..\EventTimeLineFrame.cpp", FEventTimeLine); /* TFrame: File Type */
USEFORM("..\AudioTimeLineFrame.cpp", FAudioTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimeLineFrame.cpp", FMasterTimeLine); /* TFrame: File Type */
USEFORM("C:\MTIBorland\Repository\MTIUNIT.cpp", MTIForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
