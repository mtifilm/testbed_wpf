//---------------------------------------------------------------------------

#ifndef MasterTimelineTesterUnitH
#define MasterTimelineTesterUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include "EventTimeLineFrame.h"
#include "AudioTimeLineFrame.h"
#include "MasterTimeLineFrame.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include <Menus.hpp>
#include "MTIUNIT.h"
#include "HistoryMenu.h"
#include <Buttons.hpp>
#include "MTIDBMeter.h"
#include "MTITrackBar.h"
#include "VTimeCodeEdit.h"
#include <ImgList.hpp>
#include "HRTimer.h"
#include "TimelineTrackBar.h"
#include <System.ImageList.hpp>


//---------------------------------------------------------------------------
class TMainForm : public TMTIForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TCSpinEdit *PositionEdit;
        TCSpinEdit *CSpinEdit1;
        TLabel *Label1;
        TMemo *TimeMemo;
        TMenuItem *File1;
        TMenuItem *DefaultPositions1;
        TMenuItem *N1;
        TMenuItem *RestoreOnStartupMenuItem;
        TSpeedButton *BrowserButton;
        TEdit *ClipEdit;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
    TRadioGroup *FrameRadioGroup;
    TCheckBox *AtTopCBox;
        TComboBox *ThumbStyleCBox;
        TComboBox *TrackStyleCBox;
        TLabel *Label5;
        TLabel *Label6;
        TColorBox *TrackColorBox;
        TColorBox *ThumbColorBox;
        TCheckBox *ShowDetentCBox;
        TButton *BitmapButton;
        TCheckBox *ShowThumbRangeCBox;
        TButton *AnimateButton;
        TTimer *AnimateTimer;
        TImageList *ImageList;
        TCSpinEdit *CursorsSpinEdit;
        TLabel *Label7;
        TCheckBox *ShowCBox;
        TColorBox *CursorColorBox;
        TComboBox *ActiveDragComboBox;
        TLabel *Label8;
        TCSpinEdit *CursorSpinEdit;
        TLabel *Label9;
        TCheckBox *ScrollingCBox;
        TLabel *SelectedLabel;
        TLabel *PrevNextLabel;
        TTimelineTrackBar *TestTrackBar;
        TButton *Button2;
        TCSpinEdit *RangeChangeSEdit;
        TLabel *Label10;
        TFMasterTimeLine *MasterTimeLine;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall PositionEditChange(TObject *Sender);
        void __fastcall CSpinEdit1Change(TObject *Sender);
        void __fastcall MasterTimeLineResize(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall DefaultPositions1Click(TObject *Sender);
        void __fastcall UserPositions1Click(TObject *Sender);
        void __fastcall UserPositions2Click(TObject *Sender);
        void __fastcall UserPositions3Click(TObject *Sender);
        void __fastcall StartupPositions1Click(TObject *Sender);
        void __fastcall BrowserButtonClick(TObject *Sender);
        void __fastcall HistoryMenuHistoryClick(AnsiString Str);
    void __fastcall ClipEditKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FrameRadioGroupClick(TObject *Sender);
    void __fastcall AtTopCBoxClick(TObject *Sender);
        void __fastcall ThumbStyleCBoxChange(TObject *Sender);
        void __fastcall TrackStyleCBoxChange(TObject *Sender);
        void __fastcall TrackColorBoxChange(TObject *Sender);
        void __fastcall ThumbColorBoxChange(TObject *Sender);
        void __fastcall ShowDetentCBoxClick(TObject *Sender);
        void __fastcall BitmapButtonClick(TObject *Sender);
        void __fastcall ShowThumbRangeCBoxClick(TObject *Sender);
        void __fastcall AnimateButtonClick(TObject *Sender);
        void __fastcall AnimateTimerTimer(TObject *Sender);
        void __fastcall CursorsSpinEditChange(TObject *Sender);
        void __fastcall ShowCBoxClick(TObject *Sender);
        void __fastcall ActiveDragComboBoxChange(TObject *Sender);
        void __fastcall CursorSpinEditChange(TObject *Sender);
        void __fastcall MasterTimeLineTrackBarRangeChange(TObject *Sender);
        void __fastcall MasterTimeLineTrackBarKeyDown(TObject *Sender,
          WORD &Key, TShiftState Shift);
        void __fastcall MasterTimeLineTrackBarChange(TObject *Sender);
        void __fastcall ScrollingCBoxClick(TObject *Sender);
        void __fastcall OnSelectClick(TObject *Sender);
        void __fastcall MasterTimeLineTrackBarCursorChange(TObject *Sender,
          int Value);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall RangeChangeSEditChange(TObject *Sender);
private:	// User declarations
        CHRTimer AniTimer;
        void __fastcall BuildABitmap(void);
        void __fastcall MenuEventChange(TObject *Sender, EMenuEventAction Action, MTI_UINT64 &EventFlag, int &Frame, bool &Continue);

public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
        bool LoadClip(const string &ClipName);
        Graphics::TBitmap *TrackBitmap;
        Graphics::TBitmap *SavedBitmap;


        virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.
        virtual bool ReadDesignSettings(void);
        virtual bool ReadDesignSAP(void);
        void _fastcall OnAccelerate(TObject *Sender, double Speed);
        void _fastcall AudioSlipChange(TObject *Sender, double Slip);
        void _fastcall OnPrevNextChange(TObject *Sender, double Frame);
//        virtual void StartForm(void);
        void _fastcall OnScrub(TObject *Sender, int Value);

};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
