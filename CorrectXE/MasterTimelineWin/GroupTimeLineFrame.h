//---------------------------------------------------------------------------


#ifndef GroupTimeLineFrameH
#define GroupTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFGroupTimeLine : public TFrame
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
        __fastcall TFGroupTimeLine(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFGroupTimeLine *FGroupTimeLine;
//---------------------------------------------------------------------------
#endif
