inherited BookmarkTimeLine: TBookmarkTimeLine
  inherited BaseTimelinePopupMenu: TPopupMenu
    Left = 86
    Top = 65526
  end
  inherited ImportTimecodesOpenDialog: TOpenDialog
    Left = 170
    Top = 65526
  end
  inherited ExportTimecodesSaveDialog: TSaveDialog
    Left = 316
    Top = 65528
  end
  object ExportBookmarkEventsSaveDialog: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All Files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoReadOnlyReturn, ofEnableSizing]
    Title = 'Export Bookmark Events'
    Left = 394
    Top = 65530
  end
  object ImportBookmarkEventsOpenDialog: TOpenDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All Files|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Import Bookmark Events'
    Left = 363
    Top = 65530
  end
end
