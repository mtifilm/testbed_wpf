//---------------------------------------------------------------------------

#ifndef BookmarkTimeLineFrameH
#define BookmarkTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "EventTimeLineFrame.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>

#include "machine.h"
#include <Dialogs.hpp>
#include "BaseTimeLineFrame.h"

//---------------------------------------------------------------------------
class TBookmarkTimeLine : public TFEventTimeLine
{
__published:	// IDE-managed Components
   TSaveDialog *ExportBookmarkEventsSaveDialog;
   TOpenDialog *ImportBookmarkEventsOpenDialog;

private:	// User declarations

  virtual void ExportBookmarkEvents();
  virtual void ImportBookmarkEvents();
  virtual bool HaveTimeLine();
  virtual int GetTotalFrameCount();
  virtual string GetVideoTC(int frameIndex);
  virtual int GetVideoFrameIndex(CTimecode timecode);
  virtual int GetPreviousEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask);
  virtual int GetNextEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask);
  virtual MTI_TIMELINE_EVENT GetEventFlag(int frameIndex);
  virtual void DeleteEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag);
  virtual void AddEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag);
  virtual CMultiFrameEvent GetNextMultiFrameEvent(int frameIndex, MTI_TIMELINE_EVENT mfEventMask);
  virtual void DrawEventSymbol(Graphics::TBitmap *bitmap, MTI_UINT64 llEvent, int iXPos, int iFrame, int iFrameWidth);


public:		// User declarations

   __fastcall TBookmarkTimeLine(TComponent* Owner);

   virtual void GoToPreviousEvent(void);
   virtual void GoToNextEvent(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TBookmarkTimeLine *BookmarkTimeLine;
//---------------------------------------------------------------------------
#endif

