//---------------------------------------------------------------------------

#ifndef AudioTimeLineFrameH
#define AudioTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFAudioTimeLine : public TFBaseTimeLine
{
__published:	// IDE-managed Components
private:	// User declarations
        virtual void ComputeBitmap(void);

public:		// User declarations
        __fastcall TFAudioTimeLine(TComponent* Owner);
        virtual bool ReadDesignSAP(void);
        
};

extern  void BuildWaveForm(TRect Rect, TCanvas *Canvas, MTI_INT16 *Data, int nData, float Scale);
extern  void BuildWaveForm(Graphics::TBitmap *Bitmap, CTimeLine *tl, int StartFrame, int EndFrame, int Channel, float Scale, int FineBoundary);

//---------------------------------------------------------------------------
extern PACKAGE TFAudioTimeLine *FAudioTimeLine;
//---------------------------------------------------------------------------
#endif
