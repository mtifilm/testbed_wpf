//---------------------------------------------------------------------------

#ifndef AnimateTimeLineFrameH
#define AnimateTimeLineFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "BaseTimeLineFrame.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>

#include "machine.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFAnimateTimeLine : public TFBaseTimeLine
{
__published:	// IDE-managed Components
        void __fastcall DataDisplayDblClick(TObject *Sender);
   void __fastcall NextButtonClick(TObject *Sender);
   void __fastcall PreviousButtonClick(TObject *Sender);
   void __fastcall DropEventMenuItemClick(TObject *Sender);
   void __fastcall ClearAllMarksMenuItemClick(TObject *Sender);


private:	// User declarations
       virtual void ComputeBitmap(void);
       virtual void UpdateButtons(void);

public:		// User declarations

        __fastcall TFAnimateTimeLine(TComponent* Owner);
   void DrawKeyframeSymbol(Graphics::TBitmap *bitmap, int iXPos,
                           bool inclusion);

   virtual void GoToPreviousEvent(void);
   virtual void GoToNextEvent(void);


};

//---------------------------------------------------------------------------
extern PACKAGE TFAnimateTimeLine *FAnimateTimeLine;
//---------------------------------------------------------------------------
#endif

