// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <math.h>   // for ceil()

#include "RefTimeLineFrame.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma resource "*.dfm"
TFRefTimeLine *FRefTimeLine;
// ---------------------------------------------------------------------------

#define BIG_TICK 1.0
#define MEDIUM_TICK 0.3
#define SHORT_TICK 0.175

// ---------------------------------------------------------------------------
__fastcall TFRefTimeLine::TFRefTimeLine(TComponent* Owner)
		: TFBaseTimeLine(Owner)
{
	Height = 21;
	DataDisplay->Color = clCream;
}
// ---------------------------------------------------------------------------

// ---------------ComputeBitmap-----------------------------------------------
//
// This draws the data into the frames
//
// ****************************************************************************
void TFRefTimeLine::ComputeBitmap(void)
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> ENTER TFRefTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");

	// Do nothing if we have not been set up
	if (TimeLine() == NULL)
	{
		return;
	}

	if (Frames() < 2)
	{
		return;
	}

	// Save some typing
	TCanvas *canvas = _OffScreenBitmap->Canvas;
	int HeightBm = _OffScreenBitmap->Height;
	int WidthBm = _OffScreenBitmap->Width;
	double BeginFrame = StartFrame();
	double EndFrame = StartFrame() + Frames();
	double MinFrame = Min();
	double MaxFrame = Max();
	double MidFrame = AdjustedMidFrame();
	double NumFrames = Frames();
	double NoteResWidthInPixels = canvas->TextWidth("99:99:99:99.XX");

	// Keep in range

	if (BeginFrame < Min())
	{
		BeginFrame = Min();
	}

	if (EndFrame > Max() + 1)
	{
		EndFrame = Max() + 1;
	}

   // Don't unnecessarily recompute - this is slow!
	if (
		_oldHeightBm == HeightBm &&
		_oldWidthBm == WidthBm &&
		_oldBeginFrame == BeginFrame &&
		_oldEndFrame == EndFrame &&
		_oldMinFrame == MinFrame &&
		_oldMaxFrame == MaxFrame &&
		_oldMidFrame == MidFrame &&
		_oldNumFrames == NumFrames &&
		_oldNoteResWidthInPixels == NoteResWidthInPixels
		)
	{
		////TRACE_3(errout << "<x>x<x>x<x>x<x> ABORT TFRefTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");
		return;
	}

	_oldHeightBm = HeightBm;
	_oldWidthBm = WidthBm;
	_oldBeginFrame = BeginFrame;
	_oldEndFrame = EndFrame;
	_oldMinFrame = MinFrame;
	_oldMaxFrame = MaxFrame;
	_oldMidFrame = MidFrame;
	_oldNumFrames = NumFrames;
	_oldNoteResWidthInPixels = NoteResWidthInPixels;

	TFBaseTimeLine::ComputeBitmap();

	canvas->Pen->Color = clDkGray;
	canvas->Font->Color = clDkGray;

	int prevX = -1;
	AnsiString prevNote;
	float prevTickFrac = -1;

	for (double frame = BeginFrame; frame < EndFrame; frame += 1.0)
	{
		if (frame < 0)
		{
			continue;
		}

		float tickFrac = 0.;
		int x = WidthBm * (frame - BeginFrame) / float(Frames()) - 1;
		AnsiString note;
		double pixelsPerFrame = WidthBm / NumFrames;
		int noteResWidthInFrames = (int) ceil(NoteResWidthInPixels / pixelsPerFrame);
		int xOffset = 0;
		int pow2 = 1;
		int exp = 0;

		if ((MidFrame - MinFrame) < (NumFrames / 2))
		{
			xOffset = (int) floor(WidthBm * ((NumFrames / 2.0) - MidFrame + MinFrame) / NumFrames) - 1;
		}

		x += xOffset;

		for (exp = 0; pow2 < noteResWidthInFrames; ++exp)
		{
			pow2 <<= 1;
		}

		if (((int)frame % pow2) == 0)
		{
			tickFrac = BIG_TICK;
			note = TimeLine()->getVideoTC((int) frame).c_str();

			// Trim blanks off front of non-timecode timecodes
			// Careful!! Frickin' AnsiString indexes are 1-based!!!
			// For some reason, linker can't find AnsiString::SubString in XE5upd2
			while (note.Length() > 1 && AnsiString((AnsiStringToStdString(note).substr(1, 1)).c_str()) == " ")
			{
				note = AnsiString((AnsiStringToStdString(note).substr(2, note.Length() - 1)).c_str());
			}
		}
		else
		{
			while ((pow2 >>= 1) > 1)
			{
				if ((((int)frame % pow2) == 0) && ((pixelsPerFrame * pow2) > 3.0))
				{
					tickFrac = (pow2 > 2) ? MEDIUM_TICK : SHORT_TICK;
					break;
				}

				exp -= 1;
			}

			if ((pow2 == 1) && (pixelsPerFrame > 3.0))
			{
				tickFrac = SHORT_TICK;
			}
		}

		if (x != prevX || tickFrac != prevTickFrac)
		{
			canvas->MoveTo(x, HeightBm - (int)(tickFrac * HeightBm));
			canvas->LineTo(x, HeightBm);
		}

		if (note != "" && note != prevNote)
		{
			int textX = x + 3;
			int textY = 0;

			canvas->TextOut(textX, textY, note);

		}

		prevX = x;
		prevTickFrac = tickFrac;
		prevNote = note;
	}

	////TRACE_3(errout << "<x>x<x>x<x>x<x> EXIT TFRefTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");
}

void __fastcall TFRefTimeLine::DataDisplayDblClick(TObject *Sender)
{

	if (DblClickCallback)
	{
		DblClickCallback(Sender);

		// ignore mouse down
		bDblClickIgnoreMouse = true;
	}
}
