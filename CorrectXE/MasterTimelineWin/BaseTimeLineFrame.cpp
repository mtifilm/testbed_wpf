//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BaseTimeLineFrame.h"

#include "autoDiffGui.h"       // for the "cut detector" dialog
#include "ClipIdentifier.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "pTimecode.h"
#include "ShowModalDialog.h"
#include "TimelineTrackBar.h"

#include <math.h>
#include <algorithm>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFBaseTimeLine *FBaseTimeLine;
//---------------------------------------------------------------------------
__fastcall TFBaseTimeLine::TFBaseTimeLine(TComponent* Owner)
        : TFrame(Owner)
{
  // In the case that the driving data is set to local
  _CommonData = &_LocalCommonData;

  _OffScreenBitmap = new Graphics::TBitmap;
  _RecomputeFlag = true;
  _Tracking = false;
  _ShowTicks = false;
  _ShowFrameCache = false;
  _OnMenuEventChange = NULL;
  Builds = 0;
  Paints = 0;

  bDblClickIgnoreMouse = false;

  DataDisplay->ControlStyle << csOpaque;

  // Push the default symbol colors
  // These can be overridden later
  SymbolColor.push_back(clBlack);
  SymbolColor.push_back(clPurple);
  SymbolColor.push_back(clBlue);
  SymbolColor.push_back(clNavy);
  SymbolColor.push_back(clBlue);
  SymbolColor.push_back(clRed);
  SymbolColor.push_back(clGreen);
  SymbolColor.push_back(clGray);
  SymbolColor.push_back(clTeal);

  // This is until events are defined
  EventMask = 0;
  MFEventMask = 0;
  Channel = 0;



  // To avoid new components, replace panel with this panel

  HidePanel = new TFlickerPanel(this);
  HidePanel->Left = DataDisplay->Left;
  DataDisplay->Left = 0;
  HidePanel->Top = DataDisplay->Top;
  DataDisplay->Top = 0;
  HidePanel->Width = DataDisplay->Width;
  HidePanel->Height = DataDisplay->Height;
  HidePanel->Align = DataDisplay->Align;
  HidePanel->Anchors = DataDisplay->Anchors;
  HidePanel->Parent = this;
  DataDisplay->Parent = HidePanel;

  DblClickCallback = 0;
}

__fastcall TFBaseTimeLine::~TFBaseTimeLine(void)
{
   delete _OffScreenBitmap;
   _OffScreenBitmap = NULL;
}

void TFBaseTimeLine::setDblClickCallback (void (* fcn)(TObject *Sender))
{
  DblClickCallback = fcn;
}


//---------------------------------------------------------------------------

/*-----------------RedrawDisplay------------------John Mertus----Apr 2003---*/

	 void TFBaseTimeLine::RedrawDisplay(void)

// This forces updates to all the children displays and its timecodes
//
/****************************************************************************/
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> ENTER TFBaseTimeLine::RedrawDisplay <x>x<x>x<x>x<x>");
	DataDisplay->Invalidate();
   _RecomputeFlag = true;
	////TRACE_3(errout << "<x>x<x>x<x>x<x> EXIT TFBaseTimeLine::RedrawDisplay <x>x<x>x<x>x<x>");
}

//-----------------DrawCursors---------------------John Mertus----Feb 2004---

    void TFBaseTimeLine::DrawCursors(TCanvas *Canvas)

// This is the default drawing for the cursors
// It writes the cursor to the off screen bitmap
//
/****************************************************************************/
{
  if (Min() == Max()) return;  // QQQ No cursor if clip is one frame !!!

  int WidthBm = DataDisplay->Width;
  TRect Rect(0,0, DataDisplay->Width, DataDisplay->Height);

  for (unsigned i=0; i < _CommonData->Cursors()->size(); i++)
    {
       CTimelineCursor *tlc = &_CommonData->Cursors()->at(i);
       double fMin = Min();
       double fMax = Max();
       double position = tlc->Position;

       // Draw the cursor if in range
       if ((position >= fMin) && (position <= fMax))
         {
           int x = WidthBm*(tlc->Position - StartFrame())/float(Frames()) - 1;
           int TotalFrameCount = TotalFrames();
           int StartPixel = (WidthBm-1)*(float(StartFrame() - Min())/(Frames()-1));
           int LastPixel = (WidthBm-1)*(float(TotalFrameCount - .5)/(Frames()-1));
           tlc->FrameWidth = (LastPixel - StartPixel) / TotalFrameCount;
           tlc->Draw(x,0, Rect, Canvas);
         }
    }
}

//-----------------ComputeBitmap------------------John Mertus----Apr 2003---

	 void TFBaseTimeLine::ComputeBitmap(void)

// This builds the initial part of the bitmap
//
/****************************************************************************/
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> ENTER TFBaseTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");

  if (!HaveTimeLine()) return;
  if (Frames() < 2) return;

  int xOffset;

  TCanvas *DataCanvas = _OffScreenBitmap->Canvas;
  int HeightBm = _OffScreenBitmap->Height;
  int WidthBm = _OffScreenBitmap->Width;


  // Is timeline center frame < MidFrame of display
  if ((AdjustedMidFrame() - Min()) < Frames()/2)
    {
     xOffset = (WidthBm)*(Frames()/2. - AdjustedMidFrame() + Min())/((float)Frames());

     DataCanvas->Pen->Mode = pmNop;
     DataCanvas->Brush->Color = clGray;
     DataCanvas->FillRect(TRect(0, HeightBm, xOffset, 0));
    }
  else
    {
      xOffset = 0;
	 }

	////TRACE_3(errout << "<x>x<x>x<x>x<x> TFBaseTimeLine::ComputeBitmap CHKPT 1 <x>x<x>x<x>x<x>");

   int TotalFrameCount = TotalFrames();

   int StartPixel = (WidthBm-1)*(float(StartFrame() - Min())/(Frames()-1));
   int LastPixel = (WidthBm-1)*(float(TotalFrameCount - .5)/(Frames()-1));


   // Are we at the end?   If so, draw a box
   int xBackOffset = std::min(LastPixel-StartPixel, WidthBm);
   DataCanvas->Pen->Mode = pmNop;
   DataCanvas->Brush->Color = clGray;
   DataCanvas->FillRect(TRect(xBackOffset+1, HeightBm, WidthBm, 0));

	////TRACE_3(errout << "<x>x<x>x<x>x<x> TFBaseTimeLine::ComputeBitmap CHKPT 2 <x>x<x>x<x>x<x>");

   //  Erase all that has been drawn
   if (ShowTicks())
     DataCanvas->Brush->Color = (TColor) 0x00F4F4F4;
   else
     DataCanvas->Brush->Color = DataDisplay->Color;

	DataCanvas->FillRect(TRect(xOffset, HeightBm, xBackOffset+1, 0));

	////TRACE_3(errout << "<x>x<x>x<x>x<x> TFBaseTimeLine::ComputeBitmap CHKPT 3 <x>x<x>x<x>x<x>");

   DataCanvas->Pen->Mode = pmCopy;

	Builds++;

	////TRACE_3(errout << "<x>x<x>x<x>x<x> EXIT TFBaseTimeLine::ComputeBitmap <x>x<x>x<x>x<x>");
}
//---------------------------------------------------------------------------


/*------------------OffsetFrame------------------John Mertus----Apr 2003---*/

    double TFBaseTimeLine::OffsetFrame(void)

//  Return the Offset display frame
//
/****************************************************************************/
{
   return _OffsetFrame;
}

/*------------------OffsetFrame------------------John Mertus----Apr 2003---*/

    void TFBaseTimeLine::OffsetFrame(double Value)

//  Set the Offset frame to the value and update it.
//
/****************************************************************************/
{
   if (OffsetFrame() == Value) return;
   _OffsetFrame = Value;
   RedrawDisplay();
}

/*----------------AdjustedMidFrame------------------John Mertus----Apr 2003---*/

    double TFBaseTimeLine::AdjustedMidFrame(void)

//  Return the absolute frame value
//
/****************************************************************************/
{
  return _OffsetFrame + NominalMidFrame();
}

//------------------Frames------------------John Mertus----Apr 2003---

    void TFBaseTimeLine::Frames(double Value)

//  Set the Offset frame to the value and update it.
//
//***************************************************************************
{
   if (_CommonData->Frames() == Value) return;
   _CommonData->Frames(Value);
   RedrawDisplay();
}

//-------------------Frames---------------------John Mertus----Apr 2003---

    double TFBaseTimeLine::Frames(void)

//  Return the absolute frame value
//
/****************************************************************************/
{

   return _CommonData->Frames();

}

/*----------------StartFrame------------------John Mertus----Apr 2003---*/

    double TFBaseTimeLine::StartFrame(void)

//  Return the start frame for the data
//
/****************************************************************************/
{
   return AdjustedMidFrame() - Frames()/2;
}

//-----------------NominalMidFrame------------------John Mertus----Apr 2003---

    double TFBaseTimeLine::NominalMidFrame(void)

//  Return frame that is nominally at the center of the timeline (pre-offset)
//
/****************************************************************************/
{
   return _CommonData->MidFrame();
}

//-----------------NominalMidFrame--------------------John Mertus----Apr 2003---

    void TFBaseTimeLine::NominalMidFrame(double Value)

//  Set the current frame to the value and update it.
//
/****************************************************************************/
{
   if (_CommonData->MidFrame() == Value) return;
   _CommonData->MidFrame(Value);
   RedrawDisplay();
}

//--------------------TimeLine---------------------John Mertus----Apr 2003----

        CTimeLine *TFBaseTimeLine::TimeLine(void)

//  Return the size of display in frames
//
/****************************************************************************/
{
   return _CommonData->_TimeLine;
}

//--------------------TimeLine---------------------John Mertus----Apr 2003----

        void TFBaseTimeLine::TimeLine(CTimeLine *Value)

//  Return the size of display in frames
//
/****************************************************************************/
{
   if (_CommonData->_TimeLine == Value) return;
   _CommonData->_TimeLine = Value;
   RedrawDisplay();
}



//------------CommonComputeBitmap-------------------John Mertus----Apr 2003----

	 void TFBaseTimeLine::CommonComputeBitmap(void)

//  When this is called, the bitmap is recomputed by the virtual recompute call
//
/****************************************************************************/
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> ENTER TFBaseTimeLine::CommonComputeBitmap <x>x<x>x<x>x<x>");

	// Recompute the bitmap if necessary
	if (!_RecomputeFlag)
	{
		////TRACE_3(errout << "<x>x<x>x<x>x<x> ABORT TFBaseTimeLine::CommonComputeBitmap <x>x<x>x<x>x<x>");
		return;
	}

   double STime = HRT.Read();
   TRect R = DataDisplay->ClientRect;

   if (R.Height() > 2 &&  R.Width() > 2) {

#ifndef NAVIGATOR
     // ??? QQQ mbraca nuked this so I can change bg color
     DataDisplay->Color = (TColor)0x00E8E8E8;
#endif

   // In case there was a resize
   _OffScreenBitmap->Height = R.Height()-2;
   _OffScreenBitmap->Width = R.Width()-2;

   // Build the bitmap
	ComputeBitmap();

   BuildBitmapTime = HRT.Read() - STime;

   }

   // See if we want to shrink the image
	_RecomputeFlag = false;

	////TRACE_3(errout << "<x>x<x>x<x>x<x> EXIT TFBaseTimeLine::CommonComputeBitmap <x>x<x>x<x>x<x>");
}

void __fastcall TFBaseTimeLine::MaybeDataDisplayPaint(TObject *Sender)
{
	// HACK, while playing, to avoid cursor breaking up, don't repaint
	// automatically, require the program to repaint.
	if (!_CommonData->AutoPaint)
	{
		return;
	}

	DataDisplayPaint(Sender);
}

//------------DataDisplayPaint-------------------John Mertus----Apr 2003----

	void __fastcall TFBaseTimeLine::DataDisplayPaint(TObject *Sender)

//  This is called whenever a paint needs to be done
//  If RecomputeFlag, then the display is recomputed.
//  Then the Offscreenbitmap is sent to the screen
//
/****************************************************************************/
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint ENTER <x>x<x>x<x>x<x>");

	TRect R = DataDisplay->ClientRect;

	TColor ColorTop = clBtnShadow;
    TColor ColorBottom = clBtnHighlight;

    // If Active, draw with a different color boarder
      if (_CommonData->ActiveTL() == this)
        {
           ColorTop = clHotLight;
           ColorBottom = ColorTop;
		}

	// Recompute the bitmap if necessary
	if (_RecomputeFlag)
	  {
		 ////TRACE_3(errout << "<x>x<x>x<x>x<x> Before _CommonData->RecomputeAllDisplays() <x>x<x>x<x>x<x>");
		 _CommonData->RecomputeAllDisplays();
		 ////TRACE_3(errout << "<x>x<x>x<x>x<x> After _CommonData->RecomputeAllDisplays() <x>x<x>x<x>x<x>");
		 _RecomputeFlag = false;
	  }

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 2 <x>x<x>x<x>x<x>");

	// Now repaint the display, first do the frame
	TCanvas *DataCanvas = DataDisplay->Canvas;
    Graphics::TBitmap *TmpBm = new Graphics::TBitmap();
	TmpBm->Assign(_OffScreenBitmap);

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 2 <x>x<x>x<x>x<x>");

    double STime = HRT.Read();
//    DataDisplay->Canvas->Draw(1,1, _OffScreenBitmap);
    BlitBitmapTime = HRT.Read() - STime;

    DataCanvas->Pen->Color = ColorTop;
    DataCanvas->MoveTo(R.left,R.bottom-1);
    DataCanvas->LineTo(R.left,R.top);
    DataCanvas->LineTo(R.right-1,R.top);

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 3 <x>x<x>x<x>x<x>");

    DataCanvas->Pen->Color = ColorBottom;
    DataCanvas->LineTo(R.right-1,R.bottom-1);
    DataCanvas->LineTo(R.left,R.bottom-1);

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 4 <x>x<x>x<x>x<x>");

    DataCanvas = TmpBm->Canvas;
    // Draw the vertical center line
    if (_CommonData->DrawCenterLine())
      {
        int x = DataDisplay->Width/2;
        DataCanvas->Pen->Mode = pmXor;
        DataCanvas->MoveTo(x, DataDisplay->Height-2);
        DataCanvas->Pen->Color = DataDisplay->Color;
        DataCanvas->LineTo(x, 0);
        DataCanvas->Pen->Mode = pmCopy;
	  }

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 5 <x>x<x>x<x>x<x>");

	UpdateButtons();

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 6 <x>x<x>x<x>x<x>");

	DrawCursors(DataCanvas);

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 7 <x>x<x>x<x>x<x>");

	// Copy the bitmap
    double STime2 = HRT.Read();
	DataDisplay->Canvas->Draw(1,1, TmpBm);
    BlitBitmapTime = HRT.Read() - STime2;

	////TRACE_3(errout << "<x>x<x>x<x>x<x> DataDisplayPaint CHKPT 8 <x>x<x>x<x>x<x>");

	 delete TmpBm;
    PaintTime = HRT.Read() - STime;
    SelectButton->Down = (_CommonData->ActiveTL() == this);

	Paints++;
}

void __fastcall TFBaseTimeLine::DrawCursorsOnCanvas()
{
	TCanvas *DataCanvas = DataDisplay->Canvas;
	DrawCursors(DataCanvas);
}

void __fastcall TFBaseTimeLine::FrameResize(TObject *Sender)
{
   RedrawDisplay();
}
//-----------------TotalFrames---------------------John Mertus----Apr 2003---

    double  TFBaseTimeLine::TotalFrames(void)

//  Return the number of display frames
//
//***************************************************************************
{
   return _CommonData->Max() - _CommonData->Min() + 1.;
}

//--------------------Min--------------------------John Mertus----Apr 2003---

    void TFBaseTimeLine::Min(double Value)

//  set the number of the first display frame; will cause a redraw
//
//***************************************************************************
{
   _CommonData->Min(Value);
}

//---------------------Min------------------------John Mertus----Apr 2003---

    double  TFBaseTimeLine::Min(void)

//  Return the number of the first display frame
//
//***************************************************************************
{
   return _CommonData->Min();
}

//--------------------Max--------------------------John Mertus----Apr 2003---

    void TFBaseTimeLine::Max(double Value)

//  Set the number of last display frame; will cause a redraw
//
//***************************************************************************
{
   _CommonData->Max(Value);
}


//---------------------Max------------------------John Mertus----Apr 2003---

    double  TFBaseTimeLine::Max(void)

//  Return the number of the last display frame
//
//***************************************************************************
{
   return _CommonData->Max();
}

//---------------DrawCenterLine-------------------John Mertus----Feb 2004---

    bool  CTimeLineCommonData::DrawCenterLine(void)

//  Returns true if we should draw the center line
//
//***************************************************************************
{
   return _DrawCenterLine;
}


//-----------------DrawCenterLine------------------John Mertus----Feb 2004---

    void CTimeLineCommonData::DrawCenterLine(bool Value)

//  Set if we should draw the center line and then and force an update
//
//***************************************************************************
{
   if (_DrawCenterLine == Value) return;
   _DrawCenterLine = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//------------------CursorPosition----------------John Mertus----Apr 2003----

        void CTimeLineCommonData::CursorPosition(double Value)

//  Sets the position of the cursor
//
/****************************************************************************/
{
   dValue = Value;
   Message.Msg = WM_PAINTICON;
   Notify();
}

//------------------CursorPosition----------------John Mertus----Apr 2003----

        double CTimeLineCommonData::CursorPosition(void)

//  Returns the position of the cursor
//
/****************************************************************************/
{
   int AC = _Cursors->ActiveCursor();
   if ((AC < 0) || (AC >= (int)_Cursors->size())) return 0;
   return  _Cursors->at(AC).Position;
}

//------------------CachedFrameList-------------------------------Sep 2010----

        void CTimeLineCommonData::CachedFrameList(const IntegerList &List)

//  Sets the list of frames that are cached
//
/****************************************************************************/
{
   _CachedFramesList = List;
   std::sort(_CachedFramesList.begin(), _CachedFramesList.end());

   Message.Msg = WM_PAINT;  // NOT WM_SETREDRAW!
   Notify();
}

//------------------CachedFrameList-------------------------------Sep 2010----

        const IntegerList &CTimeLineCommonData::CachedFrameList()

//  Gets the list of frames that are cached
//
/****************************************************************************/
{
   return _CachedFramesList;
}

//------------------NextPrevClick------------------John Mertus----Apr 2003----

        void CTimeLineCommonData::NextPrevClick(TFBaseTimeLine *tl, int Value)

//  Sets the position of the cursor
//
/****************************************************************************/
{
   Message.Msg = WM_COMMNOTIFY;
   Message.WParam = (int)tl;
   Message.LParam = Value;
   Notify();
}

//------------------MidFrame------------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::MidFrame(double Value)

//  Set the number of the frame at the center of the display (pre offset)
//
//***************************************************************************
{
   if (_MidFrame == Value) return;
   _MidFrame = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//---------------CTimeLineCommonData------------------John Mertus----Apr 2003----

     void  TFBaseTimeLine::CommonData(CTimeLineCommonData  *Common)

// This sets the common data pointer to Common
// if Common is NULL, then local common data is used
//
//*****************************************************************************
{
   if (_CommonData == Common) return;
   if (Common == NULL)
     _CommonData = &_LocalCommonData;
   else
     _CommonData = Common;
   RedrawDisplay();
}

//---------------CTimeLineCommonData------------------John Mertus----Apr 2003----

     CTimeLineCommonData  * TFBaseTimeLine::CommonData(void)

// This just returns the common data pointer
//
//*****************************************************************************
{
   return _CommonData;
}

//---------------CTimeLineCommonData------------------John Mertus----Apr 2003----

    CTimeLineCommonData::CTimeLineCommonData(void)

//  Common data, the master timeline has the instance of this data,
//  all other class point to this data.
//
/****************************************************************************/
{
    _TimeLine = NULL;
    _MidFrame = -1;
    _Frames = 2;
    _Min = 0;
    _Max = 10;
    _MarkIn = -1;
    _MarkOut = -1;
    _ActiveTL = NULL;
    _Cursors = NULL;
    _DrawCenterLine = false;
	_FineDisplayBoundary = -1;
	AutoPaint = true;
}

//------------------Cursors------------------John Mertus----Feb 2004---

    void CTimeLineCommonData::Cursors(CTimelineCursors *cur)

//  Sets an address of the cursors
//
//***************************************************************************
{
   _Cursors = cur;
}

//------------------Cursors------------------John Mertus----Feb 2004---

    CTimelineCursors * CTimeLineCommonData::Cursors(void)

//  Returns a pointer to the cursors
//
//***************************************************************************
{
   return _Cursors;
}

//------------------ActiveTL------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::ActiveTL(TFBaseTimeLine *tl)

//  Set the timeline and forces an update
//
//***************************************************************************
{
   if (_ActiveTL == tl) return;
   _ActiveTL = tl;
   Message.Msg = WM_ACTIVATEAPP;
   Notify();
}

//-------------------ActiveTL---------------------John Mertus----Apr 2003---

    TFBaseTimeLine  *CTimeLineCommonData::ActiveTL(void)

//  Return the absolute frame value
//
/****************************************************************************/
{
   return _ActiveTL;
}
//--------------------Min--------------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::Min(double Value)

//  Set the numer of display frames and force an update
//
//***************************************************************************
{
   if (_Min == Value) return;
   _Min = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//---------------------Min------------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::Min(void)

//  Return the number of display frames
//
//***************************************************************************
{
   return _Min;
}

//--------------------Max--------------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::Max(double Value)

//  Set the number of the last display frame and force an update
//
//***************************************************************************
{
   if (_Max == Value) return;
   _Max = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//---------------------Max------------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::Max(void)

//  Return the number of Display frames
//
//***************************************************************************
{
   return _Max;
}
//--------------------MarkIn--------------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::MarkIn(double Value)

//  Set the numer of display frames and force an update
//
//***************************************************************************
{
   if (_MarkIn == Value) return;
   _MarkIn = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//---------------------MarkIn------------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::MarkIn(void)

//  Return the number of display frames
//
//***************************************************************************
{
   return _MarkIn;
}

//--------------------Max--------------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::MarkOut(double Value)

//  Set the number of the last display frame and force an update
//
//***************************************************************************
{
   if (_MarkOut == Value) return;
   _MarkOut = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//---------------------MarkOut------------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::MarkOut(void)

//  Return the number of Display frames
//
//***************************************************************************
{
   return _MarkOut;
}

//------------------Frames------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::Frames(double Value)

//  Set the numer of display frames and force an update
//
//***************************************************************************
{
   if (_Frames == Value) return;
   _Frames = Value;
   Message.Msg = WM_PAINT;
   Notify();
}

//-------------------Frames---------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::Frames(void)

//  Return the number of Display frames
//
/****************************************************************************/
{
   return _Frames;
}


//-------------------MidFrame---------------------John Mertus----Apr 2003---

    double  CTimeLineCommonData::MidFrame(void)

//  Return the number of frame at the center of the timeline
//
/****************************************************************************/
{
   return _MidFrame;
}

//--------------------HideMe----------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::HideMe(TFBaseTimeLine *btl)

// This removes the timeline btl from the master list
//
//***************************************************************************
{
   Message.Msg = WM_CLOSE;
   Message.WParam = (int)btl;
   Notify();
}

//--------------------AddMe----------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::AddMe(TFBaseTimeLine *btl)

// This adds the timeline btl from the master list
//
//***************************************************************************
{
   Message.Msg = WM_SHOWWINDOW;
   Message.WParam = (int)btl;
   Notify();
}

//------------------Changed-------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::Changed(void)

// This removes the timeline btl from the master list
//
//***************************************************************************
{
   Message.Msg = WM_SETTINGCHANGE;
   Notify();
}

//----------------RecomputeAllDisplays--------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::RecomputeAllDisplays(void)

// This calls for an update of all bitmaps
//
//***************************************************************************
{
   Message.Msg = WM_SETREDRAW;
   Notify();
}

//----------------FineDisplayBoundary--------------------John Mertus----Apr 2003---

    int CTimeLineCommonData::FineDisplayBoundary(void)

// This returns the audio boundary between fine/course
//
//***************************************************************************
{
   return _FineDisplayBoundary;
}

//----------------FineDisplayBoundary--------------------John Mertus----Apr 2003---

    void CTimeLineCommonData::FineDisplayBoundary(int Value)

// This sets the audio boundary between fine/course
//
//***************************************************************************
{
  if (_FineDisplayBoundary == Value) return;
   _FineDisplayBoundary = Value;

  Message.Msg = WM_SETREDRAW;
  Notify();
}


void __fastcall TFBaseTimeLine::SelectButtonClick(TObject *Sender)
{
  if (_CommonData->ActiveTL() != this)
    _CommonData->ActiveTL(this);
  else
    _CommonData->ActiveTL(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::PreviousButtonClick(TObject *Sender)
{
   GoToPreviousEvent();
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::NextButtonClick(TObject *Sender)
{
   GoToNextEvent();
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::GoToPreviousEvent(void)
{
   GoToPreviousEventInternal();
}
//---------------------------------------------------------------------------

int TFBaseTimeLine::GoToPreviousEventInternal(void)
{
   if (!HaveTimeLine())
   {
      return -1;
   }

   int i = floor(_CommonData->CursorPosition() + OffsetFrame());

   // Keep it in bounds
   if ((i < 0) || (i >= GetTotalFrameCount()))
   {
      return -1;
   }

   int iFrame = GetPreviousEventFrameIndex(i, EventMask) - OffsetFrame();

   bool Continue = true;
   MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
   if (_OnMenuEventChange != NULL)
   {
      _OnMenuEventChange(this, EVENT_ACTION_PREVIOUS, EventFlag, iFrame, Continue);
   }

   if (!Continue)
   {
      return iFrame;
   }

   _CommonData->CursorPosition(iFrame);
   _CommonData->NextPrevClick(this, iFrame);

   return iFrame;
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::GoToNextEvent(void)
{
   GoToNextEventInternal();
}
//---------------------------------------------------------------------------

int TFBaseTimeLine::GoToNextEventInternal(void)
{
   if (!HaveTimeLine())
   {
      return -1;
   }

   int i = ceil(_CommonData->CursorPosition() + OffsetFrame());

   // Keep it in bounds
   if ((i < 0) || (i >= GetTotalFrameCount()))
   {
      return -1;
   }

   int iFrame = GetNextEventFrameIndex(i, EventMask) - OffsetFrame();

   bool Continue = true;
   MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
   if (_OnMenuEventChange != NULL)
   {
      _OnMenuEventChange(this, EVENT_ACTION_NEXT, EventFlag, iFrame, Continue);
   }

   if (!Continue)
   {
      return iFrame;
   }

   _CommonData->CursorPosition(iFrame);
   _CommonData->NextPrevClick(this, iFrame);

   return iFrame;
}
//---------------------------------------------------------------------------

bool TFBaseTimeLine::ShowTicks()
{
   return _ShowTicks;
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::ShowTicks(bool Value)
{
   if (_ShowTicks == Value) return;
   _ShowTicks = Value;
   RedrawDisplay();
}
//---------------------------------------------------------------------------

bool TFBaseTimeLine::ShowFrameCache()
{
   return _ShowFrameCache;
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::ShowFrameCache(bool Value)
{
   if (_ShowFrameCache == Value) return;
   _ShowFrameCache = Value;
   RedrawDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::HideClick(TObject *Sender)
{
    _CommonData->HideMe(this);
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::UpdateButtons(void)
{
   int i = ceil(_CommonData->CursorPosition() + OffsetFrame());
   if (i >= 0)
    {
      float Frame = GetPreviousEventFrameIndex(i, EventMask);
      PreviousButton->Enabled = (Frame < i) && (Frame >= Min());
      Frame = GetNextEventFrameIndex(i, EventMask);
      NextButton->Enabled = (Frame > i) && (Frame <= Max());
      NextEventMenuItem->Enabled = NextButton->Enabled;
      PreviousEventMenuItem->Enabled = PreviousButton->Enabled;

      // See if an event exists
      DropEventMenuItem->Enabled = ((GetEventFlag(i) & EventMask) != 0);
      AddEventMenuItem->Enabled = !DropEventMenuItem->Enabled;
    }
}

//------------------WMEraseBkgnd------------------John Mertus----May 2003-----

  void _fastcall TFlickerPanel::WMEraseBkgnd(TMessage &Message)

//  Ignore all background erases
//
//****************************************************************************
{
  Message.Msg = 1;
}

//------------------Constructor------------------John Mertus----May 2003-----

__fastcall TFlickerPanel::TFlickerPanel(TComponent* Owner)
        : TPanel(Owner)

//  Blank constructor
//
//****************************************************************************
{
}

void __fastcall TFBaseTimeLine::FrameMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  // Check if we are dragging the cursor
  _Tracking = _Tracking && Shift.Contains(ssLeft);
   if (_Tracking)
     {
        double cp = Frames()*float(X)/_OffScreenBitmap->Width + StartFrame();
        // Don't allow it to get too small
        if (cp < 0) cp = 0;
        _CommonData->CursorPosition(cp);
        return;
     }
}
//---------------------------------------------------------------------------


void __fastcall TFBaseTimeLine::FrameMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if (bDblClickIgnoreMouse)
	{
	 bDblClickIgnoreMouse = false;
	 return;
	}

  // Move the cursor
  bool isOutsideDataDisplay = (Y >= (HidePanel->Top + HidePanel->Height - 2))
					  && ((X >= SelectButton->Left) && (X < (HidePanel->Left)));
  if (!isOutsideDataDisplay)
	 {
		if (Button == mbLeft)
		  {
			  _Tracking = (Sender == dynamic_cast<TObject *>(DataDisplay));
			  if (_Tracking)
				 {
					 double cp = Frames()*float(X)/_OffScreenBitmap->Width + StartFrame();
					 // Don't allow it to get too small
					 if (cp < 0) cp = 0;
					 _CommonData->CursorPosition(cp);
					 SetFocus();
				 }
		  }
	 }
}

//------------------WMGetDlgCode------------------John Mertus----Feb 2004-----

  void _fastcall TFBaseTimeLine::WMGetDlgCode(TWMGetDlgCode &Message)

//  Just tell windows we want to process arrow keys.
//
//****************************************************************************
{
  //We want to receive arrow key codes
  Message.Result = DLGC_WANTARROWS;
}

//--------------------KeyDown---------------------John Mertus----Feb 2004-----

     void __fastcall TFBaseTimeLine::KeyDown(Word &Key, Classes::TShiftState Shift)

//  This processes all the Key presses
//
//****************************************************************************
{
  // Preview the key
  if (OnKeyDown != NULL) OnKeyDown(this, Key, Shift);

#ifdef NAVIGATOR
  if (_CommonData->ActiveTL() == this)
   {
    if (Shift.Contains(ssShift))
     {
      switch (Key)
       {
		  case MTK_F:
           if (PreviousButton->Enabled)
              PreviousButtonClick(this);
           Key = 0; // Consumed!
           break;

        case MTK_S:
           if (NextButton->Enabled)
              NextButtonClick(this);
           Key = 0; // Consumed!
           break;
       }
     }
   }
#else
  switch (Key)
   {
    case  VK_LEFT:
       _CommonData->CursorPosition(_CommonData->CursorPosition() - 1);

       break;

    case VK_RIGHT:
       _CommonData->CursorPosition(_CommonData->CursorPosition() + 1);
       break;

    case VK_PRIOR:
       _CommonData->CursorPosition(_CommonData->CursorPosition() + 2);
       break;

    case VK_NEXT:
       _CommonData->CursorPosition(_CommonData->CursorPosition() - 2);
       break;

    case VK_END:
       _CommonData->CursorPosition(Max());
       break;

    case VK_HOME:
       _CommonData->CursorPosition(Min());
       break;
  }
#endif // NAVIGATOR

  TFrame::KeyDown(Key,Shift);

}

//---------------------------------------------------------------------------


void __fastcall TFBaseTimeLine::DataDisplayMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
	 // Find the frame
    if (Button == mbLeft)
      {
			_Tracking = false;
      }
}


//-------------------ReadSAP-------------------------John Mertus-----Jan 2002---

   bool TFBaseTimeLine::ReadSAP(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   string Section = IniSection + "." + StringToStdString(Name);
   Height = ini->ReadInteger(Section,"Height", Height);

   // This call back may or may not be necessary
   _CommonData->Changed();
   return true;
}

//-------------------------WriteSAP-----------------John Mertus-----Jan 2002---

   bool TFBaseTimeLine::WriteSAP(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   string Section = IniSection + "." + StringToStdString(Name);
   ini->WriteInteger(Section,"Height", Height);
   return true;
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TFBaseTimeLine::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  ShowTicks(ini->ReadBool(StringToStdString(Name), "ShowTicks", ShowTicks()));
  if (ShowTicks() != TicksMenuItem->Checked)
     TicksMenuItem->Checked = ShowTicks();

  ShowFrameCache(ini->ReadBool(StringToStdString(Name), "ShowFrameCache", ShowFrameCache()));
  if (ShowFrameCache() != FrameCacheMenuItem->Checked)
     FrameCacheMenuItem->Checked = ShowFrameCache();

  return true;
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

   bool TFBaseTimeLine::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  ini->WriteBool(StringToStdString(Name), "ShowTicks", ShowTicks());
  ini->WriteBool(StringToStdString(Name), "ShowFrameCache", ShowFrameCache());
  return true;
}
//---------------ReadDesignSettings---------------John Mertus----Sep 2002-----

      bool TFBaseTimeLine::ReadDesignSettings(void)

//  This restorese the settings to their original values.
//
//****************************************************************************
{
  return true;
}
//-----------------ReadDesignSAP------------------John Mertus----Sep 2002-----

      bool TFBaseTimeLine::ReadDesignSAP(void)

//  This restorese the size and position settings to their original values.
//
//****************************************************************************
{
   Height = 22;

   // This call back may or may not be necessary
   _CommonData->Changed();
   return true;
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::DataDisplayDblClick(TObject *Sender)
{
// base time line double click
}
//---------------------------------------------------------------------------

bool TFBaseTimeLine::isActiveTimeline(TFBaseTimeLine* tl)
{
   return (_CommonData->ActiveTL() == tl);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::DropEventMenuItemClick(TObject *Sender)
{
   int frame = _CommonData->CursorPosition() + OffsetFrame();
   HandleDeleteEventRequest(frame, DefaultEvent);
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::HandleDeleteEventRequest(int frame, MTI_TIMELINE_EVENT eventMask)
{
   if (!HaveTimeLine())
   {
      return;
   }

   // Keep it in bounds
   if ((frame < 0) || (frame >= GetTotalFrameCount()))
   {
      return;
   }

   // Now delete it
   bool NotHandled = true;
   MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
   if (_OnMenuEventChange != NULL)
   {
      _OnMenuEventChange(this, EVENT_ACTION_DELETE, EventFlag, frame, /* out */ NotHandled);
   }

   if (NotHandled)
   {
      TimeLine()->ResetEvent();
      DeleteEvent(frame, EventFlag);
      TimeLine()->updateEventFile();
   }

   RedrawDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::AddEventMenuItemClick(TObject *Sender)
{
   int frame = _CommonData->CursorPosition() + OffsetFrame();
   HandleAddEventRequest(frame, DefaultEvent);
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::HandleAddEventRequest(int frame, MTI_TIMELINE_EVENT eventMask)
{
   if (!HaveTimeLine())
   {
      return;
   }

   // Keep it in bounds
   if ((frame < 0) || (frame >= GetTotalFrameCount()))
   {
      return;
   }

   // Now add it
   bool NotHandled = true;
   MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
   if (_OnMenuEventChange != NULL)
   {
      _OnMenuEventChange(this, EVENT_ACTION_ADD, EventFlag, frame, /* out */ NotHandled);
   }

   if (NotHandled)
   {
      TimeLine()->ResetEvent();
      AddEvent(frame, EventFlag);
      TimeLine()->updateEventFile();
   }

   RedrawDisplay();
}

//-------------------OnMenuEventChange-------------John Mertus----Dec 2004---

        void TFBaseTimeLine::OnMenuEventChange(TMenuEventChange ChangeEvent)
//
//***************************************************************************
{
    _OnMenuEventChange = ChangeEvent;
}

//-------------------OnMenuEventChange-------------John Mertus----Dec 2004---

        TMenuEventChange TFBaseTimeLine::OnMenuEventChange(void)

//   Just return the value of the global event function
//
//***************************************************************************
{
    return _OnMenuEventChange;
}

//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::BaseTimelinePopupMenuPopup(TObject *Sender)
{
   UpdateButtons();

   JobManager jobManager;
   BrowseToReportsFolderMenuItem->Enabled = jobManager.DoesJobHaveJobFolderSubfolder(JobManager::reportsType);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ClearAllMarksMenuItemClick(TObject *Sender)
{
   if (!HaveTimeLine())
      return;

   int Response = _MTIConfirmationDialog(Handle,
                 "Please confirm that you wish to permanently delete all of   \n"
                 "the marks on this timeline");
   if (Response != MTI_DLG_OK)
      return;

   DeleteAllEvents(EventMask);

   RedrawDisplay();
}
//---------------------------------------------------------------------------

void TFBaseTimeLine::DeleteAllEvents(MTI_TIMELINE_EVENT eventFlag)
{
   TimeLine()->ResetEvent();
   for (int iFrame = 0; iFrame < GetTotalFrameCount(); ++iFrame)
   {
      bool notHandled = true;

      if (_OnMenuEventChange != NULL)
      {
         _OnMenuEventChange(this, EVENT_ACTION_DELETE, eventFlag, iFrame, notHandled);
      }

      if (notHandled)
      {
         DeleteEvent(iFrame, eventFlag);
      }
   }

   TimeLine()->updateEventFile();
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::RescanforSceneCutsMenuItemClick(
      TObject *Sender)
{
    auto clip = TimeLine()->getClip();
    if (clip == nullptr)
       return;

    string lastClickedClipPath = clip->getClipPathName();
    if (lastClickedClipPath.empty())
       return;

// The following code is from TBinManagerForm::ClipPopupUpdateCutsItemClick()

    TAutoDiffForm *difform = new TAutoDiffForm(this);
    difform->setCurClip(lastClickedClipPath);
    ShowModalDialog(difform);
    delete difform;
    difform = 0;

// End of code stolen from TBinManagerForm::ClipPopupUpdateCutsItemClick()

    // Fix bug 1623 - line stolen from TTimelineForm::performCutAction()
    TimeLine()->ResetEvent();  // force reconstruction

    RedrawDisplay();
}
//---------------------------------------------------------------------------

namespace {
   inline bool isValidTimecodeCharacter(char c)
   {
      bool isSeparator = (c == ':' || c == '.');
      return isSeparator || isdigit(c);
   }
}
//---------------------------------------------------------------------------

   void __fastcall TFBaseTimeLine::ImportEDLMenuItemClick(TObject *Sender)
   {
      const char COMMENT_CHAR('#');
      const char NULL_TERMINATOR('\0');

      if (!HaveTimeLine())
      {
         return;
      }

      // Ask the job manager to suggest where to load the timecodes from.
      // Cuts are special - they go in the cut list job folder.
      // Other timecodes will go in the master clip folder by default.
      JobManager::JobFolderType jobFolderType = JobManager::unknownType;
      string descriptor = "timecodes";
      if (EventMask == EVENT_CUT_MASK)
      {
         jobFolderType = JobManager::cutListsType;
         descriptor = "cuts";
      }

      JobManager jobManager;
      string suggestedFolder = jobManager.GetJobFolderSubfolderPath(jobFolderType);
      string suggestedFilename = jobManager.GetUnversionedJobFolderFileName(descriptor, suggestedFolder, "edl");
      ImportTimecodesOpenDialog->InitialDir = suggestedFolder.c_str();
      ImportTimecodesOpenDialog->FileName = suggestedFilename.c_str();
      ImportTimecodesOpenDialog->Title = "Import EDL";
      ImportTimecodesOpenDialog->Filter = "EDL Files (*.edl)|*.edl|All Files|*.*";

      bool retVal = ImportTimecodesOpenDialog->Execute();
      if (!retVal)
      {
         // cancelled
         return;
      }

      string filename = StringToStdString(ImportTimecodesOpenDialog->FileName);
      if (filename.empty())
      {
         return;
      }

      MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
      CTimecode TC;
      int OutOfRangeCount = 0;
      int InRangeCount = 0;

      auto clip = TimeLine()->getClip();
      if (clip == nullptr)
      {
         MTIostringstream ostr;
         ostr << "Internal error: no current clip    ";
         TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }

      if (!TimeLine()->getVideoCTimecode(0, TC))
      {
         MTIostringstream ostr;
         ostr << "Can't figure out timecode for current clip    ";
         TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }

      FILE *fp = fopen(filename.c_str(), "r");
      if (fp == NULL)
      {
         MTIostringstream ostr;
         ostr << "Can't open file " << filename << " for reading    ";
         TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }

      char line[1000]; // this was static, don't know why.
      while (NULL != fgets(line, sizeof(line), fp))
      {
         int i, j;
         bool foundTC = false;
         bool skipIt = false;
         bool isAllDigits = true;

         // We are looking for lines that look like this:
         // 000426  R060 V C 01:06:22:23 01:06:30:12 01:31:30:07 01:31:37:20 #comment
         // There must be 8 data fields and the 4th field must contain only the letter C.
         // If so, the 7th field will contain the cut timecode.

         char junk[sizeof(line)];
         char edTypeBuffer[sizeof(line)];
         edTypeBuffer[0] = NULL_TERMINATOR;
         char cutTimecodeBuffer[sizeof(line)];
         char ninthFieldBuffer[sizeof(line)];

         // This is safe because each string buffer can hold the whole line.
         int fieldCount = sscanf(line, "%s %s %s %s %s %s %s %s %s", junk, junk, junk, edTypeBuffer, junk, junk, cutTimecodeBuffer, junk,
               ninthFieldBuffer);

         // Don't use this line if it appears to not be a cut event:
         // - it has fewer than 8 fields
         // - it has more than 8 fields, and the 9th doesn't start a comment
         // - the 4th field isn't a 'C' (I don't know what the spec is, so
         // I just check if the field starts with a 'C' or 'c')
         if (fieldCount < 8 || (fieldCount > 8 && ninthFieldBuffer[0] != COMMENT_CHAR) ||
               (edTypeBuffer[0] != 'C' && edTypeBuffer[0] != 'c'))
         {
            // Skip to the next line.
            continue;
         }

         j = -1; // We want i to be pointing at 0 on the first try
         while (!(foundTC || skipIt))
         {
            // Find the first digit of the timecode; stop looking at this
            // line if we hit the null terminator or the comment character
            for (i = j + 1; i < (int)sizeof(cutTimecodeBuffer); ++i)
            {
               if (isdigit(cutTimecodeBuffer[i]))
               {
                  break;
               }

               if (cutTimecodeBuffer[i] == COMMENT_CHAR || cutTimecodeBuffer[i] == NULL_TERMINATOR)
               {
                  skipIt = true;
                  break;
               }
            }

            // Handle going off the end of the cutTimecodeBuffer, which can never happen
            if (i == (int)sizeof(cutTimecodeBuffer))
            {
               skipIt = true;
            }

            if (!skipIt)
            {
               // Got a potential timecode...
               // Find the extent of the entire timecode, which consists of
               // digits and separators (':' and '.')
               for (j = i + 1; j < (int)sizeof(cutTimecodeBuffer); ++j)
               {
                  if (!isValidTimecodeCharacter(cutTimecodeBuffer[j]))
                  {
                     break;
                  }

                  // Need to know if it's a timecode hh:mm:ss:ff or frame number.
                  isAllDigits = isAllDigits && isdigit(cutTimecodeBuffer[j]);
               }

               // Again with the paranoia!
               if (j == (int)sizeof(cutTimecodeBuffer))
               {
                  j -= 1;
               }

               // Null terminate following the alleged timecode, and try to
               // convert it (will return false if it's not a valid timecode)
               cutTimecodeBuffer[j] = '\0';

               if (isAllDigits)
               {
                  foundTC = true;
                  TC.setAbsoluteTime(atoi(&cutTimecodeBuffer[i]));
               }
               else if (TC.getFramesPerSecond() == 0)
               {
                  // TODO: If user selected a frame rate, use that!
                  TC.setFramesPerSecond(24);
                  foundTC = TC.setTimeASCII(&cutTimecodeBuffer[i]);
                  TC.setFramesPerSecond(0);
               }
               else
               {
                  foundTC = TC.setTimeASCII(&cutTimecodeBuffer[i]);
               }
            }

         } // end while still looking for the timecode on this line

         if (foundTC)
         {
            // Add an event at this timecode
            int iFrame;
            bool NotHandled = true;

            if (0 <= (iFrame = TimeLine()->getVideoFrameIndex(TC)))
            {
               ++InRangeCount;
               if (_OnMenuEventChange != NULL)
               {
                  _OnMenuEventChange(this, EVENT_ACTION_ADD, EventFlag, iFrame, NotHandled);
               }

               if (NotHandled)
               {
                  AddEvent(iFrame, EventFlag);
               }
            }
            else
            {
               // Count timecode that were out of range so we can show
               // a warning message at the end
               ++OutOfRangeCount;
            }
         }
      }

      fclose(fp);
      RedrawDisplay();
      clip->updateAllTrackFiles(); // Shouldn't this be TimeLine()->updateEventFIle()? QQQ

      if (OutOfRangeCount)
      {
         string Qualifier = (InRangeCount > 0) ? string("Some") : string("All");
         _MTIWarningDialog(Handle, Qualifier + " timecodes were outside the range for this clip   ");
      }
   }
// ---------------------------------------------------------------------------

// Should implement these with regex.
namespace
{
   string extractReelNumberFromImageName(const string &imageName)
   {
      // Find non-digits at end of name.
      string reelNumber = imageName;
      const string DIGITS = "0123456789";
      auto pos = reelNumber.find_last_of(DIGITS);
      if (pos != string::npos)
      {
         // Find run of digits preceeding the non-digits and erase them all.
         pos = reelNumber.find_last_not_of(DIGITS, pos);
         if (pos != string::npos)
         {
            reelNumber.erase(pos + 1);
         }
         else
         {
            reelNumber.clear();
         }
      }
      else
      {
         reelNumber.clear();
      }

      return reelNumber;
   }

   string sanitizeEdlName(const string &rawName)
   {
      // Translate each run of non-alphanumeric characters to a single underscore.
      MTIostringstream os;
      const char UNDERSCORE_CHAR = '_';
      char prev = 0;

      for (char c : rawName)
      {
         if (!isalnum(c))
         {
            c = UNDERSCORE_CHAR;
         }

         if (c != UNDERSCORE_CHAR || prev != UNDERSCORE_CHAR)
         {
            os << c;
            prev = c;
         }
      }

      string sanitizedName = os.str();

      // Trim trailing underscores.
      auto pos = sanitizedName.find_last_not_of(UNDERSCORE_CHAR);
      if (pos != string::npos)
      {
         sanitizedName.erase(pos + 1);
      }
      else
      {
         sanitizedName.clear();
      }

      return sanitizedName;
   }
};
// ---------------------------------------------------------------------------

void TFBaseTimeLine::ExportEdl(bool asTimecodes)
{
   if (!HaveTimeLine())
   {
      return;
   }

   // Ask the job manager to suggest where to store the cut timecodes to.
   // Cuts are special - they go in the cut list job folder.
   // Other timecodes will go in the master clip folder by default.
   JobManager::JobFolderType jobFolderType = JobManager::unknownType;
   string descriptor = "timecodes";
   if (EventMask == EVENT_CUT_MASK)
   {
      jobFolderType = JobManager::cutListsType;
      descriptor = "cuts";
   }

   JobManager jobManager;
   string suggestedFolder = jobManager.GetJobFolderSubfolderPath(jobFolderType);
   string suggestedFilename = jobManager.GetUnversionedJobFolderFileName(descriptor, suggestedFolder, "edl");
   ExportTimecodesSaveDialog->InitialDir = suggestedFolder.c_str();
   ExportTimecodesSaveDialog->FileName = suggestedFilename.c_str();
   ExportTimecodesSaveDialog->Title = "Export EDL";
   ExportTimecodesSaveDialog->Filter = "EDL Files (*.edl)|*.edl|All Files|*.*";

   bool retVal = ExportTimecodesSaveDialog->Execute();
   if (!retVal)
   {
      // cancelled
      return;
   }

   string filename = StringToStdString(ExportTimecodesSaveDialog->FileName);
   if (filename.empty())
   {
      return;
   }

   MTI_TIMELINE_EVENT EventFlag = EventMask; // all visible marks for this timeline
   FILE *fp = fopen(filename.c_str(), "w");

   if (fp == NULL)
   {
      MTIostringstream ostr;
      ostr << "Can't open file " << filename << " for writing    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   auto clip = TimeLine()->getClip();
   CTimecode clipInTimecode = clip->getInTimecode();
   CTimecode clipOutTimecode = clip->getOutTimecode();
   if (asTimecodes)
   {
      if (clipInTimecode.getFramesPerSecond() == 0)
      {
         PTimecode ptc;
         ptc.getDefaultUserPreferredStringStyle();
         int fps;
         bool df;
         ptc.getDefaultFpsAndDropFrame(fps, df);
         clipInTimecode.setFramesPerSecond(fps);
         clipOutTimecode.setFramesPerSecond(fps);
         if (df)
         {
            clipInTimecode.setFlags(DROPFRAME);
            clipOutTimecode.setFlags(DROPFRAME);
         }
      }
   }
   else
   {
      clipInTimecode.setFramesPerSecond(0);
      clipOutTimecode.setFramesPerSecond(0);
      clipInTimecode.setFlags(0);
      clipOutTimecode.setFlags(0);
   }

   // For the EDL "TITLE" we use the clip path relative to the main root.
   // For the EDL "Reel Number" we use the part of the image file name before
   // that comes before the frame number (so, e.g. Reel_1_0086400.dpx yields "Reel_1").
   // If the image file name begins with the frame number, then we switch to
   // using the BIN path for the title, and the CLIP NAME for the reel number.
   // Note that where paths and names are used, they are "sanitized" by translating
   // all non-alphanumeric characters to underscores. If there are runs of
   // non-alphanumeric characters, each of those are translated to a single
   // underscore, and trailing underscores will be trimmed.
   CVideoFrameList *videoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
	CVideoFrame* videoFrame = videoFrameList->getFrame(0);
   string imageFilePath = videoFrameList->getImageFilePath(videoFrame);
   string imageFileName = GetFileName(imageFilePath);   // without extension (no ".dpx")

   ClipIdentifier clipId(clip->getBinPath(), clip->getClipName());
   string edlTitle = sanitizeEdlName(CPMPGetPathRelativeToBinRoot(clipId.GetMasterClipPath()));
   string edlReelNumber = sanitizeEdlName(extractReelNumberFromImageName(imageFileName));
   if (edlReelNumber.empty())
   {
      edlTitle = sanitizeEdlName(CPMPGetPathRelativeToBinRoot(clipId.GetMasterClipId().GetBinPath()));
      edlReelNumber = sanitizeEdlName(clipId.GetMasterClipName());
   }

   fprintf(fp, "TITLE:  %s\n", edlTitle.c_str());
   fprintf(fp, "FCM: %sDROP FRAME\n", ((clipInTimecode.getFlags() & DROPFRAME) ? "" : "NON-"));

   int shotInFrameIndex = -1; // make sure we look at frame 0!
   int shotOutFrameIndex = 0; // Always put a cut at frame 0!
   bool foundAnotherCut = true;
   for (int eventNumber = 1; foundAnotherCut; ++eventNumber)
   {
      shotInFrameIndex = shotOutFrameIndex;
      shotOutFrameIndex = GetNextEventFrameIndex(shotInFrameIndex, EventFlag);
      if (shotOutFrameIndex == shotInFrameIndex)
      {
         // No more cuts - last shot ends at end of clip!
         shotOutFrameIndex = clipOutTimecode - clipInTimecode;
         foundAnotherCut = false;
      }

      CTimecode shotInTimecode = clipInTimecode + shotInFrameIndex;
      CTimecode shotOutTimecode = clipInTimecode + shotOutFrameIndex;

      // Build the shot timecode strings
      string shotInTimecodeString;
      string shotOutTimecodeString;
      shotInTimecode.getTimeString(shotInTimecodeString);
      shotOutTimecode.getTimeString(shotOutTimecodeString);

      int writeCount =
         fprintf(fp, "%06d  %-32s V     C        %s %s %s %s\n",
                     eventNumber,
                     edlReelNumber.c_str(),
                     shotInTimecodeString.c_str(),
                     shotOutTimecodeString.c_str(),
                     shotInTimecodeString.c_str(),
                     shotOutTimecodeString.c_str());

      if (writeCount < 0)
      {
         MTIostringstream ostr;
         ostr << "Can't write to file " << filename << "    ";
         TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         fclose(fp);
         return;
      }
   }

   if (fclose(fp) == EOF)
   {
      MTIostringstream ostr;
      ostr << "Got error while closing file " << filename << "    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ExportEdlAsTimecodesMenuItemClick(TObject *Sender)
{
   ExportEdl(true);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ExportEdlAsFrameNumbersMenuItemClick(TObject *Sender)
{
   ExportEdl(false);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ImportCutListMenuItemClick(TObject *Sender)
{
   const char COMMENT_CHAR('#');
   const char NULL_TERMINATOR('\0');

   if (!HaveTimeLine())
   {
      return;
   }

   // Ask the job manager to suggest where to load the timecodes from.
   // Cuts are special - they go in the cut list job folder.
   // Other timecodes will go in the master clip folder by default.
   JobManager::JobFolderType jobFolderType = JobManager::unknownType;
   string descriptor = "timecodes";
   if (EventMask == EVENT_CUT_MASK)
   {
      jobFolderType = JobManager::cutListsType;
      descriptor = "cuts";
   }

   JobManager jobManager;
   string suggestedFolder = jobManager.GetJobFolderSubfolderPath(jobFolderType);
   string suggestedFilename = jobManager.GetUnversionedJobFolderFileName(descriptor, suggestedFolder, "txt");
   ImportTimecodesOpenDialog->InitialDir = suggestedFolder.c_str();
   ImportTimecodesOpenDialog->FileName = suggestedFilename.c_str();
   ImportTimecodesOpenDialog->Title = "Import Cut List";
   ImportTimecodesOpenDialog->Filter = "Text Files (*.txt)|*.txt|All Files|*.*";

   bool retVal = ImportTimecodesOpenDialog->Execute();
   if (!retVal)
   {
      return;
   } // cancelled

   string filename = StringToStdString(ImportTimecodesOpenDialog->FileName);
   if (filename.empty())
   {
      return;
   }

   MTI_TIMELINE_EVENT EventFlag = DefaultEvent;
   CTimecode TC;
   int OutOfRangeCount = 0;
   int InRangeCount = 0;

   auto clip = TimeLine()->getClip();
   if (clip == nullptr)
   {
      MTIostringstream ostr;
      ostr << "Internal error: no current clip    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   if (!TimeLine()->getVideoCTimecode(0, TC))
   {
      MTIostringstream ostr;
      ostr << "Can't figure out timecode for current clip    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   FILE *fp = fopen(filename.c_str(), "r");
   if (fp == NULL)
   {
      MTIostringstream ostr;
      ostr << "Can't open file " << filename << " for reading    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }
   static char buffer[1000];
   while (NULL != fgets(buffer, sizeof(buffer), fp))
   {
      int i, j;
      bool foundTC = false;
      bool skipIt = false;
      bool isAllDigits = true;

      j = -1; // We want i to be pointing at 0 on the first try
      while (!(foundTC || skipIt))
      {
         // Find the first digit of the timecode; stop looking at this
         // line if we hit the null terminator or the comment character
         for (i = j + 1; i < (int)sizeof(buffer); ++i)
         {
            if (isdigit(buffer[i]))
            {
               break;
            }
            if (buffer[i] == COMMENT_CHAR || buffer[i] == NULL_TERMINATOR)
            {
               skipIt = true;
               break;
            }
         }
         // Handle going off the end of the buffer, which can never happen
         if (i == (int)sizeof(buffer))
         {
            skipIt = true;
         }

         if (!skipIt)
         {
            // Got a potential timecode...
            // Find the extent of the entire timecode, which consists of
            // digits and separators (':' and '.')
            for (j = i + 1; j < (int)sizeof(buffer); ++j)
            {
               if (!isValidTimecodeCharacter(buffer[j]))
               {
                  break;
               }

               // Need to know if it's a timecode hh:mm:ss:ff or frame number.
               isAllDigits = isAllDigits && isdigit(buffer[j]);
            }

            // Again with the paranoia!
            if (j == (int)sizeof(buffer))
            {
               j -= 1;
            }

            // Null terminate following the alleged timecode, and try to
            // convert it (will return false if it's not a valid timecode)
            buffer[j] = '\0';

            if (isAllDigits)
            {
               foundTC = true;
               TC.setAbsoluteTime(atoi(&buffer[i]));
            }
            else if (TC.getFramesPerSecond() == 0)
            {
               // TODO: If user selected a frame rate, use that!
               TC.setFramesPerSecond(24);
               foundTC = TC.setTimeASCII(&buffer[i]);
               TC.setFramesPerSecond(0);
            }
            else
            {
               foundTC = TC.setTimeASCII(&buffer[i]);
            }
         }

      } // end while still looking for the timecode on this line

      if (foundTC)
      {
         // Add an event at this timecode
         int iFrame;
         bool NotHandled = true;

         if (0 <= (iFrame = TimeLine()->getVideoFrameIndex(TC)))
         {
            ++InRangeCount;
            if (_OnMenuEventChange != NULL)
            {
               _OnMenuEventChange(this, EVENT_ACTION_ADD, EventFlag, iFrame, NotHandled);
            }
            if (NotHandled)
            {
               AddEvent(iFrame, EventFlag);
            }
         }
         else
         {
            // Count timecode that were out of range so we can show
            // a warning message at the end
            ++OutOfRangeCount;
         }
      }
   }
   fclose(fp);
   RedrawDisplay();
   clip->updateAllTrackFiles(); // Shouldn't this be TimeLine()->updateEventFIle()? QQQ

   if (OutOfRangeCount)
   {
      string Qualifier = (InRangeCount > 0) ? string("Some") : string("All");
      _MTIWarningDialog(Handle, Qualifier + " timecodes were outside the range for this clip   ");
   }
}
// ---------------------------------------------------------------------------

void TFBaseTimeLine::ExportCutList(bool asTimecodes)
{
   if (!HaveTimeLine())
   {
      return;
   }

   // Ask the job manager to suggest where to store the cut timecodes to.
   // Cuts are special - they go in the cut list job folder.
   // Other timecodes will go in the master clip folder by default.
   JobManager::JobFolderType jobFolderType = JobManager::unknownType;
   string descriptor = "timecodes";
   if (EventMask == EVENT_CUT_MASK)
   {
      jobFolderType = JobManager::cutListsType;
      descriptor = "cuts";
   }

   JobManager jobManager;
   string suggestedFolder = jobManager.GetJobFolderSubfolderPath(jobFolderType);
   string suggestedFilename = jobManager.GetUnversionedJobFolderFileName(descriptor, suggestedFolder, "txt");
   ExportTimecodesSaveDialog->InitialDir = suggestedFolder.c_str();
   ExportTimecodesSaveDialog->FileName = suggestedFilename.c_str();

   bool retVal = ExportTimecodesSaveDialog->Execute();
   if (!retVal)
   {
      // cancelled
      return;
   }

   string filename = StringToStdString(ExportTimecodesSaveDialog->FileName);
   if (filename.empty())
   {
      return;
   }

   MTI_TIMELINE_EVENT EventFlag = EventMask; // all visible marks for this timeline
   FILE *fp = fopen(filename.c_str(), "w");

   if (fp == NULL)
   {
      MTIostringstream ostr;
      ostr << "Can't open file " << filename << " for writing    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   int nextFrame = -1;
   int oldFrame = -1; // make sure we look at frame 0!
   auto clip = TimeLine()->getClip();
   CVideoFrameList *videoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
   CTimecode inTimecode = videoFrameList->getInTimecode();

   while (oldFrame < (nextFrame = GetNextEventFrameIndex(oldFrame, EventFlag)))
   {
      CTimecode timecode = inTimecode + nextFrame;
      string TCString;

      if (asTimecodes)
      {
         if (timecode.getFramesPerSecond() == 0)
         {
            PTimecode ptc;
            ptc.getDefaultUserPreferredStringStyle();
            int fps;
            bool df;
            ptc.getDefaultFpsAndDropFrame(fps, df);
            timecode.setFramesPerSecond(fps);
            if (df)
            {
               timecode.setFlags(DROPFRAME);
            }
         }
      }
      else
      {
         timecode.setFramesPerSecond(0);
         timecode.setFlags(0);
      }

      timecode.getTimeString(TCString);
      TCString += "\n";
      if (EOF == fputs(TCString.c_str(), fp))
      {
         MTIostringstream ostr;
         ostr << "Can't write to file " << filename << "    ";
         TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         fclose(fp);
         return;
      }
      oldFrame = nextFrame;
   }
   if (EOF == fclose(fp))
   {
      MTIostringstream ostr;
      ostr << "Got error while closing file " << filename << "    ";
      TRACE_0(errout << "ERROR: BaseTimeLine: " << ostr.str());
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ExportCutListAsTimecodesMenuItemClick(TObject *Sender)
{
   ExportCutList(true);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ExportCutListAsFrameNumbersMenuItemClick(TObject *Sender)
{
   ExportCutList(false);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::ImportBookmarkEventsMenuItemClick(TObject *Sender)

{
   ImportBookmarkEvents();
}

void TFBaseTimeLine::ImportBookmarkEvents()
{
}
//---------------------------------------------------------------------------
void __fastcall TFBaseTimeLine::ExportBookmarkEventsMenuItemClick(TObject *Sender)
{
   ExportBookmarkEvents();
}

void TFBaseTimeLine::ExportBookmarkEvents()
{
}
// ---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::BrowseToReportsFolderMenuItemClick(TObject *Sender)
{
   JobManager jobManager;
   string reportsFolder = jobManager.GetJobFolderSubfolderPath(JobManager::reportsType);

   if (reportsFolder.empty())
   {
      return;
   }

   ::ShellExecute(NULL, "open", reportsFolder.c_str(), NULL, NULL, SW_SHOWDEFAULT);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::TicksMenuItemClick(TObject *Sender)
{
   bool checked = TicksMenuItem->Checked;
   ShowTicks(checked);
}
//---------------------------------------------------------------------------

void __fastcall TFBaseTimeLine::FrameCacheMenuItemClick(TObject *Sender)
{
   bool checked = FrameCacheMenuItem->Checked;
   ShowFrameCache(checked);
}
//---------------------------------------------------------------------------
//
// Overridable Timeline stuff for Bookmark MEGAHACK-O-RAMA.
//
bool TFBaseTimeLine::HaveTimeLine()
{
   return TimeLine() != NULL;
}

int TFBaseTimeLine::GetTotalFrameCount()
{
   return TimeLine()->getTotalFrameCount();
}

string TFBaseTimeLine::GetVideoTC(int frameIndex)
{
   return TimeLine()->getVideoTC(frameIndex);
}

int TFBaseTimeLine::GetVideoFrameIndex(CTimecode timecode)
{
   return TimeLine()->getVideoFrameIndex(timecode);
}

int TFBaseTimeLine::GetPreviousEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask)
{
   return TimeLine()->getPreviousEvent(currentFrameIndex, eventMask);
}

int TFBaseTimeLine::GetNextEventFrameIndex(int currentFrameIndex, MTI_TIMELINE_EVENT eventMask)
{
   return TimeLine()->getNextEvent(currentFrameIndex, eventMask);
}

MTI_TIMELINE_EVENT TFBaseTimeLine::GetEventFlag(int frameIndex)
{
   return TimeLine()->getEventFlag(frameIndex);
}

void TFBaseTimeLine::DeleteEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag)
{
   TimeLine()->deleteEvent(frameIndex, eventFlag, false, true);
}

void TFBaseTimeLine::AddEvent(int frameIndex, MTI_TIMELINE_EVENT eventFlag)
{
   TimeLine()->addEvent(frameIndex, eventFlag, false, true);
}

CMultiFrameEvent TFBaseTimeLine::GetNextMultiFrameEvent(int frameIndex, MTI_TIMELINE_EVENT mfEventMask)
{
   return TimeLine()->getNextMultiFrameEvent(frameIndex, mfEventMask);
}
//---------------------------------------------------------------------------
