// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "Clip3.h"
#include <math.h>

#include "MasterTimeLineFrame.h"

#include "JobManager.h"
#include "MTIstringstream.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma link "BookmarkTimeLineFrame"
#pragma link "EventTimeLineFrame"
#pragma link "RefTimeLineFrame"
#pragma link "AnimateTimeLineFrame"
#pragma link "AudioTimeLineFrame"
#pragma link "GroupTimeLineFrame"
#pragma resource "*.dfm"

TImageList *GlobalImageList;

// ---------------------------------------------------------------------------
#define TIMELINE_MARGIN 0
#define MAX_PIXELS_PER_FRAME 100
// ---------------------------------------------------------------------------

TFMasterTimeLine *FMasterTimeLine;

// ---------------------------------------------------------------------------
__fastcall TFMasterTimeLine::TFMasterTimeLine(TComponent* Owner) : TFrame(Owner)
{
   _CurrentFrame        = -1;
   _TimelineChanged     = NULL;
   _ShowHandleData      = 0;
   _StartHandleCount    = 0;
   _EndHandleCount      = 0;
   _AllowHandles        = false;
   _Scrolling           = false;
   _NonScrollingRange   = .80;
   _InhibitCallback     = 0;
   _TrackBarAtTop       = false;
   _TrackBarInitTrigger = false;
   _OldClipName         = "";
   _FirstResize         = true;
   _AutoSize            = false;
   _RawTimeLine         = new CTimeLine();

   // Build the time line
   // Disable trackbar if no clip loaded
   // NO! NEVER DISABLE THESE TRACKBARS -because sometimes they don't enable
   ResizeTrackBar->Enabled = true; // false;
   TrackBar->Enabled       = true; // false;
   // Instead do softer enable
   setResizeTrackBarSoftEnable(false);
   setTrackBarSoftEnable(false);
   TrackBar->Width         = Width - 2 * TrackBar->Left;
   TrackBar->Height        = 30;
   TrackBar->TrackColor    = (TColor)0xE8E8E8;
   TrackBar->OnKeyDown     = TrackBarKeyDown;
   TrackBar->OnSpecialDrag = SpecialDrag;
   TrackBar->Range(0);
   TrackBar->Minimum = 0;
   TrackBar->Maximum = 0;

   TrackBar->ShowHandles = false;
   TrackBar->ShowMarks   = false;

   // This needs to be replaced
   GlobalImageList = WhatIsImageList;

   // Build all the time lines

   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_CUT));

   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_SCENE_CUT));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_CADENCE));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_MARK));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_PANDS));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_LOGGER_PICTURE));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_LOGGER_AUDIO));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_ANIMATE));
   ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_REF));
   for (int i = 0; i < MAX_AUDIO_CHANNELS; i++)
   {
      ReservedTimeLines.push_back(new CTimeLineDefs(tlTYPE_AUDIO, i));
   }

   // Set the tag to the event number
   // TBD add all tags to the menus
   AudioMenuItem->Tag = tlTYPE_AUDIO;

   PictureEvent1->Tag         = tlTYPE_SCENE_CUT;
   MarksMenuItem->Tag         = tlTYPE_MARK;
   MaskAnimationMenuItem->Tag = tlTYPE_ANIMATE;

   // Set the hook for the change
   SET_CBHOOK(CommonDataChanged, _CommonData);
   InitIt = true;

   _AudioSlipIncrement = .1;
   _OnPrevNextChange   = NULL;
   _ResizePoint        = 10;
   _OldStartFrame      = -1;
   _OldStopFrame       = -1;

   // Default case is to show popup menu
   SetShowEmptyMenu(false);

   // Last thing, do the timeline
   TimeLine(_RawTimeLine);
   _CommonData.Cursors(TrackBar->Cursors());
   TrackBarRangeChange(NULL);

}

__fastcall TFMasterTimeLine::~TFMasterTimeLine()
{
   delete _RawTimeLine;
   _RawTimeLine = 0;
}

void TFMasterTimeLine::SetAutoRefresh(bool value)
{
   _CommonData.AutoPaint = value;
   RedrawDisplay();
}

// -----------------CommonDataChanged-----------------John Mertus----Mar 2002----

void TFMasterTimeLine::CommonDataChanged(void *Sender)

// This is called whenever the CommonData has Changed
// A poor man's message passing.  Parse the message from the child
//
// ****************************************************************************
{
   int i;
   TFBaseTimeLine *btl;

   switch (_CommonData.Message.Msg)
   {
   case WM_PAINT:
      RedrawDisplay();
      if ((_OnChange != NULL) && (_InhibitCallback == 0))
      {
         (_OnChange)(this);
      }
      break;

   case WM_ACTIVATEAPP:
      RedrawDisplay();
      if (_OnSelectChange != NULL)
      {
         _OnSelectChange(_CommonData.ActiveTL());
      }
      break;

   case WM_CLOSE:
      btl = (TFBaseTimeLine*)_CommonData.Message.WParam;
      ReservedTimeLines.Move(btl, CurrentTimeLines);
      BuildTimeLines(CurrentTimeLines);
      break;

   case WM_SETREDRAW:
      for (int i = 0; i < NumberOfCurrentTimeLines(); i++)
      {
         CurrentTimeLine(i)->CommonComputeBitmap();
      }
      break;

   case WM_PAINTICON:
      TrackBar->CursorPositionInternal(_CommonData.dValue, TrackBar->ActiveCursor);
      UpdateDisplay();
      break;

   case WM_COMMNOTIFY:
      if (_OnPrevNextChange != NULL)
      {
         _OnPrevNextChange((TObject *)_CommonData.Message.WParam, _CommonData.Message.LParam);
      }
      break;

   case WM_SETTINGCHANGE:
      ResizeTimeLines();
      break;
   }
}

// --------------NumberOfCurrentTimeLines---------------John Mertus----Feb 2004---

int TFMasterTimeLine::NumberOfCurrentTimeLines(void)

// Return the number of current base lines
//
// ****************************************************************************
{
   return CurrentTimeLines.size();
}

// ----------------CurrentTimeLine------------------John Mertus----Feb 2004---

TFBaseTimeLine *TFMasterTimeLine::CurrentTimeLine(int idx)

// Returns the time line at index idx, NULL if out of range
//
// ****************************************************************************
{
   if ((idx < 0) || (idx >= NumberOfCurrentTimeLines()))
   {
      return NULL;
   }
   return CurrentTimeLines[idx]->TimeLine;
}

// --------------NumberOfSelectedTimeLines---------------John Mertus----Feb 2004---

int TFMasterTimeLine::NumberOfSelectedTimeLines(void)

// Return the number of selected base lines
//
// ****************************************************************************
{
   int Result = 0;
   // Code for multibutton select
   // for (int i=0; i < NumberOfCurrentTimeLines(); i++)
   // if (CurrentTimeLine(i)->MultiSelectSpeedButton->Down) Result++;
   if (_CommonData.ActiveTL() != NULL)
   {
      Result = 1;
   }

   return Result;
}

// ---------------SelectedTimeLine-----------------John Mertus----Feb 2004---

TFBaseTimeLine *TFMasterTimeLine::SelectedTimeLine(int idx)

// Returns the time line at index idx, NULL if out of range
//
// ****************************************************************************
{
   // int n = 0;
   if ((idx < 0) || (idx >= NumberOfSelectedTimeLines()))
   {
      return NULL;
   }
   return _CommonData.ActiveTL();
   /*
    // Code for multi select
    for (int i=0; i < NumberOfCurrentTimeLines(); i++)
    if (CurrentTimeLine(i)->MultiSelectSpeedButton->Down)
    {
    n++;
    if (n == idx) return CurrentTimeLine(i);
    }

    return NULL;
    */
}

// -----------------RedrawDisplay------------------John Mertus----Apr 2003--- */

void TFMasterTimeLine::RedrawDisplay(void)

// This forces updates to all the children displays and its timecodes
//
// ****************************************************************************
{
   if (_CommonData._TimeLine == NULL)
   {
      return;
   }

   for (int i = 0; i < NumberOfCurrentTimeLines(); i++)
   {
      CurrentTimeLine(i)->RedrawDisplay();
   }

   // Update the track bar but don't generate a callback
   TrackBar->PositionOfThumb(ThumbPosition());
}

// -----------------UpdateDisplay------------------John Mertus----Apr 2003--- */

void TFMasterTimeLine::UpdateDisplay(void)

// This forces updates to all the children displays and its timecodes
//
// ****************************************************************************
{
   if (_CommonData._TimeLine == NULL)
   {
      return;
   }

   for (int i = 0; i < NumberOfCurrentTimeLines(); i++)
   {
      CurrentTimeLine(i)->DataDisplayPaint(NULL);
   }

   // Update the track bar but don't generate a callback
   TrackBar->PositionOfThumb(ThumbPosition());
}

// ---------------------------------------------------------------------------

// --------------------TotalFrames---------------------John Mertus----Apr 2003----

MTI_INT32 TFMasterTimeLine::TotalFrames(void)

// Return the size of display in frames
//
// ****************************************************************************
{
   if (TimeLine() == NULL)
   {
      return 0;
   }
   return TimeLine()->getTotalFrameCount();
}

// -----------------CurrentFrame------------------John Mertus----Apr 2003---

void TFMasterTimeLine::CurrentFrame(double Value)

// Sets the current displayed frame
//
// ****************************************************************************
{
   _InhibitCallback++;
   // QQQ mbraca - this 'if' does not make sense to me, because
   // I NEED to set the current frame while dragging because the callback
   // from the Player/Displayer is inhibited!
   // if (!TrackBar->IsDragging())
   // {
   CurrentFrameInternal(Value);
   TrackBar->Position(Value);
   if (!_CommonData.AutoPaint)
   {
      UpdateDisplay(); // Make sure cursors get re-drawn
   }
   // }
   // Original trunk:
   // if (!TrackBar->IsDragging())
   // CurrentThumbFrameInternal(Value);

   _InhibitCallback--;
}

// -----------------NonScrollingRange------------------John Mertus----Apr 2003---

double TFMasterTimeLine::NonScrollingRange(void)

// Return the current scrolling range, note Scrolling must be set on
// for the scrolling range to have any effect.
//
// ****************************************************************************
{
   return _NonScrollingRange;
}

// -----------------NonScrollingRange------------------John Mertus----Apr 2003---

void TFMasterTimeLine::NonScrollingRange(double Value)

// Set the current scrolling range, note Scrolling must be set on
// for the scrolling range to have any effect.
// NonScrolling range is between 0 and 1.
//
// ****************************************************************************
{
   if (Value < 0)
   {
      Value = 0;
   }
   if (Value > 1)
   {
      Value = 1;
   }
   if (Value == _NonScrollingRange)
   {
      return;
   }
   _NonScrollingRange = Value;
}

// -----------------CurrentFrame------------------John Mertus----Apr 2003---

double TFMasterTimeLine::CurrentFrame(void)

// Return the current displayed frame
//
// ****************************************************************************
{
   return _CurrentFrame;
}

// -----------------ThumbPosition------------------John Mertus----Apr 2003---

double TFMasterTimeLine::ThumbPosition(void)

// Return the center point of the trackbar slider
//
// ****************************************************************************
{
   return TrackBar->PositionOfThumb();
}

// -----------------ThumbPosition------------------John Mertus----Apr 2003---

void TFMasterTimeLine::ThumbPosition(double Value)

// set the center point of the trackbar slider
//
// ****************************************************************************
{
   int oldPos = ThumbPosition();
   TrackBar->PositionOfThumb(Value);
   if (oldPos != ThumbPosition())
   {
      TrackBarChange(NULL);
   }
}

// -------------------AudioSlipIncrement------------------John Mertus----Apr 2003---

double TFMasterTimeLine::AudioSlipIncrement(void)

// Return the current displayed frame
//
// ****************************************************************************
{
   return _AudioSlipIncrement;
}

// -------------------AudioSlipIncrement------------------John Mertus----Apr 2003---

double TFMasterTimeLine::AudioSlipIncrement(double Value)

// Sets and returns the current displayed frame
//
// ****************************************************************************
{
   if (Value == _AudioSlipIncrement)
   {
      return _AudioSlipIncrement;
   }
   _AudioSlipIncrement = Value;
   return _AudioSlipIncrement;
}

// -------------CurrentFrameInternal------------John Mertus----Apr 2003---

void TFMasterTimeLine::CurrentFrameInternal(double Value)

// Set the current frame to the value and update it.
//
// ****************************************************************************
{
   if (_CurrentFrame != Value)
   {
      _CurrentFrame = Value;
      RedrawDisplay();
   }
   if ((Value >= 0) && _TrackBarInitTrigger)
   {
      _TrackBarInitTrigger = false;

      // Need to position it twice to make sure the "change" code
      // gets triggered (short circuits if it thinks it didn't change)
      int savePosition              = ResizeTrackBar->ThumbPosition;
      ResizeTrackBar->ThumbPosition = ResizeTrackBar->Minimum;
      ResizeTrackBar->ThumbPosition = savePosition;
      // (ResizeTrackBar->Minimum + ResizeTrackBar->Maximum) / 2;

      // For it to be even
      int newTrackBarRange = ResizeTrackBarWarpPtoV(ResizeTrackBar->ThumbPosition);
      newTrackBarRange += newTrackBarRange % 2;
      TrackBar->Range(newTrackBarRange);
      TrackBar->ScrollToFrame(Value);
   }
}

// -------------ThumbPositionInternal------------John Mertus----Apr 2003---

void TFMasterTimeLine::ThumbPositionInternal(double Value)

// Move the track bar slider to be centered at the frame specified by Value
//
// ****************************************************************************
{
   if (TrackBar->PositionOfThumb() == Value && _CommonData.MidFrame() == Value)
   {
      return;
   }

   TrackBar->PositionOfThumb(Value);
   _CommonData.MidFrame(Value);

   RedrawDisplay();
}

// --------------------Frames---------------------John Mertus----Apr 2003----

double TFMasterTimeLine::Frames(void)

// Return the size of display in frames
//
// ****************************************************************************
{
   return _CommonData.Frames();
}

// ------------------StartFrame------------------John Mertus----Apr 2003--- */

double TFMasterTimeLine::StartFrame(void)

// Return the current start frame
//
// ****************************************************************************
{
   double nResult = ThumbPosition() - Frames() / 2;
   if (nResult < 0)
   {
      nResult = 0;
   }

   return nResult;
}

// ------------------StopFrame------------------John Mertus----Apr 2003--- */

double TFMasterTimeLine::StopFrame(void)

// Return the current stop frame (exclusive)
//
// ****************************************************************************
{
   double nResult = ThumbPosition() + (Frames() / 2) + 1;
   if (nResult > TotalFrames())
   {
      nResult = TotalFrames();
   }

   return nResult;
}

// --------------------Frames---------------------John Mertus----Apr 2003----

void TFMasterTimeLine::Frames(double nSize)

// Sets the number of frames to display.
// Minimum size is 2.
//
// ****************************************************************************
{
   // We are going to try to show nFramesToShow in the timeline. Normally
   // we just spread that number of frames out over the entire width of the
   // timeline, but we restrict the width of each frame to MAX_PIXELS_PER_FRAME
   // because otherwise it just looks stupid to have giant frames if the clip
   // only has a few frames. So there will be dark gray patches on each side
   // of the displayed frames in that case.

   int nFramesToShow     = nSize;
   int nMinWidthInFrames = (Width < MAX_PIXELS_PER_FRAME) ? 1 : Width / MAX_PIXELS_PER_FRAME;
   if (nFramesToShow < nMinWidthInFrames)
   {
      nFramesToShow = nMinWidthInFrames;
   }

   // Update the GUI
   // Note: MUST NEVER SET _CommonData.Frames() TO ANYTHING LESS THAN 2 !!!
   int newWidthInFrames = (nFramesToShow < 3) ? 3 : nFramesToShow;
   if (_CommonData.Frames() != newWidthInFrames)
   {
      _CommonData.Frames(newWidthInFrames);
      RedrawDisplay();
   }

   // We want to set the width of the blue thumb in the main trackbar --
   // This is not subject to the 2-frame minimum, but is subject to a
   // maximum represented by the full size of the clip.
   // HACK-O-RAMA: We added a "Soft" enable to control the track bar - if
   // it isn't "soft-enabled", then try not to show anything at all!
   int nTotalFrames = TrackBar->Maximum - TrackBar->Minimum + 1;
   int newTrackBarRange = (nFramesToShow < nTotalFrames) ? nFramesToShow : nTotalFrames;
   newTrackBarRange += newTrackBarRange % 2;
   TrackBar->Range(newTrackBarRange);
   TrackBarChange(NULL);

   // Disable the resize track bar if it isn't needed
   setResizeTrackBarSoftEnable(nTotalFrames > nMinWidthInFrames);
}

// ---------------CurrentFrameForceChangeCB--------John Mertus----Apr 2003----

void TFMasterTimeLine::CurrentFrameForceChangeCB(double Value)

// Sets the number of frames to display.
// Minimum size is 2.
//
// ****************************************************************************
{
   if (CurrentFrame() == Value)
   {
      return;
   }
   CurrentFrame(Value);
   if (_OnChange != NULL)
   {
      (_OnChange)(this);
   }
}

// ---------------ThumbFrameForceChangeCB----------John Mertus----Apr 2003----

void TFMasterTimeLine::ThumbFrameForceChangeCB(double Value)

// Called when slider is moved?
//
// ****************************************************************************
{
   if (ThumbPosition() == Value)
   {
      return;
   }
   ThumbPositionInternal(Value);
   if (_OnChange != NULL)
   {
      (_OnChange)(this);
   }
}

// --------------------TimeLine---------------------John Mertus----Apr 2003----

CTimeLine *TFMasterTimeLine::TimeLine(void)

// Return the size of display in frames
//
// *************************************************************************** /
{
   return _CommonData._TimeLine;
}

// --------------------TimeLine---------------------John Mertus----Apr 2003----

void TFMasterTimeLine::TimeLine(CTimeLine *Value)

// Return the size of display in frames
//
// ************************************************************************** */
{
   _CommonData._TimeLine = Value;

   // Reset some values
   _StartHandleCount = 0;
   _EndHandleCount   = 0;
   if (_CommonData._TimeLine == NULL)
   {
      RedrawDisplay();
      return;
   }

   // Compute the handles
   _StartHandleCount = _CommonData._TimeLine->getHandleCount();
   _EndHandleCount = _CommonData._TimeLine->getHandleCount();

   Max(TotalFrames() - 1);
   Min(0);

   TrackBar->LeftHandles  = _StartHandleCount;
   TrackBar->RightHandles = _EndHandleCount;

   RedrawDisplay();
}

// --------------------------------------------------------------------------

void TFMasterTimeLine::SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface)
{
   if (_CommonData._TimeLine == NULL)
   {
      return;
   }

   _CommonData._TimeLine->SetAnimateTimelineInterface(newInterface);
}

// --------------------BuildTimeLines---------------John Mertus----Apr 2003----

void TFMasterTimeLine::BuildTimeLines(const CTimeLineDefsList &TimeLineList)

// This creates the timelines driven by the list of timelines
// CTList.  Each CTList is a vector of STLDefs.
//
// *************************************************************************** /
{
   // Go through and remove all timelines
   for (int i = ComponentCount - 1; i >= 0; i--)
   {
      if (Components[i]->InheritsFrom(__classid(TFBaseTimeLine)))
      {
         TFBaseTimeLine *btl = (TFBaseTimeLine*)Components[i];
         btl->Parent         = NULL;
         RemoveComponent(btl);
      }
   }

   // Build them
   for (unsigned i = 0; i < TimeLineList.size(); i++)
   {
      TimeLineList[i]->Realize();
      TFBaseTimeLine *btl = TimeLineList[i]->TimeLine;
      btl->CommonData(&_CommonData);
      btl->Left = 0;
      // Set height if adding an audio track
      if (TimeLineList[i]->EventType == tlTYPE_AUDIO)
      {
         btl->Height = TIMELINE_HEIGHT_AUDIO_TRACK;
      }

      // As the autoscroll bar comes into effect, the width of
      // Track 0 might actually change, so this is NOT equal
      // to assiging all the GroupTimeLineWidth
      if (i == 0)
      {
         btl->Width = GroupTimeLine->Width;
      }
      else
      {
         btl->Width = TimeLineList[0]->TimeLine->Width;
      }

      btl->Hint = TimeLineList[i]->Name.c_str();
      btl->Parent = GroupTimeLine;
		btl->OnMenuEventChange(_OnMenuEventChange);
      InsertComponent(btl);
   }

   ResizeTimeLines();

   // Set callback so callers will know we changed the tracks and they
   // may have to do something.
   if (_OnTrackRebuildChange != NULL)
   {
      _OnTrackRebuildChange(this);
   }
}

// --------------------ResizeTimeLines---------------John Mertus----Apr 2003----

void TFMasterTimeLine::ResizeTimeLines(void)

// This just resizes the current timelines without destroying and rebuildin
// them.
//
// ************************************************************************** */
{
   static bool _ForceFirst = false;

   // Wish I knew a better way
   if (_FirstResize)
   {
      _GroupTimeLineTop = GroupTimeLine->Top;

      // IfTrackBar is above the lines, we don't need extra space
      if (TrackBarAtTop())
      {
         _SpaceGroupToTrackBar = 0;
      }
      else
      {
         _SpaceGroupToTrackBar = TrackBarPanel->Top - GroupTimeLine->Top - GroupTimeLine->Height;
         if (_SpaceGroupToTrackBar < 0)
         {
            _SpaceGroupToTrackBar = 0;
         }
      }

      _BottomSpace = Height - TrackBarPanel->Top - TrackBarPanel->Height;
      if (_BottomSpace < 0)
      {
         _BottomSpace = 0;
      }

      _AutoSize = GroupTimeLine->AutoSize;
      GroupTimeLine->AutoSize = false;
      _FirstResize            = false;
      _ForceFirst             = true;
   }

   TrackBarPanel->Width = Width - 2 * TrackBarPanel->Left;
   GroupTimeLine->Width = Width - GroupTimeLine->Left - GroupTimeLine->Left;

   // Put on all the track bars
   int T = 0;
   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      btl->Top            = T;
      btl->Left           = 0;
      T                   = T + btl->Height;
   }

   if (TrackBarAtTop())
   {
      TrackBarPanel->Top = _GroupTimeLineTop;
      GroupTimeLine->Top = TrackBarPanel->Top + TrackBarPanel->Height + _SpaceGroupToTrackBar;
   }
   else
   {
      GroupTimeLine->Top = _GroupTimeLineTop;
      TrackBarPanel->Top = GroupTimeLine->Top + T + _SpaceGroupToTrackBar;
   }

   // Autosize means just autosize the vertical and let the hor take care of itself.
   _AutoSize = true;
   if (_AutoSize)
   {
	  Height                       = T + TrackBarPanel->Height + _GroupTimeLineTop  + _SpaceGroupToTrackBar - 12;
	  GroupTimeLine->Height        = T + 12;
	  masterTimeLineRequiredHeight = Height;
	  groupTimeLineRequiredHeight  = GroupTimeLine->Height;
   }
   else
   {
      groupTimeLineRequiredHeight  = T;
	  masterTimeLineRequiredHeight = T + TrackBarPanel->Height + _GroupTimeLineTop + _BottomSpace + _SpaceGroupToTrackBar;
      if (masterTimeLineRequiredHeight < groupTimeLineRequiredHeight)
      {
         masterTimeLineRequiredHeight = groupTimeLineRequiredHeight;
      }
      if (_ForceFirst)
      {
         GroupTimeLine->Height = T;
         _ForceFirst           = false;
      }
   }
}

// This used to be called only when the track bar thumb was moved.
// Now it maky be called by timeline update, and the callback
// inhibit needs to be tested. MBraca-050712
void __fastcall TFMasterTimeLine::TrackBarChange(TObject *Sender)
{
   // was 0.5    changed because we want no gray at start
   ThumbPositionInternal(floor(TrackBar->PositionOfThumb()) + 0.0);

   if (_TimelineChanged != NULL)
   {
      (_TimelineChanged)(this, _CommonData.MidFrame());
   }

   if ((_OnChange != NULL) && (_InhibitCallback == 0))
   {
      (_OnChange)(this);
   }

   SendNotificationIfVisibleFrameRangeChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TFMasterTimeLine::TrackBarCursorChange(TObject *Sender, int Value)
{
   TrackBarChange(Sender);
   if (_Scrolling)
   {
      CursorPosition(CursorPosition());
   }

   UpdateDisplay();
}
// ---------------------------------------------------------------------------

void __fastcall TFMasterTimeLine::TrackBarRangeChange(TObject *Sender)
{
   Frames(TrackBar->Range());
   ResizeTrackBar->ThumbPosition = ResizeTrackBarWarpVtoP(TrackBar->Range());
   SendNotificationIfVisibleFrameRangeChanged();
}
// ---------------------------------------------------------------------------

void TFMasterTimeLine::SendNotificationIfVisibleFrameRangeChanged()
{
   int startFrame = (int)StartFrame();
   int stopFrame = (int)StopFrame();
   if (_OldStartFrame == startFrame && _OldStopFrame == stopFrame)
   {
      return;
   }

   _OldStartFrame = startFrame;
   _OldStopFrame  = stopFrame;

   VisibleFrameRangeHasChanged.Notify();
}
// ---------------------------------------------------------------------------

void TFMasterTimeLine::GetVisibleFrameIndexRange(int &inIndex, int &exclusiveOutIndex)
{
   inIndex = (int)StartFrame();
   exclusiveOutIndex = (int)StopFrame();
}

// ---------------------------------------------------------------------------

// --------------------CTimeLineDefs---------------John Mertus----Apr 2003----

CTimeLineDefs::CTimeLineDefs(TIMELINE_TYPE et, long d)

// This specialized factory constructor creates a timeline of type et with data d
//
// ****************************************************************************
{
   MTIostringstream os; // Encode the data as a string
   os << d + 1;

   EventType = et;
   Data      = d;
   TimeLine  = NULL;

   switch (et)
   {
   case tlTYPE_AUDIO:
      Name = "Channel " + os.str();
      break;

   case tlTYPE_CUT:
      Name = "LTC breaks";
      break;

   case tlTYPE_SCENE_CUT:

      Name = "Scene breaks";
      break;

   case tlTYPE_CADENCE:
      Name = "Cadence";
      break;

   case tlTYPE_MARK:
      Name = "Marks";
      break;

   case tlTYPE_PANDS:
      Name = "Pan&Scan events";
      break;

   case tlTYPE_LOGGER_PICTURE:
      Name = "Picture events";
      break;

   case tlTYPE_LOGGER_AUDIO:
      Name = "Audio events";
      break;

   case tlTYPE_REF:
      Name = "Reference";
      break;

   case tlTYPE_ANIMATE:
      Name = "Mask Animation";
      break;

   default:
      throw(this);
   }
}

// --------------------CTimeLineDefs---------------John Mertus----Apr 2003----

CTimeLineDefs *CTimeLineDefs::Realize(void)

// This takes the template that is defined when the CTimeLineDefs is first
// defined and creates a full class out of it.
// If it already has been realized, nothing is done.
// NOTE: This will be changed in the future!
//
// ****************************************************************************
{
   MTIostringstream os; // Encode the data as a string

   if (TimeLine != NULL)
   {
      return this;
   } // Already realized

   // This is REALLY stupid, we should just have a list of popup menus and
   // attach those to the timeline.
   TFEventTimeLine *eventTimeLine;

   switch (EventType)
   {
   case tlTYPE_AUDIO:
      TimeLine = new TFAudioTimeLine(NULL);
      os << "AudioTimeLine" << Data;
      TimeLine->Name    = os.str().c_str();
      TimeLine->Channel = Data;
      GlobalImageList->GetBitmap(5 + Data, TimeLine->SelectButton->Glyph);
      TimeLine->AddEventMenuItem->Visible      = false;
      TimeLine->DropEventMenuItem->Visible     = false;
      TimeLine->NextEventMenuItem->Visible     = false;
      TimeLine->PreviousEventMenuItem->Visible = false;
      TimeLine->FrameCacheMenuItem->Visible    = false;
      TimeLine->TicksMenuItem->Visible         = false;

      break;

   case tlTYPE_CUT:
      TimeLine = new TFEventTimeLine(NULL);
      os << "CutTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_AUD_MASK;
      TimeLine->DefaultEvent = EVENT_AUD_BOUNDARY;

      GlobalImageList->GetBitmap(0, TimeLine->SelectButton->Glyph);

      break;

   case tlTYPE_SCENE_CUT:
      TimeLine = new TFEventTimeLine(NULL);
      os << "SceneTimeLine" << Data;
      TimeLine->Name                                = os.str().c_str();
      TimeLine->EventMask                           = EVENT_CUT_MASK;
      TimeLine->DefaultEvent                        = EVENT_CUT_ADD;
      TimeLine->AddEventMenuItem->Caption           = "&Add Cut";
      TimeLine->DropEventMenuItem->Caption          = "&Drop Cut";
      TimeLine->NextEventMenuItem->Caption          = "&Next Cut";
      TimeLine->PreviousEventMenuItem->Caption      = "&Previous Cut";
      TimeLine->ClearAllMarksMenuItem->Visible      = true;
      TimeLine->ImportCutListMenuItem->Visible      = true;
      TimeLine->ExportCutListSubMenu->Visible       = true;
      TimeLine->ImportEDLMenuItem->Visible          = true;
      TimeLine->N2->Visible                         = true; // separator
      TimeLine->RescanforSceneCutsMenuItem->Visible = true;
      TimeLine->N3->Visible                         = true; // separator
      TimeLine->ShowTicks(true);
      TimeLine->ShowFrameCache(true);
      TimeLine->N4->Visible = true; // separator

      eventTimeLine = (TFEventTimeLine *)TimeLine;

      GlobalImageList->GetBitmap(0, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_CADENCE:
      TimeLine = new TFEventTimeLine(NULL);
      os << "CadenceTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_CADENCE_MASK;
      TimeLine->DefaultEvent = EVENT_CADENCE_BREAK;
      GlobalImageList->GetBitmap(3, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_MARK:
      TimeLine = new TBookmarkTimeLine(NULL);
      os << "MarksTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_MARK_MASK;
		TimeLine->DefaultEvent = EVENT_BOOKMARK;
      GlobalImageList->GetBitmap(1, TimeLine->SelectButton->Glyph);
		TimeLine->AddEventMenuItem->Caption              = "&Add Bookmark";
      TimeLine->DropEventMenuItem->Caption             = "&Drop Bookmark";
      TimeLine->ClearAllMarksMenuItem->Caption         = "&Clear All Bookmarks";
		TimeLine->NextEventMenuItem->Caption             = "&Next Bookmark";
      TimeLine->PreviousEventMenuItem->Caption         = "&Previous Bookmark";
      TimeLine->ClearAllMarksMenuItem->Visible         = true;
      TimeLine->ImportBookmarkEventsMenuItem->Visible  = true;
      TimeLine->ExportBookmarkEventsMenuItem->Visible  = true;
      TimeLine->BrowseToReportsFolderMenuItem->Visible = true;

     // Turn off color break cuts (programing like kurt and loving it)
      eventTimeLine = (TFEventTimeLine *)TimeLine;

      TimeLine->N2->Visible = true; // separator
      break;

   case tlTYPE_PANDS:
      TimeLine = new TFEventTimeLine(NULL);
      os << "PandsTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_PANDS_MASK;
      TimeLine->MFEventMask  = MF_EVENT_LOG_MASK;
      TimeLine->DefaultEvent = EVENT_PANDS_STATIC;
      GlobalImageList->GetBitmap(4, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_LOGGER_PICTURE:
      TimeLine = new TFEventTimeLine(NULL);
      os << "LoggerPictureTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_PIC_MASK;
      TimeLine->MFEventMask  = MF_EVENT_LOG_PST;
      TimeLine->DefaultEvent = MF_EVENT_LOG_PST;
      GlobalImageList->GetBitmap(4, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_LOGGER_AUDIO:
      TimeLine = new TFEventTimeLine(NULL);
      os << "LoggerAudioTimeLine" << Data;
      TimeLine->Name         = os.str().c_str();
      TimeLine->EventMask    = EVENT_AUD_MASK;
      TimeLine->MFEventMask  = MF_EVENT_LOG_AST;
      TimeLine->DefaultEvent = MF_EVENT_LOG_AST;
      GlobalImageList->GetBitmap(2, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_REF:
      TimeLine = new TFRefTimeLine(NULL);
      os << "RefTimeLine" << Data;
      TimeLine->Name = os.str().c_str();
      // Only the "Hide" menu item is visible
      TimeLine->AddEventMenuItem->Visible      = false;
      TimeLine->DropEventMenuItem->Visible     = false;
      TimeLine->NextEventMenuItem->Visible     = false;
      TimeLine->PreviousEventMenuItem->Visible = false;
      TimeLine->N1->Visible                    = false;
      TimeLine->FrameCacheMenuItem->Visible    = false;
      TimeLine->TicksMenuItem->Visible         = false;
      TimeLine->N4->Visible                    = false; // separator

      GlobalImageList->GetBitmap(15, TimeLine->SelectButton->Glyph);
      break;

   case tlTYPE_ANIMATE:
      TimeLine = new TFAnimateTimeLine(NULL);
      os << "AnimateTimeLine" << Data;
      TimeLine->Name                           = os.str().c_str();
		TimeLine->EventMask    						  = EVENT_KEYFRAME_MASK;
		TimeLine->DefaultEvent 						  = EVENT_KEYFRAME;
		TimeLine->AddEventMenuItem->Visible      = true;
      TimeLine->DropEventMenuItem->Visible     = true;
      TimeLine->NextEventMenuItem->Visible     = true;
      TimeLine->PreviousEventMenuItem->Visible = true;
      TimeLine->ClearAllMarksMenuItem->Visible = true;
      TimeLine->N1->Visible                    = false;
      TimeLine->AddEventMenuItem->Caption      = "&Add Keyframe";
      TimeLine->DropEventMenuItem->Caption     = "&Drop Keyframe";
      TimeLine->NextEventMenuItem->Caption     = "&Next Keyframe";
      TimeLine->PreviousEventMenuItem->Caption = "&Previous Keyframe";
      TimeLine->ClearAllMarksMenuItem->Caption = "&Clear All Keyframes";

      GlobalImageList->GetBitmap(13, TimeLine->SelectButton->Glyph);
      break;

   default:
      throw(this);
   }

   return this;
}

// ------------------------Move--------------------John Mertus----Apr 2003----

bool CTimeLineDefsList::Move(TFBaseTimeLine *btl, CTimeLineDefsList &tlList, int Pos)

// Moves a timeline associated with the BASE timeline btl from the tlList.
// Pos is where to move it before in the list, 0 is first position, -1 is after all.
// If btl does not map to a tl in tlList, the return is false.
//
// ****************************************************************************
{
   return Move(tlList.FindTimeLineDefs(btl), tlList, Pos);
}

// ------------------------Move--------------------John Mertus----Apr 2003----

bool CTimeLineDefsList::Move(CTimeLineDefs *tl, CTimeLineDefsList &tlList, int Pos)

// Moves a timeline tl from tlList to this.
// Pos is where to move it before in the list, 0 is first position, -1 is after all.
// If tl does not exist in tlList, the return is false.
//
// ****************************************************************************
{
   CTimeLineDefsList::iterator iter = find(tlList.begin(), tlList.end(), tl);
   if (iter == tlList.end())
   {
      return false;
   }

   // Erase it from the list
   tlList.erase(iter);
   if (Pos < 0)
   {
      insert(end(), tl);
   }
   else
   {
      insert(begin() + Pos, tl);
   }

   return true;
}

// ------------------------MoveAllFrom--------------John Mertus----Apr 2003----

void CTimeLineDefsList::MoveAllFrom(CTimeLineDefsList &tlList)

// Moves all the tlList into the current timeline
//
// ****************************************************************************
{
   for (unsigned i = 0; i < tlList.size(); i++)
   {
      insert(end(), tlList[i]);
   }

   tlList.clear();

}

// ------------------------Move--------------------John Mertus----Apr 2003----

bool CTimeLineDefsList::Move(TIMELINE_TYPE et, int d, CTimeLineDefsList &tlList, int Pos)

// Moves a timeline tl from tlList to this.
// Pos is where to move it before in the list, 0 is first position, -1 is after all.
// If tl does not exist in tlList, the return is false.
//
// ****************************************************************************
{
   return Move(tlList.FindTimeLineDefs(et, d), tlList, Pos);
}

// -------------------FindTimeLineDefs--------------John Mertus----Apr 2003----

CTimeLineDefs *CTimeLineDefsList::FindTimeLineDefs(TIMELINE_TYPE et, int d)

// This finds the time line associatged with event et and data d.
// If not found, the return is NULL
//
// ****************************************************************************
{
   for (unsigned i = 0; i < size(); i++)
   {
      CTimeLineDefs *tl = at(i);
      if ((tl->Data == d) && (tl->EventType == et))
      {
         return tl;
      }
   }

   return NULL;
}

// -------------------FindTimeLineDefs--------------John Mertus----Apr 2003----

CTimeLineDefs *CTimeLineDefsList::FindTimeLineDefs(const string &strName)

// This finds the time line associatged with name
// If not found, the return is NULL
//
// ****************************************************************************
{
   for (unsigned i = 0; i < size(); i++)
   {
      CTimeLineDefs *tl = at(i);
      if (tl->Name == strName)
      {
         return tl;
      }
   }

   return NULL;
}

// -------------------FindTimeLineDefs--------------John Mertus----Apr 2003----

CTimeLineDefs *CTimeLineDefsList::FindTimeLineDefs(TFBaseTimeLine *btl)

// This finds the time line associatged with the base time line btl
// If not found, the return is NULL
//
// ****************************************************************************
{
   if (btl == NULL)
   {
      return NULL;
   }

   for (unsigned i = 0; i < size(); i++)
   {
      CTimeLineDefs *tl = at(i);
      if (tl->TimeLine == btl)
      {
         return tl;
      }
   }

   return NULL;
}

// TEMPORARY
void __fastcall TFMasterTimeLine::WMShow(TMessage &Message)
{
   TFrame::Dispatch(&Message);
}

void __fastcall TFMasterTimeLine::FrameResize(TObject *Sender)
{
   // The first time, which always happens at startup, this builds the display
   if (InitIt)
   {
      InitIt = false;
      ReadMenuItemClick(NULL);
      // Save the design time lines
      DesignTimeLines = CurrentTimeLines;
   }
}

// -------------------TimeLinePopupPopup--------------John Mertus----Apr 2003----

void __fastcall TFMasterTimeLine::TimeLinePopupPopup(TObject *Sender)

// When the configuration menu is popped up, this looks at the CurrentTimeLines
// and sets the "Checked" property of the menu to those that exist in the CurrentTimeLine
//
// ****************************************************************************
{
   UpdateMenus();
}

// -------------------UpdateMenus--------------Mike Braca----Oct 2004----

void TFMasterTimeLine::UpdateMenus()

// This looks at the CurrentTimeLines and sets the "Checked" property of
// the menu to those that exist in the CurrentTimeLine
//
// ****************************************************************************
{
   // Save these default values so they can be restored if needed.
   static AnsiString handlesMenuItemCaptionOrig = HandlesMenuItem->Caption;
   static bool handlesMenuItemEnabledOrig       = HandlesMenuItem->Enabled;
   static bool handlesMenuItemVisibleOrig       = HandlesMenuItem->Visible;

   // Special case for Image Ingest Tool which should not show Popup with
   // Audio options on it.  This is a bit of a hack because we use
   // the HandlesMenuItem to show the text we want since Handles are
   // not used within Control Dailies.
   if (GetShowEmptyMenu())
   {
      HandlesMenuItem->Caption  = " Timeline popup menu items are not relevant" " for this tool";
      HandlesMenuItem->Enabled  = false;
      HandlesMenuItem->Visible  = true;
      ShowHideMenuItem->Visible = false;
      return;
   }
   else
   {
      HandlesMenuItem->Caption  = handlesMenuItemCaptionOrig;
      HandlesMenuItem->Enabled  = handlesMenuItemEnabledOrig;
      HandlesMenuItem->Visible  = handlesMenuItemVisibleOrig;
      ShowHideMenuItem->Visible = true; // Default
   }

   // Loop over all items in the top-level popup menu
   for (int i = 0; i < ShowHideMenuItem->Count; i++)
   {
      TMenuItem *mi     = ShowHideMenuItem->Items[i];
      TIMELINE_TYPE tag = (TIMELINE_TYPE) mi->Tag;
      int subTag        = 0;

      // Some of the menu items, like Audio, have submenus
      if (mi->Count > 0)
      {
         bool bSubmenuItemidChecked  = false;
         int iNumSubmenuItemsChecked = 0;

         // Loop over submenu items
         for (int j = 0; j < mi->Count; j++)
         {
            TMenuItem *msi = mi->Items[j];

            if ((TIMELINE_TYPE)mi->Tag == tlTYPE_AUDIO)
            {
               // For audio, the index of the submenu item will be
               // the sub-tag (also referred to as the "data"). To
               // compute the corresponding audio channel, add one
               // to the index.
               subTag = j;
            }
            else
            {
               // For non-audio, the tag comes from the submenu
               // item, and the sub-tag is 0.
               tag = (TIMELINE_TYPE) msi->Tag;
            }

            // The submenu item will be checked if we find it in the
            // "current" timeline list
            msi->Checked = (CurrentTimeLines.FindTimeLineDefs(tag, subTag) != NULL);
            // Update aggregate flag so we know whether or not to
            // try to check the parent menu item if any of the submenu
            // items are checked
            bSubmenuItemidChecked |= msi->Checked;

            // Count the number of subitems checked
            iNumSubmenuItemsChecked += msi->Checked;

         } // end loop over submenu items

         // Update the parent checked state, in case it is checkable
         mi->Checked = bSubmenuItemidChecked;

         // Update the Show All / Hide Menu Items for Audio
         if ((TIMELINE_TYPE)mi->Tag == tlTYPE_AUDIO)
         {
            if (iNumSubmenuItemsChecked == 0)
            { // none checked
               HideAudioTracks1->Enabled = false;
               ShowAllAudio1->Enabled    = true;
            }
            else if (iNumSubmenuItemsChecked == mi->Count)
            { // all checked
               HideAudioTracks1->Enabled = true;
               ShowAllAudio1->Enabled    = false;
            }
            else
            { // some checked
               HideAudioTracks1->Enabled = true;
               ShowAllAudio1->Enabled    = true;
            }
         }
      }
      else
      {
         // No submenu, no problem
         mi->Checked = (CurrentTimeLines.FindTimeLineDefs(tag) != NULL);
      }
   } // end loop over top-level menu items

   // Now do the Handles
   HandlesMenuItem->Checked = AllowHandles();
   TrackBar->ShowHandles = AllowHandles();

}

// -------------------ReadMenuItemClick-------------John Mertus----Apr 2003----

void __fastcall TFMasterTimeLine::ReadMenuItemClick(TObject *Sender)

// This is the reverse of TimeLinePopupPopup.
// This reads the popup menu and creates the master time line according to
// the menu items that have been checked.
//
// ****************************************************************************
{

   // Move all back to the Reserved
   while (CurrentTimeLines.size() > 0)
   {
      ReservedTimeLines.Move(CurrentTimeLines[0], CurrentTimeLines);
   }

   // There MUST be a better way!!!!
   for (int i = 0; i < ShowHideMenuItem->Count; i++)
   {
      // Check for the submenu
      TMenuItem *mi = ShowHideMenuItem->Items[i];
      // Audio is different
      if (mi->Count > 0)
      {
         if (mi->Tag == tlTYPE_AUDIO)
         {
            for (int j = 0; j < mi->Count; j++)
            {
               TMenuItem *msi = mi->Items[j];
               if (msi->Checked)
               {
                  CurrentTimeLines.Move((TIMELINE_TYPE)mi->Tag, msi->Tag, ReservedTimeLines);
               }
               else
               {
                  ReservedTimeLines.Move((TIMELINE_TYPE)mi->Tag, msi->Tag, CurrentTimeLines);
               }
            }
         }
         else
         {
            for (int j = 0; j < mi->Count; j++)
            {
               TMenuItem *msi = mi->Items[j];
               if (msi->Checked)
               {
                  CurrentTimeLines.Move((TIMELINE_TYPE)msi->Tag, 0, ReservedTimeLines);
               }
               else
               {
                  ReservedTimeLines.Move((TIMELINE_TYPE)msi->Tag, 0, CurrentTimeLines);
               }
            }
         }
      }
      else if (mi->Tag > 0)
      {
         if (mi->Checked)
         {
            CurrentTimeLines.Move((TIMELINE_TYPE)mi->Tag, 0, ReservedTimeLines);
         }
         else
         {
            ReservedTimeLines.Move((TIMELINE_TYPE)mi->Tag, 0, CurrentTimeLines);
         }
      }
   }

   BuildTimeLines(CurrentTimeLines);

}

// ----------------------AudioSlip------------------John Mertus----Apr 2003----

double TFMasterTimeLine::AudioSlip(double Value)

// This changes the slip of all the audio channels by Value Frames
//
// ****************************************************************************
{
   for (int i = 0; i < MAX_AUDIO_CHANNELS; i++)
   {
      CTimeLineDefs *ctd = CurrentTimeLines.FindTimeLineDefs(tlTYPE_AUDIO, i);
      if (ctd != NULL)
      {
         ctd->TimeLine->OffsetFrame(Value);
      }
   }

   return AudioSlip();
}

// ------------------AudioSlipForceCB---------------John Mertus----Apr 2003----

double TFMasterTimeLine::AudioSlipForceCB(double Value)

// This changes the slip of all the audio channels by Value Frames
// and then a callback is done
//
// ****************************************************************************
{
   if (Value == AudioSlip())
   {
      return AudioSlip();
   }
   AudioSlip(Value);
   if (_AudioSlipChanged != NULL)
   {
      _AudioSlipChanged(this, Value);
   }
   return AudioSlip();
}

// ----------------------AudioSlip------------------John Mertus----Apr 2003----

double TFMasterTimeLine::AudioSlip(void)

// Return the current audio slip
//
// ****************************************************************************
{
   CTimeLineDefs *ctd = CurrentTimeLines.FindTimeLineDefs(tlTYPE_AUDIO, 0);
   if (ctd != NULL)
   {
      return ctd->TimeLine->OffsetFrame();
   }
   else
   {
      return 0;
   }
}
// ---------------------------------------------------------------------------

void TFMasterTimeLine::SetChangeEvent(TNotifyEvent Callback)
{
   _OnChange = Callback;
}

// ------------------GetChangeEvent----------------John Mertus----May 2003-----

TNotifyEvent TFMasterTimeLine::GetChangeEvent(void)

// Jus treturn the change event function
//
// ****************************************************************************
{
   return _OnChange;
}

// ---------------------------------------------------------------------------

// ------------------SetAudioSlipChangeEvent----------------John Mertus----May 2003-----

void TFMasterTimeLine::SetAudioSlipChangeEvent(TTimelineChanged Callback)
{
   AudioSlipChanged = Callback;
}

// ------------------GetAudioSlipChangeEvent----------------John Mertus----May 2003-----

TTimelineChanged TFMasterTimeLine::GetAudioSlipChangeEvent(void)

// Jus treturn the change event function
//
// ****************************************************************************
{
   return AudioSlipChanged;
}

// ------------------TrackBarAtTop-----------------John Mertus----Nov 2003-----

bool TFMasterTimeLine::TrackBarAtTop(bool Value)

// Sets the current value of the track bar position
//
// ****************************************************************************
{
   if (Value == TrackBarAtTop())
   {
      return TrackBarAtTop();
   }
   _TrackBarAtTop = Value;

   ResizeTimeLines();
   return TrackBarAtTop();
}

// ------------------TrackBarAtTop-----------------John Mertus----Nov 2003-----

bool TFMasterTimeLine::TrackBarAtTop(void)

// Returns the current value of the track bar position
//
// ****************************************************************************
{
   return _TrackBarAtTop;
}

// ----------------------Max-----------------------John Mertus----May 2003-----

double TFMasterTimeLine::Max(void)

// Return the maximum setting.  The Max setting must be >= the Min
//
// ****************************************************************************
{
   return TrackBar->Max();
}

// ----------------------Min-----------------------John Mertus----May 2003-----

double TFMasterTimeLine::Min(void)

// Return the minimum setting.  The Max setting must be >= the Min
//
// ****************************************************************************
{
   return TrackBar->Min();
}

// ----------------------Max-----------------------John Mertus----May 2003-----

void TFMasterTimeLine::Max(double Value)

// Set the maximum, if the min is greater then this maximum, Min is set
// to the Max.  The range may be also adjusted
//
// ****************************************************************************
{
   if (AllowHandles())
   {
      if (Value < 0)
      {
         Value = 0;
      }
      if (Value > (TotalFrames() - 1))
      {
         Value = TotalFrames() - 1;
      }
   }
   else
   {
      if (Value < _StartHandleCount)
      {
         Value = _StartHandleCount;
      }
      if (Value > (TotalFrames() - 1 - _EndHandleCount))
      {
         Value = TotalFrames() - 1 - _EndHandleCount;
      }
   }
   TrackBar->Max(Value);
   _CommonData.Max(Value);
}

// ----------------------Min-----------------------John Mertus----May 2003-----

void TFMasterTimeLine::Min(double Value)

// Set the minimum, if the Max is greater then this mimimum, the Max is
// to the Min.
//
// ****************************************************************************
{
   if (AllowHandles())
   {
      if (Value < 0)
      {
         Value = 0;
      }
      if (Value > (TotalFrames() - 1))
      {
         Value = TotalFrames() - 1;
      }
   }
   else
   {
      if (Value < _StartHandleCount)
      {
         Value = _StartHandleCount;
      }
      if (Value > (TotalFrames() - 1 - _EndHandleCount))
      {
         Value = TotalFrames() - 1 - _EndHandleCount;
      }
   }
   TrackBar->Min(Value);
   _CommonData.Min(Value);
}

// --------------------Scrolling-------------------John Mertus----May 2003-----

void TFMasterTimeLine::Scrolling(bool Value)

//
// ****************************************************************************
{
   if (_Scrolling == Value)
   {
      return;
   }
   _Scrolling = Value;

   // For a jump into the thumb by this call
   CursorPosition(CursorPosition());
}

// --------------------AllowHandles----------------John Mertus----May 2003-----

void TFMasterTimeLine::AllowHandles(bool Value)

// This changes if the handles will be displayed or not.
// If allowed, min and max can go to [0,TotalFrames)
// if not allowed, min and max go to [StartCount, TotalFrames-EndHandleCount)
//
// ****************************************************************************
{
   if (Value == _AllowHandles)
   {
      return;
   }
   _AllowHandles = Value;
   Max(TotalFrames() - 1);
   Min(0);
   TrackBar->ShowHandles = Value;
}

// --------------------AllowHandles----------------John Mertus----May 2003-----

bool TFMasterTimeLine::AllowHandles(void)

// This returns the value of AllowHandles
// If allowed, min and max can go to [0,TotalFrames)
// if not allowed, min and max go to [StartCount, TotalFrames-EndHandleCount)
//
// ****************************************************************************
{
   return _AllowHandles;
}

void __fastcall TFMasterTimeLine::HandlesMenuItemClick(TObject *Sender)
{
   AllowHandles(!AllowHandles());

}

vector<double>TFMasterTimeLine::DebugIndividualTimes(void)
{
   vector<double>Result;
   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      Result.push_back(CurrentTimeLines[i]->TimeLine->BuildBitmapTime);
      Result.push_back(CurrentTimeLines[i]->TimeLine->BlitBitmapTime);
      Result.push_back(CurrentTimeLines[i]->TimeLine->PaintTime);
      Result.push_back(CurrentTimeLines[i]->TimeLine->Builds);
      Result.push_back(CurrentTimeLines[i]->TimeLine->Paints);
   }
   return Result;
}

void __fastcall TFMasterTimeLine::FrameMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
   Screen->Cursor = crDefault;
}
// ---------------------------------------------------------------------------

// -------------------ReadSAP-------------------------John Mertus-----Jan 2002---

bool TFMasterTimeLine::ReadSAP(CIniFile *ini, const string &IniSection)

// This reads the Size and Positions properties from the ini file
// ini is the ini file
// IniSection is the IniSection
//
// Return should be true
//
// Read the individual sap
//
// ******************************************************************************
{
   bool Result = true;
   _CommonData.InhibitCallbacks();

   // remove all current time lines
   ReservedTimeLines.MoveAllFrom(CurrentTimeLines);

   // Factory for producing the lines
   StringList TimeLineNames;
   ini->ReadStringList(IniSection, "ActiveLines", &TimeLineNames);
   for (unsigned i = 0; i < TimeLineNames.size(); i++)
   {
      CTimeLineDefs *tl = ReservedTimeLines.FindTimeLineDefs(TimeLineNames[i]);
      if (tl != NULL)
      {
         tl->Realize(); // Just to be sure they exist
         CurrentTimeLines.Move(tl, ReservedTimeLines);
      }
   }

   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->ReadSAP(ini, IniSection);
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->ReadSAP(ini, IniSection);
      }
   }

   // We know the lines, now restore them
   BuildTimeLines(CurrentTimeLines);

   ResizeTrackBar->Width = ini->ReadInteger(IniSection, "TrackBarSplitter", ResizeTrackBar->Width);

   UpdateMenus();

   _CommonData.UnInhibitCallbacks();
   return Result;
}

// -------------------------WriteSAP-----------------John Mertus-----Jan 2002---

bool TFMasterTimeLine::WriteSAP(CIniFile *ini, const string &IniSection)

// This writes the Size and Positions properties from the ini file
// ini is the ini file
// IniSection is the IniSection
//
// Return should be true
//
// Write all possible timelines, even if hidden
//
// ******************************************************************************
{
   bool Result = true;

   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->WriteSAP(ini, IniSection);
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->WriteSAP(ini, IniSection);
      }
   }

   // This is because we are aligned left
   ini->WriteInteger(IniSection, "TrackBarSplitter", ResizeTrackBar->Width);
   return Result;
}

// ----------------ReadSettings---------------------John Mertus-----Jan 2001---

bool TFMasterTimeLine::ReadSettings(CIniFile *ini, const string &IniSection)

// This reads the Size and Positions properties from the ini file
// ini is the ini file
// IniSection is the IniSection
//
// Return should be true
//
// Note: This function is designed to be overridden in the derived class
//
// ******************************************************************************
{
   bool Result = true;

   // D'oh! Stupid fricken component should not be remembering this stuff!
   bool saveShowThumb   = TrackBar->ShowThumb;
   bool saveShowMarks   = TrackBar->ShowMarks;
   bool saveShowCursors = TrackBar->ShowCursors;

   TrackBar->ReadSettings(ini, IniSection);

   // put back stuff that shouldn't be saved in ini file
   TrackBar->ShowThumb   = saveShowThumb;
   TrackBar->ShowMarks   = saveShowMarks;
   TrackBar->ShowCursors = saveShowCursors;

   _CommonData.InhibitCallbacks();
   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->ReadSettings(ini, IniSection);
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->ReadSettings(ini, IniSection);
      }
   }

   // We know all the time lines, now display them
   _CommonData.UnInhibitCallbacks();

   return Result;
}

// ----------------WriteSettings----------------John Mertus-----Jan 2001---

bool TFMasterTimeLine::WriteSettings(CIniFile *ini, const string &IniSection)

// This writes the Size and Positions properties from the ini file
// ini is the ini file
// IniSection is the IniSection
//
// Return should be true
//
// ******************************************************************************
{
   bool Result = true;
   StringList CurrentTimeLineEvents;
   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->WriteSettings(ini, IniSection);
      CurrentTimeLineEvents.push_back(CurrentTimeLines[i]->Name);
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->WriteSettings(ini, IniSection);
      }
   }

   ini->WriteStringList(IniSection, "ActiveLines", &CurrentTimeLineEvents);

   // Write the trackbar settings
   TrackBar->WriteSettings(ini, IniSection);

   return Result;
}
// ---------------ReadDesignSettings---------------John Mertus----Sep 2002-----

bool TFMasterTimeLine::ReadDesignSettings(void)

// This restorese the settings to their original values.
//
// ****************************************************************************
{
   bool Result = true;

   _CommonData.InhibitCallbacks();
   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->ReadDesignSettings();
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->ReadDesignSettings();
      }
   }
   _CommonData.UnInhibitCallbacks();

   return Result;

}
// -----------------ReadDesignSAP------------------John Mertus----Sep 2002-----

bool TFMasterTimeLine::ReadDesignSAP(void)

// This restorese the size and position settings to their original values.
//
// ****************************************************************************
{
   bool Result = true;

   _CommonData.InhibitCallbacks();

   // Restore the time lines
   ReservedTimeLines.MoveAllFrom(CurrentTimeLines);
   for (unsigned i = 0; i < DesignTimeLines.size(); i++)
   {
      CurrentTimeLines.Move(DesignTimeLines[i], ReservedTimeLines);
   }

   for (unsigned i = 0; i < CurrentTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = CurrentTimeLines[i]->TimeLine;
      Result              = Result && btl->ReadDesignSAP();
   }

   for (unsigned i = 0; i < ReservedTimeLines.size(); i++)
   {
      TFBaseTimeLine *btl = ReservedTimeLines[i]->TimeLine;
      if (btl != NULL)
      {
         Result = Result && btl->ReadDesignSAP();
      }
   }

   BuildTimeLines(CurrentTimeLines);
   _CommonData.UnInhibitCallbacks();

   return Result;
}

void __fastcall TFMasterTimeLine::SpecialDrag(TObject *Sender, int Value)
{
   if (_OnScrub != NULL)
   {
      _OnScrub(this, Value);
   }
}

void __fastcall TFMasterTimeLine::TrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
   // WE DON'T WANT THE TIMELINE TO RESPOND TO ANY KEYS!!!!
   Key = 0;
}

bool TFMasterTimeLine::SetClipParameters(const string &strClipNameArg, int iVideoSelect, int iAudioSelect, int iFramingSelect)
{
   bool success = !TimeLine()->setClipName(strClipNameArg, iVideoSelect, iAudioSelect, iFramingSelect);
   TimeLine(_RawTimeLine);
   setResizeTrackBarSoftEnable(success);
   setTrackBarSoftEnable(success);

   if (success)
   {
      if (strClipNameArg != _OldClipName)
      {
         _TrackBarInitTrigger = true;
         _OldClipName         = strClipNameArg;
      }
      else if (iFramingSelect != _OldFramingSelection)
      {
         auto clip = TimeLine()->getClip();
         if (clip != nullptr)
         {
            int iAdjustedFrame = clip->getVideoProxy(iVideoSelect)->translateFrameIndex(TrackBar->PositionOfThumb(),
                                                     _OldFramingSelection,
                                                     iFramingSelect);
            TrackBar->ThumbPosition = iAdjustedFrame;
            TrackBarChange(NULL); // Why doesn't this happen automatically?
            TrackBarRangeChange(NULL); // Why doesn't this happen automatically?
         }
      }

      _OldFramingSelection = iFramingSelect;
   }

   return success;
}

// --------------------TrackStyle------------------John Mertus----May 2003-----

TTrackStyle TFMasterTimeLine::TrackStyle(TTrackStyle Value)

// Property interface into track style
//
// ****************************************************************************
{
   TrackBar->TrackStyle = Value;
   return TrackBar->TrackStyle;
}

// --------------------TrackStyle------------------John Mertus----May 2003-----

TTrackStyle TFMasterTimeLine::TrackStyle(void)

// Property interface into track style
//
// ****************************************************************************
{
   return TrackBar->TrackStyle;
}

// -------------------ThumbStyle------------------John Mertus----May 2003-----

TThumbStyle TFMasterTimeLine::ThumbStyle(TThumbStyle Value)

// Property interface into Thumb style
//
// ****************************************************************************
{
   TrackBar->ThumbStyle = Value;
   return TrackBar->ThumbStyle;
}

// --------------------ThumbStyle------------------John Mertus----May 2003-----

TThumbStyle TFMasterTimeLine::ThumbStyle(void)

// Property interface into Thumb style
//
// ****************************************************************************
{
   return TrackBar->ThumbStyle;
}

// -----------------TrackColor------------------John Mertus----May 2003-----

TColor TFMasterTimeLine::TrackColor(TColor Value)

// Property interface into track color
//
// ****************************************************************************
{
   TrackBar->TrackColor = Value;
   return TrackBar->TrackColor;
}

// -----------------ThumbColor------------------John Mertus----May 2003-----

TColor TFMasterTimeLine::ThumbColor(TColor Value)

// Property interface into Thumb color
//
// ****************************************************************************
{
   TrackBar->ThumbColor = Value;
   return TrackBar->ThumbColor;
}

// -----------------TrackColor------------------John Mertus----May 2003-----

TColor TFMasterTimeLine::TrackColor(void)

// Property interface into track color
//
// ****************************************************************************
{
   return TrackBar->TrackColor;
}

// -----------------ThumbColor------------------John Mertus----May 2003-----

TColor TFMasterTimeLine::ThumbColor(void)

// Property interface into Thumb color
//
// ****************************************************************************
{
   return TrackBar->ThumbColor;
}

// -------------------ShowThumbDetent---------------John Mertus----Jan 2004-----

bool TFMasterTimeLine::ShowThumbDetent(bool Value)

// Property interface into Thumb color
//
// ****************************************************************************
{
   TrackBar->ShowThumbDetent = Value;
   return TrackBar->ShowThumbDetent;
}

// -------------------ShowThumbDetent---------------John Mertus----Jan 2004-----

bool TFMasterTimeLine::ShowThumbDetent(void)

// Property interface into Thumb color
//
// ****************************************************************************
{
   return TrackBar->ShowThumbDetent;
}

// --------------------CursorPosition---------------John Mertus----Feb 2004-----

double TFMasterTimeLine::CursorPosition(int nCur)

// Returns the position of the nCur cursor
//
// ****************************************************************************
{
   return TrackBar->CursorPosition(nCur);
}

// --------------------CursorPosition---------------John Mertus----Feb 2004-----

double TFMasterTimeLine::CursorPosition(void)

// Returns the position of the active cursor
//
// ****************************************************************************
{
   return CursorPosition(TrackBar->ActiveCursor);
}

// --------------------CursorPosition---------------John Mertus----Feb 2004-----

double TFMasterTimeLine::CursorPosition(double Value)

// Sets the default cursor to the value Value
//
// ****************************************************************************
{
   return CursorPosition(Value, TrackBar->ActiveCursor);
}

// --------------------CursorPosition---------------John Mertus----Feb 2004-----

double TFMasterTimeLine::CursorPosition(double Value, int nCur)

// Sets the nCur cursor to the value Value
//
// ****************************************************************************
{
   // if nothing has changed, just return
   if (Value == TrackBar->CursorPosition(nCur))
   { // KMM
      return TrackBar->CursorPosition(nCur);
   }

   TrackBar->CursorPosition(Value, nCur);
   //
   // Move the thumb if we desire
   //
   if (_Scrolling && (TrackBar->ActiveCursor == nCur))
   {
      double Delta = _NonScrollingRange * TrackBar->Range() / 2;
      if (CursorPosition(nCur) < (ThumbPosition() - Delta))
      {
         ThumbPositionInternal(CursorPosition(nCur) + Delta);
      }
      else if (CursorPosition(nCur) >= (ThumbPosition() + Delta))
      {
         ThumbPositionInternal(CursorPosition(nCur) - Delta);
      }
   }
   UpdateDisplay();
   return TrackBar->CursorPosition(nCur);
}

void __fastcall TFMasterTimeLine::ResizeTrackBarChange(TObject *Sender)
{
   Frames(ResizeTrackBarWarpPtoV(ResizeTrackBar->ThumbPosition));
}

// ---------------DrawCenterLine-------------------John Mertus----Feb 2004---

bool TFMasterTimeLine::DrawCenterLine(void)

// Return the number of Display frames
//
// ***************************************************************************
{
   return _CommonData.DrawCenterLine();
}

// -----------------DrawCenterLine------------------John Mertus----Feb 2004---

void TFMasterTimeLine::DrawCenterLine(bool Value)

// Set the numer of display frames and force an update
//
// ***************************************************************************
{
   if (DrawCenterLine() == Value)
   {
      return;
   }
   _CommonData.DrawCenterLine(Value);
   RedrawDisplay();
}

// ---------------------------------------------------------------------------

void TFMasterTimeLine::Select(enum TIMELINE_TYPE iType)
{
   // find this track type
   for (int i = 0; i < NumberOfCurrentTimeLines(); i++)
   {
      if (CurrentTimeLines[i]->EventType == iType)
      {
         _CommonData.ActiveTL(CurrentTimeLine(i));
         return;
      }
   }

   // deselect all
   _CommonData.ActiveTL(NULL);
   return;
}

// ----------------FineDisplayBoundary--------------------John Mertus----Apr 2003---

int TFMasterTimeLine::FineDisplayBoundary(void)

// This returns the audio boundary between fine/course
//
// ***************************************************************************
{
   return _CommonData.FineDisplayBoundary();
}

// ----------FineDisplayBoundary--------------------John Mertus----Apr 2003---

void TFMasterTimeLine::FineDisplayBoundary(int Value)

// This sets the audio boundary between fine/course
//
// ***************************************************************************
{
   _CommonData.FineDisplayBoundary(Value);
}

// --------------------LinearWarpVtoP---------------John Mertus----Feb 2004---

double TFMasterTimeLine::LinearWarpVtoP(double Value)

// This interal routine set the Thumb position of the resize timeline
// in frames.
//
// ***************************************************************************
{
   if (Max() == Min())
   {
      return ResizeTrackBar->Minimum;
   }

   double d = (ResizeTrackBar->Maximum - ResizeTrackBar->Minimum) *
              (1 - (Value - Min()) / (Max() - Min() + 1)) + ResizeTrackBar->Minimum;
   return d;
}

// ------------------LinearWarpPtoV------------------John Mertus----Feb 2004---

double TFMasterTimeLine::LinearWarpPtoV(double Value)

// This internal routine retuns the Thumb position of the resize timeline
// in frames.
//
// ***************************************************************************
{
   if (ResizeTrackBar->Maximum == ResizeTrackBar->Minimum)
   {
      return Min();
   }

   double fp = (Max() - Min() + 1) * (1 - (Value - ResizeTrackBar->Minimum)
         / (ResizeTrackBar->Maximum - ResizeTrackBar->Minimum + 1))
         + Min();

   return fp;
}

// -------------------BiLinearWarpVtoP--------------John Mertus----Feb 2004---

double TFMasterTimeLine::BiLinearWarpVtoP(double Value)

// This interal routine set the Thumb position of the resize timeline
// in frames.  The small (< _ResizePoint) maps two to one
// the large is a linear
//
// ***************************************************************************
{
   double mValue = Value - 0;
   if (mValue <= 2 * _ResizePoint)
   {
      return ResizeTrackBar->Maximum - mValue / 2;
   }

   double mTrackBarMax = ResizeTrackBar->Maximum - _ResizePoint;
   double mValueMin = 0 + 2 * _ResizePoint;

   if (TotalFrames() <= mValueMin)
   {
      return ResizeTrackBar->Minimum;
   }

   double d = (mTrackBarMax - ResizeTrackBar->Minimum) * (1 - (Value - mValueMin)
              / (TotalFrames() - mValueMin)) + ResizeTrackBar->Minimum;
   return d;
}

// -----------------BiLinearWarpPtoV-----------------John Mertus----Feb 2004---

double TFMasterTimeLine::BiLinearWarpPtoV(double Value)

// This internal routine returns the Thumb position of the resize timeline
// in frames.  The close to maximum (withing _ResizePoint) is a one to two
// warping the large is a linear
//
// ***************************************************************************
{
   double mPos = ResizeTrackBar->Maximum - Value;
   if (mPos <= _ResizePoint)
   {
      return 2 * mPos + 0;
   }

   double mTrackBarMax = ResizeTrackBar->Maximum - _ResizePoint;
   double mValueMin = 0 + 2 * _ResizePoint;

   if (ResizeTrackBar->Minimum >= mTrackBarMax)
   {
      return TotalFrames();
   }

   double fp = (TotalFrames() - mValueMin) * (1 - (Value - ResizeTrackBar->Minimum)
                / (mTrackBarMax - ResizeTrackBar->Minimum)) + mValueMin;
   return fp;
}

// ------------ResizeTrackBarWarp------------------John Mertus----Feb 2004---

double TFMasterTimeLine::ResizeTrackBarWarpPtoV(double Value)

// This internal routine warps a position to a frame value
//
// ***************************************************************************
{
   return BiLinearWarpPtoV(Value);
}

// --------------------ResizeTrackBarIWarp-----------John Mertus----Feb 2004---

double TFMasterTimeLine::ResizeTrackBarWarpVtoP(double Value)

// This MUST be the inverse of ResizeTrackBarWarpVtoP, it warps the frame value
// to a position.
//
// ***************************************************************************
{
   return BiLinearWarpVtoP(Value);
}

// -----------------------ResizePoint---------------John Mertus----Mar 2004---

double TFMasterTimeLine::ResizePoint(double Value)

// This sets the point at which the binlinear warping changes
// Value is truncated to be between the ranges of the resize
//
// ***************************************************************************
{
   if (Value < ResizeTrackBar->Minimum)
   {
      Value = ResizeTrackBar->Minimum;
   }
   if (Value > ResizeTrackBar->Maximum)
   {
      Value = ResizeTrackBar->Maximum;
   }
   if (_ResizePoint == Value)
   {
      return _ResizePoint;
   }
   _ResizePoint = Value;
   ResizeTrackBarChange(NULL);
   return _ResizePoint;
}

// ----------------SetOnMenuEventChange-------------John Mertus----Dec 2004---

void TFMasterTimeLine::SetOnMenuEventChange(TMenuEventChange ChangeEvent)

// Set the hook in this and all other timelines
//
// ***************************************************************************
{
	_OnMenuEventChange = ChangeEvent;
   for (int i = 0; i < NumberOfCurrentTimeLines(); i++)
   {
		CurrentTimeLine(i)->OnMenuEventChange(_OnMenuEventChange);
   }
}

// -----------------------ResizePoint---------------John Mertus----Mar 2004---

double TFMasterTimeLine::ResizePoint(void)

// This returns the point at which the binlinear warping changes
//
// ***************************************************************************
{
   return _ResizePoint;
}

// ----------------------GetLine-----------------------Kea
// this gets a reference to a timeline specified off of the current timelines list.
// Its accessible to other classes so other classes can find a specific timeline.
// -----------------------------------------------------

CTimeLineDefs *TFMasterTimeLine::getLine(const string &findme, bool inCurrentTL)
{
   CTimeLineDefs *timeLineDefs = CurrentTimeLines.FindTimeLineDefs(findme);

   if ((timeLineDefs == NULL) && (!inCurrentTL))
   {
      timeLineDefs = ReservedTimeLines.FindTimeLineDefs(findme);
   }

   return timeLineDefs;
}

void __fastcall TFMasterTimeLine::ShowAllAudio1Click(TObject *Sender)
{
   int iNChannelAudio;

   // get pointer to audio track
   CAudioTrack *atp = TimeLine()->getClip()->getAudioTrack(0);

   if (atp)
   {
      CAudioFrameList *aflp = atp->getAudioFrameList(0);
      if (aflp)
      {
         iNChannelAudio = aflp->getAudioFormat()->getChannelCount();
         for (int i = 0; i < iNChannelAudio; i++)
         {
            AudioMenuItem->Items[i]->Checked = true;
         }

         // Set the sizes for the audio channels
         for (unsigned int i = 0; i < ReservedTimeLines.size(); i++)
         {
            if (ReservedTimeLines[i]->EventType == tlTYPE_AUDIO)
            {
               if (ReservedTimeLines[i]->TimeLine == NULL)
               {
                  ReservedTimeLines[i]->Realize();
                  ReservedTimeLines[i]->TimeLine->Height = TIMELINE_HEIGHT_AUDIO_TRACK;
               }
            }
         }

         // Set the tag to the event number
         ReadMenuItemClick(NULL);
      }
   }

}
// ---------------------------------------------------------------------------

void __fastcall TFMasterTimeLine::HideAudioTracks1Click(TObject *Sender)
{
   for (int i = 0; i < MAX_AUDIO_CHANNELS; i++)
   {
      AudioMenuItem->Items[i]->Checked = false;
   }
   ReadMenuItemClick(NULL);

}
// ---------------------------------------------------------------------------

// ----------------GetMasterTimeLineRequiredHeight--Mike Russell---Dec 2006---

int TFMasterTimeLine::GetMasterTimeLineRequiredHeight(void)

// This returns the value of masterTimeLineRequiredHeight.
//
// ***************************************************************************
{
   return masterTimeLineRequiredHeight;
}

// ----------------GetGroupTimeLineRequiredHeight---Mike Russell---Dec 2006---

int TFMasterTimeLine::GetGroupTimeLineRequiredHeight(void)

// This returns the value of groupTimeLineRequiredHeight.
//
// ***************************************************************************
{
   return groupTimeLineRequiredHeight;
}

// ----------------SetGroupTimeLineHeight---Mike Russell---Dec 2006---

void TFMasterTimeLine::SetGroupTimeLineHeight(int height)

// This sets the GroupTimeLine->Height.
//
// ***************************************************************************
{
   GroupTimeLine->Height = height;
}

// ----------------SetTrackBarTop---Mike Russell---Dec 2006---

void TFMasterTimeLine::SetTrackBarTop(int top)

//
// ***************************************************************************
{
   TrackBarPanel->Top = top;
}

bool TFMasterTimeLine::SetShowEmptyMenu(bool b)
{
   showEmptyMenuF = b;
   return b;
}

bool TFMasterTimeLine::GetShowEmptyMenu(void)
{
   return showEmptyMenuF;
}

//////////////////////////////////////
/////// H A C K - O - R A M A  ///////
//////////////////////////////////////
// We can't use the real "Enabled" property for the TTimelineTrackBars
// because there is a bug that we can't track down that causes the timeline
// to not re-enable under certain circumstances. So, we do a fake disable
// by hiding the thumbs and/or cursors

void TFMasterTimeLine::setResizeTrackBarSoftEnable(bool onOff)
{
   if (!onOff && ResizeTrackBar->TrackColor != clBtnFace)
   {
      // Store old track color for later, don't force a change if we
      // are at default color
      _InitialResizeTrackColor = ResizeTrackBar->TrackColor;
   }

   ResizeTrackBar->ShowThumb = onOff;
   ResizeTrackBar->TrackColor = onOff ? _InitialResizeTrackColor : clBtnFace;
}

bool TFMasterTimeLine::getResizeTrackBarSoftEnable()
{
   return ResizeTrackBar->ShowThumb;
}

void TFMasterTimeLine::setTrackBarSoftEnable(bool onOff)
{
   TrackBar->ShowThumb   = onOff;
   TrackBar->ShowCursors = onOff;
   TrackBar->ShowHandles = onOff;
   TrackBar->ShowMarks   = onOff;
}

bool TFMasterTimeLine::getTrackBarSoftEnable()
{
   return TrackBar->ShowThumb;
}
///// END OF  H A C K - O - R A M A //////////
