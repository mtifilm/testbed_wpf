//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AudioTimeLineFrame.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma resource "*.dfm"
TFAudioTimeLine *FAudioTimeLine;
//---------------------------------------------------------------------------
__fastcall TFAudioTimeLine::TFAudioTimeLine(TComponent* Owner)
        : TFBaseTimeLine(Owner)
{
   Channel = 0;
#ifdef NAVIGATOR
   // QQQ Need to figure out a better way to do this!!
   // In Navigator, audio is not so important, so default to shorter
   // timelines. Also color them green for aesthetic effect.
   Height = 29;
   DataDisplay->Color = (TColor) 0xf0f0e0;
#endif // NAVIGATOR
}

//-------------BuildWaveForm-----------------------John Mertus---Feb 2004----

   void BuildWaveForm(TRect Rect, TCanvas *Canvas, MTI_INT16 *Data, int nData, float Scale)

// This builds a waveform on the Canvas Canvas within the Bounds of the
// rectangle Rect.  Array is an array of the data points and N is the number of
// them.  Scale is an additional scale factor.
//
//  The data points are assumed to be between -32768 and 32767, the
// these form the maximum ranges.  Outof bound data, after scaling, is pegged
// at the top or bottom
//
//
//**************************************************************************
{
   if (nData <= 0) return;    // Nothing to do

   int h = Rect.Height();

   // Clear out the rectangle
   TPenStyle psOld = Canvas->Pen->Style;
   Canvas->Pen->Style = psSolid;

   // We start at the left, collect points until the bin is full
   // then plot the bin.
   double fInc = (double)(Rect.Width())/(double)(nData);
   double fScale = Scale*h/65536.0;
   int iOld = 0;
   int idx = 0;
   int mn = Data[0];
   int mx = Data[0];
   for (int i=0; i < nData; i++)
     {
        idx = i*fInc;
        if (idx != iOld)               // We have crossed a border
          {
            // First time through, just move to the start of drawing
            if (iOld != 0)
              Canvas->LineTo(iOld + Rect.Left, h/2 - fScale*mn);
            else
              Canvas->MoveTo(iOld + Rect.Left, h/2 - fScale*mn);

            Canvas->LineTo(iOld + Rect.Left, h/2 - fScale*mx);  // Lines do not contain last point
            mn = Data[i];
            mx = Data[i];
            iOld = idx;
          }
        else
          {
            if (mn > Data[i])
              mn = Data[i];
            if (mx < Data[i])
              mx = Data[i];
          }
     }

   // Flush last buffer
   Canvas->LineTo(iOld + Rect.Left, h/2 - fScale*mn);
   Canvas->LineTo(iOld + Rect.Left, h/2 - fScale*mx);  // Lines do not contain last point

   // LineTo's don't do the last point, add it
   Canvas->Pixels[iOld+Rect.Left][ h/2 - fScale*mx] = Canvas->Pen->Color;

   // Restore old mode
   Canvas->Pen->Style = psOld;

}

   void BuildWaveForm(Graphics::TBitmap *Bitmap, CTimeLine *tl, int StartFrame, int EndFrame, int Channel, float Scale, int FineBoundary)
{
  if (tl == NULL) return;
  int Frames = EndFrame - StartFrame;
  if (Frames <= 1) return;

  int HeightBm = Bitmap->Height;
  int WidthBm = Bitmap->Width;

//  int StartPixel = (WidthBm-1)*(float(StartFrame)/(Frames-1));
//  int LastPixel = (WidthBm-1)*(float(tl->getTotalFrameCount() - .5)/(Frames-1));
  int StartPixel = ((float)WidthBm)*(float(StartFrame)/((float)Frames)) + .5;
  int LastPixel = ((float)WidthBm)*(float(tl->getTotalFrameCount())/((float)Frames)) + .5;

  // Are we at the end?
  int xBackOffset = std::min(LastPixel-StartPixel, WidthBm);

  int xOffset = 0;
  if (StartFrame < 0)
     xOffset = WidthBm*(std::min(abs(StartFrame), Frames))/((float)Frames);
  else
     xOffset = 0;


  // Three major cases, first is if the number of frames are "small"
  if ((Frames <= FineBoundary) || ((FineBoundary < 0) && (Frames < WidthBm/AUDIO_PROXY_PER_FRAME)))
    {
       int sF = StartFrame;
       double eF = sF + Frames;
       // Adjust for beginning
       if (sF < 0)
         {
            sF = 0;
            eF = Frames + StartFrame + 1;
         }

       // Adjust for the end
       if (eF > (tl->getTotalFrameCount()+1)) eF = tl->getTotalFrameCount() + 1;

       int N = tl->getAudioCount (sF, eF);
       double SPerF = ((double)N)/((int)eF - (int)sF);
       MTI_INT16 *Data = new MTI_INT16[N];
       int nRead = tl->getAudio(sF, eF,	&Data, N, 1, &Channel);

       // Frames is not necessarily an integer, adjust for the points
       N = SPerF*Frames;
       if (N > nRead) N = nRead;
       int iBPad = (sF - (int)sF)*SPerF;

       BuildWaveForm(TRect(xOffset, 0, xBackOffset, HeightBm), Bitmap->Canvas, Data+iBPad, N-iBPad, Scale);
       delete [] Data;
       return;
    }



   //  Here is the problem when there are more frames than pixels.
   //  For speed purposes, each pixel receives one frame values.  However
   //  as the current frame changes, that frame may not be displayed and its
   //  neighbor choosen.  This makes the display "jitters" as the display scrolls
   //  slowly.
   //
   //  For this reason, at a given size, once it is decided to display
   //  a given frame, that one will allways be displayed even if its
   //  neighbor is a "better" fit for that current frame.
   //
   //  Thus, the frames to be displayed are computed from zero, then
   //  the display shifted.

   // First check if there are more frames than pixels, if so, just use the maximum
   //
   if (Frames >= WidthBm)
     {
       for (int i = xOffset; i < WidthBm; i++)
         {
            // Find the nearest frame
            int iFrame = (Frames)*(float(i + StartPixel)/(WidthBm))+.5;
            int iFrameNext = (Frames)*(float((i+1) + StartPixel)/(WidthBm))+.5;
            if (iFrame < 0) break;

            // assume audio is invalid, use zero
            int sMin=0, sMax=0;

            for (int iF = iFrame; iF < iFrameNext; iF++)
             {
              if (tl->isAudioValid(Channel, iF))
               {
                TL_AUDIO_PROXY *ap = tl->getAudioProxyPointer(iF);
                if (ap->spMinValue[Channel] < sMin || iF == iFrame)
                 {
                  sMin = ap->spMinValue[Channel];
                 }
                if (ap->spMaxValue[Channel] < sMax || iF == iFrame)
                 {
                  sMax = ap->spMaxValue[Channel];
                 }
               }
             }


            sMin *= Scale;
            sMax *= Scale;

            sMin = std::max(sMin, -32767);
            sMax = std::min(sMax,  32767);

            Bitmap->Canvas->MoveTo(i, HeightBm - (sMin+32767.)/
		65564.0*HeightBm);
            Bitmap->Canvas->LineTo(i, HeightBm - (sMax+32767.)/
		65564.0*HeightBm - 1);
         }
       return;
     }

// Build the data array
// N is the number of samples
// This is not exactly correct but its good enough

   int iSSubFrame = (float)AUDIO_PROXY_PER_FRAME*
	((Frames)*(float(xOffset + StartPixel)/(float)WidthBm)) + .5;
   int iSF = iSSubFrame/AUDIO_PROXY_PER_FRAME;
   int iESubFrame = (float)AUDIO_PROXY_PER_FRAME*
	((Frames)*(float(WidthBm + StartPixel)/(float)WidthBm)) + .5;
   int iEF = iESubFrame/AUDIO_PROXY_PER_FRAME;
   int N = 2*AUDIO_PROXY_PER_FRAME*(iEF - iSF + 1);
   MTI_INT16 *Data = new MTI_INT16[N];

   for (int iFrame = iSF; iFrame <= iEF; iFrame++)
     {
        if (iFrame < 0)
            break;

        if (tl->isAudioValid(Channel, iFrame))
          {
            TL_AUDIO_PROXY *ap = tl->getAudioProxyPointer(iFrame);
            for (int i=0; i < AUDIO_PROXY_PER_FRAME; i++)
              {
                 Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i] =
			ap->sppMinValue[i][Channel];
                 Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i+1] =
			ap->sppMaxValue[i][Channel];
              }
          }
        else
         {
          // invalid audio, use zero
          for (int i= 0; i < AUDIO_PROXY_PER_FRAME; i++)
           {
            Data[2*(iFrame-iSF)*AUDIO_PROXY_PER_FRAME + 2*i] = 0;
            Data[2*(iFrame-iSF)*AUDIO_PROXY_PER_FRAME + 2*i+1] = 0;
           }
         }
      }

   // For non integer frames, this represents the start (iSO) and the End (iEO) offsets into the frame proxy
   int iSO = 2*(iSSubFrame % AUDIO_PROXY_PER_FRAME);
   int iEO = 2*(iESubFrame % AUDIO_PROXY_PER_FRAME);
   int iNO =  2*AUDIO_PROXY_PER_FRAME*(iEF - iSF) - iSO + iEO;

   BuildWaveForm(TRect(xOffset, 0, xBackOffset, HeightBm), Bitmap->Canvas, Data+iSO, iNO-iSO, Scale);

   delete [] Data;}


//-----------------ComputeBitmap------------------John Mertus----Apr 2003---

    void TFAudioTimeLine::ComputeBitmap(void)

// This draws the data into the frames
//
//****************************************************************************
{
  TFBaseTimeLine::ComputeBitmap();

  if (TimeLine() == NULL) return;
  if (Frames() <= 1) return;

  int xOffset;

  TCanvas *DataCanvas = _OffScreenBitmap->Canvas;
  int HeightBm = _OffScreenBitmap->Height;
  int WidthBm = _OffScreenBitmap->Width;

  // Is timeline center frame < MidFrame of display
  if ((AdjustedMidFrame() - Min()) < Frames()/2)
     xOffset = (WidthBm)*(Frames()/2. - AdjustedMidFrame() + Min())/((float)Frames());
  else
     xOffset = 0;

  int StartPixel = (WidthBm-1)*(float(StartFrame() - Min())/(Frames()-1));
  int LastPixel = (WidthBm-1)*(float(TotalFrames() - .5)/(Frames()-1));

  // Are we at the end?
  int xBackOffset = std::min(LastPixel-StartPixel, WidthBm);

  DataCanvas->Pen->Color = SymbolColor[1];
  DataCanvas->Pen->Mode = pmCopy;

  // Three major cases, first is if the number of frames are "small"
  if ((Frames() <= CommonData()->FineDisplayBoundary()) || ((CommonData()->FineDisplayBoundary() < 0) && (Frames() < WidthBm/AUDIO_PROXY_PER_FRAME)))
    {
       double sF = StartFrame();
       double eF = sF + Frames() + 1;
       // Adjust for beginning
       if (sF < Min())
         {
            sF = Min();
            eF = Frames() + StartFrame() + 1;
         }

       // Adjust for the end
       if (eF > (Max()+1)) eF = Max() + 1;

       int N = TimeLine()->getAudioCount (sF, eF);
       double SPerF = ((double)N)/((int)eF - (int)sF);;
       MTI_INT16 *Data = new MTI_INT16[N];
       int nRead = TimeLine()->getAudio(sF, eF,	&Data, N, 1, &Channel);

       // Frames() is not necessarily an integer, adjust for the points
       N = SPerF*Frames();
       if (N > nRead) N = nRead;
       int iBPad = (sF - (int)sF)*SPerF;

       BuildWaveForm(TRect(xOffset, 0, xBackOffset, HeightBm), _OffScreenBitmap->Canvas, Data + iBPad, N - iBPad, TimeLine()->getScale());
       delete [] Data;
       return;
    }


   //  Here is the problem when there are more frames than pixels.
   //  For speed purposes, each pixel receives one frame values.  However
   //  as the current frame changes, that frame may not be displayed and its
   //  neighbor choosen.  This makes the display "jitters" as the display scrolls
   //  slowly.
   //
   //  For this reason, at a given size, once it is decided to display
   //  a given frame, that one will allways be displayed even if its
   //  neighbor is a "better" fit for that current frame.
   //
   //  Thus, the frames to be displayed are computed from zero, then
   //  the display shifted.

   // First check if there are more frames than pixels, if so, just use the maximum
   //
   if (Frames() >= WidthBm)
     {
       for (int i = xOffset; i < WidthBm; i++)
         {
            // Find the nearest frame
            int iFrame = (Frames()-1)*(float(i + StartPixel)/(WidthBm-1)) + .5 + Min();
            if ((iFrame > Max()) || (iFrame < 0)) break;

            int sMin, sMax;
            if (TimeLine()->isAudioValid(Channel, iFrame))
              {
                TL_AUDIO_PROXY *ap = TimeLine()->getAudioProxyPointer(iFrame);
                sMin = ap->spMinValue[Channel] * TimeLine()->getScale();
                sMax = ap->spMaxValue[Channel] * TimeLine()->getScale();

                sMin = std::max(sMin, -32767);
                sMax = std::min(sMax,  32767);
              }
            else
             {
              // audio invalid, use zero
              sMin = 0;
              sMax = 0;
             }

            DataCanvas->MoveTo(i, HeightBm - (sMin+32767.)/65564.0*HeightBm);
            DataCanvas->LineTo(i, HeightBm - (sMax+32767.)/65564.0*HeightBm - 1);

         }
       return;
     }

// Build the data array
// N is the number of samples
// Notice that this is done even if there is no audio.  We could jump out eariler
// if we knew there was not any.

   int iSSubFrame = AUDIO_PROXY_PER_FRAME*((Frames()-1)*(float(xOffset + StartPixel)/(WidthBm-1)) + .5 + Min());
   int iSF = iSSubFrame/AUDIO_PROXY_PER_FRAME;
   int iESubFrame = AUDIO_PROXY_PER_FRAME*((Frames()-1)*(float(WidthBm + StartPixel)/(WidthBm-1)) + .5 + Min());
   int iEF = iESubFrame/AUDIO_PROXY_PER_FRAME;
   int N = 2*AUDIO_PROXY_PER_FRAME*(iEF - iSF + 1);
   MTI_INT16 *Data = new MTI_INT16[N];

   int iNO = 0;
   for (int iFrame = iSF; iFrame <= iEF; iFrame++)
     {
        if ((iFrame > Max()) || (iFrame < 0))
            break;

        if (TimeLine()->isAudioValid(Channel, iFrame))
          {
            TL_AUDIO_PROXY *ap = TimeLine()->getAudioProxyPointer(iFrame);
            for (int i=0; i < AUDIO_PROXY_PER_FRAME; i++)
              {
                 Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i] = ap->sppMinValue[i][Channel];
                 Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i+1] = ap->sppMaxValue[i][Channel];
                 iNO = iNO + 2;
              }
          }
        else
         {
          // audio is invalid, use zero
          for (int i=0; i < AUDIO_PROXY_PER_FRAME; i++)
           {
            Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i] = 0;
            Data[2*(iFrame - iSF)*AUDIO_PROXY_PER_FRAME + 2*i+1] = 0;
            iNO = iNO + 2;
           }
         }
      }

   int iSO = 2*(iSSubFrame % AUDIO_PROXY_PER_FRAME);
   int iEO = 2*(iESubFrame % AUDIO_PROXY_PER_FRAME);
   if (iNO > 0)
     {
        iNO =  iNO - iSO + (2*AUDIO_PROXY_PER_FRAME - iEO);
        BuildWaveForm(TRect(xOffset, 0, xBackOffset, HeightBm), _OffScreenBitmap->Canvas, Data+iSO, iNO-iSO, TimeLine()->getScale());
     }

   delete [] Data;
}

//-----------------ReadDesignSAP------------------John Mertus----Sep 2002-----

      bool TFAudioTimeLine::ReadDesignSAP(void)

//  This restorese the size and position settings to their original values.
//
//****************************************************************************
{
#ifdef NAVIGATOR
    Height = 29;    // shorter timelines for Navigator by default
#else
    Height = 48;
#endif // NAVIGATOR

   // This call back may or may not be necessary
   CommonData()->Changed();
   return true;
}




