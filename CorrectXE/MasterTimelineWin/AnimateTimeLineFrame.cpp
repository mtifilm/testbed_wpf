//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "AnimateTimeLineFrame.h"
#include <math.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BaseTimeLineFrame"
#pragma resource "*.dfm"
TFAnimateTimeLine *FAnimateTimeLine;

//---------------------------------------------------------------------------

__fastcall TFAnimateTimeLine::TFAnimateTimeLine(TComponent* Owner)
        : TFBaseTimeLine(Owner)
{
    EventMask = EVENT_NONE;                 // No Event
#ifdef NAVIGATOR
    // QQQ - Need to figure out a better way to do this!
    Height = 21;
    DataDisplay->Color = TColor(0xFFFAF0);
#endif // NAVIGATOR
}
//---------------------------------------------------------------------------

void TFAnimateTimeLine::ComputeBitmap(void)

// This draws the data into the frames
//
//****************************************************************************
{
   TFBaseTimeLine::ComputeBitmap();

   // Do nothing if we have not been set up
   if (TimeLine() == NULL)
      return;
   if (Frames() < 2)
      return;

   int iSF = StartFrame();
   int iEnd = StartFrame() + Frames();
   
   // Keep in range
   if (iEnd > (int) Max() + 1) iEnd = Max() + 1;
   if (iSF < (int) Min()) iSF = Min();


   // Iterate over all of the keyframes.  Draw the ones that are within
   // the visible portion of the timeline
   int keyframeIndex;
   bool haveKeyframe, inclusion;
   haveKeyframe = TimeLine()->GetFirstKeyframe(keyframeIndex, inclusion);
   while (haveKeyframe)
      {
      if (keyframeIndex >= iEnd)
         break;

      if (keyframeIndex >=iSF)
         {
         int x = DataDisplay->Width*(keyframeIndex-StartFrame())/float(Frames())-1;
         DrawKeyframeSymbol(_OffScreenBitmap, x, inclusion);
         }

      haveKeyframe = TimeLine()->GetNextKeyframe(keyframeIndex, inclusion);
      }
}

void TFAnimateTimeLine::DrawKeyframeSymbol(Graphics::TBitmap *bitmap, int iXPos,
                                           bool inclusion)
{
   // Draw keyframe symbol in timeline

   // Save some typing
   TCanvas *canvas = bitmap->Canvas;
   int iHeight = bitmap->Height;

#if 0
   if (inclusion)
      canvas->Pen->Color = clRed;
   else
      canvas->Pen->Color = clGreen;
#else
   canvas->Pen->Color = clBlack;
#endif

   canvas->MoveTo(iXPos, 0);
   canvas->LineTo(iXPos, iHeight);

}  // DrawKeyframeSymbol

// ---------------------------------------------------------------------------

void __fastcall TFAnimateTimeLine::DataDisplayDblClick(TObject *Sender)
{

  if (DblClickCallback)
   {
    DblClickCallback (Sender);

    // ignore mouse down
    bDblClickIgnoreMouse = true;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFAnimateTimeLine::NextButtonClick(TObject *Sender)
{
   GoToNextEvent();
}
//---------------------------------------------------------------------------

void __fastcall TFAnimateTimeLine::PreviousButtonClick(TObject *Sender)
{
   GoToPreviousEvent();
}
//---------------------------------------------------------------------------

void TFAnimateTimeLine::GoToNextEvent(void)
{
   // Go to next keyframe

   if (TimeLine() == NULL)
      return;

   int i = ceil(CommonData()->CursorPosition() + OffsetFrame());

   // Keep it in bounds
   if ((i < 0) || (i >= TimeLine()->getTotalFrameCount())) return;

   int previousKeyframeIndex;
   bool haveKeyframe, dummy;
   haveKeyframe = TimeLine()->GetNextKeyframe(i, dummy);
   if (!haveKeyframe)
      return;

   int iFrame = i - OffsetFrame();

   CommonData()->CursorPosition(iFrame);
   CommonData()->NextPrevClick(this, iFrame);
}
//---------------------------------------------------------------------------

void TFAnimateTimeLine::GoToPreviousEvent(void)
{
   // Go to previous keyframe
   if (TimeLine() == NULL)
      return;

   int i = floor(CommonData()->CursorPosition() + OffsetFrame());

   // Keep it in bounds
   if ((i < 0) || (i >= TimeLine()->getTotalFrameCount())) return;

   int previousKeyframeIndex;
   bool haveKeyframe, dummy;
   haveKeyframe = TimeLine()->GetPreviousKeyframe(i, dummy);
   if (!haveKeyframe)
      return;

   int iFrame = i - OffsetFrame();

   CommonData()->CursorPosition(iFrame);
   CommonData()->NextPrevClick(this, iFrame);
}
//---------------------------------------------------------------------------

void __fastcall TFAnimateTimeLine::DropEventMenuItemClick(TObject *Sender)
{
   // Delete the keyframe
   if (TimeLine() == NULL)
      return;

   int frameIndex = ceil(CommonData()->CursorPosition() + OffsetFrame());
   // Keep it in bounds
   if ((frameIndex < 0) || (frameIndex >= TimeLine()->getTotalFrameCount()))
       return;

   TimeLine()->DeleteKeyframe(frameIndex);

   RedrawDisplay();

}
//---------------------------------------------------------------------------

void __fastcall TFAnimateTimeLine::ClearAllMarksMenuItemClick(
      TObject *Sender)
{
   if (TimeLine() == NULL)
      return;

   // TBD: Dialog box to confirm user really wants to delete all keyframes

   TimeLine()->DeleteAllKeyframes();

   RedrawDisplay();
}

//---------------------------------------------------------------------------

void TFAnimateTimeLine::UpdateButtons(void)
{
   int i = ceil(CommonData()->CursorPosition() + OffsetFrame());
   if (i >= 0)
    {
      int previousKeyframeIndex;
      bool haveKeyframe, dummy;

      // Enable Next buttons if there are keyframes after this frame
      haveKeyframe = TimeLine()->GetNextKeyframe(i, dummy);
      NextButton->Enabled = haveKeyframe;
      NextEventMenuItem->Enabled = NextButton->Enabled;

      // Enable Previous buttons if there are keyframes before this frame
      int j = floor(CommonData()->CursorPosition() + OffsetFrame());
      haveKeyframe = TimeLine()->GetPreviousKeyframe(j, dummy);
      PreviousButton->Enabled = haveKeyframe;
      PreviousEventMenuItem->Enabled = PreviousButton->Enabled;

      // Enable Delete Keyframe menu item if we're on a keyframe.
      DropEventMenuItem->Enabled = TimeLine()->IsKeyframeHere(i);

      // Enable Clear All menu item if any keyframes exist
      haveKeyframe = TimeLine()->GetFirstKeyframe(i, dummy);
      ClearAllMarksMenuItem->Enabled = haveKeyframe;
    }
}

//---------------------------------------------------------------------------



