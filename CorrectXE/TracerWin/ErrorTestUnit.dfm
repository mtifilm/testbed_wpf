object Form1: TForm1
  Left = 192
  Top = 107
  Caption = 'Form1'
  ClientHeight = 292
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    349
    292)
  PixelsPerInch = 96
  TextHeight = 13
  object Test: TButton
    Left = 128
    Top = 273
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Test'
    TabOrder = 0
    OnClick = TestClick
  end
  object Memo: TMemo
    Left = 8
    Top = 8
    Width = 340
    Height = 250
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 264
    Top = 272
  end
end
