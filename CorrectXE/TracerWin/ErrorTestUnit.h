//---------------------------------------------------------------------------

#ifndef ErrorTestUnitH
#define ErrorTestUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "IniFile.h"
#include "bthread.h"
#include <ExtCtrls.hpp>


//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Test;
        TMemo *Memo;
        TTimer *Timer1;
        void __fastcall TestClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
private:	// User declarations

  static void StaticTestThread(void *vp, void *vpReserved)
   { ((TForm1 *)vp)->TestThread(vpReserved); }

   void MemoAdd(const string &msg);
   bool TestThread(void *vpReserved);
   ErrorTID etidTestThread;
   bool bThreadDead;

protected:
   DEFINE_ERROR_HANDLER(TForm1);

public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
