//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <stdlib.h>

#include "ErrorTestUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
   INIT_ERROR_HANDLER;
}

char MainErrMsg1[] = "    Main Error Message";
#define MAIN_ERROR_1 17

char MainErrMsg2[] = "    Second Main Error Message";
#define MAIN_ERROR_2 133

char ThreadErrMsg1[] = "    First thread Error Message";
#define THREAD_ERROR_1 -209

char ThreadErrMsg2[] = "    First Dialogbox from thread using Notify";
#define THREAD_ERROR_2 -1271

char ThreadErrMsg3[] = "    Second Dialogbox from thread using NotifyNoWait";
#define THREAD_ERROR_3 -2034

void TForm1::MemoAdd(const string &msg)
{
   Memo->Lines->Add(msg.c_str());
}

int TForm1::ErrorHandler(const ErrorTID etid, const int Error, const string &Message)
{
     ostringstream os;
     MemoAdd("Callback Test: Next two lines should be the same");
     if (Error == THREAD_ERROR_2)
        os << ThreadErrMsg2 << ", Error: " << THREAD_ERROR_2;
     else
        os << ThreadErrMsg3 << ", Error: " << THREAD_ERROR_3;

     MemoAdd(os.str());
     os.str("");
     os << Message << ", Error: " << Error;
     MemoAdd(os.str());
     MemoAdd("");

     _MTIInformationDialog(Handle, os.str().c_str());

     MemoAdd("Callback Test: Next two TID should be the same, the third different");
     os.str("");
     os << "    Thread: " << etidTestThread << ", ErrorHandler: " << etid;
     os << ", Current: " << theError.getCurrentTID();
     MemoAdd(os.str());
     MemoAdd("");
     return(Error);
}
//------------------TestThread------------------John Mertus----Jan 2002-----

      bool TForm1::TestThread(void *vpReserved)

//  This sets some thread errors and reports them
//
//****************************************************************************
{
   // Note, setting the TID before the BThreadBegin is safe, after is not
   etidTestThread = theError.getCurrentTID();
   theError.set(THREAD_ERROR_1,ThreadErrMsg1);
   BThreadBegin(vpReserved);
   Sleep(100);

   theError.setMessage(ThreadErrMsg2);
   theError.setError(THREAD_ERROR_2);
   int iRet = theError.Notify();
   if (iRet == THREAD_ERROR_2)
   {
	 theError.setMessage(ThreadErrMsg3);
	 theError.setError(THREAD_ERROR_3);
     theError.NotifyNoWait();
   }

   // Signal we are done
   bThreadDead = true;
   return(true);
}

//---------------------------------------------------------------------------
void __fastcall TForm1::TestClick(TObject *Sender)
{
  ostringstream os;

  Memo->Clear();
  theError.set(MAIN_ERROR_1,MainErrMsg1);

  // Spawn a thread to report errors
  if (BThreadSpawn(StaticTestThread, (void*)this) == NULL)
    {
  ////    _MTIErrorDialog(Handle, "Thread Spawn Failed");
      return;
	}

  bThreadDead = false;
  Timer1->Enabled = true;
  //
  //  Test Thread separation, BThreadSpawn sets the thread error
  //
  //
  os.str("");
  MemoAdd("Separation Test: Next two lines should be the same");
  os << MainErrMsg1 << ", Error: " << MAIN_ERROR_1;
  MemoAdd(os.str());
  os.str("");
  os << theError.getMessage() << ", Error: " << theError.getError();
  MemoAdd(os.str());
  MemoAdd("");

  //
  //  Now read the thread error messages
  //
  MemoAdd("Thread Test: Next two lines should be the same");
  os.str("");
  os << ThreadErrMsg1 << ", Error: " << THREAD_ERROR_1;
  MemoAdd(os.str());
  os.str("");
  os << theError.getMessage(etidTestThread) << ", Error: " << theError.getError(etidTestThread);
  MemoAdd(os.str());
  MemoAdd("");

  //
  //  Now set and the main errors via the property interface Error an Message
  os.str("");
  theError.setError(MAIN_ERROR_2);
  theError.setMessage(MainErrMsg2);
  MemoAdd("Property test: Next two lines should be the same");
  os << MainErrMsg2 << ", Error: " << MAIN_ERROR_2;
  MemoAdd(os.str());
  os.str("");
  os << theError.getMessage() << ", Error: " << theError.getError();
  MemoAdd(os.str());
  MemoAdd("");

  os.str("");
  os << theError.getFmtError();
  MemoAdd(os.str());
  MemoAdd("Done with inline tests, start thread tests");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
  if (bThreadDead)
  {
    MemoAdd("These lines should appear after first thread dialog box");
    MemoAdd("but before the second thread dialog box is closed");
    Timer1->Enabled = false;
  }

}
//---------------------------------------------------------------------------



