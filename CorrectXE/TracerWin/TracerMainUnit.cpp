//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TracerMainUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma resource "*.dfm"
#include "MTIio.h"
#include "IniFile.h"
#include "About.h"
#include "MTIWinInterface.h"


static unsigned CF_MTIText = 0;
static unsigned guMTITextMessage= 0;

//--------------GetMTIClipboardData---------------John Mertus-----Sep 2001---

      bool GetMTIClipboardData(string &Result)

// This gets any clipboard message of the CF_MTIText =(MTITraceFormat)
// clipboard format.   The message is cleared aftewards.
//
//***************************************************************************
{
   // Register a clipboard format
   if (CF_MTIText == 0)
       CF_MTIText = RegisterClipboardFormat("MTITraceFormat");
   if (CF_MTIText == 0) return(false);

   if (!IsClipboardFormatAvailable(CF_MTIText)) return(false);
  // Can't open clipboard
   if (!OpenClipboard(NULL)) return(false);

   HANDLE Data = GetClipboardData(CF_MTIText);
   if (Data == NULL)
      {
         CloseClipboard();
         return(false);
      }

   Result = (char *)GlobalLock(Data);
   // Strip off the last LF
   if (Result.length() > 0)
     if (Result[Result.length()-1] == '\n')
         Result = Result.substr(0,Result.length()-1);
   //
   // Clear out the format
   SetClipboardData(CF_MTIText, NULL);

   CloseClipboard();
   return(true);
}

TMTIMainForm *MTIMainForm;
//---------------------------------------------------------------------------
__fastcall TMTIMainForm::TMTIMainForm(TComponent* Owner)
        : TMTIForm(Owner)
{
    SendMessage(Handle, CW_START_PROGRAM, 0, 0);
    return;
}

// Just Exit
void __fastcall TMTIMainForm::Exit1Click(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a copy.  Note: Component writers MUST respond to
// WM_COPY
void __fastcall TMTIMainForm::Copy1Click(TObject *Sender)
{
  SendMessage(GetFocus(), WM_COPY, 0, 0);
}

//---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a CUT.  Note: Component writers MUST respond to
// WM_CUT
void __fastcall TMTIMainForm::Cut1Click(TObject *Sender)
{
  SendMessage(GetFocus(), WM_CUT, 0, 0);
}
//---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a PASTE.  Note: Component writers MUST respond to
// WM_PASTE
void __fastcall TMTIMainForm::Paste1Click(TObject *Sender)
{
  SendMessage(GetFocus(), WM_PASTE, 0, 0);
}

//---------------------------------------------------------------------------

// This restores the design size of the program
void __fastcall TMTIMainForm::RestoreSize1Click(TObject *Sender)
{
    RestoreSAP();
}
//---------------------------------------------------------------------------

// This restores all sizes of windows that are created
void __fastcall TMTIMainForm::RestoreAllSizes1Click(TObject *Sender)
{
   RestoreAllSAP();
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::Open2Click(TObject *Sender)
{
    if (OpenDialog->Execute())
     {
        OpenMemo(OpenDialog->FileName);
     }
}
//---------------------------------------------------------------------------

void TMTIMainForm::OpenMemo(String fname)
{
////        if (FileName != "") HistoryList->Add(FileName);
        FileName = fname;
        Memo->Lines->LoadFromFile(FileName);
        bfCheckForChange = true;
        Memo->Modified = false;
}


//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
//    if (FileName != "") HistoryList->Add(FileName.c_str());
 //   TBIniSavePositions(this, IniFileName->c_str(), DefaultIniSection->c_str());
//    HistoryList->SaveToFile(IniFileName->c_str(), DefaultIniSection->c_str());
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::SelectAll1Click(TObject *Sender)
{
    if (GetFocus() == Memo->Handle) Memo->SelectAll();
}
//---------------------------------------------------------------------------

void TMTIMainForm::MakeLineEndingsWindowsStyle(string &s)
{
   string::size_type LFPos = 0;
   while ((LFPos = s.find("\n", LFPos)) != string::npos)
   {
      if (LFPos == 0 || s[LFPos-1] != '\r')
      {
         s.insert(LFPos, "\r");
         LFPos++;
      }
      LFPos++;
   }

   // no bare \r at end of s; perhaps should erase all bare \r in s
   if (s[s.size()-1] == '\r')
      s.erase(s.size()-1, 1);
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::MTITextMessage(TMessage &Message)

{
   string Result;
   if (GetMTIClipboardData(Result))
     {
       MakeLineEndingsWindowsStyle(Result);
       Memo->Lines->Add(Result.c_str());
       Memo->Perform(EM_SCROLLCARET,0,Memo->Lines->Count);

       // Very crude way of finding the ini files
       // Should be better but I don't care
       int i = Result.find("ini file: ");
       if (i < 0) i = Result.find("nslation: ");
       if (i > 0)
         {
           i = i + 10;
           int n = Result.find('\n',i);
          //// HistoryList->Add(Result.substr(i,n-i).c_str());
         }
     }
}

void __fastcall TMTIMainForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
  if (Memo->Modified && bfCheckForChange)
    {
      int Res = MessageDlg("File has been modified,\nSave it?", mtConfirmation, TMsgDlgButtons() << mbYes << mbNo << mbCancel, 0);
      if (Res == mrCancel)
        {
           CanClose = false;
           return;
        }

      if (Res == mrYes)
       {
         Save1Click(Sender);
         if (Memo->Modified)
           {
             CanClose = false;   // Not really saved
             return;
           }

       }
    }

  CPMPSetLogFileName("");  // This closes the log file
  SaveSettings(CM_STARTUP_DATA);
  SaveSAP(CM_STARTUP_DATA);
//  TMTIForm::FormCloseQuery(Sender, CanClose);
}

// Test routines for INI files, not used in tracer but used to test
// CPMP
void TMTIMainForm::ReadStringList(void)
{
   CIniFile *ini = new CIniFile("c:\\temp\\test.ini");
   StringList sl, Default;
   Default.push_back("d:\\MTIShare\\.mtibins");
   ini->ReadStringList("BinDirRoots","Root",&sl, &Default);
   for (unsigned int i = 0; i < sl.size(); i++)
        Memo->Lines->Add(sl[i].c_str());
}

void TMTIMainForm::WriteStringList(void)
{
//   CIniFile *ini = new CIniFile("c:\\temp\\test.ini");
//   StringList sl;
//   for (int i = 0; i < Memo->Lines->Count; i++)
//   {
//        string s = (string)(Memo->Lines->Strings[i].c_str());
//        sl.push_back(*s);
//   }
//   ini->WriteStringList("BinDirRoots","Root",&sl);
//   delete ini;
}

//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::SaveAs1Click(TObject *Sender)
{
   if (SaveDialog->Execute())
     {
       FileName = SaveDialog->FileName;
       Memo->Lines->SaveToFile(FileName);
       Memo->Modified = false;
       bfCheckForChange = true;
////       SaveButton->Enabled = false;
     }
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::Save1Click(TObject *Sender)
{
   if (FileName == "")
     SaveAs1Click(Sender);
   else
    {
       Memo->Lines->SaveToFile(FileName);
       Memo->Modified = false;
////       SaveButton->Enabled = false;
       bfCheckForChange = true;
    }
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::AlwaysonTop1Click(TObject *Sender)
{
   if (FormStyle == fsStayOnTop) FormStyle = fsNormal;
   else FormStyle = fsStayOnTop;
   UpdateForm();
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::Close1Click(TObject *Sender)
{
   if (Memo->Modified)
    {
      int Res = MessageDlg("File has been modified,\nSave it?", mtConfirmation, TMsgDlgButtons() << mbYes << mbNo << mbCancel, 0);
      if (Res == mrCancel) return;
      if (Res == mrYes) Save1Click(Sender);
    }
   FileName = "";
   Memo->Lines->Clear();
   Memo->Modified = false;
   ////SaveButton->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::MemoChange(TObject *Sender)
{
    //// SaveButton->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::ClearButtonClick(TObject *Sender)
{
   Memo->Lines->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::WordWrap1Click(TObject *Sender)
{
   Memo->WordWrap = !Memo->WordWrap;
   UpdateForm();
}
//---------------------------------------------------------------------------

//----------------WriteSettings--------------------John Mertus-----Jan 2001---

    bool TMTIMainForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the ini files, saves the position and size
//
//******************************************************************************
{
  if (ini == NULL) return(false);
//
//  Save the values
  ini->WriteBool(IniSection, "WordWrap", Memo->WordWrap);
  return(true);
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TMTIMainForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  Thes reads the init file and sets the saved position and size
//
//******************************************************************************
{
  if (ini == NULL) return(false);
  Memo->WordWrap = ini->ReadBool(IniSection, "WordWrap", Memo->WordWrap);
  ////TBIniLoadPositions(this, GetIniFileName().c_str(), GetIniSection().c_str());
  UpdateForm();
  return(true);
}

//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::About1Click(TObject *Sender)
{
   AboutForm = new TAboutForm(NULL);
   AboutForm->ShowModal();
   delete AboutForm;
   AboutForm = nullptr;

}

//---------------------------------------------------------------------------

        void TMTIMainForm::UpdateForm(void)
{
   AlwaysonTop1->Checked = (FormStyle == fsStayOnTop);
   WordWrap1->Checked = Memo->WordWrap;
   if (Memo->WordWrap)
     Memo->ScrollBars = ssVertical;
   else
     Memo->ScrollBars = ssBoth;
}

void __fastcall TMTIMainForm::FormCreate(TObject *Sender)
{
    bfCheckForChange = false;

    guMTITextMessage = RegisterWindowMessage("MTITraceMessage");

    // Clear any data on clipboard by reading it
    string tmp;
    GetMTIClipboardData(tmp);

    UpdateForm();

   //// PasteButton->Enabled = IsClipboardFormatAvailable(CF_TEXT);
    fClipboardOwner = SetClipboardViewer(Handle);
    ////LoadMainIcons();
}

//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::ToolButton2Click(TObject *Sender)
{
   TRACE_0(errout << "This is a test of debug");
   LOG_MSG(errout << "Only a message");
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::FormDestroy(TObject *Sender)
{
   ChangeClipboardChain(Handle, fClipboardOwner);
}

void TMTIMainForm::WMChangeCBChain(TWMChangeCBChain &Msg)
 {
   // If we are removed, change our owner
   if (Msg.Remove == fClipboardOwner)
     fClipboardOwner = Msg.Next;
   else
     SendMessage(fClipboardOwner, WM_CHANGECBCHAIN, (unsigned)Msg.Remove, (unsigned)Msg.Next);

   Msg.Result = 0;
 }


 //---------------------------------------------------------------------------

//    Procedure WMChangeCBChain(var Msg: TWMChangeCBChain); message WM_CHANGECBCHAIN;
//    Procedure WMDrawClipboard(var Msg: TWMDrawClipboard); message WM_DRAWCLIPBOARD;

    // Note: this dispatch function overrides message passing
    // Understand message passing before hacking this.
   void __fastcall TMTIMainForm::Dispatch(void *Message)
{
   TMessage *ms = (TMessage *)Message;

   if (ms->Msg == guMTITextMessage)
      {
          MTITextMessage(*ms);
          return;
      }

   // Process other messages
   switch (ms->Msg)
     {
       case WM_CHANGECBCHAIN:
          WMChangeCBChain(*(TWMChangeCBChain *)ms);
          break;

       case WM_DRAWCLIPBOARD:
          MTITextMessage(*ms);
 ////         PasteButton->Enabled = IsClipboardFormatAvailable(CF_TEXT);
          break;

       default:
         TMTIForm::Dispatch(Message);
     }
}

void __fastcall TMTIMainForm::MemoSelectionChange(TObject *Sender)
{
   //// CutButton->Enabled = (Memo->SelLength != 0);
   //// CopyButton->Enabled = (Memo->SelLength != 0);
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::HistoryListClick(TObject *Sender,
      const AnsiString Filename)
{

   OpenMemo(Filename);
}
//---------------------------------------------------------------------------



void __fastcall TMTIMainForm::ButtonMenuItemClick(TObject *Sender)
{
 //// ButtonToolbar->Visible = !ButtonToolbar->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TMTIMainForm::ToolBarMenuItemClick(TObject *Sender)
{
//  MainMenuItem->Checked = MainToolbar->Visible;
//  ButtonMenuItem->Checked = ButtonToolbar->Visible;
}
//---------------------------------------------------------------------------


void __fastcall TMTIMainForm::TestToolButtonClick(TObject *Sender)
{
        TRACE_0(errout << "Stupid");
}
//---------------------------------------------------------------------------

