//---------------------------------------------------------------------------

#ifndef TracerMainUnitH
#define TracerMainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "MTIUnit.h"
#include <Dialogs.hpp>
#include <ImgList.hpp>
#include <System.ImageList.hpp>

//---------------------------------------------------------------------------
class TMTIMainForm : public TMTIForm
{
__published:	// IDE-managed Components
        TPanel *LeftDockPanel;
        TRichEdit *Memo;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TImageList *ButtonImageList;
	TMenuItem *File1;
	TMenuItem *New1;
	TMenuItem *Open2;
	TMenuItem *Close1;
	TMenuItem *N2;
	TMenuItem *Save1;
	TMenuItem *SaveAs1;
	TMenuItem *N1;
	TMenuItem *Exit1;
	TMenuItem *Edit1;
	TMenuItem *Cut1;
	TMenuItem *Copy1;
	TMenuItem *Paste1;
	TMenuItem *SelectAll1;
	TMenuItem *N3;
	TMenuItem *Clear1;
	TMenuItem *Windows1;
	TMenuItem *RestoreSize1;
	TMenuItem *RestoreAllSizes1;
	TMenuItem *N4;
	TMenuItem *AlwaysonTop1;
	TMenuItem *WordWrap1;
	TMenuItem *Help1;
	TMenuItem *About1;
	TMenuItem *TBSeparatorItem1;
	TMenuItem *ToolBarMenuItem;
	TMenuItem *ButtonMenuItem;
	TMenuItem *TBSeparatorItem5;
        TPopupMenu *TBPopupMenu1;
	TMainMenu *MainMenu1;
	TToolBar *ToolBar1;
	TToolButton *TestToolButton;
        void __fastcall Copy1Click(TObject *Sender);
        void __fastcall Cut1Click(TObject *Sender);
        void __fastcall Paste1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall RestoreSize1Click(TObject *Sender);
        void __fastcall RestoreAllSizes1Click(TObject *Sender);
        void __fastcall Open2Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall SelectAll1Click(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall SaveAs1Click(TObject *Sender);
        void __fastcall Save1Click(TObject *Sender);
        void __fastcall AlwaysonTop1Click(TObject *Sender);
        void __fastcall Close1Click(TObject *Sender);
        void __fastcall MemoChange(TObject *Sender);
        void __fastcall ClearButtonClick(TObject *Sender);
        void __fastcall WordWrap1Click(TObject *Sender);
        void __fastcall About1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall ToolButton2Click(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall MemoSelectionChange(TObject *Sender);
        void __fastcall HistoryListClick(TObject *Sender,
          const AnsiString Filename);
        void __fastcall ButtonMenuItemClick(TObject *Sender);
        void __fastcall ToolBarMenuItemClick(TObject *Sender);
	void __fastcall TestToolButtonClick(TObject *Sender);
private:	// User declarations
        String FileName;
        bool bfCheckForChange;
        unsigned guMTITextMessage;
        HWND fClipboardOwner;
        void UpdateForm(void);
        void WMChangeCBChain(TWMChangeCBChain &Msg);
        void ReadStringList(void);
        void WriteStringList(void);
        void MakeLineEndingsWindowsStyle(string &s);

protected:
    void __fastcall MTITextMessage(TMessage &Message);
    virtual void __fastcall Dispatch(void *Message);

public:		// User declarations
    __fastcall TMTIMainForm(TComponent* Owner);
    virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
    void OpenMemo(String fname);

};
//---------------------------------------------------------------------------
extern PACKAGE TMTIMainForm *MTIMainForm;
//---------------------------------------------------------------------------
#endif
