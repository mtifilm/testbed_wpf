// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("..\MtiComponents\Repository\MTIUNIT.cpp", MTIForm);
USEFORM("TracerMainUnit.cpp", Form1);
USEFORM("..\MtiComponents\Repository\About.cpp", AboutForm);
//---------------------------------------------------------------------------
#include "IniFile.h"
#include "TracerMainUnit.h"
// ---------------------------------------------------------------------------
HANDLE hMutex = NULL;

// ---------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    // Make sure another instance is not running....
    hMutex = CreateMutex(NULL, TRUE, "MTITracer");
    if (GetLastError() == ERROR_ALREADY_EXISTS)
    {
	return 0;
    }

    try
    {
	Application->Initialize();
	Application->Title = "Tracer";
	Application->CreateForm(__classid(TMTIMainForm), &MTIMainForm);
		Application->CreateForm(__classid(TAboutForm), &AboutForm);
		Application->Run();
    }
    catch (Exception &exception)
    {
	Application->ShowException(&exception);
    }
    if (hMutex != NULL)
    {
	// Switch off the multiple instance stopping flag
	(void)CloseHandle(hMutex);
	hMutex = NULL;
    };

    return 0;
}
// ---------------------------------------------------------------------------
