//---------------------------------------------------------------------------

#ifndef UnitVTRControlH
#define UnitVTRControlH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "VTimeCodeEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormVTRControl : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *Label1;
        TLabel *LabelTimecode;
        TEdit *EditPort;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TPanel *videoPlayGroupPanel;
        TBitBtn *navStopButton;
        TBitBtn *navPlayReverseButton;
        TBitBtn *navPlayButton;
        TBitBtn *navRewindButton;
        TBitBtn *navFastForwardButton;
        TPanel *videoJogButtonGroup;
        TBitBtn *JogReverseButton;
        TBitBtn *JogForwardButton;
        TBitBtn *ContJogReverseButton;
        TBitBtn *ContJogForwardButton;
        TPanel *videoGotoButtonGroup;
        TBitBtn *GotoInButton;
        TBitBtn *GotoMarkIn;
        TBitBtn *GotoOutButton;
        TBitBtn *GotoMarkOut;
        TPanel *videoLoopButtonGroup;
        TBitBtn *PlayInOutButton;
        TBitBtn *LoopPlayButton;
        TBitBtn *PlayBetweenMarksButton;
        TBitBtn *LoopBetweenMarksButton;
        TPanel *videoMarkButtonGroup;
        TBitBtn *MarkInButton;
        TBitBtn *RemoveMarkInButton;
        TBitBtn *MarkOutButton;
        TBitBtn *RemoveMarkOutButton;
        TBitBtn *SetMarkInButton;
        TBitBtn *ExpandMarksButton;
        TBitBtn *SetMarkOutButton;
        TBitBtn *UnexpandMarksButton;
        TBitBtn *CueToButton;
        VTimeCodeEdit *CueupTimecode;
        TTimer *Timer1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall navStopButtonClick(TObject *Sender);
        void __fastcall navPlayReverseButtonClick(TObject *Sender);
        void __fastcall navRewindButtonClick(TObject *Sender);
        void __fastcall navPlayButtonClick(TObject *Sender);
        void __fastcall navFastForwardButtonClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
    class CCallSerCom *SerialPort;
public:		// User declarations
        __fastcall TFormVTRControl(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormVTRControl *FormVTRControl;
//---------------------------------------------------------------------------
#endif
