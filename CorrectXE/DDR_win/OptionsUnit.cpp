//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OptionsUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "OptionsFrameUnit"
#pragma resource "*.dfm"
TParametersForm *ParametersForm;
//---------------------------------------------------------------------------
__fastcall TParametersForm::TParametersForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TParametersForm::OkButtonClick(TObject *Sender)
{
   OptionsFrame->UpdateEmulator();
}
//---------------------------------------------------------------------------

