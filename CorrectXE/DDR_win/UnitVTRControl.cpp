//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitVTRControl.h"
#include "CallSerCom.h"
#include "UnitDDRControl.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TFormVTRControl *FormVTRControl;
//---------------------------------------------------------------------------
__fastcall TFormVTRControl::TFormVTRControl(TComponent* Owner)
    : TForm(Owner)
{
  SerialPort = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::Button1Click(TObject *Sender)
{
  SerialPort->SetSerialPort (atoi(EditPort->Text.c_str()));
  SerialPort->Open();
}
//---------------------------------------------------------------------------



void __fastcall TFormVTRControl::FormCreate(TObject *Sender)
{
  SerialPort = new CCallSerCom;
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::FormDestroy(TObject *Sender)
{
  delete SerialPort;
  SerialPort = 0;
}
//---------------------------------------------------------------------------


void __fastcall TFormVTRControl::Button3Click(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::Button2Click(TObject *Sender)
{
  SerialPort->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::Timer1Timer(TObject *Sender)
{
  char caTime[16];
  SerialPort->TimeSense (caTime);

  LabelTimecode->Caption = caTime;

  if (SerialPort->IsConnected())
   {
    Button1->Enabled = false;
    Button2->Enabled = true;
   }
  else
   {
    Button1->Enabled = true;
    Button2->Enabled = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::navStopButtonClick(TObject *Sender)
{
  SerialPort->Stop();        
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::navPlayReverseButtonClick(TObject *Sender)
{
  SerialPort->PlayBack();        
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::navPlayButtonClick(TObject *Sender)
{
  SerialPort->Play();
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::navRewindButtonClick(TObject *Sender)
{
  SerialPort->Rewind();        
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::navFastForwardButtonClick(TObject *Sender)
{
  SerialPort->FastForward();        
}
//---------------------------------------------------------------------------

void __fastcall TFormVTRControl::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  // process key events        
}
//---------------------------------------------------------------------------

