//---------------------------------------------------------------------------

#ifndef ProxyUnitH
#define ProxyUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
// forward declarations
class CImageFormat;

class TProxyForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TMainMenu *MainMenu1;
        TMenuItem *ViewMenu;
        TMenuItem *FieldsSubMenu;
        TMenuItem *Field1MenuItem;
        TMenuItem *Field2MenuItem;
        TMenuItem *BothFieldsMenuItem;
        TMenuItem *ZonesSubMenu;
        TMenuItem *SafeActionMenuItem;
        TMenuItem *SafeTitleMenuItem;
        void __fastcall FormSize(TObject *Sender);
        void __fastcall Refresh(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FieldsSubMenuClick(TObject *Sender);
        void __fastcall Field1MenuItemClick(TObject *Sender);
        void __fastcall Field2MenuItemClick(TObject *Sender);
        void __fastcall BothFieldsMenuItemClick(TObject *Sender);
        void __fastcall ZonesSubMenuClick(TObject *Sender);
        void __fastcall SafeActionMenuItemClick(TObject *Sender);
        void __fastcall SafeTitleMenuItemClick(TObject *Sender);
        void __fastcall CreateForm(TObject *Sender);
        void __fastcall DestroyForm(TObject *Sender);

private:	// User declarations

   HPEN whitePen;

public:		// User declarations
        __fastcall TProxyForm(TComponent* Owner);

   void DrawZonesOntoScreen(HDC scrdc, int zmsk);
   void DrawZonesIntoBitmap(HDC bmdc,  int zmsk);
   void ClearImage(int zmsk);
   void BltProxy(HDC bmdc, int zmsk);

};
//---------------------------------------------------------------------------
extern PACKAGE TProxyForm *ProxyForm;
//---------------------------------------------------------------------------
#endif
