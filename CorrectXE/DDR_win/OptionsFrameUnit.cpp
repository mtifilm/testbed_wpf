//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OptionsFrameUnit.h"
#include "IniFile.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "VDoubleEdit"
#pragma resource "*.dfm"
TOptionsFrame *OptionsFrame;


//---------------------------------------------------------------------------
__fastcall TOptionsFrame::TOptionsFrame(TComponent* Owner)
        : TFrame(Owner)
{
   _DDR = NULL;
   _bOverlayUpdate = false;
   _PortCount = 0;

  _WholeOverlayImage = new Graphics::TBitmap;
  _WholeOverlayImage->Assign(OverlayImage->Picture->Bitmap);

  // Build the combo boxes
  for (int i=0; i < SynchAssociations.Size(); i++)
    VideoSynchCBox->Items->Add(SynchAssociations.DisplayNameByIndex(i).c_str());

  for (int i=0; i < VideoLoopbackAssociations.Size(); i++)
    VideoLoopBackCBox->Items->Add(VideoLoopbackAssociations.DisplayNameByIndex(i).c_str());

  for (int i=0; i < VideoPrecisionAssociations.Size(); i++)
   {
     PrecisionOutCBox->Items->Add(VideoPrecisionAssociations.DisplayNameByIndex(i).c_str());
     PrecisionInCBox->Items->Add(VideoPrecisionAssociations.DisplayNameByIndex(i).c_str());
   }
}
//---------------------------------------------------------------------------

        void TOptionsFrame::SetEmulator(CVTRInterface &DDR)
{
   _DDR = &DDR;
   UpdateForm();
}

        void TOptionsFrame::UpdateForm(void)
{

  // Overlay Section
  _bOverlayUpdate = false;
  OverlayCBox->Checked = _DDR->getOverlayEnable();
  _OldOvlyX = _DDR->getOverlayX()/100.0*OverlayImage->Width;
  _OldOvlyY = _DDR->getOverlayY()/100.0*OverlayImage->Height;
  OverlaySizeSEdit->Value = _DDR->getOverlaySZ();

  // -1 means a special background
  if (_DDR->getOverlayBG() == -1)
    OverlayBGColorGroup->ItemIndex = 1;
  else
    OverlayBGColorGroup->ItemIndex = 0;

  OverlayFGColorGroup->ItemIndex = _DDR->getOverlayFG() % 2;

  // "Emulator" responses
  MTI_UINT32 val0, val1;
  _DDR->getDeviceType (_PortCount, val0, val1);

  MTIostringstream os;
  os << hex << setw(2) << setfill('0') << val0 << ":"  << setw(2) << setfill('0')<< val1;
  DeviceIDEdit->Text = os.str().c_str();

  MaxShuttleSpeedSEdit->Value = _DDR->getMaxShuttleSpeed();

  VideoTimeCodeDelayPlaySEdit->Value = _DDR->getVideoTimeCodeDelayPlay();
  VideoTimeCodeDelayRecordSEdit->Value = _DDR->getVideoTimeCodeDelayRecord();
  IgnoreControllerTimingCBox->Checked = _DDR->getIgnoreControllerTiming() == 0;
  StopReactionFrameSEdit->Value = _DDR->getStopReactionFrame();
  InstructionFrameDelaySEdit->Value = _DDR->getInstructionFrameDelay();

  ProxyFileCBox->Checked = _DDR->getProxyIn(_PortCount);
  DifferenceFileCBox->Checked = _DDR->getDiffFile(_PortCount);

  // Audio Section
  AudioInCBox->Checked = _DDR->getAudioIn(_PortCount);
  AudioOutCBox->Checked = _DDR->getAudioOut(_PortCount);

  AudioWriteDiskCBox->Checked = _DDR->getAudioInhibitIn(_PortCount);
  AudioReadDiskCBox->Checked = _DDR->getAudioInhibitOut(_PortCount);

  AudioOutDeviceName->Text = _DDR->getAudioDeviceNameOut().c_str();
  AudioInDeviceName->Text = _DDR->getAudioDeviceNameIn().c_str();

  AudioSlipInEdit->dValue = _DDR->getAudioSlipIn();
  AudioSlipOutEdit->dValue = _DDR->getAudioSlipOut();

  AudioBlendEdit->dValue = _DDR->getMilliSecondsAudioBlend();

  // Video Section
  VideoInCBox->Checked = _DDR->getVideoIn(_PortCount);
  VideoOutCBox->Checked = _DDR->getVideoOut(_PortCount);

  VideoWriteDiskCBox->Checked = _DDR->getVideoInhibitIn(_PortCount);
  VideoReadDiskCBox->Checked = _DDR->getVideoInhibitOut(_PortCount);

  VideoOutDeviceName->Text = _DDR->getVideoBoardNameOut().c_str();
  VideoInDeviceName->Text = _DDR->getVideoBoardNameOut().c_str();

  // Now read the connect parameters
  int iP = _DDR->getSerialPort(_PortCount);
  int iS = _DDR->getSocketPort(_PortCount);

  // Connect to the network or serial port
  if (iP == -1)
    {
      ConnectGroupBox->ItemIndex = 1;
      iP = 1;
    }

  // Don't expose the -1 default
  if (iS == -1) iS = 1234;
  DDRSerialPortSEdit->Value = iP;
  DDRSocketSEdit->Value = iS;

  // Update the Video synch signals
  VideoSynchCBox->ItemIndex =
    VideoSynchCBox->Items->IndexOf(SynchAssociations.DisplayName(_DDR->getVideoSyncSource()).c_str());

  VideoLoopBackCBox->ItemIndex =
    VideoLoopBackCBox->Items->IndexOf(VideoLoopbackAssociations.DisplayName(_DDR->getVideoLoopback()).c_str());

  PrecisionInCBox->ItemIndex =
    PrecisionInCBox->Items->IndexOf(VideoPrecisionAssociations.DisplayName(_DDR->getVideoExternalPrecisionIN()).c_str());

  PrecisionOutCBox->ItemIndex =
    PrecisionOutCBox->Items->IndexOf(VideoPrecisionAssociations.DisplayName(_DDR->getVideoExternalPrecisionOUT()).c_str());

  // This inhibits the overlay updates
  _bOverlayUpdate = true;
  DrawOverlay(_OldOvlyX, _OldOvlyY);
  Modified = false;
}

        void TOptionsFrame::UpdateEmulator(void)

//  This copies the GUI to the interface

{

  // Overlay Section
  _DDR->setOverlayEnable(OverlayCBox->Checked );
  _DDR->setOverlayX(_OldOvlyX*100.0/OverlayImage->Width+.99);
  _DDR->setOverlayY(_OldOvlyY*100.0/OverlayImage->Height+.99);
  _DDR->setOverlaySZ(OverlaySizeSEdit->Value);

    // Now decode the FG/Color buttons
  _DDR->setOverlayFG(OverlayFGColorGroup->ItemIndex);

  if (OverlayBGColorGroup->ItemIndex == 1)
    _DDR->setOverlayFG(-1);
  else
    _DDR->setOverlayFG((OverlayFGColorGroup->ItemIndex + 1) %2);

  // "Emulator" responses
  MTI_UINT32 val0, val1;
  sscanf(DeviceIDEdit->Text.c_str(), "%x:%x",&val0, &val1);
  _DDR->setDeviceType (_PortCount, val0, val1);

  _DDR->setMaxShuttleSpeed(MaxShuttleSpeedSEdit->Value);

  _DDR->setVideoTimeCodeDelayPlay(VideoTimeCodeDelayPlaySEdit->Value );
  _DDR->setVideoTimeCodeDelayRecord(VideoTimeCodeDelayRecordSEdit->Value);
  _DDR->setIgnoreControllerTiming(IgnoreControllerTimingCBox->Checked);
  _DDR->setStopReactionFrame(StopReactionFrameSEdit->Value );
  _DDR->setInstructionFrameDelay(InstructionFrameDelaySEdit->Value);

  _DDR->setProxyIn(_PortCount, ProxyFileCBox->Checked);
  _DDR->setDiffFile(_PortCount, DifferenceFileCBox->Checked);

  // Audio Section
  _DDR->setAudioIn(_PortCount, AudioInCBox->Checked);
  _DDR->setAudioOut(_PortCount, AudioOutCBox->Checked);

  _DDR->setAudioInhibitIn(_PortCount, AudioWriteDiskCBox->Checked);
  _DDR->setAudioInhibitOut(_PortCount, AudioReadDiskCBox->Checked);

  _DDR->setAudioDeviceNameOut(AudioOutDeviceName->Text.c_str());
  _DDR->setAudioDeviceNameIn(AudioInDeviceName->Text.c_str());

  _DDR->setAudioSlipIn(AudioSlipInEdit->dValue);
  _DDR->setAudioSlipOut(AudioSlipOutEdit->dValue);

  _DDR->setMilliSecondsAudioBlend(AudioBlendEdit->dValue);

  // Video Section
  _DDR->setVideoIn(_PortCount, VideoInCBox->Checked);
  _DDR->setVideoOut(_PortCount, VideoOutCBox->Checked);

  _DDR->setVideoInhibitIn(_PortCount, VideoWriteDiskCBox->Checked);
  _DDR->setVideoInhibitOut(_PortCount, VideoReadDiskCBox->Checked);

  _DDR->setVideoBoardNameOut(VideoOutDeviceName->Text.c_str());
  _DDR->setVideoBoardNameOut(VideoInDeviceName->Text.c_str());

  // Now write the connect parameters
  if (ConnectGroupBox->ItemIndex == 1)
      {
        _DDR->setSerialPort(_PortCount, -1);
        _DDR->setSocketPort(_PortCount, DDRSocketSEdit->Value);
      }
    else
      {
        _DDR->setSerialPort(_PortCount, DDRSerialPortSEdit->Value);
        _DDR->setSocketPort(_PortCount, -1);
      }
}

void __fastcall TOptionsFrame::ConnectGroupBoxClick(TObject *Sender)
{
   bool bV = (ConnectGroupBox->ItemIndex == 1);
   DDRNetworkPanel->Visible = bV;
   DDRSerial232Panel->Visible = !bV;
}

//---------------------------------------------------------------------------
void __fastcall TOptionsFrame::OverlayImageMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   _bMoveOverlay = true;
   OverlayImageMouseMove(Sender, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TOptionsFrame::OverlayImageMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   _bMoveOverlay = false;
}
//---------------------------------------------------------------------------


void TOptionsFrame::DrawOverlay(int X, int Y)
{
  // Do nothing if nothing wanted
  if (!_bOverlayUpdate) return;

  // Restore everything, then redraw if necessary
  OverlayImage->Picture->Bitmap->Assign(_WholeOverlayImage);

  if (OverlayCBox->Checked)
    {
       int is = OverlaySizeSEdit->Value;
       float fs = OverlayImage->Width/1920.0;

       TRect ARect(X, Y, X + fs*11*is*8, Y + fs*is*12);

       Graphics::TBitmap* Image = new Graphics::TBitmap;
       TimeCodeImageList->GetBitmap(OverlayFGColorGroup->ItemIndex, Image);
       Image->Transparent = (OverlayBGColorGroup->ItemIndex == 1);

       OverlayImage->Canvas->StretchDraw(ARect, Image);

       // Clean up
       delete Image;

       // Save the positions if we need to change position
       _OldOvlyX = X;
       _OldOvlyY = Y;
    }
}

void __fastcall TOptionsFrame::OverlayImageMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  if (_bMoveOverlay)
    {
        Modified = true;
        DrawOverlay(X, Y);
    }
}
//---------------------------------------------------------------------------

void __fastcall TOptionsFrame::RedrawOverlay(TObject *Sender)
{
   Modified = true;
   DrawOverlay(_OldOvlyX, _OldOvlyY);
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------


void __fastcall TOptionsFrame::OverlayCenterButtonClick(TObject *Sender)
{
   int NewOvlyX = (1920 - OverlaySizeSEdit->Value*11*8)/1920.*OverlayImage->Width/2;
   if (NewOvlyX == _OldOvlyX) return;
   DrawOverlay(NewOvlyX, _OldOvlyY);
   Modified = true;
}
//---------------------------------------------------------------------------



