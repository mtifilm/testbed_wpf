//---------------------------------------------------------------------------

#ifndef WaveformUnitH
#define WaveformUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TWaveformForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TLabel *GainLabel;
        TTrackBar *GainTrackbar;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Refresh(TObject *Sender);
        void __fastcall FormSize(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TWaveformForm(TComponent* Owner);

   void ClearImage();
   void BltWaveform(HDC bmdc);

};
//---------------------------------------------------------------------------
extern PACKAGE TWaveformForm *WaveformForm;
//---------------------------------------------------------------------------
#endif
