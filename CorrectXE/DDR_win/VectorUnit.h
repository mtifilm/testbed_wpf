//---------------------------------------------------------------------------

#ifndef VectorUnitH
#define VectorUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "MTITrackBar.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TVectorForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TLabel *GainLabel;
        TTrackBar *GainTrackbar;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Refresh(TObject *Sender);
        void __fastcall FormSize(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TVectorForm(TComponent* Owner);

   void ClearImage();
   void BltVectorScope(HDC bmdc);
   
};
//---------------------------------------------------------------------------
extern PACKAGE TVectorForm *VectorForm;
//---------------------------------------------------------------------------
#endif
