//---------------------------------------------------------------------------

#ifndef OptionsUnitH
#define OptionsUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "OptionsFrameUnit.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TParametersForm : public TForm
{
__published:	// IDE-managed Components
        TOptionsFrame *OptionsFrame;
        TBitBtn *OkButton;
        TBitBtn *CancelButton;
        void __fastcall OkButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TParametersForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TParametersForm *ParametersForm;
//---------------------------------------------------------------------------
#endif
