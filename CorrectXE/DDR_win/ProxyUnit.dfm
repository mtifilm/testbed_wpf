object ProxyForm: TProxyForm
  Left = 100
  Top = 100
  Width = 552
  Height = 580
  Caption = 'Proxy'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = CreateForm
  OnDestroy = DestroyForm
  OnPaint = Refresh
  OnResize = FormSize
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 16
    Top = 0
    Width = 416
    Height = 416
  end
  object MainMenu1: TMainMenu
    Left = 40
    Top = 16
    object ViewMenu: TMenuItem
      Caption = 'View'
      object FieldsSubMenu: TMenuItem
        Caption = 'Fields'
        OnClick = FieldsSubMenuClick
        object Field1MenuItem: TMenuItem
          Caption = 'Field 1'
          RadioItem = True
          OnClick = Field1MenuItemClick
        end
        object Field2MenuItem: TMenuItem
          Caption = 'Field 2'
          RadioItem = True
          OnClick = Field2MenuItemClick
        end
        object BothFieldsMenuItem: TMenuItem
          Caption = 'Both'
          RadioItem = True
          OnClick = BothFieldsMenuItemClick
        end
      end
      object ZonesSubMenu: TMenuItem
        Caption = 'Zones'
        OnClick = ZonesSubMenuClick
        object SafeActionMenuItem: TMenuItem
          Caption = 'Safe Action'
          OnClick = SafeActionMenuItemClick
        end
        object SafeTitleMenuItem: TMenuItem
          Caption = 'Safe Title'
          OnClick = SafeTitleMenuItemClick
        end
      end
    end
  end
end
