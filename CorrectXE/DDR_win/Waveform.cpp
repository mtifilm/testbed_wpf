//
// implementation file for class CWaveform
//
#include "Waveform.h"
#include "PixelEngine.h"

////////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CWaveform::
CWaveform()
{
   // allocate a pixel engine
   pxlEng = new CPixelEngine;

   // clear the dimensions
   waveformWdth = 0;
   waveformHght = 0;
}

/////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CWaveform::
~CWaveform()
{
   delete pxlEng;
}

/////////////////////////////////////////////////////////////////////
//
//    C O N V E R T
//
void CWaveform::convert( unsigned int *srcbuf,
                         int srcWdth,
                         int srcHght,
                         int srcPitch,
                         unsigned int *dstbuf,
                         int dstwdth,
                         int dsthght,
                         int dstpitch,
                         double gain)
{
   // save the dimensions
   waveformWdth = dstwdth;
   waveformHght = dsthght;

   // the y-coord of the last line of the waveform
   baseline = dsthght*.95;

   // we have 4 vertical zones: R, G, B, and Y
   begRed   = 0;
   endRed   = waveformWdth/4 - 1;
   begGreen = waveformWdth/4;
   endGreen = waveformWdth/2 - 1;
   begBlue  = waveformWdth/2;
   endBlue  = 3*waveformWdth/4 - 1;
   begY     = 3*waveformWdth/4;
   endY     = waveformWdth - 1;

   // horz scale factor maps the width of the source
   // RGB proxy to the width of each waveform zone
   double xscale = (double)(dstwdth/4)/(double)srcWdth;

   // vert scale factor maps the 0-255 amplitude to
   // the height of each waveform zone
   double yscale = (double)(dsthght)*.9/(double)256.;

   // hook the pixel engine to the dest frame buffer
   pxlEng->setFrameBuffer(dstpitch>>2, dsthght, dstbuf);

   // clear the dest buffer
   pxlEng->setBGColor(0x00000000);
   pxlEng->clearFrameBuffer();

   // based on the gain, calculate the plotting color
   int color = 63 + gain*192;
   color = (color<<16)+(color<<8)+(color);
   pxlEng->setFGColor(color);

   // get ready to generate waveforms
   unsigned char *src;
   unsigned int val;
   int srcadv = 2*srcPitch - 4*srcWdth;
   int prey, cury;

   // RED zone:
   // for each source row do
   //    for each source column do
   //       calculate coords
   //       plot point
   //    next column
   // next row
   //
   src = (unsigned char *)srcbuf;
   for (int i=0;i<srcHght;i+=2) {

      prey = -1000000;

      for (int j=0;j<srcWdth;j+=2) {

         val = src[2];
         cury = baseline-val*yscale;
         drawPoint(begRed+j*xscale,prey,cury);
         prey = cury;

         src += 8;

      }

      src += srcadv;
   }

   // GREEN zone:
   // for each source row do
   //    for each source column do
   //       calculate coords
   //       plot point
   //    next column
   // next row
   //
   src = (unsigned char *)srcbuf;
   for (int i=0;i<srcHght;i+=2) {

      prey = -1000000;

      for (int j=0;j<srcWdth;j+=2) {

         val = src[1];
         cury = baseline-val*yscale;
         drawPoint(begGreen+j*xscale,prey,cury);
         prey = cury;

         src += 8;

      }

      src += srcadv;
   }

   // BLUE zone:
   // for each source row do
   //    for each source column do
   //       calculate coords
   //       plot point
   //    next column
   // next row
   //
   src = (unsigned char *)srcbuf;
   for (int i=0;i<srcHght;i+=2) {

      prey = -1000000;

      for (int j=0;j<srcWdth;j+=2) {

         val = src[0];
         cury = baseline-val*yscale;
         drawPoint(begBlue+j*xscale,prey,cury);
         prey = cury;

         src += 8;

      }

      src += srcadv;
   }

   // Y zone:
   // for each source row do
   //    for each source column do
   //       calculate coords
   //       plot point
   //    next column
   // next row
   //
   yscale /= 3.;
   src = (unsigned char *)srcbuf;
   for (int i=0;i<srcHght;i+=2) {

      prey = -1000000;

      for (int j=0;j<srcWdth;j+=2) {

         val = src[0]+src[1]+src[2];
         cury = baseline-val*yscale;
         drawPoint(begY+j*xscale,prey,cury);
         prey = cury;

         src += 8;

      }

      src += srcadv;
   }

   // draw the axes and UV compass
   drawReticle();
}

#define THRSH 10
void CWaveform::drawPoint(int x, int prey, int cury)
{
   // if the change in y from the previous point
   // is significant, go to the point & draw a dot
   int dely = cury - prey;
   if ((dely < -THRSH)||(dely > THRSH)) {
      pxlEng->moveTo(x, cury);
      pxlEng->drawDot();
   }
   // else draw a line to the new position
   else
      pxlEng->lineTo(x, cury);
}

void CWaveform::drawReticle()
{
   // the plotting color
   pxlEng->setFGColor(0x00808080);

   // draw bounds between zones
   pxlEng->moveTo(begGreen,0);
   pxlEng->lineTo(begGreen,waveformHght);
   pxlEng->moveTo(begBlue,0);
   pxlEng->lineTo(begBlue,waveformHght);
   pxlEng->moveTo(begY,0);
   pxlEng->lineTo(begY,waveformHght);

   // label the zones
   pxlEng->drawText(begRed+3  , 3, "R");
   pxlEng->drawText(begGreen+3, 3, "G");
   pxlEng->drawText(begBlue+3 , 3, "B");
   pxlEng->drawText(begY+3    , 3, "Y");
}

