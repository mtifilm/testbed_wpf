//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DDRUnit.h"
#include "ProxyUnit.h"
#include "VectorUnit.h"
#include "WaveformUnit.h"
#include "BinManagerGUIUnit.h"
#include "BinMgrGuiInterface.h"
#include "OptionsUnit.h"
#include "Clip3.h"
#include "err_clip.h"
#include "BinManager.h"
#include "AudioFormat3.h"
#include "ImageFormat3.h"
#include "Convert.h"
#include "VectorScope.h"
#include "Waveform.h"
#include "UnitDDRControl.h"
#include "UnitVTRControl.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "OptionsFrameUnit"
#pragma link "HistoryMenu"
#pragma link "MTIUNIT"
#pragma link "historymenu"
#pragma resource "*.dfm"
TMainForm  *MainForm;
//---------------------------------------------------------------------------

// Encryption for "CONTROL-DDR"
MTI_UINT32 FEATURE_CONTROL_DDR[] = {16, 0x982565c, 0x0cff903f, 0x4a1792cc, 0x1bb9ff44, 0xc9b85ffb};

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TMTIForm(Owner)
{
  // clear create flag
  created = false;

  // set defaults for proxy "fields" and "zones"
  // normal display of both fields
  fieldsMask = FIELD_DISPLAY_BOTH;

  // neither "safe action" nor "safe title"
  zonesMask  = 0;

  // Local information
  _PortCount = 0;
  _ImgFormatType = IF_TYPE_INVALID;

  CIniFile *ini = CPMPOpenLocalMachineIniFile();
  if (ini == NULL)
    {
      theError.set(-1,"Failed to open Local Machine ini");
      _MTIErrorDialog(Handle, theError.getFmtError());
      return;
    }

  string Section = "RemoteEdit";
  string sFileName = ini->ReadFileName(Section,"EmulatorConfig",
					     "$(CPMP_LOCAL_DIR)emulator.cfg");

  if (!DeleteIniFile(ini))
    {
      theError.set(-1,"Failed to close Local Machine ini");
      _MTIErrorDialog(Handle, theError.getFmtError());
      return;
    }

  if (_DDR.readCfg(sFileName, -1) != 0)
    {
      theError.set(-1,"Error reading VTR configuration file");
      _MTIErrorDialog(Handle, theError.getFmtError());
      return;
    }

   HistoryMenu->RebuildMenu();

   // Load the history
   srcImageFormat = NULL;
   while (!HistoryMenu->HistoryList.empty())
    {
       if (OpenClip(HistoryMenu->HistoryList[0])) break;
       HistoryMenu->HistoryList.Delete(0);
    }

   // Load the icon
   Application->Icon->Handle = LoadImage(HInstance,
                         "MainIcon",
                         IMAGE_ICON,
                         32,32,
                         LR_DEFAULTSIZE);
   
   // Startup all the programs
   StartProgram();
}


//---------------------------------------------------------------------------
void __fastcall TMainForm::BrowserButtonClick(TObject *Sender)
{
  // 3rd argument makes Bin Manager fsNormal
  ShowBinManagerGUI(false, BM_MODE_NORMAL);
  if (BinManagerForm->ShowModal() == mrOk)
    {
      StringList sl = BinManagerForm->SelectedClipNames();
      if (!sl.empty())
        {
           OpenClip(sl[0]);
        }
    }
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::ParametersButtonClick(TObject *Sender)
{
   ParametersForm->OptionsFrame->SetEmulator(_DDR);
   ParametersForm->ShowModal();
   ProxyCBox->Checked = _DDR.getProxyIn(_PortCount);
}
//---------------------------------------------------------------------------
//
// TBD Temporary until Advance Record and Batch Record are similar
CClip *OpenClipByName(const string &Name);

void TMainForm::UpdateForm(void)
{
  if (!OpenClip(ClipEdit->Text.c_str()))
     _MTIErrorDialog(Handle, theError.getFmtError());

}

bool TMainForm::OpenClip(const string &strClipName)
// This takes the currently displayed clip and updates all the parameters
{
  CBinManager binMgr;
  string strBinName, strClipNamePart;

  StartInButton->Enabled = false;
  StartOutButton->Enabled = false;

  auto cpClip = OpenClipByName(strClipName);
  if (cpClip == NULL)
    {
      theError.set(-1, (string)"Could not open clip " + strClipName);
      return false;
    }

  //
  // Do the audio information
  //
  AudioCBox->Enabled = (cpClip->getAudioTrackCount() > 0);
  AudioCBox->Checked = AudioCBox->Enabled;

  ProxyCBox->Enabled = (cpClip->getVideoProxyCount() > 1);
  ProxyCBox->Checked = ProxyCBox->Enabled;

  PTimecode ptc(cpClip->getInTimecode());
  InTCLabel->Caption = ((string)ptc).c_str();

  ptc = cpClip->getOutTimecode();
  OutTCLabel->Caption = ((string)ptc).c_str();

   _ImgFormatType = cpClip->getImageFormat(0)->getImageFormatType();

  //
  // we'll need this to set up our proxy, waveform, & vectorscope displays
  //
  srcImageFormat = cpClip->getImageFormat(VIDEO_SELECT_NORMAL);
  //
  if ((ProxyForm != NULL)&&(ProxyForm->Visible)) {
     SetProxyImageFormat();
     ProxyForm->ClearImage(zonesMask);
  }

  // reset the fields mask to display both fields
  fieldsMask = FIELD_DISPLAY_BOTH;

  // neither "safe action" nor "safe title"
  zonesMask  = 0;

  //
  // Rest come from clip info
  //
  int iRet = binMgr.splitClipFileName (strClipName, strBinName, strClipNamePart);
  if (iRet)
    {
      MTIostringstream os;
      os << "Error spliting the clip "
         << iRet;
      theError.set(CLIP_ERROR_BAD_CLIP_FILENAME,os);
      return false;
    }

  CClipInitInfo *clipInitInfo;
  clipInitInfo = binMgr.openClipOrClipIni(strBinName, strClipNamePart, &iRet);
  if (iRet == 0)
    {

      // Image Format Type Name
      FormatLabel->Caption = clipInitInfo->getImageFormatName().c_str();
      DescLabel->Caption = clipInitInfo->getDescription().c_str();
      ptc = clipInitInfo->getDurationTimecode();
      DurLabel->Caption = ((string)ptc).c_str();
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
    }
  else
   {
     MTIostringstream os;
     os << "Failed to open clip " << strClipName  << " Status = " << iRet;
     theError.set(-1,os);
     return false;
   }

  HistoryMenu->Add(strClipName);
  ClipEdit->Text = strClipName.c_str();

  StartInButton->Enabled = true;
  StartOutButton->Enabled = true;

  return true;
}

bool TMainForm::StartTransfer(void)
{
  string strClipName = ClipEdit->Text.c_str();
  MTIostringstream os;

  if (_DDR.setClipName(_PortCount, strClipName) != 0)
    {
      theError.set(-1,(string)"DDR Start Error: Could not set clip name " + strClipName);
      return false;
    }

  int iRet = _DDR.prepareTransfer(MODE_SINGLE_TRANSFER);
  switch (iRet)
    {
       case ERR_EMULATOR_NO_VIDEO_DETECTED:
          theError.set(iRet,"DDR Start error: No video signal detected");
          return false;

       case ERR_EMULATOR_WRONG_VIDEO_DETECTED:
         {
           os.str("");
           os << "DDR Start error: Clip format and video format mismatch" << endl;
           os << "Clip format: " << CImageInfo::queryImageFormatTypeName(_ImgFormatType) << endl;
           EImageFormatTypeList *list = _DDR.getImageFormatTypeList();
           for (unsigned i = 0; i < list->size(); i++)
             {
               os << "Detected format: " << CImageInfo::queryImageFormatTypeName (list->at(i)) << endl;
             }
           theError.set(iRet, os);
         }
         return false;

       default:
          if (iRet != 0)
            {
               theError.set(iRet,"DDR Start error: Generic Begin transfer error");
               return false;
            }
     }

  iRet = _DDR.beginTransfer();
  if (iRet != 0)
    {
       theError.set(iRet,"DDR Start error: Generic Begin transfer error");
       return false;
    }

   return true;

}

bool TMainForm::StopTransfer(void)
{
   _DDR.endTransfer();
   return true;
}

void __fastcall TMainForm::StartButtonClick(TObject *Sender)
{
 if (ClipEdit->Text == "")
  {
    _MTIErrorDialog(Handle, "Please open a clip");
    return;
  }

 CBusyCursor BusyCursor(true);

 _DDR.setVideoIn(_PortCount,StartInButton == Sender);
 _DDR.setVideoOut(_PortCount, StartOutButton == Sender);

 // Set the direction information
 if (StartInButton == Sender)
  {
    Caption = "MTI Digital Disk Recorder - Transfer to Computer";
    _DDR.setAudioIn(_PortCount, AudioCBox->Checked);
    _DDR.setAudioOut(_PortCount, false);
    _DDR.setProxyIn(_PortCount, ProxyCBox->Checked);

  }
 else
  {
    Caption = "MTI Digital Disk Recorder - Transfer from Computer";
    _DDR.setAudioOut(_PortCount, AudioCBox->Checked);
    _DDR.setAudioIn(_PortCount, false);
    _DDR.setProxyIn(_PortCount, false);
  }

 if (!StartTransfer())
   {
      _MTIErrorDialog(Handle, theError.getFmtError());
      return;
   }
 // Setup the run panel
 RunPanel->Visible = true;
 OpenPanel->Visible = false;

 ClipNameLabel->Caption = ClipEdit->Text;
 StatusMemo->Clear();
 StatusMemo->Lines->Add(Caption);
 StatusMemo->Lines->Add((String)"  Clip: " + ClipNameLabel->Caption);
 StatusMemo->Lines->Add((String)"    In:" + InTCLabel->Caption);
 StatusMemo->Lines->Add((String)"   Out:" + OutTCLabel->Caption);
 StatusMemo->Lines->Add((String)"   Dur:" + DurLabel->Caption);
 StatusMemo->Lines->Add((String)"Format:" + FormatLabel->Caption);
 StatusMemo->Lines->Add((String)"  Desc:" + DescLabel->Caption);
 StatusMemo->Lines->Add("");
 if (_DDR.getSerialPort(_PortCount) == -1)
    StatusMemo->Lines->Add((String)"Connection is over socket " + _DDR.getSocketPort(_PortCount));
 else
    StatusMemo->Lines->Add((String)"Connection is over serial port " + _DDR.getSerialPort(_PortCount));


}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StopButtonClick(TObject *Sender)
{
 CBusyCursor BusyCursor(true);

 if (!StopTransfer()) _MTIErrorDialog(Handle, theError.getFmtError());
 OpenPanel->Visible = true;
 RunPanel->Visible = false;

 Caption = "MTI Digital Disk Recorder";
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Exit1Click(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::HistoryMenuHistoryClick(AnsiString Str)
{
  if (!OpenClip(Str.c_str()))
   {
     _MTIErrorDialog(Handle, theError.getFmtError());
     return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
   // turn off the timer (this fixes the CLOSE hangup)
   MainForm->Timer1->Enabled = false;

   // save the form properties
   SaveAllProperties();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ProxyCBoxClick(TObject *Sender)
{
  _DDR.setProxyIn(_PortCount, ProxyCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ProxyMenuClick(TObject *Sender)
{
   if (srcImageFormat != NULL) {

      if (!ProxyMenuItem->Checked) {

         SetProxyImageFormat();
         ProxyForm->Show();
         ProxyForm->ClearImage(zonesMask);
         ProxyMenuItem->Checked = true;

      }
      else {

         ProxyForm->Hide();
         ProxyMenuItem->Checked = false;
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::VectorMenuClick(TObject *Sender)
{
   if (srcImageFormat != NULL) {

      if (!VectorScopeMenuItem->Checked) {

         SetProxyImageFormat();
         VectorForm->Show();
         VectorForm->ClearImage();
         VectorScopeMenuItem->Checked = true;
      }
      else {

         VectorForm->Hide();
         VectorScopeMenuItem->Checked = false;
      }
   }
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::WaveformMenuClick(TObject *Sender)
{
   if (srcImageFormat != NULL) {

      if (!WaveformMenuItem->Checked) {

         SetProxyImageFormat();
         WaveformForm->Show();
         WaveformForm->ClearImage();
         WaveformMenuItem->Checked = true;
      }
      else {

         WaveformForm->Hide();
         WaveformMenuItem->Checked = false;
      }
   }
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::CreateForm(TObject *Sender)
{

#ifdef DS1961S_DONGLE
   // The main featue is checked out here, if a failure, we set the
   // _StartupFailedMessage message to be non-null
   // Later this is checked and we die if necessary.
   _StartupFailedMessage = "";   // Not really necessary
   if (!License.CheckOut(FEATURE_CONTROL_DDR))
     {
        _StartupFailedMessage = "DDR is not licensed on this computer\nPlease contact MTI-Film for more information\n";
        _StartupFailedMessage += License.LastErrorMessage();
     }
   else
     {
         SetResolutionPrefix("CONTROL-DDR");
     }
#endif


   // allocate a converter
   cnvt = new CConvert;

   // allocate a vectorscope generator
   vctr = new CVectorScope;

   // allocate a waveform generator
   wave = new CWaveform;

   // allocate a PROXY bitmap
   proxyBM = new CMTIBitmap;

   // create proxies at max width
   proxyWdth = MAX_PROXY_WIDTH;

  // now initialize the PROXY bitmap
   proxyBM->initBitmap(Handle, MAX_PROXY_WIDTH, MAX_PROXY_WIDTH);

   // allocate a VECTORPROXY bitmap
   vectorProxyBM = new unsigned int[VECTORPROXY_WIDTH*VECTORPROXY_WIDTH];

   // allocate a VECTORSCOPE bitmap
   vectorBM = new CMTIBitmap;

   // create vectorscope at max size
   vectorWdth = MAX_VECTOR_WIDTH;

   // now initialize the VECTORSCOPE bitmap
   vectorBM->initBitmap(Handle, MAX_VECTOR_WIDTH, MAX_VECTOR_WIDTH);

   // allocate a VECTORSCOPE bitmap
   waveBM = new CMTIBitmap;

   // create vectorscope at max size
   waveformWdth = MAX_WAVEFORM_WIDTH;

   // now initialize the VECTORSCOPE bitmap
   waveBM->initBitmap(Handle, MAX_WAVEFORM_WIDTH, MAX_WAVEFORM_WIDTH);

   // the form is created
   created = true;
   PostMessage(Handle, CW_START_PROGRAM, 0, 0);

   // the bin manager gui interface for the clip picker
   binMgrGuiInterface = new CBinMgrGUIInterface;

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::DestroyForm(TObject *Sender)
{
   // delete the waeform bitmap
   delete waveBM;

   // delete vector bitmap
   delete vectorBM;

   // delete the vectorproxy bitmap
   delete [] vectorProxyBM;
   
   // delete proxy bitmap
   delete proxyBM;

   // delete the waveform converter
   delete wave;

   // delete the vectorscope converter
   delete vctr;

   // delete the converter
   delete cnvt;
}

//---------------------------------------------------------------------------
void TMainForm::SetProxyImageFormat()
{
   // if there's only one field then
   // the "Fields"  menu is disabled
   ProxyForm->FieldsSubMenu->Enabled = srcImageFormat->getInterlaced();

   // set frame rectangle & dims
   frmRect = srcImageFormat->getActivePictureRect();
   frmWdth = frmRect.right - frmRect.left + 1;
   frmHght = frmRect.bottom - frmRect.top + 1;

   // get this from clip
   pixelAspectRatio = srcImageFormat->getPixelAspectRatio();

   // ...but calculate this for consistency
   frameAspectRatio = ((double)frmWdth*pixelAspectRatio)/(double)frmHght;

   // set the proxy dimensions
   SetProxyDimensions(ProxyForm->Width);

   // set vectorscope dimensions
   SetVectorScopeDimensions(VectorForm->Width);

   // set the waveform dimensions
   SetWaveformDimensions(WaveformForm->Width);
}

//---------------------------------------------------------------------------
void TMainForm::SetProxyDimensions(int wdth)
{
   // the proxy form has been stretched...
   // recalculate its dimensions
   proxyWdth = wdth - 2*BORDER_WIDTH;
   if (proxyWdth < MIN_PROXY_WIDTH)
      proxyWdth = MIN_PROXY_WIDTH;
   if (proxyWdth > MAX_PROXY_WIDTH)
      proxyWdth = MAX_PROXY_WIDTH;
   proxyHght = (int)((double)proxyWdth / frameAspectRatio);

   // reset these from the saved values
   ProxyForm->Image1->Left   = BORDER_WIDTH - 4;
   ProxyForm->Image1->Top    = 0;
   ProxyForm->Image1->Width  = proxyWdth;
   ProxyForm->Image1->Height = proxyHght;
   ProxyForm->Width  = proxyWdth + 2*BORDER_WIDTH;
   ProxyForm->Height = proxyHght + 2*BORDER_WIDTH + HEADER_HEIGHT + 10;
}

//---------------------------------------------------------------------------
int TMainForm::GetFieldsMask()
{
   return(fieldsMask);
}

void TMainForm::SetFieldsMask(int fmsk)
{
   fieldsMask = fmsk;
}

//---------------------------------------------------------------------------
int TMainForm::GetZonesMask()
{
   return(zonesMask);
}

void TMainForm::SetZonesMask(int zmsk)
{
   zonesMask = zmsk;
}

//---------------------------------------------------------------------------
void TMainForm::SetVectorScopeDimensions(int wdth)
{
   // the vectorproxy derives its aspect ratio from the image format
   vectorProxyWdth = VECTORPROXY_WIDTH;
   vectorProxyHght = (int)((double)vectorProxyWdth / frameAspectRatio);

   // the vector form has been stretched...
   // recalculate its dimensions
   vectorWdth = wdth - 2*BORDER_WIDTH;
   if (vectorWdth < MIN_VECTOR_WIDTH)
      vectorWdth = MIN_VECTOR_WIDTH;
   if (vectorWdth > MAX_VECTOR_WIDTH)
      vectorWdth = MAX_VECTOR_WIDTH;
   vectorHght = vectorWdth;

   // now adjust the GUI components to go with new wdth
   VectorForm->Image1->Left   = BORDER_WIDTH;
   VectorForm->Image1->Top    = BORDER_WIDTH;
   VectorForm->Image1->Width  = vectorWdth;
   VectorForm->Image1->Height = vectorHght;

   VectorForm->GainLabel->Left = BORDER_WIDTH;
   VectorForm->GainLabel->Top  = BORDER_WIDTH + vectorHght + 20;

   VectorForm->GainTrackbar->Left = BORDER_WIDTH + 35;
   VectorForm->GainTrackbar->Top  = BORDER_WIDTH + vectorHght + 24;

   int gainWdth = vectorWdth - 35;
   if (gainWdth > 221)
      gainWdth = 221;
   VectorForm->GainTrackbar->Width = gainWdth;

   VectorForm->Width  = vectorWdth + 2*BORDER_WIDTH;
   VectorForm->Height = vectorHght + BORDER_WIDTH + HEADER_HEIGHT + 60;
}

//---------------------------------------------------------------------------
void TMainForm::SetWaveformDimensions(int wdth)
{
   // the waveform form has been stretched...
   // recalculate its dimensions
   waveformWdth = wdth - 2*BORDER_WIDTH;
   if (waveformWdth < MIN_WAVEFORM_WIDTH)
      waveformWdth = MIN_WAVEFORM_WIDTH;
   if (waveformWdth > MAX_WAVEFORM_WIDTH)
      waveformWdth = MAX_WAVEFORM_WIDTH;
   waveformHght = waveformWdth;

   // now adjust the GUI components to go with new wdth
   WaveformForm->Image1->Left   = BORDER_WIDTH;
   WaveformForm->Image1->Top    = BORDER_WIDTH;
   WaveformForm->Image1->Width  = waveformWdth;
   WaveformForm->Image1->Height = waveformHght;

   WaveformForm->GainLabel->Left = BORDER_WIDTH;
   WaveformForm->GainLabel->Top  = BORDER_WIDTH + waveformHght + 20;

   WaveformForm->GainTrackbar->Left = BORDER_WIDTH + 35;
   WaveformForm->GainTrackbar->Top  = BORDER_WIDTH + waveformHght + 24;

   int gainWdth = waveformWdth - 35;
   if (gainWdth > 221)
      gainWdth = 221;
   WaveformForm->GainTrackbar->Width = gainWdth;

   WaveformForm->Width  = waveformWdth + 2*BORDER_WIDTH;
   WaveformForm->Height = waveformHght + BORDER_WIDTH + HEADER_HEIGHT + 60;
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::TimerWorkProc(TObject *Sender)
{
   // note - we can't do any conversions until the dimensions
   // of the Proxy, Vectorscope, and Waveform have been estab-
   // lished and 'SetProxyImageFormat' has been called. This
   // is done for the first time when the Proxy, Vectorscope,
   // or Waveform window is selected from the menu
   //
   if (!created||
      ((ProxyForm==NULL)||(VectorForm==NULL)||(WaveformForm==NULL))||
      ((!ProxyForm->Visible)&&(!VectorForm->Visible)&&(!WaveformForm->Visible)))
      return;

   // get the native frame's fields from the VTR
   MTI_UINT8 **flds = _DDR.ImagePointerRequest();

   if (flds != NULL) {

      // based on the fields mask, fill in a new ptr array
      // with the addresses of the fields we want to display
      MTI_UINT8 *fpts[2];
      fpts[0] = flds[0]; fpts[1] = flds[1];
      if (fieldsMask == FIELD_DISPLAY_1)
         fpts[0] = fpts[1] = flds[0];
      if (fieldsMask == FIELD_DISPLAY_2)
         fpts[0] = fpts[1] = flds[1];

      // convert YUV to RGB in the proxy bitmap
      cnvt->convert( fpts,
                     srcImageFormat,
                     &frmRect,
                     proxyBM->getBitmapFrameBuffer(),
                     proxyWdth,
                     proxyHght,
                     MAX_PROXY_WIDTH*4 );

      // do it again for the vectorproxy bitmap
      // the width of this is always the same
      // irrespective of the vectorscope size
      cnvt->convert( flds,
                     srcImageFormat,
                     &frmRect,
                     vectorProxyBM,
                     vectorProxyWdth,
                     vectorProxyHght,
                     VECTORPROXY_WIDTH*4 );

      // release as soon as possible, so emulator can keep moving.
      _DDR.ImagePointerRelease();

      // if Proxy form is visible
      //    blit the Proxy to the proxy form
      //    and draw the "safe" zones if enabled
      if ((ProxyForm != NULL)&&(ProxyForm->Visible))
         ProxyForm->BltProxy(proxyBM->getDCHandle(),zonesMask);

      // if VectorScope form is visible
      //    generate VectorScope bitmap from Proxy bitmap
      //    blit the VectorScope to the vectorscope form
      if ((VectorForm != NULL)&&(VectorForm->Visible)) {

         // generate the vectorscope display in the
         // vector bitmap, then blit it to the panel
         vctr->convert( vectorProxyBM,
                        vectorProxyWdth,
                        vectorProxyHght,
                        VECTORPROXY_WIDTH*4,
                        vectorBM->getBitmapFrameBuffer(),
                        vectorWdth,
                        vectorHght,
                        MAX_VECTOR_WIDTH*4,
                        (double)VectorForm->GainTrackbar->Position/10001.);

         VectorForm->BltVectorScope(vectorBM->getDCHandle());

      }

      // if Waveform is visible
      //    generate Waveform bitmap from Proxy bitmap
      //    blt the Waveform bitmap to the waveform form
      if ((WaveformForm != NULL)&&(WaveformForm->Visible)) {

         //
         //   V E C T O R P R O X Y   T E S T   P A T T E R N - R, G, & B horz shaded stripes
         //
#if 0
         unsigned int *d = vectorProxyBM;
         for (int i=0;i<vectorProxyHght/3;i++) { // RED stripe
            unsigned int val = ((double)i*3/(double)vectorProxyHght)*(double)(0xff);
            val = val << 16;
            for (int j=0;j<vectorProxyWdth;j++)
               *d++ = val;
         }
         for (int i=0;i<vectorProxyHght/3;i++) { // GREEN stripe
            unsigned int val = ((double)i*3/(double)vectorProxyHght)*(double)(0xff);
            val = val<<8;
            for (int j=0;j<vectorProxyWdth;j++)
            *d++ = val;
         }
         for (int i=0;i<vectorProxyHght/3;i++) { // BLUE stripe
            unsigned int val = ((double)i*3/(double)vectorProxyHght)*(double)(0xff);
            for (int j=0;j<vectorProxyWdth;j++)
               *d++ = val;
         }
#endif
         // generate the waveform display in the
         // waveform bitmap, then blit it to the panel
         wave->convert( vectorProxyBM,
                        vectorProxyWdth,
                        vectorProxyHght,
                        VECTORPROXY_WIDTH*4,
                        waveBM->getBitmapFrameBuffer(),
                        waveformWdth,
                        waveformHght,
                        MAX_WAVEFORM_WIDTH*4,
                        (double)WaveformForm->GainTrackbar->Position/10001.);

         WaveformForm->BltWaveform(waveBM->getDCHandle());

      }

   } // end processing FLDS passed by emulator

}

void __fastcall TMainForm::DDR1Click(TObject *Sender)
{
  FormDDRControl->Show();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::VTR1Click(TObject *Sender)
{
  FormVTRControl->Show();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{

  
  switch (Key)
   {
    case MTK_F1:
      if (Shift.Contains (ssShift))
       {
Beep();
        // make DDR control window have focus
        FormDDRControl->SetFocus();
        Key = 0;
       }
    break;
    case MTK_F2:
      if (Shift.Contains (ssShift))
       {
Beep();
        // make VTR control window have focus
        FormVTRControl->SetFocus();
        Key = 0;
       }
    break;
    case MTK_F7:
      if (Shift.Contains (ssCtrl))
       {
Beep();
       // SerialPort->PlayBack();
        Key = 0;
       }
    break;
    case MTK_LEFT:
      if (Shift.Contains (ssAlt))
       {
Beep();
      //  int iNewPosition = AudioSlipScrollBar->Position-1;
      //  ScrollBarScroll (AudioSlipScrollBar, scTrack, iNewPosition);
        Key = 0;
       }
    break;
    case MTK_RIGHT:
      if (Shift.Contains (ssAlt))
       {
Beep();
     //   int iNewPosition = AudioSlipScrollBar->Position+1;
    //    ScrollBarScroll (AudioSlipScrollBar, scTrack, iNewPosition);
        Key = 0;
       }
    break;
    case MTK_E:
   //   SerialPort->Jog(-1);
      if (Shift.Contains (ssShift))
    //    SerialPort->Rewind();
      Key = 0;
    break;
    case MTK_R:
   //   SerialPort->Jog(1);
      if (Shift.Contains (ssShift))
   //     SerialPort->FastForward();
      Key = 0;
    break;
    case MTK_C:
    //  SerialPort->Stop();
Beep();
      Key = 0;
    break;
    case MTK_V:
  //    SerialPort->Play();
Beep();
      Key = 0;
    break;
    case MTK_X:
  //    SerialPort->PlayBack();
Beep();
      Key = 0;
    break;
   }


}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//
    void TMainForm::StartForm(void)
{
#ifdef DS1961S_DONGLE
//  This checks to see if the navigator is licensed
   if (_StartupFailedMessage != "")
     {
        _MTIErrorDialog(Handle, _StartupFailedMessage);
        exit(0);
     }

#endif
}

