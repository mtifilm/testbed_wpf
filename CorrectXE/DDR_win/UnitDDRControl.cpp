//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitDDRControl.h"
#include "CallSerCom.h"
#include "UnitVTRControl.h"
#include "MTIKeyDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TFormDDRControl *FormDDRControl;
//---------------------------------------------------------------------------
__fastcall TFormDDRControl::TFormDDRControl(TComponent* Owner)
    : TForm(Owner)
{
  SerialPort = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Button1Click(TObject *Sender)
{
  SerialPort->SetNetName (EditHost->Text.c_str());
  SerialPort->SetSocket (atoi(EditSocket->Text.c_str()));
  SerialPort->Open();
}
//---------------------------------------------------------------------------



void __fastcall TFormDDRControl::FormCreate(TObject *Sender)
{
  SerialPort = new CCallSerCom;
  SerialPort->SetMaxSpeed (3.);

  // set the audio slip control to medium
  mediumcontrol1Click(NULL);

  // clear the audio slip
  fAudioSlipCenter = 0.;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::FormDestroy(TObject *Sender)
{
  delete SerialPort;
  SerialPort = 0;
}
//---------------------------------------------------------------------------


void __fastcall TFormDDRControl::Button3Click(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Button2Click(TObject *Sender)
{
  SerialPort->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Timer1Timer(TObject *Sender)
{
  char caTime[16];
  SerialPort->TimeSense (caTime);

  LabelTimecode->Caption = caTime;

  if (SerialPort->IsConnected())
   {
    Button1->Enabled = false;
    Button2->Enabled = true;
   }
  else
   {
    Button1->Enabled = true;
    Button2->Enabled = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::navStopButtonClick(TObject *Sender)
{
  SerialPort->Stop();        
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::navPlayReverseButtonClick(TObject *Sender)
{
  SerialPort->PlayBack();        
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::navPlayButtonClick(TObject *Sender)
{
  SerialPort->Play();
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::navRewindButtonClick(TObject *Sender)
{
  SerialPort->Rewind();        
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::navFastForwardButtonClick(TObject *Sender)
{
  SerialPort->FastForward();        
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  // Process Key events

  
  switch (Key)
   {
    case MTK_F1:
      if (Shift.Contains (ssShift))
       {
Beep();
        // make DDR control window have focus
        FormDDRControl->SetFocus();
        Key = 0;
       }
    break;
    case MTK_F2:
      if (Shift.Contains (ssShift))
       {
Beep();
        // make VTR control window have focus
        FormVTRControl->SetFocus();
        Key = 0;
       }
    break;
    case MTK_F7:
      if (Shift.Contains (ssCtrl))
       {
Beep();
        SerialPort->PlayBack();
        Key = 0;
       }
    break;
    case MTK_LEFT:
      if (Shift.Contains (ssAlt))
       {
Beep();
        int iNewPosition = AudioSlipScrollBar->Position-1;
        ScrollBarScroll (AudioSlipScrollBar, scTrack, iNewPosition);
        Key = 0;
       }
    break;
    case MTK_RIGHT:
      if (Shift.Contains (ssAlt))
       {
Beep();
        int iNewPosition = AudioSlipScrollBar->Position+1;
        ScrollBarScroll (AudioSlipScrollBar, scTrack, iNewPosition);
        Key = 0;
       }
    break;
    case MTK_E:
   //   SerialPort->Jog(-1);
      if (Shift.Contains (ssShift))
        SerialPort->Rewind();
      Key = 0;
    break;
    case MTK_R:
   //   SerialPort->Jog(1);
      if (Shift.Contains (ssShift))
        SerialPort->FastForward();
      Key = 0;
    break;
    case MTK_C:
      SerialPort->Stop();
Beep();
      Key = 0;
    break;
    case MTK_V:
      SerialPort->Play();
Beep();
      Key = 0;
    break;
    case MTK_X:
      SerialPort->PlayBack();
Beep();
      Key = 0;
    break;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::ScrollBarScroll(TObject *Sender,
      TScrollCode ScrollCode, int &ScrollPos)
{
  TScrollBar *sbp = (TScrollBar *)Sender;

  sbp->Position = ScrollPos;

}
//---------------------------------------------------------------------------

float TFormDDRControl::getAudioSlip ()
{

  float fAudioSlip = ((float)AudioSlipScrollBar->Position /
                (float)AudioSlipScrollBar->Max) *
                (float)iAudioSlipFrameRange + fAudioSlipCenter;

  return fAudioSlip;
}

void TFormDDRControl::setAudioPosition (int iNewFrame)
{
  // set the position so that the AudioSlip and AudioSlipCenter are maintained

  float fAudioSlip = getAudioSlip ();
  iAudioSlipFrameRange = iNewFrame;
  AudioSlipScrollBar->Position = (((fAudioSlip - fAudioSlipCenter) /
      (float)iAudioSlipFrameRange) * (float)AudioSlipScrollBar->Max)  + .5;
}


void __fastcall TFormDDRControl::AudioSlipButtonClick(TObject *Sender)
{
  int iNewPosition = 0;
  ScrollBarScroll (AudioSlipScrollBar, scTrack, iNewPosition);
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::finecontrol1Click(TObject *Sender)
{
  setAudioPosition (20);   // twenty frames
  finecontrol1->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::mediumcontrol1Click(TObject *Sender)
{
  setAudioPosition (50);  // 50 frames
  mediumcontrol1->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::coarsecontrol1Click(TObject *Sender)
{
  setAudioPosition (200);  // 200 frames
  coarsecontrol1->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::verycoarsecontrol1Click(TObject *Sender)
{
  setAudioPosition (1500);  // 1500 frames
  verycoarsecontrol1->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Returntocenter1Click(TObject *Sender)
{
  fAudioSlipCenter = 0.;
  AudioSlipButtonClick (AudioSlipButton);
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Returntozero1Click(TObject *Sender)
{
  AudioSlipButtonClick (AudioSlipButton);
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::Resetcenter1Click(TObject *Sender)
{
  fAudioSlipCenter = getAudioSlip ();
  AudioSlipButtonClick (AudioSlipButton);
}
//---------------------------------------------------------------------------

void __fastcall TFormDDRControl::AudioSlipScrollBarChange(TObject *Sender)
{
  char caPosition[16];
  sprintf (caPosition, "%.2f", getAudioSlip());
  AudioSlipValueLabel->Caption = caPosition;
}
//---------------------------------------------------------------------------


void __fastcall TFormDDRControl::RecordButtonMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if (Shift.Contains (ssShift))
   {
    SerialPort->Record();
   }
  else
   {
    _MTIWarningDialog (Handle, "You must use SHIFT+Click to record");
   }
}
//---------------------------------------------------------------------------

