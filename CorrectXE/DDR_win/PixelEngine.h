#include <memory.h>
#ifndef CPXLENG
#define CPXLENG

// PIXEL-ENGINE class for graphical operations on RGBA frame-buffers  

class CPixelEngine
{

protected:

   unsigned int 
      *frameBuffer,
      *curaddr;

   int
      dwide,
      dhigh,
      dpitch,

      curx,
      cury;

   unsigned char linePattern;

   unsigned int
      fgndColor,
      bgndColor;

public:

   CPixelEngine();

   ~CPixelEngine();

   int getFrameBufferSize() const;
   void setFrameBuffer(int wdth, int hght, unsigned int *frmbuf);
   unsigned int *getFrameBuffer() const;

   void setLinePattern(unsigned char pat);

#define COLOR_RED   0x00ff0000
#define COLOR_GREEN 0x0000ff00
#define COLOR_BLUE  0x000000ff
#define COLOR_BLACK 0x00000000
#define COLOR_WHITE 0x00ffffff
   void setFGColor(unsigned int);
   void setBGColor(unsigned int);

   void drawRectangle(int l,int b, int r, int t);
   void clearFrameBuffer();
   void jogFrameBufferLeft();
   void moveTo(int x, int y);
   void lineTo(int x, int y);
   void arcTo(int x, int y, double xctr, double yctr);
   void drawDot();
   void drawText(int x, int y, char *txt);
};

#endif

