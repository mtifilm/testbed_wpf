//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DDRUnit.h"
#include "ProxyUnit.h"
#include <math.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TProxyForm *ProxyForm;
//---------------------------------------------------------------------------
__fastcall TProxyForm::TProxyForm(TComponent* Owner)
        : TForm(Owner)
{

}

//---------------------------------------------------------------------------

void __fastcall TProxyForm::CreateForm(TObject *Sender)
{
   whitePen = CreatePen(PS_DOT,0,(COLORREF)0x00ffffff);
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::DestroyForm(TObject *Sender)
{
   // all we have to do
   DeleteObject(whitePen);

   // remember, OnResize can get called AFTER OnDestroy is called
   OnResize = NULL;
}
//---------------------------------------------------------------------------

void TProxyForm::DrawZonesOntoScreen(HDC scrdc, int zmsk)
{
   // set up to draw dashed, XORed lines
   HPEN oldPen     = SelectObject(scrdc,whitePen);
   int oldRasterOp = SetROP2(scrdc,R2_NOT);
   int oldBkMode   = SetBkMode(scrdc,TRANSPARENT);

   if (zmsk & ZONE_DISPLAY_SAFE_ACTION) {

      double safeActionFactor = sqrt(.95);
      int safeActionWidth  = safeActionFactor * Image1->Width;
      int safeActionHeight = safeActionFactor * Image1->Height;
      RECT safeAction;
      safeAction.left   = Image1->Left + (Image1->Width  -  safeActionWidth)/2;
      safeAction.top    = Image1->Top  + (Image1->Height - safeActionHeight)/2;
      safeAction.right  = safeAction.left + safeActionWidth - 1;
      safeAction.bottom = safeAction.top + safeActionHeight - 1;

      MoveToEx(scrdc,safeAction.left,safeAction.top,NULL);
      LineTo(scrdc,safeAction.right,safeAction.top);
      LineTo(scrdc,safeAction.right,safeAction.bottom);
      LineTo(scrdc,safeAction.left,safeAction.bottom);
      LineTo(scrdc,safeAction.left,safeAction.top);
   }

   if (zmsk & ZONE_DISPLAY_SAFE_TITLE) {

      double safeTitleFactor = sqrt(.90);
      int safeTitleWidth  = safeTitleFactor * Image1->Width;
      int safeTitleHeight = safeTitleFactor * Image1->Height;
      RECT safeTitle;
      safeTitle.left   = Image1->Left + (Image1->Width  -  safeTitleWidth)/2;
      safeTitle.top    = Image1->Top  + (Image1->Height - safeTitleHeight)/2;
      safeTitle.right  = safeTitle.left  + safeTitleWidth - 1;
      safeTitle.bottom = safeTitle.top  + safeTitleHeight - 1;

      MoveToEx(scrdc,safeTitle.left,safeTitle.top,NULL);
      LineTo(scrdc,safeTitle.right,safeTitle.top);
      LineTo(scrdc,safeTitle.right,safeTitle.bottom);
      LineTo(scrdc,safeTitle.left,safeTitle.bottom);
      LineTo(scrdc,safeTitle.left,safeTitle.top);
   }

   // restore the old settings
   SetBkMode(scrdc,oldBkMode);
   SetROP2(scrdc,oldRasterOp);
   SelectObject(scrdc,oldPen);
}

//---------------------------------------------------------------------------

void TProxyForm::DrawZonesIntoBitmap(HDC bmdc, int zmsk)
{
   // set up to draw dashed, XORed lines
   HPEN oldPen     = SelectObject(bmdc,whitePen);
   int oldRasterOp = SetROP2(bmdc,R2_NOT);
   int oldBkMode   = SetBkMode(bmdc,TRANSPARENT);

   if (zmsk & ZONE_DISPLAY_SAFE_ACTION) {

      double safeActionFactor = sqrt(.95);
      int safeActionWidth  = safeActionFactor * Image1->Width;
      int safeActionHeight = safeActionFactor * Image1->Height;
      RECT safeAction;
      safeAction.left   = (Image1->Width  -  safeActionWidth)/2;
      safeAction.top    = (Image1->Height - safeActionHeight)/2;
      safeAction.right  = safeAction.left + safeActionWidth - 1;
      safeAction.bottom = safeAction.top + safeActionHeight - 1;

      MoveToEx(bmdc,safeAction.left,safeAction.top,NULL);
      LineTo(bmdc,safeAction.right,safeAction.top);
      LineTo(bmdc,safeAction.right,safeAction.bottom);
      LineTo(bmdc,safeAction.left,safeAction.bottom);
      LineTo(bmdc,safeAction.left,safeAction.top);
   }

   if (zmsk & ZONE_DISPLAY_SAFE_TITLE) {

      double safeTitleFactor = sqrt(.90);
      int safeTitleWidth  = safeTitleFactor * Image1->Width;
      int safeTitleHeight = safeTitleFactor * Image1->Height;
      RECT safeTitle;
      safeTitle.left   = (Image1->Width  -  safeTitleWidth)/2;
      safeTitle.top    = (Image1->Height - safeTitleHeight)/2;
      safeTitle.right  = safeTitle.left  + safeTitleWidth - 1;
      safeTitle.bottom = safeTitle.top  + safeTitleHeight - 1;

      MoveToEx(bmdc,safeTitle.left,safeTitle.top,NULL);
      LineTo(bmdc,safeTitle.right,safeTitle.top);
      LineTo(bmdc,safeTitle.right,safeTitle.bottom);
      LineTo(bmdc,safeTitle.left,safeTitle.bottom);
      LineTo(bmdc,safeTitle.left,safeTitle.top);
   }

   // restore the old settings
   SetBkMode(bmdc,oldBkMode);
   SetROP2(bmdc,oldRasterOp);
   SelectObject(bmdc,oldPen);
}

//---------------------------------------------------------------------------
void TProxyForm::ClearImage(int zmsk)
{
   HDC screenDC = GetDC(Handle);

   // blit the image
   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          screenDC,
          0,0,
          BLACKNESS);

   if (zmsk != 0)
      DrawZonesOntoScreen(screenDC, zmsk);

   ReleaseDC(Handle,screenDC);
}

//---------------------------------------------------------------------------
void __fastcall TProxyForm::FormSize(TObject *Sender)
{
   MainForm->SetProxyDimensions(Width);
   ClearImage(MainForm->GetZonesMask());
}

//---------------------------------------------------------------------------
void __fastcall TProxyForm::Refresh(TObject *Sender)
{
   ClearImage(MainForm->GetZonesMask());
}

//---------------------------------------------------------------------------
void TProxyForm::BltProxy(HDC bmdc, int zmsk)
{
   if (zmsk != 0)
      DrawZonesIntoBitmap(bmdc, zmsk);

   HDC screenDC = GetDC(Handle);

   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          bmdc,
          0,0,
          SRCCOPY);

   //if (zmsk != 0)
   //   DrawZones(screenDC, zmsk);

   ReleaseDC(Handle,screenDC);
}

void __fastcall TProxyForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   MainForm->ProxyMenuItem->Checked = false;        
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::FieldsSubMenuClick(TObject *Sender)
{
   switch(MainForm->GetFieldsMask()) {

      case FIELD_DISPLAY_1:
         Field1MenuItem->Checked = true;
         break;

      case FIELD_DISPLAY_2:
         Field2MenuItem->Checked = true;
         break;

      case FIELD_DISPLAY_BOTH:
         BothFieldsMenuItem->Checked = true;
         break;
   }

}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::Field1MenuItemClick(TObject *Sender)
{
   if (!Field1MenuItem->Checked) {
      Field1MenuItem->Checked = true;
      MainForm->SetFieldsMask(FIELD_DISPLAY_1);
   }
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::Field2MenuItemClick(TObject *Sender)
{
   if (!Field2MenuItem->Checked) {
      Field2MenuItem->Checked = true;
      MainForm->SetFieldsMask(FIELD_DISPLAY_2);
   }
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::BothFieldsMenuItemClick(TObject *Sender)
{
   if (!BothFieldsMenuItem->Checked) {
      BothFieldsMenuItem->Checked = true;
      MainForm->SetFieldsMask(FIELD_DISPLAY_BOTH);
   }
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::ZonesSubMenuClick(TObject *Sender)
{
   int zmsk = MainForm->GetZonesMask();
   SafeActionMenuItem->Checked = ((zmsk&ZONE_DISPLAY_SAFE_ACTION) != 0);
   SafeTitleMenuItem->Checked  = ((zmsk&ZONE_DISPLAY_SAFE_TITLE)  != 0);
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::SafeActionMenuItemClick(TObject *Sender)
{
   int zmsk = MainForm->GetZonesMask();
   zmsk ^= ZONE_DISPLAY_SAFE_ACTION;
   SafeActionMenuItem->Checked = ((zmsk&ZONE_DISPLAY_SAFE_ACTION) != 0);
   MainForm->SetZonesMask(zmsk);
   ClearImage(zmsk);
}
//---------------------------------------------------------------------------

void __fastcall TProxyForm::SafeTitleMenuItemClick(TObject *Sender)
{
   int zmsk = MainForm->GetZonesMask();
   zmsk ^= ZONE_DISPLAY_SAFE_TITLE;
   SafeActionMenuItem->Checked = ((zmsk&ZONE_DISPLAY_SAFE_TITLE) != 0);
   MainForm->SetZonesMask(zmsk);
   ClearImage(zmsk);
}
//---------------------------------------------------------------------------


