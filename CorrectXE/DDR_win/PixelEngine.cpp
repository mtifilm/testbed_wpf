//
// implementation file for class CPixelEngine
//
#include "PixelEngine.h"
#include "font8x10.h"
#include <math.h>
#include <iostream>

//
// constructor
//
CPixelEngine::
CPixelEngine()
{
}

//
// destructor
//
CPixelEngine::
~CPixelEngine()
{
}

//
// return the required frame-buffer size in bytes
//
int CPixelEngine::
getFrameBufferSize() const
{
   return(dpitch*(dhigh+1)*4);
}

//
// set the frame buffer
//
void CPixelEngine::
setFrameBuffer(int wdth, int hght, unsigned int *frmbuf)
{
   // pitch and limits of frame buffer
   dpitch = wdth;
   dwide  = wdth-1;
   dhigh  = hght-1;

   // -> frame buffer
   frameBuffer = frmbuf;

   // init these
   fgndColor = 0;
   bgndColor = 0;
   linePattern = 0xff;
}

//
// get the frame buffer
//
unsigned int *CPixelEngine::
getFrameBuffer() const
{
   return(frameBuffer);
}

//
// set the current line pattern
//
void CPixelEngine::
setLinePattern(unsigned char pat)
{
   linePattern = pat;
}

//
// set the current fgnd color
//
void CPixelEngine::
setFGColor(unsigned int col)
{
   fgndColor = col;
}

//
// set the current bgnd color
//
void CPixelEngine::
setBGColor(unsigned int col)
{
   bgndColor = col;
}

//
// draw a rectangle
//
void CPixelEngine::
drawRectangle(int l, int b, int r, int t)
{
   register int rowcnt = r-l+1;
   register int step = dpitch - rowcnt;

   register unsigned int *d = frameBuffer + b*dpitch + l;
   register unsigned int val = fgndColor;
   for (int i=0;i<(t-b+1);i++) {
      for (int j=0;j<rowcnt;j++) {
         *d++ = val;
      }
      d += step;
   }
}

//
// clear the frame buffer
//
void CPixelEngine::
clearFrameBuffer()
{
   unsigned int *dst = frameBuffer;
   int pels = (dhigh+1)*dpitch;
   int col = bgndColor;
   _asm {
      mov   eax,col
      mov   edi,dst
      mov   ecx,pels
      rep   stosd
   }
}

//
// shift the contents of the frame buffer left
//
void CPixelEngine::
jogFrameBufferLeft()
{
   unsigned int *dst = frameBuffer;
   unsigned int *src = dst+1;
   int pels = dpitch-1;
   for (int i=0;i<=dhigh;i++) {
      for (int j=0;j<pels;j++)
         *dst++ = *src++;
      *dst++ = bgndColor;
      src++;
   }
}

//
// move CP to (x,y)
//
void CPixelEngine::
moveTo(int x, int y)
{
   curx = x;
   cury = y;
   curaddr = frameBuffer + y*dpitch + x;
}

//
// draw dot at CP
//
void CPixelEngine::
drawDot()
{
   *curaddr = fgndColor;
}

//
// draw line from CP to (x,y)
//
void CPixelEngine::
lineTo(int x, int y)
{
   register unsigned int *d = curaddr;
   register unsigned char pat = linePattern;
   register unsigned int val = fgndColor;
   register int delx, dely;
   register int brx,bry,pen;

   if ((dely=y-cury)==0) { // horz
      if ((delx=x-curx) >= 0) { // rgt
         if (pat==0xFF) {
            while (delx--!=0) {
               *d++ = val;
            }
         }
         else {
            while (delx--!=0) {
               if (pat&0x80) {
                  *d++ = val;
                  pat = (pat<<1)+1;
               }
               else {
                  d++;
                  pat = (pat<<1);
               }
            }
         }
         curx = x;
         this->curaddr = d;
         return;
      }
      else { // lft
         if (pat==0xFF) {
            while (delx++!=0) {
               *d-- = val;
            }
         }
         else {
            while (delx++!=0) {
               if (pat&0x80) {
                  *d-- = val;
                  pat = (pat<<1)+1;
               }
               else {
                  d--;
                  pat = (pat<<1);
               }
            }
         }
         curx = x;
         this->curaddr = d;
         return;
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert
         if ((dely=y-cury) > 0) { // dn
            if (pat==0xFF) {
               while (dely--!=0) {
                  *d  = val;
                  d += dpitch;
               }
            }
            else {
               while (dely--!=0) {
                  if (pat&0x80) {
                     *d = val;
                     pat = (pat<<1)+1;
                  }
                  else {
                     pat = (pat<<1);
                  }
                  d += dpitch;
               }
            }
            cury = y;
            this->curaddr = d;
            return;
         }
         else { // up
            if (pat==0xFF) {
               while (dely++!=0) {
                  *d  = val;
                  d -= dpitch;
               }
            }
            else {
               while (dely++!=0) {
                  if (pat&0x80) {
                     *d = val;
                     pat = (pat<<1)+1;
                  }
                  else {
                     pat = (pat<<1);
                  }
                  d -= dpitch;
               }
            }
            cury = y;
            this->curaddr = d;
            return;
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  if (pat==0xFF) {
                     while (delx--!=0) {
                        *d++ = val;
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           d += dpitch;
                        }
                     }
                  }
                  else {
                     while (delx--!=0) {
                        if (pat&0x80) {
                           *d++ = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           d++;
                           pat = (pat<<1);
                        }
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           d += dpitch;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
               else { // dn-rgt steep
                  pen = dely;
                  if (pat==0xFF) {
                     while (dely--!=0) {
                        *d = val;
                        d += dpitch;
                        if ((pen-=brx) <= 0) {
                           pen += bry;
                           d++;
                        }
                     }
                  }
                  else {
                     while (dely--!=0) {
                        if (pat&0x80) {
                           *d = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           pat = (pat<<1);
                        }
                        d += dpitch;
                        if ((pen-=brx) <= 0) {
                           pen += bry;
                           d++;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
            }
            else { // lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  if (pat==0xFF) {
                     while (delx--!=0) {
                        *d-- = val;
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           d += dpitch;
                        }
                     }
                  }
                  else {
                     while (delx--!=0) {
                        if (pat&0x80) {
                           *d-- = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           d--;
                           pat = (pat<<1);
                        }
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           d += dpitch;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
               else { // dn-lft steep
                  pen = dely;
                  if (pat==0xFF) {
                     while (dely--!=0) {
                        *d = val;
                        d += dpitch;
                        if ((pen-=brx) <= 0) {
                           pen += bry;
                           d--;
                        }
                     }
                  }
                  else {
                     while (dely--!=0) {
                        if (pat&0x80) {
                           *d = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           pat = (pat<<1);
                        }
                        d += dpitch;
                        if ((pen-=brx) <= 0) {
                           pen += bry;
                           d--;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  if (pat==0xFF) {
                     while (delx--!=0) {
                        *d++ = val;
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           d -= dpitch;
                        }
                     }
                  }
                  else {
                     while (delx--!=0) {
                        if (pat&0x80) {
                           *d++ = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           d++;
                           pat = (pat<<1);
                        }
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           d -= dpitch;
                        }
                     }

                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
               else { // up-rgt steep
                  pen = dely;
                  if (pat==0xFF) {
                     while (dely--!=0) {
                        *d = val;
                        d -= dpitch;
                        if ((pen-=brx) < 0) {
                           pen += bry;
                           d++;
                        }
                     }
                  }
                  else {
                     while (dely--!=0) {
                        if (pat&0x80) {
                           *d = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           pat = (pat<<1);
                        }
                        d -= dpitch;
                        if ((pen-=brx) < 0) {
                           pen += bry;
                           d++;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
            }
            else { // lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  if (pat==0xFF) {
                     while (delx--!=0) {
                        *d-- = val;
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           d -= dpitch;
                        }
                     }
                  }
                  else {
                     while (delx--!=0) {
                        if (pat&0x80) {
                           *d-- = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           d--;
                           pat = (pat<<1);
                        }
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           d -= dpitch;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
               else { // up-lft steep
                  pen = dely;
                  if (pat==0xFF) {
                     while (dely--!=0) {
                        *d = val;
                        d -= dpitch;
                        if ((pen-=brx) < 0) {
                           pen += bry;
                           d--;
                        }
                     }
                  }
                  else {
                     while (dely--!=0) {
                        if (pat&0x80) {
                           *d = val;
                           pat = (pat<<1)+1;
                        }
                        else {
                           pat = (pat<<1);
                        }
                        d -= dpitch;
                        if ((pen-=brx) < 0) {
                           pen += bry;
                           d--;
                        }
                     }
                  }
                  curx = x;
                  cury = y;
                  this->curaddr = d;
                  return;
               }
            }
         }
      }
   }
}

#define SMLARC    512
#define MIDARC 131072

//
// arc routine which draws an elliptical arc
// which lies totally inside one quadrant
//
void CPixelEngine::
arcTo(int x, int y, double xctr, double yctr)
{
   register unsigned  int  *d = curaddr;
   register unsigned char pat = linePattern;
   register unsigned  int val = fgndColor;

   register int xmo, ymo, xymo, xend, yend;

   register double fdelxi, fdelyi, fdelxf, fdelyf;
   register int     delxi,  delyi,  delxf,  delyf;

   xmo = 1;
   if ((xend=x-curx)<0) {
      xmo = -xmo;
      xend = -xend;
   }
   ymo = dpitch;
   if ((yend=y-cury)<0) {
      ymo = -ymo;
      yend = -yend;
   }
   xymo = xmo + ymo;

   if (((xend<3)&&(yend<3))||(x==curx)||(y==cury)) {
      lineTo(x,y);
      return;
   }
   else { // not tiny or trivial arc

      register unsigned char smlcnt = 0;
      register unsigned char midcnt = 0;
      if ((fdelxi=(double)curx-xctr)<0) fdelxi = -fdelxi;
      if (fdelxi<MIDARC) {
         delxi = (int)fdelxi;
         if (delxi<SMLARC) smlcnt++;
         midcnt++;
      }
      if ((fdelyi=(double)cury-yctr)<0) fdelyi = -fdelyi;
      if (fdelyi<MIDARC) {
         delyi = (int)fdelyi;
         if (delyi<SMLARC) smlcnt++;
         midcnt++;
      }
      if ((fdelxf=(double)x-xctr)<0) fdelxf = -fdelxf;
      if (fdelxf<MIDARC) {
         delxf = (int)fdelxf;
         if (delxf<SMLARC) smlcnt++;
         midcnt++;
      }
      if ((fdelyf=(double)y-yctr)<0) fdelyf = -fdelyf;
      if (fdelyf<MIDARC) {
         delyf = (int)fdelyf;
         if (delyf<SMLARC) smlcnt++;
         midcnt++;
      }

      if (smlcnt==4) { // small arc

         register int aa = delyf*delyf - delyi*delyi;
         register int xdel = 2 * aa * delxi;
         register int bb = delxi*delxi - delxf*delxf;
         register int ydel = 2 * bb * delyi;

         if (delxi > delxf) {
            xdel = -xdel + aa;
            ydel =  ydel + bb;
         }
         else {
            xdel =  xdel + aa;
            ydel = -ydel + bb;
         }
         aa *= 2;
         bb *= 2;

         register int pen = 0;
         while (true) {

            register int  xpent,  absxpent;
            register int  ypent,  absypent;
            register int xypent, absxypent;

            if (pat&0x80) *d = val;

            if ((absxpent=xpent=pen+xdel)<0) absxpent = -absxpent;
            if ((absypent=ypent=pen+ydel)<0) absypent = -absypent;
            if ((absxypent=xypent=pen+xdel+ydel)<0) absxypent = -absxypent;

            if ((absxpent<=absypent)&&(absxpent<absxypent)) {
               pen = xpent;
               xdel += aa;
               d += xmo;
               xend--;
            }
            else if (absypent<=absxypent) {
               pen = ypent;
               ydel += bb;
               d += ymo;
               yend--;
            }
            else {
               pen = xypent;
               xdel += aa;
               ydel += bb;
               d += xymo;
               xend--; yend--;
            }
            pat = ((pat&0x80)!=0)?(pat<<1)+1:(pat<<1);
            if ((xend|yend)==0) {
               moveTo(x,y);
               linePattern = pat;
               return;
            }

         }

      }
      else if (midcnt==4) { // mid-size arc

         register double faa = fdelyf*fdelyf - fdelyi*fdelyi;
         register double fxdel = 2 * faa * fdelxi;
         register double fbb = fdelxi*fdelxi - fdelxf*fdelxf;
         register double fydel = 2 * fbb * fdelyi;

         if (fdelxi > fdelxf) {
            fxdel = -fxdel + faa;
            fydel =  fydel + fbb;
         }
         else {
            fxdel =  fxdel + faa;
            fydel = -fydel + fbb;
         }
         faa *= 2;
         fbb *= 2;

         register double fpen = 0;
         while (true) {

            register double  fxpent,  fabsxpent;
            register double  fypent,  fabsypent;
            register double fxypent, fabsxypent;

            if (pat&0x80) *d = val;

            if ((fabsxpent=fxpent=fpen+fxdel)<0) fabsxpent = -fabsxpent;
            if ((fabsypent=fypent=fpen+fydel)<0) fabsypent = -fabsypent;
            if ((fabsxypent=fxypent=fpen+fxdel+fydel)<0) fabsxypent = -fabsxypent;

            if ((fabsxpent<=fabsypent)&&(fabsxpent<fabsxypent)) {
               fpen = fxpent;
               fxdel += faa;
               d += xmo;
               xend--;
            }
            else if (fabsypent<=fabsxypent) {
               fpen = fypent;
               fydel += fbb;
               d += ymo;
               yend--;
            }
            else {
               fpen = fxypent;
               fxdel += faa;
               fydel += fbb;
               d += xymo;
               xend--; yend--;
            }
            pat = ((pat&0x80)!=0)?(pat<<1)+1:(pat<<1);
            if ((xend|yend)==0) {
               moveTo(x,y);
               linePattern = pat;
               return;
            }

         }

      }
      else { // large arc - draw straight line

         lineTo(x,y);
         return;

      }
   }
}

//
// draw text string beginning at upper left (x,y)
//
void CPixelEngine::
drawText(int x, int y, char *txt)
{
   // -> (top, lft) corner of current symbol
   unsigned int *ulptr = frameBuffer + y*dpitch + x;

   // row advance for 8-wide raster
   int dotadv = dpitch - 8;

   // each character
   unsigned char ch;

   // for each char in text string
   while (ch = *txt++) {

      unsigned int *dotptr = ulptr;

      for (int i=0;i<10;i++) {

         unsigned char fntrow = numeral[ch].row[i];
         unsigned char fntscn = 0x80;

         for (int j=0;j<8;j++) {

            if (fntrow&fntscn) {

               *dotptr++ = fgndColor;

            }
            else
               dotptr++;

            fntscn = fntscn >> 1;
         }

         dotptr += dotadv;
      }

      ulptr += 10;
   }
}

