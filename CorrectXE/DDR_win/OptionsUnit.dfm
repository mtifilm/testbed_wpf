object ParametersForm: TParametersForm
  Left = 348
  Top = 250
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Emulation Parameters'
  ClientHeight = 299
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inline OptionsFrame: TOptionsFrame
    Left = 8
    Top = 8
  end
  object OkButton: TBitBtn
    Left = 40
    Top = 264
    Width = 75
    Height = 25
    Caption = '&OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = OkButtonClick
  end
  object CancelButton: TBitBtn
    Left = 256
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
