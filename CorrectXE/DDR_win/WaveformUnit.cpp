//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DDRUnit.h"
#include "WaveformUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWaveformForm *WaveformForm;
//---------------------------------------------------------------------------
__fastcall TWaveformForm::TWaveformForm(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TWaveformForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   MainForm->WaveformMenuItem->Checked = false;
}

//---------------------------------------------------------------------------
void __fastcall TWaveformForm::Refresh(TObject *Sender)
{
   ClearImage();
}

//---------------------------------------------------------------------------
void __fastcall TWaveformForm::FormSize(TObject *Sender)
{
   MainForm->SetWaveformDimensions(Width);
   ClearImage();
}

//---------------------------------------------------------------------------
void TWaveformForm::ClearImage()
{
   HDC screenDC = GetDC(Handle);

   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          screenDC,
          0,0,
          BLACKNESS);

   ReleaseDC(Handle,screenDC);
}

//---------------------------------------------------------------------------
void TWaveformForm::BltWaveform(HDC bmdc)
{
   HDC screenDC = GetDC(Handle);

   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          bmdc,
          0,0,
          SRCCOPY);

   ReleaseDC(Handle,screenDC);
}

//---------------------------------------------------------------------------

