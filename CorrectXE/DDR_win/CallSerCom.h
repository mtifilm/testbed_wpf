/*
	Name:	CallSerCom.h
	By:	Kevin Manbeck
	Date:	April 5, 2003

	A c++ wrapper around the sercom structure
*/

#include "sercom.h"

class CCallSerCom
{
public:
  CCallSerCom();
  ~CCallSerCom ();

  void Rewind ();
  void PlayBack ();
  void Stop ();
  void Play ();
  void Record ();
  void FastForward ();
  void TimeSense(char *cpTimeCode);
  void Open();
  void Close();
  bool IsConnected();
  void SetMaxSpeed (float fSpeed);
  void SetAudioSlip (float fAudioSlip);
  void SetSerialPort (int iPort);
  void SetSocket (int iSocket);
  void SetNetName (const char *cpNetName);

private:
  SER_COM_STRUCT
    scsPort;
  TIME_CODE
    tcMarkIn,
    tcMarkOut,
    tcBeginning,
    tcEnding;
  bool
    bSerialPortOpen;

  float
    fMaxSpeed;

  void IssueCommand();
  int IssueCommandSilent();
};
