//
// include file for class CVectorScope
//
#ifndef VECTORSCOPE_H
#define VECTORSCOPE_H

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"

// forward declarations
class CPixelEngine;

class CVectorScope
{

public:

   CVectorScope();

   ~CVectorScope();

   void convert( unsigned int *srcbuf,
                 int srcWdth,
                 int srcHght,
                 int srcPitch,
                 unsigned int *dstbuf,
                 int dstwdth,
                 int dsthght,
                 int dstpitch,
                 double gain);

private:

   void drawReticle();

private:

   CPixelEngine *pxlEng;

#define MAX_HISTOGRAM_WIDTH 512
   unsigned int *histogram;

///////////////////////////////////////////////////////////////////

// the INDEX into the UV table is assembled from:
//
// the 6 most-significant bits of R in bits 12-17
#define mskr 0x3f000
// the 6 most-significant bits of G in bits 6-11
#define mskg 0x00fc0
// the 6 most-significant bits of B in bits 0-5
#define mskb 0x0003f

// the entries in the table are unsigned integers
// with 16 bits of U in the lower portion and 16
// bits in the higher portion.
//
   unsigned int *UVTbl;


   // VECTORSCOPE dimensions
   int vectorScopeWdth,
       vectorScopeHght;

   // center of scope
   int xctr, yctr;

   // radius of compass
   int radius;

};

#endif

 