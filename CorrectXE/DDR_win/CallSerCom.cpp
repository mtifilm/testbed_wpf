/*
	Name:	CallSerCom.cpp
	By:	Kevin Manbeck
	Date:	April 5, 2003

	A c++ wrapper around the sercom structure
*/

#include "CallSerCom.h"
#include "IniFile.h"

CCallSerCom::CCallSerCom()
{
  scsPort.iVersionNumber = SER_COM_VERSION_2001_01_22;
  scsPort.iPort = -1;
  strcpy (scsPort.caNetName, "");
  scsPort.iSocket = -1;
  scsPort.iBaudRate = NULL_BAUD_RATE;
  scsPort.iVerbose = FALSE;
  scsPort.iSlaveMode = FALSE;
  scsPort.iRS422Protocol = TRUE;
  scsPort.iProtocol = SER_COM_PROTOCOL_SONY422;
  scsPort.iaDeviceType[0] = 0x13;
  scsPort.iaDeviceType[1] = 0x15;

  bSerialPortOpen = false;
  tcMarkIn.iH = -1;
  tcMarkOut.iH = -1;
  tcBeginning.iH = -1;
  tcEnding.iH = -1;

  fMaxSpeed = 50.;

}  /* CCallSerCom */

CCallSerCom::~CCallSerCom()
{
  Close();
}  /* ~CCallSerCom */

void CCallSerCom::SetSerialPort (int iPort)
{
  if (bSerialPortOpen == false)
   {
    scsPort.iPort = iPort;
   }
}

void CCallSerCom::SetSocket (int iSocket)
{
  if (bSerialPortOpen == false)
   {
    scsPort.iSocket = iSocket;
   }
}

void CCallSerCom::SetNetName (const char *cpNetName)
{
  if (bSerialPortOpen == false)
   {
    strcpy (scsPort.caNetName, cpNetName);
   }
}

void CCallSerCom::Rewind ()
{
  scsPort.iCommand = VTR_SHUTTLE_REVERSE;
  scsPort.fTapeSpeed = fMaxSpeed;
  IssueCommand ();
}  /* Rewind */

void CCallSerCom::PlayBack ()
{
  scsPort.iCommand = VTR_VARIABLE_REVERSE;
  scsPort.fTapeSpeed = 1.;
  IssueCommand ();
}  /* PlayBack */

void CCallSerCom::Stop ()
{
  scsPort.iCommand = VTR_STOP;
  IssueCommand ();
}  /* Stop */

void CCallSerCom::Play ()
{
  scsPort.iCommand = VTR_PLAY;
  IssueCommand ();
}  /* Play */

void CCallSerCom::FastForward ()
{
  scsPort.iCommand = VTR_SHUTTLE_FORWARD;
  scsPort.fTapeSpeed = fMaxSpeed;
  IssueCommand ();
} /* FastForward */

void CCallSerCom::Record ()
{
  scsPort.iCommand = VTR_PLAY;
  IssueCommand ();
  scsPort.iCommand = VTR_EDIT_ON;
  IssueCommand ();
}

void CCallSerCom::TimeSense(char *cpTimeCode)
{
  scsPort.iCommand = VTR_TIME_SENSE_LTC;
  if (IssueCommandSilent() == 0)
   {
    if (scsPort.tcTimeCode.iDF)
     {
      sprintf (cpTimeCode, "%.2d:%.2d:%.2d.%.2d", 
		scsPort.tcTimeCode.iH,
		scsPort.tcTimeCode.iM,
		scsPort.tcTimeCode.iS,
		scsPort.tcTimeCode.iF);
     }
    else
     {
      sprintf (cpTimeCode, "%.2d:%.2d:%.2d:%.2d", 
		scsPort.tcTimeCode.iH,
		scsPort.tcTimeCode.iM,
		scsPort.tcTimeCode.iS,
		scsPort.tcTimeCode.iF);
     }
   }
  else
   {
    sprintf (cpTimeCode, "OFF LINE");
   }
}  /* TimeSense */

void CCallSerCom::IssueCommand()
{
  int iRet;

  if (bSerialPortOpen)
   {
    iRet = SerComCommand (&scsPort);
    if (iRet == SER_COM_NO_RESPONSE)
     {
      _MTIErrorDialog ("No response was received");
     }
    else if (iRet == SER_COM_NO_ACKNOWLEDGE)
     {
      char caMessage[64];
      sprintf (caMessage, "Command was not acknowledged 0x%x\n",
			scsPort.ucErrorValue);
      _MTIErrorDialog (caMessage);
     }
    else if (iRet != SER_COM_SUCCESS)
     {
      char caMessage[64];
      sprintf (caMessage, "Device returned %d\n", iRet);
      _MTIErrorDialog (caMessage);
     }
   }
  else
   {
    _MTIErrorDialog ("Device is not connected");
   }

}  /* IssueCommand */

int CCallSerCom::IssueCommandSilent()
{
  int iRet;

  iRet = 0;
  if (bSerialPortOpen)
   {
    iRet = SerComCommand (&scsPort);
   }

  return iRet;
}  /* IssueCommandSilent */

void CCallSerCom::Open()
{
  int iRet;

  // close the current session before opening a new one
  Close ();

  // Open up the serial port
  iRet = SerComOpen (&scsPort);
  if (iRet)
   {
    _MTIErrorDialog ("Unable to connect to a remote device");
   }
  else
   {
    bSerialPortOpen = true;
   }
}

void CCallSerCom::Close()
{
  int iRet;
  if (bSerialPortOpen == true)
   {
    // close up the serial port
    iRet = SerComClose (&scsPort);
    if (iRet)
     {
      _MTIErrorDialog ("Unable to close the serial port");
     }
   }
  bSerialPortOpen = false;
}

bool CCallSerCom::IsConnected ()
{
  return bSerialPortOpen;
}

void CCallSerCom::SetMaxSpeed (float fSpeed)
{
  fMaxSpeed = fSpeed;
}

void CCallSerCom::SetAudioSlip (float fAudioSlip)
{
  scsPort.iCommand = VTR_SET_AUDIO_SLIP;
  scsPort.fAudioSlip = fAudioSlip;
  IssueCommand ();
}
