//
// include file for class CWaveform
//
#ifndef WAVEFORM_H
#define WAVEFORM_H

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"

// forward declarations
class CPixelEngine;

class CWaveform
{

public:

   CWaveform();

   ~CWaveform();

   void convert( unsigned int *srcbuf,
                 int srcWdth,
                 int srcHght,
                 int srcPitch,
                 unsigned int *dstbuf,
                 int dstwdth,
                 int dsthght,
                 int dstpitch,
                 double gain);

private:

   void drawPoint(int x, int prey, int cury);

   void drawReticle();

private:

   CPixelEngine *pxlEng;

   // VECTORSCOPE dimensions
   int waveformWdth,
       waveformHght;

   int baseline;

   // define coordinates of ver-
   // tical zones of waveform
   int begRed,   endRed;
   int begGreen, endGreen;
   int begBlue,  endBlue;
   int begY,     endY;

};

#endif


 