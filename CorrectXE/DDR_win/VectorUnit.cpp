//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DDRUnit.h"
#include "VectorUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTITrackBar"
#pragma resource "*.dfm"
TVectorForm *VectorForm;
//---------------------------------------------------------------------------
__fastcall TVectorForm::TVectorForm(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TVectorForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   MainForm->VectorScopeMenuItem->Checked = false;
}

//---------------------------------------------------------------------------
void __fastcall TVectorForm::Refresh(TObject *Sender)
{
   ClearImage();
}

//---------------------------------------------------------------------------
void __fastcall TVectorForm::FormSize(TObject *Sender)
{
   MainForm->SetVectorScopeDimensions(Width);
   ClearImage();
}

//---------------------------------------------------------------------------
void TVectorForm::ClearImage()
{
   HDC screenDC = GetDC(Handle);

   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          screenDC,
          0,0,
          BLACKNESS);

   ReleaseDC(Handle,screenDC);
}

//---------------------------------------------------------------------------
void TVectorForm::BltVectorScope(HDC bmdc)
{
   HDC screenDC = GetDC(Handle);

   BitBlt(screenDC,
          Image1->Left,
          Image1->Top,
          Image1->Width,
          Image1->Height,
          bmdc,
          0,0,
          SRCCOPY);

   ReleaseDC(Handle,screenDC);
}

//---------------------------------------------------------------------------

