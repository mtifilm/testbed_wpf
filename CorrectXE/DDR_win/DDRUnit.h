//---------------------------------------------------------------------------

#ifndef DDRUnitH
#define DDRUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "OptionsFrameUnit.h"
#include "HistoryMenu.h"
#include <Menus.hpp>
#include <Buttons.hpp>
#include "MTIUNIT.h"
#include <winsock.h>
#include "VTRInterface.h"
#include "MTIBitmap.h"
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>

// forward declarations
class CImageFormat;
class CConvert;
class CVectorScope;
class CWaveform;
class CBinMgrGUIInterface;

//---------------------------------------------------------------------------
class TMainForm : public TMTIForm
{
__published:	// IDE-managed Components
        THistoryMenu *HistoryMenu;
        TMenuItem *File1;
        TMenuItem *Exit1;
        TMenuItem *OpenMenuItem;
        TMenuItem *N1;
        TPanel *OpenPanel;
        TBitBtn *StartInButton;
        TLabel *Label1;
        TSpeedButton *BrowserButton;
        TLabel *DescLabel;
        TLabel *FormatLabel;
        TLabel *DescriptionLabel;
        TLabel *Label4;
        TLabel *Label2;
        TLabel *InTCLabel;
        TLabel *Label3;
        TLabel *OutTCLabel;
        TLabel *Label5;
        TLabel *DurLabel;
        TBevel *Bevel2;
        TCheckBox *AudioCBox;
        TCheckBox *ProxyCBox;
        TBitBtn *ParametersButton;
        TBitBtn *StartOutButton;
        TBevel *Bevel1;
        TPanel *Panel1;
        TCheckBox *Video;
        TPanel *RunPanel;
        TSpeedButton *SpeedButton1;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label12;
        TLabel *Label14;
        TLabel *Label16;
        TPanel *Panel3;
        TBitBtn *StopButton;
        TLabel *ClipNameLabel;
        TLabel *StatusLabel;
        TLabel *Label6;
        TRichEdit *StatusMemo;
        TEdit *ClipEdit;
        TMenuItem *View1;
        TMenuItem *ProxyMenuItem;
        TMenuItem *N2;
        TMenuItem *VectorScopeMenuItem;
        TTimer *Timer1;
        TMenuItem *N3;
        TMenuItem *WaveformMenuItem;
    TMenuItem *Control1;
    TMenuItem *DDR1;
    TMenuItem *VTR1;
        void __fastcall BrowserButtonClick(TObject *Sender);
        void __fastcall ParametersButtonClick(TObject *Sender);
        void __fastcall StartButtonClick(TObject *Sender);
        void __fastcall StopButtonClick(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall HistoryMenuHistoryClick(AnsiString Str);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall ProxyCBoxClick(TObject *Sender);
        void __fastcall ProxyMenuClick(TObject *Sender);
        void __fastcall VectorMenuClick(TObject *Sender);
        void __fastcall CreateForm(TObject *Sender);
        void __fastcall DestroyForm(TObject *Sender);
        void __fastcall TimerWorkProc(TObject *Sender);
        void __fastcall WaveformMenuClick(TObject *Sender);
        void __fastcall DDR1Click(TObject *Sender);
        void __fastcall VTR1Click(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);

protected:
       virtual void StartForm(void);

private:	// User declarations

        CVTRInterface _DDR;
        int _PortCount;
        EImageFormatType _ImgFormatType;

        bool StartTransfer(void);
        bool StopTransfer(void);

        void UpdateForm(void);
        bool OpenClip(const string &strClipName);

#ifdef DS1961S_DONGLE
       // FlexLM private variables
       string _StartupFailedMessage;
       CRsrcCtrl License;
#endif;

   // flag to shunt the timer until CreateForm is called
   bool created;

   // YUV-to-RGB color converter
   CConvert *cnvt;

   // RGB-to-vectorscope converter
   CVectorScope *vctr;

   // RGB-to-waveform converter
   CWaveform *wave;

   // PROXY bitmap
   CMTIBitmap *proxyBM;

   // VECTORPROXY bitmap (not an MTIBitmap - just RAM)
   unsigned int *vectorProxyBM;

   // VECTORSCOPE bitmap
   CMTIBitmap *vectorBM;

   // WAVEFORM bitmap
   CMTIBitmap *waveBM;

   // image format of currently loaded clip
   const CImageFormat *srcImageFormat;

   // variables for generating proxy graphics
   RECT frmRect;         // active rectangle (frame coords)
   int frmWdth, frmHght; // dimensions of above
   double pixelAspectRatio;
   double frameAspectRatio;

#define BORDER_WIDTH 20
#define HEADER_HEIGHT 24
   //
   // dimensions of displayed proxy
   //
#define MIN_PROXY_WIDTH 128
#define MAX_PROXY_WIDTH 512
   //
   int proxyWdth, proxyHght;
   //
   // mask to show which fields are displayed in the proxy
   //
#define FIELD_DISPLAY_1    1
#define FIELD_DISPLAY_2    2
#define FIELD_DISPLAY_BOTH 3
   //
   int fieldsMask;
   //
   // mask to show which auxiliary zones are displayed in the proxy
   //
#define ZONE_DISPLAY_SAFE_ACTION 1
#define ZONE_DISPLAY_SAFE_TITLE  2
   //
   int zonesMask;
   //
   // dimensions of vectorproxy bitmap
   //
#define VECTORPROXY_WIDTH 256
   int vectorProxyWdth, vectorProxyHght;
   //
   // dimensions of displayed vectorscope
   //
#define MIN_VECTOR_WIDTH 128
#define MAX_VECTOR_WIDTH 512
   //
   int vectorWdth, vectorHght;
   //
   // dimensions of displayed waveform
   //
#define MIN_WAVEFORM_WIDTH 128
#define MAX_WAVEFORM_WIDTH 512
   //
   int waveformWdth, waveformHght;

   // this is to make the bin manager not crash when we try to select a clip
   CBinMgrGUIInterface *binMgrGuiInterface;

public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);

   void SetProxyImageFormat();
   void SetProxyDimensions(int wdth);
   int  GetFieldsMask();
   void SetFieldsMask(int fmsk);
   int  GetZonesMask();
   void SetZonesMask(int zmsk);
   void SetVectorScopeDimensions(int wdth);
   void SetWaveformDimensions(int wdth);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
