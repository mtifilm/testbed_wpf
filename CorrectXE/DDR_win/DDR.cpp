//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("DDRUnit.cpp", MainForm);
USEFORM("OptionsFrameUnit.cpp", OptionsFrame); /* TFrame: File Type */
USEFORM("C:\MTIBorland\Repository\MTIUNIT.cpp", MTIForm);
USEFORM("OptionsUnit.cpp", ParametersForm);
USEFORM("ProxyUnit.cpp", ProxyForm);
USEFORM("VectorUnit.cpp", VectorForm);
USEFORM("WaveformUnit.cpp", WaveformForm);
USEFORM("UnitDDRControl.cpp", FormDDRControl);
USEFORM("UnitVTRControl.cpp", FormVTRControl);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TMTIForm), &MTIForm);
                 Application->CreateForm(__classid(TParametersForm), &ParametersForm);
                 Application->CreateForm(__classid(TProxyForm), &ProxyForm);
                 Application->CreateForm(__classid(TVectorForm), &VectorForm);
                 Application->CreateForm(__classid(TWaveformForm), &WaveformForm);
                 Application->CreateForm(__classid(TFormDDRControl), &FormDDRControl);
                 Application->CreateForm(__classid(TFormVTRControl), &FormVTRControl);
                 Application->CreateForm(__classid(TFormVTRControl), &FormVTRControl);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
