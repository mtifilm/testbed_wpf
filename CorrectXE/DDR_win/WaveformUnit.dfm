object WaveformForm: TWaveformForm
  Left = 100
  Top = 100
  Width = 552
  Height = 624
  Caption = 'Waveform'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnPaint = Refresh
  OnResize = FormSize
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 16
    Top = 13
    Width = 416
    Height = 416
  end
  object GainLabel: TLabel
    Left = 16
    Top = 449
    Width = 22
    Height = 13
    Caption = 'Gain'
  end
  object GainTrackbar: TTrackBar
    Left = 45
    Top = 450
    Width = 371
    Height = 7
    Max = 10000
    Orientation = trHorizontal
    Frequency = 1
    Position = 5000
    SelEnd = 0
    SelStart = 0
    TabOrder = 0
    TickMarks = tmBottomRight
    TickStyle = tsAuto
  end
end
