//
// implementation file for class CVectorScope
//
#include "VectorScope.h"
#include "PixelEngine.h"

///////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CVectorScope::
CVectorScope()
{
   // allocate a pixel engine
   pxlEng = new CPixelEngine;

   // no histogram buffer yet
   histogram = new unsigned int[MAX_HISTOGRAM_WIDTH*MAX_HISTOGRAM_WIDTH];

   // clear the dimensions
   vectorScopeWdth = 0;
   vectorScopeHght = 0;

#define USE_FAST

#ifdef USE_SLOW
   MTI_UINT16 iR,iG,iB;

   MTI_UINT16 U, V, Y;

   CImageFormat RGBfmt;
   RGBfmt.setColorSpace(IF_COLOR_SPACE_CCIR_601_M);
   RGBfmt.setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
   RGBfmt.setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);

   CImageFormat YUVfmt;
   YUVfmt.setColorSpace(IF_COLOR_SPACE_CCIR_601_M);
   YUVfmt.setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
   YUVfmt.setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

   CColorSpaceConvert csc;
   csc.Init(&RGBfmt,&YUVfmt);

   UVTbl = new unsigned int[64*64*64];
   MTI_UINT16 *dst = (MTI_UINT16 *)UVTbl;
   for (iR=0;iR<256;iR+=4) {
      for (iG=0;iG<256;iG+=4) {
         for (iB=0;iB<256;iB+=4) {

            csc.ConvertOneValue(iR,iG,iB,&Y,&U,&V);

            *dst++ = U;
            *dst++ = V;
         }
      }
   }
#endif

#ifdef USE_FAST
   UVTbl = new unsigned int[64*64*64];
   MTI_INT16 *dst = (MTI_INT16 *)UVTbl;
   for (int iR=0;iR<256;iR+=4) {

      double UR = iR*(-.14822266*256);
      double VR = iR*( .43921484*256);

      for (int iG=0;iG<256;iG+=4) {

         double URG = UR + iG*(-.29099219*256);
         double VRG = VR + iG*(-.36778906*256);

         for (int iB=0;iB<256;iB+=4) {

            double U = URG + iB*( .43921484*256)+ 0;
            double V = VRG + iB*(-.07142578*256)+ 0;

            *dst++ = (MTI_INT16)U;
            *dst++ = (MTI_INT16)V;
         }
      }
   }
#endif

}

/////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CVectorScope::
~CVectorScope()
{
   delete [] UVTbl;

   delete [] histogram;

   delete pxlEng;
}

#ifdef METHOD1
/////////////////////////////////////////////////////////////////////
//
//    C O N V E R T
//
void CVectorScope::convert( unsigned int *srcbuf,
                            int srcWdth,
                            int srcHght,
                            int srcPitch,
                            unsigned int *dstbuf,
                            int dstwdth,
                            int dsthght,
                            int dstpitch,
                            double gain)
{
   // save the dimensions
   vectorScopeWdth = dstwdth;
   vectorScopeHght = dsthght;

   // we'll need these coords
   xctr   = vectorScopeWdth / 2;
   yctr   = vectorScopeHght / 2;
   radius = vectorScopeHght / 2 - 25;
   double scale = (double)radius / (double)32768.;

   // hook the pixel engine to the dest frame buffer
   pxlEng->setFrameBuffer(dstpitch>>2, dsthght, dstbuf);

   // clear the dest buffer
   pxlEng->setBGColor(0x00000000);
   pxlEng->clearFrameBuffer();

   // clear the active part of the histogram buffer
   memset(histogram, 0, dstpitch*dsthght);

   // for each source row do
   //    for each source column do
   //       convert pixel value to (U,V) pair
   //       scale to (Ux,Vy)
   //       increment histogram cell
   //       include in maximum
   //    next column
   // next row
   //
   unsigned char *src = (unsigned char *)srcbuf;
   unsigned int rval,gval,bval;
   int srcadv = (srcPitch>>2) - srcWdth;
   unsigned int maximum = 0;

   for (int i=0;i<srcHght;i++) {

      for (int j=0;j<srcWdth;j++) {

         bval = *src++;
         gval = *src++;
         rval = *src++;
         src++;

         int index = (((int)rval<<10)&mskr) +
                     (((int)gval<< 4)&mskg) +
                     ((int)bval>> 2);

         unsigned int uv = UVTbl[index];

         //short int U = 252*(-.14822266*256);
         //short int V = 252*( .43921484*256);

         short int U = uv&0xffff;
         short int V = uv>>16;

         int Ux = (double)U*scale;
         int Vy = (double)V*scale;

         unsigned int *dst = histogram + (dstpitch>>2)*(-Vy+yctr) + (Ux+xctr);

         *dst += 1;

         if (*dst > maximum)
            maximum = *dst;
      }

      src += srcadv;
   }

   // set histogram threshold
   int threshold = maximum * (1.0 - gain);
   if (threshold < 4)
      threshold = 4;
   double fthreshold = (double)threshold;

   // for each histogram row and destination row
   //    for each histogram col and destination col
   //       if cell count > threshold
   //          cell count = threshold
   //       destination value = cell count * 0x00ffffff / threshold
   //    next
   // next
   //
   unsigned int  *hst = histogram;
   unsigned char *dst = (unsigned char *)dstbuf;
   int hstadv = (dstpitch>>2) - dstwdth;
   int dstadv = hstadv << 2;
   for (int i=0;i<dsthght;i++) {

      for (int j=0;j<dstwdth;j++) {

         int cellcount = *hst++;
         if (cellcount > threshold)
            cellcount = threshold;

         unsigned char val = ((double)cellcount/fthreshold)*(double)(0xff);
         *dst++ = val;
         *dst++ = val;
         *dst++ = val;
         dst++;
      }

      hst += hstadv;
      dst += dstadv;
   }

   // draw the axes and UV compass
   drawReticle();
}
#endif


#ifdef METHOD2
/////////////////////////////////////////////////////////////////////
//
//    C O N V E R T
//
void CVectorScope::convert( unsigned int *srcbuf,
                            int srcWdth,
                            int srcHght,
                            int srcPitch,
                            unsigned int *dstbuf,
                            int dstwdth,
                            int dsthght,
                            int dstpitch,
                            double gain)
{
   // save the dimensions
   vectorScopeWdth = dstwdth;
   vectorScopeHght = dsthght;

   // we'll need these coords
   xctr   = vectorScopeWdth / 2;
   yctr   = vectorScopeHght / 2;
   radius = vectorScopeHght / 2 - 25;
   double scale = (double)radius / (double)32768.;

   // hook the pixel engine to the dest frame buffer
   pxlEng->setFrameBuffer(dstpitch>>2, dsthght, dstbuf);

   // clear the dest buffer
   pxlEng->setBGColor(0x00000000);
   pxlEng->clearFrameBuffer();

   // clear the active part of the histogram buffer
   memset(histogram, 0, dstpitch*dsthght);

   // based on the gain, calculate the plotting color
   int color = 63 + gain*192;
   color = (color<<16)+(color<<8)+(color);

   // for each source row do
   //    for each source column do
   //       convert pixel value to (U,V) pair
   //       scale to (Ux,Vy)
   //       draw dot at (Ux,Vy)
   //    next column
   // next row
   //
   unsigned char *src = (unsigned char *)srcbuf;
   unsigned int rval,gval,bval;
   int srcadv = (srcPitch>>2) - srcWdth;

   for (int i=0;i<srcHght;i++) {

      for (int j=0;j<srcWdth;j++) {

         bval = *src++;
         gval = *src++;
         rval = *src++;
         src++;

         int index = (((int)rval<<10)&mskr) +
                     (((int)gval<< 4)&mskg) +
                     ((int)bval>> 2);

         unsigned int uv = UVTbl[index];

         short int U = uv&0xffff;
         short int V = uv>>16;

         int Ux = (double)U*scale;
         int Vy = (double)V*scale;

         unsigned int *dst = dstbuf + (dstpitch>>2)*(-Vy+yctr) + (Ux+xctr);

         *dst += color;
      }

      src += srcadv;
   }

   // draw the axes and UV compass
   drawReticle();
}
#endif

#define METHOD3
#ifdef METHOD3
/////////////////////////////////////////////////////////////////////
//
//    C O N V E R T
//
void CVectorScope::convert( unsigned int *srcbuf,
                            int srcWdth,
                            int srcHght,
                            int srcPitch,
                            unsigned int *dstbuf,
                            int dstwdth,
                            int dsthght,
                            int dstpitch,
                            double gain)
{
   // save the dimensions
   vectorScopeWdth = dstwdth;
   vectorScopeHght = dsthght;

   // we'll need these coords
   xctr   = vectorScopeWdth / 2;
   yctr   = vectorScopeHght / 2;
   radius = vectorScopeHght / 2 - 25;
   double scale = (double)radius / (double)32768.;

   // hook the pixel engine to the dest frame buffer
   pxlEng->setFrameBuffer(dstpitch>>2, dsthght, dstbuf);

   // clear the dest buffer
   pxlEng->setBGColor(0x00000000);
   pxlEng->clearFrameBuffer();

   // clear the active part of the histogram buffer
   memset(histogram, 0, dstpitch*dsthght);

   // based on the gain, calculate the plotting color
   int color = 63 + gain*192;
   color = (color<<16)+(color<<8)+(color);
   pxlEng->setFGColor(color);

   // for each source row do
   //    convert 1st pixel to (U,V) pair
   //    scale to (Ux,Vy)
   //    move CP to (Ux,Vy)
   //    for each source column do
   //       convert pixel value to (U,V) pair
   //       scale to (Ux,Vy)
   //       line to (Ux,Vy)
   //    next column
   // next row
   //
   unsigned char *src = (unsigned char *)srcbuf;
   unsigned int rval,gval,bval;
   int srcadv = (srcPitch>>2) - srcWdth;

   for (int i=0;i<srcHght;i++) {

      bval = *src++;
      gval = *src++;
      rval = *src++;
      src++;

      int index = (((int)rval<<10)&mskr) +
                  (((int)gval<< 4)&mskg) +
                  ((int)bval>> 2);

      unsigned int uv = UVTbl[index];

      short int U = uv&0xffff;
      short int V = uv>>16;

      int Ux = (double)U*scale;
      int Vy = (double)V*scale;

      pxlEng->moveTo(xctr+Ux,yctr-Vy);

      for (int j=1;j<srcWdth;j++) {

         bval = *src++;
         gval = *src++;
         rval = *src++;
         src++;

         int index = (((int)rval<<10)&mskr) +
                     (((int)gval<< 4)&mskg) +
                     ((int)bval>> 2);

         unsigned int uv = UVTbl[index];

         short int U = uv&0xffff;
         short int V = uv>>16;

         int Ux = (double)U*scale;
         int Vy = (double)V*scale;

         pxlEng->lineTo(xctr+Ux,yctr-Vy);
      }

      src += srcadv;
   }

   // draw the axes and UV compass
   drawReticle();
}
#endif

void CVectorScope::drawReticle()
{
   // the plotting color
   pxlEng->setFGColor(0x00808080);
   
   // draw U and V axes
   pxlEng->moveTo(0,yctr);
   pxlEng->lineTo(vectorScopeWdth,yctr);
   pxlEng->moveTo(xctr,0);
   pxlEng->lineTo(xctr,vectorScopeHght);

   // draw compass circle
   pxlEng->moveTo(xctr+radius,yctr);
   pxlEng->arcTo (xctr,yctr-radius,xctr,yctr);
   pxlEng->arcTo (xctr-radius,yctr,xctr,yctr);
   pxlEng->arcTo (xctr,yctr+radius,xctr,yctr);
   pxlEng->arcTo (xctr+radius,yctr,xctr,yctr);

   // label the axes
   pxlEng->drawText(xctr+4, 12, "V");
   pxlEng->drawText(xctr+yctr-18, yctr-12, "U");
}
