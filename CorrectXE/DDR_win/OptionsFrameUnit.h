//---------------------------------------------------------------------------


#ifndef OptionsFrameUnitH
#define OptionsFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "cspin.h"
#include "VDoubleEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Mask.hpp>
#include <winsock.h>
#include "VTRInterface.h"
#include <ImgList.hpp>
#include "IniFile.h"


//---------------------------------------------------------------------------
class TOptionsFrame : public TFrame
{
__published:	// IDE-managed Components
        TPageControl *DDRPageControl;
        TTabSheet *AudioTabSheet;
        TLabel *Label38;
        TLabel *Label39;
        TLabel *Label40;
        TBevel *Bevel1;
        TLabel *Label41;
        TLabel *Label42;
        TEdit *AudioInDeviceName;
        TEdit *AudioOutDeviceName;
        VDoubleEdit *AudioSlipInEdit;
        VDoubleEdit *AudioSlipOutEdit;
        VDoubleEdit *AudioBlendEdit;
        TCheckBox *AudioWriteDiskCBox;
        TCheckBox *AudioReadDiskCBox;
        TCheckBox *AudioInCBox;
        TCheckBox *AudioOutCBox;
        TTabSheet *GeneralTabSheet;
        TPanel *DDRSerial232Panel;
        TLabel *Label45;
        TCSpinEdit *DDRSerialPortSEdit;
        TPanel *DDRNetworkPanel;
        TLabel *Label10;
        TCSpinEdit *DDRSocketSEdit;
        TRadioGroup *ConnectGroupBox;
        TTabSheet *EmulatorTabSheet;
        TLabel *Label36;
        TLabel *Label34;
        TLabel *Label1;
        TLabel *Label2;
        TMaskEdit *DeviceIDEdit;
        TCSpinEdit *MaxShuttleSpeedSEdit;
        TCheckBox *ProxyFileCBox;
        TCheckBox *DifferenceFileCBox;
        TCSpinEdit *StopReactionFrameSEdit;
        TCSpinEdit *InstructionFrameDelaySEdit;
        TCheckBox *IgnoreControllerTimingCBox;
        TTabSheet *OverlayTabSheet;
        TLabel *Label22;
        TCheckBox *OverlayCBox;
        TPanel *OverlayPanel;
        TImage *OverlayImage;
        TCSpinEdit *OverlaySizeSEdit;
        TRadioGroup *OverlayBGColorGroup;
        TRadioGroup *OverlayFGColorGroup;
        TBitBtn *OverlayCenterButton;
        TTabSheet *VideoTabSheet;
        TLabel *Label37;
        TLabel *Label33;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *VideoOutDeviceName;
        TCheckBox *VideoWriteDiskCBox;
        TCheckBox *VideoReadDiskCBox;
        TEdit *VideoInDeviceName;
        TCheckBox *VideoInCBox;
        TCheckBox *VideoOutCBox;
        TCSpinEdit *VideoTimeCodeDelayPlaySEdit;
        TCSpinEdit *VideoTimeCodeDelayRecordSEdit;
        TImageList *TimeCodeImageList;
        TTabSheet *SynchSheet;
        TComboBox *VideoSynchCBox;
        TLabel *Label5;
        TLabel *Label6;
        TComboBox *VideoLoopBackCBox;
        TLabel *Label7;
        TLabel *Label8;
        TComboBox *PrecisionOutCBox;
        TComboBox *PrecisionInCBox;
        void __fastcall ConnectGroupBoxClick(TObject *Sender);
        void __fastcall OverlayImageMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall OverlayImageMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall OverlayImageMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall RedrawOverlay(TObject *Sender);
        void __fastcall OverlayCenterButtonClick(TObject *Sender);
private:	// User declarations
        CVTRInterface *_DDR;
        bool _bOverlayUpdate;
        Graphics::TBitmap* _WholeOverlayImage;
        int _OldOvlyX, _OldOvlyY;
        bool _bMoveOverlay;
        int _PortCount;

        void DrawOverlay(int X, int Y);

public:		// User declarations
        bool Modified;
        
        __fastcall TOptionsFrame(TComponent* Owner);
        void SetEmulator(CVTRInterface &DDR);
        void UpdateForm(void);
        void UpdateEmulator(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TOptionsFrame *OptionsFrame;
//---------------------------------------------------------------------------
#endif
