//---------------------------------------------------------------------------

#ifndef UnitDDRControlH
#define UnitDDRControlH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "VTimeCodeEdit.h"
#include <Buttons.hpp>
#include "sercom.h"
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFormDDRControl : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TEdit *EditHost;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *EditSocket;
    TButton *Button1;
    TButton *Button2;
    TPanel *videoPlayGroupPanel;
    TBitBtn *navStopButton;
    TBitBtn *navPlayReverseButton;
    TBitBtn *navPlayButton;
    TBitBtn *navRewindButton;
    TBitBtn *navFastForwardButton;
    TPanel *videoJogButtonGroup;
    TBitBtn *JogReverseButton;
    TBitBtn *JogForwardButton;
    TBitBtn *ContJogReverseButton;
    TBitBtn *ContJogForwardButton;
    TPanel *videoGotoButtonGroup;
    TBitBtn *GotoInButton;
    TBitBtn *GotoMarkIn;
    TBitBtn *GotoOutButton;
    TBitBtn *GotoMarkOut;
    TPanel *videoLoopButtonGroup;
    TBitBtn *PlayInOutButton;
    TBitBtn *LoopPlayButton;
    TBitBtn *PlayBetweenMarksButton;
    TBitBtn *LoopBetweenMarksButton;
    TPanel *videoMarkButtonGroup;
    TBitBtn *MarkInButton;
    TBitBtn *RemoveMarkInButton;
    TBitBtn *MarkOutButton;
    TBitBtn *RemoveMarkOutButton;
    TBitBtn *SetMarkInButton;
    TBitBtn *RecordButton;
    TBitBtn *SetMarkOutButton;
    TBitBtn *UnexpandMarksButton;
    TBitBtn *CueToButton;
    VTimeCodeEdit *CueupTimecode;
        TLabel *LabelTimecode;
    TButton *Button3;
        TTimer *Timer1;
        TLabel *AudioSlipLabel;
        TScrollBar *AudioSlipScrollBar;
        TLabel *AudioSlipValueLabel;
        TMenuItem *N1;
        TButton *AudioSlipButton;
        TPopupMenu *AudioSlipPopupMenu;
        TMenuItem *finecontrol1;
        TMenuItem *mediumcontrol1;
        TMenuItem *verycoarsecontrol1;
        TMenuItem *N2;
        TMenuItem *Returntocenter1;
        TMenuItem *Returntozero1;
        TMenuItem *Resetcenter1;
        TMenuItem *coarsecontrol1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall navStopButtonClick(TObject *Sender);
        void __fastcall navPlayReverseButtonClick(TObject *Sender);
        void __fastcall navPlayButtonClick(TObject *Sender);
        void __fastcall navRewindButtonClick(TObject *Sender);
        void __fastcall navFastForwardButtonClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ScrollBarScroll(TObject *Sender,
          TScrollCode ScrollCode, int &ScrollPos);
        void __fastcall AudioSlipButtonClick(TObject *Sender);
        void __fastcall finecontrol1Click(TObject *Sender);
        void __fastcall mediumcontrol1Click(TObject *Sender);
        void __fastcall coarsecontrol1Click(TObject *Sender);
        void __fastcall verycoarsecontrol1Click(TObject *Sender);
        void __fastcall Returntocenter1Click(TObject *Sender);
        void __fastcall Returntozero1Click(TObject *Sender);
        void __fastcall Resetcenter1Click(TObject *Sender);
        void __fastcall AudioSlipScrollBarChange(TObject *Sender);
    void __fastcall RecordButtonMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
private:	// User declarations
    class CCallSerCom *SerialPort;


    float getAudioSlip ();
    void setAudioPosition (int iNewFrame);
    int iAudioSlipFrameRange;
    float fAudioSlipCenter;

public:		// User declarations
    __fastcall TFormDDRControl(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDDRControl *FormDDRControl;
//---------------------------------------------------------------------------
#endif
