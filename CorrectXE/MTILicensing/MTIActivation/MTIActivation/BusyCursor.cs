﻿// <copyright file="BusyCursor.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, All Rights Reserved
// </copyright>
// <author>BlissDev2\Mertus</author>
// <date>2/8/2011 2:29:17 PM</date>
// <summary>Implements the BusyCursor class, StyleCop safe</summary>

namespace MTIActivation
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Create a busy cursor that goes away when dispose is called
    /// </summary>
    /// <remarks>
    /// using (new BusyCursor(this))
    /// {
    ///    Task
    /// }
    /// </remarks>
    public class BusyCursor : IDisposable
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// Storage for the original cursor
        /// </summary>
        private readonly Cursor oldCursor;

        /// <summary>
        /// Storage for the form
        /// </summary>
        private readonly FrameworkElement frameworkElement;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BusyCursor class
        /// </summary>
        /// <param name="frameworkElement">The framework element to set cursor on</param>
        public BusyCursor(FrameworkElement frameworkElement)
        {
            this.frameworkElement = frameworkElement;
            this.oldCursor = this.frameworkElement.Cursor;
            this.frameworkElement.Cursor = Cursors.Wait;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Show a regular cursor, and remove the reference to the form, so GC
            // will collect the memory when the form is closed.
            this.frameworkElement.Cursor = this.oldCursor;

            // Send Tell the GC that the Finalize process no longer needs
            // Send To be run for this object.
            GC.SuppressFinalize(this);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
