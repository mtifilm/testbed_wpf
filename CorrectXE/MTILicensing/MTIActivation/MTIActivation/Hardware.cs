﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Hardware.cs" company="MTI Film, LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/16/2012 10:27:00 AM</date>
// <summary>Various useful hardware routines.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Management;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows;

    // This is a COM related enum for IShellItem::GetDisplayName and SHGetNameFromIDList.  It is used in the GetVolumeLabelExplorerName().
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here. Windows Definition being matched.")]
    // ReSharper disable InconsistentNaming
    internal enum SIGDN : uint
    {
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        NORMALDISPLAY = 0,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        PARENTRELATIVEPARSING = 0x80018001,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        DESKTOPABSOLUTEPARSING = 0x80028000,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        PARENTRELATIVEEDITING = 0x80031001,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        DESKTOPABSOLUTEEDITING = 0x8004c000,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        FILESYSPATH = 0x80058000,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        URL = 0x80068000,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        PARENTRELATIVEFORADDRESSBAR = 0x8001c001,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        PARENTRELATIVE = 0x80080001,
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        PARENTRELATIVEFORUI = 0x80094001,
    }
    // ReSharper restore InconsistentNaming

    /// <summary>
    /// Various useful network routines.
    /// </summary>
    internal class Hardware
    {
        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The default volume labels
        /// </summary>
        internal static readonly List<string> DefaultVolumeLabels = new List<string> { "Local Disk", "Removable Disk" };

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Interfaces
        //////////////////////////////////////////////////
        #region Interfaces

        /// <summary>
        /// COM IShellItem interface definition.
        /// </summary>
        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("43826d1e-e718-42ee-bc55-a1e261c37bfe")]
        internal interface IShellItem
        {
            /// <summary>
            /// Binds the automatic handler.
            /// </summary>
            /// <param name="pbc">The PBC.</param>
            /// <param name="bhid">The bhid.</param>
            /// <param name="riid">The riid.</param>
            /// <param name="ppv">The PPV.</param>
            void BindToHandler(IntPtr pbc, [MarshalAs(UnmanagedType.LPStruct)]Guid bhid, [MarshalAs(UnmanagedType.LPStruct)]Guid riid, out IntPtr ppv);

            /// <summary>
            /// The get parent.
            /// </summary>
            /// <param name="ppsi">
            /// The ppsi.
            /// </param>
            void GetParent(out IShellItem ppsi);

            /// <summary>
            /// Gets the display name.
            /// </summary>
            /// <param name="sigdnName">Name of the sigdn.</param>
            /// <param name="ppszName">Name of the PPSZ.</param>
            void GetDisplayName(SIGDN sigdnName, out StringBuilder ppszName);

            /// <summary>
            /// Gets the attributes.
            /// </summary>
            /// <param name="sfgaoMask">The sfgao mask.</param>
            /// <param name="psfgaoAttribs">The psfgao attribs.</param>
            void GetAttributes(uint sfgaoMask, out uint psfgaoAttribs);

            /// <summary>
            /// Compares the specified psi.
            /// </summary>
            /// <param name="psi">The psi.</param>
            /// <param name="hint">The hint.</param>
            /// <param name="piOrder">The pi order.</param>
            [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
            void Compare(IShellItem psi, uint hint, out int piOrder);
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the volume information.
        /// </summary>
        /// <param name="volume">The volume.</param>
        /// <param name="volumeName">Name of the volume.</param>
        /// <param name="volumeNameSize">Size of the volume name.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="serialNumberLength">Length of the serial number.</param>
        /// <param name="flags">The flags.</param>
        /// <param name="fileSystemType">The fileSystemType.</param>
        /// <param name="fileSystemTypeSize">The fileSystemTypeSize.</param>
        /// <returns>The Volume Information.</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        internal static extern bool GetVolumeInformation(string volume, StringBuilder volumeName, uint volumeNameSize, out uint serialNumber, out uint serialNumberLength, out uint flags, StringBuilder fileSystemType, uint fileSystemTypeSize);

        /// <summary>
        /// Shes the name of the create item from parsing.
        /// </summary>
        /// <param name="pszPath">The PSZ path.</param>
        /// <param name="pbc">The PBC.</param>
        /// <param name="riid">The riid.</param>
        /// <param name="ppv">The PPV.</param>
        [DllImport("shell32.dll", CharSet = CharSet.Unicode, PreserveSig = false)]
        internal static extern void SHCreateItemFromParsingName([In][MarshalAs(UnmanagedType.LPWStr)] string pszPath, [In] IntPtr pbc, [In][MarshalAs(UnmanagedType.LPStruct)] Guid riid, [Out][MarshalAs(UnmanagedType.Interface, IidParameterIndex = 2)] out IShellItem ppv);

        /// <summary>
        /// Gets the Hard Disk Drive serial number.
        /// </summary>
        /// <param name="driveOrPath">The drive or path.</param>
        /// <returns>The specified hard disk's serial number.</returns>
        internal static string GetHddSerialNumber(string driveOrPath)
        {
            if (string.IsNullOrEmpty(driveOrPath))
            {
                return string.Empty;
            }

            // Get just the drive letter with the : and \
            var drive = GetDriveStandardized(driveOrPath);

            // Change ":\" to just ":" - this is the format needed for the call to ManagementObject()
            drive = drive.EndsWith(@":\") ? drive.Substring(0, drive.Length - 1) : drive;

            var disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + drive + "\"");
            disk.Get();

            return disk["VolumeSerialNumber"] == null ? string.Empty : disk["VolumeSerialNumber"].ToString();
        }

        /// <summary>
        /// Gets the label of the volume.
        /// </summary>
        /// <param name="driveOrPath">The drive or path.</param>
        /// <returns>The volume label.</returns>
        internal static string GetVolumeLabel(string driveOrPath)
        {
            if (string.IsNullOrEmpty(driveOrPath))
            {
                return string.Empty;
            }

            // Get just the drive letter with the : and \
            var drive = GetDriveStandardized(driveOrPath);

            var driveInfo = new DriveInfo(drive);

            return driveInfo.VolumeLabel;
        }

        /// <summary>
        /// Gets the label of the volume in the same format that Windows Explorer does.
        /// </summary>
        /// <param name="driveOrPath">The drive or path.</param>
        /// <returns>The volume label.</returns>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
        internal static string GetVolumeLabelExplorerName(string driveOrPath)
        {
            if (string.IsNullOrEmpty(driveOrPath))
            {
                return string.Empty;
            }

            // Get just the drive letter with the : and \
            var drive = GetDriveStandardized(driveOrPath);

            IShellItem iShellItem;
            SHCreateItemFromParsingName(drive, IntPtr.Zero, new Guid("43826d1e-e718-42ee-bc55-a1e261c37bfe"), out iShellItem);
            StringBuilder ret;
            iShellItem.GetDisplayName(SIGDN.PARENTRELATIVEEDITING, out ret);
            return ret.ToString();
        }

        /// <summary>
        /// Determines whether the specified drive has its default volume label (e.g. "Local Disk").
        /// </summary>
        /// <param name="drive">The drive.</param>
        /// <returns>True if drive has default volume label, false if it doesn't or drive isn't ready.</returns>
        internal static bool IsDefaultVolumeLabel(string drive)
        {
            if (string.IsNullOrWhiteSpace(drive))
            {
                return false;
            }

            drive = GetDriveStandardized(drive);

            var driveInfo = new DriveInfo(drive);

            // Can't check if not ready
            if (!driveInfo.IsReady)
            {
                return false;
            }

            var driveType = driveInfo.DriveType;
            var label = GetVolumeLabelExplorerName(drive);

            var isDefaultValue = false;
            switch (driveType)
            {
                case DriveType.Fixed:
                    if (label == "Local Disk")
                    {
                        isDefaultValue = true;
                    }

                    break;

                case DriveType.Removable:
                    if (label == "Removable Disk")
                    {
                        isDefaultValue = true;
                    }

                    break;
            }

            return isDefaultValue;
        }

        /// <summary>
        /// Sets the drive label.
        /// </summary>
        /// <param name="driveToSet">The drive to set.</param>
        /// <param name="driveLabel">The drive label.</param>
        /// <returns>True if setting drive label succeeded, false otherwise.</returns>
        internal static bool SetDriveLabel(string driveToSet, string driveLabel)
        {
            driveToSet = GetDriveStandardized(driveToSet);
            if (driveToSet.Length != 3)
            {
                return false;
            }

            var drive = new DriveInfo(driveToSet);

            // Make sure drive is ready and not LTFS (LTFS doesn't allow setting the drive label through Windows).
            if (drive.IsReady && drive.DriveFormat != "LTFS")
            {
                try
                {
                    drive.VolumeLabel = driveLabel;
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to set drive label: " + ex.GetInnermostExceptionMessage());
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Get free bytes on a drive or junction point
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="freeSpace">The freeSpace.</param>
        /// <returns>true if successful</returns>
        internal static bool DriveFreeBytes(string folderName, out ulong freeSpace)
        {
            freeSpace = 0;
            var actualFolderName = GetActualFolderName(folderName);
            ulong free, dummy1, dummy2;
            if (NativeMethods.GetDiskFreeSpaceEx(actualFolderName, out free, out dummy1, out dummy2))
            {
                freeSpace = free;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the total bytes ona drive or junction point
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="totalSpace">The total space.</param>
        /// <returns>true if successful</returns>
        internal static bool DriveTotalBytes(string folderName, out ulong totalSpace)
        {
            totalSpace = 0;
            var actualFolderName = GetActualFolderName(folderName);
            ulong total, dummy1, dummy2;
            if (NativeMethods.GetDiskFreeSpaceEx(actualFolderName, out dummy1, out total, out dummy2))
            {
                totalSpace = total;
                return true;
            }

            return false;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Gets the actual folder name.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <returns>A converted folder name.</returns>
        private static string GetActualFolderName(string folderName)
        {
            var temp = folderName;
            if (string.IsNullOrWhiteSpace(temp))
            {
                throw new ArgumentNullException("folderName");
            }

            if (JunctionPoint.Exists(temp))
            {
                // it's a junction point, not a normal drive, so convert it to the form \\?\Volume{XXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
                var targetVolume = JunctionPoint.GetTarget(temp);
                temp = @"\\?\" + targetVolume;
            }

            if (!temp.EndsWith("\\"))
            {
                temp += '\\';
            }

            return temp;
        }

        /// <summary>
        /// Gets the drive from the drive letter or path in standard format - e.g. C:\
        /// </summary>
        /// <param name="driveOrPath">The drive or path.</param>
        /// <returns>Just the drive with : and \.</returns>
        private static string GetDriveStandardized(string driveOrPath)
        {
            // Handle case where just "C" is passed in.
            if (driveOrPath.Length == 1)
            {
                driveOrPath += ":";
            }

            // This will happen if just "C:" is passed in.
            if (driveOrPath.Length == 2 && driveOrPath[1] == ':')
            {
                driveOrPath += @"\";
            }

            // Get just the drive letter with the : and \
            var drive = Path.GetPathRoot(driveOrPath);

            return drive;
        }

        #endregion
    }
}
