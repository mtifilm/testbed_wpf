﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QlmProductData.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/10/2012 4:28:18 PM</date>
// <summary>Specific Product data for a QLM licensed product.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Specific Product data for a QLM licensed product.
    /// </summary>
    internal class QlmProductData
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// Product Id.
        /// </summary>
        private int id;

        /// <summary>
        /// Major Version.
        /// </summary>
        private int majorVersion;

        /// <summary>
        /// Minor Version.
        /// </summary>
        private int minorVersion;

        /// <summary>
        /// Product GUID.
        /// </summary>
        private uint[] guid;

        /// <summary>
        /// Encryption Key
        /// </summary>
        private uint[] encryptionKey;

        /// <summary>
        /// Public Key
        /// </summary>
        private uint[] publicKey;

        /// <summary>
        /// Dictionary to map feature number to feature name
        /// </summary>
        private Dictionary<int, string> features = new Dictionary<int, string>();

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QlmProductData class.
        /// </summary>
        internal QlmProductData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QlmProductData"/> class.
        /// </summary>
        /// <param name="productData">The product data.</param>
        internal QlmProductData(QlmProductData productData)
        {
            // Copy over all properties.
            this.Name = productData.Name;
            this.id = productData.id;
            this.majorVersion = productData.majorVersion;
            this.minorVersion = productData.minorVersion;
            this.guid = productData.guid;
            this.encryptionKey = productData.encryptionKey;
            this.publicKey = productData.publicKey;
            this.features = productData.features;
            this.HasSignificantLicensingChange = productData.HasSignificantLicensingChange;
        }

        /// <summary>
        /// Initializes a new instance of the QlmProductData class
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="majorVer">The major ver.</param>
        /// <param name="minorVer">The minor ver.</param>
        internal QlmProductData(string productName, int majorVer, int minorVer)
        {
            this.Name = productName;
            this.id = 0;
            this.majorVersion = 0;
            this.minorVersion = 0;
            this.guid = new uint[MtiLicense.UintArraySize];
            this.encryptionKey = new uint[MtiLicense.UintArraySize];
            this.publicKey = new uint[MtiLicense.UintArraySize];
            this.HasSignificantLicensingChange = false;

            this.LoadProductData(this.Name, majorVer, minorVer);
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        internal string Name { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The Product id.
        /// </value>
        internal int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
            }
        }

        /// <summary>
        /// Gets or sets the major version.
        /// </summary>
        /// <value>
        /// The major version.
        /// </value>
        internal int MajorVersion
        {
            get
            {
                return this.majorVersion;
            }
 
            set
            {
                this.majorVersion = value;
            }
        }

        /// <summary>
        /// Gets or sets the minor version.
        /// </summary>
        /// <value>
        /// The minor version.
        /// </value>
        internal int MinorVersion
        {
            get
            {
                return this.minorVersion;
            }

            set
            {
                this.minorVersion = value;
            }
        }

        /// <summary>
        /// Gets or sets the GUID.
        /// </summary>
        /// <value>
        /// The Product GUID.
        /// </value>
        internal uint[] Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                this.guid = value;
            }
        }

        /// <summary>
        /// Gets or sets the encryption key.
        /// </summary>
        /// <value>
        /// The encryption key.
        /// </value>
        internal uint[] EncryptionKey
        {
            get
            {
                return this.encryptionKey;
            }

            set
            {
                this.encryptionKey = value;
            }
        }

        /// <summary>
        /// Gets or sets the internal key.
        /// </summary>
        /// <value>
        /// The internal key.
        /// </value>
        internal uint[] PublicKey
        {
            get
            {
                return this.publicKey;
            }

            set
            {
                this.publicKey = value;
            }
        }

        /// <summary>
        /// Gets or sets the features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        internal Dictionary<int, string> Features
        {
            get
            {
                return this.features;
            }

            set
            {
                this.features = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has a significant licensing change which would
        /// preclude loading a previous version's license key as a starting point.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has a significant licensing change; otherwise, <c>false</c>.
        /// </value>
        internal bool HasSignificantLicensingChange { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the index of the feature.
        /// </summary>
        /// <param name="featureSet">The feature set.</param>
        /// <param name="featureId">The feature id.</param>
        /// <returns>The feature index.</returns>
        internal static int GetFeatureIndex(int featureSet, int featureId)
        {
            // These checks are for internal programming problems.
            if (featureSet < 0 || featureSet > 3)
            {
                throw new ArgumentException("Impossible value for featureSet, must be between 0 and 3");
            }

            var validFeatureIds = new List<int> { 1, 2, 4, 8, 16, 32, 64, 128 };

            if (!validFeatureIds.Contains(featureId))
            {
                throw new ArgumentException("Impossible value for featureId, must be 1, 2, 4, 8, 16, 32, 64 or 128");
            }

            return (featureSet << 8) + featureId;
        }

        /// <summary>
        /// Gets the name of the feature.
        /// </summary>
        /// <param name="featureSet">The feature set.</param>
        /// <param name="featureId">The feature id.</param>
        /// <returns>The name of the feature.</returns>
        internal string GetFeatureName(int featureSet, int featureId)
        {
            return this.features[GetFeatureIndex(featureSet, featureId)];
        }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <returns>The full name.</returns>
        internal string GetFullName()
        {
            return string.Format("{0}-{1}.{2}", this.Name, this.MajorVersion, this.MinorVersion);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Loads the product data.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="majorVer">The major version.</param>
        /// <param name="minorVer">The minor version.</param>
        private void LoadProductData(string productName, int majorVer, int minorVer)
        {
            var qlmLicenseProductsTable = new QlmLicenseProductsTable();
            var table = qlmLicenseProductsTable.ProductsTable;

            var productEntry =
                (from r in table
                 where r.Name == productName && r.MajorVersion == majorVer && r.MinorVersion == minorVer
                 select r).FirstOrDefault();

            if (productEntry != null)
            {
                this.id = productEntry.Id;
                this.majorVersion = productEntry.MajorVersion;
                this.minorVersion = productEntry.MinorVersion;
                this.guid = productEntry.Guid;
                this.encryptionKey = productEntry.EncryptionKey;
                this.publicKey = productEntry.PublicKey;
                this.features = productEntry.Features;
                this.HasSignificantLicensingChange = productEntry.HasSignificantLicensingChange;
            }
            else
            {
                var sb = new StringBuilder();
                sb.AppendFormat(
                    "Product {0} is an unknown product for this release",
                    MtiLicense.GetFormattedProductStringShort(productName, majorVer, minorVer));
                var stringBuilder = sb.AppendLine();
                sb.AppendLine("Valid products are");
                foreach (var pe in table)
                {
                    sb.AppendLine("\t" + pe.GetFullName());
                }

                throw new InvalidDataException(sb.ToString());
            }
        }

        #endregion
    }
}
