﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QlmLicenseProductsTable.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/10/2012 11:47:43 AM</date>
// <summary>A table of QLM products including their internal values.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    /// <summary>
    /// A table of QLM products including their internal values. 
    /// </summary>
    internal class QlmLicenseProductsTable
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The table of Latest Products (highest version number for a product).
        /// </summary>
        private readonly Dictionary<string, string> latestProductsTable = new Dictionary<string, string>();

        /// <summary>
        /// List of QLM products.
        /// </summary>
        private readonly List<QlmProductData> productsTable = new List<QlmProductData>();

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QlmLicenseProductsTable class
        /// </summary>
        internal QlmLicenseProductsTable()
        {
            this.LoadQlmProducts();
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets the products table.
        /// </summary>
        internal List<QlmProductData> ProductsTable
        {
            get
            {
                return this.productsTable;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the latest major version for a specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The latest major version.</returns>
        internal int GetLatestProductVersionMajor(string product)
        {
            if (this.latestProductsTable.ContainsKey(product))
            {
                return MtiLicense.GetMajorVersionFromCombinedVersion(this.latestProductsTable[product]);
            }

            return 0;
        }

        /// <summary>
        /// Gets the latest minor version for a specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The latest minor version.</returns>
        internal int GetLatestProductVersionMinor(string product)
        {
            if (this.latestProductsTable.ContainsKey(product))
            {
                return MtiLicense.GetMinorVersionFromCombinedVersion(this.latestProductsTable[product]);
            }

            return 0;
        }

        /// <summary>
        /// Gets the latest revision for a specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The latest revision.</returns>
        internal int GetLatestProductRevision(string product)
        {
            if (this.latestProductsTable.ContainsKey(product))
            {
                return MtiLicense.GetRevisionFromCombinedVersion(this.latestProductsTable[product]);
            }

            return 0;
        }

        /// <summary>
        /// Gets the latest QLM product data for a specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The QLM Product data record.</returns>
        internal QlmProductData GetLatestQlmProductData(string product)
        {
            if (product == null)
            {
                return null;
            }

            if (this.latestProductsTable.ContainsKey(product))
            {
                var version = this.latestProductsTable[product];

                return this.GetQlmProductData(product, version);
            }

            return null;
        }

        /// <summary>
        /// Gets the QLM product data for a given product and version.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="majorVersion">The major version.</param>
        /// <param name="minorVersion">The minor version.</param>
        /// <returns>The QLM Product data record.</returns>
        internal QlmProductData GetQlmProductData(string product, int majorVersion, int minorVersion)
        {
            var productData = (from p in this.productsTable
                               where p.Name == product && p.MajorVersion == majorVersion && p.MinorVersion == minorVersion
                               select p).FirstOrDefault();

            return productData;
        }

        /// <summary>
        /// Gets the QLM product data for a given product and version.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="version">The version.</param>
        /// <returns>The QLM Product data record.</returns>
        internal QlmProductData GetQlmProductData(string product, string version)
        {
            var parts = version.Split('.');

            if (parts.Length < 2)
            {
                return null;
            }

            int majorVersion;
            int minorVersion;
            int.TryParse(parts[0], out majorVersion);
            int.TryParse(parts[1], out minorVersion);

            return this.GetQlmProductData(product, majorVersion, minorVersion);
        }

        /// <summary>
        /// Builds the latest products table.
        /// </summary>
        /// <param name="oldVersion">The old version.</param>
        /// <returns>True if there are significant changes between the old version and the new version</returns>
        internal bool HasSignificantChangeSince(QlmProductData oldVersion)
        {
            if (oldVersion == null)
            {
                return true;
            }

            var name = oldVersion.Name;
            var major = oldVersion.MajorVersion;
            var minor = oldVersion.MinorVersion;

            var latestMajorVersionIsNewer = this.GetLatestProductVersionMajor(name) > major;

            var newChangedProducts =
                this.productsTable.Where(
                    p =>
                    p.Name == name && (p.MajorVersion > major || (p.MajorVersion == major && p.MinorVersion > minor))).Where(p => p.HasSignificantLicensingChange);

            var hasSignificantChanges = latestMajorVersionIsNewer || !newChangedProducts.IsNullOrEmpty();

            return hasSignificantChanges;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Loads the QLM products.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1123:DoNotPlaceRegionsWithinElements", Justification = "Reviewed. Suppression is OK here.  Used for Deprecated license info.")]
        private void LoadQlmProducts()
        {
            //  NOTES ON ADDING NEW VERSIONS TO THIS TABLE
            //  -------------------------------------------
            //  
            //  *Add* new product versions here.
            //  Then *update* these locations too (don't need old versions for these ones):
            //   - MtiLicenseValidate.UnitTestingProductInfo
            //   - MtiLicenseLib.cpp
            //   
            //  Use the solution / project "EncryptLicenseString" to encrypt these variables:
            //   - Guid
            //   - EncryptionKey
            //   - PublicKey 
            //

            /////////////////////////////////////////////
            //// For MTI Unit Testing
            /////////////////////////////////////////////

            #region MTI Unit Testing Product License Info

            // MTIInternalTest 1.0
            var product = new QlmProductData
            {
                Name = "MTIInternalTest",
                MajorVersion = 1,
                MinorVersion = 0,
                Id = 4,
                //// Guid = "{03548043-7481-47EE-8552-4F17C4940E8D}",
                Guid = new uint[] { 0x40d87a23, 0x45dd9695, 0x93929e96, 0x92958b91, 0x929e978b, 0x9291e3e3, 0x8b9e9393, 0x948b92e0, 0x9791e592, 0x9f9296e3, 0x9ee2db7d, 0xfee6fe65, 0x7b577941, 0xab48a1dc, 0x6f294ef9, 0xf668dc41 },
                //// EncryptionKey = "caS1no653r$yAL",
                EncryptionKey = new uint[] { 0x2d255231, 0x62c5c7f5, 0x97c8c990, 0x9395d482, 0xdfe7eaef, 0xf2ed3a63, 0x54fbc670, 0x2af86ddc, 0x3bcb3f66, 0xcb30f5e5, 0xe326faca, 0xda2c7269, 0x4e413668, 0x21defad4, 0xe5d05d27, 0xdda1fe31 },
                //// PublicKey = "axLPsHtapMhXFw==",
                PublicKey = new uint[] { 0xfc754ccb, 0xf5c7deea, 0xf6d5eed2, 0xc7d6ebce, 0xfee0d19b, 0x9b4723c6, 0x435c4266, 0xfc3fc25c, 0x3a712dbb, 0x7dadbf6f, 0x46c56de7, 0x4521fdc3, 0x4851b747, 0x3935e935, 0xd17ac7c5, 0x3f26f8d0 },
                Features = new Dictionary<int, string>
                               {
                                   { QlmProductData.GetFeatureIndex(0, 1), "SeanConnery" },
                                   { QlmProductData.GetFeatureIndex(0, 2), "RogerMoore" },
                                   { QlmProductData.GetFeatureIndex(0, 4), "PierceBrosnan" },
                                   { QlmProductData.GetFeatureIndex(0, 8), "DanielCraig" }
                               }
            };

            this.productsTable.Add(product);

            // MTIInternalTest 1.1
            product = new QlmProductData(product)
            {
                MinorVersion = 1,
                //// Guid = "{5309E1AB-247C-4939-8FEE-E89345464794}",
                Guid = new uint[] { 0xf6707aff, 0x26dd9395, 0x969fe397, 0xe7e48b94, 0x9291e58b, 0x929f959f, 0x8b9ee0e3, 0xe38be39e, 0x9f959293, 0x92909291, 0x9f92db30, 0x4379dbe4, 0x2e5fee42, 0x46d2cfdc, 0xf856f2df, 0xc34f45e4 },
            };

            this.productsTable.Add(product);

            // MTIInternalTest 2.0
            product = new QlmProductData(product)
            {
                MajorVersion = 2,
                MinorVersion = 0,
                //// Guid = "{A5774CF7-0715-4033-B989-693459760212}",
                Guid = new uint[] { 0x52727a68, 0xf5dde793, 0x919192e5, 0xe0918b96, 0x9197938b, 0x92969595, 0x8be49f9e, 0x9f8b909f, 0x9592939f, 0x91909694, 0x9794db27, 0x39555046, 0xf2c9adc8, 0x7daac14a, 0x75376874, 0xc263c166 }
            };

            this.productsTable.Add(product);

            // MTIInternalTest 2.1
            product = new QlmProductData(product)
            {
                MinorVersion = 1,
                //// Guid = "{E515749D-6530-41B2-A6CF-65B8410072D6}",
                Guid = new uint[] { 0x58727ad9, 0x61dde393, 0x97939192, 0x9fe28b90, 0x9395968b, 0x9297e494, 0x8be790e5, 0xe08b9093, 0xe49e9297, 0x96969194, 0xe290dbea, 0xc36aeef1, 0xad275645, 0x686a6f49, 0xefff23ba, 0xc6d83f54 }
            };

            this.productsTable.Add(product);

            // MTIInternalTest 3.0
            product = new QlmProductData(product)
            {
                MajorVersion = 3,
                MinorVersion = 0,
                //// Guid = "{298E931C-7D7C-4FF7-89F5-9257D9B7BBF6}",
                Guid = new uint[] { 0x31507a31, 0x6ddd949f, 0x9ee39f95, 0x97e58b91, 0xe291e58b, 0x92e0e091, 0x8b9e9fe0, 0x938b9f94, 0x9391e29f, 0xe491e4e4, 0xe090db36, 0x76c3766e, 0xcff52978, 0xcd62c527, 0xfb6cadc1, 0x36cbcae1 }
            };

            this.productsTable.Add(product);

            // MTIInternalTest 3.1
            product = new QlmProductData(product)
            {
                MajorVersion = 3,
                MinorVersion = 1,
                //// Guid = "{FDBD124F-A108-41CE-AE93-91D87F2A8359}",    NOTE: *** CHANGES WITH VERSION UPDATE ***
                Guid = new uint[] { 0x7a237ad5, 0x41dde0e2, 0xe4e29794, 0x92e08be7, 0x97969e8b, 0x9297e5e3, 0x8be7e39f, 0x958b9f97, 0xe29e91e0, 0x94e79e95, 0x939fdb2a, 0x4b382cd3, 0xeec4d2ec, 0x583fd0d4, 0x7367d54f, 0x4af42f70 },
                Features = new Dictionary<int, string>
                               {
                                   { QlmProductData.GetFeatureIndex(0, 1), "DrNo" },
                                   { QlmProductData.GetFeatureIndex(0, 2), "GoldFinger" },
                                   { QlmProductData.GetFeatureIndex(0, 4), "Le Chiffre" },
                                   { QlmProductData.GetFeatureIndex(0, 8), "Trigger" }
                               }
            };

            this.productsTable.Add(product);

            // MTIITest2 1.0
            product = new QlmProductData(product) { Name = "MTITest2", MajorVersion = 1, MinorVersion = 0, HasSignificantLicensingChange = false };

            this.productsTable.Add(product);

            // MTITest2 1.1
            product = new QlmProductData(product) { MinorVersion = 1, HasSignificantLicensingChange = true };

            this.productsTable.Add(product);

            // MTITest2 1.2
            product = new QlmProductData(product) { MinorVersion = 2, HasSignificantLicensingChange = false };

            this.productsTable.Add(product);

            #endregion

            /////////////////////////////////////////////
            //// DRS Nova
            /////////////////////////////////////////////

            // DRSNova 1.0
            product = new QlmProductData
            {
                Name = "DRSNova",
                MajorVersion = 1,
                MinorVersion = 0,
                Id = 15,
                //// Guid = "{55BC2636-3FB5-422B-91C0-0E5C7778158C}"   NOTE: *** CHANGES WITH VERSION UPDATE ***
                Guid =
                    new uint[]
                              {
                                  0x7de07a75, 0xe7dd9393, 0xe4e59490, 0x95908b95, 0xe0e4938b, 0x929494e4,
                                  0x8b9f97e5, 0x968b96e3, 0x93e59191, 0x919e9793, 0x9ee5db52, 0xff545d63,
                                  0xfc4cc1dc, 0x4546e548, 0x64feead5, 0x55d26bd0
                              },
                //// EncryptionKey = "b3atl3^Rta9*",
                EncryptionKey =
                    new uint[]
                              {
                                  0x5c5c50e2, 0xbbc495c7, 0xd2ca95f8, 0xf4d2c79f, 0x8c55336a, 0x2cef344c,
                                  0xc83751c1, 0x5ce2486b, 0xf9d84e57, 0x5d4570c6, 0xf52ce44b, 0x4073f85c,
                                  0x30c96e4e, 0xab59deb5, 0xefe7dcaa, 0xeb6722ce
                              },
                //// PublicKey = "QSErlcE0vbFysQ==",
                PublicKey =
                    new uint[]
                              {
                                  0x67574c2a, 0x6df7f5e3, 0xd4cac5e3, 0x96d0c4e0, 0xdfd5f79b, 0x9b73cb74,
                                  0xaa6bc146, 0xc2aaf3c3, 0x22caf2e9, 0x356e56ee, 0xfd336cf4, 0x48e8224c,
                                  0x46d0a1cb, 0x38c773f6, 0xd825f3e2, 0x3f58446b
                              },
                Features =
                    new Dictionary<int, string>
                              {
                                  {
                                      QlmProductData.GetFeatureIndex(0, 1),
                                      "Base"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 1),
                                      "CBTool"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 2),
                                      "MTIClean"
                                  },
                              },
            };

            this.productsTable.Add(product);

            // DRSNova 2.0
            product = new QlmProductData
            {
                Name = "DRSNova",
                MajorVersion = 2,
                MinorVersion = 0,
                Id = 15,

                //// Guid = "{55BC2636-3FB5-422B-91C0-0E5C7778158C}"   NOTE: *** CHANGES WITH VERSION UPDATE ***
                Guid =
                    new uint[]
                              {
                                  0x7de07a75, 0xe7dd9393, 0xe4e59490, 0x95908b95, 0xe0e4938b, 0x929494e4,
                                  0x8b9f97e5, 0x968b96e3, 0x93e59191, 0x919e9793, 0x9ee5db52, 0xff545d63,
                                  0xfc4cc1dc, 0x4546e548, 0x64feead5, 0x55d26bd0
                              },

                ////Private Key:  AgUApfmPsw==
                EncryptionKey = 
                      new uint[]
                              {
                                  0x67c050e8, 0x73e7c1f3, 0xe7d6c0cb, 0xf6d5d19b, 0x9bef6137, 0x21eab563,
                                  0x3adadbfc, 0x5cec2aad, 0x6bd5f9d4, 0x66e9dd26, 0xfbcf29ab, 0x7aaa4be5,
                                  0x4bbff864, 0xe1416543, 0x21645141, 0xde4673c0
                              },

                //// PublicKey = "QSErlcE0vbFysQ==",
                PublicKey =
                    new uint[]
                              {
                                  0x67574c2a, 0x6df7f5e3, 0xd4cac5e3, 0x96d0c4e0, 0xdfd5f79b, 0x9b73cb74,
                                  0xaa6bc146, 0xc2aaf3c3, 0x22caf2e9, 0x356e56ee, 0xfd336cf4, 0x48e8224c,
                                  0x46d0a1cb, 0x38c773f6, 0xd825f3e2, 0x3f58446b
                              },
                Features =
                    new Dictionary<int, string>
                              {
                                  {
                                      QlmProductData.GetFeatureIndex(0, 1),
                                      "Base"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 1),
                                      "CBTool"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 2),
                                      "MTIClean"
                                  },
                              },
            };

            this.productsTable.Add(product);

            // DRSNova 4.0
            product = new QlmProductData
            {
                Name = "DRSNova",
                MajorVersion = 4,
                MinorVersion = 0,
                Id = 15,

                //// Guid = "{55BC2636-3FB5-422B-91C0-0E5C7778158C}"   NOTE: *** CHANGES WITH VERSION UPDATE ***
                Guid =
                    new uint[]
                              {
                                  0x7de07a75, 0xe7dd9393, 0xe4e59490, 0x95908b95, 0xe0e4938b, 0x929494e4,
                                  0x8b9f97e5, 0x968b96e3, 0x93e59191, 0x919e9793, 0x9ee5db52, 0xff545d63,
                                  0xfc4cc1dc, 0x4546e548, 0x64feead5, 0x55d26bd0
                              },

                ////Private Key:  AgUApfmPsw==
                EncryptionKey =
                      new uint[]
                              {
                                  0x67c050e8, 0x73e7c1f3, 0xe7d6c0cb, 0xf6d5d19b, 0x9bef6137, 0x21eab563,
                                  0x3adadbfc, 0x5cec2aad, 0x6bd5f9d4, 0x66e9dd26, 0xfbcf29ab, 0x7aaa4be5,
                                  0x4bbff864, 0xe1416543, 0x21645141, 0xde4673c0
                              },

                //// PublicKey = "QSErlcE0vbFysQ==",
                PublicKey =
                    new uint[]
                              {
                                  0x67574c2a, 0x6df7f5e3, 0xd4cac5e3, 0x96d0c4e0, 0xdfd5f79b, 0x9b73cb74,
                                  0xaa6bc146, 0xc2aaf3c3, 0x22caf2e9, 0x356e56ee, 0xfd336cf4, 0x48e8224c,
                                  0x46d0a1cb, 0x38c773f6, 0xd825f3e2, 0x3f58446b
                              },
                Features =
                    new Dictionary<int, string>
                              {
                                  {
                                      QlmProductData.GetFeatureIndex(0, 1),
                                      "Base"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 1),
                                      "CBTool"
                                  },
                                  {
                                      QlmProductData.GetFeatureIndex(1, 2),
                                      "MTIClean"
                                  },
                              },
            };

            this.productsTable.Add(product);

            // Build the Latest Products Table.
            this.BuildLatestProductsTable();
        }

        /// <summary>
        /// Builds the latest products table.
        /// </summary>
        private void BuildLatestProductsTable()
        {
            foreach (var product in this.productsTable)
            {
                if (this.latestProductsTable.ContainsKey(product.Name))
                {
                    var majorVersion = MtiLicense.GetMajorVersionFromCombinedVersion(this.latestProductsTable[product.Name]);
                    var minorVersion = MtiLicense.GetMinorVersionFromCombinedVersion(this.latestProductsTable[product.Name]);

                    // If version is greater than what we already have, then update entry.
                    if ((product.MajorVersion > majorVersion) || ((product.MajorVersion == majorVersion) && (product.MinorVersion > minorVersion)))
                    {
                        this.latestProductsTable[product.Name] = MtiLicense.GetCombinedVersion(product.MajorVersion, product.MinorVersion);
                    }
                }
                else
                {
                    this.latestProductsTable.Add(product.Name, MtiLicense.GetCombinedVersion(product.MajorVersion, product.MinorVersion));
                }
            }
        }

        #endregion
    }
}
