﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiLicenseData.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>12/10/2012 9:33:46 AM</date>
// <summary>Data related to a license instance.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;

    /// <summary>
    /// Data related to a license instance.
    /// </summary>
    internal class MtiLicenseData
    {
        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the license product info.
        /// </summary>
        internal MtiLicenseProductInfo LicenseProductInfo { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        internal string DisplayProductName { get; set; }

        /// <summary>
        /// Gets or sets the activation key.
        /// </summary>
        /// <value>
        /// The activation key.
        /// </value>
        internal string ActivationKey { get; set; }

        /// <summary>
        /// Gets or sets the license key.
        /// </summary>
        /// <value>
        /// The license key.
        /// </value>
        internal string LicenseKey { get; set; }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        internal string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        internal string UserName { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>
        /// The computer id.
        /// </value>
        internal string ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        internal DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the maintenance end date.
        /// </summary>
        /// <value>
        /// The maintenance end date.
        /// </value>
        internal DateTime MaintenanceEndDate { get; set; }

        /// <summary>
        /// Gets the hostname.
        /// </summary>
        internal string Hostname
        {
            get
            {
                return Environment.MachineName;
            } 
        }

        #endregion
    }
}
