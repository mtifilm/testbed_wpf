﻿namespace MTIActivation
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security;

    using Microsoft.Win32;

    #region window messages defines

    /// <summary>
    /// The window message defines
    /// </summary>
    internal enum WindowsMessages
    {
        /// <summary>
        /// Send Activate 
        /// </summary>
        Activate = 0x6,

        /// <summary>
        /// Send Activate App
        /// </summary>
        ActivateApp = 0x1C,

        /// <summary>
        /// Send Afx First
        /// </summary>
        AfxFirst = 0x360,

        /// <summary>
        /// Send Afx Last
        /// </summary>
        AfxLast = 0x37F,

        /// <summary>
        /// Send Application message
        /// </summary>
        App = 0x8000,

        /// <summary>
        /// Ask for cb Format Name
        /// </summary>
        AskcbFormatName = 0x30C,

        /// <summary>
        /// Send Cancel Journal
        /// </summary>
        CancelJournal = 0x4B,

        /// <summary>
        /// Send Cancel Mode
        /// </summary>
        CancelMode = 0x1F,

        /// <summary>
        /// Send Capture Changed
        /// </summary>
        CaptureChanged = 0x215,

        /// <summary>
        /// Send Changecb Chain
        /// </summary>
        ChangecbChain = 0x30D,

        /// <summary>
        /// Send Char message
        /// </summary>
        Char = 0x102,

        /// <summary>
        /// Send Char to Item
        /// </summary>
        CharToItem = 0x2F,

        /// <summary>
        /// Send Child Activate
        /// </summary>
        ChildActivate = 0x22,

        /// <summary>
        /// Send Clear control
        /// </summary>
        Clear = 0x303,

        /// <summary>
        /// Send Close control
        /// </summary>
        Close = 0x10,

        /// <summary>
        /// Send Command 
        /// </summary>
        Command = 0x111,

        /// <summary>
        /// Send Compacting
        /// </summary>
        Compacting = 0x41,

        /// <summary>
        /// Send Compare Item
        /// </summary>
        CompareItem = 0x39,

        /// <summary>
        /// Send Context Menu
        /// </summary>
        ContextMenu = 0x7B,

        /// <summary>
        /// Send Copy message
        /// </summary>
        Copy = 0x301,

        /// <summary>
        /// Send Copy Data
        /// </summary>
        CopyData = 0x4A,

        /// <summary>
        /// Send Create message
        /// </summary>
        Create = 0x1,

        /// <summary>
        /// Send Control Color Button
        /// </summary>
        ControlColorButton = 0x135,

        /// <summary>
        /// Send Control Color Dialog
        /// </summary>
        ControlColorDialog = 0x136,

        /// <summary>
        /// Send Control Color Edit
        /// </summary>
        ControlColorEdit = 0x133,

        /// <summary>
        /// Send Control Color List Box
        /// </summary>
        ControlColorListBox = 0x134,

        /// <summary>
        /// Send Control Color MessageBox
        /// </summary>
        ControlColorMessageBox = 0x132,

        /// <summary>
        /// Send Control Color ScrollBar
        /// </summary>
        ControlColorScrollBar = 0x137,

        /// <summary>
        /// Send Control Color Static
        /// </summary>
        ControlColorStatic = 0x138,

        /// <summary>
        /// Send Cut message
        /// </summary>
        Cut = 0x300,

        /// <summary>
        /// Send Dead Char
        /// </summary>
        DeadChar = 0x103,

        /// <summary>
        /// Send Delete Item
        /// </summary>
        DeleteItem = 0x2D,

        /// <summary>
        /// Send Destroy
        /// </summary>
        Destroy = 0x2,

        /// <summary>
        /// Send Destroy Clipboard
        /// </summary>
        DestroyClipboard = 0x307,

        /// <summary>
        /// Send Device Change
        /// </summary>
        DeviceChange = 0x219,

        /// <summary>
        /// Send Dev Mode Change
        /// </summary>
        DevModeChange = 0x1B,

        /// <summary>
        /// Send Display Change
        /// </summary>
        DisplayChange = 0x7E,

        /// <summary>
        /// Send Draw Clipboard
        /// </summary>
        DrawClipboard = 0x308,

        /// <summary>
        /// Send Draw Item
        /// </summary>
        DrawItem = 0x2B,

        /// <summary>
        /// Send Drop Files
        /// </summary>
        DropFiles = 0x233,

        /// <summary>
        /// Send Enable
        /// </summary>
        Enable = 0xA,

        /// <summary>
        /// Send End session
        /// </summary>
        EndSession = 0x16,

        /// <summary>
        /// Send Enter Idle
        /// </summary>
        EnterIdle = 0x121,

        /// <summary>
        /// Send Enter Menu Loop
        /// </summary>
        EnterMenuLoop = 0x211,

        /// <summary>
        /// Send Enter Size Move
        /// </summary>
        EnterSizeMove = 0x231,

        /// <summary>
        /// Send Erase Background
        /// </summary>
        EraseBackground = 0x14,

        /// <summary>
        /// Send Exit Menu Loop
        /// </summary>
        ExitMenuLoop = 0x212,

        /// <summary>
        /// Send Exit Size Move
        /// </summary>
        ExitSizeMove = 0x232,

        /// <summary>
        /// Send Font Change
        /// </summary>
        FontChange = 0x1D,

        /// <summary>
        /// Send Get Dialog Code
        /// </summary>
        GetDialogCode = 0x87,

        /// <summary>
        /// Send Get Font
        /// </summary>
        GetFont = 0x31,

        /// <summary>
        /// Send Get Hot Key
        /// </summary>
        GetHotKey = 0x33,

        /// <summary>
        /// Send Get Icon
        /// </summary>
        GetIcon = 0x7F,

        /// <summary>
        /// Send Get Min Max Info
        /// </summary>
        GetMinMaxInfo = 0x24,

        /// <summary>
        /// Send Get Object
        /// </summary>
        GetObject = 0x3D,

        /// <summary>
        /// Send Get Sys Menu
        /// </summary>
        GetSysMenu = 0x313,

        /// <summary>
        /// Send Get Text
        /// </summary>
        GetText = 0xD,

        /// <summary>
        /// Send Get Text TextLength
        /// </summary>
        GetTextLength = 0xE,

        /// <summary>
        /// Send Handheld First
        /// </summary>
        HandheldFirst = 0x358,

        /// <summary>
        /// Send Send Handheld Last
        /// </summary>
        HandheldLast = 0x35F,

        /// <summary>
        /// Send Help message
        /// </summary>
        Help = 0x53,

        /// <summary>
        /// Send HotKey message
        /// </summary>
        HotKey = 0x312,

        /// <summary>
        /// Send Horizonal Scroll
        /// </summary>
        HorizonalScroll = 0x114,

        /// <summary>
        /// Send Horizonal Scroll Clipboard
        /// </summary>
        HorizonalScrollClipboard = 0x30E,

        /// <summary>
        /// Send Icon Erase Background
        /// </summary>
        IconEraseBackground = 0x27,

        /// <summary>
        /// Send Ime Char
        /// </summary>
        ImeChar = 0x286,

        /// <summary>
        /// Send Ime Composition
        /// </summary>
        ImeComposition = 0x10F,

        /// <summary>
        /// Send ImeCompositionFull
        /// </summary>
        ImeCompositionFull = 0x284,

        /// <summary>
        /// Send Ime Control
        /// </summary>
        ImeControl = 0x283,

        /// <summary>
        /// Send Ime End Composition
        /// </summary>
        ImeEndComposition = 0x10E,

        /// <summary>
        /// Send Ime Key Down
        /// </summary>
        ImeKeyDown = 0x290,

        /// <summary>
        /// Send ImeKeyLast
        /// </summary>
        ImeKeyLast = 0x10F,

        /// <summary>
        /// Send ImeKeyUp
        /// </summary>
        ImeKeyUp = 0x291,

        /// <summary>
        /// Send ImeNotify
        /// </summary>
        ImeNotify = 0x282,

        /// <summary>
        /// Send ImeRequest
        /// </summary>
        ImeRequest = 0x288,

        /// <summary>
        /// Send ImeSelect
        /// </summary>
        ImeSelect = 0x285,

        /// <summary>
        /// Send  Set Icon Text
        ///</summary>
        ImeSetIconText = 0x281,

        /// <summary>
        /// Send Ime Start Composition
        ///</summary>
        ImeStartComposition = 0x10D,

        /// <summary>
        /// Send InitDialog
        /// </summary>
        InitDialog = 0x110,

        /// <summary>
        /// Send InitMenu message
        /// </summary>
        InitMenu = 0x116,

        /// <summary>
        /// Send nitMenuPopup
        /// </summary>
        InitMenuPopup = 0x117,

        /// <summary>
        /// Send nputlangChange
        /// </summary>
        InputlangChange = 0x51,

        /// <summary>
        /// Send nputlangChangerequest
        /// </summary>
        InputlangChangerequest = 0x50,

        /// <summary>
        /// Send KeyDown
        /// </summary>
        KeyDown = 0x100,

        /// <summary>
        /// Send KeyFirst
        /// </summary>
        KeyFirst = 0x100,

        /// <summary>
        /// Send KeyLast
        /// </summary>
        KeyLast = 0x108,

        /// <summary>
        /// Send KeyUp
        /// </summary>
        KeyUp = 0x101,

        /// <summary>
        /// Send KillFocus
        /// </summary>
        KillFocus = 0x8,

        /// <summary>
        /// Send LeftButtonDoubleClic
        /// </summary>
        LeftButtonDoubleClic = 0x203,

        /// <summary>
        /// Send LeftButtonDown
        /// </summary>
        LeftButtonDown = 0x201,

        /// <summary>
        /// Send LeftButtonUp
        /// </summary>
        LeftButtonUp = 0x202,

        /// <summary>
        /// Send MiddleButtonDoubleClick
        /// </summary>
        MiddleButtonDoubleClick = 0x209,

        /// <summary>
        /// Send MiddleButtonDown
        /// </summary>
        MiddleButtonDown = 0x207,

        /// <summary>
        /// Send MiddleButtonUp
        /// </summary>
        MiddleButtonUp = 0x208,

        /// <summary>
        /// Send MdiActivate
        /// </summary>
        MdiActivate = 0x222,

        /// <summary>
        /// Send MdiCascade
        /// </summary>
        MdiCascade = 0x227,

        /// <summary>
        /// Send MdiCreate
        /// </summary>
        MdiCreate = 0x220,

        /// <summary>
        /// Send MdiDestroy
        /// </summary>
        MdiDestroy = 0x221,

        /// <summary>
        /// Send MdiGetActive
        /// </summary>
        MdiGetActive = 0x229,

        /// <summary>
        /// Send MdiIconArrange
        /// </summary>
        MdiIconArrange = 0x228,

        /// <summary>
        /// Send MdiMaximize
        /// </summary>
        MdiMaximize = 0x225,

        /// <summary>
        /// Send MdiNext
        /// </summary>
        MdiNext = 0x224,

        /// <summary>
        /// Send MdiRefreshMenu
        /// </summary>
        MdiRefreshMenu = 0x234,

        /// <summary>
        /// Send MdiRestore
        /// </summary>
        MdiRestore = 0x223,

        /// <summary>
        /// Send MdiSetMenu
        /// </summary>
        MdiSetMenu = 0x230,

        /// <summary>
        /// Send MdiTile
        /// </summary>
        MdiTile = 0x226,

        /// <summary>
        /// Send MeasureItem
        /// </summary>
        MeasureItem = 0x2C,

        /// <summary>
        /// Send MenuChar
        /// </summary>
        MenuChar = 0x120,

        /// <summary>
        /// Send MenuCommand
        /// </summary>
        MenuCommand = 0x126,

        /// <summary>
        /// Send MenuDrag
        /// </summary>
        MenuDrag = 0x123,

        /// <summary>
        /// Send MenugetObject
        /// </summary>
        MenugetObject = 0x124,

        /// <summary>
        /// Send MenuRightButtonUp
        /// </summary>
        MenuRightButtonUp = 0x122,

        /// <summary>
        /// Send MenuSelect
        /// </summary>
        MenuSelect = 0x11F,

        /// <summary>
        /// Send MouseActivate
        /// </summary>
        MouseActivate = 0x21,

        /// <summary>
        /// Send MouseFirst
        /// </summary>
        MouseFirst = 0x200,

        /// <summary>
        /// Send MouseHover
        /// </summary>
        MouseHover = 0x2A1,

        /// <summary>
        /// Send MouseLast
        /// </summary>
        MouseLast = 0x20A,

        /// <summary>
        /// Send MouseLeave
        /// </summary>
        MouseLeave = 0x2A3,

        /// <summary>
        /// Send MouseMove
        /// </summary>
        MouseMove = 0x200,

        /// <summary>
        /// Send MouseWheel
        /// </summary>
        MouseWheel = 0x20A,

        /// <summary>
        /// Send Move messabe
        /// </summary>
        Move = 0x3,

        /// <summary>
        /// Send Moving
        /// </summary>
        Moving = 0x216,

        /// <summary>
        /// Send NcActivate
        /// </summary>
        NcActivate = 0x86,

        /// <summary>
        /// Send NcCalcSize
        /// </summary>
        NcCalcSize = 0x83,

        /// <summary>
        /// Send NcCreate
        /// </summary>
        NcCreate = 0x81,

        /// <summary>
        /// Send NcDestroy
        /// </summary>
        NcDestroy = 0x82,

        /// <summary>
        /// Send NcHitTest
        /// </summary>
        NcHitTest = 0x84,

        /// <summary>
        /// Send NcLeftButtonDoubleClick
        /// </summary>
        NcLeftButtonDoubleClick = 0xA3,

        /// <summary>
        /// Send NcLeftButtonDown
        /// </summary>
        NcLeftButtonDown = 0xA1,

        /// <summary>
        /// Send NcLeftButtonUp
        /// </summary>
        NcLeftButtonUp = 0xA2,

        /// <summary>
        /// Send NcMiddleButtonDoubleClick
        /// </summary>
        NcMiddleButtonDoubleClick = 0xA9,

        /// <summary>
        /// Send NcMiddleButtonDown
        /// </summary>
        NcMiddleButtonDown = 0xA7,

        /// <summary>
        /// Send NcMiddleButtonUp
        /// </summary>
        NcMiddleButtonUp = 0xA8,

        /// <summary>
        /// Send NcMouseHover
        /// </summary>
        NcMouseHover = 0x2A0,

        /// <summary>
        /// Send NcMouseLeave
        /// </summary>
        NcMouseLeave = 0x2A2,

        /// <summary>
        /// Send NcMouseMove
        /// </summary>
        NcMouseMove = 0xA0,

        /// <summary>
        /// Send NcPaint
        /// </summary>
        NcPaint = 0x85,

        /// <summary>
        /// Send NcRightButtonDoubleClick
        /// </summary>
        NcRightButtonDoubleClick = 0xA6,

        /// <summary>
        /// Send NcRightButtonDown
        /// </summary>
        NcRightButtonDown = 0xA4,

        /// <summary>
        /// Send NcRightButtonUp
        /// </summary>
        NcRightButtonUp = 0xA5,

        /// <summary>
        /// Send NextDialogControl
        /// </summary>
        NextDialogControl = 0x28,

        /// <summary>
        /// Send NextMenu
        /// </summary>
        NextMenu = 0x213,

        /// <summary>
        /// Send Notify
        /// </summary>
        Notify = 0x4E,

        /// <summary>
        /// Send NotifyFormat
        /// </summary>
        NotifyFormat = 0x55,

        /// <summary>
        /// Send Null message
        /// </summary>
        Null = 0x0,

        /// <summary>
        /// Send Paint
        /// </summary>
        Paint = 0xF,

        /// <summary>
        /// Send PaintClipboard
        /// </summary>
        PaintClipboard = 0x309,

        /// <summary>
        /// Send PaintIcon
        /// </summary>
        PaintIcon = 0x26,

        /// <summary>
        /// Send PaletteChanged
        /// </summary>
        PaletteChanged = 0x311,

        /// <summary>
        /// Send PaletteIsChanging
        /// </summary>
        PaletteIsChanging = 0x310,

        /// <summary>
        /// Send ParentNotify
        /// </summary>
        ParentNotify = 0x210,

        /// <summary>
        /// Send Paste
        /// </summary>
        Paste = 0x302,

        /// <summary>
        /// Send PenWinFirst
        /// </summary>
        PenWinFirst = 0x380,

        /// <summary>
        /// Send PenWinLast
        /// </summary>
        PenWinLast = 0x38F,

        /// <summary>
        /// Send Power
        /// </summary>
        Power = 0x48,

        /// <summary>
        /// Send Print
        /// </summary>
        Print = 0x317,

        /// <summary>
        /// Send PrintClient
        /// </summary>
        PrintClient = 0x318,

        /// <summary>
        /// Send QueryDragIcon
        /// </summary>
        QueryDragIcon = 0x37,

        /// <summary>
        /// Send QueryEndSession
        /// </summary>
        QueryEndSession = 0x11,

        /// <summary>
        /// Send QueryNewPalette
        /// </summary>
        QueryNewPalette = 0x30F,

        /// <summary>
        /// Send QueryOpen
        /// </summary>
        QueryOpen = 0x13,

        /// <summary>
        /// Send QueryuiState
        /// </summary>
        QueryuiState = 0x129,

        /// <summary>
        /// Send QueueSync
        /// </summary>
        QueueSync = 0x23,

        /// <summary>
        /// Send Quit message
        /// </summary>
        Quit = 0x12,

        /// <summary>
        /// Send Right Button Double Click
        /// </summary>
        RightButtonDoubleClick = 0x206,

        /// <summary>
        /// Send Righ tButton Down
        /// </summary>
        RightButtonDown = 0x204,

        /// <summary>
        /// Send Right Button Up
        /// </summary>
        RightButtonUp = 0x205,

        /// <summary>
        /// Send Renderall Formats
        /// </summary>
        RenderallFormats = 0x306,

        /// <summary>
        /// Send RenderFormat
        /// </summary>
        RenderFormat = 0x305,

        /// <summary>
        /// Send Set Cursor message
        /// </summary>
        SetCursor = 0x20,

        /// <summary>
        /// Send Set Focus message
        /// </summary>
        SetFocus = 0x7,

        /// <summary>
        /// Send Set Font message
        /// </summary>
        SetFont = 0x30,

        /// <summary>
        /// Send Set Hot Key
        /// </summary>
        SetHotKey = 0x32,

        /// <summary>
        /// Send Set Icon
        /// </summary>
        SetIcon = 0x80,

        /// <summary>
        /// Send Set Redraw
        /// </summary>
        SetRedraw = 0xB,

        /// <summary>
        /// Send SetText
        /// </summary>
        SetText = 0xC,

        /// <summary>
        /// Send SettingChange
        /// </summary>
        SettingChange = 0x1A,

        /// <summary>
        /// Send Show Window message
        /// </summary>
        ShowWindow = 0x18,

        /// <summary>
        /// Send Size message
        /// </summary>
        Size = 0x5,

        /// <summary>
        /// Send SizeClipboard
        /// </summary>
        SizeClipboard = 0x30B,

        /// <summary>
        /// Send Sizing
        /// </summary>
        Sizing = 0x214,

        /// <summary>
        /// Send Spooler Status
        /// </summary>
        SpoolerStatus = 0x2A,

        /// <summary>
        /// Send Style Changed
        /// </summary>
        StyleChanged = 0x7D,

        /// <summary>
        /// Send Style Changing
        /// </summary>
        StyleChanging = 0x7C,

        /// <summary>
        /// Send Sync Paint
        /// </summary>
        SyncPaint = 0x88,

        /// <summary>
        /// Send SysChar message
        /// </summary>
        SysChar = 0x106,

        /// <summary>
        /// Send SysColorChange
        /// </summary>
        SysColorChange = 0x15,

        /// <summary>
        /// Send SysCommand
        /// </summary>
        SysCommand = 0x112,

        /// <summary>
        /// Send SysDeadChar
        /// </summary>
        SysDeadChar = 0x107,

        /// <summary>
        /// Send SyskeyDown
        /// </summary>
        SyskeyDown = 0x104,

        /// <summary>
        /// Send SysKeyUp
        /// </summary>
        SysKeyUp = 0x105,

        /// <summary>
        /// Send Sys Timer
        /// </summary>
        SysTimer = 0x118,

        /// <summary>
        /// TCard Udocumented http://sUpport.microsoft.com/?id=108938
        /// </summary>
        TCard = 0x52,

        /// <summary>
        /// Send Time Change
        /// </summary>
        TimeChange = 0x1E,

        /// <summary>
        /// Send Timer
        /// </summary>
        Timer = 0x113,

        /// <summary>
        /// Send Undo message
        /// </summary>
        Undo = 0x304,

        /// <summary>
        /// Send Uninit Menu Popup
        /// </summary>
        UninitMenuPopup = 0x125,

        /// <summary>
        /// Send User message
        /// </summary>
        User = 0x400,

        /// <summary>
        /// Send UserChanged
        /// </summary>
        UserChanged = 0x54,

        /// <summary>
        /// Send VerticalKey to Item message
        /// </summary>
        VerticalKeyToItem = 0x2E,

        /// <summary>
        /// Send VerticalScroll
        /// </summary>
        VerticalScroll = 0x115,

        /// <summary>
        /// Send VerticalScrollClipboard
        /// </summary>
        VerticalScrollClipboard = 0x30A,

        /// <summary>
        /// Send WindowposChanged
        /// </summary>
        WindowposChanged = 0x47,

        /// <summary>
        /// Send WindowPositionChanging
        /// </summary>
        WindowPositionChanging = 0x46,

        /// <summary>
        /// Send WinIniChange
        /// </summary>
        WinIniChange = 0x1A,

        /// <summary>
        /// Send XButtonDoubleClick
        /// </summary>
        XButtonDoubleClick = 0x20D,

        /// <summary>
        /// Send XButtonDown
        /// </summary>
        XButtonDown = 0x20B,

        /// <summary>
        /// X Button Up
        /// </summary>
        XButtonUp = 0x20C
    }

    /// <summary>
    /// File access enum
    /// </summary>
    [Flags]
    internal enum EFileAccess : uint
    {
        GenericRead = 0x80000000,
        GenericWrite = 0x40000000,
        GenericExecute = 0x20000000,
        GenericAll = 0x10000000,
    }

    /// <summary>
    /// File share enum
    /// </summary>
    [Flags]
    internal enum EFileShare : uint
    {
        None = 0x00000000,
        Read = 0x00000001,
        Write = 0x00000002,
        Delete = 0x00000004,
    }

    /// <summary>
    /// Creation disposition enum
    /// </summary>
    internal enum ECreationDisposition : uint
    {
        New = 1,
        CreateAlways = 2,
        OpenExisting = 3,
        OpenAlways = 4,
        TruncateExisting = 5,
    }

    /// <summary>
    /// File attribute enums
    /// </summary>
    [Flags]
    internal enum EFileAttributes : uint
    {
        Readonly = 0x00000001,
        Hidden = 0x00000002,
        System = 0x00000004,
        Directory = 0x00000010,
        Archive = 0x00000020,
        Device = 0x00000040,
        Normal = 0x00000080,
        Temporary = 0x00000100,
        SparseFile = 0x00000200,
        ReparsePoint = 0x00000400,
        Compressed = 0x00000800,
        Offline = 0x00001000,
        NotContentIndexed = 0x00002000,
        Encrypted = 0x00004000,
        WriteThrough = 0x80000000,
        Overlapped = 0x40000000,
        NoBuffering = 0x20000000,
        RandomAccess = 0x10000000,
        SequentialScan = 0x08000000,
        DeleteOnClose = 0x04000000,
        BackupSemantics = 0x02000000,
        PosixSemantics = 0x01000000,
        OpenReparsePoint = 0x00200000,
        OpenNoRecall = 0x00100000,
        FirstPipeInstance = 0x00080000
    }

    /// <summary>
    /// The page protextion
    /// </summary>
    internal enum Win32Const : int
    {
        /// <summary>
        /// Define the invalid handle for backing by system.
        /// </summary>
        InvalidHandleValue = -1,

        /// <summary>
        /// Say we are going to wrare to memory                                                                   
        /// </summary>
        FileMapWrite = 0x0002,

        /// <summary>
        /// Copy to memory
        /// </summary>
        FileMapCopy = 0x0001,

        /// <summary>
        /// Read from memory
        /// </summary>
        FileMapRead = 0x0004,

        /// <summary>
        /// Read from memory
        /// </summary>
        FileMapReadWrite = 0x0006,

        /// <summary>
        /// All access
        /// </summary>
        FileMapAllAccess = 0x001f,

        /// <summary>
        /// Define from Win32 API
        /// </summary>
        ErrorAlreadyExists = 183
    }

    /// <summary>
    /// The page protextion
    /// </summary>
    [Flags]
    internal enum PageProtection : int
    {
        /// <summary>
        /// No access definition
        /// </summary>
        NoAccess = 0x01,

        /// <summary>
        /// Read Only flag
        /// </summary>
        Readonly = 0x02,

        /// <summary>
        /// Read/Write flag
        /// </summary>
        ReadWrite = 0x04,

        /// <summary>
        /// Copy write flag
        /// </summary>
        WriteCopy = 0x08,

        /// <summary>
        /// Memory can be Executed
        /// </summary>
        Execute = 0x10,

        /// <summary>
        /// Execute read
        /// </summary>
        ExecuteRead = 0x20,

        /// <summary>
        /// Allow read, write and execute
        /// </summary>
        ExecuteReadWrite = 0x40,

        /// <summary>
        /// Allow Execute, write and copy
        /// </summary>
        ExecuteWriteCopy = 0x80,

        /// <summary>
        /// Guard the memory
        /// </summary>
        Guard = 0x100,

        /// <summary>
        /// Do not use caching
        /// </summary>
        NoCache = 0x200,

        /// <summary>
        /// Write combine
        /// </summary>
        WriteCombine = 0x400,
    }

    #endregion

    /// <summary>
    /// Usual static class that marshals to System
    /// </summary>
    internal static class NativeMethods
    {
        /// <summary>
        /// Defines the PInvoke functions we use
        /// to access the FileMapping Win32 APIs
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal class OFSTRUCT
        {
            internal const int OFS_MAXPATHNAME = 128;
            internal byte cBytes;
            internal byte fFixedDisc;
            internal UInt16 nErrCode;
            internal UInt16 Reserved1;
            internal UInt16 Reserved2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = OFS_MAXPATHNAME)]
            internal string szPathName;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct SystemInfo
        {
            internal ProcessorInfoUnion uProcessorInfo;
            internal uint dwPageSize;
            internal IntPtr lpMinimumApplicationAddress;
            internal IntPtr lpMaximumApplicationAddress;
            internal IntPtr dwActiveProcessorMask;
            internal uint dwNumberOfProcessors;
            internal uint dwProcessorType;
            internal uint dwAllocationGranularity;
            internal ushort dwProcessorLevel;
            internal ushort dwProcessorRevision;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct ProcessorInfoUnion
        {
            [FieldOffset(0)]
            internal uint dwOemId;
            [FieldOffset(0)]
            internal ushort wProcessorArchitecture;
            [FieldOffset(2)]
            internal ushort wReserved;
        }

        /// <summary>
        /// Sends a message
        /// </summary>
        /// <param name="hWnd">handle to the window</param>
        /// <param name="wMsg">message to send</param>
        /// <param name="wParam">first parameter</param>
        /// <param name="lParam">second parameter</param>
        /// <returns>0 if success</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern int SendMessage(IntPtr hWnd, WindowsMessages wMsg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Gets the window that has focus
        /// </summary>
        /// <returns>a window's handle</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("user32.dll")]
        internal static extern IntPtr GetFocus();

        /// <summary>
        /// Gets the size of the file.
        /// </summary>
        /// <param name="fileHandle">The file handle.</param>
        /// <param name="fileSizeHigh">return the upper 32 bits of the file size.</param>
        /// <returns>The lower 32 bits of the file size, or error</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern int GetFileSize(IntPtr fileHandle, out int fileSizeHigh);

        /// <summary>
        /// Creates the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="desiredAccess">The desired access.</param>
        /// <param name="shareMode">The share mode.</param>
        /// <param name="securityAttributes">The security attributes.</param>
        /// <param name="creationDisposition">The creation disposition.</param>
        /// <param name="flagsAndAttributes">The flags and attributes.</param>
        /// <param name="templateFile">The template file.</param>
        /// <returns>A file handle</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr CreateFile(
            String fileName, int desiredAccess, int shareMode,
            IntPtr securityAttributes, int creationDisposition,
            int flagsAndAttributes, IntPtr templateFile);

        /// <summary>
        /// Opens the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="reOpenBuff">The re open buff.</param>
        /// <param name="style">The style.</param>
        /// <returns>A file handle</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
        internal static extern IntPtr OpenFile(String fileName,
            [Out, MarshalAs(UnmanagedType.LPStruct)]
                OFSTRUCT reOpenBuff,
            int style);

        /// <summary>
        /// Creates the file mapping.
        /// </summary>
        /// <param name="fileHandle">The file handle.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="protect">The protect.</param>
        /// <param name="maximumSizeLow">The maximum size low.</param>
        /// <param name="maximumSizeHigh">The maximum size high.</param>
        /// <param name="name">The name.</param>
        /// <returns>A file mapping handle</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr CreateFileMapping(
            IntPtr fileHandle, IntPtr attributes, int protect,
            int maximumSizeLow, int maximumSizeHigh,
            string name);

        /// <summary>
        /// Flushes the view of file.
        /// </summary>
        /// <param name="baseAddress">The base address.</param>
        /// <param name="numberOfBytes">The number of bytes to flush</param>
        /// <returns>True if it succeds, false otherwise</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool FlushViewOfFile(IntPtr baseAddress, int numberOfBytes);

        /// <summary>
        /// Maps the view of file.
        /// </summary>
        /// <param name="fileMappingHandle">The file mapping handle.</param>
        /// <param name="desiredAccess">The desired access.</param>
        /// <param name="fileOffsetHigh">The file offset high.</param>
        /// <param name="fileOffsetLow">The file offset low.</param>
        /// <param name="numberOfBytesToMap">The number of bytes to map.</param>
        /// <returns>The mapping object</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true)]
        internal static extern IntPtr MapViewOfFile(
            IntPtr fileMappingHandle, int desiredAccess, int fileOffsetHigh,
            int fileOffsetLow, int numberOfBytesToMap);

        /// <summary>
        /// Opens the file mapping.
        /// </summary>
        /// <param name="desiredAccess">The desired access.</param>
        /// <param name="inheritHandle">if set to <c>true</c> [inherit handle].</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr OpenFileMapping(
            int desiredAccess, bool inheritHandle, String name);

        /// <summary>
        /// Unmaps the view of file.
        /// </summary>
        /// <param name="baseAddress">The base address.</param>
        /// <returns>True if the file has been unmapped</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool UnmapViewOfFile(IntPtr baseAddress);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool CloseHandle(IntPtr handle);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr GlobalAddAtom(string atomName);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr GlobalDeleteAtom(string atomName);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern void GetSystemInfo([MarshalAs(UnmanagedType.Struct)] ref SystemInfo systemInfo);

        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr GlobalFindAtom(String atomName);

        /// <summary>
        /// Gets the DC.
        /// </summary>
        /// <param name="hWnd">The h WND.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        internal static extern IntPtr GetDC(IntPtr hWnd);

        /// <summary>
        /// Releases the DC.
        /// </summary>
        /// <param name="hWnd">The h WND.</param>
        /// <param name="hDC">The h DC.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        internal static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);

        /// <summary>
        /// Gets the device caps.
        /// </summary>
        /// <param name="hdc">The HDC.</param>
        /// <param name="devCap">The dev cap.</param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        internal static extern int GetDeviceCaps(IntPtr hdc, int devCap); 

        // class Win32MapApis
        /// <summary>
        /// This creates a rounded rectangle, see GDI doc onf CreateRoundRectRgn for examples
        /// </summary>
        /// <param name="nLeftRect"></param>
        /// <param name="nTopRect"></param>
        /// <param name="nRightRect"></param>
        /// <param name="nBottomRect"></param>
        /// <param name="nWidthEllipse"></param>
        /// <param name="nHeightEllipse"></param>
        /// <returns></returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        internal static extern IntPtr CreateRoundRectangleRegion
        (
           int nLeftRect, // x-coordinate of upper-left corner
           int nTopRect, // y-coordinate of upper-left corner
           int nRightRect, // x-coordinate of lower-right corner
           int nBottomRect, // y-coordinate of lower-right corner
           int nWidthEllipse, // height of ellipse
           int nHeightEllipse // width of ellipse
        );

        /// <summary>
        /// This just returns the executable name of the browser
        /// This will create a new brower and avoid hijacking an existing browser as opening and html file will
        /// </summary>
        /// <returns></returns>
        internal static string GetDefaultBrowser()
        {
            string browser = string.Empty;
            RegistryKey key = null;
            try
            {
                key = Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                //trim off quotes
                browser = key.GetValue(null).ToString().ToLower().Replace("\"", "");
                if (!browser.EndsWith("exe"))
                {
                    //get rid of everything after the ".exe"
                    browser = browser.Substring(0, browser.LastIndexOf(".exe") + 4);
                }
            }
            finally
            {
                if (key != null)
                {
                    key.Close();
                }
            }
            return browser;
        }

        /// <summary>
        /// Gets the memory page alignment value
        /// </summary>
        /// <returns>The page allignmen</returns>
        internal static int GetMemoryPageAlignment()
        {
            var systemInfo = new SystemInfo();
            GetSystemInfo(ref systemInfo);
            return (int)systemInfo.dwAllocationGranularity;
        }

        /// <summary>
        /// gets key state for caplock, numlock, scrolllock
        /// </summary>
        /// <param name="keyCode">The key code.</param>
        /// <returns>
        /// 0 if success
        /// </returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern short GetKeyState(int keyCode);

        /// <summary>
        /// Gets the disk free space.
        /// </summary>
        /// <param name="lpDirectoryName">Name of the lp directory.</param>
        /// <param name="lpFreeBytesAvailable">The lp free bytes available.</param>
        /// <param name="lpTotalNumberOfBytes">The lp total number of bytes.</param>
        /// <param name="lpTotalNumberOfFreeBytes">The lp total number of free bytes.</param>
        /// <returns>true if successful</returns>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetDiskFreeSpaceEx(string lpDirectoryName,
        out ulong lpFreeBytesAvailable,
        out ulong lpTotalNumberOfBytes,
        out ulong lpTotalNumberOfFreeBytes);

        /// <summary>
        /// Devices IO control.
        /// </summary>
        /// <param name="hDevice">The h device.</param>
        /// <param name="dwIoControlCode">The dw io control code.</param>
        /// <param name="inBuffer">The in buffer.</param>
        /// <param name="nInBufferSize">Size of the n in buffer.</param>
        /// <param name="outBuffer">The out buffer.</param>
        /// <param name="nOutBufferSize">Size of the n out buffer.</param>
        /// <param name="pBytesReturned">The p bytes returned.</param>
        /// <param name="lpOverlapped">The lp overlapped.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern bool DeviceIoControl(
            IntPtr hDevice,
            uint dwIoControlCode,
            IntPtr inBuffer,
            int nInBufferSize,
            IntPtr outBuffer,
            int nOutBufferSize,
            out int pBytesReturned,
            IntPtr lpOverlapped);

        /// <summary>
        /// Creates the file.
        /// </summary>
        /// <param name="lpFileName">Name of the lp file.</param>
        /// <param name="dwDesiredAccess">The dw desired access.</param>
        /// <param name="dwShareMode">The dw share mode.</param>
        /// <param name="lpSecurityAttributes">The lp security attributes.</param>
        /// <param name="dwCreationDisposition">The dw creation disposition.</param>
        /// <param name="dwFlagsAndAttributes">The dw flags and attributes.</param>
        /// <param name="hTemplateFile">The h template file.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern IntPtr CreateFile(
            string lpFileName,
            EFileAccess dwDesiredAccess,
            EFileShare dwShareMode,
            IntPtr lpSecurityAttributes,
            ECreationDisposition dwCreationDisposition,
            EFileAttributes dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        /// <summary>
        /// Copies bytes to the specified destination
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="source">The source.</param>
        /// <param name="count">The byte count.</param>
        /// <returns></returns>
        [DllImport("msvcrt.dll", SetLastError = false, EntryPoint = "memcpy")]
        internal static extern IntPtr Copy(
            IntPtr destination, 
            IntPtr source, 
            int count);
    }
}
