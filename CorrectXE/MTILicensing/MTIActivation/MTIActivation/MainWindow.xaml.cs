﻿
namespace MTIActivation
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Threading;

    using KeyEventArgs = System.Windows.Input.KeyEventArgs;
    using MessageBox = System.Windows.MessageBox;
    using MouseEventArgs = System.Windows.Input.MouseEventArgs;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window
    {
        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The MtiLicenseValidate instance.
        /// </summary>
        private MtiLicenseValidate mtiLicenseValidate;

        /// <summary>
        /// First run flag - used to affect loading of ActivationKeyTextBox.
        /// </summary>
        private bool firstRun = true;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            var args = Environment.GetCommandLineArgs();
            if (args.Length <= 1)
            {
                MessageBox.Show("Product (e.g. DRSNova-2.0.0) required on command line");
                this.Close();
                return;
            }

            try
            {
                this.LicenseProductInfo = new MtiLicenseProductInfo(args[1]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Product string incorrect format");
                this.Close();
                return;
            }

            // Validate the license
            try
            {
                this.ValidateLicense();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "License validation failed");
                this.Close();
                return;
            }

            this.IsClosed = false;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Public Properties

        /// <summary>
        /// Gets or sets the license product info.
        /// </summary>
        /// <value>
        /// The license product info.
        /// </value>
        internal MtiLicenseProductInfo LicenseProductInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is closed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is closed; otherwise, <c>false</c>.
        /// </value>
        internal bool IsClosed { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////messagebox.whow
        #region Private methods

        /// <summary>
        /// Activates the button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ActivateButtonClick(object sender, RoutedEventArgs e)
        {
            // Check if connected to Internet
            if (!Network.IsConnectedToInternet())
            {
                var message = string.Format(
                    "{0}\n{1}\n\n{2}",
                    "This computer does not appear to be connected to the Internet.",
                    "Activation will fail if you are not connected to the Internet",
                    "Are you sure you want to try to Activate?");
                var result = MessageBox.Show(message, "Internet Connection Problem", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }

            // Invalidate the license and clear some MtiLicense globals.
            if (this.mtiLicenseValidate != null)
            {
                this.mtiLicenseValidate.Invalidate();
            }

            MtiLicense.Clear();

            var activationKey = this.ActivationKeyTextBox.Text;

            MtiLicenseActivate mtiLicenseActivate;

            using (new BusyCursor(this))
            {
                try
                {
                    this.ClearGuiElementsBeforeCheck(true);
                    var statMessage = this.LicenseProductInfo.ToString() + Environment.NewLine +
                    "Activation Key: " +  activationKey + Environment.NewLine +
                    "Machine Id: " + MtiLicense.GetMachineId();
                    //// MessageBox.Show(statMessage);
                    mtiLicenseActivate = new MtiLicenseActivate(this.LicenseProductInfo, activationKey, MtiLicense.GetMachineId());
                }
                catch (Exception ex)
                {
                    if (ex.Message == "No license key was found on your system. To use the QLM Professional API, you need to activate a QLM Professional license on this system.")
                    {
                        MessageBox.Show("Dearest Developer,\n\nAs you may recall, in order to activate a license, you must run this program from outside of Visual Studio.\nWe deeply regret the inconvenience this may have caused you, but, honestly, we do not care.\n\nSincerely,\nDewey, Cheatham and Howe\nSolicitors");
                    }
                    else
                    {
                        MessageBox.Show("Exception during activation: " + Environment.NewLine + "    " + ex.Message + Environment.NewLine + "If you aren't connected to the Internet, you need to use 'Enter Key Manually'.");
                    }

                    this.UpdateGui();

                    return;
                }
            }

            if (mtiLicenseActivate.Succeeded)
            {
                var isValid = this.ValidateLicense();

                if (!isValid)
                {
                    MessageBox.Show("Activation succeeded, but Validation failed: " + this.mtiLicenseValidate.ErrorMessage);
                }
            }
            else
            {
                MessageBox.Show(mtiLicenseActivate.ErrorMessage);
            }

            this.UpdateGui();
        }

        /// <summary>
        /// Handles the "Enter Key" button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void EnterKeyButtonClick(object sender, RoutedEventArgs e)
        {
            // Enable the licenseKeyTextBox for editing and enable the Ok button.
            this.LicenseKeyTextBox.IsReadOnly = false;
            this.LicenseKeyTextBox.Focus();
            this.LicenseKeyTextBox.Text = "Enter Key Here";
            this.LicenseKeyTextBox.SelectAll();
            this.LicenseKeyOkButton.Visibility = Visibility.Visible;
            this.LicenseKeyOkButton.IsEnabled = true;
            this.EnterKeyButton.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Handles the License Key "Ok" button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void LicenseKeyOkButtonClick(object sender, RoutedEventArgs e)
        {
            // Invalidate the license and clear some MtiLicense globals.
            this.mtiLicenseValidate.Invalidate();
            MtiLicense.Clear();

            var activationKey = this.ActivationKeyTextBox.Text;
            var licenseKey = this.LicenseKeyTextBox.Text;

            MtiLicenseActivate mtiLicenseActivate;

            // Disable editing of the licenseKeyTextBox and hide the Ok button
            this.LicenseKeyTextBox.IsReadOnly = true;
            this.LicenseKeyOkButton.Visibility = Visibility.Hidden;
            this.LicenseKeyOkButton.IsEnabled = false;
            this.EnterKeyButton.Visibility = Visibility.Visible;

            using (new BusyCursor(this))
            {
                try
                {
                    this.ClearGuiElementsBeforeCheck(false);
                    var statMessage = this.LicenseProductInfo.ToString() + Environment.NewLine +
                    "Activation Key: " + activationKey + Environment.NewLine +
                    "License Key: " + licenseKey + Environment.NewLine +
                    "Machine Id: " + MtiLicense.GetMachineId();
                   //// MessageBox.Show(statMessage);
                    mtiLicenseActivate = new MtiLicenseActivate(this.LicenseProductInfo, activationKey, MtiLicense.GetMachineId(), true, licenseKey);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception during activation: " + Environment.NewLine + "    " + ex.Message);

                    this.UpdateGui();

                    return;
                }
            }

            if (mtiLicenseActivate.Succeeded)
            {
                var isValid = this.ValidateLicense();

                if (!isValid)
                {
                    MessageBox.Show("Activation succeeded, but Validation failed: " + this.mtiLicenseValidate.ErrorMessage);
                }
            }
            else
            {
                MessageBox.Show(mtiLicenseActivate.ErrorMessage);
            }

            this.UpdateGui();
        }

        /// <summary>
        /// Updates the GUI.
        /// </summary>
        private void UpdateGui()
        {
            this.ComputerIdTextBox.Text = MtiLicense.GetMachineId();
            this.HostnameTextBox.Text = MtiLicense.Hostname;

            ////this.DemoCheckBox.IsChecked = this.LicenseProductInfo.Name == "DRSNova::Demo";

            this.VersionLabel.Content = string.Format("v{0}", MtiLicense.GetCombinedFullVersion(this.LicenseProductInfo));

            this.CompanyProductTextBox.Text = MtiLicense.Company;

            this.UserProductTextBox.Text = MtiLicense.User;

            this.ComputerIdTextBlock.Content = "COMPUTER ID for " + MtiLicense.MacAddress;

            if (this.mtiLicenseValidate != null && this.mtiLicenseValidate.IsValid)
            {
                this.ExpirationTextBox.Text = this.mtiLicenseValidate.Expires ? this.mtiLicenseValidate.ExpirationDate.ToString(CultureInfo.CurrentCulture) : "Never";

                string maintenanceEndDate;

                if (MtiLicense.MaintenanceEndDate == null)
                {
                    maintenanceEndDate = "Unknown";
                }
                else if (MtiLicense.MaintenanceEndDate == DateTime.MinValue)
                {
                    maintenanceEndDate = "This version only";
                }
                else
                {
                    maintenanceEndDate = ((DateTime)MtiLicense.MaintenanceEndDate).ToString(CultureInfo.CurrentCulture);
                }

                this.MaintenanceExpirationTextBox.Text = maintenanceEndDate;
            }
            else
            {
                this.ExpirationTextBox.Text = string.Empty;

                this.MaintenanceExpirationTextBox.Text = string.Empty;
            }

            this.LicenseKeyTextBox.Text = this.mtiLicenseValidate != null ? this.mtiLicenseValidate.LicenseKey : string.Empty;

            this.EditionTextBox.Text = this.mtiLicenseValidate != null && this.mtiLicenseValidate.IsValid ? this.mtiLicenseValidate.DisplayProductName : string.Empty;

            this.FeaturesListBox.Items.Clear();

            if (this.mtiLicenseValidate != null && this.mtiLicenseValidate.Features != null)
            {
                foreach (var feature in this.mtiLicenseValidate.Features)
                {
                    this.FeaturesListBox.Items.Add(feature);
                }
            }

            // If firstRun and valid license, load ActivationKey, else if firstRun and not valid license, the load ActivationKey from previous version.
            if (this.firstRun)
            {
                this.firstRun = false;

                if (this.mtiLicenseValidate != null && this.mtiLicenseValidate.IsValid)
                {
                    this.ActivationKeyTextBox.Text = MtiLicense.ActivationKey;
                }
                else
                {
                    string oldActivationKey;
                    string oldVersion;
                    var hasSignificantLicenseChange = MtiLicense.GetMostRecentActivationKey(this.LicenseProductInfo, out oldActivationKey, out oldVersion);

                    if (hasSignificantLicenseChange)
                    {
                        this.ActivationKeyTextBox.Text = string.Empty;

                        MessageBox.Show(
                            "Due to the update to this version, you need a new license key.  Please dismiss this dialog box now "
                            + "and enter your new key.  Then activate it with either the 'Activate Online' or 'Enter License Key Manually' buttons.",
                            "*** Application Version Updated - New Activation Key Required ***");
                    }
                    else if (!string.IsNullOrEmpty(oldActivationKey))
                    {
                        this.ActivationKeyTextBox.Text = oldActivationKey;

                        MessageBox.Show(
                            "Due to the update, you must reactivate your license.  The prior version's Activation Key has been "
                            + "pre-loaded for you.  You MUST now re-activate with either the 'Activate Online' or 'Enter License Key Manually' buttons.",
                            "*** Application Version Updated - Activation Required ***");
                    }
                }
            }

            // Allow GUI to update
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { }));
        }

        /// <summary>
        /// Clear GUI elements before doing activation check.
        /// </summary>
        /// <param name="clearLicenseKey">if set to <c>true</c> [clear license key].</param>
        private void ClearGuiElementsBeforeCheck(bool clearLicenseKey)
        {
            this.ComputerIdTextBox.Text = MtiLicense.GetMachineId();
            this.HostnameTextBox.Text = MtiLicense.Hostname;

            this.CompanyProductTextBox.Text = string.Empty;

            this.UserProductTextBox.Text = string.Empty;

            this.ExpirationTextBox.Text = string.Empty;

            this.MaintenanceExpirationTextBox.Text = string.Empty;

            this.EditionTextBox.Text = string.Empty;

            if (clearLicenseKey)
            {
                this.LicenseKeyTextBox.Text = string.Empty;
            }

            this.FeaturesListBox.Items.Clear();

            this.UpdateGui();
        }

        /// <summary>
        /// Handles the KeyDown event - so we can show info when CTRL-I is pressed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void LicensingMainWindowKeyDown(object sender, KeyEventArgs e)
        {
            // Show some info for CTRL-I
            if (e.Key == Key.I && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                MessageBox.Show(string.Format("Hostname: {0}\nMAC: {1}\nHDD: {2}", MtiLicense.Hostname, MtiLicense.MacAddress, MtiLicense.HddSerialNumber));
            }
        }

        /// <summary>
        /// Validates the license.
        /// </summary>
        /// <returns>True or false depending on whether the license was validated.</returns>
        private bool ValidateLicense()
        {
            if (string.IsNullOrEmpty(this.LicenseProductInfo.Name))
            {
                return false;
            }

            this.mtiLicenseValidate = new MtiLicenseValidate(this.LicenseProductInfo);

            return this.mtiLicenseValidate.IsValid;
        }

        /// <summary>
        /// Mtis the URL text block mouse up.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MtiUrlTextBlockMouseUp(object sender, MouseButtonEventArgs e)
        {
            var text = "http://" + ((TextBlock)sender).Text;

            // Create a hyperlink
            var link = new Hyperlink(new Run(text))
            {
                NavigateUri = new Uri(text)
            };

            // Start default web browswer
            Process.Start(link.NavigateUri.ToString());
        }

        /// <summary>
        /// Mtis the URL text block mouse enter.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void MtiUrlTextBlockMouseEnter(object sender, MouseEventArgs e)
        {
            ((TextBlock)sender).FontWeight = FontWeights.Bold;
        }

        /// <summary>
        /// Mtis the URL text block mouse leave.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void MtiUrlTextBlockMouseLeave(object sender, MouseEventArgs e)
        {
            ((TextBlock)sender).FontWeight = FontWeights.Normal;
        }

        /// <summary>
        /// Handles the Closed event of the LicensingMainWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void LicensingMainWindowClosed(object sender, EventArgs e)
        {
            this.IsClosed = true;
        }

        /// <summary>
        /// Handles the Click event of the closeButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Event that fires when Window has been rendered.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void LicensingMainWindowContentRendered(object sender, EventArgs e)
        {
            this.UpdateGui();
        }

        #endregion

        /// <summary>
        /// Handles the product CheckBox click event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleProductCheckBoxClickEvent(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;

            if (checkBox == null)
            {
                return;
            }

            if (checkBox.IsChecked == null)
            {
                return;
            }

            var isChecked = (bool)checkBox.IsChecked;

            this.LicenseProductInfo.Name = isChecked ? "DRSNova::Demo" : "DRSNova";

            MtiLicense.SaveActiveCortexLicenseProduct(this.LicenseProductInfo.Name);

            // Invalidate the license and clear some MtiLicense globals.
            this.mtiLicenseValidate.Invalidate();
            MtiLicense.Clear();

            this.UpdateGui();
        }
    }
}

