﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/30/2012 3:23:47 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------
namespace MTIActivation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// Extensions to encryption
    /// </summary>
    internal static class MtiLicenseExtensions
    {
        /// <summary>
        /// XOR value to use for data
        /// Note: This is intentionally NOT internal there so it will not be easily visible to .NET decompilers.
        /// </summary>
        private const char XorValue = (char)0xa6;

        /// <summary>
        /// XOR value to use for length
        /// Note: This is intentionally NOT internal there so it will not be easily visible to .NET decompilers.
        /// </summary>
        private const char XorForLen = (char)0x5d;

        /// <summary>
        /// Chars to skip at beginning
        /// Note: This is intentionally NOT internal there so it will not be easily visible to .NET decompilers.
        /// </summary>
        private const int SkipStart = 2;

        /// <summary>
        /// Chars to skip after length
        /// Note: This is intentionally NOT internal there so it will not be easily visible to .NET decompilers.
        /// </summary>
        private const int SkipRandom = 2;

        // Allow Letters, Numbers, Punctuation & Braces 
        #region Public Methods and Operators

        /// <summary>
        /// The is printable char.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns>The <see cref="bool" />.</returns>
        internal static bool IsPrintableChar(this char c)
        {
            if (char.IsLetterOrDigit(c) || char.IsPunctuation(c) || c == '{' || c == '}')
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Decrypts the UINT array.
        /// Note: In order to encrypt the data passed in here, use the solution / project "EncryptLicenseString"
        /// </summary>
        /// <param name="data">The UINT data.</param>
        /// <returns>A string of the decrypted data</returns>
        internal static string DecryptUintArray(this uint[] data)
        {
            var charArray = new char[sizeof(uint) * data.Length];

            // Split the uints into a single char array
            var i = 0;
            foreach (var u in data)
            {
                charArray[i++] = (char)((u & 0xFF000000) >> 24);
                charArray[i++] = (char)((u & 0x00FF0000) >> 16);
                charArray[i++] = (char)((u & 0x0000FF00) >> 8);
                charArray[i++] = (char)(u & 0x000000FF);
            }

            // Set array location so we can get the length
            var loc = SkipStart;

            // Get the length
            var len = (charArray[loc] ^ XorForLen) - 1;   // Subtract 1 because we included the length of the length
            loc++;

            // Skip some random chars
            loc += SkipRandom;

            var decryptedArray = new char[len];

            // XOR each char to decrypt it
            for (var j = 0; j < len; j++)
            {
                decryptedArray[j] = (char)(charArray[loc] ^ XorValue);
                loc++;
            }

            var decryptedString = new string(decryptedArray);

            return decryptedString;
        }

        /// <summary>
        /// Returns a hex string of the MD5 hash
        /// </summary>
        /// <param name="hashData">a byte array</param>
        /// <returns>32 character string</returns>
        internal static string ComputeMD5Hash(this byte[] hashData)
        {
            return ComputeMD5Hashes(hashData);
        }

        /// <summary>
        /// Computes the MD5 hash.
        /// </summary>
        /// <param name="hashData">The hash data.</param>
        /// <returns>the 32 character hash string</returns>
        internal static string ComputeMD5Hash(this string hashData)
        {
            return Encoding.ASCII.GetBytes(hashData).ComputeMD5Hash();
        }

        /// <summary>
        /// Returns a hex string of the MD5 hash
        /// </summary>
        /// <param name="hashData">a list of byte arrays</param>
        /// <returns>32 character string</returns>
        internal static string ComputeMD5Hashes(params byte[][] hashData)
        {
            var md5 = MD5.Create();
            foreach (var dataBuffer in hashData)
            {
                md5.TransformBlock(dataBuffer, 0, dataBuffer.Length, null, 0);
            }

            // Finish it up
            md5.TransformFinalBlock(hashData[0], 0, 0);

            return md5.Hash.ToHex();
        }
        
        /// <summary>
        /// Checks to see if an enumeration has no elements
        /// </summary>
        /// <param name="enumerable">the enumeration to check </param>
        /// <returns>true if rows contains no elements, false otherwise</returns>
        internal static bool IsEmpty(this IEnumerable enumerable)
        {
            // Get a row so we can use reflection to use its name
            return !enumerable.GetEnumerator().MoveNext();
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified enumerable].
        /// </summary>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns>
        /// <c>true</c> if collection is null or empty; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsNullOrEmpty(this IEnumerable enumerable)
        {
            return enumerable == null || enumerable.IsEmpty();
        }

        /// <summary>
        /// Convert bytes to lower case hex representations, 2 hex values each byte.
        /// </summary>
        /// <param name="source">a byte array</param>
        /// <returns>the lower case hex representation</returns>
        internal static string ToHex(this byte[] source)
        {
            var hexStrings = from b in source select b.ToString("x2", CultureInfo.InvariantCulture);

            return string.Join(string.Empty, hexStrings.ToArray());
        }

        /// <summary>
        /// Gets the innermost exception message text.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns>A string representing the innermost exception text.</returns>
        internal static string GetInnermostExceptionMessage(this Exception ex)
        {
            var innermostExceptionMessage = ex.Message;

            var innerException = ex.InnerException;
            while (innerException != null)
            {
                innermostExceptionMessage = innerException.Message;
                innerException = innerException.InnerException;
            }

            return innermostExceptionMessage;
        }
        #endregion
    }
}