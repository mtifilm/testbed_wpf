﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiLicenseActivate.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/11/2012 2:26:51 PM</date>
// <summary>License Activation.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;
    using System.Text.RegularExpressions;

    using QlmLicenseLib;

    /// <summary>
    /// License Activation.
    /// </summary>
    internal class MtiLicenseActivate
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// Pointer to the actual QlmLicense class.
        /// </summary>
        private readonly QlmLicense qlmLicense;

        /// <summary>
        /// The license key (computer specific).
        /// </summary>
        private readonly string licenseKey;

        /// <summary>
        /// The company name for the license.
        /// </summary>
        private readonly string companyName;

        /// <summary>
        /// The company email for the license.
        /// </summary>
        private readonly string companyEmail;

        /// <summary>
        /// The user name associated with the license.
        /// </summary>
        private readonly string userName;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MtiLicenseActivate class
        /// </summary>
        /// <param name="productInfo">The license product info.</param>
        /// <param name="activationKey">The activation key.</param>
        /// <param name="machineId">The machine ID.</param>
        /// <param name="manualActivation">if set to <c>true</c> manual activation.</param>
        /// <param name="licenseKeyLocal">The local license key used for manual activation.</param>
        /// <exception cref="System.ArgumentException">Unimplemented License Vendor:  + licenseVendor</exception>
        internal MtiLicenseActivate(MtiLicenseProductInfo productInfo, string activationKey, string machineId, bool manualActivation = false, string licenseKeyLocal = null)
        {
            this.Succeeded = false;

            if (productInfo.LicenseVendor == MtiLicense.LicenseVendor.Qlm)
            {
                this.qlmLicense = new QlmLicense(true);

                // Setup for making the Activation call
                var qlmProductData = new QlmProductData(productInfo.Name, productInfo.MajorVersion, productInfo.MinorVersion);

                this.qlmLicense.CommunicationEncryptionKey = MtiLicense.QlmCommunicationEncryptionKey.DecryptUintArray();

                this.qlmLicense.DefineProduct(qlmProductData.Id, productInfo.Name, qlmProductData.MajorVersion, qlmProductData.MinorVersion, qlmProductData.EncryptionKey.DecryptUintArray(), qlmProductData.Guid.DecryptUintArray());

                this.qlmLicense.PublicKey = qlmProductData.PublicKey.DecryptUintArray();

                // Delete any old license key file
                MtiLicense.DeleteLicenseKey(productInfo);

                if (manualActivation)
                {
                    this.ErrorMessage = this.qlmLicense.ValidateLicenseEx(licenseKeyLocal, machineId);

                    var isValid = MtiLicenseValidate.GetLicenseValidityFromStatus((int)this.qlmLicense.GetStatus());

                    this.licenseKey = licenseKeyLocal;
                    this.companyName = "Manual Activation - Unknown";
                    this.companyEmail = "Manual Activation - Unknown";
                    this.userName = "Manual Activation - Unknown";
                    var expirationDate = this.qlmLicense.ExpiryDate;
                    var maintenanceEndDate = DateTime.MinValue;

                    if (isValid)
                    {
                        this.Succeeded = true;

                        var mtiLicenseData = new MtiLicenseData
                        {
                            LicenseProductInfo = productInfo,
                            ActivationKey = activationKey,
                            LicenseKey = this.licenseKey,
                            CompanyName = this.companyName,
                            UserName = this.userName,
                            ComputerId = machineId,
                            ExpirationDate = expirationDate,
                            MaintenanceEndDate = maintenanceEndDate
                        };

                        MtiLicense.SaveLicenseKey(mtiLicenseData);
                        this.Status = ReturnStatus.Success.ToString();
                    }
                    else
                    {
                        this.ErrorMessage += Environment.NewLine + this.qlmLicense.GetStatus().ToString();
                        this.ErrorMessage += ", " + this.qlmLicense.GetErrorMessage(this.licenseKey, (int)this.qlmLicense.GetStatus());
                        this.Status = ReturnStatus.Error.ToString();
                    }
                }
                else
                {
                    // Try the Activation
                    string response;
                    this.qlmLicense.ActivateLicenseEx(MtiLicense.QlmServiceUrl, activationKey, machineId, Environment.MachineName, MtiLicense.QlmEngineVersion, MtiLicense.QlmEncryptionIdType, string.Empty, out response);

                    ILicenseInfo licenseInfo = new LicenseInfo();

                    var message = string.Empty;

                    if (this.qlmLicense.ParseResults(response, ref licenseInfo, ref message))
                    {
                        this.licenseKey = licenseInfo.ComputerKey;
                        this.companyName = licenseInfo.Company;
                        this.companyEmail = licenseInfo.Email;
                        this.userName = licenseInfo.FullName;

                        if (licenseInfo.Status == ReturnStatus.Success)
                        {
                            this.Succeeded = true;

                            var expirationDate = this.GetExpirationDate(this.licenseKey, machineId);
                            var maintenanceEndDate = this.qlmLicense.GetMaintenancePlanRenewalDate(MtiLicense.QlmServiceUrl, activationKey);

                            var mtiLicenseData = new MtiLicenseData
                            {
                                LicenseProductInfo = productInfo,
                                ActivationKey = activationKey,
                                LicenseKey = this.licenseKey,
                                CompanyName = this.companyName,
                                UserName = this.userName,
                                ComputerId = machineId,
                                ExpirationDate = expirationDate,
                                MaintenanceEndDate = maintenanceEndDate
                            };

                            MtiLicense.SaveLicenseKey(mtiLicenseData);
                        }
                    }

                    this.Status = licenseInfo.Status.ToString();
                    this.ErrorMessage = AddProductNameToActivationErrorString(licenseInfo.ErrorMessage, productInfo.Name);
                }
            }
            else
            {
                throw new ArgumentException("Unimplemented License Vendor: " + productInfo.LicenseVendor);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        #region Properties

        /// <summary>
        /// Gets the license key.
        /// </summary>
        internal string LicenseKey
        {
            get
            {
                return this.licenseKey;
            }
        }

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        internal string CompanyName
        {
            get
            {
                return this.companyName;
            }
        }

        /// <summary>
        /// Gets the company email.
        /// </summary>
        internal string CompanyEmail
        {
            get
            {
                return this.companyEmail;
            }
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        internal string ErrorMessage { get; private set; }

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        internal string UserName
        {
            get
            {
                return this.userName;
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        internal string Status { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="MtiLicenseActivate"/> is succeeded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if succeeded; otherwise, <c>false</c>.
        /// </value>
        internal bool Succeeded { get; private set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the Maintenance Plan Expiration Date.
        /// </summary>
        /// <param name="activationKey">The activation key.</param>
        /// <returns>The Maintenance Plan expiration date.</returns>
        internal DateTime MtiLicenseGetMaintenanceExpirationDate(string activationKey)
        {
            var expirationDate = this.qlmLicense.GetMaintenancePlanRenewalDate(MtiLicense.QlmServiceUrl, activationKey);

            return expirationDate;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Updates the activation error string to include the Product name.
        /// </summary>
        /// <param name="errorString">The error string.</param>
        /// <param name="product">The product.</param>
        /// <returns>The modified activation error message</returns>
        private static string AddProductNameToActivationErrorString(string errorString, string product)
        {
            // Just return if we get an empty error string.
            if (errorString == string.Empty)
            {
                return errorString;
            }

            // See if the error string matches the message we are trying to add the Product name to.  If so, add it.  If not, just return what we got.
            string start;
            string version;

            var regMessage = new Regex(@"^(?<start>The activation key .*? does not correspond to) version (?<version>(\d+(\.\d+)))\.$");
            if (regMessage.IsMatch(errorString))
            {
                start = regMessage.Match(errorString).Groups["start"].Value;
                version = regMessage.Match(errorString).Groups["version"].Value;
            }
            else
            {
                return errorString;
            }

            var newErrorString = string.Format("{0} {1} {2}.", start, product, version);

            return newErrorString;
        }

        /// <summary>
        /// Gets the expiration date.
        /// </summary>
        /// <param name="licenseKeyLocal">The license key local.</param>
        /// <param name="machineId">The machine id.</param>
        /// <returns>The expiration date.</returns>
        private DateTime GetExpirationDate(string licenseKeyLocal, string machineId)
        {
            // Get the Expiration Date
            // Note: This gives back the wrong value: "var expirationDate = licenseInfo.ExpiryDate", so we use:
            // MtiLicenseValidate to get the ExpiryDate.
            this.ErrorMessage = this.qlmLicense.ValidateLicenseEx(licenseKeyLocal, machineId);
            var isValid = MtiLicenseValidate.GetLicenseValidityFromStatus((int)this.qlmLicense.GetStatus());

            DateTime expirationDate;

            if (isValid)
            {
                expirationDate = this.qlmLicense.ExpiryDate;
            }
            else
            {
                this.Succeeded = false;
                this.Status = ReturnStatus.Error.ToString();
                throw new MtiLicenseException("Activation Succeeded, but Validation failed.  Are you using an expired key?");
            }

            return expirationDate;
        }

        #endregion
    }
}
