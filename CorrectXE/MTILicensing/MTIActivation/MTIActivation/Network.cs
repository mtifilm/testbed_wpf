﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Network.cs" company="MTI Film, LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>12/8/2011 11:23:33 AM</date>
// <summary>Various useful network routines.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;

    /// <summary>
    /// Various useful network routines.
    /// </summary>
    internal class Network
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets all the MAC addresses on the system which look like IPv4 ones (length is 12) and are not Tunnel, Loopback, Virtual or USB.
        /// </summary>
        /// <returns>List of MAC addresses.</returns>
        internal static List<string> GetMacAddressesReal()
        {
            var macAddresses = new List<string>();

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel)
                {
                    var macAddress = nic.GetPhysicalAddress().ToString();

                    if (macAddress.Length != 12)
                    {
                        continue;
                    }

                    if (nic.Description.Contains("Virtual") || nic.Description.Contains("USB"))
                    {
                        continue;
                    }

                    macAddresses.Add(macAddress);
                }
            }

            macAddresses.Sort();
            return macAddresses;
        }

        /// <summary>
        /// Gets all the MAC addresses on the system which look like IPv4 ones (length is 12) and are in the "Up" state.
        /// </summary>
        /// <returns>List of MAC addresses.</returns>
        internal static List<string> GetMacAddressesUp()
        {
            var macAddresses = new List<string>();

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    var macAddress = nic.GetPhysicalAddress().ToString();

                    if (macAddress.Length == 12)
                    {
                        macAddresses.Add(macAddress);
                    }
                }
            }

            return macAddresses;
        }

        /// <summary>
        /// Gets the IPv4 addresses.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <returns>List of IP addresses.</returns>
        internal static List<string> GetIpv4Addresses(string hostName)
        {
            var ipAddresses = new List<string>();

            foreach (var ipAddress in Dns.GetHostAddresses(hostName))
            {
                var ipString = ipAddress.ToString();

                // We want only the IPv4 addresses
                if (IPAddress.Parse(ipString).AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddresses.Add(ipString);
                }
            }

            return ipAddresses;
        }

        /// <summary>
        /// Determines whether [is connected to internet].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is connected to internet]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsConnectedToInternet()
        {
            WinInetInterop.InternetConnectionState description = 0;

            var connected = WinInetInterop.InternetGetConnectedState(ref description, 0);

            // We don't use this, but in the future it could be useful.
            ////var connectionState = description.ToString();

            return connected;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
