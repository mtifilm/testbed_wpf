﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiLicenseValidate.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/9/2012 3:40:57 PM</date>
// <summary>Implementation of MTI Licensing Validation - a layer to abstract from the vendor (e.g. QLM) licensing.</summary>
// --------------------------------------------------------------------------------------------------------------------
namespace MTIActivation
{
    #region Using References

    using System;
    using System.Collections.Generic;

    using QlmLicenseLib;

    #endregion

    /// <summary>
    /// Implementation of MTI Licensing Validation - a layer to abstract from the vendor (e.g. QLM) licensing.
    /// </summary>
    internal class MtiLicenseValidate
    {
        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Constants

        /// <summary>
        /// The standard edition display name
        /// </summary>
        private const string DrsNovaEnterpriseEdition = "DRS Nova Enterprise Edition";

        /// <summary>
        /// The demo edition display name
        /// </summary>
        private const string DemoEditionDisplayName = "DRS Nova Evaluation Edition";

        /// <summary>
        /// The Dit edition display name
        /// </summary>
        private const string DrsNovaStandardEdition = "DRS Nova Standard Edition";

        #endregion

        #region Static Fields

        /// <summary>
        /// The license product info
        /// </summary>
        private static MtiLicenseProductInfo licenseProductInfo = new MtiLicenseProductInfo(string.Empty, -1, -1, -1);

        #endregion

        #region Fields

        /// <summary>
        /// Expiration Date.
        /// </summary>
        private DateTime expirationDate;

        /// <summary>
        /// Pointer to QlmProductData class.
        /// </summary>
        private QlmProductData qlmProductData;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the MtiLicenseValidate class.
        /// </summary>
        internal MtiLicenseValidate()
        {
            if (licenseProductInfo.LicenseVendor != MtiLicense.LicenseVendor.Undefined
                && licenseProductInfo.Name != string.Empty && licenseProductInfo.MajorVersion != -1
                && licenseProductInfo.MinorVersion != -1 && licenseProductInfo.Revision != -1)
            {
                this.Validate(licenseProductInfo);
            }
            else
            {
                throw new MtiLicenseException(
                    "MtiLicenseValidate() must first be called with product name, and version numbers.");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MtiLicenseValidate" /> class.
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        internal MtiLicenseValidate(MtiLicenseProductInfo productInfo)
        {
            this.Validate(productInfo);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        internal string Company { get; private set; }

        /// <summary>
        /// Gets the days left.
        /// </summary>
        internal int DaysLeft { get; private set; }

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        /// <summary>
        /// Gets the display name of the product.
        /// </summary>
        internal string DisplayProductName
        {
            get
            {
                // The sequence of checks is very important. Must be from strongest to weakest
                ////if (this.IsDemoEdition)
                ////{
                ////    return DemoEditionDisplayName;
                ////}

                if (this.IsFeatureLicensed("CBTool") && this.IsFeatureLicensed("MTIClean"))
                {
                    return DrsNovaEnterpriseEdition;
                }

                return DrsNovaStandardEdition;
            }
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        internal string ErrorMessage { get; private set; }

        /// <summary>
        /// Gets the expiration date.
        /// </summary>
        internal DateTime ExpirationDate
        {
            get
            {
                return this.expirationDate;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="MtiLicenseValidate"/> is expires.
        /// </summary>
        internal bool Expires { get; private set; }

        /// <summary>
        /// Gets the features.
        /// </summary>
        internal List<string> Features { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the current product is CortexDailies.
        /// </summary>
        internal bool IsDrsNovaEdition
        {
            get
            {
                return this.IsValid && this.ProductName == "DRSNova";
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is demo.
        /// The implementation here is done as a "not" of CortexDailies instead of as equal "Demo" to hopefully help with a hacker changing
        /// the name such to an invalid name that would turn off Demo mode.
        /// </summary>
        internal bool IsDemo
        {
            get
            {
                return this.IsValid && !this.IsDrsNovaEdition;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is the Demo Edition.
        /// </summary>
        internal bool IsDemoEdition
        {
            get
            {
                return this.IsDemo;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        internal bool IsValid { get; private set; }

        /// <summary>
        /// Gets the license key.
        /// </summary>
        internal string LicenseKey { get; private set; }

        /// <summary>
        /// Gets the major version.
        /// </summary>
        internal int MajorVersion
        {
            get
            {
                return licenseProductInfo.MajorVersion;
            }
        }

        /// <summary>
        /// Gets the minor version.
        /// </summary>
        internal int MinorVersion
        {
            get
            {
                return licenseProductInfo.MinorVersion;
            }
        }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        internal string ProductName
        {
            get
            {
                return licenseProductInfo.Name;
            }
        }

        /// <summary>
        /// Gets the QLM license.
        /// </summary>
        internal QlmLicense QlmLicense { get; private set; }

        /// <summary>
        /// Gets the revision.
        /// </summary>
        internal int Revision
        {
            get
            {
                return licenseProductInfo.Revision;
            }
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        internal string User { get; private set; }

        /// <summary>
        /// Gets the version.
        /// </summary>
        internal string Version
        {
            get
            {
                return MtiLicense.GetCombinedFullVersion(licenseProductInfo);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public Methods and Operators

        /// <summary>
        /// Gets the display product name from features.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="features">The features.</param>
        /// <returns>The Display Product Name.</returns>
        internal static string GetDisplayProductNameFromFeatures(string productName, List<string> features)
        {
            // Someday we may need this
            ////if (productName == "CortexDailies::Demo")
            ////{
            ////    return DemoEditionDisplayName;
            ////}

            ////if (features == null)
            ////{
            ////    return string.Empty;
            ////}

            ////if (features.Contains("CX_BASE") && features.Contains("CX_DAILIES") && features.Contains("CX_ENT"))
            ////{
            ////    return EnterpriseEditionDisplayName;
            ////}

            ////if (features.Contains("CX_BASE") && !features.Contains("CX_DAILIES") && features.Contains("CX_ENT"))
            ////{
            ////    return TranscodeEditionDisplayName;
            ////}

            ////if (features.Contains("CX_BASE") && features.Contains("CX_DAILIES") && !features.Contains("CX_ENT"))
            ////{
            ////    return DailiesEditionDisplayName;
            ////}

            // We should never all threw to here.
            return string.Empty;
        }

        /// <summary>
        /// Gets the license validity from the QLM ValidateLicenseEx() status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>True or false based on value of status from QLM's ValidateLicenseEx()</returns>
        internal static bool GetLicenseValidityFromStatus(int status)
        {
            bool valid;

            ///////////////////////////////////////////////////
            // Check if license is valid
            // For the definition of IsTrue, see below
            //////////////////////////////////////////////////
            //// TODO: Need to make sure we properly handle Demo Keys
            if (IsTrue(status, (int)ELicenseStatus.EKeyInvalid)
                || IsTrue(status, (int)ELicenseStatus.EKeyProductInvalid)
                || IsTrue(status, (int)ELicenseStatus.EKeyVersionInvalid)
                || IsTrue(status, (int)ELicenseStatus.EKeyMachineInvalid)
                || IsTrue(status, (int)ELicenseStatus.EKeyTampered)
                || IsTrue(status, (int)ELicenseStatus.EKeyNeedsActivation)
                || IsTrue(status, (int)ELicenseStatus.EKeyExceededAllowedInstances))
            {
                // the key is invalid
                valid = false;
            }
            else if (IsTrue(status, (int)ELicenseStatus.EKeyDemo))
            {
                if (IsTrue(status, (int)ELicenseStatus.EKeyExpired))
                {
                    // the key has expired
                    valid = false;
                }
                else
                {
                    // the demo key is still valid
                    valid = true;
                }
            }
            else if (IsTrue(status, (int)ELicenseStatus.EKeyPermanent))
            {
                // the key is OK
                valid = true;
            }
            else
            {
                // Unexpected status - just return.
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// Invalidates this instance.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        internal void Invalidate(string errorMessage = "")
        {
            this.IsValid = false;
            this.LicenseKey = string.Empty;
            this.Features = new List<string>();
            this.DaysLeft = 0;
            this.Expires = true;
            this.expirationDate = DateTime.MinValue;
            this.QlmLicense = null;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Determines whether the specified feature is licensed.
        /// </summary>
        /// <param name="feature">The feature.</param>
        /// <returns>True if feature is licensed.</returns>
        internal bool IsFeatureLicensed(string feature)
        {
            return this.Features.Contains(feature);
        }

        /// <summary>
        /// Determines whether feature is licensed, fake parameter name
        /// </summary>
        /// <param name="dummyCheck">The dummy check.</param>
        /// <param name="seed">The dummy seed.</param>
        /// <returns><c>true</c> if feature is licensed; otherwise, <c>false</c>.</returns>
        internal bool IsFeatureLicensed(uint[] dummyCheck, int seed)
        {
            return this.IsFeatureLicensed(dummyCheck.DecryptUintArray());
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Methods

        /// <summary>
        /// Compares flags - from QLM example.
        /// </summary>
        /// <param name="value1">Value 1 to compare.</param>
        /// <param name="value2">Value 2 to compare.</param>
        /// <returns>True/False based on whether bitwise AND of the supplied ints is the same as either one.</returns>
        private static bool IsTrue(int value1, int value2)
        {
            if (((value1 & value2) == value1) || ((value1 & value2) == value2))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the QLM licensed features.
        /// </summary>
        /// <returns>List of the licensed features.</returns>
        private List<string> GetQlmLicensedFeatures()
        {
            var featureList = new List<string>();

            for (var featureSet = 0; featureSet < 4; featureSet++)
            {
                for (var j = 0; j < 8; j++)
                {
                    var featureId = 1 << j;

                    if (this.QlmLicense.IsFeatureEnabledEx(featureSet, featureId))
                    {
                        var featureIndex = QlmProductData.GetFeatureIndex(featureSet, featureId);
                        var featureName = this.qlmProductData.Features.ContainsKey(featureIndex)
                                              ? this.qlmProductData.Features[featureIndex]
                                              : string.Format("Unknown {0}/{1}", featureSet, featureId);

                        featureList.Add(featureName);
                    }
                }
            }

            // To allow 1.6 and 2.0 to run together, we translate the CD features into CX features
            // Or visa-versa
            // This translation will be removed when we go to 2.0
            //// DELETE AFTER 2.0 GOES INTO PRODUCTION
            ////var features20To16 = new Dictionary<string, string>
            ////                      {
            ////                          { "CX_BASE", "CD_RENDER" },
            ////                          { "CX_DAILIES", "CD_DAILIES" },
            ////                          { "CX_ENT", "CD_SHARE" },
            ////                          { "CX_BETA", "CD_BETA" },
            ////                          { "CX_CDC_FREE", "CD_ALC_DVD" },
            ////                          { "CX_CDC_H264", "CD_ALC_BLURAY" },
            ////                          { "CX_CDC_STD", "CD_ALC_VC3" },
            ////                          { "CX_CDC_ENT", "CD_ALC_CINEFORM" },
            ////                          { "CX_ALC_IPAD", "CD_ALC_IPAD" }
            ////                      };

            ////if (this.MajorVersion == 2)
            ////{
            ////    foreach (var f in features20To16.Where(f => featureList.Contains(f.Key)))
            ////    {
            ////        featureList.Add(f.Value);
            ////    }
            ////}
            ////else
            ////{
            ////    foreach (var f in features20To16.Where(f => featureList.Contains(f.Value)))
            ////    {
            ////        featureList.Add(f.Key);
            ////    }
            ////}

            //// END DELETE AFTER 2.0 GOES INTO PRODUCTIN
            return featureList;
        }

        /// <summary>
        /// Validates the specified license.
        /// </summary>
        /// <param name="licenseProductInfoLocal">The license product info local.</param>
        private void Validate(MtiLicenseProductInfo licenseProductInfoLocal)
        {
            licenseProductInfo = licenseProductInfoLocal;

            this.Invalidate();

            if (licenseProductInfo.LicenseVendor == MtiLicense.LicenseVendor.Qlm)
            {
                this.QlmLicense = new QlmLicense(true);
                this.qlmProductData = new QlmProductData(
                    licenseProductInfo.Name, 
                    licenseProductInfo.MajorVersion, 
                    licenseProductInfo.MinorVersion);

                // Set these based on what matched in QlmProductData constructor - will be 0 if no match.
                licenseProductInfo.MajorVersion = this.qlmProductData.MajorVersion;
                licenseProductInfo.MinorVersion = this.qlmProductData.MinorVersion;

                var machineId = MtiLicense.GetMachineId();
                var licenseKeyLocal = MtiLicense.GetLicenseKey(licenseProductInfo);

                this.Company = MtiLicense.Company;
                this.User = MtiLicense.User;

                this.QlmLicense.CommunicationEncryptionKey = MtiLicense.QlmCommunicationEncryptionKey.DecryptUintArray();
                
                this.QlmLicense.DefineProduct(
                    this.qlmProductData.Id, 
                    licenseProductInfo.Name, 
                    this.qlmProductData.MajorVersion, 
                    this.qlmProductData.MinorVersion, 
                    this.qlmProductData.EncryptionKey.DecryptUintArray(), 
                    this.qlmProductData.Guid.DecryptUintArray());

                this.QlmLicense.PublicKey = this.qlmProductData.PublicKey.DecryptUintArray();

                this.ErrorMessage = this.QlmLicense.ValidateLicenseEx(licenseKeyLocal, machineId);

                var status = (int)this.QlmLicense.GetStatus();

                this.IsValid = GetLicenseValidityFromStatus(status);

                ///////////////////////////////////////////////////
                // Set Other properties if license is Valid
                ///////////////////////////////////////////////////
                if (this.IsValid)
                {
                    ///////////////////////////////////////////////////
                    // Set License Key
                    ///////////////////////////////////////////////////
                    this.LicenseKey = licenseKeyLocal;

                    ///////////////////////////////////////////////////
                    // Set Expiration Fields
                    ///////////////////////////////////////////////////

                    // DaysLeft == -1 means that the license doesn't expire
                    if (this.QlmLicense.DaysLeft == -1)
                    {
                        this.DaysLeft = int.MaxValue;
                        this.Expires = false;
                        this.expirationDate = DateTime.MaxValue;
                    }
                    else
                    {
                        this.DaysLeft = this.QlmLicense.DaysLeft;
                        this.Expires = true;
                        this.expirationDate = this.QlmLicense.ExpiryDate;
                    }

                    ///////////////////////////////////////////////////
                    // Set Features
                    ///////////////////////////////////////////////////
                    this.Features = this.GetQlmLicensedFeatures();
                }

                // Require a BETA feature license to run a not-yet-released version (1.5.0)
                // An official release will be tagged as .1 (1.5.1)
                if (this.Revision == 0 && !this.IsDemo && !this.IsFeatureLicensed("CX_BETA"))
                {
                    this.Invalidate("This version requires a Beta license");
                }
            }
            else
            {
                throw new ArgumentException("Unimplemented License Vendor: " + licenseProductInfo.LicenseVendor);
            }
        }

        #endregion
    }
}