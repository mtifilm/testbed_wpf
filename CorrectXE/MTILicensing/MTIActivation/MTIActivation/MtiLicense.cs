﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiLicense.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>1/11/2012 2:28:07 PM</date>
// <summary>Common MTILicense code.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;

    /// <summary>
    /// Common MTILicense code.
    /// </summary>
    internal class MtiLicense
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// URL of the QLM web service.
        /// </summary>
        internal const string QlmServiceUrl = @"https://quicklicensemanager.com/mtifilm/qlm/qlmservice.asmx";

        /// <summary>
        /// QLM engine version.
        /// </summary>
        internal const string QlmEngineVersion = "5.0.0";

        /// <summary>
        /// Encryption type we use.
        /// </summary>
        internal const string QlmEncryptionIdType = "Custom";

        /// <summary>
        /// Number of Uints in an Encrypted Uint Array - this is based on an original string of length 64 and putting 4 chars per uint.
        /// </summary>
        internal const int UintArraySize = 16;

        /// <summary>
        /// Directory for Licenses
        /// </summary>
        internal const string LicenseDirectory = @"C:\ProgramData\MTI Film\Licenses";

        /// <summary>
        /// The default license product info for unit testing.
        /// </summary>
        internal static readonly MtiLicenseProductInfo UnitTestingProductInfo = new MtiLicenseProductInfo(LicenseVendor.Qlm, @"CortexDailies::Dailies", 2, 0, 0);

        /// <summary>
        /// Uint version of Qlm Communication Encryption Key - used to connect to the QLM web service.
        /// qlmCommunicationEncryptionKey = "{946B7514-9B10-4EAE-8E9D-6707B2669084}"
        /// </summary>
        internal static readonly uint[] QlmCommunicationEncryptionKey = { 0x72f57ace, 0x30dd9f92, 0x90e49193, 0x97928b9f, 0xe497968b, 0x92e3e7e3, 0x8b9ee39f, 0xe28b9091, 0x9691e494, 0x90909f96, 0x9e92db62, 0x534b65f1, 0xe56c52c1, 0xc34b7575, 0xc964f123, 0x3446e26a };

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The license selector file
        /// </summary>
        private static readonly string LicenseSelectorFile = Path.Combine(LicenseDirectory, "ActiveCortexDailiesLicense.txt");

        /// <summary>
        /// Backing store for MaintenanceEndDate.
        /// </summary>
        private static DateTime? maintenanceEndDate;

        /// <summary>
        /// Backing store for ExpirationDate.
        /// </summary>
        private static DateTime? expirationDate;

        #endregion

        //////////////////////////////////////////////////
        // Enums
        //////////////////////////////////////////////////
        #region Enums

        /// <summary>
        /// The License Vendor.
        /// </summary>
        internal enum LicenseVendor
        {
            /// <summary>
            /// Undefined License Vendor.
            /// </summary>
            Undefined,

            /// <summary>
            /// QLM License Vendor.
            /// </summary>
            Qlm
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets the hostname.
        /// </summary>
        internal static string Hostname { get; private set; }

        /// <summary>
        /// Gets the mac address.
        /// </summary>
        internal static string MacAddress { get; private set; }

        /// <summary>
        /// Gets the HDD serial number.
        /// </summary>
        internal static string HddSerialNumber { get; private set; }

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        internal static string Company { get; private set; }

        /// <summary>
        /// Gets the user.
        /// </summary>
        internal static string User { get; private set; }

        /// <summary>
        /// Gets the activation key.
        /// </summary>
        internal static string ActivationKey { get; private set; }

        /// <summary>
        /// Gets the expiration date.
        /// </summary>
        /// <remarks>
        /// Requires backing store (can't be auto-property) due to DateTime.TryParse().
        /// </remarks>
        // ReSharper disable ConvertToAutoProperty
        internal static DateTime? ExpirationDate
        // ReSharper restore ConvertToAutoProperty
        {
            get
            {
                return expirationDate;
            }

            private set
            {
                expirationDate = value;
            }
        }

        /// <summary>
        /// Gets the maintenance end date.
        /// </summary>
        /// <remarks>
        /// Requires backing store (can't be auto-property) due to DateTime.TryParse().
        /// </remarks>
        // ReSharper disable ConvertToAutoProperty
        internal static DateTime? MaintenanceEndDate
        // ReSharper restore ConvertToAutoProperty
        {
            get
            {
                return maintenanceEndDate;
            }

            private set
            {
                maintenanceEndDate = value;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the machine ID.
        /// </summary>
        /// <returns>The machine ID - a combination of the hostname, first MAC address and system disk's serial number.</returns>
        internal static string GetMachineId()
        {
            Hostname = Environment.MachineName;

            var macAddresses = Network.GetMacAddressesReal();

            // Assign a bogus MAC address if there really isn't one
            MacAddress = macAddresses.Count > 0 ? macAddresses[0] : "F9B08D297579";
            
            HddSerialNumber = Hardware.GetHddSerialNumber(Environment.SystemDirectory);

            var custom = Hostname + MacAddress + HddSerialNumber;

            var machineId = custom.ComputeMD5Hash();

            return machineId;
        }

        /// <summary>
        /// Gets the short formatted product string (no revision).
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="majorVersion">The major version.</param>
        /// <param name="minorVersion">The minor version.</param>
        /// <returns>The formatted product string - e.g. "COHOG 1.0"</returns>
        internal static string GetFormattedProductStringShort(string product, int majorVersion, int minorVersion)
        {
            return string.Format("{0} {1}.{2}", product, majorVersion, minorVersion);
        }

        /// <summary>
        /// Gets the formatted product string.
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The formatted product string - e.g. "COHOG 1.0.2"</returns>
        internal static string GetFormattedProductString(MtiLicenseProductInfo productInfo)
        {
            return string.Format("{0} {1}.{2}.{3}", productInfo.Name, productInfo.MajorVersion, productInfo.MinorVersion, productInfo.Revision);
        }

        /// <summary>
        /// Gets the combined version string - e.g. "1.0"
        /// </summary>
        /// <param name="majorVersion">The major version.</param>
        /// <param name="minorVersion">The minor version.</param>
        /// <returns>The combined version string - e.g. "1.0"</returns>
        internal static string GetCombinedVersion(int majorVersion, int minorVersion)
        {
            return string.Format("{0}.{1}", majorVersion, minorVersion);
        }

        /// <summary>
        /// Gets the combined version string - e.g. "1.0.1"
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The combined version string - e.g. "1.0.1"</returns>
        internal static string GetCombinedFullVersion(MtiLicenseProductInfo productInfo)
        {
            return string.Format("{0}.{1}.{2}", productInfo.MajorVersion, productInfo.MinorVersion, productInfo.Revision);
        }

        /// <summary>
        /// Gets the combined full version from the full product string (e.g. Cortex::ControlDailies-1.0.1 -> 1.0.1).
        /// </summary>
        /// <param name="fullProductString">The full product string.</param>
        /// <returns>The combined version string - e.g. "1.0.1"</returns>
        internal static string GetCombinedFullVersion(string fullProductString)
        {
            var parts = fullProductString.Split('-');
            return parts.Length >= 2 ? parts[1] : string.Empty;
        }

        /// <summary>
        /// Gets the major version from the version string.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>The major version.</returns>
        internal static int GetMajorVersionFromCombinedVersion(string version)
        {
            return GetSubVersionFromCombinedVersion(version, 0);
        }

        /// <summary>
        /// Gets the minor version from the version string.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>The minor version.</returns>
        internal static int GetMinorVersionFromCombinedVersion(string version)
        {
            return GetSubVersionFromCombinedVersion(version, 1);
        }

        /// <summary>
        /// Gets the revision from the version string.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>The revision.</returns>
        internal static int GetRevisionFromCombinedVersion(string version)
        {
            return GetSubVersionFromCombinedVersion(version, 2);
        }

        /// <summary>
        /// Gets the sub-version from the version string.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <param name="index">The index of the sub-version (0: major, 1: minor, 2: revision).</param>
        /// <returns>The sub-version according to the supplied index .</returns>
        internal static int GetSubVersionFromCombinedVersion(string version, int index)
        {
            var subVersion = 0;

            // Remove possible product name start (e.g. Cortex::ControlDailies)
            if (version.Contains("-"))
            {
                version = GetCombinedFullVersion(version);
            }

            var parts = version.Split('.');
            if (parts.Length > index)
            {
                int.TryParse(parts[index], out subVersion);
            }

            return subVersion;
        }

        /// <summary>
        /// Gets the license key.
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The license key.</returns>
        internal static string GetLicenseKey(MtiLicenseProductInfo productInfo)
        {
            return ReadLicenseKey(productInfo);
        }

        /// <summary>
        /// Gets the most recent activation key for the specified product.
        /// </summary>
        /// <param name="productInfo">The product information.</param>
        /// <param name="activationKey">The activation key.</param>
        /// <param name="priorVersion">The prior version found.</param>
        /// <returns>True if there are significant changes since the last licensed product version</returns>
        internal static bool GetMostRecentActivationKey(MtiLicenseProductInfo productInfo, out string activationKey, out string priorVersion)
        {
            activationKey = null;
            priorVersion = null;

            // If no matching file is found for the product in productInfo, then fileProductData will be null
            var mostRecentFile = GetMostRecentLicenseFilename(productInfo.Name);
            var fileProductNameFull = ReadProduct(mostRecentFile);
            var fileProductName = GetProductNameFromFullProductName(fileProductNameFull);
            var qlmLicenseProductsTable = new QlmLicenseProductsTable();
            var fileProductData = qlmLicenseProductsTable.GetLatestQlmProductData(fileProductName);

            // If there has been a significant change since the previous version, a new Activation Key will be required.
            if (qlmLicenseProductsTable.HasSignificantChangeSince(fileProductData))
            {
                return true;
            }

            // Get the activation key from the most recent file (typically the previous version).
            activationKey = ReadActivationKey(mostRecentFile);
            priorVersion = GetCombinedFullVersion(ReadProduct(mostRecentFile));

            return false;
        }

        /// <summary>
        /// True if the products are the same while ignoring the version number (e.g. CortexDailies::Dailies 1.2.3 vs. CortexDailies::Dailies 1.2.6)
        /// </summary>
        /// <param name="productInfo1">The product info1.</param>
        /// <param name="productInfo2">The product info2.</param>
        /// <returns>True if they are the same product (ignoring the revision number).</returns>
        internal static bool AreSameProductIgnoringRevision(MtiLicenseProductInfo productInfo1, MtiLicenseProductInfo productInfo2)
        {
            return productInfo1.LicenseVendor == productInfo2.LicenseVendor
                   && productInfo1.Name == productInfo2.Name
                   && productInfo1.MajorVersion == productInfo2.MajorVersion
                   && productInfo1.MinorVersion == productInfo2.MinorVersion;
        }

        /// <summary>
        /// Checks if the active cortex license selector file exists.
        /// </summary>
        /// <returns>True if the file exists.</returns>
        internal static bool DoesActiveCortexLicenseSelectorFileExist()
        {
            return File.Exists(LicenseSelectorFile);
        }

        /// <summary>
        /// Gets the active Cortex license product (e.g. Cortex::ControlDailies).
        /// </summary>
        /// <returns>The active Cortex license product name.</returns>
        internal static string GetActiveCortexLicenseProduct()
        {
            var productName = string.Empty;

            if (File.Exists(LicenseSelectorFile))
            {
                using (var textReader = new StreamReader(LicenseSelectorFile))
                {
                    productName = textReader.ReadLine();
                }
            }

            return productName;
        }

        /// <summary>
        /// Saves the Active Cortex license product (e.g. Cortex::ControlDailies).
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        internal static void SaveActiveCortexLicenseProduct(string productName)
        {
            CreateLicenseDirectoryIfNeeded();

            // Write the file
            try
            {
                using (var writer = new StreamWriter(LicenseSelectorFile))
                {
                    writer.WriteLine(productName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error writing license selector file: '" + LicenseSelectorFile + "' - " + ex.Message);
            }
        }

        /// <summary>
        /// Saves the license key.
        /// </summary>
        /// <param name="mtiLicenseData">The MTI license data.</param>
        internal static void SaveLicenseKey(MtiLicenseData mtiLicenseData)
        {
            // This is normally set within GetMachineId(), but in case that wasn't already called, we set it here.
            if (string.IsNullOrEmpty(Hostname))
            {
                Hostname = Environment.MachineName;
            }

            WriteLicenseKey(mtiLicenseData);
        }

        /// <summary>
        /// Deletes the license key file.
        /// </summary>
        /// <param name="productInfo">The license product info.</param>
        internal static void DeleteLicenseKey(MtiLicenseProductInfo productInfo)
        {
            // Can't delete file if directory doesn't exist.
            if (!Directory.Exists(LicenseDirectory))
            {
                return;
            }

            // Get Filename
            var filename = GetLicenseFilename(productInfo);

            // Can't delete file if file doesn't exist.
            if (!File.Exists(filename))
            {
                return;
            }

            try
            {
                File.Delete(filename);
            }
            catch (Exception ex)
            {
                throw new Exception("Error deleting existing license file: '" + filename + "' - " + ex.Message);
            }
        }

        /// <summary>
        /// Gets the Product License Name based on the EXE name.
        /// </summary>
        /// <returns>The Product License Name.</returns>
        internal static string GetProductLicenseName()
        {
            if (!DoesActiveCortexLicenseSelectorFileExist())
            {
                SaveActiveCortexLicenseProduct("CortexDailies::Dailies");
            }

            return GetActiveCortexLicenseProduct();
        }

        /// <summary>
        /// Gets the primary executable's major version.
        /// </summary>
        /// <returns>The primary executable's major version</returns>
#if DEBUG
        [DebuggerNonUserCode]
#endif
        internal static int GetPrimaryExecutableMajorVersion()
        {
            var primaryExeInfo = GetPrimaryExecutableInformation();

            if (primaryExeInfo.DeveloperModeDvdBluRayHack)
            {
                var qlmLicenseProductsTable = new QlmLicenseProductsTable();
                return qlmLicenseProductsTable.GetLatestProductVersionMajor(GetProductLicenseName());
            }

            return AssemblyName.GetAssemblyName(Path.Combine(primaryExeInfo.Directory, primaryExeInfo.Filename)).Version.Major;
        }

        /// <summary>
        /// Gets the primary executable's minor version.
        /// </summary>
        /// <returns>The primary executable's minor version</returns>
        internal static int GetPrimaryExecutableMinorVersion()
        {
            var primaryExeInfo = GetPrimaryExecutableInformation();

            if (primaryExeInfo.DeveloperModeDvdBluRayHack)
            {
                var qlmLicenseProductsTable = new QlmLicenseProductsTable();
                return qlmLicenseProductsTable.GetLatestProductVersionMinor(GetProductLicenseName());
            }

            return AssemblyName.GetAssemblyName(Path.Combine(primaryExeInfo.Directory, primaryExeInfo.Filename)).Version.Minor;
        }

        /// <summary>
        /// Gets the primary executable's revision.
        /// </summary>
        /// <returns>The primary executable's revision.</returns>
        internal static int GetPrimaryExecutableRevision()
        {
            var primaryExeInfo = GetPrimaryExecutableInformation();

            if (primaryExeInfo.DeveloperModeDvdBluRayHack)
            {
                // Since revision numbers aren't actually in the QlmLicenseProductsTable, we always return 0 here
                // in the case where we cannot actually check the executable b/c we are in "DeveloperModeDvdBluRayHack" mode.
                return 0;
            }

            return AssemblyName.GetAssemblyName(Path.Combine(primaryExeInfo.Directory, primaryExeInfo.Filename)).Version.Revision;
        }

        /// <summary>
        /// Gets the primary executable product info.
        /// </summary>
        /// <returns>The primary executable product info.</returns>
        internal static MtiLicenseProductInfo GetPrimaryExecutableProductInfo()
        {
            var productInfo = new MtiLicenseProductInfo(LicenseVendor.Qlm, GetProductLicenseName(), GetPrimaryExecutableMajorVersion(), GetPrimaryExecutableMinorVersion(), GetPrimaryExecutableRevision());

            return productInfo;
        }

        /// <summary>
        /// Clears the fields that were read from the license key file.
        /// </summary>
        internal static void Clear()
        {
            Company = string.Empty;
            User = string.Empty;
            ActivationKey = string.Empty;
            MaintenanceEndDate = DateTime.MinValue;
            ExpirationDate = DateTime.MinValue;
        }

        /// <summary>
        /// Gets the short name of the product name from the product name (e.g. Cortex::ControlDailies -> ControlDailies).
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <returns>The short name of the product name from the product name.</returns>
        internal static string GetShortProductNameFromProductName(string productName)
        {
            if (productName.IndexOf("::", StringComparison.Ordinal) == -1)
            {
                return null;
            }

            var shortProductName = productName.Substring(productName.LastIndexOf("::", StringComparison.Ordinal) + 2);

            return shortProductName;
        }

        /// <summary>
        /// Gets the name of the product from the full product name.
        /// </summary>
        /// <param name="fullProductName">Full name of the product.</param>
        /// <returns>The product name.</returns>
        internal static string GetProductNameFromFullProductName(string fullProductName)
        {
            if (fullProductName.IndexOf('-') == -1)
            {
                return null;
            }

            var productName = fullProductName.Substring(0, fullProductName.LastIndexOf('-'));

            return productName;
        }

        /// <summary>
        /// Dumps the license information.
        /// </summary>
        /// <param name="mtiLicenseValidate">The mti license validate.</param>
        internal static void DumpLicenseInfo(MtiLicenseValidate mtiLicenseValidate)
        {
            Console.WriteLine("Product:          {0}", mtiLicenseValidate.ProductName);
            Console.WriteLine("Version:          {0}", mtiLicenseValidate.Version);
            Console.WriteLine("License Key:      {0}", mtiLicenseValidate.LicenseKey);
            Console.WriteLine("Company:          {0}", mtiLicenseValidate.Company);
            Console.WriteLine("User:             {0}", mtiLicenseValidate.User);
            Console.WriteLine("Hostname:         {0}", Hostname);
            Console.WriteLine("Computer ID:      {0}", GetMachineId());
            Console.WriteLine("Expiration:       {0}", GetExpirationDatePretty(mtiLicenseValidate));
            Console.WriteLine("Maintenance Ends: {0}", GetMaintenanceEndDatePretty());

            foreach (var feature in mtiLicenseValidate.Features)
            {
                Console.WriteLine("Feature:          {0}", feature);
            }
        }

        /// <summary>
        /// Gets the expiration date such that non-expiring returns the string "Never".
        /// </summary>
        /// <param name="mtiLicenseValidate">The MTILicenseValidate class.</param>
        /// <returns>Expiration Date in pretty format.</returns>
        internal static string GetExpirationDatePretty(MtiLicenseValidate mtiLicenseValidate)
        {
            return mtiLicenseValidate.Expires ? mtiLicenseValidate.ExpirationDate.ToString(CultureInfo.CurrentCulture) : "Never";
        }

        /// <summary>
        /// Gets the maintenance end date in pretty format.
        /// </summary>
        /// <returns>Maintenance End Date in pretty format</returns>
        internal static string GetMaintenanceEndDatePretty()
        {
            string maintEndDate;
            if (MaintenanceEndDate == null)
            {
                maintEndDate = "Unknown";
            }
            else if (MaintenanceEndDate == DateTime.MinValue)
            {
                maintEndDate = "This version only";
            }
            else
            {
                maintEndDate = ((DateTime)MaintenanceEndDate).ToString(CultureInfo.CurrentCulture);
            }

            return maintEndDate;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Creates the license directory if needed.
        /// </summary>
        /// <exception cref="System.Exception">Error creating licensing directory: ' + LicenseDirectory + ' -  + ex.Message</exception>
        private static void CreateLicenseDirectoryIfNeeded()
        {
            if (!Directory.Exists(LicenseDirectory))
            {
                try
                {
                    Directory.CreateDirectory(LicenseDirectory);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating licensing directory: '" + LicenseDirectory + "' - " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Writes the license key and associated info to the XML file.
        /// </summary>
        /// <param name="mtiLicenseData">The mti license data.</param>
        private static void WriteLicenseKey(MtiLicenseData mtiLicenseData)
        {
            CreateLicenseDirectoryIfNeeded();

            var filename = GetLicenseFilename(mtiLicenseData.LicenseProductInfo);

            // Setup parameters for XML license file
            var xmlWriterSettings = new XmlWriterSettings { Indent = true, Encoding = new UTF8Encoding(false) };

            // Write the XML file
            try
            {
                using (var writer = XmlWriter.Create(filename, xmlWriterSettings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("MTILicense"); // MTILicense element start

                    writer.WriteElementString("Product", GetFullProductName(mtiLicenseData.LicenseProductInfo));
                    writer.WriteElementString("ActivationKey", mtiLicenseData.ActivationKey);
                    writer.WriteElementString("LicenseKey", mtiLicenseData.LicenseKey);
                    writer.WriteElementString("Company", mtiLicenseData.CompanyName);
                    writer.WriteElementString("User", mtiLicenseData.UserName);
                    writer.WriteElementString("Hostname", mtiLicenseData.Hostname);
                    writer.WriteElementString("ComputerId", mtiLicenseData.ComputerId);
                    writer.WriteElementString("ExpirationDate", mtiLicenseData.ExpirationDate.ToString(CultureInfo.InvariantCulture));
                    writer.WriteElementString("MaintenanceEndDate", mtiLicenseData.MaintenanceEndDate.ToString(CultureInfo.InvariantCulture));

                    writer.WriteEndElement(); // MTILicense element end
                    writer.WriteEndDocument();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error writing license file: '" + filename + "' - " + ex.Message);
            }
        }

        /// <summary>
        /// Reads the license key.
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The license key.</returns>
        private static string ReadLicenseKey(MtiLicenseProductInfo productInfo)
        {
            var filename = GetLicenseFilename(productInfo);

            if (!File.Exists(filename))
            {
                return string.Empty;
            }

            // Find the key within the XML and return it
            var key = string.Empty;

            using (var reader = XmlReader.Create(filename))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        // Get element name and switch on it.
                        string data;

                        switch (reader.Name)
                        {
                            case "Product":
                                var productActual = GetXmlValue(reader);

                                // Sanity Check to make sure this is the right license file for this version or a newer one.
                                var productInfoActual = new MtiLicenseProductInfo(productActual);

                                if (productInfo > productInfoActual)
                                {
                                    return string.Empty;
                                }

                                break;
                            case "LicenseKey":
                                key = GetXmlValue(reader);

                                break;
                            case "Hostname":
                                var hostActual = GetXmlValue(reader);

                                // Sanity Check to make sure this is the right host.
                                if (hostActual != Hostname)
                                {
                                    return string.Empty;
                                }

                                break;

                            case "Company":
                                Company = GetXmlValue(reader);

                                break;

                            case "User":
                                User = GetXmlValue(reader);

                                break;

                            case "ExpirationDate":
                                data = GetXmlValue(reader);
                                DateTime tmpExpirationDate;
                                DateTime.TryParse(data, out tmpExpirationDate);
                                expirationDate = tmpExpirationDate;

                                break;

                            case "MaintenanceEndDate":
                                data = GetXmlValue(reader);
                                DateTime tmpMaintenanceEndDate;
                                DateTime.TryParse(data, out tmpMaintenanceEndDate);
                                maintenanceEndDate = tmpMaintenanceEndDate;

                                break;

                            case "ActivationKey":
                                ActivationKey = GetXmlValue(reader);

                                break;
                        }
                    }
                }

                return key;
            }
        }

        /// <summary>
        /// Reads the value for the specified key from the license file.
        /// </summary>
        /// <param name="filename">The license filename.</param>
        /// <param name="key">The key.</param>
        /// <returns>The value read.</returns>
        private static string ReadValue(string filename, string key)
        {
            if (!File.Exists(filename))
            {
                return string.Empty;
            }

            // Find the value within the XML and return it
            var value  = string.Empty;
            using (var reader = XmlReader.Create(filename))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        if (reader.Name == key)
                        {
                            value = GetXmlValue(reader);

                            break;
                        }
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Reads the product from the specified file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>The product.</returns>
        private static string ReadProduct(string filename)
        {
            // Find the key within the XML and return it
            var activationKey = ReadValue(filename, "Product");

            return activationKey;
        }

        /// <summary>
        /// Reads the activation key from the specified file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>The activation key.</returns>
        private static string ReadActivationKey(string filename)
        {
            // Find the key within the XML and return it
            var activationKey = ReadValue(filename, "ActivationKey");

            return activationKey;
        }

        /// <summary>
        /// Gets the XML value.
        /// </summary>
        /// <param name="reader">The XML reader.</param>
        /// <returns>Value read from XML.</returns>
        private static string GetXmlValue(XmlReader reader)
        {
            var value = string.Empty;

            if (reader.Read())
            {
                value = reader.Value.Trim();
            }

            return value;
        }

        /// <summary>
        /// Gets the full name of the product with the revision number (e.g. Cortex::ControlDailies-1.2.3).
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The full product name. (e.g. Cortex::ControlDailies-1.2.3)</returns>
        private static string GetFullProductName(MtiLicenseProductInfo productInfo)
        {
            return string.Format("{0}-{1}.{2}.{3}", productInfo.Name, productInfo.MajorVersion, productInfo.MinorVersion, productInfo.Revision);
        }

        /// <summary>
        /// Gets the license filename of the product with the version number (e.g. MtiLicense_Cortex__ControlDailies-1.2).
        /// </summary>
        /// <param name="productInfo">The product info.</param>
        /// <returns>The license filename of the product with the version number (e.g. MtiLicense_Cortex__ControlDailies-1.2).</returns>
        private static string GetLicenseFilenameBaseWithVersion(MtiLicenseProductInfo productInfo)
        {
            return string.Format("{0}-{1}.{2}", GetLicenseFilenameBase(productInfo.Name), productInfo.MajorVersion, productInfo.MinorVersion);
        }

        /// <summary>
        /// Gets the license filename.
        /// </summary>
        /// <param name="productInfo">The license product info.</param>
        /// <returns>The full filename for the specified license.</returns>
        private static string GetLicenseFilename(MtiLicenseProductInfo productInfo)
        {
            var filename = string.Format("{0}.qlm", GetLicenseFilenameBaseWithVersion(productInfo));

            return Path.Combine(LicenseDirectory, filename);
        }

        /// <summary>
        /// Gets the license filename base.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The license filename base (e.g. MTILicense_Cortex__ControlDailies)</returns>
        private static string GetLicenseFilenameBase(string product)
        {
            var productForFilename = product.Replace("::", "__");

            var filenameBase = string.Format("MTILicense_{0}", productForFilename);

            return filenameBase;
        }

        /// <summary>
        /// Gets the primary executable's information - directory, filename and short filename.
        /// </summary>
        /// <returns>The primary executable's information - directory, filename and short filename</returns>
        private static dynamic GetPrimaryExecutableInformation()
        {
            var exeName = AppDomain.CurrentDomain.FriendlyName;

            // Get just the part before .exe (or .vshost.exe during debugging) - e.g. "Cortex"
            if (exeName.LastIndexOf(".vshost.exe", StringComparison.Ordinal) != -1)
            {
                exeName = exeName.Substring(0, exeName.LastIndexOf(".vshost.exe", StringComparison.Ordinal));
            }
            else if (exeName.LastIndexOf(".exe", StringComparison.Ordinal) != -1)
            {
                exeName = exeName.Substring(0, exeName.LastIndexOf(".exe", StringComparison.Ordinal));
            }

            // Get the directory where the application is running from
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;

            // Setup our dynamic result and initialize it.
            dynamic result = new ExpandoObject();

            result.Filename = string.Empty;
            result.ShortFilename = string.Empty;
            result.Directory = string.Empty;

            // This is used only in the case of _mtiDvdBuilder.exe & _mtiBdBuilder.exe for developers.  When we decide we are running in Developer Mode, then
            // we set this flag because normally the directory of the exe determines the path to use to search for the primary exe.  Since in Developer Mode,
            // _mtiDvdBuilder.exe & _mtiBdBuilder.exe are located at an absolute path location (as defined by MTI_COHOGDVD_DIR and MTI_COHOGBLURAY_DIR), we
            // cannot figure out where the primary EXE is located so we can't determine what license to use.  We use this flag to tell the licensing check
            // to just use the latest version in the QlmLicenseProductsTable for Cortex::ControlDailies.
            // Note: If a developer wishes to test the licensing for a lesser product than Cortex::ControlDailies, it will be difficult in the case of
            //   _mtiDvdBuilder.exe & _mtiBdBuilder.exe, but that's probably unlikely to really be an issue.
            result.DeveloperModeDvdBluRayHack = false;

            // Check for just "Cortex" (used by developers), then check for support exe's and set primaryExeName based on Main App (e.g. Cortex__ControlDailies, Cortex__Convey or Cortex__Capture)
            if (exeName == "Cortex")
            {
                result.Filename = "Cortex.exe";
                result.ShortFilename = "Cortex";
                result.Directory = baseDir;
            }
            else if (exeName.StartsWith("Cortex__"))
            {
                // Handle Cortex__Capture, Cortex__Convey & Cortex__ControlDailies
                result.Filename = exeName + ".exe";
                result.ShortFilename = exeName;
                result.Directory = baseDir;
            }
            else if (exeName == "CohogRender")
            {
                string primaryExeName;
                string primaryExeShortName;

                if (ExecutableExists(baseDir, new List<string> { "Cortex__ControlDailies.exe", "Cortex__Convey.exe", "Cortex__Capture.exe", "Cortex.exe" }, out primaryExeName, out primaryExeShortName))
                {
                    result.Filename = primaryExeName;
                    result.ShortFilename = primaryExeShortName;
                    result.Directory = baseDir;
                }
            }
            else if (exeName == "_mtiDvdBuilder")
            {
                const string InstalledPathPart = @"\x86\Sonic\Dvd";

                // Set a flag based on whether we are in Installed or Developer mode.
                // If the path to the running EXE contains "\x86\Sonic\Dvd", we must be running an Installed version.
                var installedMode = baseDir.Contains(InstalledPathPart);

                if (installedMode)
                {
                    var baseDirInstalled = baseDir.Replace(InstalledPathPart, string.Empty);

                    string primaryExeName;
                    string primaryExeShortName;

                    if (ExecutableExists(baseDirInstalled, new List<string> { "Cortex__ControlDailies.exe", "Cortex__Convey.exe", "Cortex__Capture.exe", "Cortex.exe" }, out primaryExeName, out primaryExeShortName))
                    {
                        result.Filename = primaryExeName;
                        result.ShortFilename = primaryExeShortName;
                        result.Directory = baseDirInstalled;
                    }
                }
                else
                {
                    result.Filename = "Cortex.exe";
                    result.ShortFilename = TrimExeFromString(result.Filename);
                    result.DeveloperModeDvdBluRayHack = true;
                }
            }
            else if (exeName == "_mtiBdBuilder")
            {
                const string InstalledPathPart = @"\x86\Sonic\BluRay";

                // Set a flag based on whether we are in Installed or Developer mode.
                // If the path to the running EXE contains "\x86\Sonic\Dvd", we must be running an Installed version.
                var installedMode = baseDir.Contains(InstalledPathPart);

                if (installedMode)
                {
                    var baseDirInstalled = baseDir.Replace(InstalledPathPart, string.Empty);

                    string primaryExeName;
                    string primaryExeShortName;

                    if (ExecutableExists(baseDirInstalled, new List<string> { "Cortex__ControlDailies.exe", "Cortex__Convey.exe", "Cortex__Capture.exe", "Cortex.exe" }, out primaryExeName, out primaryExeShortName))
                    {
                        result.Filename = primaryExeName;
                        result.ShortFilename = primaryExeShortName;
                        result.Directory = baseDirInstalled;
                    }
                }
                else
                {
                    result.Filename = "Cortex.exe";
                    result.ShortFilename = TrimExeFromString(result.Filename);
                    result.DeveloperModeDvdBluRayHack = true;
                }
            }
            else if (exeName.StartsWith("_mti"))
            {
                // Set a couple of places to check for the primary exe - they are different for developers and installed versions, so we have to check both.
                var baseDirDeveloper = baseDir.Replace(@"\x86\", @"\x64\");
                var baseDirInstalled = baseDir.Replace(@"\x86\", string.Empty);

                string primaryExeName;
                string primaryExeShortName;

                if (ExecutableExists(baseDirDeveloper, new List<string> { "Cortex__ControlDailies.exe", "Cortex__Convey.exe", "Cortex__Capture.exe", "Cortex.exe" }, out primaryExeName, out primaryExeShortName))
                {
                    result.Filename = primaryExeName;
                    result.ShortFilename = primaryExeShortName;
                    result.Directory = baseDirDeveloper;
                }
                else if (ExecutableExists(baseDirInstalled, new List<string> { "Cortex__ControlDailies.exe", "Cortex__Convey.exe", "Cortex__Capture.exe", "Cortex.exe" }, out primaryExeName, out primaryExeShortName))
                {
                    result.Filename = primaryExeName;
                    result.ShortFilename = primaryExeShortName;
                    result.Directory = baseDirInstalled;
                }
            }
            else if (exeName == "GpuTest")
            {
                result.Filename = "Cortex.exe";
                result.ShortFilename = TrimExeFromString((string)result.Filename);
                result.DeveloperModeDvdBluRayHack = false;
            }

            return result;
        }

        /// <summary>
        /// Checks a list of executable names and returns true if one exists and sets the first matching name.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="executableNames">The executable names.</param>
        /// <param name="executableMatched">The executable matched.</param>
        /// <param name="executableShortName">Short name of the executable.</param>
        /// <returns>True if a match was found, false if not.</returns>
        private static bool ExecutableExists(string directory, IEnumerable<string> executableNames, out string executableMatched, out string executableShortName)
        {
            var result = false;
            executableMatched = string.Empty;
            executableShortName = string.Empty;

            foreach (var file in executableNames)
            {
                if (File.Exists(Path.Combine(directory, file)))
                {
                    executableMatched = file;
                    executableShortName = TrimExeFromString(executableMatched);

                    result = true;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Trims the ".exe" from passed in Executable name (if it has one).
        /// </summary>
        /// <param name="exeName">Name of the exe.</param>
        /// <returns>Trimmed name.</returns>
        private static string TrimExeFromString(string exeName)
        {
            var trimmedExe = string.Empty;

            if (exeName.LastIndexOf(".exe", StringComparison.Ordinal) != -1)
            {
                trimmedExe = exeName.Substring(0, exeName.LastIndexOf(".exe", StringComparison.Ordinal));
            }

            return trimmedExe;
        }

        /// <summary>
        /// Gets the most recent (based on major/minor version/revision number) license filename for the specified product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns>The most recent license filename for the specified product.</returns>
        private static string GetMostRecentLicenseFilename(string product)
        {
            if (!Directory.Exists(LicenseDirectory))
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(product))
            {
                return string.Empty;
            }

            var filenameBase = GetLicenseFilenameBase(product);

            var fileSearchPattern = string.Format("{0}*.qlm", filenameBase);
            var matchingFiles = Directory.GetFiles(LicenseDirectory, fileSearchPattern, SearchOption.TopDirectoryOnly);

            var latestFile = string.Empty;

            foreach (var file in matchingFiles)
            {
                // Regex for Version Number - e.g. 1.0
                const string VersionRegex = @"\d+\.\d+";

                var versionFile = Regex.Match(file, VersionRegex).ToString();
                var versionLatestFile = Regex.Match(latestFile, VersionRegex).ToString();

                if (string.CompareOrdinal(versionFile, versionLatestFile) > 0)
                {
                    latestFile = file;
                }
            }

            return latestFile;
        }

        #endregion
    }
}
