﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WinInetInterop.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>12/6/2012 11:05:17 AM</date>
// <summary>wininet.dll interop class.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;

    /// <summary>
    /// wininet.dll interop class.
    /// </summary>
    internal class WinInetInterop
    {
        //////////////////////////////////////////////////
        // Enums
        //////////////////////////////////////////////////    
        #region Enums

        /// <summary>
        /// Connection State enum for PInvoked InternetGetConnectedState
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", Justification = "Microsoft Defined flags.")]
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1516:ElementsMustBeSeparatedByBlankLine", Justification = "Microsoft Defined flags.")]
        [Flags]
        internal enum InternetConnectionState
        {
            // ReSharper disable InconsistentNaming
            INTERNET_CONNECTION_MODEM = 0x1,
            INTERNET_CONNECTION_LAN = 0x2,
            INTERNET_CONNECTION_PROXY = 0x4,
            INTERNET_RAS_INSTALLED = 0x10,
            INTERNET_CONNECTION_OFFLINE = 0x20,
            INTERNET_CONNECTION_CONFIGURED = 0x40
            // ReSharper restore InconsistentNaming
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the state of Internet connectivity.
        /// </summary>
        /// <param name="lpdwFlags">The LPDW flags.</param>
        /// <param name="reserved">Some Microsoft reserved DW.</param>
        /// <returns>True if connected, false if not.</returns>
        [DllImport("wininet.dll", CharSet = CharSet.Auto)]
        internal static extern bool InternetGetConnectedState(ref InternetConnectionState lpdwFlags, int reserved);

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
