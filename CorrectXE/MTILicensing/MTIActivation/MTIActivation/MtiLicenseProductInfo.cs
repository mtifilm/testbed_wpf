﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiLicenseProductInfo.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\michael.russell</author>
// <date>2013-07-10 17:11:25</date>
// <summary>Information for a Licensed Product.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MTIActivation
{
    using System;

    /// <summary>
    /// Information for a Licensed Product.
    /// </summary>
    internal class MtiLicenseProductInfo
    {
        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MtiLicenseProductInfo"/> class.
        /// </summary>
        /// <param name="licenseVendor">The license vendor.</param>
        /// <param name="name">The product name.</param>
        /// <param name="majorVersion">The major version.</param>
        /// <param name="minorVersion">The minor version.</param>
        /// <param name="revision">The revision.</param>
        internal MtiLicenseProductInfo(MtiLicense.LicenseVendor licenseVendor, string name, int majorVersion, int minorVersion, int revision)
        {
            this.LicenseVendor = licenseVendor;
            this.Name = name;
            this.MajorVersion = majorVersion;
            this.MinorVersion = minorVersion;
            this.Revision = revision;
        }

        /// <summary>
        /// Returns human readable string 
        /// </summary>
        public override string ToString()
        {
            var result = "LicenseVendor: " + this.LicenseVendor.ToString() + Environment.NewLine +
                    "Name: " + this.Name + Environment.NewLine +
                    "Version: " + this.MajorVersion.ToString() + "." +
                    this.MinorVersion.ToString() + "." + this.Revision.ToString();

            return result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MtiLicenseProductInfo"/> class.
        /// </summary>
        /// <param name="name">The product name.</param>
        /// <param name="majorVersion">The major version.</param>
        /// <param name="minorVersion">The minor version.</param>
        /// <param name="revision">The revision.</param>
        internal MtiLicenseProductInfo(string name, int majorVersion, int minorVersion, int revision)
            : this(MtiLicense.LicenseVendor.Qlm, name, majorVersion, minorVersion, revision)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MtiLicenseProductInfo"/> class.
        /// </summary>
        /// <param name="fullProductInfo">The full product info.</param>
        internal MtiLicenseProductInfo(string fullProductInfo)
            : this(MtiLicense.LicenseVendor.Qlm, MtiLicense.GetProductNameFromFullProductName(fullProductInfo), MtiLicense.GetMajorVersionFromCombinedVersion(fullProductInfo), MtiLicense.GetMinorVersionFromCombinedVersion(fullProductInfo), MtiLicense.GetRevisionFromCombinedVersion(fullProductInfo))
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the license vendor.
        /// </summary>
        internal MtiLicense.LicenseVendor LicenseVendor { get; set; }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Gets or sets the major version number.
        /// </summary>
        internal int MajorVersion { get; set; }

        /// <summary>
        /// Gets or sets the minor version number.
        /// </summary>
        internal int MinorVersion { get; set; }

        /// <summary>
        /// Gets or sets the revision number.
        /// </summary>
        internal int Revision { get; set; }

        /// <summary>
        /// Less than or operator - returns true if major is less, minor is less or revision number is less.
        /// </summary>
        /// <param name="a">First operand.</param>
        /// <param name="b">Second operand.</param>
        /// <returns>True if major is less, minor is less or revision number is less.</returns>
        public static bool operator <(MtiLicenseProductInfo a, MtiLicenseProductInfo b)
        {
            if (a.Name != b.Name)
            {
                throw new ArithmeticException("Apples and oranges - can't compare different products.");
            }

            // Major less = true
            if (a.MajorVersion < b.MajorVersion)
            {
                return true;
            }

            // Major greater = false
            if (a.MajorVersion > b.MajorVersion)
            {
                return false;
            }

            // Major same / minor less = true
            if (a.MinorVersion < b.MinorVersion)
            {
                return true;
            }

            // Major same / minor greater = false
            if (a.MinorVersion > b.MinorVersion)
            {
                return false;
            }

            // Major & Minor same, so just compare revisions.
            return a.Revision < b.Revision;
        }

        /// <summary>
        /// Greater than operator - returns true if major is greater, minor is greater or revision number is greater.
        /// </summary>
        /// <param name="a">First operand.</param>
        /// <param name="b">Second operand.</param>
        /// <returns>True if major is greater, the minor is greater or the revision number is greater.</returns>
        public static bool operator >(MtiLicenseProductInfo a, MtiLicenseProductInfo b)
        {
            if (a.Name != b.Name)
            {
                throw new ArithmeticException("Apples and oranges - can't compare different products.");
            }

            // Major greater = true
            if (a.MajorVersion > b.MajorVersion)
            {
                return true;
            }

            // Major less = false
            if (a.MajorVersion < b.MajorVersion)
            {
                return false;
            }

            // Major same / minor greater = true
            if (a.MinorVersion > b.MinorVersion)
            {
                return true;
            }

            // Major same / minor less = false
            if (a.MinorVersion < b.MinorVersion)
            {
                return false;
            }

            // Major & Minor same, so just compare revisions.
            return a.Revision > b.Revision;
        }

        #endregion
    }
}
