// ---------------------------------------------------------------------------

#include <vcl.h>
#include <string>
#pragma hdrstop

#include "CorrectFake.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#include <xmldoc.hpp>
#include <msxmldom.hpp>

#include "MtiLicenseLib.h"
#include "IniFile.h"

using std::string;

TMainForm *MainForm;

// Globals
string m_product = "";
int m_majorVersion = 0;
int m_minorVersion = 0;
int m_revision = 0;

// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
	MTIostringstream ss;
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();
	mtiLicense->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision);

	ComputerNameLabel->Caption = mtiLicense->GetMachineName().c_str();
	MacLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MacAddress.c_str();
	// for (auto macAddress : mtiLicense->GetMacAddressesV4())
	// {
	// MacLabel->Caption = MacLabel->Caption + macAddress.c_str() + "  ";
	// }

	string root = GetPathRoot(GetSystemDir());
	SerialNumberLabel->Caption = mtiLicense->GetHddVolumeSerialNumber(root).c_str();
	// ComputerIdLabel->Caption = ""; // mtiLicense->GetMachineId().c_str();
	ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();
	auto machineIdData = mtiLicense->GetMachineIdData();
	MachineIdDataStringGrid->RowCount = machineIdData.size() + 1;

	MachineIdDataStringGrid->Cells[1][0] = "Mac Address";
	MachineIdDataStringGrid->Cells[2][0] = "Machine Id";
	MachineIdDataStringGrid->ColWidths[0] = 25;
	MachineIdDataStringGrid->ColWidths[1] = 100;
	MachineIdDataStringGrid->Canvas->Font = MachineIdDataStringGrid->Font;
	auto colWidth1 = 75;
	auto colWidth2 = 75;
	auto i = 1;
	for (auto machineIdDatum : machineIdData)
	{
		MachineIdDataStringGrid->Cells[0][i] = IntToStr(i);
		MachineIdDataStringGrid->Cells[1][i] = machineIdDatum.MacAddress.c_str();
		MachineIdDataStringGrid->Cells[2][i] = machineIdDatum.MachineId.c_str();

		auto w1 = MachineIdDataStringGrid->Canvas->TextWidth(MachineIdDataStringGrid->Cells[1][i]);
		if (w1 > colWidth1)
			colWidth1 = w1;
		auto w2 = MachineIdDataStringGrid->Canvas->TextWidth(MachineIdDataStringGrid->Cells[2][i]);
		if (w2 > colWidth2)
			colWidth2 = w2;
		i++;
	}

	MachineIdDataStringGrid->ColWidths[1] = colWidth1 + 16;
	MachineIdDataStringGrid->ColWidths[2] = colWidth2 + 16;
	Width = MachineIdDataStringGrid->ColWidths[0] + MachineIdDataStringGrid->ColWidths[1] +
		 MachineIdDataStringGrid->ColWidths[2] + 30 + 2 * MachineIdDataStringGrid->Left;
	auto w = Width;
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::ActivationButtonClick(TObject *Sender)
{
	UpdateProductAndVersion();

	MtiLicenseLib *mtiLicense = new MtiLicenseLib();
   auto status = mtiLicense->LaunchLicensingWizard();
	delete mtiLicense;

	return;
}


// ---------------------------------------------------------------------------

void __fastcall TMainForm::ValidateButtonClick(TObject *Sender)
{
	UpdateProductAndVersion();

	// Check if the license is valid
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();
	bool isValid = mtiLicense->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision);

	// Set the labels that are the same whether license is valid or not
	LicensingForm->ProductLabel->Caption = m_product.c_str();
  //TODO: ADD THIS	LicensingForm->VersionLabel->Caption = MtiLicenseLib::GetFullVersion(m_majorVersion, m_minorVersion, m_revision).c_str();
	LicensingForm->ComputerIdLabel->Caption = mtiLicense->GetMachineId().c_str();
	LicensingForm->ComputerNameLabel->Caption = mtiLicense->GetMachineName().c_str();
	LicensingForm->FeaturesListBox->Clear();

	// Set the labels with info (if license is valid) or blank (if invalid).
	if (isValid)
	{
		LicensingForm->ValidLabel->Caption = "YES";
		LicensingForm->ActivationKeyLabel->Caption = mtiLicense->ActivationKey.c_str();
		LicensingForm->LicenseKeyLabel->Caption = mtiLicense->LicenseKey.c_str();
		LicensingForm->ExpiresLabel->Caption = mtiLicense->ExpirationDateStringPretty().c_str();
		LicensingForm->CompanyLabel->Caption = mtiLicense->Company.c_str();
		LicensingForm->UserLabel->Caption = mtiLicense->User.c_str();

		for (std::vector<string>::iterator it = mtiLicense->Features.begin(); it != mtiLicense->Features.end(); ++it)
		{
			LicensingForm->FeaturesListBox->Items->Add((*it).c_str());
		}
	}
	else
	{
		LicensingForm->ValidLabel->Caption = "NO";
		LicensingForm->ActivationKeyLabel->Caption = "";
		LicensingForm->LicenseKeyLabel->Caption = "";
		LicensingForm->ExpiresLabel->Caption = "";
		LicensingForm->CompanyLabel->Caption = "";
		LicensingForm->UserLabel->Caption = "";
	}

	MacLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MacAddress.c_str();
	ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();

	LicensingForm->ShowModal();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::LicenseFeatureCheckButtonClick(TObject *Sender)
{
	UpdateProductAndVersion();

	// First, validate the license
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();
	bool isValid = mtiLicense->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision);

	// Now, check if the feature named 'Base' is licensed
	string featureStatus = mtiLicense->IsFeatureLicensed("Base") ? "Ok" : "Not Ok";
	ShowMessage(("Feature 'Base': " + featureStatus).c_str());

	MacLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MacAddress.c_str();
	ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();
}

void __fastcall TMainForm::UpdateProductAndVersion(void)
{
	// Get the product
	AnsiString tmp = ProductEdit->Text.c_str();
	m_product = tmp.c_str();

	m_majorVersion = MajorVersionEdit->Text.ToInt();
	m_minorVersion = MinorVersionEdit->Text.ToInt();
	m_revision = RevisionEdit->Text.ToInt();
}
// ---------------------------------------------------------------------------
