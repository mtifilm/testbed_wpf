object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Convert License '
  ClientHeight = 195
  ClientWidth = 364
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 106
  TextHeight = 14
  object SuccessPanel: TPanel
    Left = 0
    Top = 0
    Width = 364
    Height = 195
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 24
    ExplicitTop = 16
    ExplicitWidth = 332
    ExplicitHeight = 161
    DesignSize = (
      364
      195)
    object CloseLabel: TLabel
      Left = 0
      Top = 79
      Width = 364
      Height = 18
      Alignment = taCenter
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'Close Window To Continue'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SuccessfulLabel: TLabel
      Left = 0
      Top = 32
      Width = 364
      Height = 22
      Alignment = taCenter
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'License Conversion Successful!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -18
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object OkButton: TButton
      Left = 136
      Top = 144
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = OkButtonClick
    end
  end
end
