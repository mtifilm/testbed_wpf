//---------------------------------------------------------------------------

#pragma hdrstop

#include "QlmProductData.h"
#include "MtiLicenseConsts.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

   QlmProductData::QlmProductData()
   {
	   _productId = 15;

		char tmp[MAX_FEATURE_NAME_LENGTH];
		memset(tmp, 0, MAX_FEATURE_NAME_LENGTH);

		// Features - might as well obfuscate these too.
		//   Base, CBTool, MTIClean
		OBFUS_4(tmp, 0x6e, 'B', 'a', 's', 'e');
		_features[{0, 1}] = tmp;
		OBFUS_6(tmp, 0x66, 'C', 'B', 'T', 'o', 'o', 'l');
		_features[{1, 1}] = tmp;
		OBFUS_8(tmp, 0x1d, 'M', 'T', 'I', 'C', 'l', 'e', 'a', 'n');
		_features[{1, 2}] = tmp;

      OBFUS_7(tmp, 0x69, 'D', 'R', 'S', 'N', 'o', 'v', 'a');
      _productName = tmp;
   }

   QlmProductData::~QlmProductData() {}

   std::string QlmProductData::getUnknownString(int featureSet, int featureId)
{
    const int bufsize = 32;
    char featureName[bufsize];

    sprintf_s(featureName, bufsize, "Unknown %d/%d", featureSet, featureId);

    std::string featureNameString = featureName;

    return featureNameString;
}
