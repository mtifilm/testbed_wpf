// ---------------------------------------------------------------------------

#pragma hdrstop

#include "MtiLicenseLib.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

std::string ConvertWCSToMBS(const wchar_t* pstr, long wslen)
{
	int len = ::WideCharToMultiByte(CP_ACP, 0, pstr, wslen, NULL, 0, NULL, NULL);

	std::string dblstr(len, '\0');
	len = ::WideCharToMultiByte(CP_ACP, 0 /* no flags */ , pstr, wslen /* not necessary NULL-terminated */ , &dblstr[0],
		 len, NULL, NULL /* no default char */);

	return dblstr;
}

BSTR ConvertMBSToBSTR(const std::string& str)
{
	int wslen = ::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), NULL, 0);

	BSTR wsdata = ::SysAllocStringLen(NULL, wslen);
	::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), wsdata, wslen);
	return wsdata;
}

std::string ConvertBSTRToMBS(BSTR bstr)
{
	int wslen = ::SysStringLen(bstr);
	return ConvertWCSToMBS((wchar_t*)bstr, wslen);
}

// --------------------------------------------------------------------------------------------------------------------
// Gets the MD5 hash of the passed string.
// Note: the string data passed in is OVERWRITTEN.  This is intentional so the original string is unavailable after
// this call.
// --------------------------------------------------------------------------------------------------------------------
string &GetMd5Hash(string &data)
{
	HCRYPTPROV hCryptProv;
	HCRYPTHASH hHash;
	BYTE bHash[0x7f];
	DWORD dwHashLen = 16; // The MD5 algorithm always returns 16 bytes.
	DWORD cbContent = (DWORD)data.length();

	// Make a byte array of the string since that's what we need to pass to CryptHashData()
	BYTE* pbContent = (BYTE*)malloc(data.length());
	for (unsigned int i = 0; i < data.length(); i++)
	{
		pbContent[i] = data.at(i);
	}

	// Calculate the MD5
	if (CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_MACHINE_KEYSET))
	{
		if (CryptCreateHash(hCryptProv, CALG_MD5, 0, 0, &hHash))
		{
			if (CryptHashData(hHash, pbContent, cbContent, 0))
			{
				if (CryptGetHashParam(hHash, HP_HASHVAL, bHash, &dwHashLen, 0))
				{
					// Make a hex string version of the numeric digest value
					data = "";

					const int bufsize = 3;
					char hexValue[bufsize];

					for (DWORD i = 0; i < dwHashLen; i++)
					{
						sprintf_s(hexValue, bufsize, "%.2x", bHash[i]);
						data += hexValue;
					}
				}
				else
				{
					data = string("Error getting hash param");
				}
			}
			else
			{
				data = string("Error hashing data");
			}
		}
		else
		{
			data = string("Error creating hash");
		}
	}
	else
	{
		data = string("Error acquiring context");
	}

	// Clean up
	if (hHash)
		CryptDestroyHash(hHash);

	if (hCryptProv)
		CryptReleaseContext(hCryptProv, 0);

	if (pbContent != NULL)
		free(pbContent);

	return data;
}

vector<string>MtiLicenseLib::GetMacAddressesV4(IQlmHardware *qlmHardware)
{
	BSTR macAddressB;
	if FAILED(qlmHardware->GetMACAddresses_2(true, &macAddressB))
	{throw std::runtime_error("Could not get mac address");}

	string macAddressLine = ConvertBSTRToMBS(macAddressB) + ";";
	size_t pos = 0;
	vector<string>macAddresses;
	string delimiter = ";";
	while ((pos = macAddressLine.find(delimiter)) != std::string::npos)
	{
		macAddresses.push_back(macAddressLine.substr(0, pos));
		macAddressLine.erase(0, pos + delimiter.length());
	}

	std::sort(macAddresses.begin(), macAddresses.end());
	return macAddresses;
}

vector<string>MtiLicenseLib::GetMacAddressesV4()
{
   return GetMacAddressesV4(_qlmHardware);
}

string MtiLicenseLib::GetFirstMacAddressV4()
{
	vector<string>macAddresses = GetMacAddressesV4();
	string returnAddress = macAddresses.empty() ? "F9B08D297579" : macAddresses[0];
	macAddresses.clear();
	return returnAddress;
}

string MtiLicenseLib::GetMachineName(IQlmHardware *qlmHardware)
{
	BSTR machineNameB;
	if FAILED(qlmHardware->GetMachineName(&machineNameB))
	{
      throw std::runtime_error("Could not get serial number");
   }

	return ConvertBSTRToMBS(machineNameB);
}

string MtiLicenseLib::GetMachineName()
{
	return GetMachineName(_qlmHardware);
}

// This REALLY should return an MachineIdDatum but we want the interface to
// be the same as version 6.
string MtiLicenseLib::GetMachineId(const string &macAddress)
{
	string machineName = GetMachineName();
	string root = GetPathRoot(GetSystemDir());
	string hddVolumeSerialNumber = GetHddVolumeSerialNumber(root);

	string custom = machineName + macAddress + hddVolumeSerialNumber;

	//
	_machineIdDatum.MachineName = machineName;
	_machineIdDatum.HddVolumeSerialNumber = hddVolumeSerialNumber;
	_machineIdDatum.MacAddress = macAddress;

	// For some reason this is inplace.
	GetMd5Hash(custom);
	_machineIdDatum.MachineId = custom;

	return custom;
}

string MtiLicenseLib::GetMachineId(IQlmHardware *qlmHardware, const string &macAddress)
{
	string machineName = GetMachineName(qlmHardware);
	string root = GetPathRoot(GetSystemDir());
	string hddVolumeSerialNumber = GetHddVolumeSerialNumber(root);

	string custom = machineName + macAddress + hddVolumeSerialNumber;

	// For some reason this is inplace.
	GetMd5Hash(custom);
	return custom;
}
int MtiLicenseLib::LaunchLicensingWizard()
{
	CComBSTR pName(wcslen(_qlmWizard.c_str()));
	wcscpy(pName.m_str, _qlmWizard.c_str());
	CComBSTR pArgs(wcslen(_wizardArgs.c_str()));
	wcscpy(pArgs.m_str, _wizardArgs.c_str());
	return _lv->QlmLicenseObject()->LaunchProcess(pName, pArgs, true, true);
}

bool MtiLicenseLib::isLicenseServerAvailable()
{
	BSTR bstrComputerId = ConvertMBSToBSTR(GetMachineId());
	BSTR bstrActivationKey = ConvertMBSToBSTR(ActivationKey);
	BSTR bstrComputerKey = ConvertMBSToBSTR(LicenseKey);

	auto di = getLicense()->GetDashboardLicenseInfo(getLicense()->DefaultWebServiceUrl, bstrActivationKey,
		 bstrComputerKey, bstrComputerId);

	return di->InternetConnection;
}

string MtiLicenseLib::GetMachineId()
{
	// Get Com interface
	BSTR machineNameB;
	if FAILED(_qlmHardware->GetMachineName(&machineNameB))
	{throw std::runtime_error("Could not get machine name");}

	BSTR hddVolumeSerialNumberB;
	if FAILED(_qlmHardware->GetFirstVolumeSerialNumber(&hddVolumeSerialNumberB))
	{throw std::runtime_error("Could not get serial number");}

	string machineName = ConvertBSTRToMBS(machineNameB);
	string macAddress = GetFirstMacAddressV4();
	string hddVolumeSerialNumber = ConvertBSTRToMBS(hddVolumeSerialNumberB);

	string custom = machineName + macAddress + hddVolumeSerialNumber;
	return GetMd5Hash(custom);
}

MtiLicenseLib::MtiLicenseLib(bool launchActivation)
{
	_lv = new TLicenseValidator();
	if (_lv == nullptr)
	{
		throw std::runtime_error("Failed to initialize licensing");
	}

	_qlmHardware = _lv->CreateQlmHardwareObject();
	if (_qlmHardware == nullptr)
	{
		throw std::runtime_error("Could not get hardware information");
	}

	UnicodeString lwFileName = _productData.getQlmLWFileName().c_str();
	_settingsFile = ExtractFilePath(Application->ExeName) + lwFileName;
	_qlmWizard = ExtractFilePath(Application->ExeName) + L"QlmLicenseWizard.exe";

   // We will use the FIRST ethernet if we need to launch
	UnicodeString computerID;

	// Now do an activation
	bool needsActivation = false;
	UnicodeString _error_msg_ = L"";
	bool licenseValid = false;

   // Now try all the computerIDs
  	for (auto macAddress : GetMacAddressesV4())
	{
		needsActivation = false;
		auto localComputerID = ConvertMBSToBSTR(GetMachineId(macAddress));
      licenseValid = _lv->ValidateLicenseAtStartup(localComputerID, _licenseBinding, needsActivation, _error_msg_);
#ifdef _DEBUG
      DBTRACE2(macAddress, licenseValid);
#endif
      if (licenseValid)
      {
         computerID = localComputerID;
         break;
      }
   }

   if (!licenseValid)
   {
      computerID = ConvertMBSToBSTR(GetMachineId());
   }

	_wizardArgs = L" /settings \"" + _settingsFile + "\"" + " /computerID " + computerID;

	if ((needsActivation || (licenseValid == false)) && launchActivation)
	{
		TRACE_0(errout << "After activation try Failed: " << _error_msg_);
		LaunchLicensingWizard();

		licenseValid = _lv->ValidateLicenseAtStartup(computerID, _licenseBinding, needsActivation, _error_msg_);
		if (needsActivation || (licenseValid == false))
		{
			TRACE_0(errout << "After activation try Failed: " << _error_msg_);
			exit(1110);
		}
	}

	// Set these values, should really be getters
	IsBaseValid = IsFeatureLicensed(_productData._features[{0, 0}]);
	IsActivated = licenseValid && (needsActivation == false);
	ActivationKey = ConvertBSTRToMBS(_lv->GetActivationKey().w_str());
	LicenseKey = ConvertBSTRToMBS(_lv->GetComputerKey().w_str());
	Company = GetCompany();
	Features = GetQlmLicensedFeatures();
	ValidateErrorMessage = ConvertBSTRToMBS(_error_msg_.w_str());
}

MtiLicenseLib::~MtiLicenseLib() {delete _lv;}

bool MtiLicenseLib::LicenseValidate(string product, int majorVersion, int minorVersion, int revision)
{
	// This ugly routine has a side effect of finding the first valid license
	for (auto macAddress : GetMacAddressesV4())
	{
		auto activationKey = _lv->GetActivationKey();
		auto computerKey = _lv->GetComputerKey();
		UnicodeString computerId = ConvertMBSToBSTR(GetMachineId(macAddress));
		bool needsActivation = false;
		UnicodeString returnMsg;

		if (_lv->ValidateLicense(activationKey, computerKey, computerId, _licenseBinding, needsActivation,
			 returnMsg) && (needsActivation == false))
		{
			return true;
		}
		else
		{
			// This only returns the last failure, so its not right
			_machineIdDatum.ValidationMessage = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
		}
	}

	return false;
}

bool MtiLicenseLib::LicenseValidate()
{
	// This ugly routine has a side effect of finding the first valid license
	for (auto macAddress : GetMacAddressesV4())
	{
		auto activationKey = _lv->GetActivationKey();
		auto computerKey = _lv->GetComputerKey();
		UnicodeString computerId = ConvertMBSToBSTR(GetMachineId(macAddress));
//		DBTRACE2(GetMachineId(macAddress), macAddress);
		bool needsActivation = false;
		UnicodeString returnMsg;

		if (_lv->ValidateLicense(activationKey, computerKey, computerId, _licenseBinding, needsActivation,
			 returnMsg) && (needsActivation == false))
		{
			return true;
		}
		else
		{
			// This only returns the last failure, so its not right
			_machineIdDatum.ValidationMessage = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
			DBTRACE(_machineIdDatum.ValidationMessage);
		}
	}

	return false;
}

// --------------------------------------------------------------------------------------------------------------------
// Gets a the volume serial number (in hex) of the specified drive.  Default is "C:" (see the .h file for the default).
// --------------------------------------------------------------------------------------------------------------------
string MtiLicenseLib::GetHddVolumeSerialNumber(string volume)
{
	TCHAR volumeName[MAX_PATH + 1] =
	{0};
	TCHAR fileSystemName[MAX_PATH + 1] =
	{0};
	DWORD serialNumber = 0;
	DWORD maxComponentLen = 0;
	DWORD fileSystemFlags = 0;

	if (volume.back() != '\\')
	{
		volume += "\\";
	}

	if (GetVolumeInformation(volume.c_str(), volumeName, ARRAYSIZE(volumeName), &serialNumber, &maxComponentLen,
		 &fileSystemFlags, fileSystemName, ARRAYSIZE(fileSystemName)))
	{
		const int bufsize = 9;
		char serialNumberStr[bufsize];
		sprintf_s(serialNumberStr, bufsize, "%.8X", serialNumber);

		return string(serialNumberStr);
	}

	return "";
}

vector<MachineIdDatum>MtiLicenseLib::GetMachineIdData()
{
	struct MachineIdDatum machineIdDatum;
	machineIdDatum.MachineName = GetMachineName();
	string root = GetPathRoot(GetSystemDir());
	machineIdDatum.HddVolumeSerialNumber = GetHddVolumeSerialNumber(root);

	vector<MachineIdDatum>machineIdData;
	for (auto macAddress : GetMacAddressesV4())
	{
		machineIdDatum.MacAddress = macAddress;
		string custom = machineIdDatum.MachineName + machineIdDatum.MacAddress + machineIdDatum.HddVolumeSerialNumber;
		machineIdDatum.MachineId = GetMd5Hash(custom);
		machineIdData.push_back(machineIdDatum);
	}

	return machineIdData;
}

bool MtiLicenseLib::IsFeatureLicensed(const string &feature)
{
	for (auto&p : _productData._features)
	{
		auto key = p.first;
		auto name = p.second;
		if (name == feature)
		{
			VARIANT_BOOL value = false;
			auto fs = key.first;
			auto fi = key.second;
			_lv->QlmLicenseObject()->IsFeatureEnabledEx(fs, fi, &value);
			return value;
		}
	}

	return false;
}

vector<string>MtiLicenseLib::GetQlmLicensedFeatures()
{
	vector<string>featureList = vector<string>();
	auto qlmLicense = _lv->QlmLicenseObject();
	auto features = _productData._features;

	for (int featureSet = 0; featureSet < 4; featureSet++)
	{
		for (int j = 0; j < 8; j++)
		{
			int featureId = 1 << j;

			// Create a sanity check to make sure the features are
			// in the Product data
			if (qlmLicense->IsFeatureEnabledEx(featureSet, featureId))
			{
				VARIANT_BOOL value = false;
				qlmLicense->IsFeatureEnabledEx(featureSet, featureId, &value);

				if (value)
				{
					// see if in product data
					if (features.find({featureSet, featureId}) == features.end())
					{
						featureList.push_back(_productData.getUnknownString(featureSet, featureId));
					}
					else
					{
						featureList.push_back(features[{featureSet, featureId}]);
					}
				}
			}
		}
	}

	return featureList;
}

string MtiLicenseLib::GetProductName()
{
	BSTR productNameB;
	auto result = _lv->QlmLicenseObject()->get_ProductName(&productNameB);
	return ConvertBSTRToMBS(productNameB);
}

string MtiLicenseLib::GetCompany()
{
	BSTR value;
	auto result = _lv->CreateLicenseInfoObject()->get_Company(&value);
	return ConvertBSTRToMBS(value);
}

int MtiLicenseLib::GetMajorVersion()
{
	long majorVersion;
	auto result = _lv->QlmLicenseObject()->get_MajorVersion(&majorVersion);
	return int(majorVersion);
}

int MtiLicenseLib::GetMinorVersion()
{
	long minorVersion;
	_lv->QlmLicenseObject()->get_MinorVersion(&minorVersion);
	return int(minorVersion);
}

bool MtiLicenseLib::Expires() {return _lv->IsEvaluation();}

bool MtiLicenseLib::IsExpired() {return _lv->EvaluationExpired();}

// --------------------------------------------------------------------------------------------------------------------
// Returns the Expiration Date string in the format 10/27/2012
// --------------------------------------------------------------------------------------------------------------------
string MtiLicenseLib::ExpirationDateString()
{
	const int bufsize = 32;
	char expirationDateChar[bufsize];
	SYSTEMTIME expirationDateSystemTime;
	DATE expiryDate;

	// See if license expires
	auto license = _lv->CreateLicenseInfoObject();
	license->get_ExpiryDate(&expiryDate);

	if (_lv->IsEvaluation())
	{
		MTIostringstream os;
		os << "Trial: " << _lv->EvaluationRemainingDays() << " days remaining";
		return os.str();
	}

	if (expiryDate == 0)
	{
		if (_lv->EvaluationExpired())
		{
			return "License has Expired";
		}

		return "Never";
	}

	// Convert DATE (a double) to SYSTEMTIME
	VariantTimeToSystemTime(expiryDate, &expirationDateSystemTime);

	sprintf_s(expirationDateChar, bufsize, "%02d/%02d/%04d", expirationDateSystemTime.wMonth,
		 expirationDateSystemTime.wDay, expirationDateSystemTime.wYear);
	string expirationDateStr = expirationDateChar;

	return expirationDateStr;
}

string MtiLicenseLib::ExpirationDateStringPretty() {return ExpirationDateString();}

string MtiLicenseLib::GetFullVersion(int majorVersion, int minorVersion, int revision)
{
	std::ostringstream os;
	os << GetMajorVersion() << "." << GetMinorVersion();

	return os.str();
}

std::tuple<bool, bool, MachineIdDatum>MtiLicenseLib::IsActiveLicensePreCheck(int minorVersion)
{
	bool activated = false;
	auto lv2 = new TLicenseValidator(minorVersion);
	if (lv2 == nullptr)
	{
		throw std::runtime_error("Failed to initialize licensing");
	}

	auto qlmHardware = lv2->CreateQlmHardwareObject();
	if (qlmHardware == nullptr)
	{
		throw std::runtime_error("Could not get hardware information");
	}

	UnicodeString returnMsg;
	MachineIdDatum machineIdDatum;
	string computerIDString;
	for (auto macAddress : GetMacAddressesV4(qlmHardware))
	{
		bool needsActivation = false;
		UnicodeString computerID = ConvertMBSToBSTR(GetMachineId(qlmHardware, macAddress));

		bool valid = lv2->ValidateLicenseAtStartup(computerID, needsActivation, returnMsg);
		if (valid && (needsActivation == false))
		{
			activated = true;
			machineIdDatum.MachineName = GetMachineName(qlmHardware);
			machineIdDatum.HddVolumeSerialNumber = GetHddVolumeSerialNumber();
			machineIdDatum.MacAddress = macAddress;
			machineIdDatum.MachineId = GetMachineId(qlmHardware, macAddress);
			machineIdDatum.ValidationMessage = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
      	auto activationKey = lv2->GetActivationKey();
			machineIdDatum.ActivationKey = ConvertWCSToMBS(activationKey.w_str(), activationKey.Length());
			break;
		}
		else
		{
#ifdef _DEBUG
			auto errMsg = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
			DBTRACE(errMsg);
#endif
		}
	}

	delete lv2;

	if (activated)
	{
		return std::tuple<bool, bool, MachineIdDatum>(true, false, machineIdDatum);
	}

	// License is not valid, back up and see if a key is correct.
	auto lv1 = new TLicenseValidator(minorVersion - 1);
	if (lv1 == nullptr)
	{
		throw std::runtime_error("Failed to initialize licensing");
	}

	qlmHardware = lv1->CreateQlmHardwareObject();
	if (qlmHardware == nullptr)
	{
		throw std::runtime_error("Could not get hardware information");
	}

	for (auto macAddress : GetMacAddressesV4(qlmHardware))
	{
		bool needsActivation = false;
		UnicodeString computerID = ConvertMBSToBSTR(GetMachineId(qlmHardware, macAddress));
		bool valid = lv1->ValidateLicenseAtStartup(computerID, needsActivation, returnMsg);

		// auto computerKey = lv1->GetComputerKey();

		if (valid && (needsActivation == false))
		{
			machineIdDatum.MachineName = GetMachineName(qlmHardware);
			machineIdDatum.HddVolumeSerialNumber = GetHddVolumeSerialNumber();
			machineIdDatum.MacAddress = macAddress;
			machineIdDatum.MachineId = GetMachineId(qlmHardware, macAddress);
			machineIdDatum.ValidationMessage = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
     		auto activationKey = lv1->GetActivationKey();
			machineIdDatum.ActivationKey = ConvertWCSToMBS(activationKey.w_str(), activationKey.Length());
			activated = true;
			break;
		}
		else
		{
			// For debugging
#ifdef _DEBUG
			auto errMsg = ConvertWCSToMBS(returnMsg.w_str(), returnMsg.Length());
			DBTRACE(errMsg);
#endif
		}
	}

	delete lv1;
	return std::tuple<bool, bool, MachineIdDatum>(false, activated, machineIdDatum);
}

int MtiLicenseLib::LaunchLicensingWizard(string computerID, string activationKey)
{
	UnicodeString lwFileName = "DRSNOVA.lw.xml";
	auto settingsFile = ExtractFilePath(Application->ExeName) + lwFileName;

   UnicodeString wizardArgs;
   if (activationKey.empty())
   {
     wizardArgs = L" /settings \"" + settingsFile + "\"" + " /computerID " + ConvertMBSToBSTR(computerID);
   }
   else
   {
     wizardArgs = L" /settings \"" + settingsFile + "\"" +
                   " /computerID " + ConvertMBSToBSTR(computerID) +
                   " /activationKey " + ConvertMBSToBSTR(activationKey);
   }

	auto qlmWizard = ExtractFilePath(Application->ExeName) + L"QlmLicenseWizard.exe";
	CComBSTR pName(wcslen(qlmWizard.c_str()));
	wcscpy(pName.m_str, qlmWizard.c_str());
	CComBSTR pArgs(wcslen(wizardArgs.c_str()));
	wcscpy(pArgs.m_str, wizardArgs.c_str());

   auto lv = new TLicenseValidator(2);
	bool result = lv->QlmLicenseObject()->LaunchProcess(pName, pArgs, true, true);
   DBTRACE(result);
   delete lv;

   return true;
}
