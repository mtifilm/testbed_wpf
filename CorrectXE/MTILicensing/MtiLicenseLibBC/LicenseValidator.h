//---------------------------------------------------------------------------

#ifndef LicenseValidatorH
#define LicenseValidatorH
#include "atlbase.h"
#include "QlmLicenseLib_TLB.h"
//---------------------------------------------------------------------------
class TLicenseValidator
{
private:
	IQlmLicense *license;
	CComBSTR activationKey;
	CComBSTR computerKey;

	bool isEvaluation;
	bool evaluationExpired;
	int evaluationRemainingDays;
	bool wrongProductVersion;

	std::string fproductPropertiesFileName;
	std::string fdefaultTrialKey;

	void SetPath(UnicodeString path);



	/// <remarks>Call ValidateLicenseAtStartup when your application is launched.
	/// If this function returns false, exit your application.
	/// </remarks>
	///
	/// <summary>
	/// Validates the license when the application starts up.
	/// The first time a license key is validated successfully,
	/// it is stored in a hidden file on the system.
	/// When the application is restarted, this code will load the license
	/// key from the hidden file and attempt to validate it again.
	/// If it validates succesfully, the function returns true.
	/// If the license key is invalid, expired, etc, the function returns false.
	/// </summary>
	/// <param name="computerID">Unique Computer identifier</param>
	/// <param name="returnMsg">Error message returned, in case of an error</param>
	/// <returns>true if the license is OK.</returns>

public:

	TLicenseValidator(int minorVersion = 2);
	TLicenseValidator(UnicodeString settingsFile);

	bool ValidateLicenseAtStartup(UnicodeString computerID, bool &needsActivation, UnicodeString &returnMsg);
	bool ValidateLicenseAtStartup(ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg);
	bool ValidateLicenseAtStartup(UnicodeString computerID, ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg);

	bool ReactivateKey(UnicodeString computerID);

	bool ValidateLicense(UnicodeString activationKey, UnicodeString computerKey, UnicodeString &computerID, ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg);

	void DeleteKeys();

	UnicodeString GetActivationKey();
	UnicodeString GetComputerKey();
	bool IsEvaluation();
	bool EvaluationExpired();
	int EvaluationRemainingDays();
	IQlmLicense* QlmLicenseObject();
	UnicodeString errorMessage;
	AnsiString qlmLicenseLibPath;

	// Methods to retrieve QLM objects without the need to register the QLMLicenseLib.dll
	Variant CreateQlmObject(AnsiString dllPath, AnsiString dllClass);
	IQlmLicense* CreateQlmLicenseObject();
	IQlmHardware* CreateQlmHardwareObject();
	ILicenseInfo* CreateLicenseInfoObject();
	IQlmProductProperties* CreateProductPropertiesObject();
	IQlmProductProperty* CreateProductPropertyObject();
	IQlmAnalytics* CreateAnalyticsObject();


};
#endif