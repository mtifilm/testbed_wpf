//---------------------------------------------------------------------------
#pragma hdrstop

#include <System.SysUtils.hpp>
#include "atlbase.h"
#include <mscoree.h>
#include "mscorlib_TLB.h"
#include "QlmLicenseLib_TLB.h"

#ifdef _WIN64
#pragma comment(lib, "mscoree.a")
#else
#pragma comment(lib, "mscoree.lib")
#endif

#include "LicenseValidator.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

TLicenseValidator::TLicenseValidator(int minorVersion) {
	this->errorMessage = _T("");
	this->qlmLicenseLibPath = AnsiString("QlmLicenseLib");
	license = CreateQlmLicenseObject();
	isEvaluation = false;
	evaluationExpired = false;
	evaluationRemainingDays = -1;

	// Always obfuscate your code. In particular, you should always obfuscate all arguments
	// of DefineProduct and the Public Key (i.e. encrypt all the string arguments).
	bool d = license->DefineProduct (15, CComBSTR("DRSNOVA"), 4, minorVersion, CComBSTR("b3atl3^Rta9*"), CComBSTR("{55BC2636-3FB5-422B-91C0-0E5C7778158C}"));
	license->LicenseEngineLibrary = ELicenseEngineLibrary::ELicenseEngineLibrary_DotNet;
	license->publicKey = CComBSTR("QSErlcE0vbFysQ==");
	license->CommunicationEncryptionKey = CComBSTR("{946B7514-9B10-4EAE-8E9D-6707B2669084}");
	license->DefaultWebServiceUrl = CComBSTR("https://quicklicensemanager.com/mtifilm/qlm/qlmservice.asmx");
	license->RsaPublicKey = CComBSTR("<RSAKeyValue><Modulus>s2VTZn31WWmQtjP51tL74M2q86GX9s1+A0k9+2nTLhYYv34U3M/Uh4MMbhRDii92mOxSLani97pF9YIDpHEHAYjqhcxuYk1EbBX8vFpTijz7H8wywRVdXyrmEGnUCd7BC396gRj4urPpgFnuWSivpYeboTjvWOjOIyQpsBWKqXE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
	license->StoreKeysLocation = EStoreKeysTo::EStoreKeysTo_ERegistry;
	license->StoreKeysOptions = EStoreKeysOptions::EStoreKeysOptions_EStoreKeysPerUserAndMachine;
	license->ValidateOnServer = false;
	license->PublishAnalytics = false;
	license->EvaluationPerUser = true;
	license->EnableMultibyte = true;
	license->ExpiryDateRoundHoursUp = true;
	license->EnableSoapExtension = true;
	license->EnableClientLanguageDetection = true;
	license->LimitTerminalServerInstances = false;
}

TLicenseValidator::TLicenseValidator(UnicodeString settingsFile)
{
	CComBSTR errorMsg("");
	this->qlmLicenseLibPath = AnsiString("QlmLicenseLib");

	license = CreateQlmLicenseObject();
	license->LoadSettings(CComBSTR(settingsFile.c_str()));

	license->RsaPublicKey =
		 CComBSTR(
		 "<RSAKeyValue><Modulus>s2VTZn31WWmQtjP51tL74M2q86GX9s1+A0k9+2nTLhYYv34U3M/Uh4MMbhRDii92mOxSLani97pF9YIDpHEHAYjqhcxuYk1EbBX8vFpTijz7H8wywRVdXyrmEGnUCd7BC396gRj4urPpgFnuWSivpYeboTjvWOjOIyQpsBWKqXE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");

	if ((license->ValidateSettingsFile(settingsFile.c_str(), &errorMsg)) == false)
		throw Exception(errorMsg.m_str);
}

bool isTrue(int const &nVal1, int const& nVal2) {
	bool result;
	result = ((nVal1 & nVal2) == nVal1) || ((nVal1 & nVal2) == nVal2);
	return result;
}


bool TLicenseValidator::ValidateLicenseAtStartup(UnicodeString computerID, bool &needsActivation, UnicodeString &returnMsg) {
	return ValidateLicenseAtStartup(computerID, ELicenseBinding::ELicenseBinding_UserDefined, needsActivation, returnMsg);
}

bool TLicenseValidator::ValidateLicenseAtStartup(ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg) {
	return ValidateLicenseAtStartup(NULL, licenseBinding, needsActivation, returnMsg);
}

bool TLicenseValidator::ValidateLicenseAtStartup(UnicodeString computerID, ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg) {
	bool result;
	returnMsg = L"";
	EServerErrorCode errorCode;
	CComBSTR serverMessage;
	bool serverUpdateRequiresReactivation = false;

	needsActivation = false;

	CComBSTR storedActivationKey(L"");
	CComBSTR storedComputerKey(L"");

	this->license->ReadKeys(&storedActivationKey, &storedComputerKey);

	if (storedActivationKey.Length() > 0) {
		activationKey = storedActivationKey;
	}

	if (storedComputerKey.Length() > 0) {
		computerKey = storedComputerKey;
	}

	result = ValidateLicense(activationKey.m_str, computerKey.m_str, computerID, licenseBinding, needsActivation, returnMsg);

	if ((result == true) && license->ValidateOnServer)
	{
		ILicenseInfo *li = CreateLicenseInfoObject();
		CComBSTR bstrServerMessage;
		if ((license->ValidateLicenseOnServerEx2(CComBSTR(license->DefaultWebServiceUrl), CComBSTR(activationKey), CComBSTR(computerKey),
			CComBSTR(computerID.c_str()), CComBSTR(""), false, &li, &errorCode, &bstrServerMessage) == false))
		{
			returnMsg = bstrServerMessage.m_str;
			result = false;
		}

		if (errorCode == EServerErrorCode::EServerErrorCode_NoError)
		{
			serverUpdateRequiresReactivation = (li->NewExpiryDate != NULL) || (wcslen(li->NewFeatures) > 0) || (li->NewFloatingSeats != -1);
		}
	}

	if ((wrongProductVersion || evaluationExpired || serverUpdateRequiresReactivation) && license->ValidateOnServer)
	{
		result = this->ReactivateKey(computerID);
	}

	return result;
}

/// <remarks>Call this function in the dialog where the user enters the license key to validate the license.</remarks>
/// <summary>
/// Validates a license key. If you provide a computer key, the computer key is validated.
/// Otherwise, the activation key is validated.
/// If you are using machine bound keys (UserDefined), you can provide the computer identifier,
/// otherwise set the computerID to an empty string.
/// </summary>
/// <param name="activationKey">Activation Key</param>
/// <param name="computerKey">Computer Key</param>
/// <param name="computerID">Unique Computer identifier</param>
/// <returns>true if the license is OK.</returns>
bool TLicenseValidator::ReactivateKey(UnicodeString computerID) {
	bool result;
	bool ret;
	UnicodeString newComputerKey;
	int nStatus;
	DATE serverDate;
	CComBSTR response;
	IQlmHardware *qlmHardware;
	ILicenseInfo *licenseInfo;
	CComBSTR activationMessage;
	bool needsActivation;
	UnicodeString returnMsg;

	result = false;

	if (license->Ping(CComBSTR(""), &response, &serverDate) == false)
	{
		// we cannot connect to the server so we cannot do any validation with the server
		return false;
	}

	response = L"";
	license->ReactivateLicense(CComBSTR(license->DefaultWebServiceUrl), CComBSTR(activationKey), CComBSTR(computerID.c_str()), &response);

	licenseInfo = CreateLicenseInfoObject();

	if ((license->ParseResults(response, &licenseInfo, &activationMessage) == true))
	{
		newComputerKey = licenseInfo->ComputerKey;
		needsActivation = false;

		result = ValidateLicense(activationKey.m_str, newComputerKey, computerID, ELicenseBinding::ELicenseBinding_UserDefined,
			needsActivation, returnMsg);

		if (result == true)
		{
			license->StoreKeys(activationKey, CComBSTR(newComputerKey.c_str()));
		}
	}

	return result;
}


/// <remarks>Call this function in the dialog where the user enters the license key to validate the license.</remarks>
/// <summary>
/// Validates a license key. If you provide a computer key, the computer key is validated.
/// Otherwise, the activation key is validated.
/// If you are using machine bound keys (UserDefined), you can provide the computer identifier,
/// otherwise set the computerID to an empty string.
/// </summary>
/// <param name="activationKey">Activation Key</param>
/// <param name="computerKey">Computer Key</param>
/// <param name="computerID">Unique Computer identifier</param>
/// <returns>true if the license is OK.</returns>
bool TLicenseValidator::ValidateLicense(UnicodeString activationKey, UnicodeString computerKey, UnicodeString &computerID, ELicenseBinding licenseBinding, bool &needsActivation, UnicodeString &returnMsg) {
	bool result;
	bool ret;
	UnicodeString licenseKey;
	int nStatus;

	ret = false;
	needsActivation = false;

	isEvaluation = false;
	evaluationExpired = false;
	evaluationRemainingDays = -1;
	wrongProductVersion = false;

	licenseKey = computerKey;

	if (licenseKey == "")
	{
		licenseKey = activationKey;

		if (licenseKey == "")
		{
			result = false;
			returnMsg = "No license key was found.";
			return result;
		}
	}

	if (licenseBinding == ELicenseBinding::ELicenseBinding_UserDefined)
	{
		returnMsg = license->ValidateLicenseEx(CComBSTR(licenseKey.c_str()), CComBSTR(computerID.c_str()));
	}
	else
	{
		returnMsg = license->ValidateLicenseEx3(CComBSTR(licenseKey.c_str()), licenseBinding, false, false);
		computerID = license->GetComputerID(licenseBinding);
	}

	nStatus = (int)license->GetStatus();

	if (isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyInvalid) ||
		isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyProductInvalid) ||
		isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyMachineInvalid) ||
		isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyExceededAllowedInstances) ||
		isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyTampered))                // the key is invalid
	{
		ret = false;
	}
	else if (isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyVersionInvalid))
	{
		wrongProductVersion = true;
		ret = false;
	}
	else if (isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyDemo))
	{
		isEvaluation = true;
		if (isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyExpired))
		{
			// the key has expired
			ret = false;
			evaluationExpired = true;
		}
		else
		{
			// the demo key is still valid
			ret = true;
			evaluationRemainingDays = license->DaysLeft;
		}
	}
	else if (isTrue(nStatus, (int)ELicenseStatus::ELicenseStatus_EKeyPermanent))   // the key is OK
	{
		ret = true;
	}

	if (ret)
	{
		if (license->LicenseType == ELicenseType::ELicenseType_Activation)
		{
			needsActivation = true;
			ret = false;
		}
	}

	result = ret;

	return result;
}

/// <summary>
/// Deletes the license keys stored on the computer.
/// </summary>
void TLicenseValidator::DeleteKeys()
{
	license->DeleteKeys();
}

/// <summary>
/// Returns the registered activation key
/// </summary>
UnicodeString TLicenseValidator::GetActivationKey()
{
	return activationKey.m_str;
}

/// <summary>
/// Returns the registered computer key
/// </summary>
UnicodeString TLicenseValidator::GetComputerKey()
{
	return computerKey.m_str;
}

bool TLicenseValidator::IsEvaluation()
{
	return isEvaluation;
}

bool TLicenseValidator::EvaluationExpired()
{
	return evaluationExpired;
}

int TLicenseValidator::EvaluationRemainingDays()
{
	return evaluationRemainingDays;
}

/// <summary>
/// Returns the underlying license object
/// </summary>
IQlmLicense* TLicenseValidator::QlmLicenseObject()
{
	return license;
}


void TLicenseValidator::SetPath(UnicodeString path)
{
	std::string ShortPath;

	if (path.SubString(1, 2) == L"\\\\")
	{
		UnicodeString ShortPath = ExtractShortPathName(path);
		SetDllDirectoryW(ShortPath.c_str());
	}
	else
	{
		SetDllDirectoryW(path.c_str());
	}
}

Variant TLicenseValidator::CreateQlmObject(AnsiString dllPath, AnsiString dllClass)
{
	ICorRuntimeHost *clr = NULL;
	SetPath(ExtractFilePath(ParamStr(0)));

	HRESULT hr = CorBindToRuntime(L"v4.0.30319", NULL, CLSID_CorRuntimeHost, IID_ICorRuntimeHost, (void**)&clr);
	if (FAILED(hr)) return 0;

	clr->Start();
	IUnknown* appDomUnk = NULL;
	_AppDomain* domain = NULL;
	_ObjectHandle* handle = NULL;

	clr->GetDefaultDomain((IUnknown **)&appDomUnk);
	appDomUnk->QueryInterface(IID__AppDomain, (void **)&domain);

	hr = domain->CreateInstance((BSTR)WideString(dllPath), (BSTR)WideString(dllClass), &handle);

	Variant vtObject;
	if (SUCCEEDED(hr))
	{
		handle->Unwrap(vtObject);
	}

	clr->Stop();
	return vtObject;
}

IQlmLicense* TLicenseValidator::CreateQlmLicenseObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.QlmLicense"));

	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	IQlmLicense *qlmLicense = NULL;
	dispatch->QueryInterface(IID_IQlmLicense, (void **)&qlmLicense);

	return qlmLicense;
}

IQlmHardware* TLicenseValidator::CreateQlmHardwareObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.QlmHardware"));
	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	IQlmHardware *qlmHardware = NULL;
	dispatch->QueryInterface(IID_IQlmHardware, (void **)&qlmHardware);
	return qlmHardware;
}

ILicenseInfo* TLicenseValidator::CreateLicenseInfoObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.LicenseInfo"));
	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	ILicenseInfo *licenseInfo = NULL;
	dispatch->QueryInterface(IID_ILicenseInfo, (void **)&licenseInfo);
	return licenseInfo;
}

IQlmProductProperties* TLicenseValidator::CreateProductPropertiesObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.QlmProductProperties"));
	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	IQlmProductProperties *pps = NULL;
	dispatch->QueryInterface(IID_IQlmProductProperties, (void **)&pps);
	return pps;
}

IQlmProductProperty* TLicenseValidator::CreateProductPropertyObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.QlmProductProperties"));
	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	IQlmProductProperty *pp = NULL;
	dispatch->QueryInterface(IID_IQlmProductProperty, (void **)&pp);
	return pp;
}

IQlmAnalytics* TLicenseValidator::CreateAnalyticsObject()
{
	Variant o;
	o = CreateQlmObject(qlmLicenseLibPath, AnsiString("QlmLicenseLib.QlmAnalytics"));
	IDispatch *dispatch;
	dispatch = static_cast<IDispatch*>(o.pdispVal);
	IQlmAnalytics *qlmAnalytics = NULL;
	dispatch->QueryInterface(IID_IQlmProductProperty, (void **)&qlmAnalytics);
	if (qlmAnalytics != NULL) qlmAnalytics->_set_QlmLicenseObject(this->license);
	return qlmAnalytics;
}
