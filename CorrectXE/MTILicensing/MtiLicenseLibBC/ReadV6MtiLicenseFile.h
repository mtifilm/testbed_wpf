//---------------------------------------------------------------------------

#ifndef ReadV6MtiLicenseFileH
#define ReadV6MtiLicenseFileH
//---------------------------------------------------------------------------

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>  // for std::sort
#include <map>
#include "IniFile.h"

class ReadV6MtiLicenseFile
{
public:
	ReadV6MtiLicenseFile(const string &product, int majorVersion, int minorVersion);

	bool isFileValid() const {return _isFileValid;}

	string getLicenseFileName() {return getLicenseFileName(_productName, _majorVersion, _minorVersion);}
	string getProductNameRead() const {return _productNameRead;}
	string getComputerKeyLocal() const {return _computerKeyLocal;}
	string getComputerIdLocal() const {return _computerIdLocal;}
	string getActivationKey() const {return _activationKey;}
	string getCompany() const {return _company;}
	string getUser() const {return _user;}

   bool removeOldLicense();

private:
	string getLicenseFileName(const string &path, const string &product, int majorVersion, int minorVersion);
   string getProductCombinedName(const string &product, int majorVersion, int minorVersion);
	string getLicenseFileName(const string &product, int majorVersion, int minorVersion);

	bool doesLicenseFileExist(const string &product, int majorVersion, int minorVersion);

	bool readFile(const string &licenseFile);

  string _productNameRead;
  string _computerKeyLocal;
  string _activationKey;
  string _computerIdLocal;
  string _company;
  string _user;

  string _productName;
  int _majorVersion = 0;
  int _minorVersion = 0;
  bool _isFileValid = false;
};

#endif
