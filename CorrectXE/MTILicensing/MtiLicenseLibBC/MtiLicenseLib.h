// ---------------------------------------------------------------------------

#ifndef MtiLicenseLibH
#define MtiLicenseLibH
// ---------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include <vcl.h>

// #include <winsock2.h>
// #include <iphlpapi.h>

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>  // for std::sort
#include <map>
#include "IniFile.h"

#ifdef _WIN64
#pragma comment(lib, "MtiLicenseLib.a")
#pragma comment(lib, "core_code.a")
#else
#pragma comment(lib, "MtiLicenseLib.lib")
#pragma comment(lib, "core_code.lib")
#endif

// #include <Wincrypt.h>
// #pragma comment (lib, "Crypt32")
// #define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

#include <Winapi.ActiveX.hpp>
#include "LicenseValidator.h"
#include "QlmProductData.h"
#include "ReadV6MtiLicenseFile.h"

struct MachineIdDatum
{
	string MachineName;
	string HddVolumeSerialNumber;
	string MacAddress;
	string MachineId;
   string ActivationKey;
	string ValidationMessage;
};

// NOTE:  This is way too complicated but it arose out of FlexLM and role our own licensing wizard
// I think the complexity and stupid logic makes it a little harder to break.
class MtiLicenseLib
{
public:
	// Constructor / Destructor
	MtiLicenseLib(bool launchActivation = false);
	virtual ~MtiLicenseLib();

   // This is just used before MtiLicenseLib(true)
   // first = is current version license valid
   // second = is previous version valid
   // WARING!! The license validator must also be hacked to pass minor version
   static std::tuple<bool, bool, MachineIdDatum> IsActiveLicensePreCheck(int minorVersion);

	// Methods
	static string GetMachineName(IQlmHardware *qlmHardware);
   string GetMachineName();
	static vector<string>GetMacAddressesV4(IQlmHardware *qlmHardware);
   vector<string>GetMacAddressesV4();
	string GetFirstMacAddressV4();
	static string GetHddVolumeSerialNumber(string volume = string("C:"));

	// string &GetMd5Hash(string &data);
 	static string GetMachineId(IQlmHardware *qlmHardware, const string &macAddress);
	string GetMachineId();
   string GetProductName() const {return _productData.getProductName(); }
	// string GetLicenseFilename(const string &product, int majorVersion, int minorVersion);
	// string GetLicenseKeyFromFile(string product, int majorVersion, int minorVersion, int revision);
	// static string GetProductCombinedName(const string &product, int majorVersion, int minorVersion);
	// static string GetProductCombinedFullName(const string &product, int majorVersion, int minorVersion, int revision);
	   string GetFullVersion(int majorVersion, int minorVersion, int revision);
	// string GetProductNameFromCombinedFullName(string combinedFullName);
	// string GetVersion(string combinedFullName);
	// int GetMajorVersion(string combinedFullName);
	// int GetMinorVersion(string combinedFullName);
	// int GetRevision(string combinedFullName);
	// int CompareVersions(string version1, string version2);
	//
	// vector<string> GetAllFeatureNames(string product, int majorVersion, int minorVersion);
	bool LicenseValidate(string product, int majorVersion, int minorVersion,
		int revision);
  	bool LicenseValidate();

	// bool DoesLicenseFileExist(const string &product, int majorVersion, int minorVersion);
	// int GetLatestMajorVersion(const string &product);
	// int GetLatestMinorVersion(const string &product);

	bool IsFeatureLicensed(const string &feature);
	bool Expires();
  	bool IsExpired();

   string ExpirationDateString();
	string ExpirationDateStringPretty(void);;
	bool IsDemo() const { return _lv->IsEvaluation(); }

   // Base feature ALWAYS exists but is checked periodically
	bool IsBaseValid = false;

   // In theory a license can be activated but no base
   bool IsActivated = false;

   // This is no longer used but is a placeholder for
   // old code
   string ValidateErrorMessage;

	// Objects
   string ActivationKey;
	string LicenseKey;
	string Company{"MTI Film, LLCxx"};
	string User{"Everyone"};
	vector<string> Features;

	vector<MachineIdDatum>GetMachineIdData();

	MachineIdDatum &GetCurrentMachineIdDatum()
   {
		return _machineIdDatum;
	}

   vector<string> GetQlmLicensedFeatures();

	int LaunchLicensingWizard();
 //	int LaunchLicensingWizard(const UnicodeString &additionalArgs);
	static int LaunchLicensingWizard(string computerID, string activationKey);
	string GetProductName();
   string GetCompany();
   int GetMajorVersion();
   int GetMinorVersion();
   bool isLicenseServerAvailable();

   UnicodeString getLicenseWizard() const { return _qlmWizard; }
   UnicodeString getWizardArgs() const { return _wizardArgs; }
   UnicodeString getSettingsFile() const { return _settingsFile; }

   IQlmLicense* getLicense() { return _lv->QlmLicenseObject(); }

private:
	string GetMachineId(const string &macAddress);

	MachineIdDatum _machineIdDatum;
	UnicodeString _qlmWizard;
	UnicodeString _settingsFile;
	UnicodeString _wizardArgs;

	TLicenseValidator* _lv = nullptr;
	IQlmHardware *_qlmHardware = nullptr;
	ELicenseBinding _licenseBinding = ELicenseBinding::ELicenseBinding_UserDefined;

   // MTI's infor about product
   QlmProductData _productData;
};
#endif
