//---------------------------------------------------------------------------

#ifndef CorrectFakeH
#define CorrectFakeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>

#include "LicensingUnit.h"
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:    // IDE-managed Components
    TButton *ActivationButton;
    TButton *ValidateButton;
    TButton *LicenseFeatureCheckButton;
    TEdit *ProductEdit;
    TEdit *MajorVersionEdit;
    TEdit *RevisionEdit;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *MinorVersionEdit;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *MacLabel;
	TLabel *ComputerNameLabel;
	TLabel *SerialNumberLabel;
	TLabel *Label6;
	TLabel *ComputerIdLabel;
	TStringGrid *MachineIdDataStringGrid;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *ExpiresLabel;
    void __fastcall ActivationButtonClick(TObject *Sender);
    void __fastcall ValidateButtonClick(TObject *Sender);
    void __fastcall LicenseFeatureCheckButtonClick(TObject *Sender);

private:    // User declarations
    void __fastcall UpdateProductAndVersion(void);

public:     // User declarations
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
