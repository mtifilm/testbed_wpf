object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'DRS Nova'
  ClientHeight = 384
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    387
    384)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 55
    Top = 46
    Width = 10
    Height = 13
    Caption = ' . '
  end
  object Label2: TLabel
    Left = 100
    Top = 46
    Width = 10
    Height = 13
    Caption = ' . '
  end
  object Label3: TLabel
    Left = 55
    Top = 116
    Width = 23
    Height = 13
    Caption = 'Mac:'
  end
  object Label4: TLabel
    Left = 47
    Top = 138
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label5: TLabel
    Left = 39
    Top = 157
    Width = 39
    Height = 13
    Caption = 'Disk SN:'
  end
  object MacLabel: TLabel
    Left = 91
    Top = 116
    Width = 72
    Height = 13
    Caption = '000000000000'
  end
  object ComputerNameLabel: TLabel
    Left = 91
    Top = 138
    Width = 45
    Height = 13
    Caption = 'computer'
  end
  object SerialNumberLabel: TLabel
    Left = 90
    Top = 157
    Width = 36
    Height = 13
    Caption = '123456'
  end
  object Label6: TLabel
    Left = 23
    Top = 179
    Width = 57
    Height = 13
    Caption = 'Machine ID:'
  end
  object ComputerIdLabel: TLabel
    Left = 90
    Top = 179
    Width = 36
    Height = 13
    Caption = '123456'
  end
  object Label7: TLabel
    Left = 8
    Top = 232
    Width = 114
    Height = 13
    Caption = 'Potential License Values'
  end
  object Label8: TLabel
    Left = 39
    Top = 198
    Width = 39
    Height = 13
    Caption = 'Expires:'
  end
  object ExpiresLabel: TLabel
    Left = 91
    Top = 198
    Width = 29
    Height = 13
    Caption = 'Never'
  end
  object ActivationButton: TButton
    Left = 238
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Deactivate'
    TabOrder = 0
    OnClick = ActivationButtonClick
  end
  object ValidateButton: TButton
    Left = 238
    Top = 47
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Validate Old'
    TabOrder = 1
    OnClick = ValidateButtonClick
  end
  object LicenseFeatureCheckButton: TButton
    Left = 222
    Top = 78
    Width = 111
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Check Feature '#39'Base'#39
    TabOrder = 2
    OnClick = LicenseFeatureCheckButtonClick
  end
  object ProductEdit: TEdit
    Left = 24
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'DRSNova'
  end
  object MajorVersionEdit: TEdit
    Left = 24
    Top = 43
    Width = 25
    Height = 21
    TabOrder = 4
    Text = '2'
  end
  object RevisionEdit: TEdit
    Left = 120
    Top = 43
    Width = 25
    Height = 21
    TabOrder = 5
    Text = '1'
  end
  object MinorVersionEdit: TEdit
    Left = 74
    Top = 43
    Width = 25
    Height = 21
    TabOrder = 6
    Text = '0'
  end
  object MachineIdDataStringGrid: TStringGrid
    Left = 4
    Top = 248
    Width = 379
    Height = 129
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 3
    DefaultColWidth = 128
    FixedCols = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 7
    RowHeights = (
      24
      24
      24
      24
      24)
  end
end
