//---------------------------------------------------------------------------

#ifndef LicensingUnitH
#define LicensingUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TLicensingForm : public TForm
{
__published:    // IDE-managed Components
    TLabel *Label1;
    TLabel *Label3;
    TListBox *FeaturesListBox;
    TButton *OkButton;
    TLabel *Label2;
    TLabel *ValidLabel;
    TLabel *LicenseKeyLabel;
    TLabel *Label12;
    TLabel *ComputerNameLabel;
    TLabel *Label4;
    TLabel *VersionLabel;
    TLabel *Label15;
    TLabel *ProductLabel;
    TLabel *ComputerIdLabel;
    TLabel *Label13;
    TLabel *Label6;
    TLabel *CompanyLabel;
    TLabel *Label5;
    TLabel *UserLabel;
    TLabel *Label14;
    TLabel *ExpiresLabel;
    TLabel *Label17;
    TLabel *UpgradesExpireLabel;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *ActivationKeyLabel;
private:    // User declarations
public:     // User declarations
    __fastcall TLicensingForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLicensingForm *LicensingForm;
//---------------------------------------------------------------------------
#endif
