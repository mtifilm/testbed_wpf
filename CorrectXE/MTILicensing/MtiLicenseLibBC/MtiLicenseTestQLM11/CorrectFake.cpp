// ---------------------------------------------------------------------------

#include <vcl.h>
#include <string>
#pragma hdrstop

#include "CorrectFake.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#include <xmldoc.hpp>
#include <msxmldom.hpp>

#include "MtiLicenseLib.h"
#include "MtiDialogs.h"
#include "IniFile.h"
#include "QlmLicenseLib_TLB.h"

#ifdef _WIN64
#pragma comment(lib, "CommonGuiWinLib.a")
#else
#pragma comment(lib, "CommonGuiWinLib.lib")
#endif

using std::string;

TMainForm *MainForm;

// Globals
string m_product = "";
int m_majorVersion = 0;
int m_minorVersion = 0;
int m_revision = 0;

// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
	// This stupid code just prints a message about licensing
	if (MtiLicenseLib::IsActiveLicensePreCheck() == false)
	{
		MTIostringstream os;
		os << "DRS Nova is not licensed for this computer" << std::endl << std::endl;
		os << "You must have admin privilages to activate a license" << std::endl;
		os << "If you have a license key, and have such privilages" << std::endl;
		os << "press yes, otherwise no and obtain a license key and admin rights" << std::endl << std::endl;
		os << "Do you wish to continue to activate license";

		if (_MTIConfirmationDialog(os.str()) != MTI_DLG_OK)
		{
			// exit(1113);
		}
	}

	std::ostringstream ss;
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();

	if (mtiLicense->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision) == false)
	{
		// ShowMessage("Not licensed");
		// return;
	}

	ComputerNameLabel->Caption = mtiLicense->GetMachineName().c_str();
	MacLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MacAddress.c_str();

	string root = GetPathRoot(GetSystemDir());
	SerialNumberLabel->Caption = mtiLicense->GetHddVolumeSerialNumber(root).c_str();

	// ComputerIdLabel->Caption = ""; // mtiLicense->GetMachineId().c_str();
	ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();
	auto machineIdData = mtiLicense->GetMachineIdData();
	MachineIdDataStringGrid->RowCount = machineIdData.size() + 1;

	MachineIdDataStringGrid->Cells[1][0] = "Mac Address";
	MachineIdDataStringGrid->Cells[2][0] = "Machine Id";
	MachineIdDataStringGrid->ColWidths[0] = 25;
	MachineIdDataStringGrid->ColWidths[1] = 100;
	MachineIdDataStringGrid->Canvas->Font = MachineIdDataStringGrid->Font;
	auto colWidth1 = 75;
	auto colWidth2 = 75;
	auto i = 1;
	for (auto machineIdDatum : machineIdData)
	{
		MachineIdDataStringGrid->Cells[0][i] = IntToStr(i);
		MachineIdDataStringGrid->Cells[1][i] = machineIdDatum.MacAddress.c_str();
		MachineIdDataStringGrid->Cells[2][i] = machineIdDatum.MachineId.c_str();

		auto w1 = MachineIdDataStringGrid->Canvas->TextWidth(MachineIdDataStringGrid->Cells[1][i]);
		if (w1 > colWidth1)
			colWidth1 = w1;
		auto w2 = MachineIdDataStringGrid->Canvas->TextWidth(MachineIdDataStringGrid->Cells[2][i]);
		if (w2 > colWidth2)
			colWidth2 = w2;
		i++;
	}

	MachineIdDataStringGrid->ColWidths[1] = colWidth1 + 16;
	MachineIdDataStringGrid->ColWidths[2] = colWidth2 + 16;
	Width = MachineIdDataStringGrid->ColWidths[0] + MachineIdDataStringGrid->ColWidths[1] +
		 MachineIdDataStringGrid->ColWidths[2] + 30 + 2 * MachineIdDataStringGrid->Left;

	ProductEdit->Text = mtiLicense->GetProductName().c_str();
	MajorVersionEdit->Text = IntToStr(mtiLicense->GetMajorVersion());
	MinorVersionEdit->Text = IntToStr(mtiLicense->GetMinorVersion());

	auto expires = mtiLicense->Expires();
	auto dateString = mtiLicense->ExpirationDateStringPretty();
	ExpiresLabel->Caption = dateString.c_str();

	auto features = mtiLicense->GetQlmLicensedFeatures();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::ActivationButtonClick(TObject *Sender)
{
	MtiLicenseLib mtiLicense;

	mtiLicense.LaunchLicensingWizard();

	// UpdateProductAndVersion();
	//
	// STARTUPINFO StartupInfo;
	// PROCESS_INFORMATION ProcInfo;
	// memset(&StartupInfo, 0, sizeof(StartupInfo));
	// memset(&ProcInfo, 0, sizeof(ProcInfo));
	//
	// StartupInfo.cb = sizeof(StartupInfo); // Set structure size
	//
	// // Find the MTIActivation.exe executable - we check the current directory and also
	// // the Release directory to hopefully handle the case where it's run directly and
	// // also for when run via an installer.
	// string command = "MTIActivation.exe";
	// if (!FileExists(command.c_str()))
	// {
	// command = "..\\Release\\MTIActivation.exe";
	// }
	//
	// string commandAndArgs = command + " " + MtiLicenseLib::GetProductCombinedFullName(m_product, m_majorVersion, m_minorVersion, m_revision);
	// LPSTR commandAndArgsLpStr = const_cast<char *>(commandAndArgs.c_str());
	// bool res = CreateProcess(NULL, commandAndArgsLpStr, NULL, NULL, NULL, NULL, NULL, NULL, &StartupInfo, &ProcInfo); // starts MyApp
	// if (res)
	// {
	// WaitForSingleObject(ProcInfo.hThread, INFINITE); // wait forever for process to finish
	// SetFocus(); // Bring back focus
	// }
	// else
	// {
	// ShowMessage("Failed to start MTIActivation.exe via CreateProcess()");
	// }
	//
	// return;

#ifdef EXAMPLE_OF_ACTIVATION_CODE
	// Example Code for doing activation from C++ - we hopefully will never need it since
	// we handle that from C#, but it's here for future reference.
	// If it really needs to be used, then the MtiLicenseLib class should be expanded to
	// handle qlmLicense->ActivationLicenseEx() instead of using the TLicenseValidator
	// class - which was example code from QLM.
	WideString webServiceUrl = "https://quicklicensemanager.com/mtifilm/qlm/qlmservice.asmx";
	WideString communicationEncryptionKey = "{946B7514-9B10-4EAE-8E9D-6707B2669084}";
	WideString response;

	TLicenseValidator *lv = new TLicenseValidator();
	IQlmLicense *lic = lv->Qlmlicense();
	lv->Qlmlicense()->CommunicationEncryptionKey = communicationEncryptionKey;
	HRESULT result = lv->Qlmlicense()->ActivateLicenseEx(webServiceUrl, "AYJZ1-G0A04-VW14X-78F31-3H1U7-8XIT4-FJY877GP",
		 "MTI-PC", "MTI-PC", "5.0.0", "Custom", "", response);

	ShowMessage("License: " + response);
#endif
}
// ---------------------------------------------------------------------------

std::string ConvertWCSToMBS(const wchar_t* pstr, long wslen)
{
	int len = ::WideCharToMultiByte(CP_ACP, 0, pstr, wslen, NULL, 0, NULL, NULL);

	std::string dblstr(len, '\0');
	len = ::WideCharToMultiByte(CP_ACP, 0 /* no flags */ , pstr, wslen /* not necessary NULL-terminated */ , &dblstr[0],
		 len, NULL, NULL /* no default char */);

	return dblstr;
}

BSTR ConvertMBSToBSTR(const std::string& str)
{
	int wslen = ::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), NULL, 0);

	BSTR wsdata = ::SysAllocStringLen(NULL, wslen);
	::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), wsdata, wslen);
	return wsdata;
}

std::string ConvertBSTRToMBS(BSTR bstr)
{
	int wslen = ::SysStringLen(bstr);
	return ConvertWCSToMBS((wchar_t*)bstr, wslen);
}

void __fastcall TMainForm::ValidateButtonClick(TObject *Sender)
{
	ReadV6MtiLicenseFile v6LicenseFile("DRSNOVA", 4, 0);
	if (v6LicenseFile.isFileValid() == false)
	{
		MTIostringstream os;
		os << "Could not read " << std::endl;
		os << v6LicenseFile.getLicenseFileName();
		ShowMessage(os.str().c_str());
		return;
	}

	string productNameRead = v6LicenseFile.getProductNameRead();
	string computerKeyLocal = v6LicenseFile.getComputerKeyLocal();
	string computerIdLocal = v6LicenseFile.getComputerIdLocal();
	string activationKey = v6LicenseFile.getActivationKey();
	string company = v6LicenseFile.getCompany();
	string user = v6LicenseFile.getUser();

	// Now we get a license, the license may not be known to QLM
	// but the ID is known
	// This is a sanity check
	MtiLicenseLib mtiLicenseLib;
	auto ids = mtiLicenseLib.GetMachineIdData();

	auto it = find_if(ids.begin(), ids.end(), [&](MachineIdDatum id) {return id.MachineId == computerIdLocal;});

	auto found = it != ids.end();
	if (found == false)
	{
		ShowMessage("Machine Id does not match stored Id");
	}
	else
	{
		UnicodeString uniComputerId(computerIdLocal.c_str(), computerIdLocal.length());
		UnicodeString uniActivationKey(activationKey.c_str(), activationKey.length());
		UnicodeString uniComputerKey(computerKeyLocal.c_str(), computerKeyLocal.length());

		BSTR bstrComputerId(uniComputerId.w_str());
		BSTR bstrActivationKey = ConvertMBSToBSTR(activationKey);
		BSTR bstrComputerKey = ConvertMBSToBSTR(computerKeyLocal);
		BSTR errorMessage;

		auto license = mtiLicenseLib.getLicense();

		MTIostringstream os;
//		if (mtiLicenseLib.isLicenseServerAvailable() == false)
//		{
//			os << "Could not contact license server";
//		}
//		else
//		{
//			os << "License server found";
//		}
//
//		ShowMessage(os.str().c_str());


		{

			string command = "ConvertToQlm11License.exe";
			if (!FileExists(command.c_str()))
			{
				command = "..\\Release\\ConvertToQlm11License.exe";
			}

			SHELLEXECUTEINFO shExInfo =
			{0};
			shExInfo.cbSize = sizeof(shExInfo);
			shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
			shExInfo.hwnd = 0;
			shExInfo.lpVerb = _T("runas"); // Operation to perform
			shExInfo.lpFile = _T(command.c_str()); // Application to start
			shExInfo.lpParameters = ""; // Additional parameters
			shExInfo.lpDirectory = 0;
			shExInfo.nShow = SW_SHOW;
			shExInfo.hInstApp = 0;

			if (ShellExecuteEx(&shExInfo))
			{
				WaitForSingleObject(shExInfo.hProcess, INFINITE);
				CloseHandle(shExInfo.hProcess);
			}

//			STARTUPINFO StartupInfo;
//			PROCESS_INFORMATION ProcInfo;
//			memset(&StartupInfo, 0, sizeof(StartupInfo));
//			memset(&ProcInfo, 0, sizeof(ProcInfo));
//
//			StartupInfo.cb = sizeof(StartupInfo); // Set structure size
//
//			// Find the MTIActivation.exe executable - we check the current directory and also
//			// the Release directory to hopefully handle the case where it's run directly and
//			// also for when run via an installer.
//			string command = "ConvertToQlm11License.exe";
//			if (!FileExists(command.c_str()))
//			{
//				command = "..\\Release\\ConvertToQlm11License.exe";
//			}
//
//			string commandAndArgs = command;
//			LPSTR commandAndArgsLpStr = const_cast<char *>(commandAndArgs.c_str());
//			bool res = CreateProcess(NULL, commandAndArgsLpStr, NULL, NULL, NULL, NULL, NULL, NULL, &StartupInfo,
//				 &ProcInfo);


		}

		VARIANT_BOOL userLevelResult = false;
		VARIANT_BOOL machineLevelResult = false;
		VARIANT_BOOL pRetVal = false;

		ShowMessage(ConvertBSTRToMBS(bstrActivationKey).c_str());
		ShowMessage(ConvertBSTRToMBS(bstrComputerKey).c_str());

		return;
		//
		// auto status = license->ValidateLicenseEx(bstrComputerKey,
		// bstrComputerId,
		// &errorMessage);
		// ShowMessage(ConvertBSTRToMBS(errorMessage).c_str());

		os << "An older DRSNova 4.0 license has been found" << std::endl;
		os << "A one time conversion must be preformed" << std::endl;
		os << "You need to grant admin privilages to convert the license" << std::endl << std::endl;
		os << "To preform the license conversion, choose OK to launch licensing wizard " << std::endl;
		os << "and, when asked, allow the licensing wizard admin access " << std::endl << std::endl;

		os << "Warning, the Wizard may say the license is activated but follow these steps anyways" << std::endl;

		os << "Choose activate license " << std::endl;
		os << "Next, choose Offline Activation " << std::endl;
		os << "Finally, activate the license using the activate button" << std::endl << std::endl;
		ShowMessage(os.str().c_str());
		UnicodeString args = L" /settings \"" + mtiLicenseLib.getSettingsFile() + "\"" + " /computerID " + uniComputerId +
			 L" /activationKey \"" + uniActivationKey + "\"" + " /computerKey " + uniComputerKey;

		// mtiLicenseLib.LaunchLicensingWizard(args);
	}
}

// ---------------------------------------------------------------------------

void __fastcall TMainForm::LicenseFeatureCheckButtonClick(TObject *Sender)
{
	// First, validate the license
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();

	// Now, check if the feature named 'Base' is licensed
	string featureStatus = mtiLicense->IsFeatureLicensed("Base") ? "Ok" : "Not Ok";
	ShowMessage(("Feature 'Base': " + featureStatus).c_str());

	MacLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MacAddress.c_str();
	ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();
}

void __fastcall TMainForm::UpdateProductAndVersion(void)
{
	// Get the product
	AnsiString tmp = ProductEdit->Text.c_str();
	m_product = tmp.c_str();

	m_majorVersion = MajorVersionEdit->Text.ToInt();
	m_minorVersion = MinorVersionEdit->Text.ToInt();
	m_revision = RevisionEdit->Text.ToInt();
}
// ---------------------------------------------------------------------------
