//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ConvertToQlm11LicenseUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

#include <xmldoc.hpp>
#include <msxmldom.hpp>

#include "MtiLicenseLib.h"
#include "IniFile.h"

#ifdef _WIN64
#pragma comment(lib, "CommonGuiWinLib.a")
#else
#pragma comment(lib, "CommonGuiWinLib.lib")
#endif

//---------------------------------------------------------------------------

std::string ConvertWCSToMBS(const wchar_t* pstr, long wslen)
{
	int len = ::WideCharToMultiByte(CP_ACP, 0, pstr, wslen, NULL, 0, NULL, NULL);

	std::string dblstr(len, '\0');
	len = ::WideCharToMultiByte(CP_ACP, 0 /* no flags */ , pstr, wslen /* not necessary NULL-terminated */ , &dblstr[0],
		 len, NULL, NULL /* no default char */);

	return dblstr;
}

BSTR ConvertMBSToBSTR(const std::string& str)
{
	int wslen = ::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), NULL, 0);

	BSTR wsdata = ::SysAllocStringLen(NULL, wslen);
	::MultiByteToWideChar(CP_ACP, 0 /* no flags */ , str.data(), str.length(), wsdata, wslen);
	return wsdata;
}

std::string ConvertBSTRToMBS(BSTR bstr)
{
	int wslen = ::SysStringLen(bstr);
	return ConvertWCSToMBS((wchar_t*)bstr, wslen);
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	// See if a license is valid
	if (MtiLicenseLib::IsActiveLicensePreCheck())
   {
   	SuccessfulLabel->Caption = "License already converted";
      return;
   }

   // See if a license has been found
   ReadV6MtiLicenseFile v6LicenseFile("DRSNOVA", 4, 0);
   if (v6LicenseFile.isFileValid() == false)
   {
	   SuccessfulLabel->Caption = "No previous license found to convert";
      CloseLabel->Caption = v6LicenseFile.getLicenseFileName().c_str();
      return;
   }

	string productNameRead = v6LicenseFile.getProductNameRead();
	string computerKeyLocal = v6LicenseFile.getComputerKeyLocal();
	string computerIdLocal = v6LicenseFile.getComputerIdLocal();
	string activationKey = v6LicenseFile.getActivationKey();
	string company = v6LicenseFile.getCompany();
	string user = v6LicenseFile.getUser();

   // Now we get a license, the license may not be known to QLM
   // but the ID is known
   // This is a sanity check
   MtiLicenseLib mtiLicenseLib;
   auto ids = mtiLicenseLib.GetMachineIdData();

   auto it = find_if(ids.begin(), ids.end(),
   	[&](MachineIdDatum id) { return id.MachineId == computerIdLocal; } );

   auto found = (it != ids.end());

   if (found == false)
   {
   	   SuccessfulLabel->Caption = "License found but unable to convert";
         CloseLabel->Caption = "License does not match machine id";
         return;
   }

   auto license = mtiLicenseLib.getLicense();
	BSTR bstrActivationKey = ConvertMBSToBSTR(activationKey);
	BSTR bstrComputerKey = ConvertMBSToBSTR(computerKeyLocal);
	BSTR errorMessage;
	VARIANT_BOOL userLevelResult = false;
	VARIANT_BOOL machineLevelResult = false;
	VARIANT_BOOL pRetVal = false;

   auto status = license->StoreKeysEx
      (
      	bstrActivationKey,
         bstrComputerKey,
         &userLevelResult,
         &machineLevelResult,
         &errorMessage,
         &pRetVal
      );
}

//---------------------------------------------------------------------------
void __fastcall TForm1::OkButtonClick(TObject *Sender)
{
   Close();
}

//---------------------------------------------------------------------------
