/*
    File Name:  Obfuscate.h
    Create:  May 01, 2004
    Original Author: John Mertus
    Updated for Visual C++: March 5, 2012
*/

/*
   The purpose behind these OBFUS_n macros is to obfuscate the strings in an executable.
   This is not encryption.

   One very easy method of attack is to find the feature string DNXHD and patch
   the executable so the feature string now reads H264.  When the DNXHD is checked,
   H264 is requested as the license check, and as a result one gets DNXHD for free.

   One has a choice of Obfuscation or Encryption.  Since the Encryption keys must
   be embedded in the code for decryption to work, for small strings, Obfuscation is
   as good as Encryption.

   There are two usual ways for Obfuscation; the first is to break the string apart
   in the code and put it together at run time, the second is provide a reversible
   function, either symmetric (XOR, ROT13) or asymmetric ones.  In the latter case
   a separate program must be run to produce a string.  That string is still identifiable as
   a string in the executable and thus subject to an attack; that is, if the same decode
   function and key are used, then a cracker can, by educated guesswork, find the encoded strings
   and swap them.  Just as above, this defeat the obfuscation without even knowing the
   algorithm or key!

   We decided to use the first method for the small feature keys.  For this purpose
   there are a sequence of macros
        OBFUS_n(char *feature,  unsigned char salt, char 0, char 1, char 2, �, char n-1);

   In order to hide the 8 character CORR_DRS string, one does
        char Feature[MAX_FEATURE_LEN+1] ;    //   Defined in lmclient.h, (this is 31).
        OBFUS_8(Feature, 0x31, 'C', 'O', 'R', 'R', '_', 'D',  'R', 'S');

   This will result in Feature being an zero terminated char string of  "CORR_DRS".
   There will be no string stored in the executable, in fact, the characters that make up the
   features are themselves not even stored.

   The salt is a number choosen by random that is less than 128 that is used to generate
   differing code blocks.  Thus
          OBFUS_8(Feature, 0x78, 'C', 'O', 'R', 'R', '_', 'D',  'R', 'S');
   still produces "CORR_DRS" but the code block that generates it will be slightly
   different.  Thus hacker tricks that locate encryption by finding blocks of code that are
   similar in the executable is made much more difficult.

   A word of caution, only use a fixed number, never a variable or an expression such as
   random(128).  This is for two reasons; the most important is that the compiler collapse
   a character and salt together and this obfuscate the character.  A variable will put the character
   into the code itself.  The second reason is that this obfuscating is very, very fast and costs almost
   nothing, which is why character arrays and not strings were used.

   To repeat, OBFUS_n is not an attempt at encryption, but is used to hide the strings.
   If this method becomes public, it then decreases the effectness.

*/

//---------------------------------------------------------------------------

#ifndef _ObfuscateH
#define _ObfuscateH

// Note: See repo film (Cohog branch) in Supplementary\Misc for "obfus_c++.pl" to help generate the "OBFUS_" lines easily.

#define OBFUS_2(s, p, A0, A1) \
  { char c=(char)(A0^p); s[0] = (char)c^p; \
    s[2] ^=s[2]; \
    char c1 = (char)(A1^0xD3^(p+p)); \
    s[1] = (char)c1^0xD3^(p+p); }

#define OBFUS_3(s, p, A0, A1, A2) \
  { char c=(char)(A0^p); s[0] = (char)c^p; \
    char c2 = (char)(A2^0x8F^p); \
    c = A1^(char)(p+p); \
    s[1] = c^(char)(p+p); \
    s[3] ^=s[3]; \
    s[2] = (char)c2^0x8F^p; }

#define OBFUS_4(s, p, A0, A1, A2, A3) \
  { char c=(char)(A0^p); s[0] = (char)c^p; \
    char c3 = (char)(A3^0xF0^p); \
    c = A1^(char)(p+p); \
    s[3] = (char)c3^0xF0^p; \
    s[1] = (char)c^(char)(p+p); \
    c = (char)(A2^0xE1)^(char)(p+p); \
    s[4] ^=s[4]; \
    s[2] = (char)c^0xE1^(char)(p+p); }

#define OBFUS_5(s, p, A0, A1, A2, A3, A4) \
  OBFUS_3(s, p, A0, A1, A2) \
  OBFUS_2((s+3), p+p+p, A3, A4)

#define OBFUS_6(s, p, A0, A1, A2, A3, A4, A5) \
  OBFUS_3(s, p, A0, A1, A2) \
  OBFUS_3((s+3), p+p, A3, A4, A5)

#define OBFUS_7(s, p, A0, A1, A2, A3, A4, A5, A6) \
  OBFUS_4(s, p, A0, A1, A2, A3) \
  OBFUS_3((s+4), p+p, A4, A5, A6)

#define OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_4(s, p, A0, A1, A2, A3) \
  OBFUS_4((s+4), p+p + 1, A4, A5, A6, A7)

#define OBFUS_9(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8) \
  OBFUS_6(s, p, A0, A1, A2, A3, A4, A5) \
  OBFUS_3((s+6), p+p + 11, A6, A7, A8)

#define OBFUS_10(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9) \
  OBFUS_6(s, p, A0, A1, A2, A3, A4, A5) \
  OBFUS_4((s+6), p+p + 11, A6, A7, A8, A9)

#define OBFUS_11(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10) \
  OBFUS_5(s, p, A0, A1, A2, A3, A4) \
  OBFUS_6((s+5), p+p + 21, A5, A6, A7, A8, A9, A10)

#define OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_4((s+8), p+p + 21, A8, A9, A10, A11)

#define OBFUS_13(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12) \
  OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_5((s+8), p+p + 21, A8, A9, A10, A11, A12)

#define OBFUS_14(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13) \
  OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_6((s+8), p+p + 21, A8, A9, A10, A11, A12, A13)

#define OBFUS_15(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14) \
  OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_7((s+8), p+p + 21, A8, A9, A10, A11, A12, A13, A14)

#define OBFUS_16(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15) \
  OBFUS_8(s, p, A0, A1, A2, A3, A4, A5, A6, A7) \
  OBFUS_8((s+8), p+p + 21, A8, A9, A10, A11, A12, A13, A14, A15)

#define OBFUS_17(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16) \
  OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_5((s+12), p, A12, A13, A14, A15, A16)

#define OBFUS_18(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17) \
  OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_6((s+12), p, A12, A13, A14, A15, A16, A17)

#define OBFUS_19(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18) \
  OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_7((s+12), p, A12, A13, A14, A15, A16, A17, A18)

#define OBFUS_22(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21) \
  OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_10((s+12), p, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21)

#define OBFUS_38(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21, A22, A23, A24, A25, A26, A27, A28, A29, A30, A31, A32, A33, A34, A35, A36, A37) \
  OBFUS_12(s, p, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) \
  OBFUS_12((s+12), p, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21, A22, A23) \
  OBFUS_12((s+24), p, A24, A25, A26, A27, A28, A29, A30, A31, A32, A33, A34, A35) \
  OBFUS_2((s+36), p, A36, A37)

#endif