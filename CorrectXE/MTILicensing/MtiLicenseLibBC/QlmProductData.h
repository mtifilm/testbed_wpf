//---------------------------------------------------------------------------

#ifndef QlmProductDataH
#define QlmProductDataH
//---------------------------------------------------------------------------

#include "Obfuscate.h"
#include <string>
#include <map>

class QlmProductData final
{
public:
	// Constructor / Destructor
	QlmProductData();
	~QlmProductData();

	// Methods
   std::string getUnknownString(int featureSet, int featureId);
   std::string getProductName() const { return _productName;}

	// Objects
	long _productId = -1;
	std::map<std::pair<int, int>, std::string> _features;

   std::string getQlmLWFileName() const { return _qlmLWFileName; }

private:
   std::string _productName;
   std::string _qlmLWFileName = "DRSNOVA.lw.xml";
};

#endif
