// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 10/2/2019 4:51:17 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319\mscoree.tlb (1)
// LIBID: {5477469E-83B1-11D2-8B49-00A0C9B7C9C4}
// LCID: 0
// Helpfile: 
// HelpString: Common Language Runtime Execution Engine 2.4 Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN64
// Errors:
//   Error creating palette bitmap of (TComCallUnmarshal) : Server C:\Windows\SysWOW64\mscoree.dll contains no icons
//   Error creating palette bitmap of (TComCallUnmarshalV4) : Server C:\Windows\SysWOW64\mscoree.dll contains no icons
//   Error creating palette bitmap of (TCorRuntimeHost) : Server C:\Windows\SysWOW64\mscoree.dll contains no icons
//   Error creating palette bitmap of (TCLRRuntimeHost) : Server C:\Windows\SysWOW64\mscoree.dll contains no icons
//   Error creating palette bitmap of (TTypeNameFactory) : Server C:\Windows\SysWOW64\mscoree.dll contains no icons
// Cmdline:
//   "C:\Builds\62143\Soraco\QLM12\Sources\redistrib\delphi\bin\tlibimp.exe"  -C "C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319\mscoree.tlb" -D"C:\Builds\62143\Soraco\QLM12\Sources\QlmLicenseLib\bin\Release\sa"
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "mscoree_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Mscoree_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
const GUID LIBID_mscoree = {0x5477469E, 0x83B1, 0x11D2,{ 0x8B, 0x49, 0x00,0xA0, 0xC9, 0xB7,0xC9, 0xC4} };
const GUID IID_ICorSvcDependencies = {0xDDB34005, 0x9BA3, 0x4025,{ 0x95, 0x54, 0xF0,0x0A, 0x2D, 0xF5,0xDB, 0xF5} };
const GUID GUID__SvcWorkerPriority = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID IID_ICorSvcWorker = {0xD1047BC2, 0x67C0, 0x400C,{ 0xA9, 0x4C, 0xE6,0x44, 0x46, 0xA6,0x7F, 0xBE} };
const GUID IID_ICorSvcWorker2 = {0xF3358A7D, 0x0061, 0x4776,{ 0x88, 0x0E, 0xA2,0xF2, 0x1B, 0x9E,0xF9, 0x3E} };
const GUID IID_ICorSvcWorker3 = {0xDC516615, 0x47BE, 0x477E,{ 0x8B, 0x55, 0xC5,0xAB, 0xE0, 0xD7,0x6B, 0x8F} };
const GUID GUID__NGenPrivateAttributes = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID IID_ICorSvcSetPrivateAttributes = {0xB18E0B40, 0xC089, 0x4350,{ 0x83, 0x28, 0x06,0x6C, 0x66, 0x8B,0xCC, 0xC2} };
const GUID IID_ICorSvcRepository = {0xD5346658, 0xB5FD, 0x4353,{ 0x96, 0x47, 0x07,0xAD, 0x47, 0x83,0xD5, 0xA0} };
const GUID IID_ICorSvcAppX = {0x5C814791, 0x559E, 0x4F7F,{ 0x83, 0xCE, 0x18,0x4A, 0x4C, 0xCB,0xAE, 0x24} };
const GUID IID_ICorSvcLogger = {0xD189FF1A, 0xE266, 0x4F13,{ 0x96, 0x37, 0x4B,0x95, 0x22, 0x27,0x9F, 0xFC} };
const GUID IID_ICorSvcPooledWorker = {0x0631E7E2, 0x6046, 0x4FDE,{ 0x8B, 0x6D, 0xA0,0x9B, 0x64, 0xFD,0xA6, 0xF3} };
const GUID IID_ICorSvcBindToWorker = {0x5C6FB596, 0x4828, 0x4ED5,{ 0xB9, 0xDD, 0x29,0x3D, 0xAD, 0x73,0x6F, 0xB5} };
const GUID IID_ITypeName = {0xB81FF171, 0x20F3, 0x11D2,{ 0x8D, 0xCC, 0x00,0xA0, 0xC9, 0xB0,0x05, 0x22} };
const GUID IID_ITypeNameBuilder = {0xB81FF171, 0x20F3, 0x11D2,{ 0x8D, 0xCC, 0x00,0xA0, 0xC9, 0xB0,0x05, 0x23} };
const GUID IID_ITypeNameFactory = {0xB81FF171, 0x20F3, 0x11D2,{ 0x8D, 0xCC, 0x00,0xA0, 0xC9, 0xB0,0x05, 0x21} };
const GUID IID_IApartmentCallback = {0x178E5337, 0x1528, 0x4591,{ 0xB1, 0xC9, 0x1C,0x6E, 0x48, 0x46,0x86, 0xD8} };
const GUID IID_IManagedObject = {0xC3FCC19E, 0xA970, 0x11D2,{ 0x8B, 0x5A, 0x00,0xA0, 0xC9, 0xB7,0xC9, 0xC4} };
const GUID IID_ICatalogServices = {0x04C6BE1E, 0x1DB1, 0x4058,{ 0xAB, 0x7A, 0x70,0x0C, 0xCC, 0xFB,0xF2, 0x54} };
const GUID CLSID_ComCallUnmarshal = {0x3F281000, 0xE95A, 0x11D2,{ 0x88, 0x6B, 0x00,0xC0, 0x4F, 0x86,0x9F, 0x04} };
const GUID IID_IMarshal = {0x00000003, 0x0000, 0x0000,{ 0xC0, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x46} };
const GUID IID_IStream = {0x0000000C, 0x0000, 0x0000,{ 0xC0, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x46} };
const GUID IID_ISequentialStream = {0x0C733A30, 0x2A1C, 0x11CE,{ 0xAD, 0xE5, 0x00,0xAA, 0x00, 0x44,0x77, 0x3D} };
const GUID GUID__LARGE_INTEGER = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID GUID__ULARGE_INTEGER = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID GUID_tagSTATSTG = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID GUID__FILETIME = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID CLSID_ComCallUnmarshalV4 = {0x45FB4600, 0xE6E8, 0x4928,{ 0xB2, 0x5E, 0x50,0x47, 0x6F, 0xF7,0x94, 0x25} };
const GUID CLSID_CorRuntimeHost = {0xCB2F6723, 0xAB3A, 0x11D2,{ 0x9C, 0x40, 0x00,0xC0, 0x4F, 0xA3,0x0A, 0x3E} };
const GUID IID_ICorRuntimeHost = {0xCB2F6722, 0xAB3A, 0x11D2,{ 0x9C, 0x40, 0x00,0xC0, 0x4F, 0xA3,0x0A, 0x3E} };
const GUID IID_ICorConfiguration = {0x5C2B07A5, 0x1E98, 0x11D3,{ 0x87, 0x2F, 0x00,0xC0, 0x4F, 0x79,0xED, 0x0D} };
const GUID IID_IGCThreadControl = {0xF31D1788, 0xC397, 0x4725,{ 0x87, 0xA5, 0x6A,0xF3, 0x47, 0x2C,0x27, 0x91} };
const GUID IID_IGCHostControl = {0x5513D564, 0x8374, 0x4CB9,{ 0xAE, 0xD9, 0x00,0x83, 0xF4, 0x16,0x0A, 0x1D} };
const GUID IID_IDebuggerThreadControl = {0x23D86786, 0x0BB5, 0x4774,{ 0x8F, 0xB5, 0xE3,0x52, 0x2A, 0xDD,0x62, 0x46} };
const GUID IID_IGCHost = {0xFAC34F6E, 0x0DCD, 0x47B5,{ 0x80, 0x21, 0x53,0x1B, 0xC5, 0xEC,0xCA, 0x63} };
const GUID GUID__COR_GC_STATS = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID GUID__COR_GC_THREAD_STATS = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID IID_IValidator = {0x63DF8730, 0xDC81, 0x4062,{ 0x84, 0xA2, 0x1F,0xF9, 0x43, 0xF5,0x9F, 0xAC} };
const GUID IID_IVEHandler = {0x856CA1B2, 0x7DAB, 0x11D3,{ 0xAC, 0xEC, 0x00,0xC0, 0x4F, 0x86,0xC3, 0x09} };
const GUID GUID_tag_VerError = {0x00000000, 0x0000, 0x0000,{ 0x00, 0x00, 0x00,0x00, 0x00, 0x00,0x00, 0x00} };
const GUID IID_IDebuggerInfo = {0xBF24142D, 0xA47D, 0x4D24,{ 0xA6, 0x6D, 0x8C,0x21, 0x41, 0x94,0x4E, 0x44} };
const GUID CLSID_CLRRuntimeHost = {0x90F1A06E, 0x7712, 0x4762,{ 0x86, 0xB5, 0x7A,0x5E, 0xBA, 0x6B,0xDB, 0x02} };
const GUID IID_ICLRRuntimeHost = {0x90F1A06C, 0x7712, 0x4762,{ 0x86, 0xB5, 0x7A,0x5E, 0xBA, 0x6B,0xDB, 0x02} };
const GUID IID_IHostControl = {0x02CA073C, 0x7079, 0x4860,{ 0x88, 0x0A, 0xC2,0xF7, 0xA4, 0x49,0xC9, 0x91} };
const GUID IID_ICLRControl = {0x9065597E, 0xD1A1, 0x4FB2,{ 0xB6, 0xBA, 0x7E,0x1F, 0xCE, 0x23,0x0F, 0x61} };
const GUID IID_ICLRValidator = {0x63DF8730, 0xDC81, 0x4062,{ 0x84, 0xA2, 0x1F,0xF9, 0x43, 0xF5,0x9F, 0xDD} };
const GUID CLSID_TypeNameFactory = {0xB81FF171, 0x20F3, 0x11D2,{ 0x8D, 0xCC, 0x00,0xA0, 0xC9, 0xB0,0x05, 0x25} };

};     // namespace Mscoree_tlb
