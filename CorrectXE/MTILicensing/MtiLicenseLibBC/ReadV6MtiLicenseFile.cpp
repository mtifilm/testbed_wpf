// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ReadV6MtiLicenseFile.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

#ifdef __BORLANDC__
#include <xmldoc.hpp>
#include <msxmldom.hpp>
#pragma comment(lib, "xmlrtl.bpi")
#endif

ReadV6MtiLicenseFile::ReadV6MtiLicenseFile(const string &product, int majorVersion, int minorVersion)
{
   if (doesLicenseFileExist(product, majorVersion, minorVersion) == false)
   {
      _isFileValid = false;
   	return;
   }

   _productName = product;
   _majorVersion = majorVersion;
   _minorVersion = minorVersion;
   readFile(getLicenseFileName());
}

// --------------------------------------------------------------------------------------------------------------------
// Gets the License Filename based on the product, major version and minor version numbers.
// --------------------------------------------------------------------------------------------------------------------
string ReadV6MtiLicenseFile::getLicenseFileName(const string &product, int majorVersion, int minorVersion) {
	return getLicenseFileName(GetProgramDataDir(), product, majorVersion, minorVersion);}

string ReadV6MtiLicenseFile::getLicenseFileName(const string &path, const string &product, int majorVersion,
	 int minorVersion)
{
	// TODO: The licenseDirectory should probably not be hard-coded.
	string licenseDirectory = AddDirSeparator(path) + "Licenses\\";

	// Replace colons with underscores
	string productWithUnderscores = product;
	replace(productWithUnderscores.begin(), productWithUnderscores.end(), ':', '_');

	string productCombinedName = getProductCombinedName(productWithUnderscores, majorVersion, minorVersion);
	string filename = "MTILicense_" + productCombinedName + ".qlm";
	string fullFilename = licenseDirectory + filename;

	return fullFilename;
}

bool ReadV6MtiLicenseFile::doesLicenseFileExist(const string &product, int majorVersion, int minorVersion)
{
	// Look up the license
	// This code is a little kluge that looks up the file in the newer folder, if not found
	// it looks it up in the old, if found, copies it over
	string licenseFileName = getLicenseFileName(product, majorVersion, minorVersion);
	return DoesFileExist(licenseFileName);
}

// --------------------------------------------------------------------------------------------------------------------
// Gets the combined name for a product and major / minor versions - e.g. "COHOG-1.0"
// --------------------------------------------------------------------------------------------------------------------
string ReadV6MtiLicenseFile::getProductCombinedName(const string &product, int majorVersion, int minorVersion)
{
	// Combine product, & version #'s as a key for looking up the product specific data
	const int bufsize = 256;
	char tmp[bufsize];

	sprintf_s(tmp, bufsize, "%s-%d.%d", product.c_str(), majorVersion, minorVersion);

	string combinedName = tmp;

	return combinedName;
}

bool ReadV6MtiLicenseFile::readFile(const string &licenseFile)
{
	_di_IXMLDocument document = LoadXMLDocument(licenseFile.c_str());

	const _di_IXMLNode nodeElement = document->ChildNodes->FindNode("MTILicense");

	bool gotLicenseKey = false;
	bool gotActivationKey = false;
	bool gotProduct = false;
	bool gotCompany = false;
	bool gotUser = false;
   bool gotComputerId = false;
   _isFileValid = false;

	if (nodeElement != NULL)
	{
		for (int i = 0; i < nodeElement->ChildNodes->Count && !_isFileValid; i++)
		{
			const _di_IXMLNode child = nodeElement->ChildNodes->Get(i);

			AnsiString name = child->NodeName;

			if (name == "Product" && child->IsTextElement)
			{
				gotProduct = true;
				AnsiString tmp = child->Text;
				_productNameRead = tmp.c_str();
			}

			if (name == "LicenseKey" && child->IsTextElement)
			{
				gotLicenseKey = true;
				AnsiString tmp = child->Text;
				_computerKeyLocal = tmp.c_str();
			}

         if (name == "ComputerId" && child->IsTextElement)
			{
				gotComputerId = true;
				AnsiString tmp = child->Text;
				_computerIdLocal = tmp.c_str();
			}

			if (name == "ActivationKey" && child->IsTextElement)
			{
				gotActivationKey = true;
				AnsiString tmp = child->Text;
				_activationKey = tmp.c_str();
			}

			if (name == "Company" && child->IsTextElement)
			{
				gotCompany = true;
				AnsiString tmp = child->Text;
				_company = tmp.c_str();
			}

			if (name == "User" && child->IsTextElement)
			{
				gotUser = true;
				AnsiString tmp = child->Text;
				_user = tmp.c_str();
			}

			_isFileValid = gotLicenseKey && gotActivationKey && gotProduct && gotCompany && gotUser && gotComputerId;
		}
	}

   return _isFileValid;
}

bool ReadV6MtiLicenseFile::removeOldLicense()
{
	string licenseFileName = getLicenseFileName();
	if (DoesFileExist(licenseFileName) == false)
   {
      return true;
   }

   auto outOfWayFilename = ::ReplaceFileExt(licenseFileName, ".v6");
   if (::DeleteFile(outOfWayFilename.c_str()) == false)
   {
      if (GetLastError() != 2)
      {
      	return false;
      }
   }

   return ::RenameFile(licenseFileName.c_str(), outOfWayFilename.c_str());
}

