//---------------------------------------------------------------------------

#ifndef GrainModel_16uH
#define GrainModel_16uH
//---------------------------------------------------------------------------

#include "ipp.h"
//---------------------------------------------------------------------------

#define NUMBER_OF_GRAIN_BINS 10

class GrainModel
{
public:
	static GrainModel CreateFrom(Ipp16u *sourceImage, int nRows, int nCols, int whichChannel, Ipp16u maxValue);

	inline Ipp16uArray getSmoothImage() const  { return _smoothImage; }
	inline Ipp16uArray getDiffsImage() const { return _diffsImage; }
	inline Ipp16u getGrainCenter(int binIndex) const { return _grainCenter[binIndex]; }
	inline Ipp16u getGrainIntensity(int binIndex) const { return _grainIntensity[binIndex]; }

private:
	Ipp16uArray _smoothImage;
	Ipp16uArray _diffsImage;
	Ipp16u _grainCenter[NUMBER_OF_GRAIN_BINS] = {0};
	Ipp16u _grainIntensity[NUMBER_OF_GRAIN_BINS] = {0};

	GrainModel makeGrainModel(const Ipp16uArray &Src, IppiRect roi);
	GrainModel makeGrainModelUsingHistogram(const Ipp16uArray &Src, IppiRect roi);
};

#endif
