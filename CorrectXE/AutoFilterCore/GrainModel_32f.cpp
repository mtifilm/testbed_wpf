//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainModel_32f.h"

#include "HistogramIpp.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "math.h"
#include "cmath"
#include "SynchronousThreadRunner.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define USE_HISTOGRAM_INSTEAD_OF_SORTING
#define NUMBER_OF_HISTOGRAM_BINS 65535
#define MULTITHREAD_INTENSITY
#define MULTITHREAD_IMPORT

const int KERNEL_RADIUS = 3;
const double IMAGE_CENTER = .8;
const double PERCENTILE_HIGH = .8;
const double PERCENTILE_LOW = .2;
//------------------------------------------------------------------------------

namespace
{
//---------------------------

struct IntensityParams
{
	vector<float> *binDiffs;
   Ipp32f *binDiffMin;
   Ipp32f *binDiffMax;
   Ipp32f *grainIntensities;
};

int RunThreadToComputeIntensityForOneBin(void *vp, int jobNumber, int totalJobs)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<IntensityParams *>(vp);
   auto binDiffs = params->binDiffs;
   auto binDiffMin = params->binDiffMin;
   auto binDiffMax = params->binDiffMax;
   auto grainIntensities = params->grainIntensities;

   auto bin = jobNumber;
   grainIntensities[bin] = 0.0f;
   int binDiffCount = int(binDiffs[bin].size());

   if (binDiffCount > 1)
   {
#if 1
      // Compute diffs histogram.
	   Ipp32fArray binDiffArray({binDiffCount}, &binDiffs[bin][0]);
      const int numberOfHistogramBins = 1000;
      HistogramIpp histogramGenerator(binDiffArray, numberOfHistogramBins, binDiffMin[bin],  binDiffMax[bin]);
      Ipp32f histoBinQuantum = (binDiffMax[bin] - binDiffMin[bin]) / numberOfHistogramBins;
      auto diffsHistogramAllChans = histogramGenerator.compute(binDiffArray);
      auto diffsHistogram = diffsHistogramAllChans[0];

      int lowCount = binDiffCount * PERCENTILE_LOW;
      int highCount = binDiffCount * PERCENTILE_HIGH;
      size_t runningSum = 0;
      int lowIndex = -1;

      do
      {
         runningSum += diffsHistogram[++lowIndex];
      }
      while (runningSum < lowCount && lowIndex < numberOfHistogramBins);

      int highIndex = lowIndex;
      if (highIndex < (binDiffCount - 1))
      {
         do
         {
            runningSum += diffsHistogram[++highIndex];
         }
         while (runningSum < highCount && highIndex < numberOfHistogramBins);

         grainIntensities[bin] = (highIndex - lowIndex) * histoBinQuantum;
      }
#else
      Ipp32f oldGrainIntensities[NUMBER_OF_GRAIN_BINS];
      std::sort(binDiffs[bin].begin(), binDiffs[bin].end());
      int highIndex = my_lround(PERCENTILE_HIGH * (binDiffs[bin].size() - 1));
      int lowIndex = my_lround(PERCENTILE_LOW * (binDiffs[bin].size() - 1));
      grainIntensities[bin] = binDiffs[bin][highIndex] - binDiffs[bin][lowIndex];
#endif
   }

   return 0;
}

const int NumberOfSlices = 32;

IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
{
   IppiRect sliceRoi;
   int nominalRowsPerSlice = height / NumberOfSlices;
   int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
   sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
   sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
   sliceRoi.x = 0;
   sliceRoi.width = width;

   return sliceRoi;
}
//---------------------------

struct ImportParams
{
   Ipp32fArray yArray;
   MTI_UINT16 *rgb;
   int whichChannel;
   MTI_UINT16 maxValue;
};

int RunThreadToImportRawData(void *vp, int sliceNumber, int totalJobs)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<ImportParams *>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->yArray.getRows(), params->yArray.getCols(), sliceNumber);
	Ipp32fArray yArray = params->yArray(sliceRoi);
   int nRow = yArray.getRows();
   int nCol = yArray.getCols();
   int whichChannel = params->whichChannel;
   MTI_UINT16 *rgb = params->rgb + (sliceRoi.y * sliceRoi.width * 3);
   MTI_UINT16 maxValue = params->maxValue;

	if (whichChannel < 3)
	{
		yArray.importOneNormalizedChannelFromRgb(rgb, nRow, nCol, whichChannel, maxValue);
   }
	else
	{
		yArray.importNormalizedYuvFromRgb(rgb, nRow, nCol, maxValue);
	}

   return 0;
}
//---------------------------

//#define MULTITHREAD_BIN_FILLING    S L O W !!!
#ifdef MULTITHREAD_BIN_FILLING

struct BinDiffsParams
{
   Ipp32fArray smoothArray;
   Ipp32fArray diffsArray;
	vector<float> *binDiffs;
   int *binDiffsActualSize;
   Ipp32f *grainLeft;
   Ipp32f *grainRight;
   Ipp32f *binDiffMin;
   Ipp32f *binDiffMax;
   CSpinLock *binDiffLock;
};

int RunBinDiffsThread(void *vp, int sliceNumber)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<BinDiffsParams *>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->smoothArray.getRows(), params->smoothArray.getCols(), sliceNumber);

   Ipp32fArray smoothArraySlice(params->smoothArray, sliceRoi);
   Ipp32fArray diffsArraySlice(params->diffsArray, sliceRoi);
	vector<float> *binDiffs = params->binDiffs;
   int *binDiffsActualSize = params->binDiffsActualSize;
   Ipp32f *grainLeft = params->grainLeft;
   Ipp32f *grainRight = params->grainRight;
   Ipp32f *binDiffMin = params->binDiffMin;
   Ipp32f *binDiffMax = params->binDiffMax;
   CSpinLock *binDiffLock = params->binDiffLock;

	auto smoothRoiIter = smoothArraySlice.begin();
	auto diffsRoiIter = diffsArraySlice.begin();

	while (smoothRoiIter != smoothArraySlice.end())
	{
		Ipp32f value = *smoothRoiIter;
      int bin = -1;
		for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
		{
			if (value > grainLeft[bin] && value < grainRight[bin])
			{
            CAutoSpinLocker lock(binDiffLock[bin]);
            Ipp32f diff = *diffsRoiIter;
            binDiffs[bin][binDiffsActualSize[bin]++] = diff;
            binDiffMin[bin] = min<Ipp32f>(binDiffMin[bin], diff);
            binDiffMax[bin] = max<Ipp32f>(binDiffMax[bin], diff);
            break;
			}
		}

		++smoothRoiIter;
		++diffsRoiIter;
	}

   return 0;
}

#endif

} // anonymous namespace
//------------------------------------------------------------------------------

// function [GrainCenter, GrainIntensity] = MakeGrainModel (RGB, chan)
// %
// %  Grain intensity is a function of image intensity.
// %  This function measures how grain values are affected
// %  by underlying signal intensity.
// %
/* static */
GrainModel GrainModel::CreateFrom(
	Ipp16u *rgb,
	int nRow,
	int nCol,
	int whichChannel,
	Ipp16u maxValue,
	BlobPixelArrayWrapper *blobPixelArrayWrapper,
	MTI_UINT16 pixelFilterMask)
{
	// % extract the channel to use
	//	if chan == 4
	//		 Src = rgb2gray (RGB(:,:,1:3));
	//	else
	//		 Src = RGB(:,:,chan);
	//	end
   CHRTimer GRAIN_MODEL_IMPORT_TIMER;

	MTIassert(whichChannel >= 0 && whichChannel <= 3);
	Ipp32fArray yArray({nCol, nRow});

   ImportParams importParams = { yArray, rgb, whichChannel, maxValue };

#ifdef MULTITHREAD_IMPORT

   SynchronousThreadRunner multithread(NumberOfSlices, &importParams, RunThreadToImportRawData);
   multithread.Run();

#else

   for (auto i = 0; i < NumberOfSlices; ++i)
   {
      RunThreadToImportRawData(&importParams, i, 1);
   }

#endif

//	DBTRACE(GRAIN_MODEL_IMPORT_TIMER.ReadAsString());

   CHRTimer GRAIN_MODEL_MAKE_TIMER;

	// % the coordinates of the central portion.  This is done to avoid mattes
	// % and other edge effects.  This should be skipped if there is a reliable
	// % region of interest.
	// [nRow, nCol, ~] = size(RGB);
	// row0 = round(1 + nRow*(1-IMAGE_CENTER)/2);
	// row1 = round(nRow - nRow*(1-IMAGE_CENTER)/2);
	// col0 = round(1 + nCol*(1-IMAGE_CENTER)/2);
	// col1 = round(nCol - nCol*(1-IMAGE_CENTER)/2);
	int roiX = my_lround(nCol * (1 - IMAGE_CENTER) / 2);
	int roiW = my_lround(nCol - nCol * (1 - IMAGE_CENTER) / 2) - roiX;
	int roiY = my_lround(nRow * (1 - IMAGE_CENTER) / 2);
	int roiH = my_lround(nRow - nRow * (1 - IMAGE_CENTER) / 2) - roiY;
	IppiRect roi = { roiX, roiY, roiW, roiH };

	GrainModel grainModel;
	grainModel.makeGrainModel(yArray, roi, blobPixelArrayWrapper, pixelFilterMask);

//   DBTRACE(GRAIN_MODEL_MAKE_TIMER.ReadAsString());

	return grainModel;
}
//------------------------------------------------------------------------------
#define MIN_SIGNIFICANT_DEBRIS_SIZE 5
inline bool IS_NAN(float x) { return x != x; }
#ifndef NAN
#define NAN (std::sqrt(-2))
#endif

void GrainModel::cleanTheSourceImage(
	Ipp32fArray &sourceImage,
	BlobPixelArrayWrapper *blobPixelArrayWrapper,
	MTI_UINT16 pixelFilterMask)
{
	auto cleanImage = sourceImage;
	auto imageIterator = sourceImage.begin();
	BlobIterator blobIterator(blobPixelArrayWrapper);

	for (auto blobLabel = blobIterator.getNextBlob();
	blobLabel >= 0;
	blobLabel = blobIterator.getNextBlob())
	{
		BlobPixelIterator blobPixelIterator1(blobLabel, blobPixelArrayWrapper);
      auto blobPixelCount = blobPixelIterator1.getPixelCount();
		if (blobPixelCount < MIN_SIGNIFICANT_DEBRIS_SIZE)
		{
			// Not big enough to affect the grain model computations, we guess.
			continue;
		}

		// Change all the defect pixels to NaN to simplify computing the mean of the
		// surround pixels.
		for (auto pixelIndex = blobPixelIterator1.getNextPixel();
		pixelIndex >= 0;
		pixelIndex = blobPixelIterator1.getNextPixel())
		{
			imageIterator[pixelIndex] = NAN;
		}

		int surroundCount = 0;
		float surroundSum = 0;
		int nRows = sourceImage.getRows();
		int nCols = sourceImage.getCols();

		// Compute the mean of the blob's suround pixels.
		BlobPixelIterator blobPixelIterator2(blobLabel, blobPixelArrayWrapper);
		for (auto pixelIndex = blobPixelIterator2.getNextPixel();
		pixelIndex >= 0;
		pixelIndex = blobPixelIterator2.getNextPixel())
		{
			int currentRow = pixelIndex / nCols;
			int currentCol = pixelIndex % nCols;

			for (auto surroundRow = -1; surroundRow <= 1; ++surroundRow)
			{
				// Edge conditions
				if ((currentRow == 0 && surroundRow == -1)
				|| (currentRow == (nRows -1) && surroundRow == 1))
				{
					continue;
				}

				for (auto surroundCol = -1; surroundCol <= 1; ++surroundCol)
				{
					// Edge conditions
					if ((currentCol == 0 && surroundCol == -1)
					|| (currentCol == (nCols -1) && surroundCol == 1))
					{
						continue;
					}

					int surroundIndex = pixelIndex + (surroundRow * nCols) + surroundCol;

					// If the pixel's value is NaN, it's a defect pixel, so don't
               // count it!
					if (IS_NAN(imageIterator[surroundIndex]))
					{
						continue;
					}

					++surroundCount;
					surroundSum += imageIterator[surroundIndex];
				}
			}
		}

		auto surroundMean = (surroundCount == 0) ? 0 : surroundSum / surroundCount;

		// Replace all defect pixels with the mean of the surround pixels.
		BlobPixelIterator blobPixelIterator3(blobLabel, blobPixelArrayWrapper);
		for (auto pixelIndex = blobPixelIterator3.getNextPixel();
		pixelIndex >= 0;
		pixelIndex = blobPixelIterator3.getNextPixel())
		{
			imageIterator[pixelIndex] = surroundMean;
		}
	}
}
//------------------------------------------------------------------------------

void GrainModel::makeGrainModel(
	const Ipp32fArray &sourceImage,
	IppiRect roi,
	BlobPixelArrayWrapper *blobPixelArrayWrapper,
	MTI_UINT16 pixelFilterMask)
{
	MTIassert(sourceImage.getComponents() == 1);
	GrainModel grainModel;

   CHRTimer GRAIN_COPY_SOURCE_TIMER;
   Ipp32fArray cleanImage = sourceImage.duplicate();
//   DBTRACE(GRAIN_COPY_SOURCE_TIMER.ReadAsString());

   CHRTimer GRAIN_MODEL_INPAINT_TIMER;
   cleanTheSourceImage(cleanImage, blobPixelArrayWrapper, pixelFilterMask);
//   DBTRACE(GRAIN_MODEL_INPAINT_TIMER.ReadAsString());

	CHRTimer GRAIN_MODEL_SMOOTH_TIMER;

	// % the smoothing kernel
	// Kern = ones(2*KERNEL_RADIUS+1,2*KERNEL_RADIUS+1);
	// Kern = Kern / sum(Kern(:));

	// % the smoothed image.  Grain is removed.
	// Src_smooth = conv2(Src,Kern,'same');
	IppiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };
	_smoothImage = cleanImage.applyBoxFilter(boxSize);

//	DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());
	CHRTimer GRAIN_MODEL_DIFFS_TIMER;

	// % the difference image.  Un-regularized grain signal.
	// Src_diff = Src - Src_smooth;
	_diffsImage = sourceImage - _smoothImage;

//   DBTRACE(GRAIN_MODEL_DIFFS_TIMER.ReadAsString());
   CHRTimer GRAIN_MODEL_HISTOGRAM_TIMER;

	// % extract the central portions
   // Src_smooth = Src_smooth(row0:row1,col0:col1);
   // Src_diff = Src_diff(row0:row1,col0:col1);
	Ipp32fArray smoothImageWithRoi = _smoothImage(roi);
	Ipp32fArray diffsImageWithRoi = _diffsImage(roi);

   // Compute intensity histogram.
	HistogramIpp smoothHistogramGenerator(smoothImageWithRoi, NUMBER_OF_HISTOGRAM_BINS, 0.F, 1.0001F);
   auto smoothHistogramAllChans = smoothHistogramGenerator.compute(smoothImageWithRoi);
   auto smoothHistogram = smoothHistogramAllChans[0];

//   DBTRACE(GRAIN_MODEL_HISTOGRAM_TIMER.ReadAsString());
   CHRTimer GRAIN_MODEL_SETUP_BINS_TIMER;

	int numberOfElements = smoothImageWithRoi.area();
	double grainBinRadius = numberOfElements / (2.0  * NUMBER_OF_GRAIN_BINS);
   double histoBinIntensityQuantum = 1.0 / NUMBER_OF_HISTOGRAM_BINS;

	Ipp32f grainLeft[NUMBER_OF_GRAIN_BINS];
	Ipp32f grainRight[NUMBER_OF_GRAIN_BINS];

   // We need to break twice per histo bin, once on the bin boundary and
   // once at the center of the bin.
   //
   // Special case first break, left edge of first grain bin.
   auto grainBin = 0;
   grainLeft[grainBin] = 0;
   size_t runningSum = 0;
   int breakCount = 1;

   // Breaks between first and last.
   int nextBreak = grainBinRadius;
   const int totalNumberOfBreaks = (2 * NUMBER_OF_GRAIN_BINS) + 1;
   for (auto histoBin = 1; histoBin < NUMBER_OF_HISTOGRAM_BINS; ++histoBin)
   {
      runningSum += smoothHistogram[histoBin];
      if (runningSum >= nextBreak)
      {
         double histoBinIntensity = histoBin * histoBinIntensityQuantum;
         int grainBin = (breakCount - 1) / 2;
         if ((breakCount % 2) == 1)
         {
            _grainCenter[grainBin] = histoBinIntensity;
         }
         else
         {
            grainRight[grainBin] = histoBinIntensity;
            grainLeft[grainBin + 1] = histoBinIntensity;
         }

         nextBreak = int(++breakCount * grainBinRadius);
      }
   }

//   DBTRACE(GRAIN_MODEL_SETUP_BINS_TIMER.ReadAsString());
   CHRTimer GRAIN_MODEL_FILL_BINS_TIMER;

	// % determine how difference is related to intensity
	// GrainIntensity = zeros(NUMBER_OF_GRAIN_BINS, 1);
	for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
	{
		_grainIntensity[bin] = 0;
	}

	// for i = 1:NUMBER_OF_GRAIN_BINS
	// 	 % end points of the bin.  Allow overlap to smooth
	// 	 % the resulting signal
	//
	// 	 % all pixels that fall in this bin
	// 	 idx = find( (Src_smooth(:) > GrainLeft(i)) & (Src_smooth(:) < GrainRight(i)) );
	//
	// 	 % the grain values in this bin, sorted
	// 	 diff = sort(Src_diff(idx));
	//
	// 	 % the range observed in the bin
	// 	 GrainIntensity(i) = diff(round(PERCENTILE_HIGH*numel(diff))) - diff(round(PERCENTILE_LOW*numel(diff)));
	// end

	vector<float> binDiffs[NUMBER_OF_GRAIN_BINS];
   Ipp32f binDiffMin[NUMBER_OF_GRAIN_BINS];
   Ipp32f binDiffMax[NUMBER_OF_GRAIN_BINS];
   int binDiffsActualSize[NUMBER_OF_GRAIN_BINS];
   CSpinLock binDiffLock[NUMBER_OF_GRAIN_BINS];

   // Here 1.1 gives us 10% padding
   size_t diffBinReserveSize = size_t(numberOfElements * (1.1 / NUMBER_OF_GRAIN_BINS));
   for (auto i = 0; i < NUMBER_OF_GRAIN_BINS; ++i)
   {
      binDiffs[i].resize(diffBinReserveSize);
      binDiffsActualSize[i] = 0;
      binDiffMin[i] = 1.0f;
      binDiffMax[i] = 0.0f;
   }

#ifdef MULTITHREAD_BIN_FILLING

   BinDiffsParams bdParams =
   { smoothImageWithRoi, diffsImageWithRoi, binDiffs, binDiffsActualSize,
     grainLeft, grainRight, binDiffMin, binDiffMax, binDiffLock };
   SynchronousThreadRunner multithread(NumberOfSlices, &bdParams, RunBinDiffsThread);
   multithread.Run();

#else

	auto smoothRoiIter = smoothImageWithRoi.begin();
	auto diffsRoiIter = diffsImageWithRoi.begin();

	while (smoothRoiIter != smoothImageWithRoi.end())
	{
		Ipp32f value = *smoothRoiIter;
      int bin = -1;
		for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
		{
			if (value > grainLeft[bin] && value < grainRight[bin])
			{
            if (binDiffsActualSize[bin] >= diffBinReserveSize)
            {
               break;
            }

            Ipp32f diff = *diffsRoiIter;
            binDiffs[bin][binDiffsActualSize[bin]++] = diff;
            binDiffMin[bin] = std::min<Ipp32f>(binDiffMin[bin], diff);
            binDiffMax[bin] = std::max<Ipp32f>(binDiffMax[bin], diff);
            break;
			}
		}

		++smoothRoiIter;
		++diffsRoiIter;
	}

#endif

   for (auto i = 0; i < NUMBER_OF_GRAIN_BINS; ++i)
   {
      binDiffs[i].resize(binDiffsActualSize[i]);
   }

//   DBTRACE(GRAIN_MODEL_FILL_BINS_TIMER.ReadAsString());
//   DBTRACE(diffBinReserveSize);
//   DBTRACE(binDiffs[0].size());
//   DBTRACE(binDiffs[1].size());
//   DBTRACE(binDiffs[2].size());
//   DBTRACE(binDiffs[3].size());
//   DBTRACE(binDiffs[4].size());
//   DBTRACE(binDiffs[5].size());
//   DBTRACE(binDiffs[6].size());
//   DBTRACE(binDiffs[7].size());
//   DBTRACE(binDiffs[8].size());
//   DBTRACE(binDiffs[9].size());
   CHRTimer GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER;

   IntensityParams intensityParams = { binDiffs, binDiffMin, binDiffMax, _grainIntensity };

#ifdef MULTITHREAD_INTENSITY

   SynchronousThreadRunner multithread(NUMBER_OF_GRAIN_BINS, &intensityParams, RunThreadToComputeIntensityForOneBin);
   multithread.Run();

#else

   for (auto i = 0; i < NUMBER_OF_GRAIN_BINS; ++i)
   {
      RunThreadToComputeIntensityForOneBin(&intensityParams, i, 1);
   }

#endif

//   DBTRACE(GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER.ReadAsString());
//
//   DBTRACE(_grainIntensity[0]);
//   DBTRACE(_grainIntensity[1]);
//   DBTRACE(_grainIntensity[2]);
//   DBTRACE(_grainIntensity[3]);
//   DBTRACE(_grainIntensity[4]);
//   DBTRACE(_grainIntensity[5]);
//   DBTRACE(_grainIntensity[6]);
//   DBTRACE(_grainIntensity[7]);
//   DBTRACE(_grainIntensity[8]);
//   DBTRACE(_grainIntensity[9]);
}
//------------------------------------------------------------------------------

