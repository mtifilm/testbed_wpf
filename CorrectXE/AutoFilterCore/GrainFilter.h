//---------------------------------------------------------------------------

#ifndef GrainFilterH
#define GrainFilterH
//---------------------------------------------------------------------------

#include "BlobIdentifier.h"
#include "ipp.h"
#include "GrainModel_32f.h"
//---------------------------------------------------------------------------

class GrainFilter
{
public:
	static GrainFilter CreateFrom(
					Ipp16u *sourceImage,
					int nRows,
					int nCols,
					int whichChannel,
					Ipp16u maxValue,
					BlobPixelArrayWrapper *blobPixelArrayWrapper,
					MTI_UINT16 pixelFilterMask);

	static GrainFilter CreateFrom(const GrainModel &grainModel);

   bool isNull() { return _grainImage.isEmpty(); }
   void clear() { _grainImage.clear();; }

	bool isThisBlobActuallyGrain(BlobPixelIterator blobPixels, long param);
   bool isThisBlobActuallyGrain(long *blobPixelIndices, long pixelCount, long param);

private:

	Ipp32fArray _grainImage;

	void measureGrain(const GrainModel &grainModel);
};

#endif
