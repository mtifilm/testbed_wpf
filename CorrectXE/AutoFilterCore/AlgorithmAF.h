#ifndef AlgorithmAfH
#define AlgorithmAfH

/*
	AlgorithmAF.h	- header file for AutoFilter algorithm
	Kevin Manbeck
	Oct 24, 2002

	The AlgorithmAF class is used to perform auto filter.  Here is
	the list of public functions.

        NOTE:  These functions should be called in the order described here.

        Initialize ()
		-
		- this is used to initialize the AF class.
		-
		- It should be called from CAutoFilterProc::SetIOConfig()

	SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg)
		-
		- this is used to describe the image characteristics.
		-
		- The lNRowArg and lNColArg are used to describe the total
		- size of the image (including any vertical or horizontal
		- interval).
		-
		- The lActive arguments are used to describe the coordinates
		- of the active portion of the image.  The Start coordinates
        	- are INCLUSIVE.  The Stop coordinates are EXCLUSIVE.
		-
		- It should be called from CAutoFilterProc::SetIOConfig()

	setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel)
		-
		- ucpBlend and ucpLabel are a pointers to arrays indicating how
		- different pixels should be treated.  Set ucpBlend and ucpLabel
		- to NULL to ignore masking.  Or don't call this function to
		- ignore masking.
		-
		- It should be called from CAutoFilterProc::BeginProcessing()

	SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[])
		-
		- lower bound is the smallest value allowed in this format
		- upper bound is the largest value allowed in this format
		-
		- It should be called from CAutoFilterProc::SetIOConfig()

        SetAlphaMatteType(EAlphaMatteType alphatype)
                -
                - sets the type of alpha matte present
                -

	AssignParameter (CAutoFilterParameters *afp)
		-
		- this copies user parameter out of the tool's paramter
		- class into the internal parameter structure.
		-
		- It should be called from CAutoFilterProc::SetParameters().

	SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex)
		-
		- this is used to tell the auto filter algorithm where to find
		- the images to process.
		-
		- uspSrcArg is a list of 3 images.
		- lpFrameIndex is a list of relative frame indices.  Possible
		- values for lpFrameIndex:  (-2, -1, 0), (-1, 0, 1), and
		- (0, 1, 2).  These correspond to past-only, past and future,
		- and future-only.  The decision on which of these three
		- to use should depend on the output of the shot detector.
		-
		- should be called by CAutoFilterProc::DoProcess()

	SetDstImgPtr (MTI_UINT16 *uspDstArg)
		-
		- this is used to tell the auto filter algorithm where
		- to put the results
		-
		- should be called by CAutoFilterProc::DoProcess()

	SetPixelRegionList (CPixelRegionList *prlpArg)
		-
		- this is used to tell the auto filter algorithm where
		- to put original values
		-
		- should be called by CAutoFilterProc::DoProcess()

	Process ()
		-
		- this is used to render the results of auto filter
		-
		- should be called by CAutoFilterProc::DoProcess()
*/

#include "machine.h"
#include "AutoFilterCoreDLL.h"
#include "AutoFilterParameters.h"
#include "BlobIdentifier.h"
#include "GrainFilter.h"
#include "ImageInfo.h"
#include "MTImalloc.h"
#include "mthread.h"
#include <unordered_map>

class CPixelRegionList;
class CBlobProcessor;
struct AF_PreallocedMemory;

#define AF_NUM_SRC_FRAMES 3

#define PREALLOCATE_ALL_MEMORY 1  // Now handles up to 10K files!!!

/************** values of Pixel Mask ***********************************/
#define PM_NONE 0x0
#define PM_EDGE_BLUR_ALPHA     0x000F // 0-15: 0 - all src, 15=all fix
                                 // It is very important that "all src" = 0!!
#define   PM_EDGE_BLUR_ALPHA_ALL_SRC 0x0
#define   PM_EDGE_BLUR_ALPHA_ALL_FIX 0xF
#define   PM_EDGE_BLUR_ALPHA_MASK    0xF

#define PM_DEFECT_EDGE_BLUR    0x0010 // blending pixels outside of fix area
#define PM_DEFECT_SURROUND     0x0020 // non-defect pixels used to find fix
#define PM_DEFECT_IN_NEIGHBOR  0x0040 // possibly defective - replace these
#define PM_DEFECT_IN_PIXEL     0x0080 // clearly defective - replace these

#define PM_DEFECT_FILTER_0     0x0100 // Identify which filter found defect
#define PM_DEFECT_FILTER_1     0x0200
#define PM_DEFECT_FILTER_2     0x0400
#define PM_DEFECT_FILTER_3     0x0800
#define PM_DEFECT_FILTER_4     0x1000
#define PM_DEFECT_FILTER_5     0x2000

#if (AF_MAX_SETTINGS > 6)
You need to add more flags!!
#endif

#define PM_DEFECT_REPAIRED     0x4000 // This pixel was replaced
#define PM_DO_NOT_SKIP         0x8000 // Don't skip this pixel (it's within ROI)
/************** values of Pixel Mask ***********************************/

#define DEFECT_MAX_EDGE_BLUR_STEPS 10
#define DEFECT_EDGE_BLUR_STEPS_PER_PIXEL 2

// Neighbor "inner dim" is always 0
#define DEFECT_NEIGHBOR_OUTER_DIM 3
#define DEFECT_SURROUND_INNER_DIM DEFECT_NEIGHBOR_OUTER_DIM
#define DEFECT_SURROUND_OUTER_DIM (DEFECT_SURROUND_INNER_DIM + 2)
#define DEFECT_EDGE_BLUR_INNER_DIM DEFECT_SURROUND_INNER_DIM
#define DEFECT_EDGE_BLUR_OUTER_DIM \
		  (DEFECT_EDGE_BLUR_INNER_DIM + \
			DEFECT_MAX_EDGE_BLUR_STEPS / DEFECT_EDGE_BLUR_STEPS_PER_PIXEL)

#define DEFECT_NEIGHBOR_OUTER_DIM_ALPHA 1
#define DEFECT_SURROUND_INNER_DIM_ALPHA DEFECT_NEIGHBOR_OUTER_DIM_ALPHA
#define DEFECT_SURROUND_OUTER_DIM_ALPHA (DEFECT_SURROUND_INNER_DIM_ALPHA + 2)
#define DEFECT_EDGE_BLUR_INNER_DIM_ALPHA DEFECT_SURROUND_INNER_DIM_ALPHA
#define DEFECT_EDGE_BLUR_OUTER_DIM_ALPHA \
		  (DEFECT_EDGE_BLUR_INNER_DIM_ALPHA + \
			DEFECT_MAX_EDGE_BLUR_STEPS / DEFECT_EDGE_BLUR_STEPS_PER_PIXEL)

#define MIN_VALUE_FOR_MAX_SIZE 0    // was 10

#if ((DEFECT_SURROUND_OUTER_DIM_ALPHA > DEFECT_SURROUND_OUTER_DIM) \
	|| (DEFECT_EDGE_BLUR_OUTER_DIM_ALPHA > DEFECT_EDGE_BLUR_OUTER_DIM))
THIS MAKES NO SENSE!
#endif

#if (DEFECT_EDGE_BLUR_OUTER_DIM > DEFECT_SURROUND_OUTER_DIM)
#define DEFECT_BIGGEST_DIM DEFECT_EDGE_BLUR_OUTER_DIM
#else
#define DEFECT_BIGGEST_DIM DEFECT_SURROUND_OUTER_DIM
#endif

//inline static bool getDefectNeighborOuterDim(bool detectOnAlpha) { return detectOnAlpha ? DEFECT_NEIGHBOR_OUTER_DIM_ALPHA  : DEFECT_NEIGHBOR_OUTER_DIM;  }
//inline static bool getDefectSurroundInnerDim(bool detectOnAlpha) { return detectOnAlpha ? DEFECT_SURROUND_INNER_DIM_ALPHA  : DEFECT_SURROUND_INNER_DIM;  }
//inline static bool getDefectSurroundOuterDim(bool detectOnAlpha) { return detectOnAlpha ? DEFECT_SURROUND_OUTER_DIM_ALPHA  : DEFECT_SURROUND_OUTER_DIM;  }
//inline static bool getDefectEdgeBlurInnerDim(bool detectOnAlpha) { return detectOnAlpha ? DEFECT_EDGE_BLUR_INNER_DIM_ALPHA : DEFECT_EDGE_BLUR_INNER_DIM; }
//inline static bool getDefectEdgeBlurOuterDim(bool detectOnAlpha) { return detectOnAlpha ? DEFECT_EDGE_BLUR_OUTER_DIM_ALPHA : DEFECT_EDGE_BLUR_OUTER_DIM; }

#define NUM_MTI_UINT16 65436

#define SPATIAL_CONTRAST_OFFSET 10

#define RFT_NONE 0x0
#define RFT_COMPONENT0_ABOVE 0x1        /* the principle component */
#define RFT_COMPONENT0_BELOW 0x2        /* the principle component */
#define RFT_COMPONENT1_BELOW 0x4
#define RFT_COMPONENT1_ABOVE 0x8
#define RFT_COMPONENT2_BELOW 0x10
#define RFT_COMPONENT2_ABOVE 0x20

#define NOMINAL_BLOB_SIZE 10000

#define PAN_ESTIMATE_LENGTH 0.1
#define PAN_ESTIMATE_START 0.1
#define PAN_ESTIMATE_NUM_STEP 20

// allowed blob sizes (in percent of total image size)
#define BLOB_SIZE_0   (0.)                        // value at user setting 0
#define BLOB_SIZE_10  (100./(1920.*1080.))        // value at user setting 10
#define BLOB_SIZE_50  (750./(1920.*1080.))        // value at user setting 50
#define BLOB_SIZE_100 (1.)                        // value at user setting 100

// larger blobs allowed if debris was identified by ALPHA CHANNEL
#define BLOB_SIZE_0_ALPHA   (0.)                   // value at user setting 0
#define BLOB_SIZE_10_ALPHA  (200./(2048.*1556.))   // value at user setting 10
#define BLOB_SIZE_50_ALPHA  (2000./(2048.*1556.))  // value at user setting 50
#define BLOB_SIZE_100_ALPHA (1.)                   // value at user setting 100

// allowed motion tolerance values (in percentage)
#define MOTION_TOL_0 (0.95)
#define MOTION_TOL_50 (0.85)
#define MOTION_TOL_100 (0.65)

// much more tolerant if debris was identified by ALPHA CHANNEL
// UPDATE: I changed this back to match non-ALPHA.
#define MOTION_TOL_0_ALPHA (0.95)
#define MOTION_TOL_50_ALPHA (0.85)
#define MOTION_TOL_100_ALPHA (0.05)

#define MAX_MOTION_THRESH .3  // motion is limited to this portion of image range

// NOTE!!! If you change these, YOU MUST CHANGE MAX_MOTIONS, below
// #define MAX_MOTION_OFFSET_ROW .02	// allowed portion of frame
// #define MAX_MOTION_OFFSET_COL .04	// allowed portion of frame
// Just for grins, let's jack these up by 50%
#define MAX_MOTION_OFFSET_ROW .03	// allowed portion of frame
#define MAX_MOTION_OFFSET_COL .06	// allowed portion of frame

// allowed spatial contrast (in percent of image range)
#define CONTRAST_SPATIAL_0 .3           // value for user setting 0
#define CONTRAST_SPATIAL_50 .15         // value for user setting 50
#define CONTRAST_SPATIAL_100 0.         // value for user setting 100

// allowed temporal contrast (in percent of image range)
#define CONTRAST_TEMPORAL_0 .3          // value for user setting 0
#define CONTRAST_TEMPORAL_50 .15        // value for user setting 50
#define CONTRAST_TEMPORAL_100 0.        // value for user setting 100

// allowed std error values
#define STANDARD_ERROR_0 10.		// value for user setting 0
#define STANDARD_ERROR_50 7.		// value for user setting 50
#define STANDARD_ERROR_100 4.		// value for user setting 100

const long maxLong(0x7FFFFFFF);
const long minLong(0x80000000);
//------------------------------------------------------------------------------

// This is needed so arg gets evaluated only once
inline float FSQR(float fArg)
{
   return (fArg * fArg);
}

inline long LSQR(long lArg)
{
   return (lArg * lArg);
}

inline long LABS(long lArg)
{
   return ((lArg < 0)? -lArg : lArg);
}
inline long LEDGEDIST(long lRC, long lInnerDim)
{
   // This is for edge blending:
   // We have concentric rects and are trying to determine the distance
   // of a point that is in the outer square but not in the inner square
   // from the edge of the inner square

   long dist1 = LABS(lRC - lInnerDim);   // right/bottom inner edge distance
   long dist2 = LABS((-lInnerDim) - lRC);  // top/left inner edge distance
   // return the min distance
   return ((dist1 < dist2)? dist1 : dist2);
}
//------------------------------------------------------------------------------

struct SBoundRect
{
   // Right and Bottom are INCLUSIVE!!
   long top, right, bottom, left;

   SBoundRect()
   : top(maxLong)
   , right(minLong)
   , bottom(minLong)
   , left(maxLong)
   {};

   inline long getWidth()   const { return (right - left + 1); };
   inline void setWidth(long newWidth)
   {
      left -= (newWidth - getWidth())/2;
      right += newWidth - getWidth();
   }
   inline long getHeight()  const { return (bottom - top + 1); };
   inline void setHeight(long newHeight)
   {
      top -= (newHeight - getHeight())/2;
      bottom += newHeight - getHeight();
   }
   inline bool isValid() const { return (right != minLong); };
   inline void addPoint(long x, long y)
   {
      if (y < top)    top = y;
      if (x > right)  right = x;
      if (y > bottom) bottom = y;
      if (x < left)   left = x;
   };
   inline void getCenterXY(long &x, long &y) const
   {
      x = left + (getWidth() / 2);
      y = top + (getHeight() / 2);
   };
   inline RECT getInclRECT()
   {
      RECT retVal;
      retVal.top    = top;
      retVal.bottom = bottom;
      retVal.left   = left;
      retVal.right  = right;
      return retVal;
   };
   inline RECT getExclRECT()
   {
      RECT retVal;
      retVal.top    = top;
      retVal.bottom = bottom + 1;   // make exclusive
      retVal.left   = left;
      retVal.right  = right + 1;    // make exclusive
      return retVal;
   };
   inline void clip(long minX, long maxX, long minY, long maxY)
   {
      if (minY > top)    top = minY;
      if (maxX < right)  right = maxX;
      if (maxY < bottom) bottom = maxY;
      if (minX > left)   left = minX;
   };
   inline bool contains(long x, long y)
   {
      return (x >= left && x <= right && y >= top && y <= bottom);
   }
};
//------------------------------------------------------------------------------

// Grain filters for R, G, B, and Y
struct GrainFilterSet
{
   GrainFilter grainFilter[AF_MAX_COMPONENTS + 1];

   GrainFilter &operator[](int index)
   {
      MTIassert(index >= 0 && index <= AF_MAX_COMPONENTS);
      return grainFilter[index];
   }

   void clear()
   {
      for (auto index = 0; index <= AF_MAX_COMPONENTS; ++index)
      {
         grainFilter[index].clear();
      }
   }
};
//------------------------------------------------------------------------------

struct POINTER_STRUCT
 {
                  /* pointers to data */
  MTI_UINT16
    *uspCurr0,          /* component 0 at current time */
    *uspCurr1,          /* component 1 at current time */
    *uspCurr2,          /* component 2 at current time */
    *uspRefA0,          /* component 0 at reference time A, usually time - 1 */
    *uspRefA1,          /* component 1 at reference time A, usually time - 1 */
    *uspRefA2,          /* component 2 at reference time A, usually time - 1 */
    *uspRefB0,          /* component 0 at reference time B, usually time + 1 */
    *uspRefB1,          /* component 1 at reference time B, usually time + 1 */
    *uspRefB2;          /* component 2 at reference time B, usually time + 1 */

  MTI_UINT16
    *uspCurrN,		/* component 0, north */
    *uspCurrS,		/* component 0, south */
    *uspCurrE,		/* component 0, east */
    *uspCurrW,		/* component 0, west */
    *uspCurrNE,		/* component 0, north-east */
    *uspCurrNW,		/* component 0, north-west */
    *uspCurrSE,		/* component 0, south-east */
    *uspCurrSW,		/* component 0, south-west */
    *uspRefAN,		/* component 0, north */
    *uspRefAS,		/* component 0, south */
    *uspRefAE,		/* component 0, east */
    *uspRefAW,		/* component 0, west */
    *uspRefBN,		/* component 0, north */
    *uspRefBS,		/* component 0, south */
    *uspRefBE,		/* component 0, east */
    *uspRefBW;		/* component 0, west */

    void dump() const
    {
       DBTRACE("========POINTER_STRUCT============");
       DBTRACE(uspCurr0);
       DBTRACE(uspCurr1);
       DBTRACE(uspCurr2);
       DBTRACE(uspRefA0);
       DBTRACE(uspRefA1);
       DBTRACE(uspRefA2);
       DBTRACE(uspRefB0);
       DBTRACE(uspRefB1);
       DBTRACE(uspRefB2);
       DBTRACE(uspCurrN);
       DBTRACE(uspCurrS);
       DBTRACE(uspCurrE);
       DBTRACE(uspCurrW);
       DBTRACE(uspCurrNE);
       DBTRACE(uspCurrNW);
       DBTRACE(uspCurrSE);
       DBTRACE(uspCurrSW);
       DBTRACE(uspRefAN);
       DBTRACE(uspRefAS);
       DBTRACE(uspRefAE);
       DBTRACE(uspRefAW);
       DBTRACE(uspRefBN);
       DBTRACE(uspRefBS);
       DBTRACE(uspRefBE);
       DBTRACE(uspRefBW);
    }
 };
//------------------------------------------------------------------------------

struct AUTO_FILTER_SETTING
 {
//  NOTE:  when adding new items to this structure, remember to update
//  CompareParameter() function.

  long
    lPrincipalComponent,
    lActiveComponent,
    lComponentMask,
    lGrainFilterComponent;
  bool
    bUseAlphaMatteToFindDebris;

  long
    laMinVal[AF_MAX_COMPONENTS],	// threshold on range
    laMaxVal[AF_MAX_COMPONENTS],	// threshold on range
    lContrastDirection,
    lMotionOffsetRow,
    lMotionOffsetCol,
    lMotionThresh,
    lMaxSizeThresh,
    lMinSizeThresh;

  float
    fContrastSpatial,
    fContrastTemporal,
    fMotionTol;

  bool
    bUseContrastTemporal,
    bUseContrastSpatial,
	 bUseMotion;

  float
    fEdgeBlur;

  bool
    bUseHardMotionOnDust;
  long
    lDustMaxSize;

  bool bUseGrainFilter;
  long lGrainFilterParam;

  float fLuminanceThreshold;

  int filterChangeCounter;

  long
    lFrameIndex;

//  NOTE:  when adding new items to this structure, remember to update
//  CompareParameter() function.

  void dump() const
  {
   DBCOMMENT("========AUTO_FILTER_SETTING============");
   DBTRACE(lPrincipalComponent);
   DBTRACE(lActiveComponent);
   DBTRACE(lComponentMask);
   DBTRACE(lGrainFilterComponent);
   DBTRACE(bUseAlphaMatteToFindDebris);
   DBTRACE(laMinVal[0]);
   DBTRACE(laMaxVal[0]);
   DBTRACE(lContrastDirection);
   DBTRACE(lMotionOffsetRow);
   DBTRACE(lMotionOffsetCol);
   DBTRACE(lMotionThresh);
   DBTRACE(lMaxSizeThresh);
   DBTRACE(lMinSizeThresh);
   DBTRACE(fContrastSpatial);
   DBTRACE(fContrastTemporal);
   DBTRACE(fMotionTol);
   DBTRACE(bUseContrastTemporal);
   DBTRACE(bUseContrastSpatial);
   DBTRACE(bUseMotion);
   DBTRACE(fEdgeBlur);
   DBTRACE(bUseHardMotionOnDust);
   DBTRACE(lDustMaxSize);
   DBTRACE(bUseGrainFilter);
   DBTRACE(lGrainFilterParam);
   DBTRACE(fLuminanceThreshold);
   DBTRACE(filterChangeCounter);
   DBTRACE(lFrameIndex);
   DBCOMMENT("====================");
  }
 };
//------------------------------------------------------------------------------

struct AUTO_FILTER_ALGORITHM
 {
  MTI_UINT16
    *uspSpatialContrastThresh,
    *uspTemporalContrastThresh;

#define MOTION_PAST 0
#define MOTION_PAST_ONLY 1
#define MOTION_FUTURE 2
#define MOTION_FUTURE_ONLY 3
#define NUM_MOTION_INDEX 4
  long
    lNMotion,
    *lpMotionRCC[NUM_MOTION_INDEX],
    *lpMotionRCCA,
    *lpMotionRCCB;

  bool
    bSurroundUseSelf,
    bSurroundUseA,
    bSurroundUseB;

  long
    lSurroundMotionA,
    lSurroundMotionB;

  POINTER_STRUCT
    ps;

  AUTO_FILTER_ALGORITHM()
  : uspSpatialContrastThresh(NULL),
    uspTemporalContrastThresh(NULL),
    lNMotion(0),
    lpMotionRCCA(NULL),
    lpMotionRCCB(NULL),
    bSurroundUseSelf(false),
    bSurroundUseA(false),
    bSurroundUseB(false),
    lSurroundMotionA(0),
    lSurroundMotionB(0)
  {
     for (int i = 0; i < NUM_MOTION_INDEX; ++i)
      {
       lpMotionRCC[i] = NULL;
      }
     memset(&ps, 0, sizeof(ps));
  }

  void dump() const
  {
    DBTRACE("========AUTO_FILTER_ALGORITHM============");
    DBTRACE(*uspSpatialContrastThresh);
    DBTRACE(*uspTemporalContrastThresh);
    DBTRACE(lNMotion);
    DBTRACE(*lpMotionRCC[0]);
    DBTRACE(*lpMotionRCCA);
    DBTRACE(*lpMotionRCCB);
    DBTRACE(bSurroundUseSelf);
    DBTRACE(bSurroundUseA);
    DBTRACE(bSurroundUseB);
    DBTRACE(lSurroundMotionA);
    DBTRACE(lSurroundMotionB);
    ps.dump();
    DBTRACE("====================");
  }
 };

////------------------------------------------------------------------------------
//
//class CAutoFilterParameters
//{
//public:
//	CAutoFilterParameters() {};
//   ~CAutoFilterParameters() {};
//
//	void InitGeneral ()
//    {
//     // set to default values
//	  lNSetting = 0;
//	 }
//
//	void InitSetting (int iSetting)
//    {
//     // increment number of settings
//     lNSetting++;
//
//     lPrincipalComponent[iSetting] = -1;     // "do the right thing"
//     lComponentMask[iSetting] = 0x7;
//     bUseAlphaMatteToFindDebris[iSetting] = false;
//
//     // set to default values
//     faMinVal[iSetting][0] = 0.;
//     faMaxVal[iSetting][0] = 100.;
//     faMinVal[iSetting][1] = 0.;
//     faMaxVal[iSetting][1] = 100.;
//     faMinVal[iSetting][2] = 0.;
//     faMaxVal[iSetting][2] = 100.;
//
//     faContrastSpatial[iSetting] = 50.;
//     faContrastTemporal[iSetting] = 50.;
//     laContrastDirection[iSetting] = 0;
//
//	  faMotionOffsetRow[iSetting] = 10.;
//     faMotionOffsetCol[iSetting] = 10.;
//     faMotionThresh[iSetting] = 50.;
//
//     faMaxSize[iSetting] = 50.;
//     faMinSize[iSetting] = 0.;
//
//	  bUseHardMotionOnDust[iSetting] = false;
//	  lDustMaxSize[iSetting] = 10;
//
//     bUseGrainFilter[iSetting] = false;
//     lGrainFilterParam[iSetting] = 50;
//     fLuminanceThreshold[iSetting] = 100;
//
//     strSettingName[iSetting] = "";
//    }
//
//    void dump()
//    {
//      DBTRACE("=================");
//      DBTRACE(lNSetting);
//      for (auto iSetting = 0; iSetting < lNSetting; ++iSetting)
//      {
//        DBTRACE("=================");
//        DBTRACE(iSetting);
//
//        DBTRACE(lPrincipalComponent[iSetting]);
//        DBTRACE(lComponentMask[iSetting]);
//        DBTRACE(bUseAlphaMatteToFindDebris[iSetting]);
//
//        DBTRACE(faMinVal[iSetting][0]);
//        DBTRACE(faMaxVal[iSetting][0]);
//        DBTRACE(faMinVal[iSetting][1]);
//        DBTRACE(faMaxVal[iSetting][1]);
//        DBTRACE(faMinVal[iSetting][2]);
//        DBTRACE(faMaxVal[iSetting][2]);
//
//        DBTRACE(faContrastSpatial[iSetting]);
//        DBTRACE(faContrastTemporal[iSetting]);
//        DBTRACE(laContrastDirection[iSetting]);
//
//        DBTRACE(faMotionOffsetRow[iSetting]);
//        DBTRACE(faMotionOffsetCol[iSetting]);
//        DBTRACE(faMotionThresh[iSetting]);
//
//        DBTRACE(faMaxSize[iSetting]);
//        DBTRACE(faMinSize[iSetting]);
//
//        DBTRACE(bUseHardMotionOnDust[iSetting]);
//        DBTRACE(lDustMaxSize[iSetting]);
//
//        DBTRACE(bUseGrainFilter[iSetting]);
//        DBTRACE(lGrainFilterParam[iSetting]);
//
//        DBTRACE(strSettingName[iSetting]);
//      }
//      DBTRACE("=================");
//    }
//
//   //
//   //  parameters to control rounds of filtering
//   //
//   long
//     lNSetting;
//
//	//
//   //	parameters to control component processing
//   //
//   long
//     lPrincipalComponent[AF_MAX_SETTINGS], // which is most important component
//     lComponentMask[AF_MAX_SETTINGS];      // which components to process
//   bool
//     bUseAlphaMatteToFindDebris[AF_MAX_SETTINGS]; // look for debris in alpha
//
//   //
//   //	parameters to control image value range
//   //
//   float
//     faMinVal[AF_MAX_SETTINGS][AF_MAX_COMPONENTS],	// range: 0% to 100%
//     faMaxVal[AF_MAX_SETTINGS][AF_MAX_COMPONENTS];	// range: 0% to 100%
//
//   //
//   //  parameters to control contrast
//   //
//   float
//     faContrastSpatial[AF_MAX_SETTINGS],		// range: 0% to 100%
//     faContrastTemporal[AF_MAX_SETTINGS];		// range: 0% to 100%
//   long
//     laContrastDirection[AF_MAX_SETTINGS];		// range: -1, 0, 1
//
//   //
//   //  parameters to control motion
//   //
//   float
//     faMotionOffsetRow[AF_MAX_SETTINGS],		// range 0% to 100%
//     faMotionOffsetCol[AF_MAX_SETTINGS],		// range 0% to 100%
//     faMotionThresh[AF_MAX_SETTINGS];		// range 0% to 100%
//
//   //
//   //  parameters to control size of defect
//   //
//   float
//     faMaxSize[AF_MAX_SETTINGS];			// range 0% to 100%
//   float
//     faMinSize[AF_MAX_SETTINGS];			// range 0% to 100%
//
//   //
//   // Edge blur for fixes
//   //
//   float
//     faEdgeBlur[AF_MAX_SETTINGS];           // range 0% to 100%
//
//   //
//   // Special case "dust" (small debris)
//   //
//   bool
//	  bUseHardMotionOnDust[AF_MAX_SETTINGS];
//   long
//	  lDustMaxSize[AF_MAX_SETTINGS];        // total debris pixel count
//
//   // Grain filter
//   bool
//      bUseGrainFilter[AF_MAX_SETTINGS];
//   long
//      lGrainFilterParam[AF_MAX_SETTINGS];
//   long
//      lLuminanceThreshold[AF_MAX_SETTINGS];
//
//	//
//	// Info
//	//
//	string
//		strSettingName[AF_MAX_SETTINGS];
//
//   // Dummy parameter than can be used to force a reset of the AF algorithm
//   int filterChangeCounter;
//
//   //TEST - frame index
//   long
//     lFrameIndex;
//
//   //
//
//};
////------------------------------------------------------------------------------

struct SNeighborhood
{
  long lNNeighbor;
  long *lpNeighbor;
  long lNSurround;
  long *lpSurround;
  long lNEdgeBlur;
  long *lpEdgeBlur;
  long *lpFixAlpha;

  long lNColBasis;

  SNeighborhood();
  ~SNeighborhood();
  int Allocate(long lArgNNeighbor, long lArgNSurround, long lArgEdgeBlur);

private:

  long *allocatedMemory;

};
//------------------------------------------------------------------------------

struct MotionCacheMapEntry
{
   MTI_INT32 MotionA = INVALID_MOTION;
   MTI_INT32 MotionB = INVALID_MOTION;

   static MTI_INT32 INVALID_MOTION;
};

typedef std::unordered_map<long, MotionCacheMapEntry> MotionCacheMap;
//------------------------------------------------------------------------------

struct StatsAF
{
	int totalBlobsDetected = 0;
	int totalBlobsAfterConsolidation = 0;
	int totalBlobsAfterSizeRangeCheck = 0;
	int sizeOfLargestBlobDetected = 0;
	int sizeOfLargestBlobAfterConsolidation = 0;
	int sizeOfLargestBlobAfterSizeRangeCheck = 0;
	int sizeOfSmallestBlobDetected = 0x7FFFFFFF;
	int sizeOfSmallestBlobAfterConsolidation = 0x7FFFFFFF;
	int sizeOfSmallestBlobAfterSizeRangeCheck = 0x7FFFFFFF;
	int sizeRangeMin = 0;
	int sizeRangeMax = 0;
	bool blobCountLimitExceeded = false;
	int totalProcessCandidateBlobs = 0;
	int totalTooSmallBlobs = 0;
	int totalTooBigBlobs = 0;
	int totalTooBrightBlobs = 0;
   int totalBlobsIdentifiedAsGrain = 0;
	int totalBlobsActuallyProcessed = 0;
	int totalDustBlobsInpainted = 0;
	int finalSizeOfLargestBlob = 0;
	int finalSizeOfSmallestBlob = 0x7FFFFFFF;
	string settingName;

//	void MergeBlobIsolatorStats(const CBlobIsolator::BlobIsolatorStats &BIStats)
//	{
//		totalBlobsDetected = BIStats.totalBlobsDetected;
//		totalBlobsAfterConsolidation = BIStats.totalBlobsAfterConsolidation;
//		totalBlobsAfterSizeRangeCheck = BIStats.totalBlobsAfterSizeRangeCheck;
//		sizeOfLargestBlobDetected = BIStats.sizeOfLargestBlobDetected;
//		sizeOfLargestBlobAfterConsolidation = BIStats.sizeOfLargestBlobAfterConsolidation;
//		sizeOfLargestBlobAfterSizeRangeCheck = BIStats.sizeOfLargestBlobAfterSizeRangeCheck;
//		sizeOfSmallestBlobDetected = BIStats.sizeOfSmallestBlobDetected;
//		sizeOfSmallestBlobAfterConsolidation = BIStats.sizeOfSmallestBlobAfterConsolidation;
//		sizeOfSmallestBlobAfterSizeRangeCheck = BIStats.sizeOfSmallestBlobAfterSizeRangeCheck;
//		sizeRangeMin = BIStats.sizeRangeMin;
//		sizeRangeMax = BIStats.sizeRangeMax;
//	};

	void Clear()
	{
		StatsAF clear;
		*this = clear;
	}
};
//------------------------------------------------------------------------------

struct AFProgressReport
{
   int phase = 0;
   int defectCount = 0;
   int defectivePixelCount = 0;
   int fixedDefectCount = 0;
   int fixedPixelCount = 0;
   int ignoredDefectCount = 0;
   int ignoredDefectivePixelCount = 0;
};
//------------------------------------------------------------------------------

class MTI_AUTOFILTERCOREDLL_API CAlgorithmAF
 {
public:
  CAlgorithmAF();
  ~CAlgorithmAF();

  void Initialize (bool bThreadInternally,
						 AF_PreallocedMemory *pPreallocedmemoryArg);
  int SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg);
  int SetMaskPointer (MTI_UINT16 *uspMask=NULL);
  int SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[]);
  int SetAlphaMatteType(EAlphaMatteType alphatype, bool isHalfFloats);
  int AssignParameter (CAutoFilterParameters *afp);
  int SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex);
  void SetDstImgPtr (MTI_UINT16 *uspDstArg);
  void SetPixelRegionList (CPixelRegionList *prlpArg);
  void SetModifiedPixelMap(MTI_UINT8 *modifiedPixelMap);
  int Process (GrainFilterSet &grainFilters);
  Ipp8uArray MakeDetectionMap(
      GrainFilterSet &grainFilters,
      BlobPixelArrayWrapper **defectBlobPixelArrayWrapperPtrPtr);
  void setPrintOut (bool newPrintFlag);

  void setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel);

  void SetEmergencyStopFlagPtr(const bool *newEmergencyStopFlagPtr);
  void GetStats(StatsAF *afStats) const;

private:
  //
  //  General purpose values
  //
  long
    lNRow,		// vertical dimension of image
    lNCol,		// horizontal dimension of image
    lNCom,		// number of components
    lActiveStartRow,	// location of active picture:  INCLUSIVE
    lActiveStopRow,	// location of active picture:  EXCLUSIVE
	 lActiveStartCol,	// location of active picture:  INCLUSIVE
    lActiveStopCol,	// location of active picture:  EXCLUSIVE
    lNRowCol,		// total number of pixels
    lNRowColCom,	// total number of pixel*components
    laLowerBound[AF_MAX_COMPONENTS],	// minimum allowed image value
	 laUpperBound[AF_MAX_COMPONENTS];	// maximum allowed image value

  EAlphaMatteType
    eAlphaMatteType;

  bool
    bDoDeferredDimensionSetup,
    bNeedToSetupPixelMask,
	 bAlphaDataIsHalfFloats = false,
	 _bMakingDetectionMapOnly = false,
    _detectionStuffIsValid = false,
    _motionStuffIsValid = false,
    _fixStuffIsValid = false;

  //
  //  Parameter settings to control behavior of algorithm.  These can
  //  be changed as the algorithm runs.
  //
  AUTO_FILTER_SETTING
    *afspSetting;
  long
    lNSetting;

  //
  //  Storage used internally
  //
  AUTO_FILTER_ALGORITHM
    *afapAlgorithm;		// one for each setting

  MTI_UINT16
    *uspPixelMask;

  long
    lPixelMaskPadding;

  long
//	 *lpBlobPixelArray,
	 *lpBlobRC,
    lNPixelsInBlob,
    lNPixelsInBiggestAllowedBlob,
    *lpPanBlobRC,
    lNPanBlob;

	BlobPixel
      *_blobPixelArray;

  long    // preallocated temp lists
    *lpUnexclRowColComLists,
    *lpMotionDiffs[2];

  bool
    bBlobPixelArrayWasMalloced,
    bBlobLabelArrayWasMalloced,
    bBlobRCWasMalloced,
    bUnexclRowColComListWasMalloced,
    bPixelMaskWasMalloced;

  long
    lPanMotionA,
    lPanMotionB,
    lMinPanMotion,
    lMaxPanMotion;

  MotionCacheMap _bestMotionCacheMap;
  MTI_UINT8 *_modifiedPixelMap;

  MTI_UINT16
    *uspSrc[AF_NUM_SRC_FRAMES],
    *uspDst;

  CPixelRegionList
    *prlpOriginalValues;

  GrainFilterSet _grainFilters;
  BlobPixelArrayWrapper **_precomputedBlobPixelArrayWrapperPtrPtr;
  BlobPixelArrayWrapper **_consolidatedBlobPixelArrayWrapperPtrPtr;
  Ipp8uArray _detectionMap;

  CBlobProcessor
    *bppBlobProc;

  void FreeEverything();
  int AllocAlgorithm (long lNSettingArg);
  void FreeAlgorithm ();
  void FreeGeneral ();
  void CopyParameter (CAutoFilterParameters *app, AUTO_FILTER_SETTING *afsp, long lSetting);
  bool CompareParameter (AUTO_FILTER_SETTING *afsp1, AUTO_FILTER_SETTING *afsp2);
  void CompareParameter(
      AUTO_FILTER_SETTING *afsp1,
      AUTO_FILTER_SETTING *afsp2,
      bool &detectionInvalidated,
      bool &motionInvalidated,
      bool &fixesInvalidated);
  int SetupAlgorithm (long lSetting);
  int SetupContrast (long lSetting);
  int SetupPerSettingMotion (long lSetting);
  int AllocPixelMask ();
  void SetupPixelMask ();
  int SetupBlob ();
  int SetupNeighbors ();
  int SetupOneNeighborhood(long lHood);
  void SetupOneNeighborhood(
			SNeighborhood *neighborhood, long lHood,
			long lNeighborOuterDim, long lSurroundInnerDim,
			long lSurroundOuterDim, long lEdgeBlurInnerDim);
  void AssignPointers (long lSetting);
  int AnalyzeBlobs (long lSetting, BlobPixelArrayWrapper &blobPixelArrayWrapper);
  bool ProcessBlob (long lSetting);
  void ResetPixelMask ();
  int PixelMaskType (long lSetting);

	void AlphaFcn (long lSetting);
  void SetPixelMaskFromAlphaChannel(long lSetting);
  void RangeFcn (long lSetting);
  void SpatialRangeFcn (long lSetting, long lRangeFcnType,
        long lMinVal0, long lMaxVal0, long lMinVal1, long lMaxVal1,
        long lMinVal2, long lMaxVal2);
  void TemporalRangeFcn (long lSetting, long lRangeFcnType,
        long lMinVal0, long lMaxVal0, long lMinVal1, long lMaxVal1,
        long lMinVal2, long lMaxVal2);
  void ContrastFcn (long lSetting, long lRowCol, long lPixelMask);
  void IdentifyBlobs(BlobPixelArrayWrapper &blobPixelArrayWrapper);
  void FilterDefectPixelBlobs(long lSetting);

  StatsAF
	 _afStats[AF_MAX_SETTINGS];

  bool
    bPrintOut;

  const MTI_UINT8
	 *ucpROIBlend;

  MTI_UINT8
    *ucpROILabel;

  /*
    For computing neighborhoods - SHARED BY ALL THREADS -
    +1 is because it's 0 through max steps
  */
// Breaks the build!
//static SNeighborhood M_Neighborhood[DEFECT_MAX_EDGE_BLUR_STEPS + 1];
  static SNeighborhood *M_Neighborhood;
  static SNeighborhood *M_Neighborhood_Alpha;
  static CSpinLock M_GlobalNeighborhoodLock;

  AF_PreallocedMemory *pPreallocedMemory;

  /*
     For aborting
  */
  const bool *EmergencyStopFlagPtr;
 };
//------------------------------------------------------------------------------

#define ROW_COL_2K  (2048 * 1556)
#define ROW_COL_4K  (ROW_COL_2K *  4)
#define ROW_COL_6K  (ROW_COL_2K *  9)
#define ROW_COL_8K  (ROW_COL_2K * 16)
#define ROW_COL_10K (ROW_COL_2K * 25)

// The following depends on MAX_MOTION_OFFSET_ROW and MAX_MOTION_OFFSET_COL
//-----------old-------------------
// MAX_MOTION_OFFSET_ROW = 0.02
// MAX_MOTION_OFFSET_COL = 0.04
// MotionOffsetRow = 3134 * .02 = 62.68 // 3112 * .02 = 62.24
// MotionOffsetCol = 4096 * .04 = 163.84
// MAX_MOTIONS = (2 * 63 + 1) * (2 * 164 + 1) = 41783
//-----------NEW-10k-max-----------
// MAX_MOTION_OFFSET_ROW = 0.02
// MAX_MOTION_OFFSET_COL = 0.04
// MotionOffsetRow = 7750 * .02 = 155.6
// MotionOffsetCol = 10240 * .04 = 409.6
// MAX_MOTIONS = (2 * 156 + 1) * (2 * 410 + 1) = 256973
//---------------------------------
#define MAX_MOTIONS 385460  // we jacked up the numbers by 50% (see above)

// OK, I have no idea about this padding business
#define MAX_PIXEL_MASK_PADDING_PIXELS_2K (DEFECT_BIGGEST_DIM * (2048 + 1))
#define MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_2K (ROW_COL_2K + (2 * MAX_PIXEL_MASK_PADDING_PIXELS_2K))

#define MAX_PIXEL_MASK_PADDING_PIXELS_4K (DEFECT_BIGGEST_DIM * (4096 + 1))
#define MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_4K (ROW_COL_4K + (2 * MAX_PIXEL_MASK_PADDING_PIXELS_4K))

#define MAX_PIXEL_MASK_PADDING_PIXELS_6K (DEFECT_BIGGEST_DIM * (6144 + 1))
#define MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_6K (ROW_COL_6K + (2 * MAX_PIXEL_MASK_PADDING_PIXELS_6K))

#define MAX_PIXEL_MASK_PADDING_PIXELS_8K (DEFECT_BIGGEST_DIM * (8192 + 1))
#define MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_8K (ROW_COL_8K + (2 * MAX_PIXEL_MASK_PADDING_PIXELS_8K))

#define MAX_PIXEL_MASK_PADDING_PIXELS_10K (DEFECT_BIGGEST_DIM * (10240 + 1))
#define MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_10K (ROW_COL_10K + (2 * MAX_PIXEL_MASK_PADDING_PIXELS_10K))
//------------------------------------------------------------------------------

// Memory preallocation hack - layout of flying spaghetti buffer

// This stuff should be set up once before processing, then it is the
// same for all frames regardless of size

struct AF_PreallocedMemory
{
    struct AF_PreallocedMemory_PerSetting
    {
       MTI_UINT16 usSpatialContrastThresh[NUM_MTI_UINT16];
       MTI_UINT16 usTemporalContrastThresh[NUM_MTI_UINT16];

       struct SPerMotion
       {
          long lMotionRCC[MAX_MOTIONS];

       } perMotion[NUM_MOTION_INDEX];

    } perSetting[AF_MAX_SETTINGS];

  // Temp stuff, not preserved between frames - same size for all frame sizes

  long        lMotionDiffs[2][MAX_MOTIONS];

  // Address accessors
  virtual ~AF_PreallocedMemory() {};
  virtual MTI_UINT16 * getSpatialContrastThreshAddr(long lSetting)
          { return &perSetting[lSetting].usSpatialContrastThresh[0]; };
  virtual MTI_UINT16 * getTemporalContrastThreshAddr(long lSetting)
			 { return &perSetting[lSetting].usTemporalContrastThresh[0]; };
  virtual long * getMotionRCCAddr(long lSetting, long lIndex)
          { return &perSetting[lSetting].perMotion[lIndex].lMotionRCC[0]; };
  virtual long * getMotionDiffsAddr(long which)
          { return &lMotionDiffs[which][0]; };

  // These depend on the frame size - handled in subtypes
  virtual MTI_UINT16 * getPixelMask() = 0;
  virtual BlobPixel * getBlobPixelArrayAddr() = 0;
  virtual long * getBlobRCAddr() = 0;
  virtual long * getUnexclRowColComLists() = 0;
  virtual size_t getTotalSize() const = 0;
};
//------------------------------------------------------------------------------

 // Frame-size-dependent stuff - 2K and smaller version

struct AF_PreallocedMemory_2K : public AF_PreallocedMemory
{
	// Temp stuff, not preserved between frames - 2K sizes

	MTI_UINT16  usPixelMask[MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_2K];

	BlobPixel   blobPixelArray[ROW_COL_2K];
	long        lBlobRC[ROW_COL_2K];
	long        lUnexclRowColComLists[ROW_COL_2K];

  virtual ~AF_PreallocedMemory_2K() {};
  virtual MTI_UINT16 * getPixelMask()
			 { return &usPixelMask[0]; };
  virtual BlobPixel * getBlobPixelArrayAddr()
			 { return &blobPixelArray[0]; };
  virtual long * getBlobRCAddr()
          { return &lBlobRC[0]; };
  virtual long * getUnexclRowColComLists()
          { return &lUnexclRowColComLists[0]; };
  virtual size_t getTotalSize() const
          { return sizeof(AF_PreallocedMemory_2K); }
};
//------------------------------------------------------------------------------

 // Frame-size-dependent stuff - 4K version

struct AF_PreallocedMemory_4K : public AF_PreallocedMemory
{
   // Temp stuff, not preserved between frames - 4K sizes

   MTI_UINT16  usPixelMask[MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_4K];

	BlobPixel   blobPixelArray[ROW_COL_4K];
	long        lBlobRC[ROW_COL_4K];
   long        lUnexclRowColComLists[ROW_COL_4K];

  virtual ~AF_PreallocedMemory_4K() {};
  virtual MTI_UINT16 * getPixelMask()
          { return &usPixelMask[0]; };
  virtual BlobPixel * getBlobPixelArrayAddr()
			 { return &blobPixelArray[0]; };
  virtual long * getBlobRCAddr()
          { return &lBlobRC[0]; };
  virtual long * getUnexclRowColComLists()
          { return &lUnexclRowColComLists[0]; };
  virtual size_t getTotalSize() const
          { return sizeof(AF_PreallocedMemory_4K); }
};
//------------------------------------------------------------------------------

 // Frame-size-dependent stuff - 6K version

struct AF_PreallocedMemory_6K : public AF_PreallocedMemory
{
	// Temp stuff, not preserved between frames - 6K sizes

	MTI_UINT16  usPixelMask[MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_6K];

	BlobPixel   blobPixelArray[ROW_COL_6K];
	long        lBlobRC[ROW_COL_6K];
	long        lUnexclRowColComLists[ROW_COL_6K];

  virtual ~AF_PreallocedMemory_6K() {};
  virtual MTI_UINT16 * getPixelMask()
			 { return &usPixelMask[0]; };
  virtual BlobPixel * getBlobPixelArrayAddr()
			 { return &blobPixelArray[0]; };
  virtual long * getBlobRCAddr()
			 { return &lBlobRC[0]; };
  virtual long * getUnexclRowColComLists()
			 { return &lUnexclRowColComLists[0]; };
  virtual size_t getTotalSize() const
          { return sizeof(AF_PreallocedMemory_6K); }
};
//------------------------------------------------------------------------------

 // Frame-size-dependent stuff - 8K version

struct AF_PreallocedMemory_8K : public AF_PreallocedMemory
{
	// Temp stuff, not preserved between frames - 8K sizes

	MTI_UINT16  usPixelMask[MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_8K];

	BlobPixel   blobPixelArray[ROW_COL_8K];
	long        lBlobRC[ROW_COL_8K];
	long        lUnexclRowColComLists[ROW_COL_8K];

  virtual ~AF_PreallocedMemory_8K() {};
  virtual MTI_UINT16 * getPixelMask()
			 { return &usPixelMask[0]; };
  virtual BlobPixel * getBlobPixelArrayAddr()
			 { return &blobPixelArray[0]; };
  virtual long * getBlobRCAddr()
			 { return &lBlobRC[0]; };
  virtual long * getUnexclRowColComLists()
			 { return &lUnexclRowColComLists[0]; };
  virtual size_t getTotalSize() const
          { return sizeof(AF_PreallocedMemory_8K); }
};
//------------------------------------------------------------------------------

 // Frame-size-dependent stuff - 10K version

struct AF_PreallocedMemory_10K : public AF_PreallocedMemory
{
	// Temp stuff, not preserved between frames - 10K sizes

	MTI_UINT16  usPixelMask[MAX_PIXEL_MASK_PADDED_PIXEL_COUNT_10K];

	BlobPixel   blobPixelArray[ROW_COL_10K];
	long        lBlobRC[ROW_COL_10K];
	long        lUnexclRowColComLists[ROW_COL_10K];

  virtual ~AF_PreallocedMemory_10K() {};
  virtual MTI_UINT16 * getPixelMask()
			 { return &usPixelMask[0]; };
  virtual BlobPixel * getBlobPixelArrayAddr()
			 { return &blobPixelArray[0]; };
  virtual long * getBlobRCAddr()
			 { return &lBlobRC[0]; };
  virtual long * getUnexclRowColComLists()
			 { return &lUnexclRowColComLists[0]; };
  virtual size_t getTotalSize() const
          { return sizeof(AF_PreallocedMemory_10K); }
};
//------------------------------------------------------------------------------

#endif
