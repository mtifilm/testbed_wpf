#ifndef ERR_AUTO_FILTER
#define ERR_AUTO_FILTER

// allowed range for error values:  -5049 -- 5000

#define ERR_PLUGIN_AF_BAD_VERTICAL_DIMENSION   -5000
#define ERR_PLUGIN_AF_BAD_HORIZONTAL_DIMENSION -5001
#define ERR_PLUGIN_AF_BAD_COMPONENT_DIMENSION  -5002
#define ERR_PLUGIN_AF_BAD_VERTICAL_ACTIVE      -5003
#define ERR_PLUGIN_AF_BAD_HORIZONTAL_ACTIVE    -5004
#define ERR_PLUGIN_AF_MALLOC                   -5005
#define ERR_PLUGIN_AF_BAD_THREAD               -5006
#define ERR_PLUGIN_AF_BLOB_MALLOC              -5007
#define ERR_PLUGIN_AF_NO_ALPHA_CHANNEL		     -5008
#define ERR_PLUGIN_AF_NO_FILTERS               -5009
#define ERR_PLUGIN_AF_AUTO_TRACKING_FAIL       -5010
#define ERR_PLUGIN_AF_BAD_MARKS                -5011

#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_1    -5041
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_2    -5042
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_3    -5043
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_4    -5044
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_5    -5045
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_6    -5046
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_7    -5047
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_8    -5048
#define ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_9    -5049
#endif
