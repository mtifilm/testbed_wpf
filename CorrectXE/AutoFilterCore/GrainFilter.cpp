//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainFilter.h"

#include "HistogramIpp.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "math.h"
#include "SynchronousThreadRunner.h"
#include "map"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define USE_HISTOGRAM_INSTEAD_OF_SORTING
#define NUMBER_OF_HISTOGRAM_BINS 65535

const int KERNEL_RADIUS = 3;
const double IMAGE_CENTER = .8;
const double PERCENTILE_HIGH = .8;
const double PERCENTILE_LOW = .2;
//------------------------------------------------------------------------------

namespace
{
//---------------------------

const int NumberOfSlices = 32;

IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
{
   IppiRect sliceRoi;
   int nominalRowsPerSlice = height / NumberOfSlices;
   int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
   sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
   sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
   sliceRoi.x = 0;
   sliceRoi.width = width;

   return sliceRoi;
}
//---------------------------

struct MeasureParams
{
	Ipp32fArray Src_smooth;
	Ipp32fArray Src_diff;
	Ipp32fArray grainImage;
	double *slope;
	double *inter;
   const GrainModel *grainModel;
};

int RunThreadToMeasureGrain(void *vp, int sliceNumber, int totalJobs)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<MeasureParams *>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->Src_smooth.getRows(), params->Src_smooth.getCols(), sliceNumber);

   Ipp32fArray Src_smooth = params->Src_smooth(sliceRoi);
   Ipp32fArray Src_diff = params->Src_diff(sliceRoi);
   Ipp32fArray grainImage = params->grainImage(sliceRoi);
	double *slope = params->slope;
	double *inter = params->inter;
   const GrainModel *grainModel = params->grainModel;

	auto smoothIter = Src_smooth.begin();
	auto diffsIter = Src_diff.begin();
	auto grainIter = grainImage.begin();

	while (smoothIter != Src_smooth.end())
	{
		Ipp32f value = *smoothIter;
		if (*smoothIter < grainModel->getGrainCenter(0))
		{
			*grainIter = *diffsIter / grainModel->getGrainIntensity(0);
		}
		else if (*smoothIter >= grainModel->getGrainCenter(NUMBER_OF_GRAIN_BINS - 1))
		{
			*grainIter = *diffsIter / grainModel->getGrainIntensity(NUMBER_OF_GRAIN_BINS - 1);
		}
		else
		{
			for (auto i = 0; i < (NUMBER_OF_GRAIN_BINS - 1); ++i)
			{
				if (value >= grainModel->getGrainCenter(i) && value < grainModel->getGrainCenter(i + 1))
				{
					double GI = (slope[i] * (*smoothIter)) + inter[i];
					*grainIter = *diffsIter / GI;
				}
			}
		}

		++smoothIter;
		++diffsIter;
		++grainIter;
	}

   return 0;
}

} // anonymous namespace
//------------------------------------------------------------------------------

/* static */
GrainFilter GrainFilter::CreateFrom(
	Ipp16u *sourceImage,
	int nRows,
	int nCols,
	int whichChannel,
	Ipp16u maxValue,
	BlobPixelArrayWrapper *blobPixelArrayWrapper,
	MTI_UINT16 pixelFilterMask)
{
	auto grainModel = GrainModel::CreateFrom(
								sourceImage,
								nRows,
								nCols,
								whichChannel,
								maxValue,
								blobPixelArrayWrapper,
								pixelFilterMask);

	return CreateFrom(grainModel);
}
//------------------------------------------------------------------------------

/* static */
GrainFilter GrainFilter::CreateFrom(const GrainModel &grainModel)
{
	GrainFilter grainFilter;
	grainFilter.measureGrain(grainModel);

	return grainFilter;
}
//------------------------------------------------------------------------------

//function Grain = MeasureGrain (RGB, chan, GrainCenter, GrainIntensity)
//%
//%  The grain portions of the image will have values less than
//%  a threshold.  Values above the threshold are certainly not grain.
//%
//%  The value of the threshold should be somewhere between 2 and 5.
//%

void GrainFilter::measureGrain(const GrainModel &grainModel)
{
	//% the manifest constants
	//KERNEL_RADIUS = 3;

	//% extract the channel to use
	//if chan == 4
	//	 Src = rgb2gray (RGB(:,:,1:3));
	//else
	//	 Src = RGB(:,:,chan);
	//end
	//

	//% the smoothing kernel
	//Kern = ones(2*KERNEL_RADIUS+1,2*KERNEL_RADIUS+1);
	//Kern = Kern / sum(Kern(:));

	//% the smoothed image.  Grain is removed.
	//Src_smooth = conv2(Src,Kern,'same');
	Ipp32fArray Src_smooth = grainModel.getSmoothImage();

	//% the difference image.  Un-regularized grain signal.
	//Src_diff = Src - Src_smooth;
	Ipp32fArray Src_diff = grainModel.getDiffsImage();

	//% regularize the grain signal
	//Grain = zeros(size(RGB,1), size(RGB,2));
	Ipp32fArray grainImage(Src_smooth.getSize());
	grainImage.zero();

	//% find all values less than the smallest GrainCenter
	//idx = find(Src_smooth(:) < GrainCenter(1));
	//Grain(idx) = Src_diff(idx) / GrainIntensity(1);
	//
	//% find all values greater than the largest GrainCenter
	//idx = find(Src_smooth(:) >= GrainCenter(end));
	//Grain(idx) = Src_diff(idx) / GrainIntensity(end);
	//
	//% the rest of the values
	//for i = 1:numel(GrainCenter)-1
	//    idx = find ( (Src_smooth(:) >= GrainCenter(i)) & ...
	//                 (Src_smooth(:) < GrainCenter(i+1)) );
	//
	//    % linear interpolation of between endpoints
	//    %
	//    % when x = GrainCenter(i) use y = GrainIntensity(i)
	//    % when x = GrainCenter(i+1) use y = GrainIntensity(i+1)
	//
	//	 slope = (GrainIntensity(i+1) - GrainIntensity(i)) / ...
	//            (GrainCenter(i+1) - GrainCenter(i));
	//    inter = GrainIntensity(i) - slope * GrainCenter(i);
	//
	//    GI = slope*Src_smooth(idx) + inter;
	//    Grain(idx) = Src_diff(idx) ./ GI;
	//end

   CHRTimer GRAIN_FILTER_MEASURE_TIMER;

//	auto smoothIter = Src_smooth.begin();
//	auto diffsIter = Src_diff.begin();
//	auto grainIter = grainImage.begin();
	double slope[NUMBER_OF_GRAIN_BINS - 1];
	double inter[NUMBER_OF_GRAIN_BINS - 1];
	for (auto i = 0; i < (NUMBER_OF_GRAIN_BINS - 1); ++i)
	{
		slope[i] = (grainModel.getGrainIntensity(i + 1) - grainModel.getGrainIntensity(i)) /
							(grainModel.getGrainCenter(i + 1) - grainModel.getGrainCenter(i));
		inter[i] =  grainModel.getGrainIntensity(i) - slope[i] * grainModel.getGrainCenter(i);
	}

//	while (smoothIter != Src_smooth.end())
//	{
//		Ipp32f value = *smoothIter;
//		if (*smoothIter < grainModel.getGrainCenter(0))
//		{
//			*grainIter = *diffsIter / grainModel.getGrainIntensity(0);
//		}
//		else if (*smoothIter >= grainModel.getGrainCenter(NUMBER_OF_GRAIN_BINS - 1))
//		{
//			*grainIter = *diffsIter / grainModel.getGrainIntensity(NUMBER_OF_GRAIN_BINS - 1);
//		}
//		else
//		{
//			for (auto i = 0; i < (NUMBER_OF_GRAIN_BINS - 1); ++i)
//			{
//				if (value >= grainModel.getGrainCenter(i) && value < grainModel.getGrainCenter(i + 1))
//				{
//					double GI = (slope[i] * (*smoothIter)) + inter[i];
//					*grainIter = *diffsIter / GI;
//				}
//			}
//		}
//
//		++smoothIter;
//		++diffsIter;
//		++grainIter;
//	}
   MeasureParams mParams = { Src_smooth, Src_diff, grainImage, slope, inter, &grainModel };
   SynchronousThreadRunner multithread(NumberOfSlices, &mParams, RunThreadToMeasureGrain);
   multithread.Run();

   //DBTRACE(GRAIN_FILTER_MEASURE_TIMER.ReadAsString());

	//
	//end

	_grainImage = grainImage;
}
//------------------------------------------------------------------------------

////#define _DEBUG_GRAIN_TEST
#ifdef _DEBUG_GRAIN_TEST
bool GrainFilter::isThisBlobActuallyGrain(BlobPixelIterator blobPixels, long param)
{
	auto grainIter = _grainImage.begin();
	float sum = 0.f;

   const int imageWidth = 4096;
   int minImageX = 10000;
   int minImageY = 10000;
   int maxImageX = 0;
   int maxImageY = 0;
   int pixelCount = 0;
   auto blobPixelsCopy = blobPixels;
   std::map<int, int> valMap;

	while(true)
	{
		int index = blobPixels.getNextPixel();
		if (index == INVALID_BLOB_PIXEL_ADDRESS)
		{
			break;
		}

      int imageX = index % imageWidth;
      int imageY = index / imageWidth;
      minImageX = min<float>(imageX, minImageX);
      minImageY = min<float>(imageY, minImageY);
      maxImageX = max<float>(imageX, maxImageX);
      maxImageY = max<float>(imageY, maxImageY);
      ++pixelCount;

		sum += grainIter[index];
//      TRACE_0(errout << "grain[" << index << "] = " << grainIter[index] << ", SUM = " << sum);
      valMap[index] = (int)((grainIter[index] * 1000.0) + 0.5);
	}

   // Change param 0-100 to range 0-50.
   float threshold = param / 2.f;

   int valsW = maxImageX - minImageX + 1;
   int valsH = maxImageY - minImageY + 1;
   int valsArea = valsW * valsH;
//   DBTRACE(minImageX);
//   DBTRACE(minImageY);
//   DBTRACE(maxImageX);
//   DBTRACE(maxImageY);
//   DBTRACE(valsW);
//   DBTRACE(valsH);
//   DBTRACE(valsArea);

//   if (valsArea >= 100 && fabs(sum) <= threshold)
if (pixelCount == 215)
   {
      DBTRACE(pixelCount);
      DBTRACE(param);
      DBTRACE(threshold);
      DBTRACE(fabs(sum));

      int *vals = new int[valsArea];
      for (auto i = 0; i < valsArea; ++i)
      {
         vals[i] = 0;
      }

      for (auto valMapEntry : valMap)
      {
         int index = valMapEntry.first;
         int valX = (index % imageWidth) - minImageX;
         int valY = (index / imageWidth) - minImageY;

         int valsIndex = valX + (valY * valsW);
         vals[valsIndex] = valMapEntry.second;
         //TRACE_0(errout << "vals[" << valsIndex << "] = " << vals[valsIndex]);
      }

      for (auto y = 0; y < valsH; ++y)
      {
         MTIostringstream os;
         for (auto x = 0; x < valsW; ++x)
         {
            int i = x + (y * valsW);
            int val = vals[i];

            if (val > 0)
            {
               os << "   " << (val / 1000) << "." << setw(3) << setfill('0') << (val % 1000);
            }
            else if (val < 0)
            {
               val = -val;
               os << "  -" << (val / 1000) << "." << setw(3) << setfill('0') << (val % 1000);
            }
            else
            {
               os << "   ---  ";
            }
         }

         TRACE_0(errout << os.str());
      }
   }

	return fabs(sum) <= threshold;
}
#else
bool GrainFilter::isThisBlobActuallyGrain(BlobPixelIterator blobPixels, long param)
{
	auto grainIter = _grainImage.begin();
	float sum = 0.f;
	while(true)
	{
		int index = blobPixels.getNextPixel();
		if (index == INVALID_BLOB_PIXEL_ADDRESS)
		{
			break;
		}

		sum += grainIter[index];
	}

   // Change param 0-100 to range 0-50.
   float threshold = param / 2.f;
	return fabs(sum) <= threshold;
}
#endif
//------------------------------------------------------------------------------

bool GrainFilter::isThisBlobActuallyGrain(long *blobPixelIndicess, long pixelCount, long param)
{
	auto grainIter = _grainImage.begin();
	float sum = 0.f;

	for (auto index = 0; index < pixelCount; ++index)
   {
		sum += grainIter[blobPixelIndicess[index]];
	}

	return fabs(sum) <= 10.f;
}
//------------------------------------------------------------------------------


