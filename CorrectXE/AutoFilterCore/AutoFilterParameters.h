//---------------------------------------------------------------------------

#ifndef AutoFilterParametersH
#define AutoFilterParametersH

#include "IniFile.h"  // THIS SUCKS.
//---------------------------------------------------------------------------

#define AF_MAX_SETTINGS 6     // aka max number of filters
#define AF_MAX_COMPONENTS 3
//------------------------------------------------------------------------------

class CAutoFilterParameters
{
public:
	CAutoFilterParameters() {};
   ~CAutoFilterParameters() {};

	void InitGeneral ()
    {
     // set to default values
	  lNSetting = 0;
	 }

	void InitSetting (int iSetting)
    {
     // increment number of settings
     lNSetting++;

     lPrincipalComponent[iSetting] = -1;     // "do the right thing"
     lComponentMask[iSetting] = 0x7;
     bUseAlphaMatteToFindDebris[iSetting] = false;

     // set to default values
     faMinVal[iSetting][0] = 0.;
     faMaxVal[iSetting][0] = 100.;
     faMinVal[iSetting][1] = 0.;
     faMaxVal[iSetting][1] = 100.;
     faMinVal[iSetting][2] = 0.;
     faMaxVal[iSetting][2] = 100.;

     faContrastSpatial[iSetting] = 50.;
     faContrastTemporal[iSetting] = 50.;
     laContrastDirection[iSetting] = 0;

	  faMotionOffsetRow[iSetting] = 10.;
     faMotionOffsetCol[iSetting] = 10.;
     faMotionThresh[iSetting] = 50.;

     faMaxSize[iSetting] = 50.;
     faMinSize[iSetting] = 0.;

	  bUseHardMotionOnDust[iSetting] = false;
	  lDustMaxSize[iSetting] = 10;

     bUseGrainFilter[iSetting] = false;
     lGrainFilterParam[iSetting] = 50;

     lLuminanceThreshold[iSetting] = 100;

     strSettingName[iSetting] = "";
    }

    void dump()
    {
      DBTRACE("=================");
      DBTRACE(lNSetting);
      for (auto iSetting = 0; iSetting < lNSetting; ++iSetting)
      {
        DBTRACE("=================");
        DBTRACE(iSetting);

        DBTRACE(lPrincipalComponent[iSetting]);
        DBTRACE(lComponentMask[iSetting]);
        DBTRACE(bUseAlphaMatteToFindDebris[iSetting]);

        DBTRACE(faMinVal[iSetting][0]);
        DBTRACE(faMaxVal[iSetting][0]);
        DBTRACE(faMinVal[iSetting][1]);
        DBTRACE(faMaxVal[iSetting][1]);
        DBTRACE(faMinVal[iSetting][2]);
        DBTRACE(faMaxVal[iSetting][2]);

        DBTRACE(faContrastSpatial[iSetting]);
        DBTRACE(faContrastTemporal[iSetting]);
        DBTRACE(laContrastDirection[iSetting]);

        DBTRACE(faMotionOffsetRow[iSetting]);
        DBTRACE(faMotionOffsetCol[iSetting]);
        DBTRACE(faMotionThresh[iSetting]);

        DBTRACE(faMaxSize[iSetting]);
        DBTRACE(faMinSize[iSetting]);

        DBTRACE(bUseHardMotionOnDust[iSetting]);
        DBTRACE(lDustMaxSize[iSetting]);

		  DBTRACE(bUseGrainFilter[iSetting]);
		  DBTRACE(lGrainFilterParam[iSetting]);

        DBTRACE(lLuminanceThreshold[iSetting]);

        DBTRACE(strSettingName[iSetting]);
      }
      DBTRACE("=================");
    }

   //
   //  parameters to control rounds of filtering
   //
   long lNSetting = 0;

	//
   //	parameters to control component processing
   //
   long lPrincipalComponent[AF_MAX_SETTINGS]; // which is most important component
   long lComponentMask[AF_MAX_SETTINGS];      // which components to process
   bool bUseAlphaMatteToFindDebris[AF_MAX_SETTINGS]; // look for debris in alpha

   //
   //	parameters to control image value range
   //
   float faMinVal[AF_MAX_SETTINGS][AF_MAX_COMPONENTS];	// range: 0% to 100%
	float faMaxVal[AF_MAX_SETTINGS][AF_MAX_COMPONENTS];	// range: 0% to 100%

   //
   //  parameters to control contrast
   //
   float faContrastSpatial[AF_MAX_SETTINGS];	    	// range: 0% to 100%
   float faContrastTemporal[AF_MAX_SETTINGS];		// range: 0% to 100%
   long laContrastDirection[AF_MAX_SETTINGS];		// range: -1, 0, 1

   //
   //  parameters to control motion
   //
   float faMotionOffsetRow[AF_MAX_SETTINGS];		// range 0% to 100%
   float faMotionOffsetCol[AF_MAX_SETTINGS];		// range 0% to 100%
   float faMotionThresh[AF_MAX_SETTINGS];		   // range 0% to 100%

   //
   //  parameters to control size of defect
   //
   float faMaxSize[AF_MAX_SETTINGS];			// range 0% to 100%
   float faMinSize[AF_MAX_SETTINGS];			// range 0% to 100%

   //
   // Edge blur for fixes
   //
   float faEdgeBlur[AF_MAX_SETTINGS];           // range 0% to 100%

   //
   // Special case "dust" (small debris)
   //
   bool bUseHardMotionOnDust[AF_MAX_SETTINGS];
   long lDustMaxSize[AF_MAX_SETTINGS];        // total debris pixel count

   // Grain filter
   bool bUseGrainFilter[AF_MAX_SETTINGS];
   long lGrainFilterParam[AF_MAX_SETTINGS];

   // Alpha luminance threshold
   long lLuminanceThreshold[AF_MAX_SETTINGS];

	//
	// Info
	//
	string strSettingName[AF_MAX_SETTINGS];

   // Dummy parameter than can be used to force a reset of the AF algorithm
	int filterChangeCounter = 0;

   //TEST - frame index
   long lFrameIndex = -1;

   //
};
//------------------------------------------------------------------------------


#endif
