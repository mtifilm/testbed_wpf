#include "BlobProcessor.h"
#include "PixelRegions.h"
#include "IniFile.h"    // For TRACE_N() !!!!
#include <math.h>

/*
   Optimization control
 */
#define UNROLL_LOOPS 0

// To trace a bunch of crap at trace_0
//#define VERBOSE_TRACE    // NOT NOT NOT NOT NOT ok to CHECK IN LIKE THIS ##########################

//-------------------------------------------------------------------------

CBlobProcessor::CBlobProcessor(bool bThreadInternally)
: m_bThreadInternally(bThreadInternally)
{
   for (int i = 0; i < AF_NUM_SRC_FRAMES; ++i)
   {
      m_uspSrc[i] = nullptr;
   }

   /*
     New "motion search thread" stuff
     Max of two threads if we're supposed to thread internally -- one looks
     reference A (usually the previous frame) and the other thread looks at
     reference B (usually the next frame)

     Uggh, now uses 4 threads??
   */
   m_nMotionThreads = m_bThreadInternally? 4 : 1;

   m_tsMotionThreadCtrl.PrimaryFunction   = (void (*)(void *,int))&FindBestMotionMT;
   m_tsMotionThreadCtrl.SecondaryFunction = NULL;
   m_tsMotionThreadCtrl.CallBackFunction  = NULL;
   m_tsMotionThreadCtrl.vpApplicationData = this;
   m_tsMotionThreadCtrl.iNThread          = m_nMotionThreads;
   m_tsMotionThreadCtrl.iNJob             = m_nMotionThreads;
   if (m_nMotionThreads > 1)
   {
      int iRet = MThreadAlloc(&m_tsMotionThreadCtrl);
      if (iRet)
      {
         TRACE_0(errout << "ERROR: BlobProcessor: MThreadAlloc failed, iRet=" << iRet);
         m_nMotionThreads = 1;
      }
   }
}
//-------------------------------------------------------------------------

CBlobProcessor::~CBlobProcessor()
{
   if (m_inpaintBuffer != NULL)
      delete [] m_inpaintBuffer;

   if (m_nMotionThreads > 1)
   {
      MThreadFree(&m_tsMotionThreadCtrl);
   }
}
//-------------------------------------------------------------------------

bool CBlobProcessor::ProcessBlob(long lNPixelsInBlogArg,
                                 long *lpBlobRCArg,
                                 const AUTO_FILTER_SETTING *afspArg,
                                 int pixelMaskTypeArg)
{
//   DBTRACE("========== PROCESS BLOB ==============");
//   DBTRACE(*lpBlobRCArg);
//   DBTRACE(lNPixelsInBlogArg);
   //afspArg->dump();

   bool retVal = false;

   m_lNPixelsInBlob = lNPixelsInBlogArg;
	if (m_lNPixelsInBlob < 1)
	{
		return retVal;
	}

   m_lpBlobRC = lpBlobRCArg;
   m_afsp = afspArg;
   m_pixelMaskType_lSetting = pixelMaskTypeArg;

   // Check if we need to do motion estimation.
   if (m_lNPixelsInBlob > m_afsp->lMaxSizeThresh || m_lNPixelsInBlob < m_afsp->lMinSizeThresh)
   {
      m_afap->bSurroundUseSelf = false;
      m_afap->bSurroundUseA = false;
      m_afap->bSurroundUseB = false;
      m_afap->lSurroundMotionA = 0;
      m_afap->lSurroundMotionB = 0;
   }
   else if (m_lNPixelsInBlob <= m_afsp->lDustMaxSize
   && m_afsp->bUseHardMotionOnDust == true)
   {
      m_afap->bSurroundUseSelf = true;
      m_afap->bSurroundUseA = false;
      m_afap->bSurroundUseB = false;
      m_afap->lSurroundMotionA = 0;
      m_afap->lSurroundMotionB = 0;
   }
   else if (m_afsp->bUseMotion == false)
   {
      m_afap->bSurroundUseSelf = false;
      m_afap->bSurroundUseA = true;
      m_afap->bSurroundUseB = true;
      m_afap->lSurroundMotionA = 0;
      m_afap->lSurroundMotionB = 0;
   }
   else
   {
      // Need to do motion estimation.
      m_afap->bSurroundUseSelf = false;

      bool foundMotion = false;
      if (m_bestMotionCacheMap != nullptr)
      {
         auto iter = m_bestMotionCacheMap->find(*m_lpBlobRC);
         if (iter != m_bestMotionCacheMap->end())
         {
            m_afap->lSurroundMotionA = iter->second.MotionA;
            m_afap->lSurroundMotionB = iter->second.MotionB;

            m_afap->bSurroundUseA = m_afap->lSurroundMotionA != MotionCacheMapEntry::INVALID_MOTION;
            m_afap->bSurroundUseB = m_afap->lSurroundMotionB != MotionCacheMapEntry::INVALID_MOTION;

            foundMotion = true;
         }
      }

      if (!foundMotion)
      {
         // To speed up the bulk of the searches, we divide the blobs into those
         // which are close to an edge and those that are not.
         long lLowerSafeLevel, lUpperSafeLevel;
         DetermineSafeLevels(lLowerSafeLevel, lUpperSafeLevel);

         // Do a motion search on the non-defective, surrounding pixels
         MotionSearch(&m_afap->lSurroundMotionA,
                      &m_afap->lSurroundMotionB,
                      m_lNPixelsInBlob,
                      m_lpBlobRC,
                      lLowerSafeLevel, lUpperSafeLevel,
                      PM_DEFECT_IN_NEIGHBOR|PM_DEFECT_IN_PIXEL);

         if (m_bestMotionCacheMap != nullptr)
         {
            MotionCacheMapEntry motionCacheMapEntry;
			motionCacheMapEntry.MotionA = m_afap->bSurroundUseA
                                          ? m_afap->lSurroundMotionA
                                          : MotionCacheMapEntry::INVALID_MOTION;
            motionCacheMapEntry.MotionB = m_afap->bSurroundUseB
                                          ? m_afap->lSurroundMotionB
                                          : MotionCacheMapEntry::INVALID_MOTION;
            (*m_bestMotionCacheMap)[*m_lpBlobRC] = motionCacheMapEntry;
         }
      }
   }

//   DBTRACE(m_afap->lSurroundMotionA);
//   DBTRACE(m_afap->lSurroundMotionB);

   // Filter out the unsatisfactorty fixes
   MotionClassify(m_afap->lSurroundMotionA,
                  m_afap->lSurroundMotionB,
                  &m_afap->bSurroundUseA,
                  &m_afap->bSurroundUseB);

//   DBTRACE(m_afap->bSurroundUseA);
//   DBTRACE(m_afap->bSurroundUseB);

   // Odd place for this!
   if ((m_emergencyStopFlagPtr != NULL) && *m_emergencyStopFlagPtr)
   {
		return retVal;
   }

   // Apply the fix.
   if (m_afap->bSurroundUseSelf)
   {
      // Apply hard motion on dust.
		InpaintApply();
		retVal = true;
   }
   else if (m_afap->bSurroundUseA || m_afap->bSurroundUseB)
   {
      // Apply normal fix that wasn't filtered out.
		retVal = MotionApply(
                  m_afap->lpMotionRCCA[m_afap->lSurroundMotionA],
                  m_afap->lpMotionRCCB[m_afap->lSurroundMotionB],
                  m_afap->bSurroundUseA,
                  m_afap->bSurroundUseB);
   }

	return retVal;
}
//-------------------------------------------------------------------------

/*
	This function determines the safe upper and lower levels given
        a set of motions for the NON-TRAJECTORY mode.
*/
void CBlobProcessor::DetermineSafeLevels(long &lLowerSafeLevel,
													  long &lUpperSafeLevel)
{
  long lMostPositiveMotion, lMostNegativeMotion;

  // slot 0 is always 0, and order of the rest will be either strictly
  // ascending or descending; assumes that the list for frame B will
  // have greater gain than the list for frame A (see "SetSrcImgPtr")

  long *lpMotionRCC = m_afap->lpMotionRCCB; // note that it's "B"
  long lNMotion = m_afap->lNMotion;

  if (lpMotionRCC[1] < 0)
  {
     lMostNegativeMotion = lpMotionRCC[1];
     lMostPositiveMotion = lpMotionRCC[lNMotion - 1];
  }
  else
  {
     lMostPositiveMotion = lpMotionRCC[1];
     lMostNegativeMotion = lpMotionRCC[lNMotion - 1];
  }

  lLowerSafeLevel = -lMostNegativeMotion;
  lUpperSafeLevel = m_lNRowColCom - lMostPositiveMotion - 1;

//  TRACE_3(errout << "~~~~~~~~~~~~~~MNMot=" << lMostNegativeMotion << ", MPMot=" << lMostPositiveMotion
//                             << ", LSLvl=" << lLowerSafeLevel     << ", USLvl=" << lUpperSafeLevel);
////  MTIassert(lLowerSafeLevel >= 0 && lLowerSafeLevel < m_lNRowColCom);
////  MTIassert(lUpperSafeLevel >= 0 && lUpperSafeLevel < m_lNRowColCom);
  MTIassert(lUpperSafeLevel > lLowerSafeLevel);
}
//-------------------------------------------------------------------------

/*
	This function finds the best motion vectors from the
	current frame to frame A and to frame B.
*/
void CBlobProcessor::MotionSearch(
   long *lpMotionA,
   long *lpMotionB,
   long lNBlobArg,
   long *lpBlobRCArg,
   long lLowerSafeLevel,
   long lUpperSafeLevel,
   long lExcludeMask)
{
  MTI_UINT16 *uspCurr = m_uspSrc[0] + m_afsp->lActiveComponent;

  // Precompute the list of "unexcluded" points to check (i.e. non-defect
  // pixels); we further categorize the points into "safe" and "unsafe"
  // lists, where a point is "safe" if it is farther from the top edge of
  // the frame than the most negative motion value, and it is farther from
  // the bottom edge than the most positive motion value.
  long lNUnexclRowColComSafe = 0;
  long lNUnexclRowColComUnsafe = 0;
  //long *m_lpUnexclRowColComSafeList = new long[lNBlobArg];    now pre-alloced
  //long *m_lpUnexclRowColComUnsafeList = new long[lNBlobArg];  now pre-alloced

  // Make sure we were called in proper sequence!
  MTIassert(m_lpUnexclRowColComLists != NULL);
  if (m_lpUnexclRowColComLists == NULL)
  {
     *lpMotionA = *lpMotionB = 0;
     return;
  }

  // make two lists of unexcluded pixels... "safe" and "unsafe"
  // UPDATE: To save space I now only allocate one list that contains
  // all the unsafe pixels followed immediately by all the "safe" pixels
  m_lpUnexclRowColComUnsafeList = m_lpUnexclRowColComLists;
  for (long lBlob = 0; lBlob < lNBlobArg; lBlob++)
  {
     long lRowCol = lpBlobRCArg[lBlob];
     if ((m_uspPixelMask[lRowCol] & lExcludeMask) == 0)
     {
        long lRowColCom = lRowCol * m_lNCom;
        if (lRowColCom < lLowerSafeLevel || lRowColCom > lUpperSafeLevel)
           m_lpUnexclRowColComUnsafeList[lNUnexclRowColComUnsafe++] = lRowColCom;
     }
  }
  m_lpUnexclRowColComSafeList = m_lpUnexclRowColComLists + lNUnexclRowColComUnsafe;
  for (long lBlob = 0; lBlob < lNBlobArg; lBlob++)
  {
     long lRowCol = lpBlobRCArg[lBlob];
     if ((m_uspPixelMask[lRowCol] & lExcludeMask) == 0)
     {
        long lRowColCom = lRowCol * m_lNCom;
        if (lRowColCom >= lLowerSafeLevel && lRowColCom <= lUpperSafeLevel)
           m_lpUnexclRowColComSafeList[lNUnexclRowColComSafe++] = lRowColCom;
     }
  }

  // Clear temp buffers
  long lNMotion = m_afap->lNMotion;
  memset(m_lpMotionDiffs[0], 0, lNMotion * sizeof(long));
  memset(m_lpMotionDiffs[1], 0, lNMotion * sizeof(long));

  if (m_nMotionThreads > 1)
  {
     long lRet;

     mMT_afap = m_afap;
     mMT_uspCurr = uspCurr;
     mMT_uspRefA = m_uspSrc[1] + m_afsp->lActiveComponent;
     mMT_uspRefB = m_uspSrc[2] + m_afsp->lActiveComponent;
     mMT_lNUnexclRowColComSafe = lNUnexclRowColComSafe;
     mMT_lNUnexclRowColComUnsafe = lNUnexclRowColComUnsafe;
     mMT_lpUnexclRowColComSafeList = m_lpUnexclRowColComSafeList;
     mMT_lpUnexclRowColComUnsafeList = m_lpUnexclRowColComUnsafeList;

     lRet = MThreadStart(&m_tsMotionThreadCtrl);  // returns when threads exit
     if (lRet != 0)
     {
        TRACE_0(errout << "ERROR: CAlgorithmAF::MotionSearch: can't start the processing threads"  << endl
                     << "       Return code was " << lRet);
        return;
     }

     if (m_lpMotionDiffs[0][mMT_lBestMotionA[0]] <
         m_lpMotionDiffs[0][mMT_lBestMotionA[1]])
     {
        *lpMotionA = mMT_lBestMotionA[0];
     }
     else
     {
        *lpMotionA = mMT_lBestMotionA[1];
     }

     if (m_lpMotionDiffs[1][mMT_lBestMotionB[0]] <
         m_lpMotionDiffs[1][mMT_lBestMotionB[1]])
     {
        *lpMotionB = mMT_lBestMotionB[0];
     }
     else
     {
        *lpMotionB = mMT_lBestMotionB[1];
     }
  }
  else
  {
     *lpMotionA = FindBestMotion(
                    uspCurr,
                    m_uspSrc[1] + m_afsp->lActiveComponent,
                    m_afap->lNMotion,
                    m_afap->lpMotionRCCA,
                    lNUnexclRowColComUnsafe,
                    m_lpUnexclRowColComUnsafeList,
                    lNUnexclRowColComSafe,
                    m_lpUnexclRowColComSafeList,
                    m_lpMotionDiffs[0]);         // Output

     *lpMotionB = FindBestMotion(
                    uspCurr,
                    m_uspSrc[2] + m_afsp->lActiveComponent,
                    m_afap->lNMotion,
                    m_afap->lpMotionRCCB,
                    lNUnexclRowColComUnsafe,
                    m_lpUnexclRowColComUnsafeList,
                    lNUnexclRowColComSafe,
                    m_lpUnexclRowColComSafeList,
                    m_lpMotionDiffs[1]);         // Output
   }

  //delete[] m_lpUnexclRowColComSafeList;       now prealloced at largest size
  //delete[] m_lpUnexclRowColComUnsafeList;     now prealloced at largest size

}
//-------------------------------------------------------------------------

void CBlobProcessor::FindBestMotionMT(void *vp, int iJob)
{
   CBlobProcessor *_this = reinterpret_cast<CBlobProcessor *>( vp );

   long laNMotion[2];
   laNMotion[0] = (_this->mMT_afap->lNMotion + 1) / 2;
   laNMotion[1] = _this->mMT_afap->lNMotion - laNMotion[0];

   long lNMotion;
   MTI_UINT16 *uspRef;
   long *lpMotionRCC;
   long *lpMotionDiffs;

   switch (iJob)
   {
      case 0:
         lNMotion = laNMotion[0];
         uspRef = _this->mMT_uspRefA;
         lpMotionRCC = _this->mMT_afap->lpMotionRCCA;
         lpMotionDiffs = _this->m_lpMotionDiffs[0];
         break;
      case 1:
         lNMotion = laNMotion[1];
         uspRef = _this->mMT_uspRefA;
         lpMotionRCC = _this->mMT_afap->lpMotionRCCA + laNMotion[0];
         lpMotionDiffs = _this->m_lpMotionDiffs[0] + laNMotion[0];
         break;
      case 2:
         lNMotion = laNMotion[0];
         uspRef = _this->mMT_uspRefB;
         lpMotionRCC = _this->mMT_afap->lpMotionRCCB;
         lpMotionDiffs = _this->m_lpMotionDiffs[1];
         break;
      case 3:
         lNMotion = laNMotion[1];
         uspRef = _this->mMT_uspRefB;
         lpMotionRCC = _this->mMT_afap->lpMotionRCCB + laNMotion[0];
         lpMotionDiffs = _this->m_lpMotionDiffs[1] + laNMotion[0];
         break;
   }

   long lBestMotion = _this->FindBestMotion(
                             _this->mMT_uspCurr,
                             uspRef,
                             lNMotion,
                             lpMotionRCC,
                             _this->mMT_lNUnexclRowColComUnsafe,
                             _this->mMT_lpUnexclRowColComUnsafeList,
                             _this->mMT_lNUnexclRowColComSafe,
                             _this->mMT_lpUnexclRowColComSafeList,
                             lpMotionDiffs);
   switch (iJob)
   {
      case 0:
         _this->mMT_lBestMotionA[0] = lBestMotion;
         break;
      case 1:
         _this->mMT_lBestMotionA[1] = lBestMotion + laNMotion[0];
         break;
      case 2:
         _this->mMT_lBestMotionB[0] = lBestMotion;
         break;
      case 3:
         _this->mMT_lBestMotionB[1] = lBestMotion + laNMotion[0];
         break;
   }
}
//-------------------------------------------------------------------------

void CBlobProcessor::FindBestMotion1(
   MTI_UINT16 *uspCurr,
   MTI_UINT16 *uspRef,
   long lNMotion,
   long *lpMotionRCC,
   long lNUnexclRowColComUnsafe,
   long *lpUnexclRowColComUnsafeList,
   long *lpMotionDiffs)
{
  // Local accesses to save 1 MOV inst in inner loop!
  long lLocalNRowColCom = m_lNRowColCom;
  long lLocalNMotion = lNMotion;
  long lLocalNUnexclRowColComUnsafe = lNUnexclRowColComUnsafe;

  // Process the "unsafe" list - includes an out-of-bounds check
  for (int i = 0; i < lLocalNUnexclRowColComUnsafe; ++i)
  {
     MTI_INT32 lRowColCom = lpUnexclRowColComUnsafeList[i];
     MTI_INT32 lCurr = (MTI_INT32) uspCurr[lRowColCom];

     for (int j = 0; j < lLocalNMotion; ++j)
     {
        MTI_INT32 lRowColComRef = lRowColCom + lpMotionRCC[j];

        // If the motion puts us off the top or bottom of the frame, we
        // wrap around to the bottom or top of the frame, respectively.
        // The theory is that whatever's way on the other side of the frame
        // will most probably not match too well so we'll get a reasonably
        // large diff, and this is consistent with what happens naturally at
        // the sides of the frame!
        if (lRowColComRef < 0)
           lRowColComRef += lLocalNRowColCom;
        else if (lRowColComRef >= lLocalNRowColCom)
           lRowColComRef -= lLocalNRowColCom;

        MTI_INT32 lDiff = ((MTI_INT32) uspRef[lRowColComRef]) - lCurr;
        if (lDiff < 0)
           lpMotionDiffs[j] -= lDiff;
        else
           lpMotionDiffs[j] += lDiff;
     }
  }
}  /* FindBestMotion1 */
//-------------------------------------------------------------------------

void CBlobProcessor::FindBestMotion2(
   MTI_UINT16 *uspCurr,
   MTI_UINT16 *uspRef,
   long lNMotion,
   long *lpMotionRCC,
   long lNUnexclRowColComSafe,
   long *lpUnexclRowColComSafeList,
   long *lpMotionDiffs)
{
  // Process the "safe" list
  for (int i = 0; i < lNUnexclRowColComSafe; ++i)
  {
     MTI_INT32 lRowColCom = lpUnexclRowColComSafeList[i];
     MTI_INT32 lCurr = (MTI_INT32) uspCurr[lRowColCom];
     long *lpMotion = &lpMotionRCC[0];
     long *lpDiff = &lpMotionDiffs[0];
     long *lpLastDiff = &lpMotionDiffs[lNMotion-1];
     int stop = lNMotion;
     MTI_UINT16 *uspRefRCC = &uspRef[lRowColCom];

     for (int j = 0; j < stop; ++j)
     {
        long lMotion = lpMotion[j];
#ifdef _DEBUG
        // Check if it really was safe!!
        MTIassert((lRowColCom + lMotion) >= 0);
        MTIassert((lRowColCom + lMotion) < m_lNRowColCom);
#endif
        MTI_INT32 lDiff = ((MTI_INT32) uspRefRCC[lMotion]) - lCurr;
        if (lDiff < 0)
           lpDiff[j] -= lDiff;
        else
           lpDiff[j] += lDiff;
     }
  }
}  /* FindBestMotion2 */
//-------------------------------------------------------------------------

long CBlobProcessor::FindBestMotion3(long lNMotion, long *lpMotionDiffs)
{
  long *lpZerothMotion = &lpMotionDiffs[0];
  long *lpFirstMotion = &lpMotionDiffs[1];
  long *lpLastMotion = &lpMotionDiffs[lNMotion-1];
  long *lpBestMotion = lpZerothMotion;

  // find the best motion (lowest diff)
  for (long *lpMotion = lpFirstMotion; lpMotion <= lpLastMotion; ++lpMotion)
  {
     if (*lpMotion < *lpBestMotion)
     {
        lpBestMotion = lpMotion;
     }
  }
  return (lpBestMotion - lpZerothMotion);

}  /* FindBestMotion3 */
//-------------------------------------------------------------------------

long CBlobProcessor::FindBestMotion(
   MTI_UINT16 *uspCurr,
   MTI_UINT16 *uspRef,
   long lNMotion,
   long *lpMotionRCC,
   long lNUnexclRowColComUnsafe,
   long *m_lpUnexclRowColComUnsafeList,
   long lNUnexclRowColComSafe,
   long *m_lpUnexclRowColComSafeList,
   long *lpMotionDiffs)
{
  FindBestMotion1(
        uspCurr,
        uspRef,
        lNMotion,
        lpMotionRCC,
        lNUnexclRowColComUnsafe,
        m_lpUnexclRowColComUnsafeList,
        lpMotionDiffs);

  FindBestMotion2(
        uspCurr,
        uspRef,
        lNMotion,
        lpMotionRCC,
        lNUnexclRowColComSafe,
        m_lpUnexclRowColComSafeList,
        lpMotionDiffs);

  return FindBestMotion3(lNMotion, lpMotionDiffs);

}  /* FindBestMotion */
//-------------------------------------------------------------------------

// For speed, this replaces RowColComWrapAround()
// Deal with top/bottom image boundary problems by wrapping row*col*com index
// to the opposite edge of the image if it falls outside the image; the theory
// is that whatever's there will probably not match the blob whose motion we
// are estimating so contrast should be relatively high and not affect our
// searching (I guess!).
#define CHECK_ROWCOLCOM_BOUNDS(ROWCOLCOM, TOP) \
   if (ROWCOLCOM < 0)                          \
      ROWCOLCOM += TOP;                        \
   if (ROWCOLCOM >= TOP)                       \
      ROWCOLCOM -= TOP;
//-------------------------------------------------------------------------

void CBlobProcessor::MotionClassify(long lMotionA, long lMotionB,
                                    bool *bpUseA, bool *bpUseB)
/*
	After determining best motion vector for a blob of pixels, decide
	if the motion is good enough to use.
*/
{
   long lMotionThresh, lNSurround, lNSurroundTot, lRCCA, lRCCB, lRowColComA,
         lRowColComB, lRowCol, lRowColCom, lBlob, lNDefect, lNDefectTot,
         lNDefectNeighbor, lNDefectNeighborTot;
   long lNLayer, lNLayerTot;
   MTI_UINT16 *uspCurr0, *uspRefA0, *uspRefB0, usTemporalThresh;
   MTI_UINT16 *uspCurr1, *uspRefA1, *uspRefB1;
   MTI_UINT16 *uspCurr2, *uspRefA2, *uspRefB2;
   long lDiffA0, lDiffB0, lDiffAB0;
   long lDiffA1, lDiffB1;
   long lDiffA2, lDiffB2;
   float fMotionTol;

   POINTER_STRUCT *psp = &m_afap->ps;

   lMotionThresh = m_afsp->lMotionThresh;
   fMotionTol = m_afsp->fMotionTol;

 /*
         Run through the blob and count the number of pixels that are
         satisfied by the motion.
 */

   uspCurr0 = psp->uspCurr0;

   lRCCA = m_afap->lpMotionRCCA[lMotionA];
   uspRefA0 = psp->uspRefA0;
   lRCCB = m_afap->lpMotionRCCB[lMotionB];
   uspRefB0 = psp->uspRefB0;

 /*
         Defective pixels must have large contrast.  Non-defective pixels
         must have small contrast.
 */

   lNSurround = 0;
   lNSurroundTot = 0;
   lNDefect = 0;
   lNDefectTot = 0;
   lNDefectNeighbor = 0;
   lNDefectNeighborTot = 0;
   lNLayer = 0;
   lNLayerTot = 0;
   for (lBlob = 0; lBlob < m_lNPixelsInBlob; lBlob++)
   {
      if ((m_emergencyStopFlagPtr != NULL) && *m_emergencyStopFlagPtr)
         break;

      lRowCol = m_lpBlobRC[lBlob];
      lRowColCom = lRowCol * m_lNCom;
      lRowColComA = lRowColCom + lRCCA;
      CHECK_ROWCOLCOM_BOUNDS(lRowColComA, m_lNRowColCom);
      lRowColComB = lRowColCom + lRCCB;
      CHECK_ROWCOLCOM_BOUNDS(lRowColComB, m_lNRowColCom);

      if (m_uspPixelMask[lRowCol] & PM_DEFECT_IN_PIXEL)
      {
         usTemporalThresh = m_afap->uspTemporalContrastThresh[uspCurr0[lRowColCom]];
         lNDefectTot++;
         if (m_afsp->lContrastDirection == -1)
         {
            if (uspRefA0[lRowColComA] > usTemporalThresh &&
                uspRefB0[lRowColComB] > usTemporalThresh)
            {
                /* Found desired contrast  */
               lNDefect++;
            }
         }
         else if (m_afsp->lContrastDirection == 1)
         {
            if (uspRefA0[lRowColComA] < usTemporalThresh &&
                uspRefB0[lRowColComB] < usTemporalThresh)
            {
                /* Found desired contrast  */
               lNDefect++;
            }
         }
         else
         {
             /* no contrast is required */
            lNDefect++;
         }
      }
      else
      {
         lDiffA0 = uspCurr0[lRowColCom] - uspRefA0[lRowColComA];
         lDiffB0 = uspCurr0[lRowColCom] - uspRefB0[lRowColComB];
         lDiffAB0 = uspRefA0[lRowColComA] - uspRefB0[lRowColComB];
         if (lDiffA0 < 0)
            lDiffA0 = -lDiffA0;
         if (lDiffB0 < 0)
            lDiffB0 = -lDiffB0;
         if (lDiffAB0 < 0)
            lDiffAB0 = -lDiffAB0;

 /*
         The SURROUND pixels must have small contrast between Curr and
         RefA, Curr and RefB, and RefA and RefB.

         The DEFECT_IN_NEIGHBOR pixels must have small contrast between
         RefA and RefB.
 */

         if (m_uspPixelMask[lRowCol] & PM_DEFECT_IN_NEIGHBOR)
         {
            lNDefectNeighborTot++;
            if (lDiffAB0 < lMotionThresh)
            {
               lNDefectNeighbor++;
            }
         }
         else
         {
            lNSurroundTot++;
            if ((lDiffA0 < lMotionThresh) &&
                (lDiffB0 < lMotionThresh) &&
                (lDiffAB0 < lMotionThresh))
            {
               lNSurround++;
            }
         }
      }
   }

   /*
         If the number of satisfied pixel is large enough, use this motion

         Also, the number of defective pixels must be large enough

         mbraca mod: if the defect is small, this check gets rather random,
         especially when there is lots of grain. So if the defect is
         smaller than the dust size, lets compare the sums instead
         of the individual items
   */

   if ((lNSurround + lNDefectNeighbor + lNDefect) <= m_afsp->lDustMaxSize  &&
       (lNSurround + lNDefectNeighbor + lNDefect + lNLayer) >=
             (fMotionTol * (lNSurroundTot + lNDefectNeighborTot +
                                            lNDefectTot + lNLayerTot))
      )
   {
#ifdef VERBOSE_TRACE
      char stupidBuffer[1001];
      sprintf (stupidBuffer, "TRUE because: dust: %ld >= %ld  sums: %ld >= %f  defect: %ld %ld  thresh: %ld\n",
               m_afsp->lDustMaxSize, (lNSurround + lNDefectNeighbor + lNDefect),
               (lNSurround + lNDefectNeighbor + lNDefect + lNLayer),
               (fMotionTol * (lNSurroundTot + lNDefectNeighborTot + lNDefectTot + lNLayerTot)),
               lNDefect, lNDefectTot, lMotionThresh);
      TRACE_0(errout << stupidBuffer);
#endif
      *bpUseA = true;
      *bpUseB = true;
   }
   else if (
         (lNSurround >= fMotionTol * (float)lNSurroundTot) &&
         (lNDefectNeighbor >= fMotionTol * (float)lNDefectNeighborTot) &&
         (lNDefect >= fMotionTol * (float)lNDefectTot) &&
         (lNLayerTot == 0 || (lNLayer >= fMotionTol * (float)lNLayerTot))
      )
   {
#ifdef VERBOSE_TRACE
      char stupidBuffer2[1001];
      sprintf (stupidBuffer2, "TRUE  because: surround: %ld %.2f  neigh: %ld %.2f  defect: %ld %.2f  thresh: %ld %.2f\n",
               lNSurround,
               fMotionTol * (float)lNSurroundTot,
               lNDefectNeighbor,
               fMotionTol * (float)lNDefectNeighborTot,
               lNDefect,
               fMotionTol * (float)lNDefectTot,
               lMotionThresh,
               fMotionTol);
      TRACE_0(errout << stupidBuffer2);
#endif
      *bpUseA = true;
      *bpUseB = true;
   }
   else
   {
#ifdef VERBOSE_TRACE
      char stupidBuffer3[1001];
      sprintf (stupidBuffer3, "FALSE because: surround: %ld %.2f  neigh: %ld %.2f  defect: %ld %.2f  thresh: %ld %.2f\n",
               lNSurround,
               fMotionTol * (float)lNSurroundTot,
               lNDefectNeighbor,
               fMotionTol * (float)lNDefectNeighborTot,
               lNDefect,
               fMotionTol * (float)lNDefectTot,
               lMotionThresh,
               fMotionTol);
      TRACE_0(errout << stupidBuffer3);
#endif
      *bpUseA = false;
      *bpUseB = false;
   }

   return;
}  /* MotionClassify */
//-------------------------------------------------------------------------

bool CBlobProcessor::MotionApply(long lRCCA, long lRCCB, bool bUseMotionA, bool bUseMotionB)
{
 /*
         Use the motion estimated for the surrounding blob to fill in
         the defective areas
 */
	if ((bUseMotionA || bUseMotionB) == false)
	{
		return false;
	}

	MTI_UINT16 *uspRefA = m_uspSrc[1]; // WRONG POINTER PREVIOUSLY: m_afap->ps.uspRefA0;
	MTI_UINT16 *uspRefB = m_uspSrc[2]; // WRONG POINTER PREVIOUSLY: m_afap->ps.uspRefB0;

	long lPixelMask1 = m_pixelMaskType_lSetting | PM_DEFECT_IN_NEIGHBOR;
	long lPixelMask2 = m_pixelMaskType_lSetting | PM_DEFECT_EDGE_BLUR;

	for (long lBlob = 0; lBlob < m_lNPixelsInBlob; lBlob++)
	{
		if ((m_emergencyStopFlagPtr != NULL) && *m_emergencyStopFlagPtr)
		{
			break;
		}

      long lRowCol = m_lpBlobRC[lBlob];
      if ((m_uspPixelMask[lRowCol] & lPixelMask1) == lPixelMask1 ||
          (m_uspPixelMask[lRowCol] & lPixelMask2) == lPixelMask2)
		{
			long laSum[AF_MAX_COMPONENTS];
			long lCom;
			for (lCom = 0; lCom < AF_MAX_COMPONENTS; lCom++)
         {
				laSum[lCom] = 0;
			}

			long lNSum = 0;
			if (bUseMotionA)
         {
            lNSum++;
            for (lCom = 0; lCom < AF_MAX_COMPONENTS; lCom++)
            {
               long lRowColCom = lRowCol * m_lNCom + (lCom /* WTF? - m_afsp->lActiveComponent */);
               long lRowColComA = lRowColCom + lRCCA;
               CHECK_ROWCOLCOM_BOUNDS(lRowColComA, m_lNRowColCom);
               laSum[lCom] += uspRefA[lRowColComA];
            }
			}

         if (bUseMotionB)
         {
            lNSum++;
            for (lCom = 0; lCom < AF_MAX_COMPONENTS; lCom++)
            {
               long lRowColCom = lRowCol * m_lNCom + (lCom/* WTF?  - m_afsp->lActiveComponent */);
               long lRowColComB = lRowColCom + lRCCB;
               CHECK_ROWCOLCOM_BOUNDS(lRowColComB, m_lNRowColCom);
               laSum[lCom] += uspRefB[lRowColComB];
            }
         }

			if (!lNSum)
			{
				// Can't happen - I check it at start of method!
				continue;
			}

			m_uspPixelMask[lRowCol] |= PM_DEFECT_REPAIRED;

			// Preserve original value of pixel
			if (m_prlpOriginalValues)
			{
				POINT pPoint;
				long lRow = lRowCol / m_lNCol;
				long lCol = lRowCol % m_lNCol;
				pPoint.x = lCol;
				pPoint.y = lRow;
				m_prlpOriginalValues->Add (pPoint, m_uspSrc[0], m_lNCol, m_lNRow, m_lNCom);
			}

         if (m_modifiedPixelMap)
         {
            m_modifiedPixelMap[lRowCol] = 1;
         }

			// Repair - use pixel from frame A, frame B, or (most typically)
			// the average of the two. Also blend with original value if
			// at the edge of the ROI or if doing an edge blend on the fix.

			for (lCom = 0; lCom < AF_MAX_COMPONENTS; lCom++)
			{
				if ((m_afsp->lComponentMask & (1<<lCom)) == 0)
				{
					continue;
				}

				long lRowColCom = lRowCol * m_lNCom + lCom;

				float fNew = (float) laSum[lCom] / (float) lNSum;
				float fBlur = 1.0;
				float fROI = 1.0;

				// Note: "No ROI" may either be indicated by NULL blend
				// array pointer or by an array full of 255s
				if (m_ucpROIBlend != 0 && m_ucpROIBlend[lRowCol] != 255)
				{
					fROI = m_ucpROIBlend[lRowCol] / 255.F;
				}

				// Edge blend (a.k.a. edge "blur") is encoded in low 4 bits
				// of the "pixel mask" flag array (15 = no blend)
				if ((m_uspPixelMask[lRowCol] & lPixelMask2) == lPixelMask2)
				{
					fBlur = (m_uspPixelMask[lRowCol] & PM_EDGE_BLUR_ALPHA_MASK)
								/ 15.F;
				}

				if (fROI != 1.0F || fBlur != 1.0)
				{
					float fOrig = (float) m_uspDst[lRowColCom];
					float fWei = fROI * fBlur;
					fNew = fWei * fNew + (1-fWei) * fOrig;
				}
				m_uspDst[lRowColCom] = fNew + 0.5;
			}
		}
	}

	return true;
}  /* MotionApply */
//-------------------------------------------------------------------------

bool CBlobProcessor::IsEdgePixel(long lRowCol,
							long *neighborOffsets, long neighborCount,
							unsigned short innerMask, unsigned short outerMask)
{
   bool bRet = false;

	// Center pixel must test true for inner mask
   if ((m_uspPixelMask[lRowCol] & innerMask) == innerMask)
    {
     for (int i = 0; i < neighborCount; ++i)
      {
       long lTestRowCol = lRowCol + neighborOffsets[i];
       if (lTestRowCol < 0 || lTestRowCol > m_lNRowCol)
         continue;
   //////////////////////////////////////////////////////////////
   // CAREFUL usually if inner mask is set, so is outer mask!! //
   //////////////////////////////////////////////////////////////
       if ((m_uspPixelMask[lTestRowCol] & outerMask) == outerMask &&
          (m_uspPixelMask[lTestRowCol] & innerMask) != innerMask)
        {
         bRet = true;
         break;
        }
      }
    }

   return bRet;
}
//-------------------------------------------------------------------------

void CBlobProcessor::DoOneInpaintCycle(const SBoundRect &inpaintRegion,
                       int componentsPerPixel,
                       float *inpaintBuffer) //,
                       //const unsigned short fullFramePixelMaskArray[],
                       //long framePitch)
{
   long xPitch = componentsPerPixel;
   long yPitch = inpaintRegion.getWidth() * xPitch;

   // Guaranteed none of the target pixels are in the outermost rows/cols -
   // Not true if the defect touches the edge of the frame, but we don't care
   for (int row = (inpaintRegion.top + 1);
        row <= (inpaintRegion.bottom - 1);
        ++row)
   {
      long frameRowColOffset = (row * m_lNCol) + inpaintRegion.left + 1;
      long bufferRowColOffset =
         ((row - inpaintRegion.top) * inpaintRegion.getWidth()) + 1;

      for (int col = (inpaintRegion.left + 1);
           col <= (inpaintRegion.right - 1);
           ++col)
      {
         const unsigned short targetPixelMask = (PM_DEFECT_SURROUND |
                PM_DEFECT_IN_NEIGHBOR | PM_DEFECT_IN_PIXEL);
         if (m_uspPixelMask[frameRowColOffset] & targetPixelMask)
         {
            float *fpCenter, *fpEast, *fpWest, *fpNorth, *fpSouth,
                  *fpNorthEast, *fpNorthWest, *fpSouthEast, *fpSouthWest;

            fpCenter = &inpaintBuffer[bufferRowColOffset * xPitch];
            fpNorth = fpCenter - yPitch;
            fpSouth = fpCenter + yPitch;
            fpEast  = fpCenter + xPitch;
            fpWest  = fpCenter - xPitch;
            fpNorthEast = fpNorth + xPitch;
            fpSouthEast = fpSouth + xPitch;
            fpNorthWest = fpNorth - xPitch;
            fpSouthWest = fpSouth - xPitch;

            for (int comp = 0; comp < componentsPerPixel; ++comp)
            {
               float fEast, fWest, fNorth, fSouth;
               float fNorm;
               const float stabilizer = 1e-8;

               fEast  = 1.0 /
                        sqrt(FSQR(fpCenter[comp] - fpEast[comp]) +
                             FSQR((fpNorthEast[comp] - fpSouthEast[comp]) / 2.0) +
                             stabilizer);
               fWest  = 1.0 /
                        sqrt(FSQR(fpCenter[comp] - fpWest[comp]) +
                             FSQR((fpNorthWest[comp] - fpSouthWest[comp]) / 2.0) +
                             stabilizer);
               fNorth = 1.0 /
                        sqrt(FSQR(fpCenter[comp] - fpNorth[comp]) +
                             FSQR((fpNorthEast[comp] - fpNorthWest[comp]) / 2.0) +
                             stabilizer);
               fSouth = 1.0 /
                        sqrt(FSQR(fpCenter[comp] - fpSouth[comp]) +
                             FSQR((fpSouthEast[comp] - fpSouthWest[comp]) / 2.0) +
                             stabilizer);

               fNorm = fEast + fWest + fNorth + fSouth;
               fEast  /= fNorm;
               fWest  /= fNorm;
               fNorth /= fNorm;
               fSouth /= fNorm;

               fpCenter[comp] = fEast  * fpEast[comp]  +
                                fWest  * fpWest[comp]  +
                                fNorth * fpNorth[comp] +
                                fSouth * fpSouth[comp];
            }
         }

         ++frameRowColOffset;
         ++bufferRowColOffset;

      } // for each col in this row
   } // for each row

   return;
}
//-------------------------------------------------------------------------

void CBlobProcessor::InpaintApply()
{
   // short circuit so we don't have to check validity of bound rects
   if (m_lNPixelsInBlob < 0)
      return;

   // I HATE frickin' globals with short crappy names
   long &componentsPerPixel = m_lNCom;
   MTI_UINT16 *framePixelData = m_uspSrc[0];
   long &framePitchInPixels = m_lNCol;

   /*
    * Create the inpaint pixel buffer that is a float mirror of the
    * portion of image pixels that is a rectangle covering the
    * entire "blob" representing the defective pixels, their immediate
    * neighbors, and the "surround", as well as enough padding to make
    * inpainting algorithm work.
    *
    * 1. Discover blob boundaries & minimum enclosing rectangle
    */
   SBoundRect blobBoundRect, surroundBoundRect, neighborBoundRect, defectBoundRect;
   SBoundRect &inpaintRegionRect = blobBoundRect;
   for (long pixelIndex = 0; pixelIndex < m_lNPixelsInBlob; ++pixelIndex)
   {
      long lRowCol = m_lpBlobRC[pixelIndex];
      long lRow = lRowCol / m_lNCol;
      long lCol = lRowCol % m_lNCol;
      unsigned short usPixelMaskFlags = m_uspPixelMask[lRowCol];

      blobBoundRect.addPoint(lCol, lRow);
      if ((usPixelMaskFlags & PM_DEFECT_SURROUND) != 0)
         surroundBoundRect.addPoint(lCol, lRow);
      if ((usPixelMaskFlags & PM_DEFECT_IN_NEIGHBOR) != 0)
         neighborBoundRect.addPoint(lCol, lRow);
      if ((usPixelMaskFlags & PM_DEFECT_IN_PIXEL) != 0)
         defectBoundRect.addPoint(lCol, lRow);
   }

   /*
    * 2. Pad the rectangle with a single border of pixels
    */
   inpaintRegionRect.setWidth(inpaintRegionRect.getWidth() + 2);
   inpaintRegionRect.setHeight(inpaintRegionRect.getHeight() + 2);
   inpaintRegionRect.clip(0, m_lNCol-1, 0, m_lNRow-1);

   /*
    * 3. Create the buffer, or make sure existing one is big enough
    */
   long newComponentCount = inpaintRegionRect.getWidth()
                            * inpaintRegionRect.getHeight()
                            * componentsPerPixel;
   if (newComponentCount > m_inpaintBufferSizeInComponents)
   {
      float *tempBuffer = NULL;
      try {
         tempBuffer = new float[newComponentCount];
      } catch (...) {}
      if (tempBuffer == NULL)
         return;
      if (m_inpaintBuffer != NULL)
         delete[] m_inpaintBuffer;
      m_inpaintBuffer = tempBuffer;
      m_inpaintBufferSizeInComponents = newComponentCount;
   }

   /*
    * 4. Move all the pixel data, converting each component to float. Also
    *    initialize the target pixels with a weighted average of the
    *    "surround" pixels relative to their distance from the target pixel
    */
   long bufferIndex = 0;
   int row, col, comp;
   const unsigned short TEST_MASK = (PM_DEFECT_SURROUND |
                                     PM_DEFECT_IN_NEIGHBOR |
                                     PM_DEFECT_IN_PIXEL);
   const unsigned short NEIGHBOR_OR_DEFECT = (PM_DEFECT_IN_NEIGHBOR |
                                              PM_DEFECT_IN_PIXEL);
   const unsigned short SURROUND_ONLY = PM_DEFECT_SURROUND;
   const unsigned short NEIGHBOR_ONLY = PM_DEFECT_IN_NEIGHBOR;

   for (row = inpaintRegionRect.top; row <= inpaintRegionRect.bottom; row++)
   {
      long offsetIntoFrameInPixels = (row * framePitchInPixels + inpaintRegionRect.left);
      long offsetIntoFrameInComponents = componentsPerPixel * offsetIntoFrameInPixels;
      MTI_UINT16 *rowPixelData = framePixelData + offsetIntoFrameInComponents;

      for (col = inpaintRegionRect.left; col <= inpaintRegionRect.right; ++col)
      {
         if (m_uspPixelMask[offsetIntoFrameInPixels] & NEIGHBOR_OR_DEFECT)
         {
            const float noZeroDivide = 1e-8F;
            float accumulator[3] = { 0.F, 0.F, 0.F };
            float norm = noZeroDivide;
            long innerBufferIndex = 0;

            for (long neighborRow = inpaintRegionRect.top;
                 neighborRow <= inpaintRegionRect.bottom;
                 ++neighborRow)
            {
               for (long neighborCol = inpaintRegionRect.left;
                    neighborCol <= inpaintRegionRect.right;
                    ++neighborCol)
               {
                  long frameRowCol = (neighborRow * framePitchInPixels)
                                    + neighborCol;

                  if ((m_uspPixelMask[frameRowCol] & TEST_MASK) == SURROUND_ONLY)
                  {
                     long frameRowColComp = frameRowCol * componentsPerPixel;
                     float weight = 1.0 /
                      (FSQR(neighborRow - row) + FSQR(neighborCol - col) + noZeroDivide);

                     for (comp = 0; comp < componentsPerPixel; ++comp)
                     {
                        accumulator[comp] += weight *
                              float(framePixelData[frameRowColComp+comp]);
                     }
                     norm += weight;
                  }
                  innerBufferIndex += componentsPerPixel;
               }
            }
            if ((m_uspPixelMask[offsetIntoFrameInPixels] & NEIGHBOR_OR_DEFECT)
                                                           == NEIGHBOR_ONLY)
            {
               for (comp = 0; comp < componentsPerPixel; ++comp)
               {
                  m_inpaintBuffer[bufferIndex++] =
                     ((accumulator[comp] / norm) + float(*rowPixelData++)) / 2;
               }
            }
            else
            {
               for (comp = 0; comp < componentsPerPixel; ++comp)
               {
                  m_inpaintBuffer[bufferIndex++] = accumulator[comp] / norm;
                  ++rowPixelData;    // source pixel is completely ignored
               }
            }
         }
         else
         {
            for (comp = 0; comp < componentsPerPixel; comp++)
            {
               m_inpaintBuffer[bufferIndex++] = float(*rowPixelData++);
            }
         }
         ++offsetIntoFrameInPixels;
         offsetIntoFrameInComponents += componentsPerPixel;

      }  // For each column in m_inpaintBuffer
   }  // For each row in m_inpaintBuffer

   /*
    * OK, now iterate the crap out of it!
    */
   const int NumberOfIterations = 250;
   for (int iter = 0; iter < NumberOfIterations; ++iter)
   {
      DoOneInpaintCycle(inpaintRegionRect, componentsPerPixel, m_inpaintBuffer); //,
                        //m_uspPixelMask, framePitchInPixels);
   }

   /*
    * Copy the result to the destination buffer
    */
   long bufferRowColCompOffset = 0;  // Rows are contiguous in the buffer
   for (row = inpaintRegionRect.top; row <= inpaintRegionRect.bottom; ++row)
   {
      long dstRowColOffset = row * framePitchInPixels + inpaintRegionRect.left;
      long dstRowColCompOffset = dstRowColOffset * componentsPerPixel;

      for (col = inpaintRegionRect.left;
           col <= inpaintRegionRect.right;
           ++col)
      {
         long neighborOffsets[4] = {    -m_lNCol,
                                    -1,         +1,
                                        +m_lNCol};
         float fixBlend = 1.0F;
         if ((m_uspPixelMask[dstRowColOffset] & TEST_MASK) == SURROUND_ONLY)
         {
            if (!IsEdgePixel(dstRowColOffset, neighborOffsets, 4,
                             NEIGHBOR_ONLY, SURROUND_ONLY))
               fixBlend = 0.25F;
            else
               fixBlend = 0.50F;
         }
         else  if ((m_uspPixelMask[dstRowColOffset] & NEIGHBOR_OR_DEFECT)
                                                        == NEIGHBOR_ONLY)
         {
            if (IsEdgePixel(dstRowColOffset, neighborOffsets, 4,
                             NEIGHBOR_ONLY, SURROUND_ONLY))
               fixBlend = 0.75F;
            else
               fixBlend = 1.0F;
         }
         float origBlend = 1.0F - fixBlend;

         if (m_uspPixelMask[dstRowColOffset] & TEST_MASK)
         {
            for (comp = 0; comp < componentsPerPixel; comp++)
            {
               m_uspDst[dstRowColCompOffset + comp] = int(
                     (origBlend * m_uspDst[dstRowColCompOffset + comp]) +
                     (fixBlend * m_inpaintBuffer[bufferRowColCompOffset + comp])
                      + 0.5);
            }
            m_uspPixelMask[dstRowColOffset] |= PM_DEFECT_REPAIRED;
         }
         ++dstRowColOffset;
         dstRowColCompOffset += 3;
         bufferRowColCompOffset += 3;
      }
   }

   /*
    * Add original values to the history
    */
   RECT fixRect = inpaintRegionRect.getExclRECT();
   if (m_prlpOriginalValues)
      m_prlpOriginalValues->Add(fixRect, m_uspSrc[0], m_lNCol, m_lNRow, m_lNCom);

   if (m_modifiedPixelMap)
   {
      for (auto row = fixRect.top; row <= fixRect.bottom; ++row)
      {
         for (auto col = fixRect.left; col <= fixRect.right; ++ col)
         {
            auto rowCol = col + (row * m_lNCol);
            m_modifiedPixelMap[rowCol] = 1;
         }
      }
   }

   /*
    * Done!
    */
}
//-------------------------------------------------------------------------

