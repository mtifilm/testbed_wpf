//---------------------------------------------------------------------------

#ifndef GrainModel_32fH
#define GrainModel_32fH
//---------------------------------------------------------------------------

#include "ipp.h"
#include "BlobIdentifier.h"
//---------------------------------------------------------------------------

#define NUMBER_OF_GRAIN_BINS 10
#include "Ippheaders.h"

class GrainModel
{
public:
	static GrainModel CreateFrom(
				Ipp16u *rgb,
				int nRow,
				int nCol,
				int whichChannel,
				Ipp16u maxValue,
				BlobPixelArrayWrapper *blobPixelArrayWrapper,
				MTI_UINT16 pixelFilterMask);

	inline Ipp32fArray getSmoothImage() const  { return _smoothImage; }
	inline Ipp32fArray getDiffsImage() const { return _diffsImage; }
	inline Ipp32f getGrainCenter(int binIndex) const { return _grainCenter[binIndex]; }
	inline Ipp32f getGrainIntensity(int binIndex) const { return _grainIntensity[binIndex]; }

private:
	Ipp32fArray _smoothImage;
	Ipp32fArray _diffsImage;
	Ipp32f _grainCenter[NUMBER_OF_GRAIN_BINS] = {0};
	Ipp32f _grainIntensity[NUMBER_OF_GRAIN_BINS] = {0};

   void makeGrainModel(
      const Ipp32fArray &sourceImage,
      IppiRect roi,
      BlobPixelArrayWrapper *blobPixelArrayWrapper,
      MTI_UINT16 pixelFilterMask);

	static void cleanTheSourceImage(
		Ipp32fArray &sourceImage,
		BlobPixelArrayWrapper *blobPixelArrayWrapper,
		MTI_UINT16 pixelFilterMask);
};

#endif
