#ifndef BlobProcessorH
#define BlobProcessorH

#include "AlgorithmAF.h"


//-------------------------------------------------------------------------

class CBlobProcessor
{
public:

   CBlobProcessor(bool bThreadInternally);
   ~CBlobProcessor();

   inline void Initialize(
      AUTO_FILTER_ALGORITHM *afapArg,
      MTI_UINT16 *uspPixelMask,
      const MTI_UINT8 *ucpROIBlend,
      CPixelRegionList *prlpOriginalValues,
      MotionCacheMap *bestMotionCacheMap,
      MTI_UINT8 *modifiedPixelMap)
   {
      //afapArg->dump();
      m_afap = afapArg;
      MTIassert((m_afap->ps).uspCurr0 != nullptr);
      MTIassert((m_afap->ps).uspRefA0 != nullptr);
      MTIassert((m_afap->ps).uspRefB0 != nullptr);
      m_uspPixelMask = uspPixelMask;
      m_ucpROIBlend = ucpROIBlend;
      m_prlpOriginalValues = prlpOriginalValues;
      m_bestMotionCacheMap = bestMotionCacheMap;
      m_modifiedPixelMap = modifiedPixelMap;
   }

   inline void SetImageInfo(long lNRow, long lNCol, long lNCom,
                            MTI_UINT16 **uspSrc, MTI_UINT16 *uspDst)
   {
      m_lNRow = lNRow;
      m_lNCol = lNCol;
      m_lNCom = lNCom;
      m_lNRowCol = lNRow * lNCol;
      m_lNRowColCom = m_lNRowCol * lNCom;
      for (int i = 0; i < AF_NUM_SRC_FRAMES; ++i)
      {
         m_uspSrc[i] = uspSrc[i];
      }

      m_uspDst = uspDst;
   }

   inline void SetEmergencyStopPtr(const bool *newEmergencyStopPtr)
   {
      m_emergencyStopFlagPtr = newEmergencyStopPtr;
   }

   inline void SetTempStuff(long *lpUnexclRowColComLists, long *lpMotionDiffs[])
   {
      m_lpUnexclRowColComLists = lpUnexclRowColComLists;
      m_lpUnexclRowColComSafeList = NULL;
      m_lpUnexclRowColComUnsafeList = NULL;
      m_lpMotionDiffs[0] = lpMotionDiffs[0];
      m_lpMotionDiffs[1] = lpMotionDiffs[1];
   }

   bool ProcessBlob(long lNPixelsInBlogArg,
                    long *lpBlobRCArg,
                    const AUTO_FILTER_SETTING *afspArg,
                    int pixelMaskTypeArg);

private:

   /*
       Motion
   */
   void SetupMotionFromTrajectories(
                      long lNBlobArg, long *lpBlobRCArg,
                      long lExcludeMask);
   void DetermineSafeLevels(long &lLowerSaveLevel, long &lUpperSafeLevel);
   void MotionSearch (long *lpMotionA, long *lpMotionB,
                      long lNBlobArg, long *lpBlobRCArg,
                      long lLowerSafeLevel, long lUpperSafeLevel,
                      long lExcludeMask);
   void FindBestMotion1(MTI_UINT16 *uspCurr, MTI_UINT16 *uspRef,
                       long lNMotion, long *lpMotionRCC,
                       long lNUnexclRowColComUnsafe,
                       long *lpUnexclRowColComUnsafeList,
                       long *lpMotionDiffs);
   void FindBestMotion2(MTI_UINT16 *uspCurr, MTI_UINT16 *uspRef,
                       long lNMotion, long *lpMotionRCC,
                       long lNUnexclRowColComSafe,
                       long *lpUnexclRowColComSafeList,
                       long *lpMotionDiffs);
   long FindBestMotion3(long lNMotion, long *lpMotionDiffs);
   long FindBestMotion(MTI_UINT16 *uspCurr, MTI_UINT16 *uspRef,
                       long lNMotion, long *lpMotionRCC,
                       long lNUnexclRowColComUnsafe,
                       long *lpUnexclRowColComUnsafeList,
                       long lNUnexclRowColComSafe,
                       long *lpUnexclRowColComSafeList,
                       long *lpMotionDiffs);
   void MotionClassify(long lMotionA, long lMotionB,
                       bool *bpUseA, bool *bpUseB);
	bool MotionApply(long lRCCA, long lRCCB, bool bUseMotionA, bool bUseMotionB);

   /*
      Inpainting
   */
   long m_inpaintBufferSizeInComponents = 0;
   float *m_inpaintBuffer = nullptr;                 // locally allocated & freed (boo!)
   bool IsEdgePixel(long lRowCol, long *neighborOffsets, long neighborCount,
                    unsigned short innerMask, unsigned short outerMask);
   void DoOneInpaintCycle(const SBoundRect &inpaintRegion,
                          int componentsPerPixel,
                          float *inpaintBuffer);
   void InpaintApply();

   /*
      Frame
   */
   long m_lNRow = 0;
   long m_lNCol = 0;
   long m_lNCom = 0;
   long m_lNRowCol = 0;
   long m_lNRowColCom = 0;

   // m_uspSrc is initialized in the constructor
   MTI_UINT16 *m_uspSrc[AF_NUM_SRC_FRAMES];        // r/o - shared
   MTI_UINT16 *m_uspDst = nullptr;                 // shared - no lock needed

   /*
      Processing
   */
   long *m_lpBlobRC = nullptr;                     // r/o
   long m_lNPixelsInBlob = 0;
   const AUTO_FILTER_SETTING *m_afsp = nullptr;    // r/o shared
   int m_pixelMaskType_lSetting = 0;
   MotionCacheMap *m_bestMotionCacheMap = nullptr;
   /*
      Setup
   */
   AUTO_FILTER_ALGORITHM *m_afap = nullptr;          // private
   MTI_UINT16 *m_uspPixelMask = nullptr;             // shared - needs lock
   const MTI_UINT8 *m_ucpROIBlend = nullptr;         // r/o - shared
   CPixelRegionList *m_prlpOriginalValues = nullptr; // shared - needs lock
   MTI_UINT8 *m_modifiedPixelMap = nullptr;          // shared - needs lock
   
   // Temps
   long *m_lpUnexclRowColComLists = nullptr;         // private
   long *m_lpUnexclRowColComSafeList = nullptr;      // private
   long *m_lpUnexclRowColComUnsafeList = nullptr;    // private
   long *m_lpMotionDiffs[2] = { nullptr, nullptr };  // private

   /*
      Abort
   */
   const bool *m_emergencyStopFlagPtr = nullptr;

   /*
     Multithreaded Motion Search
   */
   bool m_bThreadInternally;    // Initialized by constructor
   static void FindBestMotionMT(void *vp, int iJob);

   int m_nMotionThreads = 0;
   MTHREAD_STRUCT m_tsMotionThreadCtrl; // Initialized by constructor

   // CLEAN THIS UP!
   AUTO_FILTER_ALGORITHM *mMT_afap = nullptr;
   MTI_UINT16 *mMT_uspCurr = nullptr;
   MTI_UINT16 *mMT_uspRefA = nullptr;
   MTI_UINT16 *mMT_uspRefB = nullptr;
   long mMT_lNUnexclRowColComSafe = 0;
   long mMT_lNUnexclRowColComUnsafe = 0;
   long *mMT_lpUnexclRowColComSafeList = nullptr;
   long *mMT_lpUnexclRowColComUnsafeList = nullptr;
   long mMT_lBestMotionA[2] = { 0, 0 };
   long mMT_lBestMotionB[2] = { 0, 0 };

};

//-------------------------------------------------------------------------

#endif // __BLOB_PROCESSOR_H__
 