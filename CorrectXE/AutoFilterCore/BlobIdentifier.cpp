#include "BlobIdentifier.h"
#include <stdlib.h>
#include "MTImalloc.h"
#include "MTIio.h"
#include "err_auto_filter.h"

#include <algorithm>  // Include algorithms
using std::sort;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::createBlob(int rowcol)
{
	BlobPixel &head = getBlobPixelAt(rowcol);
	MTIassert(head.link == INVALID_BLOB_PIXEL_ADDRESS);
	MTIassert(head.head == INVALID_BLOB_PIXEL_ADDRESS);
	head.head = rowcol;
	head.link = rowcol;
	++_totalBlobCount;
	++_totalPixelsInBlobs;

//   TRACE_0(errout << "CREATE BLOB #" << _totalBlobCount << " rowcol=" << rowcol);
}
//------------------------------------------------------------------------------

BlobPixelArrayWrapper::BlobPixelArrayWrapper(const BlobPixelArrayWrapper &rhs)
: _blobPixelArrayWidth(rhs._blobPixelArrayWidth)
, _blobPixelArrayHeight(rhs._blobPixelArrayHeight)
, _blobPixelArraySize(rhs._blobPixelArraySize)
, _totalBlobCount(rhs._totalBlobCount)
, _totalPixelsInBlobs(rhs._totalPixelsInBlobs)
, _arrayIsSealed(rhs._arrayIsSealed)
{
   auto arraySizeInBytes = rhs._blobPixelArraySize * sizeof(BlobPixel);
   _blobPixelArray = (BlobPixel *) MTImalloc(arraySizeInBytes);
   MTImemcpy(_blobPixelArray, rhs._blobPixelArray, arraySizeInBytes);
   _weOwnTheBlobPixels = true;
}
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::insertPixelIntoBlob(int blobPixelRowCol, int newBlobPixelRowCol)
{
	BlobPixel &newBlobPixel(getBlobPixelAt(newBlobPixelRowCol));
	MTIassert(newBlobPixel.head == INVALID_BLOB_PIXEL_ADDRESS);
	MTIassert(newBlobPixel.link == INVALID_BLOB_PIXEL_ADDRESS);
	MTIassert(&newBlobPixel != &_invalidBlobPixel);

	int headRowCol = getBlobHeadRowCol(blobPixelRowCol);
	MTIassert(headRowCol == INVALID_BLOB_PIXEL_ADDRESS);
	BlobPixel &headPixel(getBlobPixelAt(headRowCol));
	if (newBlobPixelRowCol < headRowCol)
	{
		// We are replacing the head!!
		newBlobPixel.link = headPixel.link;
		newBlobPixel.head = newBlobPixelRowCol;
		headPixel.link = newBlobPixelRowCol;
		headPixel.head = newBlobPixelRowCol;

		// Fix all the frickin' head links!
		for (BlobPixel *bb = &getBlobPixelAt(newBlobPixel.link);
				bb->link != newBlobPixelRowCol;
				bb = &getBlobPixelAt(bb->link))
		{
			bb->head = newBlobPixelRowCol;
		}

		return;
	}

	// Insertion sort! Should be OK because we're normally adding at the tail.
	BlobPixel *bb;
	for (bb = &getBlobPixelAt(headRowCol);
			bb->link > newBlobPixelRowCol;
			bb = &getBlobPixelAt(bb->link))
	{
		// Just searching.
	}

	newBlobPixel.head = bb->head;
	newBlobPixel.link = bb->link;
	bb->link = newBlobPixelRowCol;

	++_totalPixelsInBlobs;
}
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::mergeBlobs(int rowCol1, int rowCol2)
{
	int headRowCol1 = getBlobHeadRowCol(rowCol1);
	int headRowCol2 = getBlobHeadRowCol(rowCol2);

	// If neither pixel is in a blob, make a blob from one of them because we
	// need a blob to merge into!
	if (!(isInABlob(headRowCol1) || isInABlob(headRowCol2)))
	{
		createBlob(headRowCol1);
	}

	// If one pixel is in a blob the other is not, add the other one to the blob.
	if (!isInABlob(headRowCol1))
	{
		insertPixelIntoBlob(headRowCol2, headRowCol1);
		return;
	}
	else if (!isInABlob(headRowCol2))
	{
		insertPixelIntoBlob(headRowCol1, headRowCol2);
		return;
	}

	// Both pixels are in blobs. If it's the same blob, we don't need to
	// do anything!
	if (headRowCol1 == headRowCol2)
	{
		// Already merged!
		return;
	}

	// Need to merge. We merge the blob with the higher head address into
	// the other blob. Start with the tail, which is what the head's "link"
	// points to.
	BlobPixel &sourceHead = getBlobPixelAt(std::max<int>(headRowCol1, headRowCol2));
	int mergeSourceRowCol = sourceHead.link;

	// Start looking to merge at tail (between the head and the existing tail).
	// NOTE the rowcol's are always sorted head-to-tail, so for each insertion
	// we start looking at the previously inserted bit.
	int destHeadRowCol = std::min<int>(headRowCol1, headRowCol2);
	BlobPixel &destHead = getBlobPixelAt(destHeadRowCol);
	int mergeDestRowCol = destHeadRowCol;

	while (mergeSourceRowCol != INVALID_BLOB_PIXEL_ADDRESS)
	{
		BlobPixel *bb;
		for (bb = &getBlobPixelAt(mergeDestRowCol);
			  bb->link > mergeSourceRowCol;
			  bb = &getBlobPixelAt(mergeDestRowCol))
		{
			// Haven't found insertion point, so walk back towards head.
			mergeDestRowCol = bb->link;
		}

		BlobPixel &newBlobPixel = getBlobPixelAt(mergeSourceRowCol);
		int nextSourceRowCol = (newBlobPixel.head == mergeSourceRowCol)
										? INVALID_BLOB_PIXEL_ADDRESS
										: newBlobPixel.link;
		newBlobPixel.head = bb->head;
		newBlobPixel.link = bb->link;
		bb->link = mergeSourceRowCol;
		mergeDestRowCol = mergeSourceRowCol;
		mergeSourceRowCol = nextSourceRowCol;
	}

	--_totalBlobCount;

//   TRACE_0(errout << "MERGE BLOBS #" << _totalBlobCount << " rowCol1=" << rowCol1 << " rowCol2=" << rowCol2);
}
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::prepareForIteration()
{
	if (_arrayIsSealed)
	{
		// Already prepared
		return;
	}

	_arrayIsSealed = true;
	_totalBlobCount = 0;
	int lastBlobFound = -1;

	for (int rowcol = 0; rowcol < _blobPixelArraySize; ++rowcol)
	{
		if (isHeadOfABlob(rowcol))
		{
			unsigned int count = reverseLinks(rowcol);

			// Space saving hack - negate pixel count and save in 'head' variable.
			_blobPixelArray[rowcol].head = -((int)count);
			++_totalBlobCount;
			lastBlobFound = rowcol;
		}
	}

//	TRACE_0(errout << "PREPARE FOR ITERATION: _totalBlobCount=" << _totalBlobCount << ", lastBlobFound=" << lastBlobFound);
}
//------------------------------------------------------------------------------

unsigned int BlobPixelArrayWrapper::getBlobPixelCount(int rowcol)
{
	// This is MUCH quicker after we've "sealed" the array.
	if (_arrayIsSealed)
	{
		BlobPixel &blobHead = getBlobHead(rowcol);
		MTIassert(blobHead.head < 0);
		return (blobHead.head == INVALID_BLOB_PIXEL_ADDRESS) ? 0 : -blobHead.head;
	}

	BlobPixel &blobHead = getBlobHead(rowcol);
	if (!isValidAddress(blobHead.head))
	{
		return 0;
	}

	unsigned int count = 1;
	int next = blobHead.link;
	int previous = blobHead.head;
	while (isValidAddress(next) && next != previous)
	{
		++count;
		previous = next;
		next = _blobPixelArray[next].link;
	}

	return count;
}
//------------------------------------------------------------------------------

unsigned int BlobPixelArrayWrapper::reverseLinks(int rowcol)
{
	BlobPixel &blobHead = getBlobHead(rowcol);
	MTIassert(isValidAddress(blobHead.head));
	if (!isValidAddress(blobHead.head))
	{
		// this pixel is not in a blob!
		return 0;
	}

//#ifdef _DEBUG
//	MTIostringstream os;
//	os << "BLOB " << rowcol << ":";
//	int pixCount = 0;
//	int pixRowcol = blobHead.head;
//	do {
//		BlobPixel &pix = getBlobPixelAt(pixRowcol);
//		pixRowcol = pix.link;
//		++pixCount;
//		if (pixCount < 11) os << " " << pixRowcol;
//		if (pixCount == 11) os << " ...";
//	} while (pixRowcol != blobHead.head);
//
//	os << " COUNT=" << pixCount;
//	TRACE_0(errout << os.str());
//#endif

	// Note that before reversal, blobHead.link cannot be invalid (points
	// either to the tail or back to itself for a one-pixel blob).
	int oldLink = blobHead.link;
	int newLink = INVALID_BLOB_PIXEL_ADDRESS;
	blobHead.link = INVALID_BLOB_PIXEL_ADDRESS;
	unsigned int count;
	for (count = 0; isValidAddress(oldLink); ++count)
	{
		int savedLink = _blobPixelArray[oldLink].link;
		_blobPixelArray[oldLink].link = newLink;
		newLink = oldLink;
		oldLink = savedLink;
	}

	return count;
}
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::deleteBlob(int rowcol)
{
	int headRowcol = getBlobHeadRowCol(rowcol);
	if (!isValidAddress(headRowcol))
	{
		// this pixel is not in a blob!
		return;
	}

	--_totalBlobCount;

	int next = headRowcol;
	int prev = INVALID_BLOB_PIXEL_ADDRESS;
	while (isValidAddress(next) && next != prev)
	{
		BlobPixel &nextPixel = getBlobPixelAt(next);
		prev = next;
		next = nextPixel.link;
		nextPixel.link = INVALID_BLOB_PIXEL_ADDRESS;
		nextPixel.head = INVALID_BLOB_PIXEL_ADDRESS;
		--_totalPixelsInBlobs;
	}
}
//------------------------------------------------------------------------------

void BlobPixelArrayWrapper::copyFrom(const BlobPixelArrayWrapper &rhs)
{
   MTIassert(_blobPixelArrayWidth > 0);
   MTIassert(_blobPixelArrayHeight > 0);
   MTIassert(_blobPixelArrayWidth == rhs._blobPixelArrayWidth);
   MTIassert(_blobPixelArrayHeight == rhs._blobPixelArrayHeight);
   _totalBlobCount = rhs._totalBlobCount;
   _totalPixelsInBlobs = rhs._totalPixelsInBlobs;
   _arrayIsSealed = rhs._arrayIsSealed;

   auto arraySizeInBytes = _blobPixelArraySize * sizeof(BlobPixel);
   MTImemcpy(_blobPixelArray, rhs._blobPixelArray, arraySizeInBytes);
}
//------------------------------------------------------------------------------

int BlobPixelIterator::getNextPixel()
{
	int result = _next;
	if (_next != INVALID_BLOB_PIXEL_ADDRESS)
	{
		int prev = _next;

		_next = _blobPixelArray->getBlobPixelAt(_next).link;

		if (_next == prev)
		{
			_next = INVALID_BLOB_PIXEL_ADDRESS;
		}
	}

	return result;
}
//------------------------------------------------------------------------------

int BlobIterator::getNextBlob()
{
	while (true)
	{
		if (!_blobPixelArray->isValidAddress(_nextPixelToCheck))
		{
			return INVALID_BLOB_PIXEL_ADDRESS;
		}

		if (_blobPixelArray->isHeadOfABlob(_nextPixelToCheck))
		{
			break;
		}

		++_nextPixelToCheck;
	}

	return _nextPixelToCheck++;
}
//------------------------------------------------------------------------------

void BlobIdentifier::identifyBlobs(
				MTI_UINT16 pixelFlagMask,
				MTI_UINT16 *pixelFlagArray)
{
	int nCols = _blobPixelArray->getBlobPixelArrayWidth();
	int nRows = _blobPixelArray->getBlobPixelArrayHeight();
	_blobPixelArray->initArray();

	for (int row = 0; row < nRows; ++row)
	{
		for (int col = 0; col < nCols; ++col)
		{
			int rowcol = (row * nCols) + col;

			// Should this pixel be put in a blob? Flags for the pixel must
			// exactly match the filter mask.
			if ((pixelFlagArray[rowcol] & pixelFlagMask) == 0) //!= pixelFlagMask)
			{
				// Not in a blob - skip it!
				continue;
			}

			// Create a blob for the pixel.
			_blobPixelArray->createBlob(rowcol);

#ifdef MAKE_BIGGER_BLOBS
			// Examine and possibly merge with the three adjacent pixels in the row above.
			if (row > 0)
			{
				// Col offsets are 0 to 1 on left edge, -1 to 0 on right edge,
				// and -1 to 1 everywhere else.
				int firstColOffset = (col == 0) ? 0 : -1;
				int lastColOffset = (col == (nCols - 1)) ? 0 : 1;
				for (int i = firstColOffset; i <= lastColOffset; ++i)
				{
					int testRowcol = rowcol - nCols + i;
					if (_blobPixelArray->isInABlob(testRowcol))
					{
						_blobPixelArray->mergeBlobs(testRowcol, rowcol);
					}
				}
			}
#else
			// Possibly merge with the pixel DIRECTLY ABOVE this one.
			if (row > 0)
			{
				int testRowcol = rowcol - nCols;
				if (_blobPixelArray->isInABlob(testRowcol))
				{
					_blobPixelArray->mergeBlobs(testRowcol, rowcol);
				}
			}
#endif

			// Possibly merge with the pixel to the left of this one.
			if (col > 0)
			{
				int testRowcol = rowcol - 1;
				if (_blobPixelArray->isInABlob(testRowcol))
				{
					_blobPixelArray->mergeBlobs(testRowcol, rowcol);
				}
			}
		}
	}
}
//------------------------------------------------------------------------------


