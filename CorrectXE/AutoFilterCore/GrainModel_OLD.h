//---------------------------------------------------------------------------

#ifndef GrainModel_OLDH
#define GrainModel_OLDH
//---------------------------------------------------------------------------

#include "ipp.h"
//---------------------------------------------------------------------------

#define NUMBER_OF_GRAIN_BINS 10

class GrainModel
{
public:
	static GrainModel CreateFrom(Ipp16u *sourceImage, int nRows, int nCols, int whichChannel, Ipp16u maxValue);

	inline Ipp32fArray getSmoothImage() const  { return _smoothImage; }
	inline Ipp32fArray getDiffsImage() const { return _diffsImage; }
	inline Ipp32f getGrainCenter(int binIndex) const { return _grainCenter[binIndex]; }
	inline Ipp32f getGrainIntensity(int binIndex) const { return _grainIntensity[binIndex]; }

private:
	Ipp32fArray _smoothImage;
	Ipp32fArray _diffsImage;
	Ipp32f _grainCenter[NUMBER_OF_GRAIN_BINS] = {0.f};
	Ipp32f _grainIntensity[NUMBER_OF_GRAIN_BINS] = {0.f};

	GrainModel makeGrainModel(const Ipp32fArray &Src, IppiRect roi);
	GrainModel makeGrainModelUsingHistogram(const Ipp32fArray &Src, IppiRect roi);
};

#endif