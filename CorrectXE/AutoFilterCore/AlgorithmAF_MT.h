#ifndef AlgorithmAf_MtH
#define AlgorithmAf_MtH

/*
	AlgorithmAF_MT.h	- header file for AutoFilter algorithm
	Kevin Manbeck
	Feb 6, 2003

	The AlgorithmAF_MT class is used to perform auto filter.  It is the
	MULTI-THREADED version of AlgorithmAF.

        Here is the list of public functions.

        NOTE:  These functions should be called in the order described here.

        Initialize (long lNThreadArg)
		-
		- this is used to initialize the AF class. 
		-
		- It should be called from CAutoFilterProc::SetIOConfig()


	SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg)
		-
		- this is used to describe the image characteristics.
		-
		- The lNRowArg and lNColArg are used to describe the total
		- size of the image (including any vertical or horizontal
		- interval).
		-
		- The lActive arguments are used to describe the coordinates
		- of the active portion of the image.  The Start coordinates
        	- are INCLUSIVE.  The Stop coordinates are EXCLUSIVE.
		-
		- It should be called from CAutoFilterProc::SetIOConfig()

	setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel)
		-
		- ucpBlend and ucpLabel are a pointers to arrays indicating how
		- different pixels should be treated.  Set ucpBlend and ucpLabel
		- to NULL to ignore masking.  Or don't call this function to
		- ignore masking.
		-
		- It should be called from CAutoFilterProc::BeginProcessing()

	SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[])
		-
		- lower bound is the smallest value allowed in this format
		- upper bound is the largest value allowed in this format
		-
		- It should be called from CAutoFilterProc::SetIOConfig()

        SetMonoAlpha (bool bHasAlpha)
                -
                - initialize blobs from 1-bit alpha matte
                -

        Set8BitAlpha (bool bHasAlpha)
                -
                - initialize blobs from 8-bit alpha matte
                -

	AssignParameter (CAutoFilterParameters *afp, long lThread)
		-
		- this copies user parameter out of the tool's paramter
		- class into the internal parameter structure.
		-
		- lThread controls to which of the multiple threads this
		- function applies.
		-
		- It should be called from CAutoFilterProc::SetParameters().

	SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex, long lThread)
		-
		- this is used to tell the auto filter algorithm where to find
		- the images to process.
		-
		- uspSrcArg is a list of 3 images.
		- lpFrameIndex is a list of relative frame indices.  Possible
		- values for lpFrameIndex:  (-2, -1, 0), (-1, 0, 1), and
		- (0, 1, 2).  These correspond to past-only, past and future,
		- and future-only.  The decision on which of these three
		- to use should depend on the output of the shot detector.
		-
		- lThread controls to which of the multiple threads this
		- function applies.
		-
		- should be called by CAutoFilterProc::DoProcess()

	SetDstImgPtr (MTI_UINT16 *uspDstArg, long lThread)
		-
		- this is used to tell the auto filter algorithm where
		- to put the results
		-
		- lThread controls to which of the multiple threads this
		- function applies.
		-
		- set uspDstArg to NULL to skip processing on lThread.
		-
		- should be called by CAutoFilterProc::DoProcess()

	SetPixelRegionList (CPixelRegionList *prlpArg, long lThread)
		-
		- this is used to tell the auto filter algorithm where
		- to put original values
		-
		- lThread controls to which of the multiple threads this
		- function applies.
		-
		- should be called by CAutoFilterProc::DoProcess()

	Process ()
		- 
		- this is used to render the results of auto filter
		-
		- should be called by CAutoFilterProc::DoProcess()
*/

////////////////////////////////////////////////////////////////////////

#include "AutoFilterCoreDLL.h"
#include "AlgorithmAF.h"
#include "ImageFormat3.h"
#include "MTImalloc.h"
#include "mthread.h"
#include <string>
using std::string;

////////////////////////////////////////////////////////////////////////

#define AF_MAX_THREAD_COUNT      4        // Maximum number of autofilter
                                          // algorithm processing threads
#define AF_MIN_THREAD_COUNT      2        // Minimum number of threads for
                                          // multi-frame processing

// Maximum number of input and output frames per processing iteration
#define AF_MAX_OUTPUT_FRAMES     (AF_MAX_THREAD_COUNT)
#define AF_MAX_INPUT_FRAMES      (AF_MAX_THREAD_COUNT + 2)
#define AF_MIN_OUTPUT_FRAMES     (AF_MIN_THREAD_COUNT)
#define AF_MIN_INPUT_FRAMES      (AF_MIN_THREAD_COUNT + 2)

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

class MTI_AUTOFILTERCOREDLL_API CAlgorithmAF_MT
 {
public:
  CAlgorithmAF_MT();
  ~CAlgorithmAF_MT();

  int Initialize (const CImageFormat &imageFormat);
  int Initialize (long lNThreadArg, bool bConserveMemoryArg,
                   long lNRowArg, long lNColArg, long lNComArg,
                   long lActiveStartRowArg, long lActiveStopRowArg,
                   long lActiveStartColArg, long lActiveStopColArg);
  int setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel);
  int setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel,
                              long lThread);
  int SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[]);
  int SetAlphaMatteType(EAlphaMatteType alphatype, bool isHalfFloats);
  int SetEmergencyStopFlagPtr (const bool *newEmergencyStopFlagPtr);
  int AssignParameter (CAutoFilterParameters *afp);
  int AssignParameter (CAutoFilterParameters *afp, long lThread);
  int SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex, long lThread);
  void SetDstImgPtr (MTI_UINT16 *uspDstArg, long lThread);
  void SetPixelRegionList (CPixelRegionList *prlpArg, long lThread);
  void SetModifiedPixelMap(MTI_UINT8 *modifiedPixelMap, long lThread);
  int Process ();
  void GetStats(StatsAF *afStats) const;

  static void CallProcess (void *vp, int iJob);

private:

  int SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg);

  long lNRow = 0;
  long lNCol = 0;
        
  // These are per THREAD
  struct SPerThreadData
  {
    CAlgorithmAF aafp;
    AF_PreallocedMemory *aafpmp;
    bool bAvailable;  // This set of data is presently in use

    SPerThreadData()
    : aafpmp(NULL), bAvailable(false)
    {};

	 ~SPerThreadData()
    {
      delete aafpmp;
    }

  } *perThread;
  
  void *csThreadDataCriticalSection;

  // These are per FRAME TO PROCESS (job)
  struct SPerFrameData
   {
    const MTI_UINT8 *ucpBlend;
    MTI_UINT8 *ucpLabel;
    MTI_UINT16 *uspaSrc[AF_NUM_SRC_FRAMES];
    long laFrameIndex[AF_NUM_SRC_FRAMES];
    MTI_UINT16 *uspDst;
    CPixelRegionList *prlp;
    MTI_UINT8 *modifiedPixelMap;

    SPerFrameData()
    : ucpBlend(NULL)
    , ucpLabel(NULL)
    , uspDst(NULL)
    {
       for (int i = 0; i < AF_NUM_SRC_FRAMES; ++i)
       {
          uspaSrc[i] = NULL;
          laFrameIndex[i] = -1;
       }
    };

    ~SPerFrameData()
    {
    };

   } *perFrame;

  MTHREAD_STRUCT msAutoFilter;

  long lNFramesToProcess;
  long lNThread;
  long lOldNFramesToProcess;
  long lOldNThread;
  long lOldMaxThreads;
  bool bThreadExternally;
  bool bThreadsWereAlloced;
  int iProcessError;

public:
  StatsAF _afStats[AF_MAX_SETTINGS];

 };

#endif



