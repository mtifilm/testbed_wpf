/*
   File Name:  AutoFilterCoreDLL.h
   Created Mar 7/2006 by Kurt Tolksdorf

   This just defines the export/import macros
   if MTI_AUTOFILTERCOREDLL is defined, exports are defined
   else imports are defined
*/

#ifndef AUTOFILTERCOREDLL_H
#define AUTOFILTERCOREDLL_H

//////////////////////////////////////////////////////////////////////
#include "machine.h"

#ifdef _WIN32                  // Window compilers need export/import

// Define Import/Export for windows
#ifdef MTI_AUTOFILTERCOREDLL
#define MTI_AUTOFILTERCOREDLL_API  __declspec(dllexport)
#else
#define MTI_AUTOFILTERCOREDLL_API  __declspec(dllimport)
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_AUTOFILTERCOREDLL_API
#endif   // End of UNIX

#endif   // Header file


 