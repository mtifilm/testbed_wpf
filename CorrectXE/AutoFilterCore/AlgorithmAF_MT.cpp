//////////////////////////////////////////////////////////////////////
/*
	AlgorithmAF_MT.cpp   - source file for AutoFilter algorithm
        Kevin Manbeck
        Feb 6, 2003

        The AlgorithmAF_MT class is used to perform auto filter.  See
	AlgorithmAF_MT.h for more detailed description.
*/
//////////////////////////////////////////////////////////////////////

#include "AlgorithmAF_MT.h"
#include "err_auto_filter.h"
#include "IniFile.h"            // for TRACE_N()
#include "MTImalloc.h"          // for flying spaghetti stuff
#include "SysInfo.h"            // for AvailableProcessorCoreCount()

//////////////////////////////////////////////////////////////////////

CAlgorithmAF_MT::CAlgorithmAF_MT()
{
  perThread = 0;
  perFrame = 0;
  lOldNFramesToProcess = -1;
  lNFramesToProcess = 0;
  lOldNThread = -1;
  lOldMaxThreads = -1;
  lNThread = 0;
  bThreadsWereAlloced = false;
  bThreadExternally = false;

   csThreadDataCriticalSection = CriticalSectionAlloc();

}  /* CAlgorithmAF_MT */

CAlgorithmAF_MT::~CAlgorithmAF_MT()
{
  delete [] perThread;
  perThread = 0;
  lNThread = 0;
  lOldNThread = 0;

  delete [] perFrame;
  perFrame = 0;
  lNFramesToProcess = 0;
  lOldNFramesToProcess = 0;

  if (bThreadsWereAlloced)
   {
    if (MThreadFree (&msAutoFilter))
     {
      TRACE_0(errout << "ERROR: ~CAlgorithmAF_MT: MThreadFree failed!!!");
      MTIassert(false);
     }
   }

  CriticalSectionFree(csThreadDataCriticalSection);

}  /* ~CAlgorithmAF_MT */

int CAlgorithmAF_MT::Initialize(const CImageFormat &imageFormat)
{
   int retVal = 0;
   RECT activePictureRect = imageFormat.getActivePictureRect();
   MTI_UINT16 maxDataValue[3];
   imageFormat.getComponentValueMax(maxDataValue);
   retVal = Initialize(AF_MAX_THREAD_COUNT, false, imageFormat.getTotalFrameHeight(),
         imageFormat.getTotalFrameWidth(), imageFormat.getComponentCountExcAlpha(), activePictureRect.top,
         activePictureRect.bottom + 1, activePictureRect.left, activePictureRect.right + 1);
   if (retVal != 0)
   {
      return retVal;
   }

   // Component bounds
   MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
   MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];
   imageFormat.getComponentValueMin(cvMin);
   imageFormat.getComponentValueMax(cvMax);
   retVal = SetImageBounds(cvMin, cvMax);
   if (retVal != 0)
   {
      return retVal;
   }

   // Other crap
   SetAlphaMatteType(imageFormat.getAlphaMatteType(), imageFormat.getColorSpace() == IF_COLOR_SPACE_EXR);
   SetEmergencyStopFlagPtr(nullptr);

   return retVal;
}

 int CAlgorithmAF_MT::Initialize (
    long lNThreadArg, bool bConserveMemoryArg,
    long lNRowArg, long lNColArg, long lNComArg,
    long lActiveStartRowArg, long lActiveStopRowArg,
    long lActiveStartColArg, long lActiveStopColArg)
{
  int iRet;

	if (lNThreadArg <= 0)
	 return ERR_PLUGIN_AF_BAD_THREAD;

  lNFramesToProcess = lNThreadArg; // May not be able to run that many threads!

  lNRow = lNRowArg;
  lNCol = lNColArg;

  auto lNRowCol = lNColArg * lNRowArg;
  MTIassert(lNRowCol <= ROW_COL_10K);
  size_t preallocSize = (lNRowCol <= ROW_COL_2K)
								? sizeof(AF_PreallocedMemory_2K)
								: ((lNRowCol <= ROW_COL_4K)
								  ? sizeof(AF_PreallocedMemory_4K)
								  : ((lNRowCol <= ROW_COL_6K)
									 ? sizeof(AF_PreallocedMemory_6K)
									 : ((lNRowCol <= ROW_COL_8K)
										? sizeof(AF_PreallocedMemory_8K)
										: sizeof(AF_PreallocedMemory_10K))));

  // OK figure out how many we can actually run by trying to preallocate
  // memory for each one (except the first thread, which gets its memory
  // from the flying spaghetti buffer, so we are assured we can run at least
  // one thread!

  // Wait - maybe there is no FSB?
  long lThread;
  long lNProcessors = SysInfo::AvailableProcessorCoreCount();
#if 0 //def _DEBUG
  long maxThreads = 1;
#else
  long maxThreads = lNFramesToProcess;
#endif
  if (bConserveMemoryArg && (maxThreads > ((lNProcessors + 1) / 2)))
     maxThreads = (lNProcessors + 1) / 2;

  // We allocate one perThread data struct per FRAME instead - in reality
  // the number of frames may exceed the number of threads
  if (lOldMaxThreads != maxThreads)
  {
     if (perThread != NULL)
        delete [] perThread;
	  perThread = new SPerThreadData[maxThreads];
  }

  for (lThread = 0; lThread < maxThreads; ++lThread)
   {
	  auto lNRowCol = lNColArg * lNRowArg;
	  MTIassert(lNRowCol <= ROW_COL_10K);
	  perThread[lThread].aafpmp = (lNRowCol <= ROW_COL_2K)
											? (AF_PreallocedMemory *) (new AF_PreallocedMemory_2K)
											: ((lNRowCol <= ROW_COL_4K)
											  ? (AF_PreallocedMemory *) (new AF_PreallocedMemory_4K)
											  : ((lNRowCol <= ROW_COL_6K)
												 ? (AF_PreallocedMemory *) (new AF_PreallocedMemory_6K)
												 : ((lNRowCol <= ROW_COL_8K)
													? (AF_PreallocedMemory *) (new AF_PreallocedMemory_8K)
													: (AF_PreallocedMemory *) (new AF_PreallocedMemory_10K))));

    if (perThread[lThread].aafpmp != NULL)
     {
      perThread[lThread].bAvailable = true;
     }
    else
     {
      break;
     }
   }

  lNThread = lThread;

  bThreadExternally = (lNThread > 1);

  // If we aren't fully multi-threaded at the high level (multiple frames
  // simultaneously), either because we're operating in 4K or because
  // we are doing a single frame, then tell the algorithm to do some
  // internal threading. But if we are using all processors already,
  // tell the algorithm to not thread internally because that would just
  // slow us down
#if 0 //def _DEBUG
  bool bThreadInternally = false;
#else
  bool bThreadInternally = (lNThread < lNProcessors);
#endif

//   DBTRACE(lNThread);

  // Allocate stuff that needs to be stashed until Process time
  if (lOldNFramesToProcess != lNFramesToProcess)
  {
     // We must force reallocation of the threads because a thread
     // parameter changed - QQQ is this true?
     lOldNThread = -1;

     // If not the first time, delete old stuff
     if (perFrame != NULL)
        delete [] perFrame;

     // Allocate new per-frame data block
     perFrame = new SPerFrameData[lNFramesToProcess];
     lOldNFramesToProcess = lNFramesToProcess;
  }

  // Initialize each per-frame processing thread
  for (lThread = 0; lThread < lNThread; lThread++)
   {
	 perThread[lThread].aafp.Initialize (bThreadInternally, perThread[lThread].aafpmp);
   }

  // set up the multithreading
  msAutoFilter.PrimaryFunction = &CallProcess;
  msAutoFilter.SecondaryFunction = NULL;
  msAutoFilter.CallBackFunction = NULL;
  msAutoFilter.vpApplicationData = this;
  msAutoFilter.iNThread = lNThread;
  msAutoFilter.iNJob = lNFramesToProcess;

  if (bThreadExternally)
   {
    if (lOldNThread != lNThread)
     {
      if (bThreadsWereAlloced)
       {
        if (MThreadFree (&msAutoFilter))
         {
          TRACE_0(errout << "ERROR: CAlgorithmAF_MT: MThreadFree failed!!!");
         }
        bThreadsWereAlloced = false;
       }
      if (MThreadAlloc (&msAutoFilter))
       {
        TRACE_0(errout << "ERROR: CAlgorithmAF_MT: MThreadAlloc failed!!!");
        bThreadExternally = false;
       }
      else
       {
        bThreadsWereAlloced = true;
        lOldNThread = lNThread;
       }
     }
   }

  // Tell each thread what the dimensions are
  iRet = SetDimension(lNRowArg, lNColArg, lNComArg,
                      lActiveStartRowArg, lActiveStopRowArg,
							 lActiveStartColArg, lActiveStopColArg);
  return iRet;

}  /* Initialize */

int CAlgorithmAF_MT::SetDimension (long lNRowArg, long lNColArg, long lNComArg,
	long lActiveStartRowArg, long lActiveStopRowArg,
	long lActiveStartColArg, long lActiveStopColArg)
{
  int iRet;

  if (lNThread <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = perThread[lThread].aafp.SetDimension(lNRowArg, lNColArg, lNComArg,
		lActiveStartRowArg, lActiveStopRowArg,
		lActiveStartColArg, lActiveStopColArg);
    if (iRet)
      return iRet;
   }

  return 0;
}  /* SetDimension */

int CAlgorithmAF_MT::setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend,
		MTI_UINT8 *ucpLabel)
{
  if (lNFramesToProcess <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (long lFrame = 0; lFrame < lNFramesToProcess; lFrame++)
   {
    setRegionOfInterestPtr(ucpBlend, ucpLabel, lFrame);
   }

  return 0;
}  /* setRegionOfInterestPtr */

int CAlgorithmAF_MT::setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend,
                                             MTI_UINT8 *ucpLabel,
                                             long lFrame)
{
  if (lFrame < 0 || lFrame >= lNFramesToProcess)
    return ERR_PLUGIN_AF_BAD_THREAD;

  // Just save these - need to apply them at "process' time
  //perThread[lThread].aafp.setRegionOfInterestPtr (ucpBlend, ucpLabel);
  perFrame[lFrame].ucpBlend = ucpBlend;
  perFrame[lFrame].ucpLabel = ucpLabel;

  return 0;
}  /* setRegionOfInterestPtr */

int CAlgorithmAF_MT::SetImageBounds (MTI_UINT16 spLowerBoundArg[],
                                     MTI_UINT16 spUpperBoundArg[])
{
  int iRet;

  if (lNThread <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = perThread[lThread].aafp.SetImageBounds(spLowerBoundArg, spUpperBoundArg);
    if (iRet)
      return iRet;
   }

  return 0;
}  /* SetImageBounds */

int CAlgorithmAF_MT::SetAlphaMatteType (EAlphaMatteType alphatype, bool isHalfFloats)
{
  int iRet;

  if (lNThread <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = perThread[lThread].aafp.SetAlphaMatteType(alphatype, isHalfFloats);
    if (iRet)
      return iRet;
   }

  return 0;
}  /* SetAlphaMatteType */

int CAlgorithmAF_MT::SetEmergencyStopFlagPtr (const bool *newEmergencyStopFlagPtr)
{
  if (lNThread <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    perThread[lThread].aafp.SetEmergencyStopFlagPtr(newEmergencyStopFlagPtr);
   }

  return 0;
}  /* SetEmergencyStopFlagPtr */

int CAlgorithmAF_MT::AssignParameter (CAutoFilterParameters *app)
{
  int iRet = 0;

  for (long lThread = 0; iRet == 0 && lThread < lNThread; lThread++)
   {
//DBTRACE(lThread);
    iRet = AssignParameter (app, lThread);
//DBLINE;
   }

  return iRet;
}  /* AssignParameter */

int CAlgorithmAF_MT::AssignParameter (CAutoFilterParameters *app, long lThread)
{
  if (lThread < 0 || lThread >= lNThread)
    return ERR_PLUGIN_AF_BAD_THREAD;

  return perThread[lThread].aafp.AssignParameter(app);
}  /* AssignParameter */

int CAlgorithmAF_MT::SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex,
		long lFrame)
{
  if (lFrame < 0 || lFrame >= lNFramesToProcess)
    return ERR_PLUGIN_AF_BAD_THREAD;

  for (int i = 0; i < AF_NUM_SRC_FRAMES; ++i)
   {
    perFrame[lFrame].uspaSrc[i] = uspSrcArg[i];
    perFrame[lFrame].laFrameIndex[i] = lpFrameIndex[i];
   }

  return 0;
}  /* SetSrcImgPtr */

void CAlgorithmAF_MT::SetDstImgPtr (MTI_UINT16 *uspDstArg, long lFrame)
{
  if (lFrame < 0 || lFrame >= lNFramesToProcess)
    return;        // just a tad inconsistent!!

  // Need to apply it at process time!
  // perThread[lThread].aafp.SetDstImgPtr(uspDstArg);
  perFrame[lFrame].uspDst = uspDstArg;

}  /* SetDstImgPtr */

void CAlgorithmAF_MT::SetPixelRegionList (CPixelRegionList *prlpArg, long lFrame)
{
  if (lFrame < 0 || lFrame >= lNFramesToProcess)
    return;        // just a tad inconsistent!!

  // Need to apply it at process time!
  // perThread[lThread].aafp.SetPixelRegionList(prlpArg);
  perFrame[lFrame].prlp = prlpArg;

  return;
}  /* SetPixelRegionList */

void CAlgorithmAF_MT::SetModifiedPixelMap(MTI_UINT8 *modifiedPixelMap, long lFrame)
{
   if (lFrame < 0 || lFrame >= lNFramesToProcess)
   {
      return;     // just a tad inconsistent!
   }

   perFrame[lFrame].modifiedPixelMap = modifiedPixelMap;
}

int CAlgorithmAF_MT::Process ()
{
  int iRet;

  if (lNThread <= 0 || lNFramesToProcess <= 0)
    return ERR_PLUGIN_AF_BAD_THREAD;

  iProcessError = 0;

  if (bThreadExternally)
   {
    iRet = MThreadStart (&msAutoFilter);
    if (iRet)
      return iRet;
   }
  else
   {
    // There's not enough memory for simultaneous processing - do sequentially
    for (int iJob = 0; iJob < msAutoFilter.iNJob; ++iJob)
     {
		CallProcess (this, iJob);
	  }
   }

  return iProcessError;

}  /* Process */

/* static */ void CAlgorithmAF_MT::CallProcess (void *vp, int iJob)
{
  int iRet;
  CAlgorithmAF_MT *aafmt;

  aafmt = (CAlgorithmAF_MT *)vp;

  // Safely find a non-busy AlgorithmAF
  long lThread;
  CriticalSectionEnter(aafmt->csThreadDataCriticalSection);
  for (lThread = 0;
		 (lThread < aafmt->lNThread) && (!aafmt->perThread[lThread].bAvailable);
		 ++lThread)
	{
	 ;// Just lookin'
	}

  if (lThread >= aafmt->lNThread)
	{
	 // oh jeez... now what?
	 TRACE_0(errout << "INTERNAL ERROR: More autofilter threads than data!!");
	 CriticalSectionExit(aafmt->csThreadDataCriticalSection);
	 return;
	}
  try {

	  if (lThread < aafmt->lNThread)
		  aafmt->perThread[lThread].bAvailable = false;

	  CriticalSectionExit(aafmt->csThreadDataCriticalSection);

	  // Apply deferred stuff
	  aafmt->perThread[lThread].aafp.setRegionOfInterestPtr (
															aafmt->perFrame[iJob].ucpBlend,
															aafmt->perFrame[iJob].ucpLabel);
	  aafmt->perThread[lThread].aafp.SetSrcImgPtr(aafmt->perFrame[iJob].uspaSrc,
																 aafmt->perFrame[iJob].laFrameIndex);
	  aafmt->perThread[lThread].aafp.SetDstImgPtr(aafmt->perFrame[iJob].uspDst);
	  aafmt->perThread[lThread].aafp.SetPixelRegionList(aafmt->perFrame[iJob].prlp);

  // Use try/catch to make sure we get to mark the thread as
  // available for more processing
     GrainFilterSet grainFilters;
	  iRet = aafmt->perThread[lThread].aafp.Process(grainFilters);
	  aafmt->perThread[lThread].aafp.GetStats(aafmt->_afStats);
  }
  catch (...)
  {
	  TRACE_0(errout << "EXCEPTION CAUGHT: in AlgorithmAF::Process");
	  iRet = 665;
  }

  aafmt->perThread[lThread].bAvailable = true;

  if (iRet != 0 && aafmt->iProcessError == 0)
	 aafmt->iProcessError = iRet;

}  /* CallProcess */


//-------------------------------------------------------------------------

void CAlgorithmAF_MT::GetStats(StatsAF *afStats) const
{
	for (int i = 0; i < AF_MAX_SETTINGS; ++i)
	{
		afStats[i] = _afStats[i];
	}
}
//-------------------------------------------------------------------------
