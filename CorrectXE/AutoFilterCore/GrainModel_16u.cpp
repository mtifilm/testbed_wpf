//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainModel_16u.h"

#include "HistogramIPP.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "math.h"
#include "SynchronousThreadRunner.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define USE_HISTOGRAM_INSTEAD_OF_SORTING
#define NUMBER_OF_HISTOGRAM_BINS 65535

const int KERNEL_RADIUS = 3;
const double IMAGE_CENTER = .8;
const double PERCENTILE_HIGH = .8;
const double PERCENTILE_LOW = .2;

const float breakTable[20] = { 0.02f, 0.08f, 0.12f, 0.18f, 0.22f, 0.28f, 0.32f, 0.38f, 0.42f, 0.48f, 0.52f, 0.58f, 0.62f, 0.68f, 0.72f, 0.78f, 0.82f, 0.88f, 0.92f, 0.98f };
//------------------------------------------------------------------------------

namespace
{
//---------------------------

#define MULTITHREAD_INTENSITY
#ifdef MULTITHREAD_INTENSITY

struct IntensityParams
{
	vector<float> *binDiffs;
   Ipp16u *grainIntensities;
};

int RunThreadToComputeIntensityForOneBin(void *vp, int sliceNumber)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<IntensityParams *>(vp);
   auto binDiffs = params->binDiffs;
   auto grainIntensities = params->grainIntensities;
   auto bin = sliceNumber;

   if (binDiffs[bin].size() > 1)
   {
      std::sort(binDiffs[bin].begin(), binDiffs[bin].end());
      int highIndex = my_lround(PERCENTILE_HIGH * (binDiffs[bin].size() - 1));
      int lowIndex = my_lround(PERCENTILE_LOW * (binDiffs[bin].size() - 1));
      grainIntensities[bin] = binDiffs[bin][highIndex] - binDiffs[bin][lowIndex];
   }

	 return 0;
}

#endif

const int NumberOfSlices = 32;

IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
{
   IppiRect sliceRoi;
   int nominalRowsPerSlice = height / NumberOfSlices;
   int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
   sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
   sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
   sliceRoi.x = 0;
   sliceRoi.width = width;

   return sliceRoi;
}
//---------------------------

//#define MULTITHREAD_IMPORT
#ifdef MULTITHREAD_IMPORT

struct ImportParams
{
   Ipp16uArray yArray;
   MTI_UINT16 *rgb;
   int whichChannel;
   MTI_UINT16 maxValue;
};

int RunThreadToImportRawData(void *vp, int sliceNumber)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<ImportParams *>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->yArray.getRows(), params->yArray.getCols(), sliceNumber);
	Ipp16uArray yArray(params->yArray, sliceRoi);
   int nRow = yArray.getRows();
   int nCol = yArray.getCols();
   int whichChannel = params->whichChannel;
   MTI_UINT16 *rgb = params->rgb + (sliceRoi.y * sliceRoi.width * 3);
   MTI_UINT16 maxValue = params->maxValue;

	if (whichChannel < 3)
	{
		yArray.importOneNormalizedChannelFromRgb(rgb, nRow, nCol, whichChannel, maxValue);
   }
	else
	{
		yArray.importNormalizedYuvFromRgb(rgb, nRow, nCol, maxValue);
	}

   return 0;
}

#endif

} // anonymous namespace
//------------------------------------------------------------------------------

// function [GrainCenter, GrainIntensity] = MakeGrainModel (RGB, chan)
// %
// %  Grain intensity is a function of image intensity.
// %  This function measures how grain values are affected
// %  by underlying signal intensity.
// %
/* static */
GrainModel GrainModel::CreateFrom(Ipp16u *rgb, int nRow, int nCol, int whichChannel, Ipp16u maxValue)
{
	// % extract the channel to use
	//	if chan == 4
	//		 Src = rgb2gray (RGB(:,:,1:3));
	//	else
	//		 Src = RGB(:,:,chan);
	//	end
//   CHRTimer GRAIN_MODEL_IMPORT_TIMER;

	MTIassert(whichChannel >= 0 && whichChannel <= 3);
	Ipp16uArray yArray(nRow, nCol);

#ifdef MULTITHREAD_IMPORT
   ImportParams importParams = { yArray, rgb, whichChannel, maxValue };
   SynchronousThreadRunner multithread(NumberOfSlices, &importParams, RunThreadToImportRawData);
   multithread.Run();
#else
	if (whichChannel < 3)
	{
		yArray.importOneNormalizedChannelFromRgb(rgb, nRow, nCol, whichChannel, maxValue);
   }
	else
	{
		yArray.importNormalizedYuvFromRgb(rgb, nRow, nCol, maxValue);
	}
#endif

//   DBTRACE(GRAIN_MODEL_IMPORT_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_MAKE_TIMER;

	// % the coordinates of the central portion.  This is done to avoid mattes
	// % and other edge effects.  This should be skipped if there is a reliable
	// % region of interest.
	// [nRow, nCol, ~] = size(RGB);
	// row0 = round(1 + nRow*(1-IMAGE_CENTER)/2);
	// row1 = round(nRow - nRow*(1-IMAGE_CENTER)/2);
	// col0 = round(1 + nCol*(1-IMAGE_CENTER)/2);
	// col1 = round(nCol - nCol*(1-IMAGE_CENTER)/2);
	int roiX = my_lround(nCol * (1 - IMAGE_CENTER) / 2);
	int roiW = my_lround(nCol - nCol * (1 - IMAGE_CENTER) / 2) - roiX;
	int roiY = my_lround(nRow * (1 - IMAGE_CENTER) / 2);
	int roiH = my_lround(nRow - nRow * (1 - IMAGE_CENTER) / 2) - roiY;
	IppiRect roi = { roiX, roiY, roiW, roiH };

	GrainModel grainModel;
	grainModel.makeGrainModel(yArray, roi);

//   DBTRACE(GRAIN_MODEL_MAKE_TIMER.ReadAsString());

	return grainModel;
}
//------------------------------------------------------------------------------

GrainModel GrainModel::makeGrainModel(const Ipp16uArray &sourceImage, IppiRect roi)
{
	MTIassert(sourceImage.getComponents() == 1);
	GrainModel grainModel;

//   CHRTimer GRAIN_MODEL_SMOOTH_TIMER;

	// % the smoothing kernel
	// Kern = ones(2*KERNEL_RADIUS+1,2*KERNEL_RADIUS+1);
	// Kern = Kern / sum(Kern(:));

	// % the smoothed image.  Grain is removed.
	// Src_smooth = conv2(Src,Kern,'same');
	IppiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };
	_smoothImage = sourceImage.applyBoxFilter(boxSize);

//   DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_DIFFS_TIMER;

	// % the difference image.  Un-regularized grain signal.
	// Src_diff = Src - Src_smooth;
	_diffsImage = sourceImage - _smoothImage;

//   DBTRACE(GRAIN_MODEL_DIFFS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_HISTOGRAM_TIMER;

	// % extract the central portions
   // Src_smooth = Src_smooth(row0:row1,col0:col1);
   // Src_diff = Src_diff(row0:row1,col0:col1);
	Ipp16uArray smoothImageWithRoi(_smoothImage, roi);
	Ipp16uArray diffsImageWithRoi(_diffsImage, roi);

   // Compute intensity histogram.
	HistogramIpp smoothHistogramGenerator(smoothImageWithRoi, NUMBER_OF_HISTOGRAM_BINS, 0, 65535);
   auto smoothHistogramAllChans = smoothHistogramGenerator.compute(smoothImageWithRoi);
   auto smoothHistogram = smoothHistogramAllChans[0];

//   DBTRACE(GRAIN_MODEL_HISTOGRAM_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_SETUP_BINS_TIMER;

	int numberOfElements = smoothImageWithRoi.area();
	double grainBinRadius = 65535 * (numberOfElements / (2.0  * NUMBER_OF_GRAIN_BINS));
   double histoBinIntensityQuantum = 65535.0 / NUMBER_OF_HISTOGRAM_BINS;

	Ipp16u grainLeft[NUMBER_OF_GRAIN_BINS];
	Ipp16u grainRight[NUMBER_OF_GRAIN_BINS];

   // We need to break twice per histo bin, once on the bin boundary and
   // once at the center of the bin.
   //
   // Special case first break, left edge of first grain bin.
   auto grainBin = 0;
   grainLeft[grainBin] = 0;
   size_t runningSum = 0;
   int breakCount = 1;

   // Breaks between first and last.
   int nextBreak = grainBinRadius;
   const int totalNumberOfBreaks = (2 * NUMBER_OF_GRAIN_BINS) + 1;
   for (auto histoBin = 1; histoBin < NUMBER_OF_HISTOGRAM_BINS; ++histoBin)
   {
      runningSum += smoothHistogram[histoBin];
      if (runningSum >= nextBreak)
      {
         double histoBinIntensity = histoBin * histoBinIntensityQuantum;
	MTIassert(sourceImage.getComponents() == 1);
	GrainModel grainModel;

//   CHRTimer GRAIN_MODEL_SMOOTH_TIMER;
//   DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());

	// % the smoothing kernel
	// Kern = ones(2*KERNEL_RADIUS+1,2*KERNEL_RADIUS+1);
	// Kern = Kern / sum(Kern(:));

	// % the smoothed image.  Grain is removed.
	// Src_smooth = conv2(Src,Kern,'same');
	IppiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };
	_smoothImage = sourceImage.applyBoxFilter(boxSize);

//   DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_DIFFS_TIMER;

	// % the difference image.  Un-regularized grain signal.
	// Src_diff = Src - Src_smooth;
	_diffsImage = sourceImage - _smoothImage;

//   DBTRACE(GRAIN_MODEL_DIFFS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_HISTOGRAM_TIMER;

	// % extract the central portions
   // Src_smooth = Src_smooth(row0:row1,col0:col1);
   // Src_diff = Src_diff(row0:row1,col0:col1);
	Ipp32fArray smoothImageWithRoi(_smoothImage, roi);
	Ipp32fArray diffsImageWithRoi(_diffsImage, roi);

   // Compute intensity histogram.
	HistogramIpp smoothHistogramGenerator(smoothImageWithRoi, NUMBER_OF_HISTOGRAM_BINS, 0.F, 1.0001F);
   auto smoothHistogramAllChans = smoothHistogramGenerator.compute(smoothImageWithRoi);
   auto smoothHistogram = smoothHistogramAllChans[0];

//   DBTRACE(GRAIN_MODEL_HISTOGRAM_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_SETUP_BINS_TIMER;

	int numberOfElements = smoothImageWithRoi.area();
	double grainBinRadius = numberOfElements / (2.0  * NUMBER_OF_GRAIN_BINS);
   double histoBinIntensityQuantum = 1.0 / NUMBER_OF_HISTOGRAM_BINS;

	Ipp32f grainLeft[NUMBER_OF_GRAIN_BINS];
	Ipp32f grainRight[NUMBER_OF_GRAIN_BINS];

   // We need to break twice per histo bin, once on the bin boundary and
   // once at the center of the bin.
   //
   // Special case first break, left edge of first grain bin.
   auto grainBin = 0;
   grainLeft[grainBin] = 0;
   size_t runningSum = 0;
   int breakCount = 1;

   // Breaks between first and last.
   int nextBreak = grainBinRadius;
   const int totalNumberOfBreaks = (2 * NUMBER_OF_GRAIN_BINS) + 1;
   for (auto histoBin = 1; histoBin < NUMBER_OF_HISTOGRAM_BINS; ++histoBin)
   {
      runningSum += smoothHistogram[histoBin];
      if (runningSum >= nextBreak)
      {
         double histoBinIntensity = histoBin * histoBinIntensityQuantum;
	MTIassert(sourceImage.getComponents() == 1);
	GrainModel grainModel;

//   CHRTimer GRAIN_MODEL_SMOOTH_TIMER;
//   DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());

	// % the smoothing kernel
	// Kern = ones(2*KERNEL_RADIUS+1,2*KERNEL_RADIUS+1);
	// Kern = Kern / sum(Kern(:));

	// % the smoothed image.  Grain is removed.
	// Src_smooth = conv2(Src,Kern,'same');
	IppiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };
	_smoothImage = sourceImage.applyBoxFilter(boxSize);

//   DBTRACE(GRAIN_MODEL_SMOOTH_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_DIFFS_TIMER;

	// % the difference image.  Un-regularized grain signal.
	// Src_diff = Src - Src_smooth;
	_diffsImage = sourceImage - _smoothImage;

//   DBTRACE(GRAIN_MODEL_DIFFS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_HISTOGRAM_TIMER;

	// % extract the central portions
   // Src_smooth = Src_smooth(row0:row1,col0:col1);
   // Src_diff = Src_diff(row0:row1,col0:col1);
	Ipp32fArray smoothImageWithRoi(_smoothImage, roi);
	Ipp32fArray diffsImageWithRoi(_diffsImage, roi);

   // Compute intensity histogram.
	HistogramIpp smoothHistogramGenerator(smoothImageWithRoi, NUMBER_OF_HISTOGRAM_BINS, 0.F, 1.0001F);
   auto smoothHistogramAllChans = smoothHistogramGenerator.compute(smoothImageWithRoi);
   auto smoothHistogram = smoothHistogramAllChans[0];

//   DBTRACE(GRAIN_MODEL_HISTOGRAM_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_SETUP_BINS_TIMER;

	int numberOfElements = smoothImageWithRoi.area();
	double grainBinRadius = numberOfElements / (2.0  * NUMBER_OF_GRAIN_BINS);
   double histoBinIntensityQuantum = 1.0 / NUMBER_OF_HISTOGRAM_BINS;

	Ipp32f grainLeft[NUMBER_OF_GRAIN_BINS];
	Ipp32f grainRight[NUMBER_OF_GRAIN_BINS];

   // We need to break twice per histo bin, once on the bin boundary and
   // once at the center of the bin.
   //
   // Special case first break, left edge of first grain bin.
   auto grainBin = 0;
   grainLeft[grainBin] = 0;
   size_t runningSum = 0;
   int breakCount = 1;

   // Breaks between first and last.
   int nextBreak = grainBinRadius;
   const int totalNumberOfBreaks = (2 * NUMBER_OF_GRAIN_BINS) + 1;
   for (auto histoBin = 1; histoBin < NUMBER_OF_HISTOGRAM_BINS; ++histoBin)
   {
      runningSum += smoothHistogram[histoBin];
      if (runningSum >= nextBreak)
      {
         double histoBinIntensity = histoBin * histoBinIntensityQuantum;
         int grainBin = (breakCount - 1) / 2;
         if ((breakCount % 2) == 1)
         {
            _grainCenter[grainBin] = histoBinIntensity;
         }
         else
         {
            grainRight[grainBin] = histoBinIntensity;
            grainLeft[grainBin + 1] = histoBinIntensity;
         }

         nextBreak = int(++breakCount * grainBinRadius);
      }
   }

//   DBTRACE(GRAIN_MODEL_SETUP_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_FILL_BINS_TIMER;

	// % determine how difference is related to intensity
	// GrainIntensity = zeros(NUMBER_OF_GRAIN_BINS, 1);
	for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
	{
		_grainIntensity[bin] = 0;
	}

	// for i = 1:NUMBER_OF_GRAIN_BINS
	// 	 % end points of the bin.  Allow overlap to smooth
	// 	 % the resulting signal
	//
	// 	 % all pixels that fall in this bin
	// 	 idx = find( (Src_smooth(:) > GrainLeft(i)) & (Src_smooth(:) < GrainRight(i)) );
	//
	// 	 % the grain values in this bin, sorted
	// 	 diff = sort(Src_diff(idx));
	//
	// 	 % the range observed in the bin
	// 	 GrainIntensity(i) = diff(round(PERCENTILE_HIGH*numel(diff))) - diff(round(PERCENTILE_LOW*numel(diff)));
	// end

	vector<float> binDiffs[NUMBER_OF_GRAIN_BINS];
	auto smoothRoiIter = smoothImageWithRoi.begin();
	auto diffsRoiIter = diffsImageWithRoi.begin();
	while (smoothRoiIter != smoothImageWithRoi.end())
	{
		Ipp32f value = *smoothRoiIter;
		for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
		{
			if (value > grainLeft[bin] & value < grainRight[bin])
			{
				binDiffs[bin].push_back(*diffsRoiIter);
			}
		}

		++smoothRoiIter;
		++diffsRoiIter;
	}

//   DBTRACE(GRAIN_MODEL_FILL_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER;
         {
            _grainCenter[breakCount / 2] = histoBinIntensity;
         }
         else
         {
            grainRight[breakCount / 2] = histoBinIntensity;
            grainLeft[(breakCount / 2) + 1] = histoBinIntensity;
         }

         nextBreak = int(++breakCount * grainBinRadius);
      }
   }

//   DBTRACE(GRAIN_MODEL_SETUP_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_FILL_BINS_TIMER;

	// % determine how difference is related to intensity
	// GrainIntensity = zeros(NUMBER_OF_GRAIN_BINS, 1);
	for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
	{
		_grainIntensity[bin] = 0;
	}

	// for i = 1:NUMBER_OF_GRAIN_BINS
	// 	 % end points of the bin.  Allow overlap to smooth
	// 	 % the resulting signal
	//
	// 	 % all pixels that fall in this bin
	// 	 idx = find( (Src_smooth(:) > GrainLeft(i)) & (Src_smooth(:) < GrainRight(i)) );
	//
	// 	 % the grain values in this bin, sorted
	// 	 diff = sort(Src_diff(idx));
	//
	// 	 % the range observed in the bin
	// 	 GrainIntensity(i) = diff(round(PERCENTILE_HIGH*numel(diff))) - diff(round(PERCENTILE_LOW*numel(diff)));
	// end

	vector<float> binDiffs[NUMBER_OF_GRAIN_BINS];
	auto smoothRoiIter = smoothImageWithRoi.begin();
	auto diffsRoiIter = diffsImageWithRoi.begin();
	while (smoothRoiIter != smoothImageWithRoi.end())
	{
		Ipp32f value = *smoothRoiIter;
		for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
		{
			if (value > grainLeft[bin] & value < grainRight[bin])
			{
				binDiffs[bin].push_back(*diffsRoiIter);
			}
		}

		++smoothRoiIter;
		++diffsRoiIter;
	}

//   DBTRACE(GRAIN_MODEL_FILL_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER;
         {
            _grainCenter[breakCount / 2] = histoBinIntensity;
         }
         else
         {
            grainRight[breakCount / 2] = histoBinIntensity;
            grainLeft[(breakCount / 2) + 1] = histoBinIntensity;
         }

         nextBreak = int(++breakCount * grainBinRadius);
      }
   }

//   DBTRACE(GRAIN_MODEL_SETUP_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_FILL_BINS_TIMER;

	// % determine how difference is related to intensity
	// GrainIntensity = zeros(NUMBER_OF_GRAIN_BINS, 1);
	for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
	{
		_grainIntensity[bin] = 0;
	}

	// for i = 1:NUMBER_OF_GRAIN_BINS
	// 	 % end points of the bin.  Allow overlap to smooth
	// 	 % the resulting signal
	//
	// 	 % all pixels that fall in this bin
	// 	 idx = find( (Src_smooth(:) > GrainLeft(i)) & (Src_smooth(:) < GrainRight(i)) );
	//
	// 	 % the grain values in this bin, sorted
	// 	 diff = sort(Src_diff(idx));
	//
	// 	 % the range observed in the bin
	// 	 GrainIntensity(i) = diff(round(PERCENTILE_HIGH*numel(diff))) - diff(round(PERCENTILE_LOW*numel(diff)));
	// end

	vector<float> binDiffs[NUMBER_OF_GRAIN_BINS];
	auto smoothRoiIter = smoothImageWithRoi.begin();
	auto diffsRoiIter = diffsImageWithRoi.begin();
	while (smoothRoiIter != smoothImageWithRoi.end())
	{
		Ipp16u value = *smoothRoiIter;
		for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
		{
			if (value > grainLeft[bin] & value < grainRight[bin])
			{
				binDiffs[bin].push_back(*diffsRoiIter);
			}
		}

		++smoothRoiIter;
		++diffsRoiIter;
	}

//   DBTRACE(GRAIN_MODEL_FILL_BINS_TIMER.ReadAsString());
//   CHRTimer GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER;

#ifdef MULTITHREAD_INTENSITY

   IntensityParams iParams = { binDiffs, _grainIntensity };
   SynchronousThreadRunner multithread(NUMBER_OF_GRAIN_BINS, &iParams, RunThreadToComputeIntensityForOneBin);
   multithread.Run();

#else

	for (auto bin = 0; bin < NUMBER_OF_GRAIN_BINS; ++bin)
	{
      if (binDiffs[bin].size() > 1)
      {
         std::sort(binDiffs[bin].begin(), binDiffs[bin].end());
         int highIndex = my_lround(PERCENTILE_HIGH * (binDiffs[bin].size() - 1));
         int lowIndex = my_lround(PERCENTILE_LOW * (binDiffs[bin].size() - 1));
         _grainIntensity[bin] = binDiffs[bin][highIndex] - binDiffs[bin][lowIndex];
      }
	}

#endif

//   DBTRACE(GRAIN_MODEL_COMPUTE_INTENSITIES_TIMER.ReadAsString());

   return grainModel;
}
//------------------------------------------------------------------------------

