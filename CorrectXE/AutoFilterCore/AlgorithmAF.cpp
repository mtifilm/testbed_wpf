/*
 AlgorithmAF.cpp   - source file for AutoFilter algorithm
 Kevin Manbeck
 Oct 24, 2002

 The AlgorithmAF class is used to perform auto filter.  See
 AlgorithmAF.h for more detailed description.
 */

#include "AlgorithmAF.h"
#include "BlobProcessor.h"
#include "PixelRegions.h"
#include "err_auto_filter.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "SynchronousThreadRunner.h"
#include "Trajectories.h"
#include <stdio.h>
#include <math.h>
#include <map>
using std::map;
#include <vector>
using std::vector;

MTI_INT32 MotionCacheMapEntry::INVALID_MOTION = 0x8000000;


namespace
{

const int NumberOfSlices = 32;

//------------------------------------------------------------------------------

IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
{
   IppiRect sliceRoi;
   int nominalRowsPerSlice = height / NumberOfSlices;
   int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
   sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
   sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
   sliceRoi.x = 0;
   sliceRoi.width = width;

   return sliceRoi;
}

struct MakeMapParams
{
   Ipp8uArray detectionMap;
   BlobPixelArrayWrapper *blobPixelArrayWrapper;
};

int RunThreadToMakeMap(void *vp, int jobNumber, int totalJobs)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<MakeMapParams *>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->detectionMap.getRows(), params->detectionMap.getCols(), jobNumber);
   Ipp8uArray detectionMap = params->detectionMap(sliceRoi);
   BlobPixelArrayWrapper *blobPixelArrayWrapper = params->blobPixelArrayWrapper;
   int wrapperOffset = sliceRoi.y * sliceRoi.width;

   auto mapIter = detectionMap.begin();

   long lNRow = detectionMap.getRows();
   long lNCol = detectionMap.getCols();
   long lRowColStart = 0;
   long lRowColStop = lNRow * lNCol;
   for (long lRowCol = lRowColStart; lRowCol < lRowColStop; ++lRowCol)
   {
      *(mapIter++) = blobPixelArrayWrapper->isInABlob(lRowCol + wrapperOffset) ? 1 : 0;
   }

   return 0;
}

} // anonymous namespace
//------------------------------------------------------------------------------

long TemporalContrastFcnNegative(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB);
long TemporalContrastFcnPositive(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB);
long TemporalContrastFcnNone(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB);
//------------------------------------------------------------------------------

/*
 Spatial contrast functions are now inlined
 */
#define USE_CONTRAST_FCN_NEGATIVE -1
#define USE_CONTRAST_FCN_NONE      0
#define USE_CONTRAST_FCN_POSITIVE  1

// For speed, this replaces RowColComWrapAround()
// Deal with top/bottom image boundary problems by wrapping row*col*com index
// to the opposite edge of the image if it falls outside the image; the theory
// is that whatever's there will probably not match the blob whose motion we
// are estimating so contrast should be relatively high and not affect our
// searching (I guess!).
#define CHECK_ROWCOLCOM_BOUNDS(ROWCOLCOM, TOP) \
   if (ROWCOLCOM < 0)                          \
     ROWCOLCOM += TOP;                 \
   if (ROWCOLCOM >= TOP)               \
     ROWCOLCOM -= TOP;

// Slightly faster way to figure out if at least 3 out of 4 conditions are met
#define AT_LEAST_3_OUT_OF_4_ARE_TRUE(A, B, C, D) \
  ((A)?  ((B)? ((C) || (D)) : ((C) && (D)))  :  ((B) && (C) && (D)))

// The test for whether or not a pixel is a defect (negative contrast case)
#define IS_CONTRAST_NEGATIVE_DEFECT(lRCC, usTemporalThresh, usSpatialThresh) \
    ( uspRefA0[lRCC] > usTemporalThresh &&               \
      uspRefAN[lRCC] > usTemporalThresh &&               \
      uspRefAS[lRCC] > usTemporalThresh &&               \
      uspRefAE[lRCC] > usTemporalThresh &&               \
      uspRefAW[lRCC] > usTemporalThresh &&               \
      uspRefB0[lRCC] > usTemporalThresh &&               \
      uspRefBN[lRCC] > usTemporalThresh &&               \
      uspRefBS[lRCC] > usTemporalThresh &&               \
      uspRefBE[lRCC] > usTemporalThresh &&               \
      uspRefBW[lRCC] > usTemporalThresh &&               \
      ( AT_LEAST_3_OUT_OF_4_ARE_TRUE(                    \
              (uspCurrN[lRCC] > usSpatialThresh),        \
              (uspCurrS[lRCC] > usSpatialThresh),        \
              (uspCurrE[lRCC] > usSpatialThresh),        \
              (uspCurrW[lRCC] > usSpatialThresh)) ||     \
        AT_LEAST_3_OUT_OF_4_ARE_TRUE(                    \
              (uspCurrNE[lRCC] > usSpatialThresh),       \
              (uspCurrNW[lRCC] > usSpatialThresh),       \
              (uspCurrSE[lRCC] > usSpatialThresh),       \
              (uspCurrSW[lRCC] > usSpatialThresh))       \
      )                                                  \
    )

// The test for whether or not a pixel is a defect (positive contrast case)
#define IS_CONTRAST_POSITIVE_DEFECT(lRCC, usTemporalThresh, usSpatialThresh) \
    ( uspRefA0[lRCC] < usTemporalThresh &&               \
      uspRefAN[lRCC] < usTemporalThresh &&               \
      uspRefAS[lRCC] < usTemporalThresh &&               \
      uspRefAE[lRCC] < usTemporalThresh &&               \
      uspRefAW[lRCC] < usTemporalThresh &&               \
      uspRefB0[lRCC] < usTemporalThresh &&               \
      uspRefBN[lRCC] < usTemporalThresh &&               \
      uspRefBS[lRCC] < usTemporalThresh &&               \
      uspRefBE[lRCC] < usTemporalThresh &&               \
      uspRefBW[lRCC] < usTemporalThresh &&               \
      ( AT_LEAST_3_OUT_OF_4_ARE_TRUE(                    \
              (uspCurrN[lRCC] < usSpatialThresh),        \
              (uspCurrS[lRCC] < usSpatialThresh),        \
              (uspCurrE[lRCC] < usSpatialThresh),        \
              (uspCurrW[lRCC] < usSpatialThresh)) ||     \
        AT_LEAST_3_OUT_OF_4_ARE_TRUE(                    \
              (uspCurrNE[lRCC] < usSpatialThresh),       \
              (uspCurrNW[lRCC] < usSpatialThresh),       \
              (uspCurrSE[lRCC] < usSpatialThresh),       \
              (uspCurrSW[lRCC] < usSpatialThresh))       \
      )                                                  \
    )
//------------------------------------------------------------------------------

/*
 Class member variable
 */
// Breaks the build!
// SNeighborhood CAlgorithmAF::M_Neighborhood[DEFECT_MAX_EDGE_BLUR_STEPS + 1];
SNeighborhood *CAlgorithmAF::M_Neighborhood = NULL;
SNeighborhood *CAlgorithmAF::M_Neighborhood_Alpha = NULL;
CSpinLock CAlgorithmAF::M_GlobalNeighborhoodLock;
//------------------------------------------------------------------------------

CAlgorithmAF::CAlgorithmAF()
: afspSetting(NULL)
, lNSetting(0)
, afapAlgorithm(NULL)
, lPixelMaskPadding(0)
, uspPixelMask(NULL)
, _blobPixelArray(nullptr)
, lpBlobRC(NULL)
, lpPanBlobRC(NULL)
, lpUnexclRowColComLists(NULL)
, lNPixelsInBiggestAllowedBlob(0)
, bBlobPixelArrayWasMalloced(false)
, bBlobLabelArrayWasMalloced(false)
, bBlobRCWasMalloced(false)
, bUnexclRowColComListWasMalloced(false)
, bPixelMaskWasMalloced(false)
, pPreallocedMemory(NULL)
, bPrintOut(false)
, ucpROIBlend(0)
, ucpROILabel(0)
, eAlphaMatteType(IF_ALPHA_MATTE_NONE)
, EmergencyStopFlagPtr(NULL)
, bppBlobProc(NULL)
, bDoDeferredDimensionSetup(true)
{

	lpMotionDiffs[0] = NULL;
	lpMotionDiffs[1] = NULL;

	Initialize(false, NULL); // by default - don't thread internally

	// First time through, set up the global Neighborhood structure
   {
      CAutoSpinLocker lock(M_GlobalNeighborhoodLock);
      if (M_Neighborhood == NULL)
      {
         M_Neighborhood = new SNeighborhood[DEFECT_MAX_EDGE_BLUR_STEPS + 1];
         M_Neighborhood_Alpha = new SNeighborhood[DEFECT_MAX_EDGE_BLUR_STEPS + 1];
      }
   }

	TRACE_3(errout << "SIZE OF AF_PreallocedMemory_2K  = " << sizeof(AF_PreallocedMemory_2K));
	TRACE_3(errout << "SIZE OF AF_PreallocedMemory_4K  = " << sizeof(AF_PreallocedMemory_4K));
	TRACE_3(errout << "SIZE OF AF_PreallocedMemory_6K  = " << sizeof(AF_PreallocedMemory_6K));
	TRACE_3(errout << "SIZE OF AF_PreallocedMemory_8K  = " << sizeof(AF_PreallocedMemory_8K));
	TRACE_3(errout << "SIZE OF AF_PreallocedMemory_10K = " << sizeof(AF_PreallocedMemory_10K));

} /* CAlgorithmAF */
//------------------------------------------------------------------------------

CAlgorithmAF::~CAlgorithmAF()
{
	FreeEverything();

	delete bppBlobProc;
	bppBlobProc = NULL;
}
//------------------------------------------------------------------------------

void CAlgorithmAF::FreeEverything()
{

	FreeGeneral();
	FreeAlgorithm();

	_blobPixelArray = nullptr;
	lpBlobRC = NULL;
	lpUnexclRowColComLists = NULL;
	uspPixelMask = NULL;
	lPixelMaskPadding = 0;

	// Ancillary crap
	bDoDeferredDimensionSetup = true;

} /* ~CAlgorithmAF */
// -------------------------------------------------------------------------

void CAlgorithmAF::Initialize(bool bThreadInternally, AF_PreallocedMemory *pPreallocedMemoryArg)
{
	if (bppBlobProc != NULL)
	{
		delete bppBlobProc;
	}
	bppBlobProc = new CBlobProcessor(bThreadInternally);
	pPreallocedMemory = pPreallocedMemoryArg;

	lNRow = -1;
	lNCol = -1;
	lNCom = -1;

	for (long lCom = 0; lCom < AF_MAX_COMPONENTS; lCom++)
	{
		laLowerBound[lCom] = -1;
		laUpperBound[lCom] = -1;
	}

	// biBlobs.Free ();

	FreeGeneral();
	FreeAlgorithm();

	prlpOriginalValues = NULL;
   _modifiedPixelMap = nullptr;

} /* Initialize */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetDimension(long lNRowArg, long lNColArg, long lNComArg,
      long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg)
{
	int iRet;

	FreeGeneral();
	FreeAlgorithm();

	if (lNRowArg <= 0)
	{
		return ERR_PLUGIN_AF_BAD_VERTICAL_DIMENSION;
	}

	if (lNColArg <= 0)
	{
		return ERR_PLUGIN_AF_BAD_HORIZONTAL_DIMENSION;
	}

	if (lNComArg <= 0)
	{
		return ERR_PLUGIN_AF_BAD_COMPONENT_DIMENSION;
	}

	if (lActiveStartRowArg < 0 || lActiveStopRowArg > lNRowArg || lActiveStartRowArg >= lActiveStopRowArg)
	{
		return ERR_PLUGIN_AF_BAD_VERTICAL_ACTIVE;
	}

	if (lActiveStartColArg < 0 || lActiveStopColArg > lNColArg || lActiveStartColArg >= lActiveStopColArg)
	{
		return ERR_PLUGIN_AF_BAD_HORIZONTAL_ACTIVE;
	}

	lNRow = lNRowArg;
	lNCol = lNColArg;
	lNCom = lNComArg;
	lNRowCol = lNRow * lNCol;
	lNRowColCom = lNRow * lNCol * lNCom;
	lActiveStartRow = lActiveStartRowArg;
	lActiveStopRow = lActiveStopRowArg;
	lActiveStartCol = lActiveStartColArg;
	lActiveStopCol = lActiveStopColArg;

	bDoDeferredDimensionSetup = true;

	return 0;
} /* SetDimension */
// -------------------------------------------------------------------------

void CAlgorithmAF::setRegionOfInterestPtr(const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel)
{
	ucpROIBlend = ucpBlend;
	ucpROILabel = ucpLabel;

	bNeedToSetupPixelMask = true;

} /* setRegionOfInterest */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetImageBounds(MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[])
{
	long lCom;

	if (lNCom <= 0 || lNCom > AF_MAX_COMPONENTS)
	{
		return ERR_PLUGIN_AF_BAD_COMPONENT_DIMENSION;
	}

	for (lCom = 0; lCom < lNCom; lCom++)
	{
		laLowerBound[lCom] = spLowerBoundArg[lCom];
		laUpperBound[lCom] = spUpperBoundArg[lCom];
	}

	return 0;
} /* SetImageBounds */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetAlphaMatteType(EAlphaMatteType alphatype, bool isHalfFloats)
{
	bAlphaDataIsHalfFloats = isHalfFloats;
	eAlphaMatteType = IF_ALPHA_MATTE_NONE;

	if ((alphatype == IF_ALPHA_MATTE_NONE)
   || (alphatype == IF_ALPHA_MATTE_1_BIT)
	|| (alphatype == IF_ALPHA_MATTE_8_BIT)
   || (alphatype == IF_ALPHA_MATTE_16_BIT)
   || (alphatype == IF_ALPHA_MATTE_2_BIT_EMBEDDED)
   || (alphatype == IF_ALPHA_MATTE_8_BIT_INTERLEAVED)
   || (alphatype == IF_ALPHA_MATTE_10_BIT_INTERLEAVED)
   || (alphatype == IF_ALPHA_MATTE_16_BIT_INTERLEAVED))
	{
		eAlphaMatteType = alphatype;
		return 0;
	}

	return -1;
} /* SetAlphaMatteType */
// -------------------------------------------------------------------------

int CAlgorithmAF::AssignParameter(CAutoFilterParameters *app)
{
	int iRet;
	long lSetting;
	AUTO_FILTER_SETTING afsLocal;
//DBLINE;

//   app->dump();

	_detectionStuffIsValid = false;
   _motionStuffIsValid = false;
   _fixStuffIsValid = false;
//DBLINE;

	// Check to see if we need to make any changes
	bool bNeedChange = false;
	if (lNSetting != app->lNSetting)
	{
      bNeedChange = true;
	}
//DBLINE;

   // SPECIAL CASE: if there is only one set of settings, then we can maybe
   // keep some precviously calculated stuff for faster single-frame previewing.
   if (!bNeedChange && lNSetting == 1)
   {
//DBLINE;
      lSetting = 0;
      bool isDetectionInvalidated = false;
      bool isMotionInvalidated = false;
      bool isFixingInvalidated = false;
//DBLINE;
      CopyParameter(app, &afsLocal, lSetting);
//DBLINE;
      CompareParameter(&afspSetting[lSetting], &afsLocal, isDetectionInvalidated, isMotionInvalidated, isFixingInvalidated);
// DBLINE;

      // It doesn't work to skip detection stuff, so always say it's not valid
      // Probably need to rewrite this module to get it to work. QQQ TODO
      _detectionStuffIsValid = false;  // !isDetectionInvalidated;
      _motionStuffIsValid = !isMotionInvalidated;
      _fixStuffIsValid = !isFixingInvalidated;

//DBLINE;
      CopyParameter(app, &afspSetting[lSetting], lSetting);
//DBLINE;

      if (isDetectionInvalidated)
      {
//DBLINE;
         AllocAlgorithm(app->lNSetting);
//DBLINE;
         afspSetting[lSetting].lFrameIndex = app->lFrameIndex;
//DBLINE;
         SetupAlgorithm(lSetting);
//DBLINE;
      }
      else if (isMotionInvalidated)
      {
//DBLINE;
         // Reset motion estimation stuff
         if (afapAlgorithm)
         {
//DBLINE;
            afapAlgorithm[lSetting].uspSpatialContrastThresh = NULL;
//DBLINE;
            afapAlgorithm[lSetting].uspTemporalContrastThresh = NULL;
//DBLINE;

            for (long lIndex = 0; lIndex < NUM_MOTION_INDEX; lIndex++)
            {
               afapAlgorithm[lSetting].lpMotionRCC[lIndex] = NULL;
            }
//DBLINE;

            _bestMotionCacheMap.clear();
//DBLINE;
         }

         lpMotionDiffs[0] = NULL;
         lpMotionDiffs[1] = NULL;

//DBLINE;
	      SetupAlgorithm(lSetting);
//DBLINE;
      }
      else if (isFixingInvalidated)
      {
//DBLINE;
         // Don't reset anything.
      }
   }
   else
   {
//DBLINE;
      for (lSetting = 0; lSetting < lNSetting && bNeedChange == false; lSetting++)
      {
//DBLINE;
         CopyParameter(app, &afsLocal, lSetting);
//DBLINE;

         bool isDetectionInvalidated = false;
         bool isMotionInvalidated = false;
         bool isFixingInvalidated = false;
//DBLINE;
         CompareParameter(&afspSetting[lSetting], &afsLocal, isDetectionInvalidated, isMotionInvalidated, isFixingInvalidated);
//DBLINE;
         bNeedChange = isDetectionInvalidated || isMotionInvalidated || isFixingInvalidated;
//DBLINE;
      }

      // only reassign parameter settings if a change is needed
      if (bNeedChange)
      {
//DBLINE;
         // allocate storage for the settings and algorithm
         iRet = AllocAlgorithm(app->lNSetting);
//DBLINE;
         if (iRet)
         {
            return (iRet);
         }

         // loop over all the settings and copy parameters
//DBLINE;
         for (lSetting = 0; lSetting < lNSetting; lSetting++)
         {
//DBLINE;
            CopyParameter(app, &afspSetting[lSetting], lSetting);
//DBLINE;
            afspSetting[lSetting].lFrameIndex = app->lFrameIndex;
//DBLINE;
            iRet = SetupAlgorithm(lSetting);
//DBLINE;
            if (iRet)
            {
               return iRet;
            }
         }
      }
      else
      {
//DBLINE;
         // Never skip any steps if there are more than one set of settings.
         _detectionStuffIsValid = false;
         _motionStuffIsValid = false;
         _fixStuffIsValid = false;
      }
   }

//DBLINE;
	return 0;
} /* AssignParameter */
// -------------------------------------------------------------------------

void CAlgorithmAF::setPrintOut(bool newPrintFlag)
{
	bPrintOut = newPrintFlag;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::SetEmergencyStopFlagPtr(const bool *newEmergencyStopFlagPtr)
{
	EmergencyStopFlagPtr = newEmergencyStopFlagPtr;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::GetStats(StatsAF *afStats) const
{
	for (int i = 0; i < AF_MAX_SETTINGS; ++i)
	{
		afStats[i] = _afStats[i];
	}
}
// -------------------------------------------------------------------------

int CAlgorithmAF::AllocAlgorithm(long lNSettingArg)
{

	FreeAlgorithm();

	if (lNSettingArg)
	{
		try
		{
			afspSetting = new AUTO_FILTER_SETTING[lNSettingArg];
			afapAlgorithm = new AUTO_FILTER_ALGORITHM[lNSettingArg];
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::AllocAlgorithm");
			return ERR_PLUGIN_AF_MALLOC;
		}
	}

	lNSetting = lNSettingArg;

	return 0;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::FreeAlgorithm()
{
	if (afapAlgorithm)
	{
		for (long lSetting = 0; lSetting < lNSetting; lSetting++)
		{
			afapAlgorithm[lSetting].uspSpatialContrastThresh = NULL;
			afapAlgorithm[lSetting].uspTemporalContrastThresh = NULL;

			for (long lIndex = 0; lIndex < NUM_MOTION_INDEX; lIndex++)
			{
				afapAlgorithm[lSetting].lpMotionRCC[lIndex] = NULL;
			}
		}
	}

	lpMotionDiffs[0] = NULL;
	lpMotionDiffs[1] = NULL;

	delete[]afapAlgorithm;
	delete[]afspSetting;

	lNSetting = 0;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::FreeGeneral()
{
	lNPixelsInBiggestAllowedBlob = 0;
}
// -------------------------------------------------------------------------

/*
 Copies parameter settings out of app into afsp
 */
void CAlgorithmAF::CopyParameter(CAutoFilterParameters *app, AUTO_FILTER_SETTING *afsp, long lSetting)
{
	long lRange, lMaxVal, lMinVal;
	float fMaxVal, fMinVal, fSize0, fSize10, fSize50, fSize100, fSlope, fInter, fSize, fContrast0, fContrast50, fContrast100, fTol0, fTol50,
			fTol100, fStdErr0, fStdErr50, fStdErr100, fContrastSetting;

	afsp->lPrincipalComponent = app->lPrincipalComponent[lSetting];
	afsp->lComponentMask = app->lComponentMask[lSetting];
	afsp->bUseAlphaMatteToFindDebris = app->bUseAlphaMatteToFindDebris[lSetting];
	afsp->lContrastDirection = app->laContrastDirection[lSetting];

   afsp->bUseGrainFilter = app->bUseGrainFilter[lSetting];
   afsp->lGrainFilterParam = app->lGrainFilterParam[lSetting];
   afsp->fLuminanceThreshold = app->lLuminanceThreshold[lSetting] / 100.F;

	// establish the threshold on RANGE
	for (long lCom = 0; lCom < lNCom; lCom++)
	{
		// 0% maps to laLowerBound, 100% maps to laUpperBound
		lRange = laUpperBound[lCom] - laLowerBound[lCom];
		fMinVal = (float)lRange * (app->faMinVal[lSetting][lCom] / 100.) + (float)laLowerBound[lCom];
		fMaxVal = (float)lRange * (app->faMaxVal[lSetting][lCom] / 100.) + (float)laLowerBound[lCom];

		lMinVal = fMinVal + 0.5;
		lMaxVal = fMaxVal + 0.5;

		afsp->laMinVal[lCom] = lMinVal;
		afsp->laMaxVal[lCom] = lMaxVal;
	}

   if (afsp->bUseAlphaMatteToFindDebris)
   {
		afsp->bUseContrastSpatial = false;
	   afsp->fContrastSpatial = 1.F;
		afsp->bUseContrastTemporal = false;
	   afsp->fContrastTemporal = 1.F;
   }
   else
   {
      // establish the threshold on SPATIAL CONTRAST
      fContrast0 = CONTRAST_SPATIAL_0;
      fContrast50 = CONTRAST_SPATIAL_50;
      fContrast100 = CONTRAST_SPATIAL_100;
      fContrastSetting = app->faContrastSpatial[lSetting];
      afsp->bUseContrastSpatial = true;

   // This can't be right!! QQQ
   //	// I added the filtering conditions to not slow down temporal contrast mode -- I hope that's OK!!
   //	if (fContrastSetting >= 100. && afsp->bUseGrainFilter == false && afsp->lMinSizeThresh < 2)
      if (fContrastSetting < 1.F)     // This makes much more sense!
      {
         // Shut it off. This is a pointless setting!
         afsp->bUseContrastSpatial = false;
         fSlope = 0.;
         fInter = 0.;
      }
      else if (fContrastSetting < 50.F)
      {
         // linear transition from fContrast0 to fContrast50
         fSlope = (fContrast50 - fContrast0) / 50.;
         fInter = fContrast0 - fSlope * 0.;
      }
      else // 50 <= Contrast setting < 100
      {
         // linear transition from fContrast50 to fContrast100
         fSlope = (fContrast100 - fContrast50) / 50.;
         fInter = fContrast50 - fSlope * 50.;
      }

      afsp->fContrastSpatial = (fSlope * fContrastSetting + fInter) * (float)afsp->lContrastDirection;

      // establish the threshold on TEMPORAL CONTRAST
      fContrast0 = CONTRAST_TEMPORAL_0;
      fContrast50 = CONTRAST_TEMPORAL_50;
      fContrast100 = CONTRAST_TEMPORAL_100;
      fContrastSetting = app->faContrastTemporal[lSetting];
      afsp->bUseContrastTemporal = true;

   // This can't be right!! QQQ
   //	if (fContrastSetting >= 100.)
      if (fContrastSetting < 1)    // This makes much more sense!
      {
         // Shut it off. This is a pointless setting!
         afsp->bUseContrastTemporal = false;
         fSlope = 0.;
         fInter = 0.;
      }
      else if (fContrastSetting < 50.)
      {
         // linear transition from fContrast0 to fContrast50
         fSlope = (fContrast50 - fContrast0) / 50.;
         fInter = fContrast0 - fSlope * 0.;
      }
      else // 50 <= Contrast setting < 100
      {
         // linear transition from fContrast50 to fContrast100
         fSlope = (fContrast100 - fContrast50) / 50.;
         fInter = fContrast50 - fSlope * 50.;
      }

      afsp->fContrastTemporal = (fSlope * fContrastSetting + fInter) * (float)afsp->lContrastDirection;
   }

	// establish the MOTION search
	afsp->lMotionOffsetRow = (app->faMotionOffsetRow[lSetting] / 100.) * (float)lNRow * MAX_MOTION_OFFSET_ROW;
	afsp->lMotionOffsetCol = (app->faMotionOffsetCol[lSetting] / 100.) * (float)lNCol * MAX_MOTION_OFFSET_COL;

	// establish the MOTION THRESHOLD
	if (afsp->lPrincipalComponent == 3) // Alpha
	{
		lRange = laUpperBound[1] - laLowerBound[1]; // Green (assume RGBA)
	}
	else
	{
		lRange = laUpperBound[afsp->lPrincipalComponent] - laLowerBound[afsp->lPrincipalComponent];
	}
	afsp->lMotionThresh = ((float)app->faMotionThresh[lSetting] / 100.) * MAX_MOTION_THRESH * (float)lRange;
	if (app->faMotionThresh[lSetting] == 100.)    // QQQ I think this should be 0, not 100??
	{
		afsp->bUseMotion = false;
	}
	else
	{
		afsp->bUseMotion = true;
	}

	// establish the MOTION TOLERANCE
	double fMotionTol = app->faMotionThresh[lSetting];
	if (!afsp->bUseAlphaMatteToFindDebris || (eAlphaMatteType == IF_ALPHA_MATTE_NONE))
	{
		fTol0 = MOTION_TOL_0;
		fTol50 = MOTION_TOL_50;
		fTol100 = MOTION_TOL_100;
	}
	else
	{
		fTol0 = MOTION_TOL_0_ALPHA;
		fTol50 = MOTION_TOL_50_ALPHA;
		fTol100 = MOTION_TOL_100_ALPHA;
	}

	if (fMotionTol < 50.)
	{
		// linear transition from fTol0 to fTol50
		fSlope = (fTol50 - fTol0) / 50.;
		fInter = fTol0 - fSlope * 0.;
	}
	else
	{
		// linear transition from fTol50 to fTol100
		fSlope = (fTol100 - fTol50) / 50.;
		fInter = fTol50 - fSlope * 50;
	}
	afsp->fMotionTol = fSlope * fMotionTol + fInter;

	// establish blob size breaks
	if (!afsp->bUseAlphaMatteToFindDebris || (eAlphaMatteType == IF_ALPHA_MATTE_NONE))
	{
		fSize0 = BLOB_SIZE_0 * (float)lNRowCol;
		fSize10 = BLOB_SIZE_10 * (float)lNRowCol;
		fSize50 = BLOB_SIZE_50 * (float)lNRowCol;
		fSize100 = BLOB_SIZE_100 * (float)lNRowCol;
	}
	else
	{
		fSize0 = BLOB_SIZE_0_ALPHA * (float)lNRowCol;
		fSize10 = BLOB_SIZE_10_ALPHA * (float)lNRowCol;
		fSize50 = BLOB_SIZE_50_ALPHA * (float)lNRowCol;
		fSize100 = BLOB_SIZE_100_ALPHA * (float)lNRowCol;
	}

	// Solve for a & b in
	// a*(50*50) + b*50 = fSize50
	// a*(10*10) + b*10 = fSize10
	float a = (fSize50 - (5.F * fSize10)) / 2000.F;
	float b = ((25.F * fSize10) - fSize50) / 200.F;

	// establish the MAX SIZE THRESHOLD
	long lSizeSetting = app->faMaxSize[lSetting];
	if (lSizeSetting <= 50)
	{
		afsp->lMaxSizeThresh = 0.99F + (a * lSizeSetting * lSizeSetting) + (b * lSizeSetting);
	}
	else
	{
		float fScaler = (fSize100 - fSize50) / (50.F * 50.F);
		afsp->lMaxSizeThresh = 0.99F + fSize50 + ((lSizeSetting - 50) * (lSizeSetting - 50) * fScaler);
	}

	TRACE_3(errout << "AF MAX SIZE:" << lSizeSetting << " => " << afsp->lMaxSizeThresh);

	// establish the MIN SIZE THRESHOLD
	lSizeSetting = std::min<float>(app->faMinSize[lSetting], 100.F);
	afsp->lMinSizeThresh = lSizeSetting; // 0.99F + (a * lSizeSetting * lSizeSetting) + (b * lSizeSetting);

	TRACE_3(errout << "AF MIN SIZE:" << lSizeSetting << " => " << afsp->lMinSizeThresh);

	// establish the DUST SIZE THRESHOLD
	lSizeSetting = std::min<int>(app->lDustMaxSize[lSetting], 50);
	afsp->lDustMaxSize = 0.99F + (a * lSizeSetting * lSizeSetting) + (b * lSizeSetting);

	TRACE_3(errout << "AF DUST SIZE:" << lSizeSetting << " => " << afsp->lDustMaxSize);

	// establish the EDGE BLUR
	afsp->fEdgeBlur = app->faEdgeBlur[lSetting];

	// establish DUST FIX MODE
	afsp->bUseHardMotionOnDust = app->bUseHardMotionOnDust[lSetting];

   // HACK
   afsp->filterChangeCounter = app->filterChangeCounter;

	return;
} /* CopyParameter */
// -------------------------------------------------------------------------

void CAlgorithmAF::CompareParameter(
   AUTO_FILTER_SETTING *afsp1,
   AUTO_FILTER_SETTING *afsp2,
   bool &detectionInvalidated,
   bool &motionInvalidated,
   bool &fixesInvalidated)
{
   detectionInvalidated = false;
   motionInvalidated = false;
   fixesInvalidated = false;

   bool minMaxChanged = false;
	for (long lCom = 0; lCom < lNCom; lCom++)
	{
		if (afsp1->laMinVal[lCom] != afsp2->laMinVal[lCom])
		{
			minMaxChanged = true;
		}
		if (afsp1->laMaxVal[lCom] != afsp2->laMaxVal[lCom])
		{
			minMaxChanged = true;
		}
	}

   if (minMaxChanged
   || afsp1->lPrincipalComponent != afsp2->lPrincipalComponent
   || afsp1->lComponentMask != afsp2->lComponentMask
   || afsp1->bUseAlphaMatteToFindDebris != afsp2->bUseAlphaMatteToFindDebris
   || afsp1->fContrastSpatial != afsp2->fContrastSpatial
   || afsp1->bUseContrastSpatial != afsp2->bUseContrastSpatial
   || afsp1->fContrastTemporal != afsp2->fContrastTemporal
   || afsp1->bUseContrastTemporal != afsp2->bUseContrastTemporal
   || afsp1->lContrastDirection != afsp2->lContrastDirection
   || afsp1->lMaxSizeThresh != afsp2->lMaxSizeThresh
   || afsp1->lMinSizeThresh != afsp2->lMinSizeThresh
	|| afsp1->bUseGrainFilter != afsp2->bUseGrainFilter
	|| afsp1->lGrainFilterParam != afsp2->lGrainFilterParam
	|| afsp1->fLuminanceThreshold != afsp2->fLuminanceThreshold
	|| afsp1->bUseHardMotionOnDust != afsp2->bUseHardMotionOnDust
	|| afsp1->lDustMaxSize != afsp2->lDustMaxSize
   || afsp1->filterChangeCounter != afsp2->filterChangeCounter)
	{
		detectionInvalidated = true;
		motionInvalidated = true;
		fixesInvalidated = true;
	}
   else if (afsp1->lMotionOffsetRow != afsp2->lMotionOffsetRow
   || afsp1->lMotionOffsetCol != afsp2->lMotionOffsetCol
   || afsp1->lMotionThresh != afsp2->lMotionThresh
   || afsp1->bUseMotion != afsp2->bUseMotion
	|| afsp1->fEdgeBlur != afsp2->fEdgeBlur)
	{
		motionInvalidated = true;
		fixesInvalidated = true;
	}
   else if (afsp1->fMotionTol != afsp2->fMotionTol)
	{
		fixesInvalidated = true;
	}

} /* CompareParameter */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupAlgorithm(long lSetting)
{
	int iRet;

	// allocate storage for the contrast lookup tables
	iRet = SetupContrast(lSetting);
	if (iRet)
	{
		return iRet;
	}

	// allocate storage for motion tables
	iRet = SetupPerSettingMotion(lSetting);
	if (iRet)
	{
		return iRet;
	}

	return 0;
} /* SetupAlgorithm */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupContrast(long lSetting)
{
	long lThresh, lRange, lVal;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;
	float fRange;

	afap->uspSpatialContrastThresh = pPreallocedMemory->getSpatialContrastThreshAddr(lSetting);
	afap->uspTemporalContrastThresh = pPreallocedMemory->getTemporalContrastThreshAddr(lSetting);

	/*
	 A positive direction for the contrast implies that a value X is
	 classified as dirt if its neighbors have values less than
	 X - delta.

	 Delta is taken to be some percentage of the allowed range.
	 */

	// calculate values for uspSpatialContrastThresh
	if (afsp->bUseContrastSpatial)
	{
		if (afsp->lPrincipalComponent == 3) // Alpha
		{
			lRange = laUpperBound[1] - laLowerBound[1]; // Green (assume RGBA)
		}
		else
		{
			lRange = laUpperBound[afsp->lPrincipalComponent] - laLowerBound[afsp->lPrincipalComponent];
		}
		fRange = (float)lRange;
		for (lVal = 0; lVal < NUM_MTI_UINT16; lVal++)
		{
			lThresh = (float)lVal - (afsp->fContrastSpatial * fRange) + 0.5;

			if (lThresh < 0)
			{
				lThresh = 0;
			}
			if (lThresh >= NUM_MTI_UINT16)
			{
				lThresh = NUM_MTI_UINT16 - 1;
			}

			afap->uspSpatialContrastThresh[lVal] = lThresh;
		}
	}

	// calculate values for uspTemporalContrastThresh
	if (afsp->bUseContrastTemporal)
	{
		if (afsp->lPrincipalComponent == 3) // Alpha
		{
			lRange = laUpperBound[1] - laLowerBound[1]; // Green (assume RGBA)
		}
		else
		{
			lRange = laUpperBound[afsp->lPrincipalComponent] - laLowerBound[afsp->lPrincipalComponent];
		}
		fRange = (float)lRange;
		for (lVal = 0; lVal < NUM_MTI_UINT16; lVal++)
		{
			lThresh = (float)lVal - (afsp->fContrastTemporal * fRange) + 0.5;
			if (lThresh < 0)
			{
				lThresh = 0;
			}
			if (lThresh >= NUM_MTI_UINT16)
			{
				lThresh = NUM_MTI_UINT16 - 1;
			}

			afap->uspTemporalContrastThresh[lVal] = lThresh;
		}
	}

	return 0;
} /* SetupContrast */
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupPerSettingMotion(long lSetting)
{
	long lRow, lCol, lRCFactor, lIndex, lMotion, lRowColCom, lMotionLocal;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;

	afap->lNMotion = (2 * afsp->lMotionOffsetRow + 1) * (2 * afsp->lMotionOffsetCol + 1);
	for (long lIndex = 0; lIndex < NUM_MOTION_INDEX; lIndex++)
	{
		afap->lpMotionRCC[lIndex] = pPreallocedMemory->getMotionRCCAddr(lSetting, lIndex);
	}

	// force lpMotionRCC[lIndex][0] to be the zero motion.
	lMotion = 1;
	for (lRow = -afsp->lMotionOffsetRow; lRow <= afsp->lMotionOffsetRow; lRow++)
	{
		for (lCol = -afsp->lMotionOffsetCol; lCol <= afsp->lMotionOffsetCol; lCol++)
		{
			lRowColCom = (lRow * lNCol + lCol) * lNCom;
			// if this is the zero motion, put it first in the list
			if (lRow == 0 && lCol == 0)
			{
				lMotionLocal = 0;
			}
			else
			{
				lMotionLocal = lMotion;
				lMotion++;
			}

			for (lIndex = 0; lIndex < NUM_MOTION_INDEX; lIndex++)
			{
				switch (lIndex)
				{
				case MOTION_PAST_ONLY:
					lRCFactor = -2;
					break;
				case MOTION_PAST:
					lRCFactor = -1;
					break;
				case MOTION_FUTURE:
					lRCFactor = 1;
					break;
				case MOTION_FUTURE_ONLY:
					lRCFactor = 2;
					break;
				}

				afap->lpMotionRCC[lIndex][lMotionLocal] = lRCFactor * lRowColCom;
			}
		}
	}

	lpMotionDiffs[0] = pPreallocedMemory->getMotionDiffsAddr(0);
	lpMotionDiffs[1] = pPreallocedMemory->getMotionDiffsAddr(1);

	return 0;
} /* SetupPerSettingMotion */
// -------------------------------------------------------------------------

int CAlgorithmAF::AllocPixelMask()
{
	// When a pixel gets flagged, the neighbors get flagged as well.  Need
	// to allocate extra storage for pixel mask
	MTIassert(lNCol <= (10 * 1024));
	lPixelMaskPadding = (lNCol <= (2 * 1024))
								? MAX_PIXEL_MASK_PADDING_PIXELS_2K
								: ((lNCol <= (4 * 1024))
									? MAX_PIXEL_MASK_PADDING_PIXELS_4K
									: ((lNCol <= (6 * 1024))
										? MAX_PIXEL_MASK_PADDING_PIXELS_6K
										: ((lNCol <= (8 * 1024))
											? MAX_PIXEL_MASK_PADDING_PIXELS_8K
											: MAX_PIXEL_MASK_PADDING_PIXELS_10K)));

	uspPixelMask = pPreallocedMemory->getPixelMask();

	// advance uspPixelMask so index 0 corresponds to image data
	uspPixelMask += lPixelMaskPadding;

	bNeedToSetupPixelMask = true;

	return 0;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::SetupPixelMask()
{
	// Initialize the pixel mask.  Only process pixels in the active region.
	// If there is a mask, don't process pixels that are masked off.
	for (long lRowCol = 0, lRow = 0; lRow < lNRow; ++lRow)
	{
		for (long lCol = 0; lCol < lNCol; ++lCol, ++lRowCol)
		{
			uspPixelMask[lRowCol] =
					(lRow >= lActiveStartRow && lRow < lActiveStopRow
					&& lCol >= lActiveStartCol && lCol < lActiveStopCol
					&& (ucpROIBlend == nullptr || ucpROIBlend[lRowCol] != 0))
						? PM_DO_NOT_SKIP
						: PM_NONE;
		}
	}
}
// -------------------------------------------------------------------------

void CAlgorithmAF::ResetPixelMask()
{
	// If the "active region" (whatever that is) hasn't changed and the mask
	// hasn't changed, the PM_DO_NOT_SKIP flags should be OK so just clear
	// everything else. I am dubious that this is much faster than
	// SetupPixelMask, so don't really see a justification for the
	// additional complexity.
	for (int lRowCol = 0; lRowCol < lNRowCol; lRowCol++)
	{
		uspPixelMask[lRowCol] &= PM_DO_NOT_SKIP;
	}
}
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupBlob()
{
	_blobPixelArray = pPreallocedMemory->getBlobPixelArrayAddr();
	return 0;
}
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupNeighbors()
{
   int iRet = 0;
   CAutoSpinLocker lock(M_GlobalNeighborhoodLock);

   // If the width of the image changed (or if this is the first time through
   // here), recompute the neighborhood offset matrices

   if (M_Neighborhood[0].lNColBasis != lNCol)
   {
      for (int i = 0; i <= DEFECT_MAX_EDGE_BLUR_STEPS; ++i)
      {
         iRet = SetupOneNeighborhood(i);
         if (iRet != 0)
         {
            break;
         }

         M_Neighborhood[i].lNColBasis = lNCol;
         M_Neighborhood_Alpha[i].lNColBasis = lNCol;
      }
   }

   return iRet;
}
// -------------------------------------------------------------------------

int CAlgorithmAF::SetupOneNeighborhood(long lHood)
{
	SetupOneNeighborhood(
		M_Neighborhood,
		lHood,
		DEFECT_NEIGHBOR_OUTER_DIM,
		DEFECT_SURROUND_INNER_DIM,
		DEFECT_SURROUND_OUTER_DIM,
		DEFECT_EDGE_BLUR_INNER_DIM);

	SetupOneNeighborhood(
		M_Neighborhood_Alpha,
		lHood,
		DEFECT_NEIGHBOR_OUTER_DIM_ALPHA,
		DEFECT_SURROUND_INNER_DIM_ALPHA,
		DEFECT_SURROUND_OUTER_DIM_ALPHA,
		DEFECT_EDGE_BLUR_INNER_DIM_ALPHA);

      return 0;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::SetupOneNeighborhood(
	SNeighborhood *neighborhood,
	long lHood,
	long lNeighborOuterDim,
	long lSurroundInnerDim,
	long lSurroundOuterDim,
	long lEdgeBlurInnerDim)
{

	// allocate nearby neighbors.  Nearby neighbors get processed.
	long lNNeighbor = LSQR(lNeighborOuterDim * 2 + 1);

	// allocate surround neighbors.  Surround neighbors are used for
	// motion estimation
	long lNInner = LSQR(lSurroundInnerDim * 2 + 1);
	long lNSurround = LSQR(lSurroundOuterDim * 2 + 1) - lNInner;

	// allocate edge blur neighbors. Edge blur neighbors may overlap with the
	// other neighbors
	lNInner = LSQR(lEdgeBlurInnerDim * 2 + 1);
	long lEdgeBlurOuterDim = (lHood + DEFECT_EDGE_BLUR_STEPS_PER_PIXEL - 1) / DEFECT_EDGE_BLUR_STEPS_PER_PIXEL + lEdgeBlurInnerDim;
	long lNEdgeBlur = LSQR(lEdgeBlurOuterDim * 2 + 1) - lNInner;

	neighborhood[lHood].Allocate(lNNeighbor, lNSurround, lNEdgeBlur);

	long lNeighbor = 0;
	long lSurround = 0;
	long lEdgeBlur = 0;
	long lMaxBlurDistance = lEdgeBlurOuterDim - lEdgeBlurInnerDim;

	for (long lR = -DEFECT_BIGGEST_DIM; lR <= DEFECT_BIGGEST_DIM; lR++)
	{
		for (long lC = -DEFECT_BIGGEST_DIM; lC <= DEFECT_BIGGEST_DIM; lC++)
		{
			if ( /* inner dim is always 0 */
			lR >= -lNeighborOuterDim
			&& lR <= lNeighborOuterDim
			&& lC >= -lNeighborOuterDim
			&& lC <= lNeighborOuterDim)
			{
				neighborhood[lHood].lpNeighbor[lNeighbor++] = lR * lNCol + lC;
			}

			if ((lR >= -lSurroundOuterDim
					&& lR <= lSurroundOuterDim
					&& lC >= -lSurroundOuterDim
					&& lC <= lSurroundOuterDim)
				&& !(lR >= -lSurroundInnerDim
					&& lR <= lSurroundInnerDim
					&& lC >= -lSurroundInnerDim
					&& lC <= lSurroundInnerDim))
			{
				neighborhood[lHood].lpSurround[lSurround++] = lR * lNCol + lC;
			}

			if ((lR >= -lEdgeBlurOuterDim
					&& lR <= lEdgeBlurOuterDim
					&& lC >= -lEdgeBlurOuterDim
					&& lC <= lEdgeBlurOuterDim)
				&& !(lR >= -lEdgeBlurInnerDim
				&& lR <= lEdgeBlurInnerDim
				&& lC >= -lEdgeBlurInnerDim
				&& lC <= lEdgeBlurInnerDim))
			{
				neighborhood[lHood].lpEdgeBlur[lEdgeBlur] = lR * lNCol + lC;

				// Need to determine the point's distance from the inner edge
				// which is the "closest" outermost non-blurred replaced pixel;
				// I can't for the life of me figure out how to do this without a
				// loop - fortunately we don't really care because this setup
				// is only done once when the tool is first run
				for (long inner = lEdgeBlurInnerDim; inner < lEdgeBlurOuterDim; ++inner)
				{
					long outer = inner + 1;
					if ((lR >= -outer && lR <= outer && lC >= -outer && lC <= outer) && !(lR >= -inner && lR <= inner && lC >= -inner && lC <=
							inner))
					{
						long lDistance = outer - lEdgeBlurInnerDim;
						// Manifest constant 15.f because 4 bit alpha (0 - 15)
						float fAlphaFudge = ((lHood % DEFECT_EDGE_BLUR_STEPS_PER_PIXEL) * 15.F) /
								(DEFECT_MAX_EDGE_BLUR_STEPS * DEFECT_EDGE_BLUR_STEPS_PER_PIXEL);
						float fFixAlpha = 1.F - lDistance / float(lMaxBlurDistance + 1);
						long lFixAlpha = long(fFixAlpha * 15.F - fAlphaFudge + 0.5);
						neighborhood[lHood].lpFixAlpha[lEdgeBlur] = lFixAlpha;
						break;
					}
				}

				++lEdgeBlur;
			}
		}
	}
}
// -------------------------------------------------------------------------

int CAlgorithmAF::SetSrcImgPtr(MTI_UINT16 **uspSrcArg, long *lpFrameIndex)
{
	long lSetting;
	AUTO_FILTER_ALGORITHM *afap;

	/*
	 Rearrange the Src pointers so that the image to process is at 0
	 Note that lpMotionRCCB will always be the motion array with the
	 greater absolute pixel gain

	 */

	// HACK ALERT!!! We get the sequence [-1,-1, 0] here when we are in
	// "PAST-ONLY" mode and it's the first frame processed, which is the
	// frame at markIn + 1, and we only have the markIn frame to look back at!!
	// In that case we use the markIn frame as both the -2 and the -1 frames,
	// which works (I hope!!) only because the intention of the feature is to
	// remove static debris, so motion  is not an issue.
	//
	if ((lpFrameIndex[0] == -2 && lpFrameIndex[1] == -1 && lpFrameIndex[2] == 0) ||
			(lpFrameIndex[0] == -1 && lpFrameIndex[1] == -1 && lpFrameIndex[2] == 0))
	{
		uspSrc[0] = uspSrcArg[2];
		uspSrc[1] = uspSrcArg[1];
		uspSrc[2] = uspSrcArg[0];

		for (lSetting = 0; lSetting < lNSetting; lSetting++)
		{
			afap = afapAlgorithm + lSetting;

			// double gain
			afap->lpMotionRCCB = afap->lpMotionRCC[MOTION_PAST_ONLY];

			// normal gain
			afap->lpMotionRCCA = afap->lpMotionRCC[MOTION_PAST];
		}
	}
	else if (lpFrameIndex[0] == -1 && lpFrameIndex[1] == 0 && lpFrameIndex[2] == 1)
	{
		uspSrc[0] = uspSrcArg[1];
		uspSrc[1] = uspSrcArg[0];
		uspSrc[2] = uspSrcArg[2];

		for (lSetting = 0; lSetting < lNSetting; lSetting++)
		{
			afap = afapAlgorithm + lSetting;

			// normal gain
			afap->lpMotionRCCA = afap->lpMotionRCC[MOTION_PAST];

			// normal gain
			afap->lpMotionRCCB = afap->lpMotionRCC[MOTION_FUTURE];
		}
	}
	else if (lpFrameIndex[0] == 0 && lpFrameIndex[1] == 1 && lpFrameIndex[2] == 2)
	{
		uspSrc[0] = uspSrcArg[0];
		uspSrc[1] = uspSrcArg[1];
		uspSrc[2] = uspSrcArg[2];

		for (lSetting = 0; lSetting < lNSetting; lSetting++)
		{
			afap = afapAlgorithm + lSetting;

			// normal gain
			afap->lpMotionRCCA = afap->lpMotionRCC[MOTION_FUTURE];

			// double gain
			afap->lpMotionRCCB = afap->lpMotionRCC[MOTION_FUTURE_ONLY];
		}
	}

	return 0;
} /* SetSrcImgPtr */
// -------------------------------------------------------------------------

void CAlgorithmAF::SetDstImgPtr(MTI_UINT16 *uspDstArg)
{
	uspDst = uspDstArg;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::SetPixelRegionList(CPixelRegionList *prlpArg)
{
	prlpOriginalValues = prlpArg;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::SetModifiedPixelMap(MTI_UINT8 *modifiedPixelMap)
{
	_modifiedPixelMap = modifiedPixelMap;
}
// -------------------------------------------------------------------------

int CAlgorithmAF::Process(GrainFilterSet &grainFilters)
{
	// if the destination is NULL, this means simply return
	// The NULL value is used by Algorithm_AF_MT in order to only
	// process one frame.
	if (uspDst == NULL)
	{
		// Successfully did nothing!
		return 0;
	}

   // Error if no settings!
   if (lNSetting < 1)
   {
      return ERR_PLUGIN_AF_NO_FILTERS;
   }

	TRACE_3(errout << "AAF_PROCESS: ENTER");
   CHRTimer PROCESS_TIMER;
   CHRTimer PROCESS_INIT_TIMER;

	_bMakingDetectionMapOnly = false;
   _precomputedBlobPixelArrayWrapperPtrPtr = nullptr;
   _grainFilters = grainFilters;

	long lSetting;
	int iRet = 0;
   bool grainFiltersChanged = false;

	if (bDoDeferredDimensionSetup)
	{
		// allocate storage for PixelMask
		try
		{
			iRet = AllocPixelMask();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::AllocPixelMask");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return iRet;
		}

		// allocate blob
		try
		{
			iRet = SetupBlob();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::SetupBlob");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return iRet;
		}

		// allocate neighbors
		try
		{
			iRet = SetupNeighbors();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::SetupNeighbors");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return iRet;
		}

		bDoDeferredDimensionSetup = false; // only once!
	}

	// Initialize the pixel mask if necessary
	if (uspPixelMask == NULL)
	{
		TRACE_0(errout << "INTERNAL ERROR: pixel mask was never allocated!");
		FreeEverything();
		return 668;
	}

   // Pan motion not implemented
	lPanMotionA = 0;
	lPanMotionB = 0;
	lMinPanMotion = 0;
	lMaxPanMotion = 0;

//   DBTRACE(PROCESS_INIT_TIMER.ReadAsString());
   CHRTimer PROCESS_ALL_SETTINGS_TIMER;

	TRACE_3(errout << "AAF_PROCESS: AFTER SETUP");

	for (lSetting = 0; lSetting < lNSetting; lSetting++)
	{
//		TRACE_0(errout << "==== AAF_PROCESS: Processing Setting " << lSetting << "=====");
		AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
		CHRTimer PROCESS_ONE_SETTING_TIMER;

		BlobPixelArrayWrapper blobPixelArrayWrapper(_blobPixelArray, lNCol, lNRow);

		if (_detectionStuffIsValid == false)
		{
			///DetectPossibleDefectsUsingAlphaChannel(lSetting);
			CHRTimer PROCESS_SETUP_DETECTION_INITIALIZATION_TIMER;

			// DETECTION PHASE INITIALIZATION

			// Initalize pixel mask if necessary based on the ROI, or if that's
			// already been done, just reset it by clearing out old detection flags.
			if (bNeedToSetupPixelMask)
			{
				SetupPixelMask();
				bNeedToSetupPixelMask = false;
			}
			else
			{
				// Initial pixel mask doesn't change between non-alpha-detect settings.
				ResetPixelMask();
			}

//			DBTRACE(PROCESS_SETUP_DETECTION_INITIALIZATION_TIMER.ReadAsString());

			// Alpha filter detection is completely different from normal filter detection.
			if (afsp->bUseAlphaMatteToFindDebris)
			{
				// Error if no alpha channel!
				if (eAlphaMatteType == IF_ALPHA_MATTE_NONE)
				{
					return ERR_PLUGIN_AF_NO_ALPHA_CHANNEL;
				}

            afspSetting[lSetting].lActiveComponent = 1; // Green (assume RGBA)
            afspSetting[lSetting].lGrainFilterComponent = 3;  // Y

            // Assign image pointers - Must be done AFTER setting lActiveComponent!!!!
            AssignPointers(lSetting);

            CHRTimer MDM_SETUP_PIXEL_MASK_1_TIMER;
            SetupPixelMask();
//            DBTRACE(MDM_SETUP_PIXEL_MASK_1_TIMER.ReadAsString());

            // Scan the alpha matte for defect pixels.
            CHRTimer MDM_DETECT_ALPHA_DEBRIS_TIMER;
            SetPixelMaskFromAlphaChannel(lSetting);
//            DBTRACE(MDM_DETECT_ALPHA_DEBRIS_TIMER.ReadAsString());

         // TODO : CALL IdentifyBlobs()
            // Make a blob map of the defective pixels.
            CHRTimer MDM_IDENTIFY_BLOBS_TIMER;
            BlobPixelArrayWrapper blobPixelArrayWrapper(_blobPixelArray, lNCol, lNRow);
            BlobIdentifier blobIdentifier(&blobPixelArrayWrapper);
            blobIdentifier.identifyBlobs(PM_DEFECT_IN_PIXEL, uspPixelMask);
//            DBTRACE(MDM_IDENTIFY_BLOBS_TIMER.ReadAsString());

            CHRTimer MDM_PREPARE_FOR_ITER_TIMER;
            blobPixelArrayWrapper.prepareForIteration();
//            DBTRACE(MDM_PREPARE_FOR_ITER_TIMER.ReadAsString());
         //

            // Run grain suppression?
            if (afspSetting[lSetting].bUseGrainFilter)
            {
               // Grain suppression is enabled.
               long grainFilterComponent = afspSetting[lSetting].lGrainFilterComponent;
               if (_grainFilters[grainFilterComponent].isNull())
               {
                  // We haven't yet measured the grain for the alpha channel,
                  // so do that now.
                  CHRTimer MDM_CREATE_GRAIN_FILTER_TIMER;

                  unsigned short maxValue = laUpperBound[afspSetting[lSetting].lActiveComponent];
                  _grainFilters[grainFilterComponent] =
                        GrainFilter::CreateFrom(
                              uspSrc[0],
                              lNRow,
                              lNCol,
                              grainFilterComponent,
                              maxValue,
                              &blobPixelArrayWrapper,
                              PM_DEFECT_IN_PIXEL);

                  MTIassert(!_grainFilters[grainFilterComponent].isNull());

                  grainFiltersChanged = true;

//                  DBTRACE(MDM_CREATE_GRAIN_FILTER_TIMER.ReadAsString());
               }

               CHRTimer MDM_FILTER_BLOBS_TIMER;

               // Perform the grain suppression, removing blobs we suspect are grain.
               BlobIterator blobIterator(&blobPixelArrayWrapper);
               int unfilteredOutBlobs = 0;
               for (auto blobLabel = blobIterator.getNextBlob(); blobPixelArrayWrapper.isValidAddress(blobLabel); blobLabel = blobIterator.getNextBlob())
               {
                  BlobPixelIterator blobPixelIter(blobLabel, &blobPixelArrayWrapper);
                  if (_grainFilters[afspSetting[lSetting].lGrainFilterComponent].isThisBlobActuallyGrain(blobPixelIter, afspSetting[lSetting].lGrainFilterParam))
                  {
                     blobPixelArrayWrapper.deleteBlob(blobLabel);
                     continue;
                  }

                  ++unfilteredOutBlobs;
               }

//               DBTRACE(MDM_FILTER_BLOBS_TIMER.ReadAsString());
            }

            // Run specular highlight suppression?
            if (afsp->bUseAlphaMatteToFindDebris && afsp->fLuminanceThreshold < 1.F)
            {
               int tooBrightThreshold[3] =
                  {int(afsp->fLuminanceThreshold * laUpperBound[0]),
                   int(afsp->fLuminanceThreshold * laUpperBound[1]),
                   int(afsp->fLuminanceThreshold * laUpperBound[2])};
               const float TooBrightCountThreshold = 0.1F;

               BlobIterator blobIterator(&blobPixelArrayWrapper);
               int unfilteredOutBlobs = 0;
               for (auto blobLabel = blobIterator.getNextBlob();
                    blobPixelArrayWrapper.isValidAddress(blobLabel);
                    blobLabel = blobIterator.getNextBlob())
               {
                  BlobPixelIterator blobPixelIter2(blobLabel, &blobPixelArrayWrapper);
                  int lNPixelsInBlob = 0;
                  int tooBrightCount = 0;
                  for (int lRowCol = blobPixelIter2.getNextPixel();
                       lRowCol >= 0;
                       lRowCol = blobPixelIter2.getNextPixel())
                  {
                     ++lNPixelsInBlob;
                     auto lRowColCom = lRowCol * 3;
                     if (uspSrc[0][lRowColCom] > tooBrightThreshold[0]
                     || uspSrc[0][lRowColCom + 1] > tooBrightThreshold[1]
                     || uspSrc[0][lRowColCom + 2] > tooBrightThreshold[2])
                     {
                        ++tooBrightCount;
                     }
                  }

                  if (tooBrightCount > int(lNPixelsInBlob * TooBrightCountThreshold))
                  {
                     blobPixelArrayWrapper.deleteBlob(blobLabel);
                     continue;
                  }

                  ++unfilteredOutBlobs;
               }
            }

            // Grow the detection blobs with neighbors and surround pixels.
            // NOTE: THIS IS DELIBERATELY DONE SLOWLY (i.e. by scanning the frame rather
            // than iterating through the blobs) because we think ContrastFcn depends
            // on that behavior! And who knows, it might not be so slow because of
            // memory caching!
            CHRTimer PROCESS_GROW_BLOBS_TIMER;
            ResetPixelMask();

            long lRowColStart = 0;
            long lRowColStop = lNRow * lNCol;
            for (long lRowCol = lRowColStart; lRowCol < lRowColStop; ++lRowCol)
            {
               if (blobPixelArrayWrapper.isInABlob(lRowCol))
               {
                  ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
               }
            }

//            DBTRACE(PROCESS_GROW_BLOBS_TIMER.ReadAsString());

			}
			else
			{
				CHRTimer PROCESS_FIND_NORMAL_DEBRIS_TIMER;

				afsp->lActiveComponent = afsp->lPrincipalComponent;

				if (afsp->lActiveComponent < 0)
				{
					afsp->lActiveComponent = 0;
				}

            // Assign image pointers - Must be done AFTER setting lActiveComponent!!!!
            AssignPointers(lSetting);

				// This is old stuff, we used to do grain suppression for normal filters as well.  Should be removed QQQ TODO
            afsp->lGrainFilterComponent = afsp->lActiveComponent;
				MTIassert(afsp->lGrainFilterComponent >= 0 && afsp->lGrainFilterComponent < 3);

				// use range contrast to init blobs
				try
				{
					RangeFcn(lSetting);
				}
				catch (...)
				{
					TRACE_0(errout << "EXCEPTION CAUGHT: in AF::RangeFcn");
					FreeEverything();
					return 671;
				}

//				DBTRACE(PROCESS_FIND_NORMAL_DEBRIS_TIMER.ReadAsString());
			}
		}

      TRACE_3(errout << "AAF_PROCESS: ID'ED BLOBS");

      if ((EmergencyStopFlagPtr != NULL) && *EmergencyStopFlagPtr)
      {
         return iRet;
		}

		// Merge defects into blobs and perform repair
		CHRTimer PROCESS_ANALYZE_BLOBS_TIMER;
		try
		{
			AnalyzeBlobs(lSetting, blobPixelArrayWrapper);
      }
      catch (...)
      {
         TRACE_0(errout << "EXCEPTION CAUGHT: in AF::AnalyzeBlobs");
         FreeEverything();
         return 673;
      }

//      DBTRACE(PROCESS_ANALYZE_BLOBS_TIMER.ReadAsString());
//		TRACE_0(errout << "==== AAF_PROCESS: Processing COMPLETED for " << lSetting << "=====");
	}

//   DBTRACE(PROCESS_ALL_SETTINGS_TIMER.ReadAsString());

	// Reset the pixel mask for next time
	// This was put in to match what used to happen, but I don't think it's
	// needed because we always either recompute or reset before processing
	// each setting.
	ResetPixelMask();

   // Copy out the (maybe) updated grain filters.
   if (grainFiltersChanged)
   {
      grainFilters = _grainFilters;
   }

//   DBTRACE(PROCESS_TIMER.ReadAsString());

	return iRet;
} /* Process */
// -------------------------------------------------------------------------

Ipp8uArray CAlgorithmAF::MakeDetectionMap(GrainFilterSet &grainFilters, BlobPixelArrayWrapper **defectBlobPixelArrayWrapperPtrPtr)
{
	TRACE_3(errout << "AAF::MakeDetectionMap: ENTER");
   CHRTimer MakeDetectionMap_TIMER;

	_bMakingDetectionMapOnly = true;

	int iRet = 0;
   _detectionMap.clear();
   _grainFilters = grainFilters;
   _precomputedBlobPixelArrayWrapperPtrPtr = defectBlobPixelArrayWrapperPtrPtr;
	bool grainFiltersChanged = false;
	long lSetting = 0;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;

	MTIassert(lNSetting == 1); // We only build maps for a single filter.
   if (lNSetting < 1)
	{
		return _detectionMap;
	}

	// THIS IS ONLY FOR ALPHA FILTER!
	MTIassert(afspSetting[lSetting].bUseAlphaMatteToFindDebris);
	if (!afspSetting[lSetting].bUseAlphaMatteToFindDebris)
	{
		return _detectionMap;
	}

	// Do nothing if there's no alpha matte!
	if (eAlphaMatteType == IF_ALPHA_MATTE_NONE)
	{
		return _detectionMap;
	}

	if (bDoDeferredDimensionSetup)
	{
		// allocate storage for PixelMask
		try
		{
			iRet = AllocPixelMask();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::AllocPixelMask");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return _detectionMap;
		}

		// allocate blob
		try
		{
			iRet = SetupBlob();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::SetupBlob");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return _detectionMap;
		}

		// allocate neighbors
		try
		{
			iRet = SetupNeighbors();
		}
		catch (...)
		{
			TRACE_0(errout << "ERROR: OUT OF MEMORY in AF::SetupNeighbors");
			iRet = ERR_PLUGIN_AF_MALLOC;
		}

		if (iRet)
		{
			FreeEverything();
			return _detectionMap;
		}

		bDoDeferredDimensionSetup = false; // only once!
	}

	// Initialize the pixel mask if necessary
	if (uspPixelMask == NULL)
	{
		TRACE_0(errout << "INTERNAL ERROR: pixel mask was never allocated!");
		FreeEverything();
		return _detectionMap;
	}

   // Pan motion not implemented
	lPanMotionA = 0;
	lPanMotionB = 0;
	lMinPanMotion = 0;
	lMaxPanMotion = 0;

	afspSetting[lSetting].lActiveComponent = 1; // Green (assume RGBA)
	afspSetting[lSetting].lGrainFilterComponent = 3;  // Y

   // If we have a previously computed BlobPixelArray, this will be pretty quick!
   if (*defectBlobPixelArrayWrapperPtrPtr != nullptr)
   {
      FilterDefectPixelBlobs(lSetting);

//      DBTRACE(MakeDetectionMap_TIMER.ReadAsString());
      return _detectionMap;
   }

   // Assign image pointers -- Must be done AFTER setting lActiveComponent!!!
   try
   {
      AssignPointers(lSetting);
   }
   catch (...)
   {
      TRACE_0(errout << "EXCEPTION CAUGHT: in AF::AssignPointers");
      FreeEverything();
      return _detectionMap;
   }

	CHRTimer MDM_SETUP_PIXEL_MASK_1_TIMER;
	SetupPixelMask();
//	DBTRACE(MDM_SETUP_PIXEL_MASK_1_TIMER.ReadAsString());

	// Scan the alpha matte for defect pixels.
	CHRTimer MDM_DETECT_ALPHA_DEBRIS_TIMER;
	SetPixelMaskFromAlphaChannel(lSetting);
//	DBTRACE(MDM_DETECT_ALPHA_DEBRIS_TIMER.ReadAsString());

// TODO : CALL IdentifyBlobs()
	// Make a blob map of the defective pixels.
   CHRTimer MDM_IDENTIFY_BLOBS_TIMER;
   BlobPixelArrayWrapper blobPixelArrayWrapper(_blobPixelArray, lNCol, lNRow);
   BlobIdentifier blobIdentifier(&blobPixelArrayWrapper);
   blobIdentifier.identifyBlobs(PM_DEFECT_IN_PIXEL, uspPixelMask);
//   DBTRACE(MDM_IDENTIFY_BLOBS_TIMER.ReadAsString());

   CHRTimer MDM_PREPARE_FOR_ITER_TIMER;
   blobPixelArrayWrapper.prepareForIteration();
//   DBTRACE(MDM_PREPARE_FOR_ITER_TIMER.ReadAsString());
//

   if (afspSetting[lSetting].bUseGrainFilter)
   {
      long grainFilterComponent = afspSetting[lSetting].lGrainFilterComponent;
      if (_grainFilters[grainFilterComponent].isNull())
      {
         CHRTimer MDM_CREATE_GRAIN_FILTER_TIMER;

         unsigned short maxValue = laUpperBound[afspSetting[lSetting].lActiveComponent];
         _grainFilters[grainFilterComponent] =
               GrainFilter::CreateFrom(
                     uspSrc[0],
                     lNRow,
                     lNCol,
                     grainFilterComponent,
                     maxValue,
                     &blobPixelArrayWrapper,
                     PM_DEFECT_IN_PIXEL);

         MTIassert(!_grainFilters[grainFilterComponent].isNull());

         grainFiltersChanged = true;

//         DBTRACE(MDM_CREATE_GRAIN_FILTER_TIMER.ReadAsString());
      }

      //	CHRTimer MDM_FILTER_TIMER;
      //	FilterDefectPixelBlobs(lSetting);
      //	DBTRACE(MDM_FILTER_TIMER.ReadAsString());
      CHRTimer MDM_FILTER_BLOBS_TIMER;

      BlobIterator blobIterator(&blobPixelArrayWrapper);
      int unfilteredOutBlobs = 0;
      for (auto blobLabel = blobIterator.getNextBlob(); blobPixelArrayWrapper.isValidAddress(blobLabel); blobLabel = blobIterator.getNextBlob())
      {
         BlobPixelIterator blobPixelIter(blobLabel, &blobPixelArrayWrapper);
         if (_grainFilters[afspSetting[lSetting].lGrainFilterComponent].isThisBlobActuallyGrain(blobPixelIter, afspSetting[lSetting].lGrainFilterParam))
         {
            blobPixelArrayWrapper.deleteBlob(blobLabel);
            continue;
         }

         ++unfilteredOutBlobs;
      }

//      DBTRACE(MDM_FILTER_BLOBS_TIMER.ReadAsString());
   }

   // Run specular highlight suppression?
   if (afsp->bUseAlphaMatteToFindDebris && afsp->fLuminanceThreshold < 1.F)
   {
      int tooBrightThreshold[3] =
         {int(afsp->fLuminanceThreshold * laUpperBound[0]),
          int(afsp->fLuminanceThreshold * laUpperBound[1]),
          int(afsp->fLuminanceThreshold * laUpperBound[2])};
      const float TooBrightCountThreshold = 0.1F;

      BlobIterator blobIterator(&blobPixelArrayWrapper);
      int unfilteredOutBlobs = 0;
      for (auto blobLabel = blobIterator.getNextBlob();
           blobPixelArrayWrapper.isValidAddress(blobLabel);
           blobLabel = blobIterator.getNextBlob())
      {
         BlobPixelIterator blobPixelIter2(blobLabel, &blobPixelArrayWrapper);
         int lNPixelsInBlob = 0;
         int tooBrightCount = 0;
         for (int lRowCol = blobPixelIter2.getNextPixel();
              lRowCol >= 0;
              lRowCol = blobPixelIter2.getNextPixel())
         {
            ++lNPixelsInBlob;
            auto lRowColCom = lRowCol * 3;
            if (uspSrc[0][lRowColCom] > tooBrightThreshold[0]
            || uspSrc[0][lRowColCom + 1] > tooBrightThreshold[1]
            || uspSrc[0][lRowColCom + 2] > tooBrightThreshold[2])
            {
               ++tooBrightCount;
            }
         }

         if (tooBrightCount > int(lNPixelsInBlob * TooBrightCountThreshold))
         {
            blobPixelArrayWrapper.deleteBlob(blobLabel);
            continue;
         }

         ++unfilteredOutBlobs;
      }
   }

   CHRTimer FDPB_MAKE_MAP_TIMER;

   // This one could be done by iterating through blobs instead of doing
   // a complete scan.
   _detectionMap = Ipp8uArray({lNCol, lNRow});
//      auto mapIter = _detectionMap.begin();
//
//		long lRowColStart = 0;
//		long lRowColStop = lNRow * lNCol;
//		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; ++lRowCol)
//		{
//			*(mapIter++) = blobPixelArrayWrapper.isInABlob(lRowCol) ? 1 : 0;
//		}
   MakeMapParams mParams = { _detectionMap, &blobPixelArrayWrapper };
   SynchronousThreadRunner multithread(NumberOfSlices, &mParams, RunThreadToMakeMap);
   multithread.Run();

//   DBTRACE(FDPB_MAKE_MAP_TIMER.ReadAsString());

   // Copy out the (maybe) updated grain filters.
   if (grainFiltersChanged)
   {
      grainFilters = _grainFilters;
   }

//	DBTRACE(MakeDetectionMap_TIMER.ReadAsString());

	return _detectionMap;
}
// -------------------------------------------------------------------------

void CAlgorithmAF::AssignPointers (long lSetting)
{
	long lCom, lCnt, lSize;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;
	POINTER_STRUCT *psp = &afap->ps;

	lCnt = 1;
	for (lCom = 0; lCom < lNCom; lCom++)
	{
		if (lCom == afsp->lActiveComponent)
		{
			psp->uspCurr0 = uspSrc[0] + lCom;
			psp->uspRefA0 = uspSrc[1] + lCom;
			psp->uspRefB0 = uspSrc[2] + lCom;
		}
		else if (lCnt == 1)
		{
			psp->uspCurr1 = uspSrc[0] + lCom;
			psp->uspRefA1 = uspSrc[1] + lCom;
			psp->uspRefB1 = uspSrc[2] + lCom;
			lCnt++;
		}
		else if (lCnt == 2)
		{
			psp->uspCurr2 = uspSrc[0] + lCom;
			psp->uspRefA2 = uspSrc[1] + lCom;
			psp->uspRefB2 = uspSrc[2] + lCom;
			lCnt++;
		}
	}

	// establish addresses of spatial neighbors
	lSize = SPATIAL_CONTRAST_OFFSET;

	psp->uspCurrN = psp->uspCurr0 - lNCol * lSize * lNCom; /* north */
	psp->uspCurrS = psp->uspCurr0 + lNCol * lSize * lNCom; /* south */
	psp->uspCurrE = psp->uspCurr0 + lSize * lNCom; /* east */
	psp->uspCurrW = psp->uspCurr0 - lSize * lNCom; /* west */

	psp->uspCurrNE = psp->uspCurrN + lSize * lNCom; /* north-east */
	psp->uspCurrNW = psp->uspCurrN - lSize * lNCom; /* north-west */
	psp->uspCurrSE = psp->uspCurrS + lSize * lNCom; /* south-east */
	psp->uspCurrSW = psp->uspCurrS - lSize * lNCom; /* south-west */

	psp->uspRefAN = psp->uspRefA0 - lNCol * lNCom; /* north */
	psp->uspRefAS = psp->uspRefA0 + lNCol * lNCom; /* south */
	psp->uspRefAE = psp->uspRefA0 + lNCom; /* east */
	psp->uspRefAW = psp->uspRefA0 - lNCom; /* west */

	psp->uspRefBN = psp->uspRefB0 - lNCol * lNCom; /* north */
	psp->uspRefBS = psp->uspRefB0 + lNCol * lNCom; /* south */
	psp->uspRefBE = psp->uspRefB0 + lNCom; /* east */
	psp->uspRefBW = psp->uspRefB0 - lNCom; /* west */

	return;
} /* AssignPointers */
// -------------------------------------------------------------------------

int CAlgorithmAF::AnalyzeBlobs(long lSetting, BlobPixelArrayWrapper &blobPixelArrayWrapper)
{
	long lLabel, lRowCol;
	bool bFinishBlob;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;
	StatsAF *afStats = &_afStats[lSetting];
	afStats->totalProcessCandidateBlobs = 0;
	afStats->totalBlobsActuallyProcessed = 0;
	// afStats->totalTooSmallBlobs = 0;
	afStats->totalTooBigBlobs = 0;
   afStats->totalBlobsIdentifiedAsGrain = 0;
	afStats->totalDustBlobsInpainted = 0;
	afStats->finalSizeOfLargestBlob = 0;
	afStats->finalSizeOfSmallestBlob = 0x7FFFFFFF;
	afStats->totalBlobsDetected = 0;
	afStats->sizeOfLargestBlobDetected = 0;
	afStats->sizeOfSmallestBlobDetected = 0X7fffffff;
	afStats->totalBlobsAfterSizeRangeCheck = 0;

	// Sort pixels into blobs
////	BlobPixelArrayWrapper blobPixelArrayWrapper(_blobPixelArray, lNCol, lNRow);

   if (_detectionStuffIsValid == false)
   {
		BlobIdentifier blobIdentifier(&blobPixelArrayWrapper);

		CHRTimer ANALYZE_BLOBS_IDENTIFY_TIMER;

		try
		{
			MTI_UINT16 pixelFilterMask = PixelMaskType(lSetting) | ((afsp->lPrincipalComponent == 3) ? PM_DEFECT_IN_PIXEL : 0);
			blobIdentifier.identifyBlobs(pixelFilterMask, uspPixelMask);
		}
		catch (...)
		{
			TRACE_0(errout << "EXCEPTION CAUGHT: in IdentifyBlobs");
			FreeEverything();
			return 674;
		}

//		DBTRACE(ANALYZE_BLOBS_IDENTIFY_TIMER.ReadAsString());
		CHRTimer ANALYZE_BLOBS_PREP_ITER_TIMER;

		try
		{
			blobPixelArrayWrapper.prepareForIteration();
		}
		catch (...)
		{
			TRACE_0(errout << "EXCEPTION CAUGHT: in blobPixelArrayWrapper.prepareForIteration()");
			FreeEverything();
			return 675;
		}

//		DBTRACE(ANALYZE_BLOBS_PREP_ITER_TIMER.ReadAsString());
	}

   CHRTimer ANALYZE_BLOBS_PROCESS_TIMER;

	TRACE_3(errout << "AAF_PROCESS: After filling blob buckets ");

	lpBlobRC = pPreallocedMemory->getBlobRCAddr();
	lpUnexclRowColComLists = pPreallocedMemory->getUnexclRowColComLists();

	// Process each blob. Blobs are represented by "buckets" full of pixels.
	lNPixelsInBlob = 0;

   // "Too bright" hack
   bool checkForTooBright = afsp->bUseAlphaMatteToFindDebris && afsp->fLuminanceThreshold < 1.F;
   int tooBrightThreshold[3] =
      {int(afsp->fLuminanceThreshold * laUpperBound[0]),
       int(afsp->fLuminanceThreshold * laUpperBound[1]),
       int(afsp->fLuminanceThreshold * laUpperBound[2])};

	BlobIterator blobIterator(&blobPixelArrayWrapper);
	for (lLabel = blobIterator.getNextBlob(); lLabel >= 0; lLabel = blobIterator.getNextBlob())
	{
		// See if we should stop
		if ((EmergencyStopFlagPtr != NULL) && *EmergencyStopFlagPtr)
		{
			TRACE_0(errout << "INFO: Autofilter processing stopped after " << afStats->totalBlobsActuallyProcessed << " blobs were processed");
			break;
		}

		++afStats->totalBlobsDetected;

		if (_detectionStuffIsValid == false)
		{
         // Copy the pixel addresses to a linear array for speedy random access
			BlobPixelIterator blobPixelIterator(lLabel, &blobPixelArrayWrapper);
			bool tooBig = false;
         bool tooBright = false;
         int tooBrightCount = 0;
         AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;

			for (lNPixelsInBlob = 0, lRowCol = blobPixelIterator.getNextPixel();
			     lRowCol >= 0 && tooBig == false;
			     lRowCol = blobPixelIterator.getNextPixel())
			{
				lpBlobRC[lNPixelsInBlob++] = lRowCol;
				if (lNPixelsInBlob > afsp->lMaxSizeThresh)
				{
					tooBig = true;
				}

            if (checkForTooBright)
            {
               auto lRowColCom = lRowCol * 3;
               if (uspSrc[0][lRowColCom] > tooBrightThreshold[0]
               || uspSrc[0][lRowColCom + 1] > tooBrightThreshold[1]
               || uspSrc[0][lRowColCom + 2] > tooBrightThreshold[2])
               {
                  ++tooBrightCount;
               }
            }
			}

         if (tooBrightCount > int(lNPixelsInBlob * 0.1F))
         {
            tooBright = true;
         }

         afStats->sizeOfSmallestBlobDetected = std::min<int>(afStats->sizeOfSmallestBlobDetected, lNPixelsInBlob);
         afStats->sizeOfLargestBlobDetected = std::max<int>(afStats->sizeOfLargestBlobDetected, lNPixelsInBlob);

         if (tooBig)
         {
            ++afStats->totalTooBigBlobs;
            continue;
         }

         if (lNPixelsInBlob < afsp->lMinSizeThresh)
         {
            ++afStats->totalTooSmallBlobs;
            continue;
         }

         if (tooBright)
         {
            ++afStats->totalTooBrightBlobs;
            continue;
         }


         ++afStats->totalBlobsAfterSizeRangeCheck;
         afStats->sizeOfSmallestBlobAfterSizeRangeCheck = std::min<int>(afStats->sizeOfSmallestBlobAfterSizeRangeCheck, lNPixelsInBlob);
         afStats->sizeOfLargestBlobAfterSizeRangeCheck = std::max<int>(afStats->sizeOfLargestBlobAfterSizeRangeCheck, lNPixelsInBlob);

         // TRACE_0(errout << "PROCESS BLOB #" << lLabel << " (lNPixelsInBloB == " << lNPixelsInBlob << "?)");

         // Process the blob.
         ++afStats->totalProcessCandidateBlobs;
      }

		try
		{
			bool bProcessed = ProcessBlob(lSetting);

			afStats->totalBlobsActuallyProcessed += bProcessed ? 1 : 0;
			afStats->totalDustBlobsInpainted += (bProcessed && afap->bSurroundUseSelf) ? 1 : 0;
			afStats->finalSizeOfLargestBlob = std::max<int>(afStats->finalSizeOfLargestBlob, bProcessed ? lNPixelsInBlob : 0);
			afStats->finalSizeOfSmallestBlob = std::min<int>(afStats->finalSizeOfSmallestBlob, bProcessed ? lNPixelsInBlob : 0x7FFFFFFF);
		}
		catch (...)
		{
			TRACE_0(errout << "EXCEPTION CAUGHT: in AF::ProcessBlob");
			FreeEverything();
			return 676;
		}
	}

//   DBTRACE(ANALYZE_BLOBS_PROCESS_TIMER.ReadAsString());

//	TRACE_0(errout << lSetting << " INITIAL DETECT:   " << afStats->totalBlobsDetected << " blobs, " << "largest = " <<
//			afStats->sizeOfLargestBlobDetected << ", " << "smallest = " << afStats->sizeOfSmallestBlobDetected << ", " << "too many blobs? " <<
//			afStats->blobCountLimitExceeded);
//	TRACE_0(errout << lSetting << " AFTER SIZE CHECK: " << afStats->totalBlobsAfterSizeRangeCheck << " blobs, " << "largest = " <<
//			afStats->sizeOfLargestBlobAfterSizeRangeCheck << ", " << "smallest = " << afStats->sizeOfSmallestBlobAfterSizeRangeCheck);
//	TRACE_0(errout << lSetting << " FINAL BLOB COUNT: " << afStats->totalBlobsActuallyProcessed << " blobs " << "largest = " <<
//			afStats->finalSizeOfLargestBlob << ", " << "smallest = " << afStats->finalSizeOfSmallestBlob << "of which " <<
//			afStats->totalDustBlobsInpainted << " were inpainted dust).");

	return 0;
}
// -------------------------------------------------------------------------

bool CAlgorithmAF::ProcessBlob(long lSetting)
{
   bool retVal = false;
   AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
   AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;

   bppBlobProc->Initialize(afap, uspPixelMask, ucpROIBlend, prlpOriginalValues,
                           _motionStuffIsValid ? &_bestMotionCacheMap : nullptr,
                           _modifiedPixelMap);

   bppBlobProc->SetImageInfo(lNRow, lNCol, lNCom, uspSrc, uspDst);
   bppBlobProc->SetEmergencyStopPtr(EmergencyStopFlagPtr);
   bppBlobProc->SetTempStuff(lpUnexclRowColComLists, lpMotionDiffs);

   retVal = bppBlobProc->ProcessBlob(lNPixelsInBlob, lpBlobRC, afsp, PixelMaskType(lSetting));

   return retVal;
}
// -------------------------------------------------------------------------

int CAlgorithmAF::PixelMaskType(long lSetting)
		/*
		 This function converts the lSetting value into its equivalent value
		 of PM_DEFECT_FILTER_?.
		 */
{
	if (lSetting == 0)
	{
		return PM_DEFECT_FILTER_0;
	}
	else if (lSetting == 1)
	{
		return PM_DEFECT_FILTER_1;
	}
	else if (lSetting == 2)
	{
		return PM_DEFECT_FILTER_2;
	}
	else if (lSetting == 3)
	{
		return PM_DEFECT_FILTER_3;
	}
	else if (lSetting == 4)
	{
		return PM_DEFECT_FILTER_4;
	}
	else if (lSetting == 5)
	{
		return PM_DEFECT_FILTER_5;
	}
	else
	{
		return PM_NONE;
	}
} /* PixelMaskType */

////////////////////////////////////////////////////////////////////////
//
// ALPHA FUNCTIONS
//
///////////////////////////////////////////////////////////////////////

void CAlgorithmAF::IdentifyBlobs(BlobPixelArrayWrapper &blobPixelArrayWrapper)
{
	if (_precomputedBlobPixelArrayWrapperPtrPtr == nullptr || *_precomputedBlobPixelArrayWrapperPtrPtr == nullptr)
	{
		CHRTimer FDPB_IDENTIFY_BLOBS_TIMER;
		BlobIdentifier blobIdentifier(&blobPixelArrayWrapper);
		blobIdentifier.identifyBlobs(PM_DEFECT_IN_PIXEL, uspPixelMask);
//		DBTRACE(FDPB_IDENTIFY_BLOBS_TIMER.ReadAsString());

		CHRTimer FDPB_PREPARE_FOR_ITER_TIMER;
		blobPixelArrayWrapper.prepareForIteration();
//		DBTRACE(FDPB_PREPARE_FOR_ITER_TIMER.ReadAsString());

		if (_precomputedBlobPixelArrayWrapperPtrPtr != nullptr)
      {
			CHRTimer FDPB_STASH_BLOB_PIXELS_TIMER;
         *_precomputedBlobPixelArrayWrapperPtrPtr = new BlobPixelArrayWrapper(blobPixelArrayWrapper);
//			DBTRACE(FDPB_STASH_BLOB_PIXELS_TIMER.ReadAsString());
		}
   }
	else
	{
		CHRTimer FDPB_COPY_BLOB_PIXELS_FROM_STASH_TIMER;
		blobPixelArrayWrapper.copyFrom(**_precomputedBlobPixelArrayWrapperPtrPtr);
//		DBTRACE(FDPB_COPY_BLOB_PIXELS_FROM_STASH_TIMER.ReadAsString());
	}
}

void CAlgorithmAF::FilterDefectPixelBlobs(long lSetting)
{
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;

	BlobPixelArrayWrapper blobPixelArrayWrapper(_blobPixelArray, lNCol, lNRow);
	IdentifyBlobs(blobPixelArrayWrapper);

	CHRTimer FDPB_FILTER_BLOBS_TIMER;

	BlobIterator blobIterator(&blobPixelArrayWrapper);
	StatsAF &afStats = _afStats[lSetting];
	afStats.totalTooSmallBlobs = 0;
	afStats.totalBlobsIdentifiedAsGrain = 0;
	int unfilteredOutBlobs = 0;

   // "Too bright" hack
   int tooBrightThreshold[3] =
      {int(afsp->fLuminanceThreshold * laUpperBound[0]),
       int(afsp->fLuminanceThreshold * laUpperBound[1]),
       int(afsp->fLuminanceThreshold * laUpperBound[2])};
   const float TooBrightCountThreshold = 0.1F;

	for (long lRowCol = blobIterator.getNextBlob(); blobPixelArrayWrapper.isValidAddress(lRowCol); lRowCol = blobIterator.getNextBlob())
	{
		int pixelCount = blobPixelArrayWrapper.getBlobPixelCount(lRowCol);
		if (pixelCount < afsp->lMinSizeThresh)
		{
			++afStats.totalTooSmallBlobs;
			blobPixelArrayWrapper.deleteBlob(lRowCol);
			continue;
		}

		BlobPixelIterator blobPixelIter(lRowCol, &blobPixelArrayWrapper);

		if (afsp->bUseGrainFilter
		&& !_grainFilters[afsp->lGrainFilterComponent].isNull()
		&& _grainFilters[afsp->lGrainFilterComponent].isThisBlobActuallyGrain(blobPixelIter, afsp->lGrainFilterParam))
		{
			++afStats.totalBlobsIdentifiedAsGrain;
			blobPixelArrayWrapper.deleteBlob(lRowCol);
			continue;
		}

      if (afsp->bUseAlphaMatteToFindDebris)
      {
         BlobPixelIterator blobPixelIter2(lRowCol, &blobPixelArrayWrapper);
         int lNPixelsInBlob = 0;
         int tooBrightCount = 0;
         for (int lRowCol = blobPixelIter2.getNextPixel();
              lRowCol >= 0;
              lRowCol = blobPixelIter2.getNextPixel())
         {
            ++lNPixelsInBlob;
            auto lRowColCom = lRowCol * 3;
            if (uspSrc[0][lRowColCom] > tooBrightThreshold[0]
            || uspSrc[0][lRowColCom + 1] > tooBrightThreshold[1]
            || uspSrc[0][lRowColCom + 2] > tooBrightThreshold[2])
            {
               ++tooBrightCount;
            }
         }

         if (tooBrightCount > int(lNPixelsInBlob * TooBrightCountThreshold))
         {
            continue;
         }
      }

		++unfilteredOutBlobs;
	}

//	DBTRACE(FDPB_FILTER_BLOBS_TIMER.ReadAsString());

	if (_bMakingDetectionMapOnly)
	{
		CHRTimer FDPB_MAKE_MAP_TIMER;

		// This one could be done by iterating through blobs instead of doing
		// a complete scan.
		_detectionMap = Ipp8uArray({lNCol, lNRow});
//      auto mapIter = _detectionMap.begin();
//
//		long lRowColStart = 0;
//		long lRowColStop = lNRow * lNCol;
//		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; ++lRowCol)
//		{
//			*(mapIter++) = blobPixelArrayWrapper.isInABlob(lRowCol) ? 1 : 0;
//		}
		MakeMapParams mParams = { _detectionMap, &blobPixelArrayWrapper };
      SynchronousThreadRunner multithread(NumberOfSlices, &mParams, RunThreadToMakeMap);
      multithread.Run();

//      DBTRACE(FDPB_MAKE_MAP_TIMER.ReadAsString());
	}
	else
	{
		CHRTimer FDPB_CONTRAST_FCN_TIMER;

		// NOTE: THIS IS DELIBERATELY DONE SLOWLY (i.e. by scanning the frame rather
		// than iterating through the blobs) because we think ContrastFcn depends
		// on that behavior! And who knows, it might not be so slow because of
		// memory caching!
		ResetPixelMask();

		long lRowColStart = 0;
		long lRowColStop = lNRow * lNCol;
		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; ++lRowCol)
		{
			if (blobPixelArrayWrapper.isInABlob(lRowCol))
			{
				ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
			}
		}

//		DBTRACE(FDPB_CONTRAST_FCN_TIMER.ReadAsString());
	}
}

///*
// Used by AlphaFilter.
//
// This function is the highest-level Alpha call. It is responsible
// for establishing which pixels are classified as dirt, based on the
// Alpha channel data.
//
// It can process a 1-bit, 8-bit, or 16-bit alpha mattes which have
// been appended to the back of the intermediate
// */
void CAlgorithmAF::SetPixelMaskFromAlphaChannel(long lSetting)
{
	long lRowColStart = 0;
	long lRowColStop = lNRow * lNCol;

	// the alpha matte begins just beyond the end of the intermediate
	MTI_UINT16 *alphawrd = uspSrc[0] + (lNCom * lRowColStop);

	switch (eAlphaMatteType)
	{
	case IF_ALPHA_MATTE_1_BIT:
		{
			MTI_UINT8 alphamsk = 0x01;
			MTI_UINT8 *alphabyt = (MTI_UINT8*) alphawrd;

			for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				// Only look at pixels in the ROI (full frame if not masked)
				if (uspPixelMask[lRowCol] & PM_DO_NOT_SKIP)
				{
					// If corresponding alpha matte bit is set, this is a defect pixel
					if ((*alphabyt) & alphamsk)
					{
						// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
						uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
					}
				}

				// advance to next Alpha matte bit
				alphamsk <<= 1;
				if (alphamsk == 0)
				{
					alphabyt++;
					alphamsk = 0x01;
				}
			}
		}

      break;

	case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
		{
			MTI_UINT8 alphamsk = 0x03;
			MTI_UINT8 *alphabyt = (MTI_UINT8*) alphawrd;

			for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				// Only look at pixels in the ROI (full frame if not masked)
				if (uspPixelMask[lRowCol] & PM_DO_NOT_SKIP)
				{
					// If corresponding alpha matte bit is set, this is a defect pixel
					if ((*alphabyt) & alphamsk)
					{
						// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
						uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
					}
				}

				// advance to next Alpha matte bits
				alphamsk <<= 2;
				if (alphamsk == 0)
				{
					alphabyt++;
					alphamsk = 0x03;
				}
			}
		}

		break;

	case IF_ALPHA_MATTE_8_BIT:
	case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
		{
			MTI_UINT8 *alphabyt = (MTI_UINT8*) alphawrd;

			for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if ((uspPixelMask[lRowCol] & PM_DO_NOT_SKIP) && alphabyt[lRowCol])
				{
					// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
					uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
				}
			}
		}

		break;

	case IF_ALPHA_MATTE_16_BIT:

		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
		{
			if ((uspPixelMask[lRowCol] & PM_DO_NOT_SKIP) && alphawrd[lRowCol])
			{
				// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
				uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
			}
		}

		break;

	case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:

		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
		{
			// Only look at pixels in the ROI (full frame if not masked)
			if (uspPixelMask[lRowCol] & PM_DO_NOT_SKIP)
			{
				bool isDefective = false;
				if (bAlphaDataIsHalfFloats)
				{
					// Stupid FotoKem hack - they turn Scanity 2-bit alpha dirt map into float
					// (0, 1, 2 are defective and 3 is not). They map 3 -> 1.0 so here I declare
					// anything under 0.6875 to be defective! Note that in our internal half format
					// the sign bit is flipped, so BC00 == 1.0, B800 == 0.5 and so B980 is 0.6875
					// (.5 + .125 + .0625)
					const MTI_UINT16 Point6875 = 0xB980U;
					isDefective = alphawrd[lRowCol] < Point6875;
				}
				else
				{
					// QQQ This looks suspect - don't know where this came from!!
					// If high order byte of corresponding alpha matte wrd is set
					// to something other than 0xFF or 0x00, this is a defect pixel
//					MTI_UINT16 foo = alphawrd[lRowCol] & 0xFF00U;
//					isDefective = foo != 0xFF00U && foo != 0x0000U;
					isDefective = alphawrd[lRowCol] & 0xC000U;
				}

				if (isDefective)
				{
					// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
					uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
				}
			}
		}

		break;

	case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:

		for (long lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
		{
			// NOTE: THIS USED TO CHECK FOR
			// alphawrd[lRowCol] == 0x3ff)
			// NOT SURE WHY!!! QQQ
			if ((uspPixelMask[lRowCol] & PM_DO_NOT_SKIP) && alphawrd[lRowCol])
			{
				// ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
				uspPixelMask[lRowCol] |= PM_DEFECT_IN_PIXEL;
			}
		}

		break;

	default:
		MTIassert(eAlphaMatteType == IF_ALPHA_MATTE_1_BIT
               || eAlphaMatteType == IF_ALPHA_MATTE_2_BIT_EMBEDDED
					|| eAlphaMatteType == IF_ALPHA_MATTE_8_BIT
					|| eAlphaMatteType == IF_ALPHA_MATTE_8_BIT_INTERLEAVED
					|| eAlphaMatteType == IF_ALPHA_MATTE_16_BIT
					|| eAlphaMatteType == IF_ALPHA_MATTE_16_BIT_INTERLEAVED
					|| eAlphaMatteType == IF_ALPHA_MATTE_10_BIT_INTERLEAVED);
		break;
	}
}

////////////////////////////////////////////////////////////////////////
//
// RANGE FUNCTIONS
//
///////////////////////////////////////////////////////////////////////

void CAlgorithmAF::RangeFcn(long lSetting)
		/*
		 Used by AutoFilter.

		 This function is the highest-level range call.  This function
		 is responsible for establishing which pixels are classified as
		 dirt.
		 */
{
	long lMinVal0, lMinVal1, lMinVal2, lMaxVal0, lMaxVal1, lMaxVal2, lCom, lRangeFcnType;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;

	/*
	 Determine the type of Range processing to use.

	 The fastest is to not use range at all.

	 The slowest is to check to make certain each component is
	 above a minimum value and below a maximum value.
	 */

	lRangeFcnType = RFT_NONE;

	for (lCom = 0; lCom < lNCom; lCom++)
	{
		if (afsp->laMinVal[lCom] > laLowerBound[lCom])
		{
			switch (lCom)
			{
			case 0:
				lRangeFcnType |= RFT_COMPONENT0_ABOVE;
				break;
			case 1:
				lRangeFcnType |= RFT_COMPONENT1_ABOVE;
				break;
			case 2:
				lRangeFcnType |= RFT_COMPONENT2_ABOVE;
				break;
			}
		}

		if (afsp->laMaxVal[lCom] < laUpperBound[lCom])
		{
			switch (lCom)
			{
			case 0:
				lRangeFcnType |= RFT_COMPONENT0_BELOW;
				break;
			case 1:
				lRangeFcnType |= RFT_COMPONENT1_BELOW;
				break;
			case 2:
				lRangeFcnType |= RFT_COMPONENT2_BELOW;
				break;
			}
		}
	}

	/*
	 Adjust MinVal and MaxVal so we can use < and > instead of <= and >=.
	 */

	lMinVal0 = afsp->laMinVal[0] - 1;
	lMaxVal0 = afsp->laMaxVal[0] + 1;
	lMinVal1 = afsp->laMinVal[1] - 1;
	lMaxVal1 = afsp->laMaxVal[1] + 1;
	lMinVal2 = afsp->laMinVal[2] - 1;
	lMaxVal2 = afsp->laMaxVal[2] + 1;

	/*
	 There are two types of range functions.  Sometimes we must calculate
	 spatial and temporal contrast.  Othertimes we only calculate temporal
	 contrast.  Temporal contrast calculation is much quicker.
	 */

	if (afsp->bUseContrastSpatial || _bMakingDetectionMapOnly)
	{
		SpatialRangeFcn(lSetting, lRangeFcnType, lMinVal0, lMaxVal0, lMinVal1, lMaxVal1, lMinVal2, lMaxVal2);
	}
	else
	{
		TemporalRangeFcn(lSetting, lRangeFcnType, lMinVal0, lMaxVal0, lMinVal1, lMaxVal1, lMinVal2, lMaxVal2);
	}

	return;
} /* RangeFcn */
// -------------------------------------------------------------------------

void CAlgorithmAF::SpatialRangeFcn(long lSetting, long lRangeFcnType, long lMinVal0, long lMaxVal0, long lMinVal1, long lMaxVal1,
		long lMinVal2, long lMaxVal2)
		/*
		 This function determine which pixels have
		 1.  sufficient spatial contrast
		 2.  sufficient temporal contrast
		 3.  pixel values in the proper range
		 */
{
	long lRowCol, lRowColCom, lPixelMask, lRowColStart, lRowColStop, lRasterStart, lRasterStop, lRaster, lRowColComStart;
	MTI_UINT16 *uspCurr0, *uspCurrN, *uspCurrS, *uspCurrE, *uspCurrW, *uspCurrNE, *uspCurrNW, *uspCurrSE, *uspCurrSW, *uspRefA0, *uspRefB0,
			*uspCurr1, *uspCurr2, *uspRefAN, *uspRefAS, *uspRefAE, *uspRefAW, *uspRefBN, *uspRefBS, *uspRefBE, *uspRefBW,
			*uspSpatialContrastThresh, *uspTemporalContrastThresh, *uspPM;
	int iWhichContrastFcn;
	MTI_UINT16 usTemporalThresh;
	MTI_UINT16 usSpatialThresh;
	MTI_INT32 lCount;

	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;
	POINTER_STRUCT *psp = &afap->ps;

	uspSpatialContrastThresh = afap->uspSpatialContrastThresh;
	uspTemporalContrastThresh = afap->uspTemporalContrastThresh;

	if (afsp->bUseContrastSpatial)
	{
		if (afsp->fContrastSpatial > 0.)
		{
			iWhichContrastFcn = USE_CONTRAST_FCN_POSITIVE;
		}
		else
		{
			iWhichContrastFcn = USE_CONTRAST_FCN_NEGATIVE;
		}
	}
	else
	{
		iWhichContrastFcn = USE_CONTRAST_FCN_NONE;
	}

	/*
	 We need to take precautions that the neighboring pixels do not
	 fall outside the image.  We break the raster into three sections
	 and treat the sections differently.
	 */

	// north-west is most extreme negative pixel
	lRasterStart = (psp->uspCurr0 - psp->uspCurrNW) / lNCom;
	lRasterStart += lMinPanMotion;
	// south-east is most extreme positive pixel
	lRasterStop = lNRow * lNCol - (psp->uspCurrSE - psp->uspCurr0) / lNCom;
	lRasterStop -= lMaxPanMotion;

	if (lRasterStart > lRasterStop)
	{
		return; // nothing to do
	}

   bool doBlobFiltering = true; //afsp->lMinSizeThresh > 1 || afsp->bUseGrainFilter || _bMakingDetectionMapOnly;

	/*
	 In an effort to make the range checking faster, we acknowledge
	 several types of range checking.  The fastest:  RFT_NONE does not
	 check range at all.  The slowest:  RFT_ALL_BOTH requires all
	 components to be both greater than a minimum value and less than
	 a maximum value.
	 */

	for (lRaster = 0; lRaster < 3; lRaster++)
	{
		long lPanMotionLocalA = lPanMotionA;
		long lPanMotionLocalB = lPanMotionB;
		switch (lRaster)
		{
		case 0: // beginning of raster
			lRowColStart = 0;
			lRowColStop = lRasterStart;

			// zero offset pixels
			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;

			if (lPanMotionA < 0)
			{
				lPanMotionLocalA = 0;
			}
			if (lPanMotionB < 0)
			{
				lPanMotionLocalB = 0;
			}

			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;

			// negative offset pixels:  flip them so they won't fall off raster
			uspCurrN = psp->uspCurrS;
			uspCurrW = psp->uspCurrE;
			uspCurrNE = psp->uspCurrSE;
			uspCurrNW = psp->uspCurrSW;
			uspRefAN = psp->uspRefAS + lPanMotionLocalA;
			uspRefAW = psp->uspRefAE + lPanMotionLocalA;
			uspRefBN = psp->uspRefBS + lPanMotionLocalB;
			uspRefBW = psp->uspRefBE + lPanMotionLocalB;

			// positive offset pixels
			uspCurrS = psp->uspCurrS;
			uspCurrE = psp->uspCurrE;
			uspCurrSE = psp->uspCurrSE;
			uspCurrSW = psp->uspCurrSW;
			uspRefAS = psp->uspRefAS + lPanMotionLocalA;
			uspRefAE = psp->uspRefAE + lPanMotionLocalA;
			uspRefBS = psp->uspRefBS + lPanMotionLocalB;
			uspRefBE = psp->uspRefBE + lPanMotionLocalB;
			break;
		case 1:
			lRowColStart = lRasterStart;
			lRowColStop = lRasterStop;

			// zero offset pixels
			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;
			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;

			// negative offset pixels
			uspCurrN = psp->uspCurrN;
			uspCurrW = psp->uspCurrW;
			uspCurrNE = psp->uspCurrNE;
			uspCurrNW = psp->uspCurrNW;
			uspRefAN = psp->uspRefAN + lPanMotionLocalA;
			uspRefAW = psp->uspRefAW + lPanMotionLocalA;
			uspRefBN = psp->uspRefBN + lPanMotionLocalB;
			uspRefBW = psp->uspRefBW + lPanMotionLocalB;

			// positive offset pixels
			uspCurrS = psp->uspCurrS;
			uspCurrE = psp->uspCurrE;
			uspCurrSE = psp->uspCurrSE;
			uspCurrSW = psp->uspCurrSW;
			uspRefAS = psp->uspRefAS + lPanMotionLocalA;
			uspRefAE = psp->uspRefAE + lPanMotionLocalA;
			uspRefBS = psp->uspRefBS + lPanMotionLocalB;
			uspRefBE = psp->uspRefBE + lPanMotionLocalB;
			break;
		case 2:
			lRowColStart = lRasterStop;
			lRowColStop = lNRow * lNCol;

			// zero offset pixels
			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;

			if (lPanMotionA > 0)
			{
				lPanMotionLocalA = 0;
			}
			if (lPanMotionB > 0)
			{
				lPanMotionLocalB = 0;
			}

			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;

			// negative offset pixels
			uspCurrN = psp->uspCurrN;
			uspCurrW = psp->uspCurrW;
			uspCurrNE = psp->uspCurrNE;
			uspCurrNW = psp->uspCurrNW;
			uspRefAN = psp->uspRefAN + lPanMotionLocalA;
			uspRefAW = psp->uspRefAW + lPanMotionLocalA;
			uspRefBN = psp->uspRefBN + lPanMotionLocalB;
			uspRefBW = psp->uspRefBW + lPanMotionLocalB;

			// positive offset pixels:  flip them so they won't fall off raster
			uspCurrS = psp->uspCurrN;
			uspCurrE = psp->uspCurrW;
			uspCurrSE = psp->uspCurrNE;
			uspCurrSW = psp->uspCurrNW;
			uspRefAS = psp->uspRefAN + lPanMotionLocalA;
			uspRefAE = psp->uspRefAW + lPanMotionLocalA;
			uspRefBS = psp->uspRefBN + lPanMotionLocalB;
			uspRefBE = psp->uspRefBW + lPanMotionLocalB;
			break;
		}

		uspPM = uspPixelMask + lRowColStart;
		lRowColComStart = lRowColStart * lNCom;
		uspCurr0 += lRowColComStart;
		uspCurr1 += lRowColComStart;
		uspCurr2 += lRowColComStart;
		uspRefA0 += lRowColComStart;
		uspRefB0 += lRowColComStart;
		uspCurrN += lRowColComStart;
		uspCurrW += lRowColComStart;
		uspCurrNE += lRowColComStart;
		uspCurrNW += lRowColComStart;
		uspRefAN += lRowColComStart;
		uspRefAW += lRowColComStart;
		uspRefBN += lRowColComStart;
		uspRefBW += lRowColComStart;
		uspCurrS += lRowColComStart;
		uspCurrE += lRowColComStart;
		uspCurrSE += lRowColComStart;
		uspCurrSW += lRowColComStart;
		uspRefAS += lRowColComStart;
		uspRefAE += lRowColComStart;
		uspRefBS += lRowColComStart;
		uspRefBE += lRowColComStart;

		switch (lRangeFcnType)
		{
		case RFT_NONE:
			/*
			 This does not check range at all
			 */
			lRowColCom = 0; // QQQ Shouldn't this be lRowColStart??
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
					usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
					lPixelMask = PM_NONE;
					switch (iWhichContrastFcn)
					{
					case USE_CONTRAST_FCN_NEGATIVE:
						if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
						{
							lPixelMask = PM_DEFECT_IN_PIXEL;
						}
						break;
					case USE_CONTRAST_FCN_POSITIVE:
						if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
						{
							lPixelMask = PM_DEFECT_IN_PIXEL;
						}
						break;
					default:
					case USE_CONTRAST_FCN_NONE:
						lPixelMask = PM_DEFECT_IN_PIXEL;
						break;
					}
					if (lPixelMask)
					{
						if (doBlobFiltering)
                  {
                     uspPixelMask[lRowCol] |= lPixelMask;
                  }
                  else
                  {
						   ContrastFcn(lSetting, lRowCol, lPixelMask);
                  }
					}
				}
				uspCurr0 += lNCom;
				lRowColCom += lNCom;
			}
			break;
		case RFT_COMPONENT0_ABOVE:
			/*
			 This checks to see if the PrincipalComponent is ABOVE the minimum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case RFT_COMPONENT0_BELOW:
			/*
			 This checks to see if the Principal component is BELOW the maximum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 < lMaxVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_ABOVE | RFT_COMPONENT0_BELOW):
			/*
			 This checks to see if the Principal component is BETWEEN the minimum
			 and maximum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr0 < lMaxVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_ABOVE | RFT_COMPONENT1_ABOVE | RFT_COMPONENT2_ABOVE):
			/*
			 This checks to see if all components are ABOVE the minimums
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr1 > lMinVal1 && *uspCurr2 > lMinVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_BELOW | RFT_COMPONENT1_BELOW | RFT_COMPONENT2_BELOW):
			/*
			 This checks to see if all components are BELOW the maximums
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 < lMaxVal0 && *uspCurr1 < lMaxVal1 && *uspCurr2 < lMaxVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		default:
			/*
			 This checks to see if all components are BETWEEN the minimums and
			 maximums.
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr0 < lMaxVal0 && *uspCurr1 > lMinVal1 && *uspCurr1 < lMaxVal1 && *uspCurr2 >
							lMinVal2 && *uspCurr2 < lMaxVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						usTemporalThresh = uspTemporalContrastThresh[*uspCurr0];
						usSpatialThresh = uspSpatialContrastThresh[*uspCurr0];
						lPixelMask = PM_NONE;
						switch (iWhichContrastFcn)
						{
						case USE_CONTRAST_FCN_NEGATIVE:
							if (IS_CONTRAST_NEGATIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						case USE_CONTRAST_FCN_POSITIVE:
							if (IS_CONTRAST_POSITIVE_DEFECT(lRowColCom, usTemporalThresh, usSpatialThresh))
							{
								lPixelMask = PM_DEFECT_IN_PIXEL;
							}
							break;
						default:
						case USE_CONTRAST_FCN_NONE:
							lPixelMask = PM_DEFECT_IN_PIXEL;
							break;
						}
						if (lPixelMask)
						{
                     if (doBlobFiltering)
                     {
                        uspPixelMask[lRowCol] |= lPixelMask;
                     }
                     else
                     {
                        ContrastFcn(lSetting, lRowCol, lPixelMask);
                     }
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		}
	}

   if (doBlobFiltering)
   {
		FilterDefectPixelBlobs(lSetting);
   }

	return;
} /* SpatialRangeFcn */
// -------------------------------------------------------------------------

void CAlgorithmAF::TemporalRangeFcn(long lSetting, long lRangeFcnType, long lMinVal0, long lMaxVal0, long lMinVal1, long lMaxVal1,
		long lMinVal2, long lMaxVal2)
		/*
		 This function determine which pixels have
		 1.  sufficient temporal contrast
		 2.  pixel values in the proper range
		 NOTE:  it does not check for spatial contrast.
		 */
{
	long lRowCol, lRowColCom, lPixelMask, lRowColStart, lRowColStop, lRasterStart, lRasterStop, lRaster;
	long lContrastType;
	MTI_UINT16 usTemporalThresh;
	MTI_UINT16 *uspCurr0, *uspRefA0, *uspRefB0, *uspCurr1, *uspCurr2, *uspTemporalContrastThresh, *uspPM;
	long(*ContrastFcnLocal)(MTI_UINT16, MTI_UINT16, MTI_UINT16);

	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	AUTO_FILTER_ALGORITHM *afap = afapAlgorithm + lSetting;
	POINTER_STRUCT *psp = &afap->ps;

	/*
	 Make local copies of the image pointers
	 */

	uspTemporalContrastThresh = afap->uspTemporalContrastThresh;

	if (afsp->bUseContrastTemporal)
	{
		if (afsp->fContrastTemporal > 0.)
		{
			ContrastFcnLocal = (long(*)(MTI_UINT16, MTI_UINT16, MTI_UINT16))TemporalContrastFcnPositive;
			lContrastType = 1;
		}
		else
		{
			ContrastFcnLocal = (long(*)(MTI_UINT16, MTI_UINT16, MTI_UINT16))TemporalContrastFcnNegative;
			lContrastType = -1;
		}
	}
	else
	{
		ContrastFcnLocal = (long(*)(MTI_UINT16, MTI_UINT16, MTI_UINT16))TemporalContrastFcnNone;
		lContrastType = 0;
	}

	/*
	 In an effort to make the range checking faster, we acknowledge
	 several types of range checking.  The fastest:  RFT_NONE does not
	 check range at all.  The slowest:  RFT_ALL_BOTH requires all
	 compoennts to be both greater than a minimum value and less than
	 a maximum value.
	 */
	// most extreme negative pixel
	lRasterStart = lMinPanMotion;
	// most extreme positive pixel
	lRasterStop = lNRow * lNCol - lMaxPanMotion;

	if (lRasterStart > lRasterStop)
	{
		return; // nothing to do
	}

	/*
	 In an effort to make the range checking faster, we acknowledge
	 several types of range checking.  The fastest:  RFT_NONE does not
	 check range at all.  The slowest:  RFT_ALL_BOTH requires all
	 components to be both greater than a minimum value and less than
	 a maximum value.
	 */

	for (lRaster = 0; lRaster < 3; lRaster++)
	{
		long lPanMotionLocalA = lPanMotionA;
		long lPanMotionLocalB = lPanMotionB;
		switch (lRaster)
		{
		case 0: // beginning of raster
			lRowColStart = 0;
			lRowColStop = lRasterStart;

			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;

			if (lPanMotionA < 0)
			{
				lPanMotionLocalA = 0;
			}
			if (lPanMotionB < 0)
			{
				lPanMotionLocalB = 0;
			}

			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;

			break;
		case 1:
			lRowColStart = lRasterStart;
			lRowColStop = lRasterStop;

			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;
			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;
			break;
		case 2:
			lRowColStart = lRasterStop;
			lRowColStop = lNRow * lNCol;

			// zero offset pixels
			uspCurr0 = psp->uspCurr0;
			uspCurr1 = psp->uspCurr1;
			uspCurr2 = psp->uspCurr2;

			if (lPanMotionA > 0)
			{
				lPanMotionLocalA = 0;
			}
			if (lPanMotionB > 0)
			{
				lPanMotionLocalB = 0;
			}

			uspRefA0 = psp->uspRefA0 + lPanMotionLocalA;
			uspRefB0 = psp->uspRefB0 + lPanMotionLocalB;
			break;
		}

		uspPM = uspPixelMask + lRowColStart;
		uspCurr0 += lRowColStart;
		uspCurr1 += lRowColStart;
		uspCurr2 += lRowColStart;
		uspRefA0 += lRowColStart;
		uspRefB0 += lRowColStart;

		switch (lRangeFcnType)
		{
		case RFT_NONE:
			/*
			 This does not check range at all.  This is the "default" case.
			 It should be as fast as possible.

			 Do not call ContrastFcnLocal().  Make these calls be in line.
			 */

			switch (lContrastType)
			{
			case -1: /* negative contrast */
				lRowColCom = 0;
				for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
				{

					if ((EmergencyStopFlagPtr != NULL) && *EmergencyStopFlagPtr)
					{
						break;
					}

					if (*uspPM++&PM_DO_NOT_SKIP)
					{
						usTemporalThresh = uspTemporalContrastThresh[uspCurr0[lRowColCom]];
						if (uspRefA0[lRowColCom] > usTemporalThresh && uspRefB0[lRowColCom] > usTemporalThresh)
						{
							ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
						}
					}
					lRowColCom += lNCom;
				}
				break;
			case 0:
				for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
				{
					if (*uspPM++&PM_DO_NOT_SKIP)
					{

						if ((EmergencyStopFlagPtr != NULL) && *EmergencyStopFlagPtr)
						{
							break;
						}

						ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
					}
				}
				break;
			case 1:
				lRowColCom = 0;
				for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
				{

					if ((EmergencyStopFlagPtr != NULL) && *EmergencyStopFlagPtr)
					{
						break;
					}

					if (*uspPM++&PM_DO_NOT_SKIP)
					{
						usTemporalThresh = uspTemporalContrastThresh[uspCurr0[lRowColCom]];
						if (uspRefA0[lRowColCom] < usTemporalThresh && uspRefB0[lRowColCom] < usTemporalThresh)
						{
							ContrastFcn(lSetting, lRowCol, PM_DEFECT_IN_PIXEL);
						}
					}
					lRowColCom += lNCom;
				}
				break;
			}
			break;
		case RFT_COMPONENT0_ABOVE:
			/*
			 This checks to see if the PrincipalComponent is ABOVE the minimum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case RFT_COMPONENT0_BELOW:
			/*
			 This checks to see if the principal component is BELOW the maximum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 < lMaxVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_ABOVE | RFT_COMPONENT0_BELOW):
			/*
			 This checks to see if the principal component is BETWEEN the minimum
			 and maximum
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr0 < lMaxVal0)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_ABOVE | RFT_COMPONENT1_ABOVE | RFT_COMPONENT2_ABOVE):
			/*
			 This checks to see if all components are ABOVE the minimums
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr1 > lMinVal1 && *uspCurr2 > lMinVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		case (RFT_COMPONENT0_BELOW | RFT_COMPONENT1_BELOW | RFT_COMPONENT2_BELOW):
			/*
			 This checks to see if all components are BELOW the maximums
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 < lMaxVal0 && *uspCurr1 < lMaxVal1 && *uspCurr2 < lMaxVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		default:
			/*
			 This checks to see if all components are BETWEEN the minimums and
			 maximums.
			 */
			for (lRowCol = lRowColStart; lRowCol < lRowColStop; lRowCol++)
			{
				if (*uspPM++&PM_DO_NOT_SKIP)
				{
					if (*uspCurr0 > lMinVal0 && *uspCurr0 < lMaxVal0 && *uspCurr1 > lMinVal1 && *uspCurr1 < lMaxVal1 && *uspCurr2 >
							lMinVal2 && *uspCurr2 < lMaxVal2)
					{
						lRowColCom = (lRowCol - lRowColStart) * lNCom;
						lPixelMask = ContrastFcnLocal(uspTemporalContrastThresh[*uspCurr0], uspRefA0[lRowColCom], uspRefB0[lRowColCom]);
						if (lPixelMask)
						{
							ContrastFcn(lSetting, lRowCol, lPixelMask);
						}
					}
				}
				uspCurr0 += lNCom;
				uspCurr1 += lNCom;
				uspCurr2 += lNCom;
			}
			break;
		}
	}

	return;
} /* TemporalRangeFcn */
// -------------------------------------------------------------------------

void CAlgorithmAF::ContrastFcn(long lSetting, long lRowCol, long lPixelMask)
{
	long lNeighbor, lNeighborMask, *lpN, *lpA, lNN, lStop, lCnt;
	AUTO_FILTER_SETTING *afsp = afspSetting + lSetting;
	long lEdgeBlurSetting = long(afsp->fEdgeBlur * DEFECT_MAX_EDGE_BLUR_STEPS + 0.5);

	SNeighborhood *neighborhood = afspSetting[lSetting].bUseAlphaMatteToFindDebris
											  ? M_Neighborhood_Alpha
											  : M_Neighborhood;

   MTIassert(lPixelMask != 0);
   MTI_UINT16 *uspPM = uspPixelMask + lRowCol;
   uspPM[0] |= lPixelMask;

   /*
    Loop over two neighborhoods:  nearby neighbors and far away neighbors
    */

   for (lCnt = 0; lCnt < 2; lCnt++)
   {
      switch (lCnt)
      {
      case 0:
         lpN = neighborhood[lEdgeBlurSetting].lpNeighbor;
         lNN = neighborhood[lEdgeBlurSetting].lNNeighbor;
         lNeighborMask = PM_DEFECT_IN_NEIGHBOR | PM_EDGE_BLUR_ALPHA_ALL_FIX | PixelMaskType(lSetting);
         break;
      case 1:
         lpN = neighborhood[lEdgeBlurSetting].lpSurround;
         lNN = neighborhood[lEdgeBlurSetting].lNSurround;
         lNeighborMask = PM_DEFECT_SURROUND | PM_EDGE_BLUR_ALPHA_ALL_SRC | PixelMaskType(lSetting);
         break;
      }

      for (lNeighbor = 0; lNeighbor < lNN; ++lNeighbor)
      {
         uspPM[*lpN++] |= lNeighborMask;
      }
   }

	lNN = neighborhood[lEdgeBlurSetting].lNEdgeBlur;
	if (lNN > 0)
	{
		lpN = neighborhood[lEdgeBlurSetting].lpEdgeBlur;
		lpA = neighborhood[lEdgeBlurSetting].lpFixAlpha;
		lNeighborMask = PM_DEFECT_EDGE_BLUR | PixelMaskType(lSetting);
		for (lNeighbor = 0; lNeighbor < lNN; lNeighbor++)
		{
			long lRowCol = lpN[lNeighbor];
			long lNewAlpha = lpA[lNeighbor];
			long lOldAlpha = uspPM[lRowCol] & PM_EDGE_BLUR_ALPHA_MASK;
			if (lOldAlpha < lNewAlpha)
			{
				uspPM[lRowCol] &= ~PM_EDGE_BLUR_ALPHA_MASK;
				uspPM[lRowCol] |= lNeighborMask | lNewAlpha;
			}
			else
			{
				uspPM[lRowCol] |= lNeighborMask;
			}
		}
	}

	return;
} /* ContrastFcn */
// -------------------------------------------------------------------------

long TemporalContrastFcnNegative(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB)
		/*
		 This function determines if there is sufficient contrast for
		 pixel to be called dirt.  The dirt must be DARKER than its
		 surround.

		 It uses only TEMPORAL contrast.

		 VERY IMPORTANT:  any change here must also be made in the
		 case of (lRangeFcnType = RFT_NONE) and (lContrastType = -1).
		 */
{
	long lPixelMask;

	lPixelMask = PM_NONE;

	/*
	 We need perfect contrast to both frame A and B
	 */

	if (usRefA > usTemporalThresh && usRefB > usTemporalThresh)
	{
		lPixelMask |= PM_DEFECT_IN_PIXEL;
	}

	return lPixelMask;
} /* TemporalContrastFcnNegative */
// -------------------------------------------------------------------------

long TemporalContrastFcnPositive(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB)
		/*
		 This function determines if there is sufficient contrast for
		 pixel to be called dirt.  The dirt must be BRIGHTER than its
		 surround.

		 It uses only TEMPORAL contrast.

		 VERY IMPORTANT:  any change here must also be made in the
		 case of (lRangeFcnType = RFT_NONE) and (lContrastType = 1).
		 */
{
	long lPixelMask;

	lPixelMask = PM_NONE;

	/*
	 We need perfect contrast to both frame A and B
	 */

	if (usRefA < usTemporalThresh && usRefB < usTemporalThresh)
	{
		lPixelMask |= PM_DEFECT_IN_PIXEL;
	}

	return lPixelMask;
} /* TemporalContrastFcnPositive */
// -------------------------------------------------------------------------

long TemporalContrastFcnNone(MTI_UINT16 usTemporalThresh, MTI_UINT16 usRefA, MTI_UINT16 usRefB)
		/*
		 This fuction always returns PM_DEFECT_IN_PIXEL.
		 It is used for cases where the contrast is set to 0, i.e.,
		 all pixels have sufficient contrast.

		 VERY IMPORTANT:  any change here must also be made in the
		 case of (lRangeFcnType = RFT_NONE) and (lContrastType = 0).
		 */
{
	return (PM_DEFECT_IN_PIXEL);
}
// -------------------------------------------------------------------------

SNeighborhood::SNeighborhood() : lNNeighbor(0), lpNeighbor(NULL), lNSurround(0), lpSurround(NULL), lNEdgeBlur(0), lpEdgeBlur(NULL),
		lpFixAlpha(NULL), allocatedMemory(NULL), lNColBasis(0)
{
}

SNeighborhood::~SNeighborhood()
{
	delete[]allocatedMemory;
}

int SNeighborhood::Allocate(long lArgNNeighbor, long lArgNSurround, long lArgEdgeBlur)
{
	// Only allocate if parameters changed or first time through
	if (allocatedMemory != NULL)
	{
		if (lArgNNeighbor == lNNeighbor && lArgNSurround == lNSurround && lArgEdgeBlur == lNEdgeBlur)
		{
			return 0; // No change!
		}
		delete[]allocatedMemory; // Start fresh
	}

	lNNeighbor = lArgNNeighbor;
	lNSurround = lArgNSurround;
	lNEdgeBlur = lArgEdgeBlur;
	allocatedMemory = new long[lNNeighbor + lNSurround + (2 * lNEdgeBlur)];

	int offset = 0;
	lpNeighbor = &allocatedMemory[offset];
	offset += lNNeighbor;
	lpSurround = &allocatedMemory[offset];
	offset += lNSurround;
	lpEdgeBlur = &allocatedMemory[offset];
	offset += lNEdgeBlur;
	lpFixAlpha = &allocatedMemory[offset]; // 4 bit fraction (0 - 0xF)
	// offset += lNEdgeBlur;

	return 0;
}
// -------------------------------------------------------------------------
