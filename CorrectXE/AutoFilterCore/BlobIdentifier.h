#ifndef BlobIdentifierH
#define BlobIdentifierH

#include "AutoFilterCoreDLL.h"
#include "IniFile.h"
#include "MTImalloc.h"

// During BLOB FINDING phase, the "next pixel" link of the HEAD points at the
// TAIL, and for all other pixels the link is to the pixel with the closest
// SMALLER rowcol linear address (i.e after the head, the list is sorted in
// descending address order.
// At the start of the BLOB PIXEL ITERATION phase, the sort order gets reversed;
// the NEXT link for each pixel (including the HEAD) points at the pixel with
// the next LARGER rowcol linear, so the list is now sorted in ASCENDING
// address order. The "next" link for the tail pixel is invalid (-1).

#define INVALID_BLOB_PIXEL_ADDRESS 0x80000000

//------------------------------------------------------------------------------

struct BlobPixel
{
	int head = INVALID_BLOB_PIXEL_ADDRESS;
	int link = INVALID_BLOB_PIXEL_ADDRESS;

	inline void reset()
	{
		head = INVALID_BLOB_PIXEL_ADDRESS;
		link = INVALID_BLOB_PIXEL_ADDRESS;
	}
};
//------------------------------------------------------------------------------

class BlobPixelArrayWrapper
{
	BlobPixel *_blobPixelArray;
   bool _weOwnTheBlobPixels;
	unsigned int _blobPixelArrayWidth;
	unsigned int _blobPixelArrayHeight;
	unsigned int _blobPixelArraySize;
	unsigned int _totalBlobCount = 0;
	unsigned int _totalPixelsInBlobs = 0;
	bool _arrayIsSealed = false;
	BlobPixel _invalidBlobPixel;

public:
	BlobPixelArrayWrapper(BlobPixel *blobPixelArray, unsigned int blobPixelArrayWidth, unsigned int blobPixelArrayHeight)
	: _blobPixelArray(blobPixelArray)
   , _weOwnTheBlobPixels(false)
	, _blobPixelArrayWidth(blobPixelArrayWidth)
	, _blobPixelArrayHeight(blobPixelArrayHeight)
	, _blobPixelArraySize(blobPixelArrayWidth * blobPixelArrayHeight)
	{
   }

   BlobPixelArrayWrapper(unsigned int blobPixelArrayWidth, unsigned int blobPixelArrayHeight)
	: _blobPixelArrayWidth(blobPixelArrayWidth)
	, _blobPixelArrayHeight(blobPixelArrayHeight)
	, _weOwnTheBlobPixels(true)
   {
	 _blobPixelArraySize = blobPixelArrayWidth * blobPixelArrayHeight;
	 _blobPixelArray = (BlobPixel *) MTImalloc(_blobPixelArraySize * sizeof(BlobPixel));
  }

   BlobPixelArrayWrapper(const BlobPixelArrayWrapper &rhs);

   ~BlobPixelArrayWrapper()
   {
      if (_weOwnTheBlobPixels)
      {
         MTIfree(_blobPixelArray);
      }
   }

	inline void initArray()
	{
		for (int i = 0; i < _blobPixelArraySize; ++i)
		{
			getBlobPixelAt(i).reset();
		}
	}

	inline bool isInABlob(int rowcol)
	{
		return getBlobHeadRowCol(rowcol) != INVALID_BLOB_PIXEL_ADDRESS;
	}

	inline unsigned int getBlobPixelArrayWidth()
	{
		return _blobPixelArrayWidth;
	}

	inline unsigned int getBlobPixelArrayHeight()
	{
		return _blobPixelArrayHeight;
	}

	inline BlobPixel &getBlobPixelAt(int rowcol)
	{
		return isValidAddress(rowcol) ? _blobPixelArray[rowcol] : _invalidBlobPixel;
	}

	inline unsigned int getTotalBlobCount()
	{
		return _totalBlobCount;
   }

	inline bool isValidAddress(int rowcol)
	{
		return rowcol >= 0 && rowcol < _blobPixelArraySize;
	}

	inline bool isHeadOfABlob(int rowcol)
	{
		// This pixel is the head of the blob if the head link points to self.
		return getBlobHeadRowCol(rowcol) == rowcol;
	}

	inline int getBlobHeadRowCol(int rowcol)
	{
		int headRowCol = getBlobPixelAt(rowcol).head;
		if (headRowCol == INVALID_BLOB_PIXEL_ADDRESS)
		{
			return INVALID_BLOB_PIXEL_ADDRESS;
		}

		// Hack - to save space, after reversing links, the head's head contains
		// the negated count of pixels in the blob!
		return (headRowCol >= 0) ? headRowCol : rowcol;
	}

	// Given any pixel in a blob, return a reference to the blob head,
	// or to an invalid BlobPixel if pixel is not in any blob.
	inline BlobPixel &getBlobHead(int rowcol)
	{
		return getBlobPixelAt(getBlobHeadRowCol(rowcol));
	}

	unsigned int getBlobPixelCount(int rowcol);

	void createBlob(int rowcol);
	void insertPixelIntoBlob(int blobPixelRowcol, int newBlobPixelRowcol);
	void mergeBlobs(int rowcol1, int rowcol2);
   void deleteBlob(int rowcol);
	void prepareForIteration();

	unsigned int reverseLinks(int rowcol);

   void copyFrom(const BlobPixelArrayWrapper &rhs);

   bool isNull() { return _blobPixelArray == nullptr || _blobPixelArraySize == 0; }
};
//------------------------------------------------------------------------------

class MTI_AUTOFILTERCOREDLL_API BlobPixelIterator
{
	int _next;
	BlobPixelArrayWrapper *_blobPixelArray;

public:
	BlobPixelIterator(int head = INVALID_BLOB_PIXEL_ADDRESS, BlobPixelArrayWrapper *blobPixelArray = nullptr)
	: _next(head), _blobPixelArray(blobPixelArray)
	{}

	unsigned int getPixelCount()
	{
		return _blobPixelArray->getBlobPixelCount(_next);
	}

	int getNextPixel();
};
//------------------------------------------------------------------------------
//
//struct BlobInfo
//{
//	int blobId = INVALID_BLOB_PIXEL_ADDRESS;
//	unsigned int pixelCount = 0;
//};
//------------------------------------------------------------------------------

class MTI_AUTOFILTERCOREDLL_API BlobIterator
{
	BlobPixelArrayWrapper *_blobPixelArray;
	int _nextPixelToCheck = 0;

public:
	BlobIterator(BlobPixelArrayWrapper *blobPixelArray)
	: _blobPixelArray(blobPixelArray)
	{}

	unsigned int getBlobCount()
	{
		return _blobPixelArray->getTotalBlobCount();
	}

	int getNextBlob();
};
//------------------------------------------------------------------------------

class MTI_AUTOFILTERCOREDLL_API BlobIdentifier
{
	BlobPixelArrayWrapper *_blobPixelArray;

public:

	BlobIdentifier(BlobPixelArrayWrapper *blobPixelArray)
	: _blobPixelArray(blobPixelArray)
	{}

	void identifyBlobs(
				MTI_UINT16 pixelFlagMask,
				MTI_UINT16 *pixelFlagArray);

private:

	int _next;
};
//------------------------------------------------------------------------------


#endif // BlobIdentifierH
