// ToolSetup.cpp: implementation of the CToolNode, CToolEdge and CToolSetup
//                classes.
//
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolNode.cpp,v 1.2 2006/01/13 18:21:07 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolNode.h"
#include "BufferPool.h"
#include "err_tool.h"
#include "ImageFormat3.h"
#include "ToolManager.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                       ########################################
// ###     CToolNode Class     #######################################
// ####                       ########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolNode::CToolNode(CToolProcessor *newToolProc, int newInPortCount,
                     int newOutPortCount)
 : toolProc(newToolProc)
{

   ClearToolStatus();

   InitForGraphAlgorithms(-1);

   int i;
   for (i = 0; i < newInPortCount; ++i)
      inPortList.push_back(0);

   for (i = 0; i < newOutPortCount; ++i)
      outPortList.push_back(0);
}

CToolNode::~CToolNode()
{
}

//////////////////////////////////////////////////////////////////////
// Tool Control and Status
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    PutToolCmd
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CToolNode::PutToolCommand(CAutotoolCommand &command)
{
   if (toolProc == 0)
      return TOOL_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // If the tool node does some command processing, put that code here
   // before the command is passed to the Tool Processor

   // Pass the command on to the Tool Processor
   toolProc->PutToolCommand(command);

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    GetToolStatus
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CToolNode::GetToolStatus(CAutotoolStatus &toolStatus)
{
   int retVal;

   if (toolProc == 0)
      return TOOL_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // Get the status from the Tool Processor
   retVal = toolProc->GetToolStatus(toolStatus);
   if (retVal != 0)
      return retVal;

   // If the tool node does some status processing, put that code here
   // before the status is returned to the caller

   return 0;
}

int CToolNode::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   int retVal;

   retVal = toolProc->SetIOConfig(toolIOConfig);
   if (retVal != 0)
      return retVal;  // ERROR: Tool I/O configuration has failed

   return 0;
}

int CToolNode::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   int retVal;

   retVal = toolProc->SetFrameRange(toolFrameRange);
   if (retVal != 0)
      return retVal;  // ERROR: Setting tool's frame range has failed

   return 0;
}

int CToolNode::SetParameters(CToolParameters *toolParams)
{
   int retVal;

   CAutotoolCommand toolCommand;

   toolCommand.command = AT_CMD_SET_PARAMETERS;
   toolCommand.toolParams = toolParams;

   // Tool Parameters are passed via the command queue so that new
   // parameters can be handled synchronously with processing
   retVal = PutToolCommand(toolCommand);
   if (retVal != 0)
      return retVal;

   return 0;
}


int CToolNode::RegisterWithSystem()
{
   int retVal;

   retVal = toolProc->RegisterWithSystem();
   if (retVal != 0)
      return retVal;  // ERROR:

   return 0;
}

int CToolNode::AssignAllBufferAllocators()
{
   int retVal;

   retVal = toolProc->AssignAllBufferAllocators();
   if (retVal != 0)
      return retVal;  // ERROR:

   return 0;
}

void CToolNode::ReleaseAllBufferAllocators()
{
   toolProc->ReleaseAllBufferAllocators();
}

CToolProcessor* CToolNode::GetToolProc() const
{
   return toolProc;
}

//////////////////////////////////////////////////////////////////////
// Get and Put Frame Buffers
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get a specific frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolNode::GetFrame(int port, int frameIndex)
{
   CToolFrameBuffer *frameBuffer;

   frameBuffer = inPortList[port]->GetFrame(frameIndex);

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get the next available frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolNode::GetFrame(int port)
{
   CToolEdge *inToolEdge = inPortList[port];

   if (inToolEdge == 0)
      return 0;

   CToolFrameBuffer *frameBuffer = inToolEdge->GetFrame();

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    PutFrame
//
// Description: Put a frame into a tool's output port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
bool CToolNode::PutFrame(int port, CToolFrameBuffer *frameBuffer)
{
   return outPortList[port]->PutFrame(frameBuffer);
}

bool CToolNode::IsOutPortFull(int port)
{
   return outPortList[port]->IsFrameQueueFull();
}

//------------------------------------------------------------------------
// These are hacks to bypass a writer tool's regular tool processor and
// instead simply write a single frame synchronously or asynchronously
//------------------------------------------------------------------------

int CToolNode::PutFrameSynchronous(int outPortIndex,
                                   CToolFrameBuffer *frameBuffer)
{
   return outPortList[outPortIndex]->PutFrameSynchronous(frameBuffer);
}

int CToolNode::WriteFrameSynchronous(int inPortIndex,
                                     CToolFrameBuffer *frameBuffer)
{
   return toolProc->WriteFrameSynchronous(inPortIndex, frameBuffer);
}

int CToolNode::PutFrameAsynchronous(int outPortIndex,
                                    CToolFrameBuffer *frameBuffer)
{
   return outPortList[outPortIndex]->PutFrameAsynchronous(frameBuffer);
}

int CToolNode::WriteFrameAsynchronous(int inPortIndex,
                                      CToolFrameBuffer *frameBuffer)
{
   return toolProc->WriteFrameAsynchronous(inPortIndex, frameBuffer);
}

//------------------------------------------------------------------------

void CToolNode::RequestFrame(int port, int frameIndex)
{
   // Ask the CToolProcessor to output a particular frame.  For most tools
   // this function does not do anything.  If the source is an Input Node in
   // random-access mode, then the requested frame will be read and will
   // eventually show up in the frame queue
   toolProc->RequestFrame(port, frameIndex);
}

void CToolNode::RemoveRequest(int port, int frameIndex)
{
   toolProc->RemoveRequest(port, frameIndex);
}

int CToolNode::GetInPortCount() const
{
   return (int)inPortList.size();
}

int CToolNode::GetOutPortCount() const
{
   return (int)outPortList.size();
}

CToolNode* CToolNode::GetDstNode(int outPortIndex)
{
   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return 0;

   CToolEdge *edge = outPortList[outPortIndex];
   return edge->GetDstNode();
}

CToolNode* CToolNode::GetSrcNode(int inPortIndex)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return 0;

   CToolEdge *edge = inPortList[inPortIndex];
   return edge->GetSrcNode();
}

void CToolNode::SetInPort(CToolEdge *newSrcEdge, int inPortIndex)
{
   inPortList[inPortIndex] = newSrcEdge;
}

void CToolNode::SetOutPort(CToolEdge *newDstEdge, int outPortIndex)
{
   outPortList[outPortIndex] = newDstEdge;
}

const CImageFormat* CToolNode::GetSrcImageFormat(int inPortIndex) const
{
   return inPortList[inPortIndex]->GetImageFormat();
}

double CToolNode::GetSrcInvisibleFieldsPerFrame(int inPortIndex) const
{
   return inPortList[inPortIndex]->GetInvisibleFieldsPerFrame();
}

void CToolNode::SetDstImageFormat(int outPortIndex,
                                  const CImageFormat *newImageFormat)
{
   // Propagate the image format forward to output port
   outPortList[outPortIndex]->SetImageFormat(newImageFormat);
}

void CToolNode::SetDstInvisibleFieldsPerFrame(int outPortIndex,
                                              double newInvisibleFieldsPerFrame)
{
   // Propagate the invisible fields per frame to output port
   outPortList[outPortIndex]->SetInvisibleFieldsPerFrame(
                                                  newInvisibleFieldsPerFrame);
}

void CToolNode::SetSrcMaxEntryCount(int inPortIndex, int newMaxEntryCount)
{
   // Propagate back the maximum number of frames that will be requested
   // from input frame queue
   inPortList[inPortIndex]->SetMaxEntryCount(newMaxEntryCount);
}

void CToolNode::SetDstMaxEntryCount(int outPortIndex, int newMaxEntryCount)
{
   // Propagate forward the maximum number of frames that will be put
   // to the output frame queue
   outPortList[outPortIndex]->SetMaxEntryCount(newMaxEntryCount);
}

void CToolNode::FlushDstFrameQueue(int outPortIndex)
{
   // Flush output tool frame queue
   outPortList[outPortIndex]->Flush();
}

void CToolNode::SetParentToolSetup(CToolSetup *newToolSetup)
{
   parentToolSetup = newToolSetup;
}

CBufferPool* CToolNode::GetBufferAllocator(int bytesPerBuffer, int bufferCount,
                                           double invisibleFieldsPerFrame)
{
   return toolProc->GetSystemAPI()->GetBufferAllocator(bytesPerBuffer,
                                                       bufferCount,
                                                       invisibleFieldsPerFrame);
}

void CToolNode::ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                                       double invisibleFieldsPerFrame)
{
   toolProc->GetSystemAPI()->ReleaseBufferAllocator(bytesPerBuffer, bufferCount,
                                                    invisibleFieldsPerFrame);
}

//////////////////////////////////////////////////////////////////////
// Tool Status Flags
//////////////////////////////////////////////////////////////////////

void CToolNode::ClearToolStatus()
{
   toolStatusFlags.endOfMaterial = false;
   toolStatusFlags.error = false;
   toolStatusFlags.errorCode = 0;
   toolStatusFlags.stopped = false;
   toolStatusFlags.paused = false;
   toolStatusFlags.exitThread = false;
}

bool CToolNode::IsToolStatusStopped()
{
   return toolStatusFlags.stopped;
}

bool CToolNode::IsToolStatusPaused()
{
   return toolStatusFlags.paused;
}

bool CToolNode::IsToolStatusThreadExited()
{
   return toolStatusFlags.exitThread;
}

bool CToolNode::IsToolStatusEndOfMaterial()
{
   return toolStatusFlags.endOfMaterial;
}

bool CToolNode::IsToolStatusError()
{
   return toolStatusFlags.error;
}

int CToolNode::GetToolStatusErrorCode()
{
   return toolStatusFlags.errorCode;
}

int CToolNode::MonitorToolStatus()
{
   int retVal;
   CAutotoolStatus toolStatus;

   while ((retVal = GetToolStatus(toolStatus)) == 0)
	  {
	  switch (toolStatus.status)
		 {
         case AT_STATUS_STOPPED :
            toolStatusFlags.stopped = true;
            break;
         case AT_STATUS_PAUSED :
            toolStatusFlags.paused = true;
            break;
         case AT_STATUS_PROCESS_ERROR :
            toolStatusFlags.error = true;
            toolStatusFlags.errorCode = toolStatus.errorCode;
            break;
         case AT_STATUS_PROCESS_THREAD_ENDING :
            toolStatusFlags.exitThread = true;
            break;
         case AT_STATUS_END_OF_MATERIAL :
            toolStatusFlags.endOfMaterial = true;
            break;
		 default:   ////JAM
			break;
         }
      }

   if (retVal != 0 && retVal != TOOL_ERROR_TOOL_STATUS_QUEUE_EMPTY)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Member functions for Graph Algorithms
// (See Cormen, Leiserson & Rivest, Intro to Algorithms, Ch. 23)
//////////////////////////////////////////////////////////////////////

void CToolNode::InitForGraphAlgorithms(int newNodeIndex)
{
   nodeColor = GNC_WHITE;
   discoverTimestamp = 0;
   finishTimestamp = 0;
   predecessorNode = 0;
   nodeIndex = newNodeIndex;
}

void CToolNode::Discover(int timestamp)
{
   nodeColor = GNC_GRAY;
   discoverTimestamp = timestamp;
}

void CToolNode::Finish(int timestamp)
{
   nodeColor = GNC_BLACK;
   finishTimestamp = timestamp;
}

EGraphNodeColor CToolNode::GetNodeColor() const
{
   return nodeColor;
}

void CToolNode::SetNodeColor(EGraphNodeColor newColor)
{
   nodeColor = newColor;
}

CToolNode* CToolNode::GetPredecessorNode() const
{
   return predecessorNode;
}

void CToolNode::SetPredecessorNode(CToolNode *newPredecessorNode)
{
   predecessorNode = newPredecessorNode;
}

int CToolNode::GetNodeIndex() const
{
   return nodeIndex;
}


//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                       ########################################
// ###     CToolEdge Class     #######################################
// ####                       ########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolEdge::CToolEdge()
 : srcNode(0), srcPortIndex(-1), dstNode(0), dstPortIndex(-1),
   invisibleFieldsPerFrame(0.0), frameQueue(new CToolFrameQueue)
{
}

CToolEdge::~CToolEdge()
{
   // Delete the frame queue and any CToolFrameBuffers currently in the
   // queue
   delete frameQueue;   
}

//////////////////////////////////////////////////////////////////////
// Get and Put Frame Buffers
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get a specific frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolEdge::GetFrame(int frameIndex)
{
   CToolFrameBuffer *frameBuffer;

   frameBuffer = frameQueue->GetFrame(frameIndex);

   if (frameBuffer == 0)
      {
      // The frame that the caller requested was not in the frame queue.
      // Ask the source node for the frame.  For most nodes this does not
      // do anything.  If the source is an Input Node in random-access mode,
      // then the requested frame will be read and will eventually show
      // up in the frame queue
      srcNode->RequestFrame(srcPortIndex, frameIndex);
      }
   else
      {
      // The frame that the caller requested was in the frame queue.
      // Tell source node that the previous request was fulfilled.
      //TRACE_0(errout << "CToolEdge::GetFrame " << frameIndex);
      srcNode->RemoveRequest(srcPortIndex, frameIndex);
      }

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get next available frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolEdge::GetFrame()
{
   CToolFrameBuffer *frameBuffer;

   frameBuffer = frameQueue->GetFrame();

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    PutFrame
//
// Description: Put a frame into a tool's output port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
bool CToolEdge::PutFrame(CToolFrameBuffer *frameBuffer)
{
   return frameQueue->PutFrame(frameBuffer);
}

bool CToolEdge::IsFrameQueueFull()
{
   return frameQueue->Full();
}

//------------------------------------------------------------------------
// These are hacks to bypass a writer tool's regular tool processor and
// instead simply write a single frame synchronously or asynchronously
//------------------------------------------------------------------------

int CToolEdge::PutFrameSynchronous(CToolFrameBuffer *frameBuffer)
{
   return dstNode->WriteFrameSynchronous(dstPortIndex, frameBuffer);
}

int CToolEdge::PutFrameAsynchronous(CToolFrameBuffer *frameBuffer)
{
   return dstNode->WriteFrameAsynchronous(dstPortIndex, frameBuffer);
}
//------------------------------------------------------------------------

CToolNode* CToolEdge::GetDstNode() const
{
   return dstNode;
}

CToolNode* CToolEdge::GetSrcNode() const
{
   return srcNode;
}

void CToolEdge::SetDstNode(CToolNode *newDstToolNode, int newDstInPortIndex)
{
   dstNode = newDstToolNode;
   dstPortIndex = newDstInPortIndex;

   // Set the link from the destination Node back to this Tool Edge
   dstNode->SetInPort(this, newDstInPortIndex);
}

void CToolEdge::SetSrcNode(CToolNode *newSrcToolNode, int newSrcOutPortIndex)
{
   srcNode = newSrcToolNode;
   srcPortIndex = newSrcOutPortIndex;

   // Set The linke from the source Node back to this Tool Edge
   srcNode->SetOutPort(this, newSrcOutPortIndex);
}

const CImageFormat* CToolEdge::GetImageFormat() const
{
   return frameQueue->GetImageFormat();
}

void CToolEdge::SetImageFormat(const CImageFormat* newImageFormat)
{
   frameQueue->SetImageFormat(newImageFormat);
}

double CToolEdge::GetInvisibleFieldsPerFrame() const
{
   return invisibleFieldsPerFrame;
}

void CToolEdge::SetInvisibleFieldsPerFrame(double newInvisibleFieldsPerFrame)
{
   invisibleFieldsPerFrame = newInvisibleFieldsPerFrame;
}

void CToolEdge::SetMaxEntryCount(int newMaxEntryCount)
{
   frameQueue->SetMaxEntryCount(newMaxEntryCount);
}

void CToolEdge::Flush()
{
   frameQueue->Flush();
}

int CToolEdge::AssignAllBufferAllocators()
{
   int maxEntryCount = frameQueue->GetMaxEntryCount();
   const CImageFormat *imageFormat = frameQueue->GetImageFormat();
   if (maxEntryCount > 0 && imageFormat != 0)
      {
      int bytesPerBuffer = imageFormat->getBytesPerField();
      CBufferPool *bufferAllocator
                    = srcNode->GetBufferAllocator(bytesPerBuffer, maxEntryCount,
                                                  invisibleFieldsPerFrame);
      if (bufferAllocator == 0)
         return TOOL_ERROR_CANNOT_ALLOCATE_FRAME_BUFFERS;
      }

   return 0;
}

void CToolEdge::ReleaseAllBufferAllocators()
{
   int maxEntryCount = frameQueue->GetMaxEntryCount();
   const CImageFormat *imageFormat = frameQueue->GetImageFormat();
   if (maxEntryCount > 0 && imageFormat != 0)
      {
      int bytesPerBuffer = imageFormat->getBytesPerField();
      srcNode->ReleaseBufferAllocator(bytesPerBuffer, maxEntryCount,
                                      invisibleFieldsPerFrame);
      }
}




