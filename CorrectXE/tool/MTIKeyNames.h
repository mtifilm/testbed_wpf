// MTIKeyNames.h  Defines name strings associated with mouse buttons
//                and keyboard keys and associated MTI Virtual Key
//                symbols.
//
/*
$Header: /usr/local/filmroot/tool/include/MTIKeyNames.h,v 1.5 2007/01/25 15:05:51 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////
// Note: all alpha characters in a multicharacter key name strings,
// e.g., "backspace" must be lower case.  Single-character names, e.g., "A"
// are in whatever case is appropriate.

   // Mouse Button Names
   {MTK_LBUTTON        , "lbutton"},
   {MTK_RBUTTON        , "rbutton"},
   {MTK_MBUTTON        , "mbutton"},

   // Key Names
   {MTK_BACK           , "backspace"},
   {MTK_TAB            , "tab"},
   {MTK_RETURN         , "return"},
   {MTK_SHIFT          , "shift"},
   {MTK_CONTROL        , "ctrl"},
   {MTK_ALT            , "alt"},
   {MTK_PAUSE          , "pause"},
   {MTK_CAPSLOCK       , "capslock"},
   {MTK_ESCAPE         , "esc"},
   {MTK_SPACE          , "space"},
   {MTK_PAGEUP         , "pageup"},
   {MTK_PAGEDOWN       , "pagedown"},
   {MTK_END            , "end"},
   {MTK_HOME           , "home"},
   {MTK_LEFT           , "left"},
   {MTK_UP             , "up"},
   {MTK_RIGHT          , "right"},
   {MTK_DOWN           , "down"},
   {MTK_PRINTSCREEN    , "printscreen"},
   {MTK_INSERT         , "insert"},
   {MTK_DELETE         , "del"},
   {MTK_NUMPAD0        , "num0"},
   {MTK_NUMPAD1        , "num1"},
   {MTK_NUMPAD2        , "num2"},
   {MTK_NUMPAD3        , "num3"},
   {MTK_NUMPAD4        , "num4"},
   {MTK_NUMPAD5        , "num5"},
   {MTK_NUMPAD6        , "num6"},
   {MTK_NUMPAD7        , "num7"},
   {MTK_NUMPAD8        , "num8"},
   {MTK_NUMPAD9        , "num9"},
   {MTK_MULTIPLY       , "num*"},
   {MTK_ADD            , "num+"},
   {MTK_SUBTRACT       , "num-"},
   {MTK_DECIMAL        , "num."},
   {MTK_DIVIDE         , "num/"},
   {MTK_F1             , "f1"},
   {MTK_F2             , "f2"},
   {MTK_F3             , "f3"},
   {MTK_F4             , "f4"},
   {MTK_F5             , "f5"},
   {MTK_F6             , "f6"},
   {MTK_F7             , "f7"},
   {MTK_F8             , "f8"},
   {MTK_F9             , "f9"},
   {MTK_F10            , "f10"},
   {MTK_F11            , "f11"},
   {MTK_F12            , "f12"},
   {MTK_NUMLOCK        , "numlock"},
   {MTK_SCROLL         , "scrolllock"},

   // Single-character key names
   {MTK_SEMICOLON      , ";"},
   {MTK_COMMA          , ","},
   {MTK_MINUS          , "-"},
   {MTK_PERIOD         , "."},
   {MTK_SLASH          , "/"},
   {MTK_GRAVE          , "`"},
   {MTK_BRACKETLEFT    , "["},
   {MTK_BACKSLASH      , "\\"},
   {MTK_BRACKETRIGHT   , "]"},
   {MTK_APOSTROPHE     , "'"},
   {MTK_0              , "0"},
   {MTK_1              , "1"},
   {MTK_2              , "2"},
   {MTK_3              , "3"},
   {MTK_4              , "4"},
   {MTK_5              , "5"},
   {MTK_6              , "6"},
   {MTK_7              , "7"},
   {MTK_8              , "8"},
   {MTK_9              , "9"},
#ifdef __BORLANDC__
   {MTK_A              , "a"},
   {MTK_B              , "b"},
   {MTK_C              , "c"},
   {MTK_D              , "d"},
   {MTK_E              , "e"},
   {MTK_F              , "f"},
   {MTK_G              , "g"},
   {MTK_H              , "h"},
   {MTK_I              , "i"},
   {MTK_J              , "j"},
   {MTK_K              , "k"},
   {MTK_L              , "l"},
   {MTK_M              , "m"},
   {MTK_N              , "n"},
   {MTK_O              , "o"},
   {MTK_P              , "p"},
   {MTK_Q              , "q"},
   {MTK_R              , "r"},
   {MTK_S              , "s"},
   {MTK_T              , "t"},
   {MTK_U              , "u"},
   {MTK_V              , "v"},
   {MTK_W              , "w"},
   {MTK_X              , "x"},
   {MTK_Y              , "y"},
   {MTK_Z              , "z"},
#endif // #ifdef __BORLANDC__
#ifdef __sgi
   {MTK_a              , "a"},
   {MTK_b              , "b"},
   {MTK_c              , "c"},
   {MTK_d              , "d"},
   {MTK_e              , "e"},
   {MTK_f              , "f"},
   {MTK_g              , "g"},
   {MTK_h              , "h"},
   {MTK_i              , "i"},
   {MTK_j              , "j"},
   {MTK_k              , "k"},
   {MTK_l              , "l"},
   {MTK_m              , "m"},
   {MTK_n              , "n"},
   {MTK_o              , "o"},
   {MTK_p              , "p"},
   {MTK_q              , "q"},
   {MTK_r              , "r"},
   {MTK_s              , "s"},
   {MTK_t              , "t"},
   {MTK_u              , "u"},
   {MTK_v              , "v"},
   {MTK_w              , "w"},
   {MTK_x              , "x"},
   {MTK_y              , "y"},
   {MTK_z              , "z"},
   
   {MTK_A              , "A"},
   {MTK_B              , "B"},
   {MTK_C              , "C"},
   {MTK_D              , "D"},
   {MTK_E              , "E"},
   {MTK_F              , "F"},
   {MTK_G              , "G"},
   {MTK_H              , "H"},
   {MTK_I              , "I"},
   {MTK_J              , "J"},
   {MTK_K              , "K"},
   {MTK_L              , "L"},
   {MTK_M              , "M"},
   {MTK_N              , "N"},
   {MTK_O              , "O"},
   {MTK_P              , "P"},
   {MTK_Q              , "Q"},
   {MTK_R              , "R"},
   {MTK_S              , "S"},
   {MTK_T              , "T"},
   {MTK_U              , "U"},
   {MTK_V              , "V"},
   {MTK_W              , "W"},
   {MTK_X              , "X"},
   {MTK_Y              , "Y"},
   {MTK_Z              , "Z"},
#endif // #ifdef __sgi

   {MTK_EQUAL          , "="},

#ifdef __sgi
// X-Windows supports shifted keyboard characters, while MS Windows
// is unshifted only.  In general mapping between shifted and unshifted
// characters is keyboard-dependent and must be discovered at run-time.

// Upper-Case Punctuation
   {MTK_ASCIITILDE     , "~"},   // Shift+`
   {MTK_EXCLAMATION    , "!"},   // Shift+1
   {MTK_AT             , "@"},   // Shift+2
   {MTK_NUMBERSIGN     , "#"},   // Shift+3
   {MTK_DOLLAR         , "$"},   // Shift+4
   {MTK_PERCENT        , "%"},   // Shift+5
   {MTK_ASCIICIRCUM    , "^"},   // Shift+6
   {MTK_AMPERSAND      , "&"},   // Shift+7
   {MTK_ASTERISK       , "*"},   // Shift+8
   {MTK_PARENLEFT      , "("},   // Shift+9
   {MTK_PARENRIGHT     , ")"},   // Shift+0
   {MTK_UNDERSCORE     , "_"},   // Shift+-
   {MTK_PLUS           , "+"},   // Shift++
   {MTK_BRACELEFT      , "{"},   // Shift+[
   {MTK_BRACERIGHT     , "}"},   // Shift+]
   {MTK_BAR            , "|"},   // Shift+\ 
   {MTK_COLON          , ":"},   // Shift+;
   {MTK_QUOTEDBL       , "\""},  // Shift+'
   {MTK_LESS           , "<"},   // Shift+,
   {MTK_GREATER        , ">"},   // Shift+.
   {MTK_QUESTION       , "?"},   // Shift+/

#endif // #ifdef __sgi

