// ToolManager.h: interface for the CToolManager class.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolManager.h,v 1.41.4.12 2009/07/15 15:17:10 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef ToolManagerH
#define ToolManagerH

#include "IniFile.h"   // bring in lots of core-code stuff
#include "ToolObject.h"
#include "CPlugin.h"  // This may be bad Added JAM
#include "SafeClasses.h"  // For CRingBuffer

#include <string>
#include <vector>
using std::string;
using std::vector;

#ifdef __BORLANDC__
#include <vcl.h>
#endif // #ifdef __BORLANDC_)

#ifdef __sgi
#include <Xm/Xm.h>
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CAutotoolCommand;
class CAutotoolStatus;
class CBufferPool;
class CImageFormat;
class CMediaReaderIOConfig;
class CMediaWriterIOConfig;
class CPDL;
class CPDLEntry;
class CPDLRenderInterface;
class CPluginList;
class CProvisional;
class CToolEdge;
class CToolFrameCache;
class CToolNode;
class CToolObject;
class CToolParameters;
class CToolSetup;
class CToolSystemInterface;
class CUserInput;

//////////////////////////////////////////////////////////////////////

// Maximum number of tools that can be loaded at once
// (this is an arbitrary number, chosen to allow a fixed size array
//  to represent the tool list)
#define TOOL_MGR_MAX_TOOL_COUNT  256

//////////////////////////////////////////////////////////////////////
// --------------------------------------------------------------------------
// Structures to define IUPE (Internal UPE, where UPE = Ultimate PDL Event,
// PDL = Process Description List)

struct SIUPETool
{
   string toolName;
   CToolIOConfig *toolIOConfig;
   CToolFrameRange *toolFrameRange;
   CToolParameters *toolParameters;
};

typedef vector<SIUPETool> IUPEToolList;

struct SIUPEEdge
{
   int srcToolListIndex;
   int srcOutPortIndex;
   int dstToolListIndex;
   int dstInPortIndex;
};

typedef vector<SIUPEEdge> IUPEEdgeList;

//////////////////////////////////////////////////////////////////////
// PDL Rendering Statistics

class CPDLRenderingStats
{
public:
   CPDLRenderingStats();

   void Init();

   void IncrementEntryNumber();
   void IncrementProcessedOkCount();
   void IncrementCheckFailedCount();
   void IncrementProcessedFailedCount();
   void IncrementSkippedCount();

   int GetEntryNumber() const;
   int GetProcessedOkCount() const;
   int GetCheckFailedCount() const;
   int GetProcessedFailedCount() const;
   int GetSkippedCount() const;

   bool IsModified() const;

private:
   int entryNumber;
   int processedOkCount;
   int checkFailedCount;
   int processedFailedCount;
   int skippedCount;
   mutable bool hasChanged;
};

//////////////////////////////////////////////////////////////////////

class CToolManager
{
public:
   // Public Data Types
   // Function provided by application to create the internal tools
   typedef CToolObject* (*ToolCreateFunc)(const string& newToolName);

   // Function provided by application to create concrete instance
   // of CToolSystemInterface abstract base class
   typedef CToolSystemInterface* (*CreateToolSystemInterfaceFunc)();

   // Structure used by application code to create a statically initialized
   // list of internal tool names and their respective factory methods
   struct SInternalToolTable
   {
      const char *toolName;                // Internal tool's name
      ToolCreateFunc makeToolFunc;   // Function to create tool instance
   };

public:
   CToolManager();
   virtual ~CToolManager();

   // Show and hide a particular tool
   int showTool(int toolIndex);
   int hideTool(int toolIndex);
   bool hideToolQuery(int toolIndex);     // okay to hide the tool?

   // Force a tool to be created and initialized
   int initTool(int toolIndex);

   int getToolCount() const;
   string getToolName(int toolIndex) const;
   MTI_UINT32 **getToolFeature(int toolIndex) const;
   int findTool(const string& toolName) const;
   int findTool(const CToolObject *toolObj) const;
   bool isPluginTool(int toolIndex) const;
   CPluginStateWrapper currentPluginState(int toolIndex, bool noCache=false) const;  // Added JAM for current state

   int CheckTool(const string &toolName, string &message);

   int loadPlugins(const char* pluginPath, void *appPtr=NULL);
   int unloadPlugins();
   int setActiveTool(int toolIndex);
   int setDefaultTool(int toolIndex);
   int addFirstDibsTool(int toolIndex);
   int loadInternalTools(const SInternalToolTable *internalToolTable,
                         int toolCount);

   void SetProvisional(CProvisional *newProvisional);

   void MakeSimpleIUPE(const string& toolName, CToolIOConfig *toolIOConfig,
                       IUPEToolList& toolList, IUPEEdgeList& edgeList,
                       CMediaReaderIOConfig &mediaReaderIOConfig,
                       CMediaWriterIOConfig &mediaWriterIOConfig);
   void MakePreloadIUPE(const string& toolName,
                        CToolIOConfig *toolIOConfig,
                        IUPEToolList& toolList,
                        IUPEEdgeList& edgeList,
                        CMediaReaderIOConfig &mediaReaderIOConfig);

   int MakeSimpleToolSetup(const string& toolName,
                           EToolSetupType newToolSetupType,
                           CToolIOConfig *toolIOConfig);
   int DestroyToolSetup(int toolSetupHandle);
   int SetActiveToolSetup(int newToolSetupHandle);
   int GetActiveToolSetupHandle();
   int SetToolFrameRange(CToolFrameRange *toolFrameRange);
   int SetToolParameters(CToolParameters *toolParameters);
   int RunActiveToolSetup();
   int StopActiveToolSetup();
   int PauseActiveToolSetup();
   int EmergencyStopActiveToolSetup();
   EAutotoolStatus GetActiveToolSetupStatus();
   EAutotoolStatus GetToolSetupStatus(int toolSetupHandle);

   int InitializePDLRendering(CPDLRenderInterface *appInterface,
                              CPDL *newPDL);
   int StartPDLRendering(bool newRenderFlag, bool newStopOnErrorFlag);
   int StopPDLRendering();
   EToolControlState GetPDLRenderingControlState();
   const CPDLRenderingStats* GetPDLRenderingStats();
   int GetPDLEntryCount();
   bool IsPDLRendering();
   void SetPDLRenderingStatus(EPDLRenderingStatus newRenderingStatus);
   EPDLRenderingStatus GetPDLRenderingStatus();
   void SetReportedPDLRenderingError(int errorCode, const string &msg);
   int GetReportedPDLRenderingErrorCode();
   string GetReportedPDLRenderingErrorMsg();

   int GetProvisionalLastFrameIndex();

	bool IsToolProcessing(bool orPaused = false);
	bool CheckToolProcessing();
	bool IsProvisionalPending();
	bool IsProvisionalUnresolved();
	bool CheckProvisionalUnresolved();
   bool CheckProvisionalUnresolvedAndToolProcessing();

   bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   void NotifyGOVDone();
   void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                              const RECT &rect, const POINT *lasso);
   void NotifyRightClicked();
   bool NotifyTrackingStarting();
   void NotifyTrackingDone(int errorCode);
   void NotifyTrackingCleared();

	bool NotifyStartROIMode();
	bool NotifyUserDrewROI();
	void NotifyEndROIMode();
   void NotifyMaskChanged(const string &newMaskAsString);
   void NotifyMaskVisibilityChanged();

	void NotifyStartViewOnlyMode();
	void NotifyEndViewOnlyMode();
   bool IsInViewOnlyMode();

   void Heartbeat();
   bool updateStatusBarNow;      // flag for Heartbeat action
   void MonitorToolProcessing();
   void MonitorPDLRendering();
   void WaitForToolProcessing(bool processMessages = false);
   void WaitForToolSetupDestruction();

   CBufferPool* GetBufferAllocator(int bytesPerBuffer, int bufferCount,
                                   double invisibleFieldsPerFrame);
   int ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                              double invisibleFieldsPerFrame);
   void DeleteBufferAllocators();

   // Mutator to set application-provided function pointer used to create
   // concrete instances of applications subtype of CToolSystemInterface
   // abstract base class when the Tool Manager creates a new Tool object
   // This function must be called with applications function pointer
   // prior to creating any internal tools or loading plugins
   void setCreateToolSystemInterfaceFunc(
                                 CreateToolSystemInterfaceFunc newCreateFunc);

   CToolObject *GetTool(int index);
   int getActiveToolIndex(void) {return activeToolIndex;}

   // Tool Manager's user input event handlers for keyboard and mouse input.
   // Called by application's user input event handlers
#ifdef __BORLANDC__
   int onKeyDown(WORD &Key, TShiftState Shift);
   int onKeyUp(WORD &Key, TShiftState Shift);
   int onMouseDown(TMouseButton Button, TShiftState Shift, int X, int Y,
                   int imageX, int imageY);
   int onMouseDoubleClick(TMouseButton Button, TShiftState Shift, int X, int Y,
                   int imageX, int imageY);
   int onMouseUp(TMouseButton Button, TShiftState Shift, int X, int Y,
                 int imageX, int imageY);
   int onMouseMove(TShiftState Shift, int X, int Y, int imageX, int imageY);
#endif // #ifdef __BORLANDC__
#ifdef __sgi
   int onKeyDown(XEvent *event);
   int onKeyUp(XEvent *event);
   int onMouseDown(XEvent *event, int imageX, int imageY);
   int onMouseUp(XEvent *event, int imageX, int imageY);
   int onMouseMove(XEvent *event,int imageX, int imageY);
#endif // #ifdef __sgi

   // onNewMarks is called after markIn and/or markOut is changed
   int onNewMarks();

   // onChangingClip is called before a new clip is opened
   int onChangingClip();

   // onChangingClip is called before a new clip is opened
   int onDeletingOpenedClip();

   // onNewClip is called after a new clip is opened
   int onNewClip();

   // onChangingFraming is called before the framing (video- & film-frames) is changed
   int onChangingFraming();

   //  onNewFraming is called after the framing (video- & film-frames) is changed
   int onNewFraming();

   // onRedraw is called after a frame is drawn, allows tool to specify overlays!
   int onRedraw(int frameIndex);

   // onPreviewHackDraw is called in "preview hack" mode
   // Allows tool to modify the frame bits using internal format
   int onPreviewHackDraw(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes);

   // onPreviewDisplayBufferDraw is called in "preview display buffer" mode - allows tool to modify the frame display bits!
   int onPreviewDisplayBufferDraw(int frameIndex, int width, int height, MTI_UINT32 *pixels);

   // onTopPanelRedraw is called when the main window's top panel needs to be drawn
   int onTopPanelRedraw();

   // These implement mouse left click'n'drag notifications in the top panel.
   int onTopPanelMouseDown(int x, int y);
   int onTopPanelMouseDrag(int x, int y);
   int onTopPanelMouseUp(int x, int y);

   // onTimelineVisibleFrameRangeChange is called after the blue area of the timeline track bar is changed
   int onTimelineVisibleFrameRangeChange();

   // onPlayerStart
   int onPlayerStart();

   // onPlayerStop
   int onPlayerStop();

   // onPlaybackFilterChange
   int onPlaybackFilterChange(int);

   int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);

   int onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message);
   int onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag, const std::pair<int, int> &frameRange);
   int onClipWasDeleted(const string &deletedClipName);
   int onSwitchSubtool();

   // PDL Rendering Interface
   int onCapturePDLEntry(CPDLEntry &pdlEntry);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);
   int onPDLExecutionComplete();

   void GetLastMousePositionWindow(int &x, int &y);
   void GetLastMousePositionImage(int &x, int &y);
   
   void SetMouseOrientation(int type);

   CToolFrameCache *GetToolFrameCache();

   // Major hack to allow PDL window to give focus to the main window.
   void SetMainWindowFocus();

private:
   // Private Data Types
   struct SToolListEntry            // Structure that describes a single tool
   {
      bool isToolInitialized;       // True if tool has already been initialized
      string toolName;              // Name of tool
      int pluginListIndex;          // Tool's plugin referenced by index
                                    // into pluginList.  If -1, then tool
                                    // is internal rather than being from
                                    // a plugin.
      ToolCreateFunc makeToolFunc;  // Function to create instance of an
                                    // internal tool
      CToolObject *toolObject;      // Pointer to tool's CToolObject subtype
                                    // NULL until tool has been initialized
      MTI_UINT32 **toolFeature;     // Encrypted feature name table; e.g., [CORRECT-DRS, NULL]
   };

   typedef vector<SToolListEntry*> ToolList; // Vector of pointers to tool
                                             // list entry structures

   enum EToolSetupDestructionState
   {
      TOOL_SETUP_DESTRUCTION_NEW_ENTRY,
      TOOL_SETUP_DESTRUCTION_WAITING_FOR_THREAD_END,
      TOOL_SETUP_DESTRUCTION_DONE
   };

   struct SToolSetupDestructionState
   {
      CToolSetup *toolSetup;
      EToolSetupDestructionState currentState;
   };

   struct SBufferAllocator
   {
      CBufferPool *allocator;
      int totalFrames;
      int framesAvailable;
      int framesUsed;
   };


private:
   // Private Member Functions
   CToolObject* getActiveToolObject();
   CToolObject* getDefaultToolObject();
   CToolObject* getFirstDibsToolObject(unsigned int firstDibsToolListIndex);
   int deleteToolList();
   int shutdownTool(SToolListEntry* toolListEntry);
   int destroyTool(SToolListEntry* toolListEntry);
   int makeTool(int toolIndex);

   // Internal Tool Event Handlers
   int onMouseMove(CUserInput &userInput);
   int onUserInput(CUserInput &userInput);

   int AddToolSetup(CToolSetup *newToolSetup);
   void RemoveToolSetup(int handle);
   CToolSetup* GetToolSetup(int handle);
   void MonitorToolSetupDestruction();

   int RenderPDLEntry(CPDLEntry &pdlEntry);

private:
   static ToolList toolList;           // List of internal and plugin tools

   // Private Member Data
   static bool isInitialized;          // Set to true after Tool Manager has
                                       // been initialized

   static int activeToolIndex;         // Index into toolList of currently
                                       // active tool
   static int defaultToolIndex;        // Index into toolList of default tool

   // List of "first dibs" tools
#define MAX_FIRST_DIBS_TOOLS_COUNT 5
   static vector<int> firstDibsToolIndexList;
   static int currentFirstDibsToolIndex;

   static CPluginList *pluginList;     // List of plugins that have been loaded

   // Pointer to application-provided function to create concrete instances
   // of applications subtype of CToolSystemInterface abstract base class
   static CreateToolSystemInterfaceFunc createToolSystemInterface;

   static bool newToolSetupCommandRequested;
   static EAutotoolCommand requestedToolSetupCommand;

   static bool newToolProcessingCommandRequested;
   static EAutotoolCommand requestedToolProcessingCommand;

   static vector<CToolSetup*> toolSetupList;
   static CToolSetup* activeToolSetup;

   static vector<SToolSetupDestructionState> toolSetupDestructionQueue;

   // PDL Rendering
   static EToolControlState pdlRenderingControlState;
   static EPDLRenderingStatus pdlRenderingStatus;
   static CPDLRenderInterface* pdlRenderingAppInterface;
   static CPDL* currentRenderingPDL;
   static CPDLEntry *currentRenderingPDLEntry;
   static CPDLRenderingStats pdlRenderingStats;
   static bool newPDLRenderingCommandRequested;
   static EAutotoolCommand requestedPDLRenderingCommand;
   static bool pdlRenderFlag;
   static bool stopOnPDLRenderErrorFlag;
   static int reportedPDLRenderingErrorCode;
   static string reportedPDLRenderingErrorMsg;

   static CProvisional *provisional;

   static vector<SBufferAllocator> bufferAllocatorList;

   static int lastMousePositionWindowX;
   static int lastMousePositionWindowY;
   static int lastMousePositionImageX;
   static int lastMousePositionImageY;

   
   int mouseOrientation;

   // Tool state cache hack... this should be a map!
#define MAX_CACHED_PLUGIN_STATE_COUNT 20
   // BORLAND BUG - Breaks autobuilds!!
   //static CPluginState pluginToolStateCache[MAX_CACHED_PLUGIN_STATE_COUNT];
   static CPluginStateWrapper *pluginToolStateCache;

   static CToolFrameCache *toolFrameCache;

   static bool inHeartbeat;  // hack flag to prevent heartbeat re-entry damage

};

//////////////////////////////////////////////////////////////////////


class CToolSetup
{
public:
   CToolSetup(EToolSetupType newToolSetupType);
   virtual ~CToolSetup();

   int CreateFromIUPE(IUPEToolList &newToolList, IUPEEdgeList &newEdgeList);

   int TopologicalSort();

   int GetNodeCount() const;
   int GetEdgeCount() const;

   int PutToolCommand(CAutotoolCommand &command);
   void SetEmergencyStop();

   // Tool Status
   CAutotoolStatus MonitorToolSetupProcessing(EAutotoolCommand newToolCommand);
   void ClearToolStatus();
   int MonitorToolStatus();
   EAutotoolStatus GetMasterProcessingStatus();
   bool IsToolStatusAllStopped();
   bool IsToolStatusAllPaused();
   bool IsToolStatusAllPausedOrStopped();
   bool IsToolStatusAllThreadsExited();
   bool IsToolStatusAnyEndOfMaterial();
   bool IsToolStatusAnyError();
   int GetToolStatusFirstErrorCode();

   int SetIOConfig(IUPEToolList &toolList);
   int SetFrameRange(CToolFrameRange *frameRangeArray[]);
   int SetParameters(CToolParameters *toolParamArray[]);

   EToolSetupType GetToolSetupType();
   int RegisterWithSystem();

   int AssignAllBufferAllocators();
   void ReleaseAllBufferAllocators();

   int GetHandle() const;
   void SetHandle(int newHandle);

private:
   typedef vector<CToolNode*> ToolNodeList;
   typedef vector<CToolEdge*> ToolEdgeList;

private:
   void AddEdge(CToolEdge *toolEdge);
   void AddNode(CToolNode *toolNode);
   int DepthFirstSearch();
   int DFS_Visit(CToolNode *node);

   void DeleteNodeList();
   void DeleteEdgeList();

private:
   EToolSetupType toolSetupType;

   ToolNodeList nodeList;
   ToolEdgeList edgeList;

   int timestamp;
   list<int> topologicalSort;

   int handle;    // Handle in the Tool Manager's Tool Setup List
   EToolControlState masterToolControlState;
   EAutotoolStatus masterToolStatus;

   bool emergencyStopFlag;    // normally false, set to true to force
                              // an "emergency" stop of processing



};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLMANAGER_H)






