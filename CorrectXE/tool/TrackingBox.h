//---------------------------------------------------------------------------

#ifndef TrackingBoxH
#define TrackingBoxH

//  A TrackPoint is DPOINT with a frame number and an identifier
//
//  This is a simple base class for a collection of (x,y) points.  The points
// access is not very fast but is designed for a few hundred points per frame

#include "IniFile.h"
#include "MTIstringstream.h"
#include "SafeClasses.h"
#include <map>
#include <unordered_map>
#include <list>
#include <string>

#define INVALID_TRACKING_BOX_TAG (-1)

using std::map;
using std::unordered_map;
using std::string;

class CPDLElement;

#define TRACE_ME do { char dbstring[10001]; sprintf(dbstring, "%.10000s", toString().c_str()); TRACE_0(errout << dbstring); } while(false)

//==============================================================================

struct FPOINT
{
   float x;
   float y;

   FPOINT(): x(0), y(0) {};
   FPOINT(float xarg, float yarg): x(xarg), y(yarg) {};
   FPOINT(const FPOINT &point) : x(point.x), y (point.y) {};

   FPOINT operator+(const FPOINT &rhs) const { return FPOINT(x + rhs.x, y + rhs.y); };
   FPOINT operator+=(const FPOINT &rhs) { x += rhs.x; y += rhs.y; return *this; };
   FPOINT operator-(const FPOINT &rhs) const { return FPOINT(x - rhs.x, y - rhs.y); };
   FPOINT operator-=(const FPOINT &rhs) { x -= rhs.x; y -= rhs.y; return *this; };

   // We declare two FPOINTS to be equal if they are within 1/1000 of a pixel of each other
   // in both X and Y directions!
   bool operator==(const FPOINT &rhs) const { return (std::fabs(x - rhs.x) < 0.001F) && (std::fabs(y - rhs.y) < 0.001F); };
   bool operator!=(const FPOINT &rhs) const { return !(*this == rhs); };

   operator POINT() { POINT ip = {int(x), int(y)}; return ip; };
   operator DPOINT() { DPOINT dp = {x, y}; return dp; };

   string toString() const
   {
      MTIostringstream os;
      os << "(" << x << "," << y << ")";
      return os.str();
   };
};

//typedef map<int, FPOINT> FrameToTrackPointMap;

class TrackPointList : public  vector<FPOINT>
{
public:

   TrackPointList(size_type numberOfElements)
   : vector<FPOINT>(numberOfElements)
   {
   }

   string toString() const;
};

struct TrackingPosition
{
   FPOINT in;
   FPOINT out;
   FPOINT virtualTrackOffset;

   TrackingPosition(const FPOINT &initialPosition)
   : in(initialPosition)
   , out(initialPosition)
   , virtualTrackOffset(FPOINT(0, 0))
   {};

   string toString() const
   {
      MTIostringstream os;
      os << "{in=" << in.toString() << ", out=" << out.toString() << ", vto=" << virtualTrackOffset.toString() << "}";
      return os.str();
   };
};

class TrackingBox
{
   int _startFrame;
   vector<TrackingPosition> _track;

public:

   TrackingBox();
   TrackingBox(int startFrame, FPOINT locationAtStartFrame);

   bool isValidAtFrame(int frame) const;

   FPOINT operator[](int frame) const;

   FPOINT getTrackPointAtFrame(int frame) const;

   void setTrackPointAtFrame(int frame, FPOINT point);

   void addTrackPoint(FPOINT newPoint);

   void appendTrackingPosition(TrackingPosition position);

   void movePositionAtFrame(int frame, FPOINT offset);

   void neutralizePositionAtFrame(int frame);

   void setStartFrame(int frame);

   int getStartFrame() const;

   TrackingPosition getTrackingPositionAtFrame(int frame) const;

   void trimFramesBefore(int frame);

   void trimFramesAfter(int frame);

   int getLastValidFrame() const;

   int getNumberOfValidFrames() const;

   string toString() const;
};

enum TrackingStatus
{
   TrackingStatusIsUnknown,
   NeedToTrack,
   TrackingIsComplete
};

typedef vector<int> TagList;

class TrackingBoxSet
{
   unordered_map<int, TrackingBox> _trackingBoxes;
   TagList _trackingBoxTags;
	int _inFrame = -1;
   int _outFrame = -1;
   RECT _boundingBox = {-1, -1, -1, -1};
   bool _useBoundingBox = false;
   int _trackingBoxRadius = -1;
   int _searchBoxRadius = -1;

   bool _degrain = false;
   bool _applyLut = false;
   bool _autoContrast = false;
   int _channel = -1; // -1 == Y

   int _nextTag = 1;

public:
   TrackingBoxSet();

   int getInFrame() const;

   int getOutFrame() const;

//	void setInFrame(int frame);
//
//	void setOutFrame(int frame);
//
	void setInAndOutFrames(int inFrame, int outFrame);

   bool isFrameInRange(int frame) const;

   int addTrackingBox(int startFrame, FPOINT initialXY);

   int addTrackingBox(const TrackingBox &newTrackingBox);

   void moveTrackingBoxAtFrame(int trackingBoxTag, int frame, FPOINT offset);

   void removeTrackingBox(int trackingBoxTag);

   void removeAllTrackingBoxes();

   bool isValidTrackingBoxTag(int trackingBoxTag) const;

   const TagList &getTrackingBoxTags() const;

   TrackingBox &getTrackingBoxByTag(int trackingBoxTag);

   TrackingBox &getTrackingBoxByIndex(int index);

   const TrackingBox &getTrackingBoxByTag(int trackingBoxTag) const;

   TrackPointList getAllTrackPointsAtFrame(int frame) const;

   int getNumberOfTrackingBoxes() const;

   void invalidateTracking();

   void clear();

   int size() const;

	int getFirstUntrackedFrame() const;

	bool areAllTrackingBoxesFullyTracked() const;

   TrackingStatus getTrackingStatus() const;

   void setBoundingBox(const RECT &bb);
   RECT getBoundingBox(void) const;
   void setUseBoundingBox(const bool &flag);
   bool getUseBoundingBox(void) const;

   void setTrackingBoxRadius(int newRadius);
   int getTrackingBoxRadius() const;
   void setSearchBoxRadius(int newRadius);
   int getSearchBoxRadius() const;

   void setDegrainFlag(bool flag) { _degrain = flag; }
   bool getDegrainFlag() const { return _degrain; }
   void setApplyLutFlag(bool flag) { _applyLut = flag; }
   bool getApplyLutFlag() const { return _applyLut; }
   void setAutoContrastFlag(bool flag) { _autoContrast = flag; }
   bool getAutoContrastFlag() const { return _autoContrast; }
   int  getChannel() const { return _channel; }
   void setChannel(int channel) { _channel = channel; }

   void ReadPDLEntry(CPDLElement *pdlTrackingArray);
   void AddToPDLEntry(CPDLElement &parent);

	void checkConsistency() const;
	string toString() const;
};

std::ostream &operator<<(std::ostream &theStream, const FPOINT &point);
std::ostream &operator<<(std::ostream &theStream, const TrackPointList &pointList);
std::ostream &operator<<(std::ostream &theStream, const TrackingBox &trackingBox);
std::ostream &operator<<(std::ostream &theStream, const TrackingBoxSet &ta);

std::istream& operator>>(std::istream &theStream, FPOINT &ta);
std::istream& operator>>(std::istream &theStream, TrackingBox &ta);
std::istream& operator>>(std::istream &theStream, TrackingBoxSet &ta);

//---------------------------------------------------------------------------
#endif
