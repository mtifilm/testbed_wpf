// ToolEvent.h: interface for the CToolEvent class.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolEvent.h,v 1.1.1.1 2001/07/25 14:16:45 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOLEVENT_H)
#define TOOLEVENT_H

//////////////////////////////////////////////////////////////////////

enum EToolEventType
{
   TOOL_EVENT_TYPE_INVALID = 0,
   TOOL_EVENT_TYPE_KEY_DOWN,
   TOOL_EVENT_TYPE_KEY_UP,
   TOOL_EVENT_TYPE_MOUSE_BUTTON_DOWN,
   TOOL_EVENT_TYPE_MOUSE_BUTTON_UP,
   TOOL_EVENT_TYPE_POINTING_DEVICE_MOTION,
   TOOL_EVENT_TYPE_IMAGE_REDRAW,
   TOOL_EVENT_TYPE_NEW_INPUT_FRAME,
   TOOL_EVENT_TYPE_TOOL_COMMAND
};

//////////////////////////////////////////////////////////////////////

class CToolEvent  
{
public:
   CToolEvent();
   virtual ~CToolEvent();

   EToolEventType getEventType();
   void setEventType(EToolEventType newEventType);

private:
   EToolEventType eventType;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLEVENT_H)
