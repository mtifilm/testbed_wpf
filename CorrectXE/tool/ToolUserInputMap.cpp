// ToolUserInputMap.cpp: implementation of the CToolUserInputMap class.
//
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolUserInputMap.cpp,v 1.2.4.1 2008/11/19 13:58:48 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolUserInputMap.h"
#include <string.h>
#include "MTIKeyDef.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolUserInputMap::CToolUserInputMap()
: mapHead(0), mapTail(0)
{

}

CToolUserInputMap::~CToolUserInputMap()
{
   // Keep CodeGuard happy by cleaning up
   CToolUserInputMapEntry *mapEntry;

   for (mapEntry = mapHead;
		mapEntry != (CToolUserInputMapEntry *) 0;
        mapEntry = mapHead)
      {
      mapHead = mapEntry->forwardLink;
      delete mapEntry;
      }
}

//////////////////////////////////////////////////////////////////////
// User Input Map Table Search Functions 
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    findCommand
//
// Description: Get the Command Number that matches the given User Input,
//              given as the user input action, key code and shift state
//
// Arguments:   CUserInput &targetUserInput
//              int targetKey
//              int targetShift    
//
// Returns:     Command Number if matching action/key/shift was found.
//              -1 if search failed.        
//
// ===================================================================
int CToolUserInputMap::findCommand(CUserInput &targetUserInput)
{
   CToolUserInputMapEntry *mapEntry;
   mapEntry = findEntry(targetUserInput);

   if (mapEntry)
      {
      // User Input was found in the User Input Map Table, so return
      // the Command Number in the table entry
      return (mapEntry->commandNumber);
      }
   else
      {
      // User Input match was not found in the User Input Map Table, so return -1 
      // to indicate search failure.
      return (-1);
      }
}

//////////////////////////////////////////////////////////////////////
// User Input Map Table Maintenance Functions 
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    addMapEntry
//
// Description: Create a new entry or update an existing entry in the
//              User Input Map Table.
//
// Arguments:   CUserInput &newUserInput
//              int newCommandNumber    
//
// Returns:     None.        
//
// ===================================================================
void CToolUserInputMap::addMapEntry(CUserInput &newUserInput, 
                                   int newCommandNumber)
{
   CToolUserInputMapEntry *mapEntry;
   mapEntry = findEntry(newUserInput);

   if (mapEntry)
      {
      // User Input was found in the User Input Map Table, so replace
      // the existing Command Number
      mapEntry->commandNumber = newCommandNumber;
      }
   else
      {
      // User Input match was not found in the User Input Map Table, so create
      // a new entry and add it to the table
      mapEntry = new CToolUserInputMapEntry(newUserInput, newCommandNumber); 

      // Add new entry into linked list
      mapEntry->forwardLink = 0;
      if (mapHead == 0)
         {
         // User Input Map Table is empty, so start here
         mapHead = mapEntry;
         }
      else
         {
         // User Input Map Table is not empty, so add entry to tail
         mapTail->forwardLink = mapEntry;
         }

      // Update the tail pointer
      mapTail = mapEntry;
      }
}

// ===================================================================
//
// Function:    removeMapEntry
//
// Description: Create a new entry or update an existing entry in the
//              User Input Map Table.
//
// Arguments:   CUserInput &targetUserInput
//
// Returns:     None.        
//
// ===================================================================
void CToolUserInputMap::removeMapEntry(CUserInput &targetUserInput)
{
   CToolUserInputMapEntry *mapEntry;
   mapEntry = findEntry(targetUserInput);

   if (mapEntry)
      {
      // User Input was found in the User Input Map Table, so remove it
      // from the linked list

      if (mapEntry == mapHead)
         {
         // Entry to remove is the first in the linked list
         mapHead = mapEntry->forwardLink;
         if (mapHead == 0)
            mapTail = 0;
         }
      else
         {
         // Entry to remove is not the first in the list
         CToolUserInputMapEntry *previousEntry;
         
         // Find the entry in the map that is before the entry to
         // be removed
         for (previousEntry = mapHead; 
              previousEntry != mapTail; 
              previousEntry = previousEntry->forwardLink)
            {
            if (mapEntry == previousEntry->forwardLink)
               {
               // Found the previous entry, so update its forward link
               previousEntry->forwardLink = mapEntry->forwardLink;
               if (mapTail == mapEntry)
                  {
                  // Entry that is being removed is the last entry,
                  // so tail has to point to the previous entry
                  mapTail = previousEntry;
                  }
               }
            }
         }

      // Delete the entry
      delete mapEntry;
      }

}


//////////////////////////////////////////////////////////////////////
//  Private Search Functions
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    findEntry
//
// Description: Get the index into the User Input Map Table that matches the 
//              User Input, given as the user input action, key code and 
//              shift state.
//
// Arguments:   CUserInput &targetUserInput
//
// Returns:     Table entry pointer if matching action/key/shift was found.
//              NULL pointer if search failed.        
//
// ===================================================================
CToolUserInputMapEntry *CToolUserInputMap::findEntry(CUserInput &targetUserInput)
{
   if (mapHead == 0)
      {
      // User Input Map Table is empty, so return NULL pointer to indicate
      // search failure
      return (0);
      }

   // Iterate through User Input Map table to search for 
   // match on target User Input
   CToolUserInputMapEntry *mapEntry;
   for (mapEntry = mapHead; mapEntry != 0; mapEntry = mapEntry->forwardLink)
      {
      if (mapEntry->userInput == targetUserInput)
         {
         // Found matching user input, so return index in table
         return (mapEntry);
         }
      }

   // User Input match was not found in the User Input Map Table, so return 
   // NULL pointer to indicate search failure.
   return (0);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction for CToolUserInputMapEntry
//////////////////////////////////////////////////////////////////////

CToolUserInputMapEntry::CToolUserInputMapEntry(
                                  const CToolUserInputMapEntry &mapEntry)
: userInput(mapEntry.userInput), 
  commandNumber(mapEntry.commandNumber), forwardLink(0)
{

}

CToolUserInputMapEntry::CToolUserInputMapEntry(CUserInput &newUserInput,
                                               int newCommandNumber)
: userInput(newUserInput),
  commandNumber(newCommandNumber), forwardLink(0)
{

}

CToolUserInputMapEntry::~CToolUserInputMapEntry()
{

}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction for CUserInputConfiguration
//////////////////////////////////////////////////////////////////////

CUserInputConfiguration::CUserInputConfiguration(int newConfigItemCount,
                                              SConfigItem *newConfigItemTable)
: configItemCount(newConfigItemCount),
  configItemTable(newConfigItemTable)
{

}

CUserInputConfiguration::~CUserInputConfiguration()
{

}

int CUserInputConfiguration::getConfigItemCount() const
{
   return configItemCount;
}


const CUserInputConfiguration::SConfigItem *CUserInputConfiguration::
getConfigItem(int index) const
{
   if (index < 0 || index >= configItemCount)
      return 0;

   return configItemTable + index;
}

