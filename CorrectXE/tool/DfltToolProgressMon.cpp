#include "DfltToolProgressMon.h"
#include "IniFile.h"    // for TRACE_N()
#include "HRTimer.h"
#include "MTIstringstream.h"
#include "ThreadLocker.h"

#include <iostream>
//---------------------------------------------------------------------------

CDefaultToolProgressMonitor::CDefaultToolProgressMonitor(void)
: _FramesTotal(0)
, _BumpsPerFrame(1)
, _FramesSoFar(0)
, _Bumps(0)
, _ElapsedTimeInSeconds(0)
, _TimePerFrameInSeconds(0)
{
   _HRTimer = new CHRTimer;
   _ThreadLock = new CThreadLock;
}
//----------------------------------------------------------------

CDefaultToolProgressMonitor::~CDefaultToolProgressMonitor(void)
{
   delete _HRTimer;
   delete _ThreadLock;
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::SetIdle(bool setMessage)
{
   CAutoThreadLocker lock(*_ThreadLock);

	setFrameCountInternal(0, 1, 0, 0.0);
   if (setMessage)
      _StatusMessage = "Idle";

   TRACE_2(errout << "TPM: IDLE");
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::SetStatusMessage(const char *newMessage)
{
   _StatusMessage = newMessage;
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::PushStatusMessage(const char *newMessage)
{
   _StatusMessageStack.push(_StatusMessage);
   SetStatusMessage(newMessage);
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::PopStatusMessage(void)
{
   if (_StatusMessageStack.size() > 0)
   {
      _StatusMessage = _StatusMessageStack.top();
      _StatusMessageStack.pop();
   }
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::Push()
{
   auto data = std::make_tuple(_FramesTotal, _BumpsPerFrame, _FramesSoFar,_Bumps, _ElapsedTimeInSeconds);
   _StateStack.push(data);
}

//----------------------------------------------------------------

void CDefaultToolProgressMonitor::Pop()
{
   if (_StateStack.size() > 0)
   {
      std::tie(_FramesTotal, _BumpsPerFrame, _FramesSoFar,_Bumps, _ElapsedTimeInSeconds) = _StateStack.top();
      _StateStack.pop();
   }
}

//----------------------------------------------------------------

void CDefaultToolProgressMonitor::SetToolMessage(const char *newMessage)
{
   _ToolMessage = newMessage;
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::SetFrameCount(int newFrameCount,
																int newBumpsPerFrame,
																int preElapsedFrames,
																double timePerFrameInSeconds)
{
   CAutoThreadLocker lock(*_ThreadLock);

   setFrameCountInternal(newFrameCount, newBumpsPerFrame, preElapsedFrames, timePerFrameInSeconds);
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::StartProgress(void)
{
   CAutoThreadLocker lock(*_ThreadLock);

   _HRTimer->Start();
   _ElapsedTimeInSeconds = 0.0;
   
   showProgress();
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::BumpProgress(int bumpCount)
{
   CAutoThreadLocker lock(*_ThreadLock);
   int oldFramesSoFar = _FramesSoFar;

   _Bumps += bumpCount;
   if (_Bumps >= _BumpsPerFrame)
   {
      bumpFrameInternal(_Bumps / _BumpsPerFrame);
      _Bumps %= _BumpsPerFrame;
   }

   if (oldFramesSoFar < _FramesSoFar)
      showProgress();
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::StopProgress(bool processedToEnd)
{
   CAutoThreadLocker lock(*_ThreadLock);
   int oldFramesSoFar = _FramesSoFar;

   if (processedToEnd)
   {
      _Bumps = 0;
      bumpFrameInternal(_FramesTotal - _FramesSoFar);
   }

   if (oldFramesSoFar < _FramesSoFar)
      showProgress();

   TRACE_2(errout << "TPM: " << _StatusMessage << " STOP");
}
//---------------------------------------------------------------------------

double CDefaultToolProgressMonitor::GetTimePerFrameInSeconds()
{
	if (_FramesSoFar <= 0)
	{
		return 0.0;
	}

	return _ElapsedTimeInSeconds / _FramesSoFar;
}
//---------------------------------------------------------------------------

void CDefaultToolProgressMonitor::showProgress(void)
{
   if (_FramesTotal > 0)
   {
      string totalFramesAsString ;
      string totalTimeAsString = "-:--";
      string framesSoFarAsString;
      string timeSoFarAsString = "-:--";
      string framesToGoAsString;
      string timeToGoAsString = "-:--";
      string percentDoneAsString = "0";
      string timePerFrameAsString = "-.---";
      MTIostringstream os;

      os << _FramesTotal;
      totalFramesAsString = os.str();
      os.str("");
      os << _FramesSoFar;
      framesSoFarAsString = os.str();
      os.str("");
      os << (_FramesTotal - _FramesSoFar);
      framesToGoAsString = os.str();
      os.str("");

      if (_FramesSoFar > 0)
      {
         timeSoFarAsString = formatDuration(_ElapsedTimeInSeconds);

			double timePerFrame = _ElapsedTimeInSeconds / _FramesSoFar;
         double estimatedTotalTime = timePerFrame * _FramesTotal;
         double fractionDone = _FramesSoFar / _FramesTotal;

         totalTimeAsString = formatDuration(estimatedTotalTime);
         timeToGoAsString = formatDuration(estimatedTotalTime - _ElapsedTimeInSeconds);
         os << (int(fractionDone * 100.0));
         percentDoneAsString = os.str();
         os.str("");
         timePerFrameAsString = formatDuration(timePerFrame,
                                               /* showFraction= */ true);
      }

      os << "TPM: [" << _StatusMessage << "] Total: ";
      os << "Total: " << totalFramesAsString << " / " << totalTimeAsString;
      os << "  So far: " << framesSoFarAsString << " / " << timeSoFarAsString;
      os << " (" << percentDoneAsString << "% / " << timePerFrameAsString << ")";
      os << "  To go: " << framesToGoAsString << " / " << timeToGoAsString;

      TRACE_2(errout << os.str());
   }
}
//----------------------------------------------------------------

void CDefaultToolProgressMonitor::setFrameCountInternal(int newFrameCount,
																		  int newBumpsPerFrame,
																		  int preElapsedFrames,
																		  double timePerFrameInSeconds)
{
	_FramesTotal = newFrameCount;
	_FramesSoFar = preElapsedFrames;
   _BumpsPerFrame = newBumpsPerFrame;
   _Bumps = 0;
	_ElapsedTimeInSeconds = timePerFrameInSeconds * _FramesSoFar;
	_TimePerFrameInSeconds = timePerFrameInSeconds;
}
//---------------------------------------------------------------------------

void CDefaultToolProgressMonitor::bumpFrameInternal(int bumpCount)
{
   _FramesSoFar += bumpCount;
   if (_FramesSoFar > _FramesTotal)
      _FramesSoFar = _FramesTotal;

   _ElapsedTimeInSeconds = _HRTimer->Read() / 1000.0;
   if (_FramesSoFar > 0)
      _TimePerFrameInSeconds = _ElapsedTimeInSeconds / _FramesSoFar;
}
//---------------------------------------------------------------------------

string CDefaultToolProgressMonitor::formatDuration(double durationInSeconds,
                                                   bool showFraction)
{
   string retVal;
   int durationAsInt = int(durationInSeconds + .0005);
   int hours = durationAsInt / 3600;
   int minutes = (durationAsInt % 3600) / 60;
   int seconds = durationAsInt % 60;
   MTIostringstream os;

   if (hours > 0)
   {
      os << hours << ":" << std::setw(2) << std::setfill('0') << minutes
                  << ":" << std::setw(2) << std::setfill('0') << seconds;
   }
   else if (minutes > 0)
   {
      os << minutes << ":" << std::setw(2) << std::setfill('0') << seconds;
   }
   else if (!showFraction)
   {
      os << "0:" << std::setw(2) << std::setfill('0') << seconds;
   }
   else // showFraction
   {
      int milliseconds = int(durationInSeconds * 1000 + 0.5) % 1000;
      if (seconds > 999)
         os << seconds;
      else if (seconds > 99)
         os << seconds << "." << std::setw(1) << ((milliseconds + 50) / 100);
      else if (seconds > 9)
         os << seconds << "."
            << std::setw(2) << std::setfill('0')  << ((milliseconds + 5) / 10);
      else // seconds <= 9
         os << seconds << "."
            << std::setw(3) << std::setfill('0')  << milliseconds;
   }

   retVal = os.str();

   return retVal;
}

//---------------------------------------------------------------------------


