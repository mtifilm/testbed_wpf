// ToolFrameBuffer.cpp implementation of the the CToolFrameBuffer and
//                     CToolFrameQueue classes.
//
// CToolFrameBuffer and CToolFrameQueue are used to pass frame buffers
// to and from tools where the frame buffers are in the internal format
// (16-bit 4:4:4 progressive)
//

/*                                       
$Header: /usr/local/filmroot/tool/ToolObj/ToolFrameBuffer.cpp,v 1.6.2.11 2009/10/26 19:58:53 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolFrameBuffer.h"
#include "ImageFormat3.h"
#include "BufferPool.h"
#include "Clip3VideoTrack.h"
#include "err_imgtool.h"
#include "Extractor.h"
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "SaveRestore.h"

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CToolFrameBuffer Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#ifdef _NOT_IMPLEMENTED_
CToolFrameBuffer::CToolFrameBuffer()
 : frameIndex(-1), errorFlag(-1), endOfMaterial(false),
   visibleFrameBuffer(0), frameImageFormat(0),
   srcInterlaced(false),
   frameBufferAllocator(0),
   originalValues(new CPixelRegionList), newPixels(new CPixelRegionList),
   forceWrite(false),
   dontCache(false)
{
}
#endif

// Constructor that creates a new CToolFrameBuffer given an Image Format
// and CBuffer allocator.
CToolFrameBuffer::CToolFrameBuffer(const CImageFormat *newImageFormat,
                                   CBufferPool *newFrameBufferAllocator)
 : frameIndex(-1), errorFlag(0), endOfMaterial(false),
   visibleFrameBuffer(0),
   frameImageFormat(NULL),
   srcInterlaced(false),
   frameBufferAllocator(newFrameBufferAllocator),
   originalValues(new CPixelRegionList), newPixels(new CPixelRegionList),
   forceWrite(false),
   dontCache(false)
{
   MTIassert((newImageFormat == NULL && frameBufferAllocator == NULL) ||
             (newImageFormat != NULL && frameBufferAllocator != NULL));
   if (newImageFormat != NULL)
   {
       frameImageFormat = new CImageFormat(*newImageFormat);

       MTIassert(frameImageFormat->getTotalFrameWidth() < 10000);
       MTIassert(frameImageFormat->getTotalFrameHeight() < 7500);
       TRACE_3(errout << "NEW tool frame buffer, "
                      << frameImageFormat->getTotalFrameWidth()
                      << " x " << frameImageFormat->getTotalFrameHeight());
   }
   else
   {
       TRACE_3(errout << "NEW tool frame buffer - NULL frameImageFormat!");
   }

}

// Copy Constructor
CToolFrameBuffer::CToolFrameBuffer(const CToolFrameBuffer &srcFrameBuffer)
 : frameIndex(srcFrameBuffer.frameIndex),
   errorFlag(srcFrameBuffer.errorFlag), endOfMaterial(false),
   visibleFrameBuffer(0),
   frameImageFormat(NULL),
   srcInterlaced(srcFrameBuffer.srcInterlaced),
   frameBufferAllocator(srcFrameBuffer.frameBufferAllocator),
   originalValues(new CPixelRegionList), newPixels(new CPixelRegionList),
   forceWrite(srcFrameBuffer.forceWrite),   // Should this be (false)?
   dontCache(srcFrameBuffer.dontCache)      // Should this be (false)?
{
   // WARNING: The following statement will not work right until
   //          proper (deep) assigment operators are built for the Pixel Region
   //          classes.
   *originalValues = *(srcFrameBuffer.originalValues);

   int invisibleFieldCount = srcFrameBuffer.GetInvisibleFieldCount();

   SInvisibleFieldBuffer ifb;
   ifb.fieldBuffer = 0;
   for (int fieldIndex = 0; fieldIndex < invisibleFieldCount; ++fieldIndex)
      {
      const SInvisibleFieldBuffer &srcIfb
                                = srcFrameBuffer.invisibleFieldList[fieldIndex];
      ifb.visibleSiblingFieldIndex = srcIfb.visibleSiblingFieldIndex;
      ifb.srcVisibleFieldCount = srcIfb.srcVisibleFieldCount;
      invisibleFieldList.push_back(ifb);
      }

   MTIassert((srcFrameBuffer.frameImageFormat == NULL &&
                         frameBufferAllocator == NULL) ||
             (srcFrameBuffer.frameImageFormat != NULL &&
                         frameBufferAllocator != NULL));
   if (srcFrameBuffer.frameImageFormat != NULL)
   {
       frameImageFormat = new CImageFormat(*srcFrameBuffer.frameImageFormat);

       MTIassert(frameImageFormat->getTotalFrameWidth() < 10000);
       MTIassert(frameImageFormat->getTotalFrameHeight() < 7500);
       TRACE_3(errout << "COPY tool frame buffer, "
                      << frameImageFormat->getTotalFrameWidth()
                      << " x " << frameImageFormat->getTotalFrameHeight());
   }
   else
   {
       TRACE_3(errout << "COPY tool frame buffer - NULL frameImageFormat!");
   }
}

CToolFrameBuffer::~CToolFrameBuffer()
{
   ClearOriginalValues();
   delete originalValues;

   delete newPixels;

   delete frameImageFormat;
   
   ReleaseAllBuffers();
}

//////////////////////////////////////////////////////////////////////
// Initialization Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    Init
//
// Description: Initialize a CToolFrameBuffer instance based on an
//              existing instance
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
#ifdef _NOT_IMPLEMENTED_
int CToolFrameBuffer::Init(const CToolFrameBuffer &src)
{
   return 0;
}
#endif
//------------------------------------------------------------------------
//
// Function:    Init
//
// Description: Initialize a CTooFrameBuffer instance given an Image Format
//              and CBuffer allocator.  The Image Format must be compatible
//              with CPMP "internal" format, i.e., 16-bit, 4:4:4 progressive.
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
#ifdef _NOT_IMPLEMENTED_
int CToolFrameBuffer::Init(CImageFormat *newImageFormat,
                           CBufferPool *newFrameBufferAllocator,
                           int newInvisibleFieldCount)
{
   return 0;
}
#endif

//------------------------------------------------------------------------
//
// Function:    InitFromClip
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CToolFrameBuffer::InitFromClip(CVideoFrameList& srcFrameList,
                                   int newFrameIndex, bool useInvisibleFields)
{
   CVideoFrame *frame = srcFrameList.getFrame(newFrameIndex);
   if (frame == 0)
      {
      errorFlag = -1;  // TBD Replace with real error value
      return errorFlag;
      }

   frameIndex = newFrameIndex;

   srcInterlaced = frame->getImageFormat()->getInterlaced();

   if (useInvisibleFields)
      {
      int totalFieldCount = frame->getTotalFieldCount();

      SInvisibleFieldBuffer ifb;
      ifb.fieldBuffer = 0;
      int visibleFieldCount = frame->getVisibleFieldCount();
      for (int fieldIndex = visibleFieldCount;
           fieldIndex < totalFieldCount;
           ++fieldIndex)
         {
         CField *field = frame->getBaseField(fieldIndex);
         ifb.visibleSiblingFieldIndex = field->getVisibleSiblingFieldIndex();
         ifb.srcVisibleFieldCount = visibleFieldCount;
         invisibleFieldList.push_back(ifb);
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Copy Image
//////////////////////////////////////////////////////////////////////
// Returns true if frame & field buffers have been allocated in the
// destination (this).  Returns false if could not allocate all the
// buffers, in which case CopyImage needs to be called again.
// Example of usage:
//
//    while (!dstBuffer->CopyImage(srcBuffer, true, true))
//      {
//      // Might also want to try freeing up a buffer from the cache here
//      MTImillisleep(1); // wait a bit before trying again
//      }
//
//
bool CToolFrameBuffer::CopyImage(const CToolFrameBuffer &srcFrameBuffer,
                                 bool copyVisible, bool copyInvisible)
{
   int i;
   MTI_UINT16 *dstBuffer;
   const MTI_UINT16 *srcBuffer;
   int invisibleFieldCount;

   srcInterlaced = srcFrameBuffer.srcInterlaced;
   errorFlag = srcFrameBuffer.errorFlag;
   endOfMaterial = srcFrameBuffer.endOfMaterial;
   // don't copy the forceWrite flag
   // don't copy the dontCache flag

   if (copyVisible)
      {
      if (AllocateVisibleFrameBuffer() != 0)
         return false;
      }

   if (copyInvisible)
      {
      invisibleFieldCount = GetInvisibleFieldCount();
      int srcInvisibleFieldCount = srcFrameBuffer.GetInvisibleFieldCount();

      if (invisibleFieldCount < srcInvisibleFieldCount)
         {
         // Add entries to the invisible field list
         SInvisibleFieldBuffer ifb;
         ifb.fieldBuffer = 0;
         for (i = invisibleFieldCount; i < srcInvisibleFieldCount; ++i)
            {
            invisibleFieldList.push_back(ifb);
            }
         }

      for (i = 0; i < srcInvisibleFieldCount; ++i)
         {
         const SInvisibleFieldBuffer &srcIfb
                                = srcFrameBuffer.invisibleFieldList[i];
         SInvisibleFieldBuffer &dstIfb = invisibleFieldList[i];
         dstIfb.visibleSiblingFieldIndex = srcIfb.visibleSiblingFieldIndex;
         dstIfb.srcVisibleFieldCount = srcIfb.srcVisibleFieldCount;
         }

      if (AllocateInvisibleFieldBuffers() != 0)
         return false;
      }

   // Any code below this point will only be executed once after
   // all of the frame & field buffers have been allocated

   // Copy new pixel locations
   *newPixels = *(srcFrameBuffer.newPixels);

   MTI_UINT32 bufferSize = frameImageFormat->getBytesPerField();
   
   if (copyVisible)
      {
      srcBuffer = srcFrameBuffer.GetVisibleFrameBufferPtr();
      if (srcBuffer != 0)
         {
         dstBuffer = GetVisibleFrameBufferPtr();
         MTImemcpy((void *)dstBuffer, (void *)srcBuffer, bufferSize);
         }
      }

   if (copyInvisible)
      {
      invisibleFieldCount = GetInvisibleFieldCount();

      for (i = 0; i < invisibleFieldCount; ++i)
         {
         srcBuffer = srcFrameBuffer.invisibleFieldList[i].fieldBuffer;
         if (srcBuffer != 0)
            {
            dstBuffer = invisibleFieldList[i].fieldBuffer;
            MTImemcpy((void *)dstBuffer, (void *)srcBuffer, bufferSize);
            }
         }
      }

   return true;
}

//////////////////////////////////////////////////////////////////////
// Copy All Image Bits Only - like CopyImage but only copies the bits
//////////////////////////////////////////////////////////////////////
//
void CToolFrameBuffer::CopyAllImageBitsOnly(const CToolFrameBuffer &srcFrameBuffer)
{
   int i;
   MTI_UINT16 *dstBuffer;
   const MTI_UINT16 *srcBuffer;
   int invisibleFieldCount;

   MTI_UINT32 bufferSize = frameImageFormat->getBytesPerField();

   // Copy visible field
   srcBuffer = srcFrameBuffer.GetVisibleFrameBufferPtr();
   MTIassert(srcBuffer != 0);
   if (srcBuffer != 0)
      {
      dstBuffer = GetVisibleFrameBufferPtr();
      MTIassert(dstBuffer != 0);
      if (dstBuffer != 0)
         MTImemcpy((void *)dstBuffer, (void *)srcBuffer, bufferSize);
      }

   invisibleFieldCount = GetInvisibleFieldCount();

   // Copy invisible fields
   for (i = 0; i < invisibleFieldCount; ++i)
      {
      srcBuffer = srcFrameBuffer.invisibleFieldList[i].fieldBuffer;
      MTIassert(srcBuffer != 0);
      if (srcBuffer != 0)
         {
         dstBuffer = invisibleFieldList[i].fieldBuffer;
         MTIassert(dstBuffer != 0);
         if (dstBuffer != 0)
            MTImemcpy((void *)dstBuffer, (void *)srcBuffer, bufferSize);
         }
      }
}

//////////////////////////////////////////////////////////////////////
// Allocation
//////////////////////////////////////////////////////////////////////

int CToolFrameBuffer::AllocateAllFrameBuffers()
{
   int retVal = AllocateVisibleFrameBuffer();
   if (retVal != 0)
      return retVal;

   return AllocateInvisibleFieldBuffers();
}

int CToolFrameBuffer::AllocateVisibleFrameBuffer()
{
   if (visibleFrameBuffer == 0)
      {
      // Get a new frame buffer for the visible frame from the allocator.
      int retVal = frameBufferAllocator->allocateBuffer((void**)&visibleFrameBuffer);
      if (retVal != 0)
         return retVal;
      }

   // Return true if buffer is available, false if buffer has not been
   // allocated
   return (visibleFrameBuffer == 0) ? IMGTOOL_ERROR_NULL_PTR : 0;
}

int CToolFrameBuffer::AllocateInvisibleFieldBuffers()
{
   int invisibleFieldCount = GetInvisibleFieldCount();

   for (int i = 0; i < invisibleFieldCount; ++i)
      {
      if (invisibleFieldList[i].fieldBuffer == 0)
         {
         int retVal = frameBufferAllocator->allocateBuffer((void**)&invisibleFieldList[i].fieldBuffer);
         if (retVal != 0)
            return retVal;  // QQQ leaks previously allocated buffers?
         }

      if (invisibleFieldList[i].fieldBuffer == 0)
         return IMGTOOL_ERROR_NULL_PTR;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Native <=> Internal Format Conversion Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    ConvertFromNativeFormat
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CToolFrameBuffer::ConvertFromNativeFormat(CExtractor *extractor,
                                            const CImageFormat* srcImageFormat,
                                              MTI_UINT8** srcFields)
{
   if (errorFlag != 0)
      return errorFlag;

   MTI_UINT8 *fieldArray[2];
   int visibleFieldCount = srcImageFormat->getFieldCount();

   // Convert visible field(s) to a single internal format progressive
   // frame
   fieldArray[0] = srcFields[0];
   if (visibleFieldCount > 1)
      fieldArray[1] = srcFields[1];
   extractor->extractActivePicture(visibleFrameBuffer, fieldArray,
                                   srcImageFormat, srcImageFormat);

   // If there are any invisible fields, then convert them
   int invisibleFieldCount = GetInvisibleFieldCount();
   for (int i = 0; i < invisibleFieldCount; ++i)
      {
      if ((invisibleFieldList[i].visibleSiblingFieldIndex % visibleFieldCount)
                                                                           == 0)
         {
         // Even field or progressive frame
         fieldArray[0] = srcFields[i + visibleFieldCount];
         fieldArray[1] = 0;
         }
      else
         {
         // Odd field
         fieldArray[0] = 0;
         fieldArray[1] = srcFields[i + visibleFieldCount];
         }
      extractor->extractActivePicture(invisibleFieldList[i].fieldBuffer,
                                      fieldArray, srcImageFormat, srcImageFormat);
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    ConvertToNativeFormat
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CToolFrameBuffer::ConvertToNativeFormat(CExtractor *extractor,
                                           const CImageFormat *dstImageFormat,
                                            MTI_UINT8** dstFields)
{
   if (errorFlag != 0)
      return errorFlag;

   MTI_UINT8 *fieldArray[2];
   int visibleFieldCount = dstImageFormat->getFieldCount();

   // Convert visible frame to one or two native format fields
   fieldArray[0] = dstFields[0];
   if (visibleFieldCount > 1)
      fieldArray[1] = dstFields[1];
   extractor->replaceActivePicture(fieldArray, dstImageFormat,
                                   visibleFrameBuffer);

   // If there are any invisible fields, then convert them
   int invisibleFieldCount = GetInvisibleFieldCount();
   for (int i = 0; i < invisibleFieldCount; ++i)
      {
      if ((invisibleFieldList[i].visibleSiblingFieldIndex % visibleFieldCount)
                                                                           == 0)
         {
         // Even field or progressive frame
         fieldArray[0] = dstFields[i + visibleFieldCount];
         fieldArray[1] = 0;
         }
      else
         {
         // Odd field
         fieldArray[0] = 0;
         fieldArray[1] = dstFields[i + visibleFieldCount];
         }
      extractor->replaceActivePicture(fieldArray, dstImageFormat,
                                      invisibleFieldList[i].fieldBuffer);
      }

   return 0;
}

void CToolFrameBuffer::SetDummyImage()
{
   if (visibleFrameBuffer == 0)
      return;  // Nothing to do, no buffer

   // Get dummy component values.
   MTI_UINT16 componentValues[MAX_COMPONENT_COUNT];
   frameImageFormat->getComponentValueBlack(componentValues);  // Black for now

   RECT activePictureRect = frameImageFormat->getActivePictureRect();
   int pxlComponentCount  = 3; // frameImageFormat->getComponentCount();    xyzzy1

   int pitch = frameImageFormat->getTotalFrameWidth();
   for (int y = activePictureRect.top; y <= activePictureRect.bottom; ++y)
      {
      MTI_UINT16 *cp = visibleFrameBuffer
                  + (y * pitch + activePictureRect.left) * pxlComponentCount;
      for (int x = activePictureRect.left; x <= activePictureRect.right; ++x)
         {
         for (int i=0; i<pxlComponentCount; i++)
            *cp++ = componentValues[i];
         }
      }
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

bool CToolFrameBuffer::ContainsFloatData() const
{
	if (frameImageFormat == nullptr)
	{
		return false;
	}

	return frameImageFormat->getPixelPacking() == IF_PIXEL_PACKING_1_Half_IN_2Bytes;
}

bool CToolFrameBuffer::IsEndOfMaterial() const
{
   return endOfMaterial;
}

bool CToolFrameBuffer::IsPixelsChanged()
{
   // Returns true if the newPixels pixel region list has one or more
   // entries.
   return (newPixels->GetEntryCount() > 0);
}

//------------------------------------------------------------------------
//
// Function:     GetErrorFlag
//
// Description:  Returns current value of this CToolFrameBuffer's error
//               flag.
//
//               The errorFlag set when either the MediaReader failed to
//               read this frame or if a previous tool had an error on
//               this frame.  For example, if a frame index was requested
//               that was out-of-range for the clip, the readMedia in
//               the MediaReader would fail and set the errorFlag.  The
//               MediaReader will pass on this frame anyway, even though
//               it does not contain valid pixel data, so that the following
//               tools may fail gracefully and we do not get a pipeline stall
//               with tools waiting for frames that will never arrive.
//
//               A tool may look at the error flag to make sure the frame
//               contains valid pixel data.
//
// Arguments:    None
//
// Returns:      0 indicates no error.  Non-zero indicate an error.
//
//------------------------------------------------------------------------
int CToolFrameBuffer::GetErrorFlag() const
{
   return errorFlag;
}

int CToolFrameBuffer::GetFrameIndex() const
{
   return frameIndex;
}

int CToolFrameBuffer::GetInvisibleFieldCount() const
{
   return (int)invisibleFieldList.size();
}

bool CToolFrameBuffer::IsForceWrite() const
{
   return forceWrite;
}

bool CToolFrameBuffer::IsDontCache() const
{
   return dontCache;
}


//------------------------------------------------------------------------
//
// Function:    GetOriginalValuesPixelRegionListPtr
//
// Description: Returns a pointer to this CToolFrameBuffer's
//              pixel regions list that holds the "original values."
//              A tool is responsible for adding pixel regions to this
//              list that capture the pixels' values before the tool has
//              set new pixel values.  The original values pixel region
//              list has two uses after the tool is done with processing.
//              First, the original pixel values themselves can be saved
//              to the History.  Second, the descriptions of the pixel
//              regions (but not the pixel values) are used to know which
//              pixels to update in the invisible fields (which the tool
//              does not touch).
//
//              See CPixelRegionList for family of Add functions that
//              add the various types of pixel regions: rectangle, point, etc.
//              to the pixel region list.
//
// Arguments:   None
//
// Returns:     Pointer to CToolFrameBuffer's CPixelRegionList instance
//              that is used by the tool to record the original value
//              of pixels it is modifying.
//
//------------------------------------------------------------------------
CPixelRegionList* CToolFrameBuffer::GetOriginalValuesPixelRegionListPtr()
{
   return originalValues;
}

CPixelRegionList* CToolFrameBuffer::GetNewPixelsPixelRegionListPtr()
{
   return newPixels;
}

MTI_UINT16* CToolFrameBuffer::GetVisibleFrameBufferPtr() const
{
   return visibleFrameBuffer;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CToolFrameBuffer::SetErrorFlag(int newErrorFlag)
{
   errorFlag = newErrorFlag;
}

void CToolFrameBuffer::SetEndOfMaterial()
{
   endOfMaterial = true;
}

void CToolFrameBuffer::SetFrameIndex(int newFrameIndex)
{
   frameIndex = newFrameIndex;
}

void CToolFrameBuffer::SetForceWrite(bool newForceWrite)
{
   forceWrite = newForceWrite;
}

void CToolFrameBuffer::SetDontCache(bool newDontCache)
{
   dontCache = newDontCache;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:     SaveToHistory
//
// Description:  This function saves the "original values" in this
//               CToolFrameBuffer to the Save/Restore History.  There
//               are two parts to this.  First, the pixel values stored
//               within the originalValues pixel region list are saved to
//               the history.  It is assumed that whatever tool created
//               or modified this CToolFrameBuffer instance added a
//               entries to the originalValues pixel region list for all
//               of the pixels it set.  Second, the pixel values in the
//               invisible fields are saved to the History.  This is done
//               by replacing the pixel values in the pixel region list with
//               the pixel values from the invisible fields.  At this
//               point the pixel values in the invisible fields are assumed
//               to be the "original" values.
//
// Arguments:
//
// Returns:      None
//
//------------------------------------------------------------------------
int CToolFrameBuffer::SaveToHistory(CSaveRestore &saveRestore,
                                    int toolNumber, const string &toolString,
                                    int *saveCount)
{
   int retVal;

   if (saveCount != 0)
      *saveCount = 0;

   if (originalValues->GetEntryCount() == 0)
      return 0;   // Nothing to save

   // Save the original values for the VISIBLE FIELDS (invisible fields are
   // handled separately, below!)
      {
      // Perform the history save operation atomically, also makes sure the
      // history files get closed when we leave scope
      CAutoHistoryOpener historyOpener(saveRestore, FLUSH_TYPE_SAVE, retVal);
      if (retVal != 0)
         return retVal;    // History is unopenable

      // Tag these saved regions uniquely
      saveRestore.newGlobalSaveNumber();

      // Call history to save the original values from the visible frame
      retVal = saveRestore.saveRegionList(GetFrameIndex(), 0, -1, toolNumber,
										  toolString.c_str(), *originalValues);
      if (retVal != 0)
         return retVal;    // ERROR

      if (saveCount != 0)
         *saveCount = 1;

      }

   //////////////////////////////////////////////////////////////////////
   // WE NOW CLOSE & REOPEN THE HISTORY FILES FOR EACH INVISIBLE FIELD //
   // TO WORK AROUND A BUG IN SAVERESTORE where it overwrites visible  //
   // field values with invisible field values if we try to do it all  //
   // ine one "transaction"                                            //
   //////////////////////////////////////////////////////////////////////

   // Save the original values from the invisible fields
   int frameWdth = frameImageFormat->getTotalFrameWidth();
   int frameHght = frameImageFormat->getTotalFrameHeight();
   for (int i = 0; i < GetInvisibleFieldCount(); ++i)
      {
      // perform the history save operation atomically, also makes sure the
      // history files get closed when we leave scope
      CAutoHistoryOpener historyOpener(saveRestore, FLUSH_TYPE_SAVE, retVal);
      if (retVal != 0)
         return retVal;    // History is unopenable

      SInvisibleFieldBuffer &ifb = invisibleFieldList[i];

      // Figure out which lines, odd or even, to save.  If the native source
      // format is interlaced, then we save either even or odd lines depending
      // on whether the visible sibling field is odd or even.  If the native
      // source format is progressive, then we save both even and odd lines.
      bool oddField
              = !(ifb.visibleSiblingFieldIndex % ifb.srcVisibleFieldCount == 0);
      bool interlaced = (ifb.srcVisibleFieldCount > 1);

      // Get the original pixel values from the invisible field and
      // put them in the pixel region list of "original values"
      bool copyEven = !interlaced || !oddField;
      bool copyOdd = !interlaced || oddField;
      originalValues->ReplacePixelsFromFrame(ifb.fieldBuffer,
                                             frameWdth, frameHght,
                                             copyEven, copyOdd);

      // call history with invisible field's original values
      int fieldIndex = ifb.srcVisibleFieldCount + i;
      int oddEvenFlag;
      if (!interlaced)
         oddEvenFlag = -1; // Progressive, both odd & even lines
      else if (oddField)
         oddEvenFlag = 1;  // Interlaced, odd field
      else
         oddEvenFlag = 0;  // Interlaced, even field

      // Crank the unique tagifier
      saveRestore.newGlobalSaveNumber();

      retVal = saveRestore.saveRegionList(GetFrameIndex(), fieldIndex,
                                          oddEvenFlag, toolNumber,
                                          toolString.c_str(), *originalValues);
      if (retVal != 0)
         return retVal;    // ERROR

      if (saveCount != 0)
         *saveCount += 1;
      }

   if (retVal != 0)
      return retVal;

   return 0; // Success
}

//------------------------------------------------------------------------
//
// Function:    CopyParentHistoryToVersion
//
// Description: This function calls the saveRestoreEngine to copy a
//              parent history file to the version history directory
//              It does nothing when operating on a parent clip or
//              if the history mode is file-per-clip
//
// Arguments:   the saveRestore engine
//
// Returns:     0 on success
//
//------------------------------------------------------------------------
int CToolFrameBuffer::CopyParentHistoryToVersion(CSaveRestore &saveRestore)
{
   {
      int retVal;

      // Performs history save operation atomically, also makes sure the
      // history files get closed when we leave scope
      CAutoHistoryOpener historyOpener(saveRestore, FLUSH_TYPE_SAVE, retVal);
      if (retVal != 0)
         return retVal;    // History is unopenable

      // If version & file-per-frame, copy history file to version directory
      retVal = saveRestore.copyParentHistoryToVersion(GetFrameIndex());
      if (retVal != 0)
         return retVal;    // ERROR
   }

   return 0;
}

int  CToolFrameBuffer::RestoreFromHistoryInternal(CSaveRestore &saveRestore)
{
   int visibleFieldCount = (srcInterlaced) ? 2 : 1;
   int invisibleFieldCount = GetInvisibleFieldCount();
   int retVal = 0;

   // Get all of the history records for the given frame
   while (saveRestore.nextRestoreRecord() == 0)
      {
      // Use the restore record's field index to direct the restoration
      // to either the visible frame or one of the invisible fields
      int restoreFieldIndex = saveRestore.getCurrentRestoreRecordFieldIndex();
      if (restoreFieldIndex == 0)
         {
         // Presumably the visible frame (made from one or two fields)
         retVal = saveRestore.restoreCurrentRestoreRecordPixels(visibleFrameBuffer,
                                                       true);
         if (retVal != 0)
            break;

         }
      else
         {
         // Presumably an invisible field
         int invisibleFieldIndex = restoreFieldIndex - visibleFieldCount;
         if (invisibleFieldIndex < invisibleFieldCount)
            {
            MTI_UINT16 *fieldBuffer
                          = invisibleFieldList[invisibleFieldIndex].fieldBuffer;
            if (fieldBuffer != 0)
               {
               retVal = saveRestore.restoreCurrentRestoreRecordPixels(fieldBuffer,true);
               if (retVal != 0)
                  break;
               }
            }
         }
      }
   return retVal;
}

int CToolFrameBuffer::RestoreFromHistory(CSaveRestore &saveRestore,
                                         RECT *filterRect, POINT *filterLasso,
                                         BEZIER_POINT *filterBezier,
                                         CPixelRegionList *filterPixelRegion)
{
   int retVal = 0;
   int errorCode = 0;

   if (filterPixelRegion != 0)
      {
      CAutoHistoryOpener opener(saveRestore, FLUSH_TYPE_RESTORE,
                                GetFrameIndex(), -1,
                                *filterPixelRegion,
                                errorCode);
      if (errorCode == 0)
         {
         retVal = RestoreFromHistoryInternal(saveRestore);
         }
      }
   else
      {
      CAutoHistoryOpener opener(saveRestore, FLUSH_TYPE_RESTORE,
                                GetFrameIndex(), -1,
                                filterRect, filterLasso, filterBezier,
                                errorCode);
      if (errorCode == 0)
         {
         retVal = RestoreFromHistoryInternal(saveRestore);
         }
   }

   if (retVal != 0)
      return retVal;

   if (errorCode != 0)
      return errorCode;

// This was removed because it doesn't work - revisions must be made ONLY
// when the frame is written to disk, so is now done in the media writer
// pseudotool
#if 0
   // when no toggling is to be done, revise
   // the GOV regions immediately on restore
   if (saveRestore.getReviseGOVFlag()) {

      retVal = saveRestore.reviseGOVRegions(GetFrameIndex());
      if (retVal != 0)
         return retVal;    // ERROR
   }
#endif // 0

   return 0;
}

void CToolFrameBuffer::AppendNewPixels()
{
   // Put the new pixel values into the "original values."  This isn't
   // strictly necessary, as we should really have no pixel values
   // in the "new pixels"
   int frameWdth = frameImageFormat->getTotalFrameWidth();
   int frameHght = frameImageFormat->getTotalFrameHeight();
   originalValues->ReplacePixelsFromFrame(visibleFrameBuffer,
                                          frameWdth, frameHght,
                                          true, true);

   // Append the locations of the changed pixels, as found in the
   // originalValues pixel region list, to the newPixels pixel region list
   newPixels->Add(*originalValues);
}

void CToolFrameBuffer::AppendNewPixelsAndClearOriginalValues()
{
   // THIS IS FROM AppendNewPixels() METHOD:
   // mbraca: these comments makes NO SENSE to me!

   // Put the new pixel values into the "original values."  This isn't
   // strictly necessary, as we should really have no pixel values
   // in the "new pixels"
   int frameWdth = frameImageFormat->getTotalFrameWidth();
   int frameHght = frameImageFormat->getTotalFrameHeight();
   originalValues->ReplacePixelsFromFrame(visibleFrameBuffer,
                                          frameWdth, frameHght,
                                          true, true);

   // Append the locations of the changed pixels, as found in the
   // originalValues pixel region list, to the newPixels pixel region list,
   // then clear the original values list (i.e. move the regions from the
   // originalValues list to the newPixels list)
   newPixels->Move(*originalValues);

}

int CToolFrameBuffer::GetAllocSize()
{
	return frameBufferAllocator->getBufferSizeInBytes();
}

//------------------------------------------------------------------------
//
// Function:    Update Invisible Fields
//
// Description: Copy the changes made to the visible frame to the invisible
//              fields. The "original values" pixel region list is used to 
//              determine which pixels have been modified.
//
// Arguments:   None
//
// Returns:     None
//
//------------------------------------------------------------------------
void CToolFrameBuffer::UpdateInvisibleFields()
{
   int frameWdth = frameImageFormat->getTotalFrameWidth(); // Pitch in pixels
   int frameHght = frameImageFormat->getTotalFrameHeight();
   for (int fieldIndex = 0; fieldIndex < GetInvisibleFieldCount(); ++fieldIndex)
      {
      SInvisibleFieldBuffer &ifb = invisibleFieldList[fieldIndex];

      // Figure out which lines, odd or even, to copy.  If the native source
      // format is interlaced, then we copy either even or odd lines depending
      // on whether the visible sibling field is odd or even.  If the native
      // source format is progressive, then we copy both even and odd lines.
      bool oddField
// TTT              = (ifb.visibleSiblingFieldIndex % ifb.srcVisibleFieldCount == 0);
              = !(ifb.visibleSiblingFieldIndex % ifb.srcVisibleFieldCount == 0);
      bool interlaced = (ifb.srcVisibleFieldCount > 1);
      bool copyEven = !interlaced || !oddField;
      bool copyOdd = !interlaced || oddField;

      // Update the invisible fields with the changed values from the
      // visible frame
	  originalValues->CopyPixelsFrameToFrame(visibleFrameBuffer,
                                             frameWdth, frameHght,
                                             ifb.fieldBuffer,
                                             copyEven, copyOdd);
      }
}

//////////////////////////////////////////////////////////////////////
// Buffer Deallocation Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:     ReleaseAllBuffers
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
void CToolFrameBuffer::ReleaseAllBuffers()
{
   ReleaseVisibleFrameBuffer();

   ReleaseInvisibleFieldBuffers();
}

void CToolFrameBuffer::ReleaseVisibleFrameBuffer()
{
   if (visibleFrameBuffer != 0)
      {
      frameBufferAllocator->freeBuffer(visibleFrameBuffer);

      visibleFrameBuffer = 0;
      }
}

void CToolFrameBuffer::ReleaseInvisibleFieldBuffers()
{
   for (int fieldIndex = 0; fieldIndex < GetInvisibleFieldCount(); ++fieldIndex)
      {
      SInvisibleFieldBuffer &ifb = invisibleFieldList[fieldIndex];

      if (ifb.fieldBuffer != 0)
         {
         frameBufferAllocator->freeBuffer(ifb.fieldBuffer);
         ifb.fieldBuffer = 0;
         }
      }
}

//------------------------------------------------------------------------
//
// Function:
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
void CToolFrameBuffer::ClearOriginalValues()
{
   originalValues->Clear();
}

void CToolFrameBuffer::ClearNewPixels()
{
   newPixels->Clear();
}

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###     CToolFrameQueue Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolFrameQueue::CToolFrameQueue()
 : maxEntryCount(0), entryCount(0)
{
}

CToolFrameQueue::~CToolFrameQueue()
{
   Flush();
}

//////////////////////////////////////////////////////////////////////
// Get and Put Frame Buffers
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    PutFrame
//
// Description: Put a CToolFrameBuffer pointer into the Queue
//
// Arguments:
//
// Returns:     true if put CToolFrameBuffer into Queue
//              false if the queue was full
//
//------------------------------------------------------------------------
bool CToolFrameQueue::PutFrame(CToolFrameBuffer *frameBuffer)
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

   // Check if frame queue is full.  However, there is always room
   // for an "end of material" frame buffer
   if (Full() && !frameBuffer->IsEndOfMaterial())
      return false;

   frameList.push_back(frameBuffer);
   ++entryCount;

   return true;
}

//------------------------------------------------------------------------
//
// Function:     GetFrame
//
// Description:  Get the oldest CToolFrameBuffer from the queue.  
//
// Arguments:    None
//
// Returns:      Pointer to CToolFrameBuffer from queue.  Returns NULL
//               if no frame buffers were in the queue
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolFrameQueue::GetFrame()
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

   if (Empty())
      {
      return 0;   // Queue is empty, return NULL pointer
      }
   else
      {
      CToolFrameBuffer* frameBuffer = frameList.front();   // Get pointer
      frameList.pop_front();             // Remove the first element
      --entryCount;
      return frameBuffer;
      }
}

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get the CToolFrameBuffer from the queue that matches the
//              frame index
//
// Arguments:
//
// Returns:      Pointer to CToolFrameBuffer from queue.  Returns NULL if
//               frame buffer with caller's frame index was not in the queue
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolFrameQueue::GetFrame(int frameIndex)
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return


   if (Empty())
      {
      return 0;  // Queue is empty, return NULL pointer
      }
   else
      {
      ToolFrameListIterator iterator = findFrameIndex(frameIndex);
      if (iterator == frameList.end())
         {
         return 0;   // Frame index not found, return NULL pointer
         }
      CToolFrameBuffer *frameBuffer = *iterator;  // Get pointer
      frameList.erase(iterator);    // Remove this element
      --entryCount;
      return frameBuffer;
      }
}


//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

const CImageFormat* CToolFrameQueue::GetImageFormat() const
{
   return imageFormat;
}

int CToolFrameQueue::GetMaxEntryCount() const
{
   return maxEntryCount;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CToolFrameQueue::SetImageFormat(const CImageFormat *newImageFormat)
{
   imageFormat = newImageFormat;
}

//////////////////////////////////////////////////////////////////////
// Queue Utility Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    Empty
//
// Description:
//
// Arguments:
//
// Returns:     true if queue is empty
//
//------------------------------------------------------------------------
bool CToolFrameQueue::Empty()
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

	return frameList.empty();
}

//------------------------------------------------------------------------
//
// Function:    Check
//
// Description:
//
// Arguments:
//
// Returns:     true if frame is present in queue
//
//------------------------------------------------------------------------
bool CToolFrameQueue::Check(int frameIndex)
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

   return (findFrameIndex(frameIndex) != frameList.end());
}

//------------------------------------------------------------------------
//
// Function:
//
// Description: Remove all entries from queue and deallocate all buffers
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
void CToolFrameQueue::Flush()
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

   ToolFrameListIterator iterator;
   for (iterator = frameList.begin(); iterator != frameList.end(); ++iterator)
      {
      delete *iterator;
      }

   frameList.clear();

   entryCount = 0;
}

//------------------------------------------------------------------------
//
// Function:    findFrameIndex
//
// Description: Searches for a queue entry with the caller's
//              frame index.  Assumes thread is already locked.
//
// Arguments:   int frameIndex    Frame index to search for
//
// Returns:     Iterator into the frame buffer list
//
//------------------------------------------------------------------------
CToolFrameQueue::ToolFrameListIterator
CToolFrameQueue::findFrameIndex(int frameIndex)
{
   // First element is the least recently added
   ToolFrameListIterator iterator;
   for (iterator = frameList.begin(); iterator != frameList.end(); ++iterator)
      {
      if ((*iterator)->GetFrameIndex() == frameIndex)
         return iterator;  // found it
      }

   return iterator; // Didn't find it, return end iterator
}

bool CToolFrameQueue::Full()
{
   CAutoThreadLocker autoThreadLocker(threadLock);  // Lock thread until return

   if (maxEntryCount == 0 || entryCount < maxEntryCount)
      return false;

   return true;
}

void CToolFrameQueue::SetMaxEntryCount(int newMaxEntryCount)
{
   if (newMaxEntryCount < 0)
      newMaxEntryCount = 0;

   if (newMaxEntryCount > maxEntryCount)
      maxEntryCount = newMaxEntryCount;
}

std::ostream& operator<< (std::ostream& os, const CToolFrameQueue& tfq)
{
	bool first = true;
	for (auto iter = tfq.frameList.begin(); iter != tfq.frameList.end(); ++iter)
	{
		if (first)
		{
			first = false;
		}
		else
		{
			os << ", ";
		}

		os << (*iter)->GetFrameIndex() << " (" << (*iter)->GetAllocSize() << ")";
	}

	return os;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###     CToolFrameCache Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

void CToolFrameCache::EnableCaching(bool newEnabledFlag)
{
   if (enabledFlag == true && newEnabledFlag == false)
   {
      Clear();
   }

   enabledFlag = newEnabledFlag;
}

void CToolFrameCache::AddFrame(CToolFrameBuffer *buffer)
{
   // No-op if not presently enabled
   if (!enabledFlag)
      {
      TRACE_3(errout << "TOOLFRAMECACHE: DISABLED - can't add " << buffer->GetFrameIndex() );
      delete buffer;
      return;
      }

   // Don't cache bogus or error frames
   if (buffer == NULL || buffer->GetErrorFlag() || buffer->IsEndOfMaterial())
      {
      TRACE_3(errout << "TOOLFRAMECACHE: Reject(" << buffer->GetFrameIndex() << ")");
      delete buffer;
      return;
      }

   // Replace existing buffer for this frame if applicable
   // AND IF IT'S NOT THE SAME FUCKING BUFFER!
   CToolFrameBuffer *existing = cachedFrames.GetFrame(buffer->GetFrameIndex());
   if (existing == NULL)
	  {
	  TRACE_3(errout << "TOOLFRAMECACHE: Add(" << buffer->GetFrameIndex() << ")");
	  }
   else if (existing == buffer)
	  {
	  TRACE_3(errout << "TOOLFRAMECACHE: Skip(" << buffer->GetFrameIndex() << " - already cached!!!)");
	  return;
	  }
   else
      {
	  TRACE_3(errout << "TOOLFRAMECACHE: Replace(" << buffer->GetFrameIndex() << ")");
	  delete existing;
	  }

   // Clean it up before stashing it in the cache
   buffer->ClearOriginalValues();
   buffer->ClearNewPixels();
   buffer->SetForceWrite(false);

	cachedFrames.PutFrame(buffer);
	TRACE_3(errout << *this);
}

CToolFrameBuffer *CToolFrameCache::RetrieveFrame(int frameIndex)
{
   CToolFrameBuffer *buffer = cachedFrames.GetFrame(frameIndex);

   if (buffer == NULL)
      {
      TRACE_3(errout << "TOOLFRAMECACHE: " << frameIndex << " NOT FOUND IN CACHE");
      }
   else
      {
      MTIassert(enabledFlag); // If cache is disabled - it must be empty!!
      TRACE_3(errout << "TOOLFRAMECACHE: Retrieve(" << frameIndex << ")");
		}

	TRACE_3(errout << *this);

   return buffer;
}

// We use this call to get a frame to delete. We can't delete the frame here
// for some reason that I don't feel like investigating, so the caller needs
// to be the one to delete it

CToolFrameBuffer *CToolFrameCache::GetOldestFrame()
{
   // We are presuming that the oldest buffer (first on the queue) is
   // also the one with the lowest frame index (which is what "oldest"
   // really means here). Should we somehow enfore that, say by keeping
   // the queue sorted by ascending frame index? QQQ
   CToolFrameBuffer *buffer = cachedFrames.GetFrame();

   if (buffer != NULL)
      {
      MTIassert(enabledFlag); // If cache is disabled - it must be empty!!
      TRACE_3(errout << "TOOLFRAMECACHE: GetOldest returned " << buffer->GetFrameIndex());
      }
   else
      {
		TRACE_3(errout << "TOOLFRAMECACHE: GetOldest returned NULL");
		}

   return buffer;
}

void CToolFrameCache::Clear()
{
   TRACE_3(errout << "TOOLFRAMECACHE: CLEAR CACHE");
   cachedFrames.Flush();
}

std::ostream& operator<< (std::ostream& os, const CToolFrameCache& tfc)
{
	os << "TOOLFRAMECACHE: contains " << tfc.cachedFrames;

	return os;
}


//////////////////////////////////////////////////////////////////////


