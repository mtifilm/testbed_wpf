// ToolObject.cpp: implementation of the CToolObject class.
//
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolObject.cpp,v 1.11.2.36 2009/07/15 15:17:09 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolObject.h"
#include "BinManager.h"
#include "bthread.h"
#include "CompactTrackEditFrameUnit.h"    // Yuck QQQ
#include "DfltToolProgressMon.h"
#include "err_tool.h"
#include "HRTimer.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "SharedMap.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolEvent.h"
#include "ToolFrameBuffer.h"
#include "ToolNode.h"
#include "ToolUserInputMap.h"
#include "TrackEditFrameUnit.h"     // Yuck QQQ
#include "systemid.h"

//////////////////////////////////////////////////////////////////////

const string InViewOnlyModeKey("ToolsAreInViewOnlyMode");

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolObject::CToolObject(const string &newToolName,
                         MTI_UINT32 *newFeatureTable[])
: toolName(newToolName),
  featureTable(newFeatureTable),
  toolDisabled(true),
  toolLicensed(true),
  toolActive(false),
  toolLicenseWorkedOnce(false),
  toolLicenseNumStrikes(0),
  toolDisabledReason(TOOL_DISABLED_LICENSE_NOT_YET_CHECKED_OUT),
  userInputMap(new CToolUserInputMap),
  toolProcessingControlState(TOOL_CONTROL_STATE_STOPPED),
  toolProcessingErrorCode(0),
  Handle(0),
  renderFlag(false),
  renderToNewClipFlag(false),
  highlightFlag(true)
{
#ifndef NO_LICENSING
 string dummy;
#ifdef DS1961S_DONGLE
   rsrcCtrl = new CRsrcCtrl;
     	MTIostringstream os;
	os << "stupid";

	   if (featureTable != NULL)
      toolLicensed = rsrcCtrl->IsFeatureLicensed(featureTable[0], dummy);
#else
   extern char *ToolNameTable[];
   toolLicensed = IsToolLicensed(ToolNameTable[0], dummy);
#endif
#else
   toolLicensed = true;
#endif

   defaultToolProgressMonitor = new CDefaultToolProgressMonitor;
   toolProgressMonitor = defaultToolProgressMonitor;
}

CToolObject::~CToolObject()
{
	///TODO: FIx KLUDGE to stop crash
	return;

//   delete userInputMap;
//   userInputMap = NULL;
//
//   delete defaultToolProgressMonitor;
//   defaultToolProgressMonitor = NULL;
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    toolActivate
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::toolActivate()
{
   toolActive = true;

   // Default to mask completely disabled
   getSystemAPI()->SetMaskAllowed(false);
   getSystemAPI()->setMaskRoiIsRectangularOnly(false);
   getSystemAPI()->SetMaskToolAutoActivateRoiModeOnDraw(false);
   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::toolDeactivate()
{
   toolActive = false;
   TTrackEditFrame::ClearActiveTrackBar();
   TCompactTrackEditFrame::ClearActiveTrackBar();
   return 0;
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::toolHide()
{
   return 0;
}

// ===================================================================
//
// Function:    toolHideQuery
//
// Description: Determines if it is okay to close (hide) a tool.
//              The base version of the function always returns true,
//              indicating that it is okay to close the tool.  Individual
//              tools can override this function if the tool has some
//              reason for not allowing a close
//
// Arguments:   None
//
// Returns:     true - okay to close the tool
//              false - not okay to close the tool
//
// ===================================================================
bool CToolObject::toolHideQuery()
{
   getSystemAPI()->AcceptGOV();   // only default action is auto-accept GOV

   return true;
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::toolShow()
{
   return 0;
}

// ===================================================================
//
// Tool progress monitor
//
// ===================================================================
IToolProgressMonitor *CToolObject::GetToolProgressMonitor()
{
   return toolProgressMonitor;
}

void CToolObject::SetToolProgressMonitor(IToolProgressMonitor *newToolProgressMonitor)
{
   if (newToolProgressMonitor != NULL)
      toolProgressMonitor = newToolProgressMonitor;
   else
      toolProgressMonitor = defaultToolProgressMonitor;
}

// ===================================================================
//
// Tool execution permission
//
// ===================================================================
bool CToolObject::checkOutToolLicense()
{
#ifndef NO_LICENSING
   if (featureTable == NULL)
   {
		toolDisabled = false;
      return true;
   }

   if (!IsLicensed())
   {
      toolDisabled = true;
      toolDisabledReason = TOOL_DISABLED_TOOL_UNLICENSED;
      return false;
   }

   if (toolDisabled &&
   (toolDisabledReason == TOOL_DISABLED_LICENSE_NOT_YET_CHECKED_OUT))
   {
#ifdef DS1961S_DONGLE
      // Checkout.  The checkout should never really fail
      // Only if someone is hacking code.
      /////////////////////////////////////////////////////////////
      // NEW: it seems that checkout DOES fail for Larry, so now
      // we retry twice on failure and if all three attempts fail,
      // then we actually let the tool run; but if you get a triple
      // failure three times consecutively (the three strikes), THEN
      // we disable the tool!
      /////////////////////////////////////////////////////////////

      bool itJustWorked = false;

      if (rsrcCtrl->CheckOut(featureTable[0]))
      {
         itJustWorked = true;
      }
      else
      {
         rsrcCtrl->CheckIn();  // Need to check in even if check out failed
         if (rsrcCtrl->CheckOut(featureTable[0]))
         {
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX TOOL GLITCHED ONCE XXXXXXXXXXXXXXXXXXXXXX");
            itJustWorked = true;
         }
         else
         {
            rsrcCtrl->CheckIn();  // Need to check in even if check out failed
            if (rsrcCtrl->CheckOut(featureTable[0]))
            {
               TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX TOOL GLITCHED TWICE XXXXXXXXXXXXXXXXXXXXXX");
               itJustWorked = true;
            }
         }
      }

      if (itJustWorked)
      {
         toolDisabled = false;
         toolLicenseWorkedOnce = true;
         if (toolLicenseNumStrikes > 0)
         {
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX TOOL IS OK XXXXXXXXXXXXXXXXXXXXXX");
            toolLicenseNumStrikes = 0;
         }
      }
      else
      {
         if (toolLicenseWorkedOnce && (++toolLicenseNumStrikes < 3))
         {
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX TOOL STRIKE " << toolLicenseNumStrikes << " XXXXXXXXXXXXXXXXXXXXXX");
            // You get three strikes!
            toolDisabled = false;
         }
         else
         {
            if (toolLicenseNumStrikes ==3)
            {
               TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX TOOL STRIKE 3 - YER OUT! XXXXXXXXXXXXXXXXXXXXXX");
            }

            rsrcCtrl->CheckIn();  // Need to check in even if check out failed
            rsrcCtrl->DisplayError("Licensing failed");
            toolDisabledReason = TOOL_DISABLED_TOOL_UNLICENSED;
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX LICENSE FAIL XXXXXXXXXXXXXXXXXXXXXX");
         }
      }
#else
      toolDisabled = false;
#endif
   }

   return !toolDisabled;
#else
#endif
      toolDisabled = false;
      return true;
}

bool CToolObject::isItOkToUseToolNow()
{
   // Check out the license if we haven't already done so
	checkOutToolLicense();

	// Check view-only-mode FIRST!
	if (!toolDisabled && IsInViewOnlyMode())
	{
		toolDisabled = true;
		toolDisabledReason = TOOL_DISABLED_VIEW_ONLY_MODE;
	}
	else if (toolDisabled
	&& toolDisabledReason == TOOL_DISABLED_VIEW_ONLY_MODE
	&& !IsInViewOnlyMode())
	{
		toolDisabled = false;
	}

	if ((!toolDisabled) ||
       (toolDisabledReason == TOOL_DISABLED_MASTER_CLIP) ||
       (toolDisabledReason == TOOL_DISABLED_NO_CLIP))
      {
      if (!getSystemAPI()->isAClipLoaded())
         {
			toolDisabled = true;
			toolDisabledReason = TOOL_DISABLED_NO_CLIP;
			}
      else
         {
         ClipIdentifier currentClipId(getSystemAPI()->getClipFilename());
         CBinManager binManager;
         if (binManager.doesClipHaveVersions(currentClipId))
            {
            toolDisabled = true;
            toolDisabledReason = TOOL_DISABLED_MASTER_CLIP;
            }
         else
            {
            toolDisabled = false;
            }
         }
		}

   return !toolDisabled;
}

void CToolObject::doneWithTool()
{
   if ((!toolDisabled) || (toolDisabledReason != TOOL_DISABLED_TOOL_UNLICENSED))
      {
      toolDisabled = true;
      toolDisabledReason = TOOL_DISABLED_LICENSE_NOT_YET_CHECKED_OUT;

#ifndef NO_LICENSING
#ifdef DS1961S_DONGLE
      rsrcCtrl->CheckIn();
#endif
#endif
      }
}

//////////////////////////////////////////////////////////////////////
// Tool Event Handlers
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    onUserInput
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onUserInput(CUserInput &userInput)
{
   return (mapAndExecuteUserInput(userInput));
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onMouseMove(CUserInput &userInput)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onLeavingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onLeavingClip()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onNewMarks()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onTimelineVisibleFrameRangeChange
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onTimelineVisibleFrameRangeChange()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onChangingClip()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onDeletingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onDeletingOpenedClip()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}


// ===================================================================
//
// Function:    onNewClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onNewClip()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewFrame
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onNewFrame()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onChangingFraming
//
// Description: Called after framing (video or film) is changed
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CToolObject::onChangingFraming()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewFraming
//
// Description: Called after framing (video or film) is changed
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CToolObject::onNewFraming()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onLeavingFrame
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onLeavingFrame()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description: A tool's onRedraw event handler is called after
//              a frame has been drawn.  The tool can use the
//              onRedraw event to draw overlay graphics on top of
//              the frame image
//
// Arguments:   int frameIndex    Index of frame just drawn
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onRedraw(int frameIndex)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPreviewHackDraw
//
// Description: Calls active tool's onPreviewHackDraw event handler
//              before a frame is drawn, and the bits of the frame
//              are included in intermediate format so the tool can
//              modify them before they are actually drawn.
//
// Arguments:   int frameIndex              Index of frame to be drawn
//              unsigned short *frameBits   The bits of the frame in
//                                          intermediate format
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPreviewDisplayBufferDraw
//
// Description: Calls active tool's onPreviewDisplayBufferDraw event handler
//              before a frame is drawn, and the bits of the frame
//              are included in i8-bit BGRA format. Note that the bits
//              are provided only for pixels within the indicated bounds.
//
// Arguments:   int frameIndex              Index of frame to be drawn
//              int width, height           Width and height of the bitmap in PIXELS
//              unsigned int *pixels        The pixels of the frame in 8-bit RGBA format
//                                          (R=0x000000FF, G=0x0000FF00, B=0x00FF0000)
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onPreviewDisplayBufferDraw(int frameIndex, int width, int height, unsigned int *pixels)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onTopPanelRedraw
//
// Description: A tool's onTopPanelRedraw event handler is called after
//              it is determined that the content of the Main Window
//              top panel needs to be redrawn (i.e. it becomes visible
//              or is resized.
//
// Arguments:   None
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onTopPanelRedraw()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onTopPanelMouseDown
//
// Description: A tool's onTopPanelMouseDown event handler is called after
//              it is determined that left mouse button has been pressed
//              while the cursor is over the main window top panel.
//
// Arguments:   the client X and Y where the mouse was clicked in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onTopPanelMouseDown(int x, int y)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onTopPanelMouseDrag
//
// Description: A tool's onTopPanelMouseDrag event handler is called after
//              it is determined that mouse moved while we are in "drag"
//              mode in the main window top panel.
//
// Arguments:   the client X and Y where the mouse was moved to in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onTopPanelMouseDrag(int x, int y)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onTopPanelMouseUp
//
// Description: A tool's onTopPanelMouseUp event handler is called after
//              it is determined that mouse button was released while we
//              are in "drag" mode in the main window top panel.
//
// Arguments:   the client X and Y where the mouse was unclicked in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onTopPanelMouseUp(int x, int y)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlayerStart
//
// Description: A tool's onPlayerStart event handler is called before
//              the Player starts playing.
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onPlayerStart()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlayerStop
//
// Description: A tool's onPlayerStop event handler is called after
//              the Player is halted.  The tool can use the
//              onPlayerStop event to reset its state after processing
//              frames in an onRedraw method.
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onPlayerStop()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlaybackFilterChange
//
// Description: A tool's onPlaybacFilterChange event handler is called
//              whenever the playback filter in the Player is changed.
//              The tool can use the onPlaybackFilterChange event to reset
//              its state for subsequent processing.
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolObject::onPlaybackFilterChange(int filtr)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onChangeFrameView
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onChangeFrameView()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onToolCommand(CToolCommand &toolCommand)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolEvent
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onToolEvent(CToolEvent &toolEvent)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onHeartbeat
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onHeartbeat()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onFrameDiscardingOrCommitting
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message)
{
   message = "";
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onFrameWasDiscardedOrCommitted
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag, const std::pair<int, int> &frameRange)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onClipWasDeleted
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onClipWasDeleted(const string &deletedClipName)
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onSwitchSubtool
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onSwitchSubtool()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

//////////////////////////////////////////////////////////////////////
// Tool Information Query Functions
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    getToolName
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

const char* CToolObject::getToolName()
{
   return toolName.c_str();
}


string CToolObject::GetToolName()
{
   return toolName;
}

int CToolObject::GetToolNumber()
{
   return toolNumber;
}

bool CToolObject::IsLicensed()
{
   return toolLicensed;
}

bool CToolObject::IsDisabled()
{
   return toolDisabled;
}

bool CToolObject::IsActive()
{
   return toolActive;
}

EToolDisabledReason CToolObject::getToolDisabledReason()
{
   return toolDisabledReason;
}

// ===================================================================
//
// Function:    getSystemAPI
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
CToolSystemInterface* CToolObject::getSystemAPI() const
{
   return baseSystemAPI;
}

// ===================================================================
//
// Function:    SetRenderToNewClipFlag
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CToolObject::SetRenderToNewClipFlag(bool newval)
{
   renderToNewClipFlag = false; //newval;
}

// ===================================================================
//
// Function:    GetRenderToNewClipFlag
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
bool CToolObject::GetRenderToNewClipFlag()
{
   return renderToNewClipFlag;
}

// ===================================================================
//
// Function:    getCommandTable
//
// Description:
//
// Arguments:
//
// Returns:     Pointer to Tool's CToolCommandTable instance
//
// ===================================================================
const CToolCommandTable* CToolObject::getCommandTable()
{
   return commandTable;
}

// ===================================================================
//
// Function:    getUserInputMap
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
const CToolUserInputMap* CToolObject::getUserInputMap()
{
   return userInputMap;
}

// ===================================================================
//
// Function:    onCapturePDLEntry
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onCapturePDLEntry(CPDLElement &toolParams)
{
   return TOOL_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// ===================================================================
//
// Function:    onGoToPDLEntry
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   return TOOL_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// ===================================================================
//
// Function:    onPDLExecutionComplete
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onPDLExecutionComplete()
{
   return 0;
}

// ===================================================================
//
// Function:    OnStopPDLRendering
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::OnStopPDLRendering()
{
   return 0;
}


//////////////////////////////////////////////////////////////////////
// Protected User Input Mapping Functions
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    mapUserInput
//
// Description:
//
// Arguments:
//
// Returns:     Pointer to new CToolCommand instance
//              NULL pointer if key translation failed
//
// ===================================================================
CToolCommand* CToolObject::mapUserInput(CUserInput &userInput)
{
   int commandNumber = userInputMap->findCommand(userInput);

   if (commandNumber < 0)
      {
      // User Input does not match anything in the User Input Map Table
      // Return NULL pointer to indicate this
      return 0;
      }

   // Create a CToolCommand instance for the tool and command
   CToolCommand *toolCommand = new CToolCommand(this, commandNumber);

   // Set User Input information in the CToolCommand instance
   // TBD

   return toolCommand;
}

// ===================================================================
//
// Function:    mapAndExecuteUserInput
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::mapAndExecuteUserInput(CUserInput &userInput)
{
   int retVal;
   // Translate the User Input, via the User Input Command Table and
   // create a corresponding CToolCommand instance
   CToolCommand *toolCommand = mapUserInput(userInput);

   if (toolCommand == 0)
      {
      // User Input was not recognized by the tool
      retVal = IsInConsumeAllInputMode()? TOOL_RETURN_CODE_EVENT_CONSUMED
                                        : TOOL_RETURN_CODE_NO_ACTION;
      }
   else
      {
      // Call the tool's onToolCommand event handler to execute the Tool Command
      retVal = toolCommand->execute();

      // Delete the CToolCommand instance
      delete toolCommand;
      }

   return retVal;
}

bool CToolObject::IsInConsumeAllInputMode()
{
   return false;
}


// ===================================================================
//
// Function:    setDefaultUserInputConfiguration
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CToolObject::
setDefaultUserInputConfiguration(CUserInputConfiguration *newDefaultConfig)
{
   defaultUserInputConfig = newDefaultConfig;
}

// ===================================================================
//
// Function:    loadUserInputConfiguration
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::loadUserInputConfiguration()
{
   int retVal;

   // Use default user input configuration

   int configItemCount = defaultUserInputConfig->getConfigItemCount();
   CUserInput userInput;
   for (int i = 0; i < configItemCount; ++i)
      {
      const CUserInputConfiguration::SConfigItem *configItem;

      configItem = defaultUserInputConfig->getConfigItem(i);

      // TBD: get command/key mapping from .ini file
      // keyMapString
      // Since .ini file is not implemented yet, just use default
      const char* keyMapString = configItem->keyMap;

      retVal = userInput.parseIniUserInputMap(keyMapString);
      if (retVal != 0)
         {
         // Error parsing key mapping string
         return retVal;
         }

      userInputMap->addMapEntry(userInput, configItem->commandNumber);
      }

   return 0;
}

// ===================================================================
//
// Function:    initBaseToolObject
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::initBaseToolObject(int newToolNumber,
                                    CToolSystemInterface *newSystemAPI,
                                    CToolCommandTable *newCommandTable,
                                    CUserInputConfiguration *newDefaultConfig)
{
   // Save tool's identification
   toolNumber = newToolNumber;

   // Save the pointer to the System Interface object
   baseSystemAPI = newSystemAPI;

   commandTable = newCommandTable;

   // Establish initial keyboard and mouse button mapping
   defaultUserInputConfig = newDefaultConfig;
   int retVal = loadUserInputConfiguration();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolObject::shutdownBaseToolObject()
{
   return 0;
}

bool CToolObject::IsShowing(void)
{
   // This returns true if the tool is visible
   // Default is false
   return false;
}

bool CToolObject::AreMarksValid()
{
   int in = getSystemAPI()->getMarkIn();
   int out = getSystemAPI()->getMarkOut();
   return (in >= 0 && out > in);
}

bool CToolObject::DoesToolPreserveHistory()
{
   return true;  // Tools that do not preserve history must override this
}

// AWFUL HACK!! The "Mask Transform Editor" tries to refresh the displayed
// frame every time the mouse moves, even if no mask is being transformed.
// The reason is to produce a side effect where cursors in some obscure
// Paint mode will get updated. But this interferes with my debugging and
// profiling of other tools, so I implemented a way to turn off the
// refresh when the mask is not being manipulated
void CToolObject::SetMaskTransformEditorIdleRefreshMode()
{
   // Default is to NOT refresh the display when the mask is not being
   // transformed. THis needs to be overridden in Paint to set it to TRUE.
   getSystemAPI()->SetMaskTransformEditorIdleRefreshMode(false);
}

// This method gives the active tool a chance to clean up some state
// before a GOV actions starts (such as auto-accepting a fix), and also
// allows the active tool to prevent the GOV operation by returning false
//
bool CToolObject::NotifyGOVStarting(bool *cancelStretch)
{
   // By default, only refuse if the tool is actually running
   bool retVal = (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED);

   return retVal;
}

// This method gives the active tool a chance to restore some state
// after the GOV operation has completed
//
void CToolObject::NotifyGOVDone()
{
   // Default action is to do nothing.
}

void CToolObject::NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                         const RECT &rect, const POINT *lasso)
{
   // Default action is to do nothing.
}

// This method gives the active tool a chance to react to a right click
// (passed on by GOV tool if right click with no mouse movement)
//
void CToolObject::NotifyRightClicked()
{
   // Default action is to do nothing.
}

bool CToolObject::NotifyTrackingStarting()
{
   // By default, tell the tracked "no go"
   return false;
}

void CToolObject::NotifyTrackingDone(int errorCode)
{
   // Default action is to do nothing.
}

void CToolObject::NotifyTrackingCleared()
{
   // Default action is to do nothing.
}

bool CToolObject::NotifyStartROIMode()
{
   // Default action is to do nothing and say it's OK to start ROI mode.
   return true;
}

bool CToolObject::NotifyUserDrewROI()
{
   // Default action is to do nothing and say it was not hendled.
   return false;
}

void CToolObject::NotifyEndROIMode()
{
	// Default action is to do nothing.
}

void CToolObject::NotifyMaskChanged(const string &newMaskAsString)
{
	// Default action is to do nothing.
}

void CToolObject::NotifyMaskVisibilityChanged()
{
	// Default action is to do nothing.
}

void CToolObject::NotifyStartViewOnlyMode()
{
	setViewOnlyMode(true);

	// isItOkToUseToolNow is poorly named, since it has side effects of
	// setting toolDisabled flag and the tool disable reason.
	if (!isItOkToUseToolNow())
	{
		getSystemAPI()-> DisableTool(getToolDisabledReason());
	}
}

void CToolObject::NotifyEndViewOnlyMode()
{
	setViewOnlyMode(false);

	// isItOkToUseToolNow is poorly named, since it has side effects of
	// setting toolDisabled flag and the tool disable reason.
	if (isItOkToUseToolNow())
	{
		getSystemAPI()-> EnableTool();
	}
	else
	{
		getSystemAPI()-> DisableTool(getToolDisabledReason());
	}
}

// ===================================================================
//
// Function:    MonitorFrameProcessing
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
                                        const CAutotoolStatus& newToolStatus,
                                        int newResumeFrame)
{
   int retVal = 0;
   EAutotoolStatus newToolProcessingStatus = newToolStatus.status;

   EToolControlState nextToolControlState;

   // Tool Processing Control State Machine
   nextToolControlState = toolProcessingControlState;
   switch(toolProcessingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
            {
            toolProcessingErrorCode = 0;      // clear any old errors
            renderFlag = false;
            retVal = ProcessTrainingFrame();
            if (retVal == 0)
               {
               nextToolControlState
                           = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED;
               }
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW)
            {
            toolProcessingErrorCode = 0;      // clear any old errors
            renderFlag = false;
            retVal = RunFramePreviewing(newResumeFrame);
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
            {
            toolProcessingErrorCode = 0;      // clear any old errors
            renderFlag = true;
            retVal = RenderSingleFrame();
            if (retVal == 0)
               {
               nextToolControlState
                           = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED;
               }
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER)
            {
            toolProcessingErrorCode = 0;      // clear any old errors
            renderFlag = true;
            retVal = RunFrameProcessing(newResumeFrame);
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_PREPROCESS)
            {
            toolProcessingErrorCode = 0;      // clear any old errors
            renderFlag = false;
            retVal = RunFramePreprocessing();
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            else
               {
               // TBD: Error Handling
               }
            }

         break;
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         if (newToolProcessingStatus == AT_STATUS_STOPPED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING :
         if (newToolProcessingStatus == AT_STATUS_PROCESS_ERROR)
            {
            toolProcessingErrorCode = newToolStatus.errorCode;

            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (newToolProcessingStatus == AT_STATUS_STOPPED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
            {
            retVal = StopFrameProcessing();
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_PAUSE)
            {
            retVal = PauseFrameProcessing();
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
            else
               {
               // TBD: Error Handling
               }
            }
         break;
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
         if (newToolProcessingStatus == AT_STATUS_STOPPED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         else if (newToolProcessingStatus == AT_STATUS_PAUSED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
            {
            retVal = StopFrameProcessing();
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            else
               {
               // TBD: Error Handling
               }
            }
         break;
      case TOOL_CONTROL_STATE_PAUSED :
         if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
            {
            retVal = StopFrameProcessing();
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_CONTINUE)
            {
            retVal = RunFrameProcessing(newResumeFrame);
            if (retVal == 0)
               nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
            {
            retVal = ProcessTrainingFrame();
            if (retVal == 0)
               {
               nextToolControlState
                            = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED;
               }
            else
               {
               // TBD: Error Handling
               }
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
            {
            retVal = RenderSingleFrame();
            if (retVal == 0)
               {
               nextToolControlState
                            = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED;
               }
            else
               {
               // TBD: Error Handling
               }
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
         if (newToolProcessingStatus == AT_STATUS_STOPPED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
            {
            StopSingleFrameProcessing();
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
         if (newToolProcessingStatus == AT_STATUS_STOPPED)
            {
            nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
            }
         else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
            {
            StopSingleFrameProcessing();
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
		 break;
	  default:   ////JAM
		 break;
      }

   if (toolProcessingControlState != nextToolControlState)
      {
      // Control State has changed

      toolProcessingControlState = nextToolControlState;

      UpdateExecutionGUI(toolProcessingControlState, renderFlag);

      // Make sure the Processed/Original status bar panel is shown
      // correctly right after processing if the provisional is pending
      if (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
         getSystemAPI()->UpdateProvisionalStatusBarPanel();

      }

   return retVal;

} // MonitorFrameProcessing

// These functions can be overridden by the actual tool

int CToolObject::PauseFrameProcessing()
{
   getSystemAPI()->PauseActiveToolSetup();

   return 0;
}

int CToolObject::PreloadInputFrames()
{
   return ProcessSingleFrame(TOOL_SETUP_TYPE_PRELOAD_ONLY);
}

int CToolObject::ProcessTrainingFrame()// deprecated - use PreviewSingleFrame()
{
   return PreviewSingleFrame();
}

int CToolObject::PreviewSingleFrame()
{
   return ProcessSingleFrame(TOOL_SETUP_TYPE_TRAINING);
}

int CToolObject::RenderSingleFrame()
{
   return ProcessSingleFrame(TOOL_SETUP_TYPE_SINGLE_FRAME);
}

int CToolObject::ProcessSingleFrame(EToolSetupType toolSetupType)
{
   return 0;
}

int CToolObject::RunFramePreviewing(int newResumeFrame)
{
   return RunFrameProcessing(newResumeFrame);
}

int CToolObject::RunFrameProcessing(int newResumeFrame)
{
   return 0;
}

int CToolObject::RunFramePreprocessing()
{
   return 0;
}

int CToolObject::StopFrameProcessing()
{
   getSystemAPI()->StopActiveToolSetup();

   return 0;
}

int CToolObject::StopSingleFrameProcessing()
{
   getSystemAPI()->StopActiveToolSetup();

   return 0;
}

void CToolObject::UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                                     bool processingRenderFlag)
{
}

//////////////////////////////////////////////////////////////////////
// Execution Control GUI Interface for Tool Processing
//
// The following functions are called by the Tool's GUI to control
// tool processing.  Details for each function are in the function's
// header comments.
//     ContinueToolProcessing
//     PauseToolProcessing
//     StartToolProcessing
//     StopToolProcessing
//
// These functions may be overridden by the CToolObject's derived
// type, but it is not necessary for simple controls.
//
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    ContinueToolProcessing
//
// Description:
//
// Arguments:
//
// Returns:     None
//
// ===================================================================
int CToolObject::ContinueToolProcessing(int newResumeFrame)
{
   int retVal;
   CAutotoolStatus autoToolStatus;

   GetToolProgressMonitor()->PopStatusMessage();
   GetToolProgressMonitor()->StartProgress();

   retVal = MonitorFrameProcessing(TOOL_PROCESSING_CMD_CONTINUE,
                                   autoToolStatus, newResumeFrame);

   return retVal;
}

// ===================================================================
//
// Function:    PauseToolProcessing
//
// Description:
//
// Arguments:   None
//
// Returns:     None
//
// ===================================================================
int CToolObject::PauseToolProcessing()
{
   int retVal;
   CAutotoolStatus autoToolStatus;

   GetToolProgressMonitor()->PushStatusMessage("Paused");
   GetToolProgressMonitor()->StopProgress(false);

   retVal = MonitorFrameProcessing(TOOL_PROCESSING_CMD_PAUSE, autoToolStatus,
                                  -1);

   return retVal;
}

// ===================================================================
//
// Function:    StartToolProcessing
//
// Description:
//
// Arguments:
//
// Returns:     None
//
// ===================================================================
int CToolObject::StartToolProcessing(EToolProcessingCommand processingType,
                                     bool newHighlightFlag)
{
   int retVal = 1;

   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   // See if we need to warn about rendering with "no history" tools
   if (!DoesToolPreserveHistory())
      {
      switch (processingType)
         {
         case TOOL_PROCESSING_CMD_RENDER :
         case TOOL_PROCESSING_CMD_RENDER_1:
            // Warning is a "system call" so the base tool window can
			// "own" the dialog that's used for the warning
			retVal = getSystemAPI()->WarnThereIsNoUndo() ? 0 : 1;
			if (retVal == 1)
               return retVal;   // Nobody looks at the return code
            break;

         default:
            break;
         }
      }

   highlightFlag = newHighlightFlag;

   CAutotoolStatus autoToolStatus;
   return MonitorFrameProcessing(processingType, autoToolStatus, -1);

   return retVal;
}

// ===================================================================
//
// Function:    StopToolProcessing
//
// Description:
//
// Arguments:   None
//
// Returns:     None
//
// ===================================================================
int CToolObject::StopToolProcessing()
{
   int retVal;
   CAutotoolStatus autoToolStatus;

   GetToolProgressMonitor()->SetStatusMessage("Stopped");
   GetToolProgressMonitor()->StopProgress(false);

   retVal = MonitorFrameProcessing(TOOL_PROCESSING_CMD_STOP, autoToolStatus,
                                  -1);

   return retVal;
}

// ===================================================================
//
// Function:    GetToolProcessingControlState
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

void CToolObject::SetToolProcessingControlState(EToolControlState newControlState)
{
   toolProcessingControlState = newControlState;
}

EToolControlState CToolObject::GetToolProcessingControlState()
{
   return toolProcessingControlState;
}

int CToolObject::GetToolProcessingErrorCode()
{
   return toolProcessingErrorCode;
}

bool CToolObject::GetToolProcessingRenderFlag()
{
   return renderFlag;
}

int CToolObject::TryToResolveProvisional()
{
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolProcessingStatus
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolObject::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   int retVal;

   retVal = MonitorFrameProcessing(TOOL_PROCESSING_CMD_INVALID, autotoolStatus,
                                   -1);
   if (retVal != 0)
      return retVal;    // ERROR

	return 0;
}

//////////////////////////////////////////////////////////////////////

void CToolObject::setViewOnlyMode(bool flag)
{
	bool *sharedFlagPtr = (bool *) SharedMap::retrieve(InViewOnlyModeKey);
	if (sharedFlagPtr == nullptr)
	{
		sharedFlagPtr = new bool[1];
		SharedMap::add(InViewOnlyModeKey, sharedFlagPtr);
	}

	*sharedFlagPtr = flag;
}

//////////////////////////////////////////////////////////////////////

bool CToolObject::IsInViewOnlyMode()
{
	bool *sharedFlagPtr = (bool *) SharedMap::retrieve(InViewOnlyModeKey);
	return sharedFlagPtr ? *sharedFlagPtr : false;
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###     CToolProcessor Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolProcessor::CToolProcessor(int newToolNumber, const string &newToolName,
                               int newInPortCount, int newOutPortCount,
                               CToolSystemInterface *newSystemAPI,
                               const bool *newEmergencyStopFlagPtr,
                               IToolProgressMonitor *newToolProgressMonitor)
 : toolNumber(newToolNumber), toolName(newToolName),
   node(new CToolNode(this, newInPortCount, newOutPortCount)),
   systemAPI(newSystemAPI), emergencyStopFlagPtr(newEmergencyStopFlagPtr),
   toolProgressMonitor(newToolProgressMonitor),
   toolCommandQueue(new AutotoolCommandQueue(0)),
   toolStatusQueue(new AutotoolStatusQueue(0)),
   vpProcessThread(0)
{
   // Create Input and Output Port Lists
   InitializePortDefs();
}

CToolProcessor::~CToolProcessor()
{
   delete toolCommandQueue;
   delete toolStatusQueue;

   delete node;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CToolSystemInterface* CToolProcessor::GetSystemAPI()
{
   return systemAPI;
}

//////////////////////////////////////////////////////////////////////
// Inter-thread Communication
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    PutToolCommand
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolProcessor::PutToolCommand(CAutotoolCommand &command)
{
   toolCommandQueue->Add(command);

   return 0;
}


int CToolProcessor::GetToolCommand(CAutotoolCommand &command)
{
   if (toolCommandQueue->Empty())
      return TOOL_ERROR_TOOL_COMMAND_QUEUE_EMPTY;

   // Get oldest entry from command queue and return it to caller
   command = toolCommandQueue->Take();

   return 0;
}

// ===================================================================
//
// Function:    GetToolStatus
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolProcessor::GetToolStatus(CAutotoolStatus &status)
{
   int retVal = TOOL_ERROR_TOOL_STATUS_QUEUE_EMPTY;

   if (!toolStatusQueue->Empty())
   {
      // frickin mertus ring buffer is not designed to handle more than
      // one consumer thread
      try {

         // Get oldest entry from the status queue and return it to the caller
         status = toolStatusQueue->Take();
         retVal = 0; // success!

      }
      catch (...)
      {
         // FAIL
      }
   }

   return retVal;
}

int CToolProcessor::PutToolStatus(CAutotoolStatus &status)
{
   toolStatusQueue->Add(status);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Get and Put Frame Buffers
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get the next available frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolProcessor::GetFrame(int port)
{
   CToolFrameBuffer *frameBuffer;

   frameBuffer = node->GetFrame(port);

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    GetFrame
//
// Description: Get a specific frame for a particular tool input port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CToolFrameBuffer* CToolProcessor::GetFrame(int port, int frameIndex)
{
   CToolFrameBuffer *frameBuffer;

   frameBuffer = node->GetFrame(port, frameIndex);

   return frameBuffer;
}

//------------------------------------------------------------------------
//
// Function:    PutFrame
//
// Description: Put a frame into a tool's output port
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
bool CToolProcessor::PutFrame(int port, CToolFrameBuffer *frameBuffer)
{
   return node->PutFrame(port, frameBuffer);
}

bool CToolProcessor::IsOutPortFull(int port)
{
   return node->IsOutPortFull(port);
}

//------------------------------------------------------------------------
// These are hacks to bypass a writer tool's regular tool processor and
// instead simply write a single frame synchronously or asynchronously
//------------------------------------------------------------------------

int CToolProcessor::PutFrameSynchronous(int outPortIndex,
                                        CToolFrameBuffer *frameBuffer)
{
   return node->PutFrameSynchronous(outPortIndex, frameBuffer);
}

int CToolProcessor::WriteFrameSynchronous(int inPortIndex,
                                          CToolFrameBuffer *frameBuffer)
{
   return -1;
}

int CToolProcessor::PutFrameAsynchronous(int outPortIndex,
                                         CToolFrameBuffer *frameBuffer)
{
   return node->PutFrameAsynchronous(outPortIndex, frameBuffer);
}

int CToolProcessor::WriteFrameAsynchronous(int inPortIndex,
                                           CToolFrameBuffer *frameBuffer)
{
   return -1;
}

//------------------------------------------------------------------------
// Tools that modify a copy of the input frame should use this to copy
// the virgin bits to initialize the output buffer... it produces the
// side effect that the original bits will be marked as uncacheable
// because we want to make sure that the output frame is the one that
// ends up in the cache when we're done!
// NOTE: Theoretically it is impossible for us to spin forever here
//       because the tool should have "reserved" enough buffers so that
//       will never happen!
//------------------------------------------------------------------------

void CToolProcessor::CopyInputFrameBufferToOutputFrameBuffer(
            CToolFrameBuffer* inToolFrameBuffer,
            CToolFrameBuffer* outToolFrameBuffer)
{
	CHRTimer watchDog;
	int watchDogMinutes = 0;

   // Copy the original image, including invisible fields, to the output frame
	while(!outToolFrameBuffer->CopyImage(*inToolFrameBuffer, true, true))
   {
      // Couldn't allocate a buffer to hold the output bits...
      // let's try freeing up a cached tool frame
      CToolFrameBuffer *tempFrameBuffer =
            GetSystemAPI()->GetToolFrameCache()->GetOldestFrame();
      if (tempFrameBuffer != NULL)
      {
         delete tempFrameBuffer;
      }
      else
      {
			int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
			if (elapsedMinutes > watchDogMinutes)
			{
				watchDogMinutes = elapsedMinutes;
				TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in CopyInputFrameBufferToOutputFrameBuffer()"
									<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
			}

			// No frames available in the cache...
			// wait for a buffer to become available
         MTImillisleep(1);
      }
   }

   // SIDE EFFECT: mark old buffer as uncacheable!
   inToolFrameBuffer->SetDontCache(true);
}



//////////////////////////////////////////////////////////////////////
// Core Image Processing Functions
//////////////////////////////////////////////////////////////////////

int CToolProcessor::StartProcessThread()
{
   bool spawned = false;
   if (vpProcessThread == 0)
   {
      spawned = true;
      vpProcessThread = BThreadSpawn(ProcessThread, this);
   }

#ifdef _DEBUG
   void *temp = this;
   TRACE_3(errout << "TOOL PROCESSOR 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << (*(size_t *)(&temp)) << (spawned ? " spawned thread " : " reused thread ") << _threadid);
#endif

   return 0;
}

// ===================================================================
//
// Function:    ProcessThread
//
// Description:
//              Run this function within a new thread by calling
//                void *bThreadStruct = BThreadSpawn(ProcessThread, this)
//              from a CToolProcessor member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CToolProcessor::ProcessThread(void *vpToolProc, void *vpBThread)
{
   CToolProcessor *tp = static_cast<CToolProcessor*>(vpToolProc);
   tp->ProcessLoop(vpBThread);
}

// ===================================================================
//
// Function:     ProcessLoop
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CToolProcessor::ProcessLoop(void *vpBThread)
{
   bool procActivity = false;
   int sleepTime= TOOL_PROC_LONG_SLEEP_TIME;
   bool running = false;
   bool processingActive = true;
   CToolParameters *newToolParams = 0;
   bool endOfMaterial;
   int processingError;
   CAutotoolCommand toolCommand;
   EAutotoolCommand newToolCommand;
   CAutotoolStatus toolStatus;
   EToolControlState controlState, newControlState;
   EToolProcessState processState, newProcessState;

   controlState = TOOL_CONTROL_STATE_STOPPED;

   BThreadBegin(vpBThread);

   // Post thread-started status
   toolStatus.status = AT_STATUS_PROCESS_THREAD_STARTED;
   PutToolStatus(toolStatus);

	SToolProcessingData procData;
	CHRTimer watchDog;
	int watchDogMinutes = 0;

   // Initial control state
   controlState = TOOL_CONTROL_STATE_STOPPED;

   while (processingActive)
      {
      procActivity = false;

      // Get and Process Tool Commands
      if (GetToolCommand(toolCommand) == 0)
         {
         newToolCommand = toolCommand.command;
         }
      else
         newToolCommand = AT_CMD_INVALID;

      // Tool Control State Machine
      newControlState = controlState;
	  switch (controlState)
         {
         case TOOL_CONTROL_STATE_STOPPED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (newToolCommand == AT_CMD_RUN && !IsEmergencyStop())
               {
               endOfMaterial = false;
               processingError = 0;
               InitProcessingData(procData);
               // Tell tool that we're about to start
               if (newToolParams != 0)
                  {
                  // New parameters to set before beginning processsing
                  processingError = SetParameters(newToolParams);
                  delete newToolParams;
                  newToolParams = 0;
                  }
               if (processingError == 0)
                  {
                  SetSaveToHistory(true); // default setting to save to history
                  processingError = BeginProcessing(procData);
                  }
               if (processingError == 0)
                  {
                  // Move to the running state if no errors
                  processState = TOOL_PROCESS_STATE_SETUP;
                  newControlState = TOOL_CONTROL_STATE_RUNNING;
                  }
               else
                  {
                  // An error was returned by either SetParameters or
                  // BeginProcessing
                  TRACE_0(errout << "ERROR: CToolProcessor::ProcessLoop Startup Error "
                                 << processingError);
                  toolStatus.status = AT_STATUS_PROCESS_ERROR;
                  toolStatus.errorCode = processingError;
                  PutToolStatus(toolStatus);

                  // Push stopped status onto status queue so that everything
                  // shuts down properly
                  toolStatus.errorCode = 0;
                  toolStatus.status = AT_STATUS_STOPPED;
                  PutToolStatus(toolStatus);
                  }
               }
            else if (newToolCommand == AT_CMD_SET_PARAMETERS)
               {
               if (newToolParams != 0)
                  delete newToolParams;
               newToolParams = toolCommand.toolParams;
               }
            break;
         case TOOL_CONTROL_STATE_RUNNING :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (IsEmergencyStop())
               {
               FlushDstFrameQueues();
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (processingError != 0)
               {
               // An error was returned by one of the tool's functions
               TRACE_0(errout << "ERROR: CToolProcessor::ProcessLoop Processing Error "
                              << processingError);
               toolStatus.status = AT_STATUS_PROCESS_ERROR;
               toolStatus.errorCode = processingError;
               PutToolStatus(toolStatus);
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (endOfMaterial)
               {
               toolStatus.status = AT_STATUS_END_OF_MATERIAL;
               PutToolStatus(toolStatus);
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               TRACE_3(errout << " <<<<<<< in ToolObject: EOM -> STOPPED >>>>>>>");
               }
            else if (newToolCommand == AT_CMD_STOP)
               {
               FlushDstFrameQueues();
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (newToolCommand == AT_CMD_PAUSE)
               {
               newControlState = TOOL_CONTROL_STATE_PAUSED;
               }
            else if (newToolCommand == AT_CMD_SET_PARAMETERS)
               {
               if (newToolParams != 0)
                  delete newToolParams;
               newToolParams = toolCommand.toolParams;
               }
            break;
         case TOOL_CONTROL_STATE_PAUSED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (newToolCommand == AT_CMD_STOP || IsEmergencyStop())
               {
               FlushDstFrameQueues();
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (newToolCommand == AT_CMD_RUN && !IsEmergencyStop())
               {
               endOfMaterial = false;
               processingError = 0;
               InitProcessingData(procData);
               // Tell tool that we're about to start
               if (newToolParams != 0)
                  {
                  // New parameters to set before beginning processsing
                  processingError = SetParameters(newToolParams);
                  delete newToolParams;
                  newToolParams = 0;
                  }
               if (processingError == 0)
                  {
                  // Move to the running state if no errors
                  processState = TOOL_PROCESS_STATE_SETUP;
                  newControlState = TOOL_CONTROL_STATE_RUNNING;
                  }
               else
                  {
                  // An error was returned by either SetParameters or
                  // BeginProcessing
                  TRACE_0(errout << "ERROR: CToolProcessor::ProcessLoop Resume Error "
                                 << processingError);
                  toolStatus.status = AT_STATUS_PROCESS_ERROR;
                  toolStatus.errorCode = processingError;
                  PutToolStatus(toolStatus);
                  newControlState = TOOL_CONTROL_STATE_STOPPED;
                  }
               }
            else if (newToolCommand == AT_CMD_SET_PARAMETERS)
               {
               if (newToolParams != 0)
                  delete newToolParams;
               newToolParams = toolCommand.toolParams;
               }
			break;
			default:   ////JAM
				break;
         };

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  if (controlState != newControlState)
         {
         // The control state has changed, so perform any actions
         // that are common to the entry of the new control state
         toolStatus.errorCode = 0;
		 switch (newControlState)
            {
            case TOOL_CONTROL_STATE_STOPPED :
               processingError = EndProcessing(procData);
               if (processingError != 0)
                  {
                  toolStatus.status = AT_STATUS_PROCESS_ERROR;
                  toolStatus.errorCode = processingError;
                  PutToolStatus(toolStatus);
                  }
               FlushBuffers();
               running = false;
               sleepTime = TOOL_PROC_LONG_SLEEP_TIME;
               toolStatus.status = AT_STATUS_STOPPED;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_RUNNING :
               running = true;
               sleepTime = TOOL_PROC_SHORT_SLEEP_TIME;
               toolStatus.status = AT_STATUS_RUNNING;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_PAUSED :
               FlushDstFrameQueues();
               FlushBuffers();
               running = false;
               sleepTime = TOOL_PROC_LONG_SLEEP_TIME;
               toolStatus.status = AT_STATUS_PAUSED;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_EXITING_THREAD :
               FlushBuffers();
               FlushDstFrameQueues();
               running = false;
               processingActive = false;
               procActivity = true;
			   break;

			default:   ////JAM
				break;
			};
         }
      controlState = newControlState;

      if (running)
         {
         // Processing State Machine
         //
         // This state machine is here so that we can stop or pause
         // processing without having to wait for all processing and
         // housekeeping to be completed on the current frame to be
         // completed.
         //
         // NB: Potentially we could get stuck in some of the processing
         //     states if the previous stage never supplies the requested
         //     frames. This should not be a problem in practice.  A timeout
         //     here would give us a failsafe, but how long to wait?
         //     Too short would interfere with slow processing.

         newProcessState = processState;
         switch(processState)
            {
            case TOOL_PROCESS_STATE_SETUP :
			   // Begin this processing iteration
			   if (procData.iteration < GetIterationCount(procData))
				  {
				  // More to do
				  processingError = 0;
				  PrepareProcessingData(procData);
				  if (processingError == 0 && newToolParams != 0)
					 {
					 // New tool parameters to set before this iteration
					 processingError = SetParameters(newToolParams);
					 delete newToolParams;
					 newToolParams = 0;
					 }
				  if (processingError == 0)
					 {
					 processingError = BeginIteration(procData);
					 newProcessState = TOOL_PROCESS_STATE_GET_INPUT;
					 watchDog.Start();
					 watchDogMinutes = 0;
					 }
				  }
			   else
				  {
				  // We're done
                  GetEndOfMaterialFrames();
                  PutEndOfMaterialFrames();
                  endOfMaterial = true;
                  }
               break;

				case TOOL_PROCESS_STATE_GET_INPUT :
               // Get the input frames requested by the tool.  GetInput returns
               // true when we have all of the requested frame.  We stay
               // in this state until we have all requested frames.
               if (GetInput(&procActivity))
                  {
                  newProcessState = TOOL_PROCESS_STATE_SET_OUTPUT;
						watchDog.Start();
						watchDogMinutes = 0;
						}
					else
						{
						int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
						if (elapsedMinutes > watchDogMinutes)
							{
							watchDogMinutes = elapsedMinutes;
							TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in state TOOL_PROCESS_STATE_GET_INPUT"
												<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
							}
						}
					break;

				case TOOL_PROCESS_STATE_SET_OUTPUT :
               // Set the output buffers, either to input buffers if
               // modify-in-place or by allocating new buffers.
               if (SetOutput(&procActivity))
                  {
                  newProcessState = TOOL_PROCESS_STATE_PROCESSING;
						watchDog.Start();
						watchDogMinutes = 0;
						}
					else
						{
						int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
						if (elapsedMinutes > watchDogMinutes)
							{
							watchDogMinutes = elapsedMinutes;
							TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in state TOOL_PROCESS_STATE_SET_OUTPUT"
												<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
							}
						}
					break;

				case TOOL_PROCESS_STATE_PROCESSING :
					// Process the frames
					//DBLINE;
					processingError = DoProcess(procData);
//               DBTRACE(watchDog.ReadAsString());
					procActivity = true;
               if (procData.frameDoneFlag)
                  {
                  // DoProcess is done with this iteration, so we can
                  // move on
                  newProcessState = TOOL_PROCESS_STATE_RELEASE_BUFFERS;
						watchDog.Start();
						watchDogMinutes = 0;
						}
					else
						{
						int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
						if (elapsedMinutes > watchDogMinutes)
							{
							watchDogMinutes = elapsedMinutes;
							TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in state TOOL_PROCESS_STATE_PROCESSING"
												<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
							}
						}
					break;

				case TOOL_PROCESS_STATE_RELEASE_BUFFERS :
               // Release buffers if necessary
               if (ReleaseBuffers(&procActivity))
                  {
						newProcessState = TOOL_PROCESS_STATE_PUT_OUTPUT;
						watchDog.Start();
						watchDogMinutes = 0;
						}
					else
						{
						int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
						if (elapsedMinutes > watchDogMinutes)
							{
							watchDogMinutes = elapsedMinutes;
							TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in state TOOL_PROCESS_STATE_RELEASE_BUFFERS"
												<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
							}
						}
					break;

				case TOOL_PROCESS_STATE_PUT_OUTPUT :
               // Put the output
               if (PutOutput(&procActivity))
                  {
                  newProcessState = TOOL_PROCESS_STATE_CLEANUP;
						watchDog.Start();
						watchDogMinutes = 0;
						}
					else
						{
						int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
						if (elapsedMinutes > watchDogMinutes)
							{
							watchDogMinutes = elapsedMinutes;
							TRACE_0(errout << "WATCHDOG: Tool processor appears to be hung in state TOOL_PROCESS_STATE_PUT_OUTPUT"
												<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
							}
						}
					break;

            case TOOL_PROCESS_STATE_CLEANUP :
               // Finish this processing iteration
               processingError = EndIteration(procData);
               Application->ProcessMessages(); // might fix SAN lockout problem?
               procActivity = false; // true;    Allow GUI to update
               procData.iteration += 1;    // Advance iteration number
               newProcessState = TOOL_PROCESS_STATE_SETUP; // Do it all again
               break;

            } // End of "switch (processState)"

         processState = newProcessState;

         } // End of "if (running)"

      // If nothing was done on this pass through processing loop,
      // then sleep for a bit so we don't hog the processor.  If processing
      // is running then sleep is for a short time.  If stopped or paused
      // then sleep for a long time (sleeping too long will delay response
      // to run command, which makes single frame tools less snappy)
      if (!procActivity)
         MTImillisleep(sleepTime);

      } // End of "while (processingActive)"

   // Post thread-ending status
   toolStatus.status = AT_STATUS_PROCESS_THREAD_ENDING;
   PutToolStatus(toolStatus);
} // ProcessLoop

void CToolProcessor::InitProcessingData(SToolProcessingData &procData)
{
   procData.iteration = 0;

   procData.emergencyStopFlagPtr = GetEmergencyStopFlagPtr();
}

void CToolProcessor::PrepareProcessingData(SToolProcessingData &procData)
{
   // Iterate over each input port and clear various lists
   int inPortCount = GetInPortCount();
   for (int inPortIndex = 0; inPortIndex < inPortCount; ++inPortIndex)
      {
      // For coding convenience and readability, make some local references.
      SInPort &inPort = inPortList[inPortIndex];

      inPort.requestList.clear();
      inPort.releaseList.clear();
      inPort.inFrameList.clear();
      }

   // Iterate over each output port and clear various lists
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      SOutPort &outPort = outPortList[outPortIndex];

      outPort.requestList.clear();
      outPort.outFrameList.clear();
      }

   // Assume that when DoProcess returns it has completed this iteration.
	// DoProcess may set this flag to false to indicate that wants to continue
   // processing on this particular iteration.  This allows very slow
   // processing to return control to ProcessLoop every once it a while
   // so that we can check for pause and stop commands.  Once DoProcess
   // sets this to false, it is responsible for setting to true again.
	procData.frameDoneFlag = true;
}

// ===================================================================
//
// Function:     GetInput
//
// Description:  Get all of the input frames requested by the tool.
//               This functions is called, potentially repeatedly,
//               on each iteration of Processing Loop, until all of
//               the requested frames are obtained.
//
// Arguments:    bool *activity     Pointer to caller's activity flag
//                                  Set to false if no new frames were
//                                  obtained on this call.  Set to true
//                                  if one or more frames were obtained.
//
// Returns:
//
// ===================================================================
bool CToolProcessor::GetInput(bool *activity)
{
   bool foundAll = true;   // be optimistic
   bool foundSome;
   *activity = false;      // assume we will accomplish nothing

   // Iterate over each input port
   int inPortCount = GetInPortCount();
   for (int inPortIndex = 0; inPortIndex < inPortCount; ++inPortIndex)
      {
      foundSome = GetInputFromInPort(inPortIndex, activity);
      if (!foundSome)
         foundAll = false;
      }

   return foundAll;  // this will be true when all of the requested frames
                     // are now available
} // GetInput

bool CToolProcessor::GetInputFromInPort(int inPortIndex, bool *activity)
{
   bool foundAll = true;
   CToolFrameBuffer *frameBuffer;
   SInPort &inPort = inPortList[inPortIndex];

   // Iterate over the list of frames requested from this input port
   int requestCount = (int)inPort.requestList.size();
   for (int requestIndex = 0; requestIndex < requestCount; ++requestIndex)
      {
      if (inPort.inFrameList[requestIndex] != 0)
         continue;   // Got the requested frame already, skip to next request

      int requestedFrameIndex = inPort.requestList[requestIndex];

      frameBuffer = GetAvailableFrame(inPort, requestedFrameIndex);
      if (frameBuffer != 0)
         {
         inPort.inFrameList[requestIndex] = frameBuffer;
         *activity = true;
         }
      else
         {
         // The frame was not in the available frame list, so get the
         // requested frame from the input port's frame queue.  This is
         // a non-blocking call and it will return NULL if the frame is not
         // in the queue yet.
         frameBuffer = GetFrame(inPortIndex, requestedFrameIndex);
         if (frameBuffer != 0)
            {
            *activity = true;
            // Add to list of available frames
            AddAvailableFrame(inPort, frameBuffer);
            }
         }

      inPort.inFrameList[requestIndex] = frameBuffer;
      if (frameBuffer == 0)
         foundAll = false;   // Did not get the requested frame yet
      }

   return foundAll;

} // GetInputFromInPort

void CToolProcessor::AddAvailableFrame(SInPort &inPort,
                                       CToolFrameBuffer *frameBuffer)
{
   inPort.availableFrames.push_back(frameBuffer);
}

CToolFrameBuffer* CToolProcessor::GetAvailableFrame(SInPort &inPort,
                                                    int frameIndex)
{
   // Check the already available frames
   list<CToolFrameBuffer*>::iterator iterator;
   for (iterator = inPort.availableFrames.begin();
        iterator != inPort.availableFrames.end();
        ++iterator)
      {
      if ((*iterator)->GetFrameIndex() == frameIndex)
         return *iterator; // Found it
      }

   // If we reach this point, then the frame was not found
   return 0;
}

int CToolProcessor::GetEndOfMaterialFrames()
{
   // Iterate over each input port
   int inPortCount = GetInPortCount();
   for (int inPortIndex = 0; inPortIndex < inPortCount; ++inPortIndex)
      {
      // Ask for frame with index of -2000.  This is impossible, so GetFrame
      // will just return a NULL pointer.  If the input is from a Media
      // Reader in Random Access mode, then the -2000 will be interpreted
      // as "End of Material" request.
      GetFrame(inPortIndex, END_OF_MATERIAL_FRAME_INDEX);
      }
   return 0;
}

// ===================================================================
//
// Function:    SetOutput
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
bool CToolProcessor::SetOutput(bool *activity)
{
   bool outputSetDone = true;
   *activity = false;
   CToolFrameBuffer *frameBuffer;

   // Iterate over each output port
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      // For coding convenience and readability, make some local references.
      SOutPort &outPort = outPortList[outPortIndex];

      if (outPort.modifyInPlace)
         {
         // Modify-in-place flag indicates that an input frame
         // will be modified and used as the output frame.  To do this
         // copy the requested frame pointer(s) from the input port's
         // inFrameList to the output port's outFrameList
         int modifyInPlaceCount = outPort.requestList.size();
         for (int i = 0; i < modifyInPlaceCount; ++i)
            {
            frameBuffer = GetInputFrame(outPort.inPortIndex,
                                        outPort.requestList[i]);
            outPort.outFrameList[i] = frameBuffer;
            }
         }
      else
         {
         // Output will go to a new frame buffer that must be created
         int newFrameCount = outPort.requestList.size();
         for (int j = 0; j < newFrameCount; ++j)
            {
            if (outPort.outFrameList[j] == 0)
               {
               outPort.outFrameList[j]
                                   = new CToolFrameBuffer(outPort.imageFormat,
                                                       outPort.bufferAllocator);
               outPort.outFrameList[j]->SetFrameIndex(outPort.requestList[j]);
               }
				if (outPort.outFrameList[j]->AllocateAllFrameBuffers() != 0)
               {
               // Free up a cached tool frame buffer just in case we've really
               // run out, then try again
               CToolFrameBuffer *tempFrameBuffer =
                     GetSystemAPI()->GetToolFrameCache()->GetOldestFrame();
               delete tempFrameBuffer;
					if (outPort.outFrameList[j]->AllocateAllFrameBuffers() != 0)
                  {
                  // Could not allocate all the frame buffers.  Skip it
                  // and try again later
                  outputSetDone = false;
                  break;
                  }
               }
            }
         }

      }
   return outputSetDone;

} // SetOutput

// ===================================================================
//
// Function:     GetInputFrame
//
// Description:  Returns pointer to an input frame buffer for a frame
//               that had been requested by a call to SetInputFrames.
//
//               The requested frames are indexed by the input port index
//               and frame index.
//
//               This function is typically called from within the Tool's
//               DoProcess function to get the frames that were previously
//               requested in the BeginIteration function.
//
// Arguments:    int inPortIndex    Index of input port
//               int frameIndex     Frame index in clip's frame list
//
// Returns:      Pointer to requested CToolFrameBuffer.
//               Returns NULL pointer if arguments are out-of-range
//
// ===================================================================
CToolFrameBuffer* CToolProcessor::GetInputFrame(int inPortIndex,
                                                int frameIndex)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return 0;  // ERROR: inPortIndex is out-of-range

   SInPort &inPort = inPortList[inPortIndex];

   CToolFrameBuffer *frameBuffer;
   // Iterate over the frames that are in the input list.  These are
   // the frames that were requested in the call to SetInputFrames
   ToolFrameVector::iterator iterator;
   for (iterator = inPort.inFrameList.begin();
        iterator != inPort.inFrameList.end();
        ++iterator)
      {
      frameBuffer = *iterator;
      if (frameBuffer != 0 && frameBuffer->GetFrameIndex() == frameIndex)
         return frameBuffer;
      }

   // If we reached this point, then the requested frame index was not found
   return 0;
}

void CToolProcessor::DeleteInputFrame(int inPortIndex, int frameIndex)
{
   // Delete a frame in the input buffer given the frame index

   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return;  // ERROR: inPortIndex is out-of-range

   SInPort &inPort = inPortList[inPortIndex];

   // Iterate over the frames that are in the input list.  These are
   // the frames that were requested in the call to SetInputFrames
   ToolFrameVector::iterator iterator;
   for (iterator = inPort.inFrameList.begin();
		iterator != inPort.inFrameList.end();
        ++iterator)
	  {
	  CToolFrameBuffer * &frameBuffer = *iterator;
	  if (frameBuffer != 0 && frameBuffer->GetFrameIndex() == frameIndex)
		 {
         // NASTY hackaround for problems caused by an autofilter "past-only" hack!
		 ToolFrameVector::iterator iterator2 = iterator + 1;
		 if (iterator2 < inPort.inFrameList.end())
			{
			CToolFrameBuffer * &frameBuffer2 = *iterator2;
			if (frameBuffer2 != 0 && frameBuffer2->GetFrameIndex() == frameIndex)
			   {
			   // Next frame is the same! Skip this one!
			   continue;
			   }
			}

         if (frameBuffer->IsDontCache())
            {
            delete frameBuffer;
            }
         else
            {
            GetSystemAPI()->GetToolFrameCache()->AddFrame(frameBuffer);
            }
         frameBuffer = 0;
         return;
         }
      }

   // If we reached this point, then the requested frame index was not found
}


// ===================================================================
//
// Function:     GetRequestedInputFrame
//
// Description:  Returns pointer to an input frame buffer for a frame
//               that had been requested by a call to SetInputFrames.
//
//               The requested frames are indexed by the input port index
//               and the position in the frameIndexList argument to
//               SetInputFrames.
//
//               This function is typically called from within the Tool's
//               DoProcess function to get the frames that were previously
//               requested in the BeginIteration function.
//
// Arguments:    int inPortIndex    Index of input port
//               int requestIndex   Index of frame in frameIndexList previously
//                                  passed to SetInputFrames
//
// Returns:      Pointer to requested CToolFrameBuffer.
//               Returns NULL pointer if arguments are out-of-range
//
// ===================================================================
CToolFrameBuffer* CToolProcessor::GetRequestedInputFrame(int inPortIndex,
                                                         int requestIndex)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return 0;  // ERROR: inPortIndex is out-of-range

   SInPort &inPort = inPortList[inPortIndex];

   if (requestIndex < 0 || requestIndex >= (int)inPort.inFrameList.size())
      return 0;   // ERROR: requestIndex is out-of-range

   CToolFrameBuffer *frameBuffer = inPort.inFrameList[requestIndex];

   return frameBuffer;
}
// ===================================================================
//
// Function:     GetOutputFrame
//
// Description:  Returns pointer to an output frame buffer for a frame
//               that had been requested by a call to SetOutputFrames.
//
//               The requested frames are indexed by the output port index
//               and frame index.
//
//               This function is typically called from within the Tool's
//               DoProcess function to get the frames that were previously
//               requested in the BeginIteration function.
//
// Arguments:    int outPortIndex    Index of output port
//               int frameIndex     Frame index in clip's frame list
//
// Returns:      Pointer to requested CToolFrameBuffer.
//               Returns NULL pointer if arguments are out-of-range
//
// ===================================================================
CToolFrameBuffer* CToolProcessor::GetOutputFrame(int outPortIndex,
                                                 int frameIndex)
{
   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return 0;  // ERROR: outPortIndex is out-of-range

   SOutPort &outPort = outPortList[outPortIndex];

   CToolFrameBuffer *frameBuffer;
   // Iterate over the frames that are in the output list.  These are
   // the frames that were requested in the call to SetOutputFrames
   ToolFrameVector::iterator iterator;
   for (iterator = outPort.outFrameList.begin();
        iterator != outPort.outFrameList.end();
        ++iterator)
      {
      frameBuffer = *iterator;
      if (frameBuffer->GetFrameIndex() == frameIndex)
         return frameBuffer;
      }

   // If we reached this point, then the requested frame index was not found
   return 0;
}


// ===================================================================
//
// Function:     GetRequestedOutputFrame
//
// Description:  Returns pointer to an input frame buffer for a frame
//               that had been requested by a call to SetInputFrames.
//
//               The requested frames are indexed by the input port index
//               and the position in the frameIndexList argument to
//               SetInputFrames.
//
//               This function is typically called from within the Tool's
//               DoProcess function to get the frames that were previously
//               requested in the BeginIteration function.
//
// Arguments:    int inPortIndex    Index of input port
//               int requestIndex   Index of frame in frameIndexList previously
//                                  passed to SetInputFrames
//
// Returns:      Pointer to requested CToolFrameBuffer.
//               Returns NULL pointer if arguments are out-of-range
//
// ===================================================================
CToolFrameBuffer* CToolProcessor::GetRequestedOutputFrame(int outPortIndex,
                                                          int requestIndex)
{
   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return 0;  // ERROR: outPortIndex is out-of-range

   SOutPort &outPort = outPortList[outPortIndex];

   if (requestIndex < 0 || requestIndex >= (int)outPort.outFrameList.size())
      return 0;   // ERROR: requestIndex is out-of-range

   CToolFrameBuffer *frameBuffer = outPort.outFrameList[requestIndex];

   return frameBuffer;
}

// ===================================================================
//
// Function:    ReleaseBuffers
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
bool CToolProcessor::ReleaseBuffers(bool *activity)
{
   bool allBuffersReleased = true;
   *activity = false;

   // Iterate over each input port
   int inPortCount = GetInPortCount();
   for (int inPortIndex = 0; inPortIndex < inPortCount; ++inPortIndex)
      {
      // For coding convenience and readability, make some local references.
      SInPort &inPort = inPortList[inPortIndex];
      FrameIndexList &releaseList = inPort.releaseList;


      // Iterate over the list of frames to be released
      int releaseCount = (int)releaseList.size();
      for (int i = 0; i < releaseCount; ++i)
         {
         CToolFrameBuffer *frameBuffer = GetInputFrame(inPortIndex,
                                                       releaseList[i]);
         if (frameBuffer == 0)
            continue;   // skip to next

         int frameIndex = frameBuffer->GetFrameIndex();

         RemoveAvailableFrame(inPort, frameIndex);

         // Delete the CToolFrameBuffer, which will deallocate the
         // frame and field buffers
         //delete frameBuffer;
         DeleteInputFrame(inPortIndex, releaseList[i]);
         }
      }

   return allBuffersReleased;

} // ReleaseBuffers

void CToolProcessor::RemoveAvailableFrame(SInPort &inPort, int frameIndex)
{
   // Check the available frames
   list<CToolFrameBuffer*>::iterator iterator;
   for (iterator = inPort.availableFrames.begin();
        iterator != inPort.availableFrames.end();
        ++iterator)
      {
      if ((*iterator)->GetFrameIndex() == frameIndex)
         {
         // Found it, so remove it from the list of available frames
         inPort.availableFrames.erase(iterator);
         return;
         }
      }

   return;
}

// ===================================================================
//
// Function:    PutOutput
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
bool CToolProcessor::PutOutput(bool *activity)
{
   bool putOutputDone = true;
   *activity = false;

   // Iterate over each output port
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      // For coding convenience and readability, make some local references.
      SOutPort &outPort = outPortList[outPortIndex];
      ToolFrameVector &outFrameList = outPort.outFrameList;

      // Iterate over frames waiting to be output
      int frameCount = (int)outFrameList.size();
      for (int i = 0; i < frameCount; ++i)
         {
         // This block was added for bug 2666
         if (outFrameList[i] == 0)
            {
            // This frame has already been output on a previous pass
            continue;
            }

         if (IsOutPortFull(outPortIndex))
            {
            // The out port is full, so can't output these frames right now
            putOutputDone = false;
            break;
            }

         // Save frame index for use after frame is output
         int frameIndex = outFrameList[i]->GetFrameIndex();

         OutputFrame(outPortIndex, outFrameList[i]);

         if (outPort.modifyInPlace && !outPort.cloneForOutput)
            {
            // Modified-in-place frames must be removed from the
            // available frames list so that they are not reused by the tool
            RemoveAvailableFrame(inPortList[outPort.inPortIndex],
                                 frameIndex);
            }

         outFrameList[i] = 0;
         *activity = true;
         }
      }

   return putOutputDone;

} // PutOutput

//------------------------------------------------------------------------
//
// Function:    OutputFrame
//
// Description: Put a frame into a tool's output port.
//
// Arguments:   int port         Output port selection
//              CToolFrameBuffer *frameBuffer   Ptr to frame buffer to output
//
// Returns:     None
//
//------------------------------------------------------------------------
void CToolProcessor::OutputFrame(int port, CToolFrameBuffer *frameBuffer)
{
// Future Reference
//    Tools that modify the entire frame, e.g. Noise & Grain or
//    generate brand new frames, e.g., Format Conversion, should
//    not save the "original values" to History.  The former example
//    because it takes too much space and the latter example because
//    there is no need to save the history.
//    Need to figure out how to flag whether to save to History or not:
//    the flag could be in this CToolProcessor instance or in individual
//    CToolFrameBuffers.
//    Also, a tool always needs to identify the pixels that have changed
//    so they can be transfered to the invisible fields.  However, when
//    the history is not going to be saved (as above), there is no need
//    to copy the frames to the pixel region list (since we only want
//    the descriptions of the regions, but don't care about the values).
//
//    Tools may request that output is to a new buffer (ie, not modify-in-place).
//    This may be for the convenience of the algorithm or because the frame
//    will eventually be written to another clip.  If the frame buffer will
//    be written to the input clip, then invisible fields need to be transfered
//    from the input buffer to the output buffer.  May be need for the history.
//    However, if the output is going to another clip, then we don't need the
//    invisible fields in the output buffer because they will be reconstructed
//    by writeMediaFilmFrames.  Also do not need to save history.
//
//    We don't have to worry about the above for a while because our first tools,
//    DRS, Autofilter & Scratch all write back to the input clip.  When we
//    do Noise & Grain or Format Convert we will need to put some flags in
//    to control the handling of the invisible fields.

// FORMERLY if (saveToHistory)
//             GetSystemAPI()->SaveToolFrameBuffer(frameBuffer, toolNumber, toolName);
//
// NOW further down, if not using history still copies history file down from parent

   GetSystemAPI()->SaveToolFrameBuffer(frameBuffer, toolNumber, toolName, saveToHistory);

// TTT - SHOULD CHECK FOR ERRORS IN HISTORY

   // Copy the changes made to the visible frame to the invisible fields
   // The "original values" pixel region list is used to figure out which
   // pixels have been modified by the tool
   frameBuffer->UpdateInvisibleFields();

   ////////////////////////////////////////////////////////////////////////
   // mbraca consolidated these two methods so I can just MOVE the original
   // value pixels instead of COPYING them!
#if __OLD_WAY__
   // Add to the new pixel list, used for highlighting and preview
   frameBuffer->AppendNewPixels();

   // Clear the "original values" pixel region list since we do not need
   // them anymore.
   // TTT - maybe this should be done before the CToolFrameBuffer is passed
   //       to a tool, rather than here.
   frameBuffer->ClearOriginalValues();
#else
   // NEW WAY
   frameBuffer->AppendNewPixelsAndClearOriginalValues();
#endif


   // Send the frame to the tool's output port
   PutFrame(port, frameBuffer);
}

int CToolProcessor::PutEndOfMaterialFrames()
{
   // Iterate over each output port
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      CToolFrameBuffer *frameBuffer = new CToolFrameBuffer(NULL, NULL);

      frameBuffer->SetEndOfMaterial();

      PutFrame(outPortIndex, frameBuffer);
      }

   return 0;
}

// ===================================================================
//
// Function:      FlushBuffers
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolProcessor::FlushBuffers()
{
   // Iterate over each input port
   int inPortCount = GetInPortCount();
   for (int inPortIndex = 0; inPortIndex < inPortCount; ++inPortIndex)
      {
      // For coding convenience and readability, make some local references.
      SInPort &inPort = inPortList[inPortIndex];

      // Delete the CToolFrameBuffers in the availableFrames list.
      FlushAvailableFrames(inPort);

      // Clear the various lists in the inPort, since we are no longer
      // interested in their contents.
      inPort.requestList.clear();
      inPort.inFrameList.clear();
      }

   // Iterate over each output port
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      // For coding convenience and readability, make some local references
      SOutPort &outPort = outPortList[outPortIndex];

      if (!outPort.modifyInPlace || outPort.cloneForOutput)
         {
         ToolFrameVector &outFrameList = outPort.outFrameList;
         int outFrameCount = (int)outFrameList.size();
         for (int i = 0; i < outFrameCount; ++i)
            {
            delete outFrameList[i];
            }
         }
      outPort.outFrameList.clear();
      }

   return 0;

}  // FlushBuffers

void CToolProcessor::FlushAvailableFrames(SInPort &inPort)
{
   // Delete the CToolFrameBuffers in the availableFrames list.  These
   // are the frames that have previously be obtained from the input port
   // but have not yet been released or put to the output.  Deleting
   // the CToolFrameBuffer instances will delete image memory they are
   // managing.

   list<CToolFrameBuffer*>::iterator iterator;
   for (iterator = inPort.availableFrames.begin();
        iterator != inPort.availableFrames.end();
        ++iterator)
      {
      delete *iterator;
      }

   inPort.availableFrames.clear();
}

void CToolProcessor::FlushDstFrameQueues()
{
   // Iterate over each output port
   int outPortCount = GetOutPortCount();
   for (int outPortIndex = 0; outPortIndex < outPortCount; ++outPortIndex)
      {
      // Flush output ports (presumably tool frame queues)
      node->FlushDstFrameQueue(outPortIndex);
      }
}

// ===================================================================
//
// Function:    SetInputMaxFrames
//
// Description: Set the maximum number of frames that the tool will
//              request from the given input port during a single
//              processing cycle.
//
//              This function must be called by the SetParameters
//              member function defined in the derived CToolProcessor
//              subclass.
//
// Arguments:   int inPortIndex     Input port index
//              int maxFrames       Maximum number of input frames tool will
//                                  be using (typically request of first
//                                  iteration)
//              int maxNewFrames    Max number of new frames tool will
//                                  request per iteration after first iteration
//
// Returns:     0 if okay
//              TOOL_ERROR_INVALID_IN_PORT_INDEX if inPort is out-of-range
//
// ===================================================================
int CToolProcessor::SetInputMaxFrames(int inPortIndex, int maxFrames,
                                      int maxNewFrames)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return TOOL_ERROR_INVALID_IN_PORT_INDEX;

   inPortList[inPortIndex].maxFrames = maxFrames;

   // Tell source frame queue what the maximum number of frames
   // the tool will request
   node->SetSrcMaxEntryCount(inPortIndex, maxNewFrames);

   return 0;
}

// ===================================================================
//
// Function:     SetOutputModifyInPlace
//
// Description:  Sets the handling of output frames for a given output port.
//               Specifies that the output frame to the given output port
//               will be an input frame that has been modified by the tool.
//
//               NB: For a given output port, the functions
//                   SetOutputModifyInPlace and SetOutputNewFrame are
//                   mutually-exclusive and only one should be called.
//
//              This function is typically called by the SetParameters
//              member function defined in the derived CToolProcessor
//              subclass.
//
// Arguments:    int outPortIndex    Output port index
//               int inPortIndex     Input port that is the source of frames
//               bool cloneForOutput   If true, the output will be a copy
//                              of the modified input frame.  If false, the
//                              output frame will be the input frame.
//
// Returns:     0 if okay
//              TOOL_ERROR_INVALID_IN_PORT_INDEX if inPort is out-of-range
//              TOOL_ERROR_INVALID_OUT_PORT_INDEX if outPort is out-of-range
//
// ===================================================================
int CToolProcessor::SetOutputModifyInPlace(int outPortIndex, int inPortIndex,
                                           bool cloneForOutput)
{
   int retVal;

   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return TOOL_ERROR_INVALID_OUT_PORT_INDEX;

   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return TOOL_ERROR_INVALID_IN_PORT_INDEX;

   // Get the particular Output Port structure
   SOutPort &outPort = outPortList[outPortIndex];

   outPort.modifyInPlace = true;
   outPort.inPortIndex = inPortIndex;
   outPort.cloneForOutput = cloneForOutput;

   // Propagate the image format from the input port to the output port
   outPort.imageFormat = GetSrcImageFormat(inPortIndex);
   SetDstImageFormat(outPortIndex, outPort.imageFormat);
   outPort.invisibleFieldsPerFrame = GetSrcInvisibleFieldsPerFrame(inPortIndex);
   SetDstInvisibleFieldsPerFrame(outPortIndex, outPort.invisibleFieldsPerFrame);

   // If we are going to clone the modified-in-place input frame buffer
   // for output, we need to get a buffer allocator for the output port
   // that suits the image format
   retVal = SetOutPortBufferAllocator(outPort, false);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

// ===================================================================
//
// Function:     SetOutputNewFrame
//
// Description:  Sets the handling of output frames for a given output port.
//               Specifies that the output frame to the given output port
//               will be a new frame that is created by the tool.  This
//               new frame will have the same image format as the given
//               input port.
//
//               NB: For a given output port, the functions
//                   SetOutputModifyInPlace and SetOutputNewFrame are
//                   mutually-exclusive and only one should be called
//                   for each output port.
//
//              This function is typically called by the SetParameters
//              member function defined in the derived CToolProcessor
//              subclass.
//
// Arguments:    int outPortIndex    Output port index
//               int inPortIndex     Input port that is the source of image format
//
// Returns:     0 if okay
//              TOOL_ERROR_INVALID_IN_PORT_INDEX if inPort is out-of-range
//              TOOL_ERROR_INVALID_OUT_PORT_INDEX if outPort is out-of-range
//
// ===================================================================
int CToolProcessor::SetOutputNewFrame(int outPortIndex, int inPortIndex)
{
   int retVal;

   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return TOOL_ERROR_INVALID_OUT_PORT_INDEX;

   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return TOOL_ERROR_INVALID_IN_PORT_INDEX;

   // Get the particular Output Port structure
   SOutPort &outPort = outPortList[outPortIndex];

   outPort.modifyInPlace = false;
   outPort.inPortIndex = inPortIndex;
   outPort.cloneForOutput = false;

   // Propagate the image format from the input port to the output port
   outPort.imageFormat = GetSrcImageFormat(inPortIndex);
   SetDstImageFormat(outPortIndex, outPort.imageFormat);
   outPort.invisibleFieldsPerFrame = GetSrcInvisibleFieldsPerFrame(inPortIndex);
   SetDstInvisibleFieldsPerFrame(outPortIndex, outPort.invisibleFieldsPerFrame);

   // Get a buffer allocator for the output port that suits the
   // image format
   retVal = SetOutPortBufferAllocator(outPort, false);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

// ===================================================================
//
// Function:     SetOutputNewFrame
//
// Description:  Sets the handling of output frames for a given output port.
//               Specifies that the output frame to the given output port
//               will be a new frame that is created by the tool.  This
//               new frame will have a caller-specified image format.
//
//               NB: For a given output port, the functions
//                   SetOutputModifyInPlace and SetOutputNewFrame are
//                   mutually-exclusive and only one should be called.
//
//              This function is typically called by the SetParameters
//              member function defined in the derived CToolProcessor
//              subclass.
//
// Arguments:   int outPortIndex    Output port index
//              CImageFormat *newImageFormat   Image format for output frames
//              double invisibleFieldsPerFrame  Correction factor so enough
//                                              frame buffers are allocated
//                                              for invisible fields.
//                                              Typically .5 for film-frames
//                                              with 3:2 pull down, 0 otherwise
//
// Returns:     0 if okay
//              TOOL_ERROR_INVALID_OUT_PORT_INDEX if outPort is out-of-range
//
// ===================================================================
int CToolProcessor::SetOutputNewFrame(int outPortIndex,
                                      const CImageFormat *newImageFormat,
                                      double newInvisibleFieldsPerFrame)
{
   int retVal;

   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return TOOL_ERROR_INVALID_OUT_PORT_INDEX;

   // Get the particular Output Port structure
   SOutPort &outPort = outPortList[outPortIndex];

   outPort.modifyInPlace = false;
   outPort.inPortIndex = -1;
   outPort.cloneForOutput = false;

   // Propagate the caller's image format to the output port
   outPort.imageFormat = newImageFormat;
   SetDstImageFormat(outPortIndex, outPort.imageFormat);
   outPort.invisibleFieldsPerFrame = newInvisibleFieldsPerFrame;
   SetDstInvisibleFieldsPerFrame(outPortIndex, newInvisibleFieldsPerFrame);

   // Get a buffer allocator for the output port that suits the
   // caller's output image format
   retVal = SetOutPortBufferAllocator(outPort, false);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

// ===================================================================
//
// Function:    SetOutputMaxFrames
//
// Description: Set the maximum number of frames that the tool will
//              output to the given output port during a single
//              processing cycle.
//
//              This function must be called by the SetParameters
//              member function defined in the derived CToolProcessor
//              subclass.
//
// Arguments:   int outPortIndex    Output port index
//              int maxFrames  Maximum number of frames tool will output
//
// Returns:     0 if okay
//              TOOL_ERROR_INVALID_OUT_PORT_INDEX if outPort is out-of-range
//
// ===================================================================
int CToolProcessor::SetOutputMaxFrames(int outPortIndex, int maxFrames)
{
   int retVal;

   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return TOOL_ERROR_INVALID_OUT_PORT_INDEX;

   outPortList[outPortIndex].maxFrames = maxFrames;

   retVal = SetOutPortBufferAllocator(outPortList[outPortIndex], false);
   if (retVal != 0)
      return retVal; // ERROR

   // Tell destination frame queue the maximum number of frames
   // that will be output by this tool processor
   node->SetDstMaxEntryCount(outPortIndex, maxFrames);

   return 0;
}

int CToolProcessor::GetInPortCount() const
{
   return node->GetInPortCount();
}

int CToolProcessor::GetOutPortCount() const
{
   return node->GetOutPortCount();
}

CToolNode* CToolProcessor::GetToolNode()
{
   return node;
}

IToolProgressMonitor *CToolProcessor::GetToolProgressMonitor()
{
   return toolProgressMonitor;
}

const CImageFormat* CToolProcessor::GetSrcImageFormat(int inPortIndex) const
{
   return node->GetSrcImageFormat(inPortIndex);
}

void CToolProcessor::SetDstImageFormat(int outPortIndex,
                                       const CImageFormat *newImageFormat)
{
   // Propagate the image format forward to output port
   node->SetDstImageFormat(outPortIndex, newImageFormat);
}

double CToolProcessor::GetSrcInvisibleFieldsPerFrame(int inPortIndex) const
{
   return node->GetSrcInvisibleFieldsPerFrame(inPortIndex);
}

void CToolProcessor::SetDstInvisibleFieldsPerFrame(int outPortIndex,
                                               double newInvisibleFieldPerFrame)
{
   // Propage the invisible fields per frame to output port
   node->SetDstInvisibleFieldsPerFrame(outPortIndex, newInvisibleFieldPerFrame);
}

void CToolProcessor::InitializePortDefs()
{
   SInPort inPort;
   inPort.maxFrames = -1;
   inPortList.assign(GetInPortCount(), inPort);   // Populate list

   SOutPort outPort;
   outPort.maxFrames = -1;
   outPort.modifyInPlace = false;
   outPort.cloneForOutput = false;
   outPort.inPortIndex = -1;
   outPort.invisibleFieldsPerFrame = 0.0;
   outPort.imageFormat = 0;
   outPort.bufferAllocator = 0;
   outPortList.assign(GetOutPortCount(), outPort);  // Populate list
}

int CToolProcessor::SetOutPortBufferAllocator(SOutPort &outPort,
                                              bool assign)
{
   // Sets a Buffer Allocator in the caller's out port
   //
   // Preconditions for setting the allocator are:
   // 1. Allocator hasn't been set already
   // 2. Tool has set the maximum number of frames that will be output
   // 3. Tool has set the image format (either from an input or a new one)
   // 4. Tool will need to create new frames for output

   if (outPort.maxFrames != -1
       && outPort.imageFormat != 0
       && (outPort.bufferAllocator == 0 || assign)
       && (!outPort.modifyInPlace || outPort.cloneForOutput))
      {
      int frameCount = (assign) ? outPort.maxFrames : 0;

      int bytesPerBuffer = outPort.imageFormat->getBytesPerField();
      outPort.bufferAllocator = node->GetBufferAllocator(bytesPerBuffer,
                                                         frameCount,
                                               outPort.invisibleFieldsPerFrame);
      if (outPort.bufferAllocator == 0)
         return TOOL_ERROR_CANNOT_ALLOCATE_FRAME_BUFFERS;
      }

   return 0;
}

int CToolProcessor::SetInPortBufferAllocator(int inPortIndex)
{
   // Sets a Buffer Allocator in the caller's out port
   //
   // Preconditions for setting the allocator are:
   // 1. Tool has set the maximum number of frames that will be input
   // 2. Tool has set the image format (either from an input or a new one)

   int maxFrames = inPortList[inPortIndex].maxFrames;
   const CImageFormat *imageFormat = GetSrcImageFormat(inPortIndex);
   if (maxFrames > 0 && imageFormat != 0)
      {
      int bytesPerBuffer = imageFormat->getBytesPerField();
      CBufferPool *bufferAllocator = node->GetBufferAllocator(bytesPerBuffer,
                                                              maxFrames,
                                    GetSrcInvisibleFieldsPerFrame(inPortIndex));
      if (bufferAllocator == 0)
         return TOOL_ERROR_CANNOT_ALLOCATE_FRAME_BUFFERS;
      }

   return 0;
}

CBufferPool* CToolProcessor::GetOutputBufferAllocator(int outPortIndex)
{
   return outPortList[outPortIndex].bufferAllocator;
}

// ===================================================================
//
// Function:    SetInputFrames
//
// Description: Sets which input frame(s) the tool wants from the given
//              input port for next processing iteration.
//
//              This function is typically called by the BeginIteration
//              member function defined in the derived CToolProcessor
//              subclass.  BeginIteration is called at the beginning
//              of each frame processing iteration.
//
// Arguments:   int inPortIndex       Input Port index
//              int frameCount        Number of frames requested
//              int *frameIndexList   Array of requested frame indices
//
// Returns:     0 if success
//
// ===================================================================
int CToolProcessor::SetInputFrames(int inPortIndex, int frameCount,
                                   int *frameIndexList)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return TOOL_ERROR_INVALID_IN_PORT_INDEX;

   SInPort &inPort = inPortList[inPortIndex];

   // Transfer caller's frame indices to requestList, assuming old requests
   // have already been cleared
   for (int i = 0; i < frameCount; ++i)
      inPort.requestList.push_back(frameIndexList[i]);

   // Setup the inFrameList to receive pointers to the requested frames
   CToolFrameBuffer *frameBuffer = 0;
   inPort.inFrameList.assign(frameCount, frameBuffer);

   return 0;
}

// ===================================================================
//
// Function:    SetFramesToRelease
//
// Description: Sets which input frame(s) the tool wants to release
//              after the next processing iteration.  Frames must
//              have been previously requested from same Input Port
//              but not yet released or be put to the output.
//
//              This function is typically called by the BeginIteration
//              member function defined in the derived CToolProcessor
//              subclass.  BeginIteration is called at the beginning
//              of each frame processing iteration.
//
// Arguments:   int inPortIndex       Input Port index
//              int frameCount        Number of frames requested
//              int *frameIndexList   Array of requested frame indices
//
// Returns:     None
//
// ===================================================================
int CToolProcessor::SetFramesToRelease(int inPortIndex, int frameCount,
                                        int *frameIndexList)
{
   if (inPortIndex < 0 || inPortIndex >= GetInPortCount())
      return TOOL_ERROR_INVALID_IN_PORT_INDEX;

   SInPort &inPort = inPortList[inPortIndex];

   // Transfer caller's frame indices to releaseList, assuming old requests
   // have already been cleared
   for (int i = 0; i < frameCount; ++i)
      inPort.releaseList.push_back(frameIndexList[i]);

   return 0;
}

// ===================================================================
//
// Function:    SetOutputFrames
//
// Description: Sets which frames will be put to the Output Port after
//              the next processing iteration.  The frame to be output
//              may be an input frame that was modified in place or
//              a new frame that received the results of the processing.
//              The output handling is set prior to start of processing
//              by a call to one of the functions SetOutputModifyInPlace
//              or SetOutputNewFrame.
//
//              This function is typically called by the BeginIteration
//              member function defined in the derived CToolProcessor
//              subclass.  BeginIteration is called at the beginning
//              of each frame processing iteration.
//
// Arguments:   int outPortIndex      Output Port index
//              int frameCount        Number of frames to output
//              int *frameIndexList   Array of frame indices to be output
//
// Returns:     None
//
// ===================================================================
int CToolProcessor::SetOutputFrames(int outPortIndex, int frameCount,
                                    int *frameIndexList)
{
   if (outPortIndex < 0 || outPortIndex >= GetOutPortCount())
      return TOOL_ERROR_INVALID_OUT_PORT_INDEX;

   // Get the particular Output Port structure
   SOutPort &outPort = outPortList[outPortIndex];

   // Transfer caller's frame indices to requestList, assuming old requests
   // have already been cleared
   for (int i = 0; i < frameCount; ++i)
      outPort.requestList.push_back(frameIndexList[i]);

   // Setup the outFrameList to receive pointers to the frames
   // that will be output after processing.
   CToolFrameBuffer *frameBuffer = 0;
   outPort.outFrameList.assign(frameCount, frameBuffer);

   return 0;
}

// ===================================================================
//
// Function:    SetSaveToHistory
//
// Description: Set flag which controls whether changes to a frame
//              are written to history.  Default setting is true,
//              set when processing is started.  Most tools will
//              not use this function and just leave the default setting.
//              This function might be called for those tools where
//              it doesn't make any sense to save to history, e.g. format
//              convert
//
// Arguments:   bool newSaveToHistory  true to save to history
//                                     false to avoid saving to history
//
// Returns:     None
//
// ===================================================================
void CToolProcessor::SetSaveToHistory(bool newSaveToHistory)\
{
   saveToHistory = newSaveToHistory;
}

// ===================================================================
//
// Function:
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
bool CToolProcessor::IsEmergencyStop() const
{
   // Return the current value of the emergency stop flag
   return *emergencyStopFlagPtr;
}

const bool* CToolProcessor::GetEmergencyStopFlagPtr() const
{
   // Return a pointer to the emergency stop flag.  The pointer will
   // always be non-null and point to something real

   // Notes: 1) The caller should never cast this pointer to non-const
   //        2) The caller should never change the value of the emergency
   //           stop flag or pointer
   //        3) Only the parent CToolSetup should set or clear the
   //           emergency stop flag

   return emergencyStopFlagPtr;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CToolProcessor::AssignAllBufferAllocators()
{
   int retVal;

   // Assign memory for input
   for (int inPortIndex = 0; inPortIndex < GetInPortCount(); ++inPortIndex)
      {
      retVal = SetInPortBufferAllocator(inPortIndex);
      if (retVal != 0)
         return retVal;  // ERROR:
      }

   // Assign memory for output
   for (int outPortIndex = 0; outPortIndex < GetOutPortCount(); ++outPortIndex)
      {
      retVal = SetOutPortBufferAllocator(outPortList[outPortIndex], true);
      if (retVal != 0)
         return retVal;  // ERROR:
      }

   return 0;
}

void CToolProcessor::ReleaseAllBufferAllocators()
{
   // Release memory for input
   for (int inPortIndex = 0; inPortIndex < GetInPortCount(); ++inPortIndex)
      {
      int maxFrames = inPortList[inPortIndex].maxFrames;
      const CImageFormat *imageFormat = GetSrcImageFormat(inPortIndex);
      if (maxFrames > 0 && imageFormat != 0)
         {
         int bytesPerBuffer = imageFormat->getBytesPerField();
         node->ReleaseBufferAllocator(bytesPerBuffer, maxFrames,
                                    GetSrcInvisibleFieldsPerFrame(inPortIndex));
         }
      }

   // Release memory for output
   for (int outPortIndex = 0; outPortIndex < GetOutPortCount(); ++outPortIndex)
      {
      int maxFrames = outPortList[outPortIndex].maxFrames;
      const CImageFormat *imageFormat = outPortList[outPortIndex].imageFormat;
      if (maxFrames > 0 && imageFormat != 0)
         {
         int bytesPerBuffer = imageFormat->getBytesPerField();
         node->ReleaseBufferAllocator(bytesPerBuffer, maxFrames,
                             outPortList[outPortIndex].invisibleFieldsPerFrame);
         }
      }

}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###     CToolParameters Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolParameters::CToolParameters(int newInPortCount, int newOutPortCount)
 : regionOfInterest(0)
{
}

CToolParameters::~CToolParameters()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CRegionOfInterest* CToolParameters::GetRegionOfInterest() const
{
   return regionOfInterest;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CToolParameters::SetRegionOfInterest(CRegionOfInterest *newROI)
{
   regionOfInterest = newROI;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                           ####################################
// ###     CToolIOConfig Class     ###################################
// ####                           ####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolIOConfig::CToolIOConfig(int newInPortCount, int newOutPortCount)
 : processingThreadCount(1)
 , processingThreadsPerFrame(1)
 , doRowSlices(true)
 , slicesPerThread(1)
 , noopProcessing(false)
{
}

CToolIOConfig::~CToolIOConfig()
{
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###     CToolFrameRange Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolFrameRange::CToolFrameRange(int newInPortCount, int newOutPortCount)
{
   SFrameRange frameRange;
   frameRange.randomAccess = false;
   frameRange.inFrameIndex = -1;
   frameRange.outFrameIndex = -1;

   inFrameRange.assign(newInPortCount, frameRange);
   outFrameRange.assign(newOutPortCount, frameRange);
}

// Copy Constructor
CToolFrameRange::CToolFrameRange(const CToolFrameRange& srcToolFrameRange)
{
   inFrameRange.assign(srcToolFrameRange.inFrameRange.begin(),
                       srcToolFrameRange.inFrameRange.end());
   outFrameRange.assign(srcToolFrameRange.outFrameRange.begin(),
                        srcToolFrameRange.outFrameRange.end());
}

CToolFrameRange::~CToolFrameRange()
{
}

// Assignment Operator
CToolFrameRange&
CToolFrameRange::operator=(const CToolFrameRange& srcToolFrameRange)
{
   if (this != &srcToolFrameRange)  // avoid self-copy
      {
      inFrameRange.assign(srcToolFrameRange.inFrameRange.begin(),
                          srcToolFrameRange.inFrameRange.end());
      outFrameRange.assign(srcToolFrameRange.outFrameRange.begin(),
                           srcToolFrameRange.outFrameRange.end());
      }

   return *this;
}
