// ToolSystemInterface.h: interface for the CToolSystemInterface class.
//
//  CToolSystemInterface is an Abstract Base Class that provides
//  an interface by which Tools can call functions in the Base System
//  without being tied to the internals of the Base System.
//
//  The Base System creates concrete instantiations of its particular
//  subtype of CToolSytstemInterface
//
/*
$Header: /usr/local/filmroot/tool/include/ToolSystemInterface.h,v 1.120.2.73 2009/11/01 00:41:02 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOL_SYSTEM_INTERFACE_H)
#define TOOL_SYSTEM_INTERFACE_H

#ifdef __BORLANDC__
#include <vcl.h>
#endif // #ifdef __BORLANDC_

#ifdef __sgi
#include <Xm/Xm.h>
#endif // #ifdef __sgi

#include "machine.h"
#include "ClipSharedPtr.h"
#include "PDL.h"
#include <vector>
using std::vector;
#include "guid_mti.h"

//////////////////////////////////////////////////////////////////////
//Forward Declarations

struct AffineCoeff;
class CAutoCleanDebrisFile;
class CAutotoolCommand;
class CBrush;
class CBufferPool;
class CClip;
class CDisplayer;
class CImageFormat;
class CMagnifier;
class CMaskToolRegionList;
class CMTIBitmap;
class CPixelRegionList;
class CPlayer;
class CProvisionalToolProc;
class CRegionOfInterest;
class CRenderDestinationClip;
class CToolFrameBuffer;
class CToolFrameCache;
class CToolFrameRange;
class CToolIOConfig;
class CToolPaintInterface;
class CToolObject;
class CToolParameters;
class CToolSetup;
class TrackingBoxManager;
class CVideoFrameList;
class IToolProgressMonitor;
class CTimeLine;
class GrainPresets;

//////////////////////////////////////////////////////////////////////
// Typedefs for platform-dependent Function Pointers to Keyboard and
// Mouse Input Event Handlers.

// Event Handler Function Pointer Types for Borland C++Builder VCL
#ifdef __BORLANDC__
typedef TKeyEvent        MTIKeyEventHandler;            // Key Down/Up
typedef TMouseEvent      MTIMouseButtonEventHandler;    // Mouse Button Down/Up
typedef TMouseMoveEvent  MTIMouseMoveEventHandler;      // Mouse Move
typedef TMouseWheelEvent MTIMouseWheelEventHandler;     // Mouse Move
#endif // #ifdef __BORLANDC_)

// Event Handler Function Pointer Types for X Windows
#ifdef __sgi
typedef XtEventHandler  MTIKeyEventHandler;           // Key Down/Up
typedef XtEventHandler  MTIMouseButtonEventHandler;   // Mouse Button Down/Up
typedef XtEventHandler  MTIMouseMoveEventHandler;     // Mouse Move
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////

// Stuff added for 'tool adoption' hack

enum FosterParentEventId
{
   FPE_Noop,
   FPE_FormShow,
   FPE_FormHide,
   FPE_FormActivate,
   FPE_FormDeactivate,
   FPE_FormPaint,
   FPE_FormKeyDown,
   FPE_FormKeyUp,
   FPE_FormMouseDown,
   FPE_FormMouseUp,
   FPE_FormMouseMove,
};

struct FosterParentEventInfo
{
   FosterParentEventId Id;
   int *Sender;
   int  Key;

#define FPEI_NoMouseButton     0
#define FPEI_LeftMouseButton   1
#define FPEI_MiddleMouseButton 2
#define FPEI_RightMouseButton  3
   int MouseButton;
   int X;
   int Y;

   bool Shift, Alt, Ctrl, Left, Right,
        Middle, Double_Clicked;

   FosterParentEventInfo() { reset(); };
   void reset() { Id=FPE_Noop; Sender=NULL; Key=0; MouseButton=0; X=0; Y=0;
                  Shift=false; Alt=false; Ctrl=false;
                  Left=false; Right=false; Middle=false;
                  Double_Clicked=false; };
};
typedef bool (*FosterParentEventHandlerT)(const FosterParentEventInfo &event);

enum EToolDisabledReason
{
   TOOL_DISABLED_LICENSE_NOT_YET_CHECKED_OUT,
   TOOL_DISABLED_TOOL_UNLICENSED,
   TOOL_DISABLED_MASTER_CLIP,
   TOOL_DISABLED_NON_HISTORY,
   TOOL_DISABLED_UNAVAILABLE,
   TOOL_DISABLED_NO_CLIP,
	TOOL_DISABLED_DISABLED,
   TOOL_DISABLED_VIEW_ONLY_MODE
};

//////////////////////////////////////////////////////////////////////
// Stuff moved here from ToolObject.h
//
enum EAutotoolStatus
{
   AT_STATUS_INVALID = 0,
   AT_STATUS_STOPPED,         // Autotool processing is now stopped
   AT_STATUS_RUNNING,         // Autotool processing is now running
   AT_STATUS_PAUSED,           // Autotool processing is now paused
   AT_STATUS_END_OF_MATERIAL,
   AT_STATUS_PROCESS_ERROR,
   AT_STATUS_PROCESS_THREAD_STARTED,
   AT_STATUS_PROCESS_THREAD_ENDING
};

enum EToolSetupType
{
   TOOL_SETUP_TYPE_INVALID,        // NOP
   TOOL_SETUP_TYPE_SINGLE_FRAME,   // Process a single frame, use Provisional
   TOOL_SETUP_TYPE_MULTI_FRAME,    // Process a range of frames
   TOOL_SETUP_TYPE_TRAINING,       // Process a single frame without the
                                   // prospect of writing back to disk
   TOOL_SETUP_TYPE_PRELOAD_ONLY    // Load the input buffers then bail out
};

// Stuff moved here from GOVTool.h
//
enum ESelectedRegionShape
{
   SELECTED_REGION_SHAPE_NONE,
   SELECTED_REGION_SHAPE_RECT,
   SELECTED_REGION_SHAPE_LASSO,
   SELECTED_REGION_SHAPE_BEZIER,
   SELECTED_REGION_SHAPE_REGION
};

// Results from the Accept Or Reject dialog
enum EAcceptOrRejectChangeResult
{
   PENDING_CHANGE_ACCEPT,
   PENDING_CHANGE_REJECT,
   PENDING_CHANGE_NO_ACTION
};

// Stoopid tilde hack gets even stoopider!
enum ETildeKeyHackMode
{
   TILDE_KEY_HACK_DRS_MODE,
   TILDE_KEY_HACK_PAINT_MODE,
   TILDE_KEY_HACK_OTHER_MODE
};


// AutoClean display HACK-O-RAMA!!
struct OverlayDisplayItem
{
   enum EType
   {
      ACOIT_rect,
      ACOIT_vector,
      ACOIT_shadow,
      ACOIT_rect_thick,
      ACOIT_rect_inclusive,
   };

   EType itemType;
   RECT rect;
   unsigned char R, G, B, A;
   unsigned char RS, GS, BS, AS;
   int id;
};

typedef vector<OverlayDisplayItem> OverlayDisplayList;

// Copied from MaskToolRegions.h
enum ETSIMaskRegionShape
{
   TSI_MASK_REGION_INVALID = 0,
   TSI_MASK_REGION_RECT,
   TSI_MASK_REGION_LASSO,
   TSI_MASK_REGION_BEZIER,
   TSI_MASK_REGION_QUAD
};


//////////////////////////////////////////////////////////////////////

class CToolSystemInterface
{
public:
	CToolSystemInterface();
	virtual ~CToolSystemInterface();

    // Stretch Box API
   // These functions are used to display a stretching rectangle that
   // tracks the mouse cursor position
   //    startStretchRectangleOrLasso() - Starts drawing the stretch rectangle with
   //       one corner at the mouse position when the function is called and
   //       the opposite corner tracks the current mouse position
   //    stopStretchRectangleOrLasso() - Stops drawing the stretch rectangle and
   //       removes it from screen.  Records the coordinates of the
   //       rectangle at the time function is called.
   //    getStretchRectangleCoordinates() - Obtains the coordinates of
   //       the stretch rectangle recorded when stopStretchRectangleOrLasso was
   //       called.  Rectangle is in clip image coordinate space.  Function
   //       returns 0 when a rectangle is available and non-zero when a
   //       rectangle is not available.  Rectangle is available after
   //       startStretchRectangleOrLasso then stopStretchRectangleOrLasso were called.
   //       Rectangle is not available if user "cancelled" the rectangle
   //       by stretching it in the opposite of the initial direction.
   virtual int  setGraphicsColor(int) = 0;
   virtual void setStretchRectangleColor(int) = 0;
   virtual void startStretchRectangleOrLasso() = 0;
   virtual void stopStretchRectangleOrLasso(bool dspfrm=true) = 0;
   virtual void cancelStretchRectangle() = 0;
   virtual int getStretchRectangleCoordinates(RECT &imageRect, POINT* &lasso) = 0;
   virtual RECT getStretchRectangleScreenCoords() = 0;
   virtual int getLastFrameIndex() = 0;
   virtual void drawRectangleFrame(RECT *) = 0;
   virtual void drawLassoFrame(POINT *lasso) = 0;
   virtual void drawBezierFrame(BEZIER_POINT *bezier) = 0;
   virtual void lockReader(bool) = 0;
   virtual bool getMousePositionFrame(int *newx, int *newy) = 0;
   virtual void clipRectangleFrame(RECT *) = 0;
   virtual void clipLassoFrame(POINT *lasso) = 0;

   // Zoom API
   virtual void zoom(const RECT &showRect) = 0;
   virtual void unzoom() = 0;

   // Color Mask API
   virtual void setRGBMask(int) = 0;
   virtual int  getRGBMask() = 0;

   // Paint API
   #define DISPLAY_MODE_TRUE_VIEW   0
   #define DISPLAY_MODE_1_TO_1      1
   #define DISPLAY_MODE_FIT_WIDTH   2
   virtual int  getDisplayMode() = 0;
   virtual void setDisplayMode(int) = 0;
   virtual void resetDisplayMode() = 0;
   virtual void enterPaintMode() = 0;
   virtual void initPaintHistory() = 0;
   virtual void enableAutoAccept(bool) = 0;
   virtual void initImportProxy(int, int) = 0;
   virtual void exitPaintMode() = 0;
   virtual void executeNavigatorToolCommand(int) = 0;
   virtual void displayFrameAndBrush() = 0;

// CAREFUL!!! THESE ARE DUPLICATED IN Displayer.h !!!!!!!
   #define CAPTURE_MODE_TARGET         0
   #define CAPTURE_MODE_IMPORT         1
   #define CAPTURE_MODE_BOTH           2
   #define CAPTURE_MODE_ORIG_TARGET    3
   #define CAPTURE_MODE_ORIG_IMPORT    4
   #define CAPTURE_MODE_ALTCLIP_IMPORT 5
   virtual void setCaptureMode(int) = 0;
   virtual int  getCaptureMode() = 0;

// CAREFUL!!! THESE ARE DUPLICATED IN Displayer.h !!!!!!!
   #define IMPORT_MODE_NONE     0
   #define IMPORT_MODE_ABSOLUTE 1
   #define IMPORT_MODE_RELATIVE 2
   #define IMPORT_MODE_ORIGINAL 3
   #define IMPORT_MODE_ALTCLIP  4
   virtual void setImportMode(int) = 0;
   virtual int  getImportMode() = 0;

   virtual  int setImportFrameClipName(const string &clipName) = 0;
   virtual string getImportFrameClipName() = 0;
   virtual ClipSharedPtr getImportClip() = 0;
   virtual CVideoFrameList* getImportClipVideoFrameList() = 0;
   virtual  int computeDesiredImportFrame(int) = 0;
   virtual void setImportFrameOffset(int) = 0;
   virtual  int getImportFrameOffset() = 0;
   virtual int syncImportFrameTimecode() = 0;
   virtual void preloadImportFrame(int, int,
                                   double, double, double,
                                   double, double, double) = 0;
   virtual void forceTargetAndImportFrameLoaded() = 0;                                     
   virtual void loadTargetAndImportFrames(int)= 0;
   virtual void setImportFrameTimecode(int) = 0;
   virtual  int getImportFrameTimecode() = 0;
   virtual void clearPaintFrames() = 0;
   virtual bool getPaintFrames(int *trg, int *imp) = 0;
   virtual void unloadTargetAndImportFrames() = 0;
   virtual bool targetFrameIsModified() = 0;
   virtual void enableForceTargetLoad(bool) = 0;
   virtual void enableForceImportLoad(bool) = 0;
   virtual void loadTargetFrame(int) = 0;
   virtual void setTargetMark() = 0;
   virtual void reloadModifiedTargetFrame() = 0;
   virtual void redrawTargetFrame() = 0;
   virtual void reloadModifiedImportFrame() = 0;
   virtual void reloadUnmodifiedTargetFrame() = 0;
   virtual MTI_UINT16 *getTargetIntermediate() = 0;
   virtual MTI_UINT16 *getTargetOriginalIntermediate() = 0;
   virtual MTI_UINT16 *getTargetPreStrokeIntermediate() = 0;
   virtual void loadImportFrame(int) = 0;
   virtual void setImportMark() = 0;

// CAREFUL!!! THESE ARE DUPLICATED IN Displayer.h !!!!!!!
   #define ONIONSKIN_MODE_OFF    0
   #define ONIONSKIN_MODE_COLOR  1
   #define ONIONSKIN_MODE_POSNEG (2+4)      // hahaha wtf??
   virtual void setOnionSkinMode(int) = 0;
   virtual void setOnionSkinTargetWgt(double) = 0;

   virtual void editMask() = 0;
#define CANCELLED              0
#define ACQUIRING_IMPORT_FRAME 6
#define MOVING_IMPORT_FRAME    7
#define EDIT_MASK              8
#define ACQUIRING_PIXELS       9
#define PAINTING_PIXELS       10
#define TRACKING_CLONE_PT     11
#define PICKING_COLOR         12
#define PICKING_SEED          13
#define HEALING_PIXELS        14
#define PICKING_AUTOALIGN     15
   virtual void setMouseMinorState(int) = 0;
   virtual void setColorPickSampleSize(int) = 0;
   virtual void getColorPickSample(int, int, int&, float&, float&, float&) = 0;
   virtual void clearFillSeed() = 0;
   virtual void setFillTolerance(int) = 0;
   virtual void recycleFillSeed() = 0;
   virtual void setDensityStrength(int) = 0;
   virtual void setGrainStrength(int) = 0;
   virtual void setGrainPresets(GrainPresets *) = 0;
   virtual void moveImportFrame(int, int) = 0;
   virtual void setMainWindowFocus() = 0;
   virtual TForm *getBaseToolForm() = 0;
   virtual void setMainWindowAutofocus(bool onoff) = 0;
   virtual void enableBrushDisplay(bool) = 0;
   virtual void loadPaintBrush(CBrush *, int, int, int, float *) = 0;
   virtual CBrush *getPaintBrush(int *, int *, bool *, float *) = 0;
   virtual void loadCloneBrush(CBrush *, bool, int, int) = 0;
   virtual CBrush *getCloneBrush(bool *, int *, int *) = 0;
   virtual void unloadPaintBrush() = 0;
   virtual void unloadCloneBrush() = 0;
   virtual void setAutoAlignBoxSize(int) = 0;
   virtual void setDisplayProcessed(bool) = 0;
   virtual void mouseDn() = 0;
   virtual void mouseMv(int, int) = 0;
   virtual void mouseUp() = 0;
   virtual CPixelRegionList& getOriginalPixels() = 0;
   virtual int  getToolFrame(MTI_UINT16 *, int) = 0;
   virtual int  putToolFrame(MTI_UINT16 *, MTI_UINT16 *, CPixelRegionList&, int) = 0;
   virtual int  putToolFrame(MTI_UINT16 *, MTI_UINT16 *, MTI_UINT16 *, int) = 0;
   virtual void makeStrokeStack() = 0;
   virtual void clearStrokeStack() = 0;
   virtual int pushStrokeFromTarget(int) = 0;
   virtual int popStrokesToTarget(int) = 0;
   virtual void completeMacroStroke() = 0;
   virtual void paintOneBrushPositionFrame(int, int) = 0;
   virtual void paintOneStrokeSegmentFrame(int, int, int, int) = 0;
   virtual int initMaskForInpainting() = 0;
   virtual int iterateOneInpaintCycle() = 0;
   virtual int getMaxComponentValue() = 0;
   virtual void forceMouseToWindowCenter() = 0;
   virtual void useTrackingAlignOffsets(bool) = 0;
   virtual void clearAllTrackingAlignOffsets() = 0;
   virtual void setTrackingAlignOffsets(int frame, double xoff, double yoff) = 0;
	virtual void getTrackingAlignOffsets(int frame, double &xoff, double &yoff) = 0;
	virtual bool doesFrameHaveAnyHistory(int frame) = 0;
	virtual void setAltClipDiffThreshold(float newThreshold) = 0;
	virtual void setOrigValDiffThreshold(float newThreshold) = 0;
	virtual void setPaintToolChangesPending(bool flag) = 0;
	virtual bool arePaintToolChangesPending() = 0;

   // Registration API
   virtual void unloadRegistrationFrame() = 0;
   virtual void loadRegistrationFrame(int) = 0;
   virtual void clearLoadedRegistrationFrame() = 0;
   virtual void clearRegisteredRegistrationFrame() = 0;
   virtual MTI_UINT16 *getPreRegisteredIntermediate() = 0;
   virtual MTI_UINT16 *getRegisteredIntermediate() = 0;
   virtual void displayFromRenderThread(MTI_UINT16 *, int) = 0;
   virtual void enterRegistrationMode() = 0;
   virtual void exitRegistrationMode() = 0;
   virtual void SetMatBox(RECT *) = 0;
   virtual void GetMatBox(RECT *) = 0;
   virtual void SetRegistrationTransform(AffineCoeff *) = 0;
   virtual void GetRegistrationTransform(AffineCoeff *) = 0;

   // DeWarp API
   virtual void getLastFrameAsIntermediate(MTI_UINT16 *) = 0;

   // Magnifier API
   virtual CMagnifier * allocateMagnifier(int wdth, int hght, RECT *magwin) = 0;
   virtual void freeMagnifier(CMagnifier *mag) = 0;

   // Timeline API
   virtual CTimeLine *getClipTimeline() = 0;
   virtual void getTimelineVisibleFrameIndexRange(int &inIndex, int &exclusiveOutIndex) = 0;
	virtual void getCutList(vector<int> &cutList) = 0;
   virtual int getShowAlphaState() = 0; // Tri-state (-1 = disabled, 0 = off, 1 = on)
   virtual void setShowAlphaState(bool newState) = 0;

   //  These get data from the player
   virtual bool isDisplaying() = 0;
   virtual long getIdleTime() = 0;
   virtual void fullStop() = 0;
   virtual void goToFrame(int) = 0;
   virtual void goToFrameSynchronous(int) = 0;
   virtual void refreshFrame() = 0;
   virtual void refreshFrameCached(bool fast = false) = 0;

// CAREFUL!!! THESE ARE DUPLICATED IN Player.h !!!!!!!
// CAREFUL!!! THEASE ARE FLAGS!
#define PLAYBACK_FILTER_INVALID           -1
#define PLAYBACK_FILTER_NONE               0
#define PLAYBACK_FILTER_ORIGINAL_VALUES    1
#define PLAYBACK_FILTER_HIGHLIGHT_FIXES    2
#define PLAYBACK_FILTER_BOTH               (PLAYBACK_FILTER_ORIGINAL_VALUES + PLAYBACK_FILTER_HIGHLIGHT_FIXES)
#define PLAYBACK_FILTER_IMPORT_FRAME_DIFFS 4

   virtual void setPlaybackFilter(int newFilter, bool refresh=false) = 0;
   virtual int  getPlaybackFilter() = 0;
   virtual const CImageFormat * getVideoClipImageFormat() = 0;
   virtual void fattenRectangleFrame(RECT *, int) = 0;
   virtual void setDRSLassoMode(bool state) = 0;
   virtual bool getDRSLassoMode() = 0;
   virtual MTI_INT32 getMarkIn() = 0;
   virtual MTI_INT32 getMarkOut() = 0;
   virtual void setMarkIn(MTI_INT32 nFrame) = 0;
   virtual void setMarkOut(MTI_INT32 nFrame) = 0;
   virtual bool isAClipLoaded() = 0;
   virtual bool isMasterClipLoaded() = 0;
   virtual bool doesClipHaveVersions() = 0;
	virtual void toolPlayClipFrameRangeInclusive(int firstFrameIndex, int lastFrameIndex) = 0;

   // Interact with the MainWindow
   virtual void UpdateStatusBar(const string &, int) = 0;
   virtual RECT GetClientRectScreenCoords() = 0;
   virtual RECT getImageRectangle() = 0;

   virtual void *GetMainWindowHandle() = 0;
   virtual int scaleXFrameToClient(int) = 0;
   virtual int scaleYFrameToClient(int) = 0;

   // Save & Restore Interface
   virtual int DisplayAllReviewRegions(int frameIndex) = 0;
   virtual int DisplaySingleReviewRegion(bool goNext, bool goZoom, int frameIndex) = 0;
   virtual int GoToNextOrPreviousReviewFrame(bool goNext, int frameIndex) = 0;
   virtual int SelectReviewRegion(CPixelRegionList& reviewRegion) = 0;
   virtual int ClearReviewRegions(bool doRedraw) = 0;
   virtual int MarkReviewRegions() = 0;
   virtual int ReviseAllGOVRegions() = 0;
   virtual int ReviseGOVRegions(int frameNumber) = 0;
   virtual int DiscardGOVRevisions() = 0;

   virtual int RestoreToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                      RECT *filterRect, POINT *filterLasso,
                                      BEZIER_POINT *filterBezier,
                                      CPixelRegionList *filterPixelRegion) = 0;
   virtual int SaveToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                 int toolNumber, const string& toolString,
                                 bool saveToHistory) = 0;
   virtual void ValidateHistory(int frameIndex) = 0;
   virtual void DiscardUnvalidatedHistory() = 0;

   // Clip API
   virtual string getClipFilename() = 0;
   virtual int getVideoProxyIndex() = 0;
   virtual int getVideoFramingIndex() = 0;
   virtual ClipSharedPtr getClip() = 0;
   virtual CVideoFrameList* getVideoFrameList() = 0;
   virtual CRenderDestinationClip* getRenderDestinationClip() = 0;
#define CLIP_DEST_SRC 0
#define CLIP_DEST_NEW 1
   virtual void initRenderDestinationClip(int) = 0;
   virtual vector<string> getClipMetadataDirectoryChain(const string &metadataTypeName = string("")) = 0;

   // Get function pointers to the main window's keyboard and mouse
   // event handlers.  The main window's event handlers can be attached
   // to the Tool's windows so that user input is passed to the main window.
   // Return types are platform-dependent typedefs defined above.
#ifndef NO_GUI_HACK
   virtual MTIKeyEventHandler getKeyDownEventHandler() = 0;
   virtual MTIKeyEventHandler getKeyUpEventHandler() = 0;
   virtual MTIMouseButtonEventHandler getMouseButtonDownEventHandler() = 0;
   virtual MTIMouseButtonEventHandler getMouseButtonUpEventHandler() = 0;
   virtual MTIMouseMoveEventHandler getMouseMoveEventHandler() = 0;
   virtual MTIMouseWheelEventHandler getMouseWheelEventHandler() = 0;
#endif

   // Tool Manager API
   virtual bool DeactivateTool(const CToolObject *toolObj) = 0;

   virtual int MakeSimpleToolSetup(const string& toolName,
                                   EToolSetupType newToolSetupType,
                                   CToolIOConfig *toolIOConfig) = 0;
   virtual int DestroyToolSetup(int toolSetupHandle) = 0;
   virtual int SetActiveToolSetup(int newToolSetupHandle) = 0;
   virtual int GetActiveToolSetupHandle() = 0;
   virtual int SetToolFrameRange(CToolFrameRange *toolFrameRange) = 0;
   virtual int SetToolParameters(CToolParameters *toolParameters) = 0;
   virtual int RunActiveToolSetup() = 0;
   virtual int StopActiveToolSetup() = 0;
   virtual int EmergencyStopActiveToolSetup() = 0;
   virtual int PauseActiveToolSetup() = 0;
   virtual EAutotoolStatus GetActiveToolSetupStatus() = 0;
   virtual EAutotoolStatus GetToolSetupStatus(int toolSetupHandle) = 0;
   virtual void WaitForToolProcessing() = 0;

   virtual CBufferPool* GetBufferAllocator(int bytesPerBuffer, int bufferCount,
                                           double invisibleFieldsPerFrame) = 0;
   virtual void ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                                       double invisibleFieldsPerFrame) = 0;

   // Provisional Interface
   virtual void ToggleProvisional(bool goZoom=false) = 0;
   virtual void AcceptProvisional(bool goToFrame) = 0;
   virtual void AcceptProvisionalInBackground() = 0;
   virtual void RejectProvisional(bool goToFrame) = 0;
   virtual void RejectProvisionalInBackground() = 0;
   virtual void RefreshProvisional() = 0;
   virtual void ClearProvisional(bool) = 0;
   virtual bool IsProvisionalPending() = 0;
   virtual bool IsProcessedVisible() = 0;
   virtual CToolFrameBuffer* GetProvisionalFrame() = 0;
   virtual int GetProvisionalFrameIndex() = 0;
   virtual int SetProvisionalHighlight(bool newHighlightFlag) = 0;
   virtual int SetProvisionalRender(bool newRenderFlag) = 0;
   virtual int SetProvisionalReprocess(bool newReprocessFlag) = 0;
   virtual int SetProvisionalReview(bool newReviewFlag) = 0;

   virtual bool CheckProvisionalUnresolved() = 0;
   virtual bool CheckToolProcessing() = 0;
   virtual bool CheckProvisionalUnresolvedAndToolProcessing() = 0;
   virtual void UpdateProvisionalStatusBarPanel() = 0;

   // PDL & Rendering
   virtual int CapturePDLEntry(bool all = false) = 0;
   virtual bool IsPDLRendering() = 0;
   virtual bool IsPDLEntryLoading() = 0;

   // Provisional Interface - extensions for interface to Provisional
   // pseudo-tool.  Not to be used by regular tools
   virtual int RegisterProvisionalToolProc(CProvisionalToolProc *toolProc) = 0;
   virtual int PutFrameToProvisional(CToolFrameBuffer *toolFrame) = 0;

   // Mask Tool Interface
   virtual void SetMaskAllowed(bool regularMode, bool roiMode = false, bool initialRoiModeState = false) = 0;
   virtual bool IsMaskEnabled() = 0;
   virtual bool IsMaskAvailable() = 0;
   virtual bool IsMaskVisible() = 0;
   virtual bool IsMaskVisibilityLockedOff() = 0;
   virtual bool IsMaskAnimated() = 0;
   virtual bool IsMaskExclusive() = 0;
   virtual bool IsMaskInRoiMode() = 0;
   virtual CRegionOfInterest* GetMaskRoi() = 0;
   virtual int GetMaskRoi(int frameIndex, CRegionOfInterest &maskRoi) = 0;
   virtual void ClearMask() = 0;
   virtual void SetMaskToolAutoActivateRoiModeOnDraw(bool flag) = 0;
   virtual void SetMaskFromPdlEntry(CPDLEntry &pdlEntry) = 0;
   virtual int SaveMaskToFile(const string &filename) = 0;
   virtual int LoadMaskFromFile(const string &filename) = 0;
   virtual string GetMaskAsString() = 0;

   // PAINT MASK SHIT
   virtual void CalculateMaskROI(int) = 0;
   virtual void SetMaskTransformEditorIdleRefreshMode(bool onOffFlag) = 0;
   virtual void EnableMaskOutlineDisplay(bool) = 0;
   virtual void setMaskVisibilityLockedOff(bool flag) = 0;
   virtual void ExecuteMaskToolCommand(int mskcmd) = 0;

   ///////////////////////////////
   // Hack for tool to set mask //
   ///////////////////////////////
   virtual void setRectToMask(const RECT &Rect) = 0;
   virtual void setMaskRoiIsRectangularOnly(bool flag) = 0;

   // When in ROI mode and user draws an ROI, this returns the shape drawn.
   // Here rect is passed by reference and is always filled in to be the
   // bounding rect of the selected ROI. If the shape is a lasso and the
   // lasso parameter is non-NULL, it must point to a POINT array of size
   // MAX_LASSO_PTS, which gets filled in. If the shape is a bezier and the
   // bezier parameter is non-null, it must point to a BEZIER_POINT array
   // of size MAX_BEZIER_PTS, which gets filled in.
   virtual ESelectedRegionShape GetROIShape(RECT &rect,
                                            POINT *lasso,
                                            BEZIER_POINT *bezier) = 0;
   virtual void SetROIShape(ESelectedRegionShape shape,
                            const RECT &rect,
                            const POINT *lasso,
                            const BEZIER_POINT *bezier) = 0;
   virtual void RestorePreviousROIShape() = 0;
   virtual void LockROI(bool flag) = 0;

   // from Mask Tool to active tool:
   virtual bool NotifyStartROIMode() = 0; // Tool returns TRUE if ROI mode OK
   virtual bool NotifyUserDrewROI() = 0;  // Tool returns TRUE if handled
	virtual void NotifyEndROIMode() = 0;
   virtual void NotifyMaskChanged(const string &newMaskAsString) = 0;
   virtual void NotifyMaskVisibilityChanged() = 0;

   // Reticle Tool Interface
   virtual void InternalDisableReticleTool() = 0;
   virtual void InternalEnableReticleTool() = 0;

   // Bezier Editor for selection regions
   virtual void BezierMouseDown() = 0;
   virtual void BezierMouseMove(int x, int y) = 0;
   virtual void BezierMouseUp() = 0;
   virtual void BezierCtrlMouseDown() = 0;
   virtual void BezierShiftMouseDown() = 0;
   virtual void ClearBezier(bool doRedraw) = 0;
   virtual void FreeBezier(BEZIER_POINT* bezierPts) = 0;
   virtual bool GetBezierExtent(RECT &) = 0;
   virtual BEZIER_POINT* GetBezier() = 0;
   virtual bool IsBezierClosed() = 0;
   virtual void RedrawBezier() = 0;

   // Transform Editor for Paint II
   virtual void SetToolPaintInterface(CToolPaintInterface *) = 0;
   virtual void TransformPositionImportFrame() = 0;
   virtual void TransformMouseDown() = 0;
   virtual void TransformMouseMove(int x, int y) = 0;
   virtual void TransformMouseUp() = 0;
   virtual void TransformCtrlMouseDown() = 0;
   virtual void TransformShiftMouseDown() = 0;
   virtual void ClearTransform(bool doRedraw) = 0;
   virtual void SaveTransform() = 0;
   virtual void RestoreTransform() = 0;
   virtual void SetRotation(double) = 0;
   virtual void SetSkew(double) = 0;
   virtual void SetStretchX(double) = 0;
   virtual void SetStretchY(double) = 0;
   virtual void SetOffsetX(double) = 0;
   virtual void SetOffsetY(double) = 0;
   virtual void SetOffsetXY(double, double) = 0;
   virtual void SetTransform(double, double, double,
                             double, double, double) = 0;
   virtual void GetTransform(double *, double *, double *,
                             double *, double *, double *) = 0;
   virtual void RedrawTransform() = 0;
   virtual void TransformTimer() = 0;

   // for Tracking Points Editor in DeWarp
   virtual double dscaleXClientToFrame(int) = 0;
   virtual double dscaleYClientToFrame(int) = 0;
   virtual void drawUnfilledBoxFrameNoFlush(double x, double y) = 0;
   virtual void drawFilledBoxFrameNoFlush(double x, double y) = 0;
   virtual void drawUnfilledBoxScaledNoFlush(double x, double y, double rad) = 0;
   virtual void drawFilledBoxScaledNoFlush(double x, double y, double rad) = 0;
   virtual void drawArrowFrameNoFlush(double frx, double fry,
                                      double tox, double toy,
                                      double wdt, double len) = 0;

   // New accurate pixel tracking boxes for stabilization:
   // Nested boxes with optional crosshairs - use 0 for outer radius if
   // you don't want an outer box.
   virtual void drawTrackingBoxNoFlush(double centerX, double centerY,
                                       double innerRadiusX, double innerRadiusY,
                                       double outerRadiusX, double outerRadiusY,
                                       bool showCrosshairs) = 0;
   virtual void flushGraphics() = 0;

   // Methods to pass the Component (YUV) values from Navigator

   virtual int GetYcomp() = 0;
   virtual int GetUcomp() = 0;
   virtual int GetVcomp() = 0;
   virtual int getBitsPerComponent() = 0;
   virtual int getPixelComponents() = 0;
   virtual bool isMouseInFrame() = 0;

   // Cursor control API

   virtual void showCursor(bool showFlag) = 0;
   virtual void enterCursorOverrideMode(int CursorIndex) = 0;
   virtual void exitCursorOverrideMode() = 0;
   virtual void screenToClientCoords(int inX, int inY, int &outX, int &outY) = 0;
   virtual void informToolWindowStatus(bool windowActive) = 0;
   virtual void setPreferredFrameCursor(TCursor cursor) = 0;

   // Base tool ("Plugin Adopter") API

   // 'topPanel' arg must be a TPanel* that is the ancestor of all
   // of the adopted tool GUI's controls

   // These get put in the tag of the control we use as a notification
   // hack to the original GUI forms of the tools
   virtual int AdoptPluginToolGUI(
             const string &toolName, int tabIndex, int *topPanel,
             FosterParentEventHandlerT eventHandler) = 0;
   virtual void UnadoptPluginToolGUI(int tabIndex) = 0;
   virtual void FocusAdoptedControl(int *control) = 0;
   // The following really returns a TWinControl*
   virtual int *GetFocusedAdoptedControl(void) = 0;
   virtual void DisableTool(EToolDisabledReason reason) = 0;
   virtual void EnableTool() = 0;
   virtual bool WarnThereIsNoUndo() = 0;  // true = OK, false = Cancel
   // The following really returns a TComponent*
   virtual int *GetBaseToolFormHandle() = 0;  // for dialog positioning
   virtual EAcceptOrRejectChangeResult ShowAcceptOrRejectDialog(
                             bool *turnOnAutoAccept=NULL) = 0;

   ////////////////////////
   // GOV Pseudotool API //
   ////////////////////////

   // from GOV to active tool:
   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr) = 0;
   virtual void NotifyGOVDone() = 0;
   virtual void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                      const RECT &rect,
                                      const POINT *lasso) = 0;
   virtual void NotifyRightClicked() = 0;

   // from active tool to GOV
   virtual bool IsGOVInProgress() = 0;
   virtual bool IsGOVPending() = 0;
   virtual void AcceptGOV() = 0;
   virtual void RejectGOV() = 0;
   virtual void SetTildeKeyHackMode(ETildeKeyHackMode newMode) = 0;
   virtual void SetGOVRepeatShape(ESelectedRegionShape shape,
                                  const RECT &rect,
                                  const POINT *lasso,
                                  const CPixelRegionList *regionList) = 0;
   virtual void SetGOVToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor) = 0;

   /////////////////////////////
   // Tracking Pseudotool API //
   /////////////////////////////

   // from Tracking to active tool:
   virtual bool NotifyTrackingStarting() = 0;
   virtual void NotifyTrackingDone(int errorCode) = 0;
   virtual void NotifyTrackingCleared() = 0;

   // from active tool to Tracking:
   virtual bool LockTrackingTool() = 0;
   virtual void ReleaseTrackingTool() = 0;
   virtual void SetTrackingToolEditor(TrackingBoxManager *newEditor) = 0;
   virtual void SetTrackingToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor) = 0;
   virtual void ActivateTrackingTool() = 0;
   virtual void DeactivateTrackingTool() = 0;
   virtual void StartTracking() = 0;
   virtual void StopTracking() = 0;


   //////////////////////////
   // Tool Frame Cache API //
   //////////////////////////

   virtual CToolFrameCache *GetToolFrameCache() = 0;


   ///////////////////////////////
   // Background Thread Support //
   ///////////////////////////////

   virtual void FireRefreshTimerFromBackground() = 0; // force heartbeat


   /////////////////////
   // AutoClean APIs  //
   /////////////////////

   // Get enabled flag from master ini file
   virtual bool IsAutoCleanEnabled() = 0;

   // Debris file access
//   virtual int GetAutoCleanDebrisVersion() = 0;
//   virtual string GetAutoCleanDebrisFileName(int frameIndex) = 0;
//   virtual CAutoCleanDebrisFile *GetAutoCleanDebrisFile(int frameIndex) = 0;
//   virtual void ReleaseAutoCleanDebrisFile(CAutoCleanDebrisFile *debrisFile) = 0;

   // AutoClean display HACK-O-RAMA
   virtual void SetAutoCleanOverlayList(const OverlayDisplayList &newList) = 0;
   virtual void MakeSureThisRectIsVisible(const RECT &rect) = 0;
   virtual void GetDisplayLutValues(MTI_UINT8 *outputValues) = 0;
   virtual int GetListOfSmallHistoryRectangles(
                  int frameIndex,
                  int sizeLimit,             // max size of a side, in pixels
                  vector<RECT> &rectList) = 0;   // output
   virtual int GetOriginalValues(
                  int frameIndex,
                  RECT filterRect,          // INCLUSIVE right & bottom
                  MTI_UINT16 *outBuf) = 0;  // filterRect size,
                                            // prefill with current values

   /////////////////////
   // Shameless Hacks //
   /////////////////////

   virtual bool IsPlayerReallyPlaying() = 0;
   virtual void TellTheDisplayerToRedisplayTheCurrentFrame() = 0;

   #define SHAPE_HACK_ELLIPSE 0
   #define SHAPE_HACK_RECTANGLE 1
   virtual void TellTheDisplayerToDrawAShape(
      double centerX, double centerY,
      int shapeType, int radius, int aspectRatio, int angle,
      MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B) = 0;

   virtual void IDontNeedThisKeyDownEvent(WORD &Key, TShiftState Shift) = 0;

	// Preview hack for color breathing tool
	virtual void enterPreviewHackMode() = 0;
	virtual void exitPreviewHackMode() = 0;

	virtual void NotifyStartViewOnlyMode() = 0;
	virtual void NotifyEndViewOnlyMode() = 0;


   ///////////////////////////////
   // Main Window Top Panel API //
   ///////////////////////////////

   virtual void        SetMainWindowTopPanelVisibility(bool onOff) = 0;
   virtual bool        GetMainWindowTopPanelVisibility()           = 0;
   virtual CMTIBitmap *GetBitmapRefForTopPanelDrawing()            = 0;
   virtual void        DrawBitmapOnMainWindowTopPanel()            = 0;

	// User guide viewer API
	virtual void SetToolNameForUserGuide(const string &toolName) = 0;

};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOL_SYSTEM_INTERFACE_H)

