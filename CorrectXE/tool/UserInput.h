#ifndef UserInputH
#define UserInputH

// UserInput.h: interface for the CUserInput class.
//
// Description: The CUserInput class is a platform-independent wrapper
//              around the platform-dependent information contained in
//              user-input events
/*
$Header: /usr/local/filmroot/tool/include/UserInput.h,v 1.6 2007/01/24 20:29:14 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifdef __BORLANDC__
#include <vcl.h>
#define KEY_SHIFT 16
#endif // #ifdef __BORLANDC_)

#ifdef __sgi
#include <Xm/Xm.h>
#define KEY_SHIFT 65505
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////

// Mask constants for "shift"
#define USER_INPUT_SHIFT_MASK_NONE     0x00
#define USER_INPUT_SHIFT_MASK_SHIFT    0x01
#define USER_INPUT_SHIFT_MASK_CONTROL  0x02
#define USER_INPUT_SHIFT_MASK_ALT      0x04
#define USER_INPUT_SHIFT_MASK_PHYS_ALT 0x08
#define USER_INPUT_SHIFT_MASK_LBUTTON  0x10
#define USER_INPUT_SHIFT_MASK_MBUTTON  0x20
#define USER_INPUT_SHIFT_MASK_RBUTTON  0x40

// Masks shift, control and alt state bits, 
// but ignoring mouse button contribution to shift
#define SHIFT_STATE_MASK (USER_INPUT_SHIFT_MASK_SHIFT | USER_INPUT_SHIFT_MASK_CONTROL | USER_INPUT_SHIFT_MASK_ALT)
 
//////////////////////////////////////////////////////////////////////

enum EUserInputAction
{
   USER_INPUT_ACTION_INVALID = 0,
   USER_INPUT_ACTION_KEY_DOWN,          // Keyboard key pressed
   USER_INPUT_ACTION_KEY_UP,            // Keyboard key released
   USER_INPUT_ACTION_MOUSE_BUTTON_DOWN, // Mouse button pressed
   USER_INPUT_ACTION_MOUSE_BUTTON_UP,   // Mouse button released
   USER_INPUT_ACTION_MOUSE_MOVE,        // Mouse has moved
   USER_INPUT_ACTION_MOUSE_DOUBLE_CLICK // Mouse button double click
};

//////////////////////////////////////////////////////////////////////

class CKeyNameTable
{
public:

struct SKeyName
{
   int keyCode;
   const char *keyName;
};

   CKeyNameTable(SKeyName *newKeyTable, int newKeyTableSize);
   ~CKeyNameTable();

	int findKeySymbol(const char* targetKeyName) const;
	const char* findKeyName(int targetKeySym) const;

private:

   SKeyName *keyTable;
   int keyTableSize;
};


//////////////////////////////////////////////////////////////////////

class CUserInput  
{
public:
	CUserInput();
	virtual ~CUserInput();

   void clearUserInput();

   // Comparison operators
   bool operator==(const CUserInput& rhs) const;
   bool operator!=(const CUserInput& rhs) const;

	const CKeyNameTable* getKeyNameTable() const;

   // Functions to initialize key mapping from .ini file or
   // to write out key mapping
	int makeIniUserInputMap(char *keyMapString);
	int parseIniUserInputMap(const char* keyMapString);

   // Functions to set CUserInput from platform-dependent user input events
#ifdef __BORLANDC__
   void setFromKeyEvent(EUserInputAction newAction, WORD &borlandKey,
                               TShiftState &borlandShift);
	void setFromMouseEvent(EUserInputAction newAction,
                                 TMouseButton &BorlandMouseButton,
                                 TShiftState &borlandShift, int x, int y);
   void setFromMouseMoveEvent(TShiftState &borlandShift, int x, int y);
	static unsigned int convertFromBorlandShift(TShiftState &borlandShift);
#endif // #ifdef __BORLANDC_)

#ifdef __sgi
   void setFromKeyEvent(XKeyEvent *keyEvent);
   void setFromMouseEvent(XButtonEvent *buttonEvent);
   void setFromMouseMoveEvent(XMotionEvent *motionEvent);
   static unsigned int convertFromXShift(unsigned int XState);
#endif // #ifdef __sgi

#if !defined(__sgi) && !defined(__BORLANDC__)
   D'oh! Input is not going to work!!
#endif

   // Guess whether a particular key is "shifted"
   static bool isKeyShifted(unsigned int key);

public:
   // Member variables.  All are public to simplify access

   EUserInputAction action;// User Input action code

   unsigned int key;       // Platform-dependent key code when action is 
                           // USER_INPUT_ACTION__KEY_DOWN or _KEY_UP.
                           // Platform-dependent mouse button code when 
                           // action is USER_INPUT_ACTION__MOUSE_BUTTON_DOWN
                           // or _MOUSE_BUTTON_UP
   unsigned int shift;     // Platform-independent bit flag indicating state 
                           // of <Shift>, <Ctrl> and <Alt> keys plus state 
                           // of mouse buttons.  See constants 
                           // USER_INPUT_SHIFT_MASK_

   // Window coordinates (valid on actions _MOUSE_BUTTON_DOWN, _MOUSE_BUTTON_UP
   // and _MOUSE_MOVE)
   int windowX;
   int windowY;

   // Image coordinates (valid on actions _MOUSE_BUTTON_DOWN, _MOUSE_BUTTON_UP
   // and _MOUSE_MOVE)
   int imageX;
   int imageY;

private:
   static CKeyNameTable *keyNameTable;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(USERINPUT_H)


