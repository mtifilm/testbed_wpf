#ifndef TrackingBoxManagerH
#define TrackingBoxManagerH
//----------------------------------------------------------------------------

#include "machine.h"
#include "TrackingBox.h"
//----------------------------------------------------------------------------

class TrackingBoxSet;
class CToolSystemInterface;
//----------------------------------------------------------------------------

#define DECOUPLE_MARKS_FROM_TRACKED_RANGE 0

#define MOD_KEY_NONE  0
#define MOD_KEY_SHIFT 1
#define MOD_KEY_ALT   2
#define MOD_KEY_CTRL  4
//----------------------------------------------------------------------------
// Colors hack
enum ETPColor
{
   ETPRed,
   ETPGreen,
   ETPBlue,
   ETPYellow,
};
//----------------------------------------------------------------------------

class TrackingBoxManager
{
public:

   TrackingBoxManager();

   void loadSystemAPI(CToolSystemInterface *newSystemAPI);
   void setIgnoringMarksFlag(bool newIgnoreFlag);
   void setTrailSize(int numberOfPoints);
   void setCanMoveTrackPointsAtAnyFrame(bool flag);
   void setCanSelectMultiplePoints(bool flag);
   void setCanDragPoints(bool flag);
   void setPanningMode(bool flag);

   void setTrackingBoxSizes(
            int newTrackBoxExtentX,     int newTrackBoxExtentY,
            int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
            int newSearchBoxExtentUp,   int newSearchBoxExtentDown);

   void setTrackingArray(TrackingBoxSet *newArray, bool preserveTrackingData = false);
   TrackingBoxSet *getTrackingArray();
   void resetTrackingArray();
	void removeAllTrackingBoxes();
   void invalidateAllTrackingData();
	bool isTrackingDataValidOverEntireRange() const;
	bool isFrameTrackingDataValid(int frame) const;
	bool wasTrackingDataInvalidatedByRendering();

	void notifyTrackingStopped();
	void notifyTrackingComplete();
   void notifyTrackingInvalid();
	void notifyFrameRendered(int frame);
	void notifyRenderComplete();

   void setRange(int newStartFrame, int newStopFrame);
   void setRangeFromMarks();
	void getRange(int &startFrame, int &stopFrame) const;

// CAREFUL: 0 (FALSE) Means you CAN change the marks!!!
#define TRK_IN_MARK_CAN_CHANGE                 0
#define TRK_IN_MARK_IS_DELETED                 1
#define TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL   2
#define TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL  3
	int  canInMarkChange() const;

// CAREFUL: 0 (FALSE) Means you CAN change the marks!!!
#define TRK_OUT_MARK_CAN_CHANGE                0
#define TRK_OUT_MARK_IS_DELETED                1
#define TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL  2
#define TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL 3
	int  canOutMarkChange()const;

#define START_FRAME_CAN_CHANGE                 TRK_IN_MARK_CAN_CHANGE
#define START_FRAME_IS_DELETED                 TRK_IN_MARK_IS_DELETED
#define START_FRAME_IS_MOVED_INSIDE_INTERVAL   TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL
#define START_FRAME_IS_MOVED_OUTSIDE_INTERVAL  TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL
	int  checkStartFrameChange(int newStartFrame) const;

#define STOP_FRAME_CAN_CHANGE                  TRK_OUT_MARK_CAN_CHANGE
#define STOP_FRAME_IS_DELETED                  TRK_OUT_MARK_IS_DELETED
#define STOP_FRAME_IS_MOVED_INSIDE_INTERVAL    TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL
#define STOP_FRAME_IS_MOVED_OUTSIDE_INTERVAL   TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL
   int  checkStopFrameChange(int newStopFrame) const;

	int  getRangeBegin() const {return _startFrame; }
	int  getRangeEnd() const {return _stopFrame; }

   void mouseDown(int modKeys);
   void mouseMove(int x, int y);
   void mouseUp();

   void drawTrackingPointsCurrentFrame(int defaultColor = SOLID_RED);
   void drawLineFromSelectedTrackingPointToMousePosition(int color);
   void refreshTrackingPointsFrame();
   void updateLastSelectedTag();
   void setUniqueSelection(int tpIndex);
	void unselectAllTrackingPoints();
	void deleteSelectedTrackingPoints();
	int getNumberOfTrackingBoxes();

   SPROPERTY(int, lastSelectedTag, _lastSelectedTag, setLastSelectedTag, TrackingBoxManager);
   SPROPERTY(bool, displayTrackingPoints, _displayTrackingPoints, setDisplayTrackingPoints, TrackingBoxManager);
   CBHook rangeChange;    // Call on a range change
   CBHook TrackingPointStateChange; // Call when any state changes
   CBHook TrackingParameterChange;  // Call when points or range change
   CBHook NextSelectedTagIsANewTrackingPoint; // Call before setting lastSelectedTag for a new tracking point

private:
   int  trackBoxExtentX;
   int  trackBoxExtentY;
   int  searchBoxExtentLeft;
   int  searchBoxExtentRight;
   int  searchBoxExtentUp;
   int  searchBoxExtentDown;

   int  findTrackingPointInBox(DRECT &selbox);
   int  createTrackingPoint(double x, double y, bool sel);
   void translateSelectedTrackingPointsCurrentFrame(double delx, double dely);

   void selectTrackingBox(int trackingBoxTag);
   void unselectTrackingBox(int trackingBoxTag);
   void selectAllTrackingBoxes();
   void unselectAllTrackingBoxes();
   bool isTrackingBoxSelected(int trackingBoxTag) const;

   // setters for properties
   void setLastSelectedTag(int newlastSelectedTag);
   void setDisplayTrackingPoints(bool newDisp);

   int  _lastSelectedTag;
	int _trailSize;
	ETPColor _color;

	void invalidateTrackingDataIfItWasUsedForRendering();
	int _lastTrackingArrayFrameRendered = -1;
	bool _trackingArrayWasRendered;

   CToolSystemInterface *systemAPI;

   TrackingBoxSet *trackingArray;
   TagList _selectedTrackingBoxTags;

   int _startFrame;
   int _stopFrame;

   // Configuration flags.
   bool _isIgnoringMarks;
   bool _displayTrackingPoints;
   bool _canSelectMultiplePoints;
   bool _canMoveTrackPointsAtAnyFrame;
   bool _canDragPoints;

   // State flags.
   bool _isMouseButtonDown;
   bool _isABoxBeingDragged;
   bool _isInPanningMode;

   // mouse client coords
   int mouseXClient;
   int mouseYClient;

   // mouse frame coords
   double mouseXFrame;
   double mouseYFrame;

   // mouse deltas
   double deltaMouseXFrame;
   double deltaMouseYFrame;
};

#endif
