// ToolCommandTable.h: interface for the CToolCommandTable class.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolCommandTable.h,v 1.1.1.1 2001/07/25 14:16:45 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOLCOMMANDTABLE_H)
#define TOOLCOMMANDTABLE_H

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

class CToolCommandTable  
{
public:

struct STableEntry
{
   int commandNumber;      // Command Number.  Uniquely identifies a
                           // command for a tool. Must be a non-negative
                           // number
   const char* commandName;      // Command Name.  Uniquely identifies a command
                           // for a tool.  Must be not be empty or null.
};

public:
   CToolCommandTable(int newCommandCount, 
                     STableEntry *newTableEntries);
   virtual ~CToolCommandTable();

   const char * findCommandName(int targetCommandNumber) const;
   int findCommandNumber(const char *targetCommandName) const ;

private:
   int commandCount;
   STableEntry *tableEntries;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLCOMMANDTABLE_H)
