// ToolManager.cpp: implementation of the CToolManager class.
//
/*
$Header: /usr/local/filmroot/tool/ToolMgr/ToolManager.cpp,v 1.9.2.25 2009/10/21 02:28:32 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolManager.h"
#include "ImageFormat3.h"
#include "BufferPool.h"
#include "CPlugin.h"
#include "err_imgtool.h"
#include "err_tool.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "Provisional.h"
#include "ToolFrameBuffer.h"   // for cache hack
#include "ToolMediaIO.h"
#include "ToolNode.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"
#include "UserInput.h"
#include "systemid.h"

#include <math.h>

//////////////////////////////////////////////////////////////////////
// CToolManager's Static Data

bool                   CToolManager::isInitialized = false;
int                    CToolManager::activeToolIndex = -1;
int                    CToolManager::defaultToolIndex = -1;
vector<int>            CToolManager::firstDibsToolIndexList;
int                    CToolManager::currentFirstDibsToolIndex = -1;
CToolManager::ToolList CToolManager::toolList;
CPluginList*           CToolManager::pluginList = nullptr;
CToolManager::CreateToolSystemInterfaceFunc
                       CToolManager::createToolSystemInterface = 0;

bool                   CToolManager::newToolSetupCommandRequested = false;
EAutotoolCommand       CToolManager::requestedToolSetupCommand = AT_CMD_INVALID;
bool                   CToolManager::newToolProcessingCommandRequested = false;
EAutotoolCommand       CToolManager::requestedToolProcessingCommand = AT_CMD_INVALID;
vector<CToolSetup*>    CToolManager::toolSetupList;
CToolSetup*            CToolManager::activeToolSetup = 0;
vector<CToolManager::SToolSetupDestructionState>
                       CToolManager::toolSetupDestructionQueue;

// PDL Rendering
EToolControlState      CToolManager::pdlRenderingControlState
                                                  = TOOL_CONTROL_STATE_STOPPED;
EPDLRenderingStatus    CToolManager::pdlRenderingStatus
                                                = PDL_RENDERING_STATUS_STOPPED;
CPDLRenderInterface*   CToolManager::pdlRenderingAppInterface = 0;
CPDL*                  CToolManager::currentRenderingPDL = 0;
CPDLEntry*             CToolManager::currentRenderingPDLEntry = 0;
CPDLRenderingStats     CToolManager::pdlRenderingStats;
bool                   CToolManager::newPDLRenderingCommandRequested = false;
EAutotoolCommand       CToolManager::requestedPDLRenderingCommand
                                                              = AT_CMD_INVALID;
bool                   CToolManager::pdlRenderFlag = false;
bool                   CToolManager::stopOnPDLRenderErrorFlag = true;
int                    CToolManager::reportedPDLRenderingErrorCode = 0;
string                 CToolManager::reportedPDLRenderingErrorMsg;

CProvisional*          CToolManager::provisional = 0;
vector<CToolManager::SBufferAllocator>
                       CToolManager::bufferAllocatorList;

int                    CToolManager::lastMousePositionWindowX = 0;
int                    CToolManager::lastMousePositionWindowY = 0;
int                    CToolManager::lastMousePositionImageX = 0;
int                    CToolManager::lastMousePositionImageY = 0;

bool                   CToolManager::inHeartbeat = false;

// BORLAND BUG
//CPluginState*        CToolManager::pluginToolStateCache[MAX_CACHED_PLUGIN_STATE_COUNT];
CPluginStateWrapper*   CToolManager::pluginToolStateCache = nullptr;

CToolFrameCache*       CToolManager::toolFrameCache = nullptr;



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolManager::CToolManager()
{
   if (!isInitialized)
      {
      // Initialize CToolManager's static data

      // Hack to speed up tool menu generation
	  pluginToolStateCache = new CPluginStateWrapper[MAX_CACHED_PLUGIN_STATE_COUNT];
      for (int i = 0; i < MAX_CACHED_PLUGIN_STATE_COUNT; ++i)
         pluginToolStateCache[i].State(PLUGIN_STATE_INVALID);

      // Hack to recycle loaded tool frame buffers
      toolFrameCache = new CToolFrameCache;

      // Hack to prevent heartbeat re-entry damage
      inHeartbeat = false;

      // Remember that Tool Manager has been initialized
      isInitialized = true;
      }

   mouseOrientation = 0;
   updateStatusBarNow = false;
}

CToolManager::~CToolManager()
{
}

//////////////////////////////////////////////////////////////////////
// Tool Event Handlers
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    onUserInput
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolManager::onUserInput(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;
   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Give the "first dibs" tools the first crack at the user input
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        (firstDibsToolObject != nullptr) && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED);
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onUserInput(userInput);
      }

   // If "first dibs" tool did not return an error or consume the event,
   // pass the event to the active tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject *activeToolObject = getActiveToolObject();
      if (activeToolObject != 0)
         {
         retVal = activeToolObject->onUserInput(userInput);
         }
      }

   // If active tool did not return an error or consume the event,
   // then call the default tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject* defaultToolObject = getDefaultToolObject();
      if (defaultToolObject != 0)
         {
         retVal = defaultToolObject->onUserInput(userInput);
         }
      }

   return retVal;
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolManager::onMouseMove(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   lastMousePositionWindowX = userInput.windowX;
   lastMousePositionWindowY = userInput.windowY;
   lastMousePositionImageX = userInput.imageX;
   lastMousePositionImageY = userInput.imageY;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Give the "first dibs" tools the first crack at the user input
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        (firstDibsToolObject != nullptr) && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED);
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onMouseMove(userInput);
      }

   // If "first dibs" tool did not return an error or consume the event,
   // pass the event to the active tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject *activeToolObject = getActiveToolObject();
      if (activeToolObject != 0)
         {
         retVal = activeToolObject->onMouseMove(userInput);
         }
      }

   // If active tool did not return an error or consume the event,
   // then call the default tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject* defaultToolObject = getDefaultToolObject();
      if (defaultToolObject != 0)
         {
         retVal = defaultToolObject->onMouseMove(userInput);
         }
      }

   return retVal;
}

void CToolManager::GetLastMousePositionWindow(int &x, int &y)
{
   x = lastMousePositionWindowX;
   y = lastMousePositionWindowY;
}

void CToolManager::GetLastMousePositionImage(int &x, int &y)
{
   x = lastMousePositionImageX;
   y = lastMousePositionImageY;
}


void CToolManager::SetMouseOrientation(int type)
{
  mouseOrientation = type;

}

CToolFrameCache *CToolManager::GetToolFrameCache()
{
   return toolFrameCache;
}

void CToolManager::SetMainWindowFocus()
{
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == nullptr)
   {
      return;
   }

   activeToolObject->getSystemAPI()->setMainWindowFocus();
}


// ===================================================================
//
// Function:    onRedraw
//
// Description: Calls active tool's onRedraw event handler after
//              a frame has been drawn.  The tool can use the
//              onRedraw event to draw overlay graphics on top of
//              the frame image
//
// Arguments:   int frameIndex    Index of frame just drawn
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onRedraw(int frameIndex)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Pass the redraw event to ALL the "first dibs" tools
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        firstDibsToolObject != nullptr;
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onRedraw(frameIndex);
      }

   // Pass the event to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onRedraw(frameIndex);
      }

   // Always send the onRedraw event to the default tool in addition
   // to the active tool
   CToolObject* defaultToolObject = getDefaultToolObject();
   if (defaultToolObject != 0)
      {
      retVal = defaultToolObject->onRedraw(frameIndex);
      }

   return retVal;
}

// ===================================================================
//
// Function:    onPreviewHackDraw
//
// Description: Calls active tool's onPreviewHackDraw event handler
//              before a frame is drawn, and the bits of the frame
//              are included in intermediate format so the tool can
//              modify them before they are actually drawn.
//
// Arguments:   int frameIndex              Index of frame to be drawn
//              unsigned short *frameBits   The bits of the frame in
//                                          intermediate format
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onPreviewHackDraw(frameIndex, frameBits, frameBitsSizeInBytes);
      }

      // T E S T I N G
   if (retVal == TOOL_RETURN_CODE_NO_ACTION)
   {
      unsigned int firstDibsToolIndex = 0;
      CToolObject *firstDibsToolObject;

      // Pass the redraw event to ALL the "first dibs" tools
      for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
           firstDibsToolObject != nullptr;
           firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
         {
         retVal = firstDibsToolObject->onPreviewHackDraw(frameIndex, frameBits, frameBitsSizeInBytes);
         }
   }

   return retVal;
}

// ===================================================================
//
// Function:    onPreviewDisplayBufferDraw
//
// Description: Calls active tool's onPreviewHackDraw event handler
//              before a frame is drawn, and the bits of the frame
//              are included in i8-bit BGRA format. Note that the bits
//              are provided only for pixels within the indicated bounds.
//
// Arguments:   int frameIndex              Index of frame to be drawn
//              int width, height           Width and height of the bitmap in PIXELS
//              unsigned int *pixels        The pixels of the frame in 8-bit RGBA format
//                                          (R=0x000000FF, G=0x0000FF00, B=0x00FF0000)
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onPreviewDisplayBufferDraw(int frameIndex, int width, int height, unsigned int *pixels)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onPreviewDisplayBufferDraw(frameIndex, width, height, pixels);
      }

   return retVal;
}

// ===================================================================
//
// Function:    onTopPanelRedraw
//
// Description: Calls active tool's onTopPanelRedraw event handler after
//              it is determined that the content of the Main Window
//              top panel needs to be redrawn (i.e. it becomes visible
//              or is resized.
//
// Arguments:   None
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onTopPanelRedraw()
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onTopPanelRedraw();
      }

   return retVal;
}

// ===================================================================
//
// Function:    onTopPanelMouseDown
//
// Description: Calls active tool's onTopPanelMouseDown event handler after
//              it is determined that left mouse button has been pressed
//              while the cursor is over the main window top panel.
//
// Arguments:   the client X and Y where the mouse was clicked in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onTopPanelMouseDown(int x, int y)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onTopPanelMouseDown(x, y);
      }

   return retVal;
}

// ===================================================================
//
// Function:    onTopPanelMouseDrag
//
// Description: Calls active tool's onTopPanelMouseDrag event handler after
//              it is determined that mouse moved ewhile we are in "drag"
//              mode in the main window top panel.
//
// Arguments:   the client X and Y where the mouse was moved to in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onTopPanelMouseDrag(int x, int y)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onTopPanelMouseDrag(x, y);
      }

   return retVal;
}

// ===================================================================
//
// Function:    onTopPanelMouseUp
//
// Description: Calls active tool's onTopPanelMouseUp event handler after
//              it is determined that mouse button was released while we
//              are in "drag" mode in the main window top panel.
//
// Arguments:   the client X and Y where the mouse was unclicked in the top panel.
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onTopPanelMouseUp(int x, int y)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // We only pass the event to the active tool, not the "first dibs"
   // or default tools
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onTopPanelMouseUp(x, y);
      }

   return retVal;
}

// ===================================================================
//
// Function:    onPlayerStart
//
// Description: Calls active tool's onPlayerStart event handler just
//              before playing is about to begin.
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onPlayerStart()
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Pass the onPlayerStart event to ALL the "first dibs" tools
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        firstDibsToolObject != nullptr;
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onPlayerStart();
      }

   // Pass the event to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onPlayerStart();
      }

   // Always send the onPlayerStart event to the default tool in addition
   // to the active tool
   CToolObject* defaultToolObject = getDefaultToolObject();
   if (defaultToolObject != 0)
      {
      retVal = defaultToolObject->onPlayerStart();
      }

   return retVal;
}

// ===================================================================
//
// Function:    onPlayerStop
//
// Description: Calls active tool's onPlayerStop event handler after
//              a frame has been drawn.  The tool can use the
//              onPlayerStop event to reset its state when processing
//              frames in onRedraw
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onPlayerStop()
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Pass the onPlayerStop event to ALL the "first dibs" tools
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        firstDibsToolObject != nullptr;
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onPlayerStop();
      }

   // Pass the onPlayerStop event to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onPlayerStop();
      }

   // Always send the onPlayerStop event to the default tool in addition
   // to the active tool
   CToolObject* defaultToolObject = getDefaultToolObject();
   if (defaultToolObject != 0)
      {
      retVal = defaultToolObject->onPlayerStop();
      }

   return retVal;
}

// ===================================================================
//
// Function:    onPlaybackFilterChange
//
// Description: Calls active tool's onPlaybacFilterChange event handler
//              the Player's playback filter has been changed. The tool
//              can use the event to modify its "onReraw" behavior
//
// Arguments:   none
//
// Returns:     Tool Return code from active tool
//
// ===================================================================
int CToolManager::onPlaybackFilterChange(int fltr)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Pass the onPlaybackFilterChange event to ALL the "first dibs" tools
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        firstDibsToolObject != nullptr;
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onPlaybackFilterChange(fltr);
      }

   // Pass the onPlaybackFilterChange event to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject != 0)
      {
      retVal = activeToolObject->onPlaybackFilterChange(fltr);
      }

   // Always send the onPlaybackFilterChange event to the default tool
   // in addition to the active tool
   CToolObject* defaultToolObject = getDefaultToolObject();
   if (defaultToolObject != 0)
      {
      retVal = defaultToolObject->onPlaybackFilterChange(fltr);
      }

   return retVal;
}

// onNewMarks is called after markIn and/or markOut is changed
int CToolManager::onNewMarks()
{
   int retVal;

   // onNewMarks event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onNewMarks();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onNewMarks: call to onNewMarks failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   return 0;
}

// onTimelineVisibleFrameRangeChange is called after the blue area on the timeline track bar is changed
int CToolManager::onTimelineVisibleFrameRangeChange()
{
   int retVal;

   // onTimelineVisibleFrameRangeChange event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onTimelineVisibleFrameRangeChange();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onTimelineVisibleFrameRangeChange: call to onTimelineVisibleFrameRangeChange failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   return 0;
}

// onChangingClip is called before a new clip is opened
int CToolManager::onChangingClip()
{
   int retVal;

   // onChangingClip event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onChangingClip();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onChangingClip: call to onChangingClip failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   // This line was moved here from onNewClip!
   DeleteBufferAllocators();

   return 0;
}

// onDeletingOpenedClip is called right after onChangingClip when the currently
// open clip is being deleted.
int CToolManager::onDeletingOpenedClip()
{
   int retVal;

   // onDeletingOpenedClip event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onDeletingOpenedClip();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onDeletingOpenedClip: call to onDeletingOpenedClip failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   return 0;
}

// onNewClip is called after a new clip is opened
int CToolManager::onNewClip()
{
   int retVal;

   // onNewClip event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onNewClip();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onNewClip: call to onNewClip failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   return 0;
}

// onChangingFraming is called before the framing (video- & film-frames) is changed
int CToolManager::onChangingFraming()
{
   int retVal;

   // onChangingFraming event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onChangingFraming();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onChangingFraming: call to onChangingFraming failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   // Delete any frame buffers allocated for tools
   DeleteBufferAllocators();

   return 0;
}

// onNewFraming is called after the framing (video- & film-frames) is changed
int CToolManager::onNewFraming()
{
   int retVal;

   // onNewFraming event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         retVal = toolObj->onNewFraming();
         if (retVal & TOOL_RETURN_CODE_ERROR)
            {
            TRACE_0(errout << "ERROR: CToolManager::onNewFraming: call to onNewFraming failed "
                           << endl
                           << "       for " << toolList[i]->toolName
                           << " tool" << endl
                           << "       Return code " << retVal);
            }
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CToolManager::showTool(int toolIndex)
{
   int retVal;

   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return -1;   // Return null pointer
      }

   // Check it tool has been initialized yet
   if(!toolList[toolIndex]->isToolInitialized)
      {
      // The tool needs to be initialized
      retVal = makeTool(toolIndex);
      if (retVal != 0)
         {
         // ERROR: Could not create or initialize this tool
         return retVal;
         }
      }

   TRACE_1(errout << "============= SHOW TOOL "
                  << getToolName(toolIndex)
                  << " =============");


   retVal = toolList[toolIndex]->toolObject->toolShow();
   if (retVal != 0)
      {
      // ERROR: toolShow failed
      return retVal;
      }

   // Set this tool to be the active tool
   setActiveTool(toolIndex);

   return 0;
}

int CToolManager::hideTool(int toolIndex)
{
   int retVal;

   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return -1;   // Return null pointer
      }

   // Check it tool has been initialized yet
   if(toolList[toolIndex]->isToolInitialized)
      {

      TRACE_1(errout << "============= HIDE TOOL "
                     << getToolName(toolIndex)
                     << " =============");
      retVal = toolList[toolIndex]->toolObject->toolHide();
      if (retVal != 0)
         {
         // ERROR: toolHide failed
         return retVal;
         }

      // If this tool is the currently active tool, then deactivate it
      if (toolIndex == activeToolIndex)
         {
         setActiveTool(-1);
         }
      }

   return 0;
}

bool CToolManager::hideToolQuery(int toolIndex)
{
   int retVal;

   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return true;
      }

   // Check it tool has been initialized yet
   if(toolList[toolIndex]->isToolInitialized)
      {
      return toolList[toolIndex]->toolObject->toolHideQuery();
      }

   return true;
}

int CToolManager::initTool(int toolIndex)
{
   // Force a tool to be created and initialized

   int retVal;

   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return -1;   // Return null pointer
      }

   // Check it tool has been initialized yet
   if(!toolList[toolIndex]->isToolInitialized)
      {
      // The tool needs to be initialized
      retVal = makeTool(toolIndex);
      if (retVal != 0)
         {
         // ERROR: Could not create or initialize this tool
         return retVal;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    setActiveTool
//
// Description:
//
// Arguments:  int toolIndex    Tool index for new active tool
//                              or -1 to force no active tool
//
// Returns:
//
// ===================================================================
int CToolManager::setActiveTool(int toolIndex)
{
   // Range-check toolIndex argument.
   if (toolIndex != -1 && (toolIndex < 0 || toolIndex >= getToolCount()))
      {
      return -1;
      }

   if (toolIndex != -1 && toolIndex == activeToolIndex)
      return 0;   // Nothing to do, active tool is set already

   int retVal = 0;
   // Deactivate the currently active tool if there is one
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != 0)
      {
      TRACE_1(errout << "============= DEACTIVATE "
                     << getToolName(activeToolIndex)
                     << " =============");

      retVal = activeTool->toolDeactivate();
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CToolManager::setActiveTool call to " << endl
                        << "       CToolObject::toolDeactivate failed with return code "
                        << retVal);
         }
      }

   // Hack - Enable tool frame caching by default - if tool doesn't use the
   // tool infrastructure and is using the prealloced buffer space for
   // something else (i.e. Paint), it must disable caching in toolActivate()
   GetToolFrameCache()->EnableCaching(true);

   // Set new active tool
   activeToolIndex = toolIndex;
   activeTool = getActiveToolObject();
   if (activeTool != 0)
      {
      TRACE_1(errout << "============== ACTIVATE "
                     << getToolName(activeToolIndex)
                     << " ==============");

      retVal = activeTool->toolActivate();
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CToolManager::setActiveTool call to " << endl
                        << "       CToolObject::toolActivate failed with return code "
                        << retVal);
         }

      // GOD-AWFUL HACK!!
      // The "Mask Transform Editor" tries to refresh the displayed
      // frame every time the mouse moves, even if no mask is being
      // transformed. The reason is to produce a side effect where
      // cursors in some obscure Paint mode will get updated. But this
      // interferes with my debugging and profiling of other tools, so I
      // implemented a way to turn off the refresh when the mask is not
      // being manipulated
      activeTool->SetMaskTransformEditorIdleRefreshMode();

      }

   return 0;
}

// ===================================================================
//
// Function:    setDefaultTool
//
// Description:
//
// Arguments:  int toolIndex    Tool index for new default tool
//                              or -1 to force no default tool
//
// Returns:
//
// ===================================================================
int CToolManager::setDefaultTool(int toolIndex)
{
   // Range-check toolIndex argument.
   if (toolIndex != -1 && (toolIndex < 0 || toolIndex >= getToolCount()))
      {
      return -1;
      }

   defaultToolIndex = toolIndex;

   return 0;
}

// ===================================================================
//
// Function:    addFirstDibsTool
//
// Description:
//
// Arguments:  int toolIndex    Tool index for new "first dibs" tool (the
//                              tool that gets first crack at handling
//                              user inputs)
//                              or -1 to force no default tool
//
// Returns:
//
// ===================================================================
int CToolManager::addFirstDibsTool(int toolIndex)
{
   // Range-check toolIndex argument.
   if (toolIndex != -1 && (toolIndex < 0 || toolIndex >= getToolCount()))
      {
      return -1;
      }

   firstDibsToolIndexList.push_back(toolIndex);

   return 0;
}

int CToolManager::onCapturePDLEntry(CPDLEntry &pdlEntry)
{
   int retVal;

   // Get the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == 0)
      return 0;   // no active tool, so nothing to do

   CPDLElement *toolParams = pdlEntry.AddTool(activeToolObject->GetToolName());

   retVal = activeToolObject->onCapturePDLEntry(*toolParams);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolManager::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   int retVal;

   // Get the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == 0)
      return 0;   // no active tool, so nothing to do

   retVal = activeToolObject->onGoToPDLEntry(pdlEntry);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolManager::onPDLExecutionComplete()
{
   // onPDLExecutionComplete event is called for all tools
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         // Ignore errors
         toolObj->onPDLExecutionComplete();
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CToolManager::makeTool(int toolIndex)
{
   CToolObject *toolObject = 0;
   int pluginIndex = toolList[toolIndex]->pluginListIndex;

   // pluginIndex tells us whether the tool is internal or in a plugin
   if (pluginIndex == -1)
      {
      // Internal tool
      toolObject = toolList[toolIndex]->makeToolFunc(
                                                toolList[toolIndex]->toolName);
      }
   else if (pluginIndex >= 0 && pluginIndex < (int)pluginList->size())
      {
      // Tool resides in a plugin
      CPlugin *plugin = (*pluginList)[pluginIndex];

      // Plugin creates the particular CToolObject subtype instance
      toolObject
              = (CToolObject*)(plugin->Create(toolList[toolIndex]->toolName));
      }
   else
      {
      // ERROR: pluginListIndex for this tool is out-of-range
      return -2;
      }

   if (toolObject == 0)
      {
      // ERROR: Plugin could not create the Tool Object
      return -3;
      }

   // Tool Manager remembers the pointer to the new CToolObject
   toolList[toolIndex]->toolObject = toolObject;

   // Make a new Tool API Object for the new tool
   if (createToolSystemInterface == 0)
      {
      // ERROR: Application has not provided a function to create
      //        its concrete subtype of the CToolSystemInterface abstract
      //        base class
      return -4;
      }
   CToolSystemInterface *newToolAPI = createToolSystemInterface();

   // Initialize the tool
   int retVal = toolObject->toolInitialize(newToolAPI);
   if (retVal != 0)
      {
      // ERROR: Could not initialize the tool
      return retVal;
      }

   // Tool Manager remembers that this tool has been initialized
   toolList[toolIndex]->isToolInitialized = true;

   return 0;
}

int CToolManager::deleteToolList()
{
   // First call ALL the toolShutdowns, THEN delete all the tools, in case
   // there are tool interdependencies that would cause a deleted tool
   // to be accessed by the toolShutdown of a later tool
   for (int toolIndex = 0; toolIndex < getToolCount(); ++toolIndex)
      {
      SToolListEntry* toolListEntry = toolList[toolIndex];
      if (toolListEntry == nullptr)
         continue;
      int retVal = shutdownTool(toolListEntry);
      if (retVal != 0)
         {
         // ERROR: could not destroy tool
         return retVal;
         }
      }
   for (int toolIndex = 0; toolIndex < getToolCount(); ++toolIndex)
      {
      SToolListEntry* toolListEntry = toolList[toolIndex];
      if (toolListEntry == nullptr)
         continue;
      int retVal = destroyTool(toolListEntry);
      if (retVal != 0)
         {
         // ERROR: could not destroy tool
         return retVal;
         }
      delete toolListEntry;
      toolList[toolIndex] = 0;
      }

   return 0;
}

int CToolManager::shutdownTool(SToolListEntry* toolListEntry)
{
   int retVal = 0;
   CToolObject *toolObject = toolListEntry->toolObject;

   // Shutdown the tool by calling the tool's shutdown function
   if (toolListEntry->isToolInitialized && toolObject)
      {
      // Shutdown the tool
      retVal = toolObject->toolShutdown();
      if (retVal != 0)
         {
         // ERROR: Tool's shutdown function had an error
         return retVal;
         }
      }

   return 0;
}

int CToolManager::destroyTool(SToolListEntry* toolListEntry)
{
   CToolObject *toolObject = toolListEntry->toolObject;

   // Shutdown the tool by calling the tool's shutdown function
   if (toolListEntry->isToolInitialized && toolObject)
      {
      // Get the tool's System API
      CToolSystemInterface *systemAPI = toolObject->getSystemAPI();

      // Delete the tool's System API
      // Note: the System API object is deleted here rather than in the
      // CToolObject because it was created by the Tool Manager in makeTool
      delete systemAPI;
      }

   // Destroy the tool if one exists
   if (toolObject)
      {
      // Delete the tool object
      delete toolObject;
      }

   toolListEntry->toolObject = 0;
   toolListEntry->isToolInitialized = false;

   return 0;
}

/////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    getActiveToolObject
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
CToolObject* CToolManager::getActiveToolObject()
{
   if (activeToolIndex < 0 || activeToolIndex >= getToolCount())
      {
      return 0;
      }

   return toolList[activeToolIndex]->toolObject;
}

// ===================================================================
//
// Function:    getDefaultToolObject
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
CToolObject* CToolManager::getDefaultToolObject()
{
   if (defaultToolIndex < 0 || defaultToolIndex >= getToolCount())
      {
      return 0;
      }

   // the entry for the default tool may be nullptr - check it
   SToolListEntry * defaultTool = toolList[defaultToolIndex];
   if (defaultTool == nullptr) return 0;

   // Check it default tool has been initialized yet
   if(!defaultTool->isToolInitialized)
      {
      // The tool needs to be initialized
      int retVal = makeTool(defaultToolIndex);
      if (retVal != 0)
         {
         // ERROR: Could not create or initialize this tool
         return 0;
         }
      }

   return toolList[defaultToolIndex]->toolObject;
}

// ===================================================================
//
// Function:    getFirstDibsToolObject
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
CToolObject* CToolManager::getFirstDibsToolObject(
                              unsigned int firstDibsToolListIndex)
{
   // If at end of list, return nullptr
   if (firstDibsToolListIndex >= firstDibsToolIndexList.size())
      {
      return nullptr;
      }

   // The entry for the firstDibs tool may be nullptr - check it
   int toolListIndex = firstDibsToolIndexList[firstDibsToolListIndex];
   SToolListEntry *firstDibsTool = toolList[toolListIndex];
   if (firstDibsTool == nullptr)
      return nullptr;

   // Check it firstDibs tool has been initialized yet
   if(!firstDibsTool->isToolInitialized)
      {
      // The tool needs to be initialized
      int retVal = makeTool(toolListIndex);
      if (retVal != 0)
         {
         // ERROR: Could not create or initialize this tool
         return nullptr;
         }
      }

   return firstDibsTool->toolObject;
}

// ===================================================================
//
// Function:    loadInternalTools
//
// Description: Must only be called once
//
// Arguments:   char *toolNameTable[]   Array of pointers to internal tool names
//              int toolCount           Number of internal tools
//
// Returns:
//
// ===================================================================
int CToolManager::loadInternalTools(const SInternalToolTable *internalToolTable,
                                    int toolCount)
{
   for (int toolIndex = 0; toolIndex < toolCount; ++toolIndex)
      {
      // Initialize a new tool list entry and remember the tool name
      SToolListEntry* toolListEntry = new SToolListEntry;
      toolListEntry->isToolInitialized = false;
      toolListEntry->toolName = internalToolTable[toolIndex].toolName;
      toolListEntry->makeToolFunc = internalToolTable[toolIndex].makeToolFunc;
      toolListEntry->pluginListIndex = -1;   // Not a plugin
      toolListEntry->toolObject = 0;

      // Add this to the tool list
      toolList.push_back(toolListEntry);
      }

   return 0;
}

// ===================================================================
//
// Function:    loadPlugins
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolManager::loadPlugins(const char* pluginPath, void *appPtr)
{
   // Plugin List can only be loaded once
   if (pluginList == 0)
      {
      // Create a new instance of the CPluginList, which loads all of the
      // plugins in the pluginPath
	  pluginList = new CPluginList(pluginPath);
      if (pluginList == 0)
         {
         // ERROR: Plugin Load failed
         return -1;
         }

      // Iterate through plugins to get the name of the tools they contain
      for (int pluginIndex = 0; pluginIndex < (int)pluginList->size(); ++pluginIndex)
         {
		 CPlugin *plugin = (*pluginList)[pluginIndex];
// TTT The next statement causes a crash on exit if the Scratch tool was loaded
// TTT         plugin->SendTheMessage(PIN_MSG_SET_PARENT, appPtr);
		 char **toolNameTable = (char**)(plugin->Resource(PIN_RES_TOOL_NAMES));
         int toolCount = (int)(plugin->Resource(PIN_RES_TOOL_NUMBER));
         MTI_UINT32 **FeatureTable = (MTI_UINT32 **)(plugin->Resource(PIN_RES_FEATURE));
         for (int toolIndex = 0; toolIndex < toolCount; ++toolIndex)
            {
            // Initialize a new tool list entry and remember the tool name
            SToolListEntry* toolListEntry = new SToolListEntry;
            toolListEntry->isToolInitialized = false;
            toolListEntry->toolName = toolNameTable[toolIndex];
            toolListEntry->pluginListIndex = pluginIndex;
            toolListEntry->makeToolFunc = 0;  // Internal tools only
            toolListEntry->toolObject = 0;

            // If feature table is nullptr, don't load it
            toolListEntry->toolFeature = FeatureTable;

            // Add this to the tool list
            toolList.push_back(toolListEntry);
            }
         }
      }

   return 0;
}

// ===================================================================
//
// Function:    unloadPlugins
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolManager::unloadPlugins()
{
   int retVal = 0;

   // Deactivate the active tool
   setActiveTool(-1);

   // Shutdown and delete each tool in the tool list
   retVal = deleteToolList();
   if (retVal != 0)
      {
      // ERROR: could not destroy all the tools in the tool list
      return retVal;
		}

#if 0
   // Delete the pluginList, which unloads each of the plugins
   if (pluginList != 0)
      {
      delete pluginList;
		}
#endif

	return 0;
}

// ===================================================================
//
// Function:    getToolCount
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CToolManager::getToolCount() const
{
   return (int)toolList.size();
}

string CToolManager::getToolName(int toolIndex) const
{
   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return 0;   // Return null pointer
      }

   return toolList[toolIndex]->toolName;
}

MTI_UINT32 **CToolManager::getToolFeature(int toolIndex) const
{
   if (toolIndex < 0 || toolIndex >= getToolCount())
      {
      // ERROR: toolIndex out-of-range
      return 0;   // Return null pointer
      }

   return toolList[toolIndex]->toolFeature;
}

int CToolManager::findTool(const string& toolName) const
{
   int toolCount = getToolCount();
   for (int toolIndex = 0; toolIndex < toolCount; ++toolIndex)
      {
      SToolListEntry* toolListEntry = toolList[toolIndex];
      if (toolListEntry != nullptr && toolName == toolListEntry->toolName)
         {
         // Tool name matches
         return toolIndex;
         }
      }

   // Matching tool name was not found
   return -1;
}

int CToolManager::findTool(const CToolObject *toolObj) const
{
   int toolCount = getToolCount();
   for (int toolIndex = 0; toolIndex < toolCount; ++toolIndex)
      {
      if (toolObj == toolList[toolIndex]->toolObject)
         {
         // Tool object pointer matches
         return toolIndex;
         }
      }

   // Matching tool object was not found
   return -1;
}

bool CToolManager::isPluginTool(int toolIndex) const
{
   // Returns true if Tool is from a Plugin (rather than an internal
   // tool or somesuch)

   if (toolIndex < 0 || toolIndex >= getToolCount())
        return false;  //  Caller's toolIndex is out-of-range

   return !(toolList[toolIndex]->pluginListIndex < 0);
}

CPluginStateWrapper CToolManager::currentPluginState(int toolIndex, bool noCache) const
{
   CPluginStateWrapper cps;
   cps.State(PLUGIN_STATE_INVALID);

   if (!isPluginTool(toolIndex))
      return cps;

   int pluginIndex = toolList[toolIndex]->pluginListIndex;

   // pluginIndex tells us whether the tool is internal or in a plugin
   if (pluginIndex == -1)
      return cps;

   if (pluginIndex >= 0 && pluginIndex < (int)pluginList->size())
      {
      // Tool resides in a plugin
      if ((!noCache) && (pluginIndex < MAX_CACHED_PLUGIN_STATE_COUNT
		 && pluginToolStateCache[pluginIndex].State() != PLUGIN_STATE_INVALID))
         {
         cps = pluginToolStateCache[pluginIndex];
         }
      else
         {
         CPlugin *plugin = (*pluginList)[pluginIndex];
         cps = plugin->CurrentState();

         // Cache the result
         if (pluginIndex < MAX_CACHED_PLUGIN_STATE_COUNT)
            pluginToolStateCache[pluginIndex] = cps;
         }
      }

   return cps;
}

int CToolManager::CheckTool(const string &toolName, string &message)
{
   int retVal;

   // Clear the caller's message
   message.erase();

   // Find the tool in the Tool Manager's tool list
   int toolIndex = findTool(toolName);
   if (toolIndex < 0)
      {
      // ERROR: Cannot find the tool in the Tool List.  This may occur
      //        if either the tool name is bogus, if the tool DLL is not
      //        installed on this system or if the DLL could not be loaded
      //        as CPMP plugin
      message = "The " + toolName + " tool is not available";
      return TOOL_ERROR_CANNOT_FIND_TOOL_BY_NAME;
      }

   if (!isPluginTool(toolIndex))
      {
      // If the tool is in the Tool List but not a plugin, we conclude that
      // it is an internal tool.  By definition, internal tools are always
      // available and licensed
      return 0;
      }

   // Check the licensing state of the tool
   CPluginStateWrapper pluginState = currentPluginState(toolIndex);
   switch (pluginState.State())
      {
      case PLUGIN_STATE_ENABLED:
         // Tool is licensed
         retVal = 0;
         break;

      case PLUGIN_STATE_DEMO:
      case PLUGIN_STATE_DISABLED:
         // Tool is not licensed
         message = "The " + toolName + " tool is " + pluginState.strReason();
         retVal = TOOL_ERROR_TOOL_IS_NOT_LICENSED;
         break;

      default:
         // Unknown or invalid licensing.  This condition should never
         // happen
         message = "The " + toolName + " tool's licensing is unknown";
         retVal = TOOL_ERROR_UNKNOWN_LICENSE;
         break;
      }

   if (retVal != 0)
      return retVal;

   return 0;  // tool is okay
}


/////////////////////////////////////////////////////////////////////////////
//
// User Input Event Handlers for Keyboard and Mouse input.
//
// onKeyDown, onKeyUp, onMouseDown, onMouseUp and onMouseMove
//
// Each function converts the Borland-defined event into a CPMP internal
// event as a CUserInput instance.  This user input event is then sent
// to the Tool Manager to dispatch to the appropriate tool.
//
/////////////////////////////////////////////////////////////////////////////
#ifdef __BORLANDC__
int CToolManager::onKeyDown(WORD &Key, TShiftState Shift)
{
   // Convert the Borland Key event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromKeyEvent(USER_INPUT_ACTION_KEY_DOWN, Key, Shift);

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}
//---------------------------------------------------------------------------

int CToolManager::onKeyUp(WORD &Key, TShiftState Shift)
{
   // Convert the Borland Key event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromKeyEvent(USER_INPUT_ACTION_KEY_UP, Key, Shift);

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}
//---------------------------------------------------------------------------

int CToolManager::onMouseDown(TMouseButton Button, TShiftState Shift,
                               int X, int Y, int imageX, int imageY)
{
   // Convert the Borland Mouse Button event to a CPMP internal event
   CUserInput newUserInput;

#ifndef FRAMEWRX
   if(this->mouseOrientation == 1)
   {
     if(Button == mbLeft)
       Button = mbMiddle;
     else if(Button == mbMiddle)
       Button = mbLeft;
   }
#endif // FRAMEWRX
   newUserInput.setFromMouseEvent(USER_INPUT_ACTION_MOUSE_BUTTON_DOWN,
                                  Button, Shift, X, Y);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}

int CToolManager::onMouseDoubleClick(TMouseButton Button, TShiftState Shift,
                               int X, int Y, int imageX, int imageY)
{
   // Convert the Borland Mouse Button event to a CPMP internal event
   CUserInput newUserInput;

#ifndef FRAMEWRX
   if(this->mouseOrientation == 1)
   {
     if(Button == mbLeft)
       Button = mbMiddle;
     else if(Button == mbMiddle)
       Button = mbLeft;
   }
#endif // FRAMEWRX
   newUserInput.setFromMouseEvent(USER_INPUT_ACTION_MOUSE_DOUBLE_CLICK,
                                  Button, Shift, X, Y);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}

//---------------------------------------------------------------------------

int CToolManager::onMouseUp(TMouseButton Button, TShiftState Shift,
                             int X, int Y, int imageX, int imageY)
{
   // Convert the Borland Mouse Button event to a CPMP internal event
   CUserInput newUserInput;

#ifndef FRAMEWRX
   if(this->mouseOrientation == 1)
   {
     if(Button == mbLeft)
       Button = mbMiddle;
     else if(Button == mbMiddle)
       Button = mbLeft;
   }
#endif // FRAMEWRX
   newUserInput.setFromMouseEvent(USER_INPUT_ACTION_MOUSE_BUTTON_UP,
                                  Button, Shift, X, Y);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}

//---------------------------------------------------------------------------

int CToolManager::onMouseMove(TShiftState Shift, int X, int Y,
                              int imageX, int imageY)
{
   // Convert the Borland Mouse Movement event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromMouseMoveEvent(Shift, X, Y);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onMouseMove(newUserInput);

   return retVal;
}

#endif // #ifdef __BORLANDC__

#ifdef __sgi
int CToolManager::onKeyDown(XEvent *event)
{
   // Convert the X Key event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromKeyEvent((XKeyEvent*)event);

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}
//---------------------------------------------------------------------------

int CToolManager::onKeyUp(XEvent *event)
{
   // Convert the X Key event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromKeyEvent((XKeyEvent*)event);

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}
//---------------------------------------------------------------------------

int CToolManager::onMouseDown(XEvent *event, int imageX, int imageY)
{
   // Convert the X Mouse Button event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromMouseEvent((XButtonEvent*)event);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}
//---------------------------------------------------------------------------

int CToolManager::onMouseUp(XEvent *event, int imageX, int imageY)
{
   // Convert the X Mouse Button event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromMouseEvent((XButtonEvent*)event);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onUserInput(newUserInput);

   return retVal;
}

//---------------------------------------------------------------------------

int CToolManager::onMouseMove(XEvent *event, int imageX, int imageY)
{
   // Convert the X Mouse Movement event to a CPMP internal event
   CUserInput newUserInput;
   newUserInput.setFromMouseMoveEvent((XMotionEvent*)event);

   // Set coordinates scaled to image coordinate space
   newUserInput.imageX = imageX;
   newUserInput.imageY = imageY;

   // Send the user input event to the Tool Manager, which will dispatch it
   // to the appropriate tool
   CToolManager toolManager;
   int retVal = toolManager.onMouseMove(newUserInput);

   return retVal;
}



#endif // #ifdef __sgi

//---------------------------------------------------------------------------

void CToolManager::setCreateToolSystemInterfaceFunc(
                                 CreateToolSystemInterfaceFunc newCreateFunc)
{
   createToolSystemInterface = newCreateFunc;
}


CToolObject * CToolManager::GetTool(int index)
{
   return(toolList[index]->toolObject);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CToolManager::SetProvisional(CProvisional *newProvisional)
{
   provisional = newProvisional;
}

void CToolManager::MakeSimpleIUPE(const string& toolName,
                                  CToolIOConfig *toolIOConfig,
                                  IUPEToolList& toolList,
                                  IUPEEdgeList& edgeList,
                                  CMediaReaderIOConfig &mediaReaderIOConfig,
                                  CMediaWriterIOConfig &mediaWriterIOConfig)
{
   // Establish I/O Configuration for the Media Reader
   mediaReaderIOConfig.useNavCurrentClip = true;

   // Establish I/O Configuration for the Media Writer
   mediaWriterIOConfig.useNavCurrentClip = true;

   // Create IUPE Tool and Edge Lists, which will capture the image data flow:
   //
   //    (Media Reader) -> (Caller's Tool) -> (Provisional) -> (Media Writer)
   //

   // Create IUPE tool list
   toolList.clear();
   SIUPETool tool;  // temporary
   tool.toolFrameRange = 0;
   tool.toolParameters = 0;

   // Add entries to the tool list: Tool Name + Tool Parameters for each tool
   tool.toolName = "MediaReader";    // Tool 0  (MediaReader)
   tool.toolIOConfig = &mediaReaderIOConfig;
   toolList.push_back(tool);
   tool.toolName = toolName;         // Tool 1  (Caller's Tool)
   tool.toolIOConfig = toolIOConfig;
   toolList.push_back(tool);
   tool.toolName = "Provisional";    // Tool 2  (Provisional)
   tool.toolIOConfig = 0; // No params
   toolList.push_back(tool);
   tool.toolName = "MediaWriter";    // Tool 3  (MediaWriter)
   tool.toolIOConfig = &mediaWriterIOConfig;
   toolList.push_back(tool);

   // Create the IUPE edge list
   edgeList.clear();
   SIUPEEdge edge;  // temporary
   edge.srcToolListIndex = 0;        // Source      = Tool 0 (MediaReader)
   edge.srcOutPortIndex = 0;
   edge.dstToolListIndex = 1;        // Destination = Tool 1 (Caller's tool)
   edge.dstInPortIndex = 0;
   edgeList.push_back(edge);

   edge.srcToolListIndex = 1;        // Source      = Tool 1 (Caller's tool)
   edge.srcOutPortIndex = 0;
   edge.dstToolListIndex = 2;        // Destination = Tool 2 (Provisional)
   edge.dstInPortIndex = 0;
   edgeList.push_back(edge);

   edge.srcToolListIndex = 2;        // Source      = Tool 2 (Provisional)
   edge.srcOutPortIndex = 0;
   edge.dstToolListIndex = 3;        // Destination = Tool 3 (MediaWriter)
   edge.dstInPortIndex = 0;
   edgeList.push_back(edge);
}

void CToolManager::MakePreloadIUPE(const string& toolName,
                                   CToolIOConfig *toolIOConfig,
                                   IUPEToolList& toolList,
                                   IUPEEdgeList& edgeList,
                                   CMediaReaderIOConfig &mediaReaderIOConfig)
{
   // Establish I/O Configuration for the Media Reader
   mediaReaderIOConfig.useNavCurrentClip = true;

   // Create IUPE Tool and Edge Lists, which will capture the image data flow:
   //
   //    (Media Reader) -> (Caller's Tool) , which must do nothing
   //

   // Create IUPE tool list
   toolList.clear();
   SIUPETool tool;  // temporary
   tool.toolFrameRange = 0;
   tool.toolParameters = 0;

   // Add entries to the tool list: Tool Name + Tool Parameters for each tool
   tool.toolName = "MediaReader";    // Tool 0  (MediaReader)
   tool.toolIOConfig = &mediaReaderIOConfig;
   toolList.push_back(tool);
   tool.toolName = toolName;         // Tool 1  (Caller's Tool)
   tool.toolIOConfig = toolIOConfig;
   toolList.push_back(tool);

   // Create the IUPE edge list
   edgeList.clear();
   SIUPEEdge edge;  // temporary
   edge.srcToolListIndex = 0;        // Source      = Tool 0 (MediaReader)
   edge.srcOutPortIndex = 0;
   edge.dstToolListIndex = 1;        // Destination = Tool 1 (Caller's tool)
   edge.dstInPortIndex = 0;
   edgeList.push_back(edge);
}

int CToolManager::MakeSimpleToolSetup(const string& toolName,
                                      EToolSetupType newToolSetupType,
                                      CToolIOConfig *toolIOConfig)
{
   int retVal;

   int toolMgrIndex = findTool(toolName);
   if (toolMgrIndex < 0)
      {
      TRACE_0(errout << "ERROR: CToolManager::MakeSimpleToolSetup: CToolManager could "
                     << endl
                     << "       not find the tool named: "
                     << toolName);
      return -1;
      }

   // Create trivial IUPE with single tool, one input and one output
   // using the Application's current clip
   IUPEToolList iUPEToolList;
   IUPEEdgeList iUPEEdgeList;
   CMediaReaderIOConfig mediaReaderIOConfig;
   CMediaWriterIOConfig mediaWriterIOConfig;
   if (newToolSetupType == TOOL_SETUP_TYPE_PRELOAD_ONLY)
      {
      MakePreloadIUPE(toolName, toolIOConfig, iUPEToolList, iUPEEdgeList,
                      mediaReaderIOConfig);
      }
   else
      {
      MakeSimpleIUPE(toolName, toolIOConfig, iUPEToolList, iUPEEdgeList,
                     mediaReaderIOConfig, mediaWriterIOConfig);
      }

   // Create the Tool Setup from the IUPE
   CToolSetup *toolSetup = new CToolSetup(newToolSetupType);

   int newToolSetupHandle = AddToolSetup(toolSetup);

   retVal = toolSetup->CreateFromIUPE(iUPEToolList, iUPEEdgeList);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CToolManager::MakeSimpleToolSetup: Call to "
                     << endl
                     << "       CToolSetup::CreateFromIUPE failed with return code "
                     << retVal);
      //delete toolSetup;
      DestroyToolSetup(newToolSetupHandle);
      WaitForToolSetupDestruction();
      return -1;    // ERROR: Could not initialize Tool Setup
      }

   // Topological Sort of tools
   retVal = toolSetup->TopologicalSort();
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CToolManager::MakeSimpleToolSetup: Call to "
                     << endl
                     << "       CToolSetup::TopologicalSort failed with return code "
                     << retVal);
      //delete toolSetup;
      DestroyToolSetup(newToolSetupHandle);
      WaitForToolSetupDestruction();
      return -1;  // ERROR: Could not do topological sort, maybe a cycle
      }

   // Set I/O configuration of each tool in the topological sort order
   // Topological sort order allows propagation of image formats through
   // the network of tools
   retVal = toolSetup->SetIOConfig(iUPEToolList);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CToolManager::MakeSimpleToolSetup: Call to "
                     << endl
                     << "       CToolSetup::SetIOConfig failed with return code "
                     << retVal);
      //delete toolSetup;
      DestroyToolSetup(newToolSetupHandle);
      WaitForToolSetupDestruction();
      return -1;
      }

   return newToolSetupHandle;
}

int CToolManager::DestroyToolSetup(int toolSetupHandle)
{
   // Tool Setup destruction will occur in the background and this
   // function will return before the Tool Setup is destroyed.  Caller will
   // not be notified when destruction is complete, but should not
   // attempt to use the tool setup handle for anything once this
   // function is called.

   CToolSetup *toolSetup = GetToolSetup(toolSetupHandle);

   if (toolSetup == 0)
      return TOOL_ERROR_INVALID_TOOL_SETUP_HANDLE;

   if (toolSetup == activeToolSetup)
      return TOOL_ERROR_CANNOT_DESTROY_ACTIVE_TOOL_SETUP;

   // Add this Tool Setup to the tool setup destruction queue.
   // This will be picked up in the next call to MonitorToolProcessing
   // and the destruction will occur in the background.
   SToolSetupDestructionState dState;
   dState.currentState = TOOL_SETUP_DESTRUCTION_NEW_ENTRY;
   dState.toolSetup = toolSetup;
   toolSetupDestructionQueue.push_back(dState);

   return 0;
}

void CToolManager::MonitorToolSetupDestruction()
{
   if (toolSetupDestructionQueue.empty())
      return;

   int queueCount = (int)toolSetupDestructionQueue.size();
   bool timeToClearList = true;

   for (int i = 0; i < queueCount; ++i)
      {
      SToolSetupDestructionState &dState = toolSetupDestructionQueue[i];
      CToolSetup *toolSetup = dState.toolSetup;

      EToolSetupDestructionState nextState = dState.currentState;
      switch (dState.currentState)
         {
         case TOOL_SETUP_DESTRUCTION_NEW_ENTRY :
            RemoveToolSetup(toolSetup->GetHandle());
            toolSetup->MonitorToolSetupProcessing(AT_CMD_EXIT_THREAD);
            nextState = TOOL_SETUP_DESTRUCTION_WAITING_FOR_THREAD_END;
            break;
         case TOOL_SETUP_DESTRUCTION_WAITING_FOR_THREAD_END :
            toolSetup->MonitorToolSetupProcessing(AT_CMD_INVALID);
            if (toolSetup->IsToolStatusAllThreadsExited())
               {
               delete toolSetup;
               nextState = TOOL_SETUP_DESTRUCTION_DONE;
               }
			break;

		 default:  //TODO
			break;
         }

      dState.currentState = nextState;
      if (dState.currentState != TOOL_SETUP_DESTRUCTION_DONE)
         timeToClearList = false;
      }

   if (timeToClearList)
      toolSetupDestructionQueue.clear();
}

int CToolManager::SetActiveToolSetup(int newToolSetupHandle)
{
//   DBCOMMENT("INTERRDIAG: SetActiveToolSetup() ENTER");
   int retVal;
   string errorFunc;

try {
   errorFunc = "GetToolSetup";
   CToolSetup *toolSetup = GetToolSetup(newToolSetupHandle);

   // Check for bad handle
   MTIassert(newToolSetupHandle == -1 || toolSetup != 0);
   if (newToolSetupHandle != -1 && toolSetup == 0)
   {
      return TOOL_ERROR_INVALID_TOOL_SETUP_HANDLE;
   }

   if (toolSetup == activeToolSetup)
   {
      // Tool setup already active, so nothing to do
//      DBCOMMENT("INTERRDIAG: SetActiveToolSetup() exited - nothing to do");
      return 0;
   }

   // If there is already an active tool setup, then check that this
   // tool setup is not running.
   if (activeToolSetup != 0)
   {
      errorFunc = "GetMasterProcessingStatus";
      if (activeToolSetup->GetMasterProcessingStatus() == AT_STATUS_RUNNING)
      {
//         DBCOMMENT("INTERRDIAG: SetActiveToolSetup() exited with error: another tool setup is running");
         return TOOL_ERROR_ACTIVE_TOOL_RUNNING;
      }

      // Detach the Provisional from the currently active tool
      // TBD

      // Release the frame memory assigned to the currently active tool
      errorFunc = "GetMasterProcessingStatus";
		activeToolSetup->ReleaseAllBufferAllocators();

      activeToolSetup = 0;    // No active tool setup anymore
   }

   if (newToolSetupHandle == -1)
   {
//      DBCOMMENT("INTERRDIAG: SetActiveToolSetup() exited - deactivate only");
      return 0; // Deliberate invalid handle, so assume caller just
                // wanted to deactivate the currently active tool setup
   }

   // Set the Provisional mode
   if (provisional != 0)
   {
      errorFunc = "provisional->SetMode";
      provisional->SetMode(toolSetup->GetToolSetupType());
   }

   // Assign frame memory for new active tool setup
   errorFunc = "toolSetup->AssignAllBufferAllocators #1";
	retVal = toolSetup->AssignAllBufferAllocators();

	// We sometimes can't get all the buffers that we need. Keep deleting buffers
	// from the tool frame cache until we get them all, or die trying.
	while (retVal == IMGTOOL_ERROR_NO_AVAILABLE_BUFFERS)
	{
		// Undo partial result.
      errorFunc = "toolSetup->ReleaseAllBufferAllocators";
		toolSetup->ReleaseAllBufferAllocators();

		// Let's free up a cached tool frame, then try again.
      errorFunc = "GetToolFrameCache()->GetOldestFrame()";
		CToolFrameBuffer *tempFrameBuffer = GetToolFrameCache()->GetOldestFrame();
		if (tempFrameBuffer == nullptr)
		{
			// No more cached buffers to free up!
			break;
		}

      errorFunc = "delete tempFrameBuffer";
		delete tempFrameBuffer;
      errorFunc = "toolSetup->AssignAllBufferAllocators #2";
		retVal = toolSetup->AssignAllBufferAllocators();
	}

   if (retVal != 0)
   {
      // No dice!
      DBTRACE(retVal);
//      DBCOMMENT("INTERRDIAG: SetActiveToolSetup() exited - OUT OF BUFFERS");
      return retVal;
   }

   // Allow each Tool Processor to do something.  This attaches
   // the Provisional to the new active tool setup
   errorFunc = "toolSetup->RegisterWithSystem";
   toolSetup->RegisterWithSystem();

   activeToolSetup = toolSetup;
}
catch(...)
{
//   DBCOMMENT("INTERRDIAG: Exception thrown from GetToolSetup()");
   DBTRACE(errorFunc);
   return TOOL_ERROR_UNEXPECTED_INTERNAL_ERROR;
}

//   DBCOMMENT("INTERRDIAG: SetActiveToolSetup() EXIT");
   return 0;
}

int CToolManager::GetActiveToolSetupHandle()
{
   if (activeToolSetup == 0)
      return -1;     // No active tool setup

   return activeToolSetup->GetHandle();
}

int CToolManager::SetToolFrameRange(CToolFrameRange *toolFrameRange)
{
   int retVal;

   if (activeToolSetup == 0)
      return TOOL_ERROR_NO_ACTIVE_TOOL_SETUP;

   // Assumes a "Simple" tool setup
   CToolFrameRange mediaReaderFrameRange(0, 1);
   mediaReaderFrameRange.outFrameRange[0].randomAccess
                               = toolFrameRange->inFrameRange[0].randomAccess;
   mediaReaderFrameRange.outFrameRange[0].inFrameIndex
                               = toolFrameRange->inFrameRange[0].inFrameIndex;
   mediaReaderFrameRange.outFrameRange[0].outFrameIndex
                               = toolFrameRange->inFrameRange[0].outFrameIndex;

   CToolFrameRange *frameRangeArray[4];
   frameRangeArray[0] = &mediaReaderFrameRange;
   frameRangeArray[1] = toolFrameRange;
   frameRangeArray[2] = 0;   // Provisional Tool - no frame range
   frameRangeArray[3] = 0;   // Media Writer - don't care

   retVal = activeToolSetup->SetFrameRange(frameRangeArray);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolManager::SetToolParameters(CToolParameters *toolParams)
{
   int retVal;

   if (activeToolSetup == 0)
      return TOOL_ERROR_NO_ACTIVE_TOOL_SETUP;

   // Assumes a "Simple" tool setup

   CToolParameters *toolParamArray[4];
   toolParamArray[0] = 0;
   toolParamArray[1] = toolParams;
   toolParamArray[2] = 0;
   toolParamArray[3] = 0;

   retVal = activeToolSetup->SetParameters(toolParamArray);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolManager::RunActiveToolSetup()
{


   // The new command will be picked up the next time MonitorToolProcessing
   // is called
   requestedToolSetupCommand = AT_CMD_RUN;
   newToolSetupCommandRequested = true;

   return 0;
}

int CToolManager::StopActiveToolSetup()
{
   // The new command will be picked up the next time MonitorToolProcessing
   // is called
   requestedToolSetupCommand = AT_CMD_STOP;
   newToolSetupCommandRequested = true;

   return 0;
}

int CToolManager::EmergencyStopActiveToolSetup()
{
   if (activeToolSetup != 0)
      activeToolSetup->SetEmergencyStop();

   return StopActiveToolSetup();
}

int CToolManager::PauseActiveToolSetup()
{
   // The new command will be picked up the next time MonitorToolProcessing
   // is called
   requestedToolSetupCommand = AT_CMD_PAUSE;
   newToolSetupCommandRequested = true;

   return 0;
}


int CToolManager::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;
   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Give the "first dibs" tools the first crack at the tool events
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        (firstDibsToolObject != nullptr) && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED);
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      retVal = firstDibsToolObject->onToolProcessingStatus(autotoolStatus);
      }

   // If "first dibs" tool did not return an error or consume the event,
   // pass the event to the active tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject *activeToolObject = getActiveToolObject();
      if (activeToolObject != 0)
         {
         retVal = activeToolObject->onToolProcessingStatus(autotoolStatus);
         }
      }

   // If active tool did not return an error or consume the event,
   // then call the default tool
   if (!(retVal & TOOL_RETURN_CODE_ERROR)
       && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED))
      {
      CToolObject* defaultToolObject = getDefaultToolObject();
      if (defaultToolObject != 0)
         {
         retVal = defaultToolObject->onToolProcessingStatus(autotoolStatus);
         }
      }
   return retVal;
}

int CToolManager::onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag, const std::pair<int, int> &frameRange)
{
   int retVal;

   // Called for all tools, whether active or not!!
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         toolObj->onFrameWasDiscardedOrCommitted(frameIndex, discardedFlag, frameRange);
         }
      }

   return 0;
}
   int onClipWasDeleted(string deletedClipName);

int CToolManager::onClipWasDeleted(const string &deletedClipName)
{
   int retVal;

   // Called for all tools, whether active or not!!
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
         toolObj->onClipWasDeleted(deletedClipName);
         }
      }

   return 0;
}

int CToolManager::onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message)
{
   int retVal;

   // Called for all tools, whether active or not!!
   for (int i = 0; i < getToolCount(); ++i)
      {
      CToolObject *toolObj = toolList[i]->toolObject;
      if (toolObj != 0)
         {
	          auto status = toolObj->onFrameDiscardingOrCommitting(discardedFlag, frameRange, message);
             if (status != TOOL_RETURN_CODE_NO_ACTION)
             {
             	return status;
             }
         }
      }

   return TOOL_RETURN_CODE_NO_ACTION;
}

int CToolManager::onSwitchSubtool()
{
   // Pass the onSwitchSubtool event only to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == nullptr)
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   return activeToolObject->onSwitchSubtool();
}

EAutotoolStatus CToolManager::GetActiveToolSetupStatus()
{
   if (activeToolSetup != 0)
      return activeToolSetup->GetMasterProcessingStatus();
   else
      return AT_STATUS_INVALID;
}

EAutotoolStatus CToolManager::GetToolSetupStatus(int toolSetupHandle)
{
   CToolSetup *toolSetup = GetToolSetup(toolSetupHandle);

   if (toolSetup != 0)
      return toolSetup->GetMasterProcessingStatus();
   else
      return AT_STATUS_INVALID;
}

bool CToolManager::IsToolProcessing(bool orPaused)
{
	bool retVal = false;

	///////////////////////////////////////////////////////////////////
	// HACK!! If we are presently preloading some files for a tool,
	// wait until the process completes - should be pretty quick! This
	// is to avoid popping up a 'you must stop processing tool' dialog
   // if we happen to be called here while preloading is in progress
	///////////////////////////////////////////////////////////////////
	if (activeToolSetup != 0 &&
      activeToolSetup->GetToolSetupType() == TOOL_SETUP_TYPE_PRELOAD_ONLY &&
		GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
	  {
        WaitForToolProcessing();
	  }
	///////////////////////////////////////////////////////////////////

   if (activeToolSetup != 0
   && (GetActiveToolSetupStatus() == AT_STATUS_RUNNING
      || (orPaused && GetActiveToolSetupStatus() == AT_STATUS_PAUSED)))
	  {
	  retVal = true;
	  }

	return retVal;
}

bool CToolManager::CheckToolProcessing()
{
   bool retVal;
   bool checkAgain;
   do
   {
      checkAgain = false;
      const bool orPaused = true;
      retVal = IsToolProcessing(orPaused);
      if (retVal)
      {
         auto ok = MTIYouMustStopToolProcessingDialog();
         if (ok == MTI_DLG_RETRY)
         {
            checkAgain = true;
            MTImillisleep(1000);
         }
      }
   } while (checkAgain);

	return retVal;
}

bool CToolManager::IsProvisionalPending()
{
	return provisional->IsProvisionalPending();
}

bool CToolManager::IsProvisionalUnresolved()
{
	return provisional->IsProvisionalUnresolved();
}

bool CToolManager::CheckProvisionalUnresolved()
{
	if (IsProvisionalUnresolved())
      {
      _MTIErrorDialog("You must accept or reject the pending fix.");
      return true;
      }

   return false;
}

bool CToolManager::CheckProvisionalUnresolvedAndToolProcessing()
{
   bool retVal = true;
   int toolCallResult = TOOL_RETURN_CODE_NO_ACTION;

   if (CheckToolProcessing())
      return true;

   unsigned int firstDibsToolIndex = 0;
   CToolObject *firstDibsToolObject;

   // Give the "first dibs" tools the first crack at the tool events
   for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
        (firstDibsToolObject != nullptr) && !(retVal & TOOL_RETURN_CODE_EVENT_CONSUMED);
        firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
      {
      toolCallResult = firstDibsToolObject->TryToResolveProvisional();
      }

   if (toolCallResult == TOOL_RETURN_CODE_NO_ACTION)
      {
      CToolObject *activeTool = getActiveToolObject();
      if (activeTool != nullptr)
         {
         toolCallResult = activeTool->TryToResolveProvisional();
         }
      }

   if (toolCallResult == TOOL_RETURN_CODE_NO_ACTION)
      {
      // go ahead and do it the old-fashioned way
      retVal = CheckProvisionalUnresolved();
      }
   else
      {
      retVal = provisional->IsProvisionalUnresolved();
      }

   return retVal;
}

bool CToolManager::NotifyGOVStarting(bool *cancelStretch)
{
   bool retVal = true;

   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      retVal = activeTool->NotifyGOVStarting(cancelStretch);
   }

   return retVal;
}

void CToolManager::NotifyGOVDone()
{
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      activeTool->NotifyGOVDone();
   }
}

void CToolManager::NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                         const RECT &rect, const POINT *lasso)
{
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      activeTool->NotifyGOVShapeChanged(shape, rect, lasso);
   }
}

void CToolManager::NotifyRightClicked()
{
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      activeTool->NotifyRightClicked();
   }
}

bool CToolManager::NotifyTrackingStarting()
{
   bool retVal = false;

   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      retVal = activeTool->NotifyTrackingStarting();
   }

   return retVal;
}

void CToolManager::NotifyTrackingDone(int errorCode)
{
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      activeTool->NotifyTrackingDone(errorCode);
   }
}

void CToolManager::NotifyTrackingCleared()
{
   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      activeTool->NotifyTrackingCleared();
   }
}

bool CToolManager::NotifyStartROIMode()
{
   bool retVal = false;

   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      retVal = activeTool->NotifyStartROIMode();
   }

   return retVal;
}

bool CToolManager::NotifyUserDrewROI()
{
   bool retVal = false;

   CToolObject *activeTool = getActiveToolObject();
   if (activeTool != nullptr)
   {
      retVal = activeTool->NotifyUserDrewROI();
   }

   return retVal;
}

void CToolManager::NotifyEndROIMode()
{
	CToolObject *activeTool = getActiveToolObject();
	if (activeTool != nullptr)
	{
		activeTool->NotifyEndROIMode();
	}
}

void CToolManager::NotifyMaskChanged(const string &newMaskAsString)
{
   // Pass the NotifyMaskChanged event only to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == nullptr)
   {
      return;
   }

   activeToolObject->NotifyMaskChanged(newMaskAsString);
}

void CToolManager::NotifyMaskVisibilityChanged()
{
   // Pass the NotifyMaskVisibilityChanged event only to the active tool
   CToolObject *activeToolObject = getActiveToolObject();
   if (activeToolObject == nullptr)
   {
      return;
   }

   activeToolObject->NotifyMaskVisibilityChanged();
}

void CToolManager::NotifyStartViewOnlyMode()
{
	CToolObject *activeTool = getActiveToolObject();
	if (activeTool != nullptr)
	{
		activeTool->NotifyStartViewOnlyMode();
	}
}

void CToolManager::NotifyEndViewOnlyMode()
{
	CToolObject *activeTool = getActiveToolObject();
	if (activeTool != nullptr)
	{
		activeTool->NotifyEndViewOnlyMode();
	}
}

bool CToolManager::IsInViewOnlyMode()
{
	CToolObject *activeTool = getActiveToolObject();
	if (activeTool == nullptr)
	{
		return false;
	}

	return activeTool->IsInViewOnlyMode();
}


// Called periodically from the main thread
void CToolManager::Heartbeat()
{
   // HACK! Prevent re-entry damage when processing takes too long!
   if (!inHeartbeat)
      {
      inHeartbeat = true;

      // Check for processing status changes
      MonitorToolProcessing();
		MonitorPDLRendering();

		// All the "first dibs" tools get called.
		unsigned int firstDibsToolIndex = 0;
		CToolObject *firstDibsToolObject;
		for (firstDibsToolObject = getFirstDibsToolObject(firstDibsToolIndex);
			  firstDibsToolObject != nullptr;
			  firstDibsToolObject = getFirstDibsToolObject(++firstDibsToolIndex))
			{
			firstDibsToolObject->onHeartbeat();
			}

		// Always pass the "on heartbeat" event to the active tool.
      CToolObject *activeToolObject = getActiveToolObject();
      if (activeToolObject != 0)
         {
			activeToolObject->onHeartbeat();
         }

      // Update status bar if soemone requested we do so
      if (updateStatusBarNow)
         {
         updateStatusBarNow = false;
         provisional->UpdateStatusBar();
         }
      inHeartbeat = false;
      }
}

void CToolManager::MonitorToolProcessing()
{
   EAutotoolCommand newToolSetupCommand;
   CAutotoolCommand toolCommandObj;

   // If any tool setup's are queue for destruction, monitor it here
   MonitorToolSetupDestruction();

   if (newToolSetupCommandRequested)
      {
      newToolSetupCommand = requestedToolSetupCommand;
      }
   else
      newToolSetupCommand = AT_CMD_INVALID;

   // Clear command request flag
   newToolSetupCommandRequested = false;

   // Monitor the processing of the active tool setup
   CAutotoolStatus toolStatusObj;
   if (activeToolSetup != 0)
      toolStatusObj = activeToolSetup->MonitorToolSetupProcessing(newToolSetupCommand);

   // Check if the master tool processing status has changed
   if (toolStatusObj.status != AT_STATUS_INVALID)
      {
      // Tell the provisional that processing has started or stopped
      if (toolStatusObj.status == AT_STATUS_RUNNING)
         provisional->StartProcessing();
      else if (toolStatusObj.status == AT_STATUS_STOPPED)
         {
         provisional->StopProcessing();

         // History cleanup hack
         getActiveToolObject()->getSystemAPI()->DiscardUnvalidatedHistory();
         }

      // Inform the Active Tool about the change in the Tool Processing status
      onToolProcessingStatus(toolStatusObj);
      }

} // MonitorToolProcessing

int CToolManager::InitializePDLRendering(CPDLRenderInterface *newAppInterface,
                                         CPDL *newPDL)
{
   // TBD - probably want to do some error checking here
   //       -- check for null arguments
   //       -- check that a fix is not pending
   //       -- check that normal tool processing is not occuring
   //       -- check that PDL rendering is not already occurring

   pdlRenderingAppInterface = newAppInterface;
   currentRenderingPDL = newPDL;
   pdlRenderingStats.Init();
   pdlRenderingStatus = PDL_RENDERING_STATUS_STOPPED;

   return 0;
}

int CToolManager::StartPDLRendering(bool newRenderFlag, bool newStopOnErrorFlag)
{
   // TBD - may want to do some error checking here
   //       -- check that a fix is not pending
   //       -- check that normal tool processing is not occuring
   //       -- check that PDL rendering is not already occurring

   // The new command will be picked up the next time MonitorPDLRendering
   // is called
   requestedPDLRenderingCommand = AT_CMD_RUN;
   newPDLRenderingCommandRequested = true;
   pdlRenderFlag = newRenderFlag;
   stopOnPDLRenderErrorFlag = newStopOnErrorFlag;

   return 0;
}

int CToolManager::StopPDLRendering()
{
    // The new command will be picked up the next time MonitorPDLRendering
    // is called
    requestedPDLRenderingCommand = AT_CMD_STOP;
    newPDLRenderingCommandRequested = true;

    CToolObject *activeTool = getActiveToolObject();
    if (activeTool == nullptr)
    {
        return 0;
    }

    return activeTool->OnStopPDLRendering();
}

bool CToolManager::IsPDLRendering()
{
   return (pdlRenderingControlState != TOOL_CONTROL_STATE_STOPPED);
}

void CToolManager::SetReportedPDLRenderingError(int errorCode, const string &msg)
{
   reportedPDLRenderingErrorCode = errorCode;
   reportedPDLRenderingErrorMsg = msg;
}

int CToolManager::GetReportedPDLRenderingErrorCode()
{
   return reportedPDLRenderingErrorCode;
}

string CToolManager::GetReportedPDLRenderingErrorMsg()
{
   return reportedPDLRenderingErrorMsg;
}

void CToolManager::SetPDLRenderingStatus(EPDLRenderingStatus newRenderingStatus )
{
   pdlRenderingStatus = newRenderingStatus;
}

EPDLRenderingStatus CToolManager::GetPDLRenderingStatus()
{
   return pdlRenderingStatus;
}

/*
EAutotoolStatus CToolManager::GetPDLRenderingStatus()
{
   return pdlRenderingStatus;
}
*/

EToolControlState CToolManager::GetPDLRenderingControlState()
{
   return pdlRenderingControlState;
}

const CPDLRenderingStats* CToolManager::GetPDLRenderingStats()
{
   // returns a pointer to a CPDLRenderingStats, a class which
   // contains statistics about the current PDL rendering.
   // The contents of the statistics is meaningful while rendering.

   return &pdlRenderingStats;
}

int CToolManager::GetPDLEntryCount()
{
   if (currentRenderingPDL == 0)
      return 0;

   return currentRenderingPDL->GetEntryCount();
}

void CToolManager::MonitorPDLRendering()
{
   // This function implements to core PDL "rendering engine" that
   // iterates over the Entries in a PDL, causing the specified Tool
   // to render per the instructions in the PDL Entry.  This function
   // is called regularly by the application program.

   // Note: MonitorToolProcessing must be called just before calling this
   //       function so that the tool processing status is freshly updated.

   // This function has two parts:
   //   1) PDL Rendering State Machine and
   //   2) PDL Rendering Status Updater
   // The PDL Rendering state machine monitors and responds to transitions
   // to commands and external conditions.  The PDL Rendering Status Updater
   // interprets the transitions in the PDL Rendering state machine's state
   // and composes status information used by external components.

   // PDL Rendering State machine
   //   - responds to PDL Rendering Start and Stop commands
   //   - initiates tool processing for each PDL Entry
   //   - watches for completion of processing for a PDL Entry and
   //     then goes to next PDL Entry
   //   - watches for processing errors
   //   - outputs generated in response to input transitions
   //  Input to State Machine
   //   - Current state in pdlRenderingControlState
   //   - PDL Render Commands: AT_CMD_RUN, AT_CMD_STOP (does not respond to PAUSE)
   //   - PDL and iterator into PDL for successive PDL Entries
   //   - Active Tool Setup Status, including error status
   //  Output of State Machine
   //   - New state in pdlRenderingControlState
   //   - Updated PDL Entry iterator
   //   - Initiate processing for next PDL Entry
   //   - Stop current processing
   //   - PDL Rendering status update

   int retVal;

   // Pick up new PDL Rendering commands
   EAutotoolCommand newPDLRenderingCommand;
   if (newPDLRenderingCommandRequested)
      newPDLRenderingCommand = requestedPDLRenderingCommand;
   else
      newPDLRenderingCommand = AT_CMD_INVALID;
   newPDLRenderingCommandRequested = false;

   // Get the the latest status of the active tool
   CToolObject *toolObj = getActiveToolObject();
   int toolProcessingErrorCode;
   EToolControlState toolControlState;
   if (toolObj == 0)
      {
      toolProcessingErrorCode = 0;
      toolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
   else
      {
      toolProcessingErrorCode = toolObj->GetToolProcessingErrorCode();
      toolControlState = toolObj->GetToolProcessingControlState();
      }

   // PDL Rendering State Machine
   EToolControlState nextPDLRenderingControlState = pdlRenderingControlState;
   switch(pdlRenderingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         // Current state: Not rendering

         if (newPDLRenderingCommand == AT_CMD_RUN
             && toolControlState != TOOL_CONTROL_STATE_RUNNING
             && currentRenderingPDL != 0)
            {
            // A request was received to start rendering a PDL and the
            // prerequisite conditions have been satisfied

            pdlRenderingStatus = PDL_RENDERING_STATUS_INITIALIZING;
            currentRenderingPDLEntry = 0;
            pdlRenderingStats.Init();
            currentRenderingPDL->InitPDLEntryIterator();
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_RUNNING;
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING :
         // Current state: Rendering

         if (toolControlState == TOOL_CONTROL_STATE_STOPPED
             && newPDLRenderingCommand != AT_CMD_STOP)
            {
            // The current PDL Entry has been completed or there has
            // been a processing error.  If the PDL entry processing
            // has been completed, then go on to the next PDL Entry.
            // If there was an error, then either quit or go on to the
            // next entry, depending on the stopOnPDLRenderErrorFlag variable.
            // If there are no more entries, then the PDL is done

            if (currentRenderingPDLEntry != 0)
               {
               if (toolProcessingErrorCode == 0)
                  {
                  // Mark status of the PDL entry as processing successfully done
                  pdlRenderingStats.IncrementProcessedOkCount();
                  currentRenderingPDLEntry->SetEntryStatus(PDL_ENTRY_STATUS_PROCESSED_OK);
                  }

               else
                  {
                  // An error occurred during processing, so mark the PDL entry
                  // and remember the error code and error message
                  pdlRenderingStats.IncrementProcessedFailedCount();
                  currentRenderingPDLEntry->SetEntryStatus(PDL_ENTRY_STATUS_PROCESSED_FAILED);
                  currentRenderingPDLEntry->SetErrorCode(toolProcessingErrorCode);
                  MTIostringstream tmpMsg;
                  tmpMsg << GetReportedPDLRenderingErrorMsg();
                  if (tmpMsg.str().empty())
                     tmpMsg << "Processing failed, Error Code = "
                            << toolProcessingErrorCode;
                  currentRenderingPDLEntry->SetErrorMessage(tmpMsg.str());
                  }
               }

            if (toolProcessingErrorCode != 0 && stopOnPDLRenderErrorFlag)
               {
               // A processing error occurred and rendering should stop
               pdlRenderingStatus = PDL_RENDERING_STATUS_STOPPED_BY_ERROR;
               nextPDLRenderingControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else
               {
               // Move on to next good PDL entry
               bool foundAGoodPDLEntry = false;
               while (!foundAGoodPDLEntry)
                  {
                  CPDLEntry *pdlEntry = currentRenderingPDL->GetNextPDLEntry();
                  if (pdlEntry == 0)
                     break;   // No more PDL entries available

                  pdlRenderingStats.IncrementEntryNumber();

                  // Only process entries that have never been processed
                  if (pdlEntry->GetEntryStatus() != PDL_ENTRY_STATUS_UNPROCESSED)
                     {
                     // Increment skip count for entries that have been
                     // successfully processed, increment check failed count
                     // for any other unprocessed status
                     if (pdlEntry->GetEntryStatus() == PDL_ENTRY_STATUS_PROCESSED_OK)
                        pdlRenderingStats.IncrementSkippedCount();
                     else
                        pdlRenderingStats.IncrementCheckFailedCount();

                     if (pdlEntry->GetEntryStatus() != PDL_ENTRY_STATUS_PROCESSED_OK
                         && stopOnPDLRenderErrorFlag)
                        break;   // quit on first error
                     else
                        continue;  // keep looking for an entry to process
                     }

                  retVal = RenderPDLEntry(*pdlEntry);
                  currentRenderingPDLEntry = pdlEntry;
                  if (retVal == 0)
                     foundAGoodPDLEntry = true;
                  else
                     pdlRenderingStats.IncrementCheckFailedCount();

                  if (retVal != 0 && stopOnPDLRenderErrorFlag)
                     break;   // quit on error
                  }

               if (foundAGoodPDLEntry)
                  pdlRenderingStatus = PDL_RENDERING_STATUS_RUNNING;
               else
                  {
                  pdlRenderingStatus = PDL_RENDERING_STATUS_COMPLETED;
                  nextPDLRenderingControlState = TOOL_CONTROL_STATE_STOPPED;
                  }
               }
            }
         else if (newPDLRenderingCommand == AT_CMD_STOP)
            {
            // Stop the current tool processing
            if (toolObj != 0)
               toolObj->StopToolProcessing();

            if (currentRenderingPDLEntry != 0)
               {
               currentRenderingPDLEntry->SetEntryStatus(PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER);
               currentRenderingPDLEntry->SetErrorMessage("Processing stopped by user command");
               }

            pdlRenderingStatus = PDL_RENDERING_STATUS_STOPPED_BY_USER;
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
		 break;
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         // Current state: Waiting for rendering to stop after request to stop

         if (toolControlState == TOOL_CONTROL_STATE_STOPPED
             && newPDLRenderingCommand != AT_CMD_STOP
             /* && not an error */)
            {
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         // else if (a tool error occurred)
         //   {
              // TBD - Do something about the error
         //   }
		 break;

		 default:  //TODO
			break;
      }

   // TBD - Provide enough status information so that PDL Viewer and
   //       Rendering dialog can figure out how to update:
   //       -- when complete PDL rendering in done
   //       -- which PDL Entry has started processing
   //       -- when a PDL Entry has finished processing
   //       -- when there is an error condition to report
   // TBD - Error handling
   //       -- error from CToolSetup and CToolProcessor
   //       -- dongle time error (at beginning or midway)
   //       -- error from Media Writer (read-only DPX, etc)
   // TBD - Immediate stop (rather than waiting for current frame to finish)

   pdlRenderingControlState = nextPDLRenderingControlState;

} // MonitorPDLRendering

int CToolManager::RenderPDLEntry(CPDLEntry &pdlEntry)
{
   int retVal;   // On any error return immediately and do not attempt
                 // to render

   // Reset reported error code and message
   reportedPDLRenderingErrorCode = 0;
   reportedPDLRenderingErrorMsg = "";

   // Check that the PDL Entry is okay, i.e., clip exists, timecodes okay, etc.
   retVal = pdlEntry.CheckEntry();
   if (retVal != 0)
      return retVal;

   // TBD: 1) Check that we are licensed for source clip's resolution

   // Check that the tool is available and licensed
   string toolStatusMsg;
   retVal = CheckTool(pdlEntry.toolList[0]->toolName, toolStatusMsg);
   if (retVal != 0)
      {
      // An error was detected, so mark this PDL Entry
      pdlEntry.SetEntryStatus(PDL_ENTRY_STATUS_CHECK_FAILED);
      pdlEntry.SetErrorCode(retVal);
      pdlEntry.SetErrorMessage(toolStatusMsg);
      return retVal;
      }

   // Ask the application to initialize itself based on the instructions
   // in the PDL Entry.  This may include opening source and destination clips,
   // setting the Marks, activating the Tool and setting the Tool's parameters.
   retVal = pdlRenderingAppInterface->GoToPDLEntry(pdlEntry);
   if (retVal != 0)
      {
      // ERROR: The application could not initialize based on the PDL Entry,
      //        e.g., clip doesn't exist, tool is unavailable, etc.
      return retVal;
      }

   // The tool specified in the PDL Entry is now the active tool setup,
   // so start the tool processing.  This is an asynchronous start, so we
   // return immediately, possibly before the tool has started processing
   // and almost certainly before the tool has finished processing.
   CToolObject *toolObj = getActiveToolObject();

   if (pdlRenderFlag)
      {
      // Start rendering without highlighting
      retVal = toolObj->StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);
      }
   else
      {
      // Start Previewing with highlighting (don't change clip)
      retVal = toolObj->StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, true);
      }

   if (retVal != 0)
      return retVal; // ERROR: Start has failed.  Unlikely to occur

   // Mark PDL entry that processing is in progress
   pdlEntry.SetEntryStatus(PDL_ENTRY_STATUS_PROCESSING_IN_PROGRESS);

   return 0;
}

void CToolManager::WaitForToolSetupDestruction()
{
   bool done = false;

   while (!done)
      {
      MonitorToolSetupDestruction();

      if (toolSetupDestructionQueue.empty())
         done = true;
      else
         MTImillisleep(2);
      }
}

void CToolManager::WaitForToolProcessing(bool processMessages)
{
   if (GetActiveToolSetupStatus() == AT_STATUS_INVALID)
      {
      // No tool active or tool was never run
      return;
      }

   bool done = false;

   while (!done)
      {
      MonitorToolProcessing();

      if (GetActiveToolSetupStatus() == AT_STATUS_STOPPED)
         done = true;
      else
         {
         if (processMessages)
            Application->ProcessMessages();

         MTImillisleep(2);
         }
      }

   updateStatusBarNow = true;
}

int CToolManager::GetProvisionalLastFrameIndex()
{
   return provisional->GetLastFrameIndex();
}

//////////////////////////////////////////////////////////////////////
// Tool Setup List Functions
//////////////////////////////////////////////////////////////////////

int CToolManager::AddToolSetup(CToolSetup *newToolSetup)
{
   if (newToolSetup == 0)
      return -1;    // Cannot add null ptr to tool setup list

   // Create a new entry in the tool setup list
   toolSetupList.push_back(newToolSetup);

   // New handle is the index of the Tool Setup pointer in the
   // tool setup list
   int newHandle = (int)(toolSetupList.size() - 1);

   newToolSetup->SetHandle(newHandle);

   return newHandle;
}

void CToolManager::RemoveToolSetup(int handle)
{
   if (handle < 0 || handle >= (int)toolSetupList.size())
      return;  // Handle is out-of-range

   // Remove handle from tool setup
   CToolSetup *toolSetup = toolSetupList[handle];
   if (toolSetup != 0)
      toolSetup->SetHandle(-1);

   // Mark entry with null pointer to indicate that the handle is
   // not valid and no longer refers to a Tool Setup
   toolSetupList[handle] = 0;
}

CToolSetup* CToolManager::GetToolSetup(int handle)
{
// -1 is OK   MTIassert(handle >= 0 && handle < (int)toolSetupList.size());
   if (handle < 0 || handle >= (int)toolSetupList.size())
      return 0;  // Handle is out-of-range

   return toolSetupList[handle];
}

//////////////////////////////////////////////////////////////////////
// Buffer Allocator Management
//////////////////////////////////////////////////////////////////////

CBufferPool* CToolManager::GetBufferAllocator(int bytesPerBuffer,
                                              int bufferCount,
                                              double invisibleFieldsPerFrame)
{
   int retVal;
	int newCount;
   CBufferPool *bufferAllocator;

   // If bufferCount is 0 or less, then just make sure that a CBufferPool
   // instance exists.

   // Adjust bufferCount to accommodate the buffers for invisible fields
   if (bufferCount > 0)
      {
      bufferCount = (int)(ceil((double)bufferCount * (1.0 + invisibleFieldsPerFrame)));
      }

   // We will be looking for a buffer allocator that can supply a buffer
   // with the size of bytesPerFrame
   for (int i = 0; i < (int)bufferAllocatorList.size(); ++i)
		{
      bufferAllocator = bufferAllocatorList[i].allocator;
		if (bufferAllocator->getBufferSizeInBytes() == (size_t) bytesPerBuffer)
         {
         if (bufferCount > 0)
            {
            if (bufferCount > bufferAllocatorList[i].framesAvailable)
               {
               // Increase the size of the buffer allocator
               newCount = bufferCount - bufferAllocatorList[i].framesAvailable;

					retVal = bufferAllocator->increasePoolSize(newCount);
               if (retVal != 0)
                  {
                  TRACE_0(errout << "ERROR: CToolManager::GetBufferAllocator" << endl
                                 << "       Call to CBufferPool::increaseToolBufferPool failed"
                                 << endl
                                 << "       Returned " << retVal);
                  // TTT - Need more error handling here.  This is a soft error
                  return 0;
                  }

               bufferAllocatorList[i].framesAvailable = 0;
               bufferAllocatorList[i].framesUsed += bufferCount;
               bufferAllocatorList[i].totalFrames += newCount;
               }
            else
               {
               // We have enough buffers allocated already, so just
               // update the available and used counts
               bufferAllocatorList[i].framesAvailable -= bufferCount;
               bufferAllocatorList[i].framesUsed += bufferCount;
               }
            }

			TRACE_2(errout << "INFO: CToolManager::GetBufferAllocator Existing CBufferPool(" << bytesPerBuffer << "):"
								<< " buffer count " << bufferCount
								<< ", total frames " << bufferAllocatorList[i].totalFrames
								<< ", available " << bufferAllocatorList[i].framesAvailable
								<< ", used " << bufferAllocatorList[i].framesUsed);

			return bufferAllocator;
         }
      }

   // If we reached this point, then would could not find a buffer allocator
   // that matched the caller's size
   newCount = std::max(1, bufferCount);
   bufferAllocator = CBufferPool::Create(bytesPerBuffer);
   retVal = bufferAllocator->increasePoolSize(newCount);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CToolManager::GetBufferAllocator" << endl
                     << "       Call to CBufferPool::initToolBufferPool failed"
                     << endl
                     << "       Returned " << retVal);
      // TTT - Need more error handling here.  This is a soft error
      return 0;
      }

   // Add the new buffer allocator to this Tool Setup's buffer allocator list
   SBufferAllocator newListEntry;
   newListEntry.allocator = bufferAllocator;
   newListEntry.totalFrames = newCount;
   if (bufferCount > 0)
      {
      newListEntry.framesAvailable = 0;
      newListEntry.framesUsed = bufferCount;
      }
   else
      {
      newListEntry.framesAvailable = newCount;
      newListEntry.framesUsed = 0;
      }
   bufferAllocatorList.push_back(newListEntry);

	TRACE_2(errout << "INFO: CToolManager::GetBufferAllocator New CBufferPool(" << bytesPerBuffer << "):"
							<< " buffer count " << bufferCount
							<< ", total frames " << newListEntry.totalFrames
							<< ", available " << newListEntry.framesAvailable
							<< ", used " << newListEntry.framesUsed);
	return bufferAllocator;
}

int CToolManager::ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                                         double invisibleFieldsPerFrame)
{
   CBufferPool *bufferAllocator;

   // Adjust bufferCount to accommodate the buffers for invisible fields
   if (bufferCount > 0)
      {
      bufferCount
          = (int)(ceil((double)bufferCount * (1.0 + invisibleFieldsPerFrame)));
      }

   // We will be looking for a buffer allocator that can supply a buffer
   // with the size of bytesPerBuffer

   for (int i = 0; i < (int)bufferAllocatorList.size(); ++i)
      {
      bufferAllocator = bufferAllocatorList[i].allocator;
      if (bufferAllocator->getBufferSizeInBytes() == (size_t) bytesPerBuffer)
         {
         bufferAllocatorList[i].framesAvailable =
                    std::min(bufferAllocatorList[i].totalFrames,
                        bufferAllocatorList[i].framesAvailable + bufferCount);
         bufferAllocatorList[i].framesUsed =
                    std::max(0, bufferAllocatorList[i].framesUsed - bufferCount);

			TRACE_2(errout << "INFO: CToolManager::ReleaseBufferAllocator(" << bytesPerBuffer << "):"
								<< " buffer count " << bufferCount
								<< ", total frames " << bufferAllocatorList[i].totalFrames
								<< ", available " << bufferAllocatorList[i].framesAvailable
								<< ", used " << bufferAllocatorList[i].framesUsed);

         break;
         }
      }

   return 0;
}

void CToolManager::DeleteBufferAllocators()
{
   // VERY IMPORTANT! CLEAR THE TOOL FRAME BUFFER CACHE FIRST!
   toolFrameCache->Clear();

   // Delete all of the buffer allocators.
   // Note: Make sure nobody is using the buffer allocators anymore
   //       or is holding a buffer that was allocated with one
   //       of the CBufferPool that came from GetBufferAllocator.
   //       Otherwise terrible things will happen!

// We no longer destroy the whole buffer pool because we're afraid some buffers
// might still be in use, so we just get rid of unused buffers instead.
// Also I don't see why we shouldn't keep the existing allocators - they're
// small and generally frame sizes will be the same among clips being worked on!
//
//   for (int i = 0; i < (int)bufferAllocatorList.size(); ++i)
//      {
//      CBufferPool::Destroy(bufferAllocatorList[i].allocator);
//      }
//
//   bufferAllocatorList.clear();

   for (int i = 0; i < (int)bufferAllocatorList.size(); ++i)
      {
      bool allBuffersWereDestroyed = CBufferPool::DestroyAllUnusedBuffers(bufferAllocatorList[i].allocator);
      MTIassert(allBuffersWereDestroyed);
      if (!allBuffersWereDestroyed)
         {
         TRACE_0(errout << "INTERNAL ERROR: some buffers of size "
                        << bufferAllocatorList[i].allocator->getBufferSizeInBytes()
                        << " were in use so could not be destroyed!");
         }

         // Just abandon those buffers!
         bufferAllocatorList[i].totalFrames = 0;
         bufferAllocatorList[i].framesAvailable = 0;
         bufferAllocatorList[i].framesUsed = 0;
      }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                ###############################
// ###     CPDLRenderingStats Class     ##############################
// ####                                ###############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

CPDLRenderingStats::CPDLRenderingStats()
 : entryNumber(-1), processedOkCount(0), checkFailedCount(0),
   processedFailedCount(0), skippedCount(0), hasChanged(false)
{
}

void CPDLRenderingStats::Init()
{
   entryNumber = 0;
   processedOkCount = 0;
   checkFailedCount = 0;
   processedFailedCount = 0;
   skippedCount = 0;
   hasChanged = true;
}

void CPDLRenderingStats::IncrementEntryNumber()
{
   ++entryNumber;
   hasChanged = true;
}

void CPDLRenderingStats::IncrementProcessedOkCount()
{
   ++processedOkCount;
   hasChanged = true;
}

void CPDLRenderingStats::IncrementCheckFailedCount()
{
   ++checkFailedCount;
   hasChanged = true;
}

void CPDLRenderingStats::IncrementProcessedFailedCount()
{
   ++processedFailedCount;
   hasChanged = true;
}

void CPDLRenderingStats::IncrementSkippedCount()
{
   ++skippedCount;
   hasChanged = true;
}

int CPDLRenderingStats::GetEntryNumber() const
{
   return entryNumber;
}

int CPDLRenderingStats::GetProcessedOkCount() const
{
   return processedOkCount;
}

int CPDLRenderingStats::GetCheckFailedCount() const
{
   return checkFailedCount;
}

int CPDLRenderingStats::GetProcessedFailedCount() const
{
   return processedFailedCount;
}

int CPDLRenderingStats::GetSkippedCount() const
{
   return skippedCount;
}

bool CPDLRenderingStats::IsModified() const
{
   bool modified = hasChanged;
   hasChanged = false;
   return modified;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                        #######################################
// ###     CToolSetup Class     ######################################
// ####                        #######################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolSetup::CToolSetup(EToolSetupType newToolSetupType)
 : toolSetupType(newToolSetupType), handle(-1),
   masterToolControlState(TOOL_CONTROL_STATE_STOPPED),
   masterToolStatus(AT_STATUS_STOPPED),
   emergencyStopFlag(false)
{
}

CToolSetup::~CToolSetup()
{
   DeleteNodeList();    // Delete Tool Nodes and their Tool Processors

   DeleteEdgeList();
}

void CToolSetup::DeleteNodeList()
{
   // Note:  Make sure that the tool proc has flushed any buffers it
   //        was holding and that any threads in the tool proc have
   //        been stopped

   for (int i = 0; i < GetNodeCount(); ++i)
      {
      CToolProcessor *toolProc = nodeList[i]->GetToolProc();
      delete toolProc;   // Also deletes the node
      }

   nodeList.clear();
}

void CToolSetup::DeleteEdgeList()
{
   for (int i = 0; i < GetEdgeCount(); ++i)
      {
      delete edgeList[i];
      }

   edgeList.clear();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CToolSetup::CreateFromIUPE(IUPEToolList &newToolList, IUPEEdgeList &newEdgeList)
{
   CToolManager toolMgr;

   // Iterate over the tools in the IUPE's tool list
   int toolCount = (int)newToolList.size();
   for (int toolListIndex = 0; toolListIndex < toolCount; ++toolListIndex)
      {
      SIUPETool &IUPETool = newToolList[toolListIndex];

// TTT - DO WE ALLOW FOR MediaReader AND MediaWriter NODES THAT ALREADY
//       EXIST, OR DO WE RECREATE THEM EVERY TIME????

      // Given the tool's name from the PDL Event, get a pointer to the
      // tool's CToolObject subtype instance
      int toolMgrIndex = toolMgr.findTool(IUPETool.toolName);
      if (toolMgrIndex < 0)
         {
         TRACE_0(errout << "ERROR: CToolSetup::CreateFromIUPE: CToolManager could "
                        << endl
                        << "       not find the tool named: "
                        << IUPETool.toolName);
         return TOOL_ERROR_CANNOT_FIND_TOOL_BY_NAME;
         }

      CToolObject *toolObj = toolMgr.GetTool(toolMgrIndex);
      if (toolObj == 0)
         {
         TRACE_0(errout << "ERROR: CToolSetup::CreateFromIUPE:" << endl
                        << "       CToolManager::GetTool returned null pointer"
                        << endl
                        << "       for tool named: " << IUPETool.toolName);
         return TOOL_ERROR_TOOL_OBJ_NOT_AVAILABLE;
         }

      // Create a new instance of the tool's CToolProcessor subtype
      CToolProcessor *toolProc = toolObj->makeToolProcessorInstance(
                                                           &emergencyStopFlag);
      if (toolProc == 0)
         {
         TRACE_0(errout << "ERROR: CToolSetup::CreateFromIUPE:" << endl
                        << "       CToolObject::makeToolProcessorInstance"
                        << endl
                        << "       returned null pointer for tool named: "
                        << IUPETool.toolName);
         return TOOL_ERROR_TOOL_PROCESSOR_INSTANTIATION_FAILED;
         }

      // Get the CToolNode from the new CToolProcessor subtype instance
      CToolNode *toolNode = toolProc->GetToolNode();

      // Put the new node in this Tool Setup's node list
      AddNode(toolNode);
      }

   // Iterate over the edges in the IUPE's edge list
   int edgeCount = (int)newEdgeList.size();
   for (int edgeListIndex = 0; edgeListIndex < edgeCount; ++edgeListIndex)
      {
      SIUPEEdge &IUPEEdge = newEdgeList[edgeListIndex];

      // Create a new CToolEdge instance to link two tools
      CToolEdge *toolEdge = new CToolEdge;
      AddEdge(toolEdge);

      CToolNode *srcToolNode = nodeList[IUPEEdge.srcToolListIndex];
      toolEdge->SetSrcNode(srcToolNode, IUPEEdge.srcOutPortIndex);

      CToolNode *dstToolNode = nodeList[IUPEEdge.dstToolListIndex];
      toolEdge->SetDstNode(dstToolNode, IUPEEdge.dstInPortIndex);
      }

   return 0;
}

void CToolSetup::AddEdge(CToolEdge *toolEdge)
{
   edgeList.push_back(toolEdge);
}

void CToolSetup::AddNode(CToolNode *toolNode)
{
   nodeList.push_back(toolNode);
   toolNode->SetParentToolSetup(this);
}

int CToolSetup::SetIOConfig(IUPEToolList &toolList)
{
   int retVal;

   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];
      CToolIOConfig *toolIOConfig = toolList[nodeIndex].toolIOConfig;
      retVal = node->SetIOConfig(toolIOConfig);
      if (retVal != 0)
         return retVal; // ERROR: Tool's I/O configuration has failed
      }

   return 0;
}

int CToolSetup::SetFrameRange(CToolFrameRange *frameRangeArray[])
{
   int retVal;

   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];
      CToolFrameRange *toolFrameRange = frameRangeArray[nodeIndex];
      retVal = node->SetFrameRange(toolFrameRange);
      if (retVal != 0)
         return retVal; // ERROR: Setting tool's frame range has failed
      }

   return 0;
}

int CToolSetup::SetParameters(CToolParameters *toolParamArray[])
{
   int retVal;

   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];
      CToolParameters *toolParams = toolParamArray[nodeIndex];
      retVal = node->SetParameters(toolParams);
      if (retVal != 0)
         return retVal; // ERROR: Setting tool's parameters has failed
      }

   return 0;
}

int CToolSetup::RegisterWithSystem()
{
   // Allows each Tool Proc in tool setup to do something when
   // the setup becomes the active setup

   int retVal;

   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];
      retVal = node->RegisterWithSystem();
      if (retVal != 0)
         return retVal; // ERROR: Tool's I/O configuration has failed
      }

   return 0;
}

int CToolSetup::PutToolCommand(CAutotoolCommand &command)
{
   int retVal;

   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      retVal = node->PutToolCommand(command);
      if (retVal != 0)
         return retVal; // ERROR: Failed to queue the tool command
      }

   return 0;
}

void CToolSetup::SetEmergencyStop()
{
   // Set the emergency stop flag, which is visible to all CToolProcessors
   // in the Tool Setup
   emergencyStopFlag = true;
}

EToolSetupType CToolSetup::GetToolSetupType()
{
   return toolSetupType;
}

//////////////////////////////////////////////////////////////////////
// Tool Status
//////////////////////////////////////////////////////////////////////

CAutotoolStatus
CToolSetup::MonitorToolSetupProcessing(EAutotoolCommand newToolSetupCommand)
{
   EToolControlState nextToolControlState;
   CAutotoolCommand toolCommandObj;
   int toolErrorCode = 0;

   // Poll each of this Tool Setup's tool Status Queues to find
   // out the status of its Tool Processor
   MonitorToolStatus();

   // Tool Setup Master Control State Machine
   nextToolControlState = masterToolControlState;
   switch(masterToolControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         if (newToolSetupCommand == AT_CMD_EXIT_THREAD)
            {
            toolCommandObj.command = AT_CMD_EXIT_THREAD;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
            }
         else if (newToolSetupCommand == AT_CMD_RUN)
            {
            ClearToolStatus();
            emergencyStopFlag = false;
            toolCommandObj.command = AT_CMD_RUN;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            }
         break;
	  case TOOL_CONTROL_STATE_WAITING_TO_STOP :
		 if (newToolSetupCommand == AT_CMD_EXIT_THREAD)
            {
            // QQQ This would bypass History Cleanup Hack. Is that a problem?
            // I'd feel more comfortable if we always went through STOPPED
            toolCommandObj.command = AT_CMD_EXIT_THREAD;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
            }
		 else if (IsToolStatusAllStopped())
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING :
         if (newToolSetupCommand == AT_CMD_EXIT_THREAD)
            {
            toolCommandObj.command = AT_CMD_EXIT_THREAD;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
            }
         else if (IsToolStatusAnyEndOfMaterial())
			{
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (IsToolStatusAnyError())
            {
            toolErrorCode = GetToolStatusFirstErrorCode();
            TRACE_0(errout << "ERROR: CToolSetup::MonitorToolSetupProcessing while RUNNING ("
                           << toolErrorCode << ")");
            toolCommandObj.command = AT_CMD_STOP;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (newToolSetupCommand == AT_CMD_STOP)
            {
            toolCommandObj.command = AT_CMD_STOP;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (newToolSetupCommand == AT_CMD_PAUSE)
            {
            toolCommandObj.command = AT_CMD_PAUSE;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
            }
         break;
	  case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
         if (newToolSetupCommand == AT_CMD_EXIT_THREAD)
            {
            toolCommandObj.command = AT_CMD_EXIT_THREAD;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
            }
         else if (IsToolStatusAllStopped())
            {
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         else if (IsToolStatusAllPausedOrStopped())
            {
            nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
            }
         break;
      case TOOL_CONTROL_STATE_PAUSED :
         if (newToolSetupCommand == AT_CMD_EXIT_THREAD)
            {
            toolCommandObj.command = AT_CMD_EXIT_THREAD;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
            }
         else if (newToolSetupCommand == AT_CMD_STOP)
            {
            toolCommandObj.command = AT_CMD_STOP;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (newToolSetupCommand == AT_CMD_RUN)
            {
            ClearToolStatus();
            toolCommandObj.command = AT_CMD_RUN;
            PutToolCommand(toolCommandObj);
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
            }
         break;
      case TOOL_CONTROL_STATE_EXITING_THREAD :
         if (IsToolStatusAllThreadsExited())
            {
            // TTT - WHAT DO WE DO HERE?
            }
		 break;

		 default:  //TODO
			break;
      }

   CAutotoolStatus newToolStatusObj;
   newToolStatusObj.toolSetupHandle = GetHandle();
   if (masterToolControlState != nextToolControlState)
      {
      // Control State has changed
      switch(nextToolControlState)
         {
         case TOOL_CONTROL_STATE_STOPPED :
            newToolStatusObj.status = AT_STATUS_STOPPED;
            break;
         case TOOL_CONTROL_STATE_RUNNING :
            newToolStatusObj.status = AT_STATUS_RUNNING;
            break;
         case TOOL_CONTROL_STATE_PAUSED :
            {
            newToolStatusObj.status = AT_STATUS_PAUSED;
            CToolManager toolMgr;
            newToolStatusObj.frameIndex
                                      = toolMgr.GetProvisionalLastFrameIndex();
            }
            break;
         case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
         case TOOL_CONTROL_STATE_EXITING_THREAD :
         default :
            newToolStatusObj.status = AT_STATUS_INVALID;
            break;
         }
      }

   // Remember error code in newToolStatusObj
   // toolErrorCode is set to 0 if no error.  If there was an error in
   // to bowels of the tool processing, then toolErrorCode has got that error.
   newToolStatusObj.errorCode = toolErrorCode;
   if (toolErrorCode != 0)
     newToolStatusObj.status = AT_STATUS_PROCESS_ERROR;

   masterToolControlState = nextToolControlState;

   // Remember tool status
   if (newToolStatusObj.status != AT_STATUS_INVALID)
      masterToolStatus = newToolStatusObj.status;

   return newToolStatusObj;

} // MonitorToolSetupProcessing

EAutotoolStatus CToolSetup::GetMasterProcessingStatus()
{
   return masterToolStatus;
}

void CToolSetup::ClearToolStatus()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      node->ClearToolStatus();
      }
}

int CToolSetup::MonitorToolStatus()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      node->MonitorToolStatus();
      }

   return 0;
}

bool CToolSetup::IsToolStatusAllStopped()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (!node->IsToolStatusStopped())
         return false;
      }

   return true;
}

bool CToolSetup::IsToolStatusAllPaused()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (!node->IsToolStatusPaused())
         return false;
      }

   return true;
}

bool CToolSetup::IsToolStatusAllPausedOrStopped()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (!node->IsToolStatusPaused() && !node->IsToolStatusStopped())
         return false;
      }

   return true;
}

bool CToolSetup::IsToolStatusAnyError()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (node->IsToolStatusError())
         return true;
      }

   return false;
}

int CToolSetup::GetToolStatusFirstErrorCode()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (node->IsToolStatusError())
         return node->GetToolStatusErrorCode();
      }

   return 0;
}

bool CToolSetup::IsToolStatusAnyEndOfMaterial()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (node->IsToolStatusEndOfMaterial())
         return true;
      }

   return false;
}

bool CToolSetup::IsToolStatusAllThreadsExited()
{
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      int nodeIndex = *iterator;
      CToolNode *node = nodeList[nodeIndex];

      if (!node->IsToolStatusThreadExited())
         return false;
      }

   return true;
}



//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CToolSetup::GetEdgeCount() const
{
   return (int)edgeList.size();
}

int CToolSetup::GetNodeCount() const
{
   return (int)nodeList.size();
}

//////////////////////////////////////////////////////////////////////
// Buffer Allocator Management
//////////////////////////////////////////////////////////////////////

int CToolSetup::AssignAllBufferAllocators()
{
   int retVal;

   TRACE_2(errout << "INFO: CToolSetup::AssignAllBufferAllocators NODES");

   // Iterator over the tool nodes
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      TRACE_2(errout << "INFO: CToolSetup::AssignAllBufferAllocators Node " << *iterator);
      retVal = nodeList[*iterator]->AssignAllBufferAllocators();
      if (retVal != 0)
         return retVal; // ERROR:
      }

   TRACE_2(errout << "INFO: CToolSetup::AssignAllBufferAllocators EDGES "
						<< GetEdgeCount());

   // Iterate over the tool edges
	for (int i = 0; i < GetEdgeCount(); ++i)
      {
      TRACE_2(errout << "INFO: CToolSetup::AssignAllBufferAllocators Edge " << i);
      retVal = edgeList[i]->AssignAllBufferAllocators();
      if (retVal != 0)
         return retVal; // ERROR:
      }

   return 0;
}

void CToolSetup::ReleaseAllBufferAllocators()
{
   TRACE_2(errout << "INFO: CToolSetup::ReleaseAllBufferAllocators NODES");

   // Iterator over the tool nodes
   for (list<int>::iterator iterator = topologicalSort.begin();
        iterator != topologicalSort.end();
        ++iterator)
      {
      nodeList[*iterator]->ReleaseAllBufferAllocators();
      }

	TRACE_2(errout << "INFO: CToolSetup::ReleaseAllBufferAllocators EDGES "
						<< GetEdgeCount());

   // Iterate over the tool edges
   for (int i = 0; i < GetEdgeCount(); ++i)
      {
      edgeList[i]->ReleaseAllBufferAllocators();
      }
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CToolSetup::GetHandle() const
{
   return handle;
}

void CToolSetup::SetHandle(int newHandle)
{
   handle = newHandle;
}

//////////////////////////////////////////////////////////////////////
// Graph Algorithms
// (See Cormen, Leiserson & Rivest, Intro to Algorithms, Ch. 23)
//////////////////////////////////////////////////////////////////////

int CToolSetup::TopologicalSort()
{
   int retVal;

   retVal = DepthFirstSearch();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CToolSetup::DepthFirstSearch()
{
   int nodeIndex;
   int retVal;

   topologicalSort.clear();

   timestamp = 0;

   int nodeCount = GetNodeCount();

   // Prepare nodes in node list for Depth-first-search
   for (nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
      nodeList[nodeIndex]->InitForGraphAlgorithms(nodeIndex);

   // Visit each node in DAG
   for (nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
      {
      CToolNode *node = nodeList[nodeIndex];
      if (node->GetNodeColor() == GNC_WHITE)
         {
         retVal = DFS_Visit(node);     // Recurse
         if (retVal != 0)
            return retVal;   // ERROR
         }
      }

   return 0;
}

// Recursion function for Depth First Search of Tool DAG
int CToolSetup::DFS_Visit(CToolNode *node)
{
   int retVal;

   // A white node has been discovered
   node->Discover(++timestamp);

   // For each entry in node's adjacency list...
   for (int outPort = 0; outPort < node->GetOutPortCount(); ++outPort)
      {
      CToolNode *dstNode = node->GetDstNode(outPort);
      EGraphNodeColor dstNodeColor = dstNode->GetNodeColor();
      MTIassert(dstNodeColor != GNC_GRAY);  // programming error
      if (dstNodeColor == GNC_GRAY)
         {
         // The GRAY color on the destination node indicates that the
         // destination node is an ancestor of the input node in the
         // depth-first tree.  This indicates that there is a cycle in the
         // Tool DAG.  Cycles in Tool DAG are outlawed, so this is an error.
         return TOOL_ERROR_CYCLE_IN_TOOL_DAG;
         }
      else if (dstNodeColor == GNC_WHITE)
         {
         // We have "discovered" a node.  This destination node is WHITE,
         // indicating it has never been visited.  Use the destination node
         // as the starting point for a new depth first search by recursing
         // into DFS_Visit.
         dstNode->SetPredecessorNode(node);
         retVal = DFS_Visit(dstNode);
         if (retVal != 0)
            return retVal; // ERROR, so unwind recursion
         }
      }

   node->Finish(++timestamp);

   // Accumulate node indices into the topological sort
   topologicalSort.push_front(node->GetNodeIndex());

   return 0;
}
