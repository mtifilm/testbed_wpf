#ifndef ColorPickToolH
#define ColorPickToolH
#include "PropX.h"
#include "UserInput.h"
class CToolSystemInterface;


class CColorPickTool
{
public:

   CColorPickTool();

   void loadSystemAPI(CToolSystemInterface *newSystemAPI);

   // Go in/out of Color Pick mode
   void SetEnabled(bool newEnabled);
   bool IsEnabled();

   // The following callback serves to notify the caller whenever the color
   // under the cursor changes while the ColorPickTool is active and the
   // left mouse button is held down
   CBHook newColorHoveredOver;

   // The following callback notifies the caller when the left mouse button
   // is released while the colorpicker
   CBHook newColorPicked;

   // Get the current color - caller will call this in response to either
   // of the above callbacks
   void getColor(int &comp0, int &comp1, int &comp2);
   void getXY(int &Xout, int &Yout);

   // These need to get called appropriately by the owner tool
   int onMouseDown(CUserInput &userInput);
   int onMouseMove(CUserInput &userInput);
   int onMouseUp(CUserInput &userInput);

private:

   bool Enabled;
   int R, G, B, X, Y;
   CToolSystemInterface *SystemAPI;
   bool MouseIsDown;

};

#endif
 