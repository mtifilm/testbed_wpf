// ToolSetup.h: interface for the CToolNode, CToolEdge and CToolSetup classes.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolNode.h,v 1.10 2005/01/10 16:22:35 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOL_SETUP_H)
#define TOOL_SETUP_H

#include "IniFile.h"   // bring in lots of core-code stuff

#include <string>
#include <vector>
using std::string;
using std::vector;

//////////////////////////////////////////////////////////////////////

#define END_OF_MATERIAL_FRAME_INDEX -2000
#define NO_FRAMES_AVAILABLE -2001

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CAutotoolCommand;
class CAutotoolStatus;
class CBufferPool;
class CImageFormat;
class CToolEdge;
class CToolFrameBuffer;
class CToolFrameQueue;
class CToolFrameRange;
class CToolIOConfig;
class CToolParameters;
class CToolProcessor;
class CToolSetup;


//////////////////////////////////////////////////////////////////////

// Node colors used by graph algorithms.
enum EGraphNodeColor
{
   GNC_WHITE, // Node has not been discovered by search
   GNC_GRAY,  // Node has been discovered and adjacency list is being explored
   GNC_BLACK  // Finished visiting node
};

class CToolNode
{
public:
   CToolNode(CToolProcessor *newToolProc, int newInPortCount,
             int newOutPortCount);
   virtual ~CToolNode();

   int PutToolCommand(CAutotoolCommand &command);
   int GetToolStatus(CAutotoolStatus &toolStatus);

   CToolProcessor* GetToolProc() const;

   CToolFrameBuffer* GetFrame(int port, int frameIndex);
   CToolFrameBuffer* GetFrame(int port);
   bool PutFrame(int port, CToolFrameBuffer *frameBuffer);
   bool IsOutPortFull(int port);

   // These are hacks to bypass a writer tool's regular tool processor and
   // instead simply write a single frame synchronously or asynchronously
   int PutFrameSynchronous(int outPortIndex, CToolFrameBuffer *frameBuffer);
   int WriteFrameSynchronous(int inPortIndex, CToolFrameBuffer *frameBuffer);
   int PutFrameAsynchronous(int outPortIndex, CToolFrameBuffer *frameBuffer);
   int WriteFrameAsynchronous(int inPortIndex, CToolFrameBuffer *frameBuffer);

   void RequestFrame(int port, int frameIndex);
   void RemoveRequest(int port, int frameIndex);

   int GetInPortCount() const;
   int GetOutPortCount() const;

   CToolNode *GetDstNode(int outPortIndex);
   CToolNode *GetSrcNode(int inPortIndex);

   void SetInPort(CToolEdge *newSrcEdge, int inPortIndex);
   void SetOutPort(CToolEdge *newDstEdge, int outPortIndex);

   const CImageFormat* GetSrcImageFormat(int inPort) const;
   void SetDstImageFormat(int outPort, const CImageFormat *newImageFormat);
   double GetSrcInvisibleFieldsPerFrame(int inPortIndex) const;
   void SetDstInvisibleFieldsPerFrame(int outPortIndex,
                                      double newInvisibleFieldsPerFrame);
   void SetSrcMaxEntryCount(int inPortIndex, int newMaxEntryCount);
   void SetDstMaxEntryCount(int outPortIndex, int newMaxEntryCount);

   void FlushDstFrameQueue(int outPortIndex);

   CBufferPool* GetBufferAllocator(int bytesPerBuffer, int bufferCount,
                                   double invisibleFieldsPerFrame);
   void ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                               double invisibleFieldsPerFrame);
   int AssignAllBufferAllocators();
   void ReleaseAllBufferAllocators();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);
   int SetParameters(CToolParameters *toolParams);
   int RegisterWithSystem();

   void SetParentToolSetup(CToolSetup *newToolSetup);

   // Member functions used by graph algorithms
   void InitForGraphAlgorithms(int newNodeIndex);
   void Discover(int timestamp);
   void Finish(int timestamp);
   EGraphNodeColor GetNodeColor() const;
   void SetNodeColor(EGraphNodeColor newColor);
   CToolNode* GetPredecessorNode() const;
   void SetPredecessorNode(CToolNode *newPredecessorNode);
   int GetNodeIndex() const;

   // Tool Status
   void ClearToolStatus();
   bool IsToolStatusStopped();
   bool IsToolStatusPaused();
   bool IsToolStatusThreadExited();
   bool IsToolStatusEndOfMaterial();
   bool IsToolStatusError();
   int GetToolStatusErrorCode();
   int MonitorToolStatus();

private:
   typedef vector<CToolEdge*> PortList;

   struct SToolStatusFlags
   {
      bool endOfMaterial;
      bool error;
      int errorCode;
      bool stopped;
      bool paused;
      bool exitThread;
   };

private:
   CToolSetup *parentToolSetup;

   CToolProcessor *toolProc;

   PortList inPortList;
   PortList outPortList;

   SToolStatusFlags toolStatusFlags;

   // Variables used for graph algorithms
   EGraphNodeColor nodeColor;
   CToolNode *predecessorNode;
   int discoverTimestamp;
   int finishTimestamp;
   int nodeIndex;
};

// --------------------------------------------------------------------------

class CToolEdge
{
public:
   CToolEdge();
   virtual ~CToolEdge();

   CToolFrameBuffer* GetFrame(int frameIndex);
   CToolFrameBuffer* GetFrame();
   bool PutFrame(CToolFrameBuffer *frameBuffer);
   bool IsFrameQueueFull();
   int PutFrameSynchronous(CToolFrameBuffer *frameBuffer);
   int PutFrameAsynchronous(CToolFrameBuffer *frameBuffer);

   CToolNode* GetDstNode() const;
   CToolNode* GetSrcNode() const;

   void SetDstNode(CToolNode *newDstNode, int newDstInPortIndex);
   void SetSrcNode(CToolNode *newSrcNode, int newSrcOutPortIndex);

   const CImageFormat* GetImageFormat() const;
   void SetImageFormat(const CImageFormat* newImageFormat);
   double GetInvisibleFieldsPerFrame() const;
   void SetInvisibleFieldsPerFrame(double newInvisibleFieldsPerFrame);
   void SetMaxEntryCount(int newMaxEntryCount);
   
   int AssignAllBufferAllocators();
   void ReleaseAllBufferAllocators();
   
   bool Empty();
   void Flush();

private:
   CToolNode *srcNode;  // Source Tool Node
   int srcPortIndex;
   CToolNode *dstNode;  // Destination Tool Node
   int dstPortIndex;

   double invisibleFieldsPerFrame;
   CToolFrameQueue *frameQueue;   // Frame Buffer Queue
};


//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOL_SETUP_H)




