#ifndef ToolProgressMonitorH
#define ToolProgressMonitorH

#include "unknwn.h"

/*
 * Tool Progress Monitor Interface
 */

class IToolProgressMonitor
{
public:
	virtual void SetIdle(bool setMessage = true) = 0;
	virtual void SetStatusMessage(const char *newMessage) = 0;
	virtual void PushStatusMessage(const char *newMessage) = 0;
	virtual void PopStatusMessage() = 0;
	virtual void SetToolMessage(const char *newMessage) = 0;
	virtual void SetFrameCount(int newFrameCount, int newClicksPerFrame = 1, int preElapsedFrames = 0, double timePerFrameInSeconds=0.0) = 0;
	virtual void StartProgress() = 0;
	virtual void BumpProgress(int bumpCount = 1) = 0;
	virtual void StopProgress(bool processedToEnd = true) = 0;
   virtual void Push() = 0;
   virtual void Pop() = 0;
	virtual double GetTimePerFrameInSeconds() = 0;
};

#endif // TOOL_PROGRESS_MONITOR_H
