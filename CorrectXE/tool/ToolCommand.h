// ToolCommand.h: interface for the CToolCommand class.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolCommand.h,v 1.2 2001/08/06 18:36:19 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOLCOMMAND_H)
#define TOOLCOMMAND_H

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CToolObject;

//////////////////////////////////////////////////////////////////////

class CToolCommand
{
public:
	CToolCommand(CToolObject *newToolObject, int newCommandNumber);
	virtual ~CToolCommand();

	int execute();
   int getCommandNumber();

private:
   CToolObject *toolObject;
   int commandNumber;

   // TBD: More command info stuff needed here

};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLCOMMAND_H)


