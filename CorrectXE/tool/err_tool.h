// err_tool.h: Definitions of tool library error codes.
//
/*
$Header: /usr/local/filmroot/tool/include/err_tool.h,v 1.5 2005/01/04 01:06:47 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef TOOL_ERRORS_H
#define TOOL_ERRORS_H

//////////////////////////////////////////////////////////////////////
// Error Code Defines (Numeric Order)
//
// Note: try to keep numbers in the range of -3000 to -3999

#define TOOL_ERROR_SUCCESS                                  0

// Generic error codes
// NB: Use of these is discouraged in favor of specific error codes
#define TOOL_ERROR_FUNCTION_NOT_IMPLEMENTED                 -3000
#define TOOL_ERROR_UNEXPECTED_INTERNAL_ERROR                -3001
#define TOOL_ERROR_UNEXPECTED_NULL_RETURN                   -3002
#define TOOL_ERROR_UNEXPECTED_NULL_ARGUMENT                 -3003

// Specific error codes
#define TOOL_ERROR_TOOL_COMMAND_QUEUE_EMPTY                 -3100
#define TOOL_ERROR_TOOL_STATUS_QUEUE_EMPTY                  -3101
#define TOOL_ERROR_INVALID_IN_PORT_INDEX                    -3102
#define TOOL_ERROR_INVALID_OUT_PORT_INDEX                   -3103
#define TOOL_ERROR_CYCLE_IN_TOOL_DAG                        -3104
#define TOOL_ERROR_CANNOT_FIND_TOOL_BY_NAME                 -3105
#define TOOL_ERROR_TOOL_OBJ_NOT_AVAILABLE                   -3106
#define TOOL_ERROR_TOOL_PROCESSOR_INSTANTIATION_FAILED      -3107
#define TOOL_ERROR_CANNOT_ALLOCATE_FRAME_BUFFERS            -3108
#define TOOL_ERROR_CANNOT_DESTROY_ACTIVE_TOOL_SETUP         -3109

#define TOOL_ERROR_INVALID_TOOL_SETUP_HANDLE                -3110
#define TOOL_ERROR_ACTIVE_TOOL_RUNNING                      -3111
#define TOOL_ERROR_NO_ACTIVE_TOOL_SETUP                     -3112
#define TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED                -3113
#define TOOL_ERROR_TOOL_IS_NOT_LICENSED                     -3114
#define TOOL_ERROR_UNKNOWN_LICENSE                          -3115
#define TOOL_ERROR_TIMECODE_NOT_IN_DESTINATION              -3116
#define TOOL_ERROR_FRAME_IS_MISSING                         -3117

#define TOOL_ERROR_INVALID_TRACKING_BOX_TAG                 -3118
#define TOOL_ERROR_CORRUPT_TRACKING_BOX                     -3119
#define TOOL_ERROR_TRACKING_BOX_TOO_CLOSE_TO_EDGE           -3120

//////////////////////////////////////////////////////////////////////
// Error Code Descriptions (Alpha Order)
/*

TOOL_ERROR_ACTIVE_TOOL_RUNNING
   Attempt to perform some action, such as changing the active
   tool setup, while the active tool setup is running.
   
TOOL_ERROR_CANNOT_ALLOCATE_FRAME_BUFFERS
   Cannot allocate all of the frame buffers that we want

TOOL_ERROR_CANNOT_DESTROY_ACTIVE_TOOL_SETUP
   An attempt was made to destroy a Tool Setup while it was the active
   tool setup.

TOOL_ERROR_CANNOT_FIND_TOOL_BY_NAME
   Query for a Tool Object by name to Tool Manager failed
   
TOOL_ERROR_CYCLE_IN_TOOL_DAG
   There is a cycle in the data flow DAG of the tool setup.

TOOL_ERROR_INVALID_IN_PORT_INDEX
   An InPort index was out-of-range in a member function of a CToolNode
   or CToolProcessor subtype instance.

TOOL_ERROR_INVALID_OUT_PORT_INDEX
   An OutPort index was out-of-range in a member function of a CToolNode
   or CToolProcessor subtype instance.

TOOL_ERROR_INVALID_TOOL_SETUP_HANDLE
   Invalid tool setup handle was passed to a Tool Manager or Tool Setup
   function.  A handle may be invalid because  1) it was deliberately set to -1
   to indicate an invalid handle 2) refers to a Tool Setup that has been
   destroyed or 3) is out-of-range.

TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED
   Memory allocation failed for the native frame ring buffer used in the Media Reader
   and Media Writer.

TOOL_ERROR_NO_ACTIVE_TOOL_SETUP
   A function was called that required an active tool setup while an
   there is not an active tool setup.

TOOL_ERROR_TIMECODE_NOT_IN_DESTINATION
   The source timecode could not be found in the destination clip
   
TOOL_ERROR_TOOL_COMMAND_QUEUE_EMPTY
   The Tool Command Queue is empty.  Not really an error, more of
   a condition.

TOOL_ERROR_TOOL_IS_NOT_LICENSED
   The Plugin tool is available, but it is not licensed for this system

TOOL_ERROR_TOOL_OBJ_NOT_AVAILABLE
   The Tool Manager returned a NULL pointer on call to GetTool

TOOL_ERROR_TOOL_PROCESSOR_INSTANTIATION_FAILED
   A Tool Object failed to construct an instance of its CToolProcessor
   subtype

TOOL_ERROR_TOOL_STATUS_QUEUE_EMPTY
   The Tool Status Queue is empty  .  Not really an error, more of
   a normal condition

TOOL_ERROR_UNKNOWN_LICENSE
   The Plugin tool is available, but the license state is unknown or
   invalid.

TOOL_ERROR_FRAME_IS_MISSING
   A tool cannot process because the read failed for a frame that it needs
   
*/
//////////////////////////////////////////////////////////////////////
#endif // #ifndef TOOL_ERRORS_H

