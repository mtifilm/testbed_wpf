// ToolMediaIO.cpp: implementation of the classes:
//                      CMediaReaderTool
//                      CMediaReaderToolProc
//                      CMediaWriterTool
//                      CMediaWriterToolProc
//
/*
$Header: /usr/local/filmroot/tool/ToolMgr/ToolMediaIO.cpp,v 1.15.2.8 2009/08/06 22:57:57 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolMediaIO.h"
#include "BinManager.h"
#include "bthread.h"
#include "ImageFormat3.h"
#include "BufferPool.h"
#include "Clip3.h"
#include "Clip3VideoTrack.h"
#include "err_clip.h"
#include "err_imgtool.h"
#include "err_tool.h"
#include "Extractor.h"
#include "HRTimer.h"
#include "MTIsleep.h"
#include "ThreadLocker.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolFrameBuffer.h"
#include "ToolNode.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"

//////////////////////////////////////////////////////////////////////
// Static Variables

static MTI_UINT16 mediaReaderToolNumber = MEDIA_READER_TOOL_NUMBER;
// -------------------------------------------------------------------------
// Media Reader Keyboard and Mouse Button Configuration
// (The Media Reader Tool does not have a GUI and does not accept user input,
//  so these tables contain only a single dummy command to keep the
//  tool initialization function happy)

static CUserInputConfiguration::SConfigItem mediaReaderDefaultConfigItems[] =
{
   // Media Reader Command               Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
   // Dummy Command
   { MEDIA_READER_CMD_DUMMY,              " A            KeyDown   " },
};

static CUserInputConfiguration *mediaReaderUserInputConfiguration
 = new CUserInputConfiguration(sizeof(mediaReaderDefaultConfigItems)
                               /sizeof(CUserInputConfiguration::SConfigItem),
                               mediaReaderDefaultConfigItems);

// -------------------------------------------------------------------------
// Media Reader Command Table

static CToolCommandTable::STableEntry mediaReaderCommandTableEntries[] =
{
   // Media Reader Command              Media Reader Command String
   // ----------------------------------------------------------------
   // Dummy Command
   { MEDIA_READER_CMD_DUMMY,              "Dummy"                 },
};

static CToolCommandTable *mediaReaderCommandTable = new CToolCommandTable(
  sizeof(mediaReaderCommandTableEntries)/sizeof(CToolCommandTable::STableEntry),
                                    mediaReaderCommandTableEntries);

// ===========================================================================

static MTI_UINT16 mediaWriterToolNumber = MEDIA_WRITER_TOOL_NUMBER;

// -------------------------------------------------------------------------
// Media Writer Keyboard and Mouse Button Configuration
// (The Media Writer Tool does not have a GUI and does not accept user input,
//  so these tables contain only a single dummy command to keep the
//  tool initialization function happy)

static CUserInputConfiguration::SConfigItem mediaWriterDefaultConfigItems[] =
{
   // Media Writer Command               Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
   // Dummy Command
   { MEDIA_WRITER_CMD_DUMMY,              " A            KeyDown   " },
};

static CUserInputConfiguration *mediaWriterUserInputConfiguration
 = new CUserInputConfiguration(sizeof(mediaWriterDefaultConfigItems)
                               /sizeof(CUserInputConfiguration::SConfigItem),
                               mediaWriterDefaultConfigItems);

// -------------------------------------------------------------------------
// Media Writer Command Table

static CToolCommandTable::STableEntry mediaWriterCommandTableEntries[] =
{
   // Media Writer Command              Media Writer Command String
   // ----------------------------------------------------------------
   // Dummy Command
   { MEDIA_WRITER_CMD_DUMMY,              "Dummy"                 },
};

static CToolCommandTable *mediaWriterCommandTable = new CToolCommandTable(
  sizeof(mediaWriterCommandTableEntries)/sizeof(CToolCommandTable::STableEntry),
                                    mediaWriterCommandTableEntries);

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                               ################################
// ###     CMediaReaderTool Class      ###############################
// ####                               ################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaReaderTool::CMediaReaderTool(const string &newToolName)
: CToolObject(newToolName, NULL)
{
}

CMediaReaderTool::~CMediaReaderTool()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

CToolObject* CMediaReaderTool::MakeTool(const string& newToolName)
{
   return new CMediaReaderTool(newToolName);
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the Media Reader Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CMediaReaderTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CMediaReaderTool's base class, i.e., CToolObject
   // This must be done before the CMediaReaderTool initializes itself
   retVal = initBaseToolObject(mediaReaderToolNumber, newSystemAPI,
                               mediaReaderCommandTable,
                               mediaReaderUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "ERROR: CMediaReaderTool::toolInitialize" << endl
                     << "       CToolObject::initBaseToolObject failed" << endl
                     << "       with return = " << retVal);
      return retVal;
      }

   // Initialize the CMediaReaderTool components


   return retVal;
}   // end toolInitialize


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              Media Reader Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CMediaReaderTool::toolShutdown()
{
   int retVal;

   // Shut down Media Reader Tool

   // Shutdown the Media Reader Tool's base class, i.e., CToolObject.
   // This must be done after the Media Reader Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
} // end toolShutdown

CToolProcessor* CMediaReaderTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   CMediaReaderToolProc *toolProc = new CMediaReaderToolProc(GetToolNumber(),
                                                             GetToolName(),
                                                             getSystemAPI(),
                                                       newEmergencyStopFlagPtr);
   return toolProc;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                               ################################
// ###     CMediaWriterTool Class      ###############################
// ####                               ################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaWriterTool::CMediaWriterTool(const string &newToolName)
: CToolObject(newToolName, NULL)
{
}

CMediaWriterTool::~CMediaWriterTool()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

CToolObject* CMediaWriterTool::MakeTool(const string& newToolName)
{
   return new CMediaWriterTool(newToolName);
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the Media Writer Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CMediaWriterTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CMediaWriterTool's base class, i.e., CToolObject
   // This must be done before the CMediaWriterTool initializes itself
   retVal = initBaseToolObject(mediaWriterToolNumber, newSystemAPI,
                               mediaWriterCommandTable,
                               mediaWriterUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "ERROR: CMediaWriterTool::toolInitialize" << endl
                     << "       CToolObject::initBaseToolObject failed" << endl
                     << "       with return = " << retVal);
      return retVal;
      }

   // Initialize the CMediaWriterTool components


   return retVal;
}   // end toolInitialize


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              Media Writer Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CMediaWriterTool::toolShutdown()
{
   int retVal;

   // Shut down Media Writer Tool

   // Shutdown the Media Writer Tool's base class, i.e., CToolObject.
   // This must be done after the Media Writer Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
} // end toolShutdown

CToolProcessor* CMediaWriterTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   CMediaWriterToolProc *toolProc = new CMediaWriterToolProc(GetToolNumber(),
                                                             GetToolName(),
                                                             getSystemAPI(),
                                                       newEmergencyStopFlagPtr);
   return toolProc;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                   ############################
// ###     CMediaReaderToolProc Class      ###########################
// ####                                   ############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaReaderToolProc::CMediaReaderToolProc(int newToolNumber,
                                           const string& newToolName,
                                           CToolSystemInterface *newSystemAPI,
                                           const bool *newEmergencyStopFlagPtr)
: CToolProcessor(newToolNumber, newToolName, 0, 1, newSystemAPI,
                 newEmergencyStopFlagPtr),
  clip(nullptr), videoProxyIndex(-1), videoFramingIndex(-1), frameList(0),
  internalImageFormat(new CImageFormat),
  nativeImageFormat(0), clipLoaded(false),
  readerState(READER_STATE_WAIT_FOR_NEW_FRAME_INDEX),
  formatConversionState(CONVERT_STATE_WAIT_FOR_NEW_FRAME),
  extractor(new CExtractor),
  sequentialAccessEnabled(false), sequentialFrameIndex(NO_FRAMES_AVAILABLE),
  inFrameIndex(NO_FRAMES_AVAILABLE), outFrameIndex(NO_FRAMES_AVAILABLE),
  randomAccessEnabled(false),
  vpMediaReaderThread(0), vpConverterThread(0), exitThreadFlag(false)
{
   // Start the media  reader and converter threads
   vpConverterThread = BThreadSpawn(ConverterThread, this);
   vpMediaReaderThread = BThreadSpawn(MediaReaderThread, this);
}

CMediaReaderToolProc::~CMediaReaderToolProc()
{
   delete extractor;
   delete internalImageFormat;
   if (clip != nullptr)
      {
      CBinManager binMgr;
      binMgr.closeClip(clip);
      clip = 0;
      }
}

//////////////////////////////////////////////////////////////////////
// Set Parameters
//////////////////////////////////////////////////////////////////////

int CMediaReaderToolProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   int retVal;
   CBinManager binMgr;

   CMediaIOConfig *mediaIOConfig = static_cast<CMediaIOConfig*>(toolIOConfig);

   string clipName;
   string binPath;
   if (mediaIOConfig->useNavCurrentClip)
      {
      clipName = GetSystemAPI()->getClipFilename();
      }
   else
      {
      binPath = mediaIOConfig->binPath;
      clipName = mediaIOConfig->clipName;
      }

   ClipSharedPtr clipLocal;
   if (binPath.empty())
      {
      // The bin path is empty, so assume there is enough info in
      // the clipName alone to open the clip
      clipLocal = binMgr.openClip(clipName, &retVal);
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CMediaReaderToolProc::SetIOConfig " << endl
                        << "       could not open clip " << endl
                        << "       ClipName: " << clipName << endl
                        << "       return value = " << retVal);
         return retVal;
         }
      }
   else
      {
      // Open the clip with both bin path and clip name
      clipLocal = binMgr.openClip(binPath, clipName, &retVal);
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CMediaReaderToolProc::SetIOConfig " << endl
                        << "       could not open clip " << endl
                        << "       BinPath: " << binPath << endl
                        << "       ClipName: " << clipName << endl
                        << "       return value = " << retVal);
         return retVal;
         }
      }

   int videoProxyIndex;
   int videoFramingIndex;
   if (mediaIOConfig->useNavCurrentClip)
      {
      videoProxyIndex = GetSystemAPI()->getVideoProxyIndex();
      videoFramingIndex = GetSystemAPI()->getVideoFramingIndex();
      }
   else
      {
      videoProxyIndex = mediaIOConfig->videoProxyIndex;
      videoFramingIndex = mediaIOConfig->videoFramingIndex;
      }

   // Load the clip
   retVal = LoadClip(clipLocal, videoProxyIndex, videoFramingIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CMediaReaderToolProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CBinManager binMgr;

   // Enable the access type: Random or Sequential
   CToolFrameRange::SFrameRange &outFrameRange
                                            = toolFrameRange->outFrameRange[0];
   EnableRandomAccess(outFrameRange.randomAccess);
   EnableSequentialAccess(!outFrameRange.randomAccess);

   SetFrameIndexRange(outFrameRange.inFrameIndex,
                      outFrameRange.outFrameIndex);

   return 0;  // Not implemented yet
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaReaderToolProc::AssignAllBufferAllocators()
{
   // This function is called when the CToolSetup to which this
   // CMediaReaderToolProc is attached becomes the active tool setup

   int retVal;

   // Allocate memory for internal frame buffers
   retVal = CToolProcessor::AssignAllBufferAllocators();
   if (retVal != 0)
      return retVal;

   // Assign buffers for media read buffers
   CBufferPool *bufferAllocator = GetToolNode()->GetBufferAllocator(
                                           frameRingBuffer.GetFieldByteCount(),
                                           frameRingBuffer.GetBufferCount(),
                                           0.0);
   if (bufferAllocator == 0)
         return TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
   retVal = frameRingBuffer.AllocateFieldBuffers();
   if (retVal != 0)
      return retVal;

   return 0;
}

void CMediaReaderToolProc::ReleaseAllBufferAllocators()
{
   // This function is called when the CToolSetup to which this
   // CMediaReaderToolProc is attached is no longer the active tool setup

   // Release internal frame buffers
   CToolProcessor::ReleaseAllBufferAllocators();

   // Free memory for media read buffers
   frameRingBuffer.FreeFieldBuffers();
   GetToolNode()->ReleaseBufferAllocator(frameRingBuffer.GetFieldByteCount(),
                                         frameRingBuffer.GetBufferCount(), 0.0);
}

//////////////////////////////////////////////////////////////////////
// Load & Unload Clip
//////////////////////////////////////////////////////////////////////
// ===================================================================
//
// Function:     LoadClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CMediaReaderToolProc::LoadClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
                                   int newVideoFramingIndex)
{
   int retVal;

   // Before loading the new clip, unload everything
// TTT   UnloadClip();

// TTT - Avoid reloading clip every time
   if (clipLoaded)
      return 0;   // Got a clip already

   // Get clip data pointers
   clip = newClip;
   videoProxyIndex = newVideoProxyIndex;
   videoFramingIndex = newVideoFramingIndex;
   frameList = clip->getVideoFrameList(videoProxyIndex, videoFramingIndex);
   nativeImageFormat = frameList->getImageFormat();

   // Initialize read media buffer ring (memory isn't allocated here though)
   int maxFieldCount = frameList->getMaxTotalFieldCount();
   MTI_UINT32 maxFieldByteCount = std::max<MTI_UINT32>(
                                    frameList->getMaxPaddedFieldByteCount(),
                                    nativeImageFormat->getBytesPerField());

   // Get a buffer allocator of the correct size
   CBufferPool *bufferAllocator = GetToolNode()->GetBufferAllocator(
                                                             maxFieldByteCount,
                                                             0, 0.0);
   if (bufferAllocator == 0)
         return TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
   frameRingBuffer.InitRing(DISK_READER_NATIVE_FRAME_COUNT, maxFieldCount,
                            maxFieldByteCount, bufferAllocator);

   // Create Image Format for 16-bit 444 internal format
   // ??? Only need to create this once, already done in constructor!!!!
   //internalImageFormat = new CImageFormat;
   internalImageFormat->setToInternalFormat(*nativeImageFormat);

   // Propagate this image format to the output port so that the
   // follow tool can find out the image format of its input
   int totalFrameCount = frameList->getTotalFrameCount();
   int invisibleFieldSum = 0;
   for (int i = 0; i < totalFrameCount; ++i)
      {
      CFrame *frame = frameList->getBaseFrame(i);
      invisibleFieldSum += frame->getInvisibleFieldCount();
      }

   double invisibleFieldsPerFrame = (double)invisibleFieldSum
                                       / (double)totalFrameCount;

   SetInputMaxFrames(0, 0, 0);    // No input frame queue
   SetOutputNewFrame(0, internalImageFormat, invisibleFieldsPerFrame);
   SetOutputMaxFrames(0, 0);

   clipLoaded = true;
   return 0;
}

// ===================================================================
//
// Function:     UnloadClip
//
// Description:
//
//               N.B. This function is expected to be called from the
//                    main thread
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CMediaReaderToolProc::UnloadClip()
{
   clipLoaded = false;
   
   // Stop the media read & converter threads
   CAutotoolCommand atCmd;
   atCmd.command = AT_CMD_STOP;
   PutToolCommand(atCmd);

   // Flush buffers
   // TBD ???

   // Deallocate buffers
   frameRingBuffer.DeleteRing();

   // Get clip data pointers
   clip = 0;
   videoProxyIndex = -1;
   videoFramingIndex = -1;
   frameList = 0;
   nativeImageFormat = 0;

   return 0;
}

// ===================================================================
//
// Function:    MediaReaderThread
//
// Description:
//              Run this function within a new thread by calling
//                void *bThreadStruct = BThreadSpawn(MediaReaderThread, this)
//              from a CMediaReader member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaReaderToolProc::MediaReaderThread(void *vpMediaReader, void *vpBThread)
{
   CMediaReaderToolProc *tp = static_cast<CMediaReaderToolProc*>(vpMediaReader);
   tp->MediaReaderLoop(vpBThread);
}

// ===================================================================
//
// Function:     MediaReaderLoop
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaReaderToolProc::MediaReaderLoop(void *vpBThread)
{
   bool threadActivity;
   bool processingActive = true;
   bool running = false;
   bool endOfMaterial = false;
   int frameIndex;
   int retVal;
   CAutotoolCommand toolCommand;
   EAutotoolCommand newToolCommand;
   CAutotoolStatus toolStatus;
   EToolControlState controlState, newControlState;
   CFrameRingBuffer::SRingSlot *bufferSlot;
	EToolMediaIOReaderState nextReaderState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
	CHRTimer watchDog;
	int watchDogMinutes = 0;

   BThreadBegin(vpBThread);

   // Initial control state
   controlState = TOOL_CONTROL_STATE_STOPPED;

   while (processingActive)
      {
      threadActivity = false;

      // Get and Process Tool Commands
      if (GetToolCommand(toolCommand) == 0)
         {
         newToolCommand = toolCommand.command;
         }
      else
         newToolCommand = AT_CMD_INVALID;

      // Tool Control State Machine
      newControlState = controlState;
      switch (controlState)
         {
         case TOOL_CONTROL_STATE_STOPPED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (newToolCommand == AT_CMD_RUN && clipLoaded
                     && !IsEmergencyStop())
               {
               if (sequentialAccessEnabled)
                  InitSequentialAccess();
               endOfMaterial = false;
               readerState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
               newControlState = TOOL_CONTROL_STATE_RUNNING;
               }
            break;
         case TOOL_CONTROL_STATE_RUNNING :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (IsEmergencyStop() || endOfMaterial
                     || newToolCommand == AT_CMD_STOP)
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (newToolCommand == AT_CMD_PAUSE)
               {
               newControlState = TOOL_CONTROL_STATE_PAUSED;
               }
            break;
         case TOOL_CONTROL_STATE_PAUSED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (IsEmergencyStop() || newToolCommand == AT_CMD_STOP)
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (newToolCommand == AT_CMD_RUN)
               {
               if (sequentialAccessEnabled)
                  InitSequentialAccess();
               endOfMaterial = false;
               readerState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
               newControlState = TOOL_CONTROL_STATE_RUNNING;
               }
			break;

			default:
				break;
         };

      if (controlState != newControlState)
         {
         // The control state has changed, so perform any actions
         // that are common to the entry of the new control state
         switch (newControlState)
            {
            case TOOL_CONTROL_STATE_STOPPED :
               if (!endOfMaterial)
                  {
                  frameRingBuffer.Flush();
                  GetToolNode()->FlushDstFrameQueue(0);
                  }
               running = false;
               toolStatus.status = AT_STATUS_STOPPED;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_RUNNING :
               // Flush queues.  These should be empty, but since converter
               // thread is free-wheeling, we don't know exactly what it
               // may have done
               frameRingBuffer.Flush();
               GetToolNode()->FlushDstFrameQueue(0);
               
               running = true;
               toolStatus.status = AT_STATUS_RUNNING;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_PAUSED :
               frameRingBuffer.Flush();
               GetToolNode()->FlushDstFrameQueue(0);
               running = false;
               toolStatus.status = AT_STATUS_PAUSED;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_EXITING_THREAD :
               processingActive = false;
               running = false;
               threadActivity = true;
               exitThreadFlag = true;  // Tell converter thread to exit
               break;
			default:
				break;
           };
         }
      controlState = newControlState;

      if (running)
         {
         CToolFrameBuffer *toolFrameBuffer;

			nextReaderState = readerState;
			switch (readerState)
            {
				case READER_STATE_WAIT_FOR_NEW_FRAME_INDEX :
               // Get the next frame index we want to read. A frame index
               // of -2000 or less indicates that we don't want any frames
               frameIndex = GenerateNextFrameIndex();
               if (frameIndex == END_OF_MATERIAL_FRAME_INDEX)
                  {
                  endOfMaterial = true;
                  }
               else if (frameIndex > END_OF_MATERIAL_FRAME_INDEX)
                  {
                  // We have a new frame index, so go to the next reader state
                  threadActivity = true;

                  // Check if the buffer is sitting in the cache
                  CToolFrameCache *cache = GetSystemAPI()->GetToolFrameCache();
                  toolFrameBuffer = cache->RetrieveFrame(frameIndex);
                  if (toolFrameBuffer != NULL)
                     {
                     // We found the right converted buffer in the cache
                     threadActivity = true;
							nextReaderState = READER_STATE_OUTPUT_CACHED_BUFFER;
                     }
                  else
                     {
                     nextReaderState = READER_STATE_WAIT_FOR_BUFFERS;
                     }
                  }
               break;
               
            case READER_STATE_WAIT_FOR_BUFFERS :
               // Check if there is an available slot in the ring buffer
               bufferSlot = frameRingBuffer.GetAvailableSlot();
               if (bufferSlot != 0)
                  {
                  retVal = frameList->readMediaAllFieldsOptimized(frameIndex,
                                                    bufferSlot->fieldBuffer);
                  bufferSlot->frameIndex = frameIndex;
                  bufferSlot->errorFlag = retVal;
                  frameRingBuffer.PutAvailableSlot();

                  threadActivity = true;
                  nextReaderState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
                  }
               break;

				case READER_STATE_OUTPUT_CACHED_BUFFER :
               // Put the converted frame into the output frame queue,
               // completely bypassing the Converter thread (since cached
               // buffers are already converted!).
               // Keep returning to this state if the output frame queue
               // is full and cannot accept the frame.
            ///////////////////////////////////////////////////////////////////
            // NO - CAN'T BYPASS THE CONVERTER BECAUSE FRAMES GET OUT OF ORDER!
            ///////////////////////////////////////////////////////////////////
               //if (PutFrame(0, toolFrameBuffer))
                  //{
                  //toolFrameBuffer = 0;
                  //threadActivity = true;
                  //nextReaderState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
                  //}
            ///////////////////////////////////////////////////////////////////
               // Check if there is an available slot in the ring buffer
               bufferSlot = frameRingBuffer.GetAvailableSlot();
               if (bufferSlot != 0)
                  {
                  // Hack - I added a pointer tot he slot struct to pass
                  // the cached buffer through the converter queue to preserve
                  // the ordering of the buffers presented to the tool proc
                  bufferSlot->alreadyConvertedToolFrameBuffer = toolFrameBuffer;
                  bufferSlot->frameIndex = frameIndex;
                  bufferSlot->errorFlag = retVal;
                  frameRingBuffer.PutAvailableSlot();

                  threadActivity = true;
                  nextReaderState = READER_STATE_WAIT_FOR_NEW_FRAME_INDEX;
                  }
            ///////////////////////////////////////////////////////////////////
               break;
            }

			if (nextReaderState != readerState)
				{
				watchDog.Start();
				watchDogMinutes = 0;
				}
			else if (readerState != READER_STATE_WAIT_FOR_NEW_FRAME_INDEX)
				{
				int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
				if (elapsedMinutes > watchDogMinutes)
					{
					watchDogMinutes = elapsedMinutes;
					TRACE_0(errout << "WATCHDOG: Media reader appears to be hung in state "
										<< ((readerState == READER_STATE_WAIT_FOR_BUFFERS)
											  ? "READER_STATE_WAIT_FOR_BUFFERS"
											  : "READER_STATE_OUTPUT_CACHED_BUFFER")
										<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
					}
				}

			readerState = nextReaderState;

         }

      // If this thread isn't doing anything other than waiting for something
      // to do, then sleep for a bit so we don't hog the processor
      if (!threadActivity)
         MTImillisleep(1);
      }
} // MediaReaderLoop

// ===================================================================
//
// Function:    ConverterThread
//
// Description:
//              Run this function within a new thread by calling
//                void *bThreadStruct = BThreadSpawn(ConverterThread, this)
//              from a CMediaReader member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaReaderToolProc::ConverterThread(void *vpMediaReader, void *vpBThread)
{
   CMediaReaderToolProc *tp = static_cast<CMediaReaderToolProc*>(vpMediaReader);
   tp->NativeToInternalConverter(vpBThread);
}

void CMediaReaderToolProc::NativeToInternalConverter(void *vpBThread)
{
   bool threadActivity;
   bool processingActive = true;
   bool running = true;
   CAutotoolStatus toolStatus;
   CFrameRingBuffer::SRingSlot *bufferSlot;
   CToolFrameBuffer *toolFrameBuffer = 0;
   int retVal;

   BThreadBegin(vpBThread);

   // Initial State
   formatConversionState = CONVERT_STATE_WAIT_FOR_NEW_FRAME;
   running = true;
	CHRTimer watchDog;
	int watchDogMinutes = 0;

   while (processingActive)
      {
      threadActivity = false;

      // Get commands
      if (exitThreadFlag)
         {
         // The Media Reader Loop thread has been stopped, so this
         // thread must also stop.
         processingActive = false;
         running = false;
         threadActivity = true;
         if (toolFrameBuffer != 0)
            {
            delete toolFrameBuffer;
            toolFrameBuffer = 0;
            }
         frameRingBuffer.DeleteRing();

         GetToolNode()->FlushDstFrameQueue(0);
         }

// TBD - 1) Need to delete toolFrameBuffer when the Media Reader is stopped
//       2) Need do something with the buffer slot when the Media Reader is stopped

      if (running && !IsEmergencyStop())
         {
         EToolMediaIOConversionState nextState = formatConversionState;
         switch (formatConversionState)
            {
            case CONVERT_STATE_WAIT_FOR_NEW_FRAME :
               // Get the next available native frame buffer from the Media Reader.
               // Keep returning to this state until a native frame is available
               // Actually because of a hack related to caching, the frame buffer
               // may already be converted so all we need to do is pass it through
               // to the output port!
               bufferSlot = frameRingBuffer.GetFrameBuffer();
               if (bufferSlot != 0 &&
                   bufferSlot->alreadyConvertedToolFrameBuffer != 0)
                   {
                   // We have a cached converted buffer, just pass it through
                   toolFrameBuffer = bufferSlot->alreadyConvertedToolFrameBuffer;
                   bufferSlot->alreadyConvertedToolFrameBuffer = 0;
                   frameRingBuffer.ReleaseFrameBuffer();

                   threadActivity = true;
                   nextState = CONVERT_STATE_PUT_OUTPUT;
                   }
               else if (bufferSlot != 0)
                  {
                  // Create a new CToolFrameBuffer to receive converted frame
                  // and fields
                  toolFrameBuffer = new CToolFrameBuffer(internalImageFormat,
                                                   GetOutputBufferAllocator(0));

                  if (bufferSlot->errorFlag == CLIP_ERROR_INVALID_FRAME_NUMBER
                     || bufferSlot->errorFlag == CLIP_ERROR_IMAGE_FILE_MISSING)
                     {
                     // Dummy-up a tool frame buffer based on first frame
                     // of clip.
                     retVal = toolFrameBuffer->InitFromClip(*frameList, 0,
                                                            false);
                     toolFrameBuffer->SetFrameIndex(bufferSlot->frameIndex);
                     toolFrameBuffer->SetErrorFlag(bufferSlot->errorFlag);
                     nextState = CONVERT_STATE_WAIT_FOR_BUFFERS;
                     }
                  else
                     {
                     retVal = toolFrameBuffer->InitFromClip(*frameList,
                                                         bufferSlot->frameIndex,
                                                            true);
                     if (bufferSlot->errorFlag != 0)
                        {
                        toolFrameBuffer->SetErrorFlag(bufferSlot->errorFlag);
                        nextState = CONVERT_STATE_CONVERT;
                        }
                     else if (retVal != 0)
                        {
                        toolFrameBuffer->SetErrorFlag(retVal);
                        nextState = CONVERT_STATE_CONVERT;
                        }
                     else
                        {
                        nextState = CONVERT_STATE_WAIT_FOR_BUFFERS;
                        }
                     }

                  threadActivity = true;// Move on to the next state immediately
                  }
               break;

            case CONVERT_STATE_WAIT_FOR_BUFFERS :
               {
               // Allocate 16-bit frame buffers for the CToolFrameBuffer frame
               // and fields.  Keep returning to this state until all of
               // the frame/field buffers are allocated
               int retVal = toolFrameBuffer->AllocateAllFrameBuffers();
               if (retVal == 0)
                  {
                  threadActivity = true;
                  nextState = CONVERT_STATE_CONVERT;
                  }
               else if (retVal == IMGTOOL_ERROR_NO_AVAILABLE_BUFFERS)
                  {
                  // Let's free up a cached tool frame just in case we're
                  // really running out of buffers
                  CToolFrameBuffer *tempFrameBuffer =
                        GetSystemAPI()->GetToolFrameCache()->GetOldestFrame();
                  if (tempFrameBuffer != NULL)
                     {
                     delete tempFrameBuffer;
                     threadActivity = true;
                     }
                  }
               else
                  {
                  toolFrameBuffer->SetErrorFlag(retVal);
                  threadActivity = true;
                  }
               }
               break;

            case CONVERT_STATE_CONVERT :
               if (toolFrameBuffer->GetErrorFlag()
                                            == CLIP_ERROR_INVALID_FRAME_NUMBER
                     || bufferSlot->errorFlag == CLIP_ERROR_IMAGE_FILE_MISSING)
                  {
                  toolFrameBuffer->SetDummyImage();
                  }
               else
                  {
                  retVal = toolFrameBuffer->ConvertFromNativeFormat(extractor,
                                                             nativeImageFormat,
                                                      bufferSlot->fieldBuffer);
                  }

               frameRingBuffer.ReleaseFrameBuffer();

               threadActivity = true;
               nextState = CONVERT_STATE_PUT_OUTPUT;
               break;

            case CONVERT_STATE_PUT_OUTPUT :

               // Put the converted frame into the output frame queue.
               // Keep returning to this state if the output frame queue
               // is full and cannot accept the frame.
               if (PutFrame(0, toolFrameBuffer))
                  {
//TRACE_3(errout << "Convert: PutFrame: Frame Index "
//               << toolFrameBuffer->GetFrameIndex() << " "
//               << toolFrameBuffer->GetErrorFlag());
                  toolFrameBuffer = 0;
                  threadActivity = true; // Move on to the next state immediately
						nextState = CONVERT_STATE_WAIT_FOR_NEW_FRAME;
                  }
               break;
            }

			if (nextState != formatConversionState)
				{
				watchDog.Start();
				watchDogMinutes = 0;
				}
			else if (formatConversionState != CONVERT_STATE_WAIT_FOR_NEW_FRAME)
				{
				int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
				if (elapsedMinutes > watchDogMinutes)
					{
					watchDogMinutes = elapsedMinutes;
					TRACE_0(errout << "WATCHDOG: NativeToInternalConverter appears to be hung in state "
										<< ((formatConversionState == CONVERT_STATE_WAIT_FOR_BUFFERS)
											  ? "CONVERT_STATE_WAIT_FOR_BUFFERS"
											  : ((formatConversionState == CONVERT_STATE_CONVERT)
													? "CONVERT_STATE_CONVERT"
													: "CONVERT_STATE_PUT_OUTPUT"))
										<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
					}
				}

			formatConversionState = nextState;
         }

      // If this thread isn't doing anything other than waiting for something
      // to do, then sleep for a bit so we don't hog the processor
      if (!threadActivity)
         MTImillisleep(1);
      }


   // Post thread-ending status
   toolStatus.status = AT_STATUS_PROCESS_THREAD_ENDING;
   PutToolStatus(toolStatus);
} // NativeToInternalConverter

//////////////////////////////////////////////////////////////////////
// Frame Request List functions
//////////////////////////////////////////////////////////////////////

void CMediaReaderToolProc::RequestFrame(int port, int frameIndex)
{
   if (port != 0)
      return;        // Only one output port
      
   if (randomAccessEnabled)
      AddFrameRequest(frameIndex);
}

void CMediaReaderToolProc::AddFrameRequest(int frameIndex)
{
   CAutoThreadLocker atl(frameRequestListThreadLock);// Lock thread until return

   for (FrameRequestListIterator iterator = frameRequestList.begin();
        iterator != frameRequestList.end();
        ++iterator)
      {
      if (iterator->frameIndex == frameIndex)
         {
         // The caller's frame has already been added to the request
         // list, so do not added it again
         return;
         }
      }

   // If we have gotten this far, then the caller's frame index is
   // not yet in the request list.

   //TRACE_0(errout << "CMediaReaderToolProc::AddFrameRequest: " << " frame " << frameIndex << " (new)");

   // Add new frame request to list
   SFrameRequest frameRequest;
   frameRequest.frameIndex = frameIndex;
   frameRequest.unprocessedRequest = true;
   frameRequestList.push_back(frameRequest);   // add to end of list
}

int CMediaReaderToolProc::GetNextRequestedFrameToRead()
{
   CAutoThreadLocker atl(frameRequestListThreadLock);// Lock thread until return

   if (frameRequestList.empty())
      return NO_FRAMES_AVAILABLE;// List is empty, return impossible frame index

   // Iterate over list, from oldest to newest request, searching for
   // a request that has not yet been processed
   for (FrameRequestListIterator iterator = frameRequestList.begin();
        iterator != frameRequestList.end();
        ++iterator)
      {
      if (iterator->unprocessedRequest)
         {
         // Mark this frame as processed
         iterator->unprocessedRequest = false;

         int frameIndex = iterator->frameIndex;
         if (frameIndex == END_OF_MATERIAL_FRAME_INDEX)
            {
            // Remove this request immediately as it uses a trick
            // frame index to indicate that the destination tool
            // is done.
            frameRequestList.erase(iterator);
            }

         // Return the requested frame index
         return frameIndex;
         }
      }

   // If we have gotten this far, then there are no unprocessed frames in
   // the request list
   return NO_FRAMES_AVAILABLE;  // Return impossible frame index
}

void CMediaReaderToolProc::RemoveRequest(int port, int frameIndex)
{
   if (port != 0)
      return;        // Only one output port

   CAutoThreadLocker atl(frameRequestListThreadLock);// Lock thread until return

   //TRACE_0(errout << "CMediaReaderToolProc::RemoveRequest " << frameIndex);

   if (!randomAccessEnabled || frameRequestList.empty())
      return;

   for (FrameRequestListIterator iterator = frameRequestList.begin();
        iterator != frameRequestList.end();
        ++iterator)
      {
      if (iterator->frameIndex == frameIndex)
         {
         //TRACE_0(errout << "CMediaReaderToolProc::RemoveRequest " << frameIndex << " (found)");
         frameRequestList.erase(iterator);
         return;
         }
      }
   //TRACE_0(errout << "CMediaReaderToolProc::RemoveRequest " << frameIndex << " (missing)");
}

void CMediaReaderToolProc::ClearRequestList()
{
   CAutoThreadLocker atl(frameRequestListThreadLock);// Lock thread until return

   frameRequestList.clear();
}

int CMediaReaderToolProc::GenerateNextFrameIndex()
{
   if (sequentialAccessEnabled)
      {
      if (sequentialFrameIndex <= END_OF_MATERIAL_FRAME_INDEX)
         return END_OF_MATERIAL_FRAME_INDEX;
      else if (sequentialFrameIndex < outFrameIndex)
         return sequentialFrameIndex++;
      else
         return END_OF_MATERIAL_FRAME_INDEX;
      }
   else if (randomAccessEnabled)
      {
      return GetNextRequestedFrameToRead();
      }
   else
      return END_OF_MATERIAL_FRAME_INDEX;
}

void CMediaReaderToolProc::InitSequentialAccess()
{
   if (sequentialAccessEnabled)
      {
      if (inFrameIndex <= END_OF_MATERIAL_FRAME_INDEX
          || outFrameIndex <= END_OF_MATERIAL_FRAME_INDEX)
         sequentialFrameIndex = END_OF_MATERIAL_FRAME_INDEX;
      else
         sequentialFrameIndex = inFrameIndex;
      }
   else
      sequentialFrameIndex = END_OF_MATERIAL_FRAME_INDEX;
}

void CMediaReaderToolProc::EnableRandomAccess(bool enable)
{
   if (enable != randomAccessEnabled)
      {
      randomAccessEnabled = enable;

      if (enable)
         {
         // Enabling random access

         // First disable sequential access
         EnableSequentialAccess(false);

         // Need to make sure the MediaReader and Converter threads
         // are running.  If we can guarantee that this function will
         //  only be called from the main thread, then we can do a
         // PutToolCommand.  
         CAutotoolCommand atCmd;
         atCmd.command = AT_CMD_RUN;
         PutToolCommand(atCmd);
         }
      else
         {
         // Disabling random access
         }
      }
}

void CMediaReaderToolProc::EnableSequentialAccess(bool enable)
{
   if (enable != sequentialAccessEnabled)
      {
      sequentialAccessEnabled = enable;

      if (enable)
         {
         // Enabling sequential access

         // First disable random access
         EnableRandomAccess(false);
         }
      else
         {
         // Disabling sequential access
         }
      }
}

void CMediaReaderToolProc::SetFrameIndexRange(int newInFrame, int newOutFrame)
{
   inFrameIndex = newInFrame;
   outFrameIndex = newOutFrame;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                   ############################
// ###     CMediaWriterToolProc Class      ###########################
// ####                                   ############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaWriterToolProc::CMediaWriterToolProc(int newToolNumber,
                                           const string& newToolName,
                                           CToolSystemInterface *newSystemAPI,
                                           const bool *newEmergencyStopFlagPtr)
: CToolProcessor(newToolNumber, newToolName, 1, 0, newSystemAPI,
                 newEmergencyStopFlagPtr),
//  clip(0), videoProxyIndex(-1), videoFramingIndex(-1), frameList(0),
//  nativeImageFormat(0), clipLoaded(false),
  dstClipLoaded(false), dstClip(nullptr), dstVideoProxyIndex(-1), dstFrameList(0),
  dstImageFormat(0), dstFrameOffset(0),
  formatConversionState(CONVERT_STATE_WAIT_FOR_NEW_FRAME),
  extractor(new CExtractor),
  vpMediaWriterThread(0), vpConverterThread(0), exitThreadFlag(false),
  syncWriteExtractor(new CExtractor)
  //** Dave's DPX Header Copy Hack
  , enableDPXHeaderCopyHack(false), srcClip(nullptr), srcFrameList(0)
  //**//
{
   // Start the media writer and converter threads
   vpConverterThread = BThreadSpawn(ConverterThread, this);
   vpMediaWriterThread = BThreadSpawn(MediaWriterThread, this);
}

CMediaWriterToolProc::~CMediaWriterToolProc()
{
   delete syncWriteExtractor;
   delete extractor;
   if (srcClip!= nullptr)
      {
      CBinManager binMgr;
      binMgr.closeClip(srcClip);
      srcClip= 0;
      }
   if (dstClip != nullptr)
      {
      CBinManager binMgr;
      binMgr.closeClip(dstClip);
      dstClip = 0;
      }
}

//////////////////////////////////////////////////////////////////////
// Set I/O Configuration
//////////////////////////////////////////////////////////////////////

int CMediaWriterToolProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   int retVal;
   CAutoErrorReporter autoErr("CMediaWriterToolProc::SetIOConfig",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   // The destination clip will be loaded the first time we want to
   // write media.
   dstClipLoaded = false;
   dstClip = 0;
   dstVideoProxyIndex = -1;
   dstFrameList = 0;
   dstImageFormat = 0;
   dstFrameOffset = 0;
   //** Dave's DPX Header Copy Hack
   srcClip= 0;
   srcFrameList = 0;
   enableDPXHeaderCopyHack = false;
   //**//

   CRenderDestinationClip *renderDest = GetSystemAPI()->getRenderDestinationClip();

   CBinManager binMgr;
   srcClip= binMgr.openClip(renderDest->GetSourceClipName(), &retVal);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Could not open source clip " << renderDest->GetSourceClipName()
                  << ", Error = " << retVal;
      return retVal;
      }

   // Load the source clip
   retVal = LoadSrcClip(renderDest->srcVideoProxyIndex,
                        renderDest->srcVideoFramingIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CMediaWriterToolProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaWriterToolProc::AssignAllBufferAllocators()
{
   int retVal;

   // Allocate memory for internal frame buffers
   retVal = CToolProcessor::AssignAllBufferAllocators();
   if (retVal != 0)
      return retVal;

   // Allocate memory for media write buffers
   CBufferPool *bufferAllocator = GetToolNode()->GetBufferAllocator(
                                           frameRingBuffer.GetFieldByteCount(),
                                           frameRingBuffer.GetBufferCount(),
                                           0.0);
   if (bufferAllocator == 0)
         return TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
   retVal = frameRingBuffer.AllocateFieldBuffers();
   if (retVal != 0)
      return retVal;

   return 0;
}

void CMediaWriterToolProc::ReleaseAllBufferAllocators()
{
   // Release internal frame buffers
   CToolProcessor::ReleaseAllBufferAllocators();

   // Free memory for media write buffers
   frameRingBuffer.FreeFieldBuffers();
   GetToolNode()->ReleaseBufferAllocator(frameRingBuffer.GetFieldByteCount(),
                                         frameRingBuffer.GetBufferCount(), 0.0);
}

//////////////////////////////////////////////////////////////////////
// Load Source and Destination Clips
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:     LoadSrcClip
//
// Description:  Initialize buffer allocation for media writer.  Since
//               the destination clip is either the source clip or
//               has the same image format as the source clip, we
//               will use the source clip as the basis of the memory allocation.
//               Note: the assumption that the source and destination
//                     clips have the same format is true for the current
//                     tools (DRS, autofilter, etc.), but would not be true
//                     if format conversion was made a Navigator-style plugin.
//
// Arguments:
//
// Returns:      0 for success, non-zero is an error code
//
// ===================================================================
int CMediaWriterToolProc::LoadSrcClip(int srcVideoProxyIndex, int srcVideoFramingIndex)
{
   int retVal;
   CAutoErrorReporter autoErr("CMediaWriterToolProc::LoadSrcClip",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   // Get clip data pointers
   srcFrameList = srcClip->getVideoFrameList(srcVideoProxyIndex,
                                             srcVideoFramingIndex);

   // Initialize write media buffer ring
   int maxFieldCount = srcFrameList->getMaxTotalFieldCount();
   MTI_UINT32 maxFieldByteCount = srcFrameList->getMaxPaddedFieldByteCount();
   CBufferPool *bufferAllocator = GetToolNode()->GetBufferAllocator(
                                                              maxFieldByteCount,
                                                              0, 0.0);
   if (bufferAllocator == 0)
      {
      autoErr.errorCode = TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
      autoErr.msg << "Could not allocate memory to write media";
      return TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
      }
   frameRingBuffer.InitRing(DISK_READER_NATIVE_FRAME_COUNT, maxFieldCount,
                            maxFieldByteCount, bufferAllocator);

   SetInputMaxFrames(0, 0, 0);
   SetOutputNewFrame(0, 0, 0.0);
   SetOutputMaxFrames(0, 0);

   return 0;
}

int CMediaWriterToolProc::LoadDstClip(int srcFrameIndex)
{
   int retVal;
   CAutoErrorReporter autoErr("CMediaWriterToolProc::LoadDstClip",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   CRenderDestinationClip *renderDest = GetSystemAPI()->getRenderDestinationClip();
   CBinManager binMgr;

   //** Dave's DPX Header Copy Hack
   enableDPXHeaderCopyHack = false; // Presume no hacking
   //**//

   if (renderDest->destType == DEST_CLIP_TYPE_NEW)
      {
      //** Dave's DPX Header Copy Hack
      enableDPXHeaderCopyHack = true; // Presume needs hack
      /**/

      retVal = CreateDstClip(renderDest);
      if (retVal != 0)
         return retVal;

#ifdef OLDWAY
      // Figure out the destination clip's in and out timecodes
      ClipSharedPtr &srcClip= binMgr.openClip(renderDest->GetSourceClipName(), &retVal);
      if (renderDest->dstInOutType == DEST_IN_OUT_TYPE_MARKS)
         {
         // TTT - this does not work with clips with 3:2 pulldown
         CVideoFrameList *srcFrameList
                = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
                                             renderDest->srcVideoFramingIndex);
         int markIn = GetSystemAPI()->getMarkIn();
         renderDest->dstClipInTC = srcFrameList->getTimecodeForFrameIndex(markIn);
         int markOut = GetSystemAPI()->getMarkOut();
         renderDest->dstClipOutTC = srcFrameList->getTimecodeForFrameIndex(markOut);
         }
      else if (renderDest->dstInOutType == DEST_IN_OUT_TYPE_ENTIRE_SRC)
         {
         renderDest->dstClipInTC = srcClip->getInTimecode();
         renderDest->dstClipOutTC = srcClip->getOutTimecode();
         }

      // Save framing type to .clp file
      string sourceClipName = renderDest->GetSourceClipName();
      dstClip->setRenderInfo(sourceClipName, srcFrameIndex,
                             renderDest->srcVideoFramingIndex);

      // Create a new clip as the render destination
      retVal = renderDest->CreateClip();
      if (retVal != 0)
         return retVal;

      // The clip now exists, so update the render destination type
      renderDest->destType = DEST_CLIP_TYPE_EXISTING;
#endif //  #ifdef OLDWAY
      }

   dstClip = binMgr.openClip(renderDest->GetDestinationClipName(), &retVal);
   if (retVal != 0)
      {
// TBD - Add more informative error message
      autoErr.errorCode = retVal;
      autoErr.msg << "Could not open destination clip "
                  << renderDest->GetDestinationClipName()
                  << ", Error = " << retVal;
      return retVal;
      }

   dstVideoProxyIndex = renderDest->srcVideoProxyIndex;

   // Get clip data pointers
   dstFrameList = dstClip->getVideoFrameList(dstVideoProxyIndex,
                                             renderDest->srcVideoFramingIndex);
   dstImageFormat = dstFrameList->getImageFormat();

   // Calculate frame index offset between source and destination clips
   if (renderDest->destType == DEST_CLIP_TYPE_SRC)
      {
      // Destination clip is the source clip, so zero offset
      dstFrameOffset = 0;
      }
   else
      {
      //** Dave's DPX Header Hack -> srcClipand srcFramelist are member vars
      // I think we're guaranteed that the srcCliphas been opened and
      // LoadSrcClip() has been run by now...
      // ClipSharedPtr &srcClip= binMgr.openClip(renderDest->GetSourceClipName(), &retVal);
      // CVideoFrameList *srcFrameList
      //        = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
      //                                     renderDest->srcVideoFramingIndex);
      MTIassert(srcClip!= nullptr);
      MTIassert(srcFrameList != nullptr);
      //**//
      CTimecode srcTimecode = srcFrameList->getTimecodeForFrameIndex(srcFrameIndex);

      int dstFrameIndex = dstFrameList->getFrameIndex(srcTimecode);
      if (dstFrameIndex < 0)
         {
         string srcTimecodeStr;
         srcTimecode.getTimeString(srcTimecodeStr);
         autoErr.errorCode = TOOL_ERROR_TIMECODE_NOT_IN_DESTINATION;
         autoErr.msg << "Destination clip does not contain timecode "
                     << srcTimecodeStr;
         return TOOL_ERROR_TIMECODE_NOT_IN_DESTINATION;
         }

      dstFrameOffset = dstFrameIndex - srcFrameIndex;
      }

   dstClipLoaded = true;

   //** Dave's DPX Header Copy Hack
   if (srcFrameList == 0 || srcFrameList->getMaxTotalFieldCount() != 1 ||
       dstFrameList == 0 || dstFrameList->getMaxTotalFieldCount() != 1)
      enableDPXHeaderCopyHack = false; // Can't hack
   /**/

   return 0;
}

// ----------------------------------------------------------------------------

int CMediaWriterToolProc::CreateDstClip(CRenderDestinationClip *renderDest)
{
   int retVal;
   CAutoErrorReporter autoErr("CMediaWriterToolProc::CreateDstClip",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (renderDest->destType != DEST_CLIP_TYPE_NEW)
      return -1;

   // Figure out the destination clip's in and out timecodes
   // If the source clip has pulldown and will be rendered in
   // film-frames mode, then the in and out timecodes must be adjusted
   // to account for the relationship between film-frames and video-frames.

   //** Dave's DPX Header Hack -> srcClipand srcFramelist are member vars
   // I think we're guaranteed that the srcCliphas been opened by now...
   // CBinManager binMgr;
   // ClipSharedPtr srcClip= binMgr.openClip(renderDest->GetSourceClipName(), &retVal);
   //        = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
   //                                     renderDest->srcVideoFramingIndex);
   MTIassert(srcClip!= nullptr);
   //**//
   if (renderDest->dstInOutType == DEST_IN_OUT_TYPE_MARKS)
      {

      CVideoFrameList *srcVideoFramesFrameList
             = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
                                          FRAMING_SELECT_VIDEO);
      if (srcVideoFramesFrameList == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      CVideoFrameList *srcFilmFramesFrameList
             = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
                                          FRAMING_SELECT_FILM);
      if (srcFilmFramesFrameList == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      int markIn = GetSystemAPI()->getMarkIn();
      int markOut = GetSystemAPI()->getMarkOut();

      if (renderDest->srcVideoFramingIndex == FRAMING_SELECT_FILM
          && srcVideoFramesFrameList != srcFilmFramesFrameList)
         {
         // There are distinct video-frames and film-frames frame lists,
         // so we assume that we need to compensate for pull-down
         CVideoProxy *videoProxy
                      = srcClip->getVideoProxy(renderDest->srcVideoProxyIndex);
         if (videoProxy == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         // Map the mark in and mark out points from the film-frames frame
         // list to the video-frames frame list
         int adjustedMarkIn = videoProxy->mapFrameIndex(markIn,
                                                        FRAMING_SELECT_FILM,
                                                        FRAMING_SELECT_VIDEO,
                                                        true);
         int adjustedMarkOut = videoProxy->mapFrameIndex(markOut - 1,
                                                         FRAMING_SELECT_FILM,
                                                         FRAMING_SELECT_VIDEO,
                                                         false) + 1;

         // Get the timecodes from the video-frames frame list
         renderDest->dstClipInTC
           = srcVideoFramesFrameList->getTimecodeForFrameIndex(adjustedMarkIn);
         renderDest->dstClipOutTC
          =  srcVideoFramesFrameList->getTimecodeForFrameIndex(adjustedMarkOut);
         }
      else
         {
         // We do not need to worry about pull-down, so just use the
         // timecodes at the mark in and mark out points in the frame
         // list to be rendered
         CVideoFrameList *srcFrameList
             = srcClip->getVideoFrameList(renderDest->srcVideoProxyIndex,
                                          renderDest->srcVideoFramingIndex);
         if (srcFrameList == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         renderDest->dstClipInTC = srcFrameList->getTimecodeForFrameIndex(markIn);
         renderDest->dstClipOutTC = srcFrameList->getTimecodeForFrameIndex(markOut);
         }
      }
   else if (renderDest->dstInOutType == DEST_IN_OUT_TYPE_ENTIRE_SRC)
      {
      renderDest->dstClipInTC = srcClip->getInTimecode();
      renderDest->dstClipOutTC = srcClip->getOutTimecode();
      }

   // Create a new clip as the render destination
   retVal = renderDest->CreateClip();
   if (retVal != 0)
      return retVal;

   // The clip now exists, so update the render destination type
   renderDest->destType = DEST_CLIP_TYPE_EXISTING;

   return 0;
}

// ===================================================================
//
// Function:    MediaWriterThread
//
// Description:
//              Run this function within a new thread by calling
//                void *bThreadStruct = BThreadSpawn(MediaWriterThread, this)
//              from a CMediaReader member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaWriterToolProc::MediaWriterThread(void *vpMediaWriter, void *vpBThread)
{
   CMediaWriterToolProc *tp = static_cast<CMediaWriterToolProc*>(vpMediaWriter);
   tp->MediaWriterLoop(vpBThread);
}

// ===================================================================
//
// Function:     MediaWriterLoop
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaWriterToolProc::MediaWriterLoop(void *vpBThread)
{
   bool threadActivity;
   bool processingActive = true;
   bool running = false;
   bool endOfMaterial = false;
   int processingError;
   int retVal;
   int oldErrorRetVal = 0;
   CAutotoolCommand toolCommand;
   EAutotoolCommand newToolCommand;
   CAutotoolStatus toolStatus;
   EToolControlState controlState, newControlState;
	CFrameRingBuffer::SRingSlot *bufferSlot;

   BThreadBegin(vpBThread);

   // Initial control state
   controlState = TOOL_CONTROL_STATE_STOPPED;

   while (processingActive)
      {
      threadActivity = false;

      // Get and Process Tool Commands
      if (GetToolCommand(toolCommand) == 0)
         {
         newToolCommand = toolCommand.command;
         }
      else
         newToolCommand = AT_CMD_INVALID;

      // Tool Control State Machine
      newControlState = controlState;
      switch (controlState)
         {
         case TOOL_CONTROL_STATE_STOPPED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (newToolCommand == AT_CMD_RUN
                     && !IsEmergencyStop())
               {
               newControlState = TOOL_CONTROL_STATE_RUNNING;
               }
            break;
         case TOOL_CONTROL_STATE_RUNNING :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (IsEmergencyStop())
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (processingError != 0)
               {
               // An error was returned by one of the tool's functions
               TRACE_0(errout << "ERROR: CMediaWriterToolProc::MediaWriterLoop Error "
                              << processingError);
               toolStatus.status = AT_STATUS_PROCESS_ERROR;
               toolStatus.errorCode = processingError;
               PutToolStatus(toolStatus);
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (endOfMaterial)
               {
               toolStatus.status = AT_STATUS_END_OF_MATERIAL;
               PutToolStatus(toolStatus);
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               TRACE_3(errout << " <<<<<<< in MediaWriter: EOM -> STOPPED >>>>>>>");
               }
#if 1 //def STOP_CMD_STOPS_OUTPUT_PATH     DON'T MAKE THIS 0!!
            else if (newToolCommand == AT_CMD_STOP)
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
#endif
            else if (newToolCommand == AT_CMD_PAUSE)
               {
               newControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
               }
            break;
         case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (IsEmergencyStop())
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
#if 1 //def STOP_CMD_STOPS_OUTPUT_PATH    DON'T MAKE THIS 0!!
            else if (newToolCommand == AT_CMD_STOP)
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
#endif
            else if (endOfMaterial)
               {
               toolStatus.status = AT_STATUS_END_OF_MATERIAL;
               PutToolStatus(toolStatus);
               newControlState = TOOL_CONTROL_STATE_PAUSED;
               }
         case TOOL_CONTROL_STATE_PAUSED :
            if (newToolCommand == AT_CMD_EXIT_THREAD)
               {
               newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
               }
            else if (newToolCommand == AT_CMD_STOP || IsEmergencyStop())
               {
               newControlState = TOOL_CONTROL_STATE_STOPPED;
               }
            else if (newToolCommand == AT_CMD_RUN /*&& clipLoaded*/)
               {
               newControlState = TOOL_CONTROL_STATE_RUNNING;
               }
            break;
			default:
				break;
		 };

      if (controlState != newControlState)
         {
         // The control state has changed, so perform any actions
         // that are common to the entry of the new control state
         // For clip2clip: update video frame lists on transitions to
         // "not running"
         switch (newControlState)
            {
            case TOOL_CONTROL_STATE_STOPPED :
               frameRingBuffer.Flush();
               running = false;
               toolStatus.status = AT_STATUS_STOPPED;
               PutToolStatus(toolStatus);
               if (dstClipLoaded)
                  dstClip->updateAllVideoFrameListFiles();
               break;
            case TOOL_CONTROL_STATE_RUNNING :
               running = true;
               endOfMaterial = false;
               processingError = 0;
               toolStatus.status = AT_STATUS_RUNNING;
               PutToolStatus(toolStatus);
               break;
            case TOOL_CONTROL_STATE_PAUSED :
               running = false;
               toolStatus.status = AT_STATUS_PAUSED;
               PutToolStatus(toolStatus);
               if (dstClipLoaded)
                  dstClip->updateAllVideoFrameListFiles();
               break;
            case TOOL_CONTROL_STATE_EXITING_THREAD :
               exitThreadFlag = true;
               processingActive = false;
               running = false;
               threadActivity = true;
               if (dstClipLoaded)
                  dstClip->updateAllVideoFrameListFiles();
               break;
			default:
				break;
			};
         }
      controlState = newControlState;

      if (running)
         {
         // Check if there is an available frame in the ring buffer
         bufferSlot = frameRingBuffer.GetFrameBuffer();
         if (bufferSlot != 0)
            {
            if (bufferSlot->endOfMaterial)
               {
               // At the end of the material, so clean up
               endOfMaterial = true;

               // update the field flags to show media written
               if (dstClipLoaded)
                  {
                  retVal = dstClip->updateAllTrackFiles();
                  if (retVal != 0)
                     {
                     CAutoErrorReporter autoErr ("CMediaWriterToolProc::MediaWriterLoop",
                                                 AUTO_ERROR_NOTIFY_WAIT,
                                                 AUTO_ERROR_TRACE_0);
                     autoErr.errorCode = retVal;
                     autoErr.msg << "At end of material, could not update track files";
                     }
                  }
               }
            else if (bufferSlot->errorFlag == 0)
               {
               CRenderDestinationClip *renderDestinationClip =
                                   GetSystemAPI()->getRenderDestinationClip();

               // Got another good native buffer, so write it to media storage
               if (renderDestinationClip->destType == DEST_CLIP_TYPE_SRC)
                  {
                  // Write all of the fields, both visible and invisible
                  retVal = dstFrameList->writeMediaAllFieldsOptimized(
                                                       bufferSlot->frameIndex,
                                                       bufferSlot->fieldBuffer);
                  if (retVal == 0)
                     {
                     // History cleanup hack
                     GetSystemAPI()->ValidateHistory(bufferSlot->frameIndex);

                     // History multiframe GOV region revision hack
                     GetSystemAPI()->ReviseGOVRegions(bufferSlot->frameIndex);
                     }
                  }
               else
                  {
                  //** Dave's DPX Header Copy Hack
                  if (enableDPXHeaderCopyHack)
                     {
                     string sourceImageFilePath;
                     CFrame *srcFrame = srcFrameList->getBaseFrame(
                                                         bufferSlot->frameIndex
                                                         - dstFrameOffset);
                     if (srcFrame != NULL)
                        {
                        CField *srcField = srcFrame->getBaseField(0);
                        CMediaLocation srcMediaLocation
                                                = srcField->getMediaLocation();
                        sourceImageFilePath
                                         = srcMediaLocation.getImageFilePath();
                        }
                        
                     retVal = dstFrameList->writeMediaWithDPXHeaderCopyHack(
                                                       bufferSlot->frameIndex,
                                                       bufferSlot->fieldBuffer,
                                                       sourceImageFilePath);
                     if (retVal == 0)
                        {
                        // History cleanup hack
                        GetSystemAPI()->ValidateHistory(bufferSlot->frameIndex);

                        // History multiframe GOV region revision hack
                        GetSystemAPI()->ReviseGOVRegions(bufferSlot->frameIndex);
                        }
                     }
                  //**//
                  else
                     {
                     // Write as "film frames,"  i.e., write the fields
                     // using the data in the visible fields only, but writing
                     // both visible and invisible fields on disk.  This
                     // results in perfect cadence, but overwrites the invisible
                     // fields on disk.
                     retVal = dstFrameList->writeMediaFilmFramesOptimized(
                                                          bufferSlot->frameIndex,
                                                          bufferSlot->fieldBuffer);
                     if (retVal == 0)
                        {
                        // History cleanup hack
                        GetSystemAPI()->ValidateHistory(bufferSlot->frameIndex);

                        // History multiframe GOV region revision hack
                        GetSystemAPI()->ReviseGOVRegions(bufferSlot->frameIndex);
                        }
                     }
                  }
               if (retVal != 0)
                  {
                  if (retVal != oldErrorRetVal)
                     {
                     oldErrorRetVal = retVal;
                     CAutoErrorReporter autoErr ("CMediaWriterToolProc::MediaWriterLoop",
                                                 AUTO_ERROR_NOTIFY_NO_WAIT,
                                                 AUTO_ERROR_TRACE_0);
                     processingError = retVal;
                     autoErr.errorCode = retVal;
                     autoErr.msg << "Could not write video media";
                     if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
                        autoErr.msg << endl << "Cannot modify BOTH the "
                                               "VIDEO and the FILM frames";
                     }
                  }

               // Update the "last media action" flag so user can see that
               // something has been done to the clip
               retVal = dstClip->updateMediaModifiedStatus(TRACK_TYPE_VIDEO,
                                                           dstVideoProxyIndex);

               if (retVal != 0)
                  {
                  if (retVal != oldErrorRetVal)
                     {
                     oldErrorRetVal = retVal;
                     CAutoErrorReporter autoErr ("CMediaWriterToolProc::MediaWriterLoop",
                                                 AUTO_ERROR_NOTIFY_NO_WAIT,
                                                 AUTO_ERROR_TRACE_0);
                     processingError = retVal;
                     autoErr.errorCode = retVal;
                     autoErr.msg << "Could not update media modified status";
                     }
                  }
               }
            else
               {
               // Got a native buffer, but it has got an error
               processingError = bufferSlot->errorFlag;
               }

            frameRingBuffer.ReleaseFrameBuffer();

            threadActivity = true;
            }
         }
         
      // If this thread isn't doing anything other than waiting for something
      // to do, then sleep for a bit so we don't hog the processor
      if (!threadActivity)
         MTImillisleep(1);
      }
} // MediaWriterLoop

// ===================================================================
//
// Function:    ConverterThread
//
// Description:
//              Run this function within a new thread by calling
//                void *bThreadStruct = BThreadSpawn(MediaReaderThread, this)
//              from a CMediaReader member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CMediaWriterToolProc::ConverterThread(void *vpMediaWriter, void *vpBThread)
{
   CMediaWriterToolProc *tp = static_cast<CMediaWriterToolProc*>(vpMediaWriter);
   tp->InternalToNativeConverter(vpBThread);
}

void CMediaWriterToolProc::InternalToNativeConverter(void *vpBThread)
{
   bool threadActivity;
   bool processingActive = true;
   bool running = true;
   CAutotoolStatus toolStatus;
   CFrameRingBuffer::SRingSlot *bufferSlot;
   CToolFrameBuffer *toolFrameBuffer;
   int retVal;
	CHRTimer watchDog;
	int watchDogMinutes = 0;

   BThreadBegin(vpBThread);

   // Initial State
   formatConversionState = CONVERT_STATE_WAIT_FOR_NEW_FRAME;

   while (processingActive)
      {
      threadActivity = false;

      // Get commands
      if (exitThreadFlag)
         {
         processingActive = false;
         running = false;
         threadActivity = false;
         if (toolFrameBuffer != 0)
            {
            delete toolFrameBuffer;
            toolFrameBuffer = 0;
            }
         frameRingBuffer.DeleteRing();
         }

      if (running && !IsEmergencyStop())
         {
         // Conversion state machine - runs continuously, waiting for source
         // frame and destination buffer then does a conversion
         EToolMediaIOConversionState nextState = formatConversionState;
         switch (formatConversionState)
            {
            case CONVERT_STATE_WAIT_FOR_NEW_FRAME :
               // Poll for a new internal format frame
               toolFrameBuffer = GetFrame(0);
               if (toolFrameBuffer != 0)
                  {
/*
                  if (!dstClipLoaded && !toolFrameBuffer->IsEndOfMaterial())
                     {
                     // First time through load the destination clip.
                     // If doing clip-to-clip rendering to a new clip,
                     // the new clip may get created here
                     retVal = LoadDstClip();
                     if (retVal != 0)
                        {
                        // Failed to load/create destination clip
                        // Throw away the Tool Frame Buffer
                        delete toolFrameBuffer;
                        toolFrameBuffer = 0;

                        toolStatus.status = AT_STATUS_PROCESS_ERROR;
                        toolStatus.errorCode = retVal;
                        PutToolStatus(toolStatus);
                        }
                     }
                  if (dstClipLoaded ||
                      (toolFrameBuffer != 0 &&
                       toolFrameBuffer->IsEndOfMaterial()))
*/
                  if (toolFrameBuffer != 0)
                     {
                     if (toolFrameBuffer->IsEndOfMaterial())
                        {
                        //TRACE_3(errout << "Frame " << toolFrameBuffer->GetFrameIndex() <<  " End of Material");
                        nextState = CONVERT_STATE_WAIT_FOR_BUFFERS;
                        }
                     else if (toolFrameBuffer->IsPixelsChanged())
                        {
                        // Got a new frame and the destination clip is available,
                        // so move on to the next state
                        //TRACE_3(errout << "Frame " << toolFrameBuffer->GetFrameIndex() <<  " Change");
                        nextState = CONVERT_STATE_WAIT_FOR_BUFFERS;
                        }
                     else if (toolFrameBuffer->IsForceWrite())
                        {
                        // Tool wants to make sure we write the buffer
                        //TRACE_3(errout << "Frame " << toolFrameBuffer->GetFrameIndex() <<  " Force Write");
                        nextState = CONVERT_STATE_WAIT_FOR_BUFFERS;
                        }
                     else
                        {
                        // This frame has not been changed, so just
                        // skip over it rather than convert and write;
                        // If skipping, toss it into the reader cache
                        // so the contents may be re-used

                        //TRACE_3(errout << "Frame " << toolFrameBuffer->GetFrameIndex() << " NO Change");
                        // Release the Tool Frame Buffer
                        if (toolFrameBuffer->IsDontCache())
                           {
                           delete toolFrameBuffer;
                           }
                        else
                           {
                           GetSystemAPI()->GetToolFrameCache()->AddFrame(
                                                              toolFrameBuffer);
                           }
                        toolFrameBuffer = 0;
                        }
                     }
                  }
               break;

            case CONVERT_STATE_WAIT_FOR_BUFFERS :
               // Poll for an available empty native format frame buffer
               bufferSlot = frameRingBuffer.GetAvailableSlot();
               if (bufferSlot != 0)
                  {
                  // Got an empty buffer, so move on to the conversion state
                  nextState = CONVERT_STATE_CONVERT;
                  }
               break;

            case CONVERT_STATE_CONVERT :
               if (toolFrameBuffer->IsEndOfMaterial())
                  {
                  // Pass on the end-of-material flag
                  bufferSlot->endOfMaterial = true;
                  bufferSlot->errorFlag = -1;
                  bufferSlot->frameIndex = END_OF_MATERIAL_FRAME_INDEX;
                  }
               else
                  {
                  bufferSlot->endOfMaterial = false;

                  if (!dstClipLoaded)
                     {
                     // First time through load the destination clip.
                     // If doing clip-to-clip rendering to a new clip,
                     // the new clip may get created here
                     retVal = LoadDstClip(toolFrameBuffer->GetFrameIndex());
                     if (retVal != 0)
                        {
                        // Failed to load/create destination clip
                        bufferSlot->errorFlag = retVal;
                        }
                     }

                  bufferSlot->frameIndex = toolFrameBuffer->GetFrameIndex()
                                           + dstFrameOffset;

                  if (dstClipLoaded)
                     {
                     // Convert the internal format frame back to a native 
                     // format of the output clip
                     retVal = toolFrameBuffer->ConvertToNativeFormat(extractor,
                                                                 dstImageFormat,
                                                      bufferSlot->fieldBuffer);
                     bufferSlot->errorFlag = retVal;
                     if (retVal != 0)
                        {
                        CAutoErrorReporter autoErr("CMediaWriterToolProc::InternalToNativeConverter",
                                                   AUTO_ERROR_NOTIFY_NO_WAIT,
                                                   AUTO_ERROR_TRACE_0);
                        autoErr.errorCode = retVal;
                        autoErr.msg << "Could not convert to native image format ";
                        }
                     }
                  }

               // Make the converted native format frame available to the
               // media writer thread
               frameRingBuffer.PutAvailableSlot();

               // Release the Tool Frame Buffer;
               if (toolFrameBuffer->IsDontCache())
                  {
                  delete toolFrameBuffer;
                  }
               else
                  {
                  // Dump it to the reader cache so the contents can be re-used
                  GetSystemAPI()->GetToolFrameCache()->AddFrame(toolFrameBuffer);
                  }
               toolFrameBuffer = 0;

               threadActivity = true;

               // Done with this frame, so restart
               nextState = CONVERT_STATE_WAIT_FOR_NEW_FRAME;
               break;
			default:
				break;
            }

			if (nextState != formatConversionState)
				{
				watchDog.Start();
				watchDogMinutes = 0;
				}
			else if (formatConversionState != CONVERT_STATE_WAIT_FOR_NEW_FRAME)
				{
				int elapsedMinutes = int(watchDog.Read()) / (60 * 1000);
				if (elapsedMinutes > watchDogMinutes)
					{
					watchDogMinutes = elapsedMinutes;
					TRACE_0(errout << "WATCHDOG: InternalToNativeConverter appears to be hung in state "
										<< ((formatConversionState == CONVERT_STATE_WAIT_FOR_BUFFERS)
											  ? "CONVERT_STATE_WAIT_FOR_BUFFERS"
											  : "CONVERT_STATE_CONVERT")
										<< " [" << elapsedMinutes << " minute" << ((elapsedMinutes == 1) ? "]" : "s]"));
					}
				}

         formatConversionState = nextState;
         }
      // If this thread isn't doing anything other than waiting for something
      // to do, then sleep for a bit so we don't hog the processor
      if (!threadActivity)
         MTImillisleep(1);
      }

   // Post thread-ending status
   toolStatus.status = AT_STATUS_PROCESS_THREAD_ENDING;
   PutToolStatus(toolStatus);
} // InternalToNativeConverter


///////////////////////////////////////////////////////////////////////
// These are hacks to bypass the tool processor and instead simply
// write a single frame synchronously or asynchronously
///////////////////////////////////////////////////////////////////////

int CMediaWriterToolProc::WriteFrameSynchronous(int inPortIndex,
                                                CToolFrameBuffer *frameBuffer)
{
   CAutoThreadLocker atl(syncWriteThreadLock);
   int retVal = WriteFrameSynchronousInternal(inPortIndex, frameBuffer);
   if (retVal != 0)
      return retVal;
   return 0;
}

namespace {

   struct SWFAThreadParams
   {
      int inPortIndex;
      CToolFrameBuffer *frameBuffer;
      CMediaWriterToolProc *mediaWriterToolProc;

      SWFAThreadParams(int inPortIndexArg,
                      CToolFrameBuffer *frameBufferArg,
                      CMediaWriterToolProc *mediaWriterToolProcArg)
      : inPortIndex(inPortIndexArg)
      , frameBuffer(frameBufferArg)
      , mediaWriterToolProc(mediaWriterToolProcArg)
      {};
   };

   void WFAThread(void *vpThreadParams, void *vpBThreadID)
   {
      SWFAThreadParams * wfaThreadParams =
            static_cast<SWFAThreadParams *>( vpThreadParams );
      CMediaWriterToolProc *mediaWriterToolProc =
            wfaThreadParams->mediaWriterToolProc;
      int inPortIndex = wfaThreadParams->inPortIndex;
      CToolFrameBuffer *frameBuffer = wfaThreadParams->frameBuffer;

      BThreadBegin(vpBThreadID);

      int retVal = mediaWriterToolProc->WriteFrameSynchronous(inPortIndex,
                                                              frameBuffer);
      if (retVal != 0)
      {
         // WHAT DO I DO WITH THE ERROR?? QQQ
      }

      /* End thread */
   };

}; /* End anonymous namespace */

int CMediaWriterToolProc::WriteFrameAsynchronous(int inPortIndex,
                                                 CToolFrameBuffer *frameBuffer)
{
   // No more than one of these threads at a time!
   CAutoThreadLocker atl(syncWriteThreadLock);
   SWFAThreadParams wfaThreadParams(inPortIndex, frameBuffer, this);

   BThreadSpawn(WFAThread, &wfaThreadParams);

   return 0;
}

// CALLER MUST LOCK syncWriteThreadLock BEFORE CALLING!
int CMediaWriterToolProc::WriteFrameSynchronousInternal(int inPortIndex,
                                           CToolFrameBuffer *frameBuffer)
{
   int retVal;
   CAutoErrorReporter autoErr("CMediaWriterToolProc::WriteFrameSynchronous",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   CFrameRingBuffer::SRingSlot *bufferSlot;

   if (!dstClipLoaded && !frameBuffer->IsEndOfMaterial())
      {
      // First time through load the destination clip.
      // If doing clip-to-clip rendering to a new clip,
      // the new clip may get created here
      retVal = LoadDstClip(frameBuffer->GetFrameIndex());
      if (retVal != 0)
         {
         // Failed to load/create destination clip
         // Throw away the Tool Frame Buffer
         return retVal;
         }
      }

   // Get field buffers
   bufferSlot = frameRingBuffer.GetAvailableSlot();
   if (bufferSlot == 0)
      {
      autoErr.errorCode = TOOL_ERROR_UNEXPECTED_INTERNAL_ERROR;
      autoErr.msg << "Could not write video media, buffer slot not available";
      return autoErr.errorCode;
      }

   // Convert from internal to native image format
   frameBuffer->ConvertToNativeFormat(syncWriteExtractor, dstImageFormat,
                                      bufferSlot->fieldBuffer);

   bufferSlot->frameIndex = frameBuffer->GetFrameIndex() - dstFrameOffset;

   // Write the native field buffers to the media disk
   retVal = dstFrameList->writeMediaAllFieldsOptimized(bufferSlot->frameIndex,
                                                       bufferSlot->fieldBuffer);
   if (retVal != 0)
      {
      // Don't cache buffer if we couldn't write it!
      // Actually -- remove it from the cache if we couldn't write it
      // because we don't really know what's on disk...
      CToolFrameBuffer *cachedFrameBuffer;
      cachedFrameBuffer = GetSystemAPI()->GetToolFrameCache()->
                                 RetrieveFrame(frameBuffer->GetFrameIndex());
      delete cachedFrameBuffer;
      cachedFrameBuffer = 0;

      delete frameBuffer;
      frameBuffer = 0;

      autoErr.errorCode = retVal;
      autoErr.msg << "Could not write video media";
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         autoErr.msg << endl << "Cannot modify BOTH the "
                                "video AND the film frames";
      return retVal;
      }

   // Release the tool frame buffer;
   if (frameBuffer->IsDontCache())
      {
      // buffer is not cacheable - delete it!
      delete frameBuffer;
      }
   else
      {
      // send it to the reader cache instead of just deleting it
      GetSystemAPI()->GetToolFrameCache()->AddFrame(frameBuffer);
      }

   // We don't own it anymore
   frameBuffer = 0;

   // Update the "last media action" flag so user can see that
   // something has been done to the clip
   retVal = dstClip->updateMediaModifiedStatus(TRACK_TYPE_VIDEO,
                                               dstVideoProxyIndex);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "CClip::updateMediaModifiedStatus failed, Error = " << retVal;
      return retVal;
      }

   return 0;
}


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CFrameRingBuffer Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrameRingBuffer::CFrameRingBuffer()
: frameCount(0), fieldCount(0), fieldByteCount(0), slotRing(0),
  headIndex(0), tailIndex(0), bufferAllocator(0)
{
}

CFrameRingBuffer::~CFrameRingBuffer()
{
   DeleteRing();
}

//////////////////////////////////////////////////////////////////////
// Allocate & Deallocate the Media Buffers
//////////////////////////////////////////////////////////////////////

void CFrameRingBuffer::InitRing(int newFrameCount, int newFieldCount,
                                int newFieldByteCount,
                                CBufferPool *newBufferAllocator)
{
   // delete the ring if it was previously used
   DeleteRing();

   frameCount = newFrameCount;
   fieldCount = newFieldCount;
   fieldByteCount = newFieldByteCount;
   bufferAllocator = newBufferAllocator;

   slotRing = new SRingSlot[frameCount];

   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      slotRing[frameIndex].fieldBuffer = new MTI_UINT8*[fieldCount];
      for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         slotRing[frameIndex].fieldBuffer[fieldIndex] = 0;
         }
      slotRing[frameIndex].frameIndex = END_OF_MATERIAL_FRAME_INDEX;
      slotRing[frameIndex].errorFlag = 0;
      slotRing[frameIndex].endOfMaterial = false;
      // Hack to pass along an already converted buffer from the cache
      slotRing[frameIndex].alreadyConvertedToolFrameBuffer = 0;
      }
}

int CFrameRingBuffer::AllocateFieldBuffers()
{
   MTIassert(bufferAllocator != 0);

   int retVal = 0;

   // Get rid of any buffers than already exist
   FreeFieldBuffers();

   if (bufferAllocator == 0)
      return TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;

   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         retVal = bufferAllocator->allocateBuffer((void **)&slotRing[frameIndex].fieldBuffer[fieldIndex]);
         if (slotRing[frameIndex].fieldBuffer[fieldIndex] == 0 && retVal == 0)
            {
            // ERROR: failed to allocate one or more field buffers
            retVal = TOOL_ERROR_MEDIA_IO_MEM_ALLOC_FAILED;
            // Keep on going to get side effect of setting buffer pointers to NULL
            }
         }
      slotRing[frameIndex].frameIndex = END_OF_MATERIAL_FRAME_INDEX;
      slotRing[frameIndex].errorFlag = 0;
      slotRing[frameIndex].endOfMaterial = false;
      // Hack to pass along an already converted buffer from the cache
      slotRing[frameIndex].alreadyConvertedToolFrameBuffer = 0;
      }

   if (retVal != 0)
      {
      FreeFieldBuffers();
      return retVal;
      }

   return retVal;
}

void CFrameRingBuffer::DeleteRing()
{
   // Free field buffers, if they exist anymore
   FreeFieldBuffers();

   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      delete [] slotRing[frameIndex].fieldBuffer;
      // Hack to pass along an already converted buffer from the cache
      delete slotRing[frameIndex].alreadyConvertedToolFrameBuffer;
      }

   delete [] slotRing;

   frameCount = 0;
   fieldCount = 0;
   fieldByteCount = 0;
   slotRing = 0;

   headIndex = 0;
   tailIndex = 0;

   // The buffer alklocator is now useless to us, so avoid dangling pointers
   //in case it is deleted
   bufferAllocator = 0;
}

void CFrameRingBuffer::FreeFieldBuffers()
{
   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         if (slotRing[frameIndex].fieldBuffer[fieldIndex] != 0)
            {
            bufferAllocator->freeBuffer(
                                 slotRing[frameIndex].fieldBuffer[fieldIndex]);
            slotRing[frameIndex].fieldBuffer[fieldIndex] = 0;
            }
         }
      }

   headIndex = 0;
   tailIndex = 0;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CFrameRingBuffer::GetBufferCount() const
{
   return frameCount * fieldCount;
}

int CFrameRingBuffer::GetFrameCount() const
{
   return frameCount;
}

int CFrameRingBuffer::GetFieldCount() const
{
   return fieldCount;
}

int CFrameRingBuffer::GetFieldByteCount() const
{
   return fieldByteCount;
}

//////////////////////////////////////////////////////////////////////
// Empty Slot Functions
//////////////////////////////////////////////////////////////////////

bool CFrameRingBuffer::IsSlotAvailable()
{
   return (headIndex - tailIndex < frameCount);
}

CFrameRingBuffer::SRingSlot* CFrameRingBuffer::GetAvailableSlot()
{
   if (!IsSlotAvailable())
      return 0;

   return &(slotRing[headIndex % frameCount]);
}

void CFrameRingBuffer::PutAvailableSlot()
{
   ++headIndex;
}

//////////////////////////////////////////////////////////////////////
//  Functions
//////////////////////////////////////////////////////////////////////

bool CFrameRingBuffer::Empty()
{
   return !(tailIndex < headIndex);
}

CFrameRingBuffer::SRingSlot* CFrameRingBuffer::GetFrameBuffer()
{
   if (Empty())
      return 0;

   return &(slotRing[tailIndex % frameCount]);
}

void CFrameRingBuffer::ReleaseFrameBuffer()
{
   ++tailIndex;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CFrameRingBuffer::Flush()
{
   headIndex = 0;
   tailIndex = 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CMediaIOConfig::CMediaIOConfig(int newInPortCount, int newOutPortCount)
 : CToolIOConfig(newInPortCount, newOutPortCount), useNavCurrentClip(false),
   videoProxyIndex(-1), videoFramingIndex(-1)
{
}

CMediaIOConfig::~CMediaIOConfig()
{
}

CMediaReaderIOConfig::CMediaReaderIOConfig()
 : CMediaIOConfig(0, 1)
{
}

CMediaReaderIOConfig::~CMediaReaderIOConfig()
{
}

CMediaWriterIOConfig::CMediaWriterIOConfig()
 : CMediaIOConfig(1, 0)
{
}

CMediaWriterIOConfig::~CMediaWriterIOConfig()
{
}





