//----------------------------------------------------------------------------
#include "TrackingBoxManager.h"

#include <math.h>
#include "ImageFormat3.h"
#include "ToolSystemInterface.h"
//----------------------------------------------------------------------------

#define INVALID_FRAME_INDEX (-1)

//----------------------------------------------------------------------------

// Clicking with the mouse:
//
// if mouse is TRACKING
//    if pt is not captured do
//       create new selected pt
//    elseif pt is captured
//       if shift key up
//          unselect all pts
//          select captured pt
//          if ctrl key dn
//             collapse slice to captured pt
//       else
//          select captured pt
//    goto drag selected pts
//----------------------------------------------------------------------------

TrackingBoxManager::TrackingBoxManager()
{
   // no tracking array yet
   trackingArray = NULL;

   // init min and max
	_startFrame = INVALID_FRAME_INDEX;
	_stopFrame = INVALID_FRAME_INDEX;
   _isMouseButtonDown = false;

   // We don't actually need these anymore since I moved the boxes into the
   // TrackArray
   trackBoxExtentX = -1;
   trackBoxExtentY = -1;
   searchBoxExtentLeft = -1;
   searchBoxExtentRight = -1;
   searchBoxExtentUp = -1;
   searchBoxExtentDown = -1;

   // init panning flag - we don't bother refreshing the screen during panning
   _isInPanningMode = false;

   //  Init the properties
   SPROPERTY_INIT(lastSelectedTag, _lastSelectedTag, setLastSelectedTag);
   SPROPERTY_INIT(displayTrackingPoints, _displayTrackingPoints, setDisplayTrackingPoints);

   _lastSelectedTag = -2;
   _displayTrackingPoints = true;
   _isABoxBeingDragged = false;
   _isIgnoringMarks = false;
   _canMoveTrackPointsAtAnyFrame = false;
   _canSelectMultiplePoints = true;
   _canDragPoints = true;
   _trailSize = 0;
   _color = ETPRed;
	_lastTrackingArrayFrameRendered = -1;
	_trackingArrayWasRendered = false;

   // Mouse position within frame
   mouseXFrame = -1;
   mouseYFrame = -1;

   systemAPI = NULL;

}
//----------------------------------------------------------------------------

void TrackingBoxManager::loadSystemAPI(CToolSystemInterface *newSystemAPI)
{
   systemAPI = newSystemAPI;

   // we can init these two variables now
	_startFrame = INVALID_FRAME_INDEX;
   _stopFrame = INVALID_FRAME_INDEX;

   // Frickin' hack because we try to draw the points initially
   // BEFORE SETTING THE SYSTEM API POINTER, which needs this pointer
   if (trackingArray != NULL)
   {
      drawTrackingPointsCurrentFrame();
   }
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setTrackingArray(TrackingBoxSet *newArray,
                                             bool preserveTrackingData)
{
	trackingArray = newArray;

	invalidateTrackingDataIfItWasUsedForRendering();
   lastSelectedTag = INVALID_TRACKING_BOX_TAG;

	if (trackingArray != nullptr && !preserveTrackingData)
	{
		if (!_isIgnoringMarks)
		{
			setRangeFromMarks();
		}

		auto inFrame = trackingArray->getInFrame();
		if (inFrame == INVALID_FRAME_INDEX)
		{
			trackingArray->clear();
		}
		else
		{
			trackingArray->invalidateTracking();
		}
	}

	invalidateTrackingDataIfItWasUsedForRendering();

   TrackingParameterChange.Notify();
}
//----------------------------------------------------------------------------

void TrackingBoxManager::resetTrackingArray()
{
	setRange(INVALID_FRAME_INDEX, INVALID_FRAME_INDEX);
	lastSelectedTag = INVALID_TRACKING_BOX_TAG;

	removeAllTrackingBoxes();
}

TrackingBoxSet *TrackingBoxManager::getTrackingArray()
{
   return trackingArray;
}
//----------------------------------------------------------------------------
//
// Tool calls this to let us know that tracking has stopped with the tracking
// array partially filled with valid data
//
void TrackingBoxManager::notifyTrackingStopped()
{
	TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------
//
// Tool calls this to let us know that the tracking array is completely
// full of valid data
//
void TrackingBoxManager::notifyTrackingComplete()
{
	TrackingPointStateChange.Notify();
}

//----------------------------------------------------------------------------
//
// Tool calls this to let us know that the tracking array is about to be used
// for rendering.
//
void TrackingBoxManager::notifyFrameRendered(int frame)
{
	if (trackingArray != NULL)
	{
		_lastTrackingArrayFrameRendered = frame;
	}
}

//----------------------------------------------------------------------------
//
// Tool calls this to let us know that the tracking array was used for a
// complete render
//
void TrackingBoxManager::notifyRenderComplete()
{
	// Take a snapshot of the array used for complete rendering
	if (trackingArray != NULL)
	{
		_trackingArrayWasRendered = true;
	}
}
//----------------------------------------------------------------------------

bool TrackingBoxManager::isTrackingDataValidOverEntireRange() const
{
	if (trackingArray == NULL || trackingArray->getInFrame() == INVALID_FRAME_INDEX)
	{
		return false;
	}

	bool boundsAreOK = _startFrame >= 0 &&
                      _stopFrame > _startFrame &&
                      _startFrame >= trackingArray->getInFrame() &&
                      _stopFrame <= trackingArray->getOutFrame();

   bool pointCountIsOK = (trackingArray->getNumberOfTrackingBoxes() >= 1);

	bool trackedOK = trackingArray->areAllTrackingBoxesFullyTracked();

	bool notYetRendered = _lastTrackingArrayFrameRendered == -1 && !_trackingArrayWasRendered;

	return (boundsAreOK && pointCountIsOK && trackedOK && notYetRendered);
}
//----------------------------------------------------------------------------

bool TrackingBoxManager::isFrameTrackingDataValid(int frame) const
{
	if (trackingArray == NULL || trackingArray->getInFrame() == INVALID_FRAME_INDEX)
	{
		return false;
	}

	if (_trackingArrayWasRendered || frame <= _lastTrackingArrayFrameRendered)
	{
		return false;
	}

	bool boundsAreOK = frame >= 0 &&
                      frame >= trackingArray->getInFrame() &&
                      frame < trackingArray->getOutFrame();

   bool pointCountIsOK = (trackingArray->getNumberOfTrackingBoxes() >= 1);

	bool trackedOK = frame < trackingArray->getFirstUntrackedFrame();

	bool notYetRendered = _lastTrackingArrayFrameRendered < frame && !_trackingArrayWasRendered;

	return (boundsAreOK && pointCountIsOK && trackedOK && notYetRendered);
}
//----------------------------------------------------------------------------

bool TrackingBoxManager::wasTrackingDataInvalidatedByRendering()
{
	if (trackingArray == NULL || trackingArray->getInFrame() == INVALID_FRAME_INDEX)
	{
		return false;
	}

	return _trackingArrayWasRendered || _lastTrackingArrayFrameRendered != -1;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::invalidateAllTrackingData()
{
	if (trackingArray == NULL)
	{
      return;
	}

	_lastTrackingArrayFrameRendered = -1;
	_trackingArrayWasRendered = false;
	trackingArray->invalidateTracking();
	TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------

void TrackingBoxManager::removeAllTrackingBoxes()
{
	if (trackingArray == NULL || trackingArray->getNumberOfTrackingBoxes() < 1 || systemAPI == NULL)
   {
		return;
	}

	trackingArray->removeAllTrackingBoxes();
	lastSelectedTag = INVALID_TRACKING_BOX_TAG;

	auto currentFrame = systemAPI->getLastFrameIndex();
	if (trackingArray->getInFrame() >= 0 && currentFrame >= trackingArray->getInFrame() && currentFrame < trackingArray->getOutFrame())
	{
		systemAPI->refreshFrameCached();
	}

	_lastTrackingArrayFrameRendered = -1;
	_trackingArrayWasRendered = false;

	TrackingParameterChange.Notify();
	TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------

int TrackingBoxManager::canInMarkChange() const
{
   // See if we were initialized properly
	if (trackingArray == NULL || systemAPI == NULL || trackingArray->getInFrame() < 0)
	{
		return START_FRAME_CAN_CHANGE;
	}

   // See if we actually care about the marks!
	if (_isIgnoringMarks)
	{
		return START_FRAME_CAN_CHANGE;   // No!
   }

   return checkStartFrameChange(systemAPI->getMarkIn());
}
//----------------------------------------------------------------------------

int TrackingBoxManager::checkStartFrameChange(int newStartFrame) const
{
	// See if we were initialized properly
	if (trackingArray == NULL || systemAPI == NULL || trackingArray->getInFrame() < 0)
      return START_FRAME_CAN_CHANGE;

   // See if there aren't any tracking points yet
   if (trackingArray->getNumberOfTrackingBoxes() == 0)
      return START_FRAME_CAN_CHANGE;

   // See if the start frame is not really changing
   if (newStartFrame == _startFrame)
      return START_FRAME_CAN_CHANGE;

   // See if the start frame was deleted
	if (newStartFrame == INVALID_FRAME_INDEX)
      return START_FRAME_IS_DELETED;

   // See if we moved outside of the previous start-stop range
   if (newStartFrame < _startFrame ||
       newStartFrame >= _stopFrame)
      return START_FRAME_IS_MOVED_OUTSIDE_INTERVAL;

   // Otherwise we must have moved to within the previous start-stop range
   return START_FRAME_IS_MOVED_INSIDE_INTERVAL;
}
//----------------------------------------------------------------------------

int TrackingBoxManager::canOutMarkChange() const
{
   // See if we were initialized properly
	if (trackingArray == NULL || systemAPI == NULL || trackingArray->getInFrame() < 0)
	{
		return STOP_FRAME_CAN_CHANGE;
	}

   // See if we actually care about the marks!
	if (_isIgnoringMarks)
	{
		return STOP_FRAME_CAN_CHANGE; // No!
	}

   return checkStopFrameChange(systemAPI->getMarkOut());
}
//------------------------------------------------------------------------------

int TrackingBoxManager::checkStopFrameChange(int newStopFrame) const
{
	// See if we were initialized properly yet
	if (trackingArray == NULL || systemAPI == NULL || trackingArray->getInFrame() < 0)
	{
		return STOP_FRAME_CAN_CHANGE;
	}

	// If the tracking array is empty, allow all
	if (trackingArray->getNumberOfTrackingBoxes() == 0)
	{
		return STOP_FRAME_CAN_CHANGE;
	}

	// If the tracking array is only on the start frame, allow all
	if ((trackingArray->getInFrame() == _startFrame) && (trackingArray->size() < 1))
	{
		return STOP_FRAME_CAN_CHANGE;
	}

	// See if the stop frame is not really changing
	if (newStopFrame == _stopFrame)
	{
		return STOP_FRAME_CAN_CHANGE;
	}

	// See if the stop frame was deleted
	if (newStopFrame == INVALID_FRAME_INDEX)
	{
		return STOP_FRAME_IS_DELETED;
	}

	// See if we moved outside of the previous start-stop range
	if (newStopFrame <= _startFrame || newStopFrame > _stopFrame)
	{
		return STOP_FRAME_IS_MOVED_OUTSIDE_INTERVAL;
	}

	// Otherwise we must have moved to within the previous start-stop range
	return STOP_FRAME_IS_MOVED_INSIDE_INTERVAL;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setRange(int newStartFrame, int newStopFrame)
{
	auto changed = _startFrame != newStartFrame || _stopFrame != newStopFrame;
	auto trackingDataCantBeAllValid = _startFrame > newStartFrame || _stopFrame < newStopFrame;
	_startFrame = newStartFrame;
	_stopFrame = newStopFrame;

	if (trackingArray != NULL
	&& (trackingArray->getInFrame() != newStartFrame || trackingArray->getOutFrame() != newStopFrame))
	{
		changed = true;
//		trackingArray->setInFrame(_startFrame);
//		trackingArray->setOutFrame(_stopFrame);
		trackingArray->setInAndOutFrames(_startFrame, _stopFrame);
	}

	invalidateTrackingDataIfItWasUsedForRendering();

	if (changed)
	{
		rangeChange.Notify();
	}

	if (systemAPI != NULL)
	{
		systemAPI->refreshFrameCached();
	}
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setRangeFromMarks()
{
	if (systemAPI != NULL)
	{
		setRange(systemAPI->getMarkIn(), systemAPI->getMarkOut());
	}
}
//----------------------------------------------------------------------------

void TrackingBoxManager::getRange(int &outStartFrame, int &outStopFrame) const
{
	outStartFrame = _startFrame;
	outStopFrame = _stopFrame;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setIgnoringMarksFlag(bool newIgnoreFlag)
{
   _isIgnoringMarks = newIgnoreFlag;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setTrailSize(int numberOfPoints)
{
	_trailSize = numberOfPoints;

   // Poorly named system call -- really "is playing?".
	if (systemAPI == NULL || systemAPI->isDisplaying())
   {
      return;
   }

   refreshTrackingPointsFrame();
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setCanMoveTrackPointsAtAnyFrame(bool flag)
{
   _canMoveTrackPointsAtAnyFrame = flag;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setCanSelectMultiplePoints(bool flag)
{
   _canSelectMultiplePoints = flag;
}
//----------------------------------------------------------------------------

void TrackingBoxManager::setCanDragPoints(bool flag)
{
   _canDragPoints = flag;
}
//------------------------------------------------------------------------------

void TrackingBoxManager::setPanningMode(bool flag)
{
   _isInPanningMode = flag;
}
//------------------------------------------------------------------------------

void TrackingBoxManager::setTrackingBoxSizes(
   int newTrackBoxExtentX,     int newTrackBoxExtentY,
   int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
   int newSearchBoxExtentUp,   int newSearchBoxExtentDown
   )
{
	if (trackingArray == NULL)
	{
		return;
	}

	invalidateTrackingDataIfItWasUsedForRendering();

   if (trackingArray->getTrackingBoxRadius() != newTrackBoxExtentX
   || trackingArray->getSearchBoxRadius() != newSearchBoxExtentLeft)
	{
		trackingArray->setTrackingBoxRadius(newTrackBoxExtentX);
		trackingArray->setSearchBoxRadius(newSearchBoxExtentLeft);

		refreshTrackingPointsFrame();

      if (trackingArray != NULL)
      {
			TrackingParameterChange.Notify();
      }

      TrackingPointStateChange.Notify();
   }
}
//------------------------------------------------------------------------------

void TrackingBoxManager::selectTrackingBox(int trackingBoxTag)
{
   unselectTrackingBox(trackingBoxTag);
   _selectedTrackingBoxTags.push_back(trackingBoxTag);
}
//------------------------------------------------------------------------------

void TrackingBoxManager::unselectTrackingBox(int trackingBoxTag)
{
   auto iter = find(_selectedTrackingBoxTags.begin(), _selectedTrackingBoxTags.end(), trackingBoxTag);
   if (iter != _selectedTrackingBoxTags.end())
   {
      _selectedTrackingBoxTags.erase(iter);
   }
}
//------------------------------------------------------------------------------

void TrackingBoxManager::selectAllTrackingBoxes()
{
	if (trackingArray == NULL)
   {
      return;
   }

   _selectedTrackingBoxTags = trackingArray->getTrackingBoxTags();
}
//------------------------------------------------------------------------------

void TrackingBoxManager::unselectAllTrackingBoxes()
{
   _selectedTrackingBoxTags.clear();
}
//------------------------------------------------------------------------------

bool TrackingBoxManager::isTrackingBoxSelected(int trackingBoxTag) const
{
   auto begin = _selectedTrackingBoxTags.begin();
   auto end = _selectedTrackingBoxTags.end();
   return find(begin, end, trackingBoxTag) != end;
}
//------------------------------------------------------------------------------
void TrackingBoxManager::invalidateTrackingDataIfItWasUsedForRendering()
{
	if (_lastTrackingArrayFrameRendered == -1 && !_trackingArrayWasRendered)
	{
		// Nothing to do here.
		return;
	}

	// This will reset _lastTrackingArrayFrameRendered and _trackingArrayWasRendered.
	invalidateAllTrackingData();
}
//------------------------------------------------------------------------------

void TrackingBoxManager::mouseDown(int modKeys)
{
	if (trackingArray == NULL || systemAPI == NULL)
	{
		return;
	}

	// I think this is just asking in a roundabout way if any clip is selected.
	int icurFrame = systemAPI->getLastFrameIndex();
   if (icurFrame < 0)
   {
      return;
   }

   _isMouseButtonDown = true;

   if (_isABoxBeingDragged)
   {
      // Work around a state bug - should not be able to get a mouse down
      // if we are dragging a box!!
      _isABoxBeingDragged = false;
   }

   // See if user clicked on a tracking point.
//   trackingArray->getExtents(trackBoxExtentX, trackBoxExtentY,
//                             searchBoxExtentLeft, searchBoxExtentRight,
//                             searchBoxExtentUp, searchBoxExtentDown);
   trackBoxExtentX = trackBoxExtentY = trackingArray->getTrackingBoxRadius();
   searchBoxExtentLeft = searchBoxExtentRight = searchBoxExtentUp = searchBoxExtentDown = trackingArray->getSearchBoxRadius();
   DRECT searchBox;
   searchBox.left   = mouseXFrame - trackBoxExtentX;
   searchBox.top    = mouseYFrame - trackBoxExtentY;
   searchBox.right  = mouseXFrame + trackBoxExtentX;
   searchBox.bottom = mouseYFrame + trackBoxExtentY;
   int trackingPointIndex = findTrackingPointInBox(searchBox);

   // NOTE: Tools actually set the MOD_KEY_SHIFT when CTRL is pressed!
   const bool leaveOnlyOneSelectedPoint = (!_canSelectMultiplePoints) || (modKeys & MOD_KEY_SHIFT) == 0;
   if (leaveOnlyOneSelectedPoint)
   {
      unselectAllTrackingPoints();
   }
   else if (trackingPointIndex == lastSelectedTag)
   {
       // We ctrl-clicked on a selected tag. Just de-select it!
      unselectTrackingBox(trackingPointIndex);
      lastSelectedTag = INVALID_TRACKING_BOX_TAG;
      return;
   }

   if (trackingPointIndex == INVALID_TRACKING_BOX_TAG)
   {
      // Create a new selected tracking point.
      trackingPointIndex = createTrackingPoint(mouseXFrame, mouseYFrame, true);

      // Obviously, this must precede the setting of the lastSelectedTag property!
      NextSelectedTagIsANewTrackingPoint.Notify();
      lastSelectedTag = trackingPointIndex;
   }
   else
   {
      // Select an existing tracking point.
      selectTrackingBox(trackingPointIndex);
      lastSelectedTag = trackingPointIndex;
   }

   if (trackingPointIndex != -1 && _canDragPoints)
   {
      _isABoxBeingDragged = true;
   }

	systemAPI->refreshFrameCached();
}
//------------------------------------------------------------------------------

void TrackingBoxManager::mouseMove(int x, int y)
{
	if (systemAPI == NULL)
	{
		return;
	}

	mouseXClient = x;
   mouseYClient = y;

   int newMouseXFrame  = systemAPI->dscaleXClientToFrame(x);
   int newMouseYFrame  = systemAPI->dscaleYClientToFrame(y);

   deltaMouseXFrame = newMouseXFrame - mouseXFrame;
   deltaMouseYFrame = newMouseYFrame - mouseYFrame;

   // Exit if we are doing nothing
   if (deltaMouseXFrame == 0 && deltaMouseYFrame == 0)
   {
      return;
   }

   mouseXFrame = newMouseXFrame;
   mouseYFrame = newMouseYFrame;

//TRACE_2(errout << "=== (" << mouseXFrame << "," << mouseYFrame << ")");

   if (!_isMouseButtonDown)
   {
      return;
   }

   if (!_isABoxBeingDragged)
   {
      return;
   }

   translateSelectedTrackingPointsCurrentFrame(deltaMouseXFrame, deltaMouseYFrame);

	invalidateTrackingDataIfItWasUsedForRendering();

	if (!_isInPanningMode)
   {
		systemAPI->refreshFrameCached();
   }

   TrackingParameterChange.Notify();
   TrackingPointStateChange.Notify();
}
//------------------------------------------------------------------------------

void TrackingBoxManager::mouseUp()
{
	if (systemAPI == NULL)
	{
		return;
	}

   _isMouseButtonDown = false;

   if (!_isABoxBeingDragged)
   {
      return;
   }

   systemAPI->refreshFrameCached();
   _isABoxBeingDragged = false;
}
//------------------------------------------------------------------------------

void TrackingBoxManager::drawTrackingPointsCurrentFrame(int defaultColor)
{
	if (trackingArray == NULL)
		return;
	if (trackingArray->getInFrame() < 0)
      return;
   if (trackingArray->getNumberOfTrackingBoxes() == 0)
      return;
   if (systemAPI == NULL)
      return;
   if (!_displayTrackingPoints)
      return;

   int curFrame = systemAPI->getLastFrameIndex();
   if (!trackingArray->isFrameInRange(curFrame))
   {
      return;
   }

	// I hope the presently set color is right!!
	int graphicsColor = 	(_trackingArrayWasRendered || _lastTrackingArrayFrameRendered >= curFrame)
								? SOLID_BLUE
								: (isTrackingDataValidOverEntireRange
									? SOLID_GREEN
									: defaultColor);

   if (!_isIgnoringMarks)
   {
      // Only show tracking points between the marks (or on the mark IN if
      // there is no mark OUT defined)
      int markInFrame = systemAPI->getMarkIn();
      int markOutFrame = systemAPI->getMarkOut();
      bool onlyShowingPointsOnMarkIn = false;

      if (markInFrame == -1)
         return;
      if (markOutFrame == -1 || markOutFrame <= markInFrame)
      {
         if (curFrame != markInFrame)
            return;

         onlyShowingPointsOnMarkIn = true;
      }
      else
      {
         if (curFrame < markInFrame || curFrame >= markOutFrame)
            return;
      }

      if (onlyShowingPointsOnMarkIn ||
          (markInFrame < trackingArray->getInFrame()) ||
          (markOutFrame > trackingArray->getOutFrame()))
      {
		 graphicsColor = defaultColor;
      }
   }

   // Find the size
   const CImageFormat *imgFmt = systemAPI->getVideoClipImageFormat();
   if (imgFmt == NULL)
      return;

   // Create the arrow sizes normalized by the image size
   int nWidth = imgFmt->getPixelsPerLine()/60;
   int nLen = 1.5*nWidth;

//   trackingArray->getExtents(trackBoxExtentX, trackBoxExtentY,
//                             searchBoxExtentLeft, searchBoxExtentRight,
//                             searchBoxExtentUp, searchBoxExtentDown);
   trackBoxExtentX = trackBoxExtentY = trackingArray->getTrackingBoxRadius();
   searchBoxExtentLeft = searchBoxExtentRight = searchBoxExtentUp = searchBoxExtentDown = trackingArray->getSearchBoxRadius();

   // No trails if size is zero or if we're at the IN frame.
   if (_trailSize > 0 && curFrame != trackingArray->getInFrame())
   {
      // Draw trails.
      int historyBoxExtent = (trackBoxExtentX) < 4 ? 2 : (trackBoxExtentX / 4);

		int firstFrameToCheck = std::max<int>(curFrame - _trailSize, 0);
		for (int frame = firstFrameToCheck; frame <= curFrame; ++frame)
      {
         for (auto tag : trackingArray->getTrackingBoxTags())
         {
				TrackingBox &trackingBox = trackingArray->getTrackingBoxByTag(tag);
            if (!trackingBox.isValidAtFrame(curFrame))
            {
               break;
            }

            if (!trackingBox.isValidAtFrame(frame))
            {
               continue;
            }

            auto trackingPosition = trackingBox.getTrackingPositionAtFrame(frame);

            int pointColor = (graphicsColor == SOLID_BLUE)
                              ? graphicsColor
                              : (trackingBox.isValidAtFrame(trackingArray->getOutFrame() - 1)
                                 ? SOLID_GREEN
                                 : (trackingBox.isValidAtFrame(trackingArray->getInFrame()+ 1)
                                    ? SOLID_YELLOW
                                    : SOLID_RED));

            systemAPI->setGraphicsColor(pointColor);

            systemAPI->drawTrackingBoxNoFlush(
                                  trackingPosition.in.x, trackingPosition.in.y,
                                  historyBoxExtent, historyBoxExtent,
                                  0, 0, // no search box
                                  false); // no crosshairs

            if (FPOINT(trackingPosition.in) != FPOINT(trackingPosition.out))
            {
               systemAPI->drawTrackingBoxNoFlush(
                                  trackingPosition.out.x, trackingPosition.out.y,
                                  historyBoxExtent, historyBoxExtent,
                                  0, 0, // no search box
                                  false); // no crosshairs

               systemAPI->drawArrowFrameNoFlush(
                                  trackingPosition.in.x, trackingPosition.in.y,
                                  trackingPosition.out.x, trackingPosition.out.y,
                                  0, 0);  // No arrow heads, just draw a line
            }
         }
      }
   }

   auto trackingBoxTags = trackingArray->getTrackingBoxTags();

   for (auto trackingBoxTag : trackingBoxTags)
   {
		TrackingBox &trackingBox = trackingArray->getTrackingBoxByTag(trackingBoxTag);
      if (!trackingBox.isValidAtFrame(curFrame))
      {
         continue;
      }

      auto trackingBoxPosition = trackingBox.getTrackingPositionAtFrame(curFrame);
      auto &in = trackingBoxPosition.in;
      auto &out = trackingBoxPosition.out;


      int pointColor = (graphicsColor == SOLID_BLUE)
                        ? graphicsColor
                        : (trackingBox.isValidAtFrame(trackingArray->getOutFrame() - 1)
                           ? SOLID_GREEN
                           : (trackingBox.isValidAtFrame(trackingArray->getInFrame()+ 1)
                              ? SOLID_YELLOW
                              : SOLID_RED));

      systemAPI->setGraphicsColor(pointColor);

      if (isTrackingBoxSelected(trackingBoxTag))
      {
         // There may be multiple selected points, only the last
         // gets a motion box
         if (trackingBoxTag == lastSelectedTag)
         {
            systemAPI->drawTrackingBoxNoFlush(
						out.x, out.y,
                  trackBoxExtentX, trackBoxExtentY,
                  searchBoxExtentLeft + trackBoxExtentX,
                  searchBoxExtentUp + trackBoxExtentY,
                  true); // show crosshairs
         }
         else
         {
            systemAPI->drawTrackingBoxNoFlush(
						out.x, out.y,
                  trackBoxExtentX, trackBoxExtentY,
                  0, 0,  // no motion box
                  true); // show crosshairs
         }
      }
      else
      {
         // Not selected - just a plain box
         systemAPI->drawTrackingBoxNoFlush(
					out.x, out.y,
               trackBoxExtentX, trackBoxExtentY,
               0, 0,   // no motion box
               false); // no crosshairs
      }

      // Was the tracking point moved at this frame?
      if (in != out)
      {
			// Yes - draw another box at the spot it moved from and an arrow from
         // that spot to the new position.
         systemAPI->drawTrackingBoxNoFlush(
					in.x, in.y,
               trackBoxExtentX, trackBoxExtentY,
               0, 0,   // no search box
               false); // no crosshairs

         systemAPI->drawArrowFrameNoFlush(in.x, in.y, out.x, out.y, nWidth, nLen);
      }

//      // See if we are linked to another box
//      tpAnchor anchorType =
//            (trackingArray->getAnchorIndex(i) == INVALID_TRACKING_BOX_TAG) ?
//            EANCHOR_NONE : trackingArray->getAnchor(i);
//      systemAPI->setGraphicsColor(SOLID_YELLOW);
//      if ((anchorType == EANCHOR_HORZIONAL) || (anchorType == EANCHOR_VERTICAL)
//            || (anchorType == EANCHOR_ALL))
//      {
//         TrackingBoxPosition& frmBaseTrkPt =
//               frmTrkPts[trackingArray->getAnchorIndex(i)];
//         systemAPI->drawArrowFrameNoFlush(FPOINT(frmTrkPt.in).x,
//               FPOINT(frmTrkPt.in).y, FPOINT(frmBaseTrkPt.in).x,
//               FPOINT(frmBaseTrkPt.in).y, nWidth, nLen);
//      }
   }
}
//------------------------------------------------------------------------------

void TrackingBoxManager::drawLineFromSelectedTrackingPointToMousePosition(int color)
{
   if (trackingArray == NULL)
   {
      return;
   }

   if (trackingArray->getNumberOfTrackingBoxes() == 0)
   {
      return;
   }

   if (systemAPI == NULL)
   {
      return;
   }

   int curFrame = systemAPI->getLastFrameIndex();
   if (!trackingArray->isFrameInRange(curFrame))
   {
      return;
   }

   int tag = lastSelectedTag;
   if (tag == INVALID_TRACKING_BOX_TAG || !trackingArray->isValidTrackingBoxTag(tag))
   {
      return;
   }

	TrackingBox &lastSelectedBox = trackingArray->getTrackingBoxByTag(tag);

   // Find the size
   const CImageFormat *imgFmt = systemAPI->getVideoClipImageFormat();
   if (imgFmt == NULL)
   {
      return;
   }

   if (mouseXFrame < 0 || mouseXFrame > imgFmt->getPixelsPerLine()
   || mouseYFrame < 0 || mouseYFrame > imgFmt->getLinesPerFrame())
   {
      // Mouse moved out of the frame - don't draw the yellow line
      return;
   }

   // Create the arrow sizes normalized by the image size
   int nWidth = imgFmt->getPixelsPerLine()/60;
   int nLen = 1.5*nWidth;

   // Draw the arrow line
   systemAPI->setGraphicsColor(color);
   auto trackingBoxPosition = lastSelectedBox.getTrackingPositionAtFrame(curFrame);
   systemAPI->drawArrowFrameNoFlush(
                         trackingBoxPosition.in.x, trackingBoxPosition.in.y,
                         mouseXFrame, mouseYFrame,
                         nWidth, nLen);

   systemAPI->flushGraphics();  // Haha there is no drawArrowFrame()!
}
//------------------------------------------------------------------------------

void TrackingBoxManager::refreshTrackingPointsFrame()
{
	if (trackingArray == NULL)
      return;
   if (systemAPI == NULL)
      return;

   //************** SIDE EFFECT ALERT !!! **************************//
   if (trackingArray->getNumberOfTrackingBoxes() <= 0 || trackingArray->getInFrame() < 0)
      lastSelectedTag = INVALID_TRACKING_BOX_TAG;
   //************** SIDE EFFECT ALERT !!! **************************//

   systemAPI->refreshFrameCached();
}
//------------------------------------------------------------------------------

void TrackingBoxManager::unselectAllTrackingPoints()
{
	if (trackingArray == NULL)
   {
      return;
   }

   unselectAllTrackingBoxes();
   lastSelectedTag = INVALID_TRACKING_BOX_TAG;
}
//------------------------------------------------------------------------------

// Given a rectangular subset of the image as defined by selbox, return
// the index of a random tracking point whose position falls inside the
// the rectangle (well it isn't really random - it returns the lowest-indexed
// point if there are more than one)

int TrackingBoxManager::findTrackingPointInBox(DRECT &selbox)
{
	if (trackingArray == NULL
	|| trackingArray->getNumberOfTrackingBoxes() == 0
	|| trackingArray->getInFrame() < 0
	|| systemAPI == NULL)
	{
		return INVALID_FRAME_INDEX;
   }

	int curFrame = systemAPI->getLastFrameIndex();

   if (trackingArray->isFrameInRange(curFrame))
   {
      auto trackingBoxTags = trackingArray->getTrackingBoxTags();

      for (auto tag : trackingBoxTags)
      {
         TrackingBox &trackingBox = trackingArray->getTrackingBoxByTag(tag);
         if (!trackingBox.isValidAtFrame(curFrame))
         {
            continue;
         }

         auto trackingBoxPosition = trackingBox.getTrackingPositionAtFrame(curFrame);

         // QQQ in? out? both?
         FPOINT tp = trackingBoxPosition.out;
         if ((tp.x >= selbox.left) && (tp.x <= selbox.right) &&
             (tp.y >= selbox.top ) && (tp.y <= selbox.bottom))
         {
            return tag;
         }
      }
   }

   // No tracking points found in the selection box
   return INVALID_FRAME_INDEX;
}
//------------------------------------------------------------------------------

// Here is the cryptic old comment for this method, which doesn't make sense:
// "Just check to see if we are still selected, if not, deselect last selected"
// What it really seems to be doing is ensuring consistency of selection of
// the tracking points temporally with respect to the selection state at the
// start frame, and making sure that the 'lastSlectedTag' property is not
// set if there aren't any tracking points selected!
// Of course, I can't understand why we allow tracking points to be selected
// at some frame indices and not others! It seems like ther should be a
// single SELECTED attribute per tracking point, NOT per tracking point per
// frame!

void TrackingBoxManager::updateLastSelectedTag()
{
   if (trackingArray == NULL)
   {
      return;
   }

   // First sanitize the selected box list.
   // Can't use the foreach here because we need to remove items.
   for (auto tagIter = _selectedTrackingBoxTags.begin(); tagIter != _selectedTrackingBoxTags.end();)
   {
      if (!trackingArray->isValidTrackingBoxTag(*tagIter))
      {
         // The iterator pointing at the item to be erased becomes invalid!
         auto tempIter = tagIter;
         tagIter++;
         _selectedTrackingBoxTags.erase(tempIter);
      }
      else
      {
         tagIter++;
      }
   }

   if (_selectedTrackingBoxTags.empty())
   {
      lastSelectedTag = INVALID_TRACKING_BOX_TAG;
   }
   else if (lastSelectedTag == INVALID_TRACKING_BOX_TAG
   || !isTrackingBoxSelected(lastSelectedTag))
   {
      lastSelectedTag = _selectedTrackingBoxTags.back();
   }

   // Is this needed? It's done in the lastSelectedTag property setter if the tag actually changes
   TrackingPointStateChange.Notify();

}
//------------------------------------------------------------------------------

int TrackingBoxManager::createTrackingPoint(double x, double y, bool sel)
{
	if (trackingArray == NULL || systemAPI == NULL)
   {
		return INVALID_TRACKING_BOX_TAG;
	}

	if (!_isIgnoringMarks)
	{
		setRangeFromMarks();
	}

	if (_startFrame == INVALID_FRAME_INDEX
	|| _stopFrame ==  INVALID_FRAME_INDEX
	|| _startFrame >= _stopFrame)
	{
		theError.set("Please set valid marks before creating tracking points!");
		theError.Notify();
		_isABoxBeingDragged = false;
		_isMouseButtonDown = false;
		return INVALID_TRACKING_BOX_TAG;
	}

	int currentFrame = systemAPI->getLastFrameIndex();
	if (currentFrame != _startFrame)
	{
		theError.set("Tracking points can only be added at the first frame in the tracking range");
		theError.Notify();
		_isABoxBeingDragged = false;
		_isMouseButtonDown = false;
		return INVALID_TRACKING_BOX_TAG;
	}

	auto newTag = trackingArray->addTrackingBox(_startFrame, FPOINT(x, y));
	setUniqueSelection(newTag);

	invalidateTrackingDataIfItWasUsedForRendering();

	TrackingParameterChange.Notify();

	return newTag;
}
//------------------------------------------------------------------------------

// Translate the selected tr points by an amount [delx, dely],
// They may wander outside the clip's active rectangle
//
void TrackingBoxManager::translateSelectedTrackingPointsCurrentFrame(double delx, double dely)
{
   if (trackingArray == NULL || systemAPI == NULL)
   {
      return;
   }

   auto curFrame = systemAPI->getLastFrameIndex();
	auto atStartFrame = curFrame == _startFrame;
   auto offset = FPOINT(delx, dely);
   for (auto tag : _selectedTrackingBoxTags)
   {
      if (!trackingArray->isValidTrackingBoxTag(tag))
      {
         continue;
      }

		trackingArray->moveTrackingBoxAtFrame(tag, curFrame, offset);
   }

	invalidateTrackingDataIfItWasUsedForRendering();

	TrackingParameterChange.Notify();
   TrackingPointStateChange.Notify();
}
//------------------------------------------------------------------------------

// Delete all the selected tracking boxes
void TrackingBoxManager::deleteSelectedTrackingPoints()
{
	if (trackingArray == NULL)
   {
      return;
   }

   for (auto tag : _selectedTrackingBoxTags)
   {
      if (!trackingArray->isValidTrackingBoxTag(tag))
      {
         continue;
      }

      trackingArray->removeTrackingBox(tag);
   }

   lastSelectedTag = INVALID_TRACKING_BOX_TAG;
   _selectedTrackingBoxTags.clear();
	invalidateTrackingDataIfItWasUsedForRendering();

	TrackingPointStateChange.Notify();
   refreshTrackingPointsFrame();
}
//------------------------------------------------------------------------------
int TrackingBoxManager::getNumberOfTrackingBoxes()
{
	return (trackingArray == NULL) ? 0 : trackingArray->getNumberOfTrackingBoxes();
}

//------------------------------------------------------------------------------

// Setter for the 'lastSelectedTag' property - don't call directly
void TrackingBoxManager::setLastSelectedTag(int newLastSelectedTag)
{
   if (_lastSelectedTag != newLastSelectedTag)
   {
      _lastSelectedTag = newLastSelectedTag;
      TrackingPointStateChange.Notify();
   }
}
//------------------------------------------------------------------------------

// Setter for the 'displayTrackingPoints' property - don't call directly
void TrackingBoxManager::setDisplayTrackingPoints(bool newDisp)
{
   if (newDisp != _displayTrackingPoints)
   {
      _displayTrackingPoints = newDisp;
      refreshTrackingPointsFrame();
   }
}
//------------------------------------------------------------------------------

void TrackingBoxManager::setUniqueSelection(int tag)
{
    unselectAllTrackingPoints();
    selectTrackingBox(tag);
    lastSelectedTag = tag;
	 TrackingPointStateChange.Notify();

	 if (systemAPI != NULL)
	 {
		systemAPI->refreshFrameCached();
	 }
}
//------------------------------------------------------------------------------
