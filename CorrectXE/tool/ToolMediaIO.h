// ToolMediaIO.h: interface for the classes:
//                      CMediaReaderTool
//                      CMediaReaderToolProc
//                      CMediaWriterTool
//                      CMediaWriterToolProc
//
/*
$Header: /usr/local/filmroot/tool/include/ToolMediaIO.h,v 1.13.2.2 2009/03/09 07:59:05 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef TOOLMEDIAIOH
#define TOOLMEDIAIOH

#include "IniFile.h"   // bring in lots of core-code stuff
#include "ToolObject.h"
#include "ClipSharedPtr.h"

#include <list>
using std::list;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBufferPool;
class CClip;
class CExtractor;
class CImageFormat;
class CRenderDestinationClip;
class CVideoFrameList;

//////////////////////////////////////////////////////////////////////
// Media Reader and Writer Tool Numbers (16-bit number that uniquely
// identifies these tools)
#define MEDIA_READER_TOOL_NUMBER 3
#define MEDIA_WRITER_TOOL_NUMBER 4

// Dummy commands for the Media Reader and Writer dummy command table
// (These tools do not have a GUI and do not accept user input)
#define MEDIA_READER_CMD_DUMMY   1
#define MEDIA_WRITER_CMD_DUMMY   1

//////////////////////////////////////////////////////////////////////

class CMediaReaderTool : public CToolObject
{
public:
   CMediaReaderTool(const string &newToolName);
   virtual ~CMediaReaderTool();

   int toolInitialize(CToolSystemInterface *newSystemAPI);
   int toolShutdown();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   CToolProcessor* makeToolProcessorInstance(
                                         const bool *newEmergencyStopFlagPtr);
};

//////////////////////////////////////////////////////////////////////

class CMediaWriterTool : public CToolObject
{
public:
   CMediaWriterTool(const string &newToolName);
   virtual ~CMediaWriterTool();

   int toolInitialize(CToolSystemInterface *newSystemAPI);
   int toolShutdown();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);
};

//////////////////////////////////////////////////////////////////////

// Number of native format frames to allocate for read media buffers
#define DISK_READER_NATIVE_FRAME_COUNT 2

//////////////////////////////////////////////////////////////////////

enum EToolMediaIOConversionState
{
   CONVERT_STATE_WAIT_FOR_NEW_FRAME,
   CONVERT_STATE_WAIT_FOR_BUFFERS,
   CONVERT_STATE_CONVERT,
   CONVERT_STATE_PUT_OUTPUT
};

enum EToolMediaIOReaderState
{
   READER_STATE_WAIT_FOR_NEW_FRAME_INDEX,
   READER_STATE_WAIT_FOR_BUFFERS,
   READER_STATE_OUTPUT_CACHED_BUFFER
};

//////////////////////////////////////////////////////////////////////

class CFrameRingBuffer
{
public:
   struct SRingSlot
      {
      int frameIndex;
      MTI_UINT8 **fieldBuffer;
      int errorFlag;
      bool endOfMaterial;
      CToolFrameBuffer *alreadyConvertedToolFrameBuffer;
      };

public:
   CFrameRingBuffer();
   virtual ~CFrameRingBuffer();

   void InitRing(int newFrameCount, int newFieldCount, int newFieldByteCount,
                 CBufferPool *newBufferAllocator);
   void DeleteRing();

   int GetBufferCount() const;
   int GetFrameCount() const;
   int GetFieldCount() const;
   int GetFieldByteCount() const;

   int AllocateFieldBuffers();
   void FreeFieldBuffers();

   bool IsSlotAvailable();
   SRingSlot* GetAvailableSlot();
   void PutAvailableSlot();

   bool Empty();
   SRingSlot* GetFrameBuffer();
   void ReleaseFrameBuffer();

   void Flush();

private:
   int frameCount;
   int fieldCount;
   int fieldByteCount;

   SRingSlot *slotRing;

   int headIndex;
   int tailIndex;

   CBufferPool *bufferAllocator;

};

//////////////////////////////////////////////////////////////////////

class CMediaIOConfig : public CToolIOConfig
{
public:
   CMediaIOConfig(int newInPortCount, int newOutPortCount);
   virtual ~CMediaIOConfig();

   bool useNavCurrentClip;    // If true, use the Navigator's current
                              // clip, proxy & framing and ignore clip
                              // parameters here
   string binPath;            // If this is an empty string, then
                              // assume clipName contains the path.
   string clipName;
   int videoProxyIndex;
   int videoFramingIndex;
};

class CMediaReaderIOConfig : public CMediaIOConfig
{
public:
   CMediaReaderIOConfig();
   virtual ~CMediaReaderIOConfig();
};

class CMediaWriterIOConfig : public CMediaIOConfig
{
public:
   CMediaWriterIOConfig();
   virtual ~CMediaWriterIOConfig();
};

//////////////////////////////////////////////////////////////////////

class CMediaReaderToolProc : public CToolProcessor
{
public:
   CMediaReaderToolProc(int newToolNumber, const string& newToolName,
                        CToolSystemInterface *newSystemAPI,
                        const bool *newEmergencyStopFlagPtr);
   virtual ~CMediaReaderToolProc();

   int LoadClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
                int newVideoFramingIndex);

   int UnloadClip();

   void EnableRandomAccess(bool enable);
   void RequestFrame(int port, int frameIndex);
   void RemoveRequest(int port, int frameIndex);

   void EnableSequentialAccess(bool enable);
   void SetFrameIndexRange(int newInFrame, int newOutFrame);

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   int AssignAllBufferAllocators();
   void ReleaseAllBufferAllocators();

   // Dummy functions needed to keep CToolProcessor happy
   int SetParameters(CToolParameters *toolParams) {return -1;};
   int BeginProcessing(SToolProcessingData &procData) {return -1;};
   int EndProcessing(SToolProcessingData &procData) {return -1;};
   int BeginIteration(SToolProcessingData &procData) {return -1;};
   int EndIteration(SToolProcessingData &procData) {return -1;};
   int DoProcess(SToolProcessingData &procData) {return -1;};
   int GetIterationCount(SToolProcessingData &procData) {return -1;};

private:

   struct SFrameRequest
   {
      int frameIndex;
      bool unprocessedRequest;
   };

   typedef list<SFrameRequest> FrameRequestList;
   typedef list<SFrameRequest>::iterator FrameRequestListIterator;

private:
   static void ConverterThread(void *vpMediaReader, void *vpBThread);
   void NativeToInternalConverter(void *vpBThread);
   static void MediaReaderThread(void *vpMediaReader, void *vpBThread);
   void MediaReaderLoop(void *vpBThread);

   // Frame Request List functions
   void AddFrameRequest(int frameIndex);
   int GetNextRequestedFrameToRead();
   void ClearRequestList();
   int GenerateNextFrameIndex();
   void InitSequentialAccess();

private:
   ClipSharedPtr clip;
   int videoProxyIndex;
   int videoFramingIndex;
   CVideoFrameList *frameList;
   const CImageFormat *nativeImageFormat;
   bool clipLoaded;

   CImageFormat *internalImageFormat;

   CFrameRingBuffer frameRingBuffer;

   EToolMediaIOReaderState readerState;
   EToolMediaIOConversionState formatConversionState;
   CExtractor *extractor;

   bool sequentialAccessEnabled;
   int sequentialFrameIndex;
   int inFrameIndex;                                     
   int outFrameIndex;

   bool randomAccessEnabled;
   FrameRequestList frameRequestList;
   CThreadLock frameRequestListThreadLock;

   void *vpMediaReaderThread;
   void *vpConverterThread;
   bool exitThreadFlag;
};

//////////////////////////////////////////////////////////////////////

class CMediaWriterToolProc : public CToolProcessor
{
public:
   CMediaWriterToolProc(int newToolNumber, const string& newToolName,
                        CToolSystemInterface *newSystemAPI,
                        const bool *newEmergencyStopFlagPtr);
   virtual ~CMediaWriterToolProc();

   int LoadSrcClip(int newVideoProxyIndex,
                   int newVideoFramingIndex);
   int LoadDstClip(int srcFrameIndex);

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   int AssignAllBufferAllocators();
   void ReleaseAllBufferAllocators();

   // Dummy functions needed to keep CToolProcessor happy
   int SetParameters(CToolParameters *toolParams) {return -1;};
   int BeginProcessing(SToolProcessingData &procData) {return -1;};
   int EndProcessing(SToolProcessingData &procData) {return -1;};
   int BeginIteration(SToolProcessingData &procData) {return -1;};
   int EndIteration(SToolProcessingData &procData) {return -1;};
   int DoProcess(SToolProcessingData &procData) {return -1;};
   int GetIterationCount(SToolProcessingData &procData) {return -1;};

   int WriteFrameSynchronous(int inPortIndex, CToolFrameBuffer *frameBuffer);
   int WriteFrameAsynchronous(int inPortIndex, CToolFrameBuffer *frameBuffer);

private:
   static void ConverterThread(void *vpMediaWriter, void *vpBThread);
   void InternalToNativeConverter(void *vpBThread);
   static void MediaWriterThread(void *vpMediaWriter, void *vpBThread);
   void MediaWriterLoop(void *vpBThread);
   int CreateDstClip(CRenderDestinationClip *renderDest);
   int WriteFrameSynchronousInternal(int inPortIndex, CToolFrameBuffer *frameBuffer);

   bool dstClipLoaded;
   ClipSharedPtr dstClip;
   int dstVideoProxyIndex;
   CVideoFrameList *dstFrameList;
   const CImageFormat *dstImageFormat;
   int dstFrameOffset;

   //** Dave's DPX Header Copy Hack
   ClipSharedPtr srcClip;
   CVideoFrameList *srcFrameList;
   bool enableDPXHeaderCopyHack;
   //**//

   CFrameRingBuffer frameRingBuffer;

   EToolMediaIOConversionState formatConversionState;
   CExtractor *extractor;

   void *vpMediaWriterThread;
   void *vpConverterThread;
   bool exitThreadFlag;

   CExtractor *syncWriteExtractor;
   CThreadLock syncWriteThreadLock;
};

//////////////////////////////////////////////////////////////////////

#endif //  #if !defined(TOOLMEDIAIO_H)


