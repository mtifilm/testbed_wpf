// UserInput.cpp: implementation of the CUserInput class.
//
/*
 $Header: /usr/local/filmroot/tool/ToolObj/UserInput.cpp,v 1.2 2006/01/13 18:21:07 mlm Exp $
 */
//////////////////////////////////////////////////////////////////////

#include "UserInput.h"
#include "MTIKeyDef.h"
#include <string.h>
#include <stdio.h>  // TTT

//////////////////////////////////////////////////////////////////////
// Static Variables

static CKeyNameTable::SKeyName keyNames[] =
{
#include "MTIKeyNames.h"
};

CKeyNameTable* CUserInput::keyNameTable =
   new CKeyNameTable(keyNames, sizeof(keyNames) / sizeof(CKeyNameTable::SKeyName));

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUserInput::CUserInput()
{
	clearUserInput();
}

CUserInput::~CUserInput()
{

}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CUserInput::clearUserInput()
{
	action = USER_INPUT_ACTION_INVALID;
	key = MTK_INVALID;
	shift = USER_INPUT_SHIFT_MASK_NONE;
	windowX = windowY = 0;
	imageX = imageY = 0;
}

//////////////////////////////////////////////////////////////////////
// Comparison Operators
//////////////////////////////////////////////////////////////////////

bool CUserInput:: operator == (const CUserInput &rhs) const
{
	// Compare action type
	if (action != rhs.action)
		return false;

	// Compare shift state, but ignore mouse buttons
	if ((shift & SHIFT_STATE_MASK) != (rhs.shift & SHIFT_STATE_MASK))
		return false;

	switch (action)
	{
	case USER_INPUT_ACTION_KEY_DOWN: // Keyboard key pressed
	case USER_INPUT_ACTION_KEY_UP: // Keyboard key released
		// Compare key
		if (key == rhs.key)
			return true;
		else
			return false;
	case USER_INPUT_ACTION_MOUSE_DOUBLE_CLICK: // Mouse button pressed
	case USER_INPUT_ACTION_MOUSE_BUTTON_DOWN: // Mouse button pressed
	case USER_INPUT_ACTION_MOUSE_BUTTON_UP: // Mouse button released
		// Compare button number (mouse coordinates are ignored)
		if (key == rhs.key)
			return true;
		else
			return false;
	default:
		return false;
	}
}

bool CUserInput:: operator != (const CUserInput &rhs) const
{
	return (!(*this == rhs));
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

const CKeyNameTable* CUserInput::getKeyNameTable() const
{
	return keyNameTable;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

// Guess whether a given key (supplied as a platform-dependent KeySym or
// Virtual Key) is a shifted key.  The result is only a guess because
// of variation between keyboards, especially non-US keyboards.
// Results are platform-dependent, e.g., MS Windows version always returns
// false as there are no Virtual Keys for shifted keys such as ":"
// Note: Keys from numerical keypad are treated as distinct from regular
// keyboard keys
bool CUserInput::isKeyShifted(unsigned int key)
{
#ifdef __sgi
	// Check for upper-case alphabetic characters (relevant only for X KeySyms)
	if (key >= MTK_A && key <= MTK_Z)
		return true;

	// Check for shifted key punctuation (relevant only for X KeySyms)
	switch (key)
	{
	case MTK_ASCIITILDE: // ~  (Shift+`)
	case MTK_EXCLAMATION: // !  (Shift+1)
	case MTK_AT: // @  (Shift+2)
	case MTK_NUMBERSIGN: // #  (Shift+3)
	case MTK_DOLLAR: // $  (Shift+4)
	case MTK_PERCENT: // %  (Shift+5)
	case MTK_ASCIICIRCUM: // ^  (Shift+6)
	case MTK_AMPERSAND: // &  (Shift+7)
	case MTK_ASTERISK: // *  (Shift+8)
	case MTK_PARENLEFT: // (  (Shift+9)
	case MTK_PARENRIGHT: // )  (Shift+0)
	case MTK_UNDERSCORE: // _  (Shift+-)
	case MTK_PLUS: // +  (Shift++)
	case MTK_BRACELEFT: // {  (Shift+[)
	case MTK_BRACERIGHT: // }  (Shift+])
	case MTK_BAR: // |  (Shift+\)
	case MTK_COLON: // :  (Shift+;)
	case MTK_QUOTEDBL: // "  (Shift+')
	case MTK_LESS: // <  (Shift+,)
	case MTK_GREATER: // >  (Shift+.)
	case MTK_QUESTION: // ?  (Shift+/)
		return true;
	}
#endif // #ifdef __sgi

	// Keys that drop through above are assumed to be not shifted
	return false;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CUserInput::parseIniUserInputMap(const char *keyMapString)
{
	// The key map string must have the format:
	// KeyModifiers+KeyName KeyAction
	// where
	// KeyModifiers is one or more of the modifiers Ctrl, Alt
	// and Shift separated by the "+" character.  Modifiers must be
	// in the order of Ctrl, Alt and Shift.  Parsing is case-sensitive.
	// KeyName is a keyboard key (e.g. A, [, etc) or a key name (e.g., Up,
	// Tab, etc.).  See the file MTIKeyNames.h for the possible Key Names.
	// Parsing of a multi-character Key Name is case-insensitive
	// Key Names must be specified with shifted symbols,
	// e.g., ~!@#$%^&*()_+{}|:"<>?.  Do not use the "Shift+" modifier
	// to specify these character.  Good: "*"  Bad: "Shift+8"
	// Numerical keypad keys are distinguished from regular keyboard
	// keys, e.g. "1" and "Num1" are different
	// KeyAction is "KeyDown",  "KeyUp", "ButtonDown", or "ButtonUp";
	// case-sensitive
	//
	// The Key Modifiers, "+" characters and Key Name must not be
	// separated by white space.  Good: "Ctrl+C"  Bad: "Ctrl + C"
	// The Key Name and Key Action must be separated by one or more space
	// or tab characters.
	//
	// Examples:
	// D KeyDown
	// D KeyUp
	// Ctrl+Up KeyDown
	// Ctrl+Shift+8 KeyDown
	// Shift+LButton ButtonDown

	// Initialize the this CUserInput instance
	clearUserInput();

	// Copy input string to working copy, changing all alpha characters to
	// lower-case to make comparisons easier
	const char *srcString = keyMapString; // Source working string pointer
	const size_t maxKeyMapLength = 511;
	char parseStringCopy[maxKeyMapLength + 1]; // Copy of input string
	char *parseString = parseStringCopy; // Copy working string pointer
	char srcChar;
	size_t keyMapLength = 0;
	while ((srcChar = *srcString) != 0)
	{
		if (srcChar >= 'A' && srcChar <= 'Z')
		{
			*parseString = srcChar - 'A' + 'a';
		}
		else
		{
			*parseString = srcChar;
		}

		// Check overrun of copy buffer
		if (++keyMapLength > maxKeyMapLength)
		{
			// ERROR: key map string is too long
			return -3;
		}

		// Advance string pointers
		++srcString;
		++parseString;
	}
	*parseString = 0; // Terminate copy
	parseString = parseStringCopy; // Reset working string pointer to copy

	// Skip past leading space and tab characters
	char parseChar;
	while ((parseChar = *parseString) == ' ' || parseChar == '\t')
	{
		if (parseChar == 0 || parseChar == '\n')
		{
			// Error: Unexpected end-of-line
			return -1;
		}
		++parseString;
	}

	// Look for Key Modifiers Ctrl, Alt and Shift in key map string
	const char controlModifier[] = "ctrl+";
	const char altModifier[] = "alt+";
	const char shiftModifier[] = "shift+";
	char *keyModifier;
	if ((keyModifier = strstr(parseString, controlModifier)) != 0)
	{
		// Ctrl is in the key map string
		shift |= USER_INPUT_SHIFT_MASK_CONTROL;
		parseString = keyModifier + strlen(controlModifier);
	}
	if ((keyModifier = strstr(parseString, altModifier)) != 0)
	{
		// Alt is in the key map string
		shift |= USER_INPUT_SHIFT_MASK_ALT;
		parseString = keyModifier + strlen(altModifier);
	}
	if ((keyModifier = strstr(parseString, shiftModifier)) != 0)
	{
		// Shift is in the key map string
		shift |= USER_INPUT_SHIFT_MASK_SHIFT;
		parseString = keyModifier + strlen(shiftModifier);
	}

	// Extract the Key Name, which is assumed to be all characters
	// after the Modifiers and before white space
	const size_t maxKeyNameLength = 127;
	char keyName[maxKeyNameLength + 1];
	char *keyNamePtr = keyName;
	size_t keyNameLength = 0;
	while ((parseChar = *parseString) != ' ' && parseChar != '\t')
	{
		if (parseChar == 0 || parseChar == '\n')
		{
			// Error: Missing Key Name terminator and Key Action
			return -2;
		}

		if (++keyNameLength > maxKeyNameLength)
		{
			// Error: Key Name too long
			return -3;
		}

		// Copy a character of the Key Name
		*keyNamePtr++ = parseChar;
		++parseString;
	}
	// Terminate the copied Key Name
	*keyNamePtr = 0;

	// Determine the key symbol that corresponds to the key name.
	// The method used to determine the key symbol depends on the host GUI
	// system (X or MS Windows)
	int keySymbol = MTK_INVALID;
	if (strlen(keyName) == 1)
	{
		// Single-character key name
#ifdef __sgi
		// X Windows has distinct key symbols for unshifted and shifted keys
		// Lookup the key name in the key name table to find the key symbol
		keySymbol = keyNameTable->findKeySymbol(keyName);

		// If key map string is "Shift+alpha" then change key symbol
		// to upper-case equivalent and clear shift bit
		if (keySymbol >= MTK_a && keySymbol <= MTK_z && shift & USER_INPUT_SHIFT_MASK_SHIFT)
		{
			keySymbol = keySymbol - MTK_a + MTK_A;
			shift = shift&~((unsigned int)(USER_INPUT_SHIFT_MASK_SHIFT));
		}
#endif // #ifdef __sgi

#ifdef __BORLANDC__
		// MS Windows does not have key symbols for shifted characters
		// Use Win32 function VkKeyScan to get both the key symbol and
		// control/alt/shift state for the single-character key name
		// for the current keyboard.
		TCHAR win32Char = *keyName;
		SHORT vkResult = VkKeyScan(win32Char);
		keySymbol = vkResult & 0x00FF; // Key symbol is in lower byte
		if (keySymbol == -1)
		{
			// VkKeyScan could not return a Virtual Key for the input character
			keySymbol = MTK_INVALID;
		}
		else
		{
			// Adjust the CUserInput shift flag based on shift, control and alt
			// bit flags in the high-order byte of the VkKeyScan return value
			if (vkResult & 0x0200)
			{
				// Control Key
				shift |= USER_INPUT_SHIFT_MASK_CONTROL;
			}
			if (vkResult & 0x0400)
			{
				// Alt Key
				shift |= USER_INPUT_SHIFT_MASK_ALT;
			}
			if (vkResult & 0x0100)
			{
				// Shift Key
				shift |= USER_INPUT_SHIFT_MASK_SHIFT;
			}
		}

#endif // #ifdef __BORLANDC__

	}
	else
	{
		// Multi-character key name
		// Lookup the key name in the key name table to find the key symbol
		keySymbol = keyNameTable->findKeySymbol(keyName);
	}

	if (keySymbol == MTK_INVALID)
	{
		// printf("TTT CUserInput::parseIniUserInputMap Key name not found in key name table %s %u\n", keyMapString, keySymbol);
		// Error: Key name not found in key name table
		return -4;
	}

	// Found key symbol for the key name, so set in this CUserInput
	key = keySymbol;

	// Look for Key Action in parse string
	if (keySymbol != MTK_LBUTTON && keySymbol != MTK_RBUTTON && keySymbol != MTK_MBUTTON)
	{
		// Look for Keyboard Key Action string
		if (strstr(parseString, "keydown") != 0)
		{
			action = USER_INPUT_ACTION_KEY_DOWN;
		}
		else if (strstr(parseString, "keyup") != 0)
		{
			action = USER_INPUT_ACTION_KEY_UP;
		}
		else
		{
			// Error: Could not find the Key Action
			return -5;
		}
	}
	else
	{
		// Look for Mouse Button Action string
		if (strstr(parseString, "buttondown") != 0)
		{
			action = USER_INPUT_ACTION_MOUSE_BUTTON_DOWN;
		}
		else if (strstr(parseString, "buttonup") != 0)
		{
			action = USER_INPUT_ACTION_MOUSE_BUTTON_UP;
		}
        else if (strstr(parseString, "doubleclick") != 0)
		{
			action = USER_INPUT_ACTION_MOUSE_DOUBLE_CLICK;
		}
		else
		{
			// Error: Could not find the Key Action
			return -6;
		}
	}

	// Parse succeeded
	return 0;
}

int CUserInput::makeIniUserInputMap(char *keyMapString)
{
	if (keyMapString == 0)
	{
		// Error: Null pointer
		return -1;
	}

	// Check user input action
	if (action != USER_INPUT_ACTION_KEY_DOWN && action != USER_INPUT_ACTION_KEY_UP && action !=
	   USER_INPUT_ACTION_MOUSE_BUTTON_DOWN && action != USER_INPUT_ACTION_MOUSE_BUTTON_UP)
	{
		// Error: Unexpected User Input Action type
		return -2;
	}

	// Clear caller's results string
	*keyMapString = 0;

	// Start with Modifier Keys Ctrl, Alt and Shift
	if (shift & USER_INPUT_SHIFT_MASK_CONTROL)
	{
		strcat(keyMapString, "Ctrl+");
	}
	if (shift & USER_INPUT_SHIFT_MASK_ALT)
	{
		strcat(keyMapString, "Alt+");
	}
	if (shift & USER_INPUT_SHIFT_MASK_SHIFT)
	{
		strcat(keyMapString, "Shift+");
	}

	// Next add the Key/Button Name
	// Lookup the key symbol in the key name table to find the key name
	const char* keyName = keyNameTable->findKeyName(key);
	if (keyName == 0)
	{
		// Error: Could not find the key symbol
		return -3;
	}

	// Append the Key/Button Name to the caller's output string
	strcat(keyMapString, keyName);

	// Append a Space character to terminate the Key/Button Name
	strcat(keyMapString, " ");

	// Finally, append the User Input Action
	if (action == USER_INPUT_ACTION_KEY_DOWN)
	{
		strcat(keyMapString, "KeyDown");
	}
	else if (action == USER_INPUT_ACTION_KEY_UP)
	{
		strcat(keyMapString, "KeyUp");
	}
	else if (action == USER_INPUT_ACTION_MOUSE_BUTTON_DOWN)
	{
		strcat(keyMapString, "ButtonDown");
	}
	else if (action == USER_INPUT_ACTION_MOUSE_BUTTON_UP)
	{
		strcat(keyMapString, "ButtonUp");
	}
    else if (action == USER_INPUT_ACTION_MOUSE_DOUBLE_CLICK)
	{
		strcat(keyMapString, "DoubleClick");
	}


	return 0;
}

//////////////////////////////////////////////////////////////////////
// Platform-Dependent Conversion Functions
//////////////////////////////////////////////////////////////////////

#if defined(__BORLANDC__)

unsigned int CUserInput::convertFromBorlandShift(TShiftState &borlandShift)
{
	unsigned int newShift = 0; // Start with nothing set

	// Shift, Alt and Ctrl Keys
	if (borlandShift.Contains(ssShift))
		newShift |= USER_INPUT_SHIFT_MASK_SHIFT;
	if (borlandShift.Contains(ssAlt))
		newShift |= USER_INPUT_SHIFT_MASK_ALT;
	if (borlandShift.Contains(ssCommand))
		newShift |= USER_INPUT_SHIFT_MASK_PHYS_ALT;   // HACK - alt is really down
	if (borlandShift.Contains(ssCtrl))
		newShift |= USER_INPUT_SHIFT_MASK_CONTROL;

	// Left, Right and Middle Mouse Buttons
	if (borlandShift.Contains(ssLeft))
		newShift |= USER_INPUT_SHIFT_MASK_LBUTTON;
	if (borlandShift.Contains(ssRight))
		newShift |= USER_INPUT_SHIFT_MASK_RBUTTON;
	if (borlandShift.Contains(ssMiddle))
		newShift |= USER_INPUT_SHIFT_MASK_MBUTTON;

	return newShift;
}

void CUserInput::setFromKeyEvent(EUserInputAction newAction, WORD &borlandKey, TShiftState &borlandShift)
{
	clearUserInput();

	key = borlandKey;
	shift = convertFromBorlandShift(borlandShift);

	action = newAction;
}

void CUserInput::setFromMouseEvent(EUserInputAction newAction, TMouseButton &borlandMouseButton,
   TShiftState &borlandShift, int x, int y)
{
	clearUserInput();

	switch (borlandMouseButton)
	{
	case mbLeft:
		key = MTK_LBUTTON;
		break;
	case mbRight:
		key = MTK_RBUTTON;
		break;
	case mbMiddle:
		key = MTK_MBUTTON;
		break;
	default:
		// Do nothing 'til you hear from me...
		break;
	}

	// Coordinates of mouse when button was pressed/released
	windowX = x;
	windowY = y;

	// TBD: Convert from window to image coordinates
	imageX = 0;
	imageY = 0;

	shift = convertFromBorlandShift(borlandShift);

	action = newAction;
}

void CUserInput::setFromMouseMoveEvent(TShiftState &borlandShift, int x, int y)
{
	clearUserInput();

	// Mouse coordinates
	windowX = x;
	windowY = y;

	// TBD: Convert from window to image coordinates
	imageX = 0;
	imageY = 0;

	shift = convertFromBorlandShift(borlandShift);

	action = USER_INPUT_ACTION_MOUSE_MOVE;
}

#endif // #if defined(__BORLANDC__)

//////////////////////////////////////////////////////////////////////
// CKeyNameTable Class Constructor/Destructor
//////////////////////////////////////////////////////////////////////

CKeyNameTable::CKeyNameTable(SKeyName *newKeyTable, int newKeyTableSize)
{
	keyTable = newKeyTable;
	keyTableSize = newKeyTableSize;
}

CKeyNameTable::~CKeyNameTable()
{

}

//////////////////////////////////////////////////////////////////////
// Key Symbol and Key Name Table Lookup Functions
//////////////////////////////////////////////////////////////////////

const char* CKeyNameTable::findKeyName(int targetKeySym) const
{
	// Search for target key symbol in table
	// Note: Brute-force linear search.  Could be made faster,
	// but I do not think it will be called very often
	for (int i = 0; i < keyTableSize; ++i)
	{
		if (keyTable[i].keyCode == targetKeySym)
			return keyTable[i].keyName;
	}

	// Key symbol was not found, return Null pointer
	return 0;
}

int CKeyNameTable::findKeySymbol(const char *targetKeyName) const
{
	// Search for target key name in table.  Assumes all alpha characters
	// are lower-case
	// Note: Brute-force linear search.  Could be made faster,
	// but I do not think it will be called very often
	for (int i = 0; i < keyTableSize; ++i)
	{
		if (strcmp(keyTable[i].keyName, targetKeyName) == 0)
			return keyTable[i].keyCode;
	}

	// Key name was not found, return invalid key symbol
	return MTK_INVALID;
}
