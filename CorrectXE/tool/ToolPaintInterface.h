
#if !defined(TOOL_PAINT_INTERFACE_H)
#define TOOL_PAINT_INTERFACE_H

#ifdef __BORLANDC__
#include <vcl.h>
#endif // #ifdef __BORLANDC_

#ifdef __sgi
#include <Xm/Xm.h>
#endif // #ifdef __sgi

#include "machine.h"

class CToolObject;

class CToolPaintInterface
{

public:

   CToolPaintInterface() {};
   CToolPaintInterface(CToolObject *) {};
   virtual ~CToolPaintInterface() {};

public:

   virtual void setTransform(double, double,
                             double, double,
                             double, double) = 0;

   virtual void setImportFrameMode(int) = 0;

   virtual void setImportFrameTimecode(int) = 0;

   virtual void setTargetFrameTimecode(int) = 0;

   virtual void setImportFrameOffset(int) = 0;

   virtual void drawProxy(unsigned int *) = 0;

   virtual void clearStrokeStack() = 0;

   virtual void setStrokeEntry(int, int) = 0;

   virtual void setPickedColor(float *) = 0;

   virtual void onLoadNewTargetFrame() = 0;

   virtual void setFrameLoadTimer() = 0;
};

#endif

