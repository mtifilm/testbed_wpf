// ToolCommand.cpp: implementation of the CToolCommand class.
//
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolCommand.cpp,v 1.2 2006/01/13 18:21:07 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolCommand.h"
#include "ToolObject.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolCommand::CToolCommand(CToolObject *newToolObject, int newCommandNumber)
: toolObject(newToolObject), commandNumber(newCommandNumber)
{
}

CToolCommand::~CToolCommand()
{

}

//////////////////////////////////////////////////////////////////////
// Command Execution Function
//////////////////////////////////////////////////////////////////////

int CToolCommand::execute()
{
   if (toolObject == 0)
      {
      // Null ToolObject pointer
      return (-1);
      }

   return (toolObject->onToolCommand(*this));
}

int CToolCommand::getCommandNumber()
{
   return commandNumber;
}

