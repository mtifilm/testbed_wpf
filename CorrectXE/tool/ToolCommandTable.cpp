// ToolCommandTable.cpp: implementation of the CToolCommandTable class.
//
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolCommandTable.cpp,v 1.2 2006/01/13 18:21:07 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include <string.h>   // C string functions

#include "ToolCommandTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolCommandTable::CToolCommandTable(int newCommandCount, 
                                     STableEntry *newTableEntries)
: commandCount(newCommandCount),
  tableEntries(newTableEntries)
{
   if (newCommandCount < 1)
      {
      commandCount = 0;
      tableEntries = 0;
      }
}

CToolCommandTable::~CToolCommandTable()
{

}

//////////////////////////////////////////////////////////////////////
// Command Table Search Functions
//////////////////////////////////////////////////////////////////////

// ===================================================================
//
// Function:    findCommandNumber
//
// Description: Get the Command Number that matches the given Command
//              Name.  Command Name comparison is case-sensitive.
//              Searches the Command Table
//
// Arguments:   char* targetCommandName   Command Name used as key to
//                                        search the Command Table     
//
// Returns:     Command Number if matching Command Name was found.
//              -1 if search failed.        
//
// ===================================================================
int CToolCommandTable::findCommandNumber(const char *targetCommandName) const
{
   // Iterate through Command Table to search for case-sensitive 
   // match on Command Name
   for (int i = 0; i < commandCount; ++i)
      {
      if (strcmp(targetCommandName, tableEntries[i].commandName) == 0)
         {
         // Found matching Command Name, so return Command Number
         return (tableEntries[i].commandNumber);
         }
      }

   // Command Name was not found in the Command Table, so return -1 to
   // indicate search failure.
   return (-1);
}

// ===================================================================
//
// Function:    findCommandName
//
// Description: Get the Command Name that matches the given Command
//              Number. Searches the Command Table. 
//
// Arguments:   int targetCommandNumber    Command Number used as key
//                                         to search the Command Table  
//
// Returns:     Constant pointer to Command Name C-style string. 
//              NULL pointer if the search fails.        
//
// ===================================================================
const char* CToolCommandTable::findCommandName(int targetCommandNumber) const
{
   // Iterate through Command Table to search for matching Command Number
   for (int i = 0; i < commandCount; ++i)
      {
      if (targetCommandNumber == tableEntries[i].commandNumber)
         {
         // Found matching Command Number, so return pointer
         // to Command Name
         return (tableEntries[i].commandName);
         }
      }

   // Command Number was not found in the Command Table, so return NULL
   // pointer
   return (0);
}
