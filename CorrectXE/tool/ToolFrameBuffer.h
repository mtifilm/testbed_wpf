#ifndef ToolFrameBufferH
#define ToolFrameBufferH

// ToolFrameBuffer.h: interface for the CToolFrameBuffer and CToolFrameQueue
//                    classes.
//
// CToolFrameBuffer and CToolFrameQueue are used to pass frame buffers
// to and from tools where the frame buffers are in the internal format
// (16-bit 4:4:4 progressive)
//
/*
$Header: /usr/local/filmroot/tool/include/ToolFrameBuffer.h,v 1.13.2.6 2009/08/14 10:01:32 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "IniFile.h"   // bring in lots of core-code stuff

#include <string>
#include <vector>
#include <list>
using std::string;
using std::vector;
using std::list;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBufferPool;
class CExtractor;
class CImageFormat;
class CPixelRegionList;
class CSaveRestore;
class CVideoFrame;
class CVideoFrameList;

//////////////////////////////////////////////////////////////////////

class CToolFrameBuffer
{
public:
#ifdef _NOT_IMPLEMENTED_
   CToolFrameBuffer();
#endif
   CToolFrameBuffer(const CImageFormat *newImageFormat,
                    CBufferPool *newFrameBufferAllocator);
   CToolFrameBuffer(const CToolFrameBuffer &srcFrameBuffer);
   virtual ~CToolFrameBuffer();

#ifdef _NOT_IMPLEMENTED_
   int Init(const CToolFrameBuffer &src);
   int Init(CImageFormat *newImageFormat, CBufferPool *newFrameBufferAllocator,
            int newInvisibleFieldCount);
#endif
   int InitFromClip(CVideoFrameList& srcFrameList, int newFrameIndex,
                    bool useInvisibleFields);

   int ConvertFromNativeFormat(CExtractor *extractor,
                               const CImageFormat *srcImageFormat,
                               MTI_UINT8** srcFields);
   int ConvertToNativeFormat(CExtractor *extractor,
                             const CImageFormat *dstImageFormat,
                             MTI_UINT8** dstFields);

   void SetDummyImage();

   bool CopyImage(const CToolFrameBuffer &srcFrameBuffer, bool copyVisible,
                  bool copyInvisible);

   void CopyAllImageBitsOnly(const CToolFrameBuffer &srcFrameBuffer);

	bool ContainsFloatData() const;
   bool IsEndOfMaterial() const;
   int GetErrorFlag() const;
	int GetFrameIndex() const;
	bool IsForceWrite() const;
   bool IsDontCache() const;
   MTI_UINT16* GetVisibleFrameBufferPtr() const;
   CPixelRegionList* GetOriginalValuesPixelRegionListPtr();
   CPixelRegionList* GetNewPixelsPixelRegionListPtr();

   bool IsPixelsChanged();

   void SetErrorFlag(int newErrorFlag);
   void SetEndOfMaterial();
   void SetFrameIndex(int newFrameIndex);
   void SetForceWrite(bool newForceWrite);
   void SetDontCache(bool newDontCache);

   int AllocateAllFrameBuffers();

   // Functions to release buffers back to frame buffer pool
   void ReleaseAllBuffers();

   int SaveToHistory(CSaveRestore &saveRestore, int toolNumber,
                     const string &toolString, int *saveCount);
   int CopyParentHistoryToVersion(CSaveRestore &saveRestore);
   int RestoreFromHistory(CSaveRestore &saveRestore,
                          RECT *filterRect, POINT *filterLasso,
                          BEZIER_POINT *filterBezier,
                          CPixelRegionList *filterPixelRegion);

   void AppendNewPixels();
   void UpdateInvisibleFields();
   void ClearOriginalValues();
   void ClearNewPixels();

	// added by mbraca to consolidate the two operations:
	void AppendNewPixelsAndClearOriginalValues();

	int GetAllocSize();

private:
   int GetInvisibleFieldCount() const;

   int AllocateVisibleFrameBuffer();
   int AllocateInvisibleFieldBuffers();

	void ReleaseVisibleFrameBuffer();
   void ReleaseInvisibleFieldBuffers();

   int  RestoreFromHistoryInternal(CSaveRestore &saveRestore);

private:
   struct SInvisibleFieldBuffer
      {
      MTI_UINT16 *fieldBuffer;
      int visibleSiblingFieldIndex;
      int srcVisibleFieldCount;
      };
   typedef vector<SInvisibleFieldBuffer> InvisibleFieldList;

private:
   int frameIndex;                   // Frame index, of the frame within
                                     // the source or destination frame list

   int errorFlag;                    // Error code that is set when either
                                     // the DiskReader fails or if the
                                     // tool has an error on this frame.
                                     // A frame with an error probably does
                                     // not contain valid pixel data, but
                                     // is passed along to the tools so
                                     // that they can fail gracefully and
                                     // we do not get a pipeline stall.
                                     // Tools may look at this error code
                                     // to make sure the frame data is valid.
   bool endOfMaterial;               // Flag that indicates that this is a
                                     // dummy frame that marks the end of
                                     // material

   MTI_UINT16 *visibleFrameBuffer;   // Ptr to visible frame buffer
                                     // allocated by frameBufferAllocator

   InvisibleFieldList invisibleFieldList;

   const CImageFormat *frameImageFormat; // Image format of the internal format
                                       // i.e., 16-bit, 4:4:4, progressive

   bool srcInterlaced;

	CBufferPool *frameBufferAllocator;  // Ptr to frame buffer allocator that
                                       // is used to allocate and deallocate
                                       // the visible frame and invisible field
                                       // buffers

   CPixelRegionList *originalValues;
   CPixelRegionList *newPixels;     // Locations of changed pixel values

   bool forceWrite;                 // Force a write in the event that the
                                    // newPixels list is empty (full frame
                                    // tools don't use the history mechanism)

   bool dontCache;                  // This is a duplicate frame buffer - only
                                    // the original should be cached!

};

// -------------------------------------------------------------------

// Thread-safe queue of CToolFrameBuffer pointers

class CToolFrameQueue
{
public:
   CToolFrameQueue();
   virtual ~CToolFrameQueue();

   bool PutFrame(CToolFrameBuffer *frameBuffer);
   CToolFrameBuffer* GetFrame();
   CToolFrameBuffer* GetFrame(int frameIndex);

   const CImageFormat* GetImageFormat() const;
	int GetMaxEntryCount() const;
   void SetImageFormat(const CImageFormat *newImageFormat);

   void SetMaxEntryCount(int newMaxEntryCount);
   bool Full();
	bool Empty();               // true if queue is empty
   bool Check(int frameIndex); // true if frame is present in queue
   void Flush();               // Remove all entries from queue and
                               // deallocate all buffers

public:
   typedef list<CToolFrameBuffer*> ToolFrameList;
	typedef list<CToolFrameBuffer*>::iterator ToolFrameListIterator;

private:
   ToolFrameListIterator findFrameIndex(int frameIndex);

private:
	ToolFrameList frameList;

   const CImageFormat *imageFormat;

   CThreadLock threadLock;       // To make access thread-safe

   int maxEntryCount;            // Number of entries allowed in the queue
                                 // If 0, then unlimited entries
	int entryCount;               // Current number of entries

	friend std::ostream& operator<< (std::ostream& stream, const CToolFrameQueue& tfq);
};

std::ostream& operator<< (std::ostream& stream, const CToolFrameQueue& tfq);

//////////////////////////////////////////////////////////////////////

class CToolFrameCache
{
	CToolFrameQueue cachedFrames;
   bool enabledFlag;

public:
   CToolFrameCache() : enabledFlag(false) { };

   void EnableCaching(bool newEnabledFlag);
   void AddFrame(CToolFrameBuffer *buffer);
   CToolFrameBuffer *RetrieveFrame(int frameIndex);
   CToolFrameBuffer *GetOldestFrame();
   void Clear();
	friend std::ostream& operator<< (std::ostream& stream, const CToolFrameCache& tfc);
};

std::ostream& operator<< (std::ostream& stream, const CToolFrameCache& tfc);

//////////////////////////////////////////////////////////////////////

#endif // #if !defined(TOOLFRAMEBUFFER_H)



