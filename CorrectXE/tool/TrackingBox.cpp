//---------------------------------------------------------------------------
#include "TrackingBox.h"
#include "MathFunctions.h"
#include "PDL.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

string TrackPointList::toString() const
{
   MTIostringstream os;
   os << "[";
   for (auto point : *this) os << point.toString();
   os << "]";
   return os.str();
}

//---------------------------------------------------------------------------

TrackingBox::TrackingBox()
: _startFrame(0)
{
}
//---------------------------------------------------------------------------

TrackingBox::TrackingBox(int startFrame, FPOINT locationAtStartFrame)
: _startFrame(startFrame)
{
   TrackingPosition position(locationAtStartFrame);
   _track.push_back(position);
}
//---------------------------------------------------------------------------

bool TrackingBox::isValidAtFrame(int frame) const
{
	if (frame < _startFrame)
	{
		TRACE_3(errout << "frame " << frame << " < " << _startFrame);
		return false;
	}

   auto b = _startFrame >= 0 && frame >= _startFrame && frame < (_startFrame + _track.size());
   if (!b)
   {
       TRACE_1(errout << "Frame: " << frame << ", _startFrame: " <<  _startFrame << " _track.size(): " << _track.size());
   }

   return b;
}
//---------------------------------------------------------------------------

FPOINT TrackingBox:: operator[](int frame) const
{
   return getTrackPointAtFrame(frame);
}
//---------------------------------------------------------------------------

FPOINT TrackingBox::getTrackPointAtFrame(int frame) const
{
	if (!isValidAtFrame(frame))
	{
		// This is really an error, but because Larry is having unexplained
		// crashes, I'm trying to do something reasonable instead.
		if (!isValidAtFrame(_startFrame))
		{
			return FPOINT(0, 0);
		}

		// Use tarck point form closest valid frame.
		if (frame < _startFrame)
		{
			frame = _startFrame;
		}
		else
		{
			frame = getLastValidFrame();
		}

		if (!isValidAtFrame(_startFrame))
		{
			return FPOINT(0, 0);
		}
	}

   auto trackPosition = _track[frame - _startFrame];
   return trackPosition.in + trackPosition.virtualTrackOffset;
}
//---------------------------------------------------------------------------

void TrackingBox::setTrackPointAtFrame(int frame, FPOINT point)
{
	if (!isValidAtFrame(_startFrame) || (frame != _startFrame && !isValidAtFrame(frame - 1)))
	{
		// This should really never happen, but I'm trying to avoid error situations.
		_track.clear();
		_startFrame = frame;
		_track.push_back(TrackingPosition(point));
		return;
	}

   // We nuke the data for the specified frames and all succeeding frames, then append the new one.
   trimFramesAfter(frame - 1);
   addTrackPoint(point);
}
//---------------------------------------------------------------------------

void TrackingBox::addTrackPoint(FPOINT newPoint)
{
	appendTrackingPosition(TrackingPosition(newPoint));
}
//---------------------------------------------------------------------------

void TrackingBox::appendTrackingPosition(TrackingPosition position)
{
   // My debug watcher stopped being able to show structured arguments.
   auto &newPosition = position;

   // Need to deduce the virtual track offset from the previous tracking position!
   auto frameIndex = _track.size();
   if (frameIndex > 0)
   {
      auto previousPosition = _track[frameIndex - 1];
      newPosition.virtualTrackOffset = previousPosition.virtualTrackOffset + (previousPosition.in - previousPosition.out);
   }

	_track.push_back(newPosition);
}
//---------------------------------------------------------------------------

void TrackingBox::movePositionAtFrame(int frame, FPOINT offset)
{
   MTIassert(isValidAtFrame(frame));
   if (!isValidAtFrame(frame))
   {
      TRACE_0(errout << "INTERNAL ERROR: tried to move track box at an untracked frame!");
		TRACE_ME;
      return;
   }

   auto frameIndex = frame - _startFrame;
   auto &trackPosition = _track[frameIndex];
   auto newLocation = trackPosition.out + offset;

   // Always move the out point. If at the start frame, also move the in point.
   // Not that the virtual track offset is not affected - this move kicks in
   // at the next frame; it doesn't affect the track point for this frame!
   trackPosition.out = newLocation;
   if (frameIndex == 0)
   {
      trackPosition.in = newLocation;
      trackPosition.virtualTrackOffset = FPOINT(0, 0);
   }

   // All positions past the move frame are now invalid.
   trimFramesAfter(frame);
}
//---------------------------------------------------------------------------

void TrackingBox::neutralizePositionAtFrame(int frame)
{
   bool isFirstPoint = isValidAtFrame(frame - 1) == false;
   bool isSecondPoint = isFirstPoint == false && isValidAtFrame(frame - 2) == false;
   bool isLastPoint = isValidAtFrame(frame + 1) == false;
   bool isNextToLastPoint = isLastPoint == false && isValidAtFrame(frame + 2) == false;


   if (isFirstPoint && isLastPoint)
   {
      // Can't get more neutral!
      return;
   }

   auto frameIndex = frame - _startFrame;
   auto &trackPosition = _track[frameIndex];

   // Need to remove any contibution of the neutralized frame to the virtual
   // offset of the succeeding frames.
   if (isLastPoint == false)
   {
      auto virtualTrackOffset = trackPosition.in - trackPosition.out;
      for (auto iter = _track.begin() + frameIndex + 1; iter != _track.end(); ++iter)
      {
         iter->virtualTrackOffset -= virtualTrackOffset;
      }
   }

   if (isFirstPoint && isNextToLastPoint)
   {
      // Only two points!
      auto &nextTrackPosition = _track[frameIndex + 1];
      auto neutralPoint = nextTrackPosition.in + nextTrackPosition.virtualTrackOffset;
      trackPosition.in = trackPosition.out = neutralPoint;
   }
   else if (isFirstPoint)
   {
      auto &nextTrackPosition = _track[frameIndex + 1];
      auto &nextNextTrackPosition = _track[frameIndex + 2];
      auto nextTrackPoint = nextTrackPosition.in + nextTrackPosition.virtualTrackOffset;
      auto nextNextTrackPoint = nextNextTrackPosition.in + nextNextTrackPosition.virtualTrackOffset;
      auto neutralPoint = nextTrackPoint + (nextTrackPoint - nextNextTrackPoint);
      trackPosition.in = trackPosition.out = neutralPoint;
   }
   else if (isLastPoint && isSecondPoint)
   {
      // Only two points!
      auto &prevTrackPosition = _track[frameIndex - 1];
      auto neutralPoint = prevTrackPosition.out + prevTrackPosition.virtualTrackOffset;
      trackPosition.in = trackPosition.out = neutralPoint;
   }
   else if (isLastPoint)
   {
      auto &prevTrackPosition = _track[frameIndex - 1];
      auto &prevPrevTrackPosition = _track[frameIndex - 2];
      auto prevTrackPoint = prevTrackPosition.in + prevTrackPosition.virtualTrackOffset;
      auto prevPrevTrackPoint = prevPrevTrackPosition.in + prevPrevTrackPosition.virtualTrackOffset;
      auto neutralPoint = prevTrackPoint + (prevTrackPoint - prevPrevTrackPoint);
      trackPosition.in = trackPosition.out = neutralPoint;
   }
   else
   {
      auto &prevTrackPosition = _track[frameIndex - 1];
      auto &prevPrevTrackPosition = _track[frameIndex - 2];
      auto prevTrackPoint = prevTrackPosition.in + prevTrackPosition.virtualTrackOffset;
      auto prevPrevTrackPoint = prevPrevTrackPosition.in + prevPrevTrackPosition.virtualTrackOffset;

      auto &nextTrackPosition = _track[frameIndex + 1];
      auto &nextNextTrackPosition = _track[frameIndex + 2];
      auto nextTrackPoint = nextTrackPosition.in + nextTrackPosition.virtualTrackOffset;
      auto nextNextTrackPoint = nextNextTrackPosition.in + nextNextTrackPosition.virtualTrackOffset;

      // First approximation to give us a jump offset.
      trackPosition.in = prevTrackPoint + (prevTrackPoint - prevPrevTrackPoint);
      trackPosition.out = nextTrackPoint + (nextTrackPoint - nextNextTrackPoint);

      // Optional monocubic smoothing
      auto jumpOffset = trackPosition.in - trackPosition.out;

      vector<float> iVec;
      vector<float> xVec;
      vector<float> yVec;

      iVec.push_back(float(1.F));
      xVec.push_back(prevPrevTrackPoint.x);
      yVec.push_back(prevPrevTrackPoint.y);
      iVec.push_back(float(2.F));
      xVec.push_back(prevTrackPoint.x);
      yVec.push_back(prevTrackPoint.y);
      iVec.push_back(float(4.F));
      xVec.push_back(nextTrackPoint.x + jumpOffset.x);
      yVec.push_back(nextTrackPoint.y + jumpOffset.y);
      iVec.push_back(float(5.F));
      xVec.push_back(nextNextTrackPoint.x + jumpOffset.x);
      yVec.push_back(nextNextTrackPoint.y + jumpOffset.y);

      float x = interpolateMonoCubically(iVec, xVec, 3.F);
      float y = interpolateMonoCubically(iVec, yVec, 3.F);
      FPOINT neutralPoint(x, y);

      trackPosition.in = FPOINT(x, y);
      trackPosition.out = FPOINT(x + jumpOffset.x, y + jumpOffset.y);

      // Need to add jump offset contibution of the neutralized
      // frame to the virtual offset of the succeeding frames.
      if (isLastPoint == false)
      {
         for (auto iter = _track.begin() + frameIndex + 1; iter != _track.end(); ++iter)
         {
            iter->virtualTrackOffset += jumpOffset;
         }
      }
   }
}
//---------------------------------------------------------------------------

void TrackingBox::setStartFrame(int frame)
{
   if (frame == _startFrame)
   {
      // Nothing to do.
      return;
   }

	if (frame < 0)
	{
		// Mark removed - clear everything.
		_track.clear();
	}
	else if (_startFrame >= 0)
	{
		// We are changing the start frame.
		if (frame < _startFrame)
		{
			// Start frame moved to an earlier frame so is outside of the
			// tracked range. Move the tracking points from the old start
			// frame to the new start frame and clear the rest of the track.
			_startFrame = frame;
			trimFramesAfter(_startFrame);
      }
		else if (frame < (_startFrame + _track.size()))
      {
			// Start frame moved to a later frame, but not beyond the current track.
			// Trim all frames before the start frame, which will also set the start
			// frame to the desired frame.
			trimFramesBefore(frame);
      }
      else
      {
         // Start frame moved past the end of the track.
			// Here we find the last tracked frame and move the point from there to
			// the new start frame by simply removing everything before and after
			// the last frame in the track, then resetting the start frame.
         auto lastTrackedFrame = _startFrame + _track.size() - 1;
         trimFramesBefore(lastTrackedFrame);
			trimFramesAfter(lastTrackedFrame);
			_startFrame = frame;
      }
	}

   //TRACE_ME;
}
//---------------------------------------------------------------------------

int TrackingBox::getStartFrame() const
{
   return _startFrame;
}
//---------------------------------------------------------------------------

TrackingPosition TrackingBox::getTrackingPositionAtFrame(int frame) const
{
	if (!isValidAtFrame(frame))
	{
		// This is really an error, but because Larry is having unexplained
		// crashes, I'm trying to do something reasonable instead.
		if (!isValidAtFrame(_startFrame))
		{
			return TrackingPosition(FPOINT(0, 0));
		}

		// Use tarck point form closest valid frame.
		if (frame < _startFrame)
		{
			frame = _startFrame;
		}
		else
		{
			frame = getLastValidFrame();
		}

		if (!isValidAtFrame(_startFrame))
		{
			return TrackingPosition(FPOINT(0, 0));
		}
	}

	return _track[frame - _startFrame];
}
//---------------------------------------------------------------------------

void TrackingBox::trimFramesBefore(int frame)
{
	if (_startFrame < 0 || frame >= (_startFrame + _track.size()))
	{
		_track.clear();
		return;
	}

   if (frame <= _startFrame)
   {
      return;
   }

   auto endIndex = frame - _startFrame;
   _track.erase(_track.begin(), _track.begin() + endIndex);
   _startFrame += endIndex;
}
//---------------------------------------------------------------------------

void TrackingBox::trimFramesAfter(int frame)
{
	if (_startFrame < 0 || frame < _startFrame)
   {
      _track.clear();
   }

   auto index = frame - _startFrame + 1;
   if (index >= _track.size())
   {
      return;
   }

   _track.erase(_track.begin() + index, _track.end());
}
//---------------------------------------------------------------------------

int TrackingBox::getLastValidFrame() const
{
	if (!isValidAtFrame(_startFrame))
	{
		return -1;
	}

	return _startFrame + _track.size() - 1;
}
//---------------------------------------------------------------------------

int TrackingBox::getNumberOfValidFrames() const
{
   return _track.size();
}
//---------------------------------------------------------------------------

string TrackingBox::toString() const
{
   MTIostringstream os;
	auto frame = _startFrame;
	auto localTrack = _track;
   os << "[";
   for (auto pos : localTrack)
   {
      if (frame != _startFrame) os << " ";
      os << pos.toString() << "@" << frame++;
   }

   os << "]";
   return os.str();
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//-----------------------TRACKING BOX SET------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


TrackingBoxSet::TrackingBoxSet()
{
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getInFrame() const
{
	checkConsistency();
	return _inFrame;
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getOutFrame() const
{
	checkConsistency();
	return _outFrame;
}
////---------------------------------------------------------------------------
//
//void TrackingBoxSet::setInFrame(int frame)
//{
//	_inFrame = frame;
//	if (frame < 0)
//	{
//		removeAllTrackingBoxes();
//	}
//	else
//	{
//		for (auto tag : _trackingBoxTags)
//		{
//			_trackingBoxes.at(tag).setStartFrame(frame);
//		}
//	}
//
//	checkConsistency();
//}
////---------------------------------------------------------------------------
//
//void TrackingBoxSet::setOutFrame(int frame)
//{
//	_outFrame = frame;
//	if (frame >= 0)
//	{
//		for (auto tag : _trackingBoxTags)
//		{
//			_trackingBoxes.at(tag).trimFramesAfter(frame - 1);
//		}
//	}
//
//	checkConsistency();
//}
//---------------------------------------------------------------------------

void TrackingBoxSet::setInAndOutFrames(int inFrame, int outFrame)
{
	// If the in frame is invalid, EVERYTHING is invalid!
	if (inFrame < 0)
	{
		_inFrame = -1;
		_outFrame = -1;
		removeAllTrackingBoxes();
		return;
	}

	// If out frame is invalid, set it to -1 instead.
	if (outFrame <= inFrame)
	{
		outFrame = -1;
	}

	// See if we're actually changing anything.
	if (_inFrame == inFrame && _outFrame == outFrame)
	{
		// Nope.
		return;
	}

	_inFrame = inFrame;
	_outFrame = outFrame;

	// Maybe toss some tracking data for each tracking box.
	for (auto tag : _trackingBoxTags)
	{
      _trackingBoxes.at(tag).setStartFrame(_inFrame);
		_trackingBoxes.at(tag).trimFramesBefore(_inFrame);
		_trackingBoxes.at(tag).trimFramesAfter((_outFrame > _inFrame)
																? (_outFrame - 1)
																: _inFrame);
	}

	checkConsistency();
}
//---------------------------------------------------------------------------

bool TrackingBoxSet::isFrameInRange(int frame) const
{
	if (frame < 0)
	{
		return false;
	}

	if (_inFrame < 0)
	{
		return false;
	}

	if (_outFrame < 0)
	{
		return frame == _inFrame;
	}

	return frame >= _inFrame && frame < _outFrame;
}
//---------------------------------------------------------------------------

int TrackingBoxSet::addTrackingBox(int startFrame, FPOINT initialXY)
{
	TrackingBox newTrackingBox(startFrame, initialXY);
   return addTrackingBox(newTrackingBox);
}
//---------------------------------------------------------------------------

int TrackingBoxSet::addTrackingBox(const TrackingBox &newTrackingBox)
{
   auto tag = _nextTag++;
   _trackingBoxTags.push_back(tag);
   _trackingBoxes[tag] = newTrackingBox;

	MTIassert(newTrackingBox.getStartFrame() <= _inFrame);

	checkConsistency();

   return tag;
}
//---------------------------------------------------------------------------

void TrackingBoxSet::moveTrackingBoxAtFrame(int trackingBoxTag, int frame, FPOINT offset)
{
   auto &trackingBox = _trackingBoxes[trackingBoxTag];

	trackingBox.movePositionAtFrame(frame, offset);

	checkConsistency();
}
//---------------------------------------------------------------------------

void TrackingBoxSet::removeTrackingBox(int trackingBoxTag)
{
   _trackingBoxes.erase(trackingBoxTag);
   auto iter = find(_trackingBoxTags.begin(), _trackingBoxTags.end(), trackingBoxTag);
   if (iter != _trackingBoxTags.end())
   {
      _trackingBoxTags.erase(iter);
   }

	checkConsistency();
}
//---------------------------------------------------------------------------

void TrackingBoxSet::removeAllTrackingBoxes()
{
	// Snapshot because we are modifying the selection!
	auto tags = getTrackingBoxTags();
	for (auto tag : tags)
	{
		removeTrackingBox(tag);
	}

	checkConsistency();
}
//---------------------------------------------------------------------------

bool TrackingBoxSet::isValidTrackingBoxTag(int trackingBoxTag) const
{
	checkConsistency();
	auto begin = _trackingBoxTags.begin();
   auto end = _trackingBoxTags.end();
   return find(begin, end, trackingBoxTag) != end;
}
//---------------------------------------------------------------------------

const TagList &TrackingBoxSet::getTrackingBoxTags() const
{
	checkConsistency();
	return _trackingBoxTags;
}
//---------------------------------------------------------------------------

TrackingBox &TrackingBoxSet::getTrackingBoxByTag(int trackingBoxTag)
{
	TrackingBox &retVal = _trackingBoxes[trackingBoxTag];
	checkConsistency();
	return retVal;
}
//---------------------------------------------------------------------------

TrackingBox &TrackingBoxSet::getTrackingBoxByIndex(int index)
{
////	MTIassert(index < getNumberOfTrackingBoxes());
	while (getNumberOfTrackingBoxes() <= index)
	{
      addTrackingBox(_inFrame, FPOINT(0, 0));
   }

	TrackingBox &retVal =  _trackingBoxes[_trackingBoxTags[index]];
	checkConsistency();
	return retVal;
}
//---------------------------------------------------------------------------

const TrackingBox &TrackingBoxSet::getTrackingBoxByTag(int trackingBoxTag) const
{
	MTIassert(_trackingBoxes.find(trackingBoxTag) != _trackingBoxes.end());

	const TrackingBox &retVal =  _trackingBoxes.at(trackingBoxTag);
	checkConsistency();
	return retVal;
}
//---------------------------------------------------------------------------

TrackPointList TrackingBoxSet::getAllTrackPointsAtFrame(int frame) const
{
   TrackPointList result(0);
   for (auto tag : _trackingBoxTags)
   {
		TrackingBox trackingBox = getTrackingBoxByTag(tag);
      if (!trackingBox.isValidAtFrame(frame))
      {
         continue;
      }

      result.push_back(trackingBox.getTrackPointAtFrame(frame));
   }

	checkConsistency();
	return result;
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getNumberOfTrackingBoxes() const
{
   checkConsistency();
	return size();
}
//---------------------------------------------------------------------------

void TrackingBoxSet::invalidateTracking()
{
	if (_inFrame < 0)
   {
      clear();
      return;
   }

   for (auto tag : getTrackingBoxTags())
   {
      TrackingBox &trackingBox = getTrackingBoxByTag(tag);
		trackingBox.trimFramesBefore(_inFrame);
      trackingBox.trimFramesAfter(_inFrame);
	}

	checkConsistency();
}
//---------------------------------------------------------------------------

void TrackingBoxSet::clear()
{
   _trackingBoxes.clear();
	_trackingBoxTags.clear();
	_inFrame = _outFrame = -1;

	checkConsistency();
}
//---------------------------------------------------------------------------

int TrackingBoxSet::size() const
{
	checkConsistency();
	return _trackingBoxTags.size();
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getFirstUntrackedFrame() const
{
	if (_inFrame < 0)
	{
		return -1;
	}

	if (_outFrame < 0)
	{
		return _inFrame + 1;
	}

	auto lastFullyTrackedFrame = _outFrame;
	for (auto tag : _trackingBoxTags)
	{
		auto lastValidBoxFrame = _trackingBoxes.at(tag).getLastValidFrame();
		if (lastValidBoxFrame >= 0)
		{
			lastFullyTrackedFrame = std::min<int>(lastFullyTrackedFrame, lastValidBoxFrame);
		}
	}

   return lastFullyTrackedFrame + 1;
}
//---------------------------------------------------------------------------

bool TrackingBoxSet::areAllTrackingBoxesFullyTracked() const
{
	if (_inFrame < 0 || _outFrame < 0)
	{
		return false;
	}

	auto lastFrameInRange = _outFrame - 1;
	for (auto tag : _trackingBoxTags)
	{
		auto lastValidBoxFrame = _trackingBoxes.at(tag).getLastValidFrame();
		if (lastValidBoxFrame < lastFrameInRange)
		{
			checkConsistency();
			return false;
		}
	}

	checkConsistency();
	return true;
}
//---------------------------------------------------------------------------

TrackingStatus TrackingBoxSet::getTrackingStatus() const
{
	if (_inFrame < 0 || _outFrame < 0)
	{
		return NeedToTrack;
	}

	auto status = TrackingIsComplete;
	auto endFrame = _outFrame - 1;
	for (auto tag : _trackingBoxTags)
	{
		if (!_trackingBoxes.at(tag).isValidAtFrame(endFrame))
		{
			status = NeedToTrack;
			break;
		}
	}

	checkConsistency();
	return status;
}
//---------------------------------------------------------------------------

void TrackingBoxSet::setBoundingBox(const RECT &bb)
{
   _boundingBox = bb;
}
//---------------------------------------------------------------------------

RECT TrackingBoxSet::getBoundingBox(void) const
{
   return _boundingBox;
}
//---------------------------------------------------------------------------

void TrackingBoxSet::setUseBoundingBox(const bool &flag)
{
   _useBoundingBox = flag;
}
//---------------------------------------------------------------------------

bool TrackingBoxSet::getUseBoundingBox(void) const
{
   return _useBoundingBox;
}
//---------------------------------------------------------------------------

// Extents of track box size & motion search
void TrackingBoxSet::setTrackingBoxRadius(int newRadius)
{
   _trackingBoxRadius = newRadius;
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getTrackingBoxRadius() const
{
   return _trackingBoxRadius;
}
//---------------------------------------------------------------------------

void TrackingBoxSet::setSearchBoxRadius(int newRadius)
{
   _searchBoxRadius = newRadius;
}
//---------------------------------------------------------------------------

int TrackingBoxSet::getSearchBoxRadius() const
{
   return _searchBoxRadius;
}
//---------------------------------------------------------------------------

void TrackingBoxSet::checkConsistency() const
{
	int numberOfBoxes =  _trackingBoxes.size();
	int numberOfTags = _trackingBoxTags.size();
	if (numberOfBoxes != numberOfTags)
	{
		TRACE_0(errout << "INTERNAL ERROR: Tracking box set consistency check FAIL! ("
							<< numberOfBoxes << " != " << numberOfTags << ")");
		MTImillisleep(1);
	}
}
//---------------------------------------------------------------------------

string TrackingBoxSet::toString() const
{
   MTIostringstream os;
   os << "=====================================" << endl;
   os << "TB Set: in=" << _inFrame << " out=" << _outFrame;
	os << " status=" << ((getTrackingStatus() == TrackingIsComplete)? "C" : ((getTrackingStatus() == NeedToTrack) ?  "N" : "U")) << endl;
	int numberOfBoxes =  _trackingBoxes.size();
	int numberOfTags = _trackingBoxTags.size();
	if (numberOfBoxes != numberOfTags)
	{
		os << "MISMATCH! #boxes " << numberOfBoxes << " != #tags " << numberOfTags << endl;
		os << "TAGS: ";
		for (auto tag : _trackingBoxTags)
		{
			if (tag != _trackingBoxTags[0]) os << ", ";
			os << tag;
		}

		os << endl << "BOXES: ";
		for (auto boxMapEntry : _trackingBoxes)
		{
			os << "   Tag=" << boxMapEntry.first << " Box=" << boxMapEntry.second.toString() << endl;
		}
	}
	else
	{
		for (auto tag : _trackingBoxTags)
		{
			if (tag != _trackingBoxTags[0]) os << " ";
			os << tag << ": " << _trackingBoxes.at(tag).toString() << endl;
		}
	}

   os << endl << "=====================================";
   return os.str();
};
//---------------------------------------------------------------------------

void TrackingBoxSet::ReadPDLEntry(CPDLElement *pdlTrackingArray)
{
  	auto trackingInfo = pdlTrackingArray->GetAttribString("AsString", "");

   // QQQ Can't use MTIostringstream, so I'll borrow its lock
   {
      CAutoSystemThreadLocker lock(MTIstringstream::GetLock());
      std::istringstream os(trackingInfo);
      os >> *this;
   }
}
//---------------------------------------------------------------------------

void TrackingBoxSet::AddToPDLEntry(CPDLElement &parent)
{
   CPDLElement *pdlTrackingArray = parent.MakeNewChild("TrackingBoxSet");

   // QQQ Can't use MTIostringstream, so I'll borrow its lock
   {
      CAutoSystemThreadLocker lock(MTIstringstream::GetLock());
      std::ostringstream os;
      os << *this;
      pdlTrackingArray->SetAttribString("AsString", os.str());
   }
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

std::ostream &operator<<(std::ostream &theStream, const FPOINT &point)
{
   theStream << "(" << point.x << ", " << point.y << ")";
   return theStream;
}

std::ostream &operator<<(std::ostream &theStream, const TrackPointList &pointList)
{
   bool first = true;
   for (auto point : pointList)
   {
      if (first)
      {
         first = false;
      }
      else
      {
         theStream << " ";
      }

      theStream << "(" << point.x << ", " << point.y << ")";
   }

   return theStream;
}

std::ostream &operator<<(std::ostream &theStream, const TrackingBox &trackingBox)
{
    theStream << '[' << trackingBox.getStartFrame();

    for (auto i = 0; i < trackingBox.getNumberOfValidFrames(); ++i)
    {
       if (i > 0 && (i % 10) == 0)
       {
          theStream << endl << "   ";
       }

       auto trackingPosition = trackingBox.getTrackingPositionAtFrame(trackingBox.getStartFrame() + i);
       theStream << " " << trackingPosition.in;
       if (trackingPosition.out != trackingPosition.in)
       {
          theStream << '/' << trackingPosition.out;
       }
    }

   theStream << ']';
   return theStream;
}

//   unordered_map<int, TrackingBox> _trackingBoxes;
//   TagList _trackingBoxTags;
//   int _inFrame = -1;
//   int _outFrame = -1;
//   RECT _boundingBox = {-1, -1, -1, -1};
//   bool _useBoundingBox = false;
//   int _trackingBoxRadius = -1;
//   int _searchBoxRadius = -1;
//   TrackingStatus _trackingStatus = TrackingStatusIsUnknown;

std::ostream &operator<<(std::ostream &theStream, const TrackingBoxSet &ta)
{
   theStream << "v2{" << endl;
   theStream << ta.getInFrame() << " " << ta.getOutFrame() << endl;
   theStream << ta.getTrackingBoxRadius() << " " << ta.getSearchBoxRadius() << endl;
   theStream << (ta.getUseBoundingBox() ? 1 : 0) << ' ';
   RECT bb = ta.getBoundingBox();
   theStream << bb.left << ' ' << bb.right << ' ' << bb.top << ' ' << bb.bottom << endl;

   for (auto tag : ta.getTrackingBoxTags())
   {
      theStream << ta.getTrackingBoxByTag(tag) << endl;
   }

   theStream << "}" << endl;

   return theStream;
}

std::istream& operator>>(std::istream &theStream, FPOINT &fp)
{
   char c = '\0';
   fp = FPOINT(0, 0);

   theStream >> c;
   if (!theStream.good() || c != '(')
   {
      theStream.setstate(theStream.badbit);
      return theStream;
   }

   // Do the x
   theStream >> fp.x;
   if (!theStream.good())
   {
      return theStream;
   }

   // Do the ,
   theStream >> c;
   if (!theStream.good() || c != ',')
   {
      theStream.setstate(theStream.badbit);
      return theStream;
   }

   // Do the y
   theStream >> fp.y;
   if (!theStream.good())
   {
      return theStream;
   }

   // eat the final )
   // Don't bother to check for failure.
   theStream >> c;

   return theStream;
}

std::istream& operator>>(std::istream &theStream, TrackingBox &tb)
{
   char c = '\0';
   theStream >> c;
   if (!theStream.good() || c != '[')
   {
      theStream.setstate(theStream.badbit);
      return theStream;
   }

   // Do the start frame
   int frame = -1;
   theStream >> frame;
   if (!theStream.good())
   {
      return theStream;
   }

	tb.setStartFrame(frame);

   while (true)
   {
      FPOINT inPoint;
      theStream >> inPoint;

      // fail() because we do not want to check for EOF here!
      if (theStream.fail())
      {
         break;
      }

      tb.setTrackPointAtFrame(frame, inPoint);

      theStream >> c;
      if (!theStream.good() || c == ']')
      {
         break;
      }

      if (c == '/')
      {
         FPOINT outPoint;
         theStream >> outPoint;

         // fail() because we do not want to check for EOF here!
         if (theStream.fail())
         {
            break;
         }

         tb.movePositionAtFrame(frame, outPoint - inPoint);
         ++frame;
         continue;
      }

      ++frame;
      theStream.putback(c);
   }

   return theStream;
}


std::istream& operator>>(std::istream &theStream, TrackingBoxSet &ta)
{
   ta.clear();

   char c1, c2, c3;
   theStream >> c1 >> c2 >> c3;
   if (!theStream.good() || c1 != 'v' || c2 != '2' || c3 != '{')
   {
      theStream.setstate(theStream.failbit);
      return theStream;
   }

   // Read the in and out frames.
   int in, out;
   theStream >> in >> out;
   if (!theStream.good())
   {
      return theStream;
   }

//	ta.setInFrame(in);
//	ta.setOutFrame(out);
	ta.setInAndOutFrames(in, out);

   // Read the box radii.
   int tbRadius, sbRadius;
   theStream >> tbRadius >> sbRadius;
   if (!theStream.good())
   {
      return theStream;
   }

   ta.setTrackingBoxRadius(tbRadius);
   ta.setSearchBoxRadius(sbRadius);

   // Read the bounding box information
   int bUsebb;
   RECT bb;
   theStream >> bUsebb >> bb.left >> bb.right >> bb.top >> bb.bottom;
   if (!theStream.good())
   {
      return theStream;
   }

   ta.setUseBoundingBox(bUsebb != 0);
   ta.setBoundingBox(bb);

   // Read the tracking box info.
   auto idx = 0;
   while (theStream.good())
   {
      // Read the next bit
      char c;
      theStream >> c;
      if (!theStream.good() || c == '}')
      {
         break;
      }

      theStream.putback(c);

      theStream >> ta.getTrackingBoxByIndex(idx++);
   }

   return theStream;
}
//---------------------------------------------------------------------------
