#include "ColorPickTool.h"
#include "MTIWinInterface.h"
#include "ToolObject.h"

CColorPickTool::CColorPickTool()
: R(0.F), G(0.F), B(0.f), X(0), Y(0), Enabled(false), SystemAPI(NULL)
{
}

void CColorPickTool::loadSystemAPI(CToolSystemInterface *newSystemAPI)
{
   SystemAPI  = newSystemAPI;
}

// Go in/out of Color Pick mode
void CColorPickTool::SetEnabled(bool newEnabled)
{
   if (SystemAPI != NULL)
   {
      Enabled = newEnabled;
      if (Enabled)
      {
         SystemAPI->enterCursorOverrideMode(PickerCursor());
      }
      else
      {
         SystemAPI->exitCursorOverrideMode();
      }
   }
}

bool CColorPickTool::IsEnabled()
{
   return Enabled;
}

// Get the current color - caller will call this in response to either
// of the above callbacks
void CColorPickTool::getColor(int &Rout, int &Gout, int &Bout)
{
   Rout = R;
   Gout = G;
   Bout = B;
}

// Get the point where the color was picked
void CColorPickTool::getXY(int &Xout, int &Yout)
{
   Xout = X;
   Yout = Y;
}

int CColorPickTool::onMouseDown(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   MouseIsDown = true;
   if (Enabled)
   {
      onMouseMove(userInput);
      retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return retVal;
}

int CColorPickTool::onMouseMove(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;
   int componentMaxValue;
   float comp[3];

   if (Enabled && (SystemAPI != NULL))
   {
      int newR, newG, newB;
      const int radius(1);   // Gives us average over 3x3 pixels

      X = userInput.imageX;
      Y = userInput.imageY;

      SystemAPI->setColorPickSampleSize(radius);
      SystemAPI->getColorPickSample(X, Y,
                                    componentMaxValue,
                                    comp[0], comp[1], comp[2]);
      newR = int(componentMaxValue * comp[0] + 0.5);
      newG = int(componentMaxValue * comp[1] + 0.5);
      newB = int(componentMaxValue * comp[2] + 0.5);

      if (newR > componentMaxValue) newR = componentMaxValue;
      if (newG > componentMaxValue) newG = componentMaxValue;
      if (newB > componentMaxValue) newB = componentMaxValue;
      if (newR < 0) newR = 0;
      if (newG < 0) newG = 0;
      if (newB < 0) newB = 0;

      if (newR != R || newG != G || newB != B)
      {
         R = newR;
         G = newG;
         B = newB;
         newColorHoveredOver.Notify();
      }
   }

   return retVal;
}

int CColorPickTool::onMouseUp(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;
   MouseIsDown = false;
   if (Enabled)
   {
      newColorPicked.Notify();
      retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return retVal;
}



