#ifndef ToolObjectH
#define ToolObjectH

#include <string>
#include <vector>
using std::string;
using std::vector;

#include "machine.h"
#include "MTImalloc.h"
#include "SafeClasses.h"
#include "ToolFrameBuffer.h"
#include "ToolSystemInterface.h"

//////////////////////////////////////////////////////////////////////
// Tool Return Code Values

// If bit 15 is set, lower 16 bits of return code contains an error code
#define TOOL_RETURN_CODE_ERROR 0x8000
#define TOOL_RETURN_CODE_ERROR_MASK 0xFFFF

#define TOOL_RETURN_CODE_NO_ACTION           0x0001   
#define TOOL_RETURN_CODE_PROCESSED_OK        0x0002
#define TOOL_RETURN_CODE_OPERATION_REFUSED   0x0004
#define TOOL_RETURN_CODE_EVENT_CONSUMED      0x0010

#define TOOL_PROC_SHORT_SLEEP_TIME           1
//#define TOOL_PROC_LONG_SLEEP_TIME            50
#define TOOL_PROC_LONG_SLEEP_TIME            1

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBufferPool;
class CRsrcCtrl;
class CImageFormat;
class CPDLElement;
class CPDLEntry;
class CRegionOfInterest;
class CToolCommand;
class CToolCommandTable;
class CToolEvent;
class CDefaultToolProgressMonitor;
class CToolNode;
class CToolParameters;
class CToolProcessor;
class IToolProgressMonitor;
class CToolSystemInterface;
class CToolUserInputMap;
class CUserInput;
class CUserInputConfiguration;

//////////////////////////////////////////////////////////////////////
//
#if 0 // moved to ToolSystemInterface.h
enum EAutotoolStatus
{
   AT_STATUS_INVALID = 0,
   AT_STATUS_STOPPED,         // Autotool processing is now stopped
   AT_STATUS_RUNNING,         // Autotool processing is now running
   AT_STATUS_PAUSED,           // Autotool processing is now paused
   AT_STATUS_END_OF_MATERIAL,
   AT_STATUS_PROCESS_ERROR,
   AT_STATUS_PROCESS_THREAD_STARTED,
   AT_STATUS_PROCESS_THREAD_ENDING
};
#endif

class CAutotoolStatus
{
public:
   CAutotoolStatus()
      : status(AT_STATUS_INVALID), errorCode(0), frameIndex(-1),
        toolSetupHandle(-1) {};

   EAutotoolStatus status;
   int errorCode;
   int frameIndex;
   int toolSetupHandle;
};

typedef CRingBuffer<CAutotoolStatus> AutotoolStatusQueue;

//////////////////////////////////////////////////////////////////////

enum EAutotoolCommand
{
   AT_CMD_INVALID = 0,
   AT_CMD_STOP,               // Stop autotool processing
   AT_CMD_RUN,                // Start autotool processing
   AT_CMD_PAUSE,              // Pause autotool processing
   AT_CMD_SET_PARAMETERS,     // Set tool's parameters
   AT_CMD_EXIT_THREAD         // Flush buffers and exit tool processing threads
};

class CAutotoolCommand
{
public:
   CAutotoolCommand()
      : command(AT_CMD_INVALID), toolParams(0) {};

   EAutotoolCommand command;
   CToolParameters *toolParams;
};

typedef CRingBuffer<CAutotoolCommand> AutotoolCommandQueue;

//////////////////////////////////////////////////////////////////////

enum EToolProcessingCommand
{
   TOOL_PROCESSING_CMD_INVALID = 0,
   TOOL_PROCESSING_CMD_CONTINUE,
   TOOL_PROCESSING_CMD_PAUSE,
   TOOL_PROCESSING_CMD_PREVIEW,
   TOOL_PROCESSING_CMD_PREVIEW_1,
   TOOL_PROCESSING_CMD_RENDER,
   TOOL_PROCESSING_CMD_RENDER_1,
   TOOL_PROCESSING_CMD_STOP,
   TOOL_PROCESSING_CMD_PREPROCESS,
   TOOL_PROCESSING_CMD_CUSTOM1,
   TOOL_PROCESSING_CMD_CUSTOM2,
   TOOL_PROCESSING_CMD_CUSTOM3,
   TOOL_PROCESSING_CMD_CUSTOM4,
   TOOL_PROCESSING_CMD_CUSTOM5,
   TOOL_PROCESSING_CMD_CUSTOM6,
};

//////////////////////////////////////////////////////////////////////

// Outer states of tool control
enum EToolControlState
{
   TOOL_CONTROL_STATE_STOPPED,
   TOOL_CONTROL_STATE_WAITING_TO_STOP,    // Used by Tool Mgr, but not used
                                          // by Tool Proc
   TOOL_CONTROL_STATE_WAITING_TO_RUN,
   TOOL_CONTROL_STATE_RUNNING,
   TOOL_CONTROL_STATE_WAITING_TO_PAUSE,
   TOOL_CONTROL_STATE_PAUSED,
   TOOL_CONTROL_STATE_EXITING_THREAD,
   TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED,
   TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED,
   TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED,
   TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED,
};

//////////////////////////////////////////////////////////////////////

enum EPDLRenderingStatus
{
   PDL_RENDERING_STATUS_INVALID = 0,
   PDL_RENDERING_STATUS_STOPPED,
   PDL_RENDERING_STATUS_INITIALIZING,
   PDL_RENDERING_STATUS_RUNNING,
   PDL_RENDERING_STATUS_COMPLETED,
   PDL_RENDERING_STATUS_STOPPED_BY_USER,
   PDL_RENDERING_STATUS_STOPPED_BY_ERROR
};

//////////////////////////////////////////////////////////////////////
#if 0 // moved to ToolSystemInterface.h
enum EToolSetupType
{
   TOOL_SETUP_TYPE_INVALID,        // NOP
   TOOL_SETUP_TYPE_SINGLE_FRAME,   // Process a single frame, use Provisional
   TOOL_SETUP_TYPE_MULTI_FRAME,    // Process a range of frames
   TOOL_SETUP_TYPE_TRAINING        // Process a single frame without the
                                   // prospect of writing back to disk
};
#endif

//////////////////////////////////////////////////////////////////////

class CToolObject
{
public:
   CToolObject(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CToolObject();

   // Tool Event Handlers
   virtual int onChangeFrameView();     // DEFUNCT

   virtual int onCapturePDLEntry(CPDLElement &toolParams);
   virtual int onGoToPDLEntry(CPDLEntry &pdlEntry);
   virtual int onPDLExecutionComplete();
   virtual int OnStopPDLRendering();

   virtual int onLeavingClip();     // DEFUNCT
   virtual int onLeavingFrame();     // DEFUNCT

   virtual int onMouseMove(CUserInput &userInput);

   virtual int onNewMarks();
   virtual int onChangingClip();
   virtual int onDeletingOpenedClip();
   virtual int onNewClip();
   virtual int onNewFrame();     // DEFUNCT
   virtual int onChangingFraming();
   virtual int onNewFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes);
   virtual int onPreviewDisplayBufferDraw(int frameIndex, int width, int height, unsigned int *pixels);
   virtual int onTopPanelRedraw();
   virtual int onTopPanelMouseDown(int x, int y);
   virtual int onTopPanelMouseDrag(int x, int y);
   virtual int onTopPanelMouseUp(int x, int y);
   virtual int onTimelineVisibleFrameRangeChange();
   virtual int onPlayerStart();
   virtual int onPlayerStop();
   virtual int onPlaybackFilterChange(int);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onToolEvent(CToolEvent &toolEvent);
   virtual int onUserInput(CUserInput &userInput);
   virtual int onHeartbeat();
   virtual int onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message);
   virtual int onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag, const std::pair<int, int> &frameRange);
   virtual int onClipWasDeleted(const string &deletedClipName);
   virtual int onSwitchSubtool();


   // Tool Control
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual int toolHide();
   virtual bool toolHideQuery();
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI) = 0;
   virtual int toolShow();
   virtual int toolShutdown() = 0;
   virtual void toolResetWarnings() {};

   // Tool progress monitor
   IToolProgressMonitor *GetToolProgressMonitor();
   void SetToolProgressMonitor(IToolProgressMonitor *newToolProgressMonitor);

   // Tool execution permissions
   virtual bool checkOutToolLicense();
   virtual bool isItOkToUseToolNow();
   virtual void doneWithTool();

   virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);

   // Query Functions
   const char* getToolName();
   string GetToolName();
   int GetToolNumber();
   bool IsLicensed();
   bool IsDisabled();
   bool IsActive();
   EToolDisabledReason getToolDisabledReason();
   const CToolCommandTable* getCommandTable();
   const CToolUserInputMap* getUserInputMap();
   virtual bool IsShowing(void);
   CToolSystemInterface* getSystemAPI() const;
   void SetRenderToNewClipFlag(bool);
   bool GetRenderToNewClipFlag();
	bool AreMarksValid();
	bool IsInViewOnlyMode();
	virtual bool DoesToolPreserveHistory();

   // AWFUL HACK!! The "Mask Transform Editor" tries to refresh the displayed
   // frame every time the mouse moves, even if no mask is being transformed.
   // The reason is to produce a side effect where cursors in some obscure
   // Paint mode will get updated. But this interferes with my debugging and
   // profiling of other tools, so I implemented a way to turn off the
   // refresh when the mask is not being manipulated
   virtual void SetMaskTransformEditorIdleRefreshMode();

   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   virtual void NotifyGOVDone();
   virtual void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                      const RECT &rect, const POINT *lasso);
   virtual void NotifyRightClicked();
   virtual bool NotifyTrackingStarting();
   virtual void NotifyTrackingDone(int errorCode);
   virtual void NotifyTrackingCleared();

	virtual bool NotifyStartROIMode();
   virtual bool NotifyUserDrewROI();
	virtual void NotifyEndROIMode();
   virtual void NotifyMaskChanged(const string &newMaskAsString);
   virtual void NotifyMaskVisibilityChanged();

	virtual void NotifyStartViewOnlyMode();
	virtual void NotifyEndViewOnlyMode();

   // The real tool must specialize this function
   virtual CToolProcessor* makeToolProcessorInstance(
                                     const bool *newEmergencyStopFlagPtr) = 0;

   virtual int ContinueToolProcessing(int newResumeFrame);
   virtual int PauseToolProcessing();
   virtual int StartToolProcessing(EToolProcessingCommand processingType,
                                   bool newHighlightFlag);
	virtual int StopToolProcessing();
   virtual void SetToolProcessingControlState(EToolControlState);
   virtual EToolControlState GetToolProcessingControlState();
   virtual int GetToolProcessingErrorCode();
   virtual bool GetToolProcessingRenderFlag();

   // Tool should override this if it does auto-accept
   virtual int TryToResolveProvisional();

   // Hack to get a dialog handle from the owner, ugly but I don't care
   HWND Handle;

protected:
   int shutdownBaseToolObject();
   int initBaseToolObject(int newToolNumber,
                          CToolSystemInterface *newSystemAPI,
                          CToolCommandTable *newCommandTable,
                          CUserInputConfiguration *newDefaultConfig);

   // User Input Translation
   virtual CToolCommand* mapUserInput(CUserInput &userInput);
   virtual bool IsInConsumeAllInputMode();

   // User Input Translation and Execution
   int mapAndExecuteUserInput(CUserInput &userInput);

   void setDefaultUserInputConfiguration(
                                    CUserInputConfiguration *newDefaultConfig);
   int loadUserInputConfiguration();

   virtual int MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
                                      const CAutotoolStatus& newToolStatus,
                                      int newResumeFrame);
   virtual int PauseFrameProcessing();
   virtual int PreloadInputFrames();      // Shiny and new
   virtual int ProcessTrainingFrame();    // Replaced by PreviewSingleFrame()
   virtual int PreviewSingleFrame();      // New
   virtual int RenderSingleFrame();       // New
   // New: in the following: toolSetupType is either
   // TOOL_SETUP_TYPE_SINGLE_FRAME (Render 1) or
   // TOOL_SETUP_TYPE_TRAINING_FRAME (Preview 1)
	virtual int ProcessSingleFrame(EToolSetupType toolSetupType);
   virtual int RunFramePreviewing(int newResumeFrame);
   virtual int RunFrameProcessing(int newResumeFrame);
   virtual int RunFramePreprocessing();
   virtual int StopFrameProcessing();
   virtual int StopSingleFrameProcessing();
   virtual void UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                                   bool processingRenderFlag);

protected:
   MTI_UINT32 **featureTable;
   bool renderFlag;
   bool renderToNewClipFlag;
   bool highlightFlag;
   EToolControlState toolProcessingControlState;
   int toolProcessingErrorCode;
#ifndef NO_LICENSING
   CRsrcCtrl *rsrcCtrl;
#endif

private:
   string toolName;          // Text string that uniquely identifies
                             // the tool.  Used in messages and menus
   MTI_UINT16 toolNumber;    // 16-bit number that uniquely identifies
                             // the tool.  Used in error codes.
   CToolCommandTable* commandTable;
   CToolUserInputMap* userInputMap;
   CUserInputConfiguration *defaultUserInputConfig;
   bool toolLicensed;
   bool toolLicenseWorkedOnce;
   int  toolLicenseNumStrikes;
	bool toolDisabled;
   bool toolActive;
	EToolDisabledReason toolDisabledReason;

	void setViewOnlyMode(bool flag);

	CToolSystemInterface *baseSystemAPI;  // Ptr to System Interface object

   CDefaultToolProgressMonitor *defaultToolProgressMonitor;
   IToolProgressMonitor *toolProgressMonitor;

   string prePauseStatusMessage;

   };

//////////////////////////////////////////////////////////////////////

// Internal states of tool processing
enum EToolProcessState
{
   TOOL_PROCESS_STATE_SETUP,
   TOOL_PROCESS_STATE_GET_INPUT,
   TOOL_PROCESS_STATE_SET_OUTPUT,
   TOOL_PROCESS_STATE_PROCESSING,
   TOOL_PROCESS_STATE_RELEASE_BUFFERS,
   TOOL_PROCESS_STATE_PUT_OUTPUT,
   TOOL_PROCESS_STATE_CLEANUP
};

//////////////////////////////////////////////////////////////////////

// An abstract base class that is used to hold a tool's parameters.
// Subtypes of this class are defined to hold a tool's particular parameters.
class CToolParameters
{
public:
   CToolParameters(int newInPortCount, int newOutPortCount);
   virtual ~CToolParameters();

   virtual CRegionOfInterest* GetRegionOfInterest() const;

   virtual void SetRegionOfInterest(CRegionOfInterest *newROI);

protected:
   CRegionOfInterest *regionOfInterest;// Ptr to Mask ROI owned by someone else

};


// An abstract base class that is used to hold a tool's parameters.
// Subtypes of this class are defined to hold a tool's particular parameters.
class CToolIOConfig
{
public:
   CToolIOConfig(int newInPortCount, int newOutPortCount);
   virtual ~CToolIOConfig();

public:
   // Data is all public, don't want to bother with accessors & mutators
   // at this point.  This sentiment may change in the future, so beware.

   int processingThreadCount;     // Need to rename this concurrentFrameCount
   int processingThreadsPerFrame;
   bool doRowSlices;
   int slicesPerThread;
   bool noopProcessing;
   bool disableGpu = false;

};

// An abstract base class that is used to hold a tool's
class CToolFrameRange
{
public:
   CToolFrameRange(int newInPortCount, int newOutPortCount);
   CToolFrameRange(const CToolFrameRange& srcToolFrameRange);
   virtual ~CToolFrameRange();

   CToolFrameRange& operator=(const CToolFrameRange& srcToolFrameRange);

public:

   struct SFrameRange
   {
      bool randomAccess;      // If true, in and out frame index is ignored
      int inFrameIndex;
      int outFrameIndex;
   };

public:
   // Data is all public, don't want to bother with accessors & mutators
   // at this point.  This sentiment may change in the future, so beware.

   // Frame ranges for each input and output port
   // It is up to the tool to make sense of the in and out frame ranges
   vector<SFrameRange> inFrameRange;        // indexed by in port number
   vector<SFrameRange> outFrameRange;       // indexed by out port number

};

//////////////////////////////////////////////////////////////////////

class CToolProcessor
{
public:
   CToolProcessor(int newToolNumber, const string &newToolName,
                  int newInPortCount, int newOutPortCount,
                  CToolSystemInterface *newSystemAPI,
                  const bool *newEmergencyStopFlagPtr,
                  IToolProgressMonitor *newToolProgressMonitor=NULL);
   virtual ~CToolProcessor();


   int PutToolCommand(CAutotoolCommand &command);
   int GetToolStatus(CAutotoolStatus &status);

   CToolNode *GetToolNode();

   virtual int SetIOConfig(CToolIOConfig *toolIOConfig) = 0;
   virtual int SetFrameRange(CToolFrameRange *toolFrameRange) = 0;

   virtual void RequestFrame(int port, int frameIndex) {return;}
   virtual void RemoveRequest(int port, int frameIndex) {return;}

   virtual int RegisterWithSystem() {return 0;}

   virtual int AssignAllBufferAllocators();
   virtual void ReleaseAllBufferAllocators();

   CToolSystemInterface* GetSystemAPI();

   // These are hacks to bypass a writer tool's regular tool processor and
   // instead simply write a single frame synchronously or asynchronously
   virtual int WriteFrameSynchronous(int inPortIndex,
                                     CToolFrameBuffer *frameBuffer);
   virtual int WriteFrameAsynchronous(int inPortIndex,
                                     CToolFrameBuffer *frameBuffer);

protected:

   typedef vector<CToolFrameBuffer*>  ToolFrameVector;

   struct SToolProcessingData
   {
      int iteration;          // Iteration number, starts with 0
      bool frameDoneFlag;     // Set by DoProcess to false when DoProcess
                              // wants to continue processing on this
                              // particular iteration.  Allows very slow
                              // processing to return control so we can
                              // check for commands
      const bool *emergencyStopFlagPtr;   // Pointer to emergency stop flag
                                          // owned by CToolSetup.  Flag is
                                          // normally set to false.  Will be
                                          // set to true if an immediate
                                          // stop to processing is needed.
                                          // DoProcess function should monitor
                                          // this flag and return if set to
                                          // true
   };

protected:
   virtual int StartProcessThread();

   // Tool's functions called from within context of the processing loop.
   // These functions are provided by the CToolProcessor derived type.
   virtual int SetParameters(CToolParameters *toolParams) = 0;
   virtual int BeginProcessing(SToolProcessingData &procData) = 0;
   virtual int EndProcessing(SToolProcessingData &procData) = 0;
   virtual int BeginIteration(SToolProcessingData &procData) = 0;
   virtual int EndIteration(SToolProcessingData &procData) = 0;
   virtual int DoProcess(SToolProcessingData &procData) = 0;
   virtual int GetIterationCount(SToolProcessingData &procData) = 0;

   int GetToolCommand(CAutotoolCommand &command);
   int PutToolStatus(CAutotoolStatus &status);

   CToolFrameBuffer* GetFrame(int port);
   CToolFrameBuffer* GetFrame(int port, int frameIndex);
   bool PutFrame(int port, CToolFrameBuffer *frameBuffer);
   bool IsOutPortFull(int port);
   int PutFrameSynchronous(int outPortIndex, CToolFrameBuffer *frameBuffer);
   int PutFrameAsynchronous(int outPortIndex, CToolFrameBuffer *frameBuffer);
   void CopyInputFrameBufferToOutputFrameBuffer(CToolFrameBuffer* inToolFrameBuffer,
                                                CToolFrameBuffer* outToolFrameBuffer);

   // Functions to setup the Tool's input and output handling prior
   // to starting processing.  Typically called from within the
   // SetParameters member function of the derived CToolProcessor subclass.
   int SetInputMaxFrames(int inPortIndex, int maxFrames, int maxNewFrames);
   int SetOutputModifyInPlace(int outPortIndex, int inPortIndex,
                              bool cloneForOutput);
   int SetOutputNewFrame(int outPortIndex, int inPortIndex);
   int SetOutputNewFrame(int outPortIndex, const CImageFormat *newImageFormat,
                         double newInvisibleFieldsPerFrame = 0.0);
   int SetOutputMaxFrames(int outPortIndex, int maxFrames);
   int SetIterationCount(int newIterationCount);

   const CImageFormat *GetSrcImageFormat(int inPortIndex) const;
   void SetDstImageFormat(int outPortIndex,
                          const CImageFormat *newImageFormat);
   double GetSrcInvisibleFieldsPerFrame(int inPortIndex) const;
   void SetDstInvisibleFieldsPerFrame(int outPortIndex,
                                      double newInvisibleFieldsPerFrame);

   // Functions to specify the input frames, output frames and frames to
   // be released.  Typically called from within the ProcessSetup function
   // of the derived CToolProcessor subclass
   int SetInputFrames(int inPortIndex, int frameCount, int *frameIndexList);
   int SetFramesToRelease(int inPortIndex, int frameCount, int *frameIndexList);
   int SetOutputFrames(int outPortIndex, int frameCount, int *frameIndexList);
   void SetSaveToHistory(bool newSaveToHistory);

   // Functions to get frames that were requested by SetInputFrames and
   // SetOutputFrames.
   CToolFrameBuffer* GetInputFrame(int inPortIndex, int frameIndex);
   CToolFrameBuffer* GetRequestedInputFrame(int inPortIndex, int requestIndex);
   CToolFrameBuffer* GetOutputFrame(int outPortIndex, int frameIndex);
   CToolFrameBuffer* GetRequestedOutputFrame(int outPortIndex, int requestIndex);

   CBufferPool* GetOutputBufferAllocator(int outPortIndex);

   int GetInPortCount() const;
   int GetOutPortCount() const;

   int PutEndOfMaterialFrames();

   bool IsEmergencyStop() const;
   const bool* GetEmergencyStopFlagPtr() const;
   
   void FlushDstFrameQueues();

   IToolProgressMonitor *GetToolProgressMonitor();

private:

   typedef vector<int> FrameIndexList;

   // SInPort - structure that holds data associated with a single
   //           Input Port
   struct SInPort
   {
      int maxFrames;  // Maximum number of input frames that the tool will
                      // require from the input port for processing.  Used to
                      // allocate the right number of buffers, but does not
                      // actually limit how many frames the tool can request
                      // at once

      FrameIndexList requestList;       // Desired frame indices
      FrameIndexList releaseList;       // Frame indices to release after
                                        // processing

      ToolFrameVector inFrameList;             // Frames ptrs after GetFrame
      list<CToolFrameBuffer*> availableFrames; // Frames previously GetFrame'd
                                               // but not yet released
   };

   // SOutPort - structure that holds data associated with a single
   //            Output Port
   struct SOutPort
   {
      int maxFrames;           // Maximum number of frames that will be output
                               // to the output port from processing
      bool modifyInPlace;      // If true, then the output frame will be an
                               // input frame that has been modified in place.
                               // If false, then the output frame is new.
      bool cloneForOutput;     // Copy the modified input frame to a
                               // new output frame
      int inPortIndex;         // Input port that is used as the source
                               // for the modified-in-place frames.  Only used
                               // when modifyInPlace flag is true.

      double invisibleFieldsPerFrame; // Correction factor so enough
                                      // frame buffers are allocated
                                      // for invisible fields.
                                      // Typically .5 for film-frames
                                      // with 3:2 pull down, 0 otherwise

      const CImageFormat *imageFormat; // Image format for new frames
      CBufferPool *bufferAllocator;  // Buffer allocator used if output frame
                                     // is to be a new frame

      FrameIndexList requestList;    // Desired frame indices to output

      ToolFrameVector outFrameList;  // Ptrs to output frames, may  be input
                                     // frames or new frames
   };

private:
   static void ProcessThread(void* vpToolProc, void* vpBThread);
   void ProcessLoop(void* vpBThread);

   void InitProcessingData(SToolProcessingData &procData);
   void PrepareProcessingData(SToolProcessingData &procData);

   bool GetInput(bool *activity);
   bool GetInputFromInPort(int inPortIndex, bool *activity);
   int GetEndOfMaterialFrames();
   bool SetOutput(bool *activity);
   bool ReleaseBuffers(bool *activity);
   bool PutOutput(bool *activity);
   void OutputFrame(int port, CToolFrameBuffer *frameBuffer);

   void AddAvailableFrame(SInPort &inPort, CToolFrameBuffer *frameBuffer);
   void FlushAvailableFrames(SInPort &inPort);
   CToolFrameBuffer* GetAvailableFrame(SInPort &inPort, int frameIndex);
   void RemoveAvailableFrame(SInPort &inPort, int frameIndex);
   void DeleteInputFrame(int inPortIndex, int frameIndex);

   int FlushBuffers();

   void InitializePortDefs();
   int SetInPortBufferAllocator(int inPortIndex);
   int SetOutPortBufferAllocator(SOutPort &outPort, bool assign);

private:
   int toolNumber;
   string toolName;
   
   CToolNode *node;
   CToolSystemInterface *systemAPI;

   const bool *emergencyStopFlagPtr;         // Pointer to emergency stop
                                             // flag owned by the CToolSetup

   AutotoolCommandQueue *toolCommandQueue;
   AutotoolStatusQueue *toolStatusQueue;

   void *vpProcessThread;

   // Tool's Input and Output Port data
   vector<SInPort> inPortList;
   vector<SOutPort> outPortList;

   bool saveToHistory;

   IToolProgressMonitor *toolProgressMonitor;

};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLOBJECT_H)



