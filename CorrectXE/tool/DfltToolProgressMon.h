#ifndef DfltToolProgressMonH
#define DfltToolProgressMonH

/*
 * Default Tool Progress Monitor Class
 */

#include "ToolProgressMonitor.h"
class CHRTimer;
class CThreadLock;
#include <string>
#include <stack>
#include <tuple>
using std::string;
using std::stack;

class CDefaultToolProgressMonitor : public IToolProgressMonitor
{
public:
    CDefaultToolProgressMonitor();
    virtual ~CDefaultToolProgressMonitor(void);

    virtual void SetIdle(bool setMessage=true);
    virtual void SetStatusMessage(const char *newMessage);
    virtual void PushStatusMessage(const char *newMessage);
    virtual void PopStatusMessage(void);
    virtual void SetToolMessage(const char *newMessage);
	 virtual void SetFrameCount(int newFrameCount, int newBumpsPerFrame=1, int preElapsedFrames=0, double timePerFrameInSeconds=0.0);
	 virtual void StartProgress(void);
    virtual void BumpProgress(int bumpCount=1);
	 virtual void StopProgress(bool processedToEnd=true);
    virtual void Push();
    virtual void Pop();
	 virtual double GetTimePerFrameInSeconds();

protected:
    CHRTimer *_HRTimer;
    CThreadLock *_ThreadLock;
    int _FramesTotal;
    int _BumpsPerFrame;
    int _FramesSoFar;
    int _Bumps;
    double _ElapsedTimeInSeconds;
    double _TimePerFrameInSeconds;
    string _StatusMessage;
    string _ToolMessage;
    stack<string> _StatusMessageStack;
    stack<std::tuple<int, int, int, int, double>> _StateStack;

    void setFrameCountInternal(int newFrameCount, int newClicksPerFrame, int preElapsedFrames, double timePerFrameInSeconds);
    void bumpFrameInternal(int bumpCount);
    string formatDuration(double durationInSeconds, bool showFraction=false);

    virtual void showProgress();
};

#endif // DfltToolProgressMonH
