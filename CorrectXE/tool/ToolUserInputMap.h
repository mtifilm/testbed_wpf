// ToolUserInputMap.h: interface for the CToolUserInputMap class.
//
/*
$Header: /usr/local/filmroot/tool/include/ToolUserInputMap.h,v 1.1.1.1 2001/07/25 14:16:45 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(TOOLUSERINPUTMAP_H)
#define TOOLUSERINPUTMAP_H

#include "UserInput.h"

//////////////////////////////////////////////////////////////////////

class CUserInputConfiguration  
{
public:

struct SConfigItem
{
   int commandNumber;      // Command Number.  Uniquely identifies a
                           // command for a tool. Must be a non-negative
                           // number
   const char* keyMap;     // Key Map.  Pointer to NULL terminated string
                           // that defines the keyboard key or mouse
                           // button that corresponds to the command number
};

public:
	CUserInputConfiguration(int newConfigItemCount, 
                           SConfigItem *newConfigItemTable);
	virtual ~CUserInputConfiguration();

	const SConfigItem *getConfigItem(int index) const;
	int getConfigItemCount() const;

private:
   int configItemCount;
   SConfigItem *configItemTable;
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

class CToolUserInputMapEntry
{
public:
   CToolUserInputMapEntry(const CToolUserInputMapEntry &mapEntry);
	CToolUserInputMapEntry(CUserInput &newUserInput, int newCommandNumber);
   virtual ~CToolUserInputMapEntry();

public:
   CUserInput userInput;

   int commandNumber;               // Tool's Command Number

   CToolUserInputMapEntry *forwardLink;
};

//////////////////////////////////////////////////////////////////////

class CToolUserInputMap  
{
public:
   CToolUserInputMap();
   virtual ~CToolUserInputMap();

   int findCommand(CUserInput &targetUserInput);

   void addMapEntry(CUserInput &newUserInput, int newCommandNumber);
   void removeMapEntry(CUserInput &targetUserInput);

private:
   CToolUserInputMapEntry *mapHead;
   CToolUserInputMapEntry *mapTail;

   CToolUserInputMapEntry* findEntry(CUserInput &targetUserInput);
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(TOOLUSERINPUTMAP_H)
