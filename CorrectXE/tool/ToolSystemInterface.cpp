// ToolSystemInterface.cpp: implementation of the CToolSystemInterface class.
//
// Note: Since CToolSystemInterface is an Abstract Base Class that defines
//       an interface, this .cpp file will not have much in it.
//       
/*
$Header: /usr/local/filmroot/tool/ToolObj/ToolSystemInterface.cpp,v 1.2 2006/01/13 18:21:07 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolSystemInterface.h"
#include "ToolMediaIO.h"
#include "ToolObject.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CToolSystemInterface::CToolSystemInterface()
{

}

CToolSystemInterface::~CToolSystemInterface()
{

}


