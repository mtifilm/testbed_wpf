//---------------------------------------------------------------------------

#ifndef SecuriteFrameH
#define SecuriteFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include "ParamPanelForm.h"
#include <Buttons.hpp>
#include <ImgList.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "License.h"

//---------------------------------------------------------------------------
class TSecurityPanel : public TParamPanel
{
__published:	// IDE-managed Components
        TPanel *OldPanel;
        TLabel *Label9;
        TSpeedButton *DongleFileResetButton;
        TSpeedButton *DongleFileBrowseButton;
        TEdit *DongleFileEdit;
        TGroupBox *DongleGroup;
        TLabel *Label7;
        TLabel *Label8;
        TBitBtn *DetectDongleButton;
        TBitBtn *InitDongleButton;
        TBitBtn *RegisterDongletButton;
        TCSpinEdit *DonglePortSEdit;
        TEdit *SystemIDEdit;
        TGroupBox *StatusGroupBox;
        TLabel *LeaseTimeLabel;
        TLabel *Label15;
        TBitBtn *RefreshDongleButton;
        TBitBtn *TimeDongleButton;
        TPanel *FlexPanel;
        TLabel *Label6;
        TSpeedButton *CheckFeatureButton;
        TSpeedButton *FindAllLicButton;
        TComboBox *FeatureEdit;
        TListView *LicenseListView;
        TTimer *DirtyTimer;
        TGroupBox *GroupBox2;
        TLabel *Label1;
        TLabel *Label5;
        TLabel *LicenseLabel;
        TSpeedButton *AuthorizeLicHelpButton;
        TLabel *LastUsedTimeLabel;
        TEdit *HostIDEdit;
        TEdit *DongleIDEdit;
        TLabel *Label2;
        TRadioGroup *DongleAccessGroup;
        TGroupBox *GroupBox3;
        TLabel *PortLabel;
        TCSpinEdit *PortSpinEdit;
        TBitBtn *AutoDetectButton;
        TBitBtn *CheckDongleButton;
        TPanel *FlexFilePanel;
        TEdit *LicPathEdit;
        TLabel *LicPathLabel;
        TSpeedButton *LicFileBrowseButton;
        TSpeedButton *NetworkButton;
        void __fastcall DongleDetectButtonClick(TObject *Sender);
        void __fastcall InitDongleButtonClick(TObject *Sender);
        void __fastcall RefreshDongleButtonClick(TObject *Sender);
        void __fastcall RegisterDongletButtonClick(TObject *Sender);
        void __fastcall TimeDongleButtonClick(TObject *Sender);
        void __fastcall DongleFileResetButtonClick(TObject *Sender);
        void __fastcall DongleFileBrowseButtonClick(TObject *Sender);
        void __fastcall DongleFileEditChange(TObject *Sender);
        void __fastcall DonglePortSEditChange(TObject *Sender);
        void __fastcall AutoDetectButtonClick(TObject *Sender);
        void __fastcall CheckDongleButtonClick(TObject *Sender);
        void __fastcall FindAllLicButtonClick(TObject *Sender);
        void __fastcall PortSpinEditChange(TObject *Sender);
        void __fastcall CheckFeatureButtonClick(TObject *Sender);
        void __fastcall AuthorizeLicHelpButtonClick(TObject *Sender);
        void __fastcall DirtyTimerTimer(TObject *Sender);
        void __fastcall LicFileBrowseButtonClick(TObject *Sender);
        void __fastcall LicenseListViewDblClick(TObject *Sender);
        void __fastcall DongleAccessGroupClick(TObject *Sender);
        void __fastcall NetworkButtonClick(TObject *Sender);

protected:
     void __fastcall SetAdminPriv(bool Value);

private:	// User declarations
       bool IsDongleInitialized(int iPort);
       bool CheckInitAndRegistered(int Port);
       bool InitializeDongle(int iPort);
       bool RegisterSoftware(void);
       int  iGhostPort;

       MTI_UINT64 ullSysID;

       bool UpdateDongleInfo(void);
       void FindHostIDClick(void);
       String AuthorizeLicenseInfo;
       bool bDongleStatusChanged;
       int DongleOldStatus;

       void SetAdapterNumber(int nAdapt);
       int GetAdapterNumber(void);
       IntegerList _PortNumbers;   // This maps the selected dongle adapter to the current port

public:		// User declarations
       bool Read(void);
       bool Apply(void);
       void Restore(void);

        __fastcall TSecurityPanel(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSecurityPanel *SecurityPanel;
//---------------------------------------------------------------------------
#endif
