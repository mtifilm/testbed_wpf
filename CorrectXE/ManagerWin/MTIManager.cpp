//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("MTIManagerUnit.cpp", MainForm);
USEFORM("FormatDialogUnit.cpp", FormatDialog);
USEFORM("DefineMachineDialog.cpp", MachineDefineDialog);
USEFORM("ExtendClipMediaDialog.cpp", ClipExtendMediaDlg);
USEFORM("ParamPanelForm.cpp", ParamPanel); /* TFrame: File Type */
USEFORM("VTRFrame.cpp", VTRPanelFrame); /* TFrame: File Type */
USEFORM("SecuriteFrame.cpp", SecurityPanel); /* TFrame: File Type */
USEFORM("DDRFrame.cpp", DDRPanel); /* TFrame: File Type */
USEFORM("ClipFrame.cpp", ClipPanel); /* TFrame: File Type */
USEFORM("SiteFrame.cpp", SitePanel); /* TFrame: File Type */
USEFORM("AboutUnit.cpp", AboutForm);
USEFORM("RawDiskAddFrame.cpp", VStorePanel); /* TFrame: File Type */
USEFORM("AutoDetectUnit.cpp", AutoDetectForm);
USEFORM("FlexServerUnit.cpp", FlexServerDialog);
USEFORM("LicenseInfo.cpp", LicenseInfoForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
         Application->CreateForm(__classid(TFormatDialog), &FormatDialog);
         Application->CreateForm(__classid(TMachineDefineDialog), &MachineDefineDialog);
         Application->CreateForm(__classid(TClipExtendMediaDlg), &ClipExtendMediaDlg);
         Application->CreateForm(__classid(TAutoDetectForm), &AutoDetectForm);
         Application->CreateForm(__classid(TFlexServerDialog), &FlexServerDialog);
         Application->CreateForm(__classid(TLicenseInfoForm), &LicenseInfoForm);
         Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
