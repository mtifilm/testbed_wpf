//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SiteFrame.h"
#include "DllSupport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ParamPanelForm"
//#pragma link "CSPIN"
#pragma resource "*.dfm"

TSitePanel *SitePanel;
//---------------------------------------------------------------------------
__fastcall TSitePanel::TSitePanel(TComponent* Owner)
        : TParamPanel(Owner)
{
   Caption = "Site";
   Details = false;

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TSitePanel::BinRootResetButtonClick(TObject *Sender)
{
    BinRootEdit->Text = DEFAULT_BIN_PATH;
}

void __fastcall TSitePanel::LogFileDirEditChange(TObject *Sender)
{
    Modified = true;
    ChangeHintOnEdit(Sender);
}


//-------------LocalMachineEditChange-------------John Mertus----Oct 2002-----

  void __fastcall TSitePanel::LocalMachineEditChange(TObject *Sender)

//  Called when the Local machine site has changed.  This modifies the site.
//
//****************************************************************************
{
    if (EqualIgnoreCase(GetMTIEnv("CPMP_LOCAL_DIR"), LocalMachineEdit->Text.c_str())) return;
    Modified = true;
    ChangeHintOnEdit(Sender);
}

//-------------SharedDirectoryEditChange-------------John Mertus----Oct 2002-----

  void __fastcall TSitePanel::SharedDirectoryEditChange(TObject *Sender)

//  Called when the shared directory site has changed.  This modifies the site.
//
//****************************************************************************
{
    if (EqualIgnoreCase(GetMTIEnv("CPMP_SHARED_DIR"), SharedDirectoryEdit->Text.c_str())) return;
    Modified = true;
    ChangeHintOnEdit(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TSitePanel::LogFileDirResetButtonClick(TObject *Sender)
{
   LogFileDirEdit->Text = DEFAULT_LOG_PATH;
}
//---------------------------------------------------------------------------


void __fastcall TSitePanel::BinRootBrowseButtonClick(TObject *Sender)
{
    string sName = BinRootEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    string newDir = GetUserFolder(Handle, sName.c_str(), "Clip Root Directory");
    if (!newDir.empty())
      BinRootEdit->Text = newDir.c_str();
}
//---------------------------------------------------------------------------

void __fastcall TSitePanel::LogFileDirBrowseButtonClick(TObject *Sender)
{
    string sName = LogFileDirEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    string newDir = GetUserFolder(Handle, sName.c_str(), "Log Files Directory");
    if (!newDir.empty())
      LogFileDirEdit->Text = newDir.c_str();
}

//---------------------------------------------------------------------------
//----------GetSystemEnvironmentVariable----------John Mertus----Oct 2002-----

     string GetSystemEnvironmentVariable(const char *envar, bool bSystemEnv=true)

//  This gets the current environment variables
//    envar is the variable to decode
//    bSystemEnv is true for the Systemwide defaults,
//                  false for the user defaults.
//
//****************************************************************************
{
  char subkey[256], value[256];
  HKEY hkeyroot;
  HKEY hk;

  if (bSystemEnv)
   {
     hkeyroot = HKEY_LOCAL_MACHINE;
     strcpy(subkey, "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment");
   }
 else
   {
     hkeyroot = HKEY_CURRENT_USER;
     strcpy(subkey, "Environment");
   }

 if (RegOpenKeyEx(hkeyroot, subkey, 0, KEY_QUERY_VALUE, &hk) != ERROR_SUCCESS)
  return ""; // error

 DWORD vs = 255;
 DWORD Reg;
 if (RegQueryValueEx(hk, envar, NULL, &Reg, value, &vs) ==
      ERROR_SUCCESS) {

   string Result = value;
   return Result;
  }
  else
   return ""; //error
 }

//----------SetSystemEnvironmentVariable----------John Mertus----Oct 2002-----

  bool SetSystemEnvironmentVariable(char *envar, char *value, bool bSystemEnv=true)

//  This sets the current environment variables
//    envar is the variable to set
//    value is its value
//    bSystemEnv is true for the Systemwide defaults,
//                  false for the user defaults.
//
//****************************************************************************
                        {
  char subkey[256];
  HKEY hkeyroot;
  HKEY hk;

  // Choose which variable to use
  if (bSystemEnv)
   {
     hkeyroot = HKEY_LOCAL_MACHINE;
     strcpy(subkey, "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment");
   }
  else
   {
     hkeyroot = HKEY_CURRENT_USER;
     strcpy(subkey, "Environment");
   }

  // Set it
  if (RegOpenKeyEx(hkeyroot, subkey, 0, KEY_SET_VALUE, &hk) != ERROR_SUCCESS)
   return false; // error

  if (RegSetValueEx(hk, envar, 0, REG_EXPAND_SZ, value, strlen(value)+1) ==
      ERROR_SUCCESS)
   {

      DWORD RetCode;
      SendMessageTimeout (HWND_BROADCAST, WM_WININICHANGE,
		0, (LPARAM) "Environment",
		SMTO_ABORTIFHUNG, 5000, &RetCode);

      // We have changed the string on the global level, now change it
      // for the current program
      string s;
      s = envar;
      s = s + "=" + value;
      putenv(s.c_str());

      return true;
    }
  else
   return false; //error
 }


//*************************Site Configuration********************************

       bool TSitePanel::Read(void)
{
    StringList sl;
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    ini->ReadStringList("BinDirectories","Root", &sl);
    if (sl.size() > 0) BinRootEdit->Text = sl[0].c_str();


    //bool bDirectX = ini->ReadBool ("General", "DirectXGraphics", true);
    //CheckBox1->Checked = 1-bDirectX;

    DeleteIniFile(ini);

    LocalMachineEdit->Text = GetMTIEnv("CPMP_LOCAL_DIR").c_str();
    SharedDirectoryEdit->Text = GetMTIEnv("CPMP_SHARED_DIR").c_str();

    // Read the Trace levels
    ini = CPMPOpenMasterIniFile();
    if (ini == NULL) return false;   // Error is set

    TraceLevelSedit->Value = ini->ReadInteger("TraceOptions", "TraceLevel", 0);
    LogLevelSEdit->Value = ini->ReadInteger("TraceOptions", "LogLevel", 0);
    VersionsSEdit->Value = ini->ReadInteger("TraceOptions", "Versions", 5);
    LogFileDirEdit->Text = ini->ReadString("TraceOptions", "LogPath", DEFAULT_LOG_PATH).c_str();


    DeleteIniFile(ini);

    Modified = false;
    return true;

}

       bool TSitePanel::Apply(void)
{
    StringList sl;
    // Write the bin roots
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    ini->ReadStringList("BinDirectories","Root", &sl);
    if (sl.size() > 0)
      {
        sl[0] = BinRootEdit->Text.c_str();
      }
    else
      {
        sl.push_back(BinRootEdit->Text.c_str());
      }
    ini->WriteStringList("BinDirectories","Root", &sl);

    //ini->WriteBool ("General", "DirectXGraphics", (1-CheckBox1->Checked));
    DeleteIniFile(ini);

    // Write the Debug levels
    ini = CPMPOpenMasterIniFile();
    if (ini == NULL) return false;   // Error is set

    ini->WriteInteger("TraceOptions", "TraceLevel", TraceLevelSedit->Value);
    ini->WriteInteger("TraceOptions", "LogLevel", LogLevelSEdit->Value);
    ini->WriteInteger("TraceOptions", "Versions", VersionsSEdit->Value);
    ini->WriteString("TraceOptions", "LogPath", LogFileDirEdit->Text.c_str());

    DeleteIniFile(ini);

    //
    //  Because these generate changes that affect other panels, make sure
    //  there is a real need to change these.
    if (!EqualIgnoreCase(GetMTIEnv("CPMP_LOCAL_DIR"), LocalMachineEdit->Text.c_str()))
      if (!SetSystemEnvironmentVariable("CPMP_LOCAL_DIR", LocalMachineEdit->Text.c_str()))
        _MTIErrorDialog(Handle, "Could not set the LocalMachine Directory");

    if (!EqualIgnoreCase(GetMTIEnv("CPMP_SHARED_DIR"), SharedDirectoryEdit->Text.c_str()))
      if (!SetSystemEnvironmentVariable("CPMP_SHARED_DIR", SharedDirectoryEdit->Text.c_str()))
        _MTIErrorDialog(Handle, "Could not set the Shared Directory");

    Modified = false;
    return true;
}

void __fastcall TSitePanel::LocalMachineResetButtonClick(TObject *Sender)
{
    LocalMachineEdit->Text = CPMPGetDefaultLocalDir().c_str();
}
//---------------------------------------------------------------------------

void __fastcall TSitePanel::SharedDirectoryResetButtonClick(TObject *Sender)
{
    SharedDirectoryEdit->Text = DEFAULT_SHARED_DIR;
}



void __fastcall TSitePanel::SharedDirectoryBrowseButtonClick(TObject *Sender)
{
    string sName = SharedDirectoryEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    string newDir = GetUserFolder(Handle, sName.c_str(), "Shared Directory");
    if (!newDir.empty())
      SharedDirectoryEdit->Text = newDir.c_str();
}

//---------------------------------------------------------------------------

void __fastcall TSitePanel::LocalMachineBrowseButtonClick(TObject *Sender)
{
    string newDir = GetUserFolder(Handle,
          LocalMachineEdit->Text.c_str(), "Local Machine Directory");
    if (!newDir.empty())
      LocalMachineEdit->Text = newDir.c_str();
}

//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TSitePanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
   TParamPanel::SetAdminPriv(Value);
   if (AdminPrivilege)
     {
       LocalMachineEdit->ReadOnly = false;
       LocalMachineEdit->Color = clWindow;
       LocalMachineResetButton->Enabled = true;
       LocalMachineBrowseButton->Enabled = true;
     }
   else
     {
       LocalMachineEdit->ReadOnly = true;
       LocalMachineEdit->Color = clBtnFace;
       LocalMachineResetButton->Enabled = false;
       LocalMachineBrowseButton->Enabled = false;
     }

   // Now make the rest alikte
   SharedDirectoryEdit->ReadOnly = LocalMachineEdit->ReadOnly;
   SharedDirectoryEdit->Color = LocalMachineEdit->Color;
   SharedDirectoryResetButton->Enabled = LocalMachineResetButton->Enabled;
   SharedDirectoryBrowseButton->Enabled = LocalMachineBrowseButton->Enabled;

   BinRootEdit->ReadOnly = LocalMachineEdit->ReadOnly;
   BinRootEdit->Color = LocalMachineEdit->Color;
   BinRootResetButton->Enabled = LocalMachineResetButton->Enabled;
   BinRootBrowseButton->Enabled = LocalMachineBrowseButton->Enabled;

   LogFileDirEdit->ReadOnly = LocalMachineEdit->ReadOnly;
   LogFileDirEdit->Color = LocalMachineEdit->Color;
   LogFileDirResetButton->Enabled = LocalMachineResetButton->Enabled;
   LogFileDirBrowseButton->Enabled = LocalMachineBrowseButton->Enabled;
}

void __fastcall TSitePanel::AuditChange(TObject *Sender)
{
    Modified = true;
}
//---------------------------------------------------------------------------
#if 0
void __fastcall TSitePanel::CheckBox1Click(TObject *Sender)
{
  Modified = true;
}
#endif
//---------------------------------------------------------------------------

void __fastcall TSitePanel::BinRootEditChange(TObject *Sender)
{
    Modified = true;
    ChangeHintOnEdit(Sender);
}
//---------------------------------------------------------------------------

