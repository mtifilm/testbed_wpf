//---------------------------------------------------------------------------

#ifndef SiteFrameH
#define SiteFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ParamPanelForm.h"
#include <ImgList.hpp>
#include "cspin.h"
#include <Buttons.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TSitePanel : public TParamPanel
{
__published:	// IDE-managed Components
        TLabel *Label2;
        TEdit *LocalMachineEdit;
        TSpeedButton *LocalMachineResetButton;
        TSpeedButton *LocalMachineBrowseButton;
        TSpeedButton *SharedDirectoryBrowseButton;
        TSpeedButton *SharedDirectoryResetButton;
        TEdit *SharedDirectoryEdit;
        TEdit *BinRootEdit;
        TLabel *Label4;
        TLabel *Label21;
        TEdit *LogFileDirEdit;
        TSpeedButton *LogFileDirResetButton;
        TSpeedButton *LogFileDirBrowseButton;
        TSpeedButton *BinRootBrowseButton;
        TSpeedButton *BinRootResetButton;
        TGroupBox *GroupBox1;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TCSpinEdit *TraceLevelSedit;
        TCSpinEdit *LogLevelSEdit;
        TCSpinEdit *VersionsSEdit;
        TLabel *Label3;
        void __fastcall LocalMachineResetButtonClick(TObject *Sender);
        void __fastcall SharedDirectoryBrowseButtonClick(TObject *Sender);
        void __fastcall SharedDirectoryResetButtonClick(TObject *Sender);
        void __fastcall LocalMachineBrowseButtonClick(TObject *Sender);
        void __fastcall LocalMachineEditChange(TObject *Sender);
        void __fastcall LogFileDirResetButtonClick(TObject *Sender);
        void __fastcall BinRootBrowseButtonClick(TObject *Sender);
        void __fastcall LogFileDirBrowseButtonClick(TObject *Sender);
        void __fastcall LogFileDirEditChange(TObject *Sender);
        void __fastcall BinRootResetButtonClick(TObject *Sender);
        void __fastcall SharedDirectoryEditChange(TObject *Sender);
        void __fastcall AuditChange(TObject *Sender);
        void __fastcall BinRootEditChange(TObject *Sender);
        //void __fastcall CheckBox1Click(TObject *Sender);

private:	// User declarations

protected:
     void __fastcall SetAdminPriv(bool Value);

public:		// User declarations
        __fastcall TSitePanel(TComponent* Owner);
       // Configuration routines
       bool Read(void);
       bool Apply(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TSitePanel *SitePanel;
//---------------------------------------------------------------------------
#endif
