//----------------------------------------------------------------------------
#ifndef FlexServerUnitH
#define FlexServerUnitH
//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TFlexServerDialog : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TLabel *Label3;
        TEdit *ServerNameEdit;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
private:
public:
	virtual __fastcall TFlexServerDialog(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TFlexServerDialog *FlexServerDialog;
//----------------------------------------------------------------------------
#endif    
