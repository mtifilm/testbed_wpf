//---------------------------------------------------------------------------

#ifndef AutoDetectUnitH
#define AutoDetectUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include "machine.h"

enum {AUTO_SUCCESS, AUTO_FAILED, AUTO_USER_ABORT, AUTO_NOT_FOUND};

//---------------------------------------------------------------------------
class TAutoDetectForm : public TForm
{
__published:	// IDE-managed Components
        TBitBtn *CancelButton;
        TBitBtn *OkButton;
        TLabel *Label1;
        TGroupBox *GroupBox1;
        TCheckBox *SerialPortCBox;
        TCheckBox *USBCBox;
        TCheckBox *ParallelCBox;
        TStatusBar *StatusBar;
        void __fastcall OkButtonClick(TObject *Sender);
private:	// User declarations
       int   *_PortNum;
       int   *_PortType;
       string CompanyName, LicenseName, ErrString;
       void UpdateStatusBar(const string &str, int nP);
       int CheckSerial(void);
       int CheckParallel(void);
       int CheckUSB(void);
       int CheckForOldDongle(void);       

       
public:		// User declarations
        __fastcall TAutoDetectForm(TComponent* Owner);
       int __fastcall ShowCustomModal(int &PortNum, int &PortType);

};
//---------------------------------------------------------------------------
extern PACKAGE TAutoDetectForm *AutoDetectForm;
//---------------------------------------------------------------------------
#endif
