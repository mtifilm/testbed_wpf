//---------------------------------------------------------------------------

#ifndef VTRFrameH
#define VTRFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ParamPanelForm.h"
#include "cspin.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "IniFile.h"
#include <Menus.hpp>
#include <ImgList.hpp>
#include <Dialogs.hpp>
#include "IniFile.h"

//---------------------------------------------------------------------------
// Storage for the timing Scheme Properties
// Very ugly, but quick and dirty


typedef struct SchemeDataStruct
{
  string Name;

  int OffsetToVTR;
  int OffsetToComputer;
  int BackupOnPickup;
  int AllowableJitter;
  int FrameStart;
  int FrameEnd;
  bool IssueInhibit;
  int TimeSenseType;
  //
  //  Inheritence bits
  bool bi_OffsetToVTR;
  bool bi_OffsetToComputer;
  bool bi_BackupOnPickup;
  bool bi_AllowableJitter;
  bool bi_FrameStart;
  bool bi_FrameEnd;
  bool bi_IssueInhibit;
  bool bi_TimeSenseType;

  StringList Formats;
  SchemeDataStruct(void);

  // I/O routines
  void Write(CIniFile *ini);

   // Some routines for conversion
   int GetTimeSenseIndex(void) const;
   void SetTimeSenseIndex(int Value);
   string GetTimeSenseHint(void) const;

} SchemeData;

typedef struct MachineSchemeDataStruct
{
  string Name;                            // Machine Name
  StringList Sections;                    // List of the sections

  // Actual Parameters
  bool SupportsAutoEdit;
  int InstructionDelay;
  int PrerollHint;
  int PostrollHint;
  bool SetSensePreroll;

  //  Inheritence bits
  bool bi_SupportsAutoEdit;
  bool bi_InstructionDelay;
  bool bi_PrerollHint;
  bool bi_PostrollHint;
  bool bi_SetSensePreroll;

  // Constructor
  MachineSchemeDataStruct(void);

  // I/O routines
  void Write(CIniFile *ini);
  void Read(CIniFile *ini);

} MachineSchemeData;

class TVTRPanelFrame : public TParamPanel
{
__published:	// IDE-managed Components
        TLabel *Label20;
        TLabel *SchemataLabel;
        TListView *MachineListView;
        TListBox *SchemataListBox;
        TListBox *FormatListBox;
        TLabel *Label14;
        TEdit *TimingFileEdit;
        TSpeedButton *TimingFileResetButton;
        TSpeedButton *TimingFileBrowseButton;
        TLabel *Label6;
        TBitBtn *VTRDetectButton;
        TBitBtn *VTRQueryButton;
        TCSpinEdit *SerialPortSEdit;
        TLabel *Label23;
        TBevel *Bevel2;
        TLabel *FormatLabel;
        TRadioGroup *VTRGroup;
        TPopupMenu *MachineTypePopup;
        TMenuItem *MachineNewMenu;
        TMenuItem *MachineEditMenu;
        TMenuItem *MachineDeleteMenu;
        TPopupMenu *SchemataPopupMenu;
        TMenuItem *SchemataNewMenu;
        TMenuItem *SchemataInsertMenu;
        TMenuItem *SchemataDeleteMenu;
        TPopupMenu *FormatPopupMenu;
        TMenuItem *FormatInsertMenu;
        TMenuItem *FormatDeleteMenu;
        TPopupMenu *TimeSensePopupMenu;
        TMenuItem *BestBest1;
        TMenuItem *LTCLongitudinalTimeCode1;
        TMenuItem *CLTCCorrectLTC1;
        TMenuItem *VITCVerticalIntervalTimecode1;
        TComboBox *ComputerDropList;
        TPanel *FormatPanel;
        TGroupBox *DefaultGroupBox;
        TCheckBox *CheckBox20;
        TCSpinEdit *DOffsetToVTRSEdit;
        TCSpinEdit *DOffsetToComputerSEdit;
        TCSpinEdit *DBackupOnPickupSEdit;
        TCSpinEdit *DAllowableJitterSEdit;
        TCSpinEdit *DFrameStartSEdit;
        TCSpinEdit *DFrameEndSEdit;
        TComboBox *DTimeSenseComboBox;
        TComboBox *DIssueInhibitCBox;
        TGroupBox *SchemeGroup;
        TLabel *Label32;
        TLabel *Label31;
        TLabel *Label30;
        TLabel *Label29;
        TLabel *Label28;
        TLabel *Label25;
        TLabel *Label24;
        TLabel *Label1;
        TCSpinEdit *OffsetToVTRSEdit;
        TCSpinEdit *OffsetToComputerSEdit;
        TCSpinEdit *BackupOnPickupSEdit;
        TCSpinEdit *AllowableJitterSEdit;
        TCSpinEdit *FrameStartSEdit;
        TCSpinEdit *FrameEndSEdit;
        TCheckBox *UOffsetToVTRCBox;
        TCheckBox *UOffsetToComputerCBox;
        TCheckBox *UBackupOnPickupCBox;
        TCheckBox *UAllowableJitterCBox;
        TCheckBox *UFrameStartCBox;
        TCheckBox *UFrameEndCBox;
        TCheckBox *UIssueInhibitCBox;
        TCheckBox *UTimeSenseCBox;
        TComboBox *TimeSenseComboBox;
        TComboBox *IssueInhibitCBox;
        TPanel *MachinePanel;
        TGroupBox *MachineDefaultBox;
        TCheckBox *CheckBox1;
        TCSpinEdit *DInstructionDelaySEdit;
        TGroupBox *MachineGroupBox;
        TLabel *Label10;
        TLabel *Label11;
        TCSpinEdit *InstructionDelaySEdit;
        TCheckBox *USupportsAutoEditCBox;
        TCheckBox *UInstructionDelayCBox;
        TLabel *Label26;
        TCSpinEdit *PrerollSEdit;
        TCheckBox *UPrerollCBox;
        TLabel *Label27;
        TCSpinEdit *PostrollSEdit;
        TCheckBox *UPostrollCBox;
        TCSpinEdit *DPrerollSEdit;
        TCSpinEdit *DPostrollSEdit;
        TComboBox *DSupportsAutoEditCBox;
        TComboBox *SupportsAutoEditCBox;
        TGroupBox *GlobalGroupBox;
        TCheckBox *AssembleEditCBox;
        TPanel *InitialPanel;
        TLabel *Label2;
        TComboBox *SetSensePrerollCBox;
        TCheckBox *USetSensePrerollCBox;
        TComboBox *DSetSensePrerollCBox;
        TCheckBox *GlobalVTROffsetRecToComCheckBox;
        TCSpinEdit *GlobalVTROffsetRecToComSpinEdit;
        TCheckBox *GlobalComputerOffsetRecToVTRCheckBox;
        TCSpinEdit *GlobalComputerOffsetRecToVTRSpinEdit;
        void __fastcall MachineListViewSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
        void __fastcall SchemataListBoxClick(TObject *Sender);
        void __fastcall SerialPortSEditChange(TObject *Sender);
        void __fastcall VTRDefaultsChange(TObject *Sender);
        void __fastcall VTREnableDefaultsClick(TObject *Sender);
        void __fastcall SchemeChangedClick(TObject *Sender);

        void __fastcall FormatInsertMenuClick(TObject *Sender);
        void __fastcall FormatDeleteMenuClick(TObject *Sender);
        void __fastcall SchemataDeleteMenuClick(TObject *Sender);
        void __fastcall SchemataInsertMenuClick(TObject *Sender);
        void __fastcall SchemataPopupMenuPopup(TObject *Sender);
        void __fastcall FormatPopupMenuPopup(TObject *Sender);
        void __fastcall SchemataNewMenuClick(TObject *Sender);
        void __fastcall MachineTypePopupPopup(TObject *Sender);
        void __fastcall MachineDeleteMenuClick(TObject *Sender);
        void __fastcall MachineEditMenuClick(TObject *Sender);
        void __fastcall MachineNewMenuClick(TObject *Sender);
        void __fastcall VTRDetectButtonClick(TObject *Sender);
        void __fastcall VTRQueryButtonClick(TObject *Sender);
        void __fastcall TimingFileResetButtonClick(TObject *Sender);
        void __fastcall TimingFileBrowseButtonClick(TObject *Sender);
        void __fastcall TimingFileEditExit(TObject *Sender);
        void __fastcall ComputerDropListChange(TObject *Sender);
        void __fastcall MachineSchemeChangedClick(TObject *Sender);
        void __fastcall MachineListViewClick(TObject *Sender);
        void __fastcall AssembleEditCBoxClick(TObject *Sender);
        void __fastcall GlobalVTROffsetRecToComCheckBoxClick(
          TObject *Sender);
        void __fastcall GlobalComputerOffsetRecToVTRCheckBoxClick(
          TObject *Sender);
        void __fastcall GlobalVTROffsetRecToComSpinEditChange(
          TObject *Sender);
        void __fastcall GlobalComputerOffsetRecToVTRSpinEditChange(
          TObject *Sender);

protected:
        virtual void DetailsChanged(bool Value);
        void __fastcall SetAdminPriv(bool Value);

private:	// User declarations
       string sComputerName;
       string sCompClass;
       string sCurrentSchemeName;
       TListItem *liCurrentMachineItem;
       MachineSchemeData DefaultMachineSchemeData;

       SchemeData  DefaultScheme;
       vector<SchemeData> Schemata;

       void CurrentMachineSchemataInsert(const string &sV);

       void WriteTimingFile(void);
       SchemeData ReadScheme(CIniFile *ini, const string &SchemeSection);
       SchemeData SchemeFromName(const string &Name);
       string LongSchemeName(const string &Name);
       string ShortSchemeName(const string &Name);
       int SchemeIndexFromName(const string &Name);
       void ReadSchemata(CIniFile *ini);
       void UpdateSchemeDisplay(const SchemeData &Scheme);
       void UpdateDefaultSchemeDisplay(void);

       void UpdateMachineSchemeDisplay(MachineSchemeData *MSD);
       void UpdateDefaultMachineSchemeDisplay(void);

       void ShowMachineDetails(void);
       void ShowFormatDetails(void);

       int m_InhibitAllUpdates;                     // Ugly update flag

public:		// User declarations
       virtual bool Read(void);
       virtual bool ReadFile(const string &FileName);
       virtual bool Apply(void);
       virtual bool IniHasChangedCB(void);

      __fastcall TVTRPanelFrame(TComponent* Owner);
      bool VTRQueryID(int &D1, int&D2);
};
//---------------------------------------------------------------------------
extern PACKAGE TVTRPanelFrame *VTRPanelFrame;
//---------------------------------------------------------------------------
#endif
