//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "VTRFrame.h"
#include "FormatDialogUnit.h"
#include "DefineMachineDialog.h"
#include "EditTiming.h"
#include <shlobj.h>
#include "DllSupport.h"
#include "W32Serial.h"
#include "CEditCtrl.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ParamPanelForm"
//#pragma link "CSPIN"
#pragma resource "*.dfm"
TVTRPanelFrame *VTRPanelFrame;

//---------------------------------------------------------------------------
__fastcall TVTRPanelFrame::TVTRPanelFrame(TComponent* Owner)
        : TParamPanel(Owner)
{
   VTRPanelFrame = this;            // Used by subforms
   liCurrentMachineItem = NULL;
   Caption = "VTR";
   EnableDetails(true);
   m_InhibitAllUpdates = 0;
}

//-----------------------Read---------------------John Mertus----Oct 2002-----

     bool TVTRPanelFrame::Read(void)

//  This looks up the standard configuration file name and reads it
//
//****************************************************************************
{
    // Open the file
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    // Read the data from the ini file
    p_sCfgIniFileName = ini->ReadString(VTR_SECTION, "TimingFileEdit", DEFAULT_TIMING_FILE);
    TimingFileEdit->Text = p_sCfgIniFileName.c_str();
    ChangeHintOnEdit(TimingFileEdit);
    AssembleEditCBox->Checked = ini->ReadBool(VTR_SECTION,"AllowAssembleEdit", false);

    // Fill in the Global Offset settings in the Global Options section
    GlobalVTROffsetRecToComCheckBox->Checked      = ini->ReadBool(VTR_SECTION,
       "UseGlobalVTROffsetRecToCom", false);
    GlobalComputerOffsetRecToVTRCheckBox->Checked = ini->ReadBool(VTR_SECTION,
       "UseGlobalComputerOffsetRecToVTR", false);

    SerialPortSEdit->Value = ini->ReadInteger(VTR_SECTION, "SerialPort", 1);
    if (ini->ReadBool(VTR_SECTION, "UseRS422Protocol", true))
      VTRGroup->ItemIndex = 1;
    else
      VTRGroup->ItemIndex = 0;

    sComputerName = ini->ReadString(GENERAL_SECTION, "ComputerType", "CT_WIN_DVS").c_str();

    // Load the computer name
    DeleteIniFile(ini);

    return ReadFile(p_sCfgIniFileName);
}

//-----------------------ReadFile-----------------John Mertus----Oct 2002-----

     bool TVTRPanelFrame::ReadFile(const string &FileName)

//  Read the VTR (Timing) configuraion from FileName
//  Note: $(var) variables are expanded
//   Return is true if entire file was read.
//
//****************************************************************************
{
    string sFileName = ConformEnvironmentName(FileName);

    // ****************Load the timing file ******************************
    CIniFile *ini = CreateIniFile(sFileName);
    if (ini == NULL) return false;

    // Load allthe computer items
    StringList sl;
    ini->ReadSection("Computer",&sl);
    ComputerDropList->Items->Clear();
    for (unsigned i=0; i < sl.size(); i++)
        ComputerDropList->Items->Add(sl[i].c_str());

    ComputerDropList->ItemIndex = -1;
    for (int i=0; i < ComputerDropList->Items->Count; i++)
        if (ComputerDropList->Items->Strings[i].c_str() == sComputerName)
           ComputerDropList->ItemIndex = i;


    //  Check for no computer
    if (ComputerDropList->ItemIndex == -1)
	  _MTIErrorDialog(Handle, "VTR Section Warning\nVideo Board Interface is not properly set\nPlease set and save");

    // Read the Global Computer Defaults
    GlobalVTROffsetRecToComSpinEdit->Value = ini->ReadInteger("GlobalComputerDefaults",
       "AutoEditGlobalVTROffsetRecToCom", 0);
    GlobalComputerOffsetRecToVTRSpinEdit->Value = ini->ReadInteger("GlobalComputerDefaults",
       "AutoEditGlobalComputerOffsetRecToVTR", 0);

    // Read all the schemes
    ReadSchemata(ini);

    // Load the machine names
    sl.clear();
    ini->ReadSections(&sl);

    // Clear the list
    MachineListView->Items->Clear();
    MachineListView->ReadOnly = true;

    DefaultMachineSchemeData.Name = "MachineDefaults";
    DefaultMachineSchemeData.Read(ini);
    UpdateDefaultMachineSchemeDisplay();

    for (unsigned i=0; i < sl.size(); i++)
      {
          // See if the key defines a machine
          // Note: We are assuming that Machine does not exist in any other key
          // but the machine names.
          if (ini->KeyExists(sl[i], "Machine"))
          {
             TListItem *ListItem = MachineListView->Items->Add();
             //  Add the ID string
             ListItem->Caption = UpperCase(sl[i].c_str());
             //  Add the name
             ListItem->SubItems->Add(ini->ReadString(sl[i],"Machine","No Name").c_str());
             //  Get the formats
             MachineSchemeData *MSD = new MachineSchemeData;
             ListItem->Data = MSD;
             MSD->Name = sl[i];                // Get the machine name
             MSD->Read(ini);                   // Get the machine parameters

             StringList *slFmt = &MSD->Sections;

             // We have the scheme, check to see if they really exist, if not
             // Create them
             for (unsigned j=0; j < slFmt->size(); j++)
               if (SchemeIndexFromName(LongSchemeName(slFmt->at(j))) < 0)
                 {
                   SchemeData NewScheme;
                   NewScheme.Name = LongSchemeName(slFmt->at(j));
                   Schemata.push_back(NewScheme);
                 }
          }
      }

    DeleteIniFile(ini);

    // Reset the modify flag
    Modified = false;
    p_sLoadedIniFileName = sFileName;
    return true;
}

//*************************VTR Configuration********************************

//--------------SchemeIndexFromName---------------John Mertus----Nov 2002-----

     int TVTRPanelFrame::SchemeIndexFromName(const string &Name)

//  Return the index in the Scheme list from the full name
//
//****************************************************************************
   {
      for (unsigned i=0; i < Schemata.size(); i++)
        if (EqualIgnoreCase(Schemata[i].Name,Name)) return i;

      return -1;
   }

//-----------------SchemeFromName-----------------John Mertus----Nov 2002-----

     SchemeData TVTRPanelFrame::SchemeFromName(const string &Name)

//  Return a copy of the entry in the Scheme list from the full name
//
//****************************************************************************
   {
      int i = SchemeIndexFromName(Name);
      // If found, return Schemata
      if (i >= 0) return Schemata[i];
      return DefaultScheme;
   }

//-------------------ReadScheme-------------------John Mertus----Nov 2002-----

     SchemeData TVTRPanelFrame::ReadScheme(CIniFile *ini, const string &SchemeSection)

//  This reads an individual scheme from ini file ini with name SchemeSection
//  and return the information in the structure SchemeData
//
//****************************************************************************
{
   SchemeData Result;

   // Read the data, filling in defaults
   Result.OffsetToVTR = ini->ReadInteger(SchemeSection, "AutoEditComputerOffsetRecToVTR", 0);
   Result.OffsetToComputer = ini->ReadInteger(SchemeSection, "AutoEditVTROffsetRecToCom", 0);
   Result.BackupOnPickup = ini->ReadInteger(SchemeSection, "PickupBackup", 2);
   Result.AllowableJitter = ini->ReadInteger(SchemeSection, "AllowableBobble", 1);
   Result.FrameStart = ini->ReadInteger(SchemeSection, "FramePercentLower", 25);
   Result.FrameEnd = ini->ReadInteger(SchemeSection, "FramePercentUpper", 40);
   Result.IssueInhibit = ini->ReadBool(SchemeSection, "IssueRecordInhibitCommand", true);
   Result.TimeSenseType = ini->ReadInteger(SchemeSection, "TimeSenseType", VTR_TIME_SENSE_BEST);

   // Defaults are a special case, force all to be read
   if (EqualIgnoreCase(SchemeSection, LongSchemeName("Defaults")))
     {
       Result.bi_OffsetToVTR = true;
       Result.bi_OffsetToComputer = true;
       Result.bi_BackupOnPickup = true;
       Result.bi_AllowableJitter = true;
       Result.bi_FrameStart = true;
       Result.bi_FrameEnd = true;
       Result.bi_IssueInhibit = true;
       Result.bi_TimeSenseType = true;
     }
   else
     {
       // Check for existence of key
       Result.bi_OffsetToVTR = ini->KeyExists(SchemeSection, "AutoEditComputerOffsetRecToVTR");
       Result.bi_OffsetToComputer = ini->KeyExists(SchemeSection, "AutoEditVTROffsetRecToCom");
       Result.bi_BackupOnPickup = ini->KeyExists(SchemeSection, "PickupBackup");
       Result.bi_AllowableJitter = ini->KeyExists(SchemeSection, "AllowableBobble");
       Result.bi_FrameStart = ini->KeyExists(SchemeSection, "FramePercentLower");
       Result.bi_FrameEnd = ini->KeyExists(SchemeSection, "FramePercentUpper");
       Result.bi_IssueInhibit = ini->KeyExists(SchemeSection, "IssueRecordInhibitCommand");
       Result.bi_TimeSenseType = ini->KeyExists(SchemeSection, "TimeSenseType");
     }

   //  This ids the scheme for reverse lookup.
   Result.Name = SchemeSection;

   //  Now read in the available formats
   Result.Formats.clear();
   ini->ReadStringList(SchemeSection,"Format",&Result.Formats);
   return Result;
}

//-------------------ReadSchemata-------------------John Mertus----Nov 2002-----

     void TVTRPanelFrame::ReadSchemata(CIniFile *ini)

//  This read all the Schemata from the ini file ini
//
//****************************************************************************
{
    // Read the defaults
    sCompClass = ini->ReadString("Computer", sComputerName, "C1");
    DefaultScheme = ReadScheme(ini, LongSchemeName("Defaults"));

    // Read the schemes
    Schemata.clear();
    StringList sl;
    ini->ReadSections(&sl);
    for (unsigned i=0; i < sl.size(); i++)
      {
        if (EqualIgnoreCase(sCompClass, sl[i].substr(0,sCompClass.size())))
          {
             // Don't read in the defaults scheme
             if (!EqualIgnoreCase(sl[i], LongSchemeName("Defaults")))
               Schemata.push_back(ReadScheme(ini,sl[i]));
          }
      }

    UpdateDefaultSchemeDisplay();
}

//-----------UpdateDefaultSchemeDisplay-----------John Mertus----Nov 2002-----

     void TVTRPanelFrame::UpdateDefaultSchemeDisplay(void)

//  This is used to copy the default scheme to the display
//   All changes are inhibited until the display is updated.
//   Then the Scheme change is updated
//
//****************************************************************************
{
   m_InhibitAllUpdates++;

   // Defaults are always set
   DOffsetToVTRSEdit->Value = DefaultScheme.OffsetToVTR;
   DOffsetToComputerSEdit->Value = DefaultScheme.OffsetToComputer;
   DBackupOnPickupSEdit->Value = DefaultScheme.BackupOnPickup;
   DAllowableJitterSEdit->Value = DefaultScheme.AllowableJitter;
   DFrameStartSEdit->Value = DefaultScheme.FrameStart;
   DFrameEndSEdit->Value = DefaultScheme.FrameEnd;
   if (DefaultScheme.IssueInhibit) DIssueInhibitCBox->ItemIndex = 0; else DIssueInhibitCBox->ItemIndex = 1;
   DTimeSenseComboBox->ItemIndex = DefaultScheme.GetTimeSenseIndex();
   DTimeSenseComboBox->Hint = DefaultScheme.GetTimeSenseHint().c_str();

   m_InhibitAllUpdates--;

   SchemeChangedClick(NULL);
}

//--------------UpdateSchemeDisplay--------------John Mertus----Nov 2002-----

    void TVTRPanelFrame::UpdateSchemeDisplay(const SchemeData &Scheme)

//  This displays the scheme Scheme on the panel
//   All changes are inhibited until the display is updated.
//   Then the Scheme change is updated
//
//****************************************************************************
{
   // On internal update, we don't want a change to be called

   // Strip the : from the name
   sCurrentSchemeName = Scheme.Name;
   if (ShortSchemeName(Scheme.Name) == "Defaults")
    {
       FormatListBox->Clear();
       FormatLabel->Caption = "No Scheme Selected";
       SchemeGroup->Enabled = false;
       FormatLabel->Enabled = false;
       return;
    }
    
   m_InhibitAllUpdates++;

   SchemeGroup->Enabled = true;
   FormatLabel->Enabled = true;

   SchemeGroup->Caption = ShortSchemeName(Scheme.Name).c_str();
   // Copy over the default or the real value
   UOffsetToVTRCBox->Checked = !Scheme.bi_OffsetToVTR;
   OffsetToVTRSEdit->Enabled = !UOffsetToVTRCBox->Checked;
   if (Scheme.bi_OffsetToVTR)
      OffsetToVTRSEdit->Value = Scheme.OffsetToVTR;
   else
      OffsetToVTRSEdit->Value = DOffsetToVTRSEdit->Value;

   UOffsetToComputerCBox->Checked = !Scheme.bi_OffsetToComputer;
   OffsetToComputerSEdit->Enabled = !UOffsetToComputerCBox->Checked;
   if (Scheme.bi_OffsetToComputer)
     OffsetToComputerSEdit->Value = Scheme.OffsetToComputer;
   else
     OffsetToComputerSEdit->Value = DOffsetToComputerSEdit->Value;

   UBackupOnPickupCBox->Checked = !Scheme.bi_BackupOnPickup;
   BackupOnPickupSEdit->Enabled = Scheme.bi_BackupOnPickup;;
   if (Scheme.bi_BackupOnPickup)
     BackupOnPickupSEdit->Value = Scheme.BackupOnPickup;
   else
     BackupOnPickupSEdit->Value = DBackupOnPickupSEdit->Value;

   UAllowableJitterCBox->Checked = !Scheme.bi_AllowableJitter;
   AllowableJitterSEdit->Enabled = Scheme.bi_AllowableJitter;
   if (Scheme.bi_AllowableJitter)
     AllowableJitterSEdit->Value = Scheme.AllowableJitter;
   else
     AllowableJitterSEdit->Value = DAllowableJitterSEdit->Value;

   UFrameStartCBox->Checked = !Scheme.bi_FrameStart;
   FrameStartSEdit->Enabled = Scheme.bi_FrameStart;
   if (Scheme.bi_FrameStart)
     FrameStartSEdit->Value = Scheme.FrameStart;
   else
     FrameStartSEdit->Value = DFrameStartSEdit->Value;


   UFrameEndCBox->Checked = !Scheme.bi_FrameEnd;
   FrameEndSEdit->Enabled = Scheme.bi_FrameEnd;
   if (Scheme.bi_FrameEnd)
     FrameEndSEdit->Value = Scheme.FrameEnd;
   else
     FrameEndSEdit->Value = DFrameEndSEdit->Value;

   UIssueInhibitCBox->Checked = !Scheme.bi_IssueInhibit;
   IssueInhibitCBox->Enabled = Scheme.bi_IssueInhibit;
   if (Scheme.bi_IssueInhibit)
     {
       if (Scheme.IssueInhibit) IssueInhibitCBox->ItemIndex = 0; else IssueInhibitCBox->ItemIndex = 1;
     }
   else
     IssueInhibitCBox->ItemIndex = DIssueInhibitCBox->ItemIndex;

   UTimeSenseCBox->Checked = !Scheme.bi_TimeSenseType;
   TimeSenseComboBox->Enabled = Scheme.bi_TimeSenseType;
   if (Scheme.bi_TimeSenseType)
     {
       TimeSenseComboBox->ItemIndex = Scheme.GetTimeSenseIndex();
       TimeSenseComboBox->Hint = DefaultScheme.GetTimeSenseHint().c_str();
     }
   else
     {
       TimeSenseComboBox->ItemIndex = DTimeSenseComboBox->ItemIndex;
       TimeSenseComboBox->Hint = DTimeSenseComboBox->Hint;
     }

   // Load the formats
   FormatListBox->Clear();
   for (unsigned j=0; j < Scheme.Formats.size(); j++)
       {
          FormatListBox->Items->Add((Scheme.Formats[j].substr(8).c_str()));
       }

   // See if blank
   if (Scheme.Formats.size() == 0)
     FormatListBox->Items->Add("No Format!");

   FormatLabel->Caption = SchemeGroup->Caption + " Formats";

   // Restore modified flag
   SchemeGroup->Enabled = true;
   m_InhibitAllUpdates--;
}

//---------UpdateDefaultMachineSchemeDisplay---------John Mertus----Nov 2002-----

     void TVTRPanelFrame::UpdateDefaultMachineSchemeDisplay(void)

//  This is used to copy the default machine display.
//   All changes are inhibited until the display is updated.
//   Then the Scheme change is updated
//
//****************************************************************************
{
   m_InhibitAllUpdates++;

   // Defaults are always set
   DPrerollSEdit->Value = DefaultMachineSchemeData.PrerollHint;
   DPostrollSEdit->Value = DefaultMachineSchemeData.PostrollHint;
   DInstructionDelaySEdit->Value = DefaultMachineSchemeData.InstructionDelay;
   if (DefaultMachineSchemeData.SupportsAutoEdit) DSupportsAutoEditCBox->ItemIndex = 0; else DSupportsAutoEditCBox->ItemIndex = 1;
   if (DefaultMachineSchemeData.SetSensePreroll) DSetSensePrerollCBox->ItemIndex = 0; else DSetSensePrerollCBox->ItemIndex = 1;

   m_InhibitAllUpdates--;

   MachineSchemeChangedClick(NULL);
}

//--------------UpdateMachineSchemeDisplay----------John Mertus----Nov 2002-----

    void TVTRPanelFrame::UpdateMachineSchemeDisplay(MachineSchemeData *MSD)

//  This displays the Machine Information
//   All changes are inhibited until the display is updated.
//   Then the machine change is updated
//
//****************************************************************************
{
   // On internal update, we don't want changes to be called
   m_InhibitAllUpdates++;

   // Copy over the default or the real value
   USupportsAutoEditCBox->Checked = !MSD->bi_SupportsAutoEdit;
   SupportsAutoEditCBox->Enabled = MSD->bi_SupportsAutoEdit;
   if (MSD->bi_SupportsAutoEdit)
     {
       if (MSD->SupportsAutoEdit) SupportsAutoEditCBox->ItemIndex = 0; else SupportsAutoEditCBox->ItemIndex = 1;
     }
   else
     SupportsAutoEditCBox->ItemIndex = DSupportsAutoEditCBox->ItemIndex;

   UInstructionDelayCBox->Checked = !MSD->bi_InstructionDelay;
   InstructionDelaySEdit->Enabled = !UInstructionDelayCBox->Checked;
   if (MSD->bi_InstructionDelay)
     InstructionDelaySEdit->Value = MSD->InstructionDelay;
   else
     InstructionDelaySEdit->Value = DInstructionDelaySEdit->Value;

   UPrerollCBox->Checked = !MSD->bi_PrerollHint;
   PrerollSEdit->Enabled = MSD->bi_PrerollHint;
   if (MSD->bi_PrerollHint)
     PrerollSEdit->Value  = MSD->PrerollHint;
   else
     PrerollSEdit->Value = DPrerollSEdit->Value;

   UPostrollCBox->Checked = !MSD->bi_PostrollHint;
   PostrollSEdit->Enabled = MSD->bi_PostrollHint;
   if (MSD->bi_PostrollHint)
     PostrollSEdit->Value  = MSD->PostrollHint;
   else
     PostrollSEdit->Value = DPostrollSEdit->Value;

   USetSensePrerollCBox->Checked = !MSD->bi_SetSensePreroll;
   SetSensePrerollCBox->Enabled = MSD->bi_SetSensePreroll;
   if (MSD->bi_SetSensePreroll)
     {
       if (MSD->SetSensePreroll) SetSensePrerollCBox->ItemIndex = 0; else SetSensePrerollCBox->ItemIndex = 1;
     }
   else
     SetSensePrerollCBox->ItemIndex = DSetSensePrerollCBox->ItemIndex;

   // Now do the scheme list
   StringList *slFmt = &MSD->Sections;
   SchemataListBox->Clear();
   for (unsigned i=0; i < slFmt->size(); i++)
     SchemataListBox->Items->Add(((*slFmt)[i]).c_str());

   // Restore modified flag
   MachineGroupBox->Enabled = true;
   m_InhibitAllUpdates--;
}

//-----------------LongSchemeName-----------------John Mertus----Nov 2002-----

         string TVTRPanelFrame::LongSchemeName(const string &Name)

//  This returns the scheme name with the computer prefix
//
//****************************************************************************
{
  return(sCompClass + ":" + Name);
}

//----------------ShortSchemeName-----------------John Mertus----Jan 2004-----

         string TVTRPanelFrame::ShortSchemeName(const string &Name)

//  This returns the scheme name with the computer name striped off
//
//****************************************************************************
{
  return Name.substr(Name.find(":")+1);
}

//----------------WriteTimingFile-----------------John Mertus----Nov 2002-----

      void  TVTRPanelFrame::WriteTimingFile(void)

//  This writes the timing file using the time file name entry.
//
//****************************************************************************
{

     string sName = ConformEnvironmentName(TimingFileEdit->Text.c_str());
     CIniFile *ini = CreateIniFile(sName);
     if (ini == NULL)
       {
          _MTIErrorDialog(Handle, "Could not create " + sName);
          return;
       }

     // Write out the Global Computer Defaults section
     ini->WriteInteger("GlobalComputerDefaults",
        "AutoEditGlobalVTROffsetRecToCom", GlobalVTROffsetRecToComSpinEdit->Value);
     ini->WriteInteger("GlobalComputerDefaults",
        "AutoEditGlobalComputerOffsetRecToVTR", GlobalComputerOffsetRecToVTRSpinEdit->Value);

     // The list may contain machines that were deleted, loop through the
     // the machine names and if they are not in the machine list, delete them
     // Load the machine names and erase any not found

     StringList sl;
     ini->ReadSections(&sl);
     for (unsigned i=0; i < sl.size(); i++)
       {
          // See if the key defines a machine
          if (ini->KeyExists(sl[i], "Machine"))
          {
             // See if this machine exists, if not delete it
             if (MachineListView->FindCaption(0, sl[i].c_str(), false, true, true) == NULL)
                ini->EraseSection(sl[i]);
          }
        }

     // Write the default one
     DefaultMachineSchemeData.Write(ini);

     // Run through the machine names
     TListItems *Items = MachineListView->Items;  // Save some typing
     for (int i = 0; i < Items->Count; i++)
       {
          TListItem *Item = Items->Item[i];
          string sID = Item->Caption.c_str();
          ini->WriteString(sID, "Machine", Item->SubItems->Strings[0].c_str());
          MachineSchemeData *MSD = (MachineSchemeData *)Item->Data;
          MSD->Write(ini);
       }

     // Now write the Schemata
     for (unsigned i=0; i < Schemata.size(); i++)
         Schemata[i].Write(ini);

     DefaultScheme.Write(ini);

     DeleteIniFile(ini);
}

//-------------------Apply-----------------------John Mertus----Nov 2002-----

    bool TVTRPanelFrame::Apply(void)

//  Usual apply method, write all the information to the file
//
//****************************************************************************
{
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    // Write the file and computer name 
    if (ini == NULL) return false;

    ini->WriteString(GENERAL_SECTION, "ComputerType", sComputerName);
    ini->WriteString(VTR_SECTION, "TimingConfig", TimingFileEdit->Text.c_str());

    // Write the data to the ini file
    ini->WriteInteger(VTR_SECTION, "SerialPort",  SerialPortSEdit->Value);
    ini->WriteBool(VTR_SECTION, "UseRS422Protocol", VTRGroup->ItemIndex == 1);
    ini->WriteBool(VTR_SECTION,"AllowAssembleEdit", AssembleEditCBox->Checked);
    ini->WriteBool(VTR_SECTION, "UseGlobalVTROffsetRecToCom",
       GlobalVTROffsetRecToComCheckBox->Checked);
    ini->WriteBool(VTR_SECTION, "UseGlobalComputerOffsetRecToVTR",
       GlobalComputerOffsetRecToVTRCheckBox->Checked);
    DeleteIniFile(ini);

    WriteTimingFile();
    Modified = false;
    return true;

}

//-----------MachineListViewSelectItem------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::MachineListViewSelectItem(TObject *Sender,
                                                 TListItem *Item, bool Selected)

//  When user clicks on a machine, this does the update to the Schemata
//
//****************************************************************************
{
   liCurrentMachineItem = Item;
   if (liCurrentMachineItem == NULL)
     {
       SchemataListBox->Clear();
       SchemataLabel->Caption = "Schemata";
       return;
     }

   MachineSchemeData *MSD = (MachineSchemeData *)liCurrentMachineItem->Data;
   UpdateMachineSchemeDisplay(MSD);

   SchemataLabel->Caption = Item->SubItems->Strings[0] + " Schemata(" + Item->Caption + ")";

   // Change the name
   MachineGroupBox->Caption = Item->SubItems->Strings[0]+ "(" + Item->Caption + ")";

   ShowMachineDetails();
}


//---------------SchemataListBoxClick---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemataListBoxClick(TObject *Sender)

//  This just changes the format box to reflect the clicked scheme
//
//****************************************************************************
{
   // Find the selected format
   int i = SchemataListBox->ItemIndex;
   if (i < 0)
     {
       FormatListBox->Clear();
       FormatLabel->Caption = "Schema Formats";
       return;
     }


   string LongName = LongSchemeName(SchemataListBox->Items->Strings[i].c_str());
   i = SchemeIndexFromName(LongName);
   // Create a scheme if none exists
   if (i < 0)
     {
       SchemeData NewScheme;
       NewScheme.Name = LongName;
       Schemata.push_back(NewScheme);
     }

   SchemeData S = SchemeFromName(LongName);
   // Load the other parameters
   UpdateSchemeDisplay(S);

   ShowFormatDetails();

}


//-------------VTREnableDefaultsClick-------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::VTREnableDefaultsClick(TObject *Sender)

//  Called to copy the inheritance bits from the GUI to the scheme.
//
//****************************************************************************
{
   if (m_InhibitAllUpdates > 0) return;
   int idx = SchemeIndexFromName(sCurrentSchemeName);
   if (idx >= 0)
     {
       SchemeData *Scheme = &Schemata[idx];

       Scheme->bi_OffsetToVTR = !UOffsetToVTRCBox->Checked;
       Scheme->bi_OffsetToComputer = !UOffsetToComputerCBox->Checked;
       Scheme->bi_BackupOnPickup = !UBackupOnPickupCBox->Checked;
       Scheme->bi_AllowableJitter = !UAllowableJitterCBox->Checked;
       Scheme->bi_FrameStart = !UFrameStartCBox->Checked;
       Scheme->bi_FrameEnd = !UFrameEndCBox->Checked;
       Scheme->bi_IssueInhibit = !UIssueInhibitCBox->Checked;
       Scheme->bi_TimeSenseType = !UTimeSenseCBox->Checked;
       UpdateSchemeDisplay(*Scheme);
    }
   // Do the Machine parameters
   if (MachineListView->Selected != NULL)
     {
        MachineSchemeData *MSD = (MachineSchemeData *)MachineListView->Selected->Data;
        MSD->bi_SupportsAutoEdit = !USupportsAutoEditCBox->Checked;
        MSD->bi_InstructionDelay = !UInstructionDelayCBox->Checked;
        MSD->bi_PrerollHint = !UPrerollCBox->Checked;
        MSD->bi_PostrollHint = !UPostrollCBox->Checked;
        MSD->bi_SetSensePreroll = !USetSensePrerollCBox->Checked;
        UpdateMachineSchemeDisplay(MSD);
     }


   Modified = true;
 }
//---------------------------------------------------------------------------


//-------------FormatInsertMenuClick--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::FormatInsertMenuClick(TObject *Sender)

//  Inserts a new format
//
//****************************************************************************
{
     if (FormatDialog->ShowModal() == mrOk)
       {
          TListItems *ListItems = FormatDialog->FormatListView->Items;
          // Loop through the items and start to add the selected items
          for (int i=0; i < ListItems->Count; i++)
            if (ListItems->Item[i]->Selected)
               {
                  // Check for duplication, if not, add the item
                 if (FormatListBox->Items->IndexOf(ListItems->Item[i]->Caption) == -1)
                   {
                     int jS = SchemeIndexFromName(sCurrentSchemeName);
                     // if found (and it always should be), add to list
                     if (jS >= 0)
                       {
                          string sV = (string)"IF_TYPE_" + ListItems->Item[i]->Caption.c_str();
                          Schemata[jS].Formats.push_back(sV);
                          UpdateSchemeDisplay(Schemata[jS]);
                          Modified = true;  // We have changed
                       }
                   }
               }
       };
}

//-------------FormatDeleteMenuClick--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::FormatDeleteMenuClick(TObject *Sender)

//  This deletes a format from a scheme
//
//****************************************************************************
{
      int jS = -1;
      for (int i=FormatListBox->Items->Count-1; i >= 0; i--)
        if (FormatListBox->Selected[i])
           {
             // Delete from the scheme then redraw
             // Insures there is no error
             string sV = (string)"IF_TYPE_" + FormatListBox->Items->Strings[i].c_str();
             jS = SchemeIndexFromName(sCurrentSchemeName);
             // if found (and it always should be), delete from list
             if (jS >= 0)
               {
                 // Go delete all copies of it (there should be only one)
                 for (int k=(int)Schemata[jS].Formats.size()-1; k >=0; k--)
                   if (sV == Schemata[jS].Formats[k])
                     {
                        Schemata[jS].Formats.erase(Schemata[jS].Formats.begin()+k);
                        Modified = true;  // We have changed
                     }
               }
           }

      if (jS >= 0) UpdateSchemeDisplay(Schemata[jS]);

}

//-------------SchemataDeleteMenuClick--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemataDeleteMenuClick(TObject *Sender)

//  This deletes a scheme
//
//****************************************************************************
{
      for (int i=SchemataListBox->Items->Count-1; i >= 0; i--)
        if (SchemataListBox->Selected[i])
           {
             // Delete from the scheme then redraw
             // Insures there is no error
             string sV = SchemataListBox->Items->Strings[i].c_str();
             MachineSchemeData *MSD = (MachineSchemeData *)liCurrentMachineItem->Data;
             StringList *slFmt = &MSD->Sections;
             // Go delete all copies of it (there should be only one)
             for (int k=(int)slFmt->size()-1; k >=0; k--)
               if (sV == slFmt->at(k))
                 {
                    slFmt->erase(slFmt->begin()+k);
                    Modified = true;  // We have changed
                 }
           }

        // Do all the updates
      MachineListViewSelectItem(Sender, liCurrentMachineItem, true);
      SchemataListBoxClick(Sender);

}
//---------------------------------------------------------------------------

//-------------SchemataInsertMenuClick--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemataInsertMenuClick(TObject *Sender)

//    Called when an insert menu is choosen
//
//****************************************************************************
{
   //  See if we even care about this insert
   if (liCurrentMachineItem == NULL) return;

   String sV = ((TMenuItem *)Sender)->Caption;

   // The menu is built with & in it.  Why I don't know. Remove them
   int k;
   while ((k = sV.Pos("&")) > 0) sV.Delete(k,1);

   CurrentMachineSchemataInsert(sV.c_str());
}

//----------CurrentMachineSchemataInsert-----------John Mertus----Jam 2004-----

  void TVTRPanelFrame::CurrentMachineSchemataInsert(const string &sV)

//   Called to add a scheme format with name sV to the current machine
//
//****************************************************************************
{
   // The menu is built with & in it.  Why I don't know. Remove them
   MachineSchemeData *MSD = (MachineSchemeData *)liCurrentMachineItem->Data;
   StringList *sl = &MSD->Sections;

   // Check for duplication, if not, add the item
   for (unsigned i = 0; i < sl->size(); i++)
     if (EqualIgnoreCase(sl->at(i),sV)) return;    // Found, do nothing

   sl->push_back(sV.c_str());

   Modified = true;  // We have changed
   MachineListViewSelectItem(NULL, liCurrentMachineItem, true);
}

//---------------------------------------------------------------------------


//--------------SchemataPopupMenuPopup--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemataPopupMenuPopup(TObject *Sender)

//   Called before a Menu Popup.  This first enables the correct menus
//  then creates the insert menu.
//
//****************************************************************************
{
   SchemataInsertMenu->Enabled = (liCurrentMachineItem != NULL);
   SchemataNewMenu->Enabled = (liCurrentMachineItem != NULL);
   SchemataDeleteMenu->Enabled = (liCurrentMachineItem != NULL) && (SchemataListBox->ItemIndex >= 0);

   SchemataInsertMenu->Clear();

   // Build the menu to copy it over
   for (unsigned i = 0; i < Schemata.size(); i++)
     {
        TMenuItem *mi = new TMenuItem(SchemataInsertMenu);
        string sV = ShortSchemeName(Schemata[i].Name);
        mi->Caption = sV.c_str();
        mi->OnClick = SchemataInsertMenuClick;
        SchemataInsertMenu->Add(mi);
     }
}


//--------------FormatPopupMenuPopup--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::FormatPopupMenuPopup(TObject *Sender)

//  This enables/disables the format menu items
//
//****************************************************************************
{
   bool bE = (sCurrentSchemeName != LongSchemeName("Defaults"));
   FormatInsertMenu->Enabled = (sCurrentSchemeName != "") && bE;

   bool bS = false;
   for (int i = 0; i < FormatListBox->Items->Count; i++)
      bS = bS || FormatListBox->Selected[i];

   FormatDeleteMenu->Enabled = bE && bS;
}

//---------------SchemeChangedClick---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemeChangedClick(TObject *Sender)

//  When the user changes a scheme value, this recomputes everything.
//
//****************************************************************************
{
   if (m_InhibitAllUpdates > 0) return;

   int idx = SchemeIndexFromName(sCurrentSchemeName);
   if (idx < 0) return;
   SchemeData *Scheme = &Schemata[idx];

   if (Scheme->bi_OffsetToVTR)
     Scheme->OffsetToVTR = OffsetToVTRSEdit->Value;

   if (Scheme->bi_OffsetToComputer)
     Scheme->OffsetToComputer = OffsetToComputerSEdit->Value;

   if (Scheme->bi_BackupOnPickup)
     Scheme->BackupOnPickup = BackupOnPickupSEdit->Value;

   if (Scheme->bi_AllowableJitter)
     Scheme->AllowableJitter = AllowableJitterSEdit->Value;

   if (Scheme->bi_FrameStart)
     Scheme->FrameStart = FrameStartSEdit->Value;

   if (Scheme->bi_FrameEnd)
     Scheme->FrameEnd = FrameEndSEdit->Value;

   if (Scheme->bi_IssueInhibit)
     Scheme->IssueInhibit = (IssueInhibitCBox->ItemIndex == 0);

   if (Scheme->bi_TimeSenseType)
     Scheme->SetTimeSenseIndex(TimeSenseComboBox->ItemIndex);

   Modified = true;
   UpdateSchemeDisplay(*Scheme);
}

//---------------SchemataNewMenuClick---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SchemataNewMenuClick(TObject *Sender)

//  Requests a new Scheme name and create it
//
//****************************************************************************
{

   String NewValue = Trim(InputBox("New Scheme", "New Scheme Name", ""));
   if (NewValue == "") return;  // Nothing to do

   // Check to see if it exists
   int i = SchemeIndexFromName(LongSchemeName(UpperCase(NewValue).c_str()));
   if (i >= 0)
     {
         string sV = ShortSchemeName(Schemata[i].Name);
         if (_MTIWarningDialog(Handle, sV + " scheme already exists\nOK to use this Scheme\nCancel to start over.") == MTI_DLG_CANCEL) return;
         CurrentMachineSchemataInsert(sV);
         return;
     }

    //Create a default scheme
    SchemeData NewScheme;

    NewScheme.Name = LongSchemeName(NewValue.c_str());
    Schemata.push_back(NewScheme);

    // This should Never happen
    if (liCurrentMachineItem == NULL) return;

    MachineSchemeData *MSD = (MachineSchemeData *)liCurrentMachineItem->Data;
    StringList *sl = &MSD->Sections;

    sl->push_back(NewValue.c_str());

    Modified = true;  // We have changed
    MachineListViewSelectItem(Sender, liCurrentMachineItem, true);
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::MachineTypePopupPopup(TObject *Sender)
{
    MachineDeleteMenu->Enabled = (MachineListView->Selected != NULL);
}
//---------------------------------------------------------------------------

//--------------MachineDeleteMenuClick--------------John Mertus----Nov 2002-----

    void __fastcall TVTRPanelFrame::MachineDeleteMenuClick(TObject *Sender)

//  This just deletes the selected machine
//
//****************************************************************************
{
   if (MachineListView->Selected == NULL) return;

   // Note: Delete seems to also delete the object
   MachineListView->Selected->Delete();
   Modified = true;  // We have changed
}

//--------------MachineEditMenuClick--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::MachineEditMenuClick(TObject *Sender)

//  Edit a machine name and id
//
//****************************************************************************
{
   if (MachineListView->Selected == NULL) return;

   MachineDefineDialog->NameEdit->Text = MachineListView->Selected->SubItems->Strings[0];
   MachineDefineDialog->IDHighEdit->Text = MachineListView->Selected->Caption;
   MachineDefineDialog->CopySchemeCBox->Checked = false;
   if (MachineDefineDialog->ShowModal() == mrOk)
     {
        MachineListView->Selected->SubItems->Strings[0] = Trim(MachineDefineDialog->NameEdit->Text);
        MachineListView->Selected->Caption = UpperCase(MachineDefineDialog->IDHighEdit->Text);

        // If copy is defined, copy schemes over
        if (MachineDefineDialog->CopySchemeCBox->Checked)
          {
              TListItem *li = MachineDefineDialog->liUseMachine;
              if (li != NULL)
                {
                   StringList *sl =(StringList *)li->Data;
                   StringList *slO = (StringList *)MachineListView->Selected->Data;

                   // Check to see if the Scheme name is already in the list
                   // if not, define it
                   for (unsigned i=0; i < sl->size(); i++)
                     if (find(slO->begin(), slO->end(), sl->at(i)) == slO->end())
                       slO->push_back(sl->at(i));

                   // Force an update
                   MachineListViewSelectItem(Sender, liCurrentMachineItem, true);

                }

          }
        Modified = true;  // We have changed
     }
}
//---------------------------------------------------------------------------

//-------------SerialPortSEditChange--------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::SerialPortSEditChange(TObject *Sender)

//
//
//****************************************************************************
{
    Modified = true;  // We have changed
}
//---------------------------------------------------------------------------


//--------------MachineNewMenuClick---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::MachineNewMenuClick(TObject *Sender)

//   This is called to display the dialog to create a new machine
//  Set the desired parameters (NameEdit, IDEdit) then on success
//
//****************************************************************************
{
   MachineDefineDialog->NameEdit->Text = "New VTR";
   if (MachineDefineDialog->ShowModal() == mrOk)
     {
        // Check to see if it exists
        String sID = UpperCase(Trim(MachineDefineDialog->IDHighEdit->Text));
        if (MachineListView->FindCaption(0,sID, false, true, true) != NULL)
          {
             String sName = MachineListView->FindCaption(0,sID, false, true, true)->SubItems->Strings[0];
             _MTIErrorDialog(Handle, ("Machine ID " + sID + " already exists with name " +  sName).c_str());
             return;
          }
        // Create a new item
        TListItem *ListItem = MachineListView->Items->Add();

        //  Insert the ID string
        ListItem->Caption = sID;
        //  Insert the name
        ListItem->SubItems->Add(MachineDefineDialog->NameEdit->Text);

        //  Create a blank format
        MachineSchemeData *MSD = new MachineSchemeData;
        ListItem->Data = MSD;

        // If copy is defined, copy schemes over
        if (MachineDefineDialog->CopySchemeCBox->Checked)
          {
              TListItem *li = MachineDefineDialog->liUseMachine;
              if (li != NULL)
                {
                  *MSD = *(MachineSchemeData *)li->Data;
                  MSD->Name = sID.c_str();
                }
          }

        MachineListView->Selected = ListItem;
        ListItem->MakeVisible(false);

        Modified = true;  // We have changed
     }

   // Reset the text back
   MachineDefineDialog->IDHighEdit->Text = "00:00";
}
//---------------------------------------------------------------------------

//------------VTRDetectButtonClick------------------John Mertus----Oct 2002-----

  void __fastcall TVTRPanelFrame::VTRDetectButtonClick(TObject *Sender)

//  This does the vtr detection
//   NOTE: Windows defines serial ports by names, we by port numbers
//   this should be ok, but might be a problem in the future.
//
//****************************************************************************
{
  int iRet;
  ostringstream os;
  CSercom *Sercom;

  TCursor OldCursor = Screen->Cursor;
  Screen->Cursor = crHourGlass;

  StringList PortList = MTIGetSerialPorts();
  for (unsigned i=0; i < PortList.size(); i++)
    {
      string sPN = PortList[i];

      //Ugly, ugly, ugly, extract number from port
      string::reverse_iterator ir = find_if(sPN.rbegin(),sPN.rend(),not1(ptr_fun(::isdigit)));
      istringstream istr(sPN.substr(sPN.rend()-ir));
      int iPort;
      if ((istr >> iPort) == 0)
	{
	  _MTIErrorDialog(Handle, (string)"Error decoding Serial port number from " + sPN);
          Screen->Cursor = OldCursor;
	  return;
	}
        
      os.str("");
      os << " Checking for VTR on " << sPN;
      UpdateStatusBar(os.str(), 1);

      // Port Ok, go check if we can open it
      Sercom = new CSercom(NULL);
      if (Sercom->OpenClient(iPort) == SER_COM_SUCCESS)
        {
          int D1, D2;
          if (Sercom->GetID(D1, D2))
            {
               // Create an ID string
               os.str("");
               os << setfill('0') << setw(2) << hex << D1 << ":" << setw(2) << hex << D2;
               string sID = UpperCase(os.str().c_str()).c_str();
               os.str("");
               string DevName = Sercom->GetDeviceName();
               if (DevName != "")
                 {
                   os << DevName << " found with ID " << sID << " on port " << iPort;
                   _MTIInformationDialog(Handle, os.str());
                   MachineListView->Selected = MachineListView->FindCaption(0, sID.c_str(), false, true, true);
                   MachineListView->Selected->MakeVisible(false);
                 }
               else if (theError.getError() == RET_ET_NO_COMPUTER_TYPE_SECTION)
                 {
                   _MTIErrorDialog(Handle, (string)"Could no Query VTR because\n" + theError.getFmtError());
                   Screen->Cursor = OldCursor;
                   UpdateStatusBar("", 1);
                   Sercom->Close();
                   delete Sercom;
                   return;
                 }
               else
                 {
                   os << "VTR found but timing file entry missing";
                   os << endl << "Tape Deck ID is " << sID;
                   os << endl << "Do you wish to define a new VTR with this ID?";
                   if (_MTIConfirmationDialog(Handle, os.str()) == MTI_DLG_OK)
                     {
                       MachineDefineDialog->IDHighEdit->Text = sID.c_str();
                       MachineNewMenuClick(NULL);
                     }
                 }

              // Close up everything
              SerialPortSEdit->Value = iPort;
              Sercom->Close();
              delete Sercom;
              Screen->Cursor = OldCursor;
              UpdateStatusBar("", 1);
              return;
            }
        }

      // Only get here if VTR was not found, try again
      UpdateStatusBar("", 1);
      Sercom->Close();
      delete Sercom;
    }

  // Nothing found
  Screen->Cursor = OldCursor;
  _MTIErrorDialog(Handle, "No VTR was found.\n Check cables and that no other program is using the VTR");
}
//---------------------------------------------------------------------------

//-------------------VTRQueryID-------------------John Mertus----Nov 2002-----

  bool TVTRPanelFrame::VTRQueryID(int &D1, int&D2)

//  Ask for the VTR ID, D1, D2 return the VTR ID
//  Return is true if the ID is valid, false if not
//
//****************************************************************************
{
    ostringstream os;
    CSercom *Sercom;
    TCursor OldCursor = Screen->Cursor;
    Screen->Cursor = crHourGlass;

    int iPort = SerialPortSEdit->Value;

    // Port Ok, go check if we can open it
    Sercom = new CSercom(NULL);
    if (Sercom->OpenClient(iPort) != SER_COM_SUCCESS)
      {
        os << "Could not open port " << iPort << endl << theError.getMessage();
        delete Sercom;
        Screen->Cursor = OldCursor;
        return false;
      }

    // Can open, look for a name
    if (Sercom->GetID(D1, D2))
      {
         delete Sercom;
         Screen->Cursor = OldCursor;
         return true;
      }

    // Close up
    delete Sercom;
    Screen->Cursor = OldCursor;
    os << "Could not locate VTR on port " << iPort;
    theError.set(-1, os);
    return false;
}


//--------------VTRQueryButtonClick---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::VTRQueryButtonClick(TObject *Sender)

//  Loop through and see if there is a VTR anywhere
//
//****************************************************************************
{
    ostringstream os;
    CSercom *Sercom;
    TCursor OldCursor = Screen->Cursor;
    Screen->Cursor = crHourGlass;

    int iPort = SerialPortSEdit->Value;

    // Port Ok, go check if we can open it
    Sercom = new CSercom(NULL);
    if (Sercom->OpenClient(iPort) != SER_COM_SUCCESS)
      {
        _MTIErrorDialog(Handle, theError.getFmtError());
        Sercom->Close();
        delete Sercom;
        Screen->Cursor = OldCursor;
        return;
      }
    // Can open, look for a name
    int D1, D2;
    if (Sercom->GetID(D1, D2))
      {
         // Create an ID string
         os << setfill('0') << setw(2) << hex << D1 << ":" << setw(2) << hex << D2;
         string sID = UpperCase(os.str().c_str()).c_str();
         os.str("");

         string DevName = Sercom->GetDeviceName();
         if (DevName != "")
           {
            os << DevName << " found with ID " << sID;
             _MTIInformationDialog(Handle, os.str());
             MachineListView->Selected = MachineListView->FindCaption(0,sID.c_str(), false, true, true);
             if (MachineListView->Selected != NULL)
                MachineListView->Selected->MakeVisible(false);
           }
         else if (theError.getError() == RET_ET_NO_COMPUTER_TYPE_SECTION)
           {
             _MTIErrorDialog(Handle, (string)"Could no Query VTR because\n" + theError.getFmtError());
           }
         else
           {
             os << "VTR found but timing file entry missing";
             os << endl << "Tape Deck ID is " << sID;
             os << endl << "Do you wish to define a new VTR with this ID?";
             if (_MTIConfirmationDialog(Handle, os.str()) == MTI_DLG_OK)
               {
                  MachineDefineDialog->IDHighEdit->Text = sID.c_str();
                  MachineNewMenuClick(NULL);
               }
           }
      }
    else
      {
         os.str("");
         os << "No VTR found on serial port " << iPort;
         _MTIErrorDialog(Handle, os.str());
      }


    // Close up
    Sercom->Close();
    delete Sercom;
    Screen->Cursor = OldCursor;

}


//---------------VTRDefaultsChange----------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::VTRDefaultsChange(TObject *Sender)

// This updates the scheme associated with the selected machine
//
//****************************************************************************
{
   if (m_InhibitAllUpdates > 0) return;

   SchemeData *Scheme = &DefaultScheme;

   Scheme->OffsetToVTR = DOffsetToVTRSEdit->Value;
   Scheme->OffsetToComputer = DOffsetToComputerSEdit->Value;
   Scheme->BackupOnPickup = DBackupOnPickupSEdit->Value;
   Scheme->AllowableJitter = DAllowableJitterSEdit->Value;
   Scheme->FrameStart = DFrameStartSEdit->Value;
   Scheme->FrameEnd = DFrameEndSEdit->Value;
   Scheme->IssueInhibit = (DIssueInhibitCBox->ItemIndex == 0);
   Scheme->SetTimeSenseIndex(DTimeSenseComboBox->ItemIndex);
   DTimeSenseComboBox->Hint = Scheme->GetTimeSenseHint().c_str();
   SchemeChangedClick(NULL);

   // Now do the Machine defaults
   MachineSchemeData *MSD = &DefaultMachineSchemeData;
   MSD->SupportsAutoEdit =(DSupportsAutoEditCBox->ItemIndex == 0);
   MSD->InstructionDelay = DInstructionDelaySEdit->Value;
   MSD->PrerollHint = DPrerollSEdit->Value;
   MSD->PostrollHint = DPostrollSEdit->Value;
   MSD->SetSensePreroll =(DSetSensePrerollCBox->ItemIndex == 0);
   MachineSchemeChangedClick(NULL);

   Modified = true;
}

//-----------------DetailsChanged-----------------John Mertus----Nov 2002-----

     void TVTRPanelFrame::DetailsChanged(bool Value)

//  Called when the details changed is requested
//
//****************************************************************************
{
    // Set the size of the details button
    if (Details)
     {
        Width = FormatPanel->Left + FormatPanel->Width + 12;
     }
    else
     {
        // This should compute to 400 so there are seamless switches
        Width = FormatPanel->Left;
     }
   return;
}

//********************CODE FOR THE FILE PROCESSING***************************

//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TVTRPanelFrame::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
   TParamPanel::SetAdminPriv(Value);
   if (AdminPrivilege)
     {
       TimingFileEdit->ReadOnly = false;
       TimingFileEdit->Color = clWindow;
       TimingFileResetButton->Enabled = true;
       TimingFileBrowseButton->Enabled = true;
       AssembleEditCBox->Enabled = true;
       GlobalVTROffsetRecToComCheckBox->Enabled      = true;
       GlobalComputerOffsetRecToVTRCheckBox->Enabled = true;
       GlobalVTROffsetRecToComSpinEdit->Enabled      = true;
       GlobalComputerOffsetRecToVTRSpinEdit->Enabled = true;
     }
   else
     {
       TimingFileEdit->ReadOnly = true;
       TimingFileEdit->Color = clBtnFace;
       TimingFileResetButton->Enabled = false;
       TimingFileBrowseButton->Enabled = false;
       AssembleEditCBox->Enabled = false;
       GlobalVTROffsetRecToComCheckBox->Enabled      = false;
       GlobalComputerOffsetRecToVTRCheckBox->Enabled = false;
       GlobalVTROffsetRecToComSpinEdit->Enabled      = false;
       GlobalComputerOffsetRecToVTRSpinEdit->Enabled = false;
     }

  ComputerDropList->Color = TimingFileEdit->Color;
  ComputerDropList->Enabled = !TimingFileEdit->ReadOnly;

}

//--------------------IniHasChangedCB-------------John Mertus----Nov 2002-----

   bool TVTRPanelFrame::IniHasChangedCB(void)

// This is called when the ini file may have changed.  Either by a change
// in the environmental variable or fromt he user.  The ChangeIniFile does
// all the real work.
//
//****************************************************************************
{
   return ChangeIniFile(TimingFileEdit);
}

//---------------TimingFileEditExit---------------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::TimingFileEditExit(TObject *Sender)

//  Check to see if there has been a change via an IniHasChangedCB
//
//****************************************************************************
{
   IniHasChangedCB();
}

//-----------TimingFileResetButtonClick-----------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::TimingFileResetButtonClick(TObject *Sender)

//  Just restore the default timing file
//  Generates an IniHasChangedCB call back.
//
//****************************************************************************
{
    TimingFileEdit->Text = DEFAULT_TIMING_FILE;
    IniHasChangedCB();
}

//----------TimingFileBrowseButtonClick-----------John Mertus----Nov 2002-----

  void __fastcall TVTRPanelFrame::TimingFileBrowseButtonClick(TObject *Sender)

//  Come here when the browse button is clicked.  Sender is ignored.
//  If the user enters a file name, the IniHasChangedCB call back is called
//
//****************************************************************************
{
    TEdit *Edit = (TEdit *)Sender;
    string sName = ConformEnvironmentName(Edit->Text.c_str());
    string sTmp = ConformEnvironmentName(DEFAULT_TIMING_FILE);

    OpenCfgDialog->DefaultExt = ExtractFileExt(sTmp.c_str());
    OpenCfgDialog->FileName = ExtractFileName(sTmp.c_str());
    OpenCfgDialog->InitialDir = ExtractFilePath(sName.c_str());
    if (OpenCfgDialog->Execute())
      {
        Edit->Text = OpenCfgDialog->FileName;
        IniHasChangedCB();
      }
}

//***************CSchemeData Class*******************************************
static string TimeSenseHints[] = {"Most accurate timecode", "Longitudinal Timecode", "Corrected Longitudinal Timecode", "Vertical Interval Timecode","Unknown",};
static int TimeSenseValues[] = {VTR_TIME_SENSE_BEST, VTR_TIME_SENSE_LTC, VTR_TIME_SENSE_CORRECTED_LTC, VTR_TIME_SENSE_VITC, -1};
#define TSSIZE (sizeof(TimeSenseValues)/sizeof(int))

//----------------SchemeDataStruct----------------John Mertus----Nov 2002-----

    SchemeDataStruct::SchemeDataStruct(void)

//  Usual constructor, set all the defaults.
//
//****************************************************************************
{
      OffsetToVTR = 0;
      OffsetToComputer = 0;
      BackupOnPickup = 2;
      AllowableJitter = 1;
      FrameStart = 20;
      FrameEnd = 40;
      IssueInhibit = true;
      TimeSenseType = VTR_TIME_SENSE_BEST;
      //
      //  Inheritance bits
      bi_OffsetToVTR = true;
      bi_OffsetToComputer = true;
      bi_BackupOnPickup = false;
      bi_AllowableJitter = false;
      bi_FrameStart = false;
      bi_FrameEnd = false;
      bi_IssueInhibit = false;
      bi_TimeSenseType = false;
}

//---------------GetTimeSenseIndex----------------John Mertus----Nov 2002-----

     int SchemeDataStruct::GetTimeSenseIndex(void) const

//
//
//****************************************************************************
{
   for (int i=0; i < TSSIZE; i++)
     if (TimeSenseValues[i] == TimeSenseType) return i;

   return -1;
}

//---------------SetTimeSenseIndex----------------John Mertus----Nov 2002-----

     void SchemeDataStruct::SetTimeSenseIndex(int Value)

//
//
//****************************************************************************
{
   // Check if value is in range, note unknown is out of range
   if ((Value < 0) || Value >= (TSSIZE-1)) return;
   TimeSenseType = TimeSenseValues[Value];
}

//----------------GetTimeSenseHint----------------John Mertus----Nov 2002-----

     string SchemeDataStruct::GetTimeSenseHint(void) const

//
//
//****************************************************************************
{
   int Value = GetTimeSenseIndex();
   if ((Value < 0) || Value >= (TSSIZE-1)) return "Unknown";
   return TimeSenseHints[Value];
}

//---------------------Write----------------------John Mertus----Nov 2002-----

     void SchemeDataStruct::Write(CIniFile *ini)

//  This writes the scheme data to the ini file ini
//
//****************************************************************************
{
   string sID = Name;

   // Write the format list
   ini->WriteStringList(sID, "Format", &Formats);

   if (bi_OffsetToVTR)
     ini->WriteInteger(sID, "AutoEditComputerOffsetRecToVTR", OffsetToVTR);
   else
     ini->DeleteKey(sID,"AutoEditComputerOffsetRecToVTR");

   if (bi_OffsetToComputer)
     ini->WriteInteger(sID, "AutoEditVTROffsetRecToCom", OffsetToComputer);
   else
     ini->DeleteKey(sID,"AutoEditVTROffsetRecToCom");

   if (bi_BackupOnPickup)
     ini->WriteInteger(sID, "PickupBackup", BackupOnPickup);
   else
     ini->DeleteKey(sID,"PickupBackup");

   if (bi_AllowableJitter)
     ini->WriteInteger(sID, "AllowableBobble", AllowableJitter);
   else
     ini->DeleteKey(sID,"AllowableBobble");

   if (bi_FrameStart)
     ini->WriteInteger(sID, "FramePercentLower", FrameStart);
   else
     ini->DeleteKey(sID,"FramePercentLower");

   if (bi_FrameEnd)
     ini->WriteInteger(sID, "FramePercentUpper", FrameEnd);
   else
     ini->DeleteKey(sID,"FramePercentUpper");

   if (bi_IssueInhibit)
     ini->WriteBool(sID, "IssueRecordInhibitCommand", IssueInhibit);
   else
     ini->DeleteKey(sID,"IssueRecordInhibitCommand");

   if (bi_TimeSenseType)
     ini->WriteInteger(sID, "TimeSenseType", TimeSenseType);
   else
     ini->DeleteKey(sID,"TimeSenseType");
}

//---------------------Read-----------------------John Mertus----Nov 2002-----

     void MachineSchemeDataStruct::Read(CIniFile *ini)

//  This reads the machine parameters, note Name must be defined properly.
//  ini is the current file from which to read
//
//****************************************************************************
{
   ini->ReadStringList(Name, "Section", &Sections);

   SupportsAutoEdit = ini->ReadBool(Name, "SupportsAutoEdit", true);
   InstructionDelay = ini->ReadInteger(Name, "InstructionDelay", 5);
   PrerollHint = ini->ReadInteger(Name, "PrerollHint", 5);
   PostrollHint = ini->ReadInteger(Name, "PostrollHint", 2);
   SetSensePreroll = ini->ReadBool(Name, "SetSensePreRoll", false);

   // Defaults are a special case, force all to be read
   if (EqualIgnoreCase(Name, "MachineDefaults"))
     {
       bi_PrerollHint = true;
       bi_PostrollHint = true;
       bi_SupportsAutoEdit = true;
       bi_InstructionDelay = true;
       bi_SetSensePreroll = true;
     }
   else
     {
       // Check for existence of key
       bi_PrerollHint = ini->KeyExists(Name, "PrerollHint");
       bi_PostrollHint = ini->KeyExists(Name, "PostrollHint");
       bi_SupportsAutoEdit = ini->KeyExists(Name, "SupportsAutoEdit");
       bi_InstructionDelay = ini->KeyExists(Name, "InstructionDelay");
       bi_SetSensePreroll = ini->KeyExists(Name,"SetSensePreRoll");
     }
}

//---------------------Write----------------------John Mertus----Nov 2002-----

     void MachineSchemeDataStruct::Write(CIniFile *ini)

//  This writes the machine scheme data to the ini file ini
//
//****************************************************************************
{
   string sID = Name;

   // Write the sections
   ini->WriteStringList(sID, "Section", &Sections);

   if (bi_SupportsAutoEdit)
     ini->WriteBool(sID, "SupportsAutoEdit", SupportsAutoEdit);
   else
     ini->DeleteKey(sID,"SupportsAutoEdit");

   if (bi_InstructionDelay)
     ini->WriteInteger(sID, "InstructionDelay", InstructionDelay);
   else
     ini->DeleteKey(sID,"InstructionDelay");

   if (bi_PrerollHint)
     ini->WriteInteger(sID, "PrerollHint", PrerollHint);
   else
     ini->DeleteKey(sID,"PrerollHint");

   if (bi_PostrollHint)
     ini->WriteInteger(sID, "PostrollHint", PostrollHint);
   else
     ini->DeleteKey(sID,"PostrollHint");

   if (bi_SetSensePreroll)
     ini->WriteBool(sID, "SetSensePreRoll", SetSensePreroll);
   else
     ini->DeleteKey(sID,"SetSensePreRoll");

}

//---------MachineSchemeDataStruct----------------John Mertus----Nov 2002-----

    MachineSchemeDataStruct::MachineSchemeDataStruct(void)

//  Usual constructor, set all the defaults.
//
//****************************************************************************
{
      SupportsAutoEdit = true;
      InstructionDelay = 5;
      PrerollHint = 5;
      PostrollHint = 2;
      SetSensePreroll = false;
      //
      //  Inheritance bits
      bi_SupportsAutoEdit = false;
      bi_InstructionDelay = false;
      bi_PrerollHint = false;
      bi_PostrollHint = false;
      bi_SetSensePreroll = false;
}

void __fastcall TVTRPanelFrame::ComputerDropListChange(TObject *Sender)
{
    if (ApplyQuery() == IDCANCEL)
      {
        for (int i=0; i < ComputerDropList->Items->Count; i++)
           if (ComputerDropList->Items->Strings[i].c_str() == sComputerName) ComputerDropList->ItemIndex = i;
        return;
      }
      
    sComputerName = ComputerDropList->Text.c_str();
    ReadFile(p_sCfgIniFileName);

    // Redisplay the scheme
    UpdateSchemeDisplay(SchemeFromName(LongSchemeName(SchemeGroup->Caption.c_str())));
    Modified = true;
}
//---------------------------------------------------------------------------


void __fastcall TVTRPanelFrame::MachineSchemeChangedClick(TObject *Sender)
{
   if (m_InhibitAllUpdates > 0) return;

   // Do the Machine defaults
   if (MachineListView->Selected == NULL) return;
   MachineSchemeData *MSD = (MachineSchemeData *)MachineListView->Selected->Data;
   if (MSD == NULL) return;

   if (MSD->bi_SupportsAutoEdit)
     MSD->SupportsAutoEdit = (SupportsAutoEditCBox->ItemIndex == 0);

   if (MSD->bi_InstructionDelay)
     MSD->InstructionDelay = InstructionDelaySEdit->Value;

   if (MSD->bi_PrerollHint)
     MSD->PrerollHint = PrerollSEdit->Value;

   if (MSD->bi_PostrollHint)
     MSD->PostrollHint = PostrollSEdit->Value;

   if (MSD->bi_SetSensePreroll)
     MSD->SetSensePreroll = (SetSensePrerollCBox->ItemIndex == 0);

   Modified = true;
   UpdateMachineSchemeDisplay(MSD);
}
//---------------------------------------------------------------------------

   void TVTRPanelFrame::ShowFormatDetails(void)
{
       FormatPanel->Visible = true;
       MachinePanel->Visible = false;
       InitialPanel->Visible = false;
}
   void TVTRPanelFrame::ShowMachineDetails(void)
{
       FormatPanel->Visible = false;
       MachinePanel->Visible = true;
       InitialPanel->Visible = false;
}

void __fastcall TVTRPanelFrame::MachineListViewClick(TObject *Sender)
{
   ShowMachineDetails();
   UpdateSchemeDisplay(DefaultScheme);
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::AssembleEditCBoxClick(TObject *Sender)
{
   Modified = true;
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::GlobalVTROffsetRecToComCheckBoxClick(
      TObject *Sender)
{
   Modified = true;
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::GlobalComputerOffsetRecToVTRCheckBoxClick(
      TObject *Sender)
{
   Modified = true;
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::GlobalVTROffsetRecToComSpinEditChange(
      TObject *Sender)
{
   Modified = true;
}
//---------------------------------------------------------------------------

void __fastcall TVTRPanelFrame::GlobalComputerOffsetRecToVTRSpinEditChange(
      TObject *Sender)
{
   Modified = true;
}
//---------------------------------------------------------------------------

