//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ParamPanelForm.h"
#include "IniFile.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TParamPanel *ParamPanel;
//---------------------------------------------------------------------------
//------------------TParamPanel-------------------John Mertus----Nov 2002-----

  __fastcall TParamPanel::TParamPanel(TComponent* Owner)
          : TFrame(Owner)

//  Usual Constructor
//
//****************************************************************************

{
   m_bModified = false;
   m_bDetails = false;
   m_bHasDetails = false;
   p_bAdminPriv = false;
   Caption = "Override Me and my Icon";
}
//---------------------------------------------------------------------------

//------------------SetModified-------------------John Mertus----Nov 2002-----

       void TParamPanel::SetModified(bool Value)

//  This is called when the Modified property is changed
//  Will generate a callback if desired
//
//****************************************************************************
{
   if (Value == m_bModified) return;
   m_bModified = Value;
   if (m_OnModified != NULL) m_OnModified(this);
}

//-------------------SetDetails-------------------John Mertus----Nov 2002-----

     void TParamPanel::SetDetails(bool Value)

//  Called when details change
//  This calls the virtual function DetailsChanged
//  and also a callback if desired
//   Note: The details will never change unless HasDetails is true.
//
//****************************************************************************
{
   // Nothing to do
   if (!m_bHasDetails) return;
   if (Value == m_bDetails) return;

   m_bDetails = Value;
   DetailsChanged(Value);
   if (m_OnDetails != NULL) m_OnDetails(this);
}

//-----------------DetailsChanged-----------------John Mertus----Nov 2002-----

     void TParamPanel::DetailsChanged(bool Value)

//  Virtual Details change callback, do nothing
//
//****************************************************************************
{
   return;
}

//-----------------ChangeHintOnEdit---------------John Mertus----Nov 2002-----

   void TParamPanel::ChangeHintOnEdit(TObject *Sender)

//  This changes the hint on the TEdit by expanding any $ varaiables
//
//****************************************************************************
{
    TEdit *te = (TEdit *)Sender;
    string sHint = ConformEnvironmentName(te->Text.c_str());
    te->Hint = sHint.c_str();
}

//---------------------IniHasChangedCB------------John Mertus----Nov 2002-----

   bool  TParamPanel::IniHasChangedCB(void)

//  This is called when the environment variable is changed.  The changing
// program calls this.  The derived class should call ChangeHintOnEdit for
// those that use environment variables and reload the files.
//
//****************************************************************************
{
   return true;
}

//---------------------ChangeIniFile--------------John Mertus----Nov 2002-----

    bool TParamPanel::ChangeIniFile(TObject *Sender)

// This special purpose is called when a Edit that controls an ini file
// has changed.  Sender is the TEdit that represents the ini file
//
//  This checks to see if the loaded file matches, if so it does nothing
// Then it checks if the file exists and alows a backout
// Finally it reloads the file.
//   Two hidden values are used
//    p_sLoadedIniFileName  which is the complete name of the file
//    p_sCfgIniFileName which is the name of the fine in MTILocalMachine configuration
//
// Return is true if the Edit name matches the loaded name after the call.
//
//****************************************************************************
{
   TEdit *Edit = (TEdit *)Sender;
   string sCfgName = ConformEnvironmentName(Edit->Text.c_str());
   if (EqualIgnoreCase(p_sLoadedIniFileName, sCfgName)) return true;

   // If file does not exist, question if we want a new one
   if (!DoesFileExist(sCfgName))
     if (_MTIConfirmationDialog(Handle, (string)"File " + sCfgName + " Does not exist\nCreate a new file?") != MTI_DLG_OK)
       {
          Edit->SetFocus();
          return false;
       }

   // Ask to see if we want to save the data
   ApplyQuery();
   ReadFile(sCfgName);
   ChangeHintOnEdit(Edit);

   // The modified bit is set if the file in the local machine is
   // different from the displayed one.
   Modified = !EqualIgnoreCase(Edit->Text.c_str(), p_sCfgIniFileName);
   return true;
}


//--------------------ApplyQuery------------------John Mertus----Nov 2002-----

   int  TParamPanel::ApplyQuery(void)

//  Call this when the user needs to be asked if the data should be saved.
// Return is button pressed or IDOK if there were no changes to saave
//
//****************************************************************************
{
   int mb = IDOK;
   if (Modified)
     {
       String str = (String)"The " + Caption + " Configuration has been modified\nSave Configuration?";
       mb = MessageBox(Application->Handle, str.c_str(),
            "Configuration Not Saved", MB_ICONWARNING | MB_YESNOCANCEL);
       if (mb == IDYES) Apply();
    }

   return mb;
}
//-------------------EnableDetails----------------John Mertus----Nov 2002-----

     void  TParamPanel::EnableDetails(bool Value)

//  This is used only at constructor time to set if there are details or not.
//  It sets the HasDetails flag to VALUE and turns the details true.
//
//****************************************************************************
{
     m_bHasDetails = Value;
     m_bDetails = true;
}

//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TParamPanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Override this
//  to handle your own.  Call this from the overriden routine!
//
//****************************************************************************
{
   p_bAdminPriv = Value;
}

   void TParamPanel::UpdateStatusBar(const string &Msg, int iPanel)
{

   SendMessage(Parent->Handle, CM_STATUSMESSAGE, (int)Msg.c_str(), iPanel);
   Application->ProcessMessages();
}
