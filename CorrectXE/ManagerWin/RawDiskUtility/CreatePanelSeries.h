//---------------------------------------------------------------------------


#ifndef CreatePanelSeriesH
#define CreatePanelSeriesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include "RdWriter.h"
#include "ReadWriteControl.h"
#include <Buttons.hpp>
#include <Graphics.hpp>


//---------------------------------------------------------------------------
class TPanelSeries : public TFrame
{
__published:	// IDE-managed Components
        TPanel *writePanel;
        TLabel *Label12;
        TLabel *statusLabel;
        TGroupBox *GroupBox2;
        TLabel *pathLabel;
        TLabel *sizeLabel;
        TLabel *nameLabel;
        TProgressBar *rawDiskProgressBar;
        TPanel *introPanel;
        TPanel *diskSizePanel;
        TLabel *Label7;
        TLabel *rawDiskSizeLabel;
        TLabel *minLabel;
        TLabel *maxLabel;
        TImage *DiskWarningLine;
        TTrackBar *diskSizeTrackbar;
        TGroupBox *GroupBox3;
        TLabel *Label3;
        TRadioButton *noAudioRadio;
        TRadioButton *audioFromFileRadio;
        TEdit *audioDirectoryEdit;
        TPanel *fileNamePanel;
        TLabel *Label2;
        TLabel *Label4;
        TLabel *Label13;
        TGroupBox *GroupBox1;
        TLabel *Label5;
        TLabel *Label6;
        TEdit *diskNameEdit;
        TSaveDialog *rawDiskDialog;
        TOpenDialog *openAudioDialog;
        TSpeedButton *browseButton;
        TSpeedButton *defaultButton;
        TPanel *Panel1;
        TLabel *directoryLabel;
        TButton *makeRawDiskButton;
        TImage *Image1;
        TMemo *introMemo;

        
        void __fastcall browseButtonClick(TObject *Sender);
        void __fastcall enterChunkSize(TObject *Sender);
         void __fastcall dragBar(TObject *Sender);
        void __fastcall makeRawDiskButtonClick(TObject *Sender);
        void __fastcall genericRadioClick(TObject *Sender);
        void __fastcall defaultButtonClick(TObject *Sender);


private:	// User declarations

        TPanel *currentpanel;
        //This boolean makes the cancel method behave differently when disk writing is in progress.
        bool writing_todisk;
        TPanel *panellist[4];
        String rawdiskdir;
        String sharedir;
        String disk_name;
        String audio_dir;
        
        int panelindex;
        int audio_opt;
        __int64 disk_spacefree;
        __int64 chunk_size;
        __int64 current_size;
        __int64 max_size;
        int start_mode;
        bool refreshDirectory();
        void calculateMaxDiskSize();
        RdWriter *rd_writer;
        String formatIntegerString(__int64 i);
        void prepConfigure();


        //void setOwner(TReadWriteControlForm *rwc);



public:		// User declarations
        __fastcall TPanelSeries(TComponent* Owner);
        __fastcall ~TPanelSeries();
        void next();
          void  back();
           bool can_gonext;
           bool can_goprev;
           bool at_end;
           void resetPanels();
           void resetEverything();
    
};
//---------------------------------------------------------------------------
extern PACKAGE TPanelSeries *PanelSeries;
//---------------------------------------------------------------------------
#endif
