#ifndef ReadWriteUtilityExceptionH
#define ReadWriteUtilityExceptionH
#include <Classes.hpp>

//---------------------------------------------------------------------------
class ReadWriteUtilityException{
private:
public:
        __fastcall ReadWriteUtilityException(char* msg, String pur);
        char* _message;
        String _purpose;

protected:

};
#endif
