//---------------------------------------------------------------------------


#ifndef RawDiskAddFrameH
#define RawDiskAddFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "ParamPanelForm.h"

//---------------------------------------------------------------------------
class TrawDiskProperties : public TParamPanel
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TComboBox *rawDiskCombo;
        TPanel *Panel1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TEdit *diskNameEdit;
        TEdit *sizeEdit;
        TEdit *audioEdit;
        TEdit *flPathEdit;
        TButton *rdButton;
        TButton *browseButton;
        TLabel *Label6;
        TEdit *vidDirEdit;
        void __fastcall browseButtonClick(TObject *Sender);
        void __fastcall rdButtonClick(TObject *Sender);
        void __fastcall rawDiskComboChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TrawDiskProperties(TComponent* Owner);
                void __fastcall SetAdminPriv(bool Value);
        virtual void DetailsChanged(bool Value);
        String selectedDisk;
};
//---------------------------------------------------------------------------
extern PACKAGE TrawDiskProperties *rawDiskProperties;
//---------------------------------------------------------------------------
#endif
