object DeleteFrame: TDeleteFrame
  Left = 0
  Top = 0
  Width = 551
  Height = 368
  TabOrder = 0
  object Panel1: TPanel
    Left = 5
    Top = 8
    Width = 532
    Height = 329
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 197
      Width = 138
      Height = 13
      Caption = 'Select a Videostore to Delete'
    end
    object percentLabel: TLabel
      Left = 16
      Top = 296
      Width = 14
      Height = 13
      Caption = '0%'
    end
    object statusLabel: TLabel
      Left = 64
      Top = 304
      Width = 3
      Height = 13
    end
    object Label3: TLabel
      Left = 24
      Top = 24
      Width = 473
      Height = 64
      Caption = 
        'IMPORTANT:  Deleting a Videostore will PERMANENTLY remove all cl' +
        'ips and information stored on that videostore. Deleting videosto' +
        'res is strongly discouraged!  If you must delete a Videostore, b' +
        'ack up all information on it before clicking delete.'
      Color = clBtnHighlight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      WordWrap = True
    end
    object deleteComboBox: TComboBox
      Left = 16
      Top = 224
      Width = 201
      Height = 21
      ItemHeight = 13
      TabOrder = 0
    end
    object deleteProgressBar: TProgressBar
      Left = 16
      Top = 264
      Width = 369
      Height = 25
      Min = 0
      Max = 100
      TabOrder = 1
    end
    object deleteButton: TButton
      Left = 392
      Top = 264
      Width = 113
      Height = 25
      Caption = 'Delete'
      TabOrder = 2
      OnClick = deleteButtonClick
    end
    object GroupBox1: TGroupBox
      Left = 24
      Top = 104
      Width = 473
      Height = 81
      Caption = 'Instructions'
      TabOrder = 3
      object Label2: TLabel
        Left = 16
        Top = 24
        Width = 394
        Height = 39
        Caption = 
          'Select a Videostore to remove from the menu below. Then, click d' +
          'elete. Note that deleting the Videostore will remove it complete' +
          'ly from your hard drive along with any clips or audio stored on ' +
          'the Videostore. '
        WordWrap = True
      end
    end
  end
end
