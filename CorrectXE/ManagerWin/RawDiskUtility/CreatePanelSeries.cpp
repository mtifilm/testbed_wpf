





#include <vcl.h>
#pragma hdrstop
#include "FileSweeper.h"
#include "ReadWriteControl.h"
#include "WriteWrapper.h"
#include "RdWriter.h"
#include "DSFileWrapper.h"
#include "CreatePanelSeries.h"
#pragma package(smart_init)
#pragma resource "*.dfm"

//I'm still learning, so if you want to change this, you need to change it here and
//also in the array size declaration. this is just so the program can keep track.

const int PANEL_ARRAY_SIZE = 4;

//Audio options corresponding with the radio buttons.
const int AUDIO_NONE = 0;
const int AUDIO_RAWDISK = 1;
const int AUDIO_FILE = 2;

const int START_CREATE = 5;
const int START_DELETE = 6;
const int START_EDIT = 7;
const int START_SPEED = 8;

TPanelSeries *PanelSeries;


//---------------------------------------------------------------------------
//Destructor
//---------------------------------------------------------------------------
__fastcall TPanelSeries::~TPanelSeries(){
delete rd_writer;
rd_writer = 0;

}

//---------------------------------------------------------------------------
//Constructor
//---------------------------------------------------------------------------
__fastcall TPanelSeries::TPanelSeries(TComponent* Owner)
        : TFrame(Owner)
{

        at_end = false;

        writing_todisk = false;
        panelindex = 0;
         panellist[0] = introPanel;
         panellist[1] = fileNamePanel;
         panellist[2] = diskSizePanel;
         panellist[3] = writePanel;
        //Make an array of the panels so we can navigate through them.
        currentpanel = panellist[0];
        currentpanel->Visible = true;
        can_gonext = true;
        can_goprev = false;


        chunk_size = 15625;
        disk_spacefree = -1;
        current_size = -1;
        audio_opt = 0;
        audio_dir = "";
        rd_writer = new RdWriter();
        diskNameEdit->Text = "rd" + IntToStr(rd_writer->getRawDiskNo() + 1);
       directoryLabel->Caption = "f:\\rawdisks\\" + diskNameEdit->Text + ".raw";

        //draws a little line to indicate where disk gets slow and full.
        TCanvas *canvas = DiskWarningLine->Canvas ;
        canvas->Brush->Color=clLime;
        canvas->Rectangle(0,0,(DiskWarningLine->Width*80/100),DiskWarningLine->Height);
        canvas->Brush->Color = clYellow;
        canvas->Rectangle((DiskWarningLine->Width*80/100),0, (DiskWarningLine->Width*95/100),DiskWarningLine->Height);
        canvas->Brush->Color = clRed;
        canvas->Rectangle((DiskWarningLine->Width*95/100),0,DiskWarningLine->Width,DiskWarningLine->Height);

}
//---------------------------------------------------------------------------
//Method which manages the gui display when the next button is clicked.
//------------------------------------------------------------------------------

void  TPanelSeries::next()
{

  bool cangoforward = false;
if(panelindex == 0){
        at_end = false;
 
        cangoforward = true;
        }
//if info isn't filled out, we can't proceed.
else if(panelindex==1){
        cangoforward  = false;

        if (refreshDirectory()){

                 int diskdriveletter = int(toupper(rawdiskdir[1]));
                 //convert it to the int that the diskspace method wants....(A is 65, drive A: = 1)
                 diskdriveletter = diskdriveletter - 64;
                 bool dir = DoesDirectoryExist((rawdiskdir.c_str()));
                 disk_spacefree=DiskFree(diskdriveletter);
                 //Don't go to the next screen if the drive letter is invalid.
                 if((disk_spacefree!=-1)||(dir)){
                        cangoforward = true;
                 }
                else{
                        ShowMessage("Invalid drive letter or directory specified.");
                        }
        }
   calculateMaxDiskSize();
 }
else if (panelindex  == 2){

//gets the audio button values from the previous panel.
      cangoforward = false;
      if (noAudioRadio->Checked){
                audio_dir = "";
                cangoforward = true;
                }

      else{
                if(DoesDirectoryExist((audioDirectoryEdit->Text).c_str())){
                      
                        audio_dir = String((GetFilePath((audioDirectoryEdit->Text).c_str())).c_str());
                        cangoforward = true;
                  }
                else{
                        ShowMessage("Must enter a valid audio directory");
                        }

      }
      

        pathLabel->Caption = "Path: " + rawdiskdir;
        sizeLabel->Caption = "Size: " + formatIntegerString(current_size) + " bytes";
        nameLabel->Caption = "Name: " + disk_name;



        }
else if (panelindex == 3){
        at_end = false;
        }

      if(cangoforward == true){
        if (panelindex < PANEL_ARRAY_SIZE){

                panelindex ++;

                currentpanel->Visible = false;
                currentpanel =  panellist[panelindex];
                currentpanel->Visible = true;
                can_goprev = true;
                if(panelindex+1 == PANEL_ARRAY_SIZE){
                     //can_gonext  = false;
                }

        }
        }
       //Assuming that we just jumped forward, the back button should now be enabled.
       can_goprev = true;

}
//---------------------------------------------------------------------------
//Manages gui buttons and display when back is clicked.
//---------------------------------------------------------------------------

void  TPanelSeries::back()
{
if(panelindex == 3){
        can_goprev = true;              //Fix so that we don't restart the program if a person presses back after they've
        at_end = false;
        makeRawDiskButton->Enabled = true;

        }
if (panelindex >0 ){
        panelindex --;
        currentpanel->Visible = false;
        currentpanel = panellist[panelindex];
        currentpanel->Visible = true;
        can_goprev = true;


        }
if(panelindex == 0){
         can_goprev = false;

        }


        //assuming we just jumped back, we know there's something to go forward to.
        can_gonext = true;
}

//---------------------------------------------------------------------------
//Callback for the browse button for rawdisk directories.
//---------------------------------------------------------------------------

void __fastcall TPanelSeries::browseButtonClick(TObject *Sender)
{
       if(rawDiskDialog->Execute()){
               directoryLabel->Caption = rawDiskDialog->FileName;
                }
}
//---------------------------------------------------------------------------
//-------------Method which gets values ffor the rawdisk location and--------
//---------Tosses mad errors if someone entered nothing or inappropriate chars.
//---------------------------------------------------------------------------

bool TPanelSeries::refreshDirectory()
{
        bool breturn = true;
        if((directoryLabel->Caption!="")&&(diskNameEdit->Text!="")){

                        rawdiskdir = directoryLabel->Caption;
                        disk_name = diskNameEdit->Text;
                         breturn = true;
         }
        else{
                ShowMessage("You must fill in all fields.");
                 breturn  = false;
        }
        return breturn;

}
//------------------------------------------------------------------------------
//Handler for the disk size slider bar.
//------------------------------------------------------------------------------

void __fastcall TPanelSeries::dragBar(TObject *Sender)
{       __int64 X = diskSizeTrackbar->Position;
       // __int64 percentfree = (disk_spacefree * X)/diskSizeTrackbar->Width;
        if (chunk_size >0){
                __int64 newloc = ((chunk_size*512)*X);
                rawDiskSizeLabel->Caption = formatIntegerString(newloc) + "("+(IntToStr((current_size*100)/max_size))+"%"+")";
                current_size = newloc;
                }
}

//------------------------------------------------------------------------------
//If a person ever wanted to make the chunk size customizable, they'd use this
//handler.
//------------------------------------------------------------------------------

void __fastcall TPanelSeries::enterChunkSize(TObject *Sender)
//If we wanted to get the chunk size from the user, we would assign this method as a callback and use it to
//set the chunk_size variable to something from a text box.
{
                        calculateMaxDiskSize();
}

//------------------------------------------------------------------------------
//Handler for the make rawdisk button. Starts to create the rawdisk with the writer.
//------------------------------------------------------------------------------

void __fastcall TPanelSeries::makeRawDiskButtonClick(TObject *Sender)
{
        at_end = false;
     WriteWrapper *wr = new WriteWrapper(rawdiskdir, chunk_size, current_size, 1, disk_name,3,0,"","","");
        makeRawDiskButton->Enabled = false;

        writing_todisk = true;

        //For the sake of modularity, you can pass nulls into this method too. All it needs is the wrapper.
        rd_writer->writeDisk(wr, rawDiskProgressBar, statusLabel);
        rawDiskProgressBar->Position = 0;
        statusLabel->Caption = " ";
        prepConfigure();
        writing_todisk = false;
        
        can_gonext = true;
        at_end = true;
        delete wr;
        wr = 0;
}


//------------------------------------------------------------------------------
//Sets up the configuration files.
//------------------------------------------------------------------------------

 void TPanelSeries::prepConfigure(){

        //Method manages ini file writing when a new disk is created.
        //It makes a wrapper and passes it to the rdwriter which writes the information to the inifile,
        //the file written to depends on the method called.


        String freelistfile = ExtractFilePath(rawdiskdir);
        freelistfile = freelistfile + disk_name + ".rfl";
        __int64 csbyte = current_size/512;

         //Makes a wrapper for the config file information.
        DSFileWrapper *sw = new DSFileWrapper(disk_name,csbyte,rawdiskdir,freelistfile,1,audio_dir);
        rd_writer->setupDiskScheme(sw, rawDiskProgressBar,statusLabel);
        rd_writer->setupFreelist(sw, rawDiskProgressBar, statusLabel);
        rd_writer->setupRawDiskConfig(sw, rawDiskProgressBar, statusLabel);
        rawDiskProgressBar->Position = 0;
        statusLabel->Caption = "Finished.";

        delete sw;
        sw = 0;


 }

 //-----------------------------------------------------------------------------
 //Calculates the max disk size that can be used in a rawdisk file.
 //-----------------------------------------------------------------------------

 void TPanelSeries::calculateMaxDiskSize(){
        //95% of the disk space is safe no matter what the disk setup.
        __int64 maxdisksize = (disk_spacefree * 95)/100;
        __int64 minsize = 1;
          try{
                if ((chunk_size < disk_spacefree)&& (chunk_size > 0)){
                        minsize = chunk_size * 512;
                        __int64 divdisk = ((disk_spacefree*95)/100)/(minsize);
                        maxdisksize = (minsize) * (divdisk);
                        max_size = maxdisksize;
                        diskSizeTrackbar->Max = divdisk;
                        maxLabel->Caption = formatIntegerString(maxdisksize) + " bytes";

                        diskSizeTrackbar->Min = 1;
                        minLabel->Caption = formatIntegerString(minsize)+ " bytes";

                        current_size =  minsize;
                        }
               }
               //This error should be modified should we ever want to enter the chunk size manually.
               catch(Exception &e){
                        ShowMessage("Invalid Chunk Size.");
                        }
}

//------------------------------------------------------------------------------
//This method is purely cosmetic. It pust commas in the numbers on the numberline labels.
//It should be named "addCommas" but that would be unprofessional....
//------------------------------------------------------------------------------

      String TPanelSeries::formatIntegerString(__int64 i){
        String str = IntToStr(i);

        int length = str.Length();
        int divl = length/3;
        int modl = length % 3;
        int lowercount = 0;
        String comma = ",";
        String stringbuffr = "";
        String modstr = str.SubString(lowercount,lowercount+modl);
        if(modl>0){
                str = str.SubString(lowercount+modl+1, length);
                stringbuffr += modstr + comma;
                }
        length = str.Length();
        for(int i = 0;i<divl;i++){
                for(int k = 0;k<3;k++){
                        String newstr = str.SubString(3*i+k,1);
                        stringbuffr +=newstr;
                 }

                if(3*(i +1) < length){
                         stringbuffr+=comma;
                         }
       }
        return stringbuffr;
      }

//------------------------------------------------------------------------------
//Sets all the text boxes to 0 and the sliders to the start.
//------------------------------------------------------------------------------

 void TPanelSeries::resetEverything(){

        diskNameEdit->Text = "rd" + IntToStr(rd_writer->getRawDiskNo() + 1);
        directoryLabel->Caption = "f:\\rawdisks\\" + diskNameEdit->Text + ".raw";
        diskSizeTrackbar->Position = 0;
        pathLabel->Caption = "Path:";
        nameLabel->Caption = "Name:";
        sizeLabel->Caption = "Size:";
        statusLabel->Caption = " ";
        makeRawDiskButton->Enabled = true;
        rawDiskProgressBar->Position = 0;

 }
 //-----------------------------------------------------------------------------
 //Resets the panels at the beginning. Should be called in conjunction with resetEverything()
 //for best results.
 //-----------------------------------------------------------------------------

 void TPanelSeries::resetPanels(){
        currentpanel->Visible = false;
        currentpanel = panellist[0];
        panelindex = 0;
        currentpanel->Visible = true;
        can_gonext = true;
        can_goprev = false;
        at_end = false;
        }





void __fastcall TPanelSeries::genericRadioClick(TObject *Sender){
        if  (audioFromFileRadio->Checked){
                audioDirectoryEdit->Enabled = true;
                audioDirectoryEdit->Color = clWindow;
           

                }
        else{
                audioDirectoryEdit->Enabled = false;
                audioDirectoryEdit->Color = cl3DLight;
                }

}


void __fastcall TPanelSeries::defaultButtonClick(TObject *Sender)
{

  directoryLabel->Caption = "f:\\rawdisks\\" + diskNameEdit->Text + ".raw";
}
//---------------------------------------------------------------------------


