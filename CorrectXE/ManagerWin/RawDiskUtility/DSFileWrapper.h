//---------------------------------------------------------------------------

#ifndef DSFileWrapperH
#define DSFileWrapperH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>

class DSFileWrapper{
private:
__int64 num_bytes;
String disk_path;
String fl_path;
int fast_io;
String audio_path;
int num_disks;
String disk_name;

public:
__fastcall DSFileWrapper( String name,__int64 nobytes, String fullpath, String freelistdir, int fastio, String audiodirpath);
String getDiskName();

__int64 getNoBytes();
String getFullPath();
String getFreelistDir();
int getFastio();
String getAudioPath();


};
//---------------------------------------------------------------------------
#endif
