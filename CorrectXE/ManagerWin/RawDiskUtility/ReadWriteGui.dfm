object rawDiskIntroForm: TrawDiskIntroForm
  Left = 900
  Top = 147
  Width = 563
  Height = 437
  Caption = 'MTI RawDisk Utility'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object fileNamePanel: TPanel
    Tag = 2
    Left = 8
    Top = 8
    Width = 537
    Height = 345
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    Visible = False
    object Label1: TLabel
      Left = 56
      Top = 80
      Width = 100
      Height = 13
      Caption = '# Existing Raw Disks'
    end
    object Label2: TLabel
      Left = 56
      Top = 24
      Width = 91
      Height = 13
      Caption = 'Raw Disk Directory'
    end
    object Label4: TLabel
      Left = 56
      Top = 96
      Width = 3
      Height = 13
      WordWrap = True
    end
    object Label13: TLabel
      Left = 56
      Top = 128
      Width = 91
      Height = 13
      Caption = 'Desired Disk Name'
    end
    object diskPathEdit: TEdit
      Left = 56
      Top = 40
      Width = 337
      Height = 21
      Hint = 'Directory in which the raw disk will be created.'
      Color = cl3DLight
      Enabled = False
      TabOrder = 0
      Text = 'f:\rawdisks\rd1.raw'
    end
    object browseButton: TButton
      Left = 408
      Top = 40
      Width = 105
      Height = 25
      Caption = 'Browse...'
      TabOrder = 1
      OnClick = browseButtonClick
    end
    object numDisksEdit: TEdit
      Left = 56
      Top = 96
      Width = 25
      Height = 21
      Hint = 'Enter the number of Raw Disks already on this computer.'
      TabOrder = 2
      Text = '0'
      OnChange = numDisksEditChange
    end
    object GroupBox1: TGroupBox
      Left = 104
      Top = 184
      Width = 329
      Height = 121
      Caption = 'Instructions'
      TabOrder = 3
      object Label5: TLabel
        Left = 8
        Top = 24
        Width = 301
        Height = 39
        Caption = 
          'Enter  the directory in which the raw disk file should be create' +
          'd. The default is X:\rawdisks where X is the letter of the desir' +
          'ed hard disk.'
        WordWrap = True
      end
      object Label6: TLabel
        Left = 8
        Top = 72
        Width = 292
        Height = 26
        Caption = 
          'Under MTIShare Directory, locate your MTI Share folder. The defa' +
          'ult for new versions is X:\MTIShare.'
        WordWrap = True
      end
    end
    object diskNameEdit: TEdit
      Left = 56
      Top = 144
      Width = 169
      Height = 21
      TabOrder = 4
      Text = 'rd1'
    end
  end
  object diskSizePanel: TPanel
    Tag = 3
    Left = 8
    Top = 8
    Width = 537
    Height = 345
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 6
    Visible = False
    object Label7: TLabel
      Left = 16
      Top = 56
      Width = 108
      Height = 13
      Caption = 'Desired Raw Disk Size'
    end
    object rawDiskSizeLabel: TLabel
      Left = 144
      Top = 56
      Width = 71
      Height = 13
      Caption = '7,668,000 (0%)'
    end
    object minLabel: TLabel
      Left = 24
      Top = 136
      Width = 6
      Height = 13
      Caption = '0'
    end
    object maxLabel: TLabel
      Left = 406
      Top = 136
      Width = 6
      Height = 13
      Caption = '0'
    end
    object DiskWarningLine: TImage
      Left = 24
      Top = 77
      Width = 489
      Height = 9
      Transparent = True
    end
    object diskSizeTrackbar: TTrackBar
      Left = 16
      Top = 96
      Width = 513
      Height = 25
      Orientation = trHorizontal
      Frequency = 10
      Position = 0
      SelEnd = 0
      SelStart = 0
      TabOrder = 0
      TickMarks = tmBottomRight
      TickStyle = tsAuto
      OnChange = dragBar
    end
    object GroupBox3: TGroupBox
      Left = 56
      Top = 160
      Width = 417
      Height = 169
      Caption = 'Audio Options'
      TabOrder = 1
      object Label3: TLabel
        Left = 16
        Top = 16
        Width = 352
        Height = 39
        Caption = 
          'Select the location from which clips stored on this rawdisk will' +
          ' obtain audio. If you select "Get Sound from Rawdisk", be sure y' +
          'our disk is fast enough to read/write images and audio simultane' +
          'ously in realtime.'
        WordWrap = True
      end
      object noAudioRadio: TRadioButton
        Left = 16
        Top = 64
        Width = 385
        Height = 25
        Caption = 'No Sound'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object audioOnRawdiskRadio: TRadioButton
        Tag = 1
        Left = 16
        Top = 80
        Width = 281
        Height = 33
        Caption = 'Get Sound From Rawdisk'
        TabOrder = 1
      end
      object audioFromFileRadio: TRadioButton
        Tag = 2
        Left = 16
        Top = 104
        Width = 153
        Height = 25
        Caption = 'Get Sound from Files'
        TabOrder = 2
      end
      object audioDirectoryEdit: TEdit
        Left = 16
        Top = 128
        Width = 265
        Height = 21
        Color = cl3DLight
        TabOrder = 3
        Text = 'c:\'
      end
      object browseAudioButton: TButton
        Left = 288
        Top = 128
        Width = 97
        Height = 22
        Caption = 'Browse...'
        TabOrder = 4
      end
    end
  end
  object introPanel: TPanel
    Tag = 1
    Left = 8
    Top = 8
    Width = 537
    Height = 345
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    Visible = False
    object introMemo: TMemo
      Left = 8
      Top = 120
      Width = 521
      Height = 217
      BevelEdges = []
      BevelInner = bvLowered
      BevelOuter = bvRaised
      Color = clScrollBar
      Lines.Strings = (
        'Welcome to the MTI Raw Disk Creator!'
        ''
        
          'This utility will guide you through the creation of raw disks: p' +
          'ortions of your hard drive devoted to storing '
        'video.'
        
          'It is recommended that you close all other MTI applications and ' +
          'make sure you have plenty of free space on '
        'your hard drive before you continue.')
      TabOrder = 0
    end
  end
  object writePanel: TPanel
    Tag = 4
    Left = 8
    Top = 8
    Width = 537
    Height = 345
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 4
    Visible = False
    object Label12: TLabel
      Left = 24
      Top = 152
      Width = 63
      Height = 13
      Caption = '% Completion'
    end
    object statusLabel: TLabel
      Left = 99
      Top = 152
      Width = 3
      Height = 13
    end
    object GroupBox2: TGroupBox
      Left = 16
      Top = 24
      Width = 273
      Height = 89
      Caption = 'Raw Disk'
      TabOrder = 0
      object pathLabel: TLabel
        Left = 8
        Top = 24
        Width = 25
        Height = 13
        Caption = 'Path:'
      end
      object sizeLabel: TLabel
        Left = 8
        Top = 68
        Width = 23
        Height = 13
        Caption = 'Size:'
      end
      object nameLabel: TLabel
        Left = 8
        Top = 47
        Width = 31
        Height = 13
        Caption = 'Name:'
      end
    end
    object rawDiskProgressBar: TProgressBar
      Left = 24
      Top = 176
      Width = 489
      Height = 33
      Min = 0
      Max = 100
      TabOrder = 1
    end
    object makeRawDiskButton: TButton
      Left = 400
      Top = 224
      Width = 113
      Height = 33
      Caption = 'Make Raw Disk'
      TabOrder = 2
      OnClick = makeRawDiskButtonClick
    end
  end
  object nextButton: TButton
    Left = 416
    Top = 360
    Width = 123
    Height = 33
    Caption = 'Next'
    TabOrder = 0
    OnClick = nextButtonClick
  end
  object cancelButton: TButton
    Left = 8
    Top = 360
    Width = 123
    Height = 33
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = cancelButtonClick
  end
  object backButton: TButton
    Left = 264
    Top = 360
    Width = 123
    Height = 33
    Caption = 'Back'
    TabOrder = 3
    OnClick = backButtonClick
  end
  object rawDiskDialog: TSaveDialog
    DefaultExt = '.raw'
    Filter = 'Rawfiles|*.raw'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 16
    Top = 16
  end
end
