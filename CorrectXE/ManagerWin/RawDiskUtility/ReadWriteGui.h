//---------------------------------------------------------------------------

#ifndef ReadWriteGuiH
#define ReadWriteGuiH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include "RdWriter.h"
//---------------------------------------------------------------------------
class TrawDiskIntroForm : public TForm
{

__published:	// IDE-managed Components
        TButton *nextButton;
        TButton *cancelButton;
        TPanel *introPanel;
        TMemo *introMemo;
        TButton *backButton;
        TPanel *fileNamePanel;
        TEdit *diskPathEdit;
        TButton *browseButton;
        TEdit *numDisksEdit;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
        TGroupBox *GroupBox1;
        TLabel *Label5;
        TLabel *Label6;
        TPanel *diskSizePanel;
        TLabel *Label7;
        TTrackBar *diskSizeTrackbar;
        TPanel *writePanel;
        TGroupBox *GroupBox2;
        TLabel *pathLabel;
        TLabel *sizeLabel;
        TProgressBar *rawDiskProgressBar;
        TLabel *Label12;
        TButton *makeRawDiskButton;
        TSaveDialog *rawDiskDialog;
        TLabel *rawDiskSizeLabel;
        TLabel *minLabel;
        TLabel *maxLabel;
        TEdit *diskNameEdit;
        TLabel *Label13;
        TLabel *nameLabel;
        TLabel *statusLabel;
        TImage *DiskWarningLine;
        TGroupBox *GroupBox3;
        TRadioButton *noAudioRadio;
        TRadioButton *audioOnRawdiskRadio;
        TRadioButton *audioFromFileRadio;
        TEdit *audioDirectoryEdit;
        TButton *browseAudioButton;
        TLabel *Label3;

        void __fastcall nextButtonClick(TObject *Sender);
        void __fastcall backButtonClick(TObject *Sender);
        void __fastcall browseButtonClick(TObject *Sender);
        void __fastcall dragBar(TObject *Sender);
        void __fastcall enterChunkSize(TObject *Sender);
        void __fastcall makeRawDiskButtonClick(TObject *Sender);

        void __fastcall cancelButtonClick(TObject *Sender);
        void __fastcall numDisksEditChange(TObject *Sender);
        void __fastcall firstChoicePanelClick(TObject *Sender);


        
       


private:	// User declarations
        TPanel *currentpanel;
        //This boolean makes the cancel method behave differently when disk writing is in progress.
        bool writing_todisk;
        TPanel *panellist[4];
        String rawdiskdir;
        String sharedir;
        String disk_name;
        String audio_dir;
        int numdisks;
        int panelindex;
        int audio_opt;
        __int64 disk_spacefree;
        __int64 chunk_size;
        __int64 current_size;
        __int64 max_size;
        int start_mode;
        bool refreshDirectory();
        void calculateMaxDiskSize();
        RdWriter *rd_writer;
        String formatIntegerString(__int64 i);
        void prepConfigure();


public:		// User declarations
       __fastcall TrawDiskIntroForm(TComponent* Owner);
        __fastcall ~TrawDiskIntroForm (void);
        void incrementProgressBar(int toIncrement);


};
//---------------------------------------------------------------------------
extern PACKAGE TrawDiskIntroForm *rawDiskIntroForm;
//---------------------------------------------------------------------------
#endif
