object ReadWriteControlForm: TReadWriteControlForm
  Left = 248
  Top = 277
  Width = 589
  Height = 442
  Caption = 'MTI Videostore Utility'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  inline IntroFrame1: TIntroFrame
    Left = 8
    Top = 16
    Width = 545
    Height = 345
    TabOrder = 0
    inherited Image1: TImage
      Width = 209
    end
    inherited Panel1: TPanel
      Left = 232
      Width = 313
      inherited Label1: TLabel
        Caption = 
          '        Welcome to the MTI Videostore Utility.                  ' +
          ' This wizard will help you to create and edit Videostores (rawdi' +
          'sks): sections of your hard drive in which you can store your au' +
          'dio and video media for quick access.'
      end
    end
    inherited deleteRadio: TRadioButton
      Left = 256
      Hint = 'Remove a video store and all of the clips on it.'
      ParentShowHint = False
      ShowHint = True
    end
    inherited createRadio: TRadioButton
      Left = 256
      Hint = 'Create a new video store.'
      ParentShowHint = False
      ShowHint = True
    end
  end
  object nextButton: TBitBtn
    Left = 448
    Top = 376
    Width = 97
    Height = 33
    Caption = 'Next'
    TabOrder = 3
    OnClick = nextButtonClick
    Glyph.Data = {
      DE000000424DDE0000000000000076000000280000000D0000000D0000000100
      04000000000068000000C40E0000C40E00001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      F000FFFFFFF0FFFFF000FFFFFFF00FFFF000FFFFFFF000FFF000FFFFFFF0000F
      F000000000000000F0000000000000000000000000000000F000FFFFFFF0000F
      F000FFFFFFF000FFF000FFFFFFF00FFFF000FFFFFFF0FFFFF000FFFFFFFFFFFF
      F000}
  end
  object backButton: TBitBtn
    Left = 344
    Top = 376
    Width = 89
    Height = 33
    Caption = 'Back'
    TabOrder = 4
    OnClick = backButtonClick
    Glyph.Data = {
      DE000000424DDE0000000000000076000000280000000D0000000D0000000100
      04000000000068000000C40E0000C40E00001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      F000FFFFF0FFFFFFF000FFFF00FFFFFFF000FFF000FFFFFFF000FF0000FFFFFF
      F000F0000000000000000000000000000000F000000000000000FF0000FFFFFF
      F000FFF000FFFFFFF000FFFF00FFFFFFF000FFFFF0FFFFFFF000FFFFFFFFFFFF
      F000}
  end
  object cancelButton: TBitBtn
    Left = 8
    Top = 376
    Width = 97
    Height = 33
    Caption = 'Cancel'
    TabOrder = 5
    OnClick = cancelButtonClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
      993337777F777F3377F3393999707333993337F77737333337FF993399933333
      399377F3777FF333377F993339903333399377F33737FF33377F993333707333
      399377F333377FF3377F993333101933399377F333777FFF377F993333000993
      399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
      99333773FF777F777733339993707339933333773FF7FFF77333333999999999
      3333333777333777333333333999993333333333377777333333}
    NumGlyphs = 2
  end
  inline DeleteFrame1: TDeleteFrame
    Left = 8
    Top = 8
    Width = 553
    Height = 361
    TabOrder = 1
    Visible = False
    inherited Panel1: TPanel
      Width = 529
      inherited Label1: TLabel
        Top = 205
        Width = 141
        Caption = 'Select a Videostore  to Delete'
      end
      inherited deleteButton: TButton
        Hint = 'Remove Video Store and all of the information on it.'
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  inline PanelSeries1: TPanelSeries
    Left = 0
    Top = 0
    Width = 553
    Height = 369
    TabOrder = 2
    Visible = False
    inherited fileNamePanel: TPanel
      inherited Label2: TLabel
        Width = 144
        Caption = 'Video Storage Media Directory'
      end
    end
  end
end
