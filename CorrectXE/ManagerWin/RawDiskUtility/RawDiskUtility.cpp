//---------------------------------------------------------------------------

#include <vcl.h>
#include "ReadWriteControl.h"
#pragma hdrstop
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();

                 TReadWriteControlForm *controlform = new TReadWriteControlForm(Application);
                 controlform->Show();
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
