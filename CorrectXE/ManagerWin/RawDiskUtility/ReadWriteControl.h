//---------------------------------------------------------------------------

#ifndef ReadWriteControlH
#define ReadWriteControlH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CreatePanelSeries.h"
#include "introSelectionFrame.h"
#include "DeletePanelSeries.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TReadWriteControlForm : public TForm
{
__published:	// IDE-managed Components
        TIntroFrame *IntroFrame1;
        TDeleteFrame *DeleteFrame1;
        TPanelSeries *PanelSeries1;
        TBitBtn *nextButton;
        TBitBtn *backButton;
        TBitBtn *cancelButton;

        void __fastcall nextButtonClick(TObject *Sender);
        void __fastcall backButtonClick(TObject *Sender);
        void __fastcall cancelButtonClick(TObject *Sender);
private:	// User declarations
        int current_frame;
public:		// User declarations
        __fastcall TReadWriteControlForm(TComponent* Owner);
        void disableEnableNext();
        void disableEnablePrev();
        void collectiveReset();
      //  void resetStuff();
};
//---------------------------------------------------------------------------
extern PACKAGE TReadWriteControlForm *ReadWriteControlForm;
//---------------------------------------------------------------------------
#endif
