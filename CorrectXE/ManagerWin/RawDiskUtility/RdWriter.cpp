

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
//RdWriter class by Kea Johnston, July 27, 2004
//
//
//A class for writing and reading ini file entries for the purpose of configuring
//Rawdisks.
//ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <vcl.h>
#pragma hdrstop

bool GLBL_bStopFlag = false;
int  GLBL_iNumRuns = 0;

#define MEMORY_MALLOC        1
#define MEMORY_VIRTUAL_ALLOC 2
#define MEMORY_GLOBAL_ALLOC  3
#include "RdWriter.h"
//#include "HRTimer.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include "ReadWriteUtilityException.h"
#include "IniFile.h"
#include "DSFileWrapper.h";
#include <fstream>
#include <stdio.h>


//---------------------------------------------------------------------------
#pragma package(smart_init)

    __fastcall RdWriter::RdWriter(){
    }


    __fastcall RdWriter::~RdWriter(){
    }

bool RdWriter::writeDisk(WriteWrapper *wwrap, TProgressBar *rdbar, TLabel *stlabel){
        bool itworked = true;

        LARGE_INTEGER  llFileSizeBytes;
        LARGE_INTEGER  llIndex;
        LARGE_INTEGER  llNumBytesWritten;
        LARGE_INTEGER  llNumChunksToWrite;
        LARGE_INTEGER  llChunksLeftToWrite;
        LARGE_INTEGER  llStartingByte;
        LARGE_INTEGER  llSeekPos;
        LARGE_INTEGER  llBytesToWrite;
        long           lChunkSizeBlocks = 0;
        long           lChunkSizeBytes = 0;
        long           lIndex;
        long           lBytesToWrite;
        double         dStartingByte;
        double         dFileSizeBytes = 0.0;
        double         dMillisecPrev  = 0.0;
        double         dMillisecUsed  = 0.0;
        double         dMillisecShow  = 0.0;
        double         dMBperSec      = 0.0;
        double         dMBperSecMin   = 99999.9;
        double         dMBperSecMax   = 0.0;
        double         dMBperSecSum   = 0.0;
        unsigned long  ulNumBytesWritten;
        HANDLE         hHandle;
        //HRTimer        hrt;
        unsigned char *ucpData = NULL;
        char           caAlpha[] = "abcdefghijklmnopqrstuvwxyz ";
        char           caOutputLine[512];
        char           caFilename[512];
        char           caCaption[512];
        char           caMBs[512];
        char           caDiskname[512];
        char          *cpPosBackSlash;
        int            iPosBackSlash;
        int            iIterations, iIterationsCompleted, i;
        HANDLE         hMem;
        float          fProgressPerc;
        unsigned long  ulSectorsPerCluster, ulBytesPerSector = 0,
        ulNumberOfFreeClusters, ulTotalNumberOfClusters;

        GLBL_bStopFlag = false;


        //This filename is limited to 512 characters. It will crash if we go over. Is this ok?
        //Setting up data to work with this section of code by Kevin M. (Quadpart is a 64 bit integer.)

        //data setup--------------------------------------------------------------------

        StrCopy(caFilename,((char *)wwrap->getRawDiskDirectory().c_str()));
        dFileSizeBytes = (double)wwrap->getRawDiskSize();
        iIterations = wwrap->getNumIterations();
        lChunkSizeBlocks = (long)wwrap->getChunkSize();
        dStartingByte = (double) wwrap->getStartPosition();
        llFileSizeBytes.QuadPart = dFileSizeBytes;
        llSeekPos.QuadPart       = dFileSizeBytes - 1;
        lChunkSizeBytes = lChunkSizeBlocks * 512;
        llStartingByte.QuadPart  = dStartingByte;

        //prelim calculations

        llBytesToWrite.QuadPart  = dFileSizeBytes - llStartingByte.QuadPart;

        llNumChunksToWrite.QuadPart = llBytesToWrite.QuadPart / lChunkSizeBytes;


       //Memory Allocation process------------------------------------------------
       try{
                Application->ProcessMessages();
                stlabel->Caption = "(Preparing your computer...)";

                if (wwrap->getAllocMethod()== MEMORY_MALLOC)
                 {
                        hMem = NULL;
                        ucpData = (unsigned char *) malloc((unsigned int) lChunkSizeBytes);
                 }
                 else if (wwrap->getAllocMethod() == MEMORY_GLOBAL_ALLOC)
                 {
                         hMem = GlobalAlloc(GMEM_FIXED, lChunkSizeBytes);
                         ucpData = (unsigned char *) GlobalLock(hMem);
                 }
                 else /* MEMORY_VIRTUAL_ALLOC */
                 {
                         hMem = NULL;
                         ucpData = (unsigned char *) VirtualAlloc(NULL, lChunkSizeBytes, MEM_COMMIT,
                                             PAGE_READWRITE);
                }
                if (ucpData == NULL)
                {
                       throw ReadWriteUtilityException ("Memory allocation failed.", "Error");
                }

                if((rdbar!= NULL) && (stlabel!=NULL)){
                        stlabel->Caption = "(Preparing to write rawdisk...)";
                        rdbar->Position = 10;
                }
                
        //creates a really really long string of abcd etc. over and over again to take up
        //space, I assume.

                Application->ProcessMessages();
                for (lIndex = 0; lIndex < lChunkSizeBytes; lIndex++)
                {
                        ucpData[lIndex] = caAlpha[lIndex % strlen(caAlpha)];
                }
                if((rdbar!= NULL) && (stlabel!=NULL)){
                        stlabel->Caption  = "(Gathering information...)" ;
                        rdbar->Position=25;
                }


        //error checking---------------------------------------------------------

        //There weren't enough bytes in the sector.
                if (!GetBytesPerSector(caFilename, &ulBytesPerSector))
                {
                        throw ReadWriteUtilityException("No space available.", "Error");
                }

        //check to see if the chunk size is a multiple of the sector size.

                if (lChunkSizeBytes % ulBytesPerSector != 0)
                {
                        sprintf(caOutputLine,
                        "Chunk Size must be a multiple of Sector Size (%lu).  Returning...",
                        ulBytesPerSector);
                        //free up the memory taken for this file.
                        GlobalFree(hMem);
                        throw ReadWriteUtilityException(caOutputLine, "Error");
                }
                if((rdbar!= NULL) && (stlabel!=NULL)){
                        stlabel->Caption = "(Writing Rawdisk file...)";
                        rdbar->Position=30;
                 }

                dMBperSecSum = 0.0;
                iIterationsCompleted = 0;

       //Writing the file-------------------------------------------------------------
       //Make a file where we can write the information in the memory.
                for (i=0; (i < iIterations) && !GLBL_bStopFlag; i++)
                {
                        GLBL_iNumRuns++;
                        Application->ProcessMessages();
                        hHandle = CreateFile(caFilename, GENERIC_WRITE,
                         FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
                         FILE_FLAG_NO_BUFFERING, NULL);

                }
       
                if((rdbar!= NULL)){
                        rdbar->Position += 30;
                }


       //If we can't make a file,  we throw an error and die.
                if (hHandle == INVALID_HANDLE_VALUE)
                {
                        GlobalFree(hMem);
                        throw ReadWriteUtilityException("Error Opening the File!","Error");

                }

    //Points the beginning of the file to the start byte + the beginning of the file.
                llSeekPos.QuadPart       = llStartingByte.QuadPart;

                Application->ProcessMessages();
                SetFilePointerEx(hHandle, llSeekPos, NULL, FILE_BEGIN);

                Application->ProcessMessages();

                dMillisecPrev = 0;
                dMillisecShow = 0;
                dMillisecUsed = 0;

    //Start the timer.
    //hrt.StartTimer();  // This could arguably go before the CreateFile()
                dMBperSecMin = 99999.9;
                dMBperSecMax = 0.0;

                llNumBytesWritten.QuadPart = 0;

   //While the index is < the number of chunks which must be written, and the stop flag
   //is not true,
                for (llIndex.QuadPart = 0;
                (llIndex.QuadPart < llNumChunksToWrite.QuadPart) && !GLBL_bStopFlag;
                llIndex.QuadPart++)
                {
                        llChunksLeftToWrite.QuadPart = llNumChunksToWrite.QuadPart
                                    - (llIndex.QuadPart + 1);


   //Try to write the file to the disk with the values in upcdata. If it fails, it returns
   //zero, and we throw an error.

                        if (WriteFile(hHandle, ucpData, lChunkSizeBytes, &ulNumBytesWritten, NULL) == 0){
                                CloseHandle(hHandle);
                                throw ReadWriteUtilityException("Could not write file.", "Error");


        //TTT do free of mem.
                        }

                        if (ulNumBytesWritten == 0){
                                CloseHandle(hHandle);
                                throw ReadWriteUtilityException("Zero bytes written.", "Error");

                        }
                        if((rdbar!= NULL) && (stlabel!=NULL)){
                                rdbar->Position = 97;
                                stlabel->Caption = "(Finished writing, now closing file...)";
                        }

                        llNumBytesWritten.QuadPart += ulNumBytesWritten;
                        dMillisecPrev = dMillisecUsed;
      //dMillisecUsed = hrt.ReadTimer();
                }

//Close file and finish up.-----------------------------------------------
                Application->ProcessMessages();
                CloseHandle(hHandle);

   //If we stop nicely...........
                if (!GLBL_bStopFlag)
                {
                        Application->ProcessMessages();
                        iIterationsCompleted++;
      //  dMBperSec = (llNumBytesWritten.QuadPart / (1024 * 1024))
                  // (dMillisecUsed / 1000);
     //   dMBperSecSum += dMBperSec;

                if(rdbar!=NULL){
                        rdbar->Position = 100;
                }
        }

    //If we close with a stop flag,
        if (GLBL_bStopFlag){
     //Blows the file to kingdom-come if the user aborts while writing.
                Application->ProcessMessages();
                DeleteFile(caFilename);
                throw ReadWriteUtilityException("Aborted by user.", "Information");
        }

  //Clean up the memory.------------------------------------------------------

        if (wwrap->getAllocMethod() == MEMORY_MALLOC)
        {
                free(ucpData);
        }
        else if (wwrap->getAllocMethod() == MEMORY_GLOBAL_ALLOC)
        {
                 GlobalFree(hMem);
        }
        else /* MEMORY_VIRTUAL_ALLOC */
        {
                VirtualFree(ucpData, 0, MEM_RELEASE);
        }


 //Exceptions come from here (not from Hell).--------------------------

 }
 catch(ReadWriteUtilityException &e){
        MessageBox(NULL,e._message, (e._purpose).c_str(),
        MB_OK|MB_ICONINFORMATION );
        itworked = false;
}

return itworked;
}
//-------------------------------------------------------------------------
bool RdWriter::writeConfig(WriteWrapper *wwrapper){
        bool itworked = true;
        return itworked;
}

    //Swiped from readWrite test.
//--------------------------------------------------------------------------

bool RdWriter::GetBytesPerSector(char *filename, unsigned long *bytesPerSector){

        char *cpPosBackSlash;
        int   iPosBackSlash;
        char  caDiskname[512];
        char *cpDiskname = caDiskname;
        unsigned long ulSectorsPerCluster, ulNumberOfFreeClusters;
        unsigned long ulTotalNumberOfClusters;

  /*
   * Get the Number of Bytes Per Sector for current disk
   */
        try{

        //find \\in the string. Use this to get the raw filename.
        
                cpPosBackSlash = strchr(filename, '\\');
                if (cpPosBackSlash == NULL)
                        iPosBackSlash = 0;
                else
                        iPosBackSlash = cpPosBackSlash - filename;
                if (iPosBackSlash != 0)
                {
                        strcpy(caDiskname, filename);
                        caDiskname[iPosBackSlash+1] = '\0';
                }
                else
                        cpDiskname = NULL;

                if (GetDiskFreeSpace(cpDiskname, &ulSectorsPerCluster, bytesPerSector,
                &ulNumberOfFreeClusters, &ulTotalNumberOfClusters) == 0)
                {
                        throw ReadWriteUtilityException("No Free disk space.", "Error");

                }
                }
        catch (ReadWriteUtilityException e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
         //ShowMessage(e._message);
                return(false);
        }


  return(true); // OK

}
 //----------------------------------------------------------------------------
  void RdWriter::setGlobalStopFlag(bool torf){
        GLBL_bStopFlag = torf;
  }


 //Sets up the freelist file.------------------------------------------------------------



 bool RdWriter::setupFreelist(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb){
        bool worked = true;
        if(lb!=NULL){
                lb->Caption = "Creating Freelist file.";
        }
        FILE *flfile;
        if((flfile = fopen(wr->getFreelistDir().c_str(),"w"))==0){ //there were  errors.
              ShowMessage("Error opening freelist file. Are you trying to write over an open freelist file?");
                worked = false;
              }
        else{
                String outputstr = "Location=0 NumBlock=" + IntToStr(wr->getNoBytes());
                if(fprintf(flfile, outputstr.c_str(),"s")>=0){

                }
                else{
                ShowMessage("Error writing to freelist file. Is it open?");

                }

        }
        fclose(flfile);

 //get the directory for the freelist file and create it.---------------------

 return worked;
 }


 //Sets up the diskscheme.ini-----------------------------------------------
 bool RdWriter::setupDiskScheme(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb){
         bool worked = true;
         if(lb!=NULL){
                lb->Caption = "Setting up diskschemes.ini...";
         }
 //getting filename of the disksceheme file.--------------------------------

        //get the disk scheme key from the localmachine file. If it doesn't
        //exist, create it.
        string diskschemefile;
        try{
				 diskschemefile = getCfgFileLoc("DiskSchemeFile", "$(DRS_NOVA_LOCAL_DIR)DiskSchemes.ini");
                if((pb!=NULL)){
                        pb->Position += 11;
                }                                         
 //Writing active disk scheme section-------------------------------------------
                CIniFile *dsini = CreateIniFile(diskschemefile);
                String dsname = "ds-"+wr->getDiskName();
                if(dsini->SectionExists("ActiveDiskSchemes")){
                        if(!dsini->KeyExists("ActiveDiskSchemes",("ActiveDiskScheme["+(IntToStr(getRawDiskNo()+1))+"]").c_str())){
                                String towrite = "ActiveDiskScheme["+(IntToStr(getRawDiskNo()+1))+"]";
                                dsini->WriteString("ActiveDiskSchemes", towrite.c_str(), dsname.c_str());
                                dsini->SaveIfNecessary();
                              
                        }

                }
                else{
                        String towrite = "ActiveDiskScheme["+(IntToStr(getRawDiskNo()+1))+"]";
                        dsini->WriteString("ActiveDiskSchemes",towrite.c_str(),dsname.c_str());
                        dsini->SaveIfNecessary();
                        }
                        
                if(pb!=NULL){
                        pb->Position += 11;
                }
 //Writing the disk scheme configuration----------------------------------------

                String sectiondesired = "DiskScheme"+(IntToStr(getRawDiskNo()+1));

                        //if there's not already a section for this disk name, make one.
                        //if there's already a section,  leave it alone.
                if(!dsini->SectionExists(sectiondesired.c_str())){
                        
                        dsini->WriteString(sectiondesired.c_str(), "Name", dsname.c_str());
                                //The user is not going to be able to edit this in this part of the program  <<yet>>
                        dsini->WriteString(sectiondesired.c_str(), "MainVideoMediaLocation", (wr->getDiskName()).c_str());
                        if(wr->getAudioPath()!=""){
                                dsini->WriteString(sectiondesired.c_str(),"AudioMediaDirectory", (wr->getAudioPath()).c_str());
                        }
                }
                if(!DeleteIniFile(dsini)){
                        TRACE_0(errout<<"ERROR:Couldn't write to local machine file.");
                        throw ReadWriteUtilityException("Error reading/writing Local Machine ini file.", "ERROR");
                }

                if(pb!=NULL){
                        pb->Position += 11;
                }
        }

       catch(ReadWriteUtilityException &e){
              MessageBox(NULL,e._message, (e._purpose).c_str(),
              MB_OK|MB_ICONINFORMATION );
              worked = false;
       }


        return worked;
        }
 //---------------------------------------------------------------------------
 //This method sets up the rawdisk.cfg settings for the rawdisk that has just been added.
 //It takes a package of information in a wrapper wr and adds it to the ini file.
 //---------------------------------------------------------------------------

 bool RdWriter::setupRawDiskConfig(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb){
        if(lb!=NULL){
                lb->Caption = "Setting up the rawdisk.cfg...";
        }
 //get rawdisk file from the localmachine.ini.
        bool worked = true;

        string rawdiskfile;
        try{
                string defaultName = "$(CPMP_SHARED_DIR)rawdisk.cfg";
                rawdiskfile = getCfgFileLoc("RawDiskCfg",defaultName);
                

                if(rawdiskfile==""){
                        TRACE_0(errout<<"ERROR:No rawdisk file.");
                }

                CIniFile *rdini = CreateIniFile(rawdiskfile);
                String newsection = "RawDisk"+IntToStr(getRawDiskNo()+1);
                if(pb!=NULL){
                        pb->Position +=11;
                }
//Write info from wrapper file to the ini file.--------------------------------------------

                if(!(rdini->SectionExists(newsection.c_str()))){

                        rdini->WriteString(newsection.c_str(), "MediaIdentifier", (wr->getDiskName().c_str()));
                        rdini->WriteString(newsection.c_str(),"RawVideoDirectory", (wr->getFullPath().c_str()));
                        rdini->WriteString(newsection.c_str(), "FreeListFile", (wr->getFreelistDir().c_str()));
                        rdini->WriteString(newsection.c_str(), "RawDiskSize",(IntToStr(wr->getNoBytes())).c_str());
                        rdini->WriteString(newsection.c_str(),"FastIOBoundary",(IntToStr(wr->getFastio())).c_str());
                        if(pb!=NULL){
                                pb->Position +=11;
                        }
                }
                else{
                        TRACE_3(errout<<"Uhhh. The section existed already.");
                         //If the section is already there, don't do anything.
                }
                if(!DeleteIniFile(rdini)){
                        TRACE_0(errout<<"ERROR:Couldn't write to Rawdisk.cfg");
                        throw ReadWriteUtilityException("Error reading/writing to rawdisk.cfg", "ERROR");
                }
                if(pb!=NULL){
                        pb->Position +=11;
                }
        }
        catch(ReadWriteUtilityException &e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
                worked = false;
        }
return worked;
 }

 //-----------------------------------------------------------------------------
 //Method to retreive information about a rawdisk from the rawdisk.cfg and
 //diskscheme.ini files with the mediaidentifier "rdname"
 // and send it back to the caller in the
 //form of a write wrapper.
 //-----------------------------------------------------------------------------

 WriteWrapper* RdWriter::retreiveDSInfo(String *rdname){
 //setup storage variables. ----------------------------------------------------

        String name = "None";
        String rdpath= "None";
        String flpath = "None";
        __int64 disksize = 0;
        String vidpath = "None";
        String audpath = "None";
        //WriteWrapper *wrap = NULL;

 //open up the disk scheme file to get the name, vid location, and aud. directory
         string diskschemefile;
         string rawdiskconfig;


        rawdiskconfig = getCfgFileLoc("RawDiskCfg","$(CPMP_SHARED_DIR)rawdisk.cfg");
        diskschemefile = getCfgFileLoc("DiskSchemeFile","$(DRS_NOVA_LOCAL_DIR)DiskSchemes.ini");

              


                         string dsrefname = ("ds-" + *rdname).c_str();
                         string secname = searchSections(diskschemefile,"Name",dsrefname);
                         CIniFile *diskini = CreateIniFile(diskschemefile);
                        if(diskini!= NULL){


                                if(diskini->SectionExists(secname)){
                                        if(diskini->KeyExists(secname,"AudioMediaDirectory")){
                                                audpath = String(diskini->ReadString(secname, "AudioMediaDirectory", "None").c_str());

                                                }
                                        if(diskini->KeyExists(secname,"MainVideoMediaLocation")){
                                                vidpath = String(diskini->ReadString(secname,"MainVideoMediaLocation","None").c_str());
                                                }
                                        }
                                if(!DeleteIniFile(diskini)){
                                         throw ReadWriteUtilityException("error reading diskscheme ini file.", "ERROR");
                                }
                                }
                                else{
                                        throw ReadWriteUtilityException("Error opening diskscheme ini file.", "ERROR");
                                        }

        //Get info from the rawdisk.cfg file.


                string sec = searchSections(rawdiskconfig, "MediaIdentifier",rdname->c_str());
                CIniFile *rdc = CreateIniFile(rawdiskconfig);
                if(rdc!=NULL){

                        if(rdc->SectionExists(sec)){
                                if(rdc->KeyExists(sec, "MediaIdentifier")){
                                        name = String(rdc->ReadString(sec,"MediaIdentifier","NONE").c_str());
                                        }
                                if(rdc->KeyExists(sec,"RawVideoDirectory")){
                                        rdpath = String(rdc->ReadString(sec,"RawVideoDirectory","NONE").c_str());
                                        }
                                if(rdc->KeyExists(sec,"FreeListFile")){
                                        flpath = String(rdc->ReadString(sec,"FreeListFile","NONE").c_str());
                                        }
                                if(rdc->KeyExists(sec,"RawDiskSize")){
                                        disksize = StrToInt64(String(rdc->ReadString(sec,"RawDiskSize","NONE").c_str()));
                                        }
                                }

                        if(!DeleteIniFile(rdc)){
                                TRACE_0(errout<<"ERROR: Error closing rawdisk.cfg");
                                }
                        }


                        WriteWrapper *wrap = new WriteWrapper(rdpath,0,disksize,0,name,0,0,audpath,vidpath, flpath);
 return wrap;
 }
//------------------------------------------------------------------------------
//This method gets a list of the rawdisks in existance from the rawdisk cfg file and returns a tstringlist.
//mainly, this is for populating gui combo boxes, but it can be used for other stuff.
//------------------------------------------------------------------------------

 TStringList* RdWriter::getRawDisks(){

        TStringList *stlist = new TStringList();
        try{
                string rawdiskcfg =getCfgFileLoc("RawDiskCfg","$(CPMP_SHARED_DIR)rawdisk.cfg");
                 StringList storesec;
                CIniFile *rdcfg = CreateIniFile(rawdiskcfg);
                if(rdcfg!=NULL){
                        storesec.clear();
                        rdcfg->ReadSections(&storesec);
                        for(unsigned i=0;i<storesec.size();i++){
                                if (rdcfg->KeyExists(storesec[i], "MediaIdentifier")){
                                stlist->Add(String(rdcfg->ReadString(storesec[i], "MediaIdentifier","").c_str()));
                                }
                        }
                }

                if (!DeleteIniFile(rdcfg)){
                        throw ReadWriteUtilityException("Error opening rawdisk.cfg.", "ERROR");
                }
           }
        catch(ReadWriteUtilityException &e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
        }
 return stlist;
 }

 //-----------------------------------------------------------------------------
 //Kind of an inefficient method: It gets the rawdisk numbers from the rawdisk.cfg
 //file and and returns the highest listed rawdisk number from the section titles.
 //This method prevents us from having to read through the sections after a deletion and
 //subtract one from each of the rawdisk section names, ie [rawdisk1] --> [rawdisk0]
 //when a rawdisk below it is removed.
 //-----------------------------------------------------------------------------

 int RdWriter::getRawDiskNo(){
      int rawcount = -1;

      try{
                string rawdiskcfg = getCfgFileLoc("RawDiskCfg","$(CPMP_SHARED_DIR)rawdisk.cfg");
                StringList sec;
                CIniFile *rdcfg = CreateIniFile(rawdiskcfg);
                if(rdcfg!=NULL){
                        sec.clear();
                        rdcfg->ReadSections(&sec);
                        for(unsigned i=0; i<sec.size();i++){
                                if(rdcfg->KeyExists(sec[i], "MediaIdentifier")){
                                        int lngth = sec[i].length();
                                        char lastcharacter = (sec[i])[lngth-1];
                                     //TRACE_0(errout<<lngth + " secondtolast " + sec[i] + lastcharacter);
                                        if(lastcharacter-48 > rawcount){
                                                //converts the char to an int.
                                                rawcount = lastcharacter-48;
                                                //TRACE_0(errout<<rawcount);

                                        }
                                }
                        }
                }
                if(!DeleteIniFile(rdcfg)){
                        throw ReadWriteUtilityException("Error opening rawdisk.cfg.","ERROR");
                }
      }
      catch(ReadWriteUtilityException &e){
                TRACE_0(errout<< "ERROR: Trouble opening rawdisk.cfg.");
      }
 return rawcount;
 }
 //---------------------------------------------------------------------------
 //Method for removing a rawdisk from the disk scheme ini.
 //--------------------------------------------------------------------------
 bool RdWriter::delDSConfig(String diskname){
        string dscfg;

 //Open localmachine to get the name of the disk scheme file.-----------------
        bool itworks = true;
        StringList seclist;
        string secname = "";
        dscfg =getCfgFileLoc("DiskSchemeFile","$(CPMP_LOCAL_DIR)DiskSchemes.ini");


                //Look for the name of the disk.--------------------------------
        try{
                string dsname = ("ds-" + diskname).c_str();
                secname = searchSections(dscfg, "Name", dsname);
                CIniFile *rini = CreateIniFile(dscfg);
                //erase the rawdisk entries in this file.-------------------------

                if (rini->SectionExists(secname)){
                        rini->EraseSection(secname);
                }
                if(rini->SectionExists("ActiveDiskSchemes")){
                        int l = String(secname.c_str()).Length()-1;
                        String secl = String(secname[l]);
                        String aidentname = String(("ActiveDiskScheme["+secl +"]").c_str());
                        rini->DeleteKey("ActiveDiskSchemes",aidentname.c_str());
                }

                //close rawdisk cfg.--------------------------------------------

                if(!DeleteIniFile(rini)){
                        throw ReadWriteUtilityException("Error closing rawdisks.cfg.", "ERROR");
                }
        }

        catch(ReadWriteUtilityException &e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
                itworks = false;
        }

 return itworks;

 }
 //---------------------------------------------------------------------------
 //Method for removing a rawdisk from rawdisk config..
 //----------------------------------------------------------------------------
 bool RdWriter::delRDConfig(String diskname){
 string rdcfg;
 string sec;
 bool works = true;
 rdcfg = getCfgFileLoc("RawDiskCfg","$(CPMP_SHARED_DIR)rawdisk.cfg");
        try{
                sec = searchSections(rdcfg,"MediaIdentifier", diskname.c_str());
                CIniFile *rini = CreateIniFile(rdcfg);
                if(rini!=NULL){
                        if (rini->SectionExists(sec)){
                                  rini->EraseSection(sec);
                                  }

                        if(!DeleteIniFile(rini)){
                                throw ReadWriteUtilityException("Error closing disk scheme ini.", "ERROR");
                        }
                }
                else{
                        throw ReadWriteUtilityException("Error opening diskscheme.ini", "ERROR");
                }
        }
        catch(ReadWriteUtilityException &e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
                works = false;
        }

 return works;
 }

 //----------------------------------------------------------------------
 //This method deletes the rawdisk file and its freelist file.
 //----------------------------------------------------------------------
 //Get the name of the rawdisk config. PLEASE CALL THIS BEFORE YOU DELETE THE CFG ENTRIES!!!!
 //OTHERWISE, the rawdisk file won't get del'ed.
 bool RdWriter::delRawDisk(String diskname){
       
        string rdcfg;
        string filename;
        string flpath;
        string secname;
        bool works = true;
        rdcfg = getCfgFileLoc("RawDiskCfg","$(CPMP_SHARED_DIR)rawdisk.cfg");
        try{
                if(DoesFileExist(rdcfg)){
                        secname = searchSections(rdcfg, "MediaIdentifier", diskname.c_str());
                        
                        CIniFile *rd = CreateIniFile(rdcfg);
                        if(rd!=NULL){
                                 if(rd->SectionExists(secname)){
                                        filename = rd->ReadString(secname,"RawVideoDirectory","default");
                                        flpath = rd->ReadString(secname,"FreeListFile", "default");
                                 }
                                if(!DeleteIniFile(rd)){
                                       throw ReadWriteUtilityException("Error closing rawdisk.cfg.", "ERROR");
                                }
                        }
                        else{
                                TRACE_0(errout<<"ERROR:Error opening"<<rdcfg);
                                throw ReadWriteUtilityException("Error opening rawdisk.cfg.", "ERROR");

                        }

 //Try to delete the rawdisk file.----------------------------------------------
                        if(filename!=""){
                                TRACE_0(errout<<"ERROR" + filename + "error opening the preceding file.");
                                if(!DeleteFile(String(filename.c_str()))){
                                        throw ReadWriteUtilityException("Error deleting rawdisk file. Is this file open or read-only?", "ERROR");
                                }
                         }
 //Try to delete the freelist file.---------------------------------------------
                        if(flpath!=""){

                                if(!DeleteFile(String(flpath.c_str()))){
                                        throw ReadWriteUtilityException("Error deleting freelist file. Is this file open or read-only?", "ERROR");
                                }
                        }
                }

                else{
                        throw ReadWriteUtilityException("You've tried to delete a file which doesn't exist. MTI Rawdisk utility will delete the entries for this file.", "INFORMATION");
                }
 }
 //Catch all errors I have generated.-------------------------------------------
         catch(ReadWriteUtilityException &e){
                MessageBox(NULL,e._message, (e._purpose).c_str(),
                MB_OK|MB_ICONINFORMATION );
                works = false;
         }
 return works;
}


//------------------------------------------------------------------------------
//Maintenance method looks up a filename in localmachine.ini and returns it.
//lookfor is the ident. It should be set to either "RawDiskCfg" or "DiskSchemeFile"
//This method is mainly to save a lot of code repetition. Default should be
//$(CPMP_SHARED_DIR) or  $(DRS_NOVA_LOCAL_DIR)filename
//------------------------------------------------------------------------------
string RdWriter::getCfgFileLoc(string lookfor, string deflt){
CIniFile *lmini =  CPMPOpenLocalMachineIniFile();
string rdcfg;
try{
                if(lmini!=0){

                        rdcfg = lmini->ReadFileNameCreate("MediaStorage",lookfor, deflt);
                     

                         if(!DeleteIniFile(lmini)){

                              throw ReadWriteUtilityException("Error reading/writing Local Machine ini file.", "ERROR");
                        }
                        if(rdcfg==""){
                               throw ReadWriteUtilityException("no disk scheme file or no rawdisk.cfg", "ERROR");
                        }
                }
                else{
                        throw ReadWriteUtilityException("Can't open localmachine.cfg.", "ERROR");
                }
        }
        catch (ReadWriteUtilityException &e){
                TRACE_0(errout<<(e._purpose).c_str()<<":"<<e._message);
        }
return rdcfg;
}
//------------------------------------------------------------------------------
//Utility method searches sections for a section and value and returns the section that
//the value is located in.
// First parameter = filename to look in. Second Parameter = Identity to search
//sections for. Third parameter = value that the identity should have.
//------------------------------------------------------------------------------
string  RdWriter::searchSections(string filename, string id, string val){
        string secname = "NONE";
        try{
                CIniFile *fle = CreateIniFile(filename);
                StringList sl;
         //find the section we should be reading from. ---------------------------
                if(fle!=NULL){
                        sl.clear();
                        fle->ReadSections(&sl);
                        for(unsigned i = 0;i<sl.size();i++){
                                if(fle->KeyExists(sl[i], id)){
                                        if(fle->ReadString(sl[i],id,"Default")==(val).c_str()){
                                                secname = sl[i];
                                              
                                        }
                                }
                        }
                        if(!DeleteIniFile(fle)){
                                throw ReadWriteUtilityException(("Error closing ini file" + String(filename.c_str())).c_str(), String("ERROR"));
                        }
                }
                else{
                        throw ReadWriteUtilityException(("Error opening ini file " + String(filename.c_str())).c_str(), String("ERROR"));
                }
                if(secname == "NONE"){
                        throw ReadWriteUtilityException("Search returned null. Invalid Ident for Key?", "NOTE");
                }
        }
        catch (ReadWriteUtilityException &e){
                TRACE_0(errout<<(e._purpose).c_str()<<":"<<e._message);
        }
 return secname;
}


