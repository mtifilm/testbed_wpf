//---------------------------------------------------------------------------


#ifndef DeletePanelSeriesH
#define DeletePanelSeriesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TDeleteFrame : public TFrame
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TComboBox *deleteComboBox;
        TLabel *Label1;
        TProgressBar *deleteProgressBar;
        TButton *deleteButton;
        TGroupBox *GroupBox1;
        TLabel *Label2;
        TLabel *percentLabel;
        TLabel *statusLabel;
        TLabel *Label3;
        void __fastcall deleteButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        bool _finished;
        void resetEverything();
        __fastcall TDeleteFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDeleteFrame *DeleteFrame;
//---------------------------------------------------------------------------
#endif
