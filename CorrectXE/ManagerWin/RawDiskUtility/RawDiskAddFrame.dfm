object rawDiskProperties: TrawDiskProperties
  Left = 0
  Top = 0
  Width = 372
  Height = 324
  TabOrder = 0
  object Label1: TLabel
    Left = 24
    Top = 19
    Width = 92
    Height = 13
    Caption = 'Available Rawdisks'
  end
  object rawDiskCombo: TComboBox
    Left = 136
    Top = 16
    Width = 193
    Height = 21
    ItemHeight = 13
    ItemIndex = 1
    TabOrder = 0
    Text = 'Stuff'
    OnChange = rawDiskComboChange
    Items.Strings = (
      'Stuff'
      'Stuff'
      'Stuff'
      'Stuff')
  end
  object Panel1: TPanel
    Left = -13
    Top = 43
    Width = 385
    Height = 281
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Name'
    end
    object Label3: TLabel
      Left = 16
      Top = 56
      Width = 54
      Height = 13
      Caption = 'Size (bytes)'
    end
    object Label4: TLabel
      Left = 16
      Top = 104
      Width = 71
      Height = 13
      Caption = 'Audio Location'
    end
    object Label5: TLabel
      Left = 16
      Top = 152
      Width = 77
      Height = 13
      Caption = 'Freelist Location'
    end
    object Label6: TLabel
      Left = 16
      Top = 200
      Width = 71
      Height = 13
      Caption = 'Video Location'
    end
    object diskNameEdit: TEdit
      Left = 16
      Top = 24
      Width = 201
      Height = 21
      TabOrder = 0
    end
    object sizeEdit: TEdit
      Left = 16
      Top = 72
      Width = 201
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object audioEdit: TEdit
      Left = 16
      Top = 120
      Width = 201
      Height = 21
      TabOrder = 2
    end
    object flPathEdit: TEdit
      Left = 16
      Top = 168
      Width = 201
      Height = 21
      Enabled = False
      TabOrder = 3
    end
    object rdButton: TButton
      Left = 16
      Top = 248
      Width = 121
      Height = 25
      Caption = 'Make New Rawdisk'
      TabOrder = 4
      OnClick = rdButtonClick
    end
    object browseButton: TButton
      Left = 232
      Top = 120
      Width = 75
      Height = 22
      Caption = 'Browse...'
      TabOrder = 5
      OnClick = browseButtonClick
    end
    object vidDirEdit: TEdit
      Left = 16
      Top = 216
      Width = 201
      Height = 21
      TabOrder = 6
    end
  end
end
