//---------------------------------------------------------------------------

#ifndef RdWriterH
#define RdWriterH
#include "WriteWrapper.h"
#include "DSFileWrapper.h"
#include "IniFile.h"
#include <string>
#include <iostream>
#include <vector>


using std::string;
using std::ostream;

#include "machine.h"

//---------------------------------------------------------------------------
class CIniFile;
class RdWriter{
private:
        string getCfgFileLoc(string lookfor, string deflt);
        string searchSections(string filename, string id, string val);
public:

        __fastcall RdWriter();
        __fastcall ~RdWriter();
        bool writeDisk(WriteWrapper *wwrap, TProgressBar *rdbar, TLabel *stlabel);
        bool writeConfig(WriteWrapper *wwrapper);
        bool GetBytesPerSector(char *filename, unsigned long *bytesPerSector);
        void setGlobalStopFlag(bool torf);
        bool setupFreelist(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb);
        bool setupRawDiskConfig(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb);
        bool setupDiskScheme(DSFileWrapper *wr, TProgressBar *pb, TLabel *lb);
        bool delDSConfig(String diskname);
        bool delRDConfig(String diskname);
        WriteWrapper* retreiveDSInfo(String *rdname);
        TStringList* getRawDisks();
        int getRawDiskNo();
        bool delRawDisk(String diskname);


        //int setupDiskScheme(DiskSchemeWrapper *dswrapper);
};
#endif
