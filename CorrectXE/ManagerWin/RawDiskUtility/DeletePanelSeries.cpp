//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "RdWriter.h"
#include "WriteWrapper.h"

#include "DeletePanelSeries.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDeleteFrame *DeleteFrame;
RdWriter *writer;
//---------------------------------------------------------------------------
__fastcall TDeleteFrame::TDeleteFrame(TComponent* Owner)
        : TFrame(Owner)
{
        _finished =false;
        writer = new RdWriter();
        deleteComboBox->Items = writer->getRawDisks();


}
//---------------------------------------------------------------------------
void __fastcall TDeleteFrame::deleteButtonClick(TObject *Sender)
{
        deleteButton->Enabled = false;
        statusLabel->Caption = "Initializing...";
        _finished = false;
        String todel = deleteComboBox->Text;
        statusLabel->Caption = "Deleting Video Storage Media...";
        writer->delRawDisk(todel);
        percentLabel->Caption = "33%";
        deleteProgressBar->Position = 33;
        statusLabel->Caption = "Configuring rawdisks...";
        writer->delRDConfig(todel);
        percentLabel->Caption = "66%";
        deleteProgressBar->Position = 66;
        statusLabel->Caption = "Configuring MTI Programs...";
        writer->delDSConfig(todel);
        percentLabel->Caption = "99%";
        deleteProgressBar->Position = 99;
        statusLabel->Caption = "Finished.";
        deleteProgressBar->Position = 100;
        percentLabel->Caption = "100%";
        int indtodel = deleteComboBox->Items->IndexOf(todel);
        deleteComboBox->Items->Delete(indtodel);

        _finished = true;
        deleteButton->Enabled = true;
}

void TDeleteFrame::resetEverything(){
        percentLabel->Caption = "0%";
        statusLabel->Caption = "";
        deleteProgressBar->Position = 0;
        deleteComboBox->Items = writer->getRawDisks();
        deleteComboBox->Text = "";
}
//---------------------------------------------------------------------------
