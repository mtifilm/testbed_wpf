//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RawDiskAddFrame.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"


//---------------------------------------------------------------------------
#include "ParamPanelForm.h"
#include "ReadWriteControl.h"
#include "DSFileWrapper.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#pragma link "ParamPanelForm"
TrawDiskProperties *rawDiskProperties;
TReadWriteControlForm *controlform;
RdWriter *writer;
//---------------------------------------------------------------------------
__fastcall TrawDiskProperties::TrawDiskProperties(TComponent* Owner)
        : TParamPanel(Owner)
{
   selectedDisk = "rawDiskCombo";
     

        String searchstring = String("rd1");
        Caption = "Rawdisks";
        EnableDetails(false);
        writer = new RdWriter();

        controlform = new TReadWriteControlForm(Owner);
        TRACE_0(errout<<"Created a new window for the readwritecontrolform, hopefully.");
        controlform->Hide();

        //setup combobox
        rawDiskCombo->Items = writer->getRawDisks();
}
//---------------------------------------------------------------------------
  void TrawDiskProperties::DetailsChanged(bool Value){

}

void __fastcall TrawDiskProperties::SetAdminPriv(bool Value){
         TParamPanel::SetAdminPriv(Value);

 }
void __fastcall TrawDiskProperties::browseButtonClick(TObject *Sender)
{
 //       
}
//---------------------------------------------------------------------------
void __fastcall TrawDiskProperties::rdButtonClick(TObject *Sender)
{
//
}
//---------------------------------------------------------------------------
void __fastcall TrawDiskProperties::rawDiskComboChange(TObject *Sender)
{
  selectedDisk = rawDiskCombo->Text;
        WriteWrapper *wrap = writer->retreiveDSInfo(&selectedDisk);
        audioEdit->Text = wrap->getAudioPath();
        vidDirEdit->Text = wrap->getMainVidDir();
        delete wrap;
        wrap = 0;
}
//---------------------------------------------------------------------------
