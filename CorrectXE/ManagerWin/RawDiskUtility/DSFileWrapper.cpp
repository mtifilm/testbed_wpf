//---------------------------------------------------------------------------

//Kea Johnston, July 14, 2004:
//This is a wrapper class for the information that needs to be written to the disk_scheme config, freelist, and rawdisk.cfg.
//You can use the RdWriter Configuration methods with any gui as long as you pass the writer one
//of these classes. It is also possible to pass in null values for the audiopath. FastIO Works best
//with a 1 passed in. Don't pass anything else in if you want things to work!

//---------------------------------------------------------------------------
#pragma hdrstop

#include "DSFileWrapper.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
__fastcall DSFileWrapper::DSFileWrapper( String name, __int64 nobytes, String fullpath, String freelistdir, int fastio, String audiodirpath){
        disk_name = name;
        num_bytes = nobytes;
        disk_path = fullpath;
        fl_path = freelistdir;
        fast_io = fastio;
        audio_path = audiodirpath;
       
        }

__int64 DSFileWrapper::getNoBytes(){
        return num_bytes;
        }
String DSFileWrapper::getDiskName(){
        return disk_name;
        }
String DSFileWrapper::getFullPath(){
        return disk_path;
        }
String DSFileWrapper::getFreelistDir(){
        return fl_path;
        }
int DSFileWrapper::getFastio(){
        return fast_io;
        }
String DSFileWrapper::getAudioPath(){
        return audio_path;
        }

