//---------------------------------------------------------------------------


#ifndef introSelectionFrameH
#define introSelectionFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------

const int CREATE_CLICKED = 0;
const int EDIT_CLICKED = 1;
const int DELETE_CLICKED = 2;
const int SPEED_CLICKED = 3;
class TIntroFrame : public TFrame
{
__published:	// IDE-managed Components
        TRadioButton *deleteRadio;
        TPanel *Panel1;
        TLabel *Label1;
        TRadioButton *createRadio;
        TGroupBox *GroupBox1;
        TImage *Image1;
        void __fastcall createRadioClick(TObject *Sender);
    
        void __fastcall deleteRadioClick(TObject *Sender);
  
private:	// User declarations
public:		// User declarations
        __fastcall TIntroFrame(TComponent* Owner);
        int radio_clicked;
};
//---------------------------------------------------------------------------
extern PACKAGE TIntroFrame *IntroFrame;
//---------------------------------------------------------------------------
#endif
