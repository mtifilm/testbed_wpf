//---------------------------------------------------------------------------
//Gui for the RawDisk utility by Kea Johnston, July 6, 2004.
//---------------------------------------------------------------------------


#include <vcl.h>
#pragma hdrstop
#include <list.h>
#include "ReadWriteGui.h"
#include "WriteWrapper.h"
#include "RdWriter.h"
#include "DSFileWrapper.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TrawDiskIntroForm *rawDiskIntroForm;

//I'm still learning, so if you want to change this, you need to change it here and
//also in the array size declaration. this is just so the program can keep track.

const int PANEL_ARRAY_SIZE = 4;

//Audio options corresponding with the radio buttons.
const int AUDIO_NONE = 0;
const int AUDIO_RAWDISK = 1;
const int AUDIO_FILE = 2;

const int START_CREATE = 5;
const int START_DELETE = 6;
const int START_EDIT = 7;
const int START_SPEED = 8;

//---------------------------------------------------------------------------
__fastcall TrawDiskIntroForm::TrawDiskIntroForm(TComponent* Owner)
        : TForm(Owner)
{
        writing_todisk = false;
        panelindex = 0;
         panellist[0] = introPanel;
         panellist[1] = fileNamePanel;
         panellist[2] = diskSizePanel;
         panellist[3] = writePanel;
        //Make an array of the panels so we can navigate through them.
        currentpanel = panellist[0];
        currentpanel->Visible = true;

        nextButton->Enabled = true;
        backButton->Enabled = false;
        chunk_size = 15625;
        disk_spacefree = -1;
        current_size = -1;
        audio_opt = 0;
        audio_dir = "";
        rd_writer = new RdWriter();

        //draws a little line to indicate where disk gets slow and full.
        TCanvas *canvas = DiskWarningLine->Canvas ;
        canvas->Brush->Color=clLime;
        canvas->Rectangle(0,0,(DiskWarningLine->Width*80/100),DiskWarningLine->Height);
        canvas->Brush->Color = clYellow;
        canvas->Rectangle((DiskWarningLine->Width*80/100),0, (DiskWarningLine->Width*95/100),DiskWarningLine->Height);
        canvas->Brush->Color = clRed;
        canvas->Rectangle((DiskWarningLine->Width*95/100),0,DiskWarningLine->Width,DiskWarningLine->Height);



}
//---------------------------------------------------------------------------
//destructor
__fastcall TrawDiskIntroForm::~TrawDiskIntroForm(void){
  delete rd_writer;
  rd_writer = 0;
}
//---------------------------------------------------------------------------

void __fastcall TrawDiskIntroForm::nextButtonClick(TObject *Sender)
{
bool cangoforward = false;
if(panelindex == 0){
        cangoforward = true;
        }
//if info isn't filled out, we can't proceed.
else if(panelindex==1){
        cangoforward  = false;

        if (refreshDirectory()){
                 int diskdriveletter = int(toupper(rawdiskdir[1]));
                 //convert it to the int that the diskspace method wants....(A is 65, drive A: = 1)
                 diskdriveletter = diskdriveletter - 64;
                 disk_spacefree=DiskFree(diskdriveletter);
                 //Don't go to the next screen if the drive letter is invalid.
                 if(disk_spacefree!=-1){
                        cangoforward = true;
                 }
                else{
                        ShowMessage("Invalid drive letter or directory specified.");
                        }
        }
   calculateMaxDiskSize();
 }
else if (panelindex  == 2){

//gets the audio button values from the previous panel.
      cangoforward = false;
      if (noAudioRadio->Checked){
                audio_dir = "";
                }
      else if (audioOnRawdiskRadio->Checked){
                audio_dir = disk_name;
                }
      else{
                audio_dir = audioDirectoryEdit->Text;
                }
      

        pathLabel->Caption = "Path: " + rawdiskdir;
        sizeLabel->Caption = "Size: " + formatIntegerString(current_size) + " bytes";
        nameLabel->Caption = "Name: " + disk_name;
        nextButton->Enabled = false;
        cangoforward = true;

        }

      if(cangoforward == true){
        if (panelindex < PANEL_ARRAY_SIZE){
                 
                panelindex ++;

                currentpanel->Visible = false;
                currentpanel =  panellist[panelindex];
                currentpanel->Visible = true;
         }
         else{
                nextButton->Enabled = false;
        }

 }
       //Assuming that we just jumped forward, the back button should now be enabled.
        backButton->Enabled = true;
}
 //-------------------------------------------------------------------------------
 //Increments progress bar by specified ammount.
 //-------------------------------------------------------------------------------

 void TrawDiskIntroForm::incrementProgressBar(int toIncrement){
 if((toIncrement < rawDiskProgressBar->Max)&&(toIncrement > rawDiskProgressBar->Min)){
        rawDiskProgressBar->Position = toIncrement;
        }
 }
//--------------------------------------------------------------------------------
//Calculates the max size of the rawdisk and sets up the slider.
//--------------------------------------------------------------------------------

void TrawDiskIntroForm::calculateMaxDiskSize(){
        //95% of the disk space is safe no matter what the disk setup.
        __int64 maxdisksize = (disk_spacefree * 95)/100;
        __int64 minsize = 1;
          try{
                if ((chunk_size < disk_spacefree)&& (chunk_size > 0)){
                        minsize = chunk_size * 512;
                        __int64 divdisk = ((disk_spacefree*95)/100)/(minsize);
                        maxdisksize = (minsize) * (divdisk);
                        max_size = maxdisksize;
                        diskSizeTrackbar->Max = divdisk;
                        maxLabel->Caption = formatIntegerString(maxdisksize) + " bytes";

                        diskSizeTrackbar->Min = 1;
                        minLabel->Caption = formatIntegerString(minsize)+ " bytes";

                        current_size =  minsize;
                        }
               }
               //This error should be modified should we ever want to enter the chunk size manually.
               catch(Exception &e){
                        ShowMessage("Invalid Chunk Size.");
                        }
}

//--------------------------------------------------------------------------------
//This method is purely cosmetic. It pust commas in the numbers on the numberline labels.

      String TrawDiskIntroForm::formatIntegerString(__int64 i){
        String str = IntToStr(i);

        int length = str.Length();
        int divl = length/3;
        int modl = length % 3;
        int lowercount = 0;
        String comma = ",";
        String stringbuffr = "";
        String modstr = str.SubString(lowercount,lowercount+modl);
        if(modl>0){
                str = str.SubString(lowercount+modl+1, length);
                stringbuffr += modstr + comma;
                }
        length = str.Length();
        for(int i = 0;i<divl;i++){
                for(int k = 0;k<3;k++){
                        String newstr = str.SubString(3*i+k,1);
                        stringbuffr +=newstr;
                 }

                if(3*(i +1) < length){
                         stringbuffr+=comma;
                         }
       }



        return stringbuffr;

      }
//----------------------------------------------------------------------------
void __fastcall TrawDiskIntroForm::backButtonClick(TObject *Sender)
{
if (panelindex >0 ){
        panelindex --;
        currentpanel->Visible = false;
        currentpanel = panellist[panelindex];
        currentpanel->Visible = true;
        }
if(panelindex == 0){
        backButton->Enabled = false;
        }
        //assuming we just jumped back, we know there's something to go forward to.
        nextButton->Enabled = true;
}
//-----------------------------------------------------------------------------

void __fastcall TrawDiskIntroForm::browseButtonClick(TObject *Sender)
{
        if(rawDiskDialog->Execute()){
               diskPathEdit->Text = rawDiskDialog->FileName;
                }
}
//---------------------------------------------------------------------------
//-------------Method which gets values ffor the rawdisk location and--------
//-----------Tosses mad errors if someone entered nothing or letters. :)-----
//---------------------------------------------------------------------------

bool TrawDiskIntroForm::refreshDirectory()
{
        bool breturn = true;
        if((diskPathEdit->Text!="")&&(numDisksEdit->Text!="")&&(diskNameEdit->Text!="")){
                rawdiskdir = diskPathEdit->Text;

                disk_name = diskNameEdit->Text;
                }
        else{
                ShowMessage("You must fill in all fields.");
                 breturn  = false;
                }

       //try to get the value of the number of existing rawdisks. catches if someone entered nothing or entered
       //letters or symbols.

        try{

                numdisks = StrToInt(numDisksEdit->Text);
        
        }
        catch(EConvertError &e){
                if ((numDisksEdit->Text!=NULL)&&(numDisksEdit->Text!="")){
                        ShowMessage("You must enter an integer value.");
                      }
                      numdisks = 0;
                      breturn = false;
        }
        return breturn;

}

//------------------Calculates the size of the raw disk based on the-----
//------------------position of the trackbar.----------------------------


void __fastcall TrawDiskIntroForm::dragBar(TObject *Sender)
{       __int64 X = diskSizeTrackbar->Position;
        __int64 percentfree = (disk_spacefree * X)/diskSizeTrackbar->Width;
        if (chunk_size >0){
                __int64 newloc = ((chunk_size*512)*X);
                rawDiskSizeLabel->Caption = formatIntegerString(newloc) + "("+(IntToStr((current_size*100)/max_size))+"%"+")";
                current_size = newloc;
                }
}
//---------------------------------------------------------------------------

void __fastcall TrawDiskIntroForm::enterChunkSize(TObject *Sender)
//If we wanted to get the chunk size from the user, we would assign this method as a callback and use it to
//set the chunk_size variable to something from a text box.
{
                        calculateMaxDiskSize();
}
//---------------------------------------------------------------------------

void __fastcall TrawDiskIntroForm::makeRawDiskButtonClick(TObject *Sender)
{

        WriteWrapper *wr = new WriteWrapper(rawdiskdir,  numdisks, chunk_size, current_size, 1, disk_name,3,0);
        makeRawDiskButton->Enabled = false;
        backButton->Enabled = false;
        writing_todisk = true;

        //For the sake of modularity, you can pass nulls into this method too. All it needs is the wrapper.
        rd_writer->writeDisk(wr, rawDiskProgressBar, statusLabel);
        rawDiskProgressBar->Position = 0;
        statusLabel->Caption = " ";
        prepConfigure();
        writing_todisk = false;
        if(MessageBox(NULL,"Raw Disk has been created.", "Finished.",
              MB_OK|MB_ICONINFORMATION )==MB_OK){
              //go to the next screen


              }


        }

//---------------------------------------------------------------------------
    //Quits the program if it is not writing to the disk. Otherwise, it aborts writing but doesn't 1562quit.

void __fastcall TrawDiskIntroForm::cancelButtonClick(TObject *Sender)
{
        if(writing_todisk){
                rd_writer->setGlobalStopFlag(true);
        }
        else{
              if(MessageBox(NULL,"Are you sure you want to cancel rawdisk creation?","Abort Rawdisk Utility?",
               MB_YESNO|MB_ICONEXCLAMATION )==IDYES){
                        Application->Terminate();
              }

        }
}
//---------------------------------------------------------------------------


void __fastcall TrawDiskIntroForm::numDisksEditChange(TObject *Sender)
{
        diskNameEdit->Text = "rd"+ numDisksEdit->Text;

}
//Sets up the configuration files.----------------------------------------------
 void TrawDiskIntroForm::prepConfigure(){

        //Method manages ini file writing when a new disk is created.
        //It makes a wrapper and passes it to the rdwriter which writes the information to the inifile,
        //the file written to depends on the method called.


        String freelistfile = ExtractFilePath(rawdiskdir);
        freelistfile = freelistfile +"\\" + disk_name + ".rfl";
        __int64 csbyte = current_size/12;

         //Makes a wrapper for the config file information.
        DSFileWrapper *sw = new DSFileWrapper(disk_name,csbyte,rawdiskdir,freelistfile,1,audio_dir,numdisks);
        rd_writer->setupDiskScheme(sw, rawDiskProgressBar,statusLabel);
        rd_writer->setupFreelist(sw, rawDiskProgressBar, statusLabel);
        rd_writer->setupRawDiskConfig(sw, rawDiskProgressBar, statusLabel);


 }










