//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ReadWriteControl.h"
#include "introSelectionFrame.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CreatePanelSeries"
#pragma link "introSelectionFrame"
#pragma link "DeletePanelSeries"
#pragma resource "*.dfm"
TReadWriteControlForm *ReadWriteControlForm;

const int CREATE_FRAME = 1;
const int INTRO_FRAME = 2;
const int DELETE_FRAME = 3;

//---------------------------------------------------------------------------
__fastcall TReadWriteControlForm::TReadWriteControlForm(TComponent* Owner)
        : TForm(Owner)
{
current_frame = INTRO_FRAME;

backButton->Enabled = false;

}
//---------------------------------------------------------------------------
void __fastcall TReadWriteControlForm::nextButtonClick(TObject *Sender)
{

if(current_frame == INTRO_FRAME){
        if (IntroFrame1->radio_clicked == CREATE_CLICKED){
                PanelSeries1->resetEverything();
                   PanelSeries1->Visible = true;
                IntroFrame1->Visible = false;

                current_frame = CREATE_FRAME;
                backButton->Enabled = false;
                }
        else if (IntroFrame1->radio_clicked == DELETE_CLICKED){
                DeleteFrame1->resetEverything();

                DeleteFrame1->Visible = true;
                IntroFrame1->Visible = false;
                current_frame = DELETE_FRAME;
                backButton->Enabled = false;
                

                }

}
else if(current_frame == CREATE_FRAME){
if(PanelSeries1->at_end == true){
      
        }

        if (!PanelSeries1->can_gonext){
        }
                if(PanelSeries1->at_end==true){

                                current_frame = INTRO_FRAME;
                                backButton->Enabled = false;
                                nextButton->Enabled = true;

                                PanelSeries1->Visible = false;
                                IntroFrame1->Visible = true;
                                PanelSeries1->resetPanels();

                }
        else{
                        nextButton->Enabled = true;
                        backButton->Enabled = true;
       }
               PanelSeries1->next();
}
else if (current_frame == DELETE_FRAME){
        if (DeleteFrame1->_finished){
                current_frame = INTRO_FRAME;
                backButton->Enabled = false;
                nextButton->Enabled = true;
                DeleteFrame1->Visible = false;
                IntroFrame1->Visible = true;
                DeleteFrame1->_finished = false;


                }

        }
}
//---------------------------------------------------------------------------

void __fastcall TReadWriteControlForm::backButtonClick(TObject *Sender)
{
nextButton->Enabled = true;
if(current_frame == CREATE_FRAME){
        PanelSeries1->back();
        if(!PanelSeries1->can_goprev){
                backButton->Enabled = false;
                }
                else{
                        backButton->Enabled = true;
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TReadWriteControlForm::cancelButtonClick(TObject *Sender)
{
  if(MessageBox(NULL,"Do you really want to quit the rawdisk utility?", "Cancel?",
              MB_YESNO|MB_ICONWARNING )==ID_YES){
        collectiveReset();
        ModalResult = 1;
        }
}
void TReadWriteControlForm::collectiveReset(){
  current_frame = 2;
  backButton->Enabled = false;
  IntroFrame1->createRadio->Checked = true;
  IntroFrame1->deleteRadio->Checked = false;
  IntroFrame1->radio_clicked = CREATE_CLICKED;
  IntroFrame1->Visible = true;
  PanelSeries1->resetPanels();
 PanelSeries1->resetEverything();
  PanelSeries1->Visible = false;
  DeleteFrame1->Visible = false;
  }

//void __fastcall TReadWriteControlForm::resetStuff(){
   //     current_frame =
//---------------------------------------------------------------------------

