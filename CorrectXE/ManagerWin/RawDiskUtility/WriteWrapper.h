
#ifndef WriteWrapperH
#define WriteWrapperH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>

class WriteWrapper{
private:

int num_drive;
__int64 chunk_size;
__int64 rawdisk_size;
int num_iterations;
String rawdisk_dir;

String aud_path;
String main_video;
String disk_name;
int alloc_method;
int start_pos;
String free_list;

public:
        __fastcall WriteWrapper(String rddir, __int64 chunksize,  __int64 rdsize, int iterations, String diskname, int memallocmethod, int startpos, String audpath, String mainvid, String flpath);
        __fastcall ~WriteWrapper();
        __int64 getRawDiskSize();
        __int64 getChunkSize();
        int getAllocMethod();
        String getDiskName();
        int getNumIterations();
        String getRawDiskDirectory();
        String getAudioPath();
        int getStartPosition();
        String getMainVidDir();
        String getFlPath();
};
#endif


