//----------------------------------------------------------------------------------------
//This is a wrapper class for information about the rawdisk. It gets passed to the writer from the gui.
//Kea Johnston, 7-9-04
//----------------------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WriteWrapper.h"



__fastcall WriteWrapper::WriteWrapper(String rddir,  __int64 chunksize,  __int64 rdsize, int iterations, String diskname, int memallocmethod, int startpos, String audpath, String mainvid, String flpath){
        rawdisk_dir = rddir;
        
        chunk_size = chunksize;
        rawdisk_size = rdsize;
        num_iterations = iterations;
        disk_name = diskname;
        alloc_method = memallocmethod;
        start_pos = startpos;
        aud_path = audpath;
        main_video = mainvid;
        free_list = flpath;
}

__fastcall WriteWrapper::~WriteWrapper(){
}
String WriteWrapper::getAudioPath(){
        return aud_path;
        }
String WriteWrapper::getFlPath(){
        return free_list;
        }
__int64 WriteWrapper::getRawDiskSize(){
        return rawdisk_size;
}
__int64 WriteWrapper::getChunkSize(){
        return chunk_size;
}
int WriteWrapper::getNumIterations(){
        return num_iterations;
}
String WriteWrapper::getRawDiskDirectory(){
        return rawdisk_dir;
}

String WriteWrapper::getDiskName(){
        return disk_name;
 }
 int WriteWrapper::getAllocMethod(){
        return alloc_method;
 }
 int WriteWrapper::getStartPosition(){
        return start_pos;
 }
 String WriteWrapper::getMainVidDir(){
        return main_video;
        }
 


