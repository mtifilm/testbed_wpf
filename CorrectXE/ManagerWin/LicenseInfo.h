//---------------------------------------------------------------------------

#ifndef LicenseInfoH
#define LicenseInfoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "machine.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TLicenseInfoForm : public TForm
{
__published:	// IDE-managed Components
        TListView *LicenseListView;
        TBitBtn *CloseButton;
        TMemo *StatusMemo;
        TLabel *Label1;
        TLabel *Label2;
private:	// User declarations
public:		// User declarations
        __fastcall TLicenseInfoForm(TComponent* Owner);
        void DisplayLicenseInfo(const string &sFeature);
};
//---------------------------------------------------------------------------
extern PACKAGE TLicenseInfoForm *LicenseInfoForm;
//---------------------------------------------------------------------------
#endif
