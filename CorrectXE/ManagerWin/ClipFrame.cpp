//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipFrame.h"
#include "ExtendClipMediaDialog.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ParamPanelForm"
#pragma resource "*.dfm"
TClipPanel *ClipPanel;
//---------------------------------------------------------------------------
__fastcall TClipPanel::TClipPanel(TComponent* Owner)
        : TParamPanel(Owner)
{
   Caption = "Clips";
   EnableDetails(true);
}
//---------------------------------------------------------------------------

       bool TClipPanel::Read(void)
{
    // Open the file
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    RawDiskEdit->Text =
       ini->ReadString("MediaStorage","RawDiskCfg", DEFAULT_RAWDISK_FILE).c_str();
    DeleteIniFile(ini);

    string sName = RawDiskEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    ReadRawFile(sName);

    if (MediaListBox->Items->Count > 0)
      {
        MediaListBox->ItemIndex = 0;
        MediaListBoxClick(NULL);
      }

    Modified = false;
    return true;
}

       bool TClipPanel::Apply(void)
{
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return true;   // Error is set

    ini->WriteString("MediaStorage","RawDiskCfg", RawDiskEdit->Text.c_str());
    DeleteIniFile(ini);

    // Write out the raw file
    if (!WriteRawFile(RawDiskEdit->Text.c_str()))
      {
         _MTIErrorDialog(Handle, (string)"Failed to write raw disk file\n" + RawDiskEdit->Text.c_str() + "\nCheck protection");
         return false;
      }

    Modified = false;
    return true;
}

void __fastcall TClipPanel::RawDiskResetButtonClick(TObject *Sender)
{
    RawDiskEdit->Text = DEFAULT_RAWDISK_FILE;
}
//---------------------------------------------------------------------------
  bool TClipPanel::DeleteRawFileSection(const string &Name, const string &Section)
{
    CIniFile *ini = CreateIniFile(Name);

    if (ini == NULL) return false;
    ini->EraseSection(Section);
    return DeleteIniFile(ini);
}

       bool TClipPanel::WriteRawFile(const string &Name)
{
   // Read list of sections and then the media and file lists
   CIniFile *ini = CreateIniFile(Name);
   if (ini == NULL) return false;

   for (unsigned i=0; i < RawMedia.size(); i++)
     {
        MediaData *MD = &RawMedia[i];
        ini->WriteString(MD->sSectionName,"MediaIdentifier", MD->sMediaName);
        ini->WriteString(MD->sSectionName,"RawVideoDirectory", MD->sVideoMediaName);
        ini->WriteString(MD->sSectionName,"FreeListFile", MD->sFreeListName);
        ini->WriteInteger(MD->sSectionName,"FastIOBoundary", MD->iFastIO);
      }
   return DeleteIniFile(ini);
}

       bool TClipPanel::ReadRawFile(const string &Name)
{
   // Read list of sections and then the media and file lists
   StringList sl;
   CIniFile *ini = CreateIniFile(Name);
   if (ini == NULL) return false;

   // Get all the sections, deciding which are rawdisk
   sl.clear();
   RawMedia.clear();
   ini->ReadSections(&sl);

   // Clear the list
   MediaListBox->Items->Clear();

   for (unsigned i=0; i < sl.size(); i++)
     {
        // See if the section defines a rawdisk
        if (ini->KeyExists(sl[i], "MediaIdentifier"))
          {
             // Now just copy all information to a record
             MediaData MD;
             MD.sMediaName = StringTrim(ini->ReadString(sl[i],"MediaIdentifier","No Media ID"));
             MediaListBox->Items->Add(MD.sMediaName.c_str());
             MD.sVideoMediaName = ini->ReadString(sl[i],"RawVideoDirectory","");
             MD.sFreeListName = ini->ReadString(sl[i],"FreeListFile","");
             MD.iFastIO = ini->ReadInteger(sl[i],"FastIOBoundary",16);
             MD.sSectionName = sl[i];
             MD.iCount = 0;
             RawMedia.push_back(MD);
          }
      }
   return DeleteIniFile(ini);
}

void __fastcall TClipPanel::RawDiskBrowseButtonClick(TObject *Sender)
{
    string sName = RawDiskEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    OpenDialog->InitialDir = GetFilePath(sName).c_str();
    OpenDialog->DefaultExt = "cfg";
    OpenDialog->FilterIndex = 0;
    if (OpenDialog->Execute())
      {
        RawDiskEdit->Text = OpenDialog->FileName;
      }
}
//---------------------------------------------------------------------------

void __fastcall TClipPanel::RawVideoFileBrowseButtonClick(TObject *Sender)
{
    string sName = RawVideoFileEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);
    OpenDialog->DefaultExt = "raw";
    OpenDialog->InitialDir = GetFilePath(sName).c_str();
    OpenDialog->FilterIndex = 1;
    if (OpenDialog->Execute())
      {
        RawVideoFileEdit->Text = OpenDialog->FileName;
      }
}

MTI_INT64 GetFileSize(const string &FName)
{
  LARGE_INTEGER  Result;
  HANDLE hFile = CreateFile(FName.c_str(), 0, FILE_SHARE_READ, NULL,
        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

  if (hFile == NULL) return -1;
  GetFileSizeEx(hFile, &Result);
  CloseHandle(hFile);
  return Result.QuadPart;
}

  string FormatWithCommas(MTI_INT64 value)
{
  ostringstream os;
  os << value;
  string Result;
  int k = 3 - os.str().size() % 3;
  bool bFirst = true;
  for (unsigned i = 0; i < os.str().size(); i++)
    {
       if (((k%3) == 0) && (!bFirst)) Result += ",";
       Result += os.str().at(i);
       k++;
       bFirst = false;
    }

  return Result;
}

  MTI_INT64 CheckFreeListSize(const string Name)
{
   MTI_INT64 fTotal, fMax;
   char sLoc[255], sNum[255];
   fMax = 0;

   FILE *fp = fopen(Name.c_str(), "r");
   if (fp == NULL) return -1;

   while (fscanf(fp, "%s %s",sLoc, sNum) != EOF)
    {
      unsigned l1=0, N1=0;
      strtok(sLoc,"=");
      char *p = strtok(NULL,"=");
      if (p != NULL)
          sscanf(p,"%d",&l1);

      strtok(sNum,"=");
      p = strtok(NULL,"=");
      if (p != NULL)
        {
          sscanf(p,"%d",&N1);
          fTotal = (MTI_INT64)l1 + N1;
          if (fMax < fTotal) fMax = fTotal;
        }
    }

   fclose(fp);
   return fMax*512;

}

//---------------------------------------------------------------------------

void __fastcall TClipPanel::MediaListBoxClick(TObject *Sender)
{
    ostringstream os;

    MediaData *pMD = RawMedia.begin() + MediaListBox->ItemIndex;
    RawVideoFileEdit->Text = pMD->sVideoMediaName.c_str();
    FreeListEdit->Text = pMD->sFreeListName.c_str();

    // Update the status
    if (!DoesFileExist(pMD->sVideoMediaName))
      {
         VideoMediaStatusLabel->Caption = "Video media file does not exist";
      }
    else
      {
         MTI_INT64 fsize = GetFileSize(pMD->sVideoMediaName);
         if (fsize != -1)
           {
             os << "Video media size is " << FormatWithCommas(fsize);
             VideoMediaStatusLabel->Caption = os.str().c_str();
           }
         else
             VideoMediaStatusLabel->Caption = "Could not get Video Media Size";
      }

    if (!DoesFileExist(pMD->sFreeListName))
      {
         FreeListStatusLabel->Caption = "Free list file does not exist";
      }
    else
      {
         MTI_INT64 flSize = CheckFreeListSize(pMD->sFreeListName);
         os.str("");
         os << "Acording to free list, size is  " << FormatWithCommas(flSize);
         FreeListStatusLabel->Caption = os.str().c_str();
      }

}
//---------------------------------------------------------------------------
   MediaData *TClipPanel::FindMediaID(const string &sID)
{
   for (unsigned i=0; i < RawMedia.size(); i++)
     {
        if (RawMedia[i].sMediaName == sID)
           {
              return &RawMedia[i];
           }
     }

   return NULL;
}

//--------------------CheckClip------------------John Mertus----Nov 2002-----

     bool TClipPanel::CheckClip(string In)

//  When a scan clip is done, this is called for every clip in every subdirectory
// It fills in the proper data into a structure.
//
//****************************************************************************
{
   CIniFile *ini = CreateIniFile(In);
   if (ini == NULL) return true;

   string sMedia = ini->ReadString("VideoProxyInfo0\\MediaStorage", "MediaStorage_0","");
   if (sMedia != "")
     {
       sMedia = StringTrim(sMedia.substr(sMedia.find(",")+1));
       MediaData *pMD = FindMediaID(sMedia);
       if (pMD == NULL)
         {
           OrphanedClips++;
           TFontStyles fs = ClipDetailsEdit->SelAttributes->Style;
           ClipDetailsEdit->SelAttributes->Style =
           ClipDetailsEdit->SelAttributes->Style << fsBold;
           ClipDetailsEdit->SelAttributes->Color = clRed;
           ClipDetailsEdit->Lines->Add((String)"Orphan clip with media " + sMedia.c_str());
           ClipDetailsEdit->SelAttributes->Color = clWindowText;
           ClipDetailsEdit->SelAttributes->Style = fs ;
           ClipDetailsEdit->Lines->Add((String)"   " + GetFilePath(In).c_str());
           ClipDetailsEdit->Lines->Add("");
         }
       else
         {
           // Save size and location & video proxy
           pMD->iCount++;
         }

     }
   DeleteIniFile(ini);
   return true;
}

//--------------------ListClip--------------------John Mertus----Nov 2002-----

     bool TClipPanel::ListClip(string In)

//  When a list clip is done, this is called for every clip in every subdirectory
//  and outputs the list of clips
//
//****************************************************************************
{
   CIniFile *ini = CreateIniFile(In);
   if (ini == NULL) return true;

   string sMedia = ini->ReadString("VideoProxyInfo0\\MediaStorage", "MediaStorage_0","");
   if (sMedia != "")
     {
       sMedia = StringTrim(sMedia.substr(sMedia.find(",")+1));
       MediaData *pMD = FindMediaID(sMedia);
       if (pMD != NULL)
         {
            pMD->iCount++;
            if (sMedia == MediaListBox->Items->Strings[MediaListBox->ItemIndex].c_str())
              {
                 ClipDetailsEdit->Lines->Add(GetFilePath(In).c_str());
              }
         }

     }
   DeleteIniFile(ini);
   return true;
}

void __fastcall TClipPanel::ClipScanButtonClick(TObject *Sender)
{
   StringList sl;
   CPMPGetBinRoots(&sl);
   if (sl.size() == 0) return;
   string Mask = sl[0];

   ExpandEnviornmentString(Mask);
   ConformFileName(Mask);

   OrphanedClips = 0;
   ClipDetailsEdit->Clear();
   for (unsigned i=0; i < RawMedia.size(); i++) RawMedia[i].iCount = 0;
   SweepDown(this, FILE_SWEEPER_CALL(CheckClips), AddDirSeparator(Mask)+"*.clp" , false);
   for (unsigned i=0; i < RawMedia.size(); i++)
     {
       String s = "Media ";
       ClipDetailsEdit->Lines->Add(s + RawMedia[i].sMediaName.c_str());
       s = "  Total Clips: " + (String)RawMedia[i].iCount;
       ClipDetailsEdit->Lines->Add(s);
       ClipDetailsEdit->Lines->Add("");
     }

   // Report the orphaned clips
   if (OrphanedClips > 1)
     ClipDetailsEdit->Lines->Add((String)OrphanedClips + " Orphan Clips");
   else if (OrphanedClips == 1)
     ClipDetailsEdit->Lines->Add((String)OrphanedClips + " Orphan Clip");
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


void __fastcall TClipPanel::ClipDetailsButtonClick(TObject *Sender)
{
//    Details = !bDetails;
//    SelectListViewChange(Sender, SelectListView->Selected, ctState);
}

   void TClipPanel::DetailsChanged(bool Value)
{

    // Set the size of the details button
    if (Details)
     {
        Width = ClipDetailsEdit->Left + ClipDetailsEdit->Width + 16;
     }
    else
     {
        Width = ClipDetailsEdit->Left - 2;
     }
   return;
}


void __fastcall TClipPanel::ClipDeleteMediaMenuClick(TObject *Sender)
{
   ostringstream os;
   StringList sl;
   CPMPGetBinRoots(&sl);
   if (sl.size() == 0) return;
   string Mask = sl[0];

   ExpandEnviornmentString(Mask);
   ConformFileName(Mask);
   Mask = AddDirSeparator(Mask)+"*.clp";

   if (MediaListBox->ItemIndex < 0) return;
   //Check to see if media exists
   for (unsigned i=0; i < RawMedia.size(); i++) RawMedia[i].iCount = 0;
   SweepDown(this, FILE_SWEEPER_CALL(CheckClips), Mask, false);
   MediaData *pMD = FindMediaID(MediaListBox->Items->Strings[MediaListBox->ItemIndex].c_str());
   if (pMD == NULL)
     {
        // Can never happen (ha)
        _MTIErrorDialog(Handle, "Failed to find the media to delete");
        return;
     }
   if (pMD->iCount > 0)
     {
        os << pMD->sMediaName << " is referenced by " << pMD->iCount << " Clips";
        os << endl << "Cannot delete the media";
        _MTIErrorDialog(Handle, os.str());
        return;
     }

   os.str("");
   os << "Also delete the Media and Free List files? " << endl
       << "Media: " << pMD->sVideoMediaName << endl
       << "Free List: " << pMD->sFreeListName;
   int mB = _MTICancelConfirmationDialog(Handle, os.str());
   if (mB == MTI_DLG_CANCEL) return;

   os.str("");
   os << "!!This action cannot be reversed!!" << endl;
   if (mB == MTI_DLG_OK)
     os << "Really delete the media ID and all files?";
   else
     os << "Delete the media ID but save the files?";

   if (_MTIConfirmationDialog(Handle, os.str()) != MTI_DLG_OK) return;

   // First delete the media files
   if (mB == MTI_DLG_OK)
     {
        // Remove the media file
        if (remove(pMD->sVideoMediaName.c_str()) != 0)
          {
            os.str("");
            os << "Could not delete media file " << pMD->sVideoMediaName << endl;
            os << "Because: " << strerror(errno);
            _MTIErrorDialog(Handle, os.str());
          }

        // Remove the free list file
        if (remove(pMD->sFreeListName.c_str()) != 0)
          {
            os.str("");
            os << "Could not delete free list file " << pMD->sFreeListName << endl;
            os << "Because: " << strerror(errno);
            _MTIErrorDialog(Handle, os.str());
          }
     }
   string sName = RawDiskEdit->Text.c_str();
   ExpandEnviornmentString(sName);
   ConformFileName(sName);
   DeleteRawFileSection(sName, pMD->sSectionName);
   RawMedia.erase(pMD);
   ReadRawFile(sName);
}
//---------------------------------------------------------------------------

void __fastcall TClipPanel::ClipRawPopUpPopup(TObject *Sender)
{
  ClipDeleteMediaMenu->Enabled =  (MediaListBox->ItemIndex >= 0);
  ClipExtendMediaMenu->Enabled =  (MediaListBox->ItemIndex >= 0);

  ClipNewMediaMenu->Enabled = false;

}
//---------------------------------------------------------------------------

void __fastcall TClipPanel::ListClipButtonClick(TObject *Sender)
{
   StringList sl;
   CPMPGetBinRoots(&sl);
   if (sl.size() == 0) return;
   string Mask = sl[0];

   ExpandEnviornmentString(Mask);
   ConformFileName(Mask);

   OrphanedClips = 0;
   ClipDetailsEdit->Clear();
   for (unsigned i=0; i < RawMedia.size(); i++) RawMedia[i].iCount = 0;
   ClipDetailsEdit->Lines->Add((String)"Clips for Media " +MediaListBox->Items->Strings[MediaListBox->ItemIndex]);
   SweepDown(this, FILE_SWEEPER_CALL(ListClips), AddDirSeparator(Mask)+"*.clp" , false);
   MediaData *pMD = FindMediaID(MediaListBox->Items->Strings[MediaListBox->ItemIndex].c_str());
   if (pMD != NULL)
     {

       String s = (String) "  Total Clips: " + (String)pMD->iCount;
       ClipDetailsEdit->Lines->Add(s);
     }

}
//---------------------------------------------------------------------------

void __fastcall TClipPanel::ClipExtendMediaMenuClick(TObject *Sender)
{
   ClipExtendMediaDlg->pMD = FindMediaID(MediaListBox->Items->Strings[MediaListBox->ItemIndex].c_str());
   ClipExtendMediaDlg->ShowModal();
}
//---------------------------------------------------------------------------


void __fastcall TClipPanel::RawDiskEditChange(TObject *Sender)
{
    Modified = true;
    ChangeHintOnEdit(Sender);
}

//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TClipPanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
   TParamPanel::SetAdminPriv(Value);
   if (AdminPrivilege)
     {
       RawDiskEdit->ReadOnly = false;
       RawDiskEdit->Color = clWindow;
       RawDiskResetButton->Enabled = true;
       RawDiskBrowseButton->Enabled = true;
     }
   else
     {
       RawDiskEdit->ReadOnly = true;
       RawDiskEdit->Color = clBtnFace;
       RawDiskResetButton->Enabled = false;
       RawDiskBrowseButton->Enabled = false;
     }

   RawVideoFileEdit->ReadOnly = RawDiskEdit->ReadOnly;
   RawVideoFileEdit->Color = RawDiskEdit->Color;
   RawVideoFileBrowseButton->Enabled = RawDiskBrowseButton->Enabled;

   FreeListEdit->ReadOnly = RawDiskEdit->ReadOnly;
   FreeListEdit->Color = RawDiskEdit->Color;
   FreeListBrowseButton->Enabled = RawDiskBrowseButton->Enabled;

}

void __fastcall TClipPanel::RawVideoFileEditChange(TObject *Sender)
{
    MediaData *pMD = RawMedia.begin() + MediaListBox->ItemIndex;
    if (pMD != NULL)
      {
        if (pMD->sVideoMediaName != RawVideoFileEdit->Text.c_str())
          {
            Modified = true;
            pMD->sVideoMediaName = RawVideoFileEdit->Text.c_str();
          }
      }

    ChangeHintOnEdit(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TClipPanel::FreeListEditChange(TObject *Sender)
{
    MediaData *pMD = RawMedia.begin() + MediaListBox->ItemIndex;
    if (pMD != NULL)
      {
        if (pMD->sFreeListName != FreeListEdit->Text.c_str())
          {
            Modified = true;
            pMD->sFreeListName = FreeListEdit->Text.c_str();
          }
      }

    ChangeHintOnEdit(Sender);

}
//---------------------------------------------------------------------------

