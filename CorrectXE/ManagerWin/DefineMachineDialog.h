//---------------------------------------------------------------------------

#ifndef DefineMachineDialogH
#define DefineMachineDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMachineDefineDialog : public TForm
{
__published:	// IDE-managed Components
        TButton *CancelButton;
        TButton *OKButton;
        TMaskEdit *IDHighEdit;
        TLabel *Label2;
        TPanel *Panel1;
        TComboBox *CopySchemeEdit;
        TCheckBox *CopySchemeCBox;
        TEdit *NameEdit;
        TLabel *Label1;
        TPanel *Panel2;
        TSpeedButton *QueryButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall CopySchemeCBoxClick(TObject *Sender);
        void __fastcall OKButtonClick(TObject *Sender);
        void __fastcall IDHighEditKeyPress(TObject *Sender, char &Key);
        void __fastcall IDHighEditChange(TObject *Sender);
        void __fastcall QueryButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMachineDefineDialog(TComponent* Owner);
       TListItem *liUseMachine;

};
//---------------------------------------------------------------------------
extern PACKAGE TMachineDefineDialog *MachineDefineDialog;
//---------------------------------------------------------------------------
#endif
