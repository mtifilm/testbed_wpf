//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MTIManagerUnit.h"
#include <FileCtrl.hpp>
#include <shlobj.h>
#include "DllSupport.h"
#include "AboutUnit.h"
#include "MTIWinInterface.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TB2Dock"
#pragma link "TB2Item"
#pragma link "TB2MRU"
#pragma link "TB2Toolbar"
//#pragma link "CSPIN"
#pragma link "VDoubleEdit"
#pragma link "ParamPanelForm"
#pragma link "VTRFrame"
#pragma link "SecuriteFrame"
#pragma link "DDRFrame"
#pragma link "ClipFrame"
#pragma link "SiteFrame"
#pragma link "MTIUNIT"
#pragma link "RawDiskAddFrame"
#pragma resource "*.dfm"


//
// Some defines to make it easier

#define PrivilegePanel 0
#define PAWN_BITMAP_NO 46
#define BIG_PAWN_BITMAP_NO 45
#define CROWN_BITMAP_NO 44

TMainForm *MainForm;

//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TMTIForm(Owner)
{

   Panels.clear();   // Not really needed

   // Loop through the form finding all the TParamPanels that are
   // used for parameters
   for (int i=0; i < ComponentCount; i++)
     if (Components[i]->InheritsFrom(__classid(TParamPanel)) )
      {
         TParamPanel *tpp = (TParamPanel *)Components[i];
         // Create a new list item;
         TListItem *ListItem = SelectListView->Items->Add();

         //  Add the String, Icon and pointer to ParamPanel
         ListItem->Caption = (String)"  " + tpp->Caption;
         ListItem->ImageIndex = SelectionImageList->Count;
         ListItem->Data = tpp;

         Panels.push_back(tpp);  // Store the panel list
         // Copy the icon from the ParamPanel
         // If there is not one, the last one is used
         SelectionImageList->AddImages(tpp->ImageList);
         tpp->OnModify = ModifiedChanged;
         tpp->OnDetails = DetailsChanged;
      }

   // Read all the configurations
   for (unsigned i=0; i < Panels.size(); i++)
       Panels[i]->Read();

   m_bAdminPriv = true;             // Force an update
   AdminPrivilege = false;

   AdministratorMenuItem->Enabled = IsAdmin();
   PawnButton->Enabled = AdministratorMenuItem->Enabled;

   PostMessage(Handle, CW_START_PROGRAM, 0, 0);
}
//---------------------------------------------------------------------------

//-------------------StartForm--------------------John Mertus----Oct 2002-----

  void TMainForm::StartForm(void)

//  Normal start after all has been loaded.
//
//****************************************************************************
{
   RestoreAllProperties();
   UpdateAllButtons();
}

void __fastcall TMainForm::SelectListViewInfoTip(TObject *Sender,
      TListItem *Item, AnsiString &InfoTip)
{
//    if (Item->Data == NULL) return;
//TBD    InfoTip = ((PageStruct *)(Item->Data))->Hint;
}
//---------------------------------------------------------------------------

  void ClearBltBtnGlyph(TBitBtn *button)
{
    RECT aRect;
    aRect.left = 0;
    aRect.top = 0;
    aRect.right = button->Glyph->Width;
    aRect.bottom = button->Glyph->Height;
    button->Glyph->Canvas->Brush->Style = bsClear;
    button->Glyph->Canvas->Brush->Color = clBtnFace;
    button->Glyph->Canvas->FillRect(aRect);
}

void __fastcall TMainForm::SelectListViewChange(TObject *Sender,
      TListItem *Item, TItemChange Change)
{
   if (Item == NULL) return;           // Nothing selected, can happen
   if (Change != ctState) return;

   TParamPanel *tpp = (TParamPanel *)Item->Data;
   if (tpp == NULL) return;             // No page data, should never happen

   // This are hidden so autosizing will work
   TBDockTop->Visible = false;
   QuitButton->Visible = false;
   DetailsButton->Visible = false;
   SpaceLine->Visible = false;
   StatusBar->Visible = false;

   // This make the old one invisible and the new one visible
    for (unsigned i=0; i < Panels.size(); i++)
      Panels[i]->Visible = (tpp == Panels[i]);

   // Restore visibality
   TBDockTop->Visible = true;
   QuitButton->Visible = true;
   DetailsButton->Visible = tpp->HasDetails;
   SpaceLine->Width = Width - SpaceLine->Left - 16;
   SpaceLine->Visible = true;
   StatusBar->Visible = true;

   // Now set the display properly
   DetailsChanged(NULL);
   UpdateAllButtons();

}

   void TMainForm::UpdateAllButtons(void)
// This enables/disable the apply buttons for the current environment
//
{
   if (SelectListView->Selected == NULL) return;   // Nothing selected
   TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
   if (tpp == NULL) return;                       // No data, should never happen

   ApplyButton->Enabled = tpp->Modified;
   ApplySpeedButton->Enabled = tpp->Modified;

}




void __fastcall TMainForm::OkButtonClick(TObject *Sender)
{
   for (unsigned i=0; i < Panels.size(); i++)
      if (Panels[i]->Modified) Panels[i]->Apply();
   Close();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void __fastcall TMainForm::QuitButtonClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
   for (unsigned i=0; i < Panels.size(); i++)
     {
       if (Panels[i]->Modified)
        {
          String str = (String)"The " + Panels[i]->Caption + " Configuration has been modified\nSave Configuration?";
          int mb = MessageBox(Application->Handle, str.c_str(),
               "Configuration Not Saved", MB_ICONWARNING | MB_YESNOCANCEL);
          if (mb == IDYES)
            {
              Panels[i]->Apply();
            }
          else if (mb == IDCANCEL)
            {
              CanClose = false;
              return;
            }
        }
     }

// Save all the properties
  SaveAllProperties();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// This just sends a message to the component which has focus and
// then preforms a copy.  Note: Component writers MUST respond to
// WM_COPY
void __fastcall TMainForm::CopyMenuClick(TObject *Sender)
{
  SendMessage(GetFocus(), WM_COPY, 0, 0);
}

//---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a CUT.  Note: Component writers MUST respond to
// WM_CUT
void __fastcall TMainForm::CutMenuClick(TObject *Sender)
{
  SendMessage(GetFocus(), WM_CUT, 0, 0);
}
//---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a PASTE.  Note: Component writers MUST respond to
// WM_PASTE
void __fastcall TMainForm::PasteMenuClick(TObject *Sender)
{
  SendMessage(GetFocus(), WM_PASTE, 0, 0);
}

//---------------------------------------------------------------------------


void __fastcall TMainForm::DetailsButtonClick(TObject *Sender)
{
  if (SelectListView->Selected == NULL) return;   // Nothing selected
  TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
  if (tpp == NULL) return;                       // No data, should never happen
  tpp->Details = !tpp->Details;

  // Now redraw it if needed
  SelectListViewChange(Sender, SelectListView->Selected, ctState);
}

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // This is a quit, if modifed, restore Ghosts
   for (unsigned i=0; i < Panels.size(); i++)
       Panels[i]->Restore();

   // Remove the selections to avoid calling it when data is invalid
   SelectListView->OnChange = NULL;
}

void __fastcall TMainForm::ApplyButtonClick(TObject *Sender)
{
   if (SelectListView->Selected == NULL) return;   // Nothing selected
   TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
   if (tpp == NULL) return;                       // No data, should never happen
   // Now go do it
   tpp->Apply();
   UpdateAllButtons();
}

void __fastcall TMainForm::ModifiedChanged(TObject *Sender)
{
   UpdateAllButtons();
}

void __fastcall TMainForm::DetailsChanged(TObject *Sender)
{
    if (SelectListView->Selected == NULL) return;   // Nothing selected
    TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
    if (tpp == NULL) return;                       // No data, should never happen

    // Set the details button display
    if (tpp->Details)
     {
        DetailsButton->Caption = "Hide <<";
        ClearBltBtnGlyph(DetailsButton);
        ButtonImageList->GetBitmap(43, DetailsButton->Glyph);
     }
    else
     {
        DetailsButton->Caption = "Details >>";
        ClearBltBtnGlyph(DetailsButton);
        ButtonImageList->GetBitmap(42, DetailsButton->Glyph);
     }
}

  bool TMainForm::WriteSettings(CIniFile *ini, const string &IniSection)
  {
     if (SelectListView->Selected == NULL) return true;
     ini->WriteInteger(IniSection, "LastPage", SelectListView->Selected->Index);

     TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
     if (tpp == NULL) return true;                       // No data, should never happen
     ini->WriteBool(IniSection,"Details", tpp->Details);
     return true;
  }

  bool TMainForm::ReadSettings(CIniFile *ini, const string &IniSection)
  {
    int idx = ini->ReadInteger(IniSection, "LastPage", 0);
    SelectListView->Selected = SelectListView->Items->Item[idx];
    // Because the autosize fucks up if done in the constructor, the
    // details may be wrong, if so, force a recalibration.
    for (unsigned i=0; i < Panels.size(); i++)
       Panels[i]->Details = false;

    TParamPanel *tpp = (TParamPanel *)SelectListView->Selected->Data;
    tpp->Details = ini->ReadBool(IniSection, "Details", false);
    SelectListViewChange(NULL, SelectListView->Selected, ctState);
    return true;
  }

   void __fastcall TMainForm::WMIniChange(TMessage &Message)
{
   //  A major system environmental variable change has occured, alert all panels
   for (unsigned i=0; i < Panels.size(); i++)
       Panels[i]->IniHasChangedCB();
}


void __fastcall TMainForm::PrivilegeButtonClick(TObject *Sender)
{
   AdminPrivilege = !AdminPrivilege;
   StatusBar->Invalidate();
}
//---------------------------------------------------------------------------

       void __fastcall TMainForm::SetAdminPriv(bool Value)
{
   if (Value == m_bAdminPriv) return;
   m_bAdminPriv = Value;
   for (unsigned i=0; i < Panels.size(); i++)
       Panels[i]->AdminPrivilege = Value;
   UpdateAllButtons();
}

void __fastcall TMainForm::FileMenuItemClick(TObject *Sender)
{
   AdministratorMenuItem->Checked = AdminPrivilege;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ButtonMenuItemClick(TObject *Sender)
{
    ButtonToolbar->Visible = ! ButtonToolbar->Visible;
    ButtonMenuItem->Checked = ButtonToolbar->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ToolBarMenuItemClick(TObject *Sender)
{
    ButtonMenuItem->Checked = ButtonToolbar->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StatusBarDrawPanel(TStatusBar *StatusBar,
      TStatusPanel *Panel, const TRect &Rect)
{
   int i;
   TCanvas *pCanvas = StatusBar->Canvas;

   switch (Panel->Index)
    {
      // Decide to display the lock or not
      case PrivilegePanel:
         i = (Rect.Bottom-Rect.Top - ButtonImageList->Height)/2;

         if (AdminPrivilege)
           {
             ButtonImageList->Draw(pCanvas,Rect.Left+3,Rect.Top+i+1, CROWN_BITMAP_NO, true);
             PawnButton->ImageIndex = CROWN_BITMAP_NO;
           }
         else
           {
             ButtonImageList->Draw(pCanvas,Rect.Left+3,Rect.Top+i+1, BIG_PAWN_BITMAP_NO, true);
             PawnButton->ImageIndex = PAWN_BITMAP_NO;
           }
     }

}
//---------------------------------------------------------------------------
   void __fastcall TMainForm::CMStatuBar(TMessage &Message)
{
   //  A major system environmental variable change has occured, alert all panels
   StatusBar->Panels->Items[Message.LParam]->Text = (char *)Message.WParam;
}

/*
  pCanvas->Brush->Color = clRed;

  pCanvas->FillRect(Rect);
  pCanvas->Font->Color = clYellow;
  ImageList1->Draw(pCanvas,Rect.Left,Rect.Top, Panel->Index, true);
*/

void __fastcall TMainForm::AboutMenuItemClick(TObject *Sender)
{
   if (AboutForm == NULL) AboutForm = new TAboutForm(this);
   AboutForm->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::VTRPanelTimeSenseComboBoxChange(TObject *Sender)
{
  VTRPanel->SchemeChangedClick(Sender);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
  char FileName[256];
  GetModuleFileName( NULL,FileName, sizeof (FileName) );
  Caption = (AnsiString)"MTI Manager, Version " + MTIBuildDate().c_str() + ", " + GetFileDateString(FileName).c_str();
  LoadMainIcons();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Exit1Click(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------







