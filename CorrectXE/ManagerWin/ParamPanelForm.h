//---------------------------------------------------------------------------

/*
   Ths is the base panel for the parameters
   Each set of parameters inherits and overrides functions

   Properties
       Details  -  True expands the panel to another size
                   False restricts the panel.
       Modified -  True if there has been a change in the
                   parameters.  Generates an OnModified Event.


   To use, inherit a class
      Create a 32x32 icon and store in ImageList
      Initialize Caption inside the constructor to the name you want displayed
*/

#ifndef ParamPanelFormH
#define ParamPanelFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ImgList.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>

#define CM_STATUSMESSAGE (WM_APP+7116)

//---------------------------------------------------------------------------

class TParamPanel : public TFrame
{
__published:	// IDE-managed Components
        TImageList *ImageList;
        TOpenDialog *OpenCfgDialog;

private:	// User declarations
     bool m_bModified;
     bool m_bDetails;
     bool m_bHasDetails;
     TNotifyEvent m_OnModified;
     TNotifyEvent m_OnDetails;

protected:
     string p_sCfgIniFileName;       // Name with $(var) variables from ini file
                                     // Edit->Text receives this
     string p_sLoadedIniFileName;    // Name with $(var) resolved
     bool p_bAdminPriv;

     void SetModified(bool Value);
     void SetDetails(bool Value);
     virtual void __fastcall SetAdminPriv(bool Value);

     virtual void DetailsChanged(bool Value);
     void ChangeHintOnEdit(TObject *Sender);
     virtual bool ChangeIniFile(TObject *Sender);
     void EnableDetails(bool Value);

public:		// User declarations
     virtual bool Read(void) {return true;}
     virtual bool ReadFile(const string &FileName) {return true; }
     virtual bool Apply(void){return true;}
     virtual void Restore(void) {return; }
     virtual bool IniHasChangedCB(void);

     virtual int ApplyQuery(void);

   __fastcall TParamPanel(TComponent* Owner);
   __property bool Modified = {read=m_bModified, write=SetModified, default=false};
   __property bool Details = {read=m_bDetails, write=SetDetails, default=false};
   __property bool HasDetails = {read=m_bHasDetails, write=m_bHasDetails, default=false};

   void UpdateStatusBar(const string &Msg, int iPanel);


__published:	// IDE-managed Components
   __property TNotifyEvent OnModify = {read=m_OnModified, write=m_OnModified};
   __property TNotifyEvent OnDetails = {read=m_OnDetails, write=m_OnDetails};
   __property Caption;
   __property bool AdminPrivilege = {read=p_bAdminPriv, write=SetAdminPriv, default = false};


};
//---------------------------------------------------------------------------
extern PACKAGE TParamPanel *ParamPanel;
//---------------------------------------------------------------------------

#endif
