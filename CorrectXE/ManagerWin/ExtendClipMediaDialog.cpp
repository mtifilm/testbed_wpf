//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ExtendClipMediaDialog.h"
//--------------------------------------------------------------------- 
#pragma link "MTIUNIT"
#pragma resource "*.dfm"
TClipExtendMediaDlg *ClipExtendMediaDlg;
//---------------------------------------------------------------------
__fastcall TClipExtendMediaDlg::TClipExtendMediaDlg(TComponent* AOwner)
	: TMTIForm(AOwner)
{
}
   void DisplayError(char *pszAPI)
   {
       LPVOID lpvMessageBuffer;
       CHAR szPrintBuffer[512];
       DWORD nCharsWritten;

       FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM,
                NULL, GetLastError(),
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                (LPTSTR)&lpvMessageBuffer, 0, NULL);

       wsprintf(szPrintBuffer,
         "ERROR: API    = %s.\n   error code = %d.\n   message    = %s.\n",
                pszAPI, GetLastError(), (char *)lpvMessageBuffer);

       ShowMessage(szPrintBuffer);
       LocalFree(lpvMessageBuffer);
       exit(GetLastError());
   }

//---------------------------------------------------------------------
void __fastcall TClipExtendMediaDlg::DefragButtonClick(TObject *Sender)
{
      PROCESS_INFORMATION pi;
      STARTUPINFO si;

      // Set up the start up info struct.
      ZeroMemory(&si,sizeof(STARTUPINFO));
      si.cb = sizeof(STARTUPINFO);
      si.dwFlags = STARTF_USESHOWWINDOW;
      // Use this if you want to hide the child:
      si.wShowWindow = SW_NORMAL;

      if (!CreateProcess("c:\\winnt\\system32\\cmd.exe", "c:\\Winnt\\System32\\dfrg.msc", NULL,NULL,FALSE,
                         0,NULL,NULL,&si, &pi))
      DisplayError("Could not launch program");
       return;

}
//---------------------------------------------------------------------------
extern string FormatWithCommas(MTI_INT64 value);

void  TClipExtendMediaDlg::UpdateFreeSpace(void)
{
  MTI_UINT64 llBytesFree;
  unsigned long ulSectorsPerCluster, ulNumberOfFreeClusters;
  unsigned long ulTotalNumberOfClusters,ulBytesPerSector ;

  string sDiskName = ExtractFileDrive(pMD->sVideoMediaName.c_str()).c_str();
  if (GetDiskFreeSpace(sDiskName.c_str(), &ulSectorsPerCluster, &ulBytesPerSector,
    &ulNumberOfFreeClusters, &ulTotalNumberOfClusters) == 0)
  {
    DisplayError("Could not read");
    return;  // Problem
  }

  llBytesFree = (MTI_UINT64)ulNumberOfFreeClusters * ulSectorsPerCluster * ulBytesPerSector;
//  osstream os;
//  os << llBytesFree;
  SpaceLabel->Caption = FormatWithCommas(llBytesFree).c_str();

}
void __fastcall TClipExtendMediaDlg::Button1Click(TObject *Sender)
{
   UpdateFreeSpace();
}
//---------------------------------------------------------------------------

