//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DDRFrame.h"
#include "DllSupport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "ParamPanelForm"
#pragma link "VDoubleEdit"
#pragma resource "*.dfm"
TDDRPanel *DDRPanel;
//---------------------------------------------------------------------------
__fastcall TDDRPanel::TDDRPanel(TComponent* Owner)
        : TParamPanel(Owner)
{
   Caption = "DDR";
   EnableDetails(false);

  // After this point, the items may be rearanged with no problem
  WholeOverlayImage = new Graphics::TBitmap;
  WholeOverlayImage->Assign(OverlayImage->Picture->Bitmap);
  DrawOverlay(OldOvlyX, OldOvlyY);
}

//*************************************DDR Configuration************************************

//-----------------------Read---------------------John Mertus----Oct 2002-----

     bool TDDRPanel::Read(void)

//  This looks up the standard configuration file name and reads it
//
//****************************************************************************
{
  // Open the file
  CIniFile *ini = CPMPOpenLocalMachineIniFile();
  if (ini == NULL) return false;   // Error is set
  p_sCfgIniFileName = ini->ReadString(VTR_SECTION, "EmulatorConfig", DEFAULT_EMULATOR_FILE);
  DDRCfgEdit->Text = p_sCfgIniFileName.c_str();
  ChangeHintOnEdit(DDRCfgEdit);

  // Done with the file
  DeleteIniFile(ini);

  return ReadFile(p_sCfgIniFileName);
}

//-----------------------ReadFile-----------------John Mertus----Oct 2002-----

     bool TDDRPanel::ReadFile(const string &FileName)

//  Read the DDR (Emulator) configuraion from FileName
//  Note: $(var) variables are expanded
//   Return is true if entire file was read.
//
//****************************************************************************
{
  // Now read the emulator file
  string sFileName = ConformEnvironmentName(FileName);

  CIniFile *ini = CreateIniFile (sFileName);
  if (ini == NULL) return false;

  // Overlay Section
  bOverlayUpdate = false;
  OverlayCBox->Checked = (ini->ReadInteger("VTR", "OverlayEnable", 0) != 0);
  OldOvlyX = ini->ReadInteger("VTR", "OverlayX", 20)/100.0*OverlayImage->Width;
  OldOvlyY = ini->ReadInteger("VTR", "OverlayY", 20)/100.0*OverlayImage->Height;
  OverlaySizeSEdit->Value = ini->ReadInteger("VTR", "OverlaySZ", 5);

  // -1 means a special background
  if (ini->ReadInteger("VTR", "OverlayBG", 0) == -1)
    OverlayBGColorGroup->ItemIndex = 1;
  else
    OverlayBGColorGroup->ItemIndex = 0;

  OverlayFGColorGroup->ItemIndex = ini->ReadInteger("VTR", "OverlayFG", 0) % 2;

  // "Emulator" responses
  ostringstream os;
  os << hex << setw(2) << setfill('0') <<
    ini->ReadInteger("VTR", "DeviceType0A", 0x30) << ":" <<
    hex << setw(2) << setfill('0') <<
    ini->ReadInteger("VTR", "DeviceType0B", 0x10);
  DeviceIDEdit->Text = os.str().c_str();

  MaxShuttleSpeedSEdit->Value = ini->ReadInteger("VTR", "MaxShuttleSpeed", 30);

  VideoTimeCodeDelayPlaySEdit->Value = ini->ReadInteger("VTR", "VideoTimeCodeDelayPlay", 0);
  VideoTimeCodeDelayRecordSEdit->Value = ini->ReadInteger("VTR", "VideoTimeCodeDelayRecord", 0);
  IgnoreControllerTimingCBox->Checked = ini->ReadInteger("VTR", "IgnoreControllerTiming", 0) == 0;
  StopReactionFrameSEdit->Value = ini->ReadInteger("VTR", "StopReactionFrame", 0);
  InstructionFrameDelaySEdit->Value = ini->ReadInteger("VTR", "InstructionFrameDelay", 7);

  ProxyFileCBox->Checked =
     (ini->ReadInteger("VTR", "ProxyIn0", 0) != 0);
  DifferenceFileCBox->Checked =
     (ini->ReadInteger("VTR", "DiffFile0", 0) != 0);


  // Audio Section
  AudioInCBox->Checked =
     (ini->ReadInteger("VTR", "AudioIn0", 1) != 0);

  AudioOutCBox->Checked =
     (ini->ReadInteger("VTR", "AudioOut0", 1) != 0);

  AudioWriteDiskCBox->Checked =
     (ini->ReadInteger("VTR", "AudioInhibitIn0", 0) != 0);

  AudioReadDiskCBox->Checked =
     (ini->ReadInteger("VTR", "AudioInhibitOut0", 0) != 0);

  AudioOutDeviceName->Text = ini->ReadString("VTR", "AudioDeviceNameOut", "").c_str();
  AudioInDeviceName->Text = ini->ReadString("VTR", "AudioDeviceNameIn", "").c_str();

  AudioSlipInEdit->dValue = ini->ReadDouble("VTR", "AudioSlipIn", 0);
  AudioSlipOutEdit->dValue = ini->ReadDouble("VTR", "AudioSlipOut", 0);

  AudioBlendEdit->dValue = ini->ReadDouble("VTR", "MilliSecondsAudioBlend", 15);

  // Video Section
  VideoInCBox->Checked =
     (ini->ReadInteger("VTR", "VideoIn0", 1) != 0);

  VideoOutCBox->Checked =
     (ini->ReadInteger("VTR", "VideoOut0", 1) != 0);

  VideoWriteDiskCBox->Checked =
     (ini->ReadInteger("VTR", "DiskInhibitIn0", 0) != 0);

  VideoReadDiskCBox->Checked =
     (ini->ReadInteger("VTR", "DiskInhibitOut0", 0) != 0);

  VideoOutDeviceName->Text = ini->ReadString("VTR", "VideoBoardNameOut", "").c_str();
  VideoInDeviceName->Text = ini->ReadString("VTR", "VideoBoardNameIn", "").c_str();

  // Now read the connect parameters
  int iP = ini->ReadInteger("VTR", "SerialPort0", -1);
  int iS = ini->ReadInteger("VTR", "SocketPort0", 1234);

  // Connect to the network or serial port
  if (iP == -1)
    {
      ConnectGroupBox->ItemIndex = 1;
      iP = 1;
    }

  // Don't expose the -1 default
  if (iS == -1) iS = 1234;
  DDRSerialPortSEdit->Value = iP;
  DDRSocketSEdit->Value = iS;
  DDRHostEdit->Text = ini->ReadString("VTR", "HostName0", "localhost").c_str();
  //
  // close emulator config file
  DeleteIniFile(ini);

  // This inhibits the overlay updates
  bOverlayUpdate = true;
  DrawOverlay(OldOvlyX, OldOvlyY);
  Modified = false;
  p_sLoadedIniFileName = sFileName;
  return true;
}

//---------------------Apply---------------------John Mertus----Oct 2002-----

     bool TDDRPanel::Apply(void)

//   Write out the changes
//
//****************************************************************************
{
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    string sCfgName = DDRCfgEdit->Text.c_str();
    ini->WriteString(VTR_SECTION, "EmulatorConfig", sCfgName);
    DeleteIniFile(ini);

    // Now write the emulator file
    sCfgName = ConformEnvironmentName(sCfgName);
    ini = CreateIniFile (sCfgName);
    if (ini == NULL) return false;

    // Overlay Section
    if (OverlayCBox->Checked)
       ini->WriteInteger("VTR", "OverlayEnable", 1);
    else
       ini->WriteInteger("VTR", "OverlayEnable", 0);

    ini->WriteInteger("VTR", "OverlayX", OldOvlyX*100.0/OverlayImage->Width+.99);
    ini->WriteInteger("VTR", "OverlayY", OldOvlyY*100.0/OverlayImage->Height+.99);
    ini->WriteInteger("VTR", "OverlaySZ", OverlaySizeSEdit->Value);

    // Now decode the FG/Color buttons
    ini->WriteInteger("VTR", "OverlayFG", OverlayFGColorGroup->ItemIndex);

    if (OverlayBGColorGroup->ItemIndex == 1)
      ini->WriteInteger("VTR", "OverlayBG", -1);
    else
      ini->WriteInteger("VTR", "OverlayBG", (OverlayFGColorGroup->ItemIndex + 1) %2);


    // Write the results
    string s = DeviceIDEdit->Text.c_str();

    ini->WriteString("VTR","DeviceType0A", (string)"0x" + s.substr(0,2));
    ini->WriteString("VTR","DeviceType0B", (string)"0x" + s.substr(3,2));

    ini->WriteInteger("VTR", "MaxShuttleSpeed", MaxShuttleSpeedSEdit->Value);
    
    ini->WriteInteger("VTR", "VideoTimeCodeDelayPlay", VideoTimeCodeDelayPlaySEdit->Value);
    ini->WriteInteger("VTR", "VideoTimeCodeDelayRecord", VideoTimeCodeDelayRecordSEdit->Value);
    IgnoreControllerTimingCBox->Checked = ini->ReadInteger("VTR", "IgnoreControllerTiming", 0) == 0;
    ini->WriteInteger("VTR", "StopReactionFrame", StopReactionFrameSEdit->Value);
    ini->WriteInteger("VTR", "InstructionFrameDelay", InstructionFrameDelaySEdit->Value);

    if (ProxyFileCBox->Checked)
      ini->WriteInteger("VTR", "ProxyIn0", 1);
    else
      ini->WriteInteger("VTR", "ProxyIn0", 0);

    if (DifferenceFileCBox->Checked)
      ini->WriteInteger("VTR", "DiffFile0", 1);
    else
      ini->WriteInteger("VTR", "DiffFile0", 0);

    // Audio Section
    if (AudioInCBox->Checked)
       ini->WriteInteger("VTR", "AudioIn0", 1);
    else
       ini->WriteInteger("VTR", "AudioIn0", 0);

    if (AudioOutCBox->Checked)
       ini->WriteInteger("VTR", "AudioOut0", 1);
    else
       ini->WriteInteger("VTR", "AudioOut0", 0);

    if (AudioWriteDiskCBox->Checked)
       ini->WriteInteger("VTR", "AudioInhibitIn0", 1);
    else
       ini->WriteInteger("VTR", "AudioInhibitIn0", 0);

    if (AudioReadDiskCBox->Checked)
       ini->WriteInteger("VTR", "AudioInhibitOut0", 1);
    else
       ini->WriteInteger("VTR", "AudioInhibitOut0", 0);

    ini->WriteString("VTR", "AudioDeviceNameOut", AudioOutDeviceName->Text.c_str());
    ini->WriteString("VTR", "AudioDeviceNameIn", AudioInDeviceName->Text.c_str());

    ini->WriteDouble("VTR", "AudioSlipIn", AudioSlipInEdit->dValue);
    ini->WriteDouble("VTR", "AudioSlipOut", AudioSlipOutEdit->dValue);

    ini->WriteDouble("VTR", "MilliSecondsAudioBlend", AudioBlendEdit->dValue );

    // Video Section
    if (VideoInCBox->Checked)
       ini->WriteInteger("VTR", "VideoIn0", 1);
    else
       ini->WriteInteger("VTR", "VideoIn0", 0);

    if (VideoOutCBox->Checked)
       ini->WriteInteger("VTR", "VideoOut0", 1);
    else
       ini->WriteInteger("VTR", "VideoOut0", 0);

    if (VideoWriteDiskCBox->Checked)
       ini->WriteInteger("VTR", "DiskInhibitIn0", 1);
    else
       ini->WriteInteger("VTR", "DiskInhibitIn0", 0);

    if (VideoReadDiskCBox->Checked)
       ini->WriteInteger("VTR", "DiskInhibitOut0", 1);
    else
       ini->WriteInteger("VTR", "DiskInhibitOut0", 0);

    ini->WriteString("VTR", "VideoDeviceNameOut", VideoOutDeviceName->Text.c_str());
    ini->WriteString("VTR", "VideoDeviceNameIn", VideoInDeviceName->Text.c_str());


    // Connection parameters
    if (ConnectGroupBox->ItemIndex == 1)
      {
        ini->WriteInteger("VTR", "SerialPort0", -1);
        ini->WriteInteger("VTR", "SocketPort0", DDRSocketSEdit->Value);
      }
    else
      {
        ini->WriteInteger("VTR", "SerialPort0", DDRSerialPortSEdit->Value);
        ini->WriteInteger("VTR", "SocketPort0", -1);
      }

    ini->WriteString("VTR", "HostName0", DDRHostEdit->Text.c_str());

    // close emulator config file
    DeleteIniFile(ini);

    // The dongle file should be up to date because they are ghost parameters
    Modified = false;
    return true;
}


void __fastcall TDDRPanel::DDRCfgBrowseButtonClick(TObject *Sender)
{
    TEdit *Edit = DDRCfgEdit;
    string sName = ConformEnvironmentName(Edit->Text.c_str());
    string sTmp = ConformEnvironmentName(DEFAULT_EMULATOR_FILE);

    OpenCfgDialog->DefaultExt = ExtractFileExt(sTmp.c_str());
    OpenCfgDialog->FileName = ExtractFileName(sTmp.c_str());
    OpenCfgDialog->InitialDir = ExtractFilePath(sName.c_str());
    if (OpenCfgDialog->Execute())
      {
        Edit->Text = OpenCfgDialog->FileName;
        IniHasChangedCB();
      }
}
void __fastcall TDDRPanel::OverlayImageMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   bMoveOverlay = true;
   OverlayImageMouseMove(Sender, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TDDRPanel::OverlayImageMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   bMoveOverlay = false;
}
//---------------------------------------------------------------------------


void TDDRPanel::DrawOverlay(int X, int Y)
{
  // Do nothing if nothing wanted
  if (!bOverlayUpdate) return;

  // Restore everything, then redraw if necessary
  OverlayImage->Picture->Bitmap->Assign(WholeOverlayImage);

  if (OverlayCBox->Checked)
    {
       int is = OverlaySizeSEdit->Value;
       float fs = OverlayImage->Width/1920.0;

       TRect ARect(X, Y, X + fs*11*is*8, Y + fs*is*12);

       Graphics::TBitmap* Image = new Graphics::TBitmap;
       TimeCodeImageList->GetBitmap(OverlayFGColorGroup->ItemIndex, Image);
       Image->Transparent = (OverlayBGColorGroup->ItemIndex == 1);

       OverlayImage->Canvas->StretchDraw(ARect, Image);

       // Clean up
       delete Image;

       // Save the positions if we need to change position
       OldOvlyX = X;
       OldOvlyY = Y;
    }
}

void __fastcall TDDRPanel::OverlayImageMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  if (bMoveOverlay)
    {
        Modified = true;
        DrawOverlay(X, Y);
    }
}
//---------------------------------------------------------------------------

void __fastcall TDDRPanel::ReDrawOverlay(TObject *Sender)
{
   Modified = true;
   DrawOverlay(OldOvlyX, OldOvlyY);
}
//---------------------------------------------------------------------------

void __fastcall TDDRPanel::OverlayCenterButtonClick(TObject *Sender)
{
   int NewOvlyX = (1920 - OverlaySizeSEdit->Value*11*8)/1920.*OverlayImage->Width/2;
   if (NewOvlyX == OldOvlyX) return;
   DrawOverlay(NewOvlyX, OldOvlyY);
   Modified = true;
}

//---------------------------------------------------------------------------

void __fastcall TDDRPanel::ConnectGroupBoxClick(TObject *Sender)
{
   bool bV = (ConnectGroupBox->ItemIndex == 1);
   DDRNetworkPanel->Visible = bV;
   DDRSerial232Panel->Visible = !bV;
   Modified = true;
}


//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TDDRPanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
   TParamPanel::SetAdminPriv(Value);
   if (AdminPrivilege)
     {
       DDRCfgEdit->ReadOnly = false;
       DDRCfgEdit->Color = clWindow;
       DDRCfgResetButton->Enabled = true;
       DDRCfgBrowseButton->Enabled = true;
     }
   else
     {
       DDRCfgEdit->ReadOnly = true;
       DDRCfgEdit->Color = clBtnFace;
       DDRCfgResetButton->Enabled = false;
       DDRCfgBrowseButton->Enabled = false;
     }
}

//--------------------IniHasChangedCB-------------John Mertus----Nov 2002-----

   bool TDDRPanel::IniHasChangedCB(void)

// This is called when the ini file may have changed.  Either by a change
// in the environmental variable or fromt he user.  The ChangeIniFile does
// all the real work.
//
//****************************************************************************
{
   return ChangeIniFile(DDRCfgEdit);
}

void __fastcall TDDRPanel::DDRCfgResetButtonClick(TObject *Sender)
{
    DDRCfgEdit->Text = DEFAULT_EMULATOR_FILE;
    IniHasChangedCB();
}

void __fastcall TDDRPanel::DDRCfgEditExit(TObject *Sender)
{
  IniHasChangedCB();
}
//---------------------------------------------------------------------------

void __fastcall TDDRPanel::ChangeModifyClick(TObject *Sender)
{
   Modified = true;        
}
//---------------------------------------------------------------------------

