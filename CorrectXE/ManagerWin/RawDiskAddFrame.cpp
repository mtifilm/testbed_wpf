//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RawDiskAddFrame.h"
#include "DllSupport.h"
#include "RdWriter.h"
#include "DSFileWrapper.h"
#include "ReadWriteControl.h"
#include "WriteWrapper.h"
#include "UnitCheckVideoStore.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "ParamPanelForm"

#pragma resource "*.dfm"
#include "RdWriter.h"
#include "DSFileWrapper.h"
#include "ReadWriteControl.h"

TVStorePanel *VStorePanel;
 TReadWriteControlForm *controlform;
        RdWriter *writer;
//---------------------------------------------------------------------------
__fastcall TVStorePanel::TVStorePanel(TComponent* Owner)
        : TParamPanel(Owner)
{
        selectedDisk = "rawDiskCombo";


        String searchstring = String("rd1");
        Caption = "Videostores";
        EnableDetails(false);
        writer = new RdWriter();

        controlform = new TReadWriteControlForm(Owner);
        
        controlform->Hide();

        //setup combobox
        rawDiskCombo->Items = writer->getRawDisks();
}



  __fastcall TVStorePanel::~TVStorePanel(){
      delete controlform;
        controlform = 0;
        delete writer;
        writer = 0;


 }

//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TVStorePanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
        TParamPanel::SetAdminPriv(Value);
}




void __fastcall TVStorePanel::rdButtonClick(TObject *Sender)
{
 controlform->collectiveReset();
 controlform->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TVStorePanel::rawDiskComboChange(TObject *Sender)
{
   selectedDisk = rawDiskCombo->Text;
        WriteWrapper *wrap = writer->retreiveDSInfo(&selectedDisk);
        audioEdit->Text = wrap->getAudioPath();
        vidDirEdit->Text = wrap->getMainVidDir();
        double diskinbits = double(wrap->getRawDiskSize());
        diskinbits *= 512;
        //Yeah. this is dumb. Don't know how to do exponnents. it doesn't seem to like "^"
        diskinbits /= (1024*1024*1024);   //converting to gigs.
        sizeEdit->Text = diskinbits;
        flPathEdit->Text = wrap->getFlPath();
        diskNameEdit->Text = wrap->getRawDiskDirectory();



        delete wrap;
        wrap = 0;
}

//---------------------------------------------------------------------------
 
void TVStorePanel::DetailsChanged(bool Value){

}

void __fastcall TVStorePanel::rawDiskComboClick(TObject *Sender)
{

   rawDiskCombo->Items = writer->getRawDisks();
}
//---------------------------------------------------------------------------

void __fastcall TVStorePanel::checkStoreButtonClick(TObject *Sender)
{
        TFormCheckVideoStore *checkwiz = new TFormCheckVideoStore(this);
        checkwiz->ShowModal();
}
//---------------------------------------------------------------------------

