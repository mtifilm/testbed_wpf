//---------------------------------------------------------------------------

#ifndef MTIManagerUnitH
#define MTIManagerUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ImgList.hpp>
#include "TB2Dock.hpp"
#include "TB2Item.hpp"
#include "TB2MRU.hpp"
#include "TB2Toolbar.hpp"
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <Menus.hpp>
#include "cspin.h"
#include <Mask.hpp>
#include <Graphics.hpp>
#include "VDoubleEdit.h"
#include <MPlayer.hpp>
#include "ParamPanelForm.h"
#include "VTRFrame.h"
#include "SecuriteFrame.h"
#include "DDRFrame.h"
#include "ClipFrame.h"
#include "SiteFrame.h"
#include "MTIUNIT.h"
#include "RawDiskAddFrame.h"

//  This Form manages panels of type TParamPanel
//    To create such a panel, selecte New from the Repository
//    then inherit from TParamPanel
//
//  The TParamPanels obey certain rules that allow for proper functioning
//   There are described in detail that header but include read/write and
//   modification parameters.

//---------------------------------------------------------------------------
class TMainForm : public TMTIForm
{
__published:	// IDE-managed Components
        TPanel *OptionsPanel;
        TLabel *Label1;
        TImageList *ButtonImageList;
        TImageList *SelectionImageList;
        TListView *SelectListView;
        TTBDock *TBDockTop;
        TTBToolbar *MainToolbar;
        TTBSubmenuItem *FileMenuItem;
        TTBSeparatorItem *N1;
        TTBItem *Exit1;
        TTBSubmenuItem *Edit1;
        TTBItem *CutMenu;
        TTBItem *CopyMenu;
        TTBItem *PasteMenu;
        TTBItem *SelectAll1;
        TTBSeparatorItem *N3;
        TTBItem *Clear1;
        TTBSubmenuItem *Windows1;
        TTBSubmenuItem *ToolBarMenuItem;
        TTBItem *ButtonMenuItem;
        TTBSeparatorItem *TBSeparatorItem5;
        TTBItem *RestoreSize1;
        TTBItem *RestoreAllSizes1;
        TTBSeparatorItem *N4;
        TTBItem *AlwaysonTop1;
        TTBItem *WordWrap1;
        TTBSubmenuItem *Help1;
        TTBItem *AboutMenuItem;
        TTBToolbar *ButtonToolbar;
        TTBItem *ApplySpeedButton;
        TTBSeparatorItem *TBSeparatorItem3;
        TTBItem *CutButton;
        TTBItem *CopyButton;
        TTBItem *PasteButton;
        TTBSeparatorItem *TBSeparatorItem4;
        TOpenDialog *OpenDialog;
        TBitBtn *DetailsButton;
        TDDRPanel *DDRPanel;
        TClipPanel *ClipPanel;
        TSecurityPanel *SecurityPanel;
        TSitePanel *SitePanel1;
        TVTRPanelFrame *VTRPanel;
        TPanel *SpaceLine;
        TTBItem *AdministratorMenuItem;
        TStatusBar *StatusBar;
        TBitBtn *OkButton;
        TBitBtn *QuitButton;
        TBitBtn *ApplyButton;
        TVStorePanel *VStorePanel1;
        TTBItem *PawnButton;
        void __fastcall SelectListViewInfoTip(TObject *Sender,
          TListItem *Item, AnsiString &InfoTip);
        void __fastcall SelectListViewChange(TObject *Sender,
          TListItem *Item, TItemChange Change);
        void __fastcall OkButtonClick(TObject *Sender);
        void __fastcall QuitButtonClick(TObject *Sender);
        void __fastcall ApplyButtonClick(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall CutMenuClick(TObject *Sender);
        void __fastcall CopyMenuClick(TObject *Sender);
        void __fastcall PasteMenuClick(TObject *Sender);
        void __fastcall DetailsButtonClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall PrivilegeButtonClick(TObject *Sender);
        void __fastcall FileMenuItemClick(TObject *Sender);
        void __fastcall ButtonMenuItemClick(TObject *Sender);
        void __fastcall ToolBarMenuItemClick(TObject *Sender);
        void __fastcall StatusBarDrawPanel(TStatusBar *StatusBar,
          TStatusPanel *Panel, const TRect &Rect);
        void __fastcall AboutMenuItemClick(TObject *Sender);
        void __fastcall VTRPanelTimeSenseComboBoxChange(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);

protected:
     void __fastcall WMIniChange(TMessage &Message);
     void __fastcall CMStatuBar(TMessage &Message);
        BEGIN_MESSAGE_MAP
       VCL_MESSAGE_HANDLER(WM_WININICHANGE, TMessage, WMIniChange)
       VCL_MESSAGE_HANDLER(CM_STATUSMESSAGE, TMessage, CMStatuBar)
     END_MESSAGE_MAP(TMTIForm)


private:	// User declarations
       vector<TParamPanel *> Panels;
       void UpdateAllButtons(void);
       bool m_bAdminPriv;
       void __fastcall SetAdminPriv(bool Value);
       void __fastcall ModifiedChanged(TObject *Sender);
       void __fastcall DetailsChanged(TObject *Sender);

public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
       virtual void StartForm(void);
       virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
       virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
       __property bool AdminPrivilege = {read=m_bAdminPriv, write=SetAdminPriv, default = false};
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif

