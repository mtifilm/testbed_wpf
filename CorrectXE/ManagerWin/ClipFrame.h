//---------------------------------------------------------------------------

#ifndef ClipFrameH
#define ClipFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ParamPanelForm.h"
#include <ImgList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>
//
//
// Storage for the media raw disk
typedef struct MediaDataStruct
  {
     string sMediaName;
     string sVideoMediaName;
     string sFreeListName;
     string sSectionName;
     int    iFastIO;
     int    iCount;

     MediaDataStruct(void)
      {
        iCount = 0;
        iFastIO = 16;
      }

  } MediaData;


//---------------------------------------------------------------------------
class TClipPanel : public TParamPanel
{
__published:	// IDE-managed Components
        TLabel *Label5;
        TEdit *RawDiskEdit;
        TSpeedButton *RawDiskResetButton;
        TSpeedButton *RawDiskBrowseButton;
        TSpeedButton *RawVideoFileBrowseButton;
        TEdit *RawVideoFileEdit;
        TLabel *Label12;
        TLabel *Label13;
        TEdit *FreeListEdit;
        TSpeedButton *FreeListBrowseButton;
        TListBox *MediaListBox;
        TLabel *Label11;
        TLabel *Label16;
        TRichEdit *ClipDetailsEdit;
        TBitBtn *ClipScanButton;
        TBitBtn *ListClipButton;
        TPopupMenu *ClipRawPopUp;
        TMenuItem *ClipNewMediaMenu;
        TMenuItem *ClipDeleteMediaMenu;
        TMenuItem *ClipExtendMediaMenu;
        TOpenDialog *OpenDialog;
        TLabel *VideoMediaStatusLabel;
        TLabel *FreeListStatusLabel;
        void __fastcall ClipDeleteMediaMenuClick(TObject *Sender);
        void __fastcall ClipRawPopUpPopup(TObject *Sender);
        void __fastcall ListClipButtonClick(TObject *Sender);
        void __fastcall ClipExtendMediaMenuClick(TObject *Sender);
        void __fastcall RawDiskBrowseButtonClick(TObject *Sender);
        void __fastcall RawVideoFileBrowseButtonClick(TObject *Sender);
        void __fastcall MediaListBoxClick(TObject *Sender);
        void __fastcall ClipScanButtonClick(TObject *Sender);
        void __fastcall ClipDetailsButtonClick(TObject *Sender);
        void __fastcall RawDiskEditChange(TObject *Sender);
        void __fastcall RawDiskResetButtonClick(TObject *Sender);
    void __fastcall RawVideoFileEditChange(TObject *Sender);
    void __fastcall FreeListEditChange(TObject *Sender);

protected:
        virtual void DetailsChanged(bool Value);
        void __fastcall SetAdminPriv(bool Value);

private:	// User declarations
       bool ReadRawFile(const string &Name);
       bool WriteRawFile(const string &Name);
       bool DeleteRawFileSection(const string &Name, const string &Section);

       // These process all clips
       DEFINE_FILE_SWEEPER(CheckClips, CheckClip, false, TClipPanel);
       DEFINE_FILE_SWEEPER(ListClips, ListClip, false, TClipPanel);
       bool CheckClip(string In);
       bool ListClip(string In);

       vector <MediaData> RawMedia;
       MediaData *FindMediaID(const string &sID);
       int OrphanedClips;

public:		// User declarations
       bool Read(void);
       bool Apply(void);
        __fastcall TClipPanel(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipPanel *ClipPanel;
//---------------------------------------------------------------------------
#endif
