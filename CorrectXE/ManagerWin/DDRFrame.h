//---------------------------------------------------------------------------

#ifndef DDRFrameH
#define DDRFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include "ParamPanelForm.h"
#include "VDoubleEdit.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TDDRPanel : public TParamPanel
{
__published:	// IDE-managed Components
        TPageControl *DDRPageControl;
        TTabSheet *AudioTabSheet;
        TLabel *Label38;
        TLabel *Label39;
        TLabel *Label40;
        TBevel *Bevel1;
        TLabel *Label41;
        TLabel *Label42;
        TEdit *AudioInDeviceName;
        TEdit *AudioOutDeviceName;
        VDoubleEdit *AudioSlipInEdit;
        VDoubleEdit *AudioSlipOutEdit;
        VDoubleEdit *AudioBlendEdit;
        TCheckBox *AudioWriteDiskCBox;
        TCheckBox *AudioReadDiskCBox;
        TTabSheet *GeneralTabSheet;
        TPanel *DDRSerial232Panel;
        TLabel *Label45;
        TCSpinEdit *DDRSerialPortSEdit;
        TPanel *DDRNetworkPanel;
        TLabel *Label10;
        TLabel *DDRHostLabel;
        TCSpinEdit *DDRSocketSEdit;
        TEdit *DDRHostEdit;
        TRadioGroup *ConnectGroupBox;
        TTabSheet *EmulatorTabSheet;
        TLabel *Label36;
        TLabel *Label34;
        TMaskEdit *DeviceIDEdit;
        TCSpinEdit *MaxShuttleSpeedSEdit;
        TCheckBox *ProxyFileCBox;
        TCheckBox *DifferenceFileCBox;
        TTabSheet *OverlayTabSheet;
        TLabel *Label22;
        TCheckBox *OverlayCBox;
        TPanel *OverlayPanel;
        TImage *OverlayImage;
        TCSpinEdit *OverlaySizeSEdit;
        TRadioGroup *OverlayBGColorGroup;
        TRadioGroup *OverlayFGColorGroup;
        TBitBtn *OverlayCenterButton;
        TTabSheet *VideoTabSheet;
        TLabel *Label37;
        TLabel *Label33;
        TEdit *VideoOutDeviceName;
        TCheckBox *VideoWriteDiskCBox;
        TCheckBox *VideoReadDiskCBox;
        TEdit *VideoInDeviceName;
        TEdit *DDRCfgEdit;
        TLabel *Label35;
        TSpeedButton *DDRCfgBrowseButton;
        TSpeedButton *DDRCfgResetButton;
        TImageList *TimeCodeImageList;
        TCSpinEdit *StopReactionFrameSEdit;
        TLabel *Label1;
        TCSpinEdit *InstructionFrameDelaySEdit;
        TLabel *Label2;
        TCheckBox *VideoInCBox;
        TCheckBox *VideoOutCBox;
        TCheckBox *AudioInCBox;
        TCheckBox *AudioOutCBox;
        TCheckBox *IgnoreControllerTimingCBox;
        TCSpinEdit *VideoTimeCodeDelayPlaySEdit;
        TLabel *Label3;
        TCSpinEdit *VideoTimeCodeDelayRecordSEdit;
        TLabel *Label4;
        void __fastcall OverlayImageMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall OverlayImageMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall OverlayImageMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall ReDrawOverlay(TObject *Sender);
        void __fastcall DDRCfgResetButtonClick(TObject *Sender);
        void __fastcall DDRCfgBrowseButtonClick(TObject *Sender);
        void __fastcall OverlayCenterButtonClick(TObject *Sender);
        void __fastcall ConnectGroupBoxClick(TObject *Sender);
        void __fastcall DDRCfgEditExit(TObject *Sender);
        void __fastcall ChangeModifyClick(TObject *Sender);

protected:
        virtual void __fastcall SetAdminPriv(bool Value);

private:	// User declarations
       // DDR Routines
       void ReadDDRConfiguration(void);
       void ApplyDDRConfiguration(void);
       void DrawOverlay(int X, int Y);
       Graphics::TBitmap* WholeOverlayImage;
       int OldOvlyX, OldOvlyY;
       bool bOverlayUpdate;
       bool bMoveOverlay;

public:		// User declarations
       bool Read(void);
       bool ReadFile(const string &FileName);
       bool Apply(void);
       bool IniHasChangedCB(void);

        __fastcall TDDRPanel(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDDRPanel *DDRPanel;
//---------------------------------------------------------------------------
#endif
