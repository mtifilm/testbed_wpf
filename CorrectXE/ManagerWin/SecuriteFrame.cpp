//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SecuriteFrame.h"
#include "License.h"

#include "systemid.h"
#include "cibapi/sauthunx.h"
#include "cpmp_err.h"
#include "encrypt.h"
#include "DllSupport.h"
#include "W32Serial.h"
#include "FlexServerUnit.h"
#include "LicenseInfo.h"

#ifdef DS1961S_DONGLE
#include "IniFile.h"
#include "HRTimer.h"
#include "DallasDnglLic.h"
#include "MTIWinInterface.h"
#include "AutoDetectUnit.h"
#include "systemid.h"
#endif

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "ParamPanelForm"
#pragma resource "*.dfm"
TSecurityPanel *SecurityPanel;
//---------------------------------------------------------------------------
__fastcall TSecurityPanel::TSecurityPanel(TComponent* Owner)
        : TParamPanel(Owner)
{
   Caption = "Security";
   EnableDetails(false);                // No details (Should be automatic)
   bDongleStatusChanged = true;
   // To avoid too many dongle status check, we use DongleOldStatus to mark
   // which state the status is in
   DongleOldStatus = -1;
}
//---------------------------------------------------------------------------
//**********************************Security Configuration************************************

//-----------------GetAdapterNumber---------------John Mertus----Oct 2004-----

     int TSecurityPanel::GetAdapterNumber(void)

//  This return the security number based upon the adapter number
//     2 = Parallel, 5 = new serial, 6 usb
//
//****************************************************************************
{
   // Translate the radio items into a Dallas Dongle adapter number
   switch (DongleAccessGroup->ItemIndex)
     {
        case 0: return 5;
        case 1: return 6;
        case 2: return 2;
     }

   return 6;
}

//--------------SetAdapterNumber------------------John Mertus----Oct 2004-----

     void TSecurityPanel::SetAdapterNumber(int nAdapt)

//  This sets the security number based upon the adapter number
//     1 = old style, 2 = Parallel, 5 = new serial, 6 usb
//
//****************************************************************************
{
   // Translate the radio items into a Dallas Dongle adapter number
   Modified = Modified || (DongleAccessGroup->ItemIndex != nAdapt);
   switch (nAdapt)
     {
        case 6:
          DongleAccessGroup->ItemIndex = 1;
          break;

        case 2:
          DongleAccessGroup->ItemIndex = 2;
          break;

        case 5:
          DongleAccessGroup->ItemIndex = 0;
          break;

        default:
          _MTIErrorDialog(Handle, "The Dongle adapater type is invalid//Please Auto Detect the dongle");
          DongleAccessGroup->ItemIndex = 0;
          break;
     }
}


//-----------ReadSecurityConfiguration------------John Mertus----Oct 2002-----

     bool TSecurityPanel::Read(void)

//  Read the current SYSID and dongle values
//
//****************************************************************************
{
  ullSysID = sysid64();
  ostringstream os;
  os << ullSysID;
  SystemIDEdit->Text = os.str().c_str();

  // Open the file
  CIniFile *ini = CPMPOpenLocalMachineIniFile();
  if (ini == NULL) return false;   // Error is set
  DongleFileEdit->Text = ini->ReadString(GENERAL_SECTION, "DongleConfig", DEFAULT_DONGLE_FILE).c_str();
  string sDongleName = ini->ReadFileName(GENERAL_SECTION, "DongleConfig", DEFAULT_DONGLE_FILE);

  // Done with the file
  DeleteIniFile(ini);

  // Now read the dongle file
  ini = CreateIniFile (sDongleName);
  if (ini == NULL) return false;
  DonglePortSEdit->Value = ini->ReadInteger("Dongle", "Port", 1);
  iGhostPort = DonglePortSEdit->Value;

  // New Dongle
#ifdef DS1961S_DONGLE
   // This has been removed because we decided not to save the default port numbers
  //_PortNumbers = ini->ReadIntegerList("Dongle", "PortNumbers");
 // In case the numbers are undefined
  if (_PortNumbers.size() == 0)
  {
     _PortNumbers.push_back(1);  // COM1
     _PortNumbers.push_back(0);  // USB0
     _PortNumbers.push_back(1);  // LPT1
  }

  switch (theDallasDnglVerifyLic.PortType())
  {
     case 6:         // USB
       _PortNumbers[1] = theDallasDnglVerifyLic.PortNum();
       break;

    case 5:         // Serial
       _PortNumbers[0] = theDallasDnglVerifyLic.PortNum();
       break;

    case 2:         // Parallel
       _PortNumbers[2] = theDallasDnglVerifyLic.PortNum();
       break;
  }

  // The ini file should give the same as the dallas dongle
  // But in case of a mismatch, the dongle is better
  PortSpinEdit->Value = theDallasDnglVerifyLic.PortNum();
  SetAdapterNumber(theDallasDnglVerifyLic.PortType());
  _PortNumbers[DongleAccessGroup->ItemIndex] = PortSpinEdit->Value;

  FindHostIDClick();
  LicPathEdit->Text = MTIGetLicenseLocation().c_str();

  FlexPanel->Visible = true;
#else
  FlexPanel->Visible = false;
#endif

  // close dongle config file
  DeleteIniFile(ini);
  Modified = false;
  return true;
}

//-----------ApplySecurityConfiguration-----------John Mertus----Oct 2002-----

     bool TSecurityPanel::Apply(void)

//   Apply all the changes to the ghost parameters
//
//****************************************************************************
{
    CIniFile *ini = CPMPOpenLocalMachineIniFile();
    if (ini == NULL) return false;   // Error is set

    ini->WriteString(GENERAL_SECTION, "DongleConfig", DongleFileEdit->Text.c_str());

   // The dongle file should be up to date because they are ghost parameters
   // Just update the ghosts
   iGhostPort = DonglePortSEdit->Value;
#ifdef DS1961S_DONGLE
   theDallasDnglVerifyLic.WriteConfiguration(PortSpinEdit->Value, GetAdapterNumber());
   // MPR and JAM decided not to save the defaults,  Also the defaults are saved at the
   // wrong ini file.  This needs to point to the DONGLE.CFG, not localmachine
//   ini->WriteIntegerList("Dongle", "PortNumbers", _PortNumbers);
//  Currently the license location is only change via a dialog box, so it does
//  not have to be set here.  If another path exists to setting it, this must be
//  uncommented out.
//   MTISetLicenseLocation(LicPathEdit->Text.c_str());
#endif
   DeleteIniFile(ini);
   Modified = false;
   return true;
}



//--------------IsDongleInitialized---------------John Mertus----Oct 2002-----

  bool TSecurityPanel::IsDongleInitialized(int iPort)

//  This checks to see if the dongle is initialized.
//    iPort is the port to check
//
//    Returns are true if dongle is found and initialized
//                false if not, TheError is set to the error number of why not
//                ERR_DNGL_MISSING if dongle was not found
//                ERR_DNGL_BAD_STRING if dongle is found but NOT initalized
//                other dongle errors can happen and mean above.
//
//****************************************************************************
{
#ifdef DS1961S_DONGLE
  TRACE_0(errout << " IsDongleInitialized is called, should not be accessed");
  return false;
#else
  ostringstream os;

  TCursor OldCursor = Screen->Cursor;
  Screen->Cursor = crHourGlass;

  DNGL dngl;
   sprintf (dngl.caDriverName, "\\\\.\\COM%1d", iPort);
   int iRet = DnglOpen (&dngl, SECRET_DRS|SECRET_TEMP1|SECRET_TEMP2);
   if (iRet == RET_SUCCESS)
     {
       dngl.lSubKey = 2;
       dngl.lCell = 0;
       dngl.lNValue = NUM_CELL;
       for (int lValue = 0; lValue < dngl.lNValue; lValue++) dngl.laValue[lValue] = 0;

       /* read back the cells */
       dngl.lOperation = DONGLE_RDA;
       int iErr = DnglMessage(&dngl);
       Screen->Cursor = OldCursor;   // Will not happen until next message loop
       switch (iErr)
        {
          case ERR_DNGL_BAD_STRING:
             theError.set(iErr, "Dongle is not Initialized");
             return false;

          case RET_SUCCESS:
             return true;

          case ERR_DNGL_MISSING:
             os << "No dongle found on port " << iPort;
             theError.set(iErr, os);
             return false;
         }

         // Unexpected error
         theError.set(iErr, "Error in Dongle");
         return false;
     }

   // We are here only if some major error happens
   // Should never really occure
   Screen->Cursor = OldCursor;   // Will not happen until next message loop
   theError.set(iRet, "Could not open dongle");
   return false;
  #endif
}

//-------------CheckInitAndRegistered-------------John Mertus----Oct 2002-----

         bool TSecurityPanel::CheckInitAndRegistered(int iPort)

//   This checks to see if the system is initialized and registered
//   Also gives the user the option of doing so
//     iPort is the port to check
//     Return is true if it passes
//
//****************************************************************************
{
    TCursor OldCursor = Screen->Cursor;
    Screen->Cursor = crHourGlass;
    ostringstream os;

    // Check for Initialization
    if (!IsDongleInitialized(iPort))
      {
          Screen->Cursor = OldCursor;   // Will not happen until next message loop
          // Not initialized, go check why the failure
          if ((theError.getError() == ERR_DNGL_BAD_STRING))
                {
                  // Uninitialized
                  os << "Dongle at port " << iPort << " is not initialized" << endl << "Initialize it?";
                  if (_MTIConfirmationDialog(Handle, os.str()) != MTI_DLG_OK) return false;
                  if (!InitializeDongle(iPort)) return false;
                }
          // Now check if additional error
          else if ((theError.getError() != ERR_DNGL_MISSING))
            {
              _MTIErrorDialog(Handle, (string)"Dongle unexpectedly failed\n" + theError.getFmtError());
              return false;
            }
          else
            {
              os << "Dongle not found on port " << iPort;
              _MTIErrorDialog(Handle, os.str());
              return false;
            }

      }

    Screen->Cursor = OldCursor;   // Will not happen until next message loop
    // Check for registered
    if (ReadSystemID() != SYSTEM_ID_SUCCESS)
      {
         if (_MTIConfirmationDialog(Handle, "System is not Registered, Register it?") != MTI_DLG_OK) return false;
         if (!RegisterSoftware()) return false;
      }

    return true;
}

//------------DongleDetectButtonClick-------------John Mertus----Oct 2002-----

  void __fastcall TSecurityPanel::DongleDetectButtonClick(TObject *Sender)

//  This does the dongle detection,
//   NOTE: Windows defines serial ports by names, we by port numbers
//   this should be ok, but might be a problem in the future.
//
//****************************************************************************
{
  int iRet;
  ostringstream os;

  StringList PortList = MTIGetSerialPorts();
  // TBD: Find the actual hardware list
  for (unsigned i=0; i < PortList.size(); i++)
    {
      string sPN = PortList[i];

      //Ugly, ugly, ugly, extract number from port
      string::reverse_iterator ir = find_if(sPN.rbegin(),sPN.rend(),not1(ptr_fun(::isdigit)));
      istringstream istr(sPN.substr(sPN.rend()-ir));
      int iPort;
      if ((istr >> iPort) == 0)
	{
	  _MTIErrorDialog(Handle, (string)"Error decoding Serial port number from " + sPN);
	  return;
	}

      // Port Ok, go check it
      os.str("");
      os << " Checking for Dongle on " << sPN;
      UpdateStatusBar(os.str(), 1);

      if (IsDongleInitialized(iPort))
        {
	// Dongle found, update all
          os.str("");
	  os << "Dongle found at port " << iPort;
	  _MTIInformationDialog(Handle, os.str());
	  DonglePortSEdit->Value = iPort;
          UpdateStatusBar("", 1);
	  return;
	}

      // Not initialized, go check why the failure
      if ((theError.getError() == ERR_DNGL_BAD_STRING))
            {
	      // Uninitialized
	      os << "Dongle found at port " << iPort << endl << "However, it is not Initialized" << endl << "Initialize it?";
  	      DonglePortSEdit->Value = iPort;
	      if (_MTIConfirmationDialog(Handle, os.str()) != MTI_DLG_OK) return;
              InitializeDongle(iPort);
              return;
	    }


      // Now check if additional error
      if ((theError.getError() != ERR_DNGL_MISSING))
	{
          os.str("");
          os << "Dongle detect unexpectly failed on " << iPort << endl << theError.getFmtError();
	  _MTIErrorDialog(Handle, os.str());
	}

      // Only get here if dongle was not found, try again
    }

  // Nothing found
  UpdateStatusBar("", 1);
  _MTIErrorDialog(Handle, "No dongle was found");
}

//----------------InitializeDongle----------------John Mertus----Oct 2002-----

        bool TSecurityPanel::InitializeDongle(int iPort)

//  This requests the response code to initialize a new dongle
//
//****************************************************************************
{
#ifdef DS1961S_DONGLE
  TRACE_0(errout << " IsDongleInitialized is called, should not be accessed");
  return false;
#else

  if (DongleQueryDlg == NULL) DongleQueryDlg = new TDongleQueryDlg(this);
  DongleQueryDlg->Caption = "Initializing Dongle";
  DongleQueryDlg->iPort = DonglePortSEdit->Value;
  return DongleQueryDlg->ShowModal() == mrOk;
#endif
}

//----------------RegisterSoftware-----------------John Mertus----Oct 2002-----

        bool TSecurityPanel::RegisterSoftware(void)

//  This requests the response code to registers the system
//
//****************************************************************************
{
#ifdef DS1961S_DONGLE
  TRACE_0(errout << " RegisterSoftware is called, should not be accessed");
  return false;
#else

  if (DongleQueryDlg == NULL) DongleQueryDlg = new TDongleQueryDlg(this);
  DongleQueryDlg->Caption = "Register Software";
  DongleQueryDlg->iPort = DonglePortSEdit->Value;
  return DongleQueryDlg->ShowModal() == mrOk;
#endif
}

//-------------InitDongleButtonClick--------------John Mertus----Oct 2002-----

  void __fastcall TSecurityPanel::InitDongleButtonClick(TObject *Sender)

//
//
//****************************************************************************
{
  if (IsDongleInitialized(DonglePortSEdit->Value))
    {
      if (_MTIConfirmationDialog(Handle, "Dongle is properly initialized, Reinitialize it?") != MTI_DLG_OK) return;
    }
  else if (theError.getError() != ERR_DNGL_BAD_STRING)
    {
        // Dongle is not present or another error
      _MTIErrorDialog(Handle, (string)"Dongle inquiry failed\n" + theError.getFmtError());
      return;
    }
  InitializeDongle(DonglePortSEdit->Value);
}
//---------------------------------------------------------------------------


//------------RefreshDongleButtonClick------------John Mertus----Oct 2002-----

  void __fastcall TSecurityPanel::RefreshDongleButtonClick(TObject *Sender)

//
//
//****************************************************************************
{
  int lSeconds = 0;
  
  if (!CheckInitAndRegistered(DonglePortSEdit->Value)) return;

  TCursor OldCursor = Screen->Cursor;
  Screen->Cursor = crHourGlass;

  ostringstream os;
  int iH = lSeconds/3600;
  int iS = lSeconds - 3600*iH;
  int iM = iS/60;
  iS = iS - 60*iM;

  os << iH <<  " Hours, " << iM << "." << iS << " Minutes";
  LeaseTimeLabel->Caption = os.str().c_str();
  
  Screen->Cursor = OldCursor;
}
//---------------------------------------------------------------------------

//-----------RegisterDongletButtonClick-----------John Mertus----Oct 2002-----

  void __fastcall TSecurityPanel::RegisterDongletButtonClick(TObject *Sender)

//
//
//****************************************************************************
{
   int iRet = ReadSystemID();
   if (iRet == SYSTEM_ID_SUCCESS)
     {
        if (_MTIConfirmationDialog(Handle, "System is registered, Reregister it?") != MTI_DLG_OK) return;
     }
   RegisterSoftware();
}

//---------------------------------------------------------------------------

//-------------QueryDongleButtonClick-------------John Mertus----Oct 2002-----

  void __fastcall TSecurityPanel::TimeDongleButtonClick(TObject *Sender)

//  This is called whenever time needs to be added
//
//****************************************************************************
{
#ifdef DS1961S_DONGLE
  TRACE_0(errout << " TimeDongleButtonClick is called, should not be accessed");
  return;
#else
  if (!CheckInitAndRegistered(DonglePortSEdit->Value)) return;

  if (DongleQueryDlg == NULL) DongleQueryDlg = new TDongleQueryDlg(this);
  DongleQueryDlg->Visible = false;
  DongleQueryDlg->Caption = "Add Time";
  DongleQueryDlg->iPort = DonglePortSEdit->Value;
  if (DongleQueryDlg->ShowModal() == mrOk) RefreshDongleButtonClick(Sender);
#endif
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::DongleFileEditChange(TObject *Sender)
{
    Modified = true;
    ChangeHintOnEdit(Sender);
}
//---------------------------------------------------------------------------



void __fastcall TSecurityPanel::DonglePortSEditChange(TObject *Sender)
{
   string sDongleName = DongleFileEdit->Text.c_str();
   ExpandEnviornmentString(sDongleName);
   ConformFileName(sDongleName);

   CIniFile *ini = CreateIniFile (sDongleName);
   if (ini == NULL) return;
   ini->WriteInteger("Dongle", "Port", DonglePortSEdit->Value);

   // close dongle config file
   DeleteIniFile(ini);
   Modified = true;
}

//---------------------------------------------------------------------------
       void TSecurityPanel::Restore(void)
{
   string sDongleName = DongleFileEdit->Text.c_str();
   ExpandEnviornmentString(sDongleName);
   ConformFileName(sDongleName);

   CIniFile *ini = CreateIniFile (sDongleName);
   if (ini == NULL) return;
   ini->WriteInteger("Dongle", "Port", iGhostPort);

   // close dongle config file
   DeleteIniFile(ini);
}

//---------------------------------------------------------------------------
//--------------------SetAdminPriv----------------John Mertus----Nov 2002-----

     void __fastcall TSecurityPanel::SetAdminPriv(bool Value)

//  This is called when the admin privilages change.  Enable/Disable
// all the configuration files name changes
//
//****************************************************************************
{
   TParamPanel::SetAdminPriv(Value);
   if (AdminPrivilege)
     {
       DongleFileEdit->ReadOnly = true;
       DongleFileEdit->Color = clWindow;
       DongleFileResetButton->Enabled = true;
       DongleFileBrowseButton->Enabled = true;
       PortSpinEdit->Enabled = true;
       DongleAccessGroup->Enabled = true;
       AutoDetectButton->Enabled = true;
       PortLabel->Enabled = true;
       LicPathEdit->Enabled = true;
       LicPathLabel->Enabled = true;
       LicFileBrowseButton->Enabled = true;
       NetworkButton->Enabled = true;
     }
   else
     {
       DongleFileEdit->ReadOnly = true;
       DongleFileEdit->Color = clBtnFace;
       DongleFileResetButton->Enabled = false;
       DongleFileBrowseButton->Enabled = false;
       PortSpinEdit->Enabled = false;
       DongleAccessGroup->Enabled = false;
       AutoDetectButton->Enabled = false;
       PortLabel->Enabled = false;
       LicPathEdit->Enabled = true;
       LicPathLabel->Enabled = true;
       LicFileBrowseButton->Enabled = false;
       NetworkButton->Enabled = false;
     }
}

void __fastcall TSecurityPanel::DongleFileResetButtonClick(TObject *Sender)
{
    DongleFileEdit->Text = DEFAULT_DONGLE_FILE;
}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::DongleFileBrowseButtonClick(TObject *Sender)
{
    string sName = DongleFileEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);

    DongleFileEdit->Text = GetUserFolder(Handle,
        (char *)sName.c_str(), "Local Machine Directory");
}

void __fastcall TSecurityPanel::AutoDetectButtonClick(TObject *Sender)
{
#ifdef DS1961S_DONGLE
   int PortNum = theDallasDnglVerifyLic.PortNum();
   int PortType = theDallasDnglVerifyLic.PortType();
   if (AutoDetectForm->ShowCustomModal(PortNum, PortType) != mrOk) return;

   // Update the default ports
   // ORDER IS IMPORTANT
   SetAdapterNumber(PortType);
   PortSpinEdit->Value = PortNum;
   _PortNumbers[DongleAccessGroup->ItemIndex] = PortSpinEdit->Value;
   CheckDongleButtonClick(NULL);
#endif   
}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::CheckDongleButtonClick(TObject *Sender)
{
#ifdef DS1961S_DONGLE
      CBusyCursor BusyCursor(true);

      // Force a read if the dongle parameters have changed
      if ((PortSpinEdit->Value != theDallasDnglVerifyLic.PortNum()) ||
          (GetAdapterNumber() != theDallasDnglVerifyLic.PortType()))
      {

        theDallasDnglVerifyLic.PortNum(PortSpinEdit->Value);
        theDallasDnglVerifyLic.PortType(GetAdapterNumber());
      }

      bool bRet = theDallasDnglVerifyLic.ForceReadOfDongle();
      if (!bRet)
        {
           _MTIErrorDialog(Handle, theDallasDnglVerifyLic.LastErrorMessages());
        }

      if (!UpdateDongleInfo())
         _MTIErrorDialog(Handle, AuthorizeLicenseInfo.c_str());
      FindAllLicButtonClick(NULL);

      // Because of FLEXlm wierdness, read the location to see if it changed
      MTISetLicenseLocation(MTIGetLicenseLocation());
      LicPathEdit->Text = MTIGetLicenseLocation().c_str();
#endif
}
//---------------------------------------------------------------------------

  bool TSecurityPanel::UpdateDongleInfo()
{
#ifdef DS1961S_DONGLE
   // The following forces an update
   theDallasDnglVerifyLic.IsDongleValid(true);
   string s = theDallasDnglVerifyLic.RomIDStr();
   DongleIDEdit->Text = s.c_str();
   if (s == "")
     {
       LicenseLabel->Font->Color = clRed;
       LicenseLabel->Caption = "Dongle is invalid or missing";
       AuthorizeLicenseInfo = "Error accessing the dongle\n";
       AuthorizeLicenseInfo += theDallasDnglVerifyLic.LastErrorMessages().c_str();
       LastUsedTimeLabel->Caption = "";
       bDongleStatusChanged = (DongleOldStatus != 1);
       DongleOldStatus = 1;
       return false;
     }

   // See if the there is a valid license for a dongle
   // Get the authorization string in s
   if (!theDallasDnglVerifyLic.IsAuthorizeLicenseValid(s))
    {
      LicenseLabel->Font->Color = clRed;
      LicenseLabel->Caption = "No valid Authorization license";
      AuthorizeLicenseInfo = theDallasDnglVerifyLic.LastErrorMessages().c_str();
      LastUsedTimeLabel->Caption = "";
      bDongleStatusChanged = (DongleOldStatus != 2);
      DongleOldStatus = 2;
      return false;
    }

   //  License may be valid, but dongle may not exist

    string sCust, sLic;
    if (!theDallasDnglVerifyLic.IsDongleValid(true))
      {
        LicenseLabel->Font->Color = clRed;
        LicenseLabel->Caption = "Authorize License does not match Dongle";
        AuthorizeLicenseInfo = "The vendor string of the license does not match \n"
                               "the vendor string bound to the dongle.\n";
        s = theDallasDnglVerifyLic.LastErrorMessages();
        if (s != "")
        AuthorizeLicenseInfo += (String)"\nAdditional information\n" + s.c_str();
        LastUsedTimeLabel->Caption = "";
        bDongleStatusChanged = (DongleOldStatus != 3);
        DongleOldStatus = 3;
        return false;
      }

    LicenseLabel->Font->Color = clWindowText;
    LicenseLabel->Caption =  "This dongle is properly licensed";
    theDallasDnglVerifyLic.ParseCombinedName(s, sCust, sLic);
//    s = "Licensed: " + theDallasDnglVerifyLic.TimeToString(theDallasDnglVerifyLic.GetTime(DONGLE_AUTHORIZEDATE));
//    s += "\nUpdated: " + theDallasDnglVerifyLic.TimeToString(theDallasDnglVerifyLic.GetTime(DONGLE_UPDATE_DATE));
    s = theDallasDnglVerifyLic.DumpPublic();
    s += "\nCustomer: " + sCust + "\nLicense: ";
    s += sLic;
    AuthorizeLicenseInfo = s.c_str();

    s = "Last Used: " + MTITimeToDateTimeString(theDallasDnglVerifyLic.GetTime(DONGLE_LASTUSED_DATE), false);
    LastUsedTimeLabel->Caption = s.c_str();

    bDongleStatusChanged = (DongleOldStatus != 0);
    DongleOldStatus = 0;
#endif
    return true;
}

void TSecurityPanel::FindHostIDClick(void)
{
#ifdef DS1961S_DONGLE
  MTI_UINT64 Adapter[10];
  int n = getSysID(Adapter, 10);
  if (n == 0)
    {
      HostIDEdit->Text = "Ethernet adapter not found";
      return;
    }

  ostringstream os;

  for (int i=0; i < n; i++)
    {
       unsigned char *up = (unsigned char *)(Adapter+i);
       for (int j=5; j >= 0; j--)
         os << hex << setw(2) << setfill('0') << ((int)up[j] & 0xFF);

       os << " ";
    }

  HostIDEdit->Text = os.str().c_str();
#endif
}
   String LicenseToDateString(const string &Version)

// This converts a license version to a readable date
//
//********************************************************************
{
   char str[50];
   float fVer;

   // Lets protect ourself from bad version
   if (Version[0] != '1') return "All 1. Versions";

   // See if we are a type 1.0 version
   sscanf(Version.c_str(), "%f", &fVer);
   if (fVer <= 1.000099) return "Not Versioned";

   if (Version.length() != 8) return Version.c_str();

   // Make a valid time structure, we don't care the actual time
   time_t now;
   now = time(NULL);
   tm tBlock = *localtime(&now);

   string sd = "20" + Version.substr(2,2) + "-" + Version.substr(4,2) + "-" + Version.substr(6,2);
   strptime(sd.c_str(), "%Y-%m-%d", &tBlock);

   strftime(str, sizeof(str), "%b %d, %Y", &tBlock);

   return (String)str;
}

void __fastcall TSecurityPanel::FindAllLicButtonClick(TObject *Sender)
{
#ifdef DS1961S_DONGLE
   CBusyCursor BusyCursor(true);
   CRsrcCtrl License;

   // Read all the features
   FeatureEdit->Items->Clear();
   StringList Features = License.ListAllFeatures();
   for (unsigned i=0; i < Features.size(); i++)
     FeatureEdit->Items->Add(Features[i].c_str());

   LicenseListView->Clear();
   LicenseListView->Items->BeginUpdate();
   CHRTimer HRT;
   HRT.Start();                                
   for (int i=0; i < FeatureEdit->Items->Count; i++)
      {
         string sFeature = FeatureEdit->Items->Strings[i].c_str();
         if (License.CheckOut(sFeature))
           {
              TListItem *ListItem = LicenseListView->Items->Add();
              ListItem->Caption = sFeature.c_str();
              String s = License.ExpirationDate().c_str();
              ListItem->SubItems->Add(s);
              ListItem->SubItems->Add(LicenseToDateString(License.Version()));
              License.CheckIn();
           }
         else if (License.LastErrorNumber() == LM_LONGGONE)
           {
              TListItem *ListItem = LicenseListView->Items->Add();
              ListItem->Caption = sFeature.c_str();
              ListItem->SubItems->Add("**EXPIRED**");
              ListItem->SubItems->Add(LicenseToDateString(License.Version()));
           }
         // A real error has occured, display it
         else if (License.LastErrorNumber() == LM_OLDVER)
           {
              TListItem *ListItem = LicenseListView->Items->Add();
              ListItem->Caption = sFeature.c_str();
              ListItem->SubItems->Add("**OUTDATED**");
              ListItem->SubItems->Add(LicenseToDateString(License.Version()));
           }
         else if (License.LastErrorNumber() !=  LM_NOFEATURE)
           {
             License.DisplayError("Error in feature lookup");
             break;
           }
      }
   LicenseListView->Items->EndUpdate();
#endif

}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::PortSpinEditChange(TObject *Sender)
{
    Modified = true;
}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::CheckFeatureButtonClick(TObject *Sender)
{
    LicenseInfoForm->DisplayLicenseInfo(FeatureEdit->Text.c_str());
    LicenseInfoForm->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::AuthorizeLicHelpButtonClick(
      TObject *Sender)
{
  if (AuthorizeLicenseInfo == "") return;
  _MTIInformationDialog(Handle, AuthorizeLicenseInfo.c_str());

}
//---------------------------------------------------------------------------

void __fastcall TSecurityPanel::DirtyTimerTimer(TObject *Sender)
{
   UpdateDongleInfo();
   if (bDongleStatusChanged) FindAllLicButtonClick(NULL);
}
//---------------------------------------------------------------------------





void __fastcall TSecurityPanel::LicFileBrowseButtonClick(TObject *Sender)
{
    string sName = LicPathEdit->Text.c_str();
    ExpandEnviornmentString(sName);
    ConformFileName(sName);

    string sTmp = RemoveDirSeparator(GetUserFolder(Handle, (char *)sName.c_str(), "License Directory"));
    if (sTmp == "") return;

    MTISetLicenseLocation(sTmp);
    // Because of FLEXlm wierdness, read the location to see if it changed
    CheckDongleButtonClick(NULL);
    LicPathEdit->Text = MTIGetLicenseLocation().c_str();
}
//---------------------------------------------------------------------------



void __fastcall TSecurityPanel::LicenseListViewDblClick(TObject *Sender)
{
#ifdef DS1961S_DONGLE
  CRsrcCtrl License;
  if (!License.CheckOut(LicenseListView->Selected->Caption.c_str()))
    {
       License.DisplayError("License is not valid");
       return;
    }
  else
    {
      ShowMessage(LicenseListView->Selected->Caption + " is licensed");
    }
  License.CheckIn();
#endif
   ;
}
//---------------------------------------------------------------------------


void __fastcall TSecurityPanel::DongleAccessGroupClick(TObject *Sender)
{
    PortSpinEdit->Value = _PortNumbers[DongleAccessGroup->ItemIndex];
    Modified = true;
}
//---------------------------------------------------------------------------




void __fastcall TSecurityPanel::NetworkButtonClick(TObject *Sender)
{
  // See if we are already a server name, if so just make it the default
  String loc = MTIGetLicenseLocation().c_str();
  if (loc.Pos("@") == 0)
    {
      if (FlexServerDialog->ServerNameEdit->Text == "")
        FlexServerDialog->ServerNameEdit->Text = "27000@localhost";
    }
  else
    FlexServerDialog->ServerNameEdit->Text = loc;

  if (FlexServerDialog->ShowModal() != mrOk) return;

  // User wants a change
  string sTmp = FlexServerDialog->ServerNameEdit->Text.c_str();
  MTISetLicenseLocation(sTmp);
  
  // Because of FLEXlm wierdness, read the location to see if it changed
  CheckDongleButtonClick(NULL);
  LicPathEdit->Text = MTIGetLicenseLocation().c_str();

}
//---------------------------------------------------------------------------

