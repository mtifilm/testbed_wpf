//---------------------------------------------------------------------------

#ifndef FormatDialogUnitH
#define FormatDialogUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormatDialog : public TForm
{
__published:	// IDE-managed Components
        TButton *OKButton;
        TButton *CancelButton;
        TListView *FormatListView;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormatListViewSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
private:	// User declarations
public:		// User declarations
        __fastcall TFormatDialog(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormatDialog *FormatDialog;
//---------------------------------------------------------------------------
#endif
