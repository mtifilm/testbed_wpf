//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoDetectUnit.h"
#include "W32Serial.h"
#include "IniFile.h"
#include "DallasDnglLic.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAutoDetectForm *AutoDetectForm;
//---------------------------------------------------------------------------
__fastcall TAutoDetectForm::TAutoDetectForm(TComponent* Owner)
        : TForm(Owner)
{
   _PortNum = NULL;
   _PortType = NULL;
}

//---------------------------------------------------------------------------
  int __fastcall TAutoDetectForm::ShowCustomModal(int &PortNum, int &PortType)
{
    _PortNum = &PortNum;
    _PortType = &PortType;
    UpdateStatusBar("", 0);
    return ShowModal();
}

void TAutoDetectForm::UpdateStatusBar(const string &str, int nP)
{
   StatusBar->Panels->Items[nP]->Text = str.c_str();
   Application->ProcessMessages();

}

int TAutoDetectForm::CheckSerial()
{
     MTIostringstream os;
     uchar RomID[8];
     StringList PortList = MTIGetSerialPorts();
     for (unsigned i=0; i < PortList.size(); i++)
        {
          string sPN = PortList[i];

          //Ugly, ugly, ugly, extract number from port
          string::reverse_iterator ir = find_if(sPN.rbegin(),sPN.rend(),not1(ptr_fun(::isdigit)));
          MTIistringstream istr(sPN.substr(sPN.rend()-ir));
          int iPort;
          if ((istr >> iPort) == 0)
            {
              _MTIErrorDialog(Handle, (string)"Error decoding Serial port number from " + sPN);
              return AUTO_FAILED;
            }

          // Port Ok, go check it
          os.str("");
          os << " Checking for Dongle on " << sPN;
          UpdateStatusBar(os.str(), 0);

          // Just check for one type
          int iType = 5;
           if (CheckForDongle(iPort, iType, RomID))
            {
            // Dongle found, update all
              os.str("");
              os << "An MTI v2 Dongle, ID = ";
              for (int i = 7; i >= 0; i--)
                     os << hex << setw(2) << setfill('0') << uppercase <<(int)RomID[i];

              os << endl << "was found on serial port " << iPort << endl;

              if (!VerifyMTIDongle(iPort, iType, CompanyName, LicenseName, ErrString))
                {
                    os << ErrString << endl;
                }
              else
                {
                    os << "This dongle is registered to " << endl;
                    os << "Company: " << CompanyName << endl;
                    os << "License: " << LicenseName << endl;
                }
              os << "Use this dongle?";
              // If user wants this dongle, accept it
              int dRes = _MTICancelConfirmationDialog(Handle, os.str());
              if (dRes == MTI_DLG_OK)
                {
                   *_PortNum = iPort;
                   *_PortType = iType;
                   UpdateStatusBar(os.str(), 0);
                   return AUTO_SUCCESS;
                }
              else if (dRes == MTI_DLG_CANCEL)
                {
                   return AUTO_USER_ABORT;
                }
            }

          // Get here if no dongle or user did not want to use this dongle
        }
   return AUTO_NOT_FOUND;
}

int TAutoDetectForm::CheckForOldDongle(void)
{
     MTIostringstream os;
     uchar RomID[8];
     StringList PortList = MTIGetSerialPorts();
     for (unsigned i=0; i < PortList.size(); i++)
        {
          string sPN = PortList[i];

          //Ugly, ugly, ugly, extract number from port
          string::reverse_iterator ir = find_if(sPN.rbegin(),sPN.rend(),not1(ptr_fun(::isdigit)));
          MTIistringstream istr(sPN.substr(sPN.rend()-ir));
          int iPort;
          if ((istr >> iPort) == 0)
            {
              _MTIErrorDialog(Handle, (string)"Error decoding Serial port number from " + sPN);
              return AUTO_FAILED;
            }

          // Port Ok, go check it
          os.str("");
          os << " Checking for MTI v1 Dongle on " << sPN;
          UpdateStatusBar(os.str(), 0);

          // Just check for one type
          int iType = 5;
          if (CheckFor1991Dongle(iPort, iType, RomID))
            {
            // Dongle found, update all
              os.str("");
              os << "No MTI v2 Dongle was found " << endl;
              os << "but an MTI v1 Dongle ";
              os << "was found at port " << iPort << endl;

              _MTIInformationDialog(Handle, os.str());
              return AUTO_USER_ABORT;
            }
        }
   return AUTO_NOT_FOUND;
}

int TAutoDetectForm::CheckParallel(void)
{
     MTIostringstream os;
     uchar RomID[8];
     StringList PortList = MTIGetParallelPorts();
     for (unsigned i=0; i < PortList.size(); i++)
        {
          string sPN = PortList[i];

          //Ugly, ugly, ugly, extract number from port
          string::reverse_iterator ir = find_if(sPN.rbegin(),sPN.rend(),not1(ptr_fun(::isdigit)));
          MTIistringstream istr(sPN.substr(sPN.rend()-ir));
          int iPort;
          if ((istr >> iPort) == 0)
            {
              _MTIErrorDialog(Handle, (string)"Error decoding Serial port number from " + sPN);
              return AUTO_FAILED;
            }

          // Port Ok, go check it
          os.str("");
          os << " Checking for Dongle on " << sPN;
          UpdateStatusBar(os.str(), 0);

          int iType = 2;
          if (CheckForDongle(iPort, iType, RomID))
            {
            // Dongle found, update all
              os.str("");
              os << "An MTI v2 Dongle, ID - ";
              for (int i = 7; i >= 0; i--)
                     os << hex << setw(2) << setfill('0') << uppercase <<(int)RomID[i];

              os << endl << "was found at parallel port " << iPort << endl;

              if (!VerifyMTIDongle(iPort, iType, CompanyName, LicenseName, ErrString))
                {
                    os << ErrString << endl;
                }
              else
                {
                    os << "This dongle is registered to " << endl;
                    os << "Company: " << CompanyName << endl;
                    os << "License: " << LicenseName << endl;
                }
              os << "Use this dongle?";

              // If user wants this dongle, accept it
              int dRes = _MTICancelConfirmationDialog(Handle, os.str());
              if (dRes == MTI_DLG_OK)
                {
                   *_PortNum = iPort;
                   *_PortType = iType;
                   UpdateStatusBar(os.str(), 0);
                   return AUTO_SUCCESS;
                }
              else if (dRes == MTI_DLG_CANCEL)
                {
                   return AUTO_USER_ABORT;
                }
            }

          // Get here if no dongle or user did not want to use this dongle
        }
   return AUTO_NOT_FOUND;
}

int TAutoDetectForm::CheckUSB(void)
{
     MTIostringstream os;
     uchar RomID[8];

     // We may have to loop in the future
     for (unsigned iPort=0; iPort < 2; iPort++)
        {
          // Port Ok, go check it
          os.str("");
          os << " Checking for USB Dongle";
          UpdateStatusBar(os.str(), 0);

          int iType = 6;  // USB Type
          if (CheckForDongle(iPort, iType, RomID))
            {
            // Dongle found, update all
              os.str("");
              os << "An MTI v2 Dongle, ID = ";
              for (int i = 7; i >= 0; i--)
                     os << hex << setw(2) << setfill('0') << uppercase <<(int)RomID[i];

              os << endl << " was found on USB bus port " << iPort <<endl;

              if (!VerifyMTIDongle(iPort, iType, CompanyName, LicenseName, ErrString))
                {
                    os << ErrString << endl;
                }
              else
                {
                    os << "This dongle is registered to " << endl;
                    os << "Company: " << CompanyName << endl;
                    os << "License: " << LicenseName << endl << endl;
                }
              os << "Use this dongle?";

              // If user wants this dongle, accept it
              int dRes = _MTICancelConfirmationDialog(Handle, os.str());
              if (dRes == MTI_DLG_OK)
                {
                   *_PortNum = iPort;
                   *_PortType = iType;
                   UpdateStatusBar(os.str(), 0);
                   return AUTO_SUCCESS;
                }
              else if (dRes == MTI_DLG_CANCEL)
                {
                   return AUTO_USER_ABORT;
                }
            }

          // Get here if no dongle or user did not want to use this dongle
        }
   return AUTO_NOT_FOUND;
}

void __fastcall TAutoDetectForm::OkButtonClick(TObject *Sender)
{
    CBusyCursor BusyCursor(true);

    // Start the detection
    // Serial port has two versions
    if (SerialPortCBox->Checked)
      {
         // Check for type 1
         // Find serial ports
         int Res = CheckSerial();
         if (Res == AUTO_SUCCESS)
           {
              ModalResult = mrOk;
              return;
           }
         else if (Res == AUTO_USER_ABORT)
           {
              ModalResult = mrNone;
              return;
           }
      }

     // USB has one version
     if (USBCBox->Checked)
      {
         // Find USB ports
         int Res = CheckUSB();
         if (Res == AUTO_SUCCESS)
           {
              ModalResult = mrOk;
              return;
           }
         else if (Res == AUTO_USER_ABORT)
           {
              ModalResult = mrNone;
              return;
           }
      }

     // Parallel port checking
     if (ParallelCBox->Checked)
      {
         // Find Parallel ports
         int Res = CheckParallel();
         if (Res == AUTO_SUCCESS)
           {
              ModalResult = mrOk;
              return;
           }
         else if (Res == AUTO_USER_ABORT)
           {
              ModalResult = mrNone;
              return;
           }
      }

     CheckForOldDongle();
     
     _MTIErrorDialog(Handle, "Dongle selection failed!!");
     UpdateStatusBar("Dongle Selection Failed", 0);
     ModalResult = mrNone;
     return;
}
//---------------------------------------------------------------------------

