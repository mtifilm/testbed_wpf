object LicenseInfoForm: TLicenseInfoForm
  Left = 192
  Top = 107
  BorderWidth = 8
  Caption = 'LicenseInfoForm'
  ClientHeight = 375
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    562
    375)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 200
    Width = 77
    Height = 13
    Caption = 'Checkout status'
  end
  object Label2: TLabel
    Left = 0
    Top = 8
    Width = 135
    Height = 13
    Caption = 'Individual license information'
  end
  object LicenseListView: TListView
    Left = 0
    Top = 24
    Width = 562
    Height = 161
    Anchors = [akLeft, akTop, akRight]
    Columns = <
      item
        Caption = 'Feature'
        Width = 204
      end
      item
        Alignment = taCenter
        Caption = 'Expires'
        Width = 96
      end
      item
        Caption = 'Updates Until'
        Width = 88
      end
      item
        Caption = 'Status'
        Width = 150
      end>
    ColumnClick = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
  end
  object CloseButton: TBitBtn
    Left = 240
    Top = 344
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object StatusMemo: TMemo
    Left = 0
    Top = 216
    Width = 561
    Height = 113
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'StatusMemo')
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
