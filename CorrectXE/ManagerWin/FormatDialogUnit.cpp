//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormatDialogUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#include "IniFile.h"
#include "ImageInfo.h"

TFormatDialog *FormatDialog;
//---------------------------------------------------------------------------
__fastcall TFormatDialog::TFormatDialog(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormatDialog::FormShow(TObject *Sender)
{
   // Already created
   if (FormatListView->Items->Count > 0) return;

   int formatCount = CImageInfo::queryImageFormatTypeDescriptionCount();

   string name, descr1, descr2;
   for (int i = 0; i < formatCount; ++i)
      {
        EImageFormatType imageFormatType
                        = CImageInfo::queryImageFormatTypeDescriptionTable(i);


        // Add whether it is an image format used by conventional VTRs
        if (CImageInfo::queryIsFormatConventionalVTR(imageFormatType))
          {
            // Add another row to the list view
            TListItem *ListItem = FormatListView->Items->Add();
            //  Add the ID string
            name = CImageInfo::queryImageFormatTypeName(imageFormatType);
            ListItem->Caption = name.c_str();

            //  Add Description 1 & 2
            CImageInfo::queryImageFormatTypeDescription(imageFormatType, descr1, descr2);
            ListItem->SubItems->Add(descr1.c_str());
            ListItem->SubItems->Add(descr2.c_str());
          }
      }
}
//---------------------------------------------------------------------------


void __fastcall TFormatDialog::FormatListViewSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
     OKButton->Enabled = Selected;        
}
//---------------------------------------------------------------------------

