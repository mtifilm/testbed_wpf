object FormatDialog: TFormatDialog
  Left = 192
  Top = 107
  Width = 683
  Height = 482
  Caption = 'Choose Image Format Types'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    675
    455)
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 72
    Top = 419
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Enabled = False
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 521
    Top = 419
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object FormatListView: TListView
    Left = 8
    Top = 16
    Width = 658
    Height = 388
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Image Format'
        Width = 100
      end
      item
        Caption = 'Description'
        Width = 100
      end
      item
        AutoSize = True
        Caption = 'Comment'
      end>
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Microsoft Sans Serif'
    Font.Style = []
    GridLines = True
    HideSelection = False
    MultiSelect = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    TabOrder = 2
    ViewStyle = vsReport
    OnSelectItem = FormatListViewSelectItem
  end
end
