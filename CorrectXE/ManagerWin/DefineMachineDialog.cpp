//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DefineMachineDialog.h"
#include "VTRFrame.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMachineDefineDialog *MachineDefineDialog;
//---------------------------------------------------------------------------
__fastcall TMachineDefineDialog::TMachineDefineDialog(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMachineDefineDialog::FormShow(TObject *Sender)
{
   // Save some typing
   TListItems *lvItems = VTRPanelFrame->MachineListView->Items;  // Save some typing
   int idx = CopySchemeEdit->ItemIndex;
   CopySchemeEdit->Items->Clear();
   for (int i = 0; i < lvItems->Count; i++)
     {
        TListItem *Item = lvItems->Item[i];
        String sID = Item->Caption + " " + Item->SubItems->Strings[0];
        CopySchemeEdit->Items->Add(sID);
     }

   if ((idx >= 0) && (idx < CopySchemeEdit->Items->Count))
      CopySchemeEdit->ItemIndex = idx;
   else
      if (CopySchemeEdit->Items->Count > 0) CopySchemeEdit->ItemIndex = 0;

   CopySchemeCBoxClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TMachineDefineDialog::CopySchemeCBoxClick(TObject *Sender)
{
   CopySchemeEdit->Enabled = CopySchemeCBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TMachineDefineDialog::OKButtonClick(TObject *Sender)
{
    // If we cannot parse, return null
    liUseMachine = NULL;
    if (CopySchemeEdit->ItemIndex < 0) return;
    String sID = CopySchemeEdit->Items->Strings[CopySchemeEdit->ItemIndex].SetLength(5);;
    liUseMachine = VTRPanelFrame->MachineListView->FindCaption(0,sID.c_str(), false, true, true);
}
//---------------------------------------------------------------------------


void __fastcall TMachineDefineDialog::IDHighEditKeyPress(TObject *Sender,
      char &Key)
{
   Key = UpperCase(Key)[1];
   if (Key > 'F') Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TMachineDefineDialog::IDHighEditChange(TObject *Sender)
{
    OKButton->Enabled = IDHighEdit->Text.Pos(" ") == 0;
}
//---------------------------------------------------------------------------


void __fastcall TMachineDefineDialog::QueryButtonClick(TObject *Sender)
{
  int D1, D2;
  ostringstream os;

  if (VTRPanelFrame->VTRQueryID(D1, D2))
    {
      os << setfill('0') << setw(2) << hex << D1 << ":" << D2;
      IDHighEdit->Text = UpperCase(os.str().c_str());
    }
  else
    {
      _MTIErrorDialog(Handle, (string)"Error Quering VTR\n" + (string)theError.getFmtError());
    }
}
//---------------------------------------------------------------------------

