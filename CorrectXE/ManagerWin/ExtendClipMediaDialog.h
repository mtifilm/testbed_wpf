//----------------------------------------------------------------------------
#ifndef ExtendClipMediaDialogH
#define ExtendClipMediaDialogH
//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "ClipFrame.h"
#include "MTIUNIT.h"

//----------------------------------------------------------------------------
class TClipExtendMediaDlg : public TMTIForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
        TBitBtn *DefragButton;
        TButton *Button1;
        TLabel *SpaceLabel;
        void __fastcall DefragButtonClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:
   void  UpdateFreeSpace(void);

public:
   MediaData *pMD;
   virtual __fastcall TClipExtendMediaDlg(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TClipExtendMediaDlg *ClipExtendMediaDlg;
//----------------------------------------------------------------------------
#endif    
