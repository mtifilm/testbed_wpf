//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LicenseInfo.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLicenseInfoForm *LicenseInfoForm;
//---------------------------------------------------------------------------
__fastcall TLicenseInfoForm::TLicenseInfoForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

   void TLicenseInfoForm::DisplayLicenseInfo(const string &sFeature)
{
  CRsrcCtrl License;
  ListStringList listStringListInfo;

   License.FeatureInformation(sFeature, listStringListInfo);


   LicenseListView->Clear();
   LicenseListView->Items->BeginUpdate();
   StringList slExpired = listStringListInfo[CFLIC_VERSION];
   for (unsigned i=0; i < slExpired.size(); i++)
      {
         TListItem *ListItem = LicenseListView->Items->Add();
         ListItem->Caption = sFeature.c_str();
         String s = listStringListInfo[CFLIC_EXPIRES_DATE][i].c_str();
         ListItem->SubItems->Add(s);
         s = listStringListInfo[CFLIC_VERSION][i].c_str();
         ListItem->SubItems->Add(s);
         ListItem->SubItems->Add(listStringListInfo[CFLIC_STATUS][i].c_str());
      }
   LicenseListView->Items->EndUpdate();

  // Set the status
  StatusMemo->Clear();
  if (!License.CheckOut(sFeature))
    {
       string str = License.LastErrorMessage();
       SearchAndReplace(str,"\n","\r\n");
       StatusMemo->Text = (str.c_str());
       return;
    }
  else
    {
      StatusMemo->Lines->Add(" Valid License");
    }
  License.CheckIn();



}                              



