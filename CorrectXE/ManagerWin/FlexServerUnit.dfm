object FlexServerDialog: TFlexServerDialog
  Left = 271
  Top = 182
  BorderStyle = bsDialog
  Caption = 'Select License Server'
  ClientHeight = 189
  ClientWidth = 313
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    313
    189)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 297
    Height = 129
    Shape = bsFrame
  end
  object Label3: TLabel
    Left = 32
    Top = 80
    Width = 58
    Height = 13
    Caption = 'port@server'
  end
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 220
    Height = 13
    Caption = 'Enter the location of the server by port@server'
  end
  object Label2: TLabel
    Left = 32
    Top = 40
    Width = 225
    Height = 13
    Caption = 'E.g.; 27000@localhost or 4000@mathtech.com'
  end
  object Label4: TLabel
    Left = 32
    Top = 56
    Width = 174
    Height = 13
    Caption = 'Seperate multiple servers by commas'
  end
  object OKBtn: TButton
    Left = 79
    Top = 156
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 159
    Top = 156
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object ServerNameEdit: TEdit
    Left = 30
    Top = 96
    Width = 243
    Height = 21
    Hint = 'Dongle configuration file'
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    ReadOnly = True
    TabOrder = 2
  end
end
