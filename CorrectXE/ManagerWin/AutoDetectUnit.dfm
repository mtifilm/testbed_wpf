object AutoDetectForm: TAutoDetectForm
  Left = 201
  Top = 121
  BorderStyle = bsDialog
  Caption = 'Auto Detect MTI  Dongle'
  ClientHeight = 202
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 247
    Height = 26
    Caption = 
      'Warning, Auto-Detect will access the selected ports and may dist' +
      'urb peripheral devices.'
    WordWrap = True
  end
  object CancelButton: TBitBtn
    Left = 168
    Top = 144
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 0
  end
  object OkButton: TBitBtn
    Left = 56
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Find'
    Default = True
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      04000000000080000000120B0000120B00001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
      DADAADADADADADADADAD00000ADADA00000A0F000DADAD0F000D0F000ADADA0F
      000A0000000D0000000D00F000000F00000A00F000A00F00000D00F000D00F00
      000AA0000000000000ADDA0F000A0F000ADAAD00000D00000DADDAD000DAD000
      DADAADA0F0ADA0F0ADADDAD000DAD000DADAADADADADADADADAD}
    ModalResult = 1
    TabOrder = 1
    OnClick = OkButtonClick
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 64
    Width = 257
    Height = 57
    Caption = 'Search for Adapters'
    TabOrder = 2
    object SerialPortCBox: TCheckBox
      Left = 16
      Top = 24
      Width = 57
      Height = 17
      Caption = 'Serial'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object USBCBox: TCheckBox
      Left = 104
      Top = 24
      Width = 57
      Height = 17
      Caption = 'USB'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object ParallelCBox: TCheckBox
      Left = 176
      Top = 24
      Width = 57
      Height = 17
      Caption = 'Parallel'
      TabOrder = 2
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 183
    Width = 299
    Height = 19
    Panels = <
      item
        Width = 250
      end>
  end
end
