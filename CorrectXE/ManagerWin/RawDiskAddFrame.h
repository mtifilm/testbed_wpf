//---------------------------------------------------------------------------

#ifndef RawDiskAddFrameH
#define RawDiskAddFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include "ParamPanelForm.h"
#include "VDoubleEdit.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include "IniFile.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TVStorePanel: public TParamPanel
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *Label1;
        TComboBox *rawDiskCombo;
        TPanel *Panel2;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TEdit *diskNameEdit;
        TEdit *sizeEdit;
        TEdit *audioEdit;
        TEdit *flPathEdit;
        TButton *rdButton;
        TEdit *vidDirEdit;
        TButton *checkStoreButton;
        TBevel *Bevel1;
        void __fastcall rdButtonClick(TObject *Sender);
        void __fastcall rawDiskComboChange(TObject *Sender);
        void __fastcall rawDiskComboClick(TObject *Sender);
        void __fastcall checkStoreButtonClick(TObject *Sender);


protected:
        virtual void __fastcall SetAdminPriv(bool Value);

private:	// User declarations


public:		// User declarations
        __fastcall TVStorePanel(TComponent* Owner);

        virtual void DetailsChanged(bool Value);
        String selectedDisk;
     __fastcall ~TVStorePanel();

};
//---------------------------------------------------------------------------
extern PACKAGE TVStorePanel *VStorePanel;
//---------------------------------------------------------------------------
#endif
