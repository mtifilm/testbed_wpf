/*
//      Version file for DVS SDK
*/

#ifndef _DVS_VERSION_H_
#define _DVS_VERSION_H_

#define DVS_VERSION_MAJOR	3
#define DVS_VERSION_MINOR	4
#define DVS_VERSION_PATCH	0

#ifndef DVS_VERSION_FIX

#define DVS_VERSION_FIX 4
#endif

#endif
