//---------------------------------------------------------------------------

#ifndef PDLViewerManagerH
#define PDLViewerManagerH
//---------------------------------------------------------------------------

#include "pdlviewerlibint.h"
#include "PDL.h"

#include <StdActns.hpp>    // TComponent, TOpenDialog and TSaveDialog

#include <string>
using std::string;
//---------------------------------------------------------------------------

// Forward Declarations
class CPDLRenderInterface;
class TPDLViewerForm;
class CPDLView;
//---------------------------------------------------------------------------

enum EPDLEntryAddReplaceAction
{
   PDL_ENTRY_CANCEL,
   PDL_ENTRY_ADD,
   PDL_ENTRY_REPLACE
};
//---------------------------------------------------------------------------

// CPDLViewerManager - Singleton class that manages PDL Viewer forms

class MTI_PDLVIEWERLIB_API CPDLViewerManager
{
public:
   CPDLViewerManager();
   ~CPDLViewerManager();
   void SetOwner(TComponent *newOwner);

   CPDLRenderInterface* GetAppInterface();
   void SetAppInterface(CPDLRenderInterface *newAppInterface);

   string SavePDLAs(CPDLView *pdlView, const string &pdlFilename);
   void ShowActivePDL();
   void HideActivePDL();

   bool CloseQueryAll();
   void CloseAll();

   string GetDefaultPDLFolderPath();
   bool IsActivePDLVisible();
   bool DoesActivePDLContainADestructiveEntry();
   void SetActivePDLContainsADestructiveEntry();

   string GetCurrentPDLFolderPath();
   string GetCurrentPDLFolderName();

   void BeginningPDLAddOperation();
   void EndingPDLAddOperation();
   int AddToActivePDL(CPDL &newPDLEntries);
   int ReplaceInActivePDL(CPDL &newPDLEntries);
   EPDLEntryAddReplaceAction IsRecapture();
   int SaveActivePDLIfModified();

   int GoToPDLEntry(CPDLEntry &pdlEntry);

   void SetMainWinKeyDownEventHandler(TKeyEvent newKeyDownEventHandler);
   void SetMainWinKeyUpEventHandler(TKeyEvent newKeyUpEventHandler);
   TKeyEvent GetMainWinKeyUpEventHandler();
   TKeyEvent GetMainWinKeyDownEventHandler();
   void OnClipWasDeleted(const string &deletedClipName);
   typedef list<TPDLViewerForm*> PDLViewerList;

private:
   friend class TPDLViewerForm;
   int NewPDL(TPDLViewerForm *existingPDLViewer);
   int OpenPDL(TPDLViewerForm *existingPDLViewer);
   int OpenPDL(TPDLViewerForm *existingPDLViewer, const string &pdlFilename);
   void RemovePDL(const TPDLViewerForm *pdlViewer);
   void SetActivePDL(TPDLViewerForm *newActivePDLViewer);
   TPDLViewerForm *GetActivePDL();
   bool IsActivePDL(const TPDLViewerForm *pdlViewer);
   void SetReplaceTarget(TPDLViewerForm *pdlViewer, CPDL::PDLEntry pdlEntryPos);
   void ClearReplaceTarget();
   void ClearReplaceTarget(TPDLViewerForm *pdlViewer);


private:
   TPDLViewerForm *MakePDLViewer();

private:
//   // All data members are static so any instance of the CPDLViewerManager
//   // class can access the common data
//
//   static CPDLRenderInterface *appInterface;  // Interface to application
//
//   static PDLViewerList pdlViewerList;  // List of PDL viewer forms
//
//   static TPDLViewerForm *activePDLViewer;
//
//   static TOpenDialog *pdlOpenDialog;  // Open file dialog
//   static TSaveDialog *pdlSaveDialog;  // Save file dialog
//   static TComponent *formOwner;       // owner of various new'd components
//   static string pdlFolderDefault;
//   static string pdlFolderOverride;
//
//   static TPDLViewerForm *replaceTargetPDLViewer;
//   static CPDL::PDLEntry replaceTargetPDLEntryPos;   // iterator into PDL list
//
//   static bool firstWin;
//   static int firstWinTop;
//   static int firstWinLeft;
//
//   static TKeyEvent MainWinKeyDownEventHandler;
//   static TKeyEvent MainWinKeyUpEventHandler;
};
//---------------------------------------------------------------------------
#endif
