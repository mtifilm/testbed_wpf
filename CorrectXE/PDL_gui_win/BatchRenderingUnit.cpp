//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BatchRenderingUnit.h"
#include "MTIstringstream.h"
#include "ToolManager.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIBusy"
#pragma link "GrayComboBox"
#pragma link "MTIDBMeter"
#pragma link "VTimeCodeEdit"
#pragma link "MTIProgressBar"
#pragma link "MTITrackBar"
#pragma link "PopupComboBox"
#pragma link "TimelineTrackBar"
#pragma link "IconListBox"
#pragma link "TriTimecode"
#pragma link "VDoubleEdit"
#pragma link "Knob"
#pragma resource "*.dfm"
TBatchRenderingForm *BatchRenderingForm = 0;

//---------------------------------------------------------------------------
__fastcall TBatchRenderingForm::TBatchRenderingForm(TComponent* Owner,
                                                    TListBox *newPDLViewerListBox)
   : TForm(Owner)
{
   parentPDLViewerListBox = newPDLViewerListBox;
   okayToClose = false;
   renderFlag = false;
}

//---------------------------------------------------------------------------

void __fastcall TBatchRenderingForm::StartButtonClick(TObject *Sender)
{
   StartButton->Enabled = false;
   StopButton->Enabled = true;

   CToolManager toolMgr;

   toolMgr.StartPDLRendering(renderFlag, (ErrorResponse->ItemIndex==0));
}

//---------------------------------------------------------------------------

void __fastcall TBatchRenderingForm::StopButtonClick(TObject *Sender)
{
   CToolManager toolMgr;

   toolMgr.StopPDLRendering();
}

//---------------------------------------------------------------------------

void __fastcall TBatchRenderingForm::FormShow(TObject *Sender)
{
   StartButton->Enabled = true;
   StopButton->Enabled = false;

   previousPDLRenderingStatus = PDL_RENDERING_STATUS_INVALID;

   CToolManager toolMgr;
   EventProgressBar->Max = toolMgr.GetPDLEntryCount();

   MonitorPDLRendering();
}

//---------------------------------------------------------------------------

void __fastcall TBatchRenderingForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   MonitorPDLRendering();
   CanClose = okayToClose;
   if (!okayToClose)
      Beep();
}

//---------------------------------------------------------------------------

void __fastcall TBatchRenderingForm::ProgressTimerTimer(TObject *Sender)
{
   // Monitor the PDL Rendering Progress
   MonitorPDLRendering();
}

//---------------------------------------------------------------------------

void TBatchRenderingForm::ShowProgress()
{
   CToolManager toolMgr;

   const CPDLRenderingStats *stats = toolMgr.GetPDLRenderingStats();
   if (stats->IsModified())
      {
      StatsGroupBox->Enabled = true;
      MTIostringstream ostrm;

      ostrm.str("");
      ostrm << stats->GetProcessedOkCount();
      SuccessCount->Caption = ostrm.str().c_str();

      ostrm.str("");
      ostrm << stats->GetCheckFailedCount() + stats->GetProcessedFailedCount();
      FailureCount->Caption = ostrm.str().c_str();

      ostrm.str("");
      ostrm << stats->GetSkippedCount();
      SkippedCount->Caption = ostrm.str().c_str();

      ostrm.str("");
      ostrm << stats->GetProcessedOkCount() + stats->GetCheckFailedCount()
               + stats->GetProcessedFailedCount() + stats->GetSkippedCount();
      TotalCount->Caption = ostrm.str().c_str();

      int newEntryNumber = stats->GetEntryNumber();
      EventProgressBar->Position = newEntryNumber;
      ostrm.str("");
      const char *tstr = (renderFlag) ? "Rendering" : "Previewing";
      ostrm << tstr << " event " << newEntryNumber
            << " of " << toolMgr.GetPDLEntryCount();
      EventXofY->Caption = ostrm.str().c_str();

      parentPDLViewerListBox->Invalidate();
      }

}

//---------------------------------------------------------------------------

void TBatchRenderingForm::MonitorPDLRendering()
{
   CToolManager toolMgr;

   if(!Active)
      SetFocus();    // Make sure this dialog retains focus

   ShowProgress();

   EPDLRenderingStatus newPDLRenderingStatus = toolMgr.GetPDLRenderingStatus();

   if (newPDLRenderingStatus != previousPDLRenderingStatus)
      {
      string msgStr;
      switch (newPDLRenderingStatus)
         {
         case PDL_RENDERING_STATUS_STOPPED:
            msgStr = "Ready to ";
            msgStr += (renderFlag) ? "Render" : "Preview";
            msgStr += " PDL";
            break;
         case PDL_RENDERING_STATUS_INITIALIZING:
            msgStr = "Initializing...";
            break;
         case PDL_RENDERING_STATUS_RUNNING:
            msgStr = (renderFlag) ? "Rendering..." : "Previewing...";
            break;
         case PDL_RENDERING_STATUS_COMPLETED:
            msgStr = (renderFlag) ? "Rendering" : "Previewing";
            msgStr += " completed";
            break;
         case PDL_RENDERING_STATUS_STOPPED_BY_USER:
            msgStr = (renderFlag) ? "Rendering" : "Previewing";
            msgStr += " stopped by user";
            break;
         case PDL_RENDERING_STATUS_STOPPED_BY_ERROR:
			msgStr = (renderFlag) ? "Rendering" : "Previewing";
			msgStr += " stopped by error";
			break;

		 case PDL_RENDERING_STATUS_INVALID:
			msgStr = (renderFlag) ? "Rendering" : "Previewing";
			msgStr += " Invalid status";
			break;
         }
      StatusMsg1->Caption = msgStr.c_str();

      switch (newPDLRenderingStatus)
         {
         case PDL_RENDERING_STATUS_INITIALIZING:
         case PDL_RENDERING_STATUS_RUNNING:
            BusyArrows->Busy = true;
            BusyArrows->Visible = true;
            okayToClose = false;
            EventXofY->Visible = true;
            break;
         case PDL_RENDERING_STATUS_STOPPED:
         case PDL_RENDERING_STATUS_COMPLETED:
         case PDL_RENDERING_STATUS_STOPPED_BY_USER:
		 case PDL_RENDERING_STATUS_STOPPED_BY_ERROR:
		 case PDL_RENDERING_STATUS_INVALID:
            BusyArrows->Busy = false;
            BusyArrows->Visible = false;
            okayToClose = true;
			break;
         }

      switch (newPDLRenderingStatus)
         {
         case PDL_RENDERING_STATUS_COMPLETED:
         case PDL_RENDERING_STATUS_STOPPED_BY_USER:
         case PDL_RENDERING_STATUS_STOPPED_BY_ERROR:
            parentPDLViewerListBox->Invalidate();
            StopButton->Enabled = false;
			break;

		 default:
		    break;
         }

      previousPDLRenderingStatus = newPDLRenderingStatus;
      }

}

/*

void TBatchRenderingForm::MonitorPDLRendering(EAutotoolCommand newCommand)
{
   CToolManager toolMgr;

   EAutotoolStatus newStatus = toolMgr.GetPDLRenderingStatus();

   ShowProgress();

   EToolControlState nextPDLRenderingControlState = pdlRenderingControlState;
   switch(pdlRenderingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         if (newCommand == AT_CMD_RUN)
            {
            okayToClose = false;
            toolMgr.StartPDLRendering(renderFlag,
                                      (ErrorResponse->ItemIndex== 0));
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_WAITING_TO_RUN;
            }
         break;
      case TOOL_CONTROL_STATE_WAITING_TO_RUN :
         if (newCommand == AT_CMD_STOP)
            {
            StatusMsg1->Caption = (renderFlag) ? "Rendering stopped by user"
                                               : "Previewing stopped by user";
            toolMgr.StopPDLRendering();
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         else if (newStatus == AT_STATUS_RUNNING)
            {
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_RUNNING;
            }
         break;
      case TOOL_CONTROL_STATE_RUNNING :
         if (newStatus == AT_STATUS_STOPPED)
            {
            StatusMsg1->Caption = (renderFlag) ? "Rendering completed"
                                               : "Previewing completed";
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         else if (newCommand == AT_CMD_STOP)
            {
            StatusMsg1->Caption = (renderFlag) ? "Rendering stopped by user"
                                               : "Previewing stopped by user";
            toolMgr.StopPDLRendering();
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
            }
         break;
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         if (newStatus == AT_STATUS_STOPPED)
            {
            nextPDLRenderingControlState = TOOL_CONTROL_STATE_STOPPED;
            }
         break;
      }

   if (pdlRenderingControlState != nextPDLRenderingControlState)
      {
      switch(nextPDLRenderingControlState)
         {
         case TOOL_CONTROL_STATE_STOPPED :
            BusyArrows->Busy = false;
            BusyArrows->Visible = false;
            okayToClose = true;
            break;
         case TOOL_CONTROL_STATE_WAITING_TO_RUN :
            StatusMsg1->Caption = "Initializing...";
            BusyArrows->Visible = true;
            BusyArrows->Busy = true;
            break;
         case TOOL_CONTROL_STATE_RUNNING :
            EventXofY->Visible = true;
            StartStopButton->Enabled = true;
            StatusMsg1->Caption = (renderFlag) ? "Rendering..."
                                               : "Previewing...";
            BusyArrows->Visible = true;
            BusyArrows->Busy = true;
            break;
         }
      }

   pdlRenderingControlState = nextPDLRenderingControlState;
}
*/

//----------------------------------------------------------------------

void TBatchRenderingForm::SetRender(bool newRenderFlag)
{
   renderFlag = newRenderFlag;
}

//---------------------------------------------------------------------------


