//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "IniFile.h"

#include "PDLAddReplaceDialogUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPDLAddReplaceDialog *PDLAddReplaceDialog;
//---------------------------------------------------------------------------
__fastcall TPDLAddReplaceDialog::TPDLAddReplaceDialog(TComponent* Owner)
   : TForm(Owner)
{
   // ModalResult is used to communicate which button was pressed:
   //    New button sets ModalResult to mrNo
   //    Replace button sets ModalResult to mrYes
   //    Cancel button sets ModalResult to mrCancel
}
//---------------------------------------------------------------------------
void __fastcall TPDLAddReplaceDialog::KeyPress(TObject *Sender, System::WideChar &Key)
{
   if ((char)Key == 'n' || (char)Key == 'N')
   {
      ModalResult = mrNo;
   }
   else if ((char)Key == 'r' || (char)Key == 'R')
   {
      ModalResult = mrYes;
   }
}
//---------------------------------------------------------------------------


