// ---------------------------------------------------------------------------

#pragma hdrstop

#include "PDLViewerManager.h"
#include "PDLViewerProperties.h"
#include "PDLViewerUnit.h"

#include "BatchRenderingUnit.h"
#include "FileSweeper.h"
#include "FormatColumnsDialogUnit.h"
#include "IniFile.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "PDLAddReplaceDialogUnit.h"
#include "PDLViewerManager.h"
#include "PDLViewerProperties.h"
#include "timecode.h"
#include "ToolManager.h"
#include "ShowModalDialog.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

/////////////////////////////////////////////////////////////////////////////
// Static Declarations

// All data \are local static so any instance of the CPDLViewerManager
// class can access the common data
static CPDLRenderInterface* appInterface = 0;
static CPDLViewerManager::PDLViewerList pdlViewerList;
static TPDLViewerForm* activePDLViewer = 0;
static TOpenDialog* pdlOpenDialog = 0;
static TSaveDialog* pdlSaveDialog = 0;
static TComponent* formOwner = 0;
static TPDLViewerForm* replaceTargetPDLViewer = 0;
static CPDL::PDLEntry replaceTargetPDLEntryPos;
static bool firstWin = true;
static int firstWinTop = 0;
static int firstWinLeft = 0;
static string pdlFolderOverride;

TKeyEvent MainWinKeyDownEventHandler = NULL;
TKeyEvent MainWinKeyUpEventHandler = NULL;
// ---------------------------------------------------------------------------

CPDLViewerManager::CPDLViewerManager()
{
}

CPDLViewerManager::~CPDLViewerManager()
{
}
// ---------------------------------------------------------------------------

CPDLRenderInterface* CPDLViewerManager::GetAppInterface()
{
   return appInterface;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetAppInterface(CPDLRenderInterface *newAppInterface)
{
   appInterface = newAppInterface;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetOwner(TComponent *newOwner)
{
   formOwner = newOwner;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetMainWinKeyDownEventHandler(TKeyEvent newKeyDownEventHandler)
{
   MainWinKeyDownEventHandler = newKeyDownEventHandler;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetMainWinKeyUpEventHandler(TKeyEvent newKeyUpEventHandler)
{
   MainWinKeyUpEventHandler = newKeyUpEventHandler;
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

// void CPDLViewerManager::SetDefaultPDLFolder(const string &pdlFolder)
// {
// pdlFolderDefault = pdlFolder;
// }
// ---------------------------------------------------------------------------

string CPDLViewerManager::GetDefaultPDLFolderPath()
{
   JobManager jobManager;
   return jobManager.GetJobFolderSubfolderPath(JobManager::pdlsType);
}

// ---------------------------------------------------------------------------
string CPDLViewerManager::GetCurrentPDLFolderPath()
{
   return pdlFolderOverride.empty() ? GetDefaultPDLFolderPath() : pdlFolderOverride;
}
// ---------------------------------------------------------------------------

string CPDLViewerManager::GetCurrentPDLFolderName()
{
   string folder = GetCurrentPDLFolderPath();
   return folder.empty() ? string("") : ::GetFileNameWithExt(folder);
}
// ---------------------------------------------------------------------------

TKeyEvent CPDLViewerManager::GetMainWinKeyDownEventHandler()
{
   return MainWinKeyDownEventHandler;
}

TKeyEvent CPDLViewerManager::GetMainWinKeyUpEventHandler()
{
   return MainWinKeyUpEventHandler;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::NewPDL(TPDLViewerForm *existingPDLViewer)
{
   // Create a new empty PDL
   CPDL *newPDL = new CPDL(0);

   // Create a new PDL Viewer form if we're not recycling one
   TPDLViewerForm *newPDLViewer;
   if (existingPDLViewer == 0)
   {
      // Create a PDL Viewer form
      newPDLViewer = MakePDLViewer();
   }
   else
   {
      newPDLViewer = existingPDLViewer;
   }

   // Set the PDL in the PDL Viewer Form and indicate file is not known yet
   newPDLViewer->SetPDL(newPDL);
   newPDLViewer->SetPDLFilename("");

   // Set the new form to be the active PDL
   SetActivePDL(newPDLViewer);

   // Display the form
   ShowActivePDL();

   return 0;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::OpenPDL(TPDLViewerForm *existingPDLViewer)
{
   int retVal;

   // File open dialog
   if (pdlOpenDialog == 0)
   {
      pdlOpenDialog = new TOpenDialog(formOwner);
      pdlOpenDialog->Filter = "PDL files (*.pdl)|*.pdl|All files (*.*)|*.*";
      pdlOpenDialog->FilterIndex = 1;
      pdlOpenDialog->Options << ofFileMustExist;
      pdlOpenDialog->Title = "Open PDL";
   }

   string initialFolderPath = pdlFolderOverride.empty() ? GetDefaultPDLFolderPath() : pdlFolderOverride;
   if (!initialFolderPath.empty())
   {
      pdlOpenDialog->InitialDir = initialFolderPath.c_str();
   }

   bool result = pdlOpenDialog->Execute();
   if (!result)
   {
      return 0;
   } // user cancelled out of the open dialog

   string newFilename = StringToStdString(pdlOpenDialog->FileName);

   // Remember if the user overrode the default folder
   string newFolder = ::RemoveDirSeparator(::GetFilePath(newFilename));
   pdlFolderOverride = newFolder == GetDefaultPDLFolderPath() ? string("") : newFolder;

   retVal = OpenPDL(existingPDLViewer, newFilename);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::OpenPDL(TPDLViewerForm *existingPDLViewer, const string &newPDLFilename)
{
   if (!::DoesFileExist(newPDLFilename))
   {
      MTIostringstream ostrm;
      ostrm << "Cannot find PDL file " << newPDLFilename;
      _MTIErrorDialog(Application->Handle, ostrm.str());
      return -1;
   }

   // Check if the chosen PDL file is open already
   // TBD - if the chosen PDL file is already open, then ask
   // the user whether to re-read the PDL file into the
   // existing PDL Viewer or create a new viewer.

   // Parse PDL file
   CPDL *newPDL = ParsePDL(newPDLFilename);

   if (newPDL == 0)
   {
      // Parsing failed
      _MTIErrorDialog(Application->Handle, theError.getMessage());
      return theError.getError();
   }

   TPDLViewerForm *newPDLViewer;
   if (existingPDLViewer == 0)
   {
      // Create a PDL Viewer form
      newPDLViewer = MakePDLViewer();
   }
   else
   {
      newPDLViewer = existingPDLViewer;
   }

   // Set the PDL in the PDL Viewer Form
   newPDLViewer->SetPDL(newPDL);
   newPDLViewer->SetPDLFilename(newPDLFilename);

   // remember that this PDL was read from a file
   newPDLViewer->isFile = true;

   // Set the new form to be the active PDL
   SetActivePDL(newPDLViewer);

   // Display the form
   ShowActivePDL();

   return 0;
}
// ---------------------------------------------------------------------------

string CPDLViewerManager::SavePDLAs(CPDLView *pdlView, const string &pdlFilename)
{
   if (pdlSaveDialog == 0)
   {
      pdlSaveDialog = new TSaveDialog(formOwner);
      pdlSaveDialog->Filter = "PDL files (*.pdl)|*.pdl|All files (*.*)|*.*";
      pdlSaveDialog->FilterIndex = 1;
      pdlSaveDialog->DefaultExt = "pdl";
      pdlSaveDialog->Options << ofOverwritePrompt << ofHideReadOnly << ofPathMustExist << ofNoReadOnlyReturn << ofEnableSizing;
      pdlSaveDialog->Title = "Save PDL";
   }

   if (!pdlFilename.empty())
   {
      pdlSaveDialog->InitialDir = ::RemoveDirSeparator(::GetFilePath(pdlFilename)).c_str();
      pdlSaveDialog->FileName = ::GetFileNameWithExt(pdlFilename).c_str();
   }

   bool retVal = pdlSaveDialog->Execute();
   if (!retVal)
   {
      return pdlFilename;
   } // user cancelled out of the save dialog

   string newFilename = StringToStdString(pdlSaveDialog->FileName);

   // Remember if the user overrode the default folder
   string newFolder = ::RemoveDirSeparator(::GetFilePath(newFilename));
   pdlFolderOverride = newFolder == GetDefaultPDLFolderPath() ? string("") : newFolder;
   if (pdlView != 0)
   {
      pdlView->SavePDLToFile(newFilename);
   }

   return newFilename;
}
// ---------------------------------------------------------------------------

bool CPDLViewerManager::IsActivePDLVisible()
{
   // Return true if the Active PDL Viewer exists and is visible
   return (activePDLViewer != 0 && activePDLViewer->Visible);
}
// ---------------------------------------------------------------------------

bool CPDLViewerManager::DoesActivePDLContainADestructiveEntry()
{
   if (activePDLViewer == nullptr)
   {
      return false;
   }

   return activePDLViewer->hasDestrEntry;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetActivePDLContainsADestructiveEntry()
{
   if (activePDLViewer == nullptr)
   {
      return;
   }

   activePDLViewer->hasDestrEntry = true;
}

// ---------------------------------------------------------------------------
void CPDLViewerManager::ShowActivePDL()
{
   // If there are no PDL viewers, create one.
   if (activePDLViewer == 0)
   {
      NewPDL(0);
   }

   activePDLViewer->Show();

   // If the active PDL Viewer is minimized, restore it and give it the focus
   if (activePDLViewer->WindowState == wsMinimized)
   {
      activePDLViewer->WindowState = wsNormal;
   }

   activePDLViewer->SetFocus();
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::HideActivePDL()
{
   if (activePDLViewer != 0)
   {
      activePDLViewer->Hide();
   }
}
// ---------------------------------------------------------------------------

bool CPDLViewerManager::CloseQueryAll()
{
   // Do a Close Query for each of the PDL Viewer windows
   // Return true if okay to close all of them
   // Return false if it is not okay to close any

   for (PDLViewerList::iterator iter = pdlViewerList.begin(); iter != pdlViewerList.end(); ++iter)
   {
      if (!(*iter)->CloseQuery())
      {
         return false;
      }
   }

   return true;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::CloseAll()
{
   // Make a copy of the PDL Viewer list because the original list will
   // be modified
   PDLViewerList tmpPDLViewerList(pdlViewerList);

   for (PDLViewerList::iterator iter = tmpPDLViewerList.begin(); iter != tmpPDLViewerList.end(); ++iter)
   {
      if ((*iter)->CloseQuery())
      {
         (*iter)->Close();
      }
   }
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::RemovePDL(const TPDLViewerForm *pdlViewer)
{
   for (PDLViewerList::iterator iter = pdlViewerList.begin(); iter != pdlViewerList.end(); ++iter)
   {
      if ((*iter) == pdlViewer)
      {
         pdlViewerList.erase(iter);
         break;
      }
   }

   if (IsActivePDL(pdlViewer))
   {
      // TBD - set activePDLViewer to something else
      activePDLViewer = 0;

   }
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetActivePDL(TPDLViewerForm *newActivePDLViewer)
{
   if (activePDLViewer == newActivePDLViewer)
   {
      // This PDL Viewer is already active, so nothing to do
      return;
   }

   // Deactive the current active PDL Viewer
   if (activePDLViewer != 0)
   {
      activePDLViewer->SetActive(false);
   }

   newActivePDLViewer->SetActive(true);
   activePDLViewer = newActivePDLViewer;
}
// ---------------------------------------------------------------------------

bool CPDLViewerManager::IsActivePDL(const TPDLViewerForm *pdlViewer)
{
   return (activePDLViewer != 0 && pdlViewer == activePDLViewer);
}
//---------------------------------------------------------------------------

void CPDLViewerManager::BeginningPDLAddOperation()
{
   if (activePDLViewer == nullptr)
   {
      return;
   }

   activePDLViewer->BeginningPDLAddOperation();
}
//---------------------------------------------------------------------------

void CPDLViewerManager::EndingPDLAddOperation()
{
   if (activePDLViewer == nullptr)
   {
      return;
   }

   activePDLViewer->EndingPDLAddOperation();
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::AddToActivePDL(CPDL &newPDLEntries)
{
   if (activePDLViewer == nullptr)
   {
      return -1;
   }

   activePDLViewer->AppendEntries(newPDLEntries);

   ClearReplaceTarget();

   return 0;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::ReplaceInActivePDL(CPDL &newPDLEntries)
{
   if (activePDLViewer == 0)
   {
      return -1;
   }

   activePDLViewer->ReplaceEntry(newPDLEntries, replaceTargetPDLEntryPos);

   ClearReplaceTarget();

   return 0;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::SaveActivePDLIfModified()
{
   if (activePDLViewer == 0)
   {
      return -1;
   }

   activePDLViewer->SavePDLIfModified();

   return 0;
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::SetReplaceTarget(TPDLViewerForm *pdlViewer, CPDL::PDLEntry pdlEntryPos)
{
   // Remember the PDL Viewer and PDL Entry position so that the PDL entry
   // may be replaced later
   replaceTargetPDLViewer = pdlViewer;
   replaceTargetPDLEntryPos = pdlEntryPos; // iterator into PDL entry list
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::ClearReplaceTarget()
{
   replaceTargetPDLViewer = 0; // NULL pointer indicates no target for replace
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::ClearReplaceTarget(TPDLViewerForm *pdlViewer)
{
   // Only clear the replace target if pdlViewer is the same as
   // the current replace target
   if (replaceTargetPDLViewer == pdlViewer)
   {
      ClearReplaceTarget();
   }
}
// ---------------------------------------------------------------------------

void CPDLViewerManager::OnClipWasDeleted(const string &deletedClipName)
{
   // Call each of the PDL Viewer windows's OnClipWasDeleted handler.
   for (PDLViewerList::iterator iter = pdlViewerList.begin(); iter != pdlViewerList.end(); ++iter)
   {
      (*iter)->OnClipWasDeleted(deletedClipName);
   }
}
// ---------------------------------------------------------------------------

EPDLEntryAddReplaceAction CPDLViewerManager::IsRecapture()
{
   if (replaceTargetPDLViewer == 0 || replaceTargetPDLViewer != activePDLViewer)
   {
      return PDL_ENTRY_ADD;
   }

   PDLAddReplaceDialog = new TPDLAddReplaceDialog(activePDLViewer);

   int modalResult = ShowModalDialog(PDLAddReplaceDialog);
   EPDLEntryAddReplaceAction action;
   switch (modalResult)
   {
   case mrYes:
      action = PDL_ENTRY_REPLACE;
      break;
   case mrNo:
      action = PDL_ENTRY_ADD;
      break;
   default:
      action = PDL_ENTRY_CANCEL;
      break;
   }

   delete PDLAddReplaceDialog;
   PDLAddReplaceDialog = 0;

   return action;
}
// ---------------------------------------------------------------------------

TPDLViewerForm* CPDLViewerManager::MakePDLViewer()
{
   TPDLViewerForm *newPDLViewer = new TPDLViewerForm(formOwner);
   newPDLViewer->RestoreProperties();

   // Tweak the new PDL Viewer form position so it is not in the same
   // position as the previous viewer
   if (firstWin)
   {
      firstWinTop = newPDLViewer->Top;
      firstWinLeft = newPDLViewer->Left;
      firstWin = false;
   }
   else
   {
      // Note: this can roll the PDL viewer forms off the edge of
      // the screen.  Should fix.
      int offset = pdlViewerList.size() * 20;
      newPDLViewer->Top = firstWinTop + offset;
      newPDLViewer->Left = firstWinLeft + offset;
   }

   if (FormatColumnsDialog == 0)
   {
      FormatColumnsDialog = new TFormatColumnsDialog(formOwner);
   }

   newPDLViewer->InitForm(FormatColumnsDialog);

   pdlViewerList.push_back(newPDLViewer);

   return newPDLViewer;
}
// ---------------------------------------------------------------------------

int CPDLViewerManager::GoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (appInterface == 0)
   {
      return 0;
   }

   return appInterface->GoToPDLEntry(pdlEntry);
}
// ---------------------------------------------------------------------------
