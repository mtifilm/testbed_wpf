//---------------------------------------------------------------------------

#ifndef PDLViewerTestUnitH
#define PDLViewerTestUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TB2Dock.hpp"
#include "TB2Item.hpp"
#include "TB2Toolbar.hpp"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class CPDLEntry;

//---------------------------------------------------------------------------
class TPDLViewerTestForm : public TForm
{
__published:	// IDE-managed Components
   TTBDock *TBDockTop;
   TTBToolbar *MainMenu;
   TTBSubmenuItem *FileTBSubmenuItem;
   TTBItem *NewPDLTBItem;
   TTBItem *OpenPDLTBItem;
   TTBSubmenuItem *WindowTBSubmenuItem;
   TTBItem *ShowActivePDLTBItem;
   void __fastcall OpenPDLTBItemClick(TObject *Sender);
   void __fastcall ShowActivePDLTBItemClick(TObject *Sender);
   void __fastcall WindowTBSubmenuItemClick(TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TPDLViewerTestForm(TComponent* Owner);
   int GoToPDLEntry(CPDLEntry &pdlEntry);
};
//---------------------------------------------------------------------------
extern PACKAGE TPDLViewerTestForm *PDLViewerTestForm;
//---------------------------------------------------------------------------
#endif
