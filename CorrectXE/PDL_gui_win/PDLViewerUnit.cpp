// PDLViewerUnit.cpp    Implementation PDL Viewer Form class
//
/*
$Header: /usr/local/filmroot/PDL_gui_win/PDLViewerUnit.cpp,v 1.19.4.7 2009/09/09 13:46:45 tolks Exp $
*/
/////////////////////////////////////////////////////////////////////////////

#include <vcl.h>
#pragma hdrstop

#include "PDLViewerUnit.h"
#include "BatchRenderingUnit.h"
#include "FileSweeper.h"
#include "FormatColumnsDialogUnit.h"
#include "IniFile.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "PDLAddReplaceDialogUnit.h"
#include "PDLViewerManager.h"
#include "PDLViewerProperties.h"
#include "timecode.h"
#include "ToolManager.h"
#include "ShowModalDialog.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "ColorLabel"
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TPDLViewerForm *PDLViewerForm = 0;

TColor TPDLViewerForm::inactiveColor = clBtnFace;
TColor TPDLViewerForm::activeColor = clWindow;

static SColumnInit pdlViewerColInitArray[] =
{
//  Ini Key
// -------------------------------------------------
   {0               },
   {0               },
   {0               },
   {0               },
   {"Source1"       },
   {"Src1InOut"     },
   {"Destination1"  },
   {"Dst1InOut"     },
};

/////////////////////////////////////////////////////////////////////////////
// Global Functions
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// TPDLViewerForm Class Implementation
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// TPDLViewerForm Class Implementation
//
/////////////////////////////////////////////////////////////////////////////

__fastcall TPDLViewerForm::TPDLViewerForm(TComponent* Owner)
   : TMTIForm(Owner),
     viewerProperties(new CPDLViewerProperties), pdlView(NULL),
     okayToClose(false), isActivePDL(false), isFile(false),
     hasDestrEntry(false)
{
   *DefaultIniSection = "PDLViewerFormXYZ";
}

//---------------------------------------------------------------------------

__fastcall TPDLViewerForm::~TPDLViewerForm()
{
   delete pdlView;
   pdlView = 0;
   delete viewerProperties;
   viewerProperties = 0;
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FormCreate(TObject *Sender)
{
   // Read, from the ini file, the list of most recently used PDL files
   string iniFileName = CPMPIniFileName();
#ifdef PDL_MRU
  MRU TIniFile *iniFile = new TIniFile(iniFileName.c_str());
  MRU PDLFileMRUList->LoadFromIni(iniFile, "PDLMRUList");
  MRU delete iniFile;
#endif
}
//---------------------------------------------------------------------------

bool TPDLViewerForm::ReadSAP(CIniFile *ini, const string &IniSection)
{
   // Per Larry - ALWAYS stay on top
   return true;
}
//---------------------------------------------------------------------------

// QQQ As far as I can tell, this never gets called
void TPDLViewerForm::StartForm()
{
   // Initialize the PDL Viewer's Layout

   viewerProperties->InitControls(PDLViewerHeaderControl, FormatColumnsDialog);

   viewerProperties->InitColumns(pdlViewerColInitArray);

   viewerProperties->RestoreProperties();

   CPDLViewerListItem::SetThumbnailSize(120, 90);

   if (pdlView == 0)
      {
      pdlView = new CPDLView(PDLViewerListBox, viewerProperties);
      }

   // Add dummy last item
   if (PDLViewerListBox->Count == 0)
      PDLViewerListBox->AddItem("", 0);
}

#ifdef PDL_MRU
void TPDLViewerForm::InitForm(TFormatColumnsDialog *newFormatColumnsDialog,
                              TTBMRUList *newPDLMRUList)
#else
void TPDLViewerForm::InitForm(TFormatColumnsDialog *newFormatColumnsDialog)
#endif
{
   // Initialize the PDL Viewer's Layout

   viewerProperties->InitControls(PDLViewerHeaderControl,
                                  newFormatColumnsDialog);

   viewerProperties->InitColumns(pdlViewerColInitArray);

   viewerProperties->RestoreProperties();   // probably should be elsewhere

   CPDLViewerListItem::SetThumbnailSize(120, 90);

   pdlView = new CPDLView(PDLViewerListBox, viewerProperties);

#ifdef PDL_MRU
   // Hook-up the most-recently-used list
   pdlMRUList = newPDLMRUList;
   PDL_MRUListItem1->MRUList = pdlMRUList;
   PDL_MRUListItem2->MRUList = pdlMRUList;
#endif

// Now added in CPDLView constructor
//   // Add dummy last item
//   PDLViewerListBox->AddItem("", 0);
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   if (okayToClose)
      return;

   SavePDLIfModified();

   viewerProperties->SaveProperties();

#ifdef PDL_MRU
   // Save the PDL Most-Recently-Used list
   string iniFileName = CPMPIniFileName();
   TIniFile *iniFile = new TIniFile(iniFileName.c_str());
   PDLFileMRUList->SaveToIni(iniFile, "PDLMRUList");
   delete iniFile;
#endif

   okayToClose = true;
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   CPDLViewerManager pdlViewerMgr;

   pdlViewerMgr.RemovePDL(this);
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerHeaderControlSectionResize(
      THeaderControl *HeaderControl, THeaderSection *Section)
{
   viewerProperties->ResizeColumn(Section);

   PDLViewerListBox->Invalidate();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerHeaderControlSectionEndDrag(
      TObject *Sender)
{
   // The Header Sections may have been moved, so reestablish the relationship
   // between the headers and columns
   viewerProperties->SetColumnOrder();

   viewerProperties->RebuildHeader();

   PDLViewerListBox->Invalidate();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerHeaderControlSectionDrag(
                                                   TObject *Sender,
                                                   THeaderSection *FromSection,
                                                   THeaderSection *ToSection,
                                                   bool &AllowDrag)
{
   viewerProperties->MoveColumn(FromSection, ToSection);
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerFormatColumnsClick(TObject *Sender)
{
   // Set the dialog to the existing conditions
   int columnCount = viewerProperties->GetColumnCount();
   for (int i = 0; i < columnCount; ++i)
      {
      FormatColumnsDialog->SetColumnChecked(i,
                                        viewerProperties->GetColumnVisible(i));
      }

   int result = ShowModalDialog(FormatColumnsDialog);

   bool somethingChanged = false;

   if (result == mrOk)
      {
      // User may want to change something, so let's see what

      for (int i = 0; i < columnCount; ++i)
         {
         bool checked = FormatColumnsDialog->GetColumnChecked(i);
         if (checked != viewerProperties->GetColumnVisible(i))
            {
            somethingChanged = true;
            viewerProperties->ShowHideColumn(i, checked);
            }
         }
      }

   if (somethingChanged)
      {
      PDLViewerListBox->Invalidate();
      }
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerListBoxDrawItem(TWinControl *Control,
                                                         int Index, TRect &Rect,
                                                         TOwnerDrawState State)
{
   // Hack to fix problem of this function being called as the PDL Viewer
   // form is closing.
   if (okayToClose)
      return;

   TListBox *listBox = dynamic_cast<TListBox*>(Control);
   if (listBox == 0)
      return;

   // Is this list item selected?
   bool isSelected = (State.Contains(odSelected));

   // note that we draw on the listbox�s canvas, not on the form

   TCanvas *canvas = listBox->Canvas;
   if (!isSelected)
      canvas->Brush->Color = (isActivePDL) ? activeColor : inactiveColor;
   canvas->FillRect(Rect); // clear the rectangle

   // draw a box the size of the rectangle
   canvas->Pen->Color = clDkGray;
   canvas->MoveTo(Rect.Left, Rect.Bottom-1);
   canvas->LineTo(Rect.Right, Rect.Bottom-1);

   CPDLViewerListItem *pdlListItem
                       = (CPDLViewerListItem*)(listBox->Items->Objects[Index]);
   if (pdlListItem == 0)
      return;  // dummy last entry or entry that is being deleted

   CPDLEntry *pdlEntry = pdlListItem->GetPDLEntry();
   if (pdlEntry == 0)
      return;
   pdlEntry->CheckEntry();

   // TBD: 1) Check that we are licensed for source clip's resolution

   // If the PDL's status is okay, then Check that the tool is available
   // and licensed
   if (pdlEntry->GetEntryStatus() != PDL_ENTRY_STATUS_CHECK_FAILED)
      {
      CToolManager toolMgr;
      string toolStatusMsg;
      int toolStatus = toolMgr.CheckTool(pdlEntry->toolList[0]->toolName,
                                         toolStatusMsg);
      if (toolStatus != 0)
         {
         // An error was detected, so mark this PDL Entry
         pdlEntry->SetEntryStatus(PDL_ENTRY_STATUS_CHECK_FAILED);
         pdlEntry->SetErrorCode(toolStatus);
         pdlEntry->SetErrorMessage(toolStatusMsg);
         }
      }

   int headerCount = pdlListItem->parentPDLView->viewerProperties->GetHeaderCount();
   int x = Rect.Left - 2;
   MTIostringstream ostrm;
   TRect itemRect;
   itemRect.Top = Rect.Top + 2;
   itemRect.Bottom = Rect.Bottom - 2;
   for (int i = 0; i < headerCount; ++i)
      {
      CPDLViewerColumn *column = pdlListItem->parentPDLView->viewerProperties->GetMappedColumn(i);
      int width = column->GetColumnWidth();

      itemRect.Left = x + 2;
      x += width;
      itemRect.Right = x - 2;
      DrawItem(Index, pdlListItem, column->columnID, itemRect, canvas);

      canvas->MoveTo(x, Rect.Top);
      canvas->LineTo(x, Rect.Bottom);

      }

   TFont *font = canvas->Font;
   TColor defaultColor = font->Color;

   EPDLEntryStatus entryStatus = pdlEntry->GetEntryStatus();
   if (entryStatus == PDL_ENTRY_STATUS_CHECK_FAILED
       || entryStatus == PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER
       || entryStatus == PDL_ENTRY_STATUS_PROCESSED_FAILED)
      {
      font->Color = clRed;
      canvas->TextOut(Rect.Left + 10, itemRect.Bottom - abs(font->Height) - 3,
                      pdlEntry->GetErrorMessage().c_str());
      }

   font->Color = defaultColor;
}

//---------------------------------------------------------------------------

void TPDLViewerForm::DrawItem(int rowIndex, CPDLViewerListItem *pdlListItem,
                              int columnID,
                              const TRect &itemRect, TCanvas *canvas)
{
   int retVal;
   string tmpStr;
   MTIostringstream ostrm;
   CTimecode timecode(0);
   string timecodeStr;

   CPDLEntry *pdlEntry = pdlListItem->GetPDLEntry();

   TFont *font = canvas->Font;
   TColor defaultColor = font->Color;

   int h = abs(canvas->Font->Height);

   switch(columnID)
      {
      case PDL_VIEWER_COLUMN_INDEX:
         ostrm << rowIndex+1;
         canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
         break;
      case PDL_VIEWER_COLUMN_STATUS:
         switch(pdlEntry->GetEntryStatus())
            {
            case PDL_ENTRY_STATUS_CHECK_FAILED :
               PDLStatusImageList->Draw(canvas, itemRect.Left, itemRect.Top, 3);
               break;
            case PDL_ENTRY_STATUS_PROCESSED_FAILED :
               PDLStatusImageList->Draw(canvas, itemRect.Left, itemRect.Top, 2);
               break;
            case PDL_ENTRY_STATUS_PROCESSED_OK :
               PDLStatusImageList->Draw(canvas, itemRect.Left, itemRect.Top, 0);
               break;
            case PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER :
               PDLStatusImageList->Draw(canvas, itemRect.Left, itemRect.Top, 1);
            break;

          default:
            break;
            }
         break;

      case PDL_VIEWER_COLUMN_THUMBNAIL:
         pdlListItem->LoadThumbnailBitmap();
         if (pdlListItem->thumbnailBitmap != 0)
            canvas->Draw(itemRect.Left, itemRect.Top, pdlListItem->thumbnailBitmap);
         break;
      case PDL_VIEWER_COLUMN_TOOL:
         ostrm << pdlEntry->toolList[0]->toolName;
         canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
         break;
      case PDL_VIEWER_COLUMN_SOURCE:
         ostrm << pdlEntry->srcList[0]->binPath;
         canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
         ostrm.str("");
         ostrm << pdlEntry->srcList[0]->clipName;
         canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
         break;
      case PDL_VIEWER_COLUMN_SRC_IN_OUT:
         retVal = pdlEntry->srcList[0]->GetInTimecode(timecode);
         if (retVal == 0)
            {
            timecode.getTimeString(timecodeStr);
            ostrm << timecodeStr;
            canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
            ostrm.str("");
            }
         else
            {
            // In timecode is no good
            ostrm << "??:??:??:??";
            font->Color = clRed;
            canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
            ostrm.str("");
            }
         retVal = pdlEntry->srcList[0]->GetOutTimecode(timecode);
         if (retVal == 0)
            {
            timecode.getTimeString(timecodeStr);
            ostrm << timecodeStr;
            canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
            }
         else
            {
            // Out timecode is no good
            ostrm << "??:??:??:??";
            font->Color = clRed;
            canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
            ostrm.str("");
            }
         break;
      case PDL_VIEWER_COLUMN_DESTINATION:
         if (!pdlEntry->dstList.empty())
            {
            ostrm << pdlEntry->dstList[0]->binPath;
            canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
            ostrm.str("");
            ostrm << pdlEntry->dstList[0]->clipName;
            canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
            }
         break;
      case PDL_VIEWER_COLUMN_DST_IN_OUT:
         if (!pdlEntry->dstList.empty())
            {
            retVal = pdlEntry->dstList[0]->GetInTimecode(timecode);
            if (retVal == 0)
               {
               timecode.getTimeString(timecodeStr);
               ostrm << timecodeStr;
               canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
               ostrm.str("");
               }
            else
               {
               // In timecode is no good
               ostrm << "??:??:??:??";
               font->Color = clRed;
               canvas->TextOut(itemRect.Left, itemRect.Top, ostrm.str().c_str());
               ostrm.str("");
               }
            retVal = pdlEntry->dstList[0]->GetOutTimecode(timecode);
            if (retVal == 0)
               {
               timecode.getTimeString(timecodeStr);
               ostrm << timecodeStr;
               canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
               }
            else
               {
               // Out timecode is no good
               ostrm << "??:??:??:??";
               font->Color = clRed;
               canvas->TextOut(itemRect.Left, itemRect.Top + h, ostrm.str().c_str());
               ostrm.str("");
               }
            }
         break;
      }

   font->Color = defaultColor;
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerHeaderControlSectionTrack(
      THeaderControl *HeaderControl, THeaderSection *Section, int Width,
      TSectionTrackState State)
{
   viewerProperties->ResizeColumn(Section);

   PDLViewerListBox->Invalidate();

}

//---------------------------------------------------------------------------

void TPDLViewerForm::SetPDL(CPDL *newPDL)
{
   pdlView->SetPDL(newPDL);
}

//---------------------------------------------------------------------------

void TPDLViewerForm::SetPDLFilename(const string &newPDLFilename)
{
   pdlFilename = newPDLFilename;
   UpdateWindowTitle();
}

//---------------------------------------------------------------------------

void TPDLViewerForm::SavePDLIfModified()
{
   if ((pdlView == 0) || !pdlView->IsModified())
      return;

   if (pdlFilename.empty())
      {
      SetPDLFilename(MakePDLFilename());
      if (pdlFilename.empty())
         {
         TRACE_0(errout << "ERROR: SavePDLIfModified(" << pdlFilename << ") FAILED: "
                     << "Could not create default filename");
         return;
         }
      }

   int retVal = pdlView->SavePDLToFile(pdlFilename);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: SavePDLIfModified(" << pdlFilename << ") FAILED: "
                     << GetLastSystemErrorMessage());
      }
}

//---------------------------------------------------------------------------

string TPDLViewerForm::MakePDLFilename()
{
   CPDLViewerManager pdlViewerMgr;
   string pdlFolderPath = pdlViewerMgr.GetCurrentPDLFolderPath();
   if (pdlFolderPath.empty())
      return "";

//   // Linear search for the first unused number atarting at 1.
//   // Two possible optimizations:
//   // 1) Binary search
//   // 2) Cache the last known value for the current folder (or even several folders)
//   int pdlNumber = 0;
//   bool gotIt = false;
//   string filename;
//   do
//      {
//      ++pdlNumber;
//      MTIostringstream filenameOstrm;
//      filenameOstrm << pdlFolderPath << "\\" << "PDL" << pdlNumber << ".pdl";
//      filename = filenameOstrm.str();
//      if (!::DoesFileExist(filename))
//         {
//         gotIt = true;
//         }
//      }
//   while (!gotIt);
//
//   return filename;

   JobManager jobManager;
   return jobManager.GetJobFolderFileSavePath(pdlFolderPath, "", false, "pdl");
}
//---------------------------------------------------------------------------

void TPDLViewerForm::UpdateWindowTitle()
{
   MTIostringstream formHeader;
   formHeader << "PDL Viewer - ";

   // Update the caption
   if (pdlFilename.empty())
      {
      // No file - use generic caption
      formHeader << "DRS Nova by MTI Film";
      }
   else
      {
      string folderName =
         ::RemoveDirSeparator(::GetFileLastDir(::GetFilePath(pdlFilename)));
      string ext = ::GetFileExt(pdlFilename);
      string filename = (ext == ".pdl" || ext == ".PDL")
                         ? ::GetFileName(pdlFilename)
                         : ::GetFileNameWithExt(pdlFilename);
      formHeader << folderName << " / " << filename;
      }

   if (isActivePDL)
      formHeader << " [ACTIVE]";

   Caption = formHeader.str().c_str();
}
//---------------------------------------------------------------------------

bool TPDLViewerForm::CheckForClipName(const string &clipName)
{
   // Returns true if any of the PDL entries refer to the clip.
   return pdlView->CheckForClipName(clipName);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FileNewExecute(TObject *Sender)
{
   // Create a new PDL and PDL Viewer form
   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.NewPDL(0);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FileOpenExecute(TObject *Sender)
{
   SavePDLIfModified(); // Not really necessaary... just paranoid.

   // Open a different PDL in this viewer.
   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.OpenPDL(this);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FileSaveAsExecute(TObject *Sender)
{
   // Pops up a Save As dialog and save the PDL to tha selected location.
   CPDLViewerManager pdlViewerMgr;
   string filename = pdlFilename.empty() ? MakePDLFilename() : pdlFilename;
   string newFilename = pdlViewerMgr.SavePDLAs(pdlView, filename);
   SetPDLFilename(newFilename);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FileCloseAllExecute(TObject *Sender)
{
   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.CloseAll();
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FileCloseExecute(TObject *Sender)
{
   SavePDLIfModified(); // Not really necessaary... just paranoid.

   Close();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditSelectAllExecute(TObject *Sender)
{
   PDLViewerListBox->SelectAll();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditDeleteExecute(TObject *Sender)
{
   pdlView->EditDelete();
   SavePDLIfModified();

   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.ClearReplaceTarget(this);
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditCutExecute(TObject *Sender)
{
   pdlView->EditCut();
   SavePDLIfModified();

   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.ClearReplaceTarget(this);
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditCopyExecute(TObject *Sender)
{
   pdlView->EditCopy();
   SavePDLIfModified();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditPasteExecute(TObject *Sender)
{
   pdlView->EditPaste();
   SavePDLIfModified();

   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.ClearReplaceTarget(this);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditClearStatusExecute(TObject *Sender)
{
   // Clear the status of the selected PDL entries
   pdlView->EditClearStatus();
   SavePDLIfModified();
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::EditClearStatusAllExecute(TObject *Sender)
{
   // Clear the status of all of the entries in the PDL
   pdlView->EditClearStatusAll();
   SavePDLIfModified();
}
//---------------------------------------------------------------------------

void TPDLViewerForm::BeginningPDLAddOperation()
{
   PDLViewerListBox->Visible = false;
   PDLViewerUpdateInProgressPanel->Visible = true;
   PdlUpdateInProgressBlinkTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void TPDLViewerForm::EndingPDLAddOperation()
{
   PdlUpdateInProgressBlinkTimer->Enabled = false;
   PDLViewerListBox->Visible = true;
   PDLViewerUpdateInProgressPanel->Visible = false;
}
//---------------------------------------------------------------------------

void TPDLViewerForm::AppendEntries(CPDL &newEntriesPDL)
{
   pdlView->AppendEntries(newEntriesPDL);
}
//---------------------------------------------------------------------------

void TPDLViewerForm::ReplaceEntry(CPDL &newEntriesPDL,
                                  CPDL::PDLEntry targetPosition)
{
   pdlView->ReplaceEntry(newEntriesPDL, targetPosition);
   SavePDLIfModified();
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerListBoxDblClick(TObject *Sender)
{
   int retVal;

   TListBox *listBox = dynamic_cast<TListBox*>(Sender);
   if (listBox == 0)
      return;

   if (listBox->SelCount < 1)
      return;  // nothing selected

   if (listBox->SelCount > 1)
      {
      Beep();
      return;  // more than 1 selected
      }

   for (int i = 0; i < listBox->Count; ++i)
      {
      if (listBox->Selected[i])
         {
         CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
         if (pdlListItem == 0)
            continue;   // "object" is NULL, so skip this

         // Remember the position of this double click for possible replacement
         // later.  Even if GoToPDLEntry fails, we want to set the replace
         // position so that the user can correct a bad PDL entry and then
         // then replace it.
         CPDLViewerManager pdlViewerMgr;
         pdlViewerMgr.SetReplaceTarget(this, pdlListItem->pdlEntry);

         CPDLEntry *pdlEntry = pdlListItem->GetPDLEntry();
         retVal = pdlViewerMgr.GoToPDLEntry(*pdlEntry);
         if (retVal != 0)
            return;

         //CToolManager toolManager;
			//toolManager.SetMainWindowFocus();

         return;
         }
      }

	SavePDLIfModified();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PreviewAllExecute(TObject *Sender)
{
   //RenderAllExecute_WTF(false);
   bool doRender = false;

   CToolManager toolMgr;

   if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
      return;

   CPDL *pdl = pdlView->pdl;
   if (pdl == 0 || pdl->GetEntryCount() == 0)
      {
      Beep();  // should really be an error message
      return;  // No PDL to render
      }

   CPDLViewerManager pdlViewerMgr;
   CPDLRenderInterface *appInterface = pdlViewerMgr.GetAppInterface();
   if (appInterface == 0)
      return;  // shouldn't ever happen

   if (BatchRenderingForm != 0)
      return;  // form already created, shouldn't ever happen

   BatchRenderingForm = new TBatchRenderingForm(this, PDLViewerListBox);

   BatchRenderingForm->SetRender(doRender);

   // Let the Tool Manager know about the PDL that it is about to render
   toolMgr.InitializePDLRendering(appInterface, pdl);

   // Show Batch Rendering dialog, which will start the actual rendering
   ShowModalDialog(BatchRenderingForm);

   delete BatchRenderingForm;
   BatchRenderingForm = 0;

   // Let active tool clean up after PDL execution finishes
   toolMgr.onPDLExecutionComplete();
}

void __fastcall TPDLViewerForm::RenderAllExecute(TObject *Sender)
{
   //RenderAllExecute_WTF(true);
   bool doRender = true;

   CToolManager toolMgr;

   if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
      return;

   CPDL *pdl = pdlView->pdl;
   if (pdl == 0 || pdl->GetEntryCount() == 0)
      {
      Beep();  // should really be an error message
      return;  // No PDL to render
      }

   CPDLViewerManager pdlViewerMgr;
   CPDLRenderInterface *appInterface = pdlViewerMgr.GetAppInterface();
   if (appInterface == 0)
      return;  // shouldn't ever happen

   if (BatchRenderingForm != 0)
      return;  // form already created, shouldn't ever happen

   BatchRenderingForm = new TBatchRenderingForm(this, PDLViewerListBox);

   BatchRenderingForm->SetRender(doRender);

   // Let the Tool Manager know about the PDL that it is about to render
   toolMgr.InitializePDLRendering(appInterface, pdl);

   // Show Batch Rendering dialog, which will start the actual rendering
   ShowModalDialog(BatchRenderingForm);

   delete BatchRenderingForm;
   BatchRenderingForm = 0;

   // Let active tool clean up after PDL execution finishes
   toolMgr.onPDLExecutionComplete();
   SavePDLIfModified();
}

#if 0
void TPDLViewerForm::RenderAllExecute_WTF(bool doRender)
{
   CToolManager toolMgr;

   if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
      return;

   CPDL *pdl = pdlView->pdl;
   if (pdl == 0 || pdl->GetEntryCount() == 0)
      {
      Beep();  // should really be an error message
      return;  // No PDL to render
      }

   CPDLViewerManager pdlViewerMgr;
   CPDLRenderInterface *appInterface = pdlViewerMgr.GetAppInterface();
   if (appInterface == 0)
      return;  // shouldn't ever happen

   if (BatchRenderingForm != 0)
      return;  // form already created, shouldn't ever happen

   BatchRenderingForm = new TBatchRenderingForm(this, PDLViewerListBox);

   BatchRenderingForm->SetRender(doRender);

   // Let the Tool Manager know about the PDL that it is about to render
   toolMgr.InitializePDLRendering(appInterface, pdl);

   // Show Batch Rendering dialog, which will start the actual rendering
   ShowModalDialog(BatchRenderingForm);

   delete BatchRenderingForm;
   BatchRenderingForm = 0;

   // Let active tool clean up after PDL execution finishes
   toolMgr.onPDLExecutionComplete();
}
#endif
//---------------------------------------------------------------------------
#ifdef PDL_MRU

void TPDLViewerForm::AddToPDLFileMRU(const string &newPDLFilename)
{
   MRU PDLFileMRUList->Add(newPDLFilename.c_str());
}

#endif
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::ActivateTBItemClick(TObject *Sender)
{
   CPDLViewerManager pdlViewerMgr;
   pdlViewerMgr.SetActivePDL(this);
}

//---------------------------------------------------------------------------

void TPDLViewerForm::SetActive(bool newFlag)
{
   bool redraw = (isActivePDL != newFlag);

   isActivePDL = newFlag;

   PDLViewerListBox->Color = (isActivePDL) ? activeColor : inactiveColor;

   if (redraw)
      PDLViewerListBox->Invalidate();

   UpdateWindowTitle();
}
//---------------------------------------------------------------------------

bool TPDLViewerForm::AreWeInterestedInThisKeyCombo(WORD &Key, TShiftState Shift)
{
   bool retVal = false;

   //////////////////////////////////////////////////////////////////////
   // We keep all naked ALT key events and all regular keys that are
   // are pressed when only ALT is down because we want them to activate
   // our menu shortcuts, not the main window's

   if (!(Shift.Contains(ssCtrl) || Shift.Contains(ssShift)) && Key == MTK_ALT)
      retVal = true;

   else if (!(Shift.Contains(ssCtrl) || Shift.Contains(ssShift))
            && Shift.Contains(ssAlt))
      {
      switch (Key)
         {
         case MTK_SEMICOLON:
         case MTK_COMMA:
         case MTK_MINUS:
         case MTK_PERIOD:
         case MTK_SLASH:
         case MTK_GRAVE:
         case MTK_BRACKETLEFT:
         case MTK_BACKSLASH:
         case MTK_BRACKETRIGHT:
         case MTK_APOSTROPHE:
         case MTK_EQUAL:
         case MTK_0:
         case MTK_1:
         case MTK_2:
         case MTK_3:
         case MTK_4:
         case MTK_5:
         case MTK_6:
         case MTK_7:
         case MTK_8:
         case MTK_9:
         case MTK_A:
         case MTK_B:
         case MTK_C:
         case MTK_D:
         case MTK_E:
         case MTK_F:
         case MTK_G:
         case MTK_H:
         case MTK_I:
         case MTK_J:
         case MTK_K:
         case MTK_L:
         case MTK_M:
         case MTK_N:
         case MTK_O:
         case MTK_P:
         case MTK_Q:
         case MTK_R:
         case MTK_S:
         case MTK_T:
         case MTK_U:
         case MTK_V:
         case MTK_W:
         case MTK_X:
         case MTK_Y:
         case MTK_Z:
            {
            retVal = true;;
            }
         }
      }

   //////////////////////////////////////////////////////////////////////
   // Protect keys used by controls in the form
   //
   // If you want to see what to add here if various controls are added
   // to the form, go to BaseToolUnit.cpp
   //
   else if (!(Shift.Contains(ssCtrl) || Shift.Contains(ssAlt) ||
              Shift.Contains(ssShift)))
      {
      switch (Key)
         {
         case MTK_UP:
         case MTK_DOWN:
         case MTK_LEFT:
         case MTK_RIGHT:
            {
            // We want to keep all naked up/dowm arrow key events for our
            // main list, and we keep the other two arrows for consistency;
            // In fact, let's focus on the list as well
            FocusControl(PDLViewerListBox);
            if (PDLViewerListBox->ItemIndex == -1)
               {
               // If nothing in the list was selected, just select the
               // first list entry and zero the Key to indicate it's handled
               PDLViewerListBox->ItemIndex = 0;
               Key = 0;
               }

            retVal = true;;
            }
         break;

         // That's it! There are no other controls!
         }
      }

   //////////////////////////////////////////////////////////////////////

   return retVal;
}

//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
//   DBTRACE2("KEY DOWN: ", Key);
   if (!AreWeInterestedInThisKeyCombo(Key, Shift))
      {
      // We'll pass this one through to the main window
      CPDLViewerManager pdlViewerMgr;
      (pdlViewerMgr.GetMainWinKeyDownEventHandler())(Sender, Key, Shift);
      }
   else if (!(Shift.Contains(ssCtrl) || Shift.Contains(ssShift))
            && Key == MTK_ALT)
      {
      // Enable menu shortcuts
      FileTBSubmenuItem->Caption = "&File";
      EditMenuItem->Caption = "&Edit";
      ViewTBSubmenuItem->Caption = "&View";
      }
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::FormKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   CPDLViewerManager pdlViewerMgr;

//   DBTRACE2("KEY UP: ", Key);

   // Window disappears on Ctrl-comma
   if (Shift.Contains(ssCtrl) && Key == MTK_COMMA)
      {
#if 0
      // Haha wtf is this? Make the form disappear immediately when you show
      // with the Ctrl-< hot key.
      Hide();
#endif
      }

   else if (!AreWeInterestedInThisKeyCombo(Key, Shift))
      {
      // We'll pass this one through to main window
      CPDLViewerManager pdlViewerMgr;
      (pdlViewerMgr.GetMainWinKeyUpEventHandler())(Sender, Key, Shift);
      }

   else if (!(Shift.Contains(ssCtrl) || Shift.Contains(ssShift))
            && Key == MTK_ALT)
      {
      // Disable menu shortcuts so we can do ALT-Key combos
      FileTBSubmenuItem->Caption = "File";
      EditMenuItem->Caption = "Edit";
      ViewTBSubmenuItem->Caption = "View";
      }
}
//---------------------------------------------------------------------------
void __fastcall TPDLViewerForm::PdlUpdateInProgressBlinkTimerTimer(TObject *Sender)

{
   auto oldColor = PDLViewerUpdateInProgressLabel->Font->Color;
   auto newColor = (oldColor == clBlack) ? clRed : clBlack;
   PDLViewerUpdateInProgressLabel->Font->Color = newColor;
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerForm::PDLViewerListBoxKeyPress(TObject *Sender, System::WideChar &Key)

{
//	DBTRACE2("KEY PRESSED: ", Key);
	if (Key == Char(VK_RETURN))
	{
		PDLViewerListBoxDblClick(Sender);
   }
}
//---------------------------------------------------------------------------

void TPDLViewerForm::OnClipWasDeleted(const string &deletedClipName)
{
   if (pdlView && CheckForClipName(deletedClipName))
   {
      pdlView->RefreshPDL();
   }
}
//---------------------------------------------------------------------------


