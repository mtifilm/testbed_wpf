object BatchRenderingForm: TBatchRenderingForm
  Left = 190
  Top = 179
  BorderStyle = bsToolWindow
  Caption = 'Batch Rendering'
  ClientHeight = 336
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusMsg1: TLabel
    Left = 8
    Top = 16
    Width = 3
    Height = 13
  end
  object EventXofY: TLabel
    Left = 8
    Top = 80
    Width = 111
    Height = 13
    Caption = 'Rendering event X of Y'
    Visible = False
  end
  object StartButton: TButton
    Left = 16
    Top = 296
    Width = 75
    Height = 25
    Hint = 'Stop rendering'
    Caption = 'Start'
    TabOrder = 0
    OnClick = StartButtonClick
  end
  object BusyArrows: TMTIBusy
    Left = 8
    Top = 41
    Width = 32
    Height = 32
  end
  object EventProgressBar: TProgressBar
    Left = 8
    Top = 96
    Width = 225
    Height = 17
    TabOrder = 2
  end
  object ErrorResponse: TRadioGroup
    Left = 8
    Top = 216
    Width = 145
    Height = 65
    Caption = 'On Event Error'
    ItemIndex = 0
    Items.Strings = (
      'Stop on first error'
      'Continue to next event')
    TabOrder = 3
  end
  object StopButton: TButton
    Left = 112
    Top = 296
    Width = 75
    Height = 25
    Hint = 'Stop rendering'
    Caption = 'Stop'
    TabOrder = 4
    OnClick = StopButtonClick
  end
  object StatsGroupBox: TGroupBox
    Left = 8
    Top = 120
    Width = 121
    Height = 89
    Caption = 'Events Processed'
    Enabled = False
    TabOrder = 5
    object Label1: TLabel
      Left = 11
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Success:'
    end
    object Label2: TLabel
      Left = 21
      Top = 32
      Width = 34
      Height = 13
      Caption = 'Failure:'
    end
    object Label3: TLabel
      Left = 28
      Top = 72
      Width = 27
      Height = 13
      Caption = 'Total:'
    end
    object SuccessCount: TLabel
      Left = 64
      Top = 16
      Width = 42
      Height = 13
      Caption = '######'
    end
    object FailureCount: TLabel
      Left = 64
      Top = 32
      Width = 42
      Height = 13
      Caption = '######'
    end
    object TotalCount: TLabel
      Left = 64
      Top = 72
      Width = 42
      Height = 13
      Caption = '######'
    end
    object SkippedCount: TLabel
      Left = 64
      Top = 48
      Width = 42
      Height = 13
      Caption = '######'
    end
    object Label5: TLabel
      Left = 13
      Top = 48
      Width = 42
      Height = 13
      Caption = 'Skipped:'
    end
  end
  object ProgressTimer: TTimer
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 200
    Top = 296
  end
end
