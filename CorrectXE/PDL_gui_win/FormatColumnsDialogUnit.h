//---------------------------------------------------------------------------

#ifndef FormatColumnsDialogUnitH
#define FormatColumnsDialogUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>

#include <string>
using std::string;

//---------------------------------------------------------------------------
class TFormatColumnsDialog : public TForm
{
__published:	// IDE-managed Components
   TButton *OKButton;
   TButton *CancelButton;
   TCheckListBox *ColumnsCheckListBox;
   void __fastcall OKButtonClick(TObject *Sender);
   void __fastcall CancelButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TFormatColumnsDialog(TComponent* Owner);

   bool GetColumnChecked(int index) const;
   int GetColumnCount() const;
   bool GetColumnEnabled(int index) const;
   string GetColumnName(int index) const;
   void SetColumnChecked(int index, bool newChecked);
   void SetColumnEnabled(int index, bool newEnabled);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormatColumnsDialog *FormatColumnsDialog;
//---------------------------------------------------------------------------
#endif
