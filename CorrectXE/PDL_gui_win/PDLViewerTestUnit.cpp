//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PDLViewerTestUnit.h"
#include "PDLViewerUnit.h"
#include "MTIstringstream.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TB2Dock"
#pragma link "TB2Item"
#pragma link "TB2Toolbar"
#pragma resource "*.dfm"

TPDLViewerTestForm *PDLViewerTestForm;
//////////////////////////////////////////////////////////////////////////////
//
// CNavPDLViewerInterface - Callbacks from PDL Viewers to Navigator
//
//////////////////////////////////////////////////////////////////////////////

class CNavPDLViewerInterface : public CPDLRenderInterface
{
public:
   int GoToPDLEntry(CPDLEntry &pdlEntry);
};

static CNavPDLViewerInterface navPDLViewerInterface;   // one global instance

int CNavPDLViewerInterface::GoToPDLEntry(CPDLEntry &pdlEntry)
{
   return PDLViewerTestForm->GoToPDLEntry(pdlEntry);
}

//---------------------------------------------------------------------------
__fastcall TPDLViewerTestForm::TPDLViewerTestForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TPDLViewerTestForm::FormCreate(TObject *Sender)
{
   CPDLViewerManager mgr;
   mgr.SetOwner(Application);
   mgr.SetAppInterface(&navPDLViewerInterface);

}
//---------------------------------------------------------------------------
void __fastcall TPDLViewerTestForm::OpenPDLTBItemClick(TObject *Sender)
{
   CPDLViewerManager mgr;

   mgr.OpenPDL(0);
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerTestForm::ShowActivePDLTBItemClick(
      TObject *Sender)
{
   CPDLViewerManager mgr;

   mgr.ShowActivePDL();
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerTestForm::WindowTBSubmenuItemClick(
      TObject *Sender)
{
   CPDLViewerManager mgr;

   ShowActivePDLTBItem->Checked = mgr.IsActivePDLVisible();
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerTestForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   Beep();

   CPDLViewerManager mgr;
   mgr.CloseAll();
}
//---------------------------------------------------------------------------

void __fastcall TPDLViewerTestForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   Beep();   
}
//---------------------------------------------------------------------------

int TPDLViewerTestForm::GoToPDLEntry(CPDLEntry &pdlEntry)
{
   MTIostringstream ostrm;
   ostrm << "Go to PDL Entry" << endl
         << pdlEntry.srcList[0]->binPath << endl
         << pdlEntry.srcList[0]->clipName;
   _MTIErrorDialog(Handle, ostrm.str());
   return 0;
}

//---------------------------------------------------------------------------

