//---------------------------------------------------------------------------

#ifndef PDLViewerUnitH
#define PDLViewerUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include "MTIUNIT.h"
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
#include <Buttons.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
#include "ColorLabel.h"
#include "ColorPanel.h"

#include <list>
#include <string>
#include <vector>
using std::list;
using std::string;
using std::vector;

#undef PDL_MRU
#include "pdlviewerlibint.h"
#include "PDLViewerProperties.h"

// When implementing PDL MRU list, you also need to go to the designer and
// increase the witdh of the OpenPDLToolButton, change its type to
// "drop down" and add an event to go to PDLFileMRUListClick.
///#define PDL_MRU

//---------------------------------------------------------------------------
// Forward Declarations

class CPDLRenderInterface;
class CPDLViewerProperties;
class TFormatColumnsDialog;
//---------------------------------------------------------------------------

class MTI_PDLVIEWERLIB_API TPDLViewerForm;    // expose class outside of lib

class TPDLViewerForm : public TMTIForm
{
__published:	// IDE-managed Components
   TPanel *PDLViewerPanel;
   THeaderControl *PDLViewerHeaderControl;
   TListBox *PDLViewerListBox;
   TPopupMenu *PDLViewerHeaderPopupMenu;
   TMenuItem *PDLViewerFormatColumns;
		TImageList *PDLViewerTBImageList;
   TStatusBar *PDLViewerStatusBar;
   TImageList *PDLStatusImageList;
		TCoolBar *CoolBar1;
		TToolBar *ToolBar1;
		TActionList *PDLViewerActionList;
        TAction *FileNew;
        TAction *FileOpen;
        TAction *FileClose;
        TAction *FileCloseAll;
		TFileExit *FileExit;
        TEditUndo *EditUndo;
        TAction *RenderAll;
        TAction *PreviewAll;
        TAction *EditCut;
		TAction *EditCopy;
        TAction *EditPaste;
        TAction *EditDelete;
        TAction *EditSelectAll;
        TAction *EditClearStatus;
		TAction *EditClearStatusAll;
        TToolButton *NewFileToolButton;
        TToolButton *OpenPDLToolButton;
        TToolButton *ToolButton4;
        TToolButton *CutToolButton;
		TToolButton *CopyToolButton;
        TToolButton *PasteToolButton;
        TToolButton *DeleteToolButton;
        TBitBtn *ActivateButton;
        TToolButton *ToolButton1;
		TBitBtn *RenderButton;
		TToolButton *ToolButton2;
        TMainMenu *MainMenu;
		TMenuItem *FileTBSubmenuItem;
		TMenuItem *FileNewMenuItem;
        TMenuItem *FileOpenMenuItem;
		TMenuItem *MenuItem20;
		TMenuItem *FileCloseMenuItem;
		TMenuItem *FileCloseAllMenuItem;
		TMenuItem *MenuItem23;
		TMenuItem *EditMenuItem;
		TMenuItem *EditCutMenuItem;
		TMenuItem *EditCopyMenuItem;
		TMenuItem *EditPasteMenuItem;
		TMenuItem *EditDeleteMenuItem;
		TMenuItem *EditSelectAllMenuItem;
		TMenuItem *MenuItem24;
		TMenuItem *EditClearStatusMenuItem;
		TMenuItem *EditClearStatusAllMenuItem;
		TMenuItem *ViewTBSubmenuItem;
		TMenuItem *TBSubmenuItem1;
		TMenuItem *TBVisibilityMenuItem;
   TMenuItem *N1;
   TMenuItem *SaveAsMenuItem;
   TAction *FileSaveAs;
   TToolButton *SaveAsToolButton;
   TTimer *PdlUpdateInProgressBlinkTimer;
   TPanel *PDLViewerListViewBackgroundPanel;
   TColorPanel *PDLViewerUpdateInProgressPanel;
   TColorLabel *PDLViewerUpdateInProgressLabel;
   void __fastcall PDLViewerHeaderControlSectionResize(
          THeaderControl *HeaderControl, THeaderSection *Section);
   void __fastcall PDLViewerHeaderControlSectionEndDrag(TObject *Sender);
   void __fastcall PDLViewerHeaderControlSectionDrag(TObject *Sender,
          THeaderSection *FromSection, THeaderSection *ToSection,
          bool &AllowDrag);
   void __fastcall PDLViewerFormatColumnsClick(TObject *Sender);
   void __fastcall PDLViewerListBoxDrawItem(TWinControl *Control,
          int Index, TRect &Rect, TOwnerDrawState State);
   void __fastcall PDLViewerHeaderControlSectionTrack(
          THeaderControl *HeaderControl, THeaderSection *Section,
          int Width, TSectionTrackState State);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall EditSelectAllExecute(TObject *Sender);
   void __fastcall EditDeleteExecute(TObject *Sender);
   void __fastcall EditCutExecute(TObject *Sender);
   void __fastcall EditCopyExecute(TObject *Sender);
   void __fastcall EditPasteExecute(TObject *Sender);
   void __fastcall FileOpenExecute(TObject *Sender);
   void __fastcall FileNewExecute(TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FileCloseAllExecute(TObject *Sender);
   void __fastcall FileCloseExecute(TObject *Sender);
   void __fastcall PDLViewerListBoxDblClick(TObject *Sender);
   void __fastcall RenderAllExecute(TObject *Sender);
////#ifdef PDL_MRU
//   void __fastcall PDLFileMRUListClick(TObject *Sender,
//		  const AnsiString Filename);
////#endif
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall ActivateTBItemClick(TObject *Sender);
   void __fastcall PreviewAllExecute(TObject *Sender);
   void __fastcall EditClearStatusExecute(TObject *Sender);
   void __fastcall EditClearStatusAllExecute(TObject *Sender);
		void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
		  TShiftState Shift);
		void __fastcall FormKeyUp(TObject *Sender, WORD &Key,
		  TShiftState Shift);
   void __fastcall FileSaveAsExecute(TObject *Sender);
   void __fastcall PdlUpdateInProgressBlinkTimerTimer(TObject *Sender);
   void __fastcall PDLViewerListBoxKeyPress(TObject *Sender, System::WideChar &Key);

private:	// User declarations
   CPDLViewerProperties *viewerProperties;
   CPDLView *pdlView;
   string pdlFilename;
   string pdlDisplayName;
   bool okayToClose;
   bool isActivePDL;

   static TColor activeColor;
   static TColor inactiveColor;

   string MakePDLFilename();
   void UpdateWindowTitle();
   bool CheckForClipName(const string &clipName);
   void DrawItem(int rowIndex, CPDLViewerListItem *pdlListItem, int columnID,
                 const TRect &itemRect, TCanvas *canvas);
   void RenderAllExecute_WTF(bool doRender);

   bool AreWeInterestedInThisKeyCombo(WORD &Key, TShiftState Shift);

public:		// User declarations
   __fastcall TPDLViewerForm(TComponent* Owner);
   virtual __fastcall ~TPDLViewerForm();
   virtual bool ReadSAP(CIniFile *ini, const string &IniSection);
   virtual void StartForm(void);
#ifdef PDL_MRU
////   void InitForm(TFormatColumnsDialog *newFormatColumnsDialog,
////                 TTBMRUList *pdlMRUList);
#else
   void InitForm(TFormatColumnsDialog *newFormatColumnsDialog);
#endif
   void SetPDL(CPDL *newPDL);
   void SetPDLFilename(const string &newPDLFilename);
   void BeginningPDLAddOperation();
   void EndingPDLAddOperation();
   void AppendEntries(CPDL &newEntriesPDL);
   void ReplaceEntry(CPDL &newEntriesPDL, CPDL::PDLEntry targetPosition);
   void AddToPDLFileMRU(const string &newPDLFilename);
   void SetActive(bool newFlag);
   void SavePDLIfModified();

   void OnClipWasDeleted(const string &deletedClipName);

   bool isFile;        // PDL originated from or was written to a file
   bool hasDestrEntry; // PDL has at least one "destructive" entry

};
//---------------------------------------------------------------------------

////extern PACKAGE TPDLViewerForm *PDLViewerForm;

//---------------------------------------------------------------------------
// Global Function Declarations

//---------------------------------------------------------------------------
#endif
