//---------------------------------------------------------------------------

#ifndef BatchRenderingUnitH
#define BatchRenderingUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "MTIBusy.h"

#include "ToolObject.h"
#include <ComCtrls.hpp>

//---------------------------------------------------------------------------
class TBatchRenderingForm : public TForm
{
__published:	// IDE-managed Components
   TButton *StartButton;
   TTimer *ProgressTimer;
   TMTIBusy *BusyArrows;
   TLabel *StatusMsg1;
   TProgressBar *EventProgressBar;
   TLabel *EventXofY;
   TRadioGroup *ErrorResponse;
   TButton *StopButton;
   TGroupBox *StatsGroupBox;
   TLabel *Label1;
   TLabel *Label2;
   TLabel *Label3;
   TLabel *SuccessCount;
   TLabel *FailureCount;
   TLabel *TotalCount;
   TLabel *SkippedCount;
   TLabel *Label5;
   void __fastcall StartButtonClick(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall ProgressTimerTimer(TObject *Sender);
   void __fastcall StopButtonClick(TObject *Sender);
private:	// User declarations
   void MonitorPDLRendering();
   void ShowProgress();

   bool okayToClose;
   bool renderFlag;

   EPDLRenderingStatus previousPDLRenderingStatus;

   TListBox *parentPDLViewerListBox;

public:		// User declarations
   __fastcall TBatchRenderingForm(TComponent* Owner, TListBox *newPDLViewerListBox);
   void SetRender(bool newRenderFlag);
};
//---------------------------------------------------------------------------
extern PACKAGE TBatchRenderingForm *BatchRenderingForm;
//---------------------------------------------------------------------------
#endif
