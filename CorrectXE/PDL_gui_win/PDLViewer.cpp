//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("PDLViewerUnit.cpp", PDLViewerForm);
USEFORM("FormatColumnsDialogUnit.cpp", FormatColumnsDialog);
USEFORM("C:\MTIBorland\Repository\MTIUNIT.cpp", MTIForm);
//---------------------------------------------------------------------------
#include "PDLViewerUnit.h"
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TPDLViewerForm), &PDLViewerForm);
       Application->CreateForm(__classid(TFormatColumnsDialog), &FormatColumnsDialog);
       Application->CreateForm(__classid(TMTIForm), &MTIForm);
       PostMessage(PDLViewerForm->Handle, CW_START_PROGRAM, 0, 0);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   catch (...)
   {
       try
       {
          throw Exception("");
       }
       catch (Exception &exception)
       {
          Application->ShowException(&exception);
       }
   }
   return 0;
}
//---------------------------------------------------------------------------
