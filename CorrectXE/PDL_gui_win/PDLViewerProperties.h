// PDLViewerProperties.h:  interface for the CPDLViewerProperties and
//                         CPDLViewerColumn classes
//
// Created by: John Starr, September 18, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/PDL_gui_win/PDLViewerProperties.h,v 1.4 2004/09/28 01:16:07 starr Exp $
*/
/////////////////////////////////////////////////////////////////////////////

#ifndef PDLViewerPropertiesH
#define PDLViewerPropertiesH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <string>
#include <vector>
#include "machine.h"
#include "PDL.h"

using std::string;
using std::vector;

//---------------------------------------------------------------------------
// Forward Declarations

class CConvert;
class CPDL;
class CPDLEntry;
class CPDLView;
class TFormatColumnsDialog;

//---------------------------------------------------------------------------

enum EPDLViewerColumnID
{
   PDL_VIEWER_COLUMN_INDEX = 0,
   PDL_VIEWER_COLUMN_STATUS = 1,
   PDL_VIEWER_COLUMN_THUMBNAIL = 2,
   PDL_VIEWER_COLUMN_TOOL = 3,
   PDL_VIEWER_COLUMN_SOURCE = 4,
   PDL_VIEWER_COLUMN_SRC_IN_OUT = 5,
   PDL_VIEWER_COLUMN_DESTINATION = 6,
   PDL_VIEWER_COLUMN_DST_IN_OUT = 7
};

//---------------------------------------------------------------------------

class CPDLViewerColumn
{
public:
   string displayName;    // Column name used in formatting editor
   string iniKeyName;     // Ini file key
   bool visible;          // Column is currently visible
   int columnID;
   int headerItemID;      // ID of THeaderSection
   THeaderSection *headerSectionCopy;  // Copy of THeaderSection used
                                       // to preserve properties originally
                                       // set in the Object Inspector and
                                       // perhaps modified during run time.
                                       // This is here because OnSectionResize
                                       // returns the wrong section after the
                                       // the headings have been reordered!
public:
   int GetColumnWidth() const;
   string GetHeadingName() const;
   void SetColumnWidth(int width);
};

// Structure that can be statically initialized
struct SColumnInit
{
   const char *iniKeyName;       // Ini file name, if NULL, same as displayName
};

class CPDLViewerProperties
{
public:
   CPDLViewerProperties();
   virtual ~CPDLViewerProperties();

   void InitControls(THeaderControl *newHeaderControl,
                     TFormatColumnsDialog *newFormatDialog);
   void InitColumns(SColumnInit *columnInitInfo);

   CPDLViewerColumn* FindColumn(int targetHeaderItemID);
   CPDLViewerColumn* FindColumnByIniKeyName(const string &targetIniKeyName);
   int GetColumnCount() const;
   bool GetColumnVisible(int columnID) const;
   int GetHeaderCount() const;
   CPDLViewerColumn* GetMappedColumn(int headerIndex);
   
   void SetColumnVisible(int columnID, bool newVisible);

   void MoveColumn(THeaderSection *fromSection, THeaderSection *toSection);
   void ResizeColumn(THeaderSection *section);
   void SetColumnOrder();
   void ShowHideColumn(int columnID, bool doShow);
   void RebuildHeader();

   int SaveProperties();
   int RestoreProperties();
   
public:
   int rowHeight;
   vector<CPDLViewerColumn*> columnList;
   vector<int> columnMap;  // Maps order of columns in header to columnID

   // handy pointers to Windows/Borland VCL controls
   THeaderControl *headerControl;
   TFormatColumnsDialog *formatDialog;

private:
   void AddColumn(THeaderSection *headerSection, const string &newIniKeyName,
                  bool newVisible, int newColumnID);
   void AddHeaderSection(CPDLViewerColumn *column);
};

//---------------------------------------------------------------------------

class CPDLViewerListItem : public TObject
{
public:
   __fastcall CPDLViewerListItem(CPDLView *newParentPDLView);
   virtual __fastcall ~CPDLViewerListItem();

   CPDLEntry *GetPDLEntry();

   void DeleteContents();
   void MoveContents(CPDL &destPDL);
   void CopyContents(CPDL &destPDL);

   void ClearStatus();
   bool RefersToClipNamed(const string &clipName);

   static void SetThumbnailSize(int newWidth, int newHeight);

   void LoadThumbnailBitmap();

public:
   CPDLView *parentPDLView;

   CPDL::PDLEntry pdlEntry;  // iterator into PDL's list of PDL entries

   Graphics::TBitmap *thumbnailBitmap;

   EPDLEntryStatus entryStatus;
   int errorCode;
   string errorMessage;

private:
   static MTI_UINT8* mediaReadBuffer;
   static MTI_UINT32 mediaReadBufferSize;
   static int thumbnailHeight;
   static int thumbnailWidth;
   static MTI_UINT32 *thumbnailBuffer;
   static CConvert *thumbnailConvert;
};

//---------------------------------------------------------------------------

class CPDLView
{
public:
   CPDLView(TListBox *newListBox, CPDLViewerProperties *newViewerProperties);
   virtual ~CPDLView();

   int SetPDL(CPDL *newPDL);
   int RefreshPDL();

   bool IsModified();

   void EditCopy();
   void EditCut();
   void EditDelete();
   void EditPaste();
   void EditClearStatus();
   void EditClearStatusAll();

   void AppendEntries(CPDL &newEntriesPDL);
   void ReplaceEntry(CPDL &newEntriesPDL, CPDL::PDLEntry targetPosition);
   bool CheckForClipName(const string &clipName);

   int SavePDLToFile(const string &filename);

public:
   TListBox *listBox;
   CPDL *pdl;
   CPDLViewerProperties *viewerProperties;

   bool hasChanged;     // true when the PDL has changed but not yet
                        // written to a file

   static CPDL *scratchPadPDL;

private:
   void ClearTheListBox();
};

//---------------------------------------------------------------------------

#endif // #ifndef PDLViewerProperties_H




 