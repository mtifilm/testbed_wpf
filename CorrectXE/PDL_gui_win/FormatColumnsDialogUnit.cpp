//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "FormatColumnsDialogUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormatColumnsDialog *FormatColumnsDialog = 0;

//---------------------------------------------------------------------------

__fastcall TFormatColumnsDialog::TFormatColumnsDialog(TComponent* Owner)
   : TForm(Owner)
{
   SetColumnEnabled(0, false); // Cannot remove the Index column
   SetColumnEnabled(1, false); // Cannot remove the Status column
}

//---------------------------------------------------------------------------
void __fastcall TFormatColumnsDialog::OKButtonClick(TObject *Sender)
{
   ModalResult = mrOk;   
}

//---------------------------------------------------------------------------
void __fastcall TFormatColumnsDialog::CancelButtonClick(TObject *Sender)
{
   ModalResult = mrCancel;   
}

//---------------------------------------------------------------------------

bool TFormatColumnsDialog::GetColumnChecked(int index)  const
{
   if (index < 0 || index >= ColumnsCheckListBox->Count)
      return false;

   return ColumnsCheckListBox->Checked[index];
}

int TFormatColumnsDialog::GetColumnCount() const
{
   return ColumnsCheckListBox->Count;
}

bool TFormatColumnsDialog::GetColumnEnabled(int index) const
{
   if (index < 0 || index >= ColumnsCheckListBox->Count)
      return false;

   return ColumnsCheckListBox->ItemEnabled[index];
}

string TFormatColumnsDialog::GetColumnName(int index)  const
{
   string colName;

   if (index < 0 || index >= ColumnsCheckListBox->Count)
      return colName;   // return empty string if index is out of range

   colName = StringToStdString(ColumnsCheckListBox->Items->Strings[index]);
   
   return colName;
}

void TFormatColumnsDialog::SetColumnChecked(int index, bool newChecked)
{
   if (index < 0 || index >= ColumnsCheckListBox->Count)
      return;

   ColumnsCheckListBox->Checked[index] = newChecked;
}

void TFormatColumnsDialog::SetColumnEnabled(int index, bool newEnabled)
{
   if (index < 0 || index >= ColumnsCheckListBox->Count)
      return;

   ColumnsCheckListBox->ItemEnabled[index] = newEnabled;
}

//---------------------------------------------------------------------------


