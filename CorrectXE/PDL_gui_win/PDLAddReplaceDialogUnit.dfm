object PDLAddReplaceDialog: TPDLAddReplaceDialog
  Left = 366
  Top = 397
  BorderStyle = bsDialog
  Caption = 'Add or Replace PDL Entry?'
  ClientHeight = 103
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnKeyPress = KeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 265
    Height = 13
    Caption = 'Add a new PDL Entry or replace the existing PDL Entry?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object AddButton: TButton
    Left = 16
    Top = 64
    Width = 75
    Height = 25
    Caption = 'New'
    Default = True
    ModalResult = 7
    TabOrder = 0
    OnKeyPress = KeyPress
  end
  object ReplaceButton: TButton
    Left = 112
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Replace'
    ModalResult = 6
    TabOrder = 1
    OnKeyPress = KeyPress
  end
  object CancelButton: TButton
    Left = 210
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    OnKeyPress = KeyPress
  end
end
