//---------------------------------------------------------------------------

#ifndef PDLAddReplaceDialogUnitH
#define PDLAddReplaceDialogUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TPDLAddReplaceDialog : public TForm
{
__published:	// IDE-managed Components
   TButton *AddButton;
   TButton *ReplaceButton;
   TButton *CancelButton;
   TLabel *Label1;
   void __fastcall KeyPress(TObject *Sender, System::WideChar &Key);


private:	// User declarations

//   void __fastcall HandleWMChar(TWMChar &Message);
//
//BEGIN_MESSAGE_MAP
//   VCL_MESSAGE_HANDLER(WM_CHAR, TWMChar, HandleWMChar)
//END_MESSAGE_MAP(TForm)

public:		// User declarations
   __fastcall TPDLAddReplaceDialog(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPDLAddReplaceDialog *PDLAddReplaceDialog;
//---------------------------------------------------------------------------
#endif
