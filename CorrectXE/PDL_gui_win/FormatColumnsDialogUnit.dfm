object FormatColumnsDialog: TFormatColumnsDialog
  Left = 195
  Top = 231
  AutoSize = True
  BorderStyle = bsDialog
  Caption = 'Format Columns'
  ClientHeight = 145
  ClientWidth = 163
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 0
    Top = 120
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 88
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = CancelButtonClick
  end
  object ColumnsCheckListBox: TCheckListBox
    Left = 1
    Top = 0
    Width = 160
    Height = 113
    Flat = False
    ItemHeight = 13
    Items.Strings = (
      'Index'
      'Status'
      'Thumbnail'
      'Tool'
      'Source 1'
      'Src 1 In/Out'
      'Destination 1'
      'Dst 1 In/Out')
    TabOrder = 2
  end
end
