// PDLViewerProperties.cpp:  Implementation of the CPDLViewerProperties and
//                           CPDLViewerColumn classes
//
// Created by: John Starr, September 18, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/PDL_gui_win/PDLViewerProperties.cpp,v 1.8.14.1 2008/09/26 20:24:01 mbraca Exp $
*/
/////////////////////////////////////////////////////////////////////////////


#include "PDLViewerProperties.h"
#include "ClipAPI.h"
#include "Convert.h"
#include "FileSweeper.h"
#include "FormatColumnsDialogUnit.h"
#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "PDL.h"

#include <fstream>
using std::ofstream;

//---------------------------------------------------------------------------
// Static Data

CPDL* CPDLView::scratchPadPDL = 0;

MTI_UINT8* CPDLViewerListItem::mediaReadBuffer = 0;
MTI_UINT32 CPDLViewerListItem::mediaReadBufferSize = 0;
int CPDLViewerListItem::thumbnailHeight = 0;
int CPDLViewerListItem::thumbnailWidth = 0;
MTI_UINT32* CPDLViewerListItem::thumbnailBuffer = 0;
CConvert* CPDLViewerListItem::thumbnailConvert = 0;

//////////////////////////////////////////////////////////////////////////////

__fastcall CPDLViewerListItem::CPDLViewerListItem(CPDLView *newParentPDLView)
 : parentPDLView(newParentPDLView), thumbnailBitmap(0)
{
   if (thumbnailConvert == 0)
      thumbnailConvert = new CConvert;
}


__fastcall CPDLViewerListItem::~CPDLViewerListItem()
{
   delete thumbnailBitmap;
}

// ----------------------------------------------------------------------------
void CPDLViewerListItem::DeleteContents()
{
   CPDL *pdl = parentPDLView->pdl;

   pdl->DeleteEntry(pdlEntry);
}

void CPDLViewerListItem::MoveContents(CPDL &destPDL)
{
   CPDL *pdl = parentPDLView->pdl;

   pdl->MoveEntry(pdlEntry, destPDL);
}

void CPDLViewerListItem::CopyContents(CPDL &destPDL)
{
   destPDL.AppendEntry(pdlEntry);
}

void CPDLViewerListItem::ClearStatus()
{
   CPDLEntry *entry = GetPDLEntry();

   entry->ClearStatus();
}

bool CPDLViewerListItem::RefersToClipNamed(const string &clipName)
{
   CPDLEntry *entry = GetPDLEntry();

   return entry->RefersToClipNamed(clipName);
}

CPDLEntry* CPDLViewerListItem::GetPDLEntry()
{
   CPDL *pdl = parentPDLView->pdl;

   return pdl->GetPDLEntry(pdlEntry);
}

//-----------------------------------------------------------------------------

void CPDLViewerListItem::SetThumbnailSize(int newWidth, int newHeight)
{
   if (newWidth == thumbnailWidth && newHeight == thumbnailHeight)
      return;  // no change, nothing to do

   if (newWidth * newHeight > thumbnailWidth * thumbnailHeight)
      {
      // reallocate the thumbnail conversion buffer

      MTIfree(thumbnailBuffer);  // free previous buffer

      thumbnailBuffer = (unsigned int *)MTImalloc(newWidth * newHeight
                                                  * sizeof(MTI_UINT32));
      if (thumbnailBuffer == 0)
         {
         // allocation failed
         thumbnailWidth = 0;
         thumbnailHeight = 0;
         return;
         }
      }

   // remember the new dimensions
   thumbnailWidth = newWidth;
   thumbnailHeight = newHeight;

}

void CPDLViewerListItem::LoadThumbnailBitmap()
{
   int retVal;

   if (thumbnailBitmap != 0)
      return;  // the thumbnail is already loaded

   // Check for thumbnail buffer
   if (thumbnailBuffer == 0)
      return;

   // Check for size of thumbnail
   if (thumbnailHeight <= 0 || thumbnailWidth <= 0)
      return;

   CPDLEntry *pdlEntry = GetPDLEntry();
   CPDLEntry_Media *srcMedia = pdlEntry->srcList[0];

   // Open the clip
   CVideoField *field = srcMedia->GetInVideoField();
   if (field == 0)
      return;

   // Check if read media buffer is big enough, reallocate if not
   const CImageFormat *srcImageFormat = srcMedia->GetImageFormat();
   MTI_UINT32 bufferSizeNeeded =  field->getDataSize();

//   //********* EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
//   // Alpha bits that were embedded in unused bits within the
//   // normal RGB pixel data are sucked out on the fly and appended
//   // to the RGB pixel data, so the max byte count increases by
//   // (number of pixels)/8 , or one alpha bit per pixel.
//   //
//   EAlphaMatteType alphaType = srcImageFormat->getAlphaMatteType();
//   if (alphaType == IF_ALPHA_MATTE_1_BIT_CINTEL_HACK)
//      {
//      bufferSizeNeeded += srcImageFormat->getAlphaBytesPerField();
//      }
//   // *******************************************************************

   if (mediaReadBufferSize < bufferSizeNeeded)
      {
      MTIfree(mediaReadBuffer);

      mediaReadBufferSize = bufferSizeNeeded;
      mediaReadBuffer = (MTI_UINT8*)MTImalloc(mediaReadBufferSize);
      if (mediaReadBuffer == 0)
         {
         mediaReadBufferSize = 0;
         return;  // buffer allocation failed
         }
      }

   // Read the field's media
   retVal = field->readMedia(mediaReadBuffer);
   if (retVal != 0)
      return;

   // Convert field to thumbnail size
   MTI_UINT8 *ucpLocal[2];
   ucpLocal[0] = mediaReadBuffer;
   ucpLocal[1] = mediaReadBuffer;
   RECT srcActivePictureRect = srcImageFormat->getActivePictureRect();
      TRACE_3(errout << "CONVERT 5");
   thumbnailConvert->convertWtf(ucpLocal, srcImageFormat, &srcActivePictureRect,
                             thumbnailBuffer, thumbnailWidth, thumbnailHeight,
                             thumbnailWidth * sizeof(MTI_UINT32));

   // Create a TBitmap to hold the Thumbnail
   thumbnailBitmap = new Graphics::TBitmap;
   thumbnailBitmap->PixelFormat = pf24bit;
   thumbnailBitmap->Height = thumbnailHeight;
   thumbnailBitmap->Width = thumbnailWidth;

   // Copy thumbnail to TBitmap
   for (int row = 0; row < thumbnailBitmap->Height; ++row)
      {
      TRGBTriple *ptr = (TRGBTriple *)thumbnailBitmap->ScanLine[row];
      MTI_UINT32 *uip = &thumbnailBuffer[row * thumbnailBitmap->Width];
      for (int col = 0; col < thumbnailBitmap->Width; ++col)
         {
         MTI_UINT32 pixel = *uip++;
         ptr->rgbtRed = (pixel >> 16) & 0xFF;
         ptr->rgbtGreen = (pixel >> 8) & 0xFF;
         ptr->rgbtBlue = pixel & 0xFF;
         ptr++;
         }
      }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

int CPDLViewerColumn::GetColumnWidth() const
{
   return headerSectionCopy->Width;
}

string CPDLViewerColumn::GetHeadingName() const
{
   string headingName = StringToStdString(headerSectionCopy->Text);

   return headingName;
}

void CPDLViewerColumn::SetColumnWidth(int width)
{
   headerSectionCopy->Width = width;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

CPDLViewerProperties::CPDLViewerProperties()
{
}

CPDLViewerProperties::~CPDLViewerProperties()
{
   // Delete the columns
   int count = GetColumnCount();
   for (int i = 0; i < count; ++i)
      {
      delete columnList[i]->headerSectionCopy;
      delete columnList[i];
      }
}

void CPDLViewerProperties::InitControls(THeaderControl *newHeaderControl,
                                        TFormatColumnsDialog *newFormatDialog)
{
   headerControl = newHeaderControl;
   formatDialog = newFormatDialog;
}

void CPDLViewerProperties::InitColumns(SColumnInit *columnInitInfo)
{
   // Default is all columns visible
   for (int i = 0; i < formatDialog->GetColumnCount(); ++i)
      {
      formatDialog->SetColumnChecked(i, true);
      }

   int headerColumnCount = headerControl->Sections->Count;
   for (int i = 0; i < headerColumnCount; ++i)
      {
      THeaderSection *headerSection = headerControl->Sections->Items[i];
      string iniKeyName;
      if (columnInitInfo[i].iniKeyName != 0)
         iniKeyName = columnInitInfo[i].iniKeyName;
      else
         iniKeyName = formatDialog->GetColumnName(i);
      int visible = formatDialog->GetColumnChecked(i);
      AddColumn(headerSection, iniKeyName, visible, i);
      }

   // Set initial column order
   SetColumnOrder();
}

void CPDLViewerProperties::AddColumn(THeaderSection *headerSection,
                                     const string &newIniKeyName,
                                     bool newVisible, int newColumnID)
{
   CPDLViewerColumn *newColumn = new CPDLViewerColumn;

   THeaderSection *headerSectionCopy = new THeaderSection(0);
   headerSectionCopy->Assign(headerSection); // copy width, text, etc.
   newColumn->headerSectionCopy = headerSectionCopy;

   newColumn->iniKeyName = newIniKeyName;
   newColumn->visible = newVisible;
   newColumn->columnID = newColumnID;
   newColumn->headerItemID = headerSection->ID;

   columnList.push_back(newColumn);
}

void CPDLViewerProperties::ShowHideColumn(int columnID, bool doShow)
{
   CPDLViewerColumn *column = columnList[columnID];
   if (column->visible == doShow)
      return;  // nothing to do

   column->visible = doShow;

   if (doShow)
      {
      // Show the column by adding the section header to the header control
      AddHeaderSection(column);
      }
   else
      {
      // Hide the column
      TCollectionItem *item;
      item = headerControl->Sections->FindItemID(column->headerItemID);
      column->headerItemID = -1;
      headerControl->Sections->Delete(item->Index);
      }

   SetColumnOrder();
}

void CPDLViewerProperties::RebuildHeader()
{
   // Delete the existing header sections
   headerControl->Sections->Clear();

   // Iterate over the visible headers
   int headerCount = GetHeaderCount();
   for (int i = 0; i < headerCount; ++i)
      {
      CPDLViewerColumn *column = GetMappedColumn(i);

      // Add the new header section to the header control
      AddHeaderSection(column);
      }
}

int CPDLViewerProperties::SaveProperties()
{
   string iniFileName = CPMPIniFileName();
   CIniFile *iniFile = CreateIniFile(iniFileName);

   string sectionName = "PDLViewerColumns";

   int i;

   StringList activeList;
   int headerCount = GetHeaderCount();
   for (i = 0; i < headerCount; ++i)
      {
      CPDLViewerColumn *column = GetMappedColumn(i);
      activeList.push_back(column->iniKeyName);
      }
   iniFile->WriteStringList(sectionName, "Header", &activeList);

   string key;

   int columnCount = GetColumnCount();
   for (i = 0; i < columnCount; ++i)
      {
      CPDLViewerColumn *column = columnList[i];
      key = "Width_" + column->iniKeyName;
      iniFile->WriteInteger(sectionName, key, column->GetColumnWidth());
      }

   DeleteIniFile(iniFile);

   return 0;
}

int CPDLViewerProperties::RestoreProperties()
{
   string iniFileName = CPMPIniFileName();
   CIniFile *iniFile = CreateIniFile(iniFileName);

   string sectionName = "PDLViewerColumns";

   StringList defaultList;
   int columnCount = GetColumnCount();
   for (int i = 0; i < columnCount; ++i)
      {
      CPDLViewerColumn *column = columnList[i];
      string key = "Width_" + column->iniKeyName;
      int width = iniFile->ReadInteger(sectionName, key, column->GetColumnWidth());
      column->SetColumnWidth(width);
      column->visible = false;   // force all columns to be invisible

      defaultList.push_back(column->iniKeyName);
      }

   StringList activeList;
   iniFile->ReadStringList(sectionName, "Header", &activeList, &defaultList);
   int activeCount = activeList.size();
   columnMap.clear();
   for (int i = 0; i < activeCount; ++i)
      {
      string name = activeList[i];
      CPDLViewerColumn *column = FindColumnByIniKeyName(name);
      if (column == 0) continue; // skip if cannot fine the name
      column->visible = true;
      columnMap.push_back(column->columnID);
      }

   iniFile->FileName = ""; // don't write the file
   DeleteIniFile(iniFile);

   RebuildHeader();

   return 0;
}

void CPDLViewerProperties::AddHeaderSection(CPDLViewerColumn *column)
{
   // Add the header section to the header control
   THeaderSection *newSection = headerControl->Sections->AddItem(0, -1);

   // Copy the Text, Width, etc to the new header section
   newSection->Assign(column->headerSectionCopy);

   // Remember the ID of the new header section
   column->headerItemID = newSection->ID;
}

void CPDLViewerProperties::MoveColumn(THeaderSection *fromSection,
                                      THeaderSection *toSection)
{
}

void CPDLViewerProperties::SetColumnOrder()
{
   // Record the mapping between the order of columns in the THeaderControl
   // and the list of all columns
   columnMap.clear();
   int headerCount = headerControl->Sections->Count;
   for (int i = 0; i < headerCount; ++i)
      {
      THeaderSection *headerSection = headerControl->Sections->Items[i];
      CPDLViewerColumn *column = FindColumn(headerSection->ID);
      columnMap.push_back(column->columnID);
      }
}

int CPDLViewerProperties::GetHeaderCount() const
{
   return columnMap.size();
}

CPDLViewerColumn* CPDLViewerProperties::GetMappedColumn(int headerIndex)
{
   int columnID = columnMap[headerIndex];

   return columnList[columnID];
}


void CPDLViewerProperties::ResizeColumn(THeaderSection *section)
{
   CPDLViewerColumn *column = FindColumn(section->ID);
   if (column == 0)
      return;  // something wrong, should never happen

   column->SetColumnWidth(section->Width);
}

CPDLViewerColumn* CPDLViewerProperties::FindColumn(int targetHeaderItemID)
{
   int columnCount = GetColumnCount();
   for (int i = 0; i < columnCount; ++i)
      {
      CPDLViewerColumn *column = columnList[i];
      if (targetHeaderItemID == column->headerItemID)
         return column;
      }

   return 0;   // if we reached this point, we didn't find it
}

CPDLViewerColumn* CPDLViewerProperties::FindColumnByIniKeyName(
                                               const string &targetIniKeyName)
{
   int columnCount = GetColumnCount();
   for (int i = 0; i < columnCount; ++i)
      {
      CPDLViewerColumn *column = columnList[i];
      if (targetIniKeyName == column->iniKeyName)
         return column;
      }

   return 0;   // if we reached this point, we didn't find it
}

int CPDLViewerProperties::GetColumnCount() const
{
   return (int)columnList.size();
}

bool CPDLViewerProperties::GetColumnVisible(int columnID) const
{
   if (columnID < 0 || columnID >= GetColumnCount())
      return false;

   return columnList[columnID]->visible;
}

void CPDLViewerProperties::SetColumnVisible(int columnID, bool newVisible)
{
   if (columnID < 0 || columnID >= GetColumnCount())
      return;

  columnList[columnID]->visible = newVisible;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

CPDLView::CPDLView(TListBox *newListBox,
                   CPDLViewerProperties *newViewerProperties)
 : listBox(newListBox)
 , pdl(NULL)
 , viewerProperties(newViewerProperties)
 , hasChanged(false)
{
   // scratchPadPDL is a global clipboard for cut & paste
   if (scratchPadPDL == 0)
      scratchPadPDL = new CPDL(0);

   // Add dummy last item
   listBox->AddItem("", 0);
}

CPDLView::~CPDLView()
{
   ClearTheListBox();
   delete pdl;
}

void CPDLView::ClearTheListBox()
{
   int itemCount = listBox->Count;
   for (int i = 0; i < itemCount; ++i)
      {
      delete listBox->Items->Objects[i];
      }

   listBox->Clear();
}

int CPDLView::SetPDL(CPDL *newPDL)
{
   // We are completely replacing the contents of this view
   ClearTheListBox();
   delete pdl;

   pdl = newPDL;

   if (pdl != NULL)
      {
      for (CPDL::PDLEntry iter = pdl->entryList.begin();
           iter != pdl->entryList.end();
           ++iter)
         {
         CPDLViewerListItem *listItem = new CPDLViewerListItem(this);
         listItem->pdlEntry = iter;
         listBox->AddItem("", listItem);
         }
      }

   // Add dummy last item
   listBox->AddItem("", 0);

   return 0;
}

int CPDLView::RefreshPDL()
{
   // Delete then recreate the view contents.
   ClearTheListBox();

   if (pdl != NULL)
      {
      for (CPDL::PDLEntry iter = pdl->entryList.begin();
           iter != pdl->entryList.end();
           ++iter)
         {
         CPDLViewerListItem *listItem = new CPDLViewerListItem(this);
         listItem->pdlEntry = iter;
         listBox->AddItem("", listItem);
         }
      }

   // Add dummy last item
   listBox->AddItem("", 0);

   return 0;
}

// ----------------------------------------------------------------------------

bool CPDLView::IsModified()
{
   if (hasChanged)
      return true;

   if (pdl != 0 && pdl->IsModified())
      {
      hasChanged = true;
      return true;
      }

   return false;
}

// ----------------------------------------------------------------------------

void CPDLView::EditCopy()
{
   // Copy selected entries to the scratch pad PDL

   if (listBox->SelCount < 1)
      return;  // nothing selected

   scratchPadPDL->Clear();  // clear whatever is in the scratch pad

   for (int i = 0; i < listBox->Count; ++i)
      {
      if (listBox->Selected[i])
         {
         CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
         if (pdlListItem == 0)
            continue;   // "object" is NULL, so skip this

         pdlListItem->CopyContents(*scratchPadPDL);
         }
      }
}

void CPDLView::EditCut()
{
   // Move selected entries from the PDL to the scratch pad PDL

   if (listBox->SelCount < 1)
      return;  // nothing selected

   scratchPadPDL->Clear();  // clear whatever is in the scratch pad

   for (int i = 0; i < listBox->Count; ++i)
      {
      if (listBox->Selected[i])
         {
         CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
         if (pdlListItem == 0)
            continue;   // "object" is NULL, so skip this

         pdlListItem->MoveContents(*scratchPadPDL);
         delete pdlListItem;
         listBox->Items->Objects[i] = 0;

         hasChanged = true;   // remember that PDL has been modified
         }
      }

   // Unselect the last row (which is our empty placeholder)
   // so it is not deleted
   listBox->Selected[listBox->Count - 1] = false;

   listBox->DeleteSelected();
}

void CPDLView::EditDelete()
{
   // Delete selected entries from the PDL.  Scratch Pad PDL is not changed

   if (listBox->SelCount < 1)
      return;  // nothing selected

   for (int i = 0; i < listBox->Count; ++i)
      {
      if (listBox->Selected[i])
         {
         CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
         if (pdlListItem == 0)
            continue;   // "object" is NULL, so skip this

         pdlListItem->DeleteContents();
         delete pdlListItem;
         listBox->Items->Objects[i] = 0;

         hasChanged = true;   // remember that PDL has been modified
         }
      }

   // Unselect the last row (which is our empty placeholder)
   // so it is not deleted
   listBox->Selected[listBox->Count - 1] = false;

   listBox->DeleteSelected();
}

void CPDLView::EditPaste()
{
   // Paste contents of Scratch Pad PDL to PDL
   // If there is something selected, then paste just before the selected item
   // If there is nothing selected, then paste just before the last row
   // (which is our empty placeholder row)

   if (scratchPadPDL->Empty())
      return;    // nothing in scratch pad to paste

   // Determine where in the list box the new entries should be pasted.
   int insertPoint;
   if (listBox->Count <= 1)
      {
      // List is empty, so insert at the beginning
      insertPoint = 0;
      }
   else if (listBox->SelCount > 0)
      {
      // One or more items in the list box are selected, so insert just
      // before the first selected item
      for (int i = 0; i < listBox->Count; ++i)
         {
         if (listBox->Selected[i])
            {
            insertPoint = i;    // this is the first selected item
            break;
            }
         }
      }
   else
      {
      // Nothing is selected, so add after the last real item
      insertPoint = listBox->Count - 1;
      }

   CPDLViewerListItem *pdlListItem
       = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[insertPoint]);
   if (pdlListItem == 0)
      {
      // Append the new entries to the end of the list
      for (CPDL::PDLEntry iter = scratchPadPDL->entryList.begin();
           iter != scratchPadPDL->entryList.end();
           ++iter, ++insertPoint)
         {
         CPDLViewerListItem *listItem = new CPDLViewerListItem(this);
         listItem->pdlEntry = pdl->AppendEntry(iter);
         listBox->Items->InsertObject(insertPoint, "", listItem);
         hasChanged = true;   // remember that PDL has been modified
         }
      }
   else
      {
      // Insert the new entries at the insert point
      CPDL::PDLEntry destPos = pdlListItem->pdlEntry;

      for (CPDL::PDLEntry iter = scratchPadPDL->entryList.begin();
           iter != scratchPadPDL->entryList.end();
           ++iter, ++insertPoint)
         {
         CPDLViewerListItem *listItem = new CPDLViewerListItem(this);
         listItem->pdlEntry = pdl->InsertEntry(iter, destPos);
         listBox->Items->InsertObject(insertPoint, "", listItem);

         hasChanged = true;   // remember that PDL has been modified
         }
      }
}

void CPDLView::AppendEntries(CPDL &newEntriesPDL)
{
   // Copy entries from newEntriesPDL into the PDL
   if (pdl == 0)
   {
      _MTIErrorDialog(Application->Handle, "Oops! You need a PDL to capture a Decision!");
      return;
   }

   // Insert just before last (dummy) entry in list box
   int insertPoint = listBox->Count - 1;

   CPDLViewerListItem *pdlListItem = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[insertPoint]);
   if (pdlListItem == 0)
   {
      // Append the new entries to the end of the list
      for (CPDL::PDLEntry iter = newEntriesPDL.entryList.begin(); iter != newEntriesPDL.entryList.end(); ++iter, ++insertPoint)
      {
         CPDLViewerListItem *listItem = new CPDLViewerListItem(this);
         listItem->pdlEntry = pdl->AppendEntry(iter);
         listBox->Items->InsertObject(insertPoint, "", listItem);
         hasChanged = true; // remember that PDL has been modified
         Application->ProcessMessages();
      }
   }

   // Make the last entry in the list visible
   int visibleItemCount = listBox->ClientHeight / listBox->ItemHeight;
   int topItemIndex = listBox->Count - 1 - visibleItemCount;
   if (topItemIndex < 0)
   {
      topItemIndex = 0;
   }

   listBox->TopIndex = topItemIndex;
}

void CPDLView::ReplaceEntry(CPDL &newEntriesPDL, CPDL::PDLEntry targetPosition)
{
   if (pdl == 0)
      {
      _MTIErrorDialog(Application->Handle, "Oops! You need a PDL to capture a Decision!");
      return;
      }

   pdl->ReplaceEntry(newEntriesPDL, targetPosition);

   hasChanged = true;

   // Redraw the list box
   listBox->Invalidate();
}

// ----------------------------------------------------------------------------

bool CPDLView::CheckForClipName(const string &clipName)
{
   // Clear status of all PDL entries

   for (int i = 0; i < listBox->Count; ++i)
      {
      CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
      if (pdlListItem == 0)
         continue;   // "object" is NULL, so skip this

      if (pdlListItem->RefersToClipNamed(clipName))
         return true;
      }

   // Not one of our clips!
   return false;
}

// ----------------------------------------------------------------------------

void CPDLView::EditClearStatus()
{
   // Clear status of selected PDL entries
   if (listBox->SelCount < 1)
      return;  // nothing selected

   for (int i = 0; i < listBox->Count; ++i)
      {
      if (listBox->Selected[i])
         {
         CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
         if (pdlListItem == 0)
            continue;   // "object" is NULL, so skip this

         pdlListItem->ClearStatus();
         }
      }

   // Redraw the list box with the updated status column
   listBox->Invalidate();
}

void CPDLView::EditClearStatusAll()
{
   // Clear status of all PDL entries

   for (int i = 0; i < listBox->Count; ++i)
      {
      CPDLViewerListItem *pdlListItem
                = static_cast<CPDLViewerListItem*>(listBox->Items->Objects[i]);
      if (pdlListItem == 0)
         continue;   // "object" is NULL, so skip this

      pdlListItem->ClearStatus();
      }

   // Redraw the list box with the updated status column
   listBox->Invalidate();
}

// ----------------------------------------------------------------------------

int CPDLView::SavePDLToFile(const string &filename)
{
   // Render the PDL to a string stream.
   std::ostringstream tempStream;

   pdl->writeXML(tempStream);

   auto handle = CreateFile(filename.c_str(), GENERIC_READ | GENERIC_WRITE,
                       FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_ALWAYS,
                       FILE_ATTRIBUTE_NORMAL, 0);
   if (handle == INVALID_HANDLE_VALUE)
   {
      auto errorCode = GetLastError();
      TRACE_0(errout << "CPDLView::SavePDLToFile failed create PDL file " << endl;
              errout << filename << endl;
              errout << GetSystemErrorMessage(errorCode));
      return -1;
   }

   DWORD numberOfBytesWritten;
   auto pdlString = tempStream.str();
   BOOL success = WriteFile(handle, pdlString.c_str(), pdlString.length(), &numberOfBytesWritten, 0);
   if (!success)
   {
      auto errorCode = GetLastError();
      TRACE_0(errout << "CPDLView::SavePDLToFile failed to write PDL file " << endl;
              errout << filename << endl;
              errout << GetSystemErrorMessage(errorCode));
      return -1;
   }

   success = CloseHandle(handle);
   if (!success)
   {
      auto errorCode = GetLastError();
      TRACE_0(errout << "CPDLView::SavePDLToFile failed to close PDL file " << endl;
              errout << filename << endl;
              errout << GetSystemErrorMessage(errorCode));
      // no "return -1" here, ignore this error.
   }

   hasChanged = false;
   return 0;
}
