
drop database if exists mtidb;

create database mtidb;

use mtidb;

drop table if exists clip;
drop table if exists take;
drop table if exists reel;
drop table if exists reel_event;
drop table if exists layoff;
drop table if exists layoff_event;
drop table if exists event;
drop table if exists system_lock;
drop table if exists ingest_sync;

#
#  clip table.
#
source ./mticlip.sql;

#
#  system_lock table.
#
source ./mtilock.sql;

#
#  take table.
#
source ./mtitake.sql;

#
#  reel table.
#
source ./mtireel.sql;

#
#  layoff table.
#
source ./mtilayoff.sql;

#
#  event table.
#
source ./mtievent.sql;

#
#  ingest sync table
#
source ./mtiingestsync.sql;


