#  This file will update "The DA" project used at NAB 2004

use mtidb;

alter table reel_event add column sort_order int DEFAULT 0;
update reel_event set sort_order=reel_event_id;



alter table take add column edit_master_only bool DEFAULT 0;
alter table take add column resample varchar(10) DEFAULT "1.0";
alter table take add column quality_assurance bool DEFAULT 0;
alter table take add column tape_layoff bool DEFAULT 0;
alter table take add column max_room_delay int DEFAULT 0;
alter table take add column episode varchar(10) "310";
alter table take add column film_dimension int DEFAULT 2;

alter table take add column keykode_type int DEFAULT 0;
alter table take add column manual_ink_pre varchar(8) DEFAULT "";
alter table take add column manual_ink_number varchar(22) DEFAULT "";
alter table take add column manual_ink_frame int DEFAULT 0;
alter table take add column manual_ink_type int DEFAULT 0;
alter table take add column ddr_media_in int DEFAULT -1;
alter table take add column audio_media_in int DEFAULT -1;

update take set ddr_media_in=-2 where ddr_media_in=-1;

# need to create list of pickup reels
CREATE TABLE pickup_reel (
    reel_id	   int NOT NULL
,   clip_id	   int NOT NULL
) TYPE=MyISAM;


#need to create the system_lock table

CREATE TABLE system_lock(
    system_lock_id	int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   client_name		varchar(32) NOT NULL
,   bin_path		varchar(255) DEFAULT ""
,   clip_name		varchar(255) DEFAULT ""
,   bin_path_dst	varchar(255) DEFAULT ""
,   clip_name_dst	varchar(255) DEFAULT ""
,   reason              int DEFAULT 0
) TYPE=MyISAM;

alter table reel add column first_reel_event_id int DEFAULT 0;
alter table layoff add column first_reel_event_id int DEFAULT 0;
alter table layoff add column last_reel_event_id int DEFAULT -1;

alter table take drop column manual_ink_foot;
alter table take drop column manual_ink_frame;

alter table take drop column keykode_type;
alter table take drop column manual_ink_pre;
alter table take drop column manual_ink_number;
alter table take drop column manual_ink_type;

alter table take add column inknumber varchar(22) DEFAULT "";
alter table take add column ink_dimension int DEFAULT 0;

alter table layoff add column clip_id int NOT NULL;

update take set audio_media_in=-2 where audio_media_in=-1;
alter table clip add column base_audio_in int DEFAULT 0;
alter table clip add column base_image_in int DEFAULT 0;

alter table reel drop column first_reel_event_id;
alter table layoff drop column first_reel_event_id;
alter table layoff drop column last_reel_event_id;

alter table reel add column closed bool DEFAULT 0;
alter table reel add column master_reel_id int DEFAULT -1;
alter table reel add column first_frame int DEFAULT 0;
alter table reel add column first_frame_dub int DEFAULT 0;
alter table layoff add column master_reel_id int DEFAULT -1;
alter table reel_event add column edit_master_only bool DEFAULT 0;
alter table reel_event add column master_reel_id int DEFAULT -1;
alter table layoff add column existing_stock bool DEFAULT 0;

drop table pickup_reel;

alter table take add column labroll varchar(10) DEFAULT "";
alter table take add column shootdate varchar(10) DEFAULT "";
alter table reel add column playout_option int DEFAULT 0;

alter table layoff_event add column clip_id int NOT NULL;
update layoff_event set clip_id=-1;
alter table layoff add column config_path mediumtext DEFAULT "";
update layoff set config_path="";

alter table take add column imageTC varchar(16) DEFAULT "";

alter table reel add column FieldD mediumtext DEFAULT "";
alter table reel add column FieldE mediumtext DEFAULT "";
alter table reel add column FieldF mediumtext DEFAULT "";
alter table reel add column FieldG mediumtext DEFAULT "";
alter table reel add column FieldH mediumtext DEFAULT "";
alter table reel add column FieldI mediumtext DEFAULT "";

alter table layoff add column layoff_flag int DEFAULT 0;
