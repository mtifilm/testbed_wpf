#
#  lock table.  One row per client machine
#
CREATE TABLE system_lock(
    system_lock_id	int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   client_name		varchar(32) NOT NULL
,   bin_path		varchar(255) DEFAULT ""
,   clip_name		varchar(255) DEFAULT ""
,   bin_path_dst	varchar(255) DEFAULT ""
,   clip_name_dst	varchar(255) DEFAULT ""
,   reason              int DEFAULT 0
) TYPE=MyISAM;
