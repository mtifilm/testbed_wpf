#
#  clip table.  One row per clip
#
CREATE TABLE clip(
    clip_id	int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   guid	varchar(64) DEFAULT ""
,   bin		varchar(255) DEFAULT ""
,   name	varchar(255) DEFAULT ""
,   date	varchar(25) DEFAULT ""
,   audio_frame int DEFAULT 0
,   image_frame int DEFAULT 0
,   base_audio_in int DEFAULT 0
,   base_image_in int DEFAULT 0
,   capture_in_progress		bool NOT NULL DEFAULT 0
) TYPE=MyISAM;
