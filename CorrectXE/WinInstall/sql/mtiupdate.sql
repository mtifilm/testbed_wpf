use mtidb;

alter table take add column audio_trim_in int DEFAULT -1;
alter table take add column audio_trim_out int DEFAULT -1;

alter table take add column ddr_trim_in int DEFAULT -1;
alter table take add column ddr_trim_out int DEFAULT -1;

alter table take add column perf_per_frame int DEFAULT -1;
alter table take add column perf_per_foot int DEFAULT -1;

alter table take add column description mediumtext DEFAULT "";

alter table clip add column audio_frame int DEFAULT 0;
alter table clip add column image_frame int DEFAULT 0;

alter table reel add column num_frame int DEFAULT 0;
alter table reel_evnet add column num_frame int DEFAULT 0;
