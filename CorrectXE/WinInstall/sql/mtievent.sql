#
#  event table.  One row per event
#
CREATE TABLE event (
    event_id	int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   clip_id	int NOT NULL
,   frame	int NOT NULL
,   audio_event int DEFAULT 0
,   INDEX (clip_id)
,   CONSTRAINT take_clip_fk FOREIGN KEY (clip_id)
		REFERENCES clip (clip_id)
) TYPE=MyISAM;
