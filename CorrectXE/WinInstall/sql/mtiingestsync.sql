#
#  ingest sync table.  One row per ingest sync
#
CREATE TABLE ingest_sync (
    ingest_sync_id	   int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   clip_id	   int NOT NULL
,   reel_id        int DEFAULT -1
,   camroll        varchar(10) NOT NULL
,   labroll        varchar(10) NOT NULL
,   shootdate      varchar(10) NOT NULL
,   ddr_in         int DEFAULT 0
,   ddr_out        int DEFAULT 0
,   src_tc         int DEFAULT 0
,   imageTC        varchar(16) DEFAULT ""
,   reel_in        int DEFAULT 0
,   keykode        varchar(22) DEFAULT ""
,   film_dimension int DEFAULT -1
,   max_room_delay int DEFAULT 0
,   last_update    timestamp
,   INDEX (clip_id)
,   CONSTRAINT ingest_sync_id FOREIGN KEY (clip_id)
		REFERENCES clip (clip_id)
) TYPE=MyISAM;
