#
#  reel table.  One row per reel
#
CREATE TABLE reel (
    reel_id	   int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   clip_id	   int NOT NULL
,   reel_name      mediumtext DEFAULT ""
,   drop_frame     bool DEFAULT 0
,   closed         bool DEFAULT 0
,   picture_start  varchar(16) DEFAULT "01:00:00:00"
,   reel_limit     varchar(16) DEFAULT "01:00:00:00"
,   num_frame      int DEFAULT 0
,   master_reel_id int DEFAULT -1
,   first_frame    int DEFAULT 0
,   first_frame_dub int DEFAULT 0
,   playout_option int DEFAULT 0
,   FieldD         mediumtext DEFAULT ""
,   FieldE         mediumtext DEFAULT ""
,   FieldF         mediumtext DEFAULT ""
,   FieldG         mediumtext DEFAULT ""
,   FieldH         mediumtext DEFAULT ""
,   FieldI         mediumtext DEFAULT ""
,   INDEX (clip_id)
,   CONSTRAINT reel_clip_fk FOREIGN KEY (clip_id)
		REFERENCES clip (clip_id)
) TYPE=MyISAM;

#
#  reel_event table.  One row for each event in the reel
#
CREATE TABLE reel_event (
    reel_event_id  int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   reel_id	   int NOT NULL
,   master_reel_id int NOT NULL
,   clip_id	   int NOT NULL
,   take_id	   int NOT NULL
,   num_frame      int DEFAULT 0
,   edit_master_only  bool DEFAULT 0
,   sort_order     int DEFAULT 0
,   last_update    timestamp
,   INDEX (reel_id)
,   CONSTRAINT reel_event_reel_fk FOREIGN KEY (reel_id)
		REFERENCES reel (reel_id)
) TYPE=MyISAM;
