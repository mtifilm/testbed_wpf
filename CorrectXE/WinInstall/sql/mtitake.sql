#
#  take table.  One row per scene/take
#
CREATE TABLE take (
    take_id	int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   clip_id	int NOT NULL
,   scene	varchar(10) NOT NULL
,   take	varchar(10) NOT NULL
,   camroll	varchar(10) NOT NULL
,   soundroll	varchar(10) NOT NULL
,   episode	varchar(10) NOT NULL
,   labroll	varchar(10) NOT NULL
,   shootdate	varchar(10) NOT NULL
,   playspeed	varchar(10) NOT NULL
,   resample    varchar(10) NOT NULL
,   tail_slate   bool DEFAULT 0
,   warning_flag bool DEFAULT 0
,   edit_master_only bool DEFAULT 0
,   quality_assurance bool DEFAULT 0
,   tape_layoff bool DEFAULT 0
,   audio_tc	float DEFAULT -1
,   audio_in	int DEFAULT -1
,   audio_out	int DEFAULT -1
,   audio_trim_in	int DEFAULT -1
,   audio_trim_out	int DEFAULT -1
,   ddr_tc	int DEFAULT -1
,   ddr_in	int DEFAULT -1
,   ddr_out	int DEFAULT -1
,   ddr_trim_in	int DEFAULT -1
,   ddr_trim_out int DEFAULT -1
,   count_event int DEFAULT -1
,   count_tk    int DEFAULT -1
,   count_audio int DEFAULT -1
,   imageTC	varchar(16) DEFAULT ""
,   keykode	varchar(22) DEFAULT ""
,   film_dimension int DEFAULT -1
,   max_room_delay int DEFAULT 0
,   audioLTC	varchar(16) DEFAULT ""
,   dropped     bool DEFAULT 0
,   last_update	timestamp
,   comments    mediumtext DEFAULT ""
,   description    mediumtext DEFAULT ""
,   inknumber	varchar(22) DEFAULT ""
,   ink_dimension int DEFAULT 0
,   ddr_media_in	int DEFAULT -1
,   audio_media_in	int DEFAULT -1
,   INDEX (clip_id)
,   CONSTRAINT take_clip_fk FOREIGN KEY (clip_id)
		REFERENCES clip (clip_id)
) TYPE=MyISAM;
