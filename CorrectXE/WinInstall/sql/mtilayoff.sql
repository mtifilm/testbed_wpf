#
#  layoff table.  One row for each layoff that has been defined
#
CREATE TABLE layoff (
    layoff_id      int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   reel_id        int NOT NULL
,   master_reel_id int NOT NULL
,   clip_id        int NOT NULL
,   layoff_name    mediumtext DEFAULT ""
,   config_path    mediumtext DEFAULT ""
,   config         mediumtext DEFAULT ""
,   existing_stock int DEFAULT 0
,   layoff_flag    int DEFAULT 0
,   INDEX (reel_id)
,   CONSTRAINT layoff_reel_fk FOREIGN KEY (reel_id)
		REFERENCES reel (reel_id)
) TYPE=MyISAM;

#
#  layoff_event table.  One row for each event on the layoff
#
CREATE TABLE layoff_event (
    layoff_event_id  int AUTO_INCREMENT NOT NULL PRIMARY KEY
,   layoff_id	     int NOT NULL
,   reel_event_id    int NOT NULL
,   clip_id          int NOT NULL
,   take_id          int NOT NULL
,   recorded	     bool DEFAULT 0
,   last_update    timestamp
,   INDEX (layoff_id)
,   CONSTRAINT layoff_event_layoff_fk FOREIGN KEY (layoff_id)
		REFERENCES layoff (layoff_id)
) TYPE=MyISAM;
