------------------------------------------------------------------------------
                     DRS Nova 4.2 release notes 3/31/2020
------------------------------------------------------------------------------

PROJECT MANAGER
---------------

- Sped up project manager list generation.

- Change "Frame Rate Changed" clip status to show the value it was changed to as, e.g., "FPS --> 24".

- Fix a bug where when you create version clips and want the history to go to a separate "metadata tree", you would get error pop-ups and the history would end up going to the media folder instead.


PDL VIEWER
----------

- After double-clicking on a PDL entry, the focus is no longer switched to the main window.

- Fix issue in the PDL viewer where key presses were no longer passed to the main window after double-clicking on an entry.


DEFLICKER TOOL
--------------

- Fix issue in Global Deflicker where graph does not update while graphing

- Fix preset selection shortcuts (1, 2, 3, 4) in Zonal Flicker. They were changing Global Flicker presets instead.


GRAIN TOOL
----------

- Fixed an issue where, when adding grain, the grain pattern was often exactly the same for each of the 4 frames in an iteration.


PAINT TOOL
----------

- Fix bug in paint where doing a GOV after a reveal with a "locked" import frame, the import frame index was wiped out so was then reset to be the first frame of the clip.

- Fix bug in paint reveal mode with "locked" import timecode. When you ran a macro, on one frame it was using an import offset that was off by one, then leaving it set to the new, erroneous value.


STABILIZE TOOL
--------------

- Improve paralleization of Auto-Stabilization analysis by trying to use all available processors.

- Added "Source is Animation" option to Auto-Stabilize. This allows the tool to operate correctly when the animation contains duplicate frames.


GENERAL
-------

- The calculation of number of processors available for tool processing now maxes out at 60 instead of 32.



------------------------------------------------------------------------------
                     DRS Nova 4.2 release notes 1/12/2020
------------------------------------------------------------------------------

DPX FILES
---------
- Added a command in the Project Manager to strip the alpha channel from RGBA media, turning them into 25% smaller RGB files.

- Added a command in the Project Manager to set the frame rate values in the DPX file headers of the clip's media.

- Fixed a potential crash when a clip contains files both with and without an alpha channel.


EXR FILES
---------
- Fixed issue where output may be uncompressed files, rather than using the compression mode of the source file.

- Fixed issue where the timecode was not being read correctly from the EXR file header.

- Fixed issues that occurred when using the Scratch tool on EXR files.


PROJECT MANAGER
---------------
- Added a "Relink Media Folder" to the clip pop-up menu which can be used to reset the media pointers of a clip when the media is moved to a different folder, avoiding having to make new clips.

- Added a "Strip alpha channel"  to the clip pop-up menu which can be used to remove the alpha channel information from RGBA media. This is only implemented for clips based on RGBA DPX files.

- Fixed issues with using UNC files names (look like \\machine_name\path, as opposed to paths with a drive letter like D:\path).


DRS TOOL
--------
- Implemented vastly improved Color Bump algorithm.


DEBRIS TOOL (renamed, was named Autofilter)
-------------------------------------------
- Enable Max Size slider for alpha filters.

- Fixed memory leak when selecting Min Size > 1.

- Fixed an issue where a crash might occur when switching to the Debris tool after starting DRS Nova with the Scene Cut timeline hidden.


DEFLICKER TOOL
--------------
- Added constraints to Zonal mode, allowing setting of either head and tail anchor frames, or a single reference frame that other frames are matched to.

- Implemented improved smoothing algorithm for Zonal mode rendering.

- Fix PDL rendering crash in Zonal mode if the analysis database could not be read.

- Greatly sped up creation of PDLs in Zonal mode.

- Fix Global mode issues with the blue line not appearing or being incorrect.


GRAIN TOOL
----------
- Implemented channel mask for grain creation so different grain can be added to the individual R, G, and B channels.


PAINT TOOL
----------
- Fixed crash when Paint brush is too big (radius > 2048).

- Fixed issues in "Alternate Clip" mode when in "compare" mode.

- Fixed an issue where Paint changes could be lost if the History file could not be written to.


GENERAL
-------
- Fixed a crash when trying to play a corrupt media file.

- THe size of the font of the timecode information at the bottom of the main widow is now changed dynamically depending on the size of the main window.


