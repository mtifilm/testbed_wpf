// This is NOT designed to be included by itself but
// included only in IppArray.h
#pragma once
#ifndef Ipp8uArrayH
#define Ipp8uArrayH

#include "IppArray.h"
using ScratchArray = Ipp8uArray;

class MTI_IPPARRAY_API Ipp8uArray : public IppArray<Ipp8u>
{

public:
	Ipp8uArray() = default;;

	Ipp8uArray(const MtiSize &size) : IppArray <Ipp8u>(size) {}
	Ipp8uArray(const MtiSize &size, Ipp8u *source, bool copyData = false) : IppArray(size, source, copyData) {}

protected:	
	Ipp8uArray(const Ipp8uArray &rhs, const IppiRect &roi) : IppArray(rhs, roi) {}
public:

	/// Specific functions
	void set(const vector<Ipp8u> &value);
	void normalDistribution(double mean, double stddev);
	void uniformDistribution(Ipp8u low, Ipp8u high);
	vector<Ipp64f> sum() const;
	vector<Ipp8u> median(const IppiRect &roi) const;
	vector<Ipp8u> median() const;

	void copyFromPod(Ipp8u *source, int size);

	// This is just to mimic the matlab function
	Ipp8uArray fliplr() const { return mirror(IppiAxis::ippAxsVertical); }
	Ipp8uArray mirror(IppiAxis flip) const;

	Ipp8uArray duplicate(const IppiRect &roi) const
	{
		Ipp8uArray result;
		result <<= (*this)(roi);
		return result;
	}

	Ipp8uArray duplicate() const
	{
		Ipp8uArray result;
		result <<= *this;
		return result;
	}

	// Conversion operators
	Ipp8uArray &operator <<=(const IppArray<Ipp8u>& rhs) { convertFrom(rhs); return *this; }
	Ipp8uArray &operator <<=(const IppArray<Ipp16u>& rhs) { convertFrom(rhs); return *this; }
	Ipp8uArray &operator <<=(const IppArray<Ipp32s>& rhs) { convertFrom(rhs); return *this; }
	Ipp8uArray &operator <<=(const IppArray<Ipp32f>& rhs) { convertFrom(rhs); return *this; }

	// Arithmetic, add an array and an array
	Ipp8uArray operator +(const IppArray<Ipp8u> &rhs) const;
	Ipp8uArray operator -(const IppArray<Ipp8u> &rhs) const;
	Ipp8uArray operator *(const IppArray<Ipp8u> &rhs) const;
	Ipp8uArray operator / (const IppArray<Ipp8u> &rhs) const;

	// Arithmetic, add an array and a scaler 
	Ipp8uArray operator +(const Ipp8u scalar) const;
	Ipp8uArray operator -(const Ipp8u scalar) const;
	Ipp8uArray operator *(const Ipp8u scalar) const;
	Ipp8uArray operator /(const Ipp8u scalar) const;

	// Arithmetic, add an array in place 
	Ipp8uArray &operator +=(IppArray<Ipp8u> &rhs);
	Ipp8uArray &operator -=(IppArray<Ipp8u> &rhs);
	Ipp8uArray &operator *=(IppArray<Ipp8u> &rhs);
	Ipp8uArray &operator /=(IppArray<Ipp8u> &rhs);

	// Arithmetic, add an scaler in place 
	Ipp8uArray &operator +=(const Ipp8u scalar);
	Ipp8uArray &operator -=(const Ipp8u scalar);
	Ipp8uArray &operator *=(const Ipp8u scalar);
	Ipp8uArray &operator /=(const Ipp8u scalar);

	void minEvery(const Ipp8uArray &operand);

	Ipp8uArray operator()(const MtiRect roi) const { return Ipp8uArray(*this, roi); }
	Ipp8uArray operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData = false) const;

	void Y2RGB(const Ipp8uArray & src);
	MtiPlanar<Ipp8uArray> copyToPlanar() const;
	void copyFromPlanar(const MtiPlanar<Ipp8uArray> &planes);

	vector<Ipp64f> L1Norm() const;
	vector<Ipp64f> L2Norm() const;
	void L1NormDiff(const IppArray<Ipp8u> &rhs, Ipp64f result[]) const;
	void L2NormDiff(const IppArray<Ipp8u> &rhs, Ipp64f result[]) const;
	Ipp32fArray crossCorrNorm(const IppArray<Ipp8u> &kernalImage, IppiROIShape roiShape = IppiROIShape::ippiROIFull) const;
	
	Ipp8uArray compareC(Ipp8u c, IppCmpOp cmpOp) const;
	Ipp8uArray xorC(Ipp8u value) const;
	void maxAndIndex(Ipp8u *maxValues, MtiPoint *indices) const;

   Ipp8uArray applyBoxFilter(IppiSize boxSize) const;
   Ipp8uArray applyBilateralFilter(int radius) const;
   Ipp8uArray applyAutoContrastFilter() const;
   Ipp8uArray equalizeHistogram() const;

   Ipp8uArray dilate(const Ipp8uArray &mask, Ipp8uArray &scratchArray) const;
   Ipp8uArray erode(const Ipp8uArray &mask, Ipp8uArray &scratchArray) const;

	vector<Ipp64f> mean() const;
	vector<Ipp64f> dotProduct(const IppArray &rhs) const;

	Ipp8uArray resize(IppiSize newSize) const;

	// Returns an array of the entire data 
	Ipp8uArray baseArray() const
	{
		auto result = *this;
		result.resetRoi({ { 0,0 }, getBaseSize() });
		return result;
	}

	Ipp8uArray transpose() const;
	Ipp8uArray toGray() const;
	Ipp8uArray toGray(const float weights[]) const;
	Ipp32fArray bwdist(Ipp8uArray &scratch) const;

	explicit operator Ipp16uArray() const
	{
		Ipp16uArray result;
		result <<= *this;
		return result;
	}

	explicit operator Ipp32sArray() const
	{
		Ipp32sArray result;
		result <<= *this;
		return result;
	}

	explicit operator Ipp32fArray() const
	{
		Ipp32fArray result;
		result <<= *this;
		return result;
	}

	static Ipp8uArray createDisk(int radius);

protected:
    void convertFrom(const IppArray<Ipp8u> &rhs);
	void convertFrom(const IppArray<Ipp16u> &rhs);
	void convertFrom(const IppArray<Ipp32s> &rhs);
	void convertFrom(const IppArray<Ipp32f> &rhs);

	// These need to be moved somewhere
	// medianQuick should be templated.
	// No time now
public:
	static Ipp8u median(vector<Ipp8u> &values);
	static Ipp8u medianSort(vector<Ipp8u> &values);
	static Ipp8u medianQuick(vector<Ipp8u> &values);
};


#endif 


