#ifndef FastHistogramIppH
#define FastHistogramIppH
#pragma once

#include "HistogramIpp.h"

#include <memory>
#include <deque>

class MTI_IPPARRAY_API FastHistogramIpp final
{
public:
	static vector<vector<Ipp32u>> compute(const Ipp32fArray &image, int bins, float dataMin, float dataMax);
	static Ipp32sArray computeIpp(const Ipp32fArray & image, int bins, float dataMin, float dataMax);
	static vector<vector<Ipp32u>> compute(const Ipp16uArray &image, int bins, float dataMin, float dataMax);	static vector<vector<Ipp32f>> computeProbability(const Ipp32fArray &image, int bins, float dataMin, float dataMax);

	const int lowwater = 80;
	const int highwater = 100;
	std::shared_ptr<HistogramIpp> getHistogramIpp(const Ipp32fArray &image, int bins, float dataMin, float dataMax);
	std::shared_ptr<HistogramIpp> getHistogramIpp(const Ipp16uArray &image, int bins, float dataMin, float dataMax);
	std::shared_ptr<HistogramIpp> getHistogramIpp(const IppiSize &size, int channels, IppDataType dataType, int bins, float dataMin, float dataMax);


private:
	FastHistogramIpp();

	static FastHistogramIpp &getInstance()
	{
		return _fastHistogram;
	}

	FastHistogramIpp(FastHistogramIpp const&);       // This must not be Implemented
	void operator=(FastHistogramIpp const&);          // This must not be Implemented

private:
	std::mutex _lockMutex;
	std::deque<std::shared_ptr<HistogramIpp>> _flyweightCache;
	static FastHistogramIpp _fastHistogram;
};
#endif
