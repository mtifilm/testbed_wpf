#pragma once
#ifndef Ipp32sArrayH
#define Ipp32sArrayH

#include "IppArray.h"
class Ipp32fArray;

class MTI_IPPARRAY_API Ipp32sArray : public IppArray<Ipp32s>
{

public:
	Ipp32sArray() = default;

	Ipp32sArray(const MtiSize &size) : IppArray <Ipp32s>(size) {}
	Ipp32sArray(const MtiSize &size, Ipp32s *source, bool copyData = false) : IppArray(size, source, copyData) {}

protected:
	Ipp32sArray(const IppArray<Ipp32s> & rhs, const IppiRect &roi) : IppArray(rhs, roi) {}

public:

	// Specific functions
	void set(const vector<Ipp32s> &value);
	void normalDistribution(double mean, double stddev);
	void uniformDistribution(Ipp32s low, Ipp32s high);
	vector<Ipp64f> sum() const;
	vector<Ipp32s> median(const IppiRect &roi) const;
	vector<Ipp32s> median() const;

	void copyFromPod(Ipp32s *source, int size);

	Ipp32sArray duplicate(const IppiRect &roi) const
	{
		Ipp32sArray result;
		result <<= (*this)(roi);
		result.setOriginalBits(getOriginalBits());
		return result;
	}

	Ipp32sArray duplicate() const
	{
		Ipp32sArray result;
		result <<= *this;
		result.setOriginalBits(getOriginalBits());
		return result;
	}

	// Conversion operators
	Ipp32sArray &operator <<=(const IppArray<Ipp8u>& rhs) { convertFrom(rhs); return *this; }
	Ipp32sArray &operator <<=(const IppArray<Ipp16u>& rhs) { convertFrom(rhs); return *this; }
	Ipp32sArray &operator <<=(const IppArray<Ipp32s>& rhs) { convertFrom(rhs); return *this; }
	Ipp32sArray &operator <<=(const IppArray<Ipp32f>& rhs) { convertFrom(rhs); return *this; }

	// Arithmetic &operators
	Ipp32sArray operator +(IppArray<Ipp32s> &rhs) const;
	Ipp32sArray operator -(IppArray<Ipp32s> &rhs) const;
	Ipp32sArray operator *(IppArray<Ipp32s> &rhs) const;
	Ipp32sArray operator /(IppArray<Ipp32s> &rhs) const;

	Ipp32sArray operator +(const Ipp32s scalar) const;
	Ipp32sArray operator -(const Ipp32s scalar) const;
	Ipp32sArray operator *(const Ipp32s scalar) const;
	Ipp32sArray operator / (const Ipp32s scalar) const;

   // Arithmetic, add an scalar in place 
   Ipp32sArray &operator +=(const Ipp32s scalar);
   Ipp32sArray &operator -=(const Ipp32s scalar);
   Ipp32sArray &operator *=(const Ipp32s scalar);
   Ipp32sArray &operator /=(const Ipp32s scalar);

	Ipp32sArray operator()(const MtiRect roi) const { return Ipp32sArray(*this, roi); }
	Ipp32sArray operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData = false) const;

	void Y2RGB(const Ipp32sArray & src);
	MtiPlanar<Ipp32sArray> copyToPlanar() const;
	void copyFromPlanar(const MtiPlanar<Ipp32sArray> &planes);

	vector<Ipp64f> L1Norm() const;
	vector<Ipp64f> L2Norm() const;
	//vector<Ipp64f> L1NormDiffV(const IppArray<Ipp32s> &rhs) const;
	//vector<Ipp64f> L2NormDiffV(const IppArray<Ipp32s> &rhs) const;

	vector<Ipp64f> mean() const;
	vector<Ipp64f> dotProduct(const IppArray<Ipp32s> &rhs) const;

	Ipp32sArray fliplr() const { return mirror(IppiAxis::ippAxsVertical); }
	Ipp32sArray mirror(IppiAxis flip) const;

    void L1NormDiff(const IppArray<Ipp32s> &rhs, Ipp64f result[]) const;
	void L2NormDiff(const IppArray<Ipp32s> &rhs, Ipp64f result[]) const;
	Ipp32fArray crossCorrNorm(const IppArray<Ipp32s> &kernalImage, IppiROIShape roiShape = IppiROIShape::ippiROIFull) const;
	void maxAndIndex(Ipp32s *maxValues, MtiPoint *indices) const;
	std::pair<std::vector<Ipp32s>, std::vector<MtiPoint>> maxAndIndex() const;

	Ipp32sArray transpose() const;

	// Returns an array of the entire data 
	Ipp32sArray baseArray() const
	{
		auto result = *this;
		result.resetRoi({ { 0,0 }, getBaseSize() });
		return result;
	}

	static Ipp32sArray logLut(int rows, Ipp32s maxValue, Ipp32s startValue = 100);
	static Ipp32sArray linspace(Ipp32s startValue, Ipp32s endValue, int width);
	
	operator Ipp32fArray() const;
	operator Ipp16uArray() const;
	operator Ipp8uArray() const;


protected:
	void convertFrom(const IppArray<Ipp8u> &rhs);
	void convertFrom(const IppArray<Ipp16u> &rhs);
	void convertFrom(const IppArray<Ipp32s> &rhs);
	void convertFrom(const IppArray<Ipp32f> &rhs);

	// These need to be moved somewhere
	// medianQuick should be templated.
	// No time now
public:
	static Ipp32s median(vector<Ipp32s> &values);
	static Ipp32s medianSort(vector<Ipp32s> &values);
	static Ipp32s medianQuick(vector<Ipp32s> &values);
};

#endif