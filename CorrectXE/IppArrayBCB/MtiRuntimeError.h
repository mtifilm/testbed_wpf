#pragma once
#ifndef MtiRuntimeErrorH
#define MtiRuntimeErrorH
#include <string>
#include "IppArrayLibInt.h"

#define THROW_MTI_RUNTIME_ERROR(A)\
{\
	MTIostringstream p_os;\
	p_os << __func__ << " (" << __LINE__ << "): " << (A); \
	throw MtiRuntimeError(p_os.str()); \
}

// This class not really needed, its to make Borland debugging possible
#pragma warning(push)
#pragma warning( disable : 4275)
#pragma warning( disable : 4251)

class MTI_IPPARRAY_API MtiRuntimeError :
	public std::runtime_error
{
public:
	MtiRuntimeError(const std::string &message);

	std::string getMessage() const { return _errorMessage; }

private:
	std::string _errorMessage;
};

#pragma warning(pop)
#endif

