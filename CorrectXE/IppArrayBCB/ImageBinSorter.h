#pragma once
#ifndef ImageBinSorterH
#define ImageBinSorterH

#include "Ippheaders.h"

// This class is designed to take an 2D IppArray
// and produce bins of linear indexes that are sorted by 
// intensity values in the IppArray with accuracy 16 bits
// 
//  The class will also gather values from another array
// of same size into bins based on the previous index and
// sort these values.
//
//  This is specific to a specific type of image processing
//  Because of memory allocation, the results are internally
//  stored and thus code is not thread safe and dangerous.
//  
class MTI_IPPARRAY_API ImageBinSorter
{
public:
	ImageBinSorter();

	int getNumberOfBins() const { return _numberOfBins; }
	const vector<Ipp32fArray> &sortDataToBins(const Ipp16uArray &keyImage, const Ipp32fArray &dataImage, int numberOfBins);
	const vector<Ipp32fArray> &sortDataToBins(const Ipp32fArray &keyImage, const Ipp32fArray &dataImage, int numberOfBins);

	const vector<Ipp32fArray> &getValueBins() const { return _bins32f; }
	const vector<Ipp32sArray> &getIndexBins() const  { return _binIntervals;  }

protected:
	void sortIndexes(const Ipp16uArray &image, int numberOfBins);
	void sortIndexes(const Ipp32fArray &image, int numberOfBins);
	void gatherAndSort(const Ipp32fArray &dataImage);
	void allocateBinStorage(int numberOfBins, int totalPixels);

private:
	int _numberOfBins = -1;
	Ipp32sArray _pixelValueIndexes;
	vector<Ipp32s> _edges;

	// number of bins are small and these remain the
	// same for images size and bins. So we we can use
	// Ipp arrays here.
	vector<Ipp32sArray> _binIntervals;
	vector<Ipp32fArray> _bins32f;
};


#endif

