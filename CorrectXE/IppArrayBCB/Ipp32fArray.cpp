#include "IppArray.h"
#include "Ippheaders.h"
#include <cassert>
#include "HistogramIpp.h"
#include "IppAgnostic.h"
#include <sstream>
#include <random>
#include <algorithm>
#include <functional>
#include <cmath>
#include "resampler.h"

#include "Ipp32fArray.h"
#include "MtiAncillaryTemplates.h"
#include "MtiMath.h"

namespace
{
	static std::default_random_engine generator;

	// Pseudo-IPP call!
	IppStatus IppResize_C1R_or_C3R(Ipp32f* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32f* pDst, IppiSize dstSize,
		Ipp32s dstStep, Ipp32u numChannels);

	IppStatus IppResizeAntiAliasing_C1R_or_C3R(Ipp32f* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32f* pDst, IppiSize dstSize,
		Ipp32s dstStep, Ipp32u numChannels);
}

void Ipp32fArray::normalDistribution(double mean, double stddev)
{
	std::normal_distribution<float> distribution((float)mean, (float)stddev);

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto c = 0; c < getComponents(); c++)
			{
				*inp++ = distribution(generator);
			}
		}
	}
}

#define MY_PI 3.141592653589793238462643383279502884

Ipp32fArray Ipp32fArray::normalPdf(int n, double mean, double stddev)
{
	Ipp32fArray pdfArray;
	normalPdf(n, mean, stddev, pdfArray);
	return pdfArray;
}

void Ipp32fArray::normalPdf(int n, double mean, double stddev, Ipp32fArray &pdfArray)
{
	pdfArray.createStorageIfEmpty(1, n, 1);

	auto ss = 2.0 * std::pow(stddev, 2);
	auto cols = n;
	auto offset = cols / 2;

	auto p = pdfArray.getRowPointer(0);
	for (auto c = 0; c < cols; c++)
	{
		double v = c - offset - mean;
		v = v * v / ss;
		v = std::exp(-v) / stddev / std::sqrt(2 * MY_PI);
		*p++ = float(v);
	}

	return;
}

void Ipp32fArray::uniformDistribution(float low, float high)
{
	std::uniform_real_distribution<float> distribution(low, high);
	///generator.seed(static_cast<unsigned int>(time(nullptr)));

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto d = 0; d < getComponents(); d++)
			{
				*inp++ = distribution(generator);
			}
		}
	}
}

IPP_ARRAY_APPLY_VALUE(Ipp32fArray, set, Ipp32f, ippiSet_32f)

void Ipp32fArray::convertFrom(const std::vector<Ipp64f>& rhs, const MtiSize rhsSize)
{
	createStorageIfEmpty(rhsSize);

	const auto roiSize = getSize();
	if (roiSize != rhsSize)
	{
		THROW_MTI_RUNTIME_ERROR("Dimensions of IppArray operands must match!");
	}

	if (int(rhs.size()) != rhsSize.volume())
	{
		THROW_MTI_RUNTIME_ERROR("Number of elements must agree")
	}

	// Brute force convert
	auto sourcePtr = rhs.data();
	for (auto rp : rowIterator())
	{
		for (auto c = 0; c < getWidth()*getComponents(); c++)
		{
			rp[c] = float(*sourcePtr++);
		}
	}
}

vector<Ipp64f> Ipp32fArray::sum() const
{
	auto size = getSize();
	vector<Ipp64f> result(getComponents());
	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiSum_32f_C1R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate));
		break;

	case 3:
		IppThrowOnError(ippiSum_32f_C3R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate));
		break;

	default:
		throwComponentError();
	}

	return result;
}

void Ipp32fArray::copyFromPod(Ipp32f *source, int size)
{
	auto roiSize = getSize();
	auto roi = getRoi();

	if ((size / getComponents()) != (roi.height * roi.width))
	{
		THROW_MTI_RUNTIME_ERROR(string("Size, roi area mismathc in ") + __func__);
	}

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp32f));

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiCopy_32f_C1R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize));
		break;

	case 3:
		IppThrowOnError(ippiCopy_32f_C3R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiCopy_32f_C4R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
	}
}

IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32fArray, Ipp8u, ippiConvert_8u32f)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32fArray, Ipp16u, ippiConvert_16u32f)
//void Ipp32fArray::convertFrom(const IppArray<Ipp32s> &rhs) { THROW_MTI_RUNTIME_ERROR("Conversion not supported"); }
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32fArray, Ipp32f, ippiCopy_32f)

void Ipp32fArray::convertFrom(const IppArray<Ipp32s> &rhs)
{
	if (rhs.getHeight() == 0 || rhs.getWidth() == 0)
	{
		return;
	}

	createStorageIfEmpty(rhs.getSize());

	auto roiSize = getSize();
	auto rhsSize = rhs.getSize();
	if (roiSize != rhsSize)
	{
		throw std::runtime_error("Dimensions of IppArray operands must match!");
	}

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiConvert_32s32f_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));
		break;

	case 2:
	case 3:
	case 4:
		for (auto r = 0; r < getHeight(); r++)
		{
			auto sp = rhs.getRowPointer(r);
			auto dp = getRowPointer(r);
			IppThrowOnError(ippsConvert_32s32f(sp, dp, getWidth()*getComponents()));

			//	for (auto c = 0; c < getWidth()*getComponents(); c++)
			//	{
			//		*d++ = Ipp32f(*s++);
			//	}
		}
		break;

	default:
		throwComponentError();
	}
}

Ipp32fArray Ipp32fArray::mirror(IppiAxis flip) const
{
	Ipp32fArray result(getSize());

	auto roiSize = getSize();
	auto roi = getRoi();

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp32f));

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMirror_32f_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 3:
		IppThrowOnError(ippiMirror_32f_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 4:
		IppThrowOnError(ippiMirror_32f_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	default:
		throwComponentError();
	}

	return result;
}

IPP_ARRAY_INPLACE(Ipp32fArray, minEveryInplace, ippiMinEvery_32f)

void Ipp32fArray::Y2RGB(const Ipp32fArray & src)
{
	if (src.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Input to Y2RGB must have only one component!");
	}

	if (getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Target of Y2RGB must have channels 3!");
	}

	auto &roi = getRoi();
	auto &srcRoi = src.getRoi();
	if (roi.width != srcRoi.width || roi.height != srcRoi.height)
	{
		THROW_MTI_RUNTIME_ERROR("Dimension mismatch in input to Y2RGB!");
	}

	copyFromPlanar({ src, src, src });
}

MtiPlanar<Ipp32fArray> Ipp32fArray::copyToPlanar() const
{
	if (getComponents() == 1)
	{
		return { *this };
	}

	// This is the ugliest code I've every written, but dementia does this 
	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	MtiPlanar<Ipp32fArray> result;
	for (auto i : range(getComponents()))
	{
		result.emplace_back(Ipp32fArray({ getSize(), 1 }));
		result.back().setOriginalBits(getOriginalBits());
	}

	if (getComponents() == 3)
	{
		Ipp32f *dstPtrs[3] = { result[0].data(), result[1].data(), result[2].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32f_C3P3R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	if (getComponents() == 4)
	{
		Ipp32f *dstPtrs[4] = { result[0].data(), result[1].data(), result[2].data(), result[3].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32f_C4P4R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	THROW_MTI_RUNTIME_ERROR("Number of components must be 1, 3 or 4");
}

void Ipp32fArray::copyFromPlanar(const MtiPlanar<Ipp32fArray> &planes)
{
	auto channels = int(planes.size());
	if ((channels != 1) && (channels != 3) && (channels != 4))
	{
		THROW_MTI_RUNTIME_ERROR("copyFromPlanar needs a 1, 3 or 4 size planar array");
	}

	// See if we are a null array
	this->createStorageIfEmpty(planes[0].getHeight(), planes[0].getWidth(), channels);

	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	// Planar arrays are same size, so just check the first
	if (roiSize.width != planes[0].getRoi().width || roiSize.height != planes[0].getRoi().height)
	{
		THROW_MTI_RUNTIME_ERROR("All operands of CopyFromPlanar must be the same size!");
	}

	setOriginalBits(planes[0].getOriginalBits());

	for (auto i = 1; i < channels; i++)
		if (planes[0].getRowPitchInBytes() != planes[i].getRowPitchInBytes())
		{
			THROW_MTI_RUNTIME_ERROR("All sources of copyFromPlanar must have same row pitch!");
		}

	if (channels == 1)
	{
		*this <<= planes[0];
		return;
	}

	if (channels == 3)
	{
		Ipp32f *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32f_P3C3R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
		return;
	}

	if (channels == 4)
	{
		Ipp32f *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data(), planes[3].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32f_P4C4R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
	}
}

Ipp32fArray  &Ipp32fArray::operator<<=(const std::vector<Ipp64f>& rhs)
{
	convertFrom(rhs, { int(rhs.size()) });
	return *this;
}

IPP_OPERATOR_ARRAY(Ipp32fArray, +, Ipp32f, ippiAdd_32f)
IPP_OPERATOR_ARRAY(Ipp32fArray, -, Ipp32f, ippiSub_32f)
IPP_OPERATOR_ARRAY(Ipp32fArray, *, Ipp32f, ippiMul_32f)
IPP_OPERATOR_ARRAY(Ipp32fArray, / , Ipp32f, ippiDiv_32f)

IPP_OPERATOR_ARRAY_INPLACE(Ipp32fArray, +=, Ipp32f, ippiAdd_32f)
IPP_OPERATOR_ARRAY_INPLACE(Ipp32fArray, -=, Ipp32f, ippiSub_32f)
IPP_OPERATOR_ARRAY_INPLACE(Ipp32fArray, *=, Ipp32f, ippiMul_32f)
IPP_OPERATOR_ARRAY_INPLACE(Ipp32fArray, /=, Ipp32f, ippiDiv_32f)

IPP_OPERATOR_SCALAR(Ipp32fArray, +, Ipp32f, ippiAddC_32f)
IPP_OPERATOR_SCALAR(Ipp32fArray, -, Ipp32f, ippiSubC_32f)
IPP_OPERATOR_SCALAR(Ipp32fArray, *, Ipp32f, ippiMulC_32f)
IPP_OPERATOR_SCALAR(Ipp32fArray, / , Ipp32f, ippiDivC_32f)

IPP_OPERATOR_SCALAR_INPLACE(Ipp32fArray, +=, Ipp32f, ippiAddC_32f)
IPP_OPERATOR_SCALAR_INPLACE(Ipp32fArray, -=, Ipp32f, ippiSubC_32f)
IPP_OPERATOR_SCALAR_INPLACE(Ipp32fArray, *=, Ipp32f, ippiMulC_32f)
IPP_OPERATOR_SCALAR_INPLACE(Ipp32fArray, /=, Ipp32f, ippiDivC_32f)

// Select operator for different types of copy
Ipp32fArray Ipp32fArray::operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData) const
{
	return SelectFromFunction(*this, probeRoi, copyType, utilizeExtrinsicData);
}

Ipp32fArray Ipp32fArray::fromPixelIndices(const IppArray<Ipp32s>& indices) const
{
	MTIassert(indices.getComponents() == 1);
	Ipp32fArray result({ indices.getSize(), getComponents() });

	if (getComponents() == 1)
	{
		for (auto r = 0; r < indices.getHeight(); r++)
		{
			auto ip = indices.getRowPointer(r);
			auto rp = result.getRowPointer(r);
			for (auto c = 0; c < indices.getWidth(); c++)
			{
				*rp++ = (*this)[*ip++];
			}
		}

		return result;
	}

	for (auto r = 0; r < indices.getHeight(); r++)
	{
		auto ip = indices.getRowPointer(r);
		auto rp = result.getRowPointer(r);
		for (auto c = 0; c < indices.getWidth(); c++)
		{
			// Copy the pixel
			for (auto d = 0; d < getComponents(); d++)
			{
				*rp++ = (*this)[*ip + d];
			}

			++ip;
		}
	}

	return result;
}

std::vector<Ipp32f> Ipp32fArray::fromPixelIndices(const vector<Ipp32s>& indices) const
{
	std::vector<Ipp32f> result(indices.size() * getComponents());
	auto rp = result.data();
	auto ip = indices.data();

	if (getComponents() == 1)
	{
		for (auto c = 0; c < indices.size(); c++)
		{
			*rp++ = (*this)[*ip++];
		}

		return result;
	}

	for (auto c = 0; c < indices.size(); c++)
	{
		// Copy the pixel
		for (auto d = 0; d < getComponents(); d++)
		{
			*rp++ = (*this)[*ip + d];
		}

		++ip;
	}

	return result;
}

//IPP_ARRAY_ARRAY_ARRAY(Ipp32f, AbsDiffInPlace, ippiAbsDiff_32f);
void Ipp32fArray::absDiff(const Ipp32fArray & input, Ipp32fArray &result)
{
	throwOnArraySizeChannelMismatch(input);

	result.createStorageIfEmpty(getSize());
	throwOnArraySizeChannelMismatch(result);
	auto size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiAbsDiff_32f_C1R(data(), getRowPitchInBytes(), input.data(), input.getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));
		break;

	default:
		throwComponentError();
		break;
	}

	invalidateIfConjoined(*this, result);
}

Ipp8uArray Ipp32fArray::compareC(Ipp32f c, IppCmpOp cmpOp) const
{
	auto result = Ipp8uArray(getSize());
	auto pSrc = data();
	auto srcStep = getRowPitchInBytes();
	auto pDst = result.data();
	auto dstStep = result.getRowPitchInBytes();
	IppiSize roiSize = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(
			ippiCompareC_32f_C1R(pSrc, srcStep, c, pDst, dstStep, roiSize, cmpOp)
		);
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

vector<Ipp32f> Ipp32fArray::median(const IppiRect &roi) const
{
	vector<Ipp32f> result;
	auto vects = toVector(roi);
	for (auto &vec : vects)
	{
		result.push_back(median(vec));
	}

	return result;
}

vector<Ipp32f> Ipp32fArray::median() const
{
	return median({ 0, 0, getWidth(), getHeight() });
}

Ipp32f Ipp32fArray::medianSort(vector<Ipp32f> &values)
{
	std::sort(values.begin(), values.end());
	auto n = values.size();
	auto m = n / 2;

	if ((n % 2) == 0)
	{
		return (values[m - 1] + values[m]) / 2;;
	}

	return values[m];
}

Ipp32f Ipp32fArray::median(vector<Ipp32f> &values)
{
	return medianSort(values);
	//return medianQuick(values);
}

Ipp32f Ipp32fArray::medianQuick(vector<Ipp32f> &values)
{
	int low, high;
	int median;
	int middle, cl, hh;
	Ipp32f *arr = values.data();
	int n = (int)values.size();

	low = 0; high = n - 1; median = (low + high) / 2;
	for (;;)
	{
		if (high <= low) /* One element only */
			return arr[median];

		if (high == low + 1)
		{  /* Two elements only */
			if (arr[low] > arr[high])
				std::swap(arr[low], arr[high]);

			return arr[median];
		}

		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])    std::swap(arr[middle], arr[high]);
		if (arr[low] > arr[high])       std::swap(arr[low], arr[high]);
		if (arr[middle] > arr[low])     std::swap(arr[middle], arr[low]);

		/* Swap low item (now in position middle) into position (low+1) */
		std::swap(arr[middle], arr[low + 1]);

		/* Nibble from each end towards middle, swapping items when stuck */
		cl = low + 1;
		hh = high;
		for (;;)
		{
			do cl++; while (arr[low] > arr[cl]);
			do hh--; while (arr[hh] > arr[low]);

			if (hh < cl)
				break;

			std::swap(arr[cl], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		std::swap(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = cl;
		if (hh >= median)
			high = hh - 1;
	}
}

Ipp32f Ipp32fArray::medianQuick1D(const Ipp32fArray &values)
{
	Ipp32fArray temp;
	temp <<= values;

	int low, high;
	int median;
	int middle, cl, hh;
	Ipp32f *arr = temp.data();
	int n = (int)temp.area();

	low = 0;
	high = n - 1;
	median = (low + high) / 2;
	for (;;)
	{
		if (high <= low) /* One element only */
			return arr[median];

		if (high == low + 1)
		{  /* Two elements only */
			if (arr[low] > arr[high])
				std::swap(arr[low], arr[high]);

			return arr[median];
		}

		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])    std::swap(arr[middle], arr[high]);
		if (arr[low] > arr[high])       std::swap(arr[low], arr[high]);
		if (arr[middle] > arr[low])     std::swap(arr[middle], arr[low]);

		/* Swap low item (now in position middle) into position (low+1) */
		std::swap(arr[middle], arr[low + 1]);

		/* Nibble from each end towards middle, swapping items when stuck */
		cl = low + 1;
		hh = high;
		for (;;)
		{
			do cl++; while (arr[low] > arr[cl]);
			do hh--; while (arr[hh] > arr[low]);

			if (hh < cl)
				break;

			std::swap(arr[cl], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		std::swap(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = cl;
		if (hh >= median)
			high = hh - 1;
	}
}

void Ipp32fArray::sqrtInPlace()
{
	auto dataRowSizeInElements = getWidth() * getComponents();
	if (isContiguous())
	{
		IppThrowOnError(ippsSqrt_32f_I(data(), dataRowSizeInElements * getHeight()));
		return;
	}

	for (auto r : range(getHeight()))
	{
		auto p = getRowPointer(r);
		IppThrowOnError(ippsSqrt_32f_I(p, dataRowSizeInElements));
		auto v = *(p + 180);
	}
}

Ipp32fArray Ipp32fArray::sqrt()
{
	Ipp32fArray result(getSize());

	auto dataRowSizeInElements = getWidth() * getComponents();
	if (isContiguous())
	{
		IppThrowOnError(ippsSqrt_32f(data(), result.data(), dataRowSizeInElements * getHeight()));
		return result;
	}

	for (auto r : range(getHeight()))
	{
		auto p = getRowPointer(r);
		auto d = result.getRowPointer(r);
		IppThrowOnError(ippsSqrt_32f(p, d, dataRowSizeInElements));
	}

	return result;
}

vector<Ipp64f> Ipp32fArray::L2Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNorm_L2_C1R(data(), getRowPitchInBytes(), size, result.data()));
		break;

	case 3:
		IppThrowOnError(ippiNorm_L2_C3R(data(), getRowPitchInBytes(), size, result.data()));
		break;
	}

	return result;
}

vector<Ipp64f> Ipp32fArray::L1Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNorm_L1_32f_C1R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate));
		return result;

	case 3:
		IppThrowOnError(ippiNorm_L1_32f_C3R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

void Ipp32fArray::L1NormDiff(const IppArray<Ipp32f> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);
	auto size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNormDiff_L1_32f_C1R(data(), getRowPitchInBytes(), rhs.data(),
			rhs.getRowPitchInBytes(), size, result, ippAlgHintFast));
		break;

	case 3:
		IppThrowOnError(ippiNormDiff_L1_32f_C3R(data(), getRowPitchInBytes(), rhs.data(),
			rhs.getRowPitchInBytes(), size, result, ippAlgHintFast));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return;
}



void Ipp32fArray::L2NormDiff(const IppArray<Ipp32f> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);

	IppiSize size = getSize();
	switch (getComponents())
	{
	case 1:
		ippiNormDiff_L2_32f_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result, ippAlgHintFast);
		break;

	case 3:
		ippiNormDiff_L2_32f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result, ippAlgHintFast);
		break;
	}

	return;
}

vector<Ipp64f> Ipp32fArray::dotProduct(const IppArray<Ipp32f> &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiDotProd_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;

	case 3:
	{
		IppThrowOnError(ippiDotProd_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;
	}


	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}


vector<Ipp64f> Ipp32fArray::mean() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:

		ippiMean_32f_C1R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate);
		break;

	case 3:
		ippiMean_32f_C3R(data(), getRowPitchInBytes(), size, result.data(), ippAlgHintAccurate);
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

void Ipp32fArray::maxAndIndex(Ipp32f *maxValues, MtiPoint *indices) const
{
	MtiSize size = getSize();
	int indexX[3];
	int indexY[3];

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMaxIndx_32f_C1R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		break;

	case 3:
		IppThrowOnError(ippiMaxIndx_32f_C3R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		indices[1] = { indexX[1], indexY[1] };
		indices[2] = { indexX[2], indexY[2] };
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}
}

std::pair<std::vector<Ipp32f>, std::vector<MtiPoint>> Ipp32fArray::maxAndIndex() const
{
	const auto components = getComponents();
	std::vector<Ipp32f> maxValues(components);
	std::vector<MtiPoint> indices(components);

	maxAndIndex(maxValues.data(), indices.data());
	return { maxValues, indices };
}

// This solves for x, Y = Ax given A and Y
// uses lu decomposition
// Note: rhs is a "vector" and not a column.
Ipp32fArray Ipp32fArray::solve(const Ipp32fArray& rhs) const
{
	if ((getComponents() != 1) || (rhs.getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 channel supported")
	}

	auto m = getHeight();
	auto n = getWidth();
	if (n != m)
	{
		THROW_MTI_RUNTIME_ERROR("Only square (nxn) matrices supported");
	}

	if (n != rhs.getWidth())
	{
		THROW_MTI_RUNTIME_ERROR("Rows sizes don't match");
	}

	auto tempData = duplicate();
	MTIassert(tempData.isContiguous());

	vector<int> pivot(n);
	auto status = MtiMath::ludcmp_row_major(n, tempData.data(), n, pivot.data());
	if (status != 0)
	{
		std::stringstream os;
		os << "Error in computing L " << status;
		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	auto result = rhs.duplicate();
	status = MtiMath::lubksb_row_major(tempData.data(), n, pivot.data(), result.data());
	if (status != 0)
	{
		std::stringstream os;
		os << "Error in solving " << status;
		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	return result;
}


std::pair<std::vector<Ipp32f>, std::vector<Ipp32f>> Ipp32fArray::minMax() const
{
	const auto components = getComponents();
	std::vector<Ipp32f> minValues(components);
	std::vector<Ipp32f> maxValues(components);

	switch (components)
	{
	case 1:
		IppThrowOnError(ippiMinMax_32f_C1R(data(), getRowPitchInBytes(), getSize(), minValues.data(), maxValues.data()));
		break;

	case 3:
		IppThrowOnError(ippiMinMax_32f_C3R(data(), getRowPitchInBytes(), getSize(), minValues.data(), maxValues.data()));

		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return { minValues, maxValues };
}

Ipp32fArray Ipp32fArray::resize(IppiSize newSize) const
{
	if ((getHeight() == 0) || (getWidth() == 0))
	{
		return *this;
	}

	throwOnComponentsSupportedError();

	Ipp32fArray dst({ newSize.width, newSize.height , getComponents() });
	IppiSize srcSize = { getWidth(), getHeight() };

	IppStatus status = IppResize_C1R_or_C3R(
		data(),
		srcSize,
		getRowPitchInBytes(),
		dst.data(),
		newSize,
		dst.getRowPitchInBytes(),
		getComponents());

	IppThrowOnError(status);
	return dst;
}

Ipp32fArray Ipp32fArray::resizeAntiAliasing(IppiSize newSize) const
{
	if ((getHeight() == 0) || (getWidth() == 0))
	{
		return *this;
	}

	throwOnComponentsSupportedError();

	Ipp32fArray dst({ newSize.width, newSize.height , getComponents() });
	IppiSize srcSize = { getWidth(), getHeight() };

	IppStatus status = IppResizeAntiAliasing_C1R_or_C3R(
		data(),
		srcSize,
		getRowPitchInBytes(),
		dst.data(),
		newSize,
		dst.getRowPitchInBytes(),
		getComponents());

	IppThrowOnError(status);
	return dst;
}

void Ipp32fArray::importNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax)
{
	auto roi = getRoi();
	if ((rows != roi.height) || (cols != roi.width))
	{
		std::ostringstream os;
		os << "Invalid size in ImportNormalizedYuvFromRgb, expected (" << roi.height << "," << roi.width <<
			" got (" << rows << "," << cols << ")";

		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	auto components = getComponents();
	if ((components != 3) && (components != 1))
	{
		throwComponentError();
	}

	auto inp = p;
	auto oneOverDataMax = 1.0f / float(dataMax);
	for (auto r = 0; r < rows; r++)
	{
		auto outp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			float r = *inp++*oneOverDataMax;
			float g = *inp++*oneOverDataMax;
			float b = *inp++*oneOverDataMax;

			auto y = 0.299f * r + 0.587f * g + 0.114f * b;
			*outp++ = y;

			if (components == 3)
			{
				*outp++ = 0.492f * (b - y);
				*outp++ = 0.877f * (r - y);
			}
		}
	}
}

void Ipp32fArray::importOneNormalizedChannelFromRgb(unsigned short *p, int rows, int cols, int whichChannel, int dataMax)
{
	auto roi = getRoi();
	if ((rows != roi.height) || (cols != roi.width))
	{
		std::ostringstream os;
		os << "Invalid size in importOneNormalizedChannelFromRgb, expected (" << roi.height << "," << roi.width <<
			" got (" << rows << "," << cols << ")";

		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	auto components = getComponents();
	if (components != 1)
	{
		throwComponentError();
	}

	MTIassert(whichChannel >= 0 && whichChannel <= 2);
	if (whichChannel < 0 || whichChannel > 2)
	{
		whichChannel = 1;
	}

	auto inp = p + whichChannel;
	auto oneOverDataMax = 1.0f / float(dataMax);
	for (auto r = 0; r < rows; r++)
	{
		auto outp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			*outp++ = *inp * oneOverDataMax;
			inp += 3;
		}
	}
}

//
//
//	IppArray Ipp32fArray::ImportRgbToYuv(unsigned short *p, int rows, int cols, int dataMax)
//	{
//		IppArray result(rows, cols, 3);
//		result.ImportNormalizedYuvFromRgb(p, rows, cols, dataMax);
//
//		//    // Everything is contigious, so we just have to loop.
//		//    auto inp = p;
//		//    auto outp = result.data();
//		//    for (auto i = 0; i < rows * cols; i++)
//		//    {
//		//        float r = *(inp + 0) / float(dataMax);
//		//        float g = *(inp + 1) / float(dataMax);
//		//        float b = *(inp + 2) / float(dataMax);
//		//        inp += 3;
//		//
//		//        auto y = 0.299f * r + 0.587f * g + 0.114f * b;
//		//        *(outp + 0) = y;
//		//        *(outp + 1) = 0.492f * (b - y);
//		//        *(outp + 2) = 0.877f * (r - y);
//		//
//		//        outp += 3;
//		//    }
//		//
//		return result;
//	}
//
//	void Ipp32fArray::ImportNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax)
//	{
//		if ((rows != _roi.height) || (cols != _roi.width))
//		{
//			std::ostringstream os;
//			os << "Invalid size in ImportNormalizedYuvFromRgb, expected (" << _roi.height << "," << _roi.width <<
//				" got (" << rows << "," << cols << ")";
//
//			THROW_MTI_RUNTIME_ERROR(os.str());
//		}
//
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("ImportNormalizedYuvFromRgb needs rgb data");
//		}
//
//		auto inp = p;
//		auto oneOverDataMax = 1.0f / float(dataMax);
//		for (auto r = 0; r < rows; r++)
//		{
//			auto outp = GetPointerToElement(r);
//			for (auto c = 0; c < cols; c++)
//			{
//				float r = *inp++ * oneOverDataMax;
//				float g = *inp++ * oneOverDataMax;
//				float b = *inp++ * oneOverDataMax;
//
//				auto y = 0.299f * r + 0.587f * g + 0.114f * b;
//				*outp++ = y;
//				*outp++ = 0.492f * (b - y);
//				*outp++ = 0.877f * (r - y);
//			}
//		}
//	}
//
//	std::shared_ptr<unsigned short> Ipp32fArray::ExportYuvToRgb(int dataMax)
//	{
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("Source of ExportYuvToRgb must have channels 3!");
//		}
//
//		auto rows = _roi.height;
//		auto cols = _roi.width;
//		auto imageData = new unsigned short[cols * rows * getComponents()];
//
//		auto result = std::shared_ptr<unsigned short>(imageData);
//		ExportRgbFromNormalizedYuv(imageData, dataMax);
//
//		return result;
//	}
//
void Ipp32fArray::exportRgbFromNormalizedYuv(unsigned short *p, int dataMax)
{
	if (getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Source of ExportYuvToRgb must have channels 3!");
	}

	auto roi = getRoi();
	auto rows = roi.height;
	auto cols = roi.width;

	unsigned short *outp = p;
	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			auto y = *inp++;
			auto u = *inp++;
			auto v = *inp++;

			*outp++ = Ipp16u((y + 1.140f*v) * dataMax);
			*outp++ = Ipp16u((y - 0.394f*u - 0.581f*v) * dataMax);
			*outp++ = Ipp16u((y + 2.032f*u) * dataMax);
		}
	}
}

Ipp32fArray Ipp32fArray::crossCorrNorm(const IppArray<Ipp32f> &kernalImage, IppiROIShape roiShape) const
{
	// One channel for now
	if ((getComponents() != 1) || (kernalImage.getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Only one channel for now");
	}

	// Define the type of analysis we want
	IppEnum funCfg = (IppEnum)(ippAlgAuto | roiShape | IppiNormOp::ippiNormCoefficient);
	auto srcRoiSize = getSize();
	auto tplRoiSize = kernalImage.getSize();

	int bufSize = 0;
	Ipp8u *pBuffer = nullptr;

	IppThrowOnError(ippiCrossCorrNormGetBufferSize(srcRoiSize, tplRoiSize, funCfg, &bufSize));
	pBuffer = ippsMalloc_8u(bufSize);

	auto pSrc = data();
	auto srcStep = getRowPitchInBytes();
	auto pTpl = kernalImage.data();
	auto tplStep = kernalImage.getRowPitchInBytes();

	MtiSize dstRoiSize;

	// Find the correct size of the output
	switch (roiShape)
	{
	case ippiROIFull:
		dstRoiSize = MtiSize(srcRoiSize.width + tplRoiSize.width - 1, srcRoiSize.height + tplRoiSize.height - 1);
		break;

	case ippiROISame:
		dstRoiSize = srcRoiSize;
		break;

	case ippiROIValid:
		dstRoiSize = MtiSize(srcRoiSize.width - tplRoiSize.width + 1, srcRoiSize.height - tplRoiSize.height + 1);
		break;
	default:
		THROW_MTI_RUNTIME_ERROR("roiShape invalid");
	}

	Ipp32fArray result(dstRoiSize);
	auto pDst = result.data();
	auto dstStep = result.getRowPitchInBytes();

	auto status = ippiCrossCorrNorm_32f_C1R(pSrc, srcStep, srcRoiSize, pTpl, tplStep, tplRoiSize, pDst, dstStep, funCfg, pBuffer);
	ippsFree(pBuffer);
	IppThrowOnError(status);

	return result;
}

Ipp32fArray Ipp32fArray::sharpen() const
{
	Ipp32fArray result(getSize());
	IppiMaskSize mask = ippMskSize3x3;
	IppiBorderType borderType = IppiBorderType::ippBorderRepl;
	Ipp32f borderValue[] = { 0,0,0 };

	int bufSize = 0;
	Ipp8u *pBuffer;
	IppThrowOnError(ippiFilterSharpenBorderGetBufferSize(result.getSize(), mask, ipp32f, ipp32f, getComponents(), &bufSize));
	pBuffer = ippsMalloc_8u(bufSize);
	IppStatus status = ippStsNoErr;

	switch (getComponents())
	{
	case 1:
		status = ippiFilterSharpenBorder_32f_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), mask, borderType, borderValue[0], pBuffer);
		break;

	case 3:
		status = ippiFilterSharpenBorder_32f_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), mask, borderType, borderValue, pBuffer);
		break;

	default:
		throwComponentError();
	}

	ippsFree(pBuffer);
	IppThrowOnError(status);

	return result;
}

Ipp32fArray::operator Ipp32sArray() const
{
	Ipp32sArray result(getSize());
	for (auto r = 0; r < getHeight(); r++)
	{
		auto rp = getRowPointer(r);
		auto dp = result.getRowPointer(r);
		auto n = getWidth()*getComponents();
		IppThrowOnError(ippsConvert_32f32s_Sfs(rp, dp, getWidth()*getComponents(), IppRoundMode::ippRndFinancial, 0));
	}

	return result;
}

Ipp32fArray Ipp32fArray::logLut(int n, float maxValue, float startValue)
{
// Todo: combine this
	Ipp32fArray result(n);
	result.iota(startValue);
	for (auto &r : result)
	{
		r = std::log10(r);
	}

	auto s = result[0];
	auto e = result[n - 1];
	for (auto &r : result)
	{
		r = (r - s) / (e - s) * maxValue;
	}

	return result;
}

Ipp32fArray Ipp32fArray::linspace(float startValue, float endValue, int width)
{
	auto linSpaceV = MtiMath::linspace(startValue, endValue, width);
	return Ipp32fArray(width, linSpaceV.data(), true);
}

Ipp32fArray::operator Ipp16uArray() const
{
	Ipp16uArray result(getSize());
	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiConvert_32f16u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	case 3:
		IppThrowOnError(ippiConvert_32f16u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	case 4:
		IppThrowOnError(ippiConvert_32f16u_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	default:
		throwComponentError();
	}

	return result;
}

Ipp32fArray::operator Ipp8uArray() const
{
	Ipp8uArray result(getSize());
	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiConvert_32f8u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	case 3:
		IppThrowOnError(ippiConvert_32f8u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	case 4:
		IppThrowOnError(ippiConvert_32f8u_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear));
		break;

	default:
		throwComponentError();
	}

	return result;
}
void Ipp32fArray::lanczosResampleRoi(Ipp32fArray &target, const IppiRect &roi, float clampLow, float clampHigh, float rowOffset, float colOffset)
{
	// For now, make sure the source (this) and target are the same size
	throwOnArraySizeChannelMismatch(target);

	auto numComponents = getComponents();
	Resampler* resamplers[3];

	float fractionalRowOffset = rowOffset - int(rowOffset);

	// It is unknown why the overlapCount const is 4 but this is the minimum value
	// that makes overlap work.  I believe it is because a bug in resampler
	auto filterCount = 4;
	auto absOffset = (int)std::abs(fractionalRowOffset);
	auto overlapCount = filterCount + absOffset;

	auto width = roi.width;
	auto height = roi.height + 2 * overlapCount;
	auto leftCol = roi.x;
	auto rightCol = roi.x + roi.width;

	auto dstY = fractionalRowOffset > 0 ? 0 : -overlapCount;
	dstY = -overlapCount;
	auto topRow = roi.y + dstY + int(rowOffset);   // NOTE toprow can be negative

	const float filterScale = 1.0f;
	const char* filterName = "lanczos3";

	// Create resampler, we could make this flyweight but for now it is cheap
	resamplers[0] = new Resampler(
		width, height,
		width, height,
		Resampler::BOUNDARY_CLAMP,
		clampLow, clampHigh,
		filterName,
		nullptr, nullptr,
		filterScale, filterScale,
		colOffset, fractionalRowOffset);

	//// Make the cheap ones
	//for (int i = 1; i < numComponents; i++)
	//{
	//	resamplers[i] = new Resampler(
	//		width, height,
	//		width, height,
	//		Resampler::BOUNDARY_CLAMP,
	//		clamps[0], clamps[1],
	//		filterName,
	//		resamplers[0]->get_clist_x(), resamplers[0]->get_clist_y(),
	//		filterScale, filterScale,
	//		colOffset, rowOffset);
	//}

	Ipp32fArray targetRoi(target, roi);

	// We loop, throwing out the top and bottom
	// ASSUME numberCOmponents == 1

	// Create black line
	//Ipp32fArray blackLine(1, width);
	//blackLine.set({ 0 });

	for (int srcY = 0; srcY < height; srcY++)
	{
		auto sourceLineNumber = topRow + srcY;
		for (int comp = 0; comp < numComponents; comp++)
		{
			// Do nothing if we are off the top
			if (sourceLineNumber < 0)
			{
				if (!resamplers[comp]->put_line(getRowPointer(0)))
				{
					THROW_MTI_RUNTIME_ERROR("Out of memory!");
				}
			}
			else if (sourceLineNumber >= getHeight())
			{
				if (!resamplers[comp]->put_line(getRowPointer(getHeight() - 1)))
				{
					THROW_MTI_RUNTIME_ERROR("Out of memory!");
				}
			}
			else
			{
				if (!resamplers[comp]->put_line(getRowPointer(sourceLineNumber)))
				{
					THROW_MTI_RUNTIME_ERROR("Out of memory!");
				}
			}
		}

		for (; ;)
		{
			int comp;
			for (comp = 0; comp < numComponents; comp++)
			{
				const float* pOutput_samples = resamplers[comp]->get_line();
				if (!pOutput_samples)
				{
					break;
				}

				// Do nothing if we are off the top
				if ((dstY < 0) || (dstY >= roi.height))
				{
					dstY++;
					break;
				}

				memcpy(targetRoi.getRowPointer(dstY), pOutput_samples, width * sizeof(float));
			}

			if (comp < numComponents)
			{
				// Trampoline
				break;
			}

			dstY++;
		}
	}

	delete resamplers[0];
}

Ipp32fArray Ipp32fArray::applyBoxFilter(const IppiSize &boxSize) const
{
	IppStatus ippStatus = ippStsNoErr;
	Ipp32fArray outputArray(getSize());
	IppiSize roiSize = getSize();

	int iTmpBufSize = 0;
	int numChannels = 1;
	ippStatus = ippiFilterBoxBorderGetBufferSize(roiSize, boxSize, ipp32f, numChannels, &iTmpBufSize);
	Ipp8u *pBuffer = ippsMalloc_8u(iTmpBufSize);

	Ipp32f *pSrc = data();
	Ipp32f *pDst = outputArray.data();
	int srcStep = getRowPitchInBytes();
	int dstStep = outputArray.getRowPitchInBytes();
	IppiBorderType borderType = ippBorderRepl;
	Ipp32f borderValue = 0.F;  // ignored for ippBorderRepl
	ippStatus = ippiFilterBoxBorder_32f_C1R(pSrc, srcStep, pDst, dstStep, roiSize, boxSize, borderType, &borderValue, pBuffer);

	ippsFree(pBuffer);

	return outputArray;
}

Ipp32fArray Ipp32fArray::transpose() const
{
	const auto roi = getRoi();

	// Note: transpose swaps the size!  Do not use getSize here
	Ipp32fArray result({ roi.height, roi.width, getComponents() });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiTranspose_32f_C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 3:
		IppThrowOnError(ippiTranspose_32f_C3R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiTranspose_32f_C4R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp32fArray Ipp32fArray::toGray() const
{
	// Result is one component
	Ipp32fArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 3:
		IppThrowOnError(ippiRGBToGray_32f_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiRGBToGray_32f_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp32fArray Ipp32fArray::toGray(const float weights[]) const
{
	Ipp32fArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 3:
		IppThrowOnError(ippiColorToGray_32f_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	case 4:
		IppThrowOnError(ippiColorToGray_32f_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

// TO DO, move to a better place
template<typename T>
void boxConv(T *in, int inPitchBytes, int rows, int cols, T *out, int outPitchBytes, const IppiSize &kernelSize, T *tmp)
{
	auto convHeight = rows - kernelSize.height + 1;
	auto convWidth = cols - kernelSize.width + 1;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memcpy(tmp, in, cols * sizeof(T));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto tmpr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(T);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr++ += *inr++;
		}
	}

	// Do each additional row
	for (auto r = 1; r < convHeight; r++)
	{
		auto tmpr = tmp + r * cols;
		auto sumr = tmp + (r - 1) * cols;
		auto inr = in + (r - 1) * inPitchBytes / sizeof(T);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(T);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr = *sumr++;            // get summed row
			*tmpr -= *inr++;		    // subtract top row
			*tmpr++ += *inrl++;         // add bottom row
		}
	}

	auto scaleFactor = 1.0f / (kernelSize.width * kernelSize.height);

	// Now partial sums of cols
	for (auto r = 0; r < convHeight; r++)
	{
		auto outr = out + r * outPitchBytes / sizeof(T);
		auto tmpr = tmp + r * cols;

		auto s = tmpr[0];
		for (auto c = 1; c < kernelSize.width; c++)
		{
			s += tmpr[c];
		}

		*outr++ = scaleFactor * s;

		for (auto c = 1; c < convWidth; c++)
		{
			s -= *tmpr;
			s += *(tmpr + kernelSize.width);
			tmpr++;
			*outr++ = scaleFactor * s;
		}
	}
}

//MtiRect Ipp32fArray::applyBoxFilterValid(const IppiSize &boxSize, Ipp32fArray &normArray, Ipp32fArray &scratchArray) const
//{
//	MtiRect validRoi =
//	{
//	   boxSize.width / 2,
//	   boxSize.height / 2,
//	   getWidth() - boxSize.width + 1,
//	   getHeight() - boxSize.height + 1
//	};
//	normArray.createStorageIfEmpty(validRoi.height, validRoi.width, 1);
//
//	throwOnArraySizeChannelMismatch({ validRoi.width , validRoi.height, 1 }, normArray.getSize());
//
//	scratchArray.createStorageIfEmpty(getHeight(), getWidth(), 1);
//	throwOnArraySizeChannelMismatch(scratchArray);
//
//	boxConv
//	(
//		data(),
//		getRowPitchInBytes(),
//		getHeight(),
//		getWidth(),
//		normArray.data(),
//		normArray.getRowPitchInBytes(),
//		boxSize,
//		scratchArray.data()
//	);
//
//	return validRoi;
//}

MtiRect Ipp32fArray::applyBoxFilterValid(const IppiSize &boxSize, Ipp32fArray &outputArray, Ipp8uArray &scratchArray) const
{
	MtiRect validRoi =
	{
	   boxSize.width / 2,
	   boxSize.height / 2,
	   getWidth() - boxSize.width + 1,
	   getHeight() - boxSize.height + 1
	};

	outputArray.createStorageIfEmpty(validRoi.height, validRoi.width, getComponents());
	throwOnArraySizeChannelMismatch({ validRoi.width , validRoi.height, getComponents() }, outputArray.getSize());

	auto inputArray = (*this)(validRoi);
	// create scratch buffer
	auto iTmpBufSize = 0;

	IppThrowOnError(ippiFilterBoxBorderGetBufferSize(outputArray.getSize(), boxSize, ipp32f, getComponents(), &iTmpBufSize));
	if (scratchArray.getWidth() != iTmpBufSize)
	{
		Ipp8uArray newScratch({ iTmpBufSize, 1 });
		scratchArray = newScratch;
	}

	IppiBorderType borderType = ippBorderInMem;
	Ipp32f borderValue = 0.F;  // ignored for ippBorderInMem

	switch (getComponents())
	{
	case 1:
		IppThrowOnError
		(
			ippiFilterBoxBorder_32f_C1R(
				inputArray.data(),
				inputArray.getRowPitchInBytes(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				outputArray.getSize(),
				boxSize,
				borderType,
				&borderValue,
				scratchArray.data()
			)
		);
		break;

	case 3:
		IppThrowOnError
		(
			ippiFilterBoxBorder_32f_C3R
			(
				inputArray.data(),
				inputArray.getRowPitchInBytes(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				outputArray.getSize(),
				boxSize,
				borderType,
				&borderValue,
				scratchArray.data()
			)
		);
		break;

	default:
		throwOnComponentsSupportedError();
	}

	return validRoi;
}

MtiRect Ipp32fArray::applyBoxFilter(const IppiSize &boxSize, Ipp32fArray &outputArray, Ipp8uArray &scratchArray) const
{
	outputArray.createStorageIfEmpty(getSize());
	throwOnArraySizeChannelMismatch(outputArray);

	// create scratch buffer
	int iTmpBufSize = 0;
	IppThrowOnError(ippiFilterBoxBorderGetBufferSize(getSize(), boxSize, ipp32f, getComponents(), &iTmpBufSize));
	if (scratchArray.getWidth() != iTmpBufSize)
	{
		Ipp8uArray newScratch({ iTmpBufSize, 1 });
		scratchArray = newScratch;
	}

	IppiBorderType borderType = ippBorderRepl;
	Ipp32f borderValue = 0.F;  // ignored for ippBorderRepl

	switch (getComponents())
	{
	case 1:
		IppThrowOnError
		(
			ippiFilterBoxBorder_32f_C1R(
				data(),
				getRowPitchInBytes(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				getSize(),
				boxSize,
				borderType,
				&borderValue,
				scratchArray.data()
			)
		);
		break;

	case 3:
		IppThrowOnError
		(
			ippiFilterBoxBorder_32f_C3R
			(
				data(),
				getRowPitchInBytes(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				getSize(),
				boxSize,
				borderType,
				&borderValue,
				scratchArray.data()
			)
		);
		break;

	default:
		throwOnComponentsSupportedError();
	}

	return { boxSize.width / 2, boxSize.height / 2, getWidth() - boxSize.width + 1, getHeight() - boxSize.height + 1 };
}

static Ipp32f valsigma = 0.1f;
static Ipp32f possigma = 3.0f;

// Bug: a throw does not clean up array, rewrite using IppArray type for scratch buffer
Ipp32fArray Ipp32fArray::applyBilateralFilter(int radius) const
{
	IppStatus ippStatus = ippStsNoErr;
	Ipp32fArray outputArray(getSize());
	IppiSize roiSize = getSize();

	int numChannels = 1;
	int iSpecSize = 0;
	int iTmpBufSize = 0;
	ippStatus = ippiFilterBilateralBorderGetBufferSize(
		ippiFilterBilateralGauss,
		roiSize,
		radius,
		ipp32f,
		numChannels,
		ippDistNormL1,
		&iSpecSize,
		&iTmpBufSize);

	IppThrowOnError(ippStatus);

	auto *pSpec = (IppiFilterBilateralSpec *)ippsMalloc_8u(iSpecSize);
	Ipp8u *pBuffer = ippsMalloc_8u(iTmpBufSize);

	ippStatus = ippiFilterBilateralBorderInit(
		ippiFilterBilateralGauss,
		roiSize,
		radius,
		ipp32f,
		numChannels,
		ippDistNormL1,
		valsigma,
		possigma,
		pSpec);

	IppThrowOnError(ippStatus);

	Ipp32f *pSrc = data();
	Ipp32f *pDst = outputArray.data();
	int srcStep = getRowPitchInBytes();
	int dstStep = outputArray.getRowPitchInBytes();
	IppiBorderType borderType = ippBorderRepl;
	ippStatus = ippiFilterBilateralBorder_32f_C1R(
		pSrc, srcStep,
		pDst, dstStep,
		roiSize,
		borderType, nullptr,
		pSpec, pBuffer);

	ippsFree(pSpec);
	ippsFree(pBuffer);

	IppThrowOnError(ippStatus);

	return outputArray;
}

#define NUMBER_OF_CONTRAST_BINS 1024

Ipp32fArray Ipp32fArray::applyAutoContrastFilter() const
{
	MTIassert(getComponents() == 1);
	Ipp32fArray *nonConstThis = const_cast<Ipp32fArray *>(this);
	HistogramIpp histogramGenerator(*nonConstThis, NUMBER_OF_CONTRAST_BINS, 0.0f, 1.0f);
	auto histogramAllChans = histogramGenerator.compute(*nonConstThis);
	auto histogram = histogramAllChans[0];

	float min = -1;
	float median = -1;
	float max = -1;
	int sum = 0;
	const int numberOfPixelsDividedBy2 = getHeight() * getWidth() / 2;

	for (int i = 0; i < NUMBER_OF_CONTRAST_BINS; ++i)
	{
		if (histogram[i] && min == -1)
		{
			min = i / float(NUMBER_OF_CONTRAST_BINS);
		}

		if (histogram[i])
		{
			max = i / float(NUMBER_OF_CONTRAST_BINS);
		}

		sum += histogram[i];
		if (sum >= numberOfPixelsDividedBy2 && median == -1)
		{
			median = i / float(NUMBER_OF_CONTRAST_BINS);
		}
	}

	Ipp32fArray out(getSize());
	const int middleBin = NUMBER_OF_CONTRAST_BINS / 2;
	float stretchLow = (median == min) ? 1.0f : (float(middleBin) / (median - min));
	float stretchHigh = (median == min) ? 1.0f : (float(middleBin) / (max - median));
	auto inIter = nonConstThis->begin();
	auto outIter = out.begin();
	while (inIter != nonConstThis->end())
	{
		float inVal = *inIter++;
		float outVal;
		outVal = (inVal <= median)
			? ((inVal - min) * stretchLow)
			: (middleBin + (inVal - median) * stretchHigh);
		outVal = std::min<float>(std::max<float>(outVal, 0), 1.0);
		*outIter++ = outVal;
	}

	return out;
}

Ipp32fArray Ipp32fArray::applyLut(const MtiPlanar<Ipp32fArray> &lut, Ipp8uArray &scratch, float maxLevel) const
{
	auto width = lut.getPlaneSize().width;
	auto levelsVector = MtiMath::linspace(0.0f, maxLevel, width);
	return applyLut(lut, Ipp32fArray(width, levelsVector.data()), scratch);
}

void Ipp32fArray::applyLutInPlace(const MtiPlanar<Ipp32fArray>& lut, Ipp8uArray& scratch, float maxLevel) const
{
	auto width = lut.getPlaneSize().width;
	auto levelsVector = MtiMath::linspace(0.0f, maxLevel, width);
	applyLutInPlace(lut, Ipp32fArray(width, levelsVector.data()), scratch);
}

Ipp32fArray Ipp32fArray::applyLut(const MtiPlanar<Ipp32fArray>& lut, const Ipp32fArray& levels, Ipp8uArray& scratch) const
{
	// Change this when we support alpha
	if ((getComponents() != 3) && (getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Lut channels must match")
	}

	auto numOfChannelsToChange = std::min<int>(3, getComponents());
	if ((numOfChannelsToChange != 3) && (numOfChannelsToChange != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Levels sizes must match")
	}

	// Allocate upt to 3 channels
	int pSpecSize = 0;
	const Ipp32f *pValues[3];
	const Ipp32f *pLevels[3];
	int nLevels[3];

	// Uniform number of levels
	// In case we every support alpha
	for (auto i : range(numOfChannelsToChange))
	{
		nLevels[i] = levels.getWidth();
		pLevels[i] = levels.getRowPointer(std::min<int>(i, levels.getHeight() - 1));
		pValues[i] = lut[std::min<int>(i, lut.getPlanarSize().depth - 1)].getRowPointer(0);
	}

	Ipp32fArray result(getSize());

	// We only support two cases, mono or rgb
	if (getComponents() == 1)
	{
		IppThrowOnError(ippiLUT_GetSize(ippLinear, ipp32f, ippC1, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_32f(ippLinear, ippC1, getSize(), pValues, pLevels, nLevels, pSpec));

		IppThrowOnError(ippiLUT_32f_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), pSpec));

		return result;
	}

	if (getComponents() == 3)
	{
		IppThrowOnError(ippiLUT_GetSize(ippLinear, ipp32f, ippC3, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_32f(ippLinear, ippC3, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_32f_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), pSpec));

		return result;
	}

	return result;
}

void Ipp32fArray::applyLutInPlace(const MtiPlanar<Ipp32fArray>& lut, const Ipp32fArray& levels,
	Ipp8uArray& scratch) const
{
	// Change this when we support alpha
	if ((getComponents() != 3) && (getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Lut channels must match")
	}

	auto numOfChannelsToChange = std::min<int>(3, getComponents());
	if ((numOfChannelsToChange != 3) && (numOfChannelsToChange != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Levels sizes must match")
	}

	// Allocate upt to 3 channels
	int pSpecSize = 0;
	const Ipp32f *pValues[3];
	const Ipp32f *pLevels[3];
	int nLevels[3];
	const IppiInterpolationType interpolation = ippLinear;

	// Uniform number of levels
	// In case we every support alpha
	for (auto i : range(numOfChannelsToChange))
	{
		nLevels[i] = levels.getWidth();
		pLevels[i] = levels.getRowPointer(std::min<int>(i, levels.getHeight() - 1));
		pValues[i] = lut[std::min<int>(i, lut.getPlanarSize().depth - 1)].getRowPointer(0);
	}

	// We only support two cases, mono or rgb
	if (getComponents() == 1)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp32f, ippC1, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_32f(interpolation, ippC1, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_32f_C1IR(data(), getRowPitchInBytes(), getSize(), pSpec));
	}

	if (getComponents() == 3)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp32f, ippC3, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_32f(interpolation, ippC3, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_32f_C3IR(data(), getRowPitchInBytes(), getSize(), pSpec));
	}
}

// Only supported in 2019
//Ipp32fArray Ipp32fArray::gamma709(float minValue, float maxValue) const
//{
//	Ipp32fArray result(getSize());
//
//	// We only support two cases, mono or rgb
//	if (getComponents() == 1)
//	{
//		IppThrowOnError(ippiGammaFwd_32f_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), minValue, maxValue));
//		return result;
//	}
//
//	if (getComponents() == 3)
//	{
//		IppThrowOnError(ippiGammaFwd_32f_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), minValue, maxValue));
//
//		return result;
//	}
//
//	THROW_MTI_RUNTIME_ERROR("Only one or three channels supported")
//}
//
//void Ipp32fArray::gamma709InPlace(float minValue, float maxValue) const
//{
//	// We only support two cases, mono or rgb
//	if (getComponents() == 1)
//	{
//		IppThrowOnError(ippiGammaFwd_32f_C1IR(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), minValue, maxValue));
//		return;
//	}
//
//	if (getComponents() == 3)
//	{
//		IppThrowOnError(ippiGammaFwd_32f_C3IR(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), minValue, maxValue));
//
//		return;
//	}
//
//	THROW_MTI_RUNTIME_ERROR("Only one or three channels supported")
//}

Ipp32fArray Ipp32fArray::equalizeHistogram() const
{
	const int GrayLevels = 1024;
	Ipp32fArray *nonConstThis = const_cast<Ipp32fArray*>(this);
	Ipp32f s[GrayLevels];
	Ipp32f levels[GrayLevels + 1];
	Ipp32f values[GrayLevels + 1];
	auto imgSize = getSize();

	//   Ipp32fArray tempArray = *this * float(GrayLevels);

	   // calculate histogram
	   //ippiHistogramEven_8u_C1R(pImage, imgStep, imgSize, histo, levels, GrayLevels + 1, 0, GrayLevels);
	//	  HistogramIpp histogramGenerator(*nonConstThis, GrayLevels, 0.0F, float(GrayLevels));
	//   auto histogramAllChans = histogramGenerator.compute(tempArray);
	HistogramIpp histogramGenerator(*nonConstThis, GrayLevels, 0.0F, 1.0f);
	auto histogramAllChans = histogramGenerator.compute(*nonConstThis);
	auto histogram = histogramAllChans[0];

	values[0] = 0;
	levels[0] = 0;
	s[0] = float(histogram[0]) / imgSize.width / imgSize.height;
	for (int i = 1; i < GrayLevels; ++i)
	{
		auto histNorm = float(histogram[i]) / imgSize.width / imgSize.height;
		s[i] = histNorm + s[i - 1];
		//      values[i] = Ipp32s(s[i]) * (GrayLevels - 1));
		values[i] = s[i];
		levels[i] = i / float(GrayLevels);
	}

	values[GrayLevels] = GrayLevels - 1;
	levels[GrayLevels] = 1.0;

	Ipp32fArray out(imgSize);

	// LUT
	//ippiLUT_8u_C1IR(pImage, imgStep, imgSize, values, levels, GrayLevels + 1);
	int pSpecSize = 0;
	const Ipp32f *pValues[1];
	pValues[0] = values;
	const Ipp32f *pLevels[1];
	pLevels[0] = levels;
	int pNLevels[1];
	pNLevels[0] = GrayLevels + 1;

	IppThrowOnError(ippiLUT_GetSize(ippLinear, ipp32f, ippC1, imgSize, pNLevels, &pSpecSize));
	IppiLUT_Spec* pSpec = (IppiLUT_Spec*)ippsMalloc_8u(pSpecSize);
	IppThrowOnError(ippiLUT_Init_32f(ippLinear, ippC1, imgSize, pValues, pLevels, pNLevels, pSpec));
	IppThrowOnError(ippiLUT_32f_C1R(data(), getRowPitchInBytes(), out.data(), out.getRowPitchInBytes(), imgSize, pSpec));
	ippsFree(pSpec);

	return out;
}

Ipp32fArray Ipp32fArray::dilate(const Ipp8uArray& mask, Ipp8uArray& scratchArray) const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 channel arrays can be dilated")
	}

	Ipp32fArray outputArray(getSize());

	// create scratch buffer
	int tempBufferSize = 0;
	int specSize = 0;
	IppThrowOnError(ippiMorphologyBorderGetSize_32f_C1R(getSize(), mask.getSize(), &specSize, &tempBufferSize));

	auto tempSize = std::max<int>(tempBufferSize, specSize);

	// This allocates the scratch buffer, we just need more than min
	if ((scratchArray.getWidth() != tempSize) || (scratchArray.getHeight() != 2))
	{
		Ipp8uArray newScratch({ tempSize, 2 });
		scratchArray = newScratch;
	}
	IppThrowOnError
	(
		ippiMorphologyBorderInit_32f_C1R
		(
			getSize(),
			mask.data(),
			mask.getSize(),
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		)
	);

	const IppiBorderType borderType = ippBorderRepl;
	IppThrowOnError
	(
		ippiDilateBorder_32f_C1R
		(
			data(),
			getRowPitchInBytes(),
			outputArray.data(),
			outputArray.getRowPitchInBytes(),
			getSize(),
			borderType,
			0,
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		);
	);

	return outputArray;
}

Ipp32fArray Ipp32fArray::erode(const Ipp8uArray& mask, Ipp8uArray& scratchArray) const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 channel arrays can be dilated")
	}

	Ipp32fArray outputArray(getSize());

	// create scratch buffer
	int tempBufferSize = 0;
	int specSize = 0;
	IppThrowOnError(ippiMorphologyBorderGetSize_32f_C1R(getSize(), mask.getSize(), &specSize, &tempBufferSize));

	auto tempSize = std::max<int>(tempBufferSize, specSize);

	// This allocates the scratch buffer, we just need more than min
	if ((scratchArray.getWidth() != tempSize) || (scratchArray.getHeight() != 2))
	{
		Ipp8uArray newScratch({ tempSize, 2 });
		scratchArray = newScratch;
	}

	IppThrowOnError
	(
		ippiMorphologyBorderInit_32f_C1R
		(
			getSize(),
			mask.data(),
			mask.getSize(),
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		)
	);


	const IppiBorderType borderType = ippBorderRepl;
	IppThrowOnError
	(
		ippiErodeBorder_32f_C1R
		(
			data(),
			getRowPitchInBytes(),
			outputArray.data(),
			outputArray.getRowPitchInBytes(),
			getSize(),
			borderType,
			0,
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		);
	);

	return outputArray;
}

Ipp32fArray Ipp32fArray::convSame(const IppArray & kernel, Ipp8uArray &scratchArray)
{
	Ipp32fArray outputArray;
	outputArray.createStorageIfEmpty(getSize());
	//throwOnArraySizeChannelMismatch(outputArray);

	// create scratch buffer
	int pBufferSize = 0;
	IppEnum funCfgFull = (IppEnum)(ippAlgAuto | ippiROIValid | ippiNormNone);

	IppThrowOnError(ippiConvGetBufferSize(getSize(), kernel.getSize(), ipp32f, getComponents(), funCfgFull, &pBufferSize));

	// This allocs the scratch buffer, we just need more than min
	if (scratchArray.getWidth() != pBufferSize)
	{
		Ipp8uArray newScratch({ pBufferSize, 1 });
		scratchArray = newScratch;
	}

	Ipp8u *pBuffer = scratchArray.getRowPointer(0);

	switch (getComponents())
	{
	case 1:
		IppThrowOnError
		(
			ippiConv_32f_C1R
			(
				data(),
				getRowPitchInBytes(),
				getSize(),
				kernel.data(),
				kernel.getRowPitchInBytes(),
				kernel.getSize(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				funCfgFull,
				pBuffer
			)
		);

		break;

	case 3:
		IppThrowOnError
		(
			ippiConv_32f_C3R
			(
				data(),
				getRowPitchInBytes(),
				getSize(),
				kernel.data(),
				kernel.getRowPitchInBytes(),
				kernel.getSize(),
				outputArray.data(),
				outputArray.getRowPitchInBytes(),
				funCfgFull,
				pBuffer
			)
		);
		break;

	default:
		throwOnComponentsSupportedError();
	}

	return outputArray;
}

Ipp32fArray Ipp32fArray::convFullReturnCenter(const IppArray & kernel, Ipp32fArray & outArray, Ipp8uArray & scratchArray)
{
	auto rowsOut = getHeight() + kernel.getHeight() - 1;
	auto colsOut = getWidth() + kernel.getWidth() - 1;

	MtiSize outShape = { colsOut, rowsOut,  getComponents() };
	outArray.createStorageIfEmpty(outShape);
	throwOnArraySizeChannelMismatch(outArray.getSize(), outShape);

	// create scratch buffer
	int pBufferSize = 0;
	IppEnum funCfgFull = (IppEnum)(ippAlgAuto | ippiROIFull | ippiNormNone);

	IppThrowOnError(ippiConvGetBufferSize(getSize(), kernel.getSize(), ipp32f, getComponents(), funCfgFull, &pBufferSize));

	// This allocs the scratch buffer, we just need more than min
	if (scratchArray.getWidth() < pBufferSize)
	{
		Ipp8uArray newScratch({ pBufferSize, 1,  1 });
		scratchArray = newScratch;
	}

	Ipp8u *pBuffer = scratchArray.data();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError
		(
			ippiConv_32f_C1R
			(
				data(),
				getRowPitchInBytes(),
				getSize(),
				kernel.data(),
				kernel.getRowPitchInBytes(),
				kernel.getSize(),
				outArray.data(),
				outArray.getRowPitchInBytes(),
				funCfgFull,
				pBuffer
			)
		);

		break;

	case 3:
		IppThrowOnError
		(
			ippiConv_32f_C3R
			(
				data(),
				getRowPitchInBytes(),
				getSize(),
				kernel.data(),
				kernel.getRowPitchInBytes(),
				kernel.getSize(),
				outArray.data(),
				outArray.getRowPitchInBytes(),
				funCfgFull,
				pBuffer
			)
		);
		break;

	default:
		throwOnComponentsSupportedError();
	}

	auto kr = (kernel.getHeight() + 1) / 2 - 1;
	auto kc = (kernel.getWidth() + 1) / 2 - 1;
	return outArray({ kc, kr, getWidth(), getHeight() });
}

// This needs to be sped up
vector < std::pair<Ipp32f, Ipp32s>> Ipp32fArray::sortWithIndex() const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1D can be sorted");
	}

	vector<std::pair<Ipp32f, Ipp32s>> valueIndexList(area());
	auto index = 0;
	for (auto r = 0; r < getHeight(); r++)
	{
		auto rp = getRowPointer(r);
		for (auto c = 0; c < getWidth(); c++)
		{
			valueIndexList.emplace_back(*rp++, index++);
		}
	}

	sort(valueIndexList.begin(), valueIndexList.end(), [](std::pair<Ipp32f, Ipp32s> &s0, std::pair<Ipp32f, Ipp32s> &s1)
	{
		return s0.first > s1.first;
	}
	);

	return valueIndexList;
}


//Ipp32fArray Ipp32fArray::convSame(const IppArray & kernel, Ipp8uArray &scratchArray)
	//{
	//	Ipp32fArray outputArray;
	//	outputArray.createStorageIfEmpty(getSize());
	//	throwOnArraySizeChannelMismatch(outputArray);
	//
	//	// create scratch buffer
	//	int iTmpBufSize = 0;
	//	int specSize = 0;
	//
	//	IppThrowOnError(ippiFilterBorderGetSize(kernel.getSize(), outputArray.getSize(), ipp32f, ipp32f, 1, &specSize, &iTmpBufSize));
	//	auto tempSize = std::max<int>(iTmpBufSize, specSize);
	//
	//	// This allocs the scratch buffer, we just need more than min
	//	if ((scratchArray.getWidth() != tempSize) && (scratchArray.getHeight() != 2))
	//	{
	//		Ipp8uArray newScratch(2, tempSize);
	//		scratchArray = newScratch;
	//		IppThrowOnError
	//		(
	//			ippiFilterBorderInit_32f
	//			(
	//				kernel.data(),
	//				kernel.getSize(),
	//				ipp32f,
	//				getComponents(),
	//				ippRndNear,
	//				(IppiFilterBorderSpec*)scratchArray.getRowPointer(1)
	//			)
	//		);
	//	}
	//
	//	Ipp8u *pBuffer = scratchArray.getRowPointer(0);
	//	IppiFilterBorderSpec *pSpec = (IppiFilterBorderSpec*)scratchArray.getRowPointer(1);
	//	IppiBorderType borderType = ippBorderConst;
	//	Ipp32f borderValue[] = { 0.0f, 0.0f, 0.0f };
	//
	//	switch (getComponents())
	//	{
	//	case 1:
	//		IppThrowOnError
	//		(
	//			ippiFilterBorder_32f_C1R
	//			(
	//				data(),
	//				getRowPitchInBytes(),
	//				outputArray.data(),
	//				outputArray.getRowPitchInBytes(),
	//				getSize(),
	//				borderType,
	//				borderValue,
	//				pSpec,
	//				pBuffer
	//			)
	//		);
	//
	//		break;
	//
	//	case 3:
	//		IppThrowOnError
	//		(
	//			ippiFilterBorder_32f_C3R
	//			(
	//				data(),
	//				getRowPitchInBytes(),
	//				outputArray.data(),
	//				outputArray.getRowPitchInBytes(),
	//				getSize(),
	//				borderType,
	//				borderValue,
	//				pSpec,
	//				pBuffer
	//			)
	//		);
	//		break;
	//
	//	default:
	//		throwOnComponentsSupportedError();
	//	}
	//
	//	return outputArray;
	//}

namespace
{
	IppStatus IppResize_C1R_or_C3R(Ipp32f* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32f* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
	{
		if (numChannels != 1 && numChannels != 3)
		{
			THROW_MTI_RUNTIME_ERROR("Bad number of channels in input to Resize");
		}

		IppiResizeSpec_32f* pSpec = 0;
		int specSize = 0, initSize = 0, bufSize = 0;
		Ipp8u* pBuffer = nullptr;
		Ipp8u* pInitBuf = nullptr;
		IppiPoint dstOffset = { 0, 0 };
		IppStatus status = ippStsNoErr;
		IppiBorderType border = ippBorderRepl;

		// ippiResize<Interpolation>Init functions has the following constraints
		// for the minimal size of an input image depending on the chosen
		// interpolation method:
		//    Nearest Neighbor - 1x1
		//    Linear           - 2x2
		//    2-lobed Lanczos  - 4x4
		//    3-lobed Lanczos  - 6x6

		IppiInterpolationType interpolation = ippLanczos;
		Ipp32u numLanczosLobes = 3;
		const int minLinearDim = 2;
		const int minLanczos2Dim = 4;
		const int minLanczos3Dim = 6;
		if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
			|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
		{
			interpolation = ippNearest;
		}
		else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
			|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
		{
			interpolation = ippLinear;
		}
		else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
			|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
		{
			numLanczosLobes = 2;
		}

		// Allocate spec and init buffers.
		status = ippiResizeGetSize_32f(srcSize, dstSize, interpolation, 0, &specSize, &initSize);
		if (status != ippStsNoErr)
		{
			return status;
		}

		pSpec = (IppiResizeSpec_32f*)ippsMalloc_8u(specSize);
		if (pSpec == nullptr)
		{
			return ippStsNoMemErr;
		}

		// Initialize the filter.
		switch (interpolation)
		{
		case ippNearest:
			status = ippiResizeNearestInit_32f(srcSize, dstSize, pSpec);
			break;

		case ippLinear:
			status = ippiResizeLinearInit_32f(srcSize, dstSize, pSpec);
			break;

		case ippCubic:
			pInitBuf = ippsMalloc_8u(initSize);
			if (pInitBuf == nullptr)
			{
				ippsFree(pSpec);
				return ippStsNoMemErr;
			}

			// Catmull - Rom(B = 0, C = 0.5), B - Spline(B = 1, C = 0)
			status = ippiResizeCubicInit_32f(srcSize, dstSize, 0.0f, 0.5f, pSpec, pInitBuf);
			break;

		case ippLanczos:
			pInitBuf = ippsMalloc_8u(initSize);
			if (pInitBuf == nullptr)
			{
				ippsFree(pSpec);
				return ippStsNoMemErr;
			}

			status = ippiResizeLanczosInit_32f(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
			break;

		default:
			break;
		}

		ippsFree(pInitBuf);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			return status;
		}

		// Allocate work buffer.
		status = ippiResizeGetBufferSize_32f(pSpec, dstSize, numChannels, &bufSize);

		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return status;
		}

		pBuffer = ippsMalloc_8u(bufSize);
		if (pBuffer == nullptr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return ippStsNoMemErr;
		}

		/* Resize processing */
		if (numChannels == 1)
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_32f_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_32f_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippCubic:
				status = ippiResizeCubic_32f_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_32f_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			default:
				break;
			}
		}
		else
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_32f_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_32f_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippCubic:
				status = ippiResizeCubic_32f_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_32f_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			default:
				break;
			}
		}

		ippsFree(pSpec);
		ippsFree(pBuffer);

		return status;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// This is anti aliasing resize code, Ugly as sin 
	IppStatus IppResizeAntiAliasing_C1R_or_C3R(Ipp32f* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32f* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
	{
		if (numChannels != 1 && numChannels != 3)
		{
			THROW_MTI_RUNTIME_ERROR("Bad number of channels in input to Resize");
		}

		IppiResizeSpec_32f* pSpec = 0;
		int specSize = 0, initSize = 0, bufSize = 0;
		Ipp8u* pBuffer = nullptr;
		Ipp8u* pInitBuf = nullptr;
		IppiPoint dstOffset = { 0, 0 };
		IppStatus status = ippStsNoErr;
		IppiBorderType border = ippBorderRepl;

		// ippiResize<Interpolation>Init functions has the following constraints
		// for the minimal size of an input image depending on the chosen
		// interpolation method:
		//    Nearest Neighbor - 1x1
		//    Linear           - 2x2
		//    2-lobed Lanczos  - 4x4
		//    3-lobed Lanczos  - 6x6

		IppiInterpolationType interpolation = ippLanczos;
		//	IppiInterpolationType interpolation = ippCubic;
		Ipp32u numLanczosLobes = 3;
		const int minLinearDim = 2;
		const int minLanczos2Dim = 4;
		const int minLanczos3Dim = 6;
		if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
			|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
		{
			interpolation = ippNearest;
		}
		else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
			|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
		{
			interpolation = ippLinear;
		}
		else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
			|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
		{
			numLanczosLobes = 2;
		}

		// Allocate spec and init buffers.
		status = ippiResizeGetSize_32f(srcSize, dstSize, interpolation, 1, &specSize, &initSize);
		if (status != ippStsNoErr)
		{
			return status;
		}

		pSpec = reinterpret_cast<IppiResizeSpec_32f*>(ippsMalloc_8u(specSize));
		if (pSpec == nullptr)
		{
			return ippStsNoMemErr;
		}

		pInitBuf = ippsMalloc_8u(initSize);
		if (pInitBuf == nullptr)
		{
			ippsFree(pSpec);
			return ippStsNoMemErr;
		}

		// Initialize the filter.
		switch (interpolation)
		{
		case ippLinear:
			status = ippiResizeAntialiasingLinearInit(srcSize, dstSize, pSpec, pInitBuf);
			break;

		case ippCubic:
			// Catmull - Rom(B = 0, C = 0.5), B - Spline(B = 1, C = 0)
			status = ippiResizeAntialiasingCubicInit(srcSize, dstSize, 0.0f, 0.5f, pSpec, pInitBuf);
			break;

		case ippLanczos:

			status = ippiResizeAntialiasingLanczosInit(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
			break;

		default:
			status = ippStsNotSupportedModeErr;
			break;
		}

		ippsFree(pInitBuf);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			return status;
		}

		// Allocate work buffer.
		status = ippiResizeGetBufferSize_32f(pSpec, dstSize, numChannels, &bufSize);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return status;
		}

		pBuffer = ippsMalloc_8u(bufSize);
		if (pBuffer == nullptr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return ippStsNoMemErr;
		}

		/* Resize processing */
		if (numChannels == 1)
		{
			status = ippiResizeAntialiasing_32f_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
		}
		else
		{
			status = ippiResizeAntialiasing_32f_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
		}

		ippsFree(pSpec);
		ippsFree(pBuffer);

		return status;
	}
}; // end anonymous namespace


