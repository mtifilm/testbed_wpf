#include "MtiMath.h"
#include "mkl.h"
#include <vector>
#include <algorithm>
#include <sstream>

const char noTranspose = 'N';


namespace MtiMath
{
	std::string getDfError(int errNum);

#define THROW_ON_DF_ERROR(errNum) \
if ((errNum) < 0)\
{\
	std::ostringstream p_os;\
	p_os << __func__ << " (" << __LINE__ << "): " << getDfError((errNum)); \
	throw MtiRuntimeError(p_os.str()); \
}

	// Single precision
	int ludcmp_row_major(int n, float* a, int lda, int *ipiv)
	{
		// Create the ILP64 interface
		lapack_int n_mkl = n;
		vector<lapack_int> ipiv_mkl(n);
		lapack_int lda_mkl = lda;

		auto info_mlk = LAPACKE_sgetrf(LAPACK_ROW_MAJOR, n_mkl, n_mkl, a, lda_mkl, ipiv_mkl.data());

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv[i] = ipiv_mkl[i];
		}

		return info_mlk;
	}

	int lubksb_row_major(float *a, int n, int *ipiv, float *b)
	{
		lapack_int n_mkl = n;
		lapack_int nrhs_mkl = 1;
		lapack_int lda_mkl = n;
		lapack_int ldb_mkl = 1; // is n for column major
		vector<lapack_int> ipiv_mkl(n);

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv_mkl[i] = ipiv[i];
		}

		// Solve for B, replacing B. One set of solutions for each column of B.
		return LAPACKE_sgetrs(LAPACK_ROW_MAJOR, noTranspose, n_mkl, nrhs_mkl, a, lda_mkl, ipiv_mkl.data(), b, ldb_mkl);
	}

	// Double precisions
	int ludcmp_row_major(int n, double* a, int lda, int *ipiv)
	{
		// Create the ILP64 interface
		lapack_int n_mkl = n;
		vector<lapack_int> ipiv_mkl(n);
		lapack_int lda_mkl = lda;

		auto info_mlk = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n_mkl, n_mkl, a, lda_mkl, ipiv_mkl.data());

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv[i] = ipiv_mkl[i];
		}

		return info_mlk;
	}

	int lubksb_row_major(double *a, int n, int *ipiv, double *b)
	{
		lapack_int n_mkl = n;
		lapack_int nrhs_mkl = 1;
		lapack_int lda_mkl = n;
		lapack_int ldb_mkl = 1; // is n for column major
		vector<lapack_int> ipiv_mkl(n);

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv_mkl[i] = ipiv[i];
		}

		// Solve for B, replacing B. One set of solutions for each column of B.
		return LAPACKE_dgetrs(LAPACK_ROW_MAJOR, noTranspose, n_mkl, nrhs_mkl, a, lda_mkl, ipiv_mkl.data(), b, ldb_mkl);
	}

	vector<double> pchipslopes(const vector<double> &x, const vector<double> &y, const vector<double> &del)
	{
		//	n = length(x);
		const auto n = x.size();

		//if n == 2
		//	d = repmat(del(1), size(y));
		//return
		//	end

		// Slopes at interior points.
		//  d(k) = weighted average of del(k - 1) and del(k) when they have the same sign.
		//  d(k) = 0 when del(k - 1) and del(k) have opposites signs or either is zero.
		// d = zeros(size(y));
		vector<double>	d(y.size());

		//k = find(sign(del(1:n - 2)).*sign(del(2:n - 1)) > 0);
		vector<int> k;
		for (int i = 0; i < del.size() - 1; i++)
		{
			if ((signum(del[i]) * signum(del[i + 1])) > 0)
			{
				k.push_back(i);
			}
		}

		//h = diff(x);
		vector<double> h(n - 1);
		for (auto i = 0; i < n - 1; i++)
		{
			h[i] = x[i + 1] - x[i];
		}

		//hs = h(k) + h(k + 1);
		vector<double> hs;
		for (auto k0 : k)
		{
			hs.push_back(h[k0] + h[k0 + 1]);
		}

		// w1 = (h(k) + hs). / (3 * hs);
		// w2 = (hs + h(k + 1)). / (3 * hs);
		vector<double> w1;
		vector<double> w2;
		auto k1 = 0;
		for (auto k0 : k)
		{
			auto t = 0.0;
			w1.push_back((h[k0] + hs[k1]) / (3 * hs[k1]));
			w2.push_back((h[k0 + 1] + hs[k1]) / (3 * hs[k1]));
			++k1;
		}

		//dmax = max(abs(del(k)), abs(del(k + 1)));
		//dmin = min(abs(del(k)), abs(del(k + 1)));
		vector<double> dmax;
		vector<double> dmin;
		for (auto k0 : k)
		{
			dmax.push_back(std::max<double>(std::abs(del[k0]), std::abs(del[k0 + 1])));
			dmin.push_back(std::min<double>(std::abs(del[k0]), std::abs(del[k0 + 1])));
		}

		//d(k + 1) = dmin. / conj(w1.*(del(k). / dmax) + w2.*(del(k + 1). / dmax));
		k1 = 0;
		for (auto k0 : k)
		{
			d[k0 + 1] = dmin[k1] / (w1[k1] * del[k0] / dmax[k1] + w2[k1] * del[k0 + 1] / dmax[k1]);
			++k1;
		}

		// Slopes at end points.
		//	d(1) = ((2 * h(1) + h(2))*del(1) - h(1) * del(2)) / (h(1) + h(2));
		d[0] = ((2 * h[0] + h[1])*del[0] - h[0] * del[1]) / (h[0] + h[1]);

		// Set d(1) and d(n) via non - centered, shape - preserving three - point formula.
		//if sign(d(1)) ~= sign(del(1))
		//	d(1) = 0;
		//else if(sign(del(1)) ~= sign(del(2))) && (std::abs(d(1)) > std::abs(3 * del(1)))
		//	d(1) = 3 * del(1);
		//end
		//
		if (signum(d[0]) != signum(del[0]))
		{
			d[0] = 0;
		}
		else if ((signum(del[0]) != signum(del[1])) && (std::abs(d[0]) > std::abs(3 * del[0])))
		{
			d[0] = 3 * del[0];
		}

		//	d(n) = ((2 * h(n - 1) + h(n - 2))*del(n - 1) - h(n - 1)*del(n - 2)) / (h(n - 1) + h(n - 2));

		auto n1 = n - 1;
		d[n1] = ((2 * h[n1 - 1] + h[n1 - 2])*del[n1 - 1] - h[n1 - 1] * del[n1 - 2]) / (h[n1 - 1] + h[n1 - 2]);

		//if sign(d(n)) ~= sign(del(n - 1))
		//	d(n) = 0;
		//elseif(sign(del(n - 1)) ~= sign(del(n - 2))) && (std::abs(d(n)) > std::abs(3 * del(n - 1)))
		//	d(n) = 3 * del(n - 1);
		//end
		if (signum(d[n1]) != signum(del[n1 - 1]))
		{
			d[n1] = 0;
		}
		else if ((signum(del[n1 - 1]) != signum(del[n1 - 2])) && (std::abs(d[n1]) > std::abs(3 * del[n1 - 1])))
		{
			d[n1] = 3 * del[n1 - 1];
		}

		return d;
	}

	vector<double> pchipslopes(const vector<double> &x, const vector<double> &y)
	{
		// Compute the delta
		const auto nx = x.size();
		auto ny = y.size();
		auto ni = 0;

		vector<double> del(nx - 1);
		for (auto i = 0; i < nx - 1; i++)
		{
			del[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]);
		}

		return pchipslopes(x, y, del);
	}

	std::string getDfError(int errNum)
	{
		std::ostringstream os;

		switch (errNum)
		{
		case DF_ERROR_NULL_TASK_DESCRIPTOR:
		{
			os << "Error: null task descriptor" << errNum;
			break;
		}
		case DF_ERROR_MEM_FAILURE:
		{
			os << "Error: memory allocation failure in DF functionality" << errNum;
			break;
		}
		case DF_ERROR_BAD_NX:
		{
			os << "Error: the number of breakpoints is invalid" << errNum;
			break;
		}
		case DF_ERROR_BAD_X:
		{
			os << "Error: the array which contains the breakpoints is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_X_HINT:
		{
			os << "Error: invalid flag describing structure of partition" << errNum;
			break;
		}
		case DF_ERROR_BAD_NY:
		{
			os << "Error: invalid dimension of vector-valued function y" << errNum;
			break;
		}
		case DF_ERROR_BAD_Y:
		{
			os << "Error: the array which contains function values is invalid" << errNum;
			break;
		}
		case DF_ERROR_BAD_Y_HINT:
		{
			os << "Error: invalid flag describing structure of function y" << errNum;
			break;
		}
		case DF_ERROR_BAD_SPLINE_ORDER:
		{
			os << "Error: invalid spline order" << errNum;
			break;
		}
		case DF_ERROR_BAD_SPLINE_TYPE:
		{
			os << "Error: invalid type of the spline" << errNum;
			break;
		}
		case DF_ERROR_BAD_IC_TYPE:
		{
			os << "Error: invalid type of internal conditions used in the spline construction" << errNum;
			break;
		}
		case DF_ERROR_BAD_IC:
		{
			os << "Error: array of internal conditions for spline construction is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_BC_TYPE:
		{
			os << "Error: invalid type of boundary conditions used in the spline construction" << errNum;
			break;
		}
		case DF_ERROR_BAD_BC:
		{
			os << "Error: array which presents boundary conditions for spline construction is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_PP_COEFF:
		{
			os << "Error: array of piece-wise polynomial spline coefficients is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_PP_COEFF_HINT:
		{
			os << "Error: invalid flag describing structure of the piece-wise polynomial spline coefficients" << errNum;
			break;
		}
		case DF_ERROR_BAD_PERIODIC_VAL:
		{
			os << "Error: function values at the end points of the interpolation interval are not equal as required in periodic boundary conditions" << errNum;
			break;
		}
		case DF_ERROR_BAD_DATA_ATTR:
		{
			os << "Error: invalid attribute of the pointer to be set or modified in Data Fitting task descriptor with EditIdxPtr editor" << errNum;
			break;
		}
		case DF_ERROR_BAD_DATA_IDX:
		{
			os << "Error: index of pointer to be set or modified in Data Fitting task descriptor with EditIdxPtr editor is out of range" << errNum;
			break;
		}
		case DF_ERROR_BAD_NSITE:
		{
			os << "Error: invalid number of interpolation sites" << errNum;
			break;
		}
		case DF_ERROR_BAD_SITE:
		{
			os << "Error: array of interpolation sites is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_SITE_HINT:
		{
			os << "Error: invalid flag describing structure of interpolation sites" << errNum;
			break;
		}
		case DF_ERROR_BAD_NDORDER:
		{
			os << "Error: invalid size of array that defines order of the derivatives to be computed at the interpolation sites" << errNum;
			break;
		}
		case DF_ERROR_BAD_DORDER:
		{
			os << "Error: array defining derivative orders to be computed at interpolation sites is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_DATA_HINT:
		{
			os << "Error: invalid flag providing a-priori information about partition and/or interpolation sites" << errNum;
			break;
		}
		case DF_ERROR_BAD_INTERP:
		{
			os << "Error: array of spline based interpolation results is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_INTERP_HINT:
		{
			os << "Error: invalid flag defining structure of spline based interpolation results" << errNum;
			break;
		}
		case DF_ERROR_BAD_CELL_IDX:
		{
			os << "Error: array of indices of partition cells containing interpolation sites is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_NLIM:
		{
			os << "Error: invalid size of arrays containing integration limits" << errNum;
			break;
		}
		case DF_ERROR_BAD_LLIM:
		{
			os << "Error: array of left integration limits is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_RLIM:
		{
			os << "Error: array of right integration limits is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_INTEGR:
		{
			os << "Error: array of spline based integration results is not defined" << errNum;
			break;
		}
		case DF_ERROR_BAD_INTEGR_HINT:
		{
			os << "Error: invalid flag defining structure of spline based integration results" << errNum;
			break;
		}
		case DF_ERROR_BAD_LOOKUP_INTERP_SITE:
		{
			os << "Error: bad site provided for interpolation with look-up interpolator" << errNum;
			break;
		}
		case DF_ERROR_NULL_PTR:
		{
			os << "Error: bad pointer provided in DF function" << errNum;
			break;
		}
		default: break;
		}

		return os.str();
	}

	vector<double> pchip(const vector<double> &x, const vector<double> &y, const vector<double> &t)
	{
		auto slopes = pchipslopes(x, y);

		DFTaskPtr task;				            // Data Fitting task descriptor
		auto nx = int(x.size());			        // number of break points
		auto ny = 1;							// number of functions, always 1 for us

		auto xhint = DF_NON_UNIFORM_PARTITION;
		auto yhint = DF_NO_HINT;

		/***** Create Data Fitting task *****/
		auto errcode = dfdNewTask1D(&task, nx, x.data(), xhint, ny, y.data(), yhint);
		THROW_ON_DF_ERROR(errcode);

		/***** Edit task parameters for Hermite cubic spline construction *****/
		double bc[] = { slopes[0], slopes[nx - 1] };
		int splineCoeffSize = ny * (nx - 1) * DF_PP_CUBIC;
		vector<double> scoeff(splineCoeffSize);

		errcode = dfdEditPPSpline1D(task, DF_PP_CUBIC, DF_PP_HERMITE, DF_BC_1ST_LEFT_DER | DF_BC_1ST_RIGHT_DER, bc,
			DF_IC_1ST_DER, slopes.data() + 1, scoeff.data(), DF_NO_HINT);
		THROW_ON_DF_ERROR(errcode);

		/***** Construct Hermite cubic spline using STD method *****/
		errcode = dfdConstruct1D(task, DF_PP_SPLINE, DF_METHOD_STD);
		THROW_ON_DF_ERROR(errcode);

		/***** Interpolate using PP method *****/
		MKL_INT dorder[] = { 1 };            // spline values will be computed
		double *datahint = nullptr;
		vector<double> r(t.size());

		errcode = dfdInterpolate1D(task, DF_INTERP, DF_METHOD_PP,
			int(t.size()), t.data(), DF_NON_UNIFORM_PARTITION, 1,
			dorder, datahint, r.data(), DF_MATRIX_STORAGE_COLS, nullptr);
		THROW_ON_DF_ERROR(errcode);

		errcode = dfDeleteTask(&task);
		THROW_ON_DF_ERROR(errcode);
		return r;
	}

	vector<double> standardizeLut(const vector<double>& means, const vector<double>& sds)
	{
		//	means    means 
		//	sds      standard deviations
		//	
		//	n = numel(Means);
		auto n = means.size();

		//Means = Means + 1;
		// umin = ceil(Means(1)) - 1;  % last value of t(see below) that will not be used
		auto umin = int(std::ceil(means.front())) - 1;
		umin = std::max<int>(umin, 0);  // Just in case of the unlikely event the mean[0] is zero;

		// umax = floor(Means(n));   % last value of t that will be used
		auto umax = int(std::floor(means.back()));

		//	t = zeros(1, 1024);   % the standard error function in Miller's notation 
		vector<double> t(1024);

		//	l = umin + 1;
		auto l = umin;
		
		//for k = 2:n
		for (auto k = 1; k < n; k++)
		{
			//	gap = Means(k) - Means(k - 1);
			auto gap = means[k] - means[k - 1];

			//while (l <= Means(k))
			while (l <= means[k])
			{
				//	wt = (l - Means(k - 1)) / gap;
				auto wt = (l - means[k-1]) / gap;

				//t(l) = (1 - wt)*SEs(k - 1) + wt * SEs(k);
				t[l] = (1 - wt) * sds[k - 1] + wt * sds[k];
				//l = l + 1;
				l++;
				// end
			}
			//end
		}

		//	integral = zeros(1, 1024);  % here too, only a sub - interval of the indices are
		//  used - rest if for indexing convenience
		vector<double> integral(1024, 0);

		//for l = umin + 1:umax
		for (l = umin+1; l <= umax; l++)
		{ 
		//		integral(l) = integral(l - 1) + (1 / t(l));
			integral[l] = integral[l - 1] + (1 / t[l]);
			auto x = integral[l];
			//end
		}

		// Scale the integral to get the look - up table(L) to satisfy
		auto scaleFactor = (umax - umin) / integral[umax];

		//% Build the LUT(L)

		//	L = zeros(1, 1024);
		vector<double> L(1024);
		
		//	L(umin) = umin
		//	L(umax) = umax
		L[umin] = umin;
		L[umax] = umax;
		//L(umin + 1:umax) = umin + ScaleFactor * integral(umin + 1:umax);


		for (l = umin+1; l < umax; l++)
		{
			L[l] = float(umin + scaleFactor * integral[l]);
		}

		// Finally, extrapolate with monotone increasing cubic spline to L(0) = 0 and
		// L(1023) = 1023.

		//	x = [1 umin umin + 1];
		vector<double> x = { 0, double(umin), double(umin + 1) };
		//y = [1 L(umin) L(umin + 1)];

		vector<double> y = { 0, L[umin], L[umin + 1] };
		//t = 1:1 : (umin - 1);

		vector<double> ts(umin);
		for (auto i = 0; i < umin; i++)
		{
			ts[i] = i;
		}
		
		auto p = pchip(x, y, ts);
		//L(t) = p;
		for (auto i = 0; i < p.size(); i++)
		{
			L[i] = p[i];
		}

		//% umax + 1 to 1024

		x = { umax - 1.0, double(umax), 1023.0 };
		y = { L[umax - 1], L[umax], 1023.0 };
		vector<double> te(1023 - umax);
		//t = umax + 1:1024;
		for (auto i = 0; i < te.size(); i++)
		{
			te[i] = i + umax + 1;
		}

		auto xss = te.back();
		p = pchip(x, y, te);
		for (auto i = 0; i < p.size(); i++)
		{
			L[i + umax + 1] = p[i];
		}


		//p = pchip(x, y, t);
		//L(t) = p;
		//
		return L;
	}

	std::tuple<double, double, int> meanAndSdFromHistogram(const vector<int> &counts, const double maxValue, double defaultMean, double defaultStdev)
	{
		const auto numBins = int(counts.size());
		const auto factor = maxValue / numBins;

		int virtualSize = 0;
		auto virtualSum = 0.0;
		for (auto i = 0; i < numBins; i++)
		{
			virtualSize += counts[i];
			virtualSum += factor * i * counts[i];
		}

		if (virtualSize == 0)
		{
			return std::tuple<double, double, int>(defaultMean, defaultStdev, 0);
		}

		auto mean = virtualSum / virtualSize;
		auto sumVirtualDiffs = 0.0;
		
		for (auto i = 0; i < numBins; i++)
		{
			const auto virtualDiff = factor * i - mean;
			sumVirtualDiffs += counts[i] * (virtualDiff * virtualDiff);
		}
		
		auto sd = std::sqrt(sumVirtualDiffs / virtualSize);

		return std::tuple<double, double, int>(mean, sd, virtualSize);
	}
}
