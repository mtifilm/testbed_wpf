#pragma once
#ifndef LabelMarkerH
#define LabelMarkerH

#include "Ippheaders.h"

// This takes a 8u image and labels connected sections
// It is just a wrapper around the ippiLabelMarkers_8u32s_C1R
//
//   LabelMarker labelMaker;
//   Ipp32sArray labelArray = labelMaker.createLabels(ippArray);
//
//   To turn on multiprocessing 
//      labelMaker.setMaxThreads(8);
//
class MTI_IPPARRAY_API LabelMarker final
{
public:
	LabelMarker(IppiNorm norm = IppiNorm::ippiNormInf);

	IppiNorm getConnectNorm() const { return _connectNorm; }
	int getNumberOfLabels() const { return _numberOfLabels; }

	//virtual ~LabelMarker();
	Ipp32sArray label(const Ipp8uArray &inputImage);

	void setMinMaxLabel(int minLabel, int maxLabel);

   vector<vector<int>> getClusterIndices(const Ipp32sArray &labelMask, int numberOfLabels, int minSize = 0, int maxSize = INT_MAX) const;
	vector<vector<int>> getEquivalenceIndices(const Ipp16uArray &image) const;

private:
	IppiNorm _connectNorm = IppiNorm::ippiNormInf;
	Ipp8uArray _tempBuffer;
	int _minLabel = 1;
	int _maxLabel = INT_MAX-1;
	int _numberOfLabels = 0;
};

#endif

