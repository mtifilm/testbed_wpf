#include "FastHistogramIpp.h"
#include <algorithm>

FastHistogramIpp::FastHistogramIpp() = default;

FastHistogramIpp FastHistogramIpp::_fastHistogram;

std::shared_ptr<HistogramIpp>  FastHistogramIpp::getHistogramIpp(const Ipp32fArray &image, int bins, float dataMin, float dataMax)
{
	return getHistogramIpp(image.getSize(), image.getComponents(), image.getDataType(), bins, dataMin, dataMax);
}

std::shared_ptr<HistogramIpp>  FastHistogramIpp::getHistogramIpp(const Ipp16uArray &image, int bins, float dataMin, float dataMax)
{
	return getHistogramIpp(image.getSize(), image.getComponents(), image.getDataType(), bins, dataMin, dataMax);
}


std::shared_ptr<HistogramIpp>  FastHistogramIpp::getHistogramIpp(const IppiSize &size, int components, IppDataType dataType, int bins, float dataMin, float dataMax)
{
	std::lock_guard<std::mutex> lock(_lockMutex);

	auto iter = std::find_if(_flyweightCache.begin(), _flyweightCache.end(), [=](std::shared_ptr<HistogramIpp> p)
	{ return p->isSameAs(size, components, dataType, bins, dataMin, dataMax); });

	if (iter == _flyweightCache.end())
	{
		auto np = std::make_shared<HistogramIpp>(size, components, dataType, bins, dataMin, dataMax);
		_flyweightCache.push_front(np);
	}
	else if (iter != _flyweightCache.begin())
	{
		auto np = *iter;
		_flyweightCache.erase(iter);
		_flyweightCache.push_front(np);
	}

	// see if we are too big
	if (_flyweightCache.size() > highwater)
	{
		while (_flyweightCache.size() > lowwater)
		{
			_flyweightCache.pop_back();
		}
	}

	return	_flyweightCache[0];
}

vector<vector<Ipp32u>> FastHistogramIpp::compute(const Ipp32fArray &image, int bins, float dataMin, float dataMax)
{
	return getInstance().getHistogramIpp(image, bins, dataMin, dataMax)->compute(image);
}

Ipp32sArray FastHistogramIpp::computeIpp(const Ipp32fArray &image, int bins, float dataMin, float dataMax)
{
	return getInstance().getHistogramIpp(image, bins, dataMin, dataMax)->computeF(image);
}

vector<vector<Ipp32u>> FastHistogramIpp::compute(const Ipp16uArray &image, int bins, float dataMin, float dataMax)
{
	return getInstance().getHistogramIpp(image, bins, dataMin, dataMax)->compute(image);
}

vector<vector<Ipp32f>> FastHistogramIpp::computeProbability(const Ipp32fArray &image, int bins, float dataMin, float dataMax)
{
	return getInstance().getHistogramIpp(image, bins, dataMin, dataMax)->computeProbability(image);
}