#include "IppArray.h"
#include "Ippheaders.h"
#include "IppAgnostic.h"
#include <sstream>
#include <random>
#include <algorithm>
#include "MtiAncillaryTemplates.h"

namespace
{
	std::default_random_engine generator;
}

void Ipp32sArray::normalDistribution(double mean, double stddev)
{
	std::normal_distribution<double> distribution((double)mean, (double)stddev);

	const auto rows = getRoi().height;
	const auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto c = 0; c < getComponents(); c++)
			{
				*inp++ = Ipp32s(distribution(generator));
			}
		}
	}
}

void Ipp32sArray::uniformDistribution(Ipp32s low, Ipp32s high)
{
	std::uniform_int_distribution<Ipp32s> distribution(low, high);
	generator.seed(static_cast<unsigned int>(time(nullptr)));

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto d = 0; d < getComponents(); d++)
			{
				*inp++ = Ipp32s(distribution(generator));
			}
		}
	}
}

IPP_ARRAY_APPLY_VALUE(Ipp32sArray, set, Ipp32s, ippiSet_32s)

vector<Ipp64f> Ipp32sArray::sum() const
{
	vector<Ipp64f> result(getComponents(), 0);

	auto rangeCol = range(getWidth());
	switch (getComponents())
	{
	case 1:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				result[0] += *rp++;
			}
		}

		break;

	case 3:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				result[0] += *rp++;
				result[1] += *rp++;
				result[2] += *rp++;
			}
		}

		break;

	default:
		throwComponentError();
	}

	return result;
}

void Ipp32sArray::copyFromPod(Ipp32s *source, int size)
{
	auto roiSize = getSize();
	auto roi = getRoi();

	if ((size / getComponents()) != (roi.height * roi.width))
	{
		THROW_MTI_RUNTIME_ERROR(string("Size, roi area mismathc in ") + __func__);
	}

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp32s));

	switch (getComponents())
	{
	case 1:
		ippiCopy_32s_C1R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;

	case 3:
		ippiCopy_32s_C3R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;
	}
}

void Ipp32sArray::Y2RGB(const Ipp32sArray & src)
{
	if (src.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Input to Y2RGB must have only one component!");
	}

	if (getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Target of Y2RGB must have channels 3!");
	}

	auto &roi = getRoi();
	auto &srcRoi = src.getRoi();
	if (roi.width != srcRoi.width || roi.height != srcRoi.height)
	{
		THROW_MTI_RUNTIME_ERROR("Dimension mismatch in input to Y2RGB!");
	}

	copyFromPlanar({ src, src, src });
}

MtiPlanar<Ipp32sArray> Ipp32sArray::copyToPlanar() const
{
	if (getComponents() == 1)
	{
		return { *this };
	}

	// This is the ugliest code I've every written, but dementia does this 
	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	MtiPlanar<Ipp32sArray> result;
	for (auto i : range(getComponents()))
	{
		result.emplace_back(Ipp32sArray({ getSize(), 1 }));
      result.back().setOriginalBits(getOriginalBits());
	}

	if (getComponents() == 3)
	{
		Ipp32s *dstPtrs[3] = { result[0].data(), result[1].data(), result[2].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32s_C3P3R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	if (getComponents() == 4)
	{
		Ipp32s *dstPtrs[4] = { result[0].data(), result[1].data(), result[2].data(), result[3].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32s_C4P4R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	THROW_MTI_RUNTIME_ERROR("Number of components must be 1, 3 or 4");
}

void Ipp32sArray::copyFromPlanar(const MtiPlanar<Ipp32sArray> &planes)
{
	auto channels = int(planes.size());
	if ((channels != 1) && (channels != 3) && (channels != 4))
	{
		THROW_MTI_RUNTIME_ERROR("copyFromPlanar needs a 1, 3 or 4 size planar array");
	}

	// See if we are a null array
	this->createStorageIfEmpty(planes[0].getHeight(), planes[0].getWidth(), channels);

	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	// Planar arrays are same size, so just check the first
	if (roiSize.width != planes[0].getRoi().width || roiSize.height != planes[0].getRoi().height)
	{
		THROW_MTI_RUNTIME_ERROR("All operands of CopyFromPlanar must be the same size!");
	}

	setOriginalBits(planes[0].getOriginalBits());

	for (auto i = 1; i < channels; i++)
		if (planes[0].getRowPitchInBytes() != planes[i].getRowPitchInBytes())
		{
			THROW_MTI_RUNTIME_ERROR("All sources of copyFromPlanar must have same row pitch!");
		}

	if (channels == 1)
	{
		*this <<= planes[0];
		return;
	}

	if (channels == 3)
	{
		Ipp32s *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32s_P3C3R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
		return;
	}

	if (channels == 4)
	{
		Ipp32s *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data(), planes[3].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_32s_P4C4R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
	}
}

Ipp32sArray Ipp32sArray::operator -(IppArray<Ipp32s>& rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);

	auto size = getSize();
	Ipp32sArray result(size);

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);
		auto dp = result.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp1++ - *sp2++;
			}
		}
	}

	return result;
}

// Select operator for different types of copy
Ipp32sArray Ipp32sArray::operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData) const
{
	return SelectFromFunction(*this, probeRoi, copyType, utilizeExtrinsicData);
}

Ipp32sArray Ipp32sArray::operator +(const Ipp32s scalar) const
{
	auto size = getSize();
	Ipp32sArray result(size);

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp = getRowPointer(r);
		auto dp = result.getRowPointer(r);
		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp++ + scalar;
			}
		}
	}

	return result;
}

Ipp32sArray Ipp32sArray::operator -(const Ipp32s scalar) const
{
	auto size = getSize();
	Ipp32sArray result(size);

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp = getRowPointer(r);
		auto dp = result.getRowPointer(r);
		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp++ - scalar;
			}
		}
	}

	return result;
}

Ipp32sArray Ipp32sArray::operator +(IppArray<Ipp32s> &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);

	auto size = getSize();
	Ipp32sArray result(size);

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);
		auto dp = result.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp1++ + *sp2++;
			}
		}
	}

	return result;
}

Ipp32sArray Ipp32sArray::operator *(IppArray<Ipp32s> &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);

	auto size = getSize();
	Ipp32sArray result(getSize());

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);
		auto dp = result.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp1++ * *sp2++;
			}
		}
	}

	return result;
}

Ipp32sArray Ipp32sArray::operator /(IppArray<Ipp32s> &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);

	auto size = getSize();
	Ipp32sArray result(getSize());

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);
		auto dp = result.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				*dp++ = *sp1++ / *sp2++;
			}
		}
	}

	return result;
}

Ipp32sArray Ipp32sArray::operator *(const Ipp32s scalar) const
{
   auto size = getSize();
   Ipp32sArray result(size);

   auto rangeCol = range(getWidth());
   auto rangeRow = range(getHeight());
   for (auto r : rangeRow)
   {
      auto sp = getRowPointer(r);
      auto dp = result.getRowPointer(r);
      for (auto i : rangeCol)
      {
         for (auto c : range(getComponents()))
         {
            *dp++ = *sp++ * scalar;
         }
      }
   }

   return result;
}


Ipp32sArray Ipp32sArray::operator /(const Ipp32s scalar) const
{
   auto size = getSize();
   Ipp32sArray result(size);

   auto rangeCol = range(getWidth());
   auto rangeRow = range(getHeight());
   for (auto r : rangeRow)
   {
      auto sp = getRowPointer(r);
      auto dp = result.getRowPointer(r);
      for (auto i : rangeCol)
      {
         for (auto c : range(getComponents()))
         {
            *dp++ = *sp++ / scalar;
         }
      }
   }

   return result;
}

Ipp32sArray& Ipp32sArray::operator+=(const Ipp32s scalar)
{
   for (auto r = 0; r < getHeight(); r++)
   {
      auto sp = getRowPointer(r);
      for (auto c = 0; c < getWidth()*getComponents(); c++)
      {
            sp[c] += scalar;
      }
   }

   return *this;
}

Ipp32sArray& Ipp32sArray::operator-=(const Ipp32s scalar)
{
   for (auto r = 0; r < getHeight(); r++)
   {
      auto sp = getRowPointer(r);
      for (auto c = 0; c < getWidth()*getComponents(); c++)
      {
         sp[c] -= scalar;
      }
   }

   return *this;
}

Ipp32sArray& Ipp32sArray::operator*=(const Ipp32s scalar)
{
   for (auto r = 0; r < getHeight(); r++)
   {
      auto sp = getRowPointer(r);
      for (auto c = 0; c < getWidth()*getComponents(); c++)
      {
         sp[c] *= scalar;
      }
   }

   return *this;
}

Ipp32sArray& Ipp32sArray::operator/=(const Ipp32s scalar)
{
   for (auto r = 0; r < getHeight(); r++)
   {
      auto sp = getRowPointer(r);
      for (auto c = 0; c < getWidth()*getComponents(); c++)
      {
         sp[c] /= scalar;
      }
   }

   return *this;
}

IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32sArray, Ipp8u, ippiConvert_8u32s)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32sArray, Ipp16u, ippiConvert_16u32s)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp32sArray, Ipp32s, ippiCopy_32s)
void Ipp32sArray::convertFrom(const IppArray<Ipp32f> &rhs) { THROW_MTI_RUNTIME_ERROR("Conversion not supported"); }

vector<Ipp32s> Ipp32sArray::median(const IppiRect &roi) const
{
	vector<Ipp32s> result;
	auto vects = toVector(roi);
	for (auto &vec : vects)
	{
		result.push_back(median(vec));
	}

	return result;
}

vector<Ipp32s> Ipp32sArray::median() const
{
	return median({ 0, 0, getWidth(), getHeight() });
}

Ipp32s Ipp32sArray::medianSort(vector<Ipp32s> &values)
{
	std::sort(values.begin(), values.end());
	auto n = values.size();
	auto m = n / 2;

	if ((n % 2) == 0)
	{
		return (values[m - 1] + values[m]) / 2;;
	}

	return values[m];
}

Ipp32s Ipp32sArray::median(vector<Ipp32s> &values)
{
	return medianSort(values);
}

Ipp32s Ipp32sArray::medianQuick(vector<Ipp32s> &values)
{
	int low, high;
	int median;
	int middle, cl, hh;
	Ipp32s *arr = values.data();
	int n = (int)values.size();

	low = 0; high = n - 1; median = (low + high) / 2;
	for (;;)
	{
		if (high <= low) /* One element only */
			return arr[median];

		if (high == low + 1)
		{  /* Two elements only */
			if (arr[low] > arr[high])
				std::swap(arr[low], arr[high]);

			return arr[median];
		}

		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])    std::swap(arr[middle], arr[high]);
		if (arr[low] > arr[high])       std::swap(arr[low], arr[high]);
		if (arr[middle] > arr[low])     std::swap(arr[middle], arr[low]);

		/* Swap low item (now in position middle) into position (low+1) */
		std::swap(arr[middle], arr[low + 1]);

		/* Nibble from each end towards middle, swapping items when stuck */
		cl = low + 1;
		hh = high;
		for (;;)
		{
			do cl++; while (arr[low] > arr[cl]);
			do hh--; while (arr[hh] > arr[low]);

			if (hh < cl)
				break;

			std::swap(arr[cl], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		std::swap(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = cl;
		if (hh >= median)
			high = hh - 1;
	}
}

vector<Ipp64f> Ipp32sArray::L2Norm() const
{
	vector<Ipp64f> result(getComponents(), 0);

	auto rangeCol = range(getWidth());
	switch (getComponents())
	{
	case 1:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				auto r = *rp++;
				result[0] += r*r;
			}
		}

		break;

	case 3:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				auto r = *rp++;
				result[0] += r*r;

				r = *rp++;
				result[1] += r*r;

				r = *rp++;
				result[2] += r*r;
			}
		}

		break;

	default:
		throwComponentError();
	}

	for (auto &r : result)
	{
		r = std::sqrt(r);
	}

	return result;
}

vector<Ipp64f> Ipp32sArray::L1Norm() const
{
	vector<Ipp64f> result(getComponents(), 0);

	auto rangeCol = range(getWidth());
	switch (getComponents())
	{
	case 1:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				result[0] += abs(*rp++);
			}
		}

		break;

	case 3:
		for (auto rp : rowIterator())
		{
			for (auto c : rangeCol)
			{
				result[0] += abs(*rp++);
				result[1] += abs(*rp++);
				result[2] += abs(*rp++);
			}
		}

		break;

	default:
		throwComponentError();
	}

	return result;
}

void Ipp32sArray::L1NormDiff(const IppArray<Ipp32s> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);

   for (auto i=0; i < getComponents(); i++)
   {
      result[i] = 0;
   }

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				result[c] += abs(*sp1++ - *sp2++);
			}
		}
	}

	return;
}

void Ipp32sArray::L2NormDiff(const IppArray<Ipp32s> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);

   for (auto i=0; i < getComponents(); i++)
   {
      result[i] = 0;
   }

	auto rangeCol = range(getWidth());
	auto rangeRow = range(getHeight());
	for (auto r : rangeRow)
	{
		auto sp1 = getRowPointer(r);
		auto sp2 = rhs.getRowPointer(r);

		for (auto i : rangeCol)
		{
			for (auto c : range(getComponents()))
			{
				auto v = Ipp64f(*sp1++ - *sp2++);
				result[c] += v*v;
			}
		}
	}

   for (auto i=0; i < getComponents(); i++)
   {
      result[i] = std::sqrt(result[i]);
   }

	return;
}


Ipp32fArray Ipp32sArray::crossCorrNorm(const IppArray<Ipp32s> &kernalImage, IppiROIShape roiShape) const
{
	throwNotImplemented();
	return Ipp32fArray();
}

void Ipp32sArray::maxAndIndex(Ipp32s *maxValues, MtiPoint *indices) const
{
	MtiSize size = getSize();
	//int indexX[3];
	//int indexY[3];

	switch (getComponents())
	{
	case 1:
	{
		auto m = *data();
		for (auto r = 0; r < getHeight(); r++)
		{
			auto rp = getRowPointer(r);
			for (auto c = 0; c < getWidth(); c++)
			{
				if (rp[c] > m)
				{
					m = rp[c];
					indices[0] = { c, r };
					maxValues[0] = m;
				}
			}
		}
	}
	break;

	//case 3:
	//	IppThrowOnError(ippiMaxIndx_32s_C3R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
	//	indices[0] = { indexX[0], indexY[0] };
	//	indices[1] = { indexX[1], indexY[1] };
	//	indices[2] = { indexX[2], indexY[2] };
	//	break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}
}

std::pair<std::vector<Ipp32s>, std::vector<MtiPoint>> Ipp32sArray::maxAndIndex() const
{
	const auto components = getComponents();
	std::vector<Ipp32s> maxValues(components);
	std::vector<MtiPoint> indices(components);

	maxAndIndex(maxValues.data(), indices.data());
	return { maxValues, indices };
}

Ipp32sArray Ipp32sArray::transpose() const
{
	// Fake it as a 32f array
	const auto roi = getRoi();

	// Note: transpose swaps the size!  Do not use getSize here
	Ipp32sArray result({ roi.height, roi.width, getComponents() });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiTranspose_32f_C1R((Ipp32f *)dataPtr, getRowPitchInBytes(), (Ipp32f *)result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 3:
		IppThrowOnError(ippiTranspose_32f_C3R((Ipp32f *)dataPtr, getRowPitchInBytes(), (Ipp32f *)result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiTranspose_32f_C4R((Ipp32f *)dataPtr, getRowPitchInBytes(), (Ipp32f *)result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

vector<Ipp64f> Ipp32sArray::dotProduct(const IppArray<Ipp32s> &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiDotProd_32s64f_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;

	case 3:
	{
		IppThrowOnError(ippiDotProd_32s64f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;
	}


	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

Ipp32sArray Ipp32sArray::mirror(IppiAxis flip) const
{
	Ipp32sArray result(getSize());

	auto roiSize = getSize();
	auto roi = getRoi();

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp16u));

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMirror_32s_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 3:
		IppThrowOnError(ippiMirror_32s_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 4:
		IppThrowOnError(ippiMirror_32s_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	default:
		throwComponentError();
	}

	return result;
}

vector<Ipp64f> Ipp32sArray::mean() const
{
	vector<Ipp64f> result = sum();
	for (auto &r : result)
	{
		r = r / area();
	}

	return result;
}

Ipp32sArray::operator Ipp32fArray() const
	{
		Ipp32fArray result;
		result <<= *this;
		return result;
	}

Ipp32sArray::operator Ipp16uArray() const
{
	Ipp16uArray result;
	result <<= *this;
	return result;
}

Ipp32sArray::operator Ipp8uArray() const
{
	Ipp8uArray result;
	result <<= *this;
	return result;
}

Ipp32sArray Ipp32sArray::logLut(int n, Ipp32s maxValue, Ipp32s startValue)
{
	// Linear log if start is zero
	if (startValue == 0)
	{
		auto v = MtiMath::linspace<int>(0, maxValue, n);
		return Ipp32sArray({ n }, v.data(), true);
	}

	// Todo: combine this
	Ipp32sArray result(n);
	result.iota(startValue);

	auto s = std::log10(result[0]);
	auto e = std::log10(result[n - 1]);
	for (auto &r : result)
	{
		r = my_lround((std::log10(r) - s) / (e - s) * maxValue);
	}

	return result;
}

Ipp32sArray Ipp32sArray::linspace(Ipp32s startValue, Ipp32s endValue, int width)
{
	auto linSpaceV = MtiMath::linspace(startValue, endValue, width);
	return Ipp32sArray(width, linSpaceV.data(), true);
}

//
//Ipp32sArray::operator Ipp16uArray() const
//{
//	Ipp16uArray result(getSize());
//	switch (getComponents())
//	{
//	case 1:
//		IppThrowOnError(ippiConvert_32s16u_C1RSfs(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), IppRoundMode::ippRndNear, 0));
//		break;
//
//	case 3:
//	{
//		IppiSize size3 = { 3 * result.getSize().width, 3 * result.getSize().height };
//		IppThrowOnError(ippiConvert_32s16u_C1RSfs(data(), 3 * getRowPitchInBytes(), result.data(), 3 * result.getRowPitchInBytes(), size3, IppRoundMode::ippRndNear, 0));
//	}
//	break;
//
//	default:
//		throwComponentError();
//	}
//
//	return result;
//}
//
//Ipp32sArray::operator Ipp8uArray() const
//{
//	Ipp8uArray result(getSize());
//	switch (getComponents())
//	{
//	case 1:
//		IppThrowOnError(ippiConvert_32s8u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize()));
//		break;
//
//	case 3:
//	{
//		IppiSize size3 = { 3 * result.getSize().width, 3 * result.getSize().height };
//		IppThrowOnError(ippiConvert_32s8u_C1R(data(), 3 * getRowPitchInBytes(), result.data(), 3 * result.getRowPitchInBytes(), size3));
//	}
//	break;
//
//	default:
//		throwComponentError();
//	}
//
//	return result;
//}
////
//	IppArray Ipp32sArray::Resize(IppiSize newSize) const
//	{
//		if (_data.get() == nullptr || _data->dataPtr == nullptr)
//		{
//			throw std::exception("Can't resize empty IppArray!");
//		}
//
//		if (getComponents() != 1 && getComponents() != 3)
//		{
//			throw std::exception("Bad number of channels as input to Resize");
//		}
//
//		IppArray dst(newSize.height, newSize.width, getComponents());
//		IppiSize srcSize = { _roi.width, _roi.height };
//
//		IppStatus status = IppResize_C1R_or_C3R(
//			data(),
//			srcSize,
//			getRowPitchInBytes(),
//			dst.data(),
//			newSize,
//			dst.getRowPitchInBytes(),
//			getComponents());
//
//		return dst;
//	}
//
//
//	void Ipp32sArray::Y2RGB(const IppArray& src)
//	{
//		if (src.getComponents() != 1)
//		{
//			THROW_MTI_RUNTIME_ERROR("Input to Y2RGB must have only one component!");
//		}
//
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("Target of Y2RGB must have channels 3!");
//		}
//
//		if (_roi.width != src._roi.width || _roi.height != src._roi.height)
//		{
//			THROW_MTI_RUNTIME_ERROR("Dimension mismatch in input to Y2RGB!");
//		}
//
//		IppArray src3[3] = { src, src, src };
//		CopyFromPlanar(src3);
//	}
//
//
//	IppArray Ipp32sArray::ImportRgbToYuv(unsigned short *p, int rows, int cols, int dataMax)
//	{
//		IppArray result(rows, cols, 3);
//		result.ImportNormalizedYuvFromRgb(p, rows, cols, dataMax);
//
//		//    // Everything is contigious, so we just have to loop.
//		//    auto inp = p;
//		//    auto outp = result.data();
//		//    for (auto i = 0; i < rows * cols; i++)
//		//    {
//		//        float r = *(inp + 0) / float(dataMax);
//		//        float g = *(inp + 1) / float(dataMax);
//		//        float b = *(inp + 2) / float(dataMax);
//		//        inp += 3;
//		//
//		//        auto y = 0.299f * r + 0.587f * g + 0.114f * b;
//		//        *(outp + 0) = y;
//		//        *(outp + 1) = 0.492f * (b - y);
//		//        *(outp + 2) = 0.877f * (r - y);
//		//
//		//        outp += 3;
//		//    }
//		//
//		return result;
//	}
//
//	void Ipp32sArray::ImportNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax)
//	{
//		if ((rows != _roi.height) || (cols != _roi.width))
//		{
//			std::ostringstream os;
//			os << "Invalid size in ImportNormalizedYuvFromRgb, expected (" << _roi.height << "," << _roi.width <<
//				" got (" << rows << "," << cols << ")";
//
//			THROW_MTI_RUNTIME_ERROR(os.str());
//		}
//
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("ImportNormalizedYuvFromRgb needs rgb data");
//		}
//
//		auto inp = p;
//		auto oneOverDataMax = 1.0f / float(dataMax);
//		for (auto r = 0; r < rows; r++)
//		{
//			auto outp = GetPointerToElement(r);
//			for (auto c = 0; c < cols; c++)
//			{
//				float r = *inp++ * oneOverDataMax;
//				float g = *inp++ * oneOverDataMax;
//				float b = *inp++ * oneOverDataMax;
//
//				auto y = 0.299f * r + 0.587f * g + 0.114f * b;
//				*outp++ = y;
//				*outp++ = 0.492f * (b - y);
//				*outp++ = 0.877f * (r - y);
//			}
//		}
//	}
//
//	std::shared_ptr<unsigned short> Ipp32sArray::ExportYuvToRgb(int dataMax)
//	{
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("Source of ExportYuvToRgb must have channels 3!");
//		}
//
//		auto rows = _roi.height;
//		auto cols = _roi.width;
//		auto imageData = new unsigned short[cols * rows * getComponents()];
//
//		auto result = std::shared_ptr<unsigned short>(imageData);
//		ExportRgbFromNormalizedYuv(imageData, dataMax);
//
//		return result;
//	}
//
//	void Ipp32sArray::ExportRgbFromNormalizedYuv(unsigned short *p, int dataMax)
//	{
//		if (getComponents() != 3)
//		{
//			THROW_MTI_RUNTIME_ERROR("Source of ExportYuvToRgb must have channels 3!");
//		}
//
//		auto rows = _roi.height;
//		auto cols = _roi.width;
//
//		unsigned short *outp = p;
//		for (auto r = 0; r < rows; r++)
//		{
//			auto inp = GetPointerToElement(r);
//			for (auto c = 0; c < cols; c++)
//			{
//				auto y = *inp++;
//				auto u = *inp++;
//				auto v = *inp++;
//
//				*outp++ = unsigned short((y + 1.140f*v) * dataMax);
//				*outp++ = unsigned short((y - 0.394f*u - 0.581f*v) * dataMax);
//				*outp++ = unsigned short((y + 2.032f*u) * dataMax);
//			}
//		}
//	}
//
//
//	namespace
//	{
//
//		IppStatus IppResize_C1R_or_C3R(Ipp32s* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32s* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
//		{
//			if (numChannels != 1 && numChannels != 3)
//			{
//				THROW_MTI_RUNTIME_ERROR("Bad number of channels in input to Resize");
//			}
//
//			IppiResizeSpec_32f* pSpec = 0;
//			int specSize = 0, initSize = 0, bufSize = 0;
//			Ipp8u* pBuffer = 0;
//			Ipp8u* pInitBuf = 0;
//			IppiPoint dstOffset = { 0, 0 };
//			IppStatus status = ippStsNoErr;
//			IppiBorderType border = ippBorderRepl;
//
//			// ippiResize<Interpolation>Init functions has the following constraints
//			// for the minimal size of an input image depending on the chosen
//			// interpolation method:
//			//    Nearest Neighbor - 1x1
//			//    Linear           - 2x2
//			//    2-lobed Lanczos  - 4x4
//			//    3-lobed Lanczos  - 6x6
//			IppiInterpolationType interpolation = ippLanczos;
//			Ipp32u numLanczosLobes = 3;
//			const int minLinearDim = 2;
//			const int minLanczos2Dim = 4;
//			const int minLanczos3Dim = 6;
//			if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
//				|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
//			{
//				interpolation = ippNearest;
//			}
//			else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
//				|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
//			{
//				interpolation = ippLinear;
//			}
//			else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
//				|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
//			{
//				numLanczosLobes = 2;
//			}
//
//			// Allocate spec and init buffers.
//			status = ippiResizeGetSize_32f(srcSize, dstSize, interpolation, 0, &specSize, &initSize);
//			if (status != ippStsNoErr)
//			{
//				return status;
//			}
//
//			pSpec = (IppiResizeSpec_32f*)ippsMalloc_8u(specSize);
//			if (pSpec == NULL)
//			{
//				return ippStsNoMemErr;
//			}
//
//			// Initialize the filter.
//			switch (interpolation)
//			{
//			case ippNearest:
//				status = ippiResizeNearestInit_32f(srcSize, dstSize, pSpec);
//				break;
//
//			case ippLinear:
//				status = ippiResizeLinearInit_32f(srcSize, dstSize, pSpec);
//				break;
//
//			case ippLanczos:
//				pInitBuf = ippsMalloc_8u(initSize);
//				if (pInitBuf == NULL)
//				{
//					ippsFree(pSpec);
//					return ippStsNoMemErr;
//				}
//
//				status = ippiResizeLanczosInit_32f(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
//				break;
//
//			default:
//				break;
//			}
//
//			ippsFree(pInitBuf);
//			if (status != ippStsNoErr)
//			{
//				ippsFree(pSpec);
//				return status;
//			}
//
//			// Allocate work buffer.
//			status = ippiResizeGetBufferSize_32f(pSpec, dstSize, numChannels, &bufSize);
//			if (status != ippStsNoErr)
//			{
//				ippsFree(pSpec);
//				ippsFree(pInitBuf);
//				return status;
//			}
//
//			pBuffer = ippsMalloc_8u(bufSize);
//			if (pBuffer == nullptr)
//			{
//				ippsFree(pSpec);
//				ippsFree(pInitBuf);
//				return ippStsNoMemErr;
//			}
//
//			/* Resize processing */
//			if (numChannels == 1)
//			{
//				switch (interpolation)
//				{
//				case ippNearest:
//					status = ippiResizeNearest_32s_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
//					break;
//
//				case ippLinear:
//					status = ippiResizeLinear_32s_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
//					break;
//
//				case ippLanczos:
//					status = ippiResizeLanczos_32s_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
//					break;
//
//				default:
//					break;
//				}
//			}
//			else
//			{
//				switch (interpolation)
//				{
//				case ippNearest:
//					status = ippiResizeNearest_32s_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
//					break;
//
//				case ippLinear:
//					status = ippiResizeLinear_32s_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
//					break;
//
//				case ippLanczos:
//					status = ippiResizeLanczos_32s_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
//					break;
//
//				default:
//					break;
//				}
//			}
//
//			ippsFree(pSpec);
//			ippsFree(pBuffer);
//
//			return status;
//		}
//	}; // end anonymous namespace


