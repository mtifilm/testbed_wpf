#ifndef HistogramIppH
#define HistogramIppH

// This class creates histograms using Ipp
// This allows a whole image to be processed without much copying.
//
//  usage:
//   Mat image;
//   HistogramMcv histogram(image, 100);  // use default maximum range
//   HistogramMcv histogram(image, 100, 0, 1000)  // 0 to 1000
//       results = histogram(image);
//       results = histogram(image2);  Where image2 size is same a image;
//  These histograms assume linear distributed bins and equal number for each channel. It may appear weird
// that size of ROI is part of the histogram but that is because a histogram is initialized and then can
// be used over and over again.  To optimize speed, the size must be know by ipp.

#include "ipps.h"
#include "ippi.h"
#include <thread>
#include <mutex>
#include "IppArray.h"
#include "Ipp32fArray.h"
#include "Ipp32sArray.h"
#include "Ipp16uArray.h"
#include "Ipp8uArray.h"

class MTI_IPPARRAY_API HistogramIpp
{
public:

	/// <summary>
	/// Initializes a new instance of the <see cref="HistogramMcv" /> class.
	/// </summary>
	/// <param name="size">The size, this is ROI size, not image size</param>
	/// <param name="dataType">The opencv type of the data.</param>
	/// <param name="bins">The number of bins.</param>
	/// <param name="dataMin">The histogram data minimum.</param>
	/// <param name="dataMax">The histogram data maximum.</param>
	HistogramIpp(const IppiSize &size, int channels, IppDataType dataType, int bins, float dataMin = 0, float dataMax = 0);


	/// <summary>
	/// Initializes a new instance of the <see cref="HistogramMcv" /> class.
	/// </summary>
	/// <param name="image">The image.</param>
	/// <param name="bins">The number of bins.</param>
	/// <param name="dataMin">The histogram data minimum.</param>
	/// <param name="dataMax">The histogram data maximum.</param>
	HistogramIpp(Ipp32fArray  &image, int bins, float dataMin = 0, float dataMax = 0) : HistogramIpp(image.getSize(), image.getComponents(), image.getDataType(), bins, dataMin, dataMax)
	{
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="HistogramMcv" /> class.
	/// </summary>
	/// <param name="image">The image.</param>
	/// <param name="bins">The number of bins.</param>
	/// <param name="dataMin">The histogram data minimum.</param>
	/// <param name="dataMax">The histogram data maximum.</param>
	HistogramIpp(Ipp16uArray  &image, int bins, Ipp16u dataMin = 0, Ipp16u dataMax = 0) : HistogramIpp(image.getSize(), image.getComponents(), image.getDataType(), bins, float(dataMin), float(dataMax))
	{
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="HistogramMcv" /> class.
	/// </summary>
	/// <param name="image">The image.</param>
	/// <param name="bins">The number of bins.</param>
	/// <param name="dataMin">The histogram data minimum.</param>
	/// <param name="dataMax">The histogram data maximum.</param>
	HistogramIpp(Ipp8uArray  &image, int bins, Ipp8u dataMin = 0, Ipp8u dataMax = 0) : HistogramIpp(image.getSize(), image.getComponents(), image.getDataType(), bins, float(dataMin), float(dataMax))
	{
	}

	/// <summary>
	/// Finalizes an instance of the <see cref="Histogram3C"/> class.
	/// </summary>
	~HistogramIpp()
	{
		freeAllMemory();
	}

	/// <summary>
	/// Computes the specified image from an IppArray image
	/// </summary>
	/// <param name="image">The opencv image.</param>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	vector<vector<Ipp32u>> compute(const Ipp8uArray  &image);
	vector<vector<Ipp32u>> compute(const Ipp32fArray &image);
	vector<vector<Ipp32u>> compute(const Ipp16uArray &image);
	vector<vector<Ipp32u>> compute(const Ipp32sArray &image);
   Ipp32sArray computeF(const Ipp32fArray & image);
	vector<vector<Ipp32f>> computeProbability(const Ipp32fArray  &image);

	vector<Ipp32u> counts(const vector<vector<Ipp32u>> &histograms);

	// This really should be in the datatype but not sure its worth it
	int getChannels() const { return _channels; }
	IppDataType getDataType() const { return _dataType; }
	int getBins() const { return _bins; }
	IppiSize getSize() const { return _size; }
	float getDataMin() const { return _dataMin; }
	float getDataMax() const { return _dataMax; }

	bool isSameAs(const IppiSize &size, int channels, IppDataType dataType, int bins, float dataMin, float dataMax) const;
	vector<float> getLevels() const;


protected:
	virtual void compute_8u(const Ipp8uArray  &image, Ipp32u *histogramData[]);
	virtual void compute_16u(const Ipp16uArray  &image, Ipp32u *histogramData[]);
	virtual void compute_32f(const Ipp32fArray  &image, Ipp32u *histogramData[]);
	//virtual void compute_16s(const Ipp32fArray  &image, Ipp32u *histogramData[]);

private:
	IppiSize _size = { 0, 0 };
	int _bins = 0;
	IppDataType _dataType;
	int _channels;
	float _dataMin = 0;
	float _dataMax = 0;
//	std::mutex _lockMutex;

	void initialize();
	void findActualMinMax(float &dmin, float &dmax, IppDataType depth) const;

	void freeAllMemory();

	Ipp32f *_lowerLevel = nullptr;
	Ipp32f *_upperLevel = nullptr;
	int *_levels = nullptr;
	IppiHistogramSpec *_histogramObject = nullptr;
	Ipp8u* _opaqueBuffer = nullptr;

	typedef Ipp32u * Ipp32uP;
	Ipp32uP *_histogramData = nullptr;
};
#endif