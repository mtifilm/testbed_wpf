//---------------------------------------------------------------------------

#ifndef IppExceptionH
#define IppExceptionH

#include "IppArrayLibInt.h"

// TO DO, deal with this better
#ifdef __BORLANDC__
#include "IniFile.h"
#else
// This fakes some IniFile types
#include "MtiCore.h"
#endif

#include "ippcore.h"
#include <string>
#include <vector>
using std::string;
using std::vector;

#pragma warning( push )  
#pragma warning( disable : 4275 )  

class MTI_IPPARRAY_API IppException : public std::exception
{
public:
	IppException(IppStatus status);
    virtual ~IppException();
	std::string GetErrorMessage();
	virtual const char *what() const override;

private:
	IppStatus _status;
};


#define IppThrowOnError(A)\
{\
	const IppStatus _status_ = A;\
	if (_status_ != ippStsNoErr)\
	{\
		throw IppException(_status_);\
	}\
}

#define IppThrowOnMemoryError(A)\
{\
	if ((A) == nullptr)\
	{\
		throw IppException(ippStsNoMemErr);\
	}\
}

#pragma warning( pop ) 

#endif
