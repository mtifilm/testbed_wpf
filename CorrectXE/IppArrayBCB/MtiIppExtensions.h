#ifndef MtiIppExtensionsH
#define MtiIppExtensionsH

#include <iostream>
#include <fstream>
#include "ippi.h"
#include <vector>
#include <tuple>

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include "IppArrayLibInt.h"

#ifdef __BORLANDC__
#include "IniFile.h"
#else

// This fakes some IniFile types
#include "MtiCore.h"
#define my_lround lround
#endif

struct MtiPoint
{
	int x = 0;
	int y = 0;
	int z = 0;

	MtiPoint() = default;
	MtiPoint(const IppiPoint &p) : x(p.x), y(p.y) {}
	MtiPoint(int xn, int yn, int zn = 0) : x(xn), y(yn), z(zn) {}

	// Make an implicit type conversion so native Ipp calls are 
	// can be called easily
	operator IppiPoint() const
	{
		return { x, y };
	}

	MtiPoint operator +(const IppiPoint &rhs) const
	{
		return { x + rhs.x, y + rhs.y};
	}

	// Unary - operator
	MtiPoint operator -() const
	{
		return { -x, -y };
	}

	MtiPoint operator -(const IppiPoint &rhs) const
	{
		return { x - rhs.x, y - rhs.y };
	}

	MtiPoint operator +(const MtiPoint &rhs) const
	{
		return { x + rhs.x, y + rhs.y, z - rhs.z };
	}

	MtiPoint operator -(const MtiPoint &rhs) const
	{
		return { x - rhs.x, y - rhs.y, z - rhs.z };
	}

	MtiPoint &operator +=(const MtiPoint &rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}

	MtiPoint &operator -=(const MtiPoint &rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;

		return *this;
	}

	MtiPoint &operator =(const MtiPoint &rhs)
	{
		if (this != &rhs)
		{
			x = rhs.x;
			y = rhs.y;
			z = rhs.z;
		}

		return *this;

	}

	bool operator == (const MtiPoint &rhs) const
	{
		return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
	}

	bool operator != (const MtiPoint &rhs) const
	{
		return !(*this == rhs);
	}

	//	friend std::ostream& operator<<(std::ostream& os, const MtiPoint &rhs)
	//	{
	//		os << "(x=" << rhs.x << ", y=" << rhs.y << ")";
	//		return os;
	//	}
};

struct MtiSize
{
	int width = 0;
	int height = 0;
	int depth = 0;

	MtiSize() = default;

	MtiSize(const IppiSize &rhs, int channels=1) : MtiSize(rhs.width, rhs.height, channels) {}
	MtiSize(const int widthN, const int heightN = 1, const int depthN = 1) : width(widthN), height(heightN), depth(depthN) {}
/// 10.2 BORLAND messes this up
/// 	MtiSize(const size_t widthN, const size_t heightN = 1, const size_t depthN = 1) : width(int(widthN)), height(int(heightN)), depth(int(depthN)) {}

	operator IppiSize() const
	{
		return { width, height };
	}

	MtiSize operator +(const IppiSize &rhs) const
	{
		return { width + rhs.width, height + rhs.height };
	}

	MtiSize operator *(const double scale) const
	{
		return { my_lround(width *scale), my_lround(height * scale) };
	}

	MtiSize operator -(const IppiSize &rhs) const
	{
		return { width - rhs.width, height - rhs.height };
	}

	MtiSize &operator +=(const IppiSize &rhs)
	{
		width += rhs.width;
		height += rhs.height;

		return *this;
	}

	MtiSize &operator -=(const IppiSize &rhs)
	{
		width -= rhs.width;
		height -= rhs.height;

		return *this;
	}

	MtiSize &operator =(const MtiSize &rhs)
	{
		if (this != &rhs)
		{
			width = rhs.width;
			height = rhs.height;
			depth = rhs.depth;
		}

		return *this;
	}

	bool operator == (const MtiSize &rhs) const
	{
		return (width == rhs.width) && (height == rhs.height) && (depth == rhs.depth);
	}

	bool operator != (const MtiSize &rhs) const
	{
		return !(*this == rhs);
	}

	int area() const
	{
		return width * height;
	}

	int volume() const
	{
		return width * height * depth;
	}

	// Name comes from Matlab Index To Subscripts
	MtiPoint ind2sub(int index) const
	{
      auto rowComponents = width * depth;
      return { (index % rowComponents) / depth, index / rowComponents, index % depth };
	}

   std::vector<MtiPoint> ind2sub(const std::vector<int> &indices) const
   {
      std::vector<MtiPoint> result;
	   result.reserve(indices.size());
      for (auto index : indices)
      {
         result.push_back(ind2sub(index));
      }

      return result;
   }

	int sub2ind(const MtiPoint &p) const
	{
		return p.y * width * depth + p.x* depth + p.z;
	}

	//	friend std::ostream& operator<<(std::ostream& os, const MtiSize &rhs)
	//	{
	//		os << "(w=" << rhs.width << ", h=" << rhs.height << ")";
	//		return os;
	//	}
};

struct FactorRects;

struct MtiRect
{
	int x = 0;
	int y = 0;
	int width = 0;
	int height = 0;

	// These are data tags, usually for tiling an image.
	// For example, one an MtiRect in Array2D returns the
	// (row, col) that is the position into the Array.
	// tags are ignored for any operation that creates a new MtiRect
	int linearIndex = -1;
	int containerWidth = 1;

	MtiRect() = default;
	MtiRect(int xn, int yn, int widthN, int heightN, int index = -1, int tagWidth = 1) :
		 x(xn), y(yn), width(widthN), height(heightN), linearIndex(index), containerWidth(tagWidth)
	{}

	MtiRect(IppiPoint p, IppiSize s) : MtiRect(p.x, p.y, s.width, s.height) {}
	MtiRect(const IppiRect &r) : MtiRect(r.x, r.y, r.width, r.height) {}

	operator IppiRect() const
	{
		return { x, y, width, height };
	}

	int operator[](const int &i) const
	{
		switch(i)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return width;
		case 3:
			return height;

		default:
			std::runtime_error("Bad index in MtiRect[]");
		}
		
		return 0;
	}
	
	MtiPoint tl() const { return { x, y }; }
	MtiPoint tr() const { return { x + width, y }; }
	MtiPoint bl() const { return { x, y + height }; }
	MtiPoint br() const { return { x + width, y + height }; }

	explicit operator MtiSize() const
	{
		return { width, height, 1 };
	}

	explicit operator MtiPoint() const
	{
		return { x, y };
	}

	MtiSize getSize() const
	{
		return { width, height, 1 };
	}

	// Blow up all four sides by i
	MtiRect inflate(int i) const
	{
		return inflate(i, i, i, i);
	}

	// Blow up the left and right by 1, top and bottom by t
	MtiRect inflate(int l, int t) const
	{
		return inflate(l, t, l, t);
	}

	// Blow up the left and right a size
	MtiRect inflate(const MtiSize &size) const
	{
		return inflate(size.width, size.height, size.width, size.height);
	}

	// Add ul to left, ut to top, lr to right, and br to bottom
	MtiRect inflate(int ul, int ut, int lr, int lb) const
	{
		MtiRect result = *this;
		result.x -= ul;
		result.y -= ut;
		result.width += ul + lr;
		result.height += ut + lb;

		return result;
	}

	MtiRect &operator +=(const IppiPoint &p)
	{
		x += p.x;
		y += p.y;
		return *this;
	}

	MtiRect &operator -=(const IppiPoint &p)
	{
		x -= p.x;
		y -= p.y;
		return *this;
	}

	MtiRect operator +(const IppiPoint &p) const
	{
		return { x + p.x, y + p.y, width, height };
	}

	MtiRect operator -(const IppiPoint &p) const
	{
		return { x - p.x, y - p.y, width, height };
	}

	MtiRect &operator -=(const IppiRect &rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		width -= rhs.width;
		height -= rhs.height;

		return *this;
	}

	MtiRect &operator =(const MtiRect &rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		x = rhs.x;
		y = rhs.y;
		width = rhs.width;
		height = rhs.height;

		linearIndex = rhs.linearIndex;
		containerWidth = rhs.containerWidth;
		return *this;
	}

  	FactorRects operator %(const MtiRect &probe) const;

	bool isEmpty() const
	{
		return (width <= 0) || (height <= 0);
	}

	bool contains(const MtiRect &probe) const
	{
		return probe == ((*this) & probe);
	}

	bool operator==(const IppiRect &rhs) const
	{
		if (isEmpty() && ((rhs.width <= 0) || (rhs.height <= 0)))
		{
			return true;
		}
			
		return (x == rhs.x) &&
			(y == rhs.y) &&
			(width == rhs.width) &&
			(height == rhs.height);
	}

	bool operator!=(const IppiRect &rhs) const
	{
		return !(*this == rhs);
	}

	MtiRect operator&(const IppiRect &rhs) const
	{
		const auto xr = std::max<int>(x, rhs.x);
		const auto yt = std::max<int>(y, rhs.y);
		const auto xl = std::min<int>(x + width, rhs.x + rhs.width);
		const auto yb = std::min<int>(y + height, rhs.y + rhs.height);

		const auto w0 = xl - xr;
		const auto h0 = yb - yt;

		if ((w0 < 0) || (h0 < 0))
		{
			return { 0, 0, 0 ,0 };
		}

		return { xr, yt, w0, h0, linearIndex, containerWidth };
	}

	MtiRect operator&=(const IppiRect &rhs)
	{
		*this = *this & rhs;
		return *this;
	}

	int area() const
	{
		return width * height;
	}

	// This give a point in the ROI for linear indexing
	// i.e.; the index sweeps along a row and jumps to the
	// next row on increment.
	// THIS CANNOT BE USED AS AN OFFSET OF A ROI INTO AN IppArray
	// Name comes from Matlab Index To Subscripts
	MtiPoint ind2sub(int index) const
	{
		return getSize().ind2sub(index);
	}

	int sub2ind(const IppiPoint &p) const
	{
		return getSize().sub2ind(p);
	}

	bool contains(const IppiPoint &point) const
	{
		return (point.x >= x) &&
			(point.x < (x + width)) &&
			(point.y >= y) &&
			(point.y < (y + height));
	}

	friend std::ostream& operator<<(std::ostream& os, const MtiRect &rhs)
	{
		os << "(x=" << rhs.x << ", y=" << rhs.y << ", ";
		os << "w=" << rhs.width << ", h=" << rhs.height << ")";
		return os;
	}

	int row() const { return linearIndex / containerWidth; }
	int col() const { return linearIndex % containerWidth; }
	static MtiRect createFromCenterBounded(const IppiPoint_32f center, const MtiSize &boxSize, const MtiSize &boxConstraints)
	{
		auto x = std::max<int>(int(center.x + 0.5f - boxSize.width) , 0);
		auto y = std::max<int>(int(center.y + 0.5f - boxSize.height), 0);
		auto width = 2 * boxSize.width + 1;
		auto height = 2 * boxSize.height + 1;

		x += std::max<int>((x + width) - boxConstraints.width, 0);
		y += std::max<int>((y + height) - boxConstraints.height, 0);

		return { x, y, width, height };
	}

	static MtiRect createFromCenter(const IppiPoint_32f center, const MtiSize &boxSize)
	{
		auto x = int(center.x + 0.5f - boxSize.width);
		auto y = int(center.y + 0.5f - boxSize.height);
		auto width = 2 * boxSize.width + 1;
		auto height = 2 * boxSize.height + 1;
		return { x, y, width, height };
	}
};

// This struct factors a rectangle and probe down into 14 rectangles
// A center(probe) rectangle and the 4 that surrounds the center
//      -------------------------
//      |         Top        r  |
//      | L  ------------    i  |
//      | e  |  Center  |    g  |
//      | f  ------------    h  |
//      | f     Bottom       t  |
//      -------------------------
//
//  These can overlap, can be empty and not same size
//
//  The rectangles are then broken into 8 more that
//  do not overlap
//
//      -------------------------
//      | tl |    tc    |   tr  |
//      |-----------------------|
//      | cl |  Center  |   cr  |
//      |-----------------------|
//      | bl |    bc    |   br  |
//      -------------------------
//
//  Note: The probe ROI is relative to the fullRoi. (E.g. tl is always x=0, y=0
//  the factor rects are relative to the fullRoi also.
//  This is consistent with the fullRoi being the data of an IppArray
//  
//
struct FactorRects
{
	// Warning do not change this order
	MtiRect full;

	// These overlap 
	MtiRect top;
	MtiRect left;
	MtiRect right;
	MtiRect bottom;

	// This is the center array surrounded by factors arrays
	// Disjointed
	MtiRect tl;
	MtiRect tc;
	MtiRect tr;
	MtiRect cl;
	MtiRect center;
	MtiRect cr;
	MtiRect bl;
	MtiRect bc;
	MtiRect br;

	FactorRects() = default;

	/**
	 * \brief 
	 * \param fullRoi is the large ROI to factor
	 * \param probeRoi is the smaller ROI to factor around
	 * \param relativeProbe true means the (0,0) point in probeRoi corresponds to (fullRoi.x, fullRoi.y) and the tl factor's start is at (0,0)
	 */
	FactorRects(const MtiRect &fullRoi, const MtiRect &probeRoi, bool relativeProbe = false)
	{
		// This is a kludge because computePaddingRects was written relative and zero
		if (relativeProbe)
		{
			computePaddingRects(fullRoi, probeRoi);
			return;
		}

		const auto fullRoiTopLeft = fullRoi.tl();
		auto fullRoiZero = fullRoi - fullRoiTopLeft;
		auto relativeRoi = probeRoi - fullRoiTopLeft;
		computePaddingRects(fullRoiZero, relativeRoi);
		bias(fullRoiTopLeft);
	}

	void computePaddingRects(const MtiRect &fullRoi, const MtiRect &probRoi)
	{
		// bias to zero
		full = fullRoi;
		full.x = 0;
		full.y = 0;

		center = fullRoi & probRoi;
		// center -= fullRoi.tl();

		// Fast out
		if (full == center)
		{
			return;
		}

		left = { 0, 0, center.x, full.height };
		right = { center.x + center.width, 0, full.width - center.x - center.width, full.height };
		top = { 0, 0, full.width, center.y };
		bottom = { 0, center.y + center.height, full.width, full.height - center.y - center.height };

		 tl = left & top;
		 tr = right & top;
		 bl = left & bottom;
		 br = right & bottom;

		tc = { tl.width, 0, center.width, top.height };
		bc = { tl.width, bottom.y, center.width, bottom.height };
		cl = { 0, top.height, left.width, center.height };
		cr = { right.x, top.height, right.width, center.height };
	}

	// This is really only used in debugging, 
	// The danger is if the internals change
	FactorRects(const std::vector<MtiRect> &initArray)
	{
		assert(initArray.size() == 14);
		full = initArray[0];
		top = initArray[1];
		left = initArray[2];
		right = initArray[3];
		bottom = initArray[4];
		tl = initArray[5];
		tc = initArray[6];
		tr = initArray[7];
		cl = initArray[8];
		center = initArray[9];
		cr = initArray[10];
		bl = initArray[11];
		bc = initArray[12];
		br = initArray[13];
	}

	FactorRects zeroBias() const
	{
		if (MtiPoint(0,0) == tl.tl())
		{
			return *this;
		}

		FactorRects result = *this;
		result.bias({ -tl.x, -tl.y });
		return result;
	}

	bool operator == (const FactorRects &rhs) const
	{
		// Because overriding == in MtiRect doesn't do first out, do it ugly
		if (full != rhs.full) return false;
		if (center != rhs.center) return false;
		if (top != rhs.top) return false;
		if (bottom != rhs.bottom) return false;
		if (left != rhs.left) return false;
		if (right != rhs.right) return false;
		if (tl != rhs.tl) return false;
		if (tc != rhs.tc) return false;
		if (tr != rhs.tr) return false;
		if (cl != rhs.cl) return false;
		if (cr != rhs.cr) return false;
		if (bl != rhs.bl) return false;
		if (bc != rhs.bc) return false;
		if (br != rhs.br) return false;

		return true;
	}

	bool operator != (const FactorRects &rhs) const
	{
		return !(*this == rhs);
	}

	bool isFactoringNeeded() const
	{
		return full != center;
	}

private:

	void bias(const MtiPoint &p)
	{
		full += p;
		center += p;
		top += p;
		bottom += p;
		left += p;
		right += p;
		tl += p;
		tc += p;
		tr += p;
		cl += p;
		cr += p;
		bl += p;
		bc += p;
		br += p;
	}

};

namespace
{
	std::ostream& operator<<(std::ostream& os, const IppiPoint &rhs)
	{
		os << "(x=" << rhs.x << ", y=" << rhs.y << ")";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, const IppiSize &rhs)
	{
		os << "(w=" << rhs.width << ", h=" << rhs.height << ")";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, const IppiRect &rhs)
	{
		os << "(x=" << rhs.x << ", y=" << rhs.y << ", ";
		os << "w=" << rhs.width << ", h=" << rhs.height << ")";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, const MtiSize &rhs)
	{
		os << "(w=" << rhs.width << ", h=" << rhs.height << ", ";
		os << "d=" << rhs.depth << ")";
		return os;
	}

#ifdef __BORLANDC__
	MTIostringstream& operator<<(MTIostringstream& os, const IppiPoint &rhs)
	{
		os << "(x=" << rhs.x << ", y=" << rhs.y << ")";
		return os;
	}

	MTIostringstream& operator<<(MTIostringstream& os, const IppiSize &rhs)
	{
		os << "(w=" << rhs.width << ", h=" << rhs.height << ")";
		return os;
	}

	MTIostringstream& operator<<(MTIostringstream& os, const IppiRect &rhs)
	{
		os << "(x=" << rhs.x << ", y=" << rhs.y << ", ";
		os << "w=" << rhs.width << ", h=" << rhs.height << ")";
		return os;
	}

	MTIostringstream& operator<<(MTIostringstream& os, const MtiSize &rhs)
	{
		os << "(w=" << rhs.width << ", h=" << rhs.height << ", ";
		os << "d=" << rhs.depth << ")";
		return os;
	}
#endif
}


// Need some better place for this
namespace
{
	// return the point relative to the ROI
	static IppiPoint ind2sub(int width, int index)
	{
		return { index % width, index / width };
	}

	static int sub2ind(int width, const IppiPoint &p)
	{
		return p.y * width + p.x;
	}

	static IppiPoint ind2sub(const IppiSize &size, int index)
	{
		return ind2sub(size.width, index);
	}

	static int sub2ind(const IppiSize &size, const IppiPoint &p)
	{
		return sub2ind(size.width, p);
	}

#ifdef __BORLANDC__
	string dumpRect(const RECT &rect)
	{
		std::ostringstream os;
		os << "left=" << rect.left;
		os << ", top=" << rect.top;
		os << ", bottom=" << rect.bottom;
		os << ", right=" << rect.right;

		return os.str();
	}
#endif


	template<typename T> std::string dumpVector(const std::vector<T> &vect)
	{
		std::ostringstream os;
		int i = 0;
		os << std::endl;
		for (auto &v : vect)
		{
			os << "[" << i++ << "]=" << v << std::endl;
		}

		os << std::endl;
		return os.str();
	}

	std::string dumpVector(const std::vector<IppiPoint> &vect)
	{
		std::ostringstream os;
		int i = 0;
		os << std::endl;
		for (auto &v : vect)
		{
			os << "[" << i++ << "]=" << "{" << v.x << ", " << v.y << "};" << std::endl;
		}

		os << std::endl;
		return os.str();
	}

	template<typename T>
	std::string dumpArray(const T *a, int rows, int cols)
	{
		std::ostringstream os;
		os << std::endl;
		for (auto r = 0; r < rows; r++)
		{
			auto p = a + r * cols;
			for (auto c = 0; c < cols; c++)
			{
				os << "[" << r << "," << c << "]=" << *p++ << "\t";
			}

			os << std::endl;
		}

		os << std::endl;
		return os.str();
	}

	template<typename T>
	std::string dumpArray(const T *a, int size)
	{
		std::ostringstream os;
		os << std::endl;
		auto p = a;
		for (auto r = 0; r < size; r++)
		{
			os << "[" << r << "]=" << *p++ << std::endl;
		}

		os << std::endl;
		return os.str();
	}

	template<typename T>
	void dumpFpImage(const T &array, int channel, int index, const std::string filePath)
	{
		std::ostringstream os;
		os << filePath << "\\Image" << std::setfill('0') << std::setw(3) << index << ".raw";
		std::ofstream outFile(os.str());
		auto depth = array.getComponents();

		auto roi = array.getRoi();
		outFile << array.getRoi().height << ", " << array.getRoi().width << ", rows, cols" << std::endl;
		for (auto r = 0; r < array.getRoi().height; r++)
		{
			for (auto c = 0; c < array.getRoi().width; c++)
			{
				outFile << *(array.getRowPointer(r + depth * array.getRoi().x, c + array.getRoi().y) + channel)
					<< " ";
			}

			outFile << std::endl;
		}
	}

	template<typename T>
	std::string dumpFirstDifference(T &lhs, T &rhs, int reportNumber = 1)
	{
		std::ostringstream os;

		if (lhs.getPlaneSize() != rhs.getPlaneSize())
		{
			os << "Size differs: 1st= " << lhs.getPlaneSize() << ", 2an= " << rhs.getPlaneSize();
			return  os.str();
		}

		int index = 0;
		auto rhsIt = rhs.begin();
		for (auto fl : lhs)
		{
			auto fr = *rhsIt++;
			if (fl != fr)
			{
				os << lhs.getRoi().ind2sub(index) << ": diff values = (1st= " << fl << ", " << "2an= " << fr << ")";
				if (--reportNumber == 0)
				{
					return os.str();
				}

				os << std::endl;
			}

			index++;
		}

		return "";
	}

	static IppiRect convertToIppiRect(const RECT &rect)
	{
		IppiRect result =
		{ rect.left, rect.top,  rect.right - rect.left, rect.bottom - rect.top };
		return result;
	}

	static RECT convertToRect(const IppiRect &rect)
	{
		RECT result =
		{ rect.x, rect.y, rect.x + rect.width, rect.y + rect.height };
		return result;
	}

	std::pair<MtiRect, MtiRect> expandTo50Percent(const MtiRect &targetRoi, const MtiRect &probeRoi)
	{
		const auto intersectRoi = targetRoi & probeRoi;

		// Assume probe is correct
		MtiRect result = probeRoi;
		MtiRect validProbe = { {0,0}, probeRoi.getSize() };

		// Compute new "x" parameters
		if (probeRoi.x < targetRoi.x)
		{
			const auto d = probeRoi.width - intersectRoi.width;
			if (d > intersectRoi.width)
			{
				result.width = 2 * d;
			}
		}
		else if (probeRoi.x > targetRoi.x)
		{
			const auto d = probeRoi.width - intersectRoi.width;
			if (d > intersectRoi.width)
			{
				result.width = 2 * d;
				result.x = probeRoi.x - d + intersectRoi.width;
				validProbe.x = d - intersectRoi.width;
			}
		}

		// Compute new "y" parameters
		if (probeRoi.y < targetRoi.y)
		{
			const auto d = probeRoi.height - intersectRoi.height;
			if (d > intersectRoi.height)
			{
				result.height = 2 * d;
			}
		}
		else if (probeRoi.y > targetRoi.y)
		{
			const auto d = probeRoi.height - intersectRoi.height;
			if (d > intersectRoi.height)
			{
				result.height = 2 * d;
				result.y = probeRoi.y - d + intersectRoi.height;
				validProbe.y = d - intersectRoi.height;
			}
		}

		return { result, validProbe };
	}
}

inline FactorRects MtiRect::operator %(const MtiRect &probe) const
{
	FactorRects result(*this, probe);
	return result;
}

// Not really an extension, but we don't have places to put functions like these
template<typename T, typename S>
void SortAbyB(T &a0, T &a1, S &b0, S &b1)
{
	auto d = a0.getComponents();
	for (auto r = 0; r < b0.getHeight(); r++)
	{
		auto bp0 = b0.getRowPointer(r);
		auto bp1 = b1.getRowPointer(r);
		auto ap0 = a0.getRowPointer(r);
		auto ap1 = a1.getRowPointer(r);

		for (auto c = 0; c < b0.getWidth(); c++)
		{
			if (*bp0 > *bp1)
			{
				auto tmpb = *bp0;
				*bp0 = *bp1;
				*bp1 = tmpb;

				auto tmpa = *ap0;
				*ap0 = *ap1;
				*ap1 = tmpa;

				if (d >= 2)
				{
					auto tmpa1 = ap0[1];
					ap0[1] = ap1[1];
					ap1[1] = tmpa1;
				}

				if (d == 3)
				{
					auto tmpa2 = ap0[2];
					ap0[2] = ap1[2];
					ap1[2] = tmpa2;
				}
			}

			++bp0;
			++bp1;

			ap0 += d;
			ap1 += d;
		}
	}
}

// This doesn't appear faster than above
template<typename T, typename S>
void SortAbyB3(T &a0, T &a1, S &b0, S &b1)
{
	MTIassert(a0.getComponents() == 3);

	for (auto r = 0; r < b0.getHeight(); r++)
	{
		auto bp0 = b0.getRowPointer(r);
		auto bp1 = b1.getRowPointer(r);
		auto ap0 = a0.getRowPointer(r);
		auto ap1 = a1.getRowPointer(r);

		for (auto c = 0; c < b0.getWidth(); c++)
		{
			if (*bp0 > *bp1)
			{
				std::swap(*bp0, *bp1);
				std::swap(ap0[0], ap1[0]);
				std::swap(ap0[1], ap1[1]);
				std::swap(ap0[2], ap1[2]);
			}

			++bp0;
			++bp1;

			ap0 += 3;
			ap1 += 3;
		}
	}
}

template<typename T, typename S>
//void bubbleSortAbyB(std::vector<T> &a, std::vector<S> &b)
void bubbleSortAbyB(T a[], S b[], int n)
{
	//auto n = int(b.size());
	for (auto i = 0; i < n - 1; i++)
	{
		for (auto j = 0; j < n - i - 1; j++)
		{
			SortAbyB3(a[j], a[j + 1], b[j], b[j + 1]);
		}
	}
}

template<typename T, typename S>
void bubbleSortAbyB(std::vector<T> &a, std::vector<S> &b)
{
	auto n = int(b.size());
	for (auto i = 0; i < n - 1; i++)
	{
		for (auto j = 0; j < n - i - 1; j++)
		{
			SortAbyB3(a[j], a[j + 1], b[j], b[j + 1]);
		}
	}
}

template<typename S>
void SortB(S &b0, S &b1)
{
	MTIassert(b0.getComponents() == 1);
	MTIassert(b1.getComponents() == 1);

	for (auto r = 0; r < b0.getHeight(); r++)
	{
		auto bp0 = b0.getRowPointer(r);
		auto bp1 = b1.getRowPointer(r);
		for (auto c = 0; c < b0.getWidth(); c++)
		{
			if (*bp0 > *bp1)
			{
				std::swap(*bp0, *bp1);
			}

			++bp0;
			++bp1;;
		}
	}
}

template<typename S>
void bubbleSortB(S b[], int n)
{
	//auto n = int(b.size());
	for (auto i = 0; i < n - 1; i++)
	{
		for (auto j = 0; j < n - i - 1; j++)
		{
			SortB(b[j], b[j + 1]);
		}
	}
}

template<typename S>
void bubbleSortB(std::vector<S> b)
{
	auto n = int(b.size());
	for (auto i = 0; i < n - 1; i++)
	{
		for (auto j = 0; j < n - i - 1; j++)
		{
			SortB(b[j], b[j + 1]);
		}
	}
}

// just for speed sake
inline void bubbleSortB(std::vector<float> &b)
{
	auto n = int(b.size());
	for (auto i = 0; i < n - 1; i++)
	{
		for (auto j = 0; j < n - i - 1; j++)
		{
			if (b[j] > b[j+1])
			{
				std::swap(b[j], b[j + 1]);
			}
		}
	}
}
#endif
