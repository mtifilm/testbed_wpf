#include "IppArray.h"
#include "Ippheaders.h"
#include <cassert>
#include <exception>
#include "HistogramIpp.h"
#include "IppAgnostic.h"
#include <numeric>
#include <sstream>
#include <random>
#include <algorithm>
#include "MtiAncillaryTemplates.h"

namespace
{
	std::default_random_engine generator;
	// Pseudo-IPP call!
	IppStatus IppResize_C1R_or_C3R(Ipp8u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp8u* pDst, IppiSize dstSize,
		Ipp32s dstStep, Ipp32u numChannels);
}

void Ipp8uArray::normalDistribution(double mean, double stddev)
{
	//	THROW_MTI_RUNTIME_ERROR("Not valid for arguments")
	std::normal_distribution<float> distribution((float)mean, (float)stddev);

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto c = 0; c < getComponents(); c++)
			{
				*inp++ = (Ipp8u)std::round(distribution(generator));
			}
		}
	}
}

void Ipp8uArray::uniformDistribution(Ipp8u low, Ipp8u high)
{
	std::uniform_int_distribution<Ipp16u> distribution(low, high);
	generator.seed(static_cast<unsigned int>(time(nullptr)));

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto d = 0; d < getComponents(); d++)
			{
				*inp++ = Ipp8u(distribution(generator));
			}
		}
	}
}

IPP_ARRAY_APPLY_VALUE(Ipp8uArray, set, Ipp8u, ippiSet_8u)

vector<Ipp64f> Ipp8uArray::sum() const
{
	auto size = getSize();
	vector<Ipp64f> result(getComponents());
	auto r = (Ipp64f *)result.data();
	switch (getComponents())
	{
	case 1:

		ippiSum_8u_C1R(data(), getRowPitchInBytes(), size, r);
		break;

	case 3:
		ippiSum_8u_C3R(data(), getRowPitchInBytes(), size, r);
		break;
	}

	return result;
}

void Ipp8uArray::copyFromPod(Ipp8u *source, int size)
{
	const auto roiSize = getSize();
	const auto roi = getRoi();

	if ((size / getComponents()) != (roi.height * roi.width))
	{
		THROW_MTI_RUNTIME_ERROR(string("Size, roi area mismatch in ") + __func__);
	}

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp8u));

	switch (getComponents())
	{
	case 1:
		ippiCopy_8u_C1R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;

	case 3:
		ippiCopy_8u_C3R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;
	}
}

// Select operator for different types of copy
Ipp8uArray Ipp8uArray::operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData) const
{
	return SelectFromFunction(*this, probeRoi, copyType, utilizeExtrinsicData);
}

void Ipp8uArray::Y2RGB(const Ipp8uArray & src)
{
	if (src.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Input to Y2RGB must have only one component!");
	}

	if (getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Target of Y2RGB must have channels 3!");
	}

	auto &roi = getRoi();
	auto &srcRoi = src.getRoi();
	if (roi.width != srcRoi.width || roi.height != srcRoi.height)
	{
		THROW_MTI_RUNTIME_ERROR("Dimension mismatch in input to Y2RGB!");
	}

	copyFromPlanar({ src, src, src });

}

MtiPlanar<Ipp8uArray> Ipp8uArray::copyToPlanar() const
{
	if (getComponents() == 1)
	{
		return { *this };
	}

	// This is the ugliest code I've every written, but dementia does this 
	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	MtiPlanar<Ipp8uArray> result;
	for (auto i : range(getComponents()))
	{
		result.emplace_back(Ipp8uArray({ getSize(), 1 }));
		result.back().setOriginalBits(getOriginalBits());
	}

	if (getComponents() == 3)
	{
		Ipp8u *dstPtrs[3] = { result[0].data(), result[1].data(), result[2].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_8u_C3P3R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	if (getComponents() == 4)
	{
		Ipp8u *dstPtrs[4] = { result[0].data(), result[1].data(), result[2].data(), result[3].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_8u_C4P4R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	THROW_MTI_RUNTIME_ERROR("Number of components must be 1, 3 or 4");
}

void Ipp8uArray::copyFromPlanar(const MtiPlanar<Ipp8uArray> &planes)
{
	auto channels = int(planes.size());
	if ((channels != 1) && (channels != 3) && (channels != 4))
	{
		THROW_MTI_RUNTIME_ERROR("copyFromPlanar needs a 1, 3 or 4 size planar array");
	}

	// See if we are a null array
	this->createStorageIfEmpty(planes[0].getHeight(), planes[0].getWidth(), channels);

	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	// Planar arrays are same size, so just check the first
	if (roiSize.width != planes[0].getRoi().width || roiSize.height != planes[0].getRoi().height)
	{
		THROW_MTI_RUNTIME_ERROR("All operands of CopyFromPlanar must be the same size!");
	}

	setOriginalBits(planes[0].getOriginalBits());
	for (auto i = 1; i < channels; i++)
		if (planes[0].getRowPitchInBytes() != planes[i].getRowPitchInBytes())
		{
			THROW_MTI_RUNTIME_ERROR("All sources of copyFromPlanar must have same row pitch!");
		}

	if (channels == 1)
	{
		*this <<= planes[0];
		return;
	}

	if (channels == 3)
	{
		Ipp8u *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_8u_P3C3R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
		return;
	}

	if (channels == 4)
	{
		Ipp8u *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data(), planes[3].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_8u_P4C4R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
	}
}

//Ipp8uArray Ipp8uArray::operator +(const Ipp8u scalar) const
//{
//	auto size = getSize();
//	Ipp8uArray result(size.height, size.width, getComponents());
//
//	switch (getComponents())
//	{
//	case 1:
//		IppThrowOnError(ippiAddC_8u_C1RSfs(data(), getRowPitchInBytes(), scalar, result.data(), result.getRowPitchInBytes(), size, 0));
//		break;
//
//	case 3:
//	{
//		Ipp8u scalars[3] = { scalar, scalar, scalar };
//		IppThrowOnError(ippiAddC_8u_C3RSfs(data(), getRowPitchInBytes(), scalars, result.data(), result.getRowPitchInBytes(), size, 0));
//		break;
//	}
//
//	default:
//		throwComponentError();
//		break;
//	}
//
//	return result;
//}
Ipp8uArray Ipp8uArray::mirror(IppiAxis flip) const
{
	Ipp8uArray result(getSize());

	const auto roiSize = getSize();
	const auto roi = getRoi();

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp16u));

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMirror_8u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 3:
		IppThrowOnError(ippiMirror_8u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 4:
		IppThrowOnError(ippiMirror_8u_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	default:
		throwComponentError();
	}

	return result;
}

Ipp8uArray Ipp8uArray::createDisk(int radius)
{
	return createStrelDisk<Ipp8uArray>(radius);
}

// Conversion macros
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp8uArray, Ipp8u, ippiCopy_8u)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp8uArray, Ipp16u, ippiConvert_16u8u)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp8uArray, Ipp32s, ippiConvert_32s8u)
IPP_CONVERT_ARRAY_TO_ARRAY_WITH_ROUND(Ipp8uArray, Ipp32f, ippiConvert_32f8u)

// Arithmetic operator macros
IPP_OPERATOR_ARRAY_SATURATE(Ipp8uArray, +, Ipp8u, ippiAdd_8u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp8uArray, -, Ipp8u, ippiSub_8u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp8uArray, *, Ipp8u, ippiMul_8u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp8uArray, / , Ipp8u, ippiDiv_8u)

IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp8uArray, +=, Ipp8u, ippiAdd_8u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp8uArray, -=, Ipp8u, ippiSub_8u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp8uArray, *=, Ipp8u, ippiMul_8u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp8uArray, /=, Ipp8u, ippiDiv_8u)

IPP_OPERATOR_SCALAR_SATURATE(Ipp8uArray, +, Ipp8u, ippiAddC_8u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp8uArray, -, Ipp8u, ippiSubC_8u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp8uArray, *, Ipp8u, ippiMulC_8u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp8uArray, / , Ipp8u, ippiDivC_8u)

IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp8uArray, +=, Ipp8u, ippiAddC_8u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp8uArray, -=, Ipp8u, ippiSubC_8u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp8uArray, *=, Ipp8u, ippiMulC_8u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp8uArray, /=, Ipp8u, ippiDivC_8u)

IPP_ARRAY_INPLACE(Ipp8uArray, minEvery, ippiMinEvery_8u)

vector<Ipp8u> Ipp8uArray::median(const IppiRect &roi) const
{
	vector<Ipp8u> result;
	auto vects = toVector(roi);
	for (auto &vec : vects)
	{
		result.push_back(median(vec));
	}

	return result;
}

vector<Ipp8u> Ipp8uArray::median() const
{
	return median({ 0, 0, getWidth(), getHeight() });
}

Ipp8u Ipp8uArray::medianSort(vector<Ipp8u> &values)
{
	std::sort(values.begin(), values.end());
	auto n = values.size();
	auto m = n / 2;
	if ((n % 2) == 1)
	{
		return values[m];
	}

	return values[m];
}
////xxx
Ipp8u Ipp8uArray::median(vector<Ipp8u> &values)
{
	return medianSort(values);
}

Ipp8u Ipp8uArray::medianQuick(vector<Ipp8u> &values)
{
	int low, high;
	int median;
	int middle, cl, hh;
	Ipp8u *arr = values.data();
	int n = (int)values.size();

	low = 0; high = n - 1; median = (low + high) / 2;
	for (;;)
	{
		if (high <= low) // One element only 
		{
			return arr[median];
		}

		if (high == low + 1)
		{  // Two elements only
			if (arr[low] > arr[high])
				std::swap(arr[low], arr[high]);

			return arr[median];
		}

		// Find median of low, middle and high items; swap into position low 
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])    std::swap(arr[middle], arr[high]);
		if (arr[low] > arr[high])       std::swap(arr[low], arr[high]);
		if (arr[middle] > arr[low])     std::swap(arr[middle], arr[low]);

		// Swap low item (now in position middle) into position (low+1) 
		std::swap(arr[middle], arr[low + 1]);

		// Nibble from each end towards middle, swapping items when stuck 
		cl = low + 1;
		hh = high;
		for (;;)
		{
			do cl++; while (arr[low] > arr[cl]);
			do hh--; while (arr[hh] > arr[low]);

			if (hh < cl)
				break;

			std::swap(arr[cl], arr[hh]);
		}

		// Swap middle item (in position low) back into correct position 
		std::swap(arr[low], arr[hh]);

		// Re-set active partition
		if (hh <= median)
			low = cl;
		if (hh >= median)
			high = hh - 1;
	}
}

//// xxx
vector<Ipp64f> Ipp8uArray::L2Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		ippiNorm_L2_8u_C1R(data(), getRowPitchInBytes(), size, result.data());
		break;

	case 3:
		ippiNorm_L2_8u_C3R(data(), getRowPitchInBytes(), size, result.data());
		break;
	}

	return result;
}

vector<Ipp64f> Ipp8uArray::L1Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNorm_L1_8u_C1R(data(), getRowPitchInBytes(), size, result.data()));
		return result;

	case 3:
		IppThrowOnError(ippiNorm_L1_8u_C3R(data(), getRowPitchInBytes(), size, result.data()));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

void Ipp8uArray::L1NormDiff(const IppArray<Ipp8u> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);
	auto size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNormDiff_L1_8u_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	case 3:
		IppThrowOnError(ippiNormDiff_L1_8u_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return;
}
///xxx
void Ipp8uArray::L2NormDiff(const IppArray<Ipp8u> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);

	IppiSize size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNormDiff_L2_8u_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	case 3:
		IppThrowOnError(ippiNormDiff_L2_8u_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;
	}

	return;
}

Ipp32fArray Ipp8uArray::crossCorrNorm(const IppArray<Ipp8u> &kernalImage, IppiROIShape roiShape) const
{
	throwNotImplemented();
	throw "For Compiler To Compile";
}

Ipp8uArray Ipp8uArray::compareC(Ipp8u c, IppCmpOp cmpOp) const
{
	auto result = Ipp8uArray(getSize());
	auto pSrc = data();
	auto srcStep = getRowPitchInBytes();
	auto pDst = result.data();
	auto dstStep = result.getRowPitchInBytes();
	IppiSize roiSize = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(
			ippiCompareC_8u_C1R(pSrc, srcStep, c, pDst, dstStep, roiSize, cmpOp)
		);
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp8uArray Ipp8uArray::xorC(const Ipp8u value) const
{
	Ipp8uArray result(getSize());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiXorC_8u_C1R(data(), getRowPitchInBytes(), value, result.data(), result.getRowPitchInBytes(), getSize()));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

void Ipp8uArray::maxAndIndex(Ipp8u *maxValues, MtiPoint *indices) const
{
	MtiSize size = getSize();
	int indexX[3];
	int indexY[3];

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMaxIndx_8u_C1R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		break;

	case 3:
		IppThrowOnError(ippiMaxIndx_8u_C3R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		indices[1] = { indexX[1], indexY[1] };
		indices[2] = { indexX[2], indexY[2] };
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}
}

vector<Ipp64f> Ipp8uArray::dotProduct(const IppArray &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiDotProd_8u64f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;

	case 3:
	{
		IppThrowOnError(ippiDotProd_8u64f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;
	}

	default:
		throwComponentError();
	}

	return result;
}


vector<Ipp64f> Ipp8uArray::mean() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:

		ippiMean_8u_C1R(data(), getRowPitchInBytes(), size, result.data());
		break;

	case 3:
		ippiMean_8u_C3R(data(), getRowPitchInBytes(), size, result.data());
		break;

	default:
		throwComponentError();
	}

	return result;
}

Ipp8uArray Ipp8uArray::dilate(const Ipp8uArray& mask, Ipp8uArray& scratchArray) const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 channel arrays can be dilated")
	}

	Ipp8uArray outputArray(getSize());

	// create scratch buffer
	int tempBufferSize = 0;
	int specSize = 0;
	IppThrowOnError(ippiMorphologyBorderGetSize_8u_C1R(getSize(), mask.getSize(), &specSize, &tempBufferSize));

	auto tempSize = std::max<int>(tempBufferSize, specSize);

	// This allocates the scratch buffer, we just need more than min
	if ((scratchArray.getWidth() != tempSize) || (scratchArray.getHeight() != 2))
	{
		Ipp8uArray newScratch({ tempSize, 2 });
		scratchArray = newScratch;
	}

		IppThrowOnError
		(
			ippiMorphologyBorderInit_8u_C1R
			(
				getSize(),
				mask.data(),
				mask.getSize(),
				(IppiMorphState*)scratchArray.getRowPointer(0),
				scratchArray.getRowPointer(1)
			)
		);

	const IppiBorderType borderType = ippBorderRepl;
	IppThrowOnError
	(
		ippiDilateBorder_8u_C1R
		(
			data(),
			getRowPitchInBytes(),
			outputArray.data(),
			outputArray.getRowPitchInBytes(),
			getSize(),
			borderType,
			0,
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		);
	);

	return outputArray;
}

Ipp8uArray Ipp8uArray::erode(const Ipp8uArray& mask, Ipp8uArray& scratchArray) const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 channel arrays can be dilated")
	}

	Ipp8uArray outputArray(getSize());

	// create scratch buffer
	int tempBufferSize = 0;
	int specSize = 0;
	IppThrowOnError(ippiMorphologyBorderGetSize_8u_C1R(getSize(), mask.getSize(), &specSize, &tempBufferSize));

	auto tempSize = std::max<int>(tempBufferSize, specSize);

	// This allocates the scratch buffer, we just need more than min
	if ((scratchArray.getWidth() != tempSize) || (scratchArray.getHeight() != 2))
	{
		Ipp8uArray newScratch({ tempSize, 2 });
		scratchArray = newScratch;
	}
		IppThrowOnError
		(
			ippiMorphologyBorderInit_8u_C1R
			(
				getSize(),
				mask.data(),
				mask.getSize(),
				(IppiMorphState*)scratchArray.getRowPointer(0),
				scratchArray.getRowPointer(1)
			)
		);


	const IppiBorderType borderType = ippBorderRepl;
	IppThrowOnError
	(
		ippiErodeBorder_8u_C1R
		(
			data(),
			getRowPitchInBytes(),
			outputArray.data(),
			outputArray.getRowPitchInBytes(),
			getSize(),
			borderType,
			0,
			(IppiMorphState*)scratchArray.getRowPointer(0),
			scratchArray.getRowPointer(1)
		);
	);

	return outputArray;
}

Ipp8uArray Ipp8uArray::resize(IppiSize newSize) const
{
	if ((getHeight() == 0) || (getWidth() == 0))
	{
		return *this;
	}

	throwOnComponentsSupportedError();

	Ipp8uArray dst({ newSize.width, newSize.height, getComponents() });
	IppiSize srcSize = { getWidth(), getHeight() };

	IppThrowOnError(IppResize_C1R_or_C3R(
		data(),
		srcSize,
		getRowPitchInBytes(),
		dst.data(),
		newSize,
		dst.getRowPitchInBytes(),
		getComponents()));

	return dst;
}

Ipp8uArray Ipp8uArray::transpose() const
{
	auto roi = getRoi();

	// Note: transpose swaps the size
	Ipp8uArray result({ roi.height, roi.width, getComponents() });
	auto roiSize = getSize();
	auto dataPtr = data();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiTranspose_8u_C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 3:\
		IppThrowOnError(ippiTranspose_8u_C3R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize)); \
		break;

	case 4:
		IppThrowOnError(ippiTranspose_8u_C4R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize)); \
			break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp8uArray Ipp8uArray::toGray() const
{
	// Result is one component
	Ipp8uArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 3:
		IppThrowOnError(ippiRGBToGray_8u_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiRGBToGray_8u_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp8uArray Ipp8uArray::toGray(const float weights[]) const
{
	Ipp8uArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 3:
		IppThrowOnError(ippiColorToGray_8u_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	case 4:
		IppThrowOnError(ippiColorToGray_8u_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp32fArray Ipp8uArray::bwdist(Ipp8uArray &scratch) const
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Number of components must be 1");
	}

	int bufferSize = 0;
	IppThrowOnError(ippiTrueDistanceTransformGetBufferSize_8u32f_C1R(getSize(), &bufferSize));
	if (scratch.getWidth() < bufferSize)
	{
		scratch = Ipp8uArray(bufferSize);
	}

	Ipp32fArray result(getSize());
	auto inverted = compareC(0, ippCmpEq);

	IppThrowOnError(ippiTrueDistanceTransform_8u32f_C1R(
		inverted.data(),
		inverted.getRowPitchInBytes(),
		result.data(), 
		result.getRowPitchInBytes(),
		getSize(),
		scratch.data()));

	return result;
}

namespace
{
	IppStatus IppResize_C1R_or_C3R(Ipp8u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp8u* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
	{
		IppiResizeSpec_32f* pSpec = nullptr;
		int specSize = 0, initSize = 0, bufSize = 0;
		Ipp8u* pBuffer = nullptr;
		Ipp8u* pInitBuf = nullptr;
		IppiPoint dstOffset = { 0, 0 };
		IppStatus status = ippStsNoErr;
		IppiBorderType border = ippBorderRepl;

		// ippiResize<Interpolation>Init functions has the following constraints
		// for the minimal size of an input image depending on the chosen
		// interpolation method:
		//    Nearest Neighbor - 1x1
		//    Linear           - 2x2
		//    2-lobed Lanczos  - 4x4
		//    3-lobed Lanczos  - 6x6
		IppiInterpolationType interpolation = IppiInterpolationType::ippLanczos; // ippLanczos;
		Ipp32u numLanczosLobes = 3;
		const int minLinearDim = 2;
		const int minLanczos2Dim = 4;
		const int minLanczos3Dim = 6;
		if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
			|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
		{
			interpolation = ippNearest;
		}
		else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
			|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
		{
			interpolation = ippLinear;
		}
		else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
			|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
		{
			numLanczosLobes = 2;
		}

		// Allocate spec and init buffers.
		status = ippiResizeGetSize_16u(srcSize, dstSize, interpolation, 0, &specSize, &initSize);
		if (status != ippStsNoErr)
		{
			return status;
		}

		pSpec = (IppiResizeSpec_32f*)ippsMalloc_8u(specSize);
		if (pSpec == NULL)
		{
			return ippStsNoMemErr;
		}

		// Initialize the filter.
		switch (interpolation)
		{
		case ippNearest:
			status = ippiResizeNearestInit_8u(srcSize, dstSize, pSpec);
			break;

		case ippLinear:
			status = ippiResizeLinearInit_8u(srcSize, dstSize, pSpec);
			break;

		case ippLanczos:
			pInitBuf = ippsMalloc_8u(initSize);
			if (pInitBuf == nullptr)
			{
				ippsFree(pSpec);
				return ippStsNoMemErr;
			}

			status = ippiResizeLanczosInit_8u(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
			break;

		default:
			break;
		}

		ippsFree(pInitBuf);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			return status;
		}

		// Allocate work buffer.
		status = ippiResizeGetBufferSize_8u(pSpec, dstSize, numChannels, &bufSize);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return status;
		}

		pBuffer = ippsMalloc_8u(bufSize);
		if (pBuffer == nullptr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return ippStsNoMemErr;
		}

		// Resize processing 
		if (numChannels == 1)
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_8u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_8u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
					pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_8u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
					pBuffer);
				break;

			default:
				break;
			}
		}
		else if (numChannels == 3)
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_8u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_8u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
					pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_8u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
				                                  pBuffer);
				break;

			default:
				break;
			}
		}
		else if (numChannels == 4)
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_8u_C4R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_8u_C4R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
					pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_8u_C4R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec,
					pBuffer);
				break;

			default:
				break;
			}
		}
		else
		{
		   THROW_MTI_RUNTIME_ERROR("Invalid number of components");
		}

		ippsFree(pSpec);
		ippsFree(pBuffer);

		IppThrowOnError(status);

		// Always ok
		return status;
	}
}; // end

Ipp8uArray Ipp8uArray::applyBoxFilter(IppiSize boxSize) const
{
   IppStatus ippStatus = ippStsNoErr;
   Ipp8uArray outputArray(getSize());
   IppiSize roiSize = getSize();

   int iTmpBufSize = 0;
   int numChannels = 1;
   ippStatus = ippiFilterBoxBorderGetBufferSize(roiSize, boxSize, ipp8u, numChannels, &iTmpBufSize);
   Ipp8u *pBuffer = ippsMalloc_8u(iTmpBufSize);

   Ipp8u *pSrc = data();
   Ipp8u *pDst = outputArray.data();
   int srcStep = getRowPitchInBytes();
   int dstStep = outputArray.getRowPitchInBytes();
   IppiBorderType borderType = ippBorderRepl;
   ippStatus = ippiFilterBoxBorder_8u_C1R(pSrc, srcStep, pDst, dstStep, roiSize, boxSize, borderType, nullptr, pBuffer);

   ippsFree(pBuffer);

   return outputArray;
}

Ipp8uArray Ipp8uArray::applyBilateralFilter(int radius) const
{
   IppStatus ippStatus = ippStsNoErr;
   Ipp8uArray outputArray(getSize());
   IppiSize roiSize = getSize();

   int numChannels = 1;
   int iSpecSize = 0;
   int iTmpBufSize = 0;
   ippStatus = ippiFilterBilateralBorderGetBufferSize(
                           ippiFilterBilateralGauss,
                           roiSize,
                           radius,
                           ipp8u,
                           numChannels,
                           ippDistNormL1,
                           &iSpecSize,
                           &iTmpBufSize);
   IppiFilterBilateralSpec *pSpec = (IppiFilterBilateralSpec *) ippsMalloc_8u(iSpecSize);
   Ipp8u *pBuffer = ippsMalloc_8u(iTmpBufSize);

   const Ipp32f valsigma = 400.f;
   const Ipp32f possigma = 100.f;
   ippStatus = ippiFilterBilateralBorderInit(
                           ippiFilterBilateralGauss,
                           roiSize,
                           radius,
                           ipp8u,
                           numChannels,
                           ippDistNormL1,
                           valsigma,
                           possigma,
                           pSpec);

   Ipp8u *pSrc = data();
   Ipp8u *pDst = outputArray.data();
   int srcStep = getRowPitchInBytes();
   int dstStep = outputArray.getRowPitchInBytes();
   IppiBorderType borderType = ippBorderRepl;
   ippStatus = ippiFilterBilateralBorder_8u_C1R(
                           pSrc, srcStep,
                           pDst, dstStep,
                           roiSize,
                           borderType, nullptr,
                           pSpec, pBuffer);

   ippsFree(pSpec);
   ippsFree(pBuffer);

   return outputArray;
}

Ipp8uArray Ipp8uArray::applyAutoContrastFilter() const
{
   Ipp8uArray *nonConstThis = const_cast<Ipp8uArray *>(this);
	HistogramIpp histogramGenerator(*nonConstThis, 256);
   auto histogramAllChans = histogramGenerator.compute(*nonConstThis);
   auto histogram = histogramAllChans[0];

   int min = -1;
   int median = -1;
   int max = -1;
   int sum = 0;
   const int numberOfPixelsDividedBy2 = getHeight() * getWidth() / 2;

   for (int i = 0; i < 256; ++i)
   {
      if (histogram[i] && min == -1)
      {
         min = i;
      }

      if (histogram[i])
      {
         max = i;
      }

      sum += histogram[i];
      if (sum >= numberOfPixelsDividedBy2 && median == -1)
      {
         median = i;
      }
   }

   Ipp8uArray out(getSize());
   float stretchLow = (median == min) ? 1.0f : (128.f / (median - min));
   float stretchHigh = (median == min) ? 1.0f : (128.f / (max - median));
   auto inIter = nonConstThis->begin();
   auto outIter = out.begin();
   while (inIter != nonConstThis->end())
   {
      int inVal = *inIter++;
      int outVal;
      outVal = (inVal <= median)
                  ? (int((inVal - min) * stretchLow))
                  : (int(128 + (inVal - median) * stretchHigh));
      outVal = std::min<int>(std::max<int>(outVal, 0), 255);
      *outIter++ = Ipp8u(outVal);
   }

   return out;
}

Ipp8uArray Ipp8uArray::equalizeHistogram() const
{
   const int GrayLevels = 256;
   Ipp8uArray *nonConstThis = const_cast<Ipp8uArray*>(this);
   Ipp32f s[GrayLevels];
   Ipp32s levels[GrayLevels + 1];
   Ipp32s values[GrayLevels + 1];
   auto imgSize = getSize();

   // calculate histogram
   //ippiHistogramEven_8u_C1R(pImage, imgStep, imgSize, histo, levels, GrayLevels + 1, 0, GrayLevels);
	HistogramIpp histogramGenerator(*nonConstThis, GrayLevels);
   auto histogramAllChans = histogramGenerator.compute(*nonConstThis);
   auto histogram = histogramAllChans[0];

   values[0] = 0;
   levels[0] = 0;
   s[0] = float(histogram[0]) / imgSize.width / imgSize.height;
   for (int i = 1; i < GrayLevels; ++i)
   {
      auto histNorm = float(histogram[i]) / imgSize.width / imgSize.height;
      s[i] = histNorm + s[i - 1];
      values[i] = Ipp32s(s[i] * (GrayLevels - 1));
      levels[i] = i;
   }

   values[GrayLevels] = GrayLevels;
   levels[GrayLevels] = GrayLevels;

   Ipp8uArray out(imgSize);  // careful (height, width)

   // LUT
   //ippiLUT_8u_C1IR(pImage, imgStep, imgSize, values, levels, GrayLevels + 1);
   int pSpecSize = 0;
   const Ipp32s *pValues[1];
   pValues[0] = values;
   const Ipp32s *pLevels[1];
   pLevels[0] = levels;
   int pNLevels[1];
   pNLevels[0] = GrayLevels + 1;

   IppThrowOnError(ippiLUT_GetSize(ippLinear, ipp8u, ippC1, imgSize, pNLevels, &pSpecSize));
   IppiLUT_Spec* pSpec = (IppiLUT_Spec*) ippsMalloc_8u(pSpecSize);
   IppThrowOnError(ippiLUT_Init_8u(ippLinear, ippC1, imgSize, pValues, pLevels, pNLevels, pSpec));
   IppThrowOnError(ippiLUT_8u_C1R(data(), getRowPitchInBytes(), out.data(), out.getRowPitchInBytes(), imgSize, pSpec));
   ippsFree(pSpec);

   return out;
}
