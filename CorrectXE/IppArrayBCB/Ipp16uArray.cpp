#include "IppArray.h"
#include "Ippheaders.h"
#include <cassert>
#include <exception>
#include "IppAgnostic.h"
#include <numeric>
#include <sstream>
#include <random>
#include <algorithm>
#include "MtiAncillaryTemplates.h"

namespace
{
	std::default_random_engine generator;

	// Pseudo-IPP call!
	IppStatus IppResize_C1R_or_C3R(Ipp16u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp16u* pDst, IppiSize dstSize,
		Ipp32s dstStep, Ipp32u numChannels);

	IppStatus IppResizeAntiAliasing_C1R_or_C3R(Ipp16u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp16u* pDst, IppiSize dstSize,
		Ipp32s dstStep, Ipp32u numChannels);
}

void Ipp16uArray::normalDistribution(double mean, double stddev)
{
	// This is not a correct distribution because the quantization and cuttoffs
	// but choosing a center at 32768 and SD of 5000 will give distribution that
	// reduces error

	std::normal_distribution<float> distribution((float)mean, (float)stddev);

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto c = 0; c < getComponents(); c++)
			{
				*inp++ = (Ipp16u)std::round(distribution(generator));
			}
		}
	}
}

void Ipp16uArray::uniformDistribution(Ipp16u low, Ipp16u high)
{
	std::uniform_int_distribution<Ipp16u> distribution(low, high);
	generator.seed(static_cast<unsigned int>(time(nullptr)));

	auto rows = getRoi().height;
	auto cols = getRoi().width;

	for (auto r = 0; r < rows; r++)
	{
		auto inp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			for (auto d = 0; d < getComponents(); d++)
			{
				*inp++ = distribution(generator);
			}
		}
	}
}

// Conversion macros
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp16uArray, Ipp8u, ippiConvert_8u16u)
IPP_CONVERT_ARRAY_TO_ARRAY(Ipp16uArray, Ipp16u, ippiCopy_16u)
void Ipp16uArray::convertFrom(const IppArray<Ipp32s> &rhs) { THROW_MTI_RUNTIME_ERROR("Conversion not supported"); }
IPP_CONVERT_ARRAY_TO_ARRAY_WITH_ROUND(Ipp16uArray, Ipp32f, ippiConvert_32f16u)

// Arithmetic operator macros
IPP_OPERATOR_ARRAY_SATURATE(Ipp16uArray, +, Ipp16u, ippiAdd_16u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp16uArray, -, Ipp16u, ippiSub_16u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp16uArray, *, Ipp16u, ippiMul_16u)
IPP_OPERATOR_ARRAY_SATURATE(Ipp16uArray, / , Ipp16u, ippiDiv_16u)

IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp16uArray, +=, Ipp16u, ippiAdd_16u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp16uArray, -=, Ipp16u, ippiSub_16u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp16uArray, *=, Ipp16u, ippiMul_16u)
IPP_OPERATOR_ARRAY_INPLACE_SATURATE(Ipp16uArray, /=, Ipp16u, ippiDiv_16u)

IPP_OPERATOR_SCALAR_SATURATE(Ipp16uArray, +, Ipp16u, ippiAddC_16u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp16uArray, -, Ipp16u, ippiSubC_16u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp16uArray, *, Ipp16u, ippiMulC_16u)
IPP_OPERATOR_SCALAR_SATURATE(Ipp16uArray, / , Ipp16u, ippiDivC_16u)

IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp16uArray, +=, Ipp16u, ippiAddC_16u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp16uArray, -=, Ipp16u, ippiSubC_16u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp16uArray, *=, Ipp16u, ippiMulC_16u)
IPP_OPERATOR_SCALAR_INPLACE_SATURATE(Ipp16uArray, /=, Ipp16u, ippiDivC_16u)

Ipp16uArray Ipp16uArray::mirror(IppiAxis flip) const
{
	Ipp16uArray result(getSize());

	const auto roiSize = getSize();
	const auto roi = getRoi();

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp16u));

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMirror_16u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 3:
		IppThrowOnError(ippiMirror_16u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	case 4:
		IppThrowOnError(ippiMirror_16u_C4R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, flip));
		break;

	default:
		throwComponentError();
	}

	return result;
}

IPP_ARRAY_APPLY_VALUE(Ipp16uArray, set, Ipp16u, ippiSet_16u)
IPP_ARRAY_INPLACE(Ipp16uArray, minEveryInplace, ippiMinEvery_16u)

void Ipp16uArray::absDiff(const Ipp16uArray & input, Ipp16uArray &result)
{
	throwOnArraySizeChannelMismatch(input);

	result.createStorageIfEmpty(getSize());
	throwOnArraySizeChannelMismatch(result);
	auto size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiAbsDiff_16u_C1R(data(), getRowPitchInBytes(), input.data(), input.getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));
		break;

	default:
		throwComponentError();
		break;
	}

	invalidateIfConjoined(*this, result);
}

vector<Ipp64f> Ipp16uArray::sum() const
{
	auto size = getSize();
	vector<Ipp64f> result(getComponents());
	auto r = (Ipp64f *)result.data();
	switch (getComponents())
	{
	case 1:

		ippiSum_16u_C1R(data(), getRowPitchInBytes(), size, r);
		break;

	case 3:
		ippiSum_16u_C3R(data(), getRowPitchInBytes(), size, r);
		break;
	}

	return result;
}

void Ipp16uArray::copyFromPod(Ipp16u *source, int size)
{
	auto roiSize = getSize();
	auto roi = getRoi();

	if ((size / getComponents()) != (roi.height * roi.width))
	{
		THROW_MTI_RUNTIME_ERROR(string("Size, roi area mismathc in ") + __func__);
	}

	auto sourcePitchInBytes = int(roi.width * sizeof(Ipp16u));

	switch (getComponents())
	{
	case 1:
		ippiCopy_16u_C1R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;

	case 3:
		ippiCopy_16u_C3R(source, sourcePitchInBytes, data(), getRowPitchInBytes(), roiSize);
		break;
	}
}

void Ipp16uArray::Y2RGB(const Ipp16uArray & src)
{
	if (src.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Input to Y2RGB must have only one component!");
	}

	if (getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Target of Y2RGB must have channels 3!");
	}

	auto &roi = getRoi();
	auto &srcRoi = src.getRoi();
	if (roi.width != srcRoi.width || roi.height != srcRoi.height)
	{
		THROW_MTI_RUNTIME_ERROR("Dimension mismatch in input to Y2RGB!");
	}

	copyFromPlanar({ src, src, src });

}

MtiPlanar<Ipp16uArray> Ipp16uArray::copyToPlanar() const
{
	if (getComponents() == 1)
	{
		return { *this };
	}

	// This is the ugliest code I've every written, but dementia does this 
	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	MtiPlanar<Ipp16uArray> result;
	for (auto i : range(getComponents()))
	{
		result.emplace_back(Ipp16uArray({ getSize(), 1 }));
      result.back().setOriginalBits(getOriginalBits());
	}

	if (getComponents() == 3)
	{
		Ipp16u *dstPtrs[3] = { result[0].data(), result[1].data(), result[2].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_16u_C3P3R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}

	if (getComponents() == 4)
	{
		Ipp16u *dstPtrs[4] = { result[0].data(), result[1].data(), result[2].data(), result[3].data() };
		auto dstStep = result[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_16u_C4P4R(data(), getRowPitchInBytes(), dstPtrs, dstStep, roiSize));
		return result;
	}


	THROW_MTI_RUNTIME_ERROR("Number of components must be 1, 3 or 4");
}

void Ipp16uArray::copyFromPlanar(const MtiPlanar<Ipp16uArray> &planes)
{
	auto channels = int(planes.size());
	if ((channels != 1) && (channels != 3) && (channels != 4))
	{
		THROW_MTI_RUNTIME_ERROR("copyFromPlanar needs a 1, 3 or 4 size planar array");
	}

	// See if we are a null array
	this->createStorageIfEmpty(planes[0].getHeight(), planes[0].getWidth(), channels);

	auto &roi = getRoi();
	IppiSize roiSize = { roi.width, roi.height };

	// Planar arrays are same size, so just check the first
	if (roiSize.width != planes[0].getRoi().width || roiSize.height != planes[0].getRoi().height)
	{
		THROW_MTI_RUNTIME_ERROR("All operands of CopyFromPlanar must be the same size!");
	}

	setOriginalBits(planes[0].getOriginalBits());

	for (auto i = 1; i < channels; i++)
	if (planes[0].getRowPitchInBytes() != planes[i].getRowPitchInBytes())
	{
		THROW_MTI_RUNTIME_ERROR("All sources of copyFromPlanar must have same row pitch!");
	}

	if (channels == 1)
	{
		*this <<= planes[0];
		return;
	}

	if (channels == 3)
	{
		Ipp16u *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_16u_P3C3R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
		return;
	}

	if (channels == 4)
	{
		Ipp16u *srcPtrs[] = { planes[0].data(), planes[1].data(), planes[2].data(), planes[3].data() };
		auto srcStep = planes[0].getRowPitchInBytes();
		IppThrowOnError(ippiCopy_16u_P4C4R(srcPtrs, srcStep, data(), getRowPitchInBytes(), roiSize));
	}
}

vector<Ipp16u> Ipp16uArray::median(const IppiRect &roi) const
{
	vector<Ipp16u> result;
	auto vects = toVector(roi);
	for (auto &vec : vects)
	{
		result.push_back(median(vec));
	}

	return result;
}

vector<Ipp16u> Ipp16uArray::median() const
{
	return median({ 0, 0, getWidth(), getHeight() });
}

Ipp16u Ipp16uArray::medianSort(vector<Ipp16u> &values)
{
	std::sort(values.begin(), values.end());
	auto n = values.size();
	auto m = n / 2;
	if ((n % 2) == 1)
	{
		return values[m];
	}

	return Ipp16u(std::round((values[m - 1] + values[m]) / 2));
}

Ipp16u Ipp16uArray::median(vector<Ipp16u> &values)
{
	return medianSort(values);
}

Ipp16u Ipp16uArray::medianQuick(vector<Ipp16u> &values)
{
	int low, high;
	int median;
	int middle, cl, hh;
	Ipp16u *arr = values.data();
	int n = (int)values.size();

	low = 0; high = n - 1; median = (low + high) / 2;
	for (;;)
	{
		if (high <= low) /* One element only */
			return arr[median];

		if (high == low + 1)
		{  /* Two elements only */
			if (arr[low] > arr[high])
				std::swap(arr[low], arr[high]);

			return arr[median];
		}

		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])    std::swap(arr[middle], arr[high]);
		if (arr[low] > arr[high])       std::swap(arr[low], arr[high]);
		if (arr[middle] > arr[low])     std::swap(arr[middle], arr[low]);

		/* Swap low item (now in position middle) into position (low+1) */
		std::swap(arr[middle], arr[low + 1]);

		/* Nibble from each end towards middle, swapping items when stuck */
		cl = low + 1;
		hh = high;
		for (;;)
		{
			do cl++; while (arr[low] > arr[cl]);
			do hh--; while (arr[hh] > arr[low]);

			if (hh < cl)
				break;

			std::swap(arr[cl], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		std::swap(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = cl;
		if (hh >= median)
			high = hh - 1;
	}
}

vector<Ipp64f> Ipp16uArray::L2Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		ippiNorm_L2_16u_C1R(data(), getRowPitchInBytes(), size, result.data());
		break;

	case 3:
		ippiNorm_L2_16u_C3R(data(), getRowPitchInBytes(), size, result.data());
		break;
	}

	return result;
}

vector<Ipp64f> Ipp16uArray::L1Norm() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNorm_L1_16u_C1R(data(), getRowPitchInBytes(), size, result.data()));
		return result;

	case 3:
		IppThrowOnError(ippiNorm_L1_16u_C3R(data(), getRowPitchInBytes(), size, result.data()));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return result;
}

void Ipp16uArray::L1NormDiff(const IppArray<Ipp16u> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);
	auto size = getSize();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNormDiff_L1_16u_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	case 3:
		IppThrowOnError(ippiNormDiff_L1_16u_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}

	return;
}

void Ipp16uArray::L2NormDiff(const IppArray<Ipp16u> &rhs, Ipp64f result[]) const
{
	throwOnArraySizeChannelMismatch(rhs);

	IppiSize size = getSize();
	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiNormDiff_L2_16u_C1R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;

	case 3:
		IppThrowOnError(ippiNormDiff_L2_16u_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result));
		break;
	}

	return;
}


Ipp32fArray Ipp16uArray::crossCorrNorm(const IppArray<Ipp16u> &kernalImage, IppiROIShape roiShape) const
{
	throwNotImplemented();
	throw "For compiler to compile";
}

void Ipp16uArray::importNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax)
{
	auto roi = getRoi();
	if ((rows != roi.height) || (cols != roi.width))
	{
		std::ostringstream os;
		os << "Invalid size in ImportNormalizedYuvFromRgb, expected (" << roi.height << "," << roi.width <<
			" got (" << rows << "," << cols << ")";

		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	auto components = getComponents();
	if ((components != 3) && (components != 1))
	{
		throwComponentError();
	}

	auto inp = p;
	auto oneOverDataMax = 1.0f / float(dataMax);
	for (auto r = 0; r < rows; r++)
	{
		auto outp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			float r = *inp++*oneOverDataMax;
			float g = *inp++*oneOverDataMax;
			float b = *inp++*oneOverDataMax;

			auto y = 0.299f * r + 0.587f * g + 0.114f * b;
			*outp++ = Ipp16u(y * 65535);
			if (components == 3)
			{
				*outp++ = Ipp16u(0.492f * (b - y) * 65535);
				*outp++ = Ipp16u(0.877f * (r - y) * 65535);
			}
		}
	}
}

void Ipp16uArray::importOneNormalizedChannelFromRgb(unsigned short *p, int rows, int cols, int whichChannel, int dataMax)
{
	auto roi = getRoi();
	if ((rows != roi.height) || (cols != roi.width))
	{
		std::ostringstream os;
		os << "Invalid size in importOneNormalizedChannelFromRgb, expected (" << roi.height << "," << roi.width <<
			" got (" << rows << "," << cols << ")";

		THROW_MTI_RUNTIME_ERROR(os.str());
	}

	auto components = getComponents();
	if (components != 1)
	{
		throwComponentError();
	}

	MTIassert(whichChannel >= 0 && whichChannel <= 2);
	if (whichChannel < 0 || whichChannel > 2)
	{
		whichChannel = 1;
	}

	auto inp = p + whichChannel;
	auto oneOverDataMax = 1.0f / float(dataMax);
	for (auto r = 0; r < rows; r++)
	{
		auto outp = getRowPointer(r);
		for (auto c = 0; c < cols; c++)
		{
			*outp++ = Ipp16u(*inp * oneOverDataMax * 65535);
			inp += 3;
		}
	}
}

Ipp16uArray Ipp16uArray::applyBoxFilter(IppiSize boxSize) const
{
	IppStatus ippStatus = ippStsNoErr;
	Ipp16uArray outputArray(getSize());
	IppiSize roiSize = getSize();

	int iTmpBufSize = 0;
	int numChannels = 1;
	ippStatus = ippiFilterBoxBorderGetBufferSize(roiSize, boxSize, ipp16u, numChannels, &iTmpBufSize);
	Ipp8u *pBuffer = ippsMalloc_8u(iTmpBufSize);

	Ipp16u *pSrc = data();
	Ipp16u *pDst = outputArray.data();
	int srcStep = getRowPitchInBytes();
	int dstStep = outputArray.getRowPitchInBytes();
	IppiBorderType borderType = ippBorderRepl;
	Ipp16u borderValue = 0;  // ignored for ippBorderRepl
	ippStatus = ippiFilterBoxBorder_16u_C1R(pSrc, srcStep, pDst, dstStep, roiSize, boxSize, borderType, &borderValue, pBuffer);

	ippsFree(pBuffer);

	return outputArray;
}

void Ipp16uArray::maxAndIndex(Ipp16u *maxValues, MtiPoint *indices) const
{
	IppiSize size = getSize();
	int indexX[3];
	int indexY[3];

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiMaxIndx_16u_C1R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		break;

	case 3:
		IppThrowOnError(ippiMaxIndx_16u_C3R(data(), getRowPitchInBytes(), size, maxValues, indexX, indexY));
		indices[0] = { indexX[0], indexY[0] };
		indices[1] = { indexX[1], indexY[1] };
		indices[2] = { indexX[2], indexY[2] };
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Invalid number of components");
	}
}

std::pair<std::vector<Ipp16u>, std::vector<MtiPoint>> Ipp16uArray::maxAndIndex() const
{
	const auto components = getComponents();
	std::vector<Ipp16u> maxValues(components);
	std::vector<MtiPoint> indices(components);

	maxAndIndex(maxValues.data(), indices.data());
	return { maxValues, indices };
}

vector<Ipp64f> Ipp16uArray::dotProduct(const IppArray &rhs) const
{
	throwOnArraySizeChannelMismatch(rhs);
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiDotProd_16u64f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;

	case 3:
	{
		IppThrowOnError(ippiDotProd_16u64f_C3R(data(), getRowPitchInBytes(), rhs.data(), rhs.getRowPitchInBytes(), size, result.data()));
		break;
	}

	default:
		throwComponentError();
	}

	return result;
}


vector<Ipp64f> Ipp16uArray::mean() const
{
	IppiSize size = getSize();
	vector<Ipp64f> result(getComponents());

	switch (getComponents())
	{
	case 1:

		ippiMean_16u_C1R(data(), getRowPitchInBytes(), size, result.data());
		break;

	case 3:
		ippiMean_16u_C3R(data(), getRowPitchInBytes(), size, result.data());
		break;

	default:
		throwComponentError();
	}

	return result;
}

Ipp16uArray Ipp16uArray::resize(IppiSize newSize) const
{
	if ((getHeight() == 0) || (getWidth() == 0))
	{
		return *this;
	}

	throwOnComponentsSupportedError();

	Ipp16uArray dst({ newSize.width, newSize.height, getComponents() });
	IppiSize srcSize = { getWidth(), getHeight() };

	IppStatus status = IppResize_C1R_or_C3R(
		data(),
		srcSize,
		getRowPitchInBytes(),
		dst.data(),
		newSize,
		dst.getRowPitchInBytes(),
		getComponents());

	IppThrowOnError(status);

	return dst;
}

Ipp16uArray Ipp16uArray::resizeAntiAliasing(IppiSize newSize) const
{
	if ((getHeight() == 0) || (getWidth() == 0))
	{
		return *this;
	}

	throwOnComponentsSupportedError();

	Ipp16uArray dst({ newSize.width, newSize.height , getComponents() });
	IppiSize srcSize = { getWidth(), getHeight() };

	IppStatus status = IppResizeAntiAliasing_C1R_or_C3R(
		data(),
		srcSize,
		getRowPitchInBytes(),
		dst.data(),
		newSize,
		dst.getRowPitchInBytes(),
		getComponents());

	IppThrowOnError(status);
	return dst;
}

Ipp16uArray Ipp16uArray::applyLut(const MtiPlanar<Ipp32sArray> &lut, Ipp8uArray &scratch, int maxLevel) const
{
	auto width = lut.getPlaneSize().width;
	auto levelsVector = MtiMath::linspace(0, maxLevel, width);
	return applyLut(lut, Ipp32sArray(width, levelsVector.data()), scratch);
}

void Ipp16uArray::applyLutInPlace(const MtiPlanar<Ipp32sArray>& lut, Ipp8uArray& scratch, int maxLevel) const
{
	auto width = lut.getPlaneSize().width;
	auto levelsVector = MtiMath::linspace(0, maxLevel, width);
	applyLutInPlace(lut, Ipp32sArray(width, levelsVector.data()), scratch);
}

Ipp16uArray Ipp16uArray::applyLut(const MtiPlanar<Ipp32sArray>& lut, const Ipp32sArray& levels, Ipp8uArray& scratch) const
{
	// Change this when we support alpha
	if ((getComponents() != 3) && (getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Lut channels must match")
	}

	auto numOfChannelsToChange = std::min<int>(3, getComponents());
	if ((numOfChannelsToChange != 3) && (numOfChannelsToChange != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Levels sizes must match")
	}

	// Allocate upt to 3 channels
	int pSpecSize = 0;
	const Ipp32s *pValues[3];
	const Ipp32s *pLevels[3];
	int nLevels[3];
	const IppiInterpolationType interpolation = ippLinear;

	// Uniform number of levels
	// In case we every support alpha
	for (auto i : range(numOfChannelsToChange))
	{
		nLevels[i] = levels.getWidth();
		pLevels[i] = levels.getRowPointer(std::min<int>(i, levels.getHeight() - 1));
		pValues[i] = lut[std::min<int>(i, lut.getPlanarSize().depth - 1)].getRowPointer(0);
	}

	Ipp16uArray result(getSize());

	// We only support two cases, mono or rgb
	if (getComponents() == 1)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp16u, ippC1, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_16u(interpolation, ippC1, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_16u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), pSpec));

		return result;
	}

	if (getComponents() == 3)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp16u, ippC3, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_16u(interpolation, ippC3, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_16u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), getSize(), pSpec));

		return result;
	}

	return result;
}

void Ipp16uArray::applyLutInPlace(const MtiPlanar<Ipp32sArray>& lut, const Ipp32sArray& levels,
	Ipp8uArray& scratch) const
{
	// Change this when we support alpha
	if ((getComponents() != 3) && (getComponents() != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Lut channels must match")
	}

	const auto numOfChannelsToChange = std::min<int>(3, getComponents());
	if ((numOfChannelsToChange != 3) && (numOfChannelsToChange != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Levels sizes must match")
	}

	// Allocate upt to 3 channels
	int pSpecSize = 0;
	const Ipp32s *pValues[3];
	const Ipp32s *pLevels[3];
	int nLevels[3];
	const IppiInterpolationType interpolation = ippCubic;

	// Uniform number of levels
	// In case we every support alpha
	for (auto i : range(numOfChannelsToChange))
	{
		nLevels[i] = levels.getWidth();
		pLevels[i] = levels.getRowPointer(std::min<int>(i, levels.getHeight() - 1));
		pValues[i] = lut[std::min<int>(i, lut.getPlanarSize().depth - 1)].getRowPointer(0);
	}

	// We only support two cases, mono or rgb
	if (getComponents() == 1)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp16u, ippC1, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_16u(interpolation, ippC1, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_16u_C1IR(data(), getRowPitchInBytes(), getSize(), pSpec));
	}

	if (getComponents() == 3)
	{
		IppThrowOnError(ippiLUT_GetSize(interpolation, ipp16u, ippC3, getSize(), nLevels, &pSpecSize));
		if (scratch.getWidth() < pSpecSize)
		{
			scratch = Ipp8uArray(pSpecSize);
		}

		const auto pSpec = reinterpret_cast<IppiLUT_Spec*>(scratch.data());
		IppThrowOnError(ippiLUT_Init_16u(interpolation, ippC3, getSize(), pValues, pLevels, nLevels, pSpec));
		IppThrowOnError(ippiLUT_16u_C3IR(data(), getRowPitchInBytes(), getSize(), pSpec));
	}
}

Ipp16uArray::operator Ipp32fArray() const
{
	Ipp32fArray result(getSize());
	switch (getComponents())
	{
	case 1:
		ippiConvert_16u32f_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize());
		break;

	case 3:
		ippiConvert_16u32f_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize());
		break;

	default:
		throwComponentError();
	}

	return result;
}


void Ipp16uArray::exportToInterleave3(Ipp16uArray &dest)
{
	if (getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 1 component for input to exportToInterleave3");
	}

	if (dest.getComponents() != 3)
	{
		THROW_MTI_RUNTIME_ERROR("Only three component output exportToInterleave3");
	}

	if ((getWidth() != dest.getWidth()) || (getHeight() != dest.getHeight()))
	{
		THROW_MTI_RUNTIME_ERROR("Size must match in exportToInterleave3");
	}

	// usual loop, duplicate 3 times
	for (auto r = 0; r < getHeight(); r++)
	{
		auto inPointer = getRowPointer(r);
		auto outPointer = dest.getRowPointer(r);
		for (auto c = 0; c < getWidth(); c++)
		{
			auto v = *inPointer++;
			*outPointer++ = v;
			*outPointer++ = v;
			*outPointer++ = v;
		}
	}
}

Ipp16uArray Ipp16uArray::transpose() const
{
	auto roi = getRoi();

	// Note: transpose swaps the size
	Ipp16uArray result({ roi.height, roi.width, getComponents() });
	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 1:
		IppThrowOnError(ippiTranspose_16u_C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 3:\
		IppThrowOnError(ippiTranspose_16u_C3R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize)); \
		break;

	case 4:
		IppThrowOnError(ippiTranspose_16u_C4R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize)); \
			break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp16uArray Ipp16uArray::toGray() const
{
	// Result is one component
	Ipp16uArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
   case 1:
      result <<= *this;
      break;

	case 3:
		IppThrowOnError(ippiRGBToGray_16u_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	case 4:
		IppThrowOnError(ippiRGBToGray_16u_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

Ipp16uArray Ipp16uArray::toGray(const float weights[]) const
{
	Ipp16uArray result({ getSize(), 1 });

	const auto roiSize = getSize();
	const auto dataPtr = data();

	switch (getComponents())
	{
	case 3:
		IppThrowOnError(ippiColorToGray_16u_C3C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	case 4:
		IppThrowOnError(ippiColorToGray_16u_AC4C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize, weights));
		break;

	default:
		throwComponentError();
		break;
	}

	return result;
}

// Select operator for different types of copy
Ipp16uArray Ipp16uArray::operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData) const
{
	return SelectFromFunction(*this, probeRoi, copyType, utilizeExtrinsicData);
}

Ipp16uArray Ipp16uArray::fromPixelIndices(const IppArray<Ipp32s>& indices) const
{
   MTIassert(indices.getComponents() == 1);
   Ipp16uArray result({ indices.getSize(), getComponents() });

   if (getComponents() == 1)
   {
      for (auto r = 0; r < indices.getHeight(); r++)
      {
         auto ip = indices.getRowPointer(r);
         auto rp = result.getRowPointer(r);
         for (auto c = 0; c < indices.getWidth(); c++)
         {
            *rp++ = (*this)[*ip++];
         }
      }

      return result;
   }

   for (auto r = 0; r < indices.getHeight(); r++)
   {
      auto ip = indices.getRowPointer(r);
      auto rp = result.getRowPointer(r);
      for (auto c = 0; c < indices.getWidth(); c++)
      {
         // Copy the pixel
         for (auto d = 0; d < getComponents(); d++)
         {
            *rp++ = (*this)[*ip + d];
         }

         ++ip;
      }
   }

   return result;
}

std::vector<Ipp16u> Ipp16uArray::fromPixelIndices(const vector<Ipp32s>& indices) const
{
   std::vector<Ipp16u> result(indices.size() * getComponents());
   auto rp = result.data();
   auto ip = indices.data();

   if (getComponents() == 1)
   {
      for (auto c = 0; c < indices.size(); c++)
      {
         auto i = *ip;
         auto v = (*this)[i];
         *rp++ = (*this)[*ip++];
      }

      return result;
   }

   for (auto c = 0; c < indices.size(); c++)
   {
      // Copy the pixel
      for (auto d = 0; d < getComponents(); d++)
      {
         *rp++ = (*this)[*ip + d];
      }

      ++ip;
   }

   return result;
}

Ipp16uArray Ipp16uArray::sharpen() const
{
	THROW_MTI_RUNTIME_ERROR("Not Implemented");
	//	Ipp16uArray result(getHeight(), getWidth(), getComponents());
	//	IppiMaskSize mask = ippMskSize3x3;
	//	IppiBorderType borderType = IppiBorderType::ippBorderRepl;
	//	Ipp32f borderValue[] = { 0,0,0 };
	//
	//	int bufSize = 0;
	//	Ipp8u *pBuffer;
	//	IppThrowOnError(ippiFilterSharpenBorderGetBufferSize(result.getSize(), mask, ipp16u, ipp16u, getComponents(), &bufSize));
	//	pBuffer = ippsMalloc_8u(bufSize);
	//	IppStatus status = IppStatus::ippStsNoErr;
	//
	//	switch (getComponents())
	//	{
	//	case 1:
	//		status = ippiFilterSharpenBorder_16u_C1R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), mask, borderType, borderValue[0], pBuffer);
	//		break;
	//
	//	case 3:
	//		status = ippiFilterSharpenBorder_16u_C3R(data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), result.getSize(), mask, borderType, borderValue, pBuffer);
	//		break;
	//
	//	default:
	//		throwComponentError();
	//	}
	//
	//	ippsFree(pBuffer);
	//	IppThrowOnError(status);
	//
	//	return result;
}

namespace
{

	IppStatus IppResize_C1R_or_C3R(Ipp16u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp16u* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
	{
		if (numChannels != 1 && numChannels != 3)
		{
			THROW_MTI_RUNTIME_ERROR("Bad number of channels in input to Resize");
		}

		IppiResizeSpec_32f* pSpec = nullptr;
		int specSize = 0, initSize = 0, bufSize = 0;
		Ipp8u* pBuffer = nullptr;
		Ipp8u* pInitBuf = nullptr;
		IppiPoint dstOffset = { 0, 0 };
		IppStatus status = ippStsNoErr;
		IppiBorderType border = ippBorderRepl;

		// ippiResize<Interpolation>Init functions has the following constraints
		// for the minimal size of an input image depending on the chosen
		// interpolation method:
		//    Nearest Neighbor - 1x1
		//    Linear           - 2x2
		//    2-lobed Lanczos  - 4x4
		//    3-lobed Lanczos  - 6x6
		IppiInterpolationType interpolation = ippLanczos;
		Ipp32u numLanczosLobes = 3;
		const int minLinearDim = 2;
		const int minLanczos2Dim = 4;
		const int minLanczos3Dim = 6;
		if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
			|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
		{
			interpolation = ippNearest;
		}
		else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
			|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
		{
			interpolation = ippLinear;
		}
		else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
			|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
		{
			numLanczosLobes = 2;
		}

		// Allocate spec and init buffers.
		status = ippiResizeGetSize_16u(srcSize, dstSize, interpolation, 0, &specSize, &initSize);
		if (status != ippStsNoErr)
		{
			return status;
		}

		pSpec = reinterpret_cast<IppiResizeSpec_32f*>(ippsMalloc_8u(specSize));
		if (pSpec == nullptr)
		{
			return ippStsNoMemErr;
		}

		// Initialize the filter.
		switch (interpolation)
		{
		case ippNearest:
			status = ippiResizeNearestInit_16u(srcSize, dstSize, pSpec);
			break;

		case ippLinear:
			status = ippiResizeLinearInit_16u(srcSize, dstSize, pSpec);
			break;

		case ippLanczos:
			pInitBuf = ippsMalloc_8u(initSize);
			if (pInitBuf == nullptr)
			{
				ippsFree(pSpec);
				return ippStsNoMemErr;
			}

			status = ippiResizeLanczosInit_16u(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
			break;

		default:
			break;
		}

		ippsFree(pInitBuf);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			return status;
		}

		// Allocate work buffer.
		status = ippiResizeGetBufferSize_16u(pSpec, dstSize, numChannels, &bufSize);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return status;
		}

		pBuffer = ippsMalloc_8u(bufSize);
		if (pBuffer == nullptr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return ippStsNoMemErr;
		}

		/* Resize processing */
		if (numChannels == 1)
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_16u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_16u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_16u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			default:
				break;
			}
		}
		else
		{
			switch (interpolation)
			{
			case ippNearest:
				status = ippiResizeNearest_16u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, pSpec, pBuffer);
				break;

			case ippLinear:
				status = ippiResizeLinear_16u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			case ippLanczos:
				status = ippiResizeLanczos_16u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
				break;

			default:
				break;
			}
		}

		ippsFree(pSpec);
		ippsFree(pBuffer);

		return status;
	}

	/////////////////////////////////////////////////////////////////////////////////////
// This is anti aliasing resize code
	IppStatus IppResizeAntiAliasing_C1R_or_C3R(Ipp16u* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp16u* pDst, IppiSize dstSize, Ipp32s dstStep, Ipp32u numChannels)
	{
		if (numChannels != 1 && numChannels != 3)
		{
			THROW_MTI_RUNTIME_ERROR("Bad number of channels in input to Resize");
		}

		IppiResizeSpec_32f* pSpec = nullptr;
		int specSize = 0, initSize = 0, bufSize = 0;
		Ipp8u* pBuffer = nullptr;
		Ipp8u* pInitBuf = nullptr;
		IppiPoint dstOffset = { 0, 0 };
		IppStatus status = ippStsNoErr;
		IppiBorderType border = ippBorderRepl;

		// ippiResize<Interpolation>Init functions has the following constraints
		// for the minimal size of an input image depending on the chosen
		// interpolation method:
		//    Nearest Neighbor - 1x1
		//    Linear           - 2x2
		//    2-lobed Lanczos  - 4x4
		//    3-lobed Lanczos  - 6x6

		IppiInterpolationType interpolation = ippLanczos;
		//	IppiInterpolationType interpolation = ippCubic;
		Ipp32u numLanczosLobes = 3;
		const int minLinearDim = 2;
		const int minLanczos2Dim = 4;
		const int minLanczos3Dim = 6;
		if (srcSize.width < minLinearDim || srcSize.height < minLinearDim
			|| dstSize.width < minLinearDim || dstSize.height < minLinearDim)
		{
			interpolation = ippNearest;
		}
		else if (srcSize.width < minLanczos2Dim || srcSize.height < minLanczos2Dim
			|| dstSize.width < minLanczos2Dim || dstSize.height < minLanczos2Dim)
		{
			interpolation = ippLinear;
		}
		else if (srcSize.width < minLanczos3Dim || srcSize.height < minLanczos3Dim
			|| dstSize.width < minLanczos3Dim || dstSize.height < minLanczos3Dim)
		{
			numLanczosLobes = 2;
		}

		// Allocate spec and init buffers.
		status = ippiResizeGetSize_16u(srcSize, dstSize, interpolation, 1, &specSize, &initSize);
		if (status != ippStsNoErr)
		{
			return status;
		}

		pSpec = reinterpret_cast<IppiResizeSpec_32f*>(ippsMalloc_8u(specSize));
		if (pSpec == nullptr)
		{
			return ippStsNoMemErr;
		}

		pInitBuf = ippsMalloc_8u(initSize);
		if (pInitBuf == nullptr)
		{
			ippsFree(pSpec);
			return ippStsNoMemErr;
		}

		// Initialize the filter.
		switch (interpolation)
		{
		case ippLinear:
			status = ippiResizeAntialiasingLinearInit(srcSize, dstSize, pSpec, pInitBuf);
			break;

		case ippCubic:
			// Catmull - Rom(B = 0, C = 0.5), B - Spline(B = 1, C = 0)
			status = ippiResizeAntialiasingCubicInit(srcSize, dstSize, 0.0f, 0.5f, pSpec, pInitBuf);
			break;

		case ippLanczos:

			status = ippiResizeAntialiasingLanczosInit(srcSize, dstSize, numLanczosLobes, pSpec, pInitBuf);
			break;

		default:
			status = ippStsNotSupportedModeErr;
			break;
		}

		ippsFree(pInitBuf);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			return status;
		}

		// Allocate work buffer.
		status = ippiResizeGetBufferSize_32f(pSpec, dstSize, numChannels, &bufSize);
		if (status != ippStsNoErr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return status;
		}

		pBuffer = ippsMalloc_8u(bufSize);
		if (pBuffer == nullptr)
		{
			ippsFree(pSpec);
			ippsFree(pInitBuf);
			return ippStsNoMemErr;
		}

		/* Resize processing */
		if (numChannels == 1)
		{
			status = ippiResizeAntialiasing_16u_C1R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
		}
		else
		{
			status = ippiResizeAntialiasing_16u_C3R(pSrc, srcStep, pDst, dstStep, dstOffset, dstSize, border, 0, pSpec, pBuffer);
		}

		ippsFree(pSpec);
		ippsFree(pBuffer);

		return status;
	}
}; // end
