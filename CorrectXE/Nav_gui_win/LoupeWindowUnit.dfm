object LoupeWindow: TLoupeWindow
  Left = 192
  Top = 132
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Loupe'
  ClientHeight = 248
  ClientWidth = 248
  Color = clWindow
  TransparentColor = True
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  DesignSize = (
    248
    248)
  PixelsPerInch = 96
  TextHeight = 13
  object LoupeImagePanel: TPanel
    Left = 0
    Top = 0
    Width = 248
    Height = 248
    Anchors = []
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clBlack
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object StaticLoupeImage: TImage
      Left = 3
      Top = 3
      Width = 240
      Height = 240
    end
  end
  object ParentCheckTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = ParentCheckTimerTimer
    Left = 88
    Top = 16
  end
end
