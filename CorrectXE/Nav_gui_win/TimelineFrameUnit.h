//---------------------------------------------------------------------------

#ifndef TimelineFrameUnitH
#define TimelineFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "MTITrackBar.h"
#include <Graphics.hpp>
#include "MTIUNIT.h"
#include "MasterTimeLineFrame.h"
#include <Menus.hpp>
#include <Buttons.hpp>
#include "VTimeCodeEdit.h"
#include "Reticle.h"
#include "ColorLabel.h"
#include "ColorPanel.h"

#include <string>
using std::string;

enum CUT_ACTIONS{
   CUT_DELETE = 0,
   CUT_ADD,
   CUT_NEXT,
   CUT_PREV,
   CUT_MARKSWITCH,
   CUT_REFRESH
   };

//---------------------------------------------------------------------------
class TTimelineFrame : public TFrame
{
__published:	// IDE-managed Components
   TPanel *EntirePanel;
   TPanel *ToolbarPanel;
   TLabel *timelineInTCLabel;
	TColorLabel *timelineCurrentTCLabel;
   TLabel *timelineOutTCLabel;
    TTimer *UpdateTimer;
    TPopupMenu *HackedTimeLinePopup;
    TMenuItem *CopyTCMenuItem;
    TMenuItem *N2;
    TMenuItem *ShowHideMenuItem;
    TMenuItem *SceneCutsMenuItem;
    TMenuItem *AudioMenuItem;
    TMenuItem *Ch1MenuItem;
    TMenuItem *Ch2MenuItem;
    TMenuItem *Ch3MenuItem;
    TMenuItem *Ch4MenuItem;
    TMenuItem *Ch5MenuItem;
    TMenuItem *Ch6MenuItem;
    TMenuItem *Ch7MenuItem;
    TMenuItem *Ch8MenuItem;
    TMenuItem *HandlesMenuItem;
   TPanel *LicenseAndUpgradeStatusPanel;
   TMenuItem *TCRefMenuItem;
   VTimeCodeEdit *timelineCurrentTCVTimecodeEdit;
        TMenuItem *NavButtonsMenuItem;
        TPanel *CurrentTCPanel;
        TSpeedButton *ClipInTCButton;
        TSpeedButton *MarkInTCButton;
        TSpeedButton *ClipOutTCButton;
        TMenuItem *BookmarksMenuItem;
   TMenuItem *MaskAnimationMenuItem;
        TPanel *LicenseStatusPanel;
        TTimer *AnnoyingLicenseWarningTimer;
   TPanel *UpgradeStatusPanel;
        TSpeedButton *ShowUpgradeStatusButton;
   TLabel *UpgradeMessageLabel;
        TLabel *LicenseMessageLabel;
   TSpeedButton *ShowLicenseStatusButon;
   TColorPanel *CenterToolbarCenteringPanel;
   TPanel *ReticleToolbarPanel;
	TFlowPanel *JustifyToolbarPanel;
   TPanel *ZoomToolbarPanel;
   TPanel *ChannelsToolbarPanel;
   TPanel *TimelineToolbarRibbonPanel;
   TSpeedButton *ReticleAllButton;
   TSpeedButton *ReticlePrimaryButton;
   TSpeedButton *ReticleSafeActionButton;
   TSpeedButton *ReticleSafeTitleButton;
   TSpeedButton *ReticleMattedButton;
   TComboBox *ReticleToolbarComboBox;
   TSpeedButton *ReticleEditButton;
   TSpeedButton *JustifyLeftButton;
   TSpeedButton *JustifyCenterButton;
   TSpeedButton *JustifyRightButton;
   TBitBtn *ViewModeTBItem;
   TSpeedButton *RgbAllButton;
   TSpeedButton *RgbGrayButton;
   TSpeedButton *RgbBlueButton;
   TSpeedButton *RgbAlphaButton;
   TSpeedButton *RgbGreenButton;
   TSpeedButton *RgbRedButton;
   TPanel *NavButtonsPanel;
   TSpeedButton *TLFastForwardButton;
   TSpeedButton *TLJogBackButton;
   TSpeedButton *TLJogForwardButton;
   TSpeedButton *TLPlayButton;
   TSpeedButton *TLPlayReverseButton;
   TSpeedButton *TLRewindButton;
   TSpeedButton *TLStopButton;
   TSpeedButton *TLClearMarksButton;
   TSpeedButton *TLMarkEntireClipButton;
   TSpeedButton *TLSetInMarkButton;
   TSpeedButton *TLSetMarkOutExclusiveButton;
   TSpeedButton *TLSetMarkOutInclusiveButton;
   TSpeedButton *TLSetShotMarksButton;
   TPanel *LeftToolbarCenteringPanel;
   TPanel *RightToolbarCenteringPanel;
   TColorPanel *RGBPanel;
	TPanel *DRSNamePanel;
	TLabel *DRSMessageLabel;
	TLabel *DRSNameLabel;
   TFMasterTimeLine *MTL;
   TMenuItem *TimeDisplayMenuItem;
   TMenuItem *UseClipDefaultModeMenuItem;
   TMenuItem *ForceTimecodeDisplayMenuItem;
   TMenuItem *ForceFrameNumberDisplayMenuItem;
   TMenuItem *N12;
   TMenuItem *Fps24MenuItem;
   TMenuItem *Fps25MenuItem;
   TMenuItem *Fps30MenuItem;
   TMenuItem *Fps50MenuItem;
   TMenuItem *Fps60MenuItem;
   TMenuItem *Fps60DfMenuItem;
   TMenuItem *Fps30DfMenuItem;
	TFlowPanel *FrameCompareModeToolbarPanel;
	TSpeedButton *FrameWipeCompareModeButton;
	TSpeedButton *DualFrameCompareModeButton;
   TLabel *FpsReadoutLabel;
   TSpeedButton *MarkOutTCButton;
       void __fastcall OnMTLResize(TObject *Sender);
    void __fastcall FrameResize(TObject *Sender);
    void __fastcall UpdateTimerTimer(TObject *Sender);
    void __fastcall CopyTimeMenuItemClick(TObject *Sender);
    void __fastcall timelineInTCLabelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall ToolbarPanelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall CheckedMenuItemClick(TObject *Sender);
        void __fastcall MTLHandlesMenuItemClick(TObject *Sender);
        void __fastcall ToolbarPanelMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall CutItemClick(TObject *Sender);
        void __fastcall MTLCuts1Click(TObject *Sender);
        void __fastcall MTLCh1MenuItemClick(TObject *Sender);
    void __fastcall MTLShowHideMenuItemClick(TObject *Sender);
    void __fastcall ShowHideMenuItemClick(TObject *Sender);
   void __fastcall ClipInTCButtonClick(TObject *Sender);
   void __fastcall ClipOutTCButtonClick(TObject *Sender);
   void __fastcall MarkInTCButtonClick(TObject *Sender);
   void __fastcall MarkOutTCButtonClick(TObject *Sender);
   void __fastcall TLJogBackButtonClick(TObject *Sender);
   void __fastcall TLJogForwardButtonClick(TObject *Sender);
   void __fastcall TLPlayReverseButtonClick(TObject *Sender);
   void __fastcall TLPlayButtonClick(TObject *Sender);
   void __fastcall TLStopButtonClick(TObject *Sender);
   void __fastcall TLRewindButtonClick(TObject *Sender);
   void __fastcall TLFastForwardButtonClick(TObject *Sender);
   void __fastcall TLSetInMarkButtonClick(TObject *Sender);
   void __fastcall TLSetMarkOutExclusiveButtonClick(TObject *Sender);
   void __fastcall TLSetMarkOutInclusiveButtonClick(TObject *Sender);
   void __fastcall TLMarkEntireClipButtonClick(TObject *Sender);
   void __fastcall TLSetShotMarksButtonClick(TObject *Sender);
   void __fastcall TLClearMarksButtonClick(TObject *Sender);
   void __fastcall timelineCurrentTCVTimecodeEditEnterKey(TObject *Sender);
   void __fastcall timelineCurrentTCVTimecodeEditExit(TObject *Sender);
   void __fastcall timelineCurrentTCVTimecodeEditEscapeKey(
          TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall AnnoyingLicenseWarningTimerTimer(TObject *Sender);
        void __fastcall LicenseMessageClick(TObject *Sender);
        void __fastcall ShowLicenseStatusButtonClick(TObject *Sender);
        void __fastcall UpgradeMessageClick(TObject *Sender);
   void __fastcall ReticleAllButtonClick(TObject *Sender);
   void __fastcall ReticleSelectionButtonClick(TObject *Sender);
   void __fastcall ReticleEditButtonClick(TObject *Sender);
   void __fastcall ReticleToolbarComboBoxClick(TObject *Sender);
   void __fastcall JustifyLeftButtonClick(TObject *Sender);
   void __fastcall JustifyCenterButtonClick(TObject *Sender);
   void __fastcall JustifyRightButtonClick(TObject *Sender);
   void __fastcall RgbAllButtonClick(TObject *Sender);
   void __fastcall RgbRedButtonClick(TObject *Sender);
   void __fastcall RgbGreenButtonClick(TObject *Sender);
   void __fastcall RgbBlueButtonClick(TObject *Sender);
   void __fastcall RgbGrayButtonClick(TObject *Sender);
   void __fastcall RgbAlphaButtonClick(TObject *Sender);
   void __fastcall ViewModeTBItemClick(TObject *Sender);
   void __fastcall ReticleToolbarComboBoxCloseUp(TObject *Sender);
   void __fastcall TimeDisplayDefaultModeMenuItemClick(TObject *Sender);
   void __fastcall FpsMenuItemClick(TObject *Sender);
   void __fastcall TimeDisplayMenuItemClick(TObject *Sender);
	void __fastcall DualFrameCompareModeButtonClick(TObject *Sender);
	void __fastcall FrameWipeCompareModeButtonClick(TObject *Sender);
     
private:	// User declarations
   void SetMarkPosition(int markFrame, TImage *markImage);
   void __fastcall MTLTrackBarChange(TObject *Sender);
   void __fastcall MTLMenuEventChange(TObject *Sender,
                                      EMenuEventAction Action,
                                      MTI_UINT64 &EventFlag,
                                      int &Frame, bool &Continue);
   void SyncMenusWithMTL();
   void CheckLicenseStatus();
   void ResizeCenterToolbar(int windowWidth);

   DEFINE_CBHOOK(CurrentTCChanged, TTimelineFrame);
   DEFINE_CBHOOK(ClipChanged, TTimelineFrame);
   DEFINE_CBHOOK(MarksChanged, TTimelineFrame);
   DEFINE_CBHOOK(FrameCacheChanged, TTimelineFrame);
   DEFINE_CBHOOK(VisibleFrameRangeChanged, TTimelineFrame);
   DEFINE_CBHOOK(UserPreferredTimecodeStringStyleChanged, TTimelineFrame);

   bool _bInhibit;
   bool _bTCUpdate;
   bool _bMarkUpdate;
   bool _bFrameCacheUpdate;
   int  _OldTop;

   string jumpString;

   int markFrame;

public:		// User declarations
   __fastcall TTimelineFrame(TComponent* Owner);
   void FrameCreate();
   void Init(void);
   void UpdateInTimecode();
   void UpdateOutTimecode();
   void UpdateCurTimecode();
   void UpdateMarkOutButton();
   void showTimecodeEditor(int sign); // -1=sub, 0=abs, 1=add
   void showTimecodeEditorInPresentationMode(int funcChar);
   void ResizeTimecodeToolbar();
   void UpdateTimelineLimits();
   void UpdateTimelineSlider();
   void UpdateMarks();
   void OpenForReadingScenes();
   int  GetNextScene(int *markIn, int *markOut);
   string GetJumpString();
   void SetJumpString(const string &newJumpString);
	void getCutList(vector<int> &cutList);
   int getShowAlphaState(); // Tri-state (-1 = disabled, 0 = off, 1 = on)
   void setShowAlphaState(bool state);

   //-------------------------------------------
   //Added by Kea, 6-17
   //This is probably not the nicest place to put this, being a gui and all
   //But it is accessible by the controller. Method declaration to add
   //and remove cut marks.
   //------------------------------------------
   void performCutAction(int actiontype);

   void AddBookmark(void);
   void DropBookmark(void);
   void SetBookmarkIn(void);
   void SetBookmarkOut(void);
   void GoToBookmarkIn(void);
   void GoToBookmarkOut(void);

   void goToNextEventOnSelectedTimeline();
   void goToPreviousEventOnSelectedTimeline();

   //--------------------------------------------
   void ShowMaskAnimationTimeline(bool flag);
   void ShowCutsTimeline(bool flag);
   void ShowBookmarkTimeline(bool flag);
   void ShowReferenceTimeline(bool flag);

   void ReleaseClipInTimeline(void);
   void SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface);
   void RedrawMTL();

   void UpdateRGBChannelsToolbarButtons(int rgbFlags, bool alphaEnabled, bool showAlpha);
   void UpdateZoomLevelStatus(double zoomFactor, int displayMode);
   void UpdateReticleToolbarModeButtons(int mode);
   void UpdateReticleToolbarComboBox(CReticle currentReticle);
   void ToggleReticleComboBoxFocus();
	void UpdateJustifyButtons(int justify);
	void UpdateFrameCompareToolbarButtons(int whichButtonIsDown);

   virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
   virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
   virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
   virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.

};
//---------------------------------------------------------------------------
extern PACKAGE TTimelineFrame *TimelineFrame;
//---------------------------------------------------------------------------
#endif
