//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "LUTNameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLUTNameForm *LUTNameForm = NULL;
//---------------------------------------------------------------------------

__fastcall TLUTNameForm::TLUTNameForm(TComponent* Owner)
        : TForm(Owner), defaultNameWasSet(false)

{
}
//---------------------------------------------------------------------------

void __fastcall TLUTNameForm::FormShow(TObject *Sender)
{
   // If no default name was set before the show, clear the text field
   if (defaultNameWasSet)
      defaultNameWasSet = false;
   else
      LUTNameEdit->Text = "";

   FocusControl(LUTNameEdit);
}
//---------------------------------------------------------------------------

void __fastcall TLUTNameForm::FormKeyDown(TObject *Sender, Word &Key,
        TShiftState Shift)
{
   if (Key == VK_RETURN)
   {
      Key = 0;
      ModalResult = mrOk;
   }
   if (Key == VK_ESCAPE)
   {
      Key = 0;
      ModalResult = mrCancel;
   }
}
//---------------------------------------------------------------------------

void TLUTNameForm::SetDefaultName(const string &defaultName)
{
   LUTNameEdit->Text = defaultName.c_str();
   defaultNameWasSet = true;
}
//---------------------------------------------------------------------------

string TLUTNameForm::GetName()
{
   return StringToStdString(LUTNameEdit->Text);
}
//---------------------------------------------------------------------------

