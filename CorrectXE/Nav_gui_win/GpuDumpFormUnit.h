//---------------------------------------------------------------------------

#ifndef GpuDumpFormUnitH
#define GpuDumpFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include "IniFile.h"

//---------------------------------------------------------------------------
class TGpuDumpForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *ControlPanel;
	TButton *CloseButton;
	TRichEdit *HeaderDumpMemo;
	void __fastcall CloseButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TGpuDumpForm(TComponent* Owner);
        void Clear();
        void Set(const string &newText);
};
//---------------------------------------------------------------------------
extern PACKAGE TGpuDumpForm *GpuDumpForm;
//---------------------------------------------------------------------------
#endif
