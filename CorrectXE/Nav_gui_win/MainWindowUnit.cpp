// MainWindowUnit.h   Navigator's Main Window
//
/////////////////////////////////////////////////////////////////////////////

#include <vcl.h>
#include <ddraw.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <algorithm>
#include <vector>
#pragma hdrstop

#include "MainWindowUnit.h"
#include "GoToFrameUnit.h"

#include "AboutUnit.h"
#include "BaseTool.h"
#include "BaseToolUnit.h"
#include "BinManagerGUIUnit.h"
#include "BookmarkEventViewerUnit.h"
#include "BookmarkManager.h"
#include "cpmp_err.h"
#include "ClipAPI.h"
#include "CustomReticleUnit.h"
#include "Displayer.h"
#include "DllSupport.h"
#include "Encrypt.h"
#include "err_tool.h"
#include "GOVTool.h"
#include "MediaFileIO.h"
#include "ImageDatumConvert.h"
#include "ImageFileMediaAccess.h"
#include "IniFile.h"
#include "JobManager.h"
#include "LicensingUnit.h"
#include "LoupeWindowUnit.h"
#include "LutSettingsEditor.h"
#include "LutSettingsManager.h"
#include "Magnifier.h"
#include "ManageMediaUnit.h"
#include "MaskTool.h"
#include "MaskToolUnit.h"
#include "MasterTimeLineFrame.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
#include "NavMainCommon.h"
#include "NavSystemInterface.h"
#include "NavigatorTool.h"
#include "PDLViewerManager.h"
#include "Player.h"
#include "PreferencesUnit.h"
#include "ReleaseNotesViewer.h"
#include "ReticleTool.h"
#include "SharedMap.h"
#include "ShowModalDialog.h"
#include "SysInfo.h"
#include "timecode.h"
#include "TimelineFrameUnit.h"
#include "ToolCommand.h"
#include "ToolManager.h"
#include "ToolObject.h"
#include "UserInput.h"
#include "UserGuideViewer.h"
#include "GpuWarningUnit.h"
#ifndef NO_QLM_LICENSING
#include "MtiLicenseLib.h"
#endif

// #define TESTFRAMES
#ifdef TESTFRAMES
// #include "LineEngine.h"
#endif

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "MTIBusy"
#pragma link "GrayComboBox"
#pragma link "MTIDBMeter"
#pragma link "VTimeCodeEdit"
#pragma link "MTIProgressBar"
#pragma link "MTITrackBar"
#pragma link "PopupComboBox"
#pragma link "TimelineTrackBar"
#pragma link "IconListBox"
#pragma link "VDoubleEdit"
#pragma link "Knob"
#pragma link "TimelineFrameUnit"
#pragma link "ColorSplitter"
#pragma resource "*.dfm"

// ---------------------------------------------------------------------------
#define DEFAULT_BORDER_WIDTH 20
#define MIN_WINDOW_WIDTH 20
#define MIN_WINDOW_HEIGHT 15
#define BORDER_GRAY  0x00404040

//#define DEBUG_MAXIMIZE

// ---------------------------------------------------------------------------
TmainWindow *mainWindow;
// FLEXLM features, we don't use strings to make it much more difficult for
// the cracker to patch the feature
// Encryption for "CORRECT-NAVIGATOR"
static MTI_UINT32 FEATURE_CORRECT_NAVIGATOR[] =
{32, 0x9046d2e, 0x45808e34, 0x9fb22620, 0x6404dbfa, 0x42d19c83, 0x532fc25d, 0x841b5f48, 0x73dcb449, 0xfa399812};

// Encryption for "CONTROL-NAVIGATOR"
static MTI_UINT32 FEATURE_CONTROL_NAVIGATOR[] =
{32, 0xc0f095d, 0x5956d2ad, 0xaf9752b2, 0x0c3f817d, 0x9a2a899f, 0x2cae1e6b, 0x5402d01f, 0xc8637bc9, 0x4549e7d0};

// Encryption for "CONTROL-IO"
static MTI_UINT32 FEATURE_CONTROL_IO[] =
{0x5ae3c10, 0x15f14502, 0x6c7bf217, 0x24e76a9e, 0x7bce912d, 0x28e98974};

// Encryption for "CORRECT-CADENCE"
static MTI_UINT32 FEATURE_CORRECT_CADENCE[] =
{0x3db8610, 0x99e638a, 0xa82bef34, 0x720f9ec6, 0x97245644, 0x47ac4d03};

// Feature array, used to find expiration dates
static MTI_UINT32 *FeatureArray[] =
{FEATURE_CORRECT_NAVIGATOR, FEATURE_CONTROL_NAVIGATOR, FEATURE_CONTROL_IO, FEATURE_CORRECT_CADENCE, 0};

// ************************ DEBUG CODE ********************8
#include "HRTimer.h"
#include <iomanip>
static CHRTimer HRTimer;
static bool HRTimerStarted = false;
static double HRLastTraceTime = 0.0;
static double HRLastEnterTime = 0.0;
static double HRMaxInterval = 0.0;
static int HREnterCount = 0;

////////////////////////////////////////////////////////////////////////////////

void DumpClientRects()
{
	RECT mwcr = mainWindow->ClientRect;
	RECT dpcr = mainWindow->DisplayPanel->ClientRect;
	MTIostringstream os;
	os << "------------------------------------------------------" << endl;
	os << "        MainWindow.ClientRect = { " << mwcr.left << ", " << mwcr.top << ", " << mwcr.right << ", " <<
		 mwcr.bottom << " }" << endl;
	os << "        DisplayPanel.ClientRect = { " << dpcr.left << ", " << dpcr.top << ", " << dpcr.right << ", " <<
		 dpcr.bottom << " }" << endl;
	os << "-------------------------------------------------------------------" << endl;

	TRACE_1(errout << os.str());
}

bool isTaskbarAutoHidden()
{
	auto taskbarHandle = ::FindWindow("Shell_TrayWnd", nullptr);
	if (taskbarHandle == INVALID_HANDLE_VALUE)
	{
		static bool oneShot = true;
		if (oneShot)
		{
			oneShot = false;
			DBCOMMENT("Can't get handle to system taskbar!");
		}

		return false;
	}

	APPBARDATA data;
	data.cbSize = sizeof(data);
	data.hWnd = taskbarHandle;
	auto state = (int)SHAppBarMessage(ABM_GETSTATE, &data);
	auto taskbarIsAutoHidden = (state & ABS_AUTOHIDE) != 0;
	return taskbarIsAutoHidden;
}

////////////////////////////////////////////////////////////////////////////////
// ************************ DEBUG CODE ********************8

// ---------------------------------------------------------------------------
// Bin Manager GUI Interface Implementation

typedef CBinMgrGUIInterface::lockID_t lockID_t;

bool CNavBinMgrGUIInterface::IsItSafeToUnloadCurrentClip()
{
	CToolManager toolMgr;
   if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
   {
      // A change is pending or a tool is running so
      // cannot unload the current clip right now!
      return false;
   }

	return true;
}

int CNavBinMgrGUIInterface::SaveCurrentClipState()
{
   return mainWindow->GetCurrentClipHandler()->saveClipState();
}

void CNavBinMgrGUIInterface::ClipDoubleClick(const string& clipFileName)
{
   mainWindow->OpenClipByName(clipFileName);
}

void CNavBinMgrGUIInterface::ClipDeleted(const string& clipFileName)
{
   mainWindow->UnloadClipIfCurrent(clipFileName);
}

void CNavBinMgrGUIInterface::ClipModified(const string& clipFileName)
{
	CNavCurrentClip* currentClipHandler = mainWindow->GetCurrentClipHandler();

	if (currentClipHandler->isThisCurrentClip(clipFileName))
	{
		// Clip has been modified, reload it so the Navigator knows about
		// the changes
		string currentClipFileName = currentClipHandler->getCurrentClipFileNameWithExt();
		TRACE_1(errout << "RELOAD CURRENT CLIP " << currentClipFileName);
      if (BinManagerForm != nullptr)
      {
         BinManagerForm->ClearProxy();
      }

		int stat = currentClipHandler->reloadCurrentClip();
      if (BinManagerForm != nullptr)
      {
         BinManagerForm->ReloadProxy();
      }

		if (stat != 0)
		{
			TRACE_0(errout << "ERROR: Could not reload current clip " << currentClipFileName << endl <<
				 "       Return code = " << stat);
			return;
		}

		// Rebuild the cut timeline (Bug 1623)
		mainWindow->GetTimeline()->performCutAction(CUT_REFRESH);
	}
}

int CNavBinMgrGUIInterface::FinishClipRename(lockID_t lockID, const string &oldBinPath, const string &oldClipName,
	 const string &newBinPath, const string &newClipName, string &reason)
{
	CBinManager binMgr;
	ClipModified(binMgr.makeClipFileNameWithExt(newBinPath, newClipName));
	mainWindow->UpdateWindowCaption();
	mainWindow->Invalidate();
	return 0;
}

lockID_t CNavBinMgrGUIInterface::RequestBinRename(const string &oldBinPath, const string &newBinPath, string &reason)
{
	// reject request if current clip is in oldBinPath
	string binOfCurrentClip = mainWindow->GetCurrentClipHandler()->getCurrentBinPath();
	if (binOfCurrentClip.compare(0, oldBinPath.size(), oldBinPath) == 0)
	{
		reason = "Cannot rename a bin containing an open clip.";
		return -1;
	}
	else
	{
		return 0;
	}
}

lockID_t CNavBinMgrGUIInterface::RequestClipRename(const string &oldBinPath, const string &oldClipName,
	 const string &newBinPath, const string &newClipName, string &reason)
{
	CBinManager binMgr;
	string clipFileName = binMgr.makeClipFileName(oldBinPath, oldClipName);
	if (mainWindow->GetCurrentClipHandler()->isThisCurrentClip(clipFileName))
	{
		reason = "Cannot rename an open clip.";
		return -1;
	}
	else
	{
		return 0;
	}
}

void CNavBinMgrGUIInterface::RelinkClipMedia(ClipIdentifier clipId)
{
   mainWindow->RelinkClipMedia(clipId);
}


void CNavBinMgrGUIInterface::ShowBinManagerGUI(bool show)
{
	CBinMgrGUIInterface::ShowBinManagerGUI(show);
	BinManagerForm->HighlightClip(mainWindow->GetCurrentClipHandler()->getCurrentClipFileNameWithExt(), true);
}

bool CNavBinMgrGUIInterface::RecordListOfClips(const StringList &sl, const SRecordData &BatchData)
{
	return false;
	////TODO return BatchRecordDlg->BatchRecord(sl, BatchData);
}

int CNavBinMgrGUIInterface::CanWeDiscardOrCommit(bool discardFlag, std::string &message)
{
	 return mainWindow->CanWeDiscardOrCommit(discardFlag, {-1, -1}, message);
}

#ifdef _OLD_LUT_CRAP_

void CNavBinMgrGUIInterface::SetBinDisplayLutToDefault(const string &binPath)
{
	CBinManager binManager;

	// we need to refresh the displayer's custom LUT values; first save them...
	SaveClipLUTInfo();

	binManager.setBinDisplayLutChoice(binPath, BIN_DISPLAY_LUT_DEFAULT);

	// ...then reload them.
	LoadClipLUTInfo();
}

void CNavBinMgrGUIInterface::SetBinDisplayLutToCustom(const string &binPath, bool editLUTFirst)
{
	CBinManager binManager;

	if (BinDisplayLUTEditor == 0)
		BinDisplayLUTEditor = new TBinDisplayLUTEditor(mainWindow);

	// we need to refresh the displayer's custom LUT values; first save them...
	SaveClipLUTInfo();

	if (editLUTFirst)
		BinDisplayLUTEditor->Edit(binPath);

	binManager.setBinDisplayLutChoice(binPath, BIN_DISPLAY_LUT_CUSTOM);

	// ...then reload them.
	LoadClipLUTInfo();
}

void CNavBinMgrGUIInterface::SaveClipLUTInfo() {mainWindow->GetCurrentClipHandler()->saveCustomLUT();}

void CNavBinMgrGUIInterface::LoadClipLUTInfo() {mainWindow->GetCurrentClipHandler()->reloadCurrentClipCustomLUT();}
#endif

void CNavBinMgrGUIInterface::FormKeyDown(WORD &Key, TShiftState Shift) {mainWindow->FormKeyDown(nullptr, Key, Shift);}

void CNavBinMgrGUIInterface::FormKeyUp(WORD &Key, TShiftState Shift) {mainWindow->FormKeyUp(nullptr, Key, Shift);}

// This will return false if we don't want a clip to be hacked right now!
bool CNavBinMgrGUIInterface::WeWantToHackAClip()
{
   // This checks the provisional status and makes sure no tool is processing.
   if (IsItSafeToUnloadCurrentClip())
   {
      // For sanity, stop playing
      mainWindow->GetPlayer()->fullStop();
      return true;
   }

   return false;
}

void CNavBinMgrGUIInterface::ClipHasChanged()
{
	CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();
	TRACE_2(errout << "Reload Current Clip called");

   if (BinManagerForm != nullptr)
   {
      BinManagerForm->ClearProxy();
   }

   clipHandler->reloadCurrentClip();

   if (BinManagerForm != nullptr)
   {
      BinManagerForm->ReloadProxy();
   }

	// Need this in case the change is that the alpha matte was removed.
	mainWindow->UpdateRGBChannelsToolbarButtons();
}
// ----------------------------------------------------------------------------

void CNavBinMgrGUIInterface::SaveTimelinePositions() {mainWindow->SaveTimelinePositions();}
// ----------------------------------------------------------------------------

void CNavBinMgrGUIInterface::RestoreTimelinePositions() {mainWindow->RestoreTimelinePositions();}
// ----------------------------------------------------------------------------

string CNavBinMgrGUIInterface::GetRedClipName() {return mainWindow->GetDisplayer()->getImportFrameClipName();}
// ----------------------------------------------------------------------------

void CNavBinMgrGUIInterface::GetCurrentMarks(int &markInIndex, int &markOutIndex)
{
	markInIndex = mainWindow->GetPlayer()->getMarkIn();
	markOutIndex = mainWindow->GetPlayer()->getMarkOut();
}

//////////////////////////////////////////////////////////////////////////////
//
// CNavPDLViewerInterface - Callbacks from PDL Viewers to Navigator
//
//////////////////////////////////////////////////////////////////////////////

class CNavPDLRenderInterface : public CPDLRenderInterface
{
public:
	int GoToPDLEntry(CPDLEntry &pdlEntry);
};

static CNavPDLRenderInterface navPDLRenderInterface; // one global instance

int CNavPDLRenderInterface::GoToPDLEntry(CPDLEntry &pdlEntry) {return mainWindow->GoToPDLEntry(pdlEntry);}

//////////////////////////////////////////////////////////////////////////////

string TmainWindow::reticleUnnamed = "[NEW]";
string TmainWindow::reticleSectionName = "Reticles";
string TmainWindow::defaultReticle[] =
{"[NONE]", "DEFAULT_133_FULL", "DEFAULT_166_FULL", "DEFAULT_178_FULL", "DEFAULT_185_FULL", "DEFAULT_235_FULL",
	"DEFAULT_240_FULL", "DEFAULT_133_ACADEMY", "DEFAULT_166_ACADEMY", "DEFAULT_178_ACADEMY", "DEFAULT_185_ACADEMY",
	"DEFAULT_235_ACADEMY", "DEFAULT_240_ACADEMY"};

//////////////////////////////////////////////////////////////////////////////
//
// TmainWindow - Navigator's Main Form
//
//////////////////////////////////////////////////////////////////////////////

string _StartupFailedMessage;

int TmainWindow::launchAutoUpgrade()
{
   // In case of an install, the default dir is not the release
	string command = "ConvertToQlm11License.exe";
	if (!FileExists(command.c_str()))
	{
		command = "..\\Release\\ConvertToQlm11License.exe";
	}

	SHELLEXECUTEINFO shExInfo = {0};

	shExInfo.cbSize = sizeof(shExInfo);
	shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExInfo.hwnd = 0;

   // This avoids an issue with a version of windows defender
   // IDing this exe as a Trojan virus.
	char tmp[6] = {0,0,0,0,0,0};
	OBFUS_5(tmp, 0x1d, 'r', 'u', 'n', 'a', 's');

	shExInfo.lpVerb = _T("open"); // Operation to perform
	shExInfo.lpFile = _T(command.c_str()); // Application to start
	shExInfo.lpParameters = ""; // Additional parameters
	shExInfo.lpDirectory = 0;
	shExInfo.nShow = SW_SHOW;
	shExInfo.hInstApp = 0;

	auto res = ShellExecuteEx(&shExInfo);
	if (res)
	{
		WaitForSingleObject(shExInfo.hProcess, INFINITE);
		CloseHandle(shExInfo.hProcess);
		return 0;
	}

	return ::GetLastError();
//  return 2;
}

_fastcall TmainWindow::TmainWindow(TComponent* Owner) : TMTIForm(Owner), savedMouseX(-1), savedMouseY(-1),
	 componentCount(0), toolWindowIsActive(0), loadingPDLEntry(false), licenseIsOK(false), _mainWindowState(NormalWindow),
	 restoreRibbonAfterPlaying(false), inTopPanelDragMode(false), swapWAndShiftWFlag(false)
{
	m_savedShiftState.Clear();

	binMgrGUIInterface = 0;
	OldActiveWindow = nullptr;
	SetAutofocus(false);
	ToolbarDockOldHeight = 0;
	inRefreshTimerCallback = false;
	FrameCachePollSerial = -1;

	loupeMagnification = 4;
	oldLoupePos.x = -1;
	oldLoupePos.y = -1;
	inLoupeSettingMode = false;
	aLoupeSettingChanged = false;

	this->UpdateProductAndVersion();

	HANDLE handle = Application->Handle;

	SharedMap::add("ApplicationHandle", (void *)handle);
	StupidHackVTimecodeEdit->SetSharedMapMap(SharedMap::getMap());

	// Check if the license is valid
#ifndef NO_QLM_LICENSING
   // This does all the hard work of looking at the license first
   bool versionValid = false;
   bool previousValid = false;
   MachineIdDatum licenseMachineDatum;
   std::tie(versionValid, previousValid, licenseMachineDatum) = MtiLicenseLib::IsActiveLicensePreCheck(2);

   // Note if previousValid is true versionValid IS ALWAY FALSE
   if (previousValid)
   {
   	MTIostringstream os;

		os << "A previous DRS Nova license has been found" << std::endl;
		os << "In order to upgrade you must be ";
      os << "under maintance, have admin privileges ";
		os << "and either be connected to the internet ";
		os << "or have a smart phone with a QR reader" << std::endl << std::endl;
		os << "Do you wish to upgrade license?";

		if (_MTIConfirmationDialog(os.str()) != MTI_DLG_OK)
		{
			exit(1114);
		}

      if (!MtiLicenseLib::LaunchLicensingWizard(licenseMachineDatum.MachineId, licenseMachineDatum.ActivationKey))
      {
         TRACE_0(errout << "Licensing wizard failed to launch");
  			exit(1115);
      }
   }
	else if (versionValid == false)
	{
		MTIostringstream os;
		// See if an old license
			os << "DRS Nova is not licensed for this computer" << std::endl;
     		os << "To proceed you must have an activation key from MTI Film, have admin privileges, ";
	   	os << "and either be connected to the internet ";
		   os << "or have a smart phone with a QR reader" << std::endl << std::endl;
			os << "Do you wish to continue?";

		if (_MTIConfirmationDialog(os.str()) != MTI_DLG_OK)
		{
			exit(1113);
		}
	}

	// mtiLicense takes care of license activation
	this->_mtiLicense = new MtiLicenseLib(true);

	// The program will terminate and never get here
	_StartupFailedMessage = ""; // Not really necessary
	bool isValid = this->GetMtiLicense()->IsActivated;
	if (!isValid)
	{
		_StartupFailedMessage = "DRS Nova is not licensed on this computer\nPlease enter valid license\n\n";
		_StartupFailedMessage += GetMtiLicense()->ValidateErrorMessage;
		_MTIErrorDialog(Handle, _StartupFailedMessage);
		if (!GetMtiLicense()->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision))
		{
			exit(1111);
		}
	}
	else
	{
		// Yes! THis sucks! Duplicates declarations in Clip3.h AND I think
		// MediaStorage3.h
		const string historySectionName = "History";
		const string enableAutoCleanKey = "EnableAutoClean";

		char mtiCleanName[256];
		memset(mtiCleanName, 0, 256);
		OBFUS_8(mtiCleanName, 0x15, 'M', 'T', 'I', 'C', 'l', 'e', 'a', 'n');

		bool enableAutoClean = GetMtiLicense()->IsFeatureLicensed(mtiCleanName);
		CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
		if (localMachineIniFile != nullptr)
		{
			////////////////// in MTILocalMachine.ini
			//
			// [History]
			// HistoryType=FILE_PER_CLIP      (default, or FILE_PER_FRAME)
			// EnableAutoClean=false          (default)
			//////////////////

			localMachineIniFile->WriteBool(historySectionName, enableAutoCleanKey, enableAutoClean);
			DeleteIniFile(localMachineIniFile);
		}
	}
#endif
}

__fastcall TmainWindow::~TmainWindow(void) {}

// ---------------------------------------------------------------------------
bool TmainWindow::IsFirstStartup()
{
	CIniFile ini(CPMPIniFileName());
	return !ini.SectionExists("mainWindow");
}
// ---------------------------------------------------------------------------

TTimelineFrame * TmainWindow::GetTimeline() {return TimelineFrame;}

// ---------------------------------------------------------------------------

CPlayer * TmainWindow::GetPlayer() {return clipPlayer;}
// ---------------------------------------------------------------------------

void TmainWindow::LoadNextClip()
{
	ClipSharedPtr tempClip;
	string clipName;

	// Create the bin manager if it does not exist
	if (BinManagerForm == nullptr)
		ShowBinManager(false);

	// Just in case we cannot create it
	if (BinManagerForm != nullptr)
	{
		tempClip = GetCurrentClipHandler()->getCurrentClip();
		if (tempClip == nullptr)
			clipName = "";
		else
			clipName = tempClip->getClipFileNameWithExt();

		BinManagerForm->LoadNewClip(clipName, 1);
	}

	return;
}

// ---------------------------------------------------------------------------

void TmainWindow::LoadPreviousClip()
{
	ClipSharedPtr tempClip;
	string clipName;

	// Create the bin manager if it does not exist
	if (BinManagerForm == nullptr)
		ShowBinManager(false);

	// Just in case we cannot create it
	if (BinManagerForm != nullptr)
	{
		tempClip = GetCurrentClipHandler()->getCurrentClip();
		if (tempClip == nullptr)
			clipName = "";
		else
			clipName = tempClip->getClipFileNameWithExt();

		BinManagerForm->LoadNewClip(clipName, -1);
	}
	return;
}

// ---------------------------------------------------------------------------

void TmainWindow::ToggleBetweenLastTwoClips()
{
	string prevBinPath = GetCurrentClipHandler()->getPreviousBinPath();
	string prevClipName = GetCurrentClipHandler()->getPreviousClipName();

	// Create the bin manager if it does not exist
	if (BinManagerForm == nullptr)
		ShowBinManager(false);

	// Just in case we cannot create it
	if ((!prevBinPath.empty()) && (!prevClipName.empty()) && (BinManagerForm != nullptr))
	{
		CBinManager bm;
		string clipPath = bm.makeClipFileNameWithExt(prevBinPath, prevClipName);

		if (!clipPath.empty() && bm.doesClipExist(clipPath))
		{
			// this is just wrong to have to worry about versions vs regular
			// clips here... but I'm not going to fix it now
			if (bm.isVersionClipName(prevClipName))
				BinManagerForm->LoadNewVersion(clipPath);
			else
				BinManagerForm->LoadNewClip(clipPath);
		}
	}
}

// ---------------------------------------------------------------------------
void TmainWindow::LoadNextVersionClip()
{
	// Create the bin manager if it does not exist
	if (BinManagerForm == nullptr)
		ShowBinManager(false);

	// Just in case we cannot create it
	if (BinManagerForm != nullptr)
	{
		string clipName;
		auto tempClip = GetCurrentClipHandler()->getCurrentClip();
      if (tempClip != nullptr)
      {
         clipName = tempClip->getClipFileNameWithExt();
      }

		BinManagerForm->LoadNewVersion(clipName, +1);
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::LoadPreviousVersionClip()
{
	// Create the bin manager if it does not exist
	if (BinManagerForm == nullptr)
		ShowBinManager(false);

	// Just in case we cannot create it
	if (BinManagerForm != nullptr)
	{
		string clipName;
		auto tempClip = GetCurrentClipHandler()->getCurrentClip();
      if (tempClip != nullptr)
      {
         clipName = tempClip->getClipFileNameWithExt();
      }

		BinManagerForm->LoadNewVersion(clipName, -1);
	}
}
// ---------------------------------------------------------------------------

ClipIdentifier TmainWindow::UnloadMasterOrVersionClips(ClipIdentifier clipId)
{
   ClipIdentifier masterClipId = clipId.GetMasterClipId();
   if (UnloadClipIfCurrent(masterClipId.GetClipPath()))
   {
      return masterClipId;
   }

   // Check the versions, if any.
   CBinManager binManager;
   if (binManager.doesClipHaveVersions(masterClipId.GetParentPath(), masterClipId.GetClipName()))
   {
      vector<ClipVersionNumber> versionNumberList;
      binManager.getListOfClipsActiveVersionNumbers(masterClipId, versionNumberList, true, true);
      for (unsigned j = 0; j < versionNumberList.size(); ++j)
      {
         ClipIdentifier versionClipId = masterClipId + versionNumberList[j];
         if (UnloadClipIfCurrent(versionClipId.GetClipPath()))
         {
            return versionClipId;
         }
      }
   }

   ClipIdentifier nullClipId;
   return nullClipId;
}
// ---------------------------------------------------------------------------

bool TmainWindow::UnloadClipIfCurrent(const string& clipFileName)
{
	CBinManager binMgr;

	// Force the clip file name into a standardized format
	string stdClipFileName = binMgr.makeClipFileNameWithExt(clipFileName);

	CNavCurrentClip* currentClipHandler = GetCurrentClipHandler();
	string currentClipFileName = currentClipHandler->getCurrentClipFileNameWithExt();

	if (currentClipFileName != stdClipFileName)
	{
      return false;
   }

   CToolManager toolMgr;
   toolMgr.onChangingClip(); // Notify tools that old clip is being closed

   TRACE_1(errout << "UNLOAD CURRENT CLIP " << currentClipFileName);
   int stat = currentClipHandler->unloadCurrentClip();
   if (stat != 0)
   {
      TRACE_0(errout << "ERROR: Could not unload current clip " << currentClipFileName << endl <<
          "       Return code = " << stat);
      return false;
   }


//   toolMgr.onChangingClip(); // already done above!
   toolMgr.onDeletingOpenedClip();
   BaseToolWindow->DisableTool(TOOL_DISABLED_NO_CLIP);
   UpdateWindowCaption();

   Invalidate();

   return true;
}
// ---------------------------------------------------------------------------

void TmainWindow::RelinkClipMedia(ClipIdentifier relinkClipId)
{
	CToolManager toolMgr;
	if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
   {
      // A change is pending or a tool is running so
      // cannot unload the current clip right now.
		return;
   }

   ClipIdentifier masterClipId = relinkClipId.GetMasterClipId();

   if (ManageMediaForm == nullptr)
   {
      ManageMediaForm = new TManageMediaForm(this);
   }

   CBinManager binManager;
   int retVal = -1;
   auto clip = binManager.openClip(masterClipId, &retVal);
   if (clip == nullptr || retVal != 0)
   {
      TRACE_0(errout << "ERROR: Can't open clip " << masterClipId.GetClipPath() << " (" << retVal << ")");
      return;
   }

   CVideoProxy *videoProxy = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy == nullptr || !videoProxy->isMediaFromImageFile())
   {
      // ERROR
      TRACE_0(errout << "INTERNAL ERROR: Corrupt clip: video proxy is missing!");
      return;
   }

   CVideoFrameList *videoFrameList = videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == nullptr)
   {
      // ERROR
      TRACE_0(errout << "INTERNAL ERROR: Corrupt clip: video frame list is missing!");
      return;
   }

   // This is a hack to get the filename of frame at index 0. We will look for
   // that file in the relinked media folder. Yes, I know I could probably
   // just use that path instead of parsing the media identifier from the
   // media storage, below, or I could construct the file name from the
   // media identifier and the first file number, but I'm being lazy here.
   CFrame *frame0 = videoFrameList->getFrame(0);
   string frame0Filename = GetFileNameWithExt(videoFrameList->getImageFilePath(frame0));

   const CMediaStorageList *mediaStorageList = videoProxy->getMediaStorageList();
   CMediaStorage *mediaStorage_0 = mediaStorageList->getMediaStorage(0);
   string masterClipMediaIdentifier = mediaStorage_0->getMediaIdentifier();
   string masterClipMediaFolder = RemoveDirSeparator(GetFilePath(masterClipMediaIdentifier));

   string versionClipMediaParentFolder = RemoveDirSeparator(GetFilePath(masterClipMediaFolder));
   string versionClipMediaFolderTemplate = GetFileNameWithExt(masterClipMediaFolder) + " (v#)";

   ManageMediaForm->fillIn(masterClipMediaFolder, versionClipMediaParentFolder, versionClipMediaFolderTemplate);
   ManageMediaForm->setFileNameThatMustExistInRelinkedFolder(frame0Filename);

   auto modalResult = ManageMediaForm->ShowModal();
   string mediaFolderPath = RemoveDirSeparator(ManageMediaForm->getMasterClipMediaFolder());
   string versionMediaParentFolderPath = ManageMediaForm->getVersionClipMediaParentFolder();
   if (versionMediaParentFolderPath.empty())
   {
      versionMediaParentFolderPath = RemoveDirSeparator(GetFilePath(mediaFolderPath));
   }

   if (modalResult == mrCancel || mediaFolderPath.empty())
   {
      // "Can't happen".
      return;
   }

   // If the master clip or any of its versions is the current clip, unload it.
   // Remember the unloaded clip so we can reload it after we're done.
   ClipIdentifier unloadedClipId = UnloadMasterOrVersionClips(masterClipId);

   binManager.closeClip(clip);

   // Do the relink!
   retVal = binManager.RelinkMediaFoldersForClipAndItsVersions(masterClipId, mediaFolderPath, versionMediaParentFolderPath);
   if (retVal != 0)
   {
		MTIostringstream os;
		os << "Error: Could not relink clip media for clip " << endl
         << masterClipId.GetClipName() << " or one of its versions";
		_MTIErrorDialog(Handle, os.str());
   }

   ClearFrameCache();

   if (!unloadedClipId.IsEmpty())
   {
      OpenClip(unloadedClipId);
   }
}
// ---------------------------------------------------------------------------

CDisplayer * TmainWindow::GetDisplayer() {return clipDisplayer;}

CNavCurrentClip * TmainWindow::GetCurrentClipHandler() {return currentClipHandler;}

string TmainWindow::GetMasterClipFolder()
{
	string clipPath(GetCurrentClipHandler()->getCurrentClipPath());
	string binPath = RemoveDirSeparator(GetCurrentClipHandler()->getCurrentParentPath());
	CBinManager binMgr;
	if (binMgr.isClipDir(binPath))
	{
		// It's a version clip so go up one level.
		clipPath = binPath;
	}

	return clipPath;
}

CRenderDestinationClip* TmainWindow::GetRenderDestinationClip() {return renderDestinationClip;}

void TmainWindow::InitRenderDestinationClip(int srcornew)
	 //
	 // this implements a very simple convention for rendering
	 // to a NEW clip, the name of the clip being derived from
	 // the source clip name, by appending "_Vnnn", where nnn is
	 // a version number maintained for the clip. Where media con-
	 // sists of image files, the directory for the image files
	 // is derived from that of the source, by appending "_Vnnn".
	 // The file template used is identical to that of the source.
	 //
{
	// valid args are 0 or 1
	if ((srcornew < 0) || (srcornew > 1))
		return;

	char vernum[6];
	string newVersionNumber;
	string srcClipFilename, srcMediaIdentifier, srcImageFilePath, srcImageFileTemplate;
	string dstClipFilename, dstImageFilePath, dstMediaLocation;

	ClipSharedPtr srcClip;
	CClipInitInfo srcClipInitInfo;
	CVideoInitInfo *mainVideoInitInfo;

	switch (srcornew)
	{

	case CLIP_DEST_SRC:

		renderDestinationClip->destType = DEST_CLIP_TYPE_SRC;

		break;

	case CLIP_DEST_NEW:

		renderDestinationClip->destType = DEST_CLIP_TYPE_NEW;

		// the current clip
		srcClip = GetCurrentClipHandler()->getCurrentClip();

		// get a new version number
		sprintf(vernum, "_V%d", srcClip->getNewClipVersion());
		newVersionNumber = vernum;

		// destination clip name is derived from source clip name
		// by appending _Vnnn, where nnn is the next version number
		srcClipFilename = renderDestinationClip->GetSourceClipName();
		dstClipFilename = srcClipFilename + newVersionNumber;
		renderDestinationClip->SetDestinationClip(dstClipFilename);

		// get main video init info for source clip
		srcClipInitInfo.initFromClip(srcClip);
		mainVideoInitInfo = srcClipInitInfo.getMainVideoInitInfo();

		// set up destination media location
		if (!mainVideoInitInfo->isMediaFromImageFile())
		{ // videostore

			// the MediaLocation is the same raw disk as the source
			renderDestinationClip->SetDestinationMediaLocation(srcClipInitInfo.getMediaLocation());
		}
		else
		{ // image files

			srcMediaIdentifier = mainVideoInitInfo->getMediaIdentifier();
			srcImageFilePath = RemoveDirSeparator(GetFilePath(srcMediaIdentifier));
			srcImageFileTemplate = GetFileName(srcMediaIdentifier) + GetFileExt(srcMediaIdentifier);
			srcImageFileTemplate = CImageFileMediaAccess::RemoveFrameNumberFromTemplate(srcImageFileTemplate);

			// destination image file directory is derived from source image
			// file directory by appending _Vnnn, where nnn is the version no.
			dstImageFilePath = srcImageFilePath + newVersionNumber;

			// the MediaLocation is the full image file template
			dstMediaLocation = dstImageFilePath + "\\" + srcImageFileTemplate;
			renderDestinationClip->SetDestinationMediaLocation(dstMediaLocation);

			// make sure the destination clip image file directory exists
			CreateDir(dstImageFilePath.c_str());
		}

		break;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::OpenClipMenuItemClick(TObject *Sender)
{
	// Open the Bin Manager GUI. 2nd arg signifies fsNormal
	ShowBinManager(true);
}
// ---------------------------------------------------------------------------

void TmainWindow::OpenClip(ClipIdentifier clipId)
{
   OpenClipByName(clipId.GetClipPath());
}

void TmainWindow::OpenClipByName(String FileName)
{
   OpenClipByName(StringToStdString(FileName));
}

void TmainWindow::OpenClipByName(const string &fileName)
{
	int stat;

	CToolManager toolMgr;
	if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
		return; // A change is pending or a tool is running so
	// cannot open a clip right now

	// -------------AWFUL HACK------------------
	this->SaveTimelinePositions();

	// -----------------------------------------

	// TRACE_0(errout << "LOAD CLIP " << fileName);

	CBinManager binMgr;

	if (GetCurrentClipHandler()->isThisCurrentClip(fileName))
	{
		// The clip is already open in the Navigator, so we just need to
		// reload the clip, rather than a full open
		if (BinManagerForm != nullptr)
			BinManagerForm->ClearProxy();
		stat = GetCurrentClipHandler()->reloadCurrentClip();
		if (BinManagerForm != nullptr)
			BinManagerForm->ReloadProxy();
		if (stat != 0)
		{
			// Error: Could not reload the clip
			String message = (String)"Could not open clip " + fileName.c_str();
			ShowMessage(message);
			return;
		}

		// Rebuild the cut timeline (mbraca+20041018)
		GetTimeline()->performCutAction(CUT_REFRESH);
		this->RestoreTimelinePositions();
		return; // all done
	}
	else if (binMgr.isClipOpen(fileName))
	{
		// KLUDGE if clip reference counting is activated, this bit of code
		// will no longer do what is supposed to

		// The clip is open, but not in the Navigator.  Close the clip
		// so we can open it from the clip files rather than memory.
		auto tmpClip = binMgr.openClip(fileName, &stat);
		if (stat != 0)
			return;

		tmpClip->updateAllTrackFiles(); // reload field list & time tracks

		stat = binMgr.closeClip(tmpClip);
		if (stat != 0)
			return; // close failed
	}

	// Tease apart FileName to get the Bin Path and the Clip Name
	string binPath, clipName;
	stat = binMgr.splitClipFileName(fileName, binPath, clipName);
	if (stat != 0)
	{
		// Error: Could not split the clip file name into bin & clip name
		String message = (String)"Could not parse clip file name: " + fileName.c_str();
		ShowMessage(message);
		return;
	}

	TRACE_0(errout << "Load clip " << clipName << " from bin " << binPath);

	// Notify tools that the old clip is about to close
	toolMgr.onChangingClip(); // Notify tools that old clip is being closed

	// Hide the Loupe window while the clip is changing;
	bool loupeModeVisible = IsLoupeVisible();
	if (loupeModeVisible)
	{
		HideLoupe();
	}

	// Open the new clip
	stat = GetCurrentClipHandler()->openClipByName(binPath, clipName);

	if (loupeModeVisible)
	{
		ShowLoupe();
	}

	if ((stat != 0) || (HRTimerStarted && !licenseIsOK))
	{
		// Error: Could not open the clip
		TRACE_0(errout << "ERROR: Could not open clip, error code " << stat);
		String message = (String)"Could not open clip " + fileName.c_str();
		ShowMessage(message);
		return;
	}

	// post the new file name as window caption
	UpdateWindowCaption();

	// post the framing view in the window's status bar
	UpdateFramingViewStatus();

	// Notify the Job Manager that the clip is changing
	JobManager jobManager;
	jobManager.SetMasterClipFolderPath(GetMasterClipFolder());
	string clipInString = GetSanitizedTimecodeStringFromFrameIndex(GetPlayer()->getInFrameIndex());
	string clipOutString = GetSanitizedTimecodeStringFromFrameIndex(GetPlayer()->getOutFrameIndex());
	jobManager.SetClipInOutStrings(clipInString, clipOutString);

	// Notify the Bookmark Manager that the clip is changing
	BookmarkManager bookmarkManager;
	bookmarkManager.SetCurrentTimecode(CTimecode::NOT_SET);
   auto currentClip = GetCurrentClipHandler()->getCurrentClip();
	bookmarkManager.SetCurrentClipInfo(GetMasterClipFolder(), currentClip,
		 GetCurrentClipHandler()->getCurrentVideoProxyIndex(), GetCurrentClipHandler()->getCurrentVideoFramingIndex());

	// // Notify the PDL manager of the new place to stash newly created PDLs.
	// // By default, they are placed in the master clip's folder.
	// string masterClipFolder = GetMasterClipFolder();
	// string pdlFolder = jobManager.GetPdlsFolderPath(masterClipFolder);
	// CPDLViewerManager pdlViewerMgr;
	// pdlViewerMgr.SetDefaultPDLFolder(pdlFolder);

	// Remember the clip name in the history menus
	string clipname = GetCurrentClipHandler()->getCurrentClipFileNameWithExt();
	////TODO   RecentClipsTBMRUList->Add(clipname.c_str());
	// fullWindow->HistoryFullMenu->Add(clipname);

	// We need to fill in the "render destination" source clip stuff,
	// even though the "render destination" feature has been disabled
	// TODO: GET RID OF THE RENDER DESTINATION CRAP! QQQ
	renderDestinationClip->SetSourceClip(GetCurrentClipHandler()->getCurrentClipFileName(),
		 GetCurrentClipHandler()->getCurrentVideoProxyIndex(), GetCurrentClipHandler()->getCurrentVideoFramingIndex());

	// Clear the various frame file caches
	// There's NO REASON to do this anymore!
	// MediaFileIO::ClearAllCaches();

	// Notify anyone else interested that the clip has changed
	ClipHasChanged.Notify();

	// Notify tools that there is a new clip
	toolMgr.onNewClip();

	// HACK (re)init the loupe
	LoupeWindow->init(this);

	// Set the attributes of the Cueup timecode in the Navigator Controls window
	if (BinManagerForm != nullptr)
		BinManagerForm->HighlightClip(mainWindow->GetCurrentClipHandler()->getCurrentClipFileNameWithExt(), true);

	InitComponentValuesDisplay();

	// Rebuild the cut timeline (mbraca+20040917)
	GetTimeline()->performCutAction(CUT_REFRESH);

	// Hack to tell the timeline about the animated masks
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();
	CAnimateTimelineInterface *IFPtr = maskTool->GetAnimateTimelineInterface();
	GetTimeline()->SetAnimateTimelineInterface(IFPtr);

	// -------------AWFUL HACK------------------
	this->RestoreTimelinePositions();
	// -----------------------------------------

	// Update RGBA channel toolbar in case alpha of new clip is different
	UpdateRGBChannelsToolbarButtons();

	// and finally...
	FocusOnMainWindow();
}

#define REMOTE_DISPLAY_FORMAT_SAME_AS_CLIP_INDEX 0
#define REMOTE_DISPLAY_FORMAT_YUV_INDEX          1
#define REMOTE_DISPLAY_FORMAT_RGB_INDEX          2

EPixelComponents TmainWindow::GetRemoteDisplayFormatPreference()
{
	EPixelComponents pixelComponents = IF_PIXEL_COMPONENTS_INVALID;

#if 0
	switch (PreferencesDlg->RemoteDisplayFormatRadioGroup->ItemIndex)
	{
	case REMOTE_DISPLAY_FORMAT_SAME_AS_CLIP_INDEX: // 0 is same as clip
		pixelComponents = getPixelComponents();
		break;
	case REMOTE_DISPLAY_FORMAT_YUV_INDEX: // 1 is YUV
		pixelComponents = IF_PIXEL_COMPONENTS_YUV422;
		break;
	case REMOTE_DISPLAY_FORMAT_RGB_INDEX: // 2 is RGB
		pixelComponents = IF_PIXEL_COMPONENTS_RGB;
		break;
	}
#else
	pixelComponents = getPixelComponents();
#endif

	return pixelComponents;
}

int TmainWindow::GetSlowDisplaySpeedPreference() {return PreferencesDlg->SlowSpinEdit->GetValue();}

int TmainWindow::GetMediumDisplaySpeedPreference() {return PreferencesDlg->MediumSpinEdit->GetValue();}

int TmainWindow::GetFastDisplaySpeedPreference() {return PreferencesDlg->FastSpinEdit->GetValue();}

// returns an integer which is 10x the actual gamma
//
int TmainWindow::GetGammaPreference() {return PreferencesDlg->GammaSlider->Position;}

void TmainWindow::UpdateWindowCaption()
{
	// Update the Main Window's caption with the current clip file name
	AnsiString tmpStr = "DRS Nova by MTI Film - ";

	// Add clip name
	// for version clips, need to combine the master clip name with the
	// version name (e.g.  From "/mtishare/mtibins/foo/bar/(v1)" derive
	// the display name "bar (v1)" )
	ClipIdentifier currentClipId(GetCurrentClipHandler()->getCurrentClipFileName());
	string clipName = currentClipId.IsVersionClip() ? currentClipId.GetMasterClipName() + " " + currentClipId.GetClipName
		 () : currentClipId.GetClipName();

	tmpStr += clipName.c_str();

#ifdef _DEBUG
#if _WIN64
	tmpStr += ", 64 bits";
#else
	tmpStr += ", 32 bits";
#endif
#endif

#if _DEBUG
	MEMORYSTATUSEX status;
	status.dwLength = sizeof(status);
	GlobalMemoryStatusEx(&status);
	MTIostringstream os;
	os << " " << std::min(status.ullAvailVirtual, status.ullAvailPhys) / 1024.0 / 1024.0 / 1024.0 << " GB physical available";
	tmpStr += AnsiString(os.str().c_str());
#endif

	// if (!this->m_mtiLicense->IsFeatureLicensed(this->m_licenseBaseName))
	// {
	// tmpStr += ", Error licensing feature: ";
	// tmpStr += this->m_licenseBaseName.c_str();
	// }
	// else
	// {
	// tmpStr += ",  License expires on ";
	// tmpStr += this->m_mtiLicense->ExpirationDateStringPretty().c_str();
	// }

	this->Caption = tmpStr;
}

// ---------------------------------------------------------------

void __fastcall TmainWindow::ExitMenuItemClick(TObject *Sender)
{
	// Close the main window to terminate the application
	Close();
}

// ---------------------------------------------------------------------------
// Internal Tool Information
static const char* internalToolNameTable[] =
{"Navigator", ""}; // terminates with blank name
#define INTERNAL_TOOL_COUNT ((sizeof(internalToolNameTable)/sizeof(char*))-1)

void __fastcall TmainWindow::FormCreate(TObject *Sender)
{
	int iRet;

	Application->OnActivate = AppOnActivate;
	Application->OnDeactivate = AppOnDeactivate;

	// Check to see the system ID is correct
	// THE CHECK WAS MOVED TO StartupDelayTimer()

	// load a DROPPER cursor (color picker). Index crDropper = 1
	Screen->Cursors[crDropperIndex] = (HICON)LoadCursorFromFile("Dropper.cur");
	// load a BUCKET  cursor (color picker). Index crBucket  = 2
	Screen->Cursors[crBucketIndex] = (HICON)LoadCursorFromFile("Bucket.cur");
	// load a WHITE CROSS cursor (edit mask) Index crWhCross = 3
	Screen->Cursors[crWhCrossIndex] = (HICON)LoadCursorFromFile("WhCross.cur");

	// the screen cursor will be set to an arrow when
	// SetCursor is called by the preferences unit
	// in ReadSettings (CursorCBoxClick(nullptr))
	Screen->Cursor = crHourGlass;
	Application->ProcessMessages();
	INIT_ERROR_HANDLER

		 // Initialize user-input event handler pointers in Navigator's System API
		 // in class CNavSystemInterface with  Main Window's event handler functions.
		 // These pointers are later used by Tools to redirect the user input
		 // events they receive to the Main Windows event handlers
		 CNavSystemInterface::setActiveWindow(this);
	CNavSystemInterface::setKeyDownEventHandler(FormKeyDown);
	CNavSystemInterface::setKeyUpEventHandler(FormKeyUp);
	CNavSystemInterface::setMouseButtonDownEventHandler(FormMouseDown);
	CNavSystemInterface::setMouseButtonUpEventHandler(FormMouseUp);
	CNavSystemInterface::setMouseMoveEventHandler(FormMouseMove);
	////TODO   CNavSystemInterface::setMouseWheelEventHandler(FormMouseWheel);

	// Create interface to the Bin Manager GUI
	binMgrGUIInterface = new CNavBinMgrGUIInterface;

	// Init interface to PDL Viewers
	CPDLViewerManager pdlViewerMgr;
	pdlViewerMgr.SetOwner(Application); // set owner of PDL Viewer forms
	pdlViewerMgr.SetAppInterface(&navPDLRenderInterface);
	pdlViewerMgr.SetMainWinKeyDownEventHandler(FormKeyDown);
	pdlViewerMgr.SetMainWinKeyUpEventHandler(FormKeyUp);

	clipDisplayer = new CDisplayer();
	clipDisplayer->setMainWindowPtr(mainWindow);
	SET_CBHOOK(DisplayerRectanglesChangedCB, CDisplayer::DisplayRectanglesChanged);

	CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
	int numberOfDiskReaderThreads = DEFAULT_DISK_READER_THREAD_COUNT;
	if (localMachineIniFile != nullptr)
	{
		numberOfDiskReaderThreads = localMachineIniFile->ReadInteger("General", "NumberOfDiskReaderThreads",
			 numberOfDiskReaderThreads);
	}

	DeleteIniFile(localMachineIniFile);

	clipPlayer = new CPlayer(numberOfDiskReaderThreads);
	clipPlayer->SetMainWindowPtr(mainWindow);
	SET_CBHOOK(SetFrameCacheHints, clipPlayer->MarksHaveChanged);
	SET_CBHOOK(SetFrameCacheHints, clipPlayer->CurrentTC);

	// init the automatic refresh control flags - REPLACED WITH TIMER
	// frameCyclesElapsed = 0;
	// frameCntEnable     = 0;
	forceFrameDisplayPendingFlag = false;

	// init the actual FPS modulus
	actualFPSModulus = 0;

	currentClipHandler = new CNavCurrentClip();

	renderDestinationClip = new CRenderDestinationClip;

	// MainWindowUnit maintains its own screen device
	// context and its own rendering context
	if (clipDisplayer->getGraphicsDeviceContexts(DisplayPanel->Handle, screenDC, renderRC) != 0)
	{
		ShowMessage("Clip Displayer has failed.\n" "Please make sure that your display "
			 "is set to 32 bits per pixel (highest quality)");
		exit(0); // TBD Exit properly with a Close()
	}

	// Initialize the Tool Manager with the Navigator's internal tools
	InitToolManager();

	// Set Tags in Display Devices submenu buttons
	LocalDisplayButton->Tag = DISPLAY_TO_WINDOW;
	RemoteDisplayButton->Tag = DISPLAY_TO_MONITOR;
	BothDisplayButton->Tag = DISPLAY_TO_BOTH;

	// Set Tags in Display Modes submenu buttons
	// TrueViewMenuItem->Tag = DISPLAY_MODE_TRUE_VIEW;
	AspectSubmenuItem->Tag = DISPLAY_MODE_TRUE_VIEW;
	OneToOneMenuItem->Tag = DISPLAY_MODE_1_TO_1;
	FitWidthMenuItem->Tag = DISPLAY_MODE_FIT_WIDTH;

	// Set Tags in Display Filter submenu buttons
	NoneMenuItem->Tag = PLAYBACK_FILTER_NONE;
	OriginalValuesMenuItem->Tag = PLAYBACK_FILTER_ORIGINAL_VALUES;
	HighlightFixesMenuItem->Tag = PLAYBACK_FILTER_HIGHLIGHT_FIXES;
	BothMenuItem->Tag = PLAYBACK_FILTER_BOTH;

	// Load the positions of all Toolbar2000 toolbars used on this form
	RestoreToolbars(this, MAIN_WINDOW_PREFIX);

	LoadMainIcons();

	ReadReticleListIni();

	lutSettingsManager = new CLutSettingsManager;
	SET_CBHOOK(LutSettingsHaveChanged, lutSettingsManager->LutSettingsChange);

	// This action suppresses the background fill, so reduces flashing in
	// the case where the background is always toitally obscured.
	DisplayPanel->ControlStyle << csOpaque;
	FrameBufferErrorCoverUpPanel->ControlStyle << csOpaque;
	FrameBufferErrorCoverUpPanel->Left = 0;
	FrameBufferErrorCoverUpPanel->Top = 0;
	FrameBufferErrorCoverUpPanel->Width = DisplayPanel->Width;
	FrameBufferErrorCoverUpPanel->Height = DisplayPanel->Height;
	FrameBufferErrorDisplayPanel->ControlStyle << csOpaque;
	// MainWindowTopPanel->ControlStyle << csOpaque;      Looks ugly when resizing the panel
	MainWindowBottomPanel->ControlStyle << csOpaque;
	LeftFrameClipNamePanel->ControlStyle << csOpaque;
	RightFrameClipNamePanel->ControlStyle << csOpaque;

	SET_CBHOOK(OnFrameWasDiscardedOrCommitted, CBinManager::FrameWasDiscardedOrCommittedEvent);
	SET_CBHOOK(OnClipWasDeleted, CBinManager::ClipWasDeletedEvent);

	_unmaximizedNormalWindowBounds = BoundsRect;
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormDestroy(TObject *Sender)
{
	REMOVE_CBHOOK(DisplayerRectanglesChangedCB, CDisplayer::DisplayRectanglesChanged);

	// REMOVED TO AVOID CRASH ON EXIT
	// delete currentClipHandler;    // BEFORE PLAYER!
	//
	// delete clipPlayer;
	//
	// delete clipDisplayer;
	//
	// delete renderDestinationClip;

	// necessary because of WINDOWS calling of RESIZE after DESTROY
	OnResize = nullptr;

	// Destroy the Bin Manager GUI Interface if one has been created
	if (binMgrGUIInterface != 0)
	{
		delete binMgrGUIInterface;
		binMgrGUIInterface = 0;
	}

	// MOvED THIS TO FormCloseQuery()
	// // Need to let MediaFrameFileReader clean up caches
	// MediaFileIO::Shutdown();

	delete lutSettingsManager;
	lutSettingsManager = nullptr;
}

// ---------------------------------------------------------------------------
void __fastcall TmainWindow::FormPaint(TObject *Sender)
{
	// Give the displayer the visible part of the client rectangle.
	RECT screenRect = GetUsableClientRect();

	int prefBorderWidth = (PreferencesDlg->BorderWidthSlider == nullptr) ? DEFAULT_BORDER_WIDTH :
		 PreferencesDlg->BorderWidthSlider->Position;

	clipDisplayer->setVisiblePartOfClientRectangle(&screenRect, prefBorderWidth, BORDER_GRAY);

	// If the background flag is not cocked, cock it now and refresh the frame
	// (if it is cocked we are probably resizing, so that is taking care of the
	// refresh)
	if (!clipDisplayer->getBgndFlag())
	{

		// see method RefreshTimerCallback
		clipDisplayer->setBgndFlag(true);

		// ask for the last frame again
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormHide(TObject *Sender)
{
	// turn off the ComponentValuesDisplayTimer
	// REPLACED BY CALLBACK FROM PLAYER'S MULTIMEDIA TIMER
	// ComponentValuesDisplayTimer->Enabled = false;

	// Kill the loupe window - I don't feel like dealing with it
	// LoupeWindow->deactivate();    oops - might be gone
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight,
			 bool &Resize)
{
#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("-------------FormCanResize-------------");
   DBTRACE(Left);
   DBTRACE(Top);
   DBTRACE(Left + Width);
   DBTRACE(Top + Height);
   DBTRACE(Width);
   DBTRACE(Height);
   DBTRACE(NewWidth);
   DBTRACE(NewHeight);
#endif

	// This code is to make sure when we exit from wsMaximized state we go back
   // to the geometry of the window at the time just before the window was
   // maximized. The window manager gets confused by our resizing the maximized
   // window to keep clear of the taskbar.
	if (WindowState != wsMaximized)
	{
		if (_isNormalWindowMaximized)
		{
			_isNormalWindowMaximized = false;
         if (_mainWindowState == MainWindowState::NormalWindow)
         {
            // This is the resize resulting from the window being unmaximized.
            // Restore the stashed unmaximized window geometry.
            _newWindowBoundsRect = _unmaximizedNormalWindowBounds;
				_needToChangeWindowBoundsRect = true;

#ifdef DEBUG_MAXIMIZE
				DBCOMMENT("-------------EXIT maximized state-------------");
				DBTRACE(_newWindowBoundsRect.Left);
            DBTRACE(_newWindowBoundsRect.Top);
            DBTRACE(_newWindowBoundsRect.Right);
            DBTRACE(_newWindowBoundsRect.Bottom);
#endif
			}
		}
      else if (_mainWindowState == MainWindowState::NormalWindow)
		{
         // For all other resizes while unmaximized and not in "fullscreen" or
         // "presentation" mode, just update the stored unmaximized geometry.
			_unmaximizedNormalWindowBounds = { Left, Top, Left + NewWidth, Top + NewHeight };

#ifdef DEBUG_MAXIMIZE
			DBCOMMENT("-------------NORMAL RESIZE-------------");
			DBTRACE(_unmaximizedNormalWindowBounds.Left);
         DBTRACE(_unmaximizedNormalWindowBounds.Top);
         DBTRACE(_unmaximizedNormalWindowBounds.Right);
         DBTRACE(_unmaximizedNormalWindowBounds.Bottom);
#endif
		}

		return;
	}

	////////////////////////////////////////////////////////////////////////////
	// The window is maximized. The following code makes sure that it doesn't go
	// underneath the taskbar. For some stupid reason, we are always told to
	// use the full size of the monitor, regardless of the state of the taskbar!
	////////////////////////////////////////////////////////////////////////////

	if (_isNormalWindowMaximized == false)
	{
#ifdef DEBUG_MAXIMIZE
		DBCOMMENT("================================================");
		DBCOMMENT("================ MAXIMIZING NOW ================");
		DBCOMMENT("================================================");
#endif
		_isNormalWindowMaximized = true;
	}
	else
	{
		// If the taskbar was just auto-hidden while we are maximized, our window
		// will end up missing the piece that is where the taskbar was, so fire
		// up a timer to fix that after the taskbar completely autohides (5 secs)!
		static bool taskbarWasAutoHidden = false;
		auto taskbarIsAutoHidden = isTaskbarAutoHidden();
		if (taskbarIsAutoHidden && !taskbarWasAutoHidden)
		{
#ifdef DEBUG_MAXIMIZE
			DBCOMMENT("Taskbar was just auto-hidden, so initiate hackery");
#endif
			TaskbarAutohideHackTimer->Enabled = true;
		}

		taskbarWasAutoHidden = taskbarIsAutoHidden;
	}

   // Get the "work area" of the window's monitor, which excludes the taskbar
   // area if the taskbar isn't hidden. We need to set the window geometry
   // to this work area ourselves to have the window avoid the taskbar area.
	auto monitorHandle = MonitorFromWindow(Handle, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO monitorInfo;
	monitorInfo.cbSize = sizeof(MONITORINFO);
	auto success = GetMonitorInfo(monitorHandle, &monitorInfo);
	if (!success)
   {
      TRACE_0(errout << "ERROR: Can't read monitor info!");
      return;
	}

#ifdef DEBUG_MAXIMIZE
	static int OLD_rcWork_left = -1;
	static int OLD_rcWork_top = -1;
	static int OLD_rcWork_right = -1;
	static int OLD_rcWork_bottom = -1;
	static int OLD_rcMonitor_left = -1;
	static int OLD_rcMonitor_top = -1;
	static int OLD_rcMonitor_right = -1;
	static int OLD_rcMonitor_bottom = -1;
	if (OLD_rcWork_left != monitorInfo.rcWork.left
	|| OLD_rcWork_top != monitorInfo.rcWork.top
	|| OLD_rcWork_right != monitorInfo.rcWork.right
	|| OLD_rcWork_bottom != monitorInfo.rcWork.bottom
	|| OLD_rcMonitor_left != monitorInfo.rcWork.left
	|| OLD_rcMonitor_top != monitorInfo.rcWork.top
	|| OLD_rcMonitor_right != monitorInfo.rcWork.right
	|| OLD_rcMonitor_bottom != monitorInfo.rcWork.bottom)
	{
		DBCOMMENT("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		DBCOMMENT("%%%%%%%%%%%%%%%% MONITOR RECT CHANGED! &&&&&&&&&&&&&&&&&&");
		DBCOMMENT("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		DBTRACE(monitorInfo.rcWork.left);
		DBTRACE(monitorInfo.rcWork.top);
		DBTRACE(monitorInfo.rcWork.right);
		DBTRACE(monitorInfo.rcWork.bottom);
		DBTRACE(monitorInfo.rcMonitor.left);
		DBTRACE(monitorInfo.rcMonitor.top);
		DBTRACE(monitorInfo.rcMonitor.right);
		DBTRACE(monitorInfo.rcMonitor.bottom);
		OLD_rcWork_left = monitorInfo.rcWork.left;
		OLD_rcWork_top = monitorInfo.rcWork.top;
		OLD_rcWork_right = monitorInfo.rcWork.right;
		OLD_rcWork_bottom = monitorInfo.rcWork.bottom;
		OLD_rcMonitor_left = monitorInfo.rcWork.left;
		OLD_rcMonitor_top = monitorInfo.rcWork.top;
		OLD_rcMonitor_right = monitorInfo.rcWork.right;
		OLD_rcMonitor_bottom = monitorInfo.rcWork.bottom;
	}
#endif

	// Determine the "bleed" of the window, which is how much it extends past
	// each edge of the monitor in order to hide the borders.
	auto windowEdgeBleedSize = std::max<int>(0, (Width - ClientWidth) / 2);

	// Compute and stash the desired geometry for the window, taking into
	// consideration the location of the taskbar. We will actually set it later
	// after we get the FormResize callback.
	auto newLeft = monitorInfo.rcWork.left - windowEdgeBleedSize;
	auto newTop = monitorInfo.rcWork.top - windowEdgeBleedSize;
	auto newRight = monitorInfo.rcWork.right + windowEdgeBleedSize;
	auto newBottom = monitorInfo.rcWork.bottom + windowEdgeBleedSize;
	auto correctMaximizedWidth = newRight - newLeft;
	auto correctMaximizedHeight = newBottom - newTop;

	_newWindowBoundsRect = { newLeft, newTop, newRight, newBottom };

	_needToChangeWindowBoundsRect = false;

	// If the WORK AREA matches the MONITOR AREA, do NOT try to resize anything
	// because the task bar isn't visible!
	if (monitorInfo.rcWork.left == monitorInfo.rcMonitor.left
	&& monitorInfo.rcWork.top == monitorInfo.rcMonitor.top
	&& monitorInfo.rcWork.right == monitorInfo.rcMonitor.right
	&& monitorInfo.rcWork.bottom == monitorInfo.rcMonitor.bottom
	&& NewWidth == correctMaximizedWidth
	&& NewHeight == correctMaximizedHeight)
	{
#ifdef DEBUG_MAXIMIZE
		DBCOMMENT("Work area matches monitor area.");
#endif
	}
	else if (NewWidth == correctMaximizedWidth && NewHeight == correctMaximizedHeight)
	{
#ifdef DEBUG_MAXIMIZE
		DBCOMMENT("Width and height match!");
#endif
		_needToChangeWindowBoundsRect = false;
	}
	else if (NewWidth == (correctMaximizedWidth + 2) && NewHeight == (correctMaximizedHeight + 2))
   {
		// HACK!!!
		// Sometimes the system tries to resize to dimensions that are ONE PIXEL
		// out on each side (i.e. width and height are 2 more than we expected.
		// Let's not fight that, it's a losing battle.
		_needToChangeWindowBoundsRect = false;
#ifdef DEBUG_MAXIMIZE
      DBCOMMENT("Close enough for government work!");
		DBTRACE(correctMaximizedWidth);
		DBTRACE(correctMaximizedHeight);
#endif
	}
	else
	{
		_needToChangeWindowBoundsRect = true;
	}

#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("-------------MAXIMIZED RESIZE-------------");
   DBTRACE(_needToChangeWindowBoundsRect);
	DBTRACE(_newWindowBoundsRect.Left);
	DBTRACE(_newWindowBoundsRect.Top);
	DBTRACE(_newWindowBoundsRect.Right);
	DBTRACE(_newWindowBoundsRect.Bottom);
#endif

	// DO NOT DELETE THIS DEAD CODE. I've kept it for documentary purposes.
	// This screws up massively - window gets chopped off on the side next to task bar!!
	//	NewWidth = workAreaRight + windowEdgeBleedSize - newLeft;
	//	NewHeight = workAreaBottom + windowEdgeBleedSize - newTop;

	// We now call FormResize() here because FormCanResize() is ALWAYS called
	// when the taskbar changes, but FormResize() IS NOT!!
	FormResize(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TmainWindow::FormConstrainedResize(TObject *Sender, int &MinWidth,
			 int &MinHeight, int &MaxWidth, int &MaxHeight)
{
// DO NOT DELETE THIS DEAD CODE. I've kept it for documentary purposes.
// This screws up massively - window gets chopped off on the side next to task bar!!
//
//	if (_isMainWindowMaximized)
//	{
//		MaxWidth = _newWindowRight - _newWindowLeft;
//		MaxHeight = _newWindowBottom - _newWindowTop;
//	}
}
//---------------------------------------------------------------------------

// NOTE: In addition to the normal callback, FormResize gets called directly
// from FormCanResize because FormCanResize is called  automatically when the
// taskbar changes even if the window Width and Height aren't changing, but
// FormResize is not!!
void __fastcall TmainWindow::FormResize(TObject *Sender)
{
#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("-------------FormResize-------------");
#endif

	// Let's try to cut down of flashing by delaying the actual update of
   // the window contents.
	ResizeHackTimer->Enabled = false;
	ResizeHackTimer->Interval = 250;
	ResizeHackTimer->Enabled = true;

	// Kill the loupe window - I don't feel like dealing with it
	if (LoupeWindow != nullptr)
	{
		LoupeWindow->deactivate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TmainWindow::ResizeHackTimerTimer(TObject *Sender)
{
	ResizeHackTimer->Enabled = false;
#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("xxx ResizeHackTimerTimer xxx");
#endif

	if (_needToChangeWindowBoundsRect)
	{
		_needToChangeWindowBoundsRect = false;
		BoundsRect = _newWindowBoundsRect;
	}
}
//---------------------------------------------------------------------------

void __fastcall TmainWindow::TaskbarAutohideHackTimerTimer(TObject *Sender)
{
	TaskbarAutohideHackTimer->Enabled = false;

	auto boundsRectToSet = _newWindowBoundsRect;
	boundsRectToSet.Bottom -= 1;
	BoundsRect = boundsRectToSet;
	boundsRectToSet.Bottom += 1;
	BoundsRect = boundsRectToSet;
}
//---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelResize(TObject *Sender)
{
#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
	DBCOMMENT("MMMMMMMMMMMMMMMMMM DisplayPanelResize MMMMMMMMMMMMM");
	DBCOMMENT("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
#endif

	if (IsWindowVisible(DisplayPanel->Handle) == false)
	{
		return;
	}

	// If we need to change the window geometry for any reason and what we want
   // to change it to does not match the current window geometry, change it
   // here and just exit because we'll get back here later.
#ifdef DEBUG_MAXIMIZE
	DBTRACE(Left);
	DBTRACE(Top);
	DBTRACE(Width);
	DBTRACE(Height);
#endif

	initializeTheDisplayer();

	// Zoom level will change if in trueview
	UpdateZoomLevelStatus();
}
//---------------------------------------------------------------------------

void TmainWindow::initializeTheDisplayer()
{
#ifdef DEBUG_MAXIMIZE
	DBCOMMENT("-------------initializeTheDisplayer-------------");
#endif

	// Uggh, WINDOWS may call RESIZE after DESTROY (!)
	if (clipDisplayer == nullptr)
	{
		return;
	}

	// Uggh, we may be called here before the MainWindow::FormCreate()!
	if (screenDC == nullptr || renderRC == nullptr)
	{
		return;
	}

	// Get the current border width.

	// load the window's display contexts into the Displayer
#ifdef DEBUG_MAXIMIZE
	DBTRACE(DisplayPanel->ClientWidth);
	DBTRACE(DisplayPanel->ClientHeight);
#endif
	clipDisplayer->initDisplayer(DisplayPanel->Handle, screenDC, renderRC, DisplayPanel->ClientWidth, DisplayPanel->ClientHeight);

	// give the displayer the visible part of the client rectangle.
	RECT screenRect = GetUsableClientRect();

#ifdef DEBUG_MAXIMIZE
	DBTRACE(screenRect.left);
   DBTRACE(screenRect.top);
	DBTRACE(screenRect.right);
   DBTRACE(screenRect.bottom);
#endif

	int prefBorderWidth = (PreferencesDlg == nullptr || PreferencesDlg->BorderWidthSlider == nullptr)
									? DEFAULT_BORDER_WIDTH
									: PreferencesDlg->BorderWidthSlider->Position;
	clipDisplayer->setVisiblePartOfClientRectangle(&screenRect, prefBorderWidth, BORDER_GRAY);

	// See method RefreshTimerCallback
	clipDisplayer->setBgndFlag(true);
}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateSliderAndTimecode()
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> Enter UpdateSliderAndTimecode  <x>x<x>x<x>x<x>");
	GetTimeline()->UpdateTimelineSlider();
	////TRACE_3(errout << "<x>x<x>x<x>x<x> After UpdateTimelineSlider  <x>x<x>x<x>x<x>");
	GetTimeline()->UpdateCurTimecode();
	////TRACE_3(errout << "<x>x<x>x<x>x<x> After UpdateCurTimecode(" << tmpTCStr << ") <x>x<x>x<x>x<x>");
}

// ---------------------------------------------------------------------------
//
// F R A M E  D I S P L A Y (invoked from Player multimedia timer)
// Returns TRUE if a frame was displayed, else FALSE

bool TmainWindow::RefreshTimerCallback()
{
	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB ENTER ^v^v^v^v^v^v^v^v");

	// Skip this iteration if the previous callback hasn't finished yet!
	// I believe this isn't needed any more because it is no longer
	// possible to be reentered here
	if (inRefreshTimerCallback)
	{
		TRACE_3(errout << "RefreshTimerCallback skipped a cycle!!!");
		return false;
	}
	inRefreshTimerCallback = true;

	// ************************ DEBUG CODE *********************
	CHRTimer traceTimer;
	double timeNow;
	static double prevTime = 0.0;
	double times[10] =
	{0.0};

	if (!HRTimerStarted)
	{
		HRTimer.Start();
		HRTimerStarted = true;
	}
	else
	{
		double timeNow = HRTimer.Read() / 1000.0; // convert to seconds
		double actualInterval = timeNow - HRLastEnterTime;
		\
 HRLastEnterTime = timeNow;

		times[0] = actualInterval * 1000.0;
		prevTime = traceTimer.Read();

		++HREnterCount;
		HRMaxInterval = std::max<double>(HRMaxInterval, actualInterval);

		if ((timeNow - HRLastTraceTime) > 5.0)
		{
			double totalElapsed = timeNow - HRLastTraceTime;
			double average = totalElapsed / HREnterCount;
			HRLastTraceTime = timeNow;

			int elapsedSecs = (int) totalElapsed;
			int elapsedFrac = ((int)(totalElapsed * 1000)) % 1000;
			int averageSecs = (int) average;
			int averageFrac = ((int)(average * 1000)) % 1000;
			int maxSecs = (int) HRMaxInterval;
			int maxFrac = ((int)(HRMaxInterval * 1000)) % 1000;
			TRACE_3(errout << "REFRESH STATS:" << setw(1) << " elapsed=" << elapsedSecs << "." << setw(3) << setfill('0')
				 << elapsedFrac << setw(1) << " count=" << HREnterCount << setw(1) << " avg=" << averageSecs << "." << setw
				 (3) << setfill('0') << averageFrac << setw(1) << " max=" << maxSecs << "." << setw(3) << setfill('0')
				 << maxFrac);
			HREnterCount = 0;
			HRMaxInterval = 0.0;
		}
	}
	// ************************ DEBUG CODE ********************

	timeNow = traceTimer.Read();
	times[1] = timeNow - prevTime;
	prevTime = timeNow;

	// This is checked in ShowBinManager
#ifndef NO_QLM_LICENSING
	licenseIsOK = GetMtiLicense()->IsBaseValid;
#else
	licenseIsOK = true;
#endif

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 1 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// Monitor the status of tool processing
	CToolManager toolMgr;
	toolMgr.Heartbeat();

	timeNow = traceTimer.Read();
	times[2] = timeNow - prevTime;
	prevTime = timeNow;

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 2 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// // HACK - Monitor frame cache changes
	// if (FrameCachePollSerial != CImageFileIO::GetFrameCacheSerialNumber())
	// {
	// FrameCachePollSerial = CImageFileIO::GetFrameCacheSerialNumber();
	// FrameCacheHasChanged.Notify();
	// }

	// if the bgnd flag is cocked, begin bgnd sequence
	if (clipDisplayer->getBgndFlag())
	{
		// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 3 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

		// uncock the bgnd flag
		clipDisplayer->setBgndFlag(false);

		if (!forceFrameDisplayPendingFlag)
		{
			// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 3a ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

			// this call is a NO-OP if no clip is loaded
			clipDisplayer->displayBackground();

			// Reset the timer and set the "pending" flag.
			forceFrameDisplayPendingFlag = true;
			forceFrameDisplayPendingTimer.Start();

			// QQQ why do we return here instead of continuing?
			inRefreshTimerCallback = false;
			return false;
		}
	}

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 4 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// Look for the next frame to display.
	MediaFrameBufferSharedPtr frameBuffer = clipPlayer->getNextDisplayFrame();

	timeNow = traceTimer.Read();
	times[3] = timeNow - prevTime;
	prevTime = timeNow;

	if (!frameBuffer)
	{
		// THe desired frame is late.
		// Force refresh of current frame if it's been a tenth of a second since
		// the first request to do so and we haven't already done it.
		// QQQ Not sure of the reason for this, maybe keeps the displayer happy...
		if (forceFrameDisplayPendingFlag && forceFrameDisplayPendingTimer.Read() > 100)
		{
			// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 4a ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

			// We've hit the threshold. When we've gone this long without getting
			// the desired frame, "it's time to ask for one and direct it to the window",
			// whatever that means...

			// refresh the current frame
			clipPlayer->refreshFrame();

			forceFrameDisplayPendingFlag = false;
		}

		// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB ABORT ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

		inRefreshTimerCallback = false;
		return false; // false because getNextDisplayFrame() failed
	}

	forceFrameDisplayPendingFlag = false;

	if (clipPlayer->monitorDisplayIsEnabled())
	{
		// MONITOR enabled
		// display the frame on the video monitor
		clipPlayer->outputVideo();
	}

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 5 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// update the slider & timecode in any case
	UpdateSliderAndTimecode();

	timeNow = traceTimer.Read();
	times[4] = timeNow - prevTime;
	prevTime = timeNow;

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 6 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	if (clipPlayer->getLastFrameSync())
	{
		// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 6a ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");
		clipPlayer->oneFrameToWindow(frameBuffer, clipPlayer->getLastFrameIndex(), false, true);
	}
	else
	{
		if (clipPlayer->windowDisplayIsEnabled())
		{
			// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 6b ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");
			bool pan = clipDisplayer->isPanning();
			bool fast = (clipPlayer->getSubsampledPlayback() && !pan) || (clipPlayer->getSubsampledPanning() && pan);

			clipPlayer->oneFrameToWindow(frameBuffer, clipPlayer->getLastFrameIndex(), fast, true);
		}
	}

	timeNow = traceTimer.Read();
	times[5] = timeNow - prevTime;
	prevTime = timeNow;

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB 7 ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// If REMOTE MONITOR is called for, set it up here
	// The 'initialDisplayMode' is set by 'loadVideoClip'
	// in Player.cpp. Bugzilla 2477
	//
	if (clipPlayer->getInitialDisplayMode() & DISPLAY_TO_MONITOR)
	{
		// forces init of DVS card
		clipPlayer->setDisplayMode(clipPlayer->getInitialDisplayMode(), true);

		// no longer needed
		clipPlayer->setInitialDisplayMode(0);
	}

	// advance the ring buffer
	clipPlayer->displayFrameDone();

	timeNow = traceTimer.Read();
	times[6] = timeNow - prevTime;
	prevTime = timeNow;

	if (times[0] > 100.0 || times[1] > 100.0 || times[2] > 100.0 || times[3] > 100.0 || times[4] > 100.0 || times[5] >
		 100.0 || times[6] > 100.0)
	{
		TRACE_3(errout << "RTCB TIMES: 0=" << times[0] << ", 1=" << times[1] << ", 2=" << times[2] << ", 3=" << times[3]
			 << ", 4=" << times[4] << ", 5=" << times[5] << ", 6=" << times[6]);
	}

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v RTCB EXIT ^v^v^v^v^v^v^v^v @ " << traceTimer.Read() << " msecs");

	// security crap - make it hard to get into bin manager if not licensed
	OpenClipMenuItem->Enabled = licenseIsOK;
	BinManagerMenuItem->Enabled = licenseIsOK;

	inRefreshTimerCallback = false;
	return true;
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::LoadToolsMenuItemClick(TObject *Sender)
{
	// Create plug-in list
#ifndef NO_QLM_LICENSING
	if (!GetMtiLicense()->IsBaseValid)
	{
		return;
	}
#endif

	CToolManager toolManager;
	String P = ExtractFilePath(Application->ExeName) + "Plugins\\*.dll";
	TRACE_1(errout << "Loading Tools from " << StringToStdString(P));
	int retVal = toolManager.loadPlugins(StringToStdString(P).c_str(), Application);

	if (retVal != 0)
	{
		// Error: could not load tools
		Application->MessageBox(L"Could Not Load Plugin Tools", nullptr, MB_OK);
		return;
	}

	int toolCount = toolManager.getToolCount();
	TRACE_1(errout << "Tool Count " << toolCount);

	// NOTE: BaseToolWindow->CoverUp(false) will be done when first tool is "shown"
	BaseToolWindow->CoverUp(true);

	int pluginCount = 0;
	for (int toolIndex = 0; toolIndex < toolCount; ++toolIndex)
	{
		// Initialize each plugin tool and update the green lights on the
		// base tool window cover-up panel
		if (toolManager.isPluginTool(toolIndex))
		{
			string name = toolManager.getToolName(toolIndex);
			TRACE_2(errout << "Initialize " << name << " tool");

			toolManager.initTool(toolIndex);
			BaseToolWindow->ShowLoadProgress(pluginCount++);
		}
	}
}

// ---------------------------------------------------------------------------

void TmainWindow::ShowBinManager(bool show)
{
	// Open the Bin Manager GUI.
	binMgrGUIInterface->ShowBinManagerGUI(show && licenseIsOK);
}

void TmainWindow::ShowTool(int newToolIndex)
{
	int retVal;
	MTIostringstream ostr;

	CToolManager toolMgr;
	if (toolMgr.getActiveToolIndex() != newToolIndex && !HideAllToolsQuery())
		return; // Cannot close a tool, so don't open a new one

	// First hide any tools that are showing
	HideAllTools(newToolIndex);

	// Clear the tool's message panel in the status bar, just in case
	// a new message is not set
	mainWindow->GetTimeline()->DRSMessageLabel->Caption = "";

	if (newToolIndex >= 0)
	{
		retVal = toolMgr.showTool(newToolIndex);
		if (retVal != 0)
		{
			ostr << "Could not show tool " << toolMgr.getToolName(newToolIndex)
				 << " (" << newToolIndex << ")" << endl << "Error code: " << retVal;
			_MTIErrorDialog(Handle, ostr.str());
		}
	}

	ShowActiveToolName();
}

void TmainWindow::HideTool(int toolIndex)
{
	int retVal;
	MTIostringstream ostr;

	CToolManager toolMgr;

	if (toolIndex >= 0)
	{
		if (toolMgr.hideToolQuery(toolIndex))
		{
			retVal = toolMgr.hideTool(toolIndex);
			if (retVal != 0)
			{
				ostr << "Could not hide tool " << toolMgr.getToolName(toolIndex)
					 << " (" << toolIndex << ")" << endl << "Error code: " << retVal;
				_MTIErrorDialog(Handle, ostr.str());
			}
		}
	}

	ShowActiveToolName();
}

void TmainWindow::ShowActiveToolName()
{
	// Display the active tool's name on the main window's status bar

	CToolManager toolMgr;
	string activeToolName;

	int activeToolIndex = toolMgr.getActiveToolIndex();

	if (activeToolIndex >= 0)
		activeToolName = toolMgr.getToolName(activeToolIndex);
	else
		mainWindow->GetTimeline()->DRSMessageLabel->Caption = "";

	mainWindow->GetTimeline()->DRSNameLabel->Caption = activeToolName.c_str();
}

void TmainWindow::HideAllTools(int dontHideThisTool)
{
	int retVal;
	MTIostringstream ostr;

	CToolManager toolMgr;

	// Iterate over all of the items in the Image Tools submenu,
	// each of which corresponds to a tool
	for (int toolIndex = 0; toolIndex < toolMgr.getToolCount(); toolIndex++)
	{
		if (toolIndex != dontHideThisTool)
		{
			CToolObject *toolObj = toolMgr.GetTool(toolIndex);
			if (toolObj != 0 && toolObj->IsShowing())
			{
				retVal = toolMgr.hideTool(toolIndex);
				if (retVal != 0)
				{
					ostr.str("");
					ostr << "Could not hide tool " << toolMgr.getToolName(toolIndex)
						 << " (" << toolIndex << ")" << endl << "Error code: " << retVal;
					_MTIErrorDialog(Handle, ostr.str());
				}
			}
		}
	}
}

bool TmainWindow::HideAllToolsQuery()
{
	int retVal;
	MTIostringstream ostr;

	CToolManager toolMgr;

	// Iterate over all of the items in the Image Tools submenu,
	// each of which corresponds to a tool
	for (int toolIndex = 0; toolIndex < toolMgr.getToolCount(); toolIndex++)
	{
		CToolObject *toolObj = toolMgr.GetTool(toolIndex);
		if (toolObj != 0 && toolObj->IsShowing())
		{
			if (!toolMgr.hideToolQuery(toolIndex))
				return false;
		}
	}

	return true; // okay to close the tool(s)
}

void __fastcall TmainWindow::toolReportStatus(TObject *Sender) {}

void TmainWindow::SendSwitchSubtoolCmdToActiveTool()
{
	CToolManager toolMgr;
   toolMgr.onSwitchSubtool();
}

/////////////////////////////////////////////////////////////////////////////
//
// User Input Event Handlers for Keyboard and Mouse input.
//
// FormKeyDown, FormKeyUp, FormMouseDown, FormMouseUp and FormMouseMove
//
// Each function converts the Borland-defined event into a CPMP internal
// event as a CUserInput instance.  This user input event is then sent
// to the Tool Manager to dispatch to the appropriate tool.
//
//////////////////////////////////////////////////////////////////////////
void __fastcall TmainWindow::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{

	TRACE_3(errout << "KeyDOWN Key=" << Key);
	/*
	 << setbase(16) << setw(2) << setfill('0') << Key
	 << " Shift="
	 << "(" << ((Shift.Contains(ssShift))? "SHIFT" : "")
	 << "," << ((Shift.Contains(ssAlt))? "ALT" : "")
	 << "," << ((Shift.Contains(ssCtrl))? "CTRL" : "")
	 << "," << ((Shift.Contains(ssLeft))? "LEFT" : "")
	 << "," << ((Shift.Contains(ssMiddle))? "MIDDLE" : "")
	 << "," << ((Shift.Contains(ssRight))? "RIGHT" : "")
	 << ")"
	 );
	 */
	// In case there are edit controls that are children of the main windows,
	// don't steal any keys from them (unless ALT is pressed).
	if (!Shift.Contains(ssAlt))
	{
		TWinControl *activeControl = mainWindow->Active ? mainWindow->ActiveControl : nullptr;
		TCustomEdit *editControl = dynamic_cast<TCustomEdit*>(activeControl);
		if (editControl != nullptr)
		{
			return;
		}
	}

	// NOTE! FOR SOME UNKNOWN REASON, YOU CANNOT GET Shift+Space. If you are
	// holding down the shift key and you press space, Borland sends three
	// key events:  Shift-UP, Space-DOWN, Shift-Down, which is indistinguishable
	// from you letting go the shift key, hitting space, then pressing the
	// shift key again!!

	// Save shift state for posterity
	m_savedShiftState = Shift;

	// Handle the backslash display-mode-switching variants
	// These are mutually exclusive
	//
	bool gotNakedSlash = (!Shift.Contains(ssAlt) && !Shift.Contains(ssCtrl) && !Shift.Contains(ssShift) &&
		 (Key == MTK_BACKSLASH));
	bool gotShiftSlash = (!Shift.Contains(ssAlt) && !Shift.Contains(ssCtrl) && Shift.Contains(ssShift) &&
		 (Key == MTK_BACKSLASH));
	bool gotCtrlSlash = (!Shift.Contains(ssAlt) && Shift.Contains(ssCtrl) && !Shift.Contains(ssShift) &&
		 (Key == MTK_BACKSLASH));
	bool mainWinActive = (CNavSystemInterface::getActiveWindow() == mainWindow);

	if (gotNakedSlash && (_mainWindowState == MainWindowState::NormalWindow))
	{
		ToFullScreen();
		return;
	}
	else if (gotNakedSlash || (gotShiftSlash && (_mainWindowState == MainWindowState::Presentation)))
	{
		ToNormalWindow(); // will sort out if should call ToFullScreen() instead
		return;
	}
	else if (gotShiftSlash)
	{
		ToPresentationMode();
		return;
	}

	// HACK grab CHANNEL SELECTION hot keys
	// QQQ why isn't this handled in NavigatorTool.cpp, like all the other
	// global commands??
	if (Shift.Contains(ssCtrl) && !(Shift.Contains(ssAlt) || Shift.Contains(ssShift)))
	{
		bool handled = true;

		// wtf!!! THIS DEFINE IS MISSING FROM MTIKEYDEF.H !!!
#define MTK_NUMPAD_ENTER VK_SEPARATOR
		// Hmmm, it doesn't work anyhow.... the numpad 'enter' key is the
		// same code as the regular 'enter' key (13). Sucks.

		// HACK-O-RAMA! Emulate plain old mouseclick, not CTRL-mouseclick
		m_savedShiftState.Clear();

		// KURTKODE ALERT! some of these keys are passed on to the tool, and some
		// are not... just for the Registration tool!!!  UGLEEEEE!!
		switch (Key)
		{
		case MTK_NUMPAD0:
			ShowAllColorChannels();
			handled = false; // pass along for REGISTRATION (!)
			break;

		case MTK_NUMPAD1:
			ShowOrToggleColorChannel(RED_CHANNEL);
			handled = false; // pass along for REGISTRATION (!)
			break;

		case MTK_NUMPAD2:
			ShowOrToggleColorChannel(GRN_CHANNEL);
			// Apparently registration doesn't care anout the green channel!!?!
			break;

		case MTK_NUMPAD3:
			ShowOrToggleColorChannel(BLU_CHANNEL);
			handled = false; // pass along for REGISTRATION (!)
			break;

		case MTK_DECIMAL: // Numpad decimal point
			ToggleGrayDisplay();
			break;

		case MTK_NUMPAD4:
			ToggleAlphaDisplay();
			break;

		default:
			handled = false;
			break;
		}

		// WAS THIS:
		// m_savedShiftState << ssShift; // end of HACK-O-RAMA
		// But that makes no frickin' sense so I changed it to:
		m_savedShiftState = Shift;

		if (handled)
		{
			Key = 0; // Consumed key down event
			return; // handled it
		}
	}

	// HACK grab reticle selection hot keys
	// Theoretically this should be handled in NavigatorTool.cpp,
	// like all the other global commands, but in practice it's just
	// too frickin' hard!!
	//
	// NOTE: We originally wanted to use SHIFT as the modifier for the
	// numpad keys, but you can't because when you hit, e.g.:
	//
	// SHIFT DOWN, 'NUMPAD 3' DOWN, 'NUMPAD 3' UP, SHIFT UP
	//
	// what you see here is:
	//
	// SHIFT DOWN, SHIFT UP, 'PAGE DOWN' DOWN, 'PAGE DOWN' UP, SHIFT DOWN, SHIFT UP
	//
	// D'oh!!
	//
	if ((Key != MTK_ALT) && Shift.Contains(ssAlt) && !(Shift.Contains(ssShift) || Shift.Contains(ssCtrl)))
	{
		bool wasReticleHotKey = true;

		switch (Key)
		{
		case MTK_NUMPAD0: // All
			ShowAllReticles();
			break;

		case MTK_NUMPAD1: // Primary
			ToggleReticleBits(RETICLE_MODE_PRIMARY);
			break;

		case MTK_NUMPAD2: // Safe Action
			ToggleReticleBits(RETICLE_MODE_SAFE_ACTION);
			break;

		case MTK_NUMPAD3: // Safe Title
			ToggleReticleBits(RETICLE_MODE_SAFE_TITLE);
			break;

		case MTK_NUMPAD5: // Custom
			GetTimeline()->ToggleReticleComboBoxFocus();
			break;

		default:
			wasReticleHotKey = false;
		}

		if (wasReticleHotKey)
		{
			Key = 0; // Consumed key down event
			return; // handled it
		}
	}

	// If the RETICLE COMBO BOX has the focus, and the key is up or down arrow,
	// or a modifier key, let it handle the key.
	// Otherwise, take the focus away from it and then process the key as usual.
	if (ActiveControl == GetTimeline()->ReticleToolbarComboBox)
	{
		if (Key == MTK_UP || Key == MTK_DOWN || Key == MTK_ALT || Key == MTK_SHIFT || Key == MTK_CONTROL)
		{
			// Do not intercept the key.
			return;
		}
		else
		{
			// Remove focus from the reticle combo box, then continue processing
			// the key. Note that the key can't be ALT+Numpad5 because we handled
			// that earlier (search up for "case MTK_NUMPAD5")
			FocusOnMainWindow();
		}
	}

	// ANOTHER STUPID HACK, this one is for loupe settings control
	if (inLoupeSettingMode && (Key == MTK_4 || Key == MTK_5 || Key == MTK_6 || Key == MTK_7 || Key == MTK_8 ||
		 Key == MTK_UP || Key == MTK_DOWN || Key == MTK_0))
	{
		HandleLoupeSettingKey(Key);
		return;
	}

	// Send the Key Down event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;
	int retVal = toolManager.onKeyDown(Key, Shift);

	// I'm 99% sure that this does nothing...
	if (retVal & TOOL_RETURN_CODE_EVENT_CONSUMED)
	{
		Key = 0; // Consumed key down event
	}

	TRACE_3(errout << "ToolManager.onKeyDown returned " << retVal);
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
	m_savedShiftState = Shift;

	TRACE_3(errout << "KeyUP Key=" << Key);
	/*
	 << setbase(16) << setw(2) << setfill('0') << Key
	 << " Shift="
	 << "(" << ((Shift.Contains(ssShift))? "SHIFT" : "")
	 << "," << ((Shift.Contains(ssAlt))? "ALT" : "")
	 << "," << ((Shift.Contains(ssCtrl))? "CTRL" : "")
	 << "," << ((Shift.Contains(ssLeft))? "LEFT" : "")
	 << "," << ((Shift.Contains(ssMiddle))? "MIDDLE" : "")
	 << "," << ((Shift.Contains(ssRight))? "RIGHT" : "")
	 << ")"
	 );
	 */

	// Send the user input event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;
	int retVal = toolManager.onKeyUp(Key, Shift);

	if (retVal & TOOL_RETURN_CODE_EVENT_CONSUMED)
		Key = 0; // Consumed key up event

	TRACE_3(errout << "ToolManager.onKeyUp returned " << retVal);
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (m_doubleClick)
	{
		return;
	}

	if (clipDisplayer->isMouseZoomModeActive())
	{
		clipDisplayer->setPanMode(PAN_XY);
		clipDisplayer->panStart();
		return;
	}

	// Scale mouse coordinates to image coordinate space for Displayer's window
	int imageX = clipDisplayer->scaleXClientToImageX(X);
	int imageY = clipDisplayer->scaleYClientToImageY(Y);
	int screenX = Mouse->CursorPos.x;
	int screenY = Mouse->CursorPos.y;

	TRACE_3(errout << "Mouse DOWN Button=" << ((Button == mbLeft) ? "LEFT" : ((Button == mbMiddle) ? "MIDDLE" : "RIGHT"))
		 << " Shift=" << "(" << ((Shift.Contains(ssShift)) ? "SHIFT" : "") << "," << ((Shift.Contains(ssAlt)) ? "ALT" :
		 "") << "," << ((Shift.Contains(ssCtrl)) ? "CTRL" : "") << "," << ((Shift.Contains(ssLeft)) ? "LEFT" :
		 "") << "," << ((Shift.Contains(ssMiddle)) ? "MIDDLE" : "") << "," << ((Shift.Contains(ssRight)) ? "RIGHT" :
		 "") << ") POS=(" << screenX << "," << screenY << ") IMAGE=(" << imageX << "," << imageY << ")");

	m_MouseX = imageX;
	m_MouseY = imageY;
	m_Button = Button;
	m_Shift = Shift;

	// Send the MouseDown event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;
	int retVal = toolManager.onMouseDown(Button, Shift, X, Y, imageX, imageY);

	TRACE_3(errout << "ToolManager.onMouseDown returned " << retVal);
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (m_doubleClick)
	{
		m_doubleClick = false;
		return;
	}

	if (clipDisplayer->isPanning())
	{
		clipDisplayer->panStop();
		return;
	}

	// Scale mouse coordinates to image coordinate space for Displayer's window
	int imageX = clipDisplayer->scaleXClientToImageX(X);
	int imageY = clipDisplayer->scaleYClientToImageY(Y);

	TRACE_3(errout << "Mouse UP Button=" << ((Button == mbLeft) ? "LEFT" : ((Button == mbMiddle) ? "MIDDLE" : "RIGHT"))
		 << " Shift=" << "(" << ((Shift.Contains(ssShift)) ? "SHIFT" : "") << "," << ((Shift.Contains(ssAlt)) ? "ALT" :
		 "") << "," << ((Shift.Contains(ssCtrl)) ? "CTRL" : "") << "," << ((Shift.Contains(ssLeft)) ? "LEFT" :
		 "") << "," << ((Shift.Contains(ssMiddle)) ? "MIDDLE" : "") << "," << ((Shift.Contains(ssRight)) ? "RIGHT" :
		 "") << ") POS=(" << X << "," << Y << ") IMAGE=(" << imageX << "," << imageY << ")");

	// Send the MouseUp event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;
	int retVal = toolManager.onMouseUp(Button, Shift, X, Y, imageX, imageY);

	TRACE_3(errout << "ToolManager.onMouseUp returned " << retVal);
}

// ---------------------------------------------------------------------------
#define FINE_TO_COARSE 4     // number of pixels to move the mouse per
// simulated wheel increment

void __fastcall TmainWindow::FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	// Fix: for some reason X and Y are not always correct -
	// This makes a couple of Win32 calls & guarantees it
	// This fixes the displaced stretch box in DRS in the
	// full window - KT- 10/14/11
	clipDisplayer->getMousePositionClient(&X, &Y);

	// This code should be revisited to eliminate all the
	// redundant calculations inside of it - not hard. KT

	int screenX = Mouse->CursorPos.x;
	int screenY = Mouse->CursorPos.y;

	// THis is an awful hack to turn off the loupe when no clip is displayed;
	// I don't know where else to put it!
	if (LoupeWindow->Visible && !GetPlayer()->isAVideoClipLoaded())
	{
		LoupeWindow->deactivate();
	}

	// Update the loupe contents
	if (LoupeWindow->Visible && (screenX != oldLoupePos.x || screenY != oldLoupePos.y))
	{
		MoveLoupe(X, Y);
	}

	// The (deprecated) AutoFocus Hack!
	if (!Active && autofocusHackFlag)
	{
		FocusOnMainWindow();
	}

	if (X == savedMouseX && Y == savedMouseY)
	{
		return;
	}

	savedMouseX = X;
	savedMouseY = Y;

	// Scale mouse coordinates to image coordinate space for Displayer's window
	clipDisplayer->saveMouseShiftState(Shift);

	int imageX = clipDisplayer->scaleXClientToImageX(X);
	int imageY = clipDisplayer->scaleYClientToImageY(Y);

	// Send the MouseMove event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;
	toolManager.onMouseMove(Shift, X, Y, imageX, imageY);

	// show color components of point at mouse position
	UpdateComponentValuesToolbar(isMouseInFrame());
}

// ---------------------------------------------------------------------------

//#include "mkl.h"
//
void TmainWindow::StartForm(void)
{
	char FileName[256];
	GetModuleFileName(nullptr, FileName, sizeof(FileName));
	TRACE_0(errout << "DRS Nova Version " << GetDLLVersion(FileName) << "  ";
	errout << GetFileDateString(FileName) << endl; errout << GetDLLCopyright(FileName);
	errout << GetDLLInfo(FileName, "Comments"));

//	int nThreads = mkl_get_max_threads();
//	TRACE_0(errout << "Total MKL thread available " << nThreads);

#ifndef NO_QLM_LICENSING
	// This checks to see if the navigator is licensed
	if (GetMtiLicense()->IsActivated == false)
	{
		exit(2222);
	}
#endif

	// for these to look right, the FromBottom prop-
	// erty in both HistoryMenus must be equal to 3
	// HistoryMainMenu->RebuildMenu();
	// fullWindow->HistoryFullMenu->RebuildMenu();

	CToolManager toolMgr;
	int toolIndex = toolMgr.findTool("Navigator");
	toolMgr.initTool(toolIndex); // force the Nav Tool to be created

	toolIndex = toolMgr.findTool("PluginMother");
	toolMgr.initTool(toolIndex); // force the Plugin Mother Tool to be created

	// Load all the available tools
	// LicenseForm->AddFeatureTable(FeatureArray);
	// LicenseForm->AddFeatureTable(COMMON_LICENSING_TABLE);
	LoadToolsMenuItemClick(nullptr);

	// Must do this BEFORE ReadRestartProperties, which opens a clip!
	TimelineFrame->Init();

	// read all the properties
	ReadRestartProperties();

	// LicenseForm->CheckExpirationDates();

	Show();
	FocusOnMainWindow();

	// Hack to tell the timeline about the animated masks
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();
	CAnimateTimelineInterface *IFPtr = maskTool->GetAnimateTimelineInterface();
	GetTimeline()->SetAnimateTimelineInterface(IFPtr);

	// Check to see if GPU is enabled
	GpuAccessor gpuAccessor;
	if (GetGpuWarning() && (gpuAccessor.isDriverVersionOutOfData()))
	{
		if (gpuWarningForm == nullptr)
		{
			gpuWarningForm = new TgpuWarningForm(this);
		}

		ShowModalDialog(gpuWarningForm);
	}
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::HistoryMainMenuHistoryClick(String Str) {OpenClipByName(Str);}

// ---------------WriteRestartProperties------------John Mertus-----Jan 2001---

bool TmainWindow::WriteRestartProperties(void)

	 // This writes the ini files, saves the position and size
	 //
	 // ******************************************************************************
{
	CToolManager toolManager;
	//
	// This is really not the proper way to do this because
	// inifilename could change between read/write
	//

	CIniFile* ini = new CIniFile(CPMPIniFileName());
	if (ini == nullptr)
		return (false);
	//
	// Save the values
	//
	ini->WriteString("Restart", "ClipName", GetCurrentClipHandler()->getCurrentClipFileNameWithExt());

	// Added by mbraca  (2010.08.27 - trac #16)
	ini->WriteInteger("Restart", "LastFrameIndex", GetPlayer()->getLastFrameIndex());

	// OBSOLETE  ini->WriteBool("Restart", "FilmFrames", FilmFramesMenuItem->Checked);

	int iActiveToolIndex = toolManager.getActiveToolIndex();
	CToolObject *to = nullptr;
	if (iActiveToolIndex >= 0)
		to = toolManager.GetTool(iActiveToolIndex);

	if (to != nullptr)
	{
		ini->WriteString("Restart", "ActiveTool", to->getToolName());
		ini->WriteBool("Restart", "ShowActiveTool", to->IsShowing());
	}
	else
	{
		ini->DeleteKey("Restart", "ActiveTool");
		ini->DeleteKey("Restart", "ShowActiveTool");
	}

	ini->WriteInteger("Restart", "DisplayMode", GetDisplayer()->getDisplayMode());

	// mbraca commented this out 2010.08.27 because it doesn't work - the
	// status bar shows this value, but the player always starts running at
	// "Nominal" speed!
	// ini->WriteInteger("Restart", "DisplaySpeed", GetPlayer()->getDisplaySpeedIndex());

	// mbraca uncommented this 2010.08.27 becuse the fact that it is not
	// remembered was reported as a bug. It was commented out by tolks on
	// 2/16/2010 - no log message
	ini->WriteInteger("Restart", "ImageJustification", GetDisplayer()->getImageJustification());

	ini->WriteInteger("Restart", "RGBChannelsMask", GetDisplayer()->getRGBChannelsMask());

	int dspXctr = GetDisplayer()->getDisplayXctr();
	ini->WriteInteger("Restart", "DspXctr", dspXctr);
	ini->WriteInteger("Restart", "DspYctr", GetDisplayer()->getDisplayYctr());
	ini->WriteDouble("Restart", "ZoomFactor", GetDisplayer()->getZoomFactor());

	// LUT stuff
	string userLutName;
	ELutSelection selectedLutType = lutSettingsManager->GetLutSelection(userLutName);
	ini->WriteString("Restart", "DisplayLutType", (selectedLutType == LUTSEL_CLIP) ? "CLIP" :
		 ((selectedLutType == LUTSEL_PROJECT) ? "PROJECT" : "USER"));
	ini->WriteString("Restart", "UserLutName", userLutName);
	ini->WriteString("Restart", "UserLutName", userLutName);

	// Top panel height
	ini->WriteInteger("Restart", "TopPanelHeight", MainWindowTopPanel->Height);

	ini->WriteBool("Restart", "GpuWarning", GetGpuWarning());

	// Close it all up
	if (!DeleteIniFile(ini))
		return (false);

	// Save the current clip's state for when it may be reopened
	if (GetCurrentClipHandler()->saveClipState() != 0)
		return false;

	return true;
}

// ----------------ReadRestartProperties---------------John Mertus-----Jan 2001---

bool TmainWindow::ReadRestartProperties(void)

	 // This reads the init file and sets the saved position and size
	 //
	 // ******************************************************************************
{
	//
	// Read the special properties
	CIniFile* ini = new CIniFile(CPMPIniFileName());
	if (ini == nullptr)
		return (false);

	// Minor kludge, we need to know if defaults have been loaded
	// In reality, each form should be an individual test.  But we will assume
	// that Restart not there means everything will not be there and visa versa.
	bool bFirstStartup = IsFirstStartup();

   // Pop up release notes if this is the first time we are running this version.
   auto previousVersion = int(ini->ReadInteger("General", "Version", 0));
   char filename[MAX_PATH];
   GetModuleFileName(NULL, filename, sizeof(filename));
   auto versionNumbers = GetDLLMajorAndMinorVersionNumbers(filename);
   auto thisVersion = (versionNumbers.first * 100) + versionNumbers.second;
   if (previousVersion < thisVersion)
   {
      ReleaseNotesViewer releaseNotesViewer;
      releaseNotesViewer.show();
      ini->WriteInteger("General", "Version", thisVersion);
   }

	string sName = ini->ReadString("Restart", "ClipName", "");

	// Kludge to see if the bin root has changed
	// Find the root bin directory
	StringList roots;
	CPMPGetBinRoots(&roots);
	string rootPath = RemoveDirSeparator(roots[0]); // there is only one
	string prefix = sName.substr(0, rootPath.size());
	std::transform(rootPath.begin(), rootPath.end(), rootPath.begin(), ::tolower);
	std::transform(prefix.begin(), prefix.end(), prefix.begin(), ::tolower);
	if (prefix != rootPath)
	{
		sName = "";
	}

	string sActiveToolName = ini->ReadString("Restart", "ActiveTool", "");
	bool bShowTool = ini->ReadBool("Restart", "ShowActiveTool", true);

	GetDisplayer()->setRGBChannelsMask(ini->ReadInteger("Restart", "RGBChannelsMask", ALL_CHANNELS));
	UpdateRGBChannelsToolbarButtons();

	// these are not fed into the Displayer until ImageFormat has been set up

	int dspMode = ini->ReadInteger("Restart", "DisplayMode", DISPLAY_MODE_1_TO_1);

	int dspXctr = ini->ReadInteger("Restart", "DspXctr", 0);

	int dspYctr = ini->ReadInteger("Restart", "DspYctr", 0);

	double zoomFactor = ini->ReadDouble("Restart", "ZoomFactor", 1);

	// This code is executed only on restart
	if (bFirstStartup)
		SetDefaultWindowSizes();

	// Opening the clip will automatically restore the clip state saved in
	// the ini file and set up the ImageFormat in the Displayer (!) - which
	// is needed to get the display window completely set up
	if (!sName.empty())
		OpenClipByName(sName);

	// set up the display mode
	GetDisplayer()->setDisplayMode(dspMode);

	// update the GUI viewig mode
	UpdateViewingModeStatus();

	// now set the display window center
	GetDisplayer()->setDisplayWindowCenter(dspXctr, dspYctr);

	// we needed the clip open to get the Aspect Ratio Option
	GetDisplayer()->setZoomFactor(zoomFactor);

	// update the GUI zoom factor
	UpdateZoomLevelStatus();

	// Tell plugin mother window to sync visible tool with tabs
	BaseToolWindow->ToolTabsChange(nullptr);

	// mbraca moved this down here from up above to put it after
	// all the zoom stuff
	GetDisplayer()->setImageJustification(ini->ReadInteger("Restart", "ImageJustification", 0));
	// centered
	UpdateJustificationToolbarButtons();

	// LUT stuff
	string selectedLutTypeAsString = "CLIP";
	string userLutName;
	selectedLutTypeAsString = ini->ReadString("Restart", "DisplayLutType", selectedLutTypeAsString);
	ELutSelection selectedLutType = (selectedLutTypeAsString == "CLIP") ? LUTSEL_CLIP :
		 ((selectedLutTypeAsString == "PROJECT") ? LUTSEL_PROJECT : LUTSEL_USER);
	if (selectedLutType == LUTSEL_USER)
	{
		userLutName = ini->ReadString("Restart", "UserLutName", userLutName);
	}
	lutSettingsManager->SelectLut(selectedLutType, &userLutName);

	SetGpuWarning(ini->ReadBool("Restart", "GpuWarning", true));

	// Top panel
	MainWindowTopPanel->Height = ini->ReadInteger("Restart", "TopPanelHeight", MainWindowTopPanel->Height);

	// Finally, go to the frame indicated by the LastFrameIndex parameter
	// (2010.08.27 - trac #16)
	int lastFrameIndex = GetPlayer()->getLastFrameIndex();
	lastFrameIndex = ini->ReadInteger("Restart", "LastFrameIndex", lastFrameIndex);
	GetPlayer()->goToFrame(lastFrameIndex);

	DeleteIniFile(ini);

	return (true);
}

void TmainWindow::SetDefaultWindowSizes(void)
{
	// // NOTE: Timeline form undocking is no longer supported!
	//
	// // Set the size of the forms
	// int H = Screen->Height;
	// int W = Screen->Width;
	// int E = 15;
	// SetBounds(E, E, W - 2*E, H - 3*E - TimelineFrame->Height);
	// TimelineFrame->SetBounds((W - TimelineFrame->Width) / 2, Top + Height, TimelineFrame->Width,
	// TimelineFrame->Height);
	//
	// // Set some other forms
	// // Don't control too much and pick it out.
	//
	// TimelineFrame->Visible = !TimelineFrame->DockedState();
}

RECT TmainWindow::GetUsableClientRect(void)
{
	RECT usableRect = DisplayPanel->ClientRect;

	// Trying to make "aligned = Client work. This causes infinite onPaint loop.
	// // There seems to be an issue with the styles. Our conjecture is that if
	// // the design width of the DisplayPanel is larger than the physical screen
	// // width, then it gets anchored at a value that is LARGER than the client
	// // width of the main window! It's supposed to always be two pixels less
	// // than the main window client width, so if it's not, then we fix it here!
	// int clientWidthMinusTwo = ClientWidth - 2;
	// if (usableRect.right != clientWidthMinusTwo)
	// {
	// // Note: client rect left and top are always 0!
	// usableRect.right = clientWidthMinusTwo;
	// DisplayPanel->ClientWidth = clientWidthMinusTwo;
	// }

	// Bottom panel is now aligned "bottom"
	// if (MainWindowBottomPanel->Visible)
	// {
	// usableRect.bottom -= MainWindowBottomPanel->Height;
	// }

	// DumpClientRects();

	return usableRect;
}

RECT TmainWindow::GetUsableClientRectInScreenCoords(void)
{
	RECT usableRect = GetUsableClientRect();
	TPoint upperLeft = ClientToScreen(TPoint(usableRect.left, usableRect.top));
	TPoint lowerRight = ClientToScreen(TPoint(usableRect.right, usableRect.bottom));
	usableRect.left = upperLeft.x;
	usableRect.top = upperLeft.y;
	usableRect.right = lowerRight.x;
	usableRect.bottom = lowerRight.y;

	return usableRect;
}

RECT TmainWindow::getImageRectangle(void) {return GetDisplayer()->getImageRectangle();}

RECT TmainWindow::GetStretchRectangleScreenCoords()
{
	RECT stretchRect = GetDisplayer()->getStretchRectangleClientCoords();
	TPoint upperLeft = ClientToScreen(TPoint(stretchRect.left, stretchRect.top));
	TPoint lowerRight = ClientToScreen(TPoint(stretchRect.right, stretchRect.bottom));
	stretchRect.left = upperLeft.x;
	stretchRect.top = upperLeft.y;
	stretchRect.right = lowerRight.x;
	stretchRect.bottom = lowerRight.y;
	return stretchRect;
}

void TmainWindow::SaveSAP()
{
	savedLeft = Left;
	savedTop = Top;
	savedWidth = Width;
	savedHeight = Height;
}

void TmainWindow::RestoreSAP() {SetBounds(savedLeft, savedTop, savedWidth, savedHeight);}
// ---------------------------------------------------------------------------

void TmainWindow::RestoreParentsToToolbarPanel()
{
	// Get the toolbar
	TPanel *timecodeToolbarPanel = this->GetTimeline()->CenterToolbarCenteringPanel;
	TPanel *timelineFullToolBarPanel = this->GetTimeline()->ToolbarPanel;

	FloatingTimelineToolbarRibbonParentPanel->Visible = false;
	FloatingTimecodeToolbarParentPanel->Visible = false;

	// Reparent the full toolbar from the floating ribbon to the timeline.
	timelineFullToolBarPanel->Visible = true;
	timelineFullToolBarPanel->Parent = TimelineFrame->EntirePanel;

	// Reparent timecodeToolBar back to the full timeline toolbar.
	timecodeToolbarPanel->Parent = timelineFullToolBarPanel;
   TimelineFrame->ResizeTimecodeToolbar();
	timecodeToolbarPanel->Visible = true;
   TimelineFrame->ResizeTimecodeToolbar();

   // OpenGL bug workaround - client area cannot cover entire screen!
   OpenGLBugOnePixelPaddingHackBottomColorPanel->Visible = false;

	MainWindowBottomPanel->Visible = true;
}

void TmainWindow::ToFullScreen()
{
	// The stupid timeline uses the main window state to set sizes,
   // so need to set the state first.
   auto oldMainWindowState = _mainWindowState;
	_mainWindowState = MainWindowState::FullScreen; // BEFORE WindowState = wsNormal

	// Keep track of what to restore after leaving Fullscreen mode.
   if (oldMainWindowState == MainWindowState::NormalWindow)
   {
      if (WindowState == wsMaximized)
      {
         WindowState = wsNormal;
         _needToRemaximizeWhenReturningToNormalWindow = true;
      }
      else
      {
         _unmaximizedNormalWindowBounds = BoundsRect;
      }
   }

	Menu = nullptr;
	BorderStyle = bsNone;
	BoundsRect = GetBoundsRectOfLargestMonitor();

	RestoreParentsToToolbarPanel();
	RibbonTimer->Enabled = false;
}

// ---------------------------------------------------------------------------

void TmainWindow::ToNormalWindow()
{
   // OpenGL bug workaround - client area cannot cover entire screen!
   OpenGLBugOnePixelPaddingHackBottomColorPanel->Visible = false;

	Menu = MainMenu;
	MainWindowBottomPanel->Visible = true;
	BorderStyle = bsSizeable;

	_mainWindowState = MainWindowState::NormalWindow; // BEFORE changing size!

   // Restore normal or maximized window.
   if (_needToRemaximizeWhenReturningToNormalWindow)
   {
      _needToRemaximizeWhenReturningToNormalWindow = false;
      WindowState = wsMaximized;
   }
   else
   {
	   BoundsRect = _unmaximizedNormalWindowBounds;
   }

	RestoreParentsToToolbarPanel();
	RibbonTimer->Enabled = false;
}

// ---------------------------------------------------------------------------

void TmainWindow::ToPresentationMode()
{
	// The stupid timeline uses the main window state to set sizes,
   // so need to set the state first.
   auto oldMainWindowState = _mainWindowState;
	_mainWindowState = MainWindowState::Presentation;

   // Keep track of what to restore after leaving Presentation mode.
   if (oldMainWindowState == MainWindowState::NormalWindow)
   {
      if (WindowState == wsMaximized)
      {
         WindowState = wsNormal;
         _needToRemaximizeWhenReturningToNormalWindow = true;
      }
      else
      {
         _unmaximizedNormalWindowBounds = BoundsRect;
      }
   }

	MainWindowBottomPanel->Visible = false;

   // OpenGL bug workaround - client area cannot cover entire screen!
   OpenGLBugOnePixelPaddingHackBottomColorPanel->Visible = true;

	// HACK For some reason I have to have this visible when I change the size
	// else the first time the ribbon is shown, the timecode panel isn't centered!
	FloatingTimelineToolbarRibbonParentPanel->Visible = true;

	Menu = nullptr;
	BorderStyle = bsNone;
	BoundsRect = GetBoundsRectOfLargestMonitor();

	HideAllAuxiliaryWindows();

	// Second part of the timecode panel centering hack!
	ToggleShowTimelineRibbonOverlay();

	RibbonTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void TmainWindow::RefreshBinManagerClipList()
{
	if (BinManagerForm == nullptr)
	{
		return;
	}

	BinManagerForm->RefreshClipList();
}
// ---------------------------------------------------------------------------

/* static */ void TmainWindow::HideAllAuxiliaryWindows()
{
	// Larry says to never auto-hide windows!
	// for (int i = 0; i < Screen->FormCount; i++)
	// if ((Screen->Forms[i] != mainWindow) && (Screen->Forms[i] != BaseToolWindow))
	// Screen->Forms[i]->Hide();
	//
	// ExecuteNavigatorToolCommand(NAV_CMD_HIDE_TOOL_PALETTE);
}
// ---------------------------------------------------------------------------

void TmainWindow::SaveToolbars(TForm *form, const string &prefix)
{
	string sectionNamePrefix = prefix + "\\"; // QQQ manifest constant
	string iniFilename = TOOLBAR_INI_FILE;
	ExpandEnviornmentString(iniFilename);
}

void TmainWindow::RestoreToolbars(TForm *form, const string &prefix)
{
	string sectionNamePrefix = prefix + "\\"; // QQQ manifest constant
	string iniFilename = TOOLBAR_INI_FILE;
	ExpandEnviornmentString(iniFilename);

	// Don't attempt to load if there's no ini file, else error pops up
	if (!DoesFileExist(iniFilename))
		return;
}
// ---------------------------------------------------------------------------

static bool IsClosing = false;

void __fastcall TmainWindow::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	// Avoid reentrant code
	if (IsClosing)
	{
		return;
	}

	ExecuteNavigatorToolCommand(NAV_CMD_SHOW_TOOL_PALETTE);

	CToolManager toolMgr;

	// Inform the tools of pending quit BEFORE CheckProvisional so the tool
	// has a chance to do something about it!
	if (!HideAllToolsQuery())
	{
		CanClose = false;
		return;
	}

	// Auto-accept any pending GOV operation
	int govToolIndex = toolMgr.findTool("GOV"); // QQQ Manifest constant!!
	if (govToolIndex >= 0)
	{
		CToolObject *toolObj = toolMgr.GetTool(govToolIndex);
		CGOVTool *govTool = dynamic_cast<CGOVTool*>(toolObj);
		if (govTool != nullptr)
			govTool->AcceptGOVIfPending();
	}

	if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
	{
		// A change is pending or a tool is running so cannot
		// close the app right now
		CanClose = false;
		return;
	}

	// Check if we can close all of the PDL Viewer forms
	CPDLViewerManager pdlViewerMgr;
	if (!pdlViewerMgr.CloseQueryAll())
	{
		CanClose = false;
		return;
	}

	IsClosing = true;

	// No more memory allocation or traces
	NotifyProgramExiting();

	// tell the Player to shut down
	clipPlayer->terminate();

	if (PreferencesDlg->AutoSaveSAPCBox->Checked)
	{
		SaveAllSAP(CM_STARTUP_DATA);
	}

	if (PreferencesDlg->AutoSavePreferencesCBox->Checked)
	{
		SaveAllSettings(CM_STARTUP_DATA);
		WriteRestartProperties();
	}

	// Close the Navigator's clip
	if (BinManagerForm != nullptr)
	{
		BinManagerForm->ClearProxy();
	}

	GetCurrentClipHandler()->closeCurrentClip();

#ifndef NO_LICENSING
	rsrcCtrl.CheckIn();
#endif

	BaseToolWindow->HideMaskProperties();

	// Delete the Tool Objects and Plugins they rode in on
	BaseToolWindow->CoverUp(true);
	CToolManager toolManager;
	toolManager.unloadPlugins();

	// Need to let MediaFrameFileReader clean up caches
	MediaFileIO::Shutdown();
}

// ---------------------------------------------------------------------------
void __fastcall TmainWindow::ViewMenuClick(TObject *Sender)
{
#ifndef NO_LICENSING
	CRsrcCtrl rsrcCtrl;
#endif

	LUTSubmenu->Enabled = true;
#ifndef NO_LICENSING
	CadenceRepairMenuItem->Enabled = rsrcCtrl.IsFeatureLicensed(FEATURE_CORRECT_CADENCE);
#endif
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FramingMenuItemClick(TObject *Sender)
{
	int retVal;
	int tag;

	const char *menuStr;
	TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
	if (menuItem != nullptr)
	{
		menuStr = StringToStdString(menuItem->Caption).c_str();
		tag = menuItem->Tag;
	}
	else
	{
		return; // Should really ASSERT here
	}

	// Copy Framing Menu Item's Caption to be the Status Bar Text
	// First remove ampersand character (must be a better way!)
	string statusBarStr;
	while (*menuStr != 0)
	{
		if (*menuStr == '&')
		{
			menuStr++;
			continue;
		}

		statusBarStr += *menuStr++;
	}

	// Select Framing for the current clip & proxy.  Assumes that the
	// Framing Menu Item's Tag is the framing index
	retVal = GetCurrentClipHandler()->selectVideoFraming(tag);
	if (retVal != 0)
	{
		string errMsg = "Framing View " + statusBarStr + "\n is not available";
		_MTIErrorDialog(Handle, errMsg);
		return;
	}

	// Set the current clip as the source clip, used as the default
	// rendering destination clip
	renderDestinationClip->SetSourceClip(GetCurrentClipHandler()->getCurrentClipFileName(),
		 GetCurrentClipHandler()->getCurrentVideoProxyIndex(), GetCurrentClipHandler()->getCurrentVideoFramingIndex());

	// Clear the various frame file caches
	// QQQ Why?
	// MediaFileIO::ClearAllCaches();

	// Put the name of the framing view in the status bar
	UpdateFramingViewStatus();
	ClipHasChanged.Notify();

	// Rebuild the cut timeline (mbraca+20041022)
	GetTimeline()->performCutAction(CUT_REFRESH);
}

static const char *framingViewNames[] =
{"Video Frames", "Film Frames", "Video 1, 2", "Video Field 1", "Video Field 2"};

void TmainWindow::UpdateFramingViewStatus()
{
	string statusBarStr;

	int index = GetCurrentClipHandler()->getCurrentVideoFramingIndex();
	if (index >= 0 && index < 5) // manifest constant QQQ
			 statusBarStr = framingViewNames[index];

	// The following is a really frickin' STUPID comment !!!! EXPLAIN WHY!!!
	// update speed status
	UpdateFPSViewStatus(GetPlayer()->getDisplaySpeedIndex());
}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateZoomLevelStatus()
{
	double level = GetDisplayer()->getZoomFactor();
	int percent = (int)((level * 100.0) + 0.5);
	if (percent < 1)
		percent = 1;

	char val[32];

	switch (GetDisplayer()->getDisplayMode())
	{
	case DISPLAY_MODE_TRUE_VIEW:
		sprintf(val, "True View  %d%c", percent, '%');
		GetTimeline()->ViewModeTBItem->Caption = val;
		break;

	case DISPLAY_MODE_1_TO_1:
		{
			// calculate the correct zoom caption for 1-to-1
			int fmpels = 1;
			int topels = 1;
			if (level < 1.0)
			{
				fmpels = (int)((1 / level) + 0.5);
			}
			else
			{
				topels = (int)(level + 0.5);
			}

			sprintf(val, " %d-to-%d  %d%%", fmpels, topels, percent);
			GetTimeline()->ViewModeTBItem->Caption = val;
		}

		break;

	case DISPLAY_MODE_FIT_WIDTH:
		sprintf(val, "Fit Width  %d%%", percent);
		GetTimeline()->ViewModeTBItem->Caption = val;
		break;
	}
}

// ---------------------------------------------------------------------------

void TmainWindow::UpdatePlaybackFilterStatus()
{
	string statusText = "None";
	TColorLabel *label = GetTimeline()->timelineCurrentTCLabel;
	int captureMode = GetDisplayer()->getCaptureMode();

	switch (GetPlayer()->getPlaybackFilter())
	{
	case PLAYBACK_FILTER_ORIGINAL_VALUES:
		label->Transparent = false;
		label->Color = (TColor)0x00f2ff;
		label->Font->Color = clBlack;
		statusText = "Orig";
		break;
	case PLAYBACK_FILTER_HIGHLIGHT_FIXES:
		label->Transparent = false;
		label->Color = (TColor)0xff9933;
		label->Font->Color = clBlack;
		statusText = "Fixes";
		break;
	case PLAYBACK_FILTER_BOTH:
		label->Transparent = false;
		label->Color = (TColor)0x00f2ff;
		label->Font->Color = (TColor)0xcc483f;
		statusText = "Both";
		break;
	case PLAYBACK_FILTER_IMPORT_FRAME_DIFFS:
		label->Transparent = false;
		label->Color = (captureMode == CAPTURE_MODE_ORIG_TARGET) ? (TColor)0xff20ff : (TColor)0x1010ff;
		label->Font->Color = clBlack;
		statusText = "Diffs";
		break;
	default:
		label->Transparent = true;
		label->Font->Color = clBlack;
		break;
	}

}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateFPSViewStatus(int prefSpeed)
{
	char strTemp[32];
	sprintf(strTemp, "0 fps");
	GetTimeline()->FpsReadoutLabel->Caption = strTemp;
}

void TmainWindow::UpdateFPSActual()
{
	char strTemp[32];
	int deltaFrame = clipPlayer->getDeltaFrame();
	int direction = (deltaFrame == 0) ? 0 : (deltaFrame / abs(deltaFrame));
	deltaFrame = abs(deltaFrame);

	if ((actualFPSModulus % 5) == 0)
	{
		float fSpeed = clipPlayer->getActualDisplaySpeed() * deltaFrame;
		int iSpeed = (int)(fSpeed + 0.5f);
//		if (iSpeed == 0 || iSpeed >= 10)
//		{
//			sprintf(strTemp, " %d / %d fps ", direction * iSpeed, GetPlayer()->getPlaybackSpeed());
//		}
//		else
//		{
//			sprintf(strTemp, " %3.1f / %d fps ", direction * fSpeed, GetPlayer()->getPlaybackSpeed());
//		}
      sprintf(strTemp, "%d fps", iSpeed);

      GetTimeline()->FpsReadoutLabel->Caption = strTemp;
	}

	actualFPSModulus++;
}
// ---------------------------------------------------------------------------

////////////////////////////////////////////////////////////
// CAREFUL!! THIS IS THE VIEWING MODE (e.g. "TrueView"),  //
// NOT THE 'Remote Monitor' SHIT                //
////////////////////////////////////////////////////////////

void __fastcall TmainWindow::DisplayModeClick(TObject *Sender)
{
	// Check Menu Item, others automatically unselected
	int tag;
	TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
	TMenuItem *toolbarItem = dynamic_cast<TMenuItem*>(Sender);
	TMenuItem *submenuItem = dynamic_cast<TMenuItem*>(Sender);
	if (menuItem != nullptr)
		tag = menuItem->Tag;
	else if (toolbarItem != nullptr)
		tag = toolbarItem->Tag;
	else if (submenuItem != nullptr)
		tag = submenuItem->Tag;
	else
		return; // Should really ASSERT here

	// set the display mode. You won't necessarily
	// get what you ask for, since RGB files can't
	// be displayed on the REMOTE display.
	GetDisplayer()->setDisplayMode(tag);

	// zoom all only if picture mode or
	// pixel aspect ratio changes
	// see Displayer
	// GetDisplayer()->zoomAll();
	// UpdateZoomLevelStatus();

	// refresh the screen in the new mode
	GetPlayer()->refreshFrame();

	// Update the status bar panel
	UpdateViewingModeStatus();
}

void TmainWindow::UpdateViewingModeStatus() {UpdateZoomLevelStatus();}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayDevicesSubmenuClick(TObject *Sender)
{
	if (GetPlayer() == nullptr)
		return;

	// based on the display mode in the Player,  set
	// the check marks on the display devices buttons
	switch (GetPlayer()->getDisplayMode())
	{

	case DISPLAY_TO_WINDOW:
		LocalDisplayButton->Checked = true;
		break;

	case DISPLAY_TO_MONITOR:
		RemoteDisplayButton->Checked = true;
		break;

	case DISPLAY_TO_BOTH:
		BothDisplayButton->Checked = true;
		break;
	}
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayDevicesClick(TObject *Sender)
{
	// Check Menu Item, others automatically unselected
	int tag;
	TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
	TMenuItem *toolbarItem = dynamic_cast<TMenuItem*>(Sender);
	if (menuItem != nullptr)
		tag = menuItem->Tag;
	else if (toolbarItem != nullptr)
		tag = toolbarItem->Tag;
	else
		return; // Should really ASSERT here

	// set the display mode. You won't necessarily
	// get what you ask for, since RGB files can't
	// be displayed on the REMOTE display.
	GetPlayer()->setDisplayMode(tag);
}
// ---------------------------------------------------------------------------

// -----------------BeforeMainMenuDropDown-----------John Mertus----Jul 2002-----

void TmainWindow::BeforeMainMenuDropDown(void)

	 // Just update the menu before it is displayed.
	 // Note: If states have not changed, this is very, very fast.
	 //
	 // ****************************************************************************
{
	CToolManager toolManager;
#ifndef NO_LICENSING
	CRsrcCtrl rsrcCtrl;
#endif
}
// ---------------------------------------------------------------------------


void __fastcall TmainWindow::DefaultsSapMenuClick(TObject *Sender)
{
	RestoreAllSAP(CM_DESIGNED_DATA);
	SetDefaultWindowSizes();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::StartupSAPMenuClick(TObject *Sender)
{
   RestoreAllSAP(CM_STARTUP_DATA);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::StartupPositionsClick(TObject *Sender)
{
   SaveAllSAP(CM_STARTUP_DATA);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::SaveUserPositionsMenuItemClick(TObject *Sender)
{
   SaveAllSAP(CM_USER_DATA);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::SaveStartupPositionsMenuItemClick(TObject *Sender)
{
   SaveAllSAP(CM_STARTUP_DATA);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::RestoreSAPMenuItemClick(TObject *Sender)
{
   RestoreAllSAP(CM_USER_DATA);
}

// ---------------------------------------------------------------------------

int TmainWindow::ErrorHandler(const ErrorTID etid, const int Error, const string &Message)
{
	MTIostringstream os;
	os << Message << "  (" << Error << ")";

	if (Error != ERR_DNGL_TIME_WARNING)
	{
		// If PDL Rendering, then tell Tool Manager about the error, but do
		// not open an error dialog.  If not PDL Rendering, then report
		// the error in a regular error dialog box
		CToolManager toolMgr;
		if (toolMgr.IsPDLRendering())
			toolMgr.SetReportedPDLRenderingError(Error, Message);
		else
			_MTIErrorDialog(Handle, os.str().c_str());
	}
	else
	{
		MTIInformationDialogModeless(os.str().c_str());
	}

	return true; // used as a return to Notify
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::AboutMenuClick(TObject *Sender)
{
	AboutForm = new TAboutForm(this);
	ShowModalDialog(AboutForm);

	delete AboutForm;
	AboutForm = nullptr;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::PreferencesMenuItemClick(TObject *Sender)
{
	ShowModalDialog(PreferencesDlg);

	// In case border changed
	FormResize(nullptr);
}
// ---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a copy.  Note: Component writers MUST respond to
// WM_COPY
void __fastcall TmainWindow::CopyMenuItemClick(TObject *Sender)
{
   SendMessage(GetFocus(), WM_COPY, 0, 0);
}

// ---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a CUT.  Note: Component writers MUST respond to
// WM_CUT
void __fastcall TmainWindow::CutMenuItemClick(TObject *Sender)
{
   SendMessage(GetFocus(), WM_CUT, 0, 0);
}
// ---------------------------------------------------------------------------

// This just sends a message to the component which has focus and
// then preforms a PASTE.  Note: Component writers MUST respond to
// WM_PASTE
void __fastcall TmainWindow::PasteMenuItemClick(TObject *Sender)
{
   SendMessage(GetFocus(), WM_PASTE, 0, 0);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::QuitWithoutSavingMenuItemClick(TObject *Sender)
{
	// This solution is somewhat of a kludge but shouldn't
	// do much harm
	PreferencesDlg->AutoSaveSAPCBox->Checked = false;
	PreferencesDlg->AutoSavePreferencesCBox->Checked = false;
	Close();
}
// ---------------------------------------------------------------------------

bool TmainWindow::CreateDeferredForm(const string &Name)
{
	if (Name == "BinManagerForm")
	{
		ShowBinManager(true);
		return true;
	}

	return false;
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayModeSubmenuClick(TObject *Sender)
{
	if (GetDisplayer() == nullptr)
		return;

	int dspmode = GetDisplayer()->getDisplayMode();
	bool truvue = (dspmode == DISPLAY_MODE_TRUE_VIEW);
	bool oneone = (dspmode == DISPLAY_MODE_1_TO_1);
	bool ftwdth = (dspmode == DISPLAY_MODE_FIT_WIDTH);

	// TrueViewMenuItem->Checked             = truvue;
	AspectSubmenuItem->Checked = truvue;
	OneToOneMenuItem->Checked = oneone;
	FitWidthMenuItem->Checked = ftwdth;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ShowActiveToolMenuItemClick(TObject *Sender)
{
	if (BaseToolWindow->Visible)
	{
		// BaseToolWindow->Hide();
      ExecuteNavigatorToolCommand(NAV_CMD_HIDE_TOOL_PALETTE);
	}
	else
	{
		// BaseToolWindow->Show();
		ExecuteNavigatorToolCommand(NAV_CMD_SHOW_TOOL_PALETTE);
		BaseToolWindow->SetFocus();
	}

}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::WindowsMenuClick(TObject *Sender)
{
	FullScreenMenuItem->Checked = _mainWindowState != MainWindowState::Presentation;

	BinManagerMenuItem->Enabled = true;
	BinManagerMenuItem->Checked = (BinManagerForm != nullptr) && BinManagerForm->Visible;

	BookmarkEventViewerMenuItem->Enabled = true;
	BookmarkEventViewerMenuItem->Checked = (BookmarkEventViewerForm != nullptr) && BookmarkEventViewerForm->Visible;

	CPDLViewerManager mgr;
	ShowPDLViewerMenuItem->Enabled = true;
	ShowPDLViewerMenuItem->Checked = mgr.IsActivePDLVisible();

	ShowActiveToolMenuItem->Enabled = true;
	ShowActiveToolMenuItem->Checked = BaseToolWindow->Visible;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FramingSubmenuSetup(TObject *Sender)
{
	// Update the View Frames submenu items when View|Frames item is
	// clicked or passed over with cursor.

#ifndef NO_LICENSING
	CRsrcCtrl rsrcCtrl;
#endif
	auto clip = GetCurrentClipHandler()->getCurrentClip();
	bool frameViewAvailable;
	bool fieldViewAvailable;
	if (clip == nullptr)
	{
		// A clip is not open

		frameViewAvailable = false;
		fieldViewAvailable = false;

		// Remove marks from all of the view menu items
		VideoFramesMenuItem->Checked = false;
		FilmFramesMenuItem->Checked = false;
		Fields12MenuItem->Checked = false;
		Field1OnlyMenuItem->Checked = false;
		Field2OnlyMenuItem->Checked = false;
		CadenceRepairMenuItem->Enabled = false;
		CadenceRepairMenuItem->Checked = false;
	}
	else
	{
		frameViewAvailable = true;

		// Pick the Framing View submenu item that corresponds to the
		// current clip's framing
		int framingIndex = GetCurrentClipHandler()->getCurrentVideoFramingIndex();
		for (int i = 0; i < FramingMenuItem->Count; ++i)
		{
			// TODO TTBCustomItem *viewMenuItem     =             FramingMenuItem->Items[i];
			// if (viewMenuItem->Tag == framingIndex)
			// {
			// // Check the view submenu.  All other radio buttons in the
			// // view submenu are automatically unchecked.
			// viewMenuItem->Checked     = true;
			// break;
			// }
		}

#ifndef NO_LICENSING
		CadenceRepairMenuItem->Enabled = rsrcCtrl.IsFeatureLicensed(FEATURE_CORRECT_CADENCE);
#else
		CadenceRepairMenuItem->Enabled = true;
#endif
		// If a clip is interlaced, then we can enable the Video Fields
		// framing menu items
		int videoProxyIndex = GetCurrentClipHandler()->getCurrentVideoProxyIndex();
		const CImageFormat *imageFormat = clip->getImageFormat(videoProxyIndex);
		if (imageFormat == 0)
			fieldViewAvailable = false;
		else
			fieldViewAvailable = imageFormat->getInterlaced();
	}

	// Enable or Disable the Video Frame and Film Frame menu items
	VideoFramesMenuItem->Enabled = frameViewAvailable;
	FilmFramesMenuItem->Enabled = frameViewAvailable;

	// Enable or Disable the video field menu items
	Fields12MenuItem->Enabled = fieldViewAvailable;
	Field1OnlyMenuItem->Enabled = fieldViewAvailable;
	Field2OnlyMenuItem->Enabled = fieldViewAvailable;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplaySpeedSubmenuClick(TObject *Sender)
{
	if (GetPlayer() == nullptr)
		return;
	int curSpeed = GetPlayer()->getDisplaySpeedIndex();
	switch (curSpeed)
	{

	case DISPLAY_SPEED_NOMINAL:
		NominalSpeedMenuItem->Checked = true;
		break;

	case DISPLAY_SPEED_SLOW:
		SlowSpeedMenuItem->Checked = true;
		break;

	case DISPLAY_SPEED_MEDIUM:
		MediumSpeedMenuItem->Checked = true;
		break;

	case DISPLAY_SPEED_FAST:
		FastSpeedMenuItem->Checked = true;
		break;

	case DISPLAY_SPEED_UNLIMITED:
		UnlimitedSpeedMenuItem->Checked = true;
		break;
	}

}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplaySpeedClick(TObject *Sender)
{
	// Check Menu Item, others automatically unselected
	int tag;
	TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
	TMenuItem *toolbarItem = dynamic_cast<TMenuItem*>(Sender);
	if (menuItem != nullptr)
		tag = menuItem->Tag;
	else if (toolbarItem != nullptr)
		tag = toolbarItem->Tag;
	else
		return; // Should really ASSERT here

	GetPlayer()->setDisplaySpeedIndex(tag);
	UpdateFPSViewStatus(tag);
}
// ---------------------------------------------------------------------------

// ------------------WMEraseBkgnd------------------John Mertus----May 2003-----

void _fastcall TmainWindow::WMEraseBkgnd(TMessage &Message)

	 // Ignore all background erases
	 //
	 // ****************************************************************************
{Message.Msg = 1;}

// ---------------------------------------------------------------------------
//
// Response to CB_FIRE_REFRESH_TIMER Windows message
//
void _fastcall TmainWindow::CBFireRefreshTimer(TMessage &msg)
{
	// Refresh the main window, if necessary
	// QQQ do we want to not bother if the window is invisible?
	if (RefreshTimerCallback())
		GetPlayer()->notifyMainWindowRefreshDone();

	// Update some status bar fields (FPS, component values), every 50 msec
	if (mainWindow->Visible && (CVDTimer.Read() >= 50))
	{
		UpdateSomeGUIStuff();
		CVDTimer.Start();
	}

	GetPlayer()->notifyPostMessageWasReceived();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayFilterSubmenuClick(TObject *Sender)
{
	if (GetPlayer() == nullptr)
		return;
	int curFilter = GetPlayer()->getPlaybackFilter();
	switch (curFilter)
	{

	case PLAYBACK_FILTER_NONE:
		NoneMenuItem->Checked = true;
		break;

	case PLAYBACK_FILTER_ORIGINAL_VALUES:
		OriginalValuesMenuItem->Checked = true;
		break;

	case PLAYBACK_FILTER_HIGHLIGHT_FIXES:
		HighlightFixesMenuItem->Checked = true;
		break;

	case PLAYBACK_FILTER_BOTH:
		BothMenuItem->Checked = true;
		break;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayFilterClick(TObject *Sender)
{
	// Check Menu Item, others automatically unselected
	int newFilter = PLAYBACK_FILTER_INVALID;
	int oldFilter = GetPlayer()->getPlaybackFilter();

	// Get the new filter value by downcasting to find out which
	// component triggered the event; Note though, that I see the
	// menu items to set this but I sure don't see a toolbar item!
	// Did we get rid of it at some point? QQQ
	TMenuItem *menuItem = dynamic_cast<TMenuItem*>(Sender);
	TMenuItem *toolbarItem = dynamic_cast<TMenuItem*>(Sender);
	MTIassert(menuItem != nullptr || toolbarItem != nullptr);
	if (menuItem != nullptr)
		newFilter = menuItem->Tag;
	else if (toolbarItem != nullptr)
		newFilter = toolbarItem->Tag;

	if (newFilter != oldFilter)
	{
		if (newFilter == PLAYBACK_FILTER_ORIGINAL_VALUES || oldFilter == PLAYBACK_FILTER_ORIGINAL_VALUES)
		{
			// Work around a Displayer bug where it gets highly confused
			// if we switch in or out of ORIGINAL_VALUES mode while the
			// provisional frame is pending and unresolved. The "Check..."
			// method will actually try to auto-accept before checking, which
			// should usually make this relatively invisible.
			CToolManager toolManager;
			if (toolManager.CheckProvisionalUnresolvedAndToolProcessing())
			{
				// User failed to resolve the provisional, so we refuse to
				// switch modes
				newFilter = PLAYBACK_FILTER_INVALID;
			}
			//
		}

		if (newFilter != PLAYBACK_FILTER_INVALID)
		{
			// Do it!
			GetPlayer()->setPlaybackFilter(newFilter);
			UpdatePlaybackFilterStatus();
			if (!GetPlayer()->isDisplaying())
				GetPlayer()->refreshFrameSynchronous();
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TimeDisplayMenuItenClick(TObject *Sender)
{
	PTimecode ptc;
	switch (ptc.getDefaultUserPreferredStringStyle())
	{
	case PTimecode::ForceFrameNumberStyle:
		ForceFrameNumberDisplayMenuItem->Checked = true;
		break;

	case PTimecode::ForceTimecodeStyle:
		ForceTimecodeDisplayMenuItem->Checked = true;
		break;

	default:
		UseClipDefaultModeMenuItem->Checked = true;
		break;
	}

	// Absolute value of default FPS is FPS, negative indicates drop frame!
	int fps;
	bool df;
	ptc.getDefaultFpsAndDropFrame(fps, df);
	switch (fps)
	{
	case 25:
		Fps25MenuItem->Checked = true;
		break;

	case 30:
		Fps30DfMenuItem->Checked = df;
		Fps30MenuItem->Checked = !df;
		break;

	case 50:
		Fps50MenuItem->Checked = true;
		break;

	case 60:
		Fps60DfMenuItem->Checked = df;
		Fps60MenuItem->Checked = !df;
		break;

	default:
		Fps24MenuItem->Checked = true;
		break;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TimeDisplayDefaultModeMenuItemClick(TObject *Sender) {
	GetTimeline()->TimeDisplayDefaultModeMenuItemClick(Sender);}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FpsMenuItemClick(TObject *Sender) {GetTimeline()->FpsMenuItemClick(Sender);}
// ---------------------------------------------------------------------------

void TmainWindow::ExecuteNavigatorToolCommand(int command)
{
	// This function passes a command to the Navigator tool, eventually
	// making its way to the CNavigatorTool's onToolCommand function

	// Ask the Tool Manager for the Navigator tool
	CToolManager toolManager;
	string toolName = "Navigator"; // TTT This shouldn't be hard-coded
	int toolIndex = toolManager.findTool(toolName);
	if (toolIndex < 0)
		return;

	CToolObject *toolObj = toolManager.GetTool(toolIndex);
	if (toolObj == 0)
		return;

	// Create a Tool Command object and execute the caller's command
	CToolCommand toolCommand(toolObj, command);
	toolCommand.execute();

	// Kludge, send to MaskToolUnit so it can update its status
	BaseToolWindow->MaskPropertiesFrame->processToolCommand(command);
}
// ---------------------------------------------------------------------------

void TmainWindow::RunImportExportDialog(ImportExportFormBase *form)
{
	// CNavCurrentClip* currentClipHandler = mainWindow->GetCurrentClipHandler();
	// CPlayer *player = GetPlayer();
	//
	// if (form == 0)
	// {
	// TRACE_0(errout << "Import/Export form creation FAILED");
	// }
	// else
	// if (currentClipHandler != 0 && currentClipHandler->getCurrentClip() != 0 && player != 0)
	// {
	// if (form->InitFromClip(currentClipHandler->getCurrentClip()) == 0)
	// {
	// form->SetRange(player->getMarkInTimecode(), player->getMarkOutTimecode(),
	// player->getCurrentTimecode());
	// form->Run();
	// }
	// else
	// {
	// TRACE_0(errout << "Import/Export InitFromClip FAILED");
	// }
	// }
	// else
	// {
	// // QQQ should disable the menu item...
	// _MTIErrorDialog(Handle, "Please select a clip first");
	// }
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ImportImageFilesMenuItemClick(TObject *Sender)
{
	// xxxmfioxxx
	// ImportExportFormBase *form =
	// ImportExportFormBase::GetForm(ImportExportFormBase::IMPORT_FILES_DIALOG);
	//
	// RunImportExportDialog(form);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ExportImageFilesMenuItemClick(TObject *Sender)
{
	// xxxmfioxxx
	// ImportExportFormBase *form =
	// ImportExportFormBase::GetForm(ImportExportFormBase::EXPORT_FILES_DIALOG);
	//
	// RunImportExportDialog(form);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::MaskBorderMenuItemClick(TObject *Sender)
{
	///BaseToolWindow->ShowHidePropertiesButton->Down = !BaseToolWindow->ShowHidePropertiesButton->Down;
}

// ---------------------------------------------------------------------------

void TmainWindow::UpdateMaskToolGUI(CMaskTool &maskTool)
{
   BaseToolWindow->MaskPropertiesFrame->UpdateSettings(maskTool);
   BaseToolWindow->UpdateMaskToolGUI(maskTool);

   // WHAT IN THE ACTUAL FUCKERY IS THIS?????
   // Ask the Tool Manager for the Paint tool
   CToolManager toolManager;
   CToolObject *toolObj = nullptr;
   string toolName = "Paint"; // TTT This shouldn't be hard-coded
   int toolIndex = toolManager.findTool(toolName);
   if (toolIndex >= 0)
   {
      toolObj = toolManager.GetTool(toolIndex);
   }

   if (toolObj == nullptr)
   {
      // No Paint tool!!!
      return;
   }

   auto cmd1 = maskTool.IsMaskToolActive() ? NAV_CMD_MASK_TOOL_ENABLE : NAV_CMD_MASK_TOOL_DISABLE;
   CToolCommand toolCommand1(toolObj, cmd1);
   toolCommand1.execute();

   auto shape = maskTool.getRegionShape();
   auto cmd2 = (shape == MASK_REGION_BEZIER)
               ? NAV_CMD_MASK_DRAW_BEZIER
               : ((shape == MASK_REGION_LASSO)
                  ? NAV_CMD_MASK_DRAW_LASSO
                  : NAV_CMD_MASK_DRAW_RECT);
   CToolCommand toolCommand2(toolObj, cmd2);
   toolCommand2.execute();

   auto cmd3 = maskTool.getGlobalInclusionFlag() ? NAV_CMD_MASK_INCLUDE : NAV_CMD_MASK_EXCLUDE;
   CToolCommand toolCommand3(toolObj, cmd3);
   toolCommand3.execute();

   auto cmd4 = maskTool.IsMaskVisible() ? NAV_CMD_MASK_SHOW_OUTLINE : NAV_CMD_MASK_SHOW_NOTHING;
   CToolCommand toolCommand4(toolObj, cmd4);
   toolCommand4.execute();
}

void TmainWindow::GatherMaskToolSettings(CMaskTool &maskTool) {
	BaseToolWindow->MaskPropertiesFrame->GetSettings(maskTool);}

// ---------------------------------------------------------------------------
// makes the reticle display mode buttons sticky
//
void TmainWindow::UpdateReticleToolbarModeButtons()
{
	int mode = GetReticleMode();
	GetTimeline()->UpdateReticleToolbarModeButtons(mode);
}

// ---------------------------------------------------------------------------
// updates the reticle toolbar combo box with the name of the current reticle
//
void TmainWindow::UpdateReticleToolbarComboBox()
{
	CReticle currentReticle = GetDisplayer()->getReticle();
	GetTimeline()->UpdateReticleToolbarComboBox(currentReticle);
}

// ---------------------------------------------------------------------------

void TmainWindow::ReadReticleListIni()
{
	// build the contents of the reticle combo pulldown
	auto timeline = GetTimeline();
	if (timeline == nullptr)
	{
		return;
	}

	TStrings *reticleItems = timeline->ReticleToolbarComboBox->Items;
	reticleItems->Clear();

	// load the default reticles into the pulldown
	for (int i = RETICLE_TYPE_NONE; i <= HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX; i++)
	{
		reticleItems->Add(defaultReticle[i].c_str());
	}

	// load the custom reticles into the pulldown
	CIniFile *ini = CreateIniFile(RETINIFILE);
	StringList strlReticles;
	ini->ReadSection(reticleSectionName, &strlReticles);
	for (StringList::iterator iter = strlReticles.begin(); iter != strlReticles.end(); ++iter)
	{

		string strTemp = ini->ReadString(reticleSectionName, *iter, "");

		int endIndex = strTemp.find("]");

		// add the custom reticle to the dropdown list
		reticleItems->Add(strTemp.substr(1, endIndex - 1).c_str());
	}

	DeleteIniFile(ini);
}

// ---------------------------------------------------------------------------

MTI_UINT32 **TmainWindow::GetFeatureArray() {return FeatureArray;}

// ----------------ReadSettings---------------------John Mertus-----Jan 2003---

bool TmainWindow::ReadSettings(CIniFile *ini, const string &IniSection)

	 // This reads the init file and sets the saved position and size
	 //
	 // ******************************************************************************
{
	if (ini == nullptr || CNavSystemInterface::getActiveWindow() != mainWindow)
		return (false);

	// NOTE: Timeline form undocking is no longer supported!
	// TimelineFrame->DockedState(ini->ReadBool(IniSection, "TimeLineDocked", true));
	// so we need to call TimelineFrame->ReadXXX() directly!
	TimelineFrame->ReadSAP(ini, "TimelineForm");
	TimelineFrame->ReadSettings(ini, "TimelineForm");

	return true;
}

// ----------------WriteSettings----------------John Mertus-----Jan 2003---

bool TmainWindow::WriteSettings(CIniFile *ini, const string &IniSection)

	 // This writes the ini files, saves the position and size
	 //
	 // ******************************************************************************
{
	if (ini == nullptr || CNavSystemInterface::getActiveWindow() != mainWindow)
		return (false);

	// NOTE: Timeline form undocking is no longer supported!
	// ini->WriteBool(IniSection, "TimeLineDocked", TimelineFrame->DockedState());
	// so we need to call TimelineFrame->WriteXXX() directly!

	// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	// CAN'T FIGURE OUT WHY TimelineFrame MIGHT BE nullptr HERE!
	MTIassert(TimelineFrame);
	if (TimelineFrame)
		 // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	{
		TimelineFrame->WriteSettings(ini, "TimelineForm");
		TimelineFrame->WriteSAP(ini, "TimelineForm");
	}

	return true;
}
// ---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// -------------------------------------------------------------------------//
// -------------------- LUT STUFF ------------------------------------------//
// -------------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////////

void __fastcall TmainWindow::LUTSubmenuClick(TObject *Sender)
{
	string selectedUserLutName;
	ELutSelection selectedLut = lutSettingsManager->GetLutSelection(selectedUserLutName);

	NoLutMenuItem->Checked = (selectedLut == LUTSEL_NONE);
	ProjectLutMenuItem->Checked = (selectedLut == LUTSEL_PROJECT);
	ClipLutMenuItem->Checked = (selectedLut == LUTSEL_CLIP);

	AddUserLutsToLutMenu();
}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateUserLutMenuItemCaptionList()
{
	// Makes a new list user lut names in the order that they should appear
	// in the View>LUT menu - we add a three character prefix to the front
	// of each name, "&s "  where 's' is the shortcut character for the LUT
	// name, or "~  " if the LUT name has no shortcut; we then sort this list
	// so the shortcut names come first and the rest are ascii-sorted
	//
	// QQQ really should alpha sort (ignore case) rather than ascii sort!!!

	UserLutMenuItemCaptionList.clear();

	StringList userLutNameList;
	lutSettingsManager->GetUserLutSettingsNames(userLutNameList);
	size_t userLutNameCount = userLutNameList.size();

	for (size_t i = 0; i < userLutNameCount; ++i)
	{
		// We need to sort the names such that the ones with shortcuts
		// appear first in shortcut character order and the rest appear
		// sorted alphabetically

		char shortcut = lutSettingsManager->GetUserLutShortcut(userLutNameList[i]);

		// prefix will be "&s " (where s is the shortcut character) for items
		// with shortcuts and will be "~  " for items without shortcuts (the
		// leading "~" is temporarily inserted for sorting purpose and changed
		// to a space later

		string prefix = "~  ";

		string menuItemCaption;

		if (shortcut == ' ')
			menuItemCaption = string("~  ") + userLutNameList[i];
		else
			menuItemCaption = string("&") + shortcut + " " + userLutNameList[i];

		UserLutMenuItemCaptionList.push_back(menuItemCaption);
	}

	// NAMES ARE NOW PROVIDED PRE-SORTED, so everyone gets the same ordering!
	// Sort the list if there's more than one entry
	// if (userLutNameCount > 1)
	// std::sort(UserLutMenuItemCaptionList.begin(),
	// UserLutMenuItemCaptionList.end());
}
// ---------------------------------------------------------------------------

void TmainWindow::AddUserLutsToLutMenu()
{
	string selectedUserLutName;
	ELutSelection lutSelection = lutSettingsManager->GetLutSelection(selectedUserLutName);
	bool userLutSelected = (lutSelection == LUTSEL_USER);

	// Delete all non-hardwired LUT menu items (from index
	// NUMBER_OF_HARDWIRED_LUT_MENU_ENTRIES on)
	for (int i = (LUTSubmenu->Count - 1); i >= NUMBER_OF_HARDWIRED_LUT_MENU_ENTRIES; --i)
	{
		LUTSubmenu->Delete(i);
	}

	size_t userLutNameCount = UserLutMenuItemCaptionList.size();
	if (userLutNameCount == 0)
	{
		// Nothing to add - hide the separator
		BuiltInLutMenuSeparatorItem->Visible = false;
		return;
	}
	BuiltInLutMenuSeparatorItem->Visible = true;

	bool noShortcuts = true;
	for (size_t i = 0; i < userLutNameCount; ++i)
	{
		if (UserLutMenuItemCaptionList[i][0] == '&')
		{
			noShortcuts = false;
			break;
		}
	}

	for (size_t i = 0; i < userLutNameCount; ++i)
	{
		string menuItemCaption = UserLutMenuItemCaptionList[i];
		string userLutName = menuItemCaption.substr(3);

		// If there are no shortcuts, don't put spaces in front of all the items
		if (noShortcuts)
		{
			menuItemCaption = userLutName;
		}
		else
		{
			// Erase '~' that was inserted for sorting
			if (menuItemCaption[0] == '~')
				menuItemCaption = menuItemCaption.substr(1);
		}
	}
}
// ---------------------------------------------------------------------------

// NOTE: CALL UpdateUserLutMenuItemCaptionList() FIRST!!

void TmainWindow::AddUserLutsToLutToolbar(TComboBox *lutComboBox)
{
	string selectedUserLutName;
	ELutSelection lutSelection = lutSettingsManager->GetLutSelection(selectedUserLutName);
	bool userLutSelected = (lutSelection == LUTSEL_USER);

	lutComboBox->Items->Clear();

	// QQQ THESE AWFUL MANIFEST CONSTANTS ARE SHARED WITH AddUserLutsToLutToolbar
	lutComboBox->Items->Append("[ Clip LUT ]");
	const int clipItemIndex = 0;

	lutComboBox->Items->Append("[ Project LUT ]");
	const int projectItemIndex = 1;

	int selectedUserLutIndex = -1;
	int firstUserLutItemIndex = lutComboBox->Items->Count;
	MTIassert(firstUserLutItemIndex == 2);
	size_t userLutNameCount = UserLutMenuItemCaptionList.size();

	for (size_t i = 0; i < userLutNameCount; ++i)
	{
		string menuItemCaption = UserLutMenuItemCaptionList[i];
		string userLutName = menuItemCaption.substr(3);
		lutComboBox->Items->Append(userLutName.c_str());
		if (userLutSelected && (userLutName == selectedUserLutName))
			selectedUserLutIndex = i + firstUserLutItemIndex;
	}

	int selectedItemIndex = -1;
	switch (lutSelection)
	{
	case LUTSEL_NONE:
		// Don't really know what to do here!!
		/* FALL THROUGH */
	case LUTSEL_CLIP:
		selectedItemIndex = clipItemIndex;
		break;
	case LUTSEL_PROJECT:
		selectedItemIndex = projectItemIndex;
		break;
	case LUTSEL_USER:
		selectedItemIndex = selectedUserLutIndex;
		break;
	}

	MTIassert(selectedItemIndex != -1);
	lutComboBox->ItemIndex = selectedItemIndex;
}
// ---------------------------------------------------------------------------

// This shifts the displayer over to CConvert's internal LUT
//
void __fastcall TmainWindow::NoLutMenuItemClick(TObject *Sender)
{
#if 0
	GetDisplayer()->setCustomLUTOption(LUT_OPTION_INTERNAL);
	GetDisplayer()->establishDisplayLUT();
	if (!GetPlayer()->isDisplaying())
		GetPlayer()->refreshFrameSynchronous();
#endif
}
// ---------------------------------------------------------------------------

// This shifts the displayer over to the clip's default LUT
//
void __fastcall TmainWindow::DefaultLUTMenuItemClick(TObject *Sender)
{
#if 0
	GetDisplayer()->setCustomLUTOption(LUT_OPTION_DEFAULT);
	GetDisplayer()->establishDisplayLUT();
	if (!GetPlayer()->isDisplaying())
		GetPlayer()->refreshFrameSynchronous();
#endif
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::EditLutMenuItemClick(TObject *Sender)
{
   EditLutSettings();
}
// ---------------------------------------------------------------------------

void TmainWindow::EditLutSettings()
{
	LutSettingsEditor->SetLutSettingsManager(lutSettingsManager);
	LutSettingsEditor->SetDisplayer(GetDisplayer());
	LutSettingsEditor->SetDefaultGammaTimes10(GetGammaPreference());
	ShowModalDialog(LutSettingsEditor);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ProjectLutMenuItemClick(TObject *Sender)
{
   lutSettingsManager->SelectLut(LUTSEL_PROJECT);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ClipLutMenuItemClick(TObject *Sender)
{
   lutSettingsManager->SelectLut(LUTSEL_CLIP);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::UserLUTMenuItemClick(TObject *Sender)
{
	int tag;

	// We only have to downcast to TComponent because that's where Tag is,
	// and we are too lazy to try to figure out how to suck the caption
	// out of the menu

	TComponent *component = dynamic_cast<TComponent*>(Sender);
	if (component != nullptr)
		tag = component->Tag;
	else
		return; // Should really ASSERT here

	// Get the lut name, stripping leading shortcut stuff
	string userLutName = UserLutMenuItemCaptionList[tag].substr(3);

	// Select the lut and apply the settings
	lutSettingsManager->SelectLut(LUTSEL_USER, &userLutName);
}

// ---------------------------------------------------------------------------
void TmainWindow::SelectLut(int lutListIndex)
{
	ELutSelection lutSelection = LUTSEL_NONE;
	string selectedUserLutName;

	// QQQ THESE AWFUL MANIFEST CONSTANTS ARE SHARED WITH AddUserLutsToLutToolbar
	switch (lutListIndex)
	{
	case 0:
		lutSelection = LUTSEL_CLIP;
		break;

	case 1:
		lutSelection = LUTSEL_PROJECT;
		break;

	default:
		if (lutListIndex > 1)
		{
			int selectedUserLutIndex = lutListIndex - 2;
			MTIassert(selectedUserLutIndex < (int)UserLutMenuItemCaptionList.size());
			if (selectedUserLutIndex < (int)UserLutMenuItemCaptionList.size())
			{
				lutSelection = LUTSEL_USER;
				selectedUserLutName = UserLutMenuItemCaptionList[selectedUserLutIndex].substr(3);
			}
		}
		break;
	}

	lutSettingsManager->SelectLut(lutSelection, &selectedUserLutName);

	// Move focus away from the combo box so various keys like arrow up and
	// down don't get swallowed!

	FocusOnMainWindow();
}
// ---------------------------------------------------------------------------

void TmainWindow::LoadAndApplyLutSettings(ClipSharedPtr &clip)
{
   lutSettingsManager->LoadAndApplyAllNewClipSettings(clip);
}
// ---------------------------------------------------------------------------

// THIS IS A CALLBACK from the LutSettingsManager

void TmainWindow::LutSettingsHaveChanged(void *Sender)
{
	UpdateUserLutMenuItemCaptionList();
	AddUserLutsToLutToolbar(BaseToolWindow->LutToolbarComboBox);

	RefreshFrameHackTimer->Interval = 200;
	RefreshFrameHackTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::RefreshFrameHackTimerTimer(TObject *Sender)
{
	RefreshFrameHackTimer->Enabled = false;
	GetPlayer()->refreshFrame();
}
// ---------------------------------------------------------------------------

// This shifts the displayer over to the clip's BIN LUT
//
// DEPRECATED
//
void __fastcall TmainWindow::BinLUTMenuItemClick(TObject *Sender)
{
#if 0
	CBinManager binManager;
	auto currentClip = GetCurrentClipHandler()->getCurrentClip();
	string binPath = GetCurrentClipHandler()->getCurrentBinPath();
	binManager.setBinDisplayLutChoice(binPath, BIN_DISPLAY_LUT_CUSTOM);
	if (!binManager.doesBinHaveCustomLUT(binPath))
		EditBinLUT(binPath);
	GetCurrentClipHandler()->loadCustomLUT(GetCurrentClipHandler()->getCurrentClip(), LUT_OPTION_BIN,
		 GetDisplayer()->getDefaultGamma());
	GetDisplayer()->establishDisplayLUT();
	if (!GetPlayer()->isDisplaying())
		GetPlayer()->refreshFrameSynchronous();
#endif
}
// ---------------------------------------------------------------------------

// DEPRECATED
//
void __fastcall TmainWindow::EditBinLUTMenuItemClick(TObject *Sender)
{
#if 0
	CBinManager binManager;
	CNavCurrentClip *currentClipHandler = GetCurrentClipHandler();
	string binPath = currentClipHandler->getCurrentBinPath();

	binManager.setBinDisplayLutChoice(binPath, BIN_DISPLAY_LUT_CUSTOM);
	currentClipHandler->loadCustomLUT(currentClipHandler->getCurrentClip(), LUT_OPTION_BIN,
		 GetDisplayer()->getDefaultGamma());

	EditBinLUT(binPath);

	currentClipHandler->loadCustomLUT(currentClipHandler->getCurrentClip(), LUT_OPTION_BIN,
		 GetDisplayer()->getDefaultGamma());
	GetDisplayer()->establishDisplayLUT();
	if (!GetPlayer()->isDisplaying())
		GetPlayer()->refreshFrameSynchronous();
#endif
}
// ---------------------------------------------------------------------------

#if 0

void TmainWindow::EditBinLUT(const string &binPath)
{
	CBinManager binManager;

	if (BinDisplayLUTEditor == 0)
		BinDisplayLUTEditor = new TBinDisplayLUTEditor(mainWindow);

	BinDisplayLUTEditor->Edit(binPath);
}
#endif
// ---------------------------------------------------------------------------

bool TmainWindow::PreflightChecklist()
{
	bool bRet = true;
	int iRet;

	iRet = ReadSystemID();
	if (iRet == SYSTEM_ID_NO_ADAPTERS)
	{
		_MTIErrorDialog(Handle, "It appears that no network adapters could be found\n" \
 "  on this system.  Perhaps they are disabled?  MTI\n" \
 "  software will not work in this configuration.");
		bRet = false;
	}
	else if (iRet != SYSTEM_ID_SUCCESS)
	{
		_MTIErrorDialog(Handle, "This software is not licensed for this machine.\n" \
 "  Please call Mathematical Technologies, Inc.");
		bRet = false;
	}
	return bRet;
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormShow(TObject *Sender)
{
#ifndef NO_QLM_LICENSING
	// Create the base name
	char baseName[5];
	baseName[4] = 0;
	OBFUS_4(baseName, 0x54, 'B', 'a', 's', 'e');
	this->m_licenseBaseName = baseName;

	if (!GetMtiLicense()->IsFeatureLicensed(this->m_licenseBaseName))
	{
		MTIErrorDialog(GetMtiLicense()->ValidateErrorMessage);
		exit(0);
	}
#endif
	FocusControl(DummyFocusTrackBar);

	// We want to do shortcuts only when the ALT key is held down
	SetMenuShortcutsAreOkNow(false);

	DisplayPanel->OnChange = this->RefreshDisplayWindow;

	// A non-zero number
#ifndef NO_QLM_LICENSING
	this->licenseIsOK = GetMtiLicense()->IsBaseValid;
#endif
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::BookmarkEventViewerMenuItemClick(TObject *Sender)
{
	if (BookmarkEventViewerForm == nullptr)
	{
		BookmarkEventViewerForm = new TBookmarkEventViewerForm(this);
	}

	if (BookmarkEventViewerMenuItem->Checked)
	{
		BookmarkEventViewerForm->Show();
	}
	else
	{
		BookmarkEventViewerForm->Hide();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ShowPDLViewerMenuItemClick(TObject *Sender)
{
	// Show the active PDL Viewer.  If one does not exist, creates it.
	// Sets focus to the active PDL Viewer

	CPDLViewerManager mgr;

	if (mgr.IsActivePDLVisible())
	{
		mgr.HideActivePDL();
	}
	else
	{
		mgr.ShowActivePDL();
	}

   if (Sender == FrameBufferErrorCoverUpPanel)
   {
      mgr.CloseAll();
   }
}

// ---------------------------------------------------------------------------

bool TmainWindow::IsPDLEntryLoading()
{
	// Returns true when a PDL entry is being loading.
	// This is a semi-hack to support Dewarp
	return loadingPDLEntry;
}

int TmainWindow::GoToPDLEntry(CPDLEntry &pdlEntry)
{
	int retVal;

	loadingPDLEntry = true; // Hack so tools can find out that they are being
	// set from PDL entry.

	retVal = GoToPDLEntryHack(pdlEntry);

	loadingPDLEntry = false;

	return retVal;
}

int TmainWindow::GoToPDLEntryHack(CPDLEntry &pdlEntry)
{
	CBinManager binMgr;
	int retVal;

	string clipName = pdlEntry.srcList[0]->clipName;
	string binPath = pdlEntry.srcList[0]->binPath;
	string clipFilename = binMgr.makeClipFileName(binPath, clipName);

	// Check if clip is already open
	if (!GetCurrentClipHandler()->isThisCurrentClip(clipFilename))
	{
		OpenClipByName(clipFilename);

		// TBD: Check clip open failed
	}

	// TBD: Proxy & Framing
	// Select Framing for the current clip & proxy.  Assumes that the
	// Framing Menu Item's Tag is the framing index
	int videoFramingIndex = pdlEntry.srcList[0]->videoFramingIndex;
	if (GetCurrentClipHandler()->getCurrentVideoFramingIndex() != videoFramingIndex)
	{
		retVal = GetCurrentClipHandler()->selectVideoFraming(videoFramingIndex);
		if (retVal != 0)
			return retVal;

		// Clear the various frame file caches
		// QQQ Why??
		// MediaFileIO::ClearAllCaches();

		// Put the name of the framing view in the status bar
		UpdateFramingViewStatus();
		ClipHasChanged.Notify();

		// Rebuild the cut timeline (mbraca+20041022)
		GetTimeline()->performCutAction(CUT_REFRESH);
	}

	// Set in/out mark
	int markIn = pdlEntry.srcList[0]->inFrameIndex;
	GetPlayer()->setMarkIn(markIn);
	int markOut = pdlEntry.srcList[0]->outFrameIndex;
	GetPlayer()->setMarkOut(markOut);
	// Show the marks on the timeline (or hide them if they are not set)
	GetTimeline()->UpdateMarks();

	// Go to in mark
	GetPlayer()->goToFrameSynchronous(markIn);

	// Now activate the tool specified in the PDL entry
	string toolName = pdlEntry.toolList[0]->toolName;
	CToolManager toolMgr;
	string toolStatusMsg;
	retVal = toolMgr.CheckTool(toolName, toolStatusMsg);
	if (retVal != 0)
	{
		if (!toolMgr.IsPDLRendering())
		{
			HideAllTools(-1);
			if (retVal == TOOL_ERROR_TOOL_IS_NOT_LICENSED)
			{
				// The requested tool is not licensed, so put up License Report
				////				LicenseReportDlgBox->toolIndex = toolMgr.findTool(toolName);
				////				ShowModalDialog(LicenseReportDlgBox);
			}
			else
			{
				_MTIErrorDialog(Handle, toolStatusMsg);
			}
		}
		return retVal;
	}

	// Right way to switch tools now
	BaseToolWindow->ShowTool(toolName);

	// old wrong way
	// int toolIndex = toolMgr.findTool(toolName);
	// if (toolIndex < 0)
	// return -1;   // If CheckTool has succeeded, this will never fail
	// if (toolMgr.getActiveToolIndex() != toolIndex)
	// {
	// ShowTool(toolIndex);
	// }

	// Set the Mask
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();
	maskTool->SetMask(pdlEntry);

	// Finally we send the tool some parameters
	retVal = toolMgr.onGoToPDLEntry(pdlEntry);
	if (retVal != 0)
		return retVal;

	return 0;
}

// called by the component values display timer
// routines in both the mainWindow and fullWindow
//
void TmainWindow::UpdateComponentValuesToolbar(bool mouseInFrame)
{
	int retVal;
	MTIostringstream ostrm;
	MTIostringstream ostrmsbar;
	string emptyStr;

	if (mouseInFrame)
	{
		int clientX, clientY;
		clipDisplayer->getMousePositionClient(&clientX, &clientY);

		int imageX = clipDisplayer->scaleCursorXClientToFrame(clientX);
		ostrmsbar << "X:" << right << setw(4) << setfill('0') << imageX << "  ";

		int imageY = clipDisplayer->scaleCursorYClientToFrame(clientY);
		ostrmsbar << "Y:" << right << setw(4) << setfill('0') << imageY << "  |  ";

		MTI_UINT16 compValue[MAX_COMPONENT_COUNT];
		retVal = clipDisplayer->getSinglePixelValues(imageX, imageY, compValue);

		// Stash these for access by the tool.
		YComp = compValue[0];
		UComp = compValue[1];
		VComp = compValue[2];

		// Create string for display. 'Half' data gets floating point numbers
		CVideoFrameList *frameList = GetCurrentClipHandler()->getCurrentVideoFrameList();
		const CImageFormat *imageFormat = frameList->getImageFormat();
		EPixelPacking pixelPacking = IF_PIXEL_PACKING_INVALID;
		if (frameList != 0)
		{
			pixelPacking = imageFormat->getPixelPacking();
		}

		if (pixelPacking == IF_PIXEL_PACKING_1_Half_IN_2Bytes)
		{
			MTI_UINT16 black[MAX_COMPONENT_COUNT];
			MTI_UINT16 white[MAX_COMPONENT_COUNT];
			imageFormat->getComponentValueBlack(black);
			imageFormat->getComponentValueWhite(white);
			for (int i = 0; i < componentCount; ++i)
			{
				ostrmsbar << componentAbbrevStr[i] << ":";
				if (retVal == 0)
				{
					float blackAsFloat = ImageDatum::halfToFloat(ImageDatum::toHalf(black[i]));
					float whiteAsFloat = ImageDatum::halfToFloat(ImageDatum::toHalf(white[i]));
					float compValueAsFloat = ImageDatum::halfToFloat(ImageDatum::toHalf(compValue[i]));
					float normalizedCompValue = (compValueAsFloat - blackAsFloat) / (whiteAsFloat - blackAsFloat);
					bool minusSign = normalizedCompValue < 0.F;
					float absCompValue = fabs(normalizedCompValue);
					int intPart = int(absCompValue);
					int fracWidth = (intPart > 999) ? 1 : ((intPart > 99) ? 2 : 3);
					float fracPart = (absCompValue - intPart);
					for (int i = 0; i < fracWidth; ++i)
					{
						fracPart *= 10.F;
					}

					int fracPartAsInt = int(fracPart); // dont round
					ostrmsbar << right << (minusSign ? "-" : "") << intPart << '.' << setw(fracWidth) << setfill('0')
						 << fracPartAsInt << "  ";
				}
				else
				{
					ostrmsbar << "????  ";
				}
			}
		}
		else
		{
			for (int i = 0; i < componentCount; ++i)
			{
				ostrmsbar << componentAbbrevStr[i] << ":";
				if (retVal == 0)
					ostrmsbar << right << setw(4) << setfill('0') << compValue[i] << "  ";
				else
					ostrmsbar << "????  ";
			}
		}
	}
	else
	{
		ostrmsbar << "X:---- Y:----";

		if (componentCount > 0)
			ostrmsbar << " | ";

		for (int i = 0; i < componentCount; ++i)
		{
			ostrmsbar << componentAbbrevStr[i] << ":---- ";
		}
	}

	GetTimeline()->RGBPanel->Caption = ostrmsbar.str().c_str();
}

// this method displays the component values.  Since the component values should
// only be displayed when the mouse is in a viewing frame, this method also controls
// whether or not the mouse is a crosshair (discerning if the mouse is in the frame
// is essential to both processes).
void TmainWindow::UpdateSomeGUIStuff()
{
	bool mouseInFrame = isMouseInFrame();

	// show the appropriate cursor -- pass flag to force arrow cursor if
	// mouse is not in frame
	clipDisplayer->updateCursor(!mouseInFrame || m_capturedMouse);

	// show color components of point at mouse position
	UpdateComponentValuesToolbar(mouseInFrame);

	// show the actual frames per second display speed
	UpdateFPSActual();
}

void TmainWindow::ShowCursor(bool showFlag) {clipDisplayer->showCursor(showFlag, isMouseInFrame());}

void TmainWindow::InitComponentValuesDisplay()
{
	CVideoFrameList *frameList = GetCurrentClipHandler()->getCurrentVideoFrameList();
	EPixelComponents pixelComponents = IF_PIXEL_COMPONENTS_INVALID;
	if (frameList != 0)
	{
		const CImageFormat *imageFormat = frameList->getImageFormat();
		pixelComponents = imageFormat->getPixelComponents();
	}

	switch (pixelComponents)
	{
//	case IF_PIXEL_COMPONENTS_LUMINANCE: // Luminance only, B&W
//		componentAbbrevStr[0] = "Luminance";
//		componentCount = 1;
//		break;
	case IF_PIXEL_COMPONENTS_YUV422: // CbYCrY
	case IF_PIXEL_COMPONENTS_YUV4224: // YUV + alpha, CbYACrYA
	case IF_PIXEL_COMPONENTS_YUV444: // CbYCr
		componentAbbrevStr[0] = "Y";
		componentAbbrevStr[1] = "U";
		componentAbbrevStr[2] = "V";
		componentCount = 3;
		break;
	case IF_PIXEL_COMPONENTS_RGB:
	case IF_PIXEL_COMPONENTS_RGBA: // A = alpha matte channel
	case IF_PIXEL_COMPONENTS_BGR: // BGR format as found in TGA files
	case IF_PIXEL_COMPONENTS_BGRA: // A = alpha matte channel
	case IF_PIXEL_COMPONENTS_Y:
	case IF_PIXEL_COMPONENTS_YYY:
		componentAbbrevStr[0] = "R";
		componentAbbrevStr[1] = "G";
		componentAbbrevStr[2] = "B";
		componentCount = 3;
		break;
	case IF_PIXEL_COMPONENTS_INVALID:
	default:
		componentCount = 0;
		break;
	}
}

// ---------------------------------------------------------------------------

int TmainWindow::GetYcomp() {return YComp;}

int TmainWindow::GetUcomp() {return UComp;}

int TmainWindow::GetVcomp() {return VComp;}

int TmainWindow::getBitsPerComponent() {return clipDisplayer->getBitsPerComponent();}

EPixelComponents TmainWindow::getPixelComponents() {return clipDisplayer->getPixelComponents();}

bool TmainWindow::isPointInFrame(POINT pt) {return clipDisplayer->isPointInFrameRect(pt);}

bool TmainWindow::isMouseInFrame()
{
	// Just use enter and leave in the panel to decide if mouse is in frame
	return this->mouseIsInDisplayPanel && currentClipHandler->isClipAvailable() && clipDisplayer->isMouseInFrame();
}

// Move the mouse to the center of the display panel, but ONLY IF THE MOUSE
// IS NOT PRESENTLY IN THE DISPLAY PANEL!
void TmainWindow::forceMouseToWindowCenter()
{
	// NOT isMouseInFrame()! We care about the gray area, too!
	if (!this->mouseIsInDisplayPanel)
	{
		clipDisplayer->forceMouseToWindowCenter(false);
	}
}

void TmainWindow::captureMouse()
{
	FocusOnMainWindow(); // SIDE EFFECT!
	if (!m_capturedMouse)
	{
		SetCaptureControl(this);
		m_capturedMouse = true;
	}
}

void TmainWindow::releaseMouse()
{
	if (m_capturedMouse)
	{
		ReleaseCapture();
		m_capturedMouse = false;
	}
}

// This method is called by the preferenceunit to set the default
// cursor type. Called by the preferenceunit on creation and when                                       /navigato
// the user implicitly sets the preference.
void TmainWindow::SetCursor(int type)
{
	if (type == 0)
	{
		clipDisplayer->setPreferredFrameCursor(crCross);
		SetBusyCursorToBusyCross();
	}
	else
	{
		clipDisplayer->setPreferredFrameCursor(crArrow);
		SetBusyCursorToDefault();
	}
}

// This method is called by the preferenceunit to enable or disable
// the "alternate image folder scan method" feature
bool TmainWindow::SetAltImageFolderScanMethod(bool onOffFlag)
{
	CImageFileMediaAccess::UseBinarySearchToFindAllFiles(onOffFlag);
	return onOffFlag;
}

// This method is called by the preferenceunit to enable or disable
// the "swap W and shift-W" feature
void TmainWindow::SetSwapWAndShiftWFlag(bool swapEm) {swapWAndShiftWFlag = swapEm;}

bool TmainWindow::GetSwapWAndShiftWFlag() {return swapWAndShiftWFlag;}

// this method is called by the preferenceunit to set the number of pages
// that should be allocated for the DPS Image Body Read Cache

int TmainWindow::SetFrameCacheSize(size_t sizeInPages,
	 /* OUT */ size_t *actualNumberOfPagesAllocated)
{
	int retVal = MediaFileIO::SetFrameReadCacheSize(sizeInPages,
		 /* out */ actualNumberOfPagesAllocated);
	return retVal;
}

void TmainWindow::ClearFrameCache()
{
	// Clear cache. Try to make sure the player doesn't hold on to any files as well.
	GetPlayer()->fullStop();
	MediaFileIO::ClearFrameReadCache();
	GetPlayer()->refreshFrame();
}

void TmainWindow::LockCacheFramesInMemory(bool flag)
{
   MediaFileIO::LockReadCacheFramesInMemory(flag);
}

int TmainWindow::GetCurrentFrameIndex()
{
	CPlayer *player = GetPlayer();
	return player->getLastFrameIndex();
}

CTimecode TmainWindow::GetCurrentlyDisplayedTimecode()
{
	CPlayer *player = GetPlayer();
	int frameIndex = player->getLastFrameIndex();

	if (frameIndex < 0)
	{
		return CTimecode::NOT_SET;
	}

	CVideoFrameList *frameList = GetCurrentClipHandler()->getCurrentVideoFrameList();
	if (frameList == nullptr)
	{
		return CTimecode::NOT_SET;
	}

	return frameList->getTimecodeForFrameIndex(frameIndex);
}

string TmainWindow::GetSanitizedTimecodeStringFromFrameIndex(int frameIndex)
{
	CVideoFrameList *frameList = GetCurrentClipHandler()->getCurrentVideoFrameList();
	if (frameList == nullptr)
	{
		return "";
	}

	if (frameIndex < 0)
	{
		return "";
	}

	PTimecode ptc = frameList->getTimecodeForFrameIndex(frameIndex);
	string ptcAsString = StringTrim(string(ptc));
	for (unsigned int i = 0; i < ptcAsString.size(); ++i)
	{
		if (ptcAsString[i] == ':' || ptcAsString[i] == '.')
		{
			ptcAsString[i] = '_';
		}
	}

	return ptcAsString;
}

// This method is called to tell the file caches that the
// mark in, mark out or current frame has changed
void TmainWindow::SetFrameCacheHints(void *Sender)
{
	CPlayer *player = GetPlayer();
	MediaFileIO::SetFrameReadCacheHints(player->getLastFrameIndex(), player->getMarkIn(), player->getMarkOut());

	// QQQ Doesn't belong here - either rename this function or use separate callback!
	string markInString = GetSanitizedTimecodeStringFromFrameIndex(player->getMarkIn());
	string markOutString = GetSanitizedTimecodeStringFromFrameIndex(player->getMarkOut());
	JobManager jobManager;
	jobManager.SetMarkStrings(markInString, markOutString);

	// QQQ Doesn't belong here - either rename this function or use separate callback!
	BookmarkManager bookmarkManager;
	bookmarkManager.SetCurrentTimecode(player->getCurrentTimecode());
}

// Retrieve the list of the indices of the currently cached frames
const IntegerList &TmainWindow::GetCachedFrameList()
{
	// return MediaFrameFileReader::GetListOfCachedImageBodyFrameIndices();
	static IntegerList emptyList;
	return emptyList;
}

size_t TmainWindow::GetSystemPageSizeInBytes() {return SysInfo::GetSystemPageSize();}

int TmainWindow::GetImageBodyReadDimensions(int &pixelsPerLine, int &linesPerFrame)
{
	auto clip = GetCurrentClipHandler()->getCurrentClip();
	if (clip == nullptr)
	{
		pixelsPerLine = 0;
		linesPerFrame = 0;
		return -1;
	}

	const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
	pixelsPerLine = imageFormat->getPixelsPerLine();
	linesPerFrame = imageFormat->getLinesPerFrame();
	return 0;
}

int TmainWindow::GetFrameCacheSizeInFrames()
{
	CVideoFrameList* videoFrameList = GetCurrentClipHandler()->getCurrentVideoFrameList();
	if (videoFrameList == nullptr)
	{
		return 0;
	}

	size_t cachedFrameSizeInBytes = videoFrameList->getMaxPaddedFieldByteCount();
	if (cachedFrameSizeInBytes == 0)
	{
		return 0;
	}

	return (int) MediaFileIO::GetFrameReadCacheSizeInBytes() / cachedFrameSizeInBytes;
}

// ---------------------------------------------------------------------------

void TmainWindow::SetPanMode(int newPanMode) {clipDisplayer->setPanMode(newPanMode);}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateJustificationToolbarButtons()
{
	int justify = GetDisplayer()->getImageJustification();
	GetTimeline()->UpdateJustifyButtons(justify);
}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateFrameCompareToolbarButtons()
{
	auto mode = GetPlayer()->getFrameCompareMode();
	int buttonNumber = (mode == CPlayer::FrameCompareMode::Wipe) ? 1 :
		 ((mode == CPlayer::FrameCompareMode::SideBySide) ? 2 : 0);
	GetTimeline()->UpdateFrameCompareToolbarButtons(buttonNumber);
}
// ---------------------------------------------------------------------------

void TmainWindow::LeftJustifyTheDisplay()
{
	GetDisplayer()->setImageJustification(-1);
	UpdateJustificationToolbarButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::CenterJustifyTheDisplay()
{
	GetDisplayer()->setImageJustification(0);
	UpdateJustificationToolbarButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::RightJustifyTheDisplay()
{
	GetDisplayer()->setImageJustification(1);
	UpdateJustificationToolbarButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleDualFrameCompareMode()
{
	CToolManager toolManager;
	bool previewing = toolManager.IsProvisionalPending() || toolManager.IsToolProcessing() ||
		 GetDisplayer()->arePaintToolChangesPending();
	if (GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		toolManager.NotifyEndViewOnlyMode();
		GetDisplayer()->setViewOnlyMode(false);
	}
	else if (!previewing)
	{
		toolManager.NotifyStartViewOnlyMode();
		GetDisplayer()->setViewOnlyMode(true);
	}

	auto oldCompareMode = GetPlayer()->getFrameCompareMode();
	auto newCompareMode = (oldCompareMode != CPlayer::FrameCompareMode::SideBySide) ?
		 CPlayer::FrameCompareMode::SideBySide : CPlayer::FrameCompareMode::Off;
	GetPlayer()->setFrameCompareMode(newCompareMode);
	memset(&_oldUsableClientRect, 0, sizeof(_oldUsableClientRect));
	UpdateFrameCompareLabelPositions();
	UpdateFrameCompareToolbarButtons();

	if (!clipPlayer->isDisplaying() && !toolManager.IsToolProcessing())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleFrameWipeCompareMode()
{
	CToolManager toolManager;
	bool previewing = toolManager.IsProvisionalPending() || toolManager.IsToolProcessing() ||
		 GetDisplayer()->arePaintToolChangesPending();
	if (GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::Wipe)
	{
		// Exiting from Wipe mode.
		toolManager.NotifyEndViewOnlyMode();
		GetDisplayer()->setViewOnlyMode(false);
	}
	else
	{
		if (!previewing)
		{
			// Entering Wipe mode.
			toolManager.NotifyStartViewOnlyMode();
			GetDisplayer()->setViewOnlyMode(true);
		}
		else
		{
			// Put the divider on the left, vertical. Don't call
			// moveDivider() or changeDividerAngle because we need these
			// to work before changeing modes to avoid flashing.
			GetPlayer()->changeFrameCompareWiperOriginRelative(float(-1.F), false);
			GetPlayer()->changeFrameCompareWiperAngle(float(90.F), false);
		}
	}

	auto oldCompareMode = GetPlayer()->getFrameCompareMode();
	auto newCompareMode = (oldCompareMode != CPlayer::FrameCompareMode::Wipe) ? CPlayer::FrameCompareMode::Wipe :
		 CPlayer::FrameCompareMode::Off;
	GetPlayer()->setFrameCompareMode(newCompareMode);
	memset(&_oldUsableClientRect, 0, sizeof(_oldUsableClientRect));
	UpdateFrameCompareLabelPositions();
	UpdateFrameCompareToolbarButtons();

	if (!clipPlayer->isDisplaying() && !toolManager.IsToolProcessing())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleShowFrameCompareClipNames()
{
	_showFrameCompareClipNames = !_showFrameCompareClipNames;
	auto mode = GetPlayer()->getFrameCompareMode();
	if (mode != CPlayer::FrameCompareMode::Off)
	{
		memset(&_oldUsableClientRect, 0, sizeof(_oldUsableClientRect));
		UpdateFrameCompareLabelPositions();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::SetFrameCompareClipNames(const string &leftClipNameArg, const string &rightClipNameArg)
{
	string leftClipName = leftClipNameArg; // string(" ") + leftClipNameArg + string (" ");
	string rightClipName = rightClipNameArg; // string(" ") + rightClipNameArg + string (" ");

	bool changed = false;
	if (leftClipName.c_str() != LeftFrameClipNameLabelWhite->Caption)
	{
		LeftFrameClipNameLabelWhite->Caption = leftClipName.c_str();
		changed = true;
	}

	if (rightClipName.c_str() != RightFrameClipNameLabelWhite->Caption)
	{
		RightFrameClipNameLabelWhite->Caption = rightClipName.c_str();
		changed = true;
	}

	if (changed)
	{
		memset(&_oldUsableClientRect, 0, sizeof(_oldUsableClientRect));
		UpdateFrameCompareLabelPositions();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::DisplayerRectanglesChangedCB(void *Sender)
{
	LeftFrameClipNamePanel->Visible = false;
	RightFrameClipNamePanel->Visible = false;
	FrameCompareLabelUpdateTimer->Enabled = false;
	FrameCompareLabelUpdateTimer->Interval = 100;
	FrameCompareLabelUpdateTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FrameCompareLabelUpdateTimerTimer(TObject *Sender) {UpdateFrameCompareLabelPositions();}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateFrameCompareLabelPositions()
{
	auto frameCompareMode = clipPlayer->getFrameCompareMode();

	RECT leftRect = clipDisplayer->getCompareFrameClientRectangle();
	RECT rightRect = clipDisplayer->getMainFrameClientRectangle();
	RECT usableRect = clipDisplayer->getVisiblePartOfClientRectangle();
	if (memcmp(&usableRect, &_oldUsableClientRect, sizeof(RECT)) || memcmp(&leftRect, &_oldLeftFrameRect,
		 sizeof(RECT)) || memcmp(&rightRect, &_oldRightFrameRect, sizeof(RECT)))
	{
		_oldUsableClientRect = usableRect;
		_oldLeftFrameRect = leftRect;
		_oldRightFrameRect = rightRect;

		if (frameCompareMode == CPlayer::FrameCompareMode::SideBySide)
		{
			LeftFrameClipNamePanel->Left = leftRect.left;
			LeftFrameClipNamePanel->Top = std::min<int>(usableRect.bottom - LeftFrameClipNamePanel->Height,
				 leftRect.bottom + 1);
		}
		else
		{
			// Bad naming - only one rect, still called the rightRect.
			LeftFrameClipNamePanel->Left = rightRect.left;
			LeftFrameClipNamePanel->Top = std::max<int>(usableRect.top, rightRect.top - LeftFrameClipNamePanel->Height);
		}

		RightFrameClipNamePanel->Left = rightRect.right + 1 - RightFrameClipNamePanel->Width;
		RightFrameClipNamePanel->Top = std::min<int>(usableRect.bottom - RightFrameClipNamePanel->Height,
			 rightRect.bottom + 1);
	}
	else
	{
		// No change.
		FrameCompareLabelUpdateTimer->Enabled = false;
	}

	LeftFrameClipNamePanel->Visible = _showFrameCompareClipNames && frameCompareMode != CPlayer::FrameCompareMode::Off;
	RightFrameClipNamePanel->Visible = _showFrameCompareClipNames && frameCompareMode != CPlayer::FrameCompareMode::Off;
}
// ---------------------------------------------------------------------------

void TmainWindow::SetColorChannelMask(int mask)
{
	clipDisplayer->setRGBChannelsMask(mask);
	UpdateRGBChannelsToolbarButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

int TmainWindow::GetColorChannelMask() {return clipDisplayer->getRGBChannelsMask();}
// ---------------------------------------------------------------------------

void TmainWindow::ShowAllColorChannels() {SetColorChannelMask(ALL_CHANNELS);}
// ---------------------------------------------------------------------------

void TmainWindow::ShowOrToggleColorChannel(int singleChannelMask)
{
	// Only single color channel may be specified!
	MTIassert(singleChannelMask == RED_CHANNEL || singleChannelMask == GRN_CHANNEL || singleChannelMask == BLU_CHANNEL);

	int wholeMask = clipDisplayer->getRGBChannelsMask();
	bool onlySpecifiedChannelIsSelected = (wholeMask & ALL_CHANNELS) == singleChannelMask;

	if (m_savedShiftState.Contains(ssCtrl) && !onlySpecifiedChannelIsSelected)
	{
		// Toggle blue if it's not the only thing currently selected
		wholeMask ^= singleChannelMask;

		// Turn on gray if there's only one channel selected
		bool onlyRedIsSelected = (wholeMask & ALL_CHANNELS) == RED_CHANNEL;
		bool onlyGreenIsSelected = (wholeMask & ALL_CHANNELS) == GRN_CHANNEL;
		bool onlyBlueIsSelected = (wholeMask & ALL_CHANNELS) == BLU_CHANNEL;

		if (onlyRedIsSelected || onlyGreenIsSelected || onlyBlueIsSelected)
		{
			wholeMask |= SHOW_GRAY;
		}
		else
		{
			wholeMask &= ~SHOW_GRAY;
		}
	}
	else
	{
		// Show just the specified channel as gray
		wholeMask = singleChannelMask | SHOW_GRAY;
	}

	SetColorChannelMask(wholeMask);
}

// ---------------------------------------------------------------------------
void TmainWindow::ToggleGrayDisplay()
{
	int mask = GetColorChannelMask();
	mask ^= SHOW_GRAY;
	SetColorChannelMask(mask);
}

// ---------------------------------------------------------------------------
void TmainWindow::ToggleAlphaDisplay()
{
	MTI_UINT32 alphacol = clipDisplayer->getAlphaColor();
	alphacol = (alphacol == SHOW_ALPHA_YELLOW) ? SHOW_ALPHA_TRANSPARENT : SHOW_ALPHA_YELLOW;
	clipDisplayer->setAlphaColor(alphacol);
	UpdateRGBChannelsToolbarButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::UpdateRGBChannelsToolbarButtons()
{
	GetTimeline()->UpdateRGBChannelsToolbarButtons(GetColorChannelMask(), clipDisplayer->isAlphaEnabled(),
		 clipDisplayer->getAlphaColor() != 0);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FullScreenMenuItemClick(TObject *Sender)
{
   if (_mainWindowState == MainWindowState::NormalWindow)
   {
      ToFullScreen();
   }
   else
   {
      ToNormalWindow();
   }
}
// ---------------------------------------------------------------------------

void TmainWindow::SetMouseOrientation(int type)
{
	mouseOrientation = 0; // Disabled
}

int TmainWindow::GetMouseOrientation()
{
	return 0; // Disabled
}
// ---------------------------------------------------------------------------

void TmainWindow::informToolWindowStatus(bool windowIsActive)
{
	if (windowIsActive)
	{
		toolWindowIsActive++;
	}
	else if ((--toolWindowIsActive) < 0)
	{
		toolWindowIsActive = 0;
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::EditReticles()
{
   CustomReticleDlg->Show();
}
// ---------------------------------------------------------------------------

int TmainWindow::GetReticleMode()
{
   return GetDisplayer()->getReticleMode();
}
// ---------------------------------------------------------------------------

void TmainWindow::SetReticleMode(int reticleMode)
{
	GetDisplayer()->setReticleMode(reticleMode);
	UpdateReticleToolbarModeButtons();
	if (!clipPlayer->isDisplaying())
	{
		clipPlayer->refreshFrame();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleReticleBits(int reticleBitMask)
{
	// get the current reticle mode
	int reticleMode = GetReticleMode();

	// toggle the appropriate bit in the reticle mode
	reticleMode ^= reticleBitMask;

	// set the modified reticle mode
	SetReticleMode(reticleMode);
}
// ---------------------------------------------------------------------------

void TmainWindow::ShowAllReticles()
{
	// Toggle between all reticles on and all reticles off.
	int oldReticleMode = GetReticleMode();
	int newReticleMode = (oldReticleMode & RETICLE_MODE_MATTED) |
		 (((oldReticleMode & RETICLE_MODE_ALL) == RETICLE_MODE_ALL) ? RETICLE_MODE_NONE : RETICLE_MODE_ALL);

	SetReticleMode(newReticleMode);
}
// ---------------------------------------------------------------------------

void TmainWindow::LoadReticlesByName(string reticlesName)
{
	CustomReticleDlg->LoadReticle(reticlesName);

	// show the new reticle's name in the toolbar combo box
	UpdateReticleToolbarComboBox();

	// this is the reticle saved on exit from the clip
	CReticle currentReticle = GetDisplayer()->getReticle();
	GetDisplayer()->setExitReticle(currentReticle);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormActivate(TObject *Sender) {FocusControl(DummyFocusTrackBar);}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::CreateNewVersionMenuItemClick(TObject *Sender)
{
	// This clip naming crap drives me crazy
	CBinManager binManager;
	string currentClipPath(GetCurrentClipHandler()->getCurrentClipPath());
	string masterClipPath(binManager.getMasterClipPath(currentClipPath));

	// Ugly!! Ugly!! Ugly!!  QQQ
	if (BinManagerForm == nullptr)
	{
		ShowBinManager(false); // invisible
	}

	BinManagerForm->CreateNewVersionClip(masterClipPath);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::CommitChangesAndDeleteMenuItemClick(TObject *Sender)
{
	string currentClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	// Delete Selected Version Clip

	// Allow user to change mind

	string clipName = GetCurrentClipHandler()->getCurrentClipName();
	string binPath = RemoveDirSeparator(GetCurrentClipHandler()->getCurrentParentPath());
	CBinManager binMgr;
	if (binMgr.isClipDir(binPath))
		clipName = GetFileNameWithExt(binPath) + " " + clipName;
	MTIostringstream ostr;
	ostr << "You have requested that all modifications made to this clip version:    " << endl << clipName << endl <<
		 "be copied back into the master clip and the version deleted." << endl << endl << "THERE IS NO UNDO. Proceed?";
	int retVal = _MTIConfirmationDialog(Handle, ostr.str());
	if (retVal == MTI_DLG_OK)
	{
		// It's a go!
		if (BinManagerForm == nullptr)
			ShowBinManager(false); // invisible
		BinManagerForm->CommitVersionClipChanges(currentClipPath, CTimecode::NOT_SET, CTimecode::NOT_SET);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::CommitChangesInMarkedRangeMenuItemClick(TObject *Sender)
{
	CPlayer *player = GetPlayer();
	string versionClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	// Allow user to change mind
	string clipName = GetCurrentClipHandler()->getCurrentClipName();
	string binPath = RemoveDirSeparator(GetCurrentClipHandler()->getCurrentParentPath());
	CBinManager binMgr;
	if (binMgr.isClipDir(binPath))
		clipName = GetFileNameWithExt(binPath) + " " + clipName;
	MTIostringstream ostr;
	ostr << "You have requested that the marked range of this clip version:    " << endl << clipName << endl <<
		 "be copied back into the master clip." << endl << endl << "THERE IS NO UNDO. Proceed?";
	int retVal = _MTIConfirmationDialog(Handle, ostr.str());
	if (retVal == MTI_DLG_OK)
	{
		// It's a go!
		if (BinManagerForm == nullptr)
			ShowBinManager(false); // invisible
		BinManagerForm->CommitVersionClipChanges(versionClipPath, player->getMarkInTimecode(),
			 player->getMarkOutTimecode());
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DiscardAllChangesMenuItemClick(TObject *Sender)
{
	string currentClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	// Allow user to change mind
	string clipName = GetCurrentClipHandler()->getCurrentClipName();
	string binPath = RemoveDirSeparator(GetCurrentClipHandler()->getCurrentParentPath());
	CBinManager binMgr;
	if (binMgr.isClipDir(binPath))
		clipName = GetFileNameWithExt(binPath) + " " + clipName;
	MTIostringstream ostr;
	ostr << "You have requested that all changes made to this clip version:    " << endl << clipName << endl <<
		 "be discarded. THERE IS NO UNDO. Proceed?";
	int retVal = _MTIConfirmationDialog(Handle, ostr.str());
	if (retVal == MTI_DLG_OK)
	{
		// It's a go!
		if (BinManagerForm == nullptr)
			ShowBinManager(false); // invisible
		BinManagerForm->DiscardVersionClipChanges(currentClipPath, CTimecode::NOT_SET, CTimecode::NOT_SET);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DiscardChangesInMarkedRangeMenuItemClick(TObject *Sender)
{
	CPlayer *player = GetPlayer();
	string versionClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	// Allow user to change mind
	string clipName = GetCurrentClipHandler()->getCurrentClipName();
	string binPath = RemoveDirSeparator(GetCurrentClipHandler()->getCurrentParentPath());
	CBinManager binMgr;
	if (binMgr.isClipDir(binPath))
		clipName = GetFileNameWithExt(binPath) + " " + clipName;

	MTIostringstream ostr;
	ostr << "You have requested the changes made in the marked range    " << endl << "of this clip version: " <<
		 clipName << endl << "be discarded. THERE IS NO UNDO. Proceed?";
	int retVal = _MTIConfirmationDialog(Handle, ostr.str());
	if (retVal == MTI_DLG_OK)
	{
		// See if the tool will allow it
		std::pair<int, int>range(GetPlayer()->getMarkIn(), GetPlayer()->getMarkOut());

		std::string message;
		auto status = CanWeDiscardOrCommit(true, range, message);

		if (status != TOOL_RETURN_CODE_OPERATION_REFUSED)
		{
			// It's a go!
			if (BinManagerForm == nullptr)
				ShowBinManager(false); // invisible
			BinManagerForm->DiscardVersionClipChanges(versionClipPath, player->getMarkInTimecode(),
				 player->getMarkOutTimecode());
		}
	}
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FileMenuClick(TObject *Sender)
{
	CBinManager binManager;
	string currentClipPath(GetCurrentClipHandler()->getCurrentClipPath());
	string masterClipPath(binManager.getMasterClipPath(currentClipPath));

	CreateNewVersionMenuItem->Enabled = !currentClipPath.empty();

	if (currentClipPath == masterClipPath)
	{
		// Not a version clip
		CommitChangesAndDeleteMenuItem->Enabled = false;
		CommitChangesInMarkedRangeMenuItem->Enabled = false;
		DiscardAllChangesMenuItem->Enabled = false;
		DiscardChangesInMarkedRangeMenuItem->Enabled = false;
		ExportVersionClipFilesMenuItem->Enabled = false;
	}
	else
	{
		// Is a version clip
		CommitChangesAndDeleteMenuItem->Enabled = true;
		DiscardAllChangesMenuItem->Enabled = true;
		ExportVersionClipFilesMenuItem->Enabled = true;

		CPlayer *player = GetPlayer();
		if (player->getMarkInTimecode() != CTimecode::NOT_SET && player->getMarkOutTimecode() != CTimecode::NOT_SET)
		{
			CommitChangesInMarkedRangeMenuItem->Enabled = true;
			DiscardChangesInMarkedRangeMenuItem->Enabled = true;
			ExportMarkedRangeMenuItem->Enabled = true;
		}
		else
		{
			CommitChangesInMarkedRangeMenuItem->Enabled = false;
			DiscardChangesInMarkedRangeMenuItem->Enabled = false;
			ExportMarkedRangeMenuItem->Enabled = false;
		}
	}

}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::MaskMenuClick(TObject *Sender)
{

	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();

   EnableMaskMenuItem->Enabled = maskTool->IsMaskToolActivationAllowed();
   EnableMaskMenuItem->Checked = maskTool->IsMaskToolActive() && !maskTool->IsMaskRoiModeActive();
	SaveMaskMenuItem->Enabled = maskTool->IsMaskAvailable();
   LoadMaskMenuItem->Enabled = maskTool->IsMaskToolActivationAllowed() && !maskTool->IsMaskRoiModeActive();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::EnableMaskMenuItemClick(TObject *Sender)
{
	// Enable/Disable the Mask Tool

	int toolCommand = (EnableMaskMenuItem->Checked) ? NAV_CMD_MASK_TOOL_DISABLE : NAV_CMD_MASK_TOOL_ENABLE;

	this->ExecuteNavigatorToolCommand(toolCommand);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::SaveMaskMenuItemClick(TObject *Sender)
{
   BaseToolWindow->MaskPropertiesFrame->saveMask();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::LoadMaskMenuItemClick(TObject *Sender)
{
   BaseToolWindow->MaskPropertiesFrame->loadMask();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta, TPoint &MousePos,
	 bool &Handled)
{
	Handled = true;
	m_savedShiftState = Shift;

	// TRACE_0(errout << "MOUSE WHEEL: " << WheelDelta);

	if (clipDisplayer->isMouseZoomModeActive())
	{
		this->ExecuteNavigatorToolCommand((WheelDelta > 0) ? NAV_CMD_ZOOM_IN : NAV_CMD_ZOOM_OUT);
		return;
	}

	// A HACK - turn it into up, down and left, right (ALT)  keys
	WORD simKey = 0;
	if (Shift.Contains(ssAlt))
	{
		if (WheelDelta > 0)
			simKey = MTK_RIGHT;
		else if (WheelDelta < 0)
			simKey = MTK_LEFT;

		Shift >> ssAlt; // Remove the ALT!

		// HACK!!! ADD ssCommand (mac-only command key) for tools that really
		// want to know if the ALT key is down!
		Shift << ssCommand;
	}
	else
	{
		if (WheelDelta > 0)
			simKey = MTK_UP;
		else if (WheelDelta < 0)
			simKey = MTK_DOWN;
	}

	if (simKey != 0)
	{
		FormKeyDown(Sender, simKey, Shift);
		FormKeyUp(Sender, simKey, Shift);
	}
}
// ---------------------------------------------------------------------------

// copy a string to the Windows Clipboard;
// trim leading and trailing white space
void TmainWindow::SendToClipboard(const string &str)
{
	string trimStr(StringTrim(str));
	Clipboard()->AsText = trimStr.c_str();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::BinManagerMenuItemClick(TObject *Sender)
{
	// Open or close the Bin Manager GUI. 2nd arg signifies fsAlwaysOnTop
	bool BinManagerIsVisible = (BinManagerForm != nullptr) && BinManagerForm->Visible;
	ShowBinManager(!BinManagerIsVisible);
	if ((BinManagerForm != nullptr) && BinManagerForm->Visible)
		BinManagerForm->SetFocus();
}
// ---------------------------------------------------------------------------

void TmainWindow::SetMenuShortcutsAreOkNow(bool yesNoFlag)
{
	// // I want the menus to only obey the shortcuts if the ALT key is
	// // being pressed. I was trying to do that by handling the "ShortCut"
	// // event from the toolbar, but for unknown reasons that isn't being
	// // called. So instead I add or remove the shortcut indicators to the
	// // menu captions, which isn't all bad.
	//
	// if (yesNoFlag)
	// {
	// FileMenu->Caption    = "&File";
	// HelpMenu->Caption    = "&Help";
	// MaskMenu->Caption    = "&Mask";
	// ViewMenu->Caption    = "&View";
	// WindowsMenu->Caption = "&Windows";
	// }
	// else
	// {
	// FileMenu->Caption    = "File";
	// HelpMenu->Caption    = "Help";
	// MaskMenu->Caption    = "Mask";
	// ViewMenu->Caption    = "View";
	// WindowsMenu->Caption = "Windows";
	// }
}

void TmainWindow::UpdateFrameAspectRatio(int option)
{
	AspectSubmenuItem->Enabled = false;
	AspectDefItem->Checked = false;
	Aspect133Item->Checked = false;
	Aspect166Item->Checked = false;
	Aspect178Item->Checked = false;
	Aspect185Item->Checked = false;
	Aspect220Item->Checked = false;
	Aspect235Item->Checked = false;
	Aspect239Item->Checked = false;
	Aspect255Item->Checked = false;
	Aspect266Item->Checked = false;

	switch (option)
	{

	case ASPECT_DEF:

		AspectSubmenuItem->Enabled = true;
		AspectDefItem->Checked = true;

		break;

	case ASPECT_133:

		AspectSubmenuItem->Enabled = true;
		Aspect133Item->Checked = true;

		break;

	case ASPECT_166:

		AspectSubmenuItem->Enabled = true;
		Aspect166Item->Checked = true;

		break;

	case ASPECT_178:

		AspectSubmenuItem->Enabled = true;
		Aspect178Item->Checked = true;

		break;

	case ASPECT_185:

		AspectSubmenuItem->Enabled = true;
		Aspect185Item->Checked = true;

		break;

	case ASPECT_220:

		AspectSubmenuItem->Enabled = true;
		Aspect220Item->Checked = true;

		break;

	case ASPECT_235:

		AspectSubmenuItem->Enabled = true;
		Aspect235Item->Checked = true;

		break;

	case ASPECT_239:

		AspectSubmenuItem->Enabled = true;
		Aspect239Item->Checked = true;

		break;

	case ASPECT_255:

		AspectSubmenuItem->Enabled = true;
		Aspect255Item->Checked = true;

		break;

	case ASPECT_266:

		AspectSubmenuItem->Enabled = true;
		Aspect266Item->Checked = true;

		break;

	default:

		break;
	}
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::AspectMenuItemClick(TObject *Sender)
{
	TMenuItem *button = dynamic_cast<TMenuItem*>(Sender);
	if (button != nullptr)
	{

		clipDisplayer->setFrameAspectRatio(button->Tag);

		clipDisplayer->setDisplayMode(DISPLAY_MODE_TRUE_VIEW);

		clipPlayer->refreshFrame();

		UpdateViewingModeStatus();
	}
}

// ---------------------------------------------------------------------------
void TmainWindow::CycleToNextViewMode()
{
	int dspmode = clipDisplayer->getDisplayMode();
	int newmode;

	if (dspmode == DISPLAY_MODE_TRUE_VIEW)
	{
		newmode = DISPLAY_MODE_1_TO_1;
	}
	else if (dspmode == DISPLAY_MODE_1_TO_1)
	{
		newmode = DISPLAY_MODE_TRUE_VIEW;
	}
	else if (dspmode == DISPLAY_MODE_FIT_WIDTH)
	{
		newmode = DISPLAY_MODE_1_TO_1;
	}

	// set the display mode
	GetDisplayer()->setDisplayMode(newmode);

	// zoom all only if picture mode or
	// pixel aspect ratio changes
	// see Displayer
	// GetDisplayer()->zoomAll();
	// UpdateZoomLevelStatus();

	// refresh the screen in the new mode
	GetPlayer()->refreshFrame();

	UpdateViewingModeStatus();
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleLoupe()
{
	if (LoupeWindow->Visible)
	{
		LoupeWindow->deactivate();
	}
	else
	{
		// Force update
		oldLoupePos.x = -1;
		oldLoupePos.y = -1;

		MoveLoupe(savedMouseX, savedMouseY); // makes it visible
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::ShowLoupe()
{
	if (!LoupeWindow->Visible)
	{
		ToggleLoupe();
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::HideLoupe()
{
	if (LoupeWindow->Visible)
	{
		ToggleLoupe();
	}
}
// ---------------------------------------------------------------------------

bool TmainWindow::IsLoupeVisible() {return LoupeWindow->Visible;}
// ---------------------------------------------------------------------------

int TmainWindow::GetLoupeMagnification() {return loupeMagnification;}
// ---------------------------------------------------------------------------

void TmainWindow::SetLoupeMagnification(int newMag)
{
	// QQQ MANIFEST CONSTANTS!!!
	if (newMag < 4)
		newMag = 4;
	if (newMag > 8)
		newMag = 8;
	if (newMag != loupeMagnification)
	{
		loupeMagnification = newMag;

		// Force update - will reset the paramters
		oldLoupePos.x = -1;
		oldLoupePos.y = -1;
		MoveLoupe(savedMouseX, savedMouseY);
	}

	aLoupeSettingChanged = true;
}
// ---------------------------------------------------------------------------

void TmainWindow::ToggleLoupeCrosshairs()
{
	LoupeWindow->toggleCrosshairs();

	// Force update
	oldLoupePos.x = -1;
	oldLoupePos.y = -1;
	MoveLoupe(savedMouseX, savedMouseY);

	aLoupeSettingChanged = true;
}
// ---------------------------------------------------------------------------

// SIDE EFFECT ALERT: makes the loupe visible
void TmainWindow::MoveLoupe(int clientX, int clientY)
{
	TPoint clientPos(clientX, clientY);
	TPoint screenPos(ClientToScreen(clientPos));
	TPoint imagePos;
	imagePos.x = clipDisplayer->scaleXClientToFrame(clientX);
	imagePos.y = clipDisplayer->scaleYClientToFrame(clientY);

	if (imagePos.x != oldLoupePos.x || imagePos.y != oldLoupePos.y)
	{
		oldLoupePos.x = imagePos.x;
		oldLoupePos.y = imagePos.y;

		/////////////////////////////////////////////////////
		// ORDER OF STUFF HERE IS IMPORTANT!               //
		// 1. position the loupe window                    //
		// 2. activate the loupe window (makes it visible) //
		// 3. position and refresh the loupe's magnifier   //
		/////////////////////////////////////////////////////

		RECT activePictureRect = clipDisplayer->getActiveRectangle();
		TPoint screenPosOfFrameTopLeft;
		TPoint screenPosOfFrameBottomRight;
		TPoint temp;
		temp.x = clipDisplayer->scaleXFrameToClient(activePictureRect.left);
		temp.y = clipDisplayer->scaleYFrameToClient(activePictureRect.top);
		screenPosOfFrameTopLeft = ClientToScreen(temp);
		temp.x = clipDisplayer->scaleXFrameToClient(activePictureRect.right);
		temp.y = clipDisplayer->scaleYFrameToClient(activePictureRect.bottom);
		screenPosOfFrameBottomRight = ClientToScreen(temp);
		int smallestLeft = screenPosOfFrameTopLeft.x;
		int smallestTop = screenPosOfFrameTopLeft.y;
		int biggestLeft = screenPosOfFrameBottomRight.x - LoupeWindow->Width;
		int biggestTop = screenPosOfFrameBottomRight.y - LoupeWindow->Height;

		// Normally we show the loupe window below and to the right of the
		// cursor; to avoid ever having what's under the cursor hidden by the
		// loupe window, we flip it up above the cursor when we're near the
		// bottom of the frame, and we flip it to the left of the cursot if
		// we're near the right edge

		int newLoupeLeft = screenPos.x + 24; // QQQ magic number
		int newLoupeTop = screenPos.y + 24; // QQQ magic number

		if (newLoupeLeft > biggestLeft)
		{
			// Flip it!
			newLoupeLeft = screenPos.x - 24 - LoupeWindow->Width; // QQQ magic number
		}

		if (newLoupeTop > biggestTop)
		{
			// Flip it!
			newLoupeTop = screenPos.y - 24 - LoupeWindow->Height; // QQQ magic number
		}

		// For sanity, keep the loupe entirely within the displayed frame
		if (newLoupeLeft < smallestLeft)
			newLoupeLeft = smallestLeft;
		if (newLoupeLeft > biggestLeft)
			newLoupeLeft = biggestLeft;

		if (newLoupeTop < smallestTop)
			newLoupeTop = smallestTop;
		if (newLoupeTop > biggestTop)
			newLoupeTop = biggestTop;

		LoupeWindow->Left = newLoupeLeft;
		LoupeWindow->Top = newLoupeTop;

		LoupeWindow->activate(30, loupeMagnification);

		CMagnifier *magnifier = LoupeWindow->getMagnifier();
		if (magnifier != nullptr)
		{
			//////////////////////////////////////////////////////////////////////
			// HACK ALERT! Normally you would have to refresh the frame so
			// the magnifier being used for the loupe would be refreshed, but
			// I HATE the idea of redrawing the frame continuously as the mouse
			// is moved so we take a big HACKY shortcut here by passing the
			// pointer to the last frame displayed to the loupe so it can
			// refresh the magnifier directly!!!!
			MediaFrameBufferSharedPtr frameBuffer = GetDisplayer()->getLastDisplayedFrameBuffer();
			//////////////////////////////////////////////////////////////////////

			magnifier->recenterMagnifierWindow(imagePos.x, imagePos.y);

			if (frameBuffer)
			{
				MTI_UINT8 *stupidBuf[2] =
				{frameBuffer->getImageDataPtr(), nullptr};
				magnifier->refreshMagnifier(stupidBuf);
			}
		}
	}
}
// ---------------------------------------------------------------------------

void TmainWindow::SetLoupeSettingMode(bool flag)
{
	inLoupeSettingMode = flag;
	aLoupeSettingChanged = false;
}
// ---------------------------------------------------------------------------

void TmainWindow::HandleLoupeSettingKey(int Key)
{
	switch (Key)
	{
	case MTK_4:
		SetLoupeMagnification(4);
		break;

	case MTK_5:
		SetLoupeMagnification(5);
		break;

	case MTK_6:
		SetLoupeMagnification(6);
		break;

	case MTK_7:
		SetLoupeMagnification(7);
		break;

	case MTK_8:
		SetLoupeMagnification(8);
		break;

	case MTK_UP:
		SetLoupeMagnification(GetLoupeMagnification() + 1);
		break;

	case MTK_DOWN:
		SetLoupeMagnification(GetLoupeMagnification() - 1);
		break;

	case MTK_0:
		ToggleLoupeCrosshairs();
		break;
	}
}
// ---------------------------------------------------------------------------

bool TmainWindow::didALoupeSettingChange() {return aLoupeSettingChanged;}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormDeactivate(TObject *Sender)
{
	// Kill the loupe window - I don't feel like dealing with it
	// not allowed to do this here
	///LoupeWindow->deactivate();
}
// ---------------------------------------------------------------------------

void TmainWindow::FocusOnMainWindow()
{
	// NOTE: I tried to get rid of this stupid dummy control and just give
	// focus to the DosplayPanel component, but for completely unknown
	// reasons, that causes arrow keys to be lost in some Paint modes!!!!!
	FocusControl(DummyFocusTrackBar);
	SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelMouseEnter(TObject *Sender) {this->mouseIsInDisplayPanel = true;}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelMouseLeave(TObject *Sender) {this->mouseIsInDisplayPanel = false;}
// ---------------------------------------------------------------------------

double oldResizeThumbPosition;
double oldThumbPosition;

CTimecode oldTimecode;

void TmainWindow::SaveTimelinePositions()
{
	oldResizeThumbPosition = GetTimeline()->MTL->ResizeTrackBar->Position();

	// Avoid a round off error
	oldThumbPosition = GetTimeline()->MTL->TrackBar->ThumbPosition;

	// Need to preserve TIMECODE, not thumb position!
	oldTimecode = GetPlayer()->getCurrentTimecode();
}

void TmainWindow::RestoreTimelinePositions()
{
	GetTimeline()->MTL->ResizeTrackBar->Position(oldResizeThumbPosition);
	GetTimeline()->MTL->ResizeTrackBarChange(nullptr);

	// Restore old TIMECODE, not thumb position!
	CTimecode boundedTC = GetCurrentClipHandler()->limitTimecode(oldTimecode, false);
	if (boundedTC != oldTimecode)
	{
		// Old timecode was out of range for the new clip, so go to the mark in.
		boundedTC = GetPlayer()->getMarkInTimecode();
	}

	if (boundedTC != CTimecode::NOT_SET)
	{
		GetPlayer()->goToFrameSynchronous(boundedTC);
	}

	GetTimeline()->MTL->TrackBar->PositionOfThumb(oldThumbPosition);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::RefreshDisplayWindow(TObject *Sender) {clipPlayer->refreshFrame();}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::FormClose(TObject *Sender, TCloseAction &Action)
{
	// Kludge for crash on exit, just shut down it all
	_exit(0);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::AppOnActivate(TObject *Sender) {}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::AppOnDeactivate(TObject *Sender) {}
// ---------------------------------------------------------------------------

TRect TmainWindow::GetBoundsRectOfLargestMonitor()
{
	// Gets the BoundsRect needed to match the bounds of the largest
	// monitor attached to the system.

	TRect monitorBounds(0, 0, 0, 0);

	for (int i = 0; i < Screen->MonitorCount; ++i)
	{
		Vcl::Forms::TMonitor *monitor = Screen->Monitors[i];
		if (monitor->Height * monitor->Width > monitorBounds.Height() * monitorBounds.Width())
		{
			monitorBounds = monitor->BoundsRect;
		}
	}

	return monitorBounds;
}
// ---------------------------------------------------------------------------

void TmainWindow::GoToFrame(PTimecode tcP, const string &preconformedTcString)
{
	CNavCurrentClip *clipHandler = GetCurrentClipHandler();

	if (preconformedTcString.size() > 1 && (preconformedTcString[0] == '+' || preconformedTcString[0] == '-'))
	{
		GetTimeline()->SetJumpString(preconformedTcString.substr(1));
	}

	CTimecode boundedTC = clipHandler->limitTimecode(tcP, false /* TTT true */);
	GetTimeline()->timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_CUE);
	GetTimeline()->timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
}

void TmainWindow::ToggleShowTimecodeOverlay()
{
	if (_mainWindowState != MainWindowState::Presentation)
	{
		return;
	}

	if (FloatingTimelineToolbarRibbonParentPanel->Visible)
	{
		ToggleShowTimelineRibbonOverlay();
	}

	TPanel *timecodeToolbarPanel = GetTimeline()->CenterToolbarCenteringPanel;

	timecodeToolbarPanel->Visible = true;
	timecodeToolbarPanel->Parent = FloatingTimecodeToolbarParentPanel;
   TimelineFrame->ResizeTimecodeToolbar();
	FloatingTimecodeToolbarParentPanel->Top = ClientHeight - timecodeToolbarPanel->Height - 2;
	FloatingTimecodeToolbarParentPanel->Visible = !FloatingTimecodeToolbarParentPanel->Visible;
	timecodeToolbarPanel->Left = 0;
	restoreRibbonAfterPlaying = false;
	RibbonTimer->Enabled = false;
}

// ---------------------------------------------------------------------------

void TmainWindow::ToggleShowTimelineRibbonOverlay()
{
	if (_mainWindowState != MainWindowState::Presentation)
	{
		return;
	}

	// Ugly as sin, see if we are displaying the toolbar for mainwindow
	// Reparent the toggle
	TPanel *fullToolbarPanel = GetTimeline()->ToolbarPanel;
	TPanel *timecodeToolbarPanel = GetTimeline()->CenterToolbarCenteringPanel;

	fullToolbarPanel->Parent = FloatingTimelineToolbarRibbonParentPanel;
	timecodeToolbarPanel->Parent = fullToolbarPanel;
   TimelineFrame->ResizeTimecodeToolbar();
	timecodeToolbarPanel->Left = (ClientWidth - timecodeToolbarPanel->Width) / 2;
	timecodeToolbarPanel->Visible = true;
	FloatingTimecodeToolbarParentPanel->Visible = false;

	// FloatingTimelineToolbarRibbonParentPanel is alBottom, so no need to set Top or Width
	FloatingTimelineToolbarRibbonParentPanel->Visible = !FloatingTimelineToolbarRibbonParentPanel->Visible;
	restoreRibbonAfterPlaying = false;
	RibbonTimer->Enabled = FloatingTimelineToolbarRibbonParentPanel->Visible && !GetPlayer()->isReallyPlaying();
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::RibbonTimerTimer(TObject *Sender)
{
	if (FloatingTimelineToolbarRibbonParentPanel->Visible && GetPlayer()->isReallyPlaying())
	{
		restoreRibbonAfterPlaying = true;
		FloatingTimelineToolbarRibbonParentPanel->Visible = false;
	}
	else if (restoreRibbonAfterPlaying && !GetPlayer()->isReallyPlaying())
	{
		restoreRibbonAfterPlaying = false;
		FloatingTimelineToolbarRibbonParentPanel->Visible = true;
	}
	else if (!restoreRibbonAfterPlaying && !FloatingTimelineToolbarRibbonParentPanel->Visible)
	{
		RibbonTimer->Enabled = false;
	}
}

// ---------------------------------------------------------------------------
void TmainWindow::UpdateProductAndVersion(void)
{
	// This needs to be removed
#ifndef NO_QLM_LICENSING
	// char productName[MAX_PRODUCT_NAME_LENGTH];
	// memset(productName, 0, MAX_PRODUCT_NAME_LENGTH);
	//
	// // Get the product
	// OBFUS_7(productName, 0x69, 'D', 'R', 'S', 'N', 'o', 'v', 'a');
	// m_product = productName;
	// m_majorVersion = 4;
	// m_minorVersion = 0;
	// m_revision = 1;
#endif
}

void TmainWindow::ActivateLicenseNoWait()
{
	throw std::runtime_error("Removed, needs to be done differently");
	// #ifndef NO_QLM_LICENSING
	// UpdateProductAndVersion();
	//
	// STARTUPINFO StartupInfo;
	// PROCESS_INFORMATION ProcInfo;
	// memset(&StartupInfo, 0, sizeof(StartupInfo));
	// memset(&ProcInfo, 0, sizeof(ProcInfo));
	//
	// StartupInfo.cb = sizeof(StartupInfo); // Set structure size
	//
	// // Find the MTIActivation.exe executable - we check the current directory and also
	// // the Release directory to hopefully handle the case where it's run directly and
	// // also for when run via an installer.
	// string command = "MTIActivation.exe";
	// if (!FileExists(command.c_str()))
	// {
	// command = "..\\Release\\MTIActivation.exe";
	// }
	//
	// string commandAndArgs = command + " " + MtiLicenseLib::GetProductCombinedFullName(m_product, m_majorVersion,
	// m_minorVersion, m_revision);
	// LPSTR commandAndArgsLpStr = const_cast<char *>(commandAndArgs.c_str());
	// bool res = CreateProcess(nullptr, commandAndArgsLpStr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, &StartupInfo, &ProcInfo);
	// // starts MyApp
	// if (!res)
	// {
	// ShowMessage("Failed to start MTIActivation.exe via CreateProcess()");
	// }
	// #endif
	// return;
}

void TmainWindow::ActivateLicense()
{
    auto status = GetMtiLicense()->LaunchLicensingWizard();
    delete _mtiLicense;

	_mtiLicense = new MtiLicenseLib();
	if (GetMtiLicense()->IsActivated == false)
	{
      // This was put in here to avoid a infinite loop
      GetMtiLicense()->getLicense()->DeleteKeys();

		ReadV6MtiLicenseFile v6LicenseFile("DRSNOVA", 4, 0);
      if (v6LicenseFile.removeOldLicense() == false)
      {
         auto err = GetLastError();
         MTIostringstream os;
            os << "V11 license deactivated" << std::endl;
            os << "V6 license file deactivation failed" << std::endl;
            os << "Error: " << err << ", " << GetSystemErrorMessage(err);
            _MTIErrorDialog(os.str());
      }

		ShowMessage("License deactivated, DRS Nova will now close");
      exit(1112);
	}
   else
   {
   	  ////	ShowMessage("License was NOT deactivated");
   }

	return;

#ifdef EXAMPLE_OF_ACTIVATION_CODE
	// Example Code for doing activation from C++ - we hopefully will never need it since
	// we handle that from C#, but it's here for future reference.
	// If it really needs to be used, then the MtiLicenseLib class should be expanded to
	// handle qlmLicense->ActivationLicenseEx() instead of using the TLicenseValidator
	// class - which was example code from QLM.
	WideString webServiceUrl = "https://quicklicensemanager.com/mtifilm/qlm/qlmservice.asmx";
	WideString communicationEncryptionKey = "{946B7514-9B10-4EAE-8E9D-6707B2669084}";
	WideString response;

	TLicenseValidator *lv = new TLicenseValidator();
	IQlmLicense *lic = lv->Qlmlicense();
	lv->Qlmlicense()->CommunicationEncryptionKey = communicationEncryptionKey;
	HRESULT result = lv->Qlmlicense()->ActivateLicenseEx(webServiceUrl, "AYJZ1-G0A04-VW14X-78F31-3H1U7-8XIT4-FJY877GP",
		 "MTI-PC", "MTI-PC", "5.0.0", "Custom", "", response);

	ShowMessage("License: " + response);
#endif
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DummyFocusTrackBarEnter(TObject *Sender) {
	TRACE_3(errout << "FFFFFFFFFFFFFF dummy track bar ENTER");}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DummyFocusTrackBarExit(TObject *Sender) {
	TRACE_3(errout << "FFFFFFFFFFFFFF dummy track bar EXIT");}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelEnter(TObject *Sender) {
	TRACE_3(errout << "FFFFFFFFFFFFFF display panel ENTER");}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelExit(TObject *Sender) {TRACE_3(errout << "FFFFFFFFFFFFFF display panel EXIT");}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::WMMenuChar(TWMMenuChar &Message)
{
	MainMenu->ProcessMenuChar(Message);
	if (Message.Result == MAKELRESULT(0, MNC_IGNORE))
	{
		// Returning MNC_CLOSE suppresses the Beep. Apparently telling the
		// system to close the main menu actually has no other effect.
		// QQQ Should check for chars we care about and return MNC_IGNORE for
		// those we don't!
		Message.Result = MAKELRESULT(0, MNC_CLOSE);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::LicenseInformation1Click(TObject *Sender)
{
#ifndef NO_QLM_LICENSING
	UpdateProductAndVersion();

	// Check if the license is valid
	MtiLicenseLib *mtiLicense = new MtiLicenseLib();
	bool isValid = mtiLicense->LicenseValidate(m_product, m_majorVersion, m_minorVersion, m_revision);

	// Set the labels that are the same whether license is valid or not
	LicensingForm->ProductLabel->Caption = mtiLicense->GetProductName().c_str();
	LicensingForm->VersionLabel->Caption = mtiLicense->GetFullVersion(m_majorVersion, m_minorVersion, m_revision)
		 .c_str();
	LicensingForm->ComputerIdLabel->Caption = mtiLicense->GetCurrentMachineIdDatum().MachineId.c_str();
	LicensingForm->ComputerNameLabel->Caption = mtiLicense->GetMachineName().c_str();
	LicensingForm->FeaturesListBox->Clear();

	// Set the labels with info (if license is valid) or blank (if invalid).
	if (isValid)
	{
		LicensingForm->ValidLabel->Caption = "YES";
		LicensingForm->ActivationKeyLabel->Caption = mtiLicense->ActivationKey.c_str();
		LicensingForm->LicenseKeyLabel->Caption = mtiLicense->LicenseKey.c_str();
		LicensingForm->ExpiresLabel->Caption = mtiLicense->ExpirationDateStringPretty().c_str();
		LicensingForm->CompanyLabel->Caption = mtiLicense->Company.c_str();
		LicensingForm->UserLabel->Caption = mtiLicense->User.c_str();

		for (std::vector<string>::iterator it = mtiLicense->Features.begin(); it != mtiLicense->Features.end(); ++it)
		{
			LicensingForm->FeaturesListBox->Items->Add((*it).c_str());
		}
	}
	else
	{
		LicensingForm->ValidLabel->Caption = "NO";
		LicensingForm->ActivationKeyLabel->Caption = "";
		LicensingForm->LicenseKeyLabel->Caption = "";
		LicensingForm->ExpiresLabel->Caption = "";
		LicensingForm->CompanyLabel->Caption = "";
		LicensingForm->UserLabel->Caption = "";
	}

	LicensingForm->ShowModal();
#endif
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ActivateLicense1Click(TObject *Sender)
{
	// Actually deactivate
	MTIostringstream os;
	os << "In order to deactive a license you must have admin privileges" << std::endl;
	os << "and either be connected to the internet or have a smart phone " << std::endl;
   os << "with a QR reader" << std::endl;
   os << std::endl;
	os << "Do you wish to continue?";

	if (_MTIConfirmationDialog(os.str()) == MTI_DLG_OK)
	{
		this->ActivateLicense();
	}
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TimelineFrameResize(TObject *Sender)
{
	int TFLeft = TimelineFrame->Left;
	int TFTop = TimelineFrame->Top;
	int TFHeight = TimelineFrame->Height;
	int TFWidth = TimelineFrame->Width;

	int MTLLeft = TimelineFrame->MTL->Left;
	int MTLTop = TimelineFrame->MTL->Top;
	int MTLHeight = TimelineFrame->MTL->Height;
	int MTLWidth = TimelineFrame->MTL->Width;

	int EPLeft = TimelineFrame->EntirePanel->Left;
	int EPTop = TimelineFrame->EntirePanel->Top;
	int EPHeight = TimelineFrame->EntirePanel->Height;
	int EPWidth = TimelineFrame->EntirePanel->Width;

	int BPLeft = mainWindow->MainWindowBottomPanel->Left;
	int BPTop = mainWindow->MainWindowBottomPanel->Top;
	int BPHeight = mainWindow->MainWindowBottomPanel->Height;
	int BPWidth = mainWindow->MainWindowBottomPanel->Width;

	int DPLeft = mainWindow->DisplayPanel->Left;
	int DPTop = mainWindow->DisplayPanel->Top;
	int DPHeight = mainWindow->DisplayPanel->Height;
	int DPWidth = mainWindow->DisplayPanel->Width;

	TimelineFrame->FrameResize(Sender);
	MainWindowBottomPanel->Height = TimelineFrame->Height;

	// The center panel that contains a buch of timecodes has variable-sized
   // font based on the width of the panel.
	auto centerPanel = TimelineFrame->CenterToolbarCenteringPanel;
	if (centerPanel->Visible && centerPanel->Parent != FloatingTimecodeToolbarParentPanel)
	{
		auto leftPanel = TimelineFrame->LeftToolbarCenteringPanel;
		leftPanel->Left = 0;

		auto timecodePanel = TimelineFrame->CurrentTCPanel;
		centerPanel->Left = ((ClientWidth - timecodePanel->Width) / 2) - timecodePanel->Left;

		auto rightPanel = TimelineFrame->RightToolbarCenteringPanel;
		rightPanel->Left = ClientWidth -  rightPanel->Width;

		TimelineFrame->ResizeTimecodeToolbar();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::MainWindowBottomPanelResize(TObject *Sender) {
	TimelineFrame->Width = MainWindowBottomPanel->Width;}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::MainWindowTopPanelResize(TObject *Sender)
{
	// Callback to the active tool.
	CToolManager toolManager;
	toolManager.onTopPanelRedraw();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::MainWindowTopPanelConstrainedResize(TObject *Sender, int &MinWidth, int &MinHeight,
	 int &MaxWidth, int &MaxHeight)
{
	// placeholder
}
// ---------------------------------------------------------------------------

void TmainWindow::SetTopPanelVisibility(bool onOff)
{
	if (onOff)
	{
		TopPanelSplitter->Visible = true;
		MainWindowTopPanel->Visible = true;
		CToolManager toolMgr;
		toolMgr.onTopPanelRedraw();
	}
	else
	{
		MainWindowTopPanel->Visible = false;
		TopPanelSplitter->Visible = false;
	}
}
// ---------------------------------------------------------------------------

bool TmainWindow::GetTopPanelVisibility()
{
   return MainWindowTopPanel->Visible;
}
// ---------------------------------------------------------------------------

CMTIBitmap &TmainWindow::GetBitmapRefForTopPanelDrawing()
{
	if (TopPanelOffScreenBitmap.getWidth() != MainWindowTopPanel->Width || TopPanelOffScreenBitmap.getHeight()
		 != MainWindowTopPanel->Height)
	{
		TopPanelOffScreenBitmap.initBitmap(Handle, MainWindowTopPanel->Width, MainWindowTopPanel->Height);
	}

	return TopPanelOffScreenBitmap;
}
// ---------------------------------------------------------------------------

void TmainWindow::DrawBitmapOnTopPanel()
{
	HDC canvasDC = TopPanelPaintBox->Canvas->Handle;
	TopPanelOffScreenBitmap.Draw(canvasDC);
}
// ---------------------------------------------------------------------------

void TmainWindow::ShowFrameBufferErrorMessage(const MediaFrameBuffer::ErrorInfo &errorInfo)
{
	MTIostringstream os;
	os << errorInfo.shortMessage << " (" << errorInfo.code << ")";

	ErrorMessageLine1Label->Caption = os.str().c_str();
	ErrorMessageLine2Label->Caption = errorInfo.filename.c_str();
	ErrorMessageLine3Label->Caption = errorInfo.longMessage.c_str();

   // QQQ MANIFEST CONSTANTS!
   RelinkMediaFolderButton->Visible = errorInfo.code == 100002 || errorInfo.code == 100003;

	RepositionFrameBufferErrorMessage();
	FrameBufferErrorCoverUpPanel->Visible = true;
}
// ---------------------------------------------------------------------------

void TmainWindow::RepositionFrameBufferErrorMessage()
{
	FrameBufferErrorDisplayPanel->Left = (FrameBufferErrorCoverUpPanel->Width - FrameBufferErrorDisplayPanel->Width) / 2;
	FrameBufferErrorDisplayPanel->Top =
		 (FrameBufferErrorCoverUpPanel->Height - FrameBufferErrorDisplayPanel->Height) / 2;
}
// ---------------------------------------------------------------------------

void TmainWindow::HideFrameBufferErrorMessage()
{
   FrameBufferErrorCoverUpPanel->Visible = false;
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TopPanelPaintBoxMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
	 int X, int Y)
{
	// Go into "drag" mode.
	FocusControl(MainWindowTopPanel);
	inTopPanelDragMode = true;

	// Callback to the active tool.
	CToolManager toolManager;
	toolManager.onTopPanelMouseDown(X, Y);
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TopPanelPaintBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	if (!inTopPanelDragMode)
	{
		return;
	}

	if (!Shift.Contains(ssLeft))
	{
		// Missed the up event??
		TopPanelPaintBoxMouseUp(Sender, mbLeft, Shift, X, Y);
		return;
	}

	// Callback to the active tool.
	CToolManager toolManager;
	toolManager.onTopPanelMouseDrag(X, Y);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TopPanelPaintBoxMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	 int X, int Y)
{
	if (!inTopPanelDragMode)
	{
		return;
	}

	// Callback to the active tool.
	CToolManager toolManager;
	toolManager.onTopPanelMouseUp(X, Y);

	// Cancel "drag" mode.
	FocusOnMainWindow();
	inTopPanelDragMode = false;
}

// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TopPanelSplitterPaint(TObject *Sender)
{
	auto canvas = TopPanelSplitter->Canvas;
	canvas->Brush->Color = TopPanelSplitter->Color;
	canvas->FillRect(TopPanelSplitter->ClientRect);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::TopPanelPaintBoxPaint(TObject *Sender)
{
	CToolManager toolMgr;
	toolMgr.onTopPanelRedraw();
}
// ---------------------------------------------------------------------------

void TmainWindow::OnFrameWasDiscardedOrCommitted(void *Sender)
{
	CBinManager *binManager = (CBinManager*)Sender;
	CToolManager toolMgr;
	toolMgr.onFrameWasDiscardedOrCommitted(binManager->IndexOfFrameThatWasDiscardedOrCommitted,
		 binManager->FrameWasDiscarded, binManager->DiscardedFrameRange);
}

// ---------------------------------------------------------------------------
int TmainWindow::CanWeDiscardOrCommit(bool discardFlag, const std::pair<int, int> &range, string &message)
{
	CToolManager toolMgr;
   auto frameRange = range;
   if (frameRange.first == -1)
   {
      frameRange = {0, INT_MAX};
   }

	return toolMgr.onFrameDiscardingOrCommitting(discardFlag, frameRange, message);
}


// ---------------------------------------------------------------------------

void TmainWindow::OnClipWasDeleted(void *Sender)
{
	CBinManager *binManager = (CBinManager*)Sender;
   auto deletedClipName = binManager->lastDeletedClipName;

   // Tell the tools that a clip was deleted.
	CToolManager toolMgr;
	toolMgr.onClipWasDeleted(deletedClipName);

   // Tell the PDL windows that a clip was deleted.
   CPDLViewerManager pdlViewerMgr;
	pdlViewerMgr.OnClipWasDeleted(deletedClipName);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ToolHelpMenuItemClick(TObject *Sender)
{
	UserGuideViewer userGuideViewer;
	userGuideViewer.showAndGoToToolPage();
}

// ---------------------------------------------------------------------------
void __fastcall TmainWindow::GlobalHotKeysHelpMenuItemClick(TObject *Sender)
{
	UserGuideViewer userGuideViewer;
	userGuideViewer.showAndGoToSection("GlobalHotKeys");
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::UserManualMenuItemClick(TObject *Sender)
{
	// string userManualPath = DEFAULT_USER_MANUAL_PATH;
	// ExpandEnviornmentString(userManualPath);
	// ShellExecute( nullptr,
	// "open",
	// (LPCTSTR) userManualPath.c_str(), // document to launch
	// nullptr,                    // parms -- not used  when launching a document
	// nullptr,                    // default dir (don't care here)
	// SW_SHOWNORMAL
	// );
	UserGuideViewer userGuideViewer;
	userGuideViewer.show();
}
//---------------------------------------------------------------------------

void __fastcall TmainWindow::ReleaseNotes1Click(TObject *Sender)
{
	ReleaseNotesViewer releaseNotesViewer;
	releaseNotesViewer.show();
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::DisplayPanelDblClick(TObject *Sender)
{
	// Mouse Down has set the values
	int imageX = clipDisplayer->scaleXClientToImageX(m_MouseX);
	int imageY = clipDisplayer->scaleYClientToImageY(m_MouseY);

	// Send the MouseDown event to the Tool Manager, which will dispatch it
	// to the appropriate tool
	CToolManager toolManager;

	// A double click is mouse down, mouse up, double click, mouse down, mouse up
	// This inhibites the last mouse down, mouse up events.
	m_doubleClick = true;
	toolManager.onMouseDoubleClick(m_Button, m_Shift, m_MouseX, m_MouseY, imageX, imageY);
}

// ---------------------------------------------------------------------------

#include "ClipPickerUnit.h"

void __fastcall TmainWindow::SelectClipMenuItemClick(TObject *Sender)
{
	string MyIniFileName;
	const string FormatFilter = "*";
	string fullClipName;

	fullClipName = TClipPickerForm::PickAClip(TClipPickerForm::NO_FLAGS, &MyIniFileName, &FormatFilter, &fullClipName);
	if (fullClipName != "")
	{
		OpenClipByName(fullClipName);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ExportFullClipClick(TObject *Sender)
{
	string versionClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	if (BinManagerForm == nullptr)
	{
		ShowBinManager(false); // invisible
	}

	BinManagerForm->ExportVersionClipFiles(versionClipPath);
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ExportMarkedRangeMenuItemClick(TObject *Sender)
{
	CPlayer *player = GetPlayer();
	string versionClipPath(GetCurrentClipHandler()->getCurrentClipPath());

	if (BinManagerForm == nullptr)
	{
		ShowBinManager(false); // invisible
	}

	BinManagerForm->ExportVersionClipFiles(versionClipPath, player->getMarkIn(), player->getMarkOut());
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::ExportVersionClipFilesMenuItemClick(TObject *Sender)

{
	CPlayer *player = GetPlayer();
	ExportMarkedRangeMenuItem->Enabled = player->getMarkIn() >= 0 && player->getMarkOut() > player->getMarkIn();

	ClipIdentifier currentClipId(GetCurrentClipHandler()->getCurrentClipFileName());
	ExportVersionClipFilesMenuItem->Enabled = currentClipId.IsVersionClip();
}
// ---------------------------------------------------------------------------

void TmainWindow::UnitTestCode()
{
	// Not test, just a way to debug junk
	// auto mask = (CMaskTool *)GColorBreathingTool->getSystemAPI()->GetMaskToolAsVoid();
	///   mask->SaveMaskToFile("c:\\temp\\junk.mask");

}

void __fastcall TmainWindow::MaskComputeTimerTimer(TObject *Sender)
{
	try
	{
		auto player = GetPlayer();
		if (player == nullptr)
		{
			return;
		}

		if (GetPlayer()->isReallyPlaying())
		{
			if (_wasNotPlaying)
			{
				_oldDisplayMaskBorder = GetDisplayer()->getDisplayMaskOverlay();
				GetDisplayer()->setDisplayMaskOverlay(false);
				_wasNotPlaying = false;
			}

			if (GetDisplayer()->getDisplayMaskOverlay())
			{
				GetDisplayer()->setDisplayMaskOverlay(false);
			}

			return;
		}

		if (_wasNotPlaying == false)
		{
			_wasNotPlaying = true;
			GetDisplayer()->setDisplayMaskOverlay(_oldDisplayMaskBorder);
			GetPlayer()->refreshFrameSynchronousCached();
			// mainWindow->MaskComputeTimer->Enabled = true;
		}

		auto maskAvailable = GetDisplayer()->getNewMaskAvailable();
		auto isMaskComputeRunning = GetDisplayer()->getMaskComputeRunning();

		if (maskAvailable == false && isMaskComputeRunning == false)
		{
			GetPlayer()->refreshFrameSynchronousCached();
			///		MaskComputeTimer->Enabled = false;
			return;
		}

		if (GetDisplayer()->getDisplayMaskOverlay())
		{
			GetPlayer()->refreshFrameSynchronousCached();
		}
	}

	catch (...)
	{
		mainWindow->MaskComputeTimer->Enabled = false;
		TRACE_0(errout << "Crash in mask timer");
	}
}
// ---------------------------------------------------------------------------

void __fastcall TmainWindow::RelinkMediaFolderButtonClick(TObject *Sender)
{
   // Relink from the master clip only.
	CNavCurrentClip* currentClipHandler = mainWindow->GetCurrentClipHandler();
	auto currentClip = currentClipHandler->getCurrentClip();
   ClipIdentifier clipId(currentClip->getClipPathName());
   RelinkClipMedia(clipId.GetMasterClipId());
}
//---------------------------------------------------------------------------
