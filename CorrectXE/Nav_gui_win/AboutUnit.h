//---------------------------------------------------------------------------

#ifndef AboutUnitH
#define AboutUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Buttons.hpp>

#include "ColorLabel.h"
#include "ColorPanel.h"

#include <string>
using std::string;

//---------------------------------------------------------------------------
class TAboutForm : public TForm
{
__published:	// IDE-managed Components
   TImage *BackgroundImage;
        TColorLabel *Comments;
        TColorLabel *Copyright;
        TColorLabel *Date;
   TColorLabel *PatentLabel1;
   TColorLabel *PatentLabel2;
	TColorLabel *VersionLabel;
   TSpeedButton *ExitSpeedButton;
   TColorPanel *WtfPanel2;
	TColorLabel *GpuLabel;
        void __fastcall AnywhereClick(TObject *Sender);
        void __fastcall LicenseStatusButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ShowLicensingButtonClick(TObject *Sender);
private:	// User declarations
        void GetMinLicenseDates(string &licenseExpireDate,
                                string &supportEndsDate,
                                bool &expiringSoon);
public:		// User declarations
        __fastcall TAboutForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAboutForm *AboutForm;
//---------------------------------------------------------------------------
#endif
