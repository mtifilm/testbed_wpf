//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AboutUnit.h"

#include "DllSupport.h"
#include "MTIWinInterface.h"      // for CBusyCursor
#include "ShowModalDialog.h"
#include "MTIstringstream.h"
#include "Ippheaders.h"
#include "GpuAccessor.h"

// Wrap DVS SDK include files so that the name are not mangled by C++
extern "C" {
#include "dvs_clib.h"
#include "dvs_fifo.h"
}

#define DVS_VERSION_IS_PRE_2_7 ((DVS_VERSION_MAJOR < 2) || ((DVS_VERSION_MAJOR == 2) && (DVS_VERSION_MINOR < 7)))

#include <algorithm>
using std::transform;

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TAboutForm *AboutForm;
//---------------------------------------------------------------------------
__fastcall TAboutForm::TAboutForm(TComponent* Owner)
        : TForm(Owner)
{
  char FileName[1024];

  GetModuleFileName( NULL,FileName, sizeof (FileName) );
                      DBTRACE(FileName);
	auto lib = ippGetLibVersion();
	MTIostringstream os;
	os << ", core: " << lib->Version;

  VersionLabel->Caption = (AnsiString)"Version " + GetDLLVersion(FileName).c_str() + os.str().c_str();

  GpuAccessor gpuAccessor;

  if (gpuAccessor.doesGpuExist() == false)
  {
  	 GpuLabel->Caption = gpuAccessor.getErrorMessage().c_str();
  }
  else
  {
     os.str("");
     os << "NVIDIA Driver V";
     os << gpuAccessor.getMajorDriverVersion() << "." << gpuAccessor.getMinorDriverVersion() << ", Runtime ";
     os << gpuAccessor.getMajorRuntimeVersion() << "." << gpuAccessor.getMinorRuntimeVersion() << "   ";
     os << (gpuAccessor.useGpu() ? "GPU Active" : "GPU User Disabled");
     GpuLabel->Caption = os.str().c_str();
  }

  Date->Caption = GetFileDateString(FileName).c_str();
  //Copyright->Caption = GetDLLCopyright(FileName).c_str();  //just using static label now, djm 7/2008
  Comments->Caption = GetDLLInfo(FileName, "Comments").c_str();
}
//---------------------------------------------------------------------------
void __fastcall TAboutForm::AnywhereClick(TObject *Sender)
{
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TAboutForm::LicenseStatusButtonClick(TObject *Sender)
{
//   if (LicenseForm != NULL)
//   {
//      LicenseForm->CheckExpirationDates(LICENSE_DIALOG_ALWAYS);
//   }
}
//---------------------------------------------------------------------------

void __fastcall TAboutForm::FormShow(TObject *Sender)
{
#ifndef NO_LICENSING
   if (LicenseForm != NULL)
   {
      int result = LicenseForm->CheckExpirationDates(LICENSE_DIALOG_NEVER);
      
      LicenseStatusGreenButton->Visible  = (result == LICENSE_STATUS_GREEN);
      LicenseStatusYellowButton->Visible = (result == LICENSE_STATUS_YELLOW);
      LicenseStatusRedButton->Visible    = (result == LICENSE_STATUS_ORANGE ||
                                            result == LICENSE_STATUS_RED);
   }

   CRsrcCtrl License;
   string licenseExpireDate;
   string supportEndsDate;
   bool danger;

   GetMinLicenseDates(licenseExpireDate, supportEndsDate, danger);

   string s = licenseExpireDate;
   transform(s.begin(), s.end(), s.begin(), toupper);
   if (s == "NEVER")
   {
      LicenseExpiresLabel->Caption = AnsiString("License never expires");
   }
   else if (s == "EXPIRED")
   {
      LicenseExpiresLabel->Caption = AnsiString("Licenses have expired!");
   }
   else
   {
      LicenseExpiresLabel->Caption = AnsiString("License expires ") +
                                     AnsiString(licenseExpireDate.c_str());
   }
   LicenseExpiresLabel->Left = ShowLicensingButton->Left +
                               ShowLicensingButton->Width -
                               LicenseExpiresLabel->Width;

   s = supportEndsDate;
   transform(s.begin(), s.end(), s.begin(), toupper);
   if (s == "ENDED")
   {
      SupportEndsLabel->Caption = AnsiString("Upgrade period has ended");
   }
   else if (s == "UNKNOWN")
   {
      SupportEndsLabel->Caption = AnsiString("Cannot determine upgradability");
   }
   else
   {
      SupportEndsLabel->Caption = AnsiString("Upgrades until ") +
                                  AnsiString(supportEndsDate.c_str());
   }
   SupportEndsLabel->Left = ShowLicensingButton->Left +
                            ShowLicensingButton->Width -
                            SupportEndsLabel->Width;

   DangerButton->Visible = danger;
#endif
}
//---------------------------------------------------------------------------

void __fastcall TAboutForm::ShowLicensingButtonClick(TObject *Sender)
{
//   if (LicenseStatusForm != NULL)
//	  ShowModalDialog(LicenseStatusForm);
}
//---------------------------------------------------------------------------

void TAboutForm::GetMinLicenseDates(string &licenseExpireDate,
                                    string &supportEndsDate,
                                    bool &expiringSoon)
{
   licenseExpireDate = "EXPIRED";
   supportEndsDate = "UNKNOWN";
   expiringSoon = false;

#ifndef NO_LICENSING

   CBusyCursor BusyCursor(true);
   CRsrcCtrl License;

   // Read all the features
   StringList Features = License.ListAllFeatures();
   int minSupportDaysLeft = INFINITE_NUMBER_OF_DAYS;
   int minLicenseDaysLeft = INFINITE_NUMBER_OF_DAYS;

   for (unsigned i = 0; i < Features.size(); ++i)
   {
      string sFeature = Features[i];
      bool success = License.CheckOut(sFeature);

      // We believe the support end date even if the license has expired
      if (success || (License.LastErrorNumber() == LM_LONGGONE))
      {
         int supportDaysLeft = License.GetNumberOfDaysUntilSupportExpires();
         if (supportDaysLeft >= 0 && supportDaysLeft < minSupportDaysLeft)
         {
            minSupportDaysLeft = supportDaysLeft;
            supportEndsDate = License.LicenseToDateStringThatMatchesFlex();
         }
      }

      // License expiration date is only valid if the checkout succeeded
      if (success)
      {
         int licenseDaysLeft = License.GetNumberOfDaysUntilLicenseExpires();
         if (licenseDaysLeft == INFINITE_NUMBER_OF_DAYS)
         {
            if (minLicenseDaysLeft == INFINITE_NUMBER_OF_DAYS)
               licenseExpireDate = "NEVER";
         }
         else if (licenseDaysLeft >= 0 && licenseDaysLeft < minLicenseDaysLeft)
         {
            minLicenseDaysLeft = licenseDaysLeft;
            licenseExpireDate = License.ExpirationDate();
         }
         License.CheckIn();
      }
   }

   // return a flag if a license is expiring within a week
   if (minLicenseDaysLeft < 7)
      expiringSoon = true;

#endif

}
//---------------------------------------------------------------------------




