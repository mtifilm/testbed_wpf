//---------------------------------------------------------------------------

#ifndef GLErrorH
#define GLErrorH
//---------------------------------------------------------------------------

#include <string>
using std::string;
//---------------------------------------------------------------------------

bool _check_gl_error(const char *file, int line);

///
/// Usage
/// [... some opengl calls]
/// glCheckError();
///
#define check_gl_error() _check_gl_error(__FILE__, __LINE__)

#endif
