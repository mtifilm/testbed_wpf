//---------------------------------------------------------------------------

#ifndef ManagemediaUnitH
#define ManagemediaUnitH
#include "ColorLabel.h"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
//---------------------------------------------------------------------------
#include <string>
using std::string;

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "ColorLabel.h"
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TManageMediaForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *MasterClipMediaFolderLabel;
   TEdit *MasterClipMediaFolderEdit;
   TColorLabel *MasterClipMediaFolderErrorLabel;
   TButton *MasterClipMediaFolderBrowseButton;
   TPanel *ButtonPanel;
   TButton *OkButton;
   TButton *CancelButton;
   TEdit *VersionClipMediaParentFolderEdit;
   TLabel *VersionClipMediaParentFolderLabel;
   TColorLabel *VersionClipMediaParentFolderErrorLabel;
   TButton *VersionClipMediaParentFolderBrowseButton;
   TCheckBox *VersionMediaParentSameAsMasterCheckBox;
   TEdit *VersionClipMediaFolderTemplateEdit;
   TLabel *VersionClipMediaFolderTemplateLabel;
   TColorLabel *VersionClipMediaFolderTemplateErrorLabel;
   void __fastcall MasterClipMediaFolderBrowseButtonClick(TObject *Sender);
   void __fastcall VersionClipMediaParentFolderBrowseButtonClick(TObject *Sender);
   void __fastcall VersionMediaParentSameAsMasterCheckBoxClick(TObject *Sender);
   void __fastcall TextEditExit(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall FormHide(TObject *Sender);
   void __fastcall TextEditKeyPress(TObject *Sender, System::WideChar &Key);


private:	// User declarations
   bool _initedMasterClipMediaFolder = false;
   bool _initedVersionClipMediaParentFolder = false;
   bool _initedVersionClipMediaFolderTemplate = false;
   string _fileNameThatMustExistInRelinkedFolder;
   string _oldMasterClipMediaFolder;

   bool isVersionMediaParentSameAsMaster();
   string findAnAcceptableImageFile(string directory);
   bool computeFolderPathOfFrameFile(
      const string &newPath,
      const string &oldParentPath,
      const string &filename,
      string &resultPath);
   int countPoundSigns(const string &templateString);
   bool containsBadChars(const string &templateString, const string &badChars);
   void updateState();

public:		// User declarations
   __fastcall TManageMediaForm(TComponent* Owner);
   void fillIn(
      const string &masterClipMediaFolder,
      const string &versionClipMediaParentFolder,
      const string &versionClipMediaFolderTemplate);
   string getMasterClipMediaFolder();
   string getVersionClipMediaParentFolder();
   string getVersionClipMediaFolderTemplate();
   void setMasterClipMediaFolder(const string &newFolder);
   void setVersionClipMediaParentFolder(const string &newFolder);
   void setVersionClipMediaFolderTemplate(const string &newTemplate);
   void setFileNameThatMustExistInRelinkedFolder(const string &filename);
};
//---------------------------------------------------------------------------
extern PACKAGE TManageMediaForm *ManageMediaForm;
//---------------------------------------------------------------------------

#endif
