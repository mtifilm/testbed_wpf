//
// README: Portions of this file are merged at file generation
// time. Edits can be made *only* in between specified code blocks, look
// for keywords <Begin user code> and <End user code>.
//
//////////////////////////////////////////////////////////////
//
// Header file for Test
//
//    This class is a ViewKit window subclass as described in
//    the ViewKit Programmer's Guide.  See the documentation
//    for VkSimpleWindow and VkWindow for more information on
//    how to use this class.
//
// This file created with:
//
//    Builder Xcessory Version 5.0.8
//    Code Generator Xcessory 5.0.3 (02/28/00) Script Version 5.0.8
//
//////////////////////////////////////////////////////////////


// Begin user code block <file_comments>
// End user code block <file_comments>

#ifndef Test_H
#define Test_H
#include <Vk/VkSimpleWindow.h>

//
// Globally included information (change thru Output File Names Dialog).
//


//
// Class Specific Includes (change thru the class in Resource Editor).
//



// Begin user code block <head>
// End user code block <head>

#ifndef VKDEFINES 
#define VKDEFINES 
typedef struct _UIAppDefault {
    char*      cName;       // Class name 
    char*      wName;       // Widget name 
    char*      cInstName;   // Name of class instance (nested class) 
    char*      wRsc;        // Widget resource 
    char*      value;       // value read from app-defaults 
} UIAppDefault;

#endif

class Test : public VkSimpleWindow

// Begin user code block <superclass>
// End user code block <superclass>
{

// Begin user code block <friends>
// End user code block <friends>

  public:

    Test(const char *,
        ArgList args = NULL,
        Cardinal argCount = 0 );
    virtual ~Test();
    const char *className();
    
    // Begin user code block <public>
    // End user code block <public>
  protected:
    virtual void create(Widget);
    
    // Widgets and Components created by Test
    Widget _form;
    Widget _directionToggleButton;
    Widget _setDirectionButton;
    Widget _loadVTRButton;
    Widget _textField;
    Widget _recordButton;
    Widget _loadClipButton;
    
    // These virtual functions are called from the private callbacks 
    // or event handlers intended to be overridden in derived classes
    // to define actions
    
    virtual void DirectionButtonCallBack(Widget, XtPointer, XtPointer);
    virtual void LoadButtonPress(Widget, XtPointer, XtPointer);
    virtual void RecordButtonPress(Widget, XtPointer, XtPointer);
    virtual void LoadClipButtonPress(Widget, XtPointer, XtPointer);
    
    // Begin user code block <protected>
    // End user code block <protected>
  private: 
    
    //
    // Useful declarations for setting Xt Resources.
    //
    Cardinal          ac;
    Arg               args[256];
    
    //
    // Default application and class resources.
    //
    static String     _defaultTestResources[];
    static UIAppDefault   _appDefaults[];
    static Boolean        _initAppDefaults;
    
    //
    // Default callback client data.
    //
    //
    // Modification by rhartley@ics.com Dec 15, 2000.
    //
    //
    // If left as private, it cannot be accessed....
    //
    protected:
    VkCallbackStruct  _default_callback_data;
    
    //
    // ViewKit Static Menu Structures.
    //
    
    //
    // Callbacks to interface with Motif.
    //
    static void DirectionButtonCallBackCallback(Widget, XtPointer, XtPointer);
    static void LoadButtonPressCallback(Widget, XtPointer, XtPointer);
    static void RecordButtonPressCallback(Widget, XtPointer, XtPointer);
    static void LoadClipButtonPressCallback(Widget, XtPointer, XtPointer);
    
    // Begin user code block <private>
    // End user code block <private>
};

// Begin user code block <tail>
extern Test *theTest;

// End user code block <tail>
#endif
