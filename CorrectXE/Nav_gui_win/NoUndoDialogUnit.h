//---------------------------------------------------------------------------

#ifndef NoUndoDialogUnitH
#define NoUndoDialogUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TNoUndoForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BackPanel;
        TImage *Image1;
        TLabel *Label2;
        TLabel *Label3;
        TPanel *Panel1;
        TButton *Button1;
        TButton *Button2;
private:	// User declarations
public:		// User declarations
    __fastcall TNoUndoForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TNoUndoForm *NoUndoForm;
//---------------------------------------------------------------------------
#endif
