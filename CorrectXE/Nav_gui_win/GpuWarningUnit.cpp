//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainWindowUnit.h"
#include "GpuWarningUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#include <process.h>

TgpuWarningForm *gpuWarningForm;
//---------------------------------------------------------------------------
__fastcall TgpuWarningForm::TgpuWarningForm(TComponent* Owner)
	: TForm(Owner)
{
}

static const string geForceFileName = "$(ProgramFiles(x86))\\NVIDIA Corporation\\NVIDIA GeForce Experience\\NVIDIA GeForce Experience.exe";

//---------------------------------------------------------------------------
void __fastcall TgpuWarningForm::GpuWarningCheckBoxClick(TObject *Sender)
{
   mainWindow->SetGpuWarning(GpuWarningCheckBox->Checked == false);
}
//---------------------------------------------------------------------------

void __fastcall TgpuWarningForm::GotoDriveWebPageSpeedButtonClick(TObject *Sender)
{
	HINSTANCE handle = ShellExecute(
									NULL,
									"open",
									"https://www.nvidia.com/Download/index.aspx", // document to launch
									NULL,                    // parms -- not used  when launching a document
									NULL,                    // default dir (don't care here)
									SW_SHOWNORMAL);

//"c:\\command.com","https://www.nvidia.com/Download/index.aspx","","",NULL);
}
//---------------------------------------------------------------------------

void __fastcall TgpuWarningForm::LaunchGeForceSpeedButtonClick(TObject *Sender)
{
   auto fileName = geForceFileName;
	ExpandEnviornmentString(fileName);
	HINSTANCE handle = ShellExecute(
									NULL,
									"open",
									fileName.c_str(),          // document to launch
									NULL,                      // parms -- not used  when launching a document
									fileName.c_str(),          // default dir (don't care here)
									SW_SHOWNORMAL);

//   spawnl( _P_NOWAIT, fileName.c_str(), "Stupid", nullptr );

}

//---------------------------------------------------------------------------

void __fastcall TgpuWarningForm::FormShow(TObject *Sender)
{
   auto fileName = geForceFileName;
   ExpandEnviornmentString(fileName);
   LaunchGeForceSpeedButton->Enabled = DoesFileExist(fileName);
}
//---------------------------------------------------------------------------

