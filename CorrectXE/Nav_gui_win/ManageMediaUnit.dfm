object ManageMediaForm: TManageMediaForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Manage Media Locations'
  ClientHeight = 206
  ClientWidth = 584
  Color = clBtnFace
  Constraints.MaxHeight = 240
  Constraints.MinHeight = 240
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnHide = FormHide
  OnShow = FormShow
  DesignSize = (
    584
    206)
  PixelsPerInch = 96
  TextHeight = 13
  object MasterClipMediaFolderLabel: TLabel
    Left = 11
    Top = 16
    Width = 117
    Height = 13
    Caption = 'Master Clip Media Folder'
  end
  object MasterClipMediaFolderErrorLabel: TColorLabel
    Left = 140
    Top = 16
    Width = 121
    Height = 13
    Caption = ' Error: Folder not found! '
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4210880
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
    Visible = False
  end
  object VersionClipMediaParentFolderLabel: TLabel
    Left = 11
    Top = 80
    Width = 154
    Height = 13
    Caption = 'Version Clip Media Parent Folder'
  end
  object VersionClipMediaParentFolderErrorLabel: TColorLabel
    Left = 348
    Top = 80
    Width = 121
    Height = 13
    Caption = ' Error: Folder not found! '
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4210880
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
    Visible = False
  end
  object VersionClipMediaFolderTemplateLabel: TLabel
    Left = 11
    Top = 129
    Width = 166
    Height = 13
    Caption = 'Version Clip Media Folder Template'
    Visible = False
  end
  object VersionClipMediaFolderTemplateErrorLabel: TColorLabel
    Left = 190
    Top = 129
    Width = 195
    Height = 13
    Caption = ' Error: Template must contain a single # '
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4210880
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
    Visible = False
  end
  object MasterClipMediaFolderEdit: TEdit
    Left = 8
    Top = 35
    Width = 482
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnExit = TextEditExit
    OnKeyPress = TextEditKeyPress
  end
  object MasterClipMediaFolderBrowseButton: TButton
    Left = 501
    Top = 33
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Browse'
    TabOrder = 1
    OnClick = MasterClipMediaFolderBrowseButtonClick
  end
  object ButtonPanel: TPanel
    Left = 413
    Top = 163
    Width = 168
    Height = 41
    Anchors = [akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 187
    object OkButton: TButton
      Left = 2
      Top = 8
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object CancelButton: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object VersionClipMediaParentFolderEdit: TEdit
    Left = 8
    Top = 99
    Width = 482
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Enabled = False
    TabOrder = 3
    OnExit = TextEditExit
    OnKeyPress = TextEditKeyPress
  end
  object VersionClipMediaParentFolderBrowseButton: TButton
    Left = 501
    Top = 97
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Browse'
    Enabled = False
    TabOrder = 4
    OnClick = VersionClipMediaParentFolderBrowseButtonClick
  end
  object VersionMediaParentSameAsMasterCheckBox: TCheckBox
    Left = 194
    Top = 79
    Width = 139
    Height = 17
    Caption = 'Same as Master Clip'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = VersionMediaParentSameAsMasterCheckBoxClick
  end
  object VersionClipMediaFolderTemplateEdit: TEdit
    Left = 8
    Top = 148
    Width = 211
    Height = 21
    TabOrder = 6
    Text = 'clipname (v#)'
    Visible = False
    OnExit = TextEditExit
  end
end
