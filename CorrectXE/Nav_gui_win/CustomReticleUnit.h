//---------------------------------------------------------------------------

#ifndef CustomReticleUnitH
#define CustomReticleUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include "Displayer.h"
#include "IniFile.h"
#include "Reticle.h"
#include "SpinEditFrameUnit.h"

class CToolSystemInterface;

//---------------------------------------------------------------------------
class TCustomReticleDlg : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *ReticleNameGroupBox;
         TComboBox *ReticleNameComboBox;
         TButton *SaveReticleButton;
         TButton *DeleteReticleButton;
        TRadioGroup *ApertureGroupBox;
        TGroupBox *OffsetBox;
         TLabel *XLabel;
         TLabel *YLabel;
        TGroupBox *Scale;
         TLabel *HLabel;
         TLabel *VLabel;
        TButton *RETOKButton;
        TButton *RETCancelButton;
        TRadioGroup *AspectRatioRadioGroup;
        TButton *PresetButton133;
        TButton *PresetButton166;
        TButton *PresetButton178;
        TButton *PresetButton185;
        TButton *PresetButton235;
        TButton *PresetButton240;
        TEdit *AspectRatioEditBox;
	TSpinEditFrame *XOffsetSpinEdit;
	TSpinEditFrame *YOffsetSpinEdit;
	TSpinEditFrame *HScaleSpinEdit;
	TSpinEditFrame *VScaleSpinEdit;

        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall OnLoadReticle(TObject *Sender);
        void __fastcall ReticleNameKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall SaveReticleButtonClick(TObject *Sender);
        void __fastcall DeleteReticleButtonClick(TObject *Sender);
        void __fastcall PresetButtonClick(TObject *Sender);
        void __fastcall OnAspectRatioKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ApertureGroupBoxClick(TObject *Sender);
        void __fastcall RETOKButtonClick(TObject *Sender);
        void __fastcall RETCancelButtonClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);

private:	// User declarations
        bool inhibitSpinEditCallbacks;
        DEFINE_CBHOOK(OffsetSpinEditChange, TCustomReticleDlg);

        int GetDefaultReticleType(string);
        int GetCustomReticleIndex(string);
        void SaveCurrentReticle();
        void DeleteCurrentReticle();
        void UpdateCustomAspectRatio(double);
        void ChangeAspectRatio();

public:		// User declarations
//		TSpinEditFrame *XOffsetSpinEdit;
//		TSpinEditFrame *YOffsetSpinEdit;
//		TSpinEditFrame *HScaleSpinEdit;
//		TSpinEditFrame *VScaleSpinEdit;

        __fastcall TCustomReticleDlg(TComponent* Owner);

        void setSystemAPI(CToolSystemInterface *);
        void LoadReticleListIni();
        void WriteReticleListIni();
        void LoadReticle(const string&);
        void UpdateReticleGUI();

private:
        static string reticleUnnamed;
        static string reticleSectionName;
        static string defaultReticle[16];

   CToolSystemInterface *systemAPI;

   StringList localnameList;
};
//---------------------------------------------------------------------------
extern PACKAGE TCustomReticleDlg *CustomReticleDlg;
//---------------------------------------------------------------------------
#endif
