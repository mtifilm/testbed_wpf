object gpuWarningForm: TgpuWarningForm
  Left = 0
  Top = 0
  Caption = 'Gpu Warning'
  ClientHeight = 255
  ClientWidth = 412
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 106
  TextHeight = 14
  object WarningLabel: TLabel
    Left = 24
    Top = 16
    Width = 375
    Height = 150
    AutoSize = False
    Caption = 
      'Nova has detected a compatible GPU'#13#10'However the driver version i' +
      's not current and must be updated.'#13#10#13#10'Please update the driver f' +
      'rom either'#13#10'     The NVIDIA website'#13#10'     The GeForce Experience' +
      ' App'#13#10#13#10'If available, GeForce Experience is recommened'#13#10'(GeForce' +
      ' Launch is very slow)'
    WordWrap = True
  end
  object GotoDriveWebPageSpeedButton: TSpeedButton
    Left = 24
    Top = 172
    Width = 135
    Height = 25
    Caption = 'Goto NVIDIA Drivers '
    OnClick = GotoDriveWebPageSpeedButtonClick
  end
  object LaunchGeForceSpeedButton: TSpeedButton
    Left = 212
    Top = 172
    Width = 135
    Height = 25
    Caption = 'Launch GeForce'
    Enabled = False
    OnClick = LaunchGeForceSpeedButtonClick
  end
  object Button1: TButton
    Left = 324
    Top = 216
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object GpuWarningCheckBox: TCheckBox
    Left = 24
    Top = 220
    Width = 225
    Height = 17
    Caption = 'Do not show this warning again'
    TabOrder = 1
    OnClick = GpuWarningCheckBoxClick
  end
end
