#ifndef Shader1Glsl
#define Shader1Glsl

#include <string>
using std::string;

static string VertexShader1Source =
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"layout (location = 1) in vec2 aTexCoord;\n"
"\n"
"uniform float scale;\n"
"uniform float xOffset;\n"
"uniform float yOffset;\n"
"\n"
"out vec2 TexCoord;\n"
"\n"
"void main()\n"
"{\n"
"   vec3 aPosScaled = aPos * scale;\n"
"   vec3 offsetPos = vec3(xOffset, yOffset, 0.0);\n"
"   gl_Position = vec4(aPosScaled + offsetPos, 1.0);\n"
"   TexCoord = aTexCoord;\n"
"};\n";

static string FragmentShader1Source =
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"in vec2 TexCoord;\n"
"\n"
"\n"
"uniform sampler2D texture1;\n"
"\n"
"void main()\n"
"{\n"
"   FragColor = vec4(0, 0, 0, 0);\n" //texture(texture1, TexCoord);\n"
"};\n";

#endif


