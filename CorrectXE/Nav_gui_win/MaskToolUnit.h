//---------------------------------------------------------------------------

#ifndef MaskToolUnitH
#define MaskToolUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include "MTIUNIT.h"
//---------------------------------------------------------------------------
class TMaskToolWindow : public TMTIForm
{
__published:	// IDE-managed Components
   TGroupBox *GroupBox1;
   TGroupBox *GroupBox2;
   TGroupBox *GroupBox3;
   TCheckListBox *CheckListBox1;
   TGroupBox *GroupBox4;
   TGroupBox *GroupBox5;
   TGroupBox *GroupBox6;
   TGroupBox *GroupBox7;
   TSpeedButton *SpeedButton1;
   TSpeedButton *SpeedButton2;
   TSpeedButton *SpeedButton3;
   TSpeedButton *SpeedButton4;
   TSpeedButton *SpeedButton5;
   TComboBox *ComboBox1;
   TSpeedButton *SpeedButton6;
   TSpeedButton *SpeedButton7;
   TSpeedButton *SpeedButton8;
   TSpeedButton *SpeedButton9;
   TSpeedButton *SpeedButton10;
   TSpeedButton *SpeedButton11;
   TSpeedButton *SpeedButton12;
   TSpeedButton *SpeedButton13;
   TSpeedButton *SpeedButton14;
   TSpeedButton *SpeedButton15;
   TSpeedButton *SpeedButton16;
   TSpeedButton *SpeedButton17;
   TSpeedButton *SpeedButton18;
   TSpeedButton *SpeedButton19;
   TSpeedButton *SpeedButton20;
   TSpeedButton *SpeedButton21;
   TCheckBox *CheckBox1;
   TTrackBar *TrackBar1;
   TLabel *Label1;
   TLabel *Label2;
   TCheckBox *CheckBox3;
   TTrackBar *TrackBar3;
   TLabel *Label5;
   TLabel *Label6;
private:	// User declarations
public:		// User declarations
   __fastcall TMaskToolWindow(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMaskToolWindow *MaskToolWindow;
//---------------------------------------------------------------------------
#endif
