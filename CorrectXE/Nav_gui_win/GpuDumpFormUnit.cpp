//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GpuDumpFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TGpuDumpForm *GpuDumpForm;
//---------------------------------------------------------------------------
__fastcall TGpuDumpForm::TGpuDumpForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TGpuDumpForm::Clear()
{
    HeaderDumpMemo->Lines->Text = "";
}
//---------------------------------------------------------------------------
void TGpuDumpForm::Set(const string &newText)
{
    HeaderDumpMemo->Lines->Text = newText.c_str();
}
void __fastcall TGpuDumpForm::CloseButtonClick(TObject *Sender)
{
    Hide();
}
//---------------------------------------------------------------------------
