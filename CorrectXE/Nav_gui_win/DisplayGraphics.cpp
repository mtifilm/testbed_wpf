//---------------------------------------------------------------------------

#pragma hdrstop

#include "DisplayGraphics.h"
#include "GLError.h"
#include "TheError.h"
#include "PixelRegions.h"

//#include <gl\gl.h>
//#include <gl\glu.h>
//#include "glad/glad.h"
#include <GL/glew.h>
#include "Shader.h"
#include "Shader1.glsl"

////#define USE_TEXTURE

//---------------------------------------------------------------------------
#pragma package(smart_init)
//------------------------------------------------------------------------------

static float glVertices[6][5] = {
	// positions               // texture coords
	{-1.00f,  1.0f,  0.6250f,   1.000f, 1.0f },   // top left
	{ 0.25f,  1.0f,  0.0000f,   0.375f, 1.0f },   // top middle
	{ 1.00f,  1.0f, -0.3750f,   0.000f, 1.0f },   // top right
	{ 1.00f, -1.0f, -0.6250f,   0.000f, 0.0f },   // bottom right
	{-0.25f, -1.0f,  0.0000f,   0.625f, 0.0f },   // bottom middle
	{-1.00f, -1.0f,  0.3750f,   1.000f, 0.0f },   // bottom left
};

static unsigned int glTriangleIndices[4][3] = {
	{ 0, 1, 5 },  // first triangle
	{ 1, 4, 5 },  // second triangle
	{ 1, 3, 4 },  // third triangle
	{ 1, 2, 3 },  // fourth triangle
};

static unsigned int glVBO;
static unsigned int glVAO;
static unsigned int glEBO;
static unsigned int glTexture0;
static unsigned int glTexture1;
static Shader *glShader1 = nullptr;
static RECT texRect;
static int glWindowWidth;
static int glWindowHeight;
static int textureWidth;
static int textureHeight;
//------------------------------------------------------------------------------

// this call sets the screen device context, rendering context,
// and client rectangle. It is called at OnResize time.
int CDisplayer::initDisplayGraphics(HWND handle, HDC scrndc, HGLRC rndrrc, int clientWidth, int clientHeight)
{
	CAutoErrorReporter autoErr("CDisplayer::initDisplayGraphics", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// save the window handle
	windowHandle = handle;

	// the screen device context
	screenDC = scrndc;

	// and the render context
	renderRC = rndrrc;

	// make the device contexts current
	bool err = wglMakeCurrent(screenDC, renderRC);
	if (!err)
	{
      // Add magic number 100000 to Windows error codes.
		auto lastError = ::GetLastError() + 100000;
		autoErr.errorCode = lastError;
		autoErr.msg << "wglMakeCurrent failed, Error: " << lastError;
		return lastError;
	}

	GLenum glewErr = glewInit();
	if (GLEW_OK != glewErr)
	{
	  /* Problem: glewInit failed, something is seriously wrong. */
	  TRACE_0(errout << "Error: " << glewGetErrorString(err));
	  return 200000 + glewErr;
	}

////	TRACE_0(errout << "Info: Using GLEW " << glewGetString(GLEW_VERSION));

//	//  Initialize GLAD before we call any OpenGL function.
//	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
//	{
//		std::cout << "Failed to initialize GLAD" << std::endl;
//		return -1;
//	}

	int p[4];
	glGetIntegerv(GL_VIEWPORT, p);
	check_gl_error();

	// OpenGL initializations
	//
	// when BLEND is used, it is disabled afterwards
	glDisable(GL_BLEND);
	check_gl_error();

#ifdef USE_TEXTURE
	glEnable(GL_TEXTURE_2D);
#else
	// never used
	glDisable(GL_TEXTURE_2D);
#endif

	// never used
	glDisable(GL_DITHER);
	check_gl_error();

	// when FRONT is used, BACK is reset afterwards
	glDrawBuffer(GL_BACK);
	check_gl_error();

	// set the viewport from the client rectangle
	glWindowWidth = clientWidth;
	glWindowHeight = clientHeight;
	glViewport(0, 0, clientWidth, clientHeight);
	check_gl_error();

	// byte boundary
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	check_gl_error();

	// projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	check_gl_error();

	// set the clipping volume
	glOrtho(0, clientWidth, clientHeight, 0, -1.0, 1.0);
	check_gl_error();

	// model matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	check_gl_error();

#ifdef USE_TEXTURE

//	// This is a 2D texture image of the same size as the frame buffer
//	glBindTexture(GL_TEXTURE_2D, 1);
//	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
//	glPixelStorei(GL_UNPACK_ROW_LENGTH, textureWdth);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
//
//	// load the CLEARED texture image - this will cause a
//	// flash when transitioning to and from FullScreen
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWdth, textureHght, 0, GL_RGBA, GL_UNSIGNED_BYTE, screenFrameBuffer);

	glGenVertexArrays(1, &glVAO);
	glGenBuffers(1, &glVBO);
	glGenBuffers(1, &glEBO);
	check_gl_error();

	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(glVAO);
	glBindBuffer(GL_ARRAY_BUFFER, glVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glVertices), glVertices, GL_STATIC_DRAW);
	check_gl_error();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glTriangleIndices), glTriangleIndices, GL_STATIC_DRAW);
	check_gl_error();

	// 3. then set our vertex attributes pointers
	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	//texture position attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	check_gl_error();

	// load and generate the texture
	glGenTextures(1, &glTexture0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, glTexture0);
	check_gl_error();

	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	check_gl_error();

	glGenTextures(1, &glTexture1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, glTexture1);
	check_gl_error();

	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	check_gl_error();

	// Create uninitialized textures
	TRACE_0(errout << "ALLOCATING TEXTURES");
	glActiveTexture(GL_TEXTURE0);
	textureWidth = 1024;
	textureHeight = 768;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glActiveTexture(GL_TEXTURE1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	check_gl_error();
	TRACE_0(errout << "imgRect (" << imgRect.left << ", " << imgRect.top << ", " << imgRect.right << ", " << imgRect.bottom << ") "
						<< "rgbRect (" << rgbRect.left << ", " << rgbRect.top << ", " << rgbRect.right << ", " << rgbRect.bottom << ") ");
	texRect = rgbRect;

	if (glShader1 == nullptr)
	{
		glShader1 = new Shader(VertexShader1Source.c_str(), FragmentShader1Source.c_str());
	}

#endif

	// reset the graphics color after context switch
	setGraphicsColor(graphicsColor);

	return (0);
}
//-----------------------------------------------------------------------------

void CDisplayer::clearBackBuffer()
{
	// fill image rectangle w bgnd color
	glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
	glClear(GL_COLOR_BUFFER_BIT);
}
//-----------------------------------------------------------------------------

void CDisplayer::blitPictureToBackBuffer(unsigned int *screenFrameBuffer)
{
#ifdef USE_TEXTURE

	MTIassert(glShader1);
	if (glShader1 == nullptr)
	{
		return;
	}

	// set the viewport from the client rectangle
	glViewport(rgbRect.left, rgbRect.top, textureWidth, textureHeight);
	check_gl_error();

	glActiveTexture(GL_TEXTURE0); // activate the texture unit first before binding texture
	check_gl_error();
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
// glTexSubImage2D(GL_TEXTURE_2D, 0, imgRect.left - rgbRect.left, imgRect.top - rgbRect.top, rgbWdth, rgbHght, GL_RGBA,  GL_UNSIGNED_BYTE,  screenFrameBuffer);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textureWidth, textureHeight, GL_RGBA,  GL_UNSIGNED_BYTE,  screenFrameBuffer);
	check_gl_error();
	if (true)
	{
		TRACE_0(errout << "imgRect (" << imgRect.left << ", " << imgRect.top << ", " << imgRect.right << ", " << imgRect.bottom << ") "
							<< "rgbRect (" << rgbRect.left << ", " << rgbRect.top << ", " << rgbRect.right << ", " << rgbRect.bottom << ") ");
	}

	glGenerateMipmap(GL_TEXTURE_2D);
	check_gl_error();
	glBindTexture(GL_TEXTURE_2D, glTexture0);
	check_gl_error();

	glShader1->use();
	check_gl_error();
	glShader1->setInt("texture1", 0);
	check_gl_error();
	float MixRatio = 0.0;
	glShader1->setFloat("mixValue", MixRatio);
	check_gl_error();
	float sliderValue = MixRatio - 0.5;
	glShader1->setFloat("SliderValue", sliderValue);
	check_gl_error();
	glBindVertexArray(glVAO);
	check_gl_error();

	//glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
	check_gl_error();

	// Reset the viewport from the client rectangle
	glViewport(0, 0, glWindowWidth, glWindowHeight);
	check_gl_error();

#else

	// blit the picture to the back buffer
	glDisable(GL_TEXTURE_2D);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, rgbRectWidth);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, imgRect.left - rgbRect.left);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, imgRect.top - rgbRect.top);
	glRasterPos2i(imgRect.left, imgRect.top);
	check_gl_error();

	RECT debugInspect = imgRect;
	RECT debugDisplay = dspRect;

	glPixelZoom(1, -1);
	glDrawPixels(imgWdth, imgHght, GL_RGBA, GL_UNSIGNED_BYTE, screenFrameBuffer);
	check_gl_error();

#endif
}
//-----------------------------------------------------------------------------

void CDisplayer::blitSideBySidePictureToBackBuffer(unsigned int *screenFrameBuffer)
{
#ifdef USE_TEXTURE

	MTIassert(glShader1);
	if (glShader1 == nullptr)
	{
		return;
	}

	// set the viewport from the client rectangle
	glViewport(leftRgbRect.left, leftRgbRect.top, textureWidth, textureHeight);
	check_gl_error();

	glActiveTexture(GL_TEXTURE1); // activate the texture unit first before binding texture
	check_gl_error();
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
// glTexSubImage2D(GL_TEXTURE_2D, 0, imgRect.left - rgbRect.left, imgRect.top - rgbRect.top, rgbWdth, rgbHght, GL_RGBA,  GL_UNSIGNED_BYTE,  screenFrameBuffer);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textureWidth, textureHeight, GL_RGBA,  GL_UNSIGNED_BYTE,  screenFrameBuffer);
	check_gl_error();
	if (true)
	{
		TRACE_0(errout << "imgRect (" << imgRect.left << ", " << imgRect.top << ", " << imgRect.right << ", " << imgRect.bottom << ") "
							<< "leftRgbRect (" << leftRgbRect.left << ", " << leftRgbRect.top << ", " << leftRgbRect.right << ", " << leftRgbRect.bottom << ") ");
	}

	glGenerateMipmap(GL_TEXTURE_2D);
	check_gl_error();
	glBindTexture(GL_TEXTURE_2D, glTexture0);
	check_gl_error();

	glShader1->use();
	check_gl_error();
	glShader1->setInt("texture1", 0);
	check_gl_error();
	float MixRatio = 0.0;
	glShader1->setFloat("mixValue", MixRatio);
	check_gl_error();
	float sliderValue = MixRatio - 0.5;
	glShader1->setFloat("SliderValue", sliderValue);
	check_gl_error();
	glBindVertexArray(glVAO);
	check_gl_error();

	//glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
	check_gl_error();

	// Reset the viewport from the client rectangle
	glViewport(0, 0, glWindowWidth, glWindowHeight);
	check_gl_error();

#else

//	// set the viewport to not wipe out the right-side frame
//	glViewport(0, 0, leftRgbRect.right + 1, glWindowHeight);
//	check_gl_error();

	// blit the picture to the back buffer
	glDisable(GL_TEXTURE_2D);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, rgbRectWidth);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, imgRect.left - rgbRect.left);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, imgRect.top - rgbRect.top);
	glRasterPos2i(leftRgbRect.left, imgRect.top);
	MTIassert(!check_gl_error());

	glPixelZoom(1, -1);
	glDrawPixels(imgWdth, imgHght, GL_RGBA, GL_UNSIGNED_BYTE, screenFrameBuffer);
	MTIassert(!check_gl_error());

//	// Reset the viewport from the client rectangle
//	glViewport(0, 0, glWindowWidth, glWindowHeight);
//	check_gl_error();

#endif
}
//-----------------------------------------------------------------------------

void CDisplayer::blendPictureToBackBuffer(unsigned int *screenFrameBuffer)
{
	// blend the picture to the back buffer
	glDisable(GL_TEXTURE_2D);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, rgbRectWidth);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, imgRect.left - rgbRect.left);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, imgRect.top - rgbRect.top);
	glRasterPos2i(imgRect.left, imgRect.top);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPixelZoom(1, -1);
	glDrawPixels(imgWdth, imgHght, GL_RGBA, GL_UNSIGNED_BYTE, screenFrameBuffer);
	glDisable(GL_BLEND);
}
//-----------------------------------------------------------------------------

void CDisplayer::blendMaskBorderLinesToBackBuffer(unsigned char *overlayMask)
{
	///blend the picture to the back buffer
	clipToImageRectangle();

	glDisable(GL_TEXTURE_2D);

	// If the frame isn't completely visible, we need to skip some lines
	// and/or columns of the overlay mask.
	glPixelStorei(GL_UNPACK_ROW_LENGTH, frameWidth);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, dspRect.left);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, dspRect.top);

	glRasterPos2i(imgRect.left, imgRect.top);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPixelZoom(imgWdth / float(dspWdth), -imgHght / float(dspHght));
	glDrawPixels(frameWidth, frameHeight - dspRect.top, GL_RGBA, GL_UNSIGNED_BYTE, overlayMask);
	glDisable(GL_BLEND);
	MTIassert(!check_gl_error());

	// restore default clipping
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

int CDisplayer::setGraphicsColor(int color)
{
	int retval = graphicsColor;
	graphicsColor = color;

	// color determines pen
	int col = colorPalette[graphicsColor];

	glColor4ub((col&0xff), ((col >> 8)&0xff), ((col >> 16)&0xff), 0);

	// if odd color dash it
	if (graphicsColor & 1)
	{
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0xf8f8);
	}
	else
	{
		glDisable(GL_LINE_STIPPLE);
	}

	return (retval);
}
//------------------------------------------------------------------------------

int CDisplayer::hackTheColor(MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B)
{
	glColor4ub(R, G, B, 0);
	glDisable(GL_LINE_STIPPLE);
	return graphicsColor;
}
//------------------------------------------------------------------------------

int CDisplayer::setHighlightColor(int color)
{
	int retval = highlightColor;
	highlightColor = color;
	return (retval);
}
//------------------------------------------------------------------------------

int CDisplayer::setMatteColor()
{
	int retVal = graphicsColor;
	graphicsColor = rawClearColor;

	glColor4ub((rawClearColor&0xff), ((rawClearColor >> 8)&0xff), ((rawClearColor >> 16)&0xff), 0);

	return (retVal);
}
//------------------------------------------------------------------------------

int CDisplayer::restoreGraphicsColor()
{
	int retVal = graphicsColor;
	setGraphicsColor(graphicsColor);
	return (retVal);
}
//------------------------------------------------------------------------------

int CDisplayer::setGraphicsBuffer(int buffer)
{
	int retval = graphicsBuffer;
	graphicsBuffer = buffer;

	return (retval);
}
//------------------------------------------------------------------------------

void CDisplayer::flushGraphics()
{
	glFlush();
}
//------------------------------------------------------------------------------

void CDisplayer::movTo(int x, int y)
{
	curx = x;
	cury = y;
}
//------------------------------------------------------------------------------

// NOTE: some boards have systematic
// error of one pixel vertical. That
// accounts for "+ 1" in vert args
// to glVertex2i
//
void CDisplayer::linToNoFlush(int x, int y)
{
	glDrawBuffer(graphicsBuffer);
	glBegin(GL_LINES);
	glVertex2i(curx, cury + YERR);
	glVertex2i(x, y + YERR);
	glEnd();
	// I don't why this is here, removed JAM TTT
	// glBegin(GL_POINTS);
	// glVertex2i(x, y + YERR);
	// glEnd();
	glDrawBuffer(GL_BACK);

	curx = x;
	cury = y;
}
//------------------------------------------------------------------------------

// Begins a sequence of "lineTo" calls. Vble screenDC must be set,
// the pen selected, the raster op selected, and the dashing mode.
//
void CDisplayer::moveToClient(int x, int y)
{
	// initialize for recording tranitions
	jcnt = 0; // Jordan counter
	trnptr = trnsns; // fill pointer
	trndon = true; // assume success

	// do this for the clipping
	// The x-1 is a HACK to go with the removal of the point on lineto
	// This seems to work JAM TTT
	VCurX = x - 1;
	VCurY = y;

	// if vertex inside clpRect, move to it
	if ((tcur = tCode(VCurX, VCurY)) == 0)
	{
		movTo(VCurX, VCurY);
	}
}
//------------------------------------------------------------------------------

// Draws a line segment, clipped to the clpRect.
//
void CDisplayer::lineToClientNoFlush(int x, int y)
{
	VPreX = VCurX;
	VPreY = VCurY;
	tpre = tcur;

	VCurX = x;
	VCurY = y;
	tcur = tCode(VCurX, VCurY);

	// The Jordan counter determines the covering
	// of the (lft,top) of clpRect by the lasso.
	// Initialize it by counting the crossings of
	// the top edge >= clpRect.left
	if ((((tcur & TLFT) == 0) || ((tpre & TLFT) == 0)) && (((tcur & TTOP) + (tpre & TTOP)) == TTOP))
	{
		delxi = (double)(VCurX - VPreX);
		delyi = (double)(VCurY - VPreY);
		topyi = (double)(clpRect.top - VPreY);
		XCrs = (int)(VPreX + delxi * topyi / delyi);
		if (XCrs >= clpRect.left)
		{
			jcnt ^= 1;
		}
	}

	if ((tcur & tpre) == 0)
	{
		// not trivially rejected
		if (VCurY == VPreY)
		{
			// horz
			if (VCurX > VPreX)
			{
				// rightward
				if (tpre & TLFT)
				{
					// crossing on lft edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 3;
						trnptr->coord = -VCurY;
						trnptr->x = clpRect.left;
						trnptr->y = VCurY;
						trnptr->val = 1;
						trnptr++;
					}

					curx = clpRect.left;
					cury = VCurY;
				}

				if (tcur == 0)
				{
					// VCur in middle
					linToNoFlush(VCurX, VCurY);
				}
				else
				{
					// crossing on rgt edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{
						trnptr->edge = 1;
						trnptr->coord = VCurY;
						trnptr->x = clpRect.right;
						trnptr->y = VCurY;
						trnptr->val = 1;
						trnptr++;
					}

					linToNoFlush(clpRect.right, VCurY);
				}
			}
			else if (VCurX < VPreX)
			{
				// leftward
				if (tpre & TRGT)
				{
					// crossing on rgt edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 1;
						trnptr->coord = VCurY;
						trnptr->x = clpRect.right;
						trnptr->y = VCurY;
						trnptr->val = 1;
						trnptr++;
					}

					movTo(clpRect.right, VCurY);
				}
				if (tcur == 0)
				{
					// VCur in middle
					linToNoFlush(VCurX, VCurY);
				}
				else
				{
					// crossing on lft edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 3;
						trnptr->coord = -VCurY;
						trnptr->x = clpRect.left;
						trnptr->y = VCurY;
						trnptr->val = 1;
						trnptr++;
					}

					linToNoFlush(clpRect.left, VCurY);
				}
			}
		} // end horz

		else if (VCurX == VPreX)
		{
			// vert
			if (VCurY > VPreY)
			{
				// downward
				if (tpre & TTOP)
				{
					// crossing on top edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 0;
						trnptr->coord = VCurX;
						trnptr->x = VCurX;
						trnptr->y = clpRect.top;
						trnptr->val = 1;
						trnptr++;
					}

					movTo(VCurX, clpRect.top);
				}
				if (tcur == 0)
				{
					// VCur in middle
					linToNoFlush(VCurX, VCurY);
				}
				else
				{
					// crossing on bot edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 2;
						trnptr->coord = -VCurX;
						trnptr->x = VCurX;
						trnptr->y = clpRect.bottom;
						trnptr->val = 1;
						trnptr++;
					}

					linToNoFlush(VCurX, clpRect.bottom);
				}
			}
			else if (VCurY < VPreY)
			{
				// upward
				if (tpre & TBOT)
				{
					// crossing on bot edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 2;
						trnptr->coord = -VCurX;
						trnptr->x = VCurX;
						trnptr->y = clpRect.bottom;
						trnptr->val = 1;
						trnptr++;
					}

					movTo(VCurX, clpRect.bottom);
				}
				if (tcur == 0)
				{
					// VCur in middle
					linToNoFlush(VCurX, VCurY);
				}
				else
				{
					// crossing on top edge
					if ((trnptr - trnsns) >= MAXTRANS)
					{
						trndon = false;
					}
					else
					{

						trnptr->edge = 0;
						trnptr->coord = VCurX;
						trnptr->x = VCurX;
						trnptr->y = clpRect.top;
						trnptr->val = 1;
						trnptr++;
					}

					linToNoFlush(VCurX, clpRect.top);
				}
			}

		} // end vert

		else
		{
			// oblique edge
			if ((tpre | tcur) != 0)
			{
				// there's a crossing
				delxi = (double)(VCurX - VPreX);
				delyi = (double)(VCurY - VPreY);
				lftxi = (double)(clpRect.left - VPreX);
				rgtxi = (double)(clpRect.right - VPreX);
				botyi = (double)(clpRect.bottom - VPreY);
				topyi = (double)(clpRect.top - VPreY);
			}

			if (tpre != 0)
			{
				// pre is clipped
				if (tpre & TLFT)
				{
					// clip pre to lft edge
					YCrs = (int)(VPreY + delyi * lftxi / delxi);
					if (YCrs < clpRect.top)
					{
						tpre |= TTOP;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else if (YCrs > clpRect.bottom)
					{
						tpre |= TBOT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else
					{
						// crossing on lft edge
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{

							trnptr->edge = 3;
							trnptr->coord = -YCrs;
							trnptr->x = clpRect.left;
							trnptr->y = YCrs;
							trnptr->val = 1;
							trnptr++;
						}

						movTo(clpRect.left, YCrs);
						goto donpre;
					}
				}

				if (tpre & TBOT)
				{
					// clip pre to bot edge
					XCrs = (int)(VPreX + delxi * botyi / delyi);
					if (XCrs < clpRect.left)
					{
						tpre |= TLFT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else if (XCrs > clpRect.right)
					{
						tpre |= TRGT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else
					{
						// crossing on bot edge
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{

							trnptr->edge = 2;
							trnptr->coord = -XCrs;
							trnptr->x = XCrs;
							trnptr->y = clpRect.bottom;
							trnptr->val = 1;
							trnptr++;
						}

						movTo(XCrs, clpRect.bottom);
						goto donpre;
					}
				}

				if (tpre & TRGT)
				{
					// clip pre to rgt edge
					YCrs = (int)(VPreY + delyi * rgtxi / delxi);
					if (YCrs < clpRect.top)
					{
						tpre |= TTOP;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else if (YCrs > clpRect.bottom)
					{
						tpre |= TBOT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else
					{
						// crossing on rgt edge
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{

							trnptr->edge = 1;
							trnptr->coord = YCrs;
							trnptr->x = clpRect.right;
							trnptr->y = YCrs;
							trnptr->val = 1;
							trnptr++;
						}

						movTo(clpRect.right, YCrs);
						goto donpre;
					}
				}

				if (tpre & TTOP)
				{
					// clip pre to top edge
					XCrs = (int)(VPreX + delxi * topyi / delyi);
					if (XCrs < clpRect.left)
					{
						tpre |= TLFT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else if (XCrs > clpRect.right)
					{
						tpre |= TRGT;
						if (tpre & tcur)
						{
							goto doncur;
						}
					}
					else
					{
						// crossing on top edge
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{

							trnptr->edge = 0;
							trnptr->coord = XCrs;
							trnptr->x = XCrs;
							trnptr->y = clpRect.top;
							trnptr->val = 1;
							trnptr++;
						}

						movTo(XCrs, clpRect.top);
						goto donpre;
					}
				}
donpre: ;
			}

			if (tcur == 0)
			{
				// cur is not clipped
				linToNoFlush(VCurX, VCurY);
			}
			else
			{

				if (tcur & TLFT)
				{
					// clip cur to lft edge
					YCrs = (int)(VPreY + delyi * lftxi / delxi);
					if ((YCrs >= clpRect.top) && (YCrs <= clpRect.bottom))
					{
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{
							trnptr->edge = 3;
							trnptr->coord = -YCrs;
							trnptr->x = clpRect.left;
							trnptr->y = YCrs;
							trnptr->val = 1;
							trnptr++;
						}

						linToNoFlush(clpRect.left, YCrs);
						goto doncur;
					}
				}

				if (tcur & TBOT)
				{ // clip cur to bot edge
					XCrs = (int)(VPreX + delxi * botyi / delyi);
					if ((XCrs >= clpRect.left) && (XCrs <= clpRect.right))
					{
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{
							trnptr->edge = 2;
							trnptr->coord = -XCrs;
							trnptr->x = XCrs;
							trnptr->y = clpRect.bottom;
							trnptr->val = 1;
							trnptr++;
						}

						linToNoFlush(XCrs, clpRect.bottom);
						goto doncur;
					}
				}

				if (tcur & TRGT)
				{ // clip cur to rgt edge
					YCrs = (int)(VPreY + delyi * rgtxi / delxi);
					if ((YCrs >= clpRect.top) && (YCrs <= clpRect.bottom))
					{
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{
							trnptr->edge = 1;
							trnptr->coord = YCrs;
							trnptr->x = clpRect.right;
							trnptr->y = YCrs;
							trnptr->val = 1;
							trnptr++;
						}

						linToNoFlush(clpRect.right, YCrs);
						goto doncur;
					}
				}

				if (tcur & TTOP)
				{ // clip cur to top edge
					XCrs = (int)(VPreX + delxi * topyi / delyi);
					if ((XCrs >= clpRect.left) && (XCrs <= clpRect.right))
					{
						if ((trnptr - trnsns) >= MAXTRANS)
						{
							trndon = false;
						}
						else
						{

							trnptr->edge = 0;
							trnptr->coord = XCrs;
							trnptr->x = XCrs;
							trnptr->y = clpRect.top;
							trnptr->val = 1;
							trnptr++;
						}

						linToNoFlush(XCrs, clpRect.top);
						goto doncur;
					}
				}
doncur: ;
			}
		} // end oblique
	} // end not trivially rejected
}
//------------------------------------------------------------------------------

// draws one segment of a Bezier spline
void CDisplayer::splineToClientNoFlush(int x1, int y1, int x2, int y2, int x3, int y3, unsigned int dashpat)
{
	/////////////////////////////////////////////////////////////////////////////
	//
	glDrawBuffer(graphicsBuffer);
	glBegin(GL_POINTS);

	// glVertex2i(VCurX, VCurY + YERR);

	splineTo16NoFlush(VCurX << 16, VCurY << 16, x1 << 16, y1 << 16, x2 << 16, y2 << 16, x3 << 16, y3 << 16, dashpat, 0);

	// glVertex2i(x3, y3 + YERR);

	glEnd();
	glDrawBuffer(GL_BACK);

	VCurX = x3;
	VCurY = y3;
	return;
}
//------------------------------------------------------------------------------

// This method is used for drawing Bezier splines incrementally.
// If the spline is large, it divides it into two pieces and
// calls itself recursively. If the spline is small - less than
// a pixel in length, it uses Newton's method to correct the
// position of the initial point and draws it as a GL_POINT. This
// makes a nice smooth spline, with no penalty in speed vs the
// not-so-good-looking line segment draw (above).
//
int CDisplayer::splineTo16NoFlush(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, unsigned int dashpat,
		unsigned int index)
{
	int delx = x3 - x0;
	if (delx < 0)
	{
		delx = -delx;
	}

	int dely = y3 - y0;
	if (dely < 0)
	{
		dely = -dely;
	}

	if ((delx <= 0x10000) && (dely <= 0x10000))
	{
		if ((dashpat >> (index & 15)) & 1)
		{
			int iBx = (x1 - x0);
			int absBx = iBx;
			int xtarg = x0 & 0xffff0000;
			if (iBx < 0)
			{
				absBx = -absBx;
				xtarg += 0x00010000;
			}

			int iBy = (y1 - y0);
			int absBy = iBy;
			int ytarg = y0 & 0xffff0000;
			if (iBy < 0)
			{
				absBy = -absBy;
				ytarg += 0x00010000;
			}

			if (absBx > absBy)
			{
				// x ind, y dep
#if MTI_ASM_X86_INTEL
				if (iBx == 0)
				{
					glVertex2i(((x0 >> 15) + 1) >> 1, (((y0 >> 15) + 1) >> 1) + YERR);
				}
				else
				{
					y0 += (int)((double)(xtarg - x0) * (double)iBy / (double)iBx);
					glVertex2i(xtarg >> 16, (((y0 >> 15) + 1) >> 1) + YERR);
				}
#else
				_asm
				{

					cmp iBx, 0
					jne $S1
					mov eax, y0
					sar eax, 15
					inc eax sar eax, 1
					inc eax // YERR
					push
					eax mov eax, x0
					sar eax, 15
					inc eax
					sar eax, 1
					push eax jmp SHORT $S2

			$S1 : mov eax, xtarg
					sub eax, x0
					imul iBy
					idiv iBx
					add eax, y0
					sar eax, 15
					inc eax
					sar eax, 1
					inc eax // YERR
					push
					eax mov eax, xtarg
					sar eax, 15
					inc eax
					sar eax, 1
					push eax

			$S2 : call glVertex2i
				}
#endif
			}
			else
			{
				// y ind, x dep
#if MTI_ASM_X86_INTEL
				if (iBy == 0)
				{
					glVertex2i(((x0 >> 15) + 1) >> 1, (((y0 >> 15) + 1) >> 1) + YERR);
				}
				else
				{
					x0 += (int)((double)(ytarg - y0) * (double)iBx / (double)iBy);
					glVertex2i(((x0 >> 15) + 1) >> 1, (ytarg >> 16) + YERR);
				}
#else
				_asm
				{

					cmp iBy, 0 jne $S3 mov eax, y0 sar eax, 15 inc eax sar eax, 1 inc eax // YERR
							push eax mov eax, x0 sar eax, 15 inc eax sar eax, 1 push eax jmp SHORT $S4

							$S3 : mov eax, ytarg sar eax, 15 inc eax sar eax, 1 inc eax // YERR
							push eax mov eax, ytarg sub eax, y0 imul iBx idiv iBy add eax, x0 sar eax, 15 inc eax sar eax,
					1 push eax

							$S4 : call glVertex2i
				}
#endif
			}
		}

		return 1; // one pixel
	}

	int xi1 = (x0 + x1) >> 1;
	int yi1 = (y0 + y1) >> 1;
	int xi2 = (x1 + x2) >> 1;
	int yi2 = (y1 + y2) >> 1;
	int xi3 = (x2 + x3) >> 1;
	int yi3 = (y2 + y3) >> 1;

	int xj1 = (xi1 + xi2) >> 1;
	int yj1 = (yi1 + yi2) >> 1;
	int xj2 = (xi2 + xi3) >> 1;
	int yj2 = (yi2 + yi3) >> 1;

	int xk1 = (xj1 + xj2) >> 1;
	int yk1 = (yj1 + yj2) >> 1;

	int count1 = splineTo16NoFlush(x0, y0, xi1, yi1, xj1, yj1, xk1, yk1, dashpat, index);

	int count2 = splineTo16NoFlush(xk1, yk1, xj2, yj2, xi3, yi3, x3, y3, dashpat, index + count1);

	return (count1 + count2);
}
//------------------------------------------------------------------------------

// new OpenGL version. Because we now wipe off Beziers which
// overlap the imgRect by always redrawing the gray border,
// we don't need a gray background whenever a clip is loaded.
// So this method only does it when the clip is NULL.
void CDisplayer::displayBackground()
{
	if (imgFmt == NULL)
	{
		// clear entire visRect
		glDrawBuffer(GL_BACK);
		clearBackBuffer();
		SwapBuffers(screenDC);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::drawFilledRectangleNoFlush(const RECT &rect)
{
	glDrawBuffer(graphicsBuffer);
	glBegin(GL_QUADS);
	glVertex2i(rect.left, rect.top);
	glVertex2i(rect.right, rect.top);
	glVertex2i(rect.right, rect.bottom);
	glVertex2i(rect.left, rect.bottom);
	glEnd();
}
//------------------------------------------------------------------------------

void CDisplayer::drawMaskOverlayFrame(bool *mask)
{
	if (mask == nullptr)
	{
		return;
	}

	// all graphics will be clipped to the IMAGE rectangle
	clipToImageRectangle();

	// graphics in highlight color
	int col = colorPalette[highlightColor];
	glColor4ub((col&0xff), ((col >> 8)&0xff), ((col >> 16)&0xff), 0);
	glDisable(GL_LINE_STIPPLE);

	// set target to front or back
	glDrawBuffer(graphicsBuffer);

	for (int y = 0; y < frameHeight; ++y)
	{
		int rowStartOffset = y * frameWidth;
		for (int x1 = 0; x1 < frameWidth; ++x1)
		{
			// Mask true = pixels are the same.
			if (mask[rowStartOffset + x1])
			{
				continue;
			}

			int x2 = x1 + 1;
			for (; x2 <= frameWidth; ++x2)
			{
				// Mask true = pixels are the same.
				if (mask[rowStartOffset + x2])
				{
					// Found end of run of differing pixels.
					break;
				}
			}

			RECT drawRect;
			drawRect.left = dscaleXFrameToClient(x1);
			drawRect.top = dscaleYFrameToClient(y);
			drawRect.right = dscaleXFrameToClient(x2);
			drawRect.bottom = dscaleYFrameToClient(y + 1);

			moveToClient(drawRect.left, drawRect.top);
			if (rectangleAND(&drawRect, &clpRect))
			{
				// visible
				glBegin(GL_QUADS);
				glVertex2i(drawRect.left, drawRect.top);
				glVertex2i(drawRect.right, drawRect.top);
				glVertex2i(drawRect.right, drawRect.bottom);
				glVertex2i(drawRect.left, drawRect.bottom);
				glEnd();
			}

			x1 = x2;
		}
	}

	// don't forget to flush
	flushGraphics();

	// reset target to back
	glDrawBuffer(GL_BACK);

	// restore graphics color
	restoreGraphicsColor();

	// revert to clipping to VISIBLE rectangle
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

// KT-060227 Bugzilla 2400. Reworked to do the draw with only a
// single call to flushGraphics at the end. Speed way, way up. See
// changes to displayFrame, where the call takes place.
//
void CDisplayer::drawPixelRegionListFrame(CPixelRegionList *pxlrgnlst)
{
	RECT frmrct;

	POINT padj;
	RECT radj;
	POINT *lasso;
	BEZIER_POINT *bezier;

	// all graphics will be clipped to the IMAGE rectangle
	clipToImageRectangle();

	// graphics in highlight color
	int col = colorPalette[highlightColor];
	glColor4ub((col&0xff), ((col >> 8)&0xff), ((col >> 16)&0xff), 0);
	if (highlightColor & 1)
	{
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0xf8f8);
	}
	else
	{
		glDisable(GL_LINE_STIPPLE);
	}

	// set target to front or back
	glDrawBuffer(graphicsBuffer);

	// loop through the elements of the list and draw them
	for (int i = 0; i < pxlrgnlst->GetEntryCount(); i++)
	{

		CPixelRegion *rgn = pxlrgnlst->GetEntry(i);

		switch (rgn->GetType())
		{
			case PIXEL_REGION_TYPE_POINT: // A single point

				// we adjust the coordinates so that the graphical rectangle
				// covers the corresponding rendered one
				padj = (static_cast<CPointPixelRegion*>(rgn))->GetPoint();

#ifdef OLD
				radj.left = padj.x & 0xfffffffe;
				radj.top = padj.y;
				radj.right = radj.left + 2;
				radj.bottom = padj.y + 1;
#else
				radj.left = padj.x;
				radj.top = padj.y;
				radj.right = padj.x + 1;
				radj.bottom = padj.y + 1;
#endif
				frmrct.left = dscaleXFrameToClient(radj.left);
				frmrct.top = dscaleYFrameToClient(radj.top);
				frmrct.right = dscaleXFrameToClient(radj.right) + 1;
				frmrct.bottom = dscaleYFrameToClient(radj.bottom);

				// important to keep this for trnsn ptr
				moveToClient(frmrct.left, frmrct.top);
				if (rectangleAND(&frmrct, &clpRect))
				{ // visible?
					glBegin(GL_QUADS);
					glVertex2i(frmrct.left, frmrct.top);
					glVertex2i(frmrct.right, frmrct.top);
					glVertex2i(frmrct.right, frmrct.bottom);
					glVertex2i(frmrct.left, frmrct.bottom);
					glEnd();
				}

				break;

			case PIXEL_REGION_TYPE_RECT: // Rectangle

				radj = (static_cast<CRectPixelRegion*>(rgn))->GetRect();

#ifdef OLD
				radj.left = (radj.left & 0xfffffffe);
				radj.top = radj.top;
				radj.right = (radj.right & 0xfffffffe) + 1;
				radj.bottom = radj.bottom + 1;
#else
				radj.right = radj.right + 1;
				radj.bottom = radj.bottom + 1;
#endif
				frmrct.left = dscaleXFrameToClient(radj.left);
				frmrct.top = dscaleYFrameToClient(radj.top);
				frmrct.right = dscaleXFrameToClient(radj.right) + 1;
				frmrct.bottom = dscaleYFrameToClient(radj.bottom);

				moveToClient(frmrct.left, frmrct.top);
				lineToClientNoFlush(frmrct.right, frmrct.top);
				lineToClientNoFlush(frmrct.right, frmrct.bottom);
				lineToClientNoFlush(frmrct.left, frmrct.bottom);
				lineToClientNoFlush(frmrct.left, frmrct.top);

				break;

			case PIXEL_REGION_TYPE_MASK: // Mask

				radj = (static_cast<CMaskPixelRegion*>(rgn))->GetRect();

#ifdef OLD
				radj.left = (radj.left & 0xfffffffe);
				radj.top = radj.top;
				radj.right = (radj.right & 0xfffffffe) + 1;
				radj.bottom = radj.bottom + 1;
#else
				radj.right = radj.right + 1;
				radj.bottom = radj.bottom + 1;
#endif
				frmrct.left = dscaleXFrameToClient(radj.left);
				frmrct.top = dscaleYFrameToClient(radj.top);
				frmrct.right = dscaleXFrameToClient(radj.right) + 1;
				frmrct.bottom = dscaleYFrameToClient(radj.bottom);

				moveToClient(frmrct.left, frmrct.top);
				lineToClientNoFlush(frmrct.right, frmrct.top);
				lineToClientNoFlush(frmrct.right, frmrct.bottom);
				lineToClientNoFlush(frmrct.left, frmrct.bottom);
				lineToClientNoFlush(frmrct.left, frmrct.top);

				break;

			case PIXEL_REGION_TYPE_LASSO: // Closed lasso polygon

				lasso = (static_cast<CLassoPixelRegion*>(rgn))->GetLasso();

				moveToFrame(lasso[0].x, lasso[0].y);
				for (int i = 1; ; i++)
				{
					lineToFrameNoFlush(lasso[i].x, lasso[i].y); // flush at exit
					if ((i > 2) && (lasso[i].x == lasso[0].x) && (lasso[i].y == lasso[0].y))
					{
						break;
					}
				}

				break;

			case PIXEL_REGION_TYPE_BEZIER: // Closed bezier polygon

				bezier = (static_cast<CBezierPixelRegion*>(rgn))->GetBezier();

				moveToFrame(bezier[0].x, bezier[0].y);
				for (int i = 1; ; i++)
				{
					splineToFrameNoFlush(bezier[i - 1].xctrlnxt, bezier[i - 1].yctrlnxt, bezier[i].xctrlpre, bezier[i].yctrlpre,
							bezier[i].x, bezier[i].y);
					if ((i > 1) && (bezier[i].xctrlpre == bezier[0].xctrlpre) && (bezier[i].yctrlpre == bezier[0].yctrlpre) &&
							(bezier[i].x == bezier[0].x) && (bezier[i].y == bezier[0].y) &&
							(bezier[i].xctrlnxt == bezier[0].xctrlnxt) && (bezier[i].yctrlnxt == bezier[0].yctrlnxt))
					{
						break;
					}
				}

				break;

			default:
				break;
		}

		// since each pixel region's graphics are
		// drawn with "lineTo" instructions, we can
		// draw the missing lines around the
		// edge of the clip rectangle by calling
		// "drawTransitionsClient". Now the user
		// will know if he is inside a rect/lasso/
		// bezier because the edges of the image
		// rectangle will be highlighted (in blue)
		drawTransitionsClientNoFlush();
	}

	// don't forget to flush
	flushGraphics();

	// reset target to back
	glDrawBuffer(GL_BACK);

	// restore graphics color
	restoreGraphicsColor();

	// revert to clipping to VISIBLE rectangle
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

// These methods are used by the Bezier editor to display
// Bezier splines.
void CDisplayer::drawFilledBoxClientNoFlush(int x, int y)
{
	RECT box;
	box.left = x - BOX_SIZE; // + 1
	box.top = y - BOX_SIZE; // + 1
	box.right = x + BOX_SIZE + 1; // + 2
	box.bottom = y + BOX_SIZE + 1; // + 2

	// clip to the client area and if
	// it's good, draw it filled
	if (rectangleAND(&box, &clpRect))
	{
		glBegin(GL_QUADS);
		glVertex2i(box.left, box.top);
		glVertex2i(box.right, box.top);
		glVertex2i(box.right, box.bottom);
		glVertex2i(box.left, box.bottom);
		glEnd();
	}
}

void CDisplayer::drawUnfilledBoxClientNoFlush(int x, int y)
{
	RECT box;
	box.left = x - BOX_SIZE; // + 1;
	box.top = y - BOX_SIZE; // + 1;
	box.right = x + BOX_SIZE; // + 1;
	box.bottom = y + BOX_SIZE; // + 1;

	// draw box as a (clipped) polygon
	moveToClient(box.left, box.top);
	lineToClientNoFlush(box.right, box.top);
	moveToClient(box.right, box.top);
	lineToClientNoFlush(box.right, box.bottom);
	lineToClientNoFlush(box.left, box.bottom);
	lineToClientNoFlush(box.left, box.top);

	// clear the trnsn ptr
	moveToClient(box.left, box.top);
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  TRACKING BOX //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CDisplayer::computeAlphaValueForTrackingBoxLine(double dpos)
{
	return 255 - ((int) floor((dpos - floor(dpos)) * 256.0));
}
//------------------------------------------------------------------------------

void CDisplayer::drawLineForTrackingBoxRectangleNoFlush(double dx1, double dy1, double dx2, double dy2, int col)
{
	int cR = col & 0xff;
	int cG = (col >> 8) & 0xff;
	int cB = (col >> 16) & 0xff;
	int cA = 255;
	int xinc = (dx1 == dx2) ? 1 : 0;
	int yinc = (dy1 == dy2) ? 1 : 0;

	if (xinc)
	{
		cA = computeAlphaValueForTrackingBoxLine(dx1);
	}
	else if (yinc)
	{
		cA = computeAlphaValueForTrackingBoxLine(dy1);
	}

	glColor4ub(cR, cG, cB, cA);
	moveToClient(floor(dx1), floor(dy1));
	lineToClientNoFlush(floor(dx2), floor(dy2));

	glColor4ub(cR, cG, cB, 255 - cA);
	moveToClient(floor(dx1 + xinc), floor(dy1 + yinc));
	lineToClientNoFlush(floor(dx2 + xinc), floor(dy2 + yinc));
}
//------------------------------------------------------------------------------

void CDisplayer::drawTrackingBoxRectangleWithCrosshairs(const DRECT &drct, double chlenh, double chlenv, int col,
		bool dsh)
{
	clipToImageRectangle();

	// Set up for anti-aliased line drawing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	if (dsh)
	{
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0xf8f8);
	}
	else
	{
		glDisable(GL_LINE_STIPPLE);
	}

	// box
	drawLineForTrackingBoxRectangleNoFlush(drct.left, drct.top, drct.right, drct.top, col);
	drawLineForTrackingBoxRectangleNoFlush(drct.right, drct.top, drct.right, drct.bottom, col);
	drawLineForTrackingBoxRectangleNoFlush(drct.right, drct.bottom, drct.left, drct.bottom, col);
	drawLineForTrackingBoxRectangleNoFlush(drct.left, drct.bottom, drct.left, drct.top, col);
	// crosshairs
	if (chlenh > 0)
	{
		double dceny = (drct.bottom + drct.top) / 2;
		drawLineForTrackingBoxRectangleNoFlush(drct.left + 1, dceny, drct.left + chlenh, dceny, col);
		drawLineForTrackingBoxRectangleNoFlush(drct.right - 1, dceny, drct.right - chlenh, dceny, col);
	}
	if (chlenv > 0)
	{
		double dcenx = (drct.right + drct.left) / 2;
		drawLineForTrackingBoxRectangleNoFlush(dcenx, drct.top + 1, dcenx, drct.top + chlenv, col);
		drawLineForTrackingBoxRectangleNoFlush(dcenx, drct.bottom - 1, dcenx, drct.bottom - chlenv, col);
	}

	glFlush();

	// clear the trnsn ptr      <-- haha... wtf is this????
	moveToClient(drct.left, drct.top);

	// reset gl state to defaults
	glDisable(GL_BLEND);
	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_LINE_STIPPLE);

	// restore default clipping
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

void CDisplayer::drawTrackingBoxNoFlush(double centerX, double centerY, double innerRadiusX, double innerRadiusY,
		double outerRadiusX, double outerRadiusY, bool showCrosshairs)
{
	const double dirdx = innerRadiusX * XScaleFrameToClient;
	const double dirdy = innerRadiusY * YScaleFrameToClient;
	const double dordx = outerRadiusX * XScaleFrameToClient;
	const double dordy = outerRadiusY * YScaleFrameToClient;
	const double dcenx = ddscaleXFrameToClient(centerX);
	const double dceny = ddscaleYFrameToClient(centerY);

	DRECT dibox, dobox, sibox, sobox;
	dibox.left = dcenx - dirdx;
	dibox.top = dceny - dirdy;
	dibox.right = dcenx + dirdx;
	dibox.bottom = dceny + dirdy;
	dobox.left = dcenx - dordx;
	dobox.top = dceny - dordy;
	dobox.right = dcenx + dordx;
	dobox.bottom = dceny + dordy;
	sibox.left = dibox.left + 1;
	sibox.top = dibox.top + 1;
	sibox.right = dibox.right + 1;
	sibox.bottom = dibox.bottom + 1;
	sobox.left = dobox.left + 1;
	sobox.top = dobox.top + 1;
	sobox.right = dobox.right + 1;
	sobox.bottom = dobox.bottom + 1;

	double chlenh, chlenv;

	chlenh = showCrosshairs ? (dirdx / 2) : 0.0;
	chlenv = showCrosshairs ? (dirdy / 2) : 0.0;
	// inner rect has solid lines
	drawTrackingBoxRectangleWithCrosshairs(sibox, chlenh, chlenv, 0, false);
	drawTrackingBoxRectangleWithCrosshairs(dibox, chlenh, chlenv, colorPalette[graphicsColor], false);

	if (outerRadiusX != 0 && outerRadiusY != 0)
	{
		chlenh = showCrosshairs ? (dordx - dirdx - 2) : 0.0;
		chlenv = showCrosshairs ? (dordy - dirdy - 2) : 0.0;
		// outer rect has dashed lines
		drawTrackingBoxRectangleWithCrosshairs(sobox, chlenh, chlenv, 0, true);
		drawTrackingBoxRectangleWithCrosshairs(dobox, chlenh, chlenv, colorPalette[graphicsColor], true);
	}

	// no flush!
}
