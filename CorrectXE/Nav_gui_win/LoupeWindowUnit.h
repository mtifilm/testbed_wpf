//---------------------------------------------------------------------------

#ifndef LoupeWindowUnitH
#define LoupeWindowUnitH
//---------------------------------------------------------------------------
#include "machine.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

class TmainWindow;
class CColorSpaceConvert;
class CImageFormat;
class CMagnifier;
//---------------------------------------------------------------------------

class TLoupeWindow : public TForm
{
__published:	// IDE-managed Components
    TPanel *LoupeImagePanel;
    TImage *StaticLoupeImage;
    TTimer *ParentCheckTimer;
    void __fastcall FormActivate(TObject *Sender);
    void __fastcall ParentCheckTimerTimer(TObject *Sender);

private:	// User declarations
    TImage *LoupeImage;
    TmainWindow *mainWindow;
    CMagnifier *magnifier;
    int currentSrcSize;
    float currentMagFactor;
    CColorSpaceConvert *toRGBColorConverter;
    CImageFormat *currentClipImageFormat;
    CImageFormat *internalImageFormat;
    int oldMainWindowLeft;
    int oldMainWindowTop;
    bool showCrosshairs;

    void imageMagnifierRefresh(void *mag);

public:		// User declarations
    __fastcall TLoupeWindow(TComponent* Owner);
    __fastcall ~TLoupeWindow();

    void init(TmainWindow *newMainWindow);
    void activate(int sideSize, int magFactor);
    void deactivate(void);
    void toggleCrosshairs();
    CMagnifier* getMagnifier();

    static void imageMagnifierRefreshCallback(void *obj, void *mag);

};
//---------------------------------------------------------------------------
extern PACKAGE TLoupeWindow *LoupeWindow;
//---------------------------------------------------------------------------
#endif
