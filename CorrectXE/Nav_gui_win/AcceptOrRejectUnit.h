//---------------------------------------------------------------------------

#ifndef AcceptOrRejectUnitH
#define AcceptOrRejectUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include "TimelineFrameUnit.h"
//---------------------------------------------------------------------------
class TAcceptOrRejectForm : public TForm
{
__published:	// IDE-managed Components
        TButton *AcceptButton;
        TButton *RejectButton;
        TButton *CancelButton;
    TImage *Image1;
        TLabel *Label1;
        TMemo *Memo1;
        TCheckBox *TurnOnAutoAcceptCheckBox;
    void __fastcall FormKeyPress(TObject *Sender, char &Key);
        void __fastcall RejectButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall AcceptButtonClick(TObject *Sender);
        void __fastcall CancelButtonClick(TObject *Sender);
        void __fastcall FormHide(TObject *Sender);
private:	// User declarations
    bool turnOnAutoAcceptMode;

public:		// User declarations
    __fastcall TAcceptOrRejectForm(TComponent* Owner);
    
    void ShowTurnOnAutoAcceptCheckbox(bool flag);
    bool GetTurnOnAutoAcceptChecked();
};
//---------------------------------------------------------------------------
extern PACKAGE TAcceptOrRejectForm *AcceptOrRejectForm;
//---------------------------------------------------------------------------
#endif
