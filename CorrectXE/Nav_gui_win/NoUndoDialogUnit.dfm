object NoUndoForm: TNoUndoForm
  Left = 1719
  Top = 244
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'No Undo!'
  ClientHeight = 185
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    384
    185)
  PixelsPerInch = 96
  TextHeight = 19
  object BackPanel: TPanel
    Left = 0
    Top = 0
    Width = 384
    Height = 185
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      384
      185)
    object Image1: TImage
      Left = 168
      Top = 20
      Width = 41
      Height = 41
      Picture.Data = {
        07544269746D6170F6120000424DF61200000000000036000000280000002800
        0000280000000100180000000000C0120000C40E0000C40E0000000000000000
        0000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFF
        FFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF
        FFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFF
        FFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFF
        FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFF
        FFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFF
        FFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFF
        FFFFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000
        FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFF
        FFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000
        FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000FFFFFFFF0000FF00
        00FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFC0
        C0C0C0C0C0FFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFF
        FFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF00
        00FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFF
        FFFFFF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000
        FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF00
        00FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000
        FF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFF
        FFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000
        FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000FF0000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFC0
        C0C0C0C0C0FFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF00
        00FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF
        FFFFFFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFC0
        C0C0C0C0C0FFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF
        FFFFFF0000FF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF0000
        FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFF00
        00FF0000FFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF0000FF0000FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFF0000FF0000
        FF0000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000
        FFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFF
        FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFF
        FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF
        FFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFFFF
        FFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        FFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFC0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0}
      Transparent = True
    end
    object Label2: TLabel
      Left = 21
      Top = 76
      Width = 342
      Height = 19
      Caption = 'WARNING: This tool does not preserve history -'
    end
    object Label3: TLabel
      Left = 17
      Top = 96
      Width = 350
      Height = 19
      Caption = 'You will not be able to undo the operation!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel1: TPanel
      Left = 0
      Top = 132
      Width = 384
      Height = 3
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
    end
    object Button1: TButton
      Left = 108
      Top = 148
      Width = 75
      Height = 21
      Anchors = [akLeft, akBottom]
      Caption = 'Continue'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
    end
    object Button2: TButton
      Left = 196
      Top = 148
      Width = 75
      Height = 21
      Anchors = [akLeft, akBottom]
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 2
    end
  end
end
