//---------------------------------------------------------------------------

#ifndef SplashScreenUnitH
#define SplashScreenUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include "ColorLabel.h"
//---------------------------------------------------------------------------
class TSplashScreen : public TForm
{
__published:	// IDE-managed Components
    TImage *SplashImage;
    TTimer *SplashTimer;
	TLabel *infoLabel;
	TColorLabel *VersionLabel;
    void __fastcall SplashTimerTimer(TObject *Sender);
private:	// User declarations
    bool bDoSelfDestruct;
    int refreshCounter;
public:		// User declarations
    __fastcall TSplashScreen(TComponent* Owner);

    void StartTimer(float secs = -1., bool selfDestruct = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TSplashScreen *SplashScreen;
//---------------------------------------------------------------------------
#endif
