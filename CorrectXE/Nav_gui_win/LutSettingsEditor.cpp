//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LutSettingsEditor.h"
#include "MainWindowUnit.h"
#include "Displayer.h"
#include "Player.h"
#include "LUTParser.h"
#include "LUTNameUnit.h"
#include "MTIKeyDef.h"
#include "ShowModalDialog.h"
#include "MTIstringstream.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "cspin"
#pragma link "SpinEditFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma resource "*.dfm"
TLutSettingsEditor *LutSettingsEditor;

#define LUTINIFILE      "$(CPMP_USER_DIR)LUTList.ini"
#define LUTFILELABEL    "LUT file:"
#define LUTDIRECTORY    "$(CPMP_USER_DIR)\\luts"

// Constants for LUT selection combo box.
constexpr auto ClipLutIndex = 0;
constexpr auto ProjectLutIndex = 1;
constexpr auto NumberOfBuiltInLuts = ProjectLutIndex + 1;
constexpr auto MaxNumberOfUserLuts = 10;

//---------------------------------------------------------------------------
__fastcall TLutSettingsEditor::TLutSettingsEditor(TComponent* Owner)
        : TMTIForm(Owner)
{
   SourceRangeMinIsLocked = true;
   SourceRangeMaxIsLocked = true;
   SourceRangeMinLockTendency = true;
   SourceRangeMaxLockTendency = true;
   DisplayRangeMinIsLocked = true;
   DisplayRangeMaxIsLocked = true;
   DisplayRangeMinLockTendency = true;
   DisplayRangeMaxLockTendency = true;

#if 0
	oldSpinEditValueIsValid = false;
   oldSpinEditValue = 0;
   oldSpinEditHandle = NULL;
   bEditing = false;
   cpOriginalName[0] = '\0';;
   iEditIndex = -1;
#endif

   InhibitPreviewing = false;
   InhibitCallbacks = false;

   CurrentClipDefaultColorSpace = IF_COLOR_SPACE_INVALID;
   CurrentClipBitsPerComponent = -1;
   CurrentClipIsRGB = false;
   CurrentClipIsYUV = false;
   CurrentClipIsMono = false;
   CurrentClipIsEXR = false;

   LutSettingsManager = NULL;
   CurrentLutSelection = LUTSEL_NONE;
   Displayer = NULL;
}
//---------------------------------------------------------------------------

__fastcall TLutSettingsEditor::~TLutSettingsEditor()
{
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::SetLutSettingsManager(CLutSettingsManager *newManager)
{
   LutSettingsManager = newManager;
};
//---------------------------------------------------------------------------

void TLutSettingsEditor::SetDisplayer(CDisplayer *newDisplayer)
{
   Displayer = newDisplayer;
};
//---------------------------------------------------------------------------

void TLutSettingsEditor::SetDefaultGammaTimes10(int newDefaultGamma)
{
   DefaultGammaTimes10 = newDefaultGamma;
};
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::FormCreate(TObject *Sender)
{
   InitLutSelectionGroup();
   InitSourceColorSpaceGroup();
   InitSourceRangeGroup();
   InitDisplayRangeGroup();
   InitDisplayGammaGroup();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::FormShow(TObject *Sender)
{
   GetCurrentSettings();

	// Conform defaults to match the current clip.
	GrabCurrentClipCharacteristics();
   ColorSpaceDefaultOrCustomRadioButtonClick(NULL);

   RefreshGuiFromCurrentSettings();
	TakeSnapshot();

	TMTIForm::FormShow(Sender);
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- LUT SETTINGS MANAGER INTERFACE --------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::SelectLut(ELutSelection newLut, const string *userLutName)
{
   LutSettingsManager->SelectLut(newLut, userLutName);
   RefreshGuiFromCurrentSettings();

   // Take a new snapshot - at this point we don't want to undo previous stuff
   TakeSnapshot();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::GetCurrentSettings()
{
   if (LutSettingsManager == NULL)
      return;

   // Get fresh LUT settings
   CurrentLutSelection = LutSettingsManager->GetLutSelection(CurrentUserLutName);
   if (CurrentLutSelection == LUTSEL_PROJECT)
   {
      CurrentLutSettings = LutSettingsManager->GetProjectLutSettings();
   }
   else if (CurrentLutSelection == LUTSEL_USER)
   {
      CurrentLutSettings = LutSettingsManager->GetUserLutSettings(CurrentUserLutName);
   }
   else // Default is "Clip"
   {
      CurrentLutSettings = LutSettingsManager->GetClipLutSettings();
   }

   // Get fresh set of user LUT names
   LutSettingsManager->GetUserLutSettingsNames(CurrentUserLutNameList);
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshGuiFromCurrentSettings()
{
   MTIassert(!InhibitCallbacks);
   MTIassert(!InhibitPreviewing);

   GetCurrentSettings();

   // set hack flags to prevent changes until all refreshes are done
   InhibitPreviewing = true;

   // Tell each group to refresh itself
   RefreshLutSelectionGroup();
   RefreshSourceColorSpaceGroup();
   RefreshSourceRangeGroup();
   RefreshDisplayRangeGroup();
   RefreshDisplayGammaGroup();

   // release hack flag
   InhibitPreviewing = false;

   // OK, apply the changes now
   ApplyCurrentSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ApplyCurrentSettings()
{
   if (LutSettingsManager == NULL)
      return;

   // Pass the latest and greatest settings to the LUT Settings Manager
   // And tell it to install them

   if (CurrentLutSelection == LUTSEL_PROJECT)
   {
      LutSettingsManager->SetProjectLutSettings(CurrentLutSettings);
      if (!InhibitPreviewing)
         LutSettingsManager->ApplyProjectLutSettings();
   }
   else if (CurrentLutSelection == LUTSEL_USER)
   {
      LutSettingsManager->SetUserLutSettings(CurrentUserLutName, CurrentLutSettings);
      if (!InhibitPreviewing)
         LutSettingsManager->ApplyUserLutSettings(CurrentUserLutName);
   }
   else // Default is "Clip"
   {
      LutSettingsManager->SetClipLutSettings(CurrentLutSettings);
      if (!InhibitPreviewing)
         LutSettingsManager->ApplyClipLutSettings();
   }
}
//---------------------------------------------------------------------------

// Before first preview, we want to save the settings in case user 'Cancel's
void TLutSettingsEditor::TakeSnapshot()
{
   if (LutSettingsManager == NULL)
      return;

   LutSettingsManager->TakeSnapshot();
}
//---------------------------------------------------------------------------

// Cancel preview - revert to old values
void TLutSettingsEditor::RevertToSnapshotSettings()
{
   if (LutSettingsManager == NULL)
      return;

   LutSettingsManager->RevertToSnapshot();

   RefreshGuiFromCurrentSettings();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- DISPLAYER INTERFACE -------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::HandleSettingChangeCallback()
{
}
//---------------------------------------------------------------------------


void TLutSettingsEditor::GrabCurrentClipCharacteristics()
{
   if (Displayer == NULL)
      return;

   // This Displayer call gives us the current clip's default color space
   CurrentClipDefaultColorSpace = Displayer->getFormatColorSpace();
   CurrentClipBitsPerComponent = Displayer->getBitsPerComponent();

   CurrentClipIsRGB = false;
   CurrentClipIsYUV = false;
   CurrentClipIsMono = false;
   CurrentClipIsEXR = false;
   switch (Displayer->getPixelComponents())
   {
      case IF_PIXEL_COMPONENTS_RGB:
      case IF_PIXEL_COMPONENTS_RGBA:
      case IF_PIXEL_COMPONENTS_BGR:
      case IF_PIXEL_COMPONENTS_BGRA:
         if (CurrentClipDefaultColorSpace == IF_COLOR_SPACE_EXR)
         {
            CurrentClipIsEXR = true;
         }
         else
         {
            CurrentClipIsRGB = true;
         }
         break;

      case IF_PIXEL_COMPONENTS_YUV422:
      case IF_PIXEL_COMPONENTS_YUV4224:
      case IF_PIXEL_COMPONENTS_YUV444:
         CurrentClipIsYUV = true;
         break;

//      case IF_PIXEL_COMPONENTS_LUMINANCE:
      case IF_PIXEL_COMPONENTS_Y:
      case IF_PIXEL_COMPONENTS_YYY:
         CurrentClipIsMono = true;
         break;

      default:
         break;
   }
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- LUT SELECTION GROUP -------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::InitLutSelectionGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   // not implemented yet

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshLutSelectionGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   PopulateLutSelectionComboBox();
   PopulateShortcutComboBox();
   ConformLutSelectionGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformLutSelectionGroup()
{
   MTIassert(InhibitCallbacks);

   NewUserLutButton->Enabled = true;
   CopyToClipButton->Enabled = true;
   CopyToProjectButton->Enabled = true;

   if (CurrentLutSelection == LUTSEL_USER)
   {
      DeleteUserLutButton->Enabled = true;
      ShortcutLabel->Enabled = true;
      ShortcutComboBox->Enabled = true;
      ShortcutComboBox->Color = clWindow;

      string selectedUserLutName;
      LutSettingsManager->GetLutSelection(selectedUserLutName);
      // for some reason  'stringstream << char' doesn't work
      char stupidCString[2];
      stupidCString[0] = LutSettingsManager->GetUserLutShortcut(selectedUserLutName);
      stupidCString[1] = '\0';
      ShortcutComboBox->Text = stupidCString;
   }
   else
   {
      DeleteUserLutButton->Enabled = false;
      ShortcutLabel->Enabled = false;
      ShortcutComboBox->Enabled = false;
      ShortcutComboBox->Color = clBtnFace;

      if (CurrentLutSelection == LUTSEL_CLIP)
      {
         CopyToClipButton->Enabled = false;
         ShortcutComboBox->Text = "C";
      }

      if (CurrentLutSelection == LUTSEL_PROJECT)
      {
         CopyToProjectButton->Enabled = false;
         ShortcutComboBox->Text = "P";
      }
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::PopulateLutSelectionComboBox()
{
	// Always start fresh.
	LutNameComboBox->Clear();
	LutNameComboBox->DropDownCount = NumberOfBuiltInLuts + MaxNumberOfUserLuts; // "project", "clip", and N user LUTs

	// Populate the combo box items list.
	LutNameComboBox->AddItem("[Clip LUT]", NULL);
	LutNameComboBox->AddItem("[Project LUT]", NULL);
	for (auto& lutName : CurrentUserLutNameList)
	{
		LutNameComboBox->AddItem(lutName.c_str(), NULL);
	}

	// Set the selected item.
	auto selectedItemIndex = -1;
	switch (CurrentLutSelection)
	{
		case LUTSEL_CLIP:
		default:
			selectedItemIndex = ClipLutIndex;
			break;

		case LUTSEL_PROJECT:
			selectedItemIndex = ProjectLutIndex;
			break;

		case LUTSEL_USER:
		{
			auto currentItemIndex = NumberOfBuiltInLuts;
			for (auto& name : CurrentUserLutNameList)
			{
				if (name == CurrentUserLutName)
				{
					selectedItemIndex = currentItemIndex;
				}

				++currentItemIndex;
			}

			break;
		}
	}

	LutNameComboBox->ItemIndex = (selectedItemIndex < 0) ? ClipLutIndex : selectedItemIndex;
}
//---------------------------------------------------------------------------


void __fastcall TLutSettingsEditor::LutNameComboBoxSelect(TObject *Sender)
{
	if (InhibitCallbacks || LutSettingsManager == NULL)
	{
		return;
	}

	int selectedItemIndex = (LutNameComboBox->ItemIndex < 0) ? ClipLutIndex : LutNameComboBox->ItemIndex;
	if (selectedItemIndex < NumberOfBuiltInLuts)
	{
		// Set the selection to Clip LUT or Project LUT, defaulting to the Clip LUT if not set.
		SelectLut((selectedItemIndex == ProjectLutIndex) ? LUTSEL_PROJECT : LUTSEL_CLIP);
	}
	else
	{
		string userLutName = StringToStdString(LutNameComboBox->Items->Strings[selectedItemIndex]);
		SelectLut(LUTSEL_USER, &userLutName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::CopyToProjectButtonClick(TObject *Sender)
{
   // Short circuit if [PROJECT] is selected
	if (LutNameComboBox->ItemIndex == 1)      // QQQ MANIFEST CONSTANT
		return;

   if (LutSettingsManager == NULL)
      return;

   LutSettingsManager->SetProjectLutSettings(CurrentLutSettings);
   SelectLut(LUTSEL_PROJECT);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::CopyToClipButtonClick(TObject *Sender)
{
   // Short circuit if [CLIP] is selected
	if (LutNameComboBox->ItemIndex == 0)      // QQQ MANIFEST CONSTANT
      return;

   if (LutSettingsManager == NULL)
      return;

   LutSettingsManager->SetClipLutSettings(CurrentLutSettings);
   SelectLut(LUTSEL_CLIP);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::NewUserLutButtonClick(TObject *Sender)
{
   if (LutSettingsManager == NULL)
      return;

   string newLutName;
   size_t userLutCount = CurrentUserLutNameList.size();

   for (int i = 1; newLutName.empty(); ++i)
   {
      unsigned j;
      MTIostringstream os;
      os << "Custom LUT " << std::setw(2) << std::setfill('0') << i;
      for (j = 0; j < userLutCount; ++j)
      {
         if (CurrentUserLutNameList[j] == os.str())
             break;
      }
	  if (j == userLutCount)
         newLutName = os.str();
   }

   if (LUTNameForm == NULL)
      LUTNameForm = new TLUTNameForm(this);

   LUTNameForm->SetDefaultName(newLutName);
   if (ShowModalDialog(LUTNameForm) == mrOk)
   {
      string baseLutName = LUTNameForm->GetName();
      newLutName = baseLutName;
      int copyNumber = 1;

      bool nameIsUnique = false;
      while (!nameIsUnique)
      {
         size_t i;
         for (i = 0; i < userLutCount; ++i)
         {
            if (newLutName == CurrentUserLutNameList[i])
               break;
         }
         if (i == userLutCount)
         {
            nameIsUnique = true;
         }
         else
         {
            MTIostringstream os;
            os << baseLutName << " (" << (++copyNumber) << ")";
            newLutName = os.str();
         }
      }

      LutSettingsManager->SetUserLutSettings(newLutName, CurrentLutSettings);
      SelectLut(LUTSEL_USER, &newLutName);

      // THis should have been handled in Cofrom phase - see why not QQQ
      ShortcutComboBox->ItemIndex = 0;    // select space = no shortcut yet
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::DeleteUserLutButtonClick(TObject *Sender)
{
   if (LutSettingsManager == NULL)
      return;

   // Short cuircuit if something other than a user lut is selected
	if (LutNameComboBox->ItemIndex < NumberOfBuiltInLuts)
      return;

	int selectedItemIndex = LutNameComboBox->ItemIndex;
   string userLutName = StringToStdString(LutNameComboBox->Items->Strings[selectedItemIndex]);

   LutSettingsManager->RemoveUserLutSettings(userLutName);

	SelectLut(LUTSEL_CLIP);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::LutNameComboBoxContextPopup(
      TObject *Sender, TPoint &MousePos, bool &Handled)
{
   // I'm just trying to prevent the bizarre default menu from popping up
   Handled = true;
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ShortcutComboBoxDropDown(
      TObject *Sender)
{
   PopulateShortcutComboBox();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ShortcutComboBoxChange(TObject *Sender)
{
   char newShortcut = ShortcutComboBox->Text.c_str()[0];
   string selectedUserLutName;

   LutSettingsManager->GetLutSelection(selectedUserLutName);
   LutSettingsManager->SetUserLutShortcut(selectedUserLutName, newShortcut);

   // Take a new snapshot - at this point we don't want to undo previous stuff
   TakeSnapshot();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::PopulateShortcutComboBox()
{
   string selectedUserLutName;
   ELutSelection lutSelection = LutSettingsManager->GetLutSelection(selectedUserLutName);
   char selectedLutShortcut = ' ';
   if (lutSelection == LUTSEL_USER)
      selectedLutShortcut = LutSettingsManager->GetUserLutShortcut(selectedUserLutName);
   size_t userLutCount = CurrentUserLutNameList.size();

   ShortcutComboBox->Items->Clear();
   ShortcutComboBox->Items->Append(" ");
   ShortcutComboBox->ItemIndex = 0;

   for (int i = 0; i < 10; ++i)
   {
      std::stringstream ss;
      string dropdownListItem;
      ss << i;
      ss >> dropdownListItem;

      if (dropdownListItem[0] == selectedLutShortcut)
      {
         int itemIndex = ShortcutComboBox->Items->Count;
         ShortcutComboBox->Items->Append(dropdownListItem.c_str());
         ShortcutComboBox->ItemIndex = itemIndex;
      }
      else
      {
         size_t j;
         for (j = 0; j < userLutCount; ++j)
         {
            char shortcut = LutSettingsManager->GetUserLutShortcut(CurrentUserLutNameList[j]);
            if (shortcut == dropdownListItem[0])
               break;
         }
         // OK to add if noone else is using it
         if (j == userLutCount)
            ShortcutComboBox->Items->Append(dropdownListItem.c_str());
      }
   }
}
//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- SOURCE COLOR SPACE GROUP --------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void TLutSettingsEditor::InitSourceColorSpaceGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   // Nothing to do here

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshSourceColorSpaceGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   bool usingDefault = CurrentLutSettings.UsingDefaultSourceColorSpace();

   // Do both to make sure at least one is checked!
   ColorSpaceDefaultRadioButton->Checked = usingDefault;
   ColorSpaceCustomRadioButton->Checked = !usingDefault;

   if (!usingDefault)
   {
      switch (CurrentLutSettings.GetSourceColorSpace())
      {
      default:
         ExrColorSpaceRadioButton->Checked = CurrentClipIsEXR;
         LinearColorSpaceRadioButton->Checked = !CurrentClipIsEXR;
         break;

      case LUTCS_LINEAR:
         LinearColorSpaceRadioButton->Checked = true;
         break;

      case LUTCS_LOG:
         LogColorSpaceRadioButton->Checked = true;
         break;

      case LUTCS_EXR:
         ExrColorSpaceRadioButton->Checked = true;
         break;

/********
      case LUTCS_SMPTE_240:
         SMPTE_240ColorSpaceRadioButton->Checked = true;
         break;

      case LUTCS_CCIR_709:
         CCIR_709ColorSpaceRadioButton->Checked = true;
         break;

      case LUTCS_CCIR_601BG:
         CCIR_601BGColorSpaceRadioButton->Checked = true;
         break;

      case LUTCS_CCIR_601M:
         CCIR_601MColorSpaceRadioButton->Checked = true;
         break;
********/
      }
   }

   ConformSourceColorSpaceGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::UpdateCurrentSourceColorSpaceSettings()
{
   bool defaultIsChecked = ColorSpaceDefaultRadioButton->Checked;
   CurrentLutSettings.UseDefaultSourceColorSpace(defaultIsChecked);

   if (!defaultIsChecked)
   {
      ELutColorSpace newColorSpace = CurrentClipIsEXR ? LUTCS_EXR : LUTCS_LINEAR;

      if (LogColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_LOG;

      if (ExrColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_EXR;
/******
      if (SMPTE_240ColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_SMPTE_240;
      if (CCIR_709ColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_CCIR_709;
      if (CCIR_601MColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_CCIR_601M;
      if (CCIR_601BGColorSpaceRadioButton->Checked)
         newColorSpace = LUTCS_CCIR_601BG;
*******/

      CurrentLutSettings.SetSourceColorSpace(newColorSpace);
   }

   ApplyCurrentSettings();
   RefreshGuiFromCurrentSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformSourceColorSpaceGroup()
{
   MTIassert(InhibitCallbacks);

   bool showingDefault = ColorSpaceDefaultRadioButton->Checked;

   // Normal radio button captions (without " (default)")
   LinearColorSpaceRadioButton->Caption = "Linear";
   LogColorSpaceRadioButton->Caption = "Log";
   ExrColorSpaceRadioButton->Caption = "EXR";
   CCIR_601BGColorSpaceRadioButton->Caption = "CCIR 601BG";
   CCIR_601MColorSpaceRadioButton->Caption = "CCIR 601M";
   CCIR_709ColorSpaceRadioButton->Caption = "CCIR 709";
   SMPTE_240ColorSpaceRadioButton->Caption = "SMPTE 240";

   // Linear, log and exr are disabled for YUV, the rest are disabled for RGB and Mono
   LinearColorSpaceRadioButton->Enabled = !(showingDefault || CurrentClipIsYUV || CurrentClipIsEXR);
   LogColorSpaceRadioButton->Enabled = LinearColorSpaceRadioButton->Enabled || CurrentClipIsEXR;
   ExrColorSpaceRadioButton->Enabled = CurrentClipIsEXR;
   CCIR_601BGColorSpaceRadioButton->Enabled = !(showingDefault ||
                                           CurrentClipIsRGB || CurrentClipIsMono || CurrentClipIsEXR);
   CCIR_601MColorSpaceRadioButton->Enabled = CCIR_601BGColorSpaceRadioButton->Enabled;
   CCIR_709ColorSpaceRadioButton->Enabled = CCIR_601BGColorSpaceRadioButton->Enabled;
   SMPTE_240ColorSpaceRadioButton->Enabled = CCIR_601BGColorSpaceRadioButton->Enabled;

   switch (CurrentClipDefaultColorSpace)
   {
      case IF_COLOR_SPACE_LINEAR: // Typically RGB
         LinearColorSpaceRadioButton->Caption = "Linear (default)";
         if (showingDefault)
            LinearColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_LOG:    // Typically RGB
         LogColorSpaceRadioButton->Caption = "Log (default)";
         if (showingDefault)
            LogColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_EXR:
         ExrColorSpaceRadioButton->Caption = "EXR (default)";
         if (showingDefault)
            ExrColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_SMPTE_240: // Typically video
         SMPTE_240ColorSpaceRadioButton->Caption = "SMPTE 240 (default)";
         if (showingDefault)
            SMPTE_240ColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_CCIR_709: // Typically HD video, also SD video
         CCIR_709ColorSpaceRadioButton->Caption = "CCIR 709 (default)";
         if (showingDefault)
            CCIR_709ColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_CCIR_601_BG: // 601 System B or G, Typically 625
         CCIR_601BGColorSpaceRadioButton->Caption = "CCIR 601BG (default)";
         if (showingDefault)
            CCIR_601BGColorSpaceRadioButton->Checked = true;
         break;

      case IF_COLOR_SPACE_CCIR_601_M: // 601 System M, Typically 525
         CCIR_601MColorSpaceRadioButton->Caption = "CCIR 601M (default)";
         if (showingDefault)
            CCIR_601MColorSpaceRadioButton->Checked = true;
         break;

      default:
         MTIassert(false);
         break;
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ColorSpaceDefaultOrCustomRadioButtonClick(
      TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   // Hack - want to restore old custom setting if we are going back to custom
   if (ColorSpaceCustomRadioButton->Checked)
   {
      ELutColorSpace customColorSpace = CurrentLutSettings.GetSourceColorSpace();
      switch (customColorSpace)
      {
         default:
            ExrColorSpaceRadioButton->Checked = CurrentClipIsEXR;
            LinearColorSpaceRadioButton->Checked = !CurrentClipIsEXR;
            CurrentLutSettings.SetSourceColorSpace(CurrentClipIsEXR ? LUTCS_EXR : LUTCS_LINEAR);
            break;

         case LUTCS_LINEAR:
            LinearColorSpaceRadioButton->Checked = true;
            break;

         case LUTCS_LOG:
            LogColorSpaceRadioButton->Checked = true;
            break;

         case LUTCS_EXR:
            ExrColorSpaceRadioButton->Checked = true;
            break;
/*****
         case LUTCS_SMPTE_240:
            SMPTE_240ColorSpaceRadioButton->Checked = true;
            break;

         case LUTCS_CCIR_709:
            CCIR_709ColorSpaceRadioButton->Checked = true;
            break;

         case LUTCS_CCIR_601M:
            CCIR_601MColorSpaceRadioButton->Checked = true;
            break;

         case LUTCS_CCIR_601BG:
            CCIR_601BGColorSpaceRadioButton->Checked = true;
            break;
*****/
      }
   }

   UpdateCurrentSourceColorSpaceSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::CustomColorSpaceRadioButtonClick(
      TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   UpdateCurrentSourceColorSpaceSettings();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- SOURCE REF POINTS BOX ----------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::InitSourceRangeGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   // One-time-only stuff

   // Not sure why we need to create these controls dynamically
//   SourceRangeMinRYSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMinRYSpinEdit,
//                                           "SourceRangeMinRYSpinEdit");
//   SourceRangeMinGUSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMinGUSpinEdit,
//                                           "SourceRangeMinGUSpinEdit");
//   SourceRangeMinBVSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMinBVSpinEdit,
//                                           "SourceRangeMinBVSpinEdit");
//   SourceRangeMaxRYSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMaxRYSpinEdit,
//                                           "SourceRangeMaxRYSpinEdit");
//   SourceRangeMaxGUSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMaxGUSpinEdit,
//                                           "SourceRangeMaxGUSpinEdit");
//   SourceRangeMaxBVSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//                                           xSourceRangeMaxBVSpinEdit,
//                                           "SourceRangeMaxBVSpinEdit");

   // These are the callbacks for when you click the tiny spin edit
   // up/down arrow buttons
   SET_CBHOOK(SourceRangeMinRYSpinEditHasChanged,
              SourceRangeMinRYSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherSourceRangeSpinEditHasChanged,
              SourceRangeMinGUSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherSourceRangeSpinEditHasChanged,
              SourceRangeMinBVSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SourceRangeMaxRYSpinEditHasChanged,
              SourceRangeMaxRYSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherSourceRangeSpinEditHasChanged,
              SourceRangeMaxGUSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherSourceRangeSpinEditHasChanged,
              SourceRangeMaxBVSpinEdit->SpinEditValueChange);

   InitSourceRangeExrGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::InitSourceRangeExrGroup()
{
   // Hmm, nothing to do here!
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshSourceRangeGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   if (ExrColorSpaceRadioButton->Checked && CurrentClipBitsPerComponent == 16)
   {
      RefreshSourceRangeExrGroup();
      InhibitCallbacks = false;
      return;
   }

   SourceRangeGroup->Visible = true;
   SourceRangeExrGroup->Visible = false;

   // Customize the sourceRef range groups based on the bits per component
   int maxComponentValue = 1;
   maxComponentValue <<= CurrentClipBitsPerComponent;
   maxComponentValue -= 1;

   SourceRangeMinRYSpinEdit->SetRange(0, maxComponentValue);
   SourceRangeMinGUSpinEdit->SetRange(0, maxComponentValue);
   SourceRangeMinBVSpinEdit->SetRange(0, maxComponentValue);
   SourceRangeMaxRYSpinEdit->SetRange(0, maxComponentValue);
   SourceRangeMaxGUSpinEdit->SetRange(0, maxComponentValue);
   SourceRangeMaxBVSpinEdit->SetRange(0, maxComponentValue);

   // Copy the current component settings to the combo boxes
   MTI_UINT16 minValues[3];
   MTI_UINT16 maxValues[3];
   CurrentLutSettings.GetSourceRange(CurrentClipBitsPerComponent,
                                     minValues, maxValues);
   SourceRangeMinRYSpinEdit->SetValue(minValues[0]);
   SourceRangeMinGUSpinEdit->SetValue(minValues[1]);
   SourceRangeMinBVSpinEdit->SetValue(minValues[2]);
   SourceRangeMaxRYSpinEdit->SetValue(maxValues[0]);
   SourceRangeMaxGUSpinEdit->SetValue(maxValues[1]);
   SourceRangeMaxBVSpinEdit->SetValue(maxValues[2]);

   // Customize the range groups based on the default pixel components
   if (CurrentClipIsRGB || CurrentClipIsEXR)
   {
      SourceRangeDefaultRadioButton->Enabled = true;
      DefaultRYLabel->Caption = "R";
      DefaultRYLabel->Enabled = true;
      DefaultGULabel->Caption = "G";
      DefaultGULabel->Enabled = true;
      DefaultBVLabel->Caption = "B";
      DefaultBVLabel->Enabled = true;

      DefaultBlackLabel->Enabled = true;
      DefaultWhiteLabel->Enabled = true;

      SourceRangeCustomRadioButton->Enabled = true;
      SourceRangeRYLabel->Caption = "R";
      SourceRangeRYLabel->Enabled = true;
      SourceRangeGULabel->Caption = "G";
      SourceRangeGULabel->Enabled = true;
      SourceRangeBVLabel->Caption = "B";
      SourceRangeBVLabel->Enabled = true;

      SourceRangeMinLabel->Enabled = true;
      SourceRangeMinLockButton->Enabled = true;
      SourceRangeMinRYSpinEdit->Activate(true);
      SourceRangeMinGUSpinEdit->Activate(true);
      SourceRangeMinBVSpinEdit->Activate(true);
      SourceRangeMinRYSpinEdit->Enable(true);
      SourceRangeMinGUSpinEdit->Enable(true);
      SourceRangeMinBVSpinEdit->Enable(true);

      SourceRangeMaxLabel->Enabled = true;
      SourceRangeMaxLockButton->Enabled = true;
      SourceRangeMaxRYSpinEdit->Activate(true);
      SourceRangeMaxGUSpinEdit->Activate(true);
      SourceRangeMaxBVSpinEdit->Activate(true);
      SourceRangeMaxRYSpinEdit->Enable(true);
      SourceRangeMaxGUSpinEdit->Enable(true);
      SourceRangeMaxBVSpinEdit->Enable(true);
   }
   else if (CurrentClipIsMono)
   {
      SourceRangeDefaultRadioButton->Enabled = true;
      DefaultRYLabel->Caption = "Y";
      DefaultRYLabel->Enabled = true;
      DefaultGULabel->Caption = " ";
      DefaultGULabel->Enabled = false;
      DefaultBVLabel->Caption = " ";
      DefaultBVLabel->Enabled = false;

      DefaultBlackLabel->Enabled = true;
      DefaultWhiteLabel->Enabled = true;

      SourceRangeCustomRadioButton->Enabled = true;
      SourceRangeRYLabel->Caption = "Y";
      SourceRangeRYLabel->Enabled = true;
      SourceRangeGULabel->Caption = " ";
      SourceRangeGULabel->Enabled = false;
      SourceRangeBVLabel->Caption = " ";
      SourceRangeBVLabel->Enabled = false;

      SourceRangeMinLabel->Enabled = true;
      SourceRangeMinLockButton->Enabled = false;
      SourceRangeMinIsLocked = true;
      SourceRangeMinRYSpinEdit->Activate(true);
      SourceRangeMinRYSpinEdit->Enable(true);
      SourceRangeMinGUSpinEdit->Activate(false);
      SourceRangeMinBVSpinEdit->Activate(false);

      SourceRangeMaxLabel->Enabled = true;
      SourceRangeMaxLockButton->Enabled = false;
      SourceRangeMaxIsLocked = true;
      SourceRangeMaxRYSpinEdit->Activate(true);
      SourceRangeMaxRYSpinEdit->Enable(true);
      SourceRangeMaxGUSpinEdit->Activate(false);
      SourceRangeMaxBVSpinEdit->Activate(false);
   }
   else if (CurrentClipIsYUV)
   {
     SourceRangeDefaultRadioButton->Enabled = true;
      DefaultRYLabel->Caption = "Y";
      DefaultRYLabel->Enabled = true;
      DefaultGULabel->Caption = "U";
      DefaultGULabel->Enabled = true;
      DefaultBVLabel->Caption = "V";
      DefaultBVLabel->Enabled = true;

      DefaultBlackLabel->Enabled = true;
      DefaultWhiteLabel->Enabled = true;

      SourceRangeCustomRadioButton->Enabled = true;
      SourceRangeRYLabel->Caption = "Y";
      SourceRangeRYLabel->Enabled = true;
      SourceRangeGULabel->Caption = "U";
      SourceRangeGULabel->Enabled = true;
      SourceRangeBVLabel->Caption = "V";
      SourceRangeBVLabel->Enabled = true;

      SourceRangeMinLabel->Enabled = true;
      SourceRangeMinLockButton->Enabled = true;
      SourceRangeMinRYSpinEdit->Activate(true);
      SourceRangeMinGUSpinEdit->Activate(true);
      SourceRangeMinBVSpinEdit->Activate(true);
      SourceRangeMinRYSpinEdit->Enable(true);
      SourceRangeMinGUSpinEdit->Enable(true);
      SourceRangeMinBVSpinEdit->Enable(true);

      SourceRangeMaxLabel->Enabled = true;
      SourceRangeMaxLockButton->Enabled = true;
      SourceRangeMaxRYSpinEdit->Activate(true);
      SourceRangeMaxGUSpinEdit->Activate(true);
      SourceRangeMaxBVSpinEdit->Activate(true);
      SourceRangeMaxRYSpinEdit->Enable(true);
      SourceRangeMaxGUSpinEdit->Enable(true);
      SourceRangeMaxBVSpinEdit->Enable(true);
   }
   else // None of the above
   {
      SourceRangeDefaultRadioButton->Enabled = false;
      DefaultRYLabel->Caption = " ";
      DefaultRYLabel->Enabled = false;
      DefaultGULabel->Caption = " ";
      DefaultGULabel->Enabled = false;
      DefaultBVLabel->Caption = " ";
      DefaultBVLabel->Enabled = false;

      DefaultBlackLabel->Enabled = false;
      DefaultWhiteLabel->Enabled = false;

      SourceRangeCustomRadioButton->Enabled = false;
      SourceRangeRYLabel->Caption = " ";
      SourceRangeRYLabel->Enabled = false;
      SourceRangeGULabel->Caption = " ";
      SourceRangeGULabel->Enabled = false;
      SourceRangeBVLabel->Caption = " ";
      SourceRangeBVLabel->Enabled = false;

      SourceRangeMinLabel->Enabled = false;
      SourceRangeMinLockButton->Enabled = false;
      SourceRangeMinRYSpinEdit->Activate(false);
      SourceRangeMinGUSpinEdit->Activate(false);
      SourceRangeMinBVSpinEdit->Activate(false);

      SourceRangeMaxLabel->Enabled = false;
      SourceRangeMaxLockButton->Enabled = false;
      SourceRangeMaxRYSpinEdit->Activate(false);
      SourceRangeMaxGUSpinEdit->Activate(false);
      SourceRangeMaxBVSpinEdit->Activate(false);
   }

   // Update default/custom radio buttons from current settings
   // Do both to make sure at least one is checked!
   bool usingDefault = CurrentLutSettings.UsingDefaultSourceRange();
   SourceRangeDefaultRadioButton->Checked = usingDefault;
   SourceRangeCustomRadioButton->Checked = !usingDefault;

   ConformSourceRangeGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshSourceRangeExrGroup()
{
   SourceRangeExrGroup->Visible = true;
   SourceRangeGroup->Visible = false;

   MTI_REAL32 exposureAdjust;
   MTI_REAL32 dynamicRange;
   CurrentLutSettings.GetSourceRangeExr(exposureAdjust, dynamicRange);

   // Compute exposure adjustment readout string.
   int absexposureAdjustTimes10 = (int) (0.5 + fabs(exposureAdjust * 10.0));
   int sign = (exposureAdjust < 0.0F) ? -1 : 1;
   ExposureAdjustTrackBar->Position = sign * absexposureAdjustTimes10;
   ExposureAdjustReadoutLabel->Caption = AnsiString((absexposureAdjustTimes10 == 0)
                                                    ? ""
                                                    : ((sign < 0) ? "-" : "+"))
                                       + AnsiString(absexposureAdjustTimes10 / 10)
                                       + AnsiString(".")
                                       + AnsiString(absexposureAdjustTimes10 % 10);

   // Compute dynamic range readout string.
   int dynamicRangeTimes10 = (int) (0.5F + (dynamicRange * 10.0F));
   DynamicRangeTrackBar->Position = dynamicRangeTimes10;
   DynamicRangeReadoutLabel->Caption = AnsiString(dynamicRangeTimes10 / 10)
                                     + AnsiString(".")
                                     + AnsiString(dynamicRangeTimes10 % 10);

   // Update default/custom radio buttons from current settings
   // Do both to make sure at least one is checked!
   bool usingDefault = CurrentLutSettings.UsingDefaultSourceRangeExr();
   SourceRangeExrDefaultRadioButton->Checked = usingDefault;
   SourceRangeExrCustomRadioButton->Checked = !usingDefault;

   ConformSourceRangeExrGroup();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformSourceRangeGroup()
{
   MTIassert(InhibitCallbacks);

   if (ExrColorSpaceRadioButton->Checked)
   {
      ConformSourceRangeExrGroup();
      return;
   }

   if (SourceRangeDefaultRadioButton->Checked)
   {
      SourceRangeDefaultsPanel->Visible = true;
      SourceRangeSettingsPanel->Visible = false;
   }
   else
   {
      SourceRangeDefaultsPanel->Visible = false;
      SourceRangeSettingsPanel->Visible = true;

      // Check locks vs spinedits
      MTI_UINT16 minValues[3];
      MTI_UINT16 maxValues[3];
      CurrentLutSettings.GetSourceRange(CurrentClipBitsPerComponent,
                                      minValues, maxValues);

      bool minsAreEqual  = CurrentClipIsMono || ((minValues[0] == minValues[1] && minValues[1] == minValues[2]));
      bool maxesAreEqual = CurrentClipIsMono || ((maxValues[0] == maxValues[1] && maxValues[1] == maxValues[2]));

      if (!minsAreEqual)
         SourceRangeMinIsLocked = false;
      if (!maxesAreEqual)
         SourceRangeMaxIsLocked = false;
      if (minsAreEqual && SourceRangeMinLockTendency && !SourceRangeMinIsLocked)
         SourceRangeMinIsLocked = true;
      if (maxesAreEqual && SourceRangeMaxLockTendency && !SourceRangeMaxIsLocked)
         SourceRangeMaxIsLocked = true;

      // Synch lock buttons with state variables
      if (SourceRangeMinIsLocked != SourceRangeMinLockButton->Down)
      {
         SourceRangeMinLockButton->Down = SourceRangeMinIsLocked;
         if (SourceRangeMinIsLocked)
         {
            SourceRangeMinGUSpinEdit->SetValue(SourceRangeMinRYSpinEdit->GetValue());
            SourceRangeMinBVSpinEdit->SetValue(SourceRangeMinRYSpinEdit->GetValue());
         }
      }
      if (SourceRangeMaxIsLocked != SourceRangeMaxLockButton->Down)
      {
         SourceRangeMaxLockButton->Down = SourceRangeMaxIsLocked;
         if (SourceRangeMaxIsLocked)
         {
            SourceRangeMaxGUSpinEdit->SetValue(SourceRangeMaxRYSpinEdit->GetValue());
            SourceRangeMaxBVSpinEdit->SetValue(SourceRangeMaxRYSpinEdit->GetValue());
         }
      }

      // Enable or disable slaved spinedits
      SourceRangeMinGUSpinEdit->Enable(!(SourceRangeMinIsLocked || CurrentClipIsMono));
      SourceRangeMinBVSpinEdit->Enable(!(SourceRangeMinIsLocked || CurrentClipIsMono));
      SourceRangeMaxGUSpinEdit->Enable(!(SourceRangeMaxIsLocked || CurrentClipIsMono));
      SourceRangeMaxBVSpinEdit->Enable(!(SourceRangeMaxIsLocked || CurrentClipIsMono));
   }

   MonoSourceRangeGBCoverUpPanel->Visible = CurrentClipIsMono;

   MTIassert(Displayer != NULL);
   if (Displayer != NULL)
   {
      // Ensure source range default values are correct
      char text[6];
      sprintf(text, "%d", Displayer->getComponentBlack(0));
      DefaultBlackRYEdit->Text = text;
      sprintf(text, "%d", Displayer->getComponentWhite(0));
      DefaultWhiteRYEdit->Text = text;

      sprintf(text, "%d", Displayer->getComponentBlack(1));
      DefaultBlackGUEdit->Text = CurrentClipIsMono ? "" : text;
      sprintf(text, "%d", Displayer->getComponentWhite(1));
      DefaultWhiteGUEdit->Text = CurrentClipIsMono ? "" : text;

      sprintf(text, "%d", Displayer->getComponentBlack(2));
      DefaultBlackBVEdit->Text = CurrentClipIsMono ? "" : text;
      sprintf(text, "%d", Displayer->getComponentWhite(2));
      DefaultWhiteBVEdit->Text = CurrentClipIsMono ? "" : text;
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformSourceRangeExrGroup()
{
   bool custom = SourceRangeExrCustomRadioButton->Checked;
   ExposureAdjustReadoutLabel->Enabled = custom;
   ExposureAdjustTrackBar->Enabled = custom;
   DynamicRangeReadoutLabel->Enabled = custom;
   DynamicRangeTrackBar->Enabled = custom;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::UpdateCurrentSourceRangeSettings()
{
   if (ExrColorSpaceRadioButton->Checked)
   {
      UpdateCurrentSourceRangeExrSettings();
      return;
   }

   bool defaultButtonChecked = SourceRangeDefaultRadioButton->Checked;

   CurrentLutSettings.UseDefaultSourceRange(defaultButtonChecked);

   if (!defaultButtonChecked)
   {
      MTI_UINT16 minValues[3];
      MTI_UINT16 maxValues[3];

      minValues[0] = SourceRangeMinRYSpinEdit->GetValue();
      maxValues[0] = SourceRangeMaxRYSpinEdit->GetValue();
      minValues[1] = CurrentClipIsMono ? minValues[0] : SourceRangeMinGUSpinEdit->GetValue();
      maxValues[1] = CurrentClipIsMono ? maxValues[0] : SourceRangeMaxGUSpinEdit->GetValue();
      minValues[2] = CurrentClipIsMono ? minValues[0] : SourceRangeMinBVSpinEdit->GetValue();
      maxValues[2] = CurrentClipIsMono ? maxValues[0] : SourceRangeMaxBVSpinEdit->GetValue();

      CurrentLutSettings.SetSourceRange(CurrentClipBitsPerComponent,
                                        minValues, maxValues);
   }

   ApplyCurrentSettings();
   RefreshGuiFromCurrentSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::UpdateCurrentSourceRangeExrSettings()
{
   MTI_REAL32 minValues[3] = {0.5F, 0.5F, 0.5F};
   MTI_REAL32 maxValues[3] = {0.734375F, 0.734375F, 0.734375F};
   MTI_REAL32 exposureAdjust = 0.0F;
   MTI_REAL32 dynamicRange = 15.0F;

   bool defaultButtonChecked = SourceRangeExrDefaultRadioButton->Checked;
   CurrentLutSettings.UseDefaultSourceRangeExr(defaultButtonChecked);

   if (!defaultButtonChecked)
   {
      exposureAdjust = ExposureAdjustTrackBar->Position / 10.0F;
      dynamicRange = DynamicRangeTrackBar->Position / 10.0F;
      maxValues[0] = maxValues[1] = maxValues[2] = 0.734375F - ((0.234375F * exposureAdjust) / 15.0F);
      minValues[0] = minValues[1] = minValues[2] = maxValues[0] - ((0.234375F * dynamicRange) / 15.0F);
   }

   CurrentLutSettings.SetSourceRange(minValues, maxValues);
   CurrentLutSettings.SetSourceRangeExr(exposureAdjust, dynamicRange);

   ExrLutUpdateHackTimer->Interval = 250;
   ExrLutUpdateHackTimer->Enabled = true;
   RefreshSourceRangeExrGroup();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ExrLutUpdateHackTimerTimer(TObject *Sender)
{
   // Notify of changes if the mouse left button is not down
   short leftMouseButtonState = ::GetAsyncKeyState(VK_LBUTTON);
   bool leftMouseButtonIsDown = leftMouseButtonState < 0;  // if MSB is set
   if (!leftMouseButtonIsDown)
   {
      ExrLutUpdateHackTimer->Enabled = false;
      ApplyCurrentSettings();
      RefreshGuiFromCurrentSettings();
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SourceRangeMinLockButtonClick(TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   SourceRangeMinIsLocked = !SourceRangeMinIsLocked;
   SourceRangeMinLockButton->Down = SourceRangeMinIsLocked;
   SourceRangeMinLockTendency = SourceRangeMinIsLocked;

   // The following calls UpdateCurrentSourceRangeSettings()
   SourceRangeMinRYSpinEditHasChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SourceRangeMaxLockButtonClick(TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   SourceRangeMaxIsLocked = !SourceRangeMaxIsLocked;
   SourceRangeMaxLockButton->Down = SourceRangeMaxIsLocked;
   SourceRangeMaxLockTendency = SourceRangeMaxIsLocked;

   // The following calls UpdateCurrentSourceRangeSettings()
   SourceRangeMaxRYSpinEditHasChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SourceRangeRadioButtonClick(TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   UpdateCurrentSourceRangeSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::SourceRangeMinRYSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   // Slave GU and BV to RY
   if (SourceRangeMinIsLocked)
   {
      InhibitCallbacks = true;
      SourceRangeMinGUSpinEdit->SetValue(SourceRangeMinRYSpinEdit->GetValue());
      SourceRangeMinBVSpinEdit->SetValue(SourceRangeMinRYSpinEdit->GetValue());
      InhibitCallbacks = false;
   }
   UpdateCurrentSourceRangeSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::SourceRangeMaxRYSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   // slave G and B to R
   if (SourceRangeMaxIsLocked)
   {
      InhibitCallbacks = true;
      SourceRangeMaxGUSpinEdit->SetValue(SourceRangeMaxRYSpinEdit->GetValue());
      SourceRangeMaxBVSpinEdit->SetValue(SourceRangeMaxRYSpinEdit->GetValue());
      InhibitCallbacks = false;
   }
   UpdateCurrentSourceRangeSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::OtherSourceRangeSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   if (CurrentClipIsMono)
      return;

   UpdateCurrentSourceRangeSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SourceRangeExrTrackBarChange(TObject *Sender)

{
   if (InhibitCallbacks)
      return;

   UpdateCurrentSourceRangeExrSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SourceRangeExrRadioButtonClick(TObject *Sender)

{
   if (InhibitCallbacks)
      return;

   UpdateCurrentSourceRangeExrSettings();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- DISPLAY RANGE GROUP -------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::InitDisplayRangeGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   // Not sure why these need to be constructed on the fly
//   DisplayRangeMinRSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMinRSpinEdit,
//										   "DisplayRangeMinRSpinEdit");
//   DisplayRangeMinGSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMinGSpinEdit,
//										   "DisplayRangeMinGSpinEdit");
//   DisplayRangeMinBSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMinBSpinEdit,
//										   "DisplayRangeMinBSpinEdit");
//   DisplayRangeMaxRSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMaxRSpinEdit,
//										   "DisplayRangeMaxRSpinEdit");
//   DisplayRangeMaxGSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMaxGSpinEdit,
//										   "DisplayRangeMaxGSpinEdit");
//   DisplayRangeMaxBSpinEdit = TSpinEditFrame::CreateFromTemplate(this,
//										   xDisplayRangeMaxBSpinEdit,
//										   "DisplayRangeMaxBSpinEdit");

   InhibitCallbacks = true; // I hate this shizz

   // 255 because display is 8 bit
   DisplayRangeMinRSpinEdit->SetRange(0, 255);
   DisplayRangeMinGSpinEdit->SetRange(0, 255);
   DisplayRangeMinBSpinEdit->SetRange(0, 255);
   DisplayRangeMaxRSpinEdit->SetRange(0, 255);
   DisplayRangeMaxGSpinEdit->SetRange(0, 255);
   DisplayRangeMaxBSpinEdit->SetRange(0, 255);

   DisplayRangeMinRSpinEdit->ParentFont = true;
   DisplayRangeMinGSpinEdit->ParentFont = true;
   DisplayRangeMinBSpinEdit->ParentFont = true;
   DisplayRangeMaxRSpinEdit->ParentFont = true;
   DisplayRangeMaxGSpinEdit->ParentFont = true;
   DisplayRangeMaxBSpinEdit->ParentFont = true;

   // try to get these frickin things to not show up in front of the defaults
   DisplayRangeMinRSpinEdit->SendToBack();
   DisplayRangeMinGSpinEdit->SendToBack();
   DisplayRangeMinBSpinEdit->SendToBack();
   DisplayRangeMaxRSpinEdit->SendToBack();
   DisplayRangeMaxGSpinEdit->SendToBack();
   DisplayRangeMaxBSpinEdit->SendToBack();

   // I think these are the callbacks for when you click the tiny spin edit
   // up/down arrow buttons
   SET_CBHOOK(DisplayRangeMinRSpinEditHasChanged,
              DisplayRangeMinRSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherDisplayRangeSpinEditHasChanged,
              DisplayRangeMinGSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherDisplayRangeSpinEditHasChanged,
              DisplayRangeMinBSpinEdit->SpinEditValueChange);
   SET_CBHOOK(DisplayRangeMaxRSpinEditHasChanged,
              DisplayRangeMaxRSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherDisplayRangeSpinEditHasChanged,
              DisplayRangeMaxGSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OtherDisplayRangeSpinEditHasChanged,
              DisplayRangeMaxBSpinEdit->SpinEditValueChange);

   DisplayRangeRLabel->Caption = "R";
   DisplayRangeRLabel->Enabled = true;
   DisplayRangeGLabel->Caption = "G";
   DisplayRangeGLabel->Enabled = true;
   DisplayRangeBLabel->Caption = "B";
   DisplayRangeBLabel->Enabled = true;

   DisplayRangeMinLabel->Enabled = true;
   DisplayRangeMinLockButton->Enabled = true;
   DisplayRangeMinRSpinEdit->Activate(true);
   DisplayRangeMinGSpinEdit->Activate(true);
   DisplayRangeMinBSpinEdit->Activate(true);
   DisplayRangeMinRSpinEdit->Enable(true);
   DisplayRangeMinGSpinEdit->Enable(true);
   DisplayRangeMinBSpinEdit->Enable(true);

   DisplayRangeMaxLabel->Enabled = true;
   DisplayRangeMaxLockButton->Enabled = true;
   DisplayRangeMaxRSpinEdit->Activate(true);
   DisplayRangeMaxGSpinEdit->Activate(true);
   DisplayRangeMaxBSpinEdit->Activate(true);
   DisplayRangeMaxRSpinEdit->Enable(true);
   DisplayRangeMaxGSpinEdit->Enable(true);
   DisplayRangeMaxBSpinEdit->Enable(true);

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshDisplayRangeGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   // get Displayer flag to indicate default or sourceRef source range
   bool usingDefault = CurrentLutSettings.UsingDefaultDisplayRange();

   // Copy the current custom settings to the combo boxes
   // CAREFUL: they are read in as unsigned shorts!
   MTI_UINT16 minValues[3];
   MTI_UINT16 maxValues[3];
   CurrentLutSettings.GetDisplayRange(minValues, maxValues);

   DisplayRangeMinRSpinEdit->SetValue(minValues[0]);
   DisplayRangeMinGSpinEdit->SetValue(minValues[1]);
   DisplayRangeMinBSpinEdit->SetValue(minValues[2]);
   DisplayRangeMaxRSpinEdit->SetValue(maxValues[0]);
   DisplayRangeMaxGSpinEdit->SetValue(maxValues[1]);
   DisplayRangeMaxBSpinEdit->SetValue(maxValues[2]);

   // Copy 'default' state to radio buttons
   // Do both to make sure at least one is checked!
   DisplayRangeDefaultRadioButton->Checked = usingDefault;
   DisplayRangeCustomRadioButton->Checked = !usingDefault;

   ConformDisplayRangeGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformDisplayRangeGroup()
{
   MTIassert(InhibitCallbacks);

   if (DisplayRangeDefaultRadioButton->Checked)
   {
      DisplayRangeDefaultCoverUpPanel->Visible = true;
      DisplayRangeDefaultCoverUpPanel->BringToFront();
   }
   else
   {
      DisplayRangeDefaultCoverUpPanel->Visible = false;

      // Check locks vs spinedits
      MTI_UINT16 minValues[3];
      MTI_UINT16 maxValues[3];
      CurrentLutSettings.GetDisplayRange(minValues, maxValues);
      bool minsAreEqual = (minValues[0] == minValues[1] && minValues[1] == minValues[2]);
      bool maxesAreEqual = (maxValues[0] == maxValues[1] && maxValues[1] == maxValues[2]);

      if (!minsAreEqual)
         DisplayRangeMinIsLocked = false;
      if (!maxesAreEqual)
         DisplayRangeMaxIsLocked = false;
      if (minsAreEqual && DisplayRangeMinLockTendency && !DisplayRangeMinIsLocked)
         DisplayRangeMinIsLocked = true;
      if (maxesAreEqual && DisplayRangeMaxLockTendency && !DisplayRangeMaxIsLocked)
         DisplayRangeMaxIsLocked = true;

      // Synch lock buttons with state variables
      if (DisplayRangeMinIsLocked != DisplayRangeMinLockButton->Down)
      {
         DisplayRangeMinLockButton->Down = DisplayRangeMinIsLocked;
         if (DisplayRangeMinIsLocked)
         {
            DisplayRangeMinGSpinEdit->SetValue(DisplayRangeMinRSpinEdit->GetValue());
            DisplayRangeMinBSpinEdit->SetValue(DisplayRangeMinRSpinEdit->GetValue());
         }
      }
      if (DisplayRangeMaxIsLocked != DisplayRangeMaxLockButton->Down)
      {
         DisplayRangeMaxLockButton->Down = DisplayRangeMaxIsLocked;
         if (DisplayRangeMaxIsLocked)
         {
            DisplayRangeMaxGSpinEdit->SetValue(DisplayRangeMaxRSpinEdit->GetValue());
            DisplayRangeMaxBSpinEdit->SetValue(DisplayRangeMaxRSpinEdit->GetValue());
         }
      }

      // Enable or disable slaved spinedits
      DisplayRangeMinGSpinEdit->Enable(!DisplayRangeMinIsLocked);
      DisplayRangeMinBSpinEdit->Enable(!DisplayRangeMinIsLocked);
      DisplayRangeMaxGSpinEdit->Enable(!DisplayRangeMaxIsLocked);
      DisplayRangeMaxBSpinEdit->Enable(!DisplayRangeMaxIsLocked);
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::UpdateCurrentDisplayRangeSettings()
{
   bool defaultButtonChecked = DisplayRangeDefaultRadioButton->Checked;

   CurrentLutSettings.UseDefaultDisplayRange(defaultButtonChecked);

   if (!defaultButtonChecked)
   {
      MTI_UINT16 minValues[3];
      MTI_UINT16 maxValues[3];

      minValues[0] = DisplayRangeMinRSpinEdit->GetValue();
      maxValues[0] = DisplayRangeMaxRSpinEdit->GetValue();
      minValues[1] = DisplayRangeMinGSpinEdit->GetValue();
      maxValues[1] = DisplayRangeMaxGSpinEdit->GetValue();
      minValues[2] = DisplayRangeMinBSpinEdit->GetValue();
      maxValues[2] = DisplayRangeMaxBSpinEdit->GetValue();

      CurrentLutSettings.SetDisplayRange(minValues, maxValues);
   }
   ApplyCurrentSettings();
   RefreshGuiFromCurrentSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::DisplayRangeRadioButtonClick(TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   UpdateCurrentDisplayRangeSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::DisplayRangeMinLockButtonClick(
      TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   DisplayRangeMinIsLocked = !DisplayRangeMinIsLocked;
   DisplayRangeMinLockButton->Down = DisplayRangeMinIsLocked;
   DisplayRangeMinLockTendency = DisplayRangeMinIsLocked;

   // Note: the following calls UpdateCurrentDisplayRangeSettings() for us
   DisplayRangeMinRSpinEditHasChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::DisplayRangeMaxLockButtonClick(
      TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   DisplayRangeMaxIsLocked = !DisplayRangeMaxIsLocked;
   DisplayRangeMaxLockButton->Down = DisplayRangeMaxIsLocked;
   DisplayRangeMaxLockTendency = DisplayRangeMaxIsLocked;

   // Note: the following calls UpdateCurrentDisplayRangeSettings() for us
   DisplayRangeMaxRSpinEditHasChanged(Sender);
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::DisplayRangeMinRSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   // slave G and B to R
   if (DisplayRangeMinIsLocked)
   {
      InhibitCallbacks = true;
      DisplayRangeMinGSpinEdit->SetValue(DisplayRangeMinRSpinEdit->GetValue());
      DisplayRangeMinBSpinEdit->SetValue(DisplayRangeMinRSpinEdit->GetValue());
      InhibitCallbacks = false;
   }
   UpdateCurrentDisplayRangeSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::DisplayRangeMaxRSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   // slave G and B to R
   if (DisplayRangeMaxIsLocked)
   {
      InhibitCallbacks = true;
      DisplayRangeMaxGSpinEdit->SetValue(DisplayRangeMaxRSpinEdit->GetValue());
      DisplayRangeMaxBSpinEdit->SetValue(DisplayRangeMaxRSpinEdit->GetValue());
      InhibitCallbacks = false;
   }
   UpdateCurrentDisplayRangeSettings();
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::OtherDisplayRangeSpinEditHasChanged(void *Sender)
{
   if (InhibitCallbacks)
      return;

   UpdateCurrentDisplayRangeSettings();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------- DISPLAY GAMMA GROUP -------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TLutSettingsEditor::InitDisplayGammaGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   DisplayGammaDefaultRadioButton->Checked = true;
   ConformDisplayGammaGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::RefreshDisplayGammaGroup()
{
   MTIassert(!InhibitCallbacks);
   InhibitCallbacks = true;

   int iGammaTimes10 = CurrentLutSettings.GetDisplayGammaTimes10();

   if (CurrentLutSettings.UsingDefaultDisplayGamma())
   {
      DisplayGammaDefaultRadioButton->Checked = true;
      iGammaTimes10 = DefaultGammaTimes10;
   }
   else
   {
      DisplayGammaCustomRadioButton->Checked = true;
   }

   DisplayGammaSlider->Position = iGammaTimes10;

   MTIostringstream os;
   os << (iGammaTimes10 / 10) << "." << (iGammaTimes10 % 10);
   DisplayGammaValue->Caption = os.str().c_str();

   ConformDisplayGammaGroup();

   InhibitCallbacks = false;
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ConformDisplayGammaGroup()
{
   MTIassert(InhibitCallbacks);

   if (DisplayGammaDefaultRadioButton->Checked)
   {
      DisplayGammaSlider->Enabled = false;
      DisplayGammaValue->Enabled = false;
      DisplayGammaSlider->SliderVisible = false;
   }
   else
   {
      DisplayGammaSlider->Enabled = true;
      DisplayGammaValue->Enabled = true;
      DisplayGammaSlider->SliderVisible = true;
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::UpdateCurrentDisplayGammaSettings()
{
   bool useDefault = DisplayGammaDefaultRadioButton->Checked;
   int iGammaTimes10 = DisplayGammaSlider->Position;

   CurrentLutSettings.UseDefaultDisplayGamma(useDefault);
   CurrentLutSettings.SetDisplayGammaTimes10(iGammaTimes10);

   ApplyCurrentSettings();
   RefreshGuiFromCurrentSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::DisplayGammaRadioButtonClick(
      TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   if (DisplayGammaCustomRadioButton->Checked)
   {
      DisplayGammaSlider->Position = CurrentLutSettings.GetDisplayGammaTimes10();
   }

   UpdateCurrentDisplayGammaSettings();
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::OnGammaSliderChange(TObject *Sender)
{
   if (InhibitCallbacks)
      return;

   UpdateCurrentDisplayGammaSettings();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------ BUTTONS ------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::LutOkButtonClick(TObject *Sender)
{
   // Effectively accept the previewed settings

   ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::LutCancelButtonClick(TObject *Sender)
{
   // Undo the previewed settings
   RevertToSnapshotSettings();

   // Close the dialog
   ModalResult = mrCancel;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------- IMPORT / EXPORT ------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ImportLutButtonClick(TObject *Sender)
{
   OpenFileDialog->Options.Clear();
   char cpCWD[256] = "c:\\";
   ////getcurdir(0, cpCWD + strlen(cpCWD));

   OpenFileDialog->InitialDir = (AnsiString) cpCWD;
   OpenFileDialog->Filter = "LUT files (*.LUT)|*.LUT|All files (*.*)|*.*";
   OpenFileDialog->Title = (AnsiString) "Load LUT from File";

   bool retVal = OpenFileDialog->Execute();
   if (retVal)
   {
	  ImportLut(StringToStdString(OpenFileDialog->FileName));
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::ExportLutButtonClick(TObject *Sender)
{
   if (LastLutExportDir.empty())
   {
      char cpCWD[256] = "c:\\";
      ////getcurdir(0, cpCWD + strlen(cpCWD));
      LastLutExportDir = cpCWD;
   }
   SaveFileDialog->InitialDir = (AnsiString) LastLutExportDir.c_str();
   SaveFileDialog->Options.Clear();
   SaveFileDialog->Filter = "LUT files (*.LUT)|*.lut|All files (*.*)|*.*";
   SaveFileDialog->DefaultExt = "lut";
   SaveFileDialog->Title = (AnsiString) "Save LUT to File";

   bool retVal = SaveFileDialog->Execute();
   if (retVal)
   {
      ExportLut(StringToStdString(SaveFileDialog->FileName));
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ExportLut(const string &filePath)
{
   if (Displayer == NULL)
      return;

   const CImageFormat *imageFormat = Displayer->getImageFormat();

   if (imageFormat != NULL)
   {
      // careful.... stupid export format doesn't understand defaults for
      // display stuff

      CDisplayLutSettings settings = CurrentLutSettings;
      if (settings.UsingDefaultDisplayRange())
      {
         MTI_UINT16 minValues[3] = {0, 0, 0};
         MTI_UINT16 maxValues[3] = {255, 255, 255};
         settings.SetDisplayRange(minValues, maxValues);
      }
      if (settings.UsingDefaultDisplayGamma())
         settings.SetDisplayGammaTimes10(DefaultGammaTimes10);

      settings.ExportSettingsInWackyLutFormat(filePath, imageFormat);
   }
}
//---------------------------------------------------------------------------

void TLutSettingsEditor::ImportLut(const string &filePath)
{
   if (Displayer == NULL)
      return;

   const CImageFormat *imageFormat = Displayer->getImageFormat();

   if (imageFormat != NULL)
   {
      CurrentLutSettings.ImportSettingsFromWackyLutFormat(filePath,
                                                          imageFormat);

      // stupid export format doesn't do defaults for display stuff
      MTI_UINT16 minValues[3];
      MTI_UINT16 maxValues[3];

      CurrentLutSettings.GetDisplayRange(minValues, maxValues);
      if (minValues[0] ==   0 && minValues[1] ==   0 && minValues[2] ==   0 &&
          maxValues[0] == 255 && maxValues[1] == 255 && maxValues[2] == 255 )
      {
         CurrentLutSettings.UseDefaultDisplayRange(true);
      }
      // Don't bother defaulting GAMMA

      ApplyCurrentSettings();
      RefreshGuiFromCurrentSettings();
   }
}
//---------------------------------------------------------------------------


void __fastcall TLutSettingsEditor::SpinEditEnter(
      TObject *Sender)
{
  LutOkButton->Default = false;
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::SpinEditExit(
      TObject *Sender)
{
  LutOkButton->Default = true;
}
//---------------------------------------------------------------------------


void __fastcall TLutSettingsEditor::FormMouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
   if (DisplayGammaSlider->Focused())
   {
      if (DisplayGammaSlider->Position > DisplayGammaSlider->Min)
         DisplayGammaSlider->Position = DisplayGammaSlider->Position - 1;
      Handled = true;
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::FormMouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
   if (DisplayGammaSlider->Focused())
   {
      if (DisplayGammaSlider->Position < DisplayGammaSlider->Max)
         DisplayGammaSlider->Position = DisplayGammaSlider->Position + 1;
      Handled = true;
   }
}
//---------------------------------------------------------------------------

void __fastcall TLutSettingsEditor::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
   if (DisplayGammaSlider->Focused())
   {
      if (Key == MTK_UP)
      {
         if (DisplayGammaSlider->Position < DisplayGammaSlider->Max)
            DisplayGammaSlider->Position = DisplayGammaSlider->Position + 1;
         Key = 0;
      }
      else if (Key == MTK_DOWN)
      {
         if (DisplayGammaSlider->Position > DisplayGammaSlider->Min)
            DisplayGammaSlider->Position = DisplayGammaSlider->Position - 1;
         Key = 0;
      }
   }
}
//---------------------------------------------------------------------------




