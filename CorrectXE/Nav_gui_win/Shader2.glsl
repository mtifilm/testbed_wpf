#ifndef Shader1Glsl
#define Shader1Glsl

#include <string>
using std::string;

static string VertexShader1Source =
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"in vec2 TexCoord;\n"
"in vec3 PixelColor;\n"
"in float ZPos;\n"
"\n"
"uniform sampler2D texture1;\n"
"uniform sampler2D texture2;\n"
"uniform float mixValue;\n"
"\n"
"void main()\n"
"{\n"
"   float bw = (ZPos >= 0.0) ? 1.0 : 0.0;\n"
"   if (ZPos <= 0.002 && ZPos >= -0.002)\n"
"   {\n"
"      FragColor = vec4 (0.9, 0.9, 0.9, 1.0);\n"
"   }\n"
"   else if ((ZPos > 0.002 && ZPos <= -0.003)\n"
"         || (ZPos < -0.002 && ZPos >= -0.003))\n"
"   {\n"
"      FragColor = vec4 (0.1, 0.1, 0.1, 1.0);\n"
"   }\n"
"   else\n"
"   {\n"
"      FragColor = mix(texture(texture1, TexCoord),\n"
"		   texture(texture2, TexCoord),\n"
"		   bw);\n"
"   }\n"
"\n"
"   //FragColor = vec4(PixelColor, 1.0);\n"
"\n"
"   //FragColor = vec4(PixelColor, PixelColor, PixelColor, 1.0);\n"
"\n"
"   //FragColor = vec4(bw, bw, bw, 1.0);\n"
"};\n";

static string FragmentShader1Source =
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"layout (location = 1) in vec3 aColor;\n"
"layout (location = 2) in vec2 aTexCoord;\n"
"\n"
"uniform float yOffset;\n"
"uniform float SliderValue;\n"
"\n"
"out vec2 TexCoord;\n"
"out float ZPos;\n"
"out vec3 PixelColor;\n"
"\n"
"void main()\n"
"{\n"
"   vec3 aPosInv = aPos;\n"
"   vec3 offsetPos = vec3(0.0, yOffset, 0.0);\n"
"   gl_Position = vec4(aPosInv + offsetPos, 1.0);\n"
"   TexCoord = aTexCoord;\n"
"   ZPos = aPos.z + SliderValue;\n"
"   PixelColor = aColor + SliderValue;\n"
"};\n";

#endif
