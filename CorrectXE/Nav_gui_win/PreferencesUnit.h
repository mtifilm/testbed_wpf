//---------------------------------------------------------------------------

#ifndef PreferencesUnitH
#define PreferencesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "MTIUNIT.h"
#include "cspin.h"
#include "SpinEditFrameUnit.h"
#include "TrackEditFrameUnit.h"
#include "ColorLabel.h"
//---------------------------------------------------------------------------
class TPreferencesDlg : public TMTIForm
{
__published:	// IDE-managed Components
        TButton *SaveButton;
        TButton *OKBtn;
        TButton *CancelBtn;
        TButton *RestoreButton;
        TPageControl *PreferenceControl;
        TTabSheet *GeneralSheet;
        TGroupBox *AutoSaveGroupBox;
        TCheckBox *AutoSaveSAPCBox;
        TCheckBox *AutoSavePreferencesCBox;
        TCheckBox *CursorCBox;
        TTabSheet *WindowSheet;
        TGroupBox *BorderWidth;
        TLabel *BorderWidthValue;
        TTrackBar *BorderWidthSlider;
        TTabSheet *SpeedSheet;
        TGroupBox *SpeedSettings;
        TLabel *SlowLabel;
        TLabel *MediumLabel;
        TLabel *FastLabel;
        TTabSheet *GammaSheet;
        TGroupBox *DefaultGamma;
        TLabel *GammaValue;
        TTrackBar *GammaSlider;
   TTabSheet *DPXCacheSheet;
   TLabel *StatusLine1;
   TCheckBox *EnableFrameCacheCheckBox;
   TTrackEditFrame *FrameCacheSizeTrackEdit;
   TGroupBox *StatusGroupBox;
   TLabel *StatusLine2;
   TLabel *StatusLine3;
   TPanel *BGPanel2;
   TPanel *BGPanel3;
   TPanel *BGPanel4;
   TPanel *BGPanel5;
   TPanel *BGPanel6;
   TCheckBox *SwapWAndShiftWCBox;
	TButton *ResetWarningsButton;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
   TCheckBox *LockCacheFramesInMemoryCheckBox;
   TTrackBar *ToolTipsDurationTrackBar;
   TLabel *ToolTipsDurationLabel;
   TLabel *ToolTipsOffLabel;
   TLabel *ToolTipsDUrationLongLabel;
   TLabel *ToolTipsDurationShortLabel;
   TLabel *Label4;
	TButton *ClearCacheButton;
	TTabSheet *GpuSheet;
	TPanel *BGPanel1;
	TComboBox *DeviceNameComboBox;
	TCheckBox *EnableGpuCheckBox;
	TButton *GpuInfoButton;
	TPanel *GpuInfoPanel;
	TColorLabel *GpuNoFoundColorLabel;
	TLabel *GpuNotFoundReasonLabel;
	TSpinEditFrame *SlowSpinEdit;
	TSpinEditFrame *MediumSpinEdit;
	TSpinEditFrame *FastSpinEdit;
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall OKBtnClick(TObject *Sender);
        void __fastcall CancelBtnClick(TObject *Sender);
        void __fastcall ChangeClick(TObject *Sender);
        void __fastcall SaveButtonClick(TObject *Sender);
        void __fastcall RestoreButtonClick(TObject *Sender);
        void __fastcall GammaSliderChange(TObject *Sender);
        void __fastcall CursorCBoxClick(TObject *Sender);
        void __fastcall BorderWidthSliderChange(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall EnableFrameCacheCheckBoxClick(TObject *Sender);
    void __fastcall CacheSizeTrackEditNotifyWidgetClick(TObject *Sender);
   void __fastcall SwapWAndShiftWCBoxClick(TObject *Sender);
	void __fastcall ResetWarningsButtonClick(TObject *Sender);
   void __fastcall LockCacheFramesInMemoryCheckBoxClick(TObject *Sender);
   void __fastcall ToolTipsDurationTrackBarChange(TObject *Sender);
    void __fastcall EnableGpuCheckBoxClick(TObject *Sender);
    void __fastcall DeviceNameComboBoxChange(TObject *Sender);
	void __fastcall ClearCacheButtonClick(TObject *Sender);
	void __fastcall GpuInfoButtonClick(TObject *Sender);


private:	// User declarations
     class CValues
       {

        public:
         CValues(TPreferencesDlg *AOwner)
           {
             Parent = AOwner;
             LoadCurrent();
           }

         void LoadCurrent(void)
           {
              // General
              AutoSaveSAP = Parent->AutoSaveSAPCBox->Checked;
              AutoSavePreferences = Parent->AutoSavePreferencesCBox->Checked;
              CursorIsAnArrow = Parent->CursorCBox->Checked;

              // DPX Cache
              DpxReadCacheEnable = Parent->EnableFrameCacheCheckBox->Checked;
              DpxReadCacheSizeInGB = Parent->FrameCacheSizeTrackEdit->GetValue();

              // Display Window
              BorderWidthValue = Parent->BorderWidthSlider->Position;

              // Display Speed
              SlowDisplaySpeedValue = Parent->SlowSpinEdit->GetValue();
              MediumDisplaySpeedValue = Parent->MediumSpinEdit->GetValue();
              FastDisplaySpeedValue = Parent->FastSpinEdit->GetValue();

              //Display LUT
              DisplayGammaValue = Parent->GammaSlider->Position;

              // Tool TIps Duration
              ToolTipsDurationValue = Parent->ToolTipsDurationTrackBar->Position;

//              EnableGpu = Parent->EnableGpuCheckBox->Checked;
//              GpuIndex = Parent->DeviceNameComboBox->ItemIndex;
           }

         void Restore(void)
           {
              // General
              Parent->AutoSaveSAPCBox->Checked = AutoSaveSAP;
              Parent->AutoSavePreferencesCBox->Checked = AutoSavePreferences;
              Parent->CursorCBox->Checked = CursorIsAnArrow;

              // DPX Cache
              Parent->EnableFrameCacheCheckBox->Checked = DpxReadCacheEnable;
              Parent->FrameCacheSizeTrackEdit->SetValue(DpxReadCacheSizeInGB);

              // Display Window
              Parent->BorderWidthSlider->Position = BorderWidthValue;

              // Display Speed
              Parent->SlowSpinEdit->SetValue(SlowDisplaySpeedValue);
              Parent->MediumSpinEdit->SetValue(MediumDisplaySpeedValue);
              Parent->FastSpinEdit->SetValue(FastDisplaySpeedValue);

              //Display LUT
              Parent->GammaSlider->Position = DisplayGammaValue;

              // Tool TIps Duration
              Parent->ToolTipsDurationTrackBar->Position = ToolTipsDurationValue;

//              Parent->EnableGpuCheckBox->Checked = EnableGpu;
//              Parent->DeviceNameComboBox->ItemIndex = GpuIndex;
           }

         void SetCurrent(void)
           {
              LoadCurrent();
           }

         bool HasChanged(void)
           {
              return Parent->AutoSaveSAPCBox->Checked != AutoSaveSAP
                  || Parent->AutoSavePreferencesCBox->Checked != AutoSavePreferences
                  || Parent->CursorCBox->Checked != CursorIsAnArrow

                  || Parent->EnableFrameCacheCheckBox->Checked != DpxReadCacheEnable
                  || Parent->FrameCacheSizeTrackEdit->GetValue() != DpxReadCacheSizeInGB

                  || Parent->BorderWidthSlider->Position != BorderWidthValue

                  || Parent->SlowSpinEdit->GetValue() != SlowDisplaySpeedValue
                  || Parent->MediumSpinEdit->GetValue() != MediumDisplaySpeedValue
                  || Parent->FastSpinEdit->GetValue() != FastDisplaySpeedValue

                  || Parent->GammaSlider->Position != DisplayGammaValue

                  || Parent->ToolTipsDurationTrackBar->Position != ToolTipsDurationValue;

//                  || Parent->EnableGpuCheckBox->Checked != EnableGpu
//                  || Parent->DeviceNameComboBox->ItemIndex != GpuIndex;
           }

       private:

           bool AutoSaveSAP;
           bool AutoSavePreferences;
           bool CursorIsAnArrow;
           bool Multithread4K;

           bool DpxReadCacheEnable;
           int DpxReadCacheSizeInGB;

           int BorderWidthValue;

           int SlowDisplaySpeedValue;
           int MediumDisplaySpeedValue;
           int FastDisplaySpeedValue;

           int DisplayGammaValue;
           int ToolTipsDurationValue;

           bool EnableGpu;
           int GpuIndex;
           TPreferencesDlg *Parent;
       };

     CValues *OriginalValues;
     CValues *TempValues;

     bool _cacheSizeTrackEditHasBeenInitialized = false;

	  void SetFrameCacheSize();
     void UpdateCacheCapacityStatusLine();
     void updateGpuPage();

public:		// User declarations
    __fastcall TPreferencesDlg(TComponent* Owner);
    __fastcall ~TPreferencesDlg(void);

    virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
    virtual bool ReadDesignSettings(void);

//    TSpinEditFrame *SlowSpinEdit;
//    TSpinEditFrame *MediumSpinEdit;
//    TSpinEditFrame *FastSpinEdit;
};
//---------------------------------------------------------------------------
extern PACKAGE TPreferencesDlg *PreferencesDlg;
//---------------------------------------------------------------------------
#endif
