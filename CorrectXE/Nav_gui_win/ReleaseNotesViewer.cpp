//---------------------------------------------------------------------------

#pragma hdrstop

#include "ReleaseNotesViewer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#include "FileSweeper.h"
#include "IniFile.h"
//#include "MTIDialogs.h"
//#include "MTIio.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

// Fire up the default text viewer to show the release notes.
int ReleaseNotesViewer::show()
{
	// Show the User Manual in the default plain text (.txt file) viewer.
	string releaseNotesPath = GetFilePath(GetApplicationName()) + DEFAULT_RELEASE_NOTES_NAME;
	ExpandEnviornmentString(releaseNotesPath);
	HINSTANCE handle = ShellExecute(
									NULL,
									"open",
									(LPCTSTR) releaseNotesPath.c_str(), // document to launch
									NULL,                    // parms -- not used  when launching a document
									NULL,                    // default dir (don't care here)
									SW_SHOWNORMAL
	);

	// handle >= 32 is good; 0 <= handle < 32 is an error code.
	int retVal = 0;
	if (handle == (HINSTANCE) 0)
	{
		retVal = 100000;
	}
	else if (handle < (HINSTANCE) 32)
	{
		retVal = 100000 + (int) handle;
	}

	return retVal;
}


