//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GoToFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TGoToFrameForm *GoToFrameForm;
//---------------------------------------------------------------------------

__fastcall TGoToFrameForm::TGoToFrameForm(TComponent* Owner)
   : TForm(Owner), m_triggerCharacter('\0')
{
}
//---------------------------------------------------------------------------

void TGoToFrameForm::SetTriggerCharacter(char c)
{
   m_triggerCharacter = c;
}
//---------------------------------------------------------------------------

void TGoToFrameForm::SetTimecode(PTimecode timecode)
{
   CurrentTCVTimecodeEdit->tcP = timecode;
}
//---------------------------------------------------------------------------

PTimecode TGoToFrameForm::GetTimecode()
{
   return CurrentTCVTimecodeEdit->tcP;
}
//---------------------------------------------------------------------------

string  TGoToFrameForm::GetPreConformedTimecodeWidgetString()
{
   return preconformedTcString;
}
//---------------------------------------------------------------------------

void __fastcall TGoToFrameForm::FormCreate(TObject *Sender)
{
   // Frickin VTimecodeEdit turns this on in its constructor!!
   CurrentTCVTimecodeEdit->AutoSelect = false;
}
//---------------------------------------------------------------------------

void __fastcall TGoToFrameForm::FormShow(TObject *Sender)
{
   CurrentTCVTimecodeEdit->Text = (m_triggerCharacter == '\0')
                                  ? ""
                                  : AnsiString(m_triggerCharacter);
   m_triggerCharacter = '\0';

   CurrentTCVTimecodeEdit->SetFocus();
   Application->ProcessMessages();
   CurrentTCVTimecodeEdit->SelStart = 1;
   CurrentTCVTimecodeEdit->SelLength = 0;
}
//---------------------------------------------------------------------------

void __fastcall TGoToFrameForm::CurrentTCVTimecodeEditEscapeKey(TObject *Sender)
{
   CurrentTCVTimecodeEdit->VExit(0);
   ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TGoToFrameForm::CurrentTCVTimecodeEditEnterKey(TObject *Sender)
{
   // Need to do this BEFOR VExit()!
   preconformedTcString = CurrentTCVTimecodeEdit->tcP.getPreConformedString();
   CurrentTCVTimecodeEdit->VExit(0);
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------


