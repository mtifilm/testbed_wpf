//---------------------------------------------------------------------------

#ifndef GoToFrameUnitH
#define GoToFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "VTimeCodeEdit.h"
//---------------------------------------------------------------------------
class TGoToFrameForm : public TForm
{
__published:	// IDE-managed Components
   VTimeCodeEdit *CurrentTCVTimecodeEdit;
   TButton *OkButton;
   TButton *CancelButton;
   void __fastcall FormShow(TObject *Sender);
   void __fastcall CurrentTCVTimecodeEditEscapeKey(TObject *Sender);
   void __fastcall CurrentTCVTimecodeEditEnterKey(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
   char m_triggerCharacter;
   string preconformedTcString;
public:		// User declarations
   __fastcall TGoToFrameForm(TComponent* Owner);
   void SetTriggerCharacter(char c);
   void SetTimecode(PTimecode timecode);
   PTimecode GetTimecode();
   string GetPreConformedTimecodeWidgetString();
};
//---------------------------------------------------------------------------
extern PACKAGE TGoToFrameForm *GoToFrameForm;
//---------------------------------------------------------------------------
#endif
