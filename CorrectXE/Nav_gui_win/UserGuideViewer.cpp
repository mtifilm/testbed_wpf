//---------------------------------------------------------------------------

#pragma hdrstop

#include "UserGuideViewer.h"

#include "FileSweeper.h"
#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTIio.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

string UserGuideViewer::_ActiveToolName;

// show() is different from showAndGoToSection() because we tell ShellExecute
// to run the default PDF viewer, not specifically Adobe Acrobat Reader.
int UserGuideViewer::show()
{
	// Show the User Manual in the default PDF viewer.
//	string userManualPath = DEFAULT_USER_MANUAL_PATH;
	string userManualPath = GetFilePath(GetApplicationName()) + DEFAULT_USER_MANUAL_NAME;
	ExpandEnviornmentString(userManualPath);
	HINSTANCE handle = ShellExecute(
									NULL,
									"open",
									(LPCTSTR) userManualPath.c_str(), // document to launch
									NULL,                    // parms -- not used  when launching a document
									NULL,                    // default dir (don't care here)
									SW_SHOWNORMAL
	);

	// handle >= 32 is good; 0 <= handle < 32 is an error code.
	int retVal = 0;
	if (handle == (HINSTANCE) 0)
	{
		retVal = -1;
	}
	else if (handle < (HINSTANCE) 32)
	{
		retVal = (int) handle;
	}

	return retVal;
}

int UserGuideViewer::showAndGoToToolPage()
{
	auto retVal =showAndGoToSection(_ActiveToolName);

	if (retVal != 0)
	{
		MTIostringstream os;
		os << "Failed to open AcroRd32.exe\n" << GetSystemErrorMessage(retVal);
        os << "\nUsing default windows PDF reader";
		TRACE_0(errout << os.str());
		if (_firstTime)
		{
		   _MTIErrorDialog(os.str());
		}

        show();
		_firstTime = false;
	}

    return retVal;
}

int UserGuideViewer::showAndGoToSection(const string &sectionName)
{
	// Show the User Manual in Adobe Acrobat Reader, so we can jump to a page.
	// string userGuidePath = DEFAULT_USER_MANUAL_PATH;
	auto userGuidePath = GetFilePath(GetApplicationName()) + DEFAULT_USER_MANUAL_NAME;
	ExpandEnviornmentString(userGuidePath);
	string userGuideIndexPath = GetFilePath(userGuidePath) + "UserGuideIndex.ini";
	int page = 1;
	CIniFile *indexFile = CreateIniFile(userGuideIndexPath, true);  // R/O
	if (indexFile)
	{
		page = (int)indexFile->ReadInteger("Index", sectionName, page);
		DeleteIniFile(indexFile);
		indexFile = nullptr;
	}

	MTIostringstream argStream;
	argStream << "/N /A page=" << page << " \"" << userGuidePath << "\"";

	HINSTANCE handle = ShellExecute(
								 NULL,
								 "open",
								 "AcroRd32.exe",
								 argStream.str().c_str(),
								 NULL,                    // default dir (don't care here)
								 SW_SHOWNORMAL
	);

	// handle >= 32 is good; 0 <= handle < 32 is an error code.
	int retVal = 0;
	if (handle == (HINSTANCE) 0)
	{
		retVal = -1;
	}
	else if (handle < (HINSTANCE) 32)
	{
		retVal = (int) handle;
	}


	return retVal;
}

void UserGuideViewer::setActiveToolName(const string &toolName)
{
	_ActiveToolName = toolName;
}

