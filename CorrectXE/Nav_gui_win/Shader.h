#ifndef ShaderH
#define ShaderH

#include "GLError.h"
#include <string>
using std::string;


class Shader
{
public:

	///////////////
	// Constructor reads and builds the shader.
	Shader(const string &vertexCode, const string &fragmentCode)
	{
		const char* vShaderCode = vertexCode.c_str();
		const char* fShaderCode = fragmentCode.c_str();
		char infoLog[1001];

		// Compile shaders.
		unsigned int vertex, fragment;
		int success;

		// vertex Shader
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		check_gl_error();
		glCompileShader(vertex);
		check_gl_error();

		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertex, 1001, NULL, infoLog);
			_gotAnError = true;
			_errorMessage = string("ERROR: VERTEX SHADER COMPILATION FAILED: ") + string(infoLog);
			return;
		};

		// similiar for Fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		check_gl_error();
		glCompileShader(fragment);
		check_gl_error();

		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragment, 1001, NULL, infoLog);
			_gotAnError = true;
			_errorMessage = string("ERROR: FRAGMENT SHADER COMPILATION FAILED: ") + string(infoLog);
			return;
		};

		// Link the shader Program.
		_Id = glCreateProgram();
		glAttachShader(_Id, vertex);
		check_gl_error();
		glAttachShader(_Id, fragment);
		check_gl_error();
		glLinkProgram(_Id);
		check_gl_error();
		glGetProgramiv(_Id, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(_Id, 1024, NULL, infoLog);
			_gotAnError = true;
			_errorMessage = string("ERROR: SHADER PROGRAM CREATE FAILED: ") + string(infoLog);
			return;
		}

		// delete the shaders as they're linked into our program now and no longer necessery
		glDeleteShader(vertex);
		check_gl_error();
		glDeleteShader(fragment);
		check_gl_error();
	};

	// Get the program ID.
	unsigned int getProgramId()
	{
		return _Id;
	}

	// use/activate the shader
	void use()
	{
		glUseProgram(_Id);
	}

	// utility uniform functions
	void setBool(const std::string &name, bool value) const
	{
		glUniform1i(glGetUniformLocation(_Id, name.c_str()), (int)value);
	}

	void setInt(const std::string &name, int value) const
	{
		glUniform1i(glGetUniformLocation(_Id, name.c_str()), value);
	}

	void setFloat(const std::string &name, float value) const
	{
		glUniform1f(glGetUniformLocation(_Id, name.c_str()), value);
	}

	void setVec3(const std::string &name, float value1, float value2, float value3) const
	{
		glUniform3f(glGetUniformLocation(_Id, name.c_str()), value1, value2, value3);
	}

private:

	// The program ID.
	unsigned int _Id = -1;

	// The error flag.
	bool _gotAnError = false;

	// The error message.
	string _errorMessage;

//	// utility function for checking shader compilation/linking errors.
//	// ------------------------------------------------------------------------
//	void checkCompileErrors(unsigned int shader, std::string type)
//	{
//		int success;
//		char infoLog[1024];
//		if (type != "PROGRAM")
//		{
//			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
//			if (!success)
//			{
//				glGetShaderInfoLog(shader, 1024, NULL, infoLog);
//				std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
//			}
//		}
//		else
//		{
//			glGetProgramiv(shader, GL_LINK_STATUS, &success);
//			if (!success)
//			{
//				glGetProgramInfoLog(shader, 1024, NULL, infoLog);
//				std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
//			}
//		}
//	}
};
#endif