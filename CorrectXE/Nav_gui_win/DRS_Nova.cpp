//-------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("CustomReticleUnit.cpp", CustomReticleDlg);
USEFORM("BookmarkEventViewerUnit.cpp", BookmarkEventViewerForm);
USEFORM("BaseToolUnit.cpp", BaseToolWindow);
USEFORM("AcceptOrRejectUnit.cpp", AcceptOrRejectForm);
USEFORM("AboutUnit.cpp", AboutForm);
USEFORM("PreferencesUnit.cpp", PreferencesDlg);
USEFORM("NoUndoDialogUnit.cpp", NoUndoForm);
USEFORM("MutexFailNotifierUnit.cpp", MutexFailNotifierForm);
USEFORM("MaskPropertiesFrame.cpp", Frame2); /* TFrame: File Type */
USEFORM("TimelineFrameUnit.cpp", Frame1); /* TFrame: File Type */
USEFORM("SplashScreenUnit.cpp", SplashScreen);
USEFORM("LoupeWindowUnit.cpp", LoupeWindow);
USEFORM("LicensingUnit.cpp", LicensingForm);
USEFORM("GpuWarningUnit.cpp", gpuWarningForm);
USEFORM("GpuDumpFormUnit.cpp", GpuDumpForm);
USEFORM("GoToFrameUnit.cpp", GoToFrameForm);
USEFORM("ManageMediaUnit.cpp", ManageMediaForm);
USEFORM("MainWindowUnit.cpp", mainWindow);
USEFORM("LutSettingsEditor.cpp", LutSettingsEditor);
USEFORM("LUTNameUnit.cpp", LUTNameForm);
USEFORM("..\Common_gui_win\SpinEditFrameUnit.cpp", SpinEditFrame); /* TFrame: File Type */
USEFORM("..\Common_gui_win\PresetsUnit.cpp", PresetsFrame); /* TFrame: File Type */
USEFORM("..\Common_gui_win\ExecButtonsFrameUnit.cpp", ExecButtonsFrame); /* TFrame: File Type */
USEFORM("..\MtiComponents\Repository\MTIUNIT.cpp", MTIForm);
USEFORM("..\MasterTimelineWin\RefTimeLineFrame.cpp", FRefTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\MasterTimeLineFrame.cpp", FMasterTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\AnimateTimeLineFrame.cpp", FAnimateTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\GroupTimeLineFrame.cpp", FGroupTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\EventTimeLineFrame.cpp", FEventTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\BookmarkTimeLineFrame.cpp", BookmarkTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\BaseTimeLineFrame.cpp", FBaseTimeLine); /* TFrame: File Type */
USEFORM("..\MasterTimelineWin\AudioTimeLineFrame.cpp", FAudioTimeLine); /* TFrame: File Type */
//---------------------------------------------------------------------------
#include "MainWindowUnit.h"
#include "SplashScreenUnit.h"
#include "MTIio.h"
#include "MTIsleep.h"
#include "MTIWinInterface.h"
#include "ShowModalDialog.h"
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	// Make sure another instance is not running....
	HANDLE hMutex = CreateMutex(NULL, TRUE, "DRS_NOVA_MUTEX");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		NotifyProgramRunning(false);
		CloseHandle(hMutex);
		Application->Initialize();
		Application->Title = "DRS Nova Execution Fail";
		Application->CreateForm(__classid(TMutexFailNotifierForm), &MutexFailNotifierForm);

		  Application->Run();
		  return 0;
	}

	try
	{
		Application->Initialize();
		SetApplicationPointer((void *)(StringToStdString(Application->ExeName).c_str()));
		Application->MainFormOnTaskBar = true;
		Application->Title = "DRS Nova by MTI Film";
		Application->HintHidePause = 30000;
		TStyleManager::TrySetStyle("MTI Dark");
       Application->CreateForm(__classid(TmainWindow), &mainWindow);
       Application->CreateForm(__classid(TSplashScreen), &SplashScreen);
       Application->CreateForm(__classid(TPreferencesDlg), &PreferencesDlg);
       Application->CreateForm(__classid(TCustomReticleDlg), &CustomReticleDlg);
       Application->CreateForm(__classid(TBaseToolWindow), &BaseToolWindow);
       Application->CreateForm(__classid(TAcceptOrRejectForm), &AcceptOrRejectForm);
       Application->CreateForm(__classid(TLutSettingsEditor), &LutSettingsEditor);
       Application->CreateForm(__classid(TLoupeWindow), &LoupeWindow);
       Application->CreateForm(__classid(TAboutForm), &AboutForm);
       Application->CreateForm(__classid(TBookmarkEventViewerForm), &BookmarkEventViewerForm);
       Application->CreateForm(__classid(TGoToFrameForm), &GoToFrameForm);
       Application->CreateForm(__classid(TLicensingForm), &LicensingForm);
       Application->CreateForm(__classid(TPresetsFrame), &PresetsFrame);
       Application->CreateForm(__classid(TSpinEditFrame), &SpinEditFrame);
       NotifyProgramRunning();

		// Delayed abort via splash screen timer if not authorized,
		// because if you never actually 'Run' the app, it seems that
		// you can't terminate it!!
		SplashScreen->StartTimer(1.5, false); // seconds, no self-destruct

		PostMessage(mainWindow->Handle, CW_START_PROGRAM, 0, 0);

		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("Non-execption failure");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------

//////////////// Stuff that keeps getting deleted. //////////////////////////
/*
USEFORM("MutexFailNotifierUnit.cpp", MutexFailNotifierForm);
*/
/*
THIS GOES AFTER "Application->CreateForm(__classid(TMutexFailNotifierForm), &MutexFailNotifierForm);"

		  Application->Run();
		  return 0;
	}

	try
	{
		Application->Initialize();
		SetApplicationPointer((void *)(StringToStdString(Application->ExeName).c_str()));
		Application->MainFormOnTaskBar = true;
		Application->Title = "DRS Nova by MTI Film";
		Application->HintHidePause = 30000;
		TStyleManager::TrySetStyle("MTI Dark");
*/
//---------------------------------------------------------------------------

