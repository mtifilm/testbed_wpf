// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MaskPropertiesFrame.h"
#include "MaskTool.h"
#include "MainWindowUnit.h"
#include "Displayer.h"
#include "BaseToolUnit.h"
#include "JobManager.h"
#include "BezierEditor.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "MTIUNIT"
#pragma link "TrackEditFrameUnit"
#pragma link "CompactTrackEditFrameUnit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma link "CompactTrackEditFrameUnit"
#pragma resource "*.dfm"
TMaskPropertiesFrame *MaskPropertiesFrame;

// ---------------------------------------------------------------------------
__fastcall TMaskPropertiesFrame::TMaskPropertiesFrame(TComponent* Owner) : TFrame(Owner)
{
	InteriorBlendTrackEdit->SetValue(0);
	InteriorBlendTrackEdit->SetMinAndMax(0, 200);
	InteriorBlendTrackEdit->Enable(true);

	ExteriorBlendTrackEdit->SetValue(0);
	ExteriorBlendTrackEdit->SetMinAndMax(0, 200);
	ExteriorBlendTrackEdit->Enable(true);
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::FormShow(TObject *Sender)
{
	// Save the settings in case we cancel instead of accepting
   auto blendIn = InteriorBlendTrackEdit->GetValue();
   auto blendOut = ExteriorBlendTrackEdit->GetValue();

	mainWindow->GetDisplayer()->setDisplayMaskOverlay(true);
	mainWindow->GetDisplayer()->displayFrame();

	// This doesn't belong here but screw it, it works
	if (GBaseTool == nullptr)
	{
		return;
	}

	auto baseToolForm = dynamic_cast<TBaseToolWindow*>(GBaseTool->getSystemAPI()->getBaseToolForm());
	if (baseToolForm == nullptr)
	{
		return;
	}

	baseToolForm->Mask_ShowOrHidePropertiesPanelButton->Down = true;
	setGuiFromMask();
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	// set fVisible to false, otherwise TMTIForm opens this form
	// the next time the application is started
	// fVisible = false;
	return;
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::RevertlButtonClick(TObject *Sender)
{
	// Restore the settings
	InteriorBlendTrackEdit->SetValue(getSavedInclusionBorderWidth());
	ExteriorBlendTrackEdit->SetValue(getSavedExclusionBorderWidth());
   	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}

// ---------------------------------------------------------------------------

void TMaskPropertiesFrame::GetSettings(CMaskTool &maskTool)
{
	// Exclusion and exclusion must be the same
   auto blendIn = InteriorBlendTrackEdit->GetValue();
   auto blendOut = ExteriorBlendTrackEdit->GetValue();
	maskTool.setDefaultInclusionBorderWidth(blendIn);
	maskTool.setDefaultExclusionBorderWidth(blendOut);

   auto value = float(blendIn + blendOut);
   if (value != 0)
   {
      value = (blendIn - blendOut) / value;
   }

	maskTool.setDefaultInclusionBlendSlew(value);
	maskTool.setDefaultExclusionBlendSlew(value);
}

void TMaskPropertiesFrame::UpdateSettings(CMaskTool &maskTool)
{
	// Update Inclusion Region Settings
	InteriorBlendTrackEdit->SetValue(maskTool.getDefaultInclusionBorderWidth());
	ExteriorBlendTrackEdit->SetValue(maskTool.getDefaultExclusionBorderWidth());
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::InclusiveTrackEditNotifyWidgetClick(TObject *Sender) {
	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::BlendPositionRadioGroupClick(TObject *Sender)

{mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);}
// ---------------------------------------------------------------------------

void TMaskPropertiesFrame::setGuiFromMask()
{
	if (GBaseTool == nullptr)
	{
		return;
	}

	auto baseToolForm = dynamic_cast<TBaseToolWindow*>(GBaseTool->getSystemAPI()->getBaseToolForm());
	if (baseToolForm == nullptr)
	{
		return;
	}

	baseToolForm->Mask_ShowOrHidePropertiesPanelButton->Down = isMaskFrameEnabled();
	mainWindow->GetDisplayer()->setDisplayMaskOverlay(isMaskFrameEnabled() && ShowInteriorCheckBox->Checked);
}

void TMaskPropertiesFrame::processToolCommand(int command)
{
	auto baseToolForm = dynamic_cast<TBaseToolWindow*>(GBaseTool->getSystemAPI()->getBaseToolForm());
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();

	switch (command)
	{
	case NAV_CMD_MASK_INCLUDE:
		break;

	case NAV_CMD_MASK_EXCLUDE:
		break;

	case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
		setGuiFromMask();
		break;

	case NAV_CMD_MASK_SHOW_OUTLINE:
		setGuiFromMask();
		break;

	case NAV_CMD_MASK_TOOL_ENABLE:
		break;

	case NAV_CMD_MASK_TOOL_DISABLE:
		break;

	default:
		return;
	}
}

void __fastcall TMaskPropertiesFrame::FormHide(TObject *Sender)
{
	if (GBaseTool == nullptr)
	{
		return;
	}

	auto baseToolForm = dynamic_cast<TBaseToolWindow*>(GBaseTool->getSystemAPI()->getBaseToolForm());
	if (baseToolForm == nullptr)
	{
		return;
	}

	baseToolForm->Mask_ShowOrHidePropertiesPanelButton->Down = false;
	mainWindow->GetDisplayer()->setDisplayMaskOverlay(false);
	mainWindow->GetDisplayer()->displayFrame();
}
// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)

{
	// The track bar widgets get first crack at the keys.
	if (TCompactTrackEditFrame::ProcessKeyUp(Key, Shift) || TTrackEditFrame::ProcessKeyUp(Key, Shift))
	{
		// Consumed by the widget
		Key = 0;
		return;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::CenterButtonClick(TObject *Sender)
{
   auto blendIn = InteriorBlendTrackEdit->GetValue();
   auto blendOut = ExteriorBlendTrackEdit->GetValue();
   auto value = (blendIn + blendOut)/2;
	InteriorBlendTrackEdit->SetValue(value);
   ExteriorBlendTrackEdit->SetValue(value);
	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}
// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::ExteriorButtonClick(TObject *Sender)
{
   auto blendIn = InteriorBlendTrackEdit->GetValue();
   auto blendOut = ExteriorBlendTrackEdit->GetValue();
	InteriorBlendTrackEdit->SetValue(0);
   ExteriorBlendTrackEdit->SetValue(blendIn);
	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}
// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::InteriorButtonClick(TObject *Sender)
{
   auto blendIn = InteriorBlendTrackEdit->GetValue();
   auto blendOut = ExteriorBlendTrackEdit->GetValue();;
	InteriorBlendTrackEdit->SetValue(blendOut);
   ExteriorBlendTrackEdit->SetValue(0);
	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}
// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::InclusiveSlewTrackEditNotifyWidgetClick(TObject *Sender)

{
   // Exterior track edit.
   mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}
// ---------------------------------------------------------------------------


void __fastcall TMaskPropertiesFrame::ShowInteriorCheckBoxClick(TObject *Sender)
{
	mainWindow->GetDisplayer()->setDisplayMaskOverlay(ShowInteriorCheckBox->Checked);
  	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();
	maskTool->setRegionOfInterestAvailable(ShowInteriorCheckBox->Checked);
  	mainWindow->ExecuteNavigatorToolCommand(NAV_CMD_MASK_CHANGE_BORDER);
}

// ---------------------------------------------------------------------------

void TMaskPropertiesFrame::saveMask()
{
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();

	// If no mask, then we're outta here
	if (!maskTool->IsMaskAvailable())
	{
		return;
	}

	// Ask the job manager to suggest where to store the mask to.
	JobManager jobManager;
	string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::masksType);
	string suggestedFilename = jobManager.GetJobFolderFileSaveName(suggestedFolder, "", false, "msk");

	MaskSaveDialog->InitialDir = suggestedFolder.c_str();
	MaskSaveDialog->FileName = suggestedFilename.c_str();

	// Always ask user for file name.  Should default to last used file
	bool retVal = MaskSaveDialog->Execute();
	if (!retVal)
	{
		return;
	}

	maskTool->SaveMaskToFile(StringToStdString(MaskSaveDialog->FileName));
}
// ---------------------------------------------------------------------------

void TMaskPropertiesFrame::loadMask()
{
	CMaskTool *maskTool = CNavigatorTool::GetMaskTool();

	// Query if there is already a mask
	// TBD
	//

	// Ask the job manager to suggest where to load the mask from.
	JobManager jobManager;
	string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::masksType);
	string suggestedFilename = jobManager.GetJobFolderFileLoadName(suggestedFolder, "", false, "msk");
	MaskOpenDialog->InitialDir = suggestedFolder.c_str();
	MaskOpenDialog->FileName = suggestedFilename.c_str();

	bool retVal = MaskOpenDialog->Execute();
	if (!retVal)
	{
		return;
	}

	maskTool->LoadMaskFromFile(StringToStdString(MaskOpenDialog->FileName));

}

void __fastcall TMaskPropertiesFrame::LoadMaskButtonClick(TObject *Sender)
{
	// Just load mask
	loadMask();
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::SaveMaskButtonClick(TObject *Sender)
{
	// Just save mask
	saveMask();
}

// ---------------------------------------------------------------------------

void __fastcall TMaskPropertiesFrame::MaskChangeTimerTimer(TObject *Sender)
{
	// This is really ugly, but its too complicated to have
	// everyplace call here.
	auto maskTool = CNavigatorTool::GetMaskTool();
	SaveMaskButton->Enabled = maskTool->IsMaskAvailable();
}
// ---------------------------------------------------------------------------
