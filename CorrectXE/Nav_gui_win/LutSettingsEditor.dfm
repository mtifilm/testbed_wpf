object LutSettingsEditor: TLutSettingsEditor
  Left = 1070
  Top = 122
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'LUT Settings Editor'
  ClientHeight = 642
  ClientWidth = 329
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnShow = FormShow
  DesignSize = (
    329
    642)
  PixelsPerInch = 96
  TextHeight = 13
  object SourceRangeExrGroup: TGroupBox
    Left = 14
    Top = 200
    Width = 306
    Height = 128
    Anchors = [akBottom]
    Caption = ' Source Range  '
    TabOrder = 10
    DesignSize = (
      306
      128)
    object SourceRangeExrDefaultRadioButton: TRadioButton
      Left = 17
      Top = 20
      Width = 58
      Height = 17
      Caption = 'Default'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = SourceRangeExrRadioButtonClick
    end
    object SourceRangeExrCustomRadioButton: TRadioButton
      Left = 133
      Top = 20
      Width = 58
      Height = 17
      Caption = 'Custom'
      TabOrder = 1
      OnClick = SourceRangeExrRadioButtonClick
    end
    object SourceRangeExrSliderPanel: TPanel
      Left = 0
      Top = 47
      Width = 303
      Height = 74
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        303
        74)
      object DynamicRangeLabel: TLabel
        Left = 12
        Top = 40
        Width = 76
        Height = 13
        Caption = 'Dynamic Range'
      end
      object ExposureAdjustLabel: TLabel
        Left = 12
        Top = 12
        Width = 76
        Height = 13
        Caption = 'Exposure Adjust'
      end
      object ExposureAdjustReadoutLabel: TLabel
        Left = 107
        Top = 11
        Width = 20
        Height = 15
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '0.0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DynamicRangeReadoutLabel: TLabel
        Left = 99
        Top = 39
        Width = 28
        Height = 15
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '15.0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ExposureAdjustTrackBar: TTrackBar
        Left = 128
        Top = 10
        Width = 174
        Height = 24
        Anchors = [akTop, akRight]
        Max = 70
        Min = -150
        PageSize = 10
        TabOrder = 0
        ThumbLength = 15
        TickStyle = tsManual
        OnChange = SourceRangeExrTrackBarChange
      end
      object DynamicRangeTrackBar: TTrackBar
        Left = 128
        Top = 40
        Width = 174
        Height = 24
        Anchors = [akTop, akRight]
        Max = 450
        Min = 70
        PageSize = 10
        Position = 150
        TabOrder = 1
        ThumbLength = 15
        TickStyle = tsManual
        OnChange = SourceRangeExrTrackBarChange
      end
    end
  end
  object SourceRangeGroup: TGroupBox
    Left = 12
    Top = 198
    Width = 306
    Height = 128
    Anchors = [akBottom]
    Caption = ' Source Range  '
    TabOrder = 1
    DesignSize = (
      306
      128)
    object SourceRangeDefaultsPanel: TPanel
      Left = 8
      Top = 47
      Width = 270
      Height = 78
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 3
      DesignSize = (
        270
        78)
      object DefaultBlackLabel: TLabel
        Left = 13
        Top = 21
        Width = 27
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Black'
      end
      object DefaultBVLabel: TLabel
        Left = 225
        Top = -1
        Width = 7
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'B'
      end
      object DefaultGULabel: TLabel
        Left = 155
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'G'
      end
      object DefaultRYLabel: TLabel
        Left = 85
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'R'
      end
      object DefaultWhiteLabel: TLabel
        Left = 13
        Top = 51
        Width = 28
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'White'
      end
      object DefaultWhiteRYEdit: TEdit
        Left = 69
        Top = 49
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 0
        Text = '12345'
      end
      object DefaultWhiteGUEdit: TEdit
        Left = 139
        Top = 49
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 1
        Text = '12345'
      end
      object DefaultWhiteBVEdit: TEdit
        Left = 209
        Top = 49
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        Text = '12345'
      end
      object DefaultBlackRYEdit: TEdit
        Left = 69
        Top = 19
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 3
        Text = '12345'
      end
      object DefaultBlackGUEdit: TEdit
        Left = 139
        Top = 19
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        Text = '12345'
      end
      object DefaultBlackBVEdit: TEdit
        Left = 209
        Top = 19
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 5
        Text = '12345'
      end
    end
    object SourceRangeDefaultRadioButton: TRadioButton
      Left = 17
      Top = 20
      Width = 58
      Height = 17
      Hint = 'Use the source range that was determined from the media metadata'
      Caption = 'Default'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = SourceRangeRadioButtonClick
    end
    object SourceRangeCustomRadioButton: TRadioButton
      Left = 133
      Top = 20
      Width = 58
      Height = 17
      Hint = 'Override the default source range'
      Caption = 'Custom'
      TabOrder = 1
      OnClick = SourceRangeRadioButtonClick
    end
    object MonoSourceRangeGBCoverUpPanel: TPanel
      Left = 132
      Top = 47
      Width = 147
      Height = 78
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 4
      DesignSize = (
        147
        78)
      object GMaxDisabledEdit: TEdit
        Left = 16
        Top = 49
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object BMaxDisabledEdit: TEdit
        Left = 86
        Top = 49
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 1
      end
      object GMinDisabledEdit: TEdit
        Left = 16
        Top = 19
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 2
      end
      object BMinDisabledEdit: TEdit
        Left = 86
        Top = 19
        Width = 41
        Height = 21
        TabStop = False
        Alignment = taCenter
        Anchors = [akTop, akRight]
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 3
      end
    end
    object SourceRangeSettingsPanel: TPanel
      Left = 8
      Top = 47
      Width = 270
      Height = 74
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        270
        74)
      object SourceRangeMaxLockButton: TSpeedButton
        Left = 52
        Top = 53
        Width = 12
        Height = 12
        Hint = 'Use same maximum value for all three components'
        AllowAllUp = True
        Anchors = [akTop, akRight]
        GroupIndex = 2
        Glyph.Data = {
          E6010000424DE601000000000000360000002800000010000000090000000100
          180000000000B0010000C40E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000
          0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
          525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
          6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A000000
          0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
          525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
          6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A6A6A6A
          0000006A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A
          6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A0000006A6A6A6A
          6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
          3A3A3A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6060605252525252
          526060606A6A6A6A6A6A}
        NumGlyphs = 2
        OnClick = SourceRangeMaxLockButtonClick
      end
      object SourceRangeMinLabel: TLabel
        Left = 13
        Top = 21
        Width = 17
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Min'
      end
      object SourceRangeMinLockButton: TSpeedButton
        Left = 52
        Top = 23
        Width = 12
        Height = 12
        Hint = 'Use same minimum value for all three components'
        AllowAllUp = True
        Anchors = [akTop, akRight]
        GroupIndex = 1
        Glyph.Data = {
          E6010000424DE601000000000000360000002800000010000000090000000100
          180000000000B0010000C40E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000
          0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
          525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
          6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A000000
          0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
          525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
          6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A6A6A6A
          0000006A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A
          6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A0000006A6A6A6A
          6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
          3A3A3A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6060605252525252
          526060606A6A6A6A6A6A}
        NumGlyphs = 2
        OnClick = SourceRangeMinLockButtonClick
      end
      object SourceRangeBVLabel: TLabel
        Left = 225
        Top = -1
        Width = 7
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'B'
      end
      object SourceRangeGULabel: TLabel
        Left = 155
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'G'
      end
      object SourceRangeRYLabel: TLabel
        Left = 85
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'R'
      end
      object SourceRangeMaxLabel: TLabel
        Left = 13
        Top = 51
        Width = 20
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Max'
      end
      inline SourceRangeMinRYSpinEdit: TSpinEditFrame
        Left = 69
        Top = 19
        Width = 55
        Height = 22
        Hint = 'Minimum RED value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 69
        ExplicitTop = 19
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
      inline SourceRangeMinGUSpinEdit: TSpinEditFrame
        Left = 139
        Top = 18
        Width = 55
        Height = 22
        Hint = 'Minimum GREEN value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 139
        ExplicitTop = 18
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
      inline SourceRangeMinBVSpinEdit: TSpinEditFrame
        Left = 209
        Top = 19
        Width = 55
        Height = 22
        Hint = 'Minimum BLUE value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 209
        ExplicitTop = 19
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
      inline SourceRangeMaxRYSpinEdit: TSpinEditFrame
        Left = 69
        Top = 49
        Width = 55
        Height = 22
        Hint = 'Maximum RED value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 69
        ExplicitTop = 49
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
      inline SourceRangeMaxGUSpinEdit: TSpinEditFrame
        Left = 139
        Top = 49
        Width = 55
        Height = 22
        Hint = 'Maximum GREEN value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 139
        ExplicitTop = 49
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
      inline SourceRangeMaxBVSpinEdit: TSpinEditFrame
        Left = 209
        Top = 49
        Width = 55
        Height = 22
        Hint = 'Maximum BLUE value'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnEnter = SpinEditEnter
        OnExit = SpinEditExit
        ExplicitLeft = 209
        ExplicitTop = 49
        ExplicitWidth = 55
        inherited Edit: TEdit
          Width = 41
          ExplicitWidth = 41
        end
        inherited DownButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
        inherited UpButtonOutlinePanel: TPanel
          Left = 39
          ExplicitLeft = 39
        end
      end
    end
  end
  object SourceColorSpaceGroup: TGroupBox
    Left = 12
    Top = 93
    Width = 306
    Height = 99
    Anchors = [akLeft, akBottom]
    Caption = ' Source Color Space '
    TabOrder = 0
    object ColorSpaceTopRadioPanel: TPanel
      Left = 12
      Top = 20
      Width = 281
      Height = 17
      BevelOuter = bvNone
      TabOrder = 0
      object ColorSpaceDefaultRadioButton: TRadioButton
        Left = 5
        Top = 0
        Width = 88
        Height = 17
        Hint = 'Use the color space that was determined from the media metadata'
        Caption = 'Default'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = ColorSpaceDefaultOrCustomRadioButtonClick
      end
      object ColorSpaceCustomRadioButton: TRadioButton
        Left = 121
        Top = 0
        Width = 58
        Height = 17
        Hint = 'Override the default colorspace'
        Caption = 'Custom'
        TabOrder = 1
        OnClick = ColorSpaceDefaultOrCustomRadioButtonClick
      end
    end
    object ColorSpaceBottomRadioPanel: TPanel
      Left = 8
      Top = 40
      Width = 281
      Height = 56
      BevelOuter = bvNone
      TabOrder = 1
      object CCIR_601MColorSpaceRadioButton: TRadioButton
        Left = 5
        Top = 60
        Width = 104
        Height = 17
        Caption = 'ITU-R BT.601 M'
        TabOrder = 0
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object CCIR_601BGColorSpaceRadioButton: TRadioButton
        Left = 121
        Top = 58
        Width = 152
        Height = 17
        Caption = 'ITU-R BT.601 B/G (Default)'
        TabOrder = 1
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object CCIR_709ColorSpaceRadioButton: TRadioButton
        Left = 121
        Top = 58
        Width = 152
        Height = 17
        Caption = 'ITU-R BT.709'
        TabOrder = 2
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object SMPTE_240ColorSpaceRadioButton: TRadioButton
        Left = 5
        Top = 54
        Width = 148
        Height = 17
        Caption = 'SMPTE 240'
        TabOrder = 3
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object LinearColorSpaceRadioButton: TRadioButton
        Left = 125
        Top = 0
        Width = 96
        Height = 17
        Hint = 'Interpret data as linear color representation'
        Caption = 'Linear (Default)'
        TabOrder = 4
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object LogColorSpaceRadioButton: TRadioButton
        Left = 125
        Top = 18
        Width = 96
        Height = 17
        Hint = 'Interpret data as log color representation'
        Caption = 'Log'
        TabOrder = 5
        OnClick = CustomColorSpaceRadioButtonClick
      end
      object ExrColorSpaceRadioButton: TRadioButton
        Left = 125
        Top = 36
        Width = 96
        Height = 17
        Hint = 'Interpret data as 16-bit float EXR'
        Caption = 'EXR'
        TabOrder = 6
        OnClick = CustomColorSpaceRadioButtonClick
      end
    end
  end
  object DisplayRangeGroup: TGroupBox
    Left = 12
    Top = 332
    Width = 306
    Height = 128
    Anchors = [akBottom]
    Caption = ' Display Range '
    TabOrder = 2
    DesignSize = (
      306
      128)
    object DisplayRangeRLabel: TLabel
      Left = 93
      Top = 45
      Width = 8
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'R'
    end
    object DisplayRangeGLabel: TLabel
      Left = 163
      Top = 45
      Width = 8
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'G'
    end
    object DisplayRangeBLabel: TLabel
      Left = 233
      Top = 45
      Width = 7
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'B'
    end
    object DisplayRangeMinLabel: TLabel
      Left = 21
      Top = 67
      Width = 17
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Min'
    end
    object DisplayRangeMaxLabel: TLabel
      Left = 21
      Top = 97
      Width = 20
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Max'
    end
    object DisplayRangeMinLockButton: TSpeedButton
      Left = 60
      Top = 69
      Width = 12
      Height = 12
      Hint = 'Use same minimum value for all three components'
      AllowAllUp = True
      Anchors = [akLeft, akBottom]
      GroupIndex = 3
      Glyph.Data = {
        E6010000424DE601000000000000360000002800000010000000090000000100
        180000000000B0010000C40E0000C40E000000000000000000006A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000
        0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
        525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
        6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A000000
        0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
        525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
        6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A6A6A6A
        0000006A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A
        6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A0000006A6A6A6A
        6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
        3A3A3A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6060605252525252
        526060606A6A6A6A6A6A}
      NumGlyphs = 2
      OnClick = DisplayRangeMinLockButtonClick
    end
    object DisplayRangeMaxLockButton: TSpeedButton
      Left = 60
      Top = 99
      Width = 12
      Height = 12
      Hint = 'Use same maximum value for all three components'
      AllowAllUp = True
      Anchors = [akLeft, akBottom]
      GroupIndex = 4
      Glyph.Data = {
        E6010000424DE601000000000000360000002800000010000000090000000100
        180000000000B0010000C40E0000C40E000000000000000000006A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
        6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000
        0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
        525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
        6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A000000
        0000000000000000000000000000006A6A6A6A6A6A5252525252525252525252
        525252525252526A6A6A6A6A6A0000000000000000000000000000000000006A
        6A6A6A6A6A5252525252525252525252525252525252526A6A6A6A6A6A6A6A6A
        0000006A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A
        6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A0000006A6A6A6A
        6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
        3A3A3A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6060605252525252
        526060606A6A6A6A6A6A}
      NumGlyphs = 2
      OnClick = DisplayRangeMaxLockButtonClick
    end
    inline DisplayRangeMinRSpinEdit: TSpinEditFrame
      Left = 77
      Top = 65
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 77
      ExplicitTop = 65
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    inline DisplayRangeMinGSpinEdit: TSpinEditFrame
      Left = 147
      Top = 65
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 147
      ExplicitTop = 65
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    inline DisplayRangeMinBSpinEdit: TSpinEditFrame
      Left = 217
      Top = 65
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 217
      ExplicitTop = 65
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    inline DisplayRangeMaxRSpinEdit: TSpinEditFrame
      Left = 77
      Top = 95
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      ExplicitLeft = 77
      ExplicitTop = 95
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    inline DisplayRangeMaxGSpinEdit: TSpinEditFrame
      Left = 147
      Top = 95
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      ExplicitLeft = 147
      ExplicitTop = 95
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    inline DisplayRangeMaxBSpinEdit: TSpinEditFrame
      Left = 217
      Top = 95
      Width = 55
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      ExplicitLeft = 217
      ExplicitTop = 95
      ExplicitWidth = 55
      inherited Edit: TEdit
        Width = 41
        ExplicitWidth = 41
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 39
        ExplicitLeft = 39
      end
    end
    object DisplayRangeRadioButtonPanel: TPanel
      Left = 12
      Top = 20
      Width = 281
      Height = 17
      BevelOuter = bvNone
      TabOrder = 6
      object DisplayRangeDefaultRadioButton: TRadioButton
        Left = 5
        Top = 0
        Width = 88
        Height = 17
        Hint = 'Use the full display output range (0-255)'
        Caption = 'Default'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = DisplayRangeRadioButtonClick
      end
      object DisplayRangeCustomRadioButton: TRadioButton
        Left = 121
        Top = 0
        Width = 58
        Height = 17
        Hint = 'Override the default display output range'
        Caption = 'Custom'
        TabOrder = 1
        OnClick = DisplayRangeRadioButtonClick
      end
    end
    object DisplayRangeDefaultCoverUpPanel: TPanel
      Left = 8
      Top = 46
      Width = 281
      Height = 77
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 7
      DesignSize = (
        281
        77)
      object Label14: TLabel
        Left = 85
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'R'
      end
      object Label15: TLabel
        Left = 155
        Top = -1
        Width = 8
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'G'
      end
      object Label16: TLabel
        Left = 225
        Top = -1
        Width = 7
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'B'
      end
      object Label19: TLabel
        Left = 13
        Top = 21
        Width = 17
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Min'
      end
      object Label20: TLabel
        Left = 13
        Top = 51
        Width = 20
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Max'
      end
      object Edit1: TEdit
        Left = 69
        Top = 19
        Width = 41
        Height = 21
        Hint = 'Minimum display RED value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 0
        Text = '0'
      end
      object Edit2: TEdit
        Left = 139
        Top = 18
        Width = 41
        Height = 21
        Hint = 'Minimum display GREEN value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 1
        Text = '0'
      end
      object Edit3: TEdit
        Left = 209
        Top = 19
        Width = 41
        Height = 21
        Hint = 'Minimum display BLUE value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        Text = '0'
      end
      object Edit4: TEdit
        Left = 209
        Top = 49
        Width = 41
        Height = 21
        Hint = 'Maximum display BLUE value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 3
        Text = '255'
      end
      object Edit5: TEdit
        Left = 139
        Top = 49
        Width = 41
        Height = 21
        Hint = 'Maximum display GREEN value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        Text = '255'
      end
      object Edit6: TEdit
        Left = 69
        Top = 49
        Width = 41
        Height = 21
        Hint = 'Maximum display RED value'
        Alignment = taCenter
        Color = clBtnFace
        Enabled = False
        ReadOnly = True
        TabOrder = 5
        Text = '255'
      end
    end
  end
  object DisplayGammaGroup: TGroupBox
    Left = 12
    Top = 466
    Width = 306
    Height = 91
    Anchors = [akBottom]
    Caption = ' Display Gamma '
    TabOrder = 3
    object DisplayGammaValue: TLabel
      Left = 28
      Top = 54
      Width = 20
      Height = 15
      Caption = '1.7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DisplayGammaSlider: TTrackBar
      Left = 67
      Top = 50
      Width = 208
      Height = 24
      Hint = 'Gamma value'
      Max = 30
      Min = 1
      Position = 1
      TabOrder = 0
      ThumbLength = 15
      OnChange = OnGammaSliderChange
    end
    object DisplayGammaRadioButtonPanel: TPanel
      Left = 12
      Top = 20
      Width = 281
      Height = 17
      BevelOuter = bvNone
      TabOrder = 1
      object DisplayGammaDefaultRadioButton: TRadioButton
        Left = 5
        Top = 0
        Width = 88
        Height = 17
        Hint = 'Use default display gamma value from the Preferences'
        Caption = 'Default'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = DisplayGammaRadioButtonClick
      end
      object DisplayGammaCustomRadioButton: TRadioButton
        Left = 121
        Top = 0
        Width = 58
        Height = 17
        Hint = 'Override the default display gamma value'
        Caption = 'Custom'
        TabOrder = 1
        OnClick = DisplayGammaRadioButtonClick
      end
    end
  end
  object ButtonAreaSeparator: TPanel
    Left = 0
    Top = 600
    Width = 329
    Height = 3
    Anchors = [akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 4
  end
  object LutCancelButton: TButton
    Left = 171
    Top = 609
    Width = 57
    Height = 21
    Anchors = [akBottom]
    Caption = 'Cancel'
    TabOrder = 5
    OnClick = LutCancelButtonClick
  end
  object LutOkButton: TButton
    Left = 101
    Top = 609
    Width = 57
    Height = 21
    Anchors = [akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 6
    OnClick = LutOkButtonClick
  end
  object LutSelectionGroup: TGroupBox
    Left = 12
    Top = 8
    Width = 306
    Height = 79
    Caption = ' LUT Selection '
    TabOrder = 7
    DesignSize = (
      306
      79)
    object ShortcutLabel: TLabel
      Left = 124
      Top = 48
      Width = 40
      Height = 13
      Caption = 'Shortcut'
    end
    object LutNameComboBox: TComboBox
      Left = 13
      Top = 20
      Width = 192
      Height = 21
      Hint = 'Coose a LUT to edit'
      AutoComplete = False
      Style = csDropDownList
      TabOrder = 0
      OnContextPopup = LutNameComboBoxContextPopup
      OnSelect = LutNameComboBoxSelect
      Items.Strings = (
        '[DEFAULT]'
        '[CLIP]'
        'User LUT 1'
        'User LUT 2'
        'User LUT 3'
        'User LUT 4'
        'User LUT 5'
        'User LUT 6'
        'User LUT 7'
        'User LUT 8'
        'User LUT 9'
        'User LUT 10'
        '')
    end
    object CopyToProjectButton: TButton
      Left = 216
      Top = 20
      Width = 81
      Height = 21
      Hint = 'Copy these settings to the project default LUT'
      Caption = 'Copy to Project'
      TabOrder = 1
      OnClick = CopyToProjectButtonClick
    end
    object DeleteUserLutButton: TButton
      Left = 63
      Top = 44
      Width = 50
      Height = 21
      Hint = 'Delete the selected LUT'
      Anchors = [akLeft, akBottom]
      Caption = 'Delete'
      TabOrder = 2
      OnClick = DeleteUserLutButtonClick
    end
    object ShortcutComboBox: TComboBox
      Left = 168
      Top = 44
      Width = 37
      Height = 21
      Hint = 'Set the shortcut key for the selected custom LUT'
      AutoComplete = False
      Style = csDropDownList
      DropDownCount = 11
      ItemIndex = 0
      TabOrder = 3
      OnChange = ShortcutComboBoxChange
      OnDropDown = ShortcutComboBoxDropDown
      Items.Strings = (
        ''
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '0')
    end
    object NewUserLutButton: TButton
      Left = 13
      Top = 44
      Width = 50
      Height = 21
      Hint = 'Create a new LUT'
      Caption = 'New'
      TabOrder = 4
      OnClick = NewUserLutButtonClick
    end
    object CopyToClipButton: TButton
      Left = 216
      Top = 44
      Width = 81
      Height = 21
      Hint = 'Copy these settings to the clip default LUT'
      Caption = 'Copy to Clip'
      TabOrder = 5
      OnClick = CopyToClipButtonClick
    end
  end
  object ImportLutButton: TButton
    Left = 38
    Top = 568
    Width = 120
    Height = 21
    Hint = 'Import values from a LUT file'
    Anchors = [akBottom]
    Caption = 'Import LUT file'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    TabStop = False
    OnClick = ImportLutButtonClick
  end
  object ExportLutButton: TButton
    Left = 171
    Top = 568
    Width = 120
    Height = 21
    Hint = 'Export values to a LUT file'
    Anchors = [akBottom]
    Caption = 'Export LUT file'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    TabStop = False
    OnClick = ExportLutButtonClick
  end
  object OpenFileDialog: TOpenDialog
    DefaultExt = 'lut'
    InitialDir = 'c:\'
    Left = 408
    Top = 896
  end
  object SaveFileDialog: TSaveDialog
    FileName = '0000'
    Filter = 
      'Cineon format (*.cin)|*.cin|DPX format (*.dpx)|*.dpx|SGI format ' +
      '(*.sgi)|*.sgi|Raw format (*.raw)|*.raw|RGB format (*.rgb)|*.rgb|' +
      'Targa2 format (*.tga)|*.tga|TIFF format (*.tif)|*.tif|Windows Bi' +
      'tmap format (*.bmp)|*.bmp|YUV format (*.yuv)|*.yuv'
    FilterIndex = 2
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Select Destination Image File'
    Left = 264
    Top = 648
  end
  object ExrLutUpdateHackTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = ExrLutUpdateHackTimerTimer
    Left = 283
    Top = 599
  end
end
