//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AcceptOrRejectUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TimelineFrameUnit"
#pragma resource "*.dfm"
TAcceptOrRejectForm *AcceptOrRejectForm;
//---------------------------------------------------------------------------
__fastcall TAcceptOrRejectForm::TAcceptOrRejectForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TAcceptOrRejectForm::FormKeyPress(TObject *Sender,
      char &Key)
{
    if (Key == 'a' || Key == 'A')
       ModalResult = mrAbort;
    else if (Key == 'g' || Key == 'G')
       ModalResult = mrOk;
    else if (Key == 0x1B)     // ASCII Esc
       ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TAcceptOrRejectForm::AcceptButtonClick(TObject *Sender)
{
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TAcceptOrRejectForm::RejectButtonClick(TObject *Sender)
{
   ModalResult = mrAbort;
}
//---------------------------------------------------------------------------

void __fastcall TAcceptOrRejectForm::CancelButtonClick(TObject *Sender)
{
   ModalResult = mrCancel;
}
//---------------------------------------------------------------------------


void __fastcall TAcceptOrRejectForm::FormShow(TObject *Sender)
{
   TurnOnAutoAcceptCheckBox->Checked = false;
}
//---------------------------------------------------------------------------

void __fastcall TAcceptOrRejectForm::FormHide(TObject *Sender)
{
   if (ModalResult == mrNone)
      ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void TAcceptOrRejectForm::ShowTurnOnAutoAcceptCheckbox(bool flag)
{
   TurnOnAutoAcceptCheckBox->Visible = flag;
}
//---------------------------------------------------------------------------

bool TAcceptOrRejectForm::GetTurnOnAutoAcceptChecked()
{
   return TurnOnAutoAcceptCheckBox->Checked;
}


