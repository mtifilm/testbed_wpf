//---------------------------------------------------------------------------

#ifndef UserGuideViewerH
#define UserGuideViewerH

#include <windows.h>
#include <Shellapi.h>

#include <string>
using std::string;

//---------------------------------------------------------------------------


class UserGuideViewer
{
	// Used as the key for the user guide index entry
	static string _ActiveToolName;

public:
	int show();
	int showAndGoToToolPage();
	int showAndGoToSection(const string &sectionName);

	void setActiveToolName(const string &toolName);

private:
    bool _firstTime = true;
};


#endif
