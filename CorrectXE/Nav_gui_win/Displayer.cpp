#include <math.h>
#include "MTIDialogs.h"
#include "Ippheaders.h"
//------------------------------------------------------------------------------

// Test options: enable TEST and a SINGLE test-format
// #define OLD_PATTERN

// #define NEW_PATTERN
// #define TEST
//
// #define TESTYUV8
// #define TESTYUV10
// #define TESTDVS10
// #define TESTDVS10a
// #define TESTDVS10BE
// #define TESTUYV8
// #define TESTUYV10
// #define TESTRGB8
// #define TESTRGB8LOG
// #define TESTBGR8
// #define TESTRGB10
// #define TESTRGB10LOG
// #define TESTRGB12
// #define TESTRGB12P
// #define TESTRGB16BE
// #define TESTRGB16LE
// #define TESTRGBA10

// #define TESTMON10
// #define TESTMON10P

#include "Displayer.h"

#include "MTImalloc.h"
#include "ImageFormat3.h"
#include "BinManager.h"
#include "ColorSpaceConvert.h"
#include "Convert.h"
#include "err_metadata.h"
#include "Extractor.h"
#include "LineEngine.h"
#include "StrokeChannel.h"
#include "GetDXErrStrAnsi.h"
#include "GpuAccessor.h"
#include "GrainPresets.h"
#include "Magnifier.h"
#include "MainWindowUnit.h"
#include "mt_math.h"
#include "IniFile.h"
#include "BrushAlgo.h"
#include "ToolManager.h"
#include "NavigatorTool.h"
#include "NavMainCommon.h"
#include "MaskTool.h"
#include "Player.h"
#include "RegionOfInterest.h"
#include "SaveRestore.h"
#include "TransformEditor.h"
#include "Registrar.h"
#include "Rotator.h"
#include "ImageDatumConvert.h"
#define USE_ZOOM_STACK 1
//------------------------------------------------------------------------------

CBHook CDisplayer::DisplayRectanglesChanged;

//------------------------------------------------------------------------------

CDisplayer::CDisplayer()
{
	// Life is easier with separate converters for the two displayed frames.
	_mainFrameConverter = new CConvert;
	_compareFrameConverter = new CConvert;

	// no image format yet
	imgFmt = nullptr;

	// default display context
	displayMode = DISPLAY_MODE_TRUE_VIEW;
	truZoomLevel = 1;
	bgndFlag = false;

	panMode = PAN_OFF;
	panFlag = PAN_OFF;
	panByGrabbing = false;
	mouseZoomModeIsActive = false;
	zoomedOrPannedInMouseZoomMode = false;

	RGBChannelsMask = ALL_CHANNELS;

	// palette of color values per OpenGL
	graphicsColor = SOLID_RED;
	colorPalette[SOLID_RED] = 0x000000ff;
	colorPalette[SOLID_GREEN] = 0x0000ff00;
	colorPalette[SOLID_BLUE] = 0x00ff3f00; // Hmmmm, not really blue!
	colorPalette[SOLID_WHITE] = 0x00ffffff;
	colorPalette[SOLID_YELLOW] = 0x0000ffff;
	colorPalette[SOLID_CYAN] = 0x00ffff00;
	colorPalette[SOLID_MAGENTA] = 0x00ff00ff;
	colorPalette[SOLID_BLACK] = 0x00000000;
	colorPalette[SOLID_LTGRAY] = 0x00c0c0c0;
	colorPalette[SOLID_DKGRAY] = 0x00808080;
	colorPalette[SOLID_ORANGE] = 0x0000B4F0;

	colorPalette[DASHED_RED] = colorPalette[SOLID_RED];
	colorPalette[DASHED_GREEN] = colorPalette[SOLID_GREEN];
	colorPalette[DASHED_BLUE] = colorPalette[SOLID_BLUE];
	colorPalette[DASHED_WHITE] = colorPalette[SOLID_WHITE];
	colorPalette[DASHED_YELLOW] = colorPalette[SOLID_YELLOW];
	colorPalette[DASHED_CYAN] = colorPalette[SOLID_CYAN];
	colorPalette[DASHED_MAGENTA] = colorPalette[SOLID_MAGENTA];
	colorPalette[DASHED_BLACK] = colorPalette[SOLID_BLACK];
	colorPalette[DASHED_LTGRAY] = colorPalette[SOLID_LTGRAY];
	colorPalette[DASHED_DKGRAY] = colorPalette[SOLID_DKGRAY];
	colorPalette[DASHED_ORANGE] = colorPalette[DASHED_ORANGE];

	// lasso pt buffer
	lassoMode = false;
	lasso = new POINT[MAX_LASSO_PTS];

	// get the size of the aggregate screen and allocate
	// the frame buffer for Convert operations. Make it 25%
	// larger than needed to allow for rgbRect > imgRect

	// texture for decimated display is 2K x 2K
	textureWdth = TEXTURE_WIDTH;
	textureHght = TEXTURE_HEIGHT;

	// make sure frame buffer is at least texture size
	screenWdth = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	if (screenWdth < textureWdth)
	{
		screenWdth = textureWdth;
	}
	screenHght = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	if (screenHght < textureHght)
	{
		screenHght = textureHght;
	}

	screenFrameBuffer = (unsigned int *)MTImalloc(screenWdth * screenHght * 5);

	// allocate an extractor
	extr = new CExtractor;

	// allocate a display LUT manager
	lutmgr = new CLUTManager;

	// make sure these are nullptr to start
	customLUTGamma = 17;
	customLUTOption = LUT_OPTION_INTERNAL;
	currentImageFormat = nullptr;
	currentDisplayLUT = nullptr;
	currentLUTPtr = nullptr;

	/////////////////////////////////////////////////////
	//
	// default values
	//
	// no frame to display
	// lastDisplayFields[0] = lastDisplayFields[1] = 0;
	_lastDisplayedFrameBuffer = nullptr;

	// vis part/client area
	borderWidth = 0;
	justifyImage = 0;
	zoomFactor = 1.0;
	visRect.left = 0;
	visRect.top = 0;
	visRect.right = 1023;
	visRect.bottom = 767;
	visWdth = 1024;
	visHght = 768;

	// img part/client area
	imgRect.left = 0;
	imgRect.top = 0;
	imgRect.right = 1023;
	imgRect.bottom = 767;
	imgWdth = 1023;
	imgHght = 767;

	// init frame dimension
	frmRect.left = 0;
	frmRect.top = 0;
	frmRect.right = 1023;
	frmRect.bottom = 767;
	frmWdth = 1024;
	frmHght = 768;

	// scale factors can't be 0
	XScaleFrameToClient = 1.0;
	YScaleFrameToClient = 1.0;

	// set up some reasonable values here
	pixelAspectRatio = 1.0;
	frameAspectRatio = 1.333;
	framePixelComponents = IF_PIXEL_COMPONENTS_YUV422;
	frameBitsPerComponent = 8;
	frameColorSpaceBlack[0] = 0;
	frameColorSpaceBlack[1] = 0;
	frameColorSpaceBlack[2] = 0;
	frameColorSpaceWhite[0] = 0;
	frameColorSpaceWhite[1] = 0;
	frameColorSpaceWhite[2] = 0;
	framePixelPacking = IF_PIXEL_PACKING_8Bits_IN_1Byte;
	frameFieldCount = 2;

	// init displayed rectangle
	dspRect.left = 0;
	dspRect.top = 0;
	dspRect.right = 1023;
	dspRect.bottom = 767;
	dspWdth = 1024;
	dspHght = 768;
	dspXctr = 512;
	dspYctr = 384;

	dspXctrWhenLastRotated = 0x7fffffff;
	dspYctrWhenLastRotated = 0x7fffffff;
	rotatedSinceLastTranslation = false;
	rotateExactReenterFlag = false;
	useTrackingAlignOffsets = false;

	/////////////////////////////////////////////////////
	//
	// specialized support for SCRATCH
	//
	freeMagnifiers = MAX_MAGNIFIERS;
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		magptr[i] = nullptr;
	}

	stretchRectangleColor = SOLID_RED;

	/////////////////////////////////////////////////////
	//
	// specialized support for PAINT
	//
	importProxyWdth = 0;
	importProxyHght = 0;
	importProxy = nullptr;

	desiredTargetFrame = -1;
	desiredImportFrame = -1;
	captureMode = CAPTURE_MODE_TARGET;
	importFrameMode = IMPORT_MODE_RELATIVE;
	importFrameOffset = -2;

	autoAccept = true;
	targetOrgIntBuf = nullptr;
	targetIntBuf = nullptr;
	targetPreIntBuf = nullptr;
	importPreIntBuf = nullptr;
	importIntBuf = nullptr;
	paintMdfdBuf = nullptr;
	previewHackIntBuf = nullptr;
   inPreviewHackMode = false;

	originalPels = new CPixelRegionList;
	// importDiffPels    = new CPixelRegionList;
	lseed = 8311315;

	preRegistrationBuf = nullptr;
	postRegistrationBuf = nullptr;

	// no stroke stack yet
	// paintMdfdChannel = nullptr;
	paintPelsChannel = nullptr;
	strokeStack.clear();

	// no PAINT brush yet
	paintBrush = nullptr;
	preRadius = -1.0;
	preAspect = -1.0;
	preAngle = -10000.0;
	impRot = 0.0;
	rotator = new CRotator;
	transformEditor = nullptr;

	// no CLONE brush yet
	cloneBrush = nullptr;

	paintHistory = new CSaveRestore;

	registrar = new CRegistrar;

	// autoalign box size
	autoAlignBoxSize = 64;

	// default is cursor on
	cursorOn = true;

	// state variables
	majorState = IDLE;
	minorState = CANCELLED;
	currentTool = CURRENT_TOOL_NAVIGATOR;
	onionSkinEnabled = ONIONSKIN_MODE_OFF;
	onionSkinTargetWgt = 127;
	cursorOverrideIndex = 0;
	shapeHackShapeType = -1; // invalid to force computation on first call

	// new flags
	forceTargetLoad = false;
	forceImportLoad = false;
	brushEnabled = true;
	/////////////////////////////////////////////////////

	_grainPresets = nullptr;

	// assume we're not testing
	tstBuf[0] = nullptr;
	tstBuf[1] = nullptr;

#ifdef TEST
	initTestFrame();
#endif
}
//------------------------------------------------------------------------------

CDisplayer::~CDisplayer()
{

#ifdef TEST
	delete[]extrBuf;

	MTIfree(tstBuf[1]);
	MTIfree(tstBuf[0]);
#endif

	delete registrar;

	delete paintHistory;

	delete rotator;

	// delete any SCRATCH magnifiers
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] != nullptr)
		{
			delete magptr[i];
		}
	}

	// delete any buffers for PAINT
	delete[]importProxy;

	// Prealloced buffers
	MTIfree(targetOrgIntBuf);
	MTIfree(targetIntBuf);
	MTIfree(targetPreIntBuf);
	MTIfree(importPreIntBuf);
	MTIfree(importIntBuf);
	MTIfree(paintMdfdBuf);
	MTIfree(preRegistrationBuf);
	MTIfree(postRegistrationBuf);

   if (previewHackIntBufWasCudaMalloced)
	{
      MtiCudaFreeHost(previewHackIntBuf);
   }
   else
   {
      MTIfree(previewHackIntBuf);
   }

	MTIfree(_currentFrameCompareBuf);
	MTIfree(_importFrameCompareBuf);

	// delete original pixel list
	delete originalPels;

	// delete import-target diffs mask
	delete[]_frameCompareOutputMask;

	// delete lasso pts
	delete[]lasso;

	// delete the extractor
	delete extr;

	// delete the Display LUT Manager - CODEGUARD said it was leaking 20090101
	delete lutmgr;

	// deallocate the conversion frame buffer
	MTIfree(screenFrameBuffer);

	// delete the software converters
	delete _mainFrameConverter;
	delete _compareFrameConverter;
}
//------------------------------------------------------------------------------

// FRAME DISPLAY METHODS -------------------------------------------------------
//
// this call creates a screen device context and a rendering context
// given a window HANDLE. It is called at OnCreate time
int CDisplayer::getGraphicsDeviceContexts(HWND handle, HDC& scrndc, HGLRC& rndrrc)
{
	scrndc = GetDC(handle);

	// if not running in 32-bit TRUECOLOR exit immediately
	if (GetDeviceCaps(scrndc, BITSPIXEL) != 32)
	{
		return (-1);
	}

	// now set up the rendering context
	int nPixelFormat;

	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // Size of this structure
		1, // Version of this structure
		PFD_DRAW_TO_WINDOW | // Draw to Window (not to bitmap)
				PFD_SUPPORT_OPENGL | // Support OpenGL calls in window
				PFD_DOUBLEBUFFER, // Double buffered mode
		PFD_TYPE_RGBA, // RGBA Color mode
		32, // Want 32 bit color
		0, 0, 0, 0, 0, 0, // Not used to select mode
		0, 0, // Not used to select mode
		0, 0, 0, 0, 0, // Not used to select mode
		0, // Size of depth buffer
		0, // Not used to select mode
		0, // Not used to select mode
		0, // Not used to select mode
		0, // Not used to select mode
		0, 0, 0}; // Not used to select mode

	// Choose a pixel format that best matches that described in pfd
	nPixelFormat = ChoosePixelFormat(scrndc, &pfd);

	// Set the pixel format for the device context
	SetPixelFormat(scrndc, nPixelFormat, &pfd);

	// Create the rendering context
	rndrrc = wglCreateContext(scrndc);

	// success
	return (0);
}
//------------------------------------------------------------------------------

// this call sets the screen device context, rendering context,
// and client rectangle. It is called at OnResize time.
int CDisplayer::initDisplayer(HWND handle, HDC scrndc, HGLRC rndrrc, int clientwdth, int clienthght)
{
	// if stretching a RECT or drawing a LASSO
	// home the state variables
	if ((majorState == STRETCH_RECTANGLE_ACTIVE) || (majorState == DRAW_LASSO_ACTIVE))
	{
		majorState = IDLE;
		minorState = CANCELLED;
	}

	return initDisplayGraphics(handle, scrndc, rndrrc, clientwdth, clienthght);

}
//------------------------------------------------------------------------------

void CDisplayer::setMainWindowPtr(TmainWindow *mainWin)
{
	mainWindow = mainWin;
}
//------------------------------------------------------------------------------

// Given the coordinates of the center of a display rectangle,
// use the existing display window width and height to calcu-
// late a new display rectangle. Also adjusts
//
// i) rgbRect - the rectangle in client coordinates which is
// mapped to the display rectangle. This is a
// bit larger than imgRect because it always
// includes whole frame pixels, regardless of
// how many screen pixels they generate
//
void CDisplayer::calculateDisplayRectangle(int xctr, int yctr)
{
	// save center coordinates
	dspXctr = xctr;
	dspYctr = yctr;

	// center the rgbRect over the imgRect
	rgbRect.left = imgRect.left - ((rgbRectWidth - imgWdth) >> 1);
	rgbRect.top = imgRect.top - ((rgbRectHeight - imgHght) >> 1);

	// aligned even so that CONVERT begins on "U"
	dspRect.left = (dspXctr - dspWdth / 2) & 0xfffffffe;
	dspRect.right = dspRect.left + dspWdth - 1;

	if (dspRect.left <= frmRect.left)
	{
		dspRect.left = frmRect.left;
		dspRect.right = dspRect.left + dspWdth - 1;
		rgbRect.left = imgRect.left;
	}
	else if (dspRect.right >= frmRect.right)
	{
		dspRect.right = frmRect.right;
		dspRect.left = dspRect.right + 1 - dspWdth;
		rgbRect.left = imgRect.left - (rgbRectWidth - imgWdth);
	}

	// aligned even so that CONVERT begins on field 0
	dspRect.top = (dspYctr - dspHght / 2) & 0xfffffffe;

	dspRect.bottom = dspRect.top + dspHght - 1;
	if (dspRect.top <= frmRect.top)
	{
		dspRect.top = frmRect.top;
		dspRect.bottom = dspRect.top + dspHght - 1;
		rgbRect.top = imgRect.top;
	}
	else if (dspRect.bottom >= frmRect.bottom)
	{
		dspRect.bottom = frmRect.bottom;
		dspRect.top = dspRect.bottom + 1 - dspHght;
		rgbRect.top = imgRect.top - (rgbRectHeight - imgHght);
	}

	rgbRect.right = rgbRect.left + rgbRectWidth - 1;
	rgbRect.bottom = rgbRect.top + rgbRectHeight - 1;

	// make sure the center coordinates of
	// the display window are consistent
	dspXctr = dspRect.left + dspWdth / 2;
	dspYctr = dspRect.top + dspHght / 2;

	// Notify anyone interested that the rectangles have changes
	DisplayRectanglesChanged.Notify();

#ifdef DEBUG_FUCKING_RECTS
	TRACE_0(errout << "----------------------------");
	TRACE_0(errout << "AFTER moveDisplayRectangle()");
	DumpAWholeBunchOfReallyUglyShit();
#endif
}
//------------------------------------------------------------------------------

// given a rectangle, centers the display rectangle on it
// while setting the zoomLevel to the last one used for
// unZOOMed REVIEW.
void CDisplayer::calculateReviewRectangle(RECT * rvue)
{
	int xc = (rvue->left + rvue->right) / 2;
	int yc = (rvue->top + rvue->bottom) / 2;

	// zoomLevel = reviewZoomLevel;
	calculateImageRectangle();
	calculateDisplayRectangle(xc, yc);
}
//------------------------------------------------------------------------------

// given a rectangle, sets the display rectangle to the
// smallest rectangle which surrounds it. For ZOOMed REVIEW
void CDisplayer::calculateZoomedReviewRectangle(RECT * rvue)
{
	int xc = (rvue->left + rvue->right) / 2;
	int yc = (rvue->top + rvue->bottom) / 2;

	int rwd = rvue->right - rvue->left;
	int rht = rvue->bottom - rvue->top;

	double zoomw = imgWdth / (pixelAspectRatio * 3 * rwd);
	double zoomh = imgHght / (double)(3 * rht);
	double reslt = (zoomw < zoomh ? zoomw : zoomh);
	reslt = (int)reslt;

	zoomFactor = reslt;
	MTIassert(zoomFactor > 0.0);
	if (zoomFactor <= 0.0)
	{
		zoomFactor = 1.0;
	}

	calculateImageRectangle();
	calculateDisplayRectangle(xc, yc);
	setBgndFlag(imgShrinkFlag);
}
//------------------------------------------------------------------------------

// Move the dspRect by a given amount, for the purposes
// of PANning. Adjust rgbRect as well, to ensure that
// the user sees the pixels at the edge of the frame if
// that is encountered.
//
void CDisplayer::moveDisplayRectangle(int delx, int dely)
{
	// center the imgRect inside the rgbRect
	rgbRect.left = imgRect.left - ((rgbRectWidth - imgWdth) / 2);
	rgbRect.top = imgRect.top - ((rgbRectHeight - imgHght) / 2);

	// displacement is even
	delx &= 0xfffffffe;

	// displace the rectangle
	dspRect.left += delx;
	dspRect.top += dely;
	dspRect.right += delx;
	dspRect.bottom += dely;
	dspXctr += delx;
	dspYctr += dely;

	// WHY DOESN'T JUST CALL calculateDisplayRectangle() AT THIS POINT ??? QQQ

	// respect the frame boundaries
	if (dspRect.left <= frmRect.left)
	{
		// up against the left
		dspRect.left = frmRect.left;
		dspRect.right = dspRect.left + dspWdth - 1;

		// align rgbRect so that the left pixels are visible
		rgbRect.left = imgRect.left;
	}

	if (dspRect.right >= frmRect.right)
	{
		// up against the right
		dspRect.right = frmRect.right;
		dspRect.left = dspRect.right - dspWdth + 1;

		// align rgbRect so that the right pixels are visible
		rgbRect.left = imgRect.left - (rgbRectWidth - imgWdth);
	}

	if (dspRect.top <= frmRect.top)
	{
		// up against the top
		dspRect.top = frmRect.top;
		dspRect.bottom = dspRect.top + dspHght - 1;

		// align rgbRect so that the top pixels are visible
		rgbRect.top = imgRect.top;
	}

	if (dspRect.bottom > frmRect.bottom)
	{
		// up against the bottom
		dspRect.bottom = frmRect.bottom;
		dspRect.top = dspRect.bottom - dspHght + 1;

		// align rgbRect so that the bottom pixels are visible
		rgbRect.top = imgRect.top - (rgbRectHeight - imgHght);
	}

	rgbRect.right = rgbRect.left + rgbRectWidth - 1;
	rgbRect.bottom = rgbRect.top + rgbRectHeight - 1;

#ifdef DEBUG_FUCKING_RECTS
	TRACE_0(errout << "----------------------------");
	TRACE_0(errout << "AFTER moveDisplayRectangle()");
	DumpAWholeBunchOfReallyUglyShit();
#endif
}
//------------------------------------------------------------------------------

bool CDisplayer::setFrameAspectRatio(int option)
{
	switch (option)
	{
		case ASPECT_DEF: // "Default" => values from clip
			frameAspectRatio = imgFmt->getFrameAspectRatio();
			break;

		case ASPECT_133:
			frameAspectRatio = 1.33;
			break;

		case ASPECT_166:
			frameAspectRatio = 1.66;
			break;

		case ASPECT_178:
			frameAspectRatio = 1.78;
			break;

		case ASPECT_185:
			frameAspectRatio = 1.85;
			break;

		case ASPECT_220:
			frameAspectRatio = 2.20;
			break;

		case ASPECT_235:
			frameAspectRatio = 2.35;
			break;

		case ASPECT_239:
			frameAspectRatio = 2.40;
			break;

		case ASPECT_255:
			frameAspectRatio = 2.55;
			break;

		case ASPECT_266:
			frameAspectRatio = 2.66;
			break;

		default:
			frameAspectRatio = imgFmt->getFrameAspectRatio();
			break;
	}

	// calculate pixelAspectRatio from frameAspectRatio
	pixelAspectRatio = frameAspectRatio * (double)frameHeight / (double)frameWidth;

	// if it's supposed to be 1 snap it
	if ((pixelAspectRatio > .9999) && (pixelAspectRatio < 1.0001))
	{
		pixelAspectRatio = 1;
	}

	mainWindow->UpdateViewingModeStatus();
	mainWindow->UpdateFrameAspectRatio(option);
	saveAspectRatioOption(option);
	calculateImageRectangle();

	return true;
}
//------------------------------------------------------------------------------

int CDisplayer::loadAspectRatioOption()
{
	CBinManager bm;

	string binPath = mainWindow->GetCurrentClipHandler()->getCurrentBinPath();

	CIniFile *binFile = bm.openBinIniFile(binPath);
	if (binFile == nullptr)
	{
		TRACE_1(errout << "CDisplayer::writePixelAspectRatio: cannot open bin ini file");
		return -1;
	}

	string sectionName = "AspectRatio";

	string frameAspectRatioKey = "FrameAspectRatioOption";
	int option = binFile->ReadInteger(sectionName, frameAspectRatioKey, ASPECT_DEF);

	if (!DeleteIniFile(binFile))
	{
		TRACE_1(errout << "CDisplayer::writePixelAspectRatio: cannot close bin ini file");
		return -1;
	}

	return option;
}
//------------------------------------------------------------------------------

int CDisplayer::saveAspectRatioOption(int option)
{
	CBinManager bm;

	string binPath = mainWindow->GetCurrentClipHandler()->getCurrentBinPath();

	CIniFile *binFile = bm.openBinIniFile(binPath);
	if (binFile == nullptr)
	{
		TRACE_1(errout << "CDisplayer::writePixelAspectRatio: cannot open bin ini file");
		return -1;
	}

	string sectionName = "AspectRatio";

	string frameAspectRatioKey = "FrameAspectRatioOption";
	binFile->WriteInteger(sectionName, frameAspectRatioKey, option);

	if (!DeleteIniFile(binFile))
	{
		TRACE_1(errout << "CDisplayer::writePixelAspectRatio: cannot close bin ini file");
		return -1;
	}

	return 0;
}
//------------------------------------------------------------------------------

// God I hate this fucking code.
void CDisplayer::updateAllOfKurtsFuckingRects()
{
	calculateImageRectangle();
	calculateDisplayRectangle(frameWidth / 2, frameHeight / 2);
	calculateMouseFrameCoordinates();
	setBgndFlag(imgShrinkFlag);
}
//------------------------------------------------------------------------------

// Calculate the rectangle imgRect in client coordinates which holds
// the frame image. Also sets a flag, imgShrinkFlag, which tells the
// Displayer that the new imgRect fits within the old one and that
// consequently the gray background must be re-drawn (around it).
//
#if 1
void CDisplayer::calculateImageRectangle()
{
	int w, h;
	int halfOfSpacerWidth = std::max<int>(borderWidth / 2, 4);
	if (mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		w = (visWdth / 2) - halfOfSpacerWidth - borderWidth;
	}
	else
	{
		w = visRect.right - visRect.left + 1 - borderWidth * 2;
	}

	h = visRect.bottom - visRect.top + 1 - borderWidth * 2;

	// starting values
	imgWdth = w & 0xfffffffe;
	imgHght = h;

	int frmWdthEven = (frmWdth & 0xfffffffe);

	if ((displayMode == DISPLAY_MODE_TRUE_VIEW) || (displayMode == DISPLAY_MODE_FIT_WIDTH))
	{
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}

		dspWdth = 2 * ((int)(imgWdth / (2 * pixelAspectRatio * zoomFactor)));

		while ((rgbRectWidth = (int)(dspWdth * pixelAspectRatio * zoomFactor)) < imgWdth)
		{
			dspWdth += 2;
		}

		if (dspWdth > frmWdthEven)
		{
			imgWdth = frmWdthEven * pixelAspectRatio * zoomFactor;
			rgbRectWidth = imgWdth;
			dspWdth = frmWdthEven;
		}

		dspHght = imgHght / zoomFactor;

		while ((rgbRectHeight = dspHght * zoomFactor) < imgHght)
		{
			dspHght++;
		}

		if (dspHght > frmHght)
		{
			imgHght = frmHght * zoomFactor;
			rgbRectHeight = imgHght;
			dspHght = frmHght;
		}
	}
	else if (displayMode == DISPLAY_MODE_1_TO_1)
	{

		// Don't know why, but sometimes when doing GOV the zoomfactor
		// becomes 0!!
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
		//////////////////////////////////////////////////////////////

		dspWdth = 2 * ((int)(imgWdth / (2 * zoomFactor)));

		while ((rgbRectWidth = dspWdth * zoomFactor) < imgWdth)
		{
			dspWdth += 2;
		}

		if (dspWdth > frmWdthEven)
		{
			imgWdth = frmWdthEven * zoomFactor;
			rgbRectWidth = imgWdth;
			dspWdth = frmWdthEven;
		}

		dspHght = imgHght / zoomFactor;

		while ((rgbRectHeight = dspHght * zoomFactor) < imgHght)
		{
			dspHght++;
		}

		if (dspHght > frmHght)
		{
			imgHght = frmHght * zoomFactor;
			rgbRectHeight = imgHght;
			dspHght = frmHght;
		}

	}

	// copy of the existing imgRect
	RECT oldImgRect = imgRect;

	if (mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		imgRect.left = visRect.left
							+ ((justifyImage < 0)
								? (imgWdth + borderWidth + (2 * halfOfSpacerWidth)) // left
								: ((justifyImage > 0)
									 ? (visWdth - imgWdth - borderWidth)             // right
									 : ((visWdth / 2) + halfOfSpacerWidth)));        // centered
	}
	else
	{
		imgRect.left = visRect.left + borderWidth
							+ ((justifyImage < 0)
								? 0                                    // left
								: ((justifyImage > 0)
									 ? (w - imgWdth)                    // right
									 : ((w - imgWdth) / 2)));           // centered
	}

	imgRect.top = visRect.top + borderWidth + ((h - imgHght) / 2);
	imgRect.right = imgRect.left + imgWdth - 1;
	imgRect.bottom = imgRect.top + imgHght - 1;

	RECT debugInspect = visRect;

	// rgbRect is the target of CONVERT (software mode)
	rgbRect.left = imgRect.left - ((rgbRectWidth - imgWdth) / 2);
	rgbRect.top = imgRect.top - ((rgbRectHeight - imgHght) / 2);
	rgbRect.right = rgbRect.left + rgbRectWidth - 1;
	rgbRect.bottom = rgbRect.top + rgbRectHeight - 1;

	// Compute the left rectangle if side-by-side mode.
	if (mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		leftRgbRect.left = std::max<int>(borderWidth, rgbRect.left - rgbRectWidth - (2 * halfOfSpacerWidth));
		leftRgbRect.top = rgbRect.top;
		leftRgbRect.right = leftRgbRect.left + rgbRectWidth - 1;
		leftRgbRect.bottom = rgbRect.bottom;
	}
	else
	{
		leftRgbRect.left = 0;
		leftRgbRect.top = 0;
		leftRgbRect.right = 0;
		leftRgbRect.bottom = 0;
	}

	// if the new imgRect causes new gray background to be
	// exposed, set the imgShrinkFlag and use it to set
	// bgndFlag later
	imgShrinkFlag = false;
	if ((imgRect.left > oldImgRect.left)
	|| (imgRect.top > oldImgRect.top)
	|| (imgRect.right < oldImgRect.right)
	|| (imgRect.bottom < oldImgRect.bottom))
	{
		imgShrinkFlag = true;
	}

	if (!rectangleIsValid(&imgRect))
	{ // client area too small
		imgRect = visRect;
	}

	// scale factors for mapping frame coords to client coords
	XScaleClientToFrame = ((double)dspWdth / (double)rgbRectWidth) * (1.0 + 1.0 / (double)rgbRectWidth);
	YScaleClientToFrame = ((double)dspHght / (double)rgbRectHeight) * (1.0 + 1.0 / (double)rgbRectHeight);

	// scale factors for mapping client coords to frame coords
	XScaleFrameToClient = 1.0 / XScaleClientToFrame;
	YScaleFrameToClient = 1.0 / YScaleClientToFrame;

	// scale factors for generating component display values
	XScaleCursorToFrame = XScaleClientToFrame;
	YScaleCursorToFrame = YScaleClientToFrame;

	// make sure you always have a display rectangle
	// calculateDisplayRectangle(dspRect.left + (dspRect.right-dspRect.left+1)/2,
	// dspRect.top  + (dspRect.bottom-dspRect.top+1)/2);
	calculateDisplayRectangle(dspXctr, dspYctr);

	// always pass the rgbRect along to the TransformEditor. The center
	// of this, in client coordinates, is where (dspXctr, dspYctr) maps
	transformEditor->setImageRectangle(rgbRect);

#ifdef DEBUG_FUCKING_RECTS
	TRACE_0(errout << "----------------------------");
	TRACE_0(errout << "AFTER calculateImageRectangle()");
	DumpAWholeBunchOfReallyUglyShit();
#endif
}
#else
//------------------------------------------------------------------------------

// Calculate the rectangle imgRect in client coordinates which holds
// the frame image. Also sets a flag, imgShrinkFlag, which tells the
// Displayer that the new imgRect fits within the old one and that
// consequently the gray background must be re-drawn (around it).
//
void CDisplayer::calculateImageRectangle() //// calculateSideBySideImageRectangles()
{
	int sbsHalfOfBorderWidth = std::max<int>(borderWidth / 2, 5);
	int halfWidth = (visRect.right - visRect.left + 1) / 2;
	int w = halfWidth - (sbsHalfOfBorderWidth * 3);
	int h = (visRect.bottom - visRect.top + 1) - (sbsHalfOfBorderWidth * 4);

	// starting values
	imgWdth = w & 0xfffffffe;
	imgHght = h;

	int frmWdthEven = (frmWdth & 0xfffffffe);

	if ((displayMode == DISPLAY_MODE_TRUE_VIEW) || (displayMode == DISPLAY_MODE_FIT_WIDTH))
	{
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}

		dspWdth = 2 * ((int)(imgWdth / (2 * pixelAspectRatio * zoomFactor)));

		while ((rgbRectWidth = (int)(dspWdth * pixelAspectRatio * zoomFactor)) < imgWdth)
		{
			dspWdth += 2;
		}

		if (dspWdth > frmWdthEven)
		{
			imgWdth = frmWdthEven * pixelAspectRatio * zoomFactor;
			rgbRectWidth = imgWdth;
			dspWdth = frmWdthEven;
		}

		dspHght = imgHght / zoomFactor;

		while ((rgbRectHeight = dspHght * zoomFactor) < imgHght)
		{
			dspHght++;
		}

		if (dspHght > frmHght)
		{
			imgHght = frmHght * zoomFactor;
			rgbRectHeight = imgHght;
			dspHght = frmHght;
		}
	}
	else if (displayMode == DISPLAY_MODE_1_TO_1)
	{
		// Don't know why, but sometimes when doing GOV the zoomfactor
		// becomes 0!!
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
		//////////////////////////////////////////////////////////////

		dspWdth = 2 * ((int)(imgWdth / (2 * zoomFactor)));

		while ((rgbRectWidth = dspWdth * zoomFactor) < imgWdth)
		{
			dspWdth += 2;
		}

		if (dspWdth > frmWdthEven)
		{
			imgWdth = frmWdthEven * zoomFactor;
			rgbRectWidth = imgWdth;
			dspWdth = frmWdthEven;
		}

		dspHght = imgHght / zoomFactor;

		while ((rgbRectHeight = dspHght * zoomFactor) < imgHght)
		{
			dspHght++;
		}

		if (dspHght > frmHght)
		{
			imgHght = frmHght * zoomFactor;
			rgbRectHeight = imgHght;
			dspHght = frmHght;
		}

	}

	// copy of the existing imgRect
	RECT oldImgRect = imgRect;

	imgRect.left = visRect.left + halfWidth + sbsHalfOfBorderWidth;
	imgRect.top = visRect.top + (sbsHalfOfBorderWidth * 2) + ((h - imgHght) >> 1);
	imgRect.right = imgRect.left + imgWdth - 1;
	imgRect.bottom = imgRect.top + imgHght - 1;

	RECT debugInspect = visRect;

	// rgbRect is the target of CONVERT (software mode)
	rgbRect.left = imgRect.left - ((rgbRectWidth - imgWdth) >> 1);
	rgbRect.top = imgRect.top - ((rgbRectHeight - imgHght) >> 1);
	rgbRect.right = rgbRect.left + rgbRectWidth - 1;
	rgbRect.bottom = rgbRect.top + rgbRectHeight - 1;

	// if the new imgRect causes new gray background to be
	// exposed, set the imgShrinkFlag and use it to set
	// bgndFlag later
	imgShrinkFlag = false;
	if ((imgRect.left > oldImgRect.left) || (imgRect.top > oldImgRect.top) || (imgRect.right < oldImgRect.right) ||
			(imgRect.bottom < oldImgRect.bottom))
	{
		imgShrinkFlag = true;
	}

	if (!rectangleIsValid(&imgRect))
	{
		// client area too small
		imgRect = visRect;
	}

	// scale factors for mapping frame coords to client coords
	XScaleClientToFrame = ((double)dspWdth / (double)rgbRectWidth) * (1.0 + 1.0 / (double)rgbRectWidth);
	YScaleClientToFrame = ((double)dspHght / (double)rgbRectHeight) * (1.0 + 1.0 / (double)rgbRectHeight);

	// scale factors for mapping client coords to frame coords
	XScaleFrameToClient = 1.0 / XScaleClientToFrame;
	YScaleFrameToClient = 1.0 / YScaleClientToFrame;

	// scale factors for generating component display values
	XScaleCursorToFrame = XScaleClientToFrame;
	YScaleCursorToFrame = YScaleClientToFrame;

	// make sure you always have a display rectangle
	// calculateDisplayRectangle(dspRect.left + (dspRect.right-dspRect.left+1)/2,
	// dspRect.top  + (dspRect.bottom-dspRect.top+1)/2);
	calculateDisplayRectangle(dspXctr, dspYctr);

	// always pass the rgbRect along to the TransformEditor. The center
	// of this, in client coordinates, is where (dspXctr, dspYctr) maps
	transformEditor->setImageRectangle(rgbRect);

	// Compute the left rectangle.
	leftRgbRect.left = rgbRect.left - halfWidth + sbsHalfOfBorderWidth;
	leftRgbRect.top = rgbRect.top;
	leftRgbRect.right = rgbRect.right - halfWidth + sbsHalfOfBorderWidth;
	leftRgbRect.bottom = rgbRect.bottom;
}
#endif
//------------------------------------------------------------------------------

void CDisplayer::saveMouseShiftState(TShiftState shift)
{
	Shift = shift;
}
//------------------------------------------------------------------------------
// NOT USED
TShiftState CDisplayer::getMouseShiftState()
{
	return Shift;
}
//------------------------------------------------------------------------------

// reset the mouse coordinates after changing the window
void CDisplayer::calculateMouseFrameCoordinates()
{
	// the previous & current client coordinates
	xpre = xcur;
	ypre = ycur;

	// the previous & current frame coordinates
	dxcurfrm = (double)dspRect.left + (xcur - rgbRect.left) * XScaleClientToFrame;
	dycurfrm = (double)dspRect.top + (ycur - rgbRect.top) * YScaleClientToFrame;
	dxprefrm = dxcurfrm;
	dyprefrm = dycurfrm;

	// the previous & current integer frame coords
	xcurfrm = (int)dxcurfrm;
	ycurfrm = (int)dycurfrm;
	xprefrm = xcurfrm;
	yprefrm = ycurfrm;

	// generate an extra mouse move event to the Displayer
	xcurfrm = -1;
	ycurfrm = -1;
	mouseMv(xcur, ycur);

	// generate an extra mouse move event to the tool
	int imageX = scaleXClientToImageX(X);
	int imageY = scaleYClientToImageY(Y);
	CToolManager toolManager;
	toolManager.onMouseMove(Shift, X, Y, imageX, imageY);
}
//------------------------------------------------------------------------------

// This performs all the initializations which follow the
// determination of the image format of the (new) clip.
//
void CDisplayer::setImageFormat(const CImageFormat *imgfmt)
{
	// lastDisplayFields[0] = lastDisplayFields[1] = 0;
	_lastDisplayedFrameBuffer = nullptr;

	// save the existing frame rectangle and display context
	// in case the user preference is to use current settings
	oldFrmRect = frmRect;
	oldDisplayContext = getDisplayContext();

	// save ptr to image format
	imgFmt = (CImageFormat*)imgfmt;

#ifdef TEST
	imgFmt = tstFmt;
#endif

	// adjust any allocated magnifiers
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] != nullptr)
		{
			magptr[i]->setMagnifierImageFormat(imgFmt);
		}
	}

	frmRect = imgFmt->getActivePictureRect();
	frmWdth = frmRect.right - frmRect.left + 1;
	frmHght = frmRect.bottom - frmRect.top + 1;

	// entire width of frame
	frameWidth = frmRect.right + 1;

	// entire height of frame
	frameHeight = frmRect.bottom + 1;

	// pitch of a single line now comes from clip
	framePitch = imgFmt->getFramePitch();

	/////////////////////////////////////////////////////////////////////////////
	// get the option from the BIN ini file

	int option = loadAspectRatioOption();

	setFrameAspectRatio(option);

	mainWindow->UpdateFrameAspectRatio(option);

	/////////////////////////////////////////////////////////////////////////////

	// color space
	frameColorSpace = imgFmt->getColorSpace(); // default

	// determines YUV, RGB, or luminance
	framePixelComponents = imgFmt->getPixelComponents();

	// may as well init this here
	if (frameColorSpace == IF_COLOR_SPACE_EXR)
	{
		brushComponents = BRUSH_COMPONENTS_RGB_HALF;
	}
	else
	{
		switch (framePixelComponents)
		{
//			case IF_PIXEL_COMPONENTS_LUMINANCE:
			case IF_PIXEL_COMPONENTS_Y:
			case IF_PIXEL_COMPONENTS_YYY:
				brushComponents = BRUSH_COMPONENTS_YYY;
				break;
			case IF_PIXEL_COMPONENTS_YUV422:
			case IF_PIXEL_COMPONENTS_YUV4224:
			case IF_PIXEL_COMPONENTS_YUV444:
				brushComponents = BRUSH_COMPONENTS_YUV;
				break;
			case IF_PIXEL_COMPONENTS_RGB:
			case IF_PIXEL_COMPONENTS_RGBA:
				brushComponents = BRUSH_COMPONENTS_RGB;
				break;
			case IF_PIXEL_COMPONENTS_BGR:
			case IF_PIXEL_COMPONENTS_BGRA:
				brushComponents = BRUSH_COMPONENTS_BGR;
				break;
			case IF_PIXEL_COMPONENTS_INVALID:
			default:
				brushComponents = BRUSH_COMPONENTS_INVALID;
				break;
		}
	}

	// get the number of non-alpha components
	frameComponentCount = imgFmt->getComponentCountExcAlpha();

	// get the size in bytes of the internal format frame
	int totPels = frameWidth * frameHeight;
	frameInternalSize = totPels * frameComponentCount * 2;
	switch (imgFmt->getAlphaMatteType())
	{
		case IF_ALPHA_MATTE_1_BIT:
			frameInternalSize += (totPels + 7) / 8;
			break;

		case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
			frameInternalSize += (totPels + 3) / 4;
			break;

		case IF_ALPHA_MATTE_8_BIT:
		case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
			frameInternalSize += totPels;
			break;

		case IF_ALPHA_MATTE_16_BIT:
		case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
		case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:
			frameInternalSize += totPels * 2;
			break;

		case IF_ALPHA_MATTE_NONE:
			// Do nothing
			break;

		case IF_ALPHA_MATTE_DEFAULT:
		case IF_ALPHA_MATTE_INVALID:
		default:
			MTIassert(false);
			break;
	}

	// set the number of bits per component
	frameBitsPerComponent = imgFmt->getBitsPerComponent();

	// set the component max value
	frameMaxComponentValue = ((int)1 << frameBitsPerComponent) - 1;

	// set the packing style of the components
	framePixelPacking = imgFmt->getPixelPacking();

	// field count (2 if interlaced)
	frameFieldCount = (imgFmt->getInterlaced() ? 2 : 1);

	// component mins & max's
	imgFmt->getComponentValueMin(frameColorSpaceBlack);
	imgFmt->getComponentValueMax(frameColorSpaceWhite);

	// Load up the correct Display Lut - I can't believe this is where I frickin
	// have to bury this!
	auto newClip = mainWindow->GetCurrentClipHandler()->getCurrentClip();
	mainWindow->LoadAndApplyLutSettings(newClip);

	// establish the display LUT
	establishDisplayLUT();

	// reset the panning mode to XY
	panMode = PAN_XY;

#ifndef FRAMEWRX
	// init the RGB channels mask
	RGBChannelsMask = ALL_CHANNELS;
#endif // FRAMEWRX

	// init the Alpha display to TRANSPARENT
	_mainFrameConverter->setAlphaColor(SHOW_ALPHA_TRANSPARENT);
	_compareFrameConverter->setAlphaColor(SHOW_ALPHA_TRANSPARENT);

	if (currentTool == CURRENT_TOOL_PAINT)
	{
		// free target and import buffers
		unloadTargetAndImportFrames();
	}
	else if (currentTool == CURRENT_TOOL_REGISTRATION)
	{
		// free registration buffers
		unloadRegistrationFrame();
	}
	else
	{
		// free preview hack buffer
		if (previewHackIntBufWasCudaMalloced)
		{
			MtiCudaFreeHost(previewHackIntBuf);
		}
		else
		{
			MTIfree(previewHackIntBuf);
		}

		previewHackIntBuf = nullptr;
      previewHackIntBufWasCudaMalloced = false;
	}

	// if we're switching clips, these are no good anymore
	clearAllTrackingAlignOffsets();
}
//------------------------------------------------------------------------------

int CDisplayer::initPaintHistory(ClipSharedPtr clip)
{
	return paintHistory->setClip(clip);
}
//------------------------------------------------------------------------------

const CImageFormat *CDisplayer::getImageFormat()
{
	return imgFmt;
}
//------------------------------------------------------------------------------

// Clears the image format. This call tells the Displayer
// that there's no clip loaded and that the client area
// should contain a grey background with no imgRect hole.
//
void CDisplayer::clearImageFormat()
{
	// wipe out the image format
	imgFmt = nullptr;

	// free any allocated magnifiers
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] != nullptr)
		{
			freeMagnifier(magptr[i]);
		}
	}

	// free any PAINT brush
	paintBrush = nullptr;

	// free any CLONE brush
	cloneBrush = nullptr;
}
//------------------------------------------------------------------------------

// This tells the Displayer the position in window client
// coordinates of the visible part of the client rectangle.
// This call is made in the MainWindowUnit's FormPaint method,
// based on the height of the toolbar and timeline dock sites.
//
void CDisplayer::setVisiblePartOfClientRectangle(RECT *clrect, int brdr, int clrcol)
{
	visRect.left = clrect->left;
	visRect.top = clrect->top;
	visRect.right = clrect->right - 1;
	visRect.bottom = clrect->bottom - 1;
	visWdth = visRect.right - visRect.left + 1;
	visHght = visRect.bottom - visRect.top + 1;

	// save the desired width of the gray border
	borderWidth = brdr;

	// save the raw clear color
	rawClearColor = clrcol;

	// save the clear color
	clearColor[0] = (float)((clrcol) & 0xff) / 255.;
	clearColor[1] = (float)((clrcol >> 8) & 0xff) / 255.;
	clearColor[2] = (float)((clrcol >> 16) & 0xff) / 255.;
	clearColor[3] = (float)((clrcol >> 24) & 0xff) / 255.;

	// calculate the image rectangle
	calculateImageRectangle();

	// set to clip to client visRect
	clipToVisibleRectangle();

#ifdef DEBUG_FUCKING_RECTS
	TRACE_0(errout << "----------------------------");
	TRACE_0(errout << "AFTER setVisiblePartOfClientRectangle()");
	DumpAWholeBunchOfReallyUglyShit();
#endif
}
//------------------------------------------------------------------------------

RECT CDisplayer::getVisiblePartOfClientRectangle()
{
	return visRect;
}
//------------------------------------------------------------------------------

RECT CDisplayer::getCompareFrameClientRectangle()
{
	return leftRgbRect;
}
//------------------------------------------------------------------------------

RECT CDisplayer::getMainFrameClientRectangle()
{
	return rgbRect;
}
//------------------------------------------------------------------------------

// This tells the Displayer to re-display the grey background
// in the client area, e.g., when the dimensions of imgRect
// are modified.
//
void CDisplayer::setBgndFlag(bool bg)
{
	bgndFlag = bg;
}
//------------------------------------------------------------------------------

bool CDisplayer::getBgndFlag()
{
	return (bgndFlag);
}
//------------------------------------------------------------------------------

int CDisplayer::getDisplayMode()
{
	return displayMode;
}
//------------------------------------------------------------------------------

// This call uses the above methods to toggle between the two
// main display modes. Picture mode maps dspRect to imgRect.
// One-to-One mode maps 1 pixel of the image to zoomFactor x
// zoomFactor pixels on the screen, clipping the result to
// imgRect.
//
void CDisplayer::setDisplayMode(int newmode)
{
	// first snap to integral zoom/shrink
	zoomFactor = snapZoomFactor(zoomFactor);
	MTIassert(zoomFactor > 0.0);
	if (zoomFactor <= 0.0)
	{
		zoomFactor = 1.0;
	}

	// save the zoom factor
	oldZoomFactor = zoomFactor;

	// save the current mode
	oldDisplayMode = displayMode;

	// establish the new mode
	displayMode = newmode;

	// WAS: leave the zoom level where it is
	// but try to maintain the window center
	calculateImageRectangle();
	calculateDisplayRectangle(dspXctr, dspYctr);
	setBgndFlag(imgShrinkFlag);
}
//------------------------------------------------------------------------------

// This entry point is used by the PAINT tool
// to restore the original display mode
//
void CDisplayer::resetDisplayMode()
{
	// establish the new mode
	displayMode = oldDisplayMode;

	// and the new zoom level
	zoomFactor = oldZoomFactor;
	MTIassert(zoomFactor > 0.0);
	if (zoomFactor <= 0.0)
	{
		zoomFactor = 1.0;
	}

#ifdef OLD
	if ((displayMode == DISPLAY_MODE_1_TO_1) && (zoomLevel == 0))
	{

		zoomLevel = 1;
	}
#endif

	// WAS: leave the zoom level where it is
	// but try to maintain the window center
	calculateImageRectangle();
	calculateDisplayRectangle(dspXctr, dspYctr);
	setBgndFlag(imgShrinkFlag);
}
//------------------------------------------------------------------------------

double CDisplayer::snapZoomFactor(double zmfctr)
{
	// We only want to snap in 1:1 mode!
	if (displayMode != DISPLAY_MODE_1_TO_1)
	{
		return zmfctr;
	}

	double max = 100.;
	double retVal = zmfctr;

	for (int i = 8; i > 1; i--)
	{
		double delta = fabs(zmfctr - 1 / (double)i);
		if (delta < max)
		{
			max = delta;
			retVal = 1 / (double)i;
		}
	}

	for (int i = 1; i < 20; i++)
	{
		double delta = fabs(zmfctr - (double)i);
		if (delta < max)
		{
			max = delta;
			retVal = (double)i;
		}
	}

	return retVal;
}
//------------------------------------------------------------------------------

// These methods adjust the zoom level of the Displayer. They
// cause adjustments to dspRect and imgRect.
//
bool CDisplayer::zoomAll()
{
	int w, h;
	if (mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		int sbsHalfOfBorderWidth = std::max<int>(borderWidth / 2, 5);
		int halfWidth = (visRect.right - visRect.left + 1) / 2;
		w = halfWidth - (sbsHalfOfBorderWidth * 3);
		h = (visRect.bottom - visRect.top + 1) - (sbsHalfOfBorderWidth * 4);
	}
	else
	{
		w = visRect.right - visRect.left + 1 - borderWidth * 2;
		h = visRect.bottom - visRect.top + 1 - borderWidth * 2;
	}

	// NEED?
	frameAspectRatio = ((double)frmWdth * pixelAspectRatio) / (double)frmHght;

	int frmWdthEven = (frmWdth & 0xfffffffe);

	if (displayMode == DISPLAY_MODE_TRUE_VIEW)
	{
		// find dims of the largest rectangle of correct
		// aspect ratio that fits within the client area
		imgWdth = w;
		imgHght = imgWdth / frameAspectRatio;
		if (imgHght > h)
		{
			imgHght = h;
			imgWdth = imgHght * frameAspectRatio;
		}

		// exact zoomAll
		zoomFactor = (double)imgHght / (double) frmHght;
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
	}
	else if (displayMode == DISPLAY_MODE_1_TO_1)
	{
		// find dims of the largest rectangle of correct
		// aspect ratio that fits within the client area
		imgWdth = w;
		imgHght = imgWdth * frmHght / frmWdth;
		if (imgHght > h)
		{
			imgHght = h;
			imgWdth = imgHght * frmWdth / frmHght;
		}

		// closest integral zoom/shrink
		zoomFactor = snapZoomFactor((double)imgHght / (double) frmHght);
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
	}
	else if (displayMode == DISPLAY_MODE_FIT_WIDTH)
	{
		// find dims of the largest rectangle of correct
		// aspect ratio that fits within the client area
		imgWdth = w;
		imgHght = imgWdth / frameAspectRatio;
		if (imgHght > h)
		{
			imgHght = h;
		}

		// show entire width
		zoomFactor = (double)imgWdth / ((double) frmWdth * (double) pixelAspectRatio);
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
	}

	calculateImageRectangle();
	calculateDisplayRectangle(frameWidth / 2, frameHeight / 2);
	calculateMouseFrameCoordinates();
	setBgndFlag(imgShrinkFlag);
	return (true);
}
//------------------------------------------------------------------------------

bool CDisplayer::zoomIn()
{
	double oldCenterX = (dspRect.left + dspRect.right + 1) / 2.0;
	double oldCenterY = (dspRect.top + dspRect.bottom + 1) / 2.0;
	int newCenterX;
	int newCenterY;
	double myOldZoomFactor = zoomFactor;

#if USE_ZOOM_STACK
	int oldMousePosX = zoomMousePosX;
	int oldMousePosY = zoomMousePosY;
	zoomMousePosX = -1;
	zoomMousePosY = -1;
	getMousePositionClient(&zoomMousePosX, &zoomMousePosY);
	if (zoomStackDirection == zoomingOut && !zoomStack.empty()
			&& zoomMousePosX == oldMousePosX && zoomMousePosY == oldMousePosY)
	{
		// "Undo" a previous zoom out.
		auto stackEntry = zoomStack.top();
		zoomStack.pop();
		zoomFactor = stackEntry.zoomLevel;
		newCenterX = stackEntry.x;
		newCenterY = stackEntry.y;
	}
	else
#endif
	{
		// Paranoia
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}

		if (displayMode == DISPLAY_MODE_1_TO_1)
		{
			zoomFactor = snapZoomFactor(zoomFactor);

			zoomFactor = (zoomFactor < 1.0) ? (1 / (double)((int)((1 / zoomFactor) + 0.5) - 1)) : (zoomFactor + 1.0);
		}
		else
		{
			// Increase zoom level by 10%.
			zoomFactor *= 1.1;
		}

		zoomFactor = std::min<double>(std::max<double>(zoomFactor, MIN_ZOOM_LEVEL), MAX_ZOOM_LEVEL);

		// Try to center the zoom at the cursor.
		double mousePositionFrameX;
		double mousePositionFrameY;
		if (!getMousePositionFrame(&mousePositionFrameX, &mousePositionFrameY))
		{
			mousePositionFrameX = oldCenterX;
			mousePositionFrameY = oldCenterY;
		}

		newCenterX = int(mousePositionFrameX - ((mousePositionFrameX - oldCenterX) * (myOldZoomFactor / zoomFactor)) + 0.5);
		newCenterY = int(mousePositionFrameY - ((mousePositionFrameY - oldCenterY) * (myOldZoomFactor / zoomFactor)) + 0.5);

#if USE_ZOOM_STACK
		if (zoomFactor != myOldZoomFactor)
		{
			if (zoomStackDirection != zoomingIn)
			{
				zoomStack = stack<zoomStackEntry>();
				zoomStackDirection = zoomingIn;
			}

			zoomStack.emplace(oldCenterX, oldCenterY, myOldZoomFactor);
		}
#endif
	}

	calculateImageRectangle();
	calculateDisplayRectangle(newCenterX, newCenterY);
	calculateMouseFrameCoordinates();

	setBgndFlag(imgShrinkFlag);
	return true;
}
//------------------------------------------------------------------------------

bool CDisplayer::zoomOut()
{
	double oldCenterX = (dspRect.left + dspRect.right + 1) / 2.0;
	double oldCenterY = (dspRect.top + dspRect.bottom + 1) / 2.0;
	int newCenterX;
	int newCenterY;
	double myOldZoomFactor = zoomFactor;

#if USE_ZOOM_STACK
	int oldMousePosX = zoomMousePosX;
	int oldMousePosY = zoomMousePosY;
	zoomMousePosX = -1;
	zoomMousePosY = -1;
	getMousePositionClient(&zoomMousePosX, &zoomMousePosY);
	if (zoomStackDirection == zoomingIn && !zoomStack.empty()
			&& zoomMousePosX == oldMousePosX && zoomMousePosY == oldMousePosY)
	{
		// "Undo" a previous zoom in.
		auto stackEntry = zoomStack.top();
		zoomStack.pop();
		zoomFactor = stackEntry.zoomLevel;
		newCenterX = stackEntry.x;
		newCenterY = stackEntry.y;
	}
	else
#endif
	{
		if (oldZoomFactor <= 0)
		{
			oldZoomFactor = 1.0;
		}

		if (displayMode == DISPLAY_MODE_1_TO_1)
		{
			zoomFactor = snapZoomFactor(zoomFactor);
			zoomFactor = (zoomFactor > 1.0) ? (zoomFactor - 1.0) : (1 / (double)((int)(1 / zoomFactor) + 1));
		}
		else
		{
			// Shrink to the size that we are presently 110% of.
			zoomFactor = (zoomFactor * 10) / 11;
		}

		zoomFactor = std::min<double>(std::max<double>(zoomFactor, MIN_ZOOM_LEVEL), MAX_ZOOM_LEVEL);

		// Try to center the zoom at the cursor.
		double mousePositionFrameX;
		double mousePositionFrameY;
		if (!getMousePositionFrame(&mousePositionFrameX, &mousePositionFrameY))
		{
			mousePositionFrameX = oldCenterX;
			mousePositionFrameY = oldCenterY;
		}

		newCenterX = int(mousePositionFrameX - ((mousePositionFrameX - oldCenterX) * (myOldZoomFactor / zoomFactor)) + 0.5);
		newCenterY = int(mousePositionFrameY - ((mousePositionFrameY - oldCenterY) * (myOldZoomFactor / zoomFactor)) + 0.5);

#if USE_ZOOM_STACK
		if (zoomFactor != myOldZoomFactor)
		{
			if (zoomStackDirection != zoomingOut)
			{
				zoomStack = stack<zoomStackEntry>();
				zoomStackDirection = zoomingOut;
			}

			zoomStack.emplace(oldCenterX, oldCenterY, myOldZoomFactor);
		}
#endif
	}

	calculateImageRectangle();
	calculateDisplayRectangle(newCenterX, newCenterY);
	calculateMouseFrameCoordinates();

	setBgndFlag(imgShrinkFlag);
	return true;
}
//------------------------------------------------------------------------------

void CDisplayer::setDisplayWindowCenter(int xc, int yc)
{
	dspXctr = xc;
	dspYctr = yc;

#ifdef DEBUG_FUCKING_RECTS
	TRACE_0(errout << "----------------------------");
	TRACE_0(errout << "AFTER setDisplayWindowCenter()");
	DumpAWholeBunchOfReallyUglyShit();
#endif
}
//------------------------------------------------------------------------------

int CDisplayer::getDisplayXctr()
{
	return dspXctr;
}
//------------------------------------------------------------------------------

int CDisplayer::getDisplayYctr()
{
	return dspYctr;
}
//------------------------------------------------------------------------------

bool CDisplayer::setZoomFactor(double zoomfct)
{
	if ((zoomfct > 0) && (zoomfct <= MAX_ZOOM_LEVEL))
	{

		zoomFactor = zoomfct;
		MTIassert(zoomFactor > 0.0);
		if (zoomFactor <= 0.0)
		{
			zoomFactor = 1.0;
		}
		calculateImageRectangle();
		calculateDisplayRectangle(dspXctr, dspYctr);
		calculateMouseFrameCoordinates();
		setBgndFlag(imgShrinkFlag);
		return (true);
	}
	return (false);
}
//------------------------------------------------------------------------------

double CDisplayer::getZoomFactor()
{
	return zoomFactor;
}
//------------------------------------------------------------------------------

void CDisplayer::panStart()
{
	panFlag = panMode;
	enterCursorOverrideMode(OpenHandCursor());
	zoomedOrPannedInMouseZoomMode = mouseZoomModeIsActive;
	panByGrabbing = mouseZoomModeIsActive;
	panGrabbedFramePointX = dxcurfrm;
	panGrabbedFramePointY = dycurfrm;
}
//------------------------------------------------------------------------------

void CDisplayer::panStop()
{
	panFlag = PAN_OFF;

	if (mouseZoomModeIsActive)
	{
		enterCursorOverrideMode(MagnifierCursor());
	}
	else
	{
		exitCursorOverrideMode();
	}

	panByGrabbing = false;
}
//------------------------------------------------------------------------------

void CDisplayer::setPanMode(int panmode)
{
	panMode = panmode;
}
//------------------------------------------------------------------------------

int CDisplayer::getPanMode()
{
	return panMode;
}
//------------------------------------------------------------------------------

void CDisplayer::panModeJog()
{
	if (panMode == PAN_XY)
	{
		panMode = PAN_X;
	}
	else
	{
		++panMode;
	}
}
//------------------------------------------------------------------------------

bool CDisplayer::isPanning()
{
	return (panFlag & PAN_XY) != 0;
}
//------------------------------------------------------------------------------

void CDisplayer::enterMouseZoomMode()
{
	if (mouseZoomModeIsActive)
	{
		// auto-repeat - we use that as a timer for how fast release has to be for resetting
		zoomedOrPannedInMouseZoomMode = true;
		return;
	}

	mouseZoomModeIsActive = true;
	zoomedOrPannedInMouseZoomMode = false;
	panByGrabbing = true;
	zoomStack = stack<zoomStackEntry>(); // clear
	zoomStackDirection = noDirection;
	enterCursorOverrideMode(MagnifierCursor());
}
//------------------------------------------------------------------------------

void CDisplayer::exitMouseZoomModeOrResetZoom()
{
	bool wasActive = mouseZoomModeIsActive;
	mouseZoomModeIsActive = false;
	if (!(wasActive && zoomedOrPannedInMouseZoomMode))
	{
		exitCursorOverrideMode();
		zoomAll();
	}
	else if (!isPanning())
	{
		exitCursorOverrideMode();
		panByGrabbing = false;
		zoomStack = stack<zoomStackEntry>(); // clear
		zoomStackDirection = noDirection;
	}
}
//------------------------------------------------------------------------------

// Uses the newDisplayWindowIndex to indicate how to
// set the Displayer's display context:
//
// Index           Setting
//
// -1       clip's last setting
// 0       current setting
// 1       full frame / true view
// 2       full frame / 1-to-1
//
void CDisplayer::setDisplayContext(int dspwindx, DISPLAY_CONTEXT &dspctxt)
{
	// No longer does anything!
}
//------------------------------------------------------------------------------

// Returns the key elements of the display context
//
DISPLAY_CONTEXT CDisplayer::getDisplayContext()
{
	DISPLAY_CONTEXT retval;

	retval.displayMode = displayMode;
	retval.zoomFactor = zoomFactor;
	retval.dspXctr = dspXctr;
	retval.dspYctr = dspYctr;

	return retval;
}
//------------------------------------------------------------------------------

// determines whether the display uses
//
// i) internal LUT
// ii) default  LUT
// iii) custom   LUT
//
void CDisplayer::setCustomLUTOption(int option)
{
#ifndef TEST
	customLUTOption = option;
#endif
}
//------------------------------------------------------------------------------

// returns the displayer LUT option
//
int CDisplayer::getCustomLUTOption()
{
	return customLUTOption;
}
//------------------------------------------------------------------------------

// set the color space index of the LUT:
//
// 0 - default
// 1 - linear
// 2 - log
// 3 - SMPTE 240
// 4 - CCIR 709
// 5 - CCIR 601BG
// 6 - CCIR 601M
//
void CDisplayer::setCustomLUTColorSpaceIndex(int colrspindx)
{
	customLUTColorSpaceIndex = colrspindx;
}
//------------------------------------------------------------------------------

// get the color space index (as above)
//
int CDisplayer::getCustomLUTColorSpaceIndex()
{
	return customLUTColorSpaceIndex;
}
//------------------------------------------------------------------------------

void CDisplayer::setCustomLUTTransferFunctionIndex(int xfrfnindx)
{
	customLUTTransferFunctionIndex = xfrfnindx;
}
//------------------------------------------------------------------------------

int CDisplayer::getCustomLUTTransferFunctionIndex()
{
	return customLUTTransferFunctionIndex;
}
//------------------------------------------------------------------------------

// enable/disable custom source range
//
void CDisplayer::setCustomLUTSourceRange(bool usecustom)
{
	customLUTSourceRangeEnabled = usecustom;
}
//------------------------------------------------------------------------------

// get custom LUT source range enabled flag
//
bool CDisplayer::getCustomLUTSourceRangeEnabled()
{
	return customLUTSourceRangeEnabled;
}
//------------------------------------------------------------------------------

// set the custom source range blk values
//
void CDisplayer::setCustomLUTSourceRangeBlk(MTI_UINT16 srcrngblk[])
{
	customLUTSourceRangeBlk[0] = srcrngblk[0];
	customLUTSourceRangeBlk[1] = srcrngblk[1];
	customLUTSourceRangeBlk[2] = srcrngblk[2];
}
//------------------------------------------------------------------------------

// set the custom source range whi values
//
void CDisplayer::setCustomLUTSourceRangeWhi(MTI_UINT16 srcrngwhi[])
{
	customLUTSourceRangeWhi[0] = srcrngwhi[0];
	customLUTSourceRangeWhi[1] = srcrngwhi[1];
	customLUTSourceRangeWhi[2] = srcrngwhi[2];
}
//------------------------------------------------------------------------------

// get the custom source range blk values
//
void CDisplayer::getCustomLUTSourceRangeBlk(MTI_UINT16 srcrngblk[])
{
	srcrngblk[0] = customLUTSourceRangeBlk[0];
	srcrngblk[1] = customLUTSourceRangeBlk[1];
	srcrngblk[2] = customLUTSourceRangeBlk[2];
}
//------------------------------------------------------------------------------

// get the custom source range whi values
//
void CDisplayer::getCustomLUTSourceRangeWhi(MTI_UINT16 srcrngwhi[])
{
	srcrngwhi[0] = customLUTSourceRangeWhi[0];
	srcrngwhi[1] = customLUTSourceRangeWhi[1];
	srcrngwhi[2] = customLUTSourceRangeWhi[2];
}
//------------------------------------------------------------------------------

// set the display range min values
//
void CDisplayer::setCustomLUTDisplayRangeMin(MTI_UINT16 dsprngmin[])
{
	customLUTDisplayRangeMin[0] = dsprngmin[0];
	customLUTDisplayRangeMin[1] = dsprngmin[1];
	customLUTDisplayRangeMin[2] = dsprngmin[2];
}
//------------------------------------------------------------------------------

// set the display range max values
//
void CDisplayer::setCustomLUTDisplayRangeMax(MTI_UINT16 dsprngmax[])
{
	customLUTDisplayRangeMax[0] = dsprngmax[0];
	customLUTDisplayRangeMax[1] = dsprngmax[1];
	customLUTDisplayRangeMax[2] = dsprngmax[2];
}
//------------------------------------------------------------------------------

// get the display range min values
//
void CDisplayer::getCustomLUTDisplayRangeMin(MTI_UINT16 dsprngmin[])
{
	dsprngmin[0] = customLUTDisplayRangeMin[0];
	dsprngmin[1] = customLUTDisplayRangeMin[1];
	dsprngmin[2] = customLUTDisplayRangeMin[2];
}
//------------------------------------------------------------------------------

// get the display range max values
//
void CDisplayer::getCustomLUTDisplayRangeMax(MTI_UINT16 dsprngmax[])
{
	dsprngmax[0] = customLUTDisplayRangeMax[0];
	dsprngmax[1] = customLUTDisplayRangeMax[1];
	dsprngmax[2] = customLUTDisplayRangeMax[2];
}
//------------------------------------------------------------------------------

void CDisplayer::setCustomLUTGamma(int gamma)
{
	customLUTGamma = gamma;
}
//------------------------------------------------------------------------------

int CDisplayer::getCustomLUTGamma()
{
	return customLUTGamma;
}
//------------------------------------------------------------------------------

void CDisplayer::applyLUTSettings(CDisplayLutSettings &settings)
{
	MTI_UINT16 rangeMin[3];
	MTI_UINT16 rangeMax[3];

	if (imgFmt == nullptr)
	{
		return;
	}

	bool defaultIsEXR = imgFmt->getColorSpace() == IF_COLOR_SPACE_EXR;

	if (settings.UsingDefaultSourceColorSpace() && (defaultIsEXR ? settings.UsingDefaultSourceRangeExr() :
			settings.UsingDefaultSourceRange()) && settings.UsingDefaultDisplayRange()
			&& settings.UsingDefaultDisplayGamma())
	{
		setCustomLUTOption(LUT_OPTION_DEFAULT);
		setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_DEFAULT);
	}
	else if ((!settings.UsingDefaultSourceColorSpace()) && settings.GetSourceColorSpace() == LUTCS_INVALID)
	{
		setCustomLUTOption(LUT_OPTION_INTERNAL);
		setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_DEFAULT);
	}
	else
	{
		setCustomLUTOption(LUT_OPTION_CUSTOM);
		if (settings.UsingDefaultSourceColorSpace())
		{
			setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_DEFAULT);
		}
		else
		{
			switch (settings.GetSourceColorSpace())
			{
				case LUTCS_LINEAR:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_LINEAR);
					break;
				case LUTCS_LOG:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_LOG);
					break;
				case LUTCS_EXR:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_EXR);
					break;
				case LUTCS_SMPTE_240:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_SMPTE_240);
					break;
				case LUTCS_CCIR_709:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_CCIR_709);
					break;
				case LUTCS_CCIR_601M:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_CCIR_601_M);
					break;
				case LUTCS_CCIR_601BG:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_CCIR_601_BG);
					break;
				default:
					setCustomLUTColorSpaceIndex(COLOR_SPACE_INDEX_DEFAULT);
					break;
			}
		}

		// We don't do custom transfer function anymore
		setCustomLUTTransferFunctionIndex(0); // QQQ default

		// Source range
		if ((this->imgFmt)->getColorSpace() == IF_COLOR_SPACE_EXR)
		{
			setCustomLUTSourceRange(!settings.UsingDefaultSourceRangeExr());
			settings.GetSourceRangeExr(rangeMin, rangeMax);
		}
		else
		{
			setCustomLUTSourceRange(!settings.UsingDefaultSourceRange());
			settings.GetSourceRange(getBitsPerComponent(), rangeMin, rangeMax);
		}

		setCustomLUTSourceRangeBlk(rangeMin);
		setCustomLUTSourceRangeWhi(rangeMax);

		// Display range
		if (settings.UsingDefaultDisplayRange())
		{
			rangeMin[0] = rangeMin[1] = rangeMin[2] = 0;
			rangeMax[0] = rangeMax[1] = rangeMax[2] = 255;
		}
		else
		{
			settings.GetDisplayRange(rangeMin, rangeMax);
		}

		setCustomLUTDisplayRangeMin(rangeMin);
		setCustomLUTDisplayRangeMax(rangeMax);

		// Display gamma
		if (settings.UsingDefaultDisplayGamma())
		{
			setCustomLUTGamma(mainWindow->GetGammaPreference());
		}
		else
		{
			float gamma = settings.GetDisplayGammaTimes10();
			setCustomLUTGamma(gamma);
		}
	}

	// HACK!!! DO NOT TRY TO ESTABLISH THE LUT IF THE imgFmt IS NOT
	// DEFINED YET - IT WILL CRASH!!!
	if (imgFmt != nullptr)
	{
		establishDisplayLUT();
	}
}
//------------------------------------------------------------------------------

void CDisplayer::getLUTSettings(CDisplayLutSettings &settings)
{
	MTI_UINT16 rangeMin[3];
	MTI_UINT16 rangeMax[3];

	int customLutOption = getCustomLUTOption();
	if (customLutOption == LUT_OPTION_CUSTOM)
	{
		int colorSpaceIndex = getCustomLUTColorSpaceIndex();
		if (colorSpaceIndex == COLOR_SPACE_INDEX_DEFAULT)
		{
			settings.UseDefaultSourceColorSpace(true);
		}
		else
		{
			settings.UseDefaultSourceColorSpace(false);
			switch (getCustomLUTColorSpaceIndex())
			{
				case COLOR_SPACE_INDEX_LINEAR:
					settings.SetSourceColorSpace(LUTCS_LINEAR);
					break;
				case COLOR_SPACE_INDEX_LOG:
					settings.SetSourceColorSpace(LUTCS_LOG);
					break;
				case COLOR_SPACE_INDEX_EXR:
					settings.SetSourceColorSpace(LUTCS_EXR);
					break;
				case COLOR_SPACE_INDEX_SMPTE_240:
					settings.SetSourceColorSpace(LUTCS_SMPTE_240);
					break;
				case COLOR_SPACE_INDEX_CCIR_709:
					settings.SetSourceColorSpace(LUTCS_CCIR_709);
					break;
				case COLOR_SPACE_INDEX_CCIR_601_BG:
					settings.SetSourceColorSpace(LUTCS_CCIR_601BG);
					break;
				case COLOR_SPACE_INDEX_CCIR_601_M:
					settings.SetSourceColorSpace(LUTCS_CCIR_601M);
					break;
				default:
					settings.SetSourceColorSpace(LUTCS_INVALID);
					break;
			}
		}

		if (getCustomLUTSourceRangeEnabled())
		{
			settings.UseDefaultSourceRange(true);
		}
		else
		{
			settings.UseDefaultSourceRange(false);
			getCustomLUTSourceRangeBlk(rangeMin);
			getCustomLUTSourceRangeWhi(rangeMax);
			settings.SetSourceRange(getBitsPerComponent(), rangeMin, rangeMax);
		}

		getCustomLUTDisplayRangeMin(rangeMin);
		getCustomLUTDisplayRangeMax(rangeMax);
		if (rangeMin[0] == 0 && rangeMin[1] == 0 && rangeMin[2] == 0 && rangeMax[0] == 255 && rangeMax[1]
				== 255 && rangeMax[2] == 255)
		{
			settings.UseDefaultDisplayRange(true);
		}
		else
		{
			settings.UseDefaultDisplayRange(false);
			settings.SetDisplayRange(rangeMin, rangeMax);
		}

		if (getCustomLUTGamma() == mainWindow->GetGammaPreference())
		{
			settings.UseDefaultDisplayGamma(true);
		}
		else
		{
			settings.UseDefaultDisplayGamma(false);
			settings.SetDisplayGamma(getCustomLUTGamma());
		}
	}
	else
	{
		settings.UseDefaultSourceColorSpace(true);
		settings.UseDefaultSourceRange(true);
		settings.UseDefaultDisplayRange(true);
		settings.UseDefaultDisplayGamma(true);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::establishDisplayLUT()
{
	int retVal = attemptEstablishDisplayLUT();
	MTIassert(retVal == 0)
	if (retVal != 0)
	{
		setCustomLUTOption(LUT_OPTION_INTERNAL);
		attemptEstablishDisplayLUT(); // can't fail with internal
	}
}
//------------------------------------------------------------------------------

int CDisplayer::attemptEstablishDisplayLUT()
{
	if (getCustomLUTOption() == LUT_OPTION_INTERNAL)
	{
		currentLUTPtr = nullptr;
	}
	else
	{
		// default or custom LUT
		// delete the current image format
		delete currentImageFormat;

		// make a copy of the clip's image format
		currentImageFormat = new CImageFormat;
		*currentImageFormat = *imgFmt;

		if (getCustomLUTOption() == LUT_OPTION_DEFAULT)
		{
			// color space is default in currentImageFormat
			EColorSpace colsp = imgFmt->getColorSpace();
			currentDisplayFormat.bSourceLogSpace = colsp == IF_COLOR_SPACE_LOG || colsp == IF_COLOR_SPACE_EXR;

			// HACK! If the color space is EXR, use black and white points for min and max!
			// In fact, I think it should ALWAYS use black and white points, but in KurtKode
			// it is never a good idea to fix things that are broken but behave correctly!
			if (colsp == IF_COLOR_SPACE_EXR)
			{
				unsigned short componentBlack[3];
				CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_BLACK,
						currentImageFormat->getPixelComponents(), colsp, false, currentImageFormat->getBitsPerComponent(),
						componentBlack);
				currentImageFormat->setComponentValueMin(componentBlack);

				unsigned short componentWhite[3];
				CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_WHITE,
						currentImageFormat->getPixelComponents(), colsp, false, currentImageFormat->getBitsPerComponent(),
						componentWhite);
				currentImageFormat->setComponentValueMax(componentWhite);
			}

			// source range is default in currentImageFormat

			// display range is 0-255
			currentDisplayFormat.faDisplayMin[0] = 0.;
			currentDisplayFormat.faDisplayMin[1] = 0.;
			currentDisplayFormat.faDisplayMin[2] = 0.;
			currentDisplayFormat.faDisplayMax[0] = 1.;
			currentDisplayFormat.faDisplayMax[1] = 1.;
			currentDisplayFormat.faDisplayMax[2] = 1.;

			// set the current gamma
			currentDisplayFormat.fDisplayGamma = ((float)frameGamma) / 10.;
		}
		else
		{ // customLUTOption = LUT_OPTION_CUSTOM

			EColorSpace colsp;

			switch (customLUTColorSpaceIndex)
			{
				case COLOR_SPACE_INDEX_DEFAULT:
					colsp = currentImageFormat->getColorSpace();
					break;

				case COLOR_SPACE_INDEX_LINEAR:
					colsp = IF_COLOR_SPACE_LINEAR;
					break;

				case COLOR_SPACE_INDEX_LOG:
					colsp = IF_COLOR_SPACE_LOG;
					break;

				case COLOR_SPACE_INDEX_EXR:
					colsp = IF_COLOR_SPACE_EXR;
					break;

				case COLOR_SPACE_INDEX_SMPTE_240:
					colsp = IF_COLOR_SPACE_SMPTE_240;
					break;

				case COLOR_SPACE_INDEX_CCIR_709:
					colsp = IF_COLOR_SPACE_CCIR_709;
					break;

				case COLOR_SPACE_INDEX_CCIR_601_BG:
					colsp = IF_COLOR_SPACE_CCIR_601_BG;
					break;

				case COLOR_SPACE_INDEX_CCIR_601_M:
					colsp = IF_COLOR_SPACE_CCIR_601_M;
					break;
			}

			// Deal with a custom transfer function
			if (customLUTTransferFunctionIndex != customLUTColorSpaceIndex)
			{
				EColorSpace xfrfn;
				bool tellDisplayer = true;

				switch (customLUTTransferFunctionIndex)
				{
					default:
						tellDisplayer = false;
						break;

					case COLOR_SPACE_INDEX_LINEAR:
						xfrfn = IF_COLOR_SPACE_LINEAR;
						break;

					case COLOR_SPACE_INDEX_SMPTE_240:
						xfrfn = IF_COLOR_SPACE_SMPTE_240;
						break;

					case COLOR_SPACE_INDEX_CCIR_709:
						xfrfn = IF_COLOR_SPACE_CCIR_709;
						break;

					case COLOR_SPACE_INDEX_CCIR_601_BG:
						xfrfn = IF_COLOR_SPACE_CCIR_601_BG;
						break;

					case COLOR_SPACE_INDEX_CCIR_601_M:
						xfrfn = IF_COLOR_SPACE_CCIR_601_M;
						break;
				}

				if (tellDisplayer)
				{
					currentImageFormat->setTransferCharacteristic(xfrfn);
				}
			}

			// load this into the image format copy
			currentImageFormat->setColorSpace(colsp);
			currentDisplayFormat.bSourceLogSpace = colsp == IF_COLOR_SPACE_LOG || colsp == IF_COLOR_SPACE_EXR;

			// if we use custom source ranges, load them in, too
			if (customLUTSourceRangeEnabled)
			{
				currentImageFormat->setComponentValueMin(customLUTSourceRangeBlk);
				currentImageFormat->setComponentValueMax(customLUTSourceRangeWhi);
			}

			// load the custom LUT display ranges
			for (int i = 0; i < 3; i++)
			{
				currentDisplayFormat.faDisplayMin[i] = ((float)customLUTDisplayRangeMin[i]) / 255.;
				currentDisplayFormat.faDisplayMax[i] = ((float)customLUTDisplayRangeMax[i]) / 255.;
			}

			// and finally the custom LUT gamma
			currentDisplayFormat.fDisplayGamma = ((float)customLUTGamma) / 10.;
		}

		// now create a LUT based on the image format & display format
		lutmgr->Release(currentDisplayLUT);
		currentDisplayLUT = lutmgr->Create(*currentImageFormat, currentDisplayFormat);

		if (currentDisplayLUT != 0)
		{
			currentLUTPtr = currentDisplayLUT->getTablePtr();
		}
		else
		{
			return -1;
		}
	}

	return 0;
}
//------------------------------------------------------------------------------

// gets the format's color space
//
EColorSpace CDisplayer::getFormatColorSpace()
{
	if (imgFmt == nullptr)
	{
		return IF_COLOR_SPACE_INVALID;
	}
	else
	{
		return frameColorSpace;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::setImageJustification(int jstfy)
{
	justifyImage = jstfy;
	calculateImageRectangle(); // recalculate frame bounds
}

int CDisplayer::getImageJustification()
{
	return (justifyImage);
}
//------------------------------------------------------------------------------

void CDisplayer::setRGBChannelsMask(int mask)
{
	RGBChannelsMask = mask;
}
//------------------------------------------------------------------------------

int CDisplayer::getRGBChannelsMask()
{
	return (RGBChannelsMask);
}
//------------------------------------------------------------------------------

void CDisplayer::setAlphaColor(MTI_UINT32 alphacol)
{
	// QQQ Should we set _compareFrameConverter as well?
	_mainFrameConverter->setAlphaColor(alphacol);
}
//------------------------------------------------------------------------------

MTI_UINT32 CDisplayer::getAlphaColor()
{
	return _mainFrameConverter->getAlphaColor();
}
//------------------------------------------------------------------------------

bool CDisplayer::isAlphaEnabled()
{
	if (imgFmt == 0)
	{
		return false;
	}

	return imgFmt->hasNonHiddenAlphaMatte();
}
//------------------------------------------------------------------------------

// returns the pixel components of the loaded clip
//
EPixelComponents CDisplayer::getPixelComponents()
{
	if (imgFmt == nullptr)
	{
		return IF_PIXEL_COMPONENTS_INVALID;
	}

	return framePixelComponents;
}
//------------------------------------------------------------------------------

// returns the number of bits per component
//
int CDisplayer::getBitsPerComponent()
{
	if (imgFmt == nullptr)
	{
		return 0;
	}

	return frameBitsPerComponent;
}
//------------------------------------------------------------------------------

// returns the min value of each component
//
int CDisplayer::getComponentBlack(int i)
{
	if (imgFmt == nullptr)
	{
		return 0;
	}
	else
	{
		return frameColorSpaceBlack[i];
	}
}
//------------------------------------------------------------------------------

// returns the max value of each component
//
int CDisplayer::getComponentWhite(int i)
{
	if (imgFmt == nullptr)
	{
		return 0;
	}

	return frameColorSpaceWhite[i];
}
//------------------------------------------------------------------------------

// sets the default display gamma
void CDisplayer::setDefaultGamma(int gamma)
{
	frameGamma = gamma;
}
//------------------------------------------------------------------------------

// gets the default display gamma
int CDisplayer::getDefaultGamma()
{
	return frameGamma;
}
//------------------------------------------------------------------------------

// sets the current use gamma
void CDisplayer::setCurrentGamma(int gamma)
{
	currentDisplayFormat.fDisplayGamma = ((float)gamma) / 10.;
}
//------------------------------------------------------------------------------

// gets the current use gamma
int CDisplayer::getCurrentGamma()
{
	return ((int)(10. * currentDisplayFormat.fDisplayGamma));
}
//------------------------------------------------------------------------------

// These methods support Displayer Magnifiers, so far used only for the
// the oscilloscope displays in Scratch, but soon to be used for a simple
// magnifier tool. Note that the magnifiers still use GDI for display.
// --- UPDATE 10/10/10 - No, they don't.
//
// Allocate a Displayer magnifier
//
CMagnifier * CDisplayer::allocateMagnifier(int bmwdth, int bmhght, RECT *magwin)
{
	if (freeMagnifiers == 0)
	{
		return nullptr;
	}

	int i;
	for (i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] == nullptr)
		{
			break;
		}
	}

	magptr[i] = new CMagnifier;
	if (magptr[i]->initMagnifier(bmwdth, bmhght, imgFmt, magwin) == -1)
	{
		delete magptr[i];
		magptr[i] = nullptr;
		return nullptr;
	}

	// success - one less free
	freeMagnifiers--;

	return magptr[i];
}

// Activate one of the Displayer's magnifiers, passing it a
// pointer to a refresh routine which will be called whenever
// DisplayFrame is called.
//
void CDisplayer::activateMagnifier(CMagnifier *mag, void(*rfrcallback)(void *obj, void *mag), void *rfrobj)
{
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] == mag)
		{
			mag->activateMagnifier(rfrcallback, rfrobj);
			return;
		}
	}
}
//------------------------------------------------------------------------------

// Deactivate one of the Displayer's magnifiers
//
void CDisplayer::deactivateMagnifier(CMagnifier *mag)
{
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] == mag)
		{
			mag->deactivateMagnifier();
			return;
		}
	}
}
//------------------------------------------------------------------------------

// Free one of the Displayer's magnifiers
//
void CDisplayer::freeMagnifier(CMagnifier *mag)
{
	for (int i = 0; i < MAX_MAGNIFIERS; i++)
	{
		if (magptr[i] == mag)
		{
			delete magptr[i];
			magptr[i] = nullptr;
			freeMagnifiers++;
		}
	}
}
//------------------------------------------------------------------------------

// BASIC RECTANGLE METHODS -----------------------------------------------------

// returns TRUE iff the arg rectangle is non-nullptr
bool CDisplayer::rectangleIsValid(RECT *rct)
{
	return (rct->left <= rct->right) && (rct->top <= rct->bottom);
}
//------------------------------------------------------------------------------

// rectangles r1 and r2 are ANDed and the result
// placed in r1. Returns TRUE iff the result is
// non-nullptr
bool CDisplayer::rectangleAND(RECT *r1, RECT *r2)
{
	if (r2->left > r1->left)
	{
		r1->left = r2->left;
	}

	if (r2->top > r1->top)
	{
		r1->top = r2->top;
	}

	if (r2->right < r1->right)
	{
		r1->right = r2->right;
	}

	if (r2->bottom < r1->bottom)
	{
		r1->bottom = r2->bottom;
	}

	// tell the user if the result is non-nullptr
	return rectangleIsValid(r1);
}
//------------------------------------------------------------------------------

void CDisplayer::clipPointClient(POINT *pt)
{
	if (pt->x < imgRect.left)
	{
		pt->x = imgRect.left;
	}

	if (pt->x > imgRect.right)
	{
		pt->x = imgRect.right;
	}

	if (pt->y < imgRect.top)
	{
		pt->y = imgRect.top;
	}

	if (pt->y > imgRect.bottom)
	{
		pt->y = imgRect.bottom;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::clipRectangleClient(RECT *rct)
{
	rectangleAND(rct, &visRect);
}
//------------------------------------------------------------------------------

void CDisplayer::clipLassoClient(POINT *lasso)
{
	clipPointClient(&lasso[0]);
	for (int i = 1; ; i++)
	{
		clipPointClient(&lasso[i]);
		if ((lasso[i].x == lasso[0].x) && (lasso[i].x == lasso[0].x))
		{
			break;
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::clipPointFrame(POINT *pt)
{
	if (pt->x < frmRect.left)
	{
		pt->x = frmRect.left;
	}

	if (pt->x > frmRect.right)
	{
		pt->x = frmRect.right;
	}

	if (pt->y < frmRect.top)
	{
		pt->y = frmRect.top;
	}

	if (pt->y > frmRect.bottom)
	{
		pt->y = frmRect.bottom;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::clipRectangleFrame(RECT *rct)
{
	rectangleAND(rct, &frmRect);
}
//------------------------------------------------------------------------------

void CDisplayer::clipLassoFrame(POINT *lasso)
{
	clipPointFrame(&lasso[0]);
	for (int i = 1; ; i++)
	{
		clipPointFrame(&lasso[i]);
		if ((lasso[i].x == lasso[0].x) && (lasso[i].y == lasso[0].y))
		{
			break;
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::fattenRectangleClient(RECT *rct, int fat)
{
	rct->left -= fat;
	rct->top -= fat;
	rct->right += fat;
	rct->bottom += fat;
	clipRectangleClient(rct);
}
//------------------------------------------------------------------------------

void CDisplayer::fattenRectangleFrame(RECT *rct, int fat)
{
	rct->left -= fat;
	rct->top -= fat;
	rct->right += fat;
	rct->bottom += fat;
	clipRectangleFrame(rct);
}
//------------------------------------------------------------------------------

// return true if the client point is within the frame rect
bool CDisplayer::isPointInFrameRect(POINT inPt)
{
	POINT clipPt = inPt;

	clipPointClient(&clipPt);

	return (clipPt.x == inPt.x && clipPt.y == inPt.y);
}
//------------------------------------------------------------------------------

// return true if mouse position is in frame rect
// regardless of where the tool is -- important!
bool CDisplayer::isMouseInFrame()
{
	POINT pt;
	int x = -1;
	int y = -1;

	getMousePositionClient(&x, &y);
	pt.x = (long) x;
	pt.y = (long) y;

	return isPointInFrameRect(pt);
}
//------------------------------------------------------------------------------

void CDisplayer::lineSegmentExtent(RECT *ext, POINT *p1, POINT *p2)
{
	ext->left = p1->x;
	ext->right = p2->x;
	if (p1->x > p2->x)
	{
		ext->left = p2->x;
		ext->right = p1->x;
	}

	ext->top = p1->y;
	ext->bottom = p2->y;
	if (p1->y > p2->y)
	{
		ext->top = p2->y;
		ext->bottom = p1->y;
	}
}
//------------------------------------------------------------------------------

int CDisplayer::scaleXFrameToClient(int x)
{
	return rgbRect.left + (x - dspRect.left) * XScaleFrameToClient;
}
//------------------------------------------------------------------------------

int CDisplayer::scaleYFrameToClient(int y)
{
	return rgbRect.top + (y - dspRect.top) * YScaleFrameToClient;
}
//------------------------------------------------------------------------------

int CDisplayer::dscaleXFrameToClient(double x)
{
	return (int)((double)rgbRect.left + (double)(x - dspRect.left) * XScaleFrameToClient);
}
//------------------------------------------------------------------------------

int CDisplayer::dscaleYFrameToClient(double y)
{
	return (int)((double)rgbRect.top + (double)(y - dspRect.top) * YScaleFrameToClient);
}
//------------------------------------------------------------------------------

double CDisplayer::ddscaleXFrameToClient(double x)
{
	return ((double)rgbRect.left) + (x - dspRect.left) * XScaleFrameToClient;
}
//------------------------------------------------------------------------------

double CDisplayer::ddscaleYFrameToClient(double y)
{
	return ((double)rgbRect.top) + (y - dspRect.top) * YScaleFrameToClient;
}
//------------------------------------------------------------------------------

void CDisplayer::scalePointFrameToClient(POINT *pt)
{
	pt->x = scaleXFrameToClient(pt->x);
	pt->y = scaleYFrameToClient(pt->y);
}
//------------------------------------------------------------------------------

int CDisplayer::scaleXClientToFrame(int x)
{
	return dspRect.left + (x - rgbRect.left) * XScaleClientToFrame;
}
//------------------------------------------------------------------------------

int CDisplayer::scaleYClientToFrame(int y)
{
	return dspRect.top + (y - rgbRect.top) * YScaleClientToFrame;
}
//------------------------------------------------------------------------------

// These two routines map client coordinates to frame coordinates.
// To map a client X coordinate to a frame coordinate, multiply the
// displacement from rgbRect.left by XScaleClientToFrame. But what
// we really want to do is to scale a representative floating pt
// displacement from rgbRect.left. As we go from rgbRect.left to
// rgbRect.right, we'd like this floating pt number to be the nominal
// integer coordinate, PLUS a fraction of a pixel which scales from 0
// to 1 as we move from extreme left to extreme right. This is how
// we get the expression (fdelx + fdelx/rgbRectWidth) below as the floating
// pt displacement we want to scale. To get the effect of this, we use
// a pre-calculated scale factor which differs from XScaleClientToFrame.
// Calculation for client Y coordinates is similar.
//
int CDisplayer::scaleCursorXClientToFrame(int x)
{
	double fdelx = (double)(x - rgbRect.left);
	// return dspRect.left + (int)((fdelx + fdelx/rgbRectWidth)*XScaleClientToFrame);
	// see initialization of XScaleCursorToFrame in "calculateImageRectangle"
	return dspRect.left + (int)(fdelx * XScaleCursorToFrame);
}
//------------------------------------------------------------------------------

int CDisplayer::scaleCursorYClientToFrame(int y)
{
	double fdely = (double)(y - rgbRect.top);
	// return dspRect.top  + (int)((fdely + fdely/rgbRectHeight)*YScaleClientToFrame);
	// see initialization of YScaleCursorToFrame in "calculateImageRectangle".
	return dspRect.top + (int)(fdely * YScaleCursorToFrame);
}
//------------------------------------------------------------------------------

int CDisplayer::scaleXClientToImageX(int x)
{
	// save this in case dspRect is changed
	X = x;
	return dspRect.left + (x - rgbRect.left) * XScaleClientToFrame;
}
//------------------------------------------------------------------------------

int CDisplayer::scaleYClientToImageY(int y)
{
	// save this in case dspRect is changed
	Y = y;
	return dspRect.top + (y - rgbRect.top) * YScaleClientToFrame;
}
//------------------------------------------------------------------------------

double CDisplayer::dscaleXClientToFrame(int x)
{
	return (double)dspRect.left + (double)(x - rgbRect.left) / XScaleFrameToClient;
}
//------------------------------------------------------------------------------

double CDisplayer::dscaleYClientToFrame(int y)
{
	return (double)dspRect.top + (double)(y - rgbRect.top) / YScaleFrameToClient;
}
//------------------------------------------------------------------------------

void CDisplayer::scalePointClientToFrame(POINT *pt)
{
	pt->x = scaleXClientToFrame(pt->x);
	pt->y = scaleYClientToFrame(pt->y);
}
//------------------------------------------------------------------------------

void CDisplayer::scaleRectangleFrameToClient(RECT *rct)
{
	rct->left = scaleXFrameToClient(rct->left);
	rct->top = scaleYFrameToClient(rct->top);
	rct->right = scaleXFrameToClient(rct->right);
	rct->bottom = scaleYFrameToClient(rct->bottom);
}
//------------------------------------------------------------------------------

void CDisplayer::scaleRectangleClientToFrame(RECT *rct)
{
	rct->left = scaleXClientToFrame(rct->left);
	rct->top = scaleYClientToFrame(rct->top);
	rct->right = scaleXClientToFrame(rct->right);
	rct->bottom = scaleYClientToFrame(rct->bottom);
}
//------------------------------------------------------------------------------

double CDisplayer::scaleClientXToRgbRectNormalized(int clientX)
{
	int x = std::min<int>(rgbRect.right, std::max<int>(rgbRect.left, clientX));
	return (x - rgbRect.left) / double(rgbRectWidth);
}
//------------------------------------------------------------------------------

double CDisplayer::scaleClientYToRgbRectNormalized(int clientY)
{
	int y = std::min<int>(rgbRect.bottom, std::max<int>(rgbRect.top, clientY));
	return (clientY - rgbRect.top) / double(rgbRectHeight);
}
//------------------------------------------------------------------------------

void CDisplayer::drawRectangleClientNoFlush(RECT *rect)
{
	auto &clrct = *rect;
	moveToClient(clrct.left, clrct.top);
	lineToClientNoFlush(clrct.right, clrct.top);
	lineToClientNoFlush(clrct.right, clrct.bottom);
	lineToClientNoFlush(clrct.left, clrct.bottom);
	lineToClientNoFlush(clrct.left, clrct.top);
}
//------------------------------------------------------------------------------

// this method does not show the edges where it's clipped
//
void CDisplayer::clipAndDrawRectangleFrame(RECT *rct)
{
	clipToImageRectangle();

	RECT frmrct = *rct;
	frmrct.right++;
	frmrct.bottom++;

	// scale the rectangle to client coordinates
	RECT clrct;
	clrct.left = dscaleXFrameToClient(frmrct.left);
	clrct.top = dscaleYFrameToClient(frmrct.top);
	clrct.right = dscaleXFrameToClient(frmrct.right);
	clrct.bottom = dscaleYFrameToClient(frmrct.bottom);

	// draw the rectangle using the clip methods
	moveToClient(clrct.left, clrct.top);
	lineToClientNoFlush(clrct.right, clrct.top);
	lineToClientNoFlush(clrct.right, clrct.bottom);
	lineToClientNoFlush(clrct.left, clrct.bottom);
	lineToClientNoFlush(clrct.left, clrct.top);
	flushGraphics();

	// restore default clipping
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

void CDisplayer::clipAndFillRectangleFrame(RECT *rct)
{
	RECT fmrct = *rct;

	RECT dprct = dspRect;
	dprct.right++;
	dprct.bottom++;

	if (!rectangleAND(&fmrct, &dprct))
	{
		return;
	}

	RECT clrct;
	clrct.left = dscaleXFrameToClient(fmrct.left);
	clrct.top = dscaleYFrameToClient(fmrct.top);
	clrct.right = dscaleXFrameToClient(fmrct.right) + 1;
	clrct.bottom = dscaleYFrameToClient(fmrct.bottom) + 1;

	drawFilledRectangleNoFlush(clrct);
	flushGraphics();
}
//------------------------------------------------------------------------------

// this method shows the edges where it's clipped
//
void CDisplayer::drawRectangleFrame(RECT *rct)
{
#if 1 //def OLD_CLIPPING
	clipToImageRectangle();
#else
	clipToVisibleRectangle();
#endif

	RECT frmrct = *rct;
	frmrct.right++;
	frmrct.bottom++;

	bool topclp = false;
	bool botclp = false;
	bool lftclp = false;
	bool rgtclp = false;

	// scale the rectangle to client coordinates
	RECT clrct;
	clrct.left = dscaleXFrameToClient(frmrct.left);
	clrct.top = dscaleYFrameToClient(frmrct.top);
	clrct.right = dscaleXFrameToClient(frmrct.right);
	clrct.bottom = dscaleYFrameToClient(frmrct.bottom);

	// now clip the resulting rectangle
	if (clrct.left < clpRect.left)
	{
		clrct.left = clpRect.left;
		lftclp = true;
	}

	if (clrct.top < clpRect.top)
	{
		clrct.top = clpRect.top;
		topclp = true;
	}

	if (clrct.right > clpRect.right)
	{
		clrct.right = clpRect.right;
		rgtclp = true;
	}

	if (clrct.bottom > clpRect.bottom)
	{
		clrct.bottom = clpRect.bottom;
		botclp = true;
	}

	// if it's valid, display it ... clipped lines are shown as dashed
	if (rectangleIsValid(&clrct))
	{

		int oldGraphicsColor = graphicsColor;
		moveToClient(clrct.left, clrct.top);

		if ((!topclp) && (graphicsColor & 1))
		{
			setGraphicsColor(graphicsColor & ~1);
		}

		if (topclp && ((graphicsColor & 1) == 0))
		{
			setGraphicsColor(graphicsColor | 1);
		}

		lineToClientNoFlush(clrct.right, clrct.top);

		if ((!rgtclp) && (graphicsColor & 1))
		{
			setGraphicsColor(graphicsColor & ~1);
		}

		if (rgtclp && ((graphicsColor & 1) == 0))
		{
			setGraphicsColor(graphicsColor | 1);
		}

		lineToClientNoFlush(clrct.right, clrct.bottom);

		if ((!botclp) && (graphicsColor & 1))
		{
			setGraphicsColor(graphicsColor & ~1);
		}

		if (botclp && ((graphicsColor & 1) == 0))
		{
			setGraphicsColor(graphicsColor | 1);
		}

		lineToClientNoFlush(clrct.left, clrct.bottom);

		if ((!lftclp) && (graphicsColor & 1))
		{
			setGraphicsColor(graphicsColor & ~1);
		}

		if (lftclp && ((graphicsColor & 1) == 0))
		{
			setGraphicsColor(graphicsColor | 1);
		}

		lineToClientNoFlush(clrct.left, clrct.top);

		if (oldGraphicsColor != graphicsColor)
		{
			setGraphicsColor(oldGraphicsColor);
		}

		// now the edges around imgRect
		drawTransitionsClientNoFlush();

		// makes it happen
		flushGraphics();
	}

	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

void CDisplayer::drawLassoFrame(POINT *lasso)
{
	moveToFrame(lasso[0].x, lasso[0].y);

	for (int i = 1; ; i++)
	{
		lineToFrameNoFlush(lasso[i].x, lasso[i].y);

		if ((i > 2) && (lasso[i].x == lasso[0].x) && (lasso[i].y == lasso[0].y))
		{
			break;
		}
	}

	flushGraphics();
}
//------------------------------------------------------------------------------

void CDisplayer::drawBezierFrame(BEZIER_POINT *bezier)
{
	moveToFrame(bezier[0].x, bezier[0].y);

	for (int i = 1; ; i++)
	{
		splineToFrameNoFlush(bezier[i - 1].xctrlnxt, bezier[i - 1].yctrlnxt, bezier[i].xctrlpre, bezier[i].yctrlpre,
				bezier[i].x, bezier[i].y);

		if ((i > 1) && (bezier[i].xctrlpre == bezier[0].xctrlpre) && (bezier[i].yctrlpre == bezier[0].yctrlpre) &&
				(bezier[i].x == bezier[0].x) && (bezier[i].y == bezier[0].y) && (bezier[i].xctrlnxt == bezier[0].xctrlnxt)
				&& (bezier[i].yctrlnxt == bezier[0].yctrlnxt))
		{
			break;
		}
	}

	flushGraphics();
}
//------------------------------------------------------------------------------

void CDisplayer::drawBrushFrame(CBrush *brsh, int color, double x, double y)
{
	x += 0.5;
	y += 0.5;

	if (zoomFactor >= 8)
	{
		x = (int)(x) + 0.5;
		y = (int)(y) + 0.5;
	}

	// clip the brush to the IMAGE rectangle
	clipToImageRectangle();

	// these determine the brush envelope
	// note that radius 0 generates a box 1x1
	double Radius = (double)brsh->getRadius() + 0.5;
	double Aspect = (double)brsh->getAspect() / 100.;
	double Angle = (double)brsh->getAngle();
	bool Shape = brsh->getShape();

	if ((Radius != preRadius) || (Aspect != preAspect) || (Angle != preAngle) || (Shape != preShape))
	{
		//
		// adjust the number of chords to make a smooth ellipse
		//
		// the fAngle is the cocking of the ellipse
		double SinAngle = sin(3.14159 * Angle / 180.);
		double CosAngle = cos(3.14159 * Angle / 180.);

		//
		double xb, yb, xr, yr;

		if (Shape)
		{ // rectangle

			nchords = 4;

			xb = -Radius * Aspect;
			yb = -Radius;
			brush[0] = xb * CosAngle + yb * SinAngle;
			brush[1] = -xb * SinAngle + yb * CosAngle;

			xb = Radius * Aspect;
			yb = -Radius;
			brush[2] = xb * CosAngle + yb * SinAngle;
			brush[3] = -xb * SinAngle + yb * CosAngle;

			xb = Radius * Aspect;
			yb = Radius;
			brush[4] = xb * CosAngle + yb * SinAngle;
			brush[5] = -xb * SinAngle + yb * CosAngle;

			xb = -Radius * Aspect;
			yb = Radius;
			brush[6] = xb * CosAngle + yb * SinAngle;
			brush[7] = -xb * SinAngle + yb * CosAngle;
		}
		else
		{
			// ellipse
			double r = Radius;
			if (r <= 0)
			{
				r = 1;
			}

			double chordAngle = 3.14159 / (4 * sqrt(r));
			nchords = 2 * 3.14159 / chordAngle;
			if (nchords > 256)
			{
				nchords = 256;
			}

			chordAngle = 2 * 3.14159 / (double)nchords;

			for (int i = 0; i < nchords; i++)
			{

				// generate the point
				xb = Radius * Aspect * cos(i * chordAngle);
				yb = Radius * sin(i * chordAngle);

				// the ellipse is rotated
				brush[2 * i] = xb * CosAngle + yb * SinAngle;
				brush[2 * i + 1] = -xb * SinAngle + yb * CosAngle;
			}
		}

		preRadius = Radius;
		preAspect = Aspect;
		preAngle = Angle;
		preShape = Shape;
	}
	//
	// set the brush color
	//
	setGraphicsColor(color);
	//
	// draw the brush using the frame entry points
	//
	moveToFrame(x + brush[0], y + brush[1]);
	for (int i = 1; i < nchords; i++)
	{
		lineToFrameNoFlush(x + brush[2*i], y + brush[2*i + 1]);
	}

	lineToFrameNoFlush(x + brush[0], y + brush[1]);
	flushGraphics();

	// revert to clipping to the CLIENT rectangle
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

void CDisplayer::drawAutoAlignBoxFrame(int color, double x, double y)
{
	// clip the brush to the IMAGE rectangle
	clipToImageRectangle();
	//
	// set the brush color
	//
	setGraphicsColor(color);
	//
	// draw the autoalign box using the frame entry points
	//
	moveToFrame(x - autoAlignBoxSize, y - autoAlignBoxSize);
	lineToFrameNoFlush(x + autoAlignBoxSize, y - autoAlignBoxSize);
	lineToFrameNoFlush(x + autoAlignBoxSize, y + autoAlignBoxSize);
	lineToFrameNoFlush(x - autoAlignBoxSize, y + autoAlignBoxSize);
	lineToFrameNoFlush(x - autoAlignBoxSize, y - autoAlignBoxSize);

	int hairleng = autoAlignBoxSize / 4;

	// draw some partial crosshairs
	moveToFrame(x - autoAlignBoxSize, y);
	lineToFrameNoFlush(x - hairleng, y);
	moveToFrame(x, y - autoAlignBoxSize);
	lineToFrameNoFlush(x, y - hairleng);
	moveToFrame(x + autoAlignBoxSize, y);
	lineToFrameNoFlush(x + hairleng, y);
	moveToFrame(x, y + autoAlignBoxSize);
	lineToFrameNoFlush(x, y + hairleng);

	flushGraphics();

	// revert to clipping to the CLIENT rectangle
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

// These methods give the Displayer clipped linedrawing capabilities.
// They assume that a call to "setScreenDeviceContext" has been made
// to initialize the screen device context ("screenDC"). That way we
// don't have to pass the device context as an argument. Nothing is
// done to determine line color, style, or raster op.
//
void CDisplayer::clipToVisibleRectangle()
{
	clpRect = visRect;
}
//------------------------------------------------------------------------------

void CDisplayer::clipToImageRectangle()
{
	clpRect = imgRect;
}
//------------------------------------------------------------------------------

RECT CDisplayer::getImageRectangle()
{
	return imgRect;
}
//------------------------------------------------------------------------------

// the standard Cohn-Sutherland tic-tac-toe codes
int CDisplayer::tCode(int x, int y)
{
	unsigned char tcode = 0;

	if (x < clpRect.left)
	{
		tcode |= TLFT;
	}

	if (y < clpRect.top)
	{
		tcode |= TTOP;
	}

	if (x > clpRect.right)
	{
		tcode |= TRGT;
	}

	if (y > clpRect.bottom)
	{
		tcode |= TBOT;
	}

	return (tcode);
}
//------------------------------------------------------------------------------

// our standard recursive sort is used to sort the
// transitions (crossings) around the edge of the
// image rectangle, prior to reconstructing the
// missing edges for exhibiting a Boolean AND
TRANSITION * CDisplayer::NodeSort(TRANSITION *loptr, TRANSITION *hiptr)
{
	TRANSITION *midptr, *chn1, *chn2, *temp, *head, *tail;

	if (loptr == hiptr)
	{
		loptr->nxt = nullptr;
		return loptr;
	}

	if ((hiptr - loptr) == 1)
	{
		if ((loptr->edge < hiptr->edge) || ((loptr->edge == hiptr->edge) && (loptr->coord < hiptr->coord)))
		{
			loptr->nxt = hiptr;
			hiptr->nxt = nullptr;
			return loptr;
		}
		else
		{
			hiptr->nxt = loptr;
			loptr->nxt = nullptr;
			return hiptr;
		}
	}

	/* split into two halves and sort separately */
	midptr = loptr + (hiptr - loptr) / 2;
	chn1 = CDisplayer::NodeSort(loptr, midptr);
	chn2 = CDisplayer::NodeSort(midptr + 1, hiptr);

	/* define the head of the merged chain */
	if ((chn2->edge < chn1->edge) || ((chn2->edge == chn1->edge) && (chn2->coord < chn1->coord)))
	{
		temp = chn1;
		chn1 = chn2;
		chn2 = temp;
	}

	head = tail = chn1;
	chn1 = chn1->nxt;

	/* as above, CHN1 is always the chain that advances */
	while (chn1 != nullptr)
	{
		if ((chn2->edge < chn1->edge) || ((chn2->edge == chn1->edge) && (chn2->coord < chn1->coord)))
		{
			temp = chn1;
			chn1 = chn2;
			chn2 = temp;
		}

		tail->nxt = chn1;
		tail = chn1;
		chn1 = chn1->nxt;
	}
	tail->nxt = chn2;

	return head;
}
//------------------------------------------------------------------------------

void CDisplayer::drawTransitionsClientNoFlush()
{
	// if not all transitions were recorded, we
	// don't want to go drawing the surround edges
	if (!trndon)
	{
		return;
	}

	// add nullptr transitions for the four corners
	trnptr->edge = 0;
	trnptr->coord = clpRect.right;
	trnptr->x = clpRect.right;
	trnptr->y = clpRect.top;
	trnptr->val = 0;
	trnptr++;

	trnptr->edge = 1;
	trnptr->coord = clpRect.bottom;
	trnptr->x = clpRect.right;
	trnptr->y = clpRect.bottom;
	trnptr->val = 0;
	trnptr++;

	trnptr->edge = 2;
	trnptr->coord = -clpRect.left;
	trnptr->x = clpRect.left;
	trnptr->y = clpRect.bottom;
	trnptr->val = 0;
	trnptr++;

	trnptr->edge = 3;
	trnptr->coord = -clpRect.top;
	trnptr->x = clpRect.left;
	trnptr->y = clpRect.top;
	trnptr->val = 0;
	trnptr++;

	// sort the crossings of the clip rectangle
	// beginning with the (left,top) and going in
	// order topedge->rgtedge->botedge->lftedge
	TRANSITION *tchn = NodeSort(trnsns, trnptr - 1);

	// now reconstruct the edges around the
	// periphery of the clipping rectangle
	VCurX = clpRect.left;
	VCurY = clpRect.top;
	while (tchn != nullptr)
	{
		VPreX = VCurX;
		VPreY = VCurY;

		VCurX = tchn->x;
		VCurY = tchn->y;

		if ((jcnt != 0) && // Jordan counter says "ON"
				((VCurX != VPreX) || (VCurY != VPreY)))
		{
			// non-trivial edge
			movTo(VPreX, VPreY);
			linToNoFlush(VCurX, VCurY);
		}

		// let the transition take effect
		// nothing happens at the corners
		jcnt ^= tchn->val;

		// on to next transition in chain
		tchn = tchn->nxt;
	}
}
//------------------------------------------------------------------------------

// These methods draw the vertex of a spline, given the client
// coordinates of the attach point and control points, and
// a mask which indicates which points are selected. The
// slots of the select mask are used by the BezierEditor.
//
void CDisplayer::drawVertexClientNoFlush(int xctrlpre, int yctrlpre, int x, int y, int xctrlnxt, int yctrlnxt,
		int selectmask)
{
	if (selectmask & SELECT_VTX)
	{

		// draw arms connecting attach point to control points
		moveToClient(xctrlpre, yctrlpre);
		lineToClientNoFlush(x, y);
		lineToClientNoFlush(xctrlnxt, yctrlnxt);

		// if vertex selected draw a filled box
		drawFilledBoxClientNoFlush(x, y);

		// if pre ctrl pt selected draw a filled box
		if (selectmask & SELECT_PRE)
		{
			drawFilledBoxClientNoFlush(xctrlpre, yctrlpre);
		}
		else
		{
			drawUnfilledBoxClientNoFlush(xctrlpre, yctrlpre);
		}

		// if nxt ctrl pt selected draw a filled box
		if (selectmask & SELECT_NXT)
		{
			drawFilledBoxClientNoFlush(xctrlnxt, yctrlnxt);
		}
		else
		{
			drawUnfilledBoxClientNoFlush(xctrlnxt, yctrlnxt);
		}
	}
	else
	{

		drawUnfilledBoxClientNoFlush(x, y);
	}
}
//------------------------------------------------------------------------------

// Return the active rectangle for the Bezier Editor
//
RECT CDisplayer::getActiveRectangle()
{
	return frmRect;
}
//------------------------------------------------------------------------------

// These methods draw onto the screen, given inputs
// in DOUBLE frame coordinates. This  gives  us  the
// accuracy we need to superimpose spline curves onto
// the image at all the zoom levels. The graphics are
// clipped to the clpRect.
//
void CDisplayer::moveToFrame(double x, double y)
{
	moveToClient(dscaleXFrameToClient(x), dscaleYFrameToClient(y));
}
//------------------------------------------------------------------------------

void CDisplayer::lineToFrameNoFlush(double x, double y)
{
	lineToClientNoFlush(dscaleXFrameToClient(x), dscaleYFrameToClient(y));
}
//------------------------------------------------------------------------------

void CDisplayer::splineToFrameNoFlush(double x1, double y1, double x2, double y2, double x3, double y3,
		unsigned int dashpat)
{
	splineToClientNoFlush(dscaleXFrameToClient(x1), dscaleYFrameToClient(y1), dscaleXFrameToClient(x2),
			dscaleYFrameToClient(y2), dscaleXFrameToClient(x3), dscaleYFrameToClient(y3), dashpat);
}
//------------------------------------------------------------------------------

// NOBODY CALLS THIS (it's in ToolSystemInterface, is all).
void CDisplayer::drawUnfilledBoxFrameNoFlush(double x, double y)
{
	drawUnfilledBoxClientNoFlush((int)(dscaleXFrameToClient(x) + 0.5), (int)(dscaleYFrameToClient(y) + 0.5));
}
//------------------------------------------------------------------------------

// NOBODY CALLS THIS (it's in ToolSystemInterface, is all).
void CDisplayer::drawFilledBoxFrameNoFlush(double x, double y)
{
	drawFilledBoxClientNoFlush((int)(dscaleXFrameToClient(x) + 0.5), (int)(dscaleYFrameToClient(y) + 0.5));
}
//------------------------------------------------------------------------------

// NOBODY CALLS THIS (it's in ToolSystemInterface, is all).
void CDisplayer::drawUnfilledBoxScaledNoFlush(double x, double y, double rad)
{
	// clip  to image area
	clipToImageRectangle();

	int xctr = (int)dscaleXFrameToClient(x);
	int yctr = (int)dscaleYFrameToClient(y);

	int irad = (int)(rad * XScaleFrameToClient);

	RECT obox;
	obox.left = xctr - irad;
	obox.top = yctr - irad;
	obox.right = xctr + irad;
	obox.bottom = yctr + irad;

	// draw box as a (clipped) polygon
	// note problem (!) at upper right
	moveToClient(obox.left, obox.top);
	lineToClientNoFlush(obox.right + 1, obox.top);
	moveToClient(obox.right, obox.top);
	lineToClientNoFlush(obox.right, obox.bottom);
	lineToClientNoFlush(obox.left, obox.bottom);
	lineToClientNoFlush(obox.left, obox.top);

	// clear the trnsn ptr
	moveToClient(obox.left, obox.top);

	// clip to client area
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

// NOBODY CALLS THIS (it's in ToolSystemInterface, is all).
void CDisplayer::drawFilledBoxScaledNoFlush(double x, double y, double rad)
{
	// clip  to image area
	clipToImageRectangle();

	int xctr = (int)dscaleXFrameToClient(x);
	int yctr = (int)dscaleYFrameToClient(y);

	int irad = (int)(rad * XScaleFrameToClient);

	RECT obox;
	obox.left = xctr - irad;
	obox.top = yctr - irad;
	obox.right = xctr + irad;
	obox.bottom = yctr + irad;

	irad >>= 1;

	RECT ibox;
	ibox.left = xctr - irad;
	ibox.top = yctr - irad;
	ibox.right = xctr + irad;
	ibox.bottom = yctr + irad;

	// draw outer box as a polygon
	// note problem (!) at upper right
	moveToClient(obox.left, obox.top);
	lineToClientNoFlush(obox.right + 1, obox.top);
	moveToClient(obox.right, obox.top);
	lineToClientNoFlush(obox.right, obox.bottom);
	lineToClientNoFlush(obox.left, obox.bottom);
	lineToClientNoFlush(obox.left, obox.top);

	// draw partial crosshairs
	moveToClient(xctr, obox.top);
	lineToClientNoFlush(xctr, ibox.top);
	moveToClient(xctr, obox.bottom);
	lineToClientNoFlush(xctr, ibox.bottom);
	moveToClient(obox.left, yctr);
	lineToClientNoFlush(ibox.left, yctr);
	moveToClient(obox.right, yctr);
	lineToClientNoFlush(ibox.right, yctr);

	// clear the trnsn ptr
	moveToClient(obox.left, obox.top);

	// clip to client area
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

void CDisplayer::drawVertexFrameNoFlush(double xctrlpre, double yctrlpre, double x, double y, double xctrlnxt,
		double yctrlnxt, int selectmask)
{
	drawVertexClientNoFlush((int)(dscaleXFrameToClient(xctrlpre) + 0.5), (int)(dscaleYFrameToClient(yctrlpre) + 0.5),
			(int)(dscaleXFrameToClient(x) + 0.5), (int)(dscaleYFrameToClient(y) + 0.5),
			(int)(dscaleXFrameToClient(xctrlnxt) + 0.5), (int)(dscaleYFrameToClient(yctrlnxt) + 0.5), selectmask);
}
//------------------------------------------------------------------------------

void CDisplayer::drawArrowFrameNoFlush(double frx, double fry, double tox, double toy, double wdt, double len)
{
	clipToImageRectangle();

	double delx = tox - frx;
	double dely = toy - fry;
	double leng = sqrt(delx * delx + dely * dely);

	if (len > 0.0 && leng > 2.0 * len)
	{
		int nseg = leng / len;

		double mlen = leng / (double)nseg;
		double mwdt = mlen * wdt / len;

		double udelx = delx / leng;
		double udely = dely / leng;

		for (int i = 1; i < nseg; i++)
		{
			double x, y;

			x = frx + (i - 0.25) * mlen * udelx + 0.25 * mwdt * udely;
			y = fry + (i - 0.25) * mlen * udely - 0.25 * mwdt * udelx;
			moveToFrame(x, y);

			x = frx + (i + 0.25) * mlen * udelx;
			y = fry + (i + 0.25) * mlen * udely;
			lineToFrameNoFlush(x, y);

			x = frx + (i - 0.25) * mlen * udelx - 0.25 * mwdt * udely;
			y = fry + (i - 0.25) * mlen * udely + 0.25 * mwdt * udelx;
			lineToFrameNoFlush(x, y);
		}
	}

	moveToFrame(frx, fry);
	lineToFrameNoFlush(tox, toy);

	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

// STRETCH RECTANGLE DISPLAY METHODS--------------------------------------------

void CDisplayer::setStretchRectangleColor(int color)
{
	stretchRectangleColor = color;
}

void CDisplayer::drawRectangleInProgress()
{
	setGraphicsColor(stretchRectangleColor);

	moveToFrame(strectf_lft, strectf_top);
	lineToFrameNoFlush(strectf_rgt, strectf_top);
	lineToFrameNoFlush(strectf_rgt, strectf_bot);
	lineToFrameNoFlush(strectf_lft, strectf_bot);
	lineToFrameNoFlush(strectf_lft, strectf_top);
	flushGraphics();
}
//------------------------------------------------------------------------------

// LASSO DISPLAY METHODS--------------------------------------------------------

void CDisplayer::drawLassoInProgress()
{
	setGraphicsColor(stretchRectangleColor);

	moveToClient(lasso[0].x, lasso[0].y);
	for (int i = 1; i < ilas; i++)
	{
		lineToClientNoFlush(lasso[i].x, lasso[i].y);
	}

	if (lassoSpark)
	{
		setGraphicsColor(graphicsColor + 1);
		lineToClientNoFlush(lasso[0].x, lasso[0].y);
		setGraphicsColor(graphicsColor - 1);
	}

	flushGraphics();
}
//------------------------------------------------------------------------------

// MOUSE INTERFACE METHODS -----------------------------------------------------

bool CDisplayer::getMousePositionClient(int *xp, int *yp)
{
	// get cursor pos in screen coords
	POINT curpos;
	if (!GetCursorPos(&curpos))
	{
		return (false);
	}

	ScreenToClient(windowHandle, (POINT*)&curpos);
	if ((curpos.x < clpRect.left) || (curpos.y < clpRect.top) || (curpos.x > clpRect.right) ||
			(curpos.y > clpRect.bottom))
	{
		return (false);
	}

	*xp = curpos.x;
	*yp = curpos.y;
	return (true);
}
//------------------------------------------------------------------------------

bool CDisplayer::getMousePositionFrame(int *xp, int *yp)
{
	int x, y;

	if (!getMousePositionClient(&x, &y))
	{
		return (false);
	}

	*xp = scaleXClientToFrame(x);
	*yp = scaleYClientToFrame(y);
	return (true);
}
//------------------------------------------------------------------------------

bool CDisplayer::getMousePositionFrame(double *xp, double *yp)
{
	int x, y;

	if (!getMousePositionClient(&x, &y))
	{
		return (false);
	}

	*xp = dscaleXClientToFrame(x);
	*yp = dscaleYClientToFrame(y);
	return (true);
}
//------------------------------------------------------------------------------

int CDisplayer::scaleMouseXClientToFrame(int x)
{
	return (scaleXClientToFrame(x));
}
//------------------------------------------------------------------------------

int CDisplayer::scaleMouseYClientToFrame(int y)
{
	return (scaleYClientToFrame(y));
}
//------------------------------------------------------------------------------

void CDisplayer::mouseDn()
{
	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off)
	{
		return;
	}

	if (currentTool == CURRENT_TOOL_PAINT)
	{
		if (displayProcessed && (lastFrame == targetFrame))
		{
			if (minorState == ACQUIRING_IMPORT_FRAME)
			{
				minorState = MOVING_IMPORT_FRAME;
			}
			else if (minorState == ACQUIRING_PIXELS)
			{
				minorState = PAINTING_PIXELS;
			}
		}

		if (lastFrame == targetFrame)
		{
			if (minorState == PICKING_COLOR)
			{
				pickOneColor(xcurfrm, ycurfrm);
			}
			else if (minorState == PICKING_SEED)
			{
				// save these only on a mouse-down event
				xseed = xcurfrm;
				yseed = ycurfrm;

				pickOneFillSeed(xcurfrm, ycurfrm);
			}
			else if (minorState == PICKING_AUTOALIGN)
			{
				// move the import frame to
				// coincide with the target
				autoAlign(xcurfrm, ycurfrm);
			}
		}
	}

	// if butn wasn't correct don't call this entry point - caller decides!
	// if (butn != 0) return;
	if (majorState == STRETCH_RECTANGLE_ACTIVE)
	{
		if (minorState == SEEKING_XY1)
		{
			x1 = xcur;
			y1 = ycur;

			x1f = dscaleXClientToFrame(x1);
			y1f = dscaleYClientToFrame(y1);

			// initial [x2,y2]
			x2 = x1;
			y2 = y1;

			x2f = x1f;
			y2f = y1f;

			// define strect, strectf
			strect.left = x1;
			strect.top = y1;
			strect.right = x2;
			strect.bottom = y2;

			strectf_lft = x1f;
			strectf_top = y1f;
			strectf_rgt = x2f;
			strectf_bot = y2f;

			minorState = SEEKING_DIR;
		}
	}
	else if (majorState == DRAW_LASSO_ACTIVE)
	{
		if (minorState == SEEKING_XY1)
		{
			// reset the fill ptr
			ilas = 0;

			// reset the compression ptr
			icmp = 0;

			// define the first lasso point
			lasso[ilas].x = xcur;
			lasso[ilas].y = ycur;
			ilas++;

			// advance the state machine
			minorState = SEEKING_XYN;
		}
	}
}
//------------------------------------------------------------------------------

RECT CDisplayer::getStretchRectangleClientCoords()
{
	return strect;
}
//------------------------------------------------------------------------------

void CDisplayer::mouseMv(int x, int y)
{
	// the previous & current client coordinates
	xpre = xcur;
	ypre = ycur;
	xcur = x;
	ycur = y;

	// the previous & current frame coordinates
	dxprefrm = dxcurfrm;
	dyprefrm = dycurfrm;
	dxcurfrm = (double)dspRect.left + (x - rgbRect.left) * XScaleClientToFrame;
	dycurfrm = (double)dspRect.top + (y - rgbRect.top) * YScaleClientToFrame;

	if (dxcurfrm == dxprefrm && dycurfrm == dyprefrm)
	{
		return;
	}

	// the previous & current integer frame coords
	xprefrm = xcurfrm;
	yprefrm = ycurfrm;
	xcurfrm = (int)dxcurfrm;
	ycurfrm = (int)dycurfrm;

	int xdelfrm = 0;
	int ydelfrm = 0;

	// Only PANNING is allowed in frame compare mode.
	bool inFrameCompareMode = mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off;

	if ((panFlag & PAN_XY) != 0)
	{
		if (panByGrabbing)
		{
			xdelfrm = (abs(panGrabbedFramePointX - dxcurfrm) > 1.0) ? (panGrabbedFramePointX - dxcurfrm) : 0;
			ydelfrm = (abs(panGrabbedFramePointY - dycurfrm) > 1.0) ? (panGrabbedFramePointY - dycurfrm) : 0;
		}
		else
		{
			// user holding U key down
			int xc, yc;
			if (getMousePositionClient(&xc, &yc))
			{
				int xdel = xcur - xpre;
				int ydel = ycur - ypre;

				double alfa = (double)xdel / (double)(imgRect.right - imgRect.left);
				double beta = (double)ydel / (double)(imgRect.bottom - imgRect.top);

				if ((panFlag & PAN_X) != 0)
				{
					xdelfrm = ((int)(1.25 * alfa * frmWdth)) & 0xfffffffe;
					if (xdelfrm == 0)
					{
						if (alfa > 0.0)
						{
							xdelfrm = 2;
						}
						else if (alfa < 0.0)
						{
							xdelfrm = -2;
						}
					}
				}

				if ((panFlag & PAN_Y) != 0)
				{
					ydelfrm = ((int)(1.25 * beta * frmHght)) & 0xfffffffe;
					if (ydelfrm == 0)
					{
						if (beta > 0.0)
						{
							ydelfrm = 2;
						}
						else if (beta < 0.0)
						{
							ydelfrm = -2;
						}
					}
				}
			}
		}

		if (xdelfrm != 0 || ydelfrm != 0)
		{
			// shift the display rectangle
			moveDisplayRectangle(xdelfrm, ydelfrm);

			// with the new display rectangle, recalculate cursor frame coords
			dxcurfrm = (double)dspRect.left + (x - rgbRect.left) * XScaleClientToFrame;
			dycurfrm = (double)dspRect.top + (y - rgbRect.top) * YScaleClientToFrame;
			xcurfrm = (int)dxcurfrm;
			ycurfrm = (int)dycurfrm;
		}

		zoomStack = stack<zoomStackEntry>(); // clear
		zoomStackDirection = noDirection;
	}
	else if (!inFrameCompareMode)
	{
		// no panning - RECTs and LASSOs
		if (majorState == STRETCH_RECTANGLE_ACTIVE)
		{
			if (minorState == SEEKING_DIR)
			{
				x1dir = 0;
				if ((xcur - x1) > 1)
				{
					x1dir = 1;
				}
				else if ((xcur - x1) < -1)
				{
					x1dir = -1;
				}
				y1dir = 0;
				if ((ycur - y1) > 1)
				{
					y1dir = 1;
				}
				else if ((ycur - y1) < -1)
				{
					y1dir = -1;
				}

				// this next ensures that the stretch
				// rectangle is defined even if there
				// is only ONE mouseMv event between
				// the mouseDn and mouseUp. Bugz #2172

				// new [x2,y2]
				x2 = xcur;
				y2 = ycur;

				x2f = dscaleXClientToFrame(x2);
				y2f = dscaleYClientToFrame(y2);

				// define strect, strectf
				strect.left = x1;
				strect.right = x2;
				if (x1 > x2)
				{
					strect.left = x2;
					strect.right = x1;
				}

				strect.top = y1;
				strect.bottom = y2;
				if (y1 > y2)
				{
					strect.top = y2;
					strect.bottom = y1;
				}

				strectf_lft = x1f;
				strectf_rgt = x2f;
				if (x1f > x2f)
				{
					strectf_lft = x2f;
					strectf_rgt = x1f;
				}

				strectf_top = y1f;
				strectf_bot = y2f;
				if (y1f > y2f)
				{
					strectf_top = y2f;
					strectf_bot = y1f;
				}

				if ((x1dir & y1dir) != 0)
				{
					minorState = SEEKING_XY2;
				}
			}
			else if (minorState == SEEKING_XY2)
			{
				// check to see if we have
				// the correct orientation
				x2dir = 0;
				if ((xcur - x1) > 0)
				{
					x2dir = 1;
				}
				else if ((xcur - x1) < 0)
				{
					x2dir = -1;
				}

				y2dir = 0;

				if ((ycur - y1) > 0)
				{
					y2dir = 1;
				}
				else if ((ycur - y1) < 0)
				{
					y2dir = -1;
				}

				if ((x2dir == x1dir) && (y2dir == y1dir))
				{
					// orientation good
					// continue stretching

					// new [x2,y2]
					x2 = xcur;
					y2 = ycur;

					x2f = dscaleXClientToFrame(xcur);
					y2f = dscaleYClientToFrame(ycur);

					// define strect
					strect.left = x1;
					strect.right = x2;
					if (x1 > x2)
					{
						strect.left = x2;
						strect.right = x1;
					}

					strect.top = y1;
					strect.bottom = y2;
					if (y1 > y2)
					{
						strect.top = y2;
						strect.bottom = y1;
					}

					strectf_lft = x1f;
					strectf_rgt = x2f;
					if (x1f > x2f)
					{
						strectf_lft = x2f;
						strectf_rgt = x1f;
					}

					strectf_top = y1f;
					strectf_bot = y2f;
					if (y1f > y2f)
					{
						strectf_top = y2f;
						strectf_bot = y1f;
					}
				}
				else
				{
					// the user has cancelled the box
					// he/she started by altering the
					// dir of (x2,y2) rel to (x1,y1)

					minorState = CANCELLED;
				}

				// redisplay the frame with or without stretch rectangle
				displayFrame();
			}
			else if (minorState == CANCELLED)
			{
				// check to see if we have
				// the correct orientation

				x2dir = 0;
				if ((xcur - x1) > 0)
				{
					x2dir = 1;
				}
				else if ((xcur - x1) < 0)
				{
					x2dir = -1;
				}

				y2dir = 0;

				if ((ycur - y1) > 0)
				{
					y2dir = 1;
				}
				else if ((ycur - y1) < 0)
				{
					y2dir = -1;
				}

				if ((x2dir == x1dir) && (y2dir == y1dir))
				{

					// orientation good again
					// resume normal stretching

					x2 = xcur;
					y2 = ycur;

					minorState = SEEKING_XY2;

					// redisplay frame and stretch rectangle
					displayFrame();
				}
			}
		}
		else if (majorState == DRAW_LASSO_ACTIVE)
		{
			if (minorState == SEEKING_XYN)
			{
				if (ilas >= MAX_LASSO_PTS - 2)
				{
					if (icmp < MAX_LASSO_PTS - 2)
					{
						// drop every other point
						// between icmp and ilas
						int idst = icmp;
						for (int isrc = icmp; isrc < ilas; isrc += 2)
						{
							lasso[idst++] = lasso[isrc];
						}

						// reset the pointers
						ilas = icmp = idst;

						// drop the next point in
						lasso[ilas].x = xcur;
						lasso[ilas].y = ycur;
						ilas++;
					}
					else
					{
						// can't compress any more
						// undraw and restart

						// reset the pointers
						ilas = icmp = 0;

						// first point
						lasso[ilas].x = xcur;
						lasso[ilas].y = ycur;
						ilas++;

						// redisplay frame and one-point LASSO
						displayFrame();
					}
				}
				else
				{
					// not too many lasso pts
					int delx = xcur - lasso[ilas - 1].x;
					if (delx < 0)
					{
						delx = -delx;
					}

					int dely = ycur - lasso[ilas - 1].y;
					if (dely < 0)
					{
						dely = -dely;
					}

					if ((delx + dely) > SHORTEST_LASSO_LINE)
					{
						// add another pt
						// add the new lasso pt and advance
						lasso[ilas].x = xcur;
						lasso[ilas].y = ycur;
						ilas++;

						lassoSpark = false;
						int delx = xcur - lasso[0].x;
						int dely = ycur - lasso[0].y;
						if ((delx * delx + dely * dely) < CLOSE_ENOUGH)
						{
							lassoSpark = true;
						}

						// redisplay frame and LASSO
						displayFrame();
					}
				}
			}
		}
	}

	if (currentTool == CURRENT_TOOL_PAINT && !inFrameCompareMode)
	{
		// you can grab the reference frame for POSITIONING while
		// you are PANning; also, you can move the brush and paint
		// pixels while you are PANning.
		updateCursor(false);

		if (displayProcessed && (autoAccept || (lastFrame == targetFrame)))
		{
			if ((xcurfrm != xprefrm) || (ycurfrm != yprefrm))
			{
				if (minorState == MOVING_IMPORT_FRAME)
				{
					impOff5X += (double)(xcurfrm - xprefrm);
					impOff5Y += (double)(ycurfrm - yprefrm);
					rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X,
							impOff5Y);
               CheckFencepostOverrun(importPreIntBuf);

					rotatedSinceLastTranslation = false;

					int xmitOffX = (int)impOff5X;
					int xmitOffY = (int)impOff5Y;
					transformEditor->transmitOnionSkinOffsets(xmitOffX, xmitOffY);

					displayFrame(_lastDisplayedFrameBuffer, targetFrame, true, nullptr, false);
				}
			}

			if (paintBrush != nullptr)
			{
				if (!mainWindow->GetPlayer()->isDisplaying())
				{
					displayFrame(_lastDisplayedFrameBuffer, lastFrame, true, nullptr, false);
				}
			}
		}

		if (displayProcessed && (lastFrame == importFrame))
		{
			if ((xcurfrm != xprefrm) || (ycurfrm != yprefrm))
			{
				if (minorState == TRACKING_CLONE_PT)
				{
					if (!mainWindow->GetPlayer()->isDisplaying())
					{
						displayFrame(_lastDisplayedFrameBuffer, lastFrame, true, nullptr, false);
					}
				}
			}
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::mouseUp(bool dspfrm)
{
	// if butn wasn't correct don't call this entry point - caller decides!
	// if (butn != 0) return;

	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off)
	{
		return;
	}

	if ((currentTool == CURRENT_TOOL_PAINT) && displayProcessed)
	{
		if (lastFrame == targetFrame)
		{
			if (minorState == MOVING_IMPORT_FRAME)
			{
				minorState = ACQUIRING_IMPORT_FRAME;
			}
			else if (minorState == PAINTING_PIXELS)
			{
				minorState = ACQUIRING_PIXELS;
			}
		}
	}

	if (majorState == STRETCH_RECTANGLE_ACTIVE)
	{
		if (minorState == CANCELLED)
		{
			// HACK - if in PAINT restore the minorState
			//
			if (currentTool == CURRENT_TOOL_PAINT)
			{
				minorState = savedMinorState;
			}
			else
			{
				minorState = SEEKING_XY1;
			}
		}
		else if (minorState == SEEKING_DIR)
		{
			// HACK - if in PAINT restore the minorState
			//
			if (currentTool == CURRENT_TOOL_PAINT)
			{
				minorState = savedMinorState;
			}
			else
			{
				minorState = SEEKING_XY1;
			}

			// redisplay frame but not RECT
			displayFrame();
		}
		else if (minorState == SEEKING_XY2)
		{
			minorState = RECT_COMPLETED;

			// conditional redisplay frame but not RECT
			if (dspfrm)
			{
				displayFrame();
			}

			// FORMERLY we clipped to imgRect, now we just test
			// to see if RECT does not touch imgRect
#ifdef OLD_CLIPPING
			if (!rectangleAND(&strect, &imgRect))
#else
			if ((strect.left > imgRect.right) || (strect.top > imgRect.bottom) || (strect.right < imgRect.left) ||
					(strect.bottom < imgRect.top))
#endif
			{
				minorState = SEEKING_XY1;
			}
			else
			{
				majorState = IDLE;

				if (currentTool == CURRENT_TOOL_PAINT)
				{
					minorState = savedMinorState;
				}

				if (coordMode == CLIENT_COORDS)
				{
					// note that no lasso is passed back in this mode
					(*stretchRectangleCallback)(stretchRectangleCallbackObj, &strect, nullptr);

				}
				else  // FRAME_COORDS
				{
					// map the stretched box to frame coords
					RECT strectf;
					strectf.left = strectf_lft;
					strectf.top = strectf_top;
					strectf.right = strectf_rgt;
					strectf.bottom = strectf_bot;

					// note that no lasso is passed back in this mode
					(*stretchRectangleCallback)(stretchRectangleCallbackObj, &strectf, nullptr);
				}
			}
		}
	}
	else if (majorState == DRAW_LASSO_ACTIVE)
	{
		minorState = LASSO_COMPLETED;

		int delx, dely;

		if (ilas > 1)
		{
			// redisplay frame
			displayFrame();
		}

		if ((ilas < 5) || (!lassoSpark))
		{
			minorState = SEEKING_XY1;
			return;
		}

		// now we find the kernel of the lasso - the portion
		// which is actually looped. We begin at opposite ends
		// of the poly array and work inward to find the first
		// and last line segments whose extents overlap. The
		// polygon will then be forced closed at the center of
		// intersection of those extents
		int beg = 0;
		int end = ilas - 1;

		int xcrs = lasso[0].x;
		int ycrs = lasso[0].y;
		RECT tailExt;
		RECT headExt;
		for (int i = (ilas - 2); i >= 2; i--)
		{
			lineSegmentExtent(&tailExt, &lasso[i], &lasso[i + 1]);

			for (int j = 0; j < (i - 2); j++)
			{
				lineSegmentExtent(&headExt, &lasso[j], &lasso[j + 1]);
				if (rectangleAND(&headExt, &tailExt))
				{ // intersect
					beg = j;
					end = i + 1;

					// crossing derived from extent intersect
					xcrs = (headExt.left + headExt.right) / 2;
					ycrs = (headExt.top + headExt.bottom) / 2;

					// close the polygon neatly
					lasso[beg].x = xcrs;
					lasso[beg].y = ycrs;
					lasso[end].x = xcrs;
					lasso[end].y = ycrs;
					goto f1;

				}
			}
		}

		if (lassoSpark)
		{
			lasso[++end] = lasso[beg];
		}

f1: ; // we have a closed lasso polygon in
		// client coordinates. Now that it's
		// been TRIMMED, we derive its bound-
		// ing rectangle
		strect.left = lasso[beg].x;
		strect.top = lasso[beg].y;
		strect.right = lasso[beg].x;
		strect.bottom = lasso[beg].y;
		for (int i = beg + 1; i < end; i++)
		{
			if (lasso[i].x < strect.left)
			{
				strect.left = lasso[i].x;
			}

			if (lasso[i].y < strect.top)
			{
				strect.top = lasso[i].y;
			}

			if (lasso[i].x > strect.right)
			{
				strect.right = lasso[i].x;
			}

			if (lasso[i].y > strect.bottom)
			{
				strect.bottom = lasso[i].y;
			}
		}

		// if the bounding rectangle of the loop area
		// of the lasso is OUTSIDE the image rectangle,
		// reset the minor state and start again

		// FORMERLY we clipped the strect, now we only
		// check it against imgRect
#ifdef OLD_CLIPPING
		if (!rectangleAND(&strect, &imgRect))
		{
#else
		if ((strect.left > imgRect.right) || (strect.top > imgRect.bottom) || (strect.right < imgRect.left) ||
				(strect.bottom < imgRect.top))
		{
#endif
			minorState = SEEKING_XY1;
			return;
		}

		// we have a valid lasso polygon in client coordinates
		// and a bounding rectangle in client coordinates. Time
		// to complete the operation

		majorState = IDLE;

		if (coordMode == CLIENT_COORDS)
		{
			// process poly from client coords to client coords
			// removing any redundant vertices
			int adv = 0;
#ifdef OLD_CLIPPING
			clipPointClient(&lasso[beg]);
#endif
			lasso[adv++] = lasso[beg++];
			while (beg <= end)
			{
#ifdef OLD_CLIPPING
				clipPointClient(&lasso[beg]);
#endif
				// if vertex is not redundant...
				if ((lasso[beg].x != lasso[adv - 1].x) || (lasso[beg].y != lasso[adv - 1].y))
				{
					// ...copy new vertex to output
					lasso[adv++] = lasso[beg++];
				}
				else
				{ // skip over it
					beg++;
				}
			}

			(*stretchRectangleCallback)(stretchRectangleCallbackObj, &strect, &lasso[0]);
		}
		else  // FRAME_COORDS
		{
			// process poly from client coords to frame coords
			// removing any redundant vertices
			int adv = 0;
#ifdef OLD_CLIPPING
			clipPointClient(&lasso[beg]);
#endif
			scalePointClientToFrame(&lasso[beg]);
			lasso[adv++] = lasso[beg++];
			while (beg <= end)
			{
#ifdef OLD_CLIPPING
				clipPointClient(&lasso[beg]);
#endif
				scalePointClientToFrame(&lasso[beg]);

				// if vertex is not redundant...
				if ((lasso[beg].x != lasso[adv - 1].x) || (lasso[beg].y != lasso[adv - 1].y))
				{
					// ...copy new vertex to output
					lasso[adv++] = lasso[beg++];
				}
				else
				{ // skip over it
					beg++;
				}
			}

			// make sure all lassos will terminate
			while (adv < 3)
			{
				lasso[adv++] = lasso[0];
			}

			// map the bounding rectangle to frame coords
			RECT strectf = strect;

			// scale the far edge (watch how we do this...)
			strectf.right++; // rgt edge of far pel
			strectf.bottom++; // bot edge of far pel
			scaleRectangleClientToFrame(&strectf);
			// strectf.right--;  // near edge of far pel
			// strectf.bottom--; // near edge of far pel

			// FORMERLY we clipped strectf to frmRect
			// Now that is no longer necessary
#ifdef OLD_CLIPPING
			rectangleAND(&strectf, &frmRect);
#endif
			(*stretchRectangleCallback)(stretchRectangleCallbackObj, &strectf, &lasso[0]);
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::setPreferredFrameCursor(TCursor frmcursor)
{
	preferredFrameCursor = frmcursor;
}
//------------------------------------------------------------------------------

void CDisplayer::showCursor(bool showFlag, bool forceArrowCursor)
{
	cursorOn = showFlag;
	updateCursor(forceArrowCursor);
}
//------------------------------------------------------------------------------

void CDisplayer::setViewOnlyMode(bool flag)
{
	_inViewOnlyMode = flag;

}
//------------------------------------------------------------------------------

void CDisplayer::updateCursor(bool forceArrowCursor)
{
	bool showArrowCursor = (forceArrowCursor || !isMouseInFrame()) && !mainWindow->isMouseCaptured();

	bool inFrameCompareMode = mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off;
	if (showArrowCursor || inFrameCompareMode)
	{
		if (Screen->Cursor != crArrow)
		{
			Screen->Cursor = crArrow;
		}
	}
	else if (!cursorOn)
	{
		Screen->Cursor = crNone;
	}
	else if (cursorOverrideIndex != 0)
	{
		if (Screen->Cursor != (TCursor) cursorOverrideIndex)
		{
			Screen->Cursor = (TCursor) cursorOverrideIndex;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) && ((minorState == EDIT_MASK) ||
			(minorState < ACQUIRING_IMPORT_FRAME)))
	{
		if (Screen->Cursor != crWhCross)
		{
			Screen->Cursor = crWhCross;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) && ((minorState == ACQUIRING_PIXELS) ||
			(minorState == PAINTING_PIXELS) || (minorState == PICKING_AUTOALIGN)) && displayProcessed)
	{
		if (Screen->Cursor != crNone)
		{
			Screen->Cursor = crNone;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) &&
			(onionSkinEnabled != ONIONSKIN_MODE_OFF))
	{
		if (Screen->Cursor != crHandPoint)
		{
			Screen->Cursor = crHandPoint;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == importFrame) && (minorState == TRACKING_CLONE_PT))
	{
		if (Screen->Cursor != preferredFrameCursor)
		{
			Screen->Cursor = preferredFrameCursor;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) && (minorState == PICKING_COLOR))
	{
		if (Screen->Cursor != crDropper)
		{
			Screen->Cursor = crDropper;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) && (minorState == PICKING_SEED))
	{
		if (Screen->Cursor != crBucket)
		{
			Screen->Cursor = crBucket;
		}
	}
	else if ((currentTool == CURRENT_TOOL_PAINT) && (lastFrame == targetFrame) && (minorState == HEALING_PIXELS))
	{
		if (Screen->Cursor != crHourGlass)
		{
			Screen->Cursor = crHourGlass;
		}
	}
	else
	{
		if (Screen->Cursor != preferredFrameCursor)
		{
			Screen->Cursor = preferredFrameCursor;
		}
	}
}
//------------------------------------------------------------------------------

// STRETCH RECTANGLE ACQUISITION METHODS

void CDisplayer::setDRSLassoMode(bool las)
{
	// FALSE for plain rectangle
	// TRUE  for lasso polygon
	lassoMode = las;
}
//------------------------------------------------------------------------------

bool CDisplayer::getDRSLassoMode() const
{
	// false for plain rectangle
	// true for lasso polygon
	return lassoMode;
}
//------------------------------------------------------------------------------

bool CDisplayer::isStretchingOrPainting()
{
	return (majorState > 0);
}
//------------------------------------------------------------------------------

void CDisplayer::stretchRectangleClient(void(*strcallback)(void *obj, void *rect, void *lasso), void *strobj)
{
	coordMode = CLIENT_COORDS;

	if (!lassoMode)
	{
		majorState |= STRETCH_RECTANGLE_ACTIVE;
	}
	else
	{
		majorState = DRAW_LASSO_ACTIVE;
	}

	minorState = SEEKING_XY1;

	stretchRectangleCallback = strcallback;

	stretchRectangleCallbackObj = strobj;
}
//------------------------------------------------------------------------------

void CDisplayer::stretchRectangleFrame(void(*strcallback)(void *obj, void *rect, void *lasso), void *strobj)
{
	cancelStretchRectangle();
	coordMode = FRAME_COORDS;

	if (!lassoMode)
	{
		majorState = STRETCH_RECTANGLE_ACTIVE;
	}
	else
	{
		majorState = DRAW_LASSO_ACTIVE;
	}

	savedMinorState = minorState;

	minorState = SEEKING_XY1;

	stretchRectangleCallback = strcallback;

	stretchRectangleCallbackObj = strobj;
}
//------------------------------------------------------------------------------

void CDisplayer::cancelStretchRectangle()
{
	if ((majorState == STRETCH_RECTANGLE_ACTIVE) || (majorState == DRAW_LASSO_ACTIVE))
	{
		// home the state machine
		majorState = IDLE;

		// HACK - if in PAINT restore the minorState
		if (currentTool == CURRENT_TOOL_PAINT)
		{
			minorState = savedMinorState;
		}
		else
		{
			minorState = CANCELLED;
		}

		// redisplay frame but no RECT
		displayFrame();
	}
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/////////////////////   RETICLE   //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void CDisplayer::setReticleMode(int newmode)
{
	if ((newmode >= 0) && (newmode <= 15))
	{
		reticleMode = newmode;
	}
}
//------------------------------------------------------------------------------

int CDisplayer::getReticleMode()
{
	return reticleMode;
}
//------------------------------------------------------------------------------

void CDisplayer::setReticle(CReticle& reticle)
{
	currentReticle = reticle;

	// set these so that explicit rects may be calculated
	currentReticle.setFrameRect(frmRect, pixelAspectRatio);
}
//------------------------------------------------------------------------------

CReticle CDisplayer::getReticle()
{
	return (currentReticle);
}
//------------------------------------------------------------------------------

void CDisplayer::setExitReticle(CReticle& reticle)
{
	exitReticle = reticle;
}
//------------------------------------------------------------------------------

CReticle CDisplayer::getExitReticle()
{
	return (exitReticle);
}
//------------------------------------------------------------------------------

void CDisplayer::drawReticle()
{
	RECT rct;

	if (reticleMode & RETICLE_MODE_MATTED)
	{
		setMatteColor();

		rct = currentReticle.getPrimaryRect();

		RECT frmtop;
		frmtop.left = frmRect.left;
		frmtop.top = frmRect.top;
		frmtop.right = frmRect.right + 1;
		frmtop.bottom = rct.top;
		clipAndFillRectangleFrame(&frmtop);

		RECT frmlft;
		frmlft.left = frmRect.left;
		frmlft.top = rct.top;
		frmlft.right = rct.left;
		frmlft.bottom = rct.bottom + 1;
		clipAndFillRectangleFrame(&frmlft);

		RECT frmrgt;
		frmrgt.left = rct.right + 1;
		frmrgt.top = rct.top;
		frmrgt.right = frmRect.right + 1;
		frmrgt.bottom = rct.bottom + 1;
		clipAndFillRectangleFrame(&frmrgt);

		RECT frmbot;
		frmbot.left = frmRect.left;
		frmbot.top = rct.bottom + 1;
		frmbot.right = frmRect.right + 1;
		frmbot.bottom = frmRect.bottom + 1;
		clipAndFillRectangleFrame(&frmbot);
	}

	setGraphicsColor(SOLID_YELLOW);

	if (reticleMode & RETICLE_MODE_SAFE_TITLE)
	{
		rct = currentReticle.getSafeTitleRect();
		rectangleAND(&rct, &frmRect);
		clipAndDrawRectangleFrame(&rct);
	}

	if (reticleMode & RETICLE_MODE_SAFE_ACTION)
	{
		rct = currentReticle.getSafeActionRect();
		rectangleAND(&rct, &frmRect);
		clipAndDrawRectangleFrame(&rct);
	}

	if (reticleMode & RETICLE_MODE_PRIMARY)
	{
		rct = currentReticle.getPrimaryRect();
		rectangleAND(&rct, &frmRect);
		clipAndDrawRectangleFrame(&rct);
	}
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/////////////////////   MISCELLANY  ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int CDisplayer::getSinglePixelValues(int frameX, int frameY, MTI_UINT16 *componentValues)
{
	int retVal;

	if (!_lastDisplayedFrameBuffer)
	{
		return -1;
	}

	MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
	retVal = extr->extractPoint(&(componentValues[0]), &(componentValues[1]), &(componentValues[2]), stupidBuf, imgFmt, frameX, frameY);
	return retVal;
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/////////////////////   TOOL-SPECIFIC STUFF  ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void CDisplayer::enterPaintMode()
{
	// it is an error to call this twice without intervening exitPaintMode()
	MTIassert(currentTool != CURRENT_TOOL_PAINT);
	if (currentTool != CURRENT_TOOL_PAINT)
	{
		currentTool = CURRENT_TOOL_PAINT;

		captureMode = CAPTURE_MODE_TARGET;

		// Either we have never entered Paint mode, or these should have been
		// cleared out when we last exited Paint mode
		MTIassert(targetOrgIntBuf == nullptr);
		MTIassert(targetIntBuf == nullptr);
		MTIassert(targetPreIntBuf == nullptr);
		MTIassert(importPreIntBuf == nullptr);
		MTIassert(importIntBuf == nullptr);
		MTIassert(paintMdfdBuf == nullptr);
		MTIassert(_currentFrameCompareBuf == nullptr);
		MTIassert(_importFrameCompareBuf == nullptr);
		MTIassert(_frameCompareOutputMask == nullptr);
		MTIassert(_frameCompareFrameIndex1 == -1);
		MTIassert(_frameCompareFrameIndex2 == -1);

		createStrokeStack();
	}
}
//------------------------------------------------------------------------------

void CDisplayer::enterRegistrationMode()
{
	// it's an error to call this twice without calling exitRegistrationMode()
	MTIassert(currentTool != CURRENT_TOOL_REGISTRATION);
	if (currentTool != CURRENT_TOOL_REGISTRATION)
	{
		currentTool = CURRENT_TOOL_REGISTRATION;

		clearLoadedRegistrationFrame();

		// Either we have never entered Registration mode, or these should
		// have been cleared out when we last exited Registration mode
		MTIassert(preRegistrationBuf == nullptr);
		MTIassert(postRegistrationBuf == nullptr);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::enterPreviewHackMode()
{
	inPreviewHackMode = true;
}
//------------------------------------------------------------------------------

void CDisplayer::exitPreviewHackMode()
{
	inPreviewHackMode = false;
// NO! Will be freed when we switch clips.
//   if (previewHackIntBufWasCudaMalloced)
//	{
//      MtiCudaFreeHost(previewHackIntBuf);
//   }
//   else
//   {
//      MTIfree(previewHackIntBuf);
//   }
//
//	previewHackIntBuf = nullptr;
//   previewHackIntBufWasCudaMalloced = false;
}
//------------------------------------------------------------------------------

void CDisplayer::clearLoadedRegistrationFrame()
{
	loadedRegistrationFrame = -1;
	registeredRegistrationFrame = -1;
}
//------------------------------------------------------------------------------

void CDisplayer::clearRegisteredRegistrationFrame()
{
	registeredRegistrationFrame = -1;
}
//------------------------------------------------------------------------------

void CDisplayer::enableAutoAccept(bool onoff)
{
	autoAccept = onoff;
}
//------------------------------------------------------------------------------

void CDisplayer::exitPaintMode()
{
	// it is an error to call this twice without calling enterPaintMode()
	MTIassert(currentTool == CURRENT_TOOL_PAINT);
	if (currentTool == CURRENT_TOOL_PAINT)
	{
		destroyStrokeStack();

		// We are going to give the preallocation pool back to the
		// Tool Infrastructure, so nuke these guys
		unloadTargetAndImportFrames();

		currentTool = CURRENT_TOOL_NAVIGATOR;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::exitRegistrationMode()
{
	// it's an error to call this twice without calling enterRegistrationMode()
	MTIassert(currentTool == CURRENT_TOOL_REGISTRATION);
	if (currentTool == CURRENT_TOOL_REGISTRATION)
	{
		// Release buffers
		unloadRegistrationFrame();

		currentTool = CURRENT_TOOL_NAVIGATOR;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::initImportProxy(int wdth, int hght)
{
	if (wdth != importProxyWdth || hght != importProxyHght)
	{
		delete[]importProxy;

		importProxyWdth = wdth;
		importProxyHght = hght;
		importProxy = new unsigned int[importProxyWdth * importProxyHght];
	}
}
//------------------------------------------------------------------------------

void CDisplayer::clearTransform()
{
	impRot = 0.0;
	impStrX = 1.0;
	impStrY = 1.0;
	impSkew = 0.0;
	impOff5X = 0.0;
	impOff5Y = 0.0;

	dspXctrWhenLastRotated = 0x7fffffff;
	dspYctrWhenLastRotated = 0x7fffffff;
	rotatedSinceLastTranslation = false;
}
//------------------------------------------------------------------------------

void CDisplayer::setTransform(double rot, double strx, double stry, double skew, double offx, double offy)
{
	MTIassert(strx != 0.0);
	MTIassert(stry != 0.0);

	impRot = rot;
	impStrX = strx;
	impStrY = stry;
	impSkew = skew;

	impOff5X = offx;
	impOff5Y = offy;

	// SIDE EFFECT ALERT!!!
	if ((importIntBuf != nullptr) && (importPreIntBuf != nullptr) && (imgFmt != nullptr))
	{
		rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
      CheckFencepostOverrun(importPreIntBuf);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::getTransform(double *rot, double *strx, double *stry, double *skew, double *offx, double *offy)
{
	// awful hack - if we're in Tracking Alignment mode, completely ignore
	// the current transform and substitute one that does only the tracked
	// displacement for the current target/import frame pair -- no rotation,
	// scaling or skewing

	if (useTrackingAlignOffsets)
	{
		getTrackingAlignOffsets(targetFrame, *offx, *offy);

		// trash the rest of the parameters
		*rot = 0.0;
		*strx = *stry = 1.0;
		*skew = 0.0;
	}
	else
	{
		// no shenanigans necessary
		*rot = impRot;
		*strx = impStrX;
		*stry = impStrY;
		*skew = impSkew;
		*offx = impOff5X;
		*offy = impOff5Y;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::getTrackingAlignOffsets(int frame, double &xoff, double &yoff)
{
	map<int, std::pair<float, float> >::iterator iter;

	// Use the tracking data to come up with the relative offsets
	// between the current target and import frames - the map is keyed
	// by the TARGET frame index and the values are the offset to be
	// applied to the corresponding IMPORT frame to get it to line up
	// with the target frame

	iter = trackingAlignOffsetsMap.find(frame);

	if (iter != trackingAlignOffsetsMap.end())
	{
		// should define FPOINT type instead of all this first/second crap !
		xoff = (*iter).second.first;
		yoff = (*iter).second.second;
	}
	else
	{
		// Hmm is this good?
		xoff = 0.0;
		yoff = 0.0;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::setMatBox(RECT *boxp)
{
	matBox = *boxp;
}
//------------------------------------------------------------------------------

void CDisplayer::getMatBox(RECT *boxp)
{
	*boxp = matBox;
}
//------------------------------------------------------------------------------

void CDisplayer::setRegistrationTransform(AffineCoeff *xfrm)
{
	curRegXform = *xfrm;

	registeredRegistrationFrame = -1;
}
//------------------------------------------------------------------------------

void CDisplayer::getRegistrationTransform(AffineCoeff *xfrm)
{
	*xfrm = curRegXform;
}
//------------------------------------------------------------------------------

void CDisplayer::setCaptureMode(int newMode)
{
	int oldMode = captureMode;
	captureMode = newMode;

	if (oldMode == CAPTURE_MODE_ALTCLIP_IMPORT && mainWindow->GetPlayer()->getLastFrameIndex() != targetFrame)
	{
		mainWindow->GetPlayer()->goToFrameSynchronous(targetFrame);
	}
}
//------------------------------------------------------------------------------

int CDisplayer::getCaptureMode()
{
	return captureMode;
}
//------------------------------------------------------------------------------

// WAS method "setPaintFrames(int trg, int imp);
//
void CDisplayer::clearPaintFrames()
{
	// always clear these to force a subsequent call to
	// 'fetchTargetAndImportFrames' to  fetch the frames
	//
	targetFrame = -1;
	importFrame = -1;
}
//------------------------------------------------------------------------------

bool CDisplayer::getPaintFrames(int *trg, int *imp)
{
	*trg = desiredTargetFrame;
	*imp = desiredImportFrame;

	// *trg    = (strokedTargetFrame >= 0) ? strokedTargetFrame : desiredTargetFrame;
	// *imp    = (strokedImportFrame >= 0) ? strokedImportFrame : desiredImportFrame;

	return targetFrame == *trg;
}
//------------------------------------------------------------------------------

void CDisplayer::unloadTargetAndImportFrames()
{
	////////////////////
	// Target buffers //
	////////////////////

	MTIfree(targetOrgIntBuf);
	targetOrgIntBuf = nullptr;

	targetIntBuf = nullptr;

	MTIfree(targetIntBuf);
	targetIntBuf = nullptr;

	MTIfree(targetPreIntBuf);
	targetPreIntBuf = nullptr;

	MTIfree(paintMdfdBuf);
	paintMdfdBuf = nullptr;

	// "frame diff" hack
	MTIfree(_currentFrameCompareBuf);
	MTIfree(_importFrameCompareBuf);
	delete[] _frameCompareOutputMask;
	_currentFrameCompareBuf = nullptr;
	_importFrameCompareBuf = nullptr;
	_frameCompareOutputMask = nullptr;
	_frameCompareFrameIndex1 = -1;
	_frameCompareFrameIndex2 = -1;

	// no target frame
	targetFrame = -1;

	////////////////////
	// Import buffers //
	////////////////////

	MTIfree(importPreIntBuf);
	importPreIntBuf = nullptr;

	MTIfree(importIntBuf);
	importIntBuf = nullptr;

	// no import frame
	importFrame = -1;

	/////////////////////////
	// Preview hack buffer //
	/////////////////////////

   if (previewHackIntBufWasCudaMalloced)
	{
      MtiCudaFreeHost(previewHackIntBuf);
   }
   else
   {
      MTIfree(previewHackIntBuf);
   }

	previewHackIntBuf = nullptr;
   previewHackIntBufWasCudaMalloced = false;
}
//------------------------------------------------------------------------------

void CDisplayer::unloadRegistrationFrame()
{
	MTIfree(preRegistrationBuf);
	preRegistrationBuf = nullptr;

	MTIfree(postRegistrationBuf);
	postRegistrationBuf = nullptr;

	// no register frame
	loadedRegistrationFrame = -1;
	registeredRegistrationFrame = -1;
}
//------------------------------------------------------------------------------

bool CDisplayer::targetFrameIsModified()
{
	// Only makes sense if Paint (QQQ and maybe 3Layer??) is active.
	if (currentTool == CURRENT_TOOL_NAVIGATOR)
	{
		return false;
	}

	// do an actual compare of the current target with the original;
	// this is THE correct way to determine whether target has changed
	if ((targetOrgIntBuf != nullptr) && (targetIntBuf != nullptr))
	{
		if (frameComponentCount == 1)
		{
			for (int i = 0; i < frameWidth * frameHeight; i++)
			{
				if (targetIntBuf[i] != targetOrgIntBuf[i])
				{
					return true;
				}
			}
		}
		else if (frameComponentCount == 3)
		{
			for (int i = 0; i < 3 * frameWidth * frameHeight; i += 3)
			{
				if (targetIntBuf[i] != targetOrgIntBuf[i])
				{
					return true;
				}

				if (targetIntBuf[i + 1] != targetOrgIntBuf[i + 1])
				{
					return true;
				}

				if (targetIntBuf[i + 2] != targetOrgIntBuf[i + 2])
				{
					return true;
				}
			}
		}
	}

	return false;
}
//------------------------------------------------------------------------------

int CDisplayer::allocateTargetBuffers()
{
	if (targetOrgIntBuf == nullptr)
	{
		targetOrgIntBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	if (targetIntBuf == nullptr)
	{
		targetIntBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	if (targetPreIntBuf == nullptr)
	{
		targetPreIntBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	const size_t mdfdSize = frameWidth * frameHeight * 2;

	if (paintMdfdBuf == nullptr)
	{
		paintMdfdBuf = (MTI_UINT16*) MTImalloc(mdfdSize);
	}

	if (_currentFrameCompareBuf == nullptr)
	{
		_currentFrameCompareBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	if (_importFrameCompareBuf == nullptr)
	{
		_importFrameCompareBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	// Note that _frameCompareOutputMask is allocated in allocateImportBuffers()
	return 0;
}
//------------------------------------------------------------------------------

int CDisplayer::allocateRegistrationBuffers()
{
	if (preRegistrationBuf == nullptr)
	{
		preRegistrationBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	if (postRegistrationBuf == nullptr)
	{
		postRegistrationBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	return 0;
}
//------------------------------------------------------------------------------

// Will be set to TRUE while we are xecuting a range macro.
void CDisplayer::enableForceTargetLoad(bool enbl)
{
	forceTargetLoad = enbl;
}
//------------------------------------------------------------------------------

// Will be set to TRUE while we are xecuting a range macro.
void CDisplayer::enableForceImportLoad(bool enbl)
{
	forceImportLoad = enbl;
}
//------------------------------------------------------------------------------

void CDisplayer::makeAndTransmitProxy()
{
	if ((importPreIntBuf != nullptr) && (imgFmt != nullptr))
	{
		_mainFrameConverter->displayConvertIntermediate(importPreIntBuf, imgFmt, &frmRect, importProxy, importProxyWdth, importProxyHght,
				importProxyWdth*4, currentLUTPtr);
      CheckFencepostOverrun(importPreIntBuf);

		transformEditor->transmitProxy(importProxy);
	}
}
//------------------------------------------------------------------------------

int CDisplayer::getTargetFrameIndex()
{
   return targetFrame;
}
//------------------------------------------------------------------------------

void CDisplayer::loadTargetFrame(int target, bool fast)
{
	// Allocate the target buffers if not done yet
	allocateTargetBuffers();

	if ((mainWindow->GetPlayer()->getPlaybackFilter() & PLAYBACK_FILTER_BOTH)
			== PLAYBACK_FILTER_NONE && lastFrame == target && _lastDisplayedFrameBuffer)
	{
		// the last display fields contain neither
		// original values nor highlighted fixes
		MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
      CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
		extr->extractActivePicture(targetIntBuf, stupidBuf, imgFmt, &frameImageFormat);
	}
	else
	{ // need a fresh copy right from the media,
		// sans original values & highlighted fixes

		mainWindow->GetPlayer()->getToolFrame(targetIntBuf, target);
	}

	// THIS WAS A VERY BAD IDEA! This stuff is needed for range macros while playing!
	// if (fast)
	// {
	// return;
	// }

	// copy for deriving pxl region lists for completion
	MTImemcpy(targetOrgIntBuf, targetIntBuf, frameInternalSize);

	// copy for deriving pxl region lists for strokes
	MTImemcpy(targetPreIntBuf, targetIntBuf, frameInternalSize);

	// clear the mask buffer
	MTImemset(paintMdfdBuf, 0, 2*(frameWidth*frameHeight));

	// interpolates regions and renders the mask if not already available
	(CNavigatorTool::GetMaskTool())->CalculateRegionOfInterest(target);
}
//------------------------------------------------------------------------------

void CDisplayer::loadRegistrationFrame(int registr)
{
	// contains guard
	allocateRegistrationBuffers();

	if (mainWindow->GetPlayer()->getPlaybackFilter() == PLAYBACK_FILTER_NONE && _lastDisplayedFrameBuffer)
	{
		// the last display fields contain neither
		// original values nor highlighted fixes
		MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
      CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
		extr->extractActivePicture(preRegistrationBuf, stupidBuf, imgFmt, &frameImageFormat);
	}
	else
	{
		// need a fresh copy right from the media,
		// sans original values & highlighted fixes
		mainWindow->GetPlayer()->getToolFrame(preRegistrationBuf, registr);
	}

	return;
}
//------------------------------------------------------------------------------

void CDisplayer::registerLoadedRegistrationFrame()
{
	registrar->registerChannels(postRegistrationBuf, preRegistrationBuf, imgFmt, &matBox, &curRegXform);
}

// Called after the modified target frame is
// written back to the media with new pixels.
// Mimics the effect of reloading the target
// frame and resetting the paint mask buffer
void CDisplayer::reloadModifiedTargetFrame()
{
	// buffer for deriving pxl region lists for strokes
	MTImemcpy((void *)targetPreIntBuf, (void *)targetIntBuf, frameInternalSize);

	// buffer for deriving pxl region lists for completion
	MTImemcpy((void *)targetOrgIntBuf, (void *)targetIntBuf, frameInternalSize);

	// clear the mask buffer
	MTImemset(paintMdfdBuf, 0, 2*(frameWidth*frameHeight));

	// reset the minorState - TTT
	// minorState = CANCELLED;
}
//------------------------------------------------------------------------------

// called after a stroke in CLONE MODE, when
// the IMPORT FRAME is the same as the TARGET,
// to put changes to TARGET FRAME into IMPORT
void CDisplayer::reloadModifiedImportFrame()
{
	// We only need to reload it if the import frame is the target frame from
	// the current clip!
	if (importFrame != targetFrame || _importFrameClip != nullptr)
	{
		return;
	}

	// Allocate the import buffers if not done yet
	allocateImportBuffers();

	MTImemcpy((void *)importPreIntBuf, (void *)targetIntBuf, frameInternalSize);
   CheckFencepostOverrun(importPreIntBuf);

	// initialize importIntBuf using
	// the current transform settings
	rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
   CheckFencepostOverrun(importPreIntBuf);

	// make a proxy and transmit it
	makeAndTransmitProxy();
}
//------------------------------------------------------------------------------

// Called after the modified target frame is
// discarded without writeback to the media.
// Mimics the effect of reloading the target
// frame and resetting the paint mask buffer
void CDisplayer::reloadUnmodifiedTargetFrame()
{
	// copy the unmodified target frame to the "working"
	MTImemcpy((void *)targetIntBuf, (void *)targetOrgIntBuf, frameInternalSize);

	// buffer for deriving pxl region lists for strokes
	MTImemcpy((void *)targetPreIntBuf, (void *)targetOrgIntBuf, frameInternalSize);

	// clear the mask buffer
	MTImemset(paintMdfdBuf, 0, 2*(frameWidth*frameHeight));

	// reset the minorState - TTT
	// minorState = CANCELLED;
}
//------------------------------------------------------------------------------

MTI_UINT16 * CDisplayer::getTargetIntermediate()
{
	return targetIntBuf;
}
//------------------------------------------------------------------------------

MTI_UINT16 *CDisplayer::getTargetOriginalIntermediate()
{
	return targetOrgIntBuf;
}
//------------------------------------------------------------------------------

MTI_UINT16 *CDisplayer::getTargetPreStrokeIntermediate()
{
	return targetPreIntBuf;
}
//------------------------------------------------------------------------------

MTI_UINT16 *CDisplayer::getRegisteredIntermediate()
{
	// contains guard
	allocateRegistrationBuffers();

	return postRegistrationBuf;
}
//------------------------------------------------------------------------------

MTI_UINT16 *CDisplayer::getPreRegisteredIntermediate()
{
	// contains guard
	allocateRegistrationBuffers();

	return preRegistrationBuf;
}
//------------------------------------------------------------------------------

CPixelRegionList& CDisplayer::getOriginalPixels()
{
	// here we use a new constructor which does all the heavy lifting
	originalPels->Clear(); // moved up from down below - I assume this isn't

	try
	{
		// THIS TRIGGERS A HUGE MALLOC SOMEWHERE WHEN FRAME IS 4K
		CMaskPixelRegion *diffPixels = new CMaskPixelRegion(targetIntBuf, targetOrgIntBuf, frameWidth, frameHeight,
				frameComponentCount);
		originalPels->Clear();

		// notice that this flavor of ADD takes the ptr as arg (!);
		// the pixelregion is not copied, as it was before, but
		// instead becomes the property of the pixelregionlist
		originalPels->Add(diffPixels);
	}

	catch (std::bad_alloc)
	{
		// DAMN no frickin' way to return any kind of error!!!!
		TRACE_0(errout << "ERROR: OUT OF MEMORY in Displayer::getOriginalPixels");
	}

	return (*originalPels);
}
//------------------------------------------------------------------------------

struct CompareParams
{
	Ipp16u *src1; // Pointer to first source element of interest
	int src1Step; // Source row pitch in bytes
	Ipp16u *src2; // Pointer to first source element of interest
	int src2Step; // Source row pitch in bytes
	Ipp8u *dst; // Pointer to first destination element
	int dstStep; // Destination row pitch in bytes
	IppiSize size; // Witdh and height of pixels to be compared
	Ipp16u threshold; // Difference threshold - 0 means straight up comparison
	int numberOfJobs; // The total number of jobs for segmentation calculation
};
//------------------------------------------------------------------------------

void RunThresholdedComparisonJob(void *vp, int jobNumber)
{
	if (vp == nullptr)
	{
		return;
	}

	// Image segmented by blocks of rows.
	auto params = static_cast<CompareParams*>(vp);
	IppiSize jobSize;
	jobSize.width = params->size.width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = params->size.height / params->numberOfJobs;
	auto extraRows = params->size.height % params->numberOfJobs;
	auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	auto jobSrc1 = params->src1 + (jobY * (params->src1Step / sizeof(Ipp16u)));
	auto jobSrc2 = params->src2 + (jobY * (params->src2Step / sizeof(Ipp16u)));
	auto jobDst = params->dst + (jobY * (params->dstStep / sizeof(Ipp8u)));
	jobSize.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

	// Allocate the temp buffers. Assume the pitch is the same for both!
	int tempBuf1RowPitch;
	int tempBuf2RowPitch;
	Ipp16u* tempBuf1 = ippiMalloc_16u_C3(jobSize.width, jobSize.height, &tempBuf1RowPitch);
	Ipp16u* tempBuf2 = ippiMalloc_16u_C3(jobSize.width, jobSize.height, &tempBuf2RowPitch);

	// Calculate distances. There is no 16u_C3R, so we triple the width and use 16u_C1R.
	IppiSize comboSize = jobSize;
	comboSize.width *= 3;
	IppStatus status = ippiAbsDiff_16u_C1R(jobSrc1, params->src1Step, jobSrc2, params->src2Step, tempBuf1,
			tempBuf1RowPitch, comboSize);

	// Blur the result in place with a box filter whose radius is proportional to the image width.
	// We're hoping this will eliminate holes.
	int radius = int((jobSize.width / 1000.0) + 0.5);
	int maskEdge = 1 + (2 * radius);
	IppiSize maskSize =
	{maskEdge, maskEdge};
	int numChannels = 3;
	IppiBorderType borderType = ippBorderRepl;
	int scratchBufSize;
	status = ippiFilterBoxBorderGetBufferSize(jobSize, maskSize, ipp16u, numChannels, &scratchBufSize);
	Ipp8u* scratchBuf = (Ipp8u*) ippMalloc(scratchBufSize);
	Ipp16u thresholds[3] =
	{params->threshold, params->threshold, params->threshold};

	// QQQ does this work with maskSize == {1, 1} ?
	status = ippiFilterBoxBorder_16u_C3R(tempBuf1, tempBuf1RowPitch, tempBuf2, tempBuf2RowPitch, jobSize, maskSize,
			borderType, nullptr, scratchBuf);

	// Test against threshold.
	status = ippiCompareC_16u_C3R(tempBuf2, tempBuf2RowPitch, thresholds, jobDst, params->dstStep, jobSize,
			ippCmpLessEq);

	ippiFree(tempBuf1);
	ippiFree(tempBuf2);
	ippFree(scratchBuf);
}
//------------------------------------------------------------------------------

void RunComparisonJob(void *vp, int jobNumber)
{
	if (vp == nullptr)
	{
		return;
	}

	auto params = static_cast<CompareParams*>(vp);
	if (params->threshold > 0)
	{
		RunThresholdedComparisonJob(vp, jobNumber);
		return;
	}

	// Image segmented by blocks of rows.
	IppiSize jobSize;
	jobSize.width = params->size.width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = params->size.height / params->numberOfJobs;
	auto extraRows = params->size.height % params->numberOfJobs;
	auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	auto jobSrc1 = params->src1 + (jobY * (params->src1Step / sizeof(Ipp16u)));
	auto jobSrc2 = params->src2 + (jobY * (params->src2Step / sizeof(Ipp16u)));
	auto jobDst = params->dst + (jobY * (params->dstStep / sizeof(Ipp8u)));
	jobSize.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

	// Do this job!
	IppStatus status = ippiCompare_16u_C3R(jobSrc1, params->src1Step, jobSrc2, params->src2Step, jobDst, params->dstStep,
			jobSize, ippCmpEq);
}
//------------------------------------------------------------------------------

void CDisplayer::setAltClipDiffThreshold(float newThreshold)
{
	MTIassert(newThreshold >= 0.f && newThreshold < 1.0f);
	_altClipDiffThreshold = newThreshold;
}
//------------------------------------------------------------------------------

void CDisplayer::setOrigValDiffThreshold(float newThreshold)
{
	MTIassert(newThreshold >= 0.f && newThreshold < 1.0f);
	_origValDiffThreshold = newThreshold;
}
//------------------------------------------------------------------------------

void CDisplayer::setPaintToolChangesPending(bool flag)
{
	_paintToolChangesArePending = flag;
}
//------------------------------------------------------------------------------

bool CDisplayer::arePaintToolChangesPending()
{
	return _paintToolChangesArePending;
}
//------------------------------------------------------------------------------

bool *CDisplayer::CompareImportAndTargetFrames(MTI_UINT16 diffthreshold, bool targetMayHaveChanged)
{
	MTIassert(_frameCompareOutputMask != nullptr);

	int nPixels = frameWidth * frameHeight;
	if (importIntBuf == nullptr || targetIntBuf == nullptr)
	{
		MTImemset(_frameCompareOutputMask, 0, nPixels);
		return _frameCompareOutputMask;
	}

	if (_frameCompareOutputMask != nullptr
	&& !targetMayHaveChanged
	&& ((importFrame == _frameCompareFrameIndex1 && targetFrame == _frameCompareFrameIndex2)
		|| (importFrame == _frameCompareFrameIndex2 && targetFrame == _frameCompareFrameIndex1)))
	{
		return _frameCompareOutputMask;
	}

	compareTwoFrames(importIntBuf, targetIntBuf, diffthreshold, _frameCompareOutputMask);
	_frameCompareFrameIndex1 = importFrame;
	_frameCompareFrameIndex2 = targetFrame;

	return _frameCompareOutputMask;
}
//------------------------------------------------------------------------------

void CDisplayer::compareTwoFrames(MTI_UINT16 *frameBuffer1, MTI_UINT16 *frameBuffer2, MTI_UINT16 threshold,
		bool *outputMask)
{
	MTIassert(outputMask != nullptr);

	if (frameBuffer1 == nullptr || frameBuffer2 == nullptr)
	{
		MTImemset(outputMask, 0, frameWidth * frameHeight);
		return;
	}

	CompareParams params;
	params.size.width = frameWidth;
	params.size.height = frameHeight;

	params.src1 = frameBuffer1;
	const int NUM_COMPONENTS = 3;
	params.src1Step = frameWidth * NUM_COMPONENTS*sizeof(Ipp16u);
	params.src2 = frameBuffer2;
	params.src2Step = frameWidth * NUM_COMPONENTS*sizeof(Ipp16u);

	params.dst = (Ipp8u*)outputMask;
	params.dstStep = frameWidth*sizeof(Ipp8u);

	params.threshold = threshold;

#define COMPARE_WITH_MTHREADS 1
#if COMPARE_WITH_MTHREADS
	SynchronousMthreadRunner mthreads(&params, RunComparisonJob);
	params.numberOfJobs = mthreads.GetNumberOfThreads();
#else
	params.numberOfJobs = 1;
#endif

	CHRTimer timer;
#if COMPARE_WITH_MTHREADS
	mthreads.Run();
#else
	RunComparisonJob(&params, 0);
#endif
	TRACE_3(errout << "Buffer compare took " << timer.Read() << " msecs.");
}
//------------------------------------------------------------------------------

void CDisplayer::createStrokeStack()
{
	// open pixel channel for the Stroke Stack
	string clppath = GetFilePath(mainWindow->GetCurrentClipHandler()->getCurrentClipFileName());
	pelsName = clppath + "paint.pxl";
	paintPelsChannel = new CStrokeChannel();
	paintPelsChannel->openStrokeChannel(frameComponentCount, pelsName);
	paintPelsChannel->closeStrokeChannel();
	strokeStack.clear();
}
//------------------------------------------------------------------------------

void CDisplayer::destroyStrokeStack()
{
	strokeStack.clear();

	if (paintPelsChannel != nullptr)
	{
		DeleteFile(pelsName.c_str());
		delete paintPelsChannel;
		paintPelsChannel = nullptr;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::makeStrokeStack()
{
	destroyStrokeStack();
	createStrokeStack();
}
//------------------------------------------------------------------------------

void CDisplayer::clearStrokeStack()
{
	if (paintPelsChannel == nullptr)
	{
		return;
	}

	// the actual stack
	strokeStack.clear();

	// rewind the stroke disk backups to 0
	paintPelsChannel->setStrokeChannelFilePtr((MTI_INT64)0);

	// empty the GUI listbox
	transformEditor->clearStrokeStack();
}
//------------------------------------------------------------------------------

void CDisplayer::transmitStrokeStack()
{
	// empty the GUI listbox
	transformEditor->clearStrokeStack();

	for (int i = strokeStack.size() - 1; i >= 0; i--)
	{
		Stroke& str = strokeStack[i];
		transformEditor->transmitStrokeEntry(i + 1, str.brushMode);
	}
}
//------------------------------------------------------------------------------

int CDisplayer::pushStrokeFromTarget(int macrofileleng)
{
	// "thru" or "erase"
	Stroke newStroke;
	newStroke.brushMode = brushMode;

	// length of macro file at start of stroke
	newStroke.macroFileLength = macrofileleng;

	// open the PELS channel and seek to end
	paintPelsChannel->openStrokeChannel(frameComponentCount, pelsName);

	// save the channel addr where it will be pushed
	newStroke.pelsAddr = paintPelsChannel->getStrokeChannelFilePtr();

	newStroke.pelsBytes = paintPelsChannel->putDifferenceRunsToChannel(targetPreIntBuf, targetIntBuf, targetPreIntBuf,
			frameWidth, frameHeight, true, true);
	// close the PELS channel
	paintPelsChannel->closeStrokeChannel();

	// record the transform for the new stroke
	getTransform(&newStroke.rotation, &newStroke.stretchX, &newStroke.stretchY, &newStroke.skew, &newStroke.offsetX,
			&newStroke.offsetY);

	// the import frame is a key part of the stroke context
	newStroke.importFrame = importFrame;

	// push the stroke
	strokeStack.push_back(newStroke);

	// transmit the stroke stack to the GUI
	transmitStrokeStack();

	// update the "pre-stroke" buffers in preparation for next stroke
	//
	// The "hard edge" BUG manifested as follows: enter any mode and make
	// a stroke, then reduce the brush size and make a stroke within the
	// area of the first stroke. You will see a "hard edge" instead of a
	// feathered one, in the area of the second stroke. This does not hap-
	// pen if the first stroke is accepted before the second stroke is
	// made. The problem was fixed by renewing the paintMdfdBuf and paint-
	// PreMdfdBuf, just as in "reloadModifiedTargetFrame" (which is called
	// by Accept).
	//
	MTImemset(paintMdfdBuf, 0, 2*frameWidth*frameHeight);

	MTImemcpy((void *)targetPreIntBuf, (void *)targetIntBuf, 2*frameWidth*frameHeight*frameComponentCount);

	return 0;
}
//------------------------------------------------------------------------------

// this method pops the given number of entries from
// the stroke stack and restores the import frame
// and transformation context accordingly
//
int CDisplayer::popStrokesToTarget(int popnum)
{
	int lastPopped = strokeStack.size() - popnum;

	// can't pop more than the entire stack
	if (lastPopped < 0)
	{
		return -1;
	}

	// return this value to pop strokes from the macro file
	int macrofileleng = strokeStack[lastPopped].macroFileLength;

	// Restore the context of import frame and
	// xform back to the last remaining stroke
	//
	// if popped transform is different from current do
	// popTransform = true
	// else
	// popTransform = false

	double rot, strx, stry, skw, offx, offy;
	getTransform(&rot, &strx, &stry, &skw, &offx, &offy);

	bool popTransform = ((strokeStack[lastPopped].rotation != rot) || (strokeStack[lastPopped].stretchX != strx) ||
			(strokeStack[lastPopped].stretchY != stry) || (strokeStack[lastPopped].skew != skw) ||
			(strokeStack[lastPopped].offsetX != offx) || (strokeStack[lastPopped].offsetY != offy));

	// if popped import frame is different from current
	// restore the old import frame
	// popTransform = true

	if (strokeStack[lastPopped].importFrame != importFrame)
	{
		// mbraca added this because sometimes you get here without having
		// set up the buffers yet (on a framing change, maybe?)
		allocateImportBuffers();

		// get the popped stroke's import frame
		int retVal = mainWindow->GetPlayer()->getToolFrame(importPreIntBuf, strokeStack[lastPopped].importFrame,
				_importClipFrameList);
      CheckFencepostOverrun(importPreIntBuf);
		if (retVal != -1)
		{ // success
			importFrame = strokeStack[lastPopped].importFrame;
			importFrameOffset = importFrame - targetFrame;
		}
		else
		{ // go back
			mainWindow->GetPlayer()->getToolFrame(importPreIntBuf, importFrame, _importClipFrameList);
         CheckFencepostOverrun(importPreIntBuf);
		}

		// make the new proxy and transmit it
		makeAndTransmitProxy();

		transformEditor->transmitImportFrameTimecode(importFrame);
		transformEditor->transmitImportFrameOffset(importFrameOffset);

		popTransform = true;
	}

	// if popTransform == true
	// set the transform to the popped one

	if (popTransform)
	{
		transformEditor->setTransform(strokeStack[lastPopped].rotation, strokeStack[lastPopped].stretchX,
				strokeStack[lastPopped].stretchY, strokeStack[lastPopped].skew, strokeStack[lastPopped].offsetX,
				strokeStack[lastPopped].offsetY);
	}

	// Now pop ALL the strokes from last thru lastPopped back
	// into the buffers paintMdfdBuf and targetIntBuf. Capture
	// the addresses to reset the channel file ptrs
	//
	MTI_INT64 paintMdfdReset = 0;
	MTI_INT64 paintPelsReset = 0;

	// open the pixel channel
	paintPelsChannel->openStrokeChannel(frameComponentCount, pelsName);

	// mbraca added this because sometimes you get here without having
	// set up the buffers yet (on a framing change, maybe?)
	allocateTargetBuffers();

	for (int i = strokeStack.size() - 1; i >= lastPopped; i--)
	{
		// seek to the location in pels channel & get runs
		paintPelsChannel->getDifferenceRunsFmChannel(strokeStack[i].pelsAddr, strokeStack[i].pelsBytes, targetIntBuf,
				frameWidth, frameHeight);

		// capture the file ptr value after popping the pixel region
		paintPelsReset = strokeStack[i].pelsAddr;

		// pop the entry
		strokeStack.pop_back();
	}

	// set length of PELS channel to account for pops, then close & truncate
	paintPelsChannel->setStrokeChannelFilePtr(paintPelsReset);
	paintPelsChannel->closeAndTruncateStrokeChannel();

	// transmit the stroke stack to the GUI
	transmitStrokeStack();

	// "hard edge" bug
	MTImemset(paintMdfdBuf, 0, 2*frameWidth*frameHeight);

	MTImemcpy((void *)targetPreIntBuf, (void *)targetIntBuf, 2*frameWidth*frameHeight*frameComponentCount);

	return macrofileleng;
}
//------------------------------------------------------------------------------

void CDisplayer::completeMacroStroke()
{
	// these have to be done at the end of a macro stroke
	MTImemset(paintMdfdBuf, 0, 2*frameWidth*frameHeight);

	MTImemcpy((void *)targetPreIntBuf, (void *)targetIntBuf, 2*frameWidth*frameHeight*frameComponentCount);
}
//------------------------------------------------------------------------------

int CDisplayer::setImportFrameClipName(const string &clipName)
{
	CAutoErrorReporter autoErr("CDisplayer::setImportFrameClipName", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (_importFrameClip != nullptr)
	{
		CBinManager binManager;
		binManager.closeClip(_importFrameClip);
		_importFrameClip = nullptr;
		_importClipFrameList = nullptr;
	}

	_importFrameClipName = clipName;
	if (_importFrameClipName.empty())
	{
		fetchImportFrame(desiredImportFrame);
		makeAndTransmitProxy();
		mainWindow->RefreshBinManagerClipList();
		return 0;
	}

	int retVal = 0;
	CBinManager binManager;

	_importFrameClip = binManager.openClip(_importFrameClipName, &retVal);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Can't open clip " << _importFrameClipName;
		return retVal;
	}

	// Look up the main video framelist from the clip just once
	_importClipFrameList = _importFrameClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
	if (_importClipFrameList == nullptr)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Unable to get framelist from clip " << _importFrameClipName;
		return retVal;
	}

	syncImportFrameTimecode();

	mainWindow->RefreshBinManagerClipList();

	return 0;
}
//------------------------------------------------------------------------------

string CDisplayer::getImportFrameClipName()
{
	return _importFrameClipName;
}
//------------------------------------------------------------------------------

ClipSharedPtr CDisplayer::getImportClip()
{
	return _importFrameClip;
}
//------------------------------------------------------------------------------

CVideoFrameList* CDisplayer::getImportClipVideoFrameList()
{
	return _importClipFrameList;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportMode(int newmode)
{
	// WAS: importFrame = desiredImportFrame = -1;
	//
	// importFrame is only set at the moment
	// that import frame is loaded, but
	// desiredImportFrame is always known
	// from the frame, mode and offset

	importFrameMode = newmode;

	if (newmode == IMPORT_MODE_NONE)
	{
		importFrame = desiredImportFrame;
	}

	transformEditor->transmitImportFrameMode(newmode);
}
//------------------------------------------------------------------------------

int CDisplayer::getImportMode()
{
	return importFrameMode;
}
//------------------------------------------------------------------------------

int CDisplayer::computeDesiredImportFrame(int trgfrm)
{
	if (trgfrm == -1)
	{
		trgfrm = lastFrame;
	}

	if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
	{
		return mainWindow->GetPlayer()->checkTimecodeLimits(trgfrm + importFrameOffset, _importClipFrameList);
	}
	else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
	{
		return importFrame;
	}
	else if (importFrameMode == IMPORT_MODE_ORIGINAL)
	{
		return trgfrm;
	}

	return -1;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportFrameOffset(int impoff)
{
	if (desiredTargetFrame == -1)
	{
		desiredTargetFrame = lastFrame;
	}

	// check against limits
	desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(desiredTargetFrame + impoff, _importClipFrameList);

	// go out and get the frame
	fetchImportFrame(desiredImportFrame);
	importFrame = desiredImportFrame;

	if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
	{
		desiredImportFrameOffset = impoff;
	}
	else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
	{
		desiredImportFrameOffset = desiredImportFrame - desiredTargetFrame;
	}
	else if (importFrameMode == IMPORT_MODE_ORIGINAL)
	{
		desiredImportFrameOffset = 0;
	}

	importFrameOffset = desiredImportFrameOffset;

	// transmit these to the GUI
	makeAndTransmitProxy();
	transformEditor->transmitImportFrameTimecode(importFrame);
	transformEditor->transmitImportFrameOffset(importFrameOffset);
}
//------------------------------------------------------------------------------

bool CDisplayer::syncImportFrameTimecode()
{
	if (_importClipFrameList == nullptr)
	{
		return false;
	}

	bool retVal;

	if (desiredTargetFrame == -1)
	{
		desiredTargetFrame = lastFrame;
	}

	CTimecode currentClipInTimecode = mainWindow->GetPlayer()->getInTimecode();
	CTimecode importClipInTimecode = _importClipFrameList->getInTimecode();
	desiredImportFrameOffset = currentClipInTimecode - importClipInTimecode;
	desiredImportFrame = desiredTargetFrame + desiredImportFrameOffset;

	// Restrict to import clip limits.
	importFrame = mainWindow->GetPlayer()->checkTimecodeLimits(desiredImportFrame, _importClipFrameList);
	retVal = importFrame == desiredImportFrame;
	////desiredImportFrameOffset = desiredImportFrame - desiredTargetFrame;

	// Go out and get the frame.
	fetchImportFrame(importFrame);
	// importFrame = desiredImportFrame;
	importFrameOffset = importFrame - desiredTargetFrame;

	// transmit these to the GUI
	makeAndTransmitProxy();
	transformEditor->transmitImportFrameTimecode(importFrame);
	transformEditor->transmitImportFrameOffset(desiredImportFrameOffset);

	return importFrameOffset == desiredImportFrameOffset;
}
//------------------------------------------------------------------------------

int CDisplayer::getImportFrameOffset()
{
	return importFrameOffset;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportFrameTimecode(int imptim)
{
	desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(imptim, _importClipFrameList);
	desiredImportFrameOffset = desiredImportFrame - lastFrame; // desiredTargetFrame;

	fetchImportFrame(desiredImportFrame);
	importFrame = desiredImportFrame;
	importFrameOffset = desiredImportFrameOffset;

	// transmit these to the GUI
	makeAndTransmitProxy();
	transformEditor->transmitImportFrameOffset(importFrameOffset);
	transformEditor->transmitImportFrameTimecode(importFrame);
}
//------------------------------------------------------------------------------

int CDisplayer::getImportFrameTimecode()
{
	return importFrame;
}
//------------------------------------------------------------------------------

void CDisplayer::allocateImportBuffers()
{
	// here we allocate the two buffers separately
	// helping to fix 4K buffer allocation problem
	if (importPreIntBuf == nullptr)
	{
		importPreIntBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
      CheckFencepostOverrun(importPreIntBuf);
	}

	if (importIntBuf == nullptr)
	{
		importIntBuf = (MTI_UINT16*) MTImalloc(frameInternalSize);
	}

	if (_frameCompareOutputMask == nullptr)
	{
		_frameCompareOutputMask = new bool[frameWidth * frameHeight];
	}
}
//------------------------------------------------------------------------------

// CAREFUL!! There is code very similar to this in fetchImportFrame()!!!
void CDisplayer::preloadImportFrame(int import, int impmod, double rot, double strx, double stry, double skew,
		double offx, double offy)
{
	// Allocate the import buffers if not done yet
	allocateImportBuffers();

	// SIDE EFFECT ALERT!
	importFrameOffset = importFrame - targetFrame;

	if (impmod != IMPORT_MODE_ORIGINAL)
	{
		// Careful - getToolFrame returns the FRAME NUMBER or -1 (to indicate error)
		importFrame = mainWindow->GetPlayer()->getToolFrame(importPreIntBuf, import, _importClipFrameList);
      CheckFencepostOverrun(importPreIntBuf);

		// SIDE EFFECT ALERT! This transforms importPreIntBuf to importIntBuf!
		setTransform(rot, strx, stry, skew, offx, offy);
      CheckFencepostOverrun(importPreIntBuf);
	}
	else
	{
		// IMPORT_MODE_ORIGINAL
		if (import == targetFrame)
		{
			MTImemcpy((void *)importPreIntBuf, (void *)targetIntBuf, frameInternalSize);
         CheckFencepostOverrun(importPreIntBuf);
		}
		else
		{
			importFrame = mainWindow->GetPlayer()->getToolFrame(importPreIntBuf, import);
         CheckFencepostOverrun(importPreIntBuf);
			MTIassert(importFrame >= 0);
			if (importFrame == -1)
			{
				return;
			}
		}

		int retVal;
		CAutoHistoryOpener opener(*paintHistory, FLUSH_TYPE_RESTORE, import, ALL_FIELDS, nullptr, nullptr, nullptr, retVal);
		if (retVal == 0)
		{
			while (paintHistory->nextRestoreRecord() == 0)
			{
				if (!paintHistory->getCurrentRestoreRecordResFlag())
				{
					retVal = paintHistory->restoreCurrentRestoreRecordPixels(importPreIntBuf, false);
               CheckFencepostOverrun(importPreIntBuf);

					if (retVal != 0)
					{
						break;
					}
				}
			}
		}

		MTImemcpy((void *)importIntBuf, (void *)importPreIntBuf, frameInternalSize);
      CheckFencepostOverrun(importPreIntBuf);

      // keep this properly set
      clearTransform();
  	}
}
//------------------------------------------------------------------------------

void CDisplayer::forceTargetAndImportFrameLoaded()
{
	if (targetFrame != desiredTargetFrame)
	{
		// Do this before the load (for "stroked target" hack)
		clearStrokeStack();
		loadTargetFrame(desiredTargetFrame);
		transformEditor->onLoadNewTargetFrame();
		targetFrame = desiredTargetFrame;
	}

	if (importFrame != desiredImportFrame)
	{
		fetchImportFrame(desiredImportFrame);
		importFrame = desiredImportFrame;
		makeAndTransmitProxy();
	}
	// new: always force transform of import frame when doing tracking alignment
	else if (useTrackingAlignOffsets)
	{
		if ((importIntBuf != nullptr) && (importPreIntBuf != nullptr) && (imgFmt != nullptr))
		{

			rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
		}
	}

	// NO! DON'T DO THIS because if this method is called when
	// desiredTargetFrame or desiredImport frame are not properly set,
	// it does permanent damage!
	//
	// importFrameOffset = importFrame - targetFrame;
}
//------------------------------------------------------------------------------

void CDisplayer::loadTargetAndImportFrames(int trgfrm)
{
	// this code has been lifted from the original DisplayFrame.cpp because
	// that entry point no longer provides a synchronous load of target and
	// import frames. That fn is needed by the AutoAccept mechanism, which
	// sets up new target and import frames on MOUSE-DN in a new frame

	if (importFrameMode == IMPORT_MODE_RELATIVE)
	{
		desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(trgfrm + importFrameOffset);
	}
	else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
	{
      if (importFrame >= 0)
      {
         if (desiredImportFrame < 0)
         {
            desiredImportFrame = trgfrm;
         }

			importFrame = desiredImportFrame;
      }

		importFrameOffset = importFrame - trgfrm;
	}
	else if (importFrameMode == IMPORT_MODE_ORIGINAL)
	{
		desiredImportFrame = trgfrm;
		importFrameOffset = 0;
	}
	else if (importFrameMode == IMPORT_MODE_ALTCLIP)
	{
		desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(trgfrm + importFrameOffset,
		_importClipFrameList);
	}

	if (targetFrame != trgfrm)
	{
		// this has a callback to update the GUI listbox
		// Do this before the load (for "stroked target" hack)
		clearStrokeStack();

		// load the target frame and get set up
		loadTargetFrame(trgfrm);

		// we've got our target frame
		desiredTargetFrame = trgfrm;
		targetFrame = desiredTargetFrame;
	}

	// Load the import frame if not already loaded.
	// QQQ Why does ORIGINAL ALWAYS create a new import frame?
	if (importFrameMode == IMPORT_MODE_ORIGINAL || importFrame != desiredImportFrame)
	{
		fetchImportFrame(desiredImportFrame);
		importFrame = desiredImportFrame;

		makeAndTransmitProxy();
		transformEditor->transmitImportFrameTimecode(importFrame);
		transformEditor->transmitImportFrameOffset(importFrameOffset);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::loadImportFrame(int import)
{
	// Allocate the import buffers if not done yet
	allocateImportBuffers();

	if (captureMode == CAPTURE_MODE_ALTCLIP_IMPORT)
	{
		mainWindow->GetPlayer()->getToolFrame(importIntBuf, import, _importClipFrameList);
	}
	else
	{
		if (mainWindow->GetPlayer()->getPlaybackFilter() == PLAYBACK_FILTER_NONE && _lastDisplayedFrameBuffer)
		{
			// the last display fields contain neither
			// original values nor highlighted fixes
			MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
         CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
			extr->extractActivePicture(importPreIntBuf, stupidBuf, imgFmt, &frameImageFormat);
         CheckFencepostOverrun(importPreIntBuf);
		}
		else
		{ // need a fresh copy right from the media,
			// sans original values & highlighted fixes

			mainWindow->GetPlayer()->getToolFrame(importPreIntBuf, import);
         CheckFencepostOverrun(importPreIntBuf);
		}

		// initialize importIntBuf using
		// the current transform settings
		rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
	}

	// make the new proxy and transmit it
	makeAndTransmitProxy();
	transformEditor->transmitImportFrameTimecode(import);
	transformEditor->transmitImportFrameOffset(importFrameOffset);
}
//------------------------------------------------------------------------------

// CAREFUL!! There is code very similar to this in preloadImportFrame()!!!
void CDisplayer::fetchImportFrame(int import, bool fast)
{
	// Allocate the import buffers if not done yet
	allocateImportBuffers();

	if (importFrameMode == IMPORT_MODE_ORIGINAL)
	{
		createOriginalValuesImportFrame(import, fast);
		return;
	}
	if (captureMode == CAPTURE_MODE_ALTCLIP_IMPORT)
	{
		mainWindow->GetPlayer()->getToolFrame(importIntBuf, import, _importClipFrameList);
		return;
	}

	// this call goes out and uses ReadMedia to load
	// the raw frame, then extract the intermediate
		CheckFencepostOverrun(importPreIntBuf);
	int fetchedImportFrame = mainWindow->GetPlayer()->getToolFrame(
			/*** fast ? importIntBuf : ** */ importPreIntBuf, import, _importClipFrameList);
	MTIassert(fetchedImportFrame >= 0);
      CheckFencepostOverrun(importPreIntBuf);

	// THIS WAS A VERY BAD IDEA! This stuff is needed for range macros while playing!
	// if (fast)
	// {
	// return;
	// }

	// initialize importIntBuf using
	// the current transform settings
	rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
}
//------------------------------------------------------------------------------

void CDisplayer::createOriginalValuesImportFrame(int importFrameIndex, bool fast)
{
	MTI_UINT16 *importBuf = fast ? importIntBuf : importPreIntBuf;
	if (importFrameIndex == targetFrame && !fast)
	{
		// optimization!
		// CAREFUL! This is dangerous because while playing we may not have filled
		// in targetOrgIntBuf.
		MTImemcpy(importBuf, targetOrgIntBuf, frameInternalSize);
      CheckFencepostOverrun(importPreIntBuf);
	}
	else
	{
		// QQQ extract from displayFields while playing? We're probably OK
		// performance-wise because of the frame reading cache.
		int fetchedImportFrame = mainWindow->GetPlayer()->getToolFrame(importBuf, importFrameIndex, _importClipFrameList);
	}

	int retVal = createOriginalValuesFrame(importBuf, importFrameIndex);
	MTIassert(retVal == 0 || retVal == HISTORY_ERROR_OPENING_HST_FILE);
   if (retVal != 0 && retVal != HISTORY_ERROR_OPENING_HST_FILE)
   {
      TRACE_0(errout << "ERROR: HST file I/O failed");
   }

	if (!fast)
	{
		// I don't know what the true purpose of the "Pre" buffer is, therefore
		// I don't know why we don't just read directly into the import buffer!
		// Note that this copy used to be bypassed when an error occurred reading
		// this history file (including NO history file - bug!). oh well QQQ
		MTImemcpy(importIntBuf, importPreIntBuf, frameInternalSize);
	}
}
//------------------------------------------------------------------------------

int CDisplayer::createOriginalValuesFrame(MTI_UINT16 *buffer, int frameIndex)
{
	int retVal;
	CAutoHistoryOpener opener(*paintHistory, FLUSH_TYPE_RESTORE, frameIndex, ALL_FIELDS, nullptr, nullptr, nullptr, retVal);
	if (retVal == 0)
	{
		while (paintHistory->nextRestoreRecord() == 0)
		{
			if (!paintHistory->getCurrentRestoreRecordResFlag())
			{
				int historyReadingErrorCode = paintHistory->restoreCurrentRestoreRecordPixels(buffer, false);
				MTIassert(historyReadingErrorCode == 0);
				if (historyReadingErrorCode != 0)
				{
					break;
				}
			}
		}
	}

	return retVal;
}
//------------------------------------------------------------------------------

void CDisplayer::setOnionSkinMode(int onionmode)
{
	onionSkinEnabled = onionmode;
}
//------------------------------------------------------------------------------

void CDisplayer::setOnionSkinTargetWgt(double wgt)
{
	if (wgt < 0.0)
	{
		wgt = 0.0;
	}
	else if (wgt > 1.0)
	{
		wgt = 1.0;
	}

	onionSkinTargetWgt = (unsigned char)(255. * wgt);
}
//------------------------------------------------------------------------------

void CDisplayer::setMouseMinorState(int minstate)
{
	minorState = minstate;
}
//------------------------------------------------------------------------------

void CDisplayer::setColorPickSampleSize(int siz)
{
	colorPickSampleSize = siz;
}
//------------------------------------------------------------------------------

void CDisplayer::setFillTolerance(int tol)
{
	fillTolerance = tol;
}
//------------------------------------------------------------------------------

void CDisplayer::setDensityStrength(int denstrng)
{
	densityStrength = denstrng;

	if (densityStrength > 0.)
	{
		fDensityFactor = ((float)densityStrength / 100.) * (MAX_DENSITY - 1.) + 1.;
	}
	else
	{
		fDensityFactor = ((float)densityStrength / 100.) * (1. - MIN_DENSITY) + 1.;
		if (fDensityFactor < 0.)
		{
			fDensityFactor = 0.;
		}
	}

	iDensityFactorTimes1024 = (unsigned int)(fDensityFactor * 1024. + 0.5);

	// recalculate these
	fGrainStrength = clamp<float>(fGrainStrength, GRAIN_STRENGTH_LOW_LIMIT, GRAIN_STRENGTH_HIGH_LIMIT);
	ulGrainStrengthDensityTimes1024 = (unsigned int)(fGrainStrength * iDensityFactorTimes1024);
	fGrainStrengthDensity = fGrainStrength * fDensityFactor;

	// careful to avoid fGrainStrength = 0
	// This makes NO FUCKING SENSE - It wasn't careful at all. I changed MIN_GRAIN_STENGTH
	// to be 0.0001 instead of 0.0. Wtf!!
	////	if ((fGrainStrength >= GRAIN_STRENGTH_LOW_LIMIT) && (fGrainStrength <= GRAIN_STRENGTH_HIGH_LIMIT))
	////	{
	////		ulOneOverGrainStrengthDensityTimes1024 = (unsigned int)((1. / fGrainStrength) * iDensityFactorTimes1024);
	////	}

	fOneOverGrainStrengthDensity = (1.0F / fGrainStrength) * fDensityFactor;
	ulOneOverGrainStrengthDensityTimes1024 = (unsigned int)((1. / fGrainStrength) * iDensityFactorTimes1024);
}
//------------------------------------------------------------------------------

void CDisplayer::setGrainStrength(int grstrng)
{
	grainStrength = grstrng;

	fGrainStrength = ((float)grainStrength / 100.) * (GRAIN_STRENGTH_LOW_LIMIT - GRAIN_STRENGTH_HIGH_LIMIT) +
			GRAIN_STRENGTH_HIGH_LIMIT;
	fGrainStrength = clamp<float>(fGrainStrength, GRAIN_STRENGTH_LOW_LIMIT, GRAIN_STRENGTH_HIGH_LIMIT);

	// recalculate these
	ulGrainStrengthDensityTimes1024 = (unsigned int)(fGrainStrength * iDensityFactorTimes1024);
	ulOneOverGrainStrengthDensityTimes1024 = (unsigned int)((1. / fGrainStrength) * iDensityFactorTimes1024);
	fGrainStrengthDensity = fGrainStrength * fDensityFactor;
	fOneOverGrainStrengthDensity = (1.0F / fGrainStrength) * fDensityFactor;
}
//------------------------------------------------------------------------------

void CDisplayer::setGrainPresets(GrainPresets *grainPresets)
{
	_grainPresets = grainPresets;
}
//------------------------------------------------------------------------------

// this method now moves in units of 1/5 pixel
void CDisplayer::moveImportFrame(int delx, int dely)
{
	if (displayProcessed && (lastFrame == targetFrame))
	{
		if (minorState == ACQUIRING_IMPORT_FRAME)
		{
			double froffx = delx * .2;
			double froffy = dely * .2;

			if ((froffx != 0.0) || (froffy != 0.0))
			{
				impOff5X += froffx;
				impOff5Y += froffy;

				rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
            CheckFencepostOverrun(importPreIntBuf);
			}
			rotatedSinceLastTranslation = false;

			displayFrame(_lastDisplayedFrameBuffer, targetFrame);
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::enableBrushDisplay(bool onoff)
{
	brushEnabled = onoff;
}
//------------------------------------------------------------------------------

void CDisplayer::loadPaintBrush(CBrush *newbrush, int newmode, int darkerthrsh, int lighterthrsh, float *colorcomp)
{
	paintBrush = newbrush;

	// THRU or ERASE or ORIG
	brushMode = newmode;

	// load the thresholds (note minus !)
	darkerThreshold = -darkerthrsh;
	lighterThreshold = lighterthrsh;

	// are we using them?
	usingThresholds = (darkerThreshold != 0) || (lighterThreshold != 0);

	// painting with COLOR?
	usingColor = (colorcomp != nullptr);
	if (usingColor)
	{
		colors[0] = colorcomp[0];
		colors[1] = colorcomp[1];
		colors[2] = colorcomp[2];

		switch (brushComponents)
		{
			case BRUSH_COMPONENTS_RGB:
				colorComponents[0] = (MTI_UINT16)(colorcomp[0] * (float)frameMaxComponentValue);
				colorComponents[1] = (MTI_UINT16)(colorcomp[1] * (float)frameMaxComponentValue);
				colorComponents[2] = (MTI_UINT16)(colorcomp[2] * (float)frameMaxComponentValue);
				break;

			case BRUSH_COMPONENTS_RGB_HALF:
			{
				MTI_UINT16 white = ImageDatum::fromFloat(1.0F, true);
				MTI_UINT16 black = ImageDatum::fromFloat(0.0F, true);
				MTI_UINT16 range = white - black;
				colorComponents[0] = black + (MTI_UINT16)(colorcomp[0] * range);
				colorComponents[1] = black + (MTI_UINT16)(colorcomp[1] * range);
				colorComponents[2] = black + (MTI_UINT16)(colorcomp[2] * range);
			} break;

			case BRUSH_COMPONENTS_BGR:
				colorComponents[0] = (MTI_UINT16)(colorcomp[2] * (float)frameMaxComponentValue);
				colorComponents[1] = (MTI_UINT16)(colorcomp[1] * (float)frameMaxComponentValue);
				colorComponents[2] = (MTI_UINT16)(colorcomp[0] * (float)frameMaxComponentValue);
				break;

			case BRUSH_COMPONENTS_YUV:
				// convert RGB components to YUV for painting
				colorComponents[0] = (MTI_UINT16)((colorcomp[0] * YR + colorcomp[1] * YG + colorcomp[2] * YB + .0625) * (float)frameMaxComponentValue);
				colorComponents[1] = (MTI_UINT16)((colorcomp[0] * UR + colorcomp[1] * UG + colorcomp[2] * UB + .5000) * (float)frameMaxComponentValue);
				colorComponents[2] = (MTI_UINT16)((colorcomp[0] * VR + colorcomp[1] * VG + colorcomp[2] * VB + .5000) * (float)frameMaxComponentValue);
				break;
		}
	}

	// default is brush ON
	brushEnabled = true;
}
//------------------------------------------------------------------------------

CBrush * CDisplayer::getPaintBrush(int *drkrthr, int *lgtrthr, bool *usingcol, float *col)
{
	*drkrthr = darkerThreshold;
	*lgtrthr = lighterThreshold;

	*usingcol = usingColor;

	col[0] = colors[0];
	col[1] = colors[1];
	col[2] = colors[2];

	return paintBrush;
}
//------------------------------------------------------------------------------

void CDisplayer::loadCloneBrush(CBrush *newbrush, bool isrel, int x, int y)
{
	cloneBrush = newbrush;

	// RELATIVE to paint brush?
	isRel = isrel;

	// CLONE coords or offsets
	cloneX = x;
	cloneY = y;

	// default is brush ON
	brushEnabled = true;
}
//------------------------------------------------------------------------------

CBrush * CDisplayer::getCloneBrush(bool *rel, int *x, int *y)
{
	*rel = isRel;

	*x = cloneX;
	*y = cloneY;

	return cloneBrush;
}
//------------------------------------------------------------------------------

void CDisplayer::unloadPaintBrush()
{
	paintBrush = nullptr;
}
//------------------------------------------------------------------------------

void CDisplayer::unloadCloneBrush()
{
	cloneBrush = nullptr;
}
//------------------------------------------------------------------------------

void CDisplayer::setAutoAlignBoxSize(int hlfsiz)
{
	autoAlignBoxSize = hlfsiz;
}
//------------------------------------------------------------------------------

void CDisplayer::autoAlign(int xbeg, int ybeg)
{
	// snap the box size up to power of 2
	int boxsize = autoAlignBoxSize;
	int rtshft = 0;
	while (boxsize != 0)
	{
		boxsize >>= 1;
		rtshft++;
	}

	boxsize = ((int)1) << rtshft;

	// delta pixels between eval pts
	// means we do 289 pts each time
	int stepsize = boxsize / 8;

	// the theory here changes slightly now
	// that onionSkinOfsets are always 0 &
	// the transformed Int frame embodies the
	// offset. We will calculate the offset
	// to be ADDED to (impOff5X, impOff5Y) to
	// minimize the norm of the diff of the
	// frames

	int xbest = 0;
	int ybest = 0;

	// we'll cycle down to a box of 16x16 around (xbeg, ybeg)
	while (stepsize >= 1)
	{
		// we'll vary the import offsets
		// over this range and find the
		// value which produces the best
		// match of import and target

		RECT offsetRng;
		offsetRng.left = xbest - boxsize;
		offsetRng.top = ybest - boxsize;
		offsetRng.right = xbest + boxsize;
		offsetRng.bottom = ybest + boxsize;

		// init the norm to a great big value
		double minnrm = 1000000000;

		for (int y = offsetRng.top; y <= offsetRng.bottom; y += stepsize)
		{
			for (int x = offsetRng.left; x <= offsetRng.right; x += stepsize)
			{
				// evaluate the norm of the delta(import, target)
				// over the following target box around the auto-
				// align point (xbeg, ybeg)

				RECT targetRng;
				targetRng.left = xbeg - boxsize;
				targetRng.top = ybeg - boxsize;
				targetRng.right = xbeg + boxsize;
				targetRng.bottom = ybeg + boxsize;

				// clip to target buffer
				if (targetRng.left < 0)
				{
					targetRng.left = 0;
				}

				if (targetRng.top < 0)
				{
					targetRng.top = 0;
				}

				if (targetRng.right >= frameWidth)
				{
					targetRng.right = (frameWidth - 1);
				}

				if (targetRng.bottom >= frameHeight)
				{
					targetRng.bottom = (frameHeight - 1);
				}

				// clip to import buffer (note offset (x, y))
				if (targetRng.left < x)
				{
					targetRng.left = x;
				}

				if (targetRng.top < y)
				{
					targetRng.top = y;
				}

				if (targetRng.right >= (frameWidth + x))
				{
					targetRng.right = (frameWidth - 1 + x);
				}

				if (targetRng.bottom >= (frameHeight + y))
				{
					targetRng.bottom = (frameHeight - 1 + y);
				}

				double norm = 0;
				int totpts = 0;
				for (int ytrg = targetRng.top; ytrg <= targetRng.bottom; ytrg += stepsize)
				{
					int trgoff = ytrg * frameWidth;
					int impoff = (ytrg - y) * frameWidth;

					for (int xtrg = targetRng.left; xtrg <= targetRng.right; xtrg += stepsize)
					{
						MTI_UINT16 *trgpel = targetIntBuf + (trgoff + xtrg) * frameComponentCount;
						MTI_UINT16 *imppel = importIntBuf + (impoff + xtrg - x) * frameComponentCount;

						for (int i = 0; i < frameComponentCount; i++)
						{
							double delcmp = (double)(trgpel[i] - imppel[i]);
							norm += delcmp * delcmp;
						}

						totpts++;
					}
				}

				// norm is an average over the pts contributing
				norm /= (double)totpts;

				// looking for the smallest value of the norm
				if (norm < minnrm)
				{
					minnrm = norm;
					xbest = x;
					ybest = y;
				}
			} // varying x
		} // varying y

		boxsize >>= 1;
		stepsize >>= 1;
	}

	// in the new scheme we ADD the result
	// to the offsets already in force
	impOff5X += (double)xbest;
	impOff5Y += (double)ybest;

	rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);

	// and these are what appear on the transform offsets display
	int xmitOffX = impOff5X;
	int xmitOffY = impOff5Y;

	transformEditor->transmitOnionSkinOffsets(xmitOffX, xmitOffY);
}
//------------------------------------------------------------------------------

void CDisplayer::pickOneColor(int xp, int yp)
{
	float outcomp[3];
	int compmax;
	getColorPickSample(xp, yp, compmax, outcomp[0], outcomp[1], outcomp[2]);
	transformEditor->transmitPickedColor(outcomp);
}
//------------------------------------------------------------------------------

void CDisplayer::getColorPickSample(int xp, int yp, int &compmax, float &rout, float &gout, float &bout)
{
	// Init with blackness in case the point is completely clipped!
	rout = gout = bout = 0;
	compmax = frameMaxComponentValue;

	int xmin = xp - colorPickSampleSize;
	int xmax = xp + colorPickSampleSize;
	int ymin = yp - colorPickSampleSize;
	int ymax = yp + colorPickSampleSize;

	// Accumulators
	MTI_UINT32 accumulator[3] =
	{0, 0, 0};

	// clip this rectangle to the frame (important!)

	// If fully clipped, return blackness - if we don't do this then because
	// of negative numbers the value computed for pels below may be zero
	// resulting in a "division by zero" exception later on!!!!
	if (xmax < 0 || ymax < 0 || xmin >= frameWidth || ymin >= frameHeight)
	{
		return;
	}

	if (xmin < 0)
	{
		xmin = 0;
	}

	if (xmax >= frameWidth)
	{
		xmax = (frameWidth - 1);
	}

	if (ymin < 0)
	{
		ymin = 0;
	}

	if (ymax >= frameHeight)
	{
		ymax = (frameHeight - 1);
	}

	// total pels in the sample
	int pels = (xmax - xmin + 1) * (ymax - ymin + 1);

	////   // accumulate averages here
	////   for (int i=0;i<frameComponentCount;i++) {
	////		colorComponents[i] = 0;
	////   }

	// can't use frickin' "target frame"  buffer if you're not in Paint!!!!!!
	if (currentTool == CURRENT_TOOL_PAINT)
	{

		if (targetIntBuf == nullptr)
		{
			loadTargetFrame(lastFrame);
		}

		// now do the sampling
		for (int i = ymin; i <= ymax; i++)
		{
			for (int j = xmin; j <= xmax; j++)
			{
				MTI_UINT16 *pxl = targetIntBuf + (i * frameWidth + j) * frameComponentCount;

				for (int k = 0; k < frameComponentCount; k++)
				{
					accumulator[k] += pxl[k];
				}
			}
		}

	}
	else
	{ // not in paint - extract each sample point individually

		// paranoia
		if (!_lastDisplayedFrameBuffer)
		{
			return;
		}

		MTI_UINT8 *stupidBuf[2] =
		{_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};

		// now do the sampling
		for (int x = xmin; x <= xmax; ++x)
		{
			for (int y = ymin; y <= ymax; ++y)
			{
				MTI_UINT16 ry = 0, gu = 0, bv = 0;

				int retVal = extr->extractPoint(&ry, &gu, &bv, stupidBuf, imgFmt, x, y);
				MTIassert(retVal == 0);

				accumulator[0] += ry;
				accumulator[1] += gu;
				accumulator[2] += bv;
			}
		}
	}

	// average these over the number of pixels
	for (int i = 0; i < frameComponentCount; i++)
	{
		colorComponents[i] = MTI_UINT16(accumulator[i] / pels);
	}

	float incomp[3];
	float outcomp[3];

	// in the GUI the user is allowed to think "RGB"
	switch (brushComponents)
	{
		case BRUSH_COMPONENTS_RGB:

			outcomp[0] = (float)colorComponents[0] / (float)frameMaxComponentValue;
			outcomp[1] = (float)colorComponents[1] / (float)frameMaxComponentValue;
			outcomp[2] = (float)colorComponents[2] / (float)frameMaxComponentValue;

			break;

		case BRUSH_COMPONENTS_RGB_HALF:
		{
			for (int i = 0; i < 3; i++)
			{
				incomp[i] = ImageDatum::toFloat(colorComponents[i], true);
				outcomp[i] = clamp<float>(incomp[i], 0.0F, 1.0F);
			}
		}
		break;

		case BRUSH_COMPONENTS_BGR:

			outcomp[0] = (float)colorComponents[2] / (float)frameMaxComponentValue;
			outcomp[1] = (float)colorComponents[1] / (float)frameMaxComponentValue;
			outcomp[2] = (float)colorComponents[0] / (float)frameMaxComponentValue;

			break;

		case BRUSH_COMPONENTS_YUV:

			incomp[0] = (float)colorComponents[0] / (float)frameMaxComponentValue - .0625;
			incomp[1] = (float)colorComponents[1] / (float)frameMaxComponentValue - .5000;
			incomp[2] = (float)colorComponents[2] / (float)frameMaxComponentValue - .5000;

			outcomp[0] = outcomp[1] = outcomp[2] = incomp[0] * RY;

			outcomp[0] += incomp[2] * RV;
			outcomp[1] += incomp[1] * GU + incomp[2] * GV;
			outcomp[2] += incomp[1] * BU;

			if (outcomp[0] < 0)
			{
				outcomp[0] = 0;
			}
			if (outcomp[0] > 1)
			{
				outcomp[0] = 1;
			}
			if (outcomp[1] < 0)
			{
				outcomp[1] = 0;
			}
			if (outcomp[1] > 1)
			{
				outcomp[1] = 1;
			}
			if (outcomp[2] < 0)
			{
				outcomp[2] = 0;
			}
			if (outcomp[2] > 1)
			{
				outcomp[2] = 1;
			}

			break;

		case BRUSH_COMPONENTS_LUMINANCE:
		case BRUSH_COMPONENTS_YYY:

			outcomp[0] = outcomp[1] = outcomp[2] = (float)colorComponents[0] / (float)frameMaxComponentValue;
			break;
	}

	rout = outcomp[0];
	gout = outcomp[1];
	bout = outcomp[2];
}
//------------------------------------------------------------------------------

void CDisplayer::clearFillSeed()
{
	xseed = -1;
	yseed = -1;
}
//------------------------------------------------------------------------------

void CDisplayer::pickOneFillSeed(int xp, int yp)
{
	if ((xp < 0) || (xp >= frameWidth) || (yp < 0) || (yp >= frameHeight))
	{
		return;
	}

	// set this pointer to the importPreIntBuf...
	markFilled = (MTI_UINT8*)importPreIntBuf;

	// ...and clear the buffer to use for marking filled
	MTImemset(markFilled, 0, frameWidth*frameHeight);
   CheckFencepostOverrun(importPreIntBuf);

	// init the color component limits of the fill field

	MTI_UINT16 *pxl = targetIntBuf + (yp * frameWidth + xp) * frameComponentCount;

	for (int k = 0; k < frameComponentCount; k++)
	{
		// these are unsigned components
		minFillComp[k] = pxl[k] - fillTolerance;
		if (minFillComp[k] > frameMaxComponentValue)
		{
			minFillComp[k] = 0;
		}

		maxFillComp[k] = pxl[k] + fillTolerance;
		if (maxFillComp[k] > frameMaxComponentValue)
		{
			maxFillComp[k] = frameMaxComponentValue;
		}
	}

	// fill must respect the MASK - get the ROI
#if 0
	paintMaskBuf = nullptr;
	CMaskTool *theMaskTool = CNavigatorTool::GetMaskTool();
	if (theMaskTool->IsMaskAvailable())
	{
		paintMaskBuf = theMaskTool->GetRegionOfInterest()->getBlendPtr();
	}
#else
	paintMaskBuf = (CNavigatorTool::GetMaskTool())->GetRegionOfInterest()->getBlendPtr();
#endif

	// clear fill-lines stack
	fillLines.clear();

	// allocate a fill line for the seed
	FillLine *first = new FillLine;

	// fill in seed coordinates
	first->xmin = xp;
	first->xmax = xp;
	first->ylev = yp;

	// stretch the seed in X
	expandFillLine(first);

	// push it to init the fill-lines stack
	fillLines.push_back(first);

	// do we need a separate thread?
	while (iterateOneFillCycle() == 0)
	{;
	}

	// KKKKK code -1 for a FILL stroke
	pushStrokeFromTarget(-1);

	// display the frame
	displayFrame();
   CheckFencepostOverrun(importPreIntBuf);
}
//------------------------------------------------------------------------------

void CDisplayer::recycleFillSeed()
{
	if ((xseed < 0) || (xseed >= frameWidth) || (yseed < 0) || (yseed >= frameHeight))
	{
		return;
	}

	// undo the last fill pixels
	if (popStrokesToTarget(1) == -1)
	{ // KKKKK code -1 for a FILL stroke

		// do the recycling
		pickOneFillSeed(xseed, yseed);
	}
}
//------------------------------------------------------------------------------

// caller guarantees that pixel (x, y) is inside frame limits
bool CDisplayer::pixelQualifies(int x, int y)
{
	int indx = (y * frameWidth + x);

	// if pixel is marked, it's disqualified
	MTI_UINT8 *mrk = markFilled + indx;
	if (*mrk)
	{
		return false;
	}

	// if mask is zero, pixel doesn't qualify
	if (paintMaskBuf[indx] == 0)
	{
		return false;
	}

	// okay, see if all components are in range
	MTI_UINT16 *pxl = targetIntBuf + indx * frameComponentCount;

	int good = 0;
	for (int i = 0; i < frameComponentCount; i++)
	{
		if ((pxl[i] >= minFillComp[i]) && (pxl[i] <= maxFillComp[i]))
		{
			good++;
		}
	}
      CheckFencepostOverrun(importPreIntBuf);

	return (good == frameComponentCount);
}
//------------------------------------------------------------------------------

void CDisplayer::expandFillLine(FillLine *flin)
{
	// to the lft
	while ((flin->xmin > 0) && pixelQualifies((flin->xmin - 1), flin->ylev))
	{
		flin->xmin--;
	}
	// to the rgt
	while ((flin->xmax < (frameWidth - 1)) && pixelQualifies((flin->xmax + 1), flin->ylev))
	{
		flin->xmax++;
	}
}
//------------------------------------------------------------------------------

void CDisplayer::paintFillLine(FillLine *flin)
{
	int indx = (flin->ylev * frameWidth + flin->xmin);

	MTI_UINT16 *pxl = targetIntBuf + indx * frameComponentCount;
	MTI_UINT8 *mrk = markFilled + indx;
	MTI_UINT16 *mdf = paintMdfdBuf + indx;
	const MTI_UINT8 *msk = paintMaskBuf + indx;

	int delpels = (flin->xmax - flin->xmin);
	for (int i = 0; i <= delpels; i++)
	{
		// paint one pixel by blending with the mask
		for (int k = 0; k < frameComponentCount; k++)
		{
			pxl[k] = (((unsigned int)colorComponents[k]) * ((unsigned int)msk[i]) + ((unsigned int)pxl[k]) *
					((unsigned int)(255 - msk[i]))) >> 8;
		}
		pxl += frameComponentCount;

		// mark it painted
		mrk[i] = 0xff;

		// mark it modified
		mdf[i] = 32767;
	}
      CheckFencepostOverrun(importPreIntBuf);
}
//------------------------------------------------------------------------------

void CDisplayer::deriveNorthFillLines(FillLine *flin)
{
	if (flin->ylev > 0)
	{
		int x = (flin->xmin - 1);
		if (x < 0)
		{
			x++;
		}
		int lim = flin->xmax + 1;
		if (lim == frameWidth)
		{
			lim--;
		}
		while (x <= lim)
		{
			if (pixelQualifies(x, (flin->ylev - 1)))
			{
				FillLine *nuflin = new FillLine;
				nuflin->ylev = flin->ylev - 1;
				nuflin->xmin = x;
				nuflin->xmax = x;
				expandFillLine(nuflin);
				fillLines.push_back(nuflin);
				x = nuflin->xmax + 2;
			}
			else
			{
				x++;
			}
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::deriveSouthFillLines(FillLine *flin)
{
	if (flin->ylev < (frameHeight - 1))
	{
		int x = (flin->xmin - 1);
		if (x < 0)
		{
			x++;
		}
		int lim = flin->xmax + 1;
		if (lim == frameWidth)
		{
			lim--;
		}
		while (x <= lim)
		{
			if (pixelQualifies(x, (flin->ylev + 1)))
			{
				FillLine *nuflin = new FillLine;
				nuflin->ylev = flin->ylev + 1;
				nuflin->xmin = x;
				nuflin->xmax = x;
				expandFillLine(nuflin);
				fillLines.push_back(nuflin);
				x = nuflin->xmax + 2;
			}
			else
			{
				x++;
			}
		}
	}
}
//------------------------------------------------------------------------------

int CDisplayer::iterateOneFillCycle()
{
	int lastin = fillLines.size();
	if (lastin == 0)
	{
		return -1;
	}

	// get the top of the stack in curflin
	FillLine *curflin = fillLines[lastin - 1];

	// off the top of the stack
	fillLines.pop_back();

	// paint the fill line
	paintFillLine(curflin);

	// derive and push the ones above
	if (curflin->ylev > 0)
	{
		deriveNorthFillLines(curflin);
	}

	// derive and push the ones below
	if (curflin->ylev < (frameHeight - 1))
	{
		deriveSouthFillLines(curflin);
	}

	// return storage for the popped curflin
	delete curflin;

	return 0;
}
//------------------------------------------------------------------------------

int CDisplayer::getMaxComponentValue()
{
	return frameMaxComponentValue;
}
//------------------------------------------------------------------------------

void CDisplayer::forceMouseToWindowCenter(bool warpScreenCursor)
{
	// the previous & current client coordinates
	xpre = xcur;
	ypre = ycur;
	xcur = rgbRect.left + (rgbRect.right - rgbRect.left + 1) / 2;
	ycur = rgbRect.top + (rgbRect.bottom - rgbRect.top + 1) / 2;

	// the previous & current frame coordinates
	dxprefrm = dxcurfrm;
	dyprefrm = dycurfrm;
	dxcurfrm = (double)dspRect.left + (xcur - rgbRect.left) * XScaleClientToFrame;
	dycurfrm = (double)dspRect.top + (ycur - rgbRect.top) * YScaleClientToFrame;

	// the previous & current integer frame coords
	xprefrm = xcurfrm;
	yprefrm = ycurfrm;
	xcurfrm = (int)dxcurfrm;
	ycurfrm = (int)dycurfrm;

	if (warpScreenCursor)
	{
		POINT pt
		{
			xcur, ycur
		};
		ClientToScreen(windowHandle, &pt);
		SetCursorPos(pt.x, pt.y);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::setDisplayProcessed(bool proc)
{
	displayProcessed = proc;
}
//------------------------------------------------------------------------------

void CDisplayer::paintOneBrushPositionFrame(int xp, int yp)
{
	if (!brushEnabled)
	{
		return;
	}

	RECT brrect;

	// clip the brush to the frame
	brrect.left = xp + paintBrush->getMinCol();
	brrect.top = yp + paintBrush->getMinRow();
	brrect.right = xp + paintBrush->getMaxCol() - 1;
	brrect.bottom = yp + paintBrush->getMaxRow() - 1; ;

	if (brrect.left < 0)
	{
		brrect.left = 0;
	}

	if (brrect.top < 0)
	{
		brrect.top = 0;
	}

	if (brrect.right >= frameWidth)
	{
		brrect.right = frameWidth - 1;
	}

	if (brrect.bottom >= frameHeight)
	{
		brrect.bottom = frameHeight - 1;
	}

	paintMaskBuf = (CNavigatorTool::GetMaskTool())->GetRegionOfInterest()->getBlendPtr();

	if ((brushMode == BRUSH_MODE_THRU) || (brushMode == BRUSH_MODE_ORIG))
	{
		bool useGrainPresets = false;
		MTI_INT32 iPresetGrainFactorScaledByMaxComponentValue = 0;
		float fNormalizedPresetGrainFactor = 0.0F;
		const float *faPresetGrainValues = nullptr;
		if (_grainPresets != nullptr && _grainPresets->IsEnabled())
		{
			useGrainPresets = true;
			fNormalizedPresetGrainFactor =
					2.0F * ((fGrainStrength - GRAIN_STRENGTH_HIGH_LIMIT) / (GRAIN_STRENGTH_LOW_LIMIT -
					GRAIN_STRENGTH_HIGH_LIMIT));
			iPresetGrainFactorScaledByMaxComponentValue =
					(MTI_INT32)(frameMaxComponentValue * fNormalizedPresetGrainFactor);
			faPresetGrainValues = _grainPresets->GetNormalizedGrainValues(targetIntBuf, targetFrame);
		}

		// this is now specialized for the different types of media,
		// in order to rationalize the stroke thresholding feature
		//
		if (brushComponents == BRUSH_COMPONENTS_YUV)
		{
			// YUV
			for (int y = brrect.top; y <= brrect.bottom; y++)
			{
				if (!brushEnabled)
				{
					break;
				}

				int mdfyoff = y * frameWidth;

				MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;
				//
				// the import frame now has pitch = frameWidth
				//
				int yoff = mdfyoff * frameComponentCount;

				MTI_UINT16 *srcrow1 = importIntBuf + yoff;
				MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
				MTI_UINT16 *dstrow = targetIntBuf + yoff;

				int x = brrect.left;
				int xoff = brrect.left * frameComponentCount;

				for (; x <= brrect.right; x++, xoff += frameComponentCount)
				{
					if (!brushEnabled)
					{
						break;
					}

					// integer variant
					unsigned int netFactor = iDensityFactorTimes1024;
					if (grainStrength > 0)
					{
						if (!useGrainPresets)
						{
							unsigned int iRand = iran266(&lseed);
							if (iRand < RND_THR1)
							{
								if (iRand < RND_THR2)
								{
									netFactor = ulGrainStrengthDensityTimes1024;
								}
								else
								{
									netFactor = ulOneOverGrainStrengthDensityTimes1024;
								}
							}
						}
					}

					// => individual pixel
					MTI_UINT16 *mdf = mdfrow + x;
					MTI_UINT16 *src1 = srcrow1 + xoff;
					MTI_UINT16 *src2 = srcrow2 + xoff;
					MTI_UINT16 *dst = dstrow + xoff;

					if (usingColor)
					{
						src1 = colorComponents;
					}

					// get brush weight at (x, y)
					MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

					// get value of Mask entry for this pixel
					MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

					MTI_UINT32 wgt1, wgt2;

					if (wgt && msk)
					{ // inside brush & mask

						// scale wgt1 by Mask
						wgt1 = (wgt * (MTI_UINT32)msk) / 255;

						// if using THRESHOLDS determine if pel is to be modified
						if (usingThresholds)
						{
							// if   intensity(trgpel) - intensity(imppel) is pos & < lighterThreshold
							// orif intensity(trgpel) - intensity(imppel) is neg & > darkerThreshold
							// pel is not modified
							//
							unsigned int imppelint = (unsigned int)src1[0];

							unsigned int trgpelint = (unsigned int)src2[0];

							// target intensity rel to import intensity
							int deltint = ((int)trgpelint - (int)imppelint);
							if (deltint >= 0)
							{
								if (deltint < lighterThreshold)
								{
									wgt1 = 0;
								}
							}
							else
							{
								if (deltint > darkerThreshold)
								{
									wgt1 = 0;
								}
							}
						}

						// Refer to the paint modify buffer. Update the pixel's
						// weight with the greater of the current value and wgt1.
						// Use the value assigned to the pixel
						//
						if (wgt1 > (MTI_UINT32)(*mdf))
						{
							*mdf = wgt1;
						}
						else
						{
							wgt1 = *mdf;
						}

						wgt2 = UNITY - wgt1;

						//////////////////////////////////////////////////////////////

						for (int k = 0; k < frameComponentCount; k++)
						{
							MTI_INT32 whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0 :
									iPresetGrainFactorScaledByMaxComponentValue * faPresetGrainValues[xoff + yoff + k];

							MTI_INT32 temp1 = (MTI_INT32(src1[k]) + whatToAddForPresetGrain) * wgt1;
							if (k == 0)
							{
								// only the first (Y) component is influenced by netFactor
								temp1 = (temp1 / 1024) * netFactor;
							}

							MTI_INT32 temp2 = MTI_INT32(src2[0]) * wgt2;
							MTI_INT32 temp = (temp1 + temp2) / UNITY;

							temp = (temp < 0) ? 0 : temp;
							temp = (temp > frameMaxComponentValue) ? frameMaxComponentValue : temp;

							dst[k] = MTI_UINT16(temp);
						}

						//////////////////////////////////////////////////////////////

					} // inside brush envelope
				} // brush cols
			} // brush rows
		} // end YUV
		else if (brushComponents == BRUSH_COMPONENTS_RGB
      || brushComponents == BRUSH_COMPONENTS_YYY)
		{
			// RGB
			for (int y = brrect.top; y <= brrect.bottom; y++)
			{
				if (!brushEnabled)
				{
					break;
				}

				int mdfyoff = y * frameWidth;

				MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;
				//
				// the import frame now has pitch = frameWidth
				//
				int yoff = mdfyoff * frameComponentCount;

				MTI_UINT16 *srcrow1 = importIntBuf + yoff;
				MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
				MTI_UINT16 *dstrow = targetIntBuf + yoff;

				int x = brrect.left;
				int xoff = brrect.left * frameComponentCount;

				for (; x <= brrect.right; x++, xoff += frameComponentCount)
				{
					if (!brushEnabled)
					{
						break;
					}

					// integer variant
					unsigned int netFactor = iDensityFactorTimes1024;
					if (grainStrength > 0 && !useGrainPresets)
					{
						unsigned int iRand = iran266(&lseed);
						if (iRand < RND_THR1)
						{
							if (iRand < RND_THR2)
							{
								netFactor = ulGrainStrengthDensityTimes1024;
							}
							else
							{
								netFactor = ulOneOverGrainStrengthDensityTimes1024;
							}
						}
					}

					// => individual pixel
					MTI_UINT16 *mdf = mdfrow + x;
					MTI_UINT16 *src1 = srcrow1 + xoff;
					MTI_UINT16 *src2 = srcrow2 + xoff;
					MTI_UINT16 *dst = dstrow + xoff;

					if (usingColor)
					{
						src1 = colorComponents;
					}

					// get brush weight at (x, y)
					MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

					// get value of Mask entry for this pixel
					MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

					MTI_UINT32 wgt1;

					if (wgt && msk)
					{ // inside brush & mask

						// scale wgt1 by Mask
						wgt1 = (wgt * (MTI_UINT32)msk) / 255;

						// if using THRESHOLDS determine if pel is to be modified
						if (usingThresholds)
						{
							// if   intensity(trgpel) - intensity(imppel) is pos & < lighterThreshold
							// orif intensity(trgpel) - intensity(imppel) is neg & > darkerThreshold
							// pel is not modified
							//
							unsigned int imppelint =
									(RWGT * (unsigned int)src1[0] + GWGT * (unsigned int)src1[1] +
									BWGT * (unsigned int)src1[2]) >> 15;

							unsigned int trgpelint =
									(RWGT * (unsigned int)src2[0] + GWGT * (unsigned int)src2[1] +
									BWGT * (unsigned int)src2[2]) >> 15;

							// target intensity rel to import intensity
							int deltint = ((int)trgpelint - (int)imppelint);
							if (deltint >= 0)
							{
								if (deltint < lighterThreshold)
								{
									wgt1 = 0;
								}
							}
							else
							{
								if (deltint > darkerThreshold)
								{
									wgt1 = 0;
								}
							}
						}

						// Refer to the paint modify buffer. Update the pixel's
						// weight with the greater of the current value and wgt1.
						// Use the value assigned to the pixel
						//
						if (wgt1 > (MTI_UINT32)(*mdf))
						{
							*mdf = wgt1;
						}
						else
						{
							wgt1 = *mdf;
						}

#ifdef VISUALIZE_MASK
						for (int k = 0; k < frameComponentCount; k++)
						{
							dst[k] = (MTI_UINT16)(((MTI_UINT32)(512) * wgt1 + (MTI_UINT32)(0) * wgt2) / UNITY);
						}
#else
						float fwgt1 = wgt1 / (1.0F * UNITY);
						float fwgt2 = 1.0F - fwgt1;

                  if (brushComponents == BRUSH_COMPONENTS_RGB)
                  {
                     if (faPresetGrainValues == nullptr && netFactor == 1024)
                     {
                        for (int k = 0; k < frameComponentCount; k++)
                        {
                           float destValue = (src1[k] * fwgt1) + (src2[k] * fwgt2);
                           destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
                           dst[k] = (unsigned short)(destValue + 0.5F);
                        }
                     }
                     else
                     {
                        // MAGIC NUMBER 1024 QQQ
                        float fNetFactor = netFactor / 1024.0F;
                        for (int k = 0; k < frameComponentCount; k++)
                        {
                           float whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0.0F :
                                 iPresetGrainFactorScaledByMaxComponentValue * faPresetGrainValues[xoff + yoff + k];

                           float temp1 = (src1[k] + whatToAddForPresetGrain) * fwgt1 * fNetFactor;
                           float temp2 = src2[k] * fwgt2;
                           float destValue = temp1 + temp2;

                           destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
                           dst[k] = (unsigned short)(destValue + 0.5F);
                        }
                     }
                  }
                  else
                  {
                     unsigned temp[3];
                     if (faPresetGrainValues == nullptr && netFactor == 1024)
                     {
                        for (int k = 0; k < frameComponentCount; k++)
                        {
                           float destValue = (src1[k] * fwgt1) + (src2[k] * fwgt2);
                           destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
                           temp[k] = unsigned(destValue + 0.5F);
                        }
                     }
                     else
                     {
                        // MAGIC NUMBER 1024 QQQ
                        float fNetFactor = netFactor / 1024.0F;
                        for (int k = 0; k < frameComponentCount; k++)
                        {
                           float whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0.0F :
                                 iPresetGrainFactorScaledByMaxComponentValue * faPresetGrainValues[xoff + yoff + k];

                           float temp1 = (src1[k] + whatToAddForPresetGrain) * fwgt1 * fNetFactor;
                           float temp2 = src2[k] * fwgt2;
                           float destValue = temp1 + temp2;

                           destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
                           temp[k] = unsigned(destValue + 0.5F);
                        }
                     }

                     dst[0] = dst[1] = dst[2] = (unsigned short) (std::min<unsigned>(0xFFFFU, (temp[0] + temp[1] + temp[2]) / 3));
                  }
#endif

					} // inside brush envelope
				} // brush cols
			} // brush rows
		} // end RGB
		else if (brushComponents == BRUSH_COMPONENTS_RGB_HALF)
		{
			// RGB HALF
			for (int y = brrect.top; y <= brrect.bottom; y++)
			{
				if (!brushEnabled)
				{
					break;
				}

				int mdfyoff = y * frameWidth;
				MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;

				//
				// the import frame now has pitch = frameWidth
				//
				int yoff = mdfyoff * frameComponentCount;

				MTI_UINT16 *srcrow1 = importIntBuf + yoff;
				MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
				MTI_UINT16 *dstrow = targetIntBuf + yoff;

				int x = brrect.left;
				int xoff = brrect.left * frameComponentCount;

				for (; x <= brrect.right; x++, xoff += frameComponentCount)
				{
					if (!brushEnabled)
					{
						break;
					}

					// float variant
					float fNetFactor = fDensityFactor;
					if (grainStrength > 0 && !useGrainPresets)
					{
						unsigned int iRand = iran266(&lseed);
						if (iRand < RND_THR1)
						{
							if (iRand < RND_THR2)
							{
								fNetFactor = fGrainStrengthDensity;
							}
							else
							{
								fNetFactor = fOneOverGrainStrengthDensity;
							}
						}
					}

					// => individual pixel
					MTI_UINT16 *mdf = mdfrow + x;
					MTI_UINT16 *src1 = srcrow1 + xoff;
					MTI_UINT16 *src2 = srcrow2 + xoff;
					MTI_UINT16 *dst = dstrow + xoff;

					if (usingColor)
					{
						src1 = colorComponents;
					}

					// get brush weight at (x, y)
					MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

					// get value of Mask entry for this pixel
					MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

					if (wgt && msk)
					{
						// inside brush & mask
						// scale wgt1 by Mask
						float fWgt = wgt / (1.0F * UNITY);
						float fMsk = msk / 255.0F;
						float fWgt1 = fWgt * fMsk; ;

						// if using THRESHOLDS determine if pel is to be modified
						if (usingThresholds)
						{
							// if   intensity(trgpel) - intensity(imppel) is pos & < lighterThreshold
							// orif intensity(trgpel) - intensity(imppel) is neg & > darkerThreshold
							// pel is not modified
							//
							float imppelint = fRWGT * ImageDatum::toFloat(src1[0], true) + fGWGT * ImageDatum::toFloat(src1[1],
									true) + fBWGT * ImageDatum::toFloat(src1[2], true);

							float trgpelint = fRWGT * ImageDatum::toFloat(src2[0], true) + fGWGT * ImageDatum::toFloat(src2[1],
									true) + fBWGT * ImageDatum::toFloat(src2[2], true);

							float fLighterThreshold = lighterThreshold / (1.0F * UNITY);
							float fDarkerThreshold = darkerThreshold / (1.0F * UNITY);

							// target intensity rel to import intensity
							float deltint = trgpelint - imppelint;
							if (deltint >= 0.0F)
							{
								if (deltint < fLighterThreshold)
								{
									fWgt1 = 0.0F;
								}
							}
							else
							{
								if (deltint > fDarkerThreshold)
								{
									fWgt1 = 0.0F;
								}
							}
						}

						// Refer to the paint modify buffer. Update the pixel's
						// weight with the greater of the current value and wgt1.
						// Use the value assigned to the pixel
						//
						if (fWgt1 > (*mdf / (1.0F * UNITY)))
						{
							*mdf = (unsigned int)floor(((1.0F * UNITY) * fWgt1) + 0.5);
						}
						else
						{
							fWgt1 = *mdf / (1.0F * UNITY);
						}

						float fWgt2 = 1.0F - fWgt1;

						for (int k = 0; k < frameComponentCount; k++)
						{
							////old way:
							////MTI_UINT16 temp = (MTI_UINT16)(((((MTI_UINT32)src1[k] * wgt1) >> 10) * netFactor + (MTI_UINT32)src2[k] * wgt2) >> 15);
							float whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0.0F :
									fNormalizedPresetGrainFactor * faPresetGrainValues[xoff + yoff + k];
							float temp1 = ((ImageDatum::toFloat(src1[k], true) + whatToAddForPresetGrain) * fWgt1)
									* fNetFactor;
							float temp2 = ImageDatum::toFloat(src2[k], true) * fWgt2;
							float temp = temp1 + temp2;

							dst[k] = ImageDatum::fromFloat(temp, true);
						}
					} // inside brush envelope
				} // brush cols
			} // brush rows
		} // end RGB HALF
		else if (brushComponents == BRUSH_COMPONENTS_BGR)
		{
			// BGR
			for (int y = brrect.top; y <= brrect.bottom; y++)
			{
				if (!brushEnabled)
				{
					break;
				}

				int mdfyoff = y * frameWidth;

				MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;
				//
				// the import frame now has pitch = frameWidth
				//
				int yoff = mdfyoff * frameComponentCount;

				MTI_UINT16 *srcrow1 = importIntBuf + yoff;
				MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
				MTI_UINT16 *dstrow = targetIntBuf + yoff;

				int x = brrect.left;
				int xoff = brrect.left * frameComponentCount;

				for (; x <= brrect.right; x++, xoff += frameComponentCount)
				{
					if (!brushEnabled)
					{
						break;
					}

					// integer variant
					unsigned int netFactor = iDensityFactorTimes1024;
					float fPresetGrainFactor = 0.f;
					if (grainStrength > 0 && !useGrainPresets)
					{
						unsigned int iRand = iran266(&lseed);
						if (iRand < RND_THR1)
						{
							if (iRand < RND_THR2)
							{
								netFactor = ulGrainStrengthDensityTimes1024;
							}
							else
							{
								netFactor = ulOneOverGrainStrengthDensityTimes1024;
							}
						}
					}

					// => individual pixel
					MTI_UINT16 *mdf = mdfrow + x;
					MTI_UINT16 *src1 = srcrow1 + xoff;
					MTI_UINT16 *src2 = srcrow2 + xoff;
					MTI_UINT16 *dst = dstrow + xoff;

					if (usingColor)
					{
						src1 = colorComponents;
					}

					// get brush weight at (x, y)
					MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

					// get value of Mask entry for this pixel
					MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

					MTI_UINT32 wgt1, wgt2;

					if (wgt && msk)
					{ // inside brush & mask

						// scale wgt1 by Mask
						wgt1 = (wgt * (MTI_UINT32)msk) / 255;

						// if using THRESHOLDS determine if pel is to be modified
						if (usingThresholds)
						{
							// if   intensity(trgpel) - intensity(imppel) is pos & < lighterThreshold
							// orif intensity(trgpel) - intensity(imppel) is neg & > darkerThreshold
							// pel is not modified
							//
							unsigned int imppelint =
									(BWGT * (unsigned int)src1[0] + GWGT * (unsigned int)src1[1] +
									RWGT * (unsigned int)src1[2]) >> 15;

							unsigned int trgpelint =
									(BWGT * (unsigned int)src2[0] + GWGT * (unsigned int)src2[1] +
									RWGT * (unsigned int)src2[2]) >> 15;

							// target intensity rel to import intensity
							int deltint = ((int)trgpelint - (int)imppelint);
							if (deltint >= 0)
							{
								if (deltint < lighterThreshold)
								{
									wgt1 = 0;
								}
							}
							else
							{
								if (deltint > darkerThreshold)
								{
									wgt1 = 0;
								}
							}
						}

						// Refer to the paint modify buffer. Update the pixel's
						// weight with the greater of the current value and wgt1.
						// Use the value assigned to the pixel
						//
						if (wgt1 > (MTI_UINT32)(*mdf))
						{
							*mdf = wgt1;
						}
						else
						{
							wgt1 = *mdf;
						}

						float fwgt1 = wgt1 / (1.0F * UNITY);
						float fwgt2 = 1.0F - fwgt1;

						if (faPresetGrainValues == nullptr && netFactor == 1024)
						{
							for (int k = 0; k < frameComponentCount; k++)
							{
								float destValue = (src1[k] * fwgt1) + (src2[k] * fwgt2);
								destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
								dst[k] = (unsigned short)(destValue + 0.5F);
							}
						}
						else
						{
							// MAGIC NUMBER 1024 QQQ
							float fNetFactor = netFactor / 1024.0F;
							for (int k = 0; k < frameComponentCount; k++)
							{
								////old way:
								////MTI_UINT16 temp = (MTI_UINT16)(((((MTI_UINT32)src1[k] * wgt1) >> 10) * netFactor + (MTI_UINT32)src2[k] * wgt2) >> 15);

								float whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0.0F :
										iPresetGrainFactorScaledByMaxComponentValue * faPresetGrainValues[xoff + yoff + k];

								float temp1 = (src1[k] + whatToAddForPresetGrain) * fwgt1 * fNetFactor;
								float temp2 = src2[k] * fwgt2;
								float destValue = temp1 + temp2;

								destValue = std::min<float>(frameMaxComponentValue, std::max<float>(0.0F, destValue));
								dst[k] = (unsigned short)(destValue + 0.5F);
							}
						}

						// old stuff
						// wgt2 = UNITY - wgt1;
						//
						// for (int k = 0; k < frameComponentCount; k++)
						// {
						// ////old way:
						// ////MTI_UINT16 temp = (MTI_UINT16)(((((MTI_UINT32)src1[k] * wgt1) >> 10) * netFactor + (MTI_UINT32)src2[k] * wgt2) >> 15);
						// MTI_INT32 whatToAddForPresetGrain = (faPresetGrainValues == nullptr) ? 0 :
						// iPresetGrainFactorScaledByMaxComponentValue * faPresetGrainValues[xoff + yoff + k];
						// MTI_INT32 temp1 = (((MTI_INT32(src1[k]) + whatToAddForPresetGrain) * wgt1) / 1024) * netFactor;
						// MTI_INT32 temp2 = MTI_INT32(src2[k]) * wgt2;
						// MTI_INT32 temp = (temp1 + temp2) >> 15;
						//
						// temp = (temp < 0) ? 0 : temp;
						// temp = (temp > frameMaxComponentValue) ? frameMaxComponentValue : temp;
						//
						// dst[k] = MTI_UINT16(temp);
						// }
					} // inside brush envelope
				} // brush cols
			} // brush rows
		} // BGR
		else if (brushComponents == BRUSH_COMPONENTS_LUMINANCE)
		{ // luminance

			for (int y = brrect.top; y <= brrect.bottom; y++)
			{

				if (!brushEnabled)
				{
					break;
				}

				int mdfyoff = y * frameWidth;

				MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;
				//
				// the import frame now has pitch = frameWidth
				//
				int yoff = mdfyoff * frameComponentCount;

				MTI_UINT16 *srcrow1 = importIntBuf + yoff;
				MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
				MTI_UINT16 *dstrow = targetIntBuf + yoff;

				int x = brrect.left;
				int xoff = brrect.left * frameComponentCount;

				for (; x <= brrect.right; x++, xoff += frameComponentCount)
				{
					if (!brushEnabled)
					{
						break;
					}

					// integer variant
					unsigned int netFactor = iDensityFactorTimes1024;
					float fPresetGrainFactor = 0.f;
					if (grainStrength > 0 && !useGrainPresets)
					{
						unsigned int iRand = iran266(&lseed);
						if (iRand < RND_THR1)
						{
							if (iRand < RND_THR2)
							{
								netFactor = ulGrainStrengthDensityTimes1024;
							}
							else
							{
								netFactor = ulOneOverGrainStrengthDensityTimes1024;
							}
						}
					}

					// => individual pixel
					MTI_UINT16 *mdf = mdfrow + x;
					MTI_UINT16 *src1 = srcrow1 + xoff;
					MTI_UINT16 *src2 = srcrow2 + xoff;
					MTI_UINT16 *dst = dstrow + xoff;

					if (usingColor)
					{
						src1 = colorComponents;
					}

					// get brush weight at (x, y)
					MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

					// get value of Mask entry for this pixel
					MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

					MTI_UINT32 wgt1, wgt2;

					if (wgt && msk)
					{
						// inside brush envelope
						// scale wgt1 by Mask
						wgt1 = (wgt * (MTI_UINT32)msk) / 255;

						// if using THRESHOLDS determine if pel is to be modified
						if (usingThresholds)
						{

							// if   intensity(trgpel) - intensity(imppel) is pos & < lighterThreshold
							// orif intensity(trgpel) - intensity(imppel) is neg & > darkerThreshold
							// pel is not modified
							//
							unsigned int imppelint = (unsigned int)src1[0];

							unsigned int trgpelint = (unsigned int)src2[0];

							// target intensity rel to import intensity
							int deltint = ((int)trgpelint - (int)imppelint);
							if (deltint >= 0)
							{
								if (deltint < lighterThreshold)
								{
									wgt1 = 0;
								}
							}
							else
							{
								if (deltint > darkerThreshold)
								{
									wgt1 = 0;
								}
							}
						}

						// Refer to the paint modify buffer. Update the pixel's
						// weight with the greater of the current value and wgt1.
						// Use the value assigned to the pixel
						//
						if (wgt1 > (MTI_UINT32)(*mdf))
						{
							*mdf = wgt1;
						}
						else
						{
							wgt1 = *mdf;
						}

						wgt2 = UNITY - wgt1;

						// TODO: WTF is this, JAM commented it out
						// (MTI_UINT16)(((((MTI_UINT32)src1[0]*wgt1)>>10)*netFactor +
						// (MTI_UINT32)src2[0]*wgt2)>>15);

					} // inside brush envelope
				} // brush cols
			} // brush rows
		} // end luminance
	}
	else if (brushMode == BRUSH_MODE_ERASE)
	{
		for (int y = brrect.top; y <= brrect.bottom; y++)
		{
			if (!brushEnabled)
			{
				break;
			}

			int mdfyoff = y * frameWidth;

			MTI_UINT16 *mdfrow = paintMdfdBuf + mdfyoff;
			//
			// the import frame now has pitch = frameWidth
			//
			int yoff = mdfyoff * frameComponentCount;

			MTI_UINT16 *srcrow1 = targetOrgIntBuf + yoff;
			MTI_UINT16 *srcrow2 = targetPreIntBuf + yoff;
			MTI_UINT16 *dstrow = targetIntBuf + yoff;

			int x = brrect.left;
			int xoff = brrect.left * frameComponentCount;

			for (; x <= brrect.right; x++, xoff += frameComponentCount)
			{
				if (!brushEnabled)
				{
					break;
				}

				MTI_UINT16 *mdf = mdfrow + x;
				MTI_UINT16 *src1 = srcrow1 + xoff;
				MTI_UINT16 *src2 = srcrow2 + xoff;
				MTI_UINT16 *dst = dstrow + xoff;

				// get brush weight at (x, y)
				MTI_UINT32 wgt = paintBrush->getWeightFast((y - yp), (x - xp));

				// get value of Mask entry for this pixel
				MTI_UINT8 msk = *(paintMaskBuf + mdfyoff + x);

				MTI_UINT32 wgt1, wgt2;

				if (wgt && msk)
				{ // inside brush envelope & mask

					// erase is gated but not scaled by mask
					// wgt1 = (wgt * (MTI_UINT32)msk) / 255;
					wgt1 = wgt;

					// Refer to the paint modify buffer. Update the pixel's weight
					// with the greater of the current value and wgt1.
					// Use the value assigned to the pixel
					if (wgt1 > (MTI_UINT32)(*mdf))
					{
						*mdf = wgt1;
					}
					else
					{
						wgt1 = *mdf;
					}

					wgt2 = UNITY - wgt1;

					bool isHalf = brushComponents == BRUSH_COMPONENTS_RGB_HALF;
					for (int k = 0; k < frameComponentCount; k++)
					{
						float fDst = (ImageDatum::toFloat(src1[k], isHalf) * (wgt1 / (1.0F * UNITY))) +
								+(ImageDatum::toFloat(src2[k], isHalf) * (wgt2 / (1.0F * UNITY)));
						dst[k] = ImageDatum::fromFloat(fDst, isHalf);
					}
				}
			}
		}
	}
}
//------------------------------------------------------------------------------

void CDisplayer::paintOneStrokeSegmentFrame(int xbeg, int ybeg, int xend, int yend)
{
	double xrun = xbeg;
	double yrun = ybeg;

	double delx = xend - xrun;
	double dely = yend - yrun;

	if ((delx != 0) || (dely != 0))
	{
		double length = sqrt(delx * delx + dely * dely);

		// the "5" can be turned into a fineness parameter - the
		// higher it's set, the smaller the "caterpillar" effect
		double size = (double)paintBrush->getRadius() * (double)paintBrush->getAspect() / (5 * 100);

		int pts = length / size;

		pts++;

		delx = delx / (double) pts;
		dely = dely / (double) pts;

		for (int i = 0; i < pts; i++)
		{
			if (!brushEnabled)
			{
				break;
			}

			xrun += delx;
			yrun += dely;

			paintOneBrushPositionFrame((int)(xrun + 0.5), (int)(yrun + 0.5));
		}
	}
}
//------------------------------------------------------------------------------

#ifdef INPAINTING

int CDisplayer::initMaskForInpainting()
{
	begRowMask = 0x7fffffff;
	endRowMask = -0x7fffffff;
	begColMask = 0x7fffffff;
	endColMask = -0x7fffffff;
	CMaskTool *theMaskTool = CNavigatorTool::GetMaskTool();
	if (theMaskTool->IsMaskAvailable())
	{
		const MTI_UINT8 *inpaintMask = theMaskTool->GetRegionOfInterest()->getBlendPtr();
		for (int i = 0; i < frameHeight; i++)
		{
			MTI_UINT8 *row = (MTI_UINT8*)inpaintMask + i * frameWidth;
			for (int j = 0; j < frameWidth; j++)
			{
				if (row[j] != 0)
				{
					if (i < begRowMask)
					{
						begRowMask = i;
					}
					else if (i > endRowMask)
					{
						endRowMask = i;
					}

					if (j < begColMask)
					{
						begColMask = j;
					}
					else if (j > endColMask)
					{
						endColMask = j;
					}
				}
			}
		}

		MTImemset(targetEraseIntBuf, 0, frameInternalSize); // init lo-order

		inpaintDelta = (MTI_INT32*)importPreIntBuf;
		MTImemset(inpaintDelta, 0, frameInternalSize*2);
      CheckFencepostOverrun(importPreIntBuf);

		if ((begRowMask <= endRowMask) && (begColMask <= endColMask))
		{
			return 0;
		}
	}

	return -1;
}
//------------------------------------------------------------------------------

#define FIRST_ORDER

int CDisplayer::iterateOneInpaintCycle()
{
	int pitch = frameWidth * frameComponentCount;
	MTI_UINT16 delRed, delGrn, delBlu;
	MTI_UINT8 *msk;
	MTI_UINT16 *dsthi, *dstlo, *mdf;
	MTI_INT32 *dstdel;

	CMaskTool *theMaskTool = CNavigatorTool::GetMaskTool();
	if (!theMaskTool->IsMaskAvailable())
	{
		return -1;
	}

	const MTI_UINT8 *inpaintMask = theMaskTool->GetRegionOfInterest()->getBlendPtr();
	if (inpaintMask == 0)
	{
		return -1;
	}

	// calculate the deltas
	for (int i = begRowMask; i <= endRowMask; i++)
	{
		msk = (MTI_UINT8*)inpaintMask + i * frameWidth;

		dsthi = targetIntBuf + i * pitch;
		dstlo = targetEraseIntBuf + i * pitch;
		dstdel = inpaintDelta + i * pitch;

		int k;
		for (int j = begColMask; j <= endColMask; j++)
		{
			if (msk[j] != 0)
			{
#ifdef FIRST_ORDER
				if (frameComponentCount == 3)
				{
					for (k = 3 * j; k < 3 * (j + 1); k++)
					{

						dstdel[k] = ((dsthi[k - pitch] << 16) + dstlo[k - pitch] +

								(dsthi[k - 3] << 16) + dstlo[k - 3] + (dsthi[k + 3] << 16) + dstlo[k + 3] +

								(dsthi[k + pitch] << 16) + dstlo[k + pitch] -

								4 * ((dsthi[k] << 16) + dstlo[k])) >> 3;
					}
				}
				else if (frameComponentCount == 1)
				{
					for (k = j; k < (j + 1); k++)
					{

						dstdel[k] = ((dsthi[k - pitch] << 16) + dstlo[k - pitch] +

								(dsthi[k - 3] << 16) + dstlo[k - 3] + (dsthi[k + 3] << 16) + dstlo[k + 3] +

								(dsthi[k + pitch] << 16) + dstlo[k + pitch] -

								4 * ((dsthi[k] << 16) + dstlo[k])) >> 3;
					}
				}
#endif

#ifdef SECOND_ORDER
				if (frameComponentCount == 3)
				{
					for (k = 3 * j; k < 3 * (j + 1); k++)
					{
						dstdel[k] = (-1 * ((dsthi[k - 2 * pitch] << 16) + dstlo[k - 2 * pitch]) +

								-2 * ((dsthi[k - pitch - 3] << 16) + dstlo[k - pitch - 3]) +
								8 * ((dsthi[k - pitch] << 16) + dstlo[k - pitch]) +
								-2 * ((dsthi[k - pitch + 3] << 16) + dstlo[k - pitch + 3]) +

								-1 * ((dsthi[k - 6] << 16) + dstlo[k - 6]) + 8 * ((dsthi[k - 3] << 16) + dstlo[k - 3]) +
								12 * ((dsthi[k] << 16) + dstlo[k]) + 8 * ((dsthi[k + 3] << 16) + dstlo[k + 3]) +
								-1 * ((dsthi[k + 6] << 16) + dstlo[k + 6]) +

								-2 * ((dsthi[k + pitch - 3] << 16) + dstlo[k + pitch - 3]) +
								8 * ((dsthi[k + pitch] << 16) + dstlo[k + pitch]) +
								-2 * ((dsthi[k + pitch + 3] << 16) + dstlo[k + pitch + 3]) +

								-1 * ((dsthi[k + 2 * pitch] << 16) + dstlo[k + 2 * pitch]) -

								32 * ((dsthi[k] << 16) + dstlo[k])

								) >> 6;
					}
				}
				else if (frameComponentCount == 1)
				{
					for (k = j; k < (j + 1); k++)
					{
						dstdel[k] = (-1 * ((dsthi[k - 2 * pitch] << 16) + dstlo[k - 2 * pitch]) +

								-2 * ((dsthi[k - pitch - 1] << 16) + dstlo[k - pitch - 1]) +
								8 * ((dsthi[k - pitch] << 16) + dstlo[k - pitch]) +
								-2 * ((dsthi[k - pitch + 1] << 16) + dstlo[k - pitch + 1]) +

								-1 * ((dsthi[k - 2] << 16) + dstlo[k - 2]) + 8 * ((dsthi[k - 1] << 16) + dstlo[k - 1]) +
								12 * ((dsthi[k] << 16) + dstlo[k]) + 8 * ((dsthi[k + 1] << 16) + dstlo[k + 1]) +
								-1 * ((dsthi[k + 2] << 16) + dstlo[k + 2]) +

								-2 * ((dsthi[k + pitch - 1] << 16) + dstlo[k + pitch - 1]) +
								8 * ((dsthi[k + pitch] << 16) + dstlo[k + pitch]) +
								-2 * ((dsthi[k + pitch + 1] << 16) + dstlo[k + pitch + 1]) +

								-1 * ((dsthi[k + 2 * pitch] << 16) + dstlo[k + 2 * pitch]) -

								32 * ((dsthi[k] << 16) + dstlo[k])

								) >> 6;
					}
				}
#endif

			}
		}
	}

	// apply the deltas
	for (int i = begRowMask; i <= endRowMask; i++)
	{
		msk = (MTI_UINT8*)inpaintMask + i * frameWidth;

		dsthi = targetIntBuf + i * pitch;
		dstlo = targetEraseIntBuf + i * pitch;
		dstdel = inpaintDelta + i * pitch;

		mdf = paintMdfdBuf + i * frameWidth;

		int temp;
		for (int j = begColMask; j <= endColMask; j++)
		{
			if (msk[j] != 0)
			{
				temp = (dsthi[3 * j] << 16) + dstlo[3 * j] + dstdel[3 * j];
				dsthi[3 * j] = temp >> 16;
				dstlo[3 * j] = temp;

				temp = (dsthi[3 * j + 1] << 16) + dstlo[3 * j + 1] + dstdel[3 * j + 1];
				dsthi[3 * j + 1] = temp >> 16;
				dstlo[3 * j + 1] = temp;

				temp = (dsthi[3 * j + 2] << 16) + dstlo[3 * j + 2] + dstdel[3 * j + 2];
				dsthi[3 * j + 2] = temp >> 16;
				dstlo[3 * j + 2] = temp;

				mdf[j] = 0x8000;
			}
		}
	}
      CheckFencepostOverrun(importPreIntBuf);

	return 0;
}
#endif
//------------------------------------------------------------------------------

void CDisplayer::setImportRotationFast(double angle, double strx, double stry, double skew, double offx, double offy)
{
	MTIassert(strx != 0.0);
	MTIassert(stry != 0.0);

	if ((importPreIntBuf == nullptr) || (imgFmt == nullptr))
	{
		return;
	}

	double cosine, sine;
	double offXctr, offYctr;
	double importOffsetX, importOffsetY;

	if ((angle == impRot) && (strx == impStrX) && (stry == impStrY) && (skew == impSkew))
	{
		// only changing offsets
		setImportOffsets(offx, offy);
		return;
	}

	if ((dspXctr != dspXctrWhenLastRotated) || (dspYctr != dspYctrWhenLastRotated) || !rotatedSinceLastTranslation)
	{
		// display window has changed, or the import frame
		// has not been rotated since its last translation

		importOffsetX = offx;
		importOffsetY = offy;

		offXctr = (double)(dspXctr - importOffsetX - frmWdth / 2);
		offYctr = (double)(dspYctr - importOffsetY - frmHght / 2);

		// these are the coords of the rotation pt rel to
		// the center of the import frame

		trgXctr = (cos(impRot) * offXctr - sin(impRot) * offYctr) / impStrX -
				(sin(impRot) * offXctr + cos(impRot) * offYctr) * impSkew / impStrY;

		// mbraca thinks this should be /impStrY, not /stry !!!
		trgYctr = (sin(impRot) * offXctr + cos(impRot) * offYctr) / impStrY; // /stry;

		dspXctrWhenLastRotated = dspXctr;
		dspYctrWhenLastRotated = dspYctr;
		rotatedSinceLastTranslation = true;
	}

	// the window has not changed, and we already have the coords
	// of the rotation pt rel to the center of the import frame

	double intX = (trgXctr + trgYctr * skew) * strx;
	double intY = trgYctr * stry;

	cosine = cos(angle);
	sine = sin(angle);

	// new coords from center of import frame
	offXctr = intX * cosine + intY * sine;
	offYctr = -intX * sine + intY * cosine;

	// now we derive a new onionskin offset
	importOffsetX = ((double)dspXctr - offXctr - (double)frmWdth / 2);
	importOffsetY = ((double)dspYctr - offYctr - (double)frmHght / 2);

	impOff5X = (double)((int)(importOffsetX * 5)) / 5.;
	impOff5Y = (double)((int)(importOffsetY * 5)) / 5.;

	rotateFast(importIntBuf, importPreIntBuf, imgFmt, angle, strx, stry, skew, impOff5X, impOff5Y);

	// the overlay is now rotated around the ctr
	// point of the display window
	impRot = angle;
	impStrX = strx;
	impStrY = stry;
	impSkew = skew;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportRotationExact(double angle, double strx, double stry, double skew, double offx, double offy)
{
	MTIassert(strx != 0.0);
	MTIassert(stry != 0.0);

	if ((importPreIntBuf == nullptr) || (imgFmt == nullptr))
	{
		return;
	}

	double cosine, sine;
	double offXctr, offYctr;
	double importOffsetX, importOffsetY;

	if ((angle == impRot) && (strx == impStrX) && (stry == impStrY) && (skew == impSkew))
	{
		// only changing offsets
		setImportOffsets(offx, offy);
		return;
	}

	// only changing rotation, stretch, and/or skew

	if ((dspXctr != dspXctrWhenLastRotated) || (dspYctr != dspYctrWhenLastRotated) || !rotatedSinceLastTranslation)
	{
		// display window has changed, or the import frame
		// has not been rotated since its last translation

		importOffsetX = impOff5X;
		importOffsetY = impOff5Y;

		offXctr = (double)(dspXctr - importOffsetX - frmWdth / 2);
		offYctr = (double)(dspYctr - importOffsetY - frmHght / 2);

		// these are the coords of the rotation pt rel to
		// the center of the import frame

		trgXctr = (cos(impRot) * offXctr - sin(impRot) * offYctr) / impStrX -
				(sin(impRot) * offXctr + cos(impRot) * offYctr) * impSkew / impStrY;

		// mbraca thinks this should be /impStrY, not /stry !!!
		trgYctr = (sin(impRot) * offXctr + cos(impRot) * offYctr) / impStrY; // /stry;

		dspXctrWhenLastRotated = dspXctr;
		dspYctrWhenLastRotated = dspYctr;
		rotatedSinceLastTranslation = true;
	}

	// the window has not changed, and we already have the coords
	// of the rotation pt rel to the center of the import frame

	double intX = (trgXctr + trgYctr * skew) * strx;
	double intY = trgYctr * stry;

	cosine = cos(angle);
	sine = sin(angle);

	// new coords from center of import frame
	offXctr = intX * cosine + intY * sine;
	offYctr = -intX * sine + intY * cosine;

	// now we derive a new onionskin offset
	importOffsetX = ((double)dspXctr - offXctr - (double)frmWdth / 2);
	importOffsetY = ((double)dspYctr - offYctr - (double)frmHght / 2);

	impOff5X = (double)((int)(importOffsetX * 5)) / 5.;
	impOff5Y = (double)((int)(importOffsetY * 5)) / 5.;

	rotateExact(importIntBuf, importPreIntBuf, imgFmt, angle, strx, stry, skew, impOff5X, impOff5Y);

	// the overlay is now rotated around the ctr
	// point of the display window
	impRot = angle;
	impStrX = strx;
	impStrY = stry;
	impSkew = skew;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportOffsets(double offsx, double offsy)
{
	impOff5X = (double)((int)(offsx * 5.)) / 5.;
	impOff5Y = (double)((int)(offsy * 5.)) / 5.;

	if ((importIntBuf != nullptr) && (importPreIntBuf != nullptr) && (imgFmt != nullptr))
	{
		rotateExact(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
	}

	rotatedSinceLastTranslation = false;
}
//------------------------------------------------------------------------------

void CDisplayer::setImportOffsetsFast(int offsx, int offsy)
{
	impOff5X = (double)((int)(offsx * 5.)) / 5.;
	impOff5Y = (double)((int)(offsy * 5.)) / 5.;

	if ((importIntBuf != nullptr) && (importPreIntBuf != nullptr) && (imgFmt != nullptr))
	{
		rotateFast(importIntBuf, importPreIntBuf, imgFmt, impRot, impStrX, impStrY, impSkew, impOff5X, impOff5Y);
	}

	rotatedSinceLastTranslation = false;
}
//------------------------------------------------------------------------------

void CDisplayer::getImportOffsets(double *offsx, double *offsy)
{
	*offsx = impOff5X;
	*offsy = impOff5Y;
}
//------------------------------------------------------------------------------

void CDisplayer::initTransformEditor(CTransformEditor *transformeditor, RECT& rgbrct)
{
	transformEditor = transformeditor;

	// the current image rectangle (client coords)
	rgbrct = rgbRect;
}
//------------------------------------------------------------------------------

bool CDisplayer::isOnTargetFrame()
{
	return (lastFrame == targetFrame);
}
//------------------------------------------------------------------------------

// rotateExact was hijacked for tracking alignment purposes

void CDisplayer::rotateExact(MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt, double ang, double strx,
		double stry, double skw, double xoff, double yoff)
{
	MTIassert(strx != 0.0);
	MTIassert(stry != 0.0);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);

	// awful hack - if we're in Tracking Alignment mode, completely ignore
	// the requested transform and substitute one that does only the tracked
	// displacement for this target/import frame pair -- no rotation, scaling
	// or skewing

	if (useTrackingAlignOffsets)
	{
		// getTransform has been hacked to return the buggered transform
		getTransform(&ang, &strx, &stry, &skw, &xoff, &yoff);

		// This is NASTY, but I'm having some trouble!! Stay synched with GUI
		transformEditor->transmitTransform(ang, strx, stry, skw, xoff, yoff);
	}

	// Check for no actual transformation needed.
	if (ang == 0.0 && strx == 1.0 && stry == 1.0 && skw == 0.0 && xoff == 0.0 && yoff == 0.0)
	{
		MTImemcpy(dst, src, frameInternalSize);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);
		return;
	}

	// pass it on
	rotator->rotateExact(dst, src, srcfmt, ang, strx, stry, skw, xoff, yoff);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);
}

// rotateFast was hijacked for tracking alignment purposes

void CDisplayer::rotateFast(MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt, double ang, double strx,
		double stry, double skw, double xoff, double yoff)
{
	MTIassert(strx != 0.0);
	MTIassert(stry != 0.0);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);

	// awful hack - if we're in Tracking Alignment mode, completely ignore
	// the requested transform and substitute one that does only the tracked
	// displacement for this target/import frame pair -- no rotation, scaling
	// or skewing

	if (useTrackingAlignOffsets)
	{
		// getTransform has been hacked to return the buggered transform
		getTransform(&ang, &strx, &stry, &skw, &xoff, &yoff);

		// This is NASTY, but I'm having some trouble!! Stay synched with GUI
		transformEditor->transmitTransform(ang, strx, stry, skw, xoff, yoff);
	}

	// Check for no actual transformation needed.
	if (ang == 0.0 && strx == 1.0 && stry == 1.0 && skw == 0.0 && xoff == 0.0 && yoff == 0.0)
	{
		MTImemcpy(dst, src, frameInternalSize);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);
		return;
	}

	// pass it on
	rotator->rotateFast(dst, src, srcfmt, ang, strx, stry, skw, xoff, yoff);
   CheckFencepostOverrun(dst);
   CheckFencepostOverrun(src);
}
//------------------------------------------------------------------------------

void CDisplayer::setUseTrackingAlignOffsets(bool flag)
{
	useTrackingAlignOffsets = flag;
}
//------------------------------------------------------------------------------

void CDisplayer::clearAllTrackingAlignOffsets()
{
	trackingAlignOffsetsMap.clear();
}
//------------------------------------------------------------------------------

void CDisplayer::setTrackingAlignOffsets(int frame, double xoff, double yoff)
{
	// don't need so much precision!!
	std::pair<float, float>offs((float) xoff, (float) yoff);
	trackingAlignOffsetsMap[frame] = offs;
}
//------------------------------------------------------------------------------

//////////////////////////////////////////////////////////
// This is a hack to draw Grain Tool brushz
//////////////////////////////////////////////////////////

void CDisplayer::drawShapeOnFrameNoFlush(double centerX, double centerY, int shapeType, double radius,
		double aspectRatio, double angle, MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B)
{
	centerX += 0.5;
	centerY += 0.5;

	// Haha WTF is this for??
	if (zoomFactor >= 8)
	{
		centerX = (int)(centerX) + 0.5;
		centerY = (int)(centerY) + 0.5;
	}

	// clip the brush to the IMAGE rectangle
	clipToImageRectangle();

	if (radius != shapeHackRadius || aspectRatio != shapeHackAspectRatio || angle != shapeHackAngle ||
			shapeType != shapeHackShapeType)
	{
		shapeHackRadius = radius;
		shapeHackAspectRatio = aspectRatio;
		shapeHackAngle = angle;
		shapeHackShapeType = shapeType;

		// note that radius 0 generates a box 1x1
		double dRadius = radius + 0.5;
		double dAspect = aspectRatio / 100.;
		double dAngle = angle;

		// adjust the number of chords to make a smooth ellipse
		double SinAngle = sin(3.14159 * dAngle / 180.);
		double CosAngle = cos(3.14159 * dAngle / 180.);

		double xb, yb, xr, yr;

		if (shapeType == 1)
		{
			// rectangle
			shapeHackNumChords = 4;

			xb = -dRadius * dAspect;
			yb = -dRadius;
			shapeHackVertices[0] = (xb * CosAngle) + (yb * SinAngle);
			shapeHackVertices[1] = (-xb * SinAngle) + (yb * CosAngle);

			xb = dRadius * dAspect;
			yb = -dRadius;
			shapeHackVertices[2] = (xb * CosAngle) + (yb * SinAngle);
			shapeHackVertices[3] = (-xb * SinAngle) + (yb * CosAngle);

			xb = dRadius * dAspect;
			yb = dRadius;
			shapeHackVertices[4] = (xb * CosAngle) + (yb * SinAngle);
			shapeHackVertices[5] = (-xb * SinAngle) + (yb * CosAngle);

			xb = -dRadius * dAspect;
			yb = dRadius;
			shapeHackVertices[6] = (xb * CosAngle) + (yb * SinAngle);
			shapeHackVertices[7] = (-xb * SinAngle) + (yb * CosAngle);
		}
		else
		{
			// ellipse
			double r = dRadius;
			if (r <= 0.0)
			{
				r = 1.0;
			}

			double chordAngle = 3.14159 / (4 * sqrt(r));
			shapeHackNumChords = 2 * 3.14159 / chordAngle;
			if (shapeHackNumChords > MAX_SHAPE_HACK_VERTICES)
			{
				shapeHackNumChords = MAX_SHAPE_HACK_VERTICES;
			}

			chordAngle = 2 * 3.14159 / (double)shapeHackNumChords;

			for (int i = 0; i < shapeHackNumChords; ++i)
			{
				// generate the point
				xb = dRadius * dAspect * cos(i * chordAngle);
				yb = dRadius * sin(i * chordAngle);

				// the ellipse is rotated
				shapeHackVertices[2 * i] = (xb * CosAngle) + (yb * SinAngle);
				shapeHackVertices[(2 * i) + 1] = (-xb * SinAngle) + (yb * CosAngle);
			}
		}
	}

	// Not really supposed to set an arbitrary color, so I hacked this
	int oldColor = hackTheColor(R, G, B);

	// Draw the shape using the frame entry points
	moveToFrame(centerX + shapeHackVertices[0], centerY + shapeHackVertices[1]);

	for (int i = 1; i < shapeHackNumChords; ++i)
	{
		lineToFrameNoFlush(centerX + shapeHackVertices[2 * i], centerY + shapeHackVertices[(2 * i) + 1]);
	}

	lineToFrameNoFlush(centerX + shapeHackVertices[0], centerY + shapeHackVertices[1]);

	// Unhack the color
	setGraphicsColor(oldColor);

	// revert to clipping to the CLIENT rectangle
	clipToVisibleRectangle();
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

void CDisplayer::enterCursorOverrideMode(int CursorIndex)
{
	cursorOverrideIndex = CursorIndex;
}
//------------------------------------------------------------------------------

void CDisplayer::exitCursorOverrideMode()
{
	cursorOverrideIndex = 0;
}
//------------------------------------------------------------------------------

void CDisplayer::screenToClientCoords(int inX, int inY, int &outX, int &outY)
{
	outX = inX - imgRect.left;
	outY = inY - imgRect.top;
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

void CDisplayer::getLastFrameAsIntermediate(MTI_UINT16 *intbuf)
{
	if (!_lastDisplayedFrameBuffer)
	{
		return;
	}

	MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};

	// convert native to INT
   CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
	extr->extractActivePicture(intbuf, stupidBuf, imgFmt, &frameImageFormat);
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

// HACK for loupe - expose internal buffer!!!!!!!!!
MediaFrameBufferSharedPtr CDisplayer::getLastDisplayedFrameBuffer()
{
	return _lastDisplayedFrameBuffer;
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

#ifdef FRAMEWRX

float CDisplayer::getTrueZoomLevel()
{
	// WAS: return (float) truZoomLevel;
	return (float)rgbRectHeight / (float)dspHght;
}
#endif // FRAMEWRX
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

void CDisplayer::SetAutoCleanOverlayList(const OverlayDisplayList &newList)
{
	overlayDisplayList = newList;
}

void CDisplayer::MakeSureThisRectIsVisible(const RECT &rect)
{
	// This tries to make sure the autoclean magnifier is visible
	if (rect.left < dspRect.left || rect.right > dspRect.right || rect.top < dspRect.top || rect.bottom > dspRect.bottom)
	{
		int xc = (rect.left + rect.right) / 2;
		int yc = (rect.top + rect.bottom) / 2;

		calculateImageRectangle();
		calculateDisplayRectangle(xc, yc);
	}
}
//------------------------------------------------------------------------------

void CDisplayer::GetDisplayLutValues(MTI_UINT8 *outputValues)
{
	const int nLutVals = 3 * 256; // one set each for R, G, B

	if (currentLUTPtr != nullptr)
	{
		MTImemcpy((void *)outputValues, (void *)currentLUTPtr, nLutVals);
	}
	else
	{
		// Generate neutral LUT
		for (int i = 0; i < nLutVals; ++i)
		{
			outputValues[i] = i & 255;
		}
	}
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////

#define TSTW 720
#define TSTH 576

void CDisplayer::initTestFrame()
{
#ifdef TESTYUV8
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngYUV8;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 2)*4, true);
#endif

#ifdef TESTYUV10
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_4_10Bits_IN_5Bytes);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngYUV10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 2)*5, true);
#endif

#ifdef TESTDVS10
	tstFmt = new CImageFormat;
	tstFmt->setImageFormatType(IF_TYPE_525_5994);
	tstFmt->setColorSpace(IF_COLOR_SPACE_CCIR_709);
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngDVS10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 6)*16, true);
#endif

#ifdef TESTDVS10a
	tstFmt = new CImageFormat;
	tstFmt->setImageFormatType(IF_TYPE_525_5994);
	tstFmt->setColorSpace(IF_COLOR_SPACE_CCIR_709);
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngDVS10a;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 6)*16, true);
#endif

#ifdef TESTDVS10BE
	tstFmt = new CImageFormat;
	tstFmt->setImageFormatType(IF_TYPE_525_5994);
	tstFmt->setColorSpace(IF_COLOR_SPACE_CCIR_709);
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV422);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngDVS10BE;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 6)*16, true);
#endif

#ifdef TESTUYV8
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV444);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngUYV8;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*3, true);
#endif

#ifdef TESTUYV10
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_YUV444);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngUYV10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*4, true);
#endif

#ifdef TESTRGB8
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB8;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*3, false);
#endif

#ifdef TESTRGB8LOG
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LOG);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB8;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*3, false);
#endif

#ifdef TESTBGR8
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_BGR);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_8Bits_IN_1Byte);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngBGR8;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*3, false);
#endif

#ifdef TESTRGB10
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*4, true);
#endif

#ifdef TESTRGB10LOG
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LOG);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*4, true);
#endif

#ifdef TESTRGB12
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_2_12Bits_IN_3Bytes);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, ((TSTW*36 + 31) / 32)*4, true);
#endif

#ifdef TESTRGB12P
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_2_12Bits_IN_3Bytes);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12P;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, ((TSTW + 7) / 8)*36, true);
#endif

#ifdef TESTRGB12
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_2_12Bits_IN_3Bytes);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW*9) / 2, false);
#endif

#ifdef TESTRGB12LOG
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_2_12Bits_IN_3Bytes);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LOG);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW*9) / 2, false);
#endif

#ifdef TESTRGB12LLE
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12LLE;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*6, false);
#endif

#ifdef TESTRGB12LBE
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE);
	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
	tstFmt->setInterlaced(false);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB12LBE;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*6, false);
#endif

#ifdef TESTRGB16BE
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB16BE;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*6, true);
#endif

#ifdef TESTRGB16LE
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGB16LE;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, TSTW*6, true);
#endif

#ifdef TESTRGBA10
	tstFmt = new CImageFormat;
	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGBA);
	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L);
	tstFmt->setInterlaced(true);
	tstFmt->setPixelsPerLine(TSTW);
	tstFmt->setLinesPerFrame(TSTH);
	tstFmt->setPixelAspectRatio(1.0);
	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);

	tstEng = new CLineEngRGBA10;
	tstEng->setFrameBufferDimensions(TSTW, TSTH, (TSTW / 3)*16, true);
#endif

//#ifdef TESTMON10
//	tstFmt = new CImageFormat;
//	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_LUMINANCE);
//	tstFmt->setPixelPacking(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R);
//	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
//	tstFmt->setInterlaced(true);
//	tstFmt->setPixelsPerLine(TSTW);
//	tstFmt->setLinesPerFrame(TSTH);
//	tstFmt->setPixelAspectRatio(1.0);
//	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);
//
//	tstEng = new CLineEngMON10;
//	tstEng->setFrameBufferDimensions(TSTW, TSTH, ((TSTW / 3) << 2), true);
//#endif

//#ifdef TESTMON10P
//	tstFmt = new CImageFormat;
//	tstFmt->setPixelComponents(IF_PIXEL_COMPONENTS_LUMINANCE);
//	tstFmt->setPixelPacking(IF_PIXEL_PACKING_4_10Bits_IN_5Bytes);
//	tstFmt->setColorSpace(IF_COLOR_SPACE_LINEAR);
//	tstFmt->setInterlaced(true);
//	tstFmt->setPixelsPerLine(TSTW);
//	tstFmt->setLinesPerFrame(TSTH);
//	tstFmt->setPixelAspectRatio(1.0);
//	tstFmt->setFrameAspectRatio((double)TSTW / (double)TSTH);
//
//	tstEng = new CLineEngMON10P;
//	tstEng->setFrameBufferDimensions(TSTW, TSTH, ((TSTW / 4)*5), true);
//#endif

	tstBuf[0] = (unsigned char *)MTImalloc(TSTW * TSTH * 6);
	tstBuf[1] = (unsigned char *)MTImalloc(TSTW * TSTH * 6);
	tstEng->setExternalFrameBuffer(tstBuf);

#ifdef OLD_PATTERN
	tstEng->setFGColor(0);
	tstEng->drawRectangle(0, 0, TSTW - 1, TSTH - 1);

#if 1
	tstEng->setFGColor(1);
	tstEng->setBGColor(0);

	for (int i = 0; i < 360; i += 8)
	{
		tstEng->moveTo(0, 0);
		tstEng->lineTo(i, TSTH - 1);
	}

	tstEng->setFontSize(3);
	tstEng->drawString(360, 273, "0123456789");

	tstEng->setFGColor(0xffff, 0xffff, 0xffff);
	tstEng->drawRectangle(360, 0, 404, 260);
	tstEng->setFGColor(0xffff, 0xffff, 0x0000);
	tstEng->drawRectangle(405, 0, 449, 260);
	tstEng->setFGColor(0xffff, 0x0000, 0xffff);
	tstEng->drawRectangle(450, 0, 494, 260);
	tstEng->setFGColor(0xffff, 0x0000, 0x0000);
	tstEng->drawRectangle(495, 0, 539, 260);
	tstEng->setFGColor(0x0000, 0xffff, 0xffff);
	tstEng->drawRectangle(540, 0, 584, 260);
	tstEng->setFGColor(0x0000, 0xffff, 0x0000);
	tstEng->drawRectangle(585, 0, 629, 260);
	tstEng->setFGColor(0x0000, 0x0000, 0xffff);
	tstEng->drawRectangle(630, 0, 674, 260);
	tstEng->setFGColor(0x0000, 0x0000, 0x0000);
	tstEng->drawRectangle(675, 0, TSTW - 1, 260);
#endif

	tstEng->setFGColor(0xffff, 0xffff, 0xffff);

#if 0
	for (int i = 0; i < TSTH; i += 4)
	{
		tstEng->moveTo(TSTW / 2, TSTH / 2);
		tstEng->lineTo(0, TSTH - 1 - i);
		tstEng->moveTo(TSTW / 2, TSTH / 2);
		tstEng->lineTo(TSTW - 1, i);
	}
	for (int i = 0; i < TSTW; i += 4)
	{
		tstEng->moveTo(TSTW / 2, TSTH / 2);
		tstEng->lineTo(i, 0);
		tstEng->moveTo(TSTW / 2, TSTH / 2);
		tstEng->lineTo(TSTW - 1 - i, TSTH - 1);
	}
#endif

	tstEng->moveTo(0, 0);
	tstEng->lineTo(TSTW - 1, 0);
	tstEng->lineTo(TSTW - 1, TSTH - 1);
	tstEng->lineTo(0, TSTH - 1);
	tstEng->lineTo(0, 0);
#endif

#ifdef NEW_PATTERN
	tstEng->setFGColor(0);
	tstEng->drawRectangle(0, 0, TSTW - 1, TSTH - 1);

	tstEng->setFGColor(1);
	tstEng->setBGColor(0);

	for (int i = 0; i < 360; i += 8)
	{
		tstEng->moveTo(0, 0);
		tstEng->lineTo(i, TSTH - 1);
	}

	tstEng->setFontSize(2);
	tstEng->drawString(208, 273, "INTENSITY RANGE 0-1023");

	for (int i = 0; i < 512; i++)
	{
		tstEng->setFGColor((i << 7), (i << 7), (i << 7));
		tstEng->moveTo(208 + i, 0);
		tstEng->lineTo(208 + i, 272);
	}

	tstEng->setFGColor(0xffff, 0xffff, 0xffff);
	tstEng->moveTo(0, 0);
	tstEng->lineTo(TSTW - 1, 0);
	tstEng->lineTo(TSTW - 1, TSTH - 1);
	tstEng->lineTo(0, TSTH - 1);
	tstEng->lineTo(0, 0);
#endif

#ifdef SCR_PATTERN
	// make a vertical scratch
	tstEng->setFGColor(32768 + 4096*3, 32768 + 4096*3, 32768 + 4096*3);
	tstEng->drawRectangle(0, 0, TSTW - 1, TSTH - 1);

	for (int i = -3; i < 0; i++)
	{
		tstEng->setFGColor(32678 - 4096*i, 32768 - 4096*i, 32768 - 4096*i);
		tstEng->moveTo(TSTW / 2 + i, 0);
		tstEng->lineTo(TSTW / 2 + i, TSTH - 1);
		tstEng->moveTo(0, TSTH / 2 + i);
		tstEng->lineTo(TSTW - 1, TSTH / 2 + i);
	}
	for (int i = 0; i <= +3; i++)
	{
		tstEng->setFGColor(32678 + 4096*i, 32768 + 4096*i, 32768 + 4096*i);
		tstEng->moveTo(TSTW / 2 + i, 0);
		tstEng->lineTo(TSTW / 2 + i, TSTH - 1);
		tstEng->moveTo(0, TSTH / 2 + i);
		tstEng->lineTo(TSTW - 1, TSTH / 2 + i);
	}
#endif

#if 0
	extrBuf = new MTI_UINT16[TSTW * TSTH * 4];

	for (int i = 0; i < 1; i++)
	{
		extr->extractActivePicture(extrBuf, tstBuf, tstFmt);
		MTImemset(tstBuf[0], 0, TSTW*TSTH*6);
		extr->replaceActivePicture(tstBuf, tstFmt, extrBuf);
	}
#endif

#if 0
	extrBuf = new MTI_UINT16[TSTW * TSTH * 4];

	RECT extrct;
	extrct.top = 0;
	extrct.left = 0;
	extrct.right = TSTW - 1;
	extrct.bottom = TSTH - 1;
	for (int i = 0; i < 50; i++)
	{
		extr->extractSubregion(extrBuf, tstBuf, tstFmt, &extrct);
		MTImemset(tstBuf[0], 0, TSTW*TSTH*6);
		MTImemset(tstBuf[1], 0, TSTW*TSTH*6);
		extr->replaceSubregion(tstBuf, tstFmt, &extrct, extrBuf);
		extrct.top++;
		extrct.left++;
		extrct.right--;
		extrct.bottom--;
	}
#endif

#if 0
	extrBuf = new MTI_UINT16[TSTW * TSTH * 4];

	RECT extrct;
	extrct.top = 0;
	extrct.left = 0;
	extrct.right = TSTW - 1;
	extrct.bottom = TSTH - 1;
	for (int i = 0; i < 50; i++)
	{
		extr->extractSubregionWithinFrame(extrBuf, tstBuf, tstFmt, &extrct);
		MTImemset(tstBuf[0], 0, TSTW*TSTH*6);
		MTImemset(tstBuf[1], 0, TSTW*TSTH*6);
		extr->replaceSubregionWithinFrame(tstBuf, tstFmt, &extrct, extrBuf);
		extrct.top++;
		extrct.left++;
		extrct.right--;
		extrct.bottom--;
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////

void CDisplayer::DumpAWholeBunchOfReallyUglyShit()
{
	MTIostringstream os;
	os << "------------------------------------------------------" << endl;
	os << "        rgbRect = { " << rgbRect.left << ", " << rgbRect.top << ", " << rgbRect.right << ", " <<
			rgbRect.bottom << " }" << endl;
	os << "        imgRect = { " << imgRect.left << ", " << imgRect.top << ", " << imgRect.right << ", " <<
			imgRect.bottom << " }" << endl;
	os << "        dspRect = { " << dspRect.left << ", " << dspRect.top << ", " << dspRect.right << ", " <<
			dspRect.bottom << " }" << endl;
	os << "        frmRect = { " << frmRect.left << ", " << frmRect.top << ", " << frmRect.right << ", " <<
			frmRect.bottom << " }" << endl;
	os << "        visRect = { " << visRect.left << ", " << visRect.top << ", " << visRect.right << ", " <<
			visRect.bottom << " }" << endl;
	os << "        clpRect = { " << clpRect.left << ", " << clpRect.top << ", " << clpRect.right << ", " <<
			clpRect.bottom << " }" << endl;
	os << "        rgbRectWidth x rgbRectHeight = " << rgbRectWidth << " x " << rgbRectHeight << endl;
	os << "        imgWdth x imgHght = " << imgWdth << " x " << imgHght << endl;
	os << "        dspWdth x dspHght = " << dspWdth << " x " << dspHght << endl;
	os << "        frmWdth x frmHght = " << frmWdth << " x " << frmHght << endl;
	os << "        visWdth x visHght = " << visWdth << " x " << visHght << endl;
	os << "        frameWidth x frameHeight (framePitch) = " << frameWidth << " x " << frameHeight << " (" <<
			framePitch << ")" << endl;
	os << "        screenWdth x screenHght = " << screenWdth << " x " << screenHght << endl;
	os << "        textureWdth x textureHght = " << frmWdth << " x " << frmHght << endl;

	TRACE_0(errout << os.str());
	os.str("\n");

	os << "        dspXctr, dspYctr = (" << dspXctr << ", " << dspYctr << ")" << endl;
	os << "        MW.ClientWidth x MW.ClientHeight = " << mainWindow->ClientWidth << " x " <<
			mainWindow->ClientHeight << endl;
	os << "        DP.Left & DP.Top = " << mainWindow->DisplayPanel->Left << " & " <<
			mainWindow->DisplayPanel->Top << endl;
	os << "        DP.ClientWidth x DP.ClientHeight = " << mainWindow->DisplayPanel->ClientWidth << " x " <<
			mainWindow->DisplayPanel->ClientHeight << endl;
	os << "        borderWidth = " << borderWidth << endl;
	os << "        justifyImage = " << justifyImage << endl;
	os << "        displayMode = " << displayMode << endl;
	os << "        XScaleFrameToClient x YScaleFrameToClient = " << XScaleFrameToClient << " x " <<
			YScaleFrameToClient << endl;
	os << "        XScaleClientToFrame x YScaleClientToFrame = " << XScaleClientToFrame << " x " <<
			YScaleClientToFrame << endl;
	os << "        truZoomLevel = " << truZoomLevel << "      " << "zoomFactor = " << zoomFactor << endl;
	os << "        pixelAspectRatio = " << pixelAspectRatio << "    " << "frameAspectRatio = " <<
			frameAspectRatio << endl;
	os << "-------------------------------------------------------------------" << endl;

	TRACE_0(errout << os.str());
}

////////////////////////////////////////////////////////////////////////////////

   void CDisplayer::setDisplayMaskOverlay(bool value)
   {
       _displayMaskOverlay = value;
   }
