//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SplashScreenUnit.h"

#include "DllSupport.h"
#include "MTIWinInterface.h"
#include <string>       // std::string
using std::string;
#include <iostream>     // std::cout

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma resource "*.dfm"
TSplashScreen *SplashScreen;
//---------------------------------------------------------------------------
__fastcall TSplashScreen::TSplashScreen(TComponent* Owner)
    : TForm(Owner)
{
	refreshCounter = 1;
	SplashImage->ControlStyle << csOpaque;

	char filename[MAX_PATH];
	GetModuleFileName(NULL, filename, sizeof(filename));
	string versionString = GetDLLVersion(filename);
	auto pos = versionString.find('.');
	if (pos != string::npos)
	{
		pos = versionString.find('.', pos + 1);
		if (pos != string::npos)
		{
			versionString = versionString.substr(0, pos);
		}
	}

   // Added a space so last character doesn't get cut off because it's italic.
	VersionLabel->Caption = (AnsiString)"Version " + versionString.c_str() + " ";
}

//---------------------------------------------------------------------------
void TSplashScreen::StartTimer(float secs, bool selfDestruct)
{
    if (secs > 0.)
    {
        SplashTimer->Interval = (int) (secs * 1000.);
    }
	if (IsDebuggerAttached())
    {
        FormStyle = fsNormal;
    }
    else
    {
         ::SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height,
						SWP_NOMOVE+SWP_NOSIZE);
        refreshCounter = SplashTimer->Interval / 100;
        SplashTimer->Interval = 100;
    }

	bDoSelfDestruct = selfDestruct;
    SplashTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSplashScreen::SplashTimerTimer(TObject *Sender)
{
    if (--refreshCounter > 0)
    {
	   ///  FormStyle = fsStayOnTop;
         SetZOrder(true);
		 ::SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height,
						SWP_NOMOVE+SWP_NOSIZE);
    }
    else
    {
        if (bDoSelfDestruct)
			Application->Terminate();
		else
            Close();
        SplashTimer->Enabled = false;
    }
}
//---------------------------------------------------------------------------

