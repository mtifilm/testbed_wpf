
#include "Reticle.h"
#include "IniFile.h"
#include "MTIio.h"
#include <stdio.h>

string CReticle::sectionName           = "Reticle";
string CReticle::reticleDirectory      = "$(CPMP_USER_DIR)\\reticles\\";
string CReticle::reticleExtension      = ".ret";
string CReticle::reticleUnnamed        = "[NEW]";
string CReticle::reticleTypeKey        = "Type";
string CReticle::reticleAspectRatioKey = "AspectRatio";
string CReticle::reticleApertureKey    = "Aperture";
string CReticle::reticleXOffsetKey     = "XOffset";
string CReticle::reticleYOffsetKey     = "YOffset";
string CReticle::reticleHScaleKey      = "HScale";
string CReticle::reticleVScaleKey      = "VScale";

CReticle::CReticle()
{
   reticleFile = "";
   type = RETICLE_TYPE_NONE;
   aspectRatio = 0.0;
   aperture    = 0;
   xOffset = yOffset = 0;
   hScale  = vScale = 100;

   frameRect.left   = 0x7fffffff;
   frameRect.top    = 0x7fffffff;
   frameRect.right  = 0;
   frameRect.bottom = 0;

   primary.left   = 0x7fffffff;
   primary.top    = 0x7fffffff;
   primary.right  = 0;
   primary.bottom = 0;

   safeAction.left   = 0x7fffffff;
   safeAction.top    = 0x7fffffff;
   safeAction.right  = 0;
   safeAction.bottom = 0;

   safeTitle.left   = 0x7fffffff;
   safeTitle.top    = 0x7fffffff;
   safeTitle.right  = 0;
   safeTitle.bottom = 0;
}

CReticle::~CReticle()
{
}

//--------------------------Accessors----------------------------------------

string CReticle::getFilename()
{
   return reticleFile;
}

int CReticle::getType()
{
   return type;
}

double CReticle::getAspectRatio()
{
   return aspectRatio;
}

int CReticle::getAperture()
{
   return aperture;
}

int CReticle::getXOffset()
{
   return xOffset;
}

int CReticle::getYOffset()
{
   return yOffset;
}

int CReticle::getHScale()
{
   return hScale;
}

int CReticle::getVScale()
{
   return vScale;
}

RECT CReticle::getPrimaryRect()
{
   return primary;
}

RECT CReticle::getSafeActionRect()
{
   return safeAction;
}

RECT CReticle::getSafeTitleRect()
{
   return safeTitle;
}

//--------------------------Mutators-----------------------------------------

void CReticle::setFilename(const string& filename)
{
   if (type != RETICLE_TYPE_CUSTOM)
      reticleFile = "";
   else
      reticleFile = filename;
}

void CReticle::setType(int typ)
{
   type = typ;

   if (type == RETICLE_TYPE_NONE) {

      reticleFile = "";

      aspectRatio = 0.0;
      aperture    = 0;
      xOffset     = 0;
      yOffset     = 0;
      hScale      = 100;
      vScale      = 100;
      return;
   }
   else if (type != RETICLE_TYPE_CUSTOM) {

      reticleFile = "";

      aperture = RETICLE_APERTURE_FULL;
      xOffset  = 0;
      yOffset  = 0;
      hScale   = 100;
      vScale   = 100;

      switch(type) {

         case RETICLE_TYPE_133_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_133_FULL:

            aspectRatio = 1.3333333333333;

         break;

         case RETICLE_TYPE_166_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_166_FULL:

            aspectRatio = 1.6666666666666;

         break;

         case RETICLE_TYPE_178_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_178_FULL:

            aspectRatio = 1.7777777777777;

         break;

         case RETICLE_TYPE_185_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_185_FULL:

            aspectRatio = 1.8500000000000;

         break;

         case RETICLE_TYPE_235_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_235_FULL:

            aspectRatio = 2.3500000000000;

         break;

         case RETICLE_TYPE_240_ACAD:

            aperture = RETICLE_APERTURE_ACAD;
            /* FALL THROUGH */

         case RETICLE_TYPE_240_FULL:

            aspectRatio = 2.4000000000000;

         break;

         default:

            type = RETICLE_TYPE_NONE;
            aspectRatio = 0.0;
            aperture    = 0;

         break;

      }
   }
   else { // set to custom

      // when a NONE reticle gets converted into a
      // CUSTOM reticle, make sure we don't get the
      // aspect ratio = 0
      setAspectRatio(aspectRatio);
   }
}

void CReticle::setAspectRatio(double asprat)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   aspectRatio = asprat;

   if (aspectRatio < 0.25)
      aspectRatio = 0.25;
   else if (aspectRatio > 4.0)
      aspectRatio = 4.0;
}

void CReticle::setAperture(int aprtr)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   aperture = aprtr;

   if (aperture < 0)
      aperture = 0;
   else if (aperture > 1)
      aperture = 1;
}

void CReticle::setXOffset(int xoffs)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   xOffset = xoffs;
}

void CReticle::setYOffset(int yoffs)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   yOffset = yoffs;
}

void CReticle::setHScale(int hscal)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   hScale = hscal;

   if (hScale < 0)
      hScale = 0;
   if (hScale > 200)
      hScale = 200;
}

void CReticle::setVScale(int vscal)
{
   if (type != RETICLE_TYPE_CUSTOM) return;

   vScale = vscal;

   if (vScale < 0)
      vScale = 0;
   else if (vScale > 200)
      vScale = 200;
}

int CReticle::setFrameRect(const RECT& frmrct, double pixelAspectRatio)
{
   frameRect = frmrct;
   frmWdth = frameRect.right - frameRect.left + 1;
   frmHght = frameRect.bottom - frameRect.top + 1;
   if ((frmWdth <= 0)||(frmHght <= 0))
      return -1;

   if (type == RETICLE_TYPE_NONE) {

      primary    = frameRect;
      safeAction = frameRect;
      safeTitle  = frameRect;
      return 0;
   }

   double basisWdth = frmWdth;
   int retWdth, retHght;
   int xctr, yctr;
   int safeActionWdth, safeActionHght;
   int safeTitleWdth,  safeTitleHght;

   if (aperture == RETICLE_APERTURE_ACAD)
      basisWdth *= .8926;

   retWdth = (int)basisWdth;
   retHght = (int)(basisWdth * pixelAspectRatio / aspectRatio)&0xfffffffe;
   if (retHght > frmHght) {
      retHght = frmHght;
      retWdth = (int)((double)retHght * aspectRatio / pixelAspectRatio)&0xfffffffe;
   }

   retWdth *= (double)hScale/100.0;
   retHght *= (double)vScale/100.0;

   // FULL or ACADEMY registration
   xctr = frmWdth/2;
   yctr = frmHght/2;
   if (aperture == RETICLE_APERTURE_ACAD) {
      xctr = (frameRect.right + 1) - retWdth/2;
   }

   // apply offset to (xctr, yctr)
   xctr += xOffset;
   yctr += yOffset;

   // the reticle box
   primary.left   = xctr - retWdth/2;
   primary.top    = yctr - retHght/2;
   primary.right  = xctr + retWdth/2 - 1;
   primary.bottom = yctr + retHght/2 - 1;

   // the safe action box
   safeActionWdth = retWdth * .95;
   safeActionHght = retHght * .95;

   safeAction.left   = xctr - safeActionWdth/2;
   safeAction.top    = yctr - safeActionHght/2;
   safeAction.right  = xctr + safeActionWdth/2 - 1;
   safeAction.bottom = yctr + safeActionHght/2 - 1;

   // the safe title box
   safeTitleWdth = retWdth * .90;
   safeTitleHght = retHght * .90;

   safeTitle.left   = xctr - safeTitleWdth/2;
   safeTitle.top    = yctr - safeTitleHght/2;
   safeTitle.right  = xctr + safeTitleWdth/2 - 1;
   safeTitle.bottom = yctr + safeTitleHght/2 - 1;

   return 0;
}

//---------------------------Equivalence-------------------------------------

bool operator==(const CReticle& rl, const CReticle& rr)
{
   return(

      (rl.reticleFile == rr.reticleFile)&&
      (rl.type == rr.type)&&
      (rl.aspectRatio == rr.aspectRatio)&&
      (rl.aperture == rr.aperture)&&
      (rl.xOffset == rr.xOffset)&&
      (rl.yOffset == rr.yOffset)&&
      (rl.hScale == rr.hScale)&&
      (rl.vScale == rr.vScale)
   );
}

//--------------------Conform Reticle Filename-------------------------------
int CReticle::conformFilename(string& filnam)
{
   string conformedFilename;

   for (unsigned int i=0; i<filnam.size(); i++) {

      char filchar = filnam[i];

      if (
            ((filchar >= 'a')&&(filchar <= 'z'))||
            ((filchar >= 'A')&&(filchar <= 'Z'))||
            ((filchar >= '0')&&(filchar <= '9'))||
            (filchar == '_')
         ) {

         conformedFilename.push_back(filchar);
      }
   }

   int changes = filnam.size() - conformedFilename.size();

   filnam = conformedFilename;

   return(changes);
}

//-------------------- --Delete Reticle File---------------------------------
int CReticle::deleteReticleFile(const string& filename)
{
   string fullFilename = reticleDirectory + filename + reticleExtension;
   fullFilename = ConformEnvironmentName(fullFilename);

#if defined(__sgi) || defined(__linux)

   return unlink(fullFilename.c_str());

#elif defined(_WIN32)

   if (!DeleteFile(fullFilename.c_str()))
      return -1;
   return 0;

#endif
}

//---------------------------File R/W----------------------------------------

int CReticle::readFromFile(const string& filename)
{
   string fullFileName = reticleDirectory + filename + reticleExtension;

   reticleIniFile = CreateIniFile(fullFileName);
   if (reticleIniFile==NULL) {

      setType(RETICLE_TYPE_NONE);
   }
   else {

      type        = reticleIniFile->ReadInteger(sectionName,
                                    reticleTypeKey, RETICLE_TYPE_NONE);
      aspectRatio = reticleIniFile->ReadDouble(sectionName,
                                    reticleAspectRatioKey, 0.0);
      aperture    = reticleIniFile->ReadInteger(sectionName,
                                    reticleApertureKey, 0);
      xOffset     = reticleIniFile->ReadInteger(sectionName,
                                    reticleXOffsetKey, 0);
      yOffset     = reticleIniFile->ReadInteger(sectionName,
                                    reticleYOffsetKey, 0);
      hScale      = reticleIniFile->ReadInteger(sectionName,
                                    reticleHScaleKey, 100);
      vScale      = reticleIniFile->ReadInteger(sectionName,
                                    reticleVScaleKey, 100);

      // leave the filename read from in the reticle
      reticleFile = "";
      if (type == RETICLE_TYPE_CUSTOM)
         reticleFile = filename;

      DeleteIniFile(reticleIniFile);
   }

   return 0;
}

int CReticle::writeToFile(const string& filename)
{
   string fullFileName = reticleDirectory + filename + reticleExtension;

   reticleIniFile = CreateIniFile(fullFileName);
   if (reticleIniFile==NULL)
      return -1;

   reticleIniFile->WriteInteger(sectionName, reticleTypeKey, type);
   reticleIniFile->WriteDouble(sectionName, reticleAspectRatioKey, aspectRatio);
   reticleIniFile->WriteInteger(sectionName, reticleApertureKey, aperture);
   reticleIniFile->WriteInteger(sectionName, reticleXOffsetKey, xOffset);
   reticleIniFile->WriteInteger(sectionName, reticleYOffsetKey, yOffset);
   reticleIniFile->WriteInteger(sectionName, reticleHScaleKey, hScale);
   reticleIniFile->WriteInteger(sectionName, reticleVScaleKey, vScale);

   // leave the filename written to in the reticle
   reticleFile = filename;

   DeleteIniFile(reticleIniFile);

   return 0;
}


