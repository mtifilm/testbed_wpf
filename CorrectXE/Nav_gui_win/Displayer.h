#ifndef DisplayerH
#define DisplayerH
// ---------------------------------------------------------------------------

#include <vcl.h>
#include <Classes.hpp>
#include "machine.h"
#include "ClipIdentifier.h"
#include "ClipSharedPtr.h"
#include "DisplayLutSettings.h"
#include "ImageInfo.h"
#include "LUTManager.h"
#include "MediaFrameBuffer.h"
#include "MTIWinInterface.h"
#include "NavSystemInterface.h"   // for AutoClean display hackorama
#include "Registrar.h"
#include "Reticle.h"
#include <windows.h>
#include <mmsystem.h>
#include <stack>
#include "Ippheaders.h"
#include <MaskTool.h>
using std::stack;

#include <map>
using std::map;

// #define USE_TEXTURE

#define YERR 1 // 1 ??!? QQQ

class CBrush;
class CConvert;
class CExtractor;
class CImageFormat;
class CLineEngine;
class CMagnifier;
class CMask;
class CStrokeChannel;
class CMaskPixelRegion;
class CPixelRegionList;
class CRegistrar;
class CRotator;
class CSaveRestore;
class TmainWindow;
class CTransformEditor;
class SSpaghettiContext;

// struct for aiding display of lassos as graphics -----------------------------
//
struct TRANSITION
{
	// 0 is top edge
	// 1 is rgt edge
	// 2 is bot edge
	// 3 is lft edge
	int edge;

	// x                  on top edge
	// y - imgRect.top    on rgt edge
	// imgRect.right - x  on bot edge
	// y                  on left edge
	int coord;

	// transition value
	int val;

	int x;
	int y;

	TRANSITION *nxt;
};

// struct for restoring the display context on clip load
//
struct DISPLAY_CONTEXT
{
	// #define DISPLAY_MODE_TRUE_VIEW 0
	// #define DISPLAY_MODE_1_TO_1    1
	// #define DISPLAY_MODE_FIT_WIDTH 2
	// #define DISPLAY_MODE_PAINT     3
	int displayMode;

	// #define MAX_ZOOM_LEVEL 32
	double zoomFactor;

	// center coords of display window
	int dspXctr, dspYctr;
};

// struct for saving Paint strokes
struct Stroke
{
	int brushMode;

	MTI_INT32 macroFileLength;

	// MTI_UINT64 mdfdAddr;
	// MTI_INT32  mdfdBytes;

	MTI_UINT64 pelsAddr;
	MTI_INT32 pelsBytes;

	// import frame @time of stroke
	int importFrame;

	// transform @time of stroke
	double rotation;
	double stretchX;
	double stretchY;
	double skew;
	double offsetX;
	double offsetY;
};

struct FillLine
{
	int ylev;
	int xmin;
	int xmax;
};

class CDisplayer
{
public:

	CDisplayer();

	~CDisplayer();

	// FRAME DISPLAY FUNCTIONS
	int getGraphicsDeviceContexts(HWND hndl, HDC& scrndc, HGLRC& rndrdc);

	int initDisplayer(HWND handle, HDC scrndc, HGLRC rndrdc, int clientwdth, int clienthght);
	int initDisplayGraphics(HWND handle, HDC scrndc, HGLRC rndrdc, int clientwdth, int clienthght);

	void setMainWindowPtr(TmainWindow *);

	void setVisiblePartOfClientRectangle(RECT *, int, int);
	RECT getVisiblePartOfClientRectangle();

	void setBgndFlag(bool);

	bool getBgndFlag();

	void setImageFormat(const CImageFormat *);

	const CImageFormat *getImageFormat();

	int initPaintHistory(ClipSharedPtr clip);

	void clearImageFormat();

	void calculateDisplayRectangle(int, int);
	RECT getCompareFrameClientRectangle();
	RECT getMainFrameClientRectangle();
	static CBHook DisplayRectanglesChanged;

	void calculateReviewRectangle(RECT *);

	void calculateZoomedReviewRectangle(RECT *);

	void moveDisplayRectangle(int, int);

#define ASPECT_DEF    0
#define ASPECT_133    1
#define ASPECT_166    2
#define ASPECT_178    3
#define ASPECT_185    4
#define ASPECT_220    5
#define ASPECT_235    6
#define ASPECT_239    7
#define ASPECT_255    8
#define ASPECT_266    9
	bool setFrameAspectRatio(int);

	int loadAspectRatioOption();

	int saveAspectRatioOption(int);

	void updateAllOfKurtsFuckingRects();

	void calculateImageRectangle();

	void calculateMouseFrameCoordinates();

	void displayBackground();

	// void displayFrameFromFields(unsigned char **);
	void displayFrameFromFields(MediaFrameBufferSharedPtr frameBuffer);

	void displayFrameFromTargetBuf();

	void displayFrameAndBrush();

	void showImportFrame();

	///////////////////
	void displayFrame();

	void displayFrame(MediaFrameBufferSharedPtr frameBuffer,
							int frame,
							bool fast = true,
							CPixelRegionList *pxlrgnlst = NULL,
							bool targetMayHaveChanged = true);

	void displayFrameCalledFromPlayer(MediaFrameBufferSharedPtr frameBuffer,
							int frame,
							bool fast = true,
							CPixelRegionList *pxlrgnlst = NULL,
							bool targetMayHaveChanged = true);

	void displayFrameWithWiper(
							MediaFrameBufferSharedPtr frameBuffer,
							MediaFrameBufferSharedPtr wipeFrameBuffer,
							int frame,
							bool fast,
							CPixelRegionList *pxlrgnlst,
							bool targetMayHaveChanged,
							DPOINT dividerRefPoint,
							float dividerAngleInDegrees);

	void displayFramesSideBySide(
							MediaFrameBufferSharedPtr frameBuffer,
							MediaFrameBufferSharedPtr wiperFrameBuffer,
							int frame,
							bool fast,
							CPixelRegionList *pxlrgnlst,
							bool targetMayHaveChanged);

	bool displayFrameNoSwap(
							MediaFrameBufferSharedPtr frameBuffer,
							int frame,
							bool fast,
							CPixelRegionList *pxlrgnlst,
							bool targetMayHaveChanged);

	void displayWiperFrame(MediaFrameBufferSharedPtr wiperFrameBuffer, int *dividerIndices);
	void displayFrameIfWeAreNotShowingDiffBlobs();
	void displayFrameWithHackPreview(int frameNumber, MediaFrameBufferSharedPtr frameBuffer);
	void displayFrameWithDisplayBufferPreview(int frameNumber, MediaFrameBufferSharedPtr frameBuffer);
	void displayFrameFromImportBuf();
	void displayImportOverTarget(int onionskinmode);
	void displayIntermediate(MTI_UINT16 *);

   void clearBackBuffer();
	void blitPictureToBackBuffer(unsigned int *screenFrameBuffer);
	void blitSideBySidePictureToBackBuffer(unsigned int *screenFrameBuffer);
	void blendPictureToBackBuffer(unsigned int *screenFrameBuffer);
	void blendMaskBorderLinesToBackBuffer(unsigned char *mask);
	/////////////////

	int getDisplayMode();

#define DISPLAY_MODE_TRUE_VIEW 0
#define DISPLAY_MODE_1_TO_1    1
#define DISPLAY_MODE_FIT_WIDTH 2
	void setDisplayMode(int newmode);

	void resetDisplayMode();

	double snapZoomFactor(double);

	bool zoomAll();

	bool zoomIn();

	bool zoomOut();

	void setDisplayWindowCenter(int, int);

	int getDisplayXctr();

	int getDisplayYctr();

	bool setZoomFactor(double);

	double getZoomFactor();

	void panStart();

	void panStop();

	void setPanMode(int panmode);

	int getPanMode();

	void panModeJog();

	bool isPanning();

	void enterMouseZoomMode();
	void exitMouseZoomModeOrResetZoom();

	bool isMouseZoomModeActive() {return mouseZoomModeIsActive;};

	bool didZoomOrPanInMouseZoomMode() {return zoomedOrPannedInMouseZoomMode;};

	void setDisplayContext(int mode, DISPLAY_CONTEXT &);

	DISPLAY_CONTEXT getDisplayContext();

	// LUT methods

	void setCustomLUTOption(int option);

	int getCustomLUTOption();

	void setCustomLUTColorSpaceIndex(int colrspindx);

	int getCustomLUTColorSpaceIndex();

	void setCustomLUTTransferFunctionIndex(int xfrfnindx);

	int getCustomLUTTransferFunctionIndex();

	void setCustomLUTSourceRange(bool usecustom);

	bool getCustomLUTSourceRangeEnabled();

	void setCustomLUTSourceRangeBlk(MTI_UINT16 srcrngblk[]);

	void setCustomLUTSourceRangeWhi(MTI_UINT16 srcrngwhi[]);

	void getCustomLUTSourceRangeBlk(MTI_UINT16 srcrngblk[]);

	void getCustomLUTSourceRangeWhi(MTI_UINT16 srcrngwhi[]);

	void setCustomLUTDisplayRangeMin(MTI_UINT16 dsprngmin[]);

	void setCustomLUTDisplayRangeMax(MTI_UINT16 dsprngmax[]);

	void getCustomLUTDisplayRangeMin(MTI_UINT16 dsprngmin[]);

	void getCustomLUTDisplayRangeMax(MTI_UINT16 dsprngmax[]);

	void setCustomLUTGamma(int gamma);

	int getCustomLUTGamma();

	void applyLUTSettings(CDisplayLutSettings &settings);

	void getLUTSettings(CDisplayLutSettings &settings);

	void establishDisplayLUT();

	EColorSpace getFormatColorSpace();

	void setCurrentColorSpace(EColorSpace colorspace);

	EColorSpace getCurrentColorSpace();

	EPixelComponents getPixelComponents();

	int getBitsPerComponent();

	int getComponentBlack(int i);

	int getComponentWhite(int i);

	void setDefaultGamma(int gamma);

	int getDefaultGamma();

	void setCurrentGamma(int gamma);

	int getCurrentGamma();

	void setRGBChannelsMask(int mask);

	int getRGBChannelsMask();

	void setImageJustification(int jstfy);

	int getImageJustification();

	void setAlphaColor(MTI_UINT32 alphacol);

	MTI_UINT32 getAlphaColor();

	bool isAlphaEnabled();

	// MAGNIFIER FUNCTIONS

	CMagnifier * allocateMagnifier(int wdth, int hght, RECT *magwin);

	void activateMagnifier(CMagnifier *mag, void(*rfrcallback)(void *obj, void *mag), void *strobj);

	void deactivateMagnifier(CMagnifier *mag);

	void freeMagnifier(CMagnifier *mag);

	// BASIC RECTANGLE FUNCTIONS

	bool rectangleIsValid(RECT *);

	bool rectangleAND(RECT *, RECT *);

	void clipPointClient(POINT *);

	void clipRectangleClient(RECT *);

	void clipLassoClient(POINT *);

	void clipPointFrame(POINT *);

	void clipRectangleFrame(RECT *);

	void clipLassoFrame(POINT *);

	void fattenRectangleClient(RECT *, int);

	void fattenRectangleFrame(RECT *, int);

	bool isPointInFrameRect(POINT);

	bool isMouseInFrame();

	void lineSegmentExtent(RECT *, POINT*, POINT*);

	int scaleXFrameToClient(int);
	int scaleYFrameToClient(int);

	int dscaleXFrameToClient(double);
	int dscaleYFrameToClient(double);

	double ddscaleXFrameToClient(double);
	double ddscaleYFrameToClient(double);

	void scalePointFrameToClient(POINT *);
	void scaleRectangleFrameToClient(RECT *);

	int scaleXClientToFrame(int);
	int scaleYClientToFrame(int);

	int scaleCursorXClientToFrame(int);
	int scaleCursorYClientToFrame(int);

	void saveMouseShiftState(TShiftState);

	TShiftState getMouseShiftState();

	int scaleXClientToImageX(int);
	int scaleYClientToImageY(int);

	double dscaleXClientToFrame(int);
	double dscaleYClientToFrame(int);

	void scalePointClientToFrame(POINT *);
	void scaleRectangleClientToFrame(RECT *);

	double scaleClientXToRgbRectNormalized(int clientX);
	double scaleClientYToRgbRectNormalized(int clientY);

	// BASIC GRAPHICS DISPLAY FUNCTIONS

	int setGraphicsColor(int);

	int setHighlightColor(int);

	int setMatteColor();

	int restoreGraphicsColor();

	int setGraphicsBuffer(int);

	void flushGraphics();

	//void drawRectangleClient(RECT *);

	void clipAndDrawRectangleFrame(RECT *);

	void clipAndFillRectangleFrame(RECT *);

	void drawRectangleFrame(RECT *);

	void drawRectangleClientNoFlush(RECT *rct);

	void drawFilledRectangleNoFlush(const RECT &rect);

	void drawLassoFrame(POINT *);

	void drawBezierFrame(BEZIER_POINT *);

	void drawMaskOverlayFrame(bool *mask);

	void drawPixelRegionListFrame(CPixelRegionList *);

	void drawBrushFrame(CBrush *, int color, double x, double y);

	void drawAutoAlignBoxFrame(int color, double x, double y);

	// clipped graphics - client coords

	void clipToVisibleRectangle();

	void clipToImageRectangle();

	RECT getImageRectangle();

#define TLFT 0x8
#define TTOP 0x4
#define TRGT 0x2
#define TBOT 0x1
	int tCode(int, int);

	static TRANSITION *NodeSort(TRANSITION *loptr, TRANSITION *hiptr);

	// unclipped move and draw

	void movTo(int x, int y);

	void linToNoFlush(int x, int y);

//	void linTo(int x, int y);

	// clipped move and draw - client coords

	void moveToClient(int x, int y);

	void lineToClientNoFlush(int x, int y);

//	void lineToClient(int x, int y);

	void drawTransitionsClientNoFlush();

	void splineToClientNoFlush(int x1, int y1, int x2, int y2, int x3, int y3, unsigned int dashpat);

//	void splineToClient(int x1, int y1, int x2, int y2, int x3, int y3, unsigned int dashpat);

	int splineTo16NoFlush(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, unsigned int dashpat,
		 unsigned int index);

	// box size in client coords is 2 x BOX_SIZE + 1
#define BOX_SIZE 5
	void drawUnfilledBoxClientNoFlush(int x, int y);

	void drawFilledBoxClientNoFlush(int x, int y);

//	void drawUnfilledBoxClient(int x, int y);

//	void drawFilledBoxClient(int x, int y);

	// values of select mask
#define SELECT_NONE      0
#define SELECT_VTX       1
#define SELECT_PRE       2
#define SELECT_NXT       4
#define SELECT_OPP       8
#define SELECT_EQU      16
	void drawVertexClientNoFlush(int xctrlpre, int yctrlpre, int x, int y, int xctrlnxt, int yctrlnxt, int selectmask);

//	void drawVertexClient(int xctrlpre, int yctrlpre, int x, int y, int xctrlnxt, int yctrlnxt, int selectmask);

	// clipped move and draw - frame coords

	RECT getActiveRectangle();

	void moveToFrame(double x, double y);

	void lineToFrameNoFlush(double x, double y);

//	void lineToFrame(double x, double y);

	void splineToFrameNoFlush(double x1, double y1, double x2, double y2, double x3, double y3, unsigned int dashpat = 0xffffffff);

//	void splineToFrame(double x1, double y1, double x2, double y2, double x3, double y3, unsigned int dashpat = 0xffffffff);

	void drawUnfilledBoxFrameNoFlush(double x, double y);

	void drawFilledBoxFrameNoFlush(double x, double y);

	void drawUnfilledBoxScaledNoFlush(double x, double y, double rad);

	void drawFilledBoxScaledNoFlush(double x, double y, double rad);

	void drawVertexFrameNoFlush(double xctrlpre, double yctrlpre, double x, double y, double xctrlnxt, double yctrlnxt,
		 int selectmask);

//	void drawVertexFrame(double xctrlpre, double yctrlpre, double x, double y, double xctrlnxt, double yctrlnxt, int selectmask);

	void drawArrowFrameNoFlush(double frx, double fry, double tox, double toy, double wdx, double wdy);

	// NEW ACCURATE TRACKING BOX
	void drawTrackingBoxNoFlush(double centerX, double centerY, double innerRadiusX, double innerRadiusY,
		 double outerRadiusX, double outerRadiusY, bool showCrosshairs);

	void drawShapeOnFrameNoFlush(double centerX, double centerY, int shapeType, double radius, double aspectRatio,
		 double angle, MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B);

	// RECTANGLE IN PROGRESS DISPLAY FUNCTION

	void drawRectangleInProgress();

	// LASSO IN PROGRESS DISPLAY FUNCTION

	void drawLassoInProgress();

	// MOUSE INTERFACE METHODS

	bool getMousePositionClient(int *, int *);

	bool getMousePositionFrame(int *, int *);
	bool getMousePositionFrame(double *, double *);

	int scaleMouseXClientToFrame(int);

	int scaleMouseYClientToFrame(int);

	void mouseDn();

	void mouseMv(int, int);

	void mouseUp(bool dspfrm = true);

	void showCursor(bool showFlag, bool forceArrowCursor);

	void setPreferredFrameCursor(TCursor);

	void updateCursor(bool forceArrowCursor);

	void setViewOnlyMode(bool flag);

	// STRETCH RECTANGLE ACQUISITION METHODS

	void setDRSLassoMode(bool);
	bool getDRSLassoMode() const ;

	bool isStretchingOrPainting();

	void stretchRectangleClient(void(*)(void *, void *, void *), void *);

	void setStretchRectangleColor(int color);

	void stretchRectangleFrame(void(*)(void *, void *, void *), void *);

	void cancelStretchRectangle();

	// RETICLE support methods

	void setReticleMode(int);

	int getReticleMode();

	void setExitReticle(CReticle&);

	CReticle getExitReticle();

	void setReticle(CReticle&);

	CReticle getReticle();

	void drawReticle();

	// support for PAINT tool
	void enterPaintMode();

	void enterRegistrationMode();

	void clearLoadedRegistrationFrame();

	void clearRegisteredRegistrationFrame();

	void enableAutoAccept(bool);

	void initImportProxy(int, int);

	void clearTransform();

	void setTransform(double, double, double, double, double, double);

	void getTransform(double *, double *, double *, double *, double *, double *);

	void setMatBox(RECT *);

	void getMatBox(RECT *);

	void setRegistrationTransform(AffineCoeff *);

	void getRegistrationTransform(AffineCoeff *);

	void exitPaintMode();

	void exitRegistrationMode();

	void setCaptureMode(int);

	int getCaptureMode();

	void clearPaintFrames();

	bool getPaintFrames(int *trg, int *imp);

	void unloadTargetAndImportFrames();

	void unloadRegistrationFrame();

	bool targetFrameIsModified();

	int allocateTargetBuffers();

	int allocateRegistrationBuffers();

	void enableForceTargetLoad(bool);

	void enableForceImportLoad(bool);

	void makeAndTransmitProxy();

	int getTargetFrameIndex();

	void loadTargetFrame(int target, bool fast = false);

	void loadRegistrationFrame(int registr);

	void registerLoadedRegistrationFrame();

	void reloadModifiedTargetFrame();

	void redrawTargetFrame();

	void reloadModifiedImportFrame();

	void reloadUnmodifiedTargetFrame();

	MTI_UINT16 *getTargetIntermediate();

	MTI_UINT16 *getTargetOriginalIntermediate();

	MTI_UINT16 *getTargetPreStrokeIntermediate();

	MTI_UINT16 *getRegisteredIntermediate();

	MTI_UINT16 *getPreRegisteredIntermediate();

	CPixelRegionList& getOriginalPixels();

	void setAltClipDiffThreshold(float newThreshold); // 0.0 <= t < 1.0

	void setOrigValDiffThreshold(float newThreshold); // 0.0 <= t < 1.0

	void setPaintToolChangesPending(bool flag);
	bool arePaintToolChangesPending();

	bool *CompareImportAndTargetFrames(MTI_UINT16 diffThreshold, bool targetMayHaveChanged = true);

	void compareTwoFrames(MTI_UINT16 *frameBuffer1, MTI_UINT16 *frameBuffer2, MTI_UINT16 threshold, bool *outputMask);

	void createStrokeStack();

	void destroyStrokeStack();

	void makeStrokeStack();

	void clearStrokeStack();

	void transmitStrokeStack();

	int pushStrokeFromTarget(int);

	int popStrokesToTarget(int);

	void completeMacroStroke();

	int setImportFrameClipName(const string &clipName);

	string getImportFrameClipName();

	ClipSharedPtr getImportClip();

	CVideoFrameList* getImportClipVideoFrameList();

	void setImportMode(int);

	int getImportMode();

	int computeDesiredImportFrame(int);

	void setImportFrameOffset(int);

	bool syncImportFrameTimecode();

	int getImportFrameOffset();

	void setImportFrameTimecode(int);

	int getImportFrameTimecode();

	void allocateImportBuffers();

 	void preloadImportFrame(int import, int impmod, double rotation, double stretchX, double stretchY, double skew,
		 double offsetX, double offsetY);

	void forceTargetAndImportFrameLoaded();

	void loadTargetAndImportFrames(int trgfrm);

	void loadImportFrame(int import);

	void fetchImportFrame(int import, bool fast = false);

	void createOriginalValuesImportFrame(int importFrameIndex, bool fast);

	int createOriginalValuesFrame(MTI_UINT16 *buffer, int frameIndex);

	void setOnionSkinMode(int);

	void setOnionSkinTargetWgt(double);

	void setMouseMinorState(int);

	void setColorPickSampleSize(int);

	void getColorPickSample(int, int, int&, float&, float&, float&);

	void setFillTolerance(int);

	void setDensityStrength(int);

	void setGrainStrength(int);

	void setGrainPresets(GrainPresets *);

	void moveImportFrame(int, int);

	void enableBrushDisplay(bool);

#define BRUSH_MODE_THRU  1
#define BRUSH_MODE_ERASE 2
#define BRUSH_MODE_ORIG  3
	void loadPaintBrush(CBrush *, int newmode, int darkerthrsh, int lighterthrsh, float *colorcomp);

	CBrush *getPaintBrush(int *, int *, bool *, float *);

	void loadCloneBrush(CBrush *, bool isRel, int x, int y);

	CBrush *getCloneBrush(bool *, int *, int *);

	void unloadPaintBrush();

	void unloadCloneBrush();

	void setAutoAlignBoxSize(int);

	void autoAlign(int, int);

	void pickOneColor(int, int);

	void clearFillSeed();

	void pickOneFillSeed(int, int);

	void recycleFillSeed();

	bool pixelQualifies(int, int);

	void expandFillLine(FillLine *);

	void paintFillLine(FillLine *);

	void deriveNorthFillLines(FillLine *);

	void deriveSouthFillLines(FillLine *);

	int iterateOneFillCycle();

	int getMaxComponentValue();

	void forceMouseToWindowCenter(bool warpScreenCursor);

	void setDisplayProcessed(bool proc);

	void paintOneBrushPositionFrame(int, int);

	void paintOneStrokeSegmentFrame(int, int, int, int);

	int initMaskForInpainting();

	int iterateOneInpaintCycle();

	void setImportRotationFast(double angle, double strx, double stry, double skew, double offx, double offy);

	void setImportRotationExact(double angle, double strx, double stry, double skew, double offx, double offy);

	void setImportOffsets(double, double);

	void setImportOffsetsFast(int, int);

	void getImportOffsets(double *, double *);

	void initTransformEditor(CTransformEditor *, RECT&);

	bool isOnTargetFrame();

	void rotateExact(MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt, double ang, double strx, double stry,
		 double skw, double xoff, double yoff);

	void rotateFast(MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt, double ang, double strx, double stry,
		 double skw, double xoff, double yoff);

	void setUseTrackingAlignOffsets(bool);

	void clearAllTrackingAlignOffsets();

	void setTrackingAlignOffsets(int frame, double xoff, double yoff);

	void getTrackingAlignOffsets(int frame, double &xoff, double &yoff);

	// support for CURSOR CONTROL API
	void enterCursorOverrideMode(int CursorIndex);

	void exitCursorOverrideMode();

	void screenToClientCoords(int inX, int inY, int &outX, int &outY);

	// support for DeWarp
	void getLastFrameAsIntermediate(MTI_UINT16 *);

	// HACK for loupe
	MediaFrameBufferSharedPtr getLastDisplayedFrameBuffer();

	// TEST CODE
	void initTestFrame();

	int getSinglePixelValues(int clientX, int clientY, MTI_UINT16 *componentValues);

	int getBorderWidth() {return borderWidth;};

#ifdef FRAMEWRX
	float getTrueZoomLevel();
#endif // FRAMEWRX

	// AutoClean display Hack-O-Rama!
	void SetAutoCleanOverlayList(const OverlayDisplayList &newList);

	void MakeSureThisRectIsVisible(const RECT &rect);

	void GetDisplayLutValues(MTI_UINT8 *outputValues);
	// End of AutoClean display hack-o-rama

	// for scratch sidecar positioning
	RECT getStretchRectangleClientCoords();

	// for color breathing preview hack
	void enterPreviewHackMode();
	void exitPreviewHackMode();

/////////////////////////////////////////////////////////////////////////////

	bool getDisplayMaskOverlay() const {return _displayMaskOverlay;}
	void setDisplayMaskOverlay(bool value);

//	bool getDisplayMaskInterior() const {return _displayMaskInterior;}
//
//	void setDisplayMaskInterior(bool value) {_displayMaskInterior = value;}

private:
	int attemptEstablishDisplayLUT();

	// STABILIZATION TRACKING BOX DRAWING HELPERS
	int computeAlphaValueForTrackingBoxLine(double dpos);
	void drawLineForTrackingBoxRectangleNoFlush(double dx1, double dy1, double dx2, double dy2, int col);
	void drawTrackingBoxRectangleWithCrosshairs(const DRECT &drct, double chlenh, double chlenv, int col, bool dsh);

	// => main window
	TmainWindow *mainWindow;

	// CONVERTERS
	CConvert *_mainFrameConverter = nullptr;
	CConvert *_compareFrameConverter = nullptr;

	// EXTRACTOR
	CExtractor *extr;

	// LUT MANAGER
	CLUTManager *lutmgr;

	// FRAME DISPLAY VARIABLES --------------------------------------------------

	int oldDisplayMode; // added to simplify mode restore after PAINT
	int displayMode; // added to simplify preferences implementation

	bool bgndFlag; // display gray bgnd before frame
	bool imgShrinkFlag; // set if new gray area is exposed by zoom operation

	int borderWidth; // width of colored border around image rectangle
	int justifyImage; // For 1:1 zoom 1: left, center or right justify image
	int rawClearColor; // clear color in 4-byte format
	float clearColor[4]; // R,G,B,ALPHA components of clear color

	RECT visRect; // visible part of client rectangle (client coords)
	int visWdth, visHght; // dimensions of above

	RECT rgbRect; // target rectangle of software CONVERT
   RECT leftRgbRect; // left rectangle for side-by-side frame display.
	int rgbRectWidth, rgbRectHeight; // dimensions of above

	RECT imgRect; // image part of above (client coords)
	int imgWdth, imgHght; // dimensions of above

	RECT frmRect; // frame rectangle (frame coordinates)
	int frmWdth, frmHght; // dimensions of above

	// RECT ptFrmRect; // rotated, stretched frame
	// int ptFrmWdth, ptFrmHght;

	RECT clpRect; // rectangle used for graphics clipping - visRect or imgRect
	int clpWdth, clpHght; // dimensions of above

	// parameters for the display rectangle
#define MAX_ZOOM_LEVEL 32.0
#define MIN_ZOOM_LEVEL 0.1
	double oldZoomFactor;
	double zoomFactor;
	double truZoomLevel; // ZOOM level includes trueview component

	struct zoomStackEntry
	{
		zoomStackEntry(int xArg, int yArg, double zArg) : x(xArg), y(yArg), zoomLevel(zArg) {};

		double x;
		double y;
		double zoomLevel;
	};

	stack<zoomStackEntry>zoomStack;

	int zoomMousePosX = -1;
	int zoomMousePosY = -1;

	enum ZoomDirection
	{
		zoomingOut = -1, noDirection = 0, zoomingIn = 1} zoomStackDirection = noDirection;

	int dspXctr; // center x = dspRect.left + dspWdth/2
	int dspYctr; // center y = dspRect.top  + dspHght/2

	RECT dspRect; // displayed rectangle
	int dspWdth, dspHght; // dimensions of above

	// image format and extracts
	const CImageFormat *imgFmt;
	double pixelAspectRatio; // direct from CLIP
	double frameAspectRatio; // direct from CLIP
	int frameWidth; // total frame width
	int frameHeight; // total frame height
	int framePitch; // pitch in bytes (direct from CLIP)
	EPixelComponents framePixelComponents; // YUV422 or RGB
	int frameComponentCount; // no of components per pel
	int frameInternalSize; // bytes per internal frame
	int frameBitsPerComponent; // 8, 10, or 16
	int frameMaxComponentValue; // 255, 1023, or 32767
	int framePixelPacking; // various options
	int frameFieldCount; // 2 if interlaced
	EColorSpace frameColorSpace; // color space
	MTI_UINT16 frameColorSpaceBlack[3]; // component mins
	MTI_UINT16 frameColorSpaceWhite[3]; // component maxs
	int frameGamma; // the gamma when the clip is loaded

	// scale factors for mapping frame to client coords
	double XScaleFrameToClient;
	double YScaleFrameToClient;

	// scale factors for mapping client to frame coords
	double XScaleClientToFrame;
	double YScaleClientToFrame;

	// scale factors for mapping cursor to frame coords
	double XScaleCursorToFrame;
	double YScaleCursorToFrame;

	// saved elements of the display context - needed when the
	// user selects a preference to maintain current settings
	RECT oldFrmRect; // previous frmRect
	DISPLAY_CONTEXT oldDisplayContext;

	// custom LUT variables
#define LUT_OPTION_INTERNAL 0
#define LUT_OPTION_DEFAULT  1
#define LUT_OPTION_CUSTOM   2
	int customLUTOption;

#define COLOR_SPACE_INDEX_DEFAULT     0
#define COLOR_SPACE_INDEX_LINEAR      1
#define COLOR_SPACE_INDEX_LOG         2
#define COLOR_SPACE_INDEX_SMPTE_240   3
#define COLOR_SPACE_INDEX_CCIR_709    4
#define COLOR_SPACE_INDEX_CCIR_601_BG 5
#define COLOR_SPACE_INDEX_CCIR_601_M  6
#define COLOR_SPACE_INDEX_EXR         7
	int customLUTColorSpaceIndex; // custom color space index (0-7)
	int customLUTTransferFunctionIndex; // same values as color space index

	bool customLUTSourceRangeEnabled; // custom LUT source range
	MTI_UINT16 customLUTSourceRangeBlk[3];
	MTI_UINT16 customLUTSourceRangeWhi[3];
	MTI_UINT16 customLUTDisplayRangeMin[3];
	MTI_UINT16 customLUTDisplayRangeMax[3];
	int customLUTGamma;

	// this mask filters entire channels
#define RED_CHANNEL  0x000000ff
#define GRN_CHANNEL  0x0000ff00
#define BLU_CHANNEL  0x00ff0000
#define ALL_CHANNELS 0x00ffffff
#define SHOW_GRAY    0xff000000
	int RGBChannelsMask;

	// AlphaColor is inside CConvert instance cnvt
#define SHOW_ALPHA_TRANSPARENT 0
#define SHOW_ALPHA_YELLOW 0x0000ffff

	// current values for input to LUTManager
	int currentLUToption;

	CImageFormat *currentImageFormat;
	CDisplayFormat currentDisplayFormat;
	CDisplayLUT *currentDisplayLUT;
	MTI_UINT8 *currentLUTPtr;

	// VARIABLES FOR CLIPPING LINES ---------------------------------------------

	// draw cursor
	int curx, cury;

	// edge vertices
	int VPreX, VPreY;
	int VCurX, VCurY;

	// Cohn-Sutherland codes
	unsigned char tcur, tpre;

	// crossing coordinates
	int XCrs, YCrs;

	// for clipping calculations
	double delxi, delyi;
	double lftxi, rgtxi;
	double botyi, topyi;

	// Jordan counter and transition records
#define MAXTRANS 128
	unsigned char jcnt;

	bool trndon;

	TRANSITION trnsns[MAXTRANS + 4];

	TRANSITION *trnptr;

	// OpenGL DRAWING STRUCTURES ------------------------------------------------

	// resolution of screen in pelsimportDiffPels
	int screenWdth;
	int screenHght;

	// resolution of texture 1
#define TEXTURE_WIDTH  2048
#define TEXTURE_HEIGHT 2048
	int textureWdth;
	int textureHght;

	unsigned int *screenFrameBuffer;

	HWND windowHandle;

	HDC screenDC;

	HGLRC renderRC;

#define MAX_PENS     NUMBER_RECT_COLORS // from machine.h
	unsigned int colorPalette[MAX_PENS];

	// MAGNIFIERS

#define MAX_MAGNIFIERS 8
	int freeMagnifiers;
	CMagnifier *magptr[MAX_MAGNIFIERS];

	// STRETCH RECTANGLE/LASSO VARIABLES-----------------------------------------

	// current color for RECTs, LASSOs, & BEZIERs
	int graphicsColor;

	// color for stretch rectangle while stretching
	int stretchRectangleColor;

	// current color for highlighting PIXEL REGIONS
	int highlightColor;

	// current buffer for graphics operations
	// either GL_FRONT or GL_BACK
	int graphicsBuffer;

	// lasso mode = TRUE for lasso, FALSE for plain rectangle
	bool lassoMode;

	void(*stretchRectangleCallback)(void *, void *, void *);

	void *stretchRectangleCallbackObj;

	// client coords of stretch rectangle
	RECT strect;

	// frame  coords of stretch rectangle
	double strectf_lft, strectf_top, strectf_rgt, strectf_bot;

	// cursor display flag
	bool cursorOn;

	// In view-only mode?
	bool _inViewOnlyMode = false;

#define crDropperIndex 1
#define crBucketIndex  2
#define crWhCrossIndex 3
#define crDropper TCursor(crDropperIndex)
#define crBucket  TCursor(crBucketIndex)
#define crWhCross TCursor(crWhCrossIndex)

	// the cursor to be displayed when the mouse
	// is inside imgRect, except when painting
	TCursor preferredFrameCursor;

	// the saved values of the mouse client coords
	// as they were passed in from MainWindowUnit's
	// method FormMouseMove
	TShiftState Shift;
	int X, Y;

	// the current  position of the mouse
	int xcur, ycur;

	// the previous position of the mouse
	int xpre, ypre;

	// the current position in frame coords
	double dxcurfrm, dycurfrm;

	// the previous position in frame coords
	double dxprefrm, dyprefrm;

	// the current position in frame coords
	int xcurfrm, ycurfrm;

	// the previous position in frame coords
	int xprefrm, yprefrm;

	CExtractor *singlePixelExtractor;

	// MTI_UINT8 *lastDisplayFields[2];
	MediaFrameBufferSharedPtr _lastDisplayedFrameBuffer = nullptr;

	// if the mouse has moved this far (|delx|+|dely|) make a new lasso point
#define SHORTEST_LASSO_LINE 5
	// iff the ends of the lasso are this sq distance apart or less, close it
#define CLOSE_ENOUGH 900

	// the lasso pt buffer and its fill ptr
	// MAX_LASSO_PTS is now defined in PixelRegions.h - 03/17/03
	int ilas;
	int icmp;
	POINT *lasso;

	// when ends are close enough, set this flag. When drawing
	// the LASSO, draw a "spark" across the gap between the ends
	bool lassoSpark;

	// the beg and end pts of the stretch
	int x1, y1, x2, y2;

	double x1f, y1f, x2f, y2f;

	// the direction codes for stretching
	int x1dir, y1dir, // dir just after (x1,y1)
		 x2dir, y2dir; // dir for this   (x2,y2)

#define CLIENT_COORDS 0
#define  FRAME_COORDS 1
	int coordMode;

	// A new state variable just for PAINT.
	// We can't use majorState any more
	// because MASK must be executed when
	// PAINT is up
	bool paintEnabled;

	// A new state variable to specialize
	// behavior based on the current tool
#define CURRENT_TOOL_NAVIGATOR    0
#define CURRENT_TOOL_PAINT        1
#define CURRENT_TOOL_REGISTRATION 2
#define CURRENT_TOOL_AUTOCLEAN    3
	int currentTool;

	// CAREFUL!!! THESE ARE DUPLICATED IN ToolSystemInterface.h !!!!!!!
#define CAPTURE_MODE_TARGET         0
#define CAPTURE_MODE_IMPORT         1
#define CAPTURE_MODE_BOTH           2
#define CAPTURE_MODE_ORIG_TARGET    3
#define CAPTURE_MODE_ORIG_IMPORT    4
#define CAPTURE_MODE_ALTCLIP_IMPORT 5
	int captureMode;

	// A new state variable just for PAINT.
	// The user has the option of editing
	// masks with or without the onion skin
	// display. Ergo, this must be separate.
	// CAREFUL!!! THESE ARE DUPLICATED IN ToolSystemInterface.h !!!!!!!
#define ONIONSKIN_MODE_OFF    0
#define ONIONSKIN_MODE_COLOR  1
#define ONIONSKIN_MODE_POSNEG (2+4)    // hahaha wtf??
	int onionSkinEnabled;

	// a new state variable specifying the
	// alpha weight to be assigned to the
	// target frame. The weight assigned
	// to the import frame is (255 - wt)
	unsigned char onionSkinTargetWgt;

	// A new state variable for CURSOR CONTROL.
	// A tool can tell us to override the standard
	// cursors with one of its own.
	int cursorOverrideIndex;

	// Import from a different clip.
	string _importFrameClipName;

	ClipSharedPtr _importFrameClip = nullptr;
	CVideoFrameList *_importClipFrameList = nullptr;
	// int _captureModeImportOffset = 0;

	// state variable for RECTs and LASSOs
#define IDLE                     0
#define STRETCH_RECTANGLE_ACTIVE 1
#define DRAW_LASSO_ACTIVE        2

	int majorState;

	// state variable used for PAINT
	// as well as RECTs and LASSOs.
#define CANCELLED              0
#define SEEKING_XY1            1
#define SEEKING_DIR            2
#define SEEKING_XY2            3
#define SEEKING_XYN            4
#define RECT_COMPLETED         5
#define LASSO_COMPLETED        5
#define ACQUIRING_IMPORT_FRAME 6
#define MOVING_IMPORT_FRAME    7
#define EDIT_MASK              8
#define ACQUIRING_PIXELS       9
#define PAINTING_PIXELS       10
#define TRACKING_CLONE_PT     11
#define PICKING_COLOR         12
#define PICKING_SEED          13
#define HEALING_PIXELS        14
#define PICKING_AUTOALIGN     15
	int minorState;
	int savedMinorState;

	// color pick sample size (1x1), (3x3), (5x5)
	int colorPickSampleSize;

	// current seed location
	int xseed, yseed;

	// fill tolerance (component units) & limits
	int fillTolerance;
	MTI_UINT16 minFillComp[3];
	MTI_UINT16 maxFillComp[3];
	MTI_UINT8 *markFilled;

	vector<FillLine*>fillLines;

	// flag set by position of 'U' key (pan)
#define PAN_OFF 0
#define PAN_X   1
#define PAN_Y   2
#define PAN_XY  (PAN_X + PAN_Y)
	int panMode;
	int panFlag;

	int panByGrabbing = false;
	double panGrabbedFramePointX = -1.0;
	double panGrabbedFramePointY = -1.0;

	bool mouseZoomModeIsActive;
	bool zoomedOrPannedInMouseZoomMode;

	// RETICLE VARIABLES --------------------------------------------------------

#define RETICLE_MODE_NONE        0x00
#define RETICLE_MODE_PRIMARY     0x01
#define RETICLE_MODE_SAFE_ACTION 0x02
#define RETICLE_MODE_SAFE_TITLE  0x04
#define RETICLE_MODE_ALL         0x07
#define RETICLE_MODE_MATTED      0x08
	int reticleMode = RETICLE_MODE_NONE;

	// the reticle that will be recorded in desktop.ini on exit
	CReticle exitReticle;

	// the reticle that is displayed during reticle editing
	CReticle currentReticle;

	// coordinates of primary rectangle
	RECT reticleRect;

	// coordinates of safe action rectangle
	RECT safeActionRect;

	// coords of safe title rect
	RECT safeTitleRect;

	////////////////////////////////////////////////////////////////////

	// flag to display original or processed
	bool displayProcessed;

	bool brushEnabled;

	CBrush *paintBrush;
	int brushMode;

	// Why the fuck is the max 2?
	// also I changed the min to not be zero
#define MIN_DENSITY 0.0001F
#define MAX_DENSITY 2.0F

	int densityStrength = 0;
	float fDensityFactor = 0.0F;
	int iDensityFactorTimes1024 = 0;

#define RND_RANGE 67108864
#define MIN_GRAIN_SIZE .05
#define MID_GRAIN_SIZE .25
#define RND_THR1 (RND_RANGE * MID_GRAIN_SIZE)
#define RND_THR2 ((int)(RND_THR1 * .7))
#define MAX_GRAIN_SIZE .45

	// WTF why is this ass-backwards!!!
#define GRAIN_STRENGTH_HIGH_LIMIT 1.
#define GRAIN_STRENGTH_LOW_LIMIT .5

	int grainStrength;
	float fGrainStrength;
	int grainSize;
	long lseed;
	GrainPresets *_grainPresets;

	unsigned int ulGrainStrengthDensityTimes1024;
	float fGrainStrengthDensity;
	unsigned int ulOneOverGrainStrengthDensityTimes1024;
	float fOneOverGrainStrengthDensity;

#define BRUSH_COMPONENTS_INVALID   0
#define BRUSH_COMPONENTS_LUMINANCE 1
#define BRUSH_COMPONENTS_YUV       2
#define BRUSH_COMPONENTS_RGB       3
#define BRUSH_COMPONENTS_BGR       4
#define BRUSH_COMPONENTS_RGB_HALF  5
#define BRUSH_COMPONENTS_YYY       6
	int brushComponents;

	float _altClipDiffThreshold = 0.f;
	float _origValDiffThreshold = 0.f;

	// This is a HACK - tracks the state of the accept reject buttons
	// in the Paint tool GUI!!
	bool _paintToolChangesArePending = false;

	/////////////////////////////////////////////////////////////////////////////

	int autoAlignBoxSize;

	// constants for converting RGB to YUV
#define YR ((float).25678906)
#define YG ((float).50412891)
#define YB ((float).09790625)
#define UR ((float)-.14822266)
#define UG ((float)-.29099219)
#define UB ((float).43921484)
#define VR ((float).43921484)
#define VG ((float)-.36778906)
#define VB ((float)-.07142578)

	// constants for converting YUV to RGB
#define RY ((float)1.1640625)
#define RU ((float) 0)
#define RV ((float)1.59765625 )
#define GY ((float)1.1640625)
#define GU ((float)-.390625 )
#define GV ((float)-.8125 )
#define BY ((float)1.1640625 )
#define BU ((float)2.015625)
#define BV ((float) 0)

	// color components (Color Mode)
	bool usingColor;
	float colors[3];
	MTI_UINT16 colorComponents[3];

	double preRadius;
	double preAspect;
	double preAngle;
	bool preShape;

#define CHORDS 256
	int nchords;
	int brushpts;
	double brush[CHORDS * 2];

	CBrush *cloneBrush;
	bool isRel;
	int cloneX;
	int cloneY;

	// weights for calculating luminance
	// these add up to 32768 (2**15)

#define RWGT  ((unsigned int) 6966)
#define GWGT  ((unsigned int)23436)
#define BWGT  ((unsigned int) 2366)
#define fRWGT  0.213F
#define fGWGT  0.715F
#define fBWGT  0.072F

	bool usingThresholds;
	int darkerThreshold;
	int lighterThreshold;

	double trgXctr;
	double trgYctr;

	int dspXctrWhenLastRotated;
	int dspYctrWhenLastRotated;
	bool rotatedSinceLastTranslation;

	RECT overlapRectangle;

	/////////////////////////////////////////////////////////////////////////////
	//
	// this variable always has the frame number of the last frame displayed

	int lastFrame;

	//// Inpainting (Healing Brush) /////////////////////////////////////////////

	MTI_INT32 *inpaintDelta;

	int begRowMask; // 1st row of mask to contain ones
	int endRowMask; // last row of mask to contain ones
	int begColMask; // 1st col of mask to contain ones
	int endColMask; // last col of mask to contain ones

	/////////////////////////////////////////////////////////////////////////////

	bool autoAccept;
	bool targetFrameWritten;

	// CAREFUL!!! THESE ARE DUPLICATED IN ToolSystemInterface.h !!!!!!!
#define IMPORT_MODE_NONE     0
#define IMPORT_MODE_ABSOLUTE 1
#define IMPORT_MODE_RELATIVE 2
#define IMPORT_MODE_ORIGINAL 3
#define IMPORT_MODE_ALTCLIP  4
	int importFrameMode;

	int desiredTargetFrame;
	int targetFrame;
	// int strokedTargetFrame;         // "stroked frame" hack
	int desiredImportFrame;
	int importFrame;
	// int strokedImportFrame;         // "stroked frame" hack
	int desiredImportFrameOffset;
	int importFrameOffset;

	bool forceTargetLoad;
	bool forceImportLoad;

	CSaveRestore *paintHistory;

	const MTI_UINT8 *paintMaskBuf;

	MTI_UINT16 *targetOrgIntBuf;
	MTI_UINT16 *targetIntBuf;
	MTI_UINT16 *targetPreIntBuf;

	// "frame diff" hack
	MTI_UINT16 *_currentFrameCompareBuf = nullptr;
	MTI_UINT16 *_importFrameCompareBuf = nullptr;
	int _currentFrameCompareBufFrameIndex = -1;
	int _importFrameCompareBufFrameIndex = -1;
	bool *_frameCompareOutputMask = nullptr;
	int _frameCompareFrameIndex1 = -1;
	int _frameCompareFrameIndex2 = -1;

	unsigned int *importProxy;
	int importProxyWdth, importProxyHght;

	MTI_UINT16 *importPreIntBuf;
	MTI_UINT16 *importIntBuf;

	// For ColorBreathing preview hack-o-rama
	MTI_UINT16 *previewHackIntBuf = nullptr;
   bool previewHackIntBufWasCudaMalloced = false;
	bool inPreviewHackMode = false;

	// int importWH;
	CImageFormat *ptImgFmt;
	double impRot;
	double impStrX;
	double impStrY;
	double impSkew;
	double impOff5X;
	double impOff5Y;

	// for tracking alignment mode:
	// A map to keep the x and y offsets that align the import frame
	// with a particular target frame.  Note that each map entry key
	// is the index of the **TARGET** frame, not IMPORT frame!
	// NOTE: I could've made the data type a DPOINT but I don't want
	// the size of the map to be unnecessarily doubled - probably should
	// define an FPOINT type
	map<int, std::pair<float, float> >trackingAlignOffsetsMap;
	bool useTrackingAlignOffsets;

	bool rotateExactReenterFlag; // tracking align hack flag

	CRotator *rotator;

	MTI_UINT16 *paintMdfdBuf;
	// WAS MTI_UINT16 *paintPreMdfdBuf;

#define MAX_STROKE_STACK_DEPTH 64
	int maxSavedStrokes;
	vector<Stroke>strokeStack;

	string pelsName;

	// no longer used
	// CPixelChannel *paintMdfdChannel;

	CStrokeChannel *paintPelsChannel;

	CPixelRegionList *originalPels;

	// CPixelRegionList *importDiffPels;

	CTransformEditor *transformEditor;

	/////////////////////////////////////////////////////////////////////////////

	int loadedRegistrationFrame;
	int registeredRegistrationFrame;

	MTI_UINT16 *preRegistrationBuf;
	bool preRegistrationBufWasMalloced;
	MTI_UINT16 *postRegistrationBuf;
	bool postRegistrationBufWasMalloced;

	RECT matBox;

	AffineCoeff curRegXform;

	CRegistrar *registrar;

	/////////////////////////////////////////////////////////////////////////////
	// For the shape-drawing (brush outline) hack

	int shapeHackRadius;
	int shapeHackAspectRatio;
	int shapeHackAngle;
	int shapeHackShapeType; // 0 = Ellipse, 1 = Rectangle
	int shapeHackNumChords;
#define MAX_SHAPE_HACK_VERTICES 256
	double shapeHackVertices[MAX_SHAPE_HACK_VERTICES * 2]; // x & y per vertex

	int hackTheColor(MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B);

	/////////////////////////////////////////////////////////////////////////////
	// For wiper mode

	bool _inWiperMode = false;
	MTI_UINT16 *_frameCompareIntBuf = nullptr;

	/////////////////////////////////////////////////////////////////////////////
	// for testing the various frame-formats

	unsigned char *tstBuf[2];
	int tstPitch;

	MTI_UINT16 *extrBuf;

	CImageFormat* tstFmt;

	CLineEngine *tstEng;

	CExtractor *tstExtr;

	// mask for capturing lassoed regions
	// used for debug
	CMask *mask;

	MTI_UINT16 *mskbuf;

	// Temporary
	Ipp8uArray _overlayMask;

	void showMaskOverlay();
	bool _displayMaskOverlay = false;
	bool _displayMaskInterior = false;
	void *_vpComputeMaskThread = nullptr;

	OverlayDisplayList overlayDisplayList;

	////////////////////////////////////////////////////////////////////////////////

	void DumpAWholeBunchOfReallyUglyShit();

/////////////////////////////////////////////////////////////////////////////


	// To be moved
	bool _newMaskAvailable = false;
	bool _maskComputeRunning = false;

	bool ComputeOverlayInBackground();
	Ipp8uArray _newOverlayMask;

	MaskParameters _computedMaskParameters;

public:
	void computeBorderOverlayMask();

	void setNewMaskAvailable(bool value) {_newMaskAvailable = value;}

	bool getNewMaskAvailable() const {return _newMaskAvailable;}

	void setMaskComputeRunning(bool value) {_maskComputeRunning = value;}

	bool getMaskComputeRunning() const {return _maskComputeRunning;}
};
#endif
