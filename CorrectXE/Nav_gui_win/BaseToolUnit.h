//---------------------------------------------------------------------------

#ifndef BaseToolUnitH
#define BaseToolUnitH
//---------------------------------------------------------------------------

#include "BaseTool.h"
#include "ToolSystemInterface.h"
#include "MTIUNIT.h"
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ToolWin.hpp>
#include "VTimeCodeEdit.h"
#include <Graphics.hpp>
#include <ImgList.hpp>
#include <Vcl.OleCtrls.hpp>
#include "ColorPanel.h"
#include <Vcl.Menus.hpp>
#include <System.ImageList.hpp>
#include "MaskTool.h"
#include "CompactTrackEditFrameUnit.h"
#include "MaskPropertiesFrame.h"

#include <map>
using std::map;

//---------------------------------------------------------------------------
struct TabInfo
{
    string ToolName;
    int TabPriority;
    int *TopPanel; // Really TPanel*
    FosterParentEventHandlerT EventHandler;
    bool Display;
};

//---------------------------------------------------------------------------

class TBaseToolWindow : public TMTIForm
{
__published:	// IDE-managed Components
    TTimer *MouseInToolTimer;
	TColorPanel *BaseToolBackgroundPanel;
        TPanel *ToolAdopterPanel;
        TLabel *NoToolSelectedLabel;
        TTabControl *ToolTabs;
    TPanel *SuperPanel;
    TImage *SuperImage;
    TPanel *TNLPanel;
    TLabel *ToolUnlicensedLabel1;
    TLabel *ToolUnlicensedLabel2;
    TPanel *MCPanel;
    TLabel *MasterClipLabel1;
   TLabel *MasterClipLabel4;
   TLabel *MasterClipLabel3;
    TPanel *NHPanel;
    TLabel *NonHistoryLabel1;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TPanel *TNAPanel;
    TLabel *ToolUnavailableLabel1;
    TLabel *ToolUnavailableLabel2;
        TImageList *LoadLightImageList;
    TPanel *NCPanel;
    TLabel *NoClipLabel1;
    TLabel *NoClipLabel2;
        TPanel *TIDPanel;
        TLabel *ToolDisabledLabel1;
        TLabel *ToolDisabledLabel2;
        TPanel *VOMPanel;
        TLabel *ViewOnlyModeLabel1;
        TLabel *ViewOnlyModeLabel2;
	TColorPanel *LoadCoverPanel;
        TImage *LoadLightImage;
	TImageList *ImageList1;
   TPanel *ToolbarPanel;
	TFlowPanel *MaskToolbarPanel;
   TSpeedButton *Mask_DrawRectButton;
   TSpeedButton *Mask_DrawLassoButton;
   TSpeedButton *Mask_DrawBezierButton;
   TSpeedButton *Mask_SetExclusiveButton;
   TSpeedButton *Mask_SetInclusiveButton;
   TSpeedButton *Mask_NeutralizeButton;
   TSpeedButton *Mask_ActivateRoiModeButton;
   TSpeedButton *Mask_SetKeyframeButton;
   TPanel *LutToolbarPanel;
   TSpeedButton *LutEditButton;
   TComboBox *LutToolbarComboBox;
   TMainMenu *MainMenu;
   TLabel *MasterClipLabel2;
   TSpeedButton *Mask_ShowOrHidePropertiesPanelButton;
	TMaskPropertiesFrame *MaskPropertiesFrame;
	TTimer *AnimateTimer;
   TSpeedButton *Mask_ActivateNormalModeButton;
   TSpeedButton *Mask_ShowOrHideOutlineButton;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormCreate(TObject *Sender);
    void __fastcall ToolTabsChange(TObject *Sender);
    void __fastcall ToolTabsChanging(TObject *Sender, bool &AllowChange);
    void __fastcall FormActivate(TObject *Sender);
    void __fastcall FormPaint(TObject *Sender);
    void __fastcall FormDeactivate(TObject *Sender);
    void __fastcall FormMouseWheel(TObject *Sender, TShiftState Shift,
          int WheelDelta, TPoint &MousePos, bool &Handled);
    void __fastcall FormHide(TObject *Sender);
    void __fastcall MouseInToolTimerTimer(TObject *Sender);
    void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
    void __fastcall FormMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall MaskToolbarButtonClick(TObject *Sender);
   void __fastcall LutToolbarComboBoxKeyPress(TObject *Sender, System::WideChar &Key);
   void __fastcall LutEditButtonClick(TObject *Sender);
	void __fastcall AnimateTimerTimer(TObject *Sender);
	void __fastcall LutToolbarComboBoxSelect(TObject *Sender);

private:	// User declarations
    map<int, TabInfo> MasterTabInfoMap;

   int _KeyDownCount;
   int pluginStateEnabled;
   bool _oldMaskPropertiesPanelVisible = false;

   bool InformedToolActive;
   bool InformedMouseInTool;

   void showTool(int tabIndex);
   void hideTool(int tabIndex);
   TCheckBox *findToolActiveCheckBox(TPanel *panel);
   void flipToolActiveCheckBox(TPanel *panel, bool newChecked);
   int getTabIndexFromToolName(const string &toolName);
   int getMapIndexFromToolName(const string &toolName);
   int getToolIndexForActiveTab();
   TabInfo *getTabInfoForTabIndex(int tabIndex);
   TabInfo *getTabInfoForActiveTab();

   void RebuildTabs();

   bool IsItOkToSwitchTools();
   bool sendSimpleEvent(FosterParentEventId event);
   bool sendKeyEvent(FosterParentEventId event,
      TObject *Sender, WORD &Key, TShiftState Shift);
   bool sendMouseEvent(FosterParentEventId event,
      TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
   void passKeyDownEventToMainWindow(TObject *Sender, WORD &Key,
      TShiftState Shift);
   void passKeyUpEventToMainWindow(TObject *Sender, WORD &Key,
      TShiftState Shift);

   CMaskTool *_maskTool = nullptr;

protected:
   void __fastcall HandleWMMenuChar(TWMMenuChar &Message);
 	void __fastcall WMMoveCallback(TMessage &Message);

BEGIN_MESSAGE_MAP
   VCL_MESSAGE_HANDLER(WM_MENUCHAR, TWMMenuChar, HandleWMMenuChar)
   VCL_MESSAGE_HANDLER(WM_MOVE, TMessage, WMMoveCallback)
END_MESSAGE_MAP(TMTIForm)

public:		// User declarations
  __fastcall TBaseToolWindow(TComponent* Owner);

   void ShowHidePropertiesWindow();
   void ExecuteBaseToolCommand(int command);
   int AdoptPluginToolGUI(const string &toolName,
                          int tabIndex,
                          int *topPanel,         // Really a TPanel*
                          FosterParentEventHandlerT eventHandler);
   void UnadoptPluginToolGUI(int tabIndex);
   void FocusAdoptedControl(int *control);         // Really a TWinControl*
   int *GetFocusedAdoptedControl(void);

   void ShowTool(const string &toolName);
   void HideTool(const string &toolName);
   void ShowToolCompletely(const string &toolName);
   void HideToolCompletely(const string &toolName);
   void DisableTool(EToolDisabledReason reason);
   void EnableTool();
   bool WarnThereIsNoUndo();
   EAcceptOrRejectChangeResult ShowAcceptOrRejectDialog(bool *turnOnAutoAccept);

   int GetCurrentTabIndex();
   void SwitchToTab(int tabIndex);

   bool CoverUp(bool enable);
   bool ShowLoadProgress(int index);

   void UpdateMaskToolGUI(CMaskTool &maskTool);
   void SetMaskTool(CMaskTool &maskTool);
   bool HideMaskProperties(bool animate=false);
   bool ShowMaskProperties(bool animate=false);
   bool areMaskPropertiesVisible();
   void ShowHideProperties();

  private:
   int _oldClientHeight = 754;   // Doesn't really matter the value
   int _oldBaseTop = 32;
   int _targetHeight = 0;
};
//---------------------------------------------------------------------------
extern PACKAGE TBaseToolWindow *BaseToolWindow;
//---------------------------------------------------------------------------
#endif

