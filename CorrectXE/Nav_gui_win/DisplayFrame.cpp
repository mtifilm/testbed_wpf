#include "ImageFormat3.h"
#include "Displayer.h"
#include "Extractor.h"
#include "Convert.h"
#include "LineEngine.h"
#include "Magnifier.h"
#include "MaskTool.h"
#include "MaskTransformEditor.h"
#include "MainWindowUnit.h"
#include "mt_math.h"
#include "Player.h"
#include "NavSystemInterface.h"
#include "ToolManager.h"
#include "TransformEditor.h"
#include "PixelRegions.h"
#include "SaveRestore.h"
#include "GpuAccessor.h"

#pragma warn -8002
//-----------------------------------------------------------------------------

// using texture for decimation incurs flashover
// on context switching to and from FullWindow
//-----------------------------------------------------------------------------

// Display a native frame derived from the current clip
// Used by the Navigator. Assumes current dspRect, imgFmt,
// zoomLevel, etc.

// KT-060227 added pixel region list argument. The latter
// can now be drawn into the back buffer, and made visible
// by the call to SwapBuffers. This eliminates flicker.
// The pixel region list itself is now drawn with only
// one call to glFlush at the end, eliminating the slowness.
// see Bugzilla 2400.
//-----------------------------------------------------------------------------

void CDisplayer::displayFrame(MediaFrameBufferSharedPtr frameBuffer,
										int frame,
										bool fast,
										CPixelRegionList *pxlrgnlst,
										bool targetMayHaveChanged)
{
	if (displayFrameNoSwap(frameBuffer, frame, fast, pxlrgnlst, targetMayHaveChanged))
	{
		setGraphicsBuffer(GL_FRONT);
		SwapBuffers(screenDC);
	}
}
//-----------------------------------------------------------------------------

// FULL HACK MODE!! haha!
void CDisplayer::displayFrameCalledFromPlayer(
										MediaFrameBufferSharedPtr frameBuffer,
										int frame,
										bool fast,
										CPixelRegionList *pxlrgnlst,
										bool targetMayHaveChanged)
{
	if (displayFrameNoSwap(frameBuffer, frame, fast, pxlrgnlst, targetMayHaveChanged))
	{
		setGraphicsBuffer(GL_FRONT);
		SwapBuffers(screenDC);
	}
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrameWithWiper(
	MediaFrameBufferSharedPtr frameBuffer,
	MediaFrameBufferSharedPtr wiperFrameBuffer,
	int frame,
	bool fast,
	CPixelRegionList *pxlrgnlst,
	bool targetMayHaveChanged,
	DPOINT dividerRefPoint,
	float dividerAngleInDegrees)
{
	if (!displayFrameNoSwap(frameBuffer, frame, fast, pxlrgnlst, targetMayHaveChanged))
	{
		// We're in FUCKING PAINT RANGE MACRO mode, which drew the target frame,
		// then swapped. Need to swap it back to do the overlay, which is going
		// to cause flashing. The proper hack is to inhibit the paint swap when
		// we're in split screen or side-by-side mode.
		SwapBuffers(screenDC);
		setGraphicsBuffer(GL_BACK);
	}

	// Compute the "divider line" table, which are one x offset per row where
	// the frame display switches from original frame to modified frame.
	int refX = rgbRect.left + int(rgbRectWidth * dividerRefPoint.x);
	int refY = rgbRect.top + int(rgbRectHeight * dividerRefPoint.y);
	int clampedX = std::max<int>(rgbRect.left, std::min<float>(rgbRect.right, refX));
	int clampedY = std::max<int>(rgbRect.top, std::min<float>(rgbRect.bottom, refY));
	int dividerXPerRow[4096];
	float dx = Degrees2Cosine(dividerAngleInDegrees);
	float dy = Degrees2Sine(dividerAngleInDegrees);
	int minX = rgbRect.left;
	int minY = rgbRect.top;
	int maxX = rgbRect.right;
	int maxY = rgbRect.bottom;

	if (dy < 0.00005)
	{
		// Horizontal wiper case
		int y = 0;
		int switchRow = clampedY - rgbRect.top;
		for (; y < rgbRectHeight; ++y)
		{
			// Above the line show orig.
			// Below the line show modified.
			dividerXPerRow[y] = (y < switchRow) ? rgbRectWidth : 0;
		}

		minY = maxY = clampedY;
	}
	else if (dx < 0.00005)
	{
		// Vertical wiper case, show orig to left of line.
		int x = clampedX - rgbRect.left;
		for (int y = 0; y < rgbRectHeight; ++y)
		{
			dividerXPerRow[y] = x;
		}

		minX = maxX = clampedX;
	}
	else
	{
		int x = clampedX + int(((clampedY - minY) / dy) * dx);
		if (x > maxX)
		{
			minY = std::max<float>(minY, clampedY - ((maxX - clampedX) / dx) * dy);
		}
		else
		{
			maxX = x;
		}

		x = clampedX - int(((maxY - clampedY) / dy) * dx);
		if (x < minX)
		{
			maxY = std::min<float>(maxY, clampedY + int((clampedX - minX) / dx) * dy);
		}
		else
		{
			minX = x;
		}

		int y = 0;
		for (; y < (minY - rgbRect.top); ++y)
		{
			// Above/left of the line show orig.
			dividerXPerRow[y] = rgbRectWidth;
		}

		for (; y < (maxY - rgbRect.top); ++y)
		{
			x = minX + int(((maxY - rgbRect.top - y) / dy) * dx);
			dividerXPerRow[y] = x - rgbRect.left;
		}

		for (; y < rgbRectHeight; ++y)
		{
			// Below/right of the line show modified.
			dividerXPerRow[y] = 0;
      }
	}

	displayWiperFrame(wiperFrameBuffer, dividerXPerRow);

	glLineWidth(3);
	glDisable(GL_LINE_STIPPLE);
	glColor4ub(0x0F, 0x0F, 0x0F, 0x0F);
	moveToClient(minX + 1, maxY + 1);
	lineToClientNoFlush(maxX + 1, minY + 1);
	glColor4ub(0xF0, 0xF0, 0xF0, 0x7F);
	moveToClient(minX, maxY);
	lineToClientNoFlush(maxX, minY);
	glLineWidth(1);

	// Redirect line and rectangle draw operations to the front buffer.
	setGraphicsBuffer(GL_FRONT);

	// Update the screen.
	SwapBuffers(screenDC);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFramesSideBySide(
	MediaFrameBufferSharedPtr frameBuffer,
	MediaFrameBufferSharedPtr sideBySideFrameBuffer,
	int frame,
	bool fast,
	CPixelRegionList *pxlrgnlst,
	bool targetMayHaveChanged)
{
	if (!frameBuffer || !frameBuffer->getImageDataPtr())
	{
		return;
	}

	clearBackBuffer();

	// Display the right side frame.
	// For some stupid reason, this has to be done first. QQQ
	if (!displayFrameNoSwap(frameBuffer, frame, fast, pxlrgnlst, targetMayHaveChanged))
	{
		// We're in FUCKING PAINT RANGE MACRO mode, which drew the target frame,
		// then swapped. Need to swap it back to do the overlay, which is going
		// to cause flashing. The proper hack is to inhibit the paint swap when
		// we're in split screen or side-by-side mode.
		SwapBuffers(screenDC);
		setGraphicsBuffer(GL_BACK);
	}

	// Display the left side pcture.
	unsigned char *fld[2] = {sideBySideFrameBuffer->getImageDataPtr(), nullptr};
   CImageFormat frameImageFormat = frameBuffer->getImageFormat();
	_compareFrameConverter->displayConvert(
										fld,
										&frameImageFormat,
										&dspRect,
										screenFrameBuffer,
										rgbRectWidth,
										rgbRectHeight,
										rgbRectWidth*4,
										currentLUTPtr,
										RGBChannelsMask);

	blitSideBySidePictureToBackBuffer(screenFrameBuffer);

	// Redirect line and rectangle draw operations to the front buffer.
	setGraphicsBuffer(GL_FRONT);

	// Update the screen.
	SwapBuffers(screenDC);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrameFromFields(MediaFrameBufferSharedPtr frameBuffer)
{
	if (!frameBuffer || !frameBuffer->getImageDataPtr())
	{
		return;
	}

	unsigned char *fld[2] = {frameBuffer->getImageDataPtr(), nullptr};
   CImageFormat frameImageFormat = frameBuffer->getImageFormat();

	// turn the native frame into RGBA
	_mainFrameConverter->displayConvert(
									fld,
                           &frameImageFormat,
									&dspRect,
									screenFrameBuffer,
									rgbRectWidth,
									rgbRectHeight,
									rgbRectWidth*4,
									currentLUTPtr,
									RGBChannelsMask);

	clearBackBuffer();
	blitPictureToBackBuffer(screenFrameBuffer);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayWiperFrame(MediaFrameBufferSharedPtr wiperFrameBuffer, int *dividerIndices)
{
	if (!wiperFrameBuffer || !wiperFrameBuffer->getImageDataPtr())
	{
		return;
	}

	unsigned char *fld[2] = {wiperFrameBuffer->getImageDataPtr(), nullptr};

	// turn the native frame into RGBA
   CImageFormat frameImageFormat = wiperFrameBuffer->getImageFormat();
	_compareFrameConverter->displayConvert(
									fld,
                           &frameImageFormat,
									&dspRect,
									screenFrameBuffer,
									rgbRectWidth,
									rgbRectHeight,
									rgbRectWidth*4,
									currentLUTPtr,
									RGBChannelsMask,
									0,
									true,
									dividerIndices);

	clearBackBuffer();
	blitPictureToBackBuffer(screenFrameBuffer);
}
//-----------------------------------------------------------------------------

// [Called from displayFrameNoSwap() in non-paint case]
void CDisplayer::displayFrameWithDisplayBufferPreview(int frameNumber, MediaFrameBufferSharedPtr frameBuffer)
{
	// TRACE_3(errout << "^v^v^v^v^v^v^v^v DFFF 1 ^v^v^v^v^v^v^v^v");
	if (!frameBuffer)
	{
		return;
	}

	auto localFrameBuffer = static_cast<MediaFrameBufferWithSharedData*>(frameBuffer.get());
	CImageFormat imageFormat = localFrameBuffer->getImageFormat();

	////////////////////////////////////////////////////////////////////////
	// QQQ Had to remove this because getImageDataSize() is wrong for Y->YYY
	// MTIassert(imageFormat.getBytesPerField() <= frameBuffer->getImageDataSize());
	////////////////////////////////////////////////////////////////////////

	unsigned char *stupidBuf[2] = {frameBuffer->getImageDataPtr(), nullptr};
   CImageFormat frameImageFormat = frameBuffer->getImageFormat();

	// turn the native frame into RGBA
	_mainFrameConverter->displayConvert(
									stupidBuf,
                           &frameImageFormat,
									&dspRect,
									screenFrameBuffer,
									rgbRectWidth,
									rgbRectHeight,
									rgbRectWidth*4,
									currentLUTPtr,
			RGBChannelsMask);

	CToolManager toolManager;
	toolManager.onPreviewDisplayBufferDraw(frameNumber, rgbRectWidth, rgbRectHeight, screenFrameBuffer);

	// TRACE_3(errout << "^v^v^v^v^v^v^v^v DFFF 2 ^v^v^v^v^v^v^v^v");
	clearBackBuffer();
	blitPictureToBackBuffer(screenFrameBuffer);
	// TRACE_3(errout << "^v^v^v^v^v^v^v^v DFFF 3 ^v^v^v^v^v^v^v^v");
	// TRACE_3(errout << "After blitPictureToBackBuffer 2");
}
//-----------------------------------------------------------------------------

// [Called from displayFrameNoSwap() in non-paint case]
void CDisplayer::displayFrameWithHackPreview(int frameNumber, MediaFrameBufferSharedPtr frameBuffer)
{
	// Allocate the buffer if not done already
	if (previewHackIntBuf == nullptr)
	{
	   GpuAccessor gpuAccessor;
      if (gpuAccessor.useGpu())
      {
         previewHackIntBuf = (MTI_UINT16*)MtiCudaMallocHost(frameInternalSize);
         previewHackIntBufWasCudaMalloced = true;
      }

      if (previewHackIntBuf == nullptr)
      {
	   	previewHackIntBuf = (MTI_UINT16*)MTImalloc(frameInternalSize);
         previewHackIntBufWasCudaMalloced = false;
      }
	}

	MTI_UINT8 *stupidBuf[2] = { frameBuffer->getImageDataPtr(), nullptr };

	// Convert the native frame to intermediate format
   CImageFormat frameImageFormat = frameBuffer->getImageFormat();
	extr->extractActivePicture(previewHackIntBuf, stupidBuf, imgFmt, &frameImageFormat);

	// Let the active tool modify the frame
	CToolManager toolManager;
	toolManager.onPreviewHackDraw(frameNumber, previewHackIntBuf, frameInternalSize);

	// Draw the modified frame
	displayIntermediate(previewHackIntBuf);
}
//-----------------------------------------------------------------------------

// Called from displayFrameAndBrush(), redrawTargetFrame(), displayFrameNoSwap(),
//
void CDisplayer::displayFrameFromTargetBuf()
{
	// convert processed target to RGB
	displayIntermediate(targetIntBuf);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrameAndBrush()
{
	displayFrameFromTargetBuf();

	// next graphics go to the back
	setGraphicsBuffer(GL_BACK);

	int playbackFilter = mainWindow->GetPlayer()->getPlaybackFilter();
	if (playbackFilter == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS && captureMode == CAPTURE_MODE_TARGET)
	{
		// In the following, don't round up!
		MTI_UINT16 diffThreshold = (MTI_UINT16)(_altClipDiffThreshold * getMaxComponentValue());
		bool *overlay = CompareImportAndTargetFrames(diffThreshold, true);
		int savedHighlightColor = highlightColor;
		highlightColor = SOLID_RED;
		drawMaskOverlayFrame(overlay);
		highlightColor = savedHighlightColor;
	}
	else if ((playbackFilter & PLAYBACK_FILTER_IMPORT_FRAME_DIFFS) != 0 && captureMode == CAPTURE_MODE_ORIG_TARGET)
	{
		// In the following, don't round up!
		MTI_UINT16 diffThreshold = (MTI_UINT16)(_origValDiffThreshold * getMaxComponentValue());
		bool *overlay = CompareImportAndTargetFrames(diffThreshold, true);
		int savedHighlightColor = highlightColor;
		highlightColor = SOLID_MAGENTA;
		drawMaskOverlayFrame(overlay);
		highlightColor = savedHighlightColor;
	}

	// if brushes are loaded redraw them
	if (displayProcessed && brushEnabled)
	{
		if ((autoAccept || (lastFrame == targetFrame)) && ((minorState == ACQUIRING_PIXELS) ||
				(minorState == PAINTING_PIXELS)))
		{
			if (paintBrush != NULL)
			{
				if (brushMode == BRUSH_MODE_THRU)
				{
					drawBrushFrame(paintBrush, SOLID_GREEN, dxcurfrm, dycurfrm);
				}
				else if (brushMode == BRUSH_MODE_ERASE)
				{ // was SOLID_YELLOW
					drawBrushFrame(paintBrush, SOLID_RED, dxcurfrm, dycurfrm);
				}
				else if (brushMode == BRUSH_MODE_ORIG)
				{
					drawBrushFrame(paintBrush, SOLID_YELLOW, dxcurfrm, dycurfrm);
				}
			}
		}
		if ((autoAccept || (lastFrame == targetFrame)) || (lastFrame == importFrame))
		{
			if ((cloneBrush != NULL) && (brushMode == BRUSH_MODE_THRU))
			{
				if (!isRel)
				{
					drawBrushFrame(cloneBrush, SOLID_RED, cloneX, cloneY);
				}
				else
				{
					drawBrushFrame(cloneBrush, SOLID_RED, dxcurfrm + cloneX, dycurfrm + cloneY);
				}
			}
		}
	}

	// redirect line and rectangle draw operations to FRONT
	setGraphicsBuffer(GL_FRONT);

	// everything's on the BACK surface - make it visible
	SwapBuffers(screenDC);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrameFromImportBuf()
{
	// loadImportFrame(desiredImportFrame);
	// importFrame = desiredImportFrame;
	// importFrameOffset = desiredImportFrameOffset;

	displayIntermediate(importIntBuf);
}
//-----------------------------------------------------------------------------

void CDisplayer::redrawTargetFrame()
{
	displayFrameFromTargetBuf();

	// switch the target of line and rectangle
	// draw operations to the BACK buffer for
	// displaying the next series of graphics
	setGraphicsBuffer(GL_BACK);

	// if reticle is enabled, redraw it
	if (currentReticle.getType() != RETICLE_TYPE_NONE)
	{
		drawReticle();
	}

	// THIS SUCKS: We know here that this function is ONLY CALLED while
	// executing RANGE MACRO, and since we don't want to see any cursors
	// at all during that time, dont't even consider drawing them!!
	// Hey it seems to be trying to draw them in the wrong color anyhow!!

#ifdef DRAW_CURSOR_WHILE_EXECUTING_RANGE_MACRO

	// if brushes are loaded redraw them
	if (displayProcessed && brushEnabled)
	{
		if (true && ((minorState == ACQUIRING_PIXELS) || (minorState == PAINTING_PIXELS)))
		{
			if (paintBrush != NULL)
			{
				// WTF??!? Why isn't this SOLID_GREEN ??
				drawBrushFrame(paintBrush, SOLID_RED, dxcurfrm, dycurfrm);
			}
		}
		if (true)
		{
			if ((cloneBrush != NULL) && (brushMode == BRUSH_MODE_THRU))
			{
				if (!isRel)
				{
					// WTF??!? Why isn't this SOLID_RED ?? You're right!
					drawBrushFrame(cloneBrush, SOLID_RED, cloneX, cloneY);
				}
				else
				{
					// WTF??!? Why isn't this SOLID_RED ?? You're right!
					drawBrushFrame(cloneBrush, SOLID_RED, dxcurfrm + cloneX, dycurfrm + cloneY);
				}
			}
		}
	}
#endif  // draw cursors

	if (displayProcessed && minorState == PICKING_AUTOALIGN)
	{
		drawAutoAlignBoxFrame(SOLID_YELLOW, dxcurfrm, dycurfrm);
	}

	// redirect line and rectangle draw operations to FRONT
	setGraphicsBuffer(GL_FRONT);

	// everything's on the BACK surface - make it visible
	SwapBuffers(screenDC);
}
//-----------------------------------------------------------------------------

void CDisplayer::displayImportOverTarget(int onionskenbld)
{
	MTI_UINT16 *input = (displayProcessed ? targetIntBuf : targetOrgIntBuf);

	// convert processed target to RGB
	_mainFrameConverter->displayConvertIntermediate(
												input,
												imgFmt,
												&dspRect,
												screenFrameBuffer,
												rgbRectWidth,
												rgbRectHeight,
												rgbRectWidth*4,
												currentLUTPtr,
												RGBChannelsMask,
												onionskenbld&PNMASK_POS,
												onionSkinTargetWgt );

	clearBackBuffer();
	blitPictureToBackBuffer(screenFrameBuffer);

	// convert processed import to RGB
	_mainFrameConverter->displayConvertIntermediate(
												importIntBuf,
												imgFmt,
												&dspRect,
												screenFrameBuffer,
												rgbRectWidth,
												rgbRectHeight,
												rgbRectWidth*4,
												currentLUTPtr,
												RGBChannelsMask,
												onionskenbld&PNMASK_NEG,
												255 - onionSkinTargetWgt );

	blendPictureToBackBuffer(screenFrameBuffer);
}
//-----------------------------------------------------------------------------

bool CDisplayer::displayFrameNoSwap(MediaFrameBufferSharedPtr frameBuffer,
												int frame,
												bool fast,
												CPixelRegionList *pxlrgnlst,
												bool targetMayHaveChanged)
{
	// void CDisplayer::displayFrame(unsigned char **fld, int frame, bool fast,
	// CPixelRegionList *pxlrgnlst,
	// bool targetMayHaveChanged)
	// {
	// if (tstBuf[0] != NULL) // then we're just testing
	// fld = tstBuf;
	//
	// lastDisplayFields[0] = frameBuffer;
	// lastDisplayFields[1] = fld[1];

	if (!frameBuffer)
	{
		return false;
	}

//	if (currentTool == CURRENT_TOOL_PAINT
//	&& frame == targetFrame
//	&& targetIntBuf != nullptr
//	&& mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off
//	&& !fast)
//	{
//		if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
//		{
//			desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(frame + importFrameOffset,
//					_importClipFrameList);
//		}
//		else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
//		{
//			importFrameOffset = desiredImportFrame - frame;
//		}
//		else if (importFrameMode == IMPORT_MODE_ORIGINAL)
//		{
//			desiredImportFrame = frame;
//			importFrameOffset = 0;
//		}
//
//		transformEditor->transmitImportFrameTimecode(desiredImportFrame);
//		transformEditor->transmitImportFrameOffset(importFrameOffset);
//
//		if ((targetFrame == -1) || (targetFrame != desiredTargetFrame) || !targetFrameIsModified())
//		{
//			desiredTargetFrame = frame;
//			loadTargetFrame(desiredTargetFrame);
//			targetFrame = desiredTargetFrame;
//		}
//
//		displayIntermediate(targetIntBuf);
//
//		return;
//	}

	_lastDisplayedFrameBuffer = frameBuffer;

	int playbackFilter = mainWindow->GetPlayer()->getPlaybackFilter();

	bool showRedBlobsForDiffs = currentTool == CURRENT_TOOL_PAINT
								&& playbackFilter == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS
								&& captureMode == CAPTURE_MODE_TARGET;

   bool showMagentaBlobsForDiffs = currentTool == CURRENT_TOOL_PAINT
                        && playbackFilter == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS
								&& captureMode == CAPTURE_MODE_ORIG_TARGET;

	MTI_UINT16 *currentFrameComparand = nullptr;
	MTI_UINT16 *importFrameComparand = nullptr;
	int currentFrameComparandIndex = -1;
	int importFrameComparandIndex = -1;

	lastFrame = frame;
	bool doingFrameCompare = mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off;

	switch (currentTool)
	{
		case CURRENT_TOOL_NAVIGATOR: // normal pathway
		{
			// // mbraca says: sometimes fld[0] is NULL here, don't kow why, and
			// // don't know if it's a problem in the paintEnabled case!
			// if (fld == NULL || fld[0] == NULL)
			// return;
			if (inPreviewHackMode && !imgFmt->getInterlaced())
			{
				displayFrameWithHackPreview(lastFrame, _lastDisplayedFrameBuffer);
			}
			else
			{
				displayFrameWithDisplayBufferPreview(lastFrame, _lastDisplayedFrameBuffer);
			}

			break;
		}

		case CURRENT_TOOL_PAINT: // paint enabled
		{
			if (forceTargetLoad)
			{
				// allocates buffers on 1st call
				desiredTargetFrame = frame;
				loadTargetFrame(desiredTargetFrame);
				targetFrame = desiredTargetFrame;

				// If we are in tracking alignment mode, it seems we have to
				// do something to make the right import frame be selected
				// and transformed...
				if (forceImportLoad || showRedBlobsForDiffs || showMagentaBlobsForDiffs)
				{

					if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
					{
						desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(frame + importFrameOffset,
								_importClipFrameList);
						fetchImportFrame(desiredImportFrame);
						importFrame = desiredImportFrame;
					}
					else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
					{
						importFrameOffset = desiredImportFrame - frame;
						if (importFrame != desiredImportFrame)
						{
							fetchImportFrame(desiredImportFrame);
							importFrame = desiredImportFrame;
						}
					}
					else if (importFrameMode == IMPORT_MODE_ORIGINAL)
					{
						importFrameOffset = 0;
						desiredImportFrame = frame;
						fetchImportFrame(desiredImportFrame);
						importFrame = desiredImportFrame;
					}
				}

				// When using forceTargetLoad, don't bother
				// drawing the frame after loading it -- it
				// will be drawn AFTER the MACRO is executed
				// on it. Ergo -- just call the tool method
				CToolManager toolManager;
				toolManager.onRedraw(frame);
				return false;
			}

			if ((showRedBlobsForDiffs || showMagentaBlobsForDiffs) && !doingFrameCompare)
			{
				if (frame == desiredTargetFrame)
				{
					currentFrameComparand = targetIntBuf;
					currentFrameComparandIndex = desiredTargetFrame;
				}
				else if (_currentFrameCompareBuf)
				{
					currentFrameComparand = _currentFrameCompareBuf;
					currentFrameComparandIndex = frame;
					if (frame != _currentFrameCompareBufFrameIndex)
					{
						MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
                  CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
						extr->extractActivePicture(_currentFrameCompareBuf, stupidBuf, imgFmt, &frameImageFormat);
						_currentFrameCompareBufFrameIndex = frame;
					}
				}

				MTIassert(importFrameMode != IMPORT_MODE_ABSOLUTE);
				MTIassert(importFrameMode != IMPORT_MODE_RELATIVE);
				int importFrameIndex = -1;
				if (importFrameMode == IMPORT_MODE_ALTCLIP)
				{
					importFrameIndex = mainWindow->GetPlayer()->checkTimecodeLimits(frame + importFrameOffset,
							_importClipFrameList);
				}
				else if (importFrameMode == IMPORT_MODE_ORIGINAL)
				{
					importFrameIndex = frame;
				}

				if (importFrameIndex == desiredImportFrame)
				{
					if (importFrame != desiredImportFrame)
					{
						fetchImportFrame(desiredImportFrame);
						importFrame = desiredImportFrame;
					}

					importFrameComparand = importIntBuf;
					importFrameComparandIndex = desiredImportFrame;
				}
				else if (_importFrameCompareBuf)
				{
					importFrameComparand = _importFrameCompareBuf;
					importFrameComparandIndex = importFrameIndex;
					if (importFrameIndex != _importFrameCompareBufFrameIndex)
					{
						if (importFrameMode == IMPORT_MODE_ALTCLIP)
						{
							if (importFrameIndex >= 0)
							{
								mainWindow->GetPlayer()->getToolFrame(_importFrameCompareBuf, importFrameIndex, _importClipFrameList);
							}
						}
						else if (importFrameMode == IMPORT_MODE_ORIGINAL)
						{
							MTImemcpy(_importFrameCompareBuf, _currentFrameCompareBuf, frameInternalSize);
							int retVal = createOriginalValuesFrame(_importFrameCompareBuf, importFrameIndex);
							if (retVal != 0)
							{
								importFrameComparand = nullptr;
							}
						}

						_importFrameCompareBufFrameIndex = importFrameIndex;
					}
				}
			}

			// with the Player set up for subsampled playback
			// and panning, the fast argument will be true
			// whenever the navigator is playing a sequence
			// of frames or the user is panning. It will be false
			// when the Navigator is stopped on a frame, in
			// which event the target and reference frames for
			// Paint can be grabbed inconspicuously. We do this
			// to make the "ready-to-paint" feature transparent

			if (captureMode == CAPTURE_MODE_TARGET || doingFrameCompare)
			{
				// in this state the user is not holding CTRL-SHIFT
				// down, and is navigating to the target frame

				if (onionSkinEnabled == ONIONSKIN_MODE_OFF || doingFrameCompare)
				{
					// onionskin mode OFF
					if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
					{
						desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(frame + importFrameOffset,
								_importClipFrameList);
					}
					else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
					{
						importFrameOffset = desiredImportFrame - frame;
					}
					else if (importFrameMode == IMPORT_MODE_ORIGINAL)
					{
						desiredImportFrame = frame;
						importFrameOffset = 0;
					}

					transformEditor->transmitImportFrameTimecode(desiredImportFrame);
					transformEditor->transmitImportFrameOffset(importFrameOffset);

					// WAS if (!fast&&((targetFrame == -1)||!targetFrameIsModified())
					if (!fast)
					{
						// => stopping on a frame
						if ((targetFrame == -1) || (targetFrame != desiredTargetFrame) || !targetFrameIsModified())
						{

							desiredTargetFrame = frame;

							// THIS HAD BEEN REMOVED FOR SPEED
							loadTargetFrame(desiredTargetFrame);
							targetFrame = desiredTargetFrame;

	#ifdef  USE_TIMER
							// this zeroes the settling timer so that load would
							// be automatic after N ticks. Not used right now
							transformEditor->setFrameLoadTimer();
	#endif
						}
					}

					// if on target frame
					// if displaying processed target
					// do target
					// else if displaying original
					// do original
					// else if off target do
					// display from fields
					if (frame == targetFrame)
					{
						if (displayProcessed || doingFrameCompare)
						{
							// displaying processed
							// convert processed target to RGB
							displayIntermediate(targetIntBuf);
						}
						else
						{ // displaying original

							// when a View option is selected, it can be seen
							// by selecting "Original" from the GUI. Bugz 2605
							displayFrameFromFields(_lastDisplayedFrameBuffer);
						}
					}
					else
					{
						displayFrameFromFields(_lastDisplayedFrameBuffer);
					}
				}
				else
				{
					// onionskin mode ON
					// if on target frame
					// displayImportOverTarget(onionskinEnabled)
					// else
					// display from fields

					if (frame == targetFrame)
					{
						displayImportOverTarget(onionSkinEnabled);
					}
					else
					{
						displayFrameFromFields(_lastDisplayedFrameBuffer);
					}
				}
			} // end PAINT_STATE_CAPTURE_TARGET

			else if (captureMode == CAPTURE_MODE_IMPORT)
			{
				// in this state the user is holding CTRL-SHIFT
				// down, and is navigating to the import frame

				desiredImportFrame = frame;
				desiredImportFrameOffset = desiredImportFrame - targetFrame;

				if (onionSkinEnabled == ONIONSKIN_MODE_OFF)
				{
					if (!fast)
					{ // only here do the load

						// allocates buffers on 1st call
						loadImportFrame(desiredImportFrame);
						importFrame = desiredImportFrame;
						importFrameOffset = desiredImportFrameOffset;
					}

					displayFrameFromFields(_lastDisplayedFrameBuffer);
				}
				else
				{ // onionskin mode ON

					// allocates buffers on 1st call
					loadImportFrame(desiredImportFrame);
					importFrame = desiredImportFrame;
					importFrameOffset = desiredImportFrameOffset;

					displayImportOverTarget(onionSkinEnabled);
				}

				transformEditor->transmitImportFrameTimecode(desiredImportFrame);
				transformEditor->transmitImportFrameOffset(desiredImportFrameOffset);
			}

			else if (captureMode == CAPTURE_MODE_BOTH)
			{
				// in this state the user IS holding CTRL-SHIFT down
				// and 'TrackAlign' is ON, and the user navigating to
				// the target frame while showing both the target and
				// the import frames combined according to the onion
				// skin mode

				// Designate the current frame as the target frame
				desiredTargetFrame = frame;
				if (targetFrame != desiredTargetFrame)
				{
					loadTargetFrame(desiredTargetFrame);
					targetFrame = desiredTargetFrame;
				}

				// Find the corresponding import frame
				if (importFrameMode == IMPORT_MODE_RELATIVE || importFrameMode == IMPORT_MODE_ALTCLIP)
				{
					desiredImportFrameOffset = importFrameOffset;
					desiredImportFrame = mainWindow->GetPlayer()->checkTimecodeLimits(frame + desiredImportFrameOffset,
							_importClipFrameList);
				}
				else if (importFrameMode == IMPORT_MODE_ABSOLUTE)
				{
					desiredImportFrame = importFrame;
					desiredImportFrameOffset = importFrame - frame;
				}
				else if (importFrameMode == IMPORT_MODE_ORIGINAL)
				{
					desiredImportFrame = frame;
					desiredImportFrameOffset = 0;
				}

				if (importFrame != desiredImportFrame)
				{
					fetchImportFrame(desiredImportFrame);

					importFrame = desiredImportFrame;
					importFrameOffset = desiredImportFrameOffset;

					makeAndTransmitProxy();
				}

				// display the combo
				if (onionSkinEnabled == ONIONSKIN_MODE_OFF)
				{
					if (displayProcessed)
					{
						// display target buf
						displayFrameFromTargetBuf();
					}
					else
					{
						// displaying original
						displayFrameFromFields(_lastDisplayedFrameBuffer);
					}
				}
				else
				{
					// onionskin mode ON
					displayImportOverTarget(onionSkinEnabled);
				}

				// update the paint tool gui
				transformEditor->transmitImportFrameTimecode(desiredImportFrame);
				transformEditor->transmitImportFrameOffset(desiredImportFrameOffset);

			} // end PAINT_STATE_CAPTURE_BOTH

			else if (captureMode == CAPTURE_MODE_ORIG_IMPORT)
			{
				// in this state the user is holding CTRL-SHIFT
				// down to see the Original Values frame.

				desiredTargetFrame = frame;
				desiredImportFrame = frame;
				desiredImportFrameOffset = 0;

				if (!fast)
				{
					// stopping on the frame
					if (targetFrame != desiredTargetFrame)
					{
						loadTargetFrame(desiredTargetFrame);
						targetFrame = desiredTargetFrame;
					}

					fetchImportFrame(desiredImportFrame);
					importFrame = desiredImportFrame;
					importFrameOffset = desiredImportFrameOffset;

					if (onionSkinEnabled == ONIONSKIN_MODE_OFF)
					{

						displayFrameFromImportBuf();
					}
					else
					{

						displayImportOverTarget(onionSkinEnabled);
					}
				}

				transformEditor->transmitImportFrameTimecode(desiredImportFrame);
			}
			else if (captureMode == CAPTURE_MODE_ALTCLIP_IMPORT)
			{
				// in this state the user is holding CTRL-SHIFT
				// down, and is navigating to the import frame

				// allocates buffers on 1st call
				loadImportFrame(desiredImportFrame);
				importFrame = desiredImportFrame;
				importFrameOffset = desiredImportFrameOffset;

				if (onionSkinEnabled == ONIONSKIN_MODE_OFF)
				{
					displayFrameFromImportBuf();
				}
				else
				{
					displayImportOverTarget(onionSkinEnabled);
				}

				transformEditor->transmitImportFrameTimecode(desiredImportFrame);
				transformEditor->transmitImportFrameOffset(desiredImportFrameOffset);
			}
			else if (captureMode == CAPTURE_MODE_ORIG_TARGET)
			{
				if (fast)
				{
					// playing
					if (frame != targetFrame)
					{
						MTI_UINT8 *stupidBuf[2] = {_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
                  CImageFormat frameImageFormat = _lastDisplayedFrameBuffer->getImageFormat();
						extr->extractActivePicture(importPreIntBuf, stupidBuf, imgFmt, &frameImageFormat);

						// the proxy from this will NOT have orig values
						// memcpy(importPreIntBuf, targetIntBuf, frameInternalSize);

						makeAndTransmitProxy();
					}
				}
				else
				{
					// stopping
					if (!targetFrameIsModified())
					{
						desiredTargetFrame = frame;
						if (targetFrame != desiredTargetFrame)
						{
							loadTargetFrame(desiredTargetFrame);
							targetFrame = desiredTargetFrame;
						}

						desiredImportFrame = frame;
						if (importFrame != desiredImportFrame)
						{
							fetchImportFrame(desiredImportFrame);
							importFrame = desiredImportFrame;
						}

						makeAndTransmitProxy();
					}
				}

				// display the combo
				if (onionSkinEnabled == ONIONSKIN_MODE_OFF)
				{
					if (displayProcessed)
					{
						// display target buf
						if (frame == targetFrame)
						{
							displayFrameFromTargetBuf();
						}
						else
						{
							displayFrameFromFields(_lastDisplayedFrameBuffer);
						}
					}
					else
					{
						// displaying original
						displayFrameFromFields(_lastDisplayedFrameBuffer);
					}
				}
				else
				{
					// onionskin mode ON
					displayImportOverTarget(onionSkinEnabled);
				}

				transformEditor->transmitImportFrameTimecode(frame);
			}

			break;
		}

		case CURRENT_TOOL_REGISTRATION:
		{
			if (displayProcessed)
			{
				if (frame != loadedRegistrationFrame)
				{
					loadRegistrationFrame(frame);
					loadedRegistrationFrame = frame;
				}
				if (loadedRegistrationFrame != registeredRegistrationFrame)
				{
					registerLoadedRegistrationFrame();
					registeredRegistrationFrame = loadedRegistrationFrame;
				}

				// convert registered frame to RGB
				displayIntermediate(postRegistrationBuf);
			}
			else
			{
				displayFrameFromFields(_lastDisplayedFrameBuffer);
			}

			break;
      }
	} // switch on currentTool

	// switch the target of line and rectangle
	// draw operations to the BACK buffer for
	// displaying the next series of graphics
	setGraphicsBuffer(GL_BACK);

	// if you're stretching a RECT, redraw it
	if ((majorState == STRETCH_RECTANGLE_ACTIVE) && (minorState == SEEKING_XY2) && !doingFrameCompare)
	{
		drawRectangleInProgress();
	}

	// if you're tracing a LASSO , redraw it
	if ((majorState == DRAW_LASSO_ACTIVE) && (minorState == SEEKING_XYN) && !doingFrameCompare)
	{
		drawLassoInProgress();
	}

	// if reticle is enabled, redraw it
	if (currentReticle.getType() != RETICLE_TYPE_NONE && !doingFrameCompare)
	{
		drawReticle();
	}

	// Draw overlays for import frame diffs, orig value diffs, or history
	if ((showRedBlobsForDiffs || showMagentaBlobsForDiffs) && !doingFrameCompare)
	{
		// Redraw the mask of import/target differences.
		if (importFrameComparand && currentFrameComparand && _frameCompareOutputMask)
		{
			if (!targetMayHaveChanged &&
					((importFrameComparandIndex == _frameCompareFrameIndex1 && currentFrameComparandIndex ==
					_frameCompareFrameIndex2) ||
					(importFrameComparandIndex == _frameCompareFrameIndex2 && currentFrameComparandIndex ==
					_frameCompareFrameIndex1)))
			{
				// _frameCompareOutputMask is still valid!
			}
			else
			{
				MTI_UINT16 diffThreshold = showRedBlobsForDiffs ?
						(MTI_UINT16)(_altClipDiffThreshold * getMaxComponentValue()) // Don't round up.
						: (MTI_UINT16)(_origValDiffThreshold * getMaxComponentValue()); // Don't round up.
				compareTwoFrames(importFrameComparand, currentFrameComparand, diffThreshold, _frameCompareOutputMask);
				_frameCompareFrameIndex1 = importFrameComparandIndex;
				_frameCompareFrameIndex2 = currentFrameComparandIndex;
			}

			int savedHighlightColor = highlightColor;
			highlightColor = showRedBlobsForDiffs ? SOLID_RED : SOLID_MAGENTA;
			drawMaskOverlayFrame(_frameCompareOutputMask);
			highlightColor = savedHighlightColor;
		}
	}
	else if ((playbackFilter & PLAYBACK_FILTER_HIGHLIGHT_FIXES) != 0 && captureMode != CAPTURE_MODE_ORIG_IMPORT)
	{
		// Redraw overlay of history file stuff, which often is just a rectangle
		// around the fixed area
		CSaveRestore *reviewHistory = mainWindow->GetPlayer()->getReviewHistory();

		// draw highlights into the frame where the fixes are (!)
		int retVal;
		CAutoHistoryOpener opener(*reviewHistory, FLUSH_TYPE_RESTORE, frame, ALL_FIELDS, 0, 0, 0, retVal);
		if (retVal == 0)
		{
			while (reviewHistory->nextRestoreRecord() == 0)
			{
				if (!reviewHistory->getCurrentRestoreRecordResFlag())
				{
					CPixelRegionList *reviewPixelRegionList = reviewHistory->getCurrentRestoreRecordPixelRegionList();
					if (reviewPixelRegionList == NULL)
					{
						TRACE_0(errout << "INTERNAL ERROR: in CDisplayFrame: History is messed up");
					}
					else
					{

						int savedHighlightColor = highlightColor;
						highlightColor = SOLID_BLUE;
						drawPixelRegionListFrame(reviewPixelRegionList);
						highlightColor = savedHighlightColor;

						delete reviewPixelRegionList;
						reviewPixelRegionList = NULL;
					}
				}
			}
		}
	}

	// if brushes are loaded redraw them
	if (displayProcessed && brushEnabled && !doingFrameCompare)
	{
		if ((autoAccept || (frame == targetFrame)) && ((minorState == ACQUIRING_PIXELS) ||
				(minorState == PAINTING_PIXELS)))
		{
			if (paintBrush != NULL)
			{
				if (brushMode == BRUSH_MODE_THRU)
				{
					drawBrushFrame(paintBrush, SOLID_GREEN, dxcurfrm, dycurfrm);
				}
				else if (brushMode == BRUSH_MODE_ERASE)
				{ // was SOLID_YELLOW
					drawBrushFrame(paintBrush, SOLID_RED, dxcurfrm, dycurfrm);
				}
				else if (brushMode == BRUSH_MODE_ORIG)
				{
					drawBrushFrame(paintBrush, SOLID_YELLOW, dxcurfrm, dycurfrm);
				}
			}
		}
		if ((autoAccept || (frame == targetFrame)) || (frame == importFrame))
		{
			if ((cloneBrush != NULL) && (brushMode == BRUSH_MODE_THRU))
			{
				if (!isRel)
				{
					drawBrushFrame(cloneBrush, SOLID_RED, cloneX, cloneY);
				}
				else
				{
					drawBrushFrame(cloneBrush, SOLID_RED, dxcurfrm + cloneX, dycurfrm + cloneY);
				}
			}
		}
	}

	if (displayProcessed && minorState == PICKING_AUTOALIGN && !doingFrameCompare)
	{
		drawAutoAlignBoxFrame(SOLID_YELLOW, dxcurfrm, dycurfrm);
	}

	// if there's a pixel region list, draw it now
	// THIS IS THE GOOD ONE!!!
	if (pxlrgnlst != NULL)
	{
		drawPixelRegionListFrame(pxlrgnlst);
	}

	// display any other graphics - incl spline-in-progress
	// now responds to brush enable/disable ("8" key) in PAINT
	CToolManager toolManager;
	if (brushEnabled && !doingFrameCompare)
	{
		toolManager.onRedraw(frame);
	}

	// Draw multicolor AutoClean boxes
	if (!overlayDisplayList.empty() && !doingFrameCompare)
	{
		clipToImageRectangle(); // HHH

		glDisable(GL_LINE_STIPPLE); // in case someone left 'DASHED' on

		for (unsigned i = 0; i < overlayDisplayList.size(); ++i)
		{
			glColor4ub(overlayDisplayList[i].R,
						  overlayDisplayList[i].G,
						  overlayDisplayList[i].B,
						  overlayDisplayList[i].A);

			switch (overlayDisplayList[i].itemType)
			{
				case OverlayDisplayItem::ACOIT_rect:
				{
					RECT outline = overlayDisplayList[i].rect;
					scaleRectangleFrameToClient(&outline);
					fattenRectangleClient(&outline, 1);
					drawRectangleClientNoFlush(&outline);

					if (overlayDisplayList[i].AS > 0)
					{
						fattenRectangleFrame(&outline, 1);
                  glColor4ub(overlayDisplayList[i].RS,
                             overlayDisplayList[i].GS,
                             overlayDisplayList[i].BS,
								     overlayDisplayList[i].AS);

						drawRectangleClientNoFlush(&outline);
					}

					break;
				}

				//
				case OverlayDisplayItem::ACOIT_rect_inclusive:
				{
					RECT outline = overlayDisplayList[i].rect;
					// outline.right++;
					// outline.bottom++;
					scaleRectangleFrameToClient(&outline);
					drawRectangleClientNoFlush(&outline);

					if (overlayDisplayList[i].A > 0)
					{
						fattenRectangleFrame(&outline, 1);
                  glColor4ub(overlayDisplayList[i].RS,
                             overlayDisplayList[i].GS,
                             overlayDisplayList[i].BS,
								     overlayDisplayList[i].AS);

						drawRectangleClientNoFlush(&outline);
					}

					break;
				}

				case OverlayDisplayItem::ACOIT_rect_thick:
				{
					RECT outline = overlayDisplayList[i].rect;
					scaleRectangleFrameToClient(&outline);
					RECT clientOutline = outline;
					fattenRectangleClient(&outline, 2);
					outline.right--;
					outline.bottom--;

					// draw the rectangle using the clip methods
					glLineWidth(2);
					drawRectangleClientNoFlush(&outline);

					if (overlayDisplayList[i].A > 0)
					{
						fattenRectangleClient(&clientOutline, 4);
						clientOutline.right--;
						clientOutline.bottom--;
                  glColor4ub(overlayDisplayList[i].RS,
                        overlayDisplayList[i].GS,
                        overlayDisplayList[i].BS,
								overlayDisplayList[i].AS);

						drawRectangleClientNoFlush(&clientOutline); ;
					}

					glLineWidth(1);
					break;
				}

				case OverlayDisplayItem::ACOIT_shadow:
				{
					RECT outline = overlayDisplayList[i].rect;
					--outline.left;
					--outline.top;
					++outline.right;
					++outline.bottom;

					// clipAndDrawRectangleFrame(&outline);

					////////////////////////////////////////////////
					// clipToImageRectangle();

					// scale the rectangle to client coordinates
					RECT clrct;
					clrct.left = dscaleXFrameToClient(outline.left);
					clrct.top = dscaleYFrameToClient(outline.top);
					clrct.right = dscaleXFrameToClient(outline.right);
					clrct.bottom = dscaleYFrameToClient(outline.bottom);

					// draw the rectangle using the clip methods
					moveToClient(clrct.left, clrct.top);
					lineToClientNoFlush(clrct.right, clrct.top);
					lineToClientNoFlush(clrct.right, clrct.bottom);
					lineToClientNoFlush(clrct.left, clrct.bottom);
					lineToClientNoFlush(clrct.left, clrct.top);

					glColor4ub(overlayDisplayList[i].RS,
                     overlayDisplayList[i].GS,
                     overlayDisplayList[i].BS,
							overlayDisplayList[i].AS);

					--clrct.left;
					--clrct.top;
					++clrct.right;
					++clrct.bottom;

					moveToClient(clrct.left, clrct.top);
					lineToClientNoFlush(clrct.right, clrct.top);
					lineToClientNoFlush(clrct.right, clrct.bottom);
					lineToClientNoFlush(clrct.left, clrct.bottom);
					lineToClientNoFlush(clrct.left, clrct.top);
					break;
				}

				case OverlayDisplayItem::ACOIT_vector:
				{
					// HACK: left may be > right and bottom may be < top!
					// Note we want to draw the vectors to the CENTERS of the
					// not the upper left corner, which is why we average with the
					// x, y of the next pixel right or down!
					RECT &rect = overlayDisplayList[i].rect;

					int startX = (dscaleXFrameToClient(rect.left) + dscaleXFrameToClient(rect.left + 1)) / 2;
					int startY = (dscaleYFrameToClient(rect.top) + dscaleYFrameToClient(rect.top + 1)) / 2;
					int endX = (dscaleXFrameToClient(rect.right) + dscaleXFrameToClient(rect.right + 1)) / 2;
					int endY = (dscaleYFrameToClient(rect.bottom) + dscaleYFrameToClient(rect.bottom + 1)) / 2;

					// Draw a tiny diamond at the START point!
					moveToClient(startX - 2, startY);
					lineToClientNoFlush(startX, startY - 2);
					lineToClientNoFlush(startX + 2, startY);
					lineToClientNoFlush(startX, startY + 2);
					lineToClientNoFlush(startX - 2, startY);

					// Draw the vector
					moveToClient(startX, startY);
					lineToClientNoFlush(endX, endY);
					// flushGraphics();

					break;
				}
			}

		}

		flushGraphics();
		glDrawBuffer(GL_BACK);

		// restore default clipping
		clipToVisibleRectangle();

		setGraphicsColor(graphicsColor);
	}

	if (!doingFrameCompare)
	{
		showMaskOverlay();

		// **** MOVED DOWN FROM ABOVE SO MAGNIFIERS ARE DRAWN LAST ****
		// redraw any active magnifiers
		for (int i = 0; i < MAX_MAGNIFIERS; i++)
		{
			if (magptr[i] != NULL)
			{
				// Always refresh active magnifiers
				if (magptr[i]->isActive())
				{
					MTI_UINT8 *stupidBuf[2] =
					{_lastDisplayedFrameBuffer->getImageDataPtr(), nullptr};
					magptr[i]->refreshMagnifier(stupidBuf);
				}

				// Draw activew magnifiers if visible
				if (magptr[i]->isActive() && (magptr[i]->isVisible() || magptr[i]->getAutoCleanHackFlag()))
				{
					RECT magwin = magptr[i]->getMagnifierWindow();
					magwin.right += 1; // Change from INCLUSIVE to EXCLUSIVE
					magwin.bottom += 1; // Change from INCLUSIVE to EXCLUSIVE

					if (magptr[i]->getAutoCleanHackFlag())
					{
						if (magptr[i]->isVisible())
						{
							clipToImageRectangle();

							RECT frmrct = magptr[i]->getDisplayRect();
							frmrct.right++; // Change from INCLUSIVE to EXCLUSIVE
							frmrct.bottom++; // Change from INCLUSIVE to EXCLUSIVE
							int frmwd = frmrct.right - frmrct.left;
							int frmht = frmrct.bottom - frmrct.top;
							int magwd = magwin.right - magwin.left;
							int maght = magwin.bottom - magwin.top;

							int clwd = dscaleXFrameToClient(frmwd);
							int clht = dscaleYFrameToClient(frmht);

							// scale the rectangle to client coordinates
							RECT clrct;
							clrct.left = dscaleXFrameToClient(frmrct.left);
							clrct.top = dscaleYFrameToClient(frmrct.top);
							clrct.right = dscaleXFrameToClient(frmrct.right);
							clrct.bottom = dscaleYFrameToClient(frmrct.bottom);

							// scale crosshairs as well
							int hairwd = (frmwd - magwd) / 2;
							int hairht = (frmht - maght) / 2;
							int clLeftHair = clrct.left + (dscaleXFrameToClient(frmrct.left + frmwd - magwd) - clrct.left) / 2;
							int clTopHair = clrct.top + (dscaleYFrameToClient(frmrct.top + frmht - maght) - clrct.top) / 2;
							int clRightHair =
									clrct.right + (dscaleXFrameToClient(frmrct.right - frmwd + magwd) - clrct.right) / 2;
							int clBottomHair =
									clrct.bottom + (dscaleYFrameToClient(frmrct.bottom - frmht + maght) - clrct.bottom) / 2;
							int clHairX = clrct.left + (dscaleXFrameToClient(frmrct.left + frmwd) - clrct.left) / 2;
							int clHairY = clrct.top + (dscaleYFrameToClient(frmrct.top + frmht) - clrct.top) / 2;

							// draw shadow in black
							int oldColor = setGraphicsColor(SOLID_BLACK);
							moveToClient(clrct.left + 1, clrct.top + 1);
							lineToClientNoFlush(clrct.right + 1, clrct.top + 1);
							lineToClientNoFlush(clrct.right + 1, clrct.bottom + 1);
							lineToClientNoFlush(clrct.left + 1, clrct.bottom + 1);
							lineToClientNoFlush(clrct.left + 1, clrct.top + 1);
							moveToClient(clLeftHair + 1, clHairY + 1);
							lineToClientNoFlush(clrct.left + 1, clHairY + 1);
							moveToClient(clRightHair + 1, clHairY + 1);
							lineToClientNoFlush(clrct.right + 1, clHairY + 1);
							moveToClient(clHairX + 1, clTopHair + 1);
							lineToClientNoFlush(clHairX + 1, clrct.top + 1);
							moveToClient(clHairX + 1, clBottomHair + 1);
							lineToClientNoFlush(clHairX + 1, clrct.bottom + 1);

							// draw cursor in white
							setGraphicsColor(SOLID_WHITE);
							moveToClient(clrct.left, clrct.top);
							lineToClientNoFlush(clrct.right, clrct.top);
							lineToClientNoFlush(clrct.right, clrct.bottom);
							lineToClientNoFlush(clrct.left, clrct.bottom);
							lineToClientNoFlush(clrct.left, clrct.top);
							moveToClient(clLeftHair, clHairY);
							lineToClientNoFlush(clrct.left, clHairY);
							moveToClient(clRightHair, clHairY);
							lineToClientNoFlush(clrct.right, clHairY);
							moveToClient(clHairX, clTopHair);
							lineToClientNoFlush(clHairX, clrct.top);
							moveToClient(clHairX, clBottomHair);
							lineToClientNoFlush(clHairX, clrct.bottom);

							flushGraphics();

							// restore default clipping
							clipToVisibleRectangle();
						}
					}
					else // NOt auto-clean hack... draw red magnifier-sized rectangle
					{
						int oldColor = setGraphicsColor(SOLID_RED);
						clipAndDrawRectangleFrame(&magwin);
						setGraphicsColor(oldColor);
					}
				}
			}
		}
		// **** ****
	}

	return true;
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrameIfWeAreNotShowingDiffBlobs()
{
	int playbackFilter = mainWindow->GetPlayer()->getPlaybackFilter();
	if (playbackFilter != PLAYBACK_FILTER_IMPORT_FRAME_DIFFS
	&& ((playbackFilter & PLAYBACK_FILTER_HIGHLIGHT_FIXES) == 0 || captureMode != CAPTURE_MODE_ORIG_TARGET)
	&& mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::Off)
	{
		displayFrame();
	}
}
//-----------------------------------------------------------------------------

void CDisplayer::displayFrame()
{
	// this may be called without a drawing loaded
	if (imgFmt == NULL)
	{
		return;
	}

	// mbraca changed this to go through the player so the provisional
	// overlay crap will be drawn if we are displaying the provisional frame.
	// Old way:  displayFrame(lastDisplayFields, lastFrame);
	// New way:
	mainWindow->GetPlayer()->oneFrameToWindow(_lastDisplayedFrameBuffer, lastFrame,
			/* fastFlag = */ true,
			/* recomputeImportDiffs = */ false);
}
//-----------------------------------------------------------------------------

// Draw an intermediate buffer
void CDisplayer::displayIntermediate(MTI_UINT16 *intmdt)
{
   if (imgFmt == nullptr)
   {
      return;
   }

	// turn the intermediate into RGBA
	_mainFrameConverter->displayConvertIntermediate(
												intmdt,
												imgFmt,
												&dspRect,
												screenFrameBuffer,
												rgbRectWidth,
												rgbRectHeight,
												rgbRectWidth*4,
												currentLUTPtr,
												RGBChannelsMask );

	clearBackBuffer();
	blitPictureToBackBuffer(screenFrameBuffer);
}
//-----------------------------------------------------------------------------

void CDisplayer::showMaskOverlay()
{
	if (getDisplayMaskOverlay() == false)
	{
		//mainWindow->MaskComputeTimer->Enabled = false;
		return;
	}

	if (mainWindow->GetPlayer()->isReallyPlaying())
	{
		return;
	}

	auto maskTool = CNavigatorTool::GetMaskTool();
	if ((maskTool->getDefaultExclusionBorderWidth() < 10) && (maskTool->getDefaultInclusionBorderWidth() < 10))
	{
		return;
	}

	auto majorState = maskTool->getMaskTransfomEditor()->getMajorState();
	auto maskSelected = maskTool->isAnyMaskSelected();
	if (majorState != 0 || maskSelected)
	{
		return;
	}

	if (maskTool->IsMaskBeingDrawn())
	{
		// Bad programming
		_computedMaskParameters.AllVerticies.clear();
		return;
	}

	if (maskTool->IsMaskVisible() == false)
	{
		return;
	}

	if (getNewMaskAvailable())
	{
	     _overlayMask = _newOverlayMask;
	}

	auto parametersMatch = maskTool->getUncomputedMaskParameters() == _computedMaskParameters;
	if (parametersMatch == false)
	{
		ComputeOverlayInBackground();
		mainWindow->MaskComputeTimer->Interval = 20;
		mainWindow->MaskComputeTimer->Enabled = true;
		return;
	}

	if (getMaskComputeRunning())
	{
		return;
	}

	// If there is no mask
	if (_overlayMask.getCols() == 0)
	{
		return;
	}

	if ((_overlayMask.getCols() != frameWidth) || (_overlayMask.getRows() < frameHeight))
	{
		_computedMaskParameters.AllVerticies.clear();
        return;
	}

	blendMaskBorderLinesToBackBuffer(_overlayMask.data());

        mainWindow->MaskComputeTimer->Enabled = false;
}

static void computeMaskThread(void *tp, void *threadid)
{
	CDisplayer *thisPointer = static_cast<CDisplayer*>(tp);
	thisPointer->setMaskComputeRunning(true);
	thisPointer->setNewMaskAvailable(false);
	BThreadBegin(threadid);
	try
	{
		thisPointer->computeBorderOverlayMask();
		thisPointer->setNewMaskAvailable(true);
		thisPointer->setMaskComputeRunning(false);
	}
	catch (...)
	{
		// Wrong, for now
        TRACE_0(errout << "Mask thread crash");
		thisPointer->setNewMaskAvailable(true);
		thisPointer->setMaskComputeRunning(false);
	}
}

static CSpinLock recomputeLock;

bool CDisplayer::ComputeOverlayInBackground()
{
	// Do nothing is background is busy
	if (getNewMaskAvailable())
	{
		CAutoSpinLocker dataChangeLocker(recomputeLock);
		_overlayMask = _newOverlayMask;
		setNewMaskAvailable(false);
		return true;
	}

	if (getMaskComputeRunning())
	{
		return false;
	}

	_vpComputeMaskThread = BThreadSpawn(computeMaskThread, this);

	return false;
}

void CDisplayer::computeBorderOverlayMask()
{
	try
	{
		CHRTimer hrt;
		auto maskTool = CNavigatorTool::GetMaskTool();
		auto maskParameters = maskTool->getUncomputedMaskParameters();
		Ipp8uArray smallMask = maskTool->ComputeSmallMask({frameWidth, frameHeight}, 256);;
		auto tempMask = maskTool->generateOverlayMask(smallMask, {imgWdth, imgHght}, false);
		{
			CAutoSpinLocker dataChangeLocker(recomputeLock);
			_newOverlayMask = tempMask;
			_computedMaskParameters = maskParameters;
		   ///	maskTool->setRegionOfInterestAvailable(false);
		}}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "Error: " << ex.what());
	}
	catch (...)
	{
		// Ignore for now
		TRACE_0(errout << "error");
		return;
	}
}
//-----------------------------------------------------------------------------



