//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainWindowUnit.h"
#include "Displayer.h"
#include "MTIstringstream.h"
#include "Player.h"
#include "PreferencesUnit.h"
#include "SysInfo.h"
//#include "err_format.h"
#include "ToolManager.h"
#include "GpuAccessor.h"
#include "GpuDumpFormUnit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "MTIUnit"
#pragma link "MTIUNIT"
//#pragma link "CSPIN"
#pragma link "SpinEditFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorLabel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

extern TmainWindow *mainWindow;

TPreferencesDlg *PreferencesDlg;
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TPreferencesDlg::TPreferencesDlg(TComponent* Owner)
        : TMTIForm(Owner)
{
   OriginalValues = new CValues(this);
   TempValues = new CValues(this);
   PreferenceControl->ActivePage = GeneralSheet;

   // Check GPU
   updateGpuPage();
}

   void TPreferencesDlg::updateGpuPage()
{
   GpuAccessor gpuAccessor;
   GpuInfoPanel->Visible = gpuAccessor.doesGpuExist();
   GpuNotFoundReasonLabel->Caption = gpuAccessor.getErrorMessage().c_str();

   if (gpuAccessor.doesGpuExist() == false)
   {
       EnableGpuCheckBox->Checked = false;
       EnableGpuCheckBox->Enabled = false;
       return;
   }

   int n = 0;

   // Set to zero to start, the read preferences will set it to user
   gpuAccessor.setDefaultGpu(0);

   for (auto &name : gpuAccessor.getDeviceNames())
   {
      MTIostringstream os;
      os << n++ << ": " << name;
      DeviceNameComboBox->Items->Add(os.str().c_str());
   }

   EnableGpuCheckBox->Enabled = true;
   EnableGpuCheckBox->Checked = true;
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------
void __fastcall TPreferencesDlg::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   fVisible = false;
   TempValues->Restore();
}
//---------------------------------------------------------------------------

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

   bool TPreferencesDlg::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the init file and sets the saved position and size
//
//******************************************************************************
{
  if (ini == NULL) return(false);

  AutoSaveSAPCBox->Checked = ini->ReadBool(IniSection, "SaveSAPOnExit",AutoSaveSAPCBox->Checked);
  AutoSavePreferencesCBox->Checked = ini->ReadBool(IniSection, "SaveParametersOnExit",AutoSavePreferencesCBox->Checked);
  BorderWidthSlider->Position = ini->ReadInteger(IniSection, "WindowBorderWidth", BorderWidthSlider->Position);
  SlowSpinEdit->SetValue(ini->ReadInteger(IniSection, "SlowSpeed", SlowSpinEdit->GetValue()));
  MediumSpinEdit->SetValue(ini->ReadInteger(IniSection, "MediumSpeed", MediumSpinEdit->GetValue()));
  FastSpinEdit->SetValue(ini->ReadInteger(IniSection, "FastSpeed", FastSpinEdit->GetValue()));
  GammaSlider->Position = ini->ReadInteger(IniSection, "DefaultGamma", GammaSlider->Position);
  ToolTipsDurationTrackBar->Position = ini->ReadInteger(IniSection, "ToolTipsDuration", ToolTipsDurationTrackBar->Position);

  CursorCBox->Checked = ini->ReadBool(IniSection, "CursorType", CursorCBox->Checked);
  CursorCBoxClick(NULL);

  SwapWAndShiftWCBox->Checked = ini->ReadBool(IniSection, "SwapWAndShiftW", SwapWAndShiftWCBox->Checked);
  SwapWAndShiftWCBoxClick(NULL);

  LockCacheFramesInMemoryCheckBox->Checked = ini->ReadBool(IniSection, "LockCachedFramesInMemory", LockCacheFramesInMemoryCheckBox->Checked);
  int defaultCacheSize = FrameCacheSizeTrackEdit->GetValue();
  _cacheSizeTrackEditHasBeenInitialized = true;
  FrameCacheSizeTrackEdit->SetValue(ini->ReadInteger(IniSection, "FrameCacheSize", defaultCacheSize));
  CacheSizeTrackEditNotifyWidgetClick(NULL);

  GpuAccessor gpuAccessor;
  EnableGpuCheckBox->Checked = ini->ReadBool(IniSection, "EnableGpu", true) && gpuAccessor.doesGpuExist();
  DeviceNameComboBox->ItemIndex = ini->ReadInteger(IniSection, "GpuIndex", 0);

  // Disseminate the information
  OKBtnClick(NULL);

  return true;
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

   bool TPreferencesDlg::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the ini files, saves the position and size
//
//******************************************************************************
{
  if (ini == NULL) return(false);

  ini->WriteBool(IniSection, "SaveSAPOnExit", AutoSaveSAPCBox->Checked);
  ini->WriteBool(IniSection, "SaveParametersOnExit", AutoSavePreferencesCBox->Checked);
  ini->WriteInteger(IniSection, "WindowBorderWidth", BorderWidthSlider->Position);
  ini->WriteInteger(IniSection, "SlowSpeed", SlowSpinEdit->GetValue());
  ini->WriteInteger(IniSection, "MediumSpeed", MediumSpinEdit->GetValue());
  ini->WriteInteger(IniSection, "FastSpeed", FastSpinEdit->GetValue());
  ini->WriteInteger(IniSection, "DefaultGamma", GammaSlider->Position);
  ini->WriteBool(IniSection, "CursorType", CursorCBox->Checked);
  ini->WriteBool(IniSection, "SwapWAndShiftW", SwapWAndShiftWCBox->Checked);
  ini->WriteInteger(IniSection, "ToolTipsDuration", ToolTipsDurationTrackBar->Position);
  ini->WriteInteger(IniSection, "FrameCacheSize", FrameCacheSizeTrackEdit->GetValue());

  ini->WriteBool(IniSection, "LockCachedFramesInMemory", LockCacheFramesInMemoryCheckBox->Checked);
  ini->DeleteKey(IniSection, "LockCacheFramesInMemory"); // Old default=TRUE flag; clean it up.

  ini->WriteBool(IniSection, "EnableGpu", EnableGpuCheckBox->Checked);
  ini->WriteInteger(IniSection, "GpuIndex", DeviceNameComboBox->ItemIndex);
  return true;
}

//------------ReadDesignSettings----------------John Mertus-----Jan 2001---

   bool TPreferencesDlg::ReadDesignSettings(void)

//  This writes the ini files, saves the position and size
//
//******************************************************************************
{
   OriginalValues->Restore();
   return true;
}

__fastcall TPreferencesDlg::~TPreferencesDlg(void)
{
     delete OriginalValues;
     OriginalValues = NULL;
     delete TempValues;
     TempValues = NULL;

}

void __fastcall TPreferencesDlg::FormShow(TObject *Sender)
{
    TempValues->LoadCurrent();
	SaveButton->Enabled = TempValues->HasChanged();

	UpdateCacheCapacityStatusLine();
 //  updateGpuPage();

	TMTIForm::FormShow(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::OKBtnClick(TObject *Sender)
{
    TempValues->SetCurrent();
    //
    // The preferences take effect on clip loading. But the speed
    // settings take effect immediately without reloading the clip
    //
    mainWindow->GetPlayer()->setDisplaySpeed(DISPLAY_SPEED_SLOW, SlowSpinEdit->GetValue());
    mainWindow->GetPlayer()->setDisplaySpeed(DISPLAY_SPEED_MEDIUM, MediumSpinEdit->GetValue());
    mainWindow->GetPlayer()->setDisplaySpeed(DISPLAY_SPEED_FAST, FastSpinEdit->GetValue());
    //
    // Make sure the FPS status display is up to date
    mainWindow->UpdateFPSViewStatus(mainWindow->GetPlayer()->getDisplaySpeedIndex());
    //
    // The default gamma preference also takes place immediately
    // without reloading the clip - you won't see this unless the
    // default LUT is selected.
    //
    mainWindow->GetDisplayer()->setDefaultGamma(GammaSlider->Position);
    mainWindow->GetDisplayer()->establishDisplayLUT();

//    // This will update the RemoteDisplay in case the choice has been
//    //   changed (e.g. YUV -> RGB). - 1/11/2005 - mpr
//    // Note: something should be done with the return value (an int) , but what?
//    mainWindow->GetPlayer()->resetDisplayMode();

    // This special section will save the autosave preference flag
    // automatically.
    CIniFile *ini = CreateIniFile(GetIniFileName());
    if (ini != NULL)
    {
       ini->WriteBool(GetIniSection(), "SaveParametersOnExit",AutoSavePreferencesCBox->Checked);
       DeleteIniFile(ini);
	 }

	 // Hack so border gets fixed.
    mainWindow->initializeTheDisplayer();

    // Do GPU
    GpuAccessor gpuAccessor;
    gpuAccessor.setDefaultGpu(DeviceNameComboBox->ItemIndex);
    gpuAccessor.setUseIfAvailable(EnableGpuCheckBox->Checked);
}

//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::CancelBtnClick(TObject *Sender)
{
   TempValues->Restore();
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::ChangeClick(TObject *Sender)
{
   SaveButton->Enabled = TempValues->HasChanged();
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::SaveButtonClick(TObject *Sender)
{
   SaveSettings();
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::RestoreButtonClick(TObject *Sender)
{
   RestoreSettings(CM_DESIGNED_DATA);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::GammaSliderChange(TObject *Sender)
{
  char val[4];

  sprintf(val,"%2.1f",(double)(GammaSlider->Position/10.));
  GammaValue->Caption = val;
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::CursorCBoxClick(TObject *Sender)
{
   // 0 = Crosshair, 1 = Arrow
   mainWindow->SetCursor(CursorCBox->Checked ? 1 : 0);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::SwapWAndShiftWCBoxClick(TObject *Sender)
{
   mainWindow->SetSwapWAndShiftWFlag(SwapWAndShiftWCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::BorderWidthSliderChange(TObject *Sender)
{
  char val[4];

  sprintf(val,"%2d", BorderWidthSlider->Position);
  BorderWidthValue->Caption = val;
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::FormCreate(TObject *Sender)
{
   FrameCacheSizeTrackEdit->Enable(false);
	FrameCacheSizeTrackEdit->SetMinAndMax(0, 0);
	FrameCacheSizeTrackEdit->SetLabels(StringToStdString(FrameCacheSizeTrackEdit->TitleLabel->Caption),
													"0", "0");
	FrameCacheSizeTrackEdit->SetValue(0);
   StatusLine1->Caption = "";
   StatusLine2->Caption = "";
   StatusLine3->Caption = "";

   // One-time-only stuff

   // Not sure why we need to create these controls dynamically
//   FastSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xFastEdit, "FastSpinEdit");
//   MediumSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xMediumEdit, "MediumSpinEdit");
//   SlowSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xSlowEdit, "SlowSpinEdit");

	// Enable the image file read cache controls.
   // Rule of thumb for how much memory to leave free:
   // 4 GB + (1 GB for every 8 GB physical memory installed)
	MTI_UINT32 physicalRamSizeInGb = SysInfo::GetPhysicalMemorySizeInGB();
	const size_t NumberOfGBToLeaveFree = 8 + (physicalRamSizeInGb / 8);
	const size_t MinCacheSizeLimitInGB = (physicalRamSizeInGb <= 16)? 2 : 4;

	// Default is to grab half of the physical memory installed!
//		EnableFrameCacheCheckBox->Enabled = true;
//		EnableFrameCacheCheckBox->Checked = false;
	int cacheMaxSizeInGb = (physicalRamSizeInGb <= 8)
                           ? 4
                           : (physicalRamSizeInGb - NumberOfGBToLeaveFree);
	int cacheDefaultSizeInGB = (physicalRamSizeInGb <= 8)
                               ? 2
                               : ((physicalRamSizeInGb <= 16)
                                   ? 6
										     : (physicalRamSizeInGb / 2));
	FrameCacheSizeTrackEdit->Enable(false);
	FrameCacheSizeTrackEdit->SetMinAndMax(MinCacheSizeLimitInGB, cacheMaxSizeInGb);
	MTIostringstream os1;
	os1 << MinCacheSizeLimitInGB;
	MTIostringstream os2;
	os2 << cacheMaxSizeInGb;
	FrameCacheSizeTrackEdit->SetLabels(
						StringToStdString(FrameCacheSizeTrackEdit->TitleLabel->Caption),
						os1.str(), os2.str());
	FrameCacheSizeTrackEdit->SetValue(cacheDefaultSizeInGB);
	FrameCacheSizeTrackEdit->Enable(true);

  // Default for locking pages is to turn it OFF if more than 64GB of physical memory
  // is installed, else turn it ON.
  LockCacheFramesInMemoryCheckBox->Checked = physicalRamSizeInGb <= 64;
  LockCacheFramesInMemoryCheckBoxClick(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::EnableFrameCacheCheckBoxClick(
      TObject *Sender)
{
//	SetFrameCacheSize();
}
// ---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::LockCacheFramesInMemoryCheckBoxClick(TObject *Sender)
{
   mainWindow->LockCacheFramesInMemory(LockCacheFramesInMemoryCheckBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::CacheSizeTrackEditNotifyWidgetClick(
      TObject *Sender)
{
   SetFrameCacheSize();
}
//---------------------------------------------------------------------------

void TPreferencesDlg::SetFrameCacheSize()
{
   if (_cacheSizeTrackEditHasBeenInitialized == false)
   {
      return;
   }

   const size_t oneGigabyte = 1024 * 1024 * 1024;

   StatusLine1->Caption = "";
   StatusLine2->Caption = "";
	StatusLine3->Caption = "";

   // Set the frame cache size
   size_t desiredGigabytes = FrameCacheSizeTrackEdit->GetValue();
   int retVal = 0;

   do
   {
      size_t numberOfPages = EnableFrameCacheCheckBox->Checked
									  ? ((oneGigabyte / SysInfo::GetSystemPageSize()) * desiredGigabytes)
                             : 0;
		size_t actualNumberOfPagesAllocated = 0;
		retVal = mainWindow->SetFrameCacheSize(numberOfPages, &actualNumberOfPagesAllocated);

		// See if we can't reduce the size of the cache, which should never happen!!
		if (retVal != 0 && actualNumberOfPagesAllocated > numberOfPages)
		{
			StatusLine1->Caption =
					  AnsiString("ERROR: cannot reduce the cache size to ")
					  + FrameCacheSizeTrackEdit->GetValue() + AnsiString(" GB).");

			// No sense looping! Also just leave the cache enabled.
			retVal = 0;
			break;
		}

		if (retVal == 0)
		{
			if ((int)desiredGigabytes == FrameCacheSizeTrackEdit->GetValue()
				 && actualNumberOfPagesAllocated == numberOfPages)
			{
				StatusLine1->Caption =
						  AnsiString("Cache is enabled at requested size (")
						  + FrameCacheSizeTrackEdit->GetValue() + AnsiString(" GB).");
			}
			else
         {
            float gigabytes = actualNumberOfPagesAllocated
                            * (SysInfo::GetSystemPageSize() / (float)oneGigabyte);
            char temp[256];
            int intGigabytes = (int)gigabytes;
            int fracGigabytes = (int)((gigabytes - intGigabytes) * 100);
				sprintf(temp, "%d.%02d", intGigabytes, fracGigabytes);

            StatusLine1->Caption =
                    AnsiString("Cache is enabled at reduced size - successfully allocated ")
                    + AnsiString(temp) + AnsiString(" GB.");
            StatusLine3->Caption = "To get more memory, try closing some other programs and relaunching this one.";
            retVal = 0; // good enuff!
         }
      }
   }
   while (retVal != 0 && --desiredGigabytes > 0);

   if (retVal != 0)
   {
      StatusLine1->Caption = "Cache is disabled because memory allocation failed.";
      return;
   }

	UpdateCacheCapacityStatusLine();
}
//---------------------------------------------------------------------------


void TPreferencesDlg::UpdateCacheCapacityStatusLine()
{
// This doesn't seem to work right!
//	int pixelsPerLine = 0;
//	int linesPerFrame = 0;
//	mainWindow->GetImageBodyReadDimensions(pixelsPerLine, linesPerFrame);
//	int capacity = (int)mainWindow->GetFrameCacheSizeInFrames();
//
//	if (capacity == 0)
//	{
		StatusLine2->Caption = "";
//		return;
//	}
//
//	StatusLine2->Caption = AnsiString("Capacity is ")
//								  + AnsiString(capacity)
//								  + AnsiString(" frames of size ")
//								  + AnsiString(pixelsPerLine)
//								  + AnsiString(" x ")
//								  + AnsiString(linesPerFrame)
//								  + AnsiString(".");
}

void __fastcall TPreferencesDlg::ResetWarningsButtonClick(TObject *Sender)
{
   CToolManager toolManager;

   auto n = toolManager.getToolCount();
   for (auto toolIndex = 0; toolIndex < n; toolIndex++)
   {
      auto toolObj = toolManager.GetTool(toolIndex);
      if (toolObj != nullptr)
      {
         toolObj->toolResetWarnings();
      }
   }

   mainWindow->SetGpuWarning(true);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::ClearCacheButtonClick(TObject *Sender)
{
	mainWindow->ClearFrameCache();
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::ToolTipsDurationTrackBarChange(TObject *Sender)
{
   int pause = 0;
   switch (ToolTipsDurationTrackBar->Position)
   {
      case 1:
         pause = 2500;
         break;
      case 2:
         pause = 5000;
         break;
      case 3:
         pause = 30000;
         break;
   }

   Application->ShowHint = pause != 0;
   Application->HintHidePause = pause;
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::EnableGpuCheckBoxClick(TObject *Sender)
{
    auto enabled = EnableGpuCheckBox->Checked;
    DeviceNameComboBox->Enabled = enabled;


    //AvailableGpuLabel->Enabled = enabled;
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::DeviceNameComboBoxChange(TObject *Sender)
{
   GpuAccessor gpuAccessor;

   auto deviceIndex = DeviceNameComboBox->ItemIndex;
   if ((deviceIndex < 0) || deviceIndex >= gpuAccessor.getDeviceCount())
   {
   	return;
   }

   gpuAccessor.setDefaultGpu(deviceIndex);
}
//---------------------------------------------------------------------------

void __fastcall TPreferencesDlg::GpuInfoButtonClick(TObject *Sender)
{
   GpuAccessor gpuAccessor;
   auto infoString = gpuAccessor.getInfoString(gpuAccessor.getDefaultGpu());
   if (GpuDumpForm == nullptr)
   {
      GpuDumpForm = new TGpuDumpForm(this);
   }

   if (infoString.empty() == false)
	{
		// Bring up the "dump header panel"
      auto deviceIndex = DeviceNameComboBox->ItemIndex;
      MTIostringstream os;
      os << "Gpu Info for device " << deviceIndex << ", '" << gpuAccessor.getDeviceName(deviceIndex) << "'";
      GpuDumpForm->Caption = os.str().c_str();
		GpuDumpForm->Set(infoString);
		GpuDumpForm->Show();
   }
}
//---------------------------------------------------------------------------

