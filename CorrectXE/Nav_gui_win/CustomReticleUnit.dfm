object CustomReticleDlg: TCustomReticleDlg
  Left = 429
  Top = 188
  BorderStyle = bsToolWindow
  Caption = 'Reticle Overlay'
  ClientHeight = 456
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RETOKButton: TButton
    Left = 53
    Top = 416
    Width = 73
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = RETOKButtonClick
  end
  object RETCancelButton: TButton
    Left = 133
    Top = 416
    Width = 73
    Height = 25
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = RETCancelButtonClick
  end
  object ApertureGroupBox: TRadioGroup
    Left = 16
    Top = 189
    Width = 228
    Height = 65
    Caption = ' Aperture '
    Columns = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      'Full'
      'Academy')
    ParentFont = False
    TabOrder = 2
    OnClick = ApertureGroupBoxClick
  end
  object ReticleNameGroupBox: TGroupBox
    Left = 16
    Top = 8
    Width = 228
    Height = 65
    Caption = ' Reticle Name  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object DeleteReticleButton: TButton
      Left = 174
      Top = 38
      Width = 42
      Height = 17
      Caption = 'Delete'
      TabOrder = 0
      OnClick = DeleteReticleButtonClick
    end
    object ReticleNameComboBox: TComboBox
      Left = 8
      Top = 23
      Width = 160
      Height = 21
      AutoComplete = False
      TabOrder = 1
      Text = 'DEFAULT_1.33_ACADEMY'
      OnClick = OnLoadReticle
      OnKeyDown = ReticleNameKeyDown
    end
    object SaveReticleButton: TButton
      Left = 173
      Top = 13
      Width = 42
      Height = 17
      Caption = 'Save'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = SaveReticleButtonClick
    end
  end
  object OffsetBox: TGroupBox
    Left = 16
    Top = 262
    Width = 228
    Height = 65
    Caption = ' Offset '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object XLabel: TLabel
      Left = 42
      Top = 26
      Width = 6
      Height = 13
      Caption = 'X'
    end
    object YLabel: TLabel
      Left = 130
      Top = 26
      Width = 6
      Height = 13
      Caption = 'Y'
    end
    inline XOffsetSpinEdit: TSpinEditFrame
      Left = 58
      Top = 23
      Width = 52
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 58
      ExplicitTop = 23
      ExplicitWidth = 52
      inherited Edit: TEdit
        Width = 38
        ExplicitWidth = 38
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
    end
    inline YOffsetSpinEdit: TSpinEditFrame
      Left = 146
      Top = 23
      Width = 52
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 146
      ExplicitTop = 23
      ExplicitWidth = 52
      inherited Edit: TEdit
        Width = 38
        ExplicitWidth = 38
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
    end
  end
  object Scale: TGroupBox
    Left = 16
    Top = 335
    Width = 228
    Height = 65
    Caption = ' Scale '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object HLabel: TLabel
      Left = 42
      Top = 26
      Width = 7
      Height = 13
      Caption = 'H'
    end
    object VLabel: TLabel
      Left = 130
      Top = 26
      Width = 6
      Height = 13
      Caption = 'V'
    end
    inline HScaleSpinEdit: TSpinEditFrame
      Left = 58
      Top = 23
      Width = 52
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 58
      ExplicitTop = 23
      ExplicitWidth = 52
      inherited Edit: TEdit
        Width = 38
        ExplicitWidth = 38
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
    end
    inline VScaleSpinEdit: TSpinEditFrame
      Left = 146
      Top = 23
      Width = 52
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 146
      ExplicitTop = 23
      ExplicitWidth = 52
      inherited Edit: TEdit
        Width = 38
        ExplicitWidth = 38
      end
      inherited DownButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
      inherited UpButtonOutlinePanel: TPanel
        Left = 36
        ExplicitLeft = 36
      end
    end
  end
  object AspectRatioRadioGroup: TRadioGroup
    Left = 16
    Top = 81
    Width = 228
    Height = 100
    Caption = ' Aspect Ratio '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object PresetButton133: TButton
    Left = 24
    Top = 101
    Width = 43
    Height = 17
    Caption = '1.33'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = PresetButtonClick
  end
  object PresetButton166: TButton
    Left = 24
    Top = 122
    Width = 43
    Height = 17
    Caption = '1.66'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = PresetButtonClick
  end
  object PresetButton178: TButton
    Left = 24
    Top = 144
    Width = 43
    Height = 17
    Caption = '1.78'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = PresetButtonClick
  end
  object PresetButton185: TButton
    Left = 72
    Top = 101
    Width = 43
    Height = 17
    Caption = '1.85'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = PresetButtonClick
  end
  object PresetButton235: TButton
    Left = 72
    Top = 122
    Width = 43
    Height = 17
    Caption = '2.35'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = PresetButtonClick
  end
  object PresetButton240: TButton
    Left = 72
    Top = 144
    Width = 43
    Height = 17
    Caption = '2.40'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = PresetButtonClick
  end
  object AspectRatioEditBox: TEdit
    Left = 127
    Top = 120
    Width = 97
    Height = 21
    TabOrder = 13
    OnKeyDown = OnAspectRatioKeyDown
  end
end
