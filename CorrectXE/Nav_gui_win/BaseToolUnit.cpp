// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BaseToolUnit.h"

#include "AcceptOrRejectUnit.h"
#include "BaseTool.h"
#include "CompactTrackEditFrameUnit.h"
#include "IniFile.h"
#include "MainWindowUnit.h"
#include "MTIKeyDef.h"
#include "NavigatorTool.h"
#include "NoUndoDialogUnit.h"
#include "ShowModalDialog.h"
#include "ToolCommand.h"
#include "ToolManager.h"
#include "TrackEditFrameUnit.h"
#include "MTIDialogs.h"
#include "stdio.h"
#include <math.h>
#include "Displayer.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "VTimeCodeEdit"
#pragma link "ColorPanel"
#pragma link "CompactTrackEditFrameUnit"
#pragma link "MaskPropertiesFrame"
#pragma resource "*.dfm"
TBaseToolWindow *BaseToolWindow;

// ---------------------------------------------------------------------------
__fastcall TBaseToolWindow::TBaseToolWindow(TComponent* Owner) : TMTIForm(Owner)
{
	// DO IT ALL IN FormCreate()
}

// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormCreate(TObject *Sender)
{
	InformedMouseInTool = false;
	InformedToolActive = false;
	MouseInToolTimer->Enabled = false;

	LoadLightImage->ControlStyle << csOpaque;
	BaseToolBackgroundPanel->ControlStyle << csOpaque;

	// Set Tags in Mask Toolbar Buttons to be Navigator Tool commands
	Mask_ActivateNormalModeButton->Tag = NAV_CMD_MASK_TOOL_ENBL_DSBL;
	Mask_ShowOrHideOutlineButton->Tag = NAV_CMD_MASK_TOGGLE_VISIBLE;
	Mask_ShowOrHidePropertiesPanelButton->Tag = NAV_CMD_MASK_TOGGLE_PROPERTIES;

	Mask_DrawRectButton->Tag = NAV_CMD_MASK_DRAW_RECT;
	Mask_DrawLassoButton->Tag = NAV_CMD_MASK_DRAW_LASSO;
	Mask_DrawBezierButton->Tag = NAV_CMD_MASK_DRAW_BEZIER;

	Mask_SetInclusiveButton->Tag = NAV_CMD_MASK_INCLUDE;
	Mask_SetExclusiveButton->Tag = NAV_CMD_MASK_EXCLUDE;

	Mask_SetKeyframeButton->Tag = NAV_CMD_MASK_SET_KEYFRAME;
	Mask_ActivateRoiModeButton->Tag = NAV_CMD_MASK_TOGGLE_MASK_VS_ROI;
   Mask_NeutralizeButton->Tag = NAV_CMD_MASK_HIDE_SELECTED;

	_oldClientHeight = this->ClientHeight;

	MaskPropertiesFrame->Top = BaseToolBackgroundPanel->Top + BaseToolBackgroundPanel->Height;
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormActivate(TObject *Sender)
{
	if (GBaseTool == NULL)
		return;

	sendSimpleEvent(FPE_FormActivate);

	if (!InformedToolActive) {
		GBaseTool->getSystemAPI()->informToolWindowStatus(true);
		InformedToolActive = true;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormDeactivate(TObject *Sender)
{
	if (GBaseTool == NULL)
		return;

	sendSimpleEvent(FPE_FormDeactivate);

	if (InformedToolActive) {
		GBaseTool->getSystemAPI()->informToolWindowStatus(false);
		InformedToolActive = false;
	}
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::passKeyDownEventToMainWindow(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (GBaseTool != NULL)
	{
		MTIKeyEventHandler keyEventHandler = GBaseTool->getSystemAPI()->getKeyDownEventHandler();
		keyEventHandler(Sender, Key, Shift);

		// Not sure what problem this was trying to solve, but this can't possibly
		// work because if you hold down the key you get a ton of auto-repeats!
		// Plus we may have never seen the key down event if focus was on the
		// main window and the key handler transferred focus to the tool window.
		// _KeyDownCount++;
	}
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::passKeyUpEventToMainWindow(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (GBaseTool != NULL) // && _KeyDownCount > 0) see comment below
	{
		MTIKeyEventHandler keyEventHandler = GBaseTool->getSystemAPI()->getKeyUpEventHandler();
		keyEventHandler(Sender, Key, Shift);

		// Not sure what problem this was trying to solve, but this can't possibly
		// work because if you hold down the key you get a ton of auto-repeats!
		// Plus we may have never seen the key down event if focus was on the
		// main window and the key handler transferred focus to the tool window.
		// _KeyDownCount--;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	// The track bar widgets get first crack at the keys.
	if (TCompactTrackEditFrame::ProcessKeyDown(Key, Shift) || TTrackEditFrame::ProcessKeyDown(Key, Shift))
	{
		// Consumed by the widget
		Key = 0;
		return;
	}

	// All Alt-modified keys go straight to the main window!
	if (Shift.Contains(ssAlt))
	{
		// If the tool handles the Alt Key, we will want to not zero it out
		// else the main menu shortcuts won't work!
		WORD oldKey = Key;

		passKeyDownEventToMainWindow(Sender, Key, Shift);

		if (Key == 0 && oldKey == MTK_ALT)
		{
			Key = MTK_ALT;
		}

		return;
	}

	// Don't steal any keys from a focused edit box!
	TWinControl *controlWithFocus = ActiveControl;
	TCustomEdit *customEdit = dynamic_cast<TCustomEdit*>(controlWithFocus);
	if (customEdit != NULL)
	{
		return;
	}

	// Don't steal SPACEs from check boxes!
	TCustomCheckBox *checkBox = dynamic_cast<TCustomCheckBox*>(controlWithFocus);
	if (checkBox != NULL && Key == MTK_SPACE)
	{
		return;
	}

	// Special processing for naked track bars (the custom track bar edit frames
	// were handled above).
	TTrackBar *trackBar = dynamic_cast<TTrackBar*>(controlWithFocus);
	if (trackBar != NULL && (Key == MTK_UP || Key == MTK_DOWN || Key == MTK_LEFT || Key == MTK_RIGHT))
	{
		// Arrow + Ctrl changes position by 1.
		// Arrow + Shift changes position by 1/10th of the range, minimum 1.
		// If the trackbar range (Max - Min) is less than 500, a naked arrow
		// key changes position by 1/20th of the range, minimum 1.
		// Else naked arrow key changes the position by (position / 10),
		// minimum 1. (Ugggh, per Larry - needs to match Paint radius)
		int positionChange;
		int trackBarRange = trackBar->Max - trackBar->Min + 1;
		if (Shift.Contains(ssCtrl))
		{
			positionChange = 1;
		}
		else if (Shift.Contains(ssShift))
		{
			positionChange = (trackBarRange <= 10) ? 1 : (trackBarRange / 10);
		}
		else if (trackBarRange >= 500)
		{
			// 10 vs 11 so the negative change matches the positive change!
			positionChange = 1 + (trackBar->Position / ((Key == MTK_UP || Key == MTK_RIGHT) ? 10 : 11));
		}
		else
		{
			positionChange = (trackBarRange <= 10) ? 1 : (trackBarRange / 20);
		}

		int newPosition = trackBar->Position + ((Key == MTK_UP || Key == MTK_RIGHT) ? positionChange : -positionChange);
		newPosition = std::min<int>(trackBar->Max, newPosition);
		newPosition = std::max<int>(trackBar->Min, newPosition);

		trackBar->Position = newPosition;

		// Do NOT pass the key to the track bar!
		Key = 0;
		return;
	}

	// Give the active tool a chance to claim the key.
	if (!sendKeyEvent(FPE_FormKeyDown, Sender, Key, Shift))
	{
		// Not claimed by the active tool, so pass the key down event on
		// to the main window.
		passKeyDownEventToMainWindow(Sender, Key, Shift);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
	// The track bar widgets get first crack at the keys.
	if (TCompactTrackEditFrame::ProcessKeyUp(Key, Shift) || TTrackEditFrame::ProcessKeyUp(Key, Shift))
	{
		// Consumed by the widget
		Key = 0;
		return;
	}

	// All Alt-modified keys go straight to the main window!
	if (Shift.Contains(ssAlt))
	{
		passKeyUpEventToMainWindow(Sender, Key, Shift);
	}

	// Don't steal any keys from a focused edit box!
	TWinControl *controlWithFocus = ActiveControl;
	TCustomEdit *customEdit = dynamic_cast<TCustomEdit*>(controlWithFocus);
	if (customEdit != NULL)
	{
		return;
	}

	// Don't steal SPACEs from check boxes!
	TCustomCheckBox *checkBox = dynamic_cast<TCustomCheckBox*>(controlWithFocus);
	if (checkBox != NULL && Key == MTK_SPACE)
	{
		return;
	}

	// Special processing for naked track bars (the custom track bar edit frames
	// were handled above).
	TTrackBar *trackBar = dynamic_cast<TTrackBar*>(controlWithFocus);
	if (trackBar != NULL && (Key == MTK_UP || Key == MTK_DOWN || Key == MTK_LEFT || Key == MTK_RIGHT))
	{
		// Do nothing on up events for arrows!
		Key = 0;
		return;
	}

	// Give the active tool a chance to claim the key.
	if (!sendKeyEvent(FPE_FormKeyUp, Sender, Key, Shift))
	{
		// Not claimed by the active tool, so pass the key up event on
		// to the main window.
		passKeyUpEventToMainWindow(Sender, Key, Shift);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormPaint(TObject *Sender) {sendSimpleEvent(FPE_FormPaint);}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (GBaseTool == NULL)
		return;

	sendMouseEvent(FPE_FormMouseDown, Sender, Button, Shift, X, Y);
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	if (GBaseTool == NULL)
		return;

	sendMouseEvent(FPE_FormMouseMove, Sender, (TMouseButton)0, Shift, X, Y);
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (GBaseTool == NULL)
		return;

	sendMouseEvent(FPE_FormMouseUp, Sender, Button, Shift, X, Y);
}
// ---------------------------------------------------------------------------

// Mouse wheel is turned into KeyDown/KeyUp events here - no need to
// pass it on to the tools

void __fastcall TBaseToolWindow::FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta, TPoint &MousePos,
	 bool &Handled)
{
	// TRACE_3(errout << "MOUSE WHEEL: " << WheelDelta);

	// A HACK - turn it into up and down and, if ALT is on, left and right keys
	WORD simKey = 0;
	if (Shift.Contains(ssAlt))
	{
		if (WheelDelta > 0)
			simKey = MTK_RIGHT;
		else if (WheelDelta < 0)
			simKey = MTK_LEFT;

		Shift >> ssAlt; // Don't pass on the ALT modifier!
	}
	else
	{
		if (WheelDelta > 0)
			simKey = MTK_UP;
		else if (WheelDelta < 0)
			simKey = MTK_DOWN;
	}

	if (simKey != 0)
	{
		FormKeyDown(Sender, simKey, Shift);
		FormKeyUp(Sender, simKey, Shift);
	}

	Handled = true;
}

// ---------------------------------------------------------------------------

void TBaseToolWindow::ExecuteBaseToolCommand(int command)
{
	// This function passes a command to the Base tool, eventually
	// making its way to the CBaseTool's onToolCommand function

	// Ask the Tool Manager for the Base tool
	CToolManager toolManager;
	string toolName = "PluginAdopter"; // TTT This shouldn't be hard-coded
	int toolIndex = toolManager.findTool(toolName);
	if (toolIndex < 0)
		return;

	CToolObject *toolObj = toolManager.GetTool(toolIndex);
	if (toolObj == 0)
		return;

	// Create a Tool Command object and execute the caller's command
	CToolCommand toolCommand(toolObj, command);
	toolCommand.execute();
}

// ---------------------------------------------------------------------------

int TBaseToolWindow::AdoptPluginToolGUI(const string &toolName, int tabPriority, int *topPanel, // Really TPanel*
	 FosterParentEventHandlerT eventHandler)
{
	TPanel *fosterPanel;
	int tabIndex;

	if (!Visible)
		Show();

	TabInfo tabInfo;
	tabInfo.ToolName = toolName;
	tabInfo.TabPriority = tabPriority;
	tabInfo.TopPanel = topPanel;
	tabInfo.EventHandler = eventHandler;
	tabInfo.Display = true;

	// This code is used to check there are different priorities, note this
	// does an insert but we want to insert, insert means empty ToolName
	auto &t = MasterTabInfoMap[tabPriority];
	if ((t.ToolName != toolName) && (!t.ToolName.empty()))
	{
		auto errorString = "Priorites of tools " + t.ToolName + " and " + toolName + " are the same";
		_MTIErrorDialog(Handle, errorString);
	}

	MasterTabInfoMap[tabPriority] = tabInfo;
	RebuildTabs();

	fosterPanel = reinterpret_cast<TPanel*>(topPanel);
	if (fosterPanel == NULL)
		return -1;

	fosterPanel->Parent = ToolAdopterPanel;
	fosterPanel->Visible = false;
	fosterPanel->Repaint();

	/*** Can't do this yet - tools have not been initialized
	 ToolTabs->TabIndex = 0;
	 showTool(0);
	 ** */

	return 0;
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::UnadoptPluginToolGUI(int tabIndex)
{
	if (tabIndex == ToolTabs->TabIndex)
	{
		ToolTabs->TabIndex = -1;
	}

	auto toolInfo = getTabInfoForTabIndex(tabIndex);
	if (toolInfo == nullptr)
	{
		return;
	}

	MasterTabInfoMap.erase(toolInfo->TabPriority);

	// We can have problems here if we lose the license and the window is
	// killed before we get here

	try
	{
		// This is not exactly right... if the count is smaller than the index,
		// then something is wrong
		if (ToolTabs->Tabs->Count > tabIndex)
		{
			//// ToolTabs->Tabs->Strings[tabIndex] = AnsiString(tabIndex+1);
		}
	}
	catch (...)
	{}
}

// ---------------------------------------------------------------------------

void TBaseToolWindow::FocusAdoptedControl(int *control)
{
	TWinControl *winControl = reinterpret_cast<TWinControl*>(control);
	if (winControl == NULL || !winControl->Showing || ActiveControl == winControl)
	{
		return;
	}

	///////////////////// HACK ALERT /////////////////////////////////////
	////// We have an issue where check boxes don't lose their focus  ////
	////// rectangle when using this method to switch to another      ////
	////// but doing SelectFirst clears the control                   ////
	//////////////////////////////////////////////////////////////////////
	// TWinControl *prevControl = FindNextControl(winControl, false, true, false);
	// if (prevControl == NULL)
	// {
	// prevControl = ActiveControl;
	// }
	//
	// if (prevControl != NULL && prevControl != winControl)
	// {
	// SelectNext(prevControl, true, true);
	// }
	//
	// TWinControl *activeControl = ActiveControl;
	//
	// if (activeControl != winControl)
	// {
	// winControl->SetFocus();
	// activeControl->SetFocus();
	// winControl->SetFocus();
	// }
	//
	// activeControl = ActiveControl;
	//////////////////////////////////////////////////////////////////////
	//
	////// This should have worked, but doesn't
	winControl->SetFocus();
}
// ---------------------------------------------------------------------------

int *TBaseToolWindow::GetFocusedAdoptedControl(void) {return reinterpret_cast<int *>(ActiveControl);}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::IsItOkToSwitchTools()
{
	bool retVal = true;

	// Give active tool a crack at cleaning things up
	CToolManager toolMgr;
	int toolIndex = getToolIndexForActiveTab();
	if ((toolIndex >= 0) && !toolMgr.hideToolQuery(toolIndex))
		retVal = false; // Active tool says "can't switch now"

	// Can't switch if there's processing happening or there are
	// unresolved fixes on the provisional frame
	// THIS SHOULD BE CHECKED AS PART OF hideToolQuery(), not all over the place!
	else if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
		retVal = false; // Active tool faile to resolve provisional, or is still
	// processing and failed to tell us

	return retVal;
}
// ---------------------------------------------------------------------------

int TBaseToolWindow::GetCurrentTabIndex() {return ToolTabs->TabIndex;}
// ---------------------------------------------------------------------------

void TBaseToolWindow::SwitchToTab(int tabIndex)
{
	if ((tabIndex == ToolTabs->TabIndex) || !IsItOkToSwitchTools() || (ToolTabs->Tabs->Count <= tabIndex))
	{
		return;
	}

	// It's OK to switch
	ToolTabs->TabIndex = tabIndex;
	showTool(tabIndex);
}

// ---------------------------------------------------------------------------

void TBaseToolWindow::ShowTool(const string &toolName)
{
	int tabIndex = getTabIndexFromToolName(toolName);
	if (tabIndex >= 0)
		SwitchToTab(tabIndex);
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::HideTool(const string &toolName)
{
	int tabIndex = getTabIndexFromToolName(toolName);
	if (tabIndex >= 0)
		hideTool(tabIndex);
}

void TBaseToolWindow::HideToolCompletely(const string &toolName)
{
	int mapIndex = getMapIndexFromToolName(toolName);
	if (mapIndex < 0)
	{
		return;
	}

	MasterTabInfoMap[mapIndex].Display = false;
	RebuildTabs();
}

void TBaseToolWindow::ShowToolCompletely(const string &toolName)
{
	int mapIndex = getMapIndexFromToolName(toolName);
	if (mapIndex < 0)
	{
		return;
	}

	MasterTabInfoMap[mapIndex].Display = true;
	RebuildTabs();
}

// ---------------------------------------------------------------------------
void TBaseToolWindow::DisableTool(EToolDisabledReason reason)
{
	ToolAdopterPanel->Enabled = false;

	MCPanel->Visible = false; // Can't modify master clip
	NHPanel->Visible = false; // obsolete (non-history tool)
	TNLPanel->Visible = false; // Tool not licensed
	TNAPanel->Visible = false; // Tool not available
	NCPanel->Visible = false; // No clip loaded
	TIDPanel->Visible = false; // Tool is disabled
	VOMPanel->Visible = false; // View-only mode
	switch (reason)
	{
	case TOOL_DISABLED_MASTER_CLIP:
		MCPanel->Visible = true;
		break;
	case TOOL_DISABLED_NON_HISTORY:
		NHPanel->Visible = true;
		break;
	case TOOL_DISABLED_TOOL_UNLICENSED:
		TNLPanel->Visible = true;
		break;
	case TOOL_DISABLED_UNAVAILABLE:
		TNAPanel->Visible = true;
		break;
	case TOOL_DISABLED_NO_CLIP:
		NCPanel->Visible = true;
		break;
	case TOOL_DISABLED_DISABLED:
		TIDPanel->Visible = true;
		break;
	case TOOL_DISABLED_VIEW_ONLY_MODE:
		VOMPanel->Visible = true;
		break;
	default:
#ifdef _DEBUG
		throw "Unhandled reason in DisableTool";
#endif
		break;
	}

	SuperPanel->Visible = true;
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::EnableTool()
{
	ToolAdopterPanel->Enabled = pluginStateEnabled;
	MCPanel->Visible = false; // Can't modify master clip
	NHPanel->Visible = false; // obsolete (non-history tool)
	TNLPanel->Visible = false; // Tool not licensed
	TNAPanel->Visible = true; // Tool not available
	NCPanel->Visible = false; // No clip loaded
	TIDPanel->Visible = false; // Tool is disabled
	VOMPanel->Visible = false; // View-only mode
	SuperPanel->Visible = !pluginStateEnabled;
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::WarnThereIsNoUndo()
{
	bool retVal = true; // We're fine if it isn't a Master Clip

	if (GBaseTool->getSystemAPI()->isMasterClipLoaded())
	{
		if (NoUndoForm == NULL)
			NoUndoForm = new TNoUndoForm(this);
		TModalResult modalResult = ShowModalDialog(NoUndoForm);
		retVal = (modalResult == mrOk);
	}

	return retVal;
}
// ---------------------------------------------------------------------------

EAcceptOrRejectChangeResult TBaseToolWindow::ShowAcceptOrRejectDialog(bool *turnOnAutoAccept)
{
	EAcceptOrRejectChangeResult retVal = PENDING_CHANGE_NO_ACTION;

	AcceptOrRejectForm->ShowTurnOnAutoAcceptCheckbox(turnOnAutoAccept != NULL);

	switch (ShowModalDialog(AcceptOrRejectForm))
	{
	case mrOk:
	case mrYes:
		retVal = PENDING_CHANGE_ACCEPT;
		break;
	case mrAbort:
	case mrNo:
		retVal = PENDING_CHANGE_REJECT;
		break;
	}
	if (turnOnAutoAccept != NULL)
		* turnOnAutoAccept = AcceptOrRejectForm->GetTurnOnAutoAcceptChecked();

	return retVal;
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::CoverUp(bool enable)
{
	LoadCoverPanel->Visible = enable;

	return enable;
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::ShowLoadProgress(int index)
{
	Graphics::TBitmap *bitmap = new Graphics::TBitmap;
	bitmap->Width = LoadLightImageList->Width;
	bitmap->Height = LoadLightImageList->Height;

	if (index >= LoadLightImageList->Count)
		index = LoadLightImageList->Count - 1;

	LoadLightImageList->GetBitmap(index, bitmap);
	bitmap->Transparent = true;

	TRGBQuad *p = (TRGBQuad*)bitmap->ScanLine[0];
	BYTE r = p[0].rgbRed;
	BYTE g = p[0].rgbGreen;
	BYTE b = p[0].rgbBlue;

	bitmap->TransparentColor = (TColor)0x2e2e2e;
	LoadLightImage->Left = (LoadCoverPanel->Width - bitmap->Width) / 2;
	LoadLightImage->Top = (LoadCoverPanel->Height - bitmap->Height) / 2;
	LoadLightImage->Width = bitmap->Width;
	LoadLightImage->Height = bitmap->Height;

	LoadLightImage->Picture->Bitmap = bitmap;
	LoadLightImage->Repaint();

	delete bitmap;

	return true;
}

// ----------------------------------------------------------------------------

void TBaseToolWindow::showTool(int tabIndex)
{
	auto toolInfo = getTabInfoForTabIndex(tabIndex);
	if (toolInfo == nullptr)
	{
		return;
	}

	auto panel = (TPanel*)(toolInfo->TopPanel);
	if ((panel != nullptr) && panel->Visible)
	{
		return; // Should we worry about this?
	}

	CToolManager toolManager;
	int toolIndex = toolManager.findTool(toolInfo->ToolName);

	if (toolIndex < 0)
		return;

	// HACK because paint wants main window to automatically get focus whenever
	// the cursor is moved into it - by default it is off, Paint needs to
	// turn it on in the formShow method
	mainWindow->SetAutofocus(false);

	// Security hack
	auto cps = toolManager.currentPluginState(toolIndex, true);
	pluginStateEnabled = (cps.State() == PLUGIN_STATE_ENABLED);

	// Assume tool will be enabled
	EnableTool();

	mainWindow->ShowTool(toolIndex);

	// In case this is the first tool being shown
	CoverUp(false);
	sendSimpleEvent(FPE_FormShow);
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::hideTool(int tabIndex)
{
	auto toolInfo = getTabInfoForTabIndex(tabIndex);
	if (toolInfo == nullptr)
	{
		return;
	}

	auto panel = (TPanel*)(toolInfo->TopPanel);
	if (!panel->Visible)
	{
		return; // Should we worry about this?
	}

	CToolManager toolManager;
	int toolIndex = toolManager.findTool(toolInfo->ToolName);
	if (toolIndex < 0)
	{
		return;
	}

	mainWindow->HideTool(toolIndex);

	sendSimpleEvent(FPE_FormHide); // moved this to AFTER the hide
}

// ---------------------------------------------------------------------------

TCheckBox *TBaseToolWindow::findToolActiveCheckBox(TPanel *panel)
{
	TCheckBox *cb = NULL;

	for (int i = 0; i < panel->ControlCount; ++i)
	{
		cb = dynamic_cast<TCheckBox*>(panel->Controls[i]);
		if (cb == NULL)
			continue;

		if (cb->Name == "ToolActiveCheckBox")
			break;
	}

	return cb;
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::flipToolActiveCheckBox(TPanel *panel, bool newChecked)
{
	TCheckBox *cb = findToolActiveCheckBox(panel);
	if (cb != NULL)
	{
		if (cb->Checked != newChecked)
			cb->Checked = newChecked;
	}
}
// ---------------------------------------------------------------------------

int TBaseToolWindow::getTabIndexFromToolName(const string &toolName)
{
	AnsiString toolNameAsAnsiString(toolName.c_str());
	// int tabCount = NUM_TABS; // ToolTabs->Tabs->Count;
	auto tabCount = ToolTabs->Tabs->Count;

	for (int tabIndex = 0; tabIndex < tabCount; ++tabIndex)
	{
		if (toolNameAsAnsiString == ToolTabs->Tabs->Strings[tabIndex])
			return tabIndex;
	}

	return -1;
}

int TBaseToolWindow::getMapIndexFromToolName(const string &toolName)
{
	for (auto&map : MasterTabInfoMap)
	{
		if (map.second.ToolName == toolName)
		{
			return map.first;
		}
	}

	return -1;
}
// ---------------------------------------------------------------------------

int TBaseToolWindow::getToolIndexForActiveTab()
{
	int tabIndex = ToolTabs->TabIndex;

	if (tabIndex < 0)
		return -1;

	string toolName = StringToStdString(ToolTabs->Tabs->Strings[tabIndex]);
	CToolManager toolManager;
	int toolIndex = toolManager.findTool(toolName);

	return toolIndex;
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::sendSimpleEvent(FosterParentEventId event)
{
	auto tabInfo = getTabInfoForActiveTab();
	if (tabInfo == nullptr)
	{
		return false;
	}

	auto eventHandler = tabInfo->EventHandler;
	if (eventHandler == nullptr)
	{
		return false;
	}

	FosterParentEventInfo eventInfo;
	eventInfo.Id = event;
	return (*eventHandler)(eventInfo);
}

// ---------------------------------------------------------------------------

bool TBaseToolWindow::sendKeyEvent(FosterParentEventId event, TObject *Sender, WORD &Key, TShiftState Shift)
{
	auto tabInfo = getTabInfoForActiveTab();
	if (tabInfo == nullptr)
	{
		return false;
	}

	auto eventHandler = tabInfo->EventHandler;
	if (eventHandler == nullptr)
	{
		return false;
	}

	FosterParentEventInfo eventInfo;
	eventInfo.Id = event;
	eventInfo.Sender = reinterpret_cast<int *>(Sender);
	eventInfo.Key = Key;
	eventInfo.Shift = Shift.Contains(ssShift);
	eventInfo.Alt = Shift.Contains(ssAlt);
	eventInfo.Ctrl = Shift.Contains(ssCtrl);
	eventInfo.Left = Shift.Contains(ssLeft);
	eventInfo.Right = Shift.Contains(ssRight);
	eventInfo.Middle = Shift.Contains(ssMiddle);
	eventInfo.Double_Clicked = Shift.Contains(ssDouble);
	return (*eventHandler)(eventInfo);
}

// ---------------------------------------------------------------------------

bool TBaseToolWindow::sendMouseEvent(FosterParentEventId event, TObject *Sender, TMouseButton Button, TShiftState Shift,
	 int X, int Y)
{
	auto tabInfo = getTabInfoForActiveTab();
	if (tabInfo == nullptr)
	{
		return false;
	}

	auto eventHandler = tabInfo->EventHandler;
	if (eventHandler == nullptr)
	{
		return false;
	}

	FosterParentEventInfo eventInfo;
	eventInfo.Id = event;
	eventInfo.Sender = reinterpret_cast<int *>(Sender);
	if (event != FPE_FormMouseMove)
		eventInfo.MouseButton = (Button == mbLeft) ? FPEI_LeftMouseButton :
			 ((Button == mbRight) ? FPEI_RightMouseButton : FPEI_MiddleMouseButton);
	eventInfo.X = X;
	eventInfo.Y = Y;
	eventInfo.Shift = Shift.Contains(ssShift);
	eventInfo.Alt = Shift.Contains(ssAlt);
	eventInfo.Ctrl = Shift.Contains(ssCtrl);
	eventInfo.Left = Shift.Contains(ssLeft);
	eventInfo.Right = Shift.Contains(ssRight);
	eventInfo.Middle = Shift.Contains(ssMiddle);
	eventInfo.Double_Clicked = Shift.Contains(ssDouble);
	return (*eventHandler)(eventInfo);
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::ToolTabsChange(TObject *Sender) {showTool(ToolTabs->TabIndex);}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::ToolTabsChanging(TObject *Sender, bool &AllowChange)
{
	// Tool change allowed if the active tool says so
	AllowChange = IsItOkToSwitchTools();
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::FormHide(TObject *Sender)
{
	// DO NOT PUT ANYTHING HERE! We no longer Hide() the Tool Palette,
	// we move it offscreen to make it not visible!
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::MouseInToolTimerTimer(TObject *Sender)
{
	// Uggh... I think all this crap is unnecessary
	// if ((GBaseTool != NULL) && InformedMouseInTool)
	// {
	// int X = Mouse->CursorPos.x;
	// int Y = Mouse->CursorPos.y;
	//
	// if (X < BoundsRect.Left || X >= BoundsRect.Right
	// || Y < BoundsRect.Top || Y >= BoundsRect.Bottom)
	// {
	// GBaseTool->getSystemAPI()->NotifyMouseInToolWindow();
	// InformedMouseInTool = false;
	// }
	// }
	// if ((GBaseTool == NULL) || !InformedMouseInTool)
	// MouseInToolTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::MaskToolbarButtonClick(TObject *Sender)
{
	TSpeedButton *button = dynamic_cast<TSpeedButton*>(Sender);
	if (button == NULL)
		return;

	int maskCmd = button->Tag;

	if (maskCmd == NAV_CMD_MASK_HIDE_SELECTED)
	{
		if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		{

			maskCmd = NAV_CMD_MASK_DELETE_SELECTED;
		}
	}

	mainWindow->ExecuteNavigatorToolCommand(maskCmd);

}
// ---------------------------------------------------------------------------


void __fastcall TBaseToolWindow::LutToolbarComboBoxSelect(TObject *Sender)
{
	int selectedItemIndex = LutToolbarComboBox->ItemIndex;
	mainWindow->SelectLut(selectedItemIndex);
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::LutToolbarComboBoxKeyPress(TObject *Sender, System::WideChar &Key)

{
	// char temp = (char) Key;
	// mainWindow->LutToolbarComboBoxKeyPress(Sender, temp);
	// Key = (System::WideChar) temp;

	// Only allow up and down arrow keys into the lut toolbar combo box...
	// all others will de-select the combo box!
	if (Key != MTK_UP && Key != MTK_DOWN)
	{
		mainWindow->FocusOnMainWindow();
		// Key = 0;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::LutEditButtonClick(TObject *Sender)
{
   mainWindow->EditLutSettings();
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::HandleWMMenuChar(TWMMenuChar &Message)
{
	bool suppressBeep = true;

	// TWMMenuChar::User is the virtual key code.
	// Only the TWMKey::CharCode matters to the menu.
	TWMKey twmKeyMessage;
	twmKeyMessage.CharCode = Message.User;

	if (mainWindow->MainMenu->IsShortCut(twmKeyMessage))
	{
		mainWindow->MainMenu->ProcessMenuChar(Message);
		suppressBeep = Message.Result == MAKELRESULT(0, MNC_IGNORE);
	}

	if (suppressBeep)
	{
		// Returning MNC_CLOSE suppresses the Beep. Apparently telling the
		// system to close the main menu actually has no other effect.
		// QQQ Should check for chars we care about and return MNC_IGNORE for
		// those we don't!
		Message.Result = MAKELRESULT(0, MNC_CLOSE);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::WMMoveCallback(TMessage &Message)
{
	//// MaskBorderDialog->PositionMask();
}
// ---------------------------------------------------------------------------

TabInfo *TBaseToolWindow::getTabInfoForActiveTab() {return getTabInfoForTabIndex(ToolTabs->TabIndex);}

// ---------------------------------------------------------------------------

TabInfo *TBaseToolWindow::getTabInfoForTabIndex(int tabIndex)
{
	if ((tabIndex < 0) || (tabIndex >= ToolTabs->Tabs->Count))
	{
		return nullptr;
	}

	auto mapIndex = getMapIndexFromToolName(StringToStdString(ToolTabs->Tabs->Strings[tabIndex]));
	return &MasterTabInfoMap[mapIndex];
}

// ---------------------------------------------------------------------------

void TBaseToolWindow::RebuildTabs()
{
	int activeIndex = ToolTabs->TabIndex;
	ToolTabs->Tabs->Clear();

	for (auto&map : MasterTabInfoMap)
	{
		auto &toolInfo = map.second;
		if (toolInfo.Display)
		{
			ToolTabs->Tabs->Add(String(toolInfo.ToolName.c_str()));
		}
	}

	// Restore old active index
	if (activeIndex < ToolTabs->Tabs->Count)
	{
		ToolTabs->TabIndex = activeIndex;
	}
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::SetMaskTool(CMaskTool &maskTool)
{
	_maskTool = &maskTool;
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::ShowHidePropertiesWindow()
{
	if (areMaskPropertiesVisible())
	{
		HideMaskProperties(true);
	}
	else
	{
		ShowMaskProperties(true);
		_maskTool->setRegionOfInterestAvailable(true);    // ??????? QQQ
	}
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::HideMaskProperties(bool animate)
{
	// This is a pretty bad interface but I really don't care
	auto oldValue = Mask_ShowOrHidePropertiesPanelButton->Down;

	_targetHeight = _oldClientHeight;
   MaskPropertiesFrame->setMaskFrameEnabled(false);
	Mask_ShowOrHidePropertiesPanelButton->Down = false;
   mainWindow->GetDisplayer()->setDisplayMaskOverlay(false);

	AnimateTimer->Enabled = animate;
	if (animate == false)
	{
		this->ClientHeight = _targetHeight;
	}

	mainWindow->GetDisplayer()->displayFrame();
	return oldValue;
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::ShowMaskProperties(bool animate)
{
	// This is a pretty bad interface but I really don't care
	auto oldValue = Mask_ShowOrHidePropertiesPanelButton->Down;

	_targetHeight = _oldClientHeight + MaskPropertiesFrame->Height;
	MaskPropertiesFrame->setMaskFrameEnabled(true);
	Mask_ShowOrHidePropertiesPanelButton->Down = true;
 	mainWindow->GetDisplayer()->setDisplayMaskOverlay(true);

	AnimateTimer->Enabled = animate;

	if (animate == false)
	{
		this->ClientHeight = _targetHeight;
	}

	mainWindow->GetDisplayer()->displayFrame();
	return oldValue;
}
// ---------------------------------------------------------------------------

void __fastcall TBaseToolWindow::AnimateTimerTimer(TObject *Sender)
{
	auto move = 10;
	if (this->ClientHeight > _targetHeight)
	{
		move = -move;
	}

	if (abs(this->ClientHeight - _targetHeight) <= abs(move))
	{
		this->ClientHeight = _targetHeight;
		AnimateTimer->Enabled = false;
	}
	else
	{
		this->ClientHeight += move;
	}
}
// ---------------------------------------------------------------------------

bool TBaseToolWindow::areMaskPropertiesVisible()
{
  return MaskPropertiesFrame->isMaskFrameEnabled();
}
// ---------------------------------------------------------------------------

void TBaseToolWindow::UpdateMaskToolGUI(CMaskTool &maskTool)
{
   SetMaskTool(maskTool);

   if (!maskTool.IsMaskToolActivationAllowed())
   {
      // The current tool does not allow the mask to be active!

		Mask_ActivateNormalModeButton->Enabled = false;
		Mask_ActivateNormalModeButton->Down = false;

      Mask_ShowOrHideOutlineButton->Enabled = false;
      Mask_ShowOrHideOutlineButton->Down = false;

      Mask_ShowOrHidePropertiesPanelButton->Enabled = false;
      Mask_ShowOrHidePropertiesPanelButton->Down = false;

		Mask_DrawRectButton->Enabled = false;
		Mask_DrawRectButton->Down = false;

		Mask_DrawLassoButton->Enabled = false;
		Mask_DrawLassoButton->Down = false;

		Mask_DrawBezierButton->Enabled = false;
		Mask_DrawBezierButton->Down = false;

		Mask_SetInclusiveButton->Enabled = false;
		Mask_SetInclusiveButton->Down = false;

		Mask_SetExclusiveButton->Enabled = false;
		Mask_SetExclusiveButton->Down = false;

		Mask_NeutralizeButton->Enabled = false;
		Mask_NeutralizeButton->Down = false;

		Mask_ActivateRoiModeButton->Enabled = false;
		Mask_ActivateRoiModeButton->Down = false;

		Mask_SetKeyframeButton->Enabled = false;
		Mask_SetKeyframeButton->Down = false;

      // Hide the properties sliding panel if currently visible.
      if (areMaskPropertiesVisible())
      {
         _oldMaskPropertiesPanelVisible = true;
         HideMaskProperties();
      }

      return;
   }

	if (maskTool.IsMaskRoiModeActive())
	{
      // Mask tool is now active in ROI mode.

		Mask_ActivateNormalModeButton->Enabled = true;
		Mask_ActivateNormalModeButton->Down = false;

      Mask_ShowOrHideOutlineButton->Enabled = false;
      Mask_ShowOrHideOutlineButton->Down = false;

      Mask_ShowOrHidePropertiesPanelButton->Enabled = false;
      Mask_ShowOrHidePropertiesPanelButton->Down = false;

		Mask_DrawRectButton->Enabled = true;
      Mask_DrawRectButton->Down = maskTool.getRegionShape() == MASK_REGION_RECT;

		Mask_DrawLassoButton->Enabled = !maskTool.getRoiIsRectangularOnly();
      Mask_DrawLassoButton->Down = maskTool.getRegionShape() == MASK_REGION_LASSO;

		Mask_DrawBezierButton->Enabled = !maskTool.getRoiIsRectangularOnly();
      Mask_DrawBezierButton->Down = maskTool.getRegionShape() == MASK_REGION_BEZIER;

		Mask_SetInclusiveButton->Enabled = false;
		Mask_SetInclusiveButton->Down = true;

		Mask_SetExclusiveButton->Enabled = false;
		Mask_SetExclusiveButton->Down = false;

		Mask_NeutralizeButton->Enabled = false;
		Mask_NeutralizeButton->Down = false;

		Mask_ActivateRoiModeButton->Enabled = true;
		Mask_ActivateRoiModeButton->Down = true;

		Mask_SetKeyframeButton->Enabled = false;
		Mask_SetKeyframeButton->Down = false;

      // Hide the properties sliding panel if currently visible.
      if (areMaskPropertiesVisible())
      {
         _oldMaskPropertiesPanelVisible = true;
         HideMaskProperties();
      }

      return;
	}


   if (!maskTool.IsMaskToolActive())
   {
      // Mask activation is allowed but not currently active.

		Mask_ActivateNormalModeButton->Enabled = true;
		Mask_ActivateNormalModeButton->Down = false;

      Mask_ShowOrHideOutlineButton->Enabled = false;
      Mask_ShowOrHideOutlineButton->Down = false;

      Mask_ShowOrHidePropertiesPanelButton->Enabled = false;
      Mask_ShowOrHidePropertiesPanelButton->Down = false;

		Mask_DrawRectButton->Enabled = false;
		Mask_DrawRectButton->Down = false;

		Mask_DrawLassoButton->Enabled = false;
		Mask_DrawLassoButton->Down = false;

		Mask_DrawBezierButton->Enabled = false;
		Mask_DrawBezierButton->Down = false;

		Mask_SetInclusiveButton->Enabled = false;
		Mask_SetInclusiveButton->Down = false;

		Mask_SetExclusiveButton->Enabled = false;
		Mask_SetExclusiveButton->Down = false;

		Mask_NeutralizeButton->Enabled = false;
		Mask_NeutralizeButton->Down = false;

		Mask_ActivateRoiModeButton->Enabled = maskTool.IsMaskRoiModeActivationAllowed();
		Mask_ActivateRoiModeButton->Down = false;

		Mask_SetKeyframeButton->Enabled = false;
		Mask_SetKeyframeButton->Down = false;

      // Hide the properties sliding panel if currently visible.
      if (areMaskPropertiesVisible())
      {
         _oldMaskPropertiesPanelVisible = true;
         HideMaskProperties();
      }
      return;
   }

   // The mask is active in NORMAL mode.

   // Re-show the mask properties sliding panel if we hid it while mask was
   // inactive or in ROI mode. Do this first!
   if (_oldMaskPropertiesPanelVisible == true)
   {
      _oldMaskPropertiesPanelVisible = false;
      ShowMaskProperties();
   }

   Mask_ActivateNormalModeButton->Enabled = true;
   Mask_ActivateNormalModeButton->Down = true;

   Mask_ShowOrHideOutlineButton->Enabled = !maskTool.IsMaskVisibilityLockedOff();
   Mask_ShowOrHideOutlineButton->Down = maskTool.IsMaskVisible();

   Mask_ShowOrHidePropertiesPanelButton->Enabled = true;
   Mask_ShowOrHidePropertiesPanelButton->Down = areMaskPropertiesVisible();

   Mask_DrawRectButton->Enabled = true;
   Mask_DrawRectButton->Down = maskTool.getRegionShape() == MASK_REGION_RECT;

   Mask_DrawLassoButton->Enabled = true;
   Mask_DrawLassoButton->Down = maskTool.getRegionShape() == MASK_REGION_LASSO;

   Mask_DrawBezierButton->Enabled = true;
   Mask_DrawBezierButton->Down = maskTool.getRegionShape() == MASK_REGION_BEZIER;

   Mask_SetInclusiveButton->Enabled = true;
   Mask_SetInclusiveButton->Down = maskTool.getGlobalInclusionFlag();

   Mask_SetExclusiveButton->Enabled = true;
   Mask_SetExclusiveButton->Down = !Mask_SetInclusiveButton->Down;

   Mask_NeutralizeButton->Enabled = true;
   Mask_NeutralizeButton->Down = false;

   Mask_ActivateRoiModeButton->Enabled = maskTool.IsMaskRoiModeActivationAllowed();
   Mask_ActivateRoiModeButton->Down = false;

   Mask_SetKeyframeButton->Enabled = true;
   Mask_SetKeyframeButton->Down = false;
}
//---------------------------------------------------------------------------

