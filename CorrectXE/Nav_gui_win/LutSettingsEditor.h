//---------------------------------------------------------------------------

#ifndef LutSettingsEditorH
#define LutSettingsEditorH
//---------------------------------------------------------------------------
#include "machine.h"
#include "DisplayLutSettings.h"
#include "ImageInfo.h"  // for EColorSpace
#include "IniFile.h"    // for StringList
#include "LutSettingsManager.h"
#include "MainWindowUnit.h"
#include "MTIUNIT.h"

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include "SpinEditFrameUnit.h"
#include "TrackEditFrameUnit.h"

#include <map>
#include <string>
using std::map;
using std::string;

#define MODAL_CUSTOM_LUT_DIALOG 0  // I guess some folks like to keep it up

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class TLutSettingsEditor : public TMTIForm
{
__published:	// IDE-managed Components
        TOpenDialog *OpenFileDialog;
        TSaveDialog *SaveFileDialog;
        TGroupBox *SourceColorSpaceGroup;
        TPanel *ColorSpaceTopRadioPanel;
        TRadioButton *ColorSpaceDefaultRadioButton;
        TRadioButton *ColorSpaceCustomRadioButton;
        TPanel *ColorSpaceBottomRadioPanel;
        TRadioButton *CCIR_601MColorSpaceRadioButton;
        TRadioButton *CCIR_601BGColorSpaceRadioButton;
        TRadioButton *CCIR_709ColorSpaceRadioButton;
        TRadioButton *SMPTE_240ColorSpaceRadioButton;
        TRadioButton *LinearColorSpaceRadioButton;
        TRadioButton *LogColorSpaceRadioButton;
        TGroupBox *SourceRangeGroup;
        TPanel *SourceRangeDefaultsPanel;
        TLabel *DefaultBlackLabel;
        TLabel *DefaultBVLabel;
        TLabel *DefaultGULabel;
        TLabel *DefaultRYLabel;
        TLabel *DefaultWhiteLabel;
        TEdit *DefaultWhiteRYEdit;
        TEdit *DefaultWhiteGUEdit;
        TEdit *DefaultWhiteBVEdit;
        TEdit *DefaultBlackRYEdit;
        TEdit *DefaultBlackGUEdit;
        TEdit *DefaultBlackBVEdit;
        TPanel *SourceRangeSettingsPanel;
        TSpeedButton *SourceRangeMaxLockButton;
        TLabel *SourceRangeMinLabel;
        TSpeedButton *SourceRangeMinLockButton;
        TLabel *SourceRangeBVLabel;
        TLabel *SourceRangeGULabel;
        TLabel *SourceRangeRYLabel;
        TLabel *SourceRangeMaxLabel;
        TRadioButton *SourceRangeDefaultRadioButton;
        TRadioButton *SourceRangeCustomRadioButton;
        TGroupBox *DisplayRangeGroup;
        TLabel *DisplayRangeRLabel;
        TLabel *DisplayRangeGLabel;
        TLabel *DisplayRangeBLabel;
        TLabel *DisplayRangeMinLabel;
        TLabel *DisplayRangeMaxLabel;
        TSpeedButton *DisplayRangeMinLockButton;
        TSpeedButton *DisplayRangeMaxLockButton;
        TPanel *DisplayRangeRadioButtonPanel;
        TRadioButton *DisplayRangeDefaultRadioButton;
        TRadioButton *DisplayRangeCustomRadioButton;
        TPanel *DisplayRangeDefaultCoverUpPanel;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label19;
        TLabel *Label20;
        TEdit *Edit1;
        TEdit *Edit2;
        TEdit *Edit3;
        TEdit *Edit4;
        TEdit *Edit5;
        TEdit *Edit6;
        TGroupBox *DisplayGammaGroup;
        TLabel *DisplayGammaValue;
        TTrackBar *DisplayGammaSlider;
        TPanel *DisplayGammaRadioButtonPanel;
        TRadioButton *DisplayGammaDefaultRadioButton;
        TRadioButton *DisplayGammaCustomRadioButton;
        TPanel *ButtonAreaSeparator;
        TButton *LutCancelButton;
        TButton *LutOkButton;
        TGroupBox *LutSelectionGroup;
	TComboBox *LutNameComboBox;
        TButton *ImportLutButton;
        TButton *ExportLutButton;
        TButton *CopyToProjectButton;
        TButton *DeleteUserLutButton;
        TComboBox *ShortcutComboBox;
        TLabel *ShortcutLabel;
        TButton *NewUserLutButton;
        TButton *CopyToClipButton;
   TRadioButton *ExrColorSpaceRadioButton;
   TPanel *MonoSourceRangeGBCoverUpPanel;
   TEdit *GMaxDisabledEdit;
   TEdit *BMaxDisabledEdit;
   TEdit *GMinDisabledEdit;
   TEdit *BMinDisabledEdit;
   TGroupBox *SourceRangeExrGroup;
   TPanel *SourceRangeExrSliderPanel;
   TLabel *DynamicRangeLabel;
   TRadioButton *SourceRangeExrDefaultRadioButton;
   TRadioButton *SourceRangeExrCustomRadioButton;
   TLabel *ExposureAdjustLabel;
   TLabel *ExposureAdjustReadoutLabel;
   TTrackBar *ExposureAdjustTrackBar;
   TLabel *DynamicRangeReadoutLabel;
   TTrackBar *DynamicRangeTrackBar;
   TTimer *ExrLutUpdateHackTimer;
	TSpinEditFrame *SourceRangeMinRYSpinEdit;
	TSpinEditFrame *SourceRangeMinGUSpinEdit;
	TSpinEditFrame *SourceRangeMinBVSpinEdit;
	TSpinEditFrame *SourceRangeMaxRYSpinEdit;
	TSpinEditFrame *SourceRangeMaxGUSpinEdit;
	TSpinEditFrame *SourceRangeMaxBVSpinEdit;
	TSpinEditFrame *DisplayRangeMinRSpinEdit;
	TSpinEditFrame *DisplayRangeMinGSpinEdit;
	TSpinEditFrame *DisplayRangeMinBSpinEdit;
	TSpinEditFrame *DisplayRangeMaxRSpinEdit;
	TSpinEditFrame *DisplayRangeMaxGSpinEdit;
	TSpinEditFrame *DisplayRangeMaxBSpinEdit;
        void __fastcall ColorSpaceDefaultOrCustomRadioButtonClick(TObject *Sender);
        void __fastcall SourceRangeRadioButtonClick(TObject *Sender);
        void __fastcall SourceRangeMinLockButtonClick(TObject *Sender);
        void __fastcall SourceRangeMaxLockButtonClick(TObject *Sender);
        void __fastcall DisplayRangeMinLockButtonClick(TObject *Sender);
        void __fastcall DisplayRangeMaxLockButtonClick(TObject *Sender);
        void __fastcall OnGammaSliderChange(TObject *Sender);
        void __fastcall ImportLutButtonClick(TObject *Sender);
        void __fastcall ExportLutButtonClick(TObject *Sender);
        void __fastcall LutOkButtonClick(TObject *Sender);
        void __fastcall LutCancelButtonClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall DisplayRangeRadioButtonClick(TObject *Sender);
        void __fastcall DeleteUserLutButtonClick(TObject *Sender);
        void __fastcall CopyToProjectButtonClick(TObject *Sender);
        void __fastcall CopyToClipButtonClick(TObject *Sender);
        void __fastcall NewUserLutButtonClick(TObject *Sender);
        void __fastcall DisplayGammaRadioButtonClick(
          TObject *Sender);
        void __fastcall SpinEditEnter(TObject *Sender);
        void __fastcall SpinEditExit(TObject *Sender);
        void __fastcall LutNameComboBoxContextPopup(TObject *Sender,
          TPoint &MousePos, bool &Handled);
        void __fastcall ShortcutComboBoxDropDown(TObject *Sender);
        void __fastcall ShortcutComboBoxChange(TObject *Sender);
        void __fastcall CustomColorSpaceRadioButtonClick(
          TObject *Sender);
   void __fastcall FormMouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
   void __fastcall FormMouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
   void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall SourceRangeExrTrackBarChange(TObject *Sender);
   void __fastcall SourceRangeExrRadioButtonClick(TObject *Sender);
   void __fastcall ExrLutUpdateHackTimerTimer(TObject *Sender);
	void __fastcall LutNameComboBoxSelect(TObject *Sender);

private:	// User declarations

        /////////////////////////////////////////////////////
        // Global state
        CDisplayLutSettings CurrentLutSettings;
        ELutSelection CurrentLutSelection;
        string CurrentUserLutName;
        StringList CurrentUserLutNameList;
        bool InhibitPreviewing;
        bool InhibitCallbacks;

        // Set by caller
        CLutSettingsManager *LutSettingsManager;
        CDisplayer *Displayer;
        int DefaultGammaTimes10;

        // Cached stuff from the Displayer
        EColorSpace CurrentClipDefaultColorSpace;
        int CurrentClipBitsPerComponent;
        int CurrentCLipPixelComponents;
        bool CurrentClipIsRGB;
        bool CurrentClipIsYUV;
        bool CurrentClipIsMono;
        bool CurrentClipIsEXR;

        /////////////////////////////////////////////////////
        // LUT Selection group
        void InitLutSelectionGroup();
        void RefreshLutSelectionGroup();
        void ConformLutSelectionGroup();

        void PopulateLutSelectionComboBox();
        void PopulateShortcutComboBox();
        void HandleSettingChangeCallback();

        /////////////////////////////////////////////////////
        // Source Color Mode group
        void InitSourceColorSpaceGroup();
        void RefreshSourceColorSpaceGroup();
        void ConformSourceColorSpaceGroup();
        void UpdateCurrentSourceColorSpaceSettings();

        /////////////////////////////////////////////////////
        // Source Range group
        void InitSourceRangeGroup();
        void InitSourceRangeExrGroup();
        void RefreshSourceRangeGroup();
        void RefreshSourceRangeExrGroup();
        void ConformSourceRangeGroup();
        void ConformSourceRangeExrGroup();
        void UpdateCurrentSourceRangeSettings();
        void UpdateCurrentSourceRangeExrSettings();

        bool SourceRangeMinIsLocked;
        bool SourceRangeMaxIsLocked;
        bool SourceRangeMinLockTendency;
        bool SourceRangeMaxLockTendency;

        DEFINE_CBHOOK(SourceRangeMinRYSpinEditHasChanged, TLutSettingsEditor);
        DEFINE_CBHOOK(SourceRangeMaxRYSpinEditHasChanged, TLutSettingsEditor);
        DEFINE_CBHOOK(OtherSourceRangeSpinEditHasChanged, TLutSettingsEditor);

//        TSpinEditFrame *SourceRangeMinRYSpinEdit;
//        TSpinEditFrame *SourceRangeMinGUSpinEdit;
//        TSpinEditFrame *SourceRangeMinBVSpinEdit;
//        TSpinEditFrame *SourceRangeMaxRYSpinEdit;
//        TSpinEditFrame *SourceRangeMaxGUSpinEdit;
//        TSpinEditFrame *SourceRangeMaxBVSpinEdit;

        /////////////////////////////////////////////////////
        // Display Range group
        void InitDisplayRangeGroup();
        void RefreshDisplayRangeGroup();
        void ConformDisplayRangeGroup();
        void UpdateCurrentDisplayRangeSettings();

        bool DisplayRangeMinIsLocked;
        bool DisplayRangeMaxIsLocked;
        bool DisplayRangeMinLockTendency;
        bool DisplayRangeMaxLockTendency;

        DEFINE_CBHOOK(DisplayRangeMinRSpinEditHasChanged,  TLutSettingsEditor);
        DEFINE_CBHOOK(DisplayRangeMaxRSpinEditHasChanged,  TLutSettingsEditor);
        DEFINE_CBHOOK(OtherDisplayRangeSpinEditHasChanged, TLutSettingsEditor);

//		TSpinEditFrame *DisplayRangeMinRSpinEdit;
//		TSpinEditFrame *DisplayRangeMinGSpinEdit;
//		TSpinEditFrame *DisplayRangeMinBSpinEdit;
//		TSpinEditFrame *DisplayRangeMaxRSpinEdit;
//		TSpinEditFrame *DisplayRangeMaxGSpinEdit;
//		TSpinEditFrame *DisplayRangeMaxBSpinEdit;

        /////////////////////////////////////////////////////
        // Display Gamma group
        void InitDisplayGammaGroup();
        void RefreshDisplayGammaGroup();
        void ConformDisplayGammaGroup();
        void UpdateCurrentDisplayGammaSettings();

        /////////////////////////////////////////////////////
        // LUT Settings Manager interface
        void GetCurrentSettings();
        void SelectLut(ELutSelection newLut, const string *userLutName = nullptr);
        void RefreshGuiFromCurrentSettings();
        void ApplyCurrentSettings();
        void TakeSnapshot();
        void RevertToSnapshotSettings();

        /////////////////////////////////////////////////////
        // Import/export
        void ImportLut(const string &filePath);
        void ExportLut(const string &filePath);
        string LastLutExportDir;

        /////////////////////////////////////////////////////
        // Displayer interface
        void GrabCurrentClipCharacteristics();

        /////////////////////////////////////////////////////

public:		// User declarations
        __fastcall TLutSettingsEditor(TComponent* Owner);
        __fastcall ~TLutSettingsEditor();

        void SetLutSettingsManager(CLutSettingsManager *newManager);
        void SetDisplayer(CDisplayer *newDisplayer);
        void SetDefaultGammaTimes10(int newDefaultGamma);

};
//---------------------------------------------------------------------------
extern PACKAGE TLutSettingsEditor *LutSettingsEditor;
//---------------------------------------------------------------------------
#endif
