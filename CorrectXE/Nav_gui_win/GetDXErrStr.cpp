//###########################################################################
//###########################################################################
//###                                                                     ###
//###   GetDXErrStr - AnsiString version                                  ###
//###                                                                     ###
//###       Function to retrieve textual descriptions of DirectX          ###
//###       return values. This function supports all DirectX APIs.       ###
//###       The GetErrStrGen tool was used to create this function.       ###
//###       The tool is available at                                      ###
//###           http://www.crosswinds.net/~foetsch/geterrstrgen           ###
//###                                                                     ###
//###       Usage:                                                        ###
//###           Params:                                                   ###
//###               RetVal: HRESULT return value of DirectX method        ###
//###               AddStr: Pointer to AnsiString object to be filled     ###
//###                   with the textual description of the error         ###
//###                   Can be NULL.                                      ###
//###           Returns:                                                  ###
//###               AnsiString: Name of the identifier (e.g. "S_OK")      ###
//###                                                                     ###
//###                                                                     ###
//###       Copyright � 2000 Michael F�tsch.                              ###
//###                                                                     ###
//###########################################################################
//###########################################################################

//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "GetDXErrStrAnsi.h"

// Include return value identifiers:
#include <d3d.h>
#include <ddraw.h>
#include <dinput.h>
#include <dmerror.h>
#include <dplay.h>
#include <mmreg.h>      // In C++Builder, dsound.h doesn't compile without it...
#include <dsound.h>
#include <dsetup.h>

//---------------------------------------------------------------------------

AnsiString GetDXErrStr(HRESULT RetVal, AnsiString *AddStr)
{
    AnsiString Return;

    // Compare RetVal
    switch (RetVal)
    {
// General:

    case CLASS_E_NOAGGREGATION:
        Return = "CLASS_E_NOAGGREGATION";
        if (AddStr) *AddStr =
            "Class does not support aggregation (or class object is remote).\n"
            "Could be DIERR_NOAGGREGATION:\n"
            "A non-NULL value was passed for the pUnkOuter parameter in DirectP"
            "layCreate, DirectPlayLobbyCreate, or IDirectPlayLobby3::Connect.";
        break;
    case REGDB_E_CLASSNOTREG:
        Return = "REGDB_E_CLASSNOTREG";
        if (AddStr) *AddStr =
            "The object class is not registered.\n"
            "Could be DIERR_DEVICENOTREG:\n"
            "The device or device instance is not registered with DirectInput.";
        break;
    case S_FALSE:
        Return = "S_FALSE";
        if (AddStr) *AddStr =
            "The method succeeded, but there was nothing to do.\n"
            "Could be DI_BUFFEROVERFLOW:\n"
            "The device buffer overflowed and some input was lost.\n"
            "Or DI_NOTATTACHED:\n"
            "The device exists but is not currently attached.\n"
            "Or DI_PROPNOEFFECT:\n"
            "The change in device properties had no effect.";
        break;
    case S_OK:
        Return = "S_OK";
        if (AddStr) *AddStr =
            "The operation was completed successfully.";
            break;
    case E_ACCESSDENIED:
        Return = "E_ACCESSDENIED";
        if (AddStr) *AddStr =
            "This error can be returned when an application has only foreground"
            "access to a device but is attempting to acquire the device while i"
            "n the background.\n"
            "Could be DIERR_HANDLEEXISTS:\n"
            "The device already has an event notification associated with it.\n"
            "Or DIERR_OTHERAPPHASPRIO:\n"
            "Another application has a higher priority level, preventing this c"
            "all from succeeding.\n"
            "Or DIERR_READONLY:\n"
            "The specified property cannot be changed.";
        break;
    case E_FAIL:
        Return = "E_FAIL (GENERIC)";
        if (AddStr) *AddStr =
            "The method did not succeed. There is an undefined error condition.";
        break;
    case E_HANDLE:
        Return = "E_HANDLE";
        if (AddStr) *AddStr =
            "The HWND parameter is not a valid top-level window that belongs to"
            " the process.";
        break;
    case E_INVALIDARG:
        Return = "E_INVALIDARG (INVALIDPARAMS)";
        if (AddStr) *AddStr =
            "Invalid argument. Often, this error results from failing to initia"
            "lize the dwSize member of a structure before passing it to the met"
            "hod.\n"
            "For DirectInput also:\n"
            "The object was not in a state that permitted the function to be called";
        break;
    case E_NOINTERFACE:
        Return = "E_NOINTERFACE";
        if (AddStr) *AddStr =
            "No object interface is available.";
        break;
    case E_NOTIMPL:
        Return = "E_NOTIMPL (UNSUPPORTED)";
        if (AddStr) *AddStr =
            "The method is not implemented. This value might be returned if a d"
            "river does not support a feature necessary for the operation.";
        break;
    case E_OUTOFMEMORY:
        Return = "E_OUTOFMEMORY";
        if (AddStr) *AddStr =
            "Insufficient memory to complete the task.";
        break;
    case E_PENDING:
        Return = "E_PENDING";
        if (AddStr) *AddStr =
            "Data is not yet available.\n"
            "Could be DPERR_PENDING:\n"
            "Not an error, this return indicates that an asynchronous send has "
            "reached the point where it is successfully queued. See IDirectPlay"
            "4::SendEx for more information.";
        break;
    case E_POINTER:
        Return = "E_POINTER";
        if (AddStr) *AddStr =
            "An invalid pointer (usually NULL) was passed as a parameter.";
        break;

// Direct3D:

    case D3DERR_BADMAJORVERSION:
        Return = "D3DERR_BADMAJORVERSION";
        if (AddStr) *AddStr =
            "The service that you requested is unavailable in this major versio"
            "n of DirectX. (A major version denotes a primary release, such as "
            "DirectX 6.0.)";
        break;
    case D3DERR_BADMINORVERSION:
        Return = "D3DERR_BADMINORVERSION";
        if (AddStr) *AddStr =
            "The service that you requested is available in this major version "
            "of DirectX, but not in this minor version. Get the latest version "
            "of the component run time from Microsoft. (A minor version denotes"
            " a secondary release, such as DirectX 6.1.)";
        break;
    case D3DERR_COLORKEYATTACHED:
        Return = "D3DERR_COLORKEYATTACHED";
        if (AddStr) *AddStr =
            "The application attempted to create a texture with a surface that "
            "uses a color key for transparency.";
        break;
    case D3DERR_CONFLICTINGTEXTUREFILTER:
        Return = "D3DERR_CONFLICTINGTEXTUREFILTER";
        if (AddStr) *AddStr =
            "The current texture filters cannot be used together.";
        break;
    case D3DERR_CONFLICTINGTEXTUREPALETTE:
        Return = "D3DERR_CONFLICTINGTEXTUREPALETTE";
        if (AddStr) *AddStr =
            "The current textures cannot be used simultaneously. This generally"
            " occurs when a multitexture device requires that all palettized te"

            "xtures simultaneously enabled also share the same palette.";
        break;
    case D3DERR_CONFLICTINGRENDERSTATE:
        Return = "D3DERR_CONFLICTINGRENDERSTATE";
        if (AddStr) *AddStr =
            "The currently set render states cannot be used together.";
        break;
    case D3DERR_DEVICEAGGREGATED:
        Return = "D3DERR_DEVICEAGGREGATED";
        if (AddStr) *AddStr =
            "The IDirect3DDevice7::SetRenderTarget method was called on a devic"
            "e that was retrieved from the render target surface.";
        break;
    case D3DERR_EXECUTE_CLIPPED_FAILED:
        Return = "D3DERR_EXECUTE_CLIPPED_FAILED";
        if (AddStr) *AddStr =
            "The execute buffer could not be clipped during execution.";
        break;
    case D3DERR_EXECUTE_CREATE_FAILED:
        Return = "D3DERR_EXECUTE_CREATE_FAILED";
        if (AddStr) *AddStr =
            "The execute buffer could not be created. This typically occurs whe"
            "n no memory is available to allocate the execute buffer.";
        break;
    case D3DERR_EXECUTE_DESTROY_FAILED:
        Return = "D3DERR_EXECUTE_DESTROY_FAILED";
        if (AddStr) *AddStr =
            "The memory for the execute buffer could not be deallocated.";
        break;
    case D3DERR_EXECUTE_FAILED:
        Return = "D3DERR_EXECUTE_FAILED";
        if (AddStr) *AddStr =
            "The contents of the execute buffer are invalid and cannot be execu"
            "ted.";
        break;
    case D3DERR_EXECUTE_LOCK_FAILED:
        Return = "D3DERR_EXECUTE_LOCK_FAILED";
        if (AddStr) *AddStr =
            "The execute buffer could not be locked.";
        break;
    case D3DERR_EXECUTE_LOCKED:
        Return = "D3DERR_EXECUTE_LOCKED";
        if (AddStr) *AddStr =
            "The operation requested by the application could not be completed "
            "because the execute buffer is locked.";
        break;
    case D3DERR_EXECUTE_NOT_LOCKED:
        Return = "D3DERR_EXECUTE_NOT_LOCKED";
        if (AddStr) *AddStr =
            "The execute buffer could not be unlocked because it is not current"
            "ly locked.";
        break;
    case D3DERR_EXECUTE_UNLOCK_FAILED:
        Return = "D3DERR_EXECUTE_UNLOCK_FAILED";
        if (AddStr) *AddStr =
            "The execute buffer could not be unlocked.";
        break;
    case D3DERR_INBEGIN:
        Return = "D3DERR_INBEGIN";
        if (AddStr) *AddStr =
            "The requested operation cannot be completed while scene rendering "
            "is taking place. Try again after the scene is completed and the ID"
            "irect3DDevice7::EndScene method is called.";
        break;
    case D3DERR_INBEGINSTATEBLOCK:
        Return = "D3DERR_INBEGINSTATEBLOCK";
        if (AddStr) *AddStr =
            "The operation cannot be completed while recording states for a sta"
            "te block. Complete recording by calling the IDirect3DDevice7::EndS"
            "tateBlock method, and try again.";
        break;
    case D3DERR_INITFAILED:
        Return = "D3DERR_INITFAILED";
        if (AddStr) *AddStr =
            "A rendering device could not be created because the new device cou"
            "ld not be initialized.";
        break;
    case D3DERR_INVALID_DEVICE:
        Return = "D3DERR_INVALID_DEVICE";
        if (AddStr) *AddStr =
            "The requested device type is not valid.";
        break;
    case D3DERR_INVALIDCURRENTVIEWPORT:
        Return = "D3DERR_INVALIDCURRENTVIEWPORT";
        if (AddStr) *AddStr =
            "The currently selected viewport is not valid.";
        break;
    case D3DERR_INVALIDMATRIX:
        Return = "D3DERR_INVALIDMATRIX";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the combina"
            "tion of the currently set world, view, and projection matrices is "
            "invalid (the determinant of the combined matrix is 0).";
        break;
    case D3DERR_INVALIDPALETTE:
        Return = "D3DERR_INVALIDPALETTE";
        if (AddStr) *AddStr =
            "The palette associated with a surface is invalid.";
        break;
    case D3DERR_INVALIDPRIMITIVETYPE:
        Return = "D3DERR_INVALIDPRIMITIVETYPE";
        if (AddStr) *AddStr =
            "The primitive type specified by the application is invalid.";
        break;
    case D3DERR_INVALIDRAMPTEXTURE:
        Return = "D3DERR_INVALIDRAMPTEXTURE";
        if (AddStr) *AddStr =
            "Ramp mode is being used, and the texture handle in the current mat"
            "erial does not match the current texture handle that is set as a r"
            "ender state.";
        break;
    case D3DERR_INVALIDSTATEBLOCK:
        Return = "D3DERR_INVALIDSTATEBLOCK";
        if (AddStr) *AddStr =
            "The state block handle is invalid.";
        break;
    case D3DERR_INVALIDVERTEXFORMAT:
        Return = "D3DERR_INVALIDVERTEXFORMAT";
        if (AddStr) *AddStr =
            "The combination of flexible vertex format flags specified by the a"
            "pplication is not valid.";
        break;
    case D3DERR_INVALIDVERTEXTYPE:
        Return = "D3DERR_INVALIDVERTEXTYPE";
        if (AddStr) *AddStr =
            "The vertex type specified by the application is invalid.";
        break;
    case D3DERR_LIGHT_SET_FAILED:
        Return = "D3DERR_LIGHT_SET_FAILED";
        if (AddStr) *AddStr =
            "The attempt to set lighting parameters for a light object failed.";
        break;
    case D3DERR_LIGHTHASVIEWPORT:
        Return = "D3DERR_LIGHTHASVIEWPORT";
        if (AddStr) *AddStr =
            "The requested operation failed because the light object is associa"
            "ted with another viewport.";
        break;
    case D3DERR_LIGHTNOTINTHISVIEWPORT:
        Return = "D3DERR_LIGHTNOTINTHISVIEWPORT";
        if (AddStr) *AddStr =
            "The requested operation failed because the light object has not be"
            "en associated with this viewport.";
        break;
    case D3DERR_MATERIAL_CREATE_FAILED:
        Return = "D3DERR_MATERIAL_CREATE_FAILED";
        if (AddStr) *AddStr =
            "The material could not be created. This typically occurs when no m"
            "emory is available to allocate for the material.";
        break;
    case D3DERR_MATERIAL_DESTROY_FAILED:
        Return = "D3DERR_MATERIAL_DESTROY_FAILED";
        if (AddStr) *AddStr =
            "The memory for the material could not be deallocated.";
        break;
    case D3DERR_MATERIAL_GETDATA_FAILED:
        Return = "D3DERR_MATERIAL_GETDATA_FAILED";
        if (AddStr) *AddStr =
            "The material parameters could not be retrieved.";
        break;
    case D3DERR_MATERIAL_SETDATA_FAILED:
        Return = "D3DERR_MATERIAL_SETDATA_FAILED";
        if (AddStr) *AddStr =
            "The material parameters could not be set.";
        break;
    case D3DERR_MATRIX_CREATE_FAILED:
        Return = "D3DERR_MATRIX_CREATE_FAILED";
        if (AddStr) *AddStr =
            "The matrix could not be created. This can occur when no memory is "
            "available to allocate for the matrix.";
        break;
    case D3DERR_MATRIX_DESTROY_FAILED:
        Return = "D3DERR_MATRIX_DESTROY_FAILED";
        if (AddStr) *AddStr =
            "The memory for the matrix could not be deallocated.";
        break;
    case D3DERR_MATRIX_GETDATA_FAILED:
        Return = "D3DERR_MATRIX_GETDATA_FAILED";
        if (AddStr) *AddStr =
            "The matrix data could not be retrieved. This can occur when the ma"
            "trix was not created by the current device.";
        break;
    case D3DERR_MATRIX_SETDATA_FAILED:
        Return = "D3DERR_MATRIX_SETDATA_FAILED";
        if (AddStr) *AddStr =
            "The matrix data could not be set. This can occur when the matrix w"
            "as not created by the current device.";
        break;
    case D3DERR_NOCURRENTVIEWPORT:
        Return = "D3DERR_NOCURRENTVIEWPORT";
        if (AddStr) *AddStr =

            "The viewport parameters could not be retrieved because none have b"
            "een set.";
        break;
    case D3DERR_NOTINBEGIN:
        Return = "D3DERR_NOTINBEGIN";
        if (AddStr) *AddStr =
            "The requested rendering operation could not be completed because s"
            "cene rendering has not begun. Call IDirect3DDevice7::BeginScene to"
            " begin rendering, and try again.";
        break;
    case D3DERR_NOTINBEGINSTATEBLOCK:
        Return = "D3DERR_NOTINBEGINSTATEBLOCK";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because it is only "
            "valid while recording a state block. Call the IDirect3DDevice7::Be"
            "ginStateBlock method, and try again.";
        break;
    case D3DERR_NOVIEWPORTS:
        Return = "D3DERR_NOVIEWPORTS";
        if (AddStr) *AddStr =
            "The requested operation failed because the device currently has no"
            " viewports associated with it.";
        break;
    case D3DERR_SCENE_BEGIN_FAILED:
        Return = "D3DERR_SCENE_BEGIN_FAILED";
        if (AddStr) *AddStr =
            "Scene rendering could not begin.";
        break;
    case D3DERR_SCENE_END_FAILED:
        Return = "D3DERR_SCENE_END_FAILED";
        if (AddStr) *AddStr =
            "Scene rendering could not be completed.";
        break;
    case D3DERR_SCENE_IN_SCENE:
        Return = "D3DERR_SCENE_IN_SCENE";
        if (AddStr) *AddStr =
            "Scene rendering could not begin because a previous scene was not c"
            "ompleted by a call to the IDirect3DDevice7::EndScene method.";
        break;
    case D3DERR_SCENE_NOT_IN_SCENE:
        Return = "D3DERR_SCENE_NOT_IN_SCENE";
        if (AddStr) *AddStr =
            "Scene rendering could not be completed because a scene was not sta"
            "rted by a previous call to the IDirect3DDevice7::BeginScene method"
            ".";
        break;
    case D3DERR_SETVIEWPORTDATA_FAILED:
        Return = "D3DERR_SETVIEWPORTDATA_FAILED";
        if (AddStr) *AddStr =
            "The viewport parameters could not be set.";
        break;
    case D3DERR_STENCILBUFFER_NOTPRESENT:
        Return = "D3DERR_STENCILBUFFER_NOTPRESENT";
        if (AddStr) *AddStr =
            "The requested stencil buffer operation could not be completed beca"
            "use there is no stencil buffer attached to the render target surfa"
            "ce.";
        break;
    case D3DERR_SURFACENOTINVIDMEM:
        Return = "D3DERR_SURFACENOTINVIDMEM";
        if (AddStr) *AddStr =
            "The device could not be created because the render target surface "
            "is not located in video memory. (Hardware-accelerated devices requ"
            "ire video-memory render target surfaces.)";
        break;
    case D3DERR_TEXTURE_BADSIZE:
        Return = "D3DERR_TEXTURE_BADSIZE";
        if (AddStr) *AddStr =
            "The dimensions of a current texture are invalid. This can occur wh"
            "en an application attempts to use a texture that has dimensions th"
            "at are not a power of 2 with a device that requires them.";
        break;
    case D3DERR_TEXTURE_CREATE_FAILED:
        Return = "D3DERR_TEXTURE_CREATE_FAILED";
        if (AddStr) *AddStr =
            "The texture handle for the texture could not be retrieved from the"
            " driver.";
        break;
    case D3DERR_TEXTURE_DESTROY_FAILED:
        Return = "D3DERR_TEXTURE_DESTROY_FAILED";
        if (AddStr) *AddStr =
            "The device was unable to deallocate the texture memory.";
        break;
    case D3DERR_TEXTURE_GETSURF_FAILED:
        Return = "D3DERR_TEXTURE_GETSURF_FAILED";
        if (AddStr) *AddStr =
            "The DirectDraw surface used to create the texture could not be ret"
            "rieved.";
        break;
    case D3DERR_TEXTURE_LOAD_FAILED:
        Return = "D3DERR_TEXTURE_LOAD_FAILED";
        if (AddStr) *AddStr =
            "The texture could not be loaded.";
        break;
    case D3DERR_TEXTURE_LOCK_FAILED:
        Return = "D3DERR_TEXTURE_LOCK_FAILED";
        if (AddStr) *AddStr =
            "The texture could not be locked.";
        break;
    case D3DERR_TEXTURE_LOCKED:
        Return = "D3DERR_TEXTURE_LOCKED";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the texture"
            " surface is currently locked.";
        break;
    case D3DERR_TEXTURE_NO_SUPPORT:
        Return = "D3DERR_TEXTURE_NO_SUPPORT";
        if (AddStr) *AddStr =
            "The device does not support texture mapping.";
        break;
    case D3DERR_TEXTURE_NOT_LOCKED:
        Return = "D3DERR_TEXTURE_NOT_LOCKED";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the texture"
            " surface is not locked.";
        break;
    case D3DERR_TEXTURE_SWAP_FAILED:
        Return = "D3DERR_TEXTURE_SWAP_FAILED";
        if (AddStr) *AddStr =
            "The texture handles could not be swapped.";
        break;
    case D3DERR_TEXTURE_UNLOCK_FAILED:
        Return = "D3DERR_TEXTURE_UNLOCK_FAILED";
        if (AddStr) *AddStr =
            "The texture surface could not be unlocked.";
        break;
    case D3DERR_TOOMANYOPERATIONS:
        Return = "D3DERR_TOOMANYOPERATIONS";
        if (AddStr) *AddStr =
            "The application is requesting more texture-filtering operations th"
            "an the device supports.";
        break;
    case D3DERR_TOOMANYPRIMITIVES:
        Return = "D3DERR_TOOMANYPRIMITIVES";
        if (AddStr) *AddStr =
            "The device is unable to render the provided number of primitives i"
            "n a single pass.";
        break;
    case D3DERR_UNSUPPORTEDALPHAARG:
        Return = "D3DERR_UNSUPPORTEDALPHAARG";
        if (AddStr) *AddStr =
            "The device does not support one of the specified texture-blending "
            "arguments for the alpha channel.";
        break;
    case D3DERR_UNSUPPORTEDALPHAOPERATION:
        Return = "D3DERR_UNSUPPORTEDALPHAOPERATION";
        if (AddStr) *AddStr =
            "The device does not support one of the specified texture-blending "
            "operations for the alpha channel.";
        break;
    case D3DERR_UNSUPPORTEDCOLORARG:
        Return = "D3DERR_UNSUPPORTEDCOLORARG";
        if (AddStr) *AddStr =
            "The device does not support one of the specified texture-blending "
            "arguments for color values.";
        break;
    case D3DERR_UNSUPPORTEDCOLOROPERATION:
        Return = "D3DERR_UNSUPPORTEDCOLOROPERATION";
        if (AddStr) *AddStr =
            "The device does not support one of the specified texture-blending "
            "operations for color values.";
        break;
    case D3DERR_UNSUPPORTEDFACTORVALUE:
        Return = "D3DERR_UNSUPPORTEDFACTORVALUE";
        if (AddStr) *AddStr =
            "The specified texture factor value is not supported by the device.";
        break;
    case D3DERR_UNSUPPORTEDTEXTUREFILTER:
        Return = "D3DERR_UNSUPPORTEDTEXTUREFILTER";
        if (AddStr) *AddStr =
            "The specified texture filter is not supported by the device.";
        break;
    case D3DERR_VBUF_CREATE_FAILED:
        Return = "D3DERR_VBUF_CREATE_FAILED";
        if (AddStr) *AddStr =
            "The vertex buffer could not be created. This can happen when there"
            " is insufficient memory to allocate a vertex buffer.";
        break;
    case D3DERR_VERTEXBUFFERLOCKED:
        Return = "D3DERR_VERTEXBUFFERLOCKED";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the vertex "
            "buffer is locked.";
        break;
    case D3DERR_VERTEXBUFFEROPTIMIZED:
        Return = "D3DERR_VERTEXBUFFEROPTIMIZED";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the vertex "
            "buffer is optimized. (The contents of optimized vertex buffers are"
            " driver-specific and considered private.)";
        break;
    case D3DERR_VERTEXBUFFERUNLOCKFAILED:
        Return = "D3DERR_VERTEXBUFFERUNLOCKFAILED";
        if (AddStr) *AddStr =
            "The vertex buffer could not be unlocked because the vertex buffer "
            "memory was overrun. Be sure that your application does not write b"
            "eyond the size of the vertex buffer.";
        break;
    case D3DERR_VIEWPORTDATANOTSET:
        Return = "D3DERR_VIEWPORTDATANOTSET";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because viewport pa"
            "rameters have not yet been set. Set the viewport parameters by cal"
            "ling the IDirect3DDevice7::SetViewport method, and try again.";
        break;
    case D3DERR_VIEWPORTHASNODEVICE:
        Return = "D3DERR_VIEWPORTHASNODEVICE";
        if (AddStr) *AddStr =
            "This value is used only by the IDirect3DDevice3 interface and its "
            "predecessors. For the IDirect3DDevice7 interface, this error value"
            " is not used.\nThe requested operation could not be completed beca"
            "use the viewport has not yet been associated with a device. Associ"
            "ate the viewport with a rendering device by calling the IDirect3DD"
            "evice3::AddViewport method, and try again.";
        break;
    case D3DERR_WRONGTEXTUREFORMAT:
        Return = "D3DERR_WRONGTEXTUREFORMAT";
        if (AddStr) *AddStr =
            "The pixel format of the texture surface is not valid.";
        break;
    case D3DERR_ZBUFF_NEEDS_SYSTEMMEMORY:
        Return = "D3DERR_ZBUFF_NEEDS_SYSTEMMEMORY";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the specifi"
            "ed device requires system-memory depth-buffer surfaces. (Software "
            "rendering devices require system-memory depth buffers.)";
        break;
    case D3DERR_ZBUFF_NEEDS_VIDEOMEMORY:
        Return = "D3DERR_ZBUFF_NEEDS_VIDEOMEMORY";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the specifi"
            "ed device requires video-memory depth-buffer surfaces. (Hardware-a"
            "ccelerated devices require video-memory depth buffers.)";
        break;
    case D3DERR_ZBUFFER_NOTPRESENT:
        Return = "D3DERR_ZBUFFER_NOTPRESENT";
        if (AddStr) *AddStr =
            "The requested operation could not be completed because the render "
            "target surface does not have an attached depth buffer.";
        break;

// DirectDraw:

    case DDERR_ALREADYINITIALIZED:
        Return = "DDERR_ALREADYINITIALIZED";
        if (AddStr) *AddStr =
            "The object has already been initialized.";
        break;
    case DDERR_BLTFASTCANTCLIP:
        Return = "DDERR_BLTFASTCANTCLIP";
        if (AddStr) *AddStr =
            "A DirectDrawClipper object is attached to a source surface that ha"
            "s passed into a call to the IDirectDrawSurface7::BltFast method.";
        break;
    case DDERR_CANNOTATTACHSURFACE:
        Return = "DDERR_CANNOTATTACHSURFACE";
        if (AddStr) *AddStr =
            "A surface cannot be attached to another requested surface.";
        break;
    case DDERR_CANNOTDETACHSURFACE:
        Return = "DDERR_CANNOTDETACHSURFACE";
        if (AddStr) *AddStr =
            "A surface cannot be detached from another requested surface.";
        break;
    case DDERR_CANTCREATEDC:
        Return = "DDERR_CANTCREATEDC";
        if (AddStr) *AddStr =
            "Windows cannot create any more device contexts (DCs), or a DC has "
            "requested a palette-indexed surface when the surface had no palett"
            "e and the display mode was not palette-indexed (in this case Direc"
            "tDraw cannot select a proper palette into the DC).";
        break;
    case DDERR_CANTDUPLICATE:
        Return = "DDERR_CANTDUPLICATE";
        if (AddStr) *AddStr =
            "Primary and 3-D surfaces, or surfaces that are implicitly created,"
            " cannot be duplicated.";
        break;
    case DDERR_CANTLOCKSURFACE:
        Return = "DDERR_CANTLOCKSURFACE";
        if (AddStr) *AddStr =
            "Access to this surface is refused because an attempt was made to l"
            "ock the primary surface without DCI support.";
        break;
    case DDERR_CANTPAGELOCK:
        Return = "DDERR_CANTPAGELOCK";
        if (AddStr) *AddStr =
            "An attempt to page-lock a surface failed. Page lock does not work "
            "on a display-memory surface or an emulated primary surface.";
        break;
    case DDERR_CANTPAGEUNLOCK:
        Return = "DDERR_CANTPAGEUNLOCK";
        if (AddStr) *AddStr =
            "An attempt to page-unlock a surface failed. Page unlock does not w"
            "ork on a display-memory surface or an emulated primary surface.";
        break;
    case DDERR_CLIPPERISUSINGHWND:
        Return = "DDERR_CLIPPERISUSINGHWND";
        if (AddStr) *AddStr =
            "An attempt was made to set a clip list for a DirectDrawClipper obj"
            "ect that is already monitoring a window handle.";
        break;
    case DDERR_COLORKEYNOTSET:
        Return = "DDERR_COLORKEYNOTSET";
        if (AddStr) *AddStr =
            "No source color key is specified for this operation.";
        break;
    case DDERR_CURRENTLYNOTAVAIL:
        Return = "DDERR_CURRENTLYNOTAVAIL";
        if (AddStr) *AddStr =
            "No support is currently available.";
        break;
    case DDERR_DDSCAPSCOMPLEXREQUIRED:
        Return = "DDERR_DDSCAPSCOMPLEXREQUIRED";
        if (AddStr) *AddStr =
            "New for DirectX 7.0. The surface requires the DDSCAPS_COMPLEX flag"
            ".";
        break;
    case DDERR_DCALREADYCREATED:
        Return = "DDERR_DCALREADYCREATED";
        if (AddStr) *AddStr =
            "A device context (DC) has already been returned for this surface. "
            "Only one DC can be retrieved for each surface.";
        break;
    case DDERR_DEVICEDOESNTOWNSURFACE:
        Return = "DDERR_DEVICEDOESNTOWNSURFACE";
        if (AddStr) *AddStr =
            "Surfaces created by one DirectDraw device cannot be used directly "
            "by another DirectDraw device.";
        break;
    case DDERR_DIRECTDRAWALREADYCREATED:
        Return = "DDERR_DIRECTDRAWALREADYCREATED";
        if (AddStr) *AddStr =
            "A DirectDraw object representing this driver has already been crea"
            "ted for this process.";
        break;
    case DDERR_EXCEPTION:
        Return = "DDERR_EXCEPTION";
        if (AddStr) *AddStr =
            "An exception was encountered while performing the requested operat"
            "ion.";
        break;
    case DDERR_EXCLUSIVEMODEALREADYSET:
        Return = "DDERR_EXCLUSIVEMODEALREADYSET";
        if (AddStr) *AddStr =
            "An attempt was made to set the cooperative level when it was alrea"
            "dy set to exclusive.";
        break;
    case DDERR_EXPIRED:
        Return = "DDERR_EXPIRED";
        if (AddStr) *AddStr =
            "The data has expired and is therefore no longer valid.";
        break;
    case DDERR_HEIGHTALIGN:
        Return = "DDERR_HEIGHTALIGN";
        if (AddStr) *AddStr =
            "The height of the provided rectangle is not a multiple of the requ"
            "ired alignment.";
        break;
    case DDERR_HWNDALREADYSET:
        Return = "DDERR_HWNDALREADYSET";
        if (AddStr) *AddStr =
            "The DirectDraw cooperative-level window handle has already been se"
            "t. It cannot be reset while the process has surfaces or palettes c"
            "reated.";
        break;
    case DDERR_HWNDSUBCLASSED:
        Return = "DDERR_HWNDSUBCLASSED";
        if (AddStr) *AddStr =
            "DirectDraw is prevented from restoring state because the DirectDra"
            "w cooperative-level window handle has been subclassed.";
        break;
    case DDERR_IMPLICITLYCREATED:
        Return = "DDERR_IMPLICITLYCREATED";
        if (AddStr) *AddStr =
            "The surface cannot be restored because it is an implicitly created"
            " surface.";
        break;
    case DDERR_INCOMPATIBLEPRIMARY:
        Return = "DDERR_INCOMPATIBLEPRIMARY";
        if (AddStr) *AddStr =
            "The primary surface creation request does not match the existing p"
            "rimary surface.";
        break;
    case DDERR_INVALIDCAPS:
        Return = "DDERR_INVALIDCAPS";
        if (AddStr) *AddStr =
            "One or more of the capability bits passed to the callback function"
            " are incorrect.";
        break;
    case DDERR_INVALIDCLIPLIST:
        Return = "DDERR_INVALIDCLIPLIST";
        if (AddStr) *AddStr =
            "DirectDraw does not support the provided clip list.";
        break;
    case DDERR_INVALIDDIRECTDRAWGUID:
        Return = "DDERR_INVALIDDIRECTDRAWGUID";
        if (AddStr) *AddStr =
            "The globally unique identifier (GUID) passed to the DirectDrawCrea"
            "te function is not a valid DirectDraw driver identifier.";
        break;
    case DDERR_INVALIDMODE:
        Return = "DDERR_INVALIDMODE";
        if (AddStr) *AddStr =
            "DirectDraw does not support the requested mode.";
        break;
    case DDERR_INVALIDOBJECT:
        Return = "DDERR_INVALIDOBJECT";
        if (AddStr) *AddStr =
            "DirectDraw received a pointer that was an invalid DirectDraw objec"
            "t.";
        break;
    case DDERR_INVALIDPIXELFORMAT:
        Return = "DDERR_INVALIDPIXELFORMAT";
        if (AddStr) *AddStr =
            "The pixel format was invalid as specified.";
        break;
    case DDERR_INVALIDPOSITION:
        Return = "DDERR_INVALIDPOSITION";
        if (AddStr) *AddStr =
            "The position of the overlay on the destination is no longer legal.";
        break;
    case DDERR_INVALIDRECT:
        Return = "DDERR_INVALIDRECT";
        if (AddStr) *AddStr =
            "The provided rectangle was invalid.";
        break;
    case DDERR_INVALIDSTREAM:
        Return = "DDERR_INVALIDSTREAM";
        if (AddStr) *AddStr =
            "The specified stream contains invalid data.";
        break;
    case DDERR_INVALIDSURFACETYPE:
        Return = "DDERR_INVALIDSURFACETYPE";
        if (AddStr) *AddStr =
            "The surface was of the wrong type.";
        break;
    case DDERR_LOCKEDSURFACES:
        Return = "DDERR_LOCKEDSURFACES";
        if (AddStr) *AddStr =
            "One or more surfaces are locked, causing the failure of the reques"
            "ted operation.";
        break;
    case DDERR_MOREDATA:
        Return = "DDERR_MOREDATA";
        if (AddStr) *AddStr =
            "There is more data available than the specified buffer size can ho"
            "ld.";
        break;
    case DDERR_NEWMODE:
        Return = "DDERR_NEWMODE";
        if (AddStr) *AddStr =
            "New for DirectX 7.0. When IDirectDraw7::StartModeTest is called wi"
            "th the DDSMT_ISTESTREQUIRED flag, it may return this value to deno"
            "te that some or all of the resolutions can and should be tested. I"
            "DirectDraw7::EvaluateMode returns this value to indicate that the "
            "test has switched to a new display mode.";
        break;
    case DDERR_NO3D:
        Return = "DDERR_NO3D";
        if (AddStr) *AddStr =
            "No 3-D hardware or emulation is present.";
        break;
    case DDERR_NOALPHAHW:
        Return = "DDERR_NOALPHAHW";
        if (AddStr) *AddStr =
            "No alpha-acceleration hardware is present or available, causing th"
            "e failure of the requested operation.";
        break;
    case DDERR_NOBLTHW:
        Return = "DDERR_NOBLTHW";
        if (AddStr) *AddStr =
            "No blitter hardware is present.";
        break;
    case DDERR_NOCLIPLIST:
        Return = "DDERR_NOCLIPLIST";
        if (AddStr) *AddStr =
            "No clip list is available.";
        break;
    case DDERR_NOCLIPPERATTACHED:
        Return = "DDERR_NOCLIPPERATTACHED";
        if (AddStr) *AddStr =
            "No DirectDrawClipper object is attached to the surface object.";
        break;
    case DDERR_NOCOLORCONVHW:
        Return = "DDERR_NOCOLORCONVHW";
        if (AddStr) *AddStr =
            "No color-conversion hardware is present or available.";
        break;
    case DDERR_NOCOLORKEY:
        Return = "DDERR_NOCOLORKEY";
        if (AddStr) *AddStr =
            "The surface does not currently have a color key.";
        break;
    case DDERR_NOCOLORKEYHW:
        Return = "DDERR_NOCOLORKEYHW";
        if (AddStr) *AddStr =
            "There is no hardware support for the destination color key.";
        break;
    case DDERR_NOCOOPERATIVELEVELSET:
        Return = "DDERR_NOCOOPERATIVELEVELSET";
        if (AddStr) *AddStr =
            "A create function was called without the IDirectDraw7::SetCooperat"
            "iveLevel method.";
        break;
    case DDERR_NODC:
        Return = "DDERR_NODC";
        if (AddStr) *AddStr =
            "No device context (DC) has ever been created for this surface.";
        break;
    case DDERR_NODDROPSHW:
        Return = "DDERR_NODDROPSHW";
        if (AddStr) *AddStr =
            "No DirectDraw raster-operation (ROP) hardware is available.";
        break;
    case DDERR_NODIRECTDRAWHW:
        Return = "DDERR_NODIRECTDRAWHW";
        if (AddStr) *AddStr =
            "Hardware-only DirectDraw object creation is not possible; the driv"
            "er does not support any hardware.";
        break;
    case DDERR_NODIRECTDRAWSUPPORT:
        Return = "DDERR_NODIRECTDRAWSUPPORT";
        if (AddStr) *AddStr =
            "DirectDraw support is not possible with the current display driver"
            ".";
        break;
    case DDERR_NODRIVERSUPPORT:
        Return = "DDERR_NODRIVERSUPPORT";
        if (AddStr) *AddStr =
            "New for DirectX 7.0. Testing cannot proceed because the display ad"
            "apter driver does not enumerate refresh rates.";
        break;
    case DDERR_NOEMULATION:
        Return = "DDERR_NOEMULATION";
        if (AddStr) *AddStr =
            "Software emulation is not available.";
        break;
    case DDERR_NOEXCLUSIVEMODE:
        Return = "DDERR_NOEXCLUSIVEMODE";
        if (AddStr) *AddStr =
            "The operation requires the application to have exclusive mode, but"
            " the application does not have exclusive mode.";
        break;
    case DDERR_NOFLIPHW:
        Return = "DDERR_NOFLIPHW";
        if (AddStr) *AddStr =
            "Flipping visible surfaces is not supported.";
        break;
    case DDERR_NOFOCUSWINDOW:
        Return = "DDERR_NOFOCUSWINDOW";
        if (AddStr) *AddStr =
            "An attempt was made to create or set a device window without first"
            " setting the focus window.";
        break;
    case DDERR_NOGDI:
        Return = "DDERR_NOGDI";
        if (AddStr) *AddStr =
            "No GDI is present.";
        break;
    case DDERR_NOHWND:
        Return = "DDERR_NOHWND";
        if (AddStr) *AddStr =
            "Clipper notification requires a window handle, or no window handle"
            " has been previously set as the cooperative level window handle.";
        break;
    case DDERR_NOMIPMAPHW:
        Return = "DDERR_NOMIPMAPHW";
        if (AddStr) *AddStr =
            "No mipmap-capable texture mapping hardware is present or available"
            ".";
        break;
    case DDERR_NOMIRRORHW:
        Return = "DDERR_NOMIRRORHW";
        if (AddStr) *AddStr =
            "No mirroring hardware is present or available.";
        break;
    case DDERR_NOMONITORINFORMATION:
        Return = "DDERR_NOMONITORINFORMATION";
        if (AddStr) *AddStr =
            "New for DirectX 7.0. Testing cannot proceed because the monitor ha"
            "s no associated EDID data.";
        break;
    case DDERR_NONONLOCALVIDMEM:
        Return = "DDERR_NONONLOCALVIDMEM";
        if (AddStr) *AddStr =
            "An attempt was made to allocate nonlocal video memory from a devic"
            "e that does not support nonlocal video memory.";
        break;
    case DDERR_NOOPTIMIZEHW:
        Return = "DDERR_NOOPTIMIZEHW";
        if (AddStr) *AddStr =
            "The device does not support optimized surfaces.";
        break;
    case DDERR_NOOVERLAYDEST:
        Return = "DDERR_NOOVERLAYDEST";
        if (AddStr) *AddStr =
            "The IDirectDrawSurface7::GetOverlayPosition method is called on an"
            " overlay that the IDirectDrawSurface7::UpdateOverlay method has no"
            "t been called on to establish as a destination.";
        break;
    case DDERR_NOOVERLAYHW:
        Return = "DDERR_NOOVERLAYHW";
        if (AddStr) *AddStr =
            "No overlay hardware is present or available.";
        break;
    case DDERR_NOPALETTEATTACHED:
        Return = "DDERR_NOPALETTEATTACHED";
        if (AddStr) *AddStr =
            "No palette object is attached to this surface.";
        break;
    case DDERR_NOPALETTEHW:
        Return = "DDERR_NOPALETTEHW";
        if (AddStr) *AddStr =
            "There is no hardware support for 16- or 256-color palettes.";
        break;
    case DDERR_NORASTEROPHW:
        Return = "DDERR_NORASTEROPHW";
        if (AddStr) *AddStr =
            "No appropriate raster-operation hardware is present or available.";
        break;
    case DDERR_NOROTATIONHW:
        Return = "DDERR_NOROTATIONHW";
        if (AddStr) *AddStr =
            "No rotation hardware is present or available.";
        break;
    case DDERR_NOSTEREOHARDWARE:
        Return = "DDERR_NOSTEREOHARDWARE";
        if (AddStr) *AddStr =
            "There is no stereo hardware present or available.";
        break;
    case DDERR_NOSTRETCHHW:
        Return = "DDERR_NOSTRETCHHW";
        if (AddStr) *AddStr =
            "There is no hardware support for stretching.";
        break;
    case DDERR_NOSURFACELEFT:
        Return = "DDERR_NOSURFACELEFT";
        if (AddStr) *AddStr =
            "There is no hardware present that supports stereo surfaces.";
        break;
    case DDERR_NOT4BITCOLOR:
        Return = "DDERR_NOT4BITCOLOR";
        if (AddStr) *AddStr =
            "The DirectDrawSurface object is not using a 4-bit color palette, a"
            "nd the requested operation requires a 4-bit color palette.";
        break;
    case DDERR_NOT4BITCOLORINDEX:
        Return = "DDERR_NOT4BITCOLORINDEX";
        if (AddStr) *AddStr =
            "The DirectDrawSurface object is not using a 4-bit color index pale"
            "tte, and the requested operation requires a 4-bit color index pale"
            "tte.";
        break;
    case DDERR_NOT8BITCOLOR:
        Return = "DDERR_NOT8BITCOLOR";
        if (AddStr) *AddStr =
            "The DirectDrawSurface object is not using an 8-bit color palette, "
            "and the requested operation requires an 8-bit color palette.";
        break;
    case DDERR_NOTAOVERLAYSURFACE:
        Return = "DDERR_NOTAOVERLAYSURFACE";
        if (AddStr) *AddStr =
            "An overlay component is called for a nonoverlay surface.";
        break;
    case DDERR_NOTEXTUREHW:
        Return = "DDERR_NOTEXTUREHW";
        if (AddStr) *AddStr =
            "The operation cannot be carried out because no texture-mapping har"
            "dware is present or available.";
        break;
    case DDERR_NOTFLIPPABLE:
        Return = "DDERR_NOTFLIPPABLE";
        if (AddStr) *AddStr =
            "An attempt was made to flip a surface that cannot be flipped.";
        break;
    case DDERR_NOTFOUND:
        Return = "DDERR_NOTFOUND";
        if (AddStr) *AddStr =
            "The requested item was not found.";
        break;
    case DDERR_NOTINITIALIZED:
        Return = "DDERR_NOTINITIALIZED";
        if (AddStr) *AddStr =
            "An attempt was made to call an interface method of a DirectDraw ob"
            "ject created by CoCreateInstance before the object was initialized"
            ".";
        break;
    case DDERR_NOTLOADED:
        Return = "DDERR_NOTLOADED";
        if (AddStr) *AddStr =
            "The surface is an optimized surface, but it has not yet been alloc"
            "ated any memory.";
        break;
    case DDERR_NOTLOCKED:
        Return = "DDERR_NOTLOCKED";
        if (AddStr) *AddStr =
            "An attempt was made to unlock a surface that was not locked.";
        break;
    case DDERR_NOTPAGELOCKED:
        Return = "DDERR_NOTPAGELOCKED";
        if (AddStr) *AddStr =
            "An attempt was made to page-unlock a surface with no outstanding p"
            "age locks.";
        break;
    case DDERR_NOTPALETTIZED:
        Return = "DDERR_NOTPALETTIZED";
        if (AddStr) *AddStr =
            "The surface being used is not a palette-based surface.";
        break;
    case DDERR_NOVSYNCHW:
        Return = "DDERR_NOVSYNCHW";
        if (AddStr) *AddStr =
            "There is no hardware support for vertical blank�synchronized opera"
            "tions.";
        break;
    case DDERR_NOZBUFFERHW:
        Return = "DDERR_NOZBUFFERHW";
        if (AddStr) *AddStr =
            "The operation to create a z-buffer in display memory or to perform"
            " a blit, using a z-buffer cannot be carried out because there is n"
            "o hardware support for z-buffers.";
        break;
    case DDERR_NOZOVERLAYHW:
        Return = "DDERR_NOZOVERLAYHW";
        if (AddStr) *AddStr =
            "The overlay surfaces cannot be z-layered, based on the z-order bec"
            "ause the hardware does not support z-ordering of overlays.";
        break;
    case DDERR_OUTOFCAPS:
        Return = "DDERR_OUTOFCAPS";
        if (AddStr) *AddStr =
            "The hardware needed for the requested operation has already been a"
            "llocated.";
        break;
    case DDERR_OUTOFVIDEOMEMORY:
        Return = "DDERR_OUTOFVIDEOMEMORY";
        if (AddStr) *AddStr =
            "DirectDraw does not have enough display memory to perform the oper"
            "ation.";
        break;
    case DDERR_OVERLAPPINGRECTS:
        Return = "DDERR_OVERLAPPINGRECTS";
        if (AddStr) *AddStr =
            "The source and destination rectangles are on the same surface and "
            "overlap each other.";
        break;
    case DDERR_OVERLAYCANTCLIP:
        Return = "DDERR_OVERLAYCANTCLIP";
        if (AddStr) *AddStr =
            "The hardware does not support clipped overlays.";
        break;
    case DDERR_OVERLAYCOLORKEYONLYONEACTIVE:
        Return = "DDERR_OVERLAYCOLORKEYONLYONEACTIVE";
        if (AddStr) *AddStr =
            "An attempt was made to have more than one color key active on an o"
            "verlay.";
        break;
    case DDERR_OVERLAYNOTVISIBLE:
        Return = "DDERR_OVERLAYNOTVISIBLE";
        if (AddStr) *AddStr =
            "The IDirectDrawSurface7::GetOverlayPosition method was called on a"
            " hidden overlay.";
        break;
    case DDERR_PALETTEBUSY:
        Return = "DDERR_PALETTEBUSY";
        if (AddStr) *AddStr =
            "Access to this palette is refused because the palette is locked by"
            " another thread.";
        break;
    case DDERR_PRIMARYSURFACEALREADYEXISTS:
        Return = "DDERR_PRIMARYSURFACEALREADYEXISTS";
        if (AddStr) *AddStr =
            "This process has already created a primary surface.";
        break;
    case DDERR_REGIONTOOSMALL:
        Return = "DDERR_REGIONTOOSMALL";
        if (AddStr) *AddStr =
            "The region passed to the IDirectDrawClipper::GetClipList method is"
            " too small.";
        break;
    case DDERR_SURFACEALREADYATTACHED:
        Return = "DDERR_SURFACEALREADYATTACHED";
        if (AddStr) *AddStr =
            "An attempt was made to attach a surface to another surface to whic"
            "h it is already attached.";
        break;
    case DDERR_SURFACEALREADYDEPENDENT:
        Return = "DDERR_SURFACEALREADYDEPENDENT";
        if (AddStr) *AddStr =
            "An attempt was made to make a surface a dependency of another surf"
            "ace on which it is already dependent.";
        break;
    case DDERR_SURFACEBUSY:
        Return = "DDERR_SURFACEBUSY";
        if (AddStr) *AddStr =
            "Access to the surface is refused because the surface is locked by "
            "another thread.";
        break;
    case DDERR_SURFACEISOBSCURED:
        Return = "DDERR_SURFACEISOBSCURED";
        if (AddStr) *AddStr =
            "Access to the surface is refused because the surface is obscured.";
        break;
    case DDERR_SURFACELOST:
        Return = "DDERR_SURFACELOST";
        if (AddStr) *AddStr =
            "Access to the surface is refused because the surface memory is gon"
            "e. Call the IDirectDrawSurface7::Restore method on this surface to"
            " restore the memory associated with it.";
        break;
    case DDERR_SURFACENOTATTACHED:
        Return = "DDERR_SURFACENOTATTACHED";
        if (AddStr) *AddStr =
            "The requested surface is not attached.";
        break;
    case DDERR_TESTFINISHED:
        Return = "DDERR_TESTFINISHED";
        if (AddStr) *AddStr =
            "New for DirectX 7.0. When returned by the IDirectDraw7::StartModeT"
            "est method, this value means that no test could be initiated becau"
            "se all the resolutions chosen for testing already have refresh rat"
            "e information in the registry. When returned by IDirectDraw7::Eval"
            "uateMode, the value means that DirectDraw has completed a refresh "
            "rate test.";
        break;
    case DDERR_TOOBIGHEIGHT:
        Return = "DDERR_TOOBIGHEIGHT";
        if (AddStr) *AddStr =
            "The height requested by DirectDraw is too large.";
        break;
    case DDERR_TOOBIGSIZE:
        Return = "DDERR_TOOBIGSIZE";
        if (AddStr) *AddStr =
            "The size requested by DirectDraw is too large. However, the indivi"
            "dual height and width are valid sizes.";
        break;
    case DDERR_TOOBIGWIDTH:
        Return = "DDERR_TOOBIGWIDTH";
        if (AddStr) *AddStr =
            "The width requested by DirectDraw is too large.";
        break;
    case DDERR_UNSUPPORTEDFORMAT:
        Return = "DDERR_UNSUPPORTEDFORMAT";
        if (AddStr) *AddStr =
            "The pixel format requested is not supported by DirectDraw.";
        break;
    case DDERR_UNSUPPORTEDMASK:
        Return = "DDERR_UNSUPPORTEDMASK";
        if (AddStr) *AddStr =
            "The bitmask in the pixel format requested is not supported by Dire"
            "ctDraw.";
        break;
    case DDERR_UNSUPPORTEDMODE:
        Return = "DDERR_UNSUPPORTEDMODE";
        if (AddStr) *AddStr =
            "The display is currently in an unsupported mode.";
        break;
    case DDERR_VERTICALBLANKINPROGRESS:
        Return = "DDERR_VERTICALBLANKINPROGRESS";
        if (AddStr) *AddStr =
            "A vertical blank is in progress.";
        break;
    case DDERR_VIDEONOTACTIVE:
        Return = "DDERR_VIDEONOTACTIVE";
        if (AddStr) *AddStr =
            "The video port is not active.";
        break;
    case DDERR_WASSTILLDRAWING:
        Return = "DDERR_WASSTILLDRAWING";
        if (AddStr) *AddStr =
            "The previous blit operation that is transferring information to or"
            " from this surface is incomplete.";
        break;
    case DDERR_WRONGMODE:
        Return = "DDERR_WRONGMODE";
        if (AddStr) *AddStr =
            "This surface cannot be restored because it was created in a differ"
            "ent mode.";
        break;
    case DDERR_XALIGN:
        Return = "DDERR_XALIGN";
        if (AddStr) *AddStr =
            "The provided rectangle was not horizontally aligned on a required "
            "boundary.";
        break;

// DirectInput:

    case DI_DOWNLOADSKIPPED:
        Return = "DI_DOWNLOADSKIPPED";
        if (AddStr) *AddStr =
            "The parameters of the effect were successfully updated, but the ef"
            "fect could not be downloaded because the associated device was not"
            " acquired in exclusive mode.";
        break;
    case DI_EFFECTRESTARTED:
        Return = "DI_EFFECTRESTARTED";
        if (AddStr) *AddStr =
            "The effect was stopped, the parameters were updated, and the effec"
            "t was restarted.";
        break;
    case DI_POLLEDDEVICE:
        Return = "DI_POLLEDDEVICE";
        if (AddStr) *AddStr =
            "The device is a polled device. As a result, device buffering does "
            "not collect any data and event notifications is not signaled until"
            " the IDirectInputDevice7::Poll method is called.";
        break;
    case DI_TRUNCATED:
        Return = "DI_TRUNCATED";
        if (AddStr) *AddStr =
            "The parameters of the effect were successfully updated, but some o"
            "f them were beyond the capabilities of the device and were truncat"
            "ed to the nearest supported value.";
        break;
    case DI_TRUNCATEDANDRESTARTED:
        Return = "DI_TRUNCATEDANDRESTARTED";
        if (AddStr) *AddStr =
            "Equal to DI_EFFECTRESTARTED | DI_TRUNCATED.";
        break;
    case DIERR_ACQUIRED:
        Return = "DIERR_ACQUIRED";
        if (AddStr) *AddStr =
            "The operation cannot be performed while the device is acquired.";
        break;
    case DIERR_ALREADYINITIALIZED:
        Return = "DIERR_ALREADYINITIALIZED";
        if (AddStr) *AddStr =
            "This object is already initialized";
        break;
    case DIERR_BADDRIVERVER:
        Return = "DIERR_BADDRIVERVER";
        if (AddStr) *AddStr =
            "The object could not be created due to an incompatible driver vers"
            "ion or mismatched or incomplete driver components.";
        break;
    case DIERR_BETADIRECTINPUTVERSION:
        Return = "DIERR_BETADIRECTINPUTVERSION";
        if (AddStr) *AddStr =
            "The application was written for an unsupported prerelease version "
            "of DirectInput.";
        break;
    case DIERR_DEVICEFULL:
        Return = "DIERR_DEVICEFULL";
        if (AddStr) *AddStr =
            "The device is full.";
        break;
    case DIERR_EFFECTPLAYING:
        Return = "DIERR_EFFECTPLAYING";
        if (AddStr) *AddStr =
            "The parameters were updated in memory but were not downloaded to t"
            "he device because the device does not support updating an effect w"
            "hile it is still playing.";
        break;
    case DIERR_HASEFFECTS:
        Return = "DIERR_HASEFFECTS";
        if (AddStr) *AddStr =
            "The device cannot be reinitialized because there are still effects"
            " attached to it.";
        break;
    case DIERR_INCOMPLETEEFFECT:
        Return = "DIERR_INCOMPLETEEFFECT";
        if (AddStr) *AddStr =
            "The effect could not be downloaded because essential information i"
            "s missing. For example, no axes have been associated with the effe"
            "ct, or no type-specific information has been supplied.";
        break;
    case DIERR_INPUTLOST:
        Return = "DIERR_INPUTLOST";
        if (AddStr) *AddStr =
            "Access to the input device has been lost. It must be reacquired.";
        break;
    case DIERR_MOREDATA:
        Return = "DIERR_MOREDATA";
        if (AddStr) *AddStr =
            "Not all the requested information fit into the buffer.";
        break;
    case DIERR_NOTACQUIRED:
        Return = "DIERR_NOTACQUIRED";
        if (AddStr) *AddStr =
            "The operation cannot be performed unless the device is acquired.";
        break;
    case DIERR_NOTBUFFERED:
        Return = "DIERR_NOTBUFFERED";
        if (AddStr) *AddStr =
            "The device is not buffered. Set the DIPROP_BUFFERSIZE property to "
            "enable buffering.";
        break;
    case DIERR_NOTDOWNLOADED:
        Return = "DIERR_NOTDOWNLOADED";
        if (AddStr) *AddStr =
            "The effect is not downloaded.";
        break;
    case DIERR_NOTEXCLUSIVEACQUIRED:
        Return = "DIERR_NOTEXCLUSIVEACQUIRED";
        if (AddStr) *AddStr =
            "The operation cannot be performed unless the device is acquired in"
            " DISCL_EXCLUSIVE mode.";
        break;
    case DIERR_NOTFOUND:
        Return = "DIERR_NOTFOUND/DIERR_OBJECTNOTFOUND";
        if (AddStr) *AddStr =
            "The requested object does not exist.";
        break;
    case DIERR_NOTINITIALIZED:
        Return = "DIERR_NOTINITIALIZED";
        if (AddStr) *AddStr =
            "This object has not been initialized.";
        break;
    case DIERR_OLDDIRECTINPUTVERSION:
        Return = "DIERR_OLDDIRECTINPUTVERSION";
        if (AddStr) *AddStr =
            "The application requires a newer version of DirectInput.";
        break;
    case DIERR_REPORTFULL:
        Return = "DIERR_REPORTFULL";
        if (AddStr) *AddStr =
            "More information was requested to be sent than can be sent to the "
            "device.";
        break;
    case DIERR_UNPLUGGED:
        Return = "DIERR_UNPLUGGED";
        if (AddStr) *AddStr =
            "The operation could not be completed because the device is not plu"
            "gged in.";
        break;

// DirectMusic:

    case DMUS_E_ALL_TOOLS_FAILED:
        Return = "DMUS_E_ALL_TOOLS_FAILED";
        if (AddStr) *AddStr =
            "The graph object was unable to load all tools from the IStream obj"
            "ect data, perhaps because of errors in the stream or because the t"
            "ools are incorrectly registered on the client.";
        break;
    case DMUS_E_ALL_TRACKS_FAILED:
        Return = "DMUS_E_ALL_TRACKS_FAILED";
        if (AddStr) *AddStr =
            "The segment object was unable to load all tracks from the IStream "
            "object data, perhaps because of errors in the stream or because th"
            "e tracks are incorrectly registered on the client.";
        break;
    case DMUS_E_ALREADY_ACTIVATED:
        Return = "DMUS_E_ALREADY_ACTIVATED";
        if (AddStr) *AddStr =
            "The port has been activated, and the parameter cannot be changed.";
        break;
    case DMUS_E_ALREADY_DOWNLOADED:
        Return = "DMUS_E_ALREADY_DOWNLOADED";
        if (AddStr) *AddStr =
            "The buffer has already been downloaded.";
        break;
    case DMUS_E_ALREADY_EXISTS:
        Return = "DMUS_E_ALREADY_EXISTS";
        if (AddStr) *AddStr =
            "The tool is already contained in the graph. You must create a new "
            "instance.";
        break;
    case DMUS_E_ALREADY_INITED:
        Return = "DMUS_E_ALREADY_INITED";
        if (AddStr) *AddStr =
            "The object has already been initialized.";
        break;
    case DMUS_E_ALREADY_LOADED:
        Return = "DMUS_E_ALREADY_LOADED";
        if (AddStr) *AddStr =
            "A DLS collection is already open.";
        break;
    case DMUS_E_ALREADY_SENT:
        Return = "DMUS_E_ALREADY_SENT";
        if (AddStr) *AddStr =
            "The message has already been sent.";
        break;
    case DMUS_E_ALREADYCLOSED:
        Return = "DMUS_E_ALREADYCLOSED";
        if (AddStr) *AddStr =
            "The port is not open.";
        break;
    case DMUS_E_ALREADYOPEN:
        Return = "DMUS_E_ALREADYOPEN";
        if (AddStr) *AddStr =
            "The port was already opened.";
        break;
    case DMUS_E_BADARTICULATION:
        Return = "DMUS_E_BADARTICULATION";
        if (AddStr) *AddStr =
            "Invalid articulation chunk in DLS collection.";
        break;
    case DMUS_E_BADINSTRUMENT:
        Return = "DMUS_E_BADINSTRUMENT";
        if (AddStr) *AddStr =
            "Invalid instrument chunk in DLS collection.";
        break;
    case DMUS_E_BADOFFSETTABLE:
        Return = "DMUS_E_BADOFFSETTABLE";
        if (AddStr) *AddStr =
            "The offset table has errors.";
        break;
    case DMUS_E_BADWAVE:
        Return = "DMUS_E_BADWAVE";
        if (AddStr) *AddStr =
            "Corrupt wave header.";
        break;
    case DMUS_E_BADWAVELINK:
        Return = "DMUS_E_BADWAVELINK";
        if (AddStr) *AddStr =
            "The wave-link chunk in DLS collection points to invalid wave.";
        break;
    case DMUS_E_BUFFER_EMPTY:
        Return = "DMUS_E_BUFFER_EMPTY";
        if (AddStr) *AddStr =
            "There is no data in the buffer.";
        break;
    case DMUS_E_BUFFER_FULL:
        Return = "DMUS_E_BUFFER_FULL";
        if (AddStr) *AddStr =
            "The specified number of bytes exceeds the maximum buffer size.";
        break;
    case DMUS_E_BUFFERNOTAVAILABLE:
        Return = "DMUS_E_BUFFERNOTAVAILABLE";
        if (AddStr) *AddStr =
            "The buffer is not available for download.";
        break;
    case DMUS_E_BUFFERNOTSET:
        Return = "DMUS_E_BUFFERNOTSET";
        if (AddStr) *AddStr =
            "No buffer was prepared for the data.";
        break;
/*
Where is it defined???
    case DMUS_E_CANNOT_CONVERT:
        Return = "DMUS_E_CANNOT_CONVERT";
        if (AddStr) *AddStr =
            "The requested conversion between music and MIDI values could not b"
            "e made. This usually occurs when the provided DMUS_CHORD_KEY struc"
            "ture has an invalid chord or scale pattern.";
        break;
*/
    case DMUS_E_CANNOT_FREE:
        Return = "DMUS_E_CANNOT_FREE";
        if (AddStr) *AddStr =
            "The message could not be freed, either because it was not allocate"
            "d or because it has already been freed.";
        break;
    case DMUS_E_CANNOT_OPEN_PORT:
        Return = "DMUS_E_CANNOT_OPEN_PORT";
        if (AddStr) *AddStr =
            "The default system port could not be opened.";
        break;
    case DMUS_E_CANNOTREAD:
        Return = "DMUS_E_CANNOTREAD";
        if (AddStr) *AddStr =
            "An error occurred when trying to read from the IStream object.";
        break;
    case DMUS_E_CANNOTSEEK:
        Return = "DMUS_E_CANNOTSEEK";
        if (AddStr) *AddStr =
            "The IStream object does not support Seek.";
        break;
    case DMUS_E_CANNOTWRITE:
        Return = "DMUS_E_CANNOTWRITE";
        if (AddStr) *AddStr =
            "The IStream object does not support Write.";
        break;
    case DMUS_E_CHUNKNOTFOUND:
        Return = "DMUS_E_CHUNKNOTFOUND";
        if (AddStr) *AddStr =
            "A chunk with the specified header could not be found.";
        break;
    case DMUS_E_DESCEND_CHUNK_FAIL:
        Return = "DMUS_E_DESCEND_CHUNK_FAIL";
        if (AddStr) *AddStr =
            "An attempt to descend into a chunk failed.";
        break;
    case DMUS_E_DEVICE_IN_USE:
        Return = "DMUS_E_DEVICE_IN_USE";
        if (AddStr) *AddStr =
            "The device is already in use (possibly by a non-DirectMusic client"
            ") and cannot be opened again.";
        break;
    case DMUS_E_DMUSIC_RELEASED:
        Return = "DMUS_E_DMUSIC_RELEASED";
        if (AddStr) *AddStr =
            "The operation cannot be performed because the final instance of th"
            "e DirectMusic object was released. Ports cannot be used after fina"
            "l release of the DirectMusic object.";
        break;
    case DMUS_E_DRIVER_FAILED:
        Return = "DMUS_E_DRIVER_FAILED";
        if (AddStr) *AddStr =
            "An unexpected error was returned from a device driver, indicating "
            "possible failure of the driver or hardware.";
        break;
    case DMUS_E_DSOUND_ALREADY_SET:
        Return = "DMUS_E_DSOUND_ALREADY_SET";
        if (AddStr) *AddStr =
            "A DirectSound object has already been set.";
        break;
    case DMUS_E_DSOUND_NOT_SET:
        Return = "DMUS_E_DSOUND_NOT_SET";
        if (AddStr) *AddStr =
            "The port could not be created because no DirectSound object has be"
            "en specified.";
        break;
    case DMUS_E_GET_UNSUPPORTED:
        Return = "DMUS_E_GET_UNSUPPORTED";
        if (AddStr) *AddStr =
            "Getting the parameter is not supported.";
        break;
    case DMUS_E_INSUFFICIENTBUFFER:
        Return = "DMUS_E_INSUFFICIENTBUFFER";
        if (AddStr) *AddStr =
            "The buffer is not large enough for the requested operation.";
        break;
    case DMUS_E_INVALID_BAND:
        Return = "DMUS_E_INVALID_BAND";
        if (AddStr) *AddStr =
            "The file does not contain a valid band.";
        break;
    case DMUS_E_INVALID_DOWNLOADID:
        Return = "DMUS_E_INVALID_DOWNLOADID";
        if (AddStr) *AddStr =
            "An invalid download identifier was used in the process of creating"
            " a download buffer.";
        break;
    case DMUS_E_INVALID_EVENT:
        Return = "DMUS_E_INVALID_EVENT";
        if (AddStr) *AddStr =
            "The event either is not a valid MIDI message or makes use of runni"
            "ng status and cannot be packed into the buffer.";
        break;
    case DMUS_E_INVALID_TOOL_HDR:
        Return = "DMUS_E_INVALID_TOOL_HDR";
        if (AddStr) *AddStr =
            "The IStream object's data contains an invalid tool header and, the"
            "refore, cannot be read by the graph object.";
        break;
    case DMUS_E_INVALID_TRACK_HDR:
        Return = "DMUS_E_INVALID_TRACK_HDR";
        if (AddStr) *AddStr =
            "The IStream object's data contains an invalid track header and, th"
            "erefore, cannot be read by the segment object.";
        break;
    case DMUS_E_INVALIDBUFFER:
        Return = "DMUS_E_INVALIDBUFFER";
        if (AddStr) *AddStr =
            "An invalid DirectSound buffer was handed to a port.";
        break;
    case DMUS_E_INVALIDFILE:
        Return = "DMUS_E_INVALIDFILE";
        if (AddStr) *AddStr =
            "Not a valid file.";
        break;
    case DMUS_E_INVALIDOFFSET:
        Return = "DMUS_E_INVALIDOFFSET";
        if (AddStr) *AddStr =
            "Wave chunks in the DLS collection file are at incorrect offsets.";
        break;
    case DMUS_E_INVALIDPATCH:
        Return = "DMUS_E_INVALIDPATCH";
        if (AddStr) *AddStr =
            "No instrument in the collection matches the patch number.";
        break;
    case DMUS_E_INVALIDPOS:
        Return = "DMUS_E_INVALIDPOS";
        if (AddStr) *AddStr =
            "Error reading wave data from a DLS collection. Indicates bad file.";
        break;
    case DMUS_E_LOADER_BADPATH:
        Return = "DMUS_E_LOADER_BADPATH";
        if (AddStr) *AddStr =
            "The file path is invalid.";
        break;
    case DMUS_E_LOADER_FAILEDCREATE:
        Return = "DMUS_E_LOADER_FAILEDCREATE";
        if (AddStr) *AddStr =
            "The object could not be found or created.";
        break;
    case DMUS_E_LOADER_FAILEDOPEN:
        Return = "DMUS_E_LOADER_FAILEDOPEN";
        if (AddStr) *AddStr =
            "File open failed because the file does not exist or is locked.";
        break;
    case DMUS_E_LOADER_FORMATNOTSUPPORTED:
        Return = "DMUS_E_LOADER_FORMATNOTSUPPORTED";
        if (AddStr) *AddStr =
            "The object cannot be loaded because the data format is not support"
            "ed.";
        break;
    case DMUS_E_LOADER_NOCLASSID:
        Return = "DMUS_E_LOADER_NOCLASSID";
        if (AddStr) *AddStr =
            "No class ID was supplied in DMUS_OBJECTDESC.";
        break;
    case DMUS_E_LOADER_NOFILENAME:
        Return = "DMUS_E_LOADER_NOFILENAME";
        if (AddStr) *AddStr =
            "No file name was supplied in DMUS_OBJECTDESC.";
        break;
    case DMUS_E_LOADER_OBJECTNOTFOUND:
        Return = "DMUS_E_LOADER_OBJECTNOTFOUND";
        if (AddStr) *AddStr =
            "The object was not found.";
        break;
    case DMUS_E_NO_MASTER_CLOCK:
        Return = "DMUS_E_NO_MASTER_CLOCK";
        if (AddStr) *AddStr =
            "There is no master clock in the performance. Be sure to call the I"
            "DirectMusicPerformance::Init method.";
        break;
    case DMUS_E_NOARTICULATION:
        Return = "DMUS_E_NOARTICULATION";
        if (AddStr) *AddStr =
            "Articulation missing from an instrument in the DLS collection.";
        break;
    case DMUS_E_NOSYNTHSINK:
        Return = "DMUS_E_NOSYNTHSINK";
        if (AddStr) *AddStr =
            "No sink is connected to the synthesizer.";
        break;
    case DMUS_E_NOT_DOWNLOADED_TO_PORT:
        Return = "DMUS_E_NOT_DOWNLOADED_TO_PORT";
        if (AddStr) *AddStr =
            "The object cannot be unloaded because it is not present on the por"
            "t.";
        break;
    case DMUS_E_NOT_FOUND:
        Return = "DMUS_E_NOT_FOUND";
        if (AddStr) *AddStr =
            "The requested item is not contained by the object.";
        break;
    case DMUS_E_NOT_INIT:
        Return = "DMUS_E_NOT_INIT";
        if (AddStr) *AddStr =
            "A required object is not initialized or failed to initialize.";
        break;
    case DMUS_E_NOTADLSCOL:
        Return = "DMUS_E_NOTADLSCOL";
        if (AddStr) *AddStr =
            "The object being loaded is not a valid DLS collection.";
        break;
    case DMUS_E_NOTMONO:
        Return = "DMUS_E_NOTMONO";
        if (AddStr) *AddStr =
            "The wave chunk has more than one interleaved channel. DLS format r"
            "equires mono.";
        break;
    case DMUS_E_NOTPCM:
        Return = "DMUS_E_NOTPCM";
        if (AddStr) *AddStr =
            "Wave data is not in PCM format.";
        break;
    case DMUS_E_OUT_OF_RANGE:
        Return = "DMUS_E_OUT_OF_RANGE";
        if (AddStr) *AddStr =
            "The requested time is outside the range of the segment.";
        break;
    case DMUS_E_PORT_NOT_CAPTURE:
        Return = "DMUS_E_PORT_NOT_CAPTURE";
        if (AddStr) *AddStr =
            "Not a capture port.";
        break;
    case DMUS_E_PORT_NOT_RENDER:
        Return = "DMUS_E_PORT_NOT_RENDER";
        if (AddStr) *AddStr =
            "Not an output port.";
        break;
    case DMUS_E_PORTS_OPEN:
        Return = "DMUS_E_PORTS_OPEN";
        if (AddStr) *AddStr =
            "The requested operation cannot be performed while there are instan"
            "tiated ports in any process in the system.";
        break;
    case DMUS_E_SEGMENT_INIT_FAILED:
        Return = "DMUS_E_SEGMENT_INIT_FAILED";
        if (AddStr) *AddStr =
            "Segment initialization failed, probably because of a critical memo"
            "ry situation.";
        break;
    case DMUS_E_SET_UNSUPPORTED:
        Return = "DMUS_E_SET_UNSUPPORTED";
        if (AddStr) *AddStr =
            "Setting the parameter is not supported.";
        break;
    case DMUS_E_SYNTHACTIVE:
        Return = "DMUS_E_SYNTHACTIVE";
        if (AddStr) *AddStr =
            "The synthesizer has been activated, and the parameter cannot be ch"
            "anged.";
        break;
    case DMUS_E_SYNTHINACTIVE:
        Return = "DMUS_E_SYNTHINACTIVE";
        if (AddStr) *AddStr =
            "The synthesizer has not been activated and cannot process data.";
        break;
    case DMUS_E_SYNTHNOTCONFIGURED:
        Return = "DMUS_E_SYNTHNOTCONFIGURED";
        if (AddStr) *AddStr =
            "The synthesizer is not properly configured or opened.";
        break;
    case DMUS_E_TIME_PAST:
        Return = "DMUS_E_TIME_PAST";
        if (AddStr) *AddStr =
            "The time requested is in the past.";
        break;
    case DMUS_E_TOOL_HDR_NOT_FIRST_CK:
        Return = "DMUS_E_TOOL_HDR_NOT_FIRST_CK";
        if (AddStr) *AddStr =
            "The IStream object's data does not have a tool header as the first"
            " chunk and, therefore, cannot be read by the graph object.";
        break;
    case DMUS_E_TRACK_HDR_NOT_FIRST_CK:
        Return = "DMUS_E_TRACK_HDR_NOT_FIRST_CK";
        if (AddStr) *AddStr =
            "The IStream object's data does not have a track header as the firs"
            "t chunk and, therefore, cannot be read by the segment object.";
        break;
    case DMUS_E_TRACK_NOT_FOUND:
        Return = "DMUS_E_TRACK_NOT_FOUND";
        if (AddStr) *AddStr =
            "There is no track of the requested type.";
        break;
    case DMUS_E_TYPE_DISABLED:
        Return = "DMUS_E_TYPE_DISABLED";
        if (AddStr) *AddStr =
            "A track parameter is unavailable because it has been disabled.";
        break;
    case DMUS_E_TYPE_UNSUPPORTED:
        Return = "DMUS_E_TYPE_UNSUPPORTED";
        if (AddStr) *AddStr =
            "Parameter is unsupported on this track.";
        break;
    case DMUS_E_UNKNOWNDOWNLOAD:
        Return = "DMUS_E_UNKNOWNDOWNLOAD";
        if (AddStr) *AddStr =
            "The synthesizer does not support this type of download.";
        break;
    case DMUS_E_UNKNOWN_PROPERTY:
        Return = "DMUS_E_UNKNOWN_PROPERTY";
        if (AddStr) *AddStr =
            "The property set or item is not implemented by this port.";
        break;
    case DMUS_E_UNSUPPORTED_STREAM:
        Return = "DMUS_E_UNSUPPORTED_STREAM";
        if (AddStr) *AddStr =
            "The IStream object does not contain data supported by the loading "
            "object.";
        break;
    case DMUS_E_WAVEFORMATNOTSUPPORTED:
        Return = "DMUS_E_WAVEFORMATNOTSUPPORTED";
        if (AddStr) *AddStr =
            "Invalid buffer format was handed to the synthesizer sink.";
        break;
    case DMUS_S_DOWN_OCTAVE:
        Return = "DMUS_S_DOWN_OCTAVE";
        if (AddStr) *AddStr =
            "The note has been lowered by one or more octaves to fit within the"
            " range of MIDI values.";
        break;
    case DMUS_S_END:
        Return = "DMUS_S_END";
        if (AddStr) *AddStr =
            "The operation succeeded and reached the end of the data.";
        break;
    case DMUS_S_FREE:
        Return = "DMUS_S_FREE";
        if (AddStr) *AddStr =
            "The allocated memory should be freed.";
        break;
    case DMUS_S_LAST_TOOL:
        Return = "DMUS_S_LAST_TOOL";
        if (AddStr) *AddStr =
            "There are no more tools in the graph.";
        break;
    case DMUS_S_NOBUFFERCONTROL:
        Return = "DMUS_S_NOBUFFERCONTROL";
        if (AddStr) *AddStr =
            "Although the audio output from the port is routed to the same devi"
            "ce as the given DirectSound buffer, buffer controls such as pan an"
            "d volume do not affect the output.";
        break;
    case DMUS_S_OVER_CHORD:
        Return = "DMUS_S_OVER_CHORD";
        if (AddStr) *AddStr =
            "No MIDI values have been calculated because the music value has th"
            "e note at a position higher than the top note of the chord.";
        break;
    case DMUS_S_PARTIALDOWNLOAD:
        Return = "DMUS_S_PARTIALDOWNLOAD";
        if (AddStr) *AddStr =
            "Some instruments could not be downloaded to the port.";
        break;
    case DMUS_S_PARTIALLOAD:
        Return = "DMUS_S_PARTIALLOAD";
        if (AddStr) *AddStr =
            "The object could only load partially. This can happen if some comp"
            "onents, such as embedded tracks and tools, are not registered prop"
            "erly.";
        break;
    case DMUS_S_REQUEUE:
        Return = "DMUS_S_REQUEUE";
        if (AddStr) *AddStr =
            "The message should be passed to the next tool.";
        break;
    case DMUS_S_STRING_TRUNCATED:
        Return = "DMUS_S_STRING_TRUNCATED";
        if (AddStr) *AddStr =
            "The method succeeded, but the returned string had to be truncated.";
        break;
    case DMUS_S_UP_OCTAVE:
        Return = "DMUS_S_UP_OCTAVE";
        if (AddStr) *AddStr =
            "The note has been raised by one or more octaves to fit within the "
            "range of MIDI values.";
        break;

// DirectPlay:

    case DPERR_ABORTED:
        Return = "DPERR_ABORTED";
        if (AddStr) *AddStr =
            "The operation was canceled before it could be completed.";
        break;
    case DPERR_ACCESSDENIED:
        Return = "DPERR_ACCESSDENIED";
        if (AddStr) *AddStr =
            "The session is full, or an incorrect password was supplied.";
        break;
    case DPERR_ACTIVEPLAYERS:
        Return = "DPERR_ACTIVEPLAYERS";
        if (AddStr) *AddStr =
            "The requested operation cannot be performed because there are exis"
            "ting active players.";
        break;
    case DPERR_ALREADYINITIALIZED:
        Return = "DPERR_ALREADYINITIALIZED";
        if (AddStr) *AddStr =
            "This object is already initialized.";
        break;
    case DPERR_APPNOTSTARTED:
        Return = "DPERR_APPNOTSTARTED";
        if (AddStr) *AddStr =
            "The application has not been started yet.";
        break;
    case DPERR_AUTHENTICATIONFAILED:
        Return = "DPERR_AUTHENTICATIONFAILED";
        if (AddStr) *AddStr =
            "The password or credentials supplied could not be authenticated.";
        break;
    case DPERR_BUFFERTOOLARGE:
        Return = "DPERR_BUFFERTOOLARGE";
        if (AddStr) *AddStr =
            "The data buffer is too large to store.";
        break;
    case DPERR_BUFFERTOOSMALL:
        Return = "DPERR_BUFFERTOOSMALL";
        if (AddStr) *AddStr =
            "The supplied buffer is not large enough to contain the requested d"
            "ata.";
        break;
    case DPERR_BUSY:
        Return = "DPERR_BUSY";
        if (AddStr) *AddStr =
            "A message cannot be sent because the transmission medium is busy.";
        break;
    case DPERR_CANCELFAILED:
        Return = "DPERR_CANCELFAILED";
        if (AddStr) *AddStr =
            "The message could not be canceled, possibly because it is a group "
            "message that has already been to sent to one or more members of th"
            "e group.";
        break;
    case DPERR_CANCELLED:
        Return = "DPERR_CANCELLED";
        if (AddStr) *AddStr =
            "The operation was canceled.";
        break;
    case DPERR_CANNOTCREATESERVER:
        Return = "DPERR_CANNOTCREATESERVER";
        if (AddStr) *AddStr =
            "The server cannot be created for the new session.";
        break;
    case DPERR_CANTADDPLAYER:
        Return = "DPERR_CANTADDPLAYER";
        if (AddStr) *AddStr =
            "The player cannot be added to the session.";
        break;
    case DPERR_CANTCREATEGROUP:
        Return = "DPERR_CANTCREATEGROUP";
        if (AddStr) *AddStr =
            "A new group cannot be created.";
        break;
    case DPERR_CANTCREATEPLAYER:
        Return = "DPERR_CANTCREATEPLAYER";
        if (AddStr) *AddStr =
            "A new player cannot be created.";
        break;
    case DPERR_CANTCREATEPROCESS:
        Return = "DPERR_CANTCREATEPROCESS";
        if (AddStr) *AddStr =
            "Cannot start the application.";
        break;
    case DPERR_CANTCREATESESSION:
        Return = "DPERR_CANTCREATESESSION";
        if (AddStr) *AddStr =
            "A new session cannot be created.";
        break;
    case DPERR_CANTLOADCAPI:
        Return = "DPERR_CANTLOADCAPI";
        if (AddStr) *AddStr =
            "No credentials were supplied and the CryptoAPI package (CAPI) to u"
            "se for cryptography services cannot be loaded.";
        break;
    case DPERR_CANTLOADSECURITYPACKAGE:
        Return = "DPERR_CANTLOADSECURITYPACKAGE";
        if (AddStr) *AddStr =
            "The software security package cannot be loaded.";
        break;
    case DPERR_CANTLOADSSPI:
        Return = "DPERR_CANTLOADSSPI";
        if (AddStr) *AddStr =
            "No credentials were supplied, and the Security Support Provider In"
            "terface (SSPI) that will prompt for credentials cannot be loaded.";
        break;
    case DPERR_CAPSNOTAVAILABLEYET:
        Return = "DPERR_CAPSNOTAVAILABLEYET";
        if (AddStr) *AddStr =
            "The capabilities of the DirectPlay object have not been determined"
            " yet. This error will occur if the DirectPlay object is implemente"
            "d on a connectivity solution that requires polling to determine av"
            "ailable bandwidth and latency.";
        break;
    case DPERR_CONNECTING:
        Return = "DPERR_CONNECTING";
        if (AddStr) *AddStr =
            "The method is in the process of connecting to the network. The app"
            "lication should keep using the method until it returns DP_OK, indi"
            "cating successful completion, or until it returns a different erro"
            "r.";
        break;
    case DPERR_CONNECTIONLOST:
        Return = "DPERR_CONNECTIONLOST";
        if (AddStr) *AddStr =
            "The service provider connection was reset while data was being sen"
            "t.";
        break;
    case DPERR_ENCRYPTIONFAILED:
        Return = "DPERR_ENCRYPTIONFAILED";
        if (AddStr) *AddStr =
            "The requested information could not be digitally encrypted. Encryp"
            "tion is used for message privacy. This error is only relevant in a"
            " secure session.";
        break;
    case DPERR_ENCRYPTIONNOTSUPPORTED:
        Return = "DPERR_ENCRYPTIONNOTSUPPORTED";
        if (AddStr) *AddStr =
            "The type of encryption requested in the DPSECURITYDESC structure i"
            "s not available. This error is only relevant in a secure session.";
        break;
    case DPERR_EXCEPTION:
        Return = "DPERR_EXCEPTION";
        if (AddStr) *AddStr =
            "An exception occurred when processing the request.";
        break;
    case DPERR_INVALIDFLAGS:
        Return = "DPERR_INVALIDFLAGS";
        if (AddStr) *AddStr =
            "The flags passed to this method are invalid.";
        break;
    case DPERR_INVALIDGROUP:
        Return = "DPERR_INVALIDGROUP";
        if (AddStr) *AddStr =
            "The group ID is not recognized as a valid group ID for this game s"
            "ession.";
        break;
    case DPERR_INVALIDINTERFACE:
        Return = "DPERR_INVALIDINTERFACE";
        if (AddStr) *AddStr =
            "The interface parameter is invalid.";
        break;
    case DPERR_INVALIDOBJECT:
        Return = "DPERR_INVALIDOBJECT";
        if (AddStr) *AddStr =
            "The DirectPlay object pointer is invalid.";
        break;
    case DPERR_INVALIDPASSWORD:
        Return = "DPERR_INVALIDPASSWORD";
        if (AddStr) *AddStr =
            "An invalid password was supplied when attempting to join a session"
            " that requires a password.";
        break;
    case DPERR_INVALIDPLAYER:
        Return = "DPERR_INVALIDPLAYER";
        if (AddStr) *AddStr =
            "The player ID is not recognized as a valid player ID for this game"
            " session.";
        break;
    case DPERR_INVALIDPRIORITY:
        Return = "DPERR_INVALIDPRIORITY";
        if (AddStr) *AddStr =
            "The specified priority is not within the range of allowed prioriti"
            "es, which is inclusively 0-65535.";
        break;
    case DPERR_LOGONDENIED:
        Return = "DPERR_LOGONDENIED";
        if (AddStr) *AddStr =
            "The session could not be opened because credentials are required, "
            "and either no credentials were supplied, or the credentials were i"
            "nvalid.";
        break;
    case DPERR_NOCAPS:
        Return = "DPERR_NOCAPS";
        if (AddStr) *AddStr =
            "The communication link that DirectPlay is attempting to use is not"
            " capable of this function.";
        break;
    case DPERR_NOCONNECTION:
        Return = "DPERR_NOCONNECTION";
        if (AddStr) *AddStr =
            "No communication link was established.";
        break;
    case DPERR_NOMESSAGES:
        Return = "DPERR_NOMESSAGES";
        if (AddStr) *AddStr =
            "There are no messages in the receive queue.";
        break;
    case DPERR_NONAMESERVERFOUND:
        Return = "DPERR_NONAMESERVERFOUND";
        if (AddStr) *AddStr =
            "No name server (host) could be found or created. A host must exist"
            " to create a player.";
        break;
    case DPERR_NONEWPLAYERS:
        Return = "DPERR_NONEWPLAYERS";
        if (AddStr) *AddStr =
            "The session is not accepting any new players.";
        break;
    case DPERR_NOPLAYERS:
        Return = "DPERR_NOPLAYERS";
        if (AddStr) *AddStr =
            "There are no active players in the session.";
        break;
    case DPERR_NOSESSIONS:
        Return = "DPERR_NOSESSIONS";
        if (AddStr) *AddStr =
            "There are no existing sessions for this game.";
        break;
    case DPERR_NOTLOBBIED:
        Return = "DPERR_NOTLOBBIED";
        if (AddStr) *AddStr =
            "Returned by the IDirectPlayLobby3::Connect method if the applicati"
            "on was not started by using the IDirectPlayLobby3::RunApplication "
            "method, or if there is no DPLCONNECTION structure currently initia"
            "lized for this DirectPlayLobby object.";
        break;
    case DPERR_NOTLOGGEDIN:
        Return = "DPERR_NOTLOGGEDIN";
        if (AddStr) *AddStr =
            "An action cannot be performed because a player or client applicati"
            "on is not logged on. Returned by the IDirectPlay4::Send method whe"
            "n the client application tries to send a secure message without be"
            "ing logged on.";
        break;
    case DPERR_PLAYERLOST:
        Return = "DPERR_PLAYERLOST";
        if (AddStr) *AddStr =
            "A player has lost the connection to the session.";
        break;
    case DPERR_SENDTOOBIG:
        Return = "DPERR_SENDTOOBIG";
        if (AddStr) *AddStr =
            "The message being sent by the IDirectPlay4::Send method is too lar"
            "ge.";
        break;
    case DPERR_SESSIONLOST:
        Return = "DPERR_SESSIONLOST";
        if (AddStr) *AddStr =
            "The connection to the session has been lost.";
        break;
    case DPERR_SIGNFAILED:
        Return = "DPERR_SIGNFAILED";
        if (AddStr) *AddStr =
            "The requested information could not be digitally signed. Digital s"
            "ignatures are used to establish the authenticity of messages.";
        break;
    case DPERR_TIMEOUT:
        Return = "DPERR_TIMEOUT";
        if (AddStr) *AddStr =
            "The operation could not be completed in the specified time.";
        break;
    case DPERR_UNAVAILABLE:
        Return = "DPERR_UNAVAILABLE";
        if (AddStr) *AddStr =
            "The requested function is not available at this time.";
        break;
    case DPERR_UNINITIALIZED:
        Return = "DPERR_UNINITIALIZED";
        if (AddStr) *AddStr =
            "The requested object has not been initialized.";
        break;
    case DPERR_UNKNOWNAPPLICATION:
        Return = "DPERR_UNKNOWNAPPLICATION";
        if (AddStr) *AddStr =
            "An unknown application was specified.";
        break;
    case DPERR_UNKNOWNMESSAGE:
        Return = "DPERR_UNKNOWNMESSAGE";
        if (AddStr) *AddStr =
            "The message ID isn't valid. Returned from IDirectPlay4::CancelMess"
            "age if the ID of the message to be canceled is invalid.";
        break;
    case DPERR_USERCANCEL:
        Return = "DPERR_USERCANCEL";
        if (AddStr) *AddStr =
            "Can be returned in two ways. 1) The user canceled the connection p"
            "rocess during a call to the IDirectPlay4::Open method. 2) The user"
            " clicked Cancel in one of the DirectPlay service provider dialog b"
            "oxes during a call to IDirectPlay4::EnumSessions.";
        break;
/*
Where is it defined???
    case E_UNKNOWN:
        Return = "E_UNKNOWN";
        if (AddStr) *AddStr =
            "Usually indicates a catastrophic failure of some sort; for example"
            ", the service provider cannot initialize correctly or a protocol s"
            "uch as IPX is not installed correctly on the machine.";
        break;
*/

// DirectSound:

    case DSERR_ALLOCATED:
        Return = "DSERR_ALLOCATED";
        if (AddStr) *AddStr =
            "The request failed because resources, such as a priority level, we"
            "re already in use by another caller.";
        break;
    case DSERR_ALREADYINITIALIZED:
        Return = "DSERR_ALREADYINITIALIZED";
        if (AddStr) *AddStr =
            "The object is already initialized.";
        break;
    case DSERR_BADFORMAT:
        Return = "DSERR_BADFORMAT";
        if (AddStr) *AddStr =
            "The specified wave format is not supported.";
        break;
    case DSERR_BUFFERLOST:
        Return = "DSERR_BUFFERLOST";
        if (AddStr) *AddStr =
            "The buffer memory has been lost and must be restored.";
        break;
    case DSERR_CONTROLUNAVAIL:
        Return = "DSERR_CONTROLUNAVAIL";
        if (AddStr) *AddStr =
            "The buffer control (volume, pan, and so on) requested by the calle"
            "r is not available.";
        break;
/*
Where is it defined???
    case DSERR_HWUNAVAIL:
        Return = "DSERR_HWUNAVAIL";
        if (AddStr) *AddStr =
            "The DirectSound hardware device is unavailable.";
        break;
*/
    case DSERR_INVALIDCALL:
        Return = "DSERR_INVALIDCALL";
        if (AddStr) *AddStr =
            "This function is not valid for the current state of this object.";
        break;
    case DSERR_NODRIVER:
        Return = "DSERR_NODRIVER";
        if (AddStr) *AddStr =
            "No sound driver is available for use.";
        break;
    case DSERR_OTHERAPPHASPRIO:
        Return = "DSERR_OTHERAPPHASPRIO";
        if (AddStr) *AddStr =
            "Another application has a higher priority level, preventing this c"
            "all from succeeding";
        break;
    case DSERR_PRIOLEVELNEEDED:
        Return = "DSERR_PRIOLEVELNEEDED";
        if (AddStr) *AddStr =
            "The caller does not have the priority level required for the funct"
            "ion to succeed.";
        break;
    case DSERR_UNINITIALIZED:
        Return = "DSERR_UNINITIALIZED";
        if (AddStr) *AddStr =
            "The IDirectSound::Initialize method has not been called or has not"
            " been called successfully before other methods were called.";
        break;

// DirectSetup:

    case DSETUPERR_BADSOURCESIZE:
        Return = "DSETUPERR_BADSOURCESIZE";
        if (AddStr) *AddStr =
            "A file's size could not be verified or was incorrect.";
        break;
    case DSETUPERR_BADSOURCETIME:
        Return = "DSETUPERR_BADSOURCETIME";
        if (AddStr) *AddStr =
            "A file's date and time could not be verified or were incorrect.";
        break;
    case DSETUPERR_BADWINDOWSVERSION:
        Return = "DSETUPERR_BADWINDOWSVERSION";
        if (AddStr) *AddStr =
            "DirectX does not support the Windows version on the system.";
        break;
    case DSETUPERR_CANTFINDDIR:
        Return = "DSETUPERR_CANTFINDDIR";
        if (AddStr) *AddStr =
            "The setup program could not find the working directory.";
        break;
    case DSETUPERR_CANTFINDINF:
        Return = "DSETUPERR_CANTFINDINF";
        if (AddStr) *AddStr =
            "A required .inf file could not be found.";
        break;
    case DSETUPERR_INTERNAL:
        Return = "DSETUPERR_INTERNAL";
        if (AddStr) *AddStr =
            "An internal error occurred.";
        break;
    case DSETUPERR_NEWERVERSION:
        Return = "DSETUPERR_NEWERVERSION";
        if (AddStr) *AddStr =
            "A later version of DirectX has already been installed. Application"
            "s can safely ignore this error because the newer version is not ov"
            "erwritten and is fully compatible with applications written for ea"
            "rlier versions.";
        break;
    case DSETUPERR_NOCOPY:
        Return = "DSETUPERR_NOCOPY";
        if (AddStr) *AddStr =
            "A file's version could not be verified or was incorrect.";
        break;
    case DSETUPERR_NOTPREINSTALLEDONNT:
        Return = "DSETUPERR_NOTPREINSTALLEDONNT";
        if (AddStr) *AddStr =
            "The version of Windows NT/Windows 2000 on the system does not cont"
            "ain the current version of DirectX. An older version of DirectX mi"
            "ght be present, or DirectX might be absent altogether.";
        break;
    case DSETUPERR_OUTOFDISKSPACE:
        Return = "DSETUPERR_OUTOFDISKSPACE";
        if (AddStr) *AddStr =
            "The setup program ran out of disk space during installation.";
        break;
    case DSETUPERR_SOURCEFILENOTFOUND:
        Return = "DSETUPERR_SOURCEFILENOTFOUND";
        if (AddStr) *AddStr =
            "One of the required source files could not be found.";
        break;
    case DSETUPERR_UNKNOWNOS:
        Return = "DSETUPERR_UNKNOWNOS";
        if (AddStr) *AddStr =
            "The operating system on your system is not currently supported.";
        break;
    case DSETUPERR_USERHITCANCEL:
        Return = "DSETUPERR_USERHITCANCEL";
        if (AddStr) *AddStr =
            "The Cancel button was pressed before the application was fully ins"
            "talled.";
        break;

    default:
        Return = "Unknown Return Value";
        if (AddStr) *AddStr =
            "There is no error description available for this return value.";
        break;
    }

    return Return;
}
//---------------------------------------------------------------------------


 