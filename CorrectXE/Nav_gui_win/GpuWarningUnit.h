//---------------------------------------------------------------------------

#ifndef GpuWarningUnitH
#define GpuWarningUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "GpuAccessor.h"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>

//---------------------------------------------------------------------------
class TgpuWarningForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *WarningLabel;
	TButton *Button1;
	TCheckBox *GpuWarningCheckBox;
	TSpeedButton *GotoDriveWebPageSpeedButton;
	TSpeedButton *LaunchGeForceSpeedButton;
	void __fastcall GpuWarningCheckBoxClick(TObject *Sender);
	void __fastcall GotoDriveWebPageSpeedButtonClick(TObject *Sender);
	void __fastcall LaunchGeForceSpeedButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TgpuWarningForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TgpuWarningForm *gpuWarningForm;
//---------------------------------------------------------------------------
#endif
