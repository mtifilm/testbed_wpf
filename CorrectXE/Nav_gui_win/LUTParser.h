#ifndef _LUTREADER
#define _LUTREADER

#define LUTHEADER_LINEBLANK             "#"
#define LUTHEADER_LINEDESC              "LUT "
#define LUTHEADER_LINEVERSION           "Version"
#define LUTHEADER_LINEWHITE             "RGB Reference White:"
#define LUTHEADER_LINEBLACK             "RGB Reference Black:"
#define LUTHEADER_LINESOFTCLIP          "RGB Softclip:"
#define LUTHEADER_LIENGAMMA             "RGB Film Gamma:"
#define LUTHEADER_LINEGAMMACORRECT      "RGB Gamma Correction:"
#define LUTHEADER_LINEHIGHLIGHT         "RGB HighLight:"
#define LUTHEADER_LINESHADOW            "RGB Shadow:"
#define LUTHEADER_DATA                  "LUT:"

#define LUTDESC_LOGLINEAR               "10-bit log to 8-bit linear"
#define LUTDESC_LINEARLINEAR            "10-bit linear to 8-bit linear"
#define LUTDESC_EXRLINEAR               "EXR to 8-bit linear"
#define LUTDESC_SMPTELINEAR             "SMPTE to 8-bit linear"
#define LUTDESC_CCIR709LINEAR           "CCIR 709 to 8-bit linear"
#define LUTDESC_CCIR601BGLINEAR         "CCIR 601 BG to 8-bit linear"
#define LUTDESC_CCIR601MLINEAR          "CCIR 601 M to 8-bit linear"
#define LUTDESC_OTHER                   "Other LUT"




enum LUT_TYPE {OTHER, LOGLINEAR};



class CLUTParser
{
public:

  CLUTParser();
  ~CLUTParser();

  const char    *getFilename() const;
  int            getVersion() const;
  const char    *getDescription() const;

  unsigned short getRefWhite(int) const;
  unsigned short getRefBlack(int) const;
  unsigned short getSoftclip(int) const;
  unsigned short getHighlight(int) const;
  unsigned short getShadow(int) const;
  float          getGamma(int) const;
  float          getGammaCorrect(int) const;

  void           setFilename(const char *);
  void           setVersion(int);

  void           setRefWhite(const unsigned short *);
  void           setRefBlack(const unsigned short *);
  void           setSoftclip(const unsigned short *);
  void           setGamma(const float *);
  void           setGammaCorrect(const float *);
  void           setHighlight(const unsigned short *);
  void           setShadow(const unsigned short *);
  void           setDescription(const char *);

  bool           isSupported() const;
  bool           save();
  bool           load();
  bool           isSaved() const;
  bool           isLoaded() const;
  const char    *getLastErrorMsg() const;

private:

  bool           parse();
  char *         readline(int, char *);

  char           cpFilename[256];
  char           description[512];
  int            version;

  unsigned short refWhite[3];
  unsigned short refBlack[3];
  unsigned short softclip[3];
  float          gamma[3];
  float          gammaCorrect[3];
  unsigned short highlight[3];
  unsigned short shadow[3];

  int           *data;

  char           lastErrorMsg[512];
};
#endif