//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CustomReticleUnit.h"
#include "MainWindowUnit.h"
#include "MTIDialogs.h"
#include "ToolSystemInterface.h"
#include "Player.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "cspin"
#pragma resource "*.dfm"
#pragma link "SpinEditFrameUnit"
TCustomReticleDlg *CustomReticleDlg;

#define RETINIFILE      "$(CPMP_USER_DIR)RETICLEList.ini"

#define RETFILELABEL    "RETICLE file:"

#define RETDIRECTORY    "$(CPMP_USER_DIR)\\reticles"

string TCustomReticleDlg::reticleUnnamed="[NEW]";
string TCustomReticleDlg::reticleSectionName="Reticles";
string TCustomReticleDlg::defaultReticle[]={ "[NONE]",
                                             "DEFAULT_133_FULL",
                                             "DEFAULT_166_FULL",
                                             "DEFAULT_178_FULL",
                                             "DEFAULT_185_FULL",
                                             "DEFAULT_235_FULL",
                                             "DEFAULT_240_FULL",
                                             "DEFAULT_133_ACADEMY",
                                             "DEFAULT_166_ACADEMY",
                                             "DEFAULT_178_ACADEMY",
                                             "DEFAULT_185_ACADEMY",
                                             "DEFAULT_235_ACADEMY",
                                             "DEFAULT_240_ACADEMY"
                                           };

//---------------------------------------------------------------------------
__fastcall TCustomReticleDlg::TCustomReticleDlg(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::FormCreate(TObject *Sender)
{
   PresetButton133->Tag = RETICLE_TYPE_133_FULL;
   PresetButton166->Tag = RETICLE_TYPE_166_FULL;
   PresetButton178->Tag = RETICLE_TYPE_178_FULL;
   PresetButton185->Tag = RETICLE_TYPE_185_FULL;
   PresetButton235->Tag = RETICLE_TYPE_235_FULL;
   PresetButton240->Tag = RETICLE_TYPE_240_FULL;

   // Not sure why we need to create these controls dynamically
   inhibitSpinEditCallbacks = false;
//   XOffsetSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xXOffsetSpinEdit, "XOffsetSpinEdit");
//   YOffsetSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xYOffsetSpinEdit, "YOffsetSpinEdit");
//   HScaleSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xHScaleSpinEdit, "HScaleSpinEdit");
//   VScaleSpinEdit = TSpinEditFrame::CreateFromTemplate(this, xVScaleSpinEdit, "VScaleSpinEdit");

   SET_CBHOOK(OffsetSpinEditChange, XOffsetSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OffsetSpinEditChange, YOffsetSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OffsetSpinEditChange, HScaleSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OffsetSpinEditChange, VScaleSpinEdit->SpinEditValueChange);

   // load the combo box pulldown with choices
   LoadReticleListIni();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::FormShow(TObject *Sender)
{
   // the reticle on starting the edit
   CReticle restoreReticle = mainWindow->GetDisplayer()->getReticle();
   mainWindow->GetDisplayer()->setExitReticle(restoreReticle);

   // when editing reticle, he'll probably want all 3 reticles
   mainWindow->GetDisplayer()->setReticleMode(RETICLE_MODE_ALL);

   // updates the reticle widget combo box but not the toolbar's
   UpdateReticleGUI();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::OnLoadReticle(TObject *Sender)
{
   string newReticleName = StringToStdString(ReticleNameComboBox->Text);
   LoadReticle(newReticleName);
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::ReticleNameKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   if (Key==0xD) // return
      OnLoadReticle(Sender);
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::SaveReticleButtonClick(TObject *Sender)
{
   SaveCurrentReticle();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::DeleteReticleButtonClick(
      TObject *Sender)
{
   DeleteCurrentReticle();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::PresetButtonClick(TObject *Sender)
{
   TButton *butn = static_cast <TButton *>(Sender);

   switch(butn->Tag) {

      case RETICLE_TYPE_133_FULL:
         AspectRatioEditBox->Text = "1.3333333333333";
      break;

      case RETICLE_TYPE_166_FULL:
         AspectRatioEditBox->Text = "1.6666666666666";
      break;

      case RETICLE_TYPE_178_FULL:
         AspectRatioEditBox->Text = "1.7777777777777";
      break;

      case RETICLE_TYPE_185_FULL:
         AspectRatioEditBox->Text = "1.8500000000000";
      break;

      case RETICLE_TYPE_235_FULL:
         AspectRatioEditBox->Text = "2.3500000000000";
      break;

      case RETICLE_TYPE_240_FULL:
         AspectRatioEditBox->Text = "2.4000000000000";
      break;
   }

   ChangeAspectRatio();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::OnAspectRatioKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   if (Key==0xD) {
      ChangeAspectRatio();
   }
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::ApertureGroupBoxClick(TObject *Sender)
{
   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();
   currentReticle.setFilename(reticleUnnamed);
   currentReticle.setType(RETICLE_TYPE_CUSTOM);
   currentReticle.setAperture(ApertureGroupBox->ItemIndex);
   UpdateCustomAspectRatio(currentReticle.getAspectRatio());
   ReticleNameComboBox->Text = reticleUnnamed.c_str();

   mainWindow->GetDisplayer()->setReticle(currentReticle);
   mainWindow->GetPlayer()->refreshFrameSynchronousCached();

   // set focus to MainWindow or FullWindow, whichever is active
   systemAPI->setMainWindowFocus();
}

//---------------------------------------------------------------------------
void TCustomReticleDlg::OffsetSpinEditChange(void *Sender)
{
   if (inhibitSpinEditCallbacks)
   {
      return;
   }

   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();
   currentReticle.setFilename(reticleUnnamed);
   currentReticle.setType(RETICLE_TYPE_CUSTOM);
   currentReticle.setXOffset(XOffsetSpinEdit->GetValue());
   currentReticle.setYOffset(YOffsetSpinEdit->GetValue());
   currentReticle.setHScale(HScaleSpinEdit->GetValue());
   currentReticle.setVScale(VScaleSpinEdit->GetValue());
   UpdateCustomAspectRatio(currentReticle.getAspectRatio());
   ReticleNameComboBox->Text =  reticleUnnamed.c_str();;

   mainWindow->GetDisplayer()->setReticle(currentReticle);
   mainWindow->GetPlayer()->refreshFrameSynchronousCached();

   // set focus to MainWindow or FullWindow, whichever is active
   systemAPI->setMainWindowFocus();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::RETOKButtonClick(TObject *Sender)
{
   CReticle restoreReticle = mainWindow->GetDisplayer()->getReticle();
   mainWindow->GetDisplayer()->setExitReticle(restoreReticle);
   mainWindow->UpdateReticleToolbarComboBox();
   mainWindow->Invalidate();
   this->Hide();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::RETCancelButtonClick(TObject *Sender)
{
   CReticle restoreReticle = mainWindow->GetDisplayer()->getExitReticle();
   mainWindow->GetDisplayer()->setReticle(restoreReticle);
   mainWindow-> UpdateReticleToolbarComboBox();
   mainWindow->Invalidate();
   this->Hide();
}

//---------------------------------------------------------------------------
void __fastcall TCustomReticleDlg::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   CReticle restoreReticle = mainWindow->GetDisplayer()->getExitReticle();
   mainWindow->GetDisplayer()->setReticle(restoreReticle);
   mainWindow->Invalidate();
}

//---------------------------------------------------------------------------
// get index of default reticle in default name array
//
int TCustomReticleDlg::GetDefaultReticleType(string retnam)
{
  for (int i = RETICLE_TYPE_NONE; i <= HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX; i++)
  {
     if (retnam == defaultReticle[i])
     {
        return i;
     }
  }

  return -1;
}

//---------------------------------------------------------------------------
// get index of custom reticle in local name list
//
int TCustomReticleDlg::GetCustomReticleIndex(string retnam)
{
   for (unsigned int i=0;i<localnameList.size();i++) {
      if (retnam == localnameList[i])
         return i;
   }

   return -1;
}

//---------------------------------------------------------------------------
// save the reticle named in the reticle name combo box
//
void TCustomReticleDlg::SaveCurrentReticle()
{
   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();

   int type = currentReticle.getType();
   if (type !=RETICLE_TYPE_CUSTOM) {
      _MTIErrorDialog(Handle, "Default reticles have permanent names");
      ReticleNameComboBox->Text = defaultReticle[type].c_str();
      mainWindow->Invalidate();
      return;
   }

   if(mainWindow->GetTimeline()->ReticleToolbarComboBox->Items->Count == 30) {

      MessageBox(NULL, "Only 30 Reticles can be added to list.",
                     "Adding Reticle to List", MB_OK | MB_ICONERROR | MB_TASKMODAL);

      mainWindow->Invalidate();
      return;
   }

   string newReticleName = StringToStdString(ReticleNameComboBox->Text);

   // the name of a default reticle may not be used
   int index = GetDefaultReticleType(newReticleName);
   if (index > -1) {
      _MTIErrorDialog(Handle, "Default reticle names are reserved");
      mainWindow->Invalidate();
      return;
   }

   // if the reticle is a new custom one or was sourced directly
   // from the clip on loading, it needs a filename to be stored
   if ((newReticleName == "[NEW]")||(newReticleName == "[CLIP]")) {
      _MTIErrorDialog(Handle, "A valid reticle name must be supplied");
      mainWindow->Invalidate();
      return;
   }

   // strip out all chars except alphameric, dash, or underscore
   int changes = CReticle::conformFilename(newReticleName);

   // show the conformed reticle name
   ReticleNameComboBox->Text = newReticleName.c_str();

   if ((changes > 0) ||(newReticleName.size()==0)) {

      _MTIErrorDialog(Handle, "Reticle names use alphameric characters, dash, or underscore");
      mainWindow->Invalidate();
      return;
   }
   else { // reticle name is valid

      index = GetCustomReticleIndex(newReticleName);
      if (index > -1) { // already a reticle with this name

         int retVal = _MTIConfirmationDialog("Overwrite existing reticle?");
         if (retVal == MTI_DLG_CANCEL)
            return;
      }

      // make sure reticle directory exists

      string reticleDir;
      reticleDir = ConformEnvironmentName(RETDIRECTORY);

      if(!DirectoryExists(reticleDir.c_str())) {

         CreateDir(reticleDir.c_str());

         if(!DirectoryExists(reticleDir.c_str())) {

            MessageBox(NULL, "Could not save modified Reticle",
                             "Adding Reticle", MB_OK | MB_ICONERROR | MB_TASKMODAL);
            mainWindow->Invalidate();
            return;
         }
      }

      // write reticle to file, leaving filename in reticle
      currentReticle.writeToFile(newReticleName.c_str());
      mainWindow->GetDisplayer()->setReticle(currentReticle);

      // if we didn't overwrite an old reticle, make the
      // appropriate entries into the pulldown & local lists
      if (index < 0) {

         mainWindow->GetTimeline()->ReticleToolbarComboBox->Items->Add(newReticleName.c_str());
         ReticleNameComboBox->Items->Add(newReticleName.c_str());
         localnameList.push_back(newReticleName);
      }
   }

   WriteReticleListIni();
}

//---------------------------------------------------------------------------
// delete the reticle named in the reticle name combo box
//
void TCustomReticleDlg::DeleteCurrentReticle()
{
   int index;

   string currentReticleName = StringToStdString(ReticleNameComboBox->Text);

   if ((currentReticleName=="[NEW]")||(currentReticleName=="[CLIP]"))
      return;

   // default reticles cannot be deleted
   index = GetDefaultReticleType(currentReticleName);
   if (index > -1) {
      _MTIErrorDialog(Handle, "Default reticle cannot be deleted");
      return;
   }

   // see if reticle is custom reticle
   index = GetCustomReticleIndex(currentReticleName);
   if (index < 0) {
      return;
   }

   // delete the reticle file from $(CPMP_USER_DIR)\\reticles
   int retVal = CReticle::deleteReticleFile(currentReticleName);
   if (retVal != 0) {

       return;
   }

   // remove the deleted file from the local lists
   StringList tempNameList;
   int listSize = localnameList.size();
   for(int i=0; i < listSize; i++) {
      if(index != i) {
         tempNameList.push_back(localnameList[i].c_str());
      }
   }

   localnameList.clear();
   localnameList = tempNameList;

   // update the pulldowns
   mainWindow->GetTimeline()->ReticleToolbarComboBox->Items->Delete(RETICLE_TYPE_CUSTOM+index);
   ReticleNameComboBox->Items->Delete(RETICLE_TYPE_CUSTOM+index);

   // clobber the filename in the loaded reticle
   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();
   currentReticle.setFilename(reticleUnnamed);
   mainWindow->GetDisplayer()->setReticle(currentReticle);

   // update the text
   ReticleNameComboBox->Text = reticleUnnamed.c_str();

   // write out the reticle registry
   WriteReticleListIni();
}

//---------------------------------------------------------------------------
// sets the Aspect Ratio edit box contents for the value
//
void TCustomReticleDlg::UpdateCustomAspectRatio(double value)
{
   char valstr[8];

   sprintf(valstr,"%15.13f", value);

   AspectRatioEditBox->Text = valstr;
}

//---------------------------------------------------------------------------
// updates the GUI for a change in reticle Aspect Ratio
//
void TCustomReticleDlg::ChangeAspectRatio()
{
   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();
   currentReticle.setFilename(reticleUnnamed);
   currentReticle.setType(RETICLE_TYPE_CUSTOM);
   currentReticle.setAspectRatio(atof(StringToStdString(AspectRatioEditBox->Text).c_str()));
   UpdateCustomAspectRatio(currentReticle.getAspectRatio());
   ReticleNameComboBox->Text = reticleUnnamed.c_str();

   mainWindow->GetDisplayer()->setReticle(currentReticle);
   mainWindow->GetDisplayer()->setReticleMode(RETICLE_MODE_ALL);
   mainWindow->GetPlayer()->refreshFrameSynchronousCached();

   // set focus to MainWindow or FullWindow, whichever is active
   systemAPI->setMainWindowFocus();
}

//---------------------------------------------------------------------------
// loads ptr to system API (called by ReticleTool)
//
void TCustomReticleDlg::setSystemAPI(CToolSystemInterface *sysapi)
{
   systemAPI = sysapi;
}

//---------------------------------------------------------------------------
// loads the combo box from the reticle toolbar combo box (= main stash)
//
void TCustomReticleDlg::LoadReticleListIni()
{
#if 0
  // build the contents of the reticle combo pulldown

  TStrings *srcReticleItems = mainWindow->ReticleToolbarComboBox->Items;
  TStrings *dstReticleItems = ReticleNameComboBox->Items;
  dstReticleItems->Clear();
  localnameList.clear();

  if (srcReticleItems == NULL) {
       return;
  }

  int i;
  for (i = 0; i <= HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX; i++)
  {
     dstReticleItems->Add(srcReticleItems->Strings[i].c_str());
  }

  for ( ; i < srcReticleItems->Count; i++)
  {
	  dstReticleItems->Add(srcReticleItems->Strings[i].c_str());
     localnameList.push_back(StringToStdString(srcReticleItems->Strings[i]));
  }
#else
  // QQQ REFACTOR THIS - almost identical code is in MainWindow!
  // Build the contents of the reticle name combo box
  TStrings *reticleItems = ReticleNameComboBox->Items;
  reticleItems->Clear();
  localnameList.clear();

  // Load the default reticles into the pulldown
  for (int i = RETICLE_TYPE_NONE; i <= HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX; i++)
  {
     reticleItems->Add(defaultReticle[i].c_str());
  }

  // Load the custom reticles into the pulldown
  CIniFile *ini = CreateIniFile(RETINIFILE);
  StringList strlReticles;
  ini->ReadSection(reticleSectionName, &strlReticles);
  for (StringList::iterator iter = strlReticles.begin();
        iter != strlReticles.end();
        ++iter)
  {
      string strTemp = ini->ReadString(reticleSectionName, *iter, "");
      int endIndex = strTemp.find("]");

      // Add the custom reticle to the dropdown list
      string customName = strTemp.substr(1, endIndex - 1);
      reticleItems->Add(customName.c_str());

      localnameList.push_back(customName);
  }

  DeleteIniFile(ini);
#endif
}

//---------------------------------------------------------------------------
// writes out the reticle registry
//
void TCustomReticleDlg::WriteReticleListIni()
{
   char listEntry[532];

   CIniFile *ini = CreateIniFile(RETINIFILE);

   if(ini == NULL) {
      MessageBox(NULL, "Could not save Reticle list",
                     "Save configuration", MB_OK | MB_TASKMODAL | MB_ICONERROR);
      return;
   }

   StringList strlReticles;

   for (unsigned int i=0; i<localnameList.size(); i++) {

      strcpy(listEntry, "[");
      strcat(listEntry, localnameList[i].c_str());
      strcat(listEntry, "]");
      // strcat(listEntry, localfileList[i].c_str());

      strlReticles.push_back (listEntry);
   }

   ini->WriteStringList("Reticles", "File", &strlReticles);

   ini->SaveIfNecessary();
}

//---------------------------------------------------------------------------
// loads the named reticle
//
void TCustomReticleDlg::LoadReticle(const string& retnam)
{
   CReticle newReticle = mainWindow->GetDisplayer()->getReticle();

   int index = GetDefaultReticleType(retnam);
   if (index > -1) {

      // default reticle chosen
      newReticle.setType(index);
   }
   else {

      index = GetCustomReticleIndex(retnam);
      if (index > -1) {

         // custom reticle chosen
         newReticle.readFromFile(retnam);
      }
   }

   mainWindow->GetDisplayer()->setReticle(newReticle);

   UpdateReticleGUI();

   mainWindow->GetPlayer()->refreshFrameSynchronousCached();
}

//---------------------------------------------------------------------------
// updates the reticle widget from the current reticle
//
void TCustomReticleDlg::UpdateReticleGUI()
{
   CReticle currentReticle = mainWindow->GetDisplayer()->getReticle();

   int type = currentReticle.getType();
   if (type != RETICLE_TYPE_CUSTOM) { // default (incl [NONE])
      ReticleNameComboBox->Text = defaultReticle[type].c_str();
   }
   else {                             // custom
      ReticleNameComboBox->Text = currentReticle.getFilename().c_str();
   }

   UpdateCustomAspectRatio(currentReticle.getAspectRatio());

   // short out the callbacks before updating these GUI items
   ApertureGroupBox->OnClick = NULL;
   ApertureGroupBox->ItemIndex = currentReticle.getAperture();
   ApertureGroupBox->OnClick = ApertureGroupBoxClick;

   inhibitSpinEditCallbacks = true;

   XOffsetSpinEdit->SetValue(currentReticle.getXOffset());
   YOffsetSpinEdit->SetValue(currentReticle.getYOffset());
   HScaleSpinEdit->SetValue(currentReticle.getHScale());
   VScaleSpinEdit->SetValue(currentReticle.getVScale());

   inhibitSpinEditCallbacks = false;
}

//---------------------------------------------------------------------------


