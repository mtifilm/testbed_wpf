//---------------------------------------------------------------------------

#ifndef LUTNameUnitH
#define LUTNameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;
//---------------------------------------------------------------------------

class TLUTNameForm : public TForm
{
__published:	// IDE-managed Components
        TEdit *LUTNameEdit;
        TLabel *Label1;
        TButton *OkButton;
        TButton *CancelButton;
        void __fastcall FormKeyDown(TObject *Sender, Word &Key, TShiftState Shift);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
        bool defaultNameWasSet;

public:		// User declarations
        __fastcall TLUTNameForm(TComponent* Owner);

        void SetDefaultName(const string &defaultName);
        string GetName();
};
//---------------------------------------------------------------------------
extern PACKAGE TLUTNameForm *LUTNameForm;
//---------------------------------------------------------------------------
#endif
