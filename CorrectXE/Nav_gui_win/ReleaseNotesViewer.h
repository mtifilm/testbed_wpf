//---------------------------------------------------------------------------

#ifndef ReleaseNotesViewerH
#define ReleaseNotesViewerH
//---------------------------------------------------------------------------

#include <windows.h>
#include <Shellapi.h>

#include <string>
using std::string;

//---------------------------------------------------------------------------


class ReleaseNotesViewer
{
public:
	int show();
};


#endif
