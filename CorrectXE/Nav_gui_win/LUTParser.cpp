#include "LUTParser.h"

#include "string.h"
#include "io.h"
#include "fcntl.h"
#include "stdlib.h"
#include "stdio.h"



CLUTParser::CLUTParser()
{
  memset(this->cpFilename, 0, 256 * sizeof(char));
}

CLUTParser::~CLUTParser()
{

}

const char * CLUTParser::getFilename() const
{
  return this->cpFilename;
}

int CLUTParser::getVersion() const
{
  return this->version;
}

unsigned short CLUTParser::getRefWhite(int index) const
{
  return this->refWhite[index];
}

unsigned short CLUTParser::getRefBlack(int index) const
{
  return this->refBlack[index];
}

unsigned short CLUTParser::getSoftclip(int index) const
{
  return this->softclip[index];
}

float CLUTParser::getGamma(int index) const
{
  return this->gamma[index];
}

float CLUTParser::getGammaCorrect(int index) const
{
  return this->gammaCorrect[index];
}

unsigned short CLUTParser::getHighlight(int index) const
{
  return this->highlight[index];
}

unsigned short CLUTParser::getShadow(int index) const
{
  return this->shadow[index];
}

void CLUTParser::setFilename(const char *cpTemp)
{
  strcpy(this->cpFilename, cpTemp);
}

void CLUTParser::setVersion(int version)
{
  this->version = version;
}

void CLUTParser::setRefWhite(const unsigned short *whites)
{
  memcpy(this->refWhite, whites, 3 * sizeof(short));
}

void CLUTParser::setRefBlack(const unsigned short *blacks)
{
  memcpy(this->refBlack,blacks, 3 * sizeof(short));
}

void CLUTParser::setSoftclip(const unsigned short *softs)
{
 memcpy(this->softclip, softs, 3 * sizeof(short));
}

void CLUTParser::setGamma(const float *gammas)
{
 memcpy(this->gamma, gammas, 3 * sizeof(float));
}

void CLUTParser::setGammaCorrect(const float *gammacor)
{
 memcpy(this->gammaCorrect, gammacor, 3 * sizeof(float));
}

void CLUTParser::setHighlight(const unsigned short *highs)
{
 memcpy(this->highlight, highs, 3 * sizeof(short));
}

void CLUTParser::setShadow(const unsigned short *shads)
{
 memcpy(this->shadow, shads, 3 * sizeof(short));
}

bool CLUTParser::isSupported() const
{
 return true;
}

const char * CLUTParser::getDescription() const
{
 return this->description;

}

void CLUTParser::setDescription(const char *cpTemp)
{
  if(cpTemp != NULL)
    strcpy(description, cpTemp);
    strcat(description, "\n");
}

bool CLUTParser::save()
{
 // writing only the header right now
 char cpTemp[256];

 char cpDesc[256] = "#   LUT to convert "; 
 strcat(cpDesc, description);
 char cpVersion[256] = "#   Version 3.0\n";
 char cpRefWhite[256] = "#   RGB Reference White: ";
 char cpRefBlack[256] = "#   RGB Reference Black: ";
 char cpSoftclip[256] = "#   RGB Softclip: ";
 char cpFilmGamma[256] = "#   RGB Film Gamma: ";
 char cpGammaCorrect[256] = "#   RGB Gamma Correction: ";
 char cpHighlight[256] = "#   RGB HighLight:  ";
 char cpShadow[256] = "#   RGB Shadow:    ";
 char cpDataMarker[256] = "LUT: 3 1024\n";

 sprintf(cpTemp, "%d %d %d\n", refWhite[0], refWhite[1], refWhite[2]);
 strcat(cpRefWhite, cpTemp);

 sprintf(cpTemp, "%d %d %d\n", refBlack[0], refBlack[1], refBlack[2]);
 strcat(cpRefBlack, cpTemp);

 sprintf(cpTemp, "%d %d %d\n", softclip[0], softclip[1], softclip[2]);
 strcat(cpSoftclip, cpTemp);

 sprintf(cpTemp, "%f %f %f\n", gamma[0], gamma[1], gamma[2]);
 strcat(cpFilmGamma, cpTemp);

 sprintf(cpTemp, "%f %f %f\n", gammaCorrect[0], gammaCorrect[1], gammaCorrect[2]);
 strcat(cpGammaCorrect, cpTemp);

 sprintf(cpTemp, "%d %d %d\n", shadow[0], shadow[1], shadow[2]);
 strcat(cpShadow, cpTemp);

 sprintf(cpTemp, "%d %d %d\n", highlight[0], highlight[1], highlight[2]);
 strcat(cpHighlight, cpTemp);




 int iFD = open (this->cpFilename, O_CREAT | O_RDWR | O_TEXT, 0644);

 if(iFD < 0)
 {
   strcpy(lastErrorMsg, "Could not save LUT file ");
   strcat(lastErrorMsg, this->cpFilename);
   return false;
 }

 write(iFD, "#\n", 2);
 write(iFD, cpDesc, strlen(cpDesc));
 write(iFD, cpVersion, strlen(cpVersion));
 write(iFD, cpRefWhite, strlen(cpRefWhite));
 write(iFD, cpRefBlack, strlen(cpRefBlack));
 write(iFD, cpSoftclip, strlen(cpSoftclip));
 write(iFD, cpFilmGamma, strlen(cpFilmGamma));
 write(iFD, cpGammaCorrect, strlen(cpGammaCorrect));
 write(iFD, cpHighlight, strlen(cpHighlight));
 write(iFD, cpShadow, strlen(cpShadow));
 write(iFD, "#\n", 2);
 write(iFD, cpDataMarker, strlen(cpDataMarker));

 for(int i=0; i < 3072; i++)
 {
    write(iFD, "\t0\n", 3);
 }

 close(iFD);

 strcpy(lastErrorMsg, "No error: saved LUT file ");
 strcat(lastErrorMsg, this->cpFilename);

 return true;
}

bool CLUTParser::load()
{
  return this->parse();
}

bool CLUTParser::isSaved() const
{
 return true;
}

bool CLUTParser::isLoaded() const
{
 return true;
}

bool CLUTParser::parse()
{
   char cpTemp[2048];
   char cpLine1[256];
   char cpLine2[256];
   char cpLine3[256];
   char cpLine4[256];
   char cpLine5[256];
   char cpLine6[256];
   char cpLine7[256];
   char cpLine8[256];
   char cpLine9[256];
   char cpLine10[256];
   char cpLine11[256];

   int currentLine = 0;

   char * cpCursor;
   char* token;
   bool bDone = false;

   int iFD = open (this->cpFilename, O_RDWR | O_TEXT, 0644);

   if(iFD == -1)
   {
     strcpy(lastErrorMsg, "Could not open LUT file ");
     strcat(lastErrorMsg, this->cpFilename);
     return false;
   }
   memset(cpTemp, 0, 2048);
   read(iFD, cpTemp, 2047);

   token = strtok(cpTemp, "\n");
   while(token != NULL && !bDone)
   {
      switch(currentLine)
      {

      case 0:
        strcpy(cpLine1, token);

        break;
      case 1:
        strcpy(cpLine2, token);

        break;
      case 2:
        strcpy(cpLine3, token);
       break;
      case 3:
        strcpy(cpLine4, token);
        break;
      case 4:
        strcpy(cpLine5, token);
        break;
      case 5:
        strcpy(cpLine6, token);

        break;
      case 6:
        strcpy(cpLine7, token);

        break;
      case 7:
        strcpy(cpLine8, token);

        break;
      case 8:
        strcpy(cpLine9, token);

        break;
      case 9:
        strcpy(cpLine10, token);

        break;
      case 10:
        strcpy(cpLine11, token);
        bDone = true;
        break;
      default:
        break;


      }
      token = strtok(NULL, "\n");
      currentLine++;

   }

   // line 1
   cpCursor = strstr(cpLine2, LUTHEADER_LINEDESC) + strlen(LUTHEADER_LINEDESC);
   strcpy(description, cpCursor);
   // check description, if not log-linear return false
   if(
     strstr(description, LUTDESC_LOGLINEAR) == NULL
     && strstr(description, LUTDESC_LINEARLINEAR) == NULL
     && strstr(description, LUTDESC_EXRLINEAR) == NULL
     && strstr(description, LUTDESC_SMPTELINEAR) == NULL
     && strstr(description, LUTDESC_CCIR709LINEAR) == NULL
     && strstr(description, LUTDESC_CCIR601BGLINEAR) == NULL
     && strstr(description, LUTDESC_CCIR601MLINEAR) == NULL
     )
     {
        strcpy(lastErrorMsg, "Error: Incompatible LUT description: ");
        strcat(lastErrorMsg, description);
        strcat(lastErrorMsg, " in file: ");        
        strcat(lastErrorMsg, this->cpFilename);
        return false;
     }

   // line 2
   cpCursor = strstr(cpLine3, LUTHEADER_LINEVERSION) + strlen(LUTHEADER_LINEVERSION);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->version = atoi(token);
   //line 3
   cpCursor = strstr(cpLine4, LUTHEADER_LINEWHITE) + strlen(LUTHEADER_LINEWHITE);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->refWhite[0] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->refWhite[1] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->refWhite[2] = atoi(token);
   // line 4
   cpCursor = strstr(cpLine5, LUTHEADER_LINEBLACK) + strlen(LUTHEADER_LINEBLACK);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->refBlack[0] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->refBlack[1] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->refBlack[2] = atoi(token);
   //line 5
   cpCursor = strstr(cpLine6, LUTHEADER_LINESOFTCLIP) + strlen(LUTHEADER_LINESOFTCLIP);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->softclip[0] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->softclip[1] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->softclip[2] = atoi(token);
   //line 6
   cpCursor = strstr(cpLine7, LUTHEADER_LIENGAMMA) + strlen(LUTHEADER_LIENGAMMA);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->gamma[0] = atof(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->gamma[1] = atof(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->gamma[2] = atof(token);
   // line 7
   cpCursor = strstr(cpLine8, LUTHEADER_LINEGAMMACORRECT) + strlen(LUTHEADER_LINEGAMMACORRECT);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->gammaCorrect[0] = atof(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->gammaCorrect[1] = atof(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->gammaCorrect[2] = atof(token);
   // line 8
   cpCursor = strstr(cpLine9, LUTHEADER_LINEHIGHLIGHT) + strlen(LUTHEADER_LINEHIGHLIGHT);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->highlight[0] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->highlight[1] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->highlight[2] = atoi(token);
   // line 9
   cpCursor = strstr(cpLine10, LUTHEADER_LINESHADOW) + strlen(LUTHEADER_LINESHADOW);
   token = strtok(cpCursor, " ");
   if(token != NULL)
     this->shadow[0] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->shadow[1] = atoi(token);

   token = strtok(NULL, " ");
   if(token != NULL)
     this->shadow[2] = atoi(token);


   close(iFD);

   strcpy(lastErrorMsg, "No error: Parsed LUT file ");
   strcat(lastErrorMsg, this->cpFilename);

   return true;
}

char * CLUTParser::readline(int iFD, char * cpLine)
{
  return NULL;
}

const char * CLUTParser::getLastErrorMsg() const
{
  return lastErrorMsg;
}
