object GpuDumpForm: TGpuDumpForm
  Left = 0
  Top = 0
  Caption = 'Gpu Info'
  ClientHeight = 562
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ControlPanel: TPanel
    Left = 0
    Top = 535
    Width = 844
    Height = 27
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      844
      27)
    object CloseButton: TButton
      Left = 799
      Top = 5
      Width = 45
      Height = 19
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
    end
  end
  object HeaderDumpMemo: TRichEdit
    Left = 0
    Top = 0
    Width = 844
    Height = 535
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    PlainText = True
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
    Zoom = 100
  end
end
