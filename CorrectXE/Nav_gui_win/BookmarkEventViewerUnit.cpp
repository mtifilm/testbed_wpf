// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BookmarkEventViewerUnit.h"

#include "BookmarkManager.h"
#include "JobManager.h"
#include "MainWindowUnit.h"
#include "MTIDialogs.h"
#include "NavigatorTool.h"
#include "Player.h"
#include "ToolManager.h"
#include "ToolCommand.h"
#include "ShowModalDialog.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma resource "*.dfm"
TBookmarkEventViewerForm *BookmarkEventViewerForm;

namespace
{
   String TodaysDate()
   {
      TFormatSettings formatSettings;
      formatSettings.DateSeparator    = '-';
      formatSettings.ShortDateFormat  = "yyyy/mm/dd";
      return DateToStr(Date(), formatSettings);
   }
};

// ---------------------------------------------------------------------------
__fastcall TBookmarkEventViewerForm::TBookmarkEventViewerForm(TComponent* Owner)
      : TMTIForm(Owner),
        updatingGui(false),
        needToSave(false),
        selectedBookmarkFrameIndex(-1)
{
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::FormCreate(TObject *Sender)
{
   SET_CBHOOK(OnBookmarkAdded, BookmarkManager::BookmarkAdded);
   SET_CBHOOK(OnBookmarkSelectionChange, BookmarkManager::BookmarkSelectionChange);
   SET_CBHOOK(OnSelectedBookmarkRangeEnteredOrExited, BookmarkManager::SelectedBookmarkRangeEnteredOrExited);

   GoToClipStartSpeedButton->Tag = NAV_CMD_GOTO_IN;
   GoToClipEndSpeedButton->Tag   = NAV_CMD_GOTO_OUT;
   JogBackButton->Tag            = NAV_CMD_JOG_REV_1_FRAME;
   JogForwardButton->Tag         = NAV_CMD_JOG_FWD_1_FRAME;
   PlayButton->Tag               = NAV_CMD_PLAY;
   PlayReverseButton->Tag        = NAV_CMD_PLAY_REV;
   FastForwardButton->Tag        = NAV_CMD_FAST_FWD;
   RewindButton->Tag             = NAV_CMD_REWIND;
   StopButton->Tag               = NAV_CMD_STOP;
   MarkEntireClipButton->Tag     = NAV_CMD_MARK_ALL;
   SetShotMarksButton->Tag       = NAV_CMD_MARK_SHOT;
   ClearMarksButton->Tag         = NAV_CMD_CLEAR_MARKS;
}

// ---------------------------------------------------------------------------
void __fastcall TBookmarkEventViewerForm::FormDestroy(TObject *Sender)
{
   REMOVE_CBHOOK(OnBookmarkAdded, BookmarkManager::BookmarkAdded);
   REMOVE_CBHOOK(OnBookmarkSelectionChange, BookmarkManager::BookmarkSelectionChange);
   REMOVE_CBHOOK(OnSelectedBookmarkRangeEnteredOrExited, BookmarkManager::SelectedBookmarkRangeEnteredOrExited);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::FormShow(TObject *Sender)
{
   // This both shows and selects the bookmark timeline
   ExecuteNavigatorToolCommand(NAV_CMD_SHOW_BOOKMARK_TIMELINE);

   string saveUser = bookmarkInfo.User;
   bookmarkInfo.Clear();
   bookmarkInfo.User = saveUser;

   int currentFrameIndex = mainWindow->GetCurrentFrameIndex();
   BookmarkManager bookmarkManager;
   if (bookmarkManager.DoesFrameHaveBookmarkInfo(currentFrameIndex))
   {
      // This will trigger a callback where we handle the GUI update.
      bookmarkManager.SelectBookmarkAt(currentFrameIndex);
   }
   else
   {
      bookmarkManager.SelectBookmarkAt(-1);
      UpdateGuiFromBookmarkInfo();
   }

   // This will monitor state of add and delete buttons
   ButtonStateTimer->Enabled = true;

   // Never want focus here unless we are actively editing a field.
   isDeactivateHackEnabled = true;
	Unfocus();

	TMTIForm::FormShow(Sender);
}

// ---------------------------------------------------------------------------
void __fastcall TBookmarkEventViewerForm::FormActivate(TObject *Sender)
{
   // Stupid hack to deactivate this window when it is first shown. I can't
   // seem to be able to do that directly from the OnShow handler because
   // we get activated after that is called.
   if (isDeactivateHackEnabled)
   {
      // probably not good to activate a different window from an OnActivate
      // so I'm just going to do it with a timer.
      DeactivateHackTimer->Enabled = true;
      return;
   }

   FocusControl(CloseButton);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::FormDeactivate(TObject *Sender)
{
   AutoSave();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::DeactivateHackTimerTimer(TObject *Sender)
{
   Unfocus();

   // One shot
   isDeactivateHackEnabled = false;
   DeactivateHackTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::Unfocus()
{
   if (Active)
   {
      mainWindow->FocusOnMainWindow();
   }
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::EnableControlsBasedOnCurrentState()
{
   BookmarkManager bookmarkManager;
   bool withinExistingBookmark  = bookmarkManager.IsCurrentTimecodeInSelectedBookmark();
   bool aBookmarkIsSelected     = bookmarkInfo.InTimecode != CTimecode::NOT_SET;
   bool userFieldIsFilledIn = !bookmarkInfo.User.empty();

   bool userLabelEnabled  = withinExistingBookmark || (aBookmarkIsSelected && !userFieldIsFilledIn);
   bool marksStuffEnabled = aBookmarkIsSelected && userFieldIsFilledIn;
   bool editFieldsEnabled = withinExistingBookmark && userFieldIsFilledIn;

   UserLabeledEdit->Enabled = userLabelEnabled;
   RequiredLabel->Visible   = userLabelEnabled && !userFieldIsFilledIn;

   MarkInSpeedButton->Enabled         = marksStuffEnabled;
   SetInMarkButton->Enabled           = marksStuffEnabled;
   GoToPrevEventSpeedButton->Enabled  = marksStuffEnabled;
   MarkOutSpeedButton->Enabled        = marksStuffEnabled;
   SetMarkOutInclusiveButton->Enabled = marksStuffEnabled;
   GoToNextEventSpeedButton->Enabled  = marksStuffEnabled;
   DurLabel->Enabled                  = marksStuffEnabled;
   DurationValueLabel->Enabled        = marksStuffEnabled;

   AddedLabel->Enabled                = editFieldsEnabled;
   AddedValueLabel->Enabled           = editFieldsEnabled;
   ReelNumberLabeledEdit->Enabled     = editFieldsEnabled;
   DefectComboBox->Enabled            = editFieldsEnabled;
   DefectLabel->Enabled               = editFieldsEnabled;
   LocationLabeledEdit->Enabled       = editFieldsEnabled;
   ArtistLabeledEdit->Enabled         = editFieldsEnabled;
   CompletedLabeledEdit->Enabled      = editFieldsEnabled;
   CompletedCheckButton->Enabled      = editFieldsEnabled;
   QcByLabeledEdit->Enabled           = editFieldsEnabled;
   QcDateLabeledEdit->Enabled         = editFieldsEnabled;
   QcDateCheckButton->Enabled         = editFieldsEnabled;
   NoteLabeledEdit->Enabled           = editFieldsEnabled;

   // Hack because disabled combo box looks like shit.
   DefectDisabledCoverEdit->Text = DefectComboBox->Text;
   DefectDisabledCoverEdit->Visible = !DefectComboBox->Enabled;
}
//---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::UserLabeledEditChange(TObject *Sender)
{
   if (updatingGui)
   {
      return;
   }

   if ((UserLabeledEdit->Text.IsEmpty() && !RequiredLabel->Visible)
   || (!UserLabeledEdit->Text.IsEmpty() && RequiredLabel->Visible))
   {
      bookmarkInfo.User = StringToStdString(UserLabeledEdit->Text);
      UpdateGuiFromBookmarkInfo();
      needToSave = true;
   }
   else
   {
      BookmarkItemChange(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::BookmarkItemChange(TObject *Sender)
{
   if (updatingGui)
   {
      return;
   }

   UpdateBookmarkInfoFromGui();
   needToSave = true;
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::AutoSave()
{
   if (!needToSave)
   {
      return;
   }

   Save();
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::Save()
{
   needToSave = false;

   if (selectedBookmarkFrameIndex < 0)
   {
      return;
   }

   BookmarkManager bookmarkManager;
   bookmarkManager.SaveBookmarkInfo(selectedBookmarkFrameIndex, bookmarkInfo);
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::UpdateGuiFromBookmarkInfo()
{
   if (!Visible)
   {
      return;
   }

   updatingGui = true;

   UserLabeledEdit->Text       = bookmarkInfo.User.c_str();
   AddedValueLabel->Caption    = bookmarkInfo.Date.c_str();

   EnableControlsBasedOnCurrentState();

// OLD way blanked disabled fields
//   ReelNumberLabeledEdit->Text = ReelNumberLabeledEdit->Enabled ? bookmarkInfo.ReelNumber.c_str() : "";
//   DefectComboBox->Text        = DefectComboBox->Enabled ? bookmarkInfo.DefectType.c_str() : "";
//   LocationLabeledEdit->Text   = LocationLabeledEdit->Enabled ? bookmarkInfo.DefectLocation.c_str() : "";
//   ArtistLabeledEdit->Text     = ArtistLabeledEdit->Enabled ? bookmarkInfo.Artist.c_str() : "";
//   CompletedLabeledEdit->Text  = CompletedLabeledEdit->Enabled ? bookmarkInfo.Completed.c_str() : "";
//   QcByLabeledEdit->Text       = QcByLabeledEdit->Enabled ? bookmarkInfo.QcBy.c_str() : "";
//   QcDateLabeledEdit->Text     = QcDateLabeledEdit->Enabled ? bookmarkInfo.QcDate.c_str() : "";
//   NoteLabeledEdit->Text       = NoteLabeledEdit->Enabled ? bookmarkInfo.Note.c_str() : "";
   ReelNumberLabeledEdit->Text = bookmarkInfo.ReelNumber.c_str();
   DefectComboBox->Text        = bookmarkInfo.DefectType.c_str();
   LocationLabeledEdit->Text   = bookmarkInfo.DefectLocation.c_str();
   ArtistLabeledEdit->Text     = bookmarkInfo.Artist.c_str();
   CompletedLabeledEdit->Text  = bookmarkInfo.Completed.c_str();
   QcByLabeledEdit->Text       = bookmarkInfo.QcBy.c_str();
   QcDateLabeledEdit->Text     = bookmarkInfo.QcDate.c_str();
   NoteLabeledEdit->Text       = bookmarkInfo.Note.c_str();

   MarkInSpeedButton->Caption = (bookmarkInfo.InTimecode == CTimecode::NOT_SET)
                                ? "NOT SET"
                                : StringTrim(PTimecode(bookmarkInfo.InTimecode)).c_str();

   MarkOutSpeedButton->Caption = (bookmarkInfo.InTimecode == CTimecode::NOT_SET
                                 || bookmarkInfo.OutTimecode == CTimecode::NOT_SET)
                                 ? "NOT SET"
                                 : StringTrim(PTimecode(bookmarkInfo.OutTimecode)).c_str();

   int durationInFrames = 0;
   if (bookmarkInfo.InTimecode != CTimecode::NOT_SET && bookmarkInfo.OutTimecode != CTimecode::NOT_SET)
   {
      durationInFrames = (bookmarkInfo.OutTimecode - bookmarkInfo.InTimecode) + 1;
   }

   if (durationInFrames == 0)
   {
      DurationValueLabel->Caption = "";
   }
   else
   {
      CTimecode durationTimecode = bookmarkInfo.InTimecode;
      durationTimecode.setFrameCount(durationInFrames);
      DurationValueLabel->Caption = StringTrim((string)PTimecode(durationTimecode)).c_str();
   }

   updatingGui = false;
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::UpdateBookmarkInfoFromGui()
{
   bookmarkInfo.User           = StringToStdString(UserLabeledEdit->Text);
   bookmarkInfo.ReelNumber     = StringToStdString(ReelNumberLabeledEdit->Text);
   bookmarkInfo.DefectType     = StringToStdString(DefectComboBox->Text);
   bookmarkInfo.DefectLocation = StringToStdString(LocationLabeledEdit->Text);
   bookmarkInfo.Artist         = StringToStdString(ArtistLabeledEdit->Text);
   bookmarkInfo.Completed      = StringToStdString(CompletedLabeledEdit->Text);
   bookmarkInfo.QcBy           = StringToStdString(QcByLabeledEdit->Text);
   bookmarkInfo.QcDate         = StringToStdString(QcDateLabeledEdit->Text);
   bookmarkInfo.Note           = StringToStdString(NoteLabeledEdit->Text);

// ??? These aren't editable!
//   string temp             = StringToStdString(MarkInSpeedButton->Caption);
//   bookmarkInfo.InTimecode = (temp.empty() || temp == "NOT SET") ? CTimecode::NOT_SET : PTimecode(temp);
//
//   temp                     = StringToStdString(MarkOutSpeedButton->Caption);
//   bookmarkInfo.OutTimecode = (temp.empty() || temp == "NOT SET") ? CTimecode::NOT_SET : PTimecode(temp);

   if (!bookmarkInfo.User.empty())
   {
      defaultUser = bookmarkInfo.User;
   }

   if (!bookmarkInfo.DefectType.empty())
   {
      defaultDefect = bookmarkInfo.DefectType;
   }

   if (!bookmarkInfo.ReelNumber.empty())
   {
         defaultReelNumber = bookmarkInfo.ReelNumber;
   }
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::OnBookmarkAdded(void *Sender)
{
   if (!Visible)
   {
      return;
   }

   bool moveFocusToUserEdit       = false;
   bool moveFocusToDefectComboBox = false;

   if (defaultUser.empty())
   {
      moveFocusToUserEdit = true;
   }
   else
   {
      bookmarkInfo.User         = defaultUser;
      moveFocusToDefectComboBox = true;
      if (bookmarkInfo.DefectType.empty())
      {
         bookmarkInfo.DefectType = defaultDefect;
      }
   }

   if (defaultReelNumber.empty())
   {
      JobManager jobManager;
      defaultReelNumber = jobManager.GetReelName();
   }

   if (bookmarkInfo.ReelNumber.empty())
   {
      bookmarkInfo.ReelNumber = defaultReelNumber;
   }

   UpdateGuiFromBookmarkInfo();

   if (moveFocusToUserEdit)
   {
      updatingGui              = true;
      UserLabeledEdit->Enabled = true;
      FocusControl(UserLabeledEdit);
      UserLabeledEdit->SelectAll();
      updatingGui = false;
   }
   else if (moveFocusToDefectComboBox)
   {
      updatingGui             = true;
      DefectComboBox->Enabled = true;
      FocusControl(DefectComboBox);
      DefectComboBox->DroppedDown = true;
      DefectComboBox->SelectAll();
      updatingGui = false;
   }
   else
   {
      FocusControl(CloseButton);
   }

   needToSave = true;
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::OnBookmarkSelectionChange(void *Sender)
{
   AutoSave();

   BookmarkManager bookmarkManager;
   selectedBookmarkFrameIndex = bookmarkManager.GetSelectedBookmarkFrameIndex();
   if (selectedBookmarkFrameIndex < 0)
   {
      bookmarkInfo.Clear();
   }
   else
   {
      int retVal = bookmarkManager.GetBookmarkInfo(selectedBookmarkFrameIndex, bookmarkInfo);
      if (retVal != 0)
      {
         selectedBookmarkFrameIndex = -1;
         bookmarkInfo.Clear();
      }
   }

   UpdateGuiFromBookmarkInfo();
   Unfocus();
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::OnSelectedBookmarkRangeEnteredOrExited(void *Sender)
{
   UpdateGuiFromBookmarkInfo();
}
// ---------------------------------------------------------------------------

void TBookmarkEventViewerForm::ExecuteNavigatorToolCommand(int command)
{
   // This function passes a command to the Navigator tool, eventually
   // making its way to the CNavigatorTool's onToolCommand function

   // Ask the Tool Manager for the Navigator tool
   CToolManager toolManager;
   string toolName = "Navigator"; // TTT This shouldn't be hard-coded
   int toolIndex   = toolManager.findTool(toolName);
   if (toolIndex < 0)
      return;

   CToolObject *toolObj = toolManager.GetTool(toolIndex);
   if (toolObj == 0)
      return;

   // Create a Tool Command object and execute the caller's command
   CToolCommand toolCommand(toolObj, command);
   toolCommand.execute();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::NavBarSpeedButtonClick(TObject *Sender)
{
   AutoSave();

   TControl *control = dynamic_cast<TControl*>(Sender);
   if (control == NULL)
   {
      return;
   }

   Unfocus();
   ExecuteNavigatorToolCommand(control->Tag);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::SetInMarkButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();

   int currentFrameIndex = mainWindow->GetCurrentFrameIndex();
   if (currentFrameIndex == selectedBookmarkFrameIndex || MTIWarningDialog("Are you sure you want to move this bookmark?")
      == MTI_DLG_CANCEL)
   {
      return;
   }

   bookmarkInfo.InTimecode = mainWindow->GetCurrentlyDisplayedTimecode();

   // Sanity
   if (bookmarkInfo.OutTimecode == CTimecode::NOT_SET || bookmarkInfo.OutTimecode < bookmarkInfo.InTimecode)
   {
      bookmarkInfo.OutTimecode = bookmarkInfo.InTimecode;
   }

   BookmarkInfo newBookmarkInfo = bookmarkInfo;

   BookmarkManager bookmarkManager;
   bookmarkManager.DeleteBookmark(selectedBookmarkFrameIndex);

   ExecuteNavigatorToolCommand(NAV_CMD_ADD_BOOKMARK);

   bookmarkInfo = newBookmarkInfo;
   Save();

   // After Save()!
   UpdateGuiFromBookmarkInfo();

   //// implement this!
   ////ExecuteNavigatorToolCommand(NAV_CMD_REFRESH_TIMELINES);
   mainWindow->GetTimeline()->performCutAction(CUT_REFRESH);
   Unfocus();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::SetMarkOutInclusiveButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();

   if (bookmarkInfo.InTimecode == CTimecode::NOT_SET)
   {
      MTIErrorDialog("ERROR: Please select a bookmark to set duration on!");
      return;
   }

   CTimecode currentTimecode = mainWindow->GetCurrentlyDisplayedTimecode();
   if (currentTimecode == bookmarkInfo.OutTimecode)
   {
      // No-op
      return;
   }

   if (currentTimecode < bookmarkInfo.InTimecode)
   {
      MTIErrorDialog("ERROR: OUT timecode cannot be before the IN timecode!");
      return;
   }

   // All swell!
   bookmarkInfo.OutTimecode = currentTimecode;

   Save();

   // AFTER Save()!
   UpdateGuiFromBookmarkInfo();

   //// implement this!
   ////ExecuteNavigatorToolCommand(NAV_CMD_REFRESH_TIMELINES);
   mainWindow->GetTimeline()->performCutAction(CUT_REFRESH);
   Unfocus();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::GoToInSpeedButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();

   if (bookmarkInfo.InTimecode == CTimecode::NOT_SET)
   {
      return;
   }

   mainWindow->GetPlayer()->goToFrameSynchronous(bookmarkInfo.InTimecode);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::GoToOutSpeedButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();

   if (bookmarkInfo.OutTimecode == CTimecode::NOT_SET)
   {
      return;
   }

   mainWindow->GetPlayer()->goToFrameSynchronous(bookmarkInfo.OutTimecode);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::AddButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();
   ExecuteNavigatorToolCommand(NAV_CMD_ADD_BOOKMARK);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::DeleteButtonClick(TObject *Sender)
{
   AutoSave();

   Unfocus();
   ExecuteNavigatorToolCommand(NAV_CMD_DROP_BOOKMARK);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::CloseButtonClick(TObject *Sender)
{
   AutoSave();

   Visible = false;
}

// ---------------------------------------------------------------------------
void __fastcall TBookmarkEventViewerForm::FormClick(TObject *Sender)
{
   Unfocus();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::FormHide(TObject *Sender)
{
   AutoSave();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::OnEditEnter(TObject *Sender)
{
   TCustomEdit *edit = dynamic_cast<TCustomEdit*>(Sender);
   if (edit == NULL)
   {
      return;
   }

   edit->SelectAll();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::OnEditKeyPress(TObject *Sender, System::WideChar &Key)
{
   if (Key == '\r')
   {
      // ENTER was pressed
      Unfocus();
      Key = 0;
   }

   // QQQ Should handle ESC to cancel edit!
}
// ---------------------------------------------------------------------------


void __fastcall TBookmarkEventViewerForm::ButtonStateTimerTimer(TObject *Sender)
{
   if (!Visible)
   {
      ButtonStateTimer->Enabled = false;
   }

   int currentFrameIndex = mainWindow->GetCurrentFrameIndex();
   BookmarkManager bookmarkManager;
   bool frameHasBookmarkInfo = bookmarkManager.DoesFrameHaveBookmarkInfo(currentFrameIndex);
   AddButton->Enabled        = !frameHasBookmarkInfo;
   DeleteButton->Enabled     = frameHasBookmarkInfo;

   CompletedCheckButton->Enabled = CompletedLabeledEdit->Enabled && CompletedLabeledEdit->Text.IsEmpty();
   QcDateCheckButton->Enabled    = QcDateLabeledEdit->Enabled && QcDateLabeledEdit->Text.IsEmpty();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::CompletedCheckButtonClick(TObject *Sender)
{
   CompletedLabeledEdit->Text = TodaysDate();
   needToSave = true;
   Unfocus();
}
// ---------------------------------------------------------------------------
void __fastcall TBookmarkEventViewerForm::QcDateCheckButtonClick(TObject *Sender)

{
   QcDateLabeledEdit->Text = TodaysDate();
   needToSave = true;
   Unfocus();
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::DefectComboBoxKeyPress(TObject *Sender, System::WideChar &Key)
{
   if (Key == '\r')
   {
      // ENTER was pressed
      Unfocus();
      Key = 0;
   }
   else if (Key == '\t')
   {
      // Tab
      FocusControl(LocationLabeledEdit);
      Key = 0;
   }
   else if (Key == 0x1B)
   {
      // Esc
      Unfocus();
      Key = 0;
      DefectComboBox->Text = bookmarkInfo.DefectType.c_str();
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::GoToPrevEventSpeedButtonClick(TObject *Sender)
{
   AutoSave();

   ExecuteNavigatorToolCommand(NAV_CMD_SHOW_BOOKMARK_TIMELINE);
   ExecuteNavigatorToolCommand(NAV_CMD_PREV_EVENT);
}
// ---------------------------------------------------------------------------

void __fastcall TBookmarkEventViewerForm::GoToNextEventSpeedButtonClick(TObject *Sender)
{
   AutoSave();

   ExecuteNavigatorToolCommand(NAV_CMD_SHOW_BOOKMARK_TIMELINE);
   ExecuteNavigatorToolCommand(NAV_CMD_NEXT_EVENT);
}
//---------------------------------------------------------------------------

