object LicensingForm: TLicensingForm
  Left = 0
  Top = 0
  Caption = 'Licensing'
  ClientHeight = 500
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 243
    Height = 20
    Caption = 'DRS NOVA License Information'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 156
    Width = 77
    Height = 17
    Caption = 'LICENSE KEY:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 108
    Width = 41
    Height = 17
    Caption = 'VALID: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ValidLabel: TLabel
    Left = 150
    Top = 108
    Width = 20
    Height = 17
    Caption = 'NO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object LicenseKeyLabel: TLabel
    Left = 150
    Top = 156
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 24
    Top = 180
    Width = 112
    Height = 17
    Caption = 'COMPUTER NAME:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ComputerNameLabel: TLabel
    Left = 150
    Top = 180
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 24
    Top = 84
    Width = 56
    Height = 17
    Caption = 'VERSION:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object VersionLabel: TLabel
    Left = 150
    Top = 84
    Width = 27
    Height = 17
    Caption = '0.0.0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 24
    Top = 60
    Width = 61
    Height = 17
    Caption = 'PRODUCT:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ProductLabel: TLabel
    Left = 150
    Top = 60
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ComputerIdLabel: TLabel
    Left = 150
    Top = 204
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 24
    Top = 204
    Width = 87
    Height = 17
    Caption = 'COMPUTER ID:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 24
    Top = 228
    Width = 65
    Height = 17
    Caption = 'COMPANY:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object CompanyLabel: TLabel
    Left = 150
    Top = 228
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 24
    Top = 252
    Width = 34
    Height = 17
    Caption = 'USER:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object UserLabel: TLabel
    Left = 150
    Top = 252
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 24
    Top = 276
    Width = 50
    Height = 17
    Caption = 'EXPIRES:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ExpiresLabel: TLabel
    Left = 150
    Top = 276
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 24
    Top = 300
    Width = 111
    Height = 17
    Caption = 'UPGRADES EXPIRE:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object UpgradesExpireLabel: TLabel
    Left = 150
    Top = 300
    Width = 136
    Height = 17
    Caption = 'not available from C++'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 24
    Top = 324
    Width = 62
    Height = 17
    Caption = 'FEATURES:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 24
    Top = 132
    Width = 101
    Height = 17
    Caption = 'ACTIVATION KEY:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object ActivationKeyLabel: TLabel
    Left = 150
    Top = 132
    Width = 19
    Height = 17
    Caption = 'n/a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object FeaturesListBox: TListBox
    Left = 150
    Top = 324
    Width = 163
    Height = 121
    ItemHeight = 13
    TabOrder = 0
  end
  object OkButton: TButton
    Left = 178
    Top = 457
    Width = 90
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 1
  end
end
