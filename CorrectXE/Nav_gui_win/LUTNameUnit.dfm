object LUTNameForm: TLUTNameForm
  Left = 945
  Top = 444
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Enter LUT Name'
  ClientHeight = 103
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 21
    Width = 63
    Height = 21
    AutoSize = False
    Caption = '   LUT Name:'
    Transparent = True
    Layout = tlCenter
  end
  object LUTNameEdit: TEdit
    Left = 76
    Top = 18
    Width = 181
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnKeyDown = FormKeyDown
  end
  object OkButton: TButton
    Left = 61
    Top = 64
    Width = 74
    Height = 21
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 143
    Top = 64
    Width = 75
    Height = 21
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
