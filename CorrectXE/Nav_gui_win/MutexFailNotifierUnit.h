//---------------------------------------------------------------------------

#ifndef MutexFailNotifierUnitH
#define MutexFailNotifierUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include "ColorPanel.h"
//---------------------------------------------------------------------------
class TMutexFailNotifierForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TButton *CloseButton;
	TImage *Image1;
	TColorPanel *BorderPanel;
	TColorPanel *BackgroundPanel;
	void __fastcall CloseButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMutexFailNotifierForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMutexFailNotifierForm *MutexFailNotifierForm;
//---------------------------------------------------------------------------
#endif
