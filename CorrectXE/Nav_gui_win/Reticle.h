//---------------------------------------------------------------------------
#ifndef CRETICLE_H
#define CRETICLE_H
//---------------------------------------------------------------------------
#include "machine.h"

#define RETICLE_TYPE_NONE         0
#define RETICLE_TYPE_133_FULL     1
#define RETICLE_TYPE_166_FULL     2
#define RETICLE_TYPE_178_FULL     3
#define RETICLE_TYPE_185_FULL     4
#define RETICLE_TYPE_235_FULL     5
#define RETICLE_TYPE_240_FULL     6
#define RETICLE_TYPE_133_ACAD     7
#define RETICLE_TYPE_166_ACAD     8
#define RETICLE_TYPE_178_ACAD     9
#define RETICLE_TYPE_185_ACAD    10
#define RETICLE_TYPE_235_ACAD    11
#define RETICLE_TYPE_240_ACAD    12
#define HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX RETICLE_TYPE_240_ACAD

// THIS ONE MUST BE THE HIGHEST NUMBERED INDEX!
#define RETICLE_TYPE_CUSTOM      13

#define RETICLE_APERTURE_FULL     0
#define RETICLE_APERTURE_ACAD     1

class CIniFile;

class CReticle
{
public:

   CReticle();
   ~CReticle();

   // accessors
   string getFilename();
   int getType();
   double getAspectRatio();
   int getAperture();
   int getXOffset();
   int getYOffset();
   int getHScale();
   int getVScale();
   RECT getPrimaryRect();
   RECT getSafeActionRect();
   RECT getSafeTitleRect();

   // mutators
   void setFilename(const string&);
   void setType(int);
   void setAspectRatio(double);
   void setAperture(int);
   void setXOffset(int);
   void setYOffset(int);
   void setHScale(int);
   void setVScale(int);
   int setFrameRect(const RECT&, double);

   // equivalence operator
   friend bool operator==(const CReticle&, const CReticle&);

   // conform reticle filename
   static int conformFilename(string&);

   // delete reticle file in $(CPMP_DIR)\\reticles
   static int deleteReticleFile(const string& filename);

   // file R/W
   int readFromFile(const string& filename);
   int writeToFile(const string& filename);

private:

   int type;

   string reticleFile;

   double aspectRatio;

   int aperture;

   int xOffset,
       yOffset;

   int   hScale,
         vScale;

   RECT frameRect;
   int frmWdth;
   int frmHght;

   RECT primary;
   RECT safeAction;
   RECT safeTitle;

   CIniFile *reticleIniFile;

   static string sectionName;
   static string reticleDirectory;
   static string reticleExtension;
   static string reticleUnnamed;
   static string reticleTypeKey;
   static string reticleAspectRatioKey;
   static string reticleApertureKey;
   static string reticleXOffsetKey;
   static string reticleYOffsetKey;
   static string reticleHScaleKey;
   static string reticleVScaleKey;
};

#endif
