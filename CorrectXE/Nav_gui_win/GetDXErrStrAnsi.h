//###########################################################################
//###                                                                     ###
//###   GetDXErrStr - Char version                                        ###
//###                                                                     ###
//###       Function to retrieve textual descriptions of DirectX          ###
//###       return values. This function supports all DirectX APIs.       ###
//###       The GetErrStrGen tool was used to create this function.       ###
//###       The tool is available at                                      ###
//###           http://www.crosswinds.net/~foetsch/geterrstrgen           ###
//###                                                                     ###
//###       Usage:                                                        ###
//###           Params:                                                   ###
//###               RetVal: HRESULT return value of DirectX method        ###
//###               Buf: Buffer to receive the name of the identifier.    ###
//###                   Must be big enough to hold the longest str!       ###
//###               AddStr: Pointer to array of char to be filled         ###
//###                   with the textual description of the error         ###
//###                   Can be NULL.                                      ###
//###               AddStrSize: Size of the AddStr buffer. Assumes 1024   ###
//###                   if not specified                                  ###
//###           Returns:                                                  ###
//###               Pointer Buf                                           ###
//###                                                                     ###
//###       Copyright � 2000 Michael F�tsch.                              ###
//###                                                                     ###
//###########################################################################

//---------------------------------------------------------------------------
#ifndef GetDXErrStrCharH
#define GetDXErrStrCharH
//---------------------------------------------------------------------------
AnsiString GetDXErrStr(HRESULT RetVal, AnsiString *AddStr);

//char* GetDXErrStr(HRESULT RetVal, char *Buf, char *AddStr,
//    unsigned AddStrSize);

#endif

 