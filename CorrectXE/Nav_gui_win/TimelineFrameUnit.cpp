// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TimelineFrameUnit.h"

#include "BookmarkEventViewerUnit.h"
#include "BookmarkManager.h"
#include "Displayer.h"  // Ugggh, for toolbar defines
#include "GoToFrameUnit.h"
#include "IniFile.h"
#include "MainWindowUnit.h"
#include "MaskTool.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "MTITrackBar.h"
#include "NavigatorTool.h"
#include "NavMainCommon.h"
#include "Player.h"
#include "ToolManager.h"
#include "ToolCommand.h"
#include "TimelineTrackBar.h"
#include "ShowModalDialog.h"

#include "stdio.h"
#include <Clipbrd.hpp>
#include <cmath>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma link "ColorPanel"
#pragma link "MasterTimeLineFrame"
#pragma link "MTITrackBar"
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"

enum ESelectTimecode
{
	estcNONE, estcIN, estcOUT, estcCURRENT, estcLEFT, estcRIGHT
};

TTimelineFrame *TimelineFrame;
// ---------------------------------------------------------------------------

namespace
{

	CVideoFrame *GetVideoFrame(int Frame)
	{
		if (Frame < 0)
		{
			return NULL;
		}
		CNavCurrentClip *cc = mainWindow->GetCurrentClipHandler();
		if (cc == NULL)
		{
			return NULL;
		}

		CVideoFrameList *vfl = cc->getCurrentVideoFrameList();
		if (vfl == NULL)
		{
			return NULL;
		}

		return vfl->getFrame(Frame);
	}

	void ExecuteNavigatorToolCommand(int command)
	{
		// This function passes a command to the Navigator tool, eventually
		// making its way to the CNavigatorTool's onToolCommand function

		// Ask the Tool Manager for the Navigator tool
		CToolManager toolManager;
		string toolName = "Navigator"; // TTT This shouldn't be hard-coded
		int toolIndex = toolManager.findTool(toolName);
		if (toolIndex < 0)
		{
			return;
		}

		CToolObject *toolObj = toolManager.GetTool(toolIndex);
		if (toolObj == 0)
		{
			return;
		}

		// Create a Tool Command object and execute the caller's command
		CToolCommand toolCommand(toolObj, command);
		toolCommand.execute();
	}

} // end local namespace

// -------------------OnMenuEventChange---------Mike Braca----May 2006----

void __fastcall TTimelineFrame::MTLMenuEventChange(TObject *Sender, EMenuEventAction Action, MTI_UINT64 &EventFlag, int &Frame,
		bool &Continue)

		// A little callback routine to make sure that both AUTO amd manually
		// ADDed cuts are deleted (bug 2311)
		// Also now handles confirmation for trying to replace a bookmark.
		//
		// ****************************************************************************
{
	Continue = true;

	// By default the CUT menu item only deletes the "default" event
	// from the frame. But we want to delete both types of added cuts.
	if (Action == EVENT_ACTION_DELETE && EventFlag == EVENT_CUT_ADD)
	{
		EventFlag = EVENT_CUT_MASK;
	}
	else if (Action == EVENT_ACTION_ADD && EventFlag == EVENT_BOOKMARK)
	{
		// Larry says to show and select the bookmark timeline when you
		// add a bookmark and the bookmark info viewer window is open.
		// TODO: Add a call to set mode instead of peeking at the window state
		if (BookmarkEventViewerForm != NULL && BookmarkEventViewerForm->Visible)
		{
			ShowBookmarkTimeline(true);
		}

		BookmarkManager bookmarkManager;
		if (bookmarkManager.DoesFrameHaveBookmarkInfo(Frame))
		{
			MTIostringstream os;
			os << "A bookmark has been saved to this position." << endl << "Do you want to replace it?";
			if (MTIConfirmationDialog(os.str()) == MTI_DLG_OK)
			{
				bookmarkManager.DeleteBookmark(Frame);
			}
			else
			{
				Continue = false;
			}
		}
	}
	else if (EventFlag == EVENT_KEYFRAME)
	{
		if (Action == EVENT_ACTION_ADD)
		{
			CNavigatorTool::GetMaskTool()->SetKeyframe(Frame);
		}
		else if (Action == EVENT_ACTION_DELETE)
		{
			CNavigatorTool::GetMaskTool()->DeleteKeyframe(mainWindow->GetPlayer()->getLastFrameIndex());
		}

		Continue = false;
	}
}

// ---------------------------------------------------------------------------
__fastcall TTimelineFrame::TTimelineFrame(TComponent* Owner) : TFrame(Owner)
{
	// In true components, these would be set in the object inspector
	MTL->SetChangeEvent(MTLTrackBarChange);
	MTL->TrackBarAtTop(true);
	MTL->Ch1MenuItem->Checked = false;
	MTL->Ch2MenuItem->Checked = false;
	MTL->Ch3MenuItem->Checked = false;
	MTL->Ch4MenuItem->Checked = false;
	MTL->Ch5MenuItem->Checked = false;
	MTL->Ch6MenuItem->Checked = false;
	MTL->Channel62->Checked = false;
	MTL->Ch8MenuItem->Checked = false;
	MTL->HandlesMenuItem->Checked = false;
	MTL->Cuts1->Checked = false;
	MTL->DrawCenterLine(false);
	MTL->OnMenuEventChange = &MTLMenuEventChange;

	// Set the tags
	timelineInTCLabel->Tag = estcIN;
	timelineOutTCLabel->Tag = estcOUT;
	timelineCurrentTCLabel->Tag = estcCURRENT;
	timelineInTCLabel->Caption = "";
	timelineOutTCLabel->Caption = "";
	timelineCurrentTCLabel->Caption = "00:00:00:00";
//	MarkedRangeFrameCountReadoutLabel->Caption = "0";
//	MarkedRangeFrameCountReadoutLabel->ControlStyle << csOpaque;
	ToolbarPanel->Tag = estcNONE;

	_bTCUpdate = false;
	_bMarkUpdate = false;
	_bFrameCacheUpdate = false;
	_OldTop = -1;

	jumpString = "1";
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UserPreferredTimecodeStringStyleChanged(void *Sender)
{
   UpdateCurTimecode();
   UpdateInTimecode();
   UpdateOutTimecode();
   UpdateMarks();
   FrameResize(nullptr);
}

void TTimelineFrame::UpdateInTimecode()
{
	PTimecode timecode(mainWindow->GetPlayer()->getInTimecode());
   string timecodeString = timecode.getUserPreferredStyleString();
	timelineInTCLabel->Caption = AnsiString(timecodeString.c_str()).Trim();
	ClipInTCButton->Caption = AnsiString(timecodeString.c_str()).Trim();
	UpdateMarkOutButton();
}

void TTimelineFrame::UpdateOutTimecode()
{
	PTimecode timecode(mainWindow->GetPlayer()->getOutTimecode());
   string timecodeString = timecode.getUserPreferredStyleString();
	timelineOutTCLabel->Caption = AnsiString(timecodeString.c_str()).Trim();
	ClipOutTCButton->Caption = AnsiString(timecodeString.c_str()).Trim();
	UpdateMarkOutButton();
}

void TTimelineFrame::UpdateCurTimecode()
{
	////TRACE_3(errout << "<x>x<x>x<x>x<x> Enter TTimelineFrame::UpdateCurTimecode <x>x<x>x<x>x<x>");
	PTimecode &timecode = mainWindow->GetPlayer()->CurrentTC;
   string timecodeString = timecode.getUserPreferredStyleString();
	timelineCurrentTCLabel->Caption = timecodeString.c_str();   // no Trim()!
	timelineCurrentTCLabel->Caption = timelineCurrentTCLabel->Caption.Trim();
	UpdateMarkOutButton();
	////TRACE_3(errout << "<x>x<x>x<x>x<x> Before timelineCurrentTCLabel->Repaint() <x>x<x>x<x>x<x>");
	timelineCurrentTCLabel->Repaint();
	////TRACE_3(errout << "<x>x<x>x<x>x<x> Exit TTimelineFrame::UpdateCurTimecode <x>x<x>x<x>x<x>");
}

//void TTimelineFrame::UpdateFrameIndexCrap()
//{
//	int iInIndex(mainWindow->GetPlayer()->getInFrameIndex());
//	int iOutIndex(mainWindow->GetPlayer()->getOutFrameIndex());
//	int iCurrentIndex(mainWindow->GetPlayer()->getLastFrameIndex());
//	int iInMarkIndex(mainWindow->GetPlayer()->getMarkIn());
//	int iOutMarkIndex(mainWindow->GetPlayer()->getMarkOut());
//	MTIostringstream os;
//
//	//// os << (iCurrentIndex - iInIndex + 1) << " / " <<  (iOutIndex - iInIndex + 1);
//	if (iInMarkIndex >= 0 && iOutMarkIndex >= 0)
//	{
//		os << (iOutMarkIndex - iInMarkIndex);
//	   MarkedRangeFrameCountReadoutLabel->Caption = os.str().c_str();
//		MarkedRangeColonLabel->Visible = true;
//		MarkedRangeFrameCountReadoutLabel->Visible = true;
//	}
//	else
//	{
//		MarkedRangeColonLabel->Visible = false;
//		MarkedRangeFrameCountReadoutLabel->Visible = false;
//	}
//
//	// Call this to force immediate update of the graphic.
//	// Takes care of problem with Paint frame select cycle.
//	MarkOutAndFrameCountPanel->Repaint();
//}

void TTimelineFrame::UpdateMarkOutButton()
{
	int iInIndex(mainWindow->GetPlayer()->getInFrameIndex());
	int iOutIndex(mainWindow->GetPlayer()->getOutFrameIndex());
	int iCurrentIndex(mainWindow->GetPlayer()->getLastFrameIndex());
	int iInMarkIndex(mainWindow->GetPlayer()->getMarkIn());
	int iOutMarkIndex(mainWindow->GetPlayer()->getMarkOut());
	PTimecode markOutTC(mainWindow->GetPlayer()->getMarkOutTimecode());
   MTIostringstream os;
   if (markOutTC == CTimecode::NOT_SET)
   {
      os << "Not set";
   }
   else
   {
      os << AnsiString(markOutTC.getUserPreferredStyleString().c_str()).Trim().c_str();
      if (iInMarkIndex >= 0 && iOutMarkIndex >= iInMarkIndex)
      {
         auto markedFramesCount = iOutMarkIndex - iInMarkIndex;
         os << string((markedFramesCount > 9999) ? ":" : " : ") << markedFramesCount;
      }
   }

	MarkOutTCButton->Caption = AnsiString(os.str().c_str());

   // Timecode readout ribbon widths may need to change!
   ResizeTimecodeToolbar();

	// Call this to force immediate update of the graphic.
	// Takes care of problem with Paint frame select cycle.
   // QQQ Is this really needed?
	MarkOutTCButton->Repaint();
}

void TTimelineFrame::UpdateTimelineLimits()
{
   // CAREFUL!!!! In frickin' Player world, OUT FRAME IS INCLUSIVE!!!
   // QQQ why not get the bounds from the video frame list instead?
   int inFrameIndex = mainWindow->GetPlayer()->getInFrameIndex();
   int outFrameIndex = mainWindow->GetPlayer()->getOutFrameIndex() + 1;

	_bInhibit = true;
	MTL->Min(inFrameIndex);
	MTL->Max(outFrameIndex);   // Really max or is it out? QQQ
	_bInhibit = false;
	_bMarkUpdate = true;
}

void TTimelineFrame::UpdateTimelineSlider()
{
   // ???
	_bTCUpdate = false;

	if (!mainWindow->GetPlayer()->trackBarUpdateEnabled())
	{
      return;
   }

	int frameIndex =  mainWindow->GetPlayer()->getLastFrameIndex();
	if (frameIndex == -1)
	{
		return;
	}

   MTL->CurrentFrame(frameIndex);
}

// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateMarks()
{
	// Set the visibility and position of the timeline's in and out marks
	// based on the Player's current in and out marks

	// mbraca 20041130 - moved marks to trackbar
	// SetMarkPosition(mainWindow->GetPlayer()->getMarkIn(), InMarkImage);
	// SetMarkPosition(mainWindow->GetPlayer()->getMarkOut(), OutMarkImage);

	// Rewrote the rest of this method to fix bug 2330
	MTL->TrackBar->MarkInFrame = mainWindow->GetPlayer()->getMarkIn();
	MTL->TrackBar->MarkOutFrame = mainWindow->GetPlayer()->getMarkOut();

	// This is f*cked up, but I don't know how else to get at the
	// so-called "common data". Why isn't it in the class?
	CTimeLineDefs *timeLineDefs = MTL->getLine("Scene breaks", false);
	if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
	{
		CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
		if (commonData != NULL)
		{
			commonData->MarkIn(mainWindow->GetPlayer()->getMarkIn());
			commonData->MarkOut(mainWindow->GetPlayer()->getMarkOut());
		}
	}

	PTimecode markInTC(mainWindow->GetPlayer()->getMarkInTimecode());
	MarkInTCButton->Caption = (markInTC == CTimecode::NOT_SET) ? "Not set" : AnsiString(markInTC.getUserPreferredStyleString().c_str()).Trim();
   UpdateMarkOutButton();
}

void TTimelineFrame::SetMarkPosition(int markFrame, TImage *markImage)
{
	// Do nothing on mark
	markImage->Visible = (markFrame >= 0);
	if (markFrame < 0)
	{
		return;
	}

	// Find hint timecode
	CVideoFrame *FramePtr = GetVideoFrame(markFrame);
	if (FramePtr != NULL)
	{
		PTimecode ptc;
		FramePtr->getTimecode(ptc);
		markImage->Hint = ptc.HalfFrameString().c_str();
	}

	// Kludge, the 6 should be read from the MTL->TrackBar
	markImage->Left = MTL->TrackBar->PositionInPixels(markFrame) + MTL->TrackBar->Left + 6 - markImage->Width / 2 + 1;

	// Adjust if invisible
	int MaxLeft = MTL->Width - 3 * markImage->Width / 4;
	if (markImage->Left > MaxLeft)
	{
		markImage->Left = MaxLeft;
	}
}

////---------------------------------------------------------------------------
//
// void __fastcall TTimelineFrame::FormKeyDown(TObject *Sender, WORD &Key,
// TShiftState Shift)
// {
// // Pass key down event to main window
// mainWindow->FormKeyDown(Sender, Key, Shift);
// }
////---------------------------------------------------------------------------
//
// void __fastcall TTimelineFrame::FormKeyUp(TObject *Sender, WORD &Key,
// TShiftState Shift)
// {
// // Pass key up event to main window
// mainWindow->FormKeyUp(Sender, Key, Shift);
// }
////---------------------------------------------------------------------------
//
// void __fastcall TTimelineFrame::FormCloseQuery(TObject *Sender, bool &CanClose)
// {
// fVisible = false;
// }
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::OnMTLResize(TObject *Sender)
{
	if (mainWindow == nullptr || mainWindow->Visible == false)
	{
		return;
	}

	ToolbarPanel->Left = (EntirePanel->Width - ToolbarPanel->Width) / 2;

	// if there's enough room, display the in and out timecode labels
	timelineInTCLabel->Visible = false;
	timelineOutTCLabel->Visible = timelineInTCLabel->Visible;

	// Resize the master timeline and reflect it here
	// We have a problem with inconsistent resizing
	// Resize the client according to the size of the MTL
	MTL->FrameResize(Sender);
	this->Height = MTL->Top + MTL->Height;

	// We call this a lot. It doesn't seem to mind.
	FrameResize(NULL);

	// reposition the in and out marks, if any
	UpdateMarks();
	CheckLicenseStatus();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::FrameResize(TObject *Sender)
{
	if (mainWindow == nullptr || mainWindow->Visible == false)
	{
		return;
	}

	// Width is anchored; height is controlled by number of visible timelines.
	MTL->Width = this->Width;
	this->Height = MTL->Top + MTL->Height;

	// Haha this is stupid - need to tell Master Time Line to reconfigure itself
	MTL->Frames(MTL->Frames());

   // Adjust font sizes of current timecode settings readouts
   ResizeTimecodeToolbar();

	////  NavButtonsMenuItemClick(Sender);
	CheckLicenseStatus();
}
// ---------------------------------------------------------------------------

// --------------------ClipChanged-----------------John Mertus----Mar 2003----

void TTimelineFrame::ClipChanged(void *Sender)

		// Callback when the clip has changed
		//
		// ****************************************************************************
{
	string tmpName = mainWindow->GetCurrentClipHandler()->getCurrentClipFileName();

	// Bug in TrackBar forces this
	_bInhibit = true;
	MTL->SetClipParameters(tmpName, VIDEO_SELECT_NORMAL, 0, mainWindow->GetCurrentClipHandler()->getCurrentVideoFramingIndex());
	performCutAction(CUT_REFRESH); // BUG 1623
	_bInhibit = false;
}

// -----------------CurrentTCChanged-----------------John Mertus----Mar 2003----

void TTimelineFrame::CurrentTCChanged(void *Sender)

		// Callback when the clip timecode in AdvancedRecordDlg has changed
		// This updates the record status bar
		//
		// ****************************************************************************
{
	_bTCUpdate = true;
}

// ---------------------MarksChanged-----------------John Mertus----Mar 2003----

void TTimelineFrame::MarksChanged(void *Sender)

		// Callback when the marks have changed
		//
		// ****************************************************************************
{
	_bMarkUpdate = true;

	// tell the tools that marks have changed
	CToolManager toolMgr;
	toolMgr.onNewMarks();
}

// ---------------------VisibleFrameRangeChanged--------------------------

void TTimelineFrame::VisibleFrameRangeChanged(void *Sender)

		// Callback when the blue region of the master tilene track bar has changed
		//
		// ****************************************************************************
{
	// tell the tools that marks have changed
	CToolManager toolMgr;
	toolMgr.onTimelineVisibleFrameRangeChange();
}

// ---------------------FrameCacheChanged---------------------------Sep 2010----

void TTimelineFrame::FrameCacheChanged(void *Sender)

		// Callback when the frame cache has changed
		//
		// ****************************************************************************
{
	_bFrameCacheUpdate = true;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::FrameCreate()
{
	SET_CBHOOK(ClipChanged, mainWindow->ClipHasChanged);
	SET_CBHOOK(CurrentTCChanged, mainWindow->GetPlayer()->CurrentTC);
	SET_CBHOOK(MarksChanged, mainWindow->GetPlayer()->MarksHaveChanged);
	SET_CBHOOK(FrameCacheChanged, mainWindow->FrameCacheHasChanged);
	SET_CBHOOK(VisibleFrameRangeChanged, MTL->VisibleFrameRangeHasChanged);
   SET_CBHOOK(UserPreferredTimecodeStringStyleChanged, (*(mainWindow->GetPlayer()->CurrentTC).getTimecodeStringStyleCBHook()));

	// Set Tags in Reticle toolbar
	ReticlePrimaryButton->Tag = RETICLE_MODE_PRIMARY;
	ReticleSafeActionButton->Tag = RETICLE_MODE_SAFE_ACTION;
	ReticleSafeTitleButton->Tag = RETICLE_MODE_SAFE_TITLE;
	ReticleMattedButton->Tag = RETICLE_MODE_MATTED;

	// build the contents of the reticle combo pulldown
	TStrings *reticleItems = ReticleToolbarComboBox->Items;
	reticleItems->Clear();

	// Load the default reticles into the pulldown
	for (int i = RETICLE_TYPE_NONE; i <= HIGHEST_BUILT_IN_RETICLE_TYPE_INDEX; i++)
	{
		reticleItems->Add(TmainWindow::defaultReticle[i].c_str());
	}

	// Load the custom reticles into the pulldown
	CIniFile *ini = CreateIniFile(RETINIFILE);
	StringList strlReticles;
	ini->ReadSection(TmainWindow::reticleSectionName, &strlReticles);
	for (StringList::iterator iter = strlReticles.begin(); iter != strlReticles.end(); ++iter)
	{
		string strTemp = ini->ReadString(TmainWindow::reticleSectionName, *iter, "");
		int endIndex = strTemp.find("]");

		// Add the custom reticle to the dropdown list
		reticleItems->Add(strTemp.substr(1, endIndex - 1).c_str());
	}

	DeleteIniFile(ini);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MTLTrackBarChange(TObject *Sender)
{
	// Kludge fix later
	if (_bInhibit)
	{
		return;
	}

	// Always update the "current frame" so the timeline cursors don't lag.
	// The special call to the player, below, suppresses the call from
	// the player to update the timeline when a new frame is displayed.
	MTL->CurrentFrame(MTL->TrackBar->Position());
	MTL->CursorPosition(MTL->CurrentFrame());

	// According to Kurt, this is completely bogus. There is no need to avoid
	// putting stuff in the queue while the player is active!!
	// The net effect of removing this line is that if you grab the cursor
	// and drag it while the clip is being played back, the playback will
	// immediately stop and you have control of what frames will be displayed.
	// With this line in, after you let go of the cursor it would snap back
	// to where the player was playing, which was clearly not your intention!
	// if (mainWindow->GetPlayer()->isIdle())
	if (MTL->CursorPosition() >= 0) // confuses the Player if frame < 0
	{
		// no callback from player when called at enterTrackBarPos()
		mainWindow->GetPlayer()->enterTrackBarPos(MTL->CursorPosition());
	}

	// update the timeline timecode readout
   UpdateCurTimecode();
}
//---------------------------------------------------------------------------

void __fastcall TTimelineFrame::UpdateTimerTimer(TObject *Sender)
{
	if (_bMarkUpdate)
	{
		UpdateMarks();
		_bMarkUpdate = false;
	}

	if (_bTCUpdate)
	{
		// MTL->CurrentFrame(mainWindow->GetPlayer()->getLastFrameIndex());
      UpdateCurTimecode();
		_bTCUpdate = false;
	}

	if (_bFrameCacheUpdate)
	{
		_bFrameCacheUpdate = false;

		// This is f*cked up, but I don't know how else to get at the
		// so-called "common data". Why isn't it in the class?
		CTimeLineDefs *timeLineDefs = MTL->getLine("Scene breaks", false);
		if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
		{
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->CachedFrameList(mainWindow->GetCachedFrameList());
			}
		}
	}

}
// ---------------------------------------------------------------------------

void TTimelineFrame::ReleaseClipInTimeline(void)
{
	_bInhibit = true;
	MTL->SetClipParameters("");
	_bInhibit = false;

}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::CopyTimeMenuItemClick(TObject *Sender)
{
	CVideoFrame *FramePtr;

	switch (((TComponent*)Sender)->Tag)
	{
	case estcIN:
		Clipboard()->AsText = timelineInTCLabel->Caption;
		break;

	case estcOUT:
		Clipboard()->AsText = timelineOutTCLabel->Caption;
		break;

	case estcCURRENT:
		{
			PTimecode ptc(mainWindow->GetPlayer()->getCurrentTimecode());
			Clipboard()->AsText = ptc.String().c_str();
		} break;

	case estcLEFT:
		FramePtr = GetVideoFrame(mainWindow->GetPlayer()->getMarkIn());
		if (FramePtr != NULL)
		{
			PTimecode ptc;
			FramePtr->getTimecode(ptc);
			Clipboard()->AsText = ptc.String().c_str();
		}
		else
		{
			Beep();
		}
		break;

	case estcRIGHT:
		FramePtr = GetVideoFrame(mainWindow->GetPlayer()->getMarkOut());
		if (FramePtr != NULL)
		{
			PTimecode ptc;
			FramePtr->getTimecode(ptc);
			Clipboard()->AsText = ptc.String().c_str();
		}
		else
		{
			Beep();
		}
		break;

	}

}
//---------------------------------------------------------------------------

// Note: this menu is duplicated in MainWindowUnit, which calls here.
// If you add/delete items you must make the change there, too!
void __fastcall TTimelineFrame::TimeDisplayMenuItemClick(TObject *Sender)
{
   CPlayer &player = *(mainWindow->GetPlayer());
	PTimecode &timecode = player.CurrentTC;
   switch (timecode.getDefaultUserPreferredStringStyle())
   {
      case PTimecode::ForceFrameNumberStyle:
         ForceFrameNumberDisplayMenuItem->Checked = true;
         break;

      case PTimecode::ForceTimecodeStyle:
         ForceTimecodeDisplayMenuItem->Checked = true;
         break;

      default:
         UseClipDefaultModeMenuItem->Checked = true;
         break;
   }

   int fps = timecode.getFramesPerSecond();
   bool df = false;
   bool fpsSettingsAreEnabled = false;
   if (fps == 0)
   {
      timecode.getDefaultFpsAndDropFrame(fps, df);
      fpsSettingsAreEnabled = true;
   }

   switch (fps)
   {
      case 25:
         Fps25MenuItem->Checked = true;
         break;

      case 30:
         Fps30DfMenuItem->Checked = df;
         Fps30MenuItem->Checked = !df;
         break;

      case 50:
         Fps50MenuItem->Checked = true;
         break;

      case 60:
         Fps60DfMenuItem->Checked = df;
         Fps60MenuItem->Checked = !df;
         break;

      default:
         Fps24MenuItem->Checked = true;
         break;
   }

   Fps24MenuItem->Enabled   = fpsSettingsAreEnabled;
   Fps25MenuItem->Enabled   = fpsSettingsAreEnabled;
   Fps30DfMenuItem->Enabled = fpsSettingsAreEnabled;
   Fps30MenuItem->Enabled   = fpsSettingsAreEnabled;
   Fps50MenuItem->Enabled   = fpsSettingsAreEnabled;
   Fps60DfMenuItem->Enabled = fpsSettingsAreEnabled;
   Fps60MenuItem->Enabled   = fpsSettingsAreEnabled;
}
//---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TimeDisplayDefaultModeMenuItemClick(TObject *Sender)
{
   auto menuItem = dynamic_cast<TMenuItem *>(Sender);
   MTIassert(menuItem != nullptr);
   if (menuItem == nullptr)
   {
      return;
   }

   MTIassert(menuItem->Tag == 1 || menuItem->Tag == 2 || menuItem->Tag == 3);

   auto newStringStyle = (menuItem->Tag == 2)
                           ? PTimecode::ForceTimecodeStyle
                           : ((menuItem->Tag == 3)
                              ? PTimecode::ForceFrameNumberStyle
                              : PTimecode::NormalStringStyle);

   CPlayer &player = *(mainWindow->GetPlayer());
	PTimecode &timecode = player.CurrentTC;
   timecode.setDefaultUserPreferredStringStyle(newStringStyle);

   if (timecode.getFramesPerSecond() == 0)
   {
      int fps;
      bool df;
      timecode.getDefaultFpsAndDropFrame(fps, df);
      player.changeNominalPlaybackSpeed(fps);
   }
}
//---------------------------------------------------------------------------

// Note: this menu is duplicated in MainWindowUnit, which calls here.
// If you add/delete items you must make the change there, too!
void __fastcall TTimelineFrame::FpsMenuItemClick(TObject *Sender)
{
   auto menuItem = dynamic_cast<TMenuItem *>(Sender);
   MTIassert(menuItem != nullptr);
   if (menuItem == nullptr)
   {
      return;
   }

   // Absolute value of Tag is FPS, negative indicates drop frame!
   auto tag = menuItem->Tag;
   MTIassert(tag == 24 || tag == 25 || tag == -30 || tag == 30 || tag == 50 || tag == -60 || tag == 60);

   CPlayer &player = *(mainWindow->GetPlayer());
	PTimecode &timecode = player.CurrentTC;
   bool df = tag < 0;
   int fps = df ? -tag : tag;
   timecode.setDefaultFpsAndDropFrame(fps, df);

   if (timecode.getFramesPerSecond() == 0)
   {
      player.changeNominalPlaybackSpeed(fps);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::timelineInTCLabelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (Button == mbRight)
	{
		CopyTCMenuItem->Enabled = true;
		CopyTCMenuItem->Tag = ((TComponent*)Sender)->Tag;
	}
	else
	{
		CopyTCMenuItem->Enabled = false;
		if (Button == mbLeft)
		{
			timelineCurrentTCVTimecodeEdit->tcP = mainWindow->GetPlayer()->getCurrentTimecode();
         timelineCurrentTCVTimecodeEdit->UpdateDisplay();
			timelineCurrentTCLabel->Visible = false;
			FpsReadoutLabel->Visible = false;
			timelineCurrentTCVTimecodeEdit->Visible = true;
			timelineCurrentTCVTimecodeEdit->SetFocus();
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ToolbarPanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	CopyTCMenuItem->Enabled = false;
}
// ---------------------------------------------------------------------------

bool TTimelineFrame::ReadSettings(CIniFile *ini, const string &IniSection)
{
	if (ini == NULL)
	{
		return (false);
	}
	MTL->ReadSettings(ini, IniSection);

	NavButtonsMenuItem->Checked = ini->ReadBool(IniSection, "ShowNavButtons", true);

	return true;
}
//---------------------------------------------------------------------------

bool TTimelineFrame::WriteSettings(CIniFile *ini, const string &IniSection)
{
	if (ini == NULL)
	{
		return (false);
	}

	MTL->WriteSettings(ini, IniSection);
	ini->WriteBool(IniSection, "ShowNavButtons", NavButtonsMenuItem->Checked);

	return true;
}
//---------------------------------------------------------------------------

bool TTimelineFrame::ReadSAP(CIniFile *ini, const string &IniSection)
{
	if (ini == NULL)
	{
		return (false);
	}

	MTL->ReadSAP(ini, IniSection);
	SyncMenusWithMTL();

	return true;
}
//---------------------------------------------------------------------------

void TTimelineFrame::SyncMenusWithMTL()
{
	// MTL Has an invisible set of menus - it is important that we keep
	// in synch with those.
	MTL->UpdateMenus();
	Ch1MenuItem->Checked = MTL->Ch1MenuItem->Checked;
	Ch2MenuItem->Checked = MTL->Ch2MenuItem->Checked;
	Ch3MenuItem->Checked = MTL->Ch3MenuItem->Checked;
	Ch4MenuItem->Checked = MTL->Ch4MenuItem->Checked;
	Ch5MenuItem->Checked = MTL->Ch5MenuItem->Checked;
	Ch6MenuItem->Checked = MTL->Ch6MenuItem->Checked;
	Ch7MenuItem->Checked = MTL->Channel62->Checked;
	Ch8MenuItem->Checked = MTL->Ch8MenuItem->Checked;
	HandlesMenuItem->Checked = MTL->HandlesMenuItem->Checked;
	SceneCutsMenuItem->Checked = MTL->Cuts1->Checked;
	BookmarksMenuItem->Checked = MTL->MarksMenuItem->Checked;
	TCRefMenuItem->Checked = MTL->TimecodeMenuItem->Checked;
	MaskAnimationMenuItem->Checked = MTL->MaskAnimationMenuItem->Checked;
}
//---------------------------------------------------------------------------

bool TTimelineFrame::WriteSAP(CIniFile *ini, const string &IniSection)
{
	if (ini == NULL)
	{
		return (false);
	}

	MTL->WriteSAP(ini, IniSection);
	return true;
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::CheckedMenuItemClick(TObject *Sender)
{
	// This is REAL, REAL bad,
	// However we will put the proper hooks into the Master Time Line
	MTL->Ch1MenuItem->Checked = Ch1MenuItem->Checked;
	MTL->Ch2MenuItem->Checked = Ch2MenuItem->Checked;
	MTL->Ch3MenuItem->Checked = Ch3MenuItem->Checked;
	MTL->Ch4MenuItem->Checked = Ch4MenuItem->Checked;
	MTL->Ch5MenuItem->Checked = Ch5MenuItem->Checked;
	MTL->Ch6MenuItem->Checked = Ch6MenuItem->Checked;
	MTL->Channel62->Checked = Ch7MenuItem->Checked;
	MTL->Ch8MenuItem->Checked = Ch8MenuItem->Checked;
	if (MTL->HandlesMenuItem->Checked != HandlesMenuItem->Checked)
	{
		// Urrrk...
		MTL->HandlesMenuItemClick(Sender);
		MTL->HandlesMenuItem->Checked = HandlesMenuItem->Checked;
	}

	TFBaseTimeLine *selectTimeline = NULL;
	if (MTL->Cuts1->Checked != SceneCutsMenuItem->Checked)
	{
      // "false" here because we of course need to find the timeline even if it's hidden!
		CTimeLineDefs *timeLineDefs = MTL->getLine("Scene breaks", false); // QQQ  manifest
		if ((timeLineDefs != NULL) && SceneCutsMenuItem->Checked)
		{
			selectTimeline = timeLineDefs->TimeLine;
		}
		MTL->Cuts1->Checked = SceneCutsMenuItem->Checked;

		// CAREFUL! This can be NULL the first time you show the scene cuts
		// timeline if the timeline was hidden on startup !!
		if (timeLineDefs->TimeLine != NULL)
		{
			// ===== Stolen from ShowCutsTimeline... God forbid you should show
			// ===== the frickin' timeline from only one place!
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->MarkIn(mainWindow->GetPlayer()->getMarkIn());
				commonData->MarkOut(mainWindow->GetPlayer()->getMarkOut());
				commonData->CachedFrameList(mainWindow->GetCachedFrameList());
			}
			// ===== End of code stolen from ShowCutsTimeline
		}
	}

	if (MTL->TimecodeMenuItem->Checked != TCRefMenuItem->Checked)
	{
      // "false" here because we of course need to find the timeline even if it's hidden!
		CTimeLineDefs *timeLineDefs = MTL->getLine("Reference", false); // QQQ manifest
		if ((timeLineDefs != NULL) && TCRefMenuItem->Checked)
		{
			selectTimeline = timeLineDefs->TimeLine;
		}
		MTL->TimecodeMenuItem->Checked = TCRefMenuItem->Checked;
	}

	if (MTL->MarksMenuItem->Checked != BookmarksMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Marks", false); // QQQ manifest
		if ((timeLineDefs != NULL) && BookmarksMenuItem->Checked)
		{
			selectTimeline = timeLineDefs->TimeLine;
		}
		MTL->MarksMenuItem->Checked = BookmarksMenuItem->Checked;
	}

	if (MTL->MaskAnimationMenuItem->Checked != MaskAnimationMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Mask Animation", false); // QQQ manifest
		if ((timeLineDefs != NULL) && MaskAnimationMenuItem->Checked)
		{
			selectTimeline = timeLineDefs->TimeLine;
		}
		MTL->MaskAnimationMenuItem->Checked = MaskAnimationMenuItem->Checked;
	}

	MTL->ReadMenuItemClick(Sender);

	if (selectTimeline != NULL)
	{
		// this is so frickin convoluted...
		selectTimeline->CommonData()->ActiveTL(selectTimeline);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MTLHandlesMenuItemClick(TObject *Sender)
{
	MTL->HandlesMenuItemClick(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ToolbarPanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	if (Screen->Cursor != crArrow)
	{
		Screen->Cursor = crArrow;
	}
}

// --------------------------Kea, 15-5-04----------------------------------------
// Hax0rs the buttons like the above, and reads menu item/tells the program to display the timeline.
// Called on a click on the right click popup menu on the timeline. HackedTimeline.
// ------------------------------------------------------------------------------
void __fastcall TTimelineFrame::CutItemClick(TObject *Sender)
{
	MTL->Cuts1->Checked = SceneCutsMenuItem->Checked;
	MTL->ReadMenuItemClick(Sender);

}
//---------------------------------------------------------------------------

void TTimelineFrame::OpenForReadingScenes()
{
	CPlayer *player = mainWindow->GetPlayer();
	markFrame = player->getInFrameIndex();
}
//---------------------------------------------------------------------------

int TTimelineFrame::GetNextScene(int *markIn, int *markOut)
{
	CPlayer *player = mainWindow->GetPlayer();
	int inFrame = player->getInFrameIndex();
	int outFrame = player->getOutFrameIndex() + 1;

	if (markFrame == outFrame)
	{
		return -1;
	}

	*markIn = markFrame++;

   // "false" here because we of course need to find the timeline even if it's hidden!
	CTimeLineDefs *sceneTimeLine = MTL->getLine("Scene breaks", false);
	if (sceneTimeLine == NULL)
	{
		*markIn = inFrame;
		*markOut = markFrame = outFrame;
		return 0;
	}

	CTimeLine *line = sceneTimeLine->TimeLine->TimeLine();
	while ((markFrame < outFrame) && ((line->getEventFlag(markFrame) & EVENT_CUT_MASK) == 0))
	{
		markFrame++;
	}

	*markOut = markFrame;

	return 0;
}
//---------------------------------------------------------------------------

// NOTE: CUT LIST always includes the clip in (always 0) and out frames (= number of frames in the clip)
void TTimelineFrame::getCutList(vector<int> &cutList)
{
	cutList.clear();
   cutList.push_back(0);

   // "false" here because we of course need to find the timeline even if it's hidden!
	CTimeLineDefs *sceneTimeLine = MTL->getLine("Scene breaks", false);
	if (sceneTimeLine == nullptr)
	{
		return;
	}

	TFBaseTimeLine *timeLine = sceneTimeLine->TimeLine;   // WTF
	if (timeLine == nullptr)
	{
		return;
	}

   CTimeLine *line = timeLine->TimeLine();   // WTF
	if (line == nullptr)
	{
		return;
	}

	int frame = 0;
	while (true)
	{
		// If there is no cut, getNextEvent() returns the frame you gave it!!
		int nextCut = line->getNextEvent(frame, EVENT_CUT_MASK);
		if (nextCut <= frame)
		{
			break;
		}

		cutList.push_back(nextCut);
		frame = nextCut;
	}

	// WTF: getOutFrameIndex() returns the index of the LAST FRAME in the clip, NOT the OUT frame!
	int outFrame = mainWindow->GetPlayer()->getOutFrameIndex() + 1;
   if (cutList.back() != outFrame)
   {
      cutList.push_back(outFrame);
   }
}

// Tri-state (-1 = disabled, 0 = off, 1 = on)
int TTimelineFrame::getShowAlphaState()
{
   return RgbAlphaButton->Enabled
          ? (RgbAlphaButton->Down
             ? 1
             : 0)
          : -1;
}

// Can only set it off or on (false, true)
void TTimelineFrame::setShowAlphaState(bool state)
{
   if (!RgbAlphaButton->Enabled || RgbAlphaButton->Down == state)
   {
      return;
   }

   RgbAlphaButton->Down = state;
   RgbAlphaButtonClick(nullptr);
}

// ------------------------------------------------------------------------------
// Kea, June 30, 2004
// This is the general handler for the cut editing buttons and keys. each keypress handler will pass a
// number to this method, and the corresponding action will be taken.
// ------------------------------------------------------------------------------

void TTimelineFrame::performCutAction(int actiontype)
{
	// Declarations of common local variables for each of the actions.

   // QQQ "true" here means that this will only work if the scene cuts timeline is showing!! Is that what we want??
	CTimeLineDefs *sceneTimeLine = MTL->getLine("Scene breaks", true);
	if (sceneTimeLine != NULL)
	{

		TFBaseTimeLine *frame = sceneTimeLine->TimeLine;
		CTimeLine *line = frame->TimeLine();
		CPlayer *player = mainWindow->GetPlayer();
		int currentFrame = MTL->CurrentFrame();
		int inFrame = player->getInFrameIndex();
		// !!! Just frickin' shoot me... this "out frame" is the last
		// frame in the clip ... it is NOT the OUT FRAME!
		// For sanity, we add 1 to get the real out frame
		int outFrame = player->getOutFrameIndex() + 1;
		int prevEventFrame = MARK_NOT_SET;
		int nextEventFrame = MARK_NOT_SET;

		// Clamp prev/next to current frame if before first/after last
		// event frame
		if (currentFrame >= 0)
		{
			prevEventFrame = line->getPreviousEvent(currentFrame, EVENT_CUT_MASK);
			if (prevEventFrame > currentFrame)
			{
				prevEventFrame = currentFrame;
			} // Before or at first event
			nextEventFrame = line->getNextEvent(currentFrame, EVENT_CUT_MASK);
			if (nextEventFrame < currentFrame)
			{
				nextEventFrame = currentFrame;
			} // At or after last event
		}

		// Switch performs actions on the event timeline.

		switch (actiontype)
		{

			// Delete Key "m" or "-" button resets event which does mem allocation
			// removes old event flags
			// adds a new event flag for frame of type delete.

		case CUT_DELETE:
			frame->HandleDeleteEventRequest(currentFrame, EVENT_CUT_MASK);
			break;

			// Add Key "n" or "+" button resets event which does mem allocation
			// removes old event flags
			// adds a new event_cut_add flag
		case CUT_ADD:
			frame->HandleAddEventRequest(currentFrame, EVENT_CUT_ADD);
			break;

			// Previous Key "j" or "P" button gets the previous event and makes
			// the displayer jump to that frame.

		case CUT_PREV:
			if (prevEventFrame >= inFrame)
			{
				// HUH??!? player->enterTrackBarPos(prevEventFrame);
				// HUH??!? player->setTrackBarUpdate(true);
				player->goToFrame(prevEventFrame); // better
			}
			break;

			// next key "k" or nav (8x ->) button gets the next event and makes
			// the displayer jump to that frame.

		case CUT_NEXT:
			if (nextEventFrame < outFrame)
			{
				// HUH??!? player->enterTrackBarPos(nextEventFrame);
				// HUH??!? player->setTrackBarUpdate(true);
				player->goToFrame(nextEventFrame);
			}
			break;

			// mark shot ("h" key) marks from this or previous event to the next
			// event
		case CUT_MARKSWITCH:
			{

				// If the prevEventFrame or nextEventFrame is the currentFrame,
				// then we are at or before the first event, or at or after the
				// last event, respectively. For sanity pretend the "in" and "out"
				// frames have cut events
				if (prevEventFrame == currentFrame)
				{
					prevEventFrame = inFrame;
				}
				if (nextEventFrame == currentFrame)
				{
					nextEventFrame = outFrame;
				}

				bool atMark = line->getEventFlag(currentFrame) & EVENT_CUT_MASK;
				int markInFrame = atMark ? currentFrame : prevEventFrame;
				int markOutFrame = nextEventFrame;

				player->setMarkIn(markInFrame);
				player->setMarkOut(markOutFrame);
				UpdateMarks();
			} break;

			// a no-op that simply forces the timelines to be reconstructed
		case CUT_REFRESH:
			line->ResetEvent();
			MTL->RedrawDisplay();
			break;
		}
	}
}

void TTimelineFrame::goToNextEventOnSelectedTimeline()
{
	// This is f*cked up, but I don't know how else to get at the
	// so-called "common data". Why isn't it in the class?
	CTimeLineDefs *sceneBreakTimeLineDefs = MTL->getLine("Scene breaks", false); // QQQ manifest
	CTimeLineDefs *bookmarkTimeLineDefs = MTL->getLine("Marks", false); // QQQ manifest
	CTimeLineDefs *timeLineDefs = sceneBreakTimeLineDefs;
	if (timeLineDefs == NULL || timeLineDefs->TimeLine == NULL)
	{
		timeLineDefs = bookmarkTimeLineDefs;
	}

	if (timeLineDefs == NULL || timeLineDefs->TimeLine == NULL)
	{
		return;
	}

	CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
	if (commonData == NULL)
	{
		return;
	}

	TFBaseTimeLine *timeLineToUse = commonData->ActiveTL();
	if (timeLineToUse == NULL)
	{
		// HACK if no timeline is selected, use the bookmarks one if visible.
		if (BookmarksMenuItem->Checked)
		{
			if (bookmarkTimeLineDefs != NULL && bookmarkTimeLineDefs->TimeLine != NULL)
			{
				timeLineToUse = bookmarkTimeLineDefs->TimeLine;
			}
		}
	}

	if (timeLineToUse == NULL)
	{
		// HACK if no timeline is selected and can't use bookmarks, use cuts
		if (sceneBreakTimeLineDefs != NULL && sceneBreakTimeLineDefs->TimeLine != NULL)
		{
			timeLineToUse = sceneBreakTimeLineDefs->TimeLine;
		}
	}

	if (timeLineToUse == NULL)
	{
		// Punt!
		return;
	}

	timeLineToUse->GoToNextEvent();
}

void TTimelineFrame::goToPreviousEventOnSelectedTimeline()
{
	// This is f*cked up, but I don't know how else to get at the
	// so-called "common data". Why isn't it in the class?
	CTimeLineDefs *sceneBreakTimeLineDefs = MTL->getLine("Scene breaks", false); // QQQ manifest
	CTimeLineDefs *bookmarkTimeLineDefs = MTL->getLine("Marks", false); // QQQ manifest
	CTimeLineDefs *timeLineDefs = sceneBreakTimeLineDefs;
	if (timeLineDefs == NULL || timeLineDefs->TimeLine == NULL)
	{
		timeLineDefs = bookmarkTimeLineDefs;
	}

	if (timeLineDefs == NULL || timeLineDefs->TimeLine == NULL)
	{
		return;
	}

	CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
	if (commonData == NULL)
	{
		return;
	}

	TFBaseTimeLine *timeLineToUse = commonData->ActiveTL();
	if (timeLineToUse == NULL)
	{
		// HACK if no timeline is selected, use the bookmarks one if visible.
		if (BookmarksMenuItem->Checked)
		{
			if (bookmarkTimeLineDefs != NULL && bookmarkTimeLineDefs->TimeLine != NULL)
			{
				timeLineToUse = bookmarkTimeLineDefs->TimeLine;
			}
		}
	}

	if (timeLineToUse == NULL)
	{
		// HACK if no timeline is selected and can't use bookmarks, use cuts
		if (sceneBreakTimeLineDefs != NULL && sceneBreakTimeLineDefs->TimeLine != NULL)
		{
			timeLineToUse = sceneBreakTimeLineDefs->TimeLine;
		}
	}

	if (timeLineToUse == NULL)
	{
		// Punt!
		return;
	}

	timeLineToUse->GoToPreviousEvent();
}

// --------------Kea, June 25, 2004---------------------------------------------
// Callback forwards a command to the master timeline.
// -----------------------------------------------------------------------------

void __fastcall TTimelineFrame::MTLCuts1Click(TObject *Sender)
{
	MTL->ReadMenuItemClick(Sender);

}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MTLCh1MenuItemClick(TObject *Sender)
{
	MTL->ReadMenuItemClick(Sender);

}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MTLShowHideMenuItemClick(TObject *Sender)
{
	MTL->TimeLinePopupPopup(Sender);

}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ShowHideMenuItemClick(TObject *Sender)
{
	SyncMenusWithMTL();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::Init(void)
{
	timelineCurrentTCVTimecodeEdit->Visible = false;
	timelineCurrentTCLabel->Visible = true;
	FpsReadoutLabel->Visible = true;

	TTimelineTrackBar &tb = *(MTL->TrackBar);
	bool tbEnabled = MTL->getTrackBarSoftEnable();

	tb.Height = 24;
	tb.EnableThumbRange = false; // true;
	tb.EnableThumbResize = false;

	tb.MinThumbSize = 17;
	tb.TrackStyle = trFlat;
	tb.ThumbStyle = tsRectangle; // tsRaisedRectangle;
	tb.TrackColor = (TColor)0x333333; // clWhite;
	tb.ThumbColor = (TColor)0x00907050; // clBtnFace;
	tb.Color = (TColor)0x00464646;
	tb.ThumbRangeColor = clBlack; // clBtnFace;
	tb.ShowThumbDetent = false;
	tb.ShowThumb = tbEnabled;
	tb.MarkInFrame = -1;
	tb.MarkOutFrame = -1;
	tb.ShowMarks = tbEnabled;
	tb.ShowCursors = tbEnabled;
	tb.JumpOnLeftDown = true;
	tb.ActiveDrag = adBOTH; // adCURSOR + adTHUMB
	tb.NumberOfCursors = 1;
	tb.DefaultCursorColor = (TColor)0x0000C0; // darkish red
	tb.ActiveCursor = 0;
	CTimelineCursor &cursor = tb.Cursors()->at(0);
	cursor.Style = CS_5_WIDE;
	tb.BorderWidth = 1;
	tb.ShowFocus = false;
	tb.EnableWheel = false;
	tb.Ctl3D = false;

	MTL->TrackBarAtTop(true);
	double curFrame = MTL->CurrentFrame();
	if (curFrame < 0)
	{ // HACK don't know why this is needed
		curFrame = 0;
	}

	MTL->CursorPosition(curFrame); // Forces build of timelines

	FrameCreate();
	FrameResize(NULL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ClipInTCButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_IN);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ClipOutTCButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_OUT);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MarkInTCButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_MARK_IN);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::MarkOutTCButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_MARK_OUT_MINUS_ONE);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLJogBackButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_JOG_REV_1_FRAME);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLJogForwardButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_JOG_FWD_1_FRAME);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLPlayReverseButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_PLAY_REV);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLPlayButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_PLAY);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLStopButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_STOP);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLRewindButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_REWIND);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLFastForwardButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_FAST_FWD);
}

// ---------------------------------------------------------------------------
//
// TTT - call the key handler directly so that DeWarp gets the keypress
//
void __fastcall TTimelineFrame::TLSetInMarkButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_MARK_IN);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLSetMarkOutExclusiveButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_MARK_OUT_EXCLUSIVE);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLSetMarkOutInclusiveButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_MARK_OUT_INCLUSIVE);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLMarkEntireClipButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_MARK_ALL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLSetShotMarksButtonClick(TObject *Sender)
{
	performCutAction(CUT_MARKSWITCH);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::TLClearMarksButtonClick(TObject *Sender)
{
	ExecuteNavigatorToolCommand(NAV_CMD_REMOVE_MARK_IN);
	ExecuteNavigatorToolCommand(NAV_CMD_REMOVE_MARK_OUT);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::timelineCurrentTCVTimecodeEditEnterKey(TObject *Sender)
{
	CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();

	string text(timelineCurrentTCVTimecodeEdit->tcP.getPreConformedString());
	if (text.size() > 1 && (text[0] == '+' || text[0] == '-'))
	{
		jumpString = text.substr(1);
	}

	timelineCurrentTCVTimecodeEdit->VExit(0);

	CTimecode cueupTC = timelineCurrentTCVTimecodeEdit->tcP;
	CTimecode boundedTC = clipHandler->limitTimecode(cueupTC, false /* TTT true */);
	timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
	ExecuteNavigatorToolCommand(NAV_CMD_GOTO_CUE);
	timelineCurrentTCVTimecodeEdit->Visible = false;
	timelineCurrentTCLabel->Visible = true;
	FpsReadoutLabel->Visible = true;
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::timelineCurrentTCVTimecodeEditExit(TObject *Sender)
{
	timelineCurrentTCVTimecodeEditEnterKey(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::timelineCurrentTCVTimecodeEditEscapeKey(TObject *Sender)
{
	timelineCurrentTCVTimecodeEdit->VExit(0);
	timelineCurrentTCVTimecodeEdit->Visible = false;
	timelineCurrentTCLabel->Visible = true;
	FpsReadoutLabel->Visible = true;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface)
{
	MTL->SetAnimateTimelineInterface(newInterface);
}

void TTimelineFrame::RedrawMTL()
{
	// Hack to update the timelines in the Master Timeline
	MTL->RedrawDisplay();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::showTimecodeEditor(int funcChar) // [+-*/124567890]
{
	// There is no docked timeline unit in Presentation Mode.
	if (mainWindow->_mainWindowState == MainWindowState::Presentation)
	{
		this->showTimecodeEditorInPresentationMode(funcChar);
		return;
	}

	if ((char) funcChar == '*' || (char) funcChar == '/')
	{
		PTimecode ptc(mainWindow->GetPlayer()->getCurrentTimecode());

		if (jumpString.empty() || (jumpString == string("0")))
		{
			jumpString = "1";
		}

		MTIostringstream os;
		os << (((char) funcChar == '*') ? string("+") : string("-")) << jumpString;
		ptc.ConformTimeASCII(os.str());

		CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();

		CTimecode cueupTC = ptc;
		CTimecode boundedTC = clipHandler->limitTimecode(cueupTC, false /* TTT true */);
		ExecuteNavigatorToolCommand(NAV_CMD_GOTO_CUE);

		timelineCurrentTCVTimecodeEdit->VExit(0);
		timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
		timelineCurrentTCVTimecodeEdit->Visible = false;
		timelineCurrentTCLabel->Visible = true;
		FpsReadoutLabel->Visible = true;
	}
	else
	{
		PTimecode ptc(mainWindow->GetPlayer()->getCurrentTimecode());

		timelineCurrentTCVTimecodeEdit->tcP = ptc;
		// doesn't work: timelineCurrentTCVTimecodeEdit->AutoSelect = false;
		timelineCurrentTCVTimecodeEdit->Visible = true;
		timelineCurrentTCVTimecodeEdit->SetFocus();
		timelineCurrentTCLabel->Visible = false;
		FpsReadoutLabel->Visible = false;

		if (funcChar != 0)
		{
			// Gaaaaa! Setting AutoSelect to false doesn't work, so we have
			// to make sure the setfocus message gets processed before trying
			// to set the selections
			Application->ProcessMessages();
			timelineCurrentTCVTimecodeEdit->Text = AnsiString((char) funcChar);
			timelineCurrentTCVTimecodeEdit->SelStart = 1;
			timelineCurrentTCVTimecodeEdit->SelLength = timelineCurrentTCVTimecodeEdit->Text.Length() - 1;
		}
	}
}
// ---------------------------------------------------------------------------

// Should be refactored with non-presentation-mode code, above!
void TTimelineFrame::showTimecodeEditorInPresentationMode(int funcChar)
{
	if ((char) funcChar == '*' || (char) funcChar == '/')
	{
		PTimecode ptc(mainWindow->GetPlayer()->getCurrentTimecode());

		if (jumpString.empty() || (jumpString == string("0")))
		{
			jumpString = "1";
		}

		MTIostringstream os;
		os << (((char) funcChar == '*') ? string("+") : string("-")) << jumpString;
		ptc.ConformTimeASCII(os.str());

		CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();
		////      TNavigatorWindow *navigatorWindow = mainWindow->GetNavigatorWindow();

		////      navigatorWindow->CueupTimecode->VExit(0);

		CTimecode cueupTC = ptc;
		CTimecode boundedTC = clipHandler->limitTimecode(cueupTC, false /* TTT true */);
		////      navigatorWindow->SetCueupTimecode(boundedTC);
		ExecuteNavigatorToolCommand(NAV_CMD_GOTO_CUE);

		// QQQ Ugliness
		if (!mainWindow->FloatingTimecodeToolbarParentPanel->Visible)
		{
			timelineCurrentTCVTimecodeEdit->VExit(0);
			timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
			timelineCurrentTCVTimecodeEdit->Visible = false;
			timelineCurrentTCLabel->Visible = true;
			FpsReadoutLabel->Visible = true;

			mainWindow->GetTimeline()->timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
		}
	}
	else
	{
		PTimecode ptc(mainWindow->GetPlayer()->getCurrentTimecode());

		// QQQ Ugliness
		if (!mainWindow->FloatingTimecodeToolbarParentPanel->Visible)
		{
			if (funcChar != 0)
			{
				// Need to copy in the existing timecode to set flags and such.
				GoToFrameForm->SetTimecode(ptc);
				GoToFrameForm->SetTriggerCharacter((char) funcChar);
				ShowModalDialog(GoToFrameForm);
				if (GoToFrameForm->ModalResult == mrOk)
				{
					// Need to pass the preconformed string separately because PTimecode doesn't copy it on assignment!
					mainWindow->GoToFrame(GoToFrameForm->GetTimecode(), GoToFrameForm->GetPreConformedTimecodeWidgetString());
				}
			}
		}
		else
		{
			timelineCurrentTCVTimecodeEdit->tcP = ptc;
			// doesn't work: timelineCurrentTCVTimecodeEdit->AutoSelect = false;
			timelineCurrentTCVTimecodeEdit->Visible = true;
			timelineCurrentTCVTimecodeEdit->SetFocus();
			timelineCurrentTCLabel->Visible = false;
			FpsReadoutLabel->Visible = false;

			if (funcChar != 0)
			{
				// Gaaaaa! Setting AutoSelect to false doesn't work, so we have
				// to make sure the setfocus message gets processed before trying
				// to set the selections
				Application->ProcessMessages();
				timelineCurrentTCVTimecodeEdit->Text = AnsiString((char) funcChar);
				timelineCurrentTCVTimecodeEdit->SelStart = 1;
				timelineCurrentTCVTimecodeEdit->SelLength = timelineCurrentTCVTimecodeEdit->Text.Length() - 1;
			}
		}
	}
} // ---------------------------------------------------------------------------

void TTimelineFrame::ShowMaskAnimationTimeline(bool flag)
{
	SyncMenusWithMTL();

	// DO NOT call CheckedMenuItemClick() if the state is OK else bad shit happens!
	if (MaskAnimationMenuItem->Checked != flag)
	{
		MaskAnimationMenuItem->Checked = flag;
		CheckedMenuItemClick(MaskAnimationMenuItem);
	}

	// If it's now visible, select it as well
	if (MaskAnimationMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Mask Animation", false);
		if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
		{
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->ActiveTL(timeLineDefs->TimeLine);
			}
		}
	}
}

void TTimelineFrame::ShowCutsTimeline(bool flag)
{
	SyncMenusWithMTL();

	// DO NOT call CheckedMenuItemClick() if the state is OK else bad shit happens!
	if (SceneCutsMenuItem->Checked != flag)
	{
		SceneCutsMenuItem->Checked = flag;
		CheckedMenuItemClick(SceneCutsMenuItem);
	}

	// If it's now visible, select it as well
	if (SceneCutsMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Scene breaks", false);
		if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
		{
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->MarkIn(mainWindow->GetPlayer()->getMarkIn());
				commonData->MarkOut(mainWindow->GetPlayer()->getMarkOut());
				commonData->CachedFrameList(mainWindow->GetCachedFrameList());
				commonData->ActiveTL(timeLineDefs->TimeLine);
			}
		}
	}
}

void TTimelineFrame::ShowBookmarkTimeline(bool flag)
{
	SyncMenusWithMTL();

	// DO NOT call CheckedMenuItemClick() if the state is OK else bad shit happens!
	if (BookmarksMenuItem->Checked != flag)
	{
		BookmarksMenuItem->Checked = flag;
		CheckedMenuItemClick(BookmarksMenuItem);
	}

	// If it's now visible, select it as well
	if (BookmarksMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Marks", false);
		if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
		{
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->ActiveTL(timeLineDefs->TimeLine);
			}
		}
	}
}

void TTimelineFrame::ShowReferenceTimeline(bool flag)
{
	SyncMenusWithMTL();

	// DO NOT call CheckedMenuItemClick() if the state is OK else bad shit happens!
	if (TCRefMenuItem->Checked != flag)
	{
		TCRefMenuItem->Checked = flag;
		CheckedMenuItemClick(TCRefMenuItem);
	}

	// If it's now visible, select it as well
	if (TCRefMenuItem->Checked)
	{
		CTimeLineDefs *timeLineDefs = MTL->getLine("Reference", false);
		if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
		{
			CTimeLineCommonData *commonData = timeLineDefs->TimeLine->CommonData();
			if (commonData != NULL)
			{
				commonData->ActiveTL(timeLineDefs->TimeLine);
			}
		}
	}
}

void TTimelineFrame::AddBookmark()
{
	CTimeLineDefs *timeLineDefs = MTL->getLine("Marks", false);
	if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
	{
		timeLineDefs->TimeLine->AddEventMenuItemClick(NULL);
	}
}

void TTimelineFrame::DropBookmark()
{
	CTimeLineDefs *timeLineDefs = MTL->getLine("Marks", false);
	if (timeLineDefs != NULL && timeLineDefs->TimeLine != NULL)
	{
		timeLineDefs->TimeLine->DropEventMenuItemClick(NULL);
	}
}

void TTimelineFrame::SetBookmarkIn()
{
	// Ouch! This is a really bad hack.
	if (BookmarkEventViewerForm == NULL || !BookmarkEventViewerForm->Visible || !BookmarkEventViewerForm->SetInMarkButton->Enabled)
	{
		return;
	}

	BookmarkEventViewerForm->SetInMarkButtonClick(NULL);
}

void TTimelineFrame::SetBookmarkOut()
{
	// Ouch! This is a really bad hack.
	if (BookmarkEventViewerForm == NULL || !BookmarkEventViewerForm->Visible || !BookmarkEventViewerForm->SetMarkOutInclusiveButton->Enabled)
	{
		return;
	}

	BookmarkEventViewerForm->SetMarkOutInclusiveButtonClick(NULL);
}

void TTimelineFrame::GoToBookmarkIn()
{
	// Ouch! This is a really bad hack.
	if (BookmarkEventViewerForm == NULL || !BookmarkEventViewerForm->Visible || !BookmarkEventViewerForm->MarkInSpeedButton->Enabled)
	{
		return;
	}

	BookmarkEventViewerForm->GoToInSpeedButtonClick(NULL);
}

void TTimelineFrame::GoToBookmarkOut()
{
	// Ouch! This is a really bad hack.
	if (BookmarkEventViewerForm == NULL || !BookmarkEventViewerForm->Visible || !BookmarkEventViewerForm->MarkOutSpeedButton->Enabled)
	{
		return;
	}

	BookmarkEventViewerForm->GoToOutSpeedButtonClick(NULL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::FormShow(TObject *Sender)
{
	CheckLicenseStatus();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::CheckLicenseStatus()
{
#ifndef NO_LICENSING

#endif
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::AnnoyingLicenseWarningTimerTimer(TObject *Sender)
{
	CheckLicenseStatus();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::LicenseMessageClick(TObject *Sender)
{
	LicenseStatusPanel->Visible = false;
	AnnoyingLicenseWarningTimer->Enabled = false; // toggle to reset period
	AnnoyingLicenseWarningTimer->Enabled = true; // so we'll check in an hour
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::UpgradeMessageClick(TObject *Sender)
{
	UpgradeStatusPanel->Visible = false;
	AnnoyingLicenseWarningTimer->Enabled = false; // toggle to reset period
	AnnoyingLicenseWarningTimer->Enabled = true; // so we'll check in an hour
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ShowLicenseStatusButtonClick(TObject *Sender)
{
	// if (LicenseStatusForm != NULL)
	// ShowModalDialog(LicenseStatusForm);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ReticleAllButtonClick(TObject *Sender)
{
	mainWindow->ShowAllReticles();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ReticleSelectionButtonClick(TObject *Sender)
{
	TSpeedButton *button = dynamic_cast<TSpeedButton*>(Sender);
	MTIassert(button != NULL);
	if (button == NULL)
	{
		return;
	}

	mainWindow->ToggleReticleBits(button->Tag);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ReticleEditButtonClick(TObject *Sender)
{
	mainWindow->EditReticles();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ReticleToolbarComboBoxClick(TObject *Sender)
{
	TComboBox *comboBox = dynamic_cast<TComboBox*>(Sender);
	MTIassert(comboBox != 0);
	if (comboBox == 0)
	{
		return;
	}

	string reticleName = StringToStdString(comboBox->Text);

	mainWindow->LoadReticlesByName(reticleName);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ReticleToolbarComboBoxCloseUp(TObject *Sender)
{
	mainWindow->FocusOnMainWindow();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::ToggleReticleComboBoxFocus()
{
	if (ReticleToolbarComboBox->Focused())
	{
		mainWindow->FocusOnMainWindow();
	}
	else
	{
		ReticleToolbarComboBox->SetFocus();
	}
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateReticleToolbarModeButtons(int mode)
{
	ReticleAllButton->Down = (mode & RETICLE_MODE_ALL) == RETICLE_MODE_ALL;
	ReticlePrimaryButton->Down = mode & RETICLE_MODE_PRIMARY;
	ReticleSafeActionButton->Down = mode & RETICLE_MODE_SAFE_ACTION;
	ReticleSafeTitleButton->Down = mode & RETICLE_MODE_SAFE_TITLE;
	ReticleMattedButton->Down = mode & RETICLE_MODE_MATTED;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateJustifyButtons(int justify)
{
	JustifyLeftButton->Down = justify < 0;
	JustifyCenterButton->Down = justify == 0;
	JustifyRightButton->Down = justify > 0;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateFrameCompareToolbarButtons(int whichButtonIsDown)
{
	FrameWipeCompareModeButton->Down = whichButtonIsDown == 1;
	DualFrameCompareModeButton->Down = whichButtonIsDown == 2;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateReticleToolbarComboBox(CReticle currentReticle)
{
	int type = currentReticle.getType();
	if (type < RETICLE_TYPE_CUSTOM)
	{

		ReticleToolbarComboBox->ItemIndex = type;
	}
	else
	{

		string curret = currentReticle.getFilename().c_str();
		TStrings *reticleItems = ReticleToolbarComboBox->Items;
		int i;
		for (i = RETICLE_TYPE_CUSTOM; i < reticleItems->Count; i++)
		{

			string tstret = StringToStdString(reticleItems->Strings[i]);
			if (curret == tstret)
			{
				break;
			}
		}
		ReticleToolbarComboBox->ItemIndex = i;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::JustifyLeftButtonClick(TObject *Sender)
{
	mainWindow->LeftJustifyTheDisplay();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::JustifyCenterButtonClick(TObject *Sender)
{
	mainWindow->CenterJustifyTheDisplay();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::JustifyRightButtonClick(TObject *Sender)
{
	mainWindow->RightJustifyTheDisplay();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::DualFrameCompareModeButtonClick(TObject *Sender)
{
	mainWindow->ToggleDualFrameCompareMode();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::FrameWipeCompareModeButtonClick(TObject *Sender)
{
	mainWindow->ToggleFrameWipeCompareMode();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbAllButtonClick(TObject *Sender)
{
	mainWindow->ShowAllColorChannels();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbRedButtonClick(TObject *Sender)
{
	mainWindow->ShowOrToggleColorChannel(RED_CHANNEL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbGreenButtonClick(TObject *Sender)
{
	mainWindow->ShowOrToggleColorChannel(GRN_CHANNEL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbBlueButtonClick(TObject *Sender)
{
	mainWindow->ShowOrToggleColorChannel(BLU_CHANNEL);
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbGrayButtonClick(TObject *Sender)
{
	mainWindow->ToggleGrayDisplay();
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::RgbAlphaButtonClick(TObject *Sender)
{
	mainWindow->ToggleAlphaDisplay();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateRGBChannelsToolbarButtons(int rgbFlags, bool alphaEnabled, bool showAlpha)
{
	RgbRedButton->Down = rgbFlags & RED_CHANNEL;
	RgbGreenButton->Down = rgbFlags & GRN_CHANNEL;
	RgbBlueButton->Down = rgbFlags & BLU_CHANNEL;
	RgbAllButton->Down = (rgbFlags & ALL_CHANNELS) == ALL_CHANNELS;
	RgbGrayButton->Down = rgbFlags & SHOW_GRAY;

	// update the Alpha button
	RgbAlphaButton->Enabled = alphaEnabled;
	if (alphaEnabled)
	{
		RgbAlphaButton->Down = showAlpha;
	}
	else
	{
		RgbAlphaButton->Down = false;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TTimelineFrame::ViewModeTBItemClick(TObject *Sender)
{
	mainWindow->CycleToNextViewMode();
}
// ---------------------------------------------------------------------------

void TTimelineFrame::UpdateZoomLevelStatus(double zoomFactor, int displayMode)
{
	MTIostringstream os;

	int percent = (int)((zoomFactor * 100.0) + 0.5);
	if (percent < 1)
	{
		percent = 1;
	}

	char val[32];

	switch (displayMode)
	{
	case DISPLAY_MODE_TRUE_VIEW:
		sprintf(val, "True View  %d%c", percent, '%');
		ViewModeTBItem->Caption = val;
		break;

	case DISPLAY_MODE_1_TO_1:
		{
			// calculate the correct zoom caption for 1-to-1
			int fmpels = 1;
			int topels = 1;
			if (zoomFactor < 1.0)
			{
				fmpels = (int)((1 / zoomFactor) + 0.5);
			}
			else
			{
				topels = (int)(zoomFactor + 0.5);
			}

			sprintf(val, " %d-to-%d  %d%%", fmpels, topels, percent);
			ViewModeTBItem->Caption = val;
		} break;

	case DISPLAY_MODE_FIT_WIDTH:
		sprintf(val, "Fit Width  %d%%", percent);
		ViewModeTBItem->Caption = val;
		break;
	}
}
// ---------------------------------------------------------------------------

string TTimelineFrame::GetJumpString()
{
	return jumpString;
}
// ---------------------------------------------------------------------------

void TTimelineFrame::SetJumpString(const string &newJumpString)
{
	jumpString = newJumpString;
}
//---------------------------------------------------------------------------

namespace
{
   int measureText(const AnsiString text, TFont *font)
   {
      static TBitmap *bm = new TBitmap;
      bm->Canvas->Font = font;
      return bm->Canvas->TextWidth(text);
   }
};

void TTimelineFrame::ResizeTimecodeToolbar()
{
//   DBCOMMENT("=========================================================");
//   DBTRACE(windowWidth);

   CenterToolbarCenteringPanel->ControlStyle << csOpaque;
   ClipInTCButton->ControlStyle << csOpaque;
   MarkInTCButton->ControlStyle << csOpaque;
   ClipOutTCButton->ControlStyle << csOpaque;
   MarkOutTCButton->ControlStyle << csOpaque;
   CurrentTCPanel->ControlStyle << csOpaque;
   auto parentWidth = CenterToolbarCenteringPanel->Parent->Width;

   float sizeFactor = 1.0F;
   bool stackFps = true;
   if (CenterToolbarCenteringPanel->Parent != mainWindow->FloatingTimecodeToolbarParentPanel)
   {
      sizeFactor = (parentWidth < 2000)
                     ? 0.80F // 0.76471F
                     : ((parentWidth < 2125) ? 0.85F // 0.82353F
                     : ((parentWidth < 2275) ? 0.90F // 0.88236F
                     : ((parentWidth < 2400) ? 0.95F // 0.94118F
                     : 1.0F)));

      stackFps = parentWidth < 2000;
   }

//   DBTRACE(sizeFactor);

   // FONT SIZES
   // Note that the speed buttons inherit font info from the panel.
   CenterToolbarCenteringPanel->Font->Height = int(-17 * sizeFactor);
   timelineCurrentTCLabel->Font->Height = int(-24 * sizeFactor);
   timelineCurrentTCVTimecodeEdit->Font->Height = timelineCurrentTCLabel->Font->Height;
   FpsReadoutLabel->Font->Height = CenterToolbarCenteringPanel->Font->Height + 1;

//   DBTRACE(CenterToolbarCenteringPanel->Font->Height);
//   DBTRACE(timelineCurrentTCLabel->Font->Height);
//   DBTRACE(timelineCurrentTCLabel->Width);
//   DBTRACE(timelineCurrentTCVTimecodeEdit->Font->Height);
//   DBTRACE(FpsReadoutLabel->Font->Height);

   // COMPONENT WIDTHS
   // Make clip in, clip out, and mark in buttons the same width.
   const auto speedButtonMargin = 2;
   const auto speedButtonIconWidth = 20;
   auto speedButtonMinWidth = measureText("000000", CenterToolbarCenteringPanel->Font);
   auto clipInTextWidth = measureText(ClipInTCButton->Caption, ClipInTCButton->Font);
   auto markInTextWidth = measureText(MarkInTCButton->Caption, MarkInTCButton->Font);
   auto clipOutTextWidth = measureText(ClipOutTCButton->Caption, ClipOutTCButton->Font);
   auto speedButtonTextWidth = std::max<int>(clipInTextWidth,
                                 std::max<int>(markInTextWidth,
                                    std::max<int>(clipOutTextWidth, speedButtonMinWidth)));
   auto newWidth = (speedButtonMargin * 2) + speedButtonIconWidth + clipOutTextWidth;
   if (newWidth != ClipInTCButton->Width)
   {
      ClipInTCButton->Width = newWidth;
      MarkInTCButton->Width = newWidth;
      ClipOutTCButton->Width = newWidth;
   }

   // Mark out button Is wider to accomodate the length readout.
   speedButtonMinWidth = measureText("000000 : 000", CenterToolbarCenteringPanel->Font);
   auto MarkOutTextWidth = measureText(MarkOutTCButton->Caption, MarkOutTCButton->Font);
   speedButtonTextWidth = std::max<int>(speedButtonMinWidth, MarkOutTextWidth);
   newWidth = (speedButtonMargin * 2) + speedButtonIconWidth + speedButtonTextWidth;
   if (newWidth != MarkOutTCButton->Width)
   {
      MarkOutTCButton->Width = newWidth;
   }

   // Min width for the current TC is 8 digits.
   auto minCurrentTCWidth = measureText("00000000", timelineCurrentTCLabel->Font);
   auto currentTCTextWidth = measureText(timelineCurrentTCLabel->Caption, timelineCurrentTCLabel->Font);
   newWidth = 8 + std::max<int>(minCurrentTCWidth, currentTCTextWidth);
   if (newWidth != CurrentTCPanel->Width)
   {
      CurrentTCPanel->Width = newWidth;
   }

   // FPS readout is fixed width.
   FpsReadoutLabel->Width = measureText("000 fps", FpsReadoutLabel->Font);

//   DBTRACE(ClipInTCButton->Width);
//   DBTRACE(MarkInTCButton->Width);
//   DBTRACE(CurrentTCPanel->Width);
//   DBTRACE(FpsReadoutLabel->Width);
//   DBTRACE(MarkOutTCButton->Width);
//   DBTRACE(ClipOutTCButton->Width);

   // COMPONENT POSITIONS
   auto spacing = sizeFactor * sizeFactor * measureText("00", CenterToolbarCenteringPanel->Font);
   auto newLeft = spacing;
   if (newLeft != ClipInTCButton->Left)
   {
      ClipInTCButton->Left = newLeft;
   }

   newLeft = ClipInTCButton->Left + ClipInTCButton->Width + spacing;
   if (newLeft != MarkInTCButton->Left)
   {
      MarkInTCButton->Left = newLeft;
   }

   newLeft = MarkInTCButton->Left + MarkInTCButton->Width + (spacing * 2);
   if (newLeft != CurrentTCPanel->Left)
   {
      CurrentTCPanel->Left = newLeft;
   }

   if (stackFps)
   {
      timelineCurrentTCLabel->Top = 0;
      FpsReadoutLabel->Parent = CurrentTCPanel;
      FpsReadoutLabel->Left = (CurrentTCPanel->Width - FpsReadoutLabel->Width) / 2;
      FpsReadoutLabel->Top = 18;
      MarkOutTCButton->Left = CurrentTCPanel->Left + CurrentTCPanel->Width + (spacing * 2);
   }
   else
   {
      timelineCurrentTCLabel->Top = 4;
      FpsReadoutLabel->Parent = CenterToolbarCenteringPanel;
      FpsReadoutLabel->Left = CurrentTCPanel->Left + CurrentTCPanel->Width + spacing;
      FpsReadoutLabel->Top = 24 + FpsReadoutLabel->Font->Height; // NOTE: Height is < 0!
      MarkOutTCButton->Left = FpsReadoutLabel->Left + FpsReadoutLabel->Width + (spacing * 2);
   }

   newLeft = MarkOutTCButton->Left + MarkOutTCButton->Width + spacing;
   if (newLeft != ClipOutTCButton->Left)
   {
      ClipOutTCButton->Left = newLeft;
   }

//   DBTRACE(ClipInTCButton->Left);
//   DBTRACE(MarkInTCButton->Left);
//   DBTRACE(CurrentTCPanel->Left);
//   DBTRACE(FpsReadoutLabel->Left);
//   DBTRACE(MarkOutTCButton->Left);
//   DBTRACE(ClipOutTCButton->Left);

   // Set panel width and position it so the Current TC looks somewhat centered.
   newWidth = ClipOutTCButton->Left + ClipOutTCButton->Width + spacing;

   if (CenterToolbarCenteringPanel->Parent == mainWindow->FloatingTimecodeToolbarParentPanel)
   {
      parentWidth = newWidth + 8;
      CenterToolbarCenteringPanel->Parent->Width = parentWidth;
   }

   newLeft = (parentWidth - newWidth + (stackFps ? 0 : FpsReadoutLabel->Width)) / 2;

   if (newWidth != CenterToolbarCenteringPanel->Width)
   {
      CenterToolbarCenteringPanel->Width = newWidth;
   }

   if (newLeft != CenterToolbarCenteringPanel->Left)
   {
      CenterToolbarCenteringPanel->Left = newLeft;
   }

//   DBTRACE(CenterToolbarCenteringPanel->Width);
//   DBTRACE(CenterToolbarCenteringPanel->Left);
}
