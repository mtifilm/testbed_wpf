//---------------------------------------------------------------------------

#pragma hdrstop

#include "GLError.h"

#include "IniFile.h"
#include "MTIstringstream.h"
#include <GL/glew.h>
//---------------------------------------------------------------------------


#pragma package(smart_init)


bool _check_gl_error(const char *file, int line)
{
#ifdef _DEBUG
	GLenum errorCode(glGetError());
	bool prependComma = false;
	MTIostringstream os;
	string errorString;
	int errorCount = 0;

	int GL_INVALID_OPERATION_count = 0;
	while (errorCode != GL_NO_ERROR && GL_INVALID_OPERATION_count < 5)
	{
		++errorCount;
		switch (errorCode)
		{
			case GL_INVALID_OPERATION:
				++GL_INVALID_OPERATION_count;
				errorString = "INVALID_OPERATION";
				break;

			case GL_INVALID_ENUM:
				errorString = "INVALID_ENUM";
				break;

			case GL_INVALID_VALUE:
				errorString = "INVALID_VALUE";
				break;

			case GL_OUT_OF_MEMORY:
				errorString = "OUT_OF_MEMORY";
				break;

			case GL_INVALID_FRAMEBUFFER_OPERATION:
				errorString = "INVALID_FRAMEBUFFER_OPERATION";
				break;

			default:
				errorString = "UNEXPECTED_ERROR";
				break;
		}

		if (prependComma)
		{
			os << ", ";
		}
		else
		{
			prependComma = true;
		}

		os << "GL_" << errorString << " (" << errorCode << ")";
		errorCode = glGetError();
	}

	if (errorCount == 0)
	{
		return false;
	}

	TRACE_0(errout << "ERROR: GL operation at " << file << "." << line << " failed: " << os.str());

	return true;

#else // RELEASE MODE

	return false;

#endif

}
