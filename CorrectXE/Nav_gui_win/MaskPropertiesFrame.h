//---------------------------------------------------------------------------

#ifndef MaskPropertiesFrameH
#define MaskPropertiesFrameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "ColorPanel.h"
#include "CompactTrackEditFrameUnit.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "cspin.h"
#include <ExtCtrls.hpp>
#include "MTIUNIT.h"
#include <Vcl.ComCtrls.hpp>
#include "TrackEditFrameUnit.h"
#include "CompactTrackEditFrameUnit.h"
#include <Vcl.Buttons.hpp>
#include "ColorPanel.h"
#include <Vcl.Dialogs.hpp>

//---------------------------------------------------------------------------
// Forward Declarations

class CMaskTool;
//---------------------------------------------------------------------------
class TMaskPropertiesFrame : public TFrame
{
__published:	// IDE-managed Components
	TPanel *MaskInfoPanel;
	TSpeedButton *LoadMaskButton;
	TBitBtn *RevertlButton;
	TCheckBox *ShowInteriorCheckBox;
	TBitBtn *InclusiveCenterButton;
	TBitBtn *InclusiveExteriorButton;
	TBitBtn *InclusiveInteriorButton;
	TCompactTrackEditFrame *ExteriorBlendTrackEdit;
	TCompactTrackEditFrame *InteriorBlendTrackEdit;
	TSaveDialog *MaskSaveDialog;
	TOpenDialog *MaskOpenDialog;
	TSpeedButton *SaveMaskButton;
	TTimer *MaskChangeTimer;
	TFlowPanel *SlewControlFlowPanel;
   void __fastcall RevertlButtonClick(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall InclusiveTrackEditNotifyWidgetClick(TObject *Sender);
	void __fastcall BlendPositionRadioGroupClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall CenterButtonClick(TObject *Sender);
	void __fastcall ExteriorButtonClick(TObject *Sender);
	void __fastcall InteriorButtonClick(TObject *Sender);
	void __fastcall InclusiveSlewTrackEditNotifyWidgetClick(TObject *Sender);
	void __fastcall ShowInteriorCheckBoxClick(TObject *Sender);
	void __fastcall LoadMaskButtonClick(TObject *Sender);
	void __fastcall SaveMaskButtonClick(TObject *Sender);
	void __fastcall MaskChangeTimerTimer(TObject *Sender);

private:	// User declarations
   int _savedInclusionBorderWidth = 30;
   int _savedExclusionBorderWidth = 30;

   // this uses the mask from main window.  Can be an issue
   void setGuiFromMask();

public:		// User declarations
	__fastcall TMaskPropertiesFrame(TComponent* Owner);
   void GetSettings(CMaskTool &maskTool);
   void UpdateSettings(CMaskTool &maskTool);
   void processToolCommand(int command);

   // We are currently using visiblility as the enable
   // bad but can be changed later
   bool isMaskFrameEnabled() { return this->Visible == true; }
   void setMaskFrameEnabled(bool value) { this->Visible = value; }

   void saveMask();
   void loadMask();

   int getSavedInclusionBorderWidth() const { return _savedInclusionBorderWidth; }
   void setSavedInclusionBorderWidth(int value) { _savedInclusionBorderWidth = value; }

   int getSavedExclusionBorderWidth() const { return _savedExclusionBorderWidth; }
   void setSavedexclusionBorderWidth(int value) { _savedExclusionBorderWidth = value; }
};
//---------------------------------------------------------------------------
extern PACKAGE TMaskPropertiesFrame *MaskPropertiesFrame;
//---------------------------------------------------------------------------
#endif
