// MainWindowUnit.h   Navigator's Main Window`
//
/*
 $Header: /usr/local/filmroot/Nav_gui_win/MainWindowUnit.h,v 1.161.2.40 2009/11/01 00:05:19 tolks Exp $
 */
/////////////////////////////////////////////////////////////////////////////

#ifndef MainWindowUnitH
#define MainWindowUnitH
// ---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Buttons.hpp>
#include <ImgList.hpp>

#include "BinMgrGUIInterface.h"
#include "HRTimer.h"
#include "ImageInfo.h"
#include "MTIBitmap.h"
#include "MTIUNIT.h"
#include "PropX.h"
//#include "cspin.h"
#include <Vcl.OleServer.hpp>
#include <SHDocVw.hpp>
#include <Vcl.OleCtrls.hpp>
#include "ColorPanel.h"
#include "ColorLabel.h"
#include "VTimeCodeEdit.h"
#include "VDoubleEdit.h"
#include "TimelineFrameUnit.h"
#include "ColorSplitter.h"
#include <System.ImageList.hpp>
#include <memory>

#include <gl\gl.h>
#include <gl\glu.h>
#include <string>
using std::string;
using std::setw;
using std::setfill;
using std::setbase;
using std::left;
using std::right;

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBezierEditor;
class CDisplayer;
class CExtractor;
class CLineEngine;
class CLutSettingsManager;
class CMaskTool;
class CNavCurrentClip;
class CPDLEntry;
class CPlayer;
class CRenderDestinationClip;
class CReticleTool;
class CToolObject;
class CToolSystemInterface;
class DrawingArea;
class ImportExportFormBase;
class TfullWindow;
class TTimelineFrame;
class MtiLicenseLib;
////class CAutoCleanDebrisFile;

enum MainWindowState
{
	NormalWindow, FullScreen, Presentation
};

// ---------------------------------------------------------------------------

// Bin Manager GUI Interface
class CNavBinMgrGUIInterface : public CBinMgrGUIInterface
{
public:
	void ShowBinManagerGUI(bool show);
	bool IsItSafeToUnloadCurrentClip();
	int SaveCurrentClipState();
	void ClipDoubleClick(const string& clipFileName);
	void ClipDeleted(const string& clipFileName);
	void ClipModified(const string& clipFileName);
	lockID_t RequestBinRename(const string &oldBinPath, const string &newBinPath, string &reason);
	lockID_t RequestClipRename(const string &oldBinPath, const string &oldClipName,
		const string &newBinPath, const string &newClipName, string &reason);
	int FinishClipRename(lockID_t lockID, const string &oldBinPath, const string &oldClipName,
		const string &newBinPath, const string &newClipName, string &reason);
   void RelinkClipMedia(ClipIdentifier clipId);
   bool WeWantToHackAClip();
	void ClipHasChanged(void);
	bool RecordListOfClips(const StringList &slClips, const SRecordData &BatchData);
   int CanWeDiscardOrCommit(bool discardFlag, std::string &message);
   void SaveTimelinePositions();
   void RestoreTimelinePositions();
	// void CrashRecord(const StringList &slClips, const SRecordData &BatchData);
#ifdef _OLD_LUT_CRAP_
	void SetBinDisplayLutToDefault(const string &binPath);
	void SetBinDisplayLutToCustom(const string &binPath, bool editLUTFirst);
	void SaveClipLUTInfo();
	void LoadClipLUTInfo();
#endif

	void FormKeyDown(WORD &Key, TShiftState Shift);
	void FormKeyUp(WORD &Key, TShiftState Shift);

   string GetRedClipName();
   void GetCurrentMarks(int &markInIndex, int &markOutIndex);
};

// ---------------------------------------------------------------------------
// class TmainWindow : public TMTIMainForm
class TmainWindow : public TMTIForm
{
__published: // IDE-managed Components

	TOpenDialog *OpenDialog;
	TPopupMenu *AddRemovePopup;
	TImageList *MaskTBImageList;
   TPanel *MainWindowBottomPanel;
	TImageList *TeranexIconImageList;
	TImageList *MTIIconImageList;
	TImageList *RecordedToDiskStatusImageList;
	TImageList *RGBChannelTBImageList;
	TImageList *PanTBImageList;
	TImageList *RGBChannelClickedImageList;
	TImageList *PlaybackFilterImageList;
	TImageList *JustifyTBImageList;
	TImageList *LutTBImageList;
	TMainMenu *MainMenu;
	TMenuItem *FileMenu;
	TMenuItem *OpenClipMenuItem;
	TMenuItem *N13;
	TMenuItem *CreateNewVersionMenuItem;
	TMenuItem *N14;
	TMenuItem *CommitChangesAndDeleteMenuItem;
	TMenuItem *CommitChangesInMarkedRangeMenuItem;
	TMenuItem *DiscardAllChangesMenuItem;
	TMenuItem *DiscardChangesInMarkedRangeMenuItem;
	TMenuItem *N11;
	TMenuItem *N1;
	TMenuItem *QuitWithoutSavingMenuItem;
	TMenuItem *ExitMenuItem;
	TMenuItem *PreferencesMenuItem;
	TMenuItem *ViewMenu;
	TMenuItem *DisplayModeSubmenu;
	TMenuItem *AspectSubmenuItem;
	TMenuItem *AspectDefItem;
	TMenuItem *Aspect133Item;
	TMenuItem *Aspect166Item;
	TMenuItem *Aspect178Item;
	TMenuItem *Aspect185Item;
	TMenuItem *Aspect220Item;
	TMenuItem *Aspect235Item;
	TMenuItem *Aspect239Item;
	TMenuItem *Aspect255Item;
	TMenuItem *Aspect266Item;
	TMenuItem *OneToOneMenuItem;
	TMenuItem *FitWidthMenuItem;
	TMenuItem *LUTSubmenu;
	TMenuItem *EditLutMenuItem;
	TMenuItem *EditLutMenuSeparatorItem;
	TMenuItem *NoLutMenuItem;
	TMenuItem *ClipLutMenuItem;
	TMenuItem *ProjectLutMenuItem;
	TMenuItem *BuiltInLutMenuSeparatorItem;
	TMenuItem *N3;
	TMenuItem *FramingMenuItem;
	TMenuItem *VideoFramesMenuItem;
	TMenuItem *FilmFramesMenuItem;
	TMenuItem *Fields12MenuItem;
	TMenuItem *Field1OnlyMenuItem;
	TMenuItem *Field2OnlyMenuItem;
	TMenuItem *CadenceRepairMenuItem;
	TMenuItem *DisplayDevicesSubmenu;
	TMenuItem *LocalDisplayButton;
	TMenuItem *RemoteDisplayButton;
	TMenuItem *BothDisplayButton;
	TMenuItem *DisplaySpeedSubMenu;
	TMenuItem *NominalSpeedMenuItem;
	TMenuItem *SlowSpeedMenuItem;
	TMenuItem *MediumSpeedMenuItem;
	TMenuItem *FastSpeedMenuItem;
	TMenuItem *UnlimitedSpeedMenuItem;
	TMenuItem *N5;
	TMenuItem *DisplayFilterSubmenu;
	TMenuItem *NoneMenuItem;
	TMenuItem *OriginalValuesMenuItem;
	TMenuItem *HighlightFixesMenuItem;
	TMenuItem *BothMenuItem;
	TMenuItem *MaskMenu;
	TMenuItem *EnableMaskMenuItem;
	TMenuItem *N7;
	TMenuItem *SaveMaskMenuItem;
	TMenuItem *LoadMaskMenuItem;
	TMenuItem *WindowsMenu;
	TMenuItem *FullScreenMenuItem;
	TMenuItem *N6;
	TMenuItem *BinManagerMenuItem;
	TMenuItem *ShowPDLViewerMenuItem;
	TMenuItem *RecordConsolelMenuItem;
	TMenuItem *ShowActiveToolMenuItem;
	TMenuItem *N4;
	TMenuItem *RestoreAllMenu;
	TMenuItem *DefaultsSapMenu;
	TMenuItem *RestoreSAPMenuItem;
	TMenuItem *StartupSAPMenu;
	TMenuItem *SavePositions1;
	TMenuItem *SaveUserPositionsMenuItem;
	TMenuItem *SaveStartupPositionsMenuItem;
	TMenuItem *HelpMenu;
	TMenuItem *ToolHelpMenuItem;
	TMenuItem *AboutMenu;
   TColorPanel *DisplayPanel;
	TMenuItem *BookmarkEventViewerMenuItem;
   TColorPanel *FloatingTimecodeToolbarParentPanel;
   TPanel *FloatingTimelineToolbarRibbonParentPanel;
	TTimer *RibbonTimer;
   TTimer *ResizeHackTimer;
   TTrackBar *DummyFocusTrackBar;
	TMenuItem *N8;
	TMenuItem *LicenseInformation1;
	TMenuItem *N9;
	TMenuItem *ActivateLicense1;
   TTimer *RefreshFrameHackTimer;
   TTimelineFrame *TimelineFrame;
   TColorPanel *OpenGLBugOnePixelPaddingHackLeftColorPanel;
   TColorPanel *OpenGLBugOnePixelPaddingHackRightColorPanel;
   TPanel *MainWindowTopPanel;
   TColorSplitter *TopPanelSplitter;
   TPaintBox *TopPanelPaintBox;
   TMenuItem *UserManualMenuItem;
   VTimeCodeEdit *StupidHackVTimecodeEdit;
   TMenuItem *N2;
   TMenuItem *TimeDisplayMenuIten;
   TMenuItem *UseClipDefaultModeMenuItem;
   TMenuItem *ForceTimecodeDisplayMenuItem;
   TMenuItem *ForceFrameNumberDisplayMenuItem;
   TMenuItem *N12;
   TMenuItem *Fps24MenuItem;
   TMenuItem *Fps25MenuItem;
   TMenuItem *Fps30MenuItem;
   TMenuItem *Fps50MenuItem;
   TMenuItem *Fps60MenuItem;
   TMenuItem *Fps60DfMenuItem;
   TMenuItem *Fps30DfMenuItem;
   TMenuItem *N16;
   TMenuItem *ExportVersionClipFilesMenuItem;
   TMenuItem *ExportFullClip;
   TMenuItem *ExportMarkedRangeMenuItem;
	TColorPanel *FrameBufferErrorDisplayPanel;
	TColorLabel *ErrorMessageLine1Label;
	TColorLabel *ErrorMessageLine2Label;
	TColorLabel *ErrorMessageLine3Label;
	TColorPanel *FrameBufferErrorCoverUpPanel;
	TColorPanel *LeftFrameClipNamePanel;
	TColorLabel *LeftFrameClipNameLabelWhite;
	TColorPanel *RightFrameClipNamePanel;
	TColorLabel *RightFrameClipNameLabelWhite;
	TTimer *FrameCompareLabelUpdateTimer;
	TTimer *MaskComputeTimer;
	TMenuItem *GlobalHotKeysHelpMenuItem;
   TButton *RelinkMediaFolderButton;
   TColorPanel *OpenGLBugOnePixelPaddingHackBottomColorPanel;
   TMenuItem *ReleaseNotes1;
	TTimer *TaskbarAutohideHackTimer;

	void __fastcall OpenClipMenuItemClick(TObject *Sender);
	void __fastcall ExitMenuItemClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormPaint(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall LoadToolsMenuItemClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		int X, int Y);
	void __fastcall FormMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		int X, int Y);
	void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall HistoryMainMenuHistoryClick(String Str);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall FramingMenuItemClick(TObject *Sender);
	void __fastcall DisplayModeClick(TObject *Sender);
	void __fastcall DisplayDevicesClick(TObject *Sender);
	void __fastcall DefaultsSapMenuClick(TObject *Sender);
	void __fastcall StartupSAPMenuClick(TObject *Sender);
	void __fastcall StartupPositionsClick(TObject *Sender);
	void __fastcall SaveUserPositionsMenuItemClick(TObject *Sender);
	void __fastcall SaveStartupPositionsMenuItemClick(TObject *Sender);
	void __fastcall RestoreSAPMenuItemClick(TObject *Sender);
	void __fastcall AboutMenuClick(TObject *Sender);
	void __fastcall PreferencesMenuItemClick(TObject *Sender);
	void __fastcall CopyMenuItemClick(TObject *Sender);
	void __fastcall CutMenuItemClick(TObject *Sender);
	void __fastcall PasteMenuItemClick(TObject *Sender);
	void __fastcall QuitWithoutSavingMenuItemClick(TObject *Sender);
	// void __fastcall DisplayPixelsSubMenuSetup(TObject *Sender);
	// void __fastcall ResizeDisplaySubMenuSetup(TObject *Sender);
	void __fastcall DisplayModeSubmenuClick(TObject *Sender);
	void __fastcall ShowActiveToolMenuItemClick(TObject *Sender);
	void __fastcall WindowsMenuClick(TObject *Sender);
	void __fastcall FramingSubmenuSetup(TObject *Sender);
	// void __fastcall DisplayQualitySubmenuSetup(TObject *Sender);
	// void __fastcall DisplayQualityClick(TObject *Sender);
	void __fastcall DisplaySpeedSubmenuClick(TObject *Sender);
	void __fastcall DisplaySpeedClick(TObject *Sender);
	void __fastcall DisplayDevicesSubmenuClick(TObject *Sender);

	// void __fastcall FrameDisplayWorkProc(TObject *Sender, bool &done);

	void __fastcall WMEraseBkgnd(TMessage &Message);
	void __fastcall DisplayFilterSubmenuClick(TObject *Sender);
	void __fastcall DisplayFilterClick(TObject *Sender);
	void __fastcall ImportImageFilesMenuItemClick(TObject *Sender);
	void __fastcall ExportImageFilesMenuItemClick(TObject *Sender);
	void __fastcall EnableMaskMenuItemClick(TObject *Sender);
	void __fastcall MaskBorderMenuItemClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ShowPDLViewerMenuItemClick(TObject *Sender);
	void __fastcall ViewMenuClick(TObject *Sender);
	void __fastcall NoLutMenuItemClick(TObject *Sender);
	void __fastcall DefaultLUTMenuItemClick(TObject *Sender);
	void __fastcall LUTSubmenuClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall FullScreenMenuItemClick(TObject *Sender);
	void __fastcall UserLUTMenuItemClick(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall CreateNewVersionMenuItemClick(TObject *Sender);
	void __fastcall CommitChangesAndDeleteMenuItemClick(TObject *Sender);
	void __fastcall CommitChangesInMarkedRangeMenuItemClick(TObject *Sender);
	void __fastcall DiscardAllChangesMenuItemClick(TObject *Sender);
	void __fastcall DiscardChangesInMarkedRangeMenuItemClick(TObject *Sender);
	void __fastcall FileMenuClick(TObject *Sender);
	void __fastcall MaskMenuClick(TObject *Sender);
	void __fastcall SaveMaskMenuItemClick(TObject *Sender);
	void __fastcall LoadMaskMenuItemClick(TObject *Sender);
	void __fastcall BinManagerMenuItemClick(TObject *Sender);
	void __fastcall ToolHelpMenuItemClick(TObject *Sender);
	void __fastcall AspectMenuItemClick(TObject *Sender);
	void __fastcall BinLUTMenuItemClick(TObject *Sender);
	void __fastcall EditBinLUTMenuItemClick(TObject *Sender);
	void __fastcall ProjectLutMenuItemClick(TObject *Sender);
	void __fastcall ClipLutMenuItemClick(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
//
//	////TODO
//	////#ifndef _WIN64
	void __fastcall FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
		TPoint &MousePos, bool &Handled);
	void __fastcall DisplayPanelMouseEnter(TObject *Sender);
	void __fastcall DisplayPanelMouseLeave(TObject *Sender);
	////#else
	////	void __fastcall FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDe//lta, TPoint MousePos, bool &Handled);
	////#endif
	void __fastcall RefreshDisplayWindow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BookmarkEventViewerMenuItemClick(TObject *Sender);
	void __fastcall RibbonTimerTimer(TObject *Sender);
   void __fastcall ResizeHackTimerTimer(TObject *Sender);
   void __fastcall DummyFocusTrackBarEnter(TObject *Sender);
   void __fastcall DummyFocusTrackBarExit(TObject *Sender);
   void __fastcall DisplayPanelEnter(TObject *Sender);
   void __fastcall DisplayPanelExit(TObject *Sender);
	void __fastcall LicenseInformation1Click(TObject *Sender);
	void __fastcall ActivateLicense1Click(TObject *Sender);
   void __fastcall RefreshFrameHackTimerTimer(TObject *Sender);
   void __fastcall TimelineFrameResize(TObject *Sender);
   void __fastcall MainWindowBottomPanelResize(TObject *Sender);
   void __fastcall MainWindowTopPanelResize(TObject *Sender);
   void __fastcall MainWindowTopPanelConstrainedResize(TObject *Sender, int &MinWidth,
          int &MinHeight, int &MaxWidth, int &MaxHeight);
   void __fastcall TopPanelPaintBoxMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
   void __fastcall TopPanelPaintBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
   void __fastcall TopPanelPaintBoxMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
   void __fastcall TopPanelSplitterPaint(TObject *Sender);
   void __fastcall TopPanelPaintBoxPaint(TObject *Sender);
   void __fastcall EditLutMenuItemClick(TObject *Sender);
   void __fastcall UserManualMenuItemClick(TObject *Sender);
	void __fastcall DisplayPanelDblClick(TObject *Sender);
   void __fastcall TimeDisplayDefaultModeMenuItemClick(TObject *Sender);
   void __fastcall FpsMenuItemClick(TObject *Sender);
   void __fastcall TimeDisplayMenuItenClick(TObject *Sender);
   void __fastcall SelectClipMenuItemClick(TObject *Sender);
   void __fastcall ExportFullClipClick(TObject *Sender);
   void __fastcall ExportMarkedRangeMenuItemClick(TObject *Sender);
   void __fastcall ExportVersionClipFilesMenuItemClick(TObject *Sender);
	void __fastcall FrameCompareLabelUpdateTimerTimer(TObject *Sender);
	void __fastcall MaskComputeTimerTimer(TObject *Sender);
	void __fastcall GlobalHotKeysHelpMenuItemClick(TObject *Sender);
   void __fastcall RelinkMediaFolderButtonClick(TObject *Sender);
	void __fastcall FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight, bool &Resize);
	void __fastcall FormConstrainedResize(TObject *Sender, int &MinWidth, int &MinHeight,
          int &MaxWidth, int &MaxHeight);
   void __fastcall ReleaseNotes1Click(TObject *Sender);
	void __fastcall DisplayPanelResize(TObject *Sender);
	void __fastcall TaskbarAutohideHackTimerTimer(TObject *Sender);


protected:
	virtual void StartForm(void);
	virtual void BeforeMainMenuDropDown(void);
#define CB_FIRE_REFRESH_TIMER (WM_APP + 0x2000)
	void _fastcall CBFireRefreshTimer(TMessage &msg);
	void _fastcall SCMinimizeCommand(TMessage &Message);
   void __fastcall WMMenuChar(TWMMenuChar &Message);

	DEFINE_ERROR_HANDLER(mainWindow)
   BEGIN_MESSAGE_MAP
      DEFINE_ERROR_MESSAGE_HANDLER

		// NO IDEA WHY THE NEXT LINE IS COMMENTED OUT!
		// VCL_MESSAGE_HANDLER(WM_ERASEBKGND , TMessage, WMEraseBkgnd)

		// Added to refresh the current frame from a background thread
		VCL_MESSAGE_HANDLER(CB_FIRE_REFRESH_TIMER, TMessage, CBFireRefreshTimer)

      // Added to suppress beep for Alt-<char> hotkeys
      // I didn't think I'd need this, but just declaring WMMenuChar method
      // doesn't seem to override the base class, so never got called.
      VCL_MESSAGE_HANDLER(WM_MENUCHAR, TWMMenuChar, WMMenuChar)

	END_MESSAGE_MAP(TMTIForm)

		virtual bool CreateDeferredForm(const string &Name);

private: // User declarations

	// device context and render context
	HDC screenDC = nullptr;
	HGLRC renderRC = nullptr;

	// security crap
	bool licenseIsOK;

	// for positive frame display after gray bgnd
	// or sequence of frames to video display ends
	// int frameCyclesElapsed;
	// int frameCntEnable;
	bool forceFrameDisplayPendingFlag;
	CHRTimer forceFrameDisplayPendingTimer;

	int actualFPSModulus;

	int savedLeft, savedTop, savedWidth, savedHeight;

	// ------------- toolbar save variables ---------------------------
	bool savedMaskVisible;
	bool savedMaskFloating;
	int savedMaskLeft, savedMaskTop;

	bool savedPanVisible;
	bool savedPanFloating;
	int savedPanLeft, savedPanTop;

	bool savedRGBVisible;
	bool savedRGBFloating;
	int savedRGBLeft, savedRGBTop;

	bool savedComponentsVisible;
	bool savedComponentsFloating;
	int savedComponentsLeft, savedComponentsTop;

	bool savedReticleVisible;
	bool savedReticleFloating;
	int savedReticleLeft, savedReticleTop;
	// ----------------------------------------------------------------

	CPlayer *clipPlayer;
	CDisplayer *clipDisplayer;
	CNavCurrentClip * currentClipHandler;
	CRenderDestinationClip *renderDestinationClip;

	CNavBinMgrGUIInterface *binMgrGUIInterface;

	int savedMouseX, savedMouseY;
	int componentCount;
	string componentAbbrevStr[MAX_COMPONENT_COUNT];

	void __fastcall toolReportStatus(TObject *Sender);

	bool WriteRestartProperties(void);
	bool ReadRestartProperties(void);
	void SetDefaultWindowSizes(void);

	bool RefreshTimerCallback();
	void UpdateSomeGUIStuff();
	void UpdateFramingViewStatus();
	void UpdateFPSActual();

	void RunImportExportDialog(ImportExportFormBase *form);

	void InitComponentValuesDisplay();

	int YComp;
	int UComp;
	int VComp;

	int mouseOrientation;
	int toolWindowIsActive;

	// for VIEW > LUT menu
	StringList UserLutMenuItemCaptionList;

	// PDL hack for Dewarp
	bool loadingPDLEntry;

	int GoToPDLEntryHack(CPDLEntry &pdlEntry);

	// Remember the shift state whenever it is passed to us in an event
	TShiftState m_savedShiftState;

	// Main/Full window autofocus hack
	bool autofocusHackFlag;

	// for only repainting main window if toolbar dock HEIGHT changes, not width
	int ToolbarDockOldHeight;

	// THE lut settings manager
	CLutSettingsManager *lutSettingsManager;

	// Frame cache serial number (for detecting when there are changes)
	int FrameCachePollSerial;

	bool mouseIsInDisplayPanel;

	void __fastcall AppOnActivate(TObject *Sender);
	void __fastcall AppOnDeactivate(TObject *Sender);

	TRect GetBoundsRectOfLargestMonitor();
	void showTimecodeEditor(int funcChar);

   bool _isNormalWindowMaximized = false;
	TRect _unmaximizedNormalWindowBounds;
   bool _needToRemaximizeWhenReturningToNormalWindow = false;
   bool _needToChangeWindowBoundsRect = false;
	TRect _newWindowBoundsRect;

    bool restoreRibbonAfterPlaying;
	void RestoreParentsToToolbarPanel();

   CMTIBitmap TopPanelOffScreenBitmap;

	void ActivateLicense();
	void ActivateLicenseNoWait();
	void UpdateProductAndVersion(void);
	string m_product;

	string m_licenseBaseName;
	int m_majorVersion;
	int m_minorVersion;
	int m_revision;

	MtiLicenseLib *_mtiLicense = nullptr;

	bool m_capturedMouse = false;
	int m_MouseX;
	int m_MouseY;
	TMouseButton m_Button;
	TShiftState m_Shift;
	bool m_doubleClick = false;
   bool _wasNotPlaying = true;
   bool _oldDisplayMaskBorder = true;
   bool _gpuWarning = true;

   int launchAutoUpgrade();

public: // User declarations

	__fastcall TmainWindow(TComponent* Owner);
	__fastcall ~TmainWindow(void);

   void UnitTestCode();
   void SetGpuWarning(bool on) { _gpuWarning = on; }
   bool GetGpuWarning() const { return _gpuWarning; }

	void initializeTheDisplayer();

	MtiLicenseLib *GetMtiLicense()
   {
   	return _mtiLicense;
   };

	bool IsFirstStartup();
	bool PreflightChecklist();
   void OpenClip(ClipIdentifier clipId);
	void OpenClipByName(String FileName);
   void OpenClipByName(const string &fileName);
	TTimelineFrame * GetTimeline();
	CPlayer *GetPlayer();
	void LoadNextClip();
	void LoadPreviousClip();
	void ToggleBetweenLastTwoClips();
	void LoadNextVersionClip();
	void LoadPreviousVersionClip();
   ClipIdentifier UnloadMasterOrVersionClips(ClipIdentifier clipId);
   bool UnloadClipIfCurrent(const string& clipFileName);
   void RelinkClipMedia(ClipIdentifier clipId);
	CDisplayer *GetDisplayer();
	CNavCurrentClip * GetCurrentClipHandler();
	string GetMasterClipFolder();
	CRenderDestinationClip* GetRenderDestinationClip();
	void InitRenderDestinationClip(int);
	void UpdateSliderAndTimecode();
	void UpdateWindowCaption();
	void UpdateComponentValuesToolbar(bool enabled);
	void SaveSAP();
	void RestoreSAP();
	void ToFullScreen();
	void ToNormalWindow();
	void ToPresentationMode();
	void SaveToolbars(TForm *form, const string &sectionNamePrefix);
	void RestoreToolbars(TForm *form, const string &sectionNamePrefix);

	void SetCursor(int type);
	bool SetAltImageFolderScanMethod(bool onOffFlag);
   void SetSwapWAndShiftWFlag(bool swapEm);
   bool GetSwapWAndShiftWFlag();
	int SetFrameCacheSize(size_t sizeInPages, size_t *actualNumberOfPagesAllocated = nullptr);
	void LockCacheFramesInMemory(bool flag);
	void ClearFrameCache();
	const IntegerList &GetCachedFrameList();
	size_t GetSystemPageSizeInBytes();
	int GetImageBodyReadDimensions(int &pixelsPerLine, int &linesPerFrame);
	int GetFrameCacheSizeInFrames();

	DEFINE_CBHOOK(SetFrameCacheHints, TmainWindow);
   DEFINE_CBHOOK(OnFrameWasDiscardedOrCommitted, TmainWindow);
   int CanWeDiscardOrCommit(bool discardFlag, const std::pair<int, int> &range, std::string &message);
   DEFINE_CBHOOK(OnClipWasDeleted, TmainWindow);

	void SetMouseOrientation(int type);
	int GetMouseOrientation();
	void informToolWindowStatus(bool windowIsActive);

	void ShowBinManager(bool show);
	void ShowTool(int newToolIndex);
	void HideTool(int toolIndex);
	void HideAllTools(int dontHideThisTool);
	bool HideAllToolsQuery();
	void ShowActiveToolName();
   void SendSwitchSubtoolCmdToActiveTool();

	// PDL & Rendering
	int GoToPDLEntry(CPDLEntry &pdlEntry);
	bool IsPDLEntryLoading();

	// Mask Tool
	void UpdateMaskToolGUI(CMaskTool &maskTool);
	void GatherMaskToolSettings(CMaskTool &maskTool);

	// Reticle Tool
	void ToggleReticleBits(int reticleBitMask);
	void UpdateReticleToolbarModeButtons();
	void UpdateReticleToolbarComboBox();
	void ReadReticleListIni();

	// Pixel Aspect Ratio
	void UpdateFrameAspectRatio(int);

	virtual bool WriteSettings(CIniFile *ini, const string &IniSection);
	// Writes the configuration to the file
	virtual bool ReadSettings(CIniFile *ini, const string &IniSection); // Reads configuration.

	// Callback hooks
	CBHook ClipHasChanged; // Used to notify on clip changes
	CBHook FrameCacheHasChanged;

	// YUV component value accessor methods
	int GetYcomp();
	int GetUcomp();
	int GetVcomp();
	int getBitsPerComponent();
	EPixelComponents getPixelComponents();
	bool isPointInFrame(POINT pt);
	bool isMouseInFrame();
	void forceMouseToWindowCenter();
	void captureMouse();
	void releaseMouse();
   bool isMouseCaptured() {return m_capturedMouse;};

	// Toolbar stuff
	void UpdateRGBChannelsToolbarButtons();
	void UpdateJustificationToolbarButtons();
   void LeftJustifyTheDisplay();
   void CenterJustifyTheDisplay();
	void RightJustifyTheDisplay();
	void ToggleDualFrameCompareMode();
	void ToggleFrameWipeCompareMode();
	void ToggleShowFrameCompareClipNames();
	void SetFrameCompareClipNames(const string &leftClipPathName, const string &rightClipPathName);
	void UpdateFrameCompareToolbarButtons();
	void SetColorChannelMask(int mask);
   int  GetColorChannelMask();
   void ShowAllColorChannels();
   void ShowOrToggleColorChannel(int singleChannelMask);
   void ToggleGrayDisplay();
   void ToggleAlphaDisplay();
   int  GetReticleMode();
   void SetReticleMode(int reticleBitMask);
   void EditReticles();
	void ShowAllReticles();
	void LoadReticlesByName(string reticlesName);
	void CycleToNextViewMode();
   void SetPanMode(int newPanMode);
   void EditLutSettings();
   void SelectLut(int lutListIndex);
//   string GetAutoCleanDebrisFileName(int frameIndex);
//   CAutoCleanDebrisFile *GetAutoCleanDebrisFile(int frameIndex);
//   int GetAutoCleanDebrisVersion();

	// Update status bar panels
	void UpdateFPSViewStatus(int);
	void UpdateViewingModeStatus();
	void UpdateZoomLevelStatus();
	void UpdatePlaybackFilterStatus();

	// LUT stuff
	void LoadAndApplyLutSettings(ClipSharedPtr &clip);
#define NUMBER_OF_HARDWIRED_LUT_MENU_ENTRIES 6
	void UpdateUserLutMenuItemCaptionList();
	void AddUserLutsToLutToolbar(TComboBox *lutComboBox);
	void AddUserLutsToLutMenu();

#ifndef NO_LICENSING

	// FlexLM private variables
	string _StartupFailedMessage;
	CRsrcCtrl rsrcCtrl;
#endif

	MTI_UINT32 **GetFeatureArray();

	// copy a string to the Windows Clipboard
	void SendToClipboard(const string &str);

	// Multiple SAP hack
	string GetIniSection(void)
	{
		return *DefaultIniSection;
	};

	// Presentation mode hack
	MainWindowState _mainWindowState;

	TForm *OldActiveWindow;

	void HideAllAuxiliaryWindows();

	// Main/full window autofocus hack
	void SetAutofocus(bool onoff)
	{
		autofocusHackFlag = onoff;
	};

	// Preferences
	int GetSlowDisplaySpeedPreference();
	int GetMediumDisplaySpeedPreference();
	int GetFastDisplaySpeedPreference();
	EPixelComponents GetRemoteDisplayFormatPreference();
	int GetGammaPreference();
	// int GetLUTPreference();

	// Menu shortcut control
	void SetMenuShortcutsAreOkNow(bool yesNoFlag);

	// Prevent RefreshTimerTImer reentry damage
	bool inRefreshTimerCallback;

	// Used to limit how often the status bar is updated
	CHRTimer CVDTimer;

	// Lut settings stuff
	CLutSettingsManager *GetLutSettingsManager();
	DEFINE_CBHOOK(LutSettingsHaveChanged, TmainWindow);

	// Loupe stuff
	void ToggleLoupe();
	void ShowLoupe();
	void HideLoupe();
	bool IsLoupeVisible();
	void MoveLoupe(int screenX, int screenY);
	int GetLoupeMagnification();
	void SetLoupeMagnification(int newMag);
	void ToggleLoupeCrosshairs();
	void SetLoupeSettingMode(bool flag);
	void HandleLoupeSettingKey(int Key);
	bool didALoupeSettingChange();

	POINT oldLoupePos;
	int loupeMagnification;
	bool inLoupeSettingMode;
	bool aLoupeSettingChanged;

	// Use FocusOnMainWindow() instead of mainWindow->SetFocus()!
	void FocusOnMainWindow();

	void SaveTimelinePositions();
	void RestoreTimelinePositions();

	// for RETICLE toolbar combo box
#define RETINIFILE      "$(CPMP_USER_DIR)RETICLEList.ini"

	static string reticleUnnamed;
	static string reticleSectionName;
	static string defaultReticle[16];

	// For scratch sidecar overlay window positioning
	RECT GetUsableClientRect(void);
	RECT GetUsableClientRectInScreenCoords(void);
	RECT GetStretchRectangleScreenCoords(void);
   RECT getImageRectangle(void);

	// For metadata managers
	string GetSanitizedTimecodeStringFromFrameIndex(int frameIndex);
	CTimecode GetCurrentlyDisplayedTimecode();
	int GetCurrentFrameIndex();

	// More hacks
	void ShowCursor(bool showFlag);
	void ExecuteNavigatorToolCommand(int command);
   void GoToFrame(PTimecode tcP, const string &preconformedTcString);

	void ToggleShowTimecodeOverlay();
	void ToggleShowTimelineRibbonOverlay();

	void SetTopPanelVisibility(bool onOff);
   bool GetTopPanelVisibility();
   CMTIBitmap &GetBitmapRefForTopPanelDrawing();
   void DrawBitmapOnTopPanel();
	bool inTopPanelDragMode;

	void ShowFrameBufferErrorMessage(const MediaFrameBuffer::ErrorInfo &errorInfo);
	void RepositionFrameBufferErrorMessage();
	void HideFrameBufferErrorMessage();

	bool _showFrameCompareClipNames = true;
	RECT _oldUsableClientRect = {0};
	RECT _oldLeftFrameRect = {0};
	RECT _oldRightFrameRect = {0};
	DEFINE_CBHOOK(DisplayerRectanglesChangedCB, TmainWindow);
	void UpdateFrameCompareLabelPositions();

   bool swapWAndShiftWFlag;

   void RefreshBinManagerClipList();

   bool _oldBaseToolWindowVisible = false;

  //// DEFINE_FILE_SWEEP_CB(FindDebrisVersionCB, TmainWindow);
};

// ---------------------------------------------------------------------------
extern PACKAGE TmainWindow *mainWindow;
// ---------------------------------------------------------------------------

#define TOOLBAR_INI_FILE          "$(CPMP_USER_DIR)toolbar.ini"
#define MAIN_WINDOW_PREFIX        "MainWindow"
#define FULL_SCREEN_PREFIX        "FullScreen"
#define TILE_SCREEN_PREFIX        "TileScreen"
#define TOOLBAR_CORRAL_PREFIX     "ToolbarCorral"

// ---------------------------------------------------------------------------

#endif // MainWindowUnitH
