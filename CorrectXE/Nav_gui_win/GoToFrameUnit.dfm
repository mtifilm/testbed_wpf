object GoToFrameForm: TGoToFrameForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Go to frame'
  ClientHeight = 96
  ClientWidth = 165
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CurrentTCVTimecodeEdit: VTimeCodeEdit
    Left = 20
    Top = 17
    Width = 126
    Height = 30
    AutoSelect = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnEnterKey = CurrentTCVTimecodeEditEnterKey
    OnEscapeKey = CurrentTCVTimecodeEditEscapeKey
  end
  object OkButton: TButton
    Left = 20
    Top = 60
    Width = 53
    Height = 21
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 93
    Top = 60
    Width = 53
    Height = 21
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
