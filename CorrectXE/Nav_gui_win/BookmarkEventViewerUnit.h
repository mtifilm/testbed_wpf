//---------------------------------------------------------------------------

#ifndef BookmarkEventViewerUnitH
#define BookmarkEventViewerUnitH

//---------------------------------------------------------------------------
#include "BookmarkInfo.h"
#include "MTIUNIT.h"

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "ColorLabel.h"
//---------------------------------------------------------------------------
class TBookmarkEventViewerForm : public TMTIForm
{
__published:	// IDE-managed Components
   TLabel *AddedLabel;
   TLabel *AddedValueLabel;
   TLabeledEdit *ReelNumberLabeledEdit;
   TLabeledEdit *UserLabeledEdit;
   TSpeedButton *MarkInSpeedButton;
   TSpeedButton *MarkOutSpeedButton;
   TLabel *DurLabel;
   TLabel *DurationValueLabel;
   TComboBox *DefectComboBox;
   TLabel *DefectLabel;
   TLabeledEdit *ArtistLabeledEdit;
   TLabeledEdit *CompletedLabeledEdit;
   TLabeledEdit *NoteLabeledEdit;
   TButton *AddButton;
   TButton *DeleteButton;
   TButton *CloseButton;
   TPanel *NavButtonsPanel;
   TSpeedButton *FastForwardButton;
   TSpeedButton *JogBackButton;
   TSpeedButton *JogForwardButton;
   TSpeedButton *PlayButton;
   TSpeedButton *PlayReverseButton;
   TSpeedButton *RewindButton;
   TSpeedButton *StopButton;
   TSpeedButton *ClearMarksButton;
   TSpeedButton *MarkEntireClipButton;
   TSpeedButton *SetShotMarksButton;
   TSpeedButton *SetInMarkButton;
   TSpeedButton *SetMarkOutInclusiveButton;
   TColorLabel *RequiredLabel;
   TSpeedButton *GoToNextEventSpeedButton;
   TSpeedButton *GoToPrevEventSpeedButton;
   TSpeedButton *GoToClipStartSpeedButton;
   TShape *Shape1;
   TSpeedButton *GoToClipEndSpeedButton;
   TTimer *ButtonStateTimer;
   TEdit *DefectDisabledCoverEdit;
   TTimer *DeactivateHackTimer;
   TLabeledEdit *LocationLabeledEdit;
   TLabeledEdit *QcByLabeledEdit;
   TLabeledEdit *QcDateLabeledEdit;
   TBitBtn *CompletedCheckButton;
   TBitBtn *QcDateCheckButton;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall FormDestroy(TObject *Sender);
   void __fastcall FormActivate(TObject *Sender);
   void __fastcall BookmarkItemChange(TObject *Sender);
   void __fastcall SetInMarkButtonClick(TObject *Sender);
   void __fastcall SetMarkOutInclusiveButtonClick(TObject *Sender);
   void __fastcall GoToInSpeedButtonClick(TObject *Sender);
   void __fastcall GoToOutSpeedButtonClick(TObject *Sender);
   void __fastcall AddButtonClick(TObject *Sender);
   void __fastcall DeleteButtonClick(TObject *Sender);
   void __fastcall CloseButtonClick(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall NavBarSpeedButtonClick(TObject *Sender);
   void __fastcall FormClick(TObject *Sender);
   void __fastcall FormHide(TObject *Sender);
   void __fastcall OnEditEnter(TObject *Sender);
   void __fastcall OnEditKeyPress(TObject *Sender, System::WideChar &Key);
   void __fastcall ButtonStateTimerTimer(TObject *Sender);
   void __fastcall CompletedCheckButtonClick(TObject *Sender);
   void __fastcall DefectComboBoxKeyPress(TObject *Sender, System::WideChar &Key);
   void __fastcall DeactivateHackTimerTimer(TObject *Sender);
   void __fastcall GoToPrevEventSpeedButtonClick(TObject *Sender);
   void __fastcall GoToNextEventSpeedButtonClick(TObject *Sender);
   void __fastcall FormDeactivate(TObject *Sender);
   void __fastcall QcDateCheckButtonClick(TObject *Sender);
   void __fastcall UserLabeledEditChange(TObject *Sender);




private:	// User declarations

   BookmarkInfo bookmarkInfo;
   bool updatingGui;
   bool needToSave;
   int selectedBookmarkFrameIndex;
   string defaultUser;
   string defaultReelNumber;
   string defaultDefect;
   bool isDeactivateHackEnabled;

   void EnableControlsBasedOnCurrentState();
   void AutoSave();
   void Save();
   void UpdateGuiFromBookmarkInfo();
   void UpdateBookmarkInfoFromGui();
   void Unfocus();
   static void ExecuteNavigatorToolCommand(int command);

   DEFINE_CBHOOK(OnBookmarkSelectionChange, TBookmarkEventViewerForm);
   DEFINE_CBHOOK(OnBookmarkAdded, TBookmarkEventViewerForm);
   DEFINE_CBHOOK(OnSelectedBookmarkRangeEnteredOrExited, TBookmarkEventViewerForm);

public:		// User declarations

   __fastcall TBookmarkEventViewerForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBookmarkEventViewerForm *BookmarkEventViewerForm;
//---------------------------------------------------------------------------
#endif
