//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LoupeWindowUnit.h"

#include "ColorSpaceConvert.h"
#include "Displayer.h"
#include "ImageFormat3.h"
#include "MainWindowUnit.h"
#include "Magnifier.h"
#include "Player.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TLoupeWindow *LoupeWindow;
//---------------------------------------------------------------------------



__fastcall TLoupeWindow::TLoupeWindow(TComponent* Owner)
    : TForm(Owner), mainWindow(NULL), magnifier(new CMagnifier),
      currentSrcSize(-1), currentMagFactor(1.0F),
      toRGBColorConverter(new CColorSpaceConvert),
      currentClipImageFormat(NULL), internalImageFormat(new CImageFormat),
      showCrosshairs(true)
{
   LoupeImage = NULL;
   ControlStyle << csOpaque;
   LoupeImagePanel->ControlStyle << csOpaque;
}
//---------------------------------------------------------------------------

__fastcall TLoupeWindow::~TLoupeWindow(void)
{
   // Ha this isn't going to work if we get deleted AFTER the displayer!
   // Let's just forget about it
   ///if (magnifier != NULL && mainWindow != NULL)
   ///{
      ///mainWindow->GetDisplayer()->freeMagnifier(magnifier);
   ///}
   delete toRGBColorConverter;
   delete internalImageFormat;
   delete LoupeImage;
}
//---------------------------------------------------------------------------

void TLoupeWindow::init(TmainWindow *newMainWindow)
{
   mainWindow = newMainWindow;

      // TODO: FIX THIS TO NOT USE A CONST CAST!!
   currentClipImageFormat = const_cast<CImageFormat *> (
                            mainWindow->GetPlayer()->getVideoClipImageFormat()
                            );
   if (currentClipImageFormat != NULL)
   {
      // Set up the intermediate image format; start with a copy of
      // the current clip's format to set dimensions and ranges

      *internalImageFormat = *currentClipImageFormat;
      internalImageFormat->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
      internalImageFormat->setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

      // Get the active picture rect
      ///activeRect = currentClipImageFormat->getActivePictureRect();

      // Initialize color space converter
      toRGBColorConverter->Init(currentClipImageFormat, internalImageFormat);
   }
}
//---------------------------------------------------------------------------

void TLoupeWindow::activate(int srcSize, int magFactor)
{
   if (mainWindow == NULL) return;

   if (magnifier == NULL
   || srcSize != currentSrcSize
   || magFactor != currentMagFactor)
   {
      if (magnifier != NULL)
      {
         mainWindow->GetDisplayer()->freeMagnifier(magnifier);
         magnifier = NULL;
      }

      currentSrcSize = srcSize;
      currentMagFactor = magFactor;

      RECT magRect;
      magRect.left   =  0;
      magRect.top    =  0;
      magRect.right  = srcSize - 1;  // -1 because inclusive rect
      magRect.bottom = srcSize - 1;  // -1 because inclusive rect
      int magnifiedSize = srcSize * magFactor;

      magnifier = mainWindow->GetDisplayer()-> allocateMagnifier(magnifiedSize,
                                                                 magnifiedSize,
                                                                 &magRect);
   }

   Visible = true;

   if ((magnifier != NULL) && (!magnifier->isActive()))
   {
      magnifier->
        activateMagnifier(TLoupeWindow::imageMagnifierRefreshCallback,this);
   }

   // Stupid hack timer to remove the loupe when the main window is moved,
   // because I don't feel like tracking the main window's movements
   oldMainWindowLeft = mainWindow->Left;
   oldMainWindowTop = mainWindow->Top;
   ParentCheckTimer->Enabled = true;

   // Do NOT show the red box
   magnifier->hide();
}
//---------------------------------------------------------------------------

void TLoupeWindow::deactivate(void)
{
   if (mainWindow == NULL) return;
   if (magnifier == NULL) return;

   if (magnifier->isActive())
   {
      magnifier->deactivateMagnifier();
   }

   Visible = false;

   // We never show it
   ///magnifier->hide();
}
//---------------------------------------------------------------------------

void TLoupeWindow::toggleCrosshairs()
{
   showCrosshairs = !showCrosshairs;
}
//---------------------------------------------------------------------------

CMagnifier* TLoupeWindow::getMagnifier()
{
   return magnifier;
}
//---------------------------------------------------------------------------

// the callback for refreshing the magnified image (OriginalImage)
void TLoupeWindow::imageMagnifierRefreshCallback(void *obj, void *mag)
{
   LoupeWindow->imageMagnifierRefresh(mag);
}
//---------------------------------------------------------------------------

void TLoupeWindow::imageMagnifierRefresh(void *mag)
{
   if (!Visible)
      return;

   if ((magnifier == NULL)
   || (!magnifier->isActive()))
   {
      LoupeImagePanel->Color = clGray;
      return;
   }

   CMagnifier *magnifier = static_cast<CMagnifier *>(mag);
   RECT magRect = magnifier->getMagnifierWindow();

#if USE_MAGNIFIER_INTERNAL_FORMAT

	MTI_UINT16 *srcPixels = magnifier->getIntermediateFrameBuffer();
	if (srcPixels == NULL)
		return;

	TRGBTriple q;
	int imageSize = currentSrcSize * currentMagFactor;

	Graphics::TBitmap *bitmap = new Graphics::TBitmap;
	bitmap->PixelFormat = pf24bit;
	bitmap->Height = imageSize;
	bitmap->Width = imageSize;

	const int numberOfComponentsShouldNOTBeHardwired = 3;
	int srcWidth = currentSrcSize;
	int srcHeight = currentSrcSize;
	int srcPitch = srcWidth * numberOfComponentsShouldNOTBeHardwired;

	// Ensure reasonable dynamic luminance range
	int shiftShouldNotBeHardcoded = 2;    // hardcoded to 10 bits !!!! QQQ

	// Establish the current display LUT
	MTI_UINT8 displayLut[768];
	mainWindow->GetDisplayer()->GetDisplayLutValues(displayLut);
	MTI_UINT8 *rLut = &displayLut[0];
	MTI_UINT8 *gLut = &displayLut[256];
	MTI_UINT8 *bLut = &displayLut[512];

	// Frickin' MAGNIFIER doesn't actually MAGNIFY!! We have to do it ourself!!!
	for (int srcRow = 0; srcRow < srcHeight; ++srcRow)
	{
		MTI_UINT16 *srcPtr = srcPixels + (srcRow * srcPitch);
		TRGBTriple *loupePtr;
      for (int srcCol = 0; srcCol < srcWidth; ++srcCol)
      {
			MTI_UINT16 r, g, b;

         if (toRGBColorConverter != NULL)
			{
				// Not sure what this conversion is for, since I thought the
				// magnifier already did this
				CColorSpaceConvert *clrcvt = toRGBColorConverter;
            toRGBColorConverter->ConvertOneValue(srcPtr[0], srcPtr[1], srcPtr[2],
												&r, &g, &b);
			}
         else
			{
				r = srcPtr[0];
            g = srcPtr[1];
            b = srcPtr[2];
			}

         // Clamped scaling - don't know why Convert is giving me values
         // larger than the maximum
			r >>= shiftShouldNotBeHardcoded;
			r   = (r > 0xFF)? 0xFF : r;
         g >>= shiftShouldNotBeHardcoded;
			g = (g > 0xFF)? 0xFF : g;
			b >>= shiftShouldNotBeHardcoded;
         b  = (b > 0xFF)? 0xFF : b;

			// Perform the current display LUT conversion
			r = rLut[r];
         g = gLut[g];
			b = bLut[b];

			// HACK - we've made black transparent for the window, so make sure
			// none of the data is exactly black (set blue to 1)
			if (r == 0 && g == 0 && b == 0)
            b = 1;

			// Build the pixel quad
         q.rgbtRed   = r;
         q.rgbtGreen = g;
			q.rgbtBlue  = b;

         for (int dstRow = (srcRow*currentMagFactor); dstRow < ((srcRow+1)*currentMagFactor); ++dstRow)
			{
            loupePtr = (TRGBTriple *)bitmap->ScanLine[dstRow];
            for (int dstCol = (srcCol*currentMagFactor); dstCol < ((srcCol+1)*currentMagFactor); ++dstCol)
				{
               loupePtr[dstCol] = q;
				}
			}
			srcPtr += numberOfComponentsShouldNOTBeHardwired;
		}
	}

#else

	MTI_UINT8 *magPixels = (MTI_UINT8 *)magnifier->getMagnifierFrameBuffer();
	if (magPixels == NULL)
	{
		return;
   }

	TRGBTriple q;
	int imageSize = currentSrcSize * currentMagFactor;

	Graphics::TBitmap *bitmap = new Graphics::TBitmap;
	bitmap->PixelFormat = pf24bit;
	bitmap->Height = imageSize;
	bitmap->Width = imageSize;

	for (int row = 0; row < imageSize; ++ row)
	{
		TRGBTriple *loupePtr = (TRGBTriple *)bitmap->ScanLine[row];
		for (int col = 0; col < imageSize; ++col)
		{
			// Magnifier returns 8-bit BGRA. Don't allow 0, 0, 0 because that's transparent!
			int magPixelsIndex = 4 * ((row * imageSize) + col);
			MTI_UINT8 r = magPixels[magPixelsIndex + 2];
			MTI_UINT8 g = magPixels[magPixelsIndex + 1];
			MTI_UINT8 b = magPixels[magPixelsIndex + 0];
			q.rgbtRed   = (r == 0) ? 1 : r;
			q.rgbtGreen = (g == 0) ? 1 : g;
			q.rgbtBlue  = (b == 0) ? 1 : b;
			loupePtr[col] = q;
		}
	}

#endif

	int imageOffset = 4;

	LoupeImagePanel->Left = 0;
	LoupeImagePanel->Top = 0;
	LoupeImagePanel->Width = imageSize + 2 * imageOffset;
	LoupeImagePanel->Height = LoupeImagePanel->Width;

   delete LoupeImage;
   LoupeImage = new TImage(this);
   LoupeImage->Parent = LoupeImagePanel;
   LoupeImage->ControlStyle << csOpaque;

   LoupeImage->Left = imageOffset - 1;        // Not sure why it's off by one
   LoupeImage->Top = imageOffset - 1;         // Not sure why it's off by one
   LoupeImage->Width = imageSize;
   LoupeImage->Height = LoupeImage->Width;

   TCanvas *loupeCanvas = LoupeImage->Canvas;
   loupeCanvas->CopyMode = cmSrcCopy;
   loupeCanvas->Draw(0, 0, bitmap);

   delete bitmap;

   if (showCrosshairs)
   {
      loupeCanvas->Brush->Style = bsClear;
      loupeCanvas->Pen->Width = 1;
      loupeCanvas->Pen->Mode = pmCopy;
      loupeCanvas->Pen->Style = psSolid;
      int halfSide = imageSize / 2;
      int quarterSide = 5 * imageSize / 16;  // 1/4 was too short, 5/16 instead
      int threeQuarterSide = 11 * imageSize / 16;  // really 11/16
      int fullSide = imageSize - 1;

      for (int i = 0; i <= 1; ++i)
      {
         if (i == 0)
         {
            loupeCanvas->Pen->Color = clLtGray;
         }
         else
         {
            loupeCanvas->Pen->Color = clDkGray;
         }

         int middle = halfSide + i;

         loupeCanvas->MoveTo(middle, 0);
         loupeCanvas->LineTo(middle, quarterSide);
         loupeCanvas->MoveTo(middle, threeQuarterSide);
         loupeCanvas->LineTo(middle, fullSide);

         loupeCanvas->MoveTo(0, middle);
         loupeCanvas->LineTo(quarterSide, middle);
         loupeCanvas->MoveTo(threeQuarterSide, middle);
         loupeCanvas->LineTo(fullSide, middle);
      }
   }

   // Force the drawing to be seen immediately
   LoupeImage->Update();

   ///::SelectClipRgn(loupeCanvas->Handle, NULL);
   ///::DeleteObject(MyRgn);
}
//---------------------------------------------------------------------------

void __fastcall TLoupeWindow::FormActivate(TObject *Sender)
{
   // Never want the loupe to have the focus;
   // apparently it's OK to change the focus window from inside an Activate.
   mainWindow->FocusOnMainWindow();
}
//---------------------------------------------------------------------------

void __fastcall TLoupeWindow::ParentCheckTimerTimer(TObject *Sender)
{
   if (Visible)
   {
      if (mainWindow->Left != oldMainWindowLeft
      || mainWindow->Top != oldMainWindowTop)
      {
         deactivate();
      }
   }

   if (!Visible)
   {
      ParentCheckTimer->Enabled = false;
   }
}
//---------------------------------------------------------------------------

