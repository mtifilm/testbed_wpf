inherited PreferencesDlg: TPreferencesDlg
  Left = 469
  Top = 414
  BorderIcons = [biHelp]
  BorderStyle = bsToolWindow
  Caption = 'DRS Player Preferences'
  ClientHeight = 266
  ClientWidth = 469
  Font.Height = -11
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  ShowHint = True
  OnCreate = FormCreate
  ExplicitWidth = 475
  ExplicitHeight = 295
  PixelsPerInch = 96
  TextHeight = 13
  object SaveButton: TButton
    Left = 18
    Top = 220
    Width = 73
    Height = 25
    Hint = 'Makes these settings the startup settings'
    Caption = 'Save Startup'
    TabOrder = 0
    OnClick = SaveButtonClick
  end
  object PreferenceControl: TPageControl
    Left = 4
    Top = 8
    Width = 459
    Height = 247
    ActivePage = GpuSheet
    TabOrder = 3
    object GeneralSheet: TTabSheet
      Caption = 'General'
      ParentShowHint = False
      ShowHint = True
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object BGPanel6: TPanel
        Left = -11
        Top = 0
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        object Label1: TLabel
          Left = 71
          Top = 152
          Width = 93
          Height = 13
          Caption = 'All warnings dialogs'
        end
        object Label2: TLabel
          Left = 40
          Top = 102
          Width = 288
          Height = 13
          Caption = 'Original Values.  When unchecked, the behavior is opposite.'
        end
        object Label3: TLabel
          Left = 40
          Top = 87
          Width = 389
          Height = 13
          Caption = 
            'When checked, the W key shortcut shows blue history boxes, and S' +
            'hift W shows'
        end
        object ToolTipsDurationLabel: TLabel
          Left = 263
          Top = 8
          Width = 86
          Height = 13
          Caption = 'Tool Tips Duration'
        end
        object ToolTipsOffLabel: TLabel
          Left = 265
          Top = 50
          Width = 16
          Height = 13
          Caption = 'Off'
        end
        object ToolTipsDUrationLongLabel: TLabel
          Left = 392
          Top = 50
          Width = 23
          Height = 13
          Caption = 'Long'
        end
        object ToolTipsDurationShortLabel: TLabel
          Left = 302
          Top = 50
          Width = 26
          Height = 13
          Caption = 'Short'
        end
        object Label4: TLabel
          Left = 345
          Top = 50
          Width = 36
          Height = 13
          Caption = 'Medium'
        end
        object CursorCBox: TCheckBox
          Left = 24
          Top = 69
          Width = 153
          Height = 17
          Hint = 
            'Checking this makes Navigator Cursor an Arrow, the default is Cr' +
            'osshairs'
          Caption = 'Make cursor an arrow'
          TabOrder = 0
          OnClick = CursorCBoxClick
        end
        object AutoSaveGroupBox: TGroupBox
          Left = 11
          Top = 8
          Width = 213
          Height = 55
          Caption = ' Autosave '
          TabOrder = 1
          object AutoSaveSAPCBox: TCheckBox
            Left = 16
            Top = 16
            Width = 185
            Height = 17
            Hint = 'Automatically save the window size and positions on exit.'
            Caption = 'Window Size and Positions'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = ChangeClick
          end
          object AutoSavePreferencesCBox: TCheckBox
            Left = 16
            Top = 32
            Width = 153
            Height = 17
            Hint = 'Automatically saves the program parameters on exit'
            Caption = 'Program Preferences'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = ChangeClick
          end
        end
        object SwapWAndShiftWCBox: TCheckBox
          Left = 24
          Top = 92
          Width = 17
          Height = 17
          TabOrder = 2
          OnClick = SwapWAndShiftWCBoxClick
        end
        object ResetWarningsButton: TButton
          Left = 24
          Top = 147
          Width = 41
          Height = 25
          Caption = 'Reset'
          TabOrder = 3
          OnClick = ResetWarningsButtonClick
        end
        object ToolTipsDurationTrackBar: TTrackBar
          Left = 263
          Top = 27
          Width = 150
          Height = 21
          Hint = 'Tool tips duration'
          Max = 3
          Position = 3
          TabOrder = 4
          TickStyle = tsManual
          OnChange = ToolTipsDurationTrackBarChange
        end
      end
    end
    object DPXCacheSheet: TTabSheet
      Caption = 'Frame Cache'
      ImageIndex = 5
      object BGPanel5: TPanel
        Left = -4
        Top = -3
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        DesignSize = (
          459
          226)
        inline FrameCacheSizeTrackEdit: TTrackEditFrame
          Left = 29
          Top = 46
          Width = 196
          Height = 51
          Anchors = [akTop, akRight]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitLeft = 29
          ExplicitTop = 46
          ExplicitWidth = 196
          inherited MinLabel: TLabel
            Width = 6
            Caption = '0'
            ParentFont = False
            ExplicitWidth = 6
          end
          inherited MaxLabel: TLabel
            Left = 129
            Width = 18
            Caption = '100'
            ParentFont = False
            ExplicitLeft = 129
            ExplicitWidth = 18
          end
          inherited TitleLabel: TColorLabel
            Width = 85
            Caption = 'Desired Size in GB'
            ParentFont = False
            ExplicitWidth = 85
          end
          inherited TrackBar: TTrackBar
            Width = 152
            ExplicitWidth = 152
          end
          inherited TrackBarDisabledPanel: TPanel
            Width = 140
            ExplicitWidth = 140
          end
          inherited EditAlignmentPanel: TPanel
            Left = 156
            ExplicitLeft = 156
            inherited EditDisabledPanel: TPanel
              inherited EditDisabledLabel: TLabel
                Caption = '50'
              end
            end
          end
          inherited NotifyWidget: TCheckBox
            OnClick = CacheSizeTrackEditNotifyWidgetClick
          end
          inherited TrackBarMinPositionTick: TColorPanel
            Width = 4
            ExplicitWidth = 4
          end
          inherited TrackBarMaxPositionTick: TColorPanel
            Width = 4
            ExplicitWidth = 4
          end
        end
        object EnableFrameCacheCheckBox: TCheckBox
          Left = 22
          Top = 14
          Width = 151
          Height = 17
          Caption = 'Enable Frame Cache'
          Checked = True
          State = cbChecked
          TabOrder = 1
          Visible = False
          OnClick = EnableFrameCacheCheckBoxClick
        end
        object StatusGroupBox: TGroupBox
          Left = 22
          Top = 108
          Width = 397
          Height = 65
          Caption = ' Status '
          TabOrder = 2
          object StatusLine1: TLabel
            Left = 11
            Top = 15
            Width = 63
            Height = 13
            Caption = 'Status line 1.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object StatusLine2: TLabel
            Left = 11
            Top = 30
            Width = 63
            Height = 13
            Caption = 'Status line 2.'
          end
          object StatusLine3: TLabel
            Left = 11
            Top = 45
            Width = 63
            Height = 13
            Caption = 'Status line 3.'
          end
        end
        object LockCacheFramesInMemoryCheckBox: TCheckBox
          Left = 266
          Top = 14
          Width = 135
          Height = 17
          Caption = 'Lock frames in memory'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = LockCacheFramesInMemoryCheckBoxClick
        end
        object ClearCacheButton: TButton
          Left = 290
          Top = 58
          Width = 73
          Height = 25
          Caption = 'Clear Cache'
          TabOrder = 4
          OnClick = ClearCacheButtonClick
        end
      end
    end
    object WindowSheet: TTabSheet
      Caption = 'Display Window'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 3
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object BGPanel4: TPanel
        Left = -4
        Top = -3
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        object BorderWidth: TGroupBox
          Left = 68
          Top = 48
          Width = 313
          Height = 74
          Caption = ' Border Width '
          TabOrder = 0
          object BorderWidthValue: TLabel
            Left = 148
            Top = 45
            Width = 11
            Height = 24
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -21
            Font.Name = 'Times New Roman'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BorderWidthSlider: TTrackBar
            Left = 20
            Top = 21
            Width = 273
            Height = 24
            Max = 50
            Frequency = 2
            TabOrder = 0
            ThumbLength = 15
            OnChange = BorderWidthSliderChange
          end
        end
      end
    end
    object SpeedSheet: TTabSheet
      Caption = 'Display Speed'
      ImageIndex = 4
      Constraints.MaxHeight = 219
      Constraints.MaxWidth = 451
      Constraints.MinHeight = 215
      Constraints.MinWidth = 435
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object BGPanel3: TPanel
        Left = -4
        Top = -3
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        object SpeedSettings: TGroupBox
          Left = 68
          Top = 44
          Width = 313
          Height = 61
          Caption = 'Speed Settings'
          TabOrder = 0
          object SlowLabel: TLabel
            Left = 21
            Top = 28
            Width = 22
            Height = 13
            Caption = 'Slow'
          end
          object MediumLabel: TLabel
            Left = 117
            Top = 28
            Width = 36
            Height = 13
            Caption = 'Medium'
          end
          object FastLabel: TLabel
            Left = 231
            Top = 28
            Width = 21
            Height = 13
            Caption = 'Fast'
          end
          inline SlowSpinEdit: TSpinEditFrame
            Left = 48
            Top = 25
            Width = 41
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 48
            ExplicitTop = 25
            ExplicitWidth = 41
            inherited Edit: TEdit
              Width = 27
              ExplicitWidth = 27
            end
            inherited DownButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
            inherited UpButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
          end
          inline MediumSpinEdit: TSpinEditFrame
            Left = 157
            Top = 25
            Width = 41
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ExplicitLeft = 157
            ExplicitTop = 25
            ExplicitWidth = 41
            inherited Edit: TEdit
              Width = 27
              ExplicitWidth = 27
            end
            inherited DownButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
            inherited UpButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
          end
          inline FastSpinEdit: TSpinEditFrame
            Left = 257
            Top = 25
            Width = 41
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = 257
            ExplicitTop = 25
            ExplicitWidth = 41
            inherited Edit: TEdit
              Width = 27
              ExplicitWidth = 27
            end
            inherited DownButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
            inherited UpButtonOutlinePanel: TPanel
              Left = 25
              ExplicitLeft = 25
            end
          end
        end
      end
    end
    object GammaSheet: TTabSheet
      Caption = 'Display LUT'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object BGPanel2: TPanel
        Left = -4
        Top = -3
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        object DefaultGamma: TGroupBox
          Left = 64
          Top = 45
          Width = 313
          Height = 78
          Caption = 'Default Gamma'
          TabOrder = 0
          object GammaValue: TLabel
            Left = 148
            Top = 47
            Width = 27
            Height = 24
            Caption = '1.7'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -21
            Font.Name = 'Times New Roman'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object GammaSlider: TTrackBar
            Left = 20
            Top = 21
            Width = 273
            Height = 24
            Max = 30
            Min = 1
            Position = 17
            TabOrder = 0
            ThumbLength = 15
            OnChange = GammaSliderChange
          end
        end
      end
    end
    object GpuSheet: TTabSheet
      Caption = 'GPU'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object BGPanel1: TPanel
        Left = 0
        Top = -3
        Width = 459
        Height = 226
        BevelEdges = []
        BevelOuter = bvNone
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        object GpuNoFoundColorLabel: TColorLabel
          Left = 131
          Top = 32
          Width = 210
          Height = 19
          Caption = 'No GPU acceleration available'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GpuNotFoundReasonLabel: TLabel
          Left = 8
          Top = 90
          Width = 432
          Height = 26
          Alignment = taCenter
          AutoSize = False
          Caption = 'GpuNotFoundReasonLabel'
        end
        object GpuInfoPanel: TPanel
          Left = 8
          Top = 16
          Width = 432
          Height = 145
          BevelOuter = bvNone
          ParentBackground = False
          ParentColor = True
          TabOrder = 0
          object DeviceNameComboBox: TComboBox
            Left = 95
            Top = 56
            Width = 256
            Height = 21
            Style = csDropDownList
            TabOrder = 0
            OnChange = DeviceNameComboBoxChange
          end
          object EnableGpuCheckBox: TCheckBox
            Left = 95
            Top = 33
            Width = 113
            Height = 17
            Caption = 'Use GPU if Available'
            TabOrder = 1
            OnClick = EnableGpuCheckBoxClick
          end
          object GpuInfoButton: TButton
            Left = 278
            Top = 98
            Width = 73
            Height = 25
            Hint = 'Close and save all settings'
            Caption = 'Info'
            Default = True
            TabOrder = 2
            OnClick = GpuInfoButtonClick
          end
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 203
    Top = 220
    Width = 73
    Height = 25
    Hint = 'Close and save all settings'
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object RestoreButton: TButton
    Left = 111
    Top = 220
    Width = 73
    Height = 25
    Hint = 'Restore these settings to design settings'
    Caption = 'Restore'
    TabOrder = 2
    OnClick = RestoreButtonClick
  end
  object CancelBtn: TButton
    Left = 294
    Top = 220
    Width = 73
    Height = 25
    Hint = 'Close discarding all changes'
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
    OnClick = CancelBtnClick
  end
end
