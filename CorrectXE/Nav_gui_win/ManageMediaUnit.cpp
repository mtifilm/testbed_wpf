//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ManageMediaUnit.h"

#include "FileSweeper.h"
#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTIio.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma resource "*.dfm"

TManageMediaForm *ManageMediaForm;
//---------------------------------------------------------------------------

__fastcall TManageMediaForm::TManageMediaForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------

bool TManageMediaForm::isVersionMediaParentSameAsMaster()
{
   string masterClipMediaParentFolder = RemoveDirSeparator(GetFilePath(getMasterClipMediaFolder()));
   string versionClipMediaParentFolder = RemoveDirSeparator(getVersionClipMediaParentFolder());
   return versionClipMediaParentFolder == masterClipMediaParentFolder;
}
//---------------------------------------------------------------------------

bool TManageMediaForm::computeFolderPathOfFrameFile(
   const string &newPath,
   const string &oldParentPath,
   const string &filename,
   string &resultPath)
{
   resultPath = newPath;
   bool foundTheFile = false;

   string residualPath = oldParentPath;
   string localResultPath;
   string impliedPath;

   do
   {
      // Note: AddDirSeparator returns empty string for empty input.
      string testPath = AddDirSeparator(newPath) + AddDirSeparator(impliedPath) + filename;
      if (DoesFileExist(testPath))
      {
         resultPath = RemoveDirSeparator(AddDirSeparator(newPath) + impliedPath);
         foundTheFile = true;
         break;
      }

      string nextDir = GetFileLastDir(AddDirSeparator(residualPath));
      impliedPath = AddDirSeparator(nextDir) + AddDirSeparator(impliedPath);

      // This may look like we're skipping a component in the residual path, but
      // we can bail if residualPath ends up empty here because the last part
      // of the path returned is, e.g. D:\ , which is useless to try to combine
      // with the new path!
      residualPath = GetFileAllButLastDir(AddDirSeparator(residualPath));
   }
   while (!residualPath.empty());

   return foundTheFile;
}
//---------------------------------------------------------------------------

static string okExtensions[] = {"cin", "dpx", "exr", "tif", "tiff"};

// TODO: This should really be looking specifically for the files
// needed by the clip, not just for acceptable files in general! QQQ
string TManageMediaForm::findAnAcceptableImageFile(string directory)
{
	bool foundAnAcceptableFile = false;
   string acceptableFileName;
	WIN32_FIND_DATA FindFileData;
	string pattern = directory + "\\*";
	HANDLE hFind = FindFirstFile(pattern.c_str(), &FindFileData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (strcmp(FindFileData.cFileName, ".") == 0
				 || strcmp(FindFileData.cFileName, "..") == 0)
			{
				continue;
			}

			string ext = GetFileExt(FindFileData.cFileName);
			if (ext == ".dpx" || ext == ".exr" || ext == ".tif" || ext == ".tiff")
			{
				acceptableFileName = FindFileData.cFileName;
				foundAnAcceptableFile = true;
				break;
			}
      } while(FindNextFile(hFind, &FindFileData));

		FindClose(hFind);
		hFind = INVALID_HANDLE_VALUE;
	}

   return foundAnAcceptableFile ? acceptableFileName : "";
}
//---------------------------------------------------------------------------

int TManageMediaForm::countPoundSigns(const string &templateString)
{
   int count = 0;
   const string badChars = "<>:\"/\\|?*";
   for (auto c : templateString)
   {
      count += (c == '#') ? 1 : 0;
   }

   return count;
}
//---------------------------------------------------------------------------

bool TManageMediaForm::containsBadChars(const string &templateString, const string &badChars)
{
   for (auto c : templateString)
   {
      auto iter = find(badChars.begin(), badChars.end(), c);
      if (iter != badChars.end())
      {
         return true;
      }
   }

   return false;
}
//---------------------------------------------------------------------------

void TManageMediaForm::updateState()
{
   if (!DoesDirectoryExist(getMasterClipMediaFolder()))
   {
      MasterClipMediaFolderErrorLabel->Caption = " Error: folder not found! ";
      MasterClipMediaFolderErrorLabel->Visible = true;
   }
   else if (!DoesFileExist(AddDirSeparator(getMasterClipMediaFolder()) + _fileNameThatMustExistInRelinkedFolder))
   {
      MasterClipMediaFolderErrorLabel->Caption = " Error: folder does not contain clip's media! ";
      MasterClipMediaFolderErrorLabel->Visible = true;
   }
   else
   {
      MasterClipMediaFolderErrorLabel->Visible = false;
   }

   VersionClipMediaParentFolderErrorLabel->Visible = !DoesDirectoryExist(getVersionClipMediaParentFolder());

   const string badChars = "<>:\"/\\|?*";
   if (countPoundSigns(getVersionClipMediaFolderTemplate()) != 1)
   {
      VersionClipMediaFolderTemplateErrorLabel->Caption = " Error: Template must contain exactly one # ";
      VersionClipMediaFolderTemplateErrorLabel->Visible = true;
   }
   else if (containsBadChars(getVersionClipMediaFolderTemplate(), badChars))
   {
      VersionClipMediaFolderTemplateErrorLabel->Caption = (string(" Error: can't use any of ") + badChars + " ").c_str();
      VersionClipMediaFolderTemplateErrorLabel->Visible = true;
   }

   bool isThereAnError = MasterClipMediaFolderErrorLabel->Visible
                        || VersionClipMediaParentFolderErrorLabel->Visible
                        || VersionClipMediaFolderTemplateErrorLabel->Visible;

   OkButton->Enabled = !isThereAnError;
}
//---------------------------------------------------------------------------

void TManageMediaForm::fillIn(
   const string &masterClipMediaFolder,
   const string &versionClipMediaParentFolder,
   const string &versionClipMediaFolderTemplate)
{
   setMasterClipMediaFolder(masterClipMediaFolder);
   setVersionClipMediaParentFolder(versionClipMediaParentFolder);
   setVersionClipMediaFolderTemplate(versionClipMediaFolderTemplate);
}

//---------------------------------------------------------------------------

string TManageMediaForm::getMasterClipMediaFolder()
{
   return WCharToStdString(MasterClipMediaFolderEdit->Text.c_str());
}
//---------------------------------------------------------------------------

string TManageMediaForm::getVersionClipMediaParentFolder()
{
   return WCharToStdString(VersionClipMediaParentFolderEdit->Text.c_str());
}
//---------------------------------------------------------------------------

string TManageMediaForm::getVersionClipMediaFolderTemplate()
{
   return WCharToStdString(VersionClipMediaFolderTemplateEdit->Text.c_str());
}
//---------------------------------------------------------------------------

void TManageMediaForm::setMasterClipMediaFolder(const string &newFolder)
{
   // This assumes that the caller will first set us to the OLD media folder!
   if (_oldMasterClipMediaFolder.empty())
   {
      _oldMasterClipMediaFolder = newFolder;
   }

   MasterClipMediaFolderEdit->Text = StringTrim(newFolder).c_str();

   if (!_initedMasterClipMediaFolder)
   {
      _initedMasterClipMediaFolder = true;
      if (_initedVersionClipMediaParentFolder)
      {
         VersionMediaParentSameAsMasterCheckBox->Checked = isVersionMediaParentSameAsMaster();
         VersionClipMediaParentFolderEdit->Enabled = !isVersionMediaParentSameAsMaster();
         VersionClipMediaParentFolderBrowseButton->Enabled = !isVersionMediaParentSameAsMaster();
      }
   }
   else if (VersionMediaParentSameAsMasterCheckBox->Checked)
   {
      string masterClipMediaParentFolder = RemoveDirSeparator(GetFilePath(getMasterClipMediaFolder()));
      setVersionClipMediaParentFolder(masterClipMediaParentFolder);
   }

   updateState();
}
//---------------------------------------------------------------------------

void TManageMediaForm::setVersionClipMediaParentFolder(const string &newFolder)
{
   VersionClipMediaParentFolderEdit->Text = StringTrim(newFolder).c_str();

   if (!_initedVersionClipMediaParentFolder)
   {
      _initedVersionClipMediaParentFolder = true;
      if (_initedMasterClipMediaFolder)
      {
         VersionMediaParentSameAsMasterCheckBox->Checked = isVersionMediaParentSameAsMaster();
         VersionClipMediaParentFolderEdit->Enabled = !isVersionMediaParentSameAsMaster();
         VersionClipMediaParentFolderBrowseButton->Enabled = !isVersionMediaParentSameAsMaster();
      }
   }
   else if (VersionMediaParentSameAsMasterCheckBox->Checked)
   {
      VersionMediaParentSameAsMasterCheckBox->Checked = isVersionMediaParentSameAsMaster();
      VersionClipMediaParentFolderEdit->Enabled = !isVersionMediaParentSameAsMaster();
      VersionClipMediaParentFolderBrowseButton->Enabled = !isVersionMediaParentSameAsMaster();
   }

   updateState();
}
//---------------------------------------------------------------------------

void TManageMediaForm::setVersionClipMediaFolderTemplate(const string &newTemplate)
{
   VersionClipMediaFolderTemplateEdit->Text = StringTrim(newTemplate).c_str();
   _initedVersionClipMediaFolderTemplate = true;
   updateState();
}
//---------------------------------------------------------------------------

void TManageMediaForm::setFileNameThatMustExistInRelinkedFolder(const string &filename)
{
   _fileNameThatMustExistInRelinkedFolder = filename;
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::MasterClipMediaFolderBrowseButtonClick(TObject *Sender)

{
	Application->NormalizeTopMosts();
   string startingFolder = getMasterClipMediaFolder();
	string newFolder = MTIBrowseForFolder(startingFolder, string("Browse to Media Folder (browser only shows folders, not files)"));
	Application->RestoreTopMosts();
   if (newFolder.empty() || newFolder == startingFolder)
   {
      // No-op.
      return;
   }

   string actualFolder;
   computeFolderPathOfFrameFile(
         StringTrim(newFolder),
         _oldMasterClipMediaFolder,
         _fileNameThatMustExistInRelinkedFolder,
         actualFolder);

   setMasterClipMediaFolder(actualFolder);

   updateState();
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::VersionClipMediaParentFolderBrowseButtonClick(TObject *Sender)
{
	Application->NormalizeTopMosts();
   string startingFolderPath = getVersionClipMediaParentFolder();
	string newFolderPath = MTIBrowseForFolder(startingFolderPath, string("Browse to Parent Folder (browser only shows folders, not files)"));
	Application->RestoreTopMosts();
   if (newFolderPath.empty() || newFolderPath == startingFolderPath)
   {
      // No-op.
      return;
   }

   setVersionClipMediaParentFolder(newFolderPath);
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::VersionMediaParentSameAsMasterCheckBoxClick(TObject *Sender)
{
   string masterClipMediaParentFolder = RemoveDirSeparator(GetFilePath(getMasterClipMediaFolder()));
   setVersionClipMediaParentFolder(masterClipMediaParentFolder);
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::FormShow(TObject *Sender)
{
   MTIassert(_initedMasterClipMediaFolder && _initedVersionClipMediaParentFolder && _initedVersionClipMediaFolderTemplate);
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::FormHide(TObject *Sender)
{
   _initedMasterClipMediaFolder = false;
   _initedVersionClipMediaParentFolder = false;
   _initedVersionClipMediaFolderTemplate = false;
   _oldMasterClipMediaFolder = "";
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::TextEditExit(TObject *Sender)
{
   setMasterClipMediaFolder(getMasterClipMediaFolder());
   setVersionClipMediaParentFolder(getVersionClipMediaParentFolder());
   setVersionClipMediaFolderTemplate(getVersionClipMediaFolderTemplate());
}
//---------------------------------------------------------------------------

void __fastcall TManageMediaForm::TextEditKeyPress(TObject *Sender,
          System::WideChar &Key)
{
   if (Key == 13)
   {
      TextEditExit(Sender);
      Key = 0;
   }
}
//---------------------------------------------------------------------------



