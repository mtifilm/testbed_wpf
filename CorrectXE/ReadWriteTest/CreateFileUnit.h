//---------------------------------------------------------------------------

#ifndef CreateFileUnitH
#define CreateFileUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include <Dialogs.hpp>
#include <Graphics.hpp>
#include "PERFGRAP.h"
#include <Mask.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *CreateFileButton;
        TButton *QuitButton;
        TButton *WriteFileButton;
        TLabel *Label3;
        TLabel *StatusLabel;
        TButton *ReadFileButton;
        TMemo *DetailsMemo;
        TBitBtn *StopButton;
        TCheckBox *ShowProgressCheckBox;
        TProgressBar *ProgressBar;
        TLabel *ProgressPerc;
        TLabel *Label7;
        TLabel *WebSiteLabel;
        TLabel *Label9;
  TLabel *MBsAvgLabel;
  TLabel *Label4;
  TLabel *Label13;
  TLabel *MBsLastLabel;
  TLabel *Label14;
  TLabel *Label15;
  TLabel *MBsMinLabel;
  TLabel *MBsMaxLabel;
        TLabel *Label16;
        TLabel *MBsMovingAvgLabel;
        TCheckBox *ShowEachIOCheckBox;
        TGroupBox *GroupBox1;
        TLabel *Label5;
        TEdit *VideostoreEdit;
        TRadioButton *VideostoreRadioButton;
        TRadioButton *FirstFileRadioButton;
        TLabel *Label17;
        TOpenDialog *FirstFileOpenDialog;
        TEdit *LastNumEdit;
        TLabel *LastNumLabel;
        TSpeedButton *NameBrowserButton;
        TGroupBox *GroupBox2;
        TRadioGroup *MemoryRadioGroup;
        TCheckBox *NoBufferingCheckBox;
        TRadioButton *FrameSizeRadioButtonList;
        TRadioButton *FrameSizeRadioButtonEdit;
        TLabel *ChunkSizePart1Label;
        TLabel *ChunkSizePart2Label;
        TComboBox *FrameSizeComboBox;
        TEdit *FrameSizeEdit;
        TSpeedButton *ClearMemoSpeedButton;
        TLabel *Label2;
        TEdit *StepEdit;
        TLabel *Label6;
        TEdit *JumpEdit;
        TLabel *LabelJump;
        TBevel *Bevel1;
        TCheckBox *EnableFileWriteCheckBox;
        TMaskEdit *StartTCEdit;
        TMaskEdit *EndTCEdit;
        TMaskEdit *DurTCEdit;
        TEdit *DurFrameEdit;
        TEdit *EndFrameEdit;
        TEdit *StartFrameEdit;
        TCSpinEdit *StorageFPSEdit;
        TLabel *Label1;
        TLabel *Label12;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TLabel *Label25;
        TLabel *IterationsLabel;
        TEdit *IterationsEdit;
        TComboBox *NumFieldsComboBox;
        TEdit *NumChunksEdit;
        TComboBox *IOAlignmentComboBox;
        TLabel *Label11;
        TEdit *StartByteEdit;
        TEdit *EndByteEdit;
        TEdit *DurByteEdit;
        TLabel *Label18;
        TLabel *Label26;
        TLabel *Label27;
        TLabel *IOTimesStdDevLabel;
        TProgressBar *IOBar0m;
        TProgressBar *IOBar1m;
        TProgressBar *IOBar1p;
        TProgressBar *IOBar2p;
        TProgressBar *IOBar2m;
        TProgressBar *IOBar0p;
        TLabel *LogFileLabel;
        TPanel *BufferPanel;
        TProgressBar *IOBarRel1;
        TProgressBar *IOBarRel2;
        TProgressBar *IOBarRel3;
        TProgressBar *IOBarRel5;
        TProgressBar *IOBarRel4;
        TProgressBar *IOBarRel6;
        TProgressBar *IOBarRel7;
        TProgressBar *IOBarRel8;
        TProgressBar *IOBarRel9;
        TProgressBar *IOBarRel10;
        TProgressBar *IOBarRel11;
        TProgressBar *IOBarRel12;
        TProgressBar *IOBarRel13;
        TProgressBar *IOBarRel14;
        TProgressBar *IOBarRel15;
        TProgressBar *IOBarRel16;
        TProgressBar *IOBarRel17;
        TProgressBar *IOBarRel18;
        TProgressBar *IOBarRel19;
        TProgressBar *IOBarRel20;
        TLabel *Label28;
        TLabel *Label29;
        TLabel *Label30;
        TLabel *Label31;
        TCheckBox *MovingAvgWarnCheckBox;
        TEdit *MovingAvgWarnEdit;
        TMemo *Memo;
        TSpeedButton *ClearDetailsSpeedButton;
        TLabel *Label32;
        TProgressBar *BufferUsageBar;
        TLabel *Label8;
        TEdit *MovingAvgNumSamplesEdit;
        TCheckBox *MaxSpeedCheckBox;
        TEdit *MaxSpeedEdit;
        TLabel *MBps;
        TLabel *BytesPerSecLabel;
        TLabel *IOTimesMeanLabel;
        TLabel *Label10;
        TLabel *Label33;
        TLabel *Label34;
        TLabel *TargetIOSpeedLabel;
        TCheckBox *BufferWarnCheckBox;
        TEdit *BufferWarnEdit;
        TLabel *GigabyteLabel;
        TEdit *GigabyteEdit;
        void __fastcall CreateFileButtonClick(TObject *Sender);
        void __fastcall QuitButtonClick(TObject *Sender);
        void __fastcall WriteFileButtonClick(TObject *Sender);
        void __fastcall ReadFileButtonClick(TObject *Sender);
        void __fastcall StopButtonClick(TObject *Sender);
        void __fastcall ShowProgressCheckBoxClick(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FrameSizeRadioButtonListClick(TObject *Sender);
  void __fastcall FrameSizeRadioButtonEditClick(TObject *Sender);
        void __fastcall MaxSpeedCheckBoxClick(TObject *Sender);
        void __fastcall MovingAvgWarnEditExit(TObject *Sender);
        void __fastcall BufferWarnEditExit(TObject *Sender);
        void __fastcall MaxSpeedEditExit(TObject *Sender);
        void __fastcall MovingAvgWarnEditKeyPress(TObject *Sender, char &Key);
        void __fastcall MaxSpeedEditKeyPress(TObject *Sender, char &Key);
        void __fastcall MovingAvgNumSamplesEditKeyPress(TObject *Sender,
          char &Key);
        void __fastcall MovingAvgNumSamplesEditExit(TObject *Sender);
        void __fastcall VideostoreRadioButtonClick(TObject *Sender);
        void __fastcall FirstFileRadioButtonClick(TObject *Sender);
        void __fastcall NameBrowserButtonClick(TObject *Sender);
        void __fastcall BufferWarnCheckBoxClick(TObject *Sender);
        void __fastcall MovingAvgWarnCheckBoxClick(TObject *Sender);
        void __fastcall EnableFileWriteCheckBoxClick(TObject *Sender);
        void __fastcall ClearMemoSpeedButtonClick(TObject *Sender);
        void __fastcall ClearDetailsSpeedButtonClick(TObject *Sender);
        void __fastcall MTIImageClick(TObject *Sender);
        void __fastcall WebSiteLabelClick(TObject *Sender);
        void __fastcall MaxSpeedEditChange(TObject *Sender);
        void __fastcall StartFrameEditChange(TObject *Sender);
        void __fastcall EndFrameEditChange(TObject *Sender);
        void __fastcall DurFrameEditChange(TObject *Sender);
        void __fastcall StartTCEditExit(TObject *Sender);
        void __fastcall EndTCEditExit(TObject *Sender);
        void __fastcall DurTCEditExit(TObject *Sender);
        void __fastcall StorageFPSEditChange(TObject *Sender);
        void __fastcall GigabyteEditExit(TObject *Sender);

private:	// User declarations
  void DoFileIO(int iReadOrWrite);
  void FixTCStrings();
  void CalcDurTC();
  void CalcEndTC();
  void CreateLogFile(int iReadOrWrite, char caLogFileName[]);
  int GetFrameTotalFromTC(char *caTC);
  void SetTCFromFrameTotal(int iFrameTotal, char * caTC);
  void UpdateGUISettingsDisplay();
  void CalcAndShow(int iReadOrWrite, LARGE_INTEGER llIndex, LARGE_INTEGER llNumChunksToDo,
                 LARGE_INTEGER llNumBytesDone, unsigned long ulNumBytesDone,
                 double dMillisecUsed, double dMillisecThisRun, double dMillisecThisRunInclThrot,
                 int *ipNumMovingAvgWarnings,
                 bool bShowFlag, double dMillisecForAvg, bool *bpDoThrottle,
                 double *dpMillisecToSleep);
  void ShowAverageMessage(int iCurrIndex, double dMBperSecSum);
  HANDLE OpenFile(const char *caFilename, const int iReadOrWrite);
  bool CheckIO(const int iReadOrWrite, const unsigned long ulNumBytesDone,
                     const long lChunkSizeBytes, char *caMessage);
  bool DoReadOrWriteFile(const int iReadOrWrite, const HANDLE hHandle,
                               unsigned char *ucpData,
                               const long lChunkSizeBytes,
                               unsigned long *ulpNumBytesDone);
  void MemoryAlloc(HANDLE *hpMem, unsigned char **ucppData,
                   const long lChunkSizeBytes);
  void MemoryFree(HANDLE *hpMem, unsigned char **ucppData);
  void ShowIterationMessage(const char *caModePastTense,
                            const bool bThrottleHappened,
                            const int iCurrIteration,
                            const int iIterations,
                            const double dMBperSec,
                            const int iNumMovingAvgWarnings);
  void LaunchWebSite(void);


public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
