/*  File: machine.h
    Created June 19, 2000 by John Mertus
    Keeper: John Mertus

    Version
       $Log: machine.h,v $
       Revision 1.1.1.1  2003/10/24 17:29:39  mlm
       Initial import of Read/Write Test.

       Revision 1.3  2000/12/27 22:18:35  starr
       Removed conditional compilation on _BCPLUSPLUS_ around MTI_INT64 and MTI_UINT64 in Windows and Console/DOS versions.  Added MTI_INT32 and MTI_UINT32 to Windows and Console/DOS versions

       Revision 1.2  2000/12/27 21:33:08  mertus
       Added MTI_ to all names to avoid conflicts
       Fixed unix text type in DOS

       Revision 1.1  2000/11/28 18:42:48  mertus2k
       *** empty log message ***

       Revision 1.1.1.1  2000/11/21 20:34:37  mertus2k
       Imported using TkCVS


       Revision 1.4  2000/11/16 15:48:48  mertus
       Fixed author's name to Mertus

       Revision 1.3  2000/11/16 15:47:29  mertus
       Added Kurt's name to author

       Revision 1.2  2000/11/16 15:27:23  mertus
       John Starr's fixes

    This file contains the typedefs for the MTI code that translates the
    dependent hardware onto independent software.

    This is the ONLY place that common machine dependent typedefs
    can be defined which create machine independent code.

    Note however that specific machine dependent typedefs and functions can
    be defined elsewere but used to create machine dependent code.

    This supports some conditionals

      __sgi  means we are using the sgi cc or CC compiler
      __unix means we are on a unix system, including linux, is defined for cc and gcc
      __linux means we are on a linux system
      _WINDOWS means we are on a windows machine

      __GUNC__ means we are using the gcc compiler
      __BORLANDC__ means we are using the BorlandC compiler (Note: This does not imply windows)

    THIS CODE IS MACHINE DEPENDENT!
*/

#ifndef _MACHINE_H
//------------------------------COMMON SECTION--------------------------------

//
// Deal with the endian issues, ENDIAN will be defined to mean
// which endian we are compiling under.
//
#define MTI_BIG_ENDIAN 1                  // The symbolic names
#define MTI_LITTLE_ENDIAN 2

//----------------PRE WINDOWS SECTION, BC 4------------------------------------
//
// Borland defines _Windows by default, define _WINDOWS also
//
#ifdef _Windows
#define _WINDOWS
#endif

//----------------WINDOWS SECTION, VC 6 or BC 4--------------------------------
#ifdef _WINDOWS
#include <windows.h>

#define ENDIAN MTI_LITTLE_ENDIAN             // The actual edian of the machine

// Now make explicit 64, 32, 16 and 8 bit data types.
typedef __int64 MTI_INT64;                  // Signed 64 bits
typedef unsigned __int64 MTI_UINT64;        // Unsigned 64 bits

typedef int MTI_INT32;                      // Signed 32 bits
typedef unsigned int MTI_UINT32;            // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits

typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                       // 
typedef double MTI_REAL64;

// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif

#define _MACHINE_H
#endif

//----------------MSDOS SECTION, VC 6 or BC 4--------------------------------

#ifdef _CONSOLE
#include <basetsd.h>
#define ENDIAN MTI_LITTLE_ENDIAN           // The actual edian of the machine

// Now make explicit 64, 32, 16 and 8 bit data types.
typedef __int64 MTI_INT64;                  // Signed 64 bits
typedef unsigned __int64 MTI_UINT64;        // Unsigned 64 bits

typedef int MTI_INT32;                      // Signed 32 bits
typedef unsigned int MTI_UINT32;            // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits

typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                   // 
typedef double MTI_REAL64;

//
// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif

#define _MACHINE_H
#endif



//-----------------SGI SECTION, cc or gcc compilers----------------------------
#ifdef __sgi
#include <sgidefs.h>

#define ENDIAN MTI_BIG_ENDIAN                // The actual edian of the machine
// Now make explicit 64, 32, 16 and 8 bit data types.

typedef __int64_t MTI_INT64 ;               // Signed 64 bits
typedef __uint64_t MTI_UINT64;              // Unsigned 64 bits

typedef __int32_t MTI_INT32;                // Signed 32 bits
typedef __uint32_t MTI_UINT32;              // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits
typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                   // 
typedef double MTI_REAL64;

//
// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif
#define _MACHINE_H

#endif

/* Make sure we are defined at least once */
#ifndef _MACHINE_H
#error No machine has been defined
#endif

//------------------------------COMMON SECTION---------------------------------

#define LOWPART_LONGLONG		((LONGLONG) 0x00000000FFFFFFFF)
#define HIGHPART_LONGLONG		((LONGLONG) 0xFFFFFFFF00000000)

#define LOWLONGLONG(x)			 ( (MTI_UINT32) ( (x) & LOWPART_LONGLONG )
#define HILONGLONG(x)			 ( (MTI_INT32) ( ( (x) & HIGHPART_LONGLONG ) >> 32 ) )
// End of machine.h
#endif

