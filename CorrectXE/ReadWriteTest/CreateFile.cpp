//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("CreateFile.res");
USEFORM("CreateFileUnit.cpp", Form1);
USEUNIT("HRTimer.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TForm1), &Form1);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
