/*
    File Name:  HRTimer.cpp
    Create:  March 2, 2000
    Release 1.00

    This provides a High Resolution Timer for windows.  The accurace is better
    than 10 microseconds.

                                        -John Mertus@Brown.EDU
*/

//---------------------------------------------------------------------------
#include <vcl.h>
#include <windows.h>
#include <stdio.h>

#pragma hdrstop

#include "HRTimer.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)


 /*-----------------------Constructor----------------JAM--Mar 2000----*/

  HRTimer::HRTimer()

 /* This just finds if the hardware supports the timer and finds the  */
 /* clock freq                                                        */
 /*                                                                   */
 /*********************************************************************/
{
   LARGE_INTEGER l;
   QueryPerformanceFrequency(&l);
   fClockFreq = l.QuadPart;
   StartTimer();
}

 /*-----------------------StartTimer------------------JAM--Mar 2000---*/

     void HRTimer::StartTimer()

 /* Call this to zero the timer.  This just gets the count value      */
 /* and stores it for later.                                          */
 /*                                                                   */
 /*********************************************************************/
{
   LARGE_INTEGER l;
   QueryPerformanceCounter(&l);
   fStartCount = l.QuadPart;
}

 /*-------------------------ReadTimer----------------JAM--Mar 2000----*/

     double HRTimer::ReadTimer()

 /* This returns the current count in milliseconds.  StartTimer must  */
 /* be called first                                                   */
 /*                                                                   */
 /*********************************************************************/
 {
    LARGE_INTEGER CurrentCount;
    QueryPerformanceCounter(&CurrentCount);
    return(1000.0*(CurrentCount.QuadPart - fStartCount)/(Double)(fClockFreq));
 }


 /*----------------------ReadTimerStr-------------JAM--Mar 2000-------*/

    string HRTimer::ReadTimerStr()

 /* Just produce an ansi string of the timer.  2 decimal point.       */
 /*                                                                   */
 /*********************************************************************/
{
   char Result[20];
   double x = ReadTimer();
   sprintf(Result, "%0.2f",x);
   return(string(Result));
}

 /*------------------------ShowTimer--------------JAM--Mar 2000------*/

     void HRTimer::ShowTimer()

 /* This just prints out the result to the screen.                    */
 /* be called first                                                   */
 /*                                                                   */
 /*********************************************************************/
{
    ShowMessage((ReadTimerStr() + " Milliseconds").c_str());
}

 /*----------------------GetMsFromUTC--------------JAM--Mar 2000------*/

      double GetMsFromUTC(INT64 UTC)

 /* This converst the UTC into milliseconds.                          */
 /*                                                                   */
 /*********************************************************************/
{
   static double TimeBase=0;

   LARGE_INTEGER l;
   if (TimeBase == 0)
     {
       QueryPerformanceFrequency(&l);
       TimeBase = l.QuadPart;
     }

   return(1000.0*UTC/TimeBase);
}
