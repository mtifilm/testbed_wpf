object Form1: TForm1
  Left = 248
  Top = 166
  AutoScroll = False
  Caption = 'MTI Read/Write Speed Test v1.6'
  ClientHeight = 843
  ClientWidth = 802
  Color = clBtnFace
  Constraints.MinHeight = 870
  Constraints.MinWidth = 810
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnCreate = FormCreate
  DesignSize = (
    802
    843)
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 641
    Top = 747
    Width = 33
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Status:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object StatusLabel: TLabel
    Left = 678
    Top = 747
    Width = 112
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Awaiting your command'
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object ProgressPerc: TLabel
    Left = 342
    Top = 747
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Anchors = [akLeft, akBottom]
    Caption = ' 0.00 %'
  end
  object Label7: TLabel
    Left = 85
    Top = 820
    Width = 189
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    Caption = 'Mathematical Technologies Inc.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object WebSiteLabel: TLabel
    Left = 301
    Top = 822
    Width = 78
    Height = 13
    Hint = 'http://www.mtifilm.com'
    Anchors = [akLeft, akBottom]
    Caption = 'www.mtifilm.com'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = WebSiteLabelClick
  end
  object Label9: TLabel
    Left = 5
    Top = 822
    Width = 66
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '(c) 2001-2009'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object MBsAvgLabel: TLabel
    Left = 589
    Top = 233
    Width = 56
    Height = 13
    Hint = 'Since beginning of iteration'
    Alignment = taRightJustify
    Caption = '  0.00 MB/s'
    ParentShowHint = False
    ShowHint = True
  end
  object Label4: TLabel
    Left = 513
    Top = 233
    Width = 22
    Height = 13
    Hint = 'Since beginning of iteration'
    Caption = 'Avg:'
    ParentShowHint = False
    ShowHint = True
  end
  object Label13: TLabel
    Left = 513
    Top = 261
    Width = 23
    Height = 13
    Hint = 'For last chunk'
    Caption = 'Last:'
    ParentShowHint = False
    ShowHint = True
  end
  object MBsLastLabel: TLabel
    Left = 589
    Top = 261
    Width = 56
    Height = 13
    Hint = 'For last chunk'
    Alignment = taRightJustify
    Caption = '  0.00 MB/s'
    ParentShowHint = False
    ShowHint = True
  end
  object Label14: TLabel
    Left = 513
    Top = 275
    Width = 20
    Height = 13
    Hint = 'Since beginning of iteration'
    Caption = 'Min:'
    ParentShowHint = False
    ShowHint = True
  end
  object Label15: TLabel
    Left = 513
    Top = 289
    Width = 23
    Height = 13
    Hint = 'Since beginning of iteration'
    Caption = 'Max:'
    ParentShowHint = False
    ShowHint = True
  end
  object MBsMinLabel: TLabel
    Left = 589
    Top = 275
    Width = 56
    Height = 13
    Hint = 'Since beginning of iteration'
    Alignment = taRightJustify
    Caption = '  0.00 MB/s'
    ParentShowHint = False
    ShowHint = True
  end
  object MBsMaxLabel: TLabel
    Left = 589
    Top = 289
    Width = 56
    Height = 13
    Hint = 'Since beginning of iteration'
    Alignment = taRightJustify
    Caption = '  0.00 MB/s'
    ParentShowHint = False
    ShowHint = True
  end
  object Label16: TLabel
    Left = 513
    Top = 247
    Width = 60
    Height = 13
    Caption = 'Moving Avg:'
  end
  object MBsMovingAvgLabel: TLabel
    Left = 589
    Top = 247
    Width = 56
    Height = 13
    Hint = 'Average of MB/s values for last n runs'
    Alignment = taRightJustify
    Caption = '  0.00 MB/s'
    ParentShowHint = False
    ShowHint = True
  end
  object ClearMemoSpeedButton: TSpeedButton
    Left = 9
    Top = 510
    Width = 23
    Height = 22
    Hint = 'Clear Memo Area'
    Flat = True
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00000000000000000000000000FFFFFF00FF00FF000000
      000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FFFF
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00000000000000000000000000FFFFFF00FF00FF000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0000000000000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF000000
      0000000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF000000
      000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    ParentShowHint = False
    ShowHint = True
    OnClick = ClearMemoSpeedButtonClick
  end
  object LabelJump: TLabel
    Left = 388
    Top = 747
    Width = 55
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'file  position'
  end
  object IterationsLabel: TLabel
    Left = 711
    Top = 785
    Width = 46
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Iterations:'
  end
  object IOTimesStdDevLabel: TLabel
    Left = 750
    Top = 352
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = ' 0.00 ms'
  end
  object LogFileLabel: TLabel
    Left = 512
    Top = 387
    Width = 40
    Height = 13
    Caption = 'Log File:'
  end
  object Label31: TLabel
    Left = 694
    Top = 352
    Width = 56
    Height = 13
    Caption = 'IO Std Dev:'
  end
  object ClearDetailsSpeedButton: TSpeedButton
    Left = 7
    Top = 741
    Width = 23
    Height = 22
    Hint = 'Clear Memo Area'
    Anchors = [akLeft, akBottom]
    Flat = True
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00000000000000000000000000FFFFFF00FF00FF000000
      000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000000000000000FFFF
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00000000000000000000000000FFFFFF00FF00FF000000
      0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0000000000000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF000000
      0000000000000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00FF000000
      000000000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF000000000000000000FFFFFF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    ParentShowHint = False
    ShowHint = True
    OnClick = ClearDetailsSpeedButtonClick
  end
  object Label32: TLabel
    Left = 6
    Top = 533
    Width = 623
    Height = 14
    Caption = 
      '     index,     bytes,    iotime,   iospeed,   mov avg,  ms slee' +
      'p,  mv av thrt,    buffer'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
  end
  object MBps: TLabel
    Left = 531
    Top = 352
    Width = 48
    Height = 13
    Caption = '168 MBps'
  end
  object BytesPerSecLabel: TLabel
    Left = 531
    Top = 335
    Width = 111
    Height = 13
    Caption = '168,000,000 Bytes/sec'
  end
  object IOTimesMeanLabel: TLabel
    Left = 750
    Top = 336
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = ' 0.00 ms'
  end
  object Label10: TLabel
    Left = 694
    Top = 336
    Width = 44
    Height = 13
    Caption = 'IO Mean:'
  end
  object Label33: TLabel
    Left = 6
    Top = 405
    Width = 700
    Height = 14
    Caption = 
      '     index,     bytes,    iotime,   iospeed,   mov avg,  ms slee' +
      'p,  mv av thrt,    buffer,  warnings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
  end
  object CreateFileButton: TButton
    Left = -100
    Top = -50
    Width = 80
    Height = 26
    Caption = 'Create File'
    TabOrder = 1
    TabStop = False
    Visible = False
    OnClick = CreateFileButtonClick
  end
  object QuitButton: TButton
    Left = -100
    Top = -100
    Width = 80
    Height = 27
    Caption = 'Quit'
    TabOrder = 3
    OnClick = QuitButtonClick
  end
  object WriteFileButton: TButton
    Left = 535
    Top = 809
    Width = 80
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = 'Write File'
    Enabled = False
    TabOrder = 0
    TabStop = False
    OnClick = WriteFileButtonClick
  end
  object ReadFileButton: TButton
    Left = 623
    Top = 809
    Width = 80
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = 'Read File'
    TabOrder = 2
    OnClick = ReadFileButtonClick
  end
  object DetailsMemo: TMemo
    Left = 8
    Top = 550
    Width = 783
    Height = 187
    TabStop = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object StopButton: TBitBtn
    Left = 711
    Top = 809
    Width = 80
    Height = 27
    Anchors = [akRight, akBottom]
    Caption = 'Stop'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = StopButtonClick
  end
  object ShowProgressCheckBox: TCheckBox
    Left = 40
    Top = 786
    Width = 98
    Height = 14
    TabStop = False
    Anchors = [akLeft, akBottom]
    Caption = 'Show Progress'
    Checked = True
    State = cbChecked
    TabOrder = 6
    OnClick = ShowProgressCheckBoxClick
  end
  object ProgressBar: TProgressBar
    Left = 8
    Top = 765
    Width = 783
    Height = 16
    Anchors = [akLeft, akRight, akBottom]
    Min = 0
    Max = 100
    TabOrder = 7
  end
  object ShowEachIOCheckBox: TCheckBox
    Left = 40
    Top = 747
    Width = 98
    Height = 14
    TabStop = False
    Anchors = [akLeft, akBottom]
    Caption = 'Show Each I/O'
    TabOrder = 8
    OnClick = ShowProgressCheckBoxClick
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 489
    Height = 89
    Caption = ' Mode Selection '
    TabOrder = 9
    object Label5: TLabel
      Left = 64
      Top = 20
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Videostore'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 184
      Top = 20
      Width = 68
      Height = 13
      Caption = 'File Sequence'
      Enabled = False
      Visible = False
    end
    object LastNumLabel: TLabel
      Left = 44
      Top = 66
      Width = 63
      Height = 13
      Caption = 'Last Number:'
      Visible = False
    end
    object NameBrowserButton: TSpeedButton
      Left = 434
      Top = 36
      Width = 23
      Height = 22
      Hint = 'Browse for Directory'
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000CE0E0000C40E00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77777777777777777777000000000007777700333333333077770B0333333333
        07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
        77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
        7007777777770777070777777777700077777777777777777777}
      Visible = False
      OnClick = NameBrowserButtonClick
    end
    object VideostoreEdit: TEdit
      Left = 63
      Top = 38
      Width = 313
      Height = 21
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = 'f:\junk10.dat'
    end
    object VideostoreRadioButton: TRadioButton
      Left = 44
      Top = 20
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 1
      TabStop = True
      Visible = False
      OnClick = VideostoreRadioButtonClick
    end
    object FirstFileRadioButton: TRadioButton
      Left = 164
      Top = 20
      Width = 17
      Height = 17
      Enabled = False
      TabOrder = 2
      Visible = False
      OnClick = FirstFileRadioButtonClick
    end
    object LastNumEdit: TEdit
      Left = 112
      Top = 62
      Width = 65
      Height = 21
      TabOrder = 3
      Visible = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 104
    Width = 489
    Height = 297
    Caption = ' Parameters '
    TabOrder = 10
    object ChunkSizePart1Label: TLabel
      Left = 141
      Top = 108
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Frame Size'
    end
    object ChunkSizePart2Label: TLabel
      Left = 157
      Top = 131
      Width = 35
      Height = 13
      Caption = 'Custom'
    end
    object Label2: TLabel
      Left = 192
      Top = 28
      Width = 25
      Height = 13
      Caption = 'Step:'
    end
    object Label6: TLabel
      Left = 272
      Top = 28
      Width = 28
      Height = 13
      Caption = 'Jump:'
    end
    object Bevel1: TBevel
      Left = 16
      Top = 88
      Width = 457
      Height = 3
    end
    object Label1: TLabel
      Left = 14
      Top = 175
      Width = 39
      Height = 13
      Caption = 'Start TC'
    end
    object Label12: TLabel
      Left = 14
      Top = 199
      Width = 36
      Height = 13
      Caption = 'End TC'
    end
    object Label19: TLabel
      Left = 14
      Top = 223
      Width = 40
      Height = 13
      Caption = 'Duration'
    end
    object Label20: TLabel
      Left = 156
      Top = 175
      Width = 54
      Height = 13
      Caption = 'Start Frame'
    end
    object Label21: TLabel
      Left = 156
      Top = 199
      Width = 51
      Height = 13
      Caption = 'End Frame'
    end
    object Label22: TLabel
      Left = 155
      Top = 223
      Width = 59
      Height = 13
      Caption = 'Num Frames'
    end
    object Label23: TLabel
      Left = 298
      Top = 175
      Width = 46
      Height = 13
      Caption = 'Start Byte'
    end
    object Label24: TLabel
      Left = 298
      Top = 199
      Width = 43
      Height = 13
      Caption = 'End Byte'
    end
    object Label25: TLabel
      Left = 298
      Top = 223
      Width = 51
      Height = 13
      Caption = 'Num Bytes'
    end
    object Label11: TLabel
      Left = 299
      Top = 247
      Width = 57
      Height = 13
      Caption = 'Num Blocks'
    end
    object Label18: TLabel
      Left = 14
      Top = 247
      Width = 70
      Height = 13
      Caption = 'Timecode FPS'
    end
    object Label26: TLabel
      Left = 14
      Top = 33
      Width = 37
      Height = 13
      Caption = 'IO Align'
    end
    object Label27: TLabel
      Left = 16
      Top = 53
      Width = 34
      Height = 13
      Caption = 'IO Size'
    end
    object GigabyteLabel: TLabel
      Left = 155
      Top = 247
      Width = 63
      Height = 13
      Caption = 'File Size (GB)'
    end
    object MemoryRadioGroup: TRadioGroup
      Left = 362
      Top = 18
      Width = 105
      Height = 55
      Hint = 'Normally use default of VirtualAlloc()'
      Caption = 'Memory'
      ItemIndex = 1
      Items.Strings = (
        'malloc()'
        'VirtualAlloc()'
        'GlobalAlloc()')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object NoBufferingCheckBox: TCheckBox
      Left = 193
      Top = 51
      Width = 161
      Height = 17
      Hint = 'Normally, this flag is NOT changed'
      Caption = 'FILE_FLAG_NO_BUFFERING'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object FrameSizeRadioButtonList: TRadioButton
      Left = 199
      Top = 107
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = FrameSizeRadioButtonListClick
    end
    object FrameSizeRadioButtonEdit: TRadioButton
      Left = 199
      Top = 131
      Width = 17
      Height = 17
      TabOrder = 3
      OnClick = FrameSizeRadioButtonEditClick
    end
    object FrameSizeComboBox: TComboBox
      Left = 224
      Top = 103
      Width = 241
      Height = 23
      Style = csDropDownList
      Color = clMenu
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 6
      ParentFont = False
      TabOrder = 4
      Text = '8294400 HD444 10-bit'
      OnChange = FrameSizeRadioButtonListClick
      Items.Strings = (
        '700416 NTSC 8-bit'
        '829440 PAL 8-bit'
        '875520 NTSC 10-bit'
        '1037312 PAL 10-bit'
        '4147200 HD422 8-bit'
        '5529600 HD422 10-bit'
        '8294400 HD444 10-bit'
        '12746752 2K RGB 10-bit')
    end
    object FrameSizeEdit: TEdit
      Left = 224
      Top = 127
      Width = 241
      Height = 21
      TabOrder = 5
      Text = '4194304'
      OnChange = FrameSizeRadioButtonEditClick
    end
    object StepEdit: TEdit
      Left = 219
      Top = 24
      Width = 39
      Height = 21
      TabOrder = 6
      Text = '1'
    end
    object JumpEdit: TEdit
      Left = 312
      Top = 24
      Width = 41
      Height = 21
      TabOrder = 7
      Text = '0'
    end
    object StartTCEdit: TMaskEdit
      Left = 64
      Top = 166
      Width = 84
      Height = 23
      EditMask = '99:99:99:99;1;-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 11
      ParentFont = False
      TabOrder = 8
      Text = '00:00:00:00'
      OnExit = StartTCEditExit
    end
    object EndTCEdit: TMaskEdit
      Left = 64
      Top = 190
      Width = 83
      Height = 23
      EditMask = '99:99:99:99;1;-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 11
      ParentFont = False
      TabOrder = 9
      Text = '01:00:00:00'
      OnExit = EndTCEditExit
    end
    object DurTCEdit: TMaskEdit
      Left = 64
      Top = 214
      Width = 83
      Height = 23
      EditMask = '99:99:99:99;1;-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 11
      ParentFont = False
      TabOrder = 10
      Text = '01:00:00:00'
      OnExit = DurTCEditExit
    end
    object DurFrameEdit: TEdit
      Left = 224
      Top = 214
      Width = 61
      Height = 21
      TabOrder = 11
      Text = '86400'
      OnChange = DurFrameEditChange
      OnKeyPress = MovingAvgWarnEditKeyPress
    end
    object EndFrameEdit: TEdit
      Left = 224
      Top = 190
      Width = 61
      Height = 21
      TabOrder = 12
      Text = '86400'
      OnChange = EndFrameEditChange
      OnKeyPress = MovingAvgWarnEditKeyPress
    end
    object StartFrameEdit: TEdit
      Left = 224
      Top = 166
      Width = 61
      Height = 21
      TabOrder = 13
      Text = '0'
      OnChange = StartFrameEditChange
      OnKeyPress = MovingAvgWarnEditKeyPress
    end
    object StorageFPSEdit: TCSpinEdit
      Left = 107
      Top = 244
      Width = 40
      Height = 22
      EditorEnabled = False
      MaxValue = 60
      MinValue = 1
      TabOrder = 14
      Value = 24
      OnChange = StorageFPSEditChange
    end
    object NumFieldsComboBox: TComboBox
      Left = 64
      Top = 49
      Width = 113
      Height = 21
      Style = csDropDownList
      Color = clMenu
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ItemIndex = 1
      ParentFont = False
      TabOrder = 15
      Text = '2 Fields per IO'
      OnChange = FrameSizeRadioButtonListClick
      Items.Strings = (
        '1 Field per IO'
        '2 Fields per IO')
    end
    object NumChunksEdit: TEdit
      Left = 368
      Top = 238
      Width = 97
      Height = 21
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 16
      Text = '4194304'
    end
    object IOAlignmentComboBox: TComboBox
      Left = 64
      Top = 25
      Width = 113
      Height = 21
      Style = csDropDownList
      Color = clMenu
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ItemIndex = 0
      ParentFont = False
      TabOrder = 17
      Text = '512 - NTFS Chunk'
      OnChange = FrameSizeRadioButtonListClick
      Items.Strings = (
        '512 - NTFS Chunk'
        '1024 - 1K'
        '2048 - 2K'
        '4096 - 4K'
        '16384 - 16K'
        '1048576 - 1M')
    end
    object StartByteEdit: TEdit
      Left = 368
      Top = 166
      Width = 97
      Height = 21
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 18
      Text = '4194304'
    end
    object EndByteEdit: TEdit
      Left = 368
      Top = 190
      Width = 97
      Height = 21
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 19
      Text = '4194304'
    end
    object DurByteEdit: TEdit
      Left = 368
      Top = 214
      Width = 97
      Height = 21
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 20
      Text = '4194304'
    end
    object GigabyteEdit: TEdit
      Left = 224
      Top = 238
      Width = 61
      Height = 21
      TabOrder = 21
      Text = '667'
      OnExit = GigabyteEditExit
      OnKeyPress = MovingAvgWarnEditKeyPress
    end
  end
  object EnableFileWriteCheckBox: TCheckBox
    Left = 536
    Top = 786
    Width = 89
    Height = 17
    Anchors = [akRight, akBottom]
    Caption = 'Enable Writes'
    TabOrder = 11
    OnClick = EnableFileWriteCheckBoxClick
  end
  object IterationsEdit: TEdit
    Left = 767
    Top = 784
    Width = 24
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 12
    Text = '1'
  end
  object IOBar0m: TProgressBar
    Left = 727
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 13
  end
  object IOBar1m: TProgressBar
    Left = 711
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 14
  end
  object IOBar1p: TProgressBar
    Left = 759
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 15
  end
  object IOBar2p: TProgressBar
    Left = 775
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 16
  end
  object IOBar2m: TProgressBar
    Left = 695
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 17
  end
  object IOBar0p: TProgressBar
    Left = 743
    Top = 230
    Width = 15
    Height = 100
    Min = 0
    Max = 100
    Orientation = pbVertical
    Smooth = True
    Step = 1
    TabOrder = 18
  end
  object BufferPanel: TPanel
    Left = 504
    Top = 10
    Width = 297
    Height = 217
    BevelOuter = bvNone
    TabOrder = 19
    object Label28: TLabel
      Left = 12
      Top = 141
      Width = 273
      Height = 14
      Caption = '+ 8 7 6 5 4 3 2 1 +0- 1 2 3 4 5 6 7 8 -'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
    object Label29: TLabel
      Left = 7
      Top = 155
      Width = 105
      Height = 13
      Caption = 'Faster (Buffers Added)'
    end
    object Label30: TLabel
      Left = 195
      Top = 155
      Width = 97
      Height = 13
      Caption = 'Slower (Buffers Lost)'
    end
    object Label8: TLabel
      Left = 148
      Top = 197
      Width = 99
      Height = 13
      Caption = 'Max Frames in Buffer'
    end
    object Label34: TLabel
      Left = 142
      Top = 3
      Width = 84
      Height = 13
      Caption = '\/  Target Speed:'
    end
    object TargetIOSpeedLabel: TLabel
      Left = 231
      Top = 4
      Width = 43
      Height = 13
      Caption = '30.00 ms'
    end
    object IOBarRel1: TProgressBar
      Left = 8
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 0
    end
    object IOBarRel2: TProgressBar
      Left = 22
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 1
    end
    object IOBarRel3: TProgressBar
      Left = 36
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 2
    end
    object IOBarRel5: TProgressBar
      Left = 64
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 3
    end
    object IOBarRel4: TProgressBar
      Left = 50
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 4
    end
    object IOBarRel6: TProgressBar
      Left = 78
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 5
    end
    object IOBarRel7: TProgressBar
      Left = 93
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 6
    end
    object IOBarRel8: TProgressBar
      Left = 107
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 7
    end
    object IOBarRel9: TProgressBar
      Left = 121
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 8
    end
    object IOBarRel10: TProgressBar
      Left = 135
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 9
    end
    object IOBarRel11: TProgressBar
      Left = 149
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 10
    end
    object IOBarRel12: TProgressBar
      Left = 163
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 11
    end
    object IOBarRel13: TProgressBar
      Left = 177
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 12
    end
    object IOBarRel14: TProgressBar
      Left = 191
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 13
    end
    object IOBarRel15: TProgressBar
      Left = 205
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 14
    end
    object IOBarRel16: TProgressBar
      Left = 219
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 15
    end
    object IOBarRel17: TProgressBar
      Left = 233
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 16
    end
    object IOBarRel18: TProgressBar
      Left = 248
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 17
    end
    object IOBarRel19: TProgressBar
      Left = 262
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 18
    end
    object IOBarRel20: TProgressBar
      Left = 276
      Top = 18
      Width = 12
      Height = 121
      Min = 0
      Max = 100
      Orientation = pbVertical
      Smooth = True
      Step = 1
      TabOrder = 19
    end
    object BufferUsageBar: TProgressBar
      Left = 8
      Top = 176
      Width = 281
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      Step = 1
      TabOrder = 20
    end
    object MovingAvgNumSamplesEdit: TEdit
      Left = 256
      Top = 195
      Width = 33
      Height = 21
      TabOrder = 21
      Text = '10'
      OnExit = MovingAvgNumSamplesEditExit
      OnKeyPress = MovingAvgNumSamplesEditKeyPress
    end
  end
  object MovingAvgWarnCheckBox: TCheckBox
    Left = 40
    Top = 512
    Width = 129
    Height = 17
    Caption = 'Warn if moving avg <'
    TabOrder = 20
    OnClick = MovingAvgWarnCheckBoxClick
  end
  object MovingAvgWarnEdit: TEdit
    Left = 171
    Top = 511
    Width = 41
    Height = 21
    Enabled = False
    TabOrder = 21
    Text = '166'
    OnExit = MovingAvgWarnEditExit
    OnKeyPress = MovingAvgWarnEditKeyPress
  end
  object Memo: TMemo
    Left = 8
    Top = 422
    Width = 783
    Height = 86
    TabStop = False
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 22
  end
  object MaxSpeedCheckBox: TCheckBox
    Left = 514
    Top = 312
    Width = 161
    Height = 17
    Caption = 'Max Speed (FPS)'
    TabOrder = 23
    OnClick = MaxSpeedCheckBoxClick
  end
  object MaxSpeedEdit: TEdit
    Left = 625
    Top = 309
    Width = 26
    Height = 21
    Enabled = False
    TabOrder = 24
    Text = '30'
    OnChange = MaxSpeedEditChange
    OnExit = MovingAvgWarnEditExit
    OnKeyPress = MovingAvgWarnEditKeyPress
  end
  object BufferWarnCheckBox: TCheckBox
    Left = 232
    Top = 512
    Width = 138
    Height = 17
    Caption = 'Warn if buffer <           % '
    TabOrder = 25
    OnClick = BufferWarnCheckBoxClick
  end
  object BufferWarnEdit: TEdit
    Left = 328
    Top = 510
    Width = 23
    Height = 21
    Enabled = False
    TabOrder = 26
    Text = '50'
    OnExit = BufferWarnEditExit
    OnKeyPress = MovingAvgWarnEditKeyPress
  end
  object FirstFileOpenDialog: TOpenDialog
    Left = 440
    Top = 64
  end
end
