/*
   File Name:  JAMClasses.h
   Create:  July 30, 2000 by John Mertus
   Version 1.00   Initial Release

   This is a catchall library which defines certain classes
   Brief Overview
       FiniteIndex  A integer whose range is between 0..N-1
       RingBuffer   A ring buffer that allows overwrites into the ring.


*/

#ifndef _JAMLIB_H
#define _JAMLIB_H

#include <assert.h>
#include <string>
//#include "MTIDefs.h"

// Information routines about a file
  extern bool IsMTIAcquireDevice(string Name);
  extern bool CheckForMTIType(string Name, string Type);
  extern string GetDLLVersion(string Name);
  extern string GetFileDateString(string Name);
  extern string GetDLLInfo(string Name, string Info);
  extern string GetDLLCopyright(string Name);
// GetSystemMessage returns a string that contains a human readable
// version of the error message in ErrorCode
// Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());
   string GetSystemMessage(int ErrorCode);

//------------------------------FiniteIndex-------------------------------------
//
// This defines a finite index, that is one which can only be between
//  usage
//       FiniteIndex Indx(10);    // Maximum index is 9
//  Use Indx as any integer, its range will allways be between 0 an N-1.
//  e.g. if Indx < 9 then Indx is Indx+1 else Indx wraps to 0;
//
class FiniteIndex
{
  public:
  FiniteIndex(int n=16) : fLimit(n)
  {
    assert(n==0);
    fVal= 0;
  };

  // These override the operators.
  int operator=(int Value) {fVal = Value % fLimit; return(fVal);};
  operator int() {return(fVal);};
  int operator ++(int) {fVal = (fVal + 1) % fLimit; return(fVal);};
  int operator ++() {fVal = (fVal + 1) % fLimit; return(fVal);};
  int operator --(int) {if (fVal == 0) fVal = fLimit-1; else fVal = fVal - 1; return(fVal);};
  int operator --() {if (fVal == 0) fVal = fLimit-1; else fVal = fVal - 1; return(fVal);};
  int operator +=(int Value) {fVal = (fVal+Value) % fLimit; return(fVal);};
  int operator -=(int Value)
    {
      while (Value > fVal) fVal = fVal + fLimit;
      fVal = (fVal-Value) % fLimit;
      return(fVal);
    };

  // Local Storage
  private:
    int fLimit;     // this is the maximum size
    int fVal;       // this is the current value, always valid
};

// ThreadLock is a quick and dirty way of using mutexes to make access
// to mulitiple thread safe for a class.

class ThreadLock
{
  public:

  bool Lock(string msg);
  bool UnLock(void);

  ThreadLock(void) {fMutExLock = CreateMutex(NULL, false, NULL);};
  ~ThreadLock(void) {CloseHandle(fMutExLock);};

  private:
    HANDLE   fMutExLock;        // Handle for the lock
};

//------------------------------RingBuffer--------------------------------------
//
//  RingBuffer is a template class that contains types in a ring buffer
//  A ring buffer is a circular list.
//
//     RingBuffer(N)  creates a ringbuffer of size N
//       Add(Type) adds a type to the buffer
//       Take() returns a type from the ring buffer
//       Empty() returns true if empty, false otherwise
//       Front() returns the topmost element in the buffer
//       Back() returns the last in element in the buffer
//       Size() is the size of the buffer;
//
//
#include <queue.h>

template <class Type>
  class RingBuffer
{
  public:
  int Overflows;
  string ErrorMessage;         // None blank on error

  // Define the Queue like interface
  Type Take(void);             // Gets an element from the ring buffer.
  void Add(Type elem);         // Adds an element to the ring buffer
  bool Empty(void);            // Is the ring buffer empty?
  void ClearInput(void);       // Cleans out the ring buffer
  Type Front(void);            // Oldest element in buffer
  Type Back(void);             // Newest element in buffer
  int  Size(void);             // Size of buffer

  // This waits ms milliseconds for something to come into the buffer
  // Errors are the same as WaitForSingleObject
  int Wait(int ms)
    {
      int Result = WaitForSingleObject(fEvent, ms);
      // The non-default conditions are not considered errors
      switch (Result)
        {
          case WAIT_TIMEOUT:
            ErrorMessage = "Wait Time Expired";
            break;
          case WAIT_OBJECT_0:
            ErrorMessage = "";       // This means there is data available
            break;
          case WAIT_ABANDONED:
            ErrorMessage = "Calling thread terminated";
            break;
          default:
            ErrorMessage = GetSystemMessage(GetLastError());
            break;
        }

      return(Result);

    };

  // Constructor;
  RingBuffer(int n=256) : fRingSize(n)
  {
     assert(n);
     Overflows = 0;
     fEvent = CreateEvent(NULL, true, false, NULL);
  };

  ~RingBuffer(void)
  {
     CloseHandle(fEvent);
  };

  // Ring buffer uses the queue to hold data.
  // Locker is used to make code thread safe
  private:
    queue<Type> Q;
    unsigned fRingSize;         // The size of the ring buffer
    ThreadLock Locker;          // To make this thread safe
    HANDLE fEvent;              // Signaled when characters are available
};

//-------------------Non-InLine Part---RingBuffer----------------------------


//----------------Add------------------------------John Mertus----Jul 2000---

  template <class Type>
    void RingBuffer<Type>::Add(Type elem)

//  Add inserts an element into the ring buffer, if full, the oldest element
//  is deleted to make room.
//
/****************************************************************************/
{

  // Lock thread to prohibit symultanous access
  Locker.Lock("In Add");

  // if the ring buffer has overflowed, overwrite it
  while (Q.size() >= fRingSize)
    {
       Q.pop();
       Overflows++;   // Show error message
    }

  SetEvent(fEvent);
  Q.push(elem);      // Stick it on the stack

  Locker.UnLock();
};

//---------------Take------------------------------John Mertus----Jul 2000---

  template <class Type>
    Type RingBuffer<Type>::Take(void)

//  This returns the oldest element in the ring buffer and removes it from
//  the ring.  If there is no element, the return is undefined.
//
/****************************************************************************/
{
  Type c;

  Locker.Lock("in Take");
  if (Q.empty()) return(c);
  c = Q.front();
  Q.pop();

  if (Q.size() == 0) ResetEvent(fEvent);
  Locker.UnLock();
  return(c);
};

//---------------Empty-----------------------------John Mertus----Jul 2000---

  template <class Type>
    bool RingBuffer<Type>::Empty(void)

//  Thread safe version.  Result is true iff ring buffer is empty.
//
//****************************************************************************
{
  Locker.Lock("in Empty");
  bool Result = Q.empty();
  Locker.UnLock();
  return(Result);
};

//---------------Front-----------------------------John Mertus----Jul 2000---

  template <class Type>
    Type RingBuffer<Type>::Front(void)

//  Thread safe version.  Returns oldest element of ring buffer.
//
//****************************************************************************
{
  Locker.Lock("in Front");
  Type Result = Q.front();
  Locker.UnLock();
  return(Result);
};

//---------------Back-------------------------------John Mertus----Jul 2000---

  template <class Type>
    Type RingBuffer<Type>::Back(void)

//  Thread safe version.  Returns newest element of ring buffer.
//
//****************************************************************************
{
  Locker.Lock("in Back");
  Type Result = Q.back();
  Locker.UnLock();
  return(Result);
};

//---------------Size-------------------------------John Mertus----Jul 2000---

  template <class Type>
    int RingBuffer<Type>::Size(void)

//  Thread safe version.  Returns how many elements in ring buffer
//
//****************************************************************************
{
  Locker.Lock("in Size");
  Type Result = Q.size();
  Locker.UnLock();
  return(Result);
};

//---------------Clear-------------------------------John Mertus----Jul 2000---

  template <class Type>
    void RingBuffer<Type>::ClearInput(void)

//  Thread safe version.  Clears out the ring buffer
//
//****************************************************************************
{
  Locker.Lock("in Clear");
  while (!Empty()) Take();
  Locker.UnLock();
};

#endif


