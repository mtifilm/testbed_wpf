/*
     File Name:  HRTimer.h
    Create:  March 2, 2000
    Release 1.00
*/
//---------------------------------------------------------------------------
#ifndef HRTimerH
#define HRTimerH
/*
  This defines a high resolution timer.  There are two important
  member functions
      StartTimer    Which start counting
      ReadTimer     Which returns the current count in millisecond;

  Note: This is a MACHINE SPECIFIC class.

*/
/* stamp_t: SGI-specific type for USTs and MSCs */
/* there is no "ust_t": use stamp_t ; there is no "msc_t": check library */
#include <string>

#include "machine.h"

#ifndef _STAMP_T
#define _STAMP_T
typedef INT64 stamp_t; /* used for USTs and often MSCs */
#endif


#define RDTSC_Stamp(STAMP_INT64)   \
__asm                              \
{                                  \
    cpuid                          \
    rdtsc                          \
    lea    ebx, timeA              \
    mov [ebx+4], edx               \
    mov [ebx+0], eax               \
}

class HRTimer
 {
   public:
     HRTimer();      // Construct it
     double ReadTimer();
     void StartTimer();
     void ShowTimer();     // Pops up a window with the time in it;
     string ReadTimerStr();

   private:
     LONGLONG fClockFreq;   // The HR clock frequency filled in at construct time
     LONGLONG fStartCount;  // The start fills this in;
 };

//
// Procedures
//
   double GetMsFromUTC(INT64 UTC);

//---------------------------------------------------------------------------
#endif
