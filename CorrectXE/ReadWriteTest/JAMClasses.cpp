/*
   File Name:  JAMClasses.cpp
   Create:  July 30, 2000 by John Mertus

   This is a catchall library which defines certain classes
*/

#include <sstream>
#include <JAMClasses.h>

/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

  string GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  string Result = "";

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       return(Result);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  Result = cErrorMessage;
  return(Result);
};

/*---------------Lock-----------------------------John Mertus----Jul 2000---*/

    bool ThreadLock::Lock(string msg)

/*   This waits for the thread to become available and then locks it.       */
/*  msg is displayed if the lock fails in one second.                       */
/*                                                                          */
/****************************************************************************/
{
   bool retry;

   do
     {
       retry = false;
       if (WaitForSingleObject(fMutExLock, 5000) == WAIT_TIMEOUT)
         {
//           int mb = MTIShowSevereError(0, "Lock failed to clear\n" + msg);
//           retry = (mb == IDIGNORE);
        }
     } while (retry);

   return(true);
}

/*---------------WaitAndLock----------------------John Mertus----Jul 2000---*/

      bool ThreadLock::UnLock(void)

/*   This tells the thread that the data can now be safely changed.         */
/*                                                                          */
/****************************************************************************/
{
   return(ReleaseMutex(fMutExLock));
}

//--------------GetDLLInfo---------------------John Mertus Jan 2001---

       string GetDLLInfo(string Name, string Info)

//  Return information about a file. the following information is available
//
//   'CompanyName', 'FileDescription', 'FileVersion', 'InternalName',
//   'LegalCopyright', 'LegalTradeMarks', 'OriginalFilename',
//   'ProductName', 'ProductVersion', 'Comments'

//
//*********************************************************************
{
  DWORD dwLen;
  void *Value;
  unsigned int Len;

  int n = GetFileVersionInfoSize(__TEXT((char *)Name.c_str()), &dwLen);
  if (n == 0) return("");
  void *Data = malloc(n);
  if (!GetFileVersionInfo((char *)Name.c_str(),0,n,Data))
    {
       free(Data);
       return("");
    }

  string s = "StringFileInfo\\040904E4\\";
  s += Info;
  if (!VerQueryValue(Data,__TEXT((char *)s.c_str()),&Value, &Len))
    {
       free(Data);
       return("");
    }

  string Result = (char *)Value;
  free(Data);
  return(Result);
}

//--------------GetDLLVersion------------------John Mertus Jan 2001---

     string GetDLLVersion(string Name)

//  This returns the current dll version as a string.
//
//*********************************************************************
{
  string s = GetDLLInfo(Name, "FileVersion");
  if (s == "") return("Unknown");

  int n = s.find('.');
  string V = s.substr(n+1, s.length());
  int m = V.find('.') + n +1;
  V = s.substr(0, m);
  V = V + ", Release " + s.substr(m+1, s.length());

  return(V);

}

//--------------GetDLLCopyright------------------John Mertus Jan 2001---

     string GetDLLCopyright(string Name)

//  This returns the current dll copyright as a string.
//
//*********************************************************************
{
  return(GetDLLInfo(Name, "LegalCopyright"));
}

//--------------GetFileDateString---------------John Mertus Jan 2001---

   string GetFileDateString(string Name)

//  This returns the last write date of a file.
//
//*********************************************************************
{

  const string MonthsStr[] =
    {"January", "February", "March", "April",
     "May", "June", "July", "Augest",
     "September", "October","November", "December"};

  FILETIME CreationTime;
  SYSTEMTIME SystemTime;
  // Get the time of the file creation
  HANDLE h = CreateFile(Name.c_str(), GENERIC_READ,
             FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
             OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);

//  HANDLE h = (void *)FileOpen(Name.c_str(), fmOpenRead  || fmShareDenyNone);
  if (h == INVALID_HANDLE_VALUE) return("");

  // Now find the time
  if (!GetFileTime(h, NULL, NULL, &CreationTime))
    {
       CloseHandle(h);
       return("");
    }           

  CloseHandle(h);  // No need for it anymore

  if (!FileTimeToSystemTime(&CreationTime, &SystemTime)) return("");

  // Create our date
  ostringstream str;
  str << MonthsStr[SystemTime.wMonth-1] << " " << SystemTime.wDay << ", " << SystemTime.wYear;

  return(str.str());
}


//--------------CheckForMTIType----------------John Mertus Jan 2001---

         bool CheckForMTIType(string Name, string Type)

//  Return true if the comment string matches Type
//
//*********************************************************************
{
 return(Type == GetDLLInfo(Name, "Comments"));
}

//--------------IsMTIAcquireDevice--------------John Mertus Jan 2001---

         bool IsMTIAcquireDevice(string Name)

//  Return true if the DLL defines an MTI acquire device.
//
//*********************************************************************
{
  return(CheckForMTIType(Name, "MTI Acquire Device"));
}

