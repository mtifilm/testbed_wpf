//---------------------------------------------------------------------------
// TTT - add restriction to numbers for StepEdit
//
//  1.6 -   1/ 7/2009- djm - temporarily disabled File IO option
//                           (wasn't working before) from what I can tell
//                         - added IO Align dropdown:
//                           rounds up IO size to commonly used 'Fast IO Boundaries'
//                         - added IO Size dropdown:
//                           can select 1 or 2 fields per IO
//                           (we commonly write 1 frame at a time in our apps now)
//                         - relabeled everything for ease usability
//                         - added start/end timecode boxes
//                         - added start/end frame boxes
//                         - added start/end byte & chunk read-only boxes
//                           (maybe useful for people making videostores with this app)
//                         - speed uses bytes per sec, which is calculated from the
//                           rounded-up IO size
//                         - added logging all io' to csv file
//                           eg: c:\mtirw\write_YYYY-MM-DD_HH-MM-SS.csv
//                         - separated warning/summary memo box from details memo box
//                         - tabularized output to memo and csv files with monospace font
//                         - added historgram for mean and std dev for ios
//                         - added histogram for io speed as a fraction/multiple of
//                           target speed per io (according to chosen frame size and rate)
//                         - added bar that shows the percent full of an theoretical
//                           ram frame buffer
//                         - calculate moving average using new algorithm that takes into acct
//                           the total time doing the io's
//                         - added warning checkbox for buffer underflow/overflow
//  1.5h -  3/ 3/2007- mpr - added MTI logo.  Updated (c) to 2007.
//                         - added jump to mathtech.com website if click
//                           on logo or website address.
//  1.5g                   - note this fix is only on M5 and what it did
//                           was change the default Units to "1" from "512".
//                           This Unit change was not done on the Trunk
//                           since CD prefers a default of "512".
//  1.5f - 11/22/2004- kmm - Fixed a bug where file size and starting pos
//                           were limited to 32 bits.
//                         - added "jump" feature which will change the
//                           I/O position to a new random location
//                         - added a "units" combo box.  This allows
//                         - the file size and starting position to be
//                         - entered as bytes, blocks, KB, MB, TB.
//  1.5e - 6/11/2004 - mpr - Fixed bug in Step value in Videostore where it
//                           was not seeking.
//  1.5d - 6/ 9/2004 - mpr - Added Step value instead of default increment of 1
//  1.5c - 6/ 8/2004 - mpr - moved timing so not included in GUI.
//                         - changed throttling to be based on moving
//                           average, not average
//  1.5b - 6/ 7/2004 - mpr - modularized code a bit.
//                         - changed default to VirtualAlloc() - note
//                           effective default was already VirtualAlloc()
//                           but GUI said it was GlobalAlloc()
//                         - added Clear Memo Area button
//  1.5a - 6/ 7/2004 - mpr - added option to Write Files if CheckBox checked
//  1.5  - 6/ 4/2004 - mpr - finished filename (File Sequence) Mode
//                         - changed look of interface a bit
//  1.4a - 6/ 3/2004 - mpr - added Moving Average Warning - started code
//                           for filename mode
//  1.4  - 6/ 1/2004 - mpr - added Moving Average & Show Each I/O checkbox
//  1.3t - 5/28/2004 - mpr - added Max Speed Throttle
//                         - merged Read/Write into 1 function
//
//---------------------------------------------------------------------------
#include <stdio.h>
#include <vcl.h>
#include <io.h>
#include <fcntl.h>
#include <vector>
#include <math.h>
#include <string>
#include <fstream.h>
#include <time.h>
#pragma hdrstop


#include "CreateFileUnit.h"
#include "HRTimer.h"
bool       GLBL_bStopFlag              = false;
int        GLBL_iNumRuns               = 0;
AnsiString GLBL_astrVideostore         = "";
AnsiString GLBL_astrFirstFile          = "";
AnsiString GLBL_astrVideostoreFilesize = "";
AnsiString GLBL_astrFirstFileFilesize  = "";

#define MEMORY_MALLOC        0 /* TTT should be figured out from RadioGroup */
#define MEMORY_VIRTUAL_ALLOC 1
#define MEMORY_GLOBAL_ALLOC  2

#define IO_MODE_READ         1
#define IO_MODE_WRITE        2

/* Macros */
#define ButtonsRunning \
  CreateFileButton->Enabled        = false; \
  ReadFileButton->Enabled          = false; \
  WriteFileButton->Enabled         = false; \
  StopButton->Enabled              = true;  \
  QuitButton->Enabled              = false; \
  MovingAvgNumSamplesEdit->Enabled = false; \
  MaxSpeedCheckBox->Enabled        = false; \
  MaxSpeedEdit->Enabled            = false;

#define ButtonsStopped \
  ReadFileButton->Enabled          = true;  \
  if (VideostoreRadioButton->Checked)       \
  {                                         \
    WriteFileButton->Enabled       = true;  \
    CreateFileButton->Enabled      = true;  \
  }                                         \
  else                                      \
  {                                         \
    CreateFileButton->Enabled      = false; \
    if (EnableFileWriteCheckBox->Checked)   \
      WriteFileButton->Enabled     = true;  \
    else                                    \
      WriteFileButton->Enabled     = false; \
  }                                         \
  StopButton->Enabled              = false; \
  QuitButton->Enabled              = true;  \
  MovingAvgNumSamplesEdit->Enabled = true;  \
  MaxSpeedCheckBox->Enabled        = true;  \
  if (MaxSpeedCheckBox->Checked)            \
    MaxSpeedEdit->Enabled   = true;         \
  else                                      \
    MaxSpeedEdit->Enabled   = false;

/* Forward Declarations */
bool GetBytesPerSector(char *caFilename, unsigned long *ulpBytesPerSector);
void ErrorBox (void);
bool GetBaseIndexAndExtension(AnsiString s, char *cpBase, char *cpIndex,
                              char *cpExt, char *cpBaseNoPath);
MTI_UINT64 GetFileLength(const string &Name);

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "PERFGRAP"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  // Initialize these upon startup
  GLBL_astrVideostore         = VideostoreEdit->Text;
  GLBL_astrVideostoreFilesize = EndFrameEdit->Text;
  GLBL_astrFirstFile          = "";
  GLBL_astrFirstFileFilesize  = 0;

  MovingAvgNumSamplesEditExit(Form1);
  MaxSpeedEditExit(Form1);
  BufferWarnEditExit(Form1);
  MovingAvgWarnEditExit(Form1);
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

/* Create File */
void __fastcall TForm1::CreateFileButtonClick(TObject *Sender)
{
  LARGE_INTEGER  llFileSizeBytes;
  LARGE_INTEGER  llSeekPos;
 // long           llSeekPos;
  unsigned long  ulNumBytesWritten;
  HANDLE         hHandle;
  HRTimer        hrt;
  double         dMillisecUsed  = 0.0;
  double         dMBperSec      = 0.0;
  char           caOutput[512];
  char           caFilename[512];

  /* Get Parameters */
  strcpy(caFilename, VideostoreEdit->Text.c_str());
  sscanf(EndByteEdit->Text.c_str(), "%I64d", &llFileSizeBytes.QuadPart);
  //llFileSizeBytes.QuadPart *= getUnit ();
  llSeekPos.QuadPart       = llFileSizeBytes.QuadPart - 1;

  StatusLabel->Caption = "Opening...";
  Application->ProcessMessages();
  hrt.StartTimer();
  hHandle = CreateFile(caFilename, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
            FILE_FLAG_WRITE_THROUGH, NULL);

  StatusLabel->Caption = "Seeking...";
  Application->ProcessMessages();
  SetFilePointerEx(hHandle, llSeekPos, NULL, FILE_BEGIN);
//  SetFilePointer(hHandle, llSeekPos, &llSeekPos + 4, FILE_BEGIN);
  WriteFile(hHandle, "1", 1, &ulNumBytesWritten, NULL);

  StatusLabel->Caption = "Closing...";
  Application->ProcessMessages();
  CloseHandle(hHandle);

  StatusLabel->Caption = "Done.";
  Application->ProcessMessages();

  dMillisecUsed = hrt.ReadTimer();
  dMBperSec = (llFileSizeBytes.QuadPart / (double)(1024.0 * 1024.0))
              / (dMillisecUsed / 1000.0);

  sprintf(caOutput, "%.2f MB/s", dMBperSec);

  ShowMessage(caOutput);
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

/* Quit */
void __fastcall TForm1::QuitButtonClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

/* Write File */
void __fastcall TForm1::WriteFileButtonClick(TObject *Sender)
{
  UpdateGUISettingsDisplay();
  DoFileIO(IO_MODE_WRITE);
}

//---------------------------------------------------------------------------


/* Read File */
void __fastcall TForm1::ReadFileButtonClick(TObject *Sender)
{
  UpdateGUISettingsDisplay();
  DoFileIO(IO_MODE_READ);
}

void TForm1::DoFileIO(int iReadOrWrite)
{
  LARGE_INTEGER  llFileSizeBytes;
  LARGE_INTEGER  llIndex;
  LARGE_INTEGER  llNumBytesDone;
  LARGE_INTEGER  llNumChunksToDo;
  LARGE_INTEGER  llChunksLeftToDo;
  LARGE_INTEGER  llStartingByte;
  LARGE_INTEGER  llSeekPos;
  LARGE_INTEGER  llBytesToDo;
  LARGE_INTEGER  llChunkSizeBytes;
  LARGE_INTEGER  llStep;
  LARGE_INTEGER  llSeekIncrement;
  long           lChunkSizeBlocks = 0;
  long           lFrameSizeBytes = 0;
  long           lChunkSizeBytes = 0;
  long           lIndex;
  long           lBytesToDo;
  int            iIOAlignment;
  int            iNumFieldsPerIO;
  double         dMillisecThisRun       = 0.0;
  double         dMillisecThisRunInclThrot = 0.0;
  double         dMillisecUsed          = 0.0;
  double         dMillisecUsedInclThrot = 0.0;
  double         dMBperSecAvg           = 0.0;
  double         dMBperSecLast          = 0.0;
  double         dMBperSecSum           = 0.0;
  double         dMillisecSlept         = 0.0;
  unsigned long  ulNumBytesDone;
  HANDLE         hHandle;
  HRTimer        hrt;
  unsigned char *ucpData = NULL;
  char           caAlpha[] = "abcdefghijklmnopqrstuvwxyz ";
  char           caOutputLine[512];
  char           caFilename[512];
  char           caShortFilename[512];
  char           caDiskname[512];
  char           caCaption[512];
  char           caMessage[512];
  char           caMBs[512];
  char          *cpPosBackSlash;
  int            iPosBackSlash;
  int            iIterations, iIterationsCompleted, i;
  HANDLE         hMem;
  float          fProgressPerc;
  unsigned long  ulSectorsPerCluster, ulBytesPerSector = 0,
                 ulNumberOfFreeClusters, ulTotalNumberOfClusters;
  char           caModePastTense[32];
  char           caModeGerund[32];
  char           caReadOrWriteFile[32];
  char           caBase[512];
  char           caBaseNoPath[512];
  char           caIndex[512];
  char           caExt[512];
  int            iStartIndex;
  int            iStopIndex;
  int            iCurrIndex;
  int            iTotalFiles;
  int            iNoBufferingFlag;
  int            iIndexLength;
  bool           bShowFlag;
  bool           bThrottleHappened     = false;
  int            iNumMovingAvgWarnings = 0;
  AnsiString     astrFilename;
  bool           bDoThrottle;
  double         dMillisecToSleep;
  int            iStep                 = 1;
  int            iJump                 = 0;
  int            iJumpCount            = 0;

  /* Initialization */
  if (iReadOrWrite == IO_MODE_READ)
  {
    strcpy(caModePastTense,   "Read");
    strcpy(caModeGerund,      "Reading");
    strcpy(caReadOrWriteFile, "ReadFile");
  }
  else if (iReadOrWrite == IO_MODE_WRITE)
  {
    strcpy(caModePastTense,   "Wrote");
    strcpy(caModeGerund,      "Writing");
    strcpy(caReadOrWriteFile, "WriteFile");
  }
  else   // Can't happen
  {
    ShowMessage("Illegal IO_MODE (internal error)");
    return;
  }

  GLBL_bStopFlag = false;
  ButtonsRunning;

  /* Get Parameters */
  astrFilename = VideostoreEdit->Text;
  strcpy(caFilename, VideostoreEdit->Text.c_str());
  if (strcmp(caFilename, "") == 0)
  {
    ShowMessage("Must set File Name");
    ButtonsStopped;
    return;
  }

  sscanf(EndByteEdit->Text.c_str(), "%I64d", &llFileSizeBytes.QuadPart);
  sscanf(NumFieldsComboBox->Text.c_str(), "%d", &iNumFieldsPerIO);
  sscanf(IOAlignmentComboBox->Text.c_str(), "%d", &iIOAlignment);
  sscanf(IterationsEdit->Text.c_str(), "%d", &iIterations);
  iStep = StepEdit->Text.ToInt();
  iJump = JumpEdit->Text.ToInt();
  iJumpCount = iJump;

  if (llFileSizeBytes.QuadPart == 0)
  {
    ShowMessage("Must set File Size");
    ButtonsStopped;
    return;
  }

  if (FrameSizeRadioButtonList->Checked)
    sscanf(FrameSizeComboBox->Text.c_str(), "%ld", &lFrameSizeBytes);
  else
  {
    sscanf(FrameSizeEdit->Text.c_str(), "%ld", &lFrameSizeBytes);
    if (lFrameSizeBytes == 0)
    {
      ShowMessage("Illegal Frame Size of 0");
      ButtonsStopped;
      return;
    }
  }

  sscanf(StartByteEdit->Text.c_str(), "%I64d", &llStartingByte.QuadPart);

  ///////////////////////////////////
  // Allocate Memory
  ///////////////////////////////////
  StatusLabel->Caption = "Allocating Memory...";
  Application->ProcessMessages();

  if (VideostoreRadioButton->Checked)
    lChunkSizeBytes = ((ceil((lFrameSizeBytes / 2.0)/ iIOAlignment)) * iIOAlignment) * iNumFieldsPerIO;
  else // Filename Mode
    lChunkSizeBytes = llFileSizeBytes.QuadPart;

  MemoryAlloc(&hMem, &ucpData, lChunkSizeBytes);

  if (ucpData == NULL)
  {
    ShowMessage("Memory Allocation failure.  Returning...");
    ButtonsStopped;
    return;
  }

  if (iReadOrWrite == IO_MODE_WRITE)
  {
    StatusLabel->Caption = "Filling Array...";
    Application->ProcessMessages();
    for (lIndex = 0; lIndex < lChunkSizeBytes; lIndex++)
    {
      ucpData[lIndex] = caAlpha[lIndex % strlen(caAlpha)];
    }
  }

  ///////////////////////////////////
  // Check sector size
  ///////////////////////////////////
  if (!GetBytesPerSector(caFilename, &ulBytesPerSector))
  {
    ButtonsStopped;
    MemoryFree(&hMem, &ucpData);
    return;
  }

  if (lChunkSizeBytes % ulBytesPerSector != 0)
  {
    sprintf(caOutputLine,
            "Chunk Size must be a multiple of Sector Size (%lu).  Returning...",
            ulBytesPerSector);
    ShowMessage(caOutputLine);
    ButtonsStopped;
    MemoryFree(&hMem, &ucpData);
    return;
  }

  ///////////////////////////////////
  // Do Reading / Writing
  ///////////////////////////////////
  dMBperSecSum = 0.0;

  if (FirstFileRadioButton->Checked)
  {
    // Loop thru each file
    if (!GetBaseIndexAndExtension(astrFilename, caBase, caIndex, caExt,
         caBaseNoPath))
    {
      ShowMessage("Can't find valid index in filename.  Returning...");
      ButtonsStopped;
      MemoryFree(&hMem, &ucpData);
      return;
    }

    // Get first & last indices
    iIndexLength = strlen(caIndex);
    sscanf(caIndex, "%d", &iStartIndex);
    sscanf(LastNumEdit->Text.c_str(), "%d", &iStopIndex);
    iTotalFiles = ((iStopIndex - iStartIndex) / iStep) + 1;

    sprintf(caCaption, "%s Files...", caModeGerund);
    StatusLabel->Caption = caCaption;

    // Initialize a bunch of variables
    dMillisecUsed           = 0.0;
    dMillisecThisRunInclThrot = 0.0;
    dMillisecUsedInclThrot  = 0.0;
    dMillisecSlept          = 0.0;
    llNumBytesDone.QuadPart = 0;
    iNumMovingAvgWarnings   = 0;
    bThrottleHappened       = false;
    bDoThrottle             = false;

    // Do each File
    for (i=iStartIndex; (i < iStopIndex + 1) && !GLBL_bStopFlag; i += iStep)
    {
      GLBL_iNumRuns++;
      sprintf(caIndex, "%0*d", iIndexLength, i);      // Pad with 0's as needed
      sprintf(caFilename, "%s%s%s", caBase, caIndex, caExt);
      sprintf(caShortFilename, "%s%s%s", caBaseNoPath, caIndex, caExt);

      iCurrIndex = ((i - iStartIndex) / iStep) + 1;
      sprintf(caCaption, "%s File %s (%d of %d)...", caModeGerund, caShortFilename,
               iCurrIndex, iTotalFiles);
      StatusLabel->Caption = caCaption;
      Application->ProcessMessages();

      hrt.StartTimer();

      double dMillisecBefore = hrt.ReadTimer();
      double dMillisecAfter;

      // Open the file
      hHandle = OpenFile(caFilename, iReadOrWrite);

      if (hHandle == INVALID_HANDLE_VALUE)
      {
        ShowMessage("Error Opening File");
        ButtonsStopped;
        MemoryFree(&hMem, &ucpData);
        return;
      }

      // Do the I/O
      if (!DoReadOrWriteFile(iReadOrWrite, hHandle, ucpData, lChunkSizeBytes,
                             &ulNumBytesDone))
      {
        ErrorBox();
        ButtonsStopped;
        CloseHandle(hHandle);
        MemoryFree(&hMem, &ucpData);
        return;
      }

      // Check the I/O
      if (!CheckIO(iReadOrWrite, ulNumBytesDone, lChunkSizeBytes, caMessage))
      {
        ShowMessage(caMessage);
        ButtonsStopped;
        CloseHandle(hHandle);
        MemoryFree(&hMem, &ucpData);
        return;
      }

      llNumBytesDone.QuadPart += ulNumBytesDone;

      // Close the file
      StatusLabel->Caption = "Closing File...";
      Application->ProcessMessages();

      CloseHandle(hHandle);

      // Get the timing info
      dMillisecAfter         = hrt.ReadTimer();
      dMillisecThisRun       = dMillisecAfter - dMillisecBefore;
      dMillisecThisRunInclThrot = (dMillisecThisRun + dMillisecSlept);
      dMillisecUsed          += dMillisecThisRun;
      dMillisecUsedInclThrot += (dMillisecThisRun + dMillisecSlept);

      // Show results for this run
      bShowFlag = (i < iStopIndex + 1);
      LARGE_INTEGER llIndex;
      LARGE_INTEGER llTotalFiles;
      llIndex.QuadPart      = iCurrIndex - 1;
      llTotalFiles.QuadPart = iTotalFiles;
      CalcAndShow(iReadOrWrite, llIndex, llTotalFiles, llNumBytesDone, ulNumBytesDone,
                  dMillisecUsed, dMillisecThisRun, dMillisecThisRunInclThrot,
                  &iNumMovingAvgWarnings, bShowFlag,
                  dMillisecUsedInclThrot, &bDoThrottle, &dMillisecToSleep);

      // Possible Throttling
      dMillisecSlept    = 0.0;
      bThrottleHappened = false;

      if (bDoThrottle)
      {
        double dMillisecBeforeThrottle = hrt.ReadTimer();
        Sleep(dMillisecToSleep);
        double dMillisecAfterThrottle  = hrt.ReadTimer();
        dMillisecSlept = dMillisecAfterThrottle - dMillisecBeforeThrottle;
        bThrottleHappened = true;
      }

      if (!GLBL_bStopFlag)
      {
        if (dMillisecThisRun == 0.0)       // Avoid possible divide by 0
          dMillisecThisRun = 0.000001;

        dMBperSecLast = ((double)llFileSizeBytes.QuadPart / (double) (1024.0 * 1024.0))
                    / (dMillisecThisRun / 1000);
        dMBperSecSum += dMBperSecLast;
        ShowIterationMessage(caModePastTense, bThrottleHappened, iCurrIndex,
                              iTotalFiles, dMBperSecLast, iNumMovingAvgWarnings);
      }
    }

    StatusLabel->Caption = "Done.";
    Application->ProcessMessages();

    ShowAverageMessage(iCurrIndex, dMBperSecSum);
  }
  else  // Videostore (aka rawdisk) mode
  {
    llStep.QuadPart           = iStep;
    llChunkSizeBytes.QuadPart = lChunkSizeBytes;
    llBytesToDo.QuadPart      = (llFileSizeBytes.QuadPart - llStartingByte.QuadPart) / iStep;
    llNumChunksToDo.QuadPart  = (llBytesToDo.QuadPart / llChunkSizeBytes.QuadPart);

    // Loop thru each iteration
    iIterationsCompleted = 0;
    for (i=0; (i < iIterations) && !GLBL_bStopFlag; i++)
    {
      GLBL_iNumRuns++;
      StatusLabel->Caption = "Opening File...";
      Application->ProcessMessages();

      // Open the file
      hHandle = OpenFile(caFilename, iReadOrWrite);

      if (hHandle == INVALID_HANDLE_VALUE)
      {
        ShowMessage("Error Opening File");
        ButtonsStopped;
        MemoryFree(&hMem, &ucpData);
        return;
      }

      // Seek to start position
      llSeekPos.QuadPart       = llStartingByte.QuadPart;
      llSeekIncrement.QuadPart = llChunkSizeBytes.QuadPart
                                   * llStep.QuadPart;

      StatusLabel->Caption = "Seeking...";
      Application->ProcessMessages();
      SetFilePointerEx(hHandle, llSeekPos, NULL, FILE_BEGIN);

      sprintf(caCaption, "%s File %d of %d...", caModeGerund, i + 1, iIterations);
      StatusLabel->Caption = caCaption;
      Application->ProcessMessages();

      // Initialize a bunch of variables
      dMillisecUsed           = 0.0;
      dMillisecUsedInclThrot  = 0.0;
      dMillisecSlept          = 0.0;
      llNumBytesDone.QuadPart = 0;
      iNumMovingAvgWarnings   = 0;
      bThrottleHappened       = false;
      bDoThrottle             = false;

      hrt.StartTimer();  // This could arguably go before the CreateFile()

      for (llIndex.QuadPart = 0;
           (llIndex.QuadPart < llNumChunksToDo.QuadPart) && !GLBL_bStopFlag;
           llIndex.QuadPart++)
      {
        if (llIndex.QuadPart % 50 == 0)
         {
          char caMessage[256];
          sprintf (caMessage, "Pos:  %I64d", llSeekPos.QuadPart );
          LabelJump->Caption = caMessage;
         }

        llChunksLeftToDo.QuadPart = llNumChunksToDo.QuadPart
                                      - (llIndex.QuadPart + 1);

        double dMillisecBefore = hrt.ReadTimer();
        double dMillisecAfter;

        // Seek
        SetFilePointerEx(hHandle, llSeekPos, NULL, FILE_BEGIN);

        // Do the I/O
        if (!DoReadOrWriteFile(iReadOrWrite, hHandle, ucpData, lChunkSizeBytes,
                               &ulNumBytesDone))
        {
          ErrorBox();
          ButtonsStopped;
          CloseHandle(hHandle);
          MemoryFree(&hMem, &ucpData);
          return;
        }

        // Check the I/O
        if (!CheckIO(iReadOrWrite, ulNumBytesDone, lChunkSizeBytes, caMessage))
        {
          ShowMessage(caMessage);
          ButtonsStopped;
          CloseHandle(hHandle);
          MemoryFree(&hMem, &ucpData);
          return;
        }

        // Get the timing info
        dMillisecAfter         = hrt.ReadTimer();
        dMillisecThisRun       = dMillisecAfter - dMillisecBefore;
        dMillisecUsed          += dMillisecThisRun;
        dMillisecUsedInclThrot += (dMillisecThisRun + dMillisecSlept);
        dMillisecThisRunInclThrot = (dMillisecThisRun + dMillisecSlept);
        llNumBytesDone.QuadPart += ulNumBytesDone;
        llSeekPos.QuadPart      += llSeekIncrement.QuadPart;

        if (iJump != 0)
         {
          if (iJumpCount == iJump)
           {
            //  change the seek position to some other value

            double dRand = (double)rand() / (double)RAND_MAX;
            LARGE_INTEGER llJumpSize;
            llJumpSize.QuadPart = lChunkSizeBytes;
            llJumpSize.QuadPart *= iJump;

            llSeekPos.QuadPart = dRand * (double)(llNumChunksToDo.QuadPart -
                        iJump);
            llSeekPos.QuadPart *= lChunkSizeBytes;
            llSeekPos.QuadPart += llStartingByte.QuadPart;

            iJumpCount = 0;
           }
          iJumpCount++;
         }

        if (llSeekPos.QuadPart >= llFileSizeBytes.QuadPart)
         {
          llSeekPos.QuadPart -= lChunkSizeBytes;
          if (llSeekPos.QuadPart < 0)
            llSeekPos.QuadPart = 0;
         }

        // Show results for this run
        bShowFlag = (llChunksLeftToDo.QuadPart == 0);
        CalcAndShow(iReadOrWrite, llIndex, llNumChunksToDo, llNumBytesDone, ulNumBytesDone,
                    dMillisecUsed, dMillisecThisRun, dMillisecThisRunInclThrot,
                    &iNumMovingAvgWarnings, bShowFlag,
                    dMillisecUsedInclThrot, &bDoThrottle, &dMillisecToSleep);

        // Possible Throttling
        dMillisecSlept    = 0.0;
        bThrottleHappened = false;

        if (bDoThrottle)
        {
          char caMsg[64];
          double dMillisecBeforeThrottle = hrt.ReadTimer();
          Sleep(dMillisecToSleep);
          double dMillisecAfterThrottle  = hrt.ReadTimer();
          dMillisecSlept = dMillisecAfterThrottle - dMillisecBeforeThrottle;
          bThrottleHappened = true;
        }
      }

// Since we are using FILE_FLAG_NO_BUFFERING, we can't do a ReadFile
//   of a size that's not divisible by 512, so we just skip the last
//   non-evenly divisible chunk.  Even if FILE_FLAG_NO_BUFFERING
//   is not set, we still don't do the last non-evenly divisible chunk.

      // Close the file
      StatusLabel->Caption = "Closing File...";
      Application->ProcessMessages();

      CloseHandle(hHandle);

      StatusLabel->Caption = "Done.";
      Application->ProcessMessages();

      // Print out info on this iteration
      if (!GLBL_bStopFlag)
      {
        iIterationsCompleted++;
        dMBperSecAvg  = (llNumBytesDone.QuadPart / (double)(1024.0 * 1024.0))
                    / (dMillisecUsedInclThrot / 1000.0);
        dMBperSecSum += dMBperSecAvg;

        ShowIterationMessage(caModePastTense, bThrottleHappened,
                              iIterationsCompleted, iIterations, dMBperSecAvg,
                              iNumMovingAvgWarnings);
      }
    }

    ShowAverageMessage(iIterationsCompleted, dMBperSecSum);
  }  /* else - if (FirstFileRadioButton->Checked) */

  if (GLBL_bStopFlag)
  {
    StatusLabel->Caption = "Aborted.";
    Application->ProcessMessages();
  }

  MemoryFree(&hMem, &ucpData);

  ButtonsStopped;
} /* DoFileIO */
//---------------------------------------------------------------------------

void __fastcall TForm1::StopButtonClick(TObject *Sender)
{

  GLBL_bStopFlag = true;
  ButtonsStopped;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::ShowProgressCheckBoxClick(TObject *Sender)
{
  if (!ShowProgressCheckBox->Checked)
  {
    ProgressBar->Position = 0;
    ProgressPerc->Caption = "";
    MBsAvgLabel->Caption  = "";
    MBsLastLabel->Caption = "";
    MBsMinLabel->Caption  = "";
    MBsMaxLabel->Caption  = "";
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
    UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------



void __fastcall TForm1::FrameSizeRadioButtonListClick(TObject *Sender)
{
  long lFrameSizeBytes = 0;

  FrameSizeRadioButtonEdit->Checked = false;
  FrameSizeRadioButtonList->Checked = true;
  sscanf(FrameSizeComboBox->Text.c_str(), "%ld", &lFrameSizeBytes);
  // This 'if' is a bit of a hack b/c the last entry in the ComboBox
  //   sometimes is blank - really it shouldn't exist.  So,
  //   if that's the entry selected, we just switch it to the 5400
  //   entry.
  if (lFrameSizeBytes == 0)
    FrameSizeComboBox->ItemIndex = 1;
//  ShowMessage(ChunkSizeComboBox->ItemIndex);
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FrameSizeRadioButtonEditClick(TObject *Sender)
{
  FrameSizeRadioButtonList->Checked = false;
  FrameSizeRadioButtonEdit->Checked = true;
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

bool GetBytesPerSector(char *caFilename, unsigned long *ulpBytesPerSector)
{
  char *cpPosBackSlash;
  int   iPosBackSlash;
  char  caDiskname[512];
  char *cpDiskname = caDiskname;

  unsigned long ulSectorsPerCluster, ulNumberOfFreeClusters;
  unsigned long ulTotalNumberOfClusters;

  /*
   * Get the Number of Bytes Per Sector for current disk
   */
  cpPosBackSlash = strchr(caFilename, '\\');
  if (cpPosBackSlash == NULL)
    iPosBackSlash = 0;
  else
    iPosBackSlash = cpPosBackSlash - caFilename;

  if (iPosBackSlash != 0)
  {
    strcpy(caDiskname, caFilename);
    caDiskname[iPosBackSlash+1] = '\0';
  }
  else
    cpDiskname = NULL;

  if (GetDiskFreeSpace(cpDiskname, &ulSectorsPerCluster, ulpBytesPerSector,
    &ulNumberOfFreeClusters, &ulTotalNumberOfClusters) == 0)
  {
    ErrorBox();
    return (false);  // Problem
  }

  return(true); // OK
}

void ErrorBox (void)
{
  LPVOID lpMsgBuf;

  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
    NULL,
    GetLastError(),
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    (LPTSTR) &lpMsgBuf,
    0,
    NULL
  );

  // Display the string.
  MessageBox(NULL, (LPCTSTR) lpMsgBuf, "GetLastError",
              MB_OK|MB_ICONINFORMATION );

  // Free the buffer.
  LocalFree(lpMsgBuf);
  return;  // Problem
}

void __fastcall TForm1::MaxSpeedCheckBoxClick(TObject *Sender)
{
  if (MaxSpeedCheckBox->Checked)
    MaxSpeedEdit->Enabled = true;
  else
    MaxSpeedEdit->Enabled = false;
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

bool IsValidNumericEditChar(Char Key)
{
  bool Result = false;
  if(((Key == DecimalSeparator)       ||
      (Key == '+')                    ||
      (Key == '-')                    ||
     ((Key >= '0') && (Key <= '9')))  ||
     ((Key < 0x32) && (Key != Char(VK_RETURN))))
   Result = True;

  return Result;
}

//---------------------------------------------------------------------------

#define MAX_SPEED_EDIT_MIN 1
void __fastcall TForm1::MaxSpeedEditExit(TObject *Sender)
{
  static int iDefaultValue = MaxSpeedEdit->Text.ToInt();
  int        iValue        = iDefaultValue;

  // Get the value - use default if not an Int
  iValue = MaxSpeedEdit->Text.ToIntDef(iDefaultValue);

  // Check for invalid minimum and fix
  if (iValue < MAX_SPEED_EDIT_MIN)
    iValue = MAX_SPEED_EDIT_MIN;

  // Make sure it's now a valid Integer
  MaxSpeedEdit->Text = iValue;
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MaxSpeedEditKeyPress(TObject *Sender, char &Key)
{
  if (!IsValidNumericEditChar(Key)) {
    if (Key == VK_RETURN)               // Validate if RETURN Key
      MaxSpeedEditExit(Sender);
    else
    {
      Key = 0;
      MessageBeep(0);
    }
  }

}
//---------------------------------------------------------------------------


void __fastcall TForm1::MovingAvgNumSamplesEditKeyPress(TObject *Sender,
      char &Key)
{
  if (!IsValidNumericEditChar(Key)) {
    Key = 0;
    MessageBeep(0);
  }
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

#define MOVING_AVG_NUM_SAMPLES_EDIT_MIN 1
void __fastcall TForm1::MovingAvgNumSamplesEditExit(TObject *Sender)
{
  char       caHint[64];
  char       caVerb[16];
  static int iDefaultValue = MovingAvgNumSamplesEdit->Text.ToInt();
  int        iValue        = iDefaultValue;

  // Get the value - use default if not an Int
  iValue = MovingAvgNumSamplesEdit->Text.ToIntDef(iDefaultValue);

  // Check for invalid minimum and fix
  if (iValue < MOVING_AVG_NUM_SAMPLES_EDIT_MIN)
    iValue = MOVING_AVG_NUM_SAMPLES_EDIT_MIN;

  // Make sure it's now a valid Integer
  MovingAvgNumSamplesEdit->Text = iValue;

  if (iValue == 1)
    strcpy(caVerb, "run");
  else
    strcpy(caVerb, "runs");

  sprintf(caHint, "Average of MB/s values for last %d %s", iValue, caVerb);

  MBsMovingAvgLabel->Hint = caHint;
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::VideostoreRadioButtonClick(TObject *Sender)
{
  // Enable / Disable appropriate items
  VideostoreRadioButton->Checked = true;
  FirstFileRadioButton->Checked  = false;
  WriteFileButton->Enabled       = true;
  CreateFileButton->Enabled      = true;
  LastNumEdit->Visible           = false;
  LastNumLabel->Visible          = false;
  NameBrowserButton->Visible     = false;
  FrameSizeComboBox->Visible     = true;
  FrameSizeEdit->Visible         = true;
  ChunkSizePart1Label->Visible   = true;
  ChunkSizePart2Label->Visible   = true;
  FrameSizeRadioButtonList->Visible = true;
  FrameSizeRadioButtonEdit->Visible = true;
  IterationsLabel->Visible          = true;
  IterationsEdit->Visible           = true;
  StartByteEdit->Visible        = true;
  StartFrameEdit->Visible         = true;
  EnableFileWriteCheckBox->Visible  = false;

  // Save FirstFile data
  GLBL_astrFirstFile         = VideostoreEdit->Text;
  GLBL_astrFirstFileFilesize = EndFrameEdit->Text;

  // Restore Videostore data
  VideostoreEdit->Text = GLBL_astrVideostore;
  EndFrameEdit->Text   = GLBL_astrVideostoreFilesize;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FirstFileRadioButtonClick(TObject *Sender)
{
  // Enable / Disable appropriate items
  VideostoreRadioButton->Checked = false;
  FirstFileRadioButton->Checked  = true;
  WriteFileButton->Enabled       = false;
  CreateFileButton->Enabled      = false;
  LastNumEdit->Visible           = true;
  LastNumLabel->Visible          = true;
  NameBrowserButton->Visible     = true;
  FrameSizeComboBox->Visible     = false;
  FrameSizeEdit->Visible         = false;
  ChunkSizePart1Label->Visible   = false;
  ChunkSizePart2Label->Visible   = false;
  FrameSizeRadioButtonList->Visible = false;
  FrameSizeRadioButtonEdit->Visible = false;
  IterationsLabel->Visible          = false;
  IterationsEdit->Visible           = false;
  StartByteEdit->Visible        = false;
  StartFrameEdit->Visible         = false;
  EnableFileWriteCheckBox->Visible  = true;
  if (EnableFileWriteCheckBox->Checked)
    WriteFileButton->Enabled     = true;
  else
    WriteFileButton->Enabled     = false;

  // Save Videostore data
  GLBL_astrVideostore         = VideostoreEdit->Text;
  GLBL_astrVideostoreFilesize = EndFrameEdit->Text;

  // Restore FirstFile data
  VideostoreEdit->Text = GLBL_astrFirstFile;
  EndFrameEdit->Text   = GLBL_astrFirstFileFilesize;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::NameBrowserButtonClick(TObject *Sender)
{
  char caBase[512];
  char caBaseNoPath[512];
  char caIndex[512];
  char caExt[512];
  MTI_UINT64 u64Len;

  if (FirstFileOpenDialog->Execute())
  {
    VideostoreEdit->Text = FirstFileOpenDialog->FileName;
    VideostoreEdit->Hint = FirstFileOpenDialog->FileName;

    if (GetBaseIndexAndExtension(VideostoreEdit->Text, caBase, caIndex, caExt,
                                caBaseNoPath))
      LastNumEdit->Text = caIndex;

    u64Len = GetFileLength(VideostoreEdit->Text.c_str());
    EndByteEdit->Text = u64Len;

    if (u64Len == 0)
    {
      ShowMessage("Zero Length File (or File doesn't exist)...");
      ButtonsStopped;
      return;
    }
  }
}
//---------------------------------------------------------------------------

#define MOVING_AVG_WARN_EDIT_MIN 1
void __fastcall TForm1::MovingAvgWarnEditExit(TObject *Sender)
{
  static int iDefaultValue = MovingAvgWarnEdit->Text.ToInt();
  int        iValue        = iDefaultValue;

  // Get the value - use default if not an Int
  iValue = MovingAvgWarnEdit->Text.ToIntDef(iDefaultValue);

  // Check for invalid minimum and fix
  if (iValue < MOVING_AVG_WARN_EDIT_MIN)
    iValue = MOVING_AVG_WARN_EDIT_MIN;

  // Make sure it's now a valid Integer
  MovingAvgWarnEdit->Text = iValue;
  UpdateGUISettingsDisplay();
}

void __fastcall TForm1::BufferWarnEditExit(TObject *Sender)
{
  static int iDefaultValue = BufferWarnEdit->Text.ToInt();
  int        iValue        = iDefaultValue;

  // Get the value - use default if not an Int
  iValue = BufferWarnEdit->Text.ToIntDef(iDefaultValue);

  // Check for invalid minimum and fix
  if (iValue < 1)
    iValue = 1;

  // Check for invalid max and fix
  if (iValue > 99)
    iValue = 99;

  // Make sure it's now a valid Integer
  BufferWarnEdit->Text = iValue;
  UpdateGUISettingsDisplay();
}

void __fastcall TForm1::MovingAvgWarnEditKeyPress(TObject *Sender, char &Key)
{
  if (!IsValidNumericEditChar(Key)) {
    if (Key == VK_RETURN)               // Validate if RETURN Key
      MovingAvgWarnEditExit(Sender);
    else
    {
      Key = 0;
      MessageBeep(0);
    }
  }
}

void __fastcall TForm1::BufferWarnCheckBoxClick(TObject *Sender)
{
  if (BufferWarnCheckBox->Checked)
    BufferWarnEdit->Enabled = true;
  else
    BufferWarnEdit->Enabled = false;
  UpdateGUISettingsDisplay();
}

void __fastcall TForm1::MovingAvgWarnCheckBoxClick(TObject *Sender)
{
  if (MovingAvgWarnCheckBox->Checked)
    MovingAvgWarnEdit->Enabled = true;
  else
    MovingAvgWarnEdit->Enabled = false;
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

bool GetBaseIndexAndExtension(AnsiString s, char *cpBase, char *cpIndex,
                              char *cpExt, char *cpBaseNoPath)
{
  int i;
  char caS[1024];
  bool bNum           = false;
  bool bSaveChar      = false;
  bool bSaveBase      = true;
  bool bSaveIndex     = false;
  bool bSaveExt       = false;
  bool bIndexSaved    = false;
  int  iBaseIndex     = 0;
  int  iIndexIndex    = 0;
  int  iExtIndex      = 0;
  int  iBaseNoPathIndex = 0;
  int  iEndPath       = 0;
  char c;

  strcpy(caS, s.c_str());

  // Find path
  i = strlen(caS) - 1;

  while (((c = caS[i]) != '\\') && (i >= 0))
  {
    i--;
  }

  iEndPath = i;

  strncpy(cpBase, caS, iEndPath + 1);
  iBaseIndex = iEndPath + 1;

  for (i=iEndPath + 1; i<(int) strlen(caS); i++)
  {
    c = caS[i];

    if (c >= '0' && c <= '9')
      bNum = true;
    else
      bNum = false;

    if (bNum)
    {
      bSaveChar  = false;
      bSaveIndex = true;
    }
    else
    {
      bSaveChar  = true;
      bSaveIndex = false;
    }

    if (bSaveChar)
    {
      if (!bIndexSaved)
      {
        bSaveBase = true;
        bSaveExt  = false;
      }
      else
      {
        bSaveBase = false;
        bSaveExt  = true;
      }
    }
    else
    {
      bSaveBase = false;
      bSaveExt  = false;
    }


    if (bSaveBase)
    {
      cpBase[iBaseIndex] = caS[i];
      iBaseIndex++;

      cpBaseNoPath[iBaseNoPathIndex] = caS[i];
      iBaseNoPathIndex++;
    }
    else if (bSaveIndex)
    {
      cpIndex[iIndexIndex] = caS[i];
      iIndexIndex++;
      bIndexSaved = true;
    }
    else if (bSaveExt)
    {
      cpExt[iExtIndex] = caS[i];
      iExtIndex++;
    }
  }

  cpBase[iBaseIndex]             = NULL;   // Add NULL terminator
  cpBaseNoPath[iBaseNoPathIndex] = NULL;   // Add NULL terminator
  cpIndex[iIndexIndex]           = NULL;   // Add NULL terminator
  cpExt[iExtIndex]               = NULL;   // Add NULL terminator

  if (iIndexIndex == 0)
    return(false);     // Failed
  else
    return(true);
}

//-----------------GetFileLength--------------Michael Russell----Aug 2003-----

     MTI_UINT64 GetFileLength(const string &Name)

//
//  This returns the MTI_UINT64 length of a file.
//
//****************************************************************************
{
  MTI_UINT64                u64Size;
  bool                      bResult = false;
  WIN32_FILE_ATTRIBUTE_DATA fAttribData;

  bResult = GetFileAttributesEx(Name.c_str(), GetFileExInfoStandard,
              &fAttribData);

  if (bResult)      // Success
  {
    u64Size = (((MTI_UINT64) fAttribData.nFileSizeHigh) << 32)
                | (MTI_UINT64)fAttribData.nFileSizeLow;
  }
  else
    u64Size = 0;

  return(u64Size);
}

void TForm1::CalcAndShow(int iReadOrWrite, LARGE_INTEGER llIndex, LARGE_INTEGER llNumChunksToDo,
                 LARGE_INTEGER llNumBytesDone, unsigned long ulNumBytesDone,
                 double dMillisecUsed, double dMillisecThisRun, double dMillisecThisRunInclThrot,
                 int *ipNumMovingAvgWarnings,
                 bool bShowFlag, double dMillisecForAvg, bool *bpDoThrottle,
                 double *dpMillisecToSleep)
{
  float          fProgressPerc;
  double         dMBperSecAvg       = 0.0;
  double         dMBperSecMovingAvg = 0.0;
  double         dMBperSecMovingAvgThrot = 0.0;
  double         dMBperSecLast      = 0.0;
  static vector<double> vdMillisecThisRun;
  static vector<double> vdMillisecThisRunThrot;
  static double  dPercentBufferUsage;
  static int     aiIOTimes[300];   //array to count instances of iotimes. iotime>300ms counted as 300ms
  static int     iIOTimeMin;
  static int     iIOTimeMax;
  // track io times relative to the target speed
  static double  dIOTimeTarget = 0.0;
  static int     aiIOTimesRelative[20];
  static int     iNumFieldsPerIO = 0;
  double         dMovingAvgSum;
  double         dMovingAvgSumThrot;
  vector<double>::iterator vIt;
  char           caOutputLine[512];
  static char    caLogFileName[512];
  static double  dMBperSecMin       = 99999.9;
  static double  dMBperSecMax       = 0.0;
  static double  dMillisecShow      = 0.0;
  char           caCaption[512];
  char           caMBs[512];
  int            iNumBuffers = MovingAvgNumSamplesEdit->Text.ToInt();
  double         dMovingAvgWarnThresh = 1.0;
  double         dBufferWarnThresh = 1.0;

  if (MovingAvgWarnEdit->Text != "")
        dMovingAvgWarnThresh     =  max(1.0, (double) MovingAvgWarnEdit->Text.ToInt());
  if (BufferWarnEdit->Text != "")
        dBufferWarnThresh     =  max(1.0, (double) BufferWarnEdit->Text.ToInt());



  // Reset if first run of group - should be all "static's"
  if (llIndex.QuadPart == 0)
  {
    dMBperSecMin  = 99999.9;
    dMBperSecMax  =     0.0;
    dMillisecShow =     0.0;
    vdMillisecThisRun.clear();
    vdMillisecThisRunThrot.clear();
    dPercentBufferUsage = 1.0;
   //DDDDD
    for (int i = 0; i < 301; i++)
        aiIOTimes[i] = 0;
    for (int i = 0; i < 21; i++)
        aiIOTimesRelative[i] = 0;
    iIOTimeMin = 300;
    iIOTimeMax = 0;
    CreateLogFile(iReadOrWrite, caLogFileName);
    // track io times relative to the target speed
    sscanf(NumFieldsComboBox->Text.c_str(), "%d", &iNumFieldsPerIO);
    dIOTimeTarget = (1000.0 / (double) MaxSpeedEdit->Text.ToInt())/(2.0 / (double) iNumFieldsPerIO);
  }

  *bpDoThrottle = false;

  // Percentage done
  fProgressPerc = (((double) llIndex.QuadPart + 1)
                  / (double) llNumChunksToDo.QuadPart) * 100.0;
  // Average MB/s - this is the average of the sum of all bytes / the sum
  //   of the time
  dMBperSecAvg  = ((double) llNumBytesDone.QuadPart / (double)(1024.0 * 1024.0))
                  / (dMillisecForAvg / 1000.0);
  // Last MB/s
  dMBperSecLast = ( (double)ulNumBytesDone / (double)(1024.0 * 1024.0))
                  / (dMillisecThisRun / 1000.0);

  // Moving Average MB/s
  // Remove the first entry in the vector
  if (vdMillisecThisRun.size() >= (unsigned) iNumBuffers)
      {
      vdMillisecThisRun.erase(vdMillisecThisRun.begin());
      vdMillisecThisRunThrot.erase(vdMillisecThisRunThrot.begin());
      }
  // Add an entry to the end of the vectors
  vdMillisecThisRun.push_back(dMillisecThisRun);
  vdMillisecThisRunThrot.push_back(dMillisecThisRunInclThrot);

  // Sum the vector
  dMovingAvgSum = 0.0;
  dMovingAvgSumThrot = 0.0;

  for (vIt = vdMillisecThisRun.begin(); vIt != vdMillisecThisRun.end(); vIt++)
    dMovingAvgSum += *vIt;

  for (vIt = vdMillisecThisRunThrot.begin(); vIt != vdMillisecThisRunThrot.end(); vIt++)
    dMovingAvgSumThrot += *vIt;

  // calc moving average (not incl throttle)
  if (dMovingAvgSum == 0)
      dMBperSecMovingAvg = 0;
  else
      dMBperSecMovingAvg = ( ((double) iNumBuffers * (double)ulNumBytesDone) / (double)(1024.0 * 1024.0))
                  / (dMovingAvgSum / 1000.0);

  // calc moving average with throttle
  if (dMovingAvgSumThrot == 0)
      dMBperSecMovingAvgThrot = 0;
  else
      dMBperSecMovingAvgThrot = ( ((double) iNumBuffers * (double)ulNumBytesDone) / (double)(1024.0 * 1024.0))
                  / (dMovingAvgSumThrot / 1000.0);

  // Min
  if (dMBperSecLast < dMBperSecMin)
    dMBperSecMin = dMBperSecLast;
  // Max
  if (dMBperSecLast > dMBperSecMax)
    dMBperSecMax = dMBperSecLast;


  // Possible Max Speed Throttle Delay
  unsigned long ulMaxSpeedThrottle;
  sscanf(BytesPerSecLabel->Caption.c_str(), "%I64d", &ulMaxSpeedThrottle);
  double dDesiredTime = ((llNumBytesDone.QuadPart / (double) ulMaxSpeedThrottle) * 1000.0);

  dPercentBufferUsage =    ((((double) iNumBuffers)*(ulNumBytesDone))
                          -  (((dMillisecForAvg - dDesiredTime)/  1000.0) * ulMaxSpeedThrottle))
                          /  (((double) iNumBuffers)*(ulNumBytesDone));

  *dpMillisecToSleep = dDesiredTime - dMillisecForAvg;
  *bpDoThrottle = false;
    if ((*dpMillisecToSleep > 0) && (MaxSpeedCheckBox->Checked))
        *bpDoThrottle = true;
    else
        *dpMillisecToSleep = 0;


    sprintf(caOutputLine, "%9i, %9d, %9.2f, %9.2f, %9.2f, %9.2f, %11.2f, %9.2f",
             (int)llIndex.QuadPart + 1,
             ulNumBytesDone,
             dMillisecThisRun,
             dMBperSecLast,
             dMBperSecMovingAvg,
             *dpMillisecToSleep,
             dMBperSecMovingAvgThrot,
             dPercentBufferUsage);

  // Log Each I/O
  fstream file_op(caLogFileName,ios::out|ios::app);
  file_op<<caOutputLine;
  file_op<<"\n";
  file_op.close();

  // Show Each I/O (if checked)
  if (ShowEachIOCheckBox->Checked)
  {
    DetailsMemo->Lines->Add(caOutputLine);
  }

  // Warn if Moving Average less than threshold
  // of if Buffer is less than threshold (if checked)
  if (((MovingAvgWarnCheckBox->Checked) && (dMBperSecMovingAvg < dMovingAvgWarnThresh))
    ||((BufferWarnCheckBox->Checked) && (dPercentBufferUsage < (dBufferWarnThresh / 100.0))))
  {
      (*ipNumMovingAvgWarnings)++;
    sprintf(caOutputLine, "%9i, %9d, %9.2f, %9.2f, %9.2f, %9.2f, %11.2f, %9.2f, %9i",
             (int)llIndex.QuadPart + 1,
             ulNumBytesDone,
             dMillisecThisRun,
             dMBperSecLast,
             dMBperSecMovingAvg,
             *dpMillisecToSleep,
             dMBperSecMovingAvgThrot,
             dPercentBufferUsage,
             *ipNumMovingAvgWarnings);
      Memo->Lines->Add(caOutputLine);
  }


  // Add IO time for this read to the array histrogram counter
  if (dMillisecThisRun < 301.0)
        aiIOTimes[(int) dMillisecThisRun]++;
  else
        aiIOTimes[300]++;
  if (dMillisecThisRun < iIOTimeMin)
        iIOTimeMin = (int) dMillisecThisRun;
  if (dMillisecThisRun > iIOTimeMax)
        iIOTimeMax = min((int) dMillisecThisRun, 300);

  // increment counter in relative time counter array
  // indices 10 and below are for fast IOs
  // 10: IO was less than a frame, greater than 1/2 frames
  //  9: IO was less than 1/2 frame, greater than 1/3 frame, etc.
  // indices 11 and above are for slow IOs
  // 11: IO was more than 1 frames, less than 2 frames
  // 12: IO was more than 2 frames, less than 3 frames, etc.
  if (dMillisecThisRun <= dIOTimeTarget)
        aiIOTimesRelative[11 - (min((floor(dIOTimeTarget / dMillisecThisRun)), 10))]++;
  if (dMillisecThisRun > dIOTimeTarget)
        aiIOTimesRelative[10 + (min((floor(dMillisecThisRun / dIOTimeTarget)), 10))]++;


  // Only show if enabled & either last chunk or 1/4 sec since last show
  if ((ShowProgressCheckBox->Checked)
       && (bShowFlag
//             || ((dMillisecUsed + dMillisecToSleep) - dMillisecShow > 250)))
             || (dMillisecForAvg - dMillisecShow > 250)))
  {
    // Percentage done
    ProgressBar->Position = fProgressPerc;
    sprintf(caCaption, "%6.2f %%", fProgressPerc);
    ProgressPerc->Caption = caCaption;

    // Buffer usage
    if ((dPercentBufferUsage > 0) && (dPercentBufferUsage < 1.0))
    {
        BufferUsageBar->Position = 100.0 * dPercentBufferUsage;
    }
    else if (dPercentBufferUsage < 0.0)
    {
        BufferUsageBar->Position = 0;
    }
    else
    {
        BufferUsageBar->Position = 100;
    }

    // DDDDD
    // Stats on IO Times
    int iIOTimesSum = 0;
    int iIOTimesCount = 0;
    double dIOTimesMean =0.0;
    double dIOTimesSumSq = 0.0;
    double dIOTimesStdDev = 0.0;

    // Find Sum
    for (int i = iIOTimeMin; i < iIOTimeMax + 1; i++)
        iIOTimesSum += i * aiIOTimes[i];

    // Mean
    dIOTimesMean = (double) iIOTimesSum / ((double)llIndex.QuadPart + 1);

    sprintf(caMBs, "%6.2f ms", dIOTimesMean);
    IOTimesMeanLabel->Caption = caMBs;

    // Sum of Squares
    for (int i = iIOTimeMin; i < iIOTimeMax + 1; i++)
    {
     dIOTimesSumSq += aiIOTimes[i] * (pow(((double) i - dIOTimesMean), 2.0));
    }
    //Standard Deviation
    dIOTimesStdDev = sqrt(dIOTimesSumSq / ((double)llIndex.QuadPart + 1));

    sprintf(caMBs, "%6.2f ms", dIOTimesStdDev);
    IOTimesStdDevLabel->Caption = caMBs;


    int iCount = 0;
    for (int i = iIOTimeMin; i < (int) (dIOTimesMean - 2 * dIOTimesStdDev); i++)
        iCount += aiIOTimes[i];
    IOBar2m->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);

    iCount = 0;
    for (int i = dIOTimesMean - 2 * dIOTimesStdDev; i < (int) (dIOTimesMean - dIOTimesStdDev); i++)
        iCount += aiIOTimes[i];
    IOBar1m->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);

    iCount = 0;
    for (int i = dIOTimesMean - dIOTimesStdDev; i < (int) dIOTimesMean; i++)
        iCount += aiIOTimes[i];
    IOBar0m->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);

    iCount = 0;
    for (int i = dIOTimesMean; i < (int) (dIOTimesMean + dIOTimesStdDev); i++)
        iCount += aiIOTimes[i];
    IOBar0p->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);

    iCount = 0;
    for (int i = dIOTimesMean + dIOTimesStdDev; i < (int) (dIOTimesMean + 2 * dIOTimesStdDev); i++)
        iCount += aiIOTimes[i];
    IOBar1p->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);

    iCount = 0;
    for (int i = dIOTimesMean + 2 * dIOTimesStdDev; i < iIOTimeMax+ 1; i++)
        iCount += aiIOTimes[i];
    IOBar2p->Position = 100 * iCount / ((double)llIndex.QuadPart + 1);


    IOBarRel1->Position = 100 * aiIOTimesRelative[1] / ((double)llIndex.QuadPart + 1);
    IOBarRel2->Position = 100 * aiIOTimesRelative[2] / ((double)llIndex.QuadPart + 1);
    IOBarRel3->Position = 100 * aiIOTimesRelative[3] / ((double)llIndex.QuadPart + 1);
    IOBarRel4->Position = 100 * aiIOTimesRelative[4] / ((double)llIndex.QuadPart + 1);
    IOBarRel5->Position = 100 * aiIOTimesRelative[5] / ((double)llIndex.QuadPart + 1);
    IOBarRel6->Position = 100 * aiIOTimesRelative[6] / ((double)llIndex.QuadPart + 1);
    IOBarRel7->Position = 100 * aiIOTimesRelative[7] / ((double)llIndex.QuadPart + 1);
    IOBarRel8->Position = 100 * aiIOTimesRelative[8] / ((double)llIndex.QuadPart + 1);
    IOBarRel9->Position = 100 * aiIOTimesRelative[9] / ((double)llIndex.QuadPart + 1);
    IOBarRel10->Position = 100 * aiIOTimesRelative[10] / ((double)llIndex.QuadPart + 1);
    IOBarRel11->Position = 100 * aiIOTimesRelative[11] / ((double)llIndex.QuadPart + 1);
    IOBarRel12->Position = 100 * aiIOTimesRelative[12] / ((double)llIndex.QuadPart + 1);
    IOBarRel13->Position = 100 * aiIOTimesRelative[13] / ((double)llIndex.QuadPart + 1);
    IOBarRel14->Position = 100 * aiIOTimesRelative[14] / ((double)llIndex.QuadPart + 1);
    IOBarRel15->Position = 100 * aiIOTimesRelative[15] / ((double)llIndex.QuadPart + 1);
    IOBarRel16->Position = 100 * aiIOTimesRelative[16] / ((double)llIndex.QuadPart + 1);
    IOBarRel17->Position = 100 * aiIOTimesRelative[17] / ((double)llIndex.QuadPart + 1);
    IOBarRel18->Position = 100 * aiIOTimesRelative[18] / ((double)llIndex.QuadPart + 1);
    IOBarRel19->Position = 100 * aiIOTimesRelative[19] / ((double)llIndex.QuadPart + 1);
    IOBarRel20->Position = 100 * aiIOTimesRelative[20] / ((double)llIndex.QuadPart + 1);




    // Average MB/s
    sprintf(caMBs, "%6.2f MB/s", dMBperSecAvg);
    MBsAvgLabel->Caption = caMBs;

    // Moving Average MB/s
    sprintf(caMBs, "%6.2f MB/s", dMBperSecMovingAvg);
    MBsMovingAvgLabel->Caption = caMBs;

    // Last MB/s
    sprintf(caMBs, "%6.2f MB/s", dMBperSecLast);
    MBsLastLabel->Caption = caMBs;

    // Min
    sprintf(caMBs, "%6.2f MB/s", dMBperSecMin);
    MBsMinLabel->Caption = caMBs;

    // Max
    sprintf(caMBs, "%6.2f MB/s", dMBperSecMax);
    MBsMaxLabel->Caption = caMBs;

    Application->ProcessMessages();
    dMillisecShow = dMillisecForAvg;
  }
}
//---------------------------------------------------------------------------

void TForm1::ShowAverageMessage(int iCurrIndex, double dMBperSecSum)
{
  char caOutputLine[512];

  if (iCurrIndex != 0)
  {
    sprintf(caOutputLine, "Average for %d: %.2f MB/s", iCurrIndex,
            dMBperSecSum / iCurrIndex);
    Memo->Lines->Add(caOutputLine);
    Memo->Lines->Add("");
  }
}

HANDLE TForm1::OpenFile(const char *caFilename, const int iReadOrWrite)
{
  HANDLE hHandle;

  int iNoBufferingFlag;

  if (NoBufferingCheckBox->Checked)
    iNoBufferingFlag = FILE_FLAG_NO_BUFFERING;
  else
    iNoBufferingFlag = FILE_ATTRIBUTE_NORMAL;

  if (iReadOrWrite == IO_MODE_READ)
  {
    hHandle = CreateFile(caFilename,
                         GENERIC_READ,
                         FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                         OPEN_EXISTING,
                         iNoBufferingFlag, NULL);
  }
  else   //IO_MODE_WRITE
  {
    hHandle = CreateFile(caFilename,
                         GENERIC_WRITE,
                         FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                         OPEN_ALWAYS,
                         iNoBufferingFlag, NULL);
  }

  return(hHandle);
}

void __fastcall TForm1::EnableFileWriteCheckBoxClick(TObject *Sender)
{
  if (EnableFileWriteCheckBox->Checked)
    WriteFileButton->Enabled     = true;
  else
    WriteFileButton->Enabled     = false;
  UpdateGUISettingsDisplay();
}

bool TForm1::CheckIO(const int iReadOrWrite, const unsigned long ulNumBytesDone,
                     const long lChunkSizeBytes, char *caMessage)
{
  bool bRet = true;

  // Check the I/O
  if (ulNumBytesDone == 0)
  {
    bRet = false;

    if (iReadOrWrite == IO_MODE_READ)
      strcpy(caMessage, "EOF on ReadFile");
    else // IO_MODE_WRITE
      strcpy(caMessage, "Bytes Written = 0 on WriteFile()");
  }

  if (iReadOrWrite == IO_MODE_READ)
  {
    if ((long) ulNumBytesDone != lChunkSizeBytes)
    {
      bRet = false;
      sprintf(caMessage, "Under read: exp: %ld, act: %lu", lChunkSizeBytes,
               ulNumBytesDone);
    }
  }

  return(bRet);
}            

bool TForm1::DoReadOrWriteFile(const int iReadOrWrite, const HANDLE hHandle,
                               unsigned char *ucpData,
                               const long lChunkSizeBytes,
                               unsigned long *ulpNumBytesDone)
{
  bool bRet;

  if (iReadOrWrite == IO_MODE_READ)
    bRet = ReadFile(hHandle, ucpData, lChunkSizeBytes, ulpNumBytesDone, NULL);
  else  // IO_MODE_WRITE
    bRet = WriteFile(hHandle, ucpData, lChunkSizeBytes, ulpNumBytesDone, NULL);

  return(bRet);
}

void TForm1::MemoryAlloc(HANDLE *hpMem, unsigned char **ucppData,
                         const long lChunkSizeBytes)
{
  if (MemoryRadioGroup->ItemIndex == MEMORY_MALLOC)
  {
    (*hpMem)    = NULL;
    (*ucppData) = (unsigned char *) malloc((unsigned int) lChunkSizeBytes);
  }
  else if (MemoryRadioGroup->ItemIndex == MEMORY_GLOBAL_ALLOC)
  {
    (*hpMem)    = GlobalAlloc(GMEM_FIXED, lChunkSizeBytes);
    (*ucppData) = (unsigned char *) GlobalLock(*hpMem);
  }
  else /* MEMORY_VIRTUAL_ALLOC */
  {
    (*hpMem)    = NULL;
    (*ucppData) = (unsigned char *) VirtualAlloc(NULL, lChunkSizeBytes,
                                                 MEM_COMMIT, PAGE_READWRITE);
  }
}

void TForm1::MemoryFree(HANDLE *hpMem, unsigned char **ucppData)
{
  if (MemoryRadioGroup->ItemIndex == MEMORY_MALLOC)
  {
    free(*ucppData);
  }
  else if (MemoryRadioGroup->ItemIndex == MEMORY_GLOBAL_ALLOC)
  {
    GlobalFree(*hpMem);
  }
  else /* MEMORY_VIRTUAL_ALLOC */
  {
    VirtualFree(*ucppData, 0, MEM_RELEASE);
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ClearMemoSpeedButtonClick(TObject *Sender)
{
  Memo->Clear();
  UpdateGUISettingsDisplay();
}

void __fastcall TForm1::ClearDetailsSpeedButtonClick(TObject *Sender)
{
  DetailsMemo->Clear();
  UpdateGUISettingsDisplay();
}

void TForm1::ShowIterationMessage(const char *caModePastTense,
                                  const bool bThrottleHappened,
                                  const int iCurrIteration,
                                  const int iIterations,
                                  const double dMBperSec,
                                  const int iNumMovingAvgWarnings)
{
  char caThrottle[32];
  char caOutputLine[512];

  if (bThrottleHappened)
    sprintf(caThrottle, "(Throt=%d)", MaxSpeedEdit->Text.ToInt());

  sprintf(caOutputLine, "%s %d (%d of %d): %.2f MB/s %s (Warnings: %d)",
           caModePastTense, GLBL_iNumRuns, iCurrIteration, iIterations,
           dMBperSec, caThrottle, iNumMovingAvgWarnings);

  Memo->Lines->Add(caOutputLine);
  Application->ProcessMessages();
}

//---------------------------------------------------------------------------




void __fastcall TForm1::MTIImageClick(TObject *Sender)
{
    LaunchWebSite();
}

void TForm1::LaunchWebSite(void)
{
    ShellExecute(Application->Handle, NULL, WebSiteLabel->Hint.c_str(), NULL,
       NULL, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::WebSiteLabelClick(TObject *Sender)
{
    LaunchWebSite();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MaxSpeedEditChange(TObject *Sender)
{
    UpdateGUISettingsDisplay();

}
//---------------------------------------------------------------------------


void TForm1::UpdateGUISettingsDisplay()
{
    long lFrameSizeBytes = 0.0;
    int iIOAlignment =0;
    sscanf(IOAlignmentComboBox->Text.c_str(), "%d", &iIOAlignment);
    if (FrameSizeRadioButtonList->Checked)
       sscanf(FrameSizeComboBox->Text.c_str(), "%ld", &lFrameSizeBytes);
    else
       sscanf(FrameSizeEdit->Text.c_str(), "%ld", &lFrameSizeBytes);

    lFrameSizeBytes = ((ceil((lFrameSizeBytes / 2.0)/ iIOAlignment)) * iIOAlignment) * 2.0;
    int iMaxSpeedFPS = 1;
    if (MaxSpeedEdit->Text != "")
       iMaxSpeedFPS = MaxSpeedEdit->Text.ToInt();
    int iMaxSpeedThrottle = (iMaxSpeedFPS * lFrameSizeBytes) / ((double) (1024.0 * 1024.0));
    long lMaxSpeedThrottle = iMaxSpeedFPS * lFrameSizeBytes;
    char           caLabel[512];
    sprintf(caLabel, "%3d MB/s", iMaxSpeedThrottle);
    MBps->Caption = caLabel;
    MovingAvgWarnEdit->Text = iMaxSpeedThrottle;
    sprintf(caLabel, "%ld Byte/sec", lMaxSpeedThrottle);
    BytesPerSecLabel->Caption = caLabel;


    //Start and End Positions
    LARGE_INTEGER llStartFrame;
    LARGE_INTEGER llEndFrame;
    llStartFrame.QuadPart = 0;
    llEndFrame.QuadPart = 0;

    //Start and End Byte Positions
    if (StartFrameEdit->Text != "")
        sscanf(StartFrameEdit->Text.c_str(), "%I64d", &llStartFrame.QuadPart);
    if (EndFrameEdit->Text != "")
        sscanf(EndFrameEdit->Text.c_str(), "%I64d", &llEndFrame.QuadPart);;
    StartByteEdit->Text = llStartFrame.QuadPart * lFrameSizeBytes;
    EndByteEdit->Text = llEndFrame.QuadPart * lFrameSizeBytes;
    DurByteEdit->Text = (llEndFrame.QuadPart - llStartFrame.QuadPart) * lFrameSizeBytes;
    NumChunksEdit->Text = ((llEndFrame.QuadPart - llStartFrame.QuadPart) * lFrameSizeBytes) / 512;
    GigabyteEdit->Text = ceil((llEndFrame.QuadPart * lFrameSizeBytes) / (1024.0 * 1024.0 * 1024.0));

    int iNumFieldsPerIO = 0;
    double dIOTimeTarget = 0.0;
    sscanf(NumFieldsComboBox->Text.c_str(), "%d", &iNumFieldsPerIO);
    dIOTimeTarget = (1000.0 / (double) iMaxSpeedFPS) /(2.0 / (double) iNumFieldsPerIO);

    sprintf(caLabel, "%6.2f ms/f", dIOTimeTarget);
    TargetIOSpeedLabel->Caption = caLabel;


}

void __fastcall TForm1::GigabyteEditExit(TObject *Sender)
{
  char caTC [11];
  int iGB = 0;
  int iIOAlignment =0;
  long lFrameSizeBytes = 0;
  LARGE_INTEGER llNumBytes;
  long lFrame = 0;
  sscanf(IOAlignmentComboBox->Text.c_str(), "%d", &iIOAlignment);
  if (FrameSizeRadioButtonList->Checked)
     sscanf(FrameSizeComboBox->Text.c_str(), "%ld", &lFrameSizeBytes);
  else
     sscanf(FrameSizeEdit->Text.c_str(), "%ld", &lFrameSizeBytes);

  lFrameSizeBytes = ((ceil((lFrameSizeBytes / 2.0)/ iIOAlignment)) * iIOAlignment) * 2.0;


  if (GigabyteEdit->Text != "")
      iGB = GigabyteEdit->Text.ToInt();

  llNumBytes.QuadPart = 1024 * 1024 * 1024;
  llNumBytes.QuadPart *= iGB;
  lFrame = ceil(llNumBytes.QuadPart /lFrameSizeBytes);
  SetTCFromFrameTotal((int) lFrame, caTC);
  DurTCEdit->Text = caTC;
  FixTCStrings();
  CalcEndTC();
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::StartFrameEditChange(TObject *Sender)
{
  char caTC [11];
  int iFrame = 0;
  if (StartFrameEdit->Text != "")
      iFrame = StartFrameEdit->Text.ToInt();
  SetTCFromFrameTotal(iFrame, caTC);
  StartTCEdit->Text = caTC;
  FixTCStrings();
  CalcEndTC();
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::EndFrameEditChange(TObject *Sender)
{
  char caTC [11];
  int iFrame = 0;
  if (EndFrameEdit->Text != "")
      iFrame = EndFrameEdit->Text.ToInt();
  SetTCFromFrameTotal(iFrame, caTC);
  EndTCEdit->Text = caTC;
  FixTCStrings();
  CalcDurTC();
  UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::DurFrameEditChange(TObject *Sender)
{
  char caTC [11];
  int iFrame = 0;
  if (DurFrameEdit->Text != "")
      iFrame = DurFrameEdit->Text.ToInt();
  SetTCFromFrameTotal(iFrame, caTC);
  DurTCEdit->Text = caTC;
  FixTCStrings();
  CalcEndTC();
  UpdateGUISettingsDisplay();

}
//---------------------------------------------------------------------------

void TForm1::FixTCStrings()
{
      //string strTC;
      char caSTC[11];
      char caETC[11];
      char caDTC[11];



      sscanf(StartTCEdit->Text.c_str(),  "%11c", caSTC);
      sscanf(EndTCEdit->Text.c_str(),  "%11c", caETC);
      sscanf(DurTCEdit->Text.c_str(),  "%11c", caDTC);


      for (int i = 0; i < 11; i++)
      {
         if (caSTC[i] == ' ')
             caSTC[i] = '0';
         if (caETC[i] == ' ')
             caETC[i] = '0';
         if (caDTC[i] == ' ')
             caDTC[i] = '0';
      }
      StartTCEdit->Text = caSTC;
      EndTCEdit->Text = caETC;
      DurTCEdit->Text = caDTC;

}
void __fastcall TForm1::StartTCEditExit(TObject *Sender)
{
     FixTCStrings();
     CalcEndTC();
     UpdateGUISettingsDisplay();
}
void __fastcall TForm1::EndTCEditExit(TObject *Sender)
{
     FixTCStrings();
     CalcDurTC();
     UpdateGUISettingsDisplay();
}
void __fastcall TForm1::DurTCEditExit(TObject *Sender)
{
     FixTCStrings();
     CalcEndTC();
     UpdateGUISettingsDisplay();
}
//---------------------------------------------------------------------------
int TForm1::GetFrameTotalFromTC(char *caTC)
{
  int iH, iM, iS, iF;
  int iFPS = StorageFPSEdit->Text.ToInt();
  sscanf(caTC, "%2d %*c %2d %*c %2d %*c %2d", &iH,&iM,&iS,&iF);
  int iFrameTotal = (iH * 60 * 60 * iFPS) + (iM * 60 * iFPS) + (iS * iFPS)+ iF;
  return (iFrameTotal);

}

void TForm1::SetTCFromFrameTotal(int iFrameTotal, char * caTC)
{
    int iH;
    int iM;
    int iS;
    int iF;
    int iFPS = StorageFPSEdit->Text.ToInt();

    iH = floor(iFrameTotal / (60 * 60 * iFPS));
    iM = floor((iFrameTotal - (iH * 60 * 60 * iFPS)) / (60 * iFPS));
    iS = floor((iFrameTotal - (iH * 60 * 60 * iFPS) - (iM * 60 * iFPS)) / iFPS);
    iF = iFrameTotal - (iH * 60 * 60 * iFPS) - (iM * 60 * iFPS) - (iS * iFPS);

    sprintf(caTC, "%2d:%2d:%2d:%2d", iH,iM,iS,iF);
}


void TForm1::CalcDurTC()
{


    int iStartFrame = GetFrameTotalFromTC(StartTCEdit->Text.c_str());
    int iEndFrame = GetFrameTotalFromTC(EndTCEdit->Text.c_str());

    int iDurFrame = iEndFrame - iStartFrame;
    if (iDurFrame < 0)
        iDurFrame = 0;

    char caDurTC [11];
    SetTCFromFrameTotal(iDurFrame, caDurTC);
    DurTCEdit->Text = caDurTC;

    char caFrame [512];
    sprintf(caFrame, "%d", iDurFrame);
    DurFrameEdit->Text = caFrame;

    sprintf(caFrame, "%d", iEndFrame);
    EndFrameEdit->Text = caFrame;

    FixTCStrings();

}

void TForm1::CalcEndTC()
{
    int iStartFrame = GetFrameTotalFromTC(StartTCEdit->Text.c_str());
    int iDurFrame = GetFrameTotalFromTC(DurTCEdit->Text.c_str());

    int iEndFrame = iStartFrame + iDurFrame;

    char caEndTC [11];
    SetTCFromFrameTotal(iEndFrame, caEndTC);
    EndTCEdit->Text = caEndTC;

    char caFrame [512];
    sprintf(caFrame, "%d", iEndFrame);
    EndFrameEdit->Text = caFrame;

    sprintf(caFrame, "%d", iDurFrame);
    DurFrameEdit->Text = caFrame;

    sprintf(caFrame, "%d", iStartFrame);
    StartFrameEdit->Text = caFrame;


    FixTCStrings();




}



void __fastcall TForm1::StorageFPSEditChange(TObject *Sender)
{
   CalcEndTC();
   CalcDurTC();        
}

void TForm1::CreateLogFile(int iReadOrWrite, char caLogFileName[])
{
   char caLogHeaderLine[512];
   char caReadOrWrite[5];
   char caLogFileLabel[512];

   if (iReadOrWrite == IO_MODE_READ)
        sprintf(caReadOrWrite, "read");
   else
        sprintf(caReadOrWrite, "write");

   time_t ltime;
   struct tm *Tm;

   ltime=time(NULL);
   Tm=localtime(&ltime);

   CreateDirectory("c:\\mtirw", NULL);

   sprintf(caLogFileName,
           "c:\\mtirw\\%s_%04d-%02d-%02d_%02d-%02d-%02d.csv",
           caReadOrWrite,
           Tm->tm_year+1900,
           Tm->tm_mon+1,
           Tm->tm_mday,
           Tm->tm_hour,
           Tm->tm_min,
           Tm->tm_sec);

   // Create Log File
   fstream file_new(caLogFileName,ios::out);
   file_new.close();

   // Append Header Info
   fstream file_op(caLogFileName,ios::out|ios::app);

   sprintf(caLogHeaderLine, " VideoStore: %s \n",  VideostoreEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, " Frame Size: %s \n",  FrameSizeComboBox->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "    IO Size: %s \n",  NumFieldsComboBox->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "  Alignment: %s \n",  IOAlignmentComboBox->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "   Start TC: %s \n",  StartTCEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "     End TC: %s \n",  EndTCEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "   Duration: %s \n",  DurTCEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "     TC FPS: %s \n",  StorageFPSEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "Buffer Size: %s frames \n",  MovingAvgNumSamplesEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "  Max Speed: %s fps \n",  MaxSpeedEdit->Text.c_str());
   file_op<<caLogHeaderLine;
   sprintf(caLogHeaderLine, "     index,     bytes,    iotime,   iospeed,   mov avg,  ms sleep,  mv av thrt,  buffer\n");
   file_op<<caLogHeaderLine;
   file_op<<"\n";
   file_op.close();

   sprintf(caLogFileLabel, "Log File: %s", caLogFileName);
   LogFileLabel->Caption = caLogFileLabel;

}
//---------------------------------------------------------------------------





