#pragma once
#include "chrono"
#include "iostream"
class NewTimer
{
public:
	NewTimer();
	void start();
	void stop();

	double elapsedMilliseconds();
	double intervalMilliseconds();

	std::ostream toString(std::ostream& os, std::chrono::nanoseconds ns);

private:
	bool _started;
	std::chrono::system_clock::time_point _start;
	std::chrono::system_clock::time_point _intervalStart;
	std::chrono::system_clock::time_point _end;
};

