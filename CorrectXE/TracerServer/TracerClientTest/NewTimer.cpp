#include "NewTimer.h"
#include <iomanip>

NewTimer::NewTimer()
{
	start();
}

void NewTimer::start()
{
	_started = true;
	_start = std::chrono::system_clock::now();
	_intervalStart = _start;
	_end = _start;
}

void NewTimer::stop()
{
	_started = false;
	_end = std::chrono::system_clock::now();
}

double NewTimer::elapsedMilliseconds()
{
	auto end = _end;
	if (_started)
	{
		end = std::chrono::system_clock::now();
	}

	return std::chrono::duration_cast<std::chrono::microseconds>(end - _start).count() / 1000.0;
}


double NewTimer::intervalMilliseconds()
{
	auto end = std::chrono::system_clock::now();
	auto start = _intervalStart;
	_intervalStart = end;

	return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0;
}

