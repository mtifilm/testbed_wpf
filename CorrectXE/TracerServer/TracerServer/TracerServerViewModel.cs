﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace TracerServer
{
    class TracerServerViewModel
    {
        private object locker = new object();
        private TextBox statusTextBox;
        private TextBox tracerTextBox;

        public ObservableCollection<string> StatusList { get; set; } = new ObservableCollection<string>();

        public TracerServerViewModel()
        {

        }

        public void StartServer()
        {
            this.TracerModel = new TracerServerModel();
            this.TracerModel.PropertyChanged += TracerModelPropertyChanged;
            this.TracerModel.StartServer("TracerPipe831");
        }

        public void StopServer()
        {
            this.TracerModel.PropertyChanged -= TracerModelPropertyChanged;
        }

        internal void SetTracerTextBox(TextBox tracerTextBox)
        {
            this.tracerTextBox = tracerTextBox;
        }

        internal void SetStatusTextBox(TextBox mainTextBox)
        {
            this.statusTextBox = mainTextBox;
        }

        private void TracerModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var tid = Thread.CurrentThread.ManagedThreadId;
            lock (this.locker)
            {
                switch (e.PropertyName)
                 {
                    case "StatusString":
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            this.statusTextBox?.AppendText(this.TracerModel.StatusString);
                            this.statusTextBox?.ScrollToEnd();
                        });
                        break;

                    case "TracerString":
                        if (this.tracerTextBox == null)
                        {
                            break;
                        }

                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            this.tracerTextBox?.AppendText(this.TracerModel.TracerString);
                            //var lineCount = tracerTextBox.LineCount;
                            //if (lineCount > 1000)
                            //{
                            //    Stopwatch timer = new Stopwatch();
                            //    timer.Start();
                            //    var l = 0;
                            //    for (var i = 0; i< 500; i++)
                            //    {
                            //        l += tracerTextBox.GetLineLength(i);
                            //    }

                            //    var x = timer.ElapsedMilliseconds.ToString();
                            //    tracerTextBox.Text = tracerTextBox.Text.Remove(0, l);
                            //    var y = timer.ElapsedMilliseconds.ToString();
                            //    this.tracerTextBox.AppendText(x + ",  " + y + '\n');
                            //}

                            if (this.AutoScroll)
                            {
                                this.tracerTextBox?.ScrollToEnd();
                            }
                        });
                        break;
                }
            }
        }

        internal void ClearTracerText()
        {
            this.tracerTextBox.Clear();
        }

        internal void ClearStatusText()
        {
            this.statusTextBox.Clear();
        }

        internal async Task TestClientAsync()
        {
            await Task.Run(() => TestClient());
        }

        internal int lineCount = 0;

        internal void TestClient()
        {
            var pipeClient = new NamedPipeClientStream(".",
                                      "TracerPipe831",
                                      PipeDirection.Out,
                                      PipeOptions.None,
                                      TokenImpersonationLevel.Impersonation);

            var streamStream = new StreamString(pipeClient);

            pipeClient.Connect();
            HighResolutionTimer timer = new HighResolutionTimer();
            timer.Start();

            foreach (var i in Enumerable.Range(0, 600))
            {
                var time = timer.Milliseconds().ToString("#0000.000");
                streamStream.WriteString($"{time}: We are sending string {this.lineCount++}\n");
            }


            pipeClient.Close();
        }

        public TracerServerModel TracerModel { get; set; }

        public bool AutoScroll { get; internal set; }
    }
}
