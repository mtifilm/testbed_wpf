﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TracerServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);


        private TracerServerViewModel viewModel;
        public MainWindow()
        {
            InitializeComponent();

            // Allow only one copy
            if (this.IsAnotherCopyRunning())
            {
                    Process current = Process.GetCurrentProcess();
                    foreach (Process process in Process.GetProcessesByName(current.ProcessName))
                    {
                        if (process.Id != current.Id)
                        {
                            SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                return;
            }

            this.Top = Properties.Settings.Default.Top;
            this.Left = Properties.Settings.Default.Left;
            this.Height = Properties.Settings.Default.Height;
            this.Width = Properties.Settings.Default.Width;
            if (Properties.Settings.Default.Maximized)
            {
                WindowState = WindowState.Maximized;
            }

            this.viewModel = new TracerServerViewModel();
            this.DataContext = this.viewModel;

            this.WordWrapButton.IsChecked = Properties.Settings.Default.WordWrapChecked;
            this.AutoScrollButton.IsChecked = Properties.Settings.Default.AutoScroll;

            this.StatusTabItem.Tag = new TabLocalData();
            this.TracerTabItem.Tag = new TabLocalData();
            this.viewModel.SetStatusTextBox(this.StatusTextBox);
            this.viewModel.SetTracerTextBox(this.TracerTextBox);
            this.viewModel.StartServer();
        }
        private void CommonCommandBindingCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ClearExecuted(object sender, ExecutedRoutedEventArgs e)
        { 
            var tab = TraceTabControl.SelectedItem as TabItem;
            switch (tab?.Name)
            {
                case "TracerTabItem":
                    this.viewModel.ClearTracerText();
                    break;

                case "StatusTabItem":
                    this.viewModel.ClearStatusText();
                    break;
            }
        }

        private async void TestExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ///Task longRunningTask = Task.Run(() => viewModel.TestClient());
            await viewModel.TestClientAsync();
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                // Use the RestoreBounds as the current values will be 0, 0 and the size of the screen
                Properties.Settings.Default.Top = RestoreBounds.Top;
                Properties.Settings.Default.Left = RestoreBounds.Left;
                Properties.Settings.Default.Height = RestoreBounds.Height;
                Properties.Settings.Default.Width = RestoreBounds.Width;
                Properties.Settings.Default.Maximized = true;
            }
            else
            {
                Properties.Settings.Default.Top = this.Top;
                Properties.Settings.Default.Left = this.Left;
                Properties.Settings.Default.Height = this.Height;
                Properties.Settings.Default.Width = this.Width;
                Properties.Settings.Default.Maximized = false;
            }


            Properties.Settings.Default.WordWrapChecked = (bool)this.WordWrapButton.IsChecked;
            Properties.Settings.Default.AutoScroll = (bool)this.AutoScrollButton.IsChecked;
            Properties.Settings.Default.Save();
            this.viewModel.StopServer();
        }

        private void WindowInitialized(object sender, EventArgs e)
        {
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void WordWrapExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            // Do a global word wrap rather than each tab, so handle on wordwrap checked/uncheckse
            //var tab = TraceTabControl.SelectedItem as TabItem;
            //TextBox targetTextBox;
            //switch (tab?.Name)
            //{
            //    case "TracerTabItem":
            //        targetTextBox = this.TracerTextBox;
            //        break;

            //    case "StatusTabItem":
            //        targetTextBox = this.StatusTextBox;
            //        break;

            //    default:
            //        return;
            //}

            //if (targetTextBox.TextWrapping == TextWrapping.NoWrap)
            //{
            //    targetTextBox.TextWrapping = TextWrapping.Wrap;
            //}
            //else
            //{
            //    targetTextBox.TextWrapping = TextWrapping.NoWrap;
            //}
        }

        private bool IsAnotherCopyRunning()
        {
            bool createdNew = true;
            using (Mutex mutex = new Mutex(true, "TracerServer", out createdNew))
            {
                return createdNew == false;
            }
        }

        private void WordWrapButtonChecked(object sender, RoutedEventArgs e)
        {
            TracerTextBox.TextWrapping = TextWrapping.Wrap;
            StatusTextBox.TextWrapping = TextWrapping.Wrap;
        }

        private void WordWrapButtonUnchecked(object sender, RoutedEventArgs e)
        {
            TracerTextBox.TextWrapping = TextWrapping.NoWrap;
            StatusTextBox.TextWrapping = TextWrapping.NoWrap;
        }

        private void AutoScrollButtonChecked(object sender, RoutedEventArgs e)
        {
            this.viewModel.AutoScroll = true;
        }

        private void AutoScrollButtonUnchecked(object sender, RoutedEventArgs e)
        {
            this.viewModel.AutoScroll = false;
        }
    }
}
