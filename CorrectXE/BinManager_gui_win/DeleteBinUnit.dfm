object DeleteBinDialog: TDeleteBinDialog
  Left = 227
  Top = 251
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Delete Bin'
  ClientHeight = 115
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object FileNameLabel: TLabel
    Left = 8
    Top = 24
    Width = 3
    Height = 13
  end
  object PathNameLabel: TLabel
    Left = 8
    Top = 8
    Width = 3
    Height = 13
  end
  object CancelDelete: TButton
    Left = 200
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = CancelDeleteClick
  end
  object ProgressBar: TProgressBar
    Left = 8
    Top = 48
    Width = 273
    Height = 16
    Smooth = True
    TabOrder = 1
  end
  object UpdateTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = UpdateTimerTimer
    Left = 16
    Top = 80
  end
end
