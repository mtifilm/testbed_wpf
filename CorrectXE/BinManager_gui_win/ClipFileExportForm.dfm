object ClipFileExportForm: TClipFileExportForm
  Left = 0
  Top = 0
  ClientHeight = 170
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    528
    170)
  PixelsPerInch = 96
  TextHeight = 13
  object BottomPanel: TPanel
    Left = 0
    Top = 132
    Width = 529
    Height = 40
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      529
      40)
    object StatusLabel: TLabel
      Left = 8
      Top = 11
      Width = 68
      Height = 13
      Caption = 'StatusLabel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PauseButton: TButton
      Left = 389
      Top = 8
      Width = 60
      Height = 21
      Cursor = crArrow
      Anchors = [akRight, akBottom]
      Caption = 'Pause'
      TabOrder = 1
      Visible = False
      OnClick = PauseButtonClick
    end
    object CancelButton: TButton
      Left = 457
      Top = 8
      Width = 60
      Height = 21
      Cursor = crArrow
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      TabOrder = 0
      OnClick = CancelButtonClick
    end
    object MTIBusy1: TMTIBusy
      Left = 239
      Top = 2
      Width = 32
      Height = 32
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 529
    Height = 128
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      529
      128)
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 527
      Height = 43
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      object SourceNameLabel: TLabel
        Left = 88
        Top = 8
        Width = 198
        Height = 13
        Caption = '/one/two/three/four/five/six/seven/eight'
      end
      object DestinationNameLabel: TLabel
        Left = 88
        Top = 24
        Width = 232
        Height = 13
        Caption = '/one/two/three/four/five/six/seven/eight'
      end
      object SourceLabel: TLabel
        Left = 8
        Top = 8
        Width = 45
        Height = 13
        Caption = 'Source:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DestinationLabel: TLabel
        Left = 8
        Top = 24
        Width = 69
        Height = 13
        Caption = 'Destination:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RenderProgressBar: TProgressBar
      Left = 8
      Top = 50
      Width = 513
      Height = 16
      Anchors = [akLeft, akTop, akRight]
      Smooth = True
      TabOrder = 1
    end
    object Panel2: TPanel
      Left = 0
      Top = 71
      Width = 527
      Height = 50
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 2
      object ConvertedLabel: TLabel
        Left = 8
        Top = 8
        Width = 190
        Height = 13
        Caption = 'Converted 2899888 of 3866777 frames'
      end
      object FinishedInLabel: TLabel
        Left = 8
        Top = 24
        Width = 263
        Height = 13
        Caption = 'Unable to determine the format from the name (-2345)'
      end
    end
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 358
    Top = 6
  end
end
