//----------------------------------------------------------------------------
#ifndef ExportTCUnitH
#define ExportTCUnitH
//----------------------------------------------------------------------------
#include <System.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
#include "IniFile.h"
#include "FileListIO.h"


//----------------------------------------------------------------------------
class TExportTCDialog : public TForm
{
__published:
	TBevel *Bevel1;
        TBitBtn *CancelButton;
        TBitBtn *OKButton;
        TBitBtn *RestoreButton;
        TOpenDialog *OpenDialog;
        TComboBox *FileNameComboBox;
        TLabel *Label1;
        TSpeedButton *FileBrowseButton;
        TCheckBox *InTCCheckBox;
        TCheckBox *OutTCCheckBox;
        TCheckBox *DateCheckBox;
        TCheckBox *ImageFormatCheckBox;
        TCheckBox *DescriptionCheckBox;
        TCheckBox *ClipNameCheckBox;
        void __fastcall FileBrowseButtonClick(TObject *Sender);
        void __fastcall FileNameComboBoxDropDown(TObject *Sender);
        void __fastcall OKButtonClick(TObject *Sender);
        void __fastcall RestoreButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:
        CMRU mruFileName;
        CFileListIO FileListIO;
        StringList ClipNames;
        void ParametersToGui(CFileListIO &flio);
        void HardParametersToGui(CFileListIO &flio);
        void GuiToParameters(CFileListIO &flio);

public:
	virtual __fastcall TExportTCDialog(TComponent* AOwner);
        virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
        virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
        int Execute(const StringList &List);

};
//----------------------------------------------------------------------------
extern PACKAGE TExportTCDialog *ExportTCDialog;
//----------------------------------------------------------------------------
#endif    
