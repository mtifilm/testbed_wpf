//---------------------------------------------------------------------------

#ifndef AutoDiffH
#define AutoDiffH

#include <string>
using std::string;

#include "Clip3.h"
//class CClip;
class CVideoFrameList;

//---------------------------------------------------------------------------

class CAutoDiff
{
public:

   CAutoDiff(const string &aClipPath);
   ~CAutoDiff();

   int init();
   int createDiffsFile();
   int updateCuts(float aThreshold);
   void cancel()
       { m_cancel = true; } ;

   enum PROCESSING_STATUS {
        IDLE = 0,
        BUSY,
        SUCCEEDED,
        FAILED,
        CANCELLED
   };

   PROCESSING_STATUS getProcessingStatus() const
       { return m_threadStatus; };
   float getFractionDone() const
       { return m_fractionDone; };
   string getWhatsHappening() const
       { return m_whatIAmDoingNow; };
   int getErrorCode()
       { return m_errorCode; };
   string getErrorMsg()
       { return m_errorMsg; };
   bool isDone()
       { return (m_threadStatus == SUCCEEDED ||
                 m_threadStatus == FAILED    ||
                 m_threadStatus == CANCELLED); };
   int getNumCutsFound()
       { return m_numCutsFound; };

private:

   float m_threshold;
   string m_clipPath;
   string m_diffsFilePath;
   string m_backupDiffsFilePath;
   CVideoFrameList *m_frameList;
   ClipSharedPtr m_clip;
   int m_totalFrameCount;
   bool m_cancel;

   // Progress stuff
   float m_fractionDone;    // 0.0 - 1.0
   string m_whatIAmDoingNow;
   int m_numCutsFound;
   
   // Thread control stuff
   PROCESSING_STATUS m_threadStatus;
   int m_errorCode;
   string m_errorMsg;

   static void createDiffsFile_trampoline(void *appData, void *threadid);
   int CreateDiffsFile_internal();
   static void updateCuts_trampoline(void *appData, void *threadid);
   int updateCuts_internal();
   int doCutsForOneTrack(int trackno);
};

//---------------------------------------------------------------------------

#endif
