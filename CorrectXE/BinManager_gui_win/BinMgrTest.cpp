//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("BinMgrTest.res");
USEFORM("BinMgrTestUnit.cpp", BinMgrTestForm);
USELIB("BinManagerGUILib.lib");
USELIB("..\core_code\include\core_code.lib");
USELIB("..\format\include\format.lib");
USELIB("..\clip\include\clipDLLbcb.lib");
USELIB("..\imgTool\include\imgTool.lib");
USELIB("..\devWin\include\devWinLib.lib");
USEFORM("DeleteBinUnit.cpp", DeleteBinDialog);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TBinMgrTestForm), &BinMgrTestForm);
       Application->CreateForm(__classid(TDeleteBinDialog), &DeleteBinDialog);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
