object AutoDiffForm: TAutoDiffForm
  Left = 674
  Top = 170
  BorderStyle = bsDialog
  Caption = 'Cut Detector'
  ClientHeight = 322
  ClientWidth = 309
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  DesignSize = (
    309
    322)
  PixelsPerInch = 96
  TextHeight = 13
  object diffProgressLabel: TLabel
    Left = 11
    Top = 228
    Width = 41
    Height = 13
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Progress'
    Transparent = True
  end
  object goButton: TBitBtn
    Left = 148
    Top = 293
    Width = 73
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Find Cuts'
    TabOrder = 0
    OnClick = goButtonClick
  end
  object closeButton: TBitBtn
    Left = 228
    Top = 293
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    NumGlyphs = 2
    TabOrder = 1
    OnClick = closeButtonClick
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 12
    Width = 293
    Height = 105
    Caption = ' Frame Diffs File '
    TabOrder = 2
    object RegenDiffsRadioButtonLabel: TLabel
      Left = 30
      Top = 22
      Width = 226
      Height = 26
      Caption = 
        'Regenerate frame diffs file before detecting cuts'#13#10'NOTE: This ma' +
        'y take a long time!'
      Color = clBtnFace
      ParentColor = False
    end
    object DetectOnlyRadioButtonLabel: TLabel
      Left = 30
      Top = 70
      Width = 188
      Height = 13
      Caption = 'Detect cuts using existing frame diffs file'
    end
    object RegenDiffsRadioButton: TRadioButton
      Left = 12
      Top = 20
      Width = 17
      Height = 21
      Caption = #13#10
      TabOrder = 0
    end
    object DetectOnlyRadioButton: TRadioButton
      Left = 12
      Top = 68
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 1
      TabStop = True
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 124
    Width = 293
    Height = 89
    Caption = ' Sensitivity '
    TabOrder = 3
    object Panel1: TPanel
      Left = 12
      Top = 20
      Width = 265
      Height = 61
      BevelOuter = bvNone
      TabOrder = 0
      object SensitivityTrackBar: TTrackBar
        Left = 8
        Top = 14
        Width = 247
        Height = 35
        Max = 5
        Min = 1
        PageSize = 1
        Position = 4
        TabOrder = 0
        ThumbLength = 21
        TickMarks = tmTopLeft
        TickStyle = tsNone
      end
      object Panel2: TPanel
        Left = 8
        Top = 4
        Width = 253
        Height = 16
        BevelOuter = bvNone
        TabOrder = 1
        object Label5: TLabel
          Left = 8
          Top = 1
          Width = 6
          Height = 13
          Caption = '1'
        end
        object Label6: TLabel
          Left = 64
          Top = 1
          Width = 6
          Height = 13
          Caption = '2'
        end
        object Label7: TLabel
          Left = 120
          Top = 1
          Width = 6
          Height = 13
          Caption = '3'
        end
        object Label8: TLabel
          Left = 176
          Top = 1
          Width = 6
          Height = 13
          Caption = '4'
        end
        object Label9: TLabel
          Left = 232
          Top = 1
          Width = 6
          Height = 13
          Caption = '5'
        end
      end
      object Panel3: TPanel
        Left = 12
        Top = 38
        Width = 237
        Height = 17
        BevelOuter = bvNone
        TabOrder = 2
        object Label4: TLabel
          Left = 12
          Top = 1
          Width = 81
          Height = 13
          Caption = '< Find fewer cuts'
          Transparent = True
        end
        object Label3: TLabel
          Left = 148
          Top = 1
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Find more cuts >'
          Transparent = True
        end
      end
    end
  end
  object diffProgressBar: TProgressBar
    Left = 8
    Top = 252
    Width = 293
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 4
  end
  object Panel4: TPanel
    Left = 0
    Top = 284
    Width = 309
    Height = 3
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 5
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 196
    Top = 220
  end
end
