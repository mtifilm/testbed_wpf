object DpxHeaderEditorForm: TDpxHeaderEditorForm
  Left = 494
  Top = 228
  Anchors = []
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'DPX Header Frame Rate Editor'
  ClientHeight = 221
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  DesignSize = (
    350
    221)
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressPanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 200
    DesignSize = (
      350
      221)
    object StatusLabel: TLabel
      Left = 16
      Top = 171
      Width = 81
      Height = 15
      Alignment = taCenter
      AutoSize = False
      Caption = 'Status'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object ClipNameLabel: TLabel
      Left = 16
      Top = 36
      Width = 23
      Height = 13
      Caption = 'Clip:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ClipNameValue: TLabel
      Left = 60
      Top = 36
      Width = 293
      Height = 13
      AutoSize = False
      Caption = 'Actual clip name goes here'
    end
    object ModifyingModeLabel: TLabel
      Left = 16
      Top = 8
      Width = 251
      Height = 13
      Caption = 'Modifying image files to change frame rate...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameLabel: TLabel
      Left = 16
      Top = 92
      Width = 36
      Height = 13
      Caption = 'Frame'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameValue: TLabel
      Left = 57
      Top = 92
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
    end
    object OfLabel: TLabel
      Left = 112
      Top = 92
      Width = 12
      Height = 13
      Caption = 'of'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object OfValue: TLabel
      Left = 128
      Top = 92
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
    end
    object ClipListProgressBar: TProgressBar
      Left = 16
      Top = 56
      Width = 317
      Height = 17
      TabOrder = 0
    end
    object FrameIndexProgressBar: TProgressBar
      Left = 16
      Top = 112
      Width = 317
      Height = 17
      TabOrder = 1
    end
    object Separator2: TPanel
      Left = 0
      Top = 175
      Width = 349
      Height = 3
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 2
      ExplicitTop = 154
    end
    object ProgressPanelCloseButton: TButton
      Left = 264
      Top = 189
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      ModalResult = 2
      TabOrder = 3
      OnClick = ProgressPanelCloseButtonClick
      ExplicitTop = 168
    end
  end
  object AreYouSurePanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    ExplicitHeight = 200
    DesignSize = (
      350
      221)
    object Label1: TLabel
      Left = 24
      Top = 48
      Width = 262
      Height = 19
      Caption = 'Click OK to change the frame rate of '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 24
      Top = 71
      Width = 287
      Height = 19
      Caption = 'the media files of all selected clips to 24.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SetupPanelOkButton: TButton
      Left = 168
      Top = 189
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      TabOrder = 0
      OnClick = OkButtonClick
      ExplicitTop = 168
    end
    object SetupPanelCancelButton: TButton
      Left = 264
      Top = 189
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
      ExplicitTop = 168
    end
  end
  object OptionsPanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    ExplicitHeight = 619
    DesignSize = (
      350
      221)
    object ChangeDpxHeaderFrameRateLabel: TLabel
      Left = 16
      Top = 8
      Width = 202
      Height = 16
      Caption = 'Change DPX header frame rate'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ToLabel: TLabel
      Left = 16
      Top = 62
      Width = 18
      Height = 13
      Caption = 'to: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FromLabel: TLabel
      Left = 16
      Top = 37
      Width = 30
      Height = 13
      Caption = 'from:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FromFrameRatesLabel: TLabel
      Left = 60
      Top = 37
      Width = 21
      Height = 13
      Caption = 'NNN'
    end
    object FpsLabel: TLabel
      Left = 128
      Top = 62
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'FPS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SourceClipCountLabel: TLabel
      Left = 16
      Top = 93
      Width = 138
      Height = 13
      Caption = 'for all NNN selected clips.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DifferentFrameRatesWarningLabel: TColorLabel
      Left = 16
      Top = 132
      Width = 317
      Height = 17
      Alignment = taCenter
      Caption = 
        ' WARNING: DPX files of selected clips have different frame rates' +
        '! '
      Color = clCream
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object OptionsPanelOkButton: TButton
      Left = 168
      Top = 189
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = OkButtonClick
      ExplicitTop = 587
    end
    object OptionsPanelCancelButton: TButton
      Left = 264
      Top = 189
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
      ExplicitTop = 587
    end
    object NewFpsComboBox: TComboBox
      Left = 57
      Top = 59
      Width = 61
      Height = 21
      Style = csDropDownList
      TabOrder = 2
      Items.Strings = (
        '23.976'
        '24'
        '25'
        '29.97'
        '30')
    end
  end
  object Separator1: TPanel
    Left = 0
    Top = 175
    Width = 349
    Height = 3
    Anchors = [akLeft, akRight, akBottom]
    BevelInner = bvLowered
    BevelOuter = bvLowered
    TabOrder = 2
    ExplicitTop = 573
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 312
    Top = 68
  end
end
