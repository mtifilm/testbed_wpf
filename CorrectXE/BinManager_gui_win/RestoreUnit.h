//---------------------------------------------------------------------------

#ifndef RestoreUnitH
#define RestoreUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TRestoreForm : public TForm
{
__published:	// IDE-managed Components
        TListView *RestoreListView;
        TButton *RestoreButton;
        TButton *CancelButton;
private:	// User declarations
public:		// User declarations
        __fastcall TRestoreForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRestoreForm *RestoreForm;
//---------------------------------------------------------------------------
#endif
