//---------------------------------------------------------------------------

#ifndef autoDiffGuiH
#define autoDiffGuiH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "AutoDiff.h"
//---------------------------------------------------------------------------
class TAutoDiffForm : public TForm
{
__published:	// IDE-managed Components
        TBitBtn *goButton;
        TBitBtn *closeButton;
    TGroupBox *GroupBox2;
        TLabel *RegenDiffsRadioButtonLabel;
    TRadioButton *RegenDiffsRadioButton;
    TRadioButton *DetectOnlyRadioButton;
    TGroupBox *GroupBox3;
    TProgressBar *diffProgressBar;
    TLabel *diffProgressLabel;
    TPanel *Panel1;
    TTrackBar *SensitivityTrackBar;
    TPanel *Panel2;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TPanel *Panel3;
    TLabel *Label4;
    TLabel *Label3;
        TTimer *ProgressTimer;
        TPanel *Panel4;
        TLabel *DetectOnlyRadioButtonLabel;
        void __fastcall goButtonClick(TObject *Sender);
        void __fastcall ProgressTimerTimer(TObject *Sender);
        void __fastcall closeButtonClick(TObject *Sender);
        
private:	// User declarations
    string clp_filename;
    CAutoDiff *autoDiff;

    enum {
       IDLE,
       GENERATING_DIFFS_FILE,
       UPDATING_CUTS
    } guiState;

    void resetInterface();
    float getThreshold();
    void resetAndMessage(string msge);
    void CheckDiffFileExists();

public:		// User declarations
    __fastcall TAutoDiffForm(TComponent* Owner);
    __fastcall ~TAutoDiffForm();
    void setCurClip(const string &name);
};
//---------------------------------------------------------------------------
extern PACKAGE TAutoDiffForm *AutoDiffForm;
//---------------------------------------------------------------------------
#endif
