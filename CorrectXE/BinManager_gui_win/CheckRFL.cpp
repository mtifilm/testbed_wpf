/*
	File:	CheckRFL.cpp
	By:	Kevin Manbeck
	Date:	June 2004

	The CCheckRFL class is used to compare the .rfl file against all
	active clips.  Any errors are reported and repaired if
	possible.
*/

#include "CheckRFL.h"

#include "BinDir.h"
#include "ClipAPI.h"
#include "bthread.h"
#include "MTIsleep.h"
#include "IniFile.h"
#include "BinManager.h"
#include "MediaAccess.h"
#include "RawMediaAccess.h"

#include "err_bin_manager.h"

/*

	The CBlockRun class

*/

CBlockRun::CBlockRun ()
{
  llStart = 0;
  llLength = 0;
  strClipName = "";
  strMediaIdentifier = "";
  iVideoTrack = -1;
  bOrphanFlag = false;
}  /* CBlockRun */

CBlockRun::~CBlockRun ()
{
}  /* ~CBlockRun */

/*

	CVideoStore class

*/

CVideoStore::CVideoStore ()
{
  llRawDiskSize = 0;
  strMediaIdentifier = "";
  strFreeListFile = "";
}  /* CVideoStore */

CVideoStore::~CVideoStore ()
{
  // listInUse is just pointers.  No memory is managed here
  listInUse.clear();

  for (unsigned int iCnt = 0; iCnt < listFree.size(); iCnt++)
   {
    delete listFree.at(iCnt);
   }
  listFree.clear();

  for (unsigned int iCnt = 0; iCnt < listRFL.size(); iCnt++)
   {
    delete listRFL.at(iCnt);
   }
  listRFL.clear();
}  /* ~CVideoStore */

/*

	CCheckRFL class

*/

CCheckRFL::CCheckRFL ()
{
  ReleaseStorage ();
}  /* CCheckRFL */

CCheckRFL::~CCheckRFL ()
{
  ReleaseStorage ();
}  /* ~CCheckRFL */

void CCheckRFL::ReleaseStorage ()
{
  for (unsigned int iCnt = 0; iCnt < listVideoStore.size(); iCnt++)
   {
    delete listVideoStore.at(iCnt);
   }

  listVideoStore.clear ();

  for (unsigned int iCnt = 0; iCnt < listInUse.size(); iCnt++)
   {
    delete listInUse.at(iCnt);
   }

  listInUse.clear();

  listMediaOverlapA.clear();
  listMediaOverlapB.clear();

  bRequestStop = false;
  bFinishedFlag = false;
  iErrorCondition = ERR_BIN_MANAGER_CONFIG_NOT_LOADED;

  iTotalTrack = 0;
  iCurrentTrack = 0;
  iTotalFrame = 0;
  iCurrentFrame = 0;

  strlBadClip.clear();
}  /* ReleaseStorage */

void CallDoProcessing (void *vp, void *vpReserved)
{
  ((CCheckRFL *)vp)->DoProcessing (vpReserved);
}  /* CallDoProcessing */

int CCheckRFL::StartProcessing ()
{
/*
	Checking the entire bin directory may be time consuming.
	Do it in a thread so the GUI can update progress
*/

  iErrorCondition = LoadRawDiskCfg ();

  vpDoProcessing = BThreadSpawn (CallDoProcessing, (void *)this);
  if (vpDoProcessing == NULL)
   {
    return ERR_BIN_MANAGER_BTHREAD_SPAWN;
   }
  
  return 0;
}  /* StartProcessing */

int CCheckRFL::HaltProcessing ()
{
  if (vpDoProcessing == NULL)
    return 0;		// already finished.  No need to halt

  bRequestStop = true;

  int iCnt = 0;
  while (bRequestStop && iCnt++ < 100)
   {
    MTImillisleep (10);
   }

  if (bRequestStop)
    return ERR_BIN_MANAGER_HALT_ERROR;

  return 0;
}  /* HaltProcessing */

float CCheckRFL::ProgressMeter0 ()
{
  int iTotalTrackLocal = iTotalTrack;
  int iCurrentTrackLocal = iCurrentTrack;

  if (iTotalTrackLocal == 0)
    return 0.;

  // what percentage of the tracks are complete
  return (float)iCurrentTrackLocal / (float)iTotalTrackLocal;
}

float CCheckRFL::ProgressMeter1 ()
{
  int iTotalFrameLocal = iTotalFrame;
  int iCurrentFrameLocal = iCurrentFrame;

  if (iTotalFrameLocal == 0)
    return 0.;

  // what percentage of the current track is complete
  return (float)iCurrentFrameLocal / (float)iTotalFrameLocal;
}

bool CCheckRFL::IsFinished (int *ipErrorCondition)
{
  *ipErrorCondition = iErrorCondition;
  return bFinishedFlag;
}  /* IsFinished */

string CCheckRFL::getTrackName ()
{
  if (listInUse.size() == 0)
   {
    return "VideoStore scan is starting";
   }
  else if (bFinishedFlag)
   {
    return "VideoStore scan is finished";
   }


  CBlockRun *brp = listInUse.at(listInUse.size()-1);
  return brp->strClipName;
}  /* getTrackName */

string CCheckRFL::getVideoStoreCfg ()
{
  return strVideoStoreCfg;
}  /* getVideoStoreCfg */

void CCheckRFL::findFirstVideoStore (string &strName, MTI_INT64 &llLength,
		MTI_INT64 &llActualLength, bool &bConflict)
{
  SearchVideoStore = listVideoStore.begin();

  findNextVideoStore (strName, llLength, llActualLength, bConflict);
}  /* findFirstVideoStore */

void CCheckRFL::findNextVideoStore (string &strName, MTI_INT64 &llLength,
		MTI_INT64 &llActualLength, bool &bConflict)
{
  if (SearchVideoStore == listVideoStore.end())
   {
    strName = "";
    llLength = 0;
    llActualLength = 0;
    bConflict = false;
    return;
   }

  strName = (*SearchVideoStore)->strRawVideoDirectory;
  llLength = (*SearchVideoStore)->llRawDiskSize;
  llActualLength = (*SearchVideoStore)->llRawDiskSizeActual;
  if ((*SearchVideoStore)->llRawDiskSize !=
	(*SearchVideoStore)->llRawDiskSizeActual)
   {
    bConflict = true;
   }
  else
   {
    bConflict = false;
   }

  // increment for next time
  SearchVideoStore++;
}  /* findNextVideoStore */

void CCheckRFL::findFirstRFL (string &strName, bool &bConflict)
{
  SearchVideoStore = listVideoStore.begin();

  return findNextRFL (strName, bConflict);
}  /* findFirstRFL */

void CCheckRFL::findNextRFL (string &strName, bool &bConflict)
{
  if (SearchVideoStore == listVideoStore.end())
   {
    strName = "";
    bConflict = false;
    return;
   }

  strName = (*SearchVideoStore)->strFreeListFile;
  bConflict = false;

  if ((*SearchVideoStore)->listFree.size() == 0 &&
      (*SearchVideoStore)->listRFL.size() == 1)
   {
    // this means the video store is completely full
    // the RFL should have length = 0
    if ((*SearchVideoStore)->listRFL.at(0)->llLength != 0)
     {
      bConflict = true;
      SearchVideoStore++;
      return;
     }
   }
  else if ((*SearchVideoStore)->listFree.size() !=
		(*SearchVideoStore)->listRFL.size())
   {
    bConflict = true;
    SearchVideoStore++;
    return;
   }

  for (unsigned int iCnt = 0; iCnt <
                (*SearchVideoStore)->listFree.size(); iCnt++)
   {
    if (
	(*SearchVideoStore)->listFree.at(iCnt)->llStart !=
		(*SearchVideoStore)->listRFL.at(iCnt)->llStart ||
	(*SearchVideoStore)->listFree.at(iCnt)->llLength !=
		(*SearchVideoStore)->listRFL.at(iCnt)->llLength
       )
     {
      bConflict = true;
     }
   }

  // increment for next time
  SearchVideoStore++;
}  /* findNextRFL */

void CCheckRFL::findFirstBadClip (string &strName)
{
  iSearchBadClip = 0;

  return findNextBadClip (strName);
}  /* findFirstBadClip */

void CCheckRFL::findNextBadClip (string &strName)
{

  if (iSearchBadClip == strlBadClip.size())
   {
    strName = "";
    return;
   }

  strName = strlBadClip.at(iSearchBadClip);


  // increment for next time
  iSearchBadClip++;
}  /* findNextBadClip */

void CCheckRFL::findFirstMediaOverlap (string &strNameA, string &strNameB)
{
  
  iSearchMediaOverlap = 0;

  return findNextMediaOverlap (strNameA, strNameB);
}  /* findFirstMediaOverlap */

void CCheckRFL::findNextMediaOverlap (string &strNameA, string &strNameB)
{

  if (iSearchMediaOverlap == listMediaOverlapA.size())
   {
    strNameA = "";
    strNameB = "";
    return;
   }

  strNameA = listMediaOverlapA.at(iSearchMediaOverlap);
  strNameB = listMediaOverlapB.at(iSearchMediaOverlap);


  // increment for next time
  iSearchMediaOverlap++;
}  /* findNextMediaOverlap */


int CCheckRFL::RepairRFL (int iVideoStore)
{
  CRawDiskFreeList *freeList;
  CVideoStore *vsp = listVideoStore.at(iVideoStore);

  freeList = new CRawDiskFreeList (vsp->strFreeListFile);

  freeList->clearFreeList ();

  vector <CBlockRun *>::iterator pos;
  for (pos = vsp->listFree.begin(); pos != vsp->listFree.end(); pos++)
   {
    freeList->add ((*pos)->llStart, (*pos)->llLength);
   }

  int iRet = freeList->writeFreeListFile ();
  delete freeList;

  if (iRet)
    return iRet;

  // reload the current RFL
  vector <CVideoStore *>::iterator posStore;
  for (posStore = listVideoStore.begin(); posStore != listVideoStore.end();
		posStore++)
   {
    for (unsigned int iCnt = 0; iCnt < (*posStore)->listRFL.size(); iCnt++)
     {
      delete (*posStore)->listRFL.at(iCnt);
     }
    (*posStore)->listRFL.clear();
   }

  FindRFL ();

  return 0;
}  /* RepairRFL */

void CCheckRFL::DoProcessing (void *vpReserved)
{
  int iRet;

  // return control to the caller
  if (BThreadBegin (vpReserved))
   {
    TRACE_0 (errout << "BThreadBegin returned error in CheckRFL");
    return;
   }

  // count the number of video tracks
  FindAndProcessClips (true);
  if (iErrorCondition || bRequestStop)
   {
    return;
   }

  iTotalTrack = iCurrentTrack;

  iCurrentTrack = 0;

  // process all the video tracks
  FindAndProcessClips (false);
  if (iErrorCondition || bRequestStop)
   {
    return;
   }

  // check the results
  AnalyzeResults ();
  if (iErrorCondition || bRequestStop)
   {
    return;
   }

  // return normally
  bFinishedFlag = true;
  return;

}  /* DoProcessing */

int CCheckRFL::LoadRawDiskCfg ()
{
  CIniFile *ini;

  ReleaseStorage ();  // free up previous run
  iErrorCondition = 0;

  // open the LocalMachine ini file
  string strVideoStoreCfg = "$(CPMP_LOCAL_DIR)rawdisk.cfg";
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == 0)
   {
    iErrorCondition = ERR_BIN_MANAGER_OPEN_LOCAL_MACHINE;
    return iErrorCondition;
   }

  strVideoStoreCfg = ini->ReadFileName ("MediaStorage", "RawDiskCfg",
		strVideoStoreCfg);

  delete ini;

  // open the raw disk config file
  if (DoesFileExist(strVideoStoreCfg) == false)
   {
    iErrorCondition = ERR_BIN_MANAGER_NO_RAW_DISK_CFG;
    return iErrorCondition;
   }

  ini = CreateIniFile (strVideoStoreCfg);
  if (ini == 0)
   {
    iErrorCondition = ERR_BIN_MANAGER_OPEN_RAW_DISK_CFG;
    return iErrorCondition;
   }

  // read the sections
  StringList listSection;
  ini->ReadSections (&listSection);

  // read the contents of each section
  for (unsigned int iCnt = 0; iCnt < listSection.size(); iCnt++)
   {
    string strSection = listSection.at(iCnt);
    // do all the required keys exist?
    if (
		ini->KeyExists (strSection, "MediaIdentifier") &&
		ini->KeyExists (strSection, "FreeListFile") &&
		ini->KeyExists (strSection, "RawDiskSize")
       )
     {
      CVideoStore *vsp = new CVideoStore;

      vsp->strSection = strSection;
      vsp->strRawVideoDirectory = ini->ReadFileName
		(strSection, "RawVideoDirectory", "");
      vsp->strMediaIdentifier = ini->ReadString
		(strSection, "MediaIdentifier", "");
      vsp->strFreeListFile = ini->ReadFileName
		(strSection, "FreeListFile", "");
      vsp->llRawDiskSize = ini->ReadInteger
		(strSection, "RawDiskSize", 0);

      vsp->llRawDiskSizeActual = GetFileLength (vsp->strRawVideoDirectory) /
		RF_BLOCK_SIZE;

      listVideoStore.push_back (vsp);
     }
   }


  delete ini;

  return 0;
}  /* LoadRawDiskCfg */

int CCheckRFL::RepairRawDiskCfg (int iVideoStore)
{
  CIniFile *ini;

  if (iVideoStore < 0 || iVideoStore >= (int)listVideoStore.size())
    return 0;

  // open the LocalMachine ini file
  string strVideoStoreCfg = "$(CPMP_LOCAL_DIR)rawdisk.cfg";
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == 0)
   {
    return ERR_BIN_MANAGER_OPEN_LOCAL_MACHINE;
   }

  strVideoStoreCfg = ini->ReadFileName ("MediaStorage", "RawDiskCfg",
		strVideoStoreCfg);

  delete ini;

  // open the raw disk config file
  if (DoesFileExist(strVideoStoreCfg) == false)
   {
    return ERR_BIN_MANAGER_NO_RAW_DISK_CFG;
   }

  ini = CreateIniFile (strVideoStoreCfg);
  if (ini == 0)
   {
    return ERR_BIN_MANAGER_OPEN_RAW_DISK_CFG;
   }

  CVideoStore *vsp = listVideoStore.at(iVideoStore);

  ini->WriteInteger (vsp->strSection, "RawDiskSize",
                vsp->llRawDiskSizeActual);
  vsp->llRawDiskSize = vsp->llRawDiskSizeActual;

  delete ini;

  return 0;
}  /* RepairRawDiskCfg */


void CCheckRFL::FindAndProcessClips (bool bCountOnly)
/*
	the CountOnly flag will only count up the total number
	of video tracks.
*/
{

  // load the root bins
  StringList binList;
  CPMPGetBinRoots (&binList);

  // cycle through each root bin
  for (unsigned int iCnt = 0; iCnt < binList.size() &&
		iErrorCondition == 0 && bRequestStop == false; iCnt++)
   {
    // examine all clips in this root directory
    ExamineAllClips (binList[iCnt], bCountOnly);

    // examine all sub-bins
    BinDirectoryRecurse (binList[iCnt], bCountOnly);
   }

}  /* FindAndProcessClips */

void CCheckRFL::BinDirectoryRecurse (string &strPath, bool bCountOnly)
{
  bool bFoundBin;
  CBinDir binDir;
  string strLocalPath;
  bool bFoundOne;

  // find the first bin
  bFoundOne = binDir.findFirst (strPath, BIN_SEARCH_FILTER_BIN);

  while (bFoundOne && (iErrorCondition == 0) && (bRequestStop == false))
   {
    // determine name of current bin
    strLocalPath = AddDirSeparator(strPath) + binDir.fileName;

    // examine all clips in this directory
    ExamineAllClips (strLocalPath, bCountOnly);

    // recurse to next level in the bin hierarchy
    BinDirectoryRecurse (strLocalPath, bCountOnly);

    // find next bin at current level
    bFoundOne = binDir.findNext ();
   }
}  /* BinDirectoryRecurse */


void CCheckRFL::ExamineAllClips (string &strPath, bool bCountOnly)
{
  bool bFoundOne;
  CBinManager binMgr;
  CBinDir binDir;

  bFoundOne = binDir.findFirst (strPath, BIN_SEARCH_FILTER_CLIP);

  while (bFoundOne && (iErrorCondition == 0) && (bRequestStop == false))
   {
    // open the clip
    int status;
    auto clip = binMgr.openClip (strPath, binDir.fileName, &status);
    if (status == 0)
     {
      // look for all the video tracks in this clip
      int iSelect = 0;
      CVideoProxy *vpp;
      do
       {
        vpp = clip->getVideoProxy (iSelect);

        // increment the track counter
        if (vpp != 0)
         {
          // if we are counting, just increment
          if (bCountOnly == false)
           {
            ExamineOneTrack (strPath, binDir.fileName, iSelect, vpp);
           }

          iSelect++;
          iCurrentTrack++;
         }
       } while (vpp != 0);

      binMgr.closeClip(clip);     // Done with clip

      // Look for version clips nested inside this clip
      string thisClipPath = AddDirSeparator(strPath)+binDir.fileName;
      ExamineAllClips(thisClipPath, bCountOnly);
      
     }
    else if (bCountOnly)
     {
      // skip over bad clips, but keep a list of them
      string strClipPath = binMgr.makeClipPath (strPath, binDir.fileName);
      strlBadClip.push_back (strClipPath);
     }

    // find the next clip in this directory
    bFoundOne = binDir.findNext ();
   }
}  /* ExamineAllClips */

void CCheckRFL::ExamineOneTrack (const string &strPath, const string &strClip,
		int iVideoSelect, CVideoProxy *vpp)
{
  int retVal;

  // Open appropriate frame list (version clip hack)
  int frameListSelector = vpp->getParentClip()->getMainFrameListIndex();
  CVideoFrameList *vflp = vpp->getVideoFrameList(frameListSelector);

  // if the .CLP file (variable 'MediaAllocatedFrameCount")
  // indicated that no media is allocated for this track,
  // skip field-by-field examination. This improves speed of
  // CheckVideoStore for ControlDailies esp. 6/23/05 by KT
  if (vflp->getMediaAllocatedFrameCount(false)==0) return;

  // keep track of the frames in this track
  iCurrentFrame = 0;
  iTotalFrame = vflp->getTotalFrameCount();

  vector<SMediaAllocation> mediaAllocList;

  retVal = vpp->PeekAtMediaAllocation(mediaAllocList, &iCurrentFrame,
                                      &bRequestStop);
  if (retVal != 0)
    {
     TRACE_0(errout << "ERROR: In CCheckRFL::ExamineOneTrack, PeekAtMediaAllocation returned "
                    << retVal);
     return;  // Error, now what?
    }

  vector<SMediaAllocation>::iterator allocIter;
  for (allocIter = mediaAllocList.begin(); allocIter != mediaAllocList.end();
       ++allocIter)
   {
    if (bRequestStop)
      break;  // Operator wants to stop

    SMediaAllocation *mediaAlloc = &(*allocIter);

    CMediaAccess *mediaAccess = mediaAlloc->mediaAccess;

    if (mediaAccess->getMediaType() != MEDIA_TYPE_RAW)
      break; // Not a video store, so don't bother with this track

    CBlockRun *brp = new CBlockRun;
    brp->llStart = mediaAlloc->index / RF_BLOCK_SIZE;
    brp->llLength = mediaAlloc->size / RF_BLOCK_SIZE;
    brp->strMediaIdentifier = mediaAccess->getMediaIdentifier();
    string strLocal = AddDirSeparator (strPath) + strClip;
    char caClipName[512];
    sprintf (caClipName, "%s (track %d)", strLocal.c_str(),
                        iVideoSelect);
    brp->strClipName = caClipName;
    brp->iVideoTrack = iVideoSelect;

    // add this run to the list of runs
    listInUse.push_back (brp);

   }

#ifdef OLDWAY
  // if the .CLP file (variable 'MediaAllocatedFrameCount")
  // indicated that no media is allocated for this track,
  // skip field-by-field examination. This improves speed of
  // CheckVideoStore for ControlDailies esp. 6/23/05 by KT
  if (vflp->getMediaAllocatedFrameCount(false)==0) return;

  // keep track of the frames in this track
  iCurrentFrame = 0;
  iTotalFrame = vflp->getTotalFrameCount();

  CBlockRun *brp = 0;

  for ( ; iCurrentFrame < iTotalFrame; iCurrentFrame++)
   {
    // get pointer to current frame
    CVideoFrame *frame = vflp->getFrame (iCurrentFrame);
    for (int iField = 0; iField < vflp->getImageFormat()->getFieldCount()
		&& (bRequestStop == false); iField++)
     {
      // get pointer to current field
      CField *field = frame->getBaseField (iField);

      // get to media location
      CMediaLocation mediaLocation = field->getMediaLocation();

      // get pointer to media access
      CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();

      // does this media exist on one of the video stores?
      bool bVideoStore = false;
      string strMediaIdentifier;

      if (mediaAccess)
       {
        // get the current media identifier
        strMediaIdentifier = mediaAccess->getMediaIdentifier();

        if (mediaAccess->getMediaType() == MEDIA_TYPE_RAW)
         {
          bVideoStore = true;
         }
       }

      if (field->doesMediaStorageExist () && bVideoStore)
       {
        if (brp != 0)
         {
          // is this field contiguous with the previous?
          if (
	  (mediaLocation.getDataIndex()/RF_BLOCK_SIZE ==
		brp->llStart + brp->llLength) &&
	  (strMediaIdentifier == brp->strMediaIdentifier)
             )
           {
            brp->llLength += mediaLocation.getDataSize() / RF_BLOCK_SIZE;
           }
          else
           {
            // add this run to the list of runs
            listInUse.push_back (brp);
            brp = 0;
           }
         }

        // allocate and initialize a new block run, if necessary
        if (brp == 0)
         {
          brp = new CBlockRun;
          brp->llStart = mediaLocation.getDataIndex() / RF_BLOCK_SIZE;
          brp->llLength = mediaLocation.getDataSize() / RF_BLOCK_SIZE;
          brp->strMediaIdentifier = strMediaIdentifier;
          string strLocal = AddDirSeparator (strPath) + strClip;
          char caClipName[512];
          sprintf (caClipName, "%s (track %d)", strLocal.c_str(),
                        iVideoSelect);
          brp->strClipName = caClipName;
          brp->iVideoTrack = iVideoSelect;
         }
       }
     }
   }

  // keep track of this run
  if (brp)
   {
    listInUse.push_back (brp);
   }
#endif // OLDWAY
}  /* ExamineOneTrack */

void CCheckRFL::AnalyzeResults ()
{
  // sort the in use runs
  SortRuns ();

  // find free runs
  FindFreeRuns ();

  // find the RFL entries
  FindRFL ();

  // find overlapping media
  FindOverlap ();
}  /* AnalyzeResults */

void CCheckRFL::SortRuns ()
{
  vector <CBlockRun *>::iterator posRun;
  vector <CVideoStore *>::iterator posStore;

  // sort the InUse values by video store
  for (posRun = listInUse.begin(); posRun != listInUse.end()
		&& iErrorCondition == 0; posRun++)
   {
    // find the proper video store
    bool bOrphanFlag = true;
    for (posStore = listVideoStore.begin(); posStore != listVideoStore.end();
		posStore++)
     {
      if ((*posStore)->strMediaIdentifier == (*posRun)->strMediaIdentifier)
       {
        // found the correct video store.  Now insert in correct
        // position in list
        vector <CBlockRun *>::iterator posInsert;
        posInsert = (*posStore)->listInUse.begin();
        while ( (posInsert != (*posStore)->listInUse.end()) &&
		((*posRun)->llStart >= (*posInsert)->llStart) )
         {
          posInsert++;
         }

        if (posInsert == (*posStore)->listInUse.end())
         {
          (*posStore)->listInUse.push_back (*posRun);
         }
        else
         {
          (*posStore)->listInUse.insert (posInsert, *posRun);
         }


        // this run is not orphaned
        bOrphanFlag = false;
       }
     }

    (*posRun)->bOrphanFlag = bOrphanFlag;
   }
}  /* SortRuns */

void CCheckRFL::FindFreeRuns ()
{
  vector <CBlockRun *>::iterator posRun;
  vector <CVideoStore *>::iterator posStore;
  CBlockRun *brp;
  MTI_INT64 llLast;

  for (posStore = listVideoStore.begin(); posStore != listVideoStore.end() &&
	iErrorCondition == 0; posStore++)
   {
    llLast = 0;
    for (posRun = (*posStore)->listInUse.begin();
		posRun != (*posStore)->listInUse.end(); posRun++)
     {
      if ( (*posRun)->llStart > llLast )
       {
        brp = new CBlockRun;
        brp->llStart = llLast;
        brp->llLength = (*posRun)->llStart - llLast;
 
        (*posStore)->listFree.push_back (brp);
       }

      if ( (*posRun)->llStart + (*posRun)->llLength > llLast)
       {
        llLast = (*posRun)->llStart + (*posRun)->llLength;
       }
     }

    // add any free space at the end
    if (llLast < (*posStore)->llRawDiskSize)
     {
      brp = new CBlockRun;
      brp->llStart = llLast;
      brp->llLength = (*posStore)->llRawDiskSize - llLast;

      (*posStore)->listFree.push_back (brp);
     }
   }
}  /* FindFreeRuns */

void CCheckRFL::FindRFL ()
{
  MTI_INT64 llStart, llLength;
  CRawDiskFreeList *freeList;
  CBlockRun *brp;
  int iRet;

  vector <CVideoStore *>::iterator posStore;
  for (posStore = listVideoStore.begin(); posStore != listVideoStore.end()
	&& iErrorCondition == 0; posStore++)
   {
    freeList = new CRawDiskFreeList ((*posStore)->strFreeListFile);

    // read the free list
    iRet = freeList->readFreeListFile ();
    if (iRet)
     {
      iErrorCondition = ERR_BIN_MANAGER_READ_FREE_LIST;
     }

    // get all the free blocks
    freeList->findFirst (llStart, llLength);
    while (llStart != -1 && llLength != -1)
     {
      brp = new CBlockRun;
      brp->llStart = llStart;
      brp->llLength = llLength;

      (*posStore)->listRFL.push_back (brp);

      // read in the next one
      freeList->findNext (llStart, llLength);
     }

    delete freeList;
   }

  
}  /* FindRFL */

void CCheckRFL::FindOverlap ()
{
  vector <CVideoStore *>::iterator posStore;
  vector <CBlockRun *>::iterator posRunA, posRunB;

  // loop through each video store
  for (posStore = listVideoStore.begin(); posStore != listVideoStore.end();
		posStore++)
   {
    for (posRunA = (*posStore)->listInUse.begin(); posRunA !=
		(*posStore)->listInUse.end(); posRunA++)
     {
      for (posRunB = posRunA + 1; posRunB != (*posStore)->listInUse.end();
		posRunB++)
       {
        if ( (*posRunA)->llStart + (*posRunA)->llLength >
		(*posRunB)->llStart )
         {
          // these media are in conflict
          listMediaOverlapA.push_back ( (*posRunA)->strClipName );
          listMediaOverlapB.push_back ( (*posRunB)->strClipName );
         }
       }
     }
   }

  // look for orphaned media
  for (posRunA = listInUse.begin(); posRunA != listInUse.end(); posRunA++)
   {
    if ( (*posRunA)->bOrphanFlag )
     {
      // this media does not appear an any video store.
      listMediaOverlapA.push_back ( (*posRunA)->strClipName );
      listMediaOverlapB.push_back ("No Existing VideoStore");
     }
   }

}  /* FindOverlap */

