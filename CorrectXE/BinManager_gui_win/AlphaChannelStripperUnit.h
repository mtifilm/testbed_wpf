//---------------------------------------------------------------------------

#ifndef AlphaChannelStripperUnitH
#define AlphaChannelStripperUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "IniFile.h"         // for StringList
#include "MediaInterface.h"  // for Media Hack Commands
#include <atomic>
//---------------------------------------------------------------------------
class TAlphaChannelStripperForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *AreYouSurePanel;
        TPanel *Separator1;
        TButton *SetupPanelOkButton;
        TButton *SetupPanelCancelButton;
        TPanel *ProgressPanel;
        TLabel *ClipNameLabel;
        TLabel *ClipNameValue;
        TLabel *ModifyingModeLabel;
        TLabel *FrameLabel;
        TLabel *FrameValue;
        TLabel *OfLabel;
        TLabel *OfValue;
        TProgressBar *ClipListProgressBar;
        TProgressBar *FrameIndexProgressBar;
        TPanel *Separator2;
        TButton *ProgressPanelCloseButton;
        TTimer *ProgressTimer;
        TLabel *StatusLabel;
   TLabel *Label1;
   TLabel *Label2;
        void __fastcall SetupPanelOkButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ProgressPanelCloseButtonClick(
          TObject *Sender);
        void __fastcall ProgressTimerTimer(TObject *Sender);
private:	// User declarations
        string mBinPath;
        StringList mClipList;
        void *mProcessingThreadId;

        static void ProcessingThread(void *aThreadArg, void *aThreadHandle);
        void HackAllClips();
        void HackOneClip(string aClipName);

        // Processing Thread communications
        // TO thread
        EMediaHackCommand mHackCommand;
        bool mCancelledFlag;
        // FROM thread
        AnsiString mClipName;
        int mClipIndex;
        int mTotalClipCount;
        std::atomic_int mFrameIndex;
        std::atomic_bool mAborted;
        int mTotalFrameCount;
        bool mProcessingIsCompleteFlag;
        //
public:		// User declarations
        __fastcall TAlphaChannelStripperForm(TComponent* Owner);
        void Reset();
        void SetBinPath(string aBinPath);
        void AddClip(string aClipName);

};
//---------------------------------------------------------------------------
extern PACKAGE TAlphaChannelStripperForm *AlphaChannelStripperForm;
//---------------------------------------------------------------------------
#endif
