object ClipPropertyWin: TClipPropertyWin
  Left = 528
  Top = 540
  VertScrollBar.Visible = False
  AutoSize = True
  BorderIcons = [biSystemMenu]
  BorderWidth = 10
  Caption = 'Clip Properties'
  ClientHeight = 105
  ClientWidth = 283
  Color = clBtnFace
  Constraints.MinHeight = 150
  Constraints.MinWidth = 319
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object ClipPropertyNameValLabel: TLabel
    Left = 88
    Top = 0
    Width = 15
    Height = 13
    Caption = '-----'
  end
  object Label3: TLabel
    Left = 0
    Top = 16
    Width = 33
    Height = 13
    Caption = 'Status:'
  end
  object ClipPropertyStatusValLabel: TLabel
    Left = 88
    Top = 16
    Width = 15
    Height = 13
    Caption = '-----'
  end
  object ClipPropertyNumFixesLabel: TLabel
    Left = 0
    Top = 64
    Width = 79
    Height = 13
    Caption = 'Number of Fixes:'
  end
  object ClipPropertyNumFixesValLabel: TLabel
    Left = 88
    Top = 64
    Width = 15
    Height = 13
    Caption = '-----'
  end
  object Label7: TLabel
    Left = 0
    Top = 32
    Width = 35
    Height = 13
    Caption = 'Format:'
  end
  object ClipPropertyFormatValLabel: TLabel
    Left = 88
    Top = 32
    Width = 15
    Height = 13
    Caption = '-----'
  end
  object Label8: TLabel
    Left = 0
    Top = 48
    Width = 75
    Height = 13
    Caption = 'Media Identifier:'
  end
  object ClipPropertyMediaIdentifierValLabel: TLabel
    Left = 88
    Top = 48
    Width = 15
    Height = 13
    Caption = '-----'
  end
  object ClipPropertyOKButton: TButton
    Left = 112
    Top = 80
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = ClipPropertyOKButtonClick
  end
end
