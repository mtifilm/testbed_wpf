//---------------------------------------------------------------------------

#ifndef ClipNameTemplateUnitH
#define ClipNameTemplateUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;
//---------------------------------------------------------------------------

class TClipNameTemplateForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *SelectATemplateGroupBox;
        TLabel *NumberOfDigitsLabel;
        TRadioButton *TemplateOption0RadioButton;
        TRadioButton *TemplateOption2RadioButton;
        TRadioButton *TemplateOption1RadioButton;
        TRadioButton *TemplateOption3RadioButton;
        TComboBox *NumberOfDigitsComboiBox;
        TButton *OKButton;
        TButton *CancelButton;
private:	// User declarations
public:		// User declarations
        __fastcall TClipNameTemplateForm(TComponent* Owner);
        void SetSelectedTemplate(const string &newTemplate);
        string GetSelectedTemplate();
        void SetNumberOfDigits(int newNumberOfDigits);
        int GetNumberOfDigits();

        // public constants
        static string FolderNameTemplateString;
        static string FileNameTemplateString;
        static string SequenceNumberTemplateString;
};
//---------------------------------------------------------------------------
extern PACKAGE TClipNameTemplateForm *ClipNameTemplateForm;
//---------------------------------------------------------------------------
#endif
