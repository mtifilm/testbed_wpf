//---------------------------------------------------------------------------

#ifndef BinMgrTestUnitH
#define BinMgrTestUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include "BinMgrGUIInterface.h"
#include <ComCtrls.hpp>
#include "VTimeCodeEdit.h"
#include <Dialogs.hpp>

class CClip;

//---------------------------------------------------------------------------
class CNavBinMgrGUIInterface : public CBinMgrGUIInterface
{
public:
   void ClipDoubleClick(const string& clipFileName);
   void ClipDeleted(const string& clipFileName);
};

//---------------------------------------------------------------------------
class TBinMgrTestForm : public TForm
{
__published:	// IDE-managed Components
   TMainMenu *MainMenu;
   TMenuItem *File;
   TMenuItem *Open;
   TMenuItem *N1;
   TMenuItem *Exit;
   TEdit *ClipName;
   TRichEdit *DebugOutput;
   TStatusBar *StatusBar;
   TButton *Browse;
   TComboBox *ComboBox1;
   VTimeCodeEdit *TimeCodeField;
   TButton *SetTCButton;
   TEdit *Edit1;
   TButton *Button1;
   void __fastcall OpenClick(TObject *Sender);
   void __fastcall ExitClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall FormDestroy(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
   CNavBinMgrGUIInterface *binMgrGUIInterface;
   void setCurrentClipFileName(const string& clipFileName);

   string currentClipFileName;
   auto currentClip;

public:		// User declarations
   __fastcall TBinMgrTestForm(TComponent* Owner);
   string getCurrentClipFileName();
   void OpenClipByName(const string &clipName);
   void DebugMsg(const string &msg);
   void DebugMsg(ostringstream &ostr);
};
//---------------------------------------------------------------------------
extern PACKAGE TBinMgrTestForm *BinMgrTestForm;
//---------------------------------------------------------------------------
#endif
