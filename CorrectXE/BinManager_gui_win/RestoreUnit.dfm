object RestoreForm: TRestoreForm
  Left = 504
  Top = 129
  Width = 725
  Height = 408
  Caption = 'Restore'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RestoreListView: TListView
    Left = 8
    Top = 8
    Width = 697
    Height = 321
    Columns = <
      item
        AutoSize = True
        Caption = 'Project'
      end
      item
        AutoSize = True
        Caption = 'Scanlist Name'
      end
      item
        AutoSize = True
        Caption = 'Archive Date'
      end
      item
        AutoSize = True
        Caption = 'Hostname'
      end
      item
        AutoSize = True
        Caption = 'File'
      end>
    MultiSelect = True
    RowSelect = True
    SortType = stText
    TabOrder = 0
    ViewStyle = vsReport
  end
  object RestoreButton: TButton
    Left = 32
    Top = 336
    Width = 75
    Height = 25
    Caption = '&Restore'
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 136
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
