// ---------------------------------------------------------------------------
#include <vcl.h>
#include <dir.h>
#pragma hdrstop

#include "AlphaChannelStripperUnit.h"
#include "AlphaMatteShowHideUnit.h"
#include "AutoDiff.h"
#include "autoDiffGui.h"
#include "BinDir.h"
#include "BinManagerGUIUnit.h"
#include "BinMgrGUIInterface.h"
#include "ClipAPI.h"
#include "ClipFileExporter.h"
#include "ClipFileExportForm.h"
#include "ClipPropertiesUnit.h"
#include "ClipSchemePropertiesForm.h"
#include "ComboBoxHistoryList.h"
#include "CommitVersionUnit.h"
#include "DeleteBinUnit.h"
#include "DllSupport.h"
#include "DpxHeaderEditorUnit.h"
#include "EnterCommentUnit.h"
#include "err_bin_manager.h"
#include "err_format.h"
#include "ExportTCUnit.h"
#include "FileSweeper.h"
#include "FixCountingUnit.h"
#include "HeaderDumpUnit.h"
#include "ImageFileMediaAccess.h"
#include "ImportTCUnit.h"
#include "IniFile.h"
#include "JobManager.h"
#include "JobPropertiesUnit.h"
#include "LineEngine.h"
#include "MediaFrameBuffer.h"
#include "MediaFileIO.h"
#include "MediaStorage3.h"
#include "MTIio.h"
#include "MTImalloc.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "MultipleDirectoriesUnit.h"
#include "MTIWinInterface.h"
#include "NewBinUnit.h"
#include "OverlayItem.h"
#include "PixelRegions.h"       // mpr for number of Fixes
#include "ProxyDisplayer.h"
#include "SaveRestore.h"        // mpr for number of Fixes
#include "ShowModalDialog.h"
#include "ThreadLocker.h"
#include "ToolObject.h"         // This is wrong level but hack for now
#include "UnitCheckVideoStore.h"
#include "UnitMultiBrowse.h"
#include "UnitPref.h"

#define MissingClipScheme "Select a format"
#define MissingDiskScheme "Select a location"
#define EDIT_CANCEL_BUTTON_EDIT_STRING   "Edi&t"
#define EDIT_CANCEL_BUTTON_CANCEL_STRING "C&ancel"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "TriTimecode"
#pragma link "PopupComboBox"
#pragma link "MTIUNIT"
#pragma link "VTimeCodeEdit"
#pragma link "popupcombobox"
#pragma link "vtimecodeedit"
#pragma link "NakedProxyUnit"
#pragma resource "*.dfm"

#include <algorithm>
using std::sort;
#include <filesystem>
#if __cplusplus <= 201103
	// <= 10.3.2
#  define STDFILESYSTEM std::tr2::sys
#else
	// >= 10.3.3
#  define STDFILESYSTEM std::filesystem
#endif
#include <io.h>
#include <memory>
#include <string>
using std::string;

#define clLightYellow TColor(0x00ccffff)

// Whenever you add or remove a bin manager column, increment this
// generation number so we know to ignore the saved column widths
#define COLUMN_GENERATION_NUMBER 3

typedef CBinMgrGUIInterface::lockID_t lockID_t;
const AnsiString VirtualizationDummyNodeName = "####DUMMY####";

TBinManagerForm *BinManagerForm;

// Compare bin paths without worrying about freakin' trailing separators
namespace
{
inline bool IsSameBinPath(const string &path1, const string &path2)
{
   return EqualIgnoreCase(AddDirSeparator(path1), AddDirSeparator(path2));
}
}

// Encryption for "CONTROL-IO"
MTI_UINT32 FEATURE_CONTROL_IO[] =
{0x5ae3c10, 0x15f14502, 0x6c7bf217, 0x24e76a9e, 0x7bce912d, 0x28e98974};

// Declarations used only in this module
void ReParent(TControl *Child, TWinControl *Parent);

class CException
{
}; // exception for local handling

class CBackupException : public CException
{
};
// ---------------------------------------------------------------------------
// Static Member Initialization

CBinMgrGUIInterface* TBinManagerForm::binMgrGUIInterface = 0;


//////////////////////////////////////////////////////////////////////////////
///////////////// CLIP VIEW STUFF ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------
// New "owner data" clip list view stuff

struct SVersionDataItem
{
   string versionName;
   string versionDisplayName;
   EMediaStatus mediaStatus;
   CTimecode videoMediaModified;
   string createDate;
   string comments;
   bool versionIsSelected;
   bool isHighlighted;
   bool selectStateChangedBySoftware;
   string mediaLocation;
   bool isVersioned;

   SVersionDataItem(const CClipInitInfo *aClipInitInfo = nullptr) : mediaStatus(MEDIA_STATUS_UNKNOWN), videoMediaModified(0),
         versionIsSelected(false), isHighlighted(false), selectStateChangedBySoftware(false)
   {
      if (aClipInitInfo != nullptr)
      {
         InitVersionDataItem(*aClipInitInfo);
      }
   };

	void InitVersionDataItem(const CClipInitInfo &clipInitInfo)
	{
//		DBTIMER(InitVersionDataItem);
		versionName = clipInitInfo.getClipName();
//		DBTRACE(versionName);
		mediaStatus = clipInitInfo.getClipStatus();
		string clipPath = AddDirSeparator(clipInitInfo.getBinPath()) + versionName;
      CBinManager binManager;
      versionDisplayName = binManager.makeVersionClipName(clipPath);
      videoMediaModified = CTimecode(binManager.getModifiedFrameCount(clipPath));
		comments = clipInitInfo.getDescription();

      createDate = clipInitInfo.getCreateDateTime();
      // HACK - chop off the useless trailing ".00"
      if (createDate[createDate.length() - 3] == '.')
      {
         createDate.erase(createDate.end() - 3, createDate.end());
      }

      // Sadly the media location is computed incorrectly for version clips
      // and always is the parent's instead. So as an awful hack, we simply
      // tack on the version clip name.... uggh!
      mediaLocation = clipInitInfo.getMediaLocation();
      if (!mediaLocation.empty())
      {
         // This time, with feeling: Ugggggghh!
         mediaLocation += " " + versionDisplayName;
      }

      ClipIdentifier versionClipId(clipPath);
      isVersioned = binManager.doesClipHaveVersions(versionClipId);
   };

   ~SVersionDataItem()
   {
   };

   // "find" helper function - equality depends only on clip name
   bool operator == (const SVersionDataItem * that) const
   {
      return EqualIgnoreCase(this->versionDisplayName, that->versionDisplayName);
   };

   // "sort" helper functions - Weak Strict Orderings
   struct NameAscendingOrder
   {
      bool operator()(const SVersionDataItem * leftItem, const SVersionDataItem * rightItem) const
      {
         return SmartLessThan(leftItem->versionDisplayName, rightItem->versionDisplayName);
      };
   };

   struct StatusAscendingOrder
   {
		bool operator()(const SVersionDataItem * leftItem, const SVersionDataItem * rightItem) const
      {
         return (0 > CTrack::compareMediaStatus(leftItem->mediaStatus, rightItem->mediaStatus));
      };
   };

   struct VideoMediaModifiedAscendingOrder
   {
      bool operator()(const SVersionDataItem * leftItem, const SVersionDataItem * rightItem) const
      {
         // Sorts by duration in number of frames, not lexicographically
         // or actual running time - should be good enuff
         return (leftItem->videoMediaModified.getAbsoluteTime() < rightItem->videoMediaModified.getAbsoluteTime());
      };
   };

   struct CommentsAscendingOrder
   {
      bool operator()(const SVersionDataItem * leftItem, const SVersionDataItem * rightItem) const
      {
         return LessThanIgnoreCase(leftItem->comments, rightItem->comments);
      };
   };

   struct CreateDateAscendingOrder
   {
      bool operator()(const SVersionDataItem * leftItem, const SVersionDataItem * rightItem) const
      {
         return (leftItem->createDate < rightItem->createDate);
      };
   };
};

// TODO: Move this to a .h file?
struct SClipDataItem
{
   string clipName;
   EMediaStatus mediaStatus;
   string imageFormat;
   CTimecode inTimecode;
   CTimecode outTimecode;
   CTimecode clipDuration;
   CTimecode videoMediaDuration;
   CTimecode audioMediaDuration;
   string mediaLocation;
   string clipDescription;
   string createDate;
   bool clipIsSelected;
   bool isHighlighted;
   bool selectStateChangedBySoftware;
   vector<SVersionDataItem*>versionList;

   SClipDataItem(const CClipInitInfo *aClipInitInfo = nullptr) : mediaStatus(MEDIA_STATUS_UNKNOWN), inTimecode(CTimecode::NOT_SET),
         outTimecode(CTimecode::NOT_SET), clipDuration(CTimecode::NOT_SET), videoMediaDuration(CTimecode::NOT_SET),
         audioMediaDuration(CTimecode::NOT_SET), clipIsSelected(false), isHighlighted(false), selectStateChangedBySoftware(false)
   {
      if (aClipInitInfo != nullptr)
      {
			InitClipDataItem(*aClipInitInfo);
      }
   };

	void InitClipDataItem(const CClipInitInfo &clipInitInfo)
	{
				CHRTimer hrt;
		clipName = clipInitInfo.getClipName();
		mediaStatus = clipInitInfo.getClipStatus();
		imageFormat = clipInitInfo.getImageFormatName();
		inTimecode = clipInitInfo.getInTimecode();
      outTimecode = clipInitInfo.getOutTimecode();
      clipDuration = clipInitInfo.getDurationTimecode();
      videoMediaDuration = clipInitInfo.getVideoMediaDuration();
      audioMediaDuration = clipInitInfo.getAudioMediaDuration();
      mediaLocation = clipInitInfo.getMediaLocation();
      if (mediaLocation.empty())
      {
         mediaLocation = clipInitInfo.getMarkedClipMediaLoc();
      }

		clipDescription = clipInitInfo.getDescription();
      createDate = clipInitInfo.getCreateDateTime();

		// HACK - chop off the useless trailing ".00"
		if (createDate[createDate.length() - 3] == '.')
      {
         createDate.erase(createDate.end() - 3, createDate.end());
      }

      for (unsigned i = 0; i < versionList.size(); ++i)
      {
			delete versionList[i];
		}

		versionList.clear();

		// Get the first version in the clip
      string clipPath = AddDirSeparator(clipInitInfo.getBinPath()) + clipInitInfo.getClipName();
      string masterClipPath = ClipIdentifier(clipPath).GetMasterClipPath();
      CBinDir clipDir;
		bool foundANewVersionInTheClip = clipDir.findFirst(clipPath, BIN_SEARCH_FILTER_CLIP);

		// Construct an "Owner Data" list item for each clip in the bin
		while (foundANewVersionInTheClip)
		{
			CBinManager binManager;
			int status = 0;
			std::auto_ptr<CClipInitInfo>versionInitInfo(binManager.openClipOrClipIni(clipPath, clipDir.fileName, &status));
			ClipIdentifier versionClipId(masterClipPath, clipDir.fileName);
         if (status == 0 && binManager.isVersionReallyActive(versionClipId))
         {
            TRACE_3(errout << "(((((( FOUND VERSION " << clipDir.fileName << " in clip " << masterClipPath);

				SVersionDataItem *versionDataItem = new SVersionDataItem(&*versionInitInfo);

            // Add the new list item to the list
            versionList.push_back(versionDataItem);
         }

			{
				// Recurse to find versions of versions!
				std::auto_ptr<SClipDataItem>clipDataItem(new SClipDataItem(&*versionInitInfo));
				for (int i = 0; i < clipDataItem->versionList.size(); ++i)
            {
					versionList.push_back(clipDataItem->versionList[i]);
            }

            clipDataItem->versionList.clear();
         }

			// Go on to the next version
			foundANewVersionInTheClip = clipDir.findNext();
		}
	};

   ~SClipDataItem()
   {
      for (unsigned i = 0; i < versionList.size(); ++i)
      {
         delete versionList[i];
      }

      versionList.clear();
   };

   // "find" helper function - equality depends only on clip name
   bool operator == (const SClipDataItem * that) const
   {
      return EqualIgnoreCase(this->clipName, that->clipName);
   };

   // "sort" helper functions - Weak Strict Orderings
   struct NameAscendingOrder
   {
		bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return SmartLessThan(leftItem->clipName, rightItem->clipName);
      };
   };

   struct StatusAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return (0 > CTrack::compareMediaStatus(leftItem->mediaStatus, rightItem->mediaStatus));
      };
   };

   struct ImageTypeAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return (leftItem->imageFormat < rightItem->imageFormat);
      };
   };

   struct InTimecodeAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         // I think only lexicographical sort makes sense
         char leftTC[12], rightTC[12];
         leftItem->inTimecode.getTimeASCII(leftTC);
         rightItem->inTimecode.getTimeASCII(rightTC);
         return (string(leftTC) < string(rightTC));
      };
   };

   struct OutTimecodeAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         // I think only lexicographical sort makes sense
         char leftTC[12], rightTC[12];
         leftItem->outTimecode.getTimeASCII(leftTC);
         rightItem->outTimecode.getTimeASCII(rightTC);
         return (string(leftTC) < string(rightTC));
      };
   };

   struct DurationAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         // Sorts by duration in number of frames, not lexicographically
         // or actual running time - should be good enuff
         return (leftItem->clipDuration.getAbsoluteTime() < rightItem->clipDuration.getAbsoluteTime());
      };
   };

   struct VideoMediaDurationAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return (leftItem->videoMediaDuration.getAbsoluteTime() < rightItem->videoMediaDuration.getAbsoluteTime());
      };
   };

   struct AudioMediaDurationAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return (leftItem->audioMediaDuration.getAbsoluteTime() < rightItem->audioMediaDuration.getAbsoluteTime());
      };
   };

   struct MediaLocationAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return SmartLessThan(leftItem->mediaLocation, rightItem->mediaLocation);
      };
   };

   struct DescriptionAscendingOrder
   {
		bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return LessThanIgnoreCase(leftItem->clipDescription, rightItem->clipDescription);
      };
   };

   struct CreateDateAscendingOrder
   {
      bool operator()(const SClipDataItem * leftItem, const SClipDataItem * rightItem) const
      {
         return (leftItem->createDate < rightItem->createDate);
      };
   };
};

//////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------
// Callback for "clip status changed" notification hack
//
namespace
{
CThreadLock ClipRefreshListLock;
vector<ClipIdentifier>ClipRefreshList;

void ClipStatusChangedNotificationHackCallback(const string &binPath, const string &clipName)
{
	TRACE_2(errout << "INFO: Project Manager was notified that the status has " << "changed for " << endl << "      the clip " << clipName <<
			" in bin " << binPath);

	ClipIdentifier clipId(binPath, clipName);

   {
      CAutoThreadLocker lock(ClipRefreshListLock);
      ClipRefreshList.push_back(clipId);

      // This timer now runs all the time...
      // BinManagerForm->ClipRefreshListSweepTimer->Enabled = true;
   }
}

bool SweepClipRefreshList()
{
   bool retVal = false;
   list<ClipIdentifier>ClipRefreshListCopy;
   // TRACE_3(errout << "SWEEP CLIP REFRESH LIST");
   {
      CAutoThreadLocker lock(ClipRefreshListLock);
      if (BinManagerForm->Visible && (BinManagerForm->WindowState != wsMinimized) && (ClipRefreshList.size() > 0))
      {
         retVal = true;

         // Copy out the refresh list
         for (unsigned int i = 0; i < ClipRefreshList.size(); ++i)
         {
            ClipRefreshListCopy.push_back(ClipRefreshList[i]);
         }
      }

      ClipRefreshList.clear();
   }
   while (ClipRefreshListCopy.size() > 0)
   {
      ClipIdentifier clipId = ClipRefreshListCopy.front();
      // Remove ALL entries that match!
      ClipRefreshListCopy.remove(clipId);

      // We always refresh the entire master clip, never individual versions!
      string binPath = clipId.GetBinPath();
      string clipName = clipId.GetMasterClipName();

      if (IsSameBinPath(binPath, BinManagerForm->GetSelectedBin()))
      {
         TRACE_3(errout << "Refresh " << clipName);
         BinManagerForm->RefreshOneClipListItemByName(clipName);
      }
   }
   // TRACE_3(errout << "END SWEEP");
   return retVal;
}
}; // anonymous namespace
// ---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////
////////////////////// GLOBAL SHIT ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void ShowBinManagerGUI(bool show, int mode)
{
   TComponent *Owner = Application;

   if (BinManagerForm == 0)
   {
      BinManagerForm = new TBinManagerForm(Owner);
      BinManagerForm->RestoreProperties();
   }

   BinManagerForm->SetOpMode(mode);

   if (show)
   {
      BinManagerForm->Show();
   }
   else
   {
      BinManagerForm->Hide();
   }

   // If show is true, then make sure the Project Manager GUI form has the focus
   if (BinManagerForm->Visible)
   {
      if (BinManagerForm->WindowState == wsMinimized)
      {
         BinManagerForm->WindowState = wsNormal;
      }

      BinManagerForm->SetFocus();
   }
}
// ---------------------------------------------------------------------------

void DeleteBinManagerGUI()
{
   if (BinManagerForm != 0)
   {
      delete BinManagerForm;
      BinManagerForm = 0;
   }
}
// ---------------------------------------------------------------------------

bool BlackOut(ClipSharedPtr &cpClip)
{
   unsigned char *yuvImage[2] =
   {nullptr, nullptr};
   int FontSize = 6;

   const CImageFormat *srcVideoTrackFormat = cpClip->getImageFormat(VIDEO_SELECT_NORMAL);
   int srcImageWdth = srcVideoTrackFormat->getPixelsPerLine();
   int srcImageHght = srcVideoTrackFormat->getLinesPerFrame();
   bool srcInterlaced = srcVideoTrackFormat->getInterlaced();

   CVideoFrameList *srcVideoFrameList = cpClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);

   int srcMinFrame = srcVideoFrameList->getInFrameIndex();
   int srcMaxFrame = srcVideoFrameList->getOutFrameIndex() - 1;

   CLineEngine *myEng = CLineEngineFactory::makePixelEng(*srcVideoTrackFormat);

   int srcFieldSize = srcVideoFrameList->getMaxPaddedFieldByteCount();

   yuvImage[0] = new unsigned char[srcFieldSize];
   if (srcInterlaced)
   {
      yuvImage[1] = new unsigned char[srcFieldSize];
   }

   myEng->setExternalFrameBuffer(yuvImage);

   // clear the frame buffer and set up for writing timecodes

   myEng->setFGColor(0);
   myEng->drawRectangle(0, 0, srcImageWdth - 1, srcImageHght - 1);
   myEng->setFGColor(1);
   myEng->setBGColor(0);
   myEng->setFontSize(FontSize);

   // set the timecode for each frame, then write it in
   CTimecode curTime(0);
   char curTimeStr[16];

   for (int i = srcMinFrame; i <= srcMaxFrame; i++)
   {
      CVideoFrame *curFrame = srcVideoFrameList->getFrame(i);

      curFrame->getTimecode(curTime);
      curTime.getTimeASCII(curTimeStr);
      myEng->drawString((srcImageWdth - 11*8*FontSize) / 2, (srcImageHght - 10*FontSize) / 2, curTimeStr);

      curFrame->writeMediaAllFields(yuvImage);
   }

   // clean up storage
   delete[]yuvImage[0];
   delete[]yuvImage[1];
   CLineEngineFactory::destroyPixelEng(myEng);

   // Make sure field list is updated with the status of media
   cpClip->updateAllTrackFiles();

   return (0);
}
// ---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////
////////////////////// TBinManagerForm ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

__fastcall TBinManagerForm::TBinManagerForm(TComponent* Owner) : TMTIForm(Owner), opMode(BM_MODE_NORMAL)
{
   // prevent background color from being painted during update
   ImageProxy->ControlStyle << csOpaque;

   FormPref = new TFormPref(this);
   FormPref->RestoreProperties();

   HackVideoStoreUsage();

   // versions list
   ClipListViewSplitter->Visible = true;
   VersionListView->Visible = true;

   // set up to display "esisting files" for clip create
   FileDisplayer = new ProxyDisplayer;
   if (FileDisplayer)
   {
      FileDisplayer->SetDisplayContext((HDC)Handle);
   }

   FileClipInTCEdit->tcP.setFlags(0);
   FileClipInTCEdit->tcP.setFramesPerSecond(100);

   ClipListColumnToSort = 0;
   VersionListColumnToSort = 0;
   selectedClipCount = 0;
   selectedListIsInvalid = true;
   CurrentBinClipListData = nullptr;
}

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormCreate(TObject *Sender)
{
   OriginalBinTreeWidth = BinTree->Width;
	OriginalCreateClipPanelVisible = true; //UnexpandedNewClipPanel->Visible == false;
   OriginalCreateProxyPanelVisible = ProxyDisplayPanel->Visible;
   lastClickedBinPath = "";
   lastClickedClipPath = "";
   lastClickedVersionPath = "";

   // Initialize the clip list cache by creating an empty entry for the
   // root node
   CurrentBinClipListData = new ClipListDataType;
   StringList binList;
   CPMPGetBinRoots(&binList);
   string rootPath;
   if (binList.size() > 1)
   {
      lastClickedBinPath = binList[0]; // Only use first root
      BinClipListCache[lastClickedBinPath] = CurrentBinClipListData;
      GenerateClipListData(CurrentBinClipListData, lastClickedBinPath);
   }

   // prevent update until we're done setting data
   RefreshBinTree();
   BinTree->Selected = BinTree->Items->Item[0];
   InitCreateClipPanel();

   _ClipFileDigitPrecision = 5;
   _bClipFileNameChange = false;
   _bVideoFileNameChange = false;

   // set up the proxy display
   clipProxy = 0;
   cnvtProxy = new CConvert;
   uipProxy = nullptr;
   uipProxy = (MTI_UINT32*) malloc(ImageProxy->Width * ImageProxy->Height*sizeof(MTI_UINT32));
   if (uipProxy == nullptr)
   {
      _MTIErrorDialog(Handle, "Unable to allocate storage for clip preview display");
   }

   if (binMgrGUIInterface && binMgrGUIInterface->GetInOutDurColsFlag() == false)
   {
      // In, Out, Duration
      HideColumn(COL_INDEX_IN_TIMECODE);
      HideColumn(COL_INDEX_OUT_TIMECODE);
      HideColumn(COL_INDEX_DURATION);
   }

   if (binMgrGUIInterface && binMgrGUIInterface->GetMediaLocColFlag() == false)
   {
      // Disk Scheme
      HideColumn(COL_INDEX_MEDIA_LOCATION);
   }

   iImageFileIndex = -1;
   UpdateFileDetailsPanel("");

   // Register to be notified of clip status changes
   CClip::RegisterForClipStatusChangeNotifications(&ClipStatusChangedNotificationHackCallback);
   SweepTimerAutoShutoff = 10;

   iImageFileIndex = -1;
   UpdateFileDetailsPanel("");

   ChooseFolderOrChooseFileRadioButtonClick(nullptr);

#ifdef DISABLE_VERSIONS_OF_VERSIONS_FEATURE
   CreateVersionOfVersionMenuItem->Visible = false;
   VersionListViewPopupMenuSeparator3->Visible = false;
#endif
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormDestroy(TObject *Sender)
{
   CClip::UnregisterForClipStatusChangeNotifications();

   ClearBinClipListCache();

   delete cnvtProxy;
   free(uipProxy);
   uipProxy = nullptr;

   delete FormPref;
   FormPref = 0;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ProxyVisibleClick(TObject *Sender)
{
   ProxyVisibleToggle();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ProxyVisibleToggle()
{
   ProxyDisplayPanel->Visible = !ProxyDisplayPanel->Visible;
   if (ProxyDisplayPanel->Visible)
   {
      ReloadProxy();
   }
   else
   {
      ClearProxy();
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBCloseItemClick(TObject *Sender)
{
   if (FormState.Contains(fsModal))
   {
      // This will end the modal box
      ModalResult = mrCancel;
   }
   else
   {
      Visible = false;
   }
}

// ---------------------------------------------------------------------------
void TBinManagerForm::RegisterBinMgrGUIInterface(CBinMgrGUIInterface *newInterface)
{
   binMgrGUIInterface = newInterface;
}

// ---------------------------------------------------------------------------
void TBinManagerForm::RefreshBinTree()
{
//	DBTIMER(RefreshBinTree);

	// mlm: to-do: preserve entire expansion of tree if possible

	// Remember the selected node.
	TTreeNode *selectedNode = BinTree->Selected;
   string selectedPath = "";
   if (selectedNode != nullptr)
   {
		selectedPath = getBinPathFromBinTreeNode(selectedNode);
//		DBTRACE2(StringToStdString(selectedNode->Text), selectedPath);
	}

	// Build tree from scratch.
	BinTree->Items->Clear();

	// Get the list of Bin roots, but we only use the first one.
   StringList binList;
   CPMPGetBinRoots(&binList);
	if (binList.size() < 1)
	{
		MTIostringstream ostr;
		ostr << "No project directory configured " << endl
			  << endl
			  << "Please check that CPMP_SHARED_DIR " << endl
           << "environment variable is set correctly. " << endl;
		_MTIErrorDialog(Handle, ostr.str());
		TRACE_0(errout << ostr.str());
		return;
	}

	string rootPath = binList[0];
	if (!DoesDirectoryExist(rootPath))
	{
		MTIostringstream ostr;
		ostr << "Can't find project directory" << endl
			  << rootPath << endl
			  << endl
			  << "Please check that the drive is accessible." << endl;
		_MTIErrorDialog(Handle, ostr.str());
		TRACE_0(errout << ostr.str());
		return;
	}

	// Check the storage available on the root node
	CheckRootNodeStorage(rootPath);

	// Add the root node.
   BinTree->Items->Add(nullptr, rootPath.c_str());
	TTreeNode *rootNode = BinTree->Items->Item[0];

	// Add a placeholder "dummy" child node (?why neeed if we're expanding below?)
	AddDummyBinTreeNodeChildIfNestedBins(rootPath, rootNode);

   // Expand the Root node so there is something to look at
	rootNode->Expand(false);    // don't recurse!
	BinTree->AlphaSort();       // this does recurse.

	// Restore the selected node; this has the somewhat unintended
   // consequence of refreshing the clip list also.  For now, I'll
   // leave this since I don't see any reason not to refresh the clip
   // list also, but we may want to break out the clip list refresh at
   // some point.
   if (DoesDirectoryExist(selectedPath))
   {
      SetSelectedBin(selectedPath);
   }
   else
   {
      SetSelectedBin(rootPath);
   }
}

// ---------------------------------------------------------------------------
// METHOD:      RefreshClipList
//
// DESCRIPTION: Forces the Clip List View to be redrawn using the clip
// list data
//
// ARGUMENTS:   StringList &selectListFromCaller
// Reference to the list of names of clips
// that should be set the the "selected"
// state; the list is modified by removing
// names as they are found, so on return the
// list will contain only the names that
// were NOT found.
//
// RETURNS:    The number of names in the select list that were not found
// ---------------------------------------------------------------------------

int TBinManagerForm::RefreshClipList(StringList *newSelectedList)
{
//	DBTIMER(RefreshClipList);

	CBusyCursor Busy(true);
   int retVal = GenerateClipListData(CurrentBinClipListData, lastClickedBinPath, newSelectedList);
   SortClipListData();
   SortSelectedClipVersionList();
   RepaintClipListView();
   RepaintVersionListView();

   return retVal;
}

// ---------------------------------------------------------------------------
// METHOD:      GenerateClipListData
//
// DESCRIPTION: Builds a list of metadata for all the clips in the bin
//
// ARGUMENTS:   ClipListDataType  &clipListData
// The list we are going to build
//
// const string &binPath
// The bin to which this clip list belongs
//
// StringList &selectListFromCaller
// Reference to the list of names of clips
// that should be set to the "selected"
// state; the list is modified by removing
// names as they are found, so on return the
// list will contain only the names that
// were NOT found.
//
// RETURNS:    The number of names in the select list that were not found
// ---------------------------------------------------------------------------

int TBinManagerForm::GenerateClipListData(ClipListDataType *clipListData, const string &binPath, StringList *newSelectedList)
{
//	DBCOMMENT("==========================================================");
//	DBTRACE(binPath);
//	DBTIMER(GenerateClipListData);

	// NOTE: we will empty out the newSelectedList as we find the clips --
   // we will return the number of selected clips that were not found,
   // and the names of the not-found clips will remain in the list
   int numberOfSelectedClipsLeftToFind = 0;
   if (newSelectedList != nullptr)
   {
      numberOfSelectedClipsLeftToFind = newSelectedList->size();
   }

   // Short circuit
   if (clipListData == nullptr)
   {
      return numberOfSelectedClipsLeftToFind;
   }

   // Clear out the old "owner data" clip list
   ClearClipList(clipListData);

   // Helper classes declared here - don't know what they have against
   // class methods!!
   CBinManager binMgr;
   CBinDir binDir;

   // Note whether or not this bin is the one with the hilited clip
   bool lookForTheHilitedClip = IsSameBinPath(binPath, HighlightedBinPath);

   // Get the first clip in the bin
   bool foundANewClipInTheBin = binDir.findFirst(binPath, BIN_SEARCH_FILTER_CLIP);

   // Construct an "Owner Data" list item for each clip in the bin
	while (foundANewClipInTheBin)
   {
		// Retrieve the clip info
      int status;
      CClipInitInfo *clipInitInfo;
		clipInitInfo = binMgr.openClipOrClipIni(binPath, binDir.fileName, &status);
      if (status == 0)
		{
			// Create and fill in the "owner data" list item
         bool success;
			SClipDataItem *clipDataItem = new SClipDataItem(clipInitInfo);

         if (lookForTheHilitedClip)
         {
            if (EqualIgnoreCase(binDir.fileName, HighlightedClipName))
            {
               clipDataItem->isHighlighted = true;

               // Optimization: there can only be one hilited clip!
               lookForTheHilitedClip = false;
            }
         }
         if (numberOfSelectedClipsLeftToFind > 0)
         {
            StringList::iterator iter;

            for (iter = newSelectedList->begin(); iter != newSelectedList->end(); ++iter)
            {
               if (EqualIgnoreCase((*iter), clipDataItem->clipName))
               {
                  clipDataItem->clipIsSelected = true;
                  newSelectedList->erase(iter);
                  --numberOfSelectedClipsLeftToFind;
                  break;
               }
            }
         }

         // Add the new list item to the list
         clipListData->push_back(clipDataItem);

         // Clean up
         clipInitInfo->closeClip();
         delete clipInitInfo;
      }
      else
      {
         TRACE_1(errout << "Project Manager failed to open clip " << binDir.fileName << " in Bin " << binPath << " Status = " << status);
      }

      // Go on to the next clip
      foundANewClipInTheBin = binDir.findNext();
   }

   return numberOfSelectedClipsLeftToFind;
}

// ---------------------------------------------------------------------------
// METHOD:      FindClipListIndexByClipName
//
// DESCRIPTION: Searches the current bin list for a clip by name and returns
// the index in the list of that clip
//
// ARGUMENTS:   clipname    The name of the clip to find
//
// RETURNS:     The list index, or -1 if there is no clip by that name
// ---------------------------------------------------------------------------

int TBinManagerForm::FindClipListIndexByClipName(const string &clipName)
{
   int retVal = -1; // Indicates "not found"
   const int totalClipCount = (int) CurrentBinClipListData->size();

   for (int i = 0; i < totalClipCount && retVal == -1; ++i)
   {
      SClipDataItem &clipDataItem = *CurrentBinClipListData->at(i);
      if (EqualIgnoreCase(clipDataItem.clipName, clipName))
      {
         retVal = i;
      }
   }

   return retVal;
}

// ---------------------------------------------------------------------------
// METHOD:      RefreshOneClipListItemByListIndex
//
// DESCRIPTION: Regenerates the data for one item in the clip view list and
// updates the view of that item. Preserves the "selected"
// and "highlighted" states for the item
//
// ARGUMENTS:   listIndex    The list index of the item to refresh
//
// RETURNS:     0 if succesful, else error code
// ---------------------------------------------------------------------------

int TBinManagerForm::RefreshOneClipListItemByListIndex(int listIndex)
{
   const int totalClipCount = CurrentBinClipListData->size();
   if (listIndex < 0 || listIndex >= totalClipCount)
   {
      return ERR_BIN_MANAGER_CLIP_NOT_FOUND;
   }

   // Paranoia
   MTIassert(ClipListView->Items->Count == totalClipCount);
   if (listIndex >= ClipListView->Items->Count || listIndex >= (int) CurrentBinClipListData->size())
   {
      return ERR_BIN_MANAGER_CLIP_NOT_FOUND;
   }

   int retVal;
   CBinManager binMgr;
   SClipDataItem &clipDataItem = *(CurrentBinClipListData->at(listIndex));
   CClipInitInfo *clipInitInfo = binMgr.openClipOrClipIni(lastClickedBinPath, clipDataItem.clipName, &retVal);
   if (retVal == 0)
   {
      bool clipIsSelected = clipDataItem.clipIsSelected;
      bool isHighlighted = clipDataItem.isHighlighted;

      clipDataItem.InitClipDataItem(*clipInitInfo);
      SortVersionListData(clipDataItem.versionList);

      clipDataItem.clipIsSelected = clipIsSelected;
      clipDataItem.isHighlighted = isHighlighted;
      clipInitInfo->closeClip();

      ClipListView->UpdateItems(listIndex, listIndex);
      ClipListView->Items->Item[listIndex]->Selected = clipDataItem.clipIsSelected;
   }

   // Refresh the proxy, if necessary
   string fullPath = AddDirSeparator(lastClickedBinPath) + clipDataItem.clipName;
   if (EqualIgnoreCase(fullPath, lastClickedClipPath))
   {
      //// ReloadProxy();// No - causes main thread to become unresponsive!
      RepaintVersionListView();
   }

   delete clipInitInfo;
   return retVal;
}

// ---------------------------------------------------------------------------
// METHOD:      RefreshOneClipListItemByName
//
// DESCRIPTION: Regenerates the data for one item in the clip view list and
// updates the view of that item. Preserves the "selected"
// and "highlighted" states for the item
//
// ARGUMENTS:   clipname    The clip name of the item to refresh
//
// RETURNS:     0 if succesful, else error code
// ---------------------------------------------------------------------------

int TBinManagerForm::RefreshOneClipListItemByName(const string &clipName)
{
   int listIndex = FindClipListIndexByClipName(clipName);

   return RefreshOneClipListItemByListIndex(listIndex);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::AddDummyBinTreeNodeChildIfNestedBins(string &binPath, TTreeNode * parentNode)
{
	CBinDir binDir;
	if (!binDir.hasAtLeastOneNestedBin(binPath))
	{
//		TRACE_0(errout << "No nested bins in " << binPath);
		return;
	}

//	TRACE_0(errout << "Add dummy node to bin " << binPath);
//	TRACE_0(errout << "because we found nested bin " << (binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN), binDir.fileName));
	TTreeNode *dummyNode = BinTree->Items->AddChild(parentNode, VirtualizationDummyNodeName);
	dummyNode->SelectedIndex = 1;    // No idea what this is for!
}
// ---------------------------------------------------------------------------

void TBinManagerForm::AddBinTreeNodeChildrenRecursively(string &binPath, TTreeNode * parentNode)
{
	CBinDir binDir;
	bool foundBin = binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN);

	while (foundBin)
	{
		// Add tree node
		TTreeNode *newNode = BinTree->Items->AddChild(parentNode, binDir.fileName.c_str());
		newNode->SelectedIndex = 1;

		// Now recurse for depth-first traversal of the Bin hierarchy
		string localPath = AddDirSeparator(binPath) + binDir.fileName;
		AddBinTreeNodeChildrenRecursively(localPath, newNode);

		// Go on to the next Bin at this level
		foundBin = binDir.findNext();
	}
}
// ---------------------------------------------------------------------------

void TBinManagerForm::AddOneLevelOfBinTreeNodeChildren(string &binPath, TTreeNode *parentNode, bool forceRefresh)
{
   ///////////////////////////////////////////////////////////////////////////
	// NOTE: If the node already has any children, we assume it has been
   // expanded previously filled in so we will do nothing, unless:
   // - forceRefresh is true, or
   // - the sole child is the virtualization dummy node, in which case
	//   the dummy node will be removed before filling in the real children.
   ///////////////////////////////////////////////////////////////////////////

	if (parentNode->Count > 0
   && (forceRefresh || parentNode->getFirstChild()->Text == VirtualizationDummyNodeName))
	{
		parentNode->DeleteChildren();
	}

	if (parentNode->Count > 0)
	{
		return;
	}

	CBinDir binDir;
	bool foundBin = binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN);
	while (foundBin)
	{
		// Add tree node
		TTreeNode *newNode = BinTree->Items->AddChild(parentNode, binDir.fileName.c_str());
		newNode->SelectedIndex = 1;    // ?

		// Now maybe add a dummy child if this bin has one or more nested bins.
		string localPath = AddDirSeparator(binPath) + binDir.fileName;

 //		TRACE_0(errout << "Adding real child node " << localPath);

		AddDummyBinTreeNodeChildIfNestedBins(localPath, newNode);

		// Go on to the next Bin at this level
		foundBin = binDir.findNext();
	}

	parentNode->AlphaSort();
}
// ---------------------------------------------------------------------------

string TBinManagerForm::getBinPathFromBinTreeNode(TTreeNode * node)
{
   // Sanity.
   if (node == nullptr)
   {
      return "";
   }

   // Build path recursively. Recursion terminates at the root, which has no parent.
   TTreeNode *parent = node->Parent;
   string parentPath = parent
                       ? AddDirSeparator(getBinPathFromBinTreeNode(node->Parent))
                       : "";

   return parentPath + StringToStdString(node->Text);
}
// ---------------------------------------------------------------------------

TTreeNode *TBinManagerForm::getBinTreeNodeFromBinPath(const string& binPathString)
{
   // We assert that there is only one root node!
//   MTIassert(BinTree->Items->Count == 1);
//   DBTRACE(BinTree->Items->Count);

   if (BinTree->Items->Count == 0)
   {
      // Sanity - no root!
      return nullptr;
   }

	STDFILESYSTEM::path binPath(RemoveDirSeparator(binPathString));
	auto binPathIter = binPath.begin();

	// Start at the root node. We assume there is only one.
	TTreeNode *rootNode = BinTree->Items->Item[0];
   string currentNodePath = RemoveDirSeparator(StringToStdString(rootNode->Text));
   STDFILESYSTEM::path rootPath(currentNodePath);

   // Make sure binPath includes the root path. This also moves the
   // itereator past the root path part of the bin path.
   for (auto& pathPart : rootPath)
   {
//      DBTRACE2(pathPart, *binPathIter);
//      DBTRACE2(pathPart.string(), binPathIter->string());
      if (pathPart != *binPathIter++)
      {
         // Bad bin path.
//         DBTRACE2("Bad bin path 1", binPathString);
         return nullptr;
      }
   }

	TTreeNode *currentNode = rootNode;

   // Now descend the bin path branch to find the desired node.
	while (binPathIter != binPath.end())
	{
      string binPathComponent = (*binPathIter++).string();
//      DBTRACE(binPathComponent);

      // Note: if the node has children and hasn't been de-virtualized, the
      // child count here will be 1 (the dummy node) no matter how many
      // children the node really has.
		if (currentNode->Count == 0)
		{
         // Node has no children - bin path is bad.
         DBTRACE2("Bad bin path 2", binPathString);
         return nullptr;
		}

      // De-virtualize the children if necessary.
		if (currentNode->Item[0]->Text == VirtualizationDummyNodeName)
		{
         AddOneLevelOfBinTreeNodeChildren(currentNodePath, currentNode);
		}

      // Scan the current node's children to find a match with the
      // corresponding bin path component.
      TTreeNode *childNode = currentNode->getFirstChild();
      while (childNode)
      {
         if (StringToStdString(childNode->Text) == binPathComponent)
         {
            // Found it!
            break;
         }

         childNode = currentNode->GetNextChild(childNode);
      }

      if (childNode == nullptr)
      {
         // Matching child not found => bad bin path.
//         DBTRACE2("Bad bin path 3", binPathString);
         return nullptr;
      }

      // Down to the next level!
      currentNode = childNode;
      currentNodePath = AddDirSeparator(currentNodePath) + binPathComponent;
   }

   // Success!
   return currentNode;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBRefreshMenuItemClick(TObject *Sender)
{
   CBusyCursor Busy(true);

   // Preserve selections
   StringList selectedClips;
   GetSelectedClips(selectedClips);

   // Regenerate and sort the data for the currently displayed bin
   GenerateClipListData(CurrentBinClipListData, lastClickedBinPath, &selectedClips);
   SortClipListData();

   // Larry

   // Refresh the bin tree view, which will also repaint the clip list view
   RefreshBinTree();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ClearClipList(ClipListDataType *clipListData)
{
   if (clipListData == nullptr)
   {
      return;
   }

   // Clear out the old "owner data" clip list
   // Warning: needs to be followed at some point by a repaint to fix the view
   for (int i = 0; i < (int) clipListData->size(); ++i)
   {
      delete clipListData->at(i);
   }
   clipListData->clear();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ClearBinClipListCache()
{
   BinClipListCacheType::iterator iter;
   for (iter = BinClipListCache.begin(); iter != BinClipListCache.end(); ++iter)
   {
      ClearClipList(iter->second);
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ClearAllSelections()
{
   int totalClipCount = (int) CurrentBinClipListData->size();

   // Blow away all selections
   for (int i = 0; i < totalClipCount; ++i)
   {
      CurrentBinClipListData->at(i)->clipIsSelected = false;
   }
   ClipListView->ClearSelection(); // Clears 'em all
   selectedClipCount = 0;
   selectedListIsInvalid = false;
}
// ---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////
/////////////////////// BIN TREE EVENTS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeChanging(TObject *Sender, TTreeNode *Node, bool &AllowChange)
{
	if (ComponentState.Contains(csDestroying))
	{
		// short-circuit if the form is being destroyed; bad things happen
		// when we try to access members in here because the destructor has
		// already been called when this event is fired.
		return;
	}

	if (ScanListSaveButton->Enabled)
	{
		// In "Edit" Mode, so save possible changes
		ScanListSaveButtonClick(nullptr);
	}

	return;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeChange(TObject *Sender, TTreeNode *Node)
{
   if (Node == nullptr)
   {
      return; // Avoid checks for bad nodes
   }

   int i;

	// Create a complete path from the selected Bin Tree node
	string binPath = getBinPathFromBinTreeNode(Node);
//	DBTRACE2(StringToStdString(Node->Text), binPath);

   // If we are changing to a new bin, clear the selection flags in the
   // old clip list - Note that the two or'ed conditions are supposed to be
   // equivalent, but we are paranoid
   bool weAreChangingToANewBin =
         (!IsSameBinPath(binPath, lastClickedBinPath) || (CurrentBinClipListData != BinClipListCache[lastClickedBinPath]));
   if (CurrentBinClipListData != nullptr)
   {
      if (weAreChangingToANewBin)
      {
         CancelHighlightClip(); // Poorly named - also clears all selections
      }
      else
      {
         SynchronizeSelectionsIfNecessary();
      }
   }

   // Show the bin path in the window title
   Caption = AnsiString("Project Manager - ") + binPath.c_str();

   // Remember the complete Bin Path
   lastClickedBinPath = binPath;

   // If we're changing bin, find the new bin's cached clip list
   // or create a new one
   if (weAreChangingToANewBin)
   {
      BinClipListCacheType::iterator iter;
      iter = BinClipListCache.find(lastClickedBinPath);
      if (iter == BinClipListCache.end())
      {
         // not found in cache - create new cache entry
         CBusyCursor Busy(true);
         CurrentBinClipListData = new ClipListDataType;
         BinClipListCache[lastClickedBinPath] = CurrentBinClipListData;
         GenerateClipListData(CurrentBinClipListData, lastClickedBinPath);
      }
      else
      {
         // found in cache
         CurrentBinClipListData = BinClipListCache[lastClickedBinPath];
      }

      // Pretend the highlighted clip was clicked on, for initial proxy and
      // version list
      if (IsSameBinPath(binPath, HighlightedBinPath))
      {
         CBinManager binManager;
         lastClickedClipPath = AddDirSeparator(binPath) + HighlightedClipName;
         lastClickedVersionPath = YellowHilitedVersion.IsValid() ? binManager.makeVersionClipPath(lastClickedClipPath,
               YellowHilitedVersion) : "";
      }
      else
      {
         lastClickedClipPath = "";
         lastClickedVersionPath = "";
      }
   }

   // Sort and repaint the view
   SortClipListData();
   RepaintClipListView();
	SortSelectedClipVersionList();
   RepaintVersionListView();
   if (!lastClickedVersionPath.empty())
   {
      HighlightClip(lastClickedVersionPath);
   }
   else if (!lastClickedClipPath.empty())
   {
      HighlightClip(lastClickedClipPath);
   }

   // Other stuff to take care of
//   TTreeNode *rootNode = BinTree->Items->Item[0];       ??
   ClearProxy();

   // Set the default clip name for this Bin
   SetDefaultClipName();

   // prevent update of video store usage until we're done setting data

   // Set the default schemes for this Bin
   SetDefaultMediaSchemes();

   // Trigger the handlers for clip scheme control and media location
   // control changes
   ClipSchemePopupBoxChange(0);
   MediaLocationComboBoxChange(0);

}
//---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeExpanding(TObject *Sender, TTreeNode *Node,
			 bool &AllowExpansion)
{
	AllowExpansion = true;
//	DBTRACE(StringToStdString(Node->Text));

	string binPath = getBinPathFromBinTreeNode(Node);
//	DBTRACE2(StringToStdString(Node->Text), binPath);
	AddOneLevelOfBinTreeNodeChildren(binPath, Node, false);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeClick(TObject *Sender)
{
	// if we left click on the bin list
	// we should clear the last right-click

	clearRightClick();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeDblClick(TObject *Sender)
{
	if (opMode == BM_MODE_SELECT_BIN && FormState.Contains(fsModal))
	{
		// This will end the modal box
		ModalResult = mrOk;
	}
}
// ---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------

int TBinManagerForm::AddClipToClipListData(ClipSharedPtr &clip, SClipDataItem **clipDataItemOut)
{
   if (clip == nullptr)
   {
      return 0;
   }

   // Create and initialize the clip info object
   CClipInitInfo clipInitInfo;
   int errorCode = clipInitInfo.initFromClip(clip);
   if (errorCode != 0)
   {
      return errorCode;
   }

   // Create and fill in the "owner data" list item
   SClipDataItem *clipDataItem = new SClipDataItem(&clipInitInfo);

   // New clips are always selected
   clipDataItem->clipIsSelected = true;

   // Add the new list item to the list
   CurrentBinClipListData->push_back(clipDataItem);

   // Send back the item pointer if the caller wants it
   if (clipDataItemOut != nullptr)
   {
      * clipDataItemOut = clipDataItem;
   }

   return 0;
}
// ---------------------------------------------------------------------------

int TBinManagerForm::AddClipToClipListView(ClipSharedPtr &clip)
{
   int retVal;

   // We want to end up with only the added clip selected
   ClearAllSelections();

   // Add, then select the new item
   SClipDataItem *clipDataItem;
   retVal = AddClipToClipListData(clip, &clipDataItem);
   if (retVal == 0 && clipDataItem != nullptr)
   {
      clipDataItem->clipIsSelected = true;
      lastClickedClipPath = AddDirSeparator(lastClickedBinPath) + clipDataItem->clipName;
   }

   // Sort the list and repaint the view
   SortClipListData();
   RepaintClipListView();
   // SortSelectedClipVersionList();   // Nothing to sort - it can't have any versions
   RepaintVersionListView(); // clears it

   // Show proxy for new clip
   ReloadProxy();

   return retVal;
}
// ---------------------------------------------------------------------------

int TBinManagerForm::AddClipsToClipListView(ClipListType *clipList)
{
   if (clipList == nullptr)
   {
      return 0;
   }

   int retVal = 0;
   CBusyCursor Busy(true);

   // We want to end up with only the added clips selected
   ClearAllSelections();

   for (int i = 0; i < (int) clipList->size(); ++i)
   {
      // Add, then select the new item
      SClipDataItem *clipDataItem;
      retVal = AddClipToClipListData(clipList->at(i), &clipDataItem);
      if (retVal == 0 && clipDataItem != nullptr)
      {
         clipDataItem->clipIsSelected = true;
      }
   }

   // Sort the list and repaint the view
   SortClipListData();
   RepaintClipListView();

   ClearProxy();

   return retVal;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::Timecode2AnsiString(const CTimecode &timecode, AnsiString &stringOut)
{
   string timeStr = "Unknown";
   if (timecode != CTimecode::NOT_SET)
   {
      timecode.getTimeString(timeStr);
   }
   stringOut = timeStr.c_str();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::PopulateClipViewItem(TListItem &listItem, const SClipDataItem &clipDataItem)
{
   AnsiString tmpStr;

   // Set properties of the List Item
   listItem.Caption = clipDataItem.clipName.c_str();
   listItem.Data = nullptr;

   // Enter text strings for Clip information columns, left to right

   // Description
   listItem.SubItems->Add(clipDataItem.clipDescription.c_str());

   // Clip Status
   tmpStr = CTrack::queryMediaStatusName(clipDataItem.mediaStatus).c_str();
   const int versionCount = (int) clipDataItem.versionList.size();
   if (versionCount > 0)
   {
      tmpStr = "Versioned";
   }
   else if (tmpStr == "Cloned")
   {
      // This is a bit awkward... let's see if anyone complains:
      tmpStr = "Unversioned";
   }

   listItem.SubItems->Add(tmpStr);

   // Image Format
   listItem.SubItems->Add(clipDataItem.imageFormat.c_str());

   // In, Out, Duration
   Timecode2AnsiString(clipDataItem.inTimecode, tmpStr);
   listItem.SubItems->Add(tmpStr);
   Timecode2AnsiString(clipDataItem.outTimecode, tmpStr);
   listItem.SubItems->Add(tmpStr);
   Timecode2AnsiString(clipDataItem.clipDuration, tmpStr);
   listItem.SubItems->Add(tmpStr);

   // // Media Durations
   // Timecode2AnsiString(clipDataItem.videoMediaDuration, tmpStr);
   // listItem.SubItems->Add(tmpStr);
   // Timecode2AnsiString(clipDataItem.audioMediaDuration, tmpStr);
   // listItem.SubItems->Add(tmpStr);

   // Media Location
   listItem.SubItems->Add(clipDataItem.mediaLocation.c_str());

   // Creation Date & Time
   listItem.SubItems->Add(clipDataItem.createDate.c_str());
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SortClipListData()
{

   // Short circuit no-op sort
   if (CurrentBinClipListData->size() < 2)
   {
      return;
   }

   ClipListDataType::iterator first = CurrentBinClipListData->begin();
   ClipListDataType::iterator last = CurrentBinClipListData->end();
   switch (ClipListColumnToSort)
   {
   default:
   case COL_INDEX_CLIP_NAME:
      stable_sort(first, last, SClipDataItem::NameAscendingOrder());
      break;
   case COL_INDEX_CLIP_DESCRIPTION:
      stable_sort(first, last, SClipDataItem::DescriptionAscendingOrder());
      break;
   case COL_INDEX_CLIP_STATUS:
      stable_sort(first, last, SClipDataItem::StatusAscendingOrder());
      break;
   case COL_INDEX_IMAGE_TYPE:
      stable_sort(first, last, SClipDataItem::ImageTypeAscendingOrder());
      break;
   case COL_INDEX_IN_TIMECODE:
      stable_sort(first, last, SClipDataItem::InTimecodeAscendingOrder());
      break;
   case COL_INDEX_OUT_TIMECODE:
      stable_sort(first, last, SClipDataItem::OutTimecodeAscendingOrder());
      break;
   case COL_INDEX_DURATION:
      stable_sort(first, last, SClipDataItem::DurationAscendingOrder());
      break;
   case COL_INDEX_MEDIA_LOCATION:
      stable_sort(first, last, SClipDataItem::MediaLocationAscendingOrder());
      break;
   case COL_INDEX_CREATE_DATE:
      stable_sort(first, last, SClipDataItem::CreateDateAscendingOrder());
      break;
   }
}

// ---------------------------------------------------------------------------
// RepaintClipListView
//
// Description: Forces the Clip List View to be redrawn using the clip
// list data
//
// Arguments:   NONE
//
// Returns:     NOTHING
// ---------------------------------------------------------------------------

void TBinManagerForm::RepaintClipListView()
{
   const int totalClipCount = (int) CurrentBinClipListData->size();

   if (totalClipCount < 1)
   {
      ClipListView->Items->Count = 0;
      ClipListView->Repaint();
   }
   else
   {
      // Try to update with minimal flashing
      if (ClipListView->Items->Count <= totalClipCount)
      {
         ClipListView->UpdateItems(0, ClipListView->Items->Count - 1);
         ClipListView->Items->Count = totalClipCount; // will paint the rest
      }
      else
      {
         ClipListView->Items->Count = totalClipCount;
         ClipListView->UpdateItems(0, totalClipCount - 1);
      }

      // Synchronize selections in the view from the data
      int selectedIndex = ClipListView->TopItem->Index;
      for (int i = 0; i < totalClipCount; ++i)
      {
         bool isSelected = CurrentBinClipListData->at(i)->clipIsSelected;
         SetClipListViewItemSelectedState(i, isSelected);
         if (isSelected)
         {
            selectedIndex = i;
         }
      }

      // Make sure the selected item is visible (false means no partial view)
      ClipListView->Items->Item[selectedIndex]->MakeVisible(false);
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SortSelectedClipVersionList()
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }
   SortVersionListData(CurrentBinClipListData->at(clipIndex)->versionList);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SortVersionListData(vector<SVersionDataItem *> &versionList)
{
   // Short circuit no-op sort
   if (versionList.size() < 2)
   {
      return;
   }

   VersionListDataType::iterator first = versionList.begin();
   VersionListDataType::iterator last = versionList.end();
   switch (VersionListColumnToSort)
   {
   default:
   case COL_INDEX_VERSION_NAME:
      stable_sort(first, last, SVersionDataItem::NameAscendingOrder());
      break;
   case COL_INDEX_VERSION_STATUS:
      stable_sort(first, last, SVersionDataItem::StatusAscendingOrder());
      break;
   case COL_INDEX_VERSION_MODIFIED:
      stable_sort(first, last, SVersionDataItem::VideoMediaModifiedAscendingOrder());
      break;
   case COL_INDEX_VERSION_COMMENTS:
      stable_sort(first, last, SVersionDataItem::CommentsAscendingOrder());
      break;
   case COL_INDEX_VERSION_CREATE_DATE:
      stable_sort(first, last, SVersionDataItem::CreateDateAscendingOrder());
      break;
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::RepaintVersionListView()
{
   // If there is no 'lastClickedClipPath, then we're not in the bin
   // with the highlighted clip, so we need to clear the version list pane
   if (lastClickedClipPath.empty())
   {
      VersionListView->Items->Count = 0;
      VersionListView->Repaint(); // It doesn't repaint itself?!
      return;
   }

   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }
   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   const int versionCount = (int) versionList.size();

   // Clear selection... will be reset back on if necessary later
   for (int i = 0; i < versionCount; ++i)
   {
      versionList[i]->versionIsSelected = false;
   }

   // Clear onscreen if presently being displayed
   for (int i = 0; i < VersionListView->Items->Count; ++i)
   {
      VersionListView->Items->Item[i]->Selected = false;
   }

   if (versionCount == 0)
   {
      VersionListView->Items->Count = 0;
      VersionListView->Repaint();
   }
   else
   {
      // Try to update with minimal flashing
      if (VersionListView->Items->Count <= versionCount)
      {
         VersionListView->UpdateItems(0, VersionListView->Items->Count - 1);
         VersionListView->Items->Count = versionCount; // will paint the rest
      }
      else
      {
         VersionListView->Items->Count = versionCount;
         VersionListView->UpdateItems(0, versionCount - 1);
      }

      // Synchronize selections in the view from the data
      int selectedIndex = VersionListView->TopItem->Index;
      for (int i = 0; i < versionCount; ++i)
      {
         CBinManager binManager;
         // string versionPath = binManager.makeClipPath(lastClickedClipPath, versionList[i]->versionName);
         ClipVersionNumber versionNumber(versionList[i]->versionName);
         ClipIdentifier clipId(lastClickedClipPath, versionNumber);
         string versionPath = clipId.GetClipPath();
         if (versionPath == lastClickedVersionPath)
         {
            SetVersionListViewItemSelectedState(&versionList, i, true);
            selectedIndex = i;
            break;
         }
      }

      // Make sure the selected item is visible (false means no partial view)
      VersionListView->Items->Item[selectedIndex]->MakeVisible(false);
   }

}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileMenuCreateJobItemClick(TObject *Sender)
{
   JobInfo jobInfo;
   if (!EditJobInfo(jobInfo, "Create Project"))
   {
      return;
   }

   CreateJob(jobInfo);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileMenuCreateBinItemClick(TObject *Sender)
{
   // Sanity - this should never happen.
   if (BinTree->Selected == 0 && rightClickedNode == nullptr)
   {
      _MTIErrorDialog(Handle, "Please select the parent bin before creating a new one.");
      return;
   }

   // Get the new bin's name.
   TNewBinDialog *newBinDialog = new TNewBinDialog(BinManagerForm);
   newBinDialog->Caption = "Create Bin";
   int result = ShowModalDialog(newBinDialog);
   string newBinName = StringToStdString(newBinDialog->BinNameEdit->Text);
   delete newBinDialog;
   newBinDialog = nullptr;
   if (result != mrOk)
   {
      return;
   }

   // Locate the parent bin.
   TTreeNode *parentNode = rightClickedNode ? rightClickedNode : BinTree->Selected;
   string parentPath = getBinPathFromBinTreeNode(parentNode);
   clearRightClick();

   // Create the new bin.
   CBinManager binMgr;
   int retVal = binMgr.createBin(parentPath, newBinName);
   if (retVal == CLIP_ERROR_CREATE_DIRECTORY_FAILED)
   {
      _MTIErrorDialog(Handle, theError.getMessage());
      return;
   }

   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::NewBinClick: call to" << endl
                     << "       CBinManager::createBin failed, return code " << retVal);

      MTIostringstream ostr;
      ostr << "Bin creation failed, error code = " << retVal;
      _MTIErrorDialog(Handle, ostr.str());

      return;
   }

   // If the parent bin has a child which is the virtualization dummy node,
   // we don't need to do anything else because the new bin will magically
   // appear when we expand the parent node, below.
   if (parentNode->HasChildren == false
   || parentNode->Item[0]->Text != VirtualizationDummyNodeName)
   {
      // Either the parent node has no children yet, or the children have
      // already been de-virtualized, so we have to add the bin tree node
      // for the newly created bin ourself.
      TTreeNode *newNode = BinTree->Items->AddChild(parentNode, newBinName.c_str());
      parentNode->AlphaSort();
      newNode->ImageIndex = 1;        // ??
      newNode->SelectedIndex = 1;     // ??
   }

   // Make sure the new bin is showing!
   parentNode->Expand(false);  // dont't recurse!
}
// ---------------------------------------------------------------------------

void TBinManagerForm::CreateJob(JobInfo jobInfo)
{
   if (!jobInfo.ValidateAll())
   {
      TRACE_0(errout << "ERROR: Bad job name in CreateJob()");
      return;
   }

   string jobFolderName = jobInfo.GetJobRootFolderName();
   TTreeNode *rootNode = BinTree->Items->Item[0];
   string path = getBinPathFromBinTreeNode(rootNode);

   // Make the folder hierarchy
   JobManager jobManager;
   int retVal = jobManager.CreateJobFolders(path, jobFolderName, jobInfo);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::CreateJob: call to" << endl << "       JobManager::CreateJobFolders failed,    " << endl <<
            "       return code = " << retVal);
      MTIostringstream ostr;
      ostr << "Project creation failed, error code = " << retVal;
      _MTIErrorDialog(ostr.str());
      return;
   }

   // Add Bin Tree node
   TTreeNode *newNode = BinTree->Items->AddChild(rootNode, jobFolderName.c_str());
   newNode->ImageIndex = 1;
   newNode->SelectedIndex = 1;

	// Expand node so we can see the new job
	rootNode->Expand(false);   // don't recurse!
   BinTree->AlphaSort();
   RefreshBinTree();
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::EditJobInfo(JobInfo &jobInfo, const string &caption)
{
   if (JobPropertiesForm == nullptr)
   {
      JobPropertiesForm = new TJobPropertiesForm(this);
   }

   JobPropertiesForm->Caption = caption.c_str();
   JobPropertiesForm->SetJobInfo(jobInfo);
   int result = ShowModalDialog(JobPropertiesForm);
   if (result != mrOk)
   {
      return false;
   }

   jobInfo = JobPropertiesForm->GetJobInfo();
   return true;
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::MakeNewClip(void)
{
   // mlm: to-do: protect with shared environment callbacks... ignore
   // the problem for now since this is never called in Control
   // Dailies

   // Create a new clip based on user's parameters entered in the
   // Clip Create Panel

   int retVal;
   MTIostringstream ostr;
   bool rflCorrupted = false;

   // Set up the intended parameters for the new clip in the
   // Clip Init Info structure
   CClipInitInfo clipInitInfo;

   // safety check - should have already been screened out by now
   if (clipSchemeName.empty())
   {
      _MTIErrorDialog(Handle, "Please select a valid combination of    " "Video Format, Audio Format and Storage Format    ");
      return false;
   }

   // Initialize the clipInitInfo from the user's chosen Clip Scheme.
   // member variable now set in popup box change method
   // string newClipSchemeName = ClipSchemePopupBox->Text.c_str();
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::CreateClipButtonClick: call to" << endl <<
            "       CClipInitInfo::initFromClipScheme failed, return code " << retVal);
      ostr << "Error Creating New Clip.  Error Code = " << retVal;
      ostr << endl << "Unknown Clip Scheme";
      _MTIErrorDialog(Handle, ostr.str());
      return false;
   }

   // Set the Bin Path from the Bin selected in the Bin Tree
   clipInitInfo.setBinPath(lastClickedBinPath);

   // Set the Clip Name from user's Clip Name entry field
   string newClipName;
   if (NewClipNewMediaPanel->Visible)
   {
      newClipName = StringToStdString(VideoClipNameEdit->Text);
   }
   else
   { // WTF?? How can this be? QQQ
      newClipName = StringToStdString(FileClipNameEdit->Text);
   }
   clipInitInfo.setClipName(newClipName);

   // Set the In and Out Timecode
   clipInitInfo.setInTimecode(VTimeCodeEditIn->tcP);
   clipInitInfo.setOutTimecode(VTimeCodeEditOut->tcP);

   // Check for video clip in image files
   CClipSchemeManager clipSchemeMgr;
   EMediaType videoMediaType = clipSchemeMgr.getClipSchemeMainVideoMediaType(clipSchemeName);

   // Matt made me take this out (directly settable folder/file)
#if 0
   if (CMediaInterface::isMediaTypeImageFile(videoMediaType))
   {
      MTIistringstream istr;
      string videoFileNameTemplate = MakeClipFileName(VideoFileFolderEdit, VideoFileNameEdit);
      int videoInFrameNumber;
      istr.str(VideoInFrameEdit->Text.c_str());
      istr >> videoInFrameNumber;

      int videoFrameCount = VTimeCodeEditOut->tcP - VTimeCodeEditIn->tcP;

      retVal = clipInitInfo.initMediaLocation(videoFileNameTemplate, videoInFrameNumber, videoFrameCount, true, clipSchemeName);
      if (retVal == CLIP_ERROR_IMAGE_FILE_MISSING)
      {
         // First image file is not available, so check the others
         // If none exist, then assume the user wants the Image Files
         // to be created.  If some exist, then treat this as an error

         // Verify that none of the files exist
         retVal = clipInitInfo.checkNoImageFiles2(videoFileNameTemplate, videoInFrameNumber, videoInFrameNumber + videoFrameCount);
         if (retVal != 0)
         {
            ostr << "ERROR: Some Image Files are missing, some already exist" << endl << "       Cannot determine user's intention" << endl;
            _MTIErrorDialog(Handle, ostr.str());
            return false;
         }
      }
      else if (retVal == CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME || retVal == CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER)
      {
         // the image files and clip scheme are not compatible
         // or an image file was inconsistent with the first file's header
         _MTIErrorDialog(Handle, theError.getMessage());
         return false;
      }
      else if (retVal != 0)
      {
         ostr << "ERROR: Cannot initialize from Image File " << videoFileNameTemplate << endl << "       Return Code: " << retVal << endl;

         _MTIErrorDialog(Handle, ostr.str());
         return false;
      }

      // Set the Bin Path from the Bin selected in the Bin Tree
      clipInitInfo.setBinPath(lastClickedBinPath);
   }
   else
#endif // 0
   {
      // Get the user's chosen Disk Scheme
      string diskSchemeName = StringToStdString(MediaLocationComboBox->Text);

      // Set the audio and video media locations from the Disk Scheme
      retVal = clipInitInfo.initMediaLocation(diskSchemeName);
      if (retVal != 0)
      {
         ostr << "Error Creating New Clip " << endl << theError.getMessage() << endl << "Error code " << retVal;
         _MTIErrorDialog(Handle, ostr.str());
         return false;
      }

      // Check to see if the RFL file is corrupted before creating clip
      retVal = clipInitInfo.CheckRFL();
      if (retVal != 0)
      {
         rflCorrupted = true;
         ostr << "Your VideoStore may be corrupted" << endl <<
               "Please use the Check VideoStores wizard or contact MTI Film Support for help" << endl <<
               "You risk damaging existing clips if the new clip is created" << endl << endl << "  Do you wish to continue?";
         if (_MTIConfirmationDialog(Handle, ostr.str()) != MTI_DLG_OK)
         {
            return false;
         }
         ostr.str("");
      }
   }

   CBusyCursor Busy(true);

   // Create a new Clip based on the Clip Init Info
   auto clip = clipInitInfo.createClip(&retVal);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::CreateClipButtonClick: call to" << endl <<
            "       ClipInitInfo::createClip failed. Return = " << retVal);

      ReportCreateClipError(retVal);

      return false;
   }

   // Update the Clip List
   // clipInitInfo.initFromClip(clip); // QQQ Don't know if this is still needed
   AddClipToClipListView(clip);

   if (!CMediaInterface::isMediaTypeImageFile(videoMediaType))
   {
      // Check to see if the RFL file is corrupted after creating clip
      if (!rflCorrupted)
      {
         retVal = clipInitInfo.CheckRFL();
         if (retVal != 0)
         {
            ostr << "Creating the clip " << clipInitInfo.getClipName() << endl << "has corrupted the VideoStore" << endl;
            TRACE_0(errout << "ERROR: " << ostr.str());
            _MTIErrorDialog(Handle, ostr.str());
         }
      }
   }

   // Set new bin defaults
   CBinManager binMgr;
   binMgr.setAutoClipName(lastClickedBinPath, newClipName);
   SetDefaultClipName(); // increment the number in the clip name
   binMgr.setAutoClipScheme(lastClickedBinPath, clipSchemeName);
   binMgr.setAutoDiskScheme(lastClickedBinPath, StringToStdString(MediaLocationComboBox->Text));

   CBinManager binManager;
   binManager.closeClip(clip);
   return true;
}

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CreateClipButtonClick(TObject *Sender)
{
   // NOTE!!! Need to save the clip name first because it somehow gets
   // changed during the MakeNewClip().
   //
   string NewClipName = StringToStdString(VideoClipNameEdit->Text);
   string NewVideoFormat, NewAudioFormat, NewStorageFormat;

   NewVideoFormat = StringToStdString(NewMediaVideoFormatComboBox->Text);
   NewAudioFormat = StringToStdString(NewMediaAudioFormatComboBox->Text);
   if (LocationRepositoryRadioButton->Checked)
   {
      if (VideoStoreMediaStorageComboBox->Visible)
      {
         NewStorageFormat = StringToStdString(VideoStoreMediaStorageComboBox->Text);
      }
      else
      {
         NewStorageFormat = StringToStdString(FileRepositoryStorageFormatComboBox->Text);
      }
   }
   else
   {
      NewStorageFormat = StringToStdString(FileVidClipMediaStorageComboBox->Text);
   }

   CClipSchemeManager clipSchemeManager;
   CClipScheme* clipScheme = clipSchemeManager.findClipScheme(NewVideoFormat, NewAudioFormat, NewStorageFormat);
   if (clipScheme == 0)
   {
      MTIostringstream ostr;

      TRACE_0(errout << "ERROR: Clip Scheme triangulation failed for:" << endl << "       VideoFormat=" << NewVideoFormat << endl <<
            "       AudioFormat=" << NewAudioFormat << endl << "       StorageFormat=" << NewStorageFormat);
      ostr << "Cannot find a Clip Scheme that matches    " << endl << "       VideoFormat=" << NewVideoFormat << endl <<
            "       AudioFormat=" << NewAudioFormat << endl << "       StorageFormat=" << NewStorageFormat;

      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   clipSchemeName = clipScheme->getClipSchemeName();

   MakeNewClip();
}

// ---------------------------------------------------------------------------

void TBinManagerForm::ReportCreateClipError(int errorCode)
{
   MTIostringstream ostr;

   ostr << "Clip creation failed, error code = " << errorCode;
   switch (errorCode)
   {
   case CLIP_ERROR_INVALID_TIME_CODE:
   case CLIP_ERROR_BAD_HANDLE_TIMECODE:
      ostr << endl << "Please check the timecodes";
      break;
   case CLIP_ERROR_INVALID_CLIP_NAME:
      ostr << endl << "The name is invalid";
      break;
   case CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS:
      ostr << endl << " Clip name cannot contain any of the characters " << ". \\ / : * ? \" < > | #";
      break;
   case CLIP_ERROR_CLIP_ALREADY_EXISTS:
      ostr << endl << "A clip with that name already exists!";
      break;
   case CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE:
      ostr << endl << "Not enough disk space";
      break;
   case CLIP_ERROR_INSUFFICIENT_METADATA_STORAGE:
      ostr << endl << "Insufficient storage for metadata folder." << endl << "Please delete some bins and try again.";
      break;
   case CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED:
      ostr << endl << "Sorry, but you are not licensed for this resolution " << endl << "To upgrade, please contact your reseller or MTI.";
      break;
   case CLIP_ERROR_RFL_ENTRIES_OVERLAPPING:
   case CLIP_ERROR_RFL_ENTRIES_OUT_OF_ORDER:
      ostr << endl << "RFL file may be corrupted";
      break;
   case CLIP_ERROR_RAW_DISK_OPEN_FAILED:
      ostr << endl << "Unable to open the VideoStore file";
      break;
   case FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE:
      ostr << endl << "Media file may be missing, corrupted or not a supported file type";
      break;
   }

   _MTIErrorDialog(Handle, ostr.str());
}

// ---------------------------------------------------------------------------

void TBinManagerForm::SetClipSchemeName(const string& newClipSchemeName)
{
   // short-circuit on no-op
   if (newClipSchemeName == clipSchemeName)
   {
      return;
   }

   clipSchemeName = newClipSchemeName;

   if (!clipSchemeName.empty())
   {
      // Validate new clip scheme. Note: leaves bad clip scheme set on error
      CClipSchemeManager clipSchemeMgr;
      EClipSchemeType clipSchemeType = clipSchemeMgr.getClipSchemeType(newClipSchemeName);
      if (clipSchemeType == CLIP_SCHEME_TYPE_INVALID)
      {
         // make sure it's not a Control Dailies clip panel type
         ChangeClipPanelType(clipPanelTypeVideo);

         MTIostringstream ostr;

         static char const *func = __func__;

         TRACE_0(errout << "ERROR: " << func << ": call to CClipSchemeManager::getClipSchemeType failed");
         ostr << "Error finding Clip info.  Unknown Clip Scheme: " << clipSchemeName;
         TRACE_0(errout << ostr.str());
         _MTIErrorDialog(Handle, ostr.str());
         return;
      }
      else if (clipSchemeType == CLIP_SCHEME_TYPE_IMAGE_FILE)
      {
         InOutFileClipEditExit(this);
         ChangeClipPanelType(clipPanelTypeFile);
      }
      else
      {
         // Non-file
         UpdateTimecodesWithNewClipScheme(newClipSchemeName);
         ChangeClipPanelType(clipPanelTypeVideo);
      }
   }

}

// ---------------------------------------------------------------------------

void TBinManagerForm::InitCreateClipPanel()
{
   InitClipSchemeComboBox(); // Used only for Control Dailies now
   InitCreateClipComboBoxes();
   CreateClipSelectionRadioButtonClick(this);
   ClipFormatComponentChanged(this);

   InitMediaLocationComboBox();
}

// ---------------------------------------------------------------------------

void TBinManagerForm::InitMediaLocationComboBox()
{
   // Build entries for Media Location Combo Box
   int retVal;
   MediaLocationComboBox->Items->Clear();

   // DISK SCHEMES NO LONGER USED
   //
   // CDiskSchemeList diskSchemeList;
   // retVal = diskSchemeList.ReadDiskSchemeFile();
   // if (retVal != 0)
   // {
   // _MTIErrorDialog(Handle, "Could not open Disk Scheme File");
   // return;
   // }
   //
   // // Add a combo box item to the Media Location combo box for each
   // // active disk scheme
   // StringList activeDiskSchemeNames;
   // activeDiskSchemeNames = diskSchemeList.GetActiveDiskSchemeNames();
   // for (unsigned int i = 0; i < activeDiskSchemeNames.size(); ++i)
   // {
   // MediaLocationComboBox->Items->Append(activeDiskSchemeNames[i].c_str());
   // }
   //
   // MediaLocationComboBox->ItemIndex = 0;
   //
   // MediaLocationComboBoxChange(this);
}

// ---------------------------------------------------------------------------

bool TBinManagerForm::IsMediaLocationValid(string const &mediaLocation)
{
   // DISK SCHEMES NO LONGER USED
   // CDiskSchemeList diskSchemeList;
   // int retVal = diskSchemeList.ReadDiskSchemeFile();
   // if (retVal != 0)
   // {
   // _MTIErrorDialog(Handle, "Could not open Disk Scheme File");
   // return false;
   // }
   //
   // return !diskSchemeList.Find(mediaLocation).name.empty();
   return false;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::InitClipSchemeComboBox()
{
   // Create submenus for each of the major Clip Scheme types
   // containing the active Clip Schemes
   MakeClipSchemeSubmenu(SDMenuItem, CLIP_SCHEME_FILTER_SD);
   MakeClipSchemeSubmenu(HDMenuItem, CLIP_SCHEME_FILTER_HD);
   MakeClipSchemeSubmenu(ImageFileMenuItem, CLIP_SCHEME_FILTER_IMAGE_FILE);
   MakeClipSchemeSubmenu(HSDLMenuItem, CLIP_SCHEME_FILTER_HSDL);

   // Find the first available Clip Scheme and set the clip scheme name
   if (clipSchemeName.empty())
   {
      ClipSchemePopupBox->Text = GetFirstAvailableClipScheme().c_str();
      // This control no longer holds the current clip scheme:
      // SetClipSchemeName(ClipSchemePopupBox->Text.c_str());
   }
   else
   {
      ClipSchemePopupBox->Text = clipSchemeName.c_str();
   }

   // Adding menu items requires a reload of the drop down box
   ClipSchemePopupBox->DropDownMenu = ClipSchemePopupMenu;
}
// ---------------------------------------------------------------------------

string TBinManagerForm::GetFirstAvailableClipScheme()
{
   // Find first available Clip Scheme by searching through
   // Clip Scheme combo box submenus until there is one with a Clip Scheme name

   string clipSchemeName; // hides memeber variable of the same name

   // Find the first available Clip Scheme
   if (SDMenuItem->Count > 0)
   {
      clipSchemeName = StringToStdString(SDMenuItem->Items[0]->Caption);
   }
   else if (HDMenuItem->Count > 0)
   {
      clipSchemeName = StringToStdString(HDMenuItem->Items[0]->Caption);
   }
   else if (ImageFileMenuItem->Count > 0)
   {
      clipSchemeName = StringToStdString(ImageFileMenuItem->Items[0]->Caption);
   }
   else if (HSDLMenuItem->Count > 0)
   {
      clipSchemeName = StringToStdString(HSDLMenuItem->Items[0]->Caption);
   }
   else
   {
      clipSchemeName = MissingClipScheme;
   }

   return clipSchemeName;
}
// ---------------------------------------------------------------------------

string TBinManagerForm::GetFirstAvailableDiskScheme()
{
   string diskSchemeName;
   bool empty = MediaLocationComboBox->Items->Strings[0] != "";
   diskSchemeName = empty ? StringToStdString(MediaLocationComboBox->Items->Strings[0]) : string(MissingDiskScheme);

   return diskSchemeName;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::MakeClipSchemeSubmenu(TMenuItem *menuItem, unsigned int filter)
{
   int retVal;

   // Remove any existing submenu items
   menuItem->Clear();

   // Search for active clip schemes of caller's type
   unsigned int searchFilter = CLIP_SCHEME_FILTER_ACTIVE | filter;
   CClipSchemeManager clipSchemeMgr;
   CClipSchemeManager::ClipSchemeList resultList;
   retVal = clipSchemeMgr.searchClipSchemes(searchFilter, resultList);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::MakeClipSchemeSubmenu: " << " ClipSchemeManager::searchClipSchemes " << endl <<
            "       failed with return code " << retVal);
      return;
   }

   // Iterate over search results, adding submenu item for each Clip Scheme
   for (unsigned int i = 0; i < resultList.size(); ++i)
   {
      AddClipSchemeSubmenuItem(menuItem, resultList[i]->getClipSchemeName(), filter);
   }

   // Disable (gray-out) submenus that have no clip schemes
   menuItem->Enabled = !(resultList.size() < 1);
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::IsClipSchemeValid(string const &clipScheme)
{
   CClipSchemeManager clipSchemeMgr;
   return clipSchemeMgr.findClipScheme(clipScheme) != 0;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::AddClipSchemeSubmenuItem(TMenuItem *menuItem, const string& caption, int tag)
{
   // Create a new TMenuItem to be the submenu item
   TMenuItem *newSubmenuItem = new TMenuItem(menuItem);

   // Set parameters
   newSubmenuItem->Caption = caption.c_str();
   newSubmenuItem->Tag = tag;
   // JAM   newSubmenuItem->OnClick = ClipSchemeNameClick;

   // Add the submenu item to the caller's menu item
   menuItem->Add(newSubmenuItem);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::InitCreateClipComboBoxes()
{
   CClipSchemeManager csMgr;
   string iniFileName = GetIniFileName();
   string iniFileSection = GetIniSection();

   StringList videoStoreVideoList = csMgr.getVideoFormatClassList(true);
   StringList videoStoreAudioList = csMgr.getAudioFormatClassList(true);
   StringList videoStoreStorageList = csMgr.getMediaStorageClassList(true);
   // StringList newFileVideoList = csMgr.getVideoFormatClassList(false);
   // StringList newFileAudioList = csMgr.getAudioFormatClassList(false);
   StringList newFileStorageList = csMgr.getMediaStorageClassList(false);
   unsigned int i;

   TStrings *menuItems = NewMediaVideoFormatComboBox->Items;
   menuItems->Clear();
   for (i = 0; i < videoStoreVideoList.size(); ++i)
   {
      menuItems->Add(AnsiString(videoStoreVideoList[i].c_str()));
   }

   menuItems = NewMediaAudioFormatComboBox->Items;
   menuItems->Clear();
   for (i = 0; i < videoStoreAudioList.size(); ++i)
   {
      menuItems->Add(AnsiString(videoStoreAudioList[i].c_str()));
   }

   menuItems = VideoStoreMediaStorageComboBox->Items;
   menuItems->Clear();
   for (i = 0; i < videoStoreStorageList.size(); ++i)
   {
      menuItems->Add(AnsiString(videoStoreStorageList[i].c_str()));
   }

   menuItems = FileRepositoryStorageFormatComboBox->Items;
   menuItems->Clear();
   for (i = 0; i < newFileStorageList.size(); ++i)
   {
      menuItems->Add(AnsiString(newFileStorageList[i].c_str()));
   }

   menuItems = FileVidClipMediaStorageComboBox->Items;
   menuItems->Clear();
   for (i = 0; i < newFileStorageList.size(); ++i)
   {
      menuItems->Add(AnsiString(newFileStorageList[i].c_str()));
   }

   // Rebuild folder selection history lists
   ComboBoxHistoryList videoFolderHistory(iniFileName, iniFileSection + ".VideoFolderHistory");
   videoFolderHistory.ReadProperties();
   videoFolderHistory.rebuildComboBoxList(FileVidClipVideoFolderComboBox);

   ComboBoxHistoryList audioFolderHistory(iniFileName, iniFileSection + ".AudioFolderHistory");
   audioFolderHistory.ReadProperties();
   audioFolderHistory.rebuildComboBoxList(FileVidClipAudioFolderComboBox);

   ClipFormatComponentChanged(this);

   // Search for active image file clip schemes
   unsigned int searchFilter = CLIP_SCHEME_FILTER_ACTIVE | CLIP_SCHEME_FILTER_IMAGE_FILE;
   CClipSchemeManager clipSchemeMgr;
   CClipSchemeManager::ClipSchemeList resultList;
   int retVal;

   ExistingMediaFormatComboBox->Clear();
   retVal = clipSchemeMgr.searchClipSchemes(searchFilter, resultList);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: TBinManagerForm::InitCreateClipComboBoxes: " << " ClipSchemeManager::searchClipSchemes " << endl <<
            "       failed with return code " << retVal);
      return;
   }

   // Iterate over search results, adding submenu item for each Clip Scheme
   ExistingMediaFormatComboBox->ItemIndex = -1;
   for (unsigned int i = 0; i < resultList.size(); ++i)
   {
      string schemeName = resultList[i]->getClipSchemeName();
      ExistingMediaFormatComboBox->Items->Add(schemeName.c_str());
      if (string::npos != schemeName.find("Auto")) // QQQ EVIL! EVIL!
      {
         ExistingMediaFormatComboBox->ItemIndex = i;
      }
   }
}

// ---------------------------------------------------------------------------

void TBinManagerForm::UpdateTimecodesWithNewClipScheme(const string& clipSchemeName)
{
   int retVal;

   // Get a prototypical timecode based on the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
   {
      // ERROR: Unknown Clip Scheme name
      return;
   }

   CTimecode timecodePrototype = clipInitInfo.getTimecodePrototype();

   // Remember the In and Out time strings
   // before changing anything so they can be restored.
   string tempInTCStr;
   VTimeCodeEditIn->tcP.getTimeString(tempInTCStr);

   string tempOutTCStr;
   VTimeCodeEditOut->tcP.getTimeString(tempOutTCStr);

   // make sure the duration is non-drop in the proper format
   SetVTimeCodeValue(VTimeCodeEditDur, timecodePrototype);
   VTimeCodeEditDur->tcP.setDropFrame(false);

   // convert all widgets to the proper format
   SetVTimeCodeValue(VTimeCodeEditIn, timecodePrototype);
   SetVTimeCodeValue(VTimeCodeEditOut, timecodePrototype);
   SetVTimeCodeValue(VTimeCodeEditInSmall, timecodePrototype);
   SetVTimeCodeValue(VTimeCodeEditOutSmall, timecodePrototype);
   SetVTimeCodeValue(VTimeCodeEditCen, timecodePrototype);

   // reset widget to the orignal TC values
   SetVTimeCodeValue(VTimeCodeEditIn, tempInTCStr);
   SetVTimeCodeValue(VTimeCodeEditOut, tempOutTCStr);
   SetVTimeCodeValue(VTimeCodeEditInSmall, tempInTCStr);
   SetVTimeCodeValue(VTimeCodeEditOutSmall, tempOutTCStr);
   CalcDuration();
   CalcCentral();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetDefaultClipName()
{
   // Setup the default clip name;
   CBinManager binMgr;
   string newClipName = binMgr.makeNextAutoClipName(lastClickedBinPath);

   if (newClipName.empty())
   {
      TRACE_0(errout << "Error creating new clip name for bin " << lastClickedBinPath);
      MTIostringstream ostr;
      ostr << "Error preparing for a new clip";
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   VideoClipNameEdit->Text = newClipName.c_str();
   FileClipNameEdit->Text = newClipName.c_str();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetDefaultMediaSchemes()
{
   // Setup the default clip scheme
   CBinManager binMgr;
   CClipInitInfo clipInitInfo;
   string newClipScheme = binMgr.getAutoClipScheme(lastClickedBinPath);
   string newMediaLocation = binMgr.getAutoDiskScheme(lastClickedBinPath);

   if (IsClipSchemeValid(newClipScheme) == true)
   {
      CClipSchemeManager csmgr;
      CClipScheme *clipScheme = csmgr.findClipScheme(newClipScheme);
      if (clipScheme == 0)
      {
         return;
      }

      string videoFormatClass = clipScheme->getVideoFormatClass().c_str();
      if (videoFormatClass != "") // new storage
      {
         ////AllocateNewStorageRadioButton->Checked = true;
         NewClipNewMediaPanel->Visible = true;
         NewClipExistingMediaPanel->Visible = false;

         NewMediaVideoFormatComboBox->ItemIndex = NewMediaVideoFormatComboBox->Items->IndexOf(videoFormatClass.c_str());
         NewMediaAudioFormatComboBox->ItemIndex = NewMediaAudioFormatComboBox->Items->IndexOf(clipScheme->getAudioFormatClass().c_str());

         // set storage format, and make sure we set it only in the
         // right combo box
         string newStorageClass = clipScheme->getMediaStorageClass();
         int index = VideoStoreMediaStorageComboBox->Items->IndexOf(newStorageClass.c_str());
         if (index != -1)
         {
            VideoStoreMediaStorageComboBox->ItemIndex = index;
            // switching of the combo boxes will be handled by
            // MediaLocationComboBoxChange(); called by caller
         }

         index = FileRepositoryStorageFormatComboBox->Items->IndexOf(newStorageClass.c_str());
         if (index != -1)
         {
            FileRepositoryStorageFormatComboBox->ItemIndex = index;
            // switching of the combo boxes will be handled by
            // MediaLocationComboBoxChange(); called by caller
         }

         if (IsMediaLocationValid(newMediaLocation) == true)
         {
            MediaLocationComboBox->ItemIndex = MediaLocationComboBox->Items->IndexOf(newMediaLocation.c_str());
         }
         else
         {
            TRACE_2(errout << "Note: Ignoring invalid default media location \"" << newMediaLocation << "\" for bin \"" <<
                  lastClickedBinPath << "\"");
         }
      }
      else // existing storage
      {
         ////UseExistingFilesRadioButton->Checked = true;
         NewClipNewMediaPanel->Visible = false;
         NewClipExistingMediaPanel->Visible = true;

         if (DoesDirectoryExist(newMediaLocation) == true)
         {
            FileClipFolderEdit->Text = newMediaLocation.c_str(); // folder path
         }
         else
         {
            FileClipFolderEdit->Text = "";
            FileClipFileNameEdit->Text = "";
         }

         string newMediaFormat = clipScheme->getClipSchemeName();
         int index = ExistingMediaFormatComboBox->Items->IndexOf(newMediaFormat.c_str());
         if (index != -1)
         {
            ExistingMediaFormatComboBox->ItemIndex = index;
         }
      }
   }
   else
   {
      TRACE_2(errout << "Note: Ignoring invalid default clip scheme \"" << newClipScheme << "\" for bin \"" << lastClickedBinPath << "\"");
   }
}

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::OpenClipMenuItemOrDoubleClick(TObject *Sender)
{
   TListItem *listItem = ClipListView->Selected;
   if (listItem != 0)
   {
      string clipName = StringToStdString(listItem->Caption);
      CBinManager binMgr;
      string clipFileName = binMgr.makeClipFileNameWithExt(lastClickedBinPath, clipName);
      if (binMgrGUIInterface != 0)
      {
         binMgrGUIInterface->ClipDoubleClick(clipFileName);
         lastClickedVersionPath = "";
         RepaintVersionListView();
      }

      // On a modal dialog box, the double click is an ok
      if (FormState.Contains(fsModal))
      {
         // This will end the modal box
         ModalResult = mrOk;
      }
   }
}
// ---------------------------------------------------------------------------

StringList TBinManagerForm::SelectedClipNames(void)
{
   // Just make an accessor function that returns all the names
   StringList slResult;
   GetSelectedClips(slResult);
   // Make real names from this
   for (unsigned i = 0; i < slResult.size(); i++)
   {
      slResult[i] = AddDirSeparator(lastClickedBinPath) + slResult[i];
   }

   return slResult;
}
// ---------------------------------------------------------------------------

int TBinManagerForm::DeleteOneClip(const string &binPath, const string &clipName)
{
   // Delete one clip from the currenty dsiplayed bin
   // Man, whatta loada crap!
   lockID_t lockID;
   int retVal;
   TListItem *item;
   CBinManager binMgr;

   // Tell the application that the clip is about to be deleted
   if (binMgrGUIInterface != 0)
   {
      string clipFileName = binMgr.makeClipFileNameWithExt(binPath, clipName);
      // mlm: to-do: investigate whether legacy callback should be
      // removed in favor of new callback
      binMgrGUIInterface->ClipWillBeDeleted(clipFileName);

      string reason;
      lockID = binMgrGUIInterface->RequestClipDelete(binPath, clipName, reason);
      if (binMgrGUIInterface->IsLockValid(lockID) == false)
      {
         _MTIErrorDialog(Handle, reason);
         return ERR_BIN_MANAGER_DELETE_CLIP;
      }
   }

   string clipGUID;
   CClipInitInfo *clipInitInfo;
   clipInitInfo = binMgr.openClipOrClipIni(binPath, clipName, &retVal);

   if (retVal != 0)
   {
      return retVal;
   }

   if (clipInitInfo->getClipPtr() != nullptr)
   {
      clipGUID = clipInitInfo->getClipPtr()->getClipGUIDStr();
   }
   else
   {
      clipGUID = "";
   }

   clipInitInfo->closeClip(); // no longer need opened clip
   delete clipInitInfo;

   // Tell the application that the clip was just deleted
   if (binMgrGUIInterface != 0)
   {
      ////WARNING : THE CLIP IS STILL VISIBLE IN THE BIN MANAGER AT THIS POINT////
      ///           Is that a problem?

      string clipFileName = binMgr.makeClipFileNameWithExt(binPath, clipName);

      // mlm: to-do: investigate whether legacy callback should be
      // removed in favor of new callback
      binMgrGUIInterface->ClipDeleted(clipFileName);
   }

   retVal = binMgr.deleteClipOrClipIni(binPath, clipName);
   if (retVal != 0)
   {
      if (binMgrGUIInterface != 0)
      {
         string reason;
         int retVal2 = binMgrGUIInterface->FinishLock(lockID, reason);
         if (retVal2 != 0)
         {
            _MTIErrorDialog(Handle, reason);
            return retVal;
         }
      }

      return retVal;
   }

   if (binMgrGUIInterface != 0)
   {
      string reason;
      int retVal = binMgrGUIInterface->FinishClipDelete(lockID, binPath, clipName, clipGUID, reason);
      if (retVal != 0)
      {
         _MTIErrorDialog(Handle, reason);
         return 0; // Oh well
      }
   }

   return 0;
}

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::DeleteClipMenuItemClick(TObject *Sender)
{
   // Delete Selected Clip(s)
   int retVal;
   MTIostringstream ostrMessage;
   string const clipStr = "clip";
   string const clipsStr = "clips";
   StringList toBeDeletedList;
   StringList successfullyDeletedList;

   int deleteCount = GetSelectedClips(toBeDeletedList);
   if (deleteCount < 1)
   {
      return; // No clips selected
   }

   // only continue if a clip has been selected
   ostrMessage << "Are you sure you want to delete the selected " << ((deleteCount == 1) ? clipStr : clipsStr) << "?    ";
   retVal = _MTIConfirmationDialog(Handle, ostrMessage.str());
   if (retVal != MTI_DLG_OK)
   {
      return; // User didn't really want to delete the clips
   }

   // User really wants to delete the clips
   CBusyCursor Busy(true);

   // Delete each clip on the selected clip list
   bool keepGoing = true;
   for (int i = 0; keepGoing && (i < deleteCount); ++i)
   {
      CBinManager binManager;
      string clipName = toBeDeletedList[i];
      string binName = lastClickedBinPath;
      string clipPath = binManager.makeClipPath(lastClickedBinPath, clipName);

      if (binManager.doesClipHaveVersions(lastClickedBinPath, clipName))
      {
         ostrMessage.str("");
         ostrMessage << "Clip " << clipName << " has versions - if you proceed,    " << endl <<
               "ALL versions will be permanently deleted, including    " << endl << "all media files belonging to the version clips    " <<
               endl << "(but not the media files of the parent clip)" << endl << endl << "Do you wish to proceed?";
         retVal = _MTIConfirmationDialog(Handle, ostrMessage.str());
         if (retVal != MTI_DLG_OK)
         {
            return; // User didn't really want to delete all the versions
         }

         // If one of the clip's versions is currently being
         // displayed, we need to undisplay it.
         if (clipPath == lastClickedClipPath)
         {
            LoadNewClip(lastClickedClipPath); // display parent instead for now
         }

         vector<ClipVersionNumber>versionNumberList;
         ClipIdentifier clipId(clipPath);
         binManager.getListOfClipsActiveVersionNumbers(clipId, versionNumberList, true, true);
         for (unsigned j = 0; j < versionNumberList.size(); ++j)
         {
            // Delete a version
            // HACK!! First remember the 'dirty media folder' path
            ClipIdentifier versionClipId(clipPath, versionNumberList[j]);
            string versionPath = versionClipId.GetClipPath();
            string dirtyMediaFolderPath = binManager.getDirtyMediaFolderPath(versionPath);
            binManager.setVersionStatus(clipPath, versionClipId.GetClipName(), versionWasDiscarded);
            retVal = DeleteVersionClip(versionClipId);
            if (retVal == 0)
            {
               // Try to delete the dirty media folder
               if (!dirtyMediaFolderPath.empty())
               {
                  // First remove the metadata ini file, then the media folder
                  string metadataIniFilePath = AddDirSeparator(dirtyMediaFolderPath) + CMediaStorageList::getMetadataIniFilename();
                  binManager.removeFile(metadataIniFilePath);
                  binManager.removeDirectory(dirtyMediaFolderPath);
               }
            }
            else
            {
               MTIostringstream ostr;
               ostr << "Attempt to delete version " << versionClipId.GetClipName()
                     << " of clip " << clipName << "    " << endl << "failed with error code = " << retVal << ".    ";
               _MTIErrorDialog(Handle, ostr.str());
            }
         }
      }

      retVal = DeleteOneClip(lastClickedBinPath, clipName);
      if (retVal != 0)
      {
         MTIostringstream ostr;

         ostr << "Attempt to delete clip " << clipName << "    " << endl << "failed with error code = " << retVal << ".    ";
         if (i < (deleteCount - 1))
         {
            // there are still clips left to delete - put up a dialog
            // with a "cancel" button
            if (MTI_DLG_OK != _MTIWarningDialog(Handle, ostr.str()))
            {
               keepGoing = false;
            } // User clicked cancel
         }
         else
         {
            _MTIErrorDialog(Handle, ostr.str());
         }
      }
      else
      {
         successfullyDeletedList.push_back(clipName);
         string fullPath = AddDirSeparator(lastClickedBinPath) + clipName;
         if (EqualIgnoreCase(fullPath, lastClickedClipPath))
         {
            lastClickedClipPath = "";
         }
      }
   }

   // Remove the clip(s) from the data and the view
   RemoveClipsFromClipListView(&successfullyDeletedList);

   // In case we deleted the proxy's source clip, force a reload so in
   // that case it'll draw a blank
   ReloadProxy();
   RepaintVersionListView();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileMenuDeleteBinItemClick(TObject *Sender)
{
	TTreeNode *node = (rightClickedNode == nullptr) ? BinTree->Selected : rightClickedNode;
   clearRightClick();

   if (node == nullptr)
   {
      return; // neither the last Bin nor the right-clicked Bin was selected
   }

	string binToDelete = getBinPathFromBinTreeNode(node);

   TTreeNode *rootNode = BinTree->Items->Item[0];

   // Prevent deleting root bin!
   if (node == rootNode)
   {
      _MTIErrorDialog(Handle, "Cannot delete root bin.");
      return;
   }

   // First, confirm that the user really wants to delete the bin
   MTIostringstream ostr;
   ostr << "Are you sure you want to permanently delete the bin" << endl
        << binToDelete << endl
        << " and all of its contents?";
   if (_MTIConfirmationDialog(Handle, ostr.str()) != MTI_DLG_OK)
   {
      return;
   }

   CBusyCursor Busy(true);

   // Get the list of Bin roots, but only use the first one
   StringList binList;
   CPMPGetBinRoots(&binList);
   string rootPath;
   if (binList.size() == 1)
   {
      rootPath = binList[0];
   }
   else
   {
      TRACE_0(errout << "Wrong size for Bin Path.  Size = " << binList.size());
      return;
   }

   string reason;
   lockID_t lockID;

   if (binMgrGUIInterface != 0)
   {
      lockID = binMgrGUIInterface->RequestBinDelete(binToDelete, reason);
      if (binMgrGUIInterface->IsLockValid(lockID) == false)
      {
         _MTIErrorDialog(Handle, reason);
         return;
      }
   }

   // Open the modal dialog that will report the status of deletion.
   // Once the dialog box is opened the bin deletion will start.
   // The dialog box will be closed when the deletion is completed or
   // if the user cancels the operation.
   if (DeleteBinDialog == 0)
   {
      // create if doesn't exist
      DeleteBinDialog = new TDeleteBinDialog(this);
   }

   DeleteBinDialog->SetDeleteTarget(binToDelete, rootPath);
   DeleteBinDialog->SetModeDelete(); // Set to delete mode
   TModalResult modalResult = ShowModalDialog(DeleteBinDialog);

   if (modalResult != mrOk)
   {
      if (binMgrGUIInterface != 0)
      {
         string reason;
         int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
         if (retVal != 0)
         {
            _MTIErrorDialog(Handle, reason);
            return;
         }
      }
   }
   else
   {
      if (binMgrGUIInterface != 0)
      {
         string reason;
         int retVal = binMgrGUIInterface->FinishBinDelete(lockID, binToDelete, reason);
         if (retVal != 0)
         {
            _MTIErrorDialog(Handle, reason);
            return;
         }
      }

      TTreeNode *DeletedBinNode = getBinTreeNodeFromBinPath(AddDirSeparator(binToDelete));

      // The Bin and its contents have been deleted, so update the Bin
      // and Clip lists.  The current bin should move up to the parent
      // of the deleted bin.  If the delete failed in any way then the
      // target bin should remain the current bin.

      // Update the Bin Tree display
      // RefreshBinTree();

      // removed a call to refreshbintree(); because that was not
      // preserving the expansions in the graphical tree structure,
      // instead have the node delete itself graphically.  -ms

      if (DeletedBinNode != nullptr)
      {
         DeletedBinNode->Delete();
      }

      // Fix the clip list cache
      BinClipListCacheType::iterator iter;
      iter = BinClipListCache.find(binToDelete);
      if (iter != BinClipListCache.end())
      {
         // found it in the cache
         ClipListDataType *clipListData = iter->second;
         ClearClipList(clipListData);
         BinClipListCache.erase(iter);
         // CAREFUL! Make sure to run BinTreeChange AFTER clobbering
         // CurrentBinClipListData -- Should happen in the next block of
         // code in response the selecting a new bin.
         if (CurrentBinClipListData == clipListData)
         {
            CurrentBinClipListData = nullptr;
         }

         delete clipListData;
      }

   }

   if (!DoesDirectoryExist(lastClickedBinPath))
   {
      // The lastClickedBinPath no longer exists (presumably because
      // it was just deleted), so we need to come up with a new bin name.
      // Lets try the directory that held the deleted bin.
      string newBinPath = RemoveDirSeparator(GetFilePath(RemoveDirSeparator(lastClickedBinPath)));

		SetSelectedBin(newBinPath);
      lastClickedClipPath = "";
   }
   else
   {
      // the lastClickedBinPath is valid still, so lets stay!
      TTreeNode *selectedBinNode = getBinTreeNodeFromBinPath(AddDirSeparator(lastClickedBinPath));
      BinTree->Selected = selectedBinNode;
   }

   MTIassert(CurrentBinClipListData != nullptr);

   if (!DoesDirectoryExist(HighlightedBinPath))
   {
      // The highlighted clip's bin no longer exists, so clear
      // the highlight bin and clip names.  This avoids inadvertent
      // highlighting if user immediately creates a new bin and clip
      // with the same names as the previous highlight names.
      HighlightedBinPath.erase();
      HighlightedClipName.erase();
   }

   // Paranoia - make real sure the clip list was refreshed
   // TODO: Investigate this - does it always do a double refresh?
   RefreshClipList();

   // Reload the proxy in case the proxy clip was deleted
   ReloadProxy();
   RepaintVersionListView();
}

// ---------------------------------------------------------------------------

int TBinManagerForm::GetSelectedClips(StringList& selectedClipList)
{
   // Fill the caller's StringList with the names of clips that
   // are selected in the Clip List View.  Returns the number
   // of selected clips
   string clipName;
   const int totalClipCount = (int) CurrentBinClipListData->size();

   SynchronizeSelectionsIfNecessary();

   // Optimization: quit after we find all the selections
   int numberOfSelectionsLeftToFind = selectedClipCount;

   // Clear the caller's list of clips
   selectedClipList.clear();

   // Search the clip list for selected items
   for (int i = 0; (i < totalClipCount) && (numberOfSelectionsLeftToFind > 0); ++i)
   {
      SClipDataItem &clipDataItem = *CurrentBinClipListData->at(i);
      if (clipDataItem.clipIsSelected)
      {
         selectedClipList.push_back(clipDataItem.clipName);
         --numberOfSelectionsLeftToFind;
      }
   }
   MTIassert(numberOfSelectionsLeftToFind == 0);

   return selectedClipCount;
}

int TBinManagerForm::CountSelectedClips()
{
   // Returns the number of selected clips
   SynchronizeSelectionsIfNecessary();
   return selectedClipCount;
}

void TBinManagerForm::SynchronizeSelectionsIfNecessary()
{
   // If we believe that our notion of which clips are selected may not
   // not match the TListView component's notion, we change our selections
   // to match the TListView's. They get out of synch when the user does
   // a multi selection (shift-click) because the stupid TListView doesn't
   // tell us about the selection state change for all the clips between the
   // start and end clip of the multi-selection!

   // Bug 2522 sometimes frickin' Borland blows away the list without
   // telling us
   if (ClipListView->Items->Count < 1)
   {
      ClearAllSelections();
   }
   else if (selectedListIsInvalid)
   {
      const int totalClipCount = (int) CurrentBinClipListData->size();
      selectedClipCount = 0;
      selectedListIsInvalid = false;

      for (int i = 0; i < totalClipCount; ++i)
      {
         SClipDataItem &clipDataItem = *CurrentBinClipListData->at(i);
         clipDataItem.clipIsSelected = ClipListView->Items->Item[i]->Selected;
         if (clipDataItem.clipIsSelected)
         {
            ++selectedClipCount;
         }
      }
   }
}

void TBinManagerForm::RemoveClipsFromClipListView(StringList *clipNames)
{
   if (clipNames == nullptr)
   {
      return;
   }

   const int aVeryLargeNumber = 0x7FFFFFFF; // QQQ max int
   int earliestListIndex = aVeryLargeNumber;

   // SIDE EFFECT - rather than try to preserve the selections, we blow
   // them away because usually you are removing the selected clips
   ClearAllSelections();

   for (int i = 0; i < (int) clipNames->size(); ++i)
   {
      int listIndex = FindClipListIndexByClipName(clipNames->at(i));
      if (listIndex >= 0 && listIndex <= (int) CurrentBinClipListData->size())
      {
         earliestListIndex = std::min<int>(earliestListIndex, listIndex);
         SClipDataItem *clipDataItem = CurrentBinClipListData->at(listIndex);
         delete clipDataItem;
         CurrentBinClipListData->erase(CurrentBinClipListData->begin() + listIndex);
         ClipListView->Items->Count = ClipListView->Items->Count - 1;
      }
   }

   if (ClipListView->Items->Count == 0)
   {
      ClipListView->Repaint();
   }
   else if (earliestListIndex < aVeryLargeNumber)
   {
      ClipListView->UpdateItems(earliestListIndex, // first
            ClipListView->Items->Count - 1); // last
   }
}

int TBinManagerForm::GetAllClips(StringList& allClipList)
{
   // Fill the caller's StringList with the names of every clip in the list
   // Returns the number of clips
   const int totalClipCount = (int) CurrentBinClipListData->size();

   // Clear the caller's list of clips
   allClipList.clear();

   // Fill it with all the clip names
   for (int i = 0; i < totalClipCount; ++i)
   {
      allClipList.push_back(CurrentBinClipListData->at(i)->clipName);
   }

   return (int)allClipList.size();
}

void TBinManagerForm::SetSelectedClips(StringList& listOfClipsToBeSelected)
{
   // Select the clips in the Clip List View based on the
   // caller's list of clip names (clips not on the list will
   // be de-selected)
   // Returns the number of clips actually selected

   int totalClipCount = (int) CurrentBinClipListData->size();
   selectedClipCount = 0;
   selectedListIsInvalid = false;

   // Select each of the clips on the argument list and deselect the rest
   for (int i = 0; i < totalClipCount; ++i)
   {
      string clipListClipName = CurrentBinClipListData->at(i)->clipName;
      CurrentBinClipListData->at(i)->clipIsSelected = false;

      for (int j = 0; j < (int) listOfClipsToBeSelected.size(); ++j)
      {
         if (clipListClipName == listOfClipsToBeSelected[j])
         {
            CurrentBinClipListData->at(i)->clipIsSelected = true;
            ++selectedClipCount;
            break;
         }
      }
      SetClipListViewItemSelectedState(i, CurrentBinClipListData->at(i)->clipIsSelected);
   }
   MTIassert(selectedClipCount == (int) listOfClipsToBeSelected.size());
}

// ---------------------------------------------------------------------------
string TBinManagerForm::GetSelectedBin()
{
   return lastClickedBinPath;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetSelectedBin(const string& BinPath)
{
   TTreeNode *selectedBinNode = getBinTreeNodeFromBinPath(AddDirSeparator(BinPath));

   BinTree->Selected = selectedBinNode;
   lastClickedBinPath = BinPath;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetOpMode(int mode)
{
   opMode = mode;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewColumnClick(TObject *Sender, TListColumn *Column)
{
   ClipListColumnToSort = Column->Index;

   // We're about to scramble the list, so make sure we know what's selected!
   SynchronizeSelectionsIfNecessary();

   // Sort the data list
   SortClipListData();

   // Repaint the view
   RepaintClipListView();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBSelectAllItemClick(TObject *Sender)
{
   int totalClipCount = (int) CurrentBinClipListData->size();
   if (totalClipCount == 0)
   {
      return;
   }

   // Suck the focus to the clip list so you can see the selections
   ClipListView->SetFocus();

   // Flip on all selections in data and view
   for (int i = 0; i < totalClipCount; ++i)
   {
      CurrentBinClipListData->at(i)->clipIsSelected = true;
      SetClipListViewItemSelectedState(i, true);
   }

   selectedClipCount = totalClipCount;
   selectedListIsInvalid = false;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBInvertSelectItemClick(TObject *Sender)
{
   int totalClipCount = (int) CurrentBinClipListData->size();
   if (totalClipCount == 0)
   {
      return;
   }

   // Suck the focus to the clip list so you can see the selections
   ClipListView->SetFocus();

   // Invert all the selected states in data and copy to the view
   SynchronizeSelectionsIfNecessary();
   selectedClipCount = 0;
   for (int i = 0; i < totalClipCount; ++i)
   {
      SClipDataItem &clipDataItem = *CurrentBinClipListData->at(i);
      clipDataItem.clipIsSelected = !clipDataItem.clipIsSelected;
      if (clipDataItem.clipIsSelected)
      {
         ++selectedClipCount;
      }

      SetClipListViewItemSelectedState(i, clipDataItem.clipIsSelected);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipSchemePopupBoxChange(TObject *Sender)
{
   if (ClipSchemePopupBox->Text != "")
   {
      SetClipSchemeName(ClipSchemePopupBox->Text.c_str());
   }
}

// ---------------------------------------------------------------------------
// -------------------ReadSettings-------------------------John Mertus-----Jan 2001---
bool TBinManagerForm::ReadSettings(CIniFile *ini, const string &IniSection)
{
   BinTree->Width = ini->ReadInteger(IniSection, "BinWidth", 160);
   // BinTree->Width);
   BinTree->Height = ini->ReadInteger(IniSection, "BinHeight", LeftPanel->Height - 160); // BinTree->Height);
//	bool createClipPanelVisible = true; //ini->ReadInteger(IniSection, "CreateClipVisible", UnexpandedNewClipPanel->Visible == false);
//	if (createClipPanelVisible)
//	{
//		ExpandCreateClipPanelButton_REMOVED_Click(this);
//	}
//	else
//	{
//		UnexpandCreateClipPanelButton_REMOVED_Click(this);
//	}

	PanelInOut->Visible = ini->ReadInteger(IniSection, "InOutVisible", 1);
   PanelCentral->Visible = ini->ReadInteger(IniSection, "CentralVisible", 0);
   CheckBoxCentralize->Checked = PanelCentral->Visible;

//	FixCreateClipPanelSize();

	NewMediaVideoFormatComboBox->Text = ini->ReadString(IniSection, "NewMediaVideoFormat", "").c_str();
   NewMediaAudioFormatComboBox->Text = ini->ReadString(IniSection, "NewMediaAudioFormat", "").c_str();
   VideoStoreMediaStorageComboBox->Text = ini->ReadString(IniSection, "VSStorageFormat", "").c_str();
   FileVidClipMediaStorageComboBox->Text = ini->ReadString(IniSection, "FileStorageFormat", "").c_str();
   FileVidClipVideoFolderComboBox->Text = ini->ReadString(IniSection, "FileVideoFolder", "").c_str();
   FileVidClipAudioFolderComboBox->Text = ini->ReadString(IniSection, "FileAudioFolder", "").c_str();
   FileVidClipFileNameEdit->Text = ini->ReadString(IniSection, "FileFileName", "").c_str();
   FileVidClipTCIndexCheckBox->Checked = ini->ReadBool(IniSection, "UseTCIndex", false);
   FileVidClipTCIndexCheckBoxClick(this);
   FileClipTCCheckBox->Checked = ini->ReadBool(IniSection, "UseTC", false);
   // Bugzilla 2524
   FileClipTCCheckBoxClick(this);
   bool preferChoosingClipFolder = ini->ReadBool(IniSection, "PreferChoosingClipFolder", true);
   ChooseFolderRadioButton->Checked = preferChoosingClipFolder;
   ChooseFileRadioButton->Checked = !preferChoosingClipFolder; // Not sure why I should need to do this,
   ChooseFolderOrChooseFileRadioButtonClick(nullptr);

   // Save the design column widths so we can identify defaults.
   DefaultColumnWidths.clear();
   int columnCount = ClipListView->Columns->Count;
   for (int i = 0; i < columnCount; ++i)
   {
      DefaultColumnWidths.push_back(ClipListView->Columns->Items[i]->Width);
   }

   // When we read in the column widths, if the "column generation" is wrong or
   // the count does not match what we think the number should be, we ignore the
   // saved widths because it means we added or subtracted a column so the
   // numbering may be wrong in the ini.
   int columnGeneration = ini->ReadInteger(IniSection, "ColumnGeneration", 0);
   IntegerList columnWidths = ini->ReadIntegerList(IniSection, "ColumnWidths");
   int count = columnWidths.size();
   if (columnGeneration == COLUMN_GENERATION_NUMBER && count == ClipListView->Columns->Count)
   {
      for (int i = 0; i < count; ++i)
      {
         // We use -1 to indicate "use default value".
         if (columnWidths[i] >= 0)
         {
            ClipListView->Columns->Items[i]->Width = columnWidths[i];
         }
      }
   }

   // This works because the controls' lists were set up at FormCreate time;
   // this code prevents empty or incorrect combobox settings
   if (NewMediaVideoFormatComboBox->ItemIndex < 0)
   {
      NewMediaVideoFormatComboBox->ItemIndex = 0;
   }

   if (NewMediaAudioFormatComboBox->ItemIndex < 0)
   {
      NewMediaAudioFormatComboBox->ItemIndex = 0;
   }

   if (VideoStoreMediaStorageComboBox->ItemIndex < 0)
   {
      VideoStoreMediaStorageComboBox->ItemIndex = 0;
   }

   if (FileRepositoryStorageFormatComboBox->ItemIndex < 0)
   {
      FileRepositoryStorageFormatComboBox->ItemIndex = 0;
   }

   ClipFormatComponentChanged(this);

   // Tell child dialogs to read their SAPs
   if (ImportTCDialog != nullptr)
   {
      ImportTCDialog->ReadSettings(ini, IniSection);
   }

   if (ExportTCDialog != nullptr)
   {
      ExportTCDialog->ReadSettings(ini, IniSection);
   }

   if (FormMultiBrowse != nullptr)
   {
      FormMultiBrowse->ReadSettings(ini, IniSection);
   }

// WTF???
//   TTreeNode *rootNode;
//   rootNode = BinTree->Items->Item[0];

   return true;
}

// -------------------------WriteSettings-------------------John Mertus-----Jan 2001---

bool TBinManagerForm::WriteSettings(CIniFile *ini, const string &IniSection)
{
   ini->WriteInteger(IniSection, "BinWidth", BinTree->Width);
   ini->WriteInteger(IniSection, "BinHeight", BinTree->Height);
	ini->WriteInteger(IniSection, "CreateClipVisible", true); //UnexpandedNewClipPanel->Visible == false);

   ini->WriteInteger(IniSection, "InOutVisible", PanelInOut->Visible);
   ini->WriteInteger(IniSection, "CentralVisible", PanelCentral->Visible);

   ini->WriteString(IniSection, "NewMediaVideoFormat", StringToStdString(NewMediaVideoFormatComboBox->Text));
   ini->WriteString(IniSection, "NewMediaAudioFormat", StringToStdString(NewMediaAudioFormatComboBox->Text));
   ini->WriteString(IniSection, "VSStorageFormat", StringToStdString(VideoStoreMediaStorageComboBox->Text));
   ini->WriteString(IniSection, "FileStorageFormat", StringToStdString(FileVidClipMediaStorageComboBox->Text));
   ini->WriteString(IniSection, "FileVideoFolder", StringToStdString(FileVidClipVideoFolderComboBox->Text));
   ini->WriteString(IniSection, "FileAudioFolder", StringToStdString(FileVidClipAudioFolderComboBox->Text));
   ini->WriteString(IniSection, "FileFileName", StringToStdString(FileVidClipFileNameEdit->Text));
   ini->WriteBool(IniSection, "UseTCIndex", FileVidClipTCIndexCheckBox->Checked);
   ini->WriteBool(IniSection, "UseTC", FileClipTCCheckBox->Checked); // Bugz 2524
   ini->WriteBool(IniSection, "PreferChoosingClipFolder", ChooseFolderRadioButton->Checked);

   ini->WriteInteger(IniSection, "ColumnGeneration", COLUMN_GENERATION_NUMBER);
   int count = ClipListView->Columns->Count;
   IntegerList columnWidths;
   for (int i = 0; i < count; ++i)
   {
      int width = ClipListView->Columns->Items[i]->Width;
      if (DefaultColumnWidths.size() == count && width == DefaultColumnWidths[i])
      {
         // Don't save default values, use -1 instead.
         width = -1;
      }
      columnWidths.push_back(width);
   }

   ini->WriteIntegerList(IniSection, "ColumnWidths", columnWidths);

   if (ImportTCDialog != nullptr)
   {
      ImportTCDialog->WriteSettings(ini, IniSection);
   }

   if (ExportTCDialog != nullptr)
   {
      ExportTCDialog->WriteSettings(ini, IniSection);
   }

   if (FormMultiBrowse != nullptr)
   {
      FormMultiBrowse->WriteSettings(ini, IniSection);
   }

   return true;
}

// -------------------ReadDesignSAP-------------------John Mertus-----Jul 2002---

bool TBinManagerForm::ReadDesignSAP(void)

      // This reads the design parameter
      //
      // Return should be true
      //
      // Note: This function is designed to be overridden in the derived class
      //
      // ******************************************************************************
{
   BinTree->Width = OriginalBinTreeWidth;
	UnexpandedNewClipPanel_REMOVED_->Visible = false; //OriginalCreateClipPanelVisible == false;
   ProxyDisplayPanel->Visible = OriginalCreateProxyPanelVisible;

   return true;
}

// -----------------ViewMenuItemClick-----------John Mertus----Jul 2002-----

void TBinManagerForm::CancelHighlightClip()
{
   int totalClipCount = (int) CurrentBinClipListData->size();

   // Blow away highlight and all selections
   for (int i = 0; i < totalClipCount; ++i)
   {
      CurrentBinClipListData->at(i)->clipIsSelected = false;
      if (CurrentBinClipListData->at(i)->isHighlighted)
      {
         CurrentBinClipListData->at(i)->isHighlighted = false;
         ClipListView->UpdateItems(i, i);
      }
   }

   ClipListView->ClearSelection(); // Clears 'em all
   selectedClipCount = 0;
   selectedListIsInvalid = false;
}

// -----------------HighlightClip-----------John Mertus----Jul 2002-----

bool TBinManagerForm::HighlightClip(const string &clipPath, bool expandBin)

      // This sets the yellow highlight of the clip.
      // clipPath is the full path to the clip
      // expandBin is true means the bin tree will open up to the bin name
      // expandBin false makes no change in the tree.
      //
      // ****************************************************************************
{
   // If no clip is selected, just select the root node in the bin tree.
   if (clipPath.empty())
   {
      if (BinTree->Items->Count > 0)
      {
         BinTree->Items->Item[0]->Selected = true;
      }

      return false;
   }

   CBinManager binManager;
   ClipIdentifier clipId(clipPath);
   ClipVersionNumber clipVersionNumber = clipId.GetVersionNumber();
   string masterClipPath = clipId.GetMasterClipPath();
   string masterClipBinPath = clipId.GetBinPath();
   string masterClipClipName = clipId.GetMasterClipName();

//   DBTRACE(clipId.GetClipPath());
//   DBTRACE(clipId.GetMasterClipName());
//   DBTRACE(clipId.GetVersionNumber().ToDisplayName);
//   DBTRACE(clipId.GetMasterClipPath());
//   DBTRACE(clipId.GetMasterClipName());
//   DBTRACE(clipId.GetBinPath());
//   DBTRACE(clipId.GetBinName());
//   DBTRACE(clipId.GetParentPath());
//   DBTRACE(clipId.GetParentName());

   // Remember the name for the buttons that copy the TCs in Create Clip panel
   YellowHilitedMasterClip = masterClipPath;
   YellowHilitedVersion = clipId.GetVersionNumber();

   // Find the Bin Tree node for the bin that contains the highlighted clip.
   TTreeNode *tnMatch = getBinTreeNodeFromBinPath(masterClipBinPath);

   // See if there was a match
   // Selecting the node regenerates the view, so we must always see
   // if the name matches.
   if (tnMatch != nullptr)
   {
      HighlightedBinPath = masterClipBinPath;
      HighlightedClipName = masterClipClipName;

      // Decide to expand tree .
      if (expandBin)
      {
         tnMatch->Selected = true;
      }

      // Blow away any other highlights and selections
      CancelHighlightClip();

      // There are two cases
      // 1) The bin is the currently displayed bin
      // 2) The bin in not the currently displayed bin
      // In the second case, don't highlight anything

      if (IsSameBinPath(lastClickedBinPath, HighlightedBinPath))
      {
         int totalClipCount = (int) CurrentBinClipListData->size();
         int i;
         for (i = 0; i < totalClipCount; ++i)
         {
            if (EqualIgnoreCase(masterClipClipName, CurrentBinClipListData->at(i)->clipName))
            {
               CurrentBinClipListData->at(i)->isHighlighted = true;
               CurrentBinClipListData->at(i)->clipIsSelected = true;
               ++selectedClipCount; // before setting view item state
               SetClipListViewItemSelectedState(i, true);

               // Set focus to the bin manager, not sure why
               if (BinManagerForm->Active)
               {
                  if (VersionListView->Items->Count > 0)
                  {
                     VersionListView->SetFocus();
                  }
                  else
                  {
                     ClipListView->SetFocus();
                  }
               }

               // Optimization: there can be only one highlighted clip
               break;
            }
         }

         MTIassert(selectedClipCount == 1);
      }
   }
   else  // (tnMatch != nullptr)
   {
      // There was no match because there is NO clip selected,
      // so just select the root node instead.
      if (BinTree->Items->Count > 0)
      {
         BinTree->Items->Item[0]->Selected = true;
      }

      return false;
   }

   return true;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewCustomDrawItem(TCustomListView *Sender, TListItem *Item, TCustomDrawState State,
      bool &DefaultDraw)
{
   int i = Item->Index;

   // Haha this takes care of the negative case, too!
   if ((unsigned) i >= CurrentBinClipListData->size())
   {
      return; // sanity
   }

   string fullClipName = AddDirSeparator(lastClickedBinPath) + CurrentBinClipListData->at(i)->clipName;
   if (CurrentBinClipListData->at(i)->isHighlighted)
   {
      Sender->Canvas->Brush->Color = YellowHilitedVersion.IsValid() ? clLightYellow : clYellow;

      // Uggh, what probem is THIS solving? QQQ
      if (lastClickedClipPath.empty())
      {
         lastClickedClipPath = fullClipName;
      }

      Sender->Canvas->Font->Color = clBlack;
   }
   else
   {
      string redClipName;
      if (binMgrGUIInterface)
      {
         redClipName = binMgrGUIInterface->GetRedClipName();
      }

      Sender->Canvas->Brush->Color = (fullClipName == redClipName) ? clRed : (TColor)0x6a6a6a;

      Sender->Canvas->Font->Color = clWhite;
   }
}
// ---------------------------------------------------------------------------

// Switch to a new master clip
void TBinManagerForm::LoadNewClip(string const &clipPath, int offsetToNewClip)
{
   CBusyCursor Busy(true);

   CBinManager binManager;
   ClipIdentifier clipId(clipPath);
   string masterClipPath = clipId.GetMasterClipPath();
   string masterClipBinPath = clipId.GetBinPath();
   string masterClipClipName = clipId.GetMasterClipName();

   // Switch to the bin that holds the master clip if we're not already showing it
   if (!IsSameBinPath(lastClickedBinPath, masterClipBinPath))
   {
      TTreeNode *binNode = getBinTreeNodeFromBinPath(masterClipBinPath);
      if (binNode != nullptr)
      {
         BinTreeChange(nullptr, binNode);
      }
   }

   int baseOffset = FindClipListIndexByClipName(masterClipClipName);
   if (baseOffset < 0)
   {
      // In theory, base clip will always be found, so can't get here
      MTIassert(false);
      return;
   }

   offsetToNewClip += baseOffset;

   if (offsetToNewClip < 0)
   {
      // Sanity
      offsetToNewClip = 0;
      // Beep();
   }

   const int totalClipCount = (int) CurrentBinClipListData->size();
   if (offsetToNewClip >= totalClipCount)
   {
      // Sanity
      offsetToNewClip = totalClipCount - 1;
      // Beep();
   }

   // Don't short circuit if new clip same as displayed, may actually
   // be displaying a version of the clip
   // if (offsetToNewClip == baseOffset)
   // return;

   // Go load the file
   // ?? How can clipName be different from masterClipName?? QQQ
   string clipName = CurrentBinClipListData->at(offsetToNewClip)->clipName;
   string clipFileName = binManager.makeClipFileNameWithExt(masterClipBinPath, clipName);
   if (binMgrGUIInterface != 0)
   {
      binMgrGUIInterface->ClipDoubleClick(clipFileName);
   }

   lastClickedClipPath = AddDirSeparator(lastClickedBinPath) + clipName;
   lastClickedVersionPath = "";

   ReloadProxy();

   RepaintVersionListView();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::LoadNewVersion(string const &clipPath, int offsetToNewClip)
{
   CBinManager binManager;
   string parentPath;
   ClipIdentifier clipId(clipPath);
   ClipIdentifier versionClipId(clipPath);

   CBusyCursor Busy(true);

   if (!clipId.IsVersionClip())
   {
      // Not a version clip - select the first version always
      offsetToNewClip = 0;
      vector<ClipVersionNumber>versionNumberList;

      binManager.getListOfClipsActiveVersionNumbers(clipId, versionNumberList, false);
      if (versionNumberList.empty())
      {
         LoadNewClip(clipPath);
         return;
      }

      versionClipId = clipId + versionNumberList[0];
   }
   else if (offsetToNewClip != 0)
   {
      vector<ClipVersionNumber>versionNumberList;
      ClipIdentifier parentClipId = clipId.GetParentClipId();

      binManager.getListOfClipsActiveVersionNumbers(parentClipId, versionNumberList, false);
      unsigned versionNumberListSize = versionNumberList.size();
      unsigned i;
      string oldVersionClipName = clipId.GetClipName();
      for (i = 0; i < versionNumberListSize; ++i)
      {
         if (versionNumberList[i].ToDisplayName() == oldVersionClipName)
         {
            break;
         }
      }

      if (i == versionNumberListSize)
      {
         LoadNewClip(clipPath);
         return;
      }

      // Make sure index is in range - couldn't figure out how to make % work!
      int newVersionIndex = int(i) + offsetToNewClip;
      while (newVersionIndex < 0)
      {
         newVersionIndex += versionNumberListSize;
      }

      while (newVersionIndex >= (int) versionNumberListSize)
      {
         newVersionIndex -= versionNumberListSize;
      }

      versionClipId = parentClipId + versionNumberList[newVersionIndex];
   }

   string junk;

   lastClickedClipPath = versionClipId.GetMasterClipPath();
   string binName = versionClipId.GetBinName();

   // Switch to the bin that holds the base clip if we're not already showing it
   if (!IsSameBinPath(lastClickedBinPath, binName))
   {
		TTreeNode *binNode = getBinTreeNodeFromBinPath(binName);
      if (binNode != nullptr)
      {
         BinTreeChange(nullptr, binNode);
      }

      // Why doesn't this set lastClickedBinPath?? Does BinTreeChange do it? QQQ
   }

   if (binMgrGUIInterface != 0)
   {
      binMgrGUIInterface->ClipDoubleClick(versionClipId.GetClipPath());
   }

   lastClickedVersionPath = versionClipId.GetClipPath();

   RepaintVersionListView();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::MediaLocationComboBoxChange(TObject *Sender)
{
   // DISK SCHEMES NO LONGER USED
   // if (MediaLocationComboBox->Text != "")
   // {
   // string newDiskScheme         = StringToStdString(MediaLocationComboBox->Text);
   //
   // // QQQ CACHE THE DISK SCHEME TYPE
   // CDiskSchemeList diskSchemeList;
   // if (0 != diskSchemeList.ReadDiskSchemeFile())
   // {
   // _MTIErrorDialog(Handle, "Could not open Disk Scheme File");
   // return;
   // }
   //
   // CDiskScheme diskScheme = diskSchemeList.Find(newDiskScheme);
   // switch (diskScheme.mainVideoMediaRepositoryType)
   // {
   // case MEDIA_REPOSITORY_TYPE_RAW_DISK:
   // VideoStoreMediaStorageComboBox->Visible = true;
   // FileRepositoryStorageFormatComboBox->Visible = false;
   // break;
   // case MEDIA_REPOSITORY_TYPE_FILE_SYSTEM:
   // FileRepositoryStorageFormatComboBox->Visible = true;
   // VideoStoreMediaStorageComboBox->Visible = false;
   // break;
   // default:
   // break;
   // }
   // }
   // ClipFormatComponentChanged(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormShow(TObject *Sender)
{
	if (FormState.Contains(fsModal))
	{
		ModalPanel->Top = 0;
		ModalPanel->Visible = true;
		CreateClipPanel->Visible = false;
		CreateClipFromFileOrFolderPanel->Visible = false;
		BottomPanel->Height = ModalPanel->Height;
	}
	else
	{
		CreateClipPanel->Top = 0;
		CreateClipPanel->Visible = true;
		CreateClipFromFileOrFolderPanel->Visible = true;
		ModalPanel->Visible = false;
		BottomPanel->Height = CreateClipPanel->Height;
	}

	CreateClipPanel->Visible = ! ModalPanel->Visible;
	FixCreateClipPanelSize();

   switch (opMode)
   {
   case BM_MODE_NORMAL:
   default:
      break;

   case BM_MODE_SELECT_CLIP:
      Caption = "Select a Clip";
      break;

   case BM_MODE_SELECT_BIN:
      Caption = "Select a Bin";
      break;
   }

   FillByRecordingRadioButton->Enabled = true;

   // clear tab stops from radio buttons
   RadioButtonClick(nullptr);
   UpdateTabOrder();

   TMTIForm::FormShow(Sender);
}
// ---------------------------------------------------------------------------

void ReParent(TControl *Child, TWinControl *Parent)
{
   Child->Parent = Parent;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FolderBrowseButtonClick(TObject *Sender)
{
   string sName = StringToStdString(FileClipFolderEdit->Text);
   Application->NormalizeTopMosts();
   string newDir = GetUserFolder(Handle, sName.c_str(), "File Folder");
   Application->RestoreTopMosts();
   if (!newDir.empty())
   {
      FileClipFolderEdit->Text = newDir.c_str();
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipFolderOrFileBrowserButtonClick(TObject *Sender)
{
   CBusyCursor Busy(true);
   InFileClipEdit->Text = "";
   OutFileClipEdit->Text = "";
   DurFileClipEdit->Text = "";
   if (ChooseFileRadioButton->Checked)
   {
      BrowseForMediaFile();
   }
   else
   {
      BrowseForMediaFolder();
   }
}
// ---------------------------------------------------------------------------
static string lastSelectedFolder;

string TBinManagerForm::BrowseForFolder(string startingFolderPath = lastSelectedFolder)
{
   Application->NormalizeTopMosts();
   string folder = MTIBrowseForFolder(startingFolderPath, string("Browse to Media Folder"));
   Application->RestoreTopMosts();
   if (!folder.empty())
   {
      lastSelectedFolder = folder;
   }

   return folder;
}
// ---------------------------------------------------------------------------

static string okExtensions[] =
{"cin", "dpx", "exr", "tif", "tiff"};

string TBinManagerForm::FindAnAcceptableImageFile(string directory)
{
   bool foundAnAcceptableFile = false;
   string acceptableFileName;
   WIN32_FIND_DATA FindFileData;
   string pattern = directory + "\\*";
   HANDLE hFind = FindFirstFile(pattern.c_str(), &FindFileData);
   if (hFind != INVALID_HANDLE_VALUE)
   {
      do
      {
         if (strcmp(FindFileData.cFileName, ".") == 0 || strcmp(FindFileData.cFileName, "..") == 0)
         {
            continue;
         }

         string ext = GetFileExt(FindFileData.cFileName);
         if (ext == ".dpx" || ext == ".exr" || ext == ".tif" || ext == ".tiff")
         {
            acceptableFileName = FindFileData.cFileName;
            foundAnAcceptableFile = true;
            break;
         }
      }
      while (FindNextFile(hFind, &FindFileData));

      FindClose(hFind);
      hFind = INVALID_HANDLE_VALUE;
   }

   return foundAnAcceptableFile ? acceptableFileName : "";
}
// ---------------------------------------------------------------------------

void TBinManagerForm::BrowseForMediaFolder()
{
   string startingFolderPath = StringToStdString(FileClipFolderEdit->Text);
   string directory = BrowseForFolder(startingFolderPath);
   if (directory.empty())
   {
      // Cancelled!
      return;
   }

   FileClipFolderEdit->Text = directory.c_str();
   FileClipFileNameEdit->Text = "";

   string acceptableImageFile = FindAnAcceptableImageFile(directory);
   if (acceptableImageFile.empty())
   {
      MTIostringstream os;
      os << "No image files found in that folder." << endl << "Try browsing for media file instead.";
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // This is what turns the actual file name into a template.
   FileClipFileNameEdit->Text = acceptableImageFile.c_str();
   FileClipFileNameEditExit(nullptr);

   DoSuperclipCrap();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::BrowseForMediaFile()
{
   const string strAllFileFilter
         ("DPX (*.dpx)|*.dpx|" "OpenEXR (*.exr)|*.exr|" "TIFF (*.tif, *.tiff)|*.tif; *.tiff|" "All Image Files|*.dpx; *.exr; *.tif; *.tiff|"
         "All Files|*.*");

   if (!BrowseForFileName(FileClipFolderEdit, FileClipFileNameEdit, strAllFileFilter, 9))
   {
      // Cancelled!
      return;
   }

   DoSuperclipCrap();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::DoSuperclipCrap()
{
   char caPath[1024];
   char caLastDir[512];
   char caSubDir[512];
   char caAllButLastDir[1024];
   char caNextToLastDir[1024];
   char caSuperClipName[1024];
   char caFileExt[1024];
   static TMultipleDirectoriesForm *MultipleDirectoriesForm = nullptr;

   // HACK PART 1! save Timecode overrides to be able to restore them
   // after they get clobbered when we set the clip name!
   PTimecode overrideTC = FileClipInTCEdit->tcP;
   bool overrideDF = FileClipDFCheckBox->Checked;
   int overrideFPS = FileClipFPSComboBox->ItemIndex;
   bool restoreTimecodeOverrideAtExit = false; // only if multi-directory

   if (FormPref->ShowMultipleDirectoriesForm())
   {
      struct ffblk ffblk1;
      int iRet, i, iIndex;
      char caTry[1024], caBaseSubDir[512], caDirToCheck[1024];
      bool bFoundDigit = false;
      int iSearchLocation = FormPref->GetMultipleDirectorySearchLocation();

      // Grab the extension (e.g. .dpx)
      strcpy(caFileExt, GetFileExt(StringToStdString(OpenDialog->FileName)).c_str());

      // Grab the last directory, (e.g. scene1)
      strcpy(caLastDir, GetFileLastDir(StringToStdString(FileClipFolderEdit->Text)).c_str());

      // Grab all but the last directory, (e.g. f:\images\reel1\)
      strcpy(caAllButLastDir, GetFileAllButLastDir(StringToStdString(FileClipFolderEdit->Text)).c_str());

      // Grab the next to last directory, (e.g. reel1)
      strcpy(caNextToLastDir, GetFileLastDir(caAllButLastDir).c_str());

      // Search for digit(s) in caLastDir
      int iLenLastDir = strlen(caLastDir);
      int iLastFoundDigit = 0;
      int iFirstFoundDigit = 0;

      if (iSearchLocation == SEARCH_LOCATION_END)
      {
         i = iLenLastDir - 1;
         while ((i >= 0) && (isdigit(caLastDir[i])))
         {
            bFoundDigit = true;
            iFirstFoundDigit = i; // Set this to the newly discovered digit
            i--;
         }
      }
      else // iSearchLocation == BEGINNING
      {
         i = 0;
         while ((i < iLenLastDir) && (isdigit(caLastDir[i])))
         {
            bFoundDigit = true;
            iLastFoundDigit = i; // Set this to the newly discovered digit
            i++;
         }
      }

      // If there's a digit in the directory, then we'll try to find other
      // similarly numbered directories. - mpr
      if (bFoundDigit)
      {
         int iPosFirstDigit = 0;
         int iNumDigits = 0;

         if (iSearchLocation == SEARCH_LOCATION_END)
         {
            iPosFirstDigit = iFirstFoundDigit;
            iNumDigits = iLenLastDir - iFirstFoundDigit;
         }
         else
         {
            iPosFirstDigit = 0;
            iNumDigits = iLastFoundDigit + 1;
         }

         int iListBoxIndex = 0;
         int iOrigClipIndex = 0;

         TStringList *tslDirs = new TStringList;
         TStringList *tslSubDirs = new TStringList;
         TStringList *tslFiles = new TStringList;
         TStringList *tslClipNames = new TStringList;

         tslDirs->Clear();
         tslSubDirs->Clear();
         tslDirs->Clear();
         tslClipNames->Clear();

         iListBoxIndex = 0;

         // Get BaseSubDir - (e.g. scene1 -> scene)
         if (iSearchLocation == SEARCH_LOCATION_END)
         {
            strcpy(caBaseSubDir, caLastDir);
            caBaseSubDir[iPosFirstDigit] = '\0'; // Truncate before first digit
         }
         else
         {
            char *caTmp = caLastDir;
            caTmp += iNumDigits;
            strcpy(caBaseSubDir, caTmp);
         }

         // Grab the index - (e.g. 1)
         sscanf(&(caLastDir[iPosFirstDigit]), "%d", &iIndex);

         // Build up SubDir from component parts, (e.g. scene1).  Also handle
         // cases with leading zeros.
         if (iSearchLocation == SEARCH_LOCATION_END)
         {
            sprintf(caSubDir, "%s%0*d", caBaseSubDir, iNumDigits, iIndex);
         }
         else
         {
            sprintf(caSubDir, "%0*d%s", iNumDigits, iIndex, caBaseSubDir);
         }

         // This is the first directory to check, (e.g. f:\images\reel\scene1\)
         sprintf(caDirToCheck, "%s%s\\", caAllButLastDir, caSubDir);

         // Keep checking directories
         while (DoesDirectoryExist((string)caDirToCheck))
         {
            // Generate a possible file in the first directory
            // to check, (e.g. f:\images\reel\scene1\*.dpx)
            strcpy(caTry, caDirToCheck);
            strcat(caTry, "*");
            strcat(caTry, caFileExt);

            // Search for a file that matches
            iRet = findfirst(caTry, &ffblk1, 0);
            if (iRet == 0) // We found a file that matched our wildcard
            {
               char caFileName[256];

               strcpy(caFileName, ffblk1.ff_name);
               TRACE_2(errout << "Found file: " << caTry);
               // Save the associated parts of the file for future use
               tslDirs->Add(caDirToCheck);
               tslSubDirs->Add(caSubDir);
               tslFiles->Add(caFileName);
               iListBoxIndex++;
            }

            iIndex++;

            // Generate the name of the next directory to check, (e.g. f:\images\reel\scene2)
            if (iSearchLocation == SEARCH_LOCATION_END)
            {
               sprintf(caSubDir, "%s%0*d", caBaseSubDir, iNumDigits, iIndex);
            }
            else
            {
               sprintf(caSubDir, "%0*d%s", iNumDigits, iIndex, caBaseSubDir);
            }

            sprintf(caDirToCheck, "%s%s\\", caAllButLastDir, caSubDir);
         }

         // If we found more than one similar directory, then pop up the
         // MultipleDirectoriesForm Window
         if (iListBoxIndex > 1)
         {
            if (MultipleDirectoriesForm == nullptr)
            {
               MultipleDirectoriesForm = new TMultipleDirectoriesForm(BinManagerForm);
            }

            MultipleDirectoriesForm->DirectoryCheckListBox->Items->Clear();

            // Fill in the CheckListBox with the list of directories
            for (i = 0; i < iListBoxIndex; i++)
            {
               MultipleDirectoriesForm->DirectoryCheckListBox->Items->Add(tslDirs->Strings[i]);
               // If this is the one the user selected, then pre-check it.
               if (FileClipFolderEdit->Text == tslDirs->Strings[i])
               {
                  iOrigClipIndex = i; // save index for later restoring of gui
                  MultipleDirectoriesForm->DirectoryCheckListBox->Checked[i] = true;
               }
            }

            // Fill in the proposed name for the super-clip
            if (strcmp(caNextToLastDir, "") == 0)
            {
               // If empty, then use name "super-clip"
               MultipleDirectoriesForm->SuperClipNameEdit->Text = "super-clip";
            }
            else
            {
               // Use (e.g. reel1)
               MultipleDirectoriesForm->SuperClipNameEdit->Text = caNextToLastDir;
            }

            // If the user clicked OK, then create the clips
            MultipleDirectoriesForm->PopupParent = this;
            if (ShowModalDialog(MultipleDirectoriesForm) == mrOk)
            {
               int iNumToDo = MultipleDirectoriesForm->DirectoryCheckListBox->Items->Count;
               int iNumClipsCreated = 0;
               AnsiString strFileClipFolderEditSave = "";
               AnsiString strFileClipEditSave = "";
               AnsiString strClipNameEditSave = "";

               // Save some gui variables for later restore
               strFileClipFolderEditSave = FileClipFolderEdit->Text;
               strFileClipEditSave = FileClipFileNameEdit->Text;
               strClipNameEditSave = FileClipNameEdit->Text;

               // Iterate for each item in the CheckListBox
               for (i = 0; i < iNumToDo; i++)
               {
                  if (MultipleDirectoriesForm->DirectoryCheckListBox->Checked[i])
                  {
                     char caTmp[512];
                     char caFullClipName[1024];

                     CBusyCursor Busy(true);

                     // Create all the clips the user checked
                     strcpy(caTmp, StringToStdString(MultipleDirectoriesForm->DirectoryCheckListBox->Items->Strings[i]).c_str());
                     TRACE_2(errout << "Dir: " << MultipleDirectoriesForm->DirectoryCheckListBox->Items->Strings[i].c_str());

                     // Fill in GUI variables to similate clip creation by user's clicking
                     FileClipFolderEdit->Text = tslDirs->Strings[i];
                     FileClipFileNameEdit->Text = tslFiles->Strings[i];
                     FileClipFileNameEditChange(nullptr);
                     FileClipFileNameEditExit(nullptr);

                     FileClipNameEdit->Text = tslSubDirs->Strings[i];

                     // HACK part 2: restore the overrides that were just clobbered
                     FileClipInTCEdit->tcP = overrideTC;
                     FileClipDFCheckBox->Checked = overrideDF;
                     FileClipFPSComboBox->ItemIndex = overrideFPS;
                     restoreTimecodeOverrideAtExit = true;

                     // Create the clip
                     CreateFileClipButtonClick(nullptr);

                     // Generate the full clip name (e.g. c:\MTIBins\Mike\scene1)
                     sprintf(caFullClipName, "%s\\%s", lastClickedBinPath.c_str(), StringToStdString(tslSubDirs->Strings[i]).c_str());
                     tslClipNames->Add(caFullClipName);
                     // keep track of added clipnames
                     iNumClipsCreated++;
                  }
               }

               // Restore the gui variables we changed during the multiple clip creation
               FileClipFolderEdit->Text = strFileClipFolderEditSave;
               FileClipFileNameEdit->Text = strFileClipEditSave;
               FileClipNameEdit->Text = strClipNameEditSave;

               // Create the Virtual Super Clip if user chose more than 1 directory
               // and we created more than 1 clip.
               if ((MultipleDirectoriesForm->CreateSuperClipCheckBox->State == cbChecked) && (iNumClipsCreated > 1))
               {
                  CBinManager binManager;
                  int iNFrames = 0;
                  int iRet = 0;
                  char caDir[1024];
                  char caMessage[256];

                  TRACE_2(errout << "Creating Super Clip");

                  CBusyCursor Busy(true);

                  // Get the number of frames in all the clips
                  iNFrames = GetTotalFrames(tslClipNames);
                  if (iNFrames < 0)
                  {
                     sprintf(caMessage, "GetTotalFrames failed with: %d", iNFrames);
                     theError.set(iNFrames, caMessage);
                     _MTIErrorDialog(Handle, theError.getFmtError());
                     return;
                  }

                  // open the clip based on the file the user originally selected
                  ClipSharedPtr clipSrc = binManager.openClip(StringToStdString(tslClipNames->Strings[iOrigClipIndex]), &iRet);
                  if (iRet)
                  {
                     sprintf(caMessage, "Unable to open the clip:  %s  because: %d",
                           StringToStdString(tslClipNames->Strings[iOrigClipIndex]).c_str(), iRet);
                     theError.set(iRet, caMessage);
                     _MTIErrorDialog(Handle, theError.getFmtError());
                     return;
                  }

                  sprintf(caSuperClipName, "%s\\%s", lastClickedBinPath.c_str(),
                        StringToStdString(MultipleDirectoriesForm->SuperClipNameEdit->Text).c_str());

                  // Create the Destination clip with basic info
                  ClipSharedPtr clipDst = CreateSuperClip(caSuperClipName, iNFrames, clipSrc, &iRet);
                  if (iRet)
                  {
                     sprintf(caMessage, "Unable to create super clip:  %s  because: %d", caSuperClipName, iRet);
                     theError.set(iRet, caMessage);
                     _MTIErrorDialog(Handle, theError.getFmtError());

                     binManager.closeClip(clipSrc);

                     return;
                  }

                  binManager.closeClip(clipSrc);

                  // Add in all of the subclips' frame lists
                  MakeBigVirtualClip(tslClipNames, clipDst);

                  // Update the Clip List
                  AddClipToClipListView(clipDst);
               } // if CreateSuperClipCheckBox
            } // if ShowModel == OK
         } // if (iListBoxIndex > 1)

         // Cleanup
         delete tslDirs;
         delete tslSubDirs;
         delete tslFiles;
         delete tslClipNames;
      } // if (bFoundDigit)
   } // if (FormPref->ShowMultipleDirectoriesForm)

   FileClipFileNameEditExit(nullptr);

   // HACK part 3: restore the overrides at exit
   if (restoreTimecodeOverrideAtExit)
   {
      FileClipInTCEdit->tcP = overrideTC;
      FileClipDFCheckBox->Checked = overrideDF;
      FileClipFPSComboBox->ItemIndex = overrideFPS;
   }
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::BrowseForFileName(TEdit *FolderEdit, TEdit *FileEdit, const string &Filter, int FilterIndexArg)
{
   bool retVal = false;

   OpenDialog->InitialDir = FolderEdit->Text;
   OpenDialog->FileName = "";
   OpenDialog->Filter = Filter.c_str();
   OpenDialog->FilterIndex = FilterIndexArg;

   // TBD Load Default extension
   bool openResult = OpenDialog->Execute();
   if (openResult)
   {
      AnsiString newFolder = String(GetFilePath(StringToStdString(OpenDialog->FileName)).c_str());
      AnsiString newFile = (GetFileName(StringToStdString(OpenDialog->FileName)) + GetFileExt(StringToStdString(OpenDialog->FileName)))
            .c_str();

      // As way of example, the following comments presume the file:
      // f:\images\reel1\scene1\abc.00001.dpx was selected.

      // if (FolderEdit->Text != "" && FileEdit->Text != "")
      {
         if (FolderEdit->Text != newFolder)
         {
            // Set the directory (e.g. f:\images\reel1\scene1\)
            FolderEdit->Text = newFolder;
            retVal = true;
         }

         if (FileEdit->Text != newFile)
         {
            // Set to (e.g. abc.00001.dpx)
            FileEdit->Text = newFile;
            retVal = true;
         }
      }
   } // if (OpenDialog->Execute())

   return retVal;
} /* NameBrowserButtonClick() */
// ---------------------------------------------------------------------------

string TBinManagerForm::MakeClipFileName(TEdit *folderEdit, TEdit *filenameEdit)
{
   return AddDirSeparator(StringToStdString(folderEdit->Text)) + StringToStdString(filenameEdit->Text);
}

void __fastcall TBinManagerForm::InOutFileClipEditExit(TObject *Sender)
{
   int iIn, iOut;
   MTIistringstream is;

   // Get the in and out points
   is.str(StringToStdString(InFileClipEdit->Text));
   is >> iIn;

   is.clear();
   is.str(StringToStdString(OutFileClipEdit->Text));
   is >> iOut;

   // Set the duration (inclusive)
   int frameCount = iOut - iIn + 1;
   if (frameCount < 0 || iOut < 0 || iIn < 0)
   {
      frameCount = 0;
   }
   MTIostringstream os;
   os << std::setw(_ClipFileDigitPrecision) << std::setfill('0') << frameCount;
   DurFileClipEdit->Text = os.str().c_str();

   // And all the others
   os.str("");
   os << std::setw(_ClipFileDigitPrecision) << std::setfill('0') << ((iIn < 0) ? 0 : iIn);
   InFileClipEdit->Text = os.str().c_str();

   os.str("");
   os << std::setw(_ClipFileDigitPrecision) << std::setfill('0') << ((iOut < 0) ? 0 : iOut);
   OutFileClipEdit->Text = os.str().c_str();

   if (frameCount > 0)
   {
      CreateFileClipButton->Enabled = true;
      DurFileClipEdit->Font->Color = InFileClipEdit->Font->Color;
      FileDetailsProxyFrame->ShowFrame(FileDisplayer, iIn);
      iImageFileIndex = iIn;
      UpdateFileDetailsPanel(sImageFileNameTemplate, iIn);
   }
   else
   {
      CreateFileClipButton->Enabled = false;
      DurFileClipEdit->Font->Color = clRed;
      FileDetailsProxyFrame->Reset();
      UpdateFileDetailsPanel("", 0);
   }

}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileClipFileNameEditExit(TObject *Sender)
{
   CClipInitInfo clipInitInfo;
   MTIostringstream os;
   MTIistringstream is;

   // Do nothing if we have not changed
   if (!_bClipFileNameChange)
   {
      return;
   }

   _bClipFileNameChange = false;

   // Update the "timecodes"
   string sImageFileName = MakeClipFileName(FileClipFolderEdit, FileClipFileNameEdit);

   // The caller's image file name may be a standard file name, such
   // as C:\Images\Test.0001.dpx or it may be a template with #
   // characters that indicate where the frame digits should go, such
   // as C:\Images\Test.####.dpx.  Determine whether sImageFileName is
   // a template or a regular file name and then make either the regular
   // file name or the template
   int inFrameNumber, outFrameNumber;

   if (CImageFileMediaAccess::IsTemplate(sImageFileName))
   {
      // sImageFileName is a template, so make a regular file name
      // with the current In Frame number or 0 if that file doesn't exist
      sImageFileNameTemplate = sImageFileName;
      // Get the in point
      is.clear();
      is.str(StringToStdString(InFileClipEdit->Text));
      is >> inFrameNumber;
      string tmpNameIn;
      // Try current In Frame number
      tmpNameIn = CImageFileMediaAccess::GenerateFileName2(sImageFileNameTemplate, inFrameNumber);
      if (DoesFileExist(tmpNameIn))
      {
         sImageFileName = tmpNameIn; // file name with current In Frame
      }
      else
      {
         // Try frame = 0
         string tmpName0;
         tmpName0 = CImageFileMediaAccess::GenerateFileName2(sImageFileNameTemplate, 0);
         if (DoesFileExist(tmpName0))
         {
            sImageFileName = tmpName0; // file name with frame = 0
         }
         else
         {
            sImageFileName = tmpNameIn;
         }
      }
   }
   else
   {
      // sImageFileName contains a regular file name, so attempt to
      // figure out where the frame digits are located and create
      // a template.
      sImageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(sImageFileName, false);
   }

   // Put the template name into the File Name entry field so the user
   // can see where the Project Manager thinks the frame number is.  This
   // gives the user a chance to put the frame number digits somewhere
   // else if the code guessed the wrong location.
   if (!sImageFileNameTemplate.empty())
   {
      FileClipFileNameEdit->Text = (GetFileName(sImageFileNameTemplate) + GetFileExt(sImageFileNameTemplate)).c_str();
   }
   else
   {
      // Could not parse the user's file name to create a proper template.
      // Put the user's original name back in the File Name entry field
      FileClipFileNameEdit->Text = (GetFileName(sImageFileName) + GetFileExt(sImageFileName)).c_str();

      // TTT: Should we put up a warning dialog or beep or would this just
      // be annoying while the user is typing the complete file name
      // or template?
   }

   if (DoesFileExist(sImageFileName))
   {
      // A sample image file exists, so determine the minimum and
      // maximum frame numbers
      int startFrameNumber = CImageFileMediaAccess::getFrameNumberFromFileName2(sImageFileNameTemplate, sImageFileName);
      if (startFrameNumber == -1)
      {
         // FAIL
         _MTIErrorDialog(Handle, "Invalid file name format");
         inFrameNumber = -1;
         outFrameNumber = -1;
      }
      else
      {
         CImageFileMediaAccess::FindAllFiles2(sImageFileNameTemplate, startFrameNumber, inFrameNumber, outFrameNumber);

         if (inFrameNumber == -1 || outFrameNumber == -1)
         {
            MTIostringstream os;
            os << "Image file folder scan failed -" << endl << "please enter first and last frame numbers    ";
            _MTIErrorDialog(Handle, os.str());
         }
      }

      _ClipFileDigitPrecision = CImageFileMediaAccess::GetDigitCount(sImageFileNameTemplate);

      // Set the in and out frame numbers that were just discovered
      // in the First and Last fields
      InFileClipEdit->Text = inFrameNumber;
      OutFileClipEdit->Text = outFrameNumber;

      // first frame proxy
      FileDisplayer->SetSourceName(sImageFileNameTemplate);
      FileDetailsProxyFrame->Reset();
      FileDetailsProxyFrame->ShowFrame(FileDisplayer, inFrameNumber);
      iImageFileIndex = inFrameNumber;
      UpdateFileDetailsPanel(sImageFileNameTemplate, iImageFileIndex);
   }
   else
   {
      // The sample image file doesn't exist, but we still want to
      // know how many digits are in the frame number
      _ClipFileDigitPrecision = CImageFileMediaAccess::GetDigitCount(sImageFileNameTemplate);

      InFileClipEdit->Text = 0;
      OutFileClipEdit->Text = 10;
      InOutFileClipEditExit(nullptr);

      // FileClipTCCheckBox->Checked = false;           // Bug 2524
      FileClipInTCEdit->tcP = CTimecode(1, 0, 0, 0, 0, 100);
      FileClipDFCheckBox->Checked = false;
      FileClipFPSComboBox->ItemIndex = 0;

      FileDetailsProxyFrame->Reset();
      UpdateFileDetailsPanel("", 0);

      UpdateFileDetailsPanel("");
      FileDetailsTrackBar->Enabled = false;

      return; // ??? really want to skip clipInitInfo init?? qqq
   }

   // Fix everything up
   InOutFileClipEditExit(nullptr);

   // Set the clip init info from the contents of the clip initialization file
   string newClipSchemeName = StringToStdString(ExistingMediaFormatComboBox->Text);
   // ClipSchemePopupBox->Text.c_str();
   int retVal = clipInitInfo.initFromClipScheme(newClipSchemeName);

   if (retVal != 0)
   {
      os << "ERROR: Cannot parse Clip Scheme File " << newClipSchemeName << "   Return Code: " << retVal << endl;
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Attempt to initialize the Clip and Main Video Init Info
   // from the header of the Image File
   retVal = clipInitInfo.initFromImageFile2(sImageFileNameTemplate, inFrameNumber, outFrameNumber + 1, false, newClipSchemeName, true);

   if (retVal == CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME)
   {
      // The image file and clip schemes are not compatible
      _MTIErrorDialog(Handle, theError.getMessage());
      return;
   }
   else if (retVal == FORMAT_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED)
   {
      os << "Sorry, but you are not licensed for this resolution" << "To upgrade, please contact your " << "reseller or MTI." << endl;

      _MTIErrorDialog(Handle, os.str());
      return;
   }
   else if (retVal != 0)
   {
      int theErrorCode = theError.getError();
      string theErrorMessage = theErrorCode ? theError.getMessage() : "";
      os << "ERROR: Cannot initialize from media file " << sImageFileName << endl;
      if (theErrorCode && !theErrorMessage.empty())
      {
         os << theErrorMessage << " {" << theErrorCode << ")" << endl;
      }

      os << "Return Code: " << retVal << endl;

      _MTIErrorDialog(Handle, os.str());
      return;
   }

   FileClipNameEdit->Text = clipInitInfo.getClipName().c_str();

   CTimecode tc(clipInitInfo.getInTimecode());
   int fps = tc.getFramesPerSecond();

   if (fps > 0)
   {
      // FileClipTCCheckBox->Checked = true;           // Bug 2524
      FileClipInTCEdit->tcP = tc;
      FileClipDFCheckBox->Checked = tc.isDropFrame();
      FileClipFPSComboBox->ItemIndex = (fps == 25) ? 1 : ((fps == 30) ? 2 : 0);
   }

   FileDetailsTrackBar->Enabled = true;
   FileDetailsTrackBar->Position = 0;
   FileDetailsTrackBarChange(nullptr);

   _bClipFileNameChange = false;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::DurFileClipEditExit(TObject *Sender)
{
   int iIn, iOut, iDur;
   MTIistringstream is;

   // Get the in point and duration
   is.str(StringToStdString(InFileClipEdit->Text));
   is >> iIn;

   is.clear();
   is.str(StringToStdString(DurFileClipEdit->Text));
   is >> iDur;

   // <= 0 is not allowed
   if (iDur <= 0)
   {
      iDur = 1;
      DurFileClipEdit->Text = AnsiString(iDur);
   }

   // Set the out point (inclusive)
   OutFileClipEdit->Text = iDur + iIn - 1;

   InOutFileClipEditExit(nullptr);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileClipFileNameEditChange(TObject *Sender)
{
   _bClipFileNameChange = true;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CreateFileClipButtonClick(TObject *Sender)
{
   int inFrameNumber, outFrameNumber;
   MTIistringstream is;
   MTIostringstream os;

   // Get the in and out points
   is.str(StringToStdString(InFileClipEdit->Text));
   is >> inFrameNumber;

   is.clear();
   is.str(StringToStdString(OutFileClipEdit->Text));
   is >> outFrameNumber; // inclusive out

   CClipInitInfo clipInitInfo;
   int retVal;

   // Set the clip init info from the contents of the clip initialization file
   string newClipSchemeName = StringToStdString(ExistingMediaFormatComboBox->Text);
   // ClipSchemePopupBox->Text.c_str();
   retVal = clipInitInfo.initFromClipScheme(newClipSchemeName);
   if (retVal != 0)
   {
      os << "ERROR: Cannot parse Clip Scheme File " << newClipSchemeName << "   Return Code: " << retVal << endl;
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   CBusyCursor Busy(true);

   // Attempt to initialize the Clip and Main Video Init Info
   // from the header of the Image File
   bool bBlackOut = false;
   bool useFileTimecode = FileClipTCCheckBox->Checked;

   string sImageFileNameTemplate = MakeClipFileName(FileClipFolderEdit, FileClipFileNameEdit);
   // bug 2465: changed "true" to "false" here to dispense with the
   // time-consuming check of all the file headers for consistency
   retVal = clipInitInfo.initFromImageFile2(sImageFileNameTemplate, inFrameNumber, outFrameNumber + 1, false, newClipSchemeName,
         useFileTimecode);
   if (retVal == CLIP_ERROR_IMAGE_FILE_MISSING)
   {
      // First image file is not available, so check the others
      // If none exist, then assume the user wants the Image Files
      // to be created.  If some exist, then treat this as an error

      // Verify that none of the files exist
      retVal = clipInitInfo.checkNoImageFiles2(sImageFileNameTemplate, inFrameNumber, outFrameNumber + 1);
      if (retVal != 0)
      {
         os << "ERROR: Some Image Files are missing, some already exist" << endl << "       Cannot determine user's intention" << endl;
         _MTIErrorDialog(Handle, os.str());
         return;
      }

      bBlackOut = true;
   }
   else if (retVal == CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME || retVal == CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER)
   {
      // the image files and clip scheme are not compatible
      // or an image file was inconsistent with the first file's header
      _MTIErrorDialog(Handle, theError.getMessage());
      return;
   }
   else if (retVal != 0)
   {
      os << "ERROR: Cannot initialize from Image File " << sImageFileNameTemplate << endl << "       Return Code: " << retVal << endl;

      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Set the Bin Path from the Bin selected in the Bin Tree
   clipInitInfo.setBinPath(lastClickedBinPath);

   // Set timecode info if indicated
   // -- QQQ MAY NOT BE NECESSARY  done in initFromImageFile2 above
   if (FileClipTCCheckBox->Checked)
   {
      int flags = FileClipDFCheckBox->Checked ? DROPFRAME : 0;
      MTIistringstream fpss;
      int fps;

      fpss.str(StringToStdString(FileClipFPSComboBox->Text));
      fpss >> fps;

      CTimecode tc(flags, fps);
      string tcAscii;
      FileClipInTCEdit->tcP.getTimeString(tcAscii);
      tc.setTimeASCII(tcAscii.c_str());
      clipInitInfo.setInTimecode(tc);
      clipInitInfo.setOutTimecode(tc + (outFrameNumber - inFrameNumber + 1));
   }

   // Set the Clip Name from user's Clip Name entry field
   string strClipName;
   if (NewClipNewMediaPanel->Visible)
   {
      strClipName = StringToStdString(VideoClipNameEdit->Text);
   }
   else
   {
      strClipName = StringToStdString(FileClipNameEdit->Text);
   }

   clipInitInfo.setClipName(strClipName);

   // Create the new clip
   CBinManager binMgr;
   auto clip = binMgr.createClip(clipInitInfo, &retVal);
   if (retVal != 0)
   {
      ReportCreateClipError(retVal);

      return;
   }

   if (bBlackOut)
   {
      BlackOut(clip);
   }

   binMgr.setAutoClipName(lastClickedBinPath, strClipName);
   SetDefaultClipName(); // increment the number in the clip name
   binMgr.setAutoClipScheme(lastClickedBinPath, newClipSchemeName);
   binMgr.setAutoDiskScheme(lastClickedBinPath, StringToStdString(FileClipFolderEdit->Text));

   // Update the Clip List
   AddClipToClipListView(clip);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ImportTimecodesItemClick(TObject *Sender)
{
   if (ImportTCDialog == nullptr)
   {
      ImportTCDialog = new TImportTCDialog(this);
      CIniFile *ini = CreateIniFile(GetIniFileName());
      if (ini == nullptr)
      {
         _MTIErrorDialog(Handle, (string)"Could not open inifile " + theError.getMessage());
         return;
      }

      // Now read the settings
      ImportTCDialog->ReadSettings(ini, GetIniSection());
      DeleteIniFile(ini);
   }

   // (Fix bug 2318) Set bin default clip name because that's where the
   // "multibrowse" dialog gets the name.
   string defaultClipName = StringToStdString(VideoClipNameEdit->Text.Trim());
   if (!defaultClipName.empty())
   {
      CBinManager binMgr;
      binMgr.setAutoClipName(lastClickedBinPath, defaultClipName);
   }

   ShowModalDialog(ImportTCDialog);

   RefreshClipList();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ExportSelectedClipInfoMenuItemClick(TObject *Sender)
{
   if (ExportTCDialog == nullptr)
   {
      ExportTCDialog = new TExportTCDialog(this);
      CIniFile *ini = CreateIniFile(GetIniFileName());
      if (ini == nullptr)
      {
         _MTIErrorDialog(Handle, (string)"Could not open inifile " + theError.getMessage());
         return;
      }

      // Now read the settings
      ExportTCDialog->ReadSettings(ini, GetIniSection());
      DeleteIniFile(ini);
   }

   StringList ClipList;

   // Get either the selected clips or all of them
   if (Sender == ExportAllClipInfoMenuItem)
   {
      GetAllClips(ClipList);
   }
   else
   {
      GetSelectedClips(ClipList);
   }

   if (ClipList.size() == 0)
   {
      _MTIErrorDialog(Handle, "No Clips Selected");
      return;
   }

   ExportTCDialog->Execute(ClipList);
}
// ---------------------------------------------------------------------------

// Enable Properties Entry in PopUp menu if any clips selected
void __fastcall TBinManagerForm::ClipListPopupMenuPopup(TObject *Sender)
{
   int selectCount = CountSelectedClips();
   bool exactlyOneClipIsSelected = selectCount == 1;
   bool oneOrMoreClipsAreSelected = selectCount > 0;

   ClipPopupOpenClipItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupCreateNewVersionItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupDeleteItem->Enabled = oneOrMoreClipsAreSelected;
   ClipPopupRenameItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupBrowseToFolderItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupUpdateCutsItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupShowHideAlphaMatteItem->Enabled = oneOrMoreClipsAreSelected;
   ClipPopupEditCommentsItem->Enabled = oneOrMoreClipsAreSelected;
   ClipPopupCountNumberOfFixesItem->Enabled = oneOrMoreClipsAreSelected;
   ClipPopupImportImageFilesItem->Enabled = exactlyOneClipIsSelected;
   ClipPopupExportImageFilesItem->Enabled = exactlyOneClipIsSelected;

   // Huh?? QQQ
   VersionPopupCommitVersionMenuItem->Enabled = true;
   VersionPopupExportVersionMenuItem->Enabled = true;
}

// ---------------------------------------------------------------------------
void __fastcall TBinManagerForm::FileMenuBarItemClick(TObject *Sender)
{
   TTreeNode *rootNode = BinTree->Items->Item[0];
   bool rootNodeContext = BinTree->Selected == rootNode;
   ////bool jobNodeContext = BinTree->Selected->Parent == rootNode;
   FileMenuCreateJobItem->Enabled = true;
   FileMenuCreateBinItem->Enabled = true;
   FileMenuDeleteBinItem->Enabled = !rootNodeContext;
   FileMenuRenameBinItem->Enabled = !rootNodeContext;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::EditMainBarItemClick(TObject *Sender)
{
   // placeholder
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ViewMenuBarItemClick(TObject *Sender)
{
	TViewCreateClipItem_REMOVED_->Checked = true; //UnexpandedNewClipPanel->Visible == false;
   TViewProxyDisplayItem->Checked = ProxyDisplayPanel->Visible;
}
// ---------------------------------------------------------------------------

// Enable Properties Entry in pulldown menu if any clips selected
void __fastcall TBinManagerForm::ClipMenuBarItemClick(TObject *Sender)
{
   bool IsLicensed = true; // _License.IsFeatureLicensed(FEATURE_CONTROL_IO);
   bool aClipIsSelected = (CountSelectedClips() > 0);

   PropertiesItem->Enabled = aClipIsSelected;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ClearProxy()
{
   CBinManager binMgr;

   // Close the clip if necessary
   if (clipProxy != nullptr)
   {
      binMgr.closeClip(clipProxy);
      clipProxy = nullptr;
   }

   ImageProxy->Visible = false;
}

void TBinManagerForm::DisplayProxy(bool Recreate)
{
   if (clipProxy == nullptr || uipProxy == nullptr)
   {
      return;
   }

   if ((TrackBarProxy->Position != iStoredPosition) || Recreate)
   {

      if (vflpProxy == 0)
      {
         // get video list pointer
         vflpProxy = clipProxy->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
         if (vflpProxy == 0)
         {
            MTIostringstream ostr;
            ostr << "Unable to preview clip";
            _MTIErrorDialog(Handle, ostr.str());
            ClearProxy();
            return;
         }

         // force clip to load
         Screen->Cursor = crHourGlass;
         vflpProxy->getTotalFrameCount();
         Screen->Cursor = crDefault;
      }

      // allocate storage to hold image
      MTI_UINT8 *ucp[2];
      ucp[0] = 0;
      ucp[1] = 0;
      for (int iField = 0; iField < vflpProxy->getImageFormat()->getFieldCount(); iField++)
      {
         // allocate on MTI_MEMALIGN_DEFAULT boundary for StorNext
         ucp[iField] = (MTI_UINT8*) MTImemalign(MTI_MEMALIGN_DEFAULT, vflpProxy->getMaxPaddedFieldByteCount());
         if (ucp[iField] == 0)
         {
            _MTIErrorDialog(Handle, "Unable to allocate storage for proxy display");
            MTIfree(ucp[0]);
            MTIfree(ucp[1]);
            ClearProxy();
            return;
         }
      }

      // Which image to display
      int iFrame = ((float)TrackBarProxy->Position / (float)TrackBarProxy->Max) * (float)(vflpProxy->getUserFrameCount() - 1) + 0.5;
      iFrame += vflpProxy->getInFrameIndex();

      // read in the image
      int iRet = vflpProxy->readMediaVisibleFields(iFrame, ucp);
      if (iRet)
      {
         MTIostringstream ostr;
         ostr << "Unable to read image data from clip.  Error code: " << iRet;
         TRACE_0(errout << ostr.str());
         MTIfree(ucp[0]);
         MTIfree(ucp[1]);
         ClearProxy();
         return;
      }

      // convert the image.  Avoid field interlace problems by using same field
      // twice
      MTI_UINT8 *ucpLocal[2];
      ucpLocal[0] = ucp[0];
      ucpLocal[1] = ucp[0];
      RECT rect = vflpProxy->getImageFormat()->getActivePictureRect();
      cnvtProxy->convertWtf(ucpLocal, vflpProxy->getImageFormat(), &rect, uipProxy, ImageProxy->Width, ImageProxy->Height,
            ImageProxy->Width * 4);
      CTimecode tc = vflpProxy->getTimecodeForFrameIndex(iFrame);
      tc.getTimeString(strStoredTimecode);

      // free storage
      MTIfree(ucp[0]);
      MTIfree(ucp[1]);

      SaveProxy();
   }

   // display the image
   MTI_UINT32 pixel;
   Graphics::TBitmap *DataBitMap = new Graphics::TBitmap;
   MTI_UINT32 *uip;

   DataBitMap->PixelFormat = pf24bit;
   DataBitMap->Height = ImageProxy->Height;
   DataBitMap->Width = ImageProxy->Width;

   for (int r = 0; r < DataBitMap->Height; r++)
   {
      TRGBTriple *ptr = (TRGBTriple*)DataBitMap->ScanLine[r];
      uip = &uipProxy[r * DataBitMap->Width];
      for (int c = 0; c < DataBitMap->Width; c++)
      {
         pixel = *uip++;
         ptr->rgbtRed = (pixel >> 16) & 0xFF;
         ptr->rgbtGreen = (pixel >> 8) & 0xFF;
         ptr->rgbtBlue = pixel & 0xFF;
         ptr++;
      }
   }

   ImageProxy->Canvas->Draw(0, 0, DataBitMap);
   if (ShowProxyTimecodeMenuItem->Checked)
   {
      ImageProxy->Canvas->Brush->Style = bsClear; // Wtf - transparent bkgrnd ?!
      ImageProxy->Font->Color = clBlack;
      TFont *font = new TFont;
      font->Assign(ImageProxy->Canvas->Font);
      font->Color = clBlack;
      ImageProxy->Canvas->Font->Assign(font);
      ImageProxy->Canvas->TextOut(30, 90, strStoredTimecode.c_str());
      ImageProxy->Canvas->TextOut(29, 89, strStoredTimecode.c_str());
      font->Color = clLime;
      ImageProxy->Canvas->Font->Assign(font);
      ImageProxy->Canvas->TextOut(28, 88, strStoredTimecode.c_str());
      delete font;
   }

   delete DataBitMap;
}

void TBinManagerForm::SaveProxy()
{
   iStoredPosition = -1;

   if (strStoredProxy.empty())
   {
      return;
   }

   int iFD = open(strStoredProxy.c_str(), O_WRONLY | O_TRUNC | O_CREAT | O_BINARY, S_IRUSR | S_IWUSR);
   if (iFD < 0)
   {
      TRACE_0(errout << "INFO: SaveProxy: Can't open cache file " << strStoredProxy << " for writing" << endl << "      Reason: " <<
            GetSystemMessage(errno));
      return;
   }

   if (write(iFD, "MTI proxy display image\n", 24) == -1)
   {
      close(iFD);
      return;
   }

   int iPosition, iHeight, iWidth;
   iPosition = TrackBarProxy->Position;
   iHeight = ImageProxy->Height;
   iWidth = ImageProxy->Width;
   if (write(iFD, &iPosition, 4) == -1)
   {
      close(iFD);
      return;
   }

   if (write(iFD, &iHeight, 4) == -1)
   {
      close(iFD);
      return;
   }

   if (write(iFD, &iWidth, 4) == -1)
   {
      close(iFD);
      return;
   }

   if (write(iFD, uipProxy, sizeof(MTI_UINT32) * ImageProxy->Height * ImageProxy->Width) == -1)
   {
      close(iFD);
      return;
   }

   iStoredPosition = TrackBarProxy->Position;
   close(iFD);
   return;
}

void TBinManagerForm::LoadProxy()
{
   iStoredPosition = -1;
   TrackBarProxy->Position = 0;

   if (uipProxy == nullptr)
   {
      return;
   }

   if (strStoredProxy.empty())
   {
      return;
   }

   int iFD = open(strStoredProxy.c_str(), O_RDONLY | O_BINARY);
   if (iFD < 0)
   {
      TRACE_0(errout << "INFO: LoadProxy: Can't open cache file " << strStoredProxy << " for reading" << endl << "      Reason: " <<
            GetSystemMessage(errno));
      return;
   }

   if (lseek(iFD, 24, SEEK_SET) == -1)
   {
      close(iFD);
      return;
   }

   int iPosition, iHeight, iWidth;
   iPosition = TrackBarProxy->Position;
   iHeight = ImageProxy->Height;
   iWidth = ImageProxy->Width;
   if (read(iFD, &iPosition, 4) == -1)
   {
      close(iFD);
      return;
   }
   if (read(iFD, &iHeight, 4) == -1)
   {
      close(iFD);
      return;
   }
   if (read(iFD, &iWidth, 4) == -1)
   {
      close(iFD);
      return;
   }

   if (iHeight != (int)ImageProxy->Height || iWidth != (int)ImageProxy->Width)
   {
      close(iFD);
      return;
   }

   if (read(iFD, uipProxy, sizeof(MTI_UINT32) * ImageProxy->Height * ImageProxy->Width) == -1)
   {
      close(iFD);
      return;
   }

   close(iFD);
   iStoredPosition = iPosition;
   TrackBarProxy->Position = iPosition;
   return;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TrackBarProxyChange(TObject *Sender)
{
   ImageProxy->Invalidate();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ImageProxyPaint(TObject *Sender)
{
   DisplayProxy();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CheckBoxCentralizeClick(TObject *Sender)
{
   if (CheckBoxCentralize->Checked)
   {
      PanelCentral->Visible = true;
      PanelInOut->Visible = false;
   }
   else
   {
      PanelCentral->Visible = false;
      PanelInOut->Visible = true;
   }

   UpdateTabOrder();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::UpdateTabOrder()
{
   // turn off tab stop on all items
   VTimeCodeEditIn->TabStop = false;
   VTimeCodeEditOut->TabStop = false;
   VTimeCodeEditDur->TabStop = false;
   VTimeCodeEditCen->TabStop = false;
   VideoClipNameEdit->TabStop = false;
   FileClipNameEdit->TabStop = false;
   CreateVideoClipButton->TabStop = false;
   InFileClipEdit->TabStop = false;
   OutFileClipEdit->TabStop = false;
   DurFileClipEdit->TabStop = false;
   CreateFileClipButton->TabStop = false;

   // depending on the mode, re-enable some tab stops
   int iTabOrder = 0;
   if (NewClipNewMediaPanel->Visible)
   {
      if (PanelInOut->Visible)
      {
         VTimeCodeEditIn->TabStop = FormPref->TabVideoIn1();
         VTimeCodeEditIn->TabOrder = iTabOrder++;
         VTimeCodeEditOut->TabStop = FormPref->TabVideoOut1();
         VTimeCodeEditOut->TabOrder = iTabOrder++;
         VTimeCodeEditDur->TabStop = FormPref->TabVideoDur1();
         VTimeCodeEditDur->TabOrder = iTabOrder++;
         VideoClipNameEdit->TabStop = FormPref->TabVideoName1();
         VideoClipNameEdit->TabOrder = iTabOrder++;
         CreateVideoClipButton->TabStop = FormPref->TabVideoCreate1();
         CreateVideoClipButton->TabOrder = iTabOrder++;
      }
      else
      {
         VTimeCodeEditCen->TabStop = FormPref->TabVideoCen2();
         VTimeCodeEditCen->TabOrder = iTabOrder++;
         VTimeCodeEditDur->TabStop = FormPref->TabVideoDur2();
         VTimeCodeEditDur->TabOrder = iTabOrder++;
         VideoClipNameEdit->TabStop = FormPref->TabVideoName2();
         VideoClipNameEdit->TabOrder = iTabOrder++;
         CreateVideoClipButton->TabStop = FormPref->TabVideoCreate2();
         CreateVideoClipButton->TabOrder = iTabOrder++;
      }
   }
   else
   {
      InFileClipEdit->TabStop = FormPref->TabFileIn1();
      InFileClipEdit->TabOrder = iTabOrder++;
      OutFileClipEdit->TabStop = FormPref->TabFileOut1();
      OutFileClipEdit->TabOrder = iTabOrder++;
      DurFileClipEdit->TabStop = FormPref->TabFileDur1();
      DurFileClipEdit->TabOrder = iTabOrder++;
      FileClipNameEdit->TabStop = FormPref->TabFileName1();
      FileClipNameEdit->TabOrder = iTabOrder++;
      CreateFileClipButton->TabStop = FormPref->TabFileCreate1();
      CreateFileClipButton->TabOrder = iTabOrder++;
   }

   ChooseFolderRadioButton->TabStop = true;
   ChooseFolderRadioButton->TabOrder = iTabOrder++;
   ClipFolderBrowserButton->TabStop = true;
   ClipFolderBrowserButton->TabOrder = iTabOrder++;
   ChooseFileRadioButton->TabStop = true;
   ChooseFileRadioButton->TabOrder = iTabOrder++;
   ClipFileBrowserButton->TabStop = true;
   ClipFileBrowserButton->TabOrder = iTabOrder++;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::FixCreateClipPanelSize()
{
//	// New stuff... mainly all because I moved the proxy up into the bin panel,
//	// and frickin' "autosize" doesn't work as expected now...
//	ModalPanel->Top = 0;
//
//	if (false) //UnexpandedNewClipPanel->Visible)
//	{
//		CreateClipPanel->Height = UnexpandedNewClipPanel_REMOVED_->Top + UnexpandedNewClipPanel_REMOVED_->Height;
//		CreateClipPanel->Width = BottomPanel->Width;
//	}
//	else if (GroupBoxNewClip->Visible)
//   {
//      CreateClipPanel->Height = GroupBoxNewClip->Top + GroupBoxNewClip->Height + 8;
//		CreateClipPanel->Width = BottomPanel->Width;
//	}
//   else if (GroupBoxLicense->Visible)
//	{
//      CreateClipPanel->Height = GroupBoxLicense->Top + GroupBoxLicense->Height + 8;
//      CreateClipPanel->Width = GroupBoxLicense->Left + GroupBoxLicense->Width + 20;
//	}
//   else if (GroupBoxProjectScanList->Visible)
//	{
//      CreateClipPanel->Height = GroupBoxProjectScanList->Top + GroupBoxProjectScanList->Height + 8;
//      CreateClipPanel->Width = GroupBoxProjectScanList->Left + GroupBoxProjectScanList->Width + 20;
//   }
//
//   // Position and set width of the modal panel, in case it's active
//	ModalPanel->Top = CreateClipPanel->Top + CreateClipPanel->Height + 2;
//   if (VideoStoreUsagePanel->Visible)
//	{
//      ModalPanel->Width = VideoStoreUsagePanel->Left + VideoStoreUsagePanel->Width;
//   }
//	else
//   {
//		ModalPanel->Width = CreateClipPanel->Width;
//   }
//
//   // Fix the size of the bottom panel, which wraps the ClipCreatePanel
//	// and the ModalPanel
//   if (ModalPanel->Visible)
//   {
//		BottomPanel->Height = ModalPanel->Top + ModalPanel->Height;
//	}
//   else
//	{
//		BottomPanel->Height = CreateClipPanel->Top + CreateClipPanel->Height;
//	}
//
//   // Set the "spacer" label height to keep the bottom panel from
//	// collapsing, then make sure the bottom panel is aligned with it
//	// (the spacer has the align=bottom property set -- it can't be set
//	// on the bottom panel itself because it is incompatible with the
//	// autosize=true property)
//	BottomPanelSpacerLabel->Height = BottomPanel->Height;
//	BottomPanel->Top = BottomPanelSpacerLabel->Top;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::HackVideoStoreUsage()
{
	VideoStoreUsagePanel->Visible = false;
	////binMgrGUIInterface->GetVideoStoreUsagePanelFlag();
	if (VideoStoreUsagePanel->Visible)
   {
		// fix up video store usage panel
      // NOTE: the clip scheme popup now resides permanently on
		// VideoStoreUsagePanel so it is no longer reparented
      ReParent(MediaLocationComboBox, LocationComboBoxPositioningPanel2);
   }
   else
   {
      // Put the combo box and usage widget back on the clip creation panel
      ReParent(MediaLocationComboBox, LocationComboBoxPositioningPanel1);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBViewCreateClipItem_REMOVED_Click(TObject *Sender)
{
	if (true) //TViewCreateClipItem->Checked)
   {
		UnexpandCreateClipPanelButton_REMOVED_Click(Sender);
   }
   else
   {
//		ExpandCreateClipPanelButton_REMOVED_Click(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VTimeCodeChange(TObject *Sender)
{
   VTimeCodeEdit *tce = (VTimeCodeEdit*)Sender;

   if (tce == VTimeCodeEditIn)
   {
      if (tce->Tag == 0)
      {
         // calculate the duration
         CalcDuration();

         // set the small in value
         int iIn = VTimeCodeEditIn->tcP.getAbsoluteTime();
         SetVTimeCodeValue(VTimeCodeEditInSmall, iIn);

         // set the central value
         CalcCentral();

         // update file name "in" frame if it's tied to the time code
         FileVidClipTCIndexCheckBoxClick(nullptr);
      }
      else
      {
         // was set by software.  Consume the change
         // tce->Tag = 0;
      }
   }

   if (tce == VTimeCodeEditOut)
   {
      if (tce->Tag == 0)
      {
         // calculate the duration
         CalcDuration();

         // set the small out value
         int iOut = VTimeCodeEditOut->tcP.getAbsoluteTime();
         SetVTimeCodeValue(VTimeCodeEditOutSmall, iOut);

         // set the central value
         CalcCentral();
      }
      else
      {
         // was set by software.   Consume the change
         // tce->Tag = 0;
      }
   }

   if (tce == VTimeCodeEditDur)
   {
      if (tce->Tag == 0)
      {
         if (PanelCentral->Visible)
         {
            CalcInOut();
         }
         else
         {
            int iOut = VTimeCodeEditIn->tcP.getAbsoluteTime() + tce->tcP.getAbsoluteTime();
            SetVTimeCodeValue(VTimeCodeEditOut, iOut);
            SetVTimeCodeValue(VTimeCodeEditOutSmall, iOut);

            CalcCentral();
         }
      }
      else
      {
         // set by software.  Consume the change
         // tce->Tag = 0;
      }

      int iDur = VTimeCodeEditDur->tcP.getAbsoluteTime();
      if (iDur == 0)
      {
         VTimeCodeEditDur->Font->Color = clRed;
         CreateVideoClipButton->Enabled = false;
#if 0 // Bug 2573 - Don't do this because if the VTimecode component is
         // cleared temporarily, it can never get set back to 'recording'
         if (FillByRecordingRadioButton->Checked)
         {
            FillWithBlanksRadioButton->Checked = true;
         }
#endif // Bug 2573
      }
      else
      {
         VTimeCodeEditDur->Font->Color = VTimeCodeEditIn->Font->Color;
         CreateVideoClipButton->Enabled = true;
         FillByRecordingRadioButton->Enabled = true;
         // _License.IsFeatureLicensed(FEATURE_CONTROL_IO);
      }
   }

   if (tce == VTimeCodeEditInSmall)
   {
      if (tce->Tag == 0)
      {
         // set the large in value
         int iIn = VTimeCodeEditInSmall->tcP.getAbsoluteTime();
         SetVTimeCodeValue(VTimeCodeEditIn, iIn);

         // calculate the duration
         CalcDuration();

         // set the central value
         CalcCentral();
      }
      else
      {
         // was set by software.  Consume the change
         // tce->Tag = 0;
      }
   }

   if (tce == VTimeCodeEditOutSmall)
   {
      if (tce->Tag == 0)
      {
         // set the large out value
         int iOut = VTimeCodeEditOutSmall->tcP.getAbsoluteTime();
         SetVTimeCodeValue(VTimeCodeEditOut, iOut);

         // calculate the duration
         CalcDuration();

         // set the central value
         CalcCentral();
      }
      else
      {
         // was set by software.   Consume the change
         // tce->Tag = 0;
      }
   }

   if (tce == VTimeCodeEditCen)
   {
      if (tce->Tag == 0)
      {
         // calculate the duration
         CalcInOut();
      }
      else
      {
         // was set by software.   Consume the change
         // tce->Tag = 0;
      }

      int iCen = VTimeCodeEditCen->tcP.getAbsoluteTime();
      if (iCen == 0)
      {
         VTimeCodeEditCen->Font->Color = clRed;
      }
      else
      {
         VTimeCodeEditCen->Font->Color = VTimeCodeEditIn->Font->Color;
      }
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::CalcDuration()
{
   int iIn = VTimeCodeEditIn->tcP.getAbsoluteTime();
   int iOut = VTimeCodeEditOut->tcP.getAbsoluteTime();
   int iDur;

   if (iIn >= iOut)
   {
      iDur = 0;
   }
   else
   {
      iDur = iOut - iIn;
   }

   SetVTimeCodeValue(VTimeCodeEditDur, iDur);

}

void TBinManagerForm::CalcCentral()
{
   int iIn = VTimeCodeEditIn->tcP.getAbsoluteTime();
   int iOut = VTimeCodeEditOut->tcP.getAbsoluteTime();
   int iCenter;

   if (iIn >= iOut)
   {
      iCenter = 0;
   }
   else
   {
      iCenter = .5 * (iIn + iOut);
   }

   SetVTimeCodeValue(VTimeCodeEditCen, iCenter);
}

void TBinManagerForm::CalcInOut()
{
   int iCen = VTimeCodeEditCen->tcP.getAbsoluteTime();
   int iDur = VTimeCodeEditDur->tcP.getAbsoluteTime();
   int iIn = iCen - (iDur / 2);
   int iOut = iIn + iDur;

   SetVTimeCodeValue(VTimeCodeEditIn, iIn);
   SetVTimeCodeValue(VTimeCodeEditOut, iOut);
   SetVTimeCodeValue(VTimeCodeEditInSmall, iIn);
   SetVTimeCodeValue(VTimeCodeEditOutSmall, iOut);
}

int masterTag = 100;

void TBinManagerForm::SetVTimeCodeValue(VTimeCodeEdit *tce, int i)
{
   // value is being set by software.  Do not process the callback
   tce->Tag = 1;

   // set new value
   tce->tcP.setAbsoluteTime(i);

   // re-enable callbacks
   tce->Tag = 0;
}

void TBinManagerForm::SetVTimeCodeValue(VTimeCodeEdit *tce, const CTimecode &tc)
{
   // value is being set by software.  Do not process the callback
   tce->Tag = 1;

   // set new value
   tce->tcP = tc;

   // re-enable callbacks
   tce->Tag = 0;
}

void TBinManagerForm::SetVTimeCodeValue(VTimeCodeEdit *tce, const string &str)
{
   // value is being set by software.  Do not process the callback
   tce->Tag = 1;

   // set new value
   tce->tcP = str;

   // re-enable callbacks
   tce->Tag = 0;
}

void __fastcall TBinManagerForm::InOutFileClipEditKeyPress(TObject *Sender, char &Key)
{
   if (Key == '\r')
   {
      // ENTER was pressed
      InOutFileClipEditExit(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::DurFileClipEditKeyPress(TObject *Sender, char &Key)
{
   if (Key == '\r')
   {
      // ENTER was pressed
      DurFileClipEditExit(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewSelectItem(TObject *Sender, TListItem *Item, bool Selected)
{
   /*
    This event is poorly named... when multiselect is on, it doesn't
    actually get called for each selected clip, only the ones you actually
    click on
    */

   // mlm: to-do: this callback needs work.  The sender is not
   // necessarily the item being clicked.  The callback is also called
   // when items are unselected, as indicated by the Selected
   // parameter.  We need to make sure we do the right thing with
   // lastClickedBinPath and lastClickedClipPath.  It seems that
   // lastClickedBinPath may be correct because testing shows that
   // BinTreeChange is called before this callback.  However, setting
   // lastClickedClipPath based on the Item parameter is probably
   // wrong when Selected == false.  I've observed a crash when
   // changing bins and the original bin contained a Selected, marked
   // clip.
   // mbraca: I rewrote the callback, mainly because it doesn't work when
   // "multiselect" is on, so I now track selections in the OnChanged handler

   if (ComponentState.Contains(csDestroying))
   {
      // short-circuit if the form is being destroyed; bad things happen
      // when we try to access members in here because the destructor has
      // already been called when this event is fired.
      return;
   }

   // Because we have multiselection on and because Borland doesn't give
   // us selection events for each of the multidelected items when you
   // shift-click, we really have no idea at this time what is selected.
   selectedListIsInvalid = true;

   if (Item == nullptr)
   {
      // Sometimes item comes in as nullptr, I'm not exactly sure why
      return;
   }

   MTIassert((Item->Index >= 0) && (((unsigned) Item->Index) < CurrentBinClipListData->size()));
   SClipDataItem *itemData = CurrentBinClipListData->at(Item->Index);

   if (Selected && !itemData->selectStateChangedBySoftware)
   {
      lastClickedClipPath = AddDirSeparator(lastClickedBinPath) + StringToStdString(Item->Caption);

      // Show the proxy of the clip that was clicked on
      strStoredProxy = AddDirSeparator(lastClickedClipPath) + "proxy";
      ReloadProxy();
      RepaintVersionListView();
   }
}
// --------------CompareFileDates-----------------------------------------------------

int CompareFileDates(const string &F1, const string &F2)

      // This compares the modify dates of the files F1 and F2
      // return is  -1 if modify date of F1 < modify date of F2
      // 0  if modify date of F1 = modify date of F2
      // 1 if modify date of F1 > modify date of F2
      // < -1 if error stating the files
      //
      // Note:  Granularity is 1 second
      //
      // ***********************************************************************************
{
   struct stat F1StatBuf;
   struct stat F2StatBuf;

   if (stat(F1.c_str(), &F1StatBuf) != 0)
   {
      // Can't get stat for the F1 file, maybe it doesn't exist.
      return -2;
   }

   if (stat(F2.c_str(), &F2StatBuf) != 0)
   {
      // Can't get stat for the F2 file, maybe it doesn't exist.
      return -2;
   }

   // Compare the "modified time" of the two files
   // Since the granularity of the time stamp is 1 second, we use
   // greater than or equal to be certain we detect the newest file.
   if (F1StatBuf.st_mtime < F2StatBuf.st_mtime)
   {
      return -1;
   }
   if (F1StatBuf.st_mtime > F2StatBuf.st_mtime)
   {
      return 1;
   }
   return 0;
}

void TBinManagerForm::ReloadProxy(void)
{
   CBinManager binMgr;
   string Path;
   string clipName;

   int iRet = binMgr.splitClipFileName(lastClickedClipPath, Path, clipName);
   if (iRet != 0)
   {
      TRACE_0(errout << "ReloadProxy splitClipFileName failed with " << iRet);
      return;
   }

   // Most likely not necessary but someone might call this without closing proxy
   if (clipProxy != nullptr)
   {
      binMgr.closeClip(clipProxy);
   }

   clipProxy = binMgr.openClip(Path, clipName, &iRet);
   if (iRet)
   {
      ClearProxy();
      return;
   }

   vflpProxy = 0;

   // load the proxy
   LoadProxy();

   // create a proxy and display
   string F2 = Path + "\\" + clipName + "\\" + clipName + ".diff";
   DisplayProxy(CompareFileDates(strStoredProxy, F2) <= 0);

   if (clipProxy == nullptr)
   {
      return;
   } // an error occurred

   ImageProxy->Visible = true;

   // copy IN and OUT    // I believe this should not be undone for bug 2508
   // THIS IS BOGUS - let's just always set BOTH SETS OF TIMECODES
   // CVideoProxy *videoProxy = clipProxy->getVideoProxy(VIDEO_SELECT_NORMAL);
   // if (videoProxy != 0 && videoProxy->isMediaFromImageFile())
   {
      /* Hmm, actually, this whole section is bogus... */
      // PUT IT BACK: BUG 2508
      int iIn, iOut;
      iIn = clipProxy->getInTimecode().getAbsoluteTime(); // getFrames();
      iOut = clipProxy->getOutTimecode().getAbsoluteTime(); // getFrames();
      // if the clip is a file-based clip, we want to subtract 1 outframe... (see bug 909)
      iOut--;

      char caMessage[32];
      caMessage[31] = '\0';
      snprintf(caMessage, 31, "%d", iIn);
      InFileClipEdit->Text = caMessage;
      snprintf(caMessage, 31, "%d", iOut);
      OutFileClipEdit->Text = caMessage;
      InOutFileClipEditExit(nullptr);
      /* */   // BUG 2508
   }
   // else
   {
      /* Well, Amy claims this is bogus too... so sayonara */ // BUG 2508
      SetVTimeCodeValue(VTimeCodeEditIn, clipProxy->getInTimecode());
      SetVTimeCodeValue(VTimeCodeEditOut, clipProxy->getOutTimecode());
      SetVTimeCodeValue(VTimeCodeEditInSmall, clipProxy->getInTimecode());
      SetVTimeCodeValue(VTimeCodeEditOutSmall, clipProxy->getOutTimecode());
      // before doing a CalcDuration() and CalcCentral() make sure time codes are for right clipscheme
      UpdateTimecodesWithNewClipScheme(clipSchemeName);
      // ClipSchemePopupBox->Text.c_str());
      /* */   // BUG 2508
   }

   // do not delete clip!
}

// ---------------------------------------------------------------------------
// *** USE THIS INSTEAD OF BinManagerForm->FormStyle = fsWhatever  ***
// ***     BinManagerForm->SetFormStyle(fsWhatever);
void TBinManagerForm::SetFormStyle(TFormStyle style)
{
   SynchronizeSelectionsIfNecessary();
   ////////////////////////////////
   // Per Larry... always on top
   // FormStyle = style;
   //// now done at design time!
   ////////////////////////////////
}
// ---------------------------------------------------------------------------

// void TBinManagerForm::ShowImportExportDialog(ImportExportFormBase *form)
// {
// form->Run();
// }
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewKeyPress(TObject *Sender, char &Key)
{
   if (Key == Char(VK_RETURN))
   {
      OpenClipMenuItemOrDoubleClick(nullptr);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ImportImageFilesMenuItemClick(TObject *Sender)
{
   // int retVal                 = 0;
   // ImportExportFormBase *form = 0;
   // StringList ClipList;
   //
   // GetSelectedClips(ClipList);
   // if (ClipList.size() == 0)
   // {
   // _MTIErrorDialog(Handle, "No Clips selected");
   // return;
   // }
   // if (ClipList.size() > 1)
   // {
   // _MTIErrorDialog(Handle, "More than one Clip selected");
   // return;
   // }
   //
   // form = ImportExportFormBase::GetForm(ImportExportFormBase::IMPORT_FILES_DIALOG);
   // if (form != 0)
   // {
   // retVal = form->InitFromClipName(lastClickedBinPath, ClipList[0]);
   // if (retVal == 0)
   // {
   // ShowImportExportDialog(form);
   // }
   // else
   // {
   // MTIostringstream ostr;
   //
   // TRACE_0(errout << "ERROR: TBinManagerForm::ImportImageFilesMenuItemClick: call to" << endl <<
   // "       ImportExportBaseForm::InitFromClipIni failed, return code " << retVal);
   // ostr << "Error loading Clip info.  Error Code = " << retVal;
   // _MTIErrorDialog(Handle, ostr.str());
   // }
   // }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ExportImageFilesMenuItemClick(TObject *Sender)
{
   // int retVal                 = 0;
   // ImportExportFormBase *form = 0;
   // StringList ClipList;
   //
   // GetSelectedClips(ClipList);
   // if (ClipList.size() == 0)
   // {
   // _MTIErrorDialog(Handle, "No Clips selected");
   // return;
   // }
   // if (ClipList.size() > 1)
   // {
   // _MTIErrorDialog(Handle, "More than one Clip selected");
   // return;
   // }
   //
   // form = ImportExportFormBase::GetForm(ImportExportFormBase::EXPORT_FILES_DIALOG);
   // if (form != 0)
   // {
   // retVal = form->InitFromClipName(lastClickedBinPath, ClipList[0]);
   // if (retVal == 0)
   // {
   // ShowImportExportDialog(form);
   // }
   // else
   // {
   // MTIostringstream ostr;
   //
   // TRACE_0(errout << "ERROR: ExportImageFilesMenuItemClick::CreateClipButtonClick: call to" << endl <<
   // "       ImportExportBaseForm::InitFromClipIni failed, return code " << retVal);
   // ostr << "Error loading Clip info.  Error Code = " << retVal;
   // _MTIErrorDialog(Handle, ostr.str());
   // }
   // }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CenCueButtonClick(TObject *Sender)
{
   VTimeCodeEditCen->CueToVTR();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CenCopyButtonClick(TObject *Sender)
{
   VTimeCodeEditCen->CopyFromVTR();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::InCueButtonClick(TObject *Sender)
{
   VTimeCodeEditIn->CueToVTR();
}

// ---------------------------------------------------------------------------
namespace
{
enum TCTYPE
{
   TCTYPE_IN, TCTYPE_OUT, TCTYPE_DUR
};

// A code snippet to copy a timecode from a clip to a VTimecodeEdit control
void CopyTimecodeFromClipToVTimecodeEditControl(const string & clipName, VTimeCodeEdit *tcEdit, TCTYPE tcType)
{
   int retVal = 0;
   CBinManager bm;
   auto clip = bm.openClip(clipName, &retVal);
   if (retVal == 0 && clip != nullptr)
   {
      CClipInitInfo cii;
      retVal = cii.initFromClip(clip);
      if (retVal == 0)
      {
         if (tcType == TCTYPE_IN)
         {
            PTimecode ptc(cii.getInTimecode());
            tcEdit->tcP.setTime(ptc);
         }
         else if (tcType == TCTYPE_OUT)
         {
            PTimecode ptc(cii.getOutTimecode());
            tcEdit->tcP.setTime(ptc);
         }
         else // TCTYPE_DUR
         {
            // What a PAIN in th BUTT to get this right
            CTimecode inTC(cii.getInTimecode());
            CTimecode outTC(cii.getOutTimecode());
            CTimecode durTC(0);
            durTC.setFramesPerSecond(inTC.getFramesPerSecond());
            durTC.setAbsoluteTime(outTC - inTC);
            tcEdit->tcP.setTime(durTC);
         }
         tcEdit->UpdateDisplay(); // need this? QQQ
      }
      cii.closeClip();
   }
}
} // end local namespace

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::InCopyButtonClick(TObject *Sender)
{
   VTimeCodeEditIn->CopyFromVTR();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::OutCueButtonClick(TObject *Sender)
{
   VTimeCodeEditOut->CueToVTR();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::OutCopyButtonClick(TObject *Sender)
{
   VTimeCodeEditOut->CopyFromVTR();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::DurCopyButtonClick(TObject *Sender)
{
   if (!YellowHilitedMasterClip.empty())
   {
      CopyTimecodeFromClipToVTimecodeEditControl(YellowHilitedMasterClip, VTimeCodeEditDur, TCTYPE_DUR);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CheckCopyFromVTRTimerTimer(TObject *Sender)
{
#if 0 // Bug 2508
   if (FillByRecordingRadioButton->Checked)
   {
      CenCopyButton->Enabled = VTimeCodeEditCen->CopyFromVTRAvailable();
      CenCueButton->Enabled = VTimeCodeEditCen->CueFromVTRAvailable();

      InCopyButton->Enabled = VTimeCodeEditIn->CopyFromVTRAvailable();
      InCopyButton->Hint = "Copy from current VTR timecode";
      InCueButton->Enabled = VTimeCodeEditIn->CueFromVTRAvailable();

      OutCopyButton->Enabled = VTimeCodeEditOut->CopyFromVTRAvailable();
      OutCopyButton->Hint = "Copy from current VTR timecode";
      OutCueButton->Enabled = VTimeCodeEditOut->CueFromVTRAvailable();

      DurCopyButton->Enabled = false;
   }
   else if (YellowHilitedMasterClip.empty())
   {
      CenCopyButton->Enabled = false;
      CenCueButton->Enabled = false;
      InCopyButton->Enabled = false;
      InCueButton->Enabled = false;
      OutCopyButton->Enabled = false;
      OutCueButton->Enabled = false;
      DurCopyButton->Enabled = false;
   }
   else
   {
      CenCopyButton->Enabled = false; // Hmm, should this work?
      CenCueButton->Enabled = false;
      InCopyButton->Enabled = true;
      InCopyButton->Hint = "Copy from In timecode of current clip";
      InCueButton->Enabled = false;
      OutCopyButton->Enabled = true;
      InCopyButton->Hint = "Copy from Out timecode of current clip";
      OutCueButton->Enabled = false;
      DurCopyButton->Enabled = true;
   }
#else // fix bug 2508 - buttons ALWAYS control of copy from/to the VTR!
   // Ha WTF? why do we have to ask the question for each frickin' component?
   // I love that the generic VTimecode component knows how to query the
   // state of the VTR connection! And that we have a timer firing once a
   // second all the time, even when the bin manager is not displayed, to
   // update the state of these buttons. I hope it's a lightweight query!!
   // Overall the whole business of controlling the VTR from random timecodes
   // in the "Create Clip" panel is one of the awesomest hacks in the app!
   CenCopyButton->Enabled = VTimeCodeEditCen->CopyFromVTRAvailable();
   CenCueButton->Enabled = VTimeCodeEditCen->CueFromVTRAvailable();
   InCopyButton->Enabled = VTimeCodeEditIn->CopyFromVTRAvailable();
   InCueButton->Enabled = VTimeCodeEditIn->CueFromVTRAvailable();
   OutCopyButton->Enabled = VTimeCodeEditOut->CopyFromVTRAvailable();
   OutCueButton->Enabled = VTimeCodeEditOut->CueFromVTRAvailable();
#endif

}

int TBinManagerForm::GetTotalFrames(TStringList *tslClipNames)
{
   CBinManager binManager;
   CVideoFrameList *vflp;
   ClipSharedPtr clip = nullptr;
   int iRet = 0;
   int iNFrames = 0;
   char caDir[1024];
   char caClip[1024];
   char caMessage[256];

   // Open each clip (and get the number of frames)
   // strcpy(caDir, lastClickedBinPath.c_str());

   for (int i = 0; i < tslClipNames->Count; i++)
   {
      clip = binManager.openClip(StringToStdString(tslClipNames->Strings[i]), &iRet);
      if (iRet)
      {
         sprintf(caMessage, "Unable to open the clip:  %s\\%s  because: %d", caDir, caClip, iRet);
         theError.set(iRet, caMessage);
         return iRet;
      }

      if (clip == nullptr)
      {
         return -1;
      }

      // get frame count from main video
      vflp = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
      if (vflp)
      {
         iNFrames += vflp->getUserFrameCount();
      }

      // close the clip
      iRet = binManager.closeClip(clip);
      if (iRet != 0)
      {
         sprintf(caMessage, "Unable to close the clip:  %s\\%s  because: %d", caDir, caClip, iRet);
         theError.set(iRet, caMessage);
         return iRet;
      }
   }

   return iNFrames;
}

// ---------------------------------------------------------------------------

ClipSharedPtr TBinManagerForm::CreateSuperClip(const string &strClipName, int iNFrame, ClipSharedPtr &clipSrc, int *ipRet)
{
   // we are going to create a new clip that is a copy of the old one
   ClipSharedPtr clipDst = nullptr;
   CClipInitInfo cii;
   string strBinDst, strClipDst;
   char caMessage[256];

   int iRet = cii.initFromClip(clipSrc);
   if (iRet)
   {
      *ipRet = iRet;
      return nullptr;
   }

   // set main video to virtual
   // Note:  The CClipInitInfo treats NormalVideo and VideoProxy as different
   // However, the clip treats them as the same.
   if (cii.doesNormalVideoExist())
   {
      cii.getMainVideoInitInfo()->setVirtualMediaFlag(true);
   }

   // set all video proxies to virtual
   for (int iCnt = 0; iCnt < cii.getVideoProxyCount(); iCnt++)
   {
      cii.getVideoProxyInitInfo(iCnt)->setVirtualMediaFlag(true);
   }

   // set all audio tracks to virtual
   for (int iCnt = 0; iCnt < cii.getAudioTrackCount(); iCnt++)
   {
      cii.getAudioTrackInitInfo(iCnt)->setVirtualMediaFlag(true);
   }

   // make the destination clip just the right length
   cii.setOutTimecode(cii.getInTimecode() + iNFrame);

   // set the clip bin and name
   CBinManager binManager;
   iRet = binManager.splitClipFileName(strClipName, strBinDst, strClipDst);
   if (iRet)
   {
      sprintf(caMessage, "Unable to parse clip name:  %s  because: %d", strClipName.c_str(), iRet);
      theError.set(iRet, caMessage);
      *ipRet = iRet;
      return nullptr;
   }

   cii.setClipName(strClipDst);
   cii.setBinPath(strBinDst);

   // create the clip
   clipDst = cii.createClip(&iRet);
   if (iRet)
   {
      sprintf(caMessage, "Unable to create super clip because: %d", iRet);
      theError.set(iRet, caMessage);
      *ipRet = iRet;
      return nullptr;
   }

   // TTT - should we close the clip?
   *ipRet = 0;
   return clipDst;
} /* CreateSuperClip */

// ---------------------------------------------------------------------------
//
// Written by John Starr for Mike Russell
//
int TBinManagerForm::MakeBigVirtualClip(TStringList *srcClipNames, ClipSharedPtr &dstClip)
{
   // Arguments:
   //
   // srcClipNames a vector of strings that contain the names of the
   // source clips (e.g. C:\_MTIBins\Clip001).  StringList
   // is defined in IniFile.h
   // dstClip      a pointer to the virtual clip that will receive the
   // source clip's frames

   CBinManager binMgr;
   int status;

   // Get details from destination clip
   CVideoProxy *dstVideoProxy = dstClip->getVideoProxy(VIDEO_SELECT_NORMAL);
   CVideoFrameList *dstVideoFrameList = dstVideoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
   int dstFrameIndex = dstVideoFrameList->getInFrameIndex(); // past handles
   int dstFrameCount = dstVideoFrameList->getUserFrameCount();
   // without handles

   // Iterate over the source clip names and do "virtual copy" for
   // each source clip into the destination clip
   int srcClipCount = srcClipNames->Count;
   for (int i = 0; i < srcClipCount; ++i)
   {
      // Open a source clip
      auto srcClip = binMgr.openClip(StringToStdString(srcClipNames->Strings[i]), &status);
      if (status != 0)
      {
         return status; // cannot open a source clip
      }

      // Get main video proxy and video-frames frame list
      CVideoProxy *srcVideoProxy = srcClip->getVideoProxy(VIDEO_SELECT_NORMAL);
      CVideoFrameList *srcFrameList = srcVideoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);

      // get source clip's frame count without handles
      int srcFrameCount = srcFrameList->getUserFrameCount();
      int srcFrameIndex = srcFrameList->getInFrameIndex();

      // Check if there is enough room remaining in the destination clip
      // to receive this source clip's frames
      if (dstFrameIndex + srcFrameCount > dstFrameCount)
      {
         binMgr.closeClip(srcClip);
         return status; // not enough room in the destination clip
      }

      // Do the "virtual copy" from the source to destination clip
      status = dstVideoProxy->virtualCopy(*srcVideoProxy, srcFrameIndex, dstFrameIndex, srcFrameCount);
      if (status != 0)
      {
         binMgr.closeClip(srcClip);
         return status; // cannot copy
      }

      status = binMgr.closeClip(srcClip);
      if (status != 0)
      {
         return status; // cannot close the source clip
      }

      dstFrameIndex += srcFrameCount; // destination for next iteration
   }

   // Update the destination clip files
   status = dstVideoProxy->updateFieldListFile();
   if (status != 0)
   {
      return status;
   }

   status = dstVideoFrameList->writeFrameListFile();
   if (status != 0)
   {
      return status;
   }

   status = dstClip->writeFile();
   if (status != 0)
   {
      return status;
   }

   return 0;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::TBPreferenceItemClick(TObject *Sender)
{
   if (ShowModalDialog(FormPref) == mrOk)
   {
      UpdateTabOrder();
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::RadioButtonClick(TObject *Sender)
{
   // clicking a radio button turn the tab stop on.  We need to turn if off
   FillWithBlanksRadioButton->TabStop = false;
   FillByRecordingRadioButton->TabStop = false;
   FillFromClipRadioButton->TabStop = false;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreePopupMenuPopup(TObject *Sender)
{
   // this function exists to keep track of a rightclicked bin path and node.
   // the string righClickedBinPath will be accessible by all methods
   // and is updated every time someone right clicks a bin... as is
   // rightClickedNode;

   TTreeNode *rootNode = BinTree->Items->Item[0];

	rightClickedNode = BinTree->Selected;
	string binPath = getBinPathFromBinTreeNode(rightClickedNode);
	rightClickedBinPath = binPath.empty() ? rightClickedBinPath : binPath;
	bool rootNodeContext = rightClickedNode == rootNode;

   BinPopupNewBinItem->Enabled = !rootNodeContext;
   if (rootNodeContext)
   {
      BinPopupCreateJobItem->Visible = true;
      BinPopupNewBinItem->Visible = false;
      BinPopupDeleteItem->Enabled = false;
      BinPopupRenameItem->Enabled = false;
      BinPopupShareInAndOutMarks->Enabled = false;
   }
   else
   {
      BinPopupCreateJobItem->Visible = false;
      BinPopupNewBinItem->Visible = true;
      BinPopupDeleteItem->Enabled = true;
      BinPopupRenameItem->Enabled = true;
      BinPopupShareInAndOutMarks->Enabled = true;
   }

   CBinManager binManager;
   string iniFileName = binManager.makeClipDesktopIniFileName(rightClickedBinPath);

   CIniFile *ini = CreateIniFile(iniFileName, true); // readonly
   BinPopupShareInAndOutMarks->Checked = ini->ReadBool("General", "ClipsShareInAndOutMarks", false);
   DeleteIniFile(ini);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::clearRightClick()
{
   // this function clears the rightClickedNode and binPath because
   // the user has now left-clicked another Node and we must focus
   // on that

   rightClickedNode = nullptr;
   rightClickedBinPath.clear();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipSchemeInfoButtonClick(TObject *Sender)
{
   ShowClipSchemeInfo(ClipSchemePopupBox->Text.c_str());
}

// ---------------------------------------------------------------------------
void TBinManagerForm::ShowClipSchemeInfo(const string &newClipSchemeName)
{
   // Make the form if it doesn't exist yet.
   if (ClipSchemePropForm == 0)
   {
      ClipSchemePropForm = new TClipSchemePropForm(this);
   }

   // Feed the clip scheme name to the form
   ClipSchemePropForm->SetScheme(newClipSchemeName);

   // Show it
   ShowModalDialog(ClipSchemePropForm);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormActivate(TObject *Sender)
{
   // in _MTIWinInterface, the cursor type is
   // 0 = default, 1 = arrows, 2 = cross

   oldBusyCursor = CBusyCursor::getBusyCursorType();

   if (oldBusyCursor != 1)
   {
      SetBusyCursorToBusyArrows();
   }

   this->FormStyle = TFormStyle::fsNormal;
   ////  SetWindowPos(Handle, HWND_TOPMOST, Left,Top, Width, Height, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormDeactivate(TObject *Sender)
{
   switch (oldBusyCursor)
   {
   case 0:
      SetBusyCursorToDefault();
      break;

   case 2:
      SetBusyCursorToBusyCross();
      break;
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ChangeClipPanelType(eClipPanelType eType)
{
   switch (eType)
   {
      // QQQ figure this out:   clipPanelTypeFile vs clipPanelTypeVideo
   case clipPanelTypeFile:
   case clipPanelTypeVideo:
      GroupBoxLicense->Visible = false;
      GroupBoxProjectScanList->Visible = false;
      GroupBoxNewClip->Visible = true;
      break;
   case clipPanelTypeLicense:
      GroupBoxNewClip->Visible = false;
      GroupBoxProjectScanList->Visible = false;
      GroupBoxLicense->Visible = true;
      GetLicenseInfo();
      MakeLicenseInfoEditable(false);
      break;
   case clipPanelTypeProject:
      GroupBoxNewClip->Visible = false;
      GroupBoxLicense->Visible = false;
      GroupBoxProjectScanList->Visible = true;
      ShootDate->Visible = false;
      ShootDateLabel->Visible = false;
      TransferDate->Visible = false;
      TransferDateLabel->Visible = false;
      FieldA->Visible = false;
      FieldALabel->Visible = false;
      FieldB->Visible = false;
      FieldBLabel->Visible = false;
      FieldC->Visible = false;
      FieldCLabel->Visible = false;
      WorkOrder->Visible = false;
      WorkOrderLabel->Visible = false;
      Colorist->Visible = false;
      ColoristLabel->Visible = false;
      Assist->Visible = false;
      AssistLabel->Visible = false;
      GetProjectInfo();
      MakeScanListInfoEditable(false);
      break;
   case clipPanelTypeScanList:
      GroupBoxNewClip->Visible = false;
      GroupBoxLicense->Visible = false;
      GroupBoxProjectScanList->Visible = true;
      ShootDate->Visible = true;
      ShootDateLabel->Visible = true;
      TransferDate->Visible = true;
      TransferDateLabel->Visible = true;
      FieldA->Visible = true;
      FieldALabel->Visible = true;
      FieldB->Visible = true;
      FieldBLabel->Visible = true;
      FieldC->Visible = true;
      FieldCLabel->Visible = true;
      WorkOrder->Visible = true;
      WorkOrderLabel->Visible = true;
      Colorist->Visible = true;
      ColoristLabel->Visible = true;
      Assist->Visible = true;
      AssistLabel->Visible = true;
      GetProjectInfo();
      GetScanListInfo();
      MakeScanListInfoEditable(false);
      break;
   default:
      MTIassert(0);
      break;
   }

   HackVideoStoreUsage();
   UpdateTabOrder();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ScanListEditButtonClick(TObject *Sender)
{
   if (ScanListEditButton->Caption == EDIT_CANCEL_BUTTON_EDIT_STRING)
   {
      MakeScanListInfoEditable(true);

      // change focus to first field; in scan list context, change
      // to shoot date; in project/bin context change to production
      // company
      if (ShootDate->Visible == true)
      {
         ShootDate->SetFocus();
      }
      else
      {
         ProdCo->SetFocus();
      }
   }
   else // "Cancel"
   {
      GetProjectInfo();
      GetScanListInfo();
      MakeScanListInfoEditable(false);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::LicenseEditButtonClick(TObject *Sender)
{
   if (LicenseEditButton->Caption == EDIT_CANCEL_BUTTON_EDIT_STRING)
   {
      MakeLicenseInfoEditable(true);

      // change focus to first field
      Company->SetFocus();
   }
   else // "Cancel"
   {
      GetLicenseInfo();
      MakeLicenseInfoEditable(false);
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::GetLicenseInfo()
{
   CBinManager binMgr;

   Company->Text = binMgr.getcompany(lastClickedBinPath).c_str();
   Address->Text = binMgr.getaddress(lastClickedBinPath).c_str();
   CityStatePostal->Text = binMgr.getcityStatePostal(lastClickedBinPath).c_str();
   Country->Text = binMgr.getcountry(lastClickedBinPath).c_str();
}

void TBinManagerForm::GetProjectInfo()
{
   CBinManager binMgr;

   JobNo->Text = binMgr.getjobNo(lastClickedBinPath).c_str();
   ProdCo->Text = binMgr.getprodCo(lastClickedBinPath).c_str();
   Project->Text = binMgr.getproject(lastClickedBinPath).c_str();
   Title->Text = binMgr.gettitle(lastClickedBinPath).c_str();
   ProdNo->Text = binMgr.getprodNo(lastClickedBinPath).c_str();
}

void TBinManagerForm::GetScanListInfo()
{
   CBinManager binMgr;

   // multiple clips should be handled more intelligently
   StringList selectedClips = SelectedClipNames();
   if (selectedClips.size() == 1)
   {
      string clip = selectedClips[0];
      ShootDate->Text = binMgr.getshootDate(clip).c_str();
      TransferDate->Text = binMgr.gettransferDate(clip).c_str();
      FieldA->Text = binMgr.getfieldA(clip).c_str();
      FieldB->Text = binMgr.getfieldB(clip).c_str();
      FieldC->Text = binMgr.getfieldC(clip).c_str();
      WorkOrder->Text = binMgr.getworkOrder(clip).c_str();
      Colorist->Text = binMgr.getcolorist(clip).c_str();
      Assist->Text = binMgr.getassist(clip).c_str();
   }
}

void TBinManagerForm::MakeLicenseInfoEditable(bool doit)
{
   Company->Enabled = doit;
   Address->Enabled = doit;
   CityStatePostal->Enabled = doit;
   Country->Enabled = doit;
   LicenseSaveButton->Enabled = doit;
   LicenseEditButton->Cancel = doit; // Whether "esc" cancels or not
   CancelButton->Cancel = !doit; // Project Manager "esc" opposite

   LicenseEditButton->Caption = doit ? EDIT_CANCEL_BUTTON_CANCEL_STRING : EDIT_CANCEL_BUTTON_EDIT_STRING;
}

void TBinManagerForm::MakeScanListInfoEditable(bool doit)
{
   JobNo->Enabled = doit;
   ProdCo->Enabled = doit;
   Project->Enabled = doit;
   Title->Enabled = doit;
   ProdNo->Enabled = doit;

   ShootDate->Enabled = doit;
   TransferDate->Enabled = doit;
   FieldA->Enabled = doit;
   FieldB->Enabled = doit;
   FieldC->Enabled = doit;
   WorkOrder->Enabled = doit;
   Colorist->Enabled = doit;
   Assist->Enabled = doit;
   ScanListSaveButton->Enabled = doit;
   ScanListEditButton->Cancel = doit; // Whether "esc" cancels or not
   CancelButton->Cancel = !doit; // Project Manager "esc" opposite

   ScanListEditButton->Caption = doit ? EDIT_CANCEL_BUTTON_CANCEL_STRING : EDIT_CANCEL_BUTTON_EDIT_STRING;
}

void __fastcall TBinManagerForm::LicenseSaveButtonClick(TObject *Sender)
{
   if (lastClickedBinPath.empty())
   {
      return; // neither the last Bin nor the right-clicked Bin was selected
   }

   string bin;
   bin = lastClickedBinPath;

   CBinManager binMgr;
   binMgr.setcompany(bin, StringToStdString(Company->Text));
   binMgr.setaddress(bin, StringToStdString(Address->Text));
   binMgr.setcityStatePostal(bin, StringToStdString(CityStatePostal->Text));
   binMgr.setcountry(bin, StringToStdString(Country->Text));
   MakeLicenseInfoEditable(false);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ScanListSaveButtonClick(TObject *Sender)
{
   if (lastClickedBinPath.empty())
   {
      return; // neither the last Bin nor the right-clicked Bin was selected
   }

   string bin;
   bin = lastClickedBinPath;

   CBinManager binMgr;
   binMgr.setjobNo(bin, StringToStdString(JobNo->Text));
   binMgr.setprodCo(bin, StringToStdString(ProdCo->Text));
   binMgr.setproject(bin, StringToStdString(Project->Text));
   binMgr.settitle(bin, StringToStdString(Title->Text));
   binMgr.setprodNo(bin, StringToStdString(ProdNo->Text));

   // multiple clips should be handled more intelligently
   StringList selectedClips = SelectedClipNames();
   if (selectedClips.size() == 1)
   {
      string clip = selectedClips[0];
      binMgr.setshootDate(clip, StringToStdString(ShootDate->Text));
      binMgr.settransferDate(clip, StringToStdString(TransferDate->Text));
      binMgr.setfieldA(clip, StringToStdString(FieldA->Text));
      binMgr.setfieldB(clip, StringToStdString(FieldB->Text));
      binMgr.setfieldC(clip, StringToStdString(FieldC->Text));
      binMgr.setworkOrder(clip, StringToStdString(WorkOrder->Text));
      binMgr.setcolorist(clip, StringToStdString(Colorist->Text));
      binMgr.setassist(clip, StringToStdString(Assist->Text));
   }

   MakeScanListInfoEditable(false);
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::MakeNewDailiesClip(string const &newClipName, TDailiesClipForm const *dailiesClipForm)
{
   return false;
}
// ---------------------------------------------------------------------------

// non-members
void DupMenuItem(TMenuItem *Out, TMenuItem *In)
{
   Out->Action = In->Action;
   Out->AutoHotkeys = In->AutoHotkeys;
   Out->AutoLineReduction = In->AutoLineReduction;
   Out->Bitmap = In->Bitmap;
   Out->Break = In->Break;
   Out->Checked = In->Checked;
   Out->Caption = In->Caption;
   Out->Default = In->Default;
   Out->Enabled = In->Enabled;
   Out->GroupIndex = In->GroupIndex;
   Out->HelpContext = In->HelpContext;
   Out->Hint = In->Hint;
   Out->ImageIndex = In->ImageIndex;
   Out->Name = In->Name;
   Out->RadioItem = In->RadioItem;
   Out->ShortCut = In->ShortCut;
   Out->SubMenuImages = In->SubMenuImages;
   Out->Tag = In->Tag;
   Out->Visible = In->Visible;
   Out->OnClick = In->OnClick;
   Out->OnAdvancedDrawItem = In->OnAdvancedDrawItem;
   Out->OnDrawItem = In->OnDrawItem;
   Out->OnMeasureItem = In->OnMeasureItem;

   // Create a new menu if there are submenues
   for (int i = 0; i < In->Count; i++)
   {
      TMenuItem *mi = new TMenuItem(Out);
      Out->Add(mi);
      DupMenuItem(mi, In->Items[i]);
   }
}

void __fastcall CopyMenu(TMenu *Out, TMenu *In)
{
   Out->Items->Clear();
   DupMenuItem(Out->Items, In->Items);
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::IncreaseClipMedia(CClipInitInfo *clipInfo, TDailiesClipForm *dailiesClipForm)
{
   return false;
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::DecreaseClipMedia(CClipInitInfo *clipInfo, TDailiesClipForm *dailiesClipForm)
{
   return false;
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::getImageMediaDuration(CClipInitInfo const *clipInfo, CTimecode &duration)
{
   bool retVal = true;

   duration = clipInfo->getVideoMediaDuration();

   if (duration == CTimecode::NOT_SET)
   {
      retVal = false;
   }

   return retVal;
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::getAudioMediaDuration(CClipInitInfo const *clipInfo, CTimecode &duration)
{
   bool retVal = true;

   duration = clipInfo->getAudioMediaDuration();

   if (duration == CTimecode::NOT_SET)
   {
      retVal = false;
   }

   return retVal;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::RunCutDetectionMenuItemClick(TObject *Sender)
{
   // for each selected clip, modify the timeline. and run the diff program.

   // HACK! Fix soon!
   TAutoDiffForm *difform = new TAutoDiffForm(this);
   difform->setCurClip(lastClickedClipPath);
   ShowModalDialog(difform);
   delete difform;
   difform = 0;

   // Reload the clip - force rebuild of "scene cut" timeline (bug 1623)
   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->ClipHasChanged();
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::FreeMedia(TObject *Sender, ETrackType trackType)
{
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::MultiBrowseButtonClick(TObject *Sender)
{
   if (FormMultiBrowse == 0)
   {
      FormMultiBrowse = new TFormMultiBrowse(BinManagerForm);
      CIniFile *ini = CreateIniFile(GetIniFileName());
      if (ini == nullptr)
      {
         _MTIErrorDialog(Handle, (string)"Could not open inifile " + theError.getMessage());
         return;
      }

      // Now read the settings
      FormMultiBrowse->ReadSettings(ini, GetIniSection());
      DeleteIniFile(ini);
   }

   if (lastClickedBinPath == "")
   {
      _MTIErrorDialog(Handle, "You must select an active bin");
      return;
   }

   // (Fix bug 2318) Set bin default clip name because that's where the
   // "multibrowse" dialog gets the name.
   string defaultClipName = StringToStdString(FileClipNameEdit->Text.Trim());
   if (!defaultClipName.empty())
   {
      CBinManager binMgr;
      binMgr.setAutoClipName(lastClickedBinPath, defaultClipName);
   }

   // tell the multi browser about the current bin
   FormMultiBrowse->setClickedBinPath(lastClickedBinPath);
   FormMultiBrowse->setClipSchemeName(StringToStdString(ExistingMediaFormatComboBox->Text));
   FormMultiBrowse->PopupParent = this;
   ShowModalDialog(FormMultiBrowse);

   CBusyCursor Busy(true);

   // update the UI
   MultiBrowseClipListType *list;
   FormMultiBrowse->getNewClipList(list);
   AddClipsToClipListView(list);
   FormMultiBrowse->ClearNewClipList();
   list = nullptr;

   // Set a new Default Clip Name
   SetDefaultClipName();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileClipTCCheckBoxClick(TObject *Sender)
{
   bool flag = FileClipTCCheckBox->Checked;
   FileClipInTCLabel->Enabled = flag;
   FileClipInTCEdit->Enabled = flag;
   FileClipDFLabel->Enabled = flag;
   FileClipDFCheckBox->Enabled = flag;
   FileClipFPSLabel->Enabled = flag;
   FileClipFPSComboBox->Enabled = flag;

}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileMenuRenameBinItemClick(TObject *Sender)
{
   int retVal;

   TTreeNode *oldBinNode;

   if (rightClickedNode == nullptr)
   {
      oldBinNode = BinTree->Selected;
   }
   else
   {
      oldBinNode = rightClickedNode;
   }

   clearRightClick();

   if (oldBinNode == nullptr)
   {
      return; // neither the last Bin nor the right-clicked Bin was selected
   }

	string oldBinPath = getBinPathFromBinTreeNode(oldBinNode);
	string oldBinName = GetFileNameWithExt(RemoveDirSeparator(oldBinPath));

   // prevent renaming root bin
   TTreeNode *rootNode = BinTree->Items->Item[0];
   if (oldBinNode == rootNode)
   {
      _MTIErrorDialog(Handle, "Cannot rename root bin.");
      return;
   }

   string parentPath = getBinPathFromBinTreeNode(oldBinNode->Parent);

   TNewBinDialog *NewBinDialog = new TNewBinDialog(BinManagerForm);
   MTIostringstream ostrCaption;
   ostrCaption << "Rename Bin";
   NewBinDialog->Caption = ostrCaption.str().c_str();
   NewBinDialog->BinNameEdit->Text = oldBinName.c_str();
   NewBinDialog->BinNameEdit->SelectAll();

   int result = ShowModalDialog(NewBinDialog);
   string newBinName = StringToStdString(NewBinDialog->BinNameEdit->Text);
   delete NewBinDialog;

   if (result == mrOk)
   {
      string badChars = "\\/:*?\"<>|#";
      if (newBinName.find_first_of(badChars) != string::npos)
      {
         MTIostringstream errmsg;
         errmsg << "A bin name cannot contain any of the following characters: " << endl << "    " << badChars;
         _MTIErrorDialog(Handle, errmsg.str());
         return;
      }

      string newBinPath = AddDirSeparator(parentPath) + newBinName;

      string reason;
      lockID_t lockID;

      if (binMgrGUIInterface != 0)
      {
         lockID = binMgrGUIInterface->RequestBinRename(oldBinPath, newBinPath, reason);
         if (binMgrGUIInterface->IsLockValid(lockID) == false)
         {
            _MTIErrorDialog(Handle, reason);
            return;
         }
      }

      retVal = rename(oldBinPath.c_str(), newBinPath.c_str());
      if (retVal != 0)
      {
         MTIostringstream errmsg;
         errmsg << "Rename failed: ";

         switch (errno)
         {
         case EEXIST:
            errmsg << "Either the new name already exists, or a program is using the bin folder";
            break;
         case ENOENT:
            errmsg << "The bin folder does not exist";
            break;
         default:
            errmsg << "An unknown error occurred";
            break;
         }

         _MTIErrorDialog(Handle, errmsg.str());

         if (binMgrGUIInterface != 0)
         {
            string reason;
            int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
            if (retVal != 0)
            {
               _MTIErrorDialog(Handle, reason);
               return;
            }
         }
      }
      else
      {
         retVal = CMediaInterface::renameBinMedia(oldBinPath, newBinPath);
         if (retVal != 0)
         {
            MTIostringstream errmsg;
            errmsg << "Rename failed (" << retVal << ")";
            _MTIErrorDialog(Handle, errmsg.str());

            if (binMgrGUIInterface != 0)
            {
               string reason;
               int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
               if (retVal != 0)
               {
                  _MTIErrorDialog(Handle, reason);
                  return;
               }
            }
         }
         else
         {
            // success!

            if (binMgrGUIInterface != 0)
            {
               CBinManager binMgr;
               binMgr.handleRenamedBin(oldBinPath, newBinPath);

               string reason;
               int retVal = binMgrGUIInterface->FinishBinRename(lockID, oldBinPath, newBinPath, reason);
               if (retVal != 0)
               {
                  _MTIErrorDialog(Handle, reason);
                  return;
               }
            }

            oldBinNode->Text = newBinName.c_str();

            // Fix the clip list cache
            BinClipListCacheType::iterator iter;
            iter = BinClipListCache.find(oldBinPath);
            if (iter != BinClipListCache.end())
            {
               // found it in the cache
               ClipListDataType *clipListData = iter->second;
               BinClipListCache.erase(iter);
               BinClipListCache[newBinPath] = clipListData;
            }

            if (IsSameBinPath(lastClickedBinPath, oldBinPath))
            {
               SetSelectedBin(newBinPath);
               Caption = AnsiString("Project Manager - ") + newBinPath.c_str();
            }
         }
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::RenameClipMenuItemClick(TObject *Sender)
{
   int retVal;

   StringList selectedClipList;
   string strMessage;

   int selectCount = GetSelectedClips(selectedClipList);
   if (selectCount < 1)
   {
      return; // No clips selected
   }

   string oldClipName = selectedClipList[0];

   if (selectCount > 1)
   {
      MTIostringstream ostr;
      ostr << "More than one clip selected";
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   // rename clip folder and files
   TNewBinDialog *NewBinDialog = new TNewBinDialog(BinManagerForm);
   MTIostringstream ostrCaption;
   ostrCaption << "Rename Clip";
   NewBinDialog->Caption = ostrCaption.str().c_str();
   NewBinDialog->BinNameEdit->Text = oldClipName.c_str();
   NewBinDialog->BinNameEdit->SelectAll();

   int result = ShowModalDialog(NewBinDialog);
   string newClipName = StringToStdString(NewBinDialog->BinNameEdit->Text);
   delete NewBinDialog;

   if (result == mrOk)
   {
      string binPath = lastClickedBinPath;
      string reason;
      lockID_t lockID;

      if (binMgrGUIInterface != 0)
      {
         lockID = binMgrGUIInterface->RequestClipRename(binPath, oldClipName, binPath, newClipName, reason);
         if (binMgrGUIInterface->IsLockValid(lockID) == false)
         {
            _MTIErrorDialog(Handle, reason);
            return;
         }
      }

      CBinManager binMgr;
      retVal = binMgr.renameClip(binPath.c_str(), oldClipName.c_str(), binPath.c_str(), newClipName.c_str());
      if (retVal != 0)
      {
         MTIostringstream errmsg;
         errmsg << "Rename failed: " << theError.getMessage() << " (" << retVal << ")";
         _MTIErrorDialog(Handle, errmsg.str());

         if (binMgrGUIInterface != 0)
         {
            string reason;
            int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
            if (retVal != 0)
            {
               _MTIErrorDialog(Handle, reason);
               return;
            }
         }
      }
      else
      {
         if (binMgrGUIInterface != 0)
         {
            // mlm: to-do: remove legacy callback?
            binMgrGUIInterface->ClipWasRenamed(binPath, oldClipName, binPath, newClipName);

            string reason;
            int retVal = binMgrGUIInterface->FinishClipRename(lockID, binPath, oldClipName, binPath, newClipName, reason);
            if (retVal != 0)
            {
               _MTIErrorDialog(Handle, reason);
               return;
            }
         }

         // Refresh the clip list entry for this clip. NOTE: the sort
         // order may be wrong until the user clicks on a column head
         // again; I think that's better than auto-sorting
         int listIndex = FindClipListIndexByClipName(oldClipName);
         if (listIndex >= 0 && listIndex < (int) CurrentBinClipListData->size())
         {
            CurrentBinClipListData->at(listIndex)->clipName = newClipName;
            RefreshOneClipListItemByListIndex(listIndex);
         }
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BrowseToFolderMenuItemClick(TObject *Sender)
{
   int retVal;

   StringList selectedClipList;
   string strMessage;

   int selectCount = GetSelectedClips(selectedClipList);
   if (selectCount < 1)
   {
      // No clips selected
      return;
   }
   else if (selectCount > 1)
   {
      MTIostringstream ostr;
      ostr << "More than one clip selected";
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   string clipName = selectedClipList[0];
   int listIndex = FindClipListIndexByClipName(clipName);
   if (listIndex >= 0 && listIndex < (int) CurrentBinClipListData->size())
   {
      string mediaFolder = CurrentBinClipListData->at(listIndex)->mediaLocation;
      if (!mediaFolder.empty())
      {
         ::ShellExecute(nullptr, "open", mediaFolder.c_str(), nullptr, nullptr, SW_SHOWDEFAULT);
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BrowseToVersionFolderMenuItemClick(TObject *Sender)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);

   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   string mediaFolder;
   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   for (int versionIndex = 0; versionIndex < (int) versionList.size(); ++versionIndex)
   {
      if (versionList.at(versionIndex)->versionIsSelected)
      {
         mediaFolder = versionList.at(versionIndex)->mediaLocation;
         break;
      }
   }

   if (!mediaFolder.empty())
   {
      ::ShellExecute(nullptr, "open", mediaFolder.c_str(), nullptr, nullptr, SW_SHOWDEFAULT);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipPopupEditCommentsItemClick(TObject *Sender)
{
   int retVal;

   StringList selectedClipList;
   string strMessage;

   int selectCount = GetSelectedClips(selectedClipList);
   if (selectCount < 1)
   {
      // No clips selected
      return;
   }

   string binName = lastClickedBinPath;
   string clipName = selectedClipList[0];
   int status;
   CBinManager binManager;
   auto clip = binManager.openClip(AddDirSeparator(binName) + clipName, &status);
   if (clip == nullptr)
   {
      return;
   }

   if (EnterCommentForm == nullptr)
   {
      EnterCommentForm = new TEnterCommentForm(this);
   }

   EnterCommentForm->setComment(clip->getDescription());
   EnterCommentForm->setCaption((selectCount == 1) ? ("Edit comment for clip " + clipName) : "Edit comment for multiple clips");
   binManager.closeClip(clip);

   int result = ShowModalDialog(EnterCommentForm);
   if (result != mrOk)
   {
      return;
   }

   for (int i = 0; i < selectCount; ++i)
   {
      clipName = selectedClipList[i];
      clip = binManager.openClip(AddDirSeparator(binName) + clipName, &status);

      clip->setDescription(EnterCommentForm->getComment());
      clip->writeFile(); // force a .clp file write - paranoia

      int listIndex = FindClipListIndexByClipName(clipName);
      if (listIndex >= 0 && listIndex < (int) CurrentBinClipListData->size())
      {
         CurrentBinClipListData->at(listIndex)->clipDescription = clip->getDescription();
         RefreshOneClipListItemByListIndex(listIndex);
      }

      binManager.closeClip(clip);
   }
}

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
   Accept = false;

   if (Source->InheritsFrom(__classid(TListView)))
   {
      TTreeView *TreeViewSender = (TTreeView*)Sender;
      TTreeNode *DropSite = TreeViewSender->GetNodeAt(X, Y);

      TTreeNode *rootNode = BinTree->Items->Item[0];

      if (DropSite != 0 && DropSite != rootNode)
      {
         Accept = true;
      }
   }
   else if (Source->InheritsFrom(__classid(TTreeView)))
   {
      // make sure we're not trying to drag into a bin contained by
      // the source
      TTreeView *TreeViewSender = (TTreeView*)Sender;
      TTreeView *TreeViewSource = (TTreeView*)Source;
      TTreeNode *DropSite = TreeViewSender->GetNodeAt(X, Y);
      TTreeNode *Dragee = TreeViewSource->Selected;
      if (DropSite != 0 && DropSite->HasAsParent(Dragee) == false && DropSite != Dragee)
      {
         Accept = true;
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinTreeDragDrop(TObject *Sender, TObject *Source, int X, int Y)
{
   string const ClipStr = "Clip";

   // maybe my test should be if Source == ClipListView instead?
   if (Source->InheritsFrom(__classid(TListView)))
   {
      TTreeView *TreeViewSender = (TTreeView*)Sender;

      // pNode is drop target
      TTreeNode *DropSite = TreeViewSender->GetNodeAt(X, Y);

      TTreeNode *rootNode = BinTree->Items->Item[0];

      if (DropSite != 0 && DropSite != rootNode)
      {
         StringList toBeMovedList;
         StringList successfullyMovedList;
         // Maybe we need to get the selected list from Source instead
         // of from this window's class; otherwise if the drag started
         // in another window, we're in trouble.  For now, get it from
         // this window.
         int selectCount = GetSelectedClips(toBeMovedList);
         if (selectCount < 1)
         {
            return;
         } // No clips selected

         string oldBinPath = lastClickedBinPath;
         string newBinPath = getBinPathFromBinTreeNode(DropSite);

         // Why?? ClipListView->Clear();
         CBusyCursor Busy(true);

         for (int i = 0; i < selectCount; ++i)
         {
            string clipName = toBeMovedList[i];

            CBinManager binMgr;

            string reason;
            lockID_t lockID;

            if (binMgrGUIInterface != 0)
            {
               lockID = binMgrGUIInterface->RequestClipRename(oldBinPath, clipName, newBinPath, clipName, reason);
               if (binMgrGUIInterface->IsLockValid(lockID) == false)
               {
                  _MTIErrorDialog(Handle, reason);
                  RefreshClipList(&toBeMovedList);
                  return;
               }
            }

            // Is this dangerous if there are other clip objects open
            // referring to the same clip?  How can we protect against
            // that?
            int retVal = binMgr.renameClip(oldBinPath.c_str(), clipName.c_str(), newBinPath.c_str(), clipName.c_str());
            if (retVal != 0)
            {
               MTIostringstream errmsg;
               errmsg << "Rename failed for " << clipName << " (" << retVal << ")    " << endl << theError.getMessage();
               _MTIErrorDialog(Handle, errmsg.str());

               if (binMgrGUIInterface != 0)
               {
                  string reason;
                  int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
                  if (retVal != 0)
                  {
                     _MTIErrorDialog(Handle, reason);
                     RefreshClipList(&toBeMovedList);
                     return;
                  }
               }
            }
            else
            {
               if (binMgrGUIInterface != 0)
               {
                  // mlm: to-do: remove legacy callback?
                  binMgrGUIInterface->ClipWasRenamed(oldBinPath, clipName, newBinPath, clipName);

                  string reason;
                  int retVal = binMgrGUIInterface->FinishClipRename(lockID, oldBinPath, clipName, newBinPath, clipName, reason);
                  if (retVal != 0)
                  {
                     _MTIErrorDialog(Handle, reason);
                     RefreshClipList(&toBeMovedList);
                     return;
                  }
               }
               successfullyMovedList.push_back(clipName);
            }
         }

         // Remove moved clips from the old bin old bin
         RemoveClipsFromClipListView(&successfullyMovedList);

         // Refresh new bin
         // TODO - Make an actual cache object to hide stuff like this
         BinClipListCacheType::iterator iter;
         iter = BinClipListCache.find(newBinPath);
         if (iter != BinClipListCache.end())
         {
            ClipListDataType *clipListData = iter->second;
            GenerateClipListData(clipListData, newBinPath);
         }
      }
   }
   else if (Source->InheritsFrom(__classid(TTreeView)))
   {
      // make sure we're not trying to drag into a bin contained by
      // the source
      TTreeView *TreeViewSender = (TTreeView*)Sender;
      TTreeView *TreeViewSource = (TTreeView*)Source;
      TTreeNode *DropSite = TreeViewSender->GetNodeAt(X, Y);
      TTreeNode *Dragee = TreeViewSource->Selected;
      if (DropSite != 0 && DropSite->HasAsParent(Dragee) == false && DropSite != Dragee)
      {
         // move the bin
         int retVal;

         string oldBinPath = getBinPathFromBinTreeNode(Dragee);

         string binName = StringToStdString(Dragee->Text);
			string newBinPath = getBinPathFromBinTreeNode(DropSite);
			newBinPath = AddDirSeparator(newBinPath) + binName;

         string reason;
         lockID_t lockID;

         if (binMgrGUIInterface != 0)
         {
            lockID = binMgrGUIInterface->RequestBinRename(oldBinPath, newBinPath, reason);
            if (binMgrGUIInterface->IsLockValid(lockID) == false)
            {
               _MTIErrorDialog(Handle, reason);
               return;
            }
         }

         retVal = rename(oldBinPath.c_str(), newBinPath.c_str());
         if (retVal != 0)
         {
            TTreeNode *rootNode = BinTree->Items->Item[0];
            MTIostringstream errmsg;
            errmsg << "Rename failed: ";

            switch (errno)
            {
            case EEXIST:
               errmsg << "Either the new name already exists, or a program is using the bin folder.";
               break;
            case ENOENT:
               errmsg << "The bin folder does not exist.";
               break;
            default:
               errmsg << "An unknown error occurred.";
               break;
            }

            _MTIErrorDialog(Handle, errmsg.str());

            if (binMgrGUIInterface != 0)
            {
               string reason;
               int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
               if (retVal != 0)
               {
                  _MTIErrorDialog(Handle, reason);
                  return;
               }
            }
         }
         else
         {
            retVal = CMediaInterface::renameBinMedia(oldBinPath, newBinPath);
            if (retVal != 0)
            {
               MTIostringstream errmsg;
               errmsg << "Rename failed (" << retVal << ")";
               _MTIErrorDialog(Handle, errmsg.str());

               if (binMgrGUIInterface != 0)
               {
                  string reason;
                  int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
                  if (retVal != 0)
                  {
                     _MTIErrorDialog(Handle, reason);
                     return;
                  }
               }
            }
            else
            {
               // success
               if (binMgrGUIInterface != 0)
               {
                  CBinManager binMgr;
                  binMgr.handleRenamedBin(oldBinPath, newBinPath);

                  string reason;
                  int retVal = binMgrGUIInterface->FinishBinRename(lockID, oldBinPath, newBinPath, reason);
                  if (retVal != 0)
                  {
                     _MTIErrorDialog(Handle, reason);
                     return;
                  }
               }

               Dragee->MoveTo(DropSite, naAddChild);
               DropSite->AlphaSort();

               if (IsSameBinPath(lastClickedBinPath, oldBinPath))
               {
						SetSelectedBin(newBinPath);
                  Caption = AnsiString("Project Manager - ") + newBinPath.c_str();
               }
            }
         }
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewCustomDrawSubItem(TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
   // Ignore the first one, it is done via draw
   if (SubItem == 0)
   {
      return;
   }

   TColor color = clWhite;
   if (SubItem == COL_INDEX_CLIP_STATUS)
   {
      if (CompareText(Item->SubItems->Strings[SubItem - 1], "Versioned") == 0)
      {
         color = (TColor)0x0000E8; // Red
      }
   }

   Sender->Canvas->Font->Color = color;

   int i = Item->Index;
   if (i >= (int) CurrentBinClipListData->size())
   {
      return;
   } // sanity

   if (CurrentBinClipListData->at(i)->isHighlighted)
   {
      Sender->Canvas->Brush->Color = YellowHilitedVersion.IsValid() ? clLightYellow : clYellow;

      Sender->Canvas->Font->Color = clBlack;
   }
   else
   {
      string fullClipName = AddDirSeparator(lastClickedBinPath) + CurrentBinClipListData->at(i)->clipName;
      string redClipName;
      if (binMgrGUIInterface)
      {
         redClipName = binMgrGUIInterface->GetRedClipName();
      }

      Sender->Canvas->Brush->Color = (fullClipName == redClipName) ? clRed : (TColor)0x6a6a6a;

      Sender->Canvas->Font->Color = clWhite;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VideoFileBrowseButtonClick(TObject *Sender)
{
   char caPath[1024];
   char caLastDir[512];
   char caSubDir[512];
   char caAllButLastDir[1024];
   char caNextToLastDir[1024];
   char caSuperClipName[1024];
   char caFileExt[1024];
   static TMultipleDirectoriesForm *MultipleDirectoriesForm = nullptr;
   string strFileFilter;
   CClipSchemeManager clipSchemeMgr;
   EMediaType videoMediaType = clipSchemeMgr.getClipSchemeMainVideoMediaType(clipSchemeName);

   switch (videoMediaType)
   {
   case MEDIA_TYPE_IMAGE_FILE_DPX:
      strFileFilter = "DPX (*.dpx)|*.dpx|";
      break;
   case MEDIA_TYPE_IMAGE_FILE_EXR:
      strFileFilter = "EXR (*.exr)|*.exr|";
      break;
   case MEDIA_TYPE_IMAGE_FILE_TIFF:
      strFileFilter = "TIFF (*.tif, *.tiff)|*.tif; *.tiff|";
      break;
   default:
      break;
   }

   if (BrowseForFileName(VideoFileFolderEdit, VideoFileNameEdit, strFileFilter, 1))
   {
      if (FormPref->ShowMultipleDirectoriesForm())
      {
         struct ffblk ffblk1;
         int iRet, i, iIndex;
         char caTry[1024], caBaseSubDir[512], caDirToCheck[1024];
         bool bFoundDigit = false;
         int iSearchLocation = FormPref->GetMultipleDirectorySearchLocation();

         // Grab the extension (e.g. .dpx)
         strcpy(caFileExt, GetFileExt(StringToStdString(OpenDialog->FileName)).c_str());
         // Grab the last directory, (e.g. scene1)
         strcpy(caLastDir, GetFileLastDir(StringToStdString(VideoFileFolderEdit->Text).c_str()).c_str());
         // Grab all but the last directory, (e.g. f:\images\reel1\)
         strcpy(caAllButLastDir, GetFileAllButLastDir(StringToStdString(VideoFileFolderEdit->Text).c_str()).c_str());
         // Grab the next to last directory, (e.g. reel1)
         strcpy(caNextToLastDir, GetFileLastDir(caAllButLastDir).c_str());

         // Search for digit(s) in caLastDir
         int iLenLastDir = strlen(caLastDir);
         int iLastFoundDigit = 0;
         int iFirstFoundDigit = 0;

         if (iSearchLocation == SEARCH_LOCATION_END)
         {
            i = iLenLastDir - 1;
            while ((i >= 0) && (isdigit(caLastDir[i])))
            {
               bFoundDigit = true;
               iFirstFoundDigit = i;
               // Set this to the newly discovered digit
               i--;
            }
         }
         else // iSearchLocation == BEGINNING
         {
            i = 0;
            while ((i < iLenLastDir) && (isdigit(caLastDir[i])))
            {
               bFoundDigit = true;
               iLastFoundDigit = i; // Set this to the newly discovered digit
               i++;
            }
         }

         // If there's a digit in the directory, then we'll try to find other
         // similarly numbered directories. - mpr
         if (bFoundDigit)
         {
            int iPosFirstDigit = 0;
            int iNumDigits = 0;

            if (iSearchLocation == SEARCH_LOCATION_END)
            {
               iPosFirstDigit = iFirstFoundDigit;
               iNumDigits = iLenLastDir - iFirstFoundDigit;
            }
            else
            {
               iPosFirstDigit = 0;
               iNumDigits = iLastFoundDigit + 1;
            }

            int iListBoxIndex = 0;
            int iOrigClipIndex = 0;

            TStringList *tslDirs = new TStringList;
            TStringList *tslSubDirs = new TStringList;
            TStringList *tslFiles = new TStringList;
            TStringList *tslClipNames = new TStringList;

            tslDirs->Clear();
            tslSubDirs->Clear();
            tslDirs->Clear();
            tslClipNames->Clear();

            iListBoxIndex = 0;

            // Get BaseSubDir - (e.g. scene1 -> scene)
            if (iSearchLocation == SEARCH_LOCATION_END)
            {
               strcpy(caBaseSubDir, caLastDir);
               caBaseSubDir[iPosFirstDigit] = '\0';
               // Truncate before first digit
            }
            else
            {
               char *caTmp = caLastDir;
               caTmp += iNumDigits;
               strcpy(caBaseSubDir, caTmp);
            }

            // Grab the index - (e.g. 1)
            sscanf(&(caLastDir[iPosFirstDigit]), "%d", &iIndex);

            // Build up SubDir from component parts, (e.g. scene1).  Also handle
            // cases with leading zeros.
            if (iSearchLocation == SEARCH_LOCATION_END)
            {
               sprintf(caSubDir, "%s%0*d", caBaseSubDir, iNumDigits, iIndex);
            }
            else
            {
               sprintf(caSubDir, "%0*d%s", iNumDigits, iIndex, caBaseSubDir);
            }

            // This is the first directory to check, (e.g. f:\images\reel\scene1\)
            sprintf(caDirToCheck, "%s%s\\", caAllButLastDir, caSubDir);

            // Keep checking directories
            while (DoesDirectoryExist((string)caDirToCheck))
            {
               // Generate a possible file in the first directory
               // to check, (e.g. f:\images\reel\scene1\*.dpx)
               strcpy(caTry, caDirToCheck);
               strcat(caTry, "*");
               strcat(caTry, caFileExt);

               // Search for a file that matches
               iRet = findfirst(caTry, &ffblk1, 0);
               if (iRet == 0) // We found a file that matched our wildcard
               {
                  char caFileName[256];

                  strcpy(caFileName, ffblk1.ff_name);
                  TRACE_2(errout << "Found file: " << caTry);
                  // Save the associated parts of the file for future use
                  tslDirs->Add(caDirToCheck);
                  tslSubDirs->Add(caSubDir);
                  tslFiles->Add(caFileName);
                  iListBoxIndex++;
               }

               iIndex++;
               // Generate the name of the next directory to check, (e.g. f:\images\reel\scene2)
               if (iSearchLocation == SEARCH_LOCATION_END)
               {
                  sprintf(caSubDir, "%s%0*d", caBaseSubDir, iNumDigits, iIndex);
               }
               else
               {
                  sprintf(caSubDir, "%0*d%s", iNumDigits, iIndex, caBaseSubDir);
               }

               sprintf(caDirToCheck, "%s%s\\", caAllButLastDir, caSubDir);
            }

            // If we found more than one similar directory, then pop up the
            // MultipleDirectoriesForm Window
            if (iListBoxIndex > 1)
            {
               if (MultipleDirectoriesForm == nullptr)
               {
                  MultipleDirectoriesForm = new TMultipleDirectoriesForm(BinManagerForm);
               }

               MultipleDirectoriesForm->DirectoryCheckListBox->Items->Clear();

               // Fill in the CheckListBox with the list of directories
               for (i = 0; i < iListBoxIndex; i++)
               {
                  MultipleDirectoriesForm->DirectoryCheckListBox->Items->Add(tslDirs->Strings[i]);
                  // If this is the one the user selected, then pre-check it.
                  if (VideoFileFolderEdit->Text == tslDirs->Strings[i])
                  {
                     iOrigClipIndex = i;
                     // save index for later restoring of gui
                     MultipleDirectoriesForm->DirectoryCheckListBox->Checked[i] = true;
                  }
               }

               // Fill in the proposed name for the super-clip
               if (strcmp(caNextToLastDir, "") == 0)
               {
                  // If empty, then use name "super-clip"
                  MultipleDirectoriesForm->SuperClipNameEdit->Text = "super-clip";
               }
               else
               {
                  // Use (e.g. reel1)
                  MultipleDirectoriesForm->SuperClipNameEdit->Text = caNextToLastDir;
               }

               // If the user clicked OK, then create the clips
               if (ShowModalDialog(MultipleDirectoriesForm) == mrOk)
               {
                  int iNumToDo = MultipleDirectoriesForm->DirectoryCheckListBox->Items->Count;
                  int iNumClipsCreated = 0;
                  AnsiString strVideoFileFolderEditSave = "";
                  AnsiString strVideoFileNameEditSave = "";
                  AnsiString strClipNameEditSave = "";

                  // Save some gui variables for later restore
                  strVideoFileFolderEditSave = VideoFileFolderEdit->Text;
                  strVideoFileNameEditSave = VideoFileNameEdit->Text;
                  strClipNameEditSave = VideoClipNameEdit->Text;

                  // Iterate for each item in the CheckListBox
                  for (i = 0; i < iNumToDo; i++)
                  {
                     if (MultipleDirectoriesForm->DirectoryCheckListBox->Checked[i])
                     {
                        char caTmp[512];
                        char caFullClipName[1024];

                        CBusyCursor Busy(true);

                        // Create all the clips the user checked
                        strcpy(caTmp, StringToStdString(MultipleDirectoriesForm->DirectoryCheckListBox->Items->Strings[i]).c_str());
                        TRACE_2(errout << "Dir: " << MultipleDirectoriesForm->DirectoryCheckListBox->Items->Strings[i].c_str());
                        // Fill in GUI variables to similate clip creation by user's clicking
                        VideoFileFolderEdit->Text = tslDirs->Strings[i];
                        VideoFileNameEdit->Text = tslFiles->Strings[i];
                        VideoFileEditChange(nullptr);
                        VideoFileEditExit(nullptr);
                        VideoClipNameEdit->Text = tslSubDirs->Strings[i];
                        // Create the clip
                        CreateClipButtonClick(nullptr);

                        // Generate the full clip name (e.g. c:\_MTIBins\Mike\scene1)
                        sprintf(caFullClipName, "%s\\%s", lastClickedBinPath.c_str(), StringToStdString(tslSubDirs->Strings[i]).c_str());
                        tslClipNames->Add(caFullClipName);
                        // keep track of added clipnames
                        iNumClipsCreated++;
                     }
                  }

                  // Restore the gui variables we changed during the multiple clip creation
                  VideoFileFolderEdit->Text = strVideoFileFolderEditSave;
                  VideoFileNameEdit->Text = strVideoFileNameEditSave;
                  VideoClipNameEdit->Text = strClipNameEditSave;

                  // Create the Virtual Super Clip if user chose more than 1 directory
                  // and we created more than 1 clip.
                  if ((MultipleDirectoriesForm->CreateSuperClipCheckBox->State == cbChecked) && (iNumClipsCreated > 1))
                  {
                     CBinManager binManager;
                     int iNFrames = 0;
                     int iRet = 0;
                     char caDir[1024];
                     char caMessage[256];
                     CClipInitInfo clipInitInfo;

                     TRACE_2(errout << "Creating Super Clip");

                     CBusyCursor Busy(true);

                     // Get the number of frames in all the clips
                     iNFrames = GetTotalFrames(tslClipNames);
                     if (iNFrames < 0)
                     {
                        sprintf(caMessage, "GetTotalFrames failed with: %d", iNFrames);
                        theError.set(iNFrames, caMessage);
                        _MTIErrorDialog(Handle, theError.getFmtError());
                        return;
                     }

                     // open the clip based on the file the user originally selected
                     ClipSharedPtr clipSrc = binManager.openClip(StringToStdString(tslClipNames->Strings[iOrigClipIndex]), &iRet);
                     if (iRet)
                     {
                        sprintf(caMessage, "Unable to open the clip:  %s  because: %d",
                              StringToStdString(tslClipNames->Strings[iOrigClipIndex]).c_str(), iRet);
                        theError.set(iRet, caMessage);
                        _MTIErrorDialog(Handle, theError.getFmtError());
                        return;
                     }

                     sprintf(caSuperClipName, "%s\\%s", lastClickedBinPath.c_str(),
                           StringToStdString(MultipleDirectoriesForm->SuperClipNameEdit->Text).c_str());

                     // Create the Destination clip with basic info
                     ClipSharedPtr clipDst = CreateSuperClip(caSuperClipName, iNFrames, clipSrc, &iRet);
                     if (iRet)
                     {
                        sprintf(caMessage, "Unable to create super clip:  %s  because: %d", caSuperClipName, iRet);
                        theError.set(iRet, caMessage);
                        _MTIErrorDialog(Handle, theError.getFmtError());
                        binManager.closeClip(clipSrc);
                        return;
                     }
                     binManager.closeClip(clipSrc);

                     // Add in all of the subclips' frame lists
                     MakeBigVirtualClip(tslClipNames, clipDst);

                     // Update the Clip List
                     AddClipToClipListView(clipDst);
                  } // if CreateSuperClipCheckBox
               } // if ShowModel == OK
            } // if (iListBoxIndex > 1)

            // Cleanup
            delete tslDirs;
            delete tslSubDirs;
            delete tslFiles;
            delete tslClipNames;
         } // if (bFoundDigit)
      } // if (FormPref->ShowMultipleDirectoriesForm)

      VideoFileEditExit(nullptr);
   } // if browse succeeded
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VideoFileEditChange(TObject *Sender)
{
   _bVideoFileNameChange = true;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VideoFileEditExit(TObject *Sender)
{
   CClipInitInfo clipInitInfo;
   MTIostringstream os;
   MTIistringstream is;

   // Do nothing if we have not changed
   if (!_bVideoFileNameChange)
   {
      return;
   }

   _bVideoFileNameChange = false;

   // Update the "timecodes"
   string sImageFileName = MakeClipFileName(VideoFileFolderEdit, VideoFileNameEdit);

   // The caller's image file name may be a standard file name, such
   // as C:\Images\Test.0001.dpx or it may be a template with #
   // characters that indicate where the frame digits should go, such
   // as C:\Images\Test.####.dpx.  Determine whether sImageFileName is
   // a template or a regular file name and then make either the regular
   // file name or the template
   int inFrameNumber, outFrameNumber;
   string sImageFileNameTemplate;
   if (CImageFileMediaAccess::IsTemplate(sImageFileName))
   {
      // sImageFileName is a template, so make a regular file name
      // with the current In Frame number or 0 if that file doesn't exist
      sImageFileNameTemplate = sImageFileName;
      // Get the in point
      is.clear();
      is.str(StringToStdString(VideoInFrameEdit->Text));
      is >> inFrameNumber;
      string tmpNameIn;
      // Try current In Frame number
      tmpNameIn = CImageFileMediaAccess::GenerateFileName2(sImageFileNameTemplate, inFrameNumber);
      if (DoesFileExist(tmpNameIn))
      {
         sImageFileName = tmpNameIn; // file name with current In Frame
      }
      else
      {
         // Try frame = 0
         string tmpName0;

         tmpName0 = CImageFileMediaAccess::GenerateFileName2(sImageFileNameTemplate, 0);
         if (DoesFileExist(tmpName0))
         {
            // this kind of strange QQQ
            sImageFileName = tmpName0; // file name with frame = 0
            VideoInFrameEdit->Text = "0";
         }
         else
         {
            sImageFileName = tmpNameIn; // oh well...
         }
      }
   }
   else
   {
      // sImageFileName contains a regular file name, so attempt to
      // figure out where the frame digits are located and create
      // a template.
      sImageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(sImageFileName, false);

      // Get the in point
      inFrameNumber = CImageFileMediaAccess::getFrameNumberFromFileName(sImageFileName);
      VideoInFrameEdit->Text = inFrameNumber;
   }

   // Put the template name into the File Name entry field so the user
   // can see where the Project Manager thinks the frame number is.  This
   // gives the user a chance to put the frame number digits somewhere
   // else if the code guessed the wrong location.
   if (!sImageFileNameTemplate.empty())
   {
      VideoFileNameEdit->Text = (GetFileName(sImageFileNameTemplate) + GetFileExt(sImageFileNameTemplate)).c_str();
   }
   else
   {
      // Could not parse the user's file name to create a proper template.
      // Put the user's original name back in the File Name entry field
      VideoFileNameEdit->Text = (GetFileName(sImageFileName) + GetFileExt(sImageFileName)).c_str();

      // TTT: Should we put up a warning dialog or beep or would this just
      // be annoying while the user is typing the complete file name
      // or template?
   }

   if (DoesFileExist(sImageFileName))
   {
      // A sample image file exists, so determine the minimum and
      // maximum frame numbers
      int startFrameNumber = CImageFileMediaAccess::getFrameNumberFromFileName2(sImageFileNameTemplate, sImageFileName);
      if (startFrameNumber == -1)
      {
         // FAIL
         _MTIErrorDialog(Handle, "Invalid file name format");
         inFrameNumber = -1;
         outFrameNumber = -1;
      }
      else
      {
         CImageFileMediaAccess::FindAllFiles2(sImageFileNameTemplate, startFrameNumber, inFrameNumber, outFrameNumber);

         if (inFrameNumber == -1 || outFrameNumber == -1)
         {
            MTIostringstream os;
            os << "Image file folder scan failed -" << endl << "please enter first and last frame numbers    ";
            _MTIErrorDialog(Handle, os.str());
         }
      }

      _ClipFileDigitPrecision = CImageFileMediaAccess::GetDigitCount(sImageFileNameTemplate);

      // Set in frame number and out timecode that were just discovered
      VideoInFrameEdit->Text = inFrameNumber;
      VTimeCodeEditOut->tcP = VTimeCodeEditIn->tcP + (outFrameNumber - inFrameNumber + 1);

      // Variable label for "don't fill" radio button option
      // FillWithBlanksRadioButton->Caption = "Use Existing Files";
   }
   else
   {
      // The sample image file doesn't exist, but we still want to
      // know how many digits are in the frame number
      _ClipFileDigitPrecision = CImageFileMediaAccess::GetDigitCount(sImageFileNameTemplate);

      VideoInFrameEdit->Text = 0;
      outFrameNumber = VTimeCodeEditDur->tcP.getAbsoluteTime() + inFrameNumber;
      if (outFrameNumber == inFrameNumber)
      {
         VTimeCodeEditOut->tcP = VTimeCodeEditIn->tcP;
         VTimeCodeEditOut->tcP.setMinutes(VTimeCodeEditOut->tcP.getMinutes() + 1);
         outFrameNumber = VTimeCodeEditOut->tcP - VTimeCodeEditIn->tcP;
      }

      // Variable label for "don't fill" radio button option
      // FillWithBlanksRadioButton->Caption = "Blank Frames";
   }

   // Set the clip init info from the clip scheme
   string newClipSchemeName = clipSchemeName;
   // ClipSchemePopupBox->Text.c_str();
   int retVal = clipInitInfo.initFromClipScheme(newClipSchemeName);
   if (retVal != 0)
   {
      os << "ERROR: Cannot parse Clip Scheme File " << newClipSchemeName << "   Return Code: " << retVal << endl;
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // set the media identifier and frame 0 index - also checks header consistency
   retVal = clipInitInfo.initMediaLocation(sImageFileNameTemplate, inFrameNumber, outFrameNumber - inFrameNumber + 1, true,
         newClipSchemeName);
   if (retVal == CLIP_ERROR_IMAGE_FILE_MISSING)
   {
      // First image file is not available, so check the others
      // If none exist, then assume the user wants the Image Files
      // to be created.  If some exist, then treat this as an error

      // Verify that none of the files exist
      retVal = clipInitInfo.checkNoImageFiles2(sImageFileNameTemplate, inFrameNumber, outFrameNumber + 1);
      if (retVal != 0)
      {
         os << "ERROR: Some Image Files are missing, some already exist    " << endl << "       Cannot determine user's intention" << endl;
         _MTIErrorDialog(Handle, os.str());
         return;
      }
   }
   else if (retVal == CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME || retVal == CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER)
   {
      // the image files and clip scheme are not compatible
      // or an image file was inconsistent with the first file's header
      _MTIErrorDialog(Handle, theError.getMessage());
      return;
   }
   else if (retVal == FORMAT_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED)
   {
      os << "Sorry, but you are not licensed for this resolution    " << "To upgrade, please contact your reseller or _MTI Film." << endl;

      _MTIErrorDialog(Handle, os.str());
      return;
   }
   else if (retVal != 0)
   {
      os << "ERROR: Cannot initialize from Image File" << endl << "       " << sImageFileNameTemplate << "    " << endl <<
            "       Return Code: " << retVal << endl;

      _MTIErrorDialog(Handle, os.str());
      return;
   }

   string clipName(clipInitInfo.getClipName());
   if (clipName.empty())
   {
      SetDefaultClipName();
   }
   else
   {
      VideoClipNameEdit->Text = clipName.c_str();
   }

   CTimecode tcIn(clipInitInfo.getInTimecode());
   CTimecode tcOut(clipInitInfo.getOutTimecode());
   SetVTimeCodeValue(VTimeCodeEditIn, tcIn);
   SetVTimeCodeValue(VTimeCodeEditOut, tcOut);
   SetVTimeCodeValue(VTimeCodeEditInSmall, tcIn);
   SetVTimeCodeValue(VTimeCodeEditOutSmall, tcOut);
   CalcDuration();
   CalcCentral();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ShowProxyTimecodeMenuItemClick(TObject *Sender)
{
   DisplayProxy(true);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CreateClipSelectionRadioButtonClick(TObject *Sender)
{
   if (true)
         ////UseExistingFilesRadioButton->Checked)
   {
      if (!NewClipExistingMediaPanel->Visible || NewClipNewMediaPanel->Visible)
      {
         NewClipNewMediaPanel->Visible = false;
         NewClipExistingMediaPanel->Visible = true;
      }
   }
   else
   {
      if (!NewClipNewMediaPanel->Visible || NewClipExistingMediaPanel->Visible)
      {
         NewClipExistingMediaPanel->Visible = false;
         NewClipNewMediaPanel->Visible = true;
      }
   }

   ClipFormatComponentChanged(nullptr);
}
// ---------------------------------------------------------------------------

// Is this necessary? If so, move it to a common H file
namespace
{
string CppStringFromAnsiString(const AnsiString &ansiString)
{
   string retVal;
   char *cp = new char[ansiString.Length() + 1];

   strcpy(cp, ansiString.c_str());
   retVal = cp;
   delete[]cp;

   return retVal;
}
} // local namespace

// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ComboBoxEnter(TObject *Sender)
{
   TComboBox *ComboBox = static_cast<TComboBox*>(Sender);

   // QQQ do this with the Tags
   if (ComboBox == FileVidClipVideoFolderComboBox)
   {
      FileVidClipAudioFolderComboBoxTextAtEntry = CppStringFromAnsiString(ComboBox->Text);
   }
   else if (ComboBox == FileVidClipAudioFolderComboBox)
   {
      FileVidClipAudioFolderComboBoxTextAtEntry = CppStringFromAnsiString(ComboBox->Text);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ComboBoxExit(TObject *Sender)
{
   TComboBox *ComboBox = static_cast<TComboBox*>(Sender);

   if (ComboBox == FileVidClipVideoFolderComboBox)
   {
      if (FileVidClipVideoFolderComboBoxTextAtEntry != StringToStdString(ComboBox->Text))
      {
         // Check to see if the directory exists, w OK/Cancel. If cancel,
         // put back the "text at entry".
      }
   }
   else if (ComboBox == FileVidClipAudioFolderComboBox)
   {
      if (FileVidClipAudioFolderComboBoxTextAtEntry != StringToStdString(ComboBox->Text))
      {
         // Check to see if the directory exists, w OK/Cancel. If cancel,
         // put back the "text at entry".
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ComboBoxSelect(TObject *Sender)
{
   ComboBoxExit(Sender);
   ComboBoxEnter(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ComboBoxKeyPress(TObject *Sender, char &Key)
{
   if (Key == Char(VK_RETURN))
   {
      ComboBoxSelect(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileVidClipVideoFolderBrowseButtonClick(TObject *Sender)
{
   string folderName = GetUserFolder(Handle, StringToStdString(FileVidClipVideoFolderComboBox->Text).c_str(),
         "Specify Video File Repository Folder");

   // Fixed dialog to return nullptr - mpr
   if (!folderName.empty())
   {
      FileVidClipVideoFolderComboBox->Text = folderName.c_str();
      ComboBoxSelect(FileVidClipVideoFolderComboBox);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileVidClipAudioFolderBrowseButtonClick(TObject *Sender)
{
   string folderName = GetUserFolder(Handle, StringToStdString(FileVidClipAudioFolderComboBox->Text).c_str(),
         "Specify Video File Repository Folder");

   // Fixed dialog to return nullptr - mpr
   if (!folderName.empty())
   {
      FileVidClipAudioFolderComboBox->Text = folderName.c_str();
      ComboBoxSelect(FileVidClipAudioFolderComboBox);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipFormatComponentChanged(TObject *Sender)
{
   string NewVideoFormat, NewAudioFormat, NewStorageFormat;

   NewVideoFormat = StringToStdString(NewMediaVideoFormatComboBox->Text);
   NewAudioFormat = StringToStdString(NewMediaAudioFormatComboBox->Text);
   if (LocationRepositoryRadioButton->Checked)
   {
      if (VideoStoreMediaStorageComboBox->Visible)
      {
         NewStorageFormat = StringToStdString(VideoStoreMediaStorageComboBox->Text);
      }
      else
      {
         NewStorageFormat = StringToStdString(FileRepositoryStorageFormatComboBox->Text);
      }
   }
   else
   {
      NewStorageFormat = StringToStdString(FileVidClipMediaStorageComboBox->Text);
   }

   CClipSchemeManager clipSchemeManager;
   CClipScheme* clipScheme = clipSchemeManager.findClipScheme(NewVideoFormat, NewAudioFormat, NewStorageFormat);
   if (clipScheme == 0)
   {
      NoClipSchemeFoundWarningButton->Visible = true;
      RepositoryFormatInfoButton->Visible = false;
      SetClipSchemeName("");
      return;
   }

   RepositoryFormatInfoButton->Visible = true;
   NoClipSchemeFoundWarningButton->Visible = false;

   SetClipSchemeName(clipScheme->getClipSchemeName());
   ClipSchemePopupBox->Text = clipSchemeName.c_str(); // Ugggh

   // update file name "in" frame if it's tied to the time code
   FileVidClipTCIndexCheckBoxClick(nullptr);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ExistingMediaFormatInfoButtonClick(TObject *Sender)
{
   ShowClipSchemeInfo(StringToStdString(ExistingMediaFormatComboBox->Text));
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::NewMediaFormatInfoButtonClick(TObject *Sender)
{
   ShowClipSchemeInfo(clipSchemeName);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileVidClipTCIndexCheckBoxClick(TObject *Sender)
{
   if (FileVidClipTCIndexCheckBox->Checked)
   {
      FileVidClipInFrameEdit->Enabled = false;
      FileVidClipInFrameEdit->Text = AnsiString(VTimeCodeEditIn->tcP.Index());
      VideoInFrameEdit->Enabled = false;
      VideoInFrameEdit->Text = AnsiString(VTimeCodeEditIn->tcP.Index());
      // Note: VTimeCodeEditInSmall is kept in sync with VTimeCodeEditIn
      // so don't need to check for "centralized"
   }
   else
   {
      FileVidClipInFrameEdit->Enabled = true;
      FileVidClipInFrameEdit->Text = AnsiString(0);
      VideoInFrameEdit->Enabled = true;
      VideoInFrameEdit->Text = AnsiString(0);
   }
}
// ---------------------------------------------------------------------------


void __fastcall TBinManagerForm::UnexpandCreateClipPanelButton_REMOVED_Click(TObject *Sender)
{
//	ExpandCreateClipPanelButton_REMOVED_Click(Sender);
}
// ---------------------------------------------------------------------------


void __fastcall TBinManagerForm::NoClipSchemeFoundWarningButtonClick(TObject *Sender)
{
   MTIostringstream ostr;
   string NewVideoFormat, NewAudioFormat, NewStorageFormat;

   NewVideoFormat = StringToStdString(NewMediaVideoFormatComboBox->Text);
   NewAudioFormat = StringToStdString(NewMediaAudioFormatComboBox->Text);
   if (LocationRepositoryRadioButton->Checked)
   {
      if (VideoStoreMediaStorageComboBox->Visible)
      {
         NewStorageFormat = StringToStdString(VideoStoreMediaStorageComboBox->Text);
      }
      else
      {
         NewStorageFormat = StringToStdString(FileRepositoryStorageFormatComboBox->Text);
      }
   }
   else
   {
      NewStorageFormat = StringToStdString(FileVidClipMediaStorageComboBox->Text);
   }

   ostr << "There is no Clip Scheme that matches    " << endl << "       VideoFormat=" << NewVideoFormat << endl << "       AudioFormat=" <<
         NewAudioFormat << endl << "       StorageFormat=" << NewStorageFormat;

   _MTIErrorDialog(Handle, ostr.str());
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileDetailsTrackBarChange(TObject *Sender)
{
   MTIistringstream isIn, isDur;
   int iDur, iIn;
   CTimecode timecode(0);
   int newFileIndex;

   // Get the in point and duration from the edit box
   isIn.str(StringToStdString(InFileClipEdit->Text));
   isIn >> iIn;
   isDur.str(StringToStdString(DurFileClipEdit->Text));
   isDur >> iDur;

   newFileIndex = iIn + ((FileDetailsTrackBar->Position * (iDur - 1)) / FileDetailsTrackBar->Max);
   if (newFileIndex != iImageFileIndex)
   {
      iImageFileIndex = newFileIndex;
      timecode.setAbsoluteTime(iImageFileIndex);
      UpdateFileDetailsPanel(sImageFileNameTemplate, iImageFileIndex);
      FileDetailsProxyFrame->ShowFrame(FileDisplayer, timecode);
   }
}
// ---------------------------------------------------------------------------

// TODO: Make sure tool not processing and player not running!! QQQ *******************************************
void __fastcall TBinManagerForm::ClipPopupStripAlphaChannelFromMediaFilesMenuItemClick(TObject *Sender)
{
   string clipName;
   CBinManager binMgr;
   StringList selectedClipList;
   int iStatus = -1;
   int addedCount = 0;
   int interactionResult = MTI_DLG_OK;

   if (binMgrGUIInterface && binMgrGUIInterface->WeWantToHackAClip() == false)
   {
      // Tool is processing, or a fix is pending and cannot be automatically accepted.
      // Suitable pop-ups have already be shown to the user at this point!
      return;
   }

   // Prep the processing dialog
   if (AlphaChannelStripperForm == nullptr)
   {
      AlphaChannelStripperForm = new TAlphaChannelStripperForm(this);
   }

   AlphaChannelStripperForm->Reset();
   AlphaChannelStripperForm->SetBinPath(lastClickedBinPath);

   // Add the selected clips to the list of clips to be hacked
   int selectCount = GetSelectedClips(selectedClipList);
   for (int i = 0; i < selectCount; ++i)
   {
      clipName = selectedClipList[i];

      std::auto_ptr<CClipInitInfo>clipInitInfo(binMgr.openClipOrClipIni(lastClickedBinPath, clipName, &iStatus));
      if (iStatus != 0)
      {
         MTIostringstream os;
         os << "ERROR: Can't open clip " << clipName << "    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      if (!clipInitInfo->doesNormalVideoExist())
      {
         MTIostringstream os;
         os << "Clip " << clipName << " is not a video clip    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      CVideoInitInfo *videoInitInfo = clipInitInfo->getMainVideoInitInfo();
      // Apparently, getMainVideoInitInfo() is guaranteed to not return 0

      if (!CMediaInterface::isMediaTypeImageFile(videoInitInfo->getMediaType()))
      {
         MTIostringstream os;
         os << "Clip " << clipName << " does not have video in image files    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      if (binMgr.doesClipHaveVersions(lastClickedBinPath, clipName))
      {
         MTIostringstream os;
         os << "Can't strip clip " << clipName << " because it has versions!    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      // Do some basic screening of the selected clips
      // Note that we allow clips that are marked RGB because there may be some
      // RGBA frames interspersed in the RGB clip.
      const CImageFormat *imageFormat = clipInitInfo->getClipPtr()->getImageFormat(VIDEO_SELECT_NORMAL);
      bool isDpx = imageFormat->getImageFormatType() == IF_TYPE_FILE_DPX;
      bool isAlpha16BitInterleaved = imageFormat->getAlphaMatteType() == IF_ALPHA_MATTE_16_BIT_INTERLEAVED;
      bool isNoAlpha = imageFormat->getAlphaMatteType() == IF_ALPHA_MATTE_NONE;
      bool is16Bit = imageFormat->getBitsPerComponent() == 16;
      bool isRgbOrRgba = (imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB ||
                          imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA);
      if (!isDpx || !(isAlpha16BitInterleaved || isNoAlpha) || !is16Bit || !isRgbOrRgba)
      {
         MTIostringstream os;
         os << "Clip " << clipName << " media is not 16-bit RGB(A) DPX files!    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      // All is well, so add this clip to the processing list
      AlphaChannelStripperForm->AddClip(clipName);
      ++addedCount;
   }

   // Process the clips only if the operation wasn't cancelled
   if (interactionResult == MTI_DLG_OK)
   {
      if (addedCount > 0)
      {
         // Heavy lifting happens here   QQQ why check for mrOK? Some clips may have been changed in any case!
         ShowModalDialog(AlphaChannelStripperForm);

         // Refresh the clip list to update clip status column
         RefreshClipList();

         // Reload the currently loaded clip in case we hacked it
         // TODO: Only do this if we actually hacked the currently
         // loaded clip!!
         if (binMgrGUIInterface)
         {
            binMgrGUIInterface->ClipHasChanged();
         }
      }
      else if (selectCount > 1)
      {
         _MTIInformationDialog(Handle, "Nothing to do!    ");
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipPopupChangeDpxHeaderFrameRateMenuItemClick(TObject *Sender)
{
   string clipName;
   CBinManager binMgr;
   StringList selectedClipList;
   int iStatus = -1;
   int addedCount = 0;
   int interactionResult = MTI_DLG_OK;

   if (binMgrGUIInterface && binMgrGUIInterface->WeWantToHackAClip() == false)
   {
      // Tool is processing, or a fix is pending and cannot be automatically accepted.
      // Suitable pop-ups have already be shown to the user at this point!
      return;
   }

   // Prep the processing dialog
   if (DpxHeaderEditorForm == nullptr)
   {
      DpxHeaderEditorForm = new TDpxHeaderEditorForm(this);
   }

   DpxHeaderEditorForm->Reset();
   DpxHeaderEditorForm->SetBinPath(lastClickedBinPath);

   // Add the selected clips to the list of clips to be hacked
   int selectCount = GetSelectedClips(selectedClipList);
   for (int i = 0; i < selectCount; ++i)
   {
      clipName = selectedClipList[i];

      std::auto_ptr<CClipInitInfo>clipInitInfo(binMgr.openClipOrClipIni(lastClickedBinPath, clipName, &iStatus));
      if (iStatus != 0)
      {
         MTIostringstream os;
         os << "ERROR: Can't open clip " << clipName << "    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      if (!clipInitInfo->doesNormalVideoExist())
      {
         MTIostringstream os;
         os << "Clip " << clipName << " is not a video clip    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      CVideoInitInfo *videoInitInfo = clipInitInfo->getMainVideoInitInfo();
      // Apparently, getMainVideoInitInfo() is guaranteed to not return 0

      if (!CMediaInterface::isMediaTypeImageFile(videoInitInfo->getMediaType()))
      {
         MTIostringstream os;
         os << "Clip " << clipName << " does not have video in image files    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      if (binMgr.doesClipHaveVersions(lastClickedBinPath, clipName))
      {
         MTIostringstream os;
         os << "Can't strip clip " << clipName << " because it has versions!    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      // Do some basic screening of the selected clips
      // Note that we allow clips that are marked RGB because there may be some
      // RGBA frames interspersed in the RGB clip.
      const CImageFormat *imageFormat = clipInitInfo->getClipPtr()->getImageFormat(VIDEO_SELECT_NORMAL);
      bool isDpx = imageFormat->getImageFormatType() == IF_TYPE_FILE_DPX;
      if (!isDpx)
      {
         MTIostringstream os;
         os << "Clip " << clipName << " media files are not DPX!    ";
         if (selectCount == 1)
         {
            _MTIErrorDialog(Handle, os.str());
            interactionResult = MTI_DLG_CANCEL;
            break;
         }
         else
         {
            os << endl << "Skip this clip and continue?";
            interactionResult = _MTIWarningDialog(Handle, os.str());
            if (interactionResult == MTI_DLG_CANCEL)
            {
               break; // Abort
            }
         }

         continue; // skip this clip
      }

      // All is well, so add this clip to the processing list
      DpxHeaderEditorForm->AddClip(clipName);
      ++addedCount;
   }

   // Process the clips only if the operation wasn't cancelled
   if (interactionResult == MTI_DLG_OK)
   {
      if (addedCount > 0)
      {
         // Heavy lifting happens here   QQQ why check for mrOK? Some clips may have been changed in any case!
         ShowModalDialog(DpxHeaderEditorForm);

         // Refresh the clip list to update clip status column
         RefreshClipList();

         // Reload the currently loaded clip in case we hacked it
         // TODO: Only do this if we actually hacked the currently
         // loaded clip!!
         if (binMgrGUIInterface)
         {
            binMgrGUIInterface->ClipHasChanged();
         }
      }
      else if (selectCount > 1)
      {
         _MTIInformationDialog(Handle, "Nothing to do!    ");
      }
   }
}
// ---------------------------------------------------------------------------

// Show, hide or destroy alpha matte in clip media files
void __fastcall TBinManagerForm::ShowHideAlphaMatteMenuItemClick(TObject *Sender)
{
   string clipName;
   CBinManager binMgr;
   StringList selectedClipList;
   int iStatus = -1;
   int addedCount = 0;
   int interactionResult = MTI_DLG_OK;

   // Prep the processing dialog
   if (AlphaMatteShowHideForm == nullptr)
   {
      AlphaMatteShowHideForm = new TAlphaMatteShowHideForm(this);
   }

   AlphaMatteShowHideForm->Reset();
   AlphaMatteShowHideForm->SetBinPath(lastClickedBinPath);

   // Add the selected clips to the list of clips to be hacked
   int selectCount = GetSelectedClips(selectedClipList);
   for (int i = 0; i < selectCount; ++i)
   {
      clipName = selectedClipList[i];

      std::auto_ptr<CClipInitInfo>clipInitInfo(binMgr.openClipOrClipIni(lastClickedBinPath, clipName, &iStatus));
      if (iStatus != 0)
      {
         MTIostringstream os;
         os << "ERROR: Can't open clip " << clipName << "    ";
         interactionResult = _MTIWarningDialog(Handle, os.str());
         if (interactionResult == MTI_DLG_CANCEL)
         {
            break; // Abort
         }

         continue; // skip this clip
      }
      if (!clipInitInfo->doesNormalVideoExist())
      {
         MTIostringstream os;
         os << "Clip " << clipName << " is not a video clip    ";
         interactionResult = _MTIWarningDialog(Handle, os.str());
         if (interactionResult == MTI_DLG_CANCEL)
         {
            break; // Abort
         }

         continue; // skip this clip
      }

      CVideoInitInfo *videoInitInfo = clipInitInfo->getMainVideoInitInfo();
      // Apparently, getMainVideoInitInfo() is guaranteed to not return 0

      if (!CMediaInterface::isMediaTypeImageFile(videoInitInfo->getMediaType()))
      {
         MTIostringstream os;
         os << "Clip " << clipName << " does not have video in image files    ";
         interactionResult = _MTIWarningDialog(Handle, os.str());
         if (interactionResult == MTI_DLG_CANCEL)
         {
            break; // Abort
         }

         continue; // skip this clip
      }

      EAlphaMatteType alphaMatteType;
      EAlphaMatteType alphaMatteHiddenType;

      // Do some basic screening of the selected clips
      clipInitInfo->getClipPtr()->getAlphaMatteTypes(alphaMatteType, alphaMatteHiddenType);
      // Bizarro world a la _MTI: the CImageInfo::isBlaBlaValid()
      // static methods return 0 if valid and non-zero if not valid!!!!!
      // bool hasNonInterleavedAlphaMatte =
      // (CImageInfo::isAlphaMatteTypeValid(alphaMatteType) == 0) &&
      // (!CImageInfo::queryAlphaMatteTypeInterleaved(alphaMatteType)) &&
      // (alphaMatteType != IF_ALPHA_MATTE_NONE);

      // bool hasHiddenNonInterleavedAlphaMatte =
      // (CImageInfo::isAlphaMatteTypeValid(alphaMatteHiddenType) == 0) &&
      // (!CImageInfo::queryAlphaMatteTypeInterleaved(alphaMatteHiddenType))
      // && (alphaMatteHiddenType != IF_ALPHA_MATTE_NONE);
      // TEMP CHANGES UNTIL REAL MERGE
      bool hasNonInterleavedAlphaMatte = CImageInfo::queryAlphaMatteTypeCanBeHidden(alphaMatteType);
      bool hasHiddenNonInterleavedAlphaMatte = CImageInfo::queryAlphaMatteTypeCanBeHidden(alphaMatteHiddenType);
      ////////////////////////

      if (!(hasNonInterleavedAlphaMatte || hasHiddenNonInterleavedAlphaMatte))
      {
         MTIostringstream os;
         os << "Clip " << clipName << " does not have an alpha matte that can be hidden    ";
         interactionResult = _MTIWarningDialog(Handle, os.str());
         if (interactionResult == MTI_DLG_CANCEL)
         {
            break; // Abort
         }
         continue; // skip this clip
      }

      // All is well, so add this clip to the processing list
      AlphaMatteShowHideForm->AddClip(clipName);
      ++addedCount;

      // The first clip we added will determine the default state of the
      // show/hide/destroy radio buttons of the GUI
      if (addedCount == 1)
      {
         TAlphaMatteShowHideForm::EDefaultSelection defaultSelection = hasNonInterleavedAlphaMatte ? TAlphaMatteShowHideForm::SELECT_HIDE :
               TAlphaMatteShowHideForm::SELECT_SHOW;
         AlphaMatteShowHideForm->SetGuiDefaultSelection(defaultSelection);
      }
   }

   // Process the clips only if the operation wasn't cancelled
   if (interactionResult == MTI_DLG_OK)
   {
      if (addedCount > 0)
      {
         // Heavy lifting happens here
         if (ShowModalDialog(AlphaMatteShowHideForm) == mrOk)
         {
            // Refresh the clip list to update clip status column
            RefreshClipList();

            // Reload the currently loaded clip in case we hacked it
            // TODO: Only do this if we actually hacked the currently
            // loaded clip!!
            if (binMgrGUIInterface)
            {
               binMgrGUIInterface->ClipHasChanged();
            }
         }
      }
      else if (selectCount > 1)
      {
         _MTIInformationDialog(Handle, "Nothing to do!    ");
      }
   }
}
// ---------------------------------------------------------------------------

// TTT - This should be moved to a more common area - perhaps devWin

bool TBinManagerForm::DeleteFileOrDirectory(string const &FileOrDirectory)
{
   SHFILEOPSTRUCT sfo;
   char caFileOrDirectory[512];

   ZeroMemory(&sfo, sizeof(SHFILEOPSTRUCT));
   sfo.hwnd = 0;
   sfo.wFunc = FO_DELETE;
   sfo.fFlags = FOF_ALLOWUNDO | FOF_NOERRORUI | FOF_NOCONFIRMATION;

   strcpy(caFileOrDirectory, FileOrDirectory.c_str());
   // Must be double-nullptr terminated so add another \0
   caFileOrDirectory[strlen(caFileOrDirectory) + 1] = '\0';

   sfo.pFrom = caFileOrDirectory;
   sfo.pTo = nullptr;
   int iRet = SHFileOperation(&sfo);
   if (iRet != 0)
   {
      MTIostringstream ostr;
      ostr << "TBinManagerForm::DeleteFileOrDirectory Could not " << "delete the file or directory (err = " << iRet << ")";
      TRACE_0(errout << ostr.str() << endl);
      return false;
   }

   return true;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::UpdateFileDetailsPanel(const string &fileNameTemplate, int frameIndex)
{
   // Clear details in case template is ""
   FileDetailsFileNameValue->Caption = "--";
   FileDetailsFormatValue->Caption = "--";
   FileDetailsDimensionsValue->Caption = "--";
   FileDetailsTimecodeValue->Caption = "--:--:--:--";

   if (fileNameTemplate.empty())
   {
      return;
   }

   int retVal;
   string filePath, fileName;
   char tmpDrive[_MAX_DRIVE];
   char tmpDir[_MAX_DIR];
   char tmpName[_MAX_FNAME];
   char tmpExt[_MAX_EXT];

   CImageFileMediaAccess::GenerateFileName(fileNameTemplate, filePath, frameIndex);

   _splitpath(filePath.c_str(), tmpDrive, tmpDir, tmpName, tmpExt);

   fileName = string(tmpName) + string(tmpExt);
   FileDetailsFileNameValue->Caption = fileName.c_str();

   // mio.strImageName = fileNameTemplate;
   // mio.iFirstFrame  = frameIndex;

   MediaFrameBufferSharedPtr frameBuffer;
   MediaFileIO mediaFileIO;
   retVal = mediaFileIO.readFrameBuffer(filePath, frameBuffer);
   if (retVal != 0)
   {
      MTIostringstream errorLabelStream;
      errorLabelStream << "ERROR (" << retVal << ")";
      FileDetailsFormatValue->Caption = errorLabelStream.str().c_str();
      return;
   }

   CImageFormat imageFormat = frameBuffer->getImageFormat();

   MTIostringstream formatValueStream;
   formatValueStream << CImageInfo::queryImageFormatTypeName(imageFormat.getImageFormatType()) << " ";
   formatValueStream << (((unsigned int) imageFormat.getBitsPerComponent()) & 0xFF) << "-bit ";
   formatValueStream << CImageInfo::queryPixelComponentsName(imageFormat.getPixelComponents()) << " ";
   formatValueStream << CImageInfo::queryColorSpaceName(imageFormat.getColorSpace());

   FileDetailsFormatValue->Caption = formatValueStream.str().c_str();

   MTIostringstream dimensionsValueStream;
   dimensionsValueStream << imageFormat.getPixelsPerLine() << " x " << imageFormat.getLinesPerFrame();
   FileDetailsDimensionsValue->Caption = dimensionsValueStream.str().c_str();

   CTimecode tc = frameBuffer->getTimecode();
   if (tc == CTimecode::NOT_SET || tc == 0)
   {
      tc = CTimecode(frameIndex);
   }

   string strTC;
   tc.getTimeString(strTC);
   FileDetailsTimecodeValue->Caption = strTC.c_str();

   retVal = mediaFileIO.dumpFileHeaderToString(filePath, strImageFileHeader);

   if (retVal == 0)
   {
      FileDetailsShowHeaderButton->Enabled = true;
      if (HeaderDumpForm == 0)
      {
         HeaderDumpForm = new THeaderDumpForm(this);
      }

      if (HeaderDumpForm != 0)
      {
         string title = fileName + " Image File Header";
         HeaderDumpForm->Caption = title.c_str();
         HeaderDumpForm->FilePathLabel->Caption = filePath.c_str();
         HeaderDumpForm->Set(strImageFileHeader);
      }
   }
   else
   {
      if (HeaderDumpForm != 0)
      {
         HeaderDumpForm->Hide();
      }

      strImageFileHeader = "";
      FileDetailsShowHeaderButton->Enabled = false;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileDetailsShowHeaderButtonClick(TObject *Sender)
{
   if (!strImageFileHeader.empty() && (HeaderDumpForm != 0))
   {
      // Bring up the "dump header panel"
      HeaderDumpForm->Set(strImageFileHeader);
      HeaderDumpForm->Show();
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::CommitVersionClipChanges(const string &versionClipPath, CTimecode markIn, CTimecode markOut)
{
   if (binMgrGUIInterface && !binMgrGUIInterface->IsItSafeToUnloadCurrentClip())
   {
      return;
   } // A change is pending or a tool is running

   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->SaveCurrentClipState();
   }

   string binPath;
   string versionClipName;
   string parentClipName;
   int parentFrameIndex;
   int framingType;
   CBinManager binMgr;
   int retVal = 0;

   // Open the version clip that is to be committed
   binMgr.splitClipFileName(versionClipPath, binPath, versionClipName);
   std::auto_ptr<CClipInitInfo>versionClipInfo(binMgr.openClipOrClipIni(binPath, versionClipName, &retVal));
   if (retVal != 0)
   {
      MTIostringstream os;
      os << "ERROR: Can't open clip " << versionClipName << " (" << retVal << ")    ";
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Get the version clip's rendering info, which will tell us the parent
   // clip into which we will merge the version
   // NOTE THIS IS NOT USED ANYMORE AND SHOULD BE REMOVED - QQQ
   auto versionClip = versionClipInfo->getClipPtr();
   if (versionClip != nullptr)
   {
      retVal = versionClip->getRenderInfo(parentClipName, parentFrameIndex, framingType);
   }

   if ((retVal != 0) || parentClipName.empty())
   {
      MTIostringstream os;
      os << "Commit failed because the original parent clip for clip" << endl << "version " << versionClipName << " cannot be identified";
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Version clips are now nested in the folder of the original clip
   parentClipName = binPath;

   // Break the original clip name apart into Bin and name
   string parentBinName;
   string parentClipNamePart;
   retVal = binMgr.splitClipFileName(parentClipName, parentBinName, parentClipNamePart);
   if (retVal != 0)
   {
      MTIostringstream os;
      os << "Commit failed because of an internal processing error (" << retVal << ")   ";
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Open the parent clip
   std::auto_ptr<CClipInitInfo>parentClipInfo(binMgr.openClipOrClipIni(
         // WAS lastClickedBinPath, parentClipName,
         parentBinName, parentClipNamePart, &retVal));
   if (retVal != 0)
   {
      MTIostringstream os;
      os << "Commit failed for clip version named " << versionClipName << endl << "because the original parent clip " << parentClipName <<
            endl << "could not be opened";
      _MTIErrorDialog(Handle, os.str());
      return;
   }

   // Prep the processing dialog
   if (CommitVersionForm == nullptr)
   {
      CommitVersionForm = new TCommitVersionForm(this);
   }
   CommitVersionForm->Init(TCommitVersionForm::COMMIT_CHANGES, TCommitVersionForm::DELETE_CLIP, &(*versionClipInfo), &(*parentClipInfo));
   CommitVersionForm->SetRange(markIn, markOut);

   // Do the commit
   ShowModalDialog(CommitVersionForm);

   // Random housekeeping - could have been put in FinishCommittingClip()
   ReloadProxy();
}
// ---------------------------------------------------------------------------

// Called by CommitVersionForm to remove the committed clip
void TBinManagerForm::FinishCommittingClip(CClipInitInfo *aVersionClipInfo, CClipInitInfo *aParentClipInfo, bool clipWasCommitted)
{
   if (aVersionClipInfo == nullptr || aParentClipInfo == nullptr)
   {
      return;
   }

   string versionBinPath = aVersionClipInfo->getBinPath();
   string versionClipName = aVersionClipInfo->getClipName();
   string parentClipName = aParentClipInfo->getClipName();
   string parentClipPath = AddDirSeparator(aParentClipInfo->getBinPath()) + parentClipName;

   // Commit worked, always end up showing the parent clip
   HighlightClip(parentClipPath, true); // show the clip in the BM
   ClipVersionNumber parentVersionNumber(parentClipName);
   if (parentVersionNumber.IsValid())
   {
      LoadNewVersion(parentClipPath);
   }
   else
   {
      LoadNewClip(parentClipPath);
   }

   RefreshClipList();

   // Delete the version clip
   // --------
   // PART 1: Remove the version clip's data structure from the master's
   // list of versions. Them repaint the version list view to
   // remove the line referring to the version clip

   // First find the master clip's version list - that list is ordered the
   // same way as the display (we could look at the display, but then we'd
   // have to assume that a particular column will exactly match the
   // version name)
   ClipIdentifier parentClipId(parentClipPath);
   int clipIndex = FindClipListIndexByClipName(parentClipId.GetMasterClipName());
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      TRACE_0(errout << "ERROR: BinManagerForm::CommitClip: Can't find parent clip " << parentClipName << " in clip list");
      return; // sanity
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   vector<SVersionDataItem*>::iterator selectedIter;

   // Find the line for this version in the version list view
   for (selectedIter = versionList.begin(); selectedIter != versionList.end(); ++selectedIter)
   {
      if ((*selectedIter)->versionName == versionClipName)
      {
         break;
      }
   }

   if (selectedIter != versionList.end())
   {
      // Remove it from the parent's version list and repaint the view
      versionList.erase(selectedIter);
      RepaintVersionListView();
   }

   // --------
   // PART 2: Now actually delete the clip
   // HACK!! First remember the 'dirty media folder' path
   CBinManager bm;
   string clipPath = bm.makeClipPath(versionBinPath, versionClipName);
   string dirtyMediaFolderPath = bm.getDirtyMediaFolderPath(clipPath);
   bm.setVersionStatus(versionBinPath, versionClipName, versionWasCommitted);
   int retVal = DeleteOneClip(versionBinPath, versionClipName);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: BinManagerForm::CommitClip: " "version clip deletion failed");
   }
   else if (!dirtyMediaFolderPath.empty())
   {
      // Remove the metadata ini file
      string metadataIniFilePath = AddDirSeparator(dirtyMediaFolderPath) + CMediaStorageList::getMetadataIniFilename();
      bm.removeFile(metadataIniFilePath);

      // We used to try to delete the media folder... now we rename it.
      ////bm.removeDirectory(dirtyMediaFolderPath);
      string buggeredMediaFolderPath = dirtyMediaFolderPath + (clipWasCommitted ? "-COMMITTED" : "-DISCARDED");
      retVal = rename(dirtyMediaFolderPath.c_str(), buggeredMediaFolderPath.c_str());
      if (retVal != 0)
      {
         MTIostringstream errmsg;
         errmsg << "Version media folder rename failed because ";

         switch (errno)
         {
         case EEXIST:
            errmsg << "another program is using the " << dirtyMediaFolderPath << " folder";
            break;
         case ENOENT:
            errmsg << "the " << dirtyMediaFolderPath << " folder does not exist";
            break;
         default:
            errmsg << "an unknown error occurred";
            break;
         }

         TRACE_0(errout << errmsg.str());
      }
   }

   // Finally - force re-display of the parent clip to get rid of
   // the "Master clip cannot be modified" disablement if we just deleted
   // the last version
   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->ClipHasChanged();
   }

   // Need to regenerate the master clip's version list IsVersioned flags
   ClipIdentifier clipId(clipPath);
   RefreshOneClipListItemByName(clipId.GetMasterClipName());
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CreateNewVersionMenuItemClick(TObject *Sender)
{
   CreateNewVersionClip(lastClickedClipPath);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::CreateNewVersionClip(const string &parentPath)
{
   if (binMgrGUIInterface && !binMgrGUIInterface->IsItSafeToUnloadCurrentClip())
   {
      return;
   } // A change is pending or a tool is running

   CBusyCursor Busy(true); // This is slow if there's lots of history

   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->SaveCurrentClipState();
   }

   ClipIdentifier parentClipId(parentPath);
   ClipVersionNumber newVersionNumber;
   CBinManager binManager;
   int retVal = binManager.createVersionClip(parentClipId, /* Output */ newVersionNumber);
   if (retVal != 0)
   {
      MTIostringstream ostr;
      ostr << "Clip cloning failed (" << retVal << ")";
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   ClipIdentifier versionClipId(parentPath, newVersionNumber);
   TRACE_3(errout << "LOAD NEW VERSION CLIP: " << versionClipId);
   LoadNewVersion(versionClipId.GetClipPath());
   RefreshClipList();
}
// ---------------------------------------------------------------------------
// **REFACTOR**: MOST OF THIS BELONGS IN CBinManager !!

int TBinManagerForm::DeleteVersionClip(ClipIdentifier versionClipId)
{
   CBinManager binMgr;
   string parentPath = versionClipId.GetParentPath();
   string versionClipName = versionClipId.GetClipName();
   int retVal = 0;

   // clipInitInfo containment block
   {
      std::auto_ptr<CClipInitInfo>clipInitInfo(binMgr.openClipOrClipIni(parentPath, versionClipName, &retVal));
      if (retVal != 0)
      {
         return retVal;
      }

      auto versionClip = clipInitInfo->getClipPtr();
      vector<int>listOfDirtyVideoFrameIndexes;
      vector<int>listOfDirtyFilmFrameIndexes;

      CBusyCursor Busy(true); // This is slow if there's lots of history

      // Discard dirty media
      CVideoFrameList *versionVideoFrameList = versionClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
      CVideoFrameList *versionFilmFrameList = versionClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_FILM);
      if (versionVideoFrameList == nullptr || versionFilmFrameList == nullptr)
      {
         TRACE_0(errout << "ERROR: Clip " << versionClipName << " does not have a main video frame list");
         return -1;
      }

      // Skip video frames list if we are committing and the frame list is locked
      int lockedFrameListIndex = versionClip->getLockedFrameListIndex();
      bool oneShot = true;
      if (lockedFrameListIndex != FRAMING_SELECT_VIDEO)
      {
         // Find and destroy dirty video frames
         retVal = binMgr.FindDirtyFrames(versionVideoFrameList, 0, -1,
               // all frames
               /* out */ listOfDirtyVideoFrameIndexes);
         if (retVal != 0)
         {
            return retVal;
         }

         retVal = binMgr.DiscardDirtyMedia(listOfDirtyVideoFrameIndexes, versionVideoFrameList);
         if (retVal != 0)
         {
            if (oneShot)
            {
               oneShot = false;
               TRACE_0(errout << "ERROR: Was unable to destroy one or more media files in version " << versionClipName << "!");
            }

            // Not fatal - just keep going.
         }

         // Clean up film frame list if necessary (avoid double deallocates)
         // without trying to destroy media
         if (versionVideoFrameList != versionFilmFrameList)
         {
            retVal = binMgr.FindDirtyFrames(versionFilmFrameList, 0, -1, // all frames
                  /* out */ listOfDirtyFilmFrameIndexes);
            if (retVal == 0)
            {
               // false = don't destroy media
               binMgr.DiscardDirtyMedia(listOfDirtyFilmFrameIndexes, versionFilmFrameList, nullptr, false);
            }
         }
      }

      // skip film frames if the film and video frame lists are the same
      // or if we are committing and the film frame list is locked.
      if ((versionVideoFrameList != versionFilmFrameList) && (lockedFrameListIndex != FRAMING_SELECT_FILM))
      {
         // Find and destroy dirty film frames
         retVal = binMgr.FindDirtyFrames(versionFilmFrameList, 0, -1,
               // all frames
               /* out */ listOfDirtyFilmFrameIndexes);
         if (retVal != 0)
         {
            return retVal;
         }

         retVal = binMgr.DiscardDirtyMedia(listOfDirtyFilmFrameIndexes, versionFilmFrameList);
         if (retVal != 0)
         {
            return retVal;
         }

         // Clean up video frame list if necessary (avoid double deallocates)
         // without trying to destroy media
         if (versionVideoFrameList != versionFilmFrameList)
         {
            retVal = binMgr.FindDirtyFrames(versionVideoFrameList, 0, -1,
                  // all frames
                  /* out */ listOfDirtyVideoFrameIndexes);
            if (retVal == 0)
            {
               // false = don't destroy media
               binMgr.DiscardDirtyMedia(listOfDirtyVideoFrameIndexes, versionVideoFrameList, nullptr, false);
            }
         }
      }

      // DELETE THE HISTORY FOLDER AND METADATA FOLDER
      string versionHistoryFolder = binMgr.getDirtyHistoryFolderPath(versionClip->getClipPathName());

      if (!versionHistoryFolder.empty())
      {
         // WHERE DID THE HISTORY FILES GET DELETED? QQQ
         // NOTE: we don't create Analysis or Motion folders anymore, but still delete them is we find them.
         RemoveDirectory(versionHistoryFolder.c_str());
         auto metaDataFolder = RemoveSubfolder(versionHistoryFolder, "History");
         auto analysisFolder = AddDirSeparator(metaDataFolder) + "Analysis";
         auto analysisMotionFolder = AddDirSeparator(analysisFolder) + "Motion";

         // Delete all the files in the Motion folder
         auto mask = AddDirSeparator(analysisMotionFolder) + "*.ofd";
         WIN32_FIND_DATA w32findData;
         HANDLE hFind = ::FindFirstFile(mask.c_str(), &w32findData);
         while (hFind != INVALID_HANDLE_VALUE)
         {
            auto filename = AddDirSeparator(analysisMotionFolder) + w32findData.cFileName;
            ::DeleteFile(filename.c_str());
            if (::FindNextFile(hFind, &w32findData) == FALSE)
            {
               ::FindClose(hFind);
               hFind = INVALID_HANDLE_VALUE;
            }
         }

         RemoveDirectory(analysisMotionFolder.c_str());
         RemoveDirectory(analysisFolder.c_str());
         RemoveDirectory(metaDataFolder.c_str());
      }

      // HACK TO PREVENT DOUBLE DELETION BY REGULAR CLIP DELETE CODE
      // Also prevents destruction of parent's audio media, which we share!
      clipInitInfo->getClipPtr()->setMediaDestroyed();

   } // end of clipInitInfo containment block

   // Done with media, now actually delete the clip!
   retVal = DeleteOneClip(parentPath, versionClipName);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}
// ---------------------------------------------------------------------------

int TBinManagerForm::RemoveClipFromVersionList(vector<SVersionDataItem *> *versionList, int versionIndex)
{
   // Delete Selected Version Clip and remove it from the version list view
   int retVal;

   // Find the selected version
   vector<SVersionDataItem*>::iterator selectedIter = versionList->begin() + versionIndex;
   vector<SVersionDataItem*>::iterator newIter = selectedIter + 1;

   // Display another clip while we're deleting this one
   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string versionName = (*selectedIter)->versionDisplayName;
   ClipIdentifier versionClipId(masterClipPath, versionName);
   TRACE_0(errout << "REMOVE VERSION CLIP: " << versionClipId);

   if (binManager.doesClipHaveVersions(versionClipId.GetParentPath(), versionName))
   {
      MTIostringstream ostrMessage;
      ostrMessage.str("");
      ostrMessage << "Version " << versionName << " has versions - if you proceed,    " << endl <<
            "ALL versions will be permanently deleted, including    " << endl << "all media files belonging to the version clips!    " <<
            endl << endl << "Do you wish to proceed?";
      retVal = _MTIConfirmationDialog(Handle, ostrMessage.str());
      if (retVal != MTI_DLG_OK)
      {
         // User didn't really want to delete all the versions!
         return 0;
      }
   }

   // OK, I decided to always select the parent clip when a version is deleted
   bool thisWasTheLastVersion = true;
   LoadNewClip(versionClipId.GetParentPath());

   // OK, safe to delete it now
   // First delete all versions of this clip.
   DeleteAllVersionsOfClip(versionClipId);

   // HACK!! First remember the 'dirty media folder' path
   string dirtyMediaFolderPath = binManager.getDirtyMediaFolderPath(versionClipId.GetClipPath());
   binManager.setVersionStatus(versionClipId.GetParentPath(), versionClipId.GetClipName(), versionWasDiscarded);
   // TRACE_0(errout << "DELETING VERSION CLIP: " << versionClipId);
   retVal = DeleteVersionClip(versionClipId);
   if (retVal == 0)
   {
      // Remove it from the display list
      versionList->erase(selectedIter);
      // Try to delete the dirty media folder
      if (!dirtyMediaFolderPath.empty())
      {
         // First remove the metadata ini file
         string metadataIniFilePath = AddDirSeparator(dirtyMediaFolderPath) + CMediaStorageList::getMetadataIniFilename();
         binManager.removeFile(metadataIniFilePath);

         // We used to try to delete the media folder... now we rename it.
         ////binManager.removeDirectory(dirtyMediaFolderPath);
         string buggeredMediaFolderPath = dirtyMediaFolderPath + "-DELETED";
         // TRACE_0(errout << "RENAME " << dirtyMediaFolderPath << " to " << buggeredMediaFolderPath);
         retVal = rename(dirtyMediaFolderPath.c_str(), buggeredMediaFolderPath.c_str());
         // DBTRACE(retVal);
         if (retVal != 0)
         {
            MTIostringstream errmsg;
            errmsg << "Version media folder rename failed because ";

            switch (errno)
            {
            case EEXIST:
               errmsg << "another program is using the " << dirtyMediaFolderPath << " folder";
               break;
            case ENOENT:
               errmsg << "the " << dirtyMediaFolderPath << " folder does not exist";
               break;
            default:
               errmsg << "an unknown error occurred";
               break;
            }

            TRACE_0(errout << errmsg.str());
            retVal = ERR_BIN_MANAGER_CANT_RENAME_MEDIA_FOLDER;
         }
      }
   }
   else
   {
      MTIostringstream ostr;
      ostr << "Attempt to delete version " << versionName << " of clip " << masterClipPath << "    " << endl <<
            "failed with error code = " << retVal << ".    ";
      _MTIErrorDialog(Handle, ostr.str());
   }

   // QQQ this comment does not make sense - it doesn't appear to be re-showing anything.
   // Re-show the alternate clip because I need the OnNewClip of the active
   // tool to be called AFTER the version is deleted if it was the last one
   if (binMgrGUIInterface && thisWasTheLastVersion)
   {
      binMgrGUIInterface->SaveTimelinePositions();
      binMgrGUIInterface->ClipHasChanged();
      binMgrGUIInterface->RestoreTimelinePositions();
   }

   // Update the view
   RepaintVersionListView();
   ReloadProxy();

   return retVal;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::DeleteAllVersionsOfClip(ClipIdentifier clipId)
{
   TRACE_0(errout << " DELETING ALL VERSIONS OF: " << clipId);
   string parentPath = clipId.GetParentPath();
   string clipName = clipId.GetClipName();
   string clipPath = clipId.GetClipPath();
   CBinManager binManager;
   if (!binManager.doesClipHaveVersions(parentPath, clipName))
   {
      // Nothing to do!
      return;
   }

   vector<ClipVersionNumber>versionNumberList;
   binManager.getListOfClipsActiveVersionNumbers(clipId, versionNumberList, true, true);
   for (unsigned j = 0; j < versionNumberList.size(); ++j)
   {
      // Delete a version
      // HACK!! First remember the 'dirty media folder' path
      ClipIdentifier versionClipId = clipId + versionNumberList[j];
      // TRACE_0(errout << " DELETING VERSION: " << versionClipId);
      string versionPath = versionClipId.GetClipPath();
      string dirtyMediaFolderPath = binManager.getDirtyMediaFolderPath(versionPath);
      binManager.setVersionStatus(clipPath, versionClipId.GetClipName(), versionWasDiscarded);
      int retVal = DeleteVersionClip(versionClipId);
      // DBTRACE(retVal);
      if (retVal == 0)
      {
         // Try to delete the dirty media folder
         if (!dirtyMediaFolderPath.empty())
         {
            // First remove the metadata ini file, then the media folder
            string metadataIniFilePath = AddDirSeparator(dirtyMediaFolderPath) + CMediaStorageList::getMetadataIniFilename();
            binManager.removeFile(metadataIniFilePath);

            // We used to try to delete the media folder... now we rename it.
            ////binManager.removeDirectory(dirtyMediaFolderPath);
            string buggeredMediaFolderPath = dirtyMediaFolderPath + "-DELETED";
            // TRACE_0(errout << "RENAME " << dirtyMediaFolderPath << " to " << buggeredMediaFolderPath);
            retVal = rename(dirtyMediaFolderPath.c_str(), buggeredMediaFolderPath.c_str());
            // DBTRACE(retVal);
            if (retVal != 0)
            {
               MTIostringstream errmsg;
               errmsg << "Version media folder rename failed because ";

               switch (errno)
               {
               case EEXIST:
                  errmsg << "another program is using the " << dirtyMediaFolderPath << " folder";
                  break;
               case ENOENT:
                  errmsg << "the " << dirtyMediaFolderPath << " folder does not exist";
                  break;
               default:
                  errmsg << "an unknown error occurred";
                  break;
               }

               TRACE_0(errout << errmsg.str());
            }
         }
      }
      else
      {
         MTIostringstream ostr;
         ostr << "Attempt to delete version " << versionClipId.GetClipName() << " of clip " << versionClipId.GetMasterClipName()
               << "    " << endl << "failed with error code = " << retVal << ".    ";
         _MTIErrorDialog(Handle, ostr.str());
      }
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::DiscardVersionClipChanges(const string & versionClipPath, CTimecode markIn, CTimecode markOut)
{
   // Don't allow operation if a tool is running or a fix is pending
   if (binMgrGUIInterface && !binMgrGUIInterface->IsItSafeToUnloadCurrentClip())
   {
      return;
   } // A change is pending or a tool is running

   // short circuit
   if (versionClipPath.empty())
   {
      _MTIInformationDialog(Handle,
            "You may only discard changes from \"version\" clips!    \n" "(i.e. clips created using the \"Create New Version\"    \n"
            "menu item or hot key)");
      return;
   }

   // Open the clip whose changes are to be discarded
   CBinManager binMgr;
   int retVal;

   string versionClipBinPath;
   string versionClipName;
   binMgr.splitClipFileName(versionClipPath, versionClipBinPath, versionClipName);
   std::auto_ptr<CClipInitInfo>versionClipInfo(binMgr.openClipOrClipIni(versionClipBinPath, versionClipName, &retVal));
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: while trying to discard all changes: call to" << endl << "       CBinManager::openClip failed for clip " <<
            endl << "       " << versionClipPath << endl << "       Return = " << retVal);
      MTIostringstream ostr;
      ostr << "Error: failed to open clip " << versionClipPath << endl << ". Error Code = " << retVal;

      _MTIErrorDialog(Handle, ostr.str());

      return;
   }

   string parentClipPath = versionClipBinPath;
   string parentClipBinPath;
   string parentClipName;
   binMgr.splitClipFileName(parentClipPath, parentClipBinPath, parentClipName);
   std::auto_ptr<CClipInitInfo>parentClipInfo(binMgr.openClipOrClipIni(parentClipBinPath, parentClipName, &retVal));
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: while trying to discard all changes: call to" << endl << "       CBinManager::openClip failed for clip " <<
            endl << "       " << parentClipPath << endl << "       Return = " << retVal);
      MTIostringstream ostr;
      ostr << "Error: failed to open clip " << parentClipPath << endl << ". Error Code = " << retVal;
      _MTIErrorDialog(Handle, ostr.str());
      return;
   }

   // Prep the processing dialog
   if (CommitVersionForm == nullptr)
   {
      CommitVersionForm = new TCommitVersionForm(this);
   }
   CommitVersionForm->Init(TCommitVersionForm::DISCARD_CHANGES, TCommitVersionForm::KEEP_CLIP, &(*versionClipInfo), &(*parentClipInfo));
   CommitVersionForm->SetRange(markIn, markOut);
   CommitVersionForm->SetShowWarningPanel(false);

   // Discard the changes
   ShowModalDialog(CommitVersionForm);

   // Housekeeping
   ReloadProxy();

   // Display the new version of the version clip
   LoadNewVersion(versionClipPath);
}
// ---------------------------------------------------------------------------

void TBinManagerForm::ExportVersionClipFiles(const string &versionClipPath, int markInFrameIndex, int markOutFrameIndex)
{
   // Don't allow operation if a tool is running or a fix is pending
   if (binMgrGUIInterface && !binMgrGUIInterface->IsItSafeToUnloadCurrentClip())
   {
      // A change is pending or a tool is running
      return;
   }

   // short circuit
   if (versionClipPath.empty())
   {
      _MTIInformationDialog(Handle,
            "You may only export files from \"version\" clips!    \n" "(i.e. clips created using the \"Create New Version\"    \n"
            "menu item or hot key)");
      return;
   }

   ClipFileExporter exporter;
   ClipIdentifier clipId(versionClipPath);
   exporter.SetSourceClipId(clipId);

   exporter.SetFrameRange(markInFrameIndex, markOutFrameIndex);

   string targetDirectory = computeDefaultExportDirectory(clipId);
   if (targetDirectory.empty())
   {
      Application->NormalizeTopMosts();
      targetDirectory = MTIBrowseForFolder("", string("Select Export Folder"));
      Application->RestoreTopMosts();

      if (targetDirectory.empty())
      {
         // Cancelled!
         return;
      }
   }

   exporter.SetTargetDirectory(targetDirectory);

   // Prep the processing dialog
   if (ClipFileExportForm == nullptr)
   {
      ClipFileExportForm = new TClipFileExportForm(this);
   }

   ClipFileExportForm->setExporter(&exporter); ;

   // Export the files
   ShowModalDialog(ClipFileExportForm);
}
// ---------------------------------------------------------------------------

string TBinManagerForm::computeDefaultExportDirectory(ClipIdentifier clipId)
{
   int retVal = 0;

   CBinManager binMgr;
   auto clip = binMgr.openClip(clipId, &retVal);
   if (retVal)
   {
      return "";
   }

   CVideoFrameList* videoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_FILM);
   if (videoFrameList == nullptr)
   {
      return "";
   }

   CVideoFrame *frame = videoFrameList->getFrame(0);
   CVideoField *field = frame ? frame->getField(0) : nullptr;
   if (frame == nullptr || field == nullptr)
   {
      binMgr.closeClip(clip);
      return "";
   }

   CMediaLocation mediaLocation = field->getMediaLocation();
   MTIassert(mediaLocation.isMediaTypeImageFile());
   if (!mediaLocation.isMediaTypeImageFile())
   {
      binMgr.closeClip(clip);
      return "";
   }

   CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
   string sourceFilename = mediaAccess->getImageFileName(mediaLocation);

   string versionPath = AddDirSeparator(GetFilePath(sourceFilename));
   string versionName = GetFileLastDir(versionPath);
   size_t spacePos = versionName.rfind(' ');
   if (spacePos != string::npos)
   {
      versionName = versionName.substr(0, spacePos);
   }

   string versionNumberText = clipId.GetVersionDisplayName();
   if (versionNumberText[0] == '(' && versionNumberText[versionNumberText.length() - 1] == ')')
   {
      versionNumberText = versionNumberText.substr(1, versionNumberText.length() - 2);
   }

   versionName = versionName + "_" + versionNumberText;

   string parentFolder = AddDirSeparator(GetFileAllButLastDir(versionPath));
   string exportFolder = parentFolder + "EXPORTS";
   string suggestedTargetFolder = AddDirSeparator(exportFolder) + versionName;
   BOOL success = CreateDirectory((LPCTSTR)(exportFolder.c_str()), 0);
   if (!success && GetLastError() != ERROR_ALREADY_EXISTS)
   {
      binMgr.closeClip(clip);
      return "";
   }

   success = CreateDirectory((LPCTSTR)(suggestedTargetFolder.c_str()), 0);
   if (!success && GetLastError() != ERROR_ALREADY_EXISTS)
   {
      binMgr.closeClip(clip);
      return "";
   }

   return suggestedTargetFolder;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::OKButtonClick(TObject *Sender)
{
   if (ScanListSaveButton->Enabled)
   {
      // In "Edit" Mode, so save possible changes
      ScanListSaveButtonClick(nullptr);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipListViewChanging(TObject *Sender, TListItem *Item, TItemChange Change, bool &AllowChange)
{

   if (ComponentState.Contains(csDestroying))
   {
      // short-circuit if the form is being destroyed; bad things happen
      // when we try to access members in here because the destructor has
      // already been called when this event is fired.
      return;
   }

   if (ScanListSaveButton->Enabled)
   {
      // In "Edit" Mode, so save possible changes
      ScanListSaveButtonClick(nullptr);
   }

   return;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CancelButtonClick(TObject *Sender)
{
   // Reset ScanListInfoEditable so it isn't
   GetProjectInfo();
   GetScanListInfo();
   MakeScanListInfoEditable(false);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   CancelButtonClick(Sender);
}

// ---------------------------------------------------------------------------
// This is called every time the ClipListView wants to draw an item or
// a subitem. Since we don't know which subitem is about to be drawn, we
// have to fill in the whole Item object each time!

void __fastcall TBinManagerForm::ClipListViewData(TObject *Sender, TListItem *Item)
{
   int i = Item->Index;

   // Sanity
   if (i >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   // Fill'er up!
   PopulateClipViewItem(*Item, *CurrentBinClipListData->at(i));
}
// ---------------------------------------------------------------------------

void TBinManagerForm::HideColumn(int index)
{
   // For now, once you hide a column, you can never show it again
   ClipListView->Columns->Items[index]->Width = 0;
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetClipListViewItemSelectedState(int index, bool newState)
{
   // Paranoia
   MTIassert(ClipListView->Items->Count == (int) CurrentBinClipListData->size());
   MTIassert(index < (int) CurrentBinClipListData->size());
   if (index >= ClipListView->Items->Count || index > (int) CurrentBinClipListData->size())
   {
      return;
   }

   TListItem *viewItem = ClipListView->Items->Item[index];
	SClipDataItem *dataItem = CurrentBinClipListData->at(index);
   int turnItOn = (!viewItem->Selected) && newState;
   int turnItOff = viewItem->Selected && !newState;

   if (turnItOn)
   {
      dataItem->selectStateChangedBySoftware = true;
      viewItem->Selected = true;
      dataItem->selectStateChangedBySoftware = false;
   }
   else if (turnItOff)
   {
      dataItem->selectStateChangedBySoftware = true;
      viewItem->Selected = false;
      dataItem->selectStateChangedBySoftware = false;
   }
}
// ---------------------------------------------------------------------------

void TBinManagerForm::SetVersionListViewItemSelectedState(vector<SVersionDataItem *> *versionList, int index, bool newState)
{
   // Paranoia
   MTIassert(VersionListView->Items->Count == (int) versionList->size());
   MTIassert(index < (int) versionList->size());
   if (index >= VersionListView->Items->Count || index > (int) versionList->size())
   {
      return;
   }

   TListItem *viewItem = VersionListView->Items->Item[index];
   SVersionDataItem *dataItem = versionList->at(index);
   int turnItOn = (!viewItem->Selected) && newState;
   int turnItOff = viewItem->Selected && !newState;

   if (turnItOn)
   {
      dataItem->versionIsSelected = true; // FIRST!!
      viewItem->Selected = true;
   }
   else if (turnItOff)
   {
      dataItem->versionIsSelected = false; // FIRST!!
      viewItem->Selected = false;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ClipRefreshListSweepTimerTimer(TObject *Sender)
{
   if (BinManagerForm == nullptr || !BinManagerForm->Visible || BinManagerForm->WindowState == wsMinimized)
   {
      return;
   }

	bool result = SweepClipRefreshList();

   // This timer now runsa ll the time!
   // {
   // CAutoThreadLocker lock(ClipRefreshListLock);
   // if (result || (ClipRefreshList.size() > 0))
   // {
   // SweepTimerAutoShutoff = 5;
   // }
   // else if (--SweepTimerAutoShutoff <= 0)
   // {
   // ClipRefreshListSweepTimer->Enabled = false;
   // }
   // }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewColumnClick(TObject *Sender, TListColumn *Column)
{
   VersionListColumnToSort = Column->Index;

   // We're about to scramble the list, so make sure we know what's selected!
   SynchronizeSelectionsIfNecessary();

   // Sort the data list
   SortSelectedClipVersionList();

   // Repaint the view
   RepaintVersionListView();
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewCustomDrawItem(TCustomListView *Sender, TListItem *Item, TCustomDrawState State,
      bool &DefaultDraw)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   int versionIndex = Item->Index;
   if (versionIndex >= (int) versionList.size())
   {
      return; // sanity
   }

   if (CurrentBinClipListData->at(clipIndex)->isHighlighted && EqualIgnoreCase(YellowHilitedVersion.ToDisplayName(),
         versionList.at(versionIndex)->versionDisplayName))
   {
      Sender->Canvas->Brush->Color = clYellow;
      if (!Item->Selected)
      {
         Item->Selected = true;
      }
   }
   else
   {
      Sender->Canvas->Brush->Color = (TColor)0x6a6a6a;
      Sender->Canvas->Font->Color = clWhite;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewCustomDrawSubItem(TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   int versionIndex = Item->Index;
   if (versionIndex >= (int) versionList.size())
   {
      return; // sanity
   }

   TColor color = clBlack;
   if (SubItem == COL_INDEX_VERSION_MODIFIED)
   {
      if (CompareText(Item->SubItems->Strings[SubItem - 1], "unknown") == 0)
      {
         Item->SubItems->Strings[SubItem - 1] = "00:00:00:00";
      }
      if ((CompareText(Item->SubItems->Strings[SubItem - 1], "00:00:00:00") != 0) &&
            (CompareText(Trim(Item->SubItems->Strings[SubItem - 1]), "0") != 0))
      {
         // Red if modified
         color = (TColor)0x0000E8;
      }
   }
   else if (SubItem == COL_INDEX_VERSION_STATUS)
   {
      if (CompareText(Item->SubItems->Strings[SubItem - 1], "Versioned") == 0)
      {
         // Show the word Versioned in red
         color = (TColor)0x0000E8;
      }
   }

   // WTF? I think the version clip is NEVER yellowed! QQQ
   Sender->Canvas->Font->Color = color;
   if (CurrentBinClipListData->at(clipIndex)->isHighlighted && EqualIgnoreCase(YellowHilitedVersion.ToDisplayName(),
         versionList.at(versionIndex)->versionDisplayName))
   {
      Sender->Canvas->Brush->Color = clYellow;
   }
   else
   {
      Sender->Canvas->Font->Color = clWhite;
      Sender->Canvas->Brush->Color = (TColor)0x6a6a6a;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewData(TObject *Sender, TListItem *Item)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   int versionIndex = Item->Index;
   if (versionIndex >= (int) versionList.size())
   {
      return; // sanity
   }

   //////////////////////////HACK///////////////////////////////////////////////
   // Sometimes the isVersioned flag is incorrect, so this is a HACK to
   // make sure it's right.
   string versionName = versionList.at(versionIndex)->versionDisplayName;
   ClipVersionNumber versionNumber(versionName);

   // CAREFUL: lastClickedClipPath is tha last clicked MASTER clip, never a version!
   // So here the version number must be complete.
   ClipIdentifier versionClipId(lastClickedClipPath, versionNumber);

   CBinManager binManager;
   bool &isVersioned = versionList.at(versionIndex)->isVersioned;
   isVersioned = binManager.doesClipHaveVersions(versionClipId);
   TRACE_3(errout << versionClipId << "(index " << versionIndex << ") is" << (isVersioned ? "" : " NOT") << " versioned");
   //////////////////////////HACK///////////////////////////////////////////////

   // Fill'er up!
   PopulateVersionViewItem(*Item, *versionList.at(versionIndex));
}
// ---------------------------------------------------------------------------

void TBinManagerForm::PopulateVersionViewItem(TListItem &listItem, const SVersionDataItem &versionDataItem)
{
   AnsiString tmpStr;

   // Set properties of the List Item
   string paddedVersionName = string("    ") + versionDataItem.versionDisplayName + string("    ");
   listItem.Caption = paddedVersionName.c_str();
   listItem.Data = nullptr;

   // Enter text strings for Clip information columns, left to right

   // Clip Status
   tmpStr = CTrack::queryMediaStatusName(versionDataItem.mediaStatus).c_str();
   if (versionDataItem.isVersioned)
   {
      tmpStr = "Versioned";
   }
   else if (tmpStr == "Cloned")
   {
      // This is a bit awkward... let's see if anyone complains:
      tmpStr = "Unversioned";
   }

   listItem.SubItems->Add(tmpStr);

   // Video media modified
   if (versionDataItem.videoMediaModified == CTimecode::NOT_SET)
   {
      tmpStr = "";
   }
   else
   {
      Timecode2AnsiString(versionDataItem.videoMediaModified, tmpStr);
   }

   listItem.SubItems->Add(tmpStr);

   // Creation Date & Time
   listItem.SubItems->Add(versionDataItem.createDate.c_str());

   // Comments
   listItem.SubItems->Add(versionDataItem.comments.c_str());
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewDblClick(TObject *Sender)
{
   TListItem *listItem = VersionListView->Selected;
   if ((listItem != nullptr) && !lastClickedClipPath.empty())
   {
      CBinManager binManager;
      string versionName = StringToStdString(listItem->Caption.Trim());
      ClipVersionNumber versionNumber(versionName);
      string versionPath = binManager.makeVersionClipPath(lastClickedClipPath, versionNumber);
      string clipFileName = binManager.makeClipFileNameWithExt(versionPath);
      if (binMgrGUIInterface != 0)
      {
         binMgrGUIInterface->ClipDoubleClick(clipFileName);

         lastClickedVersionPath = versionPath;
         RepaintVersionListView();
      }

      // On a modal dialog box, the double click is an ok
      if (FormState.Contains(fsModal))
      {
         // This will end the modal box
         ModalResult = mrOk;
      }
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewKeyPress(TObject *Sender, char &Key)
{
   if (Key == Char(VK_RETURN))
   {
      VersionListViewDblClick(nullptr);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewSelectItem(TObject *Sender, TListItem *Item, bool Selected)
{
   if (ComponentState.Contains(csDestroying))
   {
      // short-circuit if the form is being destroyed; bad things happen
      // when we try to access members in here because the destructor has
      // already been called when this event is fired.
      return;
   }

   if (Item == nullptr)
   {
      // Sometimes item comes in as nullptr, I'm not exactly sure why
      return;
   }

   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;

   int versionIndex = Item->Index;
   if (versionIndex >= (int) versionList.size())
   {
      return; // sanity
   }

   if (Item->Selected != versionList[versionIndex]->versionIsSelected)
   {
      if (Item->Selected)
      {
         VersionListViewDblClick(nullptr);
      }

      for (int i = 0; i < (int) versionList.size(); ++i)
      {
         if (i != versionIndex)
         {
            versionList[i]->versionIsSelected = false;
         }
      }
      versionList[versionIndex]->versionIsSelected = Item->Selected;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionListViewPopupMenuPopup(TObject *Sender)
{
   // Placeholder
   // Todo:
   // Commit disabled if there are versions of this version
   // OR leave enabled and pop up error?
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupCommitVersionMenuItemClick(TObject *Sender)
{
   // Allow user to change mind
   MTIostringstream ostr;
   ostr << "You have requested that all changes made to the selected clip    " << endl <<
         "version be copied back to the parent clip. THERE IS NO UNDO." << endl << "Proceed?";
   int retVal = _MTIConfirmationDialog(Handle, ostr.str());
   if (retVal != MTI_DLG_OK)
   {
      return;
   }

   // It's a go!
   vector<SVersionDataItem*>versionList;
   vector<SVersionDataItem*>::iterator selectedIter;
   if (!FindSelectedVersionClip(versionList, selectedIter))
   {
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   int versionIndex = selectedIter - versionList.begin();
   string versionName = versionList.at(versionIndex)->versionDisplayName;
   string versionClipPath = binManager.makeVersionClipPath(masterClipPath, versionName);

   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->SaveTimelinePositions();
   }

   CommitVersionClipChanges(versionClipPath, CTimecode::NOT_SET, CTimecode::NOT_SET);

   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->RestoreTimelinePositions();
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupExportVersionMenuItemClick(TObject *Sender)
{
   int markInFrameIndex = -1;
   int markOutFrameIndex = -1;
   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->GetCurrentMarks(markInFrameIndex, markOutFrameIndex);
   }

   VersionPopupExportMarkedRangeMenuItem->Enabled = markInFrameIndex >= 0 && markOutFrameIndex >= 0;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupExportFullClipMenuItemClick(TObject *Sender)
{
   // Export all frame files in selected version clip
   vector<SVersionDataItem*>versionList;
   vector<SVersionDataItem*>::iterator selectedIter;
   if (!FindSelectedVersionClip(versionList, selectedIter))
   {
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string versionName = (*selectedIter)->versionDisplayName;
   string versionClipPath = binManager.makeVersionClipPath(masterClipPath, versionName);

   ExportVersionClipFiles(versionClipPath, -1, -1);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupExportMarkedRangeMenuItemClick(TObject *Sender)
{
   // Export marked range frame files in selected version clip
   vector<SVersionDataItem*>versionList;
   vector<SVersionDataItem*>::iterator selectedIter;
   if (!FindSelectedVersionClip(versionList, selectedIter))
   {
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string versionName = (*selectedIter)->versionDisplayName;
   string versionClipPath = binManager.makeVersionClipPath(masterClipPath, versionName);

   int markInFrameIndex = -1;
   int markOutFrameIndex = -1;
   if (binMgrGUIInterface)
   {
      binMgrGUIInterface->GetCurrentMarks(markInFrameIndex, markOutFrameIndex);
   }

   ExportVersionClipFiles(versionClipPath, markInFrameIndex, markOutFrameIndex);
}
// ---------------------------------------------------------------------------

bool TBinManagerForm::FindSelectedVersionClip(vector<SVersionDataItem*> &versionList, vector<SVersionDataItem*>::iterator &selectedIter)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);

   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return false;
   }

   versionList = CurrentBinClipListData->at(clipIndex)->versionList;

   // There should only be exactly one clip selected
   for (selectedIter = versionList.begin(); selectedIter != versionList.end(); ++selectedIter)
   {
      if ((*selectedIter)->versionIsSelected)
      {
         break;
      }
   }

   if (selectedIter == versionList.end())
   {
      TRACE_0(errout << "INTERNAL ERROR: Can't find selected version!");
      return false;
   }

   return true;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupDiscardChangesMenuItemClick(TObject *Sender)
{
   // Discard all changes in selected version clip

   // Allow user to change mind
   MTIostringstream ostr;
   ostr << "You have requested that all changes made to the selected clip    " << endl <<
         "version be discarded. THERE IS NO UNDO. Proceed?";

   int retVal = _MTIConfirmationDialog(Handle, ostr.str());
   if (retVal != MTI_DLG_OK)
   {
      return;
   }

   // This asks the tools if you can
   std::string message;
   auto status = binMgrGUIInterface->CanWeDiscardOrCommit(true, message);
   if (status == TOOL_RETURN_CODE_OPERATION_REFUSED)
   {
      return;
   }

   // It's a go!
   vector<SVersionDataItem*>versionList;
   vector<SVersionDataItem*>::iterator selectedIter;
   if (!FindSelectedVersionClip(versionList, selectedIter))
   {
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string versionName = (*selectedIter)->versionDisplayName;
   string versionClipPath = binManager.makeVersionClipPath(masterClipPath, versionName);

   DiscardVersionClipChanges(versionClipPath, CTimecode::NOT_SET, CTimecode::NOT_SET);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CreateVersionOfVersionMenuItemClick(TObject *Sender)
{
   string masterClipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(masterClipName);

   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   int versionIndex;

   for (versionIndex = 0; versionIndex < (int) versionList.size(); ++versionIndex)
   {
      if (versionList.at(versionIndex)->versionIsSelected)
      {
         break;
      }
   }

   if (versionIndex >= (int) versionList.size())
   {
      TRACE_0(errout << "INTERNAL ERROR: Can't find selected version!");
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string parentVersionName = versionList.at(versionIndex)->versionDisplayName;
   string parentVersionClipPath = binManager.makeVersionClipPath(masterClipPath, parentVersionName);

   CreateNewVersionClip(parentVersionClipPath);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupEditCommentMenuItemClick(TObject *Sender)
{
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);

   if (clipIndex < 0 || clipIndex >= (int) CurrentBinClipListData->size())
   {
      return;
   }

   vector<SVersionDataItem*>&versionList = CurrentBinClipListData->at(clipIndex)->versionList;
   int versionIndex;

   for (versionIndex = 0; versionIndex < (int) versionList.size(); ++versionIndex)
   {
      if (versionList.at(versionIndex)->versionIsSelected)
      {
         break;
      }
   }

   if (versionIndex >= (int) versionList.size())
   {
      TRACE_0(errout << "INTERNAL ERROR: Can't find selected version!");
      return;
   }

   CBinManager binManager;
   string masterClipPath = binManager.getMasterClipPath(lastClickedClipPath);
   string versionName = versionList.at(versionIndex)->versionDisplayName;
   string versionClipPath = binManager.makeVersionClipPath(masterClipPath, versionName);

   if (EnterCommentForm == nullptr)
   {
      EnterCommentForm = new TEnterCommentForm(this);
   }

   if (EnterCommentForm == nullptr)
   {
      return;
   }

   EnterCommentForm->setComment(versionList.at(versionIndex)->comments);
   EnterCommentForm->setCaption("Edit comment for version " + versionName);
   int result = ShowModalDialog(EnterCommentForm);
   if (result != mrOk)
   {
      // Cancelled
      return;
   }

   versionList.at(versionIndex)->comments = EnterCommentForm->getComment();

   int status;
   auto clip = binManager.openClip(versionClipPath, &status);
   if (clip == nullptr)
   {
      TRACE_0(errout << "INTERNAL ERROR: Can't write comment to " << versionClipPath);
      return;
   }

   clip->setDescription(EnterCommentForm->getComment());
   clip->writeFile(); // force a .clp file write - paranoia
   binManager.closeClip(clip);

   RepaintVersionListView();
}
// ---------------------------------------------------------------------------

void TBinManagerForm::CheckRootNodeStorage(const string &strRootPath)
{
	MTIostringstream ostr;
	ULARGE_INTEGER lFreeBytesAvailableToCaller; // variable to
	// receive free
   // bytes on disk
   // available to
   // the caller
   ULARGE_INTEGER lTotalNumberOfBytes; // variable to receive
   // number of bytes on disk
   ULARGE_INTEGER lTotalNumberOfFreeBytes; // variable to receive
   // free bytes on disk
   bool bRet = ::GetDiskFreeSpaceEx(strRootPath.c_str(), &lFreeBytesAvailableToCaller, &lTotalNumberOfBytes, &lTotalNumberOfFreeBytes);
   if (bRet == 0) // failure
   {
      ostr << "Unable to determine amount of free storage" << endl << "available on " << strRootPath << ".";
		_MTIErrorDialog(Handle, ostr.str());
      return;
   }

   MTI_INT64 llFree = lFreeBytesAvailableToCaller.QuadPart;

   if (llFree < 128 * 1024 * 1024)
	{
		ostr << "DANGEROUS CONDITION:" << endl
			  << endl
			  << "The storage available on the shared drive" << endl
			  << "is dangerously low. Data corruption may" << endl
			  << "occur. Proceed at your own risk." << endl
			  << endl
			  << "Please clean up the disk that contains" << endl
			  << strRootPath;
		_MTIErrorDialog(Handle, ostr.str());
	}
   else if (llFree < 1024 * 1024 * 1024)
   {
		ostr << "CRITICAL CONDITION:" << endl
			  << endl
			  << "The storage available on the shared drive" << endl
			  << "is critically low." << endl
			  << endl
			  << "Please clean up the disk that contains" << endl
			  << strRootPath;
		_MTIErrorDialog(Handle, ostr.str());
   }

   return;
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VersionPopupDeleteMenuItemClick(TObject *Sender)
{
   // Delete Selected Version Clip
   int retVal = _MTIConfirmationDialog(Handle,
         "Really delete this version clip and permanently    \n" "destroy any modified media associated with it?    ");
   if (retVal == MTI_DLG_CANCEL)
   {
      return;
   }

   vector<SVersionDataItem*>versionList;
   vector<SVersionDataItem*>::iterator selectedIter;
   if (!FindSelectedVersionClip(versionList, selectedIter))
   {
      return;
   }

   // Remove the version from the display
   int selectedIndex = selectedIter - versionList.begin();
   retVal = RemoveClipFromVersionList(&versionList, selectedIndex);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: Failed to delete version clip" << " (error code " << retVal << ")");
   }

   // Need to regenerate the master clip's version list IsVersioned flags
   string clipName = GetFileNameWithExt(lastClickedClipPath);
   int clipIndex = FindClipListIndexByClipName(clipName);
   RefreshOneClipListItemByListIndex(clipIndex);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::BinPopupShareInAndOutMarksClick(TObject *Sender)
{
   CBinManager binManager;
   string iniFileName = binManager.makeClipDesktopIniFileName(rightClickedBinPath);

   BinPopupShareInAndOutMarks->Checked = !BinPopupShareInAndOutMarks->Checked;

   CIniFile *ini = CreateIniFile(iniFileName);
   ini->WriteBool("General", "ClipsShareInAndOutMarks", BinPopupShareInAndOutMarks->Checked);
   DeleteIniFile(ini);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
   switch (Key)
   {
   case MTK_PAGEUP:
   case MTK_PAGEDOWN:
      if (binMgrGUIInterface)
      {
         binMgrGUIInterface->FormKeyDown(Key, Shift);
      }

      Key = 0;
      break;

   case MTK_SLASH:
      if (Shift.Contains(ssShift) || Shift.Contains(ssAlt) || Shift.Contains(ssCtrl))
      {
         break;
      }
      TBRefreshMenuItemClick(nullptr);
      Key = 0;
      break;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
   switch (Key)
   {
   case MTK_PAGEUP:
   case MTK_PAGEDOWN:
      if (binMgrGUIInterface)
      {
         binMgrGUIInterface->FormKeyUp(Key, Shift);
      }

      Key = 0;
      break;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::FileClipNameEditKeyPress(TObject *Sender, char &Key)
{
   if (Key == '\r')
   {
      CreateFileClipButtonClick(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::VideoClipNameEditKeyPress(TObject *Sender, char &Key)
{
   if (Key == '\r')
   {
      CreateClipButtonClick(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::CountNumberOfFixesMenuItemClick(TObject *Sender)

{
   // Count fixes for all selected clips.
   StringList selectedClipList;
   int selectCount = GetSelectedClips(selectedClipList);

   // Except if a version clip is slelected, we only count fixes for that one clip.
   string theBinPath = lastClickedBinPath;

   if (!lastClickedVersionPath.empty())
   {
      selectCount = 1;

      int n = lastClickedVersionPath.length();
      int i = n - 1;
      while ((lastClickedVersionPath[i] != '\\') && (i > 0))
      {
         i--;
      }

      theBinPath = lastClickedVersionPath.substr(0, i + 1);

      selectedClipList.clear();
      selectedClipList.push_back(lastClickedVersionPath.substr(i + 1, n));
   }

   if (selectCount < 1)
   {
      return;
   }

   if (CountFixesUnit == nullptr)
   {
      CountFixesUnit = new TCountFixesUnit(BinManagerForm);
   }

   CountFixesUnit->SetBinPath(theBinPath);
   CountFixesUnit->SetClipNameList(selectedClipList);
   ShowModalDialog(CountFixesUnit);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::ChooseFolderOrChooseFileRadioButtonClick(TObject *Sender)
{
   bool chooseFolder = ChooseFolderRadioButton->Checked;
   bool chooseFile = ChooseFileRadioButton->Checked;
   ClipFolderBrowserButton->Enabled = chooseFolder;
   ClipFileBrowserButton->Enabled = chooseFile;
   FileClipFileNameEdit->Enabled = chooseFile;

   CImageFileMediaAccess::UseBinarySearchToFindAllFiles(chooseFolder);
}
// ---------------------------------------------------------------------------

void __fastcall TBinManagerForm::RelinkMediaFolderMenuItemClick(TObject *Sender)
{
   ClipIdentifier relinkClipId(lastClickedClipPath);
   MTIassert(binMgrGUIInterface != nullptr);
   if (binMgrGUIInterface != nullptr)
   {
      binMgrGUIInterface->RelinkClipMedia(relinkClipId);
   }
}
//---------------------------------------------------------------------------


