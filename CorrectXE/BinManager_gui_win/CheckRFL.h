/*
	File:	CheckRFL.h
	By:	Kevin Manbeck
	Date:	June 2004

	The CCheckRFL class is used to compare the .rfl file against all
	active clips.  Any errors are reported and repaired if
	possible.

*/

#ifndef CHECK_RFL_H
#define CHECK_RFL_H

#include "machine.h"

#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::ostream;

class CVideoFrameList;
class CVideoProxy;

class CBlockRun
{
public:
  CBlockRun ();
  ~CBlockRun ();

  MTI_INT64 llStart;		// in blocks
  MTI_INT64 llLength;		// in blocks
  string strClipName;
  string strMediaIdentifier;
  int iVideoTrack;
  bool bOrphanFlag;
};

class CVideoStore
{
public:
  CVideoStore ();
  ~CVideoStore ();

  MTI_INT64 llRawDiskSize;	// in blocks
  MTI_INT64 llRawDiskSizeActual;
  string strSection;
  string strMediaIdentifier;
  string strFreeListFile;
  string strRawVideoDirectory;

  vector <CBlockRun *> listInUse;
  vector <CBlockRun *> listFree;
  vector <CBlockRun *> listRFL;
};

class CCheckRFL
{
public:
  CCheckRFL ();
  ~CCheckRFL ();

  int LoadRawDiskCfg ();
  int RepairRawDiskCfg (int iVideoStore);
  string getVideoStoreCfg ();
  void findFirstVideoStore (string &strName, MTI_INT64 &llLength,
		MTI_INT64 &llActualLength, bool &bConflict);
  void findNextVideoStore (string &strName, MTI_INT64 &llLength,
		MTI_INT64 &llActualLength, bool &bConflict);

  void findFirstRFL (string &strName, bool &bConflict);
  void findNextRFL (string &strName, bool &bConflict);
  int RepairRFL (int iVideoStore);
  void findFirstBadClip (string &strName);
  void findNextBadClip (string &strName);
  void findFirstMediaOverlap (string &strNameA, string &strNameB);
  void findNextMediaOverlap (string &strNameA, string &strNameB);

  int StartProcessing ();
  int HaltProcessing ();
  float ProgressMeter0 ();
  float ProgressMeter1 ();
  bool IsFinished (int *ipErrorCondition);
  string getTrackName ();

private:
  bool bRequestStop;
  bool bFinishedFlag;
  int iErrorCondition;
  int iTotalTrack;
  int iCurrentTrack;
  int iTotalFrame;
  int iCurrentFrame;

  string strVideoStoreCfg;

  void *vpDoProcessing;

  vector <CVideoStore *> listVideoStore;
  vector <CBlockRun *> listInUse;
  vector <string> listMediaOverlapA;
  vector <string> listMediaOverlapB;


  vector <CVideoStore *>::iterator SearchVideoStore;
  unsigned int iSearchBadClip;
  unsigned int iSearchMediaOverlap;

  void DoProcessing (void *vpReserved);
  void ReleaseStorage ();
  void FindAndProcessClips (bool bCountOnly);
  void BinDirectoryRecurse (string &strPath, bool bCountOnly);
  void ExamineAllClips (string &strPath, bool bCountOnly);
  void ExamineOneTrack (const string &strPath, const string &strClip,
		int iVideoSelect, CVideoProxy *vpp);
  void AnalyzeResults ();
  void SortRuns ();
  void FindFreeRuns ();
  void FindRFL ();
  void FindOverlap ();

  vector <string> strlBadClip;

friend void CallDoProcessing (void *vp, void *vpReserved);
};

#endif
