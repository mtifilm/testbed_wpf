//---------------------------------------------------------------------------

#ifndef HeaderDumpUnitH
#define HeaderDumpUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <string>
using std::string;
//---------------------------------------------------------------------------

class THeaderDumpForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *ControlPanel;
        TRichEdit *HeaderDumpMemo;
        TLabel *FilePathLabel;
        TButton *CloseButton;
        void __fastcall CloseButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall THeaderDumpForm(TComponent* Owner);

        void Clear();
        void Set(const string &newText);
};
//---------------------------------------------------------------------------
extern PACKAGE THeaderDumpForm *HeaderDumpForm;
//---------------------------------------------------------------------------
#endif
