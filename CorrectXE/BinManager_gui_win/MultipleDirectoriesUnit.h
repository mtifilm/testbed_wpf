//---------------------------------------------------------------------------

#ifndef MultipleDirectoriesUnitH
#define MultipleDirectoriesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>

#define SEARCH_LOCATION_BEGINNING 1
#define SEARCH_LOCATION_END       2

//---------------------------------------------------------------------------
class TMultipleDirectoriesForm : public TForm
{
__published:	// IDE-managed Components
    TLabel *Label1;
    TCheckListBox *DirectoryCheckListBox;
    TButton *CheckAllButton;
    TButton *UncheckAllButton;
    TCheckBox *CreateSuperClipCheckBox;
    TLabel *Label2;
    TBevel *Bevel1;
    TBevel *Bevel2;
    TButton *GoAwayButton;
    TLabel *Label3;
    TLabel *Label4;
    TBevel *Bevel3;
    TEdit *SuperClipNameEdit;
    TBitBtn *OkButton;
    TBitBtn *BitBtnCancel;
    void __fastcall CheckAllButtonClick(TObject *Sender);
    void __fastcall UncheckAllButtonClick(TObject *Sender);
    void __fastcall DirectoryCheckListBoxClickCheck(TObject *Sender);
    void __fastcall DirectoryCheckListBoxClick(TObject *Sender);
    void __fastcall GoAwayButtonClick(TObject *Sender);
    void __fastcall SuperClipNameEditChange(TObject *Sender);
    void __fastcall CreateSuperClipCheckBoxClick(TObject *Sender);
    void __fastcall OkButtonClick(TObject *Sender);
    void __fastcall OkButtonUpdate(void);
    int  __fastcall GetNumChecked(void);
private:	// User declarations
public:		// User declarations
    __fastcall TMultipleDirectoriesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMultipleDirectoriesForm *MultipleDirectoriesForm;
//---------------------------------------------------------------------------
#endif
