//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BinMgrTestUnit.h"
#include "ClipAPI.h"
#include "DeleteBinUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TBinMgrTestForm *BinMgrTestForm;
//---------------------------------------------------------------------------

__fastcall TBinMgrTestForm::TBinMgrTestForm(TComponent* Owner)
   : TForm(Owner)
{
   currentClip = 0;
}
//---------------------------------------------------------------------------

void __fastcall TBinMgrTestForm::OpenClick(TObject *Sender)
{
   binMgrGUIInterface->ShowBinManagerGUI(true);

}
//---------------------------------------------------------------------------

string TBinMgrTestForm::getCurrentClipFileName()
{
   return currentClipFileName;
}

void TBinMgrTestForm::setCurrentClipFileName(const string& clipFileName)
{
   currentClipFileName = clipFileName;
   BinMgrTestForm->ClipName->Text = clipFileName.c_str();
}

void CNavBinMgrGUIInterface::ClipDoubleClick(const string& clipFileName)
{
   BinMgrTestForm->OpenClipByName(clipFileName);
}

void CNavBinMgrGUIInterface::ClipDeleted(const string& clipFileName)
{
   if (BinMgrTestForm->getCurrentClipFileName() == clipFileName)
      {
//      BinMgrTestForm->setCurrentClipFileName("");
      BinMgrTestForm->ClipName->Text = "Clip Deleted";
      }
}

void __fastcall TBinMgrTestForm::ExitClick(TObject *Sender)
{
   Close();   
}

//---------------------------------------------------------------------------

void __fastcall TBinMgrTestForm::FormCreate(TObject *Sender)
{
   binMgrGUIInterface = new CNavBinMgrGUIInterface;
}

//---------------------------------------------------------------------------

void __fastcall TBinMgrTestForm::FormDestroy(TObject *Sender)
{
   if (binMgrGUIInterface != 0)
      {
      delete binMgrGUIInterface;
      binMgrGUIInterface = 0;
      }
}

//---------------------------------------------------------------------------
ClipSharedPtr OpenClipByName(const string &Name);

void TBinMgrTestForm::OpenClipByName(const string &clipFileName)
{
   CBinManager binMgr;
   ostringstream ostr;

   auto newClip = ::OpenClipByName(clipFileName);
   if (newClip == 0)
      {
      DebugMsg(theError.getMessage());
      return;
      }

   if (currentClip != 0 && currentClip != newClip)
      {
      binMgr.closeClip(currentClip);
      currentClip = 0;
      }

   ostr.str("");
   ostr << "Opened new clip " << clipFileName;
   DebugMsg(ostr);
   
   setCurrentClipFileName(clipFileName);
   currentClip = newClip;

   int timeTrackCount = currentClip->getTimeTrackCount();
   ostr.str("");
   ostr << "Time Track count = " << timeTrackCount;
   DebugMsg(ostr);
   
   for (int i = 0; i < timeTrackCount; ++i)
      {
      ostr.str("");
      ostr << " Time Track " << i;
      CTimeTrack *timeTrack;
      timeTrack = currentClip->getTimeTrack(i);
      if (timeTrack == 0)
         {
         ostr << " ERROR: getTimeTrack returned NULL pointer ";
         DebugMsg(ostr);
         continue;
         }

      ETimeType timeType = timeTrack->getTimeType();
      ostr << " Type: " << CTimeTrack::queryTimeTypeName(timeType)
           << " (" << timeType << ")  AppId ["
           << timeTrack->getAppId() << "]";
      DebugMsg(ostr);

      int frameCount = timeTrack->getTotalFrameCount();
      ostr.str("");
      ostr << "  Total Frame Count: " << frameCount;
      DebugMsg(ostr);

      for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
         {
         ostr.str("");
         ostr << "    Frame " << frameIndex;
         if (timeType == TIME_TYPE_KEYKODE)
            {
            CKeykodeFrame *frame = timeTrack->getKeykodeFrame(frameIndex);
            if (frame == 0)
               {
               ostr << " getKeykodeFrame returned NULL pointer";
               DebugMsg(ostr);
               break;
               }
            ETimeType frameTimeType = frame->getTimeType();
            ostr << " " << CTimeTrack::queryTimeTypeName(frameTimeType)
                 << " (" << frameTimeType << ")" ;
            if (frameTimeType != timeType)
               {
               ostr << " ERROR: Does not match TimeTrack's Time Type";
               DebugMsg(ostr);
               break;
               }
            ostr << " [" << frame->getKeykodeStr() << "]";
            DebugMsg(ostr);
            }
         else if (timeType == TIME_TYPE_TIMECODE)
            {
            CTimecodeFrame *frame = timeTrack->getTimecodeFrame(frameIndex);
            if (frame == 0)
               {
               ostr << " getTimecodeFrame returned NULL pointer";
               DebugMsg(ostr);
               break;
               }
               
            ETimeType frameTimeType = frame->getTimeType();
            ostr << " " << CTimeTrack::queryTimeTypeName(frameTimeType)
                 << " (" << frameTimeType << ")" ;
            if (frameTimeType != timeType)
               {
               ostr << " ERROR: Does not match TimeTrack's Time Type";
               DebugMsg(ostr);
               break;
               }

            CTimecode timecode = frame->getTimecode();
            string timecodeStr;
            timecode.getTimeString(timecodeStr);
            ostr << " [" << timecodeStr << "]";
            DebugMsg(ostr);
            }
         }
      }
}

void TBinMgrTestForm::DebugMsg(const string &msg)
{
   DebugOutput->Lines->Add(msg.c_str());
}

void TBinMgrTestForm::DebugMsg(ostringstream &ostr)
{
   DebugMsg(ostr.str());
}

//---------------------------------------------------------------------------

//----------------MakeTrueClipName----------------John Mertus----May 2002-----

  string MakeTrueClipName(const string &Name)

//  This returns the actual clip name
//   Note Clips can be named as the following
//     <bin path>/ClipName 
//     <bin path>/ClipName/ClipName
//     <bin path>/ClipName/ClipName.clp
//
//****************************************************************************
{
  // Break the name apart
  string sFP = GetFilePath(Name);
  string sFN = GetFileName(Name);
  string sFE = GetFileExt(Name);
    //
  // Check to see if the name is Starr's double name, if not make it so
  string sTmp = GetFileName(RemoveDirSeparator(sFP));
  if (sTmp == sFN)
    {
      sTmp = AddFileExt(Name,".clp");
    }
  else
    {
      sTmp = AddDirSeparator(sFP + sFN) + sFN + ".clp";
    }
  return sTmp;
}

//-----------------OpenClipByName-----------------John Mertus----Apr 2002-----

    ClipSHaredPtr OpenClipByName(const string &Name)

//  This opens up a clip of name Name
//  if failure, the return is NULL and global error is set; 
//
//****************************************************************************
{
  string sBinName, sClipName, sClipNamePart;
  CBinManager bmBinMgr;
  //
  // First break off file name
  //
  string sName = MakeTrueClipName(Name);
  TRACE_2(errout << "Opening clip with Name " << sName);

  // Add an extension of one does not exist
  sName = AddFileExt(sName, ".clp");
  int iRet = bmBinMgr.splitClipFileName (sName, sBinName, sClipNamePart);
  if (iRet)
    {
        ostringstream os;
        os << "OpenClip: splitClipFileName returned: " 
	   << iRet << endl;
	TRACE_1(errout << os.str());
	theError.set(CLIP_ERROR_BAD_CLIP_FILENAME,os);
	return(NULL);
    }

  auto cpClip = bmBinMgr.openClip(sBinName, sClipNamePart, &iRet);
   if (iRet != 0)
      {
      ostringstream os;
      os << "OpenClip: returned: " << iRet << endl;
      TRACE_1(errout << os.str());
      theError.set(CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE, os);
      return(NULL);
      }

  return(cpClip);
}


void __fastcall TBinMgrTestForm::Button1Click(TObject *Sender)
{
//   DeleteBin("C:\\AA1", "C:\\AA1");
   DeleteBin("D:\\MTIBins", "D:\\MTIBins");
}
//---------------------------------------------------------------------------

