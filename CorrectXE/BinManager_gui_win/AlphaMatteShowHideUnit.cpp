//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AlphaMatteShowHideUnit.h"
#include "ClipAPI.h"
#include "bthread.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TAlphaMatteShowHideForm *AlphaMatteShowHideForm;
//---------------------------------------------------------------------------

__fastcall TAlphaMatteShowHideForm::TAlphaMatteShowHideForm(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TAlphaMatteShowHideForm::FormShow(TObject *Sender)
{
    mCancelledFlag = false;
    mHackCommand = MEDIA_HACK_INVALID;
    ProgressPanel->Visible = false;
    SetupPanel->Visible = true;
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::Reset()
{
    mClipList.clear();
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::SetBinPath(string aBinPath)
{
    mBinPath = aBinPath;
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::AddClip(string aClipName)
{
    mClipList.push_back(aClipName);
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::SetGuiDefaultSelection(
    EDefaultSelection defaultSelection)
{
    // We don't allow "destroy" as a default selection
    switch (defaultSelection)
    {
        case SELECT_HIDE:
            HideAlphaMatteRadioButton->Checked = true;
            break;
        case SELECT_SHOW:
            ShowAlphaMatteRadioButton->Checked = true;
            break;
    }
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::ProcessingThread(void *aThreadArg,
                                               void *aThreadHandle)
{
    TAlphaMatteShowHideForm *this_ =
        reinterpret_cast<TAlphaMatteShowHideForm *>(aThreadArg);

    BThreadBegin (aThreadHandle);

    if (this_ != nullptr)
    {
        this_->HackAllClips();
    }
}

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::HackAllClips()
{
    mTotalClipCount = mClipList.size();

    for (mClipIndex = 0; mClipIndex < mTotalClipCount; ++mClipIndex)
    {
        string &clipName = mClipList[mClipIndex];

        mClipName = clipName.c_str();   // mClipName is an AnsiString
        HackOneClip(clipName);
    }
    mProcessingIsCompleteFlag = true;
}

//---------------------------------------------------------------------------
namespace {  // local function - avoid cluttering the class

bool hackClipDotClpFile(ClipSharedPtr &aClip, EMediaHackCommand aHackCommand)
{
    if (aClip == nullptr)
    {
        return false;
    }
    EAlphaMatteType alphaMatteType = IF_ALPHA_MATTE_INVALID;
    EAlphaMatteType alphaMatteHiddenType = IF_ALPHA_MATTE_INVALID;

    aClip->getAlphaMatteTypes(alphaMatteType, alphaMatteHiddenType);

    // Crap out if the alpha matte cannot be hidden
    bool hasHideableAlphaMatte =
         CImageInfo::queryAlphaMatteTypeCanBeHidden(alphaMatteType);
    bool hasShowableAlphaMatte =
         CImageInfo::queryAlphaMatteTypeCanBeHidden(alphaMatteHiddenType);

    if (!(hasHideableAlphaMatte || hasShowableAlphaMatte))
    {
        return false;
    }

    EAlphaMatteType newAlphaMatteType = IF_ALPHA_MATTE_NONE;
    EAlphaMatteType newAlphaMatteHiddenType = IF_ALPHA_MATTE_NONE;
    switch (aHackCommand)
    {
        case MEDIA_HACK_HIDE_ALPHA_MATTE:
            newAlphaMatteHiddenType = hasHideableAlphaMatte? alphaMatteType
                                                     : alphaMatteHiddenType;
            break;

        case MEDIA_HACK_SHOW_ALPHA_MATTE:
            newAlphaMatteType = hasShowableAlphaMatte? alphaMatteHiddenType
                                                           : alphaMatteType;
            break;

        case MEDIA_HACK_DESTROY_ALPHA_MATTE:
            // Leave them both "NONE"
			break;

		default:
			break;
	}

    int errorCode = aClip->setAlphaMatteTypes(newAlphaMatteType,
                                              newAlphaMatteHiddenType);
    return (errorCode == 0);
}

} // End anonymous namespace

//---------------------------------------------------------------------------
void TAlphaMatteShowHideForm::HackOneClip(string aClipName)
{
    int errorCode = 0;
    CBinManager binManager;

    auto clip = binManager.openClip(mBinPath, aClipName, &errorCode);
    if (errorCode != 0)
    {
        TRACE_0(errout << "ERROR: Could not open Clip " << aClipName << endl
                       << "       in Bin " << mBinPath);
        return;
    }

    // Change the AlphaMatteType and AlphaMatteHiddenType entries in the
    // <clipname>.clp file
    if (!hackClipDotClpFile(clip, mHackCommand))
    {
        binManager.closeClip(clip);
        clip = 0;
        TRACE_0(errout << "ERROR: Alpha Matte Hacker couldn't find an alpha "
                          "matte to hack" << endl
                       << "       in clip " << aClipName);
        return;
    }

    CVideoProxy *mainVideo = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
    if (mainVideo == nullptr)
    {
        binManager.closeClip(clip);
        clip = 0;
        TRACE_0(errout << "ERROR: Clip " << aClipName
                       << " does not have a main video proxy");
        return;
    }

    CVideoFrameList *videoFrameList = mainVideo->getVideoFrameList(
                                                    FRAMING_SELECT_VIDEO);
    if (videoFrameList == nullptr)
    {
        binManager.closeClip(clip);
        clip = 0;
        TRACE_0(errout << "ERROR: Clip " << aClipName
                       << " does not have a main video frame list");
        return;
    }

    mTotalFrameCount = videoFrameList->getTotalFrameCount();

    TRACE_2(errout << "INFO: Hacking alpha matte in media for clip "
                   << aClipName << endl
                   << "      Total " << mTotalFrameCount << " frames");

    int fieldIndex;
    int fieldCount;
    CVideoFrame *frame;
    CVideoField *field;
    int slowStartDelayInMsecs = 100;
    const int delayReductionInMsecs = 20;
    const int interClipDelayInMsecs = 250;

    for (mFrameIndex = 0; mFrameIndex < mTotalFrameCount; ++mFrameIndex)
    {
        // A little hack to allow the UI to catch up with the thread
        // at the start of processing - this is especially useful when
        // there are a small number of frames
        if (slowStartDelayInMsecs > 0)
        {
             MTImillisleep(slowStartDelayInMsecs);
             slowStartDelayInMsecs -= delayReductionInMsecs;
        }

        frame = videoFrameList->getFrame(mFrameIndex);
        if (frame == 0)
        {
            binManager.closeClip(clip);
            clip = 0;
            TRACE_0(errout << "ERROR: Alpha Matte Hacker could not get frame "
                           << mFrameIndex);
            return;
        }

        fieldCount = frame->getTotalFieldCount();
        for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
        {
            field = frame->getField(fieldIndex);
            if (field == 0)
            {
                binManager.closeClip(clip);
                clip = 0;
                TRACE_0(errout << "ERROR: Alpha Matte Hacker could not get"
                               << " field " << fieldIndex
                               << " in frame " << mFrameIndex);
                return;
            }

            errorCode = field->hackMedia(mHackCommand);
            if (errorCode != 0)
            {
                binManager.closeClip(clip);
                clip = 0;
                TRACE_0(errout << "ERROR: "
                                  "Alpha Matte Hacker could not hack media"
                               << endl << "       for field " << fieldIndex
                               << " in frame " << mFrameIndex
                               << endl << "       Return code " << errorCode);
                return;
            }
        }
    }

    // Update the "last media action" (shows up in the clip "status"
    // column in the bin manager clip list view)
    EMediaStatus newLastMediaAction;
	switch (mHackCommand)
    {
        case MEDIA_HACK_HIDE_ALPHA_MATTE:
            newLastMediaAction = MEDIA_STATUS_ALPHA_HIDDEN;
            break;
        case MEDIA_HACK_SHOW_ALPHA_MATTE:
            newLastMediaAction = MEDIA_STATUS_ALPHA_UNHIDDEN;
            break;
        case MEDIA_HACK_DESTROY_ALPHA_MATTE:
            newLastMediaAction = MEDIA_STATUS_ALPHA_DESTROYED;
            break;

		default:
			break;

    }
    mainVideo->setLastMediaAction(newLastMediaAction);

    // Make status change permanent by forcing a write of the .clp
    // file. I think it's completely absurd that I have to do this
    // NOW DONE IN TRACK
    clip->writeFile();    // leaving it in until I port the TRACK STUFF! QQQ

    // Done with the clip
    binManager.closeClip(clip);
    clip = nullptr;

    // Pause to show completed progress bar in UI before continuing on to
    // the next clip in the list
    MTImillisleep(interClipDelayInMsecs);
}
//---------------------------------------------------------------------------

void __fastcall TAlphaMatteShowHideForm::SetupPanelOkButtonClick(
      TObject *Sender)
{
    // These values are all set in the processing thread and read in
    // ProgressTimer(); we initialize them to dummy values here
    mClipName = "";
    mClipIndex = 0;
    mTotalClipCount = 1;    // fixed in thread
    mFrameIndex = 0;
    mTotalFrameCount = 1;   // fixed in thread
    mProcessingIsCompleteFlag = false;
    mCancelledFlag = false;

    // These GUI elements are all maintained by ProgressTimer()
    ClipNameValue->Caption = "";    // gets set in timer
    ClipListProgressBar->Max = static_cast<int>(mTotalClipCount);
    // Min is set to 0 at design time and never changed
    ClipListProgressBar->Position = static_cast<int>(mClipIndex);
    FrameValue->Caption = "";
    OfValue->Caption = "";
    FrameIndexProgressBar->Max = static_cast<int>(mTotalFrameCount);
    // Min is set to 0 at design time and never changed
    FrameIndexProgressBar->Position = static_cast<int>(mFrameIndex);
    ProgressPanelCloseButton->Caption = "Cancel";
    StatusLabel->Visible = false;
    //

    // OK to swap the panels now
    SetupPanel->Visible = false;
    ProgressPanel->Visible = true;

    // Get alpha matte hack command based on setup panel radio buttons
    if (HideAlphaMatteRadioButton->Checked)
    {
        ModifyingModeLabel->Caption =
                "Modifying image files to hide Alpha Matte...";
        mHackCommand = MEDIA_HACK_HIDE_ALPHA_MATTE;
    }
    else if (ShowAlphaMatteRadioButton->Checked)
    {
        ModifyingModeLabel->Caption =
                "Modifying image files to show Alpha Matte...";
        mHackCommand = MEDIA_HACK_SHOW_ALPHA_MATTE;
    }
    else if (DestroyAlphaMatteRadioButton->Checked)
    {
        ModifyingModeLabel->Caption =
                "Modifying image files to destroy Alpha Matte...";
        mHackCommand = MEDIA_HACK_DESTROY_ALPHA_MATTE;
    }

    mProcessingThreadId = BThreadSpawn(ProcessingThread, (void *)this);
    if (mProcessingThreadId == nullptr)
    {
        _MTIErrorDialog(Handle, "Failed to launch processing thread   ");
        ModalResult = mrCancel;
    }
    else
    {
        // fire up the timer, which will track the progress and display it
        ProgressTimer->Enabled = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TAlphaMatteShowHideForm::ProgressPanelCloseButtonClick(
      TObject *Sender)
{
    if (ProgressPanelCloseButton->Caption == "Close")
    {
        ModalResult = mrOk;    // Setting ModalResult will hide the dialog
    }
    else // Hacking is in progress
    {
        ModalResult = mrNone;  // Don't hide the dialog
        mCancelledFlag = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TAlphaMatteShowHideForm::ProgressTimerTimer(
      TObject *Sender)
{
    // Fun with signs
    int clipIndex = static_cast<int>(mClipIndex);
    int totalClipCount = static_cast<int>(mTotalClipCount);
    int frameIndex = static_cast<int>(mFrameIndex);
    int totalFrameCount = static_cast<int>(mTotalFrameCount);

    // Update clip progress panel GUI elements
    if (ClipNameValue->Caption != mClipName)
    {
        ClipNameValue->Caption = mClipName;
    }
    if (ClipListProgressBar->Max != totalClipCount)
    {
        ClipListProgressBar->Max = totalClipCount;
        ClipListProgressBar->Position = clipIndex;
    }
    else if (ClipListProgressBar->Position != clipIndex)
    {
        ClipListProgressBar->Position = clipIndex;
    }

    // Update frame-by-frame progress
    if (FrameIndexProgressBar->Max != totalFrameCount)
    {
        OfValue->Caption = AnsiString(totalFrameCount);
        OfValue->Refresh();
        FrameIndexProgressBar->Max = totalFrameCount;
        FrameValue->Caption = AnsiString(frameIndex);
        FrameValue->Refresh();
        FrameIndexProgressBar->Position = frameIndex;
    }
    else if (FrameIndexProgressBar->Position != frameIndex)
    {
        FrameValue->Caption = AnsiString(frameIndex);
        FrameValue->Refresh();
        FrameIndexProgressBar->Position = frameIndex;
    }

    // Check if we're all done
    if (mProcessingIsCompleteFlag || mCancelledFlag)
    {
        ProgressTimer->Enabled = false;
        ProgressPanelCloseButton->Caption = "Close";
        StatusLabel->Caption = mCancelledFlag? " Cancelled " : " Finished ";
        StatusLabel->Visible = true;
    }

    ProgressPanel->Refresh();

}
//---------------------------------------------------------------------------

