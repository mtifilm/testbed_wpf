//---------------------------------------------------------------------------

#ifndef UnitCheckVideoStoreH
#define UnitCheckVideoStoreH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>

class CCheckRFL;
//---------------------------------------------------------------------------
class TFormCheckVideoStore : public TForm
{
__published:	// IDE-managed Components
        TBitBtn *bbClose;
        TTimer *Timer1;
        TPanel *Panel1;
        TLabel *Label1;
        TListView *ListView1;
        TBitBtn *bbRepair1;
        TLabel *LabelConflictsExist;
        TBitBtn *bbNext;
        TPanel *Panel2;
        TLabel *Label2;
        TLabel *LabelTrack;
        TLabel *LabelTrackName;
        TProgressBar *ProgressBar0;
        TLabel *LabelFrames;
        TProgressBar *ProgressBar1;
        TBitBtn *bbHalt;
        TPanel *Panel4;
        TLabel *Label4;
        TLabel *LabelVideoStoreCfg;
        TListView *ListView4;
        TLabel *LabelSevereError;
        TPanel *Panel5;
        TLabel *Label5;
        TListView *ListView5;
        TBitBtn *bbRepair5;
        TPanel *Panel3;
        TLabel *Label3;
        TLabel *LabelMediaCorruption;
        TListView *ListView3;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall bbHaltClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall bbNextClick(TObject *Sender);
        void __fastcall bbRepair1Click(TObject *Sender);
        void __fastcall bbRepair5Click(TObject *Sender);
        void __fastcall ListView5Click(TObject *Sender);
        void __fastcall ListView1Click(TObject *Sender);
        void __fastcall ListView4Click(TObject *Sender);
        void __fastcall ListView3Click(TObject *Sender);
private:	// User declarations
  CCheckRFL *CheckRFL;
  void InitVideoStore ();
  void InitRFL ();
  void InitCorrupted ();
  void InitOverlap ();
public:		// User declarations
        __fastcall TFormCheckVideoStore(TComponent* Owner);
};
//---------------------------------------------------------------------------
// extern PACKAGE TFormCheckVideoStore *FormCheckVideoStore;
//---------------------------------------------------------------------------
#endif
