/*
	Name:	FileListIO.cpp
	By:	Kevin Manbeck
	Date:	June 10, 2002

	The FileListIO code is used to read and write EDLs and QC reports

*/

#include "MTIio.h"                    // Unix <-> Win wrappers
#include "IniFile.h"
#include <stdio.h>
#include <math.h>

#include "FileListIO.h"
#include "timecode.h"
#include "Clip3.h"
#include "BinManager.h"

#ifdef _WINDOWS
//  Added this code to make it compile on windows
//  -jam 06/18/2003
////#include <algorith>

#endif


CEDLEntry::CEDLEntry()
{
  clear ();
}  /* CEDLEntry */

CEDLEntry::~CEDLEntry()
{
  return;
}  /* ~CEDLEntry */

void CEDLEntry::clear()
{
  strTimeCodeIn="";
  strTimeCodeOut="";
  strSrcScheme="";
  strDstScheme="";
  strDiskScheme="";
}  /* clear */

bool operator < (const CEDLEntry &edlLeft, const CEDLEntry &edlRight)
{
  // base decision on tIn
  if (edlLeft.tIn < edlRight.tIn)
    return true;
  else if (edlLeft.tIn > edlRight.tIn)
    return false;

  // edlLeft.tIn must equal edlRight.tIn
  // base decision on tOut
  if (edlLeft.tOut < edlRight.tOut)
    return true;
  else
    return false;
}

CEDLEntry CEDLEntry::operator = (const CEDLEntry &edl)
{
  if (this == &edl) return *this;
  strTimeCodeIn = edl.strTimeCodeIn;
  strTimeCodeOut = edl.strTimeCodeOut;
  strSrcScheme = edl.strSrcScheme;
  strDstScheme = edl.strDstScheme;
  strDiskScheme = edl.strDiskScheme;
  tIn = edl.tIn;
  tOut = edl.tOut;
  tStart = edl.tStart;
  return *this;
}

CFileListIO::CFileListIO(const string &strBinPathArg)
{
  ini=0;
  setFileName ("");
  setSrcScheme ("");
  setDstScheme ("");
  setDiskScheme ("");
  setCleaningPage (false);
  setConvertPage (false);

  fpClipInfoFile = NULL;

  // set rest of the values to factory defaults
  FactoryDefaults ();

  // keep track of bin path
  strBinPath = strBinPathArg;


  // open up the user's INI file and read in the starting values
  ini = CreateIniFile (CPMPIniFileName ("FileListIO"));
  if (ini == NULL)
   {
    TRACE_0 (errout << "FATAL ERROR:  unable to open FileListIO ini file");
    assert (false);
   }

  // read in the default bools
  bSortByTimeCode = ini->ReadBoolCreate ("DefaultValues", "SortByTimeCode",
		bSortByTimeCode);
  bCleaningPage = ini->ReadBoolCreate ("DefaultValues", "CleaningPage",
		bCleaningPage);
  bConvertPage = ini->ReadBoolCreate ("DefaultValues", "ConvertPage",
		bConvertPage);

  // read in the default strings
  strFileName = ini->ReadStringCreate ("DefaultValues", "FileName",
		strFileName);
  strSrcScheme = ini->ReadStringCreate ("DefaultValues", "SrcScheme",
		strSrcScheme);
  strDstScheme = ini->ReadStringCreate ("DefaultValues", "DstScheme",
		strDstScheme);
  strDiskScheme = ini->ReadStringCreate ("DefaultValues", "DiskScheme",
		strDiskScheme);

  strOverlapTimeCode = ini->ReadStringCreate ("DefaultValues",
		"OverlapTimeCode", strOverlapTimeCode);
  strAutoDuration = ini->ReadStringCreate ("DefaultValues", "AutoDuration",
		strAutoDuration);

  strSrcRefFrameI = ini->ReadStringCreate ("DefaultValues", "SrcRefFrameI",
		strSrcRefFrameI);
  strDstRefFrameI = ini->ReadStringCreate ("DefaultValues", "DstRefFrameI",
		strDstRefFrameI);
  strSrcRefFrameII = ini->ReadStringCreate ("DefaultValues", "SrcRefFrameII",
		strSrcRefFrameII);
  strDstRefFrameII = ini->ReadStringCreate ("DefaultValues", "DstRefFrameII",
		strDstRefFrameII);
  strSrcRefFrameIII = ini->ReadStringCreate ("DefaultValues", "SrcRefFrameIII",
		strSrcRefFrameIII);
  strDstRefFrameIII = ini->ReadStringCreate ("DefaultValues", "DstRefFrameIII",
		strDstRefFrameIII);
  
  // read in the default bools
  bWriteInPoint = ini->ReadBoolCreate ("DefaultValues", "WriteInPoint",
		bWriteInPoint);
  bWriteOutPoint = ini->ReadBoolCreate ("DefaultValues", "WriteOutPoint",
		bWriteOutPoint);
  bWriteFormat = ini->ReadBoolCreate ("DefaultValues", "WriteFormat",
		bWriteFormat);
  bWriteDate = ini->ReadBoolCreate ("DefaultValues", "WriteDate",
		bWriteDate);
  bWriteDescription = ini->ReadBoolCreate ("DefaultValues", "WriteDescription",
		bWriteDescription);
  bWriteClipName = ini->ReadBoolCreate ("DefaultValues", "WriteClipName",
		bWriteClipName);

}  /* CFileListIO */

CFileListIO::CFileListIO(void)
{
  ini=0;
  setFileName ("");
  setSrcScheme ("");
  setDstScheme ("");
  setCleaningPage (false);
  setConvertPage (false);

  fpClipInfoFile = NULL;

  // set rest of the values to factory defaults
  FactoryDefaults ();

}  /* CFileListIO */

CFileListIO::~CFileListIO()
{
  // free up memory associated with the CIniFile
  theError.set (0);
  DeleteIniFile (ini);

  CloseClipInfoFile ();
  
}  /* ~CFileListIO */

void CFileListIO::FactoryDefaults()
{
  // values used in INPUT
  setOverlapTimeCode ("00:00:01:00");
  setAutoDuration ("00:00:01:00");
  setSortByTimeCode (false);
  setSrcRefFrameI ("01:00:00:00");
  setDstRefFrameI ("01:00:00:00");
  setSrcRefFrameII ("02:00:00:00");
  setDstRefFrameII ("02:00:00:00");
  setSrcRefFrameIII ("03:00:00:00");
  setDstRefFrameIII ("03:00:00:00");
  setMergeClips(true);

  // values used in OUTPUT
  bWriteInPoint = true;
  bWriteOutPoint = true;
  bWriteFormat = false;
  bWriteDate = false;
  bWriteDescription = false;
  bWriteClipName = true;
}  /* FactoryDefaults */

void CFileListIO::setOverlapTimeCode (const string &strNew)
{
  strOverlapTimeCode = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "OverlapTimeCode", strOverlapTimeCode);
}  /* setOverlapTimeCode */

void CFileListIO::setBinPath (const string &strNew)
{
  strBinPath = strNew;
}  /* setBinPath */

void CFileListIO::setAutoDuration (const string &strNew)
{
  strAutoDuration = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "AutoDuration", strAutoDuration);
}  /* setAutoDuration */

void CFileListIO::setSortByTimeCode (bool bNew)
{
  bSortByTimeCode = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "SortByTimeCode", bSortByTimeCode);
}  /* setSortByTimeCode */

void CFileListIO::setCleaningPage (bool bNew)
{
  bCleaningPage = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "CleaningPage", bCleaningPage);
}  /* setCleaningPage */

void CFileListIO::setConvertPage (bool bNew)
{
  bConvertPage = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "ConvertPage", bConvertPage);
}  /* setConvertPage */

void CFileListIO::setFileName (const string &strNew)
{
  strFileName = strNew;

  if (ini)
   {
    ini->WriteString ("DefaultValues", "FileName", strFileName);
   }
}  /* setFileName */

void CFileListIO::setSrcScheme (const string &strNew)
{
  strSrcScheme = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "SrcScheme", strSrcScheme);
}  /* setSrcScheme */

void CFileListIO::setMergeClips (bool bNew)
{
  bMergeClips = bNew;

  if (ini)
    ini->WriteBool("DefaultValues", "MergeClips", bMergeClips);
}  /* setMergeClips */

void CFileListIO::setDstScheme (const string &strNew)
{
  strDstScheme = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "DstScheme", strDstScheme);
}  /* setDstScheme */

void CFileListIO::setDiskScheme (const string &strNew)
{
  strDiskScheme = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "DiskScheme", strDiskScheme);
}  /* setDiskScheme */

void CFileListIO::setSrcRefFrameI (const string &strNew)
{
  strSrcRefFrameI = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "SrcRefFrameI", strSrcRefFrameI);
}  /* setSrcRefFrameI */

void CFileListIO::setDstRefFrameI (const string &strNew)
{
  strDstRefFrameI = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "DstRefFrameI", strDstRefFrameI);
}  /* setDstRefFrameI */

void CFileListIO::setSrcRefFrameII (const string &strNew)
{
  strSrcRefFrameII = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "SrcRefFrameII", strSrcRefFrameII);
}  /* setSrcRefFrameII */

void CFileListIO::setDstRefFrameII (const string &strNew)
{
  strDstRefFrameII = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "DstRefFrameII", strDstRefFrameII);
}  /* setDstRefFrameII */

void CFileListIO::setSrcRefFrameIII (const string &strNew)
{
  strSrcRefFrameIII = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "SrcRefFrameIII", strSrcRefFrameIII);
}  /* setSrcRefFrameIII */

void CFileListIO::setDstRefFrameIII (const string &strNew)
{
  strDstRefFrameIII = strNew;

  if (ini)
    ini->WriteString ("DefaultValues", "DstRefFrameIII", strDstRefFrameIII);
}  /* setDstRefFrameIII */

void CFileListIO::setWriteInPoint (bool bNew)
{
  bWriteInPoint = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteInPoint", bWriteInPoint);
}  /* setWriteInPoint */

void CFileListIO::setWriteOutPoint (bool bNew)
{
  bWriteOutPoint = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteOutPoint", bWriteOutPoint);
}  /* setWriteOutPoint */

void CFileListIO::setWriteFormat (bool bNew)
{
  bWriteFormat = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteFormat", bWriteFormat);
}  /* setWriteFormat */

void CFileListIO::setWriteDate (bool bNew)
{
  bWriteDate = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteDate", bWriteDate);
}  /* setWriteDate */

void CFileListIO::setWriteDescription (bool bNew)
{
  bWriteDescription = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteDescription", bWriteDescription);
}  /* setWriteDescription */

void CFileListIO::setWriteClipName (bool bNew)
{
  bWriteClipName = bNew;

  if (ini)
    ini->WriteBool ("DefaultValues", "WriteClipName", bWriteClipName);
}  /* setWriteClipName */

string CFileListIO::getOverlapTimeCode ()
{
  return strOverlapTimeCode;
}  /* getOverlapTimeCode */

string CFileListIO::getAutoDuration ()
{
  return strAutoDuration;
}  /* getAutoDuration */

bool CFileListIO::getSortByTimeCode ()
{
  return bSortByTimeCode;
}  /* getSortByTimeCode */

bool CFileListIO::getCleaningPage ()
{
  return bCleaningPage;
}  /* getCleaningPage */

bool CFileListIO::getConvertPage ()
{
  return bConvertPage;
}  /* getConvertPage */

string CFileListIO::getFileName ()
{
  return strFileName;
}  /* getFileName */

string CFileListIO::getSrcScheme ()
{
  return strSrcScheme;
}  /* getSrcScheme */

string CFileListIO::getDstScheme ()
{
  return strDstScheme;
}  /* getDstScheme */

string CFileListIO::getDiskScheme ()
{
  return strDiskScheme;
}  /* getDiskScheme */

bool CFileListIO::getMergeClips ()
{
  return bMergeClips;
}  /* getDstScheme */

string CFileListIO::getSrcRefFrameI ()
{
  return strSrcRefFrameI;
}  /* getSrcRefFrameI */

string CFileListIO::getDstRefFrameI ()
{
  return strDstRefFrameI;
}  /* getDstRefFrameI */

string CFileListIO::getSrcRefFrameII ()
{
  return strSrcRefFrameII;
}  /* getSrcRefFrameII */

string CFileListIO::getDstRefFrameII ()
{
  return strDstRefFrameII;
}  /* getDstRefFrameII */

string CFileListIO::getSrcRefFrameIII ()
{
  return strSrcRefFrameIII;
}  /* getSrcRefFrameIII */

string CFileListIO::getDstRefFrameIII ()
{
  return strDstRefFrameIII;
}  /* getDstRefFrameIII */

string CFileListIO::getBinPath ()
{
  return strBinPath;
}  /* getBinPath */

bool CFileListIO::getWriteInPoint ()
{
  return bWriteInPoint;
}  /* getWriteInPoint */

bool CFileListIO::getWriteOutPoint ()
{
  return bWriteOutPoint;
}  /* getWriteOutPoint */

bool CFileListIO::getWriteFormat ()
{
  return bWriteFormat;
}  /* getWriteFormat */

bool CFileListIO::getWriteDate ()
{
  return bWriteDate;
}  /* getWriteDate */

bool CFileListIO::getWriteDescription ()
{
  return bWriteDescription;
}  /* getWriteDescription */

bool CFileListIO::getWriteClipName ()
{
  return bWriteClipName;
}  /* getWriteClipName */

// Badly named - it's really creating marked or real clips, not clip schemes
int CFileListIO::GenerateClipSchemes ()
{
  int iRet;

  // check for errors
  if (DoesDirectoryExist (strBinPath) == false)
   {
    theError.set(UTIL_ERROR_NO_BIN, strBinPath + " is an invalid bin name");
    return UTIL_ERROR_NO_BIN;
   }

  // clear the EDL list
  edlList.clear();

  // Open file and read in entries
  iRet = ReadEntries ();
  if (iRet)
    return iRet;

  // sort the entries
  SortEntries ();

  // merge the entries
  // Note for historical reasons, these still may not be merged if
  // cleaning page is false
  if (getMergeClips())
     MergeEntries ();

  iRet = CreateMarkedClips ();

  return iRet;
}  /* GenerateClipSchemes */

int CFileListIO::ReadEntries ()
{
  FILE *fp;
  bool bMissingAutoPad=false, bMissingSrcScheme=false, bMissingDstScheme=false;

  // is the file name BLANK?
  if (strFileName == "")
   {
    theError.set(UTIL_ERROR_FILE_NAME, "No file specified");
    return UTIL_ERROR_FILE_NAME;
   }

  // open file
  fp = fopen (strFileName.c_str(), "r");
  if (fp == NULL)
   {
    theError.set(UTIL_ERROR_FILE_OPEN, strFileName + " could not be opened");
    return UTIL_ERROR_FILE_OPEN;
   }

  // read until file is empty
  char caLine[1024];
  while (true)
   {
    char *cp = fgets (caLine, sizeof(caLine), fp);
    if (feof(fp) && (cp == NULL)) break;		// done
    // check for an error
    if (cp == NULL)
     {
      fclose (fp);
      theError.set(UTIL_ERROR_FILE_READ, (string)"Error reading file " + strFileName);
      return UTIL_ERROR_FILE_READ;
     }

    // Does this line have a time code?
    CEDLEntry edlLocal;
    if (LookForTimeCode (edlLocal, caLine))
     {
      // assign format for source and destination // QQQ WTF? aren't these constant??
      if (strSrcScheme != "")
        edlLocal.strSrcScheme = strSrcScheme;
      if (strDstScheme != "")
        edlLocal.strDstScheme = strDstScheme;
      if (strDiskScheme != "")
        edlLocal.strDiskScheme = strDiskScheme;

      // if not attempting to do a convert, set SrcScheme equal to DstScheme
      if (bConvertPage == false)
       {
        edlLocal.strSrcScheme = edlLocal.strDstScheme;
       }

      // is format missing?
      if (edlLocal.strSrcScheme == "")
       {
        bMissingSrcScheme = true;
       }
      if (edlLocal.strDstScheme == "")
       {
        bMissingDstScheme = true;
       }

      // is auto pad missing?
      if (edlLocal.strTimeCodeOut=="" &&
		(strAutoDuration == "00:00:00:00" || bCleaningPage == false))
       {
        bMissingAutoPad = true;
       }

      // if possible, assign time codes
      if (AssignTimeCode (edlLocal))
       {
        // add this edl element to the list
        edlList.push_back (edlLocal);
       }
     }
   }

  fclose (fp);

  // did we find any time codes?
  if (edlList.size() == 0)
   {
    // report back something helpful.  Four possibilities:
    // 1.  No timecodes found
    // 2.  missing src scheme
    // 3.  missing dst scheme
    // 4.  missing autopad value

    // check for DST scheme first
    if (bMissingDstScheme)
     {
      theError.set(UTIL_ERROR_MISSING_SCHEME_DST, "No clip data scheme specified");
      return UTIL_ERROR_MISSING_SCHEME_DST;
     }
    else if (bMissingSrcScheme)
     {
      theError.set(UTIL_ERROR_MISSING_SCHEME_SRC, "No list data scheme specified");
      return UTIL_ERROR_MISSING_SCHEME_SRC;
     }
    else if (bMissingAutoPad)
     {
      theError.set(UTIL_ERROR_MISSING_AUTOPAD, "Padding timecode missing");
      return UTIL_ERROR_MISSING_AUTOPAD;
     }
    else
     {
      theError.set(UTIL_ERROR_NO_TIMECODE, "No timecodes were found in list");
      return UTIL_ERROR_NO_TIMECODE;
     }
   }

  return 0;
}  /* ReadEntries */

bool CFileListIO::LookForTimeCode (CEDLEntry &edl, char *cpLine)
/*
	Look for 1 or 2 time code strings in cpLine.  Return true
	if found.  Return false if not found.
*/
{
  bool bRet = false;

  // clear the CEDLEntry
  edl.clear();

/*
	A time code is xx:xx:xx:xx or xx:xx:xx.xx, where x is a digit.

	Take 11 characters at a time and analyze.
*/

  char *cp;
  cp = cpLine;
  while (cp[0] != '\0')
   {
    char caTimeCode[12];
    int iRet = IsTimeCode (cp, caTimeCode);
    if (iRet)
     {
      if (edl.strTimeCodeIn=="")
       {
        edl.strTimeCodeIn = caTimeCode;
        bRet = true;
       }
      else if (edl.strTimeCodeOut=="")
       {
        // only keep strTimeCodeOut if it is different from strTimeCodeIn
        if (edl.strTimeCodeIn.compare (caTimeCode) != 0)
         {
          edl.strTimeCodeOut = caTimeCode;
         }
       }
      // advance pointer
      cp += iRet;
     }
    else
     {
      // not a time code, so advance pointer
      cp++;
     }
   }

  // if the timecodes are not valid, do not keep them
  if (edl.strTimeCodeIn < "00:00:10:00")
    return false;

  if (edl.strTimeCodeOut != "" &&
	edl.strTimeCodeOut <= edl.strTimeCodeIn)
    return false;

  // print out timecodes found:
  TRACE_3 (errout <<
	"IN:  " << edl.strTimeCodeIn << "  OUT:  " << edl.strTimeCodeOut);

  return bRet;
}  /* LookForTimeCode */

int CFileListIO::IsTimeCode (char *cp, char *cpResult)
/*
	Analyze 11 characters to determine if cp looks like

		xx:xx:xx:xx or xx:xx:xx.xx

	If so, return true
*/
{
  int iLen;

  iLen = strlen (cp);

  // timecodes of the form HH:MM:SS:FF
  if (
	iLen >= 11 &&
	(cp[0] >= '0' && cp[0] <= '9')		&&
	(cp[1] >= '0' && cp[1] <= '9')		&&
	 cp[2] == ':'				&&
	(cp[3] >= '0' && cp[3] <= '9')		&&
	(cp[4] >= '0' && cp[4] <= '9')		&&
	 cp[5] == ':'				&&
	(cp[6] >= '0' && cp[6] <= '9')		&&
	(cp[7] >= '0' && cp[7] <= '9')		&&
	(cp[8] == ':' || cp[8] == '.')		&&
	(cp[9] >= '0' && cp[9] <= '9')		&&
	(cp[10] >= '0' && cp[10] <= '9')
     )
   {
    cpResult[0] = cp[0];
    cpResult[1] = cp[1];
    cpResult[2] = ':';
    cpResult[3] = cp[3];
    cpResult[4] = cp[4];
    cpResult[5] = ':';
    cpResult[6] = cp[6];
    cpResult[7] = cp[7];
    cpResult[8] = ':';
    cpResult[9] = cp[9];
    cpResult[10] = cp[10];
    cpResult[11] = '\0';
    return 11;
   }

  // timecodes of the form HH MM SS FF
  if (
	iLen >= 11 &&
	(cp[0] >= '0' && cp[0] <= '9')		&&
	(cp[1] >= '0' && cp[1] <= '9')		&&
	 cp[2] == ' '				&&
	(cp[3] >= '0' && cp[3] <= '9')		&&
	(cp[4] >= '0' && cp[4] <= '9')		&&
	 cp[5] == ' '				&&
	(cp[6] >= '0' && cp[6] <= '9')		&&
	(cp[7] >= '0' && cp[7] <= '9')		&&
	(cp[8] == ' ')				&&
	(cp[9] >= '0' && cp[9] <= '9')		&&
	(cp[10] >= '0' && cp[10] <= '9')
     )
   {
    cpResult[0] = cp[0];
    cpResult[1] = cp[1];
    cpResult[2] = ':';
    cpResult[3] = cp[3];
    cpResult[4] = cp[4];
    cpResult[5] = ':';
    cpResult[6] = cp[6];
    cpResult[7] = cp[7];
    cpResult[8] = ':';
    cpResult[9] = cp[9];
    cpResult[10] = cp[10];
    cpResult[11] = '\0';
    return 11;
   }

  // timecodes of the form HHMMSSFF
  if (
	iLen >= 8 &&
	(cp[0] >= '0' && cp[0] <= '9')		&&
	(cp[1] >= '0' && cp[1] <= '9')		&&
	(cp[2] >= '0' && cp[2] <= '9')		&&
	(cp[3] >= '0' && cp[3] <= '9')		&&
	(cp[4] >= '0' && cp[4] <= '9')		&&
	(cp[5] >= '0' && cp[5] <= '9')		&&
	(cp[6] >= '0' && cp[6] <= '9')		&&
	(cp[7] >= '0' && cp[7] <= '9')
     )
   {
    cpResult[0] = cp[0];
    cpResult[1] = cp[1];
    cpResult[2] = ':';
    cpResult[3] = cp[2];
    cpResult[4] = cp[3];
    cpResult[5] = ':';
    cpResult[6] = cp[4];
    cpResult[7] = cp[5];
    cpResult[8] = ':';
    cpResult[9] = cp[6];
    cpResult[10] = cp[7];
    cpResult[11] = '\0';
    return 8;
   }

  // couldn't find a time code
  return 0;
}  /* IsTimeCode */

bool CFileListIO::AssignTimeCode (CEDLEntry &edl)
{
  CClipInitInfo ciiSrc;
  CClipInitInfo ciiDst;

  if (edl.strSrcScheme == "" || edl.strDstScheme == "")
    return false;

  // get the source and destination schemes
  ciiSrc.initFromClipScheme(edl.strSrcScheme);
  ciiDst.initFromClipScheme(edl.strDstScheme);

  // define some local timecode for the source
  CTimecode tSrcIn = ciiSrc.getTimecodePrototype();
  CTimecode tSrcOut = ciiSrc.getTimecodePrototype();
  CTimecode tSrcStart = ciiSrc.getTimecodePrototype();


  // how many frames of autopad?
  tSrcIn.setTimeASCII ("00:00:00:00");
  tSrcOut.setTimeASCII (strAutoDuration.c_str());
  int iAutoPadL = (tSrcOut - tSrcIn) / 2;
  int iAutoPadR = (tSrcOut - tSrcIn) - iAutoPadL;
  if (iAutoPadL <= 0)
    iAutoPadL = 0;
  if (iAutoPadR <= 0)
    iAutoPadR = 0;

  // perform auto pad, if necessary
  if (edl.strTimeCodeOut == "")
   {
    if (iAutoPadL == 0 && iAutoPadR == 0)
     {
      return false;
     }
    else
     {
      // strTimeCodeIn becomes the center of the clip
      tSrcIn.setTimeASCII (edl.strTimeCodeIn.c_str());
      tSrcStart.setTimeASCII(edl.strTimeCodeIn.c_str());
      tSrcOut = tSrcIn + iAutoPadR;
      tSrcIn = tSrcIn + (-iAutoPadL);
     }
   }
  else
   {
    tSrcIn.setTimeASCII (edl.strTimeCodeIn.c_str());
    tSrcOut.setTimeASCII (edl.strTimeCodeOut.c_str());
    tSrcStart.setTimeASCII(edl.strTimeCodeIn.c_str());
   }


  // define in and out time code for the destination
  edl.tIn = ciiDst.getTimecodePrototype();
  edl.tOut = ciiDst.getTimecodePrototype();
  edl.tStart = ciiDst.getTimecodePrototype();

  // All time code conversions assume:
  //    1.  film-based material
  //    2.  Aa frame starts at RefFrame

  float fPulldownFactor = 1.;
  if (edl.tIn.getFramesPerSecond() == 30 &&
	tSrcIn.getFramesPerSecond() != 30)
   {
    // Adding a 3:2 pulldown
    fPulldownFactor = 5./4.;
   }
  else if (tSrcIn.getFramesPerSecond() == 30 &&
	edl.tIn.getFramesPerSecond() != 30)
   {
    // Removing a 3:2 pulldown
    fPulldownFactor = 4./5.;
   }

  // need some local variables
  CTimecode tSrcRefI = ciiSrc.getTimecodePrototype();
  tSrcRefI.setTimeASCII (strSrcRefFrameI.c_str());
  CTimecode tDstRefI = ciiDst.getTimecodePrototype();
  tDstRefI.setTimeASCII (strDstRefFrameI.c_str());
  CTimecode tSrcRefII = ciiSrc.getTimecodePrototype();
  tSrcRefII.setTimeASCII (strSrcRefFrameII.c_str());
  CTimecode tDstRefII = ciiDst.getTimecodePrototype();
  tDstRefII.setTimeASCII (strDstRefFrameII.c_str());
  CTimecode tSrcRefIII = ciiSrc.getTimecodePrototype();
  tSrcRefIII.setTimeASCII (strSrcRefFrameIII.c_str());
  CTimecode tDstRefIII = ciiDst.getTimecodePrototype();
  tDstRefIII.setTimeASCII (strDstRefFrameIII.c_str());

  int iSrcDiff;
  int iDstDiff;

        // if the AlternateCheckbox is checked use the ref frames
  if (getConvertPage ()) {

    if (tSrcIn < tSrcRefII)
     {
     // Use the part I reference frame
     iSrcDiff = tSrcIn - tSrcRefI;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tIn = tDstRefI + iDstDiff;
     iSrcDiff = tSrcOut - tSrcRefI;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
	 edl.tOut = tDstRefI + iDstDiff;
     iSrcDiff = tSrcStart - tSrcRefI;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tStart = tDstRefI + iDstDiff;
     }
    else if (tSrcIn < tSrcRefIII)
     {
     // Use the part II reference frame
     iSrcDiff = tSrcIn - tSrcRefII;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tIn = tDstRefII + iDstDiff;
     iSrcDiff = tSrcOut - tSrcRefII;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tOut = tDstRefII + iDstDiff;
     iSrcDiff = tSrcStart - tSrcRefII;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tStart = tDstRefII + iDstDiff;

     }
    else
     {
     // Use the part III reference frame
     iSrcDiff = tSrcIn - tSrcRefIII;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tIn = tDstRefIII + iDstDiff;
     iSrcDiff = tSrcOut - tSrcRefIII;
	 iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tOut = tDstRefIII + iDstDiff;
     iSrcDiff = tSrcStart - tSrcRefIII;
     iDstDiff = mti_rint ((float)iSrcDiff * fPulldownFactor);
     edl.tStart = tDstRefIII + iDstDiff;

     }
   }
  else
   {
    // don't bother with the reference frames
    edl.tIn = tSrcIn;
    edl.tOut = tSrcOut;
    edl.tStart = tSrcStart;
   }


  return true;
}  /* AssignTimeCode */

void CFileListIO::SortEntries ()
{
  if (bSortByTimeCode == false || bCleaningPage == false)
    return;

  // sort in ascending order
  stable_sort (edlList.begin(), edlList.end());
  return;
}  /* SortEntries */

void CFileListIO::MergeEntries ()
{
  int iOverlapFrame;

  // Set the values of iOverlapFrame
  CClipInitInfo ciiSrc;
  ciiSrc.initFromClipScheme(edlList[0].strSrcScheme);

  CTimecode tSrcIn = ciiSrc.getTimecodePrototype();
  CTimecode tSrcOut = ciiSrc.getTimecodePrototype();

  // how many frames of overlap
  tSrcIn.setTimeASCII ("00:00:00:00");
  tSrcOut.setTimeASCII (strOverlapTimeCode.c_str());
  iOverlapFrame = (tSrcOut - tSrcIn);
  if (iOverlapFrame <= 0)
    iOverlapFrame = 0;

  if (iOverlapFrame == 0 || bCleaningPage == false)
    return;

  vector<CEDLEntry>::iterator edlp = edlList.begin() + 1;
  while (edlp != edlList.end())
   {
    bool bMerge=false;
    // Is the IN point before or close enough to the previous OUT?
    if ((edlp[0].tIn - edlp[-1].tOut) < iOverlapFrame)
     {
      // Is the OUT point after the previous IN?
      int iDiff = edlp[-1].tIn - edlp[0].tOut;
      if (iDiff < iOverlapFrame)
       {
        // merge them together
        // Use the second OUT point ...
        if (edlp[0].tOut > edlp[-1].tOut)
         {
          edlp[-1].tOut = edlp[0].tOut;
         }
        // ... and the first IN point.
        if (edlp[0].tIn < edlp[-1].tIn)
         {
          edlp[-1].tIn = edlp[0].tIn;
         }
        edlList.erase(edlp);
        bMerge = true;
       }
     }

    if (bMerge == false)
      edlp++;
   }

  return;
}  /* MergeEntries */

int CFileListIO::CreateMarkedClips ()
{
  int iRet;
  //CClipInitInfo cii;
  CBinManager bm;
  bool markOnly = true;

  // Really create file-based clips. Pretend to create videostore-based clips.
  if (!strDiskScheme.empty())
   {
    CDiskSchemeList diskSchemeList;
    if (0 != diskSchemeList.ReadDiskSchemeFile())
     {
	   TRACE_0(errout << "ERROR: CFileListIO::getWriteClipName: Cannot read disk scheme file");
	 }
    else
     {
      CDiskScheme diskScheme = diskSchemeList.Find(strDiskScheme);
      if (diskScheme.mainVideoMediaRepositoryType == MEDIA_REPOSITORY_TYPE_FILE_SYSTEM)
       {
        markOnly = false;
       }
     }
   }

  for (int iPos = 0; iPos < (int)edlList.size(); iPos++)
   {
    CClipInitInfo cii;
    // init from this destination clip scheme
    cii.initFromClipScheme (edlList[iPos].strDstScheme);
    cii.setBinPath (strBinPath);
    cii.setClipName (bm.makeNextAutoClipName(strBinPath));
    cii.setInTimecode (edlList[iPos].tIn);
    cii.setOutTimecode (edlList[iPos].tOut);

    if (markOnly)
     {
      // videostore -- "mark" the clip by only writing out the clip ini file

      // UGGGH... hack to squirrel away a snapshot of the media location
      cii.setMarkedClipMediaLoc(edlList[iPos].strDiskScheme);
      // End Hack

      iRet = bm.createClipIniFile(cii);
     }
    else
     {
      // image repository -- do full-blown clip creation
      cii.initMediaLocation(edlList[iPos].strDiskScheme);
      auto clip = cii.createClip(&iRet);

      // how hideous is this?
      if (clip != nullptr)
       {
        CBinManager bmBinMgr;
        bmBinMgr.closeClip(clip);
       }
     }
    if (iRet != 0)
     {
      theError.set(iRet, "Clip creation failed");
      return iRet;
     }

      // create the desktop.ini file so we can add a clipstart timecode

     string clipPath = (AddDirSeparator(strBinPath) + cii.getClipName());
     string desktopFilename = AddDirSeparator(clipPath) + "desktop.ini";
     char *username;
#ifdef _Windows
     username = getenv("USERNAME");
#else
     username = getenv("USER");
#endif
     string userStr;
     string tcStr;
     string timeCodeIndexKey = "TimecodeIndex";
     CTimecode timecode(0);
     if (username != 0)
        userStr = username;

   // Create section name that will look like Appname/Username such as
   // "Navigator/starr"
     string sectionName = GetFileName(GetApplicationName()) + '/' + userStr;

     CIniFile* iniFile = CreateIniFile(desktopFilename);
     if (iniFile == NULL)
        return -530;

     timecode = edlList[iPos].tStart;
     timecode.getTimeString(tcStr);
     iniFile->WriteString(sectionName, timeCodeIndexKey, tcStr);


      if (!DeleteIniFile(iniFile))
      return -531;


TRACE_3 (errout << "destination IN:  " << edlList[iPos].tIn << " OUT:  "
		<< edlList[iPos].tOut);
   }

  return 0;
}  /* CreateMarkedClips */

int CFileListIO::OpenClipInfoFile (bool bAppend)
{
  int iNChar = 0;

  // is the file name BLANK?
  if (strFileName == "")
   {
    theError.set(UTIL_ERROR_FILE_NAME, "No file specified");
    return UTIL_ERROR_FILE_NAME;
   }

  // open file
  if (bAppend)
   {
    fpClipInfoFile = fopen (strFileName.c_str(), "a");
   }
  else
   {
    fpClipInfoFile = fopen (strFileName.c_str(), "w");
   }
  if (fpClipInfoFile == NULL)
   {
    theError.set(UTIL_ERROR_FILE_OPEN, strFileName + " could not be opened for write");
    return UTIL_ERROR_FILE_OPEN;
   }

  // write out some header information
  if (fprintf (fpClipInfoFile, "\nMTI Clip Info File\n\n") < 0)
   {
    CloseClipInfoFile ();
    theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
    return UTIL_ERROR_FILE_WRITE;
   }

  if (bWriteInPoint)
   {
    iNChar += 13;
    if (fprintf (fpClipInfoFile, "%13s", "IN Point") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteOutPoint)
   {
    iNChar += 13;
    if (fprintf (fpClipInfoFile, "%13s", "OUT Point") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteFormat)
   {
    iNChar += 15;
    if (fprintf (fpClipInfoFile, "%15s", "Image Format") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteDate)
   {
    iNChar += 23;
    if (fprintf (fpClipInfoFile, "%22s", "Creation Date") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteDescription)
   {
    iNChar += 22;
    if (fprintf (fpClipInfoFile, "%22s", "Description") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteClipName)
   {
    iNChar += 22;
    if (fprintf (fpClipInfoFile, "%22s", "Clip Name") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }


  if (fprintf (fpClipInfoFile, "\n") < 0)
   {
    CloseClipInfoFile ();
    theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
    return UTIL_ERROR_FILE_WRITE;
   }

  while (iNChar > 0)
   {
    if (fprintf (fpClipInfoFile, "-") < 0)
     {
      CloseClipInfoFile ();
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
    iNChar--;
   }

  if (fprintf (fpClipInfoFile, "\n") < 0)
   {
    CloseClipInfoFile ();
    theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
    return UTIL_ERROR_FILE_WRITE;
   }

  return 0;
}  /* OpenClipInfoFile */

void CFileListIO::CloseClipInfoFile ()
{
  fclose (fpClipInfoFile);
  fpClipInfoFile = NULL;
}  /* CloseClipInfoFile */

int CFileListIO::WriteClipInfo (string const &strClipName)
{
  int iRet;

  // check for errors
  if (DoesDirectoryExist (strBinPath) == false)
   {
    theError.set(UTIL_ERROR_NO_BIN, "No Bin path specified");
    return UTIL_ERROR_NO_BIN;
   }

  // Open file and write out entry
  iRet = WriteEntries (strClipName);
  if (iRet)
    return iRet;

  return 0;
}  /* WriteClipInfo */

int CFileListIO::WriteEntries (string const &strClipName)
{
  CClipInitInfo *clipInitInfo;
  CBinManager bmBinMgr;
  int iRet;

  // is the file pointer NULL
  if (fpClipInfoFile == NULL)
   {
    return UTIL_ERROR_NULL_FILE_POINTER;
   }

  // open clip or clip ini file
  clipInitInfo = bmBinMgr.openClipOrClipIni (strBinPath, strClipName, &iRet);
  if (iRet)
   {
    return iRet;
   }

  if (bWriteInPoint)
   {
    char caTimeCode[20];
    clipInitInfo->getInTimecode().getTimeASCII(caTimeCode);
    if (fprintf (fpClipInfoFile, "%13s", caTimeCode) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteOutPoint)
   {
    char caTimeCode[20];
    clipInitInfo->getOutTimecode().getTimeASCII(caTimeCode);
    if (fprintf (fpClipInfoFile, "%13s", caTimeCode) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteFormat)
   {
    if (fprintf (fpClipInfoFile, "%15s",
		clipInitInfo->getImageFormatName().c_str()) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteDate)
   {
    char caLocal[64];
    // change ':' in Create Date so they don't look like time codes
    strcpy (caLocal, clipInitInfo->getCreateDateTime().c_str());
    caLocal[4] = '/';
    caLocal[7] = '/';
    caLocal[10] = ' ';
    caLocal[13] = ':';
    caLocal[16] = ':';
    caLocal[19] = '\0';

    if (fprintf (fpClipInfoFile, "%23s", caLocal) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteDescription)
   {
    char caLocal[64];
    strncpy (caLocal, clipInitInfo->getDescription().c_str(), 20);
    if (fprintf (fpClipInfoFile, "%22s", caLocal) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (bWriteClipName)
   {
    char caLocal[64];
    strncpy (caLocal, clipInitInfo->getClipName().c_str(), 20);
    if (fprintf (fpClipInfoFile, "%22s", caLocal) < 0)
     {
      clipInitInfo->closeClip();   // no longer need opened clip
      delete clipInitInfo;
      theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
      return UTIL_ERROR_FILE_WRITE;
     }
   }

  if (fprintf (fpClipInfoFile, "\n") < 0)
   {
    clipInitInfo->closeClip();   // no longer need opened clip
    delete clipInitInfo;
    theError.set(UTIL_ERROR_FILE_WRITE, strFileName + " write failed");
    return UTIL_ERROR_FILE_WRITE;
   }

  clipInitInfo->closeClip();   // no longer need opened clip
  delete clipInitInfo;

  return 0;
}  /* WriteEntries */


