//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HeaderDumpUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
THeaderDumpForm *HeaderDumpForm;
//---------------------------------------------------------------------------
__fastcall THeaderDumpForm::THeaderDumpForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void THeaderDumpForm::Clear()
{
    HeaderDumpMemo->Lines->Text = "";
}
//---------------------------------------------------------------------------
void THeaderDumpForm::Set(const string &newText)
{
    HeaderDumpMemo->Lines->Text = newText.c_str();
}
//---------------------------------------------------------------------------
void __fastcall THeaderDumpForm::CloseButtonClick(TObject *Sender)
{
    Hide();        
}
//---------------------------------------------------------------------------

