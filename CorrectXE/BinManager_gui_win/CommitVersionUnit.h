//---------------------------------------------------------------------------

#ifndef CommitVersionUnitH
#define CommitVersionUnitH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>

#include <vector>
using std::vector;
#include <string>
using std::string;

#include "timecode.h"

#include "Clip3.h"
//class CClip;
//class CClipInitInfo;
class CVideoFrameList;
//---------------------------------------------------------------------------

class TCommitVersionForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *SetupPanel;
        TPanel *Separator1;
        TButton *SetupPanelOkButton;
        TButton *SetupPanelCancelButton;
        TTimer *ProgressTimer;
        TPanel *ProgressPanel;
        TLabel *StatusLabel;
        TLabel *ParentClip;
        TLabel *VersionClipValue;
        TLabel *ProgressHeaderLabel;
        TProgressBar *FrameIndexProgressBar;
        TPanel *Separator2;
        TButton *ProgressPanelCloseButton;
        TLabel *ParentClipValue;
        TLabel *VersionClipLabel;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *ModifiedFrameCountValue;
        TLabel *ModifiedFrameCountLabel;
        TLabel *FrameLabel;
        TLabel *FrameValue;
        TLabel *OfLabel;
        TLabel *OfValue;
        TLabel *StatusMessageLabel;
        TTimer *AutoCloseTimer;
        TLabel *RangeLabel;
        TLabel *RangeValue;
        void __fastcall ProgressPanelCloseButtonClick(TObject *Sender);
        void __fastcall ProgressTimerTimer(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall SetupPanelOkButtonClick(TObject *Sender);
        void __fastcall AutoCloseTimerTimer(TObject *Sender);

public:

        enum OpMode { INVALID_MODE=0, COMMIT_CHANGES, DISCARD_CHANGES };
        enum ClipFate { INVALID_FATE=0, KEEP_CLIP, DELETE_CLIP };


private:	// User declarations
        CClipInitInfo *mVersionClipInfo;
        CClipInitInfo *mParentClipInfo;

        OpMode mOpMode;
        ClipFate mVersionClipFate;
        int mFirstVideoFrame;
        int mLastVideoFrame;   // -1 => to end of clip
        int mFirstFilmFrame;
        int mLastFilmFrame;    // -1 => to end of clip
        CTimecode mMarkIn;
        CTimecode mMarkOut;

        void *mProcessingThreadId;
        bool mOkToDeleteTheCommittedClip;
        bool mAutoCloseFlag;
        bool mShowWarningPanelFlag;

        void Start();
        static void ProcessingThread(void *aThreadArg, void *aThreadHandle);
        void Process();

		  int CheckAndMaybeFixDirtyMedia(
					const vector<int> &listOfDirtyFrameIndexes,
					CVideoFrameList *versionFrameList);
		  int CommitMedia(
               const vector<int> &listOfDirtyFrameIndexes,
               const CVideoFrameList *versionFrameList,
               CVideoFrameList *parentFrameList);
        int DiscardMedia(
               const vector<int> &listOfDirtyFrameIndexes,
               const CVideoFrameList *versionFrameList,
               CVideoFrameList *parentFrameList,
               bool destroyMediaFlag=true);
        int FindDirtyFrames(
               const CVideoFrameList *aFrameList,
               int firstFrameIndex, int lastFrameIndex,
               /*out*/ vector<int> &aListOfDirtyFrameIndexes);
        int ReplaceHistory(
               const vector<int> &listOfFrameIndexes,
               const CVideoFrameList *sourceFrameList,
               const CVideoFrameList *targetFrameList,
               ClipSharedPtr &sourceClip,
               ClipSharedPtr &targetClip);
        void UpdateAllSiblingVersionClipHistories(
               ClipSharedPtr &parentClip,
               const CVideoFrameList *parentVideoFrameList,
               const vector<int> &listOfDirtyVideoFrameIndexes,
               int firstVideoFrameIndex, int lastVideoFrameIndex,
               const CVideoFrameList *parentFilmFrameList,
               const vector<int> &listOfDirtyFilmFrameIndexes,
               int firstFilmFrameindex, int lastFilmFrameIndex);
        void UpdateOneSiblingVersionClipHistory(
               const vector<int> &listOfCommittedFrameIndexes,
               int firstFrameIndex, int lastFrameIndex,
               const CVideoFrameList *parentFrameList,
               CVideoFrameList *siblingFrameList,
               ClipSharedPtr &parentClip,
               ClipSharedPtr &siblingClip);

        // Processing Thread communication stuff
        // TO thread
        bool mCancelledFlag;
        // FROM thread
        bool mProcessingIsCompleteFlag;
        bool mHaltedOnErrorFlag;
        string mStatusMessage;
			  int mProgressPercent;
        int mFramesRemainingCount;
        //

		 enum WhichDialog { informationDialog, confirmationDialog, errorDialog } _dialogType;
		 bool _showDialog = false;
		 string _dialogMessage;
		 int _dialogResponse = -1;

public:		// User declarations
        __fastcall TCommitVersionForm(TComponent* Owner);
        void Init(OpMode opMode, ClipFate clipFateIfNoChangesLeft,
                  CClipInitInfo *versionClipInfo,
                  CClipInitInfo *parentClipInfo);
        void SetFrameRange(int firstVideoFrame, int lastVideoFrame,
                           int firstFilmFrame,  int lastFilmFrame);
        void SetRange(CTimecode markIn, CTimecode markOut);
        void SetShowWarningPanel(bool showWarningPanelFlag);
};
//---------------------------------------------------------------------------
extern PACKAGE TCommitVersionForm *CommitVersionForm;
//---------------------------------------------------------------------------
#endif
