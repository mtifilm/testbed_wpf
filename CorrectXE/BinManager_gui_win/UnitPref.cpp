//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitPref.h"
#include "MultipleDirectoriesUnit.h"
#include "BinManagerGUIUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma resource "*.dfm"
TFormPref *FormPref;
//---------------------------------------------------------------------------
__fastcall TFormPref::TFormPref(TComponent* Owner)
        : TMTIForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormPref::BitBtnOKClick(TObject *Sender)
{
  // update the settings in the ini file.
  SaveSettings();
}
//---------------------------------------------------------------------------

bool TFormPref::WriteSAP(CIniFile *ini, const string &IniSection)
{
   return true;
}

//---------------------------------------------------------------------------

bool TFormPref::ReadSAP(CIniFile *ini, const string &IniSection)
{
   return true;
}
//---------------------------------------------------------------------------

bool TFormPref::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the preference settings from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  CheckBoxTabVideoIn1->Checked = ini->ReadBool (IniSection,
	"TabVideoIn1", true);
  CheckBoxTabVideoOut1->Checked = ini->ReadBool (IniSection,
	"TabVideoOut1", true);
  CheckBoxTabVideoDur1->Checked = ini->ReadBool (IniSection,
	"TabVideoDur1", true);
  CheckBoxTabVideoName1->Checked = ini->ReadBool (IniSection,
	"TabVideoName1", true);
  CheckBoxTabVideoCreate1->Checked = ini->ReadBool (IniSection,
	"TabVideoCreate1", true);

  CheckBoxTabVideoCen2->Checked = ini->ReadBool (IniSection,
	"TabVideoCen2", true);
  CheckBoxTabVideoDur2->Checked = ini->ReadBool (IniSection,
	"TabVideoDur2", true);
  CheckBoxTabVideoName2->Checked = ini->ReadBool (IniSection,
	"TabVideoName2", true);
  CheckBoxTabVideoCreate2->Checked = ini->ReadBool (IniSection,
	"TabVideoCreate2", true);

  CheckBoxTabFileIn1->Checked = ini->ReadBool (IniSection,
	"TabFileIn1", true);
  CheckBoxTabFileOut1->Checked = ini->ReadBool (IniSection,
	"TabFileOut1", true);
  CheckBoxTabFileDur1->Checked = ini->ReadBool (IniSection,
	"TabFileDur1", true);
  CheckBoxTabFileName1->Checked = ini->ReadBool (IniSection,
	"TabFileName1", true);
  CheckBoxTabFileCreate1->Checked = ini->ReadBool (IniSection,
	"TabFileCreate1", true);

  ShowMultipleDirectoriesFormCheckBox->Checked = ini->ReadBool (IniSection,
    "ShowMultipleDirectoriesForm", true);

  iSearchLocation = ini->ReadInteger(IniSection,
      "MultipleDirectoriesSearchLocation", SEARCH_LOCATION_END);

  switch (iSearchLocation)
  {
    case SEARCH_LOCATION_END:
      EndRadioButton->Checked = true;
      break;
    case SEARCH_LOCATION_BEGINNING:
      BeginningRadioButton->Checked = true;
      break;
    default:
      EndRadioButton->Checked = true;
  }

  return true;
}


bool TFormPref::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the preference settings to the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
  ini->WriteBool (IniSection, "TabVideoIn1", CheckBoxTabVideoIn1->Checked);
  ini->WriteBool (IniSection, "TabVideoOut1", CheckBoxTabVideoOut1->Checked);
  ini->WriteBool (IniSection, "TabVideoDur1", CheckBoxTabVideoDur1->Checked);
  ini->WriteBool (IniSection, "TabVideoName1", CheckBoxTabVideoName1->Checked);
  ini->WriteBool (IniSection, "TabVideoCreate1", CheckBoxTabVideoCreate1->Checked);

  ini->WriteBool (IniSection, "TabVideoCen2", CheckBoxTabVideoCen2->Checked);
  ini->WriteBool (IniSection, "TabVideoDur2", CheckBoxTabVideoDur2->Checked);
  ini->WriteBool (IniSection, "TabVideoName2", CheckBoxTabVideoName2->Checked);
  ini->WriteBool (IniSection, "TabVideoCreate2", CheckBoxTabVideoCreate2->Checked);

  ini->WriteBool (IniSection, "TabFileIn1", CheckBoxTabFileIn1->Checked);
  ini->WriteBool (IniSection, "TabFileOut1", CheckBoxTabFileOut1->Checked);
  ini->WriteBool (IniSection, "TabFileDur1", CheckBoxTabFileDur1->Checked);
  ini->WriteBool (IniSection, "TabFileName1", CheckBoxTabFileName1->Checked);
  ini->WriteBool (IniSection, "TabFileCreate1", CheckBoxTabFileCreate1->Checked);

  ini->WriteBool (IniSection, "ShowMultipleDirectoriesForm", ShowMultipleDirectoriesFormCheckBox->Checked);

  if (BeginningRadioButton->Checked)
    iSearchLocation = SEARCH_LOCATION_BEGINNING;
  else
    iSearchLocation = SEARCH_LOCATION_END;

  ini->WriteInteger (IniSection, "MultipleDirectoriesSearchLocation",
                     iSearchLocation);

  return true;
}
//---------------------------------------------------------------------------

void __fastcall TFormPref::BitBtnCancelClick(TObject *Sender)
{
  // restore all the settings
  RestoreSettings ();
}
//---------------------------------------------------------------------------

bool TFormPref::TabVideoIn1()
{
  return CheckBoxTabVideoIn1->Checked;
}

bool TFormPref::TabVideoOut1()
{
  return CheckBoxTabVideoOut1->Checked;
}

bool TFormPref::TabVideoDur1()
{
  return CheckBoxTabVideoDur1->Checked;
}

bool TFormPref::TabVideoName1()
{
  return CheckBoxTabVideoName1->Checked;
}

bool TFormPref::TabVideoCreate1()
{
  return CheckBoxTabVideoCreate1->Checked;
}

bool TFormPref::TabVideoCen2()
{
  return CheckBoxTabVideoCen2->Checked;
}

bool TFormPref::TabVideoDur2()
{
  return CheckBoxTabVideoDur2->Checked;
}

bool TFormPref::TabVideoName2()
{
  return CheckBoxTabVideoName2->Checked;
}

bool TFormPref::TabVideoCreate2()
{
  return CheckBoxTabVideoCreate2->Checked;
}

bool TFormPref::TabFileIn1()
{
  return CheckBoxTabFileIn1->Checked;
}

bool TFormPref::TabFileOut1()
{
  return CheckBoxTabFileOut1->Checked;
}

bool TFormPref::TabFileDur1()
{
  return CheckBoxTabFileDur1->Checked;
}

bool TFormPref::TabFileName1()
{
  return CheckBoxTabFileName1->Checked;
}

bool TFormPref::TabFileCreate1()
{
  return CheckBoxTabFileCreate1->Checked;
}

bool TFormPref::ShowMultipleDirectoriesForm()
{
  return ShowMultipleDirectoriesFormCheckBox->Checked;
}

int TFormPref::GetMultipleDirectorySearchLocation()
{
  return iSearchLocation;
}

void TFormPref::SetShowMultipleDirectoriesForm(bool bChecked)
{
  ShowMultipleDirectoriesFormCheckBox->Checked = bChecked;
  SaveSettings();
}



