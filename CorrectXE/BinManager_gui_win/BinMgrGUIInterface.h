// BinMgrGUIInterface.h: Interface between an Application program
//                       and the BinManagerGUI DLL
//
/*
*/
//////////////////////////////////////////////////////////////////////

#ifndef BinMgrGUIInterfaceH
#define BinMgrGUIInterfaceH

//////////////////////////////////////////////////////////////////////

#include "binmgrlibint.h"
#include "Clip3.h"
#include "ClipIdentifier.h"
#include <string>
using std::string;
#include <Classes.hpp>

//////////////////////////////////////////////////////////////////////
// Bin Manager GUI Interface Class

// Support functions
enum RECORD_SEQUENCE {RS_NONE, RS_BATCHRECORD, RS_QUICKIN, RS_QUICKOUT, RS_MULTICLIP, RS_SINGLECLIP, RS_QUICKPREVIEWOUT};

// Storage for global information needed by the Record
struct SRecordData
{
   string strNewMediaIdentifier;
   RECORD_SEQUENCE RecordSequence;
};

class MTI_BINMGRLIB_API CBinMgrGUIInterface
{
public:
   // Constructor/Destructor
   CBinMgrGUIInterface();
   virtual ~CBinMgrGUIInterface();

   typedef int lockID_t;

   void ShowBinManagerGUI(bool show, bool dummy=true);

   // Application provides the following function to let the
   // bin manager know if it's OK to unload the current clip
   // at this time (can't unload if a tool is processing, e.g.)
   virtual bool IsItSafeToUnloadCurrentClip() {return true;}

   // Make sure the current clip's desktop.ini is up-to-date
   virtual int SaveCurrentClipState() {return 0;}

   // Application provides the following functions to
   // be notified of various events in the Bin Manager GUI
   virtual void ClipDoubleClick(const string& clipFileName) {}
   virtual void ClipWillBeDeleted (const string & clipFileName) {}
   virtual void ClipWillBeArchived (const string & clipFileName) {}
   virtual void ClipDeleted(const string& clipFileName) {}
   virtual void ClipModified(const string& clipFileName) {}

   // ClipWasRenamed
   //
   // will be called after a clip is renamed.  Override to perform
   // application-specific operations.
   virtual void ClipWasRenamed(const string &oldBinPath,
                               const string &oldClipName,
                               const string &newBinPath,
                               const string &newClipName) { }

   // BinWasRenamed
   //
   // will be called after a bin is renamed.  Override to perform
   // application-specific operations.
   virtual void BinWasRenamed(const string &oldBinPath,
                              const string &newBinPath) { }

   virtual void RelinkClipMedia(ClipIdentifier clipId) { }

   virtual bool WeWantToHackAClip() { return true; }
   virtual void ClipHasChanged() { };
   virtual bool RecordListOfClips(const StringList &slClips, const SRecordData &BatchData) {return true;}

   virtual int CanWeDiscardOrCommit(bool discardFlag, std::string &message)
   {
   	message = "";
      return 0;
   }

   // API for save/restore timeline positions hack
   virtual void SaveTimelinePositions() { }
   virtual void RestoreTimelinePositions() { }

   // GetFirstUnusedFrame returns:
   //
   // 0  -  iVideo and iAudio were retrieved successfully
   // non-zero -  an error occurred; iVideo and iAudio are undefined
   //
   // upon successful return, iVideo will be the first unused video
   // frame.  iAudio will be the first unused audio frame.
   virtual int GetFirstUnusedFrame (const string& clipGUID,
                                    int &iVideo, int &iAudio) { return -1; };

   // SetNewLengthBegin returns:
   //
   // 0  -  it is safe to release storage
   // non-zero -  it is not safe to release storage.  For example,
   // ingest may be occurring.
   //
   // this function will be called before changing the size of the
   // clip.  It provides the data base with the opportunity to lock
   // out activity until the change is complete.
   virtual int SetNewLengthBegin (const string& clipFileName,
                                  int iVideo, int iAudio) { return 0; };

   // SetNewLengthFinish returns:
   //
   // 0  - success
   // non-zero failure
   //
   // this function will be called after changing the size of the
   // clip.    It can be used to release the database.
   virtual int SetNewLengthFinish (const string& clipFileName) { return 0; };


   // Callbacks for safety in shared environments

   // Request*() returns non-negative lock ID if success; negative
   // error and reason if failure

   // Finish*() returns zero if success; non-zero and reason if
   // failure

   // Non-destructive, but need to check if a destructive operation is
   // underway:

   virtual lockID_t RequestClipOpenReader(const string &binPath,
                                          const string &clipName,
                                          string &reason) { return 0; }
   virtual int FinishClipOpenReader(lockID_t lockID,
                                    const string &binPath,
                                    const string &clipName,
                                    string &reason) { return 0; }

   virtual lockID_t RequestBinOpenReader(const string &binPath,
                                         string &reason) { return 0; }
   virtual int FinishBinOpenReader(lockID_t lockID,
                                   const string &binPath,
                                   string &reason) { return 0; }

   // Destructive operations that need to check first:

   virtual lockID_t RequestClipRename(const string &oldBinPath,
                                      const string &oldClipName,
                                      const string &newBinPath,
                                      const string &newClipName,
                                      string &reason) { return 0; }
   virtual int FinishClipRename(lockID_t lockID,
                                const string &oldBinPath,
                                const string &oldClipName,
                                const string &newBinPath,
                                const string &newClipName,
                                string &reason) { return 0; }

   virtual lockID_t RequestBinRename(const string &oldBinPath,
                                     const string &newBinPath,
                                     string &reason) { return 0; }
   virtual int FinishBinRename(lockID_t lockID,
                               const string &oldBinPath,
                               const string &newBinPath,
                               string &reason) { return 0; }

   virtual lockID_t RequestBinDelete(const string &binPath,
                                     string &reason) { return 0; }
   virtual int FinishBinDelete(lockID_t lockID,
                               const string &binPath,
                               string &reason) { return 0; }

   // mlm: to-do: get rid of SetNewLengthBegin() and
   // SetNewLengthFinish() and use these functions instead.  Keep
   // GetFirstUnusedFrame() since there still needs to be a
   // non-locking method of retrieving that information.
   virtual lockID_t RequestClipNewLength (const string &binPath,
                                          const string &clipName,
                                          int &firstUnusedVideoFrame,
                                          int &firstUnusedAudioFrame,
                                          string &reason) { return 0; }
   virtual int FinishClipNewLength (lockID_t lockID,
                                    const string &binPath,
                                    const string &clipName,
                                    int newVideoFrame,
                                    int newAudioFrame,
                                    string &reason) { return 0; } 

   // need separate callback from *NewLength() because even media in
   // use will be deleted
   virtual lockID_t RequestClipDeleteMedia(const string &binPath,
                                           const string &clipName,
                                           string &reason) { return 0; }
   virtual int FinishClipDeleteMedia(lockID_t lockID,
                                     const string &binPath,
                                     const string &clipName,
                                     ETrackType trackType,
                                     string &reason) { return 0; }

   virtual lockID_t RequestClipCreate(const string &newBinPath,
                                      const string &newClipName,
                                      string &reason) { return 0; }
   virtual int FinishClipCreate(lockID_t lockID,
                                const string &newBinPath,
                                const string &newClipName,
                                string &reason) { return 0; }

   virtual lockID_t RequestClipDelete(const string &binPath,
                                      const string &clipName,
                                      string &reason) { return 0; }
   virtual lockID_t RequestClipArchive(const string &binPath,
                                      const string &clipName,
                                      string &reason) { return 0; }
   virtual int FinishClipDelete(lockID_t lockID,
                                const string &binPath,
                                const string &clipName,
                                const string &clipGUID,
                                string &reason) { return 0; }

   virtual int FinishClipArchive(lockID_t lockID,
                                const string &binPath,
                                const string &clipName,
                                const string &clipGUID,
                                string &reason) { return 0; }
   virtual int FinishClipRestore (lockID_t lockID,
                                  const string &strArchiveFileName,
                                  const string &strGUID,
                                  const string &strBinPath,
                                  const string &strClipName,
                                  string &reason) { return 0; }

   virtual lockID_t RequestCheckVideoStore(string &reason) { return 0; }
   virtual int FinishCheckVideoStore(lockID_t lockID,
                                     string &reason) { return 0; }

   virtual int FinishLock(lockID_t lockID, string &reason) { return 0; }

   // call this to handle the specific needs of ClipNewLength locks
   // instead of FinishLock(); do not call this in place of
   // FinishClipNewLength().  The latter should be used to unlock
   // ClipNewLength locks under normal circumstances.
   virtual int FinishClipNewLengthLock (const string &strClient,
                                        string &reason) { return 0; }

   bool IsLockValid(lockID_t lock);


   // flag getters and setters to determine how the UI should look
   void SetVideoStoreUsagePanelFlag(bool newFlag);
   bool GetVideoStoreUsagePanelFlag();
   void SetInOutDurColsFlag(bool newFlag);
   bool GetInOutDurColsFlag();
   void SetMediaDurColsFlag(bool newFlag);
   bool GetMediaDurColsFlag();
   void SetMediaLocColFlag(bool newFlag);
   bool GetMediaLocColFlag();
   void SetArchivedColFlag(bool newFlag);
   bool GetArchivedColFlag();

   // API to pass key events back to the main window
   virtual void FormKeyDown(WORD &Key, TShiftState Shift) {};
   virtual void FormKeyUp(WORD &Key, TShiftState Shift) {};

   // Stupid hack - want to highlight this clip in red.
   virtual string GetRedClipName();

   // Another stupid hack to allow the bin manager gui to get the mark in
   // and mark out for the current clip.
   virtual void GetCurrentMarks(int &markInIndex, int &markOutIndex);


private:
   bool bVSUsagePanel;
   bool bInOutDurCols;
   bool bMediaDurCols;
   bool bMediaLocCol;
   bool bArchivedCol;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef BIN_MGR_GUI_INTERFACE_H

