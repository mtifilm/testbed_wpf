// binmgrlibint.h:  Bin Manager GUI library internal include file.
//
// This file holds various literals and other stuff used to control 
// compiler-specific compilation of the Bin Manager GUI Library
//
/*
$Header: /usr/local/filmroot/BinManager_gui_win/binmgrlibint.h,v 1.1.1.1 2002/07/09 16:19:08 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef BINMGR_LIB_INT_H
#define BINMGR_LIB_INT_H

//////////////////////////////////////////////////////////////////////
// Bin Manager GUI Library API convention
//
// Defines MTI_BINMGRLIB_API which is primarily of interest for building
// Windows DLL (dynamic linked library) version of the Bin Manager GUI Library
//
// Defines MTI_BINMGRLIB_TEMPLATE which is also used in building DLLs.
// In particular, helps export/import "explicit instantiations" of
// templates.  Explanation for this can be found in Microsoft KnowledgeBase
// articles Q168958: Exporting STL Components Inside and Outside of a Class,
// Q172396: Access Violation When Accessing STL Object in DLL
// and Q263633: BUG: Error C2946 on Explicit Instantiation of Imported Templates
//
// Warning: lack of indentation of preprocessor directives makes this
//          difficult to read, be careful if you modify this
 
#if defined(__sgi) || defined(__linux)    // UNIX compilers
#define MTI_BINMGRLIB_API       
#define MTI_BINMGRLIB_TEMPLATE
// End of #ifdef __sgi

#elif defined(_WIN32)         // Windows Win32 API

#if defined(MTI_BINMGRLIB_NO_DLL)  // Non-DLL Win32 build
#define MTI_BINMGRLIB_API
#define MTI_BINMGRLIB_TEMPLATE
// End of #if defined(MTI_BINMGRLIB_NO_DLL)

#else                            // DLL Win32 build

#if defined(_MSC_VER)         // Win32 API with Microsoft compilers
// MTI_BINMGRLIB_API_EXPORT is defined to build the Bin Manager GUI DLL and is
// not defined to build users of the Bin Manager GUI DLL
#if defined(MTI_BINMGRLIB_API_EXPORT)
#define MTI_BINMGRLIB_API __declspec(dllexport)
#define MTI_BINMGRLIB_TEMPLATE
#else
#define MTI_BINMGRLIB_API __declspec(dllimport)
#define MTI_BINMGRLIB_TEMPLATE extern
// Disable warning on extern before template instantiation
#pragma warning(disable: 4231)
#endif


// End of #if defined(_MSC_VER)

#elif defined(__BORLANDC__)   // Win32 API with Borland compilers
// MTI_BINMGRLIB_API_EXPORT is defined to build the Bin Manager GUI DLL and is
// not defined to build users of the Bin Manager GUI DLL
#if defined(MTI_BINMGRLIB_API_EXPORT)
#define MTI_BINMGRLIB_API __declspec(dllexport)
#define MTI_BINMGRLIB_TEMPLATE
#else
#define MTI_BINMGRLIB_API __declspec(dllimport)
#define MTI_BINMGRLIB_TEMPLATE
#endif
// End of #elif defined(__BORLANDC__)

#else
#error Unknown Compiler, expecting Microsoft or Borland
#endif

#endif // End of #if defined(MTI_BINMGRLIB_NO_DLL) #else

// End of #elif defined(_WIN32)

#else
#error Unknown Compiler, expecting SGI or Windows
#endif

//////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler 
// with vector of strings or string pointers
#pragma warning(disable : 4786)
#endif // #ifdef _MSC_VER

//////////////////////////////////////////////////////////////////////

#endif // #ifndef BINMGR_LIB_INT_H

