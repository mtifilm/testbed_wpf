object BinMgrTestForm: TBinMgrTestForm
  Left = 453
  Top = 226
  Caption = 'Bin Manager Tester'
  ClientHeight = 447
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ClipName: TEdit
    Left = 0
    Top = 0
    Width = 273
    Height = 21
    TabOrder = 0
    Text = 'ClipName'
  end
  object DebugOutput: TRichEdit
    Left = 0
    Top = 112
    Width = 332
    Height = 316
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    HideScrollBars = False
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    Zoom = 100
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 428
    Width = 332
    Height = 19
    Panels = <>
    ExplicitTop = 420
    ExplicitWidth = 340
  end
  object Browse: TButton
    Left = 280
    Top = 0
    Width = 57
    Height = 25
    Caption = 'Browse'
    TabOrder = 3
    OnClick = OpenClick
  end
  object ComboBox1: TComboBox
    Left = 0
    Top = 24
    Width = 273
    Height = 21
    TabOrder = 4
    Text = 'ComboBox1'
  end
  object TimeCodeField: VTimeCodeEdit
    Left = 8
    Top = 48
    Width = 72
    Height = 21
    TabOrder = 5
    AllowVTRCopy = False
  end
  object SetTCButton: TButton
    Left = 88
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Set Timecodes'
    TabOrder = 6
  end
  object Edit1: TEdit
    Left = 8
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'Edit1'
  end
  object Button1: TButton
    Left = 224
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 8
    OnClick = Button1Click
  end
  object MainMenu: TMainMenu
    Left = 280
    Top = 32
    object File: TMenuItem
      Caption = 'File'
      object Open: TMenuItem
        Caption = 'Open'
        OnClick = OpenClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit: TMenuItem
        Caption = 'Exit'
        OnClick = ExitClick
      end
    end
  end
end
