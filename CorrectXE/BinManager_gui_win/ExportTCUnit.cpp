//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ExportTCUnit.h"
#include "BinManagerGUIUnit.h"
#include "MTIDialogs.h"
#include "ShowModalDialog.h"

//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TExportTCDialog *ExportTCDialog;
//---------------------------------------------------------------------
__fastcall TExportTCDialog::TExportTCDialog(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
//-------------------ReadSettings-------------------------John Mertus-----Jan 2001---

   bool TExportTCDialog::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the setting of the variables
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//******************************************************************************
{
   string Section = IniSection + "." +  StringToStdString(Name);
   mruFileName.ReadSettings(ini, Section, "FileHistory");
   if (!mruFileName.empty()) FileNameComboBox->Text = mruFileName[0].c_str();

   FileListIO.setWriteInPoint(ini->ReadBool(Section,"WriteInPoint", FileListIO.getWriteInPoint()));
   FileListIO.setWriteOutPoint(ini->ReadBool(Section,"WriteOutPoint", FileListIO.getWriteOutPoint()));
   FileListIO.setWriteDate(ini->ReadBool(Section,"WriteDate", FileListIO.getWriteDate()));
   FileListIO.setWriteDescription(ini->ReadBool(Section,"WriteDescription", FileListIO.getWriteDescription()));
   FileListIO.setWriteFormat(ini->ReadBool(Section,"WriteFormat", FileListIO.getWriteFormat()));
   FileListIO.setWriteClipName(ini->ReadBool(Section,"WriteClipName", FileListIO.getWriteClipName()));

   return true;
}

//-------------------------WriteSettings-------------------John Mertus-----Jan 2001---

   bool TExportTCDialog::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the settings properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//
//******************************************************************************
{
   // Write the current list
   string Section = IniSection + "." +  StringToStdString(Name);

   mruFileName.WriteSettings(ini, Section, "FileHistory");
   ini->WriteBool(Section,"WriteInPoint",FileListIO.getWriteInPoint());
   ini->WriteBool(Section,"WriteOutPoint", FileListIO.getWriteOutPoint());
   ini->WriteBool(Section,"WriteDate", FileListIO.getWriteDate());
   ini->WriteBool(Section,"WriteDescription", FileListIO.getWriteDescription());
   ini->WriteBool(Section,"WriteFormat",  FileListIO.getWriteFormat());
   ini->WriteBool(Section,"WriteClipName", FileListIO.getWriteClipName());

   return true;
}

//---------------------------------------------------------------------------


void TExportTCDialog::ParametersToGui(CFileListIO &flio)
{
   FileNameComboBox->Text = flio.getFileName().c_str();
   HardParametersToGui(flio);
}

void TExportTCDialog::HardParametersToGui(CFileListIO &flio)
{
   InTCCheckBox->Checked = flio.getWriteInPoint();
   OutTCCheckBox->Checked = flio.getWriteOutPoint();
   DateCheckBox->Checked = flio.getWriteDate();
   DescriptionCheckBox->Checked = flio.getWriteDescription();
   ImageFormatCheckBox->Checked = flio.getWriteFormat();
   ClipNameCheckBox->Checked = flio.getWriteClipName();
}

void TExportTCDialog::GuiToParameters(CFileListIO &flio)
{
   flio.setWriteInPoint(InTCCheckBox->Checked);
   flio.setWriteOutPoint(OutTCCheckBox->Checked);
   flio.setWriteDate(DateCheckBox->Checked);
   flio.setWriteDescription(DescriptionCheckBox->Checked);
   flio.setWriteFormat(ImageFormatCheckBox->Checked);
   flio.setWriteClipName(ClipNameCheckBox->Checked);
   flio.setFileName(StringToStdString(FileNameComboBox->Text));
}

void __fastcall TExportTCDialog::FileBrowseButtonClick(TObject *Sender)
{
   if (OpenDialog->Execute())
      FileNameComboBox->Text = OpenDialog->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TExportTCDialog::FileNameComboBoxDropDown(TObject *Sender)
{
   FileNameComboBox->Items->Clear();
   for (unsigned i =0; i < mruFileName.size(); i++)
     FileNameComboBox->Items->Add(mruFileName[i].c_str());

}
//---------------------------------------------------------------------------

void __fastcall TExportTCDialog::OKButtonClick(TObject *Sender)
{

    CFileListIO TmpFileListIO;
	string FileName =  StringToStdString(FileNameComboBox->Text);
    TmpFileListIO.setFileName(FileName);
    ModalResult = mrNone;

    if (DoesFileExist(FileName))
      {
        if (_MTIWarningDialog(Handle, FileName + " Exists\nOverwrite this file?") != MTI_DLG_OK) return;
      }

    GuiToParameters(TmpFileListIO);
    ////TmpFileListIO.setBinPath( StringToStdString(BinManagerForm->CurrentBinPath->Text));
    TmpFileListIO.setBinPath(BinManagerForm->GetSelectedBin());
    if (TmpFileListIO.OpenClipInfoFile(false) != 0)
      {
         _MTIErrorDialog(Handle, theError.getFmtError());
         return;
      }

    for (unsigned i = 0; i < ClipNames.size(); i++)
    if (TmpFileListIO.WriteClipInfo(ClipNames[i]) != 0)
      {
         _MTIErrorDialog(Handle, theError.getFmtError());
         TmpFileListIO.CloseClipInfoFile();
         return;
      }

    TmpFileListIO.CloseClipInfoFile();

    mruFileName.push( StringToStdString(FileNameComboBox->Text));
    GuiToParameters(FileListIO);
    ModalResult = mrOk;

}
//---------------------------------------------------------------------------

        int TExportTCDialog::Execute(const StringList &List)
{
    ClipNames = List;
    return ShowModalDialog(this);
}

void __fastcall TExportTCDialog::RestoreButtonClick(TObject *Sender)
{
   if (_MTIWarningDialog(Handle, "Restore to Design Settings?") != MTI_DLG_OK) return;
   FileListIO.FactoryDefaults();
   HardParametersToGui(FileListIO);
}
//---------------------------------------------------------------------------

void __fastcall TExportTCDialog::FormShow(TObject *Sender)
{
   if (FileListIO.getFileName() == "")
     if (!mruFileName.empty()) FileListIO.setFileName(mruFileName[0].c_str());

   ParametersToGui(FileListIO);
}
//---------------------------------------------------------------------------

