/*
	File:	FileListIO.h
	By:	Kevin Manbeck
	Date:	June 10, 2002

	The FileListIO class is used to read and write EDLs and QC reports.
	
*/

#ifndef FILELISTIO_H
#define FILELISTIO_H

#include "MTIio.h"                    // Unix <-> Win wrappers
#include "timecode.h"
#include <string>
#include <iostream>

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations
class CIniFile;

/////////////////////////////////////////////////////////////////////////

class CEDLEntry
{
public:
  CEDLEntry ();
  ~CEDLEntry ();

  string
    strTimeCodeIn,	// extracted from the source file
    strTimeCodeOut,	// extracted from the source file
    strSrcScheme,
    strDstScheme,
    strDiskScheme;

  void clear ();

  CTimecode

    tIn,		// a time code used for the destination
    tOut,       // a time code used for the destination
    tStart;   // a time code used for the start frame



  CEDLEntry operator=(const CEDLEntry &edl);
  
};

class CFileListIO
{
public:
  CFileListIO(const string &strBinPathArg);
  CFileListIO(void);
  ~CFileListIO();

  string getOverlapTimeCode ();	
  string getAutoDuration ();

  bool getSortByTimeCode ();
  bool getCleaningPage ();
  bool getConvertPage ();

  string getFileName ();
  string getSrcScheme ();	// returns name of scheme
  string getDstScheme ();	// returns name of scheme
  string getDiskScheme();       // returns name of scheme

  string getSrcRefFrameI ();	// source ref frame for part I
  string getDstRefFrameI ();	// destination ref frame for part I
  string getSrcRefFrameII ();	// source ref frame for part II
  string getDstRefFrameII ();	// destination ref frame for part II
  string getSrcRefFrameIII ();	// source ref frame for part III
  string getDstRefFrameIII ();	// destination ref frame for part III

  string getBinPath ();		// current destination bin

  bool getWriteInPoint ();
  bool getWriteOutPoint ();
  bool getWriteFormat ();
  bool getWriteDate ();
  bool getWriteDescription ();
  bool getWriteClipName ();
  bool getMergeClips();

  void setOverlapTimeCode (const string &strNew);
  void setAutoDuration (const string &strNew);

  void setSortByTimeCode (bool bNew);
  void setCleaningPage (bool bNew);
  void setConvertPage (bool bNew);

  void setFileName (const string &strNew);
  void setSrcScheme (const string &strNew);
  void setDstScheme (const string &strNew);
  void setDiskScheme (const string &strNew);

  void setSrcRefFrameI (const string &strNew);
  void setDstRefFrameI (const string &strNew);
  void setSrcRefFrameII (const string &strNew);
  void setDstRefFrameII (const string &strNew);
  void setSrcRefFrameIII (const string &strNew);
  void setDstRefFrameIII (const string &strNew);

  void setWriteInPoint (bool bNew);
  void setWriteOutPoint (bool bNew);
  void setWriteFormat (bool bNew);
  void setWriteDate (bool bNew);
  void setWriteDescription (bool bNew);
  void setWriteClipName (bool bNew);
  void setBinPath (const string &str);		// current destination bin
  void setMergeClips(bool bNew);

  int GenerateClipSchemes ();
  void FactoryDefaults ();

  int OpenClipInfoFile (bool bAppend);
  int WriteClipInfo (string const &strClipName);
  void CloseClipInfoFile ();

private:
  bool
    bSortByTimeCode,
    bCleaningPage,
    bMergeClips,
    bConvertPage;

  bool
    bWriteInPoint,
    bWriteOutPoint,
    bWriteFormat,
    bWriteDate,
    bWriteDescription,
    bWriteClipName;

  string
    strFileName,
    strSrcScheme,
    strDstScheme,
    strDiskScheme,
    strOverlapTimeCode,
    strAutoDuration,
    strSrcRefFrameI,
    strDstRefFrameI,
    strSrcRefFrameII,
    strDstRefFrameII,
    strSrcRefFrameIII,
    strDstRefFrameIII,
    strBinPath;

  vector<CEDLEntry> edlList;

  CIniFile *ini;

  int ReadEntries ();
  void SortEntries ();
  void MergeEntries ();
  int CreateMarkedClips ();
  bool LookForTimeCode (CEDLEntry &edl, char *cpLine);
  int IsTimeCode (char *cp, char *cpResult);
  bool AssignTimeCode (CEDLEntry &edl);

  FILE
    *fpClipInfoFile;

  int WriteEntries (string const &strClipName);
};


// allowed range for error values:  -4500 -- -4749

#define UTIL_ERROR_FILE_NAME			 -4500
#define UTIL_ERROR_FILE_OPEN			 -4501
#define UTIL_ERROR_FILE_READ			 -4502
#define UTIL_ERROR_NO_TIMECODE			 -4503
#define UTIL_ERROR_MISSING_AUTOPAD		 -4504
#define UTIL_ERROR_MISSING_SCHEME_SRC		 -4505
#define UTIL_ERROR_MISSING_SCHEME_DST		 -4506
#define UTIL_ERROR_NO_BIN			 -4507
#define UTIL_ERROR_FILE_WRITE			 -4508
#define UTIL_ERROR_NULL_FILE_POINTER		 -4509

/*
  Alpha order

UTIL_ERROR_FILE_NAME
  No file name specified for list

UTIL_ERROR_FILE_OPEN
  Unable to open the file

UTIL_ERROR_FILE_READ
  Unable to read from file

UTIL_ERROR_FILE_WRITE
  Unable to write to file

UTIL_ERROR_MISSING_AUTOPAD
  No AutoPad value supplied

UTIL_ERROR_MISSING_SCHEME_DST
  No scheme supplied for destination

UTIL_ERROR_MISSING_SCHEME_SRC
  No scheme supplied for source

UTIL_ERROR_NO_BIN
  Supplied bin name does not exist

UTIL_ERROR_NO_TIMECODE
  Could not find any time codes in the file
  
*/

#endif


