//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FixCountingUnit.h"
#include "BinManager.h"
#include "bthread.h"
#include "SaveRestore.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCountFixesUnit *CountFixesUnit;
//---------------------------------------------------------------------------
__fastcall TCountFixesUnit::TCountFixesUnit(TComponent* Owner)
   : TForm(Owner), currentSequenceNumber(0)
{
}
//---------------------------------------------------------------------------

void TCountFixesUnit::SetBinPath(const string &newBinPath)
{
   binPath = newBinPath;
}
//---------------------------------------------------------------------------

void TCountFixesUnit::SetClipNameList(StringList newClipNames)
{
   clipNames = newClipNames;
}
//---------------------------------------------------------------------------

void __fastcall TCountFixesUnit::FormShow(TObject *Sender)
{
   currentClipName = "";
   clipsScanned = 0;
   currentClipPercent = 0;
   totalNumberOfFixes = 0;
   currentClipNumberOfFixes = 0;

   NumberOfFixesValueLabel->Caption = AnsiString("0");
   if (clipNames.empty())
   {
      ScanningStatusLabel->Caption = AnsiString("No clips selected!!");
      ScanningProgressBar->Position = 0;
      ScanningProgressBar->Max = 100;
      ScanningProgressBar->Visible = false;
   }
   else
   {
      ScanningStatusLabel->Caption = AnsiString("Preparing to scan...");
      ScanningProgressBar->Position = 0;
      ScanningProgressBar->Max = clipNames.size() * 100;
      ScanningProgressBar->Visible = false;
      showProgressDelay = 500; // half second
      hideProgressDelay = 500; // half second

      ++currentSequenceNumber;
      BThreadSpawn(CountingThreadTrampoline, this);
      ProgressTimer->Enabled = true;
   }
}
//---------------------------------------------------------------------------

void __fastcall TCountFixesUnit::FormHide(TObject *Sender)
{
   // This will effectively stop the current scan, if any.
   ++currentSequenceNumber;
   ProgressTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TCountFixesUnit::ProgressTimerTimer(TObject *Sender)
{
   ScanningProgressBar->Position = (clipsScanned * 100) + currentClipPercent;
   if (showProgressDelay > 0)
   {
      showProgressDelay -= ProgressTimer->Interval;
      if (showProgressDelay <= 0)
      {
         // Taking a while, so show the progress bar!
         ScanningProgressBar->Visible = true;
      }
   }

   NumberOfFixesValueLabel->Caption = AnsiString(totalNumberOfFixes + currentClipNumberOfFixes);

   if (clipNames.size() == 1)
   {
      ScanningStatusLabel->Caption = AnsiString(
            (clipNames[0][0] == '(')? "Version " : "Clip ")
            + clipNames[0].c_str();
   }
   else if ((clipsScanned > 0) && currentClipName.empty())
   {
      ScanningStatusLabel->Caption = AnsiString("Scanning complete!");
   }
   else if (!currentClipName.empty())
   {
      ScanningStatusLabel->Caption = AnsiString("Scanning clip ") +  currentClipName.c_str();
   }

   if (clipsScanned == clipNames.size())
   {
      showProgressDelay = 0;
      hideProgressDelay -= ProgressTimer->Interval;
      if (hideProgressDelay <= 0)
      {
         ScanningProgressBar->Visible = false;
         ProgressTimer->Enabled = false;
      }
   }
}
//---------------------------------------------------------------------------

/* static */
void TCountFixesUnit::CountingThreadTrampoline(void* parameter, void* reserved)
{
   TCountFixesUnit *_this = (TCountFixesUnit *) parameter;
   int mySequenceNumber = _this->currentSequenceNumber;
   BThreadBegin(reserved);

   _this->CountingThread(mySequenceNumber);
}
//---------------------------------------------------------------------------

void TCountFixesUnit::CountingThread(int mySequenceNumber)
{
   CBinManager binManager;
   CSaveRestore saveRestoreEngine;

   for (; clipsScanned < clipNames.size() && mySequenceNumber == currentSequenceNumber; ++clipsScanned)
   {
      currentClipName = clipNames[clipsScanned];

      int status;
      auto clip = binManager.openClip(binPath, currentClipName, &status);
      if (clip == nullptr)
      {
         continue;
      }

      int newFixes = saveRestoreEngine.getNumberOfFixes(
                             clip,
                             &currentClipPercent,
                             &currentClipNumberOfFixes,
                             &mySequenceNumber,
                             &currentSequenceNumber);

      binManager.closeClip(clip);

      if (mySequenceNumber != currentSequenceNumber)
      {
         break;
      }

      totalNumberOfFixes += newFixes;
      currentClipNumberOfFixes = 0;
      currentClipPercent = 0;
   }

   currentClipName = "";
}

//---------------------------------------------------------------------------


