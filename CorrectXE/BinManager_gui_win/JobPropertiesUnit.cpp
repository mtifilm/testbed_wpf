//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "JobPropertiesUnit.h"

#include "DllSupport.h"
#include "MTIio.h"
#include "MTIstringstream.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TJobPropertiesForm *JobPropertiesForm;
//---------------------------------------------------------------------------

__fastcall TJobPropertiesForm::TJobPropertiesForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------

namespace
{
   int EditTextToNumber(String text)
   {
      if (text.IsEmpty())
      {
         return 0;
      }

      int retVal;
      try
      {
         retVal = text.ToInt();
      }
      catch (...)
      {
         retVal = 0;
      }

      return retVal < 0 ? 0 : retVal;
   }
}
//---------------------------------------------------------------------------

void TJobPropertiesForm::SetJobInfo(const JobInfo &jobInfo)
{
   JobNameEdit->Text                = jobInfo.Name.c_str();
   NicknameEdit->Text               = jobInfo.Nickname.c_str();
   JobNumberEdit->Text              = (jobInfo.Number > 0) ? String(jobInfo.Number) : "";
   ReelCountEdit->Text              = (jobInfo.ReelCount > 0) ? String(jobInfo.ReelCount) : "";
   ASlashBCheckBox->Checked         = jobInfo.IsASlashB;
   CombinedCheckBox->Checked        = jobInfo.IsCombined;
   LastReelAOnlyCheckBox->Checked   = jobInfo.IsLastReelAOnly;
   DewarpCheckBox->Checked          = jobInfo.HasDewarpFolder;
   MasksCheckBox->Checked           = jobInfo.HasMasksFolder;
   PdlsCheckBox->Checked            = jobInfo.HasPdlsFolder;
   ReportsCheckBox->Checked         = jobInfo.HasReportsFolder;
   CutListsCheckBox->Checked        = jobInfo.HasCutListsFolder;
   StabilizeCheckBox->Checked       = jobInfo.HasStabilizeFolder;

   MediaLocationEdit->Text          = jobInfo.MediaLocation.c_str();
   DrsMediaFolderCheckBox->Checked  = jobInfo.HasDrsMediaFolder;
   UserMediaFolderCheckBox->Checked = jobInfo.HasUserMediaFolder;
   UserMediaFolderNameEdit->Text    = jobInfo.UserMediaFolderName.c_str();
   UserMediaFolderNameEdit->Enabled = UserMediaFolderCheckBox->Checked;

   UserMediaFolderCheckBoxClick(NULL);
   ASlashBCheckBoxClick(NULL);
   ReelCountEditChange(NULL);
   MediaLocationEditChange(NULL);
   JobNameEditChange(NULL);
}
//---------------------------------------------------------------------------

JobInfo TJobPropertiesForm::GetJobInfo()
{
   JobInfo jobInfo;

   jobInfo.Name                = StringToStdString(JobNameEdit->Text);
   jobInfo.Nickname            = StringToStdString(NicknameEdit->Text);
   jobInfo.Number              = EditTextToNumber(JobNumberEdit->Text);
   jobInfo.ReelCount           = EditTextToNumber(ReelCountEdit->Text);
   jobInfo.IsASlashB           = ASlashBCheckBox->Checked;
   jobInfo.IsCombined          = CombinedCheckBox->Checked;
   jobInfo.IsLastReelAOnly     = LastReelAOnlyCheckBox->Checked;
   jobInfo.HasDewarpFolder     = DewarpCheckBox->Checked;
   jobInfo.HasMasksFolder      = MasksCheckBox->Checked;
   jobInfo.HasPdlsFolder       = PdlsCheckBox->Checked;
   jobInfo.HasReportsFolder    = ReportsCheckBox->Checked;
   jobInfo.HasCutListsFolder   = CutListsCheckBox->Checked;
   jobInfo.HasStabilizeFolder  = StabilizeCheckBox->Checked;
   jobInfo.MediaLocation       = StringToStdString(MediaLocationEdit->Text);
   jobInfo.HasDrsMediaFolder   = DrsMediaFolderCheckBox->Checked;
   jobInfo.HasUserMediaFolder  = UserMediaFolderCheckBox->Checked;
   jobInfo.UserMediaFolderName = StringToStdString(UserMediaFolderNameEdit->Text);

   return jobInfo;
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::UserMediaFolderCheckBoxClick(TObject *Sender)
{
   UserMediaFolderNameEdit->Enabled = UserMediaFolderCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::BrowseForMediaLocationButtonClick(TObject *Sender)
{
   string startFolder = StringToStdString(MediaLocationEdit->Text);
   Application->NormalizeTopMosts();
   string newFolder = GetUserFolder(Handle, startFolder.c_str(), "Media Location");
   Application->RestoreTopMosts();
   if (newFolder.empty() || newFolder == startFolder)
   {
      return;
   }

   MediaLocationEdit->Text = newFolder.c_str();
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::ASlashBCheckBoxClick(TObject *Sender)
{
   if (ASlashBCheckBox->Checked)
   {
      CombinedCheckBox->Enabled = true;
      LastReelAOnlyCheckBox->Enabled = true;
   }
   else
   {
      CombinedCheckBox->Enabled = false;
      CombinedCheckBox->Checked = false;
      LastReelAOnlyCheckBox->Enabled = false;
      LastReelAOnlyCheckBox->Checked = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::ReelCountEditChange(TObject *Sender)
{
   int numberOfReels = EditTextToNumber(ReelCountEdit->Text);

   if (!ASlashBCheckBox->Enabled && numberOfReels > 0)
   {
      ASlashBCheckBox->Enabled = true;
      CombinedCheckBox->Enabled = true;
      LastReelAOnlyCheckBox->Enabled = true;
   }
   else if (ASlashBCheckBox->Enabled && numberOfReels == 0)
   {
      ASlashBCheckBox->Enabled = false;
      ASlashBCheckBox->Checked = false;
      CombinedCheckBox->Enabled = false;
      CombinedCheckBox->Checked = false;
      LastReelAOnlyCheckBox->Enabled = false;
      LastReelAOnlyCheckBox->Checked = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::JobNameEditChange(TObject *Sender)
{
   OkButton->Enabled = !JobNameEdit->Text.IsEmpty();
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::MediaLocationEditChange(TObject *Sender)
{
   if (!DrsMediaFolderCheckBox->Enabled && !MediaLocationEdit->Text.IsEmpty())
   {
      DrsMediaFolderCheckBox->Enabled = true;
      UserMediaFolderCheckBox->Enabled = true;
   }
   else if (DrsMediaFolderCheckBox->Enabled && MediaLocationEdit->Text.IsEmpty())
   {
      DrsMediaFolderCheckBox->Enabled = false;
      DrsMediaFolderCheckBox->Checked = false;
      UserMediaFolderCheckBox->Enabled = false;
      UserMediaFolderCheckBox->Checked = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TJobPropertiesForm::JobFoldersToggleButtonClick(TObject *Sender)
{
   bool toggleOn = !(DewarpCheckBox->Checked || MasksCheckBox->Checked
					   || PdlsCheckBox->Checked || ReportsCheckBox->Checked
					   || CutListsCheckBox->Checked || StabilizeCheckBox->Checked);
   DewarpCheckBox->Checked    = toggleOn;
   MasksCheckBox->Checked     = toggleOn;
   PdlsCheckBox->Checked      = toggleOn;
   ReportsCheckBox->Checked   = toggleOn;
   CutListsCheckBox->Checked  = toggleOn;
   StabilizeCheckBox->Checked = toggleOn;
}
//---------------------------------------------------------------------------

