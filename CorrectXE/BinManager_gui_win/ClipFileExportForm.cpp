// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "ClipFileExportForm.h"
#include "ImageFileMediaAccess.h"
#include "bthread.h"

#define USE_MODAL_PROGRESS_DIALOG 1

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIBusy"
#pragma resource "*.dfm"

TClipFileExportForm *ClipFileExportForm;
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

__fastcall TClipFileExportForm::TClipFileExportForm(TComponent* Owner)
      : TForm(Owner),
        _theFileExporter(0),
        CriticalSection(CriticalSectionAlloc())
{
   Reset();
}
// ---------------------------------------------------------------------------

__fastcall TClipFileExportForm::~TClipFileExportForm()
{
   if (CriticalSection != 0)
   {
      CriticalSectionFree(CriticalSection);
   }
}
// ---------------------------------------------------------------------------

void TClipFileExportForm::Reset()
{
   FullSourceName = "";
   SourceName = "";
   SourceFrame = 0;
   FullDestName = "";
   DestName = "";
   DestFrame = 0;
   SourceNameLabel->Caption = "";
   DestinationNameLabel->Caption = "";

   NukeMe = false;
   AllDone = false;
   Paused = false;
   _exporting = false;
   Initializing = true;

   OldCompletedFrames = 0;
   OldTotalElapsedTime = 0.0;
   ElapsedTimeZero = 0.0;
   FinishedInTime = 0.0;
   FinishedInUpdateTime = 0.0;
   ProcessingTimePerFrame = 0.0;
   for (int i = 0; i < NUM_HISTORY_ITEMS; ++i)
   {
      ElapsedTimeHistory[i] = 0.0;
   }

   PauseButton->Enabled = false;
   PauseButton->Caption = "Pause";
   CancelButton->Enabled = true;
   CancelButton->Caption = "Cancel";
   Caption = "Starting Export...";
   StatusLabel->Caption = "";
   ConvertedLabel->Caption = "";
   FinishedInLabel->Caption = "";
   RenderProgressBar->Min = 0;
   RenderProgressBar->Max = 100;
   RenderProgressBar->Position = 0;

   // MTIBusy1->ArrowType = batLarge;
   // MTIBusy1->Busy = false;
   // MTIBusy1->Enabled = true;
   // MTIBusy1->Visible = true;
}
// ---------------------------------------------------------------------------

void TClipFileExportForm::setExporter(ClipFileExporter *exporter)
{
   _theFileExporter = exporter;
   int lastSep = 0;

   Reset();

   ClipIdentifier clipId = _theFileExporter->GetSourceClipId();
   string clipName = clipId.IsVersionClip()
                      ? clipId.GetMasterClipName() + " " + clipId.GetClipName()
                      : clipId.GetClipName();

   SourceNameLabel->Caption = clipName.c_str();
   DestinationNameLabel->Caption = _theFileExporter->GetTargetDirectory().c_str();
   Caption = "Starting Export...";
}
// ---------------------------------------------------------------------------

void TClipFileExportForm::Go()
{
   TheRunThread = BThreadSpawn(&WorkerThread, this);
   if (TheRunThread == NULL)
   {
      TRACE_0(errout << "Failed to spawn run thread.");
      PauseButton->Enabled = false;
      PauseButton->Caption = "Pause";
      CancelButton->Enabled = true;
      CancelButton->Caption = "Close";
      StatusLabel->Caption = "ERROR - thread creation failed";
      Caption = "Export failed";
      return;
   }

   PauseButton->Enabled = true;
   PauseButton->Caption = "Pause";
   StatusLabel->Caption = "";
   Caption = "Exporting Files...";
   ProgressTimer->Enabled = true;
   // MTIBusy1->Busy = true;
}
// ---------------------------------------------------------------------------

bool TClipFileExportForm::OkToNuke()
{
   return NukeMe;
}
// ---------------------------------------------------------------------------

/* static */
void TClipFileExportForm::WorkerThread(void *vpAppData, void *vpReserved)
{
   if (BThreadBegin(vpReserved))
   {
      exit(1); // say what??!?
   }

   ((TClipFileExportForm *)vpAppData)->RunInThread();
}

// ---------------------------------------------------------------------------
void TClipFileExportForm::RunInThread()
{
   int retVal;

   HRTimer.Start();
   _exporting = true;

   _theFileExporter->ExportFiles();

   if (CriticalSection != NULL)
   {
      CriticalSectionEnter(CriticalSection);
   }

   _exporting = false;
   TheRunThread = NULL;

   ProgressTimer->Enabled = false;

   retVal = _theFileExporter->GetCompletionCode();
   UpdateProgress(retVal == 0); // Finished if no error

   if (retVal)
   {
      StatusLabel->Caption = "ERROR";
      Caption = "Export failed";
      FinishedInLabel->Caption = _theFileExporter->GetErrorMessage().c_str();
   }
   else
   {
      StatusLabel->Caption = "Done";
      Caption = "Export complete";
   }

   Paused = false;
   PauseButton->Enabled = false;
   PauseButton->Caption = "Pause";
   CancelButton->Caption = "Close";
   // MTIBusy1->Busy = false;

   AllDone = true;
   Cursor = crDefault; // turn off the hourglass

   if (!CancelButton->Enabled)
   {
#if USE_MODAL_PROGRESS_DIALOG
      ModalResult = mrOk;
#else
      Hide();
#endif
      NukeMe = true;
   }

   if (CriticalSection != NULL)
   {
      CriticalSectionExit(CriticalSection);
   }
}

// ---------------------------------------------------------------------------
void __fastcall TClipFileExportForm::ProgressTimerTimer(TObject *Sender)
{
   if (_exporting)
   {
      UpdateProgress(false); // false = not finished
   }
}
// ---------------------------------------------------------------------------

void __fastcall TClipFileExportForm::CancelButtonClick(TObject *Sender)
{
   Cancel();
}

// ---------------------------------------------------------------------------

void TClipFileExportForm::Cancel()
{
   PauseButton->Enabled = false;
   PauseButton->Caption = "Pause";
   StatusLabel->Caption = "Stopping...";
   Caption = "Export canceled";

   if (CriticalSection != NULL)
   {
      CriticalSectionEnter(CriticalSection);
   }

   CancelButton->Enabled = false;
   if (_exporting)
   {
      if (Paused)
      {
         _theFileExporter->Resume();
      }

      _theFileExporter->Cancel();
   }
   else
   {
#if USE_MODAL_PROGRESS_DIALOG
      ModalResult = mrOk;
#else
      Hide();
#endif
      AllDone = true; // just in case!
      NukeMe = true;
      Cursor = crDefault; // turn off the hourglass
   }

   if (CriticalSection != NULL)
   {
      CriticalSectionExit(CriticalSection);
   }
}
// ----------------------------------------------------------------------

void __fastcall TClipFileExportForm::PauseButtonClick(TObject *Sender)
{
   if (Paused)
   {
      Paused = false;
      _theFileExporter->Resume();
      PauseButton->Caption = "Pause";
      StatusLabel->Caption = "";
      Caption = "Exporting...";
      // MTIBusy1->Busy = false;
   }
   else
   {
      Paused = true;
      _theFileExporter->Pause();
      PauseButton->Caption = "Resume";
      StatusLabel->Caption = "Pausing...";
      Caption = "Export paused";
      // MTIBusy1->Busy = true;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TClipFileExportForm::FormShow(TObject *Sender)
{
   if (_theFileExporter == NULL)
   {
      TRACE_0(errout << "UNAUTHORIZED FORMSHOW for TClipFileExportForm");
      return;
   }

   PauseButton->Enabled = false;
   PauseButton->Caption = "Pause";
   StatusLabel->Caption = "";
   Caption = "Exporting Files";

   /*
    * Unparent ourself to get an icon on the task bar
    */
   Parent = NULL;

   Go();
}
// ---------------------------------------------------------------------------

void TClipFileExportForm::UpdateProgress(bool finished)
{
   int completedFrames = 0;
   int totalFrames = 0;
   ClipFileExporter::ExportPhase exportPhase = _theFileExporter->GetProgress(completedFrames, totalFrames);
   double totalElapsedTime = HRTimer.Read();
   int centiseconds = 0;
   int seconds = 0;
   int minutes = 0;
   int hours = 0;
   AnsiString message;

   if (Paused && exportPhase == 3)
   {
      StatusLabel->Caption = "Paused";
   }

   if (exportPhase == ClipFileExporter::IDLE_PHASE)
   {
      ConvertedLabel->Caption = "";
      FinishedInLabel->Caption = "";
   }
   else
   {
      if (completedFrames != OldCompletedFrames)
      {
         double elapsedTime = totalElapsedTime - OldTotalElapsedTime;
         double historyTimeSum = elapsedTime;
         int itemCount = 1;

         for (int i = 0; i < NUM_HISTORY_ITEMS - 1; ++i)
         {
            ElapsedTimeHistory[i] = ElapsedTimeHistory[i + 1];
            historyTimeSum += ElapsedTimeHistory[i];
            if (ElapsedTimeHistory[i] > 0.0)
            {
               itemCount += 1;
            }
         }

         ElapsedTimeHistory[NUM_HISTORY_ITEMS - 1] = elapsedTime;

         ProcessingTimePerFrame = historyTimeSum / itemCount;
         OldTotalElapsedTime = totalElapsedTime;

         OldCompletedFrames = completedFrames;

         if (CImageFileMediaAccess::IsTemplate(SourceName))
         {
            string fileName = CImageFileMediaAccess::GenerateFileName2(SourceName, SourceFrame + completedFrames - 1);

            SourceNameLabel->Caption = fileName.c_str();
         }

         if (CImageFileMediaAccess::IsTemplate(DestName))
         {
            string fileName = CImageFileMediaAccess::GenerateFileName2(DestName, DestFrame + completedFrames - 1);

            DestinationNameLabel->Caption = fileName.c_str();
         }
      }
      else if (exportPhase == ClipFileExporter::PAUSED_PHASE)
      {
         /*
          * Don't let time advance.
          */
         ElapsedTimeZero += totalElapsedTime - OldTotalElapsedTime;
         OldTotalElapsedTime = totalElapsedTime;
      }

      totalElapsedTime -= ElapsedTimeZero;

      if (totalFrames > 0)
      {
         RenderProgressBar->Max = totalFrames;
      }
      else
      {
         RenderProgressBar->Max = 100;
      }

      if (completedFrames < 0)
      {
         completedFrames = 0;
      }

      if (completedFrames > RenderProgressBar->Max)
      {
         completedFrames = RenderProgressBar->Max;
      }

      RenderProgressBar->Position = completedFrames;

      centiseconds = ((int) floor(totalElapsedTime / 10.)) % 100;
      seconds = ((int) floor(totalElapsedTime / 1000.)) % 60;
      minutes = ((int) floor(totalElapsedTime / 60000.)) % 60;
      hours = (int) floor(totalElapsedTime / 3600000.);

      message = AnsiString("Exported ") + AnsiString(completedFrames);
      if (!finished)
      {
         message += AnsiString(" of ") + AnsiString(totalFrames);
      }

      message += AnsiString(" frames in ") + AnsiString((hours < 10) ? "0" : "") + AnsiString(hours) + AnsiString(":") +
            AnsiString((minutes < 10) ? "0" : "") + AnsiString(minutes) + AnsiString(":") + AnsiString((seconds < 10) ? "0" : "") +
            AnsiString(seconds);
      ConvertedLabel->Caption = message;

      message = "";
      if (totalFrames > 0)
      {
         if ((ProcessingTimePerFrame > 0.0) && !finished)
         {
            if (totalElapsedTime >= FinishedInUpdateTime)
            {
               FinishedInTime = (ProcessingTimePerFrame * (totalFrames - completedFrames)) - (totalElapsedTime - OldTotalElapsedTime);
               FinishedInUpdateTime = totalElapsedTime + FINISHED_IN_UPDATE_INTERVAL;
            }

            if (FinishedInTime < 0.0)
            {
               FinishedInTime = 0.0;
            }

            seconds = ((int) ceil(FinishedInTime / 1000.)) % 60;
            minutes = ((int) floor(FinishedInTime / 60000.)) % 60;
            hours = (int) floor(FinishedInTime / 3600000.);
            message = AnsiString("Finished in ") + AnsiString((hours < 10) ? "0" : "") + AnsiString(hours) + AnsiString(":") +
                  AnsiString((minutes < 10) ? "0" : "") + AnsiString(minutes) + AnsiString(":") + AnsiString((seconds < 10) ? "0" : "") +
                  AnsiString(seconds);
         }
         else if ((completedFrames > 0) && finished)
         {
            ProcessingTimePerFrame = totalElapsedTime / completedFrames;
            centiseconds = ((int) floor(ProcessingTimePerFrame / 10.)) % 100;
            seconds = ((int) floor(ProcessingTimePerFrame / 1000.)) % 60;
            minutes = ((int) floor(ProcessingTimePerFrame / 60000.)) % 60;
            hours = (int) floor(ProcessingTimePerFrame / 3600000.);
            message = AnsiString(" (") + AnsiString((hours < 10) ? "0" : "") + AnsiString(hours) + AnsiString(":") +
                  AnsiString((minutes < 10) ? "0" : "") + AnsiString(minutes) + AnsiString(":") + AnsiString((seconds < 10) ? "0" : "") +
                  AnsiString(seconds) + AnsiString(".") + AnsiString((centiseconds < 10) ? "0" : "") + AnsiString(centiseconds) +
                  AnsiString(" per frame)");
         }
      }
      FinishedInLabel->Caption = message;
   }
}
// ---------------------------------------------------------------------------
