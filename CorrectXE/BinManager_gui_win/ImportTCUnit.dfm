object ImportTCDialog: TImportTCDialog
  Left = 454
  Top = 140
  Anchors = [akLeft, akBottom]
  AutoSize = True
  BorderStyle = bsDialog
  Caption = 'Create Multiple Clips (Import Timecodes)'
  ClientHeight = 499
  ClientWidth = 369
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poOwnerFormCenter
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelMiddle: TPanel
    Left = 0
    Top = 261
    Width = 369
    Height = 186
    BevelOuter = bvNone
    TabOrder = 0
    object Label4: TLabel
      Left = 26
      Top = 20
      Width = 119
      Height = 13
      Caption = 'Format of File Timecodes'
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 48
      Width = 341
      Height = 129
      Hint = 'List timecodes on the list are translated into clip timecodes'
      Caption = ' Reference Frame '
      TabOrder = 0
      object Label5: TLabel
        Left = 108
        Top = 24
        Width = 33
        Height = 13
        Caption = 'Source'
      end
      object Label6: TLabel
        Left = 212
        Top = 24
        Width = 54
        Height = 13
        Caption = 'Destination'
      end
      object Label7: TLabel
        Left = 40
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Part I'
      end
      object Label8: TLabel
        Left = 40
        Top = 68
        Width = 31
        Height = 13
        Caption = 'Part II'
      end
      object Label9: TLabel
        Left = 40
        Top = 92
        Width = 35
        Height = 13
        Caption = 'Part III'
      end
      object Part1TCSource: VTimeCodeEdit
        Left = 92
        Top = 40
        Width = 84
        Height = 19
        Hint = 'Input timecode will translate to the left timecode'
        TabOrder = 0
        AllowVTRCopy = False
      end
      object Part1TCDest: VTimeCodeEdit
        Left = 204
        Top = 40
        Width = 84
        Height = 19
        Hint = 'Souce timecode on right is mapped to this clip timecode'
        TabOrder = 1
        AllowVTRCopy = False
      end
      object Part2TCSource: VTimeCodeEdit
        Left = 92
        Top = 64
        Width = 84
        Height = 19
        Hint = 'Input timecode will translate to the left timecode'
        TabOrder = 2
        AllowVTRCopy = False
      end
      object Part2TCDest: VTimeCodeEdit
        Left = 204
        Top = 64
        Width = 84
        Height = 19
        Hint = 'Souce timecode on right is mapped to this clip timecode'
        TabOrder = 3
        AllowVTRCopy = False
      end
      object Part3TCSource: VTimeCodeEdit
        Left = 92
        Top = 88
        Width = 84
        Height = 19
        Hint = 'Input timecode will translate to the left timecode'
        TabOrder = 4
        AllowVTRCopy = False
      end
      object Part3TCDest: VTimeCodeEdit
        Left = 204
        Top = 88
        Width = 84
        Height = 19
        Hint = 'Souce timecode on right is mapped to this clip timecode'
        TabOrder = 5
        AllowVTRCopy = False
      end
    end
    object ListFormatPopupBox: TPopupComboBox
      Left = 168
      Top = 16
      Width = 152
      Height = 21
      Hint = 'Format of the timecodes in the list'
      ReadOnly = True
      TabOrder = 1
      OnChange = ListFormatPopupBoxChange
    end
    object FileTCFormatComboBox: TComboBox
      Left = 160
      Top = 16
      Width = 185
      Height = 21
      Style = csDropDownList
      TabOrder = 2
      OnChange = FileTCFormatComboBoxChange
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 447
    Width = 369
    Height = 52
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      369
      52)
    object CancelButton: TBitBtn
      Left = 260
      Top = 14
      Width = 96
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000CE0E0000D80E00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777777777777777777777779F77779F7777777777777999F777777
        9F7777199F777771F777777999F77719F7777777199F719F77777777719999F7
        7777777777999F7777777777719999F777777777199F71F77777771999F77799
        F77771999F7777799F77799F7777777799F77777777777777777}
      ModalResult = 2
      TabOrder = 2
    end
    object OKButton: TBitBtn
      Left = 158
      Top = 14
      Width = 96
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Create Clips'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080808080808
        0807F7F7F707080808080808080808A4591818185152F7070808080808081818
        292929292118509B070808080818212929323229292902189B0708081929292A
        323232323229290251F70808186A292929292A2A32322929185A086A722A2929
        FFFF29323232322902510818732929FFFFFFFF292A322A29215108187329FFFF
        2929FFFF292A29292951081874292929292929FFFF2929290251086A73732929
        29292929FFFF2929189B0808187C2929292929292929292918F70808AB6A7C29
        2929292929292918A408080808612A7C74736A72726A1863080808080808AC18
        6A73737221186A080808080808080808A56AA56AA50808080808}
      ModalResult = 1
      TabOrder = 1
      OnClick = OKButtonClick
    end
    object RestoreButton: TBitBtn
      Left = 30
      Top = 14
      Width = 75
      Height = 25
      Hint = 'Restore the design parameters'
      Anchors = [akRight, akBottom]
      Caption = 'Restore'
      Glyph.Data = {
        9E050000424D9E05000000000000360400002800000012000000120000000100
        0800000000006801000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FCFCFCFCFCFC
        FCFCFCFCFCFCFCFCFCFCFCFC0000FCFCFCFCFCFCFC0011101010FCFCFCFCFCFC
        0000FCFCFCFCFCFCFC006B6B73731010FCFCFCFC0000FCFCFCFCFCFCFC10BC7A
        79796A6110FCFCFC0000FCFCFCFCFCFCFC10B372717A616A625AFCFC0000FCFC
        FCFCFCFCFC001010101072611810FCFC0000FCFCFCFCFCFCFCFCFCFCFC5A0000
        5200FCFC0000FCFCFCFCFCFCFCFCFCFCFCFCFC510000FCFC0000FCFCFCFCFC11
        FCFCFCFCFCFCFC520000FCFC0000FCFCFCFC1010FCFCFCFCFC5A00105A00FCFC
        0000FCFCFC10B410101010101010AB626210FCFC0000FCFC106B727B797A7171
        7B72616A1962FCFC0000FC10B373793938FAFA39397AB45910F5FCFC0000FCFC
        18727A717A7A797A317A1010FCFCFCFC0000FCFCFC00AC10100000000000FCFC
        FCFCFCFC0000FCFCFCFC1100FCFCFCFCFCFCFCFCFCFCFCFC0000FCFCFCFCFC00
        FCFCFCFCFCFCFCFCFCFCFCFC0000FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
        0000}
      TabOrder = 0
      Visible = False
      OnClick = RestoreButtonClick
    end
  end
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 369
    Height = 261
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      369
      261)
    object SourceFileLabel: TLabel
      Left = 28
      Top = 112
      Width = 100
      Height = 13
      Caption = 'Timecode Source &File'
      FocusControl = FileNameComboBox
    end
    object SinglePaddingLabel: TLabel
      Left = 28
      Top = 156
      Width = 69
      Height = 13
      Hint = 'Single timecodes will be centetr padded by'
      Caption = 'Single Padding'
    end
    object FileBrowseButton: TSpeedButton
      Left = 331
      Top = 126
      Width = 22
      Height = 22
      Hint = 'Browse for timecode list '
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000CE0E0000C40E00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77777777777777777777000000000007777700333333333077770B0333333333
        07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
        77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
        7007777777770777070777777777700077777777777777777777}
      OnClick = FileBrowseButtonClick
    end
    object Bevel1: TBevel
      Left = 8
      Top = 253
      Width = 353
      Height = 4
      Shape = bsBottomLine
    end
    object RecordFormatPopupBox: TPopupComboBox
      Left = 36
      Top = 72
      Width = 185
      Height = 21
      Hint = 'Format of clip'
      ReadOnly = True
      TabOrder = 1
      Visible = False
      OnChange = RecordFormatPopupBoxChange
    end
    object FileNameComboBox: TComboBox
      Left = 28
      Top = 128
      Width = 297
      Height = 21
      Hint = 'Text files containing  list of timecodes'
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnDropDown = FileNameComboBoxDropDown
    end
    object SortCheckBox: TCheckBox
      Left = 28
      Top = 200
      Width = 121
      Height = 17
      Hint = 'Clips will be created in increasing order'
      Caption = 'Sort by Timecodes'
      TabOrder = 3
    end
    object MergeTC: VTimeCodeEdit
      Left = 220
      Top = 176
      Width = 89
      Height = 19
      Hint = 'Merge clips closer in time than this'
      TabOrder = 2
      AllowVTRCopy = False
    end
    object MergeCheckBox: TCheckBox
      Left = 220
      Top = 160
      Width = 81
      Height = 17
      Hint = 'Enables merging of clips'
      Caption = 'Merge Clips'
      Checked = True
      State = cbChecked
      TabOrder = 4
      OnClick = MergeCheckBoxClick
    end
    object AutoPadTC: VTimeCodeEdit
      Left = 28
      Top = 172
      Width = 89
      Height = 19
      Hint = 'Single timecodes are padded 1/2 length on both sides'
      TabOrder = 5
      AllowVTRCopy = False
    end
    object AlternateCheckBox: TCheckBox
      Left = 28
      Top = 224
      Width = 161
      Height = 17
      Hint = 'Check if timecode list is in a different format than the clips'
      Caption = 'Use Alternative Input Format'
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = AlternateCheckBoxClick
    end
    object NewClipPropertiesGroupBox: TGroupBox
      Left = 16
      Top = 8
      Width = 337
      Height = 97
      Caption = ' New Clip Properties                                    '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      object VideoFormatLabel: TLabel
        Left = 12
        Top = 24
        Width = 65
        Height = 13
        Caption = '&Video Format:'
        FocusControl = RecordFormatPopupBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object AudioFormatLabel: TLabel
        Left = 12
        Top = 40
        Width = 65
        Height = 13
        Caption = 'Audio Format:'
        FocusControl = RecordFormatPopupBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object StorageFormatLabel: TLabel
        Left = 12
        Top = 56
        Width = 75
        Height = 13
        Caption = 'Storage Format:'
        FocusControl = RecordFormatPopupBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object MediaLocationLabel: TLabel
        Left = 12
        Top = 72
        Width = 76
        Height = 13
        Caption = 'Media Location:'
        FocusControl = RecordFormatPopupBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object CreateClipPanelHint: TLabel
        Left = 128
        Top = 0
        Width = 120
        Height = 13
        Caption = ' (set in Create Clip panel) '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object VideoFormatValue: TLabel
        Left = 100
        Top = 24
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object AudioFormatValue: TLabel
        Left = 100
        Top = 40
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object StorageFormatValue: TLabel
        Left = 100
        Top = 56
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object MediaLocationValue: TLabel
        Left = 100
        Top = 72
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 248
    Top = 220
  end
  object PopupMenu2: TPopupMenu
    Left = 208
    Top = 220
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Text Files|*.txt; *.data, *.dat|All Files|*.*'
    Left = 292
    Top = 220
  end
end
