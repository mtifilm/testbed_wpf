// BinManagerGUIUnit.h: Interface for TBinManagerForm class
// Tabing is correct
//
//---------------------------------------------------------------------------

#ifndef BinManagerGUIUnitH
#define BinManagerGUIUnitH

//---------------------------------------------------------------------------
#include "binmgrlibint.h"

#include "BinManager.h"
#include "Clip3.h"
#include "Convert.h"
#include "IniFile.h"
#include "JobInfo.h"
#include "MTIUNIT.h"
#include "NakedProxyUnit.h"
#include "PopupComboBox.h"
#include "TriTimecode.h"
#include "VTimeCodeEdit.h"

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <ImgList.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Grids.hpp>
#include <System.ImageList.hpp>

#include <string>
using std::string;
#include <map>
using std::map;
#include <vector>
using std::vector;

//---------------------------------------------------------------------------

// Clip List Column Indices
#define COL_INDEX_CLIP_NAME        0
#define COL_INDEX_CLIP_STATUS      1
#define COL_INDEX_IMAGE_TYPE       2
#define COL_INDEX_IN_TIMECODE      3
#define COL_INDEX_OUT_TIMECODE     4
#define COL_INDEX_DURATION         5
#define COL_INDEX_CLIP_DESCRIPTION 6
#define COL_INDEX_MEDIA_LOCATION   7
#define COL_INDEX_CREATE_DATE      8
#define CLIP_LIST_COL_COUNT        9    // Number of columns in clip list

// Version List Column Indices
#define COL_INDEX_VERSION_NAME        0
#define COL_INDEX_VERSION_STATUS      1
#define COL_INDEX_VERSION_MODIFIED    2
#define COL_INDEX_VERSION_CREATE_DATE 3
#define COL_INDEX_VERSION_COMMENTS    4
#define VERSION_LIST_COL_COUNT        5  // Number of columns in VERSION list

// Operational Mode
#define BM_MODE_NORMAL         0   // Normal "Bin Manager" mode
#define BM_MODE_SELECT_CLIP    1   // "Select a single clip" dialog mode
#define BM_MODE_SELECT_BIN     2   // "Select a bin" dialog mode

//---------------------------------------------------------------------------
// Forward Declarations

class CClipInitInfo;
class CBinMgrGUIInterface;
class TDailiesClipForm;
struct SClipDataItem;
struct SVersionDataItem;

//---------------------------------------------------------------------------
// Helper class for bin path<-->clip list map
struct BinPathLessThan
{
   bool operator()(const string &left, const string &right) const
   {
      return LessThanIgnoreCase(left, right);
   }
};

//---------------------------------------------------------------------------
 class MTI_BINMGRLIB_API TBinManagerForm;
 class TBinManagerForm : public TMTIForm
{
__published:	// IDE-managed Components
   TPopupMenu *ClipSchemePopupMenu;
   TMenuItem *HDMenuItem;
   TMenuItem *ImageFileMenuItem;
   TMenuItem *SDMenuItem;
   TPopupMenu *BinTreePopupMenu;
    TMenuItem *BinPopupNewBinItem;
    TMenuItem *BinPopupDeleteItem;
   TMenuItem *HSDLMenuItem;
	TPanel *BottomPanel;
	TPanel *CreateClipPanel;
	TPanel *ModalPanel;
	TBitBtn *OKButton;
	TBitBtn *CancelButton;
	TPopupMenu *ClipListPopupMenu;
    TMenuItem *ClipPopupSeparator4;
	TTimer *CheckCopyFromVTRTimer;
    TMenuItem *ClipPopupExportImageFilesItem;
    TMenuItem *ClipPopupImportImageFilesItem;
	TImageList *ButtonImageList;
	TGroupBox *GroupBoxLicense;
	TEdit *Company;
	TEdit *Address;
	TEdit *CityStatePostal;
	TEdit *Country;
	TLabel *VersionLicense;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TGroupBox *GroupBoxProjectScanList;
	TLabel *Label11;
	TEdit *Title;
	TEdit *JobNo;
	TEdit *ProdCo;
	TEdit *Project;
	TLabel *Label12;
	TLabel *Label16;
	TLabel *Label17;
	TBevel *Bevel5;
	TEdit *ProdNo;
	TLabel *Label6;
	TEdit *WorkOrder;
	TLabel *WorkOrderLabel;
	TEdit *ShootDate;
	TEdit *TransferDate;
	TEdit *Colorist;
	TEdit *Assist;
	TLabel *ShootDateLabel;
	TLabel *TransferDateLabel;
	TLabel *ColoristLabel;
	TLabel *AssistLabel;
	TButton *ScanListEditButton;
	TButton *LicenseEditButton;
	TButton *ScanListSaveButton;
	TButton *LicenseSaveButton;
    TMenuItem *ClipPopupUpdateCutsItem;
	TLabel *FieldALabel;
	TLabel *FieldBLabel;
	TLabel *FieldCLabel;
	TEdit *FieldB;
	TEdit *FieldA;
	TEdit *FieldC;
    TMenuItem *BinPopupRenameItem;
    TMenuItem *ClipPopupRenameItem;
    TMenuItem *ClipPopupDeleteItem;
	TPopupMenu *ProxyImagePopupMenu;
	TMenuItem *ShowProxyTimecodeMenuItem;
	TPanel *GroupBoxNewClip;
	TPanel *NewClipExistingMediaPanel;
	TPanel *NewClipMainSelectorPanel;
	TPanel *NewClipNewMediaPanel;
	TBevel *Bevel9;
	TBevel *Bevel12;
	TPanel *SourceDescriptionPanel;
	TPanel *VideoStoreMediaFormatPanel;
	TLabel *AudioFormatLabel;
	TLabel *VideoFormatLabel;
	TPanel *VideoTImecodePanel;
	TPanel *FileMediaLocationPanel;
	TLabel *FileVidClipFileInFrameLabel;
	TLabel *FileVidClipFileNameLabel;
	TLabel *FileVidClipVideoFolderLabel;
	TEdit *FileVidClipFileNameEdit;
	TEdit *FileVidClipInFrameEdit;
	TBitBtn *FileVidClipVideoFolderBrowseButton;
	TComboBox *FileVidClipVideoFolderComboBox;
	TLabel *FileVidClipFileTypeLabel;
	TLabel *FileVidClipAudioFolderLabel;
	TComboBox *FileVidClipAudioFolderComboBox;
	TSpeedButton *FileVidClipAudioFolderBrowseButton;
	TPanel *CreateClipButtonPanel;
	TBevel *Bevel10;
	TCheckBox *FileVidClipTCIndexCheckBox;
	TComboBox *NewMediaVideoFormatComboBox;
	TComboBox *NewMediaAudioFormatComboBox;
	TComboBox *FileVidClipMediaStorageComboBox;
	TLabel *VideoClipNameLabel;
	TPanel *PanelInOut;
	TLabel *TCInLabel;
	TLabel *TCOutLabel;
	VTimeCodeEdit *VTimeCodeEditIn;
	TBitBtn *InCueButton;
	TBitBtn *InCopyButton;
	VTimeCodeEdit *VTimeCodeEditOut;
	TBitBtn *OutCueButton;
	TBitBtn *OutCopyButton;
	TPanel *PanelCentral;
	TLabel *TCCenterLabel;
	VTimeCodeEdit *VTimeCodeEditCen;
	TBitBtn *CenCueButton;
	TBitBtn *CenCopyButton;
	VTimeCodeEdit *VTimeCodeEditInSmall;
	VTimeCodeEdit *VTimeCodeEditOutSmall;
	TCheckBox *CheckBoxCentralize;
	VTimeCodeEdit *VTimeCodeEditDur;
	TLabel *TCDurationLabel;
	TPanel *VideoStorePickerPanel;
	TLabel *LabelLocationVideo;
	TComboBox *MediaLocationComboBox;
	TComboBox *VideoStoreMediaStorageComboBox;
	TPanel *FillModeRadioButtonPanel;
	TLabel *FillModeLabel;
	TRadioButton *FillWithBlanksRadioButton;
	TRadioButton *FillByRecordingRadioButton;
	TRadioButton *FillFromClipRadioButton;
	TLabel *RepositoryStorageFormatLabel;
	TEdit *VideoClipNameEdit;
	TBitBtn *CreateVideoClipButton;
	TOpenDialog *OpenDialog;
	TOpenDialog *OpenDialogXXX;
	TTreeView *BinTree;
	TPanel *LeftPanel;
	TPanel *ProxyDisplayPanel;
	TPanel *PanelProxy;
	TPaintBox *ImageProxy;
	TTrackBar *TrackBarProxy;
	TSplitter *BinTreeBottomSplitter;
	TSplitter *BinTreeRightSideSplitter;
	TPanel *FilePick;
	TLabel *ClipFolderLabel;
	TEdit *FileClipFileNameEdit;
	TEdit *FileClipFolderEdit;
	TPanel *CreateFileClipButtonPanel;
	TLabel *Label7;
	TEdit *FileClipNameEdit;
	TBitBtn *CreateFileClipButton;
	TPanel *FileClipTCPanel;
	TLabel *Label20;
	TCheckBox *FileClipTCCheckBox;
	TLabel *FileClipInTCLabel;
	VTimeCodeEdit *FileClipInTCEdit;
	TLabel *FileClipDFLabel;
	TCheckBox *FileClipDFCheckBox;
	TLabel *FileClipFPSLabel;
	TComboBox *FileClipFPSComboBox;
	TPanel *FileClipFrameIndexPanel;
	TLabel *FilelipFirstLabel;
	TEdit *InFileClipEdit;
	TLabel *FileClipLastLabel;
	TEdit *OutFileClipEdit;
	TLabel *FileCLipDurLabel;
	TEdit *DurFileClipEdit;
	TPanel *ExistingMediaFormatPanel;
	TPanel *Panel5;
	TSpeedButton *MultiBrowseButton;
	TSpeedButton *FileVidClipInfoButton;
	TPanel *VideoStoreUsagePanel;
	TLabel *BottomPanelSpacerLabel;
	TLabel *LabelFormatFile;
	TPopupComboBox *ClipSchemePopupBox;
	TSpeedButton *ClickSchemeInfoButton;
	TLabel *ExistingMediaFormatLabel;
	TSpeedButton *ExistingMediaFormatInfoButton;
	TComboBox *ExistingMediaFormatComboBox;
	TPanel *FileHeaderInfoPanel;
	TLabel *CreateClipLabel;
	TPanel *LocationTypeSelectionPanel;
	TRadioButton *LocationRepositoryRadioButton;
	TRadioButton *OtherLocationRadioButton;
	TSpeedButton *RepositoryFormatInfoButton;
	TPanel *VideoFilePickerPanel;
	TLabel *LabelFolderVideo;
	TLabel *LabelFileNameVideo;
	TSpeedButton *VideoFileBrowseButton;
	TLabel *LabelInFrameVideo;
	TEdit *VideoFileFolderEdit;
	TEdit *VideoFileNameEdit;
	TEdit *VideoInFrameEdit;
	TCheckBox *CheckBox1;
	TPanel *VideoStoreUsagePositioningPanel1;
	TPanel *VideoStoreUsagePositioningPanel2;
	TComboBox *FileRepositoryStorageFormatComboBox;
	TSpeedButton *UnexpandCreateClipPanelButton_REMOVED_;
	TPanel *UnexpandedNewClipPanel_REMOVED_;
	TSpeedButton *ExpandCreateClipPanelButton_REMOVED_;
	TLabel *UnexpandedNewClipPanellLabel_REMOVED_;
	TSpeedButton *AddNewMediaLocationButton;
	TSpeedButton *CreateMultipleClipsButton;
	TSpeedButton *NoClipSchemeFoundWarningButton;
   TLabel *Location;
   TPanel *LocationComboBoxPositioningPanel2;
   TPanel *LocationComboBoxPositioningPanel1;
	TBitBtn *DurCopyButton;
	TTrackBar *FileDetailsTrackBar;
   TMenuItem *ClipPopupShowHideAlphaMatteItem;
	TMenuItem *BinPopupSeparator2;
	TPanel *Panel1;
	TLabel *FileDetailsFileNameLabel;
	TLabel *FileDetailsFormatLabel;
	TLabel *FileDetailsDimensionsLabel;
	TLabel *FileDetailsTimecodeLabel;
	TLabel *FileDetailsFileNameValue;
	TLabel *FileDetailsFormatValue;
	TLabel *FileDetailsDimensionsValue;
	TLabel *FileDetailsTimecodeValue;
	TNakedProxyFrame *FileDetailsProxyFrame;
	TSpeedButton *FileDetailsShowHeaderButton;
        TMenuItem *ClipPopupCreateNewVersionItem;
	TPanel *RightPanel;
	TListView *ClipListView;
	TTimer *ClipRefreshListSweepTimer;
        TListView *VersionListView;
        TSplitter *ClipListViewSplitter;
        TPopupMenu *VersionListViewPopupMenu;
        TMenuItem *VersionPopupDeleteMenuItem;
   TMenuItem *VersionListViewPopupMenuSeparator1;
        TMenuItem *VersionPopupCommitVersionMenuItem;
   TMenuItem *VersionListViewPopupMenuSeparator2;
        TMenuItem *VersionPopupDiscardChangesMenuItem;
        TMenuItem *VersionPopupEditCommentMenuItem;
        TMenuItem *ClipPopupOpenClipItem;
        TMenuItem *ClipPopupSeparator0;
    TMenuItem *BinPopupShareInAndOutMarks;
        TMainMenu *MainMenu;
        TMenuItem *TFileSubmenuItem;
        TMenuItem *TEditSubmenuItem;
        TMenuItem *TViewItem;
        TMenuItem *TClipMenuItem;
   TMenuItem *FileMenuCreateBinItem;
   TMenuItem *FileMenuDeleteBinItem;
   TMenuItem *FileMenuRenameBinItem;
        TMenuItem *TSelectAllItem;
        TMenuItem *TInvertSelectItem;
        TMenuItem *N2;
        TMenuItem *TPreferenceItem;
        TMenuItem *TRefreshMenuItem;
        TMenuItem *N4;
	TMenuItem *TViewCreateClipItem_REMOVED_;
        TMenuItem *TViewProxyDisplayItem;
        TMenuItem *OpenClipMenuItemX;
        TMenuItem *N10;
   TMenuItem *ImportTimecodesMenuItem;
   TMenuItem *ExportClipInfoSubMenu;
   TMenuItem *ExportAllClipInfoMenuItem;
        TMenuItem *N11;
   TMenuItem *UpdateCutsMenuItem;
        TMenuItem *N12;
        TMenuItem *PropertiesItem;
   TMenuItem *ExportSelectedClipInfoMenuItem;
        TMenuItem *TCloseItem;
        TMenuItem *N14;
   TMenuItem *N3;
   TMenuItem *ClipPopupEditCommentsItem;
   TMenuItem *ClipPopupCountNumberOfFixesItem;
   TMenuItem *VersionCountNumberofFixesMenuItem;
   TPanel *CreateClipFromFileOrFolderPanel;
   TMenuItem *N5;
   TMenuItem *N6;
   TMenuItem *FileMenuCreateJobItem;
   TMenuItem *BinPopupCreateJobItem;
   TMenuItem *ClipPopupBrowseToFolderItem;
   TMenuItem *VersionPopupBrowseToFolderItem;
   TMenuItem *CreateVersionOfVersionMenuItem;
   TMenuItem *VersionListViewPopupMenuSeparator3;
	TRadioButton *ChooseFolderRadioButton;
	TRadioButton *ChooseFileRadioButton;
	TBitBtn *ClipFileBrowserButton;
	TBitBtn *ClipFolderBrowserButton;
	TLabel *ClipFileNameLabel;
   TMenuItem *VersionPopupExportVersionMenuItem;
   TMenuItem *VersionPopupExportMarkedRangeMenuItem;
   TMenuItem *VersionPopupExportFullClipMenuItem;
   TMenuItem *N1;
   TMenuItem *RelinkMediaFolderMenuItem;
   TMenuItem *ClipPopupStripAlphaChannelfromMediaFilesMenuItem;
   TMenuItem *ClipPopupChangeDpxHeaderFrameRateMenuItem;

   void __fastcall ProxyVisibleClick(TObject *Sender);
   void __fastcall TBCloseItemClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall TBRefreshMenuItemClick(TObject *Sender);
   void __fastcall BinTreeChange(TObject *Sender, TTreeNode *Node);
   void __fastcall FileMenuCreateBinItemClick(TObject *Sender);
   void __fastcall CreateClipButtonClick(TObject *Sender);
   void __fastcall OpenClipMenuItemOrDoubleClick(TObject *Sender);
   void __fastcall DeleteClipMenuItemClick(TObject *Sender);
   void __fastcall FileMenuDeleteBinItemClick(TObject *Sender);
   void __fastcall ClipListViewColumnClick(TObject *Sender,
	  TListColumn *Column);
   void __fastcall TBSelectAllItemClick(TObject *Sender);
   void __fastcall TBInvertSelectItemClick(TObject *Sender);
	void __fastcall ClipSchemePopupBoxChange(TObject *Sender);
	void __fastcall ClipListViewCustomDrawItem(TCustomListView *Sender,
	  TListItem *Item, TCustomDrawState State, bool &DefaultDraw);
	void __fastcall MediaLocationComboBoxChange(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FolderBrowseButtonClick(TObject *Sender);
	void __fastcall ClipFolderOrFileBrowserButtonClick(TObject *Sender);
  void __fastcall InOutFileClipEditExit(TObject *Sender);
  void __fastcall FileClipFileNameEditExit(TObject *Sender);
  void __fastcall DurFileClipEditExit(TObject *Sender);
  void __fastcall FileClipFileNameEditChange(TObject *Sender);
  void __fastcall CreateFileClipButtonClick(TObject *Sender);
    void __fastcall BinTreeDblClick(TObject *Sender);
	void __fastcall ImportTimecodesItemClick(TObject *Sender);
	void __fastcall ExportSelectedClipInfoMenuItemClick(TObject *Sender);
    void __fastcall ClipListPopupMenuPopup(TObject *Sender);
    void __fastcall ClipMenuBarItemClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall ImageProxyPaint(TObject *Sender);
    void __fastcall TrackBarProxyChange(TObject *Sender);
    void __fastcall CheckBoxCentralizeClick(TObject *Sender);
    void __fastcall TBViewCreateClipItem_REMOVED_Click(TObject *Sender);
	void __fastcall VTimeCodeChange(TObject *Sender);
	void __fastcall InOutFileClipEditKeyPress(TObject *Sender, char &Key);
	void __fastcall DurFileClipEditKeyPress(TObject *Sender,
	  char &Key);
	void __fastcall ClipListViewSelectItem(TObject *Sender,
	  TListItem *Item, bool Selected);
    void __fastcall ImportImageFilesMenuItemClick(TObject *Sender);
    void __fastcall ExportImageFilesMenuItemClick(TObject *Sender);
    void __fastcall ClipListViewKeyPress(TObject *Sender, char &Key);
	void __fastcall CenCueButtonClick(TObject *Sender);
	void __fastcall CenCopyButtonClick(TObject *Sender);
	void __fastcall InCueButtonClick(TObject *Sender);
	void __fastcall InCopyButtonClick(TObject *Sender);
	void __fastcall OutCueButtonClick(TObject *Sender);
	void __fastcall OutCopyButtonClick(TObject *Sender);
	void __fastcall CheckCopyFromVTRTimerTimer(TObject *Sender);
	void __fastcall TBPreferenceItemClick(TObject *Sender);
    void __fastcall RadioButtonClick(TObject *Sender);
	void __fastcall BinTreePopupMenuPopup(TObject *Sender);
	void __fastcall BinTreeClick(TObject *Sender);
	void __fastcall ClipSchemeInfoButtonClick(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall ScanListEditButtonClick(TObject *Sender);
	void __fastcall LicenseEditButtonClick(TObject *Sender);
	void __fastcall LicenseSaveButtonClick(TObject *Sender);
	void __fastcall ScanListSaveButtonClick(TObject *Sender);

	void __fastcall RunCutDetectionMenuItemClick(TObject *Sender);
	void __fastcall MultiBrowseButtonClick(TObject *Sender);
    void __fastcall FileClipTCCheckBoxClick(TObject *Sender);
    void __fastcall FileMenuRenameBinItemClick(TObject *Sender);
    void __fastcall RenameClipMenuItemClick(TObject *Sender);
    void __fastcall BinTreeDragOver(TObject *Sender, TObject *Source,
	  int X, int Y, TDragState State, bool &Accept);
    void __fastcall BinTreeDragDrop(TObject *Sender, TObject *Source,
	  int X, int Y);
	void __fastcall ClipListViewCustomDrawSubItem(
	  TCustomListView *Sender, TListItem *Item, int SubItem,
	  TCustomDrawState State, bool &DefaultDraw);
   void __fastcall VideoFileBrowseButtonClick(TObject *Sender);
   void __fastcall VideoFileEditChange(TObject *Sender);
   void __fastcall VideoFileEditExit(TObject *Sender);
	void __fastcall ShowProxyTimecodeMenuItemClick(TObject *Sender);
	void __fastcall CreateClipSelectionRadioButtonClick(TObject *Sender);
	void __fastcall ComboBoxExit(TObject *Sender);
	void __fastcall ComboBoxKeyPress(TObject *Sender, char &Key);
	void __fastcall ComboBoxSelect(TObject *Sender);
	void __fastcall ComboBoxEnter(TObject *Sender);
	void __fastcall FileVidClipVideoFolderBrowseButtonClick(
	  TObject *Sender);
	void __fastcall FileVidClipAudioFolderBrowseButtonClick(
	  TObject *Sender);
	void __fastcall ClipFormatComponentChanged(TObject *Sender);
	void __fastcall ExistingMediaFormatInfoButtonClick(
	  TObject *Sender);
	void __fastcall NewMediaFormatInfoButtonClick(TObject *Sender);
	void __fastcall FileVidClipTCIndexCheckBoxClick(TObject *Sender);
	void __fastcall UnexpandCreateClipPanelButton_REMOVED_Click(
	  TObject *Sender);
	void __fastcall NoClipSchemeFoundWarningButtonClick(TObject *Sender);
	void __fastcall FileDetailsTrackBarChange(TObject *Sender);
	void __fastcall DurCopyButtonClick(TObject *Sender);
	void __fastcall ShowHideAlphaMatteMenuItemClick(TObject *Sender);
	void __fastcall FileDetailsShowHeaderButtonClick(TObject *Sender);
	void __fastcall ClipListViewData(TObject *Sender, TListItem *Item);
	void __fastcall ClipRefreshListSweepTimerTimer(TObject *Sender);
	void __fastcall CreateNewVersionMenuItemClick(TObject *Sender);
	void __fastcall OKButtonClick(TObject *Sender);
	void __fastcall ClipListViewChanging(TObject *Sender,
	  TListItem *Item, TItemChange Change, bool &AllowChange);
	void __fastcall CancelButtonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BinTreeChanging(TObject *Sender, TTreeNode *Node,
	  bool &AllowChange);
        void __fastcall VersionListViewColumnClick(TObject *Sender,
          TListColumn *Column);
        void __fastcall VersionListViewCustomDrawItem(
          TCustomListView *Sender, TListItem *Item, TCustomDrawState State,
          bool &DefaultDraw);
        void __fastcall VersionListViewCustomDrawSubItem(
          TCustomListView *Sender, TListItem *Item, int SubItem,
          TCustomDrawState State, bool &DefaultDraw);
        void __fastcall VersionListViewData(TObject *Sender,
          TListItem *Item);
        void __fastcall VersionListViewDblClick(TObject *Sender);
        void __fastcall VersionListViewKeyPress(TObject *Sender,
          char &Key);
        void __fastcall VersionListViewSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
        void __fastcall VersionListViewPopupMenuPopup(TObject *Sender);
        void __fastcall VersionPopupCommitVersionMenuItemClick(TObject *Sender);
        void __fastcall VersionPopupDiscardChangesMenuItemClick(
          TObject *Sender);
        void __fastcall VersionPopupEditCommentMenuItemClick(
          TObject *Sender);
        void __fastcall VersionPopupDeleteMenuItemClick(TObject *Sender);
    void __fastcall BinPopupShareInAndOutMarksClick(TObject *Sender);
    void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FormKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FileClipNameEditKeyPress(TObject *Sender,
          char &Key);
        void __fastcall VideoClipNameEditKeyPress(TObject *Sender,
          char &Key);
   void __fastcall ClipPopupEditCommentsItemClick(TObject *Sender);
   void __fastcall CountNumberOfFixesMenuItemClick(TObject *Sender);
   void __fastcall FileMenuBarItemClick(TObject *Sender);
   void __fastcall ViewMenuBarItemClick(TObject *Sender);
   void __fastcall EditMainBarItemClick(TObject *Sender);
   void __fastcall FileMenuCreateJobItemClick(TObject *Sender);
   void __fastcall BrowseToFolderMenuItemClick(TObject *Sender);
   void __fastcall BrowseToVersionFolderMenuItemClick(TObject *Sender);
   void __fastcall CreateVersionOfVersionMenuItemClick(TObject *Sender);
	void __fastcall ChooseFolderOrChooseFileRadioButtonClick(TObject *Sender);
   void __fastcall VersionPopupExportMarkedRangeMenuItemClick(TObject *Sender);
   void __fastcall VersionPopupExportFullClipMenuItemClick(TObject *Sender);
   void __fastcall VersionPopupExportVersionMenuItemClick(TObject *Sender);
   void __fastcall RelinkMediaFolderMenuItemClick(TObject *Sender);
   void __fastcall ClipPopupStripAlphaChannelFromMediaFilesMenuItemClick(TObject *Sender);
   void __fastcall ClipPopupChangeDpxHeaderFrameRateMenuItemClick(TObject *Sender);
	void __fastcall BinTreeExpanding(TObject *Sender, TTreeNode *Node, bool &AllowExpansion);




private:	// User declarations

   // Original startup preferences
   int OriginalBinTreeWidth;
   bool OriginalCreateClipPanelVisible;
   bool OriginalCreateProxyPanelVisible;
   IntegerList DefaultColumnWidths;

   // sHighLightBin does double duty as the highlighted bin
   // and the currently loaded bin.
   string HighlightedBinPath;
   string HighlightedClipName;

   int SweepTimerAutoShutoff;

   void AddClipSchemeSubmenuItem(TMenuItem *menuItem, const string& caption, int tag);
	int  CountSelectedClips();
	void AddDummyBinTreeNodeChildIfNestedBins(string &binPath, TTreeNode * parentNode);
	void AddBinTreeNodeChildrenRecursively(string &path, TTreeNode* parentNode);
	void AddOneLevelOfBinTreeNodeChildren(string &binPath, TTreeNode * parentNode, bool forceRefresh = false);
	int  FindClipListIndexByClipName(const string &clipName);
	string getBinPathFromBinTreeNode(TTreeNode* node);
   TTreeNode *getBinTreeNodeFromBinPath(const string& path);
	void HideColumn(int index);
   void InitCreateClipPanel();
   void InitClipSchemeComboBox();
   void InitMediaLocationComboBox();
   bool IsClipSchemeValid(string const &clipScheme);
   bool IsMediaLocationValid(string const &mediaLocation);
   void MakeClipSchemeSubmenu(TMenuItem *menuItem, unsigned int filter);
   void PopulateClipViewItem(TListItem &listItem,
			     const SClipDataItem &clipDataItem);
   void PopulateVersionViewItem(TListItem &listItem,
			        const SVersionDataItem &versionDataItem);
   void ProxyVisibleToggle();
   void RefreshBinTree();
   void RefreshTimeRemaining();
   int  RemoveClipFromVersionList(vector<SVersionDataItem *> *versionList,
                                  int versionIndex);
   void DeleteAllVersionsOfClip(ClipIdentifier clipId);
   void SetClipSchemeName(const string& newClipSchemeName);
   string GetFirstAvailableClipScheme();
   string GetFirstAvailableDiskScheme();
   void SetDefaultClipName();
   void SetDefaultMediaSchemes();
   void Timecode2AnsiString(const CTimecode &timecode,
			    AnsiString &stringOut);
   void UpdateTimecodesWithNewClipScheme(const string& clipSchemeName);
   void ReportCreateClipError(int errorCode);
   void clearRightClick();
   bool IncreaseClipMedia(CClipInitInfo *clipInfo, TDailiesClipForm *dailiesClipForm);
   bool DecreaseClipMedia(CClipInitInfo *clipInfo, TDailiesClipForm *dailiesClipForm);
   bool getImageMediaDuration(CClipInitInfo const *clipInfo, CTimecode &duration);
	bool getAudioMediaDuration(CClipInitInfo const *clipInfo, CTimecode &duration);
   string FindAnAcceptableImageFile(string directory);
   string BrowseForFolder(string startingFolderPath);
	void BrowseForMediaFolder();
	void BrowseForMediaFile();
	void DoSuperclipCrap();

   friend class TDailiesClipForm;
   friend class TDeleteBinDialog;

   string lastClickedBinPath;
   string lastClickedClipPath;
   string lastClickedVersionPath;
   string rightClickedBinPath;
   TTreeNode *rightClickedNode;
   static CBinMgrGUIInterface *binMgrGUIInterface;
   int ClipListColumnToSort;
   int VersionListColumnToSort;
   int opMode;
   string clipSchemeName;

   ProxyDisplayer *FileDisplayer;


   // Values found in the interface
   bool MakeNewClip(void);

   long _ClipFileDigitPrecision;
   bool _bClipFileNameChange;
   bool _bVideoFileNameChange;
   string MakeClipFileName(TEdit *folderEdit, TEdit *filenameEdit);

   bool IsControlDailies();

   typedef enum {
       clipPanelTypeFile,
       clipPanelTypeVideo,
       clipPanelTypeLicense,
       clipPanelTypeProject,
       clipPanelTypeScanList
   } eClipPanelType;

   void ChangeClipPanelType(eClipPanelType eType);
   void GetLicenseInfo();
   void GetProjectInfo();
   void GetScanListInfo();
   void MakeLicenseInfoEditable(bool doit);
   void MakeScanListInfoEditable(bool doit);
   bool MakeNewDailiesClip(string const &newClipName,
			   TDailiesClipForm const *dailiesClipForm);

   // used to create a proxy display of the clip
   ClipSharedPtr clipProxy;
   CVideoFrameList *vflpProxy;
   CConvert *cnvtProxy;
   MTI_UINT32 *uipProxy;
   MTI_INT32 iStoredPosition;
   string strStoredTimecode;
   string strStoredProxy;
   void DisplayProxy(bool Recreate = false);
   void SaveProxy();
   void LoadProxy();

   void CalcDuration();
   void CalcCentral();
   void CalcInOut();
   void SetVTimeCodeValue (VTimeCodeEdit *tce, int i);
   void SetVTimeCodeValue (VTimeCodeEdit *tce, const CTimecode &tc);
   void SetVTimeCodeValue (VTimeCodeEdit *tce, const string &str);

   void UpdateTabOrder();

   void FreeMedia(TObject *Sender, ETrackType trackType);

   int GetTotalFrames(TStringList *tslClips);
   ClipSharedPtr CreateSuperClip (const string &strClipName, int iNFrame,
				       ClipSharedPtr &clipSrc, int *ipRet);
   int MakeBigVirtualClip(TStringList *srcClipNames, ClipSharedPtr &dstClip);
   bool BrowseForFileName(TEdit *FolderEdit, TEdit *FileEdit, const string &FileNameFilter, int FilterIndexArg);

   void InitCreateClipComboBoxes();
   void FixCreateClipPanelSize();
   void HackVideoStoreUsage();
   void ShowClipSchemeInfo(const string &newClipSchemeName);

   string FileVidClipVideoFolderComboBoxTextAtEntry;
   string FileVidClipAudioFolderComboBoxTextAtEntry;

   string YellowHilitedMasterClip;
   ClipVersionNumber YellowHilitedVersion;
#ifndef NO_LICENSING
   CRsrcCtrl _License;
#endif

   // File header panel stuff
   void UpdateFileDetailsPanel(const string &fileNameTemplate, int frameIndex=0);
   string sImageFileNameTemplate;
   int iImageFileIndex;
   string strImageFileHeader;

   // "Owner data" clip list stuff
   typedef vector<ClipSharedPtr> ClipListType;
   typedef vector<SClipDataItem *> ClipListDataType;
   typedef vector<SVersionDataItem *> VersionListDataType;
   ClipListDataType *CurrentBinClipListData;
   int selectedClipCount;
   bool selectedListIsInvalid;
   typedef map<string, ClipListDataType*, BinPathLessThan> BinClipListCacheType;
   BinClipListCacheType BinClipListCache;

   int AddClipToClipListData(ClipSharedPtr &clip, SClipDataItem **clipDataItemOut = NULL);
   int AddClipToClipListView(ClipSharedPtr &clip);
   int AddClipsToClipListView(ClipListType *list);
   void ClearAllSelections();
   void ClearBinClipListCache();
   void ClearClipList(ClipListDataType* clipListData);
	int GenerateClipListData(ClipListDataType *clipListData,
			    const string &binPath,
			    StringList *newSelectedList = NULL);
public: // so the clip status notify callback hack can call it
   int RefreshClipList(StringList *newSelectedList = NULL);
   int RefreshOneClipListItemByListIndex(int listIndex);
   int RefreshOneClipListItemByName(const string &clipName);
   void RepaintVersionListView();
private:
   void RemoveClipFromTheClipList(const string &clipName);
   void RemoveClipsFromClipListView(StringList *clipNames);
   void RepaintClipListView();
   void SetClipListViewItemSelectedState(int index, bool newState);
   void SetVersionListViewItemSelectedState(
                          vector<SVersionDataItem *> *versionList,
                          int index, bool newState);
   void SortClipListData();
   void SortSelectedClipVersionList();
   void SortVersionListData(vector<SVersionDataItem *> &versionList);
   void SynchronizeSelectionsIfNecessary();
	void CheckRootNodeStorage (const string &strRootPath);

   void CreateJob(JobInfo jobInfo);
   bool EditJobInfo(JobInfo &jobInfo, const string &caption);

   string computeDefaultExportDirectory(ClipIdentifier clipId);

public:		// User declarations
   __fastcall TBinManagerForm(TComponent* Owner);
	virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
	virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
   virtual bool ReadDesignSAP(void);

   static void RegisterBinMgrGUIInterface(CBinMgrGUIInterface *newInterface);
   void SetOpMode(int mode);
   int GetSelectedClips(StringList& clipList);
   int GetAllClips(StringList& clipList);
   StringList SelectedClipNames(void);
   void SetSelectedClips(StringList& clipList);
   string GetSelectedBin();
   void SetSelectedBin(const string& bin);
   void CancelHighlightClip ();
   bool HighlightClip(const string &ClipName, bool bShow=false);
   void LoadNewClip(string const &clipPath, int offsetToNewClip=0);
   void LoadNewVersion(string const &clipPath, int offsetToNewClip=0);
   string GetNewClipScheme(void);
   int DeleteOneClip(const string &binPath, const string &clipName);
   int ArchiveClip(const string &binPath, const string &clipName);

   // to keep track of BusyCursor
   int oldBusyCursor;

   // These are used to close up the proxy display
   void ClearProxy(void);
   void ReloadProxy(void);

   // *** USE THIS INSTEAD OF BinManagerForm->FormStyle = fsWhatever  ***
   // ***     BinManagerForm->SetFormStyle(fsWhatever);
   void SetFormStyle(TFormStyle style);
//   void ShowImportExportDialog(ImportExportFormBase *form);

   bool DeleteFileOrDirectory(string const &FileOrDirectory);

   void GetArchiveRelatedFlags(int  selectCount,
			       bool &AreAnyCreated,
			       bool &AreAnyArchived,
			       bool &AreAllCreated,
			       bool &AreAllArchived);
   // HACK - callback from CommitVersionForm to delete version clip after
   // successful commit
   void FinishCommittingClip(CClipInitInfo *aVersionClipInfo,
                             CClipInitInfo *aSourceClipInfo,
                             bool clipWasCommitted);

   // Version clips hack
   void CreateNewVersionClip(const string &parentPath);
   void CommitVersionClipChanges(const string & versionClipPath, CTimecode markIn = CTimecode::NOT_SET, CTimecode markOut = CTimecode::NOT_SET);
   void DiscardVersionClipChanges(const string & versionClipPath, CTimecode markIn = CTimecode::NOT_SET, CTimecode markOut = CTimecode::NOT_SET);
   void ExportVersionClipFiles(const string &versionClipPath, int markIn = -1, int markOut = -1);
   int DeleteVersionClip(ClipIdentifier versionId);
   bool FindSelectedVersionClip(vector<SVersionDataItem*> &versionList, vector<SVersionDataItem*>::iterator &selectedIter);

};

//---------------------------------------------------------------------------
extern PACKAGE TBinManagerForm MTI_BINMGRLIB_API *BinManagerForm;

//---------------------------------------------------------------------------
// Global Function Declarations

void MTI_BINMGRLIB_API DeleteBinManagerGUI();
void MTI_BINMGRLIB_API ShowBinManagerGUI(bool show, int mode);
void MTI_BINMGRLIB_API DupMenuItem(TMenuItem *Out, TMenuItem *In);
void MTI_BINMGRLIB_API __fastcall CopyMenu(TMenu *Out, TMenu *In);

//---------------------------------------------------------------------------
#endif // #ifndef BinManagerGUIUnitH
