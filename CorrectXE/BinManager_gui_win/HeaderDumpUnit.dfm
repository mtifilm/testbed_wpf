object HeaderDumpForm: THeaderDumpForm
  Left = 48
  Top = 48
  ClientHeight = 662
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsNormal
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderDumpMemo: TRichEdit
    Left = 0
    Top = 0
    Width = 584
    Height = 635
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    PlainText = True
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
  end
  object ControlPanel: TPanel
    Left = 0
    Top = 635
    Width = 584
    Height = 27
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      584
      27)
    object FilePathLabel: TLabel
      Left = 12
      Top = 8
      Width = 64
      Height = 13
      Caption = 'FilePathLabel'
    end
    object CloseButton: TButton
      Left = 540
      Top = 5
      Width = 45
      Height = 19
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
    end
  end
end
