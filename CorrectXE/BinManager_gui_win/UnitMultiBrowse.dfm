object FormMultiBrowse: TFormMultiBrowse
  Left = 43
  Top = 219
  Caption = 'Create Multiple Image File-Based Clips'
  ClientHeight = 682
  ClientWidth = 804
  Color = clBtnFace
  Constraints.MinHeight = 640
  Constraints.MinWidth = 800
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMode = pmExplicit
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    804
    682)
  PixelsPerInch = 96
  TextHeight = 13
  object TopPanel: TPanel
    Left = 8
    Top = 8
    Width = 794
    Height = 73
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      794
      73)
    object BaseFoilderLabel: TLabel
      Left = 16
      Top = 8
      Width = 304
      Height = 16
      Caption = 'Base folder that contains all frames of interest'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BrowseBaseDirPanel: TPanel
      Left = 16
      Top = 28
      Width = 345
      Height = 29
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        345
        29)
      object BrowseBaseDirButton: TSpeedButton
        Left = 319
        Top = 3
        Width = 22
        Height = 22
        Anchors = [akRight, akBottom]
        Flat = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000CE0E0000C40E00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          77777777777777777777000000000007777700333333333077770B0333333333
          07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
          77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
          7007777777770777070777777777700077777777777777777777}
        OnClick = BrowseBaseDirButtonClick
      end
      object ComboBoxBaseDir: TComboBox
        Left = 0
        Top = 4
        Width = 313
        Height = 21
        Hint = 
          'Type a local or network directory which contains all the frames ' +
          'of interest'
        Anchors = [akLeft, akRight, akBottom]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'Select a folder'
        OnChange = ComboBoxBaseDirChange
        OnExit = ComboBoxBaseDirExit
      end
    end
    object ClipNameTemplatePanel: TPanel
      Left = 376
      Top = 4
      Width = 410
      Height = 65
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 6
        Top = 4
        Width = 124
        Height = 16
        Caption = 'Clip Name Template'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ClipNameTemplateEdit: TEdit
        Left = 8
        Top = 28
        Width = 321
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
        Text = 'Clip###'
      end
      object ClipNameTemplateEditButton: TBitBtn
        Left = 336
        Top = 28
        Width = 65
        Height = 21
        Caption = '  Edit'
        Glyph.Data = {
          AA030000424DAA03000000000000360000002800000011000000110000000100
          1800000000007403000000000000000000000000000000000000FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF0000FFFF000000002571845D5F6AFFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          0000FFFF006EB7CB0302008FC8D7292D49595A64FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000FFFF0080ADB88DC8D7E2
          F1F390C9D82D2C4C2C2C4AFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF0000FFFF00FFFF008FC8D7E6F2F636A2BA8FC9D53DB7E52A
          2C4AFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000FFFF
          00FFFF007CABB393C9DA97CBDB60C6F15FC5F040B7E42B2E4AFFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF0000FFFF00FFFF00FFFF006BBBCC9DDC
          F88AD4F675CEF371CEF53EB6E42B2F4CFFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF0000FFFF00FFFF00FFFF00FFFF0032B1DE9BDDF686D4F973CEF572CD
          F43BB8E52C2F4BFFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000FFFF00FFFF00
          FFFF00FFFF00FFFF006CB7CD9ADCF985D6F976CDF574CCF43FB8E4282E4BFFFF
          00FFFF00FFFF00FFFF00FFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          6EBACD9FDAFA84D5F872CCF575CDF53FB8E42B2E4AFFFF00FFFF00FFFF00FFFF
          0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF006EBACC9ADCF987D6F7
          64BFE4AAA8A869686A060509FFFF00FFFF00FFFF0000FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF0070B9CD93D8F9B0B0B0ADAAAC939191040705
          2C2E4CFFFF00FFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF0061B5C7909090B2B2B2918F8E8487E28287E22B2D4BFFFF0000FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00ACACAC68
          6769A8ACEDA6ABEA8188E541475AFFFF0000FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF004C59D9D3D5F3A7ABEC8288E16A
          6671FFFF0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF004C5BDA4D57DA5F64C7FFFF00FFFF0000FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF0000}
        TabOrder = 1
        OnClick = ClipNameTemplateEditButtonClick
      end
    end
  end
  object MiddlePanel: TPanel
    Left = 8
    Top = 88
    Width = 794
    Height = 314
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      794
      314)
    object BrowseFolderTreeLabel: TLabel
      Left = 16
      Top = 8
      Width = 580
      Height = 32
      Caption = 
        'Browse the folder tree and select entire bin folders or individu' +
        'al frame ranges of interest'#13#10
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SelectMultipleLabel: TLabel
      Left = 19
      Top = 261
      Width = 260
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Use SHIFT-Click and CTRL-Click to select multiple items'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object TakeLongTimeLabel: TLabel
      Left = 665
      Top = 228
      Width = 117
      Height = 21
      Anchors = [akTop, akRight, akBottom]
      AutoSize = False
      Caption = '(may take a long time)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object BinTree: TTreeView
      Left = 16
      Top = 36
      Width = 337
      Height = 221
      Hint = 
        'Browse and select all the directories and/or frame ranges of int' +
        'erest'
      Anchors = [akLeft, akTop, akRight, akBottom]
      HideSelection = False
      HotTrack = True
      Indent = 19
      MultiSelect = True
      MultiSelectStyle = [msControlSelect, msShiftSelect]
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      OnChange = BinTreeChange
      OnCustomDrawItem = BinTreeCustomDrawItem
      OnExpanding = BinTreeExpanding
      Items.NodeData = {
        03050000002A0000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        000200000001064800650072004400690072002C0000000000000000000000FF
        FFFFFFFFFFFFFF00000000000000000000000001075300750062004400690072
        0031002C0000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
        000000010753007500620044006900720032002A0000000000000000000000FF
        FFFFFFFFFFFFFF00000000000000000000000001064800690073004400690072
        00280000000000000000000000FFFFFFFFFFFFFFFF0000000000000000020000
        0001054D0079004400690072002C0000000000000000000000FFFFFFFFFFFFFF
        FF000000000000000000000000010753007500620044006900720031002C0000
        000000000000000000FFFFFFFFFFFFFFFF000000000000000000000000010753
        007500620044006900720032002E0000000000000000000000FFFFFFFFFFFFFF
        FF0000000000000000000000000108540068006500690072004400690072002C
        0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000200000001
        0759006F00750072004400690072002C0000000000000000000000FFFFFFFFFF
        FFFFFF000000000000000000000000010753007500620044006900720031002C
        0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000000000001
        075300750062004400690072003200}
    end
    object CheckBoxCheckHeaders: TCheckBox
      Left = 385
      Top = 221
      Width = 281
      Height = 29
      Hint = 
        'Checking headers is time consuming when creating a clip out of m' +
        'any frames'
      Anchors = [akRight, akBottom]
      Caption = 'Examine each image header when adding items to list'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object GroupBox1: TGroupBox
      Left = 369
      Top = 148
      Width = 412
      Height = 77
      Anchors = [akTop, akRight]
      TabOrder = 2
      DesignSize = (
        412
        77)
      object CustomTimecodeLabel: TLabel
        Left = 164
        Top = 51
        Width = 14
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'TC'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object FileLabel: TLabel
        Left = 10
        Top = 51
        Width = 47
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'File Num'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EqualsLabel: TLabel
        Left = 144
        Top = 46
        Width = 11
        Height = 23
        Anchors = [akLeft, akBottom]
        Caption = '='
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Black'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object FileClipDFLabel: TLabel
        Left = 272
        Top = 51
        Width = 13
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'DF'
        Enabled = False
      end
      object FileClipFPSLabel: TLabel
        Left = 330
        Top = 51
        Width = 18
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'FPS'
        Enabled = False
      end
      object UseFileNumbersRadioButton: TRadioButton
        Left = 12
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Use file numbers'
        Checked = True
        Color = clBtnFace
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnClick = UseFileNumbersRadioButtonClick
      end
      object UseTimecodesRadioButton: TRadioButton
        Left = 136
        Top = 16
        Width = 105
        Height = 17
        Caption = 'Use timecodes'
        TabOrder = 1
        OnClick = UseTimecodesRadioButtonClick
      end
      object CalcTCEdit: VTimeCodeEdit
        Left = 184
        Top = 48
        Width = 72
        Height = 19
        Anchors = [akLeft, akBottom]
        TabOrder = 2
        AllowVTRCopy = False
      end
      object CalcFileNumberEdit: TEdit
        Left = 64
        Top = 48
        Width = 72
        Height = 21
        Anchors = [akLeft, akBottom]
        TabOrder = 3
        Text = '0'
      end
      object FileClipDFCheckBox: TCheckBox
        Left = 292
        Top = 49
        Width = 18
        Height = 17
        Anchors = [akLeft, akBottom]
        Enabled = False
        TabOrder = 4
      end
      object FileClipFPSComboBox: TComboBox
        Left = 356
        Top = 48
        Width = 45
        Height = 21
        Anchors = [akLeft, akBottom]
        Enabled = False
        ItemIndex = 0
        TabOrder = 5
        Text = '24'
        Items.Strings = (
          '24'
          '25'
          '30')
      end
      object CopyTCFromHeaderButton: TBitBtn
        Left = 264
        Top = 12
        Width = 137
        Height = 25
        Caption = 'Copy TC from Header'
        Enabled = False
        Glyph.Data = {
          E6040000424DE604000000000000360000002800000014000000140000000100
          180000000000B0040000120B0000120B0000000000000000000020FFFF20FFFF
          20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FF
          FF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF
          20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FF
          FF20FFFF20FFFF1E1A1B0101010101010101010101011E1A1B20FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF232123
          010101171717FFFFFFDBDBDBDBDBDBFFFFFF17171701010123212320FFFF20FF
          FF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF151314060606FFFFFFF6
          F6F6FFFFFF4A4A4A4A4A4AFFFFFFF6F6F6FFFFFF06060615131420FFFF20FFFF
          20FFFF20FFFF20FFFF20FFFF20FFFF232020070707FFFFFFABABAB232323FFFF
          FFFFFFFFFFFFFFFFFFFF232323ABABABFFFFFF07070723202020FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF000000FFFFFFABABABFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFABABABFFFFFF00000020FFFF20FFFF20FFFF20FF
          FF20FFFF1E1A1B121212F8F8F8212121FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF232323F8F8F81212121E1A1B20FFFF20FFFF20FFFF20FFFF
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFEFDFDFFFFFFFFFFFFFFFFFF00000020FFFF20FFFF20FFFF20FFFF010101DB
          DBDB4A4A4AFFFFFFFFFFFFFFFFFFFFFFFFC5C5C5121212080707080808B2B2B2
          FEFEFE4A4A4ADBDBDB01010120FFFF20FFFF20FFFF20FFFF010101DBDBDB4A4A
          4AFFFFFFFFFFFFFFFFFFFFFFFF0F0F0F010101101010101010B2B2B2FFFFFF4A
          4A4ADBDBDB01010120FFFF20FFFF20FFFF20FFFF000000FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF0E0E0E0A0A0AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FF00000020FFFF20FFFF20FFFF20FFFF1E1A1B121212F8F8F8212121FFFFFFFF
          FFFFFFFFFF101010080808FFFFFFFFFFFFFFFFFF212121F8F8F81212121E1A1B
          20FFFF20FFFF20FFFF20FFFF20FFFF000000FFFFFFABABABFFFFFFFFFFFFFFFF
          FF101010080808FFFFFFFFFFFFFFFFFFABABABFFFFFF00000020FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF232020070707FFFFFFABABAB232323FFFFFF101010
          080707FFFFFF232323ABABABFFFFFF07070723202020FFFF20FFFF20FFFF20FF
          FF20FFFF20FFFF20FFFF151314060606FFFFFFF6F6F6FFFFFF0A0A0A090909FF
          FFFFF6F6F6FFFFFF06060615131420FFFF20FFFF20FFFF20FFFF20FFFF20FFFF
          20FFFF20FFFF20FFFF232123010101171717FFFFFFFEFEFEFEFEFEFFFFFF1717
          1701010123212320FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF1E1A1B0101010101010101010101011E1A1B20FFFF
          20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FF
          FF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20
          FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF
          20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FFFF20FF
          FF20FFFF20FFFF20FFFF}
        TabOrder = 6
        OnClick = CopyTCFromHeaderButtonClick
      end
    end
    object ButtonAddSelectedObjects: TBitBtn
      Left = 453
      Top = 249
      Width = 217
      Height = 25
      Hint = 
        'Add the selected bins and/or frame ranges to the list of clips w' +
        'hich will '#13#10'be created'
      Anchors = [akRight, akBottom]
      Caption = 'Add selected items to Create Clips list'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 3
      OnClick = ButtonAddSelectedObjectsClick
    end
    object MessagePanel: TPanel
      Left = 16
      Top = 281
      Width = 764
      Height = 21
      Anchors = [akLeft, akRight, akBottom]
      BevelOuter = bvLowered
      Color = 15263976
      TabOrder = 4
      DesignSize = (
        764
        21)
      object LabelChecking: TLabel
        Left = 4
        Top = 4
        Width = 757
        Height = 13
        Anchors = [akLeft, akRight, akBottom]
        AutoSize = False
        Caption = 'Idle'
      end
    end
    object FileHeaderInfoPanel: TPanel
      Left = 369
      Top = 36
      Width = 412
      Height = 108
      Anchors = [akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 5
      DesignSize = (
        412
        108)
      object FileDetailsPanel: TPanel
        Left = 4
        Top = 4
        Width = 265
        Height = 89
        BevelOuter = bvNone
        TabOrder = 2
        DesignSize = (
          265
          89)
        object FileDetailsFileNameLabel: TLabel
          Left = 8
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Filename:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object FileDetailsFormatLabel: TLabel
          Left = 8
          Top = 36
          Width = 43
          Height = 13
          Caption = 'Format:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object FileDetailsDimensionsLabel: TLabel
          Left = 8
          Top = 52
          Width = 29
          Height = 13
          Caption = 'Size:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object FileDetailsTimecodeLabel: TLabel
          Left = 7
          Top = 68
          Width = 60
          Height = 13
          Caption = 'Timecode:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object FileDetailsFileNameValue: TLabel
          Left = 68
          Top = 20
          Width = 8
          Height = 13
          Caption = '--'
        end
        object FileDetailsFormatValue: TLabel
          Left = 68
          Top = 36
          Width = 8
          Height = 13
          Caption = '--'
        end
        object FileDetailsDimensionsValue: TLabel
          Left = 68
          Top = 52
          Width = 28
          Height = 13
          Caption = '-- x --'
        end
        object FileDetailsTimecodeValue: TLabel
          Left = 68
          Top = 68
          Width = 62
          Height = 13
          Caption = '-- : -- : -- : --'
        end
        object FileDetailsShowHeaderButton: TSpeedButton
          Left = 185
          Top = 56
          Width = 68
          Height = 25
          Hint = 'Browse for multiple clips'
          Anchors = [akLeft, akBottom]
          Caption = 'Header'
          Enabled = False
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000808000C0C0C0FF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            808000008080008080008080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF008080008080008080008080C0
            C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FF008080008080008080008080C0C0C0C0C0C0C0C0C0FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00808000808080
            8080408080408080408080C0C0C0C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF66B3B3808000FFFF80FFFF80FFFF8080800066B3
            B3C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000CA
            CA00FFFF80FFFF80FFFF80FFFF80808000C0C0C0C0C0C0FF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FF408080CACA00FFFF80FFFF80FFFF80FFFF80FFFF80FFFF
            80808080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF408080CACA00FF
            FF80FFFF80FFFF80FFFF80FFFF80FFFF80408080C0C0C0FF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FF408080CACA00FFFF80FFFF80FFFF80FFFF80FFFF80FFFF
            80808080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000CA
            CA00FFFF80FFFF80FFFF80FFFF80808000C0C0C0FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF66B3B3808000CACA00CACA00CACA0080800066B3
            B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FF408080408080408080FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          ParentFont = False
          OnClick = FileDetailsShowHeaderButtonClick
        end
      end
      object FileDetailsTrackBar: TTrackBar
        Left = 12
        Top = 88
        Width = 245
        Height = 15
        Hint = 'Select which frame to preview'
        Max = 1000
        PageSize = 8
        TabOrder = 0
        TabStop = False
        ThumbLength = 12
        TickMarks = tmTopLeft
        TickStyle = tsNone
        OnChange = FileDetailsTrackBarChange
      end
      object FileDetailsProxyPanel: TPanel
        Left = 272
        Top = 1
        Width = 140
        Height = 106
        Anchors = [akLeft, akBottom]
        BevelOuter = bvNone
        TabOrder = 1
        inline FileDetailsProxyFrame: TNakedProxyFrame
          Left = 0
          Top = 0
          Width = 140
          Height = 108
          TabOrder = 0
          ExplicitHeight = 108
          inherited Panel1: TPanel
            Height = 108
            Caption = ''
            ExplicitHeight = 108
            inherited BusyArrowPanel: TPanel
              Top = 71
              Width = 32
              Height = 32
              ExplicitTop = 71
              ExplicitWidth = 32
              ExplicitHeight = 32
              inherited BusyArrows: TMTIBusy
                Left = -2
                Top = 0
                ExplicitLeft = -2
                ExplicitTop = 0
              end
            end
          end
        end
      end
    end
  end
  object BottomPanel: TPanel
    Left = 9
    Top = 408
    Width = 794
    Height = 270
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    DesignSize = (
      794
      270)
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 708
      Height = 16
      Caption = 
        'Review the list of individual clips that will be created (highli' +
        'ghted in green), then click the Create Clips button'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 232
      Top = 214
      Width = 548
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 
        'To remove items from the list, use Click or SHIFT-Click or CTRL-' +
        'Click to select them, then press the Remove button'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object ButtonSelectAll: TSpeedButton
      Left = 441
      Top = 233
      Width = 129
      Height = 27
      Anchors = [akRight, akBottom]
      Caption = 'Select all items in the list'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = ButtonSelectAllClick
    end
    object ListView1: TListView
      Left = 15
      Top = 30
      Width = 766
      Height = 178
      Hint = 'This is the list of all clips which will be created'
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <
        item
          Caption = 'Clip Name'
          Width = 100
        end
        item
          Caption = 'Status'
          Width = 92
        end
        item
          Caption = 'First File Name'
          Width = 128
        end
        item
          Caption = 'Last File Name'
          Width = 128
        end
        item
          Caption = 'First TC'
          Width = 64
        end
        item
          Caption = 'Last TC'
          Width = 64
        end
        item
          Caption = 'Type'
          Width = 40
        end
        item
          Caption = 'Folder'
          Width = 300
        end>
      ColumnClick = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = []
      GridLines = True
      HideSelection = False
      Items.ItemData = {
        054B0100000200000000000000FFFFFFFFFFFFFFFF07000000FFFFFFFF000000
        000743006C00690070003000320039000F52006500610064007900200074006F
        0020006300720065006100740065000000000013670072006500610074005F00
        63006C00690070005F0030003000300030002E00640070007800000000001367
        0072006500610074005F0063006C00690070005F0030003000300030002E0064
        0070007800000000000B310030003A00300030003A00300030003A0030003000
        000000000B310030003A00300030003A00300034003A00300030000000000003
        440050005800000000001E43003A005C0069006D0061006700650073005C0067
        0072006500610074005F00620069006E005C00670072006500610074005F0063
        006C00690070000000000000000000FFFFFFFFFFFFFFFF00000000FFFFFFFF00
        00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      ViewStyle = vsReport
      OnCustomDrawItem = ListView1CustomDrawItem
      OnKeyDown = ListView1KeyDown
    end
    object BitBtnCreate: TBitBtn
      Left = 16
      Top = 233
      Width = 117
      Height = 27
      Hint = 'Create all the clips listed in the bottom panel'
      Anchors = [akLeft, akBottom]
      Caption = ' Create Clips'
      Glyph.Data = {
        26040000424D2604000000000000360000002800000012000000120000000100
        180000000000F0030000120B0000120B00000000000000000000C0C0C0C0C0C0
        C0C0C0C0BDBCBEBABABFBCBCC0BFBFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C3C3C30000C0C0C0C0C0C0C3BDB4C06E3A7200
        00881717B39191C0C0C0C0C0C0C0C0C0B9A9A99944447C07078A1C1CAF8686C1
        C3C3C0C0C0C0C0C00000C0C0C0C0C0C0C9B9A8F261007B08007A0000964343C0
        C0C0C0C0C0C0C0C0AA7D7D7A02027F00007E00008B2121C0C0C0C0C0C0C0C0C0
        0000C0C0C0C0C0C0C6B8ABFC8208E563006F00008B1D1DB39393C0C0C0B9A7A7
        973E3E7B00008000007D0000840E0EC0C0C0C0C0C0C0C0C00000C0C0C0C0C0C0
        C0BDBAD29B5EE361077D08007A0000984A4AC0C0C0A777777A03038000007D00
        00820606A46565C2C5C5C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0BEBEBFDB9D
        55E56205760600984A4BC0C0C0A777777900007D0000840C0CA66C6CBFBBBBC0
        C1C1C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C1C0E2A457D6520D7F
        1826AC7A7A984444770000850D0DA97676C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        0000C0C0C0C0AF9EB7A0A2AB8686B19495B5ADB6A06E747900007F00007C0202
        964343C5CFCFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C6A889E48428
        8A292C7400007300007A00008406008204028000008000008207078C25258E2A
        2A9135359A4C4CB59B9BB69C9CC1C3C30000CC945DFF7800650000840300A829
        00A73B15C19562A14B317B00007F00007B00007600007000007100007F000090
        2C2C912F2FB291910000CC9259FF7B00C24100C74800F9A32AE7A04DBABCB3A7
        7A7C740000974B4FC1B4A7CE691EE45A00C33E006C00007100007B00009E5252
        0000C2B7ACDC8A38ED922FEA983CBAB4B0C0C0C0C2B8A9A152377700008E2727
        BAA5A3CAC2B0DAAC76E6811FC14000B832007E00009F58580000C0C1C2C1B6AC
        C1B2A4C0B3A5C0C0C0C0C0C0C5AB90A239157B00007C02029B4F50C0C0C0C0C0
        C0CAA884F59222F58E1F8C1A19A46B6D0000C0C0C0C0C1C3C0C0C0C0C0C0C0C0
        C0C0C0C0E5AC67D748007200007B0000974747C0C0C0C0C0C0C0C0C0C2A990C1
        A68CB08886BFBDBE0000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C2B1A1E88C29D8
        52007401007E00008E2727B9A4A4C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        0000C0C0C0C0C0C0C0C0C0C0C0C0C0C1C2C0B9B3D7924EFF8C05D050006B0000
        891615B08888C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C2C2C2C0C0C0
        C0C0C0C0C0C0C0C0C0BFC1C1BDBCBADA9148FF8103D4651B9F6E7ABEB9B9C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0CABEB3CD9358C9A480BBBBC3C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BitBtnCreateClick
    end
    object ButtonRemoveSelected: TBitBtn
      Left = 593
      Top = 233
      Width = 189
      Height = 27
      Hint = 
        'Select items in the list that you want to remove, then click her' +
        'e'
      Anchors = [akRight, akBottom]
      Caption = 'Remove selected items from list'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 2
      OnClick = ButtonRemoveSelectedClick
    end
    object BitBtnCancel: TBitBtn
      Left = 167
      Top = 233
      Width = 126
      Height = 27
      Hint = 'Close this page'
      Anchors = [akLeft, akBottom]
      Cancel = True
      Caption = '   Close Window'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        AE060000424DAE06000000000000360000002800000017000000170000000100
        1800000000007806000000000000000000000000000000000000C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6808080000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
        0000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6808080000000000000C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6000000C6C6C6C6C6C6C6C6C6C6C6C680808000000000CECE00
        0000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6C6C6C6C6C6C68080800000
        0000CECE80FFFF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6C6C6C6
        80808000000000CECE80FFFF80FFFF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
        0000C6C6C680808000000000CECE80FFFF80FFFF80FFFF000000C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C600000000000000000000CECE80FFFF80FFFF80FFFF80FFFF00
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000C6C6C6000000C6C6C600000080FFFF80FFFF80FF
        FF80FFFF80FFFF000000408080408080408080408080000000C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000
        80FFFF80FFFF80FFFF80FFFF80FFFF0000004080804080804080804080800000
        00C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
        0000C6C6C600000080FFFF80FFFF80FFFF80FFFF000000000000408080408080
        408080408080000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6000000C6C6C600000080FFFF80FFFF80FFFF80FFFF80FFFF00
        0000408080408080408080408080000000C6C6C6C6C6C6808080000000000000
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C600000080FFFF80FFFF80FF
        FF80FFFF80FFFF00000040808040808040808040808000000080808000000000
        7D00008C00000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000
        80FFFF80FFFF80FFFF80FFFF80FFFF000000408080408080408080356A6A0000
        00007D00008C00008C00008C0000000000000000000000000000000000000000
        0000C6C6C600000080FFFF80FFFF80FFFF80FFFF80FFFF000000408080356A6A
        000000007D00008C00008C00008C00008C00008C00008C00008C00008C00008C
        00008C00000000000000C6C6C600000080FFFF80FFFF80FFFF80FFFF80FFFF00
        0000356A6A00800000A60000A60000A60000A60000A60000A60000A60000A600
        00A60000A60000A60000A600000000000000C6C6C600000080FFFF80FFFF80FF
        FF80FFFF80FFFF000000356A6A00800000B70000B70000B70000B70000B70000
        B70000B70000B70000B70000B70000B70000B700000000000000C6C6C6000000
        80FFFF80FFFF80FFFF80FFFF00CECE000000408080356A6A00000000800000CA
        0000CA0000CA0000CA0000CA0000CA0000CA0000CA0000CA0000CA0000000000
        0000C6C6C600000080FFFF80FFFF80FFFF00CECE000000356A6A408080408080
        408080356A6A00000000800000CA0000CA0000CA000000000000000000000000
        00000000000000000000C6C6C600000080FFFF80FFFF00CECE000000356A6A40
        808040808040808040808040808000000080808000000000800000CA00000000
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C600000080FFFF00CECE0000
        00356A6A408080408080408080408080408080408080000000C6C6C6C6C6C680
        8080000000000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000
        00CECE000000356A6A4080804080804080804080804080804080804080800000
        00C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
        0000C6C6C6000000000000356A6A408080408080408080408080408080408080
        408080408080000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6000000C6C6C600000000000000000000000000000000000000
        0000000000000000000000000000000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000}
      ModalResult = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
end
