object JobPropertiesForm: TJobPropertiesForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Project Properties'
  ClientHeight = 350
  ClientWidth = 569
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object JobNameLabel: TLabel
    Left = 28
    Top = 26
    Width = 64
    Height = 13
    Caption = 'Project Name'
  end
  object NicknameLabel: TLabel
    Left = 28
    Top = 62
    Width = 45
    Height = 13
    Caption = 'Nickname'
  end
  object JobNumberLabel: TLabel
    Left = 28
    Top = 94
    Width = 28
    Height = 13
    Caption = 'Job #'
  end
  object ReelCountLabel: TLabel
    Left = 28
    Top = 132
    Width = 37
    Height = 13
    Caption = '# Reels'
  end
  object MediaLocationLabel: TLabel
    Left = 28
    Top = 216
    Width = 51
    Height = 13
    Caption = 'Media Loc.'
  end
  object JobFoldersLabel: TLabel
    Left = 28
    Top = 179
    Width = 59
    Height = 13
    Caption = 'Job Folders:'
  end
  object MediaFoldersLabel: TLabel
    Left = 28
    Top = 250
    Width = 70
    Height = 13
    Caption = 'Media Folders:'
  end
  object JobNameEdit: TEdit
    Left = 99
    Top = 23
    Width = 313
    Height = 21
    TabOrder = 0
    OnChange = JobNameEditChange
  end
  object NicknameEdit: TEdit
    Left = 99
    Top = 59
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object JobNumberEdit: TEdit
    Left = 99
    Top = 91
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object ReelCountEdit: TEdit
    Left = 99
    Top = 129
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 3
    OnChange = ReelCountEditChange
  end
  object MediaLocationEdit: TEdit
    Left = 99
    Top = 213
    Width = 313
    Height = 21
    TabOrder = 13
    OnChange = MediaLocationEditChange
  end
  object UserMediaFolderNameEdit: TEdit
    Left = 235
    Top = 247
    Width = 177
    Height = 21
    TabOrder = 16
  end
  object ASlashBCheckBox: TCheckBox
    Left = 260
    Top = 131
    Width = 39
    Height = 17
    Caption = 'A/B'
    TabOrder = 4
    OnClick = ASlashBCheckBoxClick
  end
  object CombinedCheckBox: TCheckBox
    Left = 322
    Top = 131
    Width = 66
    Height = 17
    Caption = 'Combined'
    TabOrder = 5
  end
  object LastReelAOnlyCheckBox: TCheckBox
    Left = 404
    Top = 131
    Width = 97
    Height = 17
    Caption = 'Last reel A only'
    TabOrder = 6
  end
  object DrsMediaFolderCheckBox: TCheckBox
    Left = 116
    Top = 249
    Width = 62
    Height = 17
    Caption = 'DRS'
    TabOrder = 14
  end
  object MasksCheckBox: TCheckBox
    Left = 188
    Top = 178
    Width = 62
    Height = 17
    Caption = 'masks'
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
  object PdlsCheckBox: TCheckBox
    Left = 260
    Top = 178
    Width = 62
    Height = 17
    Caption = 'pdls'
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object ReportsCheckBox: TCheckBox
    Left = 332
    Top = 178
    Width = 62
    Height = 17
    Caption = 'reports'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  object CutListsCheckBox: TCheckBox
    Left = 404
    Top = 178
    Width = 62
    Height = 17
    Caption = 'cut_lists'
    Checked = True
    State = cbChecked
    TabOrder = 11
  end
  object StabilizeCheckBox: TCheckBox
    Left = 476
    Top = 178
    Width = 62
    Height = 17
    Caption = 'stabilize'
    Checked = True
    State = cbChecked
    TabOrder = 12
  end
  object DewarpCheckBox: TCheckBox
    Left = 116
    Top = 178
    Width = 62
    Height = 17
    Caption = 'dewarp'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object UserMediaFolderCheckBox: TCheckBox
    Left = 188
    Top = 249
    Width = 49
    Height = 17
    Caption = 'User'
    TabOrder = 15
    OnClick = UserMediaFolderCheckBoxClick
  end
  object OkButton: TButton
    Left = 200
    Top = 302
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 17
  end
  object CancelButton: TButton
    Left = 297
    Top = 302
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 18
  end
  object BrowseForMediaLocationButton: TButton
    Left = 422
    Top = 211
    Width = 27
    Height = 23
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 19
    OnClick = BrowseForMediaLocationButtonClick
  end
  object JobFoldersToggleButton: TButton
    Left = 17
    Top = 174
    Width = 75
    Height = 25
    Caption = 'Job Folders'
    TabOrder = 20
    OnClick = JobFoldersToggleButtonClick
  end
end
