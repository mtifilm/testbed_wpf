//---------------------------------------------------------------------------

#ifndef ClipFileExportFormH
#define ClipFileExportFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MTIBusy.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>

#include "ClipFileExporter.h"
#include "timecode.h"
#include "HRTimer.h"
#include "MTIBusy.h"
#include <queue>
#include <vector>
using std::string;
using std::queue;
//---------------------------------------------------------------------------

class TClipFileExportForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *BottomPanel;
   TLabel *StatusLabel;
   TButton *PauseButton;
   TButton *CancelButton;
   TMTIBusy *MTIBusy1;
   TTimer *ProgressTimer;
   TPanel *TopPanel;
   TPanel *Panel3;
   TLabel *SourceNameLabel;
   TLabel *DestinationNameLabel;
   TLabel *SourceLabel;
   TLabel *DestinationLabel;
   TProgressBar *RenderProgressBar;
   TPanel *Panel2;
   TLabel *ConvertedLabel;
   TLabel *FinishedInLabel;
   void __fastcall ProgressTimerTimer(TObject *Sender);
   void __fastcall CancelButtonClick(TObject *Sender);
   void __fastcall PauseButtonClick(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);

private:	// User declarations
    void Initialize();

public:		// User declarations
   __fastcall TClipFileExportForm(TComponent* Owner);
   __fastcall ~TClipFileExportForm();

    void               Reset();
    void               setExporter(ClipFileExporter *exporter);
    void                Go();
    void                UpdateProgress(bool finished);
    bool                OkToNuke();
    void                Cancel();

    ClipFileExporter    *_theFileExporter;
    string              SourceName;
    string              DestName;
    string              FullSourceName;
    string              FullDestName;
    int                 SourceFrame;
    int                 DestFrame;

    void                *TheRunThread;
    void                *CriticalSection;
    int                 ConvertPhase;
    int                 OldCompletedFrames;
    double              OldTotalElapsedTime;
    double              ElapsedTimeZero;
    double              ProcessingTimePerFrame;
#define FINISHED_IN_UPDATE_INTERVAL 1000.
    double              FinishedInTime;
    double              FinishedInUpdateTime;
#define NUM_HISTORY_ITEMS 20
    double              ElapsedTimeHistory[NUM_HISTORY_ITEMS];
    CHRTimer            HRTimer;

    static void         WorkerThread(void *vpAppData, void *vpReserved);
    void                RunInThread();

    bool                Initializing;
    bool                _exporting;
    bool                Paused;
    bool                AllDone;
    bool                NukeMe;
};
//---------------------------------------------------------------------------
extern PACKAGE TClipFileExportForm *ClipFileExportForm;
//---------------------------------------------------------------------------
#endif
