object ClipNameTemplateForm: TClipNameTemplateForm
  Left = 1963
  Top = 315
  Width = 351
  Height = 257
  Caption = 'Edit Clip Name Template'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    343
    223)
  PixelsPerInch = 96
  TextHeight = 13
  object SelectATemplateGroupBox: TGroupBox
    Left = 20
    Top = 12
    Width = 305
    Height = 165
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Select a template '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      305
      165)
    object NumberOfDigitsLabel: TLabel
      Left = 23
      Top = 126
      Width = 195
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Number of digits in <Sequence Number>'
    end
    object TemplateOption0RadioButton: TRadioButton
      Left = 20
      Top = 20
      Width = 181
      Height = 17
      Caption = ' Clip<Sequence Number>'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object TemplateOption2RadioButton: TRadioButton
      Left = 20
      Top = 68
      Width = 213
      Height = 17
      Caption = '<Folder Name>_<Sequence Number>'
      TabOrder = 1
    end
    object TemplateOption1RadioButton: TRadioButton
      Left = 20
      Top = 44
      Width = 233
      Height = 17
      Caption = '<File Name>_<Sequence Number>'
      TabOrder = 2
    end
    object TemplateOption3RadioButton: TRadioButton
      Left = 20
      Top = 92
      Width = 273
      Height = 17
      Caption = '<Folder Name>_<File Name>_<Sequence Number>'
      TabOrder = 3
    end
    object NumberOfDigitsComboiBox: TComboBox
      Left = 228
      Top = 122
      Width = 41
      Height = 21
      Anchors = [akLeft, akBottom]
      ItemHeight = 13
      ItemIndex = 2
      TabOrder = 4
      Text = '3'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8')
    end
  end
  object OKButton: TButton
    Left = 156
    Top = 188
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 248
    Top = 188
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
