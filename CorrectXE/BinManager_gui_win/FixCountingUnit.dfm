object CountFixesUnit: TCountFixesUnit
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Count Number of Fixes'
  ClientHeight = 123
  ClientWidth = 276
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ScanningStatusLabel: TLabel
    Left = 8
    Top = 8
    Width = 61
    Height = 13
    Caption = 'Scanning clip'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object NumberOfFixesCaptionLabel: TLabel
    Left = 8
    Top = 46
    Width = 80
    Height = 13
    Caption = 'Number of fixes:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object NumberOfFixesValueLabel: TLabel
    Left = 105
    Top = 46
    Width = 50
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '000000'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ScanningProgressBar: TProgressBar
    Left = 8
    Top = 27
    Width = 259
    Height = 9
    TabOrder = 0
  end
  object Button1: TButton
    Left = 100
    Top = 90
    Width = 75
    Height = 25
    Caption = 'OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 246
    Top = 70
  end
end
