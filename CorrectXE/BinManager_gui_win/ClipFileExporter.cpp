//---------------------------------------------------------------------------
#include "ClipFileExporter.h"

#include "BinManager.h"
#include "ClipAPI.h"
#include "FileSweeper.h"             // For getLastSystemErrorMessage()
#include "ImageFileMediaAccess.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
#include "ShowModalDialog.h"
//---------------------------------------------------------------------------

ClipFileExporter::ClipFileExporter()
{
   ClipIdentifier clipId;
   SetSourceClipId(clipId);
}
//---------------------------------------------------------------------------

ClipFileExporter::~ClipFileExporter()
{
   MTIassert(OkToNuke());
}
//---------------------------------------------------------------------------

void ClipFileExporter::SetSourceClipId(ClipIdentifier clipId)
{
   _clipId = clipId;
   _exportPhase = IDLE_PHASE;

   _inFrameIndex = -1;
   _outFrameIndex = -1;
   _targetDirectory = "";
   _completionCode = 0;
   _completedFrames = 0;
   _totalFrames = 0;
   _pauseFlag = false;
   _cancelFlag = false;
   _errorMessage = "";
}
//---------------------------------------------------------------------------

void ClipFileExporter::SetFrameRange(int inFrameIndex, int outFrameIndex)
{
   _inFrameIndex = inFrameIndex;
   _outFrameIndex = outFrameIndex;
}
//---------------------------------------------------------------------------

void ClipFileExporter::SetTargetDirectory(const string &suggestedTargetDirectory)
{
   _targetDirectory = suggestedTargetDirectory;
}
//---------------------------------------------------------------------------

ClipIdentifier ClipFileExporter::GetSourceClipId() const
{
   return _clipId;
}
//---------------------------------------------------------------------------

void ClipFileExporter::GetFrameRange(int &inFrameIndex, int &outFrameIndex) const
{
   inFrameIndex = _inFrameIndex;
   outFrameIndex = _outFrameIndex;
}
//---------------------------------------------------------------------------

string ClipFileExporter::GetTargetDirectory() const
{
   return _targetDirectory;
}
//---------------------------------------------------------------------------

int ClipFileExporter::ExportFiles()
{
   _completionCode = 0;
   _completedFrames = 0;
   _totalFrames = 0;
   _pauseFlag = false;
   _cancelFlag = false;
   _errorMessage = "Internal error";

   MTIassert(!_clipId.IsEmpty());
   if (_clipId.IsEmpty())
   {
      _completionCode = -1;
      return _completionCode;
   }

   MTIassert(!_targetDirectory.empty());
   if (_targetDirectory.empty())
   {
      _completionCode = -2;
      return _completionCode;
   }

   int retVal = 0;
   CBinManager binMgr;
   auto sourceClip = binMgr.openClip(_clipId, &retVal);
   if (retVal)
   {
      MTIostringstream os;
      os << "Can't open clip " << _clipId.GetClipName();
      _errorMessage = os.str();
      return retVal;
   }

   CVideoFrameList* videoFrameList = sourceClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_FILM);
   if (videoFrameList == nullptr)
   {
      MTIostringstream os;
      os << "Can't get frame list for " << _clipId.GetClipName();
      _errorMessage = os.str();
      _exportPhase = ABORTED_PHASE;
      _completionCode = -3;
      return _completionCode;       // Can't happen
   }

   _inFrameIndex = std::max<int>(_inFrameIndex, 0);

   int clipTotalFrameCount =  videoFrameList->getTotalFrameCount();
   MTIassert(_inFrameIndex < clipTotalFrameCount);
   if (_inFrameIndex >= clipTotalFrameCount)
   {
      _exportPhase = ABORTED_PHASE;
      _completionCode = -4;
      return _completionCode;
   }

   _outFrameIndex = (_outFrameIndex < 0) ? clipTotalFrameCount : std::min<int>(clipTotalFrameCount, _outFrameIndex);
   _totalFrames = _outFrameIndex - _inFrameIndex;

   ExportPhase exitPhase = COMPLETED_PHASE;

   for (int frameIndex = _inFrameIndex; frameIndex < _outFrameIndex; ++frameIndex)
   {
      while (_pauseFlag & !_cancelFlag)
      {
         _exportPhase = PAUSED_PHASE;
         MTImillisleep(100);
      }

      if (_cancelFlag)
      {
         _completionCode = -8;
         _errorMessage = "Copy was cancelled";
         exitPhase = CANCELED_PHASE;
         break;
      }

      _exportPhase = EXPORTING_PHASE;

      CVideoFrame *frame = videoFrameList->getFrame(frameIndex);
      CVideoField *field = frame ? frame->getField(0) : nullptr;
      if (frame == nullptr || field == nullptr)
      {
         exitPhase = ABORTED_PHASE;
         _errorMessage = "Source clip is corrupted";
         _completionCode = -5;
         break;
      }

      CMediaLocation mediaLocation = field->getMediaLocation();
      MTIassert(mediaLocation.isMediaTypeImageFile());
      if (!mediaLocation.isMediaTypeImageFile())
      {
         _errorMessage = "Source clip is wrong type";
         exitPhase = ABORTED_PHASE;
         _completionCode = -6;
         break;
      }

      CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
      string sourceFilename = mediaAccess->getImageFileName(mediaLocation);
      string targetFilename = AddDirSeparator(_targetDirectory) + GetFileNameWithExt(sourceFilename);

      BOOL CopyFileCancelFlag = false;
      DWORD copyFlags = COPY_FILE_FAIL_IF_EXISTS; // + COPY_FILE_NO_BUFFERING;
      bool success = ::CopyFileEx(sourceFilename.c_str(), targetFilename.c_str(), nullptr, nullptr, &CopyFileCancelFlag, copyFlags);
      if (!success)
      {
         string systemMessage = GetLastSystemErrorMessage();
         MTIostringstream os;
         os << "Could not copy file " << GetFileNameWithExt(sourceFilename) << endl
            << "Reason: " << systemMessage << endl
            << "Export operation aborted.";
         _errorMessage = os.str();

         exitPhase = ABORTED_PHASE;
         _completionCode = -7;
         break;
      }

      ++_completedFrames;
   }

   binMgr.closeClip(sourceClip);
   _exportPhase = exitPhase;

    return _completionCode;
}
//---------------------------------------------------------------------------

ClipFileExporter::ExportPhase ClipFileExporter::GetProgress(int &completedFrames, int &totalFrames) const
{
   completedFrames = _completedFrames;
   totalFrames = _totalFrames;
   return _exportPhase;
}
//---------------------------------------------------------------------------

int ClipFileExporter::GetCompletionCode() const
{
   return _completionCode;
}
//---------------------------------------------------------------------------

void ClipFileExporter::Pause()
{
   _pauseFlag = true;
}
//---------------------------------------------------------------------------

void ClipFileExporter::Resume()
{
   _pauseFlag = false;
}
//---------------------------------------------------------------------------

void ClipFileExporter::Cancel()
{
   _cancelFlag = true;
}
//---------------------------------------------------------------------------

bool ClipFileExporter::OkToNuke() const
{
   return _exportPhase != EXPORTING_PHASE;
}
//---------------------------------------------------------------------------

string ClipFileExporter::GetErrorMessage() const
{
   return _errorMessage;
}
//---------------------------------------------------------------------------


namespace { // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//---------------------------------------------------------------------------

bool checkNotSameMediaAsClip(const string &clipName,
                             const string &fileTemplate,
                             int &errorCode)
{
   CBinManager binMgr;
   string strBin, strClip;
   errorCode = binMgr.splitClipFileName(clipName, strBin, strClip);
   if (errorCode != 0)
   {
      return false;
   }

   auto sourceClip = binMgr.openClip(strBin, strClip, &errorCode);
   if (errorCode != 0)
   {
      return false;
   }

   CVideoFrameList *vflp;
   vflp = sourceClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_FILM);
   if (vflp == 0)
   {
      errorCode = -1; // "can't happen"
      binMgr.closeClip(sourceClip);
      return false;
   }

   int firstFrame = 0;
   int lastFrame = vflp->getTotalFrameCount() - 1;
   for (int frameIndex = firstFrame; frameIndex <= lastFrame; frameIndex++)
   {
      CVideoFrame *frame = vflp->getFrame(frameIndex);
      CVideoField *field = frame ? frame->getField(0) : nullptr;
      if (frame == nullptr || field == nullptr)
      {
         errorCode = -1;  // "can't happen"
         binMgr.closeClip(sourceClip);
         return false;
      }

      CMediaLocation mediaLocation = field->getMediaLocation();
      if (!mediaLocation.isMediaTypeImageFile())
      {
         binMgr.closeClip(sourceClip);
         return true;
      }

      CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
      string imageFileName = mediaAccess->getImageFileName(mediaLocation);
      string imageTemplate = CImageFileMediaAccess::MakeTemplate(imageFileName, false);
      if (imageTemplate == fileTemplate)
      {
         binMgr.closeClip(sourceClip);
         return false;
      }
   }

   binMgr.closeClip(sourceClip);
   return true;
}
//---------------------------------------------------------------------------

int ensureNoFilesExist(const string &fileTemplate,
		       int firstFrame, int lastFrame)
{
   int retVal = 0; CClipInitInfo clipInitInfo;

   retVal = clipInitInfo.checkNoImageFiles2(fileTemplate, firstFrame, lastFrame);

   if (retVal != 0)
   {
      retVal = _MTIConfirmationDialog("One or more existing files will be overwritten\n" "Do you wish to continue?\n");
      if (retVal != MTI_DLG_OK)
      {
         return 1;
      }

      for (int frameNumber = firstFrame; frameNumber <= lastFrame; ++frameNumber)
      {
         // Make next image file name
         string fileName = CImageFileMediaAccess::GenerateFileName2(fileTemplate, frameNumber);

         // Check if next image file exists
         if (DoesFileExist(fileName))
         {
            // Delete the file
            int retVal = DeleteFile(fileName.c_str());

            if (retVal == 0)
            {
               TRACE_0(errout << "ERROR: ensureNoFilesExist():" << "DeleteFile FAILED:" << endl << "       File:      " << fileName <<
               endl << "       Error was: " << GetLastSystemErrorMessage()); return 2; // qqq
            }
         }
      }
   }

   // No files found, or they were all successfully deleted
   return 0;
}
//---------------------------------------------------------------------------

} // end local namespace ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//---------------------------------------------------------------------------
