//---------------------------------------------------------------------------

#ifndef UnitPrefH
#define UnitPrefH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include "MTIUNIT.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormPref : public TMTIForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl1;
        TTabSheet *TabSheetFileCreation;
        TTabSheet *TabSheetTabOrder;
        TGroupBox *GroupBoxVideo1;
        TCheckBox *CheckBoxTabVideoIn1;
        TCheckBox *CheckBoxTabVideoOut1;
        TCheckBox *CheckBoxTabVideoDur1;
        TCheckBox *CheckBoxTabVideoName1;
        TCheckBox *CheckBoxTabVideoCreate1;
        TGroupBox *GroupBoxVideo2;
        TCheckBox *CheckBoxTabVideoCen2;
        TCheckBox *CheckBoxTabVideoDur2;
        TCheckBox *CheckBoxTabVideoName2;
        TCheckBox *CheckBoxTabVideoCreate2;
        TGroupBox *GroupBoxFile;
        TCheckBox *CheckBoxTabFileIn1;
        TCheckBox *CheckBoxTabFileOut1;
        TCheckBox *CheckBoxTabFileDur1;
        TCheckBox *CheckBoxTabFileName1;
        TCheckBox *CheckBoxTabFileCreate1;
        TBitBtn *BitBtnOK;
        TBitBtn *BitBtnCancel;
    TCheckBox *ShowMultipleDirectoriesFormCheckBox;
        TRadioGroup *RadioGroup1;
        TRadioButton *BeginningRadioButton;
        TRadioButton *EndRadioButton;
        TTabSheet *TeranexPage;
        TLabel *IPAddrLabel;
        TEdit *IPAddrEdit;
        void __fastcall BitBtnOKClick(TObject *Sender);
        void __fastcall BitBtnCancelClick(TObject *Sender);
private:	// User declarations
  int iSearchLocation;
public:		// User declarations
        __fastcall TFormPref(TComponent* Owner);
   virtual bool WriteSettings(CIniFile *ini, const string &IniSection);
   virtual bool ReadSettings(CIniFile *ini, const string &IniSection);

    virtual bool WriteSAP(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
    virtual bool ReadSAP(CIniFile *ini, const string &IniSection);   // Reads configuration.

  bool TabVideoIn1();
  bool TabVideoOut1();
  bool TabVideoDur1();
  bool TabVideoName1();
  bool TabVideoCreate1();

  bool TabVideoCen2();
  bool TabVideoDur2();
  bool TabVideoName2();
  bool TabVideoCreate2();

  bool TabFileIn1();
  bool TabFileOut1();
  bool TabFileDur1();
  bool TabFileName1();
  bool TabFileCreate1();

  bool ShowMultipleDirectoriesForm();
  int GetMultipleDirectorySearchLocation();
  void SetShowMultipleDirectoriesForm(bool bChecked);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPref *FormPref;
//---------------------------------------------------------------------------
#endif
