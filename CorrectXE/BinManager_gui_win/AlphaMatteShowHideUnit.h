//---------------------------------------------------------------------------

#ifndef AlphaMatteShowHideUnitH
#define AlphaMatteShowHideUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "IniFile.h"         // for StringList
#include "MediaInterface.h"  // for Media Hack Commands
//---------------------------------------------------------------------------
class TAlphaMatteShowHideForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *SetupPanel;
        TRadioButton *ShowAlphaMatteRadioButton;
        TRadioButton *HideAlphaMatteRadioButton;
        TRadioButton *DestroyAlphaMatteRadioButton;
        TLabel *Continuation1;
        TPanel *Separator1;
        TButton *SetupPanelOkButton;
        TButton *SetupPanelCancelButton;
        TLabel *Continuation2;
        TPanel *ProgressPanel;
        TLabel *ClipNameLabel;
        TLabel *ClipNameValue;
        TLabel *ModifyingModeLabel;
        TLabel *FrameLabel;
        TLabel *FrameValue;
        TLabel *OfLabel;
        TLabel *OfValue;
        TProgressBar *ClipListProgressBar;
        TProgressBar *FrameIndexProgressBar;
        TPanel *Separator2;
        TButton *ProgressPanelCloseButton;
        TTimer *ProgressTimer;
        TLabel *StatusLabel;
        void __fastcall SetupPanelOkButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ProgressPanelCloseButtonClick(
          TObject *Sender);
        void __fastcall ProgressTimerTimer(TObject *Sender);
private:	// User declarations
        string mBinPath;
        StringList mClipList;
        void *mProcessingThreadId;

        static void ProcessingThread(void *aThreadArg, void *aThreadHandle);
        void HackAllClips();
        void HackOneClip(string aClipName);

        // Processing Thread communications
        // TO thread
        EMediaHackCommand mHackCommand;
        bool mCancelledFlag;
        // FROM thread
        AnsiString mClipName;
        unsigned int mClipIndex;
        unsigned int mTotalClipCount;
        unsigned int mFrameIndex;
        unsigned int mTotalFrameCount;
        bool mProcessingIsCompleteFlag;
        //
public:		// User declarations
        __fastcall TAlphaMatteShowHideForm(TComponent* Owner);
        void Reset();
        void SetBinPath(string aBinPath);
        void AddClip(string aClipName);
        enum EDefaultSelection
        {
            SELECT_HIDE,
            SELECT_SHOW
        };
        void SetGuiDefaultSelection(EDefaultSelection defaultSelection);
};
//---------------------------------------------------------------------------
extern PACKAGE TAlphaMatteShowHideForm *AlphaMatteShowHideForm;
//---------------------------------------------------------------------------
#endif
