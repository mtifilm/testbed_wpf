//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DeleteBinUnit.h"

#include "BinDir.h"
#include "BinManagerGUIUnit.h"
#include "BinMgrGUIInterface.h"
#include "ClipAPI.h"
#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDeleteBinDialog *DeleteBinDialog;
//---------------------------------------------------------------------------

// mlm: to-do: I think a using declaration should work here, but I
// haven't gotten it to work.
typedef CBinMgrGUIInterface::lockID_t lockID_t;

__fastcall TDeleteBinDialog::TDeleteBinDialog(TComponent* Owner)
   : TForm(Owner)
{
  iModeDeleteOrArchive = MODE_DELETE;  // Default
}
//---------------------------------------------------------------------------
void __fastcall TDeleteBinDialog::CancelDeleteClick(TObject *Sender)
{
   userCancelRequest = true;
}

//---------------------------------------------------------------------------
void __fastcall TDeleteBinDialog::FormShow(TObject *Sender)
{
   ProgressBar->Position = ProgressBar->Min;

   userCancelRequest = false;

   PathNameLabel->Caption = "";
   FileNameLabel->Caption = "";

   if (iModeDeleteOrArchive == MODE_DELETE)
     DeleteBinDialog->Caption = "Delete Bin";
   else if (iModeDeleteOrArchive == MODE_ARCHIVE)
     DeleteBinDialog->Caption = "Archive Bin";

   PostMessage(Handle, CW_START_FORM, 0, 0);
}

void __fastcall TDeleteBinDialog::CMStartForm(TMessage &Message)
{
   string reason;
   lockID_t lockID;

   CBinMgrGUIInterface *binMgrGUIInterface = BinManagerForm->binMgrGUIInterface;

   currentBinPath.erase();
   currentTarget.erase();

   if (iModeDeleteOrArchive == MODE_DELETE)
     PathNameLabel->Caption = "Preparing to delete bin...";
   else if (iModeDeleteOrArchive == MODE_ARCHIVE)
     PathNameLabel->Caption = "Preparing to archive bin...";

   FileNameLabel->Caption = "";

   // Count the number of directories and files that have to be deleted/archived.
   // This is the maximum value for the progress bar
   int currentCount = countContents(targetBinPath);
   TRACE_3(errout << currentCount << " clips, files and directories to be deleted/archived");
   if (currentCount < 1)
      {
      // Nothing to do
      if (binMgrGUIInterface != 0)
         {
         string reason;
         int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
         if (retVal != 0)
            {
            _MTIErrorDialog(Handle, reason);
            return;
            }
         }

      return;
      }

   ProgressBar->Max = currentCount;

   deletedCount = 0;
   archiveSkipCount = 0;

   done = false;
   success = true;

   // Start timer that updates this dialog box
   UpdateTimer->Enabled = true;

   // Delete the bin and its contents
   int retval = deleteOrArchiveBin(targetBinPath, targetRootBin);
   success = (retval == 0) ? true : false;

   if (binMgrGUIInterface != 0)
      {
      if (success == false)
         {
         string reason;
         int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
         if (retVal != 0)
            {
            _MTIErrorDialog(Handle, reason);
            done = true;
            return;
            }
         }
      }

   // done flag is monitored by the UpdateTimer
   done = true;
}

//---------------------------------------------------------------------------

void TDeleteBinDialog::SetDeleteTarget(const string &binPath,
                                       const string& rootBin)
{
   targetBinPath = binPath;
   targetRootBin = rootBin;
}

void TDeleteBinDialog::SetModeDelete(void)
{
  iModeDeleteOrArchive = MODE_DELETE;
}

void TDeleteBinDialog::SetModeArchive(void)
{
  iModeDeleteOrArchive = MODE_ARCHIVE;
}

//---------------------------------------------------------------------------

int TDeleteBinDialog::countContents(const string &binPath)
{
   // Count the directories and files, recurse to descend directory tree

   // mlm: to-do: maybe RequestBinOpenReader() here?

   CBinDir binDir;
   int count = 0;

   CheckUserCancel();

   // Make sure there is a slash at the end of the bin path
   string path = AddDirSeparator(binPath);

   // Count the clips in this bin
   int clipCount = binDir.countAll(path,
									  BIN_SEARCH_FILTER_CLIP);
   count += clipCount;

   // Count the plain files in this Bin
   if (iModeDeleteOrArchive == MODE_DELETE) {
     int fileCount = binDir.countAll(path,
                                 BIN_SEARCH_FILTER_FILE|BIN_SEARCH_FILTER_HIDDEN);
     count += fileCount;
   }

   // Recurse down into subdirectories.  The subdirectories could be sub-bins
   // or deleted clips.
   StringList dirList;
   int dirCount = binDir.findAll(binPath,
                          BIN_SEARCH_FILTER_ANY_DIR | BIN_SEARCH_FILTER_HIDDEN,
                                 dirList);

   if (iModeDeleteOrArchive == MODE_DELETE)
     count += dirCount;

   for (int i = 0; i < dirCount; ++i)
      {
      string dirName = path + dirList[i];
      int contentsCount = countContents(dirName);
      if (contentsCount > 0)
         count += contentsCount;
      }

   return count;
}

//---------------------------------------------------------------------------

int TDeleteBinDialog::deleteOrArchiveBin(const string &binPath, const string &rootPath)
{
   int retVal;
   CBinManager binMgr;

   TRACE_3(errout << "TDeleteBinDialog::deleteBin: " << binPath);

   currentBinPath = binPath;

   if (CheckUserCancel())
      return -1;

   if (iModeDeleteOrArchive == MODE_DELETE) {
     // Start by deleting the bin's contents
     retVal = deleteBinContents(binPath, rootPath);
     if (retVal != 0)
        return retVal;

	  // Delete the bin directory if it is empty and it is not the root bin
	  CBinDir binDir;
	  if (binDir.isBinEmpty(binPath) && binPath != rootPath)
        {
        TRACE_3(errout << "TDeleteBinDialog::deleteOrArchiveBin calling CBinManager::removeDirectory("
                       << binPath << ")");

        retVal = binMgr.removeDirectory(binPath);
        if (retVal != 0)
           return retVal;

        ++deletedCount;
        }
     else
        {
        // signal to the caller that the directory was not removed
        return -1;
        }
   }
   else if (iModeDeleteOrArchive == MODE_ARCHIVE) {
     // Archiving the bin's contents
     retVal = archiveBinContents(binPath, rootPath);
     if (retVal != 0)
        return retVal;
   }
   else {
     TRACE_0(errout << "Internal Error - Unknown Mode: " << iModeDeleteOrArchive
             << endl);
     return -1;
   }

   return 0;
}

int TDeleteBinDialog::deleteBinContents(const string &binPath,
                                        const string &rootPath)
{
   CBinDir binDir;
   CBinManager binMgr;
   int retVal=0, i;
   MTIostringstream ostr;

   // CBinMgrGUIInterface *binMgrGUIInterface = BinManagerForm->binMgrGUIInterface;

   TRACE_3(errout << "TDeleteBinDialog::deleteBinContents " << binPath);

   // Make sure there is a slash at the end of the bin path
   string path = AddDirSeparator(binPath);

	// Make a list of all of the clips in this bin
	StringList clipNameList;
	int clipCount = binDir.findAll(path, BIN_SEARCH_FILTER_CLIP, clipNameList);

   // Iterate over the list of clips in the bin and delete each one
   for (i = 0; i < clipCount; ++i)
      {
      string clipName = clipNameList[i];
      currentTarget = clipName;
      if (CheckUserCancel())
         return -1;

      // mlm: to-do: don't request a clip delete here because
      // TBinManagerForm::DeleteClipOrClipIni does it

      // string reason;
      // lockID_t lockID;

      // if (binMgrGUIInterface != 0)
      //    {
      //    lockID = binMgrGUIInterface->RequestClipDelete(
      //       binPath, clipName, reason);
      //    if (binMgrGUIInterface->IsLockValid(lockID) == false)
      //       {
      //       _MTIErrorDialog(Handle, reason);
      //       return false;
      //       }
      //    }

      // Delete the clip.  This just renames the clip's file and
      // directory.  These will be deleted when we delete any subdirectories
      // in the target Bin
      retVal = BinManagerForm->DeleteOneClip(binPath, clipName);
      if (retVal != 0)
         {
         ostr.str("");
         ostr << "Unable to delete clip " << clipName << endl
              << "from Bin "  << binPath << endl
              << "Error Code = " << retVal << endl
              << "Do you wish to continue with Delete Bin?";
         int userChoice = _MTIWarningDialog(Handle, ostr.str());

         // if (binMgrGUIInterface != 0)
         //    {
         //    string reason;
         //    int retVal = binMgrGUIInterface->FinishLock(lockID, reason);
         //    if (retVal != 0)
         //       {
         //       _MTIErrorDialog(Handle, reason);
         //       return -1;
         //       }
         //    }

         if (userChoice != MTI_DLG_OK)
            return -1;   // User does not want to continue
         }

      ++deletedCount;

      // if (binMgrGUIInterface != 0)
      //    {
      //    string reason;
      //    int retVal = binMgrGUIInterface->FinishClipDelete(
      //       lockID, binPath, clipName, reason);
      //    if (retVal != 0)
      //       {
      //       _MTIErrorDialog(Handle, reason);
      //       return -1;
      //       }
      //    }
      }

   // Delete the plain files in this Bin
   StringList fileNameList;
   int fileCount = binDir.findAll(path,
                               BIN_SEARCH_FILTER_FILE|BIN_SEARCH_FILTER_HIDDEN,
                                  fileNameList);
   for (i = 0; i < fileCount; ++i)
      {
      string fileName = path + fileNameList[i];
      currentTarget = fileNameList[i];

      TRACE_3(errout << "TDeleteBinDialog::deleteBinContents: calling CBinManger::removeFile("
                     << fileName << ")");

      if (CheckUserCancel())
         return -1;

      retVal = binMgr.removeFile(fileName);
      if(retVal != 0)
         {
         // File delete failed
         MTIostringstream ostr;
         ostr << "Unable to delete file " << fileName << endl
              << "Do you wish to continue with Delete Bin?";
         if (_MTIWarningDialog(Handle, ostr.str()) != MTI_DLG_OK)
            return -1;   // User does not want to continue
         }

      ++deletedCount;
      }

   // Recurse down into subdirectories.  The subdirectories could be sub-bins
   // or deleted clips.
   StringList dirList;
   int dirCount = binDir.findAll(binPath,
                          BIN_SEARCH_FILTER_ANY_DIR | BIN_SEARCH_FILTER_HIDDEN,
                                 dirList);
   for (int i = 0; i < dirCount; ++i)
      {
      string dirName = path + dirList[i];

      retVal = deleteOrArchiveBin(dirName, rootPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

int TDeleteBinDialog::archiveBinContents(const string &binPath,
                                         const string &rootPath)
{
#if 0 // Archive stuff was nuked
   CBinDir binDir;
   int retVal=0, i;
   MTIostringstream ostr;

   //CBinMgrGUIInterface *binMgrGUIInterface = BinManagerForm->binMgrGUIInterface;

   TRACE_3(errout << "TDeleteBinDialog::archiveBinContents " << binPath);

   // Make sure there is a slash at the end of the bin path
   string path = AddDirSeparator(binPath);

   // Make a list of all of the clips in this bin
	StringList clipNameList;
	int clipCount = binDir.findAll(path, BIN_SEARCH_FILTER_CLIP, clipNameList);

   // Iterate over the list of clips in the bin and archive each one
   for (i = 0; i < clipCount; ++i)
      {
      string clipName = clipNameList[i];
      currentTarget = clipName;
      if (CheckUserCancel())
         return -1;

      // Skip Archive if already archived
      CBinManager    binMgr;
      CClipInitInfo *clipInitInfo;
      clipInitInfo = binMgr.openClipOrClipIni(binPath, clipName, &retVal);

      if (retVal != 0)
         {
         MTIostringstream ostr;
         ostr << "Error checking status (during open) for " << clipName
              << " (" << retVal << ")";
         _MTIErrorDialog(Handle, ostr.str());
         return -1;
         }

      int iClipStatus = clipInitInfo->getClipStatus();
#ifdef USER_CLIP_REF_COUNTING
      binMgr.closeClip(clipSrc);
#endif

      if (iClipStatus != MEDIA_STATUS_ARCHIVED)
         {
         // Archive the clip.
         retVal = BinManagerForm->ArchiveClip(binPath, clipName);
         if (retVal == 0)
           ++deletedCount;
         else
            {
            ostr.str("");
            ostr << "Unable to archive: " << clipName << endl
                 << "from Bin "  << binPath << endl
                 << "Error Code = " << retVal << endl
                 << "Do you wish to continue with Archive Bin?";
            int userChoice = _MTIWarningDialog(Handle, ostr.str());

            if (userChoice != MTI_DLG_OK)
               return -1;   // User does not want to continue
            }
         }
      else
        ++archiveSkipCount;

   }

   // Recurse down into subdirectories.  The subdirectories could be sub-bins
   // or deleted clips.
   StringList dirList;
   int dirCount = binDir.findAll(binPath,
                          BIN_SEARCH_FILTER_ANY_DIR | BIN_SEARCH_FILTER_HIDDEN,
                                 dirList);
   for (int i = 0; i < dirCount; ++i)
      {
      string dirName = path + dirList[i];

      retVal = deleteOrArchiveBin(dirName, rootPath);
      if (retVal != 0)
         return retVal;
      }
#endif // 0
   return 0;
}

bool TDeleteBinDialog::CheckUserCancel()
{
   Application->ProcessMessages();

   return userCancelRequest;
}

void __fastcall TDeleteBinDialog::UpdateTimerTimer(TObject *Sender)
{
   PathNameLabel->Caption = currentBinPath.c_str();
   FileNameLabel->Caption = currentTarget.c_str();

   TRACE_3(errout << "Deleted Count = " << deletedCount);
   TRACE_3(errout << "Archive Skip Count = " << archiveSkipCount);

   // Set the position of the progress bar based on the number of
   // files & directories that have been deleted
   if (deletedCount <= ProgressBar->Max)
      ProgressBar->Position = deletedCount;

   if (done)
      {
      // Last time
      UpdateTimer->Enabled = false;

      // Close Delete modal dialog box
      ModalResult = (success == true) ? mrOk : mrCancel;
      }

}

int TDeleteBinDialog::GetDeletedOrArchivedCount()
{
  return deletedCount;
}

int TDeleteBinDialog::GetArchiveSkipCount()
{
  return archiveSkipCount;
}
//---------------------------------------------------------------------------

