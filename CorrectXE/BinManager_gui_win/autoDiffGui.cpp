//---------------------------------------------------------------------------
//Kea Johnston, Aug 13, 2004 AutoDiffGuiForm
//What: a GUI for the autodiff defined in autodiff.cpp/h
//Why: So the user can specify options like threshold, and to give the user more control
//      over whether to interpret existing diff stats or to create new ones, as diff stats are
//      created automatically on clips recorded in with the vtr emulator.
//How: only 2 non-handler methods:
//      setCurClip(string str) = is a simple mutator which allows the user to set
//      the name/path of the current clip. This should be called when the class is instantiated.
//      or once before anything is called on the autodiff class.
//      Autodiff should throw errors if NULL is passed in, but it may also crash, so don't pass in null.
//      resetAndMessage(string str) occurs whenever there is an error in autodiff or the gui itself. It displays an error
//      and resets the combo box, variables, radio buttons, etc. It requires a string error message be passed in.
//A Note on thresholds:
//      Wasn't quite sure what would be a reasonable range for thresholds. Figured that the user would want more control
//      towards the more sensitive ranges, so I decided to use a power of two from .1 to 1.0 (With the 128 being rounded to 1.0
//      As a result, there is less fine-tuning ability when it comes to less sensitive thresholds.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "AutoDiff.h"
#include "autoDiffGui.h"
#include "ClipAPI.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAutoDiffForm *AutoDiffForm;
//---------------------------------------------------------------------------

__fastcall TAutoDiffForm::TAutoDiffForm(TComponent* Owner)
    : TForm(Owner), autoDiff(NULL), guiState(IDLE)
{
    // set default sensitivity - need to keep this in an ini file QQQ
    SensitivityTrackBar->Position = 3;  // setting 4 = threshold 0.02 QQQ

    resetInterface();
}
//---------------------------------------------------------------------------

__fastcall TAutoDiffForm::~TAutoDiffForm()
{
    delete autoDiff;
}
//---------------------------------------------------------------------------

void TAutoDiffForm::resetInterface()
{
    RegenDiffsRadioButton->Enabled = false;
    RegenDiffsRadioButtonLabel->Enabled = false;
    DetectOnlyRadioButton->Enabled = false;
    DetectOnlyRadioButtonLabel->Enabled = false;
    //DetectOnlyRadioButton->Checked = true;
    SensitivityTrackBar->Enabled = false;

    // initialize other stuff
    //diffProgressLabel->Caption = "";
    //diffProgressBar->Position = 0;
    goButton->Enabled = false;
    closeButton->Caption = "Cancel";
    closeButton->Enabled = true;
    ProgressTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void TAutoDiffForm::setCurClip(const string &name)
{
   clp_filename = name;
   if (name == "")
   {
      resetInterface();
   }
   else
   {
      // Check if the .diff file exists and set radio buttons accordingly
      CBinManager binMgr;
      string diffFileName = binMgr.makeClipFileName(clp_filename) + ".diff";

      RegenDiffsRadioButton->Enabled = true;
      RegenDiffsRadioButtonLabel->Enabled = true;

      if (DoesFileExist(diffFileName))
      {
         // The diff file exists, so can either regenerate the file or
         // use the existing diff file to detect the cuts
         DetectOnlyRadioButton->Enabled = true;
         DetectOnlyRadioButtonLabel->Enabled = true;
         DetectOnlyRadioButton->Checked = true;
      }
      else
      {
         // The diff file does not exist, so the only option should
         // be to regenerate the diff file
         RegenDiffsRadioButton->Checked = true;
      }
      SensitivityTrackBar->Enabled = true;
      goButton->Enabled = true;
   }
   guiState = IDLE;
}
//---------------------------------------------------------------------------

void __fastcall TAutoDiffForm::goButtonClick(TObject *Sender)
{
    int retVal;
    resetInterface();
    guiState = IDLE;
    DetectOnlyRadioButtonLabel->Enabled = true;
    RegenDiffsRadioButtonLabel->Enabled = true;
    diffProgressLabel->Caption="Initializing...";
    diffProgressBar->Position = 0;

    // Create new autodiffer if first time through here
    if (autoDiff == NULL)
    {
       autoDiff = new CAutoDiff(clp_filename);
       if (autoDiff == NULL)
       {
          closeButton->Caption = "Close";
          _MTIErrorDialog(Handle, "Out of memory    ");
          diffProgressLabel->Caption="Error: OUT OF MEMORY; cannot proceed";
          return;
       }
    }

    // Set up sutodiffer operation
    retVal = autoDiff->init();
    if (retVal != 0)
    {
       closeButton->Caption = "Close";
       _MTIErrorDialog(Handle, "Initialization failed    ");
       diffProgressLabel->Caption = theError.getFmtError().c_str();
       return;
    }

    // Tell the autodiffer to either generate a new diffs file or update
    // cuts using an existing one, depending on radio button state
    if (RegenDiffsRadioButton->Checked)
    {
       retVal = autoDiff->createDiffsFile();
       if (retVal != 0)
       {
          closeButton->Caption = "Close";
          _MTIErrorDialog(Handle, "Failed to create diffs file    ");
          diffProgressLabel->Caption = theError.getFmtError().c_str();
          return;
       }
       guiState = GENERATING_DIFFS_FILE;
    }
    else
    {
       retVal = autoDiff->updateCuts(getThreshold());
       if (retVal != 0)
       {
          closeButton->Caption = "Close";
          _MTIErrorDialog(Handle, "Failed to update cuts    ");
          diffProgressLabel->Caption = theError.getFmtError().c_str();
          return;
       }
       guiState = UPDATING_CUTS;
    }
    ProgressTimer->Enabled = true;
}
//---------------------------------------------------------------------------

float TAutoDiffForm::getThreshold()
{
    // QQQ WANTON USE OF MAGIC NUMBERS
    float threshold = 0.02;

    switch (SensitivityTrackBar->Position) {
      case 1:
        threshold = 0.08;    // was .20
        break;
      case 2:
        threshold = 0.04;    // was .10
        break;
      case 3:
        threshold = 0.02;    // was .04
         break;
      case 4:
        threshold = 0.01;    // was .02
        break;
      case 5:
        threshold = 0.005;   // was .01
        break;
    }

    return threshold;
}
//---------------------------------------------------------------------------


void __fastcall TAutoDiffForm::ProgressTimerTimer(TObject *Sender)
{
   MTIostringstream os;

   if (guiState == IDLE || autoDiff == NULL)
   {
      ProgressTimer->Enabled = false;
      return;
   }
   if (autoDiff->isDone())
   {
      switch (autoDiff->getProcessingStatus())
      {
         case CAutoDiff::SUCCEEDED:
            if (guiState == GENERATING_DIFFS_FILE)
            {
               int retVal = autoDiff->updateCuts(getThreshold());
               if (retVal != 0)
               {
                  diffProgressLabel->Caption = theError.getFmtError().c_str();
                  guiState = IDLE;
               }
               else
               {
                  guiState = UPDATING_CUTS;
               }
            }
            else // just updated cuts
            {
               int cutCount = autoDiff->getNumCutsFound();
               os << "Update complete: found ";
               if (cutCount == 0)
                  os << "no";
               else
                  os << cutCount;
               os << " scene cut" << ((cutCount == 1)? "" : "s");
               diffProgressLabel->Caption = os.str().c_str();
               guiState = IDLE;
            }
            diffProgressBar->Position = diffProgressBar->Max;
            break;

         case CAutoDiff::FAILED:
            // QQQ FIX THIS
            diffProgressLabel->Caption = theError.getFmtError().c_str();
            guiState = IDLE;
            break;

         case CAutoDiff::CANCELLED:
            diffProgressLabel->Caption = "Operation cancelled by user";
            guiState = IDLE;
			break;

		 default:
		 	break;
      }

      if (guiState == IDLE)
      {
         // Re-enable the interface
         setCurClip(clp_filename);
         closeButton->Caption = "Close";
      }
   }
   else // AutoDiffer is still busy
   {
      diffProgressLabel->Caption = autoDiff->getWhatsHappening().c_str();
      // assumes Min is always 0
      diffProgressBar->Position = (int) ((float) diffProgressBar->Max *
                                         autoDiff->getFractionDone());
   }
}
//---------------------------------------------------------------------------

void __fastcall TAutoDiffForm::closeButtonClick(TObject *Sender)
{
   if (guiState == IDLE)
   {
      // Setting ModalResult will close the window
      ModalResult = mrCancel;
   }
   else if (autoDiff != NULL)
   {
      autoDiff->cancel();
   }

}
//---------------------------------------------------------------------------

