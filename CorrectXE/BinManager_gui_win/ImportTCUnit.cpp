//--------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ImportTCUnit.h"
#include "ClipAPI.h"
#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
//---------------------------------------------------------------------
#pragma link "PopupComboBox"
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TImportTCDialog *ImportTCDialog;
//---------------------------------------------------------------------
__fastcall TImportTCDialog::TImportTCDialog(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TImportTCDialog::MergeCheckBoxClick(TObject *Sender)
{
   MergeTC->Enabled = MergeCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TImportTCDialog::AlternateCheckBoxClick(TObject *Sender)
{
     PanelMiddle->Visible = AlternateCheckBox->Checked;
     if (PanelMiddle->Visible)
       PanelBottom->Top = PanelMiddle->Top + PanelMiddle->Height;
     else
       PanelBottom->Top = PanelTop->Top + PanelTop->Height;
}
//---------------------------------------------------------------------------

//-------------------ReadSettings-------------------------John Mertus-----Jan 2001---

   bool TImportTCDialog::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the setting of the variables
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//******************************************************************************
{
   string Section = IniSection + "." + StringToStdString(Name);
   mruFileName.ReadSettings(ini, Section, "FileHistory");
   
   if (!mruFileName.empty()) FileListIO.setFileName(mruFileName[0]);

   FileListIO.setSortByTimeCode(ini->ReadBool(Section,"SortByTimeCode", FileListIO.getSortByTimeCode()));
   FileListIO.setConvertPage(ini->ReadBool(Section,"ConvertPage", FileListIO.getConvertPage()));
   FileListIO.setMergeClips(ini->ReadBool(Section,"MergeClips", FileListIO.getMergeClips()));
   FileListIO.setSrcScheme(ini->ReadString(Section,"SrcScheme", FileListIO.getSrcScheme()));
   FileListIO.setDstScheme(ini->ReadString(Section,"DstScheme", FileListIO.getDstScheme()));
   FileListIO.setAutoDuration(ini->ReadString(Section,"AutoDuration", FileListIO.getAutoDuration()));
   FileListIO.setOverlapTimeCode(ini->ReadString(Section,"OverlapTimeCode", FileListIO.getOverlapTimeCode()));

   //  Really dumb way to do this, but I don't care
   FileListIO.setSrcRefFrameI(ini->ReadString(Section,"SrcRefFrameI", FileListIO.getSrcRefFrameI()));
   FileListIO.setDstRefFrameI(ini->ReadString(Section,"DstRefFrameI", FileListIO.getDstRefFrameI()));
   FileListIO.setSrcRefFrameII(ini->ReadString(Section,"SrcRefFrameII", FileListIO.getSrcRefFrameII()));
   FileListIO.setDstRefFrameII(ini->ReadString(Section,"DstRefFrameII", FileListIO.getDstRefFrameII()));
   FileListIO.setSrcRefFrameIII(ini->ReadString(Section,"SrcRefFrameIII", FileListIO.getSrcRefFrameIII()));
   FileListIO.setDstRefFrameIII(ini->ReadString(Section,"DstRefFrameIII", FileListIO.getDstRefFrameIII()));

   ParametersToGui(FileListIO);
   return true;
}

//-------------------------WriteSettings-------------------John Mertus-----Jan 2001---

   bool TImportTCDialog::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the settings properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//
//******************************************************************************
{
  // Write the current list
  string Section = IniSection + "." +  StringToStdString(Name);

  mruFileName.WriteSettings(ini, Section, "FileHistory");

  ini->WriteBool(Section,"SortByTimeCode", FileListIO.getSortByTimeCode());
  ini->WriteBool(Section,"ConvertPage",  FileListIO.getConvertPage());
  ini->WriteBool(Section,"CleaningPage", true);
  ini->WriteBool(Section,"MergeClips", FileListIO.getMergeClips());
  ini->WriteString(Section,"SrcScheme", FileListIO.getSrcScheme());
  ini->WriteString(Section,"DstScheme", FileListIO.getDstScheme());
  ini->WriteString(Section,"AutoDuration",  FileListIO.getAutoDuration());
  ini->WriteString(Section,"OverlapTimeCode", FileListIO.getOverlapTimeCode());

  //  Really dumb way to do this, but I don't care
  ini->WriteString(Section,"SrcRefFrameI", FileListIO.getSrcRefFrameI());
  ini->WriteString(Section,"DstRefFrameI", FileListIO.getDstRefFrameI());
  ini->WriteString(Section,"SrcRefFrameII", FileListIO.getSrcRefFrameII());
  ini->WriteString(Section,"DstRefFrameII", FileListIO.getDstRefFrameII());
  ini->WriteString(Section,"SrcRefFrameIII", FileListIO.getSrcRefFrameIII());
  ini->WriteString(Section,"DstRefFrameIII", FileListIO.getDstRefFrameIII());

  return true;
}

namespace {

void SnagMenuItemsFromTheMainForm(TComboBox *src, TComboBox *dst)
{
   TStrings *srcMenuItems = src->Items;
   TStrings *dstMenuItems = dst->Items;
   
   dstMenuItems->Clear();
   for (int i = 0; i < srcMenuItems->Count; ++i)
      {
      dstMenuItems->Add(srcMenuItems->Strings[i]);
      }
   dst->ItemIndex = src->ItemIndex;
}

} // local namespace

void __fastcall TImportTCDialog::FormShow(TObject *Sender)
{
#ifdef __OLD_WAY__
//  Because only one popupmenu can be associated with a dropdown menu
// each format menu must be copied this way

   CopyMenu(PopupMenu1, BinManagerForm->ClipSchemePopupMenu);
   RecordFormatPopupBox->DropDownMenu = PopupMenu1;


   CopyMenu(PopupMenu2, BinManagerForm->ClipSchemePopupMenu);
   ListFormatPopupBox->DropDownMenu = PopupMenu2;

   // This is only needed if there is no ini files, in this case
   // set reasonable defaults.
   if (FileListIO.getDstScheme() == "")
      FileListIO.setDstScheme(BinManagerForm->ClipSchemePopupBox->Text.c_str());
   if (FileListIO.getSrcScheme() == "")
      FileListIO.setSrcScheme(FileListIO.getDstScheme());
#else
   VideoFormatValue->Caption = BinManagerForm->NewMediaVideoFormatComboBox->Text;
   AudioFormatValue->Caption = BinManagerForm->NewMediaAudioFormatComboBox->Text;
   if (BinManagerForm->VideoStoreMediaStorageComboBox->Visible)
      StorageFormatValue->Caption = BinManagerForm->VideoStoreMediaStorageComboBox->Text;
   else
      StorageFormatValue->Caption = BinManagerForm->FileRepositoryStorageFormatComboBox->Text;
   MediaLocationValue->Caption = BinManagerForm->MediaLocationComboBox->Text;

   // Initialize file TC video format combo box, if we haven't already done so
   if (FileTCFormatComboBox->Items->Count == 0)
      {
      // EVIL! Reach into bin manager form and copy out a menu!!
      SnagMenuItemsFromTheMainForm(BinManagerForm->NewMediaVideoFormatComboBox,
                                   FileTCFormatComboBox);
      FileTCFormatComboBox->Text = VideoFormatValue->Caption;
      if (FileTCFormatComboBox->ItemIndex < 0)
         FileTCFormatComboBox->ItemIndex = 0;
      }
#endif

   if (FileListIO.getFileName() == "")
     if (!mruFileName.empty()) FileListIO.setFileName(mruFileName[0].c_str());

   ParametersToGui(FileListIO);

}
//---------------------------------------------------------------------------



void __fastcall TImportTCDialog::OKButtonClick(TObject *Sender)
{
    CFileListIO TmpFileListIO;
    CBusyCursor Busy(true);

    // Collect the parameters
    GuiToParameters(TmpFileListIO);
    ////TmpFileListIO.setBinPath( StringToStdString(BinManagerForm->CurrentBinPath->Text));
    TmpFileListIO.setBinPath(BinManagerForm->GetSelectedBin());

    // Try to do it
    if (TmpFileListIO.GenerateClipSchemes() != 0)
       {
       _MTIErrorDialog(Handle, (string)"Mark file create failed\n" + theError.getFmtError());
       ModalResult = mrNone;
       return;
       }
    // On success, copy the temp parameters into persistent ones
    // THIS IS WRONG and will create a bug someday
    // The proper way is to create a copy operator and
    // copy the temp into the persistent
    GuiToParameters(FileListIO);
    mruFileName.push( StringToStdString(FileNameComboBox->Text));
}

//---------------------------------------------------------------------------

void __fastcall TImportTCDialog::FileBrowseButtonClick(TObject *Sender)
{
   if (OpenDialog->Execute())
      FileNameComboBox->Text = OpenDialog->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TImportTCDialog::FileNameComboBoxDropDown(TObject *Sender)
{
   FileNameComboBox->Items->Clear();
   for (unsigned i =0; i < mruFileName.size(); i++)
     FileNameComboBox->Items->Add(mruFileName[i].c_str());
}
//---------------------------------------------------------------------------

void TImportTCDialog::ParametersToGui(CFileListIO &flio)
{
    FileNameComboBox->Text = flio.getFileName().c_str();
#ifdef __OLD_WAY__
    RecordFormatPopupBox->Text = flio.getDstScheme().c_str();
    ListFormatPopupBox->Text = flio.getSrcScheme().c_str();
#else
    // Ha ha, I really have no idea here, there's so many frickin'
    // copies of parameters floating around
    CClipSchemeManager csMgr;
    CClipScheme *dstScheme = csMgr.findClipScheme(flio.getDstScheme());
    CClipScheme *srcScheme = csMgr.findClipScheme(flio.getSrcScheme());

    if (dstScheme != 0)
     {
      if (VideoFormatValue->Caption.IsEmpty())
         VideoFormatValue->Caption = dstScheme->getVideoFormatClass().c_str();
      if (AudioFormatValue->Caption.IsEmpty())
         AudioFormatValue->Caption = dstScheme->getAudioFormatClass().c_str();
      if (StorageFormatValue->Caption.IsEmpty())
         StorageFormatValue->Caption = dstScheme->getMediaStorageClass().c_str();
     }
    if (MediaLocationValue->Caption.IsEmpty())
      MediaLocationValue->Caption = flio.getDiskScheme().c_str();

    if (srcScheme != 0 && FileTCFormatComboBox->Text.IsEmpty())
     {
      FileTCFormatComboBox->Text = srcScheme->getVideoFormatClass().c_str();
     }
#endif
    HardParametersToGui(flio);
}

void TImportTCDialog::HardParametersToGui(CFileListIO &flio)
{
    AutoPadTC->tcP = flio.getAutoDuration();
    MergeTC->tcP = flio.getOverlapTimeCode();
    MergeCheckBox->Checked = flio.getMergeClips();
    AlternateCheckBox->Checked = flio.getConvertPage();
    SortCheckBox->Checked = flio.getSortByTimeCode();
    Part1TCSource->tcP = flio.getSrcRefFrameI();
    Part1TCDest->tcP = flio.getDstRefFrameI();
    Part2TCSource->tcP = flio.getSrcRefFrameII();
    Part2TCDest->tcP = flio.getDstRefFrameII();
    Part3TCSource->tcP = flio.getSrcRefFrameIII();
    Part3TCDest->tcP = flio.getDstRefFrameIII();
}

void TImportTCDialog::GuiToParameters(CFileListIO &flio)
{
    flio.setFileName( StringToStdString(FileNameComboBox->Text));
#ifdef __OLD_WAY__
    flio.setDstScheme(RecordFormatPopupBox->Text.c_str());
    flio.setSrcScheme(ListFormatPopupBox->Text.c_str());
#else
   CClipSchemeManager clipSchemeManager;

   // DESTINATION clip scheme
   CClipScheme* clipScheme = clipSchemeManager.findClipScheme(
										 StringToStdString(VideoFormatValue->Caption),
										 StringToStdString(AudioFormatValue->Caption),
										 StringToStdString(StorageFormatValue->Caption));
   if (clipScheme == 0)
      {
      // "Can't happen"
      MTIostringstream ostr;

      TRACE_0(errout
        << "ERROR: Clip Scheme lookup failed for:" << endl
        << "       VideoFormat=" << VideoFormatValue->Caption.c_str() << endl
        << "       AudioFormat=" << AudioFormatValue->Caption.c_str() << endl
        << "       StorageFormat=" << StorageFormatValue->Caption.c_str());
      ostr << "Cannot find a Clip Scheme that matches    " << endl
        << "       VideoFormat=" << VideoFormatValue->Caption.c_str() << endl
        << "       AudioFormat=" << AudioFormatValue->Caption.c_str() << endl
        << "       StorageFormat=" << StorageFormatValue->Caption.c_str();

      _MTIErrorDialog(Handle, ostr.str());
      }
   else
      {
      flio.setDstScheme(clipScheme->getClipSchemeName());
      }

   // SOURCE clip scheme -- only care about the video format
   clipScheme = clipSchemeManager.findClipScheme( StringToStdString(FileTCFormatComboBox->Text), "", "");
   if (clipScheme == 0)
      {
      // "Can't happen"
      MTIostringstream ostr;

      TRACE_0(errout
        << "ERROR: Clip Scheme lookup failed for:" << endl
        << "       VideoFormat=" << FileTCFormatComboBox->Text.c_str()
        << ", AudioFormat=ANY, StorageFormat=ANY");
      ostr << "Cannot find a Clip Scheme that matches    " << endl
           << "VideoFormat=" << FileTCFormatComboBox->Text.c_str()
           << ", AudioFormat=ANY, StorageFormat=ANY";

      _MTIErrorDialog(Handle, ostr.str());
      }
   else
      {
      flio.setSrcScheme(clipScheme->getClipSchemeName());
      }
    flio.setDiskScheme( StringToStdString(MediaLocationValue->Caption));
#endif
    flio.setAutoDuration(AutoPadTC->tcP);
    flio.setCleaningPage(true);
    flio.setMergeClips(MergeCheckBox->Checked);
    flio.setOverlapTimeCode(MergeTC->tcP);
    flio.setConvertPage(AlternateCheckBox->Checked);
    flio.setSortByTimeCode(SortCheckBox->Checked);
    flio.setSrcRefFrameI(Part1TCSource->tcP);
    flio.setDstRefFrameI(Part1TCDest->tcP);
    flio.setSrcRefFrameII(Part2TCSource->tcP);
    flio.setDstRefFrameII(Part2TCDest->tcP);
    flio.setSrcRefFrameIII(Part3TCSource->tcP);
    flio.setDstRefFrameIII(Part3TCDest->tcP);
}

void __fastcall TImportTCDialog::RecordFormatPopupBoxChange(
      TObject *Sender)
{
#ifdef __OLD_WAY__
  CClipInitInfo ciiDst;

  ciiDst.initFromClipScheme(RecordFormatPopupBox->Text.c_str());
  AutoPadTC->tcP.setAttributes(ciiDst.getTimecodePrototype());
  MergeTC->tcP.setAttributes(ciiDst.getTimecodePrototype());
  Part1TCDest->tcP.setAttributes(ciiDst.getTimecodePrototype());
  Part2TCDest->tcP.setAttributes(ciiDst.getTimecodePrototype());
  Part3TCDest->tcP.setAttributes(ciiDst.getTimecodePrototype());
#endif
}
//---------------------------------------------------------------------------

void __fastcall TImportTCDialog::ListFormatPopupBoxChange(TObject *Sender)
{
#ifdef __OLD_WAY__
  CClipInitInfo ciiSrc;

  ciiSrc.initFromClipScheme(ListFormatPopupBox->Text.c_str());
  Part1TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
  Part2TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
  Part3TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
#endif
}
//---------------------------------------------------------------------------

void __fastcall TImportTCDialog::RestoreButtonClick(TObject *Sender)
{
   if (_MTIWarningDialog(Handle, "Restore to Design Settings?") != MTI_DLG_OK) return;
   FileListIO.FactoryDefaults();

   HardParametersToGui(FileListIO);
}
//---------------------------------------------------------------------------



void __fastcall TImportTCDialog::FileTCFormatComboBoxChange(
      TObject *Sender)
{
#ifndef __OLD_WAY__
   CClipInitInfo ciiSrc;
   string NewVideoFormat, NewAudioFormat, NewStorageFormat;

   // THIS SUCKS... for now I am just mimicking old behavior
   NewVideoFormat   =  StringToStdString(FileTCFormatComboBox->Text);
   NewAudioFormat   =  StringToStdString(AudioFormatValue->Caption);
   NewStorageFormat =  StringToStdString(StorageFormatValue->Caption);

   CClipSchemeManager clipSchemeManager;
   CClipScheme* clipScheme = clipSchemeManager.findClipScheme(NewVideoFormat,
                                                              NewAudioFormat,
                                                              NewStorageFormat);
   if (clipScheme == 0)
      {
      // WTF! We are screwed! Need to be able to determine the timecode
      // crap directly from the video format (wildcard clip scheme search?)
      MTIostringstream ostr;
      ostr << "There is no Clip Scheme that matches    " << endl
           << "       VideoFormat=" << NewVideoFormat << endl
           << "       AudioFormat=" << NewAudioFormat << endl
           << "       StorageFormat=" << NewStorageFormat;

      _MTIErrorDialog(Handle, ostr.str());
      FileTCFormatComboBox->Text = "";
      return;
      }

  ciiSrc.initFromClipScheme(clipScheme->getClipSchemeName());
  Part1TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
  Part2TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
  Part3TCSource->tcP.setAttributes(ciiSrc.getTimecodePrototype());
#endif
}
//---------------------------------------------------------------------------

