//---------------------------------------------------------------------------

#ifndef DpxHeaderEditorUnitH
#define DpxHeaderEditorUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "IniFile.h"         // for StringList
#include "MediaInterface.h"
#include "ColorLabel.h"  // for Media Hack Commands

#include <atomic>
#include <set>
//---------------------------------------------------------------------------
class TDpxHeaderEditorForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *AreYouSurePanel;
        TPanel *Separator1;
        TButton *SetupPanelOkButton;
        TButton *SetupPanelCancelButton;
        TPanel *ProgressPanel;
        TLabel *ClipNameLabel;
        TLabel *ClipNameValue;
        TLabel *ModifyingModeLabel;
        TLabel *FrameLabel;
        TLabel *FrameValue;
        TLabel *OfLabel;
        TLabel *OfValue;
        TProgressBar *ClipListProgressBar;
        TProgressBar *FrameIndexProgressBar;
        TPanel *Separator2;
        TButton *ProgressPanelCloseButton;
        TTimer *ProgressTimer;
        TLabel *StatusLabel;
   TLabel *Label1;
   TLabel *Label2;
   TPanel *OptionsPanel;
   TButton *OptionsPanelOkButton;
   TButton *OptionsPanelCancelButton;
   TLabel *ChangeDpxHeaderFrameRateLabel;
   TLabel *ToLabel;
   TLabel *FromLabel;
   TLabel *FromFrameRatesLabel;
   TLabel *FpsLabel;
   TComboBox *NewFpsComboBox;
   TLabel *SourceClipCountLabel;
   TColorLabel *DifferentFrameRatesWarningLabel;
        void __fastcall OkButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ProgressPanelCloseButtonClick(
          TObject *Sender);
        void __fastcall ProgressTimerTimer(TObject *Sender);
private:	// User declarations
        string mBinPath;
        StringList mClipList;
        void *mProcessingThreadId;

        static void ProcessingThread(void *aThreadArg, void *aThreadHandle);
        std::tuple<string, float, int> ScanAllClipsForFrameRate();
        void ScanOneClipForFrameRate(string aClipName, std::set<float> &frameRatesFound);
        void HackAllClips();
        void HackOneClip(string aClipName, float newFrameRate);

        // Processing Thread communications
        // TO thread
        EMediaHackCommand mHackCommand;
        bool mCancelledFlag;
        // FROM thread
        AnsiString mClipName;
        int mClipIndex;
        int mTotalClipCount;
		std::atomic_int mFrameIndex;
        std::atomic_bool mAborted;
        int mTotalFrameCount;
        bool mProcessingIsCompleteFlag;
        //
public:		// User declarations
        __fastcall TDpxHeaderEditorForm(TComponent* Owner);
        void Reset();
        void SetBinPath(string aBinPath);
        void AddClip(string aClipName);

};
//---------------------------------------------------------------------------
extern PACKAGE TDpxHeaderEditorForm *DpxHeaderEditorForm;
//---------------------------------------------------------------------------
#endif
