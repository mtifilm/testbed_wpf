//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "EnterCommentUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TEnterCommentForm *EnterCommentForm;
//---------------------------------------------------------------------------
__fastcall TEnterCommentForm::TEnterCommentForm(TComponent* Owner)
        : TForm(Owner)
{
   EnterCommentEdit->Text = "";
}
//---------------------------------------------------------------------------

void TEnterCommentForm::setComment(const string &newComment)
{
   EnterCommentEdit->Text = newComment.c_str();
}
//---------------------------------------------------------------------------

string TEnterCommentForm::getComment()
{
   return StringToStdString(EnterCommentEdit->Text);
}
//---------------------------------------------------------------------------

void TEnterCommentForm::setCaption(const string &newCaption)
{
   EnterCommentLabel->Caption = String(newCaption.c_str());
}
//---------------------------------------------------------------------------




