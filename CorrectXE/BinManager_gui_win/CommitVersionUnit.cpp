//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CommitVersionUnit.h"
#include "BinManagerGUIUnit.h"   // for FinishCommittingClip() callback
#include "IniFile.h"             // for StringList
#include "ClipAPI.h"
#include "bthread.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
#include "SaveRestore.h"
#include "FileSweeper.h"
#include <vector>
using std::vector;

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCommitVersionForm *CommitVersionForm;
//---------------------------------------------------------------------------
__fastcall TCommitVersionForm::TCommitVersionForm(TComponent* Owner)
        : TForm(Owner)
        , mOpMode(INVALID_MODE)
        , mVersionClipFate(INVALID_FATE)
        , mVersionClipInfo(NULL)
        , mParentClipInfo(NULL)
        , mCancelledFlag(false)
        , mProcessingIsCompleteFlag(false)
        , mHaltedOnErrorFlag(false)
        , mStatusMessage("")
        , mProgressPercent(0)
        , mFramesRemainingCount(0)
        , mFirstVideoFrame(0)
        , mLastVideoFrame(-1)
        , mFirstFilmFrame(0)
        , mLastFilmFrame(-1)
        , mShowWarningPanelFlag(true)
        , mOkToDeleteTheCommittedClip(false)
{
}
//---------------------------------------------------------------------------

void TCommitVersionForm::Init(OpMode opMode, ClipFate clipFateIfNoChangesLeft,
                              CClipInitInfo *versionClipInfo,
                              CClipInitInfo *parentClipInfo)
{
    mOpMode = opMode;
    mVersionClipFate = clipFateIfNoChangesLeft;
    mVersionClipInfo = versionClipInfo;
    mParentClipInfo = parentClipInfo;

    VersionClipValue->Caption = mVersionClipInfo->getClipName().c_str();
    ParentClipValue->Caption = mParentClipInfo->getClipName().c_str();
    mOkToDeleteTheCommittedClip = false;

    switch (mOpMode)
    {
        case COMMIT_CHANGES:
            ProgressHeaderLabel->Caption =
                          "Copy version changes into master clip";
            break;
        case DISCARD_CHANGES:
            ProgressHeaderLabel->Caption =
                          "Discard version clip changes";
            break;
        default:
            break;
    }
    mFirstVideoFrame = 0;
    mLastVideoFrame = -1;   // end of clip
    mFirstFilmFrame = 0;
    mLastFilmFrame = -1;   // end of clip
    mMarkIn = CTimecode::NOT_SET;
    mMarkOut = CTimecode::NOT_SET;
}
//---------------------------------------------------------------------------

void TCommitVersionForm::SetFrameRange(int firstVideoFrame, int lastVideoFrame,
                                       int firstFilmFrame,  int lastFilmFrame)
{
    mFirstVideoFrame = firstVideoFrame;
    mLastVideoFrame = lastVideoFrame;
    mFirstFilmFrame = firstFilmFrame;
    mLastFilmFrame = lastFilmFrame;
}
//---------------------------------------------------------------------------

void TCommitVersionForm::SetRange(CTimecode markIn, CTimecode markOut)
{
    mMarkIn = markIn;
    mMarkOut = markOut;
}
//---------------------------------------------------------------------------

void TCommitVersionForm::SetShowWarningPanel(bool showWarningPanelFlag)
{
    mShowWarningPanelFlag = showWarningPanelFlag;
}
//---------------------------------------------------------------------------

void __fastcall TCommitVersionForm::FormShow(TObject *Sender)
{
    AutoCloseTimer->Enabled = false;
    ProgressTimer->Enabled = false;
    if (mMarkIn != CTimecode::NOT_SET && mMarkOut != CTimecode::NOT_SET)
    {
        string strMarkIn, strMarkOut;
        mMarkIn.getTimeString(strMarkIn);
        mMarkOut.getTimeString(strMarkOut);
        strMarkIn = StringTrim(strMarkIn);
        strMarkOut = StringTrim(strMarkOut);
        RangeValue->Caption = (strMarkIn + " to " + strMarkOut).c_str();
        RangeLabel->Visible = true;
        RangeValue->Visible = true;
    }
    else
    {
        RangeLabel->Visible = false;
        RangeValue->Visible = false;
    }
    if (mShowWarningPanelFlag)
    {
        ProgressPanel->Visible = false;
        SetupPanel->Visible = true;
    }
    else
    {
        // Come out guns-a-blazin'
        Start();
    }
}
//---------------------------------------------------------------------------

void TCommitVersionForm::Start()
{
    // These values are all set in the processing thread and read in
    // ProgressTimer(); we initialize them to dummy values here
    mProgressPercent = 0;
    mStatusMessage = "Initializing...";
    mProcessingIsCompleteFlag = false;
    mHaltedOnErrorFlag = false;
    mCancelledFlag = false;

    // These GUI elements are all maintained by ProgressTimer()
    FrameIndexProgressBar->Position = 0;
    ProgressPanelCloseButton->Caption = "Cancel";
    StatusMessageLabel->Caption = "Initializing...";
    StatusLabel->Visible = false;
    //

    // OK to swap the panels now
    SetupPanel->Visible = false;
    ProgressPanel->Visible = true;

    mProcessingThreadId = BThreadSpawn(ProcessingThread, (void *)this);
    if (mProcessingThreadId == NULL)
    {
        mHaltedOnErrorFlag = true;
        mStatusMessage = "INTERNAL ERROR - Processing thread launch failed";
        TRACE_0(errout << "INTERNAL ERROR: TCommitVersionForm::Start(): "
                          " Processing thread launch failed");
    }

    // fire up the timer, which will track the progress and display it
    ProgressTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void TCommitVersionForm::ProcessingThread(void *aThreadArg,
                                               void *aThreadHandle)
{
    TCommitVersionForm *this_ =
        reinterpret_cast<TCommitVersionForm *>(aThreadArg);

    BThreadBegin (aThreadHandle);

    if (this_ != NULL)
    {
        this_->Process();
    }
}
//---------------------------------------------------------------------------

void TCommitVersionForm::Process()
{
    string versionClipName = mVersionClipInfo->getClipName();
    string parentClipName = mParentClipInfo->getClipName();
    int errorCode;
    vector<int> fieldListIndexesOfFieldsChangedInThisVideoFrame;
    bool gotAHistoryCommitError = false;
    unsigned frameIndex;
    vector<int> listOfDirtyVideoFrameIndexes;
    vector<int> listOfDirtyFilmFrameIndexes;
    int numberOfFramesToProcess = 0;
    CBinManager binManager;

    auto versionClip = mVersionClipInfo->getClipPtr();
    CVideoProxy *versionMainVideo = versionClip->getVideoProxy(
                                                        VIDEO_SELECT_NORMAL);
    if (versionMainVideo == NULL)
    {
        TRACE_0(errout << "ERROR: Version clip " << versionClipName << endl
                       << "         does not have a main video proxy");
        mHaltedOnErrorFlag = true;
        return;
    }

    CVideoFrameList *versionVideoFrameList =
        versionMainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
    CVideoFrameList *versionFilmFrameList =
        versionMainVideo->getVideoFrameList(FRAMING_SELECT_FILM);
    if (versionVideoFrameList == NULL || versionFilmFrameList == NULL)
    {
        TRACE_0(errout << "ERROR: Version clip " << versionClipName << endl
                       << "         does not have a main video frame list");
        mHaltedOnErrorFlag = true;
        return;
    }

    int totalVideoFrameCount = versionVideoFrameList->getTotalFrameCount();
    int totalFilmFrameCount = versionFilmFrameList->getTotalFrameCount();

    if (mMarkIn != CTimecode::NOT_SET)
    {
       mFirstVideoFrame = versionVideoFrameList->getFrameIndex(mMarkIn);
       mFirstFilmFrame = versionFilmFrameList->getFrameIndex(mMarkIn);
    }

    if (mMarkOut != CTimecode::NOT_SET)
    {
       // GAG! is this right???? QQQ
       mLastVideoFrame = versionVideoFrameList->getFrameIndex(mMarkOut) - 1;
       mLastFilmFrame = versionFilmFrameList->getFrameIndex(mMarkOut) - 1;
    }

    if (mLastVideoFrame < 0)
        mLastVideoFrame = totalVideoFrameCount - 1;
    if (mLastFilmFrame < 0)
        mLastFilmFrame = totalFilmFrameCount - 1;

    auto parentClip = mParentClipInfo->getClipPtr();
    CVideoProxy *parentMainVideo = parentClip->getVideoProxy(
                                                         VIDEO_SELECT_NORMAL);
    if (parentMainVideo == NULL)
    {
        TRACE_0(errout << "ERROR: Parent clip " << parentClipName << endl
                       << "         does not have a main video proxy");
        mHaltedOnErrorFlag = true;
        return;
    }

    CVideoFrameList *parentVideoFrameList =
        parentMainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
    CVideoFrameList *parentFilmFrameList =
        parentMainVideo->getVideoFrameList(FRAMING_SELECT_FILM);
    if (parentVideoFrameList == NULL || parentFilmFrameList == NULL)
    {
        TRACE_0(errout << "ERROR: Parent clip " << parentClipName << endl
                       << "         does not have a main video frame list");
        mHaltedOnErrorFlag = true;
        return;
    }

    if (mOpMode == COMMIT_CHANGES)
    {
       TRACE_2(errout << "INFO: Examining "
                      << (mLastVideoFrame - mFirstVideoFrame + 1)
                      << " VIDEO frames from " << versionClipName << endl
                      << "      for committing back into parent clip "
                      << parentClipName);
    }

    // Skip video frames list if we are committing and the frame list is locked
    int lockedFrameListIndex = versionClip->getLockedFrameListIndex();
    if (mOpMode == DISCARD_CHANGES ||
        lockedFrameListIndex != FRAMING_SELECT_VIDEO)
    {
       // Find dirty video frames
       mStatusMessage = "Looking for modified frames...";
       Sleep(500);
       int retVal = FindDirtyFrames(versionVideoFrameList,
                                    mFirstVideoFrame, mLastVideoFrame,
                            /*out*/ listOfDirtyVideoFrameIndexes);
       if (retVal != 0)
       {
           mHaltedOnErrorFlag = true;
           return;
       }
       numberOfFramesToProcess +=  listOfDirtyVideoFrameIndexes.size();
    }

    // skip film frames if the film and video frame lists are the same
    // or if we are committing and the film frame list is locked.
    if ((versionVideoFrameList != versionFilmFrameList) &&
        (mOpMode == DISCARD_CHANGES ||
                lockedFrameListIndex != FRAMING_SELECT_FILM))
    {
        // Sanity check
        if (mOpMode == COMMIT_CHANGES &&
            parentVideoFrameList == parentFilmFrameList)
        {
            TRACE_0(errout << "ERROR: Could not commit version: "
                           << mVersionClipInfo->getClipName()
                           << " into clip "
                           << mParentClipInfo->getClipName() << ": "<< endl
                           << "    Clip structures don't match!!");
            mStatusMessage = "INTERNAL ERROR";
            mHaltedOnErrorFlag = true;
            return;
        }

        if (mOpMode == COMMIT_CHANGES)
        {
            TRACE_2(errout << "INFO: Examining " << totalFilmFrameCount
                           << " FILM frames from " << versionClipName << endl
                           << "      for committing back into parent clip "
                           << parentClipName);
        }

        mStatusMessage = "Looking for modified frames...";
        int retVal = FindDirtyFrames(versionFilmFrameList,
                                     mFirstFilmFrame, mLastFilmFrame,
                             /*out*/ listOfDirtyFilmFrameIndexes);
        if (retVal != 0)
        {
            mHaltedOnErrorFlag = true;
            return;
        }
        numberOfFramesToProcess +=  listOfDirtyFilmFrameIndexes.size();
    }

    // Reset the clip frame list status hacks back to defaults
    versionClip->setLockedFrameListIndex(-1);
    versionClip->setMainFrameListIndex(FRAMING_SELECT_VIDEO);

    // Bail out here if no frames to commit or discard
    if (numberOfFramesToProcess == 0)
    {
        mStatusMessage = "Nothing to do";
        Sleep(500);
        string rangeBlurb;
        _MTIInformationDialog(Handle, "No modified frames found;    \n"
                         "there is nothing to do");
        mProcessingIsCompleteFlag = true;    // Success, I guess
        return;
    }

    if (mOpMode == COMMIT_CHANGES)
	 {
		 TRACE_3(errout << "Committing video frames: verifying track file integrity...");

		 // Check the integrity of the track/field list files before committing.
		 // If they are out of sync, offer to fix the clip. Whether it's fixed or
		 // not, the commit is aborted (we want the user to have a chance to review
		 // the fixed frames before committing).
		 string firstCorruptFieldName;
		 string lastCorruptFieldName;
		 int retVal = CheckAndMaybeFixDirtyMedia(listOfDirtyVideoFrameIndexes, versionVideoFrameList);
		  if (retVal != 0)
		  {
				// Don't continue even if the track files were fixed.
				mHaltedOnErrorFlag = true;
				return;
		  }

		 TRACE_3(errout << "Committing video frames: copying media...");

		  // Commit VIDEO media and history
		  mStatusMessage = "Copying media files...";
		  mFramesRemainingCount = listOfDirtyVideoFrameIndexes.size();
		  Sleep(500);
		  retVal = CommitMedia(listOfDirtyVideoFrameIndexes,
									  versionVideoFrameList,
									  parentVideoFrameList);
		  if (retVal != 0)
		  {
				// Fatal error - don't continue
				mHaltedOnErrorFlag = true;
				return;
		  }

		  TRACE_3(errout << "Committing video frames: copying history...");

		  mStatusMessage = "Copying history files...";
		  Sleep(500);
		  retVal = ReplaceHistory(listOfDirtyVideoFrameIndexes,
										  versionVideoFrameList,
										  parentVideoFrameList,
										  versionClip,
										  parentClip);
		  if (retVal != 0)
		  {
				// Not as serious - don't abort
				gotAHistoryCommitError = true;
		  }

		  TRACE_3(errout << "Committing film frames: copying media...");

		  // Commit FILM media and history
		  mFramesRemainingCount = listOfDirtyFilmFrameIndexes.size();
		  if (mFramesRemainingCount > 0)
		  {
			  mStatusMessage = "Copying media files...";
			  retVal = CommitMedia(listOfDirtyFilmFrameIndexes,
										  versionFilmFrameList,
										  parentFilmFrameList);
			  if (retVal != 0)
			  {
               // Fatal error - don't continue
					mHaltedOnErrorFlag = true;
               return;
			  }

			  TRACE_3(errout << "Committing film frames: copying history...");

           mStatusMessage = "Copying history files...";
           retVal = ReplaceHistory(listOfDirtyFilmFrameIndexes,
											  versionFilmFrameList,
                                   parentFilmFrameList,
                                   versionClip,
											  parentClip);
           if (retVal != 0)
           {
					// Not as serious - don't abort
               gotAHistoryCommitError = true;
               // Well it is sort of serious because we don't have  a
					// frickin' clue what to do now with the histories!!
           }
        }

        // Update the "last media action" (shows up in the clip "status"
        // column in the bin manager clip list view)
		  parentMainVideo->setLastMediaAction(versionMainVideo->getLastMediaAction());

// This was moved to the end of the
//        // Update the "modified frame" counter
////        string versionClipPath = mVersionClipInfo->getClipPtr()->getClipPathName();
//        binManager.bumpModifiedFrameCounter(versionClipName, -numberOfFramesToProcess);

		  ////////////////////////////////////////////////////////////////////
		  // The nightmare continues.... we need to hack the histories of
		  // EVERY OTHER FRICKIN' SIBLING VERSION CLIP.... sigh ...
		  ////////////////////////////////////////////////////////////////////

		  TRACE_3(errout << "Updating sibling version clip histories...");

		  UpdateAllSiblingVersionClipHistories(
															parentClip,
															parentVideoFrameList,
															listOfDirtyVideoFrameIndexes,
															mFirstVideoFrame, mLastVideoFrame,
															parentFilmFrameList,
															listOfDirtyFilmFrameIndexes,
															mFirstFilmFrame, mLastFilmFrame);

		  TRACE_3(errout << "COPYING IS COMPLETE!");


		  // Clip deletion is triggered from the timer callback. Only delete if
		  // the entire clip was committed.
		  mOkToDeleteTheCommittedClip = mFirstVideoFrame == 0 && mLastVideoFrame == (totalVideoFrameCount - 1);

		  ///////////////////////////////////////////////////////////////////
		  ///  WARNING!!! WE KNOW THAT THE CLIP IS GOING TO BE DELETED SO ///
		  ///             WE DON'T BOTHER TO SYNC UP ODDBALL FRAME LISTS! ///
		  ///             This is, of course, wrong...                    ///
		  ///////////////////////////////////////////////////////////////////
	 }
	 else
	 {
        // Discard VIDEO media and history

        // Don't destroy media for a frame list that was locked
        bool destroyVideoMedia = (lockedFrameListIndex != FRAMING_SELECT_VIDEO);
        bool destroyFilmMedia = (lockedFrameListIndex != FRAMING_SELECT_FILM);

        mStatusMessage = "Discarding modified media files...";
        mFramesRemainingCount = listOfDirtyVideoFrameIndexes.size();
        Sleep(500);
        int retVal = DiscardMedia(listOfDirtyVideoFrameIndexes,
                             versionVideoFrameList,
                             parentVideoFrameList,
                             destroyVideoMedia);
        if (retVal != 0)
        {
            // Fatal error - don't continue
            mHaltedOnErrorFlag = true;
            return;
        }
        mStatusMessage = "Discarding history files...";
        Sleep(500);
        retVal = ReplaceHistory(listOfDirtyVideoFrameIndexes,
                                parentVideoFrameList,
                                versionVideoFrameList,
                                parentClip,
                                versionClip);
        if (retVal != 0)
        {
            // Not as serious - don't abort
            gotAHistoryCommitError = true;
        }

        // Discard FILM media and history
        mFramesRemainingCount = listOfDirtyFilmFrameIndexes.size();
        if (mFramesRemainingCount > 0)
        {
           mStatusMessage = "Discarding modified media files...";
           retVal = DiscardMedia(listOfDirtyFilmFrameIndexes,
                                 versionFilmFrameList,
                                 parentFilmFrameList,
                                 destroyFilmMedia);
           if (retVal != 0)
           {
               // Fatal error - don't continue
               mHaltedOnErrorFlag = true;
               return;
           }
           mStatusMessage = "Discarding history files...";
           retVal = ReplaceHistory(listOfDirtyFilmFrameIndexes,
                                   parentFilmFrameList,
                                   versionFilmFrameList,
                                   parentClip,
                                   versionClip);
           if (retVal != 0)
           {
               // Not as serious - don't abort
               gotAHistoryCommitError = true;
               // Well it is sort of serious because we don't have  a
               // frickin' clue what to do now with the histories!!
           }
        }

        // Update the "last media action" (shows up in the clip "status"
        // column in the bin manager clip list view)
        parentMainVideo->setLastMediaAction(
                              versionMainVideo->getLastMediaAction());

// This was moved to the end of the method
        // Update the "modified frame" counter
//        string versionClipPath = mVersionClipInfo->getClipPtr()->getClipPathName();
//        binManager.bumpModifiedFrameCounter(versionClipName, -numberOfFramesToProcess);

        ////////////////////////////////////////////////////////////////////
        // The nightmare continues.... we need to hack the histories of
        // EVERY OTHER FRICKIN' SIBLING VERSION CLIP.... sigh ...
        ////////////////////////////////////////////////////////////////////

        UpdateAllSiblingVersionClipHistories(parentClip,
                                             parentVideoFrameList,
                                             listOfDirtyVideoFrameIndexes,
                                             mFirstVideoFrame, mLastVideoFrame,
                                             parentFilmFrameList,
                                             listOfDirtyFilmFrameIndexes,
                                             mFirstFilmFrame, mLastFilmFrame);

        ///////////////////////////////////////////////////////////////////
        // DANGER! DANGER! WE NUKE ODDBALL FRAMELISTS HERE SO THEY WILL  //
        // GET RECONSTRUCTED AS NEEDED. But if someone is holding on to  //
        // a frame or field pointer, the app will crash. This is only    //
        // applicable to the oddball video framelists (e.g. FIELDS_1_2)  //
        ///////////////////////////////////////////////////////////////////

        CFrameList *oddballFrameList;
        CFrame *oddballFrame;
        CField *oddballField;

        oddballFrameList = versionMainVideo->getBaseFrameList(
                                                   FRAMING_SELECT_FIELDS_1_2);
        if ((oddballFrameList != NULL) &&
            oddballFrameList->isFrameListAvailable())
        {
            oddballFrameList->deleteFrameList();
            oddballFrameList->getTotalFrameCount();  // forces reconstruction
        }
        oddballFrameList = versionMainVideo->getBaseFrameList(
                                                  FRAMING_SELECT_FIELD_1_ONLY);
        if ((oddballFrameList != NULL) &&
            oddballFrameList->isFrameListAvailable())
        {
            oddballFrameList->deleteFrameList();
            oddballFrameList->getTotalFrameCount();  // forces reconstruction
        }
        oddballFrameList = versionMainVideo->getBaseFrameList(
                                                  FRAMING_SELECT_FIELD_2_ONLY);
        if ((oddballFrameList != NULL) &&
            oddballFrameList->isFrameListAvailable())
        {
            oddballFrameList->deleteFrameList();
            oddballFrameList->getTotalFrameCount();  // forces reconstruction
        }

        // Clip deletion is triggered from the timer callback. Only delete if
        // the entire clip was discarded.
        mOkToDeleteTheCommittedClip = mFirstVideoFrame == 0 && mLastVideoFrame == (totalVideoFrameCount - 1);
    }

    ////////////////////////////////////////////////////////////////////
    // WTF! need to explicitly flush frame lists to disk
    ////////////////////////////////////////////////////////////////////

    //////////////////HACK/////////////////////////HACK/////////////////
    // This is seriously messed up. For some reason if I don't ask for
    // the frame count now for the film frames track, the stupid frame
    // list writing stuff first truncates the frame list file, then asks
    // what the frame count is, so it loads the truncated file and
    // declares the frame count to be 0, so the frickin' frame list file
    // stays 0 length
    ////////////////////////////////////////////////////////////////////
    versionFilmFrameList->getTotalFrameCount();  // Ignore return value
    parentFilmFrameList->getTotalFrameCount(); // ignore return value

    versionMainVideo->writeAllFrameListFiles();
    parentMainVideo->writeAllFrameListFiles();

    // Update the Version clip and parent clip "modified frame" counters
    binManager.recomputeModifiedFrameCount(mVersionClipInfo->getClipPtr()->getClipPathName());
    binManager.recomputeModifiedFrameCount(mParentClipInfo->getClipPtr()->getClipPathName());

    // Report non-fatal error or success - picked up by timer routine
    if (gotAHistoryCommitError)
    {
        mStatusMessage = "History copy failed";
        mHaltedOnErrorFlag = true;
    }
    else
    {
        // Successful completion
        mStatusMessage = "";   // No message is good message
        mProcessingIsCompleteFlag = true;
    }
}
//---------------------------------------------------------------------------

int TCommitVersionForm::FindDirtyFrames(
               const CVideoFrameList *aFrameList,
               int firstFrameIndex, int lastFrameIndex,
     /* out */ vector<int> &aListOfDirtyFrameIndexes)
{
    CBinManager binManager;
    int retVal = binManager.FindDirtyFrames(aFrameList,
                                            firstFrameIndex, lastFrameIndex,
                                            aListOfDirtyFrameIndexes,
                                            &mProgressPercent,
                                            &mFramesRemainingCount);

    if (retVal != 0)
    {
        mStatusMessage = "INTERNAL ERROR";
        mHaltedOnErrorFlag = true;
        return retVal;
    }

    return 0;  // success
}
//---------------------------------------------------------------------------

int TCommitVersionForm::CheckAndMaybeFixDirtyMedia(
				const vector<int> &listOfDirtyFrameIndexes,
				CVideoFrameList *versionFrameList)
{
	int retVal1 = 0;
	CBinManager binManager;
	string firstCorruptFieldName;
	string lastCorruptFieldName;

	retVal1 = binManager.CheckDirtyMedia(
									listOfDirtyFrameIndexes,
									versionFrameList,
									&firstCorruptFieldName,
									&lastCorruptFieldName,
									&mProgressPercent,
									&mFramesRemainingCount);

	if (retVal1 != 0)
	{
		MTIostringstream os;
		os << "ERROR: Unable to proceed because track information was corrupted   " << endl
			<< "       in the frame range " << firstCorruptFieldName << " - " << lastCorruptFieldName << "." << endl
			<< "       Attempt to repair the track information?";
		_dialogMessage = os.str();
		_dialogType = confirmationDialog;
		_showDialog = true;
		while (_showDialog)
		{
		  MTImillisleep(50);
		}

		if (_dialogResponse == MTI_DLG_OK)
		{
			int retVal2 = binManager.FixOutOfSyncDirtyMedia(
													versionFrameList,
													&mProgressPercent,
													&mFramesRemainingCount);
			os.str("");
			if (retVal2 == 0)
			{
				os << "Repair was successful. Please review frames    " << endl
					<< "in the range " << firstCorruptFieldName << " - " << lastCorruptFieldName << endl
					<< " before retrying the commit";
				_dialogMessage = os.str();
				_dialogType = informationDialog;
				_showDialog = true;
				while (_showDialog)
				{
				  MTImillisleep(50);
				}
			}
			else
			{
				_dialogMessage = "REPAIR FAILED! The commit has been aborted";
				_dialogType = errorDialog;
				_showDialog = true;
				while (_showDialog)
				{
				  MTImillisleep(50);
				}
			}
		}
	}

	if (retVal1 != 0)
	{
		return retVal1;
	}

	return 0;
}
//---------------------------------------------------------------------------

int TCommitVersionForm::CommitMedia(
				const vector<int> &listOfDirtyFrameIndexes,
				const CVideoFrameList *versionFrameList,
				CVideoFrameList *parentFrameList)
{
	 int retVal = 0;
	 CBinManager binManager;

	 retVal = binManager.CommitMedia(listOfDirtyFrameIndexes,
												versionFrameList,
												parentFrameList,
												&mProgressPercent,
												&mFramesRemainingCount);

	 if (retVal != 0)
		  return retVal;

	 return 0;
}
//---------------------------------------------------------------------------

int TCommitVersionForm::DiscardMedia(
            const vector<int> &listOfDirtyFrameIndexes,
            const CVideoFrameList *versionFrameList,
            CVideoFrameList *parentFrameList,
            bool destroyMediaFlag)
{
    int retVal = 0;
    CBinManager binManager;

    retVal = binManager.DiscardDirtyMedia(listOfDirtyFrameIndexes,
                                          versionFrameList,
                                          parentFrameList,
                                          destroyMediaFlag,
                                          &mProgressPercent,
                                          &mFramesRemainingCount);
    if (retVal != 0)
        return retVal;

    return 0;
}
//---------------------------------------------------------------------------

void TCommitVersionForm::UpdateAllSiblingVersionClipHistories(
               ClipSharedPtr &parentClip,
               const CVideoFrameList *parentVideoFrameList,
               const vector<int> &listOfDirtyVideoFrameIndexes,
               int firstVideoFrameIndex, int lastVideoFrameIndex,
               const CVideoFrameList *parentFilmFrameList,
               const vector<int> &listOfDirtyFilmFrameIndexes,
               int firstFilmFrameIndex, int lastFilmFrameIndex
               )
{
    // This only applies to legacy "monolithic" history.
    CSaveRestore saveRestore;
    saveRestore.setClip(parentClip);
    if (saveRestore.getHistoryType() == HISTORY_TYPE_FILE_PER_FRAME)
    {
        return;
    }

    // Get a list of the sibling version clips
    CBinManager binManager;
    vector<ClipVersionNumber> versionNumberList;
    string parentPath = mVersionClipInfo->getBinPath();
    binManager.getListOfClipsActiveVersionNumbers(ClipIdentifier(parentPath), versionNumberList, true);

    // Copy history from the parent to the sibling for each committed
    // frame, but be careful not to touch frames that are dirty in the
    // sibling clip
    for (unsigned int i = 0; i < versionNumberList.size(); ++i)
    {
        int retVal;

        // Skip over the version being committed
        if (versionNumberList[i].ToDisplayName() == mVersionClipInfo->getClipName())
        {
            continue;
        }

		  ClipIdentifier siblingId(parentPath, versionNumberList[i]);
		  TRACE_3(errout << "UPDATE HISTORY FOR SIBLING: " << siblingId);
		  auto siblingClip = binManager.openClip(siblingId, &retVal);
        if (siblingClip != nullptr)
        {
            // Only look at main video
            CVideoProxy *videoProxy = siblingClip->getVideoProxy(VIDEO_SELECT_NORMAL);
            if (videoProxy != NULL)
            {
                CVideoFrameList *videoFrameList =
                         videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
                CVideoFrameList *filmFrameList =
                         videoProxy->getVideoFrameList(FRAMING_SELECT_FILM);

                // Update video frame history
                if (videoFrameList != NULL)
                {
                    UpdateOneSiblingVersionClipHistory(
                                  listOfDirtyVideoFrameIndexes,
                                  firstVideoFrameIndex, lastVideoFrameIndex,
                                  parentVideoFrameList,
                                  videoFrameList,
                                  parentClip,
                                  siblingClip);
                }
                // Update film frame history
                if (filmFrameList != NULL &&
                    filmFrameList != videoFrameList)
                {
                    UpdateOneSiblingVersionClipHistory(
                                  listOfDirtyFilmFrameIndexes,
                                  firstFilmFrameIndex, lastFilmFrameIndex,
                                  parentFilmFrameList,
                                  filmFrameList,
                                  parentClip,
                                  siblingClip);
                }
            }
        }
    }
}
//---------------------------------------------------------------------------

void TCommitVersionForm::UpdateOneSiblingVersionClipHistory(
               const vector<int> &listOfCommittedFrameIndexes,
               int firstFrameIndex, int lastFrameIndex,
               const CVideoFrameList *parentFrameList,
               CVideoFrameList *siblingFrameList,
               ClipSharedPtr &parentClip,
               ClipSharedPtr &siblingClip)
{
    if (parentFrameList == NULL || siblingFrameList == NULL)
        return;

    vector<int> myListOfDirtyFrames;
    vector<int> listOfFrameIndexesToUpdate;

    // We need to exclude the sibling's dirty frames - don't want to
    // hack those!
    FindDirtyFrames(siblingFrameList, firstFrameIndex, lastFrameIndex,
                    /*out*/ myListOfDirtyFrames);

    // Brute force weeding out of frames modified in sibling
    for (unsigned i = 0; i < listOfCommittedFrameIndexes.size(); i++)
    {
        bool ok = true;
        unsigned j;

        for (j = 0; j < myListOfDirtyFrames.size(); ++j)
        {
            if (listOfCommittedFrameIndexes[i] == myListOfDirtyFrames[j])
            {
                vector<int>::iterator iter = myListOfDirtyFrames.begin() + j;
                myListOfDirtyFrames.erase(iter);
                ok = false;
                break;
            }
        }
        if (ok)
            listOfFrameIndexesToUpdate.push_back(
                                             listOfCommittedFrameIndexes[i]);
    }

    // OK! Replace what's left
    ReplaceHistory(listOfFrameIndexesToUpdate,
                   parentFrameList,
                   siblingFrameList,
                   parentClip,
                   siblingClip);   // from, to
}

//---------------------------------------------------------------------------

int TCommitVersionForm::ReplaceHistory(
        const vector<int> &listOfFrameIndexes,
        const CVideoFrameList *sourceFrameList,
        const CVideoFrameList *targetFrameList,
        ClipSharedPtr &sourceClip,
        ClipSharedPtr &targetClip)
{
    int retVal = 0;

    if (!listOfFrameIndexes.empty())
    {
        // Copy the history for modified frames from source clip to target
        CSaveRestore saveRestore;
        retVal = saveRestore.replaceHistory(sourceFrameList->getFrameListFileName(),
                                            targetFrameList->getFrameListFileName(),
                                            sourceClip,
                                            targetClip,
                                            &listOfFrameIndexes,
                                            &mProgressPercent,
                                            &mFramesRemainingCount
                                            );
    }

    return retVal;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TCommitVersionForm::SetupPanelOkButtonClick(
      TObject *Sender)
{
    // Kick off the processing
    Start();
}
//---------------------------------------------------------------------------

void __fastcall TCommitVersionForm::ProgressPanelCloseButtonClick(
      TObject *Sender)
{
    // State is determined by the label CLose Button is "Close" or "Cancel"
    if (ProgressPanelCloseButton->Caption == "Close")
    {
        // Setting ModalResult will hide the dialog
        if (mCancelledFlag)
           ModalResult = mrCancel;
        else if (mHaltedOnErrorFlag)
           ModalResult = mrAbort;
        else
           ModalResult = mrOk;

        AutoCloseTimer->Enabled = false;
    }
    else // Committing is in progress - this is a "cancel" request
    {
        ModalResult = mrNone;  // Don't hide the dialog
        mCancelledFlag = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TCommitVersionForm::ProgressTimerTimer(TObject *Sender)
{
    int oldModifiedFrameCount = -1;

    // Update GUI elements
    if (mProgressPercent != FrameIndexProgressBar->Position)
    {
        FrameIndexProgressBar->Position = mProgressPercent;
    }

    if ( StringToStdString(StatusMessageLabel->Caption) != mStatusMessage)
    {
        StatusMessageLabel->Caption =  String(mStatusMessage.c_str());
    }

    try
    {
        oldModifiedFrameCount = ModifiedFrameCountValue->Caption.ToInt();
    }
    catch (...) {}

    if (mFramesRemainingCount != oldModifiedFrameCount)
    {
        ModifiedFrameCountValue->Caption = AnsiString(mFramesRemainingCount);
        ModifiedFrameCountLabel->Visible = mFramesRemainingCount >= 1;
        ModifiedFrameCountValue->Visible = mFramesRemainingCount >= 1;
	 }

	 // Check if we need to show a popup dialog.
	 if (_showDialog)
	 {
		 // Don't want to be reentered while in the dialog!
		 ProgressTimer->Enabled = false;

		 switch (_dialogType)
		 {
			 case informationDialog:
				 MTIInformationDialog(_dialogMessage);
				 _dialogResponse = MTI_DLG_OK;
				 break;
			 case confirmationDialog:
				 _dialogResponse = MTIConfirmationDialog(_dialogMessage);
				 break;
			 case errorDialog:
				 MTIErrorDialog(_dialogMessage);
				 _dialogResponse = MTI_DLG_OK;
				 break;
		 }

		 _showDialog = false;
		 ProgressTimer->Enabled = true;
	 }

    // Check if we're all done
    if (mProcessingIsCompleteFlag || mCancelledFlag || mHaltedOnErrorFlag)
    {
        ProgressTimer->Enabled = false;
        ProgressPanelCloseButton->Caption = "Close";
        if (mCancelledFlag)
        {
            StatusLabel->Caption = " Cancelled ";
            StatusLabel->Color = clGray;
        }
        else if (mHaltedOnErrorFlag)
        {
            StatusLabel->Caption = "   Error   ";
            StatusLabel->Color = clRed;
        }
        else
        {
            StatusLabel->Caption = " Finished ";
            StatusLabel->Color = clGreen;
            mAutoCloseFlag = false;    // used by AutoCloseTimer to delay
            AutoCloseTimer->Enabled = true;
        }
        StatusLabel->Visible = true;

        // At this point, maybe delete the committed clip.
        if (mOkToDeleteTheCommittedClip && (mVersionClipFate == DELETE_CLIP))
        {
             BinManagerForm->FinishCommittingClip(mVersionClipInfo,
                                                  mParentClipInfo,
                                                  mOpMode == COMMIT_CHANGES);
        }
        else
        {
             // Update the modified count readout
             BinManagerForm->RepaintVersionListView();

             // These lines were copied from the "Discard Changes" code for compatibility
             BinManagerForm->ReloadProxy();
             BinManagerForm->LoadNewVersion(mVersionClipInfo->getClipPath());
        }
    }

    ProgressPanel->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TCommitVersionForm::AutoCloseTimerTimer(TObject *Sender)
{
    if (mAutoCloseFlag)
    {
        AutoCloseTimer->Enabled = false;
        ModalResult = mrOk;
    }
    else
    {
       mAutoCloseFlag = true;
    }
}
//---------------------------------------------------------------------------
