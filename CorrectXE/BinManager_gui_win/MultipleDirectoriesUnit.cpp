//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MultipleDirectoriesUnit.h"

#include "MTIDialogs.h"
#include "UnitPref.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TMultipleDirectoriesForm *MultipleDirectoriesForm;
//---------------------------------------------------------------------------
__fastcall TMultipleDirectoriesForm::TMultipleDirectoriesForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::CheckAllButtonClick(
      TObject *Sender)
{
  int i;
  int iNumToDo = DirectoryCheckListBox->Items->Count;

  for (i = 0; i < iNumToDo; i++)
    DirectoryCheckListBox->Checked[i] = true;

  // Update the OK Button's status
  OkButtonUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::UncheckAllButtonClick(
      TObject *Sender)
{
  int i;
  int iNumToDo = DirectoryCheckListBox->Items->Count;

  for (i = 0; i < iNumToDo; i++)
    DirectoryCheckListBox->Checked[i] = false;

  // Update the OK Button's status
  OkButtonUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::DirectoryCheckListBoxClickCheck(
      TObject *Sender)
{
  // Never allow item to be selected - it just doesn't make sense
  if (DirectoryCheckListBox->ItemIndex != -1)
    DirectoryCheckListBox->Selected[DirectoryCheckListBox->ItemIndex] = false;

  // Update the OK Button's status
  OkButtonUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::DirectoryCheckListBoxClick(
      TObject *Sender)
{
  bool bCurrentState = false;

  // Toggle state of check box if the item was clicked - not just the
  //   check box
  if (DirectoryCheckListBox->ItemIndex != -1)
  {
    bCurrentState = DirectoryCheckListBox->Checked[DirectoryCheckListBox->ItemIndex];
    DirectoryCheckListBox->Checked[DirectoryCheckListBox->ItemIndex] = !bCurrentState;
  }

  DirectoryCheckListBoxClickCheck(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::GoAwayButtonClick(
      TObject *Sender)
{
  if (FormPref != NULL)
  {
    FormPref->SetShowMultipleDirectoriesForm(false);
    _MTIInformationDialog(Handle, "If you would like to reenable this window, then you can do so\n"
            "in the Project Manager's Preferences Page under 'Multiple File Creation'.");
  }
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::SuperClipNameEditChange(
      TObject *Sender)
{
  // Update the OK Button's status
  OkButtonUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::CreateSuperClipCheckBoxClick(
      TObject *Sender)
{
  // Update the OK Button's status
  OkButtonUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::OkButtonClick(TObject *Sender)
{
  int iNumChecked = 0;

  // Override normal mrOK so we can possibly put up a dialog box if
  //   the user had only one clip selected, but had "Create Super Clip"
  //   checked.
  ModalResult = mrNone;

  iNumChecked = GetNumChecked();

  if (iNumChecked == 1)
  {
    string strMessage;
    strMessage = "Can't create Super Clip unless at least two directories\n";
    strMessage += "  are checked.  Press OK to continue with checked clip\n";
    strMessage += "  creation or Cancel to return and select more directories.";

    if (_MTIWarningDialog(Handle, strMessage) == MTI_DLG_OK)
      ModalResult = mrOk;
  }
  else
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TMultipleDirectoriesForm::OkButtonUpdate(void)
{
  // Update the OK button based on the following possibilities:
  //   disabled - if CreateSuperClipCheckBox is checked and no name is in the edit field.
  //   disabled - if no clips checked
  //   enabled  - otherwise
  bool bButtonEnabled = true;

  if (CreateSuperClipCheckBox->Checked)
    if (SuperClipNameEdit->Text == "")
      bButtonEnabled = false;

  int iNumChecked = GetNumChecked();
  if (iNumChecked == 0)
    bButtonEnabled = false;

  OkButton->Enabled = bButtonEnabled;
}

// Return the number of checked items in the CheckListBox
int __fastcall TMultipleDirectoriesForm::GetNumChecked(void)
{
  int i;
  int iNumChecked = 0;
  int iNumToDo    = DirectoryCheckListBox->Items->Count;

  for (i = 0; i < iNumToDo; i++)
  {
    if (DirectoryCheckListBox->Checked[i])
      iNumChecked++;
  }

  return(iNumChecked);
}
