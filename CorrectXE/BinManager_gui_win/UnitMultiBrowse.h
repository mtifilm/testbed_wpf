//---------------------------------------------------------------------------

#ifndef UnitMultiBrowseH
#define UnitMultiBrowseH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <FileCtrl.hpp>

#include "ClipAPI.h"
#include "IniFile.h"  // for StringList
#include "MTIUNIT.h"
#include "NakedProxyUnit.h"
#include "VTimeCodeEdit.h"

typedef vector<ClipSharedPtr> MultiBrowseClipListType;

#include <string>
using std::string;
//---------------------------------------------------------------------------
class TFormMultiBrowse : public TMTIForm
{
__published:	// IDE-managed Components
        TPanel *TopPanel;
        TLabel *BaseFoilderLabel;
        TPanel *MiddlePanel;
        TLabel *BrowseFolderTreeLabel;
        TTreeView *BinTree;
        TPanel *BottomPanel;
        TListView *ListView1;
        TLabel *Label3;
        TCheckBox *CheckBoxCheckHeaders;
        TLabel *LabelChecking;
        TLabel *SelectMultipleLabel;
        TLabel *TakeLongTimeLabel;
        TGroupBox *GroupBox1;
        TRadioButton *UseFileNumbersRadioButton;
        TBitBtn *BitBtnCreate;
        TLabel *Label6;
        TBitBtn *ButtonRemoveSelected;
        TSpeedButton *ButtonSelectAll;
        TBitBtn *ButtonAddSelectedObjects;
        TRadioButton *UseTimecodesRadioButton;
        VTimeCodeEdit *CalcTCEdit;
        TLabel *CustomTimecodeLabel;
        TEdit *CalcFileNumberEdit;
        TLabel *FileLabel;
        TLabel *EqualsLabel;
        TPanel *MessagePanel;
        TCheckBox *FileClipDFCheckBox;
        TLabel *FileClipDFLabel;
        TLabel *FileClipFPSLabel;
        TComboBox *FileClipFPSComboBox;
        TPanel *BrowseBaseDirPanel;
        TComboBox *ComboBoxBaseDir;
        TSpeedButton *BrowseBaseDirButton;
        TPanel *ClipNameTemplatePanel;
        TBitBtn *BitBtnCancel;
        TPanel *FileHeaderInfoPanel;
        TPanel *FileDetailsPanel;
        TLabel *FileDetailsFileNameLabel;
        TLabel *FileDetailsFormatLabel;
        TLabel *FileDetailsDimensionsLabel;
        TLabel *FileDetailsTimecodeLabel;
        TLabel *FileDetailsFileNameValue;
        TLabel *FileDetailsFormatValue;
        TLabel *FileDetailsDimensionsValue;
        TLabel *FileDetailsTimecodeValue;
        TSpeedButton *FileDetailsShowHeaderButton;
        TTrackBar *FileDetailsTrackBar;
        TPanel *FileDetailsProxyPanel;
        TNakedProxyFrame *FileDetailsProxyFrame;
        TLabel *Label2;
        TEdit *ClipNameTemplateEdit;
        TBitBtn *ClipNameTemplateEditButton;
        TBitBtn *CopyTCFromHeaderButton;
        void __fastcall BrowseBaseDirButtonClick(TObject *Sender);
        void __fastcall BinTreeExpanding(TObject *Sender,
          TTreeNode *Node, bool &AllowExpansion);
        void __fastcall ComboBoxBaseDirChange(TObject *Sender);
        void __fastcall ComboBoxBaseDirExit(TObject *Sender);
        void __fastcall ButtonAddSelectedObjectsClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ButtonRemoveSelectedClick(TObject *Sender);
        void __fastcall BinTreeCustomDrawItem(TCustomTreeView *Sender,
          TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);
        void __fastcall ListView1CustomDrawItem(TCustomListView *Sender,
          TListItem *Item, TCustomDrawState State, bool &DefaultDraw);
        void __fastcall BitBtnCreateClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall ButtonSelectAllClick(TObject *Sender);
        void __fastcall ListView1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ClipNameTemplateEditButtonClick(TObject *Sender);
        void __fastcall UseFileNumbersRadioButtonClick(TObject *Sender);
        void __fastcall UseTimecodesRadioButtonClick(TObject *Sender);
        void __fastcall FileDetailsShowHeaderButtonClick(TObject *Sender);
        void __fastcall BinTreeChange(TObject *Sender, TTreeNode *Node);
        void __fastcall FileDetailsTrackBarChange(TObject *Sender);
        void __fastcall CopyTCFromHeaderButtonClick(TObject *Sender);
private:	// User declarations
  string lastClickedBinPath, strClipSchemeName;
  void AllowExpand (TTreeNode *tnpParent);
  void FindAndShowAllChildren (TTreeNode *tnpParent);
  void getPathFromNode(TTreeNode * node, string& path);
  void ParseFileList (StringList &strlFiles, StringList &strlFirstFile,
                StringList &strlLastFile, bool bCalledByAddImageRange);
  void FindAndAddAllFiles (string &path);
  void FindSomeFiles (string &path, StringList &strlFiles);
  void FindChildFiles (string &strPath, StringList &strlFiles,
                       StringList &strlFirstFileList,
                       StringList &strlLastFileList);
  void updateClipName ();
  bool containsDigit (const AnsiString &as);

  string getFileType (const string &strName);
  string CheckClipScheme (const string &strFirstFileName,
        const string &strLastFileName, int &iFrameIn,
        int &iFrameOut);

  void AddNodeToListView (TTreeNode *nodeToAdd);
  void AddToListView (string strPath, StringList &strlFiles,
                bool bCalledByAddImageRange);
  string GenerateAutoClipNameTemplate(const string &folderName,
                                      const string &fileName);
  void FillInClipNameTemplateEdit();

  bool IsValid (TListItem *Item);
  bool IsCreated (TListItem *Item);
  void CreateClip (TListItem *Item);
  string ClipNameTemplateString;
  int ClipNameTemplateNumberOfDigits;

  MultiBrowseClipListType newClipList;

  // File header panel stuff
  void UpdateFileDetailsPanel(const string &fileNameTemplate,
                              int frameIndex=-1);  // if -1, then template is
                                                   // an actual file name
  string sImageFileNameTemplate;
  int iImageFileIndex;
  int iFirstFrameNumber;
  int iLastFrameNumber;
  string strImageFileHeader;
  ProxyDisplayer *FileDisplayer;


public:		// User declarations
        __fastcall TFormMultiBrowse(TComponent* Owner);

  void ReadIniSettings(CIniFile* ini, const string &section);
  void WriteIniSettings(CIniFile* ini, const string &section);

  void setClickedBinPath (const string &newBin);
  void setClipSchemeName (const string &newClipScheme);
  void getNewClipList (MultiBrowseClipListType *&aNewClipList);
  void ClearNewClipList ();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMultiBrowse *FormMultiBrowse;
//---------------------------------------------------------------------------
#endif
