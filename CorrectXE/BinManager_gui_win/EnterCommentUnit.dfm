object EnterCommentForm: TEnterCommentForm
  Left = 742
  Top = 352
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Edit Clip Comments'
  ClientHeight = 100
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object EnterCommentLabel: TLabel
    Left = 20
    Top = 8
    Width = 136
    Height = 13
    Caption = 'Enter a comment for this clip'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object EnterCommentEdit: TEdit
    Left = 20
    Top = 32
    Width = 493
    Height = 21
    TabOrder = 0
    Text = 'EnterCommentEdit'
  end
  object CancelButton: TButton
    Left = 280
    Top = 68
    Width = 75
    Height = 21
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object OkButton: TButton
    Left = 192
    Top = 68
    Width = 75
    Height = 21
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
