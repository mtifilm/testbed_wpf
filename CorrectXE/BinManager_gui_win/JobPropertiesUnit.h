//---------------------------------------------------------------------------

#ifndef JobPropertiesUnitH
#define JobPropertiesUnitH
//---------------------------------------------------------------------------
#include "JobInfo.h"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TJobPropertiesForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *JobNameLabel;
   TEdit *JobNameEdit;
   TLabel *NicknameLabel;
   TEdit *NicknameEdit;
   TLabel *JobNumberLabel;
   TEdit *JobNumberEdit;
   TLabel *ReelCountLabel;
   TEdit *ReelCountEdit;
   TLabel *MediaLocationLabel;
   TEdit *MediaLocationEdit;
   TEdit *UserMediaFolderNameEdit;
   TCheckBox *ASlashBCheckBox;
   TCheckBox *CombinedCheckBox;
   TCheckBox *LastReelAOnlyCheckBox;
   TCheckBox *DrsMediaFolderCheckBox;
   TLabel *JobFoldersLabel;
   TCheckBox *MasksCheckBox;
   TCheckBox *PdlsCheckBox;
   TCheckBox *ReportsCheckBox;
   TCheckBox *CutListsCheckBox;
   TCheckBox *StabilizeCheckBox;
   TCheckBox *DewarpCheckBox;
   TLabel *MediaFoldersLabel;
   TCheckBox *UserMediaFolderCheckBox;
   TButton *OkButton;
   TButton *CancelButton;
   TButton *BrowseForMediaLocationButton;
	TButton *JobFoldersToggleButton;
   void __fastcall UserMediaFolderCheckBoxClick(TObject *Sender);
   void __fastcall BrowseForMediaLocationButtonClick(TObject *Sender);
   void __fastcall ASlashBCheckBoxClick(TObject *Sender);
   void __fastcall ReelCountEditChange(TObject *Sender);
   void __fastcall JobNameEditChange(TObject *Sender);
   void __fastcall MediaLocationEditChange(TObject *Sender);
	void __fastcall JobFoldersToggleButtonClick(TObject *Sender);


private:	// User declarations

public:		// User declarations

   __fastcall TJobPropertiesForm(TComponent* Owner);
   void SetJobInfo(const JobInfo &jobInfo);
   JobInfo GetJobInfo();
};
//---------------------------------------------------------------------------
extern PACKAGE TJobPropertiesForm *JobPropertiesForm;
//---------------------------------------------------------------------------
#endif
