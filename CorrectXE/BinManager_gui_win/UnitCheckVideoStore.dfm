object FormCheckVideoStore: TFormCheckVideoStore
  Left = 264
  Top = 176
  Caption = 'Check VideoStore'
  ClientHeight = 340
  ClientWidth = 826
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LabelConflictsExist: TLabel
    Left = 232
    Top = 316
    Width = 121
    Height = 18
    Caption = 'Conflicts Exist'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  object Panel2: TPanel
    Left = 40
    Top = 16
    Width = 753
    Height = 281
    TabOrder = 3
    Visible = False
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 278
      Height = 25
      Caption = 'Scanning the media . . .'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelTrack: TLabel
      Left = 24
      Top = 64
      Width = 31
      Height = 13
      Caption = 'Track:'
    end
    object LabelTrackName: TLabel
      Left = 72
      Top = 64
      Width = 107
      Height = 13
      Caption = 'Click on Scan to begin'
    end
    object LabelFrames: TLabel
      Left = 24
      Top = 128
      Width = 37
      Height = 13
      Caption = 'Frames:'
    end
    object ProgressBar0: TProgressBar
      Left = 24
      Top = 80
      Width = 705
      Height = 16
      TabOrder = 0
    end
    object ProgressBar1: TProgressBar
      Left = 24
      Top = 144
      Width = 705
      Height = 16
      TabOrder = 1
    end
    object bbHalt: TBitBtn
      Left = 352
      Top = 216
      Width = 75
      Height = 25
      Hint = 'Halt the scanning process'
      Cancel = True
      Caption = 'Halt'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333FFFFF333333000033333388888833333333333F888888FFF333
        000033338811111188333333338833FFF388FF33000033381119999111833333
        38F338888F338FF30000339119933331111833338F388333383338F300003391
        13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
        33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
        33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
        000039183811193333918338F8F833F83333838F000039118111933339118338
        F3833F83333833830000339111193333391833338F33F8333FF838F300003391
        11833338111833338F338FFFF883F83300003339111888811183333338FF3888
        83FF83330000333399111111993333333388FFFFFF8833330000333333999999
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = bbHaltClick
    end
  end
  object Panel1: TPanel
    Left = 40
    Top = 16
    Width = 753
    Height = 281
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 360
      Height = 25
      Caption = 'Video Store Configuration File:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelVideoStoreCfg: TLabel
      Left = 392
      Top = 8
      Width = 48
      Height = 25
      Caption = 'XXX'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListView1: TListView
      Left = 16
      Top = 80
      Width = 564
      Height = 185
      Columns = <
        item
          Caption = 'Video Store Name'
          Width = 250
        end
        item
          Caption = 'Reported Size (in blocks)'
          Width = 130
        end
        item
          Caption = 'Actual Size (in blocks)'
          Width = 130
        end
        item
          Caption = 'Conflict'
        end>
      ColumnClick = False
      HideSelection = False
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView1Click
    end
    object bbRepair1: TBitBtn
      Left = 592
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Repair'
      TabOrder = 1
      OnClick = bbRepair1Click
    end
  end
  object Panel5: TPanel
    Left = 40
    Top = 16
    Width = 753
    Height = 281
    TabOrder = 5
    Visible = False
    object Label5: TLabel
      Left = 16
      Top = 8
      Width = 244
      Height = 25
      Caption = 'Video Store Conflicts'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListView5: TListView
      Left = 16
      Top = 80
      Width = 564
      Height = 185
      Columns = <
        item
          Caption = 'Video Store File'
          Width = 510
        end
        item
          Caption = 'Conflict'
        end>
      ColumnClick = False
      HideSelection = False
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView5Click
    end
    object bbRepair5: TBitBtn
      Left = 592
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Repair'
      TabOrder = 1
      OnClick = bbRepair5Click
    end
  end
  object Panel4: TPanel
    Left = 40
    Top = 16
    Width = 753
    Height = 281
    TabOrder = 4
    Visible = False
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 178
      Height = 25
      Caption = 'Media Conflicts'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelSevereError: TLabel
      Left = 16
      Top = 40
      Width = 300
      Height = 36
      Caption = 'SEVERE ERRORS EXIST'#13#10'Please delete the following media:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListView4: TListView
      Left = 16
      Top = 80
      Width = 564
      Height = 185
      Columns = <
        item
          Caption = 'Media Name'
          Width = 280
        end
        item
          Caption = 'Conflicts with ...'
          Width = 280
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView4Click
    end
  end
  object Panel3: TPanel
    Left = 40
    Top = 16
    Width = 753
    Height = 281
    TabOrder = 6
    Visible = False
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 202
      Height = 25
      Caption = 'Media Corruption'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelMediaCorruption: TLabel
      Left = 16
      Top = 48
      Width = 365
      Height = 18
      Caption = 'The following media could not be opened:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListView3: TListView
      Left = 16
      Top = 80
      Width = 564
      Height = 185
      Columns = <
        item
          Caption = 'Media Name'
          Width = 560
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListView3Click
    end
  end
  object bbClose: TBitBtn
    Left = 136
    Top = 312
    Width = 75
    Height = 25
    Hint = 'Close this form'
    Caption = 'Close'
    Kind = bkClose
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
  end
  object bbNext: TBitBtn
    Left = 40
    Top = 312
    Width = 75
    Height = 25
    Hint = 'Close this form'
    Caption = 'Next  >>'
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = bbNextClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 760
    Top = 24
  end
end
