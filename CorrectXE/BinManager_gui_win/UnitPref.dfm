object FormPref: TFormPref
  Left = 461
  Top = 385
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Bin Manager Preferences'
  ClientHeight = 330
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 538
    Height = 289
    ActivePage = TabSheetFileCreation
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object TabSheetFileCreation: TTabSheet
      Caption = 'Multiple Clip Creation'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ShowMultipleDirectoriesFormCheckBox: TCheckBox
        Left = 48
        Top = 16
        Width = 281
        Height = 17
        Hint = 
          'Allows the window to appear which searches for multiple parallel' +
          ' directories with similar files when a given file is selected. (' +
          'default = checked)'
        Caption = 'Allow Searching for Other Directories with Similar Files'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object RadioGroup1: TRadioGroup
        Left = 48
        Top = 40
        Width = 185
        Height = 57
        Hint = 
          'When searching for other similarly named directories, where to e' +
          'xpect the sequence number to begin (default = End)'
        Caption = ' Other Directory Search Location '
        TabOrder = 1
      end
      object BeginningRadioButton: TRadioButton
        Left = 72
        Top = 56
        Width = 145
        Height = 17
        Hint = 
          'When searching for other similarly named directories, where to e' +
          'xpect the sequence number to begin (default = End)'
        Caption = 'Beginning (e.g. 123_MPR)'
        TabOrder = 2
      end
      object EndRadioButton: TRadioButton
        Left = 72
        Top = 72
        Width = 121
        Height = 17
        Hint = 
          'When searching for other similarly named directories, where to e' +
          'xpect the sequence number to begin (default = End)'
        Caption = 'End (e.g. scene123)'
        TabOrder = 3
      end
    end
    object TabSheetTabOrder: TTabSheet
      Caption = 'Tab Order'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBoxVideo1: TGroupBox
        Left = 360
        Top = 16
        Width = 161
        Height = 161
        Caption = 'Video format tab order (in/out)'
        TabOrder = 0
        Visible = False
        object CheckBoxTabVideoIn1: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'In Point'
          TabOrder = 0
        end
        object CheckBoxTabVideoOut1: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Out Point'
          TabOrder = 1
        end
        object CheckBoxTabVideoDur1: TCheckBox
          Left = 16
          Top = 72
          Width = 97
          Height = 17
          Caption = 'Duration'
          TabOrder = 2
        end
        object CheckBoxTabVideoName1: TCheckBox
          Left = 16
          Top = 96
          Width = 97
          Height = 17
          Caption = 'Name'
          TabOrder = 3
        end
        object CheckBoxTabVideoCreate1: TCheckBox
          Left = 16
          Top = 120
          Width = 97
          Height = 17
          Caption = 'Create Button'
          TabOrder = 4
        end
      end
      object GroupBoxVideo2: TGroupBox
        Left = 184
        Top = 16
        Width = 161
        Height = 161
        Caption = 'Video format tab order (center)'
        TabOrder = 1
        Visible = False
        object CheckBoxTabVideoCen2: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Center Point'
          TabOrder = 0
        end
        object CheckBoxTabVideoDur2: TCheckBox
          Left = 16
          Top = 56
          Width = 97
          Height = 17
          Caption = 'Duration'
          TabOrder = 1
        end
        object CheckBoxTabVideoName2: TCheckBox
          Left = 16
          Top = 88
          Width = 97
          Height = 17
          Caption = 'Name'
          TabOrder = 2
        end
        object CheckBoxTabVideoCreate2: TCheckBox
          Left = 16
          Top = 120
          Width = 97
          Height = 17
          Caption = 'Create Button'
          TabOrder = 3
        end
      end
      object GroupBoxFile: TGroupBox
        Left = 8
        Top = 16
        Width = 161
        Height = 161
        Caption = ' File format tab order '
        TabOrder = 2
        object CheckBoxTabFileIn1: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'First Frame'
          TabOrder = 0
        end
        object CheckBoxTabFileOut1: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Last Frame'
          TabOrder = 1
        end
        object CheckBoxTabFileDur1: TCheckBox
          Left = 16
          Top = 72
          Width = 97
          Height = 17
          Caption = 'Duration'
          TabOrder = 2
        end
        object CheckBoxTabFileName1: TCheckBox
          Left = 16
          Top = 96
          Width = 97
          Height = 17
          Caption = 'Name'
          TabOrder = 3
        end
        object CheckBoxTabFileCreate1: TCheckBox
          Left = 16
          Top = 120
          Width = 97
          Height = 17
          Caption = 'Create Button'
          TabOrder = 4
        end
      end
    end
    object TeranexPage: TTabSheet
      Caption = 'Teranex'
      ImageIndex = 2
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object IPAddrLabel: TLabel
        Left = 40
        Top = 24
        Width = 54
        Height = 13
        Caption = 'IP Address:'
      end
      object IPAddrEdit: TEdit
        Left = 104
        Top = 21
        Width = 121
        Height = 21
        TabOrder = 0
        Text = 'IPAddrEdit'
      end
    end
  end
  object BitBtnOK: TBitBtn
    Left = 196
    Top = 301
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BitBtnOKClick
  end
  object BitBtnCancel: TBitBtn
    Left = 282
    Top = 301
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    NumGlyphs = 2
    TabOrder = 2
    OnClick = BitBtnCancelClick
  end
end
