//---------------------------------------------------------------------------

#ifndef NewBinUnitH
#define NewBinUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TNewBinDialog : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label1;
   TEdit *BinNameEdit;
   TButton *NewBinOkButton;
   TButton *NewBinCancelButton;
private:	// User declarations
public:		// User declarations
   __fastcall TNewBinDialog(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TNewBinDialog *NewBinDialog;
//---------------------------------------------------------------------------
#endif
