//---------------------------------------------------------------------------

#ifndef ClipFileExporterH
#define ClipFileExporterH
//---------------------------------------------------------------------------

#include "ClipIdentifier.h"
//---------------------------------------------------------------------------

class ClipFileExporter
{
public:
   ClipFileExporter();
   ~ClipFileExporter();

   void SetSourceClipId(ClipIdentifier clipId);
   void SetFrameRange(int inFrameIndex, int outFrameIndex);
   void SetTargetDirectory(const string &suggestedTargetDirectory);

   ClipIdentifier GetSourceClipId() const;
   void GetFrameRange(int &inFrameIndex, int &outFrameIndex) const;
   string GetTargetDirectory() const;

   int ExportFiles();

   enum ExportPhase
   {
      IDLE_PHASE,
      EXPORTING_PHASE,
      PAUSED_PHASE,
      COMPLETED_PHASE,
      CANCELED_PHASE,
      ABORTED_PHASE
   };
   ExportPhase GetProgress(int &completedFrames, int &totalFrames) const;

   void Pause();
   void Resume();
   void Cancel();

   int GetCompletionCode() const;
   string GetErrorMessage() const;
   bool OkToNuke() const;

private:
   ClipIdentifier _clipId;
   int _inFrameIndex = -1;
   int _outFrameIndex = -1;
   string _targetDirectory;
   int _completionCode = 0;
   int _completedFrames = 0;
   int _totalFrames = 0;
   bool _pauseFlag = false;
   bool _cancelFlag = false;
   string _errorMessage;
   ExportPhase _exportPhase = IDLE_PHASE;
};
//---------------------------------------------------------------------------
#endif
