//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitMultiBrowse.h"

#include "BinDir.h"
#include "BinManager.h"
#include "ClipNameTemplateUnit.h"
#include "ImageFileMediaAccess.h"
#include "MediaFileIO.h"
#include "FileSweeper.h"
#include "HeaderDumpUnit.h"
//#include "MediaIO.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "ProxyDisplayer.h"
#include "ShowModalDialog.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NakedProxyUnit"
#pragma resource "*.dfm"
TFormMultiBrowse *FormMultiBrowse;
//---------------------------------------------------------------------------

#define MAX_HISTORY_COUNT 10
#define MULTI_BROWSE_FORM "MultiBrowseForm"
#define CHECK_ALL_HEADERS "CheckAllHeaders"
#define USE_TIMECODES "UseTimecodes"
#define BASE_DIR "BaseDir"
#define CLIP_NAME_TEMPLATE_STRING "ClipNameTemplateString"
#define CLIP_NAME_TEMPLATE_NUMBER_OF_DIGITS "ClipNameTemplateNumberOfDigits"
#define ARROW_SEPARATOR " --> "
#define NO_TIMECODE_DISPLAY "-- : -- : -- : --"
#define NO_DIMENSIONS_DISPLAY "-- x --"
#define NO_STRING_DISPLAY "--"

#define ITEM_CLIP_NAME       Caption
#define ITEM_STATUS          SubItems->Strings[0]
#define ITEM_FIRST_FILE_NAME SubItems->Strings[1]
#define ITEM_LAST_FILE_NAME  SubItems->Strings[2]
#define ITEM_FIRST_TIMECODE  SubItems->Strings[3]
#define ITEM_LAST_TIMECODE   SubItems->Strings[4]
#define ITEM_FILE_TYPE       SubItems->Strings[5]
#define ITEM_FOLDER          SubItems->Strings[6]
#define ITEM_FIRST_FRAME     SubItems->Strings[7]   // Invisible
#define ITEM_LAST_FRAME      SubItems->Strings[8]   // Invisible
#define ITEM_TIMECODE_OPT    SubItems->Strings[9]   // Invisible
//---------------------------------------------------------------------------

__fastcall TFormMultiBrowse::TFormMultiBrowse(TComponent* Owner)
        : TMTIForm(Owner)
        , ClipNameTemplateNumberOfDigits(0)    // Not set yet
{
  FileDisplayer = new ProxyDisplayer;
  if (FileDisplayer)
   {
	FileDisplayer->SetDisplayContext((HDC)Handle);
   }
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::ReadIniSettings(CIniFile* ini, const string &section)
{
  string localSection = section + "." + MULTI_BROWSE_FORM;

  bool bDefault = CheckBoxCheckHeaders->Checked;
  CheckBoxCheckHeaders->Checked = ini->ReadBool(localSection,
                                                CHECK_ALL_HEADERS,
                                                bDefault);

  // Clip Name Template stuff
  if (ClipNameTemplateForm == NULL)
  {
      ClipNameTemplateForm = new TClipNameTemplateForm(this);
  }
  string strDefault = ClipNameTemplateForm->GetSelectedTemplate();
  ClipNameTemplateString = ini->ReadString(localSection,
                                        CLIP_NAME_TEMPLATE_STRING,
                                        strDefault);
  long lDefault = ClipNameTemplateForm->GetNumberOfDigits();
  ClipNameTemplateNumberOfDigits = ini->ReadInteger(localSection,
                                        CLIP_NAME_TEMPLATE_NUMBER_OF_DIGITS,
                                        lDefault);
  FillInClipNameTemplateEdit();

  // Timecode stuff
  bDefault = UseTimecodesRadioButton->Checked;
  bool bTimecodes = ini->ReadBool(localSection, USE_TIMECODES, bDefault);
  if (bTimecodes)
   {
    UseTimecodesRadioButton->Checked = true;
    UseTimecodesRadioButtonClick(this);
   }
  else
   {
    UseFileNumbersRadioButton->Checked = true;
    UseFileNumbersRadioButtonClick(this);
   }

#if 0
  bDefault = HeaderTimecodesRadioButton->Checked
            || BothTimecodesRadioButton->Checked;
  bool useHeaderTimecodes = ini->ReadBool(localSection,
                                          USE_HEADER_TIMECODES,
                                          bDefault);
  bDefault = FrameNumberTimecodesRadioButton->Checked
            || BothTimecodesRadioButton->Checked;
  bool useFrameNumberTimecodes = ini->ReadBool(localSection,
                                               USE_FRAME_NUMBER_TIMECODES,
                                               bDefault);
  int index = (useFrameNumberTimecodes?2:0) +
              (useHeaderTimecodes?1:0);
  switch(index)
    {
      case 0:
        NoTimecodesRadioButton->Checked = true;
        break;
      case 1:
        HeaderTimecodesRadioButton->Checked = true;
        break;
      case 2:
        FrameNumberTimecodesRadioButton->Checked = true;
        break;
      case 3:
        BothTimecodesRadioButton->Checked = true;
        break;
    }
#endif
  ComboBoxBaseDir->Items->Clear();
  ComboBoxBaseDir->DropDownCount = MAX_HISTORY_COUNT;
  for (int i = 0; i < MAX_HISTORY_COUNT; ++i)
   {
    MTIostringstream os;
    os << BASE_DIR << "[" << i << "]";
    string baseDir = ini->ReadString(localSection, os.str(), "");
    if (!baseDir.empty())
     {
      ComboBoxBaseDir->Items->Add(baseDir.c_str());
     }
   }
  ComboBoxBaseDir->ItemIndex = 0;
  ComboBoxBaseDirChange(this);
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::WriteIniSettings(CIniFile* ini, const string &section)
{
  string localSection = section + "." + MULTI_BROWSE_FORM;
  int i;

  ini->WriteBool(localSection, CHECK_ALL_HEADERS,
                               CheckBoxCheckHeaders->Checked);

  // Clip Name Template stuff
  if (!ClipNameTemplateString.empty() && (ClipNameTemplateNumberOfDigits > 0))
  {
      ini->WriteString(localSection, CLIP_NAME_TEMPLATE_STRING,
                                     ClipNameTemplateString);
      ini->ReadInteger(localSection, CLIP_NAME_TEMPLATE_NUMBER_OF_DIGITS,
                                     ClipNameTemplateNumberOfDigits);
  }

  ini->WriteBool(localSection, USE_TIMECODES,
                               UseTimecodesRadioButton->Checked);

#if 0
  ini->WriteBool(localSection, USE_HEADER_TIMECODES,
                               HeaderTimecodesRadioButton->Checked
                                 || BothTimecodesRadioButton->Checked);
  ini->WriteBool(localSection, USE_FRAME_NUMBER_TIMECODES,
                               FrameNumberTimecodesRadioButton->Checked
                                 || BothTimecodesRadioButton->Checked);
#endif

  int count = ComboBoxBaseDir->Items->Count;
  if (count > MAX_HISTORY_COUNT) count = MAX_HISTORY_COUNT;
  for (i = 0; i < count; ++i)
   {
    AnsiString item = ComboBoxBaseDir->Items->Strings[i];
    MTIostringstream os;
    string baseDir = item.c_str();
    if (baseDir.empty())
      break;
    os << BASE_DIR << "[" << i << "]";
    ini->WriteString(localSection, os.str(), baseDir);
   }
  for (; i < count; ++i)
   {
    MTIostringstream os;
    os << BASE_DIR << "[" << i << "]";
    ini->DeleteKey(localSection, os.str());
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::BrowseBaseDirButtonClick(TObject *Sender)
{
  String Dir = "";
  Application->NormalizeAllTopMosts();
  SelectDirectory("Choose a base directory ", WideString(""), Dir);
  Application->RestoreTopMosts();

  if (Dir != "")
   {
    ComboBoxBaseDir->Text = Dir;  // added instead of the above stuff
    ComboBoxBaseDirChange (ComboBoxBaseDir);
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::BinTreeExpanding(TObject *Sender,
      TTreeNode *Node, bool &AllowExpansion)
{
  // attempting to expand this node.  Examine the disk and fill in
  // all directories and files found.

  FindAndShowAllChildren (Node);


  // tells UI the requested expansion is allowed.
  AllowExpansion = true;

}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ComboBoxBaseDirChange(TObject *Sender)
{
  TTreeNode *newNode;
  AnsiString newBaseDir = ComboBoxBaseDir->Text;

  // Reset the bin tree to this directory.
  BinTree->Items->Clear(); // remove any existing nodes

  // Add a root node
  newNode = BinTree->Items->Add(NULL, newBaseDir);

  // Since this is a bin, allow it to expand
  AllowExpand (newNode);

  // Expnad the top level so we don't show just one node
  newNode->Expanded = true;

  // Add it to the history (or pop it to the top if already there)
  int index = ComboBoxBaseDir->Items->IndexOf(newBaseDir);

  if (index > 0)
   { // Don't duplicate entries
    ComboBoxBaseDir->Items->Delete(index);
    index = -1;
   }
  if (index < 0)
   { // Remove excess entries, if any
    while (ComboBoxBaseDir->Items->Count >= MAX_HISTORY_COUNT)
     {
      ComboBoxBaseDir->Items->Delete(MAX_HISTORY_COUNT - 1);
     }
    // Add entry to the top of the list
    ComboBoxBaseDir->Items->Insert(0, newBaseDir);
   }
  ComboBoxBaseDir->ItemIndex = 0;
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::AllowExpand (TTreeNode *tnpParent)
{
  // clear the children from current node
  tnpParent->DeleteChildren();

  // add a dummy entry, so the parent can expand
  BinTree->Items->AddChild (tnpParent, "DUMMY");
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::FindAndShowAllChildren (TTreeNode *tnpParent)
{
  string strCurrentPath;
  TTreeNode *newNode;
  CBinDir binDir;
  bool foundBin, foundFile;

  // clear all children from current node
  tnpParent->DeleteChildren();

  // get name of current path
  strCurrentPath = "";
  getPathFromNode (tnpParent, strCurrentPath);

  // find all sub-directories of the current path
  foundBin = binDir.findFirst(strCurrentPath, BIN_SEARCH_FILTER_ANY_DIR);
  StringList strlBin;
  while (foundBin)
   {
    // alphabetize the list of files
	unsigned int iCnt = 0;
    while (iCnt < strlBin.size() && binDir.fileName > strlBin.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlBin.size())
     {
	  // add a new entry
      strlBin.push_back (binDir.fileName);
     }
	else
	 {	  // insert it
	  strlBin.insert(strlBin.begin() + iCnt, binDir.fileName);
	 }

    // Go on to the next Bin at this level
    foundBin = binDir.findNext();
   }

  // enter these bins into the tree
  for (unsigned int iCnt = 0; iCnt < strlBin.size(); iCnt++)
   {
    // Add tree node
    newNode = BinTree->Items->AddChild(tnpParent, strlBin.at(iCnt).c_str());

    // since this is a bin, allow it to expand
    AllowExpand (newNode);
   }


  // find all files in the current directory
  foundFile = binDir.findFirst(strCurrentPath, BIN_SEARCH_FILTER_FILE);
  StringList strlFiles;
  while (foundFile)
   {
    // alphabetize the list of files
    unsigned int iCnt = 0;
    while (iCnt < strlFiles.size() && binDir.fileName > strlFiles.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlFiles.size())
     {
      // add a new entry
      strlFiles.push_back (binDir.fileName);
     }
    else
	 {
        strlFiles.insert(strlFiles.begin() + iCnt, binDir.fileName);
	 }
	// Go on to the next File at this level
    foundFile = binDir.findNext();
   }

  StringList strlFirstFile, strlLastFile;
  ParseFileList (strlFiles, strlFirstFile, strlLastFile, false);

  for (unsigned int iCnt = 0; iCnt < strlFirstFile.size(); iCnt++)
   {
    string strTmp = strlFirstFile.at(iCnt);
    if (strlFirstFile.at(iCnt) != strlLastFile.at(iCnt))
     {
      strTmp += ARROW_SEPARATOR;
      strTmp += strlLastFile.at(iCnt);
     }
    newNode = BinTree->Items->AddChild(tnpParent, strTmp.c_str());
   }

}
//---------------------------------------------------------------------------

void TFormMultiBrowse::FindChildFiles (string &strPath,
                                       StringList &strlFiles,
                                       StringList &strlFirstFileList,
                                       StringList &strlLastFileList
)
{
  CBinDir binDir;
  bool foundBin, foundFile;

  // find all sub-directories of the current path
  foundBin = binDir.findFirst(strPath, BIN_SEARCH_FILTER_ANY_DIR);
  StringList strlBin;
  while (foundBin)
   {
    // alphabetize the list of files
    unsigned int iCnt = 0;
    while (iCnt < strlBin.size() && binDir.fileName > strlBin.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlBin.size())
     {
      // add a new entry
      strlBin.push_back (binDir.fileName);
     }
    else
     {
      // insert it
	  strlBin.insert (strlBin.begin() + iCnt, binDir.fileName);
     }

    // Go on to the next Bin at this level
    foundBin = binDir.findNext();
   }

  // find all files in the current directory
  foundFile = binDir.findFirst(strPath, BIN_SEARCH_FILTER_FILE);
  while (foundFile)
   {
    // alphabetize the list of files
    unsigned int iCnt = 0;
    while (iCnt < strlFiles.size() && binDir.fileName > strlFiles.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlFiles.size())
     {
      // add a new entry
      strlFiles.push_back (binDir.fileName);
     }
    else
     {
      // insert it
	  strlFiles.insert (strlFiles.begin() + iCnt, binDir.fileName);
     }

    // Go on to the next File at this level
    foundFile = binDir.findNext();
   }

  StringList strlFirstFile, strlLastFile;
  ParseFileList (strlFiles, strlFirstFile, strlLastFile, false);
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::ParseFileList (StringList &strlFiles,
		StringList &strlFirstFile, StringList &strlLastFile,
                bool bCalledByAddImageRange)
{
  int iCurr, iNext;

  strlFirstFile.clear();
  strlLastFile.clear();

  if (strlFiles.size() == 0)
   {
    return;
   }
  else if (strlFiles.size() == 1)
   {
    strlFirstFile.push_back(strlFiles.at(0));
    strlLastFile.push_back(strlFiles.at(0));
    return;
   }

  // strlFiles is already alphabetized.  Starting at the beginning of
  // the list, search for sequences of names

  for (unsigned int iFile = 0; iFile < strlFiles.size()-1; iFile++)
   {
    string strCurr, strNext;

    strCurr = strlFiles.at(iFile);
    strNext = strlFiles.at(iFile+1);


    // starting at beginning find first non-match.
    const char *cpCurr0, *cpNext0;
    cpCurr0 = strCurr.c_str();
    cpNext0 = strNext.c_str();

    bool bInSequence = false;

    // strings must have same length
    if (strlen(cpCurr0) == strlen(cpNext0))
     {
      // starting at the end, find first non-match
      const char *cpCurr1, *cpNext1;
      cpCurr1 = cpCurr0 + strlen(cpCurr0);
      cpNext1 = cpNext0 + strlen(cpNext0);

      // starting at beginning, find first non-match
      while (*cpCurr0 == *cpNext0 && *cpCurr0 != '\0' && *cpNext0 != '\0')
       {
        cpCurr0++;
        cpNext0++;
       }

      // back up to the first digit
      while (cpCurr0 != strCurr.c_str() && cpCurr0[-1] >= '0' &&
                cpCurr0[-1] <= '9')
       {
        cpCurr0--;
        cpNext0--;
       }

      // starting at end, find first non-match
      while (cpCurr1[-1] == cpNext1[-1] && cpCurr1 != cpCurr0 &&
		cpNext1 != cpNext0)
       {
        cpCurr1--;
        cpNext1--;
       }

      // the difference must be all digits to use
      const char *cpC, *cpN;
      cpC = cpCurr0;
      cpN = cpNext0;
      bool bAllDigits = true;
      if (cpCurr1 == cpCurr0 || cpNext1 == cpNext0)
       {
        bAllDigits = false;
       }
      else
       {
        while (cpC < cpCurr1 && cpN < cpNext1)
         {
          if (cpC[0] < '0' || cpC[0] > '9' || cpN[0] < '0' || cpN[0] > '9')
           {
            bAllDigits = false;
           }
          cpC++;
          cpN++;
         }
       }

      // if these strings are all digits, read them in.
      if (bAllDigits)
       {
        sscanf (cpCurr0, "%d", &iCurr);
        sscanf (cpNext0, "%d", &iNext);
        if (iCurr+1 == iNext)
         {
          // the sequence does increment by 1
          bInSequence = true;
         }
       }
      else
       {
        // not all digits, so can't determine frame count
        iCurr = -1;
        iNext = -1;
       }
     }


    // if this is being called by add image range, force
    // sequence to true
    if (bCalledByAddImageRange)
      bInSequence = true;

    if (iFile == 0)
     {
      strlFirstFile.push_back(strCurr);
      strlLastFile.push_back(strCurr);
     }

    if (bInSequence == false)
     {
      strlFirstFile.push_back (strNext);
      strlLastFile.push_back (strNext);
     }
    else
     {
      strlLastFile.pop_back();
      strlLastFile.push_back (strNext);
     }
   }
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::getPathFromNode(TTreeNode * node, string& path)
{
   TTreeNode *parent = node->Parent;

   if (parent)
      {
      // Recurse to get the rest of the path
      getPathFromNode(parent, path);
      path = AddDirSeparator(path);
	  path += StringToStdString(node->Text);
      }
   else
      {
	  path = StringToStdString(node->Text);
      }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ComboBoxBaseDirExit(TObject *Sender)
{
  // wtf ComboBoxBaseDir->Items->Add (ComboBoxBaseDir->Text);
  // Moved to ComboBoxBaseDirChange()
}
//---------------------------------------------------------------------------


void __fastcall TFormMultiBrowse::ButtonAddSelectedObjectsClick(TObject *Sender)
{
  CBusyCursor Busy(true);

  for (int i = 0; i < BinTree->Items->Count; i++)
   {
    if (BinTree->Items->Item[i]->Selected)
     {
      // If this is a directory, expand it
      if (BinTree->Items->Item[i]->HasChildren)
       {
        string strPath;
        strPath = "";
        getPathFromNode (BinTree->Items->Item[i], strPath);
        FindAndAddAllFiles (strPath);
       }
      else
       {
#if 0 // refactored
        // leaf nodes must have a digit
        if (containsDigit (BinTree->Items->Item[i]->Text))
         {
          string strPath;
          strPath = "";
          getPathFromNode (BinTree->Items->Item[i]->Parent, strPath);

          string strTmp = BinTree->Items->Item[i]->Text.c_str();

          StringList strlFiles;
          char caFirstFileName[256], caLastFileName[256];
          if (strTmp.find(ARROW_SEPARATOR, 0) == string::npos)
           {
            // just a single entry
            sscanf (strTmp.c_str(), "%s", caFirstFileName);
            strlFiles.push_back (caFirstFileName);
           }
          else
           {
            sscanf (strTmp.c_str(), "%s --> %s",
                        caFirstFileName, caLastFileName);
            strlFiles.push_back (caFirstFileName);
            strlFiles.push_back (caLastFileName);
           }

          AddToListView (strPath, strlFiles, true);
         }
#else
        AddNodeToListView(BinTree->Items->Item[i]);
#endif
       }
     }
   }


  updateClipName();

  LabelChecking->ITEM_CLIP_NAME = "";
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::FormCreate(TObject *Sender)
{
  BinTree->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::FormShow(TObject *Sender)
{
  ListView1->Items->Clear();
  ClearNewClipList ();
  LabelChecking->ITEM_CLIP_NAME = "";

	TMTIForm::FormShow(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ButtonRemoveSelectedClick(TObject *Sender)
{
   TListItem *selectedItem = ListView1->Selected;
   TItemStates selected = TItemStates() << isSelected;

   // For each selected item, get the clip name and put it in the
   // caller's list of selected clips
   while (selectedItem != 0)
      {
      TListItem *next = ListView1->GetNextItem(selectedItem, sdAll, selected);
      selectedItem->Delete();
      selectedItem = next;
      }

  updateClipName();
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::FindAndAddAllFiles (string &path)
{
  TTreeNode *newNode;
  CBinDir binDir;
  bool foundBin, foundFile;

  // find all sub-directories of the current path
  foundBin = binDir.findFirst(path, BIN_SEARCH_FILTER_ANY_DIR);
  StringList strlBin;
  while (foundBin)
   {
    // alphabetize the list of files
    unsigned int iCnt = 0;
    while (iCnt < strlBin.size() && binDir.fileName > strlBin.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlBin.size())
     {
      // add a new entry
		TRACE_3(errout << "push_back FOLDER " << binDir.fileName);
		strlBin.push_back (AddDirSeparator(path) + binDir.fileName);
	  }
    else
     {
      // insert it
	  TRACE_3(errout << "insert FOLDER " << binDir.fileName << " @ " << iCnt);
	  strlBin.insert (strlBin.begin() + iCnt, AddDirSeparator(path) +
                binDir.fileName);
     }

    // Go on to the next Bin at this level
    foundBin = binDir.findNext();
   }

  // search the sub directories
  for (unsigned int iCnt = 0; iCnt < strlBin.size(); iCnt++)
   {
    FindAndAddAllFiles (strlBin.at(iCnt));
   }


  // find all files in the current directory}
  foundFile = binDir.findFirst(path, BIN_SEARCH_FILTER_FILE);
  StringList strlFiles;
  while (foundFile)
   {
    // alphabetize the list of files
    unsigned int iCnt = 0;
    while (iCnt < strlFiles.size() && binDir.fileName > strlFiles.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlFiles.size())
     {
      // add a new entry
		TRACE_3(errout << "push_back FILE " << binDir.fileName);
		strlFiles.push_back (binDir.fileName);
     }
    else
     {
      // insert it
	  TRACE_3(errout << "insert FILE " << binDir.fileName << " @ " << iCnt);
	  strlFiles.insert (strlFiles.begin() + iCnt, binDir.fileName);
     }

    // Go on to the next File at this level
    foundFile = binDir.findNext();
   }

  // Fill in the list view
  AddToListView (path, strlFiles, false);
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::AddNodeToListView (TTreeNode *nodeToAdd)
{
  // leaf nodes must have a digit
  if (containsDigit (nodeToAdd->Text))
   {
    string strPath;
    strPath = "";
    getPathFromNode (nodeToAdd->Parent, strPath);

    string strTmp = StringToStdString(nodeToAdd->Text);

    StringList strlFiles;
    char caFirstFileName[256], caLastFileName[256];
    if (strTmp.find(ARROW_SEPARATOR, 0) == string::npos)
     {
      // just a single entry
      sscanf (strTmp.c_str(), "%s", caFirstFileName);
      strlFiles.push_back (caFirstFileName);
     }
    else
     {
      string scanFormat = string("%s") + ARROW_SEPARATOR + "%s";
      sscanf (strTmp.c_str(), scanFormat.c_str(), caFirstFileName,
                                                  caLastFileName);
      strlFiles.push_back (caFirstFileName);
      strlFiles.push_back (caLastFileName);
     }

    AddToListView (strPath, strlFiles, true);
   }
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::AddToListView (string strPath,
        StringList &strlFiles, bool bCalledByAddImageRange)
{
  StringList strlFirstFile, strlLastFile;
  ParseFileList (strlFiles, strlFirstFile, strlLastFile,
        bCalledByAddImageRange);

  for (unsigned int iCnt = 0; iCnt < strlFirstFile.size(); iCnt++)
   {
    // leaf nodes must have a digit
    if (containsDigit (strlFirstFile.at(iCnt).c_str()))
     {
      TListItem *lip = ListView1->Items->Add();
      string strFirstFileName = AddDirSeparator (strPath) + strlFirstFile.at(iCnt);
      string strLastFileName = AddDirSeparator (strPath) + strlLastFile.at(iCnt);

      // ORDER OF THESE MUST MATCH THE #define ITEM_XXXX 's ABOVE

      // the clip name
      lip->Caption = " ";

      // status
      int iFirstFrame, iLastFrame;
      lip->SubItems->Add ( CheckClipScheme (strFirstFileName, strLastFileName,
                                            iFirstFrame, iLastFrame).c_str());

      // the first file name
      lip->SubItems->Add(strlFirstFile.at(iCnt).c_str());

      // the last file name
      lip->SubItems->Add(strlLastFile.at(iCnt).c_str());

      // the timecodes
      CTimecode firstTC(iFirstFrame);
      CTimecode lastTC(iLastFrame);
      bool suckTimecodeFromFirstFile = false;
      bool useManualCalculator = false;

      if (UseFileNumbersRadioButton->Checked)
       {
        // do nothing
       }
#if 0 // removed two of the options - was too confusing
      else if (HeaderTimecodesRadioButton->Checked)
       {
        suckTimecodeFromFirstFile = true;
       }
      else if (FrameNumberTimecodesRadioButton->Checked)
       {
        useManualCalculator = false;
       }
#endif
      else // UseTimecodesRadioButton
       {
        suckTimecodeFromFirstFile = true;
        useManualCalculator = true;
       }

      int iTimecodeOpt = 0;
      if (useManualCalculator)
       {
        int offset = 0;
        int flags = FileClipDFCheckBox->Checked? DROPFRAME : 0;
        MTIistringstream fpss;
        int fps;

        fpss.str(StringToStdString(FileClipFPSComboBox->Text));
        fpss >> fps;
        CTimecode tempTC(flags, fps);
        iTimecodeOpt = fps + (flags? 100 : 0); // e.g 30 DF = 130

        try {
          offset = iFirstFrame - CalcFileNumberEdit->Text.ToInt();
        } catch (...) {}

		tempTC.setTimeASCII(StringToStdString(CalcTCEdit->Text).c_str());

        firstTC = tempTC + offset;
        lastTC = tempTC + (offset + (iLastFrame - iFirstFrame));
       }
      
      string timecodeString;
      firstTC.getTimeString(timecodeString);
      lip->SubItems->Add(timecodeString.c_str());
      lastTC.getTimeString(timecodeString);
      lip->SubItems->Add(timecodeString.c_str());

      // file type
      lip->SubItems->Add (getFileType (strFirstFileName).c_str());

      // the folder
      lip->SubItems->Add(strPath.c_str());

      // not visible, but useful
      char caTmp[32];
      sprintf (caTmp, "%d", iFirstFrame);
      lip->SubItems->Add (caTmp);
      sprintf (caTmp, "%d", iLastFrame);
      lip->SubItems->Add (caTmp);
      sprintf (caTmp, "%d", iTimecodeOpt);
      lip->SubItems->Add (caTmp);
     }
   }
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::setClickedBinPath (const string &newBin)
{
  lastClickedBinPath = newBin;
}
//---------------------------------------------------------------------------

string TFormMultiBrowse::getFileType (const string &strName)
{
  int iRet;
  string strType = "???";

#if 0 // QQQ xxxmfioxxx
  CImageFileIO ifIO;
  int iType;

  iRet = ifIO.InitFromExistingFile(strName);
  if (iRet == 0)
   {
    iType = ifIO.getFileType ();
    if (iType & FILE_TYPE_DPX)
     {
      strType = "DPX";
     }
    else if (iType & FILE_TYPE_TIFF)
     {
      strType = "TIFF";
     }
    else if (iType & FILE_TYPE_EXR)
     {
      strType = "EXR";
     }
   }
#endif // xxxmfioxxx

  return strType;
}
//---------------------------------------------------------------------------

string TFormMultiBrowse::CheckClipScheme (const string &strFirstFileName,
        const string &strLastFileName, int &iFrameIn, int &iFrameOut)
{
  string strCompatible;
  CClipInitInfo clipInitInfo;

  iFrameIn = -1;
  iFrameOut = -1;

  // Set the clip init info from the contents of the clip initialization file
  int retVal = clipInitInfo.initFromClipScheme(strClipSchemeName);
  if (retVal != 0)
    {
     strCompatible = "Scheme Error";
     return strCompatible;
   }

  // make file name into a template
  string strImageFileNameTemplate;
  strImageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(strFirstFileName,
                                                          false);

  // extract the first frame number and last frame number
  const char *cpTemplate, *cpFirst, *cpLast;
  cpTemplate = strImageFileNameTemplate.c_str();
  cpFirst = strFirstFileName.c_str();
  cpLast = strLastFileName.c_str();

  while (cpTemplate[0] != '#' && cpTemplate[0] != 0)
   {
    cpTemplate++;
    cpFirst++;
    cpLast++;
   }

  sscanf (cpFirst, "%d", &iFrameIn);
  sscanf (cpLast, "%d", &iFrameOut);

  // Attempt to initialize the Clip and Main Video Init Info
  // from the header of the Image File
  bool bCheckHeaders = CheckBoxCheckHeaders->Checked;

  if (bCheckHeaders)
   {
    string strTmp;
    strTmp = "Checking: " + strFirstFileName + " to " + strLastFileName;
    LabelChecking->Caption = strTmp.c_str();
   }

  retVal = clipInitInfo.initFromImageFile2(strImageFileNameTemplate,
        iFrameIn, iFrameOut+1, bCheckHeaders, strClipSchemeName, true);
  if (retVal == CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME)
   {
    strCompatible = "Incompatible";
   }
//  else if (retVal == FILE_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED)
//   {
//    strCompatible = "Unlicensed resolution";
//   }
  else if (retVal == FILE_ERROR_UNKNOWN_FILE_TYPE)
   {
    strCompatible = "Unknown file type";
   }
  else if (retVal == CLIP_ERROR_IMAGE_FILE_MISSING)
   {
    strCompatible = "File missing";
   }
  else if (retVal != 0)
   {
    MTIostringstream os;
    os << "Error code:  "  << retVal;

    strCompatible = os.str();
   }
  else
   {
    strCompatible = "Ready to create";
   }


  return strCompatible;
}
//---------------------------------------------------------------------------

string TFormMultiBrowse::GenerateAutoClipNameTemplate(
                              const string &folderName,
                              const string &fileName)
{
  string strTemp = ClipNameTemplateString;
  string stringToReplace;
  string newSubString;
  string::size_type pos;

  // Folder name
  stringToReplace = ClipNameTemplateForm->FolderNameTemplateString;
  // QQQ abuse of 'filesweeper' API - just want complete last component
  newSubString = GetFileName(folderName) + GetFileExt(folderName);
  pos = strTemp.find(stringToReplace);
  if (pos != string::npos)
   {
    strTemp.replace(pos, stringToReplace.length(), newSubString);
   }

  // File name -- chop off the sequence number and anything beyond that
  stringToReplace = ClipNameTemplateForm->FileNameTemplateString;
  newSubString = CImageFileMediaAccess::MakeTemplate(fileName, false);
  pos = newSubString.find('#');
  if (pos != string::npos)
   {
    newSubString.erase(pos);
   }
  pos = strTemp.find(stringToReplace);
  if (pos != string::npos)
   {
    strTemp.replace(pos, stringToReplace.length(), newSubString);
   }

  // Sequence number
  stringToReplace = ClipNameTemplateForm->SequenceNumberTemplateString;
  // make printf-style format string
  MTIostringstream ostr;
  ostr << "%." << ClipNameTemplateNumberOfDigits << "d";
  newSubString = ostr.str();
  pos = strTemp.find(stringToReplace);

  // Sanitize here, before we do the actual replacement for the sequence
  // number because that replacement string includes a '.' which we do
  // not want to sanitize out.
  string badChars = ".\\/:*?\"<>|#%' ";
  string::size_type badCharPos;
  while (string::npos != (badCharPos = strTemp.find_first_of(badChars)))
   {
    strTemp.replace(badCharPos, 1, "_");
   }

  // Continue replacing the sequence number template string
  if (pos != string::npos)
   {
    strTemp.replace(pos, stringToReplace.length(), newSubString);
   }

  // Reduce runs of underscores to a single one
  while (string::npos != (pos = strTemp.find("__")))
   {
    strTemp.replace(pos, 2, "_");
   }

  return strTemp;
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::updateClipName ()
{
  // This method was rewritten to fix bug 2317: clip names disappearing

  CBinManager binMgr;
  int iIndexOffset = 0;

  for (int i = 0; i < ListView1->Items->Count; i++)
   {
    TListItem *lip = ListView1->Items->Item[i];
    AnsiString clipName = lip->Caption.Trim();

    if (IsValid(lip))
     {
	  string autoClipNameTemplate = GenerateAutoClipNameTemplate(
										   StringToStdString(lip->ITEM_FOLDER),
										   StringToStdString(lip->ITEM_FIRST_FILE_NAME));

      string newClipName = binMgr.makeNextAutoClipName(lastClickedBinPath,
													   autoClipNameTemplate,
                                                       &iIndexOffset);
      iIndexOffset++;
      clipName = newClipName.c_str();
     }
    if (clipName.IsEmpty())
     {
	  clipName = " ";
     }
    lip->Caption = clipName;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::BinTreeCustomDrawItem(
      TCustomTreeView *Sender, TTreeNode *Node, TCustomDrawState State,
      bool &DefaultDraw)
{
  Sender->Canvas->Font->Color = clBlack;

  if (!Node->HasChildren)
   {
	// leaf nodes must contain at least 1 digit
	if (!containsDigit(Node->Text))
     {
      Sender->Canvas->Font->Color = clRed;
     }
   }
}
//---------------------------------------------------------------------------

bool TFormMultiBrowse::containsDigit (const AnsiString &as)
{
  bool bRet = true;

  char *cp = as.c_str();
  while ((cp[0] != '\0') && (cp[0] < '0' || cp[0] > '9'))
   {
    cp++;
   }
  if (cp[0] == '\0')
   {
    bRet = false;
   }

  return bRet;
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::setClipSchemeName (const string &newScheme)
{
  strClipSchemeName = newScheme;
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ListView1CustomDrawItem(
      TCustomListView *Sender, TListItem *Item, TCustomDrawState State,
      bool &DefaultDraw)
{

  if (IsCreated(Item))
   {
    Sender->Canvas->Font->Color = clDkGray;
    Sender->Canvas->Brush->Color = clWhite;
   }
  else if (IsValid(Item))
   {
    Sender->Canvas->Font->Color = clBlack;
    Sender->Canvas->Brush->Color = clMoneyGreen;
   }
  else
   {
    Sender->Canvas->Font->Color = clBlack;
    Sender->Canvas->Brush->Color = (TColor) 0xc0c0ff;   // light red
   }

}
//---------------------------------------------------------------------------

bool TFormMultiBrowse::IsValid (TListItem *Item)
{
  bool bRetVal;
  
  if (Item->ITEM_STATUS == "Ready to create")
   {
    bRetVal = true;
   }
  else
   {
    bRetVal = false;
   }

  return bRetVal;
}
//---------------------------------------------------------------------------

bool TFormMultiBrowse::IsCreated (TListItem *Item)
{
  bool bRetVal;
  
  if (Item->ITEM_STATUS == "Created")
   {
    bRetVal = true;
   }
  else
   {
    bRetVal = false;
   }

  return bRetVal;
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::BitBtnCreateClick(TObject *Sender)
{
  // create the clips
  for (int iCnt = 0; iCnt < ListView1->Items->Count; iCnt++)
   {
    TListItem *lip = ListView1->Items->Item[iCnt];

    if (IsValid (lip)==true && IsCreated (lip)==false)
     {
      // this is a valid item, so create a clip
      CreateClip (lip);

      // update the proposed clip names, since some creating a
      // clip can sometimes fail

      updateClipName ();

      ListView1->Invalidate();
     }
   }
}
//---------------------------------------------------------------------------


void TFormMultiBrowse::CreateClip (TListItem *lip)
{
  int retVal;

  // make file name into a template
  string strImageFileNameTemplate;
  strImageFileNameTemplate = AddDirSeparator (StringToStdString(lip->ITEM_FOLDER)) +
				StringToStdString(lip->ITEM_FIRST_FILE_NAME);
  strImageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(
                strImageFileNameTemplate, false);

  // get the first and last frames
  int iFrameIn=-1, iFrameOut=-1, iTimecodeOpt=0;;
  sscanf (StringToStdString(lip->ITEM_FIRST_FRAME).c_str(), "%d", &iFrameIn);
  sscanf (StringToStdString(lip->ITEM_LAST_FRAME).c_str(), "%d", &iFrameOut);
  sscanf (StringToStdString(lip->ITEM_TIMECODE_OPT).c_str(), "%d", &iTimecodeOpt);

  int iTCFlags = (iTimecodeOpt >= 100)? DROPFRAME : 0;
  int iFPS = (iTimecodeOpt % 100);
  CTimecode firstFrameTimecode(iTCFlags, iFPS);
  firstFrameTimecode.setTimeASCII(StringToStdString(lip->ITEM_FIRST_TIMECODE).c_str());

  // initialize the clipInitInfo
  CClipInitInfo *clipInitInfo = new CClipInitInfo;

  retVal = clipInitInfo->initFromClipScheme(strClipSchemeName);
  if (retVal != 0)
  {
     TRACE_0(errout << "ERROR: Cannot parse Clip Scheme File " << strClipSchemeName << "   Return Code: " << retVal);
     // Non-fatal, but cuts won't work
  }

  retVal = clipInitInfo->initFromImageFile3(strImageFileNameTemplate,
                                            iFrameIn, iFrameOut+1, false,
                                            strClipSchemeName,
                                            false,  // don't use header TC
                                            firstFrameTimecode);
   if (retVal)
    {
     lip->ITEM_STATUS = "Initialization error";
     delete clipInitInfo;
     return;
    }

   // Set the Bin Path from the Bin selected in the Bin Tree
   clipInitInfo->setBinPath(lastClickedBinPath);

   // Set the Clip Name
   clipInitInfo->setClipName(StringToStdString(lip->ITEM_CLIP_NAME));

   // Create the new clip
   CBinManager binMgr;
   auto clip = binMgr.createClip(*clipInitInfo, &retVal);
   if (retVal != 0)
    {
     MTIostringstream os;
     switch (retVal)
      {
      case CLIP_ERROR_INVALID_TIME_CODE :
      case CLIP_ERROR_BAD_HANDLE_TIMECODE :
         os <<  "In/Out error";
         break;
      case CLIP_ERROR_INVALID_CLIP_NAME :
         os <<  "Clip name error";
         break;
      case CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS :
         os <<  "Clip char error";
         break;
      case CLIP_ERROR_CLIP_ALREADY_EXISTS :
         os <<  "Clip dupl error";
         break;
      case CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE :
         os <<  "Disk space error";
         break;
      case CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED :
         os <<  "License error";
         break;
      default:
         os << "Error: " << retVal;
      }


     lip->ITEM_STATUS = os.str().c_str();

     delete clipInitInfo;
     return;
    }

   lip->ITEM_STATUS = "Created";

   delete clipInitInfo;

   newClipList.push_back (clip);
   return;
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::getNewClipList (MultiBrowseClipListType *&aNewClipList)
{
  aNewClipList = &newClipList;
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::ClearNewClipList ()
{
  CBinManager binManager;

  for (int i = 0; i < (int) newClipList.size(); i++)
   {
    auto clip = newClipList.at(i);
    binManager.closeClip(clip);
   }
  newClipList.clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::FormDestroy(TObject *Sender)
{
  ClearNewClipList ();
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ButtonSelectAllClick(TObject *Sender)
{
  if (ListView1->Items->Count == 0)
    return;

  for (int iCnt = 0; iCnt < ListView1->Items->Count; iCnt++)
   {
    ListView1->Items->Item[iCnt]->Selected = true;
   }

  ListView1->Items->Item[0]->Focused = true;
  ListView1->SetFocus();

}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ListView1KeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == VK_DELETE)
   {
    ButtonRemoveSelectedClick (NULL);
   }

  if (Key == 0x41 && Shift.Contains (ssCtrl))
   {
    // control-A means select all
    ButtonSelectAllClick (NULL);
   }


}
//---------------------------------------------------------------------------
void TFormMultiBrowse::FillInClipNameTemplateEdit()
{
  MTIostringstream ostr;
  string strTemp = ClipNameTemplateString;
  string stringToReplace = ClipNameTemplateForm->SequenceNumberTemplateString;

  ostr << std::setw(ClipNameTemplateNumberOfDigits) << std::setfill('#') << "";
  string::size_type pos = strTemp.find(stringToReplace);
  if (pos != string::npos)
   {
    strTemp.replace(pos, stringToReplace.length(), ostr.str());
   }
  ClipNameTemplateEdit->Text = strTemp.c_str();
}

//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::ClipNameTemplateEditButtonClick(
      TObject *Sender)
{
    if (ClipNameTemplateForm == NULL)
    {
        ClipNameTemplateForm = new TClipNameTemplateForm(this);
    }
    ClipNameTemplateForm->SetSelectedTemplate(ClipNameTemplateString);
    ClipNameTemplateForm->SetNumberOfDigits(ClipNameTemplateNumberOfDigits);

    ShowModalDialog(ClipNameTemplateForm);

    if (ClipNameTemplateForm->ModalResult == mrOk)
    {
        ClipNameTemplateString = ClipNameTemplateForm->GetSelectedTemplate();
        ClipNameTemplateNumberOfDigits =
                                   ClipNameTemplateForm->GetNumberOfDigits();
        FillInClipNameTemplateEdit();
    }

}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::UseFileNumbersRadioButtonClick(
      TObject *Sender)
{
    FileLabel->Enabled = false;
    CalcFileNumberEdit->Enabled = false;
    CalcFileNumberEdit->Color = clBtnFace;
    EqualsLabel->Enabled = false;
    CustomTimecodeLabel->Enabled = false;
    CalcTCEdit->Enabled = false;
    CalcTCEdit->Color = clBtnFace;
    FileClipDFLabel->Enabled = false;
    FileClipDFCheckBox->Enabled = false;
    FileClipDFCheckBox->Color = clBtnFace;
    FileClipFPSLabel->Enabled = false;
    FileClipFPSComboBox->Enabled = false;
    FileClipFPSComboBox->Color = clBtnFace;
    CopyTCFromHeaderButton->Enabled = false;

}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::UseTimecodesRadioButtonClick(
      TObject *Sender)
{
    FileLabel->Enabled = true;
    CalcFileNumberEdit->Enabled = true;
    CalcFileNumberEdit->Color = clWindow;
    EqualsLabel->Enabled = true;
    CustomTimecodeLabel->Enabled = true;
    CalcTCEdit->Enabled = true;
    CalcTCEdit->Color = clWindow;
    FileClipDFLabel->Enabled = true;
    FileClipDFCheckBox->Enabled = true;
    FileClipDFCheckBox->Color = clWindow;
    FileClipFPSLabel->Enabled = true;
    FileClipFPSComboBox->Enabled = true;
    FileClipFPSComboBox->Color = clWindow;
    if (FileDetailsTimecodeValue->Caption != NO_TIMECODE_DISPLAY) {
        CopyTCFromHeaderButton->Enabled = true;
    }
}
//---------------------------------------------------------------------------
#define NULL_VALUE -1000

// Stolen from the bin manager
void TFormMultiBrowse::UpdateFileDetailsPanel(const string &fileNameTemplate,
															int frameIndex)
{
	// Clear details in case template is ""
	FileDetailsFileNameValue->Caption   = "--";
	FileDetailsFormatValue->Caption     = "--";
	FileDetailsDimensionsValue->Caption = "--";
	FileDetailsTimecodeValue->Caption   = "--:--:--:--";

	if (fileNameTemplate.empty())
	{
		return;
	}

	int retVal;
	string filePath, fileName;
	char tmpDrive[_MAX_DRIVE];
	char tmpDir[_MAX_DIR];
	char tmpName[_MAX_FNAME];
	char tmpExt[_MAX_EXT];

	if (frameIndex >= 0)
	{
		CImageFileMediaAccess::GenerateFileName(fileNameTemplate, filePath, frameIndex);
	}
	else
	{
		filePath = fileNameTemplate;
	}

	_splitpath(filePath.c_str(), tmpDrive, tmpDir, tmpName, tmpExt);

	fileName                          = string(tmpName) + string(tmpExt);
	FileDetailsFileNameValue->Caption = fileName.c_str();

	MediaFrameBufferSharedPtr frameBuffer;
	MediaFileIO mediaFileIO;
	retVal = mediaFileIO.readFrameBuffer(filePath, frameBuffer);
	if (retVal != 0)
	{
		MTIostringstream errorLabelStream;
		errorLabelStream << "ERROR (" << retVal << ")";
		FileDetailsFormatValue->Caption = errorLabelStream.str().c_str();
		return;
	}

	CImageFormat imageFormat = frameBuffer->getImageFormat();

	MTIostringstream formatValueStream;
	formatValueStream << CImageInfo::queryImageFormatTypeName(imageFormat.getImageFormatType()) << " ";
	formatValueStream << (((unsigned int) imageFormat.getBitsPerComponent()) & 0xFF) << "-bit ";
	formatValueStream << CImageInfo::queryPixelComponentsName(imageFormat.getPixelComponents()) << " ";
	formatValueStream << CImageInfo::queryColorSpaceName(imageFormat.getColorSpace());

	FileDetailsFormatValue->Caption = formatValueStream.str().c_str();

	MTIostringstream dimensionsValueStream;
	dimensionsValueStream << imageFormat.getPixelsPerLine() << " x " << imageFormat.getLinesPerFrame();
	FileDetailsDimensionsValue->Caption = dimensionsValueStream.str().c_str();

	CTimecode tc = frameBuffer->getTimecode();
	if (tc == CTimecode::NOT_SET || tc == 0)
	{
		tc = CTimecode(frameIndex);
	}

	string strTC;
	tc.getTimeString(strTC);
	FileDetailsTimecodeValue->Caption = strTC.c_str();

	retVal = mediaFileIO.dumpFileHeaderToString(filePath, strImageFileHeader);

	if (retVal == 0)
	{
		FileDetailsShowHeaderButton->Enabled = true;
		if (HeaderDumpForm == 0)
		{
			HeaderDumpForm = new THeaderDumpForm(this);
		}

		if (HeaderDumpForm != 0)
		{
			string title                           = fileName + " Image File Header";
			HeaderDumpForm->Caption                = title.c_str();
			HeaderDumpForm->FilePathLabel->Caption = filePath.c_str();
			HeaderDumpForm->Set(strImageFileHeader);
		}
	}
	else
	{
		if (HeaderDumpForm != 0)
		{
			HeaderDumpForm->Hide();
		}

		strImageFileHeader = "";
		FileDetailsShowHeaderButton->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::FileDetailsShowHeaderButtonClick(
      TObject *Sender)
{
    if (!strImageFileHeader.empty() && (HeaderDumpForm != 0)) {
        // Bring up the "dump header panel"
        HeaderDumpForm->Set(strImageFileHeader);
        HeaderDumpForm->Show();
    }
}
//---------------------------------------------------------------------------

void TFormMultiBrowse::FindSomeFiles (string &path, StringList &strlFiles)
{
  TTreeNode *newNode;
  CBinDir binDir;
  bool foundBin, foundFile;

  // Breadth-first search for some image files. Returns a list of all the
  // non-directory files in the first directory we encounter that has any
  // in strlFiles. If the files were found in a subdirectory, path is
  // changed to point to that subdirectory

  strlFiles.clear();

  // find all files in the current directory
  foundFile = binDir.findFirst(path, BIN_SEARCH_FILTER_FILE);
  while (foundFile)
   {
    // alphabetize the list of files
    // Insertion sort?!? wow
    unsigned int iCnt = 0;
    while (iCnt < strlFiles.size() && binDir.fileName > strlFiles.at(iCnt))
     {
      iCnt++;
     }

    if (iCnt == strlFiles.size())
     {
      // add a new entry
      strlFiles.push_back (binDir.fileName);
     }
    else
     {
      // insert it
      strlFiles.insert (strlFiles.begin() + iCnt, binDir.fileName);
     }

    // Go on to the next File at this level
    foundFile = binDir.findNext();
   }

  if (!foundFile)
   {
    // find all sub-directories of the current path
    foundBin = binDir.findFirst(path, BIN_SEARCH_FILTER_ANY_DIR);
    StringList strlBin;
    while (foundBin && !foundFile)
     {
      // alphabetize the list of files
      unsigned int iCnt = 0;
      while (iCnt < strlBin.size() && binDir.fileName > strlBin.at(iCnt))
       {
        iCnt++;
       }

      if (iCnt == strlBin.size())
       {
        // add a new entry
        strlBin.push_back (AddDirSeparator(path) + binDir.fileName);
       }
      else
       {
        // insert it
		strlBin.insert (strlBin.begin() + iCnt, AddDirSeparator(path) +
                  binDir.fileName);
       }

      // Go on to the next Bin at this level
      foundBin = binDir.findNext();
     }

    // search the sub directories
    for (unsigned int iCnt = 0; iCnt < strlBin.size(); iCnt++)
     {
      path = strlBin.at(iCnt);
      FindSomeFiles (path, strlFiles);
      if (!strlFiles.empty())
       {
        foundFile = true;
        break;
       }
     }
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::BinTreeChange(TObject *Sender,
      TTreeNode *Node)
{
  string strPath;
  getPathFromNode (Node, strPath);
  StringList strlFiles;

  if (DoesDirectoryExist (strPath))
   {
    FindSomeFiles (strPath, strlFiles);    // may change strPath
   }
  else
   {
    string strFileName = GetFileName(strPath) + GetFileExt(strPath);
    strPath = GetFilePath(strPath);
    strlFiles.push_back (strFileName);
   }

  for (unsigned i = 0; i < strlFiles.size(); i++)
   {
	string strFirstFileName = strlFiles.at(i);
    string strLastFileName = strFirstFileName;

    // Look for arrow, break up into first & last file name
	string::size_type pos = strFirstFileName.find(ARROW_SEPARATOR);
    if (pos != string::npos)
     {
      int iArrowLen = string (ARROW_SEPARATOR).length();
      strLastFileName = strFirstFileName.substr (pos + iArrowLen);
      strFirstFileName.erase (pos);
     }

    // valid leaf nodes must have a digit
    if (containsDigit (strFirstFileName.c_str()))
     {
      string strFirstFilePath = AddDirSeparator(strPath) + strFirstFileName;
	  string strLastFilePath = AddDirSeparator(strPath) + strLastFileName;
      string strFileTemplate = CImageFileMediaAccess::MakeTemplate(
                                          strFirstFilePath, false); // #s OK
      iFirstFrameNumber = CImageFileMediaAccess::getFrameNumberFromFileName2(
                                          strFileTemplate,  strFirstFilePath);
      iLastFrameNumber = CImageFileMediaAccess::getFrameNumberFromFileName2(
										  strFileTemplate,  strLastFilePath);

      // Show details for the file
	  UpdateFileDetailsPanel (strFirstFilePath);

      // first frame proxy
      sImageFileNameTemplate = strFileTemplate;
      iImageFileIndex = iFirstFrameNumber;
      FileDisplayer->SetSourceName(strFileTemplate);
      FileDetailsProxyFrame->Reset();
	  FileDetailsProxyFrame->ShowFrame(FileDisplayer, iFirstFrameNumber);
      FileDetailsTrackBar->Position = 0;
	  if (iFirstFrameNumber ==  iLastFrameNumber)
		FileDetailsTrackBar->Enabled = false;
	  else if (!FileDetailsTrackBar->Enabled)
		FileDetailsTrackBar->Enabled = true;

      break;   // got one
     }

    // Not a valid file... try another one
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::FileDetailsTrackBarChange(
      TObject *Sender)
{
   TTrackBar *trackBar = (TTrackBar *)Sender;
   int iRange = iLastFrameNumber - iFirstFrameNumber;
   int iNewFileIndex = iFirstFrameNumber +
						   (trackBar->Position * (iRange - 1)) /
												  trackBar->Max;
   if (iNewFileIndex != iImageFileIndex) {
       iImageFileIndex = iNewFileIndex;
       UpdateFileDetailsPanel(sImageFileNameTemplate, iImageFileIndex);
       CTimecode timecode(iImageFileIndex);
       FileDetailsProxyFrame->ShowFrame(FileDisplayer, timecode);
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormMultiBrowse::CopyTCFromHeaderButtonClick(TObject *Sender)
{
    // Ha ha, copy the text!
    CalcTCEdit->tcP.setFlags(0);
    CalcTCEdit->tcP.setFramesPerSecond(100);
    if (FileDetailsTimecodeValue->Caption == NO_TIMECODE_DISPLAY) {
        CalcTCEdit->tcP = "01:00:00:00";
    }
    else {
        CalcTCEdit->tcP = StringToStdString(FileDetailsTimecodeValue->Caption);
    }
    CalcFileNumberEdit->Text = iImageFileIndex;
}
//---------------------------------------------------------------------------

