//---------------------------------------------------------------------------

#ifndef FixCountingUnitH
#define FixCountingUnitH
//---------------------------------------------------------------------------
#include "IniFile.h" // for StringList... uggggh.
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TCountFixesUnit : public TForm
{
__published:	// IDE-managed Components
   TLabel *ScanningStatusLabel;
   TProgressBar *ScanningProgressBar;
   TLabel *NumberOfFixesCaptionLabel;
   TLabel *NumberOfFixesValueLabel;
   TButton *Button1;
   TTimer *ProgressTimer;
   void __fastcall FormShow(TObject *Sender);
   void __fastcall FormHide(TObject *Sender);
   void __fastcall ProgressTimerTimer(TObject *Sender);
private:	// User declarations
   string binPath;
   StringList clipNames;
   string currentClipName;
   int clipsScanned;
   int currentClipPercent;
   int totalNumberOfFixes;
   int currentClipNumberOfFixes;
   int currentSequenceNumber;
   int showProgressDelay;
   int hideProgressDelay;
   static void CountingThreadTrampoline(void* parameter, void* reserved);
   void CountingThread(int mySequenceNumber);
public:		// User declarations
   __fastcall TCountFixesUnit(TComponent* Owner);
   void SetBinPath(const string &newBinPath);
   void SetClipNameList(StringList clipNames);
};
//---------------------------------------------------------------------------
extern PACKAGE TCountFixesUnit *CountFixesUnit;
//---------------------------------------------------------------------------
#endif
