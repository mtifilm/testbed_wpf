//---------------------------------------------------------------------------
//Autodiff , Kea Johnston, August 6, 2004
//What: A class that does diff file creation at the request of a gui or another program that
//      intercepts user input.
//      Required params: Filename of the clip to diff.
//      Output: A diff file in the clip directory of the requested clip.
//Why: This is a replacement for dfstat which has fewer user-options. Power users will still want to use dfstat.
//      Also, dfstat requires the use of a command prompt. In order to integrate it with the gui, it is required
//      that all of the parameters be set by default. This necessitated the rewriting of the class in a more modular fashion.
//      Unlike dfstat, autodiff can't stand alone. It needs a frontend to pass it data.
//How:  Methods that should be called seperately:
//      -Constructor takes a string filename for the clip to be diffed.
//      -Calling calculateDiff creates the diff file. it is equivalent to running dfstat with the -d -A parameters.
//      -Calling affectTimeline allows the program to write tick marks to the timeline from the
//        diff file. It is equivalent to running dfstat with the -t parameter with a threshold of .02
//      -All methods return a bool, true if successfull, and false if not. These should be used to throw
//      and catch exceptions, etc.
//--------------------------------------------------------------------------

#pragma hdrstop

#include "AutoDiff.h"
#include "bthread.h"
#include "MetaData.h"
#include "ClipAPI.h"
#include "FrameDiff.h"
#include "mti_event.h"
#include "TheError.h"
#include "err_bin_manager.h"
#include "MTImalloc.h"
#include "FileSweeper.h"
#include "MTIstringstream.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
// QQQ Magic constants
#define NUM_FRAME 4
#define PERCENTILE .5

//---------------------------------------------------------------------------

CAutoDiff::CAutoDiff(const string &aClipPath)
: m_clipPath(aClipPath)
, m_threshold(0.02)            // QQQ magic number
, m_frameList(nullptr)
, m_clip(nullptr)
, m_fractionDone(0.)
, m_numCutsFound(0)
{
}

CAutoDiff::~CAutoDiff()
{
   if (m_clip != nullptr)
   {
      CBinManager binManager;
      binManager.closeClip(m_clip);
   }
}

//---------------------------------------------------------------------------

int CAutoDiff::init()
{
   CAutoErrorReporter autoErr("CAutoDiff::init",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   int retVal;
   CBinManager binManager;
   string binName, clipName;

   // Reset
   m_threadStatus = IDLE;
   m_whatIAmDoingNow = "Initializing...";
   m_fractionDone = 0;
   m_numCutsFound = 0;
   m_cancel = false;
   m_backupDiffsFilePath = "";

   if (m_clip == nullptr)
   {
      //Open the clip
      retVal = binManager.splitClipFileName(m_clipPath, binName, clipName);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Can't parse clip name " << m_clipPath;
         return retVal;
      }
      m_clip = binManager.openClip(binName, clipName, &retVal);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Can't open clip " << m_clipPath;
         return retVal;
      }

      //Look up the main video framelist from the clip just once
      m_frameList = m_clip->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                              FRAMING_SELECT_VIDEO);
      if (m_frameList == nullptr)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Unable to get framelist from clip " << m_clipPath;
         return retVal;
      }

      // Look up total frame count just once
      m_totalFrameCount = m_frameList->getTotalFrameCount();

      // Compute diffs file path
      // QQQ magic string '.diff'
      m_diffsFilePath = AddDirSeparator(AddDirSeparator(binName) + clipName)
                                        + clipName + ".diff";
   }

   return 0;
}

namespace {
   string makeBackupFileName(const string &fileName)
   {
      char tmpStr[64];
      time_t ltime;
      time(&ltime);
      sprintf(tmpStr, "%llx", ltime);

      return fileName + "." + tmpStr + ".BAK";
   }
} // anonymous namespace

//------------------------------------------------------------------------------
//method tells the framediff class to calculate differences and creates a diff file
//based on results. Swiped from dfstat.
//-----------------------------------------------------------------------------

int CAutoDiff::createDiffsFile()
{
   CAutoErrorReporter autoErr("CAutoDiff::createDiffsFile",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   // Progress stuff
   m_whatIAmDoingNow = "Creating frame diffs file";
   m_fractionDone = 0.;

   m_threadStatus = BUSY;
   m_errorCode = 0;
   m_errorMsg = "";
   m_cancel = false;

   // Back up the existing file  in case user cancel in the middle of creating
   // a new one
   m_backupDiffsFilePath = makeBackupFileName(m_diffsFilePath);
   if ( 0 == MoveFile(m_diffsFilePath.c_str(), m_backupDiffsFilePath.c_str()) )
   {
      // Rename failed - assume nothing to back up
      m_backupDiffsFilePath = "";
   }

   // Spawn worker thread
   void * threadid = BThreadSpawn(createDiffsFile_trampoline, this);
   if (threadid == nullptr)
   {
      autoErr.errorCode = ERR_BIN_MANAGER_BTHREAD_SPAWN;
      autoErr.msg << "Can't spawn thread";
      return ERR_BIN_MANAGER_BTHREAD_SPAWN;
   }

   // Immediate return
   return 0;
}

// Static method: bridge to the worker code
void CAutoDiff::createDiffsFile_trampoline(void *appData, void *threadid)
{
   CAutoErrorReporter autoErr("CAutoDiff::createDiffsFile",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   CAutoDiff *_this = reinterpret_cast<CAutoDiff *>(appData);
   BThreadBegin(threadid);
   try
   {
     _this->CreateDiffsFile_internal();
   }
   catch(...)
   {
   }
   if (_this->m_cancel)
   {
      _this->m_threadStatus = CANCELLED;
      _this->m_whatIAmDoingNow = "Operation cancelled by user";
   }
   else if (_this->m_errorCode != 0)
   {
      autoErr.errorCode = _this->m_errorCode;
      autoErr.msg << _this->m_errorMsg;
      _this->m_threadStatus = FAILED;
   }
   else
   {
      _this->m_threadStatus = SUCCEEDED;
   }

   // Restore backed-up file or delete it
   if (!_this->m_backupDiffsFilePath.empty())
   {
      int retVal;
      if (_this->m_threadStatus == SUCCEEDED)
      {
          retVal = DeleteFile(_this->m_backupDiffsFilePath.c_str());
      }
      else
      {
         DeleteFile(_this->m_diffsFilePath.c_str());
         retVal = MoveFile(_this->m_backupDiffsFilePath.c_str(),
                           _this->m_diffsFilePath.c_str());
      }
      if (retVal == 0)
      {
         TRACE_0(errout << "CAutoDiff::createDiffsFile: "
                        << ((_this->m_threadStatus == SUCCEEDED)? "Delete"
                                                                : "Restore")
                        << " backup diffs file FAILED");
      }
      _this->m_backupDiffsFilePath = "";
   }


   // thread ends automatically
}

// This is where the actual work gets done to create the diffs file
int CAutoDiff::CreateDiffsFile_internal()
{
   CAutoErrorReporter autoErr("CAutoDiff::createDiffsFile",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   long record, frame, counti, field, recordCount;
   MTI_UINT8 *frameBuffer[2][2];   // QQQ hard-coded max fields = 2
   int retVal = 0;
   CFrameDiff frameDiff(m_frameList->getImageFormat());
   int fieldCount = m_frameList->getImageFormat()->getFieldCount();

   CMetaData metaData(m_diffsFilePath);

   m_fractionDone = 0.;

   //fire up the metadata diff file.

   retVal = metaData.Initialize(TYPE_DIFF, m_totalFrameCount, nullptr, nullptr);
   if (retVal != 0)
   {
      m_errorMsg = "Can't create diffs file";
      m_errorCode = retVal;
      return retVal;
   }
   // Select the diff section of the diff file
   retVal = metaData.SelectSection(TYPE_DIFF);
   if (retVal != 0)
   {
      m_errorMsg = "Can't format diffs file";
      m_errorCode = retVal;
      return retVal;
   }
   // Read in data from the type_diff section of the metadata file
   retVal = metaData.ReadWholeSection();
   if (retVal != 0)
   {
      m_errorMsg = "Can't read diffs file record";
      m_errorCode = retVal;
      return retVal;
   }

   // Initialize pointers to nullptr
   for(frame = 0; frame < 2; frame++)
   {
      for(field= 0; field < fieldCount; field++)
         frameBuffer[frame][field] = nullptr;
   }

   // Allocate buffer space
   int bytesPerField = m_frameList->getMaxPaddedFieldByteCount();
   for (frame = 0; frame < 2; frame++)
   {
      for (field = 0; field < fieldCount; field++)
      {
         frameBuffer[frame][field] = (MTI_UINT8 *)
            MTImemalign(MTI_MEMALIGN_DEFAULT, bytesPerField);
         if (frameBuffer[frame][field] == nullptr) {
            m_errorMsg = "Out of memory";
            m_errorCode = ERR_BIN_MANAGER_OUT_OF_MEMORY;
            return ERR_BIN_MANAGER_OUT_OF_MEMORY;
         }
      }
   }
   //-----------------------------------------------------------------------
   //Now calculate the frame differences

   recordCount = metaData.GetNRecord();
   for (record = 0; record < recordCount; record++)
   {
      MTIostringstream os;
      os << "Creating frame diffs file  ( " << (record + 1) << " / "
                                            << recordCount << " )";
      m_whatIAmDoingNow = os.str();

      // Check if cancelled by user
      if (m_cancel)
         break;

      // The following reads the desired frames into the allocated space;
      // we ping-pong between the two sets of frame buffers via the magic
      // of the % operator (note special case frame 0)
      MTI_UINT8 **currentBuffer = frameBuffer[record % 2];
      MTI_UINT8 **previousBuffer = (record > 0)? frameBuffer[(record - 1) % 2]
                                                : nullptr;

      retVal = m_frameList->readMediaAllFieldsOptimized(record, currentBuffer);
      if (retVal != 0)
      {
         MTIostringstream os;
         os <<  "Can't read frame " << record;
         m_errorMsg = os.str();
         m_errorCode = retVal;
         break;
      }
      retVal = frameDiff.SubSampleDiff(currentBuffer, previousBuffer,
                                       &metaData, record);
      if (retVal)
      {
         MTIostringstream os;
         os << "Subsampling error on frame " << record;
         m_errorMsg = os.str();
         m_errorCode = retVal;
         break;
      }
      m_fractionDone = (float) record / recordCount;
   }
   // If successful, write records to the file
   if (m_errorCode == 0)
   {
      retVal = metaData.WriteWholeSection();
      if (retVal != 0)
      {
         m_errorMsg = "Can't write diff file";
         m_errorCode = retVal;
      }
   }

   // Clean up
   for (frame = 0; frame < 2; frame++)
      for (field = 0; field < fieldCount; field++)
         MTIfree(frameBuffer[frame][field]);

   return retVal;

   // Old Kea comment:
   //Man, I am a wizard. A true star.

}

//---------------------------------------------------------------------------
//method calls the interpretation method do cuts for each timeline.
//---------------------------------------------------------------------------

int CAutoDiff::updateCuts(float aThreshold)
{
   CAutoErrorReporter autoErr("CAutoDiff::updateCuts",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   // Progress stuff
   MTIostringstream os;
   os << "Preparing to update scene cuts";
   m_whatIAmDoingNow = os.str();
   m_fractionDone = 0.;
   m_numCutsFound = 0;

   // Thread control stuff
   m_threadStatus = BUSY;
   m_errorCode = 0;
   m_errorMsg = "";
   m_cancel = false;

   // Thread parameter
   m_threshold = aThreshold;

   void * threadid = BThreadSpawn(updateCuts_trampoline, this);
   if (threadid == nullptr)
   {
      autoErr.errorCode = ERR_BIN_MANAGER_BTHREAD_SPAWN;
      autoErr.msg << "Can't spawn thread for updating cuts";
      return ERR_BIN_MANAGER_BTHREAD_SPAWN;
   }
   return 0;
}

// Static method: bridge to worker method
void CAutoDiff::updateCuts_trampoline(void *appData, void *threadid)
{
   CAutoErrorReporter autoErr("CAutoDiff::updateCuts",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   CAutoDiff *_this = reinterpret_cast<CAutoDiff *>(appData);
   BThreadBegin(threadid);
   try
   {
     _this->updateCuts_internal();
   }
   catch(...)
   {
   }
   if (_this->m_cancel)
   {
      _this->m_threadStatus = CANCELLED;
      _this->m_whatIAmDoingNow = "Operation cancelled by user";
   }
   else if (_this->m_errorCode != 0)
   {
      autoErr.errorCode = _this->m_errorCode;
      autoErr.msg << _this->m_errorMsg;
      _this->m_threadStatus = FAILED;
   }
   else
   {
      _this->m_threadStatus = SUCCEEDED;
   }
   // thread ends automatically
}

int CAutoDiff::updateCuts_internal()
{
   MTIostringstream os;
   int retVal = ERR_BIN_MANAGER_NO_EVENT_TRACKS_FOUND;

   for (int trackNo = 0; trackNo < m_clip->getTimeTrackCount(); trackNo++)
   {
      if (m_clip->getTimeTrack(trackNo)->getTimeType() == TIME_TYPE_EVENT)
      {
         os << "Updating scene cuts in track " << trackNo;
         m_whatIAmDoingNow = os.str();
         m_fractionDone = 0.;
         retVal = doCutsForOneTrack(trackNo);
         if (retVal != 0)
            break;
         if (m_cancel)
            break;
      }
   }

   return retVal;
}

//--------------------------------------------------------------------------
// This method interprets the statistics in the diff file. Again, this was
// swiped from dfstat with the exception of the error handling. This can be
// called alone, via the above method to interperet an already existing set
// of diff data, ie. the vtr emulator creates a set of diff stats when it
// brings something in from vtr. This method should be called alone with
// clips that have pre-existing diff statistics, and in correspondence
// with createDiffsFile with imported image clips
//--------------------------------------------------------------------------

int CAutoDiff::doCutsForOneTrack(int trackNo)
{
   int retVal;
   FRAME_DIFF *framediff;
   CMetaData metaData(m_diffsFilePath);
   MTIostringstream error_msg;

   retVal = metaData.Initialize(TYPE_DIFF, m_totalFrameCount,
                                nullptr, nullptr);
   if (retVal != 0)
   {
      m_errorCode = retVal;
      m_errorMsg = string("Can't open diffs file ") + m_diffsFilePath;
      return retVal;
   }

   retVal = metaData.SelectSection(TYPE_DIFF);
   if (retVal != 0)
   {
      m_errorCode = retVal;
      m_errorMsg = string("Diffs file ") + m_diffsFilePath + " is corrupt";
      return retVal;
   }

   retVal = metaData.ReadWholeSection();
   if (retVal != 0)
   {
      m_errorCode = retVal;
      m_errorMsg = string("Can't read record from diffs file ")
                   + m_diffsFilePath;
      return retVal;
   }

   framediff = (FRAME_DIFF *)metaData.GetRecordPtr();
   if(framediff == nullptr)
   {
      m_errorCode = retVal;
      m_errorMsg = string("Can't get record pointer for diffs file ")
                            + m_diffsFilePath;
      return retVal;
   }

   // Enough memory for a cut on every frame
   float *cutStore = new float[m_totalFrameCount];
   if(cutStore == nullptr)
   {
      m_errorCode = ERR_BIN_MANAGER_OUT_OF_MEMORY;
      m_errorMsg = "Out of memory";
      return retVal;
   }

   for (int currentFrame = 0; currentFrame < m_totalFrameCount; currentFrame++)
   {
      float lst[FRAME_DIFF_NUM_ROW_BLOCK * FRAME_DIFF_NUM_ROW_BLOCK];
      long rowCol = 0;

      // Progress stuff
      m_fractionDone = (currentFrame + 1) / m_totalFrameCount;
      if (m_cancel)
         break;

      for (long lRow = 0; lRow < FRAME_DIFF_NUM_ROW_BLOCK; lRow++)
      {
         for (long lCol = 0; lCol < FRAME_DIFF_NUM_COL_BLOCK; lCol++)
         {
            float minbelow = 100000.;
            float maxbelow = 0;
            float minabove = 100000.;
            float maxabove = 0;
            int startFrame = currentFrame - NUM_FRAME;
            int i;

            if(startFrame < 0)
               startFrame = 0;

            //Look below this frame.
            int endFrame = currentFrame;
            for (i = startFrame; i < endFrame; i++)
            {
               if(framediff[i].faBlockAveFrame[lRow][lCol] < minbelow)
                  minbelow =framediff[i].faBlockAveFrame[lRow][lCol];

               if(framediff[i].faBlockAveFrame[lRow][lCol] > maxbelow)
                  maxbelow = framediff[i].faBlockAveFrame[lRow][lCol];
            }

            //look above the current frame.
            startFrame = currentFrame;
            endFrame = currentFrame + NUM_FRAME;
            if (endFrame > m_totalFrameCount)
               endFrame = m_totalFrameCount;

            for (i = startFrame; i < endFrame; i++)
            {
               if(framediff[i].faBlockAveFrame[lRow][lCol] <minabove){
                  minabove = framediff[i].faBlockAveFrame[lRow][lCol];
               }
               if(framediff[i].faBlockAveFrame[lRow][lCol]>maxabove){
                  maxabove = framediff[i].faBlockAveFrame[lRow][lCol];
               }
            }


            //get the separation between the top and bottom values calculated above.
            float separation;
            if (minabove > maxbelow)
            {
               separation = minabove - maxbelow;
            }
            else if (minbelow > maxabove)
            {
               separation = minbelow - maxabove;
            }
            else
            {
               separation = 0;
               //Overlap of populations.
            }

            //enter the separation into a list.
            long rc;

            // Set rc to a number where separation is less than the list
            // value and break saving the number of rc
            for (rc = 0; rc < rowCol; rc++)
            {
               if (separation < lst[rc])
                  break;
            }

            // Shift members of the list up to accomodate new value.
            for (i = rowCol; i > rc; i--)
               lst[i] = lst[i-1];

            // Put separation in the list.
            lst[rc] = separation;
            rowCol++;
         } // end of the col loop.
      }// end of the row loop.

      //find the upper percentile.

      //give the cuts appropriate flags.
      if (lst[(int)(PERCENTILE * FRAME_DIFF_NUM_ROW_BLOCK
                               * FRAME_DIFF_NUM_COL_BLOCK)]
             > m_threshold)
      {
         framediff[currentFrame].lDiffFlag |= DF_AUTO_CUT;
      }
      else
      {
         framediff[currentFrame].lDiffFlag &= ~DF_AUTO_CUT;
      }

      //stick the cut from the list into memory.
      cutStore[currentFrame] = lst[(int)(PERCENTILE * FRAME_DIFF_NUM_ROW_BLOCK
                                                  *FRAME_DIFF_NUM_COL_BLOCK)];
   }//out of currentFrame loop.

   CTimeTrack *eventTrack = m_clip->getTimeTrack(trackNo);
   for (int frameIndex = 0; frameIndex < eventTrack->getTotalFrameCount(); ++frameIndex)
   {
      CEventFrame *eventFrame = eventTrack->getEventFrame(frameIndex);
      MTI_UINT64 eventFlags = eventFrame->getEventFlags();

      // SPECIAL CASE HACK-O-RAMA: ALWAYS set a cut event on frame 0, and
      // NEVER set a cut on frame 1. There's an issue with the above code that
      // makes it always set a cut on frame 1, which is clearly undesirable!

      if (frameIndex == 0
      || (frameIndex > 1 && cutStore[frameIndex] > m_threshold))
      {
         // Cut was auto-detected.
         eventFlags |= EVENT_CUT_AUTO;
         ++m_numCutsFound;
      }
      else
      {
         // No auto cut - make sure flag is off.
         eventFlags &= ~EVENT_CUT_AUTO;
      }

      eventFrame->setEventFlags(eventFlags);
   }

   eventTrack->writeFrameListFile();

   // Completed
   m_fractionDone = 1.0;
   delete[] cutStore;

   return 0;
}




















