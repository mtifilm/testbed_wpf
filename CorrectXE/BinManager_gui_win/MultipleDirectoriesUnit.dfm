object MultipleDirectoriesForm: TMultipleDirectoriesForm
  Left = 872
  Top = 226
  Hint = 
    'Other directories at the same level in which the file you select' +
    'ed were found.  Those directories also contain files of the same' +
    ' extension as the file you selected.'
  Caption = 'Multiple Directories Found'
  ClientHeight = 402
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    460
    402)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 71
    Top = 8
    Width = 320
    Height = 16
    Anchors = [akTop]
    Caption = 'Also Found Other Directories with Similar Files'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 14
    Top = 96
    Width = 284
    Height = 13
    Caption = 'Check All Directories for which you would like a clip created:'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 64
    Width = 466
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    Shape = bsBottomLine
  end
  object Bevel2: TBevel
    Left = 0
    Top = 307
    Width = 468
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    Shape = bsBottomLine
  end
  object Label3: TLabel
    Left = 160
    Top = 40
    Width = 191
    Height = 13
    Anchors = [akTop]
    Caption = '<-- Never show me this dialog box again.'
  end
  object Label4: TLabel
    Left = 176
    Top = 56
    Width = 159
    Height = 13
    Anchors = [akTop]
    Caption = 'Just behave the way you used to.'
  end
  object Bevel3: TBevel
    Left = 0
    Top = 343
    Width = 468
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    Shape = bsBottomLine
  end
  object DirectoryCheckListBox: TCheckListBox
    Left = 32
    Top = 112
    Width = 418
    Height = 176
    OnClickCheck = DirectoryCheckListBoxClickCheck
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
    OnClick = DirectoryCheckListBoxClick
  end
  object CheckAllButton: TButton
    Left = 37
    Top = 293
    Width = 77
    Height = 22
    Anchors = [akBottom]
    Caption = 'Check All'
    TabOrder = 1
    OnClick = CheckAllButtonClick
  end
  object UncheckAllButton: TButton
    Left = 125
    Top = 293
    Width = 77
    Height = 22
    Anchors = [akBottom]
    Caption = 'Uncheck All'
    TabOrder = 2
    OnClick = UncheckAllButtonClick
  end
  object CreateSuperClipCheckBox: TCheckBox
    Left = 37
    Top = 331
    Width = 164
    Height = 19
    Anchors = [akBottom]
    Caption = 'Also Create Super Clip Named'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = CreateSuperClipCheckBoxClick
  end
  object GoAwayButton: TButton
    Left = 88
    Top = 40
    Width = 57
    Height = 25
    Anchors = [akTop]
    Caption = 'Go Away'
    ModalResult = 2
    TabOrder = 4
    OnClick = GoAwayButtonClick
  end
  object SuperClipNameEdit: TEdit
    Left = 208
    Top = 330
    Width = 121
    Height = 21
    Hint = 'Can'#39't be empty if checked'
    Anchors = [akBottom]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnChange = SuperClipNameEditChange
  end
  object OkButton: TBitBtn
    Left = 128
    Top = 373
    Width = 75
    Height = 25
    Hint = 
      'Must have at least one directory checked and a Super Clip name i' +
      'f the "Also Create Super Clip" box is checked.'
    Anchors = [akBottom]
    Kind = bkOK
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    OnClick = OkButtonClick
  end
  object BitBtnCancel: TBitBtn
    Left = 264
    Top = 373
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 7
  end
end
