//---------------------------------------------------------------------------

#ifndef DeleteBinUnitH
#define DeleteBinUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>

#include "MTIUNIT.h"   // for CW_START_FORM only

#include <string>
using std::string;

#define MODE_DELETE  0
#define MODE_ARCHIVE 1

//---------------------------------------------------------------------------
class TDeleteBinDialog : public TForm
{
__published:	// IDE-managed Components
   TButton *CancelDelete;
   TProgressBar *ProgressBar;
   TLabel *FileNameLabel;
   TLabel *PathNameLabel;
   TTimer *UpdateTimer;
   void __fastcall CancelDeleteClick(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall UpdateTimerTimer(TObject *Sender);
   
private:	// User declarations
   int countContents(const string &binPath);
   int deleteOrArchiveBin(const string &binPath, const string &rootPath);
   int deleteBinContents(const string &binPath, const string &rootPath);
   int archiveBinContents(const string &binPath, const string &rootPath);
   bool CheckUserCancel();

   void __fastcall CMStartForm(TMessage &Message);

   BEGIN_MESSAGE_MAP
      VCL_MESSAGE_HANDLER(CW_START_FORM, TMessage, CMStartForm)
   END_MESSAGE_MAP(TForm)

   // Bin Path and Root Bin names set by caller
   string targetBinPath;
   string targetRootBin;

   // Bin Path, delete Target and count used to communicate between
   // deletition and updates in this modal dialog
   string currentBinPath;
   string currentTarget;
   int deletedCount;
   int archiveSkipCount;

   bool done;
   bool success;
   bool userCancelRequest;
   int  iModeDeleteOrArchive;

public:		// User declarations
   __fastcall TDeleteBinDialog(TComponent* Owner);
   void SetDeleteTarget(const string &binPath, const string &rootBin);
   void SetModeDelete(void);
   void SetModeArchive(void);
   int  GetDeletedOrArchivedCount(void);
   int  GetArchiveSkipCount(void);

};
//---------------------------------------------------------------------------
extern PACKAGE TDeleteBinDialog *DeleteBinDialog;
//---------------------------------------------------------------------------

#endif
