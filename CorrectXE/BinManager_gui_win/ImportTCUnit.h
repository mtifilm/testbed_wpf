//----------------------------------------------------------------------------
#ifndef ImportTCUnitH
#define ImportTCUnitH
//----------------------------------------------------------------------------
#include <System.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "PopupComboBox.h"
#include "VTimeCodeEdit.h"
#include "FileListIO.h"
#include <Menus.hpp>
#include "BinManagerGUIUnit.h"
#include <Dialogs.hpp>



//----------------------------------------------------------------------------
class TImportTCDialog : public TForm
{
__published:
        TPanel *PanelMiddle;
        TPanel *PanelBottom;
        TBitBtn *CancelButton;
        TBitBtn *OKButton;
        TGroupBox *GroupBox1;
        TLabel *Label5;
        TLabel *Label6;
        VTimeCodeEdit *Part1TCSource;
        VTimeCodeEdit *Part1TCDest;
        VTimeCodeEdit *Part2TCSource;
        VTimeCodeEdit *Part2TCDest;
        VTimeCodeEdit *Part3TCSource;
        VTimeCodeEdit *Part3TCDest;
        TPanel *PanelTop;
   TLabel *SourceFileLabel;
   TLabel *SinglePaddingLabel;
        TSpeedButton *FileBrowseButton;
        TComboBox *FileNameComboBox;
        TPopupComboBox *RecordFormatPopupBox;
        TCheckBox *SortCheckBox;
        VTimeCodeEdit *MergeTC;
        TCheckBox *MergeCheckBox;
        VTimeCodeEdit *AutoPadTC;
        TCheckBox *AlternateCheckBox;
        TPopupComboBox *ListFormatPopupBox;
        TLabel *Label4;
        TBevel *Bevel1;
        TPopupMenu *PopupMenu1;
        TPopupMenu *PopupMenu2;
        TOpenDialog *OpenDialog;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TBitBtn *RestoreButton;
   TComboBox *FileTCFormatComboBox;
        TGroupBox *NewClipPropertiesGroupBox;
        TLabel *VideoFormatLabel;
        TLabel *AudioFormatLabel;
        TLabel *StorageFormatLabel;
        TLabel *MediaLocationLabel;
        TLabel *CreateClipPanelHint;
        TLabel *VideoFormatValue;
        TLabel *AudioFormatValue;
        TLabel *StorageFormatValue;
        TLabel *MediaLocationValue;
        void __fastcall MergeCheckBoxClick(TObject *Sender);
        void __fastcall AlternateCheckBoxClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall OKButtonClick(TObject *Sender);
        void __fastcall FileBrowseButtonClick(TObject *Sender);
        void __fastcall FileNameComboBoxDropDown(TObject *Sender);
        void __fastcall RecordFormatPopupBoxChange(TObject *Sender);
        void __fastcall ListFormatPopupBoxChange(TObject *Sender);
        void __fastcall RestoreButtonClick(TObject *Sender);
   void __fastcall FileTCFormatComboBoxChange(TObject *Sender);
private:
        CMRU mruFileName;
        CFileListIO FileListIO;
        void ParametersToGui(CFileListIO &flio);
        void HardParametersToGui(CFileListIO &flio);
        void GuiToParameters(CFileListIO &flio);

public:
	virtual __fastcall TImportTCDialog(TComponent* AOwner);
   virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
   virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
//   virtual bool ReadDesignSAP(void);
};
//----------------------------------------------------------------------------
extern PACKAGE TImportTCDialog *ImportTCDialog;
//----------------------------------------------------------------------------
#endif    
