//---------------------------------------------------------------------------

#ifndef EnterCommentUnitH
#define EnterCommentUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;
//---------------------------------------------------------------------------

class TEnterCommentForm : public TForm
{
__published:	// IDE-managed Components
        TEdit *EnterCommentEdit;
        TLabel *EnterCommentLabel;
        TButton *CancelButton;
        TButton *OkButton;
private:	// User declarations
public:		// User declarations
        __fastcall TEnterCommentForm(TComponent* Owner);

        void setComment(const string &aComment);
        string getComment();
        void setCaption(const string &newCaption);
};
//---------------------------------------------------------------------------
extern PACKAGE TEnterCommentForm *EnterCommentForm;
//---------------------------------------------------------------------------
#endif
