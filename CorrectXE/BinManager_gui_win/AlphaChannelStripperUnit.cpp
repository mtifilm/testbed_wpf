// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AlphaChannelStripperUnit.h"
#include "ClipAPI.h"
#include "DpxFrameHacker.h"
#include "bthread.h"
#include "ImageFileMediaAccess.h"
#include "Ippheaders.h"           // ONLY FOR IpaStripeStream.h!!!
#include "IpaStripeStream.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
// ---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TAlphaChannelStripperForm *AlphaChannelStripperForm;

////#define NO_MULTI
// ---------------------------------------------------------------------------

__fastcall TAlphaChannelStripperForm::TAlphaChannelStripperForm(TComponent* Owner) : TForm(Owner)
{
}

// ---------------------------------------------------------------------------
void __fastcall TAlphaChannelStripperForm::FormShow(TObject *Sender)
{
   mCancelledFlag = false;
   mHackCommand = MEDIA_HACK_INVALID;
   ProgressPanel->Visible = false;
   AreYouSurePanel->Visible = true;
}

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::Reset()
{
   mClipList.clear();
}

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::SetBinPath(string aBinPath)
{
   mBinPath = aBinPath;
}

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::AddClip(string aClipName)
{
   mClipList.push_back(aClipName);
}

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::ProcessingThread(void *aThreadArg, void *aThreadHandle)
{
   TAlphaChannelStripperForm *this_ = reinterpret_cast<TAlphaChannelStripperForm*>(aThreadArg);

   BThreadBegin(aThreadHandle);

   if (this_ != NULL)
   {
      this_->HackAllClips();
   }
}

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::HackAllClips()
{
   mTotalClipCount = mClipList.size();

   for (mClipIndex = 0; mClipIndex < mTotalClipCount; ++mClipIndex)
   {
      string &clipName = mClipList[mClipIndex];

      mClipName = clipName.c_str(); // mClipName is an AnsiString
      HackOneClip(clipName);
   }

   mProcessingIsCompleteFlag = true;
}

// ---------------------------------------------------------------------------
namespace
{ // local function - avoid cluttering the class

bool hackClipDotClpFile(ClipSharedPtr aClip, EMediaHackCommand aHackCommand)
{
   if (aClip == nullptr)
   {
      return false;
   }

   EAlphaMatteType alphaMatteType = IF_ALPHA_MATTE_INVALID;
   EAlphaMatteType alphaMatteHiddenType = IF_ALPHA_MATTE_INVALID;
   EPixelComponents pixelComponents = IF_PIXEL_COMPONENTS_INVALID;

   aClip->getAlphaMatteTypes(alphaMatteType, alphaMatteHiddenType);
   aClip->getPixelComponents(pixelComponents);

   // Crap out if the alpha or pixel components are wrong.
   if (alphaMatteType != IF_ALPHA_MATTE_16_BIT_INTERLEAVED || pixelComponents != IF_PIXEL_COMPONENTS_RGBA)
   {
      return false;
   }

   int errorCode = aClip->setAlphaMatteTypes(IF_ALPHA_MATTE_NONE, IF_ALPHA_MATTE_NONE);
   if (errorCode == 0)
   {
      errorCode = aClip->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
   }

   return (errorCode == 0);
}

} // End anonymous namespace

// ---------------------------------------------------------------------------
void TAlphaChannelStripperForm::HackOneClip(string aClipName)
{
   int errorCode = 0;
   CBinManager binManager;

   auto clip = binManager.openClip(mBinPath, aClipName, &errorCode);
   if (errorCode != 0)
   {
      TRACE_0(errout << "ERROR: Could not open Clip " << aClipName << endl << "       in Bin " << mBinPath);
      return;
   }

   CVideoProxy *mainVideo = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (mainVideo == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video proxy");
      return;
   }

   CVideoFrameList *videoFrameList = mainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video frame list");
      return;
   }

   mTotalFrameCount = videoFrameList->getTotalFrameCount();

   TRACE_2(errout << "INFO: Removing alpha channel from media for clip " << aClipName << endl << "      Total " << mTotalFrameCount <<
         " frames");

   int slowStartDelayInMsecs = 100;
   const int delayReductionInMsecs = 20;
   const int interClipDelayInMsecs = 250;

   mFrameIndex = 0;
   mAborted = false;
   auto f = [&](int frameIndex)
   {
      if (mCancelledFlag || mAborted)
      {
         return;
      }

//      // A little hack to allow the UI to catch up with the thread
//      // at the start of processing - this is especially useful when
//      // there are a small number of frames
//      if (slowStartDelayInMsecs > 0)
//      {
//         MTImillisleep(slowStartDelayInMsecs);
//         slowStartDelayInMsecs -= delayReductionInMsecs;
//      }

      // Careful, the hacker instance is NOT THREADSAFE, so we need to create
      // a new one each time.
      DpxFrameHacker frameHacker;

      // Find the frame file media file path and strip alpha from the file.
      CVideoFrame *frame = videoFrameList->getFrame(frameIndex);
      CVideoField *field = frame ? frame->getField(0) : nullptr;
      if (frame == nullptr || field == nullptr)
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: Alpha Channel Stripper cant get frame " << frameIndex << " in clip " << aClipName);
         return;
      }

      /////   // Should be done something like this, but DPX alpha stripper needs the clip image format.
      /////   errorCode = field->hackMedia(mHackCommand);

      CMediaLocation mediaLocation = field->getMediaLocation();
      MTIassert(mediaLocation.isMediaTypeImageFile());
      if (!mediaLocation.isMediaTypeImageFile())
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: Alpha Channel Stripper cant get frame " << frameIndex << " in clip " << aClipName);
         return;
      }

      CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
      auto filename = mediaAccess->getImageFileName(mediaLocation);

      errorCode = frameHacker.stripAlphaChannel(filename, *(clip->getImageFormat(VIDEO_SELECT_NORMAL)));

      /////

      if (errorCode != 0)
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: Alpha Channel Stripper could not hack media of frame " << frameIndex << endl <<
               "       Return code " << errorCode);
         return;
      }

      ++mFrameIndex;
   };

   ;
   IpaStripeStream iss;
   iss << mTotalFrameCount;
   iss << efu_job(f);
#ifdef NO_MULTI
   iss.run();
#else
   iss.stripe();
#endif

   if (mCancelledFlag || mAborted)
   {
      mAborted = true;
      MTIostringstream os;
      os << "WARNING: Alpha Channel Stripper did not complete processing! " << endl
         << "         Processed " << mFrameIndex << " of " << mTotalFrameCount << " frames " << endl
         << "         in clip " << aClipName << " which may now be corrupted." << endl
         << "         You may try running the Alpha Remover again to fix it.";
      TRACE_0(errout << os.str());
      _MTIErrorDialog(Handle, os.str());
      binManager.closeClip(clip);
      clip = nullptr;
      return;
   }

   mFrameIndex = mTotalFrameCount;

   // Change the AlphaMatteType and PixelComponent entries in the
   // <clipname>.clp file
   if (!hackClipDotClpFile(clip, mHackCommand))
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Alpha Channel Stripper could not hack clip file for " << aClipName);
      return;
   }

   // Update the "last media action" (shows up in the clip "status"
   // column in the bin manager clip list view)
   mainVideo->setLastMediaAction(MEDIA_STATUS_ALPHA_DESTROYED);

   // [Legacy comments here - not sure what they mean]
   // Make status change permanent by forcing a write of the .clp
   // file. I think it's completely absurd that I have to do this
   // NOW DONE IN TRACK
   clip->writeFile(); // leaving it in until I port the TRACK STUFF! QQQ

   // Update the registered media access object for the clip.
   // Uggh, videoProxy should be taking care of this.... QQQ
   auto videoProxy = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   auto mediaStorageList = videoProxy->getMediaStorageList();
   auto mediaStorage_0 = mediaStorageList->getMediaStorage(0);
   if (mediaStorage_0 != nullptr)
   {
      CMediaStorageList *nonConstMediaStorageList = const_cast<CMediaStorageList *>(mediaStorageList);
      EMediaType mediaType = mediaStorage_0->getMediaType();
      string mediaIdentifier = mediaStorage_0->getMediaIdentifier();
      string historyDirectory = mediaStorage_0->getHistoryDirectory();
      CImageFormat imageFormat = *(videoProxy->getImageFormat());
      nonConstMediaStorageList->replaceMediaStorage(0, mediaType, mediaIdentifier, historyDirectory, imageFormat);
   }

   // Done with the clip
   binManager.closeClip(clip);
   clip = nullptr;

   // Pause to show completed progress bar in UI before continuing on to
   // the next clip in the list
   MTImillisleep(interClipDelayInMsecs);
}
// ---------------------------------------------------------------------------

void __fastcall TAlphaChannelStripperForm::SetupPanelOkButtonClick(TObject *Sender)
{
   // These values are all set in the processing thread and read in
   // ProgressTimer(); we initialize them to dummy values here
   mClipName = "";
   mClipIndex = 0;
   mTotalClipCount = 1; // fixed in thread
   mFrameIndex = 0;
   mTotalFrameCount = 1; // fixed in thread
   mProcessingIsCompleteFlag = false;
   mCancelledFlag = false;

   // These GUI elements are all maintained by ProgressTimer()
   ClipNameValue->Caption = ""; // gets set in timer
   ClipListProgressBar->Max = static_cast<int>(mTotalClipCount);
   // Min is set to 0 at design time and never changed
   ClipListProgressBar->Position = static_cast<int>(mClipIndex);
   FrameValue->Caption = "";
   OfValue->Caption = "";
   FrameIndexProgressBar->Max = static_cast<int>(mTotalFrameCount);
   // Min is set to 0 at design time and never changed
   FrameIndexProgressBar->Position = static_cast<int>(mFrameIndex);
   ProgressPanelCloseButton->Caption = "Cancel";
   StatusLabel->Visible = false;
   //

   // OK to swap the panels now
   AreYouSurePanel->Visible = false;
   ProgressPanel->Visible = true;

   // Legacy from when this was done in the clip.
   ModifyingModeLabel->Caption = "Modifying image files to remove alpha channel...";
   mHackCommand = MEDIA_HACK_DESTROY_ALPHA_MATTE;

   mProcessingThreadId = BThreadSpawn(ProcessingThread, (void *)this);
   if (mProcessingThreadId == NULL)
   {
      _MTIErrorDialog(Handle, "Failed to launch processing thread   ");
      ModalResult = mrCancel;
   }
   else
   {
      // fire up the timer, which will track the progress and display it
      ProgressTimer->Enabled = true;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TAlphaChannelStripperForm::ProgressPanelCloseButtonClick(TObject *Sender)
{
   if (ProgressPanelCloseButton->Caption == "Close")
   {
      ModalResult = mrOk; // Setting ModalResult will hide the dialog
   }
   else // Hacking is in progress
   {

      ModalResult = mrNone; // Don't hide the dialog
      mCancelledFlag = true;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TAlphaChannelStripperForm::ProgressTimerTimer(TObject *Sender)
{
   // Fun with signs
   int clipIndex = static_cast<int>(mClipIndex);
   int totalClipCount = static_cast<int>(mTotalClipCount);
   int frameIndex = static_cast<int>(mFrameIndex);
   int totalFrameCount = static_cast<int>(mTotalFrameCount);

   // Update clip progress panel GUI elements
   if (ClipNameValue->Caption != mClipName)
   {
      ClipNameValue->Caption = mClipName;
   }
   if (ClipListProgressBar->Max != totalClipCount)
   {
      ClipListProgressBar->Max = totalClipCount;
      ClipListProgressBar->Position = clipIndex;
   }
   else if (ClipListProgressBar->Position != clipIndex)
   {
      ClipListProgressBar->Position = clipIndex;
   }

   // Update frame-by-frame progress
   if (FrameIndexProgressBar->Max != totalFrameCount)
   {
      OfValue->Caption = AnsiString(totalFrameCount);
      OfValue->Refresh();
      FrameIndexProgressBar->Max = totalFrameCount;
      FrameValue->Caption = AnsiString(frameIndex);
      FrameValue->Refresh();
      FrameIndexProgressBar->Position = frameIndex;
   }
   else if (FrameIndexProgressBar->Position != frameIndex)
   {
      FrameValue->Caption = AnsiString(frameIndex);
      FrameValue->Refresh();
      FrameIndexProgressBar->Position = frameIndex;
   }

   // Check if we're all done
   if (mProcessingIsCompleteFlag || mCancelledFlag)
   {
      ProgressTimer->Enabled = false;
      ProgressPanelCloseButton->Caption = "Close";
      StatusLabel->Caption = mCancelledFlag ? " Cancelled " : " Finished ";
      StatusLabel->Visible = true;
   }

   ProgressPanel->Refresh();

}
// ---------------------------------------------------------------------------
