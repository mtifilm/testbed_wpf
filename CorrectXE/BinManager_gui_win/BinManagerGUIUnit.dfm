object BinManagerForm: TBinManagerForm
  Left = 482
  Top = 244
  Caption = 'Project Manager - DRS Nova by MTI Film'
  ClientHeight = 665
  ClientWidth = 1251
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  OldCreateOrder = False
  PopupMode = pmAuto
  ShowHint = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object BinTreeRightSideSplitter: TSplitter
    Left = 202
    Top = 0
    Width = 5
    Height = 477
    AutoSnap = False
    MinSize = 76
    ExplicitTop = 23
    ExplicitHeight = 435
  end
  object BottomPanelSpacerLabel: TLabel
    Left = 0
    Top = 477
    Width = 1251
    Height = 6
    Align = alBottom
    AutoSize = False
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object LeftPanel: TPanel
    Left = 0
    Top = 0
    Width = 202
    Height = 477
    Align = alLeft
    TabOrder = 1
    object BinTreeBottomSplitter: TSplitter
      Left = 1
      Top = 315
      Width = 200
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 76
      ExplicitTop = 273
    end
    object ProxyDisplayPanel: TPanel
      Left = 1
      Top = 320
      Width = 200
      Height = 156
      Align = alBottom
      BevelOuter = bvNone
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      DesignSize = (
        200
        156)
      object PanelProxy: TPanel
        Left = 8
        Top = 4
        Width = 185
        Height = 148
        Anchors = []
        BevelInner = bvLowered
        BevelOuter = bvLowered
        Caption = 'N/A'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -32
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          185
          148)
        object ImageProxy: TPaintBox
          Left = 3
          Top = 3
          Width = 179
          Height = 120
          Anchors = [akLeft, akTop, akRight, akBottom]
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          PopupMenu = ProxyImagePopupMenu
          OnPaint = ImageProxyPaint
        end
        object TrackBarProxy: TTrackBar
          Left = 1
          Top = 128
          Width = 183
          Height = 18
          Anchors = [akLeft, akRight, akBottom]
          Max = 100
          TabOrder = 0
          TabStop = False
          OnChange = TrackBarProxyChange
        end
      end
    end
    object BinTree: TTreeView
      Left = 1
      Top = 1
      Width = 200
      Height = 314
      Align = alClient
      DragMode = dmAutomatic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      HideSelection = False
      Indent = 19
      ParentFont = False
      PopupMenu = BinTreePopupMenu
      ReadOnly = True
      RightClickSelect = True
      TabOrder = 0
      TabStop = False
      OnChange = BinTreeChange
      OnChanging = BinTreeChanging
      OnClick = BinTreeClick
      OnDblClick = BinTreeDblClick
      OnDragDrop = BinTreeDragDrop
      OnDragOver = BinTreeDragOver
      OnExpanding = BinTreeExpanding
    end
  end
  object BottomPanel: TPanel
    Left = 0
    Top = 483
    Width = 1251
    Height = 182
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      1251
      182)
    object VideoStoreUsagePanel: TPanel
      Left = 759
      Top = 0
      Width = 276
      Height = 153
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Visible = False
      object LabelFormatFile: TLabel
        Left = 20
        Top = 11
        Width = 34
        Height = 13
        Caption = 'Format'
      end
      object ClickSchemeInfoButton: TSpeedButton
        Left = 239
        Top = 8
        Width = 21
        Height = 21
        Flat = True
        Glyph.Data = {
          AA030000424DAA03000000000000360000002800000011000000110000000100
          1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF000000000000
          272727272727272727272727272727000000000000FFFFFFFFFFFFFFFFFFFFFF
          FF00FFFFFFFFFFFFFFFFFF000000272727272727FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF272727272727000000FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF00000027
          2727FFFFFFFFFFFF272727000000000000000000595959FFFFFFFFFFFF272727
          000000FFFFFFFFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFF80808059
          5959000000595959FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
          FF000000272727FFFFFFFFFFFFFFFFFFFFFFFF595959000000595959FFFFFFFF
          FFFFFFFFFFFFFFFF272727000000FFFFFF00000000272727FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF595959000000595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27
          272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5959590000
          00595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000000000272727
          FFFFFFFFFFFFFFFFFFFFFFFF808080595959000000595959FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF27272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFF
          272727000000000000000000595959FFFFFFFFFFFFFFFFFFFFFFFF2727270000
          0000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000FFFFFF000000272727FF
          FFFFFFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFFFFFFFFFFFFFFFFFF
          272727000000FFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFFFFFFFF27
          2727000000272727FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
          FFFFFFFF000000272727FFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFF
          FFFFFFFFFF272727000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF0000002727
          27272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF272727272727000000FFFFFFFF
          FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF0000000000002727272727272727
          27272727272727000000000000FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00}
        OnClick = ClipSchemeInfoButtonClick
      end
      object Location: TLabel
        Left = 20
        Top = 38
        Width = 40
        Height = 13
        Caption = 'Location'
      end
      object ClipSchemePopupBox: TPopupComboBox
        Left = 66
        Top = 8
        Width = 167
        Height = 21
        DropDownMenu = ClipSchemePopupMenu
        TabOrder = 0
        TabStop = False
      end
      object VideoStoreUsagePositioningPanel2: TPanel
        Left = 48
        Top = 60
        Width = 200
        Height = 87
        BevelOuter = bvNone
        TabOrder = 1
      end
      object LocationComboBoxPositioningPanel2: TPanel
        Left = 66
        Top = 35
        Width = 167
        Height = 21
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
    object CreateClipPanel: TPanel
      Left = 0
      Top = 6
      Width = 1251
      Height = 176
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      DesignSize = (
        1251
        176)
      object UnexpandedNewClipPanel_REMOVED_: TPanel
        Left = 0
        Top = 0
        Width = 1034
        Height = 21
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Visible = False
        object ExpandCreateClipPanelButton_REMOVED_: TSpeedButton
          Left = 8
          Top = 6
          Width = 13
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            92000000424D9200000000000000760000002800000007000000070000000100
            0400000000001C00000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF0FFFF0FF00
            FFF0FF000FF0FF0000F0FF000FF0FF00FFF0FF0FFFF0}
          ParentFont = False
          Visible = False
        end
        object UnexpandedNewClipPanellLabel_REMOVED_: TLabel
          Left = 30
          Top = 4
          Width = 70
          Height = 16
          Caption = 'Create Clip'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
      end
      object GroupBoxProjectScanList: TGroupBox
        Left = 20
        Top = 8
        Width = 717
        Height = 137
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        object Label12: TLabel
          Left = 14
          Top = 16
          Width = 38
          Height = 13
          Caption = 'Prod Co'
        end
        object Label16: TLabel
          Left = 19
          Top = 40
          Width = 34
          Height = 13
          Caption = 'Project'
        end
        object Label17: TLabel
          Left = 32
          Top = 64
          Width = 20
          Height = 13
          Caption = 'Title'
        end
        object Label6: TLabel
          Left = 13
          Top = 88
          Width = 38
          Height = 13
          Caption = 'Prod No'
        end
        object WorkOrderLabel: TLabel
          Left = 431
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Work Order'
        end
        object ShootDateLabel: TLabel
          Left = 222
          Top = 16
          Width = 54
          Height = 13
          Caption = 'Shoot Date'
        end
        object TransferDateLabel: TLabel
          Left = 211
          Top = 40
          Width = 67
          Height = 13
          Caption = 'Transfer Date'
        end
        object ColoristLabel: TLabel
          Left = 453
          Top = 40
          Width = 36
          Height = 13
          Caption = 'Colorist'
        end
        object AssistLabel: TLabel
          Left = 460
          Top = 64
          Width = 28
          Height = 13
          Caption = 'Assist'
        end
        object Label11: TLabel
          Left = 18
          Top = 112
          Width = 33
          Height = 13
          Caption = 'Job No'
        end
        object FieldALabel: TLabel
          Left = 244
          Top = 64
          Width = 32
          Height = 13
          Caption = 'Field A'
        end
        object FieldBLabel: TLabel
          Left = 244
          Top = 88
          Width = 31
          Height = 13
          Caption = 'Field B'
        end
        object FieldCLabel: TLabel
          Left = 244
          Top = 112
          Width = 32
          Height = 13
          Caption = 'Field C'
        end
        object Title: TEdit
          Left = 60
          Top = 60
          Width = 130
          Height = 21
          TabOrder = 2
          Text = 'Title'
        end
        object ProdCo: TEdit
          Left = 60
          Top = 12
          Width = 130
          Height = 21
          TabOrder = 0
          Text = 'Prod Co'
        end
        object Project: TEdit
          Left = 60
          Top = 36
          Width = 130
          Height = 21
          TabOrder = 1
          Text = 'Project'
        end
        object ProdNo: TEdit
          Left = 60
          Top = 84
          Width = 130
          Height = 21
          TabOrder = 3
          Text = 'Prod No'
        end
        object WorkOrder: TEdit
          Left = 497
          Top = 12
          Width = 130
          Height = 21
          Color = clBtnFace
          TabOrder = 10
          Text = 'Work Order'
        end
        object ShootDate: TEdit
          Left = 284
          Top = 12
          Width = 130
          Height = 21
          TabOrder = 5
          Text = 'Shoot Date'
        end
        object TransferDate: TEdit
          Left = 284
          Top = 36
          Width = 130
          Height = 21
          TabOrder = 6
          Text = 'Transfer Date'
        end
        object Colorist: TEdit
          Left = 497
          Top = 36
          Width = 130
          Height = 21
          Color = clBtnFace
          TabOrder = 11
          Text = 'Colorist'
        end
        object Assist: TEdit
          Left = 497
          Top = 60
          Width = 130
          Height = 21
          Color = clBtnFace
          TabOrder = 12
          Text = 'Assist'
        end
        object ScanListEditButton: TButton
          Left = 636
          Top = 78
          Width = 75
          Height = 25
          Caption = 'Edi&t'
          TabOrder = 13
          OnClick = ScanListEditButtonClick
        end
        object ScanListSaveButton: TButton
          Left = 636
          Top = 106
          Width = 75
          Height = 25
          Caption = '&Save'
          Enabled = False
          TabOrder = 14
          OnClick = ScanListSaveButtonClick
        end
        object JobNo: TEdit
          Left = 60
          Top = 108
          Width = 130
          Height = 21
          Color = clBtnFace
          TabOrder = 4
          Text = 'Job No'
        end
        object FieldB: TEdit
          Left = 284
          Top = 84
          Width = 130
          Height = 21
          TabOrder = 8
          Text = 'Field B'
        end
        object FieldA: TEdit
          Left = 284
          Top = 60
          Width = 130
          Height = 21
          TabOrder = 7
          Text = 'Field A'
        end
        object FieldC: TEdit
          Left = 284
          Top = 108
          Width = 130
          Height = 21
          TabOrder = 9
          Text = 'Field C'
        end
      end
      object GroupBoxLicense: TGroupBox
        Left = 20
        Top = 8
        Width = 717
        Height = 137
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        object VersionLicense: TLabel
          Left = 128
          Top = 112
          Width = 70
          Height = 13
          Caption = 'VersionLicense'
          Visible = False
        end
        object Label1: TLabel
          Left = 336
          Top = 20
          Width = 137
          Height = 78
          Caption = 
            'MTI Film'#13#10'http://www.mtifilm.com/'#13#10#13#10'For technical support:'#13#10'fil' +
            'm_support@mathtech.com'#13#10'+1 401 831 1315'
        end
        object Label2: TLabel
          Left = 74
          Top = 16
          Width = 45
          Height = 13
          Caption = 'Company'
        end
        object Label3: TLabel
          Left = 80
          Top = 40
          Width = 39
          Height = 13
          Caption = 'Address'
        end
        object Label4: TLabel
          Left = 7
          Top = 64
          Width = 116
          Height = 13
          Caption = 'City, State, Postal Code'
        end
        object Label5: TLabel
          Left = 82
          Top = 88
          Width = 39
          Height = 13
          Caption = 'Country'
        end
        object Bevel5: TBevel
          Left = 304
          Top = 16
          Width = 3
          Height = 100
        end
        object LicenseSaveButton: TButton
          Left = 636
          Top = 106
          Width = 75
          Height = 25
          Caption = '&Save'
          TabOrder = 5
          OnClick = LicenseSaveButtonClick
        end
        object Company: TEdit
          Left = 128
          Top = 12
          Width = 145
          Height = 21
          TabOrder = 0
          Text = 'Company'
        end
        object Address: TEdit
          Left = 128
          Top = 36
          Width = 145
          Height = 21
          TabOrder = 1
          Text = 'Address'
        end
        object CityStatePostal: TEdit
          Left = 128
          Top = 60
          Width = 145
          Height = 21
          TabOrder = 2
          Text = 'CityStatePostal'
        end
        object Country: TEdit
          Left = 128
          Top = 84
          Width = 145
          Height = 21
          TabOrder = 3
          Text = 'Country'
        end
        object LicenseEditButton: TButton
          Left = 636
          Top = 78
          Width = 75
          Height = 25
          Caption = 'Edi&t'
          TabOrder = 4
          OnClick = LicenseEditButtonClick
        end
      end
      object GroupBoxNewClip: TPanel
        Left = 0
        Top = 0
        Width = 1251
        Height = 170
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          1251
          170)
        object NewClipNewMediaPanel: TPanel
          Left = 12
          Top = 12
          Width = 909
          Height = 149
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          DesignSize = (
            909
            149)
          object Bevel9: TBevel
            Left = 207
            Top = 16
            Width = 3
            Height = 125
            Anchors = [akLeft, akBottom]
          end
          object Bevel12: TBevel
            Left = 436
            Top = 16
            Width = 3
            Height = 125
            Anchors = [akLeft, akBottom]
          end
          object Bevel10: TBevel
            Left = 450
            Top = 108
            Width = 449
            Height = 3
            Anchors = [akLeft, akBottom]
          end
          object SourceDescriptionPanel: TPanel
            Left = 0
            Top = 12
            Width = 201
            Height = 133
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            TabOrder = 0
            DesignSize = (
              201
              133)
            object VideoStoreMediaFormatPanel: TPanel
              Left = 4
              Top = 66
              Width = 196
              Height = 61
              Anchors = [akLeft, akBottom]
              BevelOuter = bvNone
              TabOrder = 0
              DesignSize = (
                196
                61)
              object AudioFormatLabel: TLabel
                Left = 1
                Top = 36
                Width = 32
                Height = 26
                Anchors = [akLeft, akBottom]
                Caption = 'Audio'#13#10'Format'
              end
              object VideoFormatLabel: TLabel
                Left = 1
                Top = 8
                Width = 32
                Height = 26
                Anchors = [akLeft, akBottom]
                Caption = 'Video'#13#10'Format'
              end
              object NewMediaVideoFormatComboBox: TComboBox
                Left = 42
                Top = 12
                Width = 145
                Height = 21
                Style = csDropDownList
                Anchors = [akLeft, akBottom]
                TabOrder = 0
                TabStop = False
                OnChange = ClipFormatComponentChanged
              end
              object NewMediaAudioFormatComboBox: TComboBox
                Left = 42
                Top = 40
                Width = 145
                Height = 21
                Style = csDropDownList
                Anchors = [akLeft, akBottom]
                TabOrder = 1
                TabStop = False
                OnChange = ClipFormatComponentChanged
              end
            end
            object FillModeRadioButtonPanel: TPanel
              Left = 4
              Top = 2
              Width = 189
              Height = 65
              Anchors = [akLeft, akBottom]
              BevelOuter = bvLowered
              TabOrder = 1
              DesignSize = (
                189
                65)
              object FillModeLabel: TLabel
                Left = 12
                Top = 8
                Width = 34
                Height = 13
                Anchors = [akLeft, akBottom]
                Caption = 'Source'
              end
              object FillWithBlanksRadioButton: TRadioButton
                Left = 60
                Top = 8
                Width = 117
                Height = 17
                Anchors = [akLeft, akBottom]
                Caption = 'None (Blank Frames)'
                Checked = True
                TabOrder = 0
                TabStop = True
                OnClick = RadioButtonClick
              end
              object FillByRecordingRadioButton: TRadioButton
                Left = 60
                Top = 26
                Width = 117
                Height = 17
                Anchors = [akLeft, akBottom]
                Caption = 'Record from VTR'
                TabOrder = 1
                OnClick = RadioButtonClick
              end
              object FillFromClipRadioButton: TRadioButton
                Left = 60
                Top = 44
                Width = 117
                Height = 17
                Anchors = [akLeft, akBottom]
                Caption = 'Copy Selected Clip'
                TabOrder = 2
                OnClick = RadioButtonClick
              end
            end
          end
          object LocationTypeSelectionPanel: TPanel
            Left = 456
            Top = 40
            Width = 200
            Height = 19
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            TabOrder = 5
            Visible = False
            DesignSize = (
              200
              19)
            object LocationRepositoryRadioButton: TRadioButton
              Left = 4
              Top = 0
              Width = 80
              Height = 17
              Anchors = [akLeft, akBottom]
              Caption = 'Repository'
              Checked = True
              TabOrder = 0
              TabStop = True
            end
            object OtherLocationRadioButton: TRadioButton
              Left = 88
              Top = 0
              Width = 96
              Height = 17
              Anchors = [akLeft, akBottom]
              Caption = 'Other Location'
              TabOrder = 1
            end
          end
          object FileMediaLocationPanel: TPanel
            Left = 444
            Top = 8
            Width = 461
            Height = 93
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            TabOrder = 2
            DesignSize = (
              461
              93)
            object FileVidClipFileInFrameLabel: TLabel
              Left = 246
              Top = 64
              Width = 29
              Height = 26
              Anchors = [akLeft, akBottom]
              Caption = 'In'#13#10'Frame'
            end
            object FileVidClipFileNameLabel: TLabel
              Left = 246
              Top = 36
              Width = 28
              Height = 26
              Anchors = [akLeft, akBottom]
              Caption = 'File'#13#10'Name'
            end
            object FileVidClipVideoFolderLabel: TLabel
              Left = 246
              Top = 11
              Width = 29
              Height = 26
              Anchors = [akLeft, akBottom]
              Caption = 'Video'#13#10'Folder'
            end
            object FileVidClipFileTypeLabel: TLabel
              Left = 10
              Top = 21
              Width = 24
              Height = 26
              Anchors = [akLeft, akBottom]
              Caption = 'File'#13#10'Type'
            end
            object FileVidClipAudioFolderLabel: TLabel
              Left = 10
              Top = 61
              Width = 29
              Height = 26
              Anchors = [akLeft, akBottom]
              Caption = 'Audio'#13#10'Folder'
            end
            object FileVidClipAudioFolderBrowseButton: TSpeedButton
              Left = 200
              Top = 63
              Width = 23
              Height = 22
              Hint = 'Browse for a single clip'
              Anchors = [akLeft, akBottom]
              Flat = True
              Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                04000000000080000000CE0E0000C40E00001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                77777777777777777777000000000007777700333333333077770B0333333333
                07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
                77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
                7007777777770777070777777777700077777777777777777777}
              OnClick = FileVidClipAudioFolderBrowseButtonClick
            end
            object FileVidClipInfoButton: TSpeedButton
              Left = 200
              Top = 24
              Width = 23
              Height = 22
              Anchors = [akLeft, akBottom]
              Flat = True
              Glyph.Data = {
                AA030000424DAA03000000000000360000002800000011000000110000000100
                1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF000000000000
                272727272727272727272727272727000000000000FFFFFFFFFFFFFFFFFFFFFF
                FF00FFFFFFFFFFFFFFFFFF000000272727272727FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF272727272727000000FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF00000027
                2727FFFFFFFFFFFF272727000000000000000000595959FFFFFFFFFFFF272727
                000000FFFFFFFFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFF80808059
                5959000000595959FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
                FF000000272727FFFFFFFFFFFFFFFFFFFFFFFF595959000000595959FFFFFFFF
                FFFFFFFFFFFFFFFF272727000000FFFFFF00000000272727FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF595959000000595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27
                272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5959590000
                00595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000000000272727
                FFFFFFFFFFFFFFFFFFFFFFFF808080595959000000595959FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF27272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFF
                272727000000000000000000595959FFFFFFFFFFFFFFFFFFFFFFFF2727270000
                0000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000FFFFFF000000272727FF
                FFFFFFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFFFFFFFFFFFFFFFFFF
                272727000000FFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFFFFFFFF27
                2727000000272727FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
                FFFFFFFF000000272727FFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFF
                FFFFFFFFFF272727000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF0000002727
                27272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF272727272727000000FFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF0000000000002727272727272727
                27272727272727000000000000FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00}
              OnClick = NewMediaFormatInfoButtonClick
            end
            object FileVidClipVideoFolderBrowseButton: TBitBtn
              Left = 448
              Top = 13
              Width = 23
              Height = 22
              Hint = 'Browse for a single clip!'
              Anchors = [akLeft, akBottom]
              Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                04000000000080000000CE0E0000C40E00001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                77777777777777777777000000000007777700333333333077770B0333333333
                07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
                77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
                7007777777770777070777777777700077777777777777777777}
              TabOrder = 7
              OnClick = FileVidClipVideoFolderBrowseButtonClick
            end
            object FileVidClipFileNameEdit: TEdit
              Left = 282
              Top = 41
              Width = 175
              Height = 19
              Hint = 
                'File name prototype; e.g., MyFile.####.dpx, use "#" to indicate ' +
                'where frame number appears'
              TabStop = False
              Anchors = [akLeft, akBottom]
              TabOrder = 0
              OnChange = VideoFileEditChange
              OnExit = VideoFileEditExit
            end
            object FileVidClipInFrameEdit: TEdit
              Left = 282
              Top = 69
              Width = 91
              Height = 19
              Hint = 
                'Number that appears in the filename of the first frame of the cl' +
                'ip'
              TabStop = False
              Anchors = [akLeft, akBottom]
              TabOrder = 1
            end
            object FileVidClipVideoFolderComboBox: TComboBox
              Tag = 1
              Left = 282
              Top = 14
              Width = 167
              Height = 21
              Anchors = [akLeft, akBottom]
              TabOrder = 2
              TabStop = False
              OnEnter = ComboBoxEnter
              OnExit = ComboBoxExit
              OnKeyPress = ComboBoxKeyPress
              OnSelect = ComboBoxSelect
            end
            object FileVidClipAudioFolderComboBox: TComboBox
              Tag = 2
              Left = 46
              Top = 64
              Width = 155
              Height = 21
              Anchors = [akLeft, akBottom]
              TabOrder = 3
              TabStop = False
              OnEnter = ComboBoxEnter
              OnExit = ComboBoxExit
              OnKeyPress = ComboBoxKeyPress
              OnSelect = ComboBoxSelect
            end
            object FileVidClipTCIndexCheckBox: TCheckBox
              Left = 386
              Top = 73
              Width = 66
              Height = 17
              Hint = 'Use a unique number based on the In Timecode'
              TabStop = False
              Anchors = [akLeft, akBottom]
              Caption = 'TC Index'
              TabOrder = 4
              OnClick = FileVidClipTCIndexCheckBoxClick
            end
            object FileVidClipMediaStorageComboBox: TComboBox
              Left = 46
              Top = 25
              Width = 155
              Height = 21
              Anchors = [akLeft, akBottom]
              TabOrder = 5
              TabStop = False
              OnChange = ClipFormatComponentChanged
            end
            object VideoFilePickerPanel: TPanel
              Left = 236
              Top = 4
              Width = 225
              Height = 89
              BevelOuter = bvNone
              TabOrder = 6
              Visible = False
              DesignSize = (
                225
                89)
              object LabelFolderVideo: TLabel
                Left = 5
                Top = 0
                Width = 29
                Height = 26
                Caption = 'Video'#13#10'Folder'
              end
              object LabelFileNameVideo: TLabel
                Left = 5
                Top = 28
                Width = 28
                Height = 26
                Caption = 'File'#13#10'Name'
              end
              object VideoFileBrowseButton: TSpeedButton
                Left = 197
                Top = 60
                Width = 23
                Height = 22
                Hint = 'Browse for a single clip?'
                Flat = True
                Glyph.Data = {
                  F6000000424DF600000000000000760000002800000010000000100000000100
                  04000000000080000000CE0E0000C40E00001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
                  77777777777777777777000000000007777700333333333077770B0333333333
                  07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
                  77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
                  7007777777770777070777777777700077777777777777777777}
                OnClick = VideoFileBrowseButtonClick
              end
              object LabelInFrameVideo: TLabel
                Left = 5
                Top = 56
                Width = 29
                Height = 26
                Caption = 'In'#13#10'Frame'
              end
              object VideoFileFolderEdit: TEdit
                Left = 43
                Top = 4
                Width = 178
                Height = 19
                TabStop = False
                TabOrder = 0
                OnChange = VideoFileEditChange
                OnExit = VideoFileEditExit
              end
              object VideoFileNameEdit: TEdit
                Left = 43
                Top = 32
                Width = 178
                Height = 19
                Hint = 
                  'File name prototype; e.g., MyFile.####.dpx, use "#" to indicate ' +
                  'where frame number appears'
                TabStop = False
                TabOrder = 1
                OnChange = VideoFileEditChange
                OnExit = VideoFileEditExit
              end
              object VideoInFrameEdit: TEdit
                Left = 43
                Top = 60
                Width = 69
                Height = 19
                Hint = 
                  'File name prototype; e.g., MyFile.####.dpx, use "#" to indicate ' +
                  'where frame number appears'
                TabStop = False
                TabOrder = 2
              end
              object CheckBox1: TCheckBox
                Left = 119
                Top = 60
                Width = 66
                Height = 17
                Hint = 'Use a unique number based on the In Timecode'
                TabStop = False
                Anchors = [akLeft, akBottom]
                Caption = 'TC Index'
                TabOrder = 3
                OnClick = FileVidClipTCIndexCheckBoxClick
              end
            end
          end
          object VideoStorePickerPanel: TPanel
            Left = 445
            Top = 7
            Width = 461
            Height = 93
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            TabOrder = 4
            DesignSize = (
              461
              93)
            object LabelLocationVideo: TLabel
              Left = 3
              Top = 10
              Width = 41
              Height = 26
              Caption = 'Media'#13#10'Location'
            end
            object RepositoryStorageFormatLabel: TLabel
              Left = 3
              Top = 64
              Width = 37
              Height = 26
              Caption = 'Storage'#13#10'Format'
            end
            object RepositoryFormatInfoButton: TSpeedButton
              Left = 212
              Top = 67
              Width = 23
              Height = 22
              Anchors = [akLeft, akBottom]
              Flat = True
              Glyph.Data = {
                AA030000424DAA03000000000000360000002800000011000000110000000100
                1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF000000000000
                272727272727272727272727272727000000000000FFFFFFFFFFFFFFFFFFFFFF
                FF00FFFFFFFFFFFFFFFFFF000000272727272727FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF272727272727000000FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF00000027
                2727FFFFFFFFFFFF272727000000000000000000595959FFFFFFFFFFFF272727
                000000FFFFFFFFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFF80808059
                5959000000595959FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
                FF000000272727FFFFFFFFFFFFFFFFFFFFFFFF595959000000595959FFFFFFFF
                FFFFFFFFFFFFFFFF272727000000FFFFFF00000000272727FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF595959000000595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27
                272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5959590000
                00595959FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000000000272727
                FFFFFFFFFFFFFFFFFFFFFFFF808080595959000000595959FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF27272700000000000000272727FFFFFFFFFFFFFFFFFFFFFFFF
                272727000000000000000000595959FFFFFFFFFFFFFFFFFFFFFFFF2727270000
                0000000000272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF27272700000000FFFFFF000000272727FF
                FFFFFFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFFFFFFFFFFFFFFFFFF
                272727000000FFFFFF00FFFFFF000000272727FFFFFFFFFFFFFFFFFFFFFFFF27
                2727000000272727FFFFFFFFFFFFFFFFFFFFFFFF272727000000FFFFFF00FFFF
                FFFFFFFF000000272727FFFFFFFFFFFFFFFFFF808080272727808080FFFFFFFF
                FFFFFFFFFF272727000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF0000002727
                27272727FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF272727272727000000FFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF0000000000002727272727272727
                27272727272727000000000000FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00}
              OnClick = NewMediaFormatInfoButtonClick
            end
            object AddNewMediaLocationButton: TSpeedButton
              Left = 46
              Top = 38
              Width = 167
              Height = 22
              Caption = 'Add New Media Location'
              Flat = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000000000
                000000000000000000000000000000000000000000000000000000000000C0C0
                C0C0C0C0C0C0C0C0C0C000000000808000FFFF00808000FFFF00808000FFFF00
                808000FFFF00808000FFFF000000C0C0C0C0C0C0C0C0C0C0C0C000000000FFFF
                00808000FFFF00808000FFFF00808000FFFF00808000FFFF008080000000C0C0
                C0C0C0C0C0C0C0C0C0C000000000808000FFFF00808000FFFF00808000FFFF00
                808000FFFF00808000FFFF000000C0C0C0C0C0C0C0C0C0C0C0C000000000FFFF
                00808000FFFF00808000FFFF00808000FFFF00808000FFFF008080000000C0C0
                C0C0C0C0C0C0C0C0C0C000000000808000FFFF00808000FFFF00808000FFFF00
                808000FFFF00808000FFFF000000C0C0C0C0C0C0000000C0C0C000000000FFFF
                00808000FFFF00808000FFFF00808000FFFF00808000FFFF008080000000C0C0
                C0000000C0C0C0C0C0C000000000808000FFFF00808000FFFF00808000FFFF00
                808000FFFF00808000FFFF000000000000C0C0C0C0C0C0C0C0C0000000000000
                000000000000000000000000000000000000000000000000000000C0C0C0C0C0
                C0000000C0C0C0000000C0C0C000000000FFFF00808000FFFF008080000000C0
                C0C0C0C0C0C0C0C0000000C0C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                000000000000000000000000C0C0C0C0C0C0C0C0C0000000C0C0C0000000C0C0
                C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0}
              ParentFont = False
            end
            object NoClipSchemeFoundWarningButton: TSpeedButton
              Left = 212
              Top = 67
              Width = 23
              Height = 22
              Anchors = [akLeft, akBottom]
              Flat = True
              Glyph.Data = {
                DE030000424DDE03000000000000360000002800000011000000120000000100
                180000000000A80300000000000000000000000000000000000080FFFF80FFFF
                80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FF
                FF80FFFF80FFFF80FFFF80FFFF000000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF80FF
                FF000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF80FFFF000000FF0000FF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFF0000FF
                0000FF0000FF80FFFF008080FF0000FF0000FF8080FFFFFFFFFFFFFFFFFFFF00
                0000000000FFFFFFFFFFFFFFFFFF8080FF0000FF0000FF8080FF80FFFF0080FF
                FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF0000FF0000FF0000FF80FFFF80FFFF0080FFFF8080FF0000FF0000FF8080
                FFFFFFFFFFFFFF000000000000FFFFFFFFFFFF8080FF0000FF0000FF8080FF80
                FFFF80FFFF0080FFFF80FFFF0000FF0000FF0000FFFFFFFFFFFFFF0000000000
                00FFFFFFFFFFFF0000FF0000FF0000FF80FFFF80FFFF80FFFF0080FFFF80FFFF
                8080FF0000FF0000FF8080FFFFFFFF000000000000FFFFFF8080FF0000FF0000
                FF8080FF80FFFF80FFFF80FFFF0080FFFF80FFFF80FFFF0000FF0000FF0000FF
                FFFFFF000000000000FFFFFF0000FF0000FF0000FF80FFFF80FFFF80FFFF80FF
                FF0080FFFF80FFFF80FFFF8080FF0000FF0000FF8080FF0000000000008080FF
                0000FF0000FF8080FF80FFFF80FFFF80FFFF80FFFF0080FFFF80FFFF80FFFF80
                FFFF0000FF0000FF0000FFFFFFFFFFFFFF0000FF0000FF0000FF80FFFF80FFFF
                80FFFF80FFFF80FFFF0080FFFF80FFFF80FFFF80FFFF8080FF0000FF0000FF80
                80FF8080FF0000FF0000FF8080FF80FFFF80FFFF80FFFF80FFFF80FFFF0080FF
                FF80FFFF80FFFF80FFFF80FFFF0000FF0000FF0000FF0000FF0000FF0000FF80
                FFFF80FFFF80FFFF80FFFF80FFFF80FFFF0080FFFF80FFFF80FFFF80FFFF80FF
                FF8080FF0000FF0000FF0000FF0000FF8080FF80FFFF80FFFF80FFFF80FFFF80
                FFFF80FFFF0080FFFF80FFFF80FFFF80FFFF80FFFF80FFFF0000FF0000FF0000
                FF0000FF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF0080FFFF80FFFF
                80FFFF80FFFF80FFFF80FFFF8080FF0000FF0000FF8080FF80FFFF80FFFF80FF
                FF80FFFF80FFFF80FFFF80FFFF0080FFFF80FFFF80FFFF80FFFF80FFFF80FFFF
                80FFFF0000FF0000FF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FFFF80FF
                FF00}
              OnClick = NoClipSchemeFoundWarningButtonClick
            end
            object FileRepositoryStorageFormatComboBox: TComboBox
              Left = 46
              Top = 68
              Width = 167
              Height = 21
              Style = csDropDownList
              TabOrder = 2
              TabStop = False
              OnChange = ClipFormatComponentChanged
            end
            object VideoStoreMediaStorageComboBox: TComboBox
              Left = 46
              Top = 68
              Width = 167
              Height = 21
              Style = csDropDownList
              TabOrder = 0
              TabStop = False
              OnChange = ClipFormatComponentChanged
            end
            object VideoStoreUsagePositioningPanel1: TPanel
              Left = 254
              Top = 14
              Width = 200
              Height = 87
              BevelOuter = bvNone
              TabOrder = 1
            end
            object LocationComboBoxPositioningPanel1: TPanel
              Left = 46
              Top = 14
              Width = 167
              Height = 21
              BevelOuter = bvNone
              TabOrder = 3
              object MediaLocationComboBox: TComboBox
                Left = 4
                Top = 3
                Width = 167
                Height = 21
                Style = csDropDownList
                TabOrder = 0
                TabStop = False
                OnChange = MediaLocationComboBoxChange
              end
            end
          end
          object VideoTImecodePanel: TPanel
            Left = 212
            Top = 14
            Width = 220
            Height = 129
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object TCDurationLabel: TLabel
              Left = 16
              Top = 104
              Width = 40
              Height = 13
              Caption = 'Duration'
            end
            object PanelCentral: TPanel
              Left = 4
              Top = 22
              Width = 217
              Height = 70
              BevelOuter = bvNone
              TabOrder = 1
              Visible = False
              object TCCenterLabel: TLabel
                Left = 8
                Top = 46
                Width = 19
                Height = 13
                Caption = 'Cen'
              end
              object VTimeCodeEditCen: VTimeCodeEdit
                Left = 60
                Top = 40
                Width = 113
                Height = 24
                Hint = 'Center time code'
                TabStop = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                AllowVTRCopy = False
                OnDropFrame = VTimeCodeChange
                OnTimecodeChange = VTimeCodeChange
              end
              object CenCueButton: TBitBtn
                Left = 173
                Top = 41
                Width = 25
                Height = 25
                Hint = 'Cue VTR to Timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                  F000FFFFF0FFFFFFF000FFFF00FFFFFFF000FFF000FFFFFFF000FF0000FFFFFF
                  F000F0000000000000000000000000000000F000000000000000FF0000FFFFFF
                  F000FFF000FFFFFFF000FFFF00FFFFFFF000FFFFF0FFFFFFF000FFFFFFFFFFFF
                  F000}
                TabOrder = 1
                TabStop = False
                OnClick = CenCueButtonClick
              end
              object CenCopyButton: TBitBtn
                Left = 35
                Top = 40
                Width = 25
                Height = 25
                Hint = 'Copy from current VTR timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF0FFFF
                  F000FFFFFFF00FFFF000FFFFFFF000FFF000FFFFFFF0000FF000000000000000
                  F0000000000000000000000000000000F000000FFFF0000FF000000FFFF000FF
                  F000000FFFF00FFFF000000FFFF0FFFFF000000FFFFFFFFFF000000FFFFFFFFF
                  F000}
                TabOrder = 2
                TabStop = False
                OnClick = CenCopyButtonClick
              end
              object VTimeCodeEditInSmall: VTimeCodeEdit
                Left = 12
                Top = 8
                Width = 89
                Height = 19
                Hint = 'In time code'
                TabStop = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                AllowVTRCopy = False
                OnDropFrame = VTimeCodeChange
                OnTimecodeChange = VTimeCodeChange
              end
              object VTimeCodeEditOutSmall: VTimeCodeEdit
                Left = 109
                Top = 8
                Width = 89
                Height = 19
                Hint = 'Out time code'
                TabStop = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                AllowVTRCopy = False
                OnDropFrame = VTimeCodeChange
                OnTimecodeChange = VTimeCodeChange
              end
            end
            object PanelInOut: TPanel
              Left = 4
              Top = 22
              Width = 217
              Height = 70
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object TCInLabel: TLabel
                Left = 13
                Top = 14
                Width = 9
                Height = 13
                Caption = 'In'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object TCOutLabel: TLabel
                Left = 12
                Top = 46
                Width = 17
                Height = 13
                Caption = 'Out'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object VTimeCodeEditIn: VTimeCodeEdit
                Left = 60
                Top = 8
                Width = 113
                Height = 24
                Hint = 'In time code'
                TabStop = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                AllowVTRCopy = False
                OnDropFrame = VTimeCodeChange
                OnTimecodeChange = VTimeCodeChange
              end
              object InCueButton: TBitBtn
                Left = 173
                Top = 9
                Width = 25
                Height = 25
                Hint = 'Cue VTR to Timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                  F000FFFFF0FFFFFFF000FFFF00FFFFFFF000FFF000FFFFFFF000FF0000FFFFFF
                  F000F0000000000000000000000000000000F000000000000000FF0000FFFFFF
                  F000FFF000FFFFFFF000FFFF00FFFFFFF000FFFFF0FFFFFFF000FFFFFFFFFFFF
                  F000}
                TabOrder = 1
                TabStop = False
                OnClick = InCueButtonClick
              end
              object InCopyButton: TBitBtn
                Left = 35
                Top = 9
                Width = 25
                Height = 25
                Hint = 'Copy from current VTR timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF0FFFF
                  F000FFFFFFF00FFFF000FFFFFFF000FFF000FFFFFFF0000FF000000000000000
                  F0000000000000000000000000000000F000000FFFF0000FF000000FFFF000FF
                  F000000FFFF00FFFF000000FFFF0FFFFF000000FFFFFFFFFF000000FFFFFFFFF
                  F000}
                TabOrder = 2
                TabStop = False
                OnClick = InCopyButtonClick
              end
              object VTimeCodeEditOut: VTimeCodeEdit
                Left = 60
                Top = 40
                Width = 113
                Height = 24
                Hint = 'Out time code'
                TabStop = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                AllowVTRCopy = False
                OnDropFrame = VTimeCodeChange
                OnTimecodeChange = VTimeCodeChange
              end
              object OutCueButton: TBitBtn
                Left = 173
                Top = 41
                Width = 25
                Height = 25
                Hint = 'Cue VTR to Timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                  F000FFFFF0FFFFFFF000FFFF00FFFFFFF000FFF000FFFFFFF000FF0000FFFFFF
                  F000F0000000000000000000000000000000F000000000000000FF0000FFFFFF
                  F000FFF000FFFFFFF000FFFF00FFFFFFF000FFFFF0FFFFFFF000FFFFFFFFFFFF
                  F000}
                TabOrder = 4
                TabStop = False
                OnClick = OutCueButtonClick
              end
              object OutCopyButton: TBitBtn
                Left = 35
                Top = 41
                Width = 25
                Height = 25
                Hint = 'Copy from current VTR timecode'
                Glyph.Data = {
                  DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                  04000000000068000000C40E0000C40E00001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF0FFFF
                  F000FFFFFFF00FFFF000FFFFFFF000FFF000FFFFFFF0000FF000000000000000
                  F0000000000000000000000000000000F000000FFFF0000FF000000FFFF000FF
                  F000000FFFF00FFFF000000FFFF0FFFFF000000FFFFFFFFFF000000FFFFFFFFF
                  F000}
                TabOrder = 5
                TabStop = False
                OnClick = OutCopyButtonClick
              end
            end
            object CheckBoxCentralize: TCheckBox
              Left = 41
              Top = 5
              Width = 65
              Height = 17
              TabStop = False
              Caption = 'Centralize'
              TabOrder = 2
              OnClick = CheckBoxCentralizeClick
            end
            object VTimeCodeEditDur: VTimeCodeEdit
              Left = 64
              Top = 99
              Width = 113
              Height = 24
              Hint = 'Duration'
              TabStop = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 3
              AllowVTRCopy = False
              OnDropFrame = VTimeCodeChange
              OnTimecodeChange = VTimeCodeChange
            end
            object DurCopyButton: TBitBtn
              Left = 39
              Top = 100
              Width = 25
              Height = 25
              Hint = 'Copy from duration of current clip'
              Enabled = False
              Glyph.Data = {
                DE000000424DDE0000000000000076000000280000000D0000000D0000000100
                04000000000068000000C40E0000C40E00001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF0FFFF
                F000FFFFFFF00FFFF000FFFFFFF000FFF000FFFFFFF0000FF000000000000000
                F0000000000000000000000000000000F000000FFFF0000FF000000FFFF000FF
                F000000FFFF00FFFF000000FFFF0FFFFF000000FFFFFFFFFF000000FFFFFFFFF
                F000}
              TabOrder = 4
              TabStop = False
              Visible = False
              OnClick = DurCopyButtonClick
            end
          end
          object CreateClipButtonPanel: TPanel
            Left = 444
            Top = 115
            Width = 464
            Height = 33
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            TabOrder = 3
            DesignSize = (
              464
              33)
            object VideoClipNameLabel: TLabel
              Left = 2
              Top = 3
              Width = 28
              Height = 26
              Anchors = [akLeft, akTop, akBottom]
              Caption = 'Clip'#13#10'Name'
            end
            object CreateMultipleClipsButton: TSpeedButton
              Left = 356
              Top = 4
              Width = 100
              Height = 25
              Hint = 'Create many clips at once'
              Anchors = [akLeft, akTop, akBottom]
              Caption = 'Create Multiple'
              Flat = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              Glyph.Data = {
                B2050000424DB205000000000000360400002800000012000000130000000100
                0800000000007C01000000000000000000000001000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A6000020400000206000002080000020A0000020C0000020E000004000000040
                20000040400000406000004080000040A0000040C0000040E000006000000060
                20000060400000606000006080000060A0000060C0000060E000008000000080
                20000080400000806000008080000080A0000080C0000080E00000A0000000A0
                200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
                200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
                200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
                20004000400040006000400080004000A0004000C0004000E000402000004020
                20004020400040206000402080004020A0004020C0004020E000404000004040
                20004040400040406000404080004040A0004040C0004040E000406000004060
                20004060400040606000406080004060A0004060C0004060E000408000004080
                20004080400040806000408080004080A0004080C0004080E00040A0000040A0
                200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
                200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
                200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
                20008000400080006000800080008000A0008000C0008000E000802000008020
                20008020400080206000802080008020A0008020C0008020E000804000008040
                20008040400080406000804080008040A0008040C0008040E000806000008060
                20008060400080606000806080008060A0008060C0008060E000808000008080
                20008080400080806000808080008080A0008080C0008080E00080A0000080A0
                200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
                200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
                200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
                2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
                2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
                2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
                2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
                2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
                2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
                2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF0000000000
                00000000000000FFFFFFFFFF0000FF000707070707FB9B07070700FFFFFFFFFF
                0000FF000707070707FB9B070707000000FFFFFF0000FF000707070707FB9B07
                0707000700FFFFFF0000FF000707FB9B07FB9B07FB070007000000FF0000FF00
                070707FB9BFB9BFB07070007000700FF0000FF00079B9B9BFBFBFB9B9B9B0007
                000700FF0000FF0007FBFBFBFBFBFBFBFBFB0007000700FF0000FF000707079B
                07FBFB9B07070007000700FF0000FF0007079BFB07FB9BFB9B070007000700FF
                0000FF000000000007FB9B07FB070007000700FF0000FFFF0007070007FB9B07
                07070007000700FF0000FFFFFF00070007FB9B0707070007000700FF0000FFFF
                FFFF000007FB070707070007000700FF0000FFFFFFFFFF000000000000000007
                000700FF0000FFFFFFFFFFFF0007070707070707000700FF0000FFFFFFFFFFFF
                FF00000000000000000700FF0000FFFFFFFFFFFFFFFF000707070707070700FF
                0000FFFFFFFFFFFFFFFFFF0000000000000000FF0000}
              ParentFont = False
              OnClick = ImportTimecodesItemClick
            end
            object VideoClipNameEdit: TEdit
              Left = 49
              Top = 8
              Width = 167
              Height = 19
              TabStop = False
              Anchors = [akLeft, akTop, akBottom]
              TabOrder = 0
              OnKeyPress = VideoClipNameEditKeyPress
            end
            object CreateVideoClipButton: TBitBtn
              Left = 222
              Top = 4
              Width = 100
              Height = 25
              Hint = 'Creates a new clip from the parameters'
              Anchors = [akLeft, akTop, akBottom]
              Caption = 'Create Single'
              Glyph.Data = {
                36050000424D3605000000000000360400002800000010000000100000000100
                0800000000000001000000000000000000000001000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A6000020400000206000002080000020A0000020C0000020E000004000000040
                20000040400000406000004080000040A0000040C0000040E000006000000060
                20000060400000606000006080000060A0000060C0000060E000008000000080
                20000080400000806000008080000080A0000080C0000080E00000A0000000A0
                200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
                200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
                200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
                20004000400040006000400080004000A0004000C0004000E000402000004020
                20004020400040206000402080004020A0004020C0004020E000404000004040
                20004040400040406000404080004040A0004040C0004040E000406000004060
                20004060400040606000406080004060A0004060C0004060E000408000004080
                20004080400040806000408080004080A0004080C0004080E00040A0000040A0
                200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
                200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
                200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
                20008000400080006000800080008000A0008000C0008000E000802000008020
                20008020400080206000802080008020A0008020C0008020E000804000008040
                20008040400080406000804080008040A0008040C0008040E000806000008060
                20008060400080606000806080008060A0008060C0008060E000808000008080
                20008080400080806000808080008080A0008080C0008080E00080A0000080A0
                200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
                200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
                200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
                2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
                2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
                2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
                2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
                2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
                2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
                2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFF00070707
                0707079B07070700FFFFFFFF000707070707FB9B07070700FFFFFFFF00070707
                0707FB9B07070700FFFFFFFF000707FB9B07FB9B07FB0700FFFFFFFF00070707
                FB9BFB9BFB070700FFFFFFFF00079B9B9BFBFBFB9B9B9B00FFFFFFFF0007FBFB
                FBFBFBFBFBFBFB00FFFFFFFF000707079B07FBFB9B070700FFFFFFFF0007079B
                FB07FB9BFB9B0700FFFFFFFF000000000007FB9B07FB0700FFFFFFFFFF000707
                0007FB9B07070700FFFFFFFFFFFF00070007FB9B07070700FFFFFFFFFFFFFF00
                0007FB0707070700FFFFFFFFFFFFFFFF0000000000000000FFFF}
              TabOrder = 1
              TabStop = False
              OnClick = CreateClipButtonClick
            end
          end
        end
        object NewClipExistingMediaPanel: TPanel
          Left = 0
          Top = 21
          Width = 1234
          Height = 149
          Anchors = [akLeft, akTop, akRight]
          BevelOuter = bvNone
          Constraints.MaxWidth = 1280
          Constraints.MinWidth = 1024
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          DesignSize = (
            1234
            149)
          object FileClipFrameIndexPanel: TPanel
            Left = 195
            Top = 3
            Width = 515
            Height = 107
            Anchors = [akRight, akBottom]
            BevelOuter = bvNone
            Constraints.MinWidth = 515
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            DesignSize = (
              515
              107)
            object FilelipFirstLabel: TLabel
              Left = 385
              Top = 9
              Width = 25
              Height = 16
              Anchors = [akTop, akRight]
              Caption = 'First'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 13
            end
            object FileClipLastLabel: TLabel
              Left = 386
              Top = 40
              Width = 23
              Height = 16
              Anchors = [akTop, akRight]
              Caption = 'Last'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 14
            end
            object FileCLipDurLabel: TLabel
              Left = 385
              Top = 76
              Width = 20
              Height = 16
              Anchors = [akTop, akRight]
              Caption = 'Dur'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 13
            end
            object InFileClipEdit: TEdit
              Left = 416
              Top = 8
              Width = 96
              Height = 20
              Hint = 'First frame number'
              TabStop = False
              Alignment = taCenter
              Anchors = [akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = 15
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              Text = '00000000'
              OnExit = InOutFileClipEditExit
              OnKeyPress = InOutFileClipEditKeyPress
            end
            object OutFileClipEdit: TEdit
              Left = 416
              Top = 39
              Width = 96
              Height = 20
              Hint = 'Last frame number'
              TabStop = False
              Alignment = taCenter
              Anchors = [akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = 15
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Text = '00000000'
              OnExit = InOutFileClipEditExit
              OnKeyPress = InOutFileClipEditKeyPress
            end
            object DurFileClipEdit: TEdit
              Left = 416
              Top = 75
              Width = 96
              Height = 20
              Hint = 'Number of frames'
              TabStop = False
              Alignment = taCenter
              Anchors = [akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = 15
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              Text = '00000000'
              OnExit = DurFileClipEditExit
              OnKeyPress = DurFileClipEditKeyPress
            end
          end
          object FileHeaderInfoPanel: TPanel
            Left = 738
            Top = 3
            Width = 481
            Height = 108
            Anchors = [akRight, akBottom]
            BevelOuter = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            DesignSize = (
              481
              108)
            object Panel1: TPanel
              Left = 4
              Top = 4
              Width = 309
              Height = 89
              BevelOuter = bvNone
              Constraints.MaxWidth = 309
              Constraints.MinWidth = 309
              TabOrder = 2
              DesignSize = (
                309
                89)
              object FileDetailsFileNameLabel: TLabel
                Left = 16
                Top = 12
                Width = 57
                Height = 13
                Caption = 'File Name:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object FileDetailsFormatLabel: TLabel
                Left = 16
                Top = 28
                Width = 44
                Height = 13
                Caption = 'Format:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object FileDetailsDimensionsLabel: TLabel
                Left = 16
                Top = 44
                Width = 68
                Height = 13
                Caption = 'Dimensions:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object FileDetailsTimecodeLabel: TLabel
                Left = 16
                Top = 60
                Width = 58
                Height = 13
                Caption = 'Timecode:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object FileDetailsFileNameValue: TLabel
                Left = 96
                Top = 12
                Width = 96
                Height = 13
                Caption = 'File name goes here'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object FileDetailsFormatValue: TLabel
                Left = 96
                Top = 28
                Width = 94
                Height = 13
                Caption = 'File Type goes here'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object FileDetailsDimensionsValue: TLabel
                Left = 96
                Top = 44
                Width = 72
                Height = 13
                Caption = 'NNNN x MMMM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object FileDetailsTimecodeValue: TLabel
                Left = 96
                Top = 60
                Width = 66
                Height = 13
                Caption = 'HH:MM:SS:FF'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object FileDetailsShowHeaderButton: TSpeedButton
                Left = 185
                Top = 56
                Width = 100
                Height = 25
                Hint = 'Show file header for current clip frame.'
                Anchors = [akRight, akBottom]
                Caption = 'Show Header'
                Enabled = False
                Flat = True
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsUnderline]
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  1800000000000003000000000000000000000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000808000C0C0C0FF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  808000008080008080008080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF008080008080008080008080C0
                  C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FF008080008080008080008080C0C0C0C0C0C0C0C0C0FF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00808000808080
                  8080408080408080408080C0C0C0C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FF66B3B3808000FFFF80FFFF80FFFF8080800066B3
                  B3C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000CA
                  CA00FFFF80FFFF80FFFF80FFFF80808000C0C0C0C0C0C0FF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FF408080CACA00FFFF80FFFF80FFFF80FFFF80FFFF80FFFF
                  80808080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF408080CACA00FF
                  FF80FFFF80FFFF80FFFF80FFFF80FFFF80408080C0C0C0FF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FF408080CACA00FFFF80FFFF80FFFF80FFFF80FFFF80FFFF
                  80808080C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808000CA
                  CA00FFFF80FFFF80FFFF80FFFF80808000C0C0C0FF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FF66B3B3808000CACA00CACA00CACA0080800066B3
                  B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                  00FF408080408080408080FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                  00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                ParentFont = False
                OnClick = FileDetailsShowHeaderButtonClick
              end
            end
            object FileDetailsTrackBar: TTrackBar
              Left = 16
              Top = 86
              Width = 300
              Height = 15
              Hint = 'Selaect which frame to preview'
              Anchors = [akLeft, akTop, akRight]
              Max = 1000
              PageSize = 8
              TabOrder = 0
              TabStop = False
              ThumbLength = 12
              TickMarks = tmTopLeft
              TickStyle = tsNone
              OnChange = FileDetailsTrackBarChange
            end
            object Panel5: TPanel
              Left = 341
              Top = 0
              Width = 140
              Height = 108
              Anchors = [akTop, akRight]
              TabOrder = 1
              inline FileDetailsProxyFrame: TNakedProxyFrame
                Left = 0
                Top = 0
                Width = 140
                Height = 108
                TabOrder = 0
                ExplicitHeight = 108
                inherited Panel1: TPanel
                  Height = 108
                  BevelInner = bvNone
                  BevelOuter = bvNone
                  Caption = ''
                  ExplicitHeight = 108
                  inherited ProxyPaintBox: TPaintBox
                    Anchors = [akTop, akRight]
                  end
                  inherited BusyArrowPanel: TPanel
                    Top = 71
                    Width = 32
                    Height = 32
                    ExplicitTop = 71
                    ExplicitWidth = 32
                    ExplicitHeight = 32
                    inherited BusyArrows: TMTIBusy
                      Left = 7
                      Top = 7
                      ExplicitLeft = 7
                      ExplicitTop = 7
                    end
                  end
                end
              end
            end
          end
          object FilePick: TPanel
            Left = 0
            Top = 3
            Width = 560
            Height = 65
            Anchors = [akLeft, akTop, akRight]
            BevelOuter = bvNone
            Constraints.MaxWidth = 560
            Constraints.MinWidth = 360
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              560
              65)
            object ClipFolderLabel: TLabel
              Left = 12
              Top = 9
              Width = 36
              Height = 16
              Caption = 'Folder'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object ClipFileNameLabel: TLabel
              Left = 12
              Top = 40
              Width = 57
              Height = 16
              Caption = 'File Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object FileClipFileNameEdit: TEdit
              Left = 72
              Top = 39
              Width = 454
              Height = 19
              Hint = 
                'File name prototype; e.g., MyFile.####.dpx, use "#" to indicate ' +
                'where frame number appears'
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnChange = FileClipFileNameEditChange
              OnExit = FileClipFileNameEditExit
            end
            object FileClipFolderEdit: TEdit
              Left = 72
              Top = 8
              Width = 454
              Height = 19
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnChange = FileClipFileNameEditChange
              OnExit = FileClipFileNameEditExit
            end
          end
          object CreateFileClipButtonPanel: TPanel
            Left = 200
            Top = 116
            Width = 1019
            Height = 27
            Anchors = [akRight, akBottom]
            BevelOuter = bvNone
            Constraints.MinWidth = 1019
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              1019
              27)
            object Label7: TLabel
              Left = 386
              Top = 4
              Width = 58
              Height = 16
              Anchors = [akTop, akRight]
              Caption = 'Clip Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = False
            end
            object MultiBrowseButton: TSpeedButton
              Left = 916
              Top = 0
              Width = 100
              Height = 25
              Hint = 'Browse for multiple clips'
              Anchors = [akTop, akRight]
              Caption = 'Create Multiple'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                3A080000424D3A08000000000000360000002800000024000000130000000100
                18000000000004080000C40E0000C40E000000000000000000006A6A6A000000
                0000000000000000000000000000000000000000000000000000000000000000
                006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525252525252525252525252
                52525252525252525252525252525252525252525252526A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000FFFF8060
                60C0C0C0C0C0C0C0C0C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
                52526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C000FFFF806060C0C0C0C0C0C0C0C0C00000000000000000006A
                6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252526A6A6A6A6A6A6A6A6A6A6A
                6A000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000FFFF806060C0C0C0C0C0C0C0
                C0C0000000C0C0C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252
                526A6A6A6A6A6A6A6A6A6A6A6A000000C0C0C0C0C0C000FFFF806060C0C0C000
                FFFF806060C0C0C000FFFFC0C0C0000000C0C0C00000000000000000006A6A6A
                6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A5252526A6A6A5252525252525252526A6A6A6A6A6A000000C0C0C0C0
                C0C0C0C0C000FFFF80606000FFFF80606000FFFFC0C0C0C0C0C0000000C0C0C0
                000000C0C0C00000006A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A5252526A
                6A6A6A6A6A000000C0C0C080606080606080606000FFFF00FFFF00FFFF806060
                806060806060000000C0C0C0000000C0C0C00000006A6A6A6A6A6A5252526A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A
                6A6A5252526A6A6A5252526A6A6A6A6A6A000000C0C0C000FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000C0C0C0000000C0C0C00000
                006A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A5252526A6A6A6A6A6A000000
                C0C0C0C0C0C0C0C0C0806060C0C0C000FFFF00FFFF806060C0C0C0C0C0C00000
                00C0C0C0000000C0C0C00000006A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A
                5252526A6A6A6A6A6A000000C0C0C0C0C0C080606000FFFFC0C0C000FFFF8060
                6000FFFF806060C0C0C0000000C0C0C0000000C0C0C00000006A6A6A6A6A6A52
                52526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                5252526A6A6A5252526A6A6A5252526A6A6A6A6A6A0000000000000000000000
                00000000C0C0C000FFFF806060C0C0C000FFFFC0C0C0000000C0C0C0000000C0
                C0C00000006A6A6A6A6A6A5252525252525252525252525252526A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A5252526A6A6A6A6A
                6A6A6A6A000000C0C0C0C0C0C0000000C0C0C000FFFF806060C0C0C0C0C0C0C0
                C0C0000000C0C0C0000000C0C0C00000006A6A6A6A6A6A6A6A6A5252526A6A6A
                6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252
                526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A000000C0C0C0000000C0C0C000
                FFFF806060C0C0C0C0C0C0C0C0C0000000C0C0C0000000C0C0C00000006A6A6A
                6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A5252526A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A000000000000C0C0C000FFFFC0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0
                000000C0C0C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252526A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A5252526A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000000000000000000000000000
                000000000000000000C0C0C0000000C0C0C00000006A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A5252525252525252525252525252525252525252525252526A
                6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C00000
                006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000
                00000000000000C0C0C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A5252525252525252525252525252525252525252525252526A6A6A
                5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000
                00C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000006A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A00000000000000000000000000000000000000
                00000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A5252525252525252525252525252525252525252525252526A6A6A}
              NumGlyphs = 2
              ParentFont = False
              OnClick = MultiBrowseButtonClick
            end
            object CreateFileClipButton: TBitBtn
              Left = 810
              Top = 0
              Width = 100
              Height = 25
              Hint = 'Creates a new clip from the parameters'
              Anchors = [akTop, akRight]
              Caption = 'Create Single'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000000000000000000000000000000000006A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                0000000000000000000000000000000000000000000000000000000000000000
                000000006A6A6A6A6A6A6A6A6A6A6A6A52525252525252525252525252525252
                52525252525252525252525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0806060C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000FFFF806060C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C000FFFF806060C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C000FFFF806060C0C0C000FFFF806060C0C0C000FFFFC0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C000FFFF80606000FFFF80606000FFFFC0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C080606080606080606000FFFF00FFFF00FFFF8060608060608060
                600000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF0000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C0C0C0C0806060C0C0C000FFFF00FFFF806060C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000C0C0C0C0C0C080606000FFFFC0C0C000FFFF80606000FFFF806060C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                000000000000000000000000000000C0C0C000FFFF806060C0C0C000FFFFC0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525252526A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A000000C0C0C0C0C0C0000000C0C0C000FFFF806060C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A5252526A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A000000C0C0C0000000C0C0C000FFFF806060C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A5252526A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A000000000000C0C0C000FFFFC0C0C0C0C0C0C0C0C0C0C0
                C00000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252526A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000000000
                000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525252
                52525252525252525252525252525252525252526A6A6A6A6A6A}
              NumGlyphs = 2
              ParentFont = False
              TabOrder = 1
              TabStop = False
              OnClick = CreateFileClipButtonClick
            end
            object FileClipNameEdit: TEdit
              Left = 453
              Top = 3
              Width = 348
              Height = 19
              TabStop = False
              Anchors = [akTop, akRight]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = False
              TabOrder = 0
              OnKeyPress = FileClipNameEditKeyPress
            end
          end
          object FileClipTCPanel: TPanel
            Left = 1
            Top = 112
            Width = 360
            Height = 37
            Anchors = [akLeft, akBottom]
            BevelOuter = bvNone
            Constraints.MaxWidth = 360
            Constraints.MinWidth = 360
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object Label20: TLabel
              Left = 11
              Top = 8
              Width = 65
              Height = 32
              AutoSize = False
              Caption = 'Timecode'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object FileClipInTCLabel: TLabel
              Left = 100
              Top = 8
              Width = 11
              Height = 16
              Caption = 'In'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object FileClipDFLabel: TLabel
              Left = 212
              Top = 8
              Width = 15
              Height = 16
              Caption = 'DF'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object FileClipFPSLabel: TLabel
              Left = 258
              Top = 8
              Width = 22
              Height = 16
              Caption = 'FPS'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object FileClipTCCheckBox: TCheckBox
              Left = 72
              Top = 8
              Width = 15
              Height = 17
              TabStop = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnClick = FileClipTCCheckBoxClick
            end
            object FileClipInTCEdit: VTimeCodeEdit
              Left = 117
              Top = 7
              Width = 80
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              AllowVTRCopy = False
            end
            object FileClipDFCheckBox: TCheckBox
              Left = 232
              Top = 8
              Width = 15
              Height = 17
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
            object FileClipFPSComboBox: TComboBox
              Left = 281
              Top = 7
              Width = 45
              Height = 21
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              Text = '24'
              Items.Strings = (
                '24'
                '25'
                '30')
            end
          end
          object ExistingMediaFormatPanel: TPanel
            Left = 0
            Top = 74
            Width = 560
            Height = 29
            Anchors = [akLeft, akTop, akRight]
            BevelOuter = bvNone
            Constraints.MaxWidth = 560
            Constraints.MinWidth = 360
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            DesignSize = (
              560
              29)
            object ExistingMediaFormatLabel: TLabel
              Left = 12
              Top = 5
              Width = 41
              Height = 16
              Caption = 'Format'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object ExistingMediaFormatInfoButton: TSpeedButton
              Left = 532
              Top = 3
              Width = 23
              Height = 23
              Hint = 'Show media format information'
              Anchors = [akRight, akBottom]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Glyph.Data = {
                1E070000424D1E07000000000000360000002800000022000000110000000100
                180000000000E8060000000000000000000000000000000000006A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A5252520000000000000000000000000000005252526A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A66666652
                52525252525252525252525252526666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                00006A6A6A6A6A6A6A6A6A525252000000000000000000000000000000000000
                0000000000000000005252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6666
                665252525252525252525252525252525252525252525252525252526666666A
                6A6A6A6A6A6A6A6A00006A6A6A6A6A6A5252520000000000000000005252526A
                6A6A6A6A6A6A6A6A5252522727272727270000005252526A6A6A6A6A6A6A6A6A
                6A6A6A6666665252525252525252526666666A6A6A6A6A6A6A6A6A6666665252
                525252525252526666666A6A6A6A6A6A00006A6A6A5252520000000000005252
                526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525227272700000052
                52526A6A6A6A6A6A6666665252525252526666666A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6666665252525252526666666A6A6A00006A6A6A000000
                0000005252526A6A6A6A6A6A5252520000000000000000005252526A6A6A6A6A
                6A5252522727270000006A6A6A6A6A6A5252525252526666666A6A6A6A6A6A66
                66665252525252525252526666666A6A6A6A6A6A6666665252525252526A6A6A
                00005252520000000000006A6A6A6A6A6A6A6A6A6A6A6A525252000000525252
                6A6A6A6A6A6A6A6A6A6A6A6A2727270000005252526666665252525252526A6A
                6A6A6A6A6A6A6A6A6A6A6666665252526666666A6A6A6A6A6A6A6A6A6A6A6A52
                525252525266666600000000002727275252526A6A6A6A6A6A6A6A6A6A6A6A52
                52520000005252526A6A6A6A6A6A6A6A6A6A6A6A525252000000000000525252
                5252526666666A6A6A6A6A6A6A6A6A6A6A6A6666665252526666666A6A6A6A6A
                6A6A6A6A6A6A6A66666652525252525200000000000000006A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A5252520000005252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00
                00000000005252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A666666525252
                6666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252520000000000000000
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252520000005252526A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A0000000000005252525252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6666665252526666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A525252525252
                00000000000000006A6A6A6A6A6A6A6A6A6A6A6A525252000000000000525252
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000005252525252526A6A6A6A6A
                6A6A6A6A6A6A6A6666665252525252526666666A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A52525252525200000000000000005252526A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A525252000000000000525252
                5252526666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                6A6A6A6A6A6A6A66666652525252525200005252520000000000006A6A6A6A6A
                6A6A6A6A6A6A6A5252520000005252526A6A6A6A6A6A6A6A6A6A6A6A00000000
                00005252526666665252525252526A6A6A6A6A6A6A6A6A6A6A6A666666525252
                6666666A6A6A6A6A6A6A6A6A6A6A6A52525252525266666600006A6A6A000000
                0000005252526A6A6A6A6A6A6A6A6A0000000000000000006A6A6A6A6A6A6A6A
                6A5252520000000000006A6A6A6A6A6A5252525252526666666A6A6A6A6A6A6A
                6A6A5252525252525252526A6A6A6A6A6A6A6A6A6666665252525252526A6A6A
                00006A6A6A5252520000000000005252526A6A6A6A6A6A525252000000525252
                6A6A6A6A6A6A5252520000000000005252526A6A6A6A6A6A6666665252525252
                526666666A6A6A6A6A6A6666665252526666666A6A6A6A6A6A66666652525252
                52526666666A6A6A00006A6A6A6A6A6A5252520000000000000000005252526A
                6A6A6A6A6A6A6A6A5252520000000000000000005252526A6A6A6A6A6A6A6A6A
                6A6A6A6666665252525252525252526666666A6A6A6A6A6A6A6A6A6666665252
                525252525252526666666A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A5252520000
                000000002727272727272727272727270000000000000000005252526A6A6A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A666666525252525252525252525252525252
                5252525252525252525252526666666A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000005252526A6A
                6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A66666652
                52525252525252525252525252526666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                0000}
              NumGlyphs = 2
              ParentFont = False
              OnClick = ExistingMediaFormatInfoButtonClick
              ExplicitLeft = 332
            end
            object ExistingMediaFormatComboBox: TComboBox
              Left = 72
              Top = 4
              Width = 454
              Height = 21
              Style = csDropDownList
              Anchors = [akLeft, akRight, akBottom]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object NewClipMainSelectorPanel: TPanel
          Left = 8
          Top = 4
          Width = 86
          Height = 18
          AutoSize = True
          BevelOuter = bvNone
          TabOrder = 0
          object UnexpandCreateClipPanelButton_REMOVED_: TSpeedButton
            Left = 0
            Top = 0
            Width = 13
            Height = 13
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF0FFF0
              FFF0FF000FF0F00000F000000000FFFFFFF0FFFFFFF0}
            ParentFont = False
            Visible = False
            OnClick = UnexpandCreateClipPanelButton_REMOVED_Click
          end
          object CreateClipLabel: TLabel
            Left = 4
            Top = 2
            Width = 82
            Height = 16
            Caption = 'Create Clip '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
      end
    end
    object CreateClipFromFileOrFolderPanel: TPanel
      Left = 145
      Top = 8
      Width = 270
      Height = 25
      BevelOuter = bvNone
      TabOrder = 3
      object ChooseFolderRadioButton: TRadioButton
        Left = 3
        Top = 5
        Width = 93
        Height = 17
        Caption = 'Choose Folder'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = ChooseFolderOrChooseFileRadioButtonClick
      end
      object ChooseFileRadioButton: TRadioButton
        Left = 158
        Top = 5
        Width = 78
        Height = 17
        Caption = 'Choose File'
        TabOrder = 1
        OnClick = ChooseFolderOrChooseFileRadioButtonClick
      end
      object ClipFileBrowserButton: TBitBtn
        Left = 238
        Top = 2
        Width = 23
        Height = 23
        Hint = 'Browse for a single clip.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000CE0E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
          8888888888888888888888888888888888888888888888888888000000000008
          88887777777777788888003333333330888877888888888788880B0333333333
          088878788888888878880FB033333333308878878888888887880BFB03333333
          330878887888888888780FBFB0000000000078888777777777770BFBFBFBFB08
          888878888888887888880FBFBFBFBF08888878888888887888880BFB00000008
          8888788877777778888880008888888888888777888888888888888888888888
          8888888888888888888888888888888888888888888888888888888888888888
          8888888888888888888888888888888888888888888888888888}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 2
        OnClick = ClipFolderOrFileBrowserButtonClick
      end
      object ClipFolderBrowserButton: TBitBtn
        Left = 96
        Top = 2
        Width = 23
        Height = 23
        Hint = 'Browse for a single clip.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000CE0E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
          8888888888888888888888888888888888888888888888888888888888888888
          8888888888888888888888000000000000888877777777777788880FBFBFBFBF
          B0888878888888888788880BFBFBFBFBF0888878888888888788880FBFBFBFBF
          B0888878888888888788880BFBFBFBFBF0888878888888888788880FBFBFBFBF
          B0888878888888888788880BFBFBFBFBF0888878888888888788880FBFBFBFBF
          B0888878888888888788880BFBF0000000888878888777777788888000088888
          8888888777788888888888888888888888888888888888888888888888888888
          8888888888888888888888888888888888888888888888888888}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 3
        OnClick = ClipFolderOrFileBrowserButtonClick
      end
    end
    object ModalPanel: TPanel
      Left = 1041
      Top = 12
      Width = 201
      Height = 37
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      object OKButton: TBitBtn
        Left = 16
        Top = 10
        Width = 75
        Height = 25
        Caption = 'OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          0800000000000001000000000000000000000001000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A6000020400000206000002080000020A0000020C0000020E000004000000040
          20000040400000406000004080000040A0000040C0000040E000006000000060
          20000060400000606000006080000060A0000060C0000060E000008000000080
          20000080400000806000008080000080A0000080C0000080E00000A0000000A0
          200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
          200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
          200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
          20004000400040006000400080004000A0004000C0004000E000402000004020
          20004020400040206000402080004020A0004020C0004020E000404000004040
          20004040400040406000404080004040A0004040C0004040E000406000004060
          20004060400040606000406080004060A0004060C0004060E000408000004080
          20004080400040806000408080004080A0004080C0004080E00040A0000040A0
          200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
          200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
          200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
          20008000400080006000800080008000A0008000C0008000E000802000008020
          20008020400080206000802080008020A0008020C0008020E000804000008040
          20008040400080406000804080008040A0008040C0008040E000806000008060
          20008060400080606000806080008060A0008060C0008060E000808000008080
          20008080400080806000808080008080A0008080C0008080E00080A0000080A0
          200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
          200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
          200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
          2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
          2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
          2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
          2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
          2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
          2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
          2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080808080808
          0807F7F7F707080808080808080808A4591818185152F7070808080808081818
          292929292118509B070808080818212929323229292902189B0708081929292A
          323232323229290251F70808186A292929292A2A32322929185A086A722A2929
          FFFF29323232322902510818732929FFFFFFFF292A322A29215108187329FFFF
          2929FFFF292A29292951081874292929292929FFFF2929290251086A73732929
          29292929FFFF2929189B0808187C2929292929292929292918F70808AB6A7C29
          2929292929292918A408080808612A7C74736A72726A1863080808080808AC18
          6A73737221186A080808080808080808A56AA56AA50808080808}
        ModalResult = 1
        ParentFont = False
        TabOrder = 0
        TabStop = False
        OnClick = OKButtonClick
      end
      object CancelButton: TBitBtn
        Left = 108
        Top = 10
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Cancel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000CE0E0000D80E00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777777777777777777777770F77770F7777777777777000F777777
          0F7777000F777770F777777000F77700F7777777000F700F77777777700000F7
          7777777777000F7777777777700000F777777777000F70F77777770000F77700
          F77770000F7777700F77700F7777777700F77777777777777777}
        ModalResult = 2
        ParentFont = False
        TabOrder = 1
        TabStop = False
        OnClick = CancelButtonClick
      end
    end
  end
  object RightPanel: TPanel
    Left = 207
    Top = 0
    Width = 1044
    Height = 477
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 2
    object ClipListViewSplitter: TSplitter
      Left = 0
      Top = 372
      Width = 1044
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 76
      ExplicitTop = 330
      ExplicitWidth = 830
    end
    object ClipListView: TListView
      Left = 0
      Top = 0
      Width = 1044
      Height = 372
      Align = alClient
      Columns = <
        item
          AutoSize = True
          Caption = 'Name'
        end
        item
          AutoSize = True
          Caption = 'Comments'
        end
        item
          AutoSize = True
          Caption = 'Status'
        end
        item
          AutoSize = True
          Caption = 'Format'
        end
        item
          AutoSize = True
          Caption = 'In'
        end
        item
          AutoSize = True
          Caption = 'Out'
        end
        item
          AutoSize = True
          Caption = 'Duration'
        end
        item
          AutoSize = True
          Caption = 'Media Location'
        end
        item
          AutoSize = True
          Caption = 'Created'
        end>
      DoubleBuffered = True
      DragMode = dmAutomatic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      MultiSelect = True
      OwnerData = True
      ReadOnly = True
      RowSelect = True
      ParentDoubleBuffered = False
      ParentFont = False
      PopupMenu = ClipListPopupMenu
      TabOrder = 0
      TabStop = False
      ViewStyle = vsReport
      OnChanging = ClipListViewChanging
      OnColumnClick = ClipListViewColumnClick
      OnCustomDrawItem = ClipListViewCustomDrawItem
      OnCustomDrawSubItem = ClipListViewCustomDrawSubItem
      OnData = ClipListViewData
      OnDblClick = OpenClipMenuItemOrDoubleClick
      OnKeyPress = ClipListViewKeyPress
      OnSelectItem = ClipListViewSelectItem
    end
    object VersionListView: TListView
      Left = 0
      Top = 377
      Width = 1044
      Height = 100
      Align = alBottom
      Columns = <
        item
          Caption = 'Version'
          Width = 75
        end
        item
          Caption = 'Status'
          Width = 76
        end
        item
          Caption = 'Modified Total'
          Width = 86
        end
        item
          Caption = 'Created'
          Width = 120
        end
        item
          AutoSize = True
          Caption = 'Comments'
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      OwnerData = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = VersionListViewPopupMenu
      TabOrder = 1
      TabStop = False
      ViewStyle = vsReport
      OnColumnClick = VersionListViewColumnClick
      OnCustomDrawItem = VersionListViewCustomDrawItem
      OnCustomDrawSubItem = VersionListViewCustomDrawSubItem
      OnData = VersionListViewData
      OnKeyPress = VersionListViewKeyPress
      OnSelectItem = VersionListViewSelectItem
    end
  end
  object ClipSchemePopupMenu: TPopupMenu
    Alignment = paRight
    AutoHotkeys = maManual
    AutoPopup = False
    Left = 521
    Top = 76
    object SDMenuItem: TMenuItem
      Caption = 'SD'
    end
    object HDMenuItem: TMenuItem
      Caption = 'HD'
    end
    object ImageFileMenuItem: TMenuItem
      Caption = 'File'
      object TMenuItem
      end
    end
    object HSDLMenuItem: TMenuItem
      Caption = 'HSDL'
      object TMenuItem
      end
    end
  end
  object BinTreePopupMenu: TPopupMenu
    OnPopup = BinTreePopupMenuPopup
    Left = 16
    Top = 72
    object BinPopupCreateJobItem: TMenuItem
      Caption = 'Create &Project...'
      OnClick = FileMenuCreateJobItemClick
    end
    object BinPopupNewBinItem: TMenuItem
      Caption = 'Create &Bin...'
      OnClick = FileMenuCreateBinItemClick
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object BinPopupDeleteItem: TMenuItem
      Caption = '&Delete'
      OnClick = FileMenuDeleteBinItemClick
    end
    object BinPopupRenameItem: TMenuItem
      Caption = '&Rename...'
      OnClick = FileMenuRenameBinItemClick
    end
    object BinPopupSeparator2: TMenuItem
      Caption = '-'
    end
    object BinPopupShareInAndOutMarks: TMenuItem
      Caption = 'Share In and Out Marks'
      OnClick = BinPopupShareInAndOutMarksClick
    end
  end
  object OpenDialogXXX: TOpenDialog
    Filter = 
      'BMP|*.bmp|Cineon|*.cin|DPX|*.dpx|TGA|*.tga;*.tga2|TIFF|*.tif; *.' +
      'tiff|SGI|*.sgi;*.rgb|YUV|*.yuv; *.raw|All Image Files|*.bmp;*.ci' +
      'n;*.dpx; *.tga;*.tga2; *.tif; *.tiff;*.dpx; *.sgi;*.rgb;*.yuv; *' +
      '.raw|All Files|*.*'
    FilterIndex = 8
    Title = 'Browse For File Name'
    Left = 782
    Top = 78
  end
  object ClipListPopupMenu: TPopupMenu
    OnPopup = ClipListPopupMenuPopup
    Left = 272
    Top = 80
    object ClipPopupOpenClipItem: TMenuItem
      Caption = '&Open Clip'
      OnClick = OpenClipMenuItemOrDoubleClick
    end
    object ClipPopupCreateNewVersionItem: TMenuItem
      Caption = 'Create New &Version'
      OnClick = CreateNewVersionMenuItemClick
    end
    object ClipPopupSeparator0: TMenuItem
      Caption = '-'
    end
    object ClipPopupDeleteItem: TMenuItem
      Caption = '&Delete'
      OnClick = DeleteClipMenuItemClick
    end
    object ClipPopupRenameItem: TMenuItem
      Caption = '&Rename...'
      OnClick = RenameClipMenuItemClick
    end
    object ClipPopupEditCommentsItem: TMenuItem
      Caption = 'Edit &Comments...'
      OnClick = ClipPopupEditCommentsItemClick
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ClipPopupUpdateCutsItem: TMenuItem
      Caption = 'Run C&ut Detection...'
      OnClick = RunCutDetectionMenuItemClick
    end
    object ClipPopupChangeDpxHeaderFrameRateMenuItem: TMenuItem
      Caption = 'Change DPX Header &Frame Rate...'
      OnClick = ClipPopupChangeDpxHeaderFrameRateMenuItemClick
    end
    object ClipPopupStripAlphaChannelfromMediaFilesMenuItem: TMenuItem
      Caption = 'Remove &Alpha Channel from Media Files...'
      Hint = 'Compress 16-bit RGBA DPX files to RGB (25% size reduction)'
      OnClick = ClipPopupStripAlphaChannelFromMediaFilesMenuItemClick
    end
    object ClipPopupShowHideAlphaMatteItem: TMenuItem
      Caption = 'Show/Hide Alpha Matte...'
      Visible = False
      OnClick = ShowHideAlphaMatteMenuItemClick
    end
    object ClipPopupCountNumberOfFixesItem: TMenuItem
      Caption = 'Count &Number of Fixes...'
      OnClick = CountNumberOfFixesMenuItemClick
    end
    object ClipPopupSeparator4: TMenuItem
      Caption = '-'
      Visible = False
    end
    object ClipPopupImportImageFilesItem: TMenuItem
      Caption = '&Import Image Files...'
      Visible = False
      OnClick = ImportImageFilesMenuItemClick
    end
    object ClipPopupExportImageFilesItem: TMenuItem
      Caption = 'E&xport Image Files...'
      Visible = False
      OnClick = ExportImageFilesMenuItemClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ClipPopupBrowseToFolderItem: TMenuItem
      Caption = 'Browse to Media &Folder...'
      OnClick = BrowseToFolderMenuItemClick
    end
    object RelinkMediaFolderMenuItem: TMenuItem
      Caption = '&Relink Media Folder...'
      OnClick = RelinkMediaFolderMenuItemClick
    end
  end
  object CheckCopyFromVTRTimer: TTimer
    OnTimer = CheckCopyFromVTRTimerTimer
    Left = 644
    Top = 214
  end
  object ButtonImageList: TImageList
    Height = 26
    Masked = False
    Width = 72
    Left = 407
    Top = 46
    Bitmap = {
      494C010101000400040048001A00FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000200100001A00000001002000000000000075
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFC
      F900FBF9F2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBF9
      F200F7F1E1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F9F3
      E800F0E5C7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FAF6EE000000000000000000000000000000000000000000F7F1
      E200ECDDB7000000000000000000000000000000000000000000FDFDFA00FDFC
      FA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE00F0E3C70000000000000000000000000000000000F8F2
      E400E9DAB0000000000000000000000000000000000000000000F1E7D0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F4ECD500EDDFBC0000000000FAF7ED00E0C89000C89D
      2700C5981500CCA53D00EDDEC0000000000000000000E5D2A000FAF7EE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E1CB9000E6D4A400C6992800BA830000C699
      2600D4B45D00BC870000BB840000D4B45900E0C78700F2E8CE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFB00F4F4F40000000000000000000000000000000000E1E1E100C2C2
      C200E9E9E900000000000000000000000000FBFBFB00F1F1F100000000000000
      0000000000000000000000000000E9E9E800CDCCCC00F7F6F60000000000F9F9
      F900F0F0F00000000000F2F2F200FEFEFE00000000000000000000000000F7F7
      F700F5F6F40000000000000000000000000000000000CCCCCC00C7C7C700FBFB
      FB0000000000FEFEFE00EDEDED00FDFDFD000000000000000000000000000000
      0000EEEDEE00F7F7F70000000000FDFAF800D4B05200BE890000C18E0000C99F
      3300DFC47E00C08E0000BE8A0000CBA13200D2AE4C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009898980006040600F9F9F90000000000E2E2E2003C3B3C00000000002929
      2900000000004D4C4D00F1F2F000000000008E8E8E0000000000000000000000
      000000000000EEEEEE00474646000000000023222300000000004E4D4D001B1A
      1B007F7E7E00E2E1E10000000000D4D3D400000000000000000000000000504F
      4F00242324000000000000000000A8A8A800060404001C1A1B00242324000000
      0000908F8F00000000002C2B2C001F1D1F00F8F8F80000000000000000005857
      570000000000E1E1E10000000000D8BA6A00BC870000EDE1C100BF8B0000C99F
      3500EAD8AB00BC870000D0AC5100E1CA9400BB850000F2E7CA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000AAA9AA0033303200FEFEFE0000000000272627004F4E4F00CFCFCF00E6E6
      E600CCCCCC002C2B2C005152520000000000A3A2A30006040600000000000000
      0000000000008281820024232400CECECE00E1E0E100C5C5C500595859000706
      0700D4D4D300E5E5E500201E2000DCDCDC000000000000000000000000006D6C
      6E004A474A0000000000CFCFCF000B090B0089898900E1E1E000E1E1E100B5B5
      B50000000000AEAEAE00FAFAFA001F1E1F006968690000000000B4B4B4000403
      0300C9C9C90000000000FAF7EF00C99F3400BE880000D5B56400F2E9D200BE89
      1E00EEE0BA00C4951C00F2E9CF00C4951800BD870000DABE7A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A9A8A900312F300000000000B2B3B20012111200C7C6C700000000000000
      000000000000C8C8C80000000000E7E7E700A6A5A60003020200000000000000
      0000000000006565650059575900000000000000000000000000E7E7E7002320
      2300CECECE00E5E4E4001F1D1F00DBDBDB000000000000000000000000006C6B
      6C0046464600000000003D3C3D004A494A00EEEFEE0000000000000000000000
      000087868600120E120000000000B5B5B5000D0B0D00A7A7A7002B2B2C007A7B
      7D000000000000000000F3EAD100BB840000C08D0000B67D0000EADBBA00F5ED
      DF00F7F0E100FAF8EF00C89C2600BB850000BD870000CEA74600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A9A8A900302F3100000000006B6A6A00383738000F0E0F0035343500A8A8
      A800E4E4E400000000000000000000000000A1A1A10003020300000000000000
      000000000000C3C3C300030003005554540092919200AFAFAF00B7B7B7002F2E
      2F00CDCDCD00E5E4E5001E1C1F00DBDBDB000000000000000000000000006D6B
      6C0048484B0000000000302F300044424400000000006B696A00C4C4C400F9FA
      F900000000000000000000000000000000008C8B8C000D0C0E0051515000FFFF
      FB00F6EDD700EEE1BD00DBBE7C00C79D3400D6B87200D7B76D00DDC279000000
      000000000000FBF9F400D5B55C00D6B87300D4B36400CCA43E00E3CE9100F0E5
      C900F6EFDE00FBF9F200FEFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A9A8A900302F3000000000006867680061606000E2E2E200787777000000
      00002827280081818100E9EAE90000000000A1A1A10006030600000000000000
      00000000000000000000E0DFE0007373730046454600232222000B080B001818
      1800D0D0D000E4E4E4001E1D1E00E1E1E1000000000000000000000000006F6F
      6F004646470000000000242324009C9B9B00D2D2D30045454500000000004848
      4800A1A0A1000000000000000000000000009C9D9C000F0F110066666500FFFF
      FB00F5EDD600EEE1BD00DBBF7C00C79D3400D7B97200DCBF7300DFC37C000000
      000000000000FAF9F400D7B85D00DABE7400D4B26500CAA23800E2CC8F00F0E6
      C900F6EFDE00FBF9F200FEFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A9A8A9003130310000000000BBBBBB001A191A00E2E2E200000000000000
      0000ABA9A900202020000000000000000000A6A5A60000000000000000000000
      000000000000BFBFBF0047474700000000000000000000000000DEDEDE002220
      2200CBCBCB00E5E4E30026252700B6B6B6000000000000000000000000005655
      56004E4E4E000000000045434500606060000000000000000000ECEBEB007271
      72000F0D0F005959590000000000CACACA00191819007C7B7C001D1D1D009FA0
      A1000000000000000000F2E8D100B9820000BF8C0000B77D0000E4CFA100FBF9
      F600F9F6EE00FDFCF900C79A2000BC860000BE880000CCA33F00FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A9A8A900312F3000FEFEFE00000000003533350037363700C9C9C900E2E2
      E200C7C7C7001A191B008F8F8F0000000000A4A3A4001C1A1C0064646400D7D7
      D700DDDDDD00E1E1E1000F0E0F007E7E7E00D8D8D800E1E1E100979797000000
      0000EEEEEE00E1E1E1003B3A3A0035333600C1C0C000E1E0E100A3A3A3000604
      0600C3C3C30000000000DFDEDF000403040079797900DEDEDE00DCDCDC009898
      98000E0D0E00DBDBDC00000000003635360048474800000000009E9E9D000704
      0700DFDFDF0000000000F9F3E700C79A2600BE8A0000CEA64200F2EACC00C18F
      1D00EDDFB700CCA34500F5EEDB00C28F0E00BD880000D7B97000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000AAA9AA00312F3100FDFEFD0000000000F1F0F10055535400000000001D1D
      1D00000000008F8F8E00F9F9F900000000008D8D8D00000000003E3C3E000403
      04001211120000000000C9C9C800030003000C0B0D001B191B00000000009292
      910000000000D4D4D4000603060049484800030002001B1A1C00000000007574
      7500000000000000000000000000BAB8BA0016141500121112000B080B001211
      1200CDCDCD00000000004947490003020300E6E6E60000000000000000003736
      370015141400E8E8E80000000000D4B35B00BB840000EADAB100C5991A00C79C
      3400EBDBB000BA820000D8BD7E00DFC58700BB850000ECDDB600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00FFFFFE000000
      0000AEADAE003634360000000000000000000000000000000000F6F6F600D8D8
      D800FBFBFB00000000000000000000000000000000000000000000000000E7E8
      E700DEDEDE00000000000000000000000000E5E5E600DADADA00000000000000
      000000000000000000000000000000000000F3F3F300D8D7D700F9F9F9000000
      00000000000000000000000000000000000000000000DFDFDF00E6E6E6000000
      00000000000000000000F9F9F900000000000000000000000000000000000000
      0000FCFCFC000000000000000000FBF9F600CBA23100CBA23800BF8B0000C89F
      3200DFC58200C18D0000BD870000D4B56700CEA5360000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009696960059585A006A69
      6A004C4B4C002726270065646400646364005D5C5D00D6D6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EEE1C000D6B76700C08D0C00BC860000C79D
      2B00D6B66100BE8A0000BB850000CBA33800DEC47E00F5EDD900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A09F9F00676567006F6F
      7000767576007C7B7C00717071006F6D6F006A6A6B00D8D8D800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F8F2E400E3CD940000000000F5EEDE00D5B56500BC87
      0000C08C0000C08B0000E1CA9000FEFEFB0000000000E4D19F00FBF9F3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EEE1C20000000000000000000000000000000000F8F2
      E300E9D9AE0000000000000000000000000000000000FBF9F300F2EBD4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FBF9F200FDFCFA0000000000000000000000000000000000F7F0
      E100EBDCB5000000000000000000000000000000000000000000FBF9F200FDFC
      FB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFFFE000000000000000000000000000000000000000000F9F4
      E600EFE3C500000000000000000000000000000000000000000000000000FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBF8
      F100F6EFDF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FCFB
      F900FBF8EF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FE00FEFEFE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000200100001A0000000100010000000000A80300000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFF7FF00000000000000
      0000000000000000000000000000000000000000FFFFFFFFFFFFFFE7FF000000
      000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFE7
      FF000000000000000000000000000000000000000000000000000000FFFFFFFF
      FFFFFFE7FF000000000000000000000000000000000000000000000000000000
      FFFFFFFFFFFFFBE7CF0000000000000000000000000000000000000000000000
      00000000FFFFFFFFFFFFF9E7DF00000000000000000000000000000000000000
      0000000000000000FFFFFFFFFFFFFC819F000000000000000000000000000000
      000000000000000000000000FFFFFFFFFFFFFE003F0000000000000000000000
      00000000000000000000000000000000F3C73E24E788F2007F00000000000000
      0000000000000000000000000000000000000000F1013800E60462003F000000
      000000000000000000000000000000000000000000000000F1013800E4004400
      3F000000000000000000000000000000000000000000000000000000F23839C0
      E4720C003F000000000000000000000000000000000000000000000000000000
      F2073800E40F0018010000000000000000000000000000000000000000000000
      00000000F2013C00E40700180100000000000000000000000000000000000000
      0000000000000000F23139C0E4C20C001F000000000000000000000000000000
      000000000000000000000000F1010000040244003F0000000000000000000000
      00000000000000000000000000000000F10104080E0462003F00000000000000
      000000000000000000000000000000000000000093C7E73F1F9DF6007F000000
      000000000000000000000000000000000000000000000000803FFFFFFFFFFE00
      3F000000000000000000000000000000000000000000000000000000803FFFFF
      FFFFFC809F000000000000000000000000000000000000000000000000000000
      FFFFFFFFFFFFFDE79F0000000000000000000000000000000000000000000000
      00000000FFFFFFFFFFFFF9E7CF00000000000000000000000000000000000000
      0000000000000000FFFFFFFFFFFFFBE7EF000000000000000000000000000000
      000000000000000000000000FFFFFFFFFFFFFFE7FF0000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFE7FF00000000000000
      0000000000000000000000000000000000000000FFFFFFFFFFFFFFE7FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000}
  end
  object ProxyImagePopupMenu: TPopupMenu
    Left = 812
    Top = 132
    object ShowProxyTimecodeMenuItem: TMenuItem
      AutoCheck = True
      Caption = 'Show Timecode'
      Checked = True
      OnClick = ShowProxyTimecodeMenuItemClick
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'BMP|*.bmp|Cineon|*.cin|DPX|*.dpx|TGA|*.tga;*.tga2|TIFF|*.tif; *.' +
      'tiff|SGI|*.sgi;*.rgb|YUV|*.yuv; *.raw|All Image Files|*.bmp;*.ci' +
      'n;*.dpx; *.tga;*.tga2; *.tif; *.tiff;*.dpx; *.sgi;*.rgb;*.yuv; *' +
      '.raw|All Files|*.*'
    FilterIndex = 8
    Title = 'Browse For File Name'
    Left = 710
    Top = 82
  end
  object ClipRefreshListSweepTimer: TTimer
    Interval = 500
    OnTimer = ClipRefreshListSweepTimerTimer
    Left = 392
    Top = 128
  end
  object VersionListViewPopupMenu: TPopupMenu
    OnPopup = VersionListViewPopupMenuPopup
    Left = 271
    Top = 184
    object VersionPopupCommitVersionMenuItem: TMenuItem
      Caption = 'Commit &Version'
      OnClick = VersionPopupCommitVersionMenuItemClick
    end
    object VersionPopupExportVersionMenuItem: TMenuItem
      Caption = 'E&xport Version...'
      OnClick = VersionPopupExportVersionMenuItemClick
      object VersionPopupExportFullClipMenuItem: TMenuItem
        Caption = 'Export &Full Clip'
        OnClick = VersionPopupExportFullClipMenuItemClick
      end
      object VersionPopupExportMarkedRangeMenuItem: TMenuItem
        Caption = 'Export &Marked Range'
        OnClick = VersionPopupExportMarkedRangeMenuItemClick
      end
    end
    object VersionListViewPopupMenuSeparator1: TMenuItem
      Caption = '-'
    end
    object VersionPopupDiscardChangesMenuItem: TMenuItem
      Caption = 'Discard All Changes'
      OnClick = VersionPopupDiscardChangesMenuItemClick
    end
    object VersionPopupDeleteMenuItem: TMenuItem
      Caption = 'Delete Version'
      OnClick = VersionPopupDeleteMenuItemClick
    end
    object VersionListViewPopupMenuSeparator2: TMenuItem
      Caption = '-'
    end
    object CreateVersionOfVersionMenuItem: TMenuItem
      Caption = 'Create  Versio&n of Version'
      OnClick = CreateVersionOfVersionMenuItemClick
    end
    object VersionListViewPopupMenuSeparator3: TMenuItem
      Caption = '-'
    end
    object VersionPopupEditCommentMenuItem: TMenuItem
      Caption = 'Edit &Comment'
      OnClick = VersionPopupEditCommentMenuItemClick
    end
    object VersionPopupBrowseToFolderItem: TMenuItem
      Caption = 'Browse to &Folder'
      OnClick = BrowseToVersionFolderMenuItemClick
    end
    object VersionCountNumberofFixesMenuItem: TMenuItem
      Caption = 'Count &Number of Fixes...'
      OnClick = CountNumberOfFixesMenuItemClick
    end
  end
  object MainMenu: TMainMenu
    Left = 526
    Top = 162
    object TFileSubmenuItem: TMenuItem
      Caption = '&File'
      OnClick = FileMenuBarItemClick
      object FileMenuCreateJobItem: TMenuItem
        Caption = 'Create &Project'
        OnClick = FileMenuCreateJobItemClick
      end
      object FileMenuCreateBinItem: TMenuItem
        Caption = 'Create &Bin'
        OnClick = FileMenuCreateBinItemClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object FileMenuDeleteBinItem: TMenuItem
        Caption = '&Delete Bin'
        OnClick = FileMenuDeleteBinItemClick
      end
      object FileMenuRenameBinItem: TMenuItem
        Caption = '&Rename Bin'
        OnClick = FileMenuRenameBinItemClick
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object TCloseItem: TMenuItem
        Caption = '&Close'
        OnClick = TBCloseItemClick
      end
    end
    object TEditSubmenuItem: TMenuItem
      Caption = '&Edit'
      OnClick = EditMainBarItemClick
      object TSelectAllItem: TMenuItem
        Caption = 'Select &All'
        OnClick = TBSelectAllItemClick
      end
      object TInvertSelectItem: TMenuItem
        Caption = '&InvertSelection'
        OnClick = TBInvertSelectItemClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object TPreferenceItem: TMenuItem
        Caption = 'Preferences...'
        OnClick = TBPreferenceItemClick
      end
    end
    object TViewItem: TMenuItem
      Caption = '&View'
      OnClick = ViewMenuBarItemClick
      object TRefreshMenuItem: TMenuItem
        Caption = '&Refresh'
        OnClick = TBRefreshMenuItemClick
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object TViewCreateClipItem_REMOVED_: TMenuItem
        Caption = '&CreateClip'
        Checked = True
        Visible = False
        OnClick = TBViewCreateClipItem_REMOVED_Click
      end
      object TViewProxyDisplayItem: TMenuItem
        Caption = '&Proxy Display'
        OnClick = ProxyVisibleClick
      end
    end
    object TClipMenuItem: TMenuItem
      Caption = '&Clip'
      OnClick = ClipMenuBarItemClick
      object OpenClipMenuItemX: TMenuItem
        Caption = '&Open Clip'
        OnClick = OpenClipMenuItemOrDoubleClick
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object ImportTimecodesMenuItem: TMenuItem
        Caption = '&Import Timecodes...'
        Visible = False
        OnClick = ImportTimecodesItemClick
      end
      object ExportClipInfoSubMenu: TMenuItem
        Caption = '&Export Clip Info...'
        object ExportSelectedClipInfoMenuItem: TMenuItem
          Caption = 'Selected'
          OnClick = ExportSelectedClipInfoMenuItemClick
        end
        object ExportAllClipInfoMenuItem: TMenuItem
          Caption = 'All'
          OnClick = ExportSelectedClipInfoMenuItemClick
        end
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object UpdateCutsMenuItem: TMenuItem
        Caption = 'Run &Cut Detection...'
        OnClick = RunCutDetectionMenuItemClick
      end
      object N12: TMenuItem
        Caption = '-'
        Visible = False
      end
      object PropertiesItem: TMenuItem
        Caption = '&Properties...'
        Visible = False
      end
    end
  end
end
