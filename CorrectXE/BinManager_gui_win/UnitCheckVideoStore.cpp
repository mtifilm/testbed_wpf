//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitCheckVideoStore.h"
#include "CheckRFL.h"
#include "MTIDialogs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCheckVideoStore *FormCheckVideoStore;
//---------------------------------------------------------------------------
__fastcall TFormCheckVideoStore::TFormCheckVideoStore(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormCheckVideoStore::FormShow(TObject *Sender)
{
  CheckRFL = new CCheckRFL;

  Panel1->Visible = true;
  Panel2->Visible = false;
  Panel3->Visible = false;
  Panel4->Visible = false;
  Panel5->Visible = false;

  bbNext->Enabled = true;
  bbNext->SetFocus();

 InitVideoStore ();
}
//---------------------------------------------------------------------------
void __fastcall TFormCheckVideoStore::bbHaltClick(TObject *Sender)
{
  int iRet = CheckRFL->HaltProcessing ();
  if (iRet)
   {
    _MTIErrorDialog(Handle, "Unable to halt processing");
    return;
   }

  bbHalt->Enabled = false;
  bbClose->Enabled = true;

  
}
//---------------------------------------------------------------------------
void __fastcall TFormCheckVideoStore::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  delete CheckRFL;
}
//---------------------------------------------------------------------------
void __fastcall TFormCheckVideoStore::Timer1Timer(TObject *Sender)
{
  Timer1->Enabled = false;

  // check the progress values
  float fProgress0, fProgress1;
  int iErrorCondition;
  bool bFinishedFlag;

  fProgress0 = CheckRFL->ProgressMeter0 ();
  fProgress1 = CheckRFL->ProgressMeter1 ();

  ProgressBar0->Position = fProgress0 * 100.;
  ProgressBar1->Position = fProgress1 * 100.;

  LabelTrackName->Caption = CheckRFL->getTrackName().c_str();

  bFinishedFlag = CheckRFL->IsFinished (&iErrorCondition);

  if (iErrorCondition)
   {
    char caMessage[64];
    sprintf (caMessage,
        "An error condition has been encountered while scanning:  %d",
                iErrorCondition);
    _MTIErrorDialog(Handle, caMessage);
    bbClose->Enabled = true;
    bbHalt->Enabled = false;
    return;
   }

  if (bFinishedFlag)
   {
    bbHalt->Enabled = false;
    bbClose->Enabled = true;
    bbNext->Enabled = true;
    return;
   }



  Timer1->Enabled = true;
}
//---------------------------------------------------------------------------


void TFormCheckVideoStore::InitVideoStore ()
{
  int iRet;

  iRet = CheckRFL->LoadRawDiskCfg ();
  if (iRet)
   {
    _MTIErrorDialog(Handle, "Unable to load the video store config file");
    return;
   }

  LabelVideoStoreCfg->Caption = CheckRFL->getVideoStoreCfg().c_str();

  // load the list box with all video stores
  ListView1->Items->Clear();

  string strName;
  MTI_INT64 llLength, llActualLength;
  bool bConflict;
  bool bVideoStoreConflict = false;
  CheckRFL->findFirstVideoStore (strName, llLength, llActualLength, bConflict);
  while (strName != "")
   {
    TListItem *lip = ListView1->Items->Add ();
    lip->Caption = strName.c_str();
    char caMessage[32];
	sprintf (caMessage, "%lld", llLength);
    lip->SubItems->Add(caMessage);
    sprintf (caMessage, "%lld", llActualLength);
    lip->SubItems->Add(caMessage);

    if (bConflict)
     {
      lip->SubItems->Add("Yes");
      if (bVideoStoreConflict == false)
       {
        bVideoStoreConflict = true;
       }
     }
    else
     {
      lip->SubItems->Add("No");
     }

    CheckRFL->findNextVideoStore (strName, llLength, llActualLength, bConflict);
   }

  ListView1->Selected = 0;

  LabelConflictsExist->Visible = bVideoStoreConflict;

  bbRepair1->Enabled = false;
}


void TFormCheckVideoStore::InitRFL ()
{
  int iRet;

  // load the list box with all RFL files
  ListView5->Items->Clear();

  string strName;
  bool bConflict;
  bool bRFLConflict = false;
  CheckRFL->findFirstRFL (strName, bConflict);
  while (strName != "")
   {
    TListItem *lip = ListView5->Items->Add ();
    lip->Caption = strName.c_str();

    if (bConflict)
     {
      lip->SubItems->Add("Yes");
      if (bRFLConflict == false)
       {
        bRFLConflict = true;
       }
     }
    else
     {
      lip->SubItems->Add("No");
     }

    CheckRFL->findNextRFL (strName, bConflict);
   }

  ListView5->Selected = 0;
  LabelConflictsExist->Visible = bRFLConflict;

  bbRepair5->Enabled = false;
}

void TFormCheckVideoStore::InitCorrupted ()
{
  ListView3->Items->Clear();
  string strName;
  CheckRFL->findFirstBadClip (strName);
  while (strName != "")
   {
    TListItem *lip = ListView3->Items->Add ();
    lip->Caption = strName.c_str();

    CheckRFL->findNextBadClip (strName);
   }

  if (ListView3->Items->Count)
   {
    LabelMediaCorruption->Visible = true;
   }
  else
   {
    LabelMediaCorruption->Visible = false;
    TListItem *lip = ListView3->Items->Add ();
    lip->Caption = "<All Media Opened Successfully>";
   }

   ListView3->Selected = 0;
}

void TFormCheckVideoStore::InitOverlap ()
{
  int iRet;

  // load the list box with all RFL files
  ListView4->Items->Clear();

  string strNameA, strNameB;
  CheckRFL->findFirstMediaOverlap (strNameA, strNameB);
  while (strNameA != "")
   {
    TListItem *lip = ListView4->Items->Add ();
    lip->Caption = strNameA.c_str();
    lip->SubItems->Add(strNameB.c_str());

    CheckRFL->findNextMediaOverlap (strNameA, strNameB);
   }

  if (ListView4->Items->Count)
   {
    LabelSevereError->Visible = true;
   }
  else
   {
    LabelSevereError->Visible = false;
    TListItem *lip = ListView4->Items->Add ();
    lip->Caption = "<No Media Conflict Found>";
   }

  ListView4->Selected = 0;
}


void __fastcall TFormCheckVideoStore::bbNextClick(TObject *Sender)
{
  LabelConflictsExist->Visible = false;

  if (Panel1->Visible)
   {
    Panel1->Visible = false;
    Panel2->Visible = true;
    bbNext->SetFocus();



    // start the scanning process
    int iRet = CheckRFL->StartProcessing ();
    if (iRet)
     {
      _MTIErrorDialog(Handle, "Unable to start scan");
      return;
     }

    Timer1->Enabled = true;
    bbNext->Enabled = false;
    bbClose->Enabled = false;
   }
  else if (Panel2->Visible)
   {
    Panel2->Visible = false;
    Panel3->Visible = true;
    bbNext->SetFocus();

    InitCorrupted ();
   }
  else if (Panel3->Visible)
   {
    Panel3->Visible = false;
    Panel4->Visible = true;
    bbNext->SetFocus();

    InitOverlap ();
   }
  else if (Panel4->Visible)
   {
    Panel4->Visible = false;
    Panel5->Visible = true;
    bbNext->Enabled = false;
    bbClose->SetFocus();

    InitRFL ();
   }
}
//---------------------------------------------------------------------------


void __fastcall TFormCheckVideoStore::bbRepair1Click(TObject *Sender)
{
  if (ListView1->Selected == 0)
   {
    _MTIErrorDialog(Handle, "Please select a VideoStore for repairs");
   }

  TListItem *selectedItem = ListView1->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    if (CheckRFL->RepairRawDiskCfg (selectedItem->Index))
     {
      _MTIErrorDialog(Handle, "Unable to repair the selected VideoStore");
     }

    selectedItem = ListView1->GetNextItem (selectedItem, sdAll,
                selected);
   }

  InitVideoStore ();
}
//---------------------------------------------------------------------------




void __fastcall TFormCheckVideoStore::bbRepair5Click(TObject *Sender)
{
  if (ListView5->Selected == 0)
   {
    _MTIErrorDialog(Handle, "Please select a VideoStore File for repairs");
   }

  TListItem *selectedItem = ListView5->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    if (CheckRFL->RepairRFL (selectedItem->Index))
     {
      _MTIErrorDialog(Handle, "Unable to repair the selected VideoStore File");
     }

    selectedItem = ListView5->GetNextItem (selectedItem, sdAll,
                selected);
   }

  InitRFL ();
}
//---------------------------------------------------------------------------


void __fastcall TFormCheckVideoStore::ListView5Click(TObject *Sender)
{
  // does at least one of the selected items have a conflict?
  bool bConflict = false;

  TListItem *selectedItem = ListView5->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    if (selectedItem->SubItems->Strings[0] == "Yes")
     {
      bConflict = true;
     }

    selectedItem = ListView5->GetNextItem (selectedItem, sdAll,
                selected);
   }

  bbRepair5->Enabled = bConflict;
}
//---------------------------------------------------------------------------

void __fastcall TFormCheckVideoStore::ListView1Click(TObject *Sender)
{
  // does at least one of the selected items have a conflict?
  bool bConflict = false;

  TListItem *selectedItem = ListView1->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    if (selectedItem->SubItems->Strings[2] == "Yes")
     {
      bConflict = true;
     }

    selectedItem = ListView1->GetNextItem (selectedItem, sdAll,
                selected);
   }

  bbRepair1->Enabled = bConflict;
}
//---------------------------------------------------------------------------


void __fastcall TFormCheckVideoStore::ListView4Click(TObject *Sender)
{
  ListView4->Selected = 0;        
}
//---------------------------------------------------------------------------


void __fastcall TFormCheckVideoStore::ListView3Click(TObject *Sender)
{
  ListView3->Selected = 0;        
}
//---------------------------------------------------------------------------


