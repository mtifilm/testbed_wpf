object CommitVersionForm: TCommitVersionForm
  Left = 788
  Top = 65
  BorderStyle = bsToolWindow
  Caption = 'Commit Clip Version'
  ClientHeight = 200
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SetupPanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    DesignSize = (
      350
      200)
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 265
      Height = 32
      Caption = 
        'If you proceed at this point the following'#13#10'actions will be take' +
        'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 52
      Width = 279
      Height = 48
      Caption = 
        '1. The media from the clip version will be copied'#13#10'     back int' +
        'o the original clip. The original clip'#39's'#13#10'     frames will be pe' +
        'rmanently overwritten.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 108
      Width = 301
      Height = 32
      Caption = 
        '2. The clip containing the committed version will'#13#10'     be delet' +
        'ed if no other changes remain in the clip.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 168
      Width = 124
      Height = 16
      Caption = 'Click OK to proceed'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Separator1: TPanel
      Left = 0
      Top = 154
      Width = 349
      Height = 3
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 0
    end
    object SetupPanelOkButton: TButton
      Left = 168
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SetupPanelOkButtonClick
    end
    object SetupPanelCancelButton: TButton
      Left = 264
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
  object ProgressPanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      350
      200)
    object StatusLabel: TLabel
      Left = 16
      Top = 171
      Width = 81
      Height = 15
      Alignment = taCenter
      AutoSize = False
      Caption = 'Status'
      Color = clGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object ParentClip: TLabel
      Left = 16
      Top = 32
      Width = 58
      Height = 13
      Caption = 'Clip Name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object VersionClipValue: TLabel
      Left = 84
      Top = 48
      Width = 277
      Height = 13
      AutoSize = False
      Caption = 'Actual version name goes here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ProgressHeaderLabel: TLabel
      Left = 16
      Top = 8
      Width = 181
      Height = 13
      Caption = 'Copy version changes into master clip'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ParentClipValue: TLabel
      Left = 84
      Top = 32
      Width = 273
      Height = 13
      AutoSize = False
      Caption = 'Actual clip name goes here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object VersionClipLabel: TLabel
      Left = 16
      Top = 48
      Width = 45
      Height = 13
      Caption = 'Version:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ModifiedFrameCountValue: TLabel
      Left = 100
      Top = 131
      Width = 64
      Height = 13
      AutoSize = False
      Caption = '999999'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ModifiedFrameCountLabel: TLabel
      Left = 16
      Top = 131
      Width = 78
      Height = 13
      Caption = 'Frame Count: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameLabel: TLabel
      Left = 15
      Top = 112
      Width = 36
      Height = 13
      Caption = 'Frame'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameValue: TLabel
      Left = 53
      Top = 112
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object OfLabel: TLabel
      Left = 100
      Top = 112
      Width = 11
      Height = 13
      Caption = 'of'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object OfValue: TLabel
      Left = 114
      Top = 112
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object StatusMessageLabel: TLabel
      Left = 15
      Top = 93
      Width = 151
      Height = 13
      Caption = 'Status Message Goes Here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RangeLabel: TLabel
      Left = 16
      Top = 64
      Width = 39
      Height = 13
      Caption = 'Range:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RangeValue: TLabel
      Left = 84
      Top = 64
      Width = 277
      Height = 13
      AutoSize = False
      Caption = 'Actual range goes here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object FrameIndexProgressBar: TProgressBar
      Left = 15
      Top = 110
      Width = 317
      Height = 17
      TabOrder = 0
    end
    object Separator2: TPanel
      Left = 0
      Top = 154
      Width = 349
      Height = 3
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 1
    end
    object ProgressPanelCloseButton: TButton
      Left = 264
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      ModalResult = 2
      TabOrder = 2
      OnClick = ProgressPanelCloseButtonClick
    end
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 320
    Top = 4
  end
  object AutoCloseTimer: TTimer
    Enabled = False
    OnTimer = AutoCloseTimerTimer
    Left = 320
    Top = 40
  end
end
