// BinMgrGUIInterface.cpp: implementation of the interface between
//                         an application program and the Bin Manager
//                         GUI DLL
//
/*
$Header: /usr/local/filmroot/BinManager_gui_win/BinMgrGUIInterface.cpp,v 1.10.4.2 2008/03/19 01:59:20 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "BinMgrGUIInterface.h"
#include "BinManagerGUIUnit.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBinMgrGUIInterface::CBinMgrGUIInterface()
 : bVSUsagePanel(false), bInOutDurCols(true), bMediaDurCols(false),
   bMediaLocCol(true), bArchivedCol(false)
{
   // Tell the Bin Manager GUI about this interface instance
   TBinManagerForm::RegisterBinMgrGUIInterface(this);
}

CBinMgrGUIInterface::~CBinMgrGUIInterface()
{
   // Destroy the BinManagerGUI if it exists
   DeleteBinManagerGUI();
}

//////////////////////////////////////////////////////////////////////
// Utility Member Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    ShowBinManagerGUI
//
// Description: Shows or hides the Bin Manager GUI.  Constructs
//              a TBinManagerForm and other classes if they do not
//              already exist.  Hiding the Bin Manager GUI makes
//              it invisible but does not destroy it.
//
// Arguments:   bool show    true - make Bin Manager GUI visible
//                           false - hide Bin Manager GUI
//
//              bool dummy   used to be "ontop" argument, no longer used
//
// Returns:     none
//
//------------------------------------------------------------------------
void CBinMgrGUIInterface::ShowBinManagerGUI(bool show, bool dummy)
{
   ::ShowBinManagerGUI(show, BM_MODE_NORMAL);
}

bool CBinMgrGUIInterface::IsLockValid(lockID_t lockID)
{
   return (lockID >= 0);
}

void CBinMgrGUIInterface::SetVideoStoreUsagePanelFlag(bool newFlag)
{
   bVSUsagePanel = newFlag;
}

bool CBinMgrGUIInterface::GetVideoStoreUsagePanelFlag()
{
   return bVSUsagePanel;
}

void CBinMgrGUIInterface::SetInOutDurColsFlag(bool newFlag)
{
   bInOutDurCols = newFlag;
}

bool CBinMgrGUIInterface::GetInOutDurColsFlag()
{
   return bInOutDurCols;
}

void CBinMgrGUIInterface::SetMediaDurColsFlag(bool newFlag)
{
   bMediaDurCols = newFlag;
}

bool CBinMgrGUIInterface::GetMediaDurColsFlag()
{
   return bMediaDurCols;
}

void CBinMgrGUIInterface::SetMediaLocColFlag(bool newFlag)
{
   bMediaLocCol = newFlag;
}

bool CBinMgrGUIInterface::GetMediaLocColFlag()
{
   return bMediaLocCol;
}

void CBinMgrGUIInterface::SetArchivedColFlag(bool newFlag)
{
   bArchivedCol = newFlag;
}

bool CBinMgrGUIInterface::GetArchivedColFlag()
{
   return bArchivedCol;
}

string CBinMgrGUIInterface::GetRedClipName()
{
   return "";
}

void CBinMgrGUIInterface::GetCurrentMarks(int &markInIndex, int &markOutIndex)
{
	markInIndex = -1;
   markOutIndex = -1;
}


