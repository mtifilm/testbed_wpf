//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipNameTemplateUnit.h"
#include <assert.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClipNameTemplateForm *ClipNameTemplateForm;
//---------------------------------------------------------------------------

string TClipNameTemplateForm::FolderNameTemplateString
                              = "<Folder Name>";
string TClipNameTemplateForm::FileNameTemplateString
                              = "<File Name>";
string TClipNameTemplateForm::SequenceNumberTemplateString
                              = "<Sequence Number>";

// These must match the actual GUI dialog values
#define NUMBER_OF_TEMPLATE_CHOICES 4
#define NUMBER_OF_DIGIT_CHOICES 8

namespace
{
    string gTemplateString[NUMBER_OF_TEMPLATE_CHOICES] =
    {
        "Clip" + TClipNameTemplateForm::SequenceNumberTemplateString,
        TClipNameTemplateForm::FileNameTemplateString + "_"
               + TClipNameTemplateForm::SequenceNumberTemplateString,
        TClipNameTemplateForm::FolderNameTemplateString + "_"
               + TClipNameTemplateForm::SequenceNumberTemplateString,
        TClipNameTemplateForm::FolderNameTemplateString + "_"
               + TClipNameTemplateForm::FileNameTemplateString + "_"
               + TClipNameTemplateForm::SequenceNumberTemplateString
    };
}

//---------------------------------------------------------------------------

__fastcall TClipNameTemplateForm::TClipNameTemplateForm(TComponent* Owner)
        : TForm(Owner)
{
    assert(NUMBER_OF_TEMPLATE_CHOICES == 4);
    TemplateOption0RadioButton->Caption = gTemplateString[0].c_str();
    TemplateOption1RadioButton->Caption = gTemplateString[1].c_str();
    TemplateOption2RadioButton->Caption = gTemplateString[2].c_str();
    TemplateOption3RadioButton->Caption = gTemplateString[3].c_str();

    TemplateOption0RadioButton->Checked = true;
}
//---------------------------------------------------------------------------

void TClipNameTemplateForm::SetSelectedTemplate(const string &newTemplate)
{
    int i;

    for (i = 0; i < NUMBER_OF_TEMPLATE_CHOICES; ++i)
    {
        if (newTemplate == gTemplateString[i])
        {
            break;
        }
    }
    if (i == NUMBER_OF_TEMPLATE_CHOICES)
    {
        i = 0;
    }
}
//---------------------------------------------------------------------------

string TClipNameTemplateForm::GetSelectedTemplate()
{
    string retVal = gTemplateString[0];

    if (TemplateOption1RadioButton->Checked)
    {
         retVal = gTemplateString[1];
    }
    else if (TemplateOption2RadioButton->Checked)
    {
         retVal = gTemplateString[2];
    }
    else if (TemplateOption3RadioButton->Checked)
    {
         retVal = gTemplateString[3];
    }

    return retVal;
}
//---------------------------------------------------------------------------

void TClipNameTemplateForm::SetNumberOfDigits(int newNumberOfDigits)
{
    if (newNumberOfDigits < 1 || newNumberOfDigits > NUMBER_OF_DIGIT_CHOICES)
    {
        newNumberOfDigits = 3;   // arbitrary default
    }
    NumberOfDigitsComboiBox->ItemIndex = newNumberOfDigits - 1; // 0-based
}
//---------------------------------------------------------------------------

int TClipNameTemplateForm::GetNumberOfDigits()
{
    return NumberOfDigitsComboiBox->ItemIndex + 1;    // ItemIndex is 0-based
}
//---------------------------------------------------------------------------




