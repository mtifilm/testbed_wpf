//---------------------------------------------------------------------------

#ifndef ClipPropertiesUnitH
#define ClipPropertiesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TClipPropertyWin : public TForm
{
__published:	// IDE-managed Components
    TButton *ClipPropertyOKButton;
    TLabel *Label1;
    TLabel *ClipPropertyNameValLabel;
    TLabel *Label3;
    TLabel *ClipPropertyStatusValLabel;
    TLabel *ClipPropertyNumFixesLabel;
    TLabel *ClipPropertyNumFixesValLabel;
    TLabel *Label7;
    TLabel *ClipPropertyFormatValLabel;
    TLabel *Label8;
    TLabel *ClipPropertyMediaIdentifierValLabel;
    void __fastcall ClipPropertyOKButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TClipPropertyWin(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipPropertyWin *ClipPropertyWin;
//---------------------------------------------------------------------------
#endif
