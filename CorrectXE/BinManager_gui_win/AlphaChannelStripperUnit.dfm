object AlphaChannelStripperForm: TAlphaChannelStripperForm
  Left = 494
  Top = 228
  Anchors = []
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Alpha Channel Remover'
  ClientHeight = 200
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AreYouSurePanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    DesignSize = (
      350
      200)
    object Label1: TLabel
      Left = 24
      Top = 48
      Width = 309
      Height = 19
      Caption = 'Click OK to remove the alpha channel from '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 55
      Top = 71
      Width = 245
      Height = 19
      Caption = 'the media files of all selected clips.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Separator1: TPanel
      Left = 0
      Top = 154
      Width = 349
      Height = 3
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 0
    end
    object SetupPanelOkButton: TButton
      Left = 168
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      TabOrder = 1
      OnClick = SetupPanelOkButtonClick
    end
    object SetupPanelCancelButton: TButton
      Left = 264
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
  object ProgressPanel: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      350
      200)
    object StatusLabel: TLabel
      Left = 16
      Top = 171
      Width = 81
      Height = 15
      Alignment = taCenter
      AutoSize = False
      Caption = 'Status'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object ClipNameLabel: TLabel
      Left = 16
      Top = 36
      Width = 23
      Height = 13
      Caption = 'Clip:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ClipNameValue: TLabel
      Left = 60
      Top = 36
      Width = 293
      Height = 13
      AutoSize = False
      Caption = 'Actual clip name goes here'
    end
    object ModifyingModeLabel: TLabel
      Left = 16
      Top = 8
      Width = 245
      Height = 13
      Caption = 'Modifying image files to hack Alpha Matte...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameLabel: TLabel
      Left = 16
      Top = 92
      Width = 36
      Height = 13
      Caption = 'Frame'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FrameValue: TLabel
      Left = 57
      Top = 92
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
    end
    object OfLabel: TLabel
      Left = 112
      Top = 92
      Width = 12
      Height = 13
      Caption = 'of'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object OfValue: TLabel
      Left = 128
      Top = 92
      Width = 50
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '999999'
    end
    object ClipListProgressBar: TProgressBar
      Left = 16
      Top = 56
      Width = 317
      Height = 17
      TabOrder = 0
    end
    object FrameIndexProgressBar: TProgressBar
      Left = 16
      Top = 112
      Width = 317
      Height = 17
      TabOrder = 1
    end
    object Separator2: TPanel
      Left = 0
      Top = 154
      Width = 349
      Height = 3
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 2
    end
    object ProgressPanelCloseButton: TButton
      Left = 264
      Top = 168
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      ModalResult = 2
      TabOrder = 3
      OnClick = ProgressPanelCloseButtonClick
    end
  end
  object ProgressTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ProgressTimerTimer
    Left = 312
    Top = 68
  end
end
