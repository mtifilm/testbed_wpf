// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DpxHeaderEditorUnit.h"
#include "ClipAPI.h"
#include "DpxFrameHacker.h"
#include "bthread.h"
#include "ImageFileMediaAccess.h"
#include "Ippheaders.h"           // ONLY FOR IpaStripeStream.h!!!
#include "IpaStripeStream.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
// ---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ColorLabel"
#pragma resource "*.dfm"
TDpxHeaderEditorForm *DpxHeaderEditorForm;

// I think there is no point to multi-threading this
#define NO_MULTI

enum ComboBoxIndices
{
   InvalidIndex = -1,
   IndexOf23976,
   IndexOf24,
   IndexOf25,
   IndexOf2997,
   IndexOf30
};
// ---------------------------------------------------------------------------

__fastcall TDpxHeaderEditorForm::TDpxHeaderEditorForm(TComponent* Owner) : TForm(Owner)
{
}

// ---------------------------------------------------------------------------
void __fastcall TDpxHeaderEditorForm::FormShow(TObject *Sender)
{
   string first;
   float second;
   int third;
   std::tie(first, second, third) = ScanAllClipsForFrameRate();

   FromFrameRatesLabel->Caption = first.c_str();

   if (mTotalClipCount == 0)
   {
      SourceClipCountLabel->Caption = "NO CLIPS SELECTED!";
   }
   else if (mTotalClipCount == 1)
   {
      SourceClipCountLabel->Caption = "for the selected clip.";
   }
   else
   {
      MTIostringstream os;
      os << "for the " << mTotalClipCount << " selected clips.";
      SourceClipCountLabel->Caption = os.str().c_str();
   }

   DifferentFrameRatesWarningLabel->Visible = third > 1;

   if (NewFpsComboBox->ItemIndex == ComboBoxIndices::InvalidIndex)
   {
      NewFpsComboBox->ItemIndex = (second > 29.F && second < 30.F)
                                    ? ComboBoxIndices::IndexOf30
                                    : ComboBoxIndices::IndexOf24;
   }

   mCancelledFlag = false;
   mHackCommand = MEDIA_HACK_INVALID;
   ProgressPanel->Visible = false;
   AreYouSurePanel->Visible = false;
   OptionsPanel->Visible = true;
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::Reset()
{
   mClipList.clear();
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::SetBinPath(string aBinPath)
{
   mBinPath = aBinPath;
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::AddClip(string aClipName)
{
   mClipList.push_back(aClipName);
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::ProcessingThread(void *aThreadArg, void *aThreadHandle)
{
   TDpxHeaderEditorForm *this_ = reinterpret_cast<TDpxHeaderEditorForm*>(aThreadArg);

   BThreadBegin(aThreadHandle);

   if (this_ != NULL)
   {
      this_->HackAllClips();
   }
}

// ---------------------------------------------------------------------------
std::tuple<string, float, int> TDpxHeaderEditorForm::ScanAllClipsForFrameRate()
{
   std::set<float> frameRatesFound;
   mTotalClipCount = mClipList.size();

   for (mClipIndex = 0; mClipIndex < mTotalClipCount; ++mClipIndex)
   {
      string &clipName = mClipList[mClipIndex];

      mClipName = clipName.c_str(); // mClipName is an AnsiString
      ScanOneClipForFrameRate(clipName, frameRatesFound);
   }

   MTIostringstream(os);
   os << std::setprecision(5);
   float firstNonZeroRate = 0.F;
   int nonZeroCount = 0;
   if (frameRatesFound.size() == 0)
   {
      return std::make_tuple(string("[Not set]"), 0.F, 0);
   }

   for (auto frameRate : frameRatesFound)
   {
      // Set values are non-negative, unique ,and ordered, so 0.F can only be first.
      // Only add [Not set] if there are no valid frame rates.
      if (frameRate == 0.F)
      {
         if (frameRatesFound.size() == 1)
         {
            os << "[Not set]";
         }

         continue;
      }

      // Make a comma-separated list of the frame rates found.
      if (os.str().empty() == false)
      {
         os << ", ";
      }

      os << frameRate;
      ++nonZeroCount;
      if (firstNonZeroRate == 0.F)
      {
         firstNonZeroRate = frameRate;
      }
   }

   return std::make_tuple(os.str(), firstNonZeroRate, nonZeroCount);
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::ScanOneClipForFrameRate(string aClipName, std::set<float> &frameRatesFound)
{
   int errorCode = 0;
   CBinManager binManager;

   auto clip = binManager.openClip(mBinPath, aClipName, &errorCode);
   if (errorCode != 0)
   {
      TRACE_0(errout << "ERROR: Could not open Clip " << aClipName << endl << "       in Bin " << mBinPath);
      return;
   }

   CVideoProxy *mainVideo = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (mainVideo == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video proxy");
      return;
   }

   CVideoFrameList *videoFrameList = mainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video frame list");
      return;
   }

   mTotalFrameCount = videoFrameList->getTotalFrameCount();
   if (mTotalFrameCount < 1)
   {
      return;
   }

   // Find the frame 0's media file path.
   CVideoFrame *frame = videoFrameList->getFrame(0);
   CVideoField *field = frame ? frame->getField(0) : nullptr;
   if (frame == nullptr || field == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      mAborted = true;
      TRACE_0(errout << "ERROR: DPX Header Editor cant read frame 0 in clip " << aClipName);
      return;
   }

   CMediaLocation mediaLocation = field->getMediaLocation();
   MTIassert(mediaLocation.isMediaTypeImageFile());
   if (!mediaLocation.isMediaTypeImageFile())
   {
      binManager.closeClip(clip);
      clip = nullptr;
      mAborted = true;
      TRACE_0(errout << "ERROR: DPX Header Editor cant read frame 0 in clip " << aClipName);
      return;
   }

   CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
   auto filename = mediaAccess->getImageFileName(mediaLocation);

   binManager.closeClip(clip);
   clip = nullptr;

   // There are two frame rates in the DPX header.
   float tvFrameRate = 0.F;
   float filmFrameRate = 0.F;
   DpxFrameHacker frameHacker;
   frameHacker.getFrameRate(filename, tvFrameRate, filmFrameRate);
   frameRatesFound.insert(tvFrameRate);
   frameRatesFound.insert(filmFrameRate);

//   DBTRACE(aClipName);
//   DBTRACE(tvFrameRate);
//   DBTRACE(filmFrameRate);

   return;
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::HackAllClips()
{
   mTotalClipCount = mClipList.size();

   float newFrameRate;
   switch (NewFpsComboBox->ItemIndex)
   {
      case IndexOf23976:
         newFrameRate = 24000.F/1001.F;
         break;

      default:
      case IndexOf24:
         newFrameRate = 24.F;
         break;

      case IndexOf25:
         newFrameRate = 25.F;
         break;

      case IndexOf2997:
         newFrameRate = 30000.F/1001.F;
         break;

      case IndexOf30:
         newFrameRate = 30.F;
         break;
   }

   for (mClipIndex = 0; mClipIndex < mTotalClipCount; ++mClipIndex)
   {
      string &clipName = mClipList[mClipIndex];

      mClipName = clipName.c_str(); // mClipName is an AnsiString
      HackOneClip(clipName, newFrameRate);
   }

   mProcessingIsCompleteFlag = true;
}

// ---------------------------------------------------------------------------
void TDpxHeaderEditorForm::HackOneClip(string aClipName, float newFrameRate)
{
   int errorCode = 0;
   CBinManager binManager;

   auto clip = binManager.openClip(mBinPath, aClipName, &errorCode);
   if (errorCode != 0)
   {
      TRACE_0(errout << "ERROR: Could not open Clip " << aClipName << endl << "       in Bin " << mBinPath);
      return;
   }

   CVideoProxy *mainVideo = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (mainVideo == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video proxy");
      return;
   }

   CVideoFrameList *videoFrameList = mainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == nullptr)
   {
      binManager.closeClip(clip);
      clip = nullptr;
      TRACE_0(errout << "ERROR: Clip " << aClipName << " does not have a main video frame list");
      return;
   }

   mTotalFrameCount = videoFrameList->getTotalFrameCount();

   TRACE_2(errout << "INFO: Removing alpha channel from media for clip " << aClipName << endl << "      Total " << mTotalFrameCount <<
         " frames");

   int slowStartDelayInMsecs = 100;
   const int delayReductionInMsecs = 20;
   const int interClipDelayInMsecs = 250;

   mFrameIndex = 0;
   mAborted = false;
   auto f = [&](int frameIndex)
   {
      if (mCancelledFlag || mAborted)
      {
         return;
      }

      // Careful, the hacker instance is NOT THREADSAFE, so we need to create
      // a new one each time.
      DpxFrameHacker frameHacker;

      // Find the frame file media file path and hack the file's header.
      CVideoFrame *frame = videoFrameList->getFrame(frameIndex);
      CVideoField *field = frame ? frame->getField(0) : nullptr;
      if (frame == nullptr || field == nullptr)
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: DPX Header Editor cant get frame " << frameIndex << " in clip " << aClipName);
         return;
      }

      /////   // Should be done something like this, but DPX Header Editor needs new frame rate.
      /////   errorCode = field->hackMedia(mHackCommand);

      CMediaLocation mediaLocation = field->getMediaLocation();
      MTIassert(mediaLocation.isMediaTypeImageFile());
      if (!mediaLocation.isMediaTypeImageFile())
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: DPX Header Editor cant get frame " << frameIndex << " in clip " << aClipName);
         return;
      }

      CImageFileMediaAccess *mediaAccess = static_cast<CImageFileMediaAccess*>(mediaLocation.getMediaAccess());
      auto filename = mediaAccess->getImageFileName(mediaLocation);

      errorCode = frameHacker.changeFrameRate(filename, newFrameRate);
      if (errorCode != 0)
      {
         mAborted = true;
         TRACE_0(errout << "ERROR: DPX Header Editor could not hack media of frame " << frameIndex << endl <<
               "       Return code " << errorCode);
         return;
      }

      ++mFrameIndex;
   };

   IpaStripeStream iss;
   iss << mTotalFrameCount;
   iss << efu_job(f);
#ifdef NO_MULTI
   iss.run();
#else
   iss.stripe();
#endif

   if (mCancelledFlag || mAborted)
   {
      mAborted = true;
      MTIostringstream os;
      os << "WARNING: DPX Header Editor did not complete processing! " << endl
         << "         Processed " << mFrameIndex << " of " << mTotalFrameCount << " frames " << endl
         << "         in clip " << aClipName << endl
         << "         You may try running the DPX Header Editor again to fix it.";
      TRACE_0(errout << os.str());
      _MTIErrorDialog(Handle, os.str());
		binManager.closeClip(clip);
      clip = nullptr;
      return;
   }

   mFrameIndex = mTotalFrameCount;

   // Update the "last media action" (shows up in the clip "status"
	// column in the bin manager clip list view)
	EMediaStatus mediaStatus = (newFrameRate > 23.F && newFrameRate < 24.F)
										? MEDIA_STATUS_FPS_CHANGED_TO_2398
										: ((newFrameRate == 24.F)
											? MEDIA_STATUS_FPS_CHANGED_TO_24
											: ((newFrameRate == 25.F)
												? MEDIA_STATUS_FPS_CHANGED_TO_25
												: ((newFrameRate > 29.F && newFrameRate < 30.F)
													? MEDIA_STATUS_FPS_CHANGED_TO_2997
													: ((newFrameRate == 30.F)
														? MEDIA_STATUS_FPS_CHANGED_TO_30
														: MEDIA_STATUS_FPS_CHANGED))));
	mainVideo->setLastMediaAction(mediaStatus);

	// Done with the clip
	binManager.closeClip(clip);
	clip = nullptr;

   // Pause to show completed progress bar in UI before continuing on to
   // the next clip in the list
   MTImillisleep(interClipDelayInMsecs);
}
// ---------------------------------------------------------------------------

void __fastcall TDpxHeaderEditorForm::OkButtonClick(TObject *Sender)
{
   // These values are all set in the processing thread and read in
   // ProgressTimer(); we initialize them to dummy values here
   mClipName = "";
   mClipIndex = 0;
   mTotalClipCount = 1; // fixed in thread
   mFrameIndex = 0;
   mTotalFrameCount = 1; // fixed in thread
   mProcessingIsCompleteFlag = false;
   mCancelledFlag = false;

   // These GUI elements are all maintained by ProgressTimer()
   ClipNameValue->Caption = ""; // gets set in timer
   ClipListProgressBar->Max = static_cast<int>(mTotalClipCount);
   // Min is set to 0 at design time and never changed
   ClipListProgressBar->Position = static_cast<int>(mClipIndex);
   FrameValue->Caption = "";
   OfValue->Caption = "";
   FrameIndexProgressBar->Max = static_cast<int>(mTotalFrameCount);
   // Min is set to 0 at design time and never changed
   FrameIndexProgressBar->Position = static_cast<int>(mFrameIndex);
   ProgressPanelCloseButton->Caption = "Cancel";
   StatusLabel->Visible = false;
   //

   // OK to swap the panels now
   ProgressPanel->Visible = true;
   OptionsPanel->Visible = false;
   AreYouSurePanel->Visible = false;

   // Legacy from when this was done in the clip.
   ModifyingModeLabel->Caption = "Modifying image files to change frame rate...";
   mHackCommand = MEDIA_HACK_DESTROY_ALPHA_MATTE;

   mProcessingThreadId = BThreadSpawn(ProcessingThread, (void *)this);
   if (mProcessingThreadId == NULL)
   {
      _MTIErrorDialog(Handle, "Failed to launch processing thread   ");
      ModalResult = mrCancel;
   }
   else
   {
      // fire up the timer, which will track the progress and display it
      ProgressTimer->Enabled = true;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TDpxHeaderEditorForm::ProgressPanelCloseButtonClick(TObject *Sender)
{
   if (ProgressPanelCloseButton->Caption == "Close")
   {
      ModalResult = mrOk; // Setting ModalResult will hide the dialog
   }
   else // Hacking is in progress
   {

      ModalResult = mrNone; // Don't hide the dialog
      mCancelledFlag = true;
   }
}
// ---------------------------------------------------------------------------

void __fastcall TDpxHeaderEditorForm::ProgressTimerTimer(TObject *Sender)
{
   // Fun with signs
   int clipIndex = static_cast<int>(mClipIndex);
   int totalClipCount = static_cast<int>(mTotalClipCount);
   int frameIndex = static_cast<int>(mFrameIndex);
   int totalFrameCount = static_cast<int>(mTotalFrameCount);

   // Update clip progress panel GUI elements
   if (ClipNameValue->Caption != mClipName)
   {
      ClipNameValue->Caption = mClipName;
   }
   if (ClipListProgressBar->Max != totalClipCount)
   {
      ClipListProgressBar->Max = totalClipCount;
      ClipListProgressBar->Position = clipIndex;
   }
   else if (ClipListProgressBar->Position != clipIndex)
   {
      ClipListProgressBar->Position = clipIndex;
   }

   // Update frame-by-frame progress
   if (FrameIndexProgressBar->Max != totalFrameCount)
   {
      OfValue->Caption = AnsiString(totalFrameCount);
      OfValue->Refresh();
      FrameIndexProgressBar->Max = totalFrameCount;
      FrameValue->Caption = AnsiString(frameIndex);
      FrameValue->Refresh();
      FrameIndexProgressBar->Position = frameIndex;
   }
   else if (FrameIndexProgressBar->Position != frameIndex)
   {
      FrameValue->Caption = AnsiString(frameIndex);
      FrameValue->Refresh();
      FrameIndexProgressBar->Position = frameIndex;
   }

   // Check if we're all done
   if (mProcessingIsCompleteFlag || mCancelledFlag)
   {
      ProgressTimer->Enabled = false;
      ProgressPanelCloseButton->Caption = "Close";
      StatusLabel->Caption = mCancelledFlag ? " Cancelled " : " Finished ";
      StatusLabel->Visible = true;
   }

   ProgressPanel->Refresh();

}
// ---------------------------------------------------------------------------
