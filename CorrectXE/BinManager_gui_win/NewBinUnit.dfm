object NewBinDialog: TNewBinDialog
  Left = 248
  Top = 234
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'New Bin'
  ClientHeight = 78
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 10
    Width = 28
    Height = 13
    Caption = 'Name'
  end
  object BinNameEdit: TEdit
    Left = 64
    Top = 8
    Width = 217
    Height = 21
    TabOrder = 0
  end
  object NewBinOkButton: TButton
    Left = 48
    Top = 48
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object NewBinCancelButton: TButton
    Left = 168
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
