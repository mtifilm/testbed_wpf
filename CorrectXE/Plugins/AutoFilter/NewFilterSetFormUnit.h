//---------------------------------------------------------------------------

#ifndef NewFilterSetFormUnitH
#define NewFilterSetFormUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;
#include <vector>
using std::vector;
//---------------------------------------------------------------------------

class TNewFilterSetForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *EditLabel;
    TEdit *Edit;
    TButton *OkButton;
    TButton *CancelButton;
        TLabel *ComboBoxLabel;
        TComboBox *ComboBox;
    void __fastcall EditEnter(TObject *Sender);
        void __fastcall ComboBoxChange(TObject *Sender);
private:	// User declarations

    string nameShadow;

public:		// User declarations
    __fastcall TNewFilterSetForm(TComponent* Owner);

    void SetListOfFilterSetNames(const vector<string> &newListOfNames);
    void SetDefaultName(const string &newName);
    int GetSelectedIndex();
    string GetNewSetName();
};
//---------------------------------------------------------------------------
extern PACKAGE TNewFilterSetForm *NewFilterSetForm;
//---------------------------------------------------------------------------
#endif
