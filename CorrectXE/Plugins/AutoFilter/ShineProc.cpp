//---------------------------------------------------------------------------

#pragma hdrstop

#include "ShineProc.h"
#include "err_auto_filter.h"

#include "AlgorithmAF_MT.h"
#include "AutoTrackingPreprocessor.h"
#include "Clip3.h"
#include "ImageFormat3.h"
#include "PixelRegions.h"
#include "ShineRepairMask.h"
#include "ToolProgressMonitor.h"

#define INVALID_FRAME_INDEX (-1)
//---------------------------------------------------------------------------
#pragma package(smart_init)
//------------------------------------------------------------------------------

ShineProc::ShineProc(
   int newToolNumber,
   const string &newToolName,
   CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr,
   IToolProgressMonitor *newToolProgressMonitor,
   Ipp8uArray &modifiedPixelMap)
: CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
, _modifiedPixelMask(modifiedPixelMap)
{
   _cheshireShineRepairMask = new ShineRepairMask;
	StartProcessThread(); // Required by Tool Infrastructure
}
//------------------------------------------------------------------------------

ShineProc::~ShineProc()
{
   auto srm = reinterpret_cast<ShineRepairMask *>(_cheshireShineRepairMask);
   delete srm;
}
//------------------------------------------------------------------------------

int ShineProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // maximum number of input and output frames
   int maxInputFrames = 9; // Processed frame + 4 on either side
   int maxOutputFrames = 1;
   SetInputMaxFrames(0, maxInputFrames, maxOutputFrames); // Input Port 0
   SetOutputNewFrame(0, 0); // Output to new frame with same image format as the input
   SetOutputMaxFrames(0, maxOutputFrames); // Output Port 0

	return 0;
}
//------------------------------------------------------------------------------

int ShineProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];

   _firstInputFrameIndex = frameRange.inFrameIndex;
	_lastInputFrameIndex = frameRange.outFrameIndex - 1;
   DBTRACE(_firstInputFrameIndex);
   DBTRACE(_lastInputFrameIndex);

	return 0;
}
//------------------------------------------------------------------------------

int ShineProc::SetParameters(CToolParameters *toolParams)
{
	ShineToolParameters *shineToolParams = static_cast<ShineToolParameters*>(toolParams);

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_shineSettings = shineToolParams->getShineToolSettingsRef();

	// Set the image size
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
	_imageSize.height = (int) imageFormat->getLinesPerFrame();
	_imageSize.width = (int) imageFormat->getPixelsPerLine();
	_imageSize.depth = 3;
   DBTRACE(_imageSize);

   // Compute "Original Bits" for IppArray normalization.
	MTI_UINT16 componentValues[3];
	imageFormat->getComponentValueMax(componentValues);
	MTI_UINT16 maxPixel = std::max<MTI_UINT16>(componentValues[0], std::max<MTI_UINT16>(componentValues[1], componentValues[2]));
   _bitsPerPixelComponent = 0;
   for (auto mask = 1; mask < maxPixel; mask <<= 1)
   {
      ++_bitsPerPixelComponent;
   }

   DBTRACE(_bitsPerPixelComponent);

	return 0;
}
//------------------------------------------------------------------------------

int ShineProc::GetIterationCount(SToolProcessingData &procData)
{
	return _shineSettings.lastRepairFrameIndex - _shineSettings.firstRepairFrameIndex + 1;
}
//------------------------------------------------------------------------------

int ShineProc::BeginProcessing(SToolProcessingData &procData)
{
	GetToolProgressMonitor()->SetFrameCount(GetIterationCount(procData));
   GetToolProgressMonitor()->SetStatusMessage("Processing");
	GetToolProgressMonitor()->StartProgress();

	// Compute the pixel mask for the region of interest now for all frames
	// if the mask exists and is not animated
   _roiBlendPtr = nullptr;
   if (GetSystemAPI()->IsMaskAvailable())
   {
      if (!GetSystemAPI()->IsMaskAnimated())
		{
         // Only render the mask at the in frame and use that for all frames.
         int retVal = GetSystemAPI()->GetMaskRoi(_shineSettings.firstRepairFrameIndex, maskROI);
			if (retVal != 0)
         {
            return retVal;
         }

         _roiBlendPtr = maskROI.getBlendPtr();
      }
	}

   return 0;
}
//------------------------------------------------------------------------------

int ShineProc::BeginIteration(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("ShineProc::BeginIteration", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   _repairFrameIndex = _shineSettings.firstRepairFrameIndex + procData.iteration;
   TRACE_3(errout << "BEGIN ITERATION: Repair frame " << _repairFrameIndex);

   //=================
   // Compute scene bounds based on the list of preproc output data.
	CVideoFrameList *frameList = GetSystemAPI()->getVideoFrameList();
   auto clipIn = frameList->getInFrameIndex();
   auto clipOut = frameList->getOutFrameIndex();
   auto &preprocOutputList = *_shineSettings.shinePreprocOutputList;

   _sceneInFrameIndex = INVALID_FRAME_INDEX;
   _sceneOutFrameIndex = INVALID_FRAME_INDEX;
   _scenePreprocOutputListIndex = -1;
   for (_scenePreprocOutputListIndex = 0;
        _scenePreprocOutputListIndex < preprocOutputList.size();
        ++_scenePreprocOutputListIndex)
   {
      auto &listItem = preprocOutputList[_scenePreprocOutputListIndex];
      if (_repairFrameIndex >= listItem.inFrame && _repairFrameIndex < listItem.outFrame)
      {
         _sceneInFrameIndex = listItem.inFrame;
         _sceneOutFrameIndex = listItem.outFrame;
         break;
      }
   }

   MTIassert(_sceneInFrameIndex != INVALID_FRAME_INDEX && _sceneOutFrameIndex != INVALID_FRAME_INDEX);
   if (_sceneInFrameIndex == INVALID_FRAME_INDEX || _sceneOutFrameIndex == INVALID_FRAME_INDEX)
   {
      // Internal error, we didn't get passed the preproc data for this scene!
      return -22228;
   }

   auto shineRepairMask = reinterpret_cast<ShineRepairMask *>(_cheshireShineRepairMask);
   if (_sceneOutFrameIndex - _sceneInFrameIndex < ShineRepairMask::getMinimumShineRepairSceneSize())
   {
      int retVal = -22224;
//	   CAutoErrorReporter autoErr("ShineProc::BeginIteration", AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);
//      autoErr.errorCode = retVal;
//      autoErr.msg << "WARNING: Scene " << _sceneInFrameIndex << "-" << _sceneOutFrameIndex << " was not processed" << endl
//                              << "by the Shine tool beacause it's too short.";
      TRACE_0(errout << "WARNING: Scene " << _sceneInFrameIndex << "-" << _sceneOutFrameIndex << " was not processed" << endl
                     << "by the Shine tool beacause it's too short.");
      return retVal;
   }

   //=================
   // Set input frames.
	// Tell the Tool Infrastructure which frames we want as input
   // during this iteration.
   _firstValidInputFrameIndexPosition = -1;
   _countOfValidInputFrameIndexes = 0;
   for (auto i = 0; i < 9; ++i)
   {
      _inputFrameIndexList[i] = _repairFrameIndex - 4 + i;
      if (_inputFrameIndexList[i] >= _sceneInFrameIndex
      && _inputFrameIndexList[i] < _sceneOutFrameIndex)
      {
         if (_firstValidInputFrameIndexPosition == -1)
         {
            _firstValidInputFrameIndexPosition = i;
         }

         ++_countOfValidInputFrameIndexes;
      }
      else
      {
         _inputFrameIndexList[i] = -1;
      }
   }

	SetInputFrames(0, _countOfValidInputFrameIndexes, &_inputFrameIndexList[_firstValidInputFrameIndexPosition]);

   //=================
   // Set frames to release after the iteration is done.
   // If this is the last frame in the current shot or if this is the repair
   // frame in the range, release all the the remaining frames.
   // Else release the oldest frame, but only if it's the repair frame - 4.
   if (_repairFrameIndex == _shineSettings.lastRepairFrameIndex
   || _repairFrameIndex == (_sceneOutFrameIndex - 1))
   {
      SetFramesToRelease(0, _countOfValidInputFrameIndexes, &_inputFrameIndexList[_firstValidInputFrameIndexPosition]);
      for (int i = 0; i < _countOfValidInputFrameIndexes; ++i)
      {
         TRACE_3(errout << "frame " << _inputFrameIndexList[_firstValidInputFrameIndexPosition + i] << " will be released");
      }
   }
   else if (_inputFrameIndexList[_firstValidInputFrameIndexPosition] == _repairFrameIndex - 4)
   {
      SetFramesToRelease(0, 1, &_inputFrameIndexList[_firstValidInputFrameIndexPosition]);
      TRACE_3(errout << "frame " << _inputFrameIndexList[_firstValidInputFrameIndexPosition] << " will be released");
   }

   //==================
   // Set output frame = the repair frame.
	int outputFrameIndexList[1];
	outputFrameIndexList[0] = _repairFrameIndex;
   SetOutputFrames(0, 1, outputFrameIndexList);

   //==================
   // If the mask is animated, then must get a new region-of-interest blend array for every frame.
   if (GetSystemAPI()->IsMaskAnimated())
   {
      int retVal = GetSystemAPI()->GetMaskRoi(_repairFrameIndex, maskROI);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Internal Error in function GetMaskRoi()";
         return retVal;
      }

      _roiBlendPtr = maskROI.getBlendPtr();
   }

	return 0;
}
//------------------------------------------------------------------------------

int ShineProc::DoProcess(SToolProcessingData &procData)
{
	int retVal = 0;
   string errorFunc("<UNKNOWN>");
   auto shineRepairMask = reinterpret_cast<ShineRepairMask *>(_cheshireShineRepairMask);

	try
   {
      // ==============
      // Copy the original image to the output frame.
      // Not sure if we really need to do this, we seem to recall it fixes an issue.
      // Note: this may have to wait to allocate memory.
      int firstValidFrameIndex = _inputFrameIndexList[_firstValidInputFrameIndexPosition];
		const int InputSetIndexOfFrameToFix = _repairFrameIndex - firstValidFrameIndex;
		CToolFrameBuffer *repairToolFrameBuffer = GetRequestedInputFrame(0, InputSetIndexOfFrameToFix);
      if (repairToolFrameBuffer == nullptr)
      {
         TRACE_0(errout << "DOPROCESS COULD NOT GET repair buffer ["
                  << InputSetIndexOfFrameToFix << "] --> "
                  << _inputFrameIndexList[InputSetIndexOfFrameToFix]);
         return -22225;
      }

		CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
      if (outToolFrameBuffer == nullptr)
      {
         TRACE_0(errout << "DOPROCESS COULD NOT GET output buffer");
         return -22226;
      }

      CopyInputFrameBufferToOutputFrameBuffer(repairToolFrameBuffer, outToolFrameBuffer);

      // ==============
      // Get optical flow data and pass it to the ShineRepairMask.
      // Must only do this ONCE each time the optical flow data changes.
      // TODO: MAKE SURE preprocOutput has valid data! QQQ
      if (_sceneInFrameIndex != _oldSceneInFrameIndex || _sceneOutFrameIndex != _oldSceneOutFrameIndex)
      {
         _oldSceneInFrameIndex = _sceneInFrameIndex;
         _oldSceneOutFrameIndex = _sceneOutFrameIndex;
         auto &preprocOutputList = *_shineSettings.shinePreprocOutputList;
         auto &listItem = preprocOutputList[_scenePreprocOutputListIndex];
         auto opticalFlowPtr = static_cast<OpticalFlowData *>(listItem.preprocOutput->data);
         shineRepairMask->setOpticalFlow(*opticalFlowPtr);
      }

      // ==============
      // Wrap the input buffers and stick them in a vector
      vector<Ipp16uArray> framesToPush;
      for (int i = 0; i < _countOfValidInputFrameIndexes; ++i)
      {
         int inputFrameIndexPosition = _firstValidInputFrameIndexPosition + i;
         if (_inputFrameIndexList[inputFrameIndexPosition] == -1)
         {
            continue;
         }

         CToolFrameBuffer *inputToolFrameBuffer = GetRequestedInputFrame(0, i);
         if (inputToolFrameBuffer == nullptr)
         {
            TRACE_0(errout << "DOPROCESS COULD NOT GET input buffer ["
                     << InputSetIndexOfFrameToFix << "] --> "
                     << _inputFrameIndexList[InputSetIndexOfFrameToFix]);
            return -22227;
         }

         framesToPush.push_back(Ipp16uArray(_imageSize, inputToolFrameBuffer->GetVisibleFrameBufferPtr()));
         framesToPush.back().setOriginalBits(_bitsPerPixelComponent);
      }

      // ==============
      // Push the frames to the repair mask instance.
      int repairFrameSceneIndex = _repairFrameIndex - _sceneInFrameIndex;
      int firstInputFrameSceneIndex = firstValidFrameIndex - _sceneInFrameIndex;
      bool success = false;
      try
      {
         success = shineRepairMask->pushFrames(framesToPush, firstInputFrameSceneIndex, repairFrameSceneIndex);
      }
      catch (...)
      {
         TRACE_0(errout << "ERROR: shineRepairMask->pushFrames threw an exception");
         retVal = -22220;
         throw;
      }

      if (!success)
      {
         TRACE_0(errout << "INTERNAL ERROR: shineRepairMask->pushFrames said we didn't push enough buffers.");
         retVal = -22221;
         return retVal;
      }

      // ==============
      // Wrap the output buffer
      Ipp16uArray wrappedOutputBuffer(_imageSize, outToolFrameBuffer->GetVisibleFrameBufferPtr());
      wrappedOutputBuffer.setOriginalBits(_bitsPerPixelComponent);

      bool needToDoDark = _shineSettings.detectDark;
      bool needToDoBright = _shineSettings.detectBright;
      bool needToDoAlpha = _shineSettings.detectAlpha;
      while (true)
      {
         // ==============
         // Convert the GUI parameters.
         const int DisablingContrast = 100000000;
         int maxSizeGuiSetting = 100;
         int minSizeGuiSetting = 1;
         int aggressionGuiSetting = 100;
         int darkRelativeContrastGuiSetting = -1;     // DISABLED
         int darkAbsoluteContrastGuiSetting = -1;     // DISABLED
         int brightRelativeContrastGuiSetting = -1;   // DISABLED
         int brightAbsoluteContrastGuiSetting = -1;   // DISABLED
         Ipp8uArray alphaDetectionMap;

         if (needToDoDark)
         {
            needToDoDark = false;
            maxSizeGuiSetting = _shineSettings.darkMaxSize;
            minSizeGuiSetting = _shineSettings.darkMinSize;
            darkRelativeContrastGuiSetting = _shineSettings.darkTemporalContrast;
            darkAbsoluteContrastGuiSetting = _shineSettings.darkSpatialContrast;
            aggressionGuiSetting = _shineSettings.darkMotionTolerance;

// Mertus says this does not work anymore!
// Update: Mertus doesn't remember why he said this, so I'm putting it back in!
            if (needToDoBright
            && _shineSettings.darkMaxSize == _shineSettings.brightMaxSize
            && _shineSettings.darkMinSize == _shineSettings.brightMinSize
            && _shineSettings.darkMotionTolerance == _shineSettings.brightMotionTolerance)
            {
               needToDoBright = false;
               brightRelativeContrastGuiSetting = _shineSettings.brightTemporalContrast;
               brightAbsoluteContrastGuiSetting = _shineSettings.brightSpatialContrast;
            }
         }
         else if (needToDoBright)
         {
            needToDoBright = false;
            maxSizeGuiSetting = _shineSettings.brightMaxSize;
            minSizeGuiSetting = _shineSettings.brightMinSize;
            brightRelativeContrastGuiSetting = _shineSettings.brightTemporalContrast;
            brightAbsoluteContrastGuiSetting = _shineSettings.brightSpatialContrast;
            aggressionGuiSetting = _shineSettings.brightMotionTolerance;
         }
         else if (needToDoAlpha)
         {
            needToDoAlpha = false;
            maxSizeGuiSetting = _shineSettings.alphaMaxSize;
            aggressionGuiSetting = _shineSettings.alphaMotionTolerance;
            alphaDetectionMap = CreateAlphaDetectionMap(_repairFrameIndex, repairToolFrameBuffer->GetVisibleFrameBufferPtr());
         }
         else
         {
            break;
         }


         // Max size in pixels is computed as 2 * (guiSetting**2 * frameWidth * frameHeight) / (4096 * 3112)
         // So 100 = 10,000 at 4K (100x100), or 2500 at 2K (50x50)
         // and 50 = 2500 at 4K (50x50) or 625 at 2K (25x25)
//         auto frameSizeScalar = double(2 * _imageSize.width * _imageSize.height) / (4096 * 3112);
//         auto maxSizeSetting = maxSizeGuiSetting * maxSizeGuiSetting * frameSizeScalar;
         auto maxSizeSetting = double(maxSizeGuiSetting);

         // Min size is actual number of pixels
         auto minSizeSetting = double(minSizeGuiSetting);

         // High contrast maps 0 - 100 to 100 - 0 (reversed). Setting it to a
         // ginormous number effectively disables finding any bright debris.
         MTIassert(brightRelativeContrastGuiSetting <= 100 && brightRelativeContrastGuiSetting >= -1);
         MTIassert(brightAbsoluteContrastGuiSetting <= 100 && brightAbsoluteContrastGuiSetting >= -1);
         auto highTemporalContrastSetting = (brightRelativeContrastGuiSetting < 0)
                                             ? DisablingContrast
                                             : (100.0 - brightRelativeContrastGuiSetting);
         auto highSpatialContrastSetting = (brightAbsoluteContrastGuiSetting < 0)
                                             ? DisablingContrast
                                             : (100.0 - brightAbsoluteContrastGuiSetting);

         // Low contrast maps 0 - 100 to 100 - 0 (reversed). Setting it to a
         // ginormous number effectively disables finding any dark debris.
         MTIassert(darkRelativeContrastGuiSetting <= 100 && darkRelativeContrastGuiSetting >= -1);
         MTIassert(darkAbsoluteContrastGuiSetting <= 100 && darkAbsoluteContrastGuiSetting >= -1);
         auto lowTemporalContrastSetting = (darkRelativeContrastGuiSetting < 0)
                                             ? DisablingContrast
                                             : (100.0 - darkRelativeContrastGuiSetting);
         auto lowSpatialContrastSetting = (darkAbsoluteContrastGuiSetting < 0)
                                             ? DisablingContrast
                                             : (100.0 - darkAbsoluteContrastGuiSetting);

         // Motion threshold 0 - 100 (pass through from GUI)
         MTIassert(aggressionGuiSetting <= 100 && aggressionGuiSetting >= 0);
         auto fidelitySetting = aggressionGuiSetting;

         ShineInteractiveParams shineInteractiveParams;
         shineInteractiveParams.highScaledContrast = highTemporalContrastSetting;
         shineInteractiveParams.lowScaledContrast = lowTemporalContrastSetting;
         shineInteractiveParams.highSpatialContrast = highSpatialContrastSetting;
         shineInteractiveParams.lowSpatialContrast = lowSpatialContrastSetting;
         shineInteractiveParams.fidelity = fidelitySetting;
         shineInteractiveParams.minSize = (minSizeSetting > 0) ? minSizeSetting : 1;
        // shineInteractiveParams.maxSize = maxSizeSetting;

         // HACK TO MAKE MAX SIZE BIGGER, 1 => 1, 50 => 100, 99 = 5000, 100 => infinity
         // double coeff[] = {0.9998,  -48.9690, 48.9692};

         // HACK TO MAKE MAX SIZE BIGGER, 1 => 1, 50 => 150, 99 = 10000, 100 => infinity
         double coeff[] = {2.0202,  -99.9894,   98.9692 };

         auto ms = coeff[2];
         ms += coeff[1] * maxSizeSetting;
         ms += coeff[0] * maxSizeSetting * maxSizeSetting;
        	shineInteractiveParams.maxSize = ms;
         if (maxSizeSetting >= 100)
         {
           shineInteractiveParams.maxSize = 100000;
         }


         // ==============
         // Do the repair.
         Ipp16uArray repairedBuffer;
         try
         {
            auto doingAlpha = !alphaDetectionMap.isEmpty();
            repairedBuffer = shineRepairMask->repairImage(
                                                repairFrameSceneIndex,
                                                shineInteractiveParams,
                                                alphaDetectionMap,
                                                doingAlpha,
                                                doingAlpha);
         }
         catch (...)
         {
            TRACE_0(errout << "shineRepairMask->repairImage threw an exception");
            retVal = -22222;
            throw;
         }

         // ==============
         // For each replaced pixel, copy the defective pixel data to the history
         // pixel region, and copy the repaired pixel data to the output buffer.
         // Note: need to use <<= to copy the data instead of clobbering
         // _modifiedPixelMask's pointer.
         _modifiedPixelMask <<= shineRepairMask->getRepairMask();
         CPixelRegionList *origValuesRegionList = outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr();
         auto imageBuffer = repairToolFrameBuffer->GetVisibleFrameBufferPtr();
         if (_roiBlendPtr)
         {
            int linearIndex = 0;
            for (auto y = 0; y < _modifiedPixelMask.getHeight(); ++y)
            {
               for (auto x = 0; x < _modifiedPixelMask.getWidth(); ++x)
               {
                  auto blendVal = _roiBlendPtr[linearIndex++];
                  if (blendVal > 0 && _modifiedPixelMask[{x, y}] != 0)
                  {
                     POINT xy = {x, y};
                     origValuesRegionList->Add(xy, imageBuffer, _imageSize.width, _imageSize.height, 3);
                     auto blendFrac = blendVal / 255.F;
                     auto oneMinusBlendFrac = 1.F - blendFrac;
                     wrappedOutputBuffer[{x, y, 0}] = (oneMinusBlendFrac * wrappedOutputBuffer[{x, y, 0}])
                                                      + (blendFrac * repairedBuffer[{x, y, 0}]);
                     wrappedOutputBuffer[{x, y, 1}] = (oneMinusBlendFrac * wrappedOutputBuffer[{x, y, 1}])
                                                      + (blendFrac * repairedBuffer[{x, y, 1}]);
                     wrappedOutputBuffer[{x, y, 2}] = (oneMinusBlendFrac * wrappedOutputBuffer[{x, y, 2}])
                                                      + (blendFrac * repairedBuffer[{x, y, 2}]);
                  }
               }
            }
         }
         else
         {
            for (auto y = 0; y < _modifiedPixelMask.getHeight(); ++y)
            {
               for (auto x = 0; x < _modifiedPixelMask.getWidth(); ++x)
               {
                  if (_modifiedPixelMask[{x, y}] != 0)
                  {
                     POINT xy = {x, y};
                     origValuesRegionList->Add(xy, imageBuffer, _imageSize.width, _imageSize.height, 3);
                     wrappedOutputBuffer[{x, y, 0}] = repairedBuffer[{x, y, 0}];
                     wrappedOutputBuffer[{x, y, 1}] = repairedBuffer[{x, y, 1}];
                     wrappedOutputBuffer[{x, y, 2}] = repairedBuffer[{x, y, 2}];
                  }
               }
            }
         }
      }

      // ==============
      // Done!
		return 0; // SUCCESS
	}
	catch (string)
	{; // do nothing here
	}
	catch (std::bad_alloc)
	{
		retVal = ERR_PLUGIN_AF_MALLOC;
	}
	catch (...)
	{
      if (retVal == 0)
      {
         TRACE_0(errout << "doprocess threw an exception");
         retVal = -22223;
         throw;
      }
	}
	try
	{
		CAutoErrorReporter autoErr("CAutoFilterSingleProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		string errorMsg;
		switch (retVal)
		{
		case ERR_PLUGIN_AF_NO_FILTERS:
			errorMsg = "No valid filters found in active filter set";
			break;
		case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
			errorMsg = "No alpha channel - cannot find defect map";
			break;
		case ERR_PLUGIN_AF_MALLOC:
			errorMsg = "Out of memory";
			break;
		default:
			errorMsg = "Internal Error";
			break;
		}

		autoErr.errorCode = retVal;
		autoErr.msg << errorMsg << endl << " in function " << errorFunc;
	}
	catch (...)
	{
		TRACE_0(errout << "ShineProc::DoProcess: FATAL ERROR, code " << retVal << endl <<
				"CAUGHT EXCEPTION when trying to display error dialog!");
	}

	return retVal; // ERROR
}
//------------------------------------------------------------------------------

int ShineProc::EndIteration(SToolProcessingData &procData)
{
	GetToolProgressMonitor()->BumpProgress(1);

	return 0;
}
//------------------------------------------------------------------------------

int ShineProc::EndProcessing(SToolProcessingData &procData)
{
	if (procData.iteration == GetIterationCount(procData))
	{
		GetToolProgressMonitor()->SetStatusMessage("Processing complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	return 0;
}
// ---------------------------------------------------------------------------

// Stolen from the tool - need to refactor! QQQ
Ipp8uArray ShineProc::CreateAlphaDetectionMap(
   int frameIndex,
   MTI_UINT16 *frameBits)
{
   CHRTimer CreateDetectionMap_TIMER;

   CAlgorithmAF algorithmAF;
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
   if (imageFormat == 0)
   {
      return Ipp8uArray(); // No image format, can't happen
   }

   RECT activePictureRect = imageFormat->getActivePictureRect();
   MTI_UINT16 maxDataValue[3];
   imageFormat->getComponentValueMax(maxDataValue);
   long lNRow = imageFormat->getTotalFrameHeight();
   long lNCol = imageFormat->getTotalFrameWidth();
   long lNRowCol = lNCol * lNRow;
   long lNCom = imageFormat->getComponentCountExcAlpha();
   long lActiveStartRow = activePictureRect.top;
   long lActiveStopRow = activePictureRect.bottom + 1;
   long lActiveStartCol = activePictureRect.left;
   long lActiveStopCol = activePictureRect.right + 1;

   if (lNRowCol > ROW_COL_10K)
   {
      TRACE_0(errout << "ERROR: Image is too large (" << lNCol << "x" << lNRow << " > " << ROW_COL_10K << ")");
      return Ipp8uArray(); // TOO BIG!
   }

   CHRTimer PREALLOC_MEMORY_TIMER;
   std::unique_ptr<AF_PreallocedMemory> preallocedMemory(
         (lNRowCol <= ROW_COL_2K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_2K) :
         ((lNRowCol <= ROW_COL_4K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_4K) :
         ((lNRowCol <= ROW_COL_6K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_6K) :
         ((lNRowCol <= ROW_COL_8K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_8K) :
         (AF_PreallocedMemory*)(new AF_PreallocedMemory_10K)))));
//   DBTRACE(PREALLOC_MEMORY_TIMER.ReadAsString());

   algorithmAF.Initialize(true, preallocedMemory.get());
   int retVal = algorithmAF.SetDimension(lNRow, lNCol, lNCom, lActiveStartRow, lActiveStopRow, lActiveStartCol, lActiveStopCol);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: AlgorithmAF->SetDimension FAIL (" << retVal << ")");
      return Ipp8uArray(); // TOO BIG!
   }

   MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
   MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];
   imageFormat->getComponentValueMin(cvMin);
   imageFormat->getComponentValueMax(cvMax);
   algorithmAF.SetImageBounds(cvMin, cvMax);

   algorithmAF.SetAlphaMatteType(imageFormat->getAlphaMatteType(), imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR);
   algorithmAF.SetEmergencyStopFlagPtr(nullptr);

   CAutoFilterToolParameters afToolParams;
   afToolParams.SetRegionOfInterest(GetSystemAPI()->GetMaskRoi());

   // Make parameters for a fake alpha filter.
	CAutoFilterParameters &toolParams = afToolParams.getParameterRef();
   toolParams.lNSetting = 1;
   toolParams.strSettingName[0] = "SHINE_ALPHA";
   toolParams.lPrincipalComponent[0] = -1;  // Detect channel is not used
   toolParams.lComponentMask[0] = 7;        // Process all channels
   toolParams.faMinVal[0][0] =  0;
   toolParams.faMinVal[0][1] =  0;
   toolParams.faMinVal[0][2] =  0;
   toolParams.faMaxVal[0][0] =  100;
   toolParams.faMaxVal[0][1] =  100;
   toolParams.faMaxVal[0][2] =  100;
   toolParams.faMotionOffsetRow[0] = 0;
   toolParams.faMotionOffsetCol[0] = 0;
   toolParams.faMotionThresh[0] = _shineSettings.alphaMotionTolerance;
   toolParams.faEdgeBlur[0] = 0;
   toolParams.bUseHardMotionOnDust[0] = false;
   toolParams.lDustMaxSize[0] = 0;
   toolParams.bUseAlphaMatteToFindDebris[0] = true;
   toolParams.lGrainFilterParam[0] = _shineSettings.alphaGrainSuppression;
   toolParams.bUseGrainFilter[0] = _shineSettings.alphaGrainSuppression != 0;
   toolParams.faContrastSpatial[0] = 100;
   toolParams.faContrastTemporal[0] = 100;
   toolParams.laContrastDirection[0] = 0;  // Must be 0 for alpha
   toolParams.faMaxSize[0] = _shineSettings.alphaMaxSize;
   toolParams.faMinSize[0] = 1; // no restriction for alpha
	toolParams.lFrameIndex = frameIndex;
   retVal = algorithmAF.AssignParameter(&toolParams);

   MTI_UINT16 *uspaSrc[AF_NUM_SRC_FRAMES] = {frameBits, frameBits, frameBits};
   long laFrameIndexInInputSet[AF_NUM_SRC_FRAMES] = {-1, 0, 1};
   algorithmAF.SetSrcImgPtr(uspaSrc, laFrameIndexInInputSet);
   algorithmAF.SetDstImgPtr(frameBits);
   algorithmAF.SetPixelRegionList(nullptr);
   algorithmAF.SetModifiedPixelMap(nullptr);

   // We don't try to reuse these. I suppose that might speed up repeated D key
   // on the same frame...
   GrainFilterSet grainFilters;
   BlobPixelArrayWrapper *blobPixelArrayWrapperPtr = nullptr;
   Ipp8uArray detectionMap = algorithmAF.MakeDetectionMap(grainFilters, &blobPixelArrayWrapperPtr);

   // DBTRACE(CreateDetectionMap_TIMER.ReadAsString());

   return detectionMap;
}
//------------------------------------------------------------------------------
