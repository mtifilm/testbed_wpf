//---------------------------------------------------------------------------

#pragma hdrstop

#include "ShinePreprocOutput.h"
#include "OpticalFlowData.h"

#include <cstring>
#include <fstream>
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

ShinePreprocOutput::ShinePreprocOutput(void *opaqueData)
{
	data = opaqueData;
}
//---------------------------------------------------------------------------

ShinePreprocOutput::~ShinePreprocOutput()
{
	OpticalFlowData *opticalFlowPtr = static_cast<OpticalFlowData *>(data);
	delete opticalFlowPtr;
}
//---------------------------------------------------------------------------

bool ShinePreprocOutput::isDataValid() const
{
	OpticalFlowData *opticalFlowPtr = static_cast<OpticalFlowData *>(data);
   return opticalFlowPtr != nullptr && opticalFlowPtr->size() > 0;
}
//---------------------------------------------------------------------------

void ShinePreprocOutput::makeDataInvalid()
{
	OpticalFlowData *opticalFlowPtr = static_cast<OpticalFlowData *>(data);
   delete opticalFlowPtr;

   // Empty opticalFlowData is invalid.
   data = new OpticalFlowData;
}
//---------------------------------------------------------------------------

namespace {

   MTI_INT64 GetFileSize(const string &filename)
   {
      LARGE_INTEGER fileSize;
      HANDLE hFile = ::CreateFile(filename.c_str(), 0, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
      if (hFile == INVALID_HANDLE_VALUE)
      {
         return -1;
      }

      ::GetFileSizeEx(hFile, &fileSize);
      ::CloseHandle(hFile);

      return fileSize.QuadPart;
   }

};
//---------------------------------------------------------------------------

/* static */
ShinePreprocOutputSharedPtr ShinePreprocOutput::ReadFromFile(const string &filename)
{
   // Compute number of vector elements based on the file size and
   // create an OpticalFlowData of that size.
   auto fileSize = GetFileSize(filename);
   if (fileSize < 0)
   {
      return nullptr;
   }

   MTIassert((fileSize % sizeof(OpticalFlowDatum)) == 0);
   auto elementCount = int(fileSize / sizeof(OpticalFlowDatum));
   if (elementCount == 0)
   {
      if (fileSize > 0)
      {
         TRACE_0(errout << "ERROR: File " << filename << "is too short (" << fileSize << "<" << sizeof(OpticalFlowDatum) << ")");
      }

      auto emptyOpticalFlowData = new OpticalFlowData;
      return std::make_shared<ShinePreprocOutput>(emptyOpticalFlowData);
   }

   auto opticalFlowData = new OpticalFlowData(elementCount);

   // Overlay the vector with the data from the file.
	auto stream = std::fopen(filename.c_str(), "rb");
	if (stream == NULL)
	{
      TRACE_0(errout << "ERROR: Failed to open file " << filename << "for reading" << endl
              << "Reason: " << std::strerror(errno));
      delete opticalFlowData;
      return nullptr;
	}

   auto actualCount = std::fread(opticalFlowData->data(), sizeof(OpticalFlowDatum), elementCount, stream);
   MTIassert(actualCount == elementCount);
	if (actualCount != elementCount)
	{
      TRACE_0(errout << "ERROR: Failed to read file " << filename << endl
              << "Reason: " << std::strerror(errno));
      std::fclose(stream);
      delete opticalFlowData;
      return nullptr;
	}

   std::fclose(stream);
   return std::make_shared<ShinePreprocOutput>(opticalFlowData);
}
//---------------------------------------------------------------------------

/* static */
int ShinePreprocOutput::WriteToFile(const string &filename, ShinePreprocOutputSharedPtr &shinePreprocOutput)
{
	OpticalFlowData *opticalFlowData = static_cast<OpticalFlowData *>(shinePreprocOutput->data);

   // Overlay the vector with the data from the file.
	auto stream = std::fopen(filename.c_str(), "wb");
	if (stream == NULL)
	{
      TRACE_0(errout << "ERROR: Failed to open file " << filename << "for writing" << endl
              << "Reason: " << std::strerror(errno));
      return -1;
	}

   auto actualCount = std::fwrite(opticalFlowData->data(), sizeof(OpticalFlowDatum), opticalFlowData->size(), stream);
   MTIassert(actualCount == opticalFlowData->size());
	if (actualCount != opticalFlowData->size())
	{
      TRACE_0(errout << "ERROR: Failed to write file " << filename << endl
              << "Reason: " << std::strerror(errno));
      std::fclose(stream);
      // QQQ try to delete the file?
      return -1;
	}

   std::fclose(stream);
   return 0;
}
//---------------------------------------------------------------------------
