#ifndef ShinePreprocOutputH
#define ShinePreprocOutputH
//---------------------------------------------------------------------------

#include <memory>
using std::shared_ptr;
#include <string>
using std::string;
#include <vector>
using std::vector;
//---------------------------------------------------------------------------


struct ShinePreprocOutput;
typedef shared_ptr<ShinePreprocOutput>  ShinePreprocOutputSharedPtr;


struct ShinePreprocOutput
{
	void *data = nullptr;

	ShinePreprocOutput(void *opaqueData);
	~ShinePreprocOutput();

   static ShinePreprocOutputSharedPtr ReadFromFile(const string &path);
   static int WriteToFile(const string &path, ShinePreprocOutputSharedPtr &shinePreprocOutput);

   bool isDataValid() const;
   void makeDataInvalid();
};

struct ShinePreprocessingOutputItem
{
   int inFrame = -1;
   int outFrame = -1;
   ShinePreprocOutputSharedPtr preprocOutput;

   ShinePreprocessingOutputItem() = default;
   ShinePreprocessingOutputItem(int inFrameArg, int outFrameArg, ShinePreprocOutputSharedPtr preprocOutputArg)
   : inFrame(inFrameArg)
   , outFrame(outFrameArg)
   , preprocOutput(preprocOutputArg)
   {}
};

typedef vector<ShinePreprocessingOutputItem> ShinePreprocOutputList;

#endif
