object ExistingSetForm: TExistingSetForm
  Left = 1641
  Top = 88
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Hack Filter Set'
  ClientHeight = 144
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PleaseSelectLabel: TLabel
    Left = 16
    Top = 28
    Width = 178
    Height = 14
    Caption = 'Please select a filter set to hack:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object FilterBankSelectionComboBox: TComboBox
    Left = 16
    Top = 52
    Width = 225
    Height = 21
    Style = csDropDownList
    TabOrder = 0
    Items.Strings = (
      'Normal'
      '3 Layer Separation'
      'Alpha Filter')
  end
  object OkButton: TButton
    Left = 50
    Top = 105
    Width = 75
    Height = 21
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 143
    Top = 105
    Width = 75
    Height = 21
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
