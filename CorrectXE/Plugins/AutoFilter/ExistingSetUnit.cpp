//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ExistingSetUnit.h"
#include "MTIstringstream.h"
#include <utility>
using std::pair;
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TExistingSetForm *ExistingSetForm;
//---------------------------------------------------------------------------
__fastcall TExistingSetForm::TExistingSetForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TExistingSetForm::SetOpName(const string &newOpName)
{
   MTIostringstream os1, os2;
   string capOpName = newOpName;

   if (capOpName.size() >= 1)
      capOpName[0] += ('A' - 'a');

   os1 << capOpName << " Filter Set";
   Caption = os1.str().c_str();

   os2 << "Please select a filter set to " << newOpName;
   PleaseSelectLabel->Caption = os2.str().c_str();
}
//---------------------------------------------------------------------------

void TExistingSetForm::SetListOfFilterSetNames(
                       const vector<string> &newListOfNames)
{
   FilterBankSelectionComboBox->Clear();
   for (unsigned i = 0; i < newListOfNames.size(); ++i)
   {
      FilterBankSelectionComboBox->Items->Append(newListOfNames[i].c_str());
   }
}
//---------------------------------------------------------------------------

void TExistingSetForm::SetVisibleIndex(int newVisibleIndex)
{
    FilterBankSelectionComboBox->ItemIndex = newVisibleIndex;
}
//---------------------------------------------------------------------------

int TExistingSetForm::GetSelectedIndex()
{
   return FilterBankSelectionComboBox->ItemIndex;
}
 //---------------------------------------------------------------------------



