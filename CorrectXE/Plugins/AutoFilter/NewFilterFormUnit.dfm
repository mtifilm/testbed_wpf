object NewFilterForm: TNewFilterForm
  Left = 2193
  Top = 87
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'NewFilterForm'
  ClientHeight = 144
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    258
    144)
  PixelsPerInch = 96
  TextHeight = 13
  object PleaseEnterlabel: TLabel
    Left = 12
    Top = 28
    Width = 214
    Height = 14
    Caption = 'Please enter a name for the new filter:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit: TEdit
    Left = 12
    Top = 52
    Width = 238
    Height = 21
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    Text = 'Custom'
    OnEnter = EditEnter
  end
  object OkButton: TButton
    Left = 50
    Top = 105
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 143
    Top = 105
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
