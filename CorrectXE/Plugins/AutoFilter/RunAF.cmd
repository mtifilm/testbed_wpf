# RunAF.cmd: sample command file for AutoFilter

[General]
PrincipleComponent=1
ComponentMask=0x7
SeparateLayerFlag=true
FirstFrame=10
LastFrame=12
FileNameIn=/mtiimages/Tank/Tank.00000.rgb
FileNameOut=/tmp/Tank.00000.rgb

[AutoFilter0]
MinVal0=0.
MaxVal0=100.
MinVal1=0.
MaxVal1=100.
MinVal2=0.
MaxVal2=100.
ContrastSpatial=50.
ContrastTemporal=50.
ContrastDirection=1
MotionOffsetRow=50.
MotionOffsetCol=50.
MotionThresh=50.
Size=50.
Master=50.
Severity=50.
