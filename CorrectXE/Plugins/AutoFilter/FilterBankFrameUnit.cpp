//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FilterBankFrameUnit.h"

#include "IniFile.h"    // for MTIassert (which is really in machine.h...)
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BasicFilterFrameUnit"
#pragma resource "*.dfm"
TFilterBankFrame *FilterBankFrame;

//---------------------------------------------------------------------------

__fastcall TFilterBankFrame::TFilterBankFrame(TComponent* Owner)
    : TFrame(Owner)
{
   // Too lazy to make this right
#if (MAX_DISPLAYED_FILTERS != 6)
yo! fix the stuff below!
#endif

   // Haha yes the shift from 1-based to 0-based breeds confusion!
   FilterSelectPanel[0] = Filter1SelectPanel;
   FilterSelectPanel[1] = Filter2SelectPanel;
   FilterSelectPanel[2] = Filter3SelectPanel;
   FilterSelectPanel[3] = Filter4SelectPanel;
   FilterSelectPanel[4] = Filter5SelectPanel;
   FilterSelectPanel[5] = Filter6SelectPanel;

   FilterFrame[0] = BasicFilterFrame1;
   FilterFrame[1] = BasicFilterFrame2;
   FilterFrame[2] = BasicFilterFrame3;
   FilterFrame[3] = BasicFilterFrame4;
   FilterFrame[4] = BasicFilterFrame5;
   FilterFrame[5] = BasicFilterFrame6;

   for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
   {
      FilterFrame[i]->Tag = i;
      SET_CBHOOK(UseCheckBoxChanged,    FilterFrame[i]->UseCheckBoxChange);
      SET_CBHOOK_DATA(FilterWasClicked,
                      FilterFrame[i]->FilterClicked,
                      FilterFrame[i]);
      SET_CBHOOK_DATA(FilterWasShiftClicked,
                      FilterFrame[i]->FilterShiftClicked,
                      FilterFrame[i]);
      SET_CBHOOK_DATA(FilterWasCtrlClicked,
                      FilterFrame[i]->FilterCtrlClicked,
                      FilterFrame[i]);
   }

   UngangAll();
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Enable(int filterNumber, bool flag)
{
   if (filterNumber >= 0 && filterNumber < MAX_DISPLAYED_FILTERS)
   {
      FilterFrame[filterNumber]->Enable(flag);
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Select(int filterNumber)
{
   MTIassert(filterNumber >= 0 && filterNumber < 6);
   MTIassert(FilterFrame[filterNumber]->IsEnabled());

   if (!IsSelected(filterNumber))
   {
      int selectedFilterNumber = GetSelectedFilterNumber();

      if (selectedFilterNumber != INVALID_FILTER_NUMBER)
      {
         if (IsGanged(filterNumber))
         {
            // We are stealing selection away from another filter,
            // while preserving the present ganging state
            FilterFrame[selectedFilterNumber]->SetState(FFS_GANGED);
         }
         else
         {
            // We weren't part of the gang, so start fresh
            UngangAll();
            Unselect(selectedFilterNumber);
         }
      }

      // Select new filter
////      FilterSelectPanel[filterNumber]->Visible = true;
      FilterFrame[filterNumber]->SetState(FFS_SELECTED);

      // Don't SelectionChange.Notify() here
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Unselect(int filterNumber)
{
   if (filterNumber == INVALID_FILTER_NUMBER)
      return;

   MTIassert(filterNumber >= 0 && filterNumber < 6);
   MTIassert(FilterFrame[filterNumber]->IsEnabled());

   if (IsSelected(filterNumber))
   {
////      FilterSelectPanel[filterNumber]->Visible = false;
      FilterFrame[filterNumber]->SetState(FFS_UNSELECTED);
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Gang(int filterNumber)
{
   MTIassert(filterNumber >= 0 && filterNumber < 6);
   MTIassert(FilterFrame[filterNumber]->IsEnabled());

   if (!IsGanged(filterNumber))
   {
////      FilterSelectPanel[filterNumber]->Visible = true;
      FilterFrame[filterNumber]->SetState(FFS_GANGED);
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Ungang(int filterNumber)
{
   MTIassert(filterNumber >= 0 && filterNumber < 6);
   MTIassert(FilterFrame[filterNumber]->IsEnabled());

   // Never ungang the selected filter!
   if (IsGanged(filterNumber) && !IsSelected(filterNumber))
   {
////      FilterSelectPanel[filterNumber]->Visible = false;
      FilterFrame[filterNumber]->SetState(FFS_UNSELECTED);
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::GangAll(void)
{
   for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
   {
      if (FilterFrame[i]->IsEnabled())
      {
         Gang(i);
      }
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::UngangAll(void)
{
   for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
   {
      if (FilterFrame[i]->IsEnabled())
      {
         Ungang(i);
      }
   }
}
//---------------------------------------------------------------------------

int TFilterBankFrame::GetSelectedFilterNumber(void)
{
   int retVal = INVALID_FILTER_NUMBER;

   for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
   {
      if (FilterFrame[i]->IsSelected())
      {
         retVal = i;
         break;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

int TFilterBankFrame::GetGangCount()
{
   int retVal = 0;

   for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
   {
      if (FilterFrame[i]->IsGanged())
      {
         ++retVal;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

void TFilterBankFrame::Use(int filterNumber, bool flag)
{
   if (filterNumber >= 0 && filterNumber < MAX_DISPLAYED_FILTERS)
      FilterFrame[filterNumber]->Use(flag);
}
//---------------------------------------------------------------------------

void TFilterBankFrame::SetFilterName(int filterNumber,
                                     const string &newName)
{
   if (filterNumber >= 0 && filterNumber < MAX_DISPLAYED_FILTERS)
      FilterFrame[filterNumber]->SetFilterName(newName);
}
//---------------------------------------------------------------------------

void TFilterBankFrame::AF_SetValues(int filterNumber,
											int newMaxSize,  int newMinSize, int newDustSize,
											int newContrast, int newConfid,
											int newEdgeBlur, int newH, int newV)
{
	if (filterNumber >= 0 && filterNumber < MAX_DISPLAYED_FILTERS)
	{
		FilterFrame[filterNumber]->AF_SetValues(newMaxSize,  newMinSize, newDustSize,
															 newContrast, newConfid,
															 newEdgeBlur, newH, newV);
	}
}
//---------------------------------------------------------------------------

void TFilterBankFrame::SHINE_SetValues(int filterNumber,
											int newMaxSize,  int newMinSize,
											int newContrast, int newAggression)
{
	if (filterNumber >= 0 && filterNumber < MAX_DISPLAYED_FILTERS)
	{
		FilterFrame[filterNumber]->SHINE_SetValues(newMaxSize,  newMinSize,
														 newContrast, newAggression);
	}
}
//---------------------------------------------------------------------------

bool TFilterBankFrame::IsEnabled(int filterNumber)
{
   if (filterNumber < 0 || filterNumber >= MAX_DISPLAYED_FILTERS)
      return false;

   return FilterFrame[filterNumber]->IsEnabled();
}
//---------------------------------------------------------------------------

bool TFilterBankFrame::IsSelected(int filterNumber)
{
      return FilterFrame[filterNumber]->IsSelected();
}
//---------------------------------------------------------------------------

bool TFilterBankFrame::IsGanged(int filterNumber)
{
      return FilterFrame[filterNumber]->IsGanged();
}
//---------------------------------------------------------------------------

bool TFilterBankFrame::IsUsed(int filterNumber)
{
   if (filterNumber < 0 || filterNumber >= MAX_DISPLAYED_FILTERS)
      return false;

   return FilterFrame[filterNumber]->IsUsed();
}
//---------------------------------------------------------------------------

void TFilterBankFrame::FilterWasClicked(void *Sender)
{
   TBasicFilterFrame *filterFrame = reinterpret_cast<TBasicFilterFrame *>(Sender);
   int filterNumber = filterFrame->Tag;

   // Simple click always selects the clicked filter and ungangs the rest
   UngangAll();
   Select(filterNumber);

   SelectionChange.Notify();
}
//---------------------------------------------------------------------------

void TFilterBankFrame::FilterWasShiftClicked(void *Sender)
{
   int selectedFilterNumber = GetSelectedFilterNumber();

   // If nothing is presently selected, this acts like a regular click
   if (selectedFilterNumber == INVALID_FILTER_NUMBER)
   {
      FilterWasClicked(Sender);
   }
   else
   {
      // We want to gang all filters from the clicked one to the selected
      // one, inclusive
      TBasicFilterFrame *filterFrame = reinterpret_cast<TBasicFilterFrame *>(Sender);
      int filterNumber = filterFrame->Tag;

      // First clear out current ganging
      UngangAll();

      if (selectedFilterNumber >= filterNumber)
      {
         for (int i = filterNumber; i < selectedFilterNumber; ++i)
            Gang(i);
      }
      else
      {
         for (int i = (selectedFilterNumber + 1); i <= filterNumber; ++i)
            Gang(i);
      }

      GangChange.Notify();
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::FilterWasCtrlClicked(void *Sender)
{
   TBasicFilterFrame *filterFrame = reinterpret_cast<TBasicFilterFrame *>(Sender);
   int filterNumber = filterFrame->Tag;

   if (IsSelected(filterNumber))
   {
      // When we unselect, we also ungang all filters
      // Per order of Dave 5/18/2009 we no longer ungang filters - we
      // just designate a random other one as the main selectee
      //UngangAll();
      Unselect(filterNumber);
      Ungang(filterNumber);
      // now we have to find another one to select
      for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
      {
         if (FilterFrame[i]->IsEnabled() && FilterFrame[i]->IsGanged())
         {
            Select(i);
            break;
         }
      }

      SelectionChange.Notify();
   }
   else if (IsGanged(filterNumber))
   {
      // If ganged - toggle gang off
      Ungang(filterNumber);
      GangChange.Notify();
   }
   else if (GetSelectedFilterNumber() != INVALID_FILTER_NUMBER)
   {
      // If another filter is selected, just toggle gang on
      Gang(filterNumber);
      GangChange.Notify();
   }
   else // Nothing is presently selected
   {
      Select(filterNumber);
      SelectionChange.Notify();
   }
}
//---------------------------------------------------------------------------

void TFilterBankFrame::UseCheckBoxChanged(void *Sender)
{
   FilterUseChange.Notify();
}
//---------------------------------------------------------------------------


