//---------------------------------------------------------------------------

#ifndef ShineProcH
#define ShineProcH
//---------------------------------------------------------------------------

#include "AlgorithmAF.h"
#include "IniFile.h"
#include "Ippheaders.h"
#include "RegionOfInterest.h"
#include "ShineToolParameters.h"
#include "ToolObject.h"
//---------------------------------------------------------------------------

class ShineProc : public CToolProcessor
{
public:
	ShineProc(
      int newToolNumber,
      const string &newToolName,
      CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr,
      IToolProgressMonitor *newToolProgressMonitor,
      Ipp8uArray &modifiedPixelMask);

	virtual ~ShineProc();

	void  *_mertusCrap;
	int SetIOConfig(CToolIOConfig *toolIOConfig);
	int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
	// Tool's functions called from within context of the processing loop.
	int SetParameters(CToolParameters *toolParams);
	int BeginProcessing(SToolProcessingData &procData);
	int EndProcessing(SToolProcessingData &procData);
	int BeginIteration(SToolProcessingData &procData);
	int EndIteration(SToolProcessingData &procData);
	int DoProcess(SToolProcessingData &procData);
	int GetIterationCount(SToolProcessingData &procData);

   Ipp8uArray CreateAlphaDetectionMap(
      int frameIndex,
      MTI_UINT16 *frameBits);

private:
	int _firstInputFrameIndex = -1;
	int _lastInputFrameIndex = -1;
   int _sceneInFrameIndex = -1;
   int _sceneOutFrameIndex = -1;
   int _oldSceneInFrameIndex = -1;
   int _oldSceneOutFrameIndex = -1;
   int _scenePreprocOutputListIndex = -1;

   int _firstValidInputFrameIndexPosition = -1;
   int _countOfValidInputFrameIndexes = 0;
   int _repairFrameIndex = -1;

   void *_cheshireShineRepairMask = nullptr;
   ShineToolSettings _shineSettings;
   int _oldShinePreprocOutputGeneration = -1;

   MtiSize _imageSize;
   int _bitsPerPixelComponent = 0;

	int _framesRemaining = 0;        // Number of remaining output frames to be processed

   int _inputFrameIndexList[9];

	CRegionOfInterest maskROI;
   const MTI_UINT8 *_roiBlendPtr = nullptr;

   Ipp8uArray _modifiedPixelMask;

   CAlgorithmAF _algorithmAF;
};

#endif
