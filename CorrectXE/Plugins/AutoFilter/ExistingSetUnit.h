//---------------------------------------------------------------------------

#ifndef ExistingSetUnitH
#define ExistingSetUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <vector>
using std::vector;
#include <string>
using std::string;

//---------------------------------------------------------------------------
class TExistingSetForm : public TForm
{
__published:	// IDE-managed Components
        TComboBox *FilterBankSelectionComboBox;
        TLabel *PleaseSelectLabel;
        TButton *OkButton;
        TButton *CancelButton;

private:	// User declarations

public:		// User declarations
        __fastcall TExistingSetForm(TComponent* Owner);

        void SetOpName(const string &newOpName);
        void SetListOfFilterSetNames(const vector<string> &newListOfNames);
        void SetVisibleIndex(int newVisibleIndex);
        int GetSelectedIndex();
};
//---------------------------------------------------------------------------
extern PACKAGE TExistingSetForm *ExistingSetForm;
//---------------------------------------------------------------------------
#endif
