//---------------------------------------------------------------------------

#pragma hdrstop

#include "ShineMultiProc.h"
#include "err_auto_filter.h"

#include "AutoTrackingPreprocessor.h"
#include "Clip3.h"
#include "ImageFormat3.h"
#include "ShineRepairMask.h"
#include "ToolProgressMonitor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

ShineMultiProc::ShineMultiProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
		const bool *newEmergencyStopFlagPtr,
		IToolProgressMonitor *newToolProgressMonitor)
	: CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   _cheshireShineRepairMask = new ShineRepairMask;
	StartProcessThread(); // Required by Tool Infrastructure
}
// ---------------------------------------------------------------------------

ShineMultiProc::~ShineMultiProc()
{
   auto srm = reinterpret_cast<ShineRepairMask *>(_cheshireShineRepairMask);
   delete srm;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // maximum number of input and output frames
   int maxInputFrames = 9; // Processed frame + 4 on either side
   int maxOutputFrames = 1;
   SetInputMaxFrames(0, maxInputFrames, maxOutputFrames); // Input Port 0
   SetOutputNewFrame(0, 0); // Output to new frame with same image format as the input
   SetOutputMaxFrames(0, maxOutputFrames); // Output Port 0

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];

   // Gaaa, here inframe index means "first frame to process" on the LHS and
	// "first input frame" on the RHS
   DBTRACE(frameRange.inFrameIndex);
   DBTRACE(frameRange.outFrameIndex);
   _inFrameIndex = frameRange.inFrameIndex; // + 4;
	_outFrameIndex = frameRange.outFrameIndex; // - 4;

	// Processing one frame in one iteration
   _framesRemaining = _outFrameIndex - _inFrameIndex;
	_framesAlreadyProcessed = 0;

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::SetParameters(CToolParameters *toolParams)
{
	ShineToolParameters *shineToolParams = static_cast<ShineToolParameters*>(toolParams);

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_shineToolSettings = shineToolParams->getShineToolSettingsRef();
   _afParams = shineToolParams->getAutoFilterParameterRef();

	// Set the image size
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
	_imageSize = {.height = (int) imageFormat->getLinesPerFrame(),
					  .width = (int) imageFormat->getPixelsPerLine()};
   DBTRACE(_imageSize);

	MTI_UINT16 componentValues[3];
	imageFormat->getComponentValueMax(componentValues);
	MTI_UINT16 maxPixel = max<MTI_UINT16>(componentValues[0], max<MTI_UINT16>(componentValues[1], componentValues[2]));
   _bitsPerPixelComponent = 0;
   for (auto mask = 1; mask < maxPixel; mask <<= 1)
   {
      ++_bitsPerPixelComponent;
   }

   DBTRACE(_bitsPerPixelComponent);

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::GetIterationCount(SToolProcessingData &procData)
{
	return _outFrameIndex - _inFrameIndex;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::BeginProcessing(SToolProcessingData &procData)
{
   TRACE_0(errout << "Begin processing");
	GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
   GetToolProgressMonitor()->SetStatusMessage("Processing");
	GetToolProgressMonitor()->StartProgress();

	CVideoFrameList *frameList = GetSystemAPI()->getVideoFrameList();
   _shotInIndex = max<int>(GetSystemAPI()->getMarkIn(), frameList->getInFrameIndex());
   _shotOutIndex = min<int>(((GetSystemAPI()->getMarkOut() < 0) ? 0x7FFFFFFF : GetSystemAPI()->getMarkOut()),
                          ((frameList->getOutFrameIndex() < 0) ?  0x7FFFFFFF : frameList->getOutFrameIndex()));
   MTIassert(_shotInIndex >= 0);
   MTIassert(_shotOutIndex < 0x7FFFFFFF);
   MTIassert(_shotOutIndex - _shotInIndex >= 10);
   DBTRACE(_shotInIndex);
   DBTRACE(_shotOutIndex);

   if (_shotOutIndex - _shotInIndex < 10)
   {
      return -1;
   }

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::BeginIteration(SToolProcessingData &procData)
{
   TRACE_0(errout << "Begin iteration " << _framesAlreadyProcessed);
	int outputFrameIndexList[1];
   int indexOfFrameToProcess = _inFrameIndex + _framesAlreadyProcessed;
   MTIassert(indexOfFrameToProcess >= _shotInIndex);
   MTIassert(indexOfFrameToProcess < _shotOutIndex);

   if (indexOfFrameToProcess < _shotInIndex)
   {
      return -2;
   }

   if (indexOfFrameToProcess < _shotInIndex)
   {
      return -3;
   }

	// Tell the Tool Infrastructure which frames we want as input
   // during this iteration.
   int _firstValidInputFrameIndexPosition = -1;
   int _countOfValidInputFrameIndexes = 0;
   for (auto i = 0; i <= 9; ++i)
   {
      _inputFrameIndexList[i] = indexOfFrameToProcess - 4 + i;
      if (_inputFrameIndexList[i] >= _shotInIndex && _inputFrameIndexList[i] < _shotOutIndex)
      {
         if (_firstValidInputFrameIndexPosition == -1)
         {
            _firstValidInputFrameIndexPosition = i;
         }

         ++_countOfValidInputFrameIndexes;
      }
      else
      {
         _inputFrameIndexList[i] = -1;
      }
   }

   DBTRACE(_inputFrameIndexList[0]);
   DBTRACE(_inputFrameIndexList[1]);
   DBTRACE(_inputFrameIndexList[2]);
   DBTRACE(_inputFrameIndexList[3]);
   DBTRACE(_inputFrameIndexList[4]);
   DBTRACE(_inputFrameIndexList[5]);
   DBTRACE(_inputFrameIndexList[6]);
   DBTRACE(_inputFrameIndexList[7]);
   DBTRACE(_inputFrameIndexList[8]);

	SetInputFrames(0, _countOfValidInputFrameIndexes, &_inputFrameIndexList[_firstValidInputFrameIndexPosition]);

	// When finished processing, release the frame in slot 0, but only if
   // if it's vslid. If this is the last iteration, release all the frames.
   if (_inputFrameIndexList[0] >= 0)
   {
      SetFramesToRelease(0, 1, &_inputFrameIndexList[0]);
   }

   if (indexOfFrameToProcess == _outFrameIndex - 1)
   {
      SetFramesToRelease(0, _countOfValidInputFrameIndexes, &_inputFrameIndexList[_firstValidInputFrameIndexPosition]);
   }

   // Set output frame ( same as processed frame )
	outputFrameIndexList[0] = indexOfFrameToProcess;
   SetOutputFrames(0, 1, outputFrameIndexList);

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::DoProcess(SToolProcessingData &procData)
{
   TRACE_0(errout << "DoProcess");
	int retVal = 0;
   string errorFunc("<UNKNOWN>");

	try
   {
		MTI_UINT16 *rawOutputBuffer;
      CToolFrameBuffer *inToolFrameBuffer, *outToolFrameBuffer;

      // Copy the original image to the output frame.
      // Note: this may have to wait to allocate memory.
		const int InputSetIndexOfFrameToFix = 4;
		inToolFrameBuffer = GetRequestedInputFrame(0, InputSetIndexOfFrameToFix);
		outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
      CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBuffer, outToolFrameBuffer);

		// Tell the algorithm about the input frame buffer pointers
		// and relative indices
      for (int i = 0; i < 9; ++i)
      {
         DBTRACE(i);
         if (_inputFrameIndexList[i] == -1)
         {
            continue;
         }

         DBTRACE(_inputFrameIndexList[i]);
         inToolFrameBuffer = GetRequestedInputFrame(0, i);
         if (inToolFrameBuffer == nullptr)
         {
            TRACE_0(errout << " GetRequestedInputFrame(0, " << i << ") FAIL!");
            continue;
         }

         DBCOMMENT("Ipp16uArray");
         Ipp16uArray wrappedBuffer(_imageSize, inToolFrameBuffer->GetVisibleFrameBufferPtr());
         if (inToolFrameBuffer == nullptr)
         {
            TRACE_0(errout << " GetRequestedInputFrame(0, " << i << ")->GetVisibleFrameBufferPtr() FAIL!");
            continue;
         }

         DBTRACE(_bitsPerPixelComponent);  // must set qqq
         int PUSH_FRAME_INDEX = _inputFrameIndexList[i];
         DBTRACE(PUSH_FRAME_INDEX);
      }

      auto shineRepairMask = reinterpret_cast<ShineRepairMask *>(_cheshireShineRepairMask);


      // Get filter parameters and pass them to Mertus.
      DBTRACE(_afParams.faShineAggression[0]);
      DBTRACE(_afParams.faShineContrast[0]);
      float fidelitySetting = _afParams.faShineAggression[0];  // 0.0 to 1.0
      float contrastSetting = _afParams.faShineContrast[0] * 100.F;
      DBTRACE(fidelitySetting);
      DBTRACE(contrastSetting);

      // Get optical flow data and pass it to Mertus.
	   auto opticalFlowPtr = static_cast<OpticalFlowData *>((*_shineToolSettings.shinePreprocOutput)->data);
      DBTRACE(opticalFlowPtr->size());

      // NEED TO DO MORE STUFF HERE!
//      // Give the algorithm the pointer to the output image
//      uspDstArg = outToolFrameBuffer->GetVisibleFrameBufferPtr();
//
//		// Give the algorithm the pointer to the output image
//		AlgorithmAF->SetDstImgPtr(uspDstArg);
//
//		// Now setup the pixel region list that will receive the original
//		// values of pixels changed by the algorithm
//		//
//		AlgorithmAF->SetPixelRegionList(outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr());
//
//		// Now setup the "modified pixel map" that will record which pixels were
//		// changed by the algorithm
//		//
//		AlgorithmAF->SetModifiedPixelMap(_modifiedPixelMap);
//
//		// Finally, render the output frame
//		retVal = AlgorithmAF->Process(*_cachedGrainFilters, _cachedBlobPixelArrayWrapper);
//		if (retVal != 0)
//		{
//			throw(errorFunc = "AutofilterProcess");
//		}

      // Get the repaired frame
      int REPAIRED_FRAME_INDEX = _inputFrameIndexList[4];
      DBTRACE(REPAIRED_FRAME_INDEX);

		return 0; // SUCCESS
	}
	catch (string)
	{; // do nothing here
	}
	catch (std::bad_alloc)
	{
		retVal = ERR_PLUGIN_AF_MALLOC;
	}
	catch (...)
	{
		retVal = 666; // meaningless
	}
	try
	{
		CAutoErrorReporter autoErr("CAutoFilterSingleProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		string errorMsg;
		switch (retVal)
		{
		case ERR_PLUGIN_AF_NO_FILTERS:
			errorMsg = "No valid filters found in active filter set";
			break;
		case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
			errorMsg = "No alpha channel - cannot find defect map";
			break;
		case ERR_PLUGIN_AF_MALLOC:
			errorMsg = "Out of memory";
			break;
		default:
			errorMsg = "Internal Error";
			break;
		}

		autoErr.errorCode = retVal;
		autoErr.msg << errorMsg << endl << " in function " << errorFunc;
	}
	catch (...)
	{
		TRACE_0(errout << "ShineSingleProc::DoProcess: FATAL ERROR, code " << retVal << endl <<
				"CAUGHT EXCEPTION when trying to display error dialog!");
	}

	return retVal; // ERROR
	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::EndIteration(SToolProcessingData &procData)
{
	_framesRemaining -= 1;
	_framesAlreadyProcessed += 1;

	GetToolProgressMonitor()->BumpProgress(1);

	return 0;
}
// ---------------------------------------------------------------------------

int ShineMultiProc::EndProcessing(SToolProcessingData &procData)
{
	if (_framesRemaining == 0)
	{
		GetToolProgressMonitor()->SetStatusMessage("Processing complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	return 0;
}
// ---------------------------------------------------------------------------

