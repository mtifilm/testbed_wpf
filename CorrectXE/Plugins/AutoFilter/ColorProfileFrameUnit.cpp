//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ColorProfileFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TrackEditFrameUnit"
#pragma resource "*.dfm"
//TColorProfileFrame *ColorProfileFrame;
//---------------------------------------------------------------------------

#define SLIDER_MIN 0
#define SLIDER_MAX 255
#define DEFAULT_MED (((SLIDER_MAX-SLIDER_MIN)+SLIDER_MIN + 1)/2)
//---------------------------------------------------------------------------

__fastcall TColorProfileFrame::TColorProfileFrame(TComponent* Owner)
    : TFrame(Owner), CapturedMouse(false)
{
   EnabledFlag = false;  // to force Enable to do something
   Enable(true);
   SetFullRange();
   StashValues();
   Enable(false);
   SET_CBHOOK(ColorValueChanged, RMinTrackEdit->TrackEditValueChange);
   SET_CBHOOK(ColorValueChanged, RMaxTrackEdit->TrackEditValueChange);
   SET_CBHOOK(ColorValueChanged, GMinTrackEdit->TrackEditValueChange);
   SET_CBHOOK(ColorValueChanged, GMaxTrackEdit->TrackEditValueChange);
   SET_CBHOOK(ColorValueChanged, BMinTrackEdit->TrackEditValueChange);
   SET_CBHOOK(ColorValueChanged, BMaxTrackEdit->TrackEditValueChange);
}
//---------------------------------------------------------------------------

void TColorProfileFrame::Enable(bool flag)
{
   if (EnabledFlag == flag)
      return;    // Already in correct state
   EnabledFlag = flag;

   // Ugliness to retain "stashed" user values to "unstash" them when
   // we switch back to user-defined mode, so the values don't get lost
   // by switching to full mode then back
   if (flag)
   {
      UnstashValues();
   }
   else
   {
      StashValues();
      SetFullRange();
      CancelEyedropperMode();
   }

   RMinTrackEdit->Enable(flag);
   RMedLabelPanel->Enabled = flag;
   RMaxTrackEdit->Enable(flag);
   RLabel->Enabled = flag;
   GMinTrackEdit->Enable(flag);
   GMedLabelPanel->Enabled = flag;
   GMaxTrackEdit->Enable(flag);
   GLabel->Enabled = flag;
   BMinTrackEdit->Enable(flag);
   BMedLabelPanel->Enabled = flag;
   BMaxTrackEdit->Enable(flag);
   BLabel->Enabled = flag;

   MinColorPatchPanel->Visible = flag;
   MedColorPatchPanel->Visible = flag;
   MaxColorPatchPanel->Visible = flag;

   MinLabel->Enabled = flag;
   MaxLabel->Enabled = flag;
   EyedropperPanel->Enabled = true; //flag;    always enabled now!
   DisabledMinBackPanel->Visible = !flag;
   DisabledMedBackPanel->Visible = !flag;
   DisabledMaxBackPanel->Visible = !flag;

   MinColorPatchPanel->Visible = flag;
   MedColorPatchPanel->Visible = flag;
   MaxColorPatchPanel->Visible = flag;

   UpdateColorPatches();

}
//---------------------------------------------------------------------------
void TColorProfileFrame::ColorValueChanged(void *crap)
{
   ChangeNotifyWidget->Checked = !ChangeNotifyWidget->Checked;
   UpdateColorPatches();
   ColorProfileChange.Notify();
}
//---------------------------------------------------------------------------

void TColorProfileFrame::GetValues(int &minR, int &medR, int &maxR,
                                   int &minG, int &medG, int &maxG,
                                   int &minB, int &medB, int &maxB)
{
   minR = RMinTrackEdit->GetValue();
   medR = RMedLabel->Caption.ToInt();
   maxR = RMaxTrackEdit->GetValue();
   minG = GMinTrackEdit->GetValue();
   medG = GMedLabel->Caption.ToInt();
   maxG = GMaxTrackEdit->GetValue();
   minB = BMinTrackEdit->GetValue();
   medB = BMedLabel->Caption.ToInt();
   maxB = BMaxTrackEdit->GetValue();
}
//---------------------------------------------------------------------------

void TColorProfileFrame::SetValues(int minR, int medR, int maxR,
                                   int minG, int medG, int maxG,
                                   int minB, int medB, int maxB)
{
   RMinTrackEdit->SetMinAndMax(SLIDER_MIN, medR);
   RMaxTrackEdit->SetMinAndMax(medR, SLIDER_MAX);
   GMinTrackEdit->SetMinAndMax(SLIDER_MIN, medG);
   GMaxTrackEdit->SetMinAndMax(medG, SLIDER_MAX);
   BMinTrackEdit->SetMinAndMax(SLIDER_MIN, medB);
   BMaxTrackEdit->SetMinAndMax(medB, SLIDER_MAX);
   RMinTrackEdit->SetValue(minR);

   RMedLabel->Caption = AnsiString(medR);
   RMaxTrackEdit->SetValue(maxR);
   GMinTrackEdit->SetValue(minG);
   GMedLabel->Caption = AnsiString(medG);
   GMaxTrackEdit->SetValue(maxG);
   BMinTrackEdit->SetValue(minB);
   BMedLabel->Caption = AnsiString(medB);
   BMaxTrackEdit->SetValue(maxB);

   if (medR == 128 && medG == 128 && medB == 128)
   {
      RMedLabel->Enabled = false;
      GMedLabel->Enabled = false;
      BMedLabel->Enabled = false;
   }
   else
   {
      RMedLabel->Enabled = true;
      GMedLabel->Enabled = true;
      BMedLabel->Enabled = true;
   }

   UpdateColorPatches();
}
//---------------------------------------------------------------------------

void TColorProfileFrame::UpdateColorPatches()
{
   int minR, medR, maxR, minG, medG, maxG, minB, medB, maxB;
   GetValues(minR, medR, maxR, minG, medG, maxG, minB, medB, maxB);
   MinColorPatchPanel->Color = TColor(RGB(minR, minG, minB));
   MedColorPatchPanel->Color = TColor(RGB(medR, medG, medB));
   MaxColorPatchPanel->Color = TColor(RGB(maxR, maxG, maxB));
}
//---------------------------------------------------------------------------

bool TColorProfileFrame::IsFullRange()
{
   bool retVal = false;
   if (
     RMinTrackEdit->TrackBar->Position == RMinTrackEdit->TrackBar->Min &&
     RMaxTrackEdit->TrackBar->Position == RMaxTrackEdit->TrackBar->Max &&
     GMinTrackEdit->TrackBar->Position == GMinTrackEdit->TrackBar->Min &&
     GMaxTrackEdit->TrackBar->Position == GMaxTrackEdit->TrackBar->Max &&
     BMinTrackEdit->TrackBar->Position == BMinTrackEdit->TrackBar->Min &&
     BMaxTrackEdit->TrackBar->Position == BMaxTrackEdit->TrackBar->Max
     )
   {
     retVal = true;
   }

   return retVal;
}
//---------------------------------------------------------------------------

void TColorProfileFrame::SetFullRange()
{
   SetValues(SLIDER_MIN, DEFAULT_MED, SLIDER_MAX,
             SLIDER_MIN, DEFAULT_MED, SLIDER_MAX,
             SLIDER_MIN, DEFAULT_MED, SLIDER_MAX);
}
//---------------------------------------------------------------------------

void TColorProfileFrame::StashValues()
{
   GetValues(StashedMinR, StashedMedR, StashedMaxR,
             StashedMinG, StashedMedG, StashedMaxG,
             StashedMinB, StashedMedB, StashedMaxB);
}
//---------------------------------------------------------------------------

void TColorProfileFrame::UnstashValues()
{
   SetValues(StashedMinR, StashedMedR, StashedMaxR,
             StashedMinG, StashedMedG, StashedMaxG,
             StashedMinB, StashedMedB, StashedMaxB);
}
//---------------------------------------------------------------------------

bool TColorProfileFrame::IsInEyedropperMode()
{
   return EyedropperButton->Down;
}
//---------------------------------------------------------------------------

void TColorProfileFrame::CancelEyedropperMode()
{
   if (EyedropperButton->Down)
   {
      // THIS DOESN'T MAKE ANY FRICKING SENSE!! You should either have
      // to change the "Down" state OR pretend to Click BUT NOT BOTH!!
      // But if you don't Click, the OnClick doesn't get called, and if
      // you down turn off 'Down' then the frickin' button STAYS DOWN!
      EyedropperButton->Down = false;  // Do this FIRST, else OnClick confusion
      EyedropperButton->Click();
   }
}
//---------------------------------------------------------------------------

void __fastcall TColorProfileFrame::EyedropperButtonClick(TObject *Sender)
{
   EyedropperStateChange.Notify();
}
//---------------------------------------------------------------------------

