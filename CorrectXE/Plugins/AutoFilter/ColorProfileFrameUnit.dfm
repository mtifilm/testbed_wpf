object ColorProfileFrame: TColorProfileFrame
  Left = 0
  Top = 0
  Width = 275
  Height = 138
  TabOrder = 0
  object BLabel: TLabel
    Left = 4
    Top = 94
    Width = 6
    Height = 13
    Caption = 'B'
    Enabled = False
  end
  object GLabel: TLabel
    Left = 4
    Top = 60
    Width = 7
    Height = 13
    Caption = 'G'
    Enabled = False
  end
  object RLabel: TLabel
    Left = 4
    Top = 24
    Width = 7
    Height = 13
    Caption = 'R'
    Enabled = False
  end
  inline RMinTrackEdit: TTrackEditFrame
    Left = 12
    Top = 8
    Width = 127
    Height = 51
    TabOrder = 0
    ExplicitLeft = 12
    ExplicitTop = 8
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 55
      Width = 3
      Caption = ''
      ExplicitLeft = 55
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Tag = -1
      Left = 38
      Width = 83
      Max = 128
      Position = 0
      ExplicitLeft = 38
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Left = 44
      Width = 71
      ExplicitLeft = 44
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 0
      Align = alLeft
      ExplicitLeft = 0
      inherited Edit: TEdit
        Left = 10
        Top = 13
        Width = 28
        Height = 21
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Left = 10
        Top = 13
        Width = 28
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  inline RMaxTrackEdit: TTrackEditFrame
    Left = 152
    Top = 8
    Width = 127
    Height = 51
    TabOrder = 1
    ExplicitLeft = 152
    ExplicitTop = 8
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 57
      Width = 3
      Caption = ''
      ExplicitLeft = 57
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Width = 83
      Max = 255
      Min = 128
      Position = 255
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Width = 71
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 87
      ExplicitLeft = 87
      inherited Edit: TEdit
        Top = 13
        Width = 28
        Height = 21
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Top = 13
        Width = 28
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          Caption = '255'
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  inline GMinTrackEdit: TTrackEditFrame
    Left = 12
    Top = 44
    Width = 127
    Height = 51
    TabOrder = 2
    ExplicitLeft = 12
    ExplicitTop = 44
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 55
      Width = 3
      Caption = ''
      ExplicitLeft = 55
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Tag = -1
      Left = 38
      Width = 83
      Max = 128
      Position = 0
      ExplicitLeft = 38
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Left = 44
      Width = 71
      ExplicitLeft = 44
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 0
      Align = alLeft
      ExplicitLeft = 0
      inherited Edit: TEdit
        Left = 10
        Top = 13
        Width = 28
        Height = 21
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Left = 10
        Top = 13
        Width = 28
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  inline GMaxTrackEdit: TTrackEditFrame
    Left = 152
    Top = 44
    Width = 127
    Height = 51
    TabOrder = 3
    ExplicitLeft = 152
    ExplicitTop = 44
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 57
      Width = 3
      Caption = ''
      ExplicitLeft = 57
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Width = 83
      Max = 255
      Min = 128
      Position = 255
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Width = 71
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 87
      ExplicitLeft = 87
      inherited Edit: TEdit
        Top = 13
        Width = 28
        Height = 21
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Top = 13
        Width = 28
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          Caption = '255'
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  inline BMinTrackEdit: TTrackEditFrame
    Left = 12
    Top = 78
    Width = 127
    Height = 51
    TabOrder = 4
    ExplicitLeft = 12
    ExplicitTop = 78
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 55
      Width = 3
      Caption = ''
      ExplicitLeft = 55
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Left = 6
      Width = 3
      Caption = ''
      ExplicitLeft = 6
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Tag = -1
      Left = 38
      Width = 83
      Max = 128
      Position = 0
      ExplicitLeft = 38
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Left = 44
      Width = 71
      ExplicitLeft = 44
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 0
      Align = alLeft
      ExplicitLeft = 0
      inherited Edit: TEdit
        Left = 10
        Top = 13
        Width = 28
        Height = 21
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Left = 10
        Top = 13
        Width = 28
        ExplicitLeft = 10
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  inline BMaxTrackEdit: TTrackEditFrame
    Left = 152
    Top = 78
    Width = 127
    Height = 51
    TabOrder = 5
    ExplicitLeft = 152
    ExplicitTop = 78
    ExplicitWidth = 127
    inherited MinLabel: TLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited MaxLabel: TLabel
      Left = 57
      Width = 3
      Caption = ''
      ExplicitLeft = 57
      ExplicitWidth = 3
    end
    inherited TitleLabel: TColorLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
    inherited TrackBar: TTrackBar
      Width = 83
      Max = 255
      Min = 128
      Position = 255
      ExplicitWidth = 83
    end
    inherited TrackBarDisabledPanel: TPanel
      Width = 71
      ExplicitWidth = 71
    end
    inherited EditAlignmentPanel: TPanel
      Left = 87
      ExplicitLeft = 87
      inherited Edit: TEdit
        Top = 13
        Width = 28
        Height = 21
        ExplicitTop = 13
        ExplicitWidth = 28
        ExplicitHeight = 21
      end
      inherited EditDisabledPanel: TPanel
        Top = 13
        Width = 28
        ExplicitTop = 13
        ExplicitWidth = 28
        inherited EditDisabledLabel: TLabel
          Left = 4
          Width = 23
          Alignment = taLeftJustify
          Caption = '255'
          ExplicitLeft = 4
          ExplicitWidth = 23
        end
      end
    end
  end
  object BMedLabelPanel: TPanel
    Left = 133
    Top = 96
    Width = 23
    Height = 14
    BevelOuter = bvNone
    TabOrder = 6
    object BMedLabel: TLabel
      Left = 0
      Top = 0
      Width = 20
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '128'
      Enabled = False
    end
  end
  object DisabledMaxBackPanel: TPanel
    Left = 239
    Top = 118
    Width = 27
    Height = 14
    BevelOuter = bvNone
    BorderWidth = 1
    Color = clBtnShadow
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 7
    object DiabledMaxFillerPanel: TPanel
      Left = 1
      Top = 1
      Width = 25
      Height = 12
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object DisabledMedBackPanel: TPanel
    Left = 131
    Top = 118
    Width = 27
    Height = 14
    BevelOuter = bvNone
    BorderWidth = 1
    Color = clBtnShadow
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 8
    object DiabledMedFillerPanel: TPanel
      Left = 1
      Top = 1
      Width = 25
      Height = 12
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object DisabledMinBackPanel: TPanel
    Left = 22
    Top = 118
    Width = 27
    Height = 14
    BevelOuter = bvNone
    BorderWidth = 1
    Color = clBtnShadow
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 9
    object DiabledMinFillerPanel: TPanel
      Left = 1
      Top = 1
      Width = 25
      Height = 12
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object GMedLabelPanel: TPanel
    Left = 133
    Top = 62
    Width = 23
    Height = 14
    BevelOuter = bvNone
    TabOrder = 10
    object GMedLabel: TLabel
      Left = 0
      Top = 0
      Width = 20
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '128'
      Enabled = False
    end
  end
  object RMedLabelPanel: TPanel
    Left = 133
    Top = 26
    Width = 23
    Height = 14
    BevelOuter = bvNone
    TabOrder = 11
    object RMedLabel: TLabel
      Left = 0
      Top = 0
      Width = 20
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '128'
      Enabled = False
    end
  end
  object EyedropperPanel: TPanel
    Left = 132
    Top = 0
    Width = 25
    Height = 25
    BevelOuter = bvNone
    TabOrder = 12
    object EyedropperButton: TSpeedButton
      Left = 1
      Top = 0
      Width = 23
      Height = 23
      Glyph.Data = {
        AE050000424DAE05000000000000320400002800000014000000130000000100
        0800000000007C010000C30E0000C30E0000FF000000FF000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00C3000000CF00
        0000DB000000E7000000F3000000FF000000FF171700FF2F2F00FF535300FF6B
        6700FF7F7F00FF8B8B00FF979700FFA3A300FFAFAF00FFBBBB00FFC7C700FFCF
        C700FFDBDB00FFE7E700FFF3F300FFFBF7002B2B530037375F0043436B004F4F
        770057577F0063638B006F6F97007F7FA7008B8BB3009797BF00A7A7CF00B3B3
        DB00BFBFE700C7C7EF00CFCFF700532B2B005F3737006B434300774F4F00835B
        5B008F6767009B737300A77F7F00B38B8B00BF979700CBA3A300D7AFAF00E3BB
        BB00EBC3C300FBD3D3002F532F003B5F3B00476B4700537753005F835F006B8F
        6B00779B770083A783008FB38F009BBF9B00A7CBA700B3D7B300BFE3BF00CBEF
        CB00D7FBD700876F9700977FA700A78FB700B39BC300C3ABD300CFB7DF00DBC3
        EB008B976F0093A37B009FAF8700ABBB9300B7C79F00CBDBB300D7E7BF00E3F3
        CB000B6F9B000F7BA3001387AF00178FB7001B9BC30017A7CF001BB3DB0023BF
        E7002BCBF30037D7FF00FFF3FF00FFEBFF00FFDFFF00FFD3FF00FFC7FF00FFB7
        FF00FFA3FF00FF97FF00FF83FF00FF6BFF00FF4BFF00E700E700D700D700C300
        C700B700B700A300A700970097008B008B0077007700670067004F0053002F00
        3300EBFFFF00E7FFFF00DFFFFF00D3FFFF00BBFFFF009BFFFF003FFFFF0000F3
        F70000E7EB0000DFDF0000D3D30000C7C70000BBBB0000B3AF0000A7A700009B
        970000978F00007F7F0000777700005F5F000047470000333300FFFFF700FFFF
        E700FFFFDB00FFFFC700FFFFBB00FFFF9700FFFF7F00FFFF5300EFEF0000E3E3
        0000D7D70000CBCB0000BFBF0000B3B30000A3A30000979300008B8300007B7B
        0000676B00005B5B0000474B000023230000F3FFF300DFFFE700D7FFD700C3FF
        CF00BBFFBB00A3FFA30087FF870067FF670037FF37000BFF000000F3000000EB
        000000E3000000D7000000CB000000BF000000B3000000A70000009F00000093
        000000870000007F000000770000006F000000670000005F0000005300000047
        00000037000000230000F7F3FF00EBEBFF00DFDFFF00D3D3FF00C3C3FF00AFAF
        FF009B9BFF008B8BFF007777FF006767FF005353FF004343FF002F2FFF001717
        FF000000470000005700000067000000730000007F0000008B00000097000000
        A3000000AF000000BB000000C3000000CF000000DB000000E7000000F3007C00
        54009B006900BA007E00D9009300F000AA00FF24B600FF48C200FF6CCE00FF90
        DA00FFB4E600F0F0F000DCDCDC00C8C8C800B4B4B400A0A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF00000F0F0F0F0F0F0F0F0F0F
        0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000460F0F0F0F0F0F0F0F0F0F0F0F0F0F0F
        0F0F003A4C3A0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F002C0F08290F0F0F0F0F
        0F0F0F0F0F0F0F0F0F0F46ADF40F472B0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F2A
        00CC0F08390F0F0F0F0F0F0F0F0F0F0F0F0F0F0F4535CCAE45280F0F0F0F0F0F
        0F0F0F0F0F0F0F0F0F46442322442A0F980F0F0F0F0F0F0F0F0F0F0F0F0F2997
        0FF3463727F40F0F0F0F0F0F0F0F0F0F0F0F0F38470F550000004F0F0F0F0F0F
        0F0F0F0F0F0F0F0F083800000045F60F0F0F0F0F0F0F0F0F0F0F0FF42B4744AD
        00360F0F0F0F0F0F0F0F0F0F0F0F0F4700070FF64400270F0F0F0F0F0F0F0F0F
        0F0F0F982ACB08AD5B2600360F0F0F0F0F0F0F0F0F0F0F0F0F47AD08285DAD00
        0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F82384508260F0F0F0F0F0F0F0F0F0F0F0F
        0F0F0F0F079700F70F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F
        0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F}
      OnClick = EyedropperButtonClick
    end
  end
  object MinLabelPanel: TPanel
    Left = 18
    Top = 0
    Width = 33
    Height = 17
    BevelOuter = bvNone
    TabOrder = 13
    object MinLabel: TLabel
      Left = 8
      Top = 0
      Width = 16
      Height = 13
      Caption = 'Min'
      Enabled = False
    end
  end
  object MaxLabelPanel: TPanel
    Left = 234
    Top = 0
    Width = 33
    Height = 17
    BevelOuter = bvNone
    TabOrder = 14
    object MaxLabel: TLabel
      Left = 8
      Top = 0
      Width = 20
      Height = 13
      Caption = 'Max'
      Enabled = False
    end
  end
  object ChangeNotifyWidget: TCheckBox
    Left = -50
    Top = 60
    Width = 20
    Height = 17
    TabStop = False
    TabOrder = 15
  end
  object MinColorPatchPanel: TPanel
    Left = 23
    Top = 119
    Width = 25
    Height = 12
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 16
    Visible = False
  end
  object MedColorPatchPanel: TPanel
    Left = 132
    Top = 119
    Width = 25
    Height = 12
    BevelOuter = bvNone
    Color = clGray
    TabOrder = 17
    Visible = False
  end
  object MaxColorPatchPanel: TPanel
    Left = 240
    Top = 119
    Width = 25
    Height = 12
    BevelOuter = bvNone
    Color = clBlack
    TabOrder = 18
    Visible = False
  end
end
