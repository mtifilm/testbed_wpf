object FilterStatsForm: TFilterStatsForm
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 386
  ClientWidth = 375
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ControlPanel: TPanel
    Left = 0
    Top = 359
    Width = 375
    Height = 27
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 559
    ExplicitWidth = 519
    DesignSize = (
      375
      27)
    object FilterLabel: TLabel
      Left = 12
      Top = 6
      Width = 24
      Height = 13
      Caption = 'Filter'
    end
    object Filter1Button: TSpeedButton
      Left = 42
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Down = True
      Caption = '1'
      OnClick = FilterButtonClick
    end
    object Filter2Button: TSpeedButton
      Tag = 1
      Left = 67
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Caption = '2'
      OnClick = FilterButtonClick
    end
    object Filter3Button: TSpeedButton
      Tag = 2
      Left = 92
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Caption = '3'
      OnClick = FilterButtonClick
    end
    object Filter4Button: TSpeedButton
      Tag = 3
      Left = 117
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Caption = '4'
      OnClick = FilterButtonClick
    end
    object Filter5Button: TSpeedButton
      Tag = 4
      Left = 142
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Caption = '5'
      OnClick = FilterButtonClick
    end
    object Filter6Button: TSpeedButton
      Tag = 5
      Left = 167
      Top = 4
      Width = 19
      Height = 18
      GroupIndex = 1
      Caption = '6'
      OnClick = FilterButtonClick
    end
    object CloseButton: TButton
      Left = 323
      Top = 4
      Width = 45
      Height = 19
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
      ExplicitLeft = 467
    end
  end
  object TextBox: TRichEdit
    Left = 0
    Top = 0
    Width = 375
    Height = 359
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    PlainText = True
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
    Zoom = 100
    ExplicitWidth = 519
    ExplicitHeight = 559
  end
end
