//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BasicFilterFrameUnit.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include <sstream>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TBasicFilterFrame *BasicFilterFrame;
//---------------------------------------------------------------------------

#define SELECT_SHRINKAGE 1
#define GANG_SHRINKAGE 1
#define FULL_WIDTH 487
#define FULL_HEIGHT 24
//---------------------------------------------------------------------------

__fastcall TBasicFilterFrame::TBasicFilterFrame(TComponent* Owner)
    : TFrame(Owner)
{
   Enable(false);
   SetState(FFS_UNSELECTED);
   Tag = -1;
   CheckedStateMayBeWrong = false;
   UseCheckedState = UseCheckBox->Checked;
}

//---------------------------------------------------------------------------

void TBasicFilterFrame::Enable(bool flag)
{
  DisabledPanel->Visible = !flag;
}

//---------------------------------------------------------------------------

void TBasicFilterFrame::SetState(EFilterFrameState newState)
{
   TFontStyles readoutStyle;

   state = newState;

   switch (newState)
   {
      case FFS_SELECTED:
      case FFS_GANGED:

			AF_ValuesPanel->Left = -GANG_SHRINKAGE;
			AF_ValuesPanel->Top = -GANG_SHRINKAGE;
			SHINE_ValuesPanel->Left = -GANG_SHRINKAGE;
			SHINE_ValuesPanel->Top = -GANG_SHRINKAGE;
			UseAndFilterNamePanel->Left = -GANG_SHRINKAGE;
			UseAndFilterNamePanel->Top = -GANG_SHRINKAGE;
			BackPanel->Left = GANG_SHRINKAGE;
         BackPanel->Top = GANG_SHRINKAGE;
         BackPanel->Width = FULL_WIDTH - (2 * GANG_SHRINKAGE) - 5; // NO EFFN IDEA!
         BackPanel->Height= FULL_HEIGHT - (2 * GANG_SHRINKAGE);
			readoutStyle << fsBold;
         break;

      case FFS_UNSELECTED:
      default:

         AF_ValuesPanel->Left = 0;
         AF_ValuesPanel->Top = 0;
			SHINE_ValuesPanel->Left = 0;
			SHINE_ValuesPanel->Top = 0;
			UseAndFilterNamePanel->Left = 0;
			UseAndFilterNamePanel->Top = 0;
			BackPanel->Left = 0;
         BackPanel->Top = 0;
         BackPanel->Width = FULL_WIDTH;
         BackPanel->Height= FULL_HEIGHT;
			break;
   }

	FilterNameLabel->Font->Style = readoutStyle;
	AF_FixToleranceValue->Font->Style = readoutStyle;
   AF_ContrastOrGrainValue->Font->Style = readoutStyle;
   AF_EdgeBlurValue->Font->Style = readoutStyle;
	AF_MotionValue->Font->Style = readoutStyle;
	AF_MaxMinDustValues->Font->Style = readoutStyle;
	SHINE_MaxMinValues->Font->Style = readoutStyle;
	SHINE_ContrastValue->Font->Style = readoutStyle;
	SHINE_AggressionValue->Font->Style = readoutStyle;
}
//---------------------------------------------------------------------------

EFilterFrameState TBasicFilterFrame::GetState()
{
    return state;
}
//---------------------------------------------------------------------------

void TBasicFilterFrame::Use(bool flag)
{
   // CAREFUL! When we change the value of the checkbox programatically,
   // sometimes when you look at UseCheckBox->Checked in the onClick
   // handler, it's the OLD value, sometimes the NEW value!!  WTF!
   CheckedStateMayBeWrong = true;
   UseCheckedState = flag;

   UseCheckBox->Checked = flag;

   CheckedStateMayBeWrong = false;
}
//---------------------------------------------------------------------------

void TBasicFilterFrame::SetFilterName(const string &newName)
{
	FilterNameLabel->Caption = newName.c_str();
}
//---------------------------------------------------------------------------

void TBasicFilterFrame::AF_SetValues(int newMaxSize, int newMinSize, int newDustSize,
                                  int newContrast, int newConfid,
                                  int newEdgeBlur, int newH, int newV)
{
	MTIostringstream os;
	string newDustSizeString = "-";
	if (newDustSize >= 0)
	{
		os << newDustSize;
		newDustSizeString = os.str();
	}

	os.str("");
	os << newMaxSize << " / " << newMinSize << " / " << newDustSizeString;
	AF_MaxMinDustValues->Caption = os.str().c_str();

	os.str("");
   int newMotion = std::max<int>(newH, newV);
	os << newMotion; // newH << " / " << newV;  // xyzzy
	AF_MotionValue->Caption = os.str().c_str();

	AF_ContrastOrGrainValue->Caption = newContrast;
	AF_FixToleranceValue->Caption = newConfid;
	AF_EdgeBlurValue->Caption = newEdgeBlur;

	UseAndFilterNamePanel->Width = AF_MaxMinDustValues->Left;
	AF_ValuesPanel->Visible = true;
	SHINE_ValuesPanel->Visible = false;
}
//---------------------------------------------------------------------------

void TBasicFilterFrame::SHINE_SetValues(int newMaxSize, int newMinSize, int newContrast, int newAggression)
{
	MTIostringstream os;
	os << newMaxSize << " / " << newMinSize;
	SHINE_MaxMinValues->Caption = os.str().c_str();

	SHINE_ContrastValue->Caption = newContrast;
	SHINE_AggressionValue->Caption = newAggression;

	UseAndFilterNamePanel->Width = SHINE_MaxMinValues->Left;
	SHINE_ValuesPanel->Visible = true;
	AF_ValuesPanel->Visible = false;
}
//---------------------------------------------------------------------------

bool TBasicFilterFrame::IsEnabled()
{
	return !DisabledPanel->Visible;
}
//---------------------------------------------------------------------------

bool TBasicFilterFrame::IsSelected()
{
   return (IsEnabled() && (state == FFS_SELECTED));
}
//---------------------------------------------------------------------------

bool TBasicFilterFrame::IsGanged()
{
   return (IsEnabled() && (state == FFS_SELECTED || state == FFS_GANGED));
}
//---------------------------------------------------------------------------

bool TBasicFilterFrame::IsUsed()
{
   return (IsEnabled() && UseCheckedState);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TBasicFilterFrame::UseCheckBoxClick(TObject *Sender)
{
   // CAREFUL! If we changed the value of the checkbox programatically,
   // sometimes UseCheckBox->Checked returns the wrong value!!  (But it's
   // always right if the user clicked on it, i.e. it reflects the state
   // AFTER the click!)
   
   if (!CheckedStateMayBeWrong)
   {
      UseCheckedState = UseCheckBox->Checked;
   }

   UseCheckBoxChange.Notify();
}
//---------------------------------------------------------------------------

void __fastcall TBasicFilterFrame::FilterClick(TObject *Sender)
{
// MouoseDown event stopped working for labels - here was its code:
//	if (Shift.Contains(ssShift))
//		FilterShiftClicked.Notify();
//	else if (Shift.Contains(ssCtrl))
//		FilterCtrlClicked.Notify();
//	else
//		FilterClicked.Notify();

	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
	{
		FilterShiftClicked.Notify();
	}
	else if (HIWORD(GetKeyState(VK_CONTROL)) != 0)
	{
		FilterCtrlClicked.Notify();
	}
	else
	{
		FilterClicked.Notify();
	}
}
//---------------------------------------------------------------------------

