//---------------------------------------------------------------------------

#ifndef NewFilterFormUnitH
#define NewFilterFormUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;
//---------------------------------------------------------------------------

class TNewFilterForm : public TForm
{
__published:	// IDE-managed Components
    TLabel *PleaseEnterlabel;
    TEdit *Edit;
    TButton *OkButton;
    TButton *CancelButton;
    void __fastcall EditEnter(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TNewFilterForm(TComponent* Owner);

    void SetDefaultName(const string &newName);
    string GetNewFilterName();
};
//---------------------------------------------------------------------------
extern PACKAGE TNewFilterForm *NewFilterForm;
//---------------------------------------------------------------------------
#endif
