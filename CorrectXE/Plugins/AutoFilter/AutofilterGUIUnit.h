// ---------------------------------------------------------------------------

#ifndef AutofilterGUIUnitH
#define AutofilterGUIUnitH

#include "AutoFilterTool.h"
#include "IniFile.h"
#include "PresetParameterModel.h"
#include "PropX.h"
// ---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "MTIUNIT.h"
#include "FilterBankFrameUnit.h"
#include "TrackEditFrameUnit.h"
#include "ColorProfileFrameUnit.h"
#include "ExecButtonsFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include <Vcl.ToolWin.hpp>
#include "VDoubleEdit.h"
#include "ColorPanel.h"
#include "PresetsUnit.h"
#include "ColorLabel.h"

#include <map>
#include <string>
#include <vector>
using std::map;
using std::string;
using std::vector;
// ---------------------------------------------------------------------------

// These are used for selectively ganging parameters
enum EGangedParameter
{
	GangParam_FilterName = 0,
	GangParam_MaxSize,
	GangParam_MinSize,
	GangParam_MaxDustSize,
	GangParam_ContrastSensitivity,
	GangParam_LuminanceThreshold,
	GangParam_FixTolerance,
	GangParam_Motion,
	GangParam_EdgeBlur,
	GangParam_ShineContrast,
	GangParam_ShineMotionTolerance,
	GangParam_DebrisContrast,
	GangParam_DetectChannel,
	GangParam_ProcessChannels,
	GangParam_UseHardMotionForDust,
	GangParam_UsePrecomputedMotions,
	GangParam_UseAlphaAsMask,
	GangParam_ColorProfile,
	GangParam_UseGrainFilter,
	GangParam_GrainFilterParam,
};

enum DebrisToolOpMode
{
   INVALID,
	AF,
	SHINE
};
// ---------------------------------------------------------------------------

namespace
{

inline unsigned int gangMask(EGangedParameter param)
{
	return (1 << ((int) param));
}

inline unsigned int isSetInGangMask(unsigned int mask, EGangedParameter param)
{
	return ((mask & gangMask(param)) != 0);
}
};
// ---------------------------------------------------------------------------

extern DebrisToolOpMode QueryOpMode();
extern void ToggleOpMode();
extern void CreateAutoFilterGUI(void);
extern void DestroyAutoFilterGUI(void);
extern bool ShowAutoFilterGUI(void);
extern bool HideAutoFilterGUI(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern bool IsExecButtonEnabled(EExecButtonId button);
extern void CaptureGUIParameters(CPDLElement &pdlEntryToolParams);
extern bool IsToolVisible(void);
extern bool SetShowHighlights(bool flag);
extern bool IsShowHighlight();
extern void GatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly);
extern void WriteAllFiltersToPDLEntry(CPDLElement &pdlEntryToolParams);
extern int ReadAllFiltersfromPDLEntry(const CPDLElement &parentElement);
extern void CleanUpAfterPDLExecution();
extern void SetRedEyeButtonState(bool enabled, bool down);
extern void SetGreenEyeButtonState(bool enabled, bool down);
extern void SetShowAlphaButtonState(int newState); // Tri-state (-1 = disabled, 0 = off, 1 = on)
extern void SetPreviewFrameButtonState(bool enabled, bool down);
extern int GetNumberOfSelectedFilters();

extern void GatherShineGuiParameters(ShineToolSettings &shineSettings);
extern void EnableAnalyzeClip(bool flag);
extern void EnableAnalyzeMarked(bool flag);
extern void EnableAnalyzeShot(bool flag);
extern void EnableResetClip(bool flag);
extern void EnableResetMarked(bool flag);
extern void EnableResetShot(bool flag);
extern void EnableSingleProc(bool flag);
extern void EnableMultiProc(bool flag);
enum ShineDetectEnableFlags { DARK = 1, BRIGHT = 2, ALPHA = 4 };
extern void SetShineDetectEnabledState(unsigned char flags);
extern void EnableSubtoolSwitching(bool flag);

// ---------------------------------------------------------------------------

class TNewFilterForm;
class TNewFilterSetForm;
struct SFilter;
struct SFilterSlot;
struct SFilterSet;
class CFilterManager;

// ---------------------------------------------------------------------------

class TAutofilterGUI : public TMTIForm
{
__published: // IDE-managed Components
	TColorPanel *AutofilterControlPanel;
	TGroupBox *AF_FilterDetailsPanel;
	TPanel *ToolTitlePanel;
	TLabel *ToolTitleLabel;
	TExecButtonsFrame *ExecButtonsToolbar;
	TLabel *AF_FilterNameLabel;
	TTrackEditFrame *AF_MaxSizeTrackEdit;
   TTrackEditFrame *AF_RgbMotionTrackEdit;
	TTrackEditFrame *AF_FixToleranceTrackEdit;
	TTrackEditFrame *AF_MinContrastTrackEdit;
   TTrackEditFrame *AF_AlphaMotionTrackEdit;
	TGroupBox *AF_DebrisContrastRadioPanel;
	TLabel *AF_DebrisContrastLabel;
	TRadioButton *AF_DebrisContrastBrightRadioButton;
	TRadioButton *AF_DebrisContrastDarkRadioButton;
	TBitBtn *AF_ResetButton;
	TBitBtn *AF_SaveDefaultsButton;
	TTrackEditFrame *AF_CorrectionEdgeBlurTrackEdit;
	TBitBtn *WTF_NewSetButton;
	TGroupBox *AF_DetectChannelShadowPanel;
	TGroupBox *AF_ProcessChannelsShadowPanel;
	TCheckBox *AF_ProcessRShadowCheckBox;
	TCheckBox *AF_ProcessGShadowCheckBox;
	TCheckBox *AF_ProcessBShadowCheckBox;
	TRadioButton *AF_DetectRRadioButton;
	TRadioButton *AF_DetectGRadioButton;
	TRadioButton *AF_DetectBRadioButton;
	TRadioButton *AF_DetectARadioButton;
	TTimer *WTF_EyedropperTimer;
	TGroupBox *PreviewModePanel;
	TRadioButton *HighlightRadioButton;
	TRadioButton *ProcessedRadioButton;
	TLabel *PreviewModeLabel;
	TTrackEditFrame *AF_DustMaxSizeTrackEdit;
	TExecStatusBarFrame *ExecStatusBar;
	TPanel *WTF_ColorProfilePanel;
	TLabel *DebrisColorProfileLabel;
	TPanel *RadioButtonPanel;
	TRadioButton *FullRangeRadioButton;
	TRadioButton *UserDefinedRadioButton;
	TPanel *WTF_DisabledColorProfilePanel;
	TLabel *Label9;
	TPanel *Panel11;
	TRadioButton *RadioButton7;
	TRadioButton *RadioButton8;
	TPanel *WTF_TrackBarDisabledPanel;
	TPanel *AF_MultipleFiltersSelectedPanel;
	TLabel *AF_LBracketLabel;
	TLabel *AF_MultipleFilterSelectedLabel;
	TLabel *AF_RBracketLabel;
	TGroupBox *AF_ChannelPanel;
	TPanel *AF_DetectPanel;
	TSpeedButton *AF_DetectBlueButton;
	TSpeedButton *AF_HilitedDetectBlueButton;
	TSpeedButton *AF_DetectAlphaButton;
	TLabel *AF_DetectLabel;
	TSpeedButton *AF_DetectRedButton;
	TSpeedButton *AF_DetectGreenButton;
	TSpeedButton *AF_HilitedDetectRedButton;
	TSpeedButton *AF_HilitedDetectGreenButton;
	TSpeedButton *AF_HilitedDetectAlphaButton;
	TPanel *AF_ProcessPanel;
	TLabel *AF_ProcessLabel;
	TSpeedButton *AF_ProcessBlueButton;
	TSpeedButton *AF_ProcessGreenButton;
	TSpeedButton *AF_ProcessRedButton;
	TSpeedButton *AF_HilitedProcessRedButton;
	TSpeedButton *AF_HilitedProcessGreenButton;
	TSpeedButton *AF_HilitedProcessBlueButton;
	TComboBox *WTF_FilterBankSelectionComboBox;
	TCheckBox *AF_UseHardMotionForDustCheckBox;
	TCheckBox *AF_UsePrecomputedMotionsCheckBox;
	TCheckBox *UsePastFramesOnlyCheckBox;
	TPanel *UsePastFramesOnlyLabelPanel;
	TPanel *LeftEdgePanel;
	TLabel *AF_FilterSetNameLabel;
	TTrackEditFrame *AF_MinSizeTrackEdit;
	TCheckBox *AF_UseAlphaAsMaskCheckBox;
	TTrackEditFrame *AF_AlphaGrainSuppressionTrackEdit;
	TCheckBox *AF_UseGrainFilterCheckBox;
	TGroupBox *AF_NoFilterSelectedPanel;
	TTimer *DetectionMapLagTimer;
	TSpeedButton *AF_RedEyeButton;
	TSpeedButton *AF_GreenEyeButton;
	TPanel *AF_HideyPanel;
	TTrackEditFrame *AF_DisabledMaxSizeTrackEdit;
	TTrackEditFrame *AF_DisabledMinSizeTrackEdit;
	TTrackEditFrame *AF_DisabledContrastSensitivityTrackEdit;
	TTrackEditFrame *AF_DisabledAlphaGrainSuppressionTrackEdit;
	TGroupBox *AF_DisabledDebrisContrastRadioPanel;
	TLabel *AF_Label4;
	TRadioButton *AF_RadioButton3;
	TRadioButton *AF_RadioButton4;
	TTimer *ContinuousPreviewLagTimer;
	TPanel *AF_FilterSelectionPanel;
	TColorPanel *AF_BasicFilterPanel;
	THeaderControl *AF_DebrisHeaderControl;
	TFilterBankFrame *AF_FilterBankFrame;
	TPanel *AF_StupidHeaderRightEdgePanel;
	TBitBtn *AF_DeleteFilterButton;
	TPanel *AF_FilterBankTopPanel;
	TLabel *AF_FiltersLabel;
	TLabel *AF_SetsLabel;
	TSpeedButton *AF_SelectAllButton;
	TBitBtn *AF_SaveSetButton;
	TBitBtn *AF_DeleteSetButton;
	TBitBtn *AF_LoadSetButton;
	TBitBtn *AF_UpdateSetButton;
	TBitBtn *AF_LoadFilterButton;
	TBitBtn *AF_NewFilterButton;
	TBitBtn *AF_UnloadFilterButton;
	TLabel *AF_Label16;
	TPageControl *OpModePageControl;
   TPanel *SHINE_Dark_ControlPanel;
   TTrackEditFrame *SHINE_Dark_MaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Dark_TemporalContrastTrackEdit;
   TBitBtn *SHINE_Dark_ResetButton;
   TTrackEditFrame *SHINE_Dark_MinSizeTrackEdit;
   TTrackEditFrame *SHINE_Dark_MotionToleranceTrackEdit;
   TTrackEditFrame *SHINE_Dark_DisabledMaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Dark_DisabledMinSizeTrackEdit;
   TTrackEditFrame *SHINE_Dark_DisabledTemporalContrastTrackEdit;
   TTrackEditFrame *SHINE_Dark_DisabledMotionToleranceTrackEdit;
	TTabSheet *SHINE_TabSheet;
	TTabSheet *AF_TabSheet;
   TPanel *SHINE_AnalyzeButtonPanel;
   TSpeedButton *SHINE_AnalyzeShotButton;
   TSpeedButton *SHINE_AnalyzeClipButton;
   TPanel *SHINE_Dark_SettingsPanel;
   TPresetsFrame *SHINE_Dark_PresetsFrame;
   TSpeedButton *SHINE_Dark_EnableButton;
   TPanel *SHINE_Bright_ControlPanel;
   TSpeedButton *SHINE_Bright_EnableButton;
   TPresetsFrame *SHINE_Bright_PresetsFrame;
   TBitBtn *SHINE_Bright_ResetButton;
   TPanel *SHINE_Bright_SettingsPanel;
   TTrackEditFrame *SHINE_Bright_MotionToleranceTrackEdit;
   TTrackEditFrame *SHINE_Bright_TemporalContrastTrackEdit;
   TTrackEditFrame *SHINE_Bright_DisabledTemporalContrastTrackEdit;
   TTrackEditFrame *SHINE_Bright_MaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Bright_MinSizeTrackEdit;
   TTrackEditFrame *SHINE_Bright_DisabledMinSizeTrackEdit;
   TTrackEditFrame *SHINE_Bright_DisabledMaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Bright_DisabledMotionToleranceTrackEdit;
   TPanel *SHINE_Alpha_ControlPanel;
   TSpeedButton *SHINE_Alpha_EnableButton;
   TPresetsFrame *SHINE_Alpha_PresetsFrame;
   TBitBtn *SHINE_Alpha_ResetButton;
   TPanel *SHINE_Alpha_SettingsPanel;
   TTrackEditFrame *SHINE_Alpha_MotionToleranceTrackEdit;
   TTrackEditFrame *SHINE_Alpha_GrainSuppressionTrackEdit;
   TTrackEditFrame *SHINE_Alpha_DisabledGrainSuppressionTrackEdit;
   TTrackEditFrame *SHINE_Alpha_MaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Alpha_DisabledMaxSizeTrackEdit;
   TTrackEditFrame *SHINE_Alpha_DisabledMotionToleranceTrackEdit;
   TPanel *SHINE_LowerSeparator;
   TPanel *SHINE_UpperSeparator;
   TPanel *SHINE_Dark_DisabledPresetsPanel;
   TSpeedButton *SHINE_Dark_DisabledPresetButton1;
   TSpeedButton *SHINE_Dark_DisabledPresetButton2;
   TSpeedButton *SHINE_Dark_DisabledPresetButton3;
   TSpeedButton *SHINE_Dark_DisabledPresetButton4;
   TSpeedButton *SHINE_Dark_DisabledSavePresetButton;
   TPanel *SHINE_Bright_DisabledPresetsPanel;
   TSpeedButton *SHINE_Bright_DisabledPresetButton1;
   TSpeedButton *SHINE_Bright_DisabledPresetButton2;
   TSpeedButton *SHINE_Bright_DisabledPresetButton3;
   TSpeedButton *SHINE_Bright_DisabledPresetButton4;
   TSpeedButton *SHINE_Bright_DisabledSavePresetButton;
   TPanel *SHINE_Alpha_DisabledPresetsPanel;
   TSpeedButton *SHINE_Alpha_DisabledPresetButton2;
   TSpeedButton *SHINE_Alpha_DisabledPresetButton3;
   TSpeedButton *SHINE_Alpha_DisabledPresetButton4;
   TSpeedButton *SHINE_Alpha_DisabledSavePresetButton;
   TSpeedButton *SHINE_Alpha_DisabledPresetButton1;
   TSpeedButton *SHINE_AnalyzeMarkedButton;
   TLabel *AnalyzeLabel;
   TLabel *ResetLabel;
   TSpeedButton *SHINE_ResetShotButton;
   TSpeedButton *SHINE_ResetMarkedButton;
   TSpeedButton *SHINE_ResetClipButton;
   TSpeedButton *SHINE_Alpha_RedEyeButton;
   TSpeedButton *SHINE_Alpha_ShowAlphaButton;
   TSpeedButton *AF_ShowAlphaButton;
   TTrackEditFrame *SHINE_Bright_DisabledSpatialContrastTrackEdit;
   TTrackEditFrame *SHINE_Bright_SpatialContrastTrackEdit;
   TTrackEditFrame *SHINE_Dark_DisabledSpatialContrastTrackEdit;
   TTrackEditFrame *SHINE_Dark_SpatialContrastTrackEdit;
   TColorLabel *SHINE_ShotHasNotBeenAnalyzedLabel;
   TCheckBox *SHINE_GangBrightAndDarkCheckBox;
   TPanel *NoShineReparentPanel;
   TTrackEditFrame *AF_LuminanceThresholdTrackEdit;

	void __fastcall PreviewDisplayRadioButtonClick(TObject *Sender);
	void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender);
	void __fastcall AF_DetectChannelChanged(TObject *Sender);
	void __fastcall AF_DebrisContrastChanged(TObject *Sender);
	void __fastcall AF_NewFilterButtonClick(TObject *Sender);
	void __fastcall AF_LoadFilterButtonClick(TObject *Sender);
	void __fastcall AF_UnloadFilterButtonClick(TObject *Sender);
	void __fastcall AF_DeleteFilterButtonClick(TObject *Sender);
	void __fastcall WTF_NewSetButtonClick(TObject *Sender);
	void __fastcall AF_DeleteSetButtonClick(TObject *Sender);
	void __fastcall AF_ResetButtonClick(TObject *Sender);
	void __fastcall AF_SaveDefaultsButtonClick(TObject *Sender);
	void __fastcall AF_DetectRadioButtonChanged(TObject *Sender);
	void __fastcall AF_DetectRRadioButtonEnter(TObject *Sender);
	void __fastcall AF_DetectRRadioButtonExit(TObject *Sender);
	void __fastcall AF_DetectGRadioButtonEnter(TObject *Sender);
	void __fastcall AF_DetectGRadioButtonExit(TObject *Sender);
	void __fastcall AF_DetectBRadioButtonEnter(TObject *Sender);
	void __fastcall AF_DetectBRadioButtonExit(TObject *Sender);
	void __fastcall AF_ProcessRShadowCheckBoxClick(TObject *Sender);
	void __fastcall AF_ProcessGShadowCheckBoxClick(TObject *Sender);
	void __fastcall AF_ProcessBShadowCheckBoxClick(TObject *Sender);
	void __fastcall AF_ProcessRShadowCheckBoxEnter(TObject *Sender);
	void __fastcall AF_ProcessRShadowCheckBoxExit(TObject *Sender);
	void __fastcall AF_ProcessGShadowCheckBoxExit(TObject *Sender);
	void __fastcall AF_ProcessGShadowCheckBoxEnter(TObject *Sender);
	void __fastcall AF_ProcessBShadowCheckBoxEnter(TObject *Sender);
	void __fastcall AF_ProcessBShadowCheckBoxExit(TObject *Sender);
	void __fastcall AF_ProcessRedButtonClick(TObject *Sender);
	void __fastcall AF_ProcessGreenButtonClick(TObject *Sender);
	void __fastcall AF_ProcessBlueButtonClick(TObject *Sender);
	void __fastcall AF_HilitedProcessBlueButtonClick(TObject *Sender);
	void __fastcall AF_HilitedProcessRedButtonClick(TObject *Sender);
	void __fastcall AF_HilitedProcessGreenButtonClick(TObject *Sender);
	void __fastcall AF_DebrisContrastRadioButtonEnter(TObject *Sender);
	void __fastcall AF_DebrisContrastRadioButtonExit(TObject *Sender);
	void __fastcall AF_UseHardMotionForDustCheckBoxClick(TObject *Sender);
	void __fastcall AF_DetectARadioButtonEnter(TObject *Sender);
	void __fastcall AF_DetectARadioButtonExit(TObject *Sender);
	void __fastcall AF_SelectAllButtonClick(TObject *Sender);
	void __fastcall AF_SaveSetButtonClick(TObject *Sender);
	void __fastcall AF_LoadSetButtonClick(TObject *Sender);
	void __fastcall AF_UpdateSetButtonClick(TObject *Sender);
	void __fastcall UsePastFramesOnlyCheckBoxClick(TObject *Sender);
	void __fastcall UsePastFramesOnlyLabelPanelClick(TObject *Sender);
	void __fastcall AF_UseAlphaAsMaskCheckBoxClick(TObject *Sender);
	void __fastcall AF_UseGrainFilterCheckBoxClick(TObject *Sender);
	void __fastcall AF_GreenEyeButtonClick(TObject *Sender);
	void __fastcall DetectionMapLagTimerTimer(TObject *Sender);
	void __fastcall AF_RedEyeButtonClick(TObject *Sender);
	void __fastcall ContinuousPreviewLagTimerTimer(TObject *Sender);
	void __fastcall SHINE_Dark_ResetButtonClick(TObject *Sender);
	void __fastcall OpModePageControlChange(TObject *Sender);
   void __fastcall SHINE_AnalyzeButtonClick(TObject *Sender);
   void __fastcall SHINE_Bright_EnableButtonClick(TObject *Sender);
   void __fastcall SHINE_Dark_EnableButtonClick(TObject *Sender);
   void __fastcall SHINE_Alpha_EnableButtonClick(TObject *Sender);
   void __fastcall SHINE_Bright_ResetButtonClick(TObject *Sender);
   void __fastcall SHINE_Alpha_ResetButtonClick(TObject *Sender);
   void __fastcall SHINE_ResetAnalysisButtonClick(TObject *Sender);
   void __fastcall SHINE_Alpha_RedEyeButtonClick(TObject *Sender);
   void __fastcall SHINE_Alpha_ShowAlphaButtonClick(TObject *Sender);
   void __fastcall AF_ShowAlphaButtonClick(TObject *Sender);
   void __fastcall SHINE_GangBrightAndDarkCheckBoxClick(TObject *Sender);





private: // User declarations
   string *AF_AutofilterIniFileName = nullptr;
	string *SHINE_AutofilterIniFileName = nullptr;

	bool bDoPreviewAgain;

	CFilterManager *activeFilterManager = nullptr;
	CFilterManager *SHINE_FilterManager = nullptr;
	CFilterManager *AF_FilterManager = nullptr;

   bool SHINE_darkSettingsWereChangedLast = false;

	string AF_SelectedFilterID;

	bool inhibitCallbacks = false; // hack

	string AF_LastFilterSetLoaded;
	void AF_SelectFilter(const string &filterID);
	void AF_SelectFilter(int filterSlot);
	void AF_UnselectFilter();
   void AF_MakeSureAFilterIsSelected();

	// GLOBAL
	DEFINE_CBHOOK(MaybeHandleUserInput, TAutofilterGUI);
	// DEFINE_CBHOOK(EyedropperStateChanged, TAutofilterGUI);
	DEFINE_CBHOOK(ClipHasChanged, TAutofilterGUI);
	DEFINE_CBHOOK(MarksHaveChanged, TAutofilterGUI);

	// PER SUBTOOL
	DEFINE_CBHOOK(AF_SelectionChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_GangChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_FilterUseChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_FilterDetailsChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_MaxSizeChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_MinSizeChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_DustMaxSizeChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_MinContrastChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_LuminanceThresholdChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_FixToleranceChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_RgbMotionChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_AlphaMotionChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_EdgeBlurChanged, TAutofilterGUI);
	DEFINE_CBHOOK(AF_GrainFilterParamChanged, TAutofilterGUI);

	DEFINE_CBHOOK(SHINE_Dark_MaxSizeTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Dark_MinSizeTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Dark_TemporalContrastTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Dark_SpatialContrastTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Dark_MotionToleranceTrackEditChanged, TAutofilterGUI);

	DEFINE_CBHOOK(SHINE_Bright_MaxSizeTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Bright_MinSizeTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Bright_TemporalContrastTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Bright_SpatialContrastTrackEditChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_Bright_MotionToleranceTrackEditChanged, TAutofilterGUI);

	DEFINE_CBHOOK(SHINE_AlphaTrackEditChanged, TAutofilterGUI);

	void AF_MaybeHandleUserInput(void *Sender);
	void SHINE_MaybeHandleUserInput(void *Sender);

	int AF_GetDetectChannelFromGUI();
	int AF_GetProcessChannelsFromGUI(bool &processR, bool &processG, bool &processB);
	void AF_SetGUIDetectChannel(int channel);
	void AF_SetGUIProcessChannels(bool processR, bool processG, bool processB);

	TNewFilterForm *NewFilterNameDialog;
	TNewFilterSetForm *NewFilterSetNameDialog;

	string GetNameForNewFilter(void);
	string GetNameForNewFilterSet(void);
	SFilter *AF_CreateNewFilterFromFilterDetails(void);

	void AF_UpdateFilterDetailPanel();
	void AF_UpdateFilterDetailGUIFromFilter(const SFilter &filter);
	void AF_UpdateDefaultsButtons(void);
	void AF_FillInFilterDetailsFromGUI(SFilter &filter, unsigned int mask = ((unsigned int) - 1));
	void AF_FillInGangedFilterDetails(unsigned int detailMask = (~0));
	void AF_UpdateFilterManagerGUI(void);
	void AF_UpdateMultiSelectionIndicator();
	void AF_ConformAlphaFilterEnabling(bool alphaChannelSelected);
	void AF_FilterParameterChanged();
	void AF_FilterSetChanged();
	void AF_EnableOrDisableAlphaButton();

	void SHINE_UpdateSettingsPanels();
	void SHINE_UpdateResetButtons();

	void FocusControl(Controls::TWinControl* Control);

	int CurrentCursor;

	int _filterChangeCounter = 0;

	void AF_SyncSizeRelatedControls();
	void AF_initiateDetectionMapLagTiming();
	void AF_updateDetectionMap();
	void AF_initiateContinuousPreviewLagTiming();
	void AF_enterContinuousPreviewMode();
	void AF_exitFromContinuousPreviewMode();

   void SHINE_setDarkSettingsEnable(bool enable);
   void SHINE_setBrightSettingsEnable(bool enable);
   void SHINE_setAlphaSettingsEnable(bool enable);

   class ShinePresetParameters : public IPresetParameters
   {
   public:
      int maxSize = -1;
      int minSize = -1;
      int spatialContrast = -1;
      int temporalContrast = -1;
      int motionTolerance = -1;
      int grainSuppression = -1;

      virtual void ReadParameters(CIniFile *ini, const string &iniSectionName);
      virtual void WriteParameters(CIniFile *ini, const string &iniSectionName) const;
      virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const;

      bool operator==(const ShinePresetParameters &rhs) const;
   };

   static string SHINE_getIniFileName() { return CPMPIniFileName("Shine", "$(USERDIR)Shine.ini"); }
   PresetParameterModel<ShinePresetParameters> *SHINE_darkPresets;
   PresetParameterModel<ShinePresetParameters> *SHINE_brightPresets;
   PresetParameterModel<ShinePresetParameters> *SHINE_alphaPresets;
	DEFINE_CBHOOK(SHINE_darkPresetHasChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_brightPresetHasChanged, TAutofilterGUI);
	DEFINE_CBHOOK(SHINE_alphaPresetHasChanged, TAutofilterGUI);
   void SHINE_UpdatePresetCurrentValues();
   void SHINE_UpdateSettingsGui(const ShineToolSettings &settings);

	// Exec buttons toolbar
	void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
	void ExecStop(void);
	void ExecCaptureToPDL(void);
	void ExecCaptureAllEventsToPDL(void);
	void ExecSetResumeTimecode(int frameIndex);
	void ExecSetResumeTimecodeToCurrent(void);
	void ExecSetResumeTimecodeToDefault(void);
	void ExecGoToResumeTimecode(void);

	void PreviewFrame(void);
	void RenderFrame(void);

	void setBusyCursor(void);
	void setIdleCursor(void);

public: // User declarations
	__fastcall TAutofilterGUI(TComponent* Owner);

	void formCreate();
	void formShow();
	void formHide();
	void formDestroy();

	DebrisToolOpMode queryOpMode();
	void toggleOpMode();

	void updateExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag);
	bool runExecButtonsCommand(EExecButtonId command);
	bool isExecButtonEnabled(EExecButtonId button);

	bool setShowHighlights(bool flag);
	bool isShowHighlight();

	void gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly);
	void writeAllFiltersToPDLEntry(CPDLElement &pdlEntryToolParams);
	int readAllFiltersfromPDLEntry(const CPDLElement &parentElement);
	void cleanUpAfterPDLExecution();

	void AF_gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly);
	void SHINE_gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly);

	void setRedEyeButtonState(bool enabled, bool down);
	void AF_setGreenEyeButtonState(bool enabled, bool down);
   void AF_setShowAlphaButtonState(int newState); // Tri-state (-1 = disabled, 0 = off, 1 = on)
   void SHINE_setShowAlphaButtonState(int newState); // Tri-state (-1 = disabled, 0 = off, 1 = on)
   void SHINE_setDetectEnabledState(unsigned char flags);
   void enableSubtoolSwitching(bool flag);

	void setPreviewFrameButtonState(bool enabled, bool down);
	int getNumberOfSelectedFilters();

   void SHINE_gatherGuiParameters(ShineToolSettings &shineParams);

	virtual bool WriteSettings(CIniFile *ini, const string &IniSection);
	virtual bool ReadSettings(CIniFile *ini, const string &IniSection);

   bool FindAndReparentAutoFilterTabControlPanel(TControl *control);
};

// ---------------------------------------------------------------------------
extern PACKAGE TAutofilterGUI *AutofilterGUI;
extern MTI_UINT32 *AutoFilterFeatureTable[];
// ---------------------------------------------------------------------------
#endif
