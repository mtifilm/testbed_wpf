//---------------------------------------------------------------------------

#pragma hdrstop

#include "ShinePreprocOutputCache.h"

#include "IniFile.h"
#include "MTIstringstream.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

ShinePreprocOutputCache::ShinePreprocOutputCache(const vector<string> &directoryChain, const string &filenameExtension)
: _directoryChain(directoryChain)
{
   _filenameExtension = filenameExtension.empty()
                        ? string("dat")
                        : ((filenameExtension[0] == '.')
                              ? filenameExtension.substr(1)
                              : filenameExtension);
}
//---------------------------------------------------------------------------

ShinePreprocOutputCache::~ShinePreprocOutputCache()
{
   CAutoSpinLocker lock(_cacheLock);
   clearMainMemoryCache();
}
//---------------------------------------------------------------------------

void ShinePreprocOutputCache::addPreprocOutputToCache(const string &id, ShinePreprocOutputSharedPtr dataToCache)
{
   CAutoSpinLocker lock(_cacheLock);
   removePreprocOutputFromCache(id);
   _cacheMap[id] = dataToCache;

   auto retVal = writeCacheEntryToDisk(id, dataToCache);
   MTIassert(retVal == 0);

   ++_generation;
}
//---------------------------------------------------------------------------

void ShinePreprocOutputCache::removePreprocOutputFromCache(const string &id)
{
   CAutoSpinLocker lock(_cacheLock);

   auto iter = _cacheMap.find(id);
   if (iter != _cacheMap.end())
   {
      _cacheMap.erase(iter);
   }

   int retVal = deleteCacheEntryFromDisk(id);
   MTIassert(retVal == 0);

   ++_generation;
}
//---------------------------------------------------------------------------

ShinePreprocOutputSharedPtr ShinePreprocOutputCache::retrievePreprocOutputFromCache(const string &id)
{
   CAutoSpinLocker lock(_cacheLock);

   // operator[] creates an entry if not found, so use find() instead!
   auto iter = _cacheMap.find(id);
   if (iter != _cacheMap.end())
   {
      return iter->second;
   }

   auto cacheEntryFromDisk = readCacheEntryFromDisk(id);
   MTIassert(cacheEntryFromDisk);
   if (cacheEntryFromDisk == nullptr)
   {
      // Not found!
      return cacheEntryFromDisk;
   }

   // Keep a copy of the cache entry in memory
   _cacheMap[id] = cacheEntryFromDisk;

   return cacheEntryFromDisk;
}
//---------------------------------------------------------------------------

// Cache must be locked before calling!
void ShinePreprocOutputCache::clearMainMemoryCache()
{
   _cacheMap.clear();
}
//---------------------------------------------------------------------------

/* static */
string ShinePreprocOutputCache::makeCacheId(int inFrame, int outFrame)
{
   MTIostringstream os;
   os << inFrame << "_" << outFrame;
   return os.str();
}
//---------------------------------------------------------------------------

string ShinePreprocOutputCache::makeCacheEntryFileName(int chainLinkNumber, const string &id)
{
   if (chainLinkNumber >= _directoryChain.size())
   {
      return string("");
   }

   return AddDirSeparator(_directoryChain[chainLinkNumber]) + id + "." + _filenameExtension;
}
//---------------------------------------------------------------------------

int ShinePreprocOutputCache::writeCacheEntryToDisk(const string &id, ShinePreprocOutputSharedPtr &dataToCache)
{
   auto filename = makeCacheEntryFileName(0, id);
   if (filename.size() == 0)
   {
      return -1;
   }

   return ShinePreprocOutput::WriteToFile(filename, dataToCache);
}
//---------------------------------------------------------------------------

int ShinePreprocOutputCache::deleteCacheEntryFromDisk(const string &id)
{
   auto filename = makeCacheEntryFileName(0, id);

   // It's OK if the file does not exist.
   if (filename.empty() || !DoesFileExist(filename))
   {
      return 0;
   }

   // Delete the cache file. CAREFUL - DeleteFile returns 0 on error.
   if (FALSE == ::DeleteFile(filename.c_str()))
   {
      TRACE_0(errout << "ERROR: Failed to delete file " << filename << endl
              << "Reason: " << GetLastSystemErrorMessage());
      return -1;
   }

   return 0;
}
//---------------------------------------------------------------------------

ShinePreprocOutputSharedPtr ShinePreprocOutputCache::readCacheEntryFromDisk(const string &id)
{
   for (auto chainIndex = 0; chainIndex < _directoryChain.size(); ++chainIndex)
   {
      auto filename = makeCacheEntryFileName(chainIndex, id);
      if (!filename.empty() && DoesFileExist(filename))
      {
         return ShinePreprocOutput::ReadFromFile(filename);
      }
   }

   return std::make_shared<ShinePreprocOutput>(nullptr);
}
//---------------------------------------------------------------------------


