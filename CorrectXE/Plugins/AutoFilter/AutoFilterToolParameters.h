//---------------------------------------------------------------------------

#ifndef AutoFilterToolParametersH
#define AutoFilterToolParametersH
//---------------------------------------------------------------------------

#include "AutoFilterParameters.h"
#include "ToolObject.h"
//////////////////////////////////////////////////////////////////////////////

class CAutoFilterToolParameters : public CToolParameters
{
public:
   CAutoFilterToolParameters()
    : CToolParameters(1, 1), paramAF() {};

	virtual ~CAutoFilterToolParameters() {};
   CAutoFilterParameters& getParameterRef() { return paramAF; }

   void setUsePastFramesOnlyFlag(bool newFlag) { usePastFramesOnlyFlag = newFlag; };
   bool getUsePastFramesOnlyFlag() { return usePastFramesOnlyFlag; };

private:
	CAutoFilterParameters paramAF;
   bool usePastFramesOnlyFlag;
};

#endif
