//---------------------------------------------------------------------------


#ifndef FilterBankFrameUnitH
#define FilterBankFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "BasicFilterFrameUnit.h"
#include "PropX.h"
#include "ColorPanel.h"

#define MAX_DISPLAYED_FILTERS 6
#define INVALID_FILTER_NUMBER -1

//---------------------------------------------------------------------------

class TFilterBankFrame : public TFrame
{
__published:	// IDE-managed Components
    TColorPanel *Filter1SelectPanel;
    TColorPanel *Filter2SelectPanel;
    TColorPanel *Filter3SelectPanel;
    TColorPanel *Filter4SelectPanel;
    TColorPanel *Filter5SelectPanel;
    TColorPanel *Filter6SelectPanel;

    TBasicFilterFrame *BasicFilterFrame1;
    TBasicFilterFrame *BasicFilterFrame2;
    TBasicFilterFrame *BasicFilterFrame3;
    TBasicFilterFrame *BasicFilterFrame4;
    TBasicFilterFrame *BasicFilterFrame5;
    TBasicFilterFrame *BasicFilterFrame6;
	TColorPanel *BackgroundPanel;

private:	// User declarations
    TColorPanel *FilterSelectPanel[MAX_DISPLAYED_FILTERS];
    TBasicFilterFrame *FilterFrame[MAX_DISPLAYED_FILTERS];

    DEFINE_CBHOOK(UseCheckBoxChanged, TFilterBankFrame);

    DEFINE_CBHOOK(FilterWasClicked,      TFilterBankFrame);
    DEFINE_CBHOOK(FilterWasShiftClicked, TFilterBankFrame);
    DEFINE_CBHOOK(FilterWasCtrlClicked,  TFilterBankFrame);

public:		// User declarations
    __fastcall TFilterBankFrame(TComponent* Owner);

    void Enable(int filterNumber, bool flag);
    void Select(int filterNumber);
    void Unselect(int filterNumber);
    void Gang(int filterNumber);
    void Ungang(int filterNumber);
    void GangAll(void);
    void UngangAll(void);
    void Use(int filterNumber, bool flag);
    void SetFilterName(int filterNumber, const string &newName);
	 void AF_SetValues(int filterNumber, int newMaxSize, int newMinSize,  int newDustSize,
													 int newContrast, int newConfid,
													 int newEdgeBlur, int newH, int newV);
	 void SHINE_SetValues(int filterNumber, int newMaxSize, int newMinSize,
														 int newContrast, int newAggression);
	 bool IsEnabled(int filterNumber);
    bool IsSelected(int filterNumber);
    bool IsGanged(int filterNumber);
    bool IsUsed(int filterNumber);
    int GetSelectedFilterNumber(void);
    int GetGangCount();

    CBHook SelectionChange;
    CBHook GangChange;
    CBHook FilterUseChange;

};
//---------------------------------------------------------------------------
extern PACKAGE TFilterBankFrame *FilterBankFrame;
//---------------------------------------------------------------------------
#endif
