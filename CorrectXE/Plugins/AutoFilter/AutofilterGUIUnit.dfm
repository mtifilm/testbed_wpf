object AutofilterGUI: TAutofilterGUI
  Left = 943
  Top = 176
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsToolWindow
  Caption = 'AutofilterGUI'
  ClientHeight = 720
  ClientWidth = 512
  Color = clBtnFace
  Constraints.MaxHeight = 749
  Constraints.MaxWidth = 518
  Constraints.MinHeight = 744
  Constraints.MinWidth = 518
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object AutofilterControlPanel: TColorPanel
    Left = -8
    Top = 0
    Width = 512
    Height = 720
    BevelOuter = bvNone
    Color = 6974058
    Constraints.MaxHeight = 720
    Constraints.MaxWidth = 512
    Constraints.MinHeight = 720
    Constraints.MinWidth = 512
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      512
      720)
    object NoShineReparentPanel: TPanel
      Left = 30
      Top = 54
      Width = 480
      Height = 552
      BevelOuter = bvNone
      TabOrder = 11
      Visible = False
    end
    object ToolTitlePanel: TPanel
      Left = 0
      Top = 0
      Width = 512
      Height = 32
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Visible = False
      object ToolTitleLabel: TLabel
        Left = 216
        Top = 0
        Width = 52
        Height = 19
        Caption = 'Debris'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    inline ExecButtonsToolbar: TExecButtonsFrame
      Left = 18
      Top = 636
      Width = 498
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 18
      ExplicitTop = 636
      ExplicitWidth = 498
      inherited ButtonPressedNotifier: TSpeedButton
        OnClick = ExecButtonsToolbar_ButtonPressedNotifier
      end
      inherited ResumeGroupBox: TGroupBox
        Left = 343
        Top = 3
        ExplicitLeft = 343
        ExplicitTop = 3
      end
      inherited ToolbarPanel: TColorPanel
        inherited CapturePDLButton: TSpeedButton
          Left = 297
          ExplicitLeft = 297
        end
      end
    end
    object WTF_NewSetButton: TBitBtn
      Left = 670
      Top = 134
      Width = 75
      Height = 21
      Caption = 'New Set'
      TabOrder = 1
      TabStop = False
      OnClick = WTF_NewSetButtonClick
    end
    object PreviewModePanel: TGroupBox
      Left = 29
      Top = 608
      Width = 470
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      DesignSize = (
        470
        26)
      object PreviewModeLabel: TLabel
        Left = 268
        Top = 6
        Width = 71
        Height = 13
        Anchors = []
        Caption = 'Preview Mode:'
      end
      object UsePastFramesOnlyLabelPanel: TPanel
        Left = -3
        Top = 2
        Width = 219
        Height = 20
        Hint = 'Get fixes from already fixed frames'
        Anchors = []
        BevelOuter = bvNone
        Caption = ' Use past frames only to find fixes'
        TabOrder = 3
        OnClick = UsePastFramesOnlyLabelPanelClick
      end
      object HighlightRadioButton: TRadioButton
        Left = 348
        Top = 4
        Width = 69
        Height = 17
        Hint = 'Preview fix locations only'#13#10'Press T to toggle to showing fixes'
        Anchors = []
        Caption = 'Highlights'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = True
        OnClick = PreviewDisplayRadioButtonClick
      end
      object ProcessedRadioButton: TRadioButton
        Left = 420
        Top = 4
        Width = 45
        Height = 17
        Hint = 
          'Preview proposed fixes'#13#10'Press T to toggle to showing fix locatio' +
          'ns'
        Anchors = []
        Caption = 'Fixes'
        TabOrder = 1
        OnClick = PreviewDisplayRadioButtonClick
      end
      object UsePastFramesOnlyCheckBox: TCheckBox
        Left = 9
        Top = 4
        Width = 15
        Height = 17
        Anchors = []
        Color = clBtnFace
        ParentColor = False
        TabOrder = 2
        OnClick = UsePastFramesOnlyCheckBoxClick
      end
    end
    inline ExecStatusBar: TExecStatusBarFrame
      Left = 22
      Top = 682
      Width = 480
      Height = 38
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      ExplicitLeft = 22
      ExplicitTop = 682
      inherited BackPanel: TPanel
        inherited ReadoutPanel: TPanel
          inherited ProgressPanelLight: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited PercentValue1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue1: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue1: TLabel
              Anchors = [akLeft]
            end
          end
          inherited ProgressPanelDark: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited RemainingLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue2: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PercentValue2: TLabel
              Anchors = [akLeft]
            end
          end
        end
      end
    end
    object WTF_ColorProfilePanel: TPanel
      Left = 544
      Top = 340
      Width = 285
      Height = 173
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 6
      Visible = False
      object DebrisColorProfileLabel: TLabel
        Left = 8
        Top = 4
        Width = 95
        Height = 13
        Caption = 'Debris Color Profile:'
      end
      object RadioButtonPanel: TPanel
        Left = 119
        Top = 3
        Width = 162
        Height = 19
        BevelOuter = bvNone
        TabOrder = 0
        object FullRangeRadioButton: TRadioButton
          Left = 1
          Top = 1
          Width = 73
          Height = 17
          Caption = 'Full Range'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object UserDefinedRadioButton: TRadioButton
          Left = 77
          Top = 1
          Width = 85
          Height = 17
          Caption = 'User Defined'
          TabOrder = 1
        end
      end
    end
    object WTF_DisabledColorProfilePanel: TPanel
      Left = 544
      Top = 520
      Width = 285
      Height = 173
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 95
        Height = 13
        Caption = 'Debris Color Profile:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Panel11: TPanel
        Left = 119
        Top = 3
        Width = 162
        Height = 19
        BevelOuter = bvNone
        TabOrder = 0
        object RadioButton7: TRadioButton
          Left = 1
          Top = 1
          Width = 73
          Height = 17
          Caption = 'Full Range'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnShadow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
        object RadioButton8: TRadioButton
          Left = 77
          Top = 1
          Width = 85
          Height = 17
          Caption = 'User Defined'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnShadow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object WTF_FilterBankSelectionComboBox: TComboBox
      Left = 600
      Top = 40
      Width = 149
      Height = 21
      Style = csDropDownList
      TabOrder = 8
      TabStop = False
      Items.Strings = (
        'Normal'
        '3 Layer Separation'
        'Alpha Filter')
    end
    object LeftEdgePanel: TPanel
      Left = 0
      Top = 0
      Width = 13
      Height = 732
      BevelOuter = bvNone
      TabOrder = 9
      Visible = False
    end
    object OpModePageControl: TPageControl
      Left = 24
      Top = 54
      Width = 480
      Height = 552
      ActivePage = AF_TabSheet
      TabOrder = 10
      OnChange = OpModePageControlChange
      object SHINE_TabSheet: TTabSheet
        Caption = 'Shine'
        ImageIndex = 2
        object SHINE_TabControlPanel: TColorPanel
          Left = 0
          Top = 0
          Width = 512
          Height = 720
          BevelOuter = bvNone
          Color = 6974058
          Constraints.MaxHeight = 720
          Constraints.MaxWidth = 512
          Constraints.MinHeight = 720
          Constraints.MinWidth = 512
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Locked = True
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object SHINE_ShotHasNotBeenAnalyzedLabel: TColorLabel
            Left = 252
            Top = 488
            Width = 211
            Height = 25
            AutoSize = False
            Caption = '  Shot has not been analyzed '
            Color = 2875903
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Segoe UI Semibold'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            Transparent = False
            Visible = False
          end
          object SHINE_Dark_ControlPanel: TPanel
            Left = 264
            Top = 46
            Width = 181
            Height = 257
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              181
              257)
            object SHINE_Dark_EnableButton: TSpeedButton
              Left = 16
              Top = 0
              Width = 60
              Height = 29
              AllowAllUp = True
              GroupIndex = 111
              Glyph.Data = {
                B60A0000424DB60A00000000000036000000280000002A000000150000000100
                180000000000800A000000000000000000000000000000000000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000000000000000000000000000
                00000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF404040404040404040404040404040
                404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFF0000000000000000000000000000000000000000
                00000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFF404040404040404040404040404040404040404040
                404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFF0000000000000000000000000000000000000000000000000000
                00000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFF404040404040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                404040404040404040404040C9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFF0000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFF404040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFF404040404040404040C9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFF40
                4040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFF404040404040404040C9AEFFC9AEFF0000C9AEFF000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000C9AEFFC9AEFFC9AEFF40404040
                4040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFF404040404040C9AEFFC9AEFF0000C9AEFF000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000C9AEFFC9AEFF40404040
                4040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF404040404040C9AEFF0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000040404040404040
                4040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000404040404040C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000404040404040C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000404040404040C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000404040404040C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000404040404040C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF4040404040400000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000C9AEFF40404040404040
                4040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF404040404040C9AEFF0000C9AEFF000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000C9AEFFC9AEFF40404040
                4040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF404040404040C9AEFF0000C9AEFFC9AEFF
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFF40
                4040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFF404040404040C9AEFFC9AEFF0000C9AEFFC9AEFF
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFF40
                4040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFF404040404040404040C9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFF0000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFF404040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFF404040404040404040C9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFF0000000000000000000000000000000000000000000000000000
                00000000000000000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFF404040404040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                404040404040404040404040C9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFF0000000000000000000000000000000000000000
                00000000000000000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFF404040404040404040404040404040404040404040
                404040404040404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000000000000000000000000000000000
                00000000C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFF404040404040404040404040404040404040
                404040C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000}
              NumGlyphs = 2
              OnClick = SHINE_Dark_EnableButtonClick
            end
            inline SHINE_Dark_PresetsFrame: TPresetsFrame
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              Anchors = [akLeft, akBottom]
              AutoSize = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              ExplicitLeft = 16
              ExplicitTop = 36
              ExplicitWidth = 157
              inherited PresetPanel: TPanel
                Width = 157
                Anchors = [akLeft, akRight]
                AutoSize = False
                ExplicitWidth = 157
                inherited PresetLabel: TLabel
                  Left = 70
                  Top = 0
                  Width = 4
                  Anchors = []
                  Caption = ''
                  ExplicitLeft = 70
                  ExplicitTop = 0
                  ExplicitWidth = 4
                end
                inherited PresetButton1: TSpeedButton
                  Left = 0
                  Anchors = []
                  ExplicitLeft = 0
                end
                inherited PresetButton2: TSpeedButton
                  Left = 29
                  Anchors = []
                  ExplicitLeft = 29
                end
                inherited PresetButton3: TSpeedButton
                  Left = 58
                  Anchors = []
                  ExplicitLeft = 58
                end
                inherited PresetButton4: TSpeedButton
                  Left = 86
                  Anchors = []
                  ExplicitLeft = 86
                end
                inherited DisabledSaveButton: TBitBtn
                  Left = 122
                  Anchors = [akLeft, akBottom]
                  ExplicitLeft = 122
                end
                inherited SavePresetButton: TBitBtn
                  Left = 122
                  Anchors = []
                  ExplicitLeft = 122
                end
              end
            end
            object SHINE_Dark_SettingsPanel: TPanel
              Left = 4
              Top = 58
              Width = 181
              Height = 225
              Anchors = [akLeft, akBottom]
              BevelOuter = bvNone
              TabOrder = 1
              inline SHINE_Dark_SpatialContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Hint = 
                  'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                  'or use arrow keys to change'
                TabOrder = 9
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 103
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 103
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 77
                  Caption = 'Spatial Contrast'
                  ExplicitTop = 2
                  ExplicitWidth = 77
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 3
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
              end
              inline SHINE_Dark_DisabledSpatialContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 8
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 77
                  Caption = 'Spatial Contrast'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 77
                end
                inherited TrackBar: TTrackBar
                  Tag = -1
                  Width = 129
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Dark_TemporalContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 121
                Width = 173
                Height = 41
                Hint = 
                  'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                  'or use arrow keys to change'
                TabOrder = 1
                ExplicitLeft = 4
                ExplicitTop = 121
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 103
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 103
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 89
                  Caption = 'Temporal Contrast'
                  ExplicitTop = 2
                  ExplicitWidth = 89
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 3
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
              end
              inline SHINE_Dark_DisabledTemporalContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 121
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 3
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 121
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 89
                  Caption = 'Temporal Contrast'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 89
                end
                inherited TrackBar: TTrackBar
                  Tag = -1
                  Width = 129
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Dark_MotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 161
                Width = 173
                Height = 41
                Hint = 'Set a parameter to fix more or less suspected debris'
                TabOrder = 0
                ExplicitLeft = 4
                ExplicitTop = 161
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Width = 82
                  Caption = 'Motion Tolerance'
                  ParentFont = False
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 4
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Tag = -1
                  end
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited RedGreenTick: TColorPanel
                  TabOrder = 3
                end
              end
              inline SHINE_Dark_MaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Hint = 
                  'Set the maximum size of detected debris'#13#10'Press 1 to select'#13#10'Move' +
                  ' mouse wheel or use arrow keys to change'
                TabOrder = 6
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 123
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 123
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 42
                  Caption = 'Max Size'
                  ExplicitTop = 2
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 1
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
              end
              inline SHINE_Dark_MinSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Hint = 
                  'Set the minimum size of detected debtis'#13#10'Press 2 to select'#13#10'Move' +
                  ' mouse wheel or use arrow keys to change'
                TabOrder = 7
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 38
                  Caption = 'Min Size'
                  ExplicitTop = 2
                  ExplicitWidth = 38
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Min = 1
                  TabOrder = 2
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
              end
              inline SHINE_Dark_DisabledMinSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBtnShadow
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Left = 6
                  Top = 3
                  Width = 38
                  Caption = 'Min Size'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitLeft = 6
                  ExplicitTop = 3
                  ExplicitWidth = 38
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Enabled = False
                  Position = 10
                  SliderVisible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Tag = -1
                    Text = '10'
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Dark_DisabledMaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 4
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Left = 6
                  Top = 3
                  Width = 42
                  Caption = 'Max Size'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitLeft = 6
                  ExplicitTop = 3
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Enabled = False
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Dark_DisabledMotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 161
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 2
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 161
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 82
                  Caption = 'Motion Tolerance'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Position = 10
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Text = '10'
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
            end
            object SHINE_Dark_ResetButton: TBitBtn
              Left = 84
              Top = 0
              Width = 89
              Height = 29
              Hint = 'Reset all parameters to current defaults (Ctrl+Shift+C)'
              Anchors = [akLeft, akBottom]
              Caption = ' Reset Defaults'
              TabOrder = 0
              OnClick = SHINE_Dark_ResetButtonClick
            end
            object SHINE_Dark_DisabledPresetsPanel: TPanel
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              BevelOuter = bvNone
              TabOrder = 3
              object SHINE_Dark_DisabledPresetButton1: TSpeedButton
                Left = 0
                Top = 0
                Width = 23
                Height = 22
                Caption = '1'
                Enabled = False
              end
              object SHINE_Dark_DisabledPresetButton2: TSpeedButton
                Left = 29
                Top = 0
                Width = 23
                Height = 22
                Caption = '2'
                Enabled = False
              end
              object SHINE_Dark_DisabledPresetButton3: TSpeedButton
                Left = 58
                Top = 0
                Width = 23
                Height = 22
                Caption = '3'
                Enabled = False
              end
              object SHINE_Dark_DisabledPresetButton4: TSpeedButton
                Left = 87
                Top = 0
                Width = 23
                Height = 22
                Caption = '4'
                Enabled = False
              end
              object SHINE_Dark_DisabledSavePresetButton: TSpeedButton
                Left = 122
                Top = 0
                Width = 35
                Height = 22
                Caption = 'Save'
                Enabled = False
              end
            end
          end
          object SHINE_Bright_ControlPanel: TPanel
            Left = 20
            Top = 46
            Width = 181
            Height = 257
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              181
              257)
            object SHINE_Bright_EnableButton: TSpeedButton
              Left = 16
              Top = 0
              Width = 60
              Height = 29
              AllowAllUp = True
              GroupIndex = 111
              Glyph.Data = {
                B60A0000424DB60A00000000000036000000280000002A000000150000000100
                180000000000800A000000000000000000000000000000000000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C0C0C0C0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC0
                C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFF0000C9AEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC0C0C0C0
                C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C9AEFFC9AEFF0000C9AEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC0C0C0C0
                C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C9AEFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0
                C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C00000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC0C0C0C0C0C0C0
                C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C9AEFF0000C9AEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC0C0C0C0
                C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C9AEFF0000C9AEFFC9AEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC0
                C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C9AEFFC9AEFF0000C9AEFFC9AEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC0
                C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF
                C0C0C0C0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C0C0C0C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000C9AEFFC9AEFF
                C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9
                AEFFC9AEFFC9AEFFC9AEFFC9AEFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
                C0C0C0C9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFFC9AEFF0000}
              NumGlyphs = 2
              OnClick = SHINE_Bright_EnableButtonClick
            end
            inline SHINE_Bright_PresetsFrame: TPresetsFrame
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              Anchors = [akLeft, akBottom]
              AutoSize = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              ExplicitLeft = 16
              ExplicitTop = 36
              ExplicitWidth = 157
              inherited PresetPanel: TPanel
                Width = 157
                Anchors = [akLeft, akRight]
                ExplicitWidth = 157
                inherited PresetLabel: TLabel
                  Left = 70
                  Top = 0
                  Width = 4
                  Caption = ''
                  ExplicitLeft = 70
                  ExplicitTop = 0
                  ExplicitWidth = 4
                end
                inherited PresetButton1: TSpeedButton
                  Left = 0
                  ExplicitLeft = 0
                end
                inherited PresetButton2: TSpeedButton
                  Left = 29
                  ExplicitLeft = 29
                end
                inherited PresetButton3: TSpeedButton
                  Left = 58
                  ExplicitLeft = 58
                end
                inherited PresetButton4: TSpeedButton
                  Left = 87
                  ExplicitLeft = 87
                end
                inherited DisabledSaveButton: TBitBtn
                  Left = 122
                  ExplicitLeft = 122
                end
                inherited SavePresetButton: TBitBtn
                  Left = 122
                  ExplicitLeft = 122
                end
              end
            end
            object SHINE_Bright_SettingsPanel: TPanel
              Left = 4
              Top = 58
              Width = 181
              Height = 225
              Anchors = [akLeft, akBottom]
              BevelOuter = bvNone
              TabOrder = 1
              inline SHINE_Bright_SpatialContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Hint = 
                  'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                  'or use arrow keys to change'
                TabOrder = 9
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 103
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 77
                  Caption = 'Spatial Contrast'
                  ExplicitTop = 2
                  ExplicitWidth = 77
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 3
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_DisabledSpatialContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 8
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 77
                  Caption = 'Spatial Contrast'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 77
                end
                inherited TrackBar: TTrackBar
                  Tag = -1
                  Width = 129
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_TemporalContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 121
                Width = 173
                Height = 41
                Hint = 
                  'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                  'or use arrow keys to change'
                TabOrder = 1
                ExplicitLeft = 4
                ExplicitTop = 121
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 103
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 89
                  Caption = 'Temporal Contrast'
                  ExplicitTop = 2
                  ExplicitWidth = 89
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 3
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_DisabledTemporalContrastTrackEdit: TTrackEditFrame
                Left = 4
                Top = 121
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 3
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 121
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 89
                  Caption = 'Temporal Contrast'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 89
                end
                inherited TrackBar: TTrackBar
                  Tag = -1
                  Width = 129
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_MotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 161
                Width = 173
                Height = 41
                Hint = 'Set a parameter to fix more or less suspected debris'
                TabOrder = 0
                ExplicitLeft = 4
                ExplicitTop = 161
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Width = 82
                  Caption = 'Motion Tolerance'
                  ParentFont = False
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 4
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Tag = -1
                  end
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited RedGreenTick: TColorPanel
                  TabOrder = 3
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_MaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Hint = 
                  'Set the maximum size of detected debris'#13#10'Press 1 to select'#13#10'Move' +
                  ' mouse wheel or use arrow keys to change'
                TabOrder = 6
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 123
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 42
                  Caption = 'Max Size'
                  ExplicitTop = 2
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 1
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_MinSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Hint = 
                  'Set the minimum size of detected debtis'#13#10'Press 2 to select'#13#10'Move' +
                  ' mouse wheel or use arrow keys to change'
                TabOrder = 7
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 38
                  Caption = 'Min Size'
                  ExplicitTop = 2
                  ExplicitWidth = 38
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Min = 1
                  TabOrder = 2
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_DisabledMinSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBtnShadow
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Left = 6
                  Top = 3
                  Width = 38
                  Caption = 'Min Size'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitLeft = 6
                  ExplicitTop = 3
                  ExplicitWidth = 38
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Enabled = False
                  Position = 10
                  SliderVisible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Tag = -1
                    Text = '10'
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_DisabledMaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 4
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Left = 6
                  Top = 3
                  Width = 42
                  Caption = 'Max Size'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitLeft = 6
                  ExplicitTop = 3
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Enabled = False
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Bright_DisabledMotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 161
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 2
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 161
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 82
                  Caption = 'Motion Tolerance'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Position = 10
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Text = '10'
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
            end
            object SHINE_Bright_ResetButton: TBitBtn
              Left = 84
              Top = 0
              Width = 89
              Height = 29
              Hint = 'Reset all parameters to current defaults (Ctrl+Shift+C)'
              Anchors = [akLeft, akBottom]
              Caption = ' Reset Defaults'
              TabOrder = 0
              OnClick = SHINE_Bright_ResetButtonClick
            end
            object SHINE_Bright_DisabledPresetsPanel: TPanel
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              BevelOuter = bvNone
              TabOrder = 3
              object SHINE_Bright_DisabledPresetButton1: TSpeedButton
                Left = 0
                Top = 0
                Width = 23
                Height = 22
                Caption = '1'
                Enabled = False
              end
              object SHINE_Bright_DisabledPresetButton2: TSpeedButton
                Left = 29
                Top = 0
                Width = 23
                Height = 22
                Caption = '2'
                Enabled = False
              end
              object SHINE_Bright_DisabledPresetButton3: TSpeedButton
                Left = 58
                Top = 0
                Width = 23
                Height = 22
                Caption = '3'
                Enabled = False
              end
              object SHINE_Bright_DisabledPresetButton4: TSpeedButton
                Left = 87
                Top = 0
                Width = 23
                Height = 22
                Caption = '4'
                Enabled = False
              end
              object SHINE_Bright_DisabledSavePresetButton: TSpeedButton
                Left = 122
                Top = 0
                Width = 35
                Height = 22
                Caption = 'Save'
                Enabled = False
              end
            end
          end
          object SHINE_Alpha_ControlPanel: TPanel
            Left = 20
            Top = 332
            Width = 242
            Height = 219
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            DesignSize = (
              242
              219)
            object SHINE_Alpha_EnableButton: TSpeedButton
              Left = 16
              Top = 0
              Width = 60
              Height = 29
              AllowAllUp = True
              GroupIndex = 111
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D48484
                85000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4848485000000000000000000C8D0D4C8D0D4C8D0D4C8D0D4848485FFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5E5E5E5EC8
                D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525E5E5EC8D0D4C8D0D4C8D0D4
                848485FFFFFFFFFFFFFFFFFFFFFFFF000000C8D0D4C8D0D4848485FFFFFFFFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525252525E
                5E5EC8D0D4C8D0D45E5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                52525E5E5E5E5E5E5252525252525252525252525E5E5EC8D0D4C8D0D4848485
                FFFFFFFFFFFFFFFFFF848485FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                5252525252525252525252525252C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4FFFFFF
                FFFFFFFFFFFF848485C8D0D4848485FFFFFFFFFFFFFFFFFFFFFFFF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4FFFFFF
                FFFFFF000000C8D0D4C8D0D4C8D0D4000000FFFFFFFFFFFFFFFFFF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D45E
                5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4FFFFFF
                FFFFFFFFFFFF000000C8D0D4000000FFFFFFFFFFFFFFFFFFFFFFFF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                00000000000000C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                52525252525252525252525252525E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8D0D4C8D0D4FFFFFFFFFFFFFFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                5252C8D0D4C8D0D45252525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                D8D9D9FFFFFFFFFFFFFFFFFFFFFFFFC8D0D4C8D0D4C8D0D4C8D0D4FFFFFFFFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D45E5E5E525252525252525252525252C8
                D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4FFFF
                FFFFFFFF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4}
              NumGlyphs = 2
              OnClick = SHINE_Alpha_EnableButtonClick
            end
            object SHINE_Alpha_RedEyeButton: TSpeedButton
              Left = 185
              Top = 113
              Width = 23
              Height = 22
              Hint = 'Show or hide defect map (Ctrl+T)'
              AllowAllUp = True
              GroupIndex = 112
              Enabled = False
              Flat = True
              Glyph.Data = {
                2A0C0000424D2A0C0000000000003600000028000000440000000F0000000100
                180000000000F40B0000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
                000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
                0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000000000000000000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F
                7F7FB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241CED241CED241CED241C
                ED000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
                000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241C
                ED241CED241CED241CED241CED241CED000000000000000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF241CED241CED
                FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CEDFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F
                7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF0000
                00000000000000241CED241CED241CED000000000000241CED241CED241CED00
                0000000000000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF
                FFFFFF241CED241CED241CED241CEDFFFFFFFFFFFFFFFFFF0000000000000000
                00FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CED241CED24
                1CEDFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FB0AEFF
                B0AEFFB0AEFF7F7F7F7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F
                7F7F7F7FFFFFFF000000000000000000241CED241CED241CED00000000000000
                0000000000241CED241CED241CED000000000000000000FFFFFFFFFFFF000000
                000000000000FFFFFFFFFFFFFFFFFF241CED241CED241CED241CEDFFFFFFFFFF
                FFFFFFFF000000000000000000FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFF241CED241CED241CED241CEDFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7F7F7F7FB0AE
                FFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFF000000000000000000241CED24
                1CED241CED000000000000000000000000241CED241CED241CED000000000000
                000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF241C
                ED241CEDFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CEDFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AE
                FF7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFF000000000000000000241CED241CED241CED000000000000241CED241CED
                241CED000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F
                7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241CED241CED
                241CED241CED241CED241CED000000000000000000FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0
                AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                000000000000000000241CED241CED241CED241CED000000000000000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000
                00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 4
              OnClick = SHINE_Alpha_RedEyeButtonClick
            end
            object SHINE_Alpha_ShowAlphaButton: TSpeedButton
              Left = 210
              Top = 113
              Width = 23
              Height = 22
              Hint = 'Show or hide defect map (Ctrl+T)'
              AllowAllUp = True
              GroupIndex = 122
              Enabled = False
              Flat = True
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D48484
                85000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4848485000000000000000000C8D0D4C8D0D4C8D0D4C8D0D484848500F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5E5E5E5EC8
                D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525E5E5EC8D0D4C8D0D4C8D0D4
                84848500F2FF00F2FF00F2FF00F2FF000000C8D0D4C8D0D484848500F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525252525E
                5E5EC8D0D4C8D0D45E5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00000000000000F2FF00F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                52525E5E5E5E5E5E5252525252525252525252525E5E5EC8D0D4C8D0D4848485
                00F2FF00F2FF00F2FF84848500F2FF00F2FF00F2FF00F2FF00F2FF00F2FFC8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                5252525252525252525252525252C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF00F2FF848485C8D0D484848500F2FF00F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF000000C8D0D4C8D0D4C8D0D400000000F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D45E
                5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF00F2FF000000C8D0D400000000F2FF00F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00000000F2FF00F2FF00F2FF00F2FF00F2FF00F2FF0000
                00000000000000C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                52525252525252525252525252525E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00F2FF00F2FF00F2FFC8D0D4C8D0D400F2FF00F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                5252C8D0D4C8D0D45252525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                D8D9D900F2FF00F2FF00F2FF00F2FFC8D0D4C8D0D4C8D0D4C8D0D400F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D45E5E5E525252525252525252525252C8
                D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4}
              NumGlyphs = 2
              OnClick = SHINE_Alpha_ShowAlphaButtonClick
            end
            inline SHINE_Alpha_PresetsFrame: TPresetsFrame
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              Anchors = [akLeft, akBottom]
              AutoSize = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              ExplicitLeft = 16
              ExplicitTop = 36
              ExplicitWidth = 157
              inherited PresetPanel: TPanel
                Width = 157
                Anchors = [akLeft, akRight]
                AutoSize = False
                ExplicitWidth = 157
                inherited PresetLabel: TLabel
                  Left = 70
                  Top = 0
                  Width = 4
                  Caption = ''
                  ExplicitLeft = 70
                  ExplicitTop = 0
                  ExplicitWidth = 4
                end
                inherited PresetButton1: TSpeedButton
                  Left = 0
                  ExplicitLeft = 0
                end
                inherited PresetButton2: TSpeedButton
                  Left = 29
                  ExplicitLeft = 29
                end
                inherited PresetButton3: TSpeedButton
                  Left = 58
                  ExplicitLeft = 58
                end
                inherited PresetButton4: TSpeedButton
                  Left = 87
                  ExplicitLeft = 87
                end
                inherited DisabledSaveButton: TBitBtn
                  Left = 122
                  ExplicitLeft = 122
                end
                inherited SavePresetButton: TBitBtn
                  Left = 122
                  ExplicitLeft = 122
                end
              end
            end
            object SHINE_Alpha_SettingsPanel: TPanel
              Left = 4
              Top = 58
              Width = 181
              Height = 127
              Anchors = [akLeft, akBottom]
              BevelOuter = bvNone
              TabOrder = 1
              inline SHINE_Alpha_GrainSuppressionTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Hint = 
                  'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                  'or use arrow keys to change'
                TabOrder = 1
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 103
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 86
                  Caption = 'Grain Suppression'
                  ExplicitTop = 2
                  ExplicitWidth = 86
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 3
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Alpha_MotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Hint = 'Set a parameter to fix more or less suspected debris'
                TabOrder = 0
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Top = 28
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitTop = 28
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Width = 82
                  Caption = 'Motion Tolerance'
                  ParentFont = False
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 4
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  TabOrder = 2
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Tag = -1
                  end
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited NotifyWidget: TCheckBox
                  TabOrder = 1
                end
                inherited RedGreenTick: TColorPanel
                  TabOrder = 3
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Alpha_DisabledGrainSuppressionTrackEdit: TTrackEditFrame
                Left = 4
                Top = 41
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 3
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 41
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 86
                  Caption = 'Grain Suppression'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 86
                end
                inherited TrackBar: TTrackBar
                  Tag = -1
                  Width = 129
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Alpha_MaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Hint = 
                  'Set the maximum size of detected debris'#13#10'Press 1 to select'#13#10'Move' +
                  ' mouse wheel or use arrow keys to change'
                TabOrder = 5
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 123
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 42
                  Caption = 'Max Size'
                  ExplicitTop = 2
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  TabOrder = 1
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  TabOrder = 0
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited EditDisabledPanel: TPanel
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Alpha_DisabledMaxSizeTrackEdit: TTrackEditFrame
                Left = 4
                Top = 1
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 4
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 1
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Left = 6
                  Top = 3
                  Width = 42
                  Caption = 'Max Size'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitLeft = 6
                  ExplicitTop = 3
                  ExplicitWidth = 42
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Enabled = False
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
              inline SHINE_Alpha_DisabledMotionToleranceTrackEdit: TTrackEditFrame
                Left = 4
                Top = 81
                Width = 173
                Height = 41
                Enabled = False
                TabOrder = 2
                Visible = False
                ExplicitLeft = 4
                ExplicitTop = 81
                ExplicitWidth = 173
                ExplicitHeight = 41
                inherited MinLabel: TLabel
                  Width = 3
                  Caption = ''
                  ExplicitWidth = 3
                end
                inherited MaxLabel: TLabel
                  Left = 106
                  Width = 3
                  Caption = ''
                  ExplicitLeft = 106
                  ExplicitWidth = 3
                end
                inherited TitleLabel: TColorLabel
                  Top = 2
                  Width = 82
                  Caption = 'Motion Tolerance'
                  Font.Color = clBtnShadow
                  ParentFont = False
                  ExplicitTop = 2
                  ExplicitWidth = 82
                end
                inherited TrackBar: TTrackBar
                  Width = 129
                  Position = 10
                  SliderVisible = False
                  Visible = False
                  ExplicitWidth = 129
                end
                inherited TrackBarDisabledPanel: TPanel
                  Width = 117
                  Visible = True
                  ExplicitWidth = 117
                end
                inherited EditAlignmentPanel: TPanel
                  Left = 133
                  Height = 41
                  ExplicitLeft = 133
                  ExplicitHeight = 41
                  inherited Edit: TEdit
                    Text = '10'
                    Visible = False
                  end
                  inherited EditDisabledPanel: TPanel
                    Visible = True
                    inherited EditDisabledLabel: TLabel
                      Left = 4
                      Width = 23
                      Alignment = taLeftJustify
                      Caption = ''
                      Enabled = True
                      Font.Color = clBtnShadow
                      ExplicitLeft = 4
                      ExplicitWidth = 23
                    end
                  end
                end
                inherited TrackBarMaxPositionTick: TColorPanel
                  Left = 124
                  ExplicitLeft = 124
                end
              end
            end
            object SHINE_Alpha_ResetButton: TBitBtn
              Left = 84
              Top = 0
              Width = 89
              Height = 29
              Hint = 'Reset all parameters to current defaults (Ctrl+Shift+C)'
              Anchors = [akLeft, akBottom]
              Caption = ' Reset Defaults'
              TabOrder = 0
              OnClick = SHINE_Alpha_ResetButtonClick
            end
            object SHINE_Alpha_DisabledPresetsPanel: TPanel
              Left = 16
              Top = 36
              Width = 157
              Height = 22
              BevelOuter = bvNone
              TabOrder = 3
              object SHINE_Alpha_DisabledPresetButton2: TSpeedButton
                Left = 0
                Top = 0
                Width = 23
                Height = 22
                Caption = '1'
                Enabled = False
              end
              object SHINE_Alpha_DisabledPresetButton3: TSpeedButton
                Left = 29
                Top = 0
                Width = 23
                Height = 22
                Caption = '2'
                Enabled = False
              end
              object SHINE_Alpha_DisabledPresetButton4: TSpeedButton
                Left = 58
                Top = 0
                Width = 23
                Height = 22
                Caption = '3'
                Enabled = False
              end
              object SHINE_Alpha_DisabledSavePresetButton: TSpeedButton
                Left = 87
                Top = 0
                Width = 23
                Height = 22
                Caption = '4'
                Enabled = False
              end
              object SHINE_Alpha_DisabledPresetButton1: TSpeedButton
                Left = 122
                Top = 0
                Width = 35
                Height = 22
                Caption = 'Save'
                Enabled = False
              end
            end
          end
          object SHINE_LowerSeparator: TPanel
            Left = 8
            Top = 323
            Width = 451
            Height = 2
            TabOrder = 3
          end
          object SHINE_UpperSeparator: TPanel
            Left = 8
            Top = 37
            Width = 451
            Height = 2
            TabOrder = 4
          end
          object SHINE_GangBrightAndDarkCheckBox: TCheckBox
            Left = 216
            Top = 52
            Width = 52
            Height = 17
            Caption = 'Gang'
            TabOrder = 5
            OnClick = SHINE_GangBrightAndDarkCheckBoxClick
          end
        end
        object SHINE_AnalyzeButtonPanel: TPanel
          Left = 8
          Top = 5
          Width = 455
          Height = 26
          BevelOuter = bvNone
          TabOrder = 1
          object SHINE_AnalyzeShotButton: TSpeedButton
            Left = 64
            Top = 2
            Width = 41
            Height = 21
            Hint = 
              'Run analysis preprocessing on the shot that contains the display' +
              'ed frame'
            Caption = 'Shot'
            OnClick = SHINE_AnalyzeButtonClick
          end
          object SHINE_AnalyzeClipButton: TSpeedButton
            Left = 185
            Top = 2
            Width = 41
            Height = 21
            Hint = 'Run analysis preprocessing on the full clip'
            Caption = 'Clip'
            OnClick = SHINE_AnalyzeButtonClick
          end
          object SHINE_AnalyzeMarkedButton: TSpeedButton
            Left = 107
            Top = 2
            Width = 76
            Height = 21
            Hint = 'Run analysis preprocessing on all shots in the marked range'
            Caption = 'Marked Shots'
            OnClick = SHINE_AnalyzeButtonClick
          end
          object AnalyzeLabel: TLabel
            Left = 3
            Top = 4
            Width = 58
            Height = 17
            Caption = 'Analyze:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ResetLabel: TLabel
            Left = 241
            Top = 4
            Width = 44
            Height = 17
            Caption = 'Reset:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SHINE_ResetShotButton: TSpeedButton
            Left = 287
            Top = 2
            Width = 41
            Height = 21
            Hint = 'Discard analysis for the shot that contains the displayed frame'
            Caption = 'Shot'
            OnClick = SHINE_ResetAnalysisButtonClick
          end
          object SHINE_ResetMarkedButton: TSpeedButton
            Left = 330
            Top = 2
            Width = 76
            Height = 21
            Hint = 'Discard analysis for all shots in the marked range'
            Caption = 'Marked Shots'
            OnClick = SHINE_ResetAnalysisButtonClick
          end
          object SHINE_ResetClipButton: TSpeedButton
            Left = 408
            Top = 2
            Width = 41
            Height = 21
            Hint = 'Discard analysis for the full clip'
            Caption = 'Clip'
            OnClick = SHINE_ResetAnalysisButtonClick
          end
        end
      end
      object AF_TabSheet: TTabSheet
        Caption = 'AutoFilter'
        ImageIndex = 2
        object AutoFilterTabControlPanel: TColorPanel
          Left = 0
          Top = 0
          Width = 512
          Height = 720
          BevelOuter = bvNone
          Color = 6974058
          Constraints.MaxHeight = 720
          Constraints.MaxWidth = 512
          Constraints.MinHeight = 720
          Constraints.MinWidth = 512
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Locked = True
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object AF_NoFilterSelectedPanel: TGroupBox
            Left = 2
            Top = 240
            Width = 468
            Height = 283
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 2
            object AF_Label16: TLabel
              Left = 138
              Top = 148
              Width = 204
              Height = 25
              Caption = ' Select a filter to view details '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Segoe UI'
              Font.Style = [fsItalic]
              GlowSize = 2
              ParentFont = False
            end
          end
          object AF_FilterDetailsPanel: TGroupBox
            Left = 2
            Top = 240
            Width = 468
            Height = 283
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              468
              283)
            object AF_FilterNameLabel: TLabel
              Left = 15
              Top = 7
              Width = 184
              Height = 19
              Anchors = [akTop]
              Caption = 'Filter Name Goes Here'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object AF_FilterSetNameLabel: TLabel
              Left = 15
              Top = 26
              Width = 126
              Height = 13
              Anchors = [akTop]
              Caption = 'Filter Set Name Goes Here'
            end
            object AF_RedEyeButton: TSpeedButton
              Left = 243
              Top = 132
              Width = 23
              Height = 22
              Hint = 'Show or hide defect map (Ctrl+T)'
              AllowAllUp = True
              GroupIndex = 112
              Enabled = False
              Flat = True
              Glyph.Data = {
                2A0C0000424D2A0C0000000000003600000028000000440000000F0000000100
                180000000000F40B0000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
                000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
                0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000000000000000000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F
                7F7FB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241CED241CED241CED241C
                ED000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
                000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241C
                ED241CED241CED241CED241CED241CED000000000000000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF241CED241CED
                FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CEDFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F
                7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF0000
                00000000000000241CED241CED241CED000000000000241CED241CED241CED00
                0000000000000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF
                FFFFFF241CED241CED241CED241CEDFFFFFFFFFFFFFFFFFF0000000000000000
                00FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CED241CED24
                1CEDFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FB0AEFF
                B0AEFFB0AEFF7F7F7F7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F
                7F7F7F7FFFFFFF000000000000000000241CED241CED241CED00000000000000
                0000000000241CED241CED241CED000000000000000000FFFFFFFFFFFF000000
                000000000000FFFFFFFFFFFFFFFFFF241CED241CED241CED241CEDFFFFFFFFFF
                FFFFFFFF000000000000000000FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFF241CED241CED241CED241CEDFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7F7F7F7FB0AE
                FFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFF000000000000000000241CED24
                1CED241CED000000000000000000000000241CED241CED241CED000000000000
                000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF241C
                ED241CEDFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF241CED241CEDFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AE
                FF7F7F7F7F7F7FB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFF000000000000000000241CED241CED241CED000000000000241CED241CED
                241CED000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0AEFFB0AEFFB0AEFF7F7F7F7F7F7F7F
                7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000241CED241CED
                241CED241CED241CED241CED000000000000000000FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FB0AEFFB0AEFFB0AEFFB0
                AEFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                000000000000000000241CED241CED241CED241CED000000000000000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000
                00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 4
              OnClick = AF_RedEyeButtonClick
            end
            object AF_ShowAlphaButton: TSpeedButton
              Left = 243
              Top = 104
              Width = 23
              Height = 22
              Hint = 'Show or hide defect map (Ctrl+T)'
              AllowAllUp = True
              GroupIndex = 122
              Enabled = False
              Flat = True
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D48484
                85000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4848485000000000000000000C8D0D4C8D0D4C8D0D4C8D0D484848500F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5E5E5E5E5E5E5E5E5EC8
                D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525E5E5EC8D0D4C8D0D4C8D0D4
                84848500F2FF00F2FF00F2FF00F2FF000000C8D0D4C8D0D484848500F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525252525E
                5E5EC8D0D4C8D0D45E5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00000000000000F2FF00F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                52525E5E5E5E5E5E5252525252525252525252525E5E5EC8D0D4C8D0D4848485
                00F2FF00F2FF00F2FF84848500F2FF00F2FF00F2FF00F2FF00F2FF00F2FFC8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                5252525252525252525252525252C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF00F2FF848485C8D0D484848500F2FF00F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF000000C8D0D4C8D0D4C8D0D400000000F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D45E
                5E5E5252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2FF
                00F2FF00F2FF000000C8D0D400000000F2FF00F2FF00F2FF00F2FF000000C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D45E5E5E52
                52525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00000000F2FF00F2FF00F2FF00F2FF00F2FF00F2FF0000
                00000000000000C8D0D4C8D0D45E5E5E5252525252525252525E5E5E52525252
                52525252525252525252525252525E5E5E5E5E5E5E5E5EC8D0D4C8D0D4C8D0D4
                00F2FF00F2FF00F2FF00F2FF00F2FF00F2FFC8D0D4C8D0D400F2FF00F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D452525252525252525252525252525252
                5252C8D0D4C8D0D45252525252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                D8D9D900F2FF00F2FF00F2FF00F2FFC8D0D4C8D0D4C8D0D4C8D0D400F2FF00F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D45E5E5E525252525252525252525252C8
                D0D4C8D0D4C8D0D4C8D0D45252525252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400F2
                FF00F2FF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D45252525252525E5E5EC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4}
              NumGlyphs = 2
              OnClick = AF_ShowAlphaButtonClick
            end
            object AF_GreenEyeButton: TSpeedButton
              Left = 243
              Top = 132
              Width = 23
              Height = 22
              Hint = 'Preview (D key)'
              AllowAllUp = True
              GroupIndex = 111
              Flat = True
              Glyph.Data = {
                2A0C0000424D2A0C0000000000003600000028000000440000000F0000000100
                180000000000F40B0000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
                000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
                0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000000000000000000000000000000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F
                7F7FAFF58DAFF58DAFF58DAFF58D7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000004CB1224CB1224CB1224CB1
                22000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
                000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFF
                FFFF7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF58DAFF58DAFF58DAFF58D7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000004CB1
                224CB1224CB1224CB1224CB1224CB122000000000000000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFAFF58DAFF58D
                FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFFAFF58DAFF58DFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF58D7F7F7F
                7F7F7FAFF58DAFF58DAFF58D7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF0000
                000000000000004CB1224CB1224CB1220000000000004CB1224CB1224CB12200
                0000000000000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF
                FFFFFFAFF58DAFF58DAFF58DAFF58DFFFFFFFFFFFFFFFFFF0000000000000000
                00FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFAFF58DAFF58DAFF58DAF
                F58DFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FAFF58D
                AFF58DAFF58D7F7F7F7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF58D7F7F7F7F7F
                7F7F7F7FFFFFFF0000000000000000004CB1224CB1224CB12200000000000000
                00000000004CB1224CB1224CB122000000000000000000FFFFFFFFFFFF000000
                000000000000FFFFFFFFFFFFFFFFFFAFF58DAFF58DAFF58DAFF58DFFFFFFFFFF
                FFFFFFFF000000000000000000FFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFFAFF58DAFF58DAFF58DAFF58DFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF58D7F7F7F7F7F7F7F7F7F7F7F7FAFF5
                8DAFF58DAFF58D7F7F7F7F7F7F7F7F7FFFFFFF0000000000000000004CB1224C
                B1224CB1220000000000000000000000004CB1224CB1224CB122000000000000
                000000FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFAFF5
                8DAFF58DFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFF7F
                7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFAFF58DAFF58DFFFFFFFFFFFFFFFFFF
                7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF5
                8D7F7F7F7F7F7FAFF58DAFF58DAFF58D7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFF
                FFFF0000000000000000004CB1224CB1224CB1220000000000004CB1224CB122
                4CB122000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7FAFF58DAFF58DAFF58DAFF58DAFF58DAFF58D7F7F7F7F7F7F7F
                7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000004CB1224CB122
                4CB1224CB1224CB1224CB122000000000000000000FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
                0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F
                7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7FAFF58DAFF58DAFF58DAF
                F58D7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000000000000000004CB1224CB1224CB1224CB122000000000000000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000
                00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F
                7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7F7F7F7F7F7F7FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 4
              Visible = False
              OnClick = AF_GreenEyeButtonClick
            end
            object AF_DebrisContrastRadioPanel: TGroupBox
              Left = 14
              Top = 67
              Width = 209
              Height = 29
              Anchors = [akTop]
              TabOrder = 7
              TabStop = True
              DesignSize = (
                209
                29)
              object AF_DebrisContrastLabel: TLabel
                Left = 10
                Top = 8
                Width = 79
                Height = 13
                Caption = 'Debris Contrast:'
              end
              object AF_DebrisContrastBrightRadioButton: TRadioButton
                Left = 108
                Top = 7
                Width = 49
                Height = 17
                Hint = 'Find light debris'
                Anchors = [akTop, akRight]
                Caption = 'Bright'
                Checked = True
                TabOrder = 0
                TabStop = True
                OnClick = AF_DebrisContrastChanged
                OnEnter = AF_DebrisContrastRadioButtonEnter
                OnExit = AF_DebrisContrastRadioButtonExit
              end
              object AF_DebrisContrastDarkRadioButton: TRadioButton
                Left = 160
                Top = 7
                Width = 41
                Height = 17
                Hint = 'Find dark debris'
                Anchors = [akTop, akRight]
                Caption = 'Dark'
                TabOrder = 1
                TabStop = True
                OnClick = AF_DebrisContrastChanged
                OnEnter = AF_DebrisContrastRadioButtonEnter
                OnExit = AF_DebrisContrastRadioButtonExit
              end
            end
            object AF_DetectChannelShadowPanel: TGroupBox
              Left = 43
              Top = 111
              Width = 133
              Height = 26
              Anchors = [akTop]
              TabOrder = 20
              object AF_DetectRRadioButton: TRadioButton
                Left = 8
                Top = 4
                Width = 28
                Height = 17
                Caption = 'R'
                TabOrder = 1
                OnClick = AF_DetectRadioButtonChanged
                OnEnter = AF_DetectRRadioButtonEnter
                OnExit = AF_DetectRRadioButtonExit
              end
              object AF_DetectGRadioButton: TRadioButton
                Left = 37
                Top = 4
                Width = 28
                Height = 17
                Caption = 'G'
                Checked = True
                TabOrder = 2
                TabStop = True
                OnClick = AF_DetectRadioButtonChanged
                OnEnter = AF_DetectGRadioButtonEnter
                OnExit = AF_DetectGRadioButtonExit
              end
              object AF_DetectBRadioButton: TRadioButton
                Left = 66
                Top = 4
                Width = 28
                Height = 17
                Caption = 'B'
                TabOrder = 3
                OnClick = AF_DetectRadioButtonChanged
                OnEnter = AF_DetectBRadioButtonEnter
                OnExit = AF_DetectBRadioButtonExit
              end
              object AF_DetectARadioButton: TRadioButton
                Left = 95
                Top = 4
                Width = 28
                Height = 17
                Caption = 'A'
                TabOrder = 0
                OnClick = AF_DetectRadioButtonChanged
                OnEnter = AF_DetectARadioButtonEnter
                OnExit = AF_DetectARadioButtonExit
              end
            end
            object AF_ProcessChannelsShadowPanel: TGroupBox
              Left = 43
              Top = 138
              Width = 96
              Height = 27
              Anchors = [akTop]
              TabOrder = 21
              object AF_ProcessGShadowCheckBox: TCheckBox
                Left = 37
                Top = 4
                Width = 28
                Height = 17
                TabStop = False
                Caption = 'G'
                Checked = True
                State = cbChecked
                TabOrder = 1
                OnClick = AF_ProcessGShadowCheckBoxClick
                OnEnter = AF_ProcessGShadowCheckBoxEnter
                OnExit = AF_ProcessGShadowCheckBoxExit
              end
              object AF_ProcessRShadowCheckBox: TCheckBox
                Left = 8
                Top = 4
                Width = 28
                Height = 17
                Caption = 'R'
                Checked = True
                State = cbChecked
                TabOrder = 0
                OnClick = AF_ProcessRShadowCheckBoxClick
                OnEnter = AF_ProcessRShadowCheckBoxEnter
                OnExit = AF_ProcessRShadowCheckBoxExit
              end
              object AF_ProcessBShadowCheckBox: TCheckBox
                Left = 66
                Top = 4
                Width = 28
                Height = 17
                TabStop = False
                Caption = 'B'
                Checked = True
                State = cbChecked
                TabOrder = 2
                OnClick = AF_ProcessBShadowCheckBoxClick
                OnEnter = AF_ProcessBShadowCheckBoxEnter
                OnExit = AF_ProcessBShadowCheckBoxExit
              end
            end
            object AF_UsePrecomputedMotionsCheckBox: TCheckBox
              Left = 16
              Top = 320
              Width = 206
              Height = 17
              Anchors = [akTop]
              Caption = 'Use Precomputed Motions if Available'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 12
              Visible = False
            end
            object AF_UseGrainFilterCheckBox: TCheckBox
              Left = 27
              Top = 254
              Width = 145
              Height = 17
              Hint = 'Avoid detecting grain as defects'
              Anchors = [akTop]
              Caption = 'Enable Grain Suppression'
              Checked = True
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBtnShadow
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              State = cbChecked
              TabOrder = 13
              Visible = False
              OnClick = AF_UseGrainFilterCheckBoxClick
            end
            inline AF_DustMaxSizeTrackEdit: TTrackEditFrame
              Left = 19
              Top = 208
              Width = 173
              Height = 41
              Hint = 
                'Set the max size to designate debris as dust'#13#10'Press 8 to select'#13 +
                #10'Move mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 9
              ExplicitLeft = 19
              ExplicitTop = 208
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Top = 28
                Width = 3
                Caption = ''
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Top = 28
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Width = 67
                Caption = 'Dust Max Size'
                ParentFont = False
                ExplicitWidth = 67
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Max = 50
                Position = 0
                TabOrder = 6
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Tag = -1
                  Text = '0'
                end
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited RedGreenTick: TColorPanel
                TabOrder = 3
              end
              inherited TrackBarMinPositionTick: TColorPanel
                TabOrder = 4
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                TabOrder = 5
              end
            end
            object AF_MultipleFiltersSelectedPanel: TPanel
              Left = 8
              Top = 40
              Width = 192
              Height = 22
              Anchors = [akTop]
              BevelOuter = bvNone
              TabOrder = 22
              Visible = False
              object AF_LBracketLabel: TLabel
                Left = 2
                Top = 0
                Width = 16
                Height = 19
                Caption = ' [ '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object AF_MultipleFilterSelectedLabel: TLabel
                Left = 17
                Top = 4
                Width = 159
                Height = 17
                Caption = 'Multiple Filters Selected '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = [fsItalic]
                ParentFont = False
              end
              object AF_RBracketLabel: TLabel
                Left = 175
                Top = 0
                Width = 16
                Height = 19
                Caption = ' ] '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
            end
            object AF_ChannelPanel: TGroupBox
              Left = 14
              Top = 104
              Width = 209
              Height = 70
              Anchors = [akTop]
              Color = 6974058
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBackground = False
              ParentColor = False
              ParentFont = False
              TabOrder = 14
              object AF_DetectPanel: TPanel
                Left = 4
                Top = 3
                Width = 201
                Height = 29
                BevelOuter = bvNone
                TabOrder = 1
                object AF_DetectBlueButton: TSpeedButton
                  Left = 176
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Find debris in the BLUE channel'
                  GroupIndex = 1
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_DetectChannelChanged
                end
                object AF_DetectGreenButton: TSpeedButton
                  Left = 155
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Find debris in the GREEN channel'
                  GroupIndex = 1
                  Down = True
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_DetectChannelChanged
                end
                object AF_DetectRedButton: TSpeedButton
                  Left = 134
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Find debris in the RED channel'
                  GroupIndex = 1
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_DetectChannelChanged
                end
                object AF_DetectAlphaButton: TSpeedButton
                  Left = 113
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Use Alpha channel to find debris'
                  GroupIndex = 1
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000120B0000120B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A3675B43675B43675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A3675B43675B43675B43675B46A6A6A6A6A6A6A6A
                    6A6A6A6A3675B40080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A5E5E5E5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A3675B40080FF0080FF0080FF0080FF3675B46A6A6A6A6A
                    6A3675B40080FF0080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A5E5E5E5252525252525252525252525E5E5E6A6A6A6A6A6A5E5E5E525252
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A0080FF0080FF0080FF0080FF0080FF0080FF3675B43675
                    B40080FF0080FF0080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A5252525252525252525252525252525252525E5E5E5E5E5E525252525252
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A3675B40080FF0080FF0080FF3675B40080FF0080FF0080FF0080
                    FF0080FF0080FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
                    5E5E5252525252525252525E5E5E525252525252525252525252525252525252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A0080FF0080FF0080FF3675B46A6A6A3675B40080FF0080FF0080
                    FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
                    52525252525252525E5E5E6A6A6A5E5E5E5252525252525252525252525E5E5E
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A0080FF0080FF3675B46A6A6A6A6A6A6A6A6A3675B40080FF0080
                    FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
                    52525252525E5E5E6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525E5E5E
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A0080FF0080FF0080FF3675B46A6A6A3675B40080FF0080FF0080
                    FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
                    52525252525252525E5E5E6A6A6A5E5E5E5252525252525252525252525E5E5E
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A3675B40080FF0080FF0080FF3675B40080FF0080FF0080FF0080
                    FF0080FF0080FF3675B43675B43675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
                    5E5E5252525252525252525E5E5E525252525252525252525252525252525252
                    5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A0080FF0080FF0080FF0080FF0080FF0080FF6A6A6A6A6A
                    6A0080FF0080FF0080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A5252525252525252525252525252525252526A6A6A6A6A6A525252525252
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A3675B40080FF0080FF0080FF0080FF6A6A6A6A6A6A6A6A
                    6A6A6A6A0080FF0080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A5E5E5E5252525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A525252
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A0080FF0080FF3675B46A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_DetectChannelChanged
                end
                object AF_HilitedDetectRedButton: TSpeedButton
                  Left = 134
                  Top = 3
                  Width = 21
                  Height = 21
                  GroupIndex = 2
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
                    1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
                    1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
                    1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
                    241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
                    241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED
                    241CED241CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF241CED241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241C
                    ED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
                    1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
                    1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
                    1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
                    241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
                    241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_DetectChannelChanged
                end
                object AF_HilitedDetectGreenButton: TSpeedButton
                  Left = 155
                  Top = 4
                  Width = 21
                  Height = 21
                  GroupIndex = 2
                  Down = True
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1
                    224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_DetectChannelChanged
                end
                object AF_HilitedDetectBlueButton: TSpeedButton
                  Left = 176
                  Top = 4
                  Width = 21
                  Height = 21
                  GroupIndex = 2
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF00
                    00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_DetectChannelChanged
                end
                object AF_HilitedDetectAlphaButton: TSpeedButton
                  Left = 113
                  Top = 4
                  Width = 21
                  Height = 21
                  GroupIndex = 2
                  Flat = True
                  Glyph.Data = {
                    96090000424D9609000000000000360000002800000028000000140000000100
                    18000000000060090000120B0000120B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF54B4CF54B4CF54B4CF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF54B4CF54B4CF54B4CF54B4CF00F2FF00F2FF00F2FF00F2FF54B4CF0080FF00
                    80FF54B4CF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
                    5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E
                    5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF54B4CF0080FF0080FF00
                    80FF0080FF54B4CF00F2FF00F2FF54B4CF0080FF0080FF0080FF54B4CF00F2FF
                    00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252
                    525E5E5E6A6A6A6A6A6A5E5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF00F2FF0080FF0080FF0080FF0080FF0080FF0080FF
                    54B4CF54B4CF0080FF0080FF0080FF0080FF54B4CF00F2FF00F2FF6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A5252525252525252525252525252525252525E5E5E5E
                    5E5E5252525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF54B4CF0080FF0080FF0080FF54B4CF0080FF0080FF0080FF0080FF0080
                    FF0080FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A5E
                    5E5E5252525252525252525E5E5E525252525252525252525252525252525252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF0080FF0080
                    FF0080FF54B4CF00F2FF54B4CF0080FF0080FF0080FF0080FF54B4CF00F2FF00
                    F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A525252525252525252
                    5E5E5E6A6A6A5E5E5E5252525252525252525252525E5E5E6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF0080FF0080FF54B4CF00F2FF00
                    F2FF00F2FF54B4CF0080FF0080FF0080FF54B4CF00F2FF00F2FF00F2FF00F2FF
                    00F2FF6A6A6A6A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A
                    6A5E5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF0080FF0080FF0080FF54B4CF00F2FF54B4CF0080FF
                    0080FF0080FF0080FF54B4CF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A
                    6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A5E5E5E52525252525252
                    52525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF54B4CF0080FF0080FF0080FF54B4CF0080FF0080FF0080FF0080FF0080
                    FF0080FF54B4CF54B4CF54B4CF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A5E
                    5E5E5252525252525252525E5E5E525252525252525252525252525252525252
                    5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF0080
                    FF0080FF0080FF0080FF0080FF0080FF00F2FF00F2FF0080FF0080FF0080FF00
                    80FF54B4CF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A525252525252
                    5252525252525252525252526A6A6A6A6A6A5252525252525252525252525E5E
                    5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF54B4CF0080FF0080FF00
                    80FF0080FF00F2FF00F2FF00F2FF00F2FF0080FF0080FF0080FF54B4CF00F2FF
                    00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252
                    526A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF0080FF0080FF54B4CF00F2FF00F2FF6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_DetectChannelChanged
                end
                object AF_DetectLabel: TLabel
                  Left = 6
                  Top = 8
                  Width = 78
                  Height = 13
                  Caption = 'Detect Channel:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
              end
              object AF_ProcessPanel: TPanel
                Left = 3
                Top = 34
                Width = 201
                Height = 29
                BevelOuter = bvNone
                TabOrder = 0
                object AF_ProcessBlueButton: TSpeedButton
                  Left = 176
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Fix the BLUE channel'
                  AllowAllUp = True
                  GroupIndex = 3
                  Down = True
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_ProcessBlueButtonClick
                end
                object AF_ProcessGreenButton: TSpeedButton
                  Left = 155
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Fix the GREEN channel'
                  AllowAllUp = True
                  GroupIndex = 2
                  Down = True
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_ProcessGreenButtonClick
                end
                object AF_ProcessRedButton: TSpeedButton
                  Left = 134
                  Top = 4
                  Width = 21
                  Height = 21
                  Hint = 'Fix the RED channel'
                  AllowAllUp = True
                  GroupIndex = 1
                  Down = True
                  Glyph.Data = {
                    B60A0000424DB60A00000000000036000000280000002A000000150000000100
                    180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
                  Margin = 0
                  NumGlyphs = 2
                  OnClick = AF_ProcessRedButtonClick
                end
                object AF_HilitedProcessRedButton: TSpeedButton
                  Left = 134
                  Top = 4
                  Width = 21
                  Height = 21
                  AllowAllUp = True
                  GroupIndex = 4
                  Down = True
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
                    1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
                    1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
                    1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
                    241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
                    241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED
                    241CED241CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF241CED241CED241CED241CED241CED241CED241CED241CED241CED241C
                    ED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241C
                    ED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241C
                    ED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
                    1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
                    1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
                    1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
                    241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
                    241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_HilitedProcessRedButtonClick
                end
                object AF_HilitedProcessGreenButton: TSpeedButton
                  Left = 155
                  Top = 4
                  Width = 21
                  Height = 21
                  AllowAllUp = True
                  GroupIndex = 5
                  Down = True
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1
                    224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1
                    224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
                    B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
                    4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_HilitedProcessGreenButtonClick
                end
                object AF_HilitedProcessBlueButton: TSpeedButton
                  Left = 176
                  Top = 4
                  Width = 21
                  Height = 21
                  AllowAllUp = True
                  GroupIndex = 6
                  Down = True
                  Flat = True
                  Glyph.Data = {
                    CE070000424DCE07000000000000360000002800000024000000120000000100
                    18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A
                    52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A
                    52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A
                    6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF00
                    00FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A
                    6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
                    6A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF00
                    00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000
                    F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A5252
                    6A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A
                    6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
                    6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
                    526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
                    6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
                    526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
                    0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
                    526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
                    6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
                    FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
                    6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
                    52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
                    00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
                    FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
                    6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
                  Margin = 0
                  NumGlyphs = 2
                  Visible = False
                  OnClick = AF_HilitedProcessBlueButtonClick
                end
                object AF_ProcessLabel: TLabel
                  Left = 6
                  Top = 8
                  Width = 88
                  Height = 13
                  Caption = 'Process Channels:'
                end
              end
              object AF_UseAlphaAsMaskCheckBox: TCheckBox
                Left = 12
                Top = 26
                Width = 97
                Height = 17
                TabStop = False
                Caption = 'Alpha Mask'
                TabOrder = 2
                Visible = False
                OnClick = AF_UseAlphaAsMaskCheckBoxClick
              end
            end
            object AF_UseHardMotionForDustCheckBox: TCheckBox
              Left = 27
              Top = 188
              Width = 145
              Height = 17
              Hint = 'Fix dust by inpainting from same frame'
              Anchors = [akTop]
              Caption = 'Use Hard Motion on Dust'
              Checked = True
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBtnShadow
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              State = cbChecked
              TabOrder = 8
              OnClick = AF_UseHardMotionForDustCheckBoxClick
            end
            object AF_HideyPanel: TPanel
              Left = 14
              Top = 255
              Width = 185
              Height = 59
              BevelOuter = bvNone
              TabOrder = 23
            end
            object AF_SaveDefaultsButton: TBitBtn
              Left = 134
              Top = 252
              Width = 103
              Height = 25
              Hint = 'Save current parameter values as new defaults (Ctrl+S)'
              Anchors = [akTop]
              Caption = 'Save All as Defaults'
              TabOrder = 11
              OnClick = AF_SaveDefaultsButtonClick
            end
            object AF_ResetButton: TBitBtn
              Left = 25
              Top = 252
              Width = 103
              Height = 25
              Hint = 'Reset all parameters to current defaults (Ctrl+Shift+C)'
              Anchors = [akTop]
              Caption = ' Reset to Defaults'
              TabOrder = 10
              OnClick = AF_ResetButtonClick
            end
            object AF_DisabledDebrisContrastRadioPanel: TGroupBox
              Left = 15
              Top = 67
              Width = 209
              Height = 29
              TabOrder = 19
              Visible = False
              DesignSize = (
                209
                29)
              object AF_Label4: TLabel
                Left = 10
                Top = 8
                Width = 79
                Height = 13
                Caption = 'Debris Contrast:'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBtnShadow
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object AF_RadioButton3: TRadioButton
                Left = 108
                Top = 7
                Width = 49
                Height = 17
                Anchors = [akTop, akRight]
                Caption = 'Bright'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBtnShadow
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
              end
              object AF_RadioButton4: TRadioButton
                Left = 160
                Top = 7
                Width = 41
                Height = 17
                Anchors = [akTop, akRight]
                Caption = 'Dark'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBtnShadow
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
            end
            inline AF_MaxSizeTrackEdit: TTrackEditFrame
              Left = 272
              Top = 2
              Width = 173
              Height = 41
              Hint = 
                'Set the maximum size of detected debris'#13#10'Press 1 to select'#13#10'Move' +
                ' mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 0
              ExplicitLeft = 272
              ExplicitTop = 2
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 123
                Width = 3
                Caption = ''
                ExplicitLeft = 123
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 42
                Caption = 'Max Size'
                ExplicitTop = 2
                ExplicitWidth = 42
              end
              inherited TrackBar: TTrackBar
                Width = 129
                TabOrder = 1
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
            end
            inline AF_DisabledMaxSizeTrackEdit: TTrackEditFrame
              Left = 272
              Top = 5
              Width = 173
              Height = 41
              Enabled = False
              TabOrder = 15
              Visible = False
              ExplicitLeft = 272
              ExplicitTop = 5
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 42
                Caption = 'Max Size'
                Font.Color = clBtnShadow
                ParentFont = False
                ExplicitTop = 2
                ExplicitWidth = 42
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Enabled = False
                SliderVisible = False
                Visible = False
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                Visible = True
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Visible = False
                end
                inherited EditDisabledPanel: TPanel
                  Visible = True
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    Caption = ''
                    Enabled = True
                    Font.Color = clBtnShadow
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                Left = 124
                ExplicitLeft = 124
              end
            end
            inline AF_MinSizeTrackEdit: TTrackEditFrame
              Left = 272
              Top = 39
              Width = 173
              Height = 41
              Hint = 
                'Set the minimum size of detected debtis'#13#10'Press 2 to select'#13#10'Move' +
                ' mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 1
              ExplicitLeft = 272
              ExplicitTop = 39
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 38
                Caption = 'Min Size'
                ExplicitTop = 2
                ExplicitWidth = 38
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Min = 1
                TabOrder = 2
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
            end
            inline AF_DisabledMinSizeTrackEdit: TTrackEditFrame
              Left = 272
              Top = 42
              Width = 173
              Height = 41
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBtnShadow
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 16
              Visible = False
              ExplicitLeft = 272
              ExplicitTop = 42
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Top = 28
                Width = 3
                Caption = ''
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Top = 28
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 38
                Caption = 'Min Size'
                Font.Color = clBtnShadow
                ParentFont = False
                ExplicitTop = 2
                ExplicitWidth = 38
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Enabled = False
                Position = 10
                SliderVisible = False
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                Visible = True
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Tag = -1
                  Text = '10'
                end
                inherited EditDisabledPanel: TPanel
                  Visible = True
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    Caption = ''
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                Left = 124
                ExplicitLeft = 124
              end
            end
            inline AF_MinContrastTrackEdit: TTrackEditFrame
              Left = 272
              Top = 76
              Width = 173
              Height = 41
              Hint = 
                'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                'or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 2
              ExplicitLeft = 272
              ExplicitTop = 76
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 103
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 94
                Caption = 'Contrast Sensitivity'
                ExplicitTop = 2
                ExplicitWidth = 94
              end
              inherited TrackBar: TTrackBar
                Width = 129
                TabOrder = 3
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
            end
            inline AF_DisabledContrastSensitivityTrackEdit: TTrackEditFrame
              Left = 272
              Top = 79
              Width = 173
              Height = 41
              Enabled = False
              TabOrder = 17
              Visible = False
              ExplicitLeft = 272
              ExplicitTop = 79
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 94
                Caption = 'Contrast Sensitivity'
                Font.Color = clBtnShadow
                ParentFont = False
                ExplicitTop = 2
                ExplicitWidth = 94
              end
              inherited TrackBar: TTrackBar
                Tag = -1
                Width = 129
                SliderVisible = False
                Visible = False
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                Visible = True
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Visible = False
                end
                inherited EditDisabledPanel: TPanel
                  Visible = True
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    Caption = ''
                    Enabled = True
                    Font.Color = clBtnShadow
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                Left = 124
                ExplicitLeft = 124
              end
            end
            inline AF_AlphaGrainSuppressionTrackEdit: TTrackEditFrame
              Left = 272
              Top = 116
              Width = 173
              Height = 41
              Hint = 'Set a parameter for the Grain Filter'
              Anchors = [akTop]
              TabOrder = 3
              ExplicitLeft = 272
              ExplicitTop = 116
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Top = 28
                Width = 3
                Caption = ''
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Top = 28
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitTop = 28
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Width = 116
                Caption = 'Alpha Grain Suppression'
                ParentFont = False
                ExplicitWidth = 116
              end
              inherited TrackBar: TTrackBar
                Width = 129
                TabOrder = 4
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Tag = -1
                end
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited RedGreenTick: TColorPanel
                TabOrder = 3
              end
            end
            inline AF_DisabledAlphaGrainSuppressionTrackEdit: TTrackEditFrame
              Left = 272
              Top = 116
              Width = 173
              Height = 41
              Enabled = False
              TabOrder = 18
              Visible = False
              ExplicitLeft = 272
              ExplicitTop = 116
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Width = 3
                Caption = ''
                ExplicitLeft = 106
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 116
                Caption = 'Alpha Grain Suppression'
                Font.Color = clBtnShadow
                ParentFont = False
                ExplicitTop = 2
                ExplicitWidth = 116
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Position = 10
                SliderVisible = False
                Visible = False
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                Visible = True
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Text = '10'
                  Visible = False
                end
                inherited EditDisabledPanel: TPanel
                  Visible = True
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    Caption = ''
                    Enabled = True
                    Font.Color = clBtnShadow
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                Left = 124
                ExplicitLeft = 124
              end
            end
            inline AF_FixToleranceTrackEdit: TTrackEditFrame
              Left = 272
              Top = 153
              Width = 173
              Height = 41
              Hint = 
                'Accept more or less debris fixes'#13#10'Press 4 to select'#13#10'Move mouse ' +
                'wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 4
              ExplicitLeft = 272
              ExplicitTop = 153
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 103
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 64
                Caption = 'Fix Tolerance'
                ExplicitTop = 2
                ExplicitWidth = 64
              end
              inherited TrackBar: TTrackBar
                Width = 129
                TabOrder = 5
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited RedGreenTick: TColorPanel
                TabOrder = 3
              end
              inherited TrackBarMinPositionTick: TColorPanel
                TabOrder = 4
              end
            end
            inline AF_AlphaMotionTrackEdit: TTrackEditFrame
              Left = 272
              Top = 190
              Width = 173
              Height = 41
              Hint = 
                'Distance to search for fix in other frames (in alpha mode)'#13#10'Pres' +
                's 6 to select'#13#10'Move mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 5
              ExplicitLeft = 272
              ExplicitTop = 190
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 103
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 32
                Caption = 'Motion'
                ExplicitTop = 2
                ExplicitWidth = 32
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Position = 10
                TabOrder = 6
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Text = '10'
                end
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited RedGreenTick: TColorPanel
                TabOrder = 3
              end
              inherited TrackBarMinPositionTick: TColorPanel
                TabOrder = 4
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                TabOrder = 5
              end
            end
            inline AF_RgbMotionTrackEdit: TTrackEditFrame
              Left = 272
              Top = 190
              Width = 173
              Height = 41
              Hint = 
                'Distance to search for fix in other frames'#13#10' (in RGB mode)Press ' +
                '5 to select'#13#10'Move mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 24
              ExplicitLeft = 272
              ExplicitTop = 190
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 103
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 32
                Caption = 'Motion'
                ExplicitTop = 2
                ExplicitWidth = 32
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Position = 10
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Text = '10'
                end
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
            end
            inline AF_CorrectionEdgeBlurTrackEdit: TTrackEditFrame
              Left = 272
              Top = 227
              Width = 173
              Height = 41
              Hint = 
                'Control edge blending of applied fixes'#13#10'Press 7 to select'#13#10'Move ' +
                'mouse wheel or use arrow keys to change'
              Anchors = [akTop]
              Color = clBtnFace
              ParentColor = False
              TabOrder = 6
              ExplicitLeft = 272
              ExplicitTop = 227
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 103
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 98
                Caption = 'Correction Edge Blur'
                ExplicitTop = 2
                ExplicitWidth = 98
              end
              inherited TrackBar: TTrackBar
                Width = 129
                Max = 10
                PageSize = 1
                Position = 2
                TabOrder = 6
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited Edit: TEdit
                  Text = '2'
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited RedGreenTick: TColorPanel
                TabOrder = 3
              end
              inherited TrackBarMinPositionTick: TColorPanel
                TabOrder = 4
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                TabOrder = 5
              end
            end
            inline AF_LuminanceThresholdTrackEdit: TTrackEditFrame
              Left = 274
              Top = 78
              Width = 173
              Height = 41
              Hint = 
                'Detect more or less debris'#13#10'Press 3 to select'#13#10'Move mouse wheel ' +
                'or use arrow keys to change'
              Anchors = [akTop]
              TabOrder = 25
              ExplicitLeft = 274
              ExplicitTop = 78
              ExplicitWidth = 173
              ExplicitHeight = 41
              inherited MinLabel: TLabel
                Width = 3
                Caption = ''
                ExplicitWidth = 3
              end
              inherited MaxLabel: TLabel
                Left = 106
                Width = 3
                Caption = ''
                ExplicitLeft = 103
                ExplicitWidth = 3
              end
              inherited TitleLabel: TColorLabel
                Top = 2
                Width = 100
                Caption = 'Luminance Threshold'
                ExplicitTop = 2
                ExplicitWidth = 100
              end
              inherited TrackBar: TTrackBar
                Width = 129
                TabOrder = 3
                ExplicitWidth = 129
              end
              inherited TrackBarDisabledPanel: TPanel
                Width = 117
                TabOrder = 2
                ExplicitWidth = 117
              end
              inherited EditAlignmentPanel: TPanel
                Left = 133
                Height = 41
                TabOrder = 0
                ExplicitLeft = 133
                ExplicitHeight = 41
                inherited EditDisabledPanel: TPanel
                  inherited EditDisabledLabel: TLabel
                    Left = 4
                    Width = 23
                    Alignment = taLeftJustify
                    ExplicitLeft = 4
                    ExplicitWidth = 23
                  end
                end
              end
              inherited NotifyWidget: TCheckBox
                TabOrder = 1
              end
              inherited TrackBarMaxPositionTick: TColorPanel
                Left = 124
                ExplicitLeft = 124
              end
            end
          end
          object AF_FilterSelectionPanel: TPanel
            Left = 0
            Top = 0
            Width = 470
            Height = 240
            BevelOuter = bvNone
            TabOrder = 1
            DesignSize = (
              470
              240)
            object AF_BasicFilterPanel: TColorPanel
              Left = 0
              Top = 27
              Width = 468
              Height = 183
              Color = 6974058
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              DesignSize = (
                468
                183)
              inline AF_FilterBankFrame: TFilterBankFrame
                Left = 2
                Top = 26
                Width = 474
                Height = 151
                Anchors = [akLeft, akTop, akRight, akBottom]
                BiDiMode = bdLeftToRight
                Constraints.MaxHeight = 151
                Constraints.MaxWidth = 474
                Constraints.MinHeight = 151
                Constraints.MinWidth = 474
                Color = 6974058
                Ctl3D = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentBackground = False
                ParentBiDiMode = False
                ParentColor = False
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                ExplicitLeft = 2
                ExplicitTop = 26
                ExplicitWidth = 474
                inherited BackgroundPanel: TColorPanel
                  Width = 468
                  ExplicitWidth = 468
                  inherited Filter1SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited Filter2SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited Filter3SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited Filter4SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited Filter5SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited Filter6SelectPanel: TColorPanel
                    Width = 466
                    ExplicitWidth = 466
                  end
                  inherited BasicFilterFrame1: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                    inherited DisabledPanel: TPanel
                      inherited NoFilterLoadedLabel: TLabel
                        Color = 4605510
                      end
                    end
                  end
                  inherited BasicFilterFrame2: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      TabOrder = 0
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                    inherited DisabledPanel: TPanel
                      TabOrder = 1
                      inherited NoFilterLoadedLabel: TLabel
                        Color = 4605510
                      end
                    end
                  end
                  inherited BasicFilterFrame3: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      TabOrder = 0
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                    inherited DisabledPanel: TPanel
                      TabOrder = 1
                      inherited NoFilterLoadedLabel: TLabel
                        Color = 4605510
                      end
                    end
                  end
                  inherited BasicFilterFrame4: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      TabOrder = 0
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                    inherited DisabledPanel: TPanel
                      TabOrder = 1
                      inherited NoFilterLoadedLabel: TLabel
                        Color = 4605510
                      end
                    end
                  end
                  inherited BasicFilterFrame5: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      TabOrder = 0
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                    inherited DisabledPanel: TPanel
                      TabOrder = 1
                      inherited NoFilterLoadedLabel: TLabel
                        Color = 4605510
                      end
                    end
                  end
                  inherited BasicFilterFrame6: TBasicFilterFrame
                    Width = 464
                    Color = 4605510
                    ExplicitWidth = 464
                    inherited BackPanel: TColorPanel
                      inherited FilterClickedNotifyWidget: TCheckBox
                        TabStop = False
                      end
                    end
                  end
                end
              end
              object AF_StupidHeaderRightEdgePanel: TPanel
                Left = 468
                Top = 5
                Width = 1
                Height = 200
                TabOrder = 2
              end
              object AF_DebrisHeaderControl: THeaderControl
                Left = 3
                Top = 5
                Width = 468
                Height = 21
                Align = alNone
                Sections = <
                  item
                    Alignment = taCenter
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Use'
                    Width = 32
                  end
                  item
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Filter Name'
                    Width = 130
                  end
                  item
                    Alignment = taCenter
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Max/Min/Dust'
                    Width = 80
                  end
                  item
                    ImageIndex = -1
                    Text = 'Cnt/Grn'
                    Width = 50
                  end
                  item
                    Alignment = taCenter
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Fix Tol'
                    Width = 54
                  end
                  item
                    Alignment = taCenter
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Motion'
                    Width = 61
                  end
                  item
                    Alignment = taCenter
                    AllowClick = False
                    ImageIndex = -1
                    Text = 'Edge Blur'
                    Width = 57
                  end>
              end
            end
            object AF_DeleteFilterButton: TBitBtn
              Left = 323
              Top = 210
              Width = 75
              Height = 21
              Hint = 'Permanently delete filters'
              Anchors = []
              Caption = 'Delete Filters'
              TabOrder = 1
              TabStop = False
              OnClick = AF_DeleteFilterButtonClick
            end
            object AF_FilterBankTopPanel: TPanel
              Left = 0
              Top = 5
              Width = 492
              Height = 23
              BevelOuter = bvNone
              BorderWidth = 1
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              object AF_FiltersLabel: TLabel
                Left = 4
                Top = 2
                Width = 40
                Height = 16
                Caption = 'Filters:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsItalic]
                ParentFont = False
              end
              object AF_SetsLabel: TLabel
                Left = 288
                Top = 2
                Width = 30
                Height = 16
                Caption = 'Sets:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsItalic]
                ParentFont = False
              end
              object AF_SelectAllButton: TSpeedButton
                Left = 56
                Top = 0
                Width = 60
                Height = 21
                Hint = 'Select all filters in the active set'
                Caption = 'Select All'
                OnClick = AF_SelectAllButtonClick
              end
              object AF_SaveSetButton: TBitBtn
                Left = 330
                Top = 0
                Width = 45
                Height = 21
                Hint = 'Save filters to a new named set'
                Caption = 'Save'
                TabOrder = 0
                TabStop = False
                OnClick = AF_SaveSetButtonClick
              end
              object AF_DeleteSetButton: TBitBtn
                Left = 424
                Top = 0
                Width = 45
                Height = 21
                Hint = 'Delete the active filter set'#13#10'(does not delete the filters)'
                Caption = 'Delete'
                TabOrder = 1
                TabStop = False
                OnClick = AF_DeleteSetButtonClick
              end
              object AF_LoadSetButton: TBitBtn
                Left = 377
                Top = 0
                Width = 45
                Height = 21
                Hint = 'Load filters from a named set'
                Caption = 'Load'
                TabOrder = 2
                TabStop = False
                OnClick = AF_LoadSetButtonClick
              end
              object AF_UpdateSetButton: TBitBtn
                Left = 637
                Top = 0
                Width = 45
                Height = 21
                Hint = 'Update the filters in the current set'
                Caption = 'Update'
                TabOrder = 3
                TabStop = False
                OnClick = AF_UpdateSetButtonClick
              end
            end
            object AF_LoadFilterButton: TBitBtn
              Left = 159
              Top = 210
              Width = 75
              Height = 21
              Hint = 'Load an existing filter (Ctrl+L)'
              Anchors = []
              Caption = 'Load Filter'
              TabOrder = 3
              TabStop = False
              OnClick = AF_LoadFilterButtonClick
            end
            object AF_NewFilterButton: TBitBtn
              Left = 76
              Top = 210
              Width = 75
              Height = 21
              Hint = 'Create a new filter (= key)'
              Anchors = []
              Caption = 'New Filter'
              TabOrder = 4
              TabStop = False
              OnClick = AF_NewFilterButtonClick
            end
            object AF_UnloadFilterButton: TBitBtn
              Left = 241
              Top = 210
              Width = 75
              Height = 21
              Hint = 'Remove selected filters from the set'
              Anchors = []
              Caption = 'Unload Filters'
              TabOrder = 5
              TabStop = False
              OnClick = AF_UnloadFilterButtonClick
            end
          end
        end
      end
    end
    object WTF_TrackBarDisabledPanel: TPanel
      Left = 4
      Top = 22
      Width = 0
      Height = 8
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 7
    end
  end
  object WTF_EyedropperTimer: TTimer
    Enabled = False
    Interval = 100
    Left = 456
    Top = 12
  end
  object DetectionMapLagTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = DetectionMapLagTimerTimer
    Left = 384
    Top = 14
  end
  object ContinuousPreviewLagTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = ContinuousPreviewLagTimerTimer
    Left = 342
    Top = 18
  end
end
