//---------------------------------------------------------------------------

#ifndef ShineMultiProcH
#define ShineMultiProcH
//---------------------------------------------------------------------------

#include "ShineToolParameters.h"
#include "IniFile.h"
#include "Ippheaders.h"
#include "ToolObject.h"
//---------------------------------------------------------------------------

class ShineMultiProc : public CToolProcessor
{
public:
	ShineMultiProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
			const bool *newEmergencyStopFlagPtr,
			IToolProgressMonitor *newToolProgressMonitor);

	virtual ~ShineMultiProc();

	void  *_mertusCrap;
	int SetIOConfig(CToolIOConfig *toolIOConfig);
	int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
	// Tool's functions called from within context of the processing loop.
	int SetParameters(CToolParameters *toolParams);
	int BeginProcessing(SToolProcessingData &procData);
	int EndProcessing(SToolProcessingData &procData);
	int BeginIteration(SToolProcessingData &procData);
	int EndIteration(SToolProcessingData &procData);
	int DoProcess(SToolProcessingData &procData);
	int GetIterationCount(SToolProcessingData &procData);

private:
	int _inFrameIndex = -1;
	int _outFrameIndex = -1;

   int _shotInIndex = -1;
   int _shotOutIndex = -1;

   ShineToolSettings _shineToolSettings;
   CAutoFilterParameters _afParams;

   IppiSize _imageSize;
   int _bitsPerPixelComponent = 0;

	int _framesRemaining = 0;        // Number of remaining output frames to be processed
	int _framesAlreadyProcessed = 0; // Number of output frames already processed
	int _framesToProcess = 0;        // Number of frames to process on current iteration

   int _inputFrameIndexList[9];
   void *_cheshireShineRepairMask;
};

#endif
