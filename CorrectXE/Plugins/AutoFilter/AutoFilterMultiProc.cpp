//---------------------------------------------------------------------------

#pragma hdrstop

#include "AutoFilterMultiProc.h"
#include "AutoFilterToolParameters.h"
#include "err_auto_filter.h"
#include "AlgorithmAF_MT.h"

#include "Clip3.h"
#include "ImageFormat3.h"
#include "ToolProgressMonitor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


///////////////////////////////////////////////////////////////////////////////
// AUTOFILTER MULTI-FRAME TOOL PROCESSOR
///////////////////////////////////////////////////////////////////////////////

CAutoFilterMultiProc::CAutoFilterMultiProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
		const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor, bool doPastOnlyHack)

      : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor),
		doPastOnlyHackFlag(doPastOnlyHack)

{
   // DON'T USE FSB FOR PROCESSING YET!! WE NOW PUT THE TOOL INFRA BUFS THERE!
   AlgorithmAF = new CAlgorithmAF_MT();
   StartProcessThread(); // Required by Tool Infrastructure
}

CAutoFilterMultiProc::~CAutoFilterMultiProc()
{
	delete AlgorithmAF;
   AlgorithmAF = NULL;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	int retVal = 0;
   string errorFunc("<UNKNOWN>");
	bool bTryToConserveMemory = false;

   try
   {

      // HACK ALERT - In PAST ONLY mode, processing MUST BE SERIALIZED
      // because we use modified frames for input!!!
      // NOTE! I don't know what the problem is (see below) with setting a number here that
      // is less than AF_MIN_THREAD_COUNT... it seems to work fine with only one!
      if (doPastOnlyHackFlag)
      {
         processingThreadCount = 1;
		}
      else
      {
         // Make sure the number of AutoFilter algorithm processing threads
         // is reasonable
         // HACK!!! IF THE NUMBER OF THREADS REQUESTED IS LESS THAN THE MIN
         // WE CAN RUN, set the min, but tell the af algorithm to
         // "conserve memory"
         processingThreadCount = toolIOConfig->processingThreadCount;
         if (processingThreadCount < AF_MIN_THREAD_COUNT)
         {
            bTryToConserveMemory = true;
				processingThreadCount = AF_MIN_THREAD_COUNT;
         }
         else if (processingThreadCount > AF_MAX_THREAD_COUNT)
         {
            processingThreadCount = AF_MAX_THREAD_COUNT;
         }
      }

      // Maximum number of input and output frames per processing iteration
      maxOutputFrames = processingThreadCount;
      maxInputFrames = maxOutputFrames + 2; // Output frames + 1 on either side

		// Tell Tool Infrastructure, via CToolProcessor supertype, about the
      // maximum number of input and output frames
      SetInputMaxFrames(0, maxInputFrames, maxOutputFrames); // Input Port 0

      // HACK ALERT!!! In the past-only hack mode, we want to use the MODIFIED
      // past frames as input!!
      // GAAAH !! We can't use ModifyInPlace because I can't get it to
      // frickin' work with 'cloneOutput'.... so instead we will simply copy
      // the output frame back to the input frame after processing ... BLEAAHH
      // if (doPastOnlyHackFlag)
      // SetOutputModifyInPlace(0, 0, true);   // true = "clone for ouptut"
      // else
		SetOutputNewFrame(0, 0); // Output to new frame with same image format
      // as the input
      SetOutputMaxFrames(0, maxOutputFrames); // Output Port 0

      // Tell algorithm about image dimensions, active area, etc.
      const CImageFormat *imageFormat = GetSrcImageFormat(0);
      RECT activePictureRect = imageFormat->getActivePictureRect();
      MTI_UINT16 maxDataValue[3];
      imageFormat->getComponentValueMax(maxDataValue);
      retVal = AlgorithmAF->Initialize(processingThreadCount, bTryToConserveMemory, imageFormat->getTotalFrameHeight(),
            imageFormat->getTotalFrameWidth(), imageFormat->getComponentCountExcAlpha(), activePictureRect.top,
				activePictureRect.bottom + 1, activePictureRect.left, activePictureRect.right + 1);
      if (retVal != 0)
      {
         throw(errorFunc = "SetDimension in SetIOConfig");
      }

      // Component bounds
      MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
      MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];
      imageFormat->getComponentValueMin(cvMin);
      imageFormat->getComponentValueMax(cvMax);
		retVal = AlgorithmAF->SetImageBounds(cvMin, cvMax);
      if (retVal != 0)
      {
         throw(errorFunc = "SetImageBounds in SetIOConfig");
      }

      // Other crap
      AlgorithmAF->SetAlphaMatteType(imageFormat->getAlphaMatteType(), imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR);
      AlgorithmAF->SetEmergencyStopFlagPtr(GetEmergencyStopFlagPtr());

      return 0;
	}
   catch (string)
   {; // do nothing here
   }
   catch (std::bad_alloc)
   {
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (...)
   {
		retVal = 666; // meaningless
   }
   try
   {
      CAutoErrorReporter autoErr("CAutoFilterMultiProc::SetIOConfig", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      string errorMsg;
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
			errorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         errorMsg = "No alpha channel - cannot find defect map";
         break;
      case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
         break;
      default:
         errorMsg = "Internal Error";
			break;
      }

      autoErr.errorCode = retVal;
      autoErr.msg << errorMsg << endl << " in function " << errorFunc;
   }
   catch (...)
   {
      TRACE_0(errout << "CAutoFilterMultiProc::SetIOConfig: FATAL ERROR, code " << retVal << endl <<
            "CAUGHT EXCEPTION when trying to display error dialog!");
	}
   return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   inFrameIndex = frameRange.inFrameIndex + 1;
   outFrameIndex = frameRange.outFrameIndex - 1;

   // In "past only" mode we don't do the first (mark in) frame
   if (doPastOnlyHackFlag)
   {
      inFrameIndex += 1;
   }

   // Calculate number of frames that need to be processed
   framesRemaining = outFrameIndex - inFrameIndex; // Exclusive out frame
   framesAlreadyProcessed = 0;

   // Calculate the number of iterations, given that a maximum
   // of maxOutputFrames will be processed on each iteration.  Round up
   // to make sure that there is a last iteration for leftovers.
   iterationCount = (framesRemaining + maxOutputFrames - 1) / maxOutputFrames;

   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::SetParameters(CToolParameters *toolParams)
{
   int retVal = 0;
   string errorFunc("<UNKNOWN>");

   try
   {

      CAutoFilterToolParameters *afpToolParams = static_cast<CAutoFilterToolParameters*>(toolParams);

		retVal = AlgorithmAF->AssignParameter(&afpToolParams->getParameterRef());

      return 0;
   }

   catch (string)
   {; // do nothing here
   }
   catch (std::bad_alloc)
   {
		retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (...)
   {
      retVal = 666; // meaningless
   }
   try
   {
      CAutoErrorReporter autoErr("CAutoFilterMultiProc::SetParameters", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		string errorMsg;
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
         errorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         errorMsg = "No alpha channel - cannot find defect map";
         break;
		case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
         break;
      default:
         errorMsg = "Internal Error";
         break;
      }

      autoErr.errorCode = retVal;
		autoErr.msg << errorMsg << endl << " in function " << errorFunc;
   }
   catch (...)
   {
      TRACE_0(errout << "CAutoFilterMultiProc::SetParameters: FATAL ERROR, code " << retVal << endl <<
            "CAUGHT EXCEPTION when trying to display error dialog!");
   }
   return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::BeginProcessing(SToolProcessingData &procData)
{
   int retVal = 0;

   const CImageFormat *imageFormat = GetSrcImageFormat(0);

	RECT activePictureRect = imageFormat->getActivePictureRect();

	// Compute the pixel mask for the region of interest now for all frames
	// if the mask exists and is not animated
   if (GetSystemAPI()->IsMaskAvailable())
   {
      if (!GetSystemAPI()->IsMaskAnimated())
		{
         // Only render the mask at the in frame, and pass the same blend
         // and label arrays to all threads
         retVal = GetSystemAPI()->GetMaskRoi(inFrameIndex, maskROI[0]);
         retVal = AlgorithmAF->setRegionOfInterestPtr(maskROI[0].getBlendPtr(), NULL /* labels not used */);
			if (retVal != 0)
         {
            return retVal;
         }
      }
	}
   else
   {
      // No mask
		retVal = AlgorithmAF->setRegionOfInterestPtr(NULL, NULL);
   }

   GetToolProgressMonitor()->SetFrameCount(framesRemaining);
	GetToolProgressMonitor()->SetStatusMessage("Processing");
   GetToolProgressMonitor()->StartProgress();

   // Success, continue
	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::BeginIteration(SToolProcessingData &procData)
{
   // Determine the number of frames to process for this iteration.
   // Typically this will be maxOutputFrames, but on the last iteration
	// it will be framesRemaining (which will be less than maxOutputFrames)
   framesToProcess = std::min(framesRemaining, maxOutputFrames);

   // The input frame count for this iteration is the number of frames
	// to process (i.e., number of output frames) plus one more frame
   // on either end
   int inputFrameCount = framesToProcess + 2;

	// Index of first output frame
   int firstFrameToProcess = inFrameIndex + framesAlreadyProcessed;

   // List of frames we'll need as input
	int inputFrameIndexList[AF_MAX_INPUT_FRAMES];

   if (doPastOnlyHackFlag)
   {
		// In "past-only" mode we always look backward two frames.
      // HACK ALERT: in this mode we don't process the "mark in" frame,
      // although we do use that frame as input for the next two frames.
      // But the first processed frame (at markIn + 1) can only look back
		// ONE frame. The hack is that we tell the tool infrastructure to
      // use that frame twice, to simplify bookkeeping... since this mode
      // is used to clear static dirt, it doesn't matter that the motions
      // will not be right (normally the user should set the motion
		// parameters to 0!)

      // The framelist we are processing in the current clip
      CVideoFrameList *frameList = GetSystemAPI()->getVideoFrameList();

      for (int threadIndex = 0; threadIndex < processingThreadCount; ++threadIndex)
      {
         // Index in clip of frame this thread is going to process
			int threadProcFrame = firstFrameToProcess + threadIndex;

         if (threadProcFrame == (GetSystemAPI()->getMarkIn() + 1) || threadProcFrame == (frameList->getInFrameIndex() + 1))
         {
				// HACK First frame looks 1 backward  - TWICE!
            laaFrameIndex[threadIndex][0] = -1;
            laaFrameIndex[threadIndex][1] = -1;
            laaFrameIndex[threadIndex][2] = 0;
			}
         else
         {
            // Rest of the frames look 2 backward
				laaFrameIndex[threadIndex][0] = -2;
            laaFrameIndex[threadIndex][1] = -1;
            laaFrameIndex[threadIndex][2] = 0;
         }
		}

      // Make a list of the frame indices that will be used as input
      // HACK! We tell the TI to read the frame before the mark in frame,
		// but we never use it!

      for (int i = 0; i < inputFrameCount; ++i)
      {
			inputFrameIndexList[i] = firstFrameToProcess + i - 2;
      }
   }
   else if (framesToProcess == 1)
	{
      // Last  frame looks 2 backward
      laaFrameIndex[0][0] = -2;
      laaFrameIndex[0][1] = -1;
		laaFrameIndex[0][2] = 0;

      // Make a list of the frame indices that will be used as input.
      for (int i = 0; i < inputFrameCount; ++i)
		{
         // The first input frame will be (firstFrameToProcess - 2)
         // instead of the normal (firstFrameToProcess - 1)
         inputFrameIndexList[i] = firstFrameToProcess + i - 2;
		}
   }
   else
   {
		// The framelist we are processing in the current clip
      CVideoFrameList *frameList = GetSystemAPI()->getVideoFrameList();

      for (int threadIndex = 0; threadIndex < processingThreadCount; ++threadIndex)
		{
         // Index in clip of frame this thread is going to process
         int threadProcFrame = firstFrameToProcess + threadIndex;

			if (threadProcFrame == GetSystemAPI()->getMarkIn() || threadProcFrame == frameList->getInFrameIndex())
         {
            // First frame looks 2 forward
            laaFrameIndex[threadIndex][0] = 0;
				laaFrameIndex[threadIndex][1] = 1;
            laaFrameIndex[threadIndex][2] = 2;
         }
         else if (threadProcFrame == (GetSystemAPI()->getMarkOut() - 1) || threadProcFrame == (frameList->getOutFrameIndex() - 1))
			{
            // Last  frame looks 2 backward
            laaFrameIndex[threadIndex][0] = -2;
            laaFrameIndex[threadIndex][1] = -1;
				laaFrameIndex[threadIndex][2] = 0;
         }
         else
         {
				// Most frames look 1 backward and 1 forward
            laaFrameIndex[threadIndex][0] = -1;
            laaFrameIndex[threadIndex][1] = 0;
            laaFrameIndex[threadIndex][2] = 1;
			}
      }

      // Make a list of the frame indices that will be used as input.
		for (int i = 0; i < inputFrameCount; ++i)
      {
         inputFrameIndexList[i] = firstFrameToProcess + i - 1;
      }
	}

   // Tell the Tool Infrastructure which frames we want as input
   // during this iteration
	SetInputFrames(0, inputFrameCount, inputFrameIndexList);

   // when finished processing, release the oldest frames that we
   // will no longer need for future iterations
	//
   // HIDEOUS SPECIAL CASE!!! If the next iteration will be the last AND
   // we will only have one frame to process, then WE NEED TO KEEP AN EXTRA
   // FRAME because it has to look TWO FRAMES BACKWARD!
	//
   int framesToRelease = framesToProcess;
   if ((!doPastOnlyHackFlag) && ((framesRemaining - framesToProcess) == 1))
   {
		framesToRelease = framesToProcess - 1;
   }
   SetFramesToRelease(0, framesToRelease, inputFrameIndexList);

	// These are the frames that will be the output
   int outputFrameIndexList[AF_MAX_OUTPUT_FRAMES];
   for (int i = 0; i < framesToProcess; ++i)
   {
		outputFrameIndexList[i] = firstFrameToProcess + i;
   }
   SetOutputFrames(0, framesToProcess, outputFrameIndexList);

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
   // how many processing iterations to do before stopping

   return iterationCount;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::DoProcess(SToolProcessingData &procData)
{
   int retVal = 0;
	CAutoErrorReporter autoErr("CAutoFilterMultiProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   try
   {

      MTI_UINT16 *uspaSrcArg[AF_NUM_SRC_FRAMES];
      MTI_UINT16 *uspDstArg;
      CToolFrameBuffer *inToolFrameBuffer, *outToolFrameBuffer;

      // Index of first frame to process is 1 unless we are in past-only mode
      // in which case it is 2... it's also 2 in this
      // HIDEOUS SPECIAL CASE - there is only one frame left to process.
		// In that case it is at offset 2 in the input set because it needs to
      // look two frames backward (normally the first frame to process is at
      // offset 1)
      int inputSetIndexOfFirstFrameToProcess = 1;
		if (doPastOnlyHackFlag || (framesToProcess == 1))
      {
         inputSetIndexOfFirstFrameToProcess = 2;
      }

      // Do setup for each of the AutoFilter algorithm's processing threads
      for (int threadIndex = 0; threadIndex < processingThreadCount; ++threadIndex)
      {
			if (threadIndex < framesToProcess)
         {
            // Get pointers to the relevant input frames obtained by the
            // Tool Infrastructure.
				//
            // laaFrameIndex[threadIndex][] are the indices the 3 desired
            // input frames within the input set returned
            //
				for (int i = 0; i < AF_NUM_SRC_FRAMES; ++i)
            {
               int inputIndex = inputSetIndexOfFirstFrameToProcess + threadIndex + laaFrameIndex[threadIndex][i];
               inToolFrameBuffer = GetRequestedInputFrame(0, inputIndex);
					uspaSrcArg[i] = inToolFrameBuffer->GetVisibleFrameBufferPtr();
               retVal = inToolFrameBuffer->GetErrorFlag();
               if (retVal != 0)
               {
						autoErr.errorCode = retVal;
                  autoErr.msg << "Error in function GetVisibleFrameBufferPtr()" << endl << "Frame file is missing or corrupted.";
                  return retVal;
               }
				}

            // Tell the algorithm about the input frame buffer pointers
            // and relative indices
				retVal = AlgorithmAF->SetSrcImgPtr(uspaSrcArg, laaFrameIndex[threadIndex], threadIndex);
            if (retVal != 0)
            {
               autoErr.errorCode = retVal;
					autoErr.msg << "Internal Error in function SetSrcImgPtr()";
               return retVal;
            }

				// Get the output frame provided by the Tool Infrastructure
            outToolFrameBuffer = GetRequestedOutputFrame(0, threadIndex);

            // If the mask is animated, then must get a new region-of-interest
				// pixel mask for every frame
            if (GetSystemAPI()->IsMaskAnimated())
            {
               int outputFrameIndex = outToolFrameBuffer->GetFrameIndex();
					retVal = GetSystemAPI()->GetMaskRoi(outputFrameIndex, maskROI[threadIndex]);
               if (retVal != 0)
               {
                  autoErr.errorCode = retVal;
						autoErr.msg << "Internal Error in function GetMaskRoi()";
                  return retVal;
               }

					retVal = AlgorithmAF->setRegionOfInterestPtr(maskROI[threadIndex].getBlendPtr(), NULL, /* labels not used */ threadIndex);
               if (retVal != 0)
               {
                  autoErr.errorCode = retVal;
						autoErr.msg << "Internal Error in function setRegionOfInterestPtr()";
                  return retVal;
               }
            }

            inToolFrameBuffer = GetRequestedInputFrame(0, inputSetIndexOfFirstFrameToProcess + threadIndex);

            // If we are doing the "past-only" hack, we will modify the input
				// frame in place, then later copy it to the output frame
            // GAAAH I can't get 'modifyInPlace' to work, so as a HACK let
            // the tool modify the output frame, then copy that back to the
            // input frame!!!
				// if (doPastOnlyHackFlag)
            // {
            //// Give the algorithm the pointer to the output image
            // uspDstArg = inToolFrameBuffer->GetVisibleFrameBufferPtr();
				// }
            // else
            {
               // Copy the original image, including invisible fields, to the
					// output frame.  Note: this may have to wait to allocate memory
               CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBuffer, outToolFrameBuffer);

               // Give the algorithm the pointer to the output image
					uspDstArg = outToolFrameBuffer->GetVisibleFrameBufferPtr();
            }

            // Give the algorithm the pointer to the output image
				AlgorithmAF->SetDstImgPtr(uspDstArg, threadIndex);

            // Now setup the pixel region list that will receive the original
            // values of pixels changed by the algorithm
				AlgorithmAF->SetPixelRegionList(outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr(), threadIndex);

            // We do not need to see a map of modified pixels when doing multiple frames.
            AlgorithmAF->SetModifiedPixelMap(nullptr, 0);
			}
         else
         {
            // There are more threads than frames that need to be processed
				// for this iteration.  Tell AlgorithmAF to skip this thread by
            // passing it a NULL destination frame pointer.
            AlgorithmAF->SetDstImgPtr(NULL, threadIndex);
         }
		}

      // Finally, render to one or more output frames
      retVal = AlgorithmAF->Process();
		if (retVal != 0)
      {
         // autoErr.errorCode = retVal;
         // string errorMsg = (retVal == ERR_PLUGIN_AF_NO_ALPHA_CHANNEL)
			// ? "No alpha channel - cannot find defect map"
         // : "Error in function AlgorithmAF->Process()";
         // autoErr.msg << errorMsg;
         return retVal;
		}

      // HACK - if we're in "past-only" mode, we need to copy the modified bits
      // from the output buffer back to the input buffer.
		// No need to loop here, we know that past-only mode is single threded
      // because it needs to use the output of the previous iteration as input
      // to the current iteration!

		if (doPastOnlyHackFlag)
      {
         MTIassert(processingThreadCount == 1);

			// Overwrite the input frame bits with the modified output bits
         ///////////////////////////////////////////////////////////////
         // CAREFUL!!! WE ARE ASSUMING THAT inToolFrameBuffer AND
         // outToolFrameBuffer ARE STILL SET CORRECTLY FROM ABOVE!!!!!!
			///////////////////////////////////////////////////////////////
         inToolFrameBuffer->CopyAllImageBitsOnly(*outToolFrameBuffer);
      }

		return 0;
   }
   catch (...)
   {
		retVal = 666; // arbitrary
      autoErr.errorCode = retVal;
      autoErr.msg << "Caught exception in function AlgorithmAF->Process()";
   }

   return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::EndIteration(SToolProcessingData &procData)
{
   framesRemaining -= framesToProcess;
	framesAlreadyProcessed += framesToProcess;

   GetToolProgressMonitor()->BumpProgress(framesToProcess);

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterMultiProc::EndProcessing(SToolProcessingData &procData)
{
	// DeleteMask();
	if (framesRemaining == 0)
	{
		GetToolProgressMonitor()->SetStatusMessage("Processing complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	return 0;
}
// ---------------------------------------------------------------------------
