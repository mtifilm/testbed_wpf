//---------------------------------------------------------------------------

#pragma hdrstop

#include "ShinePreprocessProc.h"
#include "err_auto_filter.h"

#include "AutoTrackingPreprocessor.h"
#include "ImageFormat3.h"
#include "Ippheaders.h"
#include "IpaStripeStream.h"
#include "ToolProgressMonitor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

ShinePreprocessProc::ShinePreprocessProc(int newToolNumber, const string &newToolName,
	CToolSystemInterface *newSystemAPI, const bool *newEmergencyStopFlagPtr,
	IToolProgressMonitor *newToolProgressMonitor)
	: CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
	StartProcessThread(); // Required by Tool Infrastructure
}
//---------------------------------------------------------------------------

ShinePreprocessProc::~ShinePreprocessProc()
{
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	SetInputMaxFrames(0, 1, 1); // Input Port 0, 1 frame each itereation

// There may be an issue with "Modify in place" here that causes Stab to hang.
// Additional code was added to DoProcess to copy the input frame to the output
// so the screen isn't black while tracking.
//	SetOutputModifyInPlace(0, 0, false); // false = don't "clone for output"
	SetOutputNewFrame(0, 0);
//

	SetOutputMaxFrames(0, 1); // Output Port 0, just the one frame
	return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
	_inFrameIndex = frameRange.inFrameIndex;
	_outFrameIndex = frameRange.outFrameIndex;
	_iterationCount = _outFrameIndex - _inFrameIndex;

	return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::SetParameters(CToolParameters *toolParams)
{
	ShineToolParameters *shineToolParams = static_cast<ShineToolParameters*>(toolParams);

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_shineToolSettings = shineToolParams->getShineToolSettingsRef();

	// Set the image size
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
	_imageSize = {.height = (int) imageFormat->getLinesPerFrame(),
					  .width = (int) imageFormat->getPixelsPerLine()};

	MTI_UINT16 componentValues[3];
	imageFormat->getComponentValueMax(componentValues);
	_maxPixel = componentValues[0];

	 return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::GetIterationCount(SToolProcessingData &procData)
{
	// This function is called by the Tool Infrastructure so it knows
	// how many processing iterations to do before stopping
	// Calculate the number of iterations, depends on the state

	return _iterationCount;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::BeginProcessing(SToolProcessingData &procData)
{
	// Initialize data
	GetToolProgressMonitor()->SetFrameCount(_iterationCount);
	GetToolProgressMonitor()->SetStatusMessage("Analyzing");
	GetToolProgressMonitor()->StartProgress();

	// Clear old preprocessor output.
   MTIassert(_shineToolSettings.singleShotListIndex >= 0);
   if (_shineToolSettings.singleShotListIndex < 0)
   {
      return -1;
   }

   auto &listItem = (*_shineToolSettings.shinePreprocOutputList)[_shineToolSettings.singleShotListIndex];
	listItem.preprocOutput = nullptr;

	_autoTrackingPreprocessor = new AutoTrackingPreprocessor();
	_autoTrackingPreprocessor->initSceneTracking(_iterationCount);

   // Remember the current frame so we can jump back to it when we're done.
   _frameToJumpToWhenDone = GetSystemAPI()->getLastFrameIndex();

	return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::BeginIteration(SToolProcessingData &procData)
{
	// List of the one frame we'll need as input/output/to be released when done
	int frameIndexList[1];
	frameIndexList[0] = _inFrameIndex + procData.iteration;

	SetInputFrames(0, 1, frameIndexList);
	SetOutputFrames(0, 1, frameIndexList);
	SetFramesToRelease(0, 1, frameIndexList);

	return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::DoProcess(SToolProcessingData &procData)
{
	CToolFrameBuffer *inToolFrameBufferB;
	CToolFrameBuffer *outToolFrameBuffer;
	auto status = 0;

	MTI_UINT16* frameBits;
	inToolFrameBufferB = GetRequestedInputFrame(0, 0);
	frameBits = inToolFrameBufferB->GetVisibleFrameBufferPtr();
	// QQQ
	// We suspect ModifyInPlace is causing very intermittent hangs,
	// so we switched it to use a new output frame instead, which wastes time
	// due to having to copy the input frame to the new output frame... uggh.
	outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
	// auto outputBits = outToolFrameBuffer->GetVisibleFrameBufferPtr();
	CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);
	/// DBTRACE(doProcessTimer);

	float *dataPointer;

	Ipp32fArray yArray(_imageSize);

	// Get normalized Y array
	IpaStripeStream iss;

	iss << yArray;
	auto func = [&](Ipp32fArray slice)
	{
		auto sliceRoi = slice.getRoi();
		auto sliceData = frameBits + 3 * slice.getCols() * sliceRoi.y + sliceRoi.x;
		slice.importNormalizedYuvFromRgb(sliceData, sliceRoi.height, sliceRoi.width, _maxPixel);
	};

	iss << efu_32f(func); // Embarcardeo fuckup
	iss.stripe();

	try
	{
		bool opticalFlowAvailableFlag = _autoTrackingPreprocessor->processFrame(yArray, procData.iteration);
      if (opticalFlowAvailableFlag)
      {
         TRACE_0(errout << "OPTICAL FLOW IS AVAILABLE at frame " << _inFrameIndex + procData.iteration);
      }
	}
	catch (...)
	{
		status = ERR_PLUGIN_AF_AUTO_TRACKING_FAIL;
	}

	return status;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::EndIteration(SToolProcessingData &procData)
{
	GetToolProgressMonitor()->BumpProgress();
	return 0;
}
//---------------------------------------------------------------------------

int ShinePreprocessProc::EndProcessing(SToolProcessingData &procData)
{
	int result = 0;

	// Did render run to completion or was it aborted?
	if (procData.iteration == GetIterationCount(procData))
	{
		GetToolProgressMonitor()->StopProgress(true);
		GetToolProgressMonitor()->SetStatusMessage("Analysis completed");

      auto &listItem = (*_shineToolSettings.shinePreprocOutputList)[_shineToolSettings.singleShotListIndex];
      listItem.inFrame = _inFrameIndex;
      listItem.outFrame = _outFrameIndex;
		try
		{
	      if (!_autoTrackingPreprocessor->getOpticalFlowAvailable())
         {
            result = ERR_PLUGIN_AF_AUTO_TRACKING_FAIL;
         }
         else
         {
            auto opticalFlowData = new OpticalFlowData(_autoTrackingPreprocessor->getOpticalFlow());
            listItem.preprocOutput = std::make_shared<ShinePreprocOutput>(opticalFlowData);
         }
		}
		catch(...)
		{
			result = ERR_PLUGIN_AF_AUTO_TRACKING_FAIL;
		}
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Analysis stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

   // Jump to the frame we were at when we began processing.
   GetSystemAPI()->goToFrame(_frameToJumpToWhenDone);


	return result;
}
//---------------------------------------------------------------------------

