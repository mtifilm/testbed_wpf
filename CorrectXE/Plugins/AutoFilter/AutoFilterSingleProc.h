//---------------------------------------------------------------------------

#ifndef AutofilterSingleProcH
#define AutofilterSingleProcH
//---------------------------------------------------------------------------

#include "AlgorithmAF_MT.h"
#include "IniFile.h"
#include "RegionOfInterest.h"
#include "ToolObject.h"
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////

class CAutoFilterSingleProc : public CToolProcessor
{
public:
	CAutoFilterSingleProc(
         int newToolNumber,
         const string &newToolName,
         CToolSystemInterface *newSystemAPI,
			const bool *newEmergencyStopFlagPtr,
         IToolProgressMonitor *newToolProgressMonitor,
			bool doPastOnlyHack,
         StatsAF *afStats,
         GrainFilterSet &grainFilters,
			MTI_UINT8 *modifiedPixelMap);

	virtual ~CAutoFilterSingleProc();

   CAlgorithmAF  *AlgorithmAF;
	int SetIOConfig(CToolIOConfig *toolIOConfig);
	int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
	// Tool's functions called from within context of the processing loop.
	int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
	int EndProcessing(SToolProcessingData &procData);
	int BeginIteration(SToolProcessingData &procData);
	int EndIteration(SToolProcessingData &procData);
	int DoProcess(SToolProcessingData &procData);
	int GetIterationCount(SToolProcessingData &procData);

private:
   int inFrameIndex;        // Should we save the whole tool parameter object?
	int outFrameIndex;

	int framesRemaining;     // Number of remaining output frames to be processed
	int framesAlreadyProcessed; // Number of output frames already processed
   int framesToProcess;     // Number of frames to process on current iteration

   long laFrameIndexInInputSet[AF_NUM_SRC_FRAMES];

	bool doPastOnlyHackFlag;
	StatsAF *_afStats;

   AF_PreallocedMemory *_preallocedMemory = nullptr;

   GrainFilterSet *_cachedGrainFilters = nullptr;
	MTI_UINT8 *_modifiedPixelMap = nullptr;

   // Mask Region
	bool quickMaskMode;
	CRegionOfInterest maskROI;
};

#endif
