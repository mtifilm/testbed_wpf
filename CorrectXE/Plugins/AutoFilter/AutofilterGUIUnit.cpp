// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutofilterGUIUnit.h"
#include "AutoFilterTool.h"
#include "NewFilterFormUnit.h"
#include "NewFilterSetFormUnit.h"
#include "ExistingSetUnit.h"

#include "FilterManager.h"
#include "PickAFilterFormUnit.h"

#include "ClipAPI.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
#include "PDL.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"
#include "ShowModalDialog.h"

#include <iostream>
#include <vector>
#include <string>
using std::vector;
using std::string;
// ---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ExecButtonsFrameUnit"
#pragma link "MTIUNIT"
#pragma link "FilterBankFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorProfileFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "VDoubleEdit"
#pragma link "ColorPanel"
#pragma link "PresetsUnit"
#pragma link "ColorLabel"
#pragma resource "*.dfm"
TAutofilterGUI *AutofilterGUI;
// ---------------------------------------------------------------------------

#define NO_SHINE

#define PLUGIN_TAB_INDEX 3

#define GENERAL_INI_SECTION_NAME "General"
#define PREVIEW_HIGHLIGHT_KEY "PreviewHighlight"

#define DEFAULT_SAVED_SET_NAME "Saved Set"
#define DEFAULT_CUSTOM_FILTER_NAME "Custom"

static string PDLFilterNameKey = "FilterName";
static string PDLOpModeKey = "OpMode";
static string PDLFilterEnabledKey = "FilterEnabled";
static string PDLMaxDebrisSizeKey = "MaxDebrisSize";
static string PDLMinDebrisSizeKey = "MinDebrisSize";
static string PDLMaxDustSizeKey = "MaxDustSize";
static string PDLFixConfidenceKey = "MinFixConfidence";
static string PDLMinContrastKey = "MinContrast";
static string PDLLuminanceThresholdKey = "LuminanceThreshold";
static string PDLShineSpatialContrastKey = "ShineSpatialContrast";
static string PDLShineTemporalContrastKey = "ShineTemporalContrast";
static string PDLShineMotionToleranceKey = "ShineMotionTolerance";
static string PDLEdgeBlurKey = "EdgeBlur";
static string PDLBrightKey = "Bright";
static string PDLHMotionKey = "HMotion";
static string PDLVMotionKey = "VMotion";
static string PDLDetectCompKey = "DetectComp";
static string PDLProcessMaskKey = "ProcessMask";
static string PDLDustHardMotionKey = "DustHardMotion";
static string PDLAlphaMaskKey = "AlphaMask";
static string PDLGrainFilterKey = "GrainFilter";
static string PDLGrainFilterParamKey = "GrainFilterParam";

string PDLFilterTable[6] =
{PDL_0_FILTER_ID, PDL_1_FILTER_ID, PDL_2_FILTER_ID, PDL_3_FILTER_ID, PDL_4_FILTER_ID, PDL_5_FILTER_ID};

// ---------------------------------------------------------------------

#define CLAMP(val,lo,hi)  ((val<(lo))? (lo) : ((val>(hi))? (hi) : val))
// ---------------------------------------------------------------------------

DebrisToolOpMode QueryOpMode()
{
	if (AutofilterGUI == nullptr)
	{
		return DebrisToolOpMode::INVALID;
	}

	return AutofilterGUI->queryOpMode();
}

DebrisToolOpMode TAutofilterGUI::queryOpMode()
{
#ifdef NO_SHINE
   return DebrisToolOpMode::AF;
#else
	return (OpModePageControl->ActivePage == SHINE_TabSheet)
					? DebrisToolOpMode::SHINE
					: DebrisToolOpMode::AF;
#endif
}
// ---------------------------------------------------------------------------

void ToggleOpMode()
{
	if (AutofilterGUI == nullptr)
	{
		return;
	}

	AutofilterGUI->toggleOpMode();
}

void TAutofilterGUI::toggleOpMode()
{
#ifndef NO_SHINE
	if (OpModePageControl->ActivePage == SHINE_TabSheet)
	{
		OpModePageControl->ActivePage = AF_TabSheet;
	}
	else
	{
		OpModePageControl->ActivePage = SHINE_TabSheet;
	}

	OpModePageControlChange(nullptr);
#endif
}
// ---------------------------------------------------------------------------

void CreateAutoFilterGUI(void)
{
	if (AutofilterGUI != NULL)
	{
		return;
	} // already created

	AutofilterGUI = new TAutofilterGUI(Application); // Create it
	AutofilterGUI->RestoreProperties(); // QQQ formCreate does this, too!
	AutofilterGUI->formCreate();

	// Reparent the controls to the UniTool
	extern char PluginName[];
	GAutoFilter->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
			reinterpret_cast<int *>(AutofilterGUI->AutofilterControlPanel), NULL);
}

// ---------------------------------------------------------------------------

void DestroyAutoFilterGUI(void)
{
	if (AutofilterGUI == NULL)
	{
		return;
	} // already destroyed

	// Reparent the controls back to us before destroying them
	if (GAutoFilter != NULL)
	{
		GAutoFilter->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
	}
	AutofilterGUI->AutofilterControlPanel->Parent = AutofilterGUI;
	AutofilterGUI->formDestroy();

	AutofilterGUI->Free();
	AutofilterGUI = NULL;
}
// ---------------------------------------------------------------------------

bool ShowAutoFilterGUI(void)
{
	CreateAutoFilterGUI(); // Create the gui if necessary

	if (AutofilterGUI == NULL || GAutoFilter == NULL)
	{
		return false;
	}

	AutofilterGUI->AutofilterControlPanel->Visible = true;
	AutofilterGUI->formShow();

	return true;
}
// ---------------------------------------------------------------------------

bool HideAutoFilterGUI(void)
{
	if (AutofilterGUI == NULL)
	{
		return false;
	} // ERROR: not created yet

	AutofilterGUI->AutofilterControlPanel->Visible = false;
	AutofilterGUI->formHide();

	return false; // was: (AutoFilterForm->Visible);
}
// ---------------------------------------------------------------------------

bool IsToolVisible(void)
{
	if (AutofilterGUI == NULL)
	{
		return (false);
	} // not created yet

	return AutofilterGUI->AutofilterControlPanel->Visible;
}
// ---------------------------------------------------------------------------

void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag)

{
	if (AutofilterGUI == NULL)
	{
		return;
	} // not created yet, or it was destroyed

	AutofilterGUI->updateExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag);

}
// ---------------------------------------------------------------------------

__fastcall TAutofilterGUI::TAutofilterGUI(TComponent* Owner)
		: TMTIForm(Owner)
{
	if (AF_AutofilterIniFileName == NULL)
	{
		AF_AutofilterIniFileName = new string;
	}

	*AF_AutofilterIniFileName = "$(CPMP_USER_DIR)Autofilter.ini";

	if (SHINE_AutofilterIniFileName == NULL)
	{
		SHINE_AutofilterIniFileName = new string;
	}

	*SHINE_AutofilterIniFileName = "$(CPMP_USER_DIR)Shine.ini";

	bDoPreviewAgain = false;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::formCreate(void)
{
	AF_FilterManager = new CFilterManager(true);
	AF_FilterManager->ReadFromIniFile(*AF_AutofilterIniFileName);
	SHINE_FilterManager = new CFilterManager(false);
//	SHINE_FilterManager->ReadFromIniFile(*SHINE_AutofilterIniFileName);
	activeFilterManager = SHINE_FilterManager;

	RestoreProperties(); // CreateAutoFilterGUI  does this before calling us! QQQ
	AF_SelectedFilterID = INVALID_ID;

	AF_UpdateFilterManagerGUI();

   // Set track edit default values. We get them from a default ShineToolSettings.
   ShineToolSettings defaultSettings;
   SHINE_UpdateSettingsGui(defaultSettings);

	// Global callbacks
	SET_CBHOOK(MaybeHandleUserInput, GAutoFilter->GotUserInput);

	// AutoFilter callbacks
	SET_CBHOOK(AF_SelectionChanged, AF_FilterBankFrame->SelectionChange);
	SET_CBHOOK(AF_GangChanged, AF_FilterBankFrame->GangChange);
	SET_CBHOOK(AF_FilterUseChanged, AF_FilterBankFrame->FilterUseChange);
	SET_CBHOOK(AF_MaxSizeChanged, AF_MaxSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_MinSizeChanged, AF_MinSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_DustMaxSizeChanged, AF_DustMaxSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_MinContrastChanged, AF_MinContrastTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_LuminanceThresholdChanged, AF_LuminanceThresholdTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_FixToleranceChanged, AF_FixToleranceTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_RgbMotionChanged, AF_RgbMotionTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_AlphaMotionChanged, AF_AlphaMotionTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_EdgeBlurChanged, AF_CorrectionEdgeBlurTrackEdit->TrackEditValueChange);
	SET_CBHOOK(AF_GrainFilterParamChanged, AF_AlphaGrainSuppressionTrackEdit->TrackEditValueChange);

	// SHINE callbacks
	SET_CBHOOK(SHINE_Dark_MaxSizeTrackEditChanged, SHINE_Dark_MaxSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Dark_MinSizeTrackEditChanged, SHINE_Dark_MinSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Dark_TemporalContrastTrackEditChanged, SHINE_Dark_TemporalContrastTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Dark_SpatialContrastTrackEditChanged, SHINE_Dark_SpatialContrastTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Dark_MotionToleranceTrackEditChanged, SHINE_Dark_MotionToleranceTrackEdit->TrackEditValueChange);

	SET_CBHOOK(SHINE_Bright_MaxSizeTrackEditChanged, SHINE_Bright_MaxSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Bright_MinSizeTrackEditChanged, SHINE_Bright_MinSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Bright_TemporalContrastTrackEditChanged, SHINE_Bright_TemporalContrastTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Bright_SpatialContrastTrackEditChanged, SHINE_Bright_SpatialContrastTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_Bright_MotionToleranceTrackEditChanged, SHINE_Bright_MotionToleranceTrackEdit->TrackEditValueChange);

	SET_CBHOOK(SHINE_AlphaTrackEditChanged, SHINE_Alpha_MaxSizeTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_AlphaTrackEditChanged, SHINE_Alpha_GrainSuppressionTrackEdit->TrackEditValueChange);
	SET_CBHOOK(SHINE_AlphaTrackEditChanged, SHINE_Alpha_MotionToleranceTrackEdit->TrackEditValueChange);

	inhibitCallbacks = false;
	NewFilterNameDialog = NULL;
	NewFilterSetNameDialog = NULL;

	AF_LastFilterSetLoaded = INVALID_ID;

	GAutoFilter->SetToolProgressMonitor(ExecStatusBar);

   auto iniFileName = SHINE_getIniFileName();

	SHINE_darkPresets = new PresetParameterModel<ShinePresetParameters>("DarkPresets", 4);
	SHINE_darkPresets->ReadPresetsFromIni(iniFileName);
	SHINE_darkPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
	SET_CBHOOK(SHINE_darkPresetHasChanged, SHINE_darkPresets->SelectedIndexChanged);
	SHINE_Dark_PresetsFrame->Initialize(SHINE_darkPresets, iniFileName);

	SHINE_brightPresets = new PresetParameterModel<ShinePresetParameters>("BrightPresets", 4);
	SHINE_brightPresets->ReadPresetsFromIni(iniFileName);
	SHINE_brightPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
	SET_CBHOOK(SHINE_brightPresetHasChanged, SHINE_brightPresets->SelectedIndexChanged);
	SHINE_Bright_PresetsFrame->Initialize(SHINE_brightPresets, iniFileName);

	SHINE_alphaPresets = new PresetParameterModel<ShinePresetParameters>("AlphaPresets", 4);
	SHINE_alphaPresets->ReadPresetsFromIni(iniFileName);
	SHINE_alphaPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
	SET_CBHOOK(SHINE_alphaPresetHasChanged, SHINE_alphaPresets->SelectedIndexChanged);
	SHINE_Alpha_PresetsFrame->Initialize(SHINE_alphaPresets, iniFileName);

#ifdef NO_SHINE
   OpModePageControl->ActivePage = AF_TabSheet;
   FindAndReparentAutoFilterTabControlPanel(OpModePageControl);
   NoShineReparentPanel->Visible = true;
   OpModePageControl->Enabled = false;
   OpModePageControl->Visible = false;
#else
   OpModePageControl->ActivePage = SHINE_TabSheet;
#endif
}


bool TAutofilterGUI::FindAndReparentAutoFilterTabControlPanel(TControl *control)
{
   if (!control->InheritsFrom(__classid(TWinControl)))
   {
      return false;
   }

   TWinControl *twc = dynamic_cast<TWinControl*>(control);

   // Descend into the component
   for (int i = 0; i < twc->ControlCount; i++)
   {
      if (twc->Controls[i]->Name == "AutoFilterTabControlPanel")
      {
         twc->Controls[i]->Parent = NoShineReparentPanel;
         return true;
      }

      if (FindAndReparentAutoFilterTabControlPanel(twc->Controls[i]))
      {
         return true;
      }
   }

   return false;
}

// ---------------------------------------------------------------------------

void TAutofilterGUI::formShow(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	} // ERROR!

	SET_CBHOOK(ClipHasChanged, GAutoFilter->ClipChange);
	SET_CBHOOK(MarksHaveChanged, GAutoFilter->MarksChange);

	OpModePageControlChange(nullptr);
   AF_MakeSureAFilterIsSelected();
	AF_UpdateFilterDetailPanel();
	AF_FilterSetNameLabel->Caption = "";
	AF_UpdateFilterManagerGUI();
	AF_UpdateDefaultsButtons();
	SHINE_UpdateResetButtons();
   SHINE_Alpha_EnableButtonClick(nullptr);  // first! Want it to lose in case of conflict!
   SHINE_Dark_EnableButtonClick(nullptr);
   SHINE_Bright_EnableButtonClick(nullptr);

	if (UsePastFramesOnlyCheckBox->Checked != GAutoFilter->GetUsePastFramesOnlyFlag())
	{
		inhibitCallbacks = true;
		UsePastFramesOnlyCheckBox->Checked = GAutoFilter->GetUsePastFramesOnlyFlag();
		UsePastFramesOnlyCheckBoxClick(NULL);
		inhibitCallbacks = false;
	}

	updateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::formHide(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	} // ERROR!

	REMOVE_CBHOOK(ClipHasChanged, GAutoFilter->ClipChange);
	REMOVE_CBHOOK(MarksHaveChanged, GAutoFilter->MarksChange);

	AF_FilterManager->WriteToIniFile(*AF_AutofilterIniFileName);
//	SHINE_FilterManager->WriteToIniFile(*SHINE_AutofilterIniFileName);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::formDestroy(void)
{
	AF_FilterManager->WriteToIniFile(*AF_AutofilterIniFileName);
//	SHINE_FilterManager->WriteToIniFile(*SHINE_AutofilterIniFileName);
}
// ---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
	if (AutofilterGUI == NULL)
	{
		return false;
	}

	return AutofilterGUI->runExecButtonsCommand(command);
}

bool TAutofilterGUI::runExecButtonsCommand(EExecButtonId command)
{
	bool retVal = false;

	// gaaak nasty hack
	if (GAutoFilter->IsMaskInROIMode() && GAutoFilter->getSystemAPI()->IsMaskAvailable())
	{
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
	}

	retVal = ExecButtonsToolbar->RunCommand(command);

	// ultra nasty hack part II
	if (GAutoFilter->IsMaskInROIMode() && GAutoFilter->getSystemAPI()->IsMaskAvailable())
	{
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
	}

	return retVal;
}
// ---------------------------------------------------------------------------

bool IsExecButtonEnabled(EExecButtonId button)
{
	if (AutofilterGUI == NULL)
	{
		return false;
	}

	return AutofilterGUI->isExecButtonEnabled(button);
}

bool TAutofilterGUI::isExecButtonEnabled(EExecButtonId button)
{
	return ExecButtonsToolbar->IsButtonEnabled(button);
}
// ---------------------------------------------------------------------------

bool SetShowHighlights(bool flag)
{
	if (AutofilterGUI == NULL)
	{
		return false;
	}

	return AutofilterGUI->setShowHighlights(flag);
}

bool TAutofilterGUI::setShowHighlights(bool flag)
{
	bool oldSetting = isShowHighlight();
	if (flag != oldSetting)
	{
		HighlightRadioButton->Checked = flag;
		ProcessedRadioButton->Checked = !flag;
		if (GAutoFilter != NULL)
		{
			GAutoFilter->SetHighlightFlag(flag);
		}
	}

	return oldSetting;
}

bool IsShowHighlight()
{
	if (AutofilterGUI == NULL)
	{
		return false;
	}

	return AutofilterGUI->isShowHighlight();
}

bool TAutofilterGUI::isShowHighlight()
{
	return HighlightRadioButton->Checked;
}
// ---------------------------------------------------------------------------

bool TAutofilterGUI::ReadSettings(CIniFile *ini, const string &IniSection)
{
	// Read the "General" section

	HighlightRadioButton->Checked = ini->ReadBool(GENERAL_INI_SECTION_NAME, PREVIEW_HIGHLIGHT_KEY, HighlightRadioButton->Checked);
	if (!HighlightRadioButton->Checked)
	{
		ProcessedRadioButton->Checked = true;
	}

	return true;
}
// ---------------------------------------------------------------------------

bool TAutofilterGUI::WriteSettings(CIniFile *ini, const string &IniSection)
{
	// Erase the General section
	ini->EraseSection(GENERAL_INI_SECTION_NAME);

	// Write the "General" section
	ini->WriteBool(GENERAL_INI_SECTION_NAME, PREVIEW_HIGHLIGHT_KEY, HighlightRadioButton->Checked);
	return true;
}
// ---------------------------------------------------------------------------

// NOW ONLY USED FOR OLD AF!
// If selectedFilterOnly is false, this gathers settings for ALL of the filters
// whose Used check box is checked. Else parameters are gathered only for the
// the currently selected filter, which is the one whose information is showing
// in the filter details panel.
void GatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->gatherFilterParameters(toolParams, selectedFilterOnly);
}

void TAutofilterGUI::gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly)
{
	if (GAutoFilter == NULL)
	{
      // ERROR!
		return;
	}

   if (queryOpMode() == DebrisToolOpMode::SHINE)
   {
      SHINE_gatherFilterParameters(toolParams, selectedFilterOnly);
   }
   else
   {
      AF_gatherFilterParameters(toolParams, selectedFilterOnly);
   }

	// Hack to be able to force reset of the AF algorithm.
	toolParams.filterChangeCounter = _filterChangeCounter;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly)
{
	int i;
	toolParams.lNSetting = 0;

	for (i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		SFilterSlot slotInfo = activeFilterManager->GetActiveFilterSlotInfo(i);
		if ((!slotInfo.loaded) || (slotInfo.filterID == INVALID_ID))
		{
         // Done - the loaded slots are always consecutive
         break;
		}

		if (selectedFilterOnly)
		{
			if (AF_FilterBankFrame->GetSelectedFilterNumber() != i)
			{
				continue;
			}
		}
		else
		{
			if (!slotInfo.used)
			{
				// this one's disabled
				continue;
			}
		}

		SFilter filter = activeFilterManager->GetFilterDetails(slotInfo.filterID);

		toolParams.strSettingName[i] = filter.name ? *filter.name : "";

		// Bump the "valid setting set" counter
		int index = toolParams.lNSetting;
		++toolParams.lNSetting;

		// For YUV, detect channel is ALWAYS 0!
		EPixelComponents pixelComp = (EPixelComponents) GAutoFilter->getSystemAPI()->getPixelComponents();
		switch (pixelComp)
		{
		case IF_PIXEL_COMPONENTS_RGB:
		case IF_PIXEL_COMPONENTS_RGBA:
		case IF_PIXEL_COMPONENTS_BGR:
		case IF_PIXEL_COMPONENTS_BGRA:
			toolParams.lPrincipalComponent[index] = filter.detectChannel;
			break;
		default:
			toolParams.lPrincipalComponent[index] = 0;
			break;
		}

		// DETECT CHANNEL-INDEPENDENT PARAMETERS

		// Rampant manifest constants QQQ
		toolParams.lComponentMask[index] = (filter.processChannels[0] ? 1 : 0) + (filter.processChannels[1] ? 2 : 0) +
				(filter.processChannels[2] ? 4 : 0);

      toolParams.faMinVal[index][0] =  0; // (filter.minR * 100) / 255;
      toolParams.faMinVal[index][1] =  0; // (filter.minG * 100) / 255;
      toolParams.faMinVal[index][2] =  0; // (filter.minB * 100) / 255;

      toolParams.faMaxVal[index][0] =  100; // (filter.maxR * 100) / 255;
      toolParams.faMaxVal[index][1] =  100; // (filter.maxG * 100) / 255;
      toolParams.faMaxVal[index][2] =  100; // (filter.maxB * 100) / 255;

		toolParams.faMotionOffsetRow[index] = filter.rgbMotion;
		toolParams.faMotionOffsetCol[index] = filter.rgbMotion;
		toolParams.faMotionThresh[index] = filter.minFixConfidence; // (100 - filter.minFixConfidence);

		// Edge blur factor - make range 0.0 - 1.0 for sanity
		int edgeBlurMin, edgeBlurMax;
		AF_CorrectionEdgeBlurTrackEdit->GetMinAndMax(edgeBlurMin, edgeBlurMax);
		toolParams.faEdgeBlur[index] = filter.correctionEdgeBlur / float(edgeBlurMax);

		// Dust stuff
		toolParams.bUseHardMotionOnDust[index] = filter.useHardMotionOnDust;
		toolParams.lDustMaxSize[index] = filter.dustMaxSize;

		// This is sort of redundant, but I don't feel like tracing the
		// execution to see if I can get rid of it
		toolParams.bUseAlphaMatteToFindDebris[index] = (filter.detectChannel == ALPHA_FILTER_CHANNEL);

      if (toolParams.bUseAlphaMatteToFindDebris[index])
		{
			// ALPHA CHANNEL DETECT PARAMETERS

			// Grain filter stuff
			toolParams.lGrainFilterParam[index] = filter.grainFilterParam;
			// This used to have a control, should get rid of it now.
			toolParams.bUseGrainFilter[index] = filter.grainFilterParam > 0;
			toolParams.lLuminanceThreshold[index] = filter.luminanceThreshold;

			// CONTRAST IS NOT USED FOR ALPHA - but laContrastDirection MUST
			// BE SET TO 0 !!!
			toolParams.laContrastDirection[index] = 0;
			toolParams.faContrastSpatial[index] = 100;
			toolParams.faContrastTemporal[index] = 100;

			// Min Size not used for filtering alpha-detected debris
			toolParams.faMinSize[index] = 1; // no restriction
			toolParams.faMaxSize[index] = filter.maxSize;

         // Motion
         toolParams.faMotionOffsetRow[index] = filter.alphaMotion;
         toolParams.faMotionOffsetCol[index] = filter.alphaMotion;
         toolParams.faMotionThresh[index] = filter.minFixConfidence; // (100 - filter.minFixConfidence);
		}
		else
		{
			// RGB CHANNEL DETECT PARAMETERS

			// Contrast sensitivity and direction.
			// NOTE: this used to be (100 - filter.minContrast), but the slider was flipped.
			toolParams.faContrastSpatial[index] = filter.minContrast;
			toolParams.faContrastTemporal[index] = toolParams.faContrastSpatial[index];
			toolParams.laContrastDirection[index] = filter.brightOnDarkContrast ? 1 : -1;

			// Min/max sizes.
			toolParams.faMaxSize[index] = filter.maxSize;
			toolParams.faMinSize[index] = filter.minSize;

			// Grain and luminance filtering stuff - ALPHA ONLY, so disabled here.
			toolParams.lGrainFilterParam[index] = 0;
			toolParams.bUseGrainFilter[index] = false;
			toolParams.lLuminanceThreshold[index] = 100;

         // Motion
         toolParams.faMotionOffsetRow[index] = filter.rgbMotion;
         toolParams.faMotionOffsetCol[index] = filter.rgbMotion;
         toolParams.faMotionThresh[index] = filter.minFixConfidence; // (100 - filter.minFixConfidence);
		}
	}

	// Hack to be able to force reset of the AF algorithm.
	toolParams.filterChangeCounter = _filterChangeCounter;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_gatherFilterParameters(CAutoFilterParameters &toolParams, bool selectedFilterOnly)
{
	toolParams.lNSetting = 0;

   ShineToolSettings shineSettings;
   SHINE_gatherGuiParameters(shineSettings);
   if (shineSettings.detectDark)
   {
      int index = toolParams.lNSetting;
      ++toolParams.lNSetting;

      toolParams.strSettingName[index] = "SHINE_DARK";
      toolParams.lPrincipalComponent[index] = -1;  // Detect channel is not used
      toolParams.lComponentMask[index] = 7; // Process all channels
      toolParams.faMinVal[index][0] =  0; // (filter.minR * 100) / 255;
      toolParams.faMinVal[index][1] =  0; // (filter.minG * 100) / 255;
      toolParams.faMinVal[index][2] =  0; // (filter.minB * 100) / 255;
      toolParams.faMaxVal[index][0] =  100; // (filter.maxR * 100) / 255;
      toolParams.faMaxVal[index][1] =  100; // (filter.maxG * 100) / 255;
      toolParams.faMaxVal[index][2] =  100; // (filter.maxB * 100) / 255;
		toolParams.faMotionOffsetRow[index] = 0;  // Not used for detection
		toolParams.faMotionOffsetCol[index] = 0;  // Not used for detection
		toolParams.faMotionThresh[index] = SHINE_Dark_MotionToleranceTrackEdit->GetValue();
		toolParams.faEdgeBlur[index] = 0;   // Not used for detection
		toolParams.bUseHardMotionOnDust[index] = false;
		toolParams.lDustMaxSize[index] = 0;
      toolParams.bUseAlphaMatteToFindDebris[index] = false;
      toolParams.lGrainFilterParam[index] = 0;
      toolParams.bUseGrainFilter[index] = false;
      toolParams.lLuminanceThreshold[index] = 100;
      toolParams.faContrastSpatial[index] = SHINE_Dark_TemporalContrastTrackEdit->GetValue();
      toolParams.faContrastTemporal[index] = SHINE_Dark_SpatialContrastTrackEdit->GetValue();
      toolParams.laContrastDirection[index] = -1;  // "dark on bright"
      toolParams.faMaxSize[index] = SHINE_Dark_MaxSizeTrackEdit->GetValue();
      toolParams.faMinSize[index] = SHINE_Dark_MinSizeTrackEdit->GetValue();
   }

   if (shineSettings.detectBright)
   {
      int index = toolParams.lNSetting;
      ++toolParams.lNSetting;

      toolParams.strSettingName[index] = "SHINE_BRIGHT";
      toolParams.lPrincipalComponent[index] = -1;  // Detect channel is not used
      toolParams.lComponentMask[index] = 7; // Process all channels
      toolParams.faMinVal[index][0] =  0; // (filter.minR * 100) / 255;
      toolParams.faMinVal[index][1] =  0; // (filter.minG * 100) / 255;
      toolParams.faMinVal[index][2] =  0; // (filter.minB * 100) / 255;
      toolParams.faMaxVal[index][0] =  100; // (filter.maxR * 100) / 255;
      toolParams.faMaxVal[index][1] =  100; // (filter.maxG * 100) / 255;
      toolParams.faMaxVal[index][2] =  100; // (filter.maxB * 100) / 255;
		toolParams.faMotionOffsetRow[index] = 0;  // Not used for detection
		toolParams.faMotionOffsetCol[index] = 0;  // Not used for detection
		toolParams.faMotionThresh[index] = SHINE_Bright_MotionToleranceTrackEdit->GetValue();
		toolParams.faEdgeBlur[index] = 0;   // Not used for detection
		toolParams.bUseHardMotionOnDust[index] = false;
		toolParams.lDustMaxSize[index] = 0;
      toolParams.bUseAlphaMatteToFindDebris[index] = false;
      toolParams.lGrainFilterParam[index] = 0;
      toolParams.bUseGrainFilter[index] = false;
      toolParams.lLuminanceThreshold[index] = 100;
      toolParams.faContrastSpatial[index] = SHINE_Bright_TemporalContrastTrackEdit->GetValue();
      toolParams.faContrastTemporal[index] = SHINE_Bright_SpatialContrastTrackEdit->GetValue();
      toolParams.laContrastDirection[index] = 1;  // "bright on dark"
      toolParams.faMaxSize[index] = SHINE_Bright_MaxSizeTrackEdit->GetValue();
      toolParams.faMinSize[index] = SHINE_Bright_MinSizeTrackEdit->GetValue();
   }

   if (shineSettings.detectAlpha)
   {
      int index = toolParams.lNSetting;
      ++toolParams.lNSetting;

      toolParams.strSettingName[index] = "SHINE_ALPHA";
      toolParams.lPrincipalComponent[index] = -1;  // Detect channel is not used
      toolParams.lComponentMask[index] = 7; // Process all channels
      toolParams.faMinVal[index][0] =  0; // (filter.minR * 100) / 255;
      toolParams.faMinVal[index][1] =  0; // (filter.minG * 100) / 255;
      toolParams.faMinVal[index][2] =  0; // (filter.minB * 100) / 255;
      toolParams.faMaxVal[index][0] =  100; // (filter.maxR * 100) / 255;
      toolParams.faMaxVal[index][1] =  100; // (filter.maxG * 100) / 255;
      toolParams.faMaxVal[index][2] =  100; // (filter.maxB * 100) / 255;
		toolParams.faMotionOffsetRow[index] = 0;  // Not used for detection
		toolParams.faMotionOffsetCol[index] = 0;  // Not used for detection
		toolParams.faMotionThresh[index] = SHINE_Alpha_MotionToleranceTrackEdit->GetValue();
		toolParams.faEdgeBlur[index] = 0;   // Not used for detection
		toolParams.bUseHardMotionOnDust[index] = false;
		toolParams.lDustMaxSize[index] = 0;
      toolParams.bUseAlphaMatteToFindDebris[index] = true;
      toolParams.lGrainFilterParam[index] = SHINE_Alpha_GrainSuppressionTrackEdit->GetValue();
      toolParams.bUseGrainFilter[index] = true;
      toolParams.lLuminanceThreshold[index] = 100;
      toolParams.faContrastSpatial[index] = 100;
      toolParams.faContrastTemporal[index] = 100;
      toolParams.laContrastDirection[index] = 0;  // Must be 0 for alpha
      toolParams.faMaxSize[index] = SHINE_Alpha_MaxSizeTrackEdit->GetValue();
      toolParams.faMinSize[index] = 1; // no restriction for alpha
   }
}
// ---------------------------------------------------------------------------

void WriteAllFiltersToPDLEntry(CPDLElement &pdlEntryToolParams)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->writeAllFiltersToPDLEntry(pdlEntryToolParams);
}

void TAutofilterGUI::writeAllFiltersToPDLEntry(CPDLElement &pdlEntryToolParams)
{
	for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		SFilterSlot slotInfo = activeFilterManager->GetActiveFilterSlotInfo(i);
		if ((!slotInfo.loaded) || (slotInfo.filterID == INVALID_ID))
		{
      // Done - the loaded slots are always consecutive
			break;
		}

		SFilter filter = activeFilterManager->GetFilterDetails(slotInfo.filterID);
		bool enabledFlag = slotInfo.used;

		CPDLElement *pe = pdlEntryToolParams.MakeNewChild("FilterSpec");
		string opModeValue = (QueryOpMode() == DebrisToolOpMode::SHINE) ? "SHINE" : "AF";

		pe->SetAttribString(PDLFilterNameKey, *filter.name);
		pe->SetAttribString(PDLOpModeKey, opModeValue);
		pe->SetAttribBool(PDLFilterEnabledKey, enabledFlag);
		pe->SetAttribInteger(PDLMaxDebrisSizeKey, filter.maxSize);
		pe->SetAttribInteger(PDLMinDebrisSizeKey, filter.minSize);
		pe->SetAttribInteger(PDLFixConfidenceKey, filter.minFixConfidence);
		pe->SetAttribInteger(PDLMinContrastKey, filter.minContrast);
		pe->SetAttribInteger(PDLLuminanceThresholdKey, filter.luminanceThreshold);
		pe->SetAttribInteger(PDLShineSpatialContrastKey, filter.shineSpatialContrast);
		pe->SetAttribInteger(PDLShineTemporalContrastKey, filter.shineTemporalContrast);
		pe->SetAttribInteger(PDLShineMotionToleranceKey, filter.shineMotionTolerance);
      auto motion = (filter.detectChannel == ALPHA_FILTER_CHANNEL)
                        ? filter.alphaMotion
                        : filter.rgbMotion;
		pe->SetAttribInteger(PDLHMotionKey, motion);
		pe->SetAttribInteger(PDLVMotionKey, motion);
		pe->SetAttribBool(PDLBrightKey, filter.brightOnDarkContrast);

		pe->SetAttribInteger(PDLMaxDustSizeKey, filter.dustMaxSize);
		pe->SetAttribBool(PDLDustHardMotionKey, filter.useHardMotionOnDust);
		pe->SetAttribBool(PDLAlphaMaskKey, filter.useAlphaAsMask);
		pe->SetAttribInteger(PDLDetectCompKey, filter.detectChannel);
		int processmask = (filter.processChannels[0] ? 1 : 0) | (filter.processChannels[1] ? 2 : 0) | (filter.processChannels[2] ? 4 : 0);
		pe->SetAttribInteger(PDLProcessMaskKey, processmask);
		pe->SetAttribInteger(PDLEdgeBlurKey, filter.correctionEdgeBlur);
		pe->SetAttribBool(PDLGrainFilterKey, filter.useGrainFilter);
		pe->SetAttribInteger(PDLGrainFilterParamKey, filter.grainFilterParam);
	}
}
// ---------------------------------------------------------------------------

int ReadAllFiltersfromPDLEntry(const CPDLElement &pdlEntryToolParams)
{
	if (AutofilterGUI == NULL)
	{
		return false;
	}

	return AutofilterGUI->readAllFiltersfromPDLEntry(pdlEntryToolParams);
}

int TAutofilterGUI::readAllFiltersfromPDLEntry(const CPDLElement &parentElement)
{
	int filterCount = 0;

	AF_UnselectFilter(); // This also UngangAll()'s
	AF_FilterManager->ClearFilterSet(PDL_FILTER_SET_ID);
	AF_FilterManager->ActivateFilterSet(PDL_FILTER_SET_ID);

	for (unsigned int i = 0; i < parentElement.GetChildElementCount() && filterCount <= MAX_DISPLAYED_FILTERS; ++i)
	{
		CPDLElement *pe = parentElement.GetChildElement(i);
		if (pe->GetElementName() != "FilterSpec")
		{
			continue;
		}

		string &filterID = PDLFilterTable[filterCount];
		SFilter *filter = AF_FilterManager->GetFilter(filterID);
		MTIassert(filter != NULL);
		if (filter == NULL)
		{
			continue;
		}

		filter->SetName(pe->GetAttribString(PDLFilterNameKey) + " [PDL]");
		filter->maxSize = pe->GetAttribInteger(PDLMaxDebrisSizeKey);
		filter->minSize = pe->GetAttribInteger(PDLMinDebrisSizeKey);
		filter->minContrast = pe->GetAttribInteger(PDLMinContrastKey);
		filter->luminanceThreshold = pe->GetAttribInteger(PDLLuminanceThresholdKey);
		filter->shineSpatialContrast = pe->GetAttribInteger(PDLShineSpatialContrastKey);
		filter->shineTemporalContrast = pe->GetAttribInteger(PDLShineTemporalContrastKey);
		filter->shineMotionTolerance = pe->GetAttribInteger(PDLShineMotionToleranceKey);
		filter->minFixConfidence = pe->GetAttribInteger(PDLFixConfidenceKey);
		filter->rgbMotion = std::max<int>(pe->GetAttribInteger(PDLHMotionKey),
                                   pe->GetAttribInteger(PDLVMotionKey));
		filter->alphaMotion = filter->rgbMotion;
		filter->correctionEdgeBlur = pe->GetAttribInteger(PDLEdgeBlurKey);
		filter->brightOnDarkContrast = pe->GetAttribBool(PDLBrightKey);
		filter->dustMaxSize = pe->GetAttribInteger(PDLMaxDustSizeKey);
		filter->useHardMotionOnDust = pe->GetAttribBool(PDLDustHardMotionKey);
		filter->useAlphaAsMask = pe->GetAttribBool(PDLAlphaMaskKey);
		filter->detectChannel = pe->GetAttribInteger(PDLDetectCompKey);
		filter->useGrainFilter = pe->GetAttribBool(PDLGrainFilterKey);
		filter->grainFilterParam = pe->GetAttribInteger(PDLGrainFilterParamKey);

		int processTemp = pe->GetAttribInteger(PDLProcessMaskKey);
		filter->processChannels[0] = processTemp & 1;
		filter->processChannels[1] = processTemp & 2;
		filter->processChannels[2] = processTemp & 4;

		bool filterEnabled = pe->GetAttribBool(PDLFilterEnabledKey);
		activeFilterManager->LoadFilter(PDLFilterTable[filterCount], filterEnabled);

		++filterCount;
	}

   AF_MakeSureAFilterIsSelected();
  	AF_UpdateFilterManagerGUI();
	AF_UpdateFilterDetailPanel();
	AF_UpdateMultiSelectionIndicator();

	return filterCount;
}
// ---------------------------------------------------------------------------

void CleanUpAfterPDLExecution()
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	AutofilterGUI->cleanUpAfterPDLExecution();
}

void TAutofilterGUI::cleanUpAfterPDLExecution()
{
	// The filter set that was displayed before we started running the
	// PDL is still contained in the DEFAULT set, so just activate that
	AF_UnselectFilter(); // This also UngangAll()'s
	AF_FilterManager->ActivateFilterSet(AF_DEFAULT_FILTER_SET_ID);
   AF_MakeSureAFilterIsSelected();
	AF_UpdateFilterManagerGUI();
	AF_UpdateFilterDetailPanel();
	AF_UpdateMultiSelectionIndicator();

	SHINE_UpdateSettingsPanels();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::PreviewFrame(void)
{
	// Preview a single frame

	if (GAutoFilter == NULL)
	{
		return;
	} // Never can happen

	switch (GAutoFilter->GetToolProcessingControlState())
	{
	case TOOL_CONTROL_STATE_STOPPED:
	case TOOL_CONTROL_STATE_PAUSED:
		// Start Preview processing for the current frame.  This function will
		// only act if Previewing/Rendering is stopped or paused.
		GAutoFilter->StartSingleFramePreview(true); // isShowHighlight());
		setShowHighlights(true);
		break;

	case TOOL_CONTROL_STATE_RUNNING:
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
		// Preview or Render processing is running, so should not attempt
		// to do a preview 1 frame. We should never get here because the
		// control should be disabled if anything is happening... so just beep
		Beep();
		break;

	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
		// Preview 1 Frame is already running, so this function was probably
		// called because the user had changed a parameter.  Remember to run
		// Preview 1 Frame again later.
		bDoPreviewAgain = true;
		break;

	default:
		break;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::RenderFrame(void)
{
	// Render a single frame

	if (GAutoFilter == NULL)
	{
		return;
	} // Never can happen

	switch (GAutoFilter->GetToolProcessingControlState())
	{
	case TOOL_CONTROL_STATE_STOPPED:
	case TOOL_CONTROL_STATE_PAUSED:
		// Start Render processing for the current frame.  This function will
		// only act if Previewing/Rendering is stopped or paused.
		GAutoFilter->StartSingleFrameRender(isShowHighlight());
		break;

	case TOOL_CONTROL_STATE_RUNNING:
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
		// This should never happen - the control should be disabled
		// if anything is happening... so just beep!
		Beep();
		break;

	default:
		break;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ClipHasChanged(void *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	ExecSetResumeTimecodeToDefault();
	UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::MarksHaveChanged(void *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	EToolControlState toolControlState = GAutoFilter->GetToolProcessingControlState();
	if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
	{
		updateExecutionButtonToolbar(toolControlState, false);
	}
}
// ---------------------------------------------------------------------------

string TAutofilterGUI::GetNameForNewFilter(void)
{
	if (GAutoFilter == NULL)
	{
		// Never can happen
		return string("New Filter");
	}

	string retVal;

	if (NewFilterNameDialog == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		NewFilterNameDialog = new TNewFilterForm(owner);
	}

	NewFilterNameDialog->SetDefaultName(activeFilterManager->UniquifyFilterName(DEFAULT_CUSTOM_FILTER_NAME));

	int result = ShowModalDialog(NewFilterNameDialog);

	if (result == mrOk)
	{
		retVal = activeFilterManager->UniquifyFilterName(NewFilterNameDialog->GetNewFilterName());
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete NewFilterNameDialog;
	NewFilterNameDialog = NULL;

	return retVal;
}
// ---------------------------------------------------------------------------

string TAutofilterGUI::GetNameForNewFilterSet(void)
{
	string retVal;
	vector<pair<string, string> >setList;
	vector<string>nameList;

	if (NewFilterSetNameDialog == NULL)
	{
		// We want the base tool to own the form so it gets displayed at the
		// center of the base tool
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		NewFilterSetNameDialog = new TNewFilterSetForm(owner);
	}

	activeFilterManager->GetSortedListOfFilterSetNames(setList);
	for (unsigned i = 0; i < setList.size(); ++i)
	{
		nameList.push_back(setList[i].first);
	}

	NewFilterSetNameDialog->SetListOfFilterSetNames(nameList);

	int result = ShowModalDialog(NewFilterSetNameDialog);

	if (result == mrOk)
	{
		retVal = activeFilterManager->UniquifyFilterSetName(NewFilterSetNameDialog->GetNewSetName());
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete NewFilterSetNameDialog;
	NewFilterSetNameDialog = NULL;

	// If a name was entered, make sure it's unique!
	if (!retVal.empty())
	{
		if (INVALID_ID != activeFilterManager->FindFilterSetByName(retVal))
		{
			for (int ordinal = 2; ordinal < 1000; ++ordinal)
			{
				MTIostringstream os;
				os << retVal << " (" << ordinal << ")";
				string tryName = os.str();
				if (INVALID_ID == activeFilterManager->FindFilterSetByName(tryName))
				{
					retVal = tryName;
					break;
				}
			}
		}
	}

	return retVal;
}
// ---------------------------------------------------------------------------

SFilter *TAutofilterGUI::AF_CreateNewFilterFromFilterDetails(void)
{
	SFilter *newFilter = new SFilter;
	AF_FillInFilterDetailsFromGUI(*newFilter);

	return newFilter;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FillInFilterDetailsFromGUI(SFilter &filter, unsigned int mask)
{
	// Note: NOT NAME!
	// if (isSetInGangMask(mask, GangParam_FilterName))

	// Channels
	if (isSetInGangMask(mask, GangParam_DetectChannel))
	{
		filter.detectChannel = AF_GetDetectChannelFromGUI();
	}

	if (isSetInGangMask(mask, GangParam_ProcessChannels))
	{
		AF_GetProcessChannelsFromGUI(filter.processChannels[RED_FILTER_CHANNEL], filter.processChannels[GREEN_FILTER_CHANNEL],
				filter.processChannels[BLUE_FILTER_CHANNEL]);
	}

	// Max size
	if (isSetInGangMask(mask, GangParam_MaxSize))
	{
		filter.maxSize = AF_MaxSizeTrackEdit->GetValue();
	}

	// For RGB channels
	if (filter.detectChannel == ALPHA_FILTER_CHANNEL)
	{
		if (isSetInGangMask(mask, GangParam_UseGrainFilter))
		{
			filter.useGrainFilter = AF_UseGrainFilterCheckBox->Checked;
		}

		if (isSetInGangMask(mask, GangParam_GrainFilterParam))
		{
			filter.grainFilterParam = AF_AlphaGrainSuppressionTrackEdit->GetValue();
		}

      if (isSetInGangMask(mask, GangParam_LuminanceThreshold))
      {
         filter.luminanceThreshold = AF_LuminanceThresholdTrackEdit->GetValue();
      }

		filter.brightOnDarkContrast = true; // QQQ would like to have BOTH checked!
		filter.minSize = 0;
		filter.minContrast = 0;
	}
	else
	{
		if (isSetInGangMask(mask, GangParam_DebrisContrast))
		{
			filter.brightOnDarkContrast = AF_DebrisContrastBrightRadioButton->Checked;
		}

		if (isSetInGangMask(mask, GangParam_MinSize))
		{
			filter.minSize = AF_MinSizeTrackEdit->GetValue();
		}

		if (isSetInGangMask(mask, GangParam_ContrastSensitivity))
		{
			filter.minContrast = AF_MinContrastTrackEdit->GetValue();
		}

		filter.useGrainFilter = false;
		filter.grainFilterParam = 0;
      filter.luminanceThreshold = 100;
	}

	// Fix-related parameters for ALL detect channels
	if (isSetInGangMask(mask, GangParam_FixTolerance))
	{
		filter.minFixConfidence = AF_FixToleranceTrackEdit->GetValue();
	}
	if (isSetInGangMask(mask, GangParam_Motion))
	{
		auto motion = (AF_GetDetectChannelFromGUI() == ALPHA_FILTER_CHANNEL)
                     ? AF_AlphaMotionTrackEdit->GetValue()
                     : AF_RgbMotionTrackEdit->GetValue();
      filter.rgbMotion = motion;
      filter.alphaMotion = motion;
	}
	if (isSetInGangMask(mask, GangParam_EdgeBlur))
	{
		filter.correctionEdgeBlur = AF_CorrectionEdgeBlurTrackEdit->GetValue();
	}
	if (isSetInGangMask(mask, GangParam_UseHardMotionForDust))
	{
		filter.useHardMotionOnDust = AF_UseHardMotionForDustCheckBox->Checked;
	}
	if (isSetInGangMask(mask, GangParam_MaxDustSize))
	{
		filter.dustMaxSize = AF_DustMaxSizeTrackEdit->GetValue();
	}
	if (isSetInGangMask(mask, GangParam_UseAlphaAsMask))
	{
		filter.useAlphaAsMask = AF_UseAlphaAsMaskCheckBox->Checked;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FillInGangedFilterDetails(unsigned int detailMask)
{
	for (int slotNumber = 0; slotNumber < NUMBER_OF_FILTERS_PER_SET; ++slotNumber)
	{
		if (AF_FilterBankFrame->IsGanged(slotNumber))
		{
			string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(slotNumber);
			SFilter *filter = AF_FilterManager->GetFilter(filterID);

			MTIassert(filter != NULL);
			if (filter != NULL)
			{
				AF_FillInFilterDetailsFromGUI(*filter, detailMask);
			}
		}
	}

	AF_UpdateFilterManagerGUI();
	AF_UpdateDefaultsButtons();
	AF_UpdateMultiSelectionIndicator();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_UpdateFilterDetailPanel()
{
	if (AF_SelectedFilterID == INVALID_ID)
	{
		AF_NoFilterSelectedPanel->Visible = true; // Was Label
		AF_FilterNameLabel->Caption = "";
	}
	else
	{
		SFilter filter = AF_FilterManager->GetFilterDetails(AF_SelectedFilterID);

		AF_NoFilterSelectedPanel->Visible = false; // Was label
		filter.name->empty();
		if (filter.name->empty())
		{
			AF_FilterNameLabel->Caption = "[Unnamed filter]";
		}
		else
		{
			AF_FilterNameLabel->Caption = filter.name->c_str();
		}

		AF_UpdateFilterDetailGUIFromFilter(filter);

		bool defaultsAreUseful = AF_FilterManager->DoesFilterDifferFromDefaults(AF_SelectedFilterID);
		AF_ResetButton->Enabled = defaultsAreUseful;
		AF_SaveDefaultsButton->Enabled = defaultsAreUseful;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_UpdateFilterDetailGUIFromFilter(const SFilter &filter)
{
	inhibitCallbacks = true;

	bool alpha = filter.detectChannel == ALPHA_FILTER_CHANNEL; // xyzzy
	AF_MaxSizeTrackEdit->SetValue(filter.maxSize); // xyzzy
	AF_MinSizeTrackEdit->SetValue(alpha ? 0 : filter.minSize); // xyzzy
	AF_MinContrastTrackEdit->SetValue(alpha ? 0 : filter.minContrast); // xyzzy
	AF_LuminanceThresholdTrackEdit->SetValue(alpha ? filter.luminanceThreshold : 0); // xyzzy
	AF_FixToleranceTrackEdit->SetValue(filter.minFixConfidence);
	AF_RgbMotionTrackEdit->SetValue(filter.rgbMotion); // xyzzy
	AF_AlphaMotionTrackEdit->SetValue(filter.alphaMotion); // xyzzy
	AF_CorrectionEdgeBlurTrackEdit->SetValue(filter.correctionEdgeBlur);
	AF_DebrisContrastBrightRadioButton->Checked = alpha ? false : filter.brightOnDarkContrast;
	AF_DebrisContrastDarkRadioButton->Checked = alpha ? false : !filter.brightOnDarkContrast;
	AF_UseHardMotionForDustCheckBox->Checked = filter.useHardMotionOnDust;
	AF_DustMaxSizeTrackEdit->SetValue(filter.dustMaxSize);
	AF_UseGrainFilterCheckBox->Checked = filter.useGrainFilter;
	AF_AlphaGrainSuppressionTrackEdit->SetValue(filter.grainFilterParam);

	AF_SyncSizeRelatedControls();

	AF_SetGUIDetectChannel(filter.detectChannel);
	AF_SetGUIProcessChannels(filter.processChannels[RED_FILTER_CHANNEL], filter.processChannels[GREEN_FILTER_CHANNEL],
			filter.processChannels[BLUE_FILTER_CHANNEL]);

	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

// There is a problem here with ganged filters because that info is stored
// in the filter display bank, which is what we are reconstructing here,
// inappropriate memory of a previous ganging!!

void TAutofilterGUI::AF_UpdateFilterManagerGUI(void)
{
	inhibitCallbacks = true;

	int foundAnUnloadedSlot = false;
	int foundSelection = false;

	for (int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
	{
		SFilterSlot slotInfo = AF_FilterManager->GetActiveFilterSlotInfo(slot);
		if (slotInfo.loaded)
		{
			SFilter filter = AF_FilterManager->GetFilterDetails(slotInfo.filterID);
			AF_FilterBankFrame->Use(slot, slotInfo.used);
			AF_FilterBankFrame->SetFilterName(slot, *filter.name);

			AF_FilterBankFrame->AF_SetValues(
               slot,
               filter.maxSize,
               filter.minSize,
               filter.dustMaxSize,
					(filter.detectChannel == ALPHA_FILTER_CHANNEL) ? filter.grainFilterParam : filter.minContrast, filter.minFixConfidence,
					filter.correctionEdgeBlur,
               filter.rgbMotion, // xyzzy
					filter.alphaMotion); // xyzzy
			AF_FilterBankFrame->Enable(slot, true);
			if (slotInfo.filterID == AF_SelectedFilterID)
			{
				AF_FilterBankFrame->Select(slot);
				foundSelection = true;
			}
			else
			{
				AF_FilterBankFrame->Unselect(slot);
			}
		}
		else
		{
			AF_FilterBankFrame->Enable(slot, false);
			foundAnUnloadedSlot = true;
		}
	}

	AF_NewFilterButton->Enabled = foundAnUnloadedSlot;
	AF_LoadFilterButton->Enabled = foundAnUnloadedSlot;
	AF_UnloadFilterButton->Enabled = foundSelection;
	AF_DeleteFilterButton->Enabled = true; // foundSelection;

	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_UpdateDefaultsButtons(void)
{
	bool defaultsAreUseful = false;

	for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		if (AF_FilterBankFrame->IsEnabled(i) && AF_FilterBankFrame->IsGanged(i))
		{
			string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(i);
			if (AF_FilterManager->DoesFilterDifferFromDefaults(filterID))
			{
				defaultsAreUseful = true;
				break;
			}
		}
	}

	AF_ResetButton->Enabled = defaultsAreUseful;
	AF_SaveDefaultsButton->Enabled = defaultsAreUseful;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_UpdateMultiSelectionIndicator()
{
	bool multiSelection = (AF_FilterBankFrame->GetGangCount() > 1);
	AF_MultipleFiltersSelectedPanel->Visible = multiSelection;
	AF_UnloadFilterButton->Caption = AnsiString("Unload Filter") + AnsiString(multiSelection ? "s" : "");
	AF_DeleteFilterButton->Caption = AnsiString("Delete Filters"); // +
	// AnsiString(multiSelection? "s" : "");
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::FocusControl(Controls::TWinControl* Control)
{
	if ((GAutoFilter == NULL) || GAutoFilter->IsDisabled())
	{
		return;
	}

	GAutoFilter->getSystemAPI()->FocusAdoptedControl(reinterpret_cast<int *>(Control));
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SelectionChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	int selectedSlotNumber = AF_FilterBankFrame->GetSelectedFilterNumber();
	if (selectedSlotNumber == INVALID_FILTER_NUMBER)
	{
		AF_SelectedFilterID = INVALID_ID;
	}
	else
	{
		AF_SelectedFilterID = activeFilterManager->GetFilterIDFromSlotNumber(selectedSlotNumber);
	}

	AF_UpdateFilterDetailPanel();
	AF_UpdateFilterManagerGUI();
	AF_UpdateMultiSelectionIndicator();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_GangChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_UpdateFilterManagerGUI();
	AF_UpdateMultiSelectionIndicator();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FilterUseChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	for (int slotNumber = 0; slotNumber < NUMBER_OF_FILTERS_PER_SET; ++slotNumber)
	{
		string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(slotNumber);
		// CAREFUL!  IsFilterUsed() may not reflect the actual state of the
		// "use" checkbox if the filter is not enabled, so don't toggle it then!
		if ((filterID != INVALID_ID) && AF_FilterBankFrame->IsEnabled(slotNumber) &&
				(AF_FilterManager->IsFilterUsed(filterID) != AF_FilterBankFrame->IsUsed(slotNumber)))
		{
			AF_FilterManager->ToggleFilterUsed(filterID);
		}
	}

	AF_updateDetectionMap();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FilterDetailsChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	if (AF_SelectedFilterID != INVALID_ID)
	{
		SFilter *filter = AF_FilterManager->GetFilter(AF_SelectedFilterID);
		if (filter != NULL)
		{
			AF_FillInFilterDetailsFromGUI(*filter);
			AF_UpdateFilterManagerGUI();
			AF_UpdateDefaultsButtons();
		}
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_MaxSizeChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	unsigned int mask = gangMask(GangParam_MaxSize);
	AF_FillInGangedFilterDetails(mask);
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_MinSizeChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	unsigned int mask = gangMask(GangParam_MinSize);
	AF_FillInGangedFilterDetails(mask);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_DustMaxSizeChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	unsigned int mask = gangMask(GangParam_MaxDustSize);

	inhibitCallbacks = true;
	AF_SyncSizeRelatedControls();
	inhibitCallbacks = false;

	AF_FillInGangedFilterDetails(mask);

	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_GrainFilterParamChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	// Always keep hard motion checkbox in sync with trackedit
	if (AF_AlphaGrainSuppressionTrackEdit->GetValue() == 0)
	{
		AF_UseGrainFilterCheckBox->Enabled = false;
		AF_UseGrainFilterCheckBox->Font->Color = clBtnShadow;
	}
	else
	{
		AF_UseGrainFilterCheckBox->Enabled = true;
		AF_UseGrainFilterCheckBox->Font->Color = clBtnText;
	}

	unsigned int mask = gangMask(GangParam_GrainFilterParam);
	AF_FillInGangedFilterDetails(mask);

	AF_initiateDetectionMapLagTiming();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SyncSizeRelatedControls()
{
	// Always keep hard motion checkbox in sync with trackedit
	if (AF_DustMaxSizeTrackEdit->GetValue() == 0)
	{
		AF_UseHardMotionForDustCheckBox->Enabled = false;
		AF_UseHardMotionForDustCheckBox->Font->Color = clBtnShadow;
	}
	else
	{
		AF_UseHardMotionForDustCheckBox->Enabled = true;
		AF_UseHardMotionForDustCheckBox->Font->Color = clBtnText;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_MinContrastChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_ContrastSensitivity));
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_LuminanceThresholdChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_LuminanceThreshold));
	AF_initiateDetectionMapLagTiming();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FixToleranceChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_FixTolerance));
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_RgbMotionChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_Motion));
	AF_initiateContinuousPreviewLagTiming(); // instead
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_AlphaMotionChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_Motion));
	AF_initiateContinuousPreviewLagTiming(); // instead
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_EdgeBlurChanged(void *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_EdgeBlur));
	AF_initiateContinuousPreviewLagTiming(); // instead
}
// ---------------------------------------------------------------------------

int TAutofilterGUI::AF_GetDetectChannelFromGUI()
{
	return (AF_DetectRedButton->Down ? RED_FILTER_CHANNEL : (AF_DetectGreenButton->Down ? GREEN_FILTER_CHANNEL :
			(AF_DetectBlueButton->Down ? BLUE_FILTER_CHANNEL : ALPHA_FILTER_CHANNEL)));
}
// ---------------------------------------------------------------------------

int TAutofilterGUI::AF_GetProcessChannelsFromGUI(bool &processR, bool &processG, bool &processB)
{
	int retVal = (AF_ProcessRedButton->Down ? 1 : 0) + (AF_ProcessGreenButton->Down ? 1 : 0) + (AF_ProcessBlueButton->Down ? 1 : 0);

	processR = AF_ProcessRedButton->Down;
	processG = AF_ProcessGreenButton->Down;
	processB = AF_ProcessBlueButton->Down;

	return retVal;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SetGUIDetectChannel(int channel)
{
	switch (channel)
	{
	case RED_FILTER_CHANNEL:
		AF_DetectRedButton->Down = true;
		AF_HilitedDetectRedButton->Down = true;
		AF_DetectRRadioButton->Checked = true;
		break;
	case GREEN_FILTER_CHANNEL:
		AF_DetectGreenButton->Down = true;
		AF_HilitedDetectGreenButton->Down = true;
		AF_DetectGRadioButton->Checked = true;
		break;
	case BLUE_FILTER_CHANNEL:
		AF_DetectBlueButton->Down = true;
		AF_HilitedDetectBlueButton->Down = true;
		AF_DetectBRadioButton->Checked = true;
		break;
	case ALPHA_FILTER_CHANNEL:
		AF_DetectAlphaButton->Down = true;
		AF_HilitedDetectAlphaButton->Down = true;
		AF_DetectARadioButton->Checked = true;
		break;
	}

	AF_ConformAlphaFilterEnabling(AF_DetectARadioButton->Checked);
	AF_updateDetectionMap();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SetGUIProcessChannels(bool processR, bool processG, bool processB)
{
	// AF_ProcessRedButton->Down = processR;
	// AF_HilitedProcessRedButton->Down = processR;
	AF_ProcessRShadowCheckBox->Checked = processR;
	// PAF_rocessGreenButton->Down = processG;
	// AF_HilitedProcessGreenButton->Down = processG;
	AF_ProcessGShadowCheckBox->Checked = processG;
	// AF_ProcessBlueButton->Down = processB;
	// AF_HilitedProcessBlueButton->Down = processB;
	AF_ProcessBShadowCheckBox->Checked = processB;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::MaybeHandleUserInput(void *Sender)
{
	if (activeFilterManager == AF_FilterManager)
	{
		AF_MaybeHandleUserInput(Sender);
	}
	else
	{
		SHINE_MaybeHandleUserInput(Sender);
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_MaybeHandleUserInput(void *Sender)
{
	bool retVal = true; // Assume we will consume it
	string newFilterID;

	switch (GAutoFilter->WhatCommandNumberDidYouGet())
	{
	case AF_CMD_FOCUS_ON_MAX_SIZE:
		FocusControl(AF_MaxSizeTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_SIZE:
		FocusControl(AF_MinSizeTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_DUST_SIZE:
		FocusControl(AF_DustMaxSizeTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_CONTRAST:
      FocusControl((AF_DetectARadioButton->Checked)
                     ? AF_LuminanceThresholdTrackEdit->TrackBar
                     : AF_MinContrastTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE:
		FocusControl(AF_FixToleranceTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MOTION:
      FocusControl((AF_DetectARadioButton->Checked)
                     ? AF_AlphaMotionTrackEdit->TrackBar
                     : AF_RgbMotionTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_CORRECTION_EDGE_BLUR:
		FocusControl(AF_CorrectionEdgeBlurTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_GRAIN_FILTER_PARAM:
		FocusControl(AF_AlphaGrainSuppressionTrackEdit->TrackBar);
		break;

	case AF_CMD_TOGGLE_DEBRIS_CONTRAST:
		///GAutoFilter->resolveProvisional();
		if (AF_DebrisContrastBrightRadioButton->Checked)
		{
			AF_DebrisContrastDarkRadioButton->Checked = true;
		}
		else
		{
			AF_DebrisContrastBrightRadioButton->Checked = true;
		}
		break;

	case AF_CMD_CYCLE_DETECT_CHANNEL:
		if (AF_DetectRRadioButton->Checked)
		{
			AF_DetectGRadioButton->Checked = true;
		}
		else if (AF_DetectGRadioButton->Checked)
		{
			AF_DetectBRadioButton->Checked = true;
		}
		else if (AF_DetectBRadioButton->Checked && AF_DetectAlphaButton->Enabled)
		{
			AF_DetectARadioButton->Checked = true;
		}
		else
		{
			AF_DetectRRadioButton->Checked = true;
		}
		break;

	case AF_CMD_FOCUS_ON_PROCESS_CHANNELS:
		FocusControl(AF_ProcessRShadowCheckBox);
		break;

	case AF_CMD_SELECT_PREVIOUS_FILTER:
		newFilterID = AF_FilterManager->GetNextLoadedFilterID(AF_SelectedFilterID, -1);
		AF_SelectFilter(newFilterID);
		break;

	case AF_CMD_SELECT_NEXT_FILTER:
		newFilterID = AF_FilterManager->GetNextLoadedFilterID(AF_SelectedFilterID, +1);
		AF_SelectFilter(newFilterID);
		break;

	case AF_CMD_TOGGLE_USE_SELECTED_FILTER:
		AF_FilterManager->ToggleFilterUsed(AF_SelectedFilterID);
		AF_UpdateFilterManagerGUI();
		break;

	case AF_CMD_NEW_FILTER:
		AF_NewFilterButtonClick(NULL);
		break;

	case AF_CMD_LOAD_FILTER:
		AF_LoadFilterButtonClick(NULL);
		break;

	case AF_CMD_UNLOAD_FILTER:
		AF_UnloadFilterButtonClick(NULL);
		break;

	case AF_CMD_DELETE_FILTER:
		AF_DeleteFilterButtonClick(NULL);
		break;

	case AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS:
		AF_SaveDefaultsButtonClick(NULL);
		break;

	case AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS:
		AF_ResetButtonClick(NULL);
		break;

	case AF_CMD_CREATE_FILTER_SET:
		AF_SaveSetButtonClick(NULL);
		break;

	case AF_CMD_UPDATE_FILTER_SET:
		AF_UpdateSetButtonClick(NULL);
		break;

	case AF_CMD_LOAD_FILTER_SET:
		AF_LoadSetButtonClick(NULL);
		break;

	case AF_CMD_DELETE_FILTER_SET:
		AF_DeleteSetButtonClick(NULL);
		break;

	default:
		retVal = false;
		break;
	}

	GAutoFilter->SetGotUserInputNotifyResult(retVal);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_MaybeHandleUserInput(void *Sender)
{
	bool retVal = true; // Assume we will consume it
	string newFilterID;

	switch (GAutoFilter->WhatCommandNumberDidYouGet())
	{
	case AF_CMD_FOCUS_ON_MAX_SIZE:
//		FocusControl(SHINE_MaxSizeTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_SIZE:
//		FocusControl(SHINE_MinSizeTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_CONTRAST:
//		FocusControl(SHINE_ContrastTrackEdit->TrackBar);
		break;

	case AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE:
//		FocusControl(SHINE_MotionToleranceTrackEdit->TrackBar);
		break;

	case AF_CMD_TOGGLE_DEBRIS_CONTRAST:
//		if (SHINE_DebrisContrastBrightRadioButton->Checked)
//		{
//			SHINE_DebrisContrastDarkRadioButton->Checked = true;
//		}
//		else
//		{
//			SHINE_DebrisContrastBrightRadioButton->Checked = true;
//		}

		break;

	case AF_CMD_CYCLE_DETECT_CHANNEL:
		break;

	case AF_CMD_FOCUS_ON_PROCESS_CHANNELS:
		break;

	case AF_CMD_SELECT_PREVIOUS_FILTER:
		break;

	case AF_CMD_SELECT_NEXT_FILTER:
		break;

	case AF_CMD_TOGGLE_USE_SELECTED_FILTER:
		break;

	case AF_CMD_NEW_FILTER:
		break;

	case AF_CMD_LOAD_FILTER:
		break;

	case AF_CMD_UNLOAD_FILTER:
		break;

	case AF_CMD_DELETE_FILTER:
		break;

	case AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS:
		break;

	case AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS:
		break;

	case AF_CMD_CREATE_FILTER_SET:
		break;

	case AF_CMD_UPDATE_FILTER_SET:
		break;

	case AF_CMD_LOAD_FILTER_SET:
		break;

	case AF_CMD_DELETE_FILTER_SET:
		break;

	default:
		retVal = false;
		break;
	}

	GAutoFilter->SetGotUserInputNotifyResult(retVal);
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ------------------------ EXEC BUTTONS TOOLBAR -----------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender)
{
	switch (ExecButtonsToolbar->GetLastButtonClicked())
	{
	default:
	case EXEC_BUTTON_NONE:
		// No-op
		break;

	case EXEC_BUTTON_PREVIEW_FRAME:
		PreviewFrame();
		break;

	case EXEC_BUTTON_PREVIEW_ALL:
		ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
		break;

	case EXEC_BUTTON_RENDER_FRAME:
		RenderFrame();
		break;

	case EXEC_BUTTON_RENDER_ALL:
		ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
		break;

	case EXEC_BUTTON_PAUSE_OR_RESUME:
		if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
		{
			ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
		}
		else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
		{
			ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
		}
		break;

	case EXEC_BUTTON_PAUSE:
		ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
		break;

	case EXEC_BUTTON_RESUME:
		ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
		break;

	case EXEC_BUTTON_STOP:
		ExecStop();
		break;

	case EXEC_BUTTON_CAPTURE_PDL:
      if (queryOpMode() == DebrisToolOpMode::AF)
      {
		   ExecCaptureToPDL();
      }

		break;

	case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      if (queryOpMode() == DebrisToolOpMode::AF)
      {
		   ExecCaptureAllEventsToPDL();
      }

		break;

	case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
		ExecGoToResumeTimecode();
		break;

	case EXEC_BUTTON_SET_RESUME_TC:
		ExecSetResumeTimecodeToCurrent();
		break;
	}
}
// ---------------- Resume Timecode Functions --------------------------------

void TAutofilterGUI::ExecSetResumeTimecode(int frameIndex)
{
	CVideoFrameList *frameList = GAutoFilter->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
	{
		return;
	}

	if (frameIndex < 0)
	{
		frameIndex = frameList->getInFrameIndex();
	}

	PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
	ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecSetResumeTimecodeToCurrent(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	int frameIndex = GAutoFilter->getSystemAPI()->getLastFrameIndex();
	ExecSetResumeTimecode(frameIndex);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecSetResumeTimecodeToDefault(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	int frameIndex = GAutoFilter->getSystemAPI()->getMarkIn();
	ExecSetResumeTimecode(frameIndex);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecGoToResumeTimecode(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	CVideoFrameList *frameList;
	frameList = GAutoFilter->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
	{
		return;
	}

	// NO! GAutoFilter->getSystemAPI()->AcceptGOV();  // Auto-accept GOV if pending

	PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
	int frameIndex = frameList->getFrameIndex(resumeTimecode);
	GAutoFilter->getSystemAPI()->goToFrameSynchronous(frameIndex);

	// In case it ends up somewhere else?
	frameIndex = GAutoFilter->getSystemAPI()->getLastFrameIndex();
	ExecButtonsToolbar->SetResumeTimecode(frameList->getTimecodeForFrameIndex(frameIndex));
}

// ------------------ ExecRenderOrPreview ------------------------------------
//
// Call this when any render or preview button is clicked
// buttonCommand identifies the button that was pressed
//
void TAutofilterGUI::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
	switch (buttonCommand)
	{
	case TOOL_PROCESSING_CMD_RENDER:
		GAutoFilter->StartMultiFrameRender(isShowHighlight());
		break;

	case TOOL_PROCESSING_CMD_PREVIEW:
		GAutoFilter->StartMultiFramePreview(isShowHighlight());
		break;

	case TOOL_PROCESSING_CMD_PAUSE:
		GAutoFilter->PauseToolProcessing();
		break;

	case TOOL_PROCESSING_CMD_CONTINUE:
		{
			CVideoFrameList *frameList;
			frameList = GAutoFilter->getSystemAPI()->getVideoFrameList();
			int resumeFrame;
			if (frameList == NULL)
			{
				resumeFrame = GAutoFilter->getSystemAPI()->getMarkIn();
			}
			else
			{
				PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
				resumeFrame = frameList->getFrameIndex(resumeTimecode);
			}

			GAutoFilter->ContinueToolProcessing(resumeFrame);
		} break;

	default:
		break;
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecStop(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->StopToolProcessing();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecCaptureToPDL(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	bool all = false;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
	{
		all = true;
	}

	GAutoFilter->getSystemAPI()->CapturePDLEntry(all);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::ExecCaptureAllEventsToPDL(void)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->getSystemAPI()->CapturePDLEntry(true);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::updateExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	if (GAutoFilter->IsDisabled() || GAutoFilter->IsMaskInROIMode())
	{
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
		// GAutoFilter->IsMaskInROIMode()?
		// "Can't preview - in ROI Mode" :
		// "Can't preview - tool is disabled");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
		// GAutoFilter->IsMaskInROIMode()?
		// "Can't render - in ROI Mode" :
		// "Can't render - tool is disabled");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
		// GAutoFilter->IsMaskInROIMode()?
		// "Can't add to PDL - in ROI Mode" :
		// "Can't add to PDL - tool is disabled");
		return;
	}

	switch (toolProcessingControlState)
	{
	case TOOL_CONTROL_STATE_STOPPED:

		// Previewing or Rendering has stopped, either because processing
		// has completed or the user pressed the Stop button

		setIdleCursor();

		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_FRAME);

		if (GAutoFilter->AreMarksValid())
		{
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
         if (queryOpMode() == DebrisToolOpMode::SHINE)
         {
            // This is not yet implemented QQQ
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "NOT YET IMPLEMENTED");
			   ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
         }
         else
         {
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Add to PDL (, key)");
			   ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
         }

			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
			// "Preview marked range (Shift+D)");
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
			// "Render marked range (Shift+G)");
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
			// "Add to PDL (, key)");
		}
		else
		{
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
			// "Can't preview - marks are invalid");
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
			// "Can't render - marks are invalid");
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
			// "Can't add to PDL - marks are invalid");
		}

		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

		ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

		ExecButtonsToolbar->EnableResumeWidget();
		ExecSetResumeTimecodeToDefault();

		break;

	case TOOL_CONTROL_STATE_RUNNING:

		// Preview1, Render1, Preview or Render is now running

		setBusyCursor();

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		if (!processingRenderFlag)
		{
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
		}
		else
		{
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);
		}

		break;

	case TOOL_CONTROL_STATE_PAUSED:
		// Previewing or Rendering is now Paused

		setIdleCursor();

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		// Now go load the current timecode
		ExecSetResumeTimecodeToCurrent();

		break;

	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:

		// Processing single frame or waiting to stop or pause --
		// disable everything
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE && toolProcessingControlState !=
				TOOL_CONTROL_STATE_WAITING_TO_STOP)
		{
			if (!processingRenderFlag)
			{
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
			}
			else
			{
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
			}
		}
		else if (toolProcessingControlState == TOOL_CONTROL_STATE_WAITING_TO_STOP)
		{
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_STOP);
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		}

		break;

	default:
		break;
	}

	// UpdateStatusBarInfo();

} // UpdateExecutionButton()
// ---------------------------------------------------------------------------

void TAutofilterGUI::setIdleCursor(void)
{
	Screen->Cursor = crDefault;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::setBusyCursor(void)
{
	Screen->Cursor = crAppStart;
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::PreviewDisplayRadioButtonClick(TObject *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->SetHighlightFlag(isShowHighlight());
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectChannelChanged(TObject *Sender)
{
	if (inhibitCallbacks)
	{
		return;
	}

	if (Sender == (TObject*) AF_DetectRedButton)
	{
		AF_DetectRRadioButton->Checked = true;
	}
	else if (Sender == (TObject*) AF_DetectGreenButton)
	{
		AF_DetectGRadioButton->Checked = true;
	}
	else if (Sender == (TObject*) AF_DetectBlueButton)
	{
		AF_DetectBRadioButton->Checked = true;
	}
	else if (Sender == (TObject*) AF_DetectAlphaButton)
	{
		AF_DetectARadioButton->Checked = true;
	}

	AF_FillInGangedFilterDetails(gangMask(GangParam_DetectChannel));

	AF_updateDetectionMap();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectRadioButtonChanged(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		bool alphaChannelSelected = false;

		if (AF_DetectRRadioButton->Checked)
		{
			AF_DetectRedButton->Down = true;
			AF_HilitedDetectRedButton->Down = true;
		}
		else if (AF_DetectGRadioButton->Checked)
		{
			AF_DetectGreenButton->Down = true;
			AF_HilitedDetectGreenButton->Down = true;
		}
		else if (AF_DetectBRadioButton->Checked)
		{
			AF_DetectBlueButton->Down = true;
			AF_HilitedDetectBlueButton->Down = true;
		}
		else
		{
			AF_DetectAlphaButton->Down = true;
			AF_HilitedDetectAlphaButton->Down = true;
			alphaChannelSelected = true;
		}

		AF_ConformAlphaFilterEnabling(alphaChannelSelected);

		AF_FillInGangedFilterDetails(gangMask(GangParam_DetectChannel));
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_ConformAlphaFilterEnabling(bool alphaChannelSelected)
{
	AF_DisabledMinSizeTrackEdit->Visible = alphaChannelSelected;
	AF_MinContrastTrackEdit->Visible = !alphaChannelSelected;
	AF_LuminanceThresholdTrackEdit->Visible = alphaChannelSelected;
	AF_AlphaMotionTrackEdit->Visible = alphaChannelSelected;
	AF_RgbMotionTrackEdit->Visible = !alphaChannelSelected;

	AF_DisabledDebrisContrastRadioPanel->Visible = alphaChannelSelected;
	// UseAlphaAsMaskCheckBox->Enabled = !alphaChannelSelected;

	AF_DisabledAlphaGrainSuppressionTrackEdit->Visible = !alphaChannelSelected;
	setRedEyeButtonState(alphaChannelSelected, AF_RedEyeButton->Down);
	AF_setGreenEyeButtonState(alphaChannelSelected, AF_GreenEyeButton->Down);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_UseAlphaAsMaskCheckBoxClick(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_FillInGangedFilterDetails(gangMask(GangParam_UseAlphaAsMask));
	}

	AF_updateDetectionMap();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DebrisContrastChanged(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_FillInGangedFilterDetails(gangMask(GangParam_DebrisContrast));
	}

	// Now only in alpha detection mode.
	// updateDetectionMap();

	AF_FilterParameterChanged();
	AF_initiateContinuousPreviewLagTiming();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_NewFilterButtonClick(TObject *Sender)
{
	// 1. Get the name of the filter via popup - operation cancelled if
	// the name is empty
	string newFilterName = GetNameForNewFilter( /* activeFilterManager */);
	if (newFilterName.empty())
	{
		return;
	}

	// 2. Set the name on the GUI filter detail panel
	AF_FilterNameLabel->Caption = newFilterName.c_str();

	// 3. Capture the current filter details and build a new SFilter object
	SFilter *newFilter = AF_CreateNewFilterFromFilterDetails();
	*(newFilter->name) = newFilterName;

	// 4. Add the filter to the master filter list
	string newFilterID = AF_FilterManager->AddFilter(*newFilter);

	// 5. Add the filter to the first open slot for the active filter set
	// and select it
	int filterSlot = AF_FilterManager->LoadFilter(newFilterID);
	AF_UnselectFilter();
	AF_UpdateFilterManagerGUI();
	if (filterSlot >= 0)
	{
		AF_SelectFilter(newFilterID);
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SelectFilter(const string &filterID)
{
	AF_SelectFilter(AF_FilterManager->GetFilterSlot(filterID));
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_SelectFilter(int filterSlot)
{
	if (filterSlot != INVALID_SLOT_NUMBER)
	{
		string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(filterSlot);
		AF_SelectedFilterID = filterID;

		inhibitCallbacks = true;
		AF_FilterBankFrame->Select(filterSlot);
		inhibitCallbacks = false;

		AF_UpdateFilterDetailPanel();
		AF_UpdateFilterManagerGUI();
		AF_UpdateDefaultsButtons();
		AF_UpdateMultiSelectionIndicator();
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_UnselectFilter()
{
	int filterSlot = AF_FilterManager->GetFilterSlot(AF_SelectedFilterID);
	if (filterSlot != INVALID_SLOT_NUMBER)
	{
		AF_SelectedFilterID = INVALID_ID;

		inhibitCallbacks = true;
		AF_FilterBankFrame->UngangAll();
		AF_FilterBankFrame->Unselect(filterSlot);
		inhibitCallbacks = false;

		AF_UpdateFilterDetailPanel();
		AF_UpdateFilterManagerGUI();
		AF_UpdateDefaultsButtons();
		AF_UpdateMultiSelectionIndicator();
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_MakeSureAFilterIsSelected()
{
	if (AF_SelectedFilterID != INVALID_ID)
   {
      return;
   }

   int selectSlot = -1;
   for (int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
   {
      if (!AF_FilterBankFrame->IsEnabled(slot))
      {
         break;
      }

      selectSlot = 0; // Will use 0 if no loaded slots are used
      if (AF_FilterBankFrame->IsUsed(slot))
      {
         selectSlot = slot;
         break;
      }
   }

   if (selectSlot >= 0)
   {
      // THIS IS VERY UGLY - we're going to get re-entered from this call.
      // so need to just return after it!
      AF_SelectFilter(selectSlot);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_LoadFilterButtonClick(TObject *Sender)
{
	int result;

	// Only however many slots are available may be loaded
	int fullSlots = activeFilterManager->GetCountOfLoadedFilters();
	int loadableSlots = MAX_DISPLAYED_FILTERS - fullSlots;
	if (loadableSlots < 1)
	{
		// Caller should have screened these
		return;
	}

	if (PickAFilterForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		PickAFilterForm = new TPickAFilterForm(owner);
	}

	PickAFilterForm->SetOpName("load");

	vector<pair<string, string> >sortedFilterList;
	activeFilterManager->GetSortedListOfFilterNames(sortedFilterList);

	// Remove already loaded filters from the list
	for (int i = 0; i < fullSlots; ++i)
	{
		string loadedFilterID = activeFilterManager->GetFilterIDFromSlotNumber(i);
		vector<pair<string, string> >::iterator iter;

		for (iter = sortedFilterList.begin(); iter != sortedFilterList.end(); ++iter)
		{
			if ((*iter).second == loadedFilterID)
			{
				// already loaded
				sortedFilterList.erase(iter);
				break;
			}
		}
	}

	PickAFilterForm->SetFilterNameList(sortedFilterList);
	result = ShowModalDialog(PickAFilterForm);

	vector<string>selectedFilterIDs;
	int selectCount = PickAFilterForm->GetSelectedFilterIDs(selectedFilterIDs);

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete PickAFilterForm;
	PickAFilterForm = NULL;

	if (result == mrOk && selectCount > 0)
	{
		if (loadableSlots > selectCount)
		{
			loadableSlots = selectCount;
		}

		// First one gets selected
		AF_UnselectFilter();
		AF_FilterManager->LoadFilter(selectedFilterIDs[0]);
		AF_UpdateFilterManagerGUI(); // Necessary before Select Filter - uggggh!
		AF_SelectFilter(selectedFilterIDs[0]);

		// Do the rest... I decided not to gang them
		for (int i = 1; i < loadableSlots; ++i)
		{
			AF_FilterManager->LoadFilter(selectedFilterIDs[i]);
		}

		AF_UpdateFilterManagerGUI();
		AF_UpdateMultiSelectionIndicator();

		AF_FilterSetChanged();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_UnloadFilterButtonClick(TObject *Sender)
{
	// Construct the list of slots to unload else we worry the gang indicators
	// will get wiped out as we are unloading filters
	bool slotIsGanged[MAX_DISPLAYED_FILTERS];
	for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		slotIsGanged[i] = AF_FilterBankFrame->IsEnabled(i) && AF_FilterBankFrame->IsGanged(i);
	}

	// This is ugly, but we know the selected filter is going to get unloaded
	AF_SelectedFilterID = INVALID_ID;

	// Do this BACKWARDS because remaining filters move up when one is unloaded
	for (int i = (MAX_DISPLAYED_FILTERS - 1); i >= 0; --i)
	{
		if (slotIsGanged[i])
		{
			AF_FilterBankFrame->Ungang(i); // Oy
			string filterToUnload = AF_FilterManager->GetFilterIDFromSlotNumber(i);
			AF_FilterManager->UnloadFilter(filterToUnload);
			AF_UpdateFilterManagerGUI(); // Need to keep in synch inside loop
			AF_UpdateMultiSelectionIndicator();
		}
	}

	AF_UpdateFilterDetailPanel();

	AF_FilterSetChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DeleteFilterButtonClick(TObject *Sender)
{
	int result;

	if (PickAFilterForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		PickAFilterForm = new TPickAFilterForm(owner);
	}

	PickAFilterForm->SetOpName("delete");

	vector<pair<string, string> >sortedFilterList;
	activeFilterManager->GetSortedListOfFilterNames(sortedFilterList);

	PickAFilterForm->SetFilterNameList(sortedFilterList);
	result = ShowModalDialog(PickAFilterForm);

	vector<string>selectedFilterIDs;
	int selectCount = PickAFilterForm->GetSelectedFilterIDs(selectedFilterIDs);

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete PickAFilterForm;
	PickAFilterForm = NULL;

	if (result == mrOk && selectCount > 0)
	{
		for (int i = 0; i < selectCount; ++i)
		{
			if (AF_SelectedFilterID == selectedFilterIDs[i])
			{
				AF_UnselectFilter();
			}

			AF_FilterManager->DeleteFilter(selectedFilterIDs[i]);
		}

		AF_UpdateFilterManagerGUI();
		AF_UpdateMultiSelectionIndicator();
	}

	AF_FilterSetChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::WTF_NewSetButtonClick(TObject *Sender)
{
	// 1. Get the name of the filter via popup - operation cancelled if
	// the name is empty
	string newFilterSetName = GetNameForNewFilterSet( /* activeFilterManager */);
	if (newFilterSetName.empty())
	{
		return;
	}

	// 2. Create an empty set
	string filterSetID = activeFilterManager->CreateAnEmptyFilterSet(newFilterSetName);
	if (filterSetID == INVALID_ID)
	{
		// TODO: Better diagnosis!
		_MTIErrorDialog(Handle, "Failed to create new filter set");
		return;
	}

	// 3. Add the name to the filter set selection combo box and select it
	// BuildFilterSetComboBoxList();
	// FilterBankSelectionComboBox->ItemIndex =
	// FilterBankSelectionComboBox->Items->Count - 1;
	// activeFilterManager->ActivateFilterSet(newFilterSetName);
	// UpdateFilterManagerGUI();

	AF_FilterSetChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_SaveSetButtonClick(TObject *Sender)
{
	vector<pair<string, string> >setList;
	vector<string>nameList;

	// Don't know why I can't do this in the 'formCreate', but it
	// causes crash at exit if I put it there
	if (NewFilterSetForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		NewFilterSetForm = new TNewFilterSetForm(owner);
	}

	AF_FilterManager->GetSortedListOfFilterSetNames(setList);
	for (unsigned i = 0; i < setList.size(); ++i)
	{
		nameList.push_back(setList[i].first);
	}

	NewFilterSetForm->SetListOfFilterSetNames(nameList);
	NewFilterSetForm->SetDefaultName(activeFilterManager->UniquifyFilterSetName(DEFAULT_SAVED_SET_NAME));

	// Need to set the initial selection

	int result = ShowModalDialog(NewFilterSetForm);

	if (result == mrOk)
	{
		int selIndex = NewFilterSetForm->GetSelectedIndex();
		if (selIndex >= 0)
		{
			AF_FilterManager->CopyActiveFilterSetByID(setList[selIndex].second);
			AF_FilterSetNameLabel->Caption = AnsiString(nameList[selIndex].c_str());
		}
		else
		{
			// Create a new filter set
			string newFilterSetName = AF_FilterManager->UniquifyFilterSetName(NewFilterSetForm->GetNewSetName());
			AF_FilterManager->CopyActiveFilterSetByName(newFilterSetName);
			AF_FilterSetNameLabel->Caption = AnsiString(newFilterSetName.c_str());
		}
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete NewFilterSetForm;
	NewFilterSetForm = NULL;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_UpdateSetButtonClick(TObject *Sender)
{
	vector<pair<string, string> >setList;
	vector<string>nameList;
	int visibleIndex = -1;

	// Don't know why I can't do this in the 'formCreate', but it
	// causes crash at exit if I put it there
	if (ExistingSetForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		ExistingSetForm = new TExistingSetForm(owner);
	}

	ExistingSetForm->SetOpName("update");

	AF_FilterManager->GetSortedListOfFilterSetNames(setList);
	for (unsigned i = 0; i < setList.size(); ++i)
	{
		nameList.push_back(setList[i].first);
		if (setList[i].second == AF_LastFilterSetLoaded)
		{
			visibleIndex = (int) nameList.size() - 1;
		}
	}
	ExistingSetForm->SetListOfFilterSetNames(nameList);
	ExistingSetForm->SetVisibleIndex(visibleIndex);

	// Need to set the initial selection

	int result = ShowModalDialog(ExistingSetForm);

	if (result == mrOk)
	{
		int selIndex = ExistingSetForm->GetSelectedIndex();
		if (selIndex >= 0)
		{
			AF_FilterManager->CopyActiveFilterSetByID(setList[selIndex].second);
			AF_FilterSetNameLabel->Caption = AnsiString(nameList[selIndex].c_str());
		}
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete ExistingSetForm;
	ExistingSetForm = NULL;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_LoadSetButtonClick(TObject *Sender)
{
	vector<pair<string, string> >setList;
	vector<string>nameList;
	int visibleIndex = -1;

	if (ExistingSetForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		ExistingSetForm = new TExistingSetForm(owner);
	}

	ExistingSetForm->SetOpName("load");
	ExistingSetForm->SetVisibleIndex(-1);

	activeFilterManager->GetSortedListOfFilterSetNames(setList);
	for (unsigned i = 0; i < setList.size(); ++i)
	{
		nameList.push_back(setList[i].first);
		if (setList[i].second == AF_LastFilterSetLoaded)
		{
			visibleIndex = (int) nameList.size() - 1;
		}
	}

	ExistingSetForm->SetListOfFilterSetNames(nameList);
	ExistingSetForm->SetVisibleIndex(visibleIndex);

	// Need to set the initial selection

	int result = ShowModalDialog(ExistingSetForm);

	if (result == mrOk)
	{
		int selIndex = ExistingSetForm->GetSelectedIndex();
		if (selIndex >= 0)
		{
			AF_UnselectFilter(); // This also UngangAll()'s
			AF_FilterManager->ActivateFilterSet(setList[selIndex].second);
			AF_FilterManager->CopyActiveFilterSetByID(AF_DEFAULT_FILTER_SET_ID);
			AF_FilterManager->ActivateFilterSet(AF_DEFAULT_FILTER_SET_ID);
			AF_LastFilterSetLoaded = setList[selIndex].second;
			AF_FilterSetNameLabel->Caption = AnsiString(nameList[selIndex].c_str());
			AF_SelectFilter(0);
		}
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete ExistingSetForm;
	ExistingSetForm = NULL;

	AF_UpdateFilterManagerGUI();
	AF_UpdateFilterDetailPanel();
	AF_UpdateMultiSelectionIndicator();

	AF_FilterSetChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DeleteSetButtonClick(TObject *Sender)
{
	vector<pair<string, string> >setList;
	vector<string>nameList;
	int visibleIndex = -1;

	// Don't know why I can't do this in the 'formCreate', but it
	// causes crash at exit if I put it there
	if (ExistingSetForm == NULL)
	{
		TCustomForm *owner = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());
		ExistingSetForm = new TExistingSetForm(owner);
	}

	ExistingSetForm->SetOpName("delete");

	AF_FilterManager->GetSortedListOfFilterSetNames(setList);
	for (unsigned i = 0; i < setList.size(); ++i)
	{
		nameList.push_back(setList[i].first);
		// No! Force user to pick one explicitly - no default
		// if (setList[i].second == LastFilterSetLoaded)
		// {
		// visibleIndex = (int) nameList.size() - 1;
		// }
	}

	ExistingSetForm->SetListOfFilterSetNames(nameList);
	ExistingSetForm->SetVisibleIndex(visibleIndex);

	// Need to set the initial selection

	int result = ShowModalDialog(ExistingSetForm);

	if (result == mrOk)
	{
		int selIndex = ExistingSetForm->GetSelectedIndex();
		if (selIndex >= 0)
		{
			AF_FilterManager->DeleteFilterSet(setList[selIndex].second);
			if (setList[selIndex].first == StringToStdString(AF_FilterSetNameLabel->Caption))
			{
				AF_FilterSetNameLabel->Caption = "";
			}
		}
	}

	// AVOID CRASH ON EXIT - not sure why we have to do this
	delete ExistingSetForm;
	ExistingSetForm = NULL;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ResetButtonClick(TObject *Sender)
{
	for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		if (AF_FilterBankFrame->IsEnabled(i) && AF_FilterBankFrame->IsGanged(i))
		{
			string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(i);
			AF_FilterManager->RestoreFilterDefaults(filterID);
		}
	}

	SFilter filter = AF_FilterManager->GetFilterDetails(AF_SelectedFilterID);
	AF_UpdateFilterDetailGUIFromFilter(filter);
	AF_UpdateFilterManagerGUI();

	GAutoFilter->getSystemAPI()->setMainWindowFocus();
	AF_UpdateDefaultsButtons();
	GAutoFilter->getSystemAPI()->getBaseToolForm()->SetFocus();

	AF_FilterSetChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_SaveDefaultsButtonClick(TObject *Sender)
{
	// First, confirm that the user really wants to change the default values
	bool multi = (AF_FilterBankFrame->GetGangCount() > 1);

	MTIostringstream ostr;
	ostr << "This will change the default values for " << (multi ? "all selected filters   " : "the selected filter   ")
			<< endl << "There is no undo. Proceed? ";

	TCustomForm *baseToolForm = reinterpret_cast<TCustomForm*>(GAutoFilter->getSystemAPI()->GetBaseToolFormHandle());

	if (_MTIConfirmationDialog(baseToolForm->Handle, ostr.str()) != MTI_DLG_OK)
	{
		return;
	}

	for (int i = 0; i < MAX_DISPLAYED_FILTERS; ++i)
	{
		if (AF_FilterBankFrame->IsEnabled(i) && AF_FilterBankFrame->IsGanged(i))
		{
			string filterID = AF_FilterManager->GetFilterIDFromSlotNumber(i);
			AF_FilterManager->SaveFilterDefaults(AF_SelectedFilterID);
		}
	}

	GAutoFilter->getSystemAPI()->setMainWindowFocus();
	AF_UpdateDefaultsButtons();
	GAutoFilter->getSystemAPI()->getBaseToolForm()->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectRRadioButtonEnter(TObject *Sender)
{
	AF_HilitedDetectRedButton->Visible = true;
	AF_DetectLabel->Font->Style = (TFontStyles() << fsBold);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectRRadioButtonExit(TObject *Sender)
{
	AF_HilitedDetectRedButton->Visible = false;
	AF_DetectLabel->Font->Style = TFontStyles();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectGRadioButtonEnter(TObject *Sender)
{
	AF_HilitedDetectGreenButton->Visible = true;
	AF_DetectLabel->Font->Style = (TFontStyles() << fsBold);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectGRadioButtonExit(TObject *Sender)
{
	AF_HilitedDetectGreenButton->Visible = false;
	AF_DetectLabel->Font->Style = TFontStyles();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectBRadioButtonEnter(TObject *Sender)
{
	AF_HilitedDetectBlueButton->Visible = true;
	AF_DetectLabel->Font->Style = (TFontStyles() << fsBold);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectBRadioButtonExit(TObject *Sender)
{
	AF_HilitedDetectBlueButton->Visible = false;
	AF_DetectLabel->Font->Style = TFontStyles();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectARadioButtonEnter(TObject *Sender)
{
	AF_HilitedDetectAlphaButton->Visible = true;
	AF_DetectLabel->Font->Style = (TFontStyles() << fsBold);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DetectARadioButtonExit(TObject *Sender)
{
	AF_HilitedDetectAlphaButton->Visible = false;
	AF_DetectLabel->Font->Style = TFontStyles();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessRShadowCheckBoxClick(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_ProcessRedButton->Down = AF_ProcessRShadowCheckBox->Checked;
	AF_HilitedProcessRedButton->Down = AF_ProcessRShadowCheckBox->Checked;
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessGShadowCheckBoxClick(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_ProcessGreenButton->Down = AF_ProcessGShadowCheckBox->Checked;
	AF_HilitedProcessGreenButton->Down = AF_ProcessGShadowCheckBox->Checked;
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessBShadowCheckBoxClick(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_ProcessBlueButton->Down = AF_ProcessBShadowCheckBox->Checked;
	AF_HilitedProcessBlueButton->Down = AF_ProcessBShadowCheckBox->Checked;
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessRShadowCheckBoxEnter(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_HilitedProcessRedButton->Visible = true;
	AF_HilitedProcessRedButton->Down = AF_ProcessRShadowCheckBox->Checked;
	AF_ProcessLabel->Font->Style = (TFontStyles() << fsBold);
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessRShadowCheckBoxExit(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_ProcessRedButton->Down = AF_ProcessRShadowCheckBox->Checked;
	AF_HilitedProcessRedButton->Visible = false;
	AF_ProcessLabel->Font->Style = TFontStyles();
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessGShadowCheckBoxEnter(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_HilitedProcessGreenButton->Visible = true;
	AF_HilitedProcessGreenButton->Down = AF_ProcessGShadowCheckBox->Checked;
	AF_ProcessLabel->Font->Style = (TFontStyles() << fsBold);
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessGShadowCheckBoxExit(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_ProcessGreenButton->Down = AF_ProcessGShadowCheckBox->Checked;
	AF_HilitedProcessGreenButton->Visible = false;
	AF_ProcessLabel->Font->Style = TFontStyles();
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessBShadowCheckBoxEnter(TObject *Sender)
{
	inhibitCallbacks = true;
	AF_HilitedProcessBlueButton->Visible = true;
	AF_HilitedProcessBlueButton->Down = AF_ProcessBShadowCheckBox->Checked;
	AF_ProcessLabel->Font->Style = (TFontStyles() << fsBold);
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessBShadowCheckBoxExit(TObject *Sender)
{
	MTIassert(activeFilterManager == AF_FilterManager);
	inhibitCallbacks = true;
	AF_ProcessBlueButton->Down = AF_ProcessBShadowCheckBox->Checked;
	AF_HilitedProcessBlueButton->Visible = false;
	AF_ProcessLabel->Font->Style = TFontStyles();
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessRedButtonClick(TObject *Sender)
{
	MTIassert(activeFilterManager == AF_FilterManager);
	if (!inhibitCallbacks)
	{
		AF_ProcessRShadowCheckBox->Checked = AF_ProcessRedButton->Down;
		// FocusControl(ProcessRShadowCheckBox);     causes exception
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessGreenButtonClick(TObject *Sender)
{
	MTIassert(activeFilterManager == AF_FilterManager);
	if (!inhibitCallbacks)
	{
		AF_ProcessGShadowCheckBox->Checked = AF_ProcessGreenButton->Down;
		// FocusControl(ProcessGShadowCheckBox);    causes exception
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ProcessBlueButtonClick(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_ProcessBShadowCheckBox->Checked = AF_ProcessBlueButton->Down;
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_HilitedProcessRedButtonClick(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_ProcessRShadowCheckBox->Checked = AF_HilitedProcessRedButton->Down;
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_HilitedProcessGreenButtonClick(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_ProcessGShadowCheckBox->Checked = AF_HilitedProcessGreenButton->Down;
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_HilitedProcessBlueButtonClick(TObject *Sender)
{
	if (!inhibitCallbacks)
	{
		AF_ProcessBShadowCheckBox->Checked = AF_HilitedProcessBlueButton->Down;
		AF_FillInGangedFilterDetails(gangMask(GangParam_ProcessChannels));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DebrisContrastRadioButtonEnter(TObject *Sender)
{
	AF_DebrisContrastLabel->Font->Style = (TFontStyles() << fsBold);
	AF_DebrisContrastLabel->Left = 6;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_DebrisContrastRadioButtonExit(TObject *Sender)
{
	AF_DebrisContrastLabel->Font->Style = TFontStyles();
	AF_DebrisContrastLabel->Left = 10;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_UseHardMotionForDustCheckBoxClick(TObject *Sender)
{
	AF_FillInGangedFilterDetails(gangMask(GangParam_UseHardMotionForDust));
	AF_FilterParameterChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_SelectAllButtonClick(TObject *Sender)
{
	if (AF_FilterBankFrame->IsEnabled(0))
	{
		if (AF_SelectedFilterID == INVALID_ID)
		{
			AF_SelectFilter(0);
		}
		AF_FilterBankFrame->GangAll();

		AF_UpdateFilterDetailPanel();
		AF_UpdateFilterManagerGUI();
		AF_UpdateDefaultsButtons();
		AF_UpdateMultiSelectionIndicator();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::UsePastFramesOnlyCheckBoxClick(TObject *Sender)
{
	bool checked = UsePastFramesOnlyCheckBox->Checked;

	if ((!inhibitCallbacks) && (GAutoFilter != NULL))
	{
		GAutoFilter->SetUsePastFramesOnlyFlag(checked);
	}

	// TColor color = checked? clYellow : clBtnFace;
	// UsePastFramesOnlyCheckBox->Color = color;
	// UsePastFramesOnlyLabelPanel->Color = color;

	AF_FilterParameterChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::UsePastFramesOnlyLabelPanelClick(TObject *Sender)
{
	UsePastFramesOnlyCheckBoxClick(Sender);
}
// ---------------------------------------------------------------------------

// Button is now hidden - always on.
void __fastcall TAutofilterGUI::AF_UseGrainFilterCheckBoxClick(TObject *Sender)
{
	AF_FillInGangedFilterDetails(gangMask(GangParam_UseGrainFilter));

	AF_updateDetectionMap();
	AF_FilterParameterChanged();
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_RedEyeButtonClick(TObject *Sender)
{
	if (inhibitCallbacks || GAutoFilter == NULL)
	{
		return;
	}

   inhibitCallbacks = true;
   SHINE_Alpha_RedEyeButton->Down = false;
   inhibitCallbacks = false;
	GAutoFilter->SetUserDesiredDetectionVisualizationState(AF_RedEyeButton->Down);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Alpha_RedEyeButtonClick(TObject *Sender)
{
	if (inhibitCallbacks || GAutoFilter == NULL)
	{
		return;
	}

   inhibitCallbacks = true;
   AF_RedEyeButton->Down = false;
   inhibitCallbacks = false;
	GAutoFilter->SetUserDesiredDetectionVisualizationState(SHINE_Alpha_RedEyeButton->Down);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Alpha_ShowAlphaButtonClick(TObject *Sender)
{
	if (inhibitCallbacks || GAutoFilter == NULL)
	{
		return;
	}

   GAutoFilter->getSystemAPI()->setShowAlphaState(SHINE_Alpha_ShowAlphaButton->Down);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_ShowAlphaButtonClick(TObject *Sender)
{
	if (inhibitCallbacks || GAutoFilter == NULL)
	{
		return;
	}

   GAutoFilter->getSystemAPI()->setShowAlphaState(AF_ShowAlphaButton->Down);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::AF_GreenEyeButtonClick(TObject *Sender)
{
	if (inhibitCallbacks || GAutoFilter == NULL)
	{
		return;
	}

	if (AF_GreenEyeButton->Enabled && AF_GreenEyeButton->Down)
	{
		AF_enterContinuousPreviewMode();
	}
	else
	{
		AF_exitFromContinuousPreviewMode();
	}
}
// ---------------------------------------------------------------------------

void SetRedEyeButtonState(bool enabled, bool down)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->setRedEyeButtonState(enabled, down);
}

void TAutofilterGUI::setRedEyeButtonState(bool enabled, bool down)
{
   // Don't do anything if the state's not changing.
   if ((queryOpMode() == DebrisToolOpMode::SHINE
         && SHINE_Alpha_RedEyeButton->Enabled == (enabled && SHINE_Alpha_EnableButton->Down)
         && SHINE_Alpha_RedEyeButton->Down == (down && SHINE_Alpha_RedEyeButton->Enabled))
   || (queryOpMode() == DebrisToolOpMode::AF
         && AF_RedEyeButton->Enabled == enabled
         && AF_RedEyeButton->Down == down))
   {
      return;
   }

	inhibitCallbacks = true;

   if (queryOpMode() == DebrisToolOpMode::SHINE)
   {
      SHINE_Alpha_RedEyeButton->Enabled = enabled && SHINE_Alpha_EnableButton->Down;
      SHINE_Alpha_RedEyeButton->Down = down;
   }
   else
   {
      AF_RedEyeButton->Enabled = enabled;
      AF_RedEyeButton->Down = down;
   }

//	if (enabled && down && GAutoFilter != NULL)
//	{
//		GAutoFilter->CreateDetectionMapIfNecessary();
//	}

	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

void SetGreenEyeButtonState(bool enabled, bool down)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->AF_setGreenEyeButtonState(enabled, down);
}

void TAutofilterGUI::AF_setGreenEyeButtonState(bool enabled, bool down)
{
	inhibitCallbacks = true;
	AF_GreenEyeButton->Enabled = enabled;
	AF_GreenEyeButton->Down = down;
	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

// Tri-state (-1 = disabled, 0 = off, 1 = on)
void SetShowAlphaButtonState(int newState)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	AutofilterGUI->AF_setShowAlphaButtonState(newState);
	AutofilterGUI->SHINE_setShowAlphaButtonState(newState);
}

void TAutofilterGUI::SHINE_setShowAlphaButtonState(int newState)
{
   int SHINE_currentState = SHINE_Alpha_ShowAlphaButton->Enabled
                      ? (SHINE_Alpha_ShowAlphaButton->Down
                         ? 1
                         : 0)
                      : -1;

   if (SHINE_currentState == newState)
   {
      return;
   }

	inhibitCallbacks = true;

   SHINE_Alpha_ShowAlphaButton->Enabled = newState >= 0;
   SHINE_Alpha_ShowAlphaButton->Down = newState > 0;
   if (SHINE_Alpha_EnableButton->Down && !SHINE_Alpha_ShowAlphaButton->Enabled)
   {
      SHINE_Alpha_EnableButton->Down = false;
   }

   SHINE_Alpha_EnableButton->Enabled = SHINE_Alpha_ShowAlphaButton->Enabled;

	inhibitCallbacks = false;

   SHINE_Alpha_EnableButtonClick(nullptr);
}

void TAutofilterGUI::AF_setShowAlphaButtonState(int newState)
{
   int AF_currentState = AF_ShowAlphaButton->Enabled
                      ? (AF_ShowAlphaButton->Down
                         ? 1
                         : 0)
                      : -1;

   if (AF_currentState == newState)
   {
      return;
   }

	inhibitCallbacks = true;

   AF_ShowAlphaButton->Enabled = newState >= 0;
   AF_ShowAlphaButton->Down = newState > 0;

	inhibitCallbacks = false;
}
// ---------------------------------------------------------------------------

// Tri-state (-1 = all disabled, 0 = all enabled, 1 = alpha only enabled)
void SetShineDetectEnabledState(unsigned char flags)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	AutofilterGUI->SHINE_setDetectEnabledState(flags);
}

void TAutofilterGUI::SHINE_setDetectEnabledState(unsigned char flags)
{
   SHINE_Bright_EnableButton->Enabled = flags & ShineDetectEnableFlags::BRIGHT;
   SHINE_Dark_EnableButton->Enabled = flags & ShineDetectEnableFlags::DARK;
   SHINE_Alpha_EnableButton->Enabled = flags & ShineDetectEnableFlags::ALPHA;
}
// ---------------------------------------------------------------------------

void EnableSubtoolSwitching(bool flag)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	AutofilterGUI->enableSubtoolSwitching(flag);
}

void TAutofilterGUI::enableSubtoolSwitching(bool flag)
{
#ifdef NO_SHINE
   flag = false;
#endif

   OpModePageControl->Enabled = flag;
}
// ---------------------------------------------------------------------------

void SetPreviewFrameButtonState(bool enabled, bool down)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->setPreviewFrameButtonState(enabled, down);
}

void TAutofilterGUI::setPreviewFrameButtonState(bool enabled, bool down)
{
	// if (enabled)
	// {
	// ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
	// }
	// else
	// {
	// ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
	// }

	if (down)
	{
		ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
	}
	else
	{
		ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);
	}
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_initiateDetectionMapLagTiming()
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	DetectionMapLagTimer->Enabled = false;
	DetectionMapLagTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::DetectionMapLagTimerTimer(TObject *Sender)
{
	AF_updateDetectionMap();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_updateDetectionMap()
{
	DetectionMapLagTimer->Enabled = false;
	if (GAutoFilter == NULL)
	{
		return;
	}

	// Larry says don't do this --> exitFromContinuousPreviewMode();
	GAutoFilter->UpdateDetectionMap();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_initiateContinuousPreviewLagTiming()
{
	if (GAutoFilter == NULL || !GAutoFilter->GetContinuousPreviewState())
	{
		return;
	}

	ContinuousPreviewLagTimer->Enabled = false;
	ContinuousPreviewLagTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::ContinuousPreviewLagTimerTimer(TObject *Sender)
{
	ContinuousPreviewLagTimer->Enabled = false;
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->BumpContinuousPreviewSerialNumber();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_enterContinuousPreviewMode()
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->SetUserDesiredContinuousPreviewState(true);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_exitFromContinuousPreviewMode()
{
	if (GAutoFilter == NULL)
	{
		return;
	}

	GAutoFilter->SetUserDesiredContinuousPreviewState(false);
}
// ---------------------------------------------------------------------------

int GetNumberOfSelectedFilters()
{
	if (AutofilterGUI == NULL)
	{
		return 0;
	}

	return AutofilterGUI->getNumberOfSelectedFilters();
}

int TAutofilterGUI::getNumberOfSelectedFilters()
{
	if (activeFilterManager == AF_FilterManager)
	{
		return AF_FilterBankFrame->GetGangCount();
	}

	return 0;
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FilterParameterChanged()
{
	AF_exitFromContinuousPreviewMode();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::AF_FilterSetChanged()
{
	++_filterChangeCounter; // HACK
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::OpModePageControlChange(TObject *Sender)
{
#ifdef NO_SHINE
   activeFilterManager = AF_FilterManager;
   SHINE_AnalyzeButtonPanel->Visible = false;
   UsePastFramesOnlyLabelPanel->Visible = true;
   UsePastFramesOnlyCheckBox->Visible = true;
#else
   if (OpModePageControl->ActivePage == SHINE_TabSheet)
   {
      activeFilterManager = SHINE_FilterManager;
      SHINE_AnalyzeButtonPanel->Visible = true;
      UsePastFramesOnlyLabelPanel->Visible = false;
      UsePastFramesOnlyCheckBox->Visible = false;
   }
   else if (OpModePageControl->ActivePage == AF_TabSheet)
   {
      activeFilterManager = AF_FilterManager;
      SHINE_AnalyzeButtonPanel->Visible = false;
      UsePastFramesOnlyLabelPanel->Visible = true;
      UsePastFramesOnlyCheckBox->Visible = true;
   }
   else
   {
      MTIassert(false);
   }

   if (GAutoFilter == NULL)
   {
      return;
   }

   // Toggle activation to get good shit to happen, but don't do it if the tool
   // wasn't activated yet!
   if (GAutoFilter != NULL && GAutoFilter->IsActive())
   {
      GAutoFilter->toolDeactivate();
      GAutoFilter->toolActivate();
   }
#endif
}
//---------------------------------------------------------------------------
void EnableAnalyzeClip(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_AnalyzeClipButton->Enabled = flag;
	}
}

void EnableAnalyzeMarked(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_AnalyzeMarkedButton->Enabled = flag;
	}
}

void EnableAnalyzeShot(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_AnalyzeShotButton->Enabled = flag;
      AutofilterGUI->SHINE_ShotHasNotBeenAnalyzedLabel->Visible = flag;
	}
}

void EnableResetClip(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_ResetClipButton->Enabled = flag;
	}
}

void EnableResetMarked(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_ResetMarkedButton->Enabled = flag;
	}
}

void EnableResetShot(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
		AutofilterGUI->SHINE_ResetShotButton->Enabled = flag;
	}
}

void EnableSingleProc(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
      if (flag)
      {
         AutofilterGUI->ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
         AutofilterGUI->ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_FRAME);
      }
      else
      {
         AutofilterGUI->ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         AutofilterGUI->ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      }
	}
}

void EnableMultiProc(bool flag)
{
	if (AutofilterGUI != nullptr)
	{
      if (flag)
      {
         AutofilterGUI->ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
         AutofilterGUI->ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
      }
      else
      {
         AutofilterGUI->ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         AutofilterGUI->ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      }
	}
}
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------- S H I N E -------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void GatherShineGuiParameters(ShineToolSettings &shineParams)
{
	if (AutofilterGUI == NULL)
	{
		return;
	}

	return AutofilterGUI->SHINE_gatherGuiParameters(shineParams);
}

void TAutofilterGUI::SHINE_gatherGuiParameters(ShineToolSettings &shineParams)
{
   shineParams.detectDark = SHINE_Dark_EnableButton->Down;
   shineParams.detectBright = SHINE_Bright_EnableButton->Down;
   shineParams.detectAlpha = SHINE_Alpha_EnableButton->Down;

   shineParams.darkMaxSize = SHINE_Dark_MaxSizeTrackEdit->GetValue();
   shineParams.darkMinSize = SHINE_Dark_MinSizeTrackEdit->GetValue();
   shineParams.darkTemporalContrast = SHINE_Dark_TemporalContrastTrackEdit->GetValue();
   shineParams.darkSpatialContrast = SHINE_Dark_SpatialContrastTrackEdit->GetValue();
   shineParams.darkMotionTolerance = SHINE_Dark_MotionToleranceTrackEdit->GetValue();

   shineParams.brightMaxSize = SHINE_Bright_MaxSizeTrackEdit->GetValue();
   shineParams.brightMinSize = SHINE_Bright_MinSizeTrackEdit->GetValue();
   shineParams.brightTemporalContrast = SHINE_Bright_TemporalContrastTrackEdit->GetValue();
   shineParams.brightSpatialContrast = SHINE_Bright_SpatialContrastTrackEdit->GetValue();
   shineParams.brightMotionTolerance = SHINE_Bright_MotionToleranceTrackEdit->GetValue();

   shineParams.alphaMaxSize = SHINE_Alpha_MaxSizeTrackEdit->GetValue();
   shineParams.alphaGrainSuppression = SHINE_Alpha_GrainSuppressionTrackEdit->GetValue();
   shineParams.alphaMotionTolerance = SHINE_Alpha_MotionToleranceTrackEdit->GetValue();
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_AnalyzeButtonClick(TObject *Sender)
{
   auto button = static_cast<TSpeedButton *>(Sender);
   auto scope = (button == SHINE_AnalyzeShotButton)
                  ? AnalysisScope::CURRENT_SHOT
                  : ((button == SHINE_AnalyzeMarkedButton)
                        ? AnalysisScope::MARKED_RANGE
                        : AnalysisScope::FULL_CLIP);

	GAutoFilter->SHINE_RunPreprocessing(scope);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_ResetAnalysisButtonClick(TObject *Sender)
{
   auto button = static_cast<TSpeedButton *>(Sender);
   auto scope = (button == SHINE_AnalyzeShotButton)
                  ? AnalysisScope::CURRENT_SHOT
                  : ((button == SHINE_AnalyzeMarkedButton)
                        ? AnalysisScope::MARKED_RANGE
                        : AnalysisScope::FULL_CLIP);

	GAutoFilter->SHINE_ResetPreprocessing(scope);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Bright_EnableButtonClick(TObject *Sender)
{
   bool down = SHINE_Bright_EnableButton->Down;
   SHINE_setBrightSettingsEnable(down);
   if (down)
   {
      SHINE_setAlphaSettingsEnable(false);
   }

   SHINE_UpdateResetButtons();
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Dark_EnableButtonClick(TObject *Sender)
{
   bool down = SHINE_Dark_EnableButton->Down;
   SHINE_setDarkSettingsEnable(down);
   if (down)
   {
      SHINE_setAlphaSettingsEnable(false);
   }

   SHINE_UpdateResetButtons();
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Alpha_EnableButtonClick(TObject *Sender)
{
   bool down = SHINE_Alpha_EnableButton->Down;
   SHINE_setAlphaSettingsEnable(down);
   if (down)
   {
      // Should maybe save/restore these!
      SHINE_setBrightSettingsEnable(false);
      SHINE_setDarkSettingsEnable(false);
   }

   SHINE_UpdateResetButtons();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_setDarkSettingsEnable(bool enable)
{
   SHINE_Dark_EnableButton->Down = enable;
   SHINE_Dark_DisabledPresetsPanel->Visible = !enable;
   SHINE_Dark_DisabledMaxSizeTrackEdit->Visible = !enable;
   SHINE_Dark_DisabledMinSizeTrackEdit->Visible = !enable;
   SHINE_Dark_DisabledTemporalContrastTrackEdit->Visible = !enable;
   SHINE_Dark_DisabledSpatialContrastTrackEdit->Visible = !enable;
   SHINE_Dark_DisabledMotionToleranceTrackEdit->Visible = !enable;
   SHINE_Dark_ResetButton->Enabled = false;
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_setBrightSettingsEnable(bool enable)
{
   SHINE_Bright_EnableButton->Down = enable;
   SHINE_Bright_DisabledPresetsPanel->Visible = !enable;
   SHINE_Bright_DisabledMaxSizeTrackEdit->Visible = !enable;
   SHINE_Bright_DisabledMinSizeTrackEdit->Visible = !enable;
   SHINE_Bright_DisabledTemporalContrastTrackEdit->Visible = !enable;
   SHINE_Bright_DisabledSpatialContrastTrackEdit->Visible = !enable;
   SHINE_Bright_DisabledMotionToleranceTrackEdit->Visible = !enable;
   SHINE_Bright_ResetButton->Enabled = false;
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_setAlphaSettingsEnable(bool enable)
{
   SHINE_Alpha_EnableButton->Down = enable;
   SHINE_Alpha_DisabledPresetsPanel->Visible = !enable;
   SHINE_Alpha_DisabledMaxSizeTrackEdit->Visible = !enable;
   SHINE_Alpha_DisabledGrainSuppressionTrackEdit->Visible = !enable;
   SHINE_Alpha_DisabledMotionToleranceTrackEdit->Visible = !enable;
   SHINE_Alpha_ResetButton->Enabled = false;
   SHINE_Alpha_RedEyeButton->Enabled = enable;
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Bright_ResetButtonClick(TObject *Sender)
{
   ShineToolSettings defaultSettings;
   SHINE_Bright_MaxSizeTrackEdit->SetValue(defaultSettings.brightMaxSize);
   SHINE_Bright_MinSizeTrackEdit->SetValue(defaultSettings.brightMinSize);
   SHINE_Bright_TemporalContrastTrackEdit->SetValue(defaultSettings.brightTemporalContrast);
   SHINE_Bright_SpatialContrastTrackEdit->SetValue(defaultSettings.brightSpatialContrast);
   SHINE_Bright_MotionToleranceTrackEdit->SetValue(defaultSettings.brightMotionTolerance);
}
// ---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Dark_ResetButtonClick(TObject *Sender)
{
   ShineToolSettings defaultSettings;
   SHINE_Dark_MaxSizeTrackEdit->SetValue(defaultSettings.darkMaxSize);
   SHINE_Dark_MinSizeTrackEdit->SetValue(defaultSettings.darkMinSize);
   SHINE_Dark_TemporalContrastTrackEdit->SetValue(defaultSettings.darkTemporalContrast);
   SHINE_Dark_SpatialContrastTrackEdit->SetValue(defaultSettings.darkSpatialContrast);
   SHINE_Dark_MotionToleranceTrackEdit->SetValue(defaultSettings.darkMotionTolerance);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_Alpha_ResetButtonClick(TObject *Sender)
{
   ShineToolSettings defaultSettings;
   SHINE_Alpha_MaxSizeTrackEdit->SetValue(defaultSettings.alphaMaxSize);
   SHINE_Alpha_MaxSizeTrackEdit->SetValue(defaultSettings.alphaMaxSize);
   SHINE_Alpha_MotionToleranceTrackEdit->SetValue(defaultSettings.alphaMotionTolerance);
}
//---------------------------------------------------------------------------

void __fastcall TAutofilterGUI::SHINE_GangBrightAndDarkCheckBoxClick(TObject *Sender)
{
   if (SHINE_GangBrightAndDarkCheckBox->Checked == false
   || SHINE_Dark_EnableButton->Down == false
   || SHINE_Bright_EnableButton->Down == false)
   {
      return;
   }

   if (SHINE_darkSettingsWereChangedLast)
   {
      inhibitCallbacks = true;
      SHINE_Bright_MaxSizeTrackEdit->SetValue(SHINE_Dark_MaxSizeTrackEdit->GetValue());
      SHINE_Bright_MinSizeTrackEdit->SetValue(SHINE_Dark_MinSizeTrackEdit->GetValue());
      SHINE_Bright_TemporalContrastTrackEdit->SetValue(SHINE_Dark_TemporalContrastTrackEdit->GetValue());
      SHINE_Bright_SpatialContrastTrackEdit->SetValue(SHINE_Dark_SpatialContrastTrackEdit->GetValue());
      SHINE_Bright_MotionToleranceTrackEdit->SetValue(SHINE_Dark_MotionToleranceTrackEdit->GetValue());
      inhibitCallbacks = false;
   }
   else
   {
      inhibitCallbacks = true;
      SHINE_Dark_MaxSizeTrackEdit->SetValue(SHINE_Bright_MaxSizeTrackEdit->GetValue());
      SHINE_Dark_MinSizeTrackEdit->SetValue(SHINE_Bright_MinSizeTrackEdit->GetValue());
      SHINE_Dark_TemporalContrastTrackEdit->SetValue(SHINE_Bright_TemporalContrastTrackEdit->GetValue());
      SHINE_Dark_SpatialContrastTrackEdit->SetValue(SHINE_Bright_SpatialContrastTrackEdit->GetValue());
      SHINE_Dark_MotionToleranceTrackEdit->SetValue(SHINE_Bright_MotionToleranceTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_UpdateSettingsPanels()
{
      // T o  D o ...
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_UpdateResetButtons()
{
   ShineToolSettings defaultSettings;

	SHINE_Bright_ResetButton->Enabled =
         SHINE_Bright_EnableButton->Down
         && (SHINE_Bright_MaxSizeTrackEdit->GetValue() != defaultSettings.brightMaxSize
            || SHINE_Bright_MinSizeTrackEdit->GetValue() != defaultSettings.brightMinSize
            || SHINE_Bright_TemporalContrastTrackEdit->GetValue() != defaultSettings.brightTemporalContrast
            || SHINE_Bright_SpatialContrastTrackEdit->GetValue() != defaultSettings.brightSpatialContrast
            || SHINE_Bright_MotionToleranceTrackEdit->GetValue() != defaultSettings.brightMotionTolerance);

	SHINE_Dark_ResetButton->Enabled =
         SHINE_Dark_EnableButton->Down
         && (SHINE_Dark_MaxSizeTrackEdit->GetValue() != defaultSettings.darkMaxSize
            || SHINE_Dark_MinSizeTrackEdit->GetValue() != defaultSettings.darkMinSize
            || SHINE_Dark_TemporalContrastTrackEdit->GetValue() != defaultSettings.darkTemporalContrast
            || SHINE_Dark_SpatialContrastTrackEdit->GetValue() != defaultSettings.darkSpatialContrast
            || SHINE_Dark_MotionToleranceTrackEdit->GetValue() != defaultSettings.darkMotionTolerance);

	SHINE_Alpha_ResetButton->Enabled =
         SHINE_Alpha_EnableButton->Down
         && (SHINE_Alpha_MaxSizeTrackEdit->GetValue() != defaultSettings.alphaMaxSize
            || SHINE_Alpha_GrainSuppressionTrackEdit->GetValue() != defaultSettings.alphaGrainSuppression
            || SHINE_Alpha_MotionToleranceTrackEdit->GetValue() != defaultSettings.alphaMotionTolerance);
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Bright_MaxSizeTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MaxSizeTrackEdit->SetValue(SHINE_Bright_MaxSizeTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Bright_MinSizeTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MinSizeTrackEdit->SetValue(SHINE_Bright_MinSizeTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Bright_TemporalContrastTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_TemporalContrastTrackEdit->SetValue(SHINE_Bright_TemporalContrastTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Bright_SpatialContrastTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_SpatialContrastTrackEdit->SetValue(SHINE_Bright_SpatialContrastTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Bright_MotionToleranceTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MotionToleranceTrackEdit->SetValue(SHINE_Bright_MotionToleranceTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Dark_MaxSizeTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MaxSizeTrackEdit->SetValue(SHINE_Dark_MaxSizeTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Dark_MinSizeTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MinSizeTrackEdit->SetValue(SHINE_Dark_MinSizeTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Dark_TemporalContrastTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_TemporalContrastTrackEdit->SetValue(SHINE_Dark_TemporalContrastTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Dark_SpatialContrastTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_SpatialContrastTrackEdit->SetValue(SHINE_Dark_SpatialContrastTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_Dark_MotionToleranceTrackEditChanged(void *Sender)
{
   if (inhibitCallbacks)
   {
      return;
   }

   SHINE_darkSettingsWereChangedLast = false;

   if (SHINE_GangBrightAndDarkCheckBox->Checked || GAutoFilter->getAltKeyState())
   {
      inhibitCallbacks = true;
      SHINE_Dark_MotionToleranceTrackEdit->SetValue(SHINE_Dark_MotionToleranceTrackEdit->GetValue());
      inhibitCallbacks = false;
   }

   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_AlphaTrackEditChanged(void *Sender)
{
   SHINE_UpdateResetButtons();
   SHINE_UpdatePresetCurrentValues();

   AF_initiateDetectionMapLagTiming();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_darkPresetHasChanged(void *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

   auto presets = SHINE_darkPresets->GetCurrentPreset();
   SHINE_Dark_MaxSizeTrackEdit->SetValue(presets.maxSize);
   SHINE_Dark_MinSizeTrackEdit->SetValue(presets.minSize);
   SHINE_Dark_SpatialContrastTrackEdit->SetValue(presets.spatialContrast);
   SHINE_Dark_TemporalContrastTrackEdit->SetValue(presets.temporalContrast);
   SHINE_Dark_MotionToleranceTrackEdit->SetValue(presets.motionTolerance);
   int grainSuppression = -1;
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_brightPresetHasChanged(void *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

   auto presets = SHINE_brightPresets->GetCurrentPreset();
   SHINE_Bright_MaxSizeTrackEdit->SetValue(presets.maxSize);
   SHINE_Bright_MinSizeTrackEdit->SetValue(presets.minSize);
   SHINE_Bright_SpatialContrastTrackEdit->SetValue(presets.spatialContrast);
   SHINE_Bright_TemporalContrastTrackEdit->SetValue(presets.temporalContrast);
   SHINE_Bright_MotionToleranceTrackEdit->SetValue(presets.motionTolerance);
   int grainSuppression = -1;
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_alphaPresetHasChanged(void *Sender)
{
	if (GAutoFilter == NULL)
	{
		return;
	}

   auto presets = SHINE_alphaPresets->GetCurrentPreset();
   SHINE_Alpha_MaxSizeTrackEdit->SetValue(presets.maxSize);
   SHINE_Alpha_GrainSuppressionTrackEdit->SetValue(presets.grainSuppression);
   SHINE_Alpha_MotionToleranceTrackEdit->SetValue(presets.motionTolerance);

	AF_initiateDetectionMapLagTiming();
}
//---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_UpdatePresetCurrentValues()
{
	if (GAutoFilter == NULL)
	{
		return;
	}

   ShinePresetParameters darkPresets;
   darkPresets.maxSize = SHINE_Dark_MaxSizeTrackEdit->GetValue();
   darkPresets.minSize = SHINE_Dark_MinSizeTrackEdit->GetValue();
   darkPresets.spatialContrast = SHINE_Dark_SpatialContrastTrackEdit->GetValue();
   darkPresets.temporalContrast = SHINE_Dark_TemporalContrastTrackEdit->GetValue();
   darkPresets.grainSuppression = -1;
   darkPresets.motionTolerance = SHINE_Dark_MotionToleranceTrackEdit->GetValue();
   SHINE_darkPresets->SetCurrent(darkPresets);

   ShinePresetParameters brightPresets;
   brightPresets.maxSize = SHINE_Bright_MaxSizeTrackEdit->GetValue();
   brightPresets.minSize = SHINE_Bright_MinSizeTrackEdit->GetValue();
   brightPresets.spatialContrast = SHINE_Bright_SpatialContrastTrackEdit->GetValue();
   brightPresets.temporalContrast = SHINE_Bright_TemporalContrastTrackEdit->GetValue();
   brightPresets.grainSuppression = -1;
   brightPresets.motionTolerance = SHINE_Bright_MotionToleranceTrackEdit->GetValue();
   SHINE_brightPresets->SetCurrent(brightPresets);

   ShinePresetParameters alphaPresets;
   alphaPresets.maxSize = SHINE_Alpha_MaxSizeTrackEdit->GetValue();
   alphaPresets.minSize = -1;
   alphaPresets.spatialContrast = -1;
   alphaPresets.temporalContrast = -1;
   alphaPresets.grainSuppression = SHINE_Alpha_GrainSuppressionTrackEdit->GetValue();
   alphaPresets.motionTolerance = SHINE_Alpha_MotionToleranceTrackEdit->GetValue();
   SHINE_alphaPresets->SetCurrent(alphaPresets);
}
// ---------------------------------------------------------------------------

void TAutofilterGUI::SHINE_UpdateSettingsGui(const ShineToolSettings &settings)
{
   SHINE_Dark_MaxSizeTrackEdit->SetValue(settings.darkMaxSize);
	SHINE_Dark_MinSizeTrackEdit->SetValue(settings.darkMinSize);
	SHINE_Dark_TemporalContrastTrackEdit->SetValue(settings.darkTemporalContrast);
	SHINE_Dark_SpatialContrastTrackEdit->SetValue(settings.darkSpatialContrast);
	SHINE_Dark_MotionToleranceTrackEdit->SetValue(settings.darkMotionTolerance);

	SHINE_Bright_MaxSizeTrackEdit->SetValue(settings.brightMaxSize);
	SHINE_Bright_MinSizeTrackEdit->SetValue(settings.brightMinSize);
	SHINE_Bright_TemporalContrastTrackEdit->SetValue(settings.brightTemporalContrast);
	SHINE_Bright_SpatialContrastTrackEdit->SetValue(settings.brightSpatialContrast);
	SHINE_Bright_MotionToleranceTrackEdit->SetValue(settings.brightMotionTolerance);

	SHINE_Alpha_MaxSizeTrackEdit->SetValue(settings.alphaMaxSize);
	SHINE_Alpha_GrainSuppressionTrackEdit->SetValue(settings.alphaGrainSuppression);
	SHINE_Alpha_MotionToleranceTrackEdit->SetValue(settings.alphaMotionTolerance);

   SHINE_Dark_EnableButton->Down = settings.detectDark;
   SHINE_Bright_EnableButton->Down = settings.detectBright;
   SHINE_Alpha_EnableButton->Down = settings.detectAlpha;
   SHINE_Dark_EnableButtonClick(nullptr);
   SHINE_Bright_EnableButtonClick(nullptr);
   SHINE_Alpha_EnableButtonClick(nullptr);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TAutofilterGUI::ShinePresetParameters::ReadParameters(CIniFile *iniFile, const string &iniSectionName)
{
   maxSize = iniFile->ReadInteger(iniSectionName, "MaxSize", 50);
   minSize = iniFile->ReadInteger(iniSectionName, "MinSize", 1);
   spatialContrast = iniFile->ReadInteger(iniSectionName, "SpatialContrast", 50);
   temporalContrast = iniFile->ReadInteger(iniSectionName, "TemporalContrast", 50);
   motionTolerance = iniFile->ReadInteger(iniSectionName, "MotionTolerance", 50);
   grainSuppression = iniFile->ReadInteger(iniSectionName, "GrainSuppression", 50);
}
//---------------------------------------------------------------------------

void TAutofilterGUI::ShinePresetParameters::WriteParameters(CIniFile *iniFile, const string &iniSectionName) const
{
   iniFile->WriteInteger(iniSectionName, "MaxSize", maxSize);
   iniFile->WriteInteger(iniSectionName, "MinSize", minSize);
   iniFile->WriteInteger(iniSectionName, "SpatialContrast", spatialContrast);
   iniFile->WriteInteger(iniSectionName, "TemporalContrast", temporalContrast);
   iniFile->WriteInteger(iniSectionName, "MotionTolerance", motionTolerance);
   iniFile->WriteInteger(iniSectionName, "GrainSuppression", grainSuppression);
}
//---------------------------------------------------------------------------

bool TAutofilterGUI::ShinePresetParameters::AreParametersTheSame(const IPresetParameters &presetParameter) const
{
   auto rhs = static_cast<const ShinePresetParameters *>(&presetParameter);
   return *this == *rhs;
}
//---------------------------------------------------------------------------

bool TAutofilterGUI::ShinePresetParameters::operator==(const ShinePresetParameters &rhs) const
{
   return maxSize == rhs.maxSize
       && minSize == rhs.minSize
       && spatialContrast == rhs.spatialContrast
       && temporalContrast == rhs.temporalContrast
       && motionTolerance == rhs.motionTolerance
       && grainSuppression == rhs.grainSuppression;
}
//---------------------------------------------------------------------------

