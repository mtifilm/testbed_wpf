//---------------------------------------------------------------------------

#ifndef FilterStatsFormH
#define FilterStatsFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>

class StatsAF;
//---------------------------------------------------------------------------
class TFilterStatsForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *ControlPanel;
	TLabel *FilterLabel;
	TButton *CloseButton;
	TRichEdit *TextBox;
	TSpeedButton *Filter1Button;
	TSpeedButton *Filter2Button;
	TSpeedButton *Filter3Button;
	TSpeedButton *Filter4Button;
	TSpeedButton *Filter5Button;
	TSpeedButton *Filter6Button;
	void __fastcall FilterButtonClick(TObject *Sender);
	void __fastcall CloseButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
	int _currentFilterIndex;
	StatsAF *_afStats;
public:		// User declarations
	__fastcall TFilterStatsForm(TComponent* Owner, StatsAF *afStats);
   void UpdateDisplay();
};
//---------------------------------------------------------------------------
extern PACKAGE TFilterStatsForm *FilterStatsForm;
//---------------------------------------------------------------------------
#endif
