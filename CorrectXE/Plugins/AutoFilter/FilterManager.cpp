
#include "FilterManager.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include <algorithm>
#include <sstream>
//---------------------------------------------------------------------------

// Section Names/Prefixes

#define GENERAL_SECTION_NAME         "General"
#define FILTER_SECTION_PREFIX        "Filter_"
#define DEFAULTS_SECTION_PREFIX      "Defaults_"
#define FILTER_SET_SECTION_PREFIX    "FilterSet_"

#define PREVIEW_HIGHLIGHT_KEY        "PreviewHighlight"  // [GENERAL]

#define CURRENT_DEFAULTS_VERSION     2


// FILTER KEYS for INI file

#define FILTER_NAME_KEY              "filterName"
#define FILTER_ID_KEY                "filterID"

#define DEFAULTS_VERSION_KEY         "defaultsVersion"

#define MAX_SIZE_KEY                 "maxSize"
#define MIN_SIZE_KEY                 "minSize"
#define MIN_CONTRAST_KEY             "minContrast"       // == "Contrast Sensitivity"
#define LUMINANCE_THRESHOLD_KEY      "luminanceThreshold" // alpha only
#define MIN_FIX_CONFIDENCE_KEY       "minFixConfidence"  // == "Fix Tolerance"
#define RGB_MOTION_KEY               "horizontalMotion"  // For legacy reasons!!
#define ALPHA_MOTION_KEY             "alphaMotion"
#define EDGE_BLUR_KEY                "correctionEdgeBlur"

#define BRIGHT_CONTRAST_KEY          "brightOnDarkContrast"
#define USE_HARD_MOTION_KEY          "useHardMotionOnDust"
#define DUST_MAX_SIZE_KEY            "dustMaxSize"
#define USE_ALPHA_AS_MASK_KEY        "useAlphaAsMask"
#define USE_GRAIN_FILTER_KEY         "useGrainFilter"
#define GRAIN_FILTER_PARAM_KEY       "grainFilterParam"

#define DETECT_CHANNEL_KEY           "detectChannel"
#define PROCESS_RED_CHANNEL_KEY      "processRedChannel"
#define PROCESS_GREEN_CHANNEL_KEY    "processGreenChannel"
#define PROCESS_BLUE_CHANNEL_KEY     "processBlueChannel"

// FILTER SET KEYS for INI file

// Pure laziness...
#if (NUMBER_OF_FILTERS_PER_SET > 6)
Yo! Fix the crap below here!
#endif

#define FILTER_SET_NAME_KEY  "filterSetName"
#define FILTER_SET_ID_KEY    "filterSetID"

#define LOADED_1_KEY         "loaded[1]"
#define USED_1_KEY           "used[1]"
#define FILTER_1_ID_KEY      "filterID[1]"

#define LOADED_2_KEY         "loaded[2]"
#define USED_2_KEY           "used[2]"
#define FILTER_2_ID_KEY      "filterID[2]"

#define LOADED_3_KEY         "loaded[3]"
#define USED_3_KEY           "used[3]"
#define FILTER_3_ID_KEY      "filterID[3]"

#define LOADED_4_KEY         "loaded[4]"
#define USED_4_KEY           "used[4]"
#define FILTER_4_ID_KEY      "filterID[4]"

#define LOADED_5_KEY         "loaded[5]"
#define USED_5_KEY           "used[5]"
#define FILTER_5_ID_KEY      "filterID[5]"

#define LOADED_6_KEY         "loaded[6]"
#define USED_6_KEY           "used[6]"
#define FILTER_6_ID_KEY      "filterID[6]"

static const char *LoadedKeys[] = {
   LOADED_1_KEY, LOADED_2_KEY, LOADED_3_KEY,
   LOADED_4_KEY, LOADED_5_KEY, LOADED_6_KEY
   };
static const char *UsedKeys[] = {
   USED_1_KEY, USED_2_KEY, USED_3_KEY,
   USED_4_KEY, USED_5_KEY, USED_6_KEY
   };
static const char *FilterIDKeys[] = {
   FILTER_1_ID_KEY, FILTER_2_ID_KEY, FILTER_3_ID_KEY,
   FILTER_4_ID_KEY, FILTER_5_ID_KEY, FILTER_6_ID_KEY
   };

   // "Factory defaults" for filters

#define MAX_SIZE_DEFAULT                 50
#define MIN_SIZE_DEFAULT                  0
#define MIN_CONTRAST_DEFAULT             50
#define LUMINANCE_THRESHOLD_DEFAULT     100
#define MIN_FIX_CONFIDENCE_DEFAULT       50
#define HORIZ_MOTION_DEFAULT_V1          50
#define RGB_MOTION_DEFAULT               10
#define ALPHA_MOTION_DEFAULT             50
#define EDGE_BLUR_DEFAULT                 2    // CAREFUL! Range is 0 - 10!!
#define BRIGHT_CONTRAST_DEFAULT         true   // VARIABLE per built-in filter
#define USE_HARD_MOTION_DEFAULT        false
#define DUST_MAX_SIZE_DEFAULT             2
#define USE_ALPHA_AS_MASK_DEFAULT      false
#define USE_GRAIN_FILTER_DEFAULT       false
#define GRAIN_FILTER_PARAM_DEFAULT        50

#define DETECT_CHANNEL_DEFAULT            1    // VARIABLE per built-in
#define PROCESS_RED_CHANNEL_DEFAULT     true
#define PROCESS_GREEN_CHANNEL_DEFAULT   true
#define PROCESS_BLUE_CHANNEL_DEFAULT    true

#define COLOR_MIN_DEFAULT    0
#define COLOR_MAX_DEFAULT  255

namespace {
   inline bool has_prefix(const string &str, const string &pref)
   {
      return (str.substr(0, pref.size()) == pref);
   }
}

//---------------------------------------------------------------------------

SFilter::SFilter()
{
   name = new string;

	maxSize = MAX_SIZE_DEFAULT;
	minSize = MIN_SIZE_DEFAULT;
	minContrast = MIN_CONTRAST_DEFAULT;
	luminanceThreshold = LUMINANCE_THRESHOLD_DEFAULT;
   minFixConfidence = MIN_FIX_CONFIDENCE_DEFAULT;
   rgbMotion = RGB_MOTION_DEFAULT;
   alphaMotion = ALPHA_MOTION_DEFAULT;
	correctionEdgeBlur = EDGE_BLUR_DEFAULT;
   brightOnDarkContrast = BRIGHT_CONTRAST_DEFAULT;
	useHardMotionOnDust = USE_HARD_MOTION_DEFAULT;
   dustMaxSize = DUST_MAX_SIZE_DEFAULT;
	useAlphaAsMask = USE_ALPHA_AS_MASK_DEFAULT;
   useGrainFilter = USE_GRAIN_FILTER_DEFAULT;
   grainFilterParam = GRAIN_FILTER_PARAM_DEFAULT;

   // Advanced parameters
   detectChannel = DETECT_CHANNEL_DEFAULT;
   processChannels[RED_FILTER_CHANNEL] =  PROCESS_RED_CHANNEL_DEFAULT;
   processChannels[GREEN_FILTER_CHANNEL] = PROCESS_GREEN_CHANNEL_DEFAULT;
   processChannels[BLUE_FILTER_CHANNEL] = PROCESS_BLUE_CHANNEL_DEFAULT;
}

SFilter::~SFilter()
{
   delete name;
}

SFilter::SFilter(const SFilter &rhs)
{
   name = new string;

   *this = rhs;
}

SFilter& SFilter::operator=(const SFilter& rhs)
{
	if (this == &rhs)
	{
		return *this;
	}

	SFilter &lhs = *this;

	*lhs.name = *rhs.name;

	lhs.maxSize = rhs.maxSize;
	lhs.minSize = rhs.minSize;
	lhs.minContrast = rhs.minContrast;
	lhs.luminanceThreshold = rhs.luminanceThreshold;
	lhs.minFixConfidence = rhs.minFixConfidence;
	lhs.rgbMotion = rhs.rgbMotion;
	lhs.alphaMotion = rhs.alphaMotion;
	lhs.correctionEdgeBlur = rhs.correctionEdgeBlur;
	lhs.brightOnDarkContrast = rhs.brightOnDarkContrast;
	lhs.useHardMotionOnDust = rhs.useHardMotionOnDust;
	lhs.dustMaxSize = rhs.dustMaxSize;
	lhs.useAlphaAsMask = rhs.useAlphaAsMask;
	lhs.useGrainFilter = rhs.useGrainFilter;
	lhs.grainFilterParam = rhs.grainFilterParam;

	// Advanced parameters
	lhs.detectChannel = rhs.detectChannel;
	lhs.processChannels[RED_FILTER_CHANNEL] = rhs.processChannels[RED_FILTER_CHANNEL];
	lhs.processChannels[GREEN_FILTER_CHANNEL] = rhs.processChannels[GREEN_FILTER_CHANNEL];
	lhs.processChannels[BLUE_FILTER_CHANNEL] = rhs.processChannels[BLUE_FILTER_CHANNEL];

	return lhs;
}

bool SFilter::operator==(const SFilter& rhs)
{
	SFilter &lhs = *this;
	bool retVal = false;

   if (this == &rhs)
	{
		retVal = true;
   }
	else if (
      lhs.detectChannel == rhs.detectChannel
   && lhs.processChannels[RED_FILTER_CHANNEL] == rhs.processChannels[RED_FILTER_CHANNEL]
   && lhs.processChannels[GREEN_FILTER_CHANNEL] == rhs.processChannels[GREEN_FILTER_CHANNEL]
	&& lhs.processChannels[BLUE_FILTER_CHANNEL] == rhs.processChannels[BLUE_FILTER_CHANNEL]

	&& ((lhs.detectChannel == ALPHA_FILTER_CHANNEL)
      ? (lhs.useGrainFilter == rhs.useGrainFilter
         && lhs.grainFilterParam == rhs.grainFilterParam
         && lhs.luminanceThreshold == rhs.luminanceThreshold
        )
      : (lhs.brightOnDarkContrast == rhs.brightOnDarkContrast
         && lhs.minSize == rhs.minSize
			&& lhs.minContrast == rhs.minContrast
         && lhs.useAlphaAsMask == rhs.useAlphaAsMask)
        )

   && lhs.maxSize == rhs.maxSize
   && lhs.minFixConfidence == rhs.minFixConfidence
   && lhs.rgbMotion == rhs.rgbMotion
   && lhs.alphaMotion == rhs.alphaMotion
   && lhs.correctionEdgeBlur == rhs.correctionEdgeBlur
   && lhs.useHardMotionOnDust == rhs.useHardMotionOnDust
	&& lhs.dustMaxSize == rhs.dustMaxSize
	)
   {
      retVal = true;
   }

   return retVal;
}

bool SFilter::operator!=(const SFilter& rhs)
{
	return !operator==(rhs);
}

void SFilter::SetName(const string &newName)
{
   *name = newName;
}

string SFilter::GetName(void)
{
   return *name;
}

void SFilter::ReadFromIniFile(CIniFile *ini, const string &sectionName)
{
   *name =                ini->ReadString(sectionName,  FILTER_NAME_KEY,             *name);

	maxSize =              ini->ReadInteger(sectionName, MAX_SIZE_KEY,                maxSize);
	minSize =              ini->ReadInteger(sectionName, MIN_SIZE_KEY,                minSize);
	minContrast =          ini->ReadInteger(sectionName, MIN_CONTRAST_KEY,            minContrast);
	luminanceThreshold =   ini->ReadInteger(sectionName, LUMINANCE_THRESHOLD_KEY,    luminanceThreshold);
   minFixConfidence =     ini->ReadInteger(sectionName, MIN_FIX_CONFIDENCE_KEY,      minFixConfidence);
   rgbMotion =            ini->ReadInteger(sectionName, RGB_MOTION_KEY,              rgbMotion);
   alphaMotion =          ini->ReadInteger(sectionName, ALPHA_MOTION_KEY,            alphaMotion);
   correctionEdgeBlur =   ini->ReadInteger(sectionName, EDGE_BLUR_KEY,               correctionEdgeBlur);

   brightOnDarkContrast = ini->ReadInteger(sectionName, BRIGHT_CONTRAST_KEY,         brightOnDarkContrast);
   dustMaxSize =          ini->ReadInteger(sectionName, DUST_MAX_SIZE_KEY,           dustMaxSize);
	useHardMotionOnDust =  ini->ReadBool(sectionName,    USE_HARD_MOTION_KEY,         useHardMotionOnDust);
	useAlphaAsMask        =ini->ReadBool(sectionName,    USE_ALPHA_AS_MASK_KEY,       useAlphaAsMask);
	useGrainFilter =       ini->ReadBool(sectionName,    USE_GRAIN_FILTER_KEY,        useGrainFilter);
   grainFilterParam =     ini->ReadInteger(sectionName, GRAIN_FILTER_PARAM_KEY,      grainFilterParam);

// Should be safe to remove this now!
//   // UGLY HACK because defaults changed and old defaults were written to the ini file.
//   if (has_prefix(sectionName, DEFAULTS_SECTION_PREFIX))
//   {
//      int defaultsVersion =  ini->ReadInteger(sectionName, DEFAULTS_VERSION_KEY, 1);
//      if (defaultsVersion < CURRENT_DEFAULTS_VERSION)
//      {
//         if (rgbMotion == HORIZ_MOTION_DEFAULT_V1 && alphaMotion == VERT_MOTION_DEFAULT_V1)
//         {
//            rgbMotion = HORIZ_MOTION_DEFAULT;
//            alphaMotion = VERT_MOTION_DEFAULT;
//            ini->WriteInteger(sectionName, RGB_MOTION_KEY, rgbMotion);
//            ini->WriteInteger(sectionName, ALPHA_MOTION_KEY, alphaMotion);
//         }
//
//         ini->WriteInteger(sectionName, DEFAULTS_VERSION_KEY, CURRENT_DEFAULTS_VERSION);
//      }
//   }

      // Advanced parameters
   detectChannel =        ini->ReadInteger(sectionName, DETECT_CHANNEL_KEY,
                                                         detectChannel);
   processChannels[RED_FILTER_CHANNEL] =   ini->ReadBool(sectionName,
                                                   PROCESS_RED_CHANNEL_KEY,
                                       processChannels[RED_FILTER_CHANNEL]);
   processChannels[GREEN_FILTER_CHANNEL] = ini->ReadBool(sectionName,
                                                   PROCESS_GREEN_CHANNEL_KEY,
                                       processChannels[GREEN_FILTER_CHANNEL]);
   processChannels[BLUE_FILTER_CHANNEL] =  ini->ReadBool(sectionName,
                                                   PROCESS_BLUE_CHANNEL_KEY,

                                       processChannels[BLUE_FILTER_CHANNEL]);

//	minR = ini->ReadInteger(sectionName, MIN_R_KEY, minR);
//	minG = ini->ReadInteger(sectionName, MIN_G_KEY, minG);
//	minB = ini->ReadInteger(sectionName, MIN_B_KEY, minB);
//	medR = ini->ReadInteger(sectionName, MED_R_KEY, medR);
//	medG = ini->ReadInteger(sectionName, MED_G_KEY, medG);
//	medB = ini->ReadInteger(sectionName, MED_B_KEY, medB);
//	maxR = ini->ReadInteger(sectionName, MAX_R_KEY, maxR);
//	maxG = ini->ReadInteger(sectionName, MAX_G_KEY, maxG);
//	maxB = ini->ReadInteger(sectionName, MAX_B_KEY, maxB);

   *name = ini->ReadString(sectionName, FILTER_NAME_KEY, *name);
}

void SFilter::WriteToIniFile(CIniFile *ini, const string &sectionName)
{
   ini->WriteString(sectionName,  FILTER_NAME_KEY,        *name);

   if (has_prefix(sectionName, DEFAULTS_SECTION_PREFIX))
   {
	   ini->WriteInteger(sectionName, DEFAULTS_VERSION_KEY,   CURRENT_DEFAULTS_VERSION);
   }

	ini->WriteInteger(sectionName, MAX_SIZE_KEY,           maxSize);
	ini->WriteInteger(sectionName, MIN_SIZE_KEY,           minSize);
	ini->WriteInteger(sectionName, MIN_CONTRAST_KEY,       minContrast);
	ini->WriteInteger(sectionName, LUMINANCE_THRESHOLD_KEY, luminanceThreshold);
	ini->WriteInteger(sectionName, MIN_FIX_CONFIDENCE_KEY, minFixConfidence);
   ini->WriteInteger(sectionName, RGB_MOTION_KEY,         rgbMotion);
   ini->WriteInteger(sectionName, ALPHA_MOTION_KEY,       alphaMotion);
	ini->WriteInteger(sectionName, EDGE_BLUR_KEY,          correctionEdgeBlur);
	ini->WriteInteger(sectionName, BRIGHT_CONTRAST_KEY,    brightOnDarkContrast);
	ini->WriteBool   (sectionName, USE_HARD_MOTION_KEY,    useHardMotionOnDust);
   ini->WriteInteger(sectionName, DUST_MAX_SIZE_KEY,      dustMaxSize);
	ini->WriteBool   (sectionName, USE_ALPHA_AS_MASK_KEY,  useAlphaAsMask);
	ini->WriteBool   (sectionName, USE_GRAIN_FILTER_KEY,   useGrainFilter);
   ini->WriteInteger(sectionName, GRAIN_FILTER_PARAM_KEY, grainFilterParam);

      // Advanced parameters
   ini->WriteInteger(sectionName, DETECT_CHANNEL_KEY,     detectChannel);
   ini->WriteBool(sectionName, PROCESS_RED_CHANNEL_KEY,   processChannels[RED_FILTER_CHANNEL]);
   ini->WriteBool(sectionName, PROCESS_GREEN_CHANNEL_KEY, processChannels[GREEN_FILTER_CHANNEL]);
   ini->WriteBool(sectionName, PROCESS_BLUE_CHANNEL_KEY,  processChannels[BLUE_FILTER_CHANNEL]);
}
//---------------------------------------------------------------------------

SFilterSet::SFilterSet()
{
}

SFilterSet::~SFilterSet()
{
}

int SFilterSet::LoadFilter(const string &filterID, bool useFlag)
{
   int retVal = -1;

   for (int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
   {
      if (!filterSlots[slot].loaded)
      {
         retVal = slot;
         filterSlots[slot].loaded = true;
         filterSlots[slot].used = useFlag;
         filterSlots[slot].filterID = filterID;
         break;
      }
   }

   return retVal;
}

void SFilterSet::UnloadFilter(const string &filterID)
{
   UnloadFilter(FindSlot(filterID));
}

SFilterSlot SFilterSet::GetSlotInfo(int slot)
{
   SFilterSlot retVal;

   if (slot >= 0 && slot < NUMBER_OF_FILTERS_PER_SET)
      retVal = filterSlots[slot];

   return retVal;
}


void SFilterSet::UnloadAllFilters()
{
   for (int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
   {
      filterSlots[slot].loaded = false;
      filterSlots[slot].used = false;
      filterSlots[slot].filterID = INVALID_ID;
   }
}

void SFilterSet::UnloadFilter(int unloadSlot)
{
   if (unloadSlot >= 0 && unloadSlot < NUMBER_OF_FILTERS_PER_SET)
   {
      // Rearrange slots to not leave a hole
      for (int slot = unloadSlot + 1; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
         filterSlots[slot-1] = filterSlots[slot];

      // The slot to clear is now the last one!
      unloadSlot = NUMBER_OF_FILTERS_PER_SET - 1;
      filterSlots[unloadSlot].loaded = false;
      filterSlots[unloadSlot].used = false;
      filterSlots[unloadSlot].filterID = INVALID_ID;
   }
}
int SFilterSet::FindSlot(const string &filterID)
{
   int retVal = INVALID_SLOT_NUMBER;

   for (int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
   {
      if (filterID == INVALID_ID)
         break;

      if (filterSlots[slot].filterID == filterID)
      {
         retVal = slot;
         break;
      }
   }

   return retVal;
}

string SFilterSet::GetFilterIDFromSlotNumber(int slotNumber)
{
   string retVal = INVALID_ID;

   if (slotNumber >= 0 && slotNumber < NUMBER_OF_FILTERS_PER_SET)
      retVal = filterSlots[slotNumber].filterID;

   return retVal;
}

string SFilterSet::GetNextLoadedFilterID(const string &filterID, int direction)
{
   int slotNumber = FindSlot(filterID);
   slotNumber = GetNextLoadedSlotNumber(slotNumber, direction);
   return GetFilterIDFromSlotNumber(slotNumber);

}

int SFilterSet::GetNextLoadedSlotNumber(int slotNumber, int direction)
{
   int retVal = INVALID_SLOT_NUMBER;
   int firstVacantSlotNumber = INVALID_SLOT_NUMBER;
   int i;

	for (firstVacantSlotNumber = 0; firstVacantSlotNumber < NUMBER_OF_FILTERS_PER_SET; ++firstVacantSlotNumber)
	{
		if (!filterSlots[firstVacantSlotNumber].loaded)
		{
			break;
		}
	}
	if (firstVacantSlotNumber > 0)
	{
		if (slotNumber == INVALID_SLOT_NUMBER)
		{
			if (direction > 0)
			{
				retVal = 0;
			}
			else
			{
				retVal = firstVacantSlotNumber - 1;
			}
		}
		else
		{
			if (direction > 0 && slotNumber == (firstVacantSlotNumber - 1))
			{
				retVal = 0;
			}
			else if (direction > 0)
			{
				retVal = slotNumber + 1;
			}
			else if (slotNumber == 0)
			{
				retVal = firstVacantSlotNumber - 1;
			}
			else
			{
				retVal = slotNumber - 1;
			}
		}
	}

	return retVal;
}

void SFilterSet::ToggleUsed(const string &filterID)
{
   int slot = FindSlot(filterID);
   ToggleUsed(slot);
}

void SFilterSet::ToggleUsed(int slotNumber)
{
   if (slotNumber >= 0 && slotNumber < NUMBER_OF_FILTERS_PER_SET)
   {
      filterSlots[slotNumber].used = !filterSlots[slotNumber].used;
   }
}


void SFilterSet::SetName(const string &newName)
{
   name = newName;
}

string SFilterSet::GetName(void)
{
   return name;
}

void SFilterSet::ReadFromIniFile(CIniFile *ini, const string &sectionName)
{
   for (int i = 0; i < NUMBER_OF_FILTERS_PER_SET; ++i)
   {
      filterSlots[i].loaded =   ini->ReadBool(sectionName, LoadedKeys[i],
                                              filterSlots[i].loaded);
      filterSlots[i].used =     ini->ReadBool(sectionName, UsedKeys[i],
                                              filterSlots[i].used);
      filterSlots[i].filterID = ini->ReadString(sectionName, FilterIDKeys[i],
                                              filterSlots[i].filterID);
   }
}

void SFilterSet::WriteToIniFile(CIniFile *ini, const string &sectionName)
{
   ini->WriteString(sectionName, FILTER_SET_NAME_KEY, name);

   for (int i = 0; i < NUMBER_OF_FILTERS_PER_SET; ++i)
   {
      ini->WriteBool(sectionName, LoadedKeys[i], filterSlots[i].loaded);
      if (filterSlots[i].loaded)
      {
         ini->WriteBool(sectionName, UsedKeys[i], filterSlots[i].used);
         ini->WriteString(sectionName, FilterIDKeys[i],
                                       filterSlots[i].filterID);
      }
   }
}
//---------------------------------------------------------------------------

CFilterManager::CFilterManager(bool alphaIsOKArg)
{
   alphaIsOK = alphaIsOKArg;
   ActiveFilterSetID = alphaIsOK ? AF_DEFAULT_FILTER_SET_ID : SHINE_DEFAULT_FILTER_SET_ID;
}

CFilterManager::~CFilterManager()
{
}

void CFilterManager::ReadFromIniFile(const string &iniFileName)
{
   // DO NOT SET THIS READONLY! Our stupid "defaults version" hack might want to write to the file!
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini != NULL)
   {
      // Create default filters and sets - they may be overridden by
      // entries in the ini file
      CreateBuiltInFilters();
      CreateBuiltInFilterSets();

      // Read all filters and filter sets
      StringList allSections;
      ini->ReadSections(&allSections);

      // Read in all the FILTERS first
      for (unsigned int i = 0; i < allSections.size(); ++i)
      {
         string sectionName(allSections[i]);

         if (has_prefix(sectionName, FILTER_SECTION_PREFIX))
         {
            string filterID = ini->ReadString(sectionName, FILTER_ID_KEY,
                                              INVALID_ID);
            if (filterID == INVALID_ID)
            {
               // Bogus ID - skip it!
               continue;
            }

            // UGLEEE - have to uniquify the name before creating the filter!
            // Double uglee: EXCEPT FOR BUILT-IN FILTERS! This sucks!
            string filterName = ini->ReadString(sectionName,
                                                FILTER_NAME_KEY, "");
            if (filterName.empty())
            {
               // Bogus entry - skip it!
               continue;
            }
            if (filterID != AF_DEFAULT_FILTER_ID &&
                filterID != AF_WHITE_FILTER_ID &&
                filterID != AF_BLACK_FILTER_ID &&
                filterID != AF_RED_LIGHT_FILTER_ID &&
                filterID != AF_GREEN_LIGHT_FILTER_ID &&
                filterID != AF_BLUE_LIGHT_FILTER_ID &&
                filterID != AF_ALPHA_FILTER_ID &&
                filterID != AF_RED_DARK_FILTER_ID &&
                filterID != AF_GREEN_DARK_FILTER_ID &&
                filterID != AF_BLUE_DARK_FILTER_ID)
            {
               filterName = UniquifyFilterName(filterName);
            }

            // [] operator will create a map entry with default SFilter
            FilterList[filterID].ReadFromIniFile(ini, sectionName);

            FilterList[filterID].SetName(filterName);

            // Initialize defaults from current in case there is are
            // no defaults for this filter
            DefaultsList[filterID] = FilterList[filterID];

            // If alpha is not allowed and this is an alpha-detect filter -
            // kill the filter
            if ((FilterList[filterID].detectChannel == ALPHA_FILTER_CHANNEL)
                   && !alphaIsOK)
            {
               DeleteFilter(filterID);
            }
         }
      }

      // Then read in all the DEFAULTS
      for (unsigned int i = 0; i < allSections.size(); ++i)
      {
         string sectionName(allSections[i]);

         if (has_prefix(sectionName, DEFAULTS_SECTION_PREFIX))
         {
            string filterID = ini->ReadString(sectionName, FILTER_ID_KEY,
                                              INVALID_ID);
            if (filterID != INVALID_ID)
            {
               // Toss these defaults if there is no corresponding filter!
               map<string,SFilter>::iterator filterIter =
                                                FilterList.find(filterID);
               if (filterIter != FilterList.end())
               {
                  // [] operator will create a map entry with default SFilter
                  DefaultsList[filterID].ReadFromIniFile(ini, sectionName);
               }
            }
         }
      }

      // Finally read all the FILTER SETS
      for (unsigned int i = 0; i < allSections.size(); ++i)
      {
         string sectionName(allSections[i]);

         if (has_prefix(sectionName, FILTER_SET_SECTION_PREFIX))
         {
            string filterSetID = ini->ReadString(sectionName,
                                                 FILTER_SET_ID_KEY,
                                                 INVALID_ID);
            if (filterSetID == INVALID_ID)
            {
               // Bogus ID - skip it!
               continue;
            }

            // UGLEEE - have to uniquify the name before creating the set!
            string filterSetName = ini->ReadString(sectionName,
                                                   FILTER_SET_NAME_KEY, "");
            if (filterSetName.empty())
            {
               // Bogus entry - skip it!
               continue;
            }
            filterSetName = UniquifyFilterName(filterSetName);

            // [] operator will create a map entry with default SFilterSet
            FilterSetList[filterSetID].ReadFromIniFile(ini, sectionName);

            FilterSetList[filterSetID].SetName(filterSetName);

            // Sanitize the filter set by removing bogus filters
            SFilterSet &set = FilterSetList[filterSetID];
            for (int slotNum = 0; slotNum < NUMBER_OF_FILTERS_PER_SET; ++slotNum)
            {
               SFilterSlot slot = set.GetSlotInfo(slotNum);
               if (slot.loaded)
               {
                  map<string,SFilter>::iterator filterIter =
                                       FilterList.find(slot.filterID);
                  if (filterIter == FilterList.end())
                  {
                     set.UnloadFilter(slotNum);
                     --slotNum;
                  }
               }
            }

            // If the set is empty - kill the filter set - except
            // allow the default (aka the 'working' set to be empty
            if (filterSetID != (alphaIsOK ? AF_DEFAULT_FILTER_SET_ID : SHINE_DEFAULT_FILTER_SET_ID)
            && (FilterSetList[filterSetID].GetFilterIDFromSlotNumber(0) == INVALID_ID))
            {
               DeleteFilterSet(filterSetID);
            }
         }
      } // for all sections
   }

   // Done
   DeleteIniFile(ini);
}

void CFilterManager::WriteToIniFile(const string &iniFileName)
{
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini != NULL)
   {
      // Trash all existing info
      StringList allSections;
      ini->ReadSections(&allSections);
      for (unsigned int i = 0; i < allSections.size(); ++i)
      {
         string sectionName(allSections[i]);
         if (has_prefix(sectionName, FILTER_SECTION_PREFIX) ||
             has_prefix(sectionName, FILTER_SET_SECTION_PREFIX) ||
             has_prefix(sectionName, "AutoFilterForm"))  // old crap
         {
            ini->EraseSection(allSections[i]);
         }
      }

      // Write all the filters
      map<string,SFilter>::iterator filterStart = FilterList.begin();
      map<string,SFilter>::iterator filterEnd = FilterList.end();
      map<string,SFilter>::iterator filterIter;
      int ordinal;

      for (ordinal = 1, filterIter = filterStart;
           filterIter != filterEnd;
           ++filterIter, ++ordinal)
      {
         string filterID(filterIter->first);
         SFilter &filter = filterIter->second;

         if (filterID == PDL_0_FILTER_ID ||
             filterID == PDL_1_FILTER_ID ||
             filterID == PDL_2_FILTER_ID ||
             filterID == PDL_3_FILTER_ID ||
             filterID == PDL_4_FILTER_ID ||
             filterID == PDL_5_FILTER_ID)
         {
             // Invisible - not saved
             continue;
         }

         MTIostringstream os;
         os << FILTER_SECTION_PREFIX << setw(4) << setfill('0') << ordinal;
         string sectionName(os.str());

         ini->WriteString(sectionName, FILTER_ID_KEY, filterID);
         filter.WriteToIniFile(ini, sectionName);
      }

      // Write all the defaults
      filterStart = DefaultsList.begin();
      filterEnd = DefaultsList.end();

      for (ordinal = 1, filterIter = filterStart;
           filterIter != filterEnd;
           ++filterIter, ++ordinal)
      {
         string filterID(filterIter->first);
         SFilter &filter = filterIter->second;

         if (filterID == PDL_0_FILTER_ID ||
             filterID == PDL_1_FILTER_ID ||
             filterID == PDL_2_FILTER_ID ||
             filterID == PDL_3_FILTER_ID ||
             filterID == PDL_4_FILTER_ID ||
             filterID == PDL_5_FILTER_ID)
         {
             // Invisible - not saved
             continue;
         }

         MTIostringstream os;
         os << DEFAULTS_SECTION_PREFIX << setw(4) << setfill('0') << ordinal;
         string sectionName(os.str());

         ini->WriteString(sectionName, FILTER_ID_KEY, filterID);
         filter.WriteToIniFile(ini, sectionName);
      }

      // Write all the filter sets
      map<string,SFilterSet>::iterator setStart = FilterSetList.begin();
      map<string,SFilterSet>::iterator setEnd = FilterSetList.end();
      map<string,SFilterSet>::iterator setIter;

      for (ordinal = 1, setIter = setStart;
           setIter != setEnd;
           ++setIter, ++ordinal)
      {
         string filterSetID(setIter->first);
         SFilterSet &filterSet = setIter->second;

         if (filterSetID == PDL_FILTER_SET_ID)
         {
             // Invisible - not saved
             continue;
         }

         MTIostringstream os;
         os << FILTER_SET_SECTION_PREFIX << setw(4) << setfill('0') << ordinal;
         string sectionName(os.str());

         ini->WriteString(sectionName, FILTER_SET_ID_KEY, filterSetID);
         filterSet.WriteToIniFile(ini, sectionName);
      }

      // Done
      DeleteIniFile(ini);
   }
}

// Returns filter ID of found filter (or INVALID_ID)
// Should really return a list of names but we've swiched to them being
// unique
string CFilterManager::FindFilterByName(const string &name)
{
   string retVal = INVALID_ID;

   // Brute force search
   map<string,SFilter>::iterator filterStart = FilterList.begin();
   map<string,SFilter>::iterator filterEnd = FilterList.end();
   map<string,SFilter>::iterator filterIter;
   int ordinal;

   for (filterIter = filterStart; filterIter != filterEnd; ++filterIter)
   {
      SFilter &filter = filterIter->second;

      if (*filter.name == name)
      {
         retVal = filterIter->first;
         break;
      }
   }

   return retVal;
}

// Returns filter set ID of found filter set (or INVALID_ID)
// Should really return a list of names but we've swiched to them being
// unique
string CFilterManager::FindFilterSetByName(const string &name)
{
   string retVal = INVALID_ID;

   // Brute force search
   map<string,SFilterSet>::iterator setStart = FilterSetList.begin();
   map<string,SFilterSet>::iterator setEnd = FilterSetList.end();
   map<string,SFilterSet>::iterator setIter;

   for (setIter = setStart; setIter != setEnd; ++setIter)
   {
      SFilterSet &filterSet = setIter->second;

      if (filterSet.name == name)
      {
         retVal = setIter->first;
         break;
      }
   }

   return retVal;
}

string CFilterManager::AddFilter(const SFilter &filter)
{
   string guid = guid_to_string(guid_create());

   FilterList[guid] = filter;
   DefaultsList[guid] = filter;
   return guid;
}

int CFilterManager::LoadFilter(const string &filterID, bool useFlag)
{
   int slot = INVALID_SLOT_NUMBER;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      slot = activeFilterSet.LoadFilter(filterID, useFlag);
   }

   return slot;
}

void CFilterManager::UnloadFilter(const string &filterID)
{
   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      int slot = activeFilterSet.FindSlot(filterID);
      if (slot != INVALID_SLOT_NUMBER)
        activeFilterSet.UnloadFilter(slot);
   }
}

int CFilterManager::CopyFilter(const string &filterID)
{
   int slot = INVALID_SLOT_NUMBER;

   // Enforce unique filter names!


   SFilter *rhs = GetFilter(filterID);
   if (rhs != NULL)
   {
      SFilter lhs(*rhs);
      *(lhs.name) = UniquifyFilterName(*(rhs->name));
      string newFilterID = AddFilter(lhs);

      if (newFilterID != INVALID_ID)
      {
         slot = LoadFilter(newFilterID);
      }
   }

   return slot;
}

void CFilterManager::DeleteFilter(const string &filterID)
{
   // Not allowed to delete the default filter
   if (filterID == AF_DEFAULT_FILTER_ID)
      return;

   if (filterID == SHINE_DEFAULT_FILTER_ID)
      return;

   map<string,SFilterSet>::iterator setIter;
   map<string,SFilterSet>::iterator start = FilterSetList.begin();
   map<string,SFilterSet>::iterator stop = FilterSetList.end();

   for (setIter = start; setIter != stop; ++setIter)
   {
      string setID(setIter->first);
      FilterSetList[setID].UnloadFilter(filterID);
   }

   map<string,SFilter>::iterator filterIter = FilterList.find(filterID);
   if (filterIter != FilterList.end())
      FilterList.erase(filterIter);

   filterIter = DefaultsList.find(filterID);
   if (filterIter != DefaultsList.end())
      DefaultsList.erase(filterIter);
}

SFilter CFilterManager::GetFilterDetails(const string &filterID)
{
   SFilter retVal;
   map<string,SFilter>::iterator iter = FilterList.find(filterID);

   if (iter != FilterList.end())
      retVal = iter->second;

   return retVal;
}

SFilter *CFilterManager::GetFilter(const string &filterID)
{
   SFilter *retVal = NULL;
   map<string,SFilter>::iterator iter = FilterList.find(filterID);

   if (iter != FilterList.end())
      retVal = &(iter->second);

   return retVal;
}

void CFilterManager::SaveFilterDefaults(const string &filterID)
{
   map<string,SFilter>::iterator filterIter = FilterList.find(filterID);

   if (filterIter != FilterList.end())
   {
       DefaultsList[filterID] = filterIter->second;
   }
   else
   {
      // IT'S AN ERROR QQQ
   }
}

void CFilterManager::RestoreFilterDefaults(const string &filterID)
{
   map<string,SFilter>::iterator filterIter = FilterList.find(filterID);

   if (filterIter != FilterList.end())
   {
      map<string,SFilter>::iterator defaultsIter = DefaultsList.find(filterID);
      if (defaultsIter != DefaultsList.end())
      {
         // Avoid trashing the name
         // SUGGESTION: Leave name blank on defaults filters and don't copy
         // blank names in general QQQ
         defaultsIter->second.SetName(filterIter->second.GetName());
         filterIter->second = defaultsIter->second;
      }
      else
      {
         // No defaults. Now what? Just no-op, I guess
      }
   }
   else
   {
      // This is an error QQQ
   }
}

bool CFilterManager::DoesFilterDifferFromDefaults(const string &filterID)
{
   bool retVal = false;
   map<string,SFilter>::iterator filterIter = FilterList.find(filterID);

   if (filterIter != FilterList.end())
   {
      map<string,SFilter>::iterator defaultsIter = DefaultsList.find(filterID);

      if (defaultsIter != DefaultsList.end())
      {
         retVal = (filterIter->second != defaultsIter->second);
      }
   }

   return retVal;
}

SFilterSlot CFilterManager::GetActiveFilterSlotInfo(int slot)
{
   SFilterSlot retVal;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      retVal = activeFilterSet.GetSlotInfo(slot);
   }

   return retVal;
}

void CFilterManager::GetListOfFilterNames(map<string,string> &filterNames)
{
   map<string,SFilter>::iterator start = FilterList.begin();
   map<string,SFilter>::iterator end = FilterList.end();
   map<string,SFilter>::iterator iter;

   filterNames.clear();
   for (iter = start; iter != end; ++iter)
   {
      string filterName(*((iter->second).name));

      if (!filterName.empty())
         filterNames[iter->first] = filterName;
   }
}

void CFilterManager::GetListOfFilterSetNames(map<string,string> &filterSetNames)
{
   map<string,SFilterSet>::iterator start = FilterSetList.begin();
   map<string,SFilterSet>::iterator end = FilterSetList.end();
   map<string,SFilterSet>::iterator iter;

   filterSetNames.clear();
   for (iter = start; iter != end; ++iter)
   {
      string filterSetName((iter->second).name);

      if (!filterSetName.empty())
         filterSetNames[iter->first] = filterSetName;
   }
}

namespace {

   char mytolower(char in)
   {
      if (in <= 'Z' && in >= 'A')
         return in - ('Z' - 'z');

      return in;
   }

   bool filterNameIdPairLessThan(const std::pair<string,string> &lhs,
                                 const std::pair<string,string> &rhs)
   {
      string leftName = lhs.first;
      string rightName = rhs.first;

      // lower-case the string
      std::transform(leftName.begin(), leftName.end(), leftName.begin(),
                     mytolower);
      std::transform(rightName.begin(), rightName.end(), rightName.begin(),
                     mytolower);
      return leftName < rightName;
   }
};

void CFilterManager::GetSortedListOfFilterNames(
                     vector<std::pair<string,string> > &sortedList)
{
   map<string,SFilter>::iterator start = FilterList.begin();
   map<string,SFilter>::iterator end = FilterList.end();
   map<string,SFilter>::iterator iter;

   sortedList.clear();
   for (iter = start; iter != end; ++iter)
   {
      string filterID = iter->first;
      string filterName = *((iter->second).name);

      // HACK exclude the "default" and PDL filters!
      if (!filterName.empty() &&
           (filterID != AF_DEFAULT_FILTER_ID) &&
           (filterID != SHINE_DEFAULT_FILTER_ID) &&
           (filterID != PDL_0_FILTER_ID) &&
           (filterID != PDL_1_FILTER_ID) &&
           (filterID != PDL_2_FILTER_ID) &&
           (filterID != PDL_3_FILTER_ID) &&
           (filterID != PDL_4_FILTER_ID) &&
           (filterID != PDL_5_FILTER_ID))
      {
         sortedList.push_back(std::pair<string,string>(filterName, filterID));
      }
   }
   sort (sortedList.begin(), sortedList.end(), filterNameIdPairLessThan);
}

void CFilterManager::GetSortedListOfFilterSetNames(
                    vector<std::pair<string,string> > &sortedList)
{
   std::map<string,SFilterSet>::iterator start = FilterSetList.begin();
   std::map<string,SFilterSet>::iterator end = FilterSetList.end();
   std::map<string,SFilterSet>::iterator iter;

   sortedList.clear();
   for (iter = start; iter != end; ++iter)
   {
      string filterSetID = iter->first;
      string filterSetName = (iter->second).name;

      // Some sets are internal only and never visible to the user
      if (!filterSetName.empty()
      && (filterSetID != AF_DEFAULT_FILTER_SET_ID)
      && (filterSetID != SHINE_DEFAULT_FILTER_SET_ID)
      && (filterSetID != PDL_FILTER_SET_ID))
      {
         sortedList.push_back(std::pair<string,string>(filterSetName, filterSetID));
      }
   }
   sort (sortedList.begin(), sortedList.end(), filterNameIdPairLessThan);
}

bool CFilterManager::IsFilterLoaded(const string &filterID)
{
   bool retVal = false;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      retVal = (INVALID_SLOT_NUMBER != activeFilterSet.FindSlot(filterID));
   }

   return retVal;
}

bool CFilterManager::IsFilterUsed(const string &filterID)
{
   bool retVal = false;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      int slotNumber = activeFilterSet.FindSlot(filterID);
      if (slotNumber != INVALID_SLOT_NUMBER)
         retVal = activeFilterSet.filterSlots[slotNumber].used;
   }

   return retVal;
}

void CFilterManager::ToggleFilterUsed(const string &filterID)
{
   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      activeFilterSet.ToggleUsed(filterID);
   }
}

int CFilterManager::GetFilterSlot(const string &filterID)
{
   int retVal = INVALID_SLOT_NUMBER;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      retVal = activeFilterSet.FindSlot(filterID);
   }

   return retVal;
}

string CFilterManager::GetNextLoadedFilterID(const string &filterID,
                                             int direction)
{
   string retVal = INVALID_ID;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      retVal = activeFilterSet.GetNextLoadedFilterID(filterID, direction);
   }

   return retVal;
}

int CFilterManager::GetCountOfLoadedFilters()
{
   int retVal = 0;

   for (unsigned int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
   {
      string filterID = GetFilterIDFromSlotNumber(slot);
      if (filterID == INVALID_ID)
      {
         break;  // the rest of the slots are unloaded
      }
      ++retVal;
   }

   return retVal;
}

string CFilterManager::GetFilterIDFromSlotNumber(int slot)
{
   string retVal = INVALID_ID;

   if (ActiveFilterSetID != INVALID_ID)
   {
      SFilterSet &activeFilterSet = FilterSetList[ActiveFilterSetID];
      retVal = activeFilterSet.GetFilterIDFromSlotNumber(slot);
   }

   return retVal;
}

void CFilterManager::ActivateFilterSet(const string &setID)
{
   if (FilterSetList.find(setID) != FilterSetList.end())
      ActiveFilterSetID = setID;
}


void CFilterManager::CreateBuiltInFilters(void)
{
   if (alphaIsOK)
   {
      CreateABuiltInFilter(AF_DEFAULT_FILTER_ID, AF_DEFAULT_FILTER_NAME,
                           DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
      CreateABuiltInFilter(AF_WHITE_FILTER_ID, AF_WHITE_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, true);  // bright
      CreateABuiltInFilter(AF_BLACK_FILTER_ID, AF_BLACK_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, false); // dark
      CreateABuiltInFilter(AF_RED_LIGHT_FILTER_ID,   AF_RED_LIGHT_FILTER_NAME,
                           RED_FILTER_CHANNEL, true);    // bright
      CreateABuiltInFilter(AF_GREEN_LIGHT_FILTER_ID, AF_GREEN_LIGHT_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, true);  // bright
      CreateABuiltInFilter(AF_BLUE_LIGHT_FILTER_ID,  AF_BLUE_LIGHT_FILTER_NAME,
                           BLUE_FILTER_CHANNEL, true);   // bright
      CreateABuiltInFilter(AF_RED_DARK_FILTER_ID,   AF_RED_DARK_FILTER_NAME,
                           RED_FILTER_CHANNEL, false);   // dark
      CreateABuiltInFilter(AF_GREEN_DARK_FILTER_ID, AF_GREEN_DARK_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, false); // dark
      CreateABuiltInFilter(AF_BLUE_DARK_FILTER_ID,  AF_BLUE_DARK_FILTER_NAME,
                           BLUE_FILTER_CHANNEL, false);  // dark
      CreateABuiltInFilter(AF_ALPHA_FILTER_ID, AF_ALPHA_FILTER_NAME,
                              ALPHA_FILTER_CHANNEL, false);// dark
   }
   else
   {
      CreateABuiltInFilter(SHINE_DEFAULT_FILTER_ID, SHINE_DEFAULT_FILTER_NAME,
                           DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
      CreateABuiltInFilter(SHINE_WHITE_FILTER_ID, SHINE_WHITE_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, true);  // bright
      CreateABuiltInFilter(SHINE_BLACK_FILTER_ID, SHINE_BLACK_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, false); // dark
      CreateABuiltInFilter(SHINE_RED_LIGHT_FILTER_ID,   SHINE_RED_LIGHT_FILTER_NAME,
                           RED_FILTER_CHANNEL, true);    // bright
      CreateABuiltInFilter(SHINE_GREEN_LIGHT_FILTER_ID, SHINE_GREEN_LIGHT_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, true);  // bright
      CreateABuiltInFilter(SHINE_BLUE_LIGHT_FILTER_ID,  SHINE_BLUE_LIGHT_FILTER_NAME,
                           BLUE_FILTER_CHANNEL, true);   // bright
      CreateABuiltInFilter(SHINE_RED_DARK_FILTER_ID,   SHINE_RED_DARK_FILTER_NAME,
                           RED_FILTER_CHANNEL, false);   // dark
      CreateABuiltInFilter(SHINE_GREEN_DARK_FILTER_ID, SHINE_GREEN_DARK_FILTER_NAME,
                           GREEN_FILTER_CHANNEL, false); // dark
      CreateABuiltInFilter(SHINE_BLUE_DARK_FILTER_ID,  SHINE_BLUE_DARK_FILTER_NAME,
                           BLUE_FILTER_CHANNEL, false);  // dark
   }

   CreateABuiltInFilter(PDL_0_FILTER_ID,  PDL_0_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
   CreateABuiltInFilter(PDL_1_FILTER_ID,  PDL_1_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
   CreateABuiltInFilter(PDL_2_FILTER_ID,  PDL_2_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
   CreateABuiltInFilter(PDL_3_FILTER_ID,  PDL_3_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
   CreateABuiltInFilter(PDL_4_FILTER_ID,  PDL_4_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
   CreateABuiltInFilter(PDL_5_FILTER_ID,  PDL_5_FILTER_NAME,
                        DETECT_CHANNEL_DEFAULT, BRIGHT_CONTRAST_DEFAULT);
}

void CFilterManager::CreateBuiltInFilterSets(void)
{
   vector<string> filterIDs;

   if (alphaIsOK)
   {
      filterIDs.push_back(AF_WHITE_FILTER_ID);
      filterIDs.push_back(AF_BLACK_FILTER_ID);
      CreateABuiltInFilterSet(AF_DEFAULT_FILTER_SET_ID,
                              AF_DEFAULT_FILTER_SET_NAME,
                              filterIDs);
      CreateABuiltInFilterSet(AF_NORMAL_FILTER_SET_ID,
                              AF_NORMAL_FILTER_SET_NAME,
                              filterIDs);

      filterIDs.clear();
      filterIDs.push_back(AF_RED_LIGHT_FILTER_ID);
      filterIDs.push_back(AF_GREEN_LIGHT_FILTER_ID);
      filterIDs.push_back(AF_BLUE_LIGHT_FILTER_ID);
      filterIDs.push_back(AF_RED_DARK_FILTER_ID);
      filterIDs.push_back(AF_GREEN_DARK_FILTER_ID);
      filterIDs.push_back(AF_BLUE_DARK_FILTER_ID);
      CreateABuiltInFilterSet(AF_THREE_LAYER_FILTER_SET_ID,
                              AF_THREE_LAYER_FILTER_SET_NAME,
                              filterIDs);

      filterIDs.clear();
      filterIDs.push_back(AF_ALPHA_FILTER_ID);
      CreateABuiltInFilterSet(AF_ALPHA_FILTER_SET_ID,
                             AF_ALPHA_FILTER_SET_NAME,
                             filterIDs);
   }
   else
   {
      filterIDs.push_back(SHINE_WHITE_FILTER_ID);
      filterIDs.push_back(SHINE_BLACK_FILTER_ID);
      CreateABuiltInFilterSet(SHINE_DEFAULT_FILTER_SET_ID,
                              SHINE_DEFAULT_FILTER_SET_NAME,
                              filterIDs);
      CreateABuiltInFilterSet(SHINE_NORMAL_FILTER_SET_ID,
                              SHINE_NORMAL_FILTER_SET_NAME,
                              filterIDs);

      filterIDs.clear();
      filterIDs.push_back(SHINE_RED_LIGHT_FILTER_ID);
      filterIDs.push_back(SHINE_GREEN_LIGHT_FILTER_ID);
      filterIDs.push_back(SHINE_BLUE_LIGHT_FILTER_ID);
      filterIDs.push_back(SHINE_RED_DARK_FILTER_ID);
      filterIDs.push_back(SHINE_GREEN_DARK_FILTER_ID);
      filterIDs.push_back(SHINE_BLUE_DARK_FILTER_ID);
      CreateABuiltInFilterSet(SHINE_THREE_LAYER_FILTER_SET_ID,
                              SHINE_THREE_LAYER_FILTER_SET_NAME,
                              filterIDs);
   }

   filterIDs.clear();
   filterIDs.push_back(PDL_0_FILTER_ID);
   filterIDs.push_back(PDL_1_FILTER_ID);
   filterIDs.push_back(PDL_2_FILTER_ID);
   filterIDs.push_back(PDL_3_FILTER_ID);
   filterIDs.push_back(PDL_4_FILTER_ID);
   filterIDs.push_back(PDL_5_FILTER_ID);
   CreateABuiltInFilterSet(PDL_FILTER_SET_ID,
                           PDL_FILTER_SET_NAME,
                           filterIDs);

}

void CFilterManager::CreateABuiltInFilter(const string &filterID,
                          const string &name,
                          int detectChannel,
                          bool bright)
{
   // operator[] creates a default entry in the map
   FilterList[filterID].SetName(name);
   FilterList[filterID].detectChannel = detectChannel;
	FilterList[filterID].brightOnDarkContrast = bright;
   DefaultsList[filterID] = FilterList[filterID];
}

void CFilterManager::CreateABuiltInFilterSet(const string &setID,
                             const string &name,
                             const vector<string> filterIDs)
{
   // operator[] automagically creates a default entry in the map
   FilterSetList[setID].name = name;
   const bool useFlag = true;
   for (unsigned int i = 0; i < filterIDs.size(); ++i)
      FilterSetList[setID].LoadFilter(filterIDs[i], useFlag);
}

string CFilterManager::CreateAnEmptyFilterSet(const string &newName)
{
   string filterSetID = guid_to_string(guid_create());

   // operator[] automagically creates a new entry in the map
   FilterSetList[filterSetID].name = newName;
   FilterSetList[filterSetID].UnloadAllFilters();  // in case it existed

   return filterSetID;
}

string CFilterManager::CopyActiveFilterSetByName(const string &newName)
{
   string filterSetID = CreateAnEmptyFilterSet(newName);

   CopyActiveFilterSetByID(filterSetID);

   return filterSetID;
}

void CFilterManager::CopyActiveFilterSetByID(const string &filterSetID)
{
   if (ActiveFilterSetID != INVALID_ID)
   {
      FilterSetList[filterSetID].UnloadAllFilters();

      for (unsigned int slot = 0; slot < NUMBER_OF_FILTERS_PER_SET; ++slot)
      {
         string filterID = GetFilterIDFromSlotNumber(slot);
         if (filterID == INVALID_ID)
         {
            break;  // the rest of the slots are unloaded
         }

#if _DONT_SHARE_FILTERS_BETWEEN_SETS_
         SFilter *rhs = GetFilter(filterID);
         if (rhs != NULL)
         {
            SFilter lhs(*rhs);
            *(lhs.name) = UniquifyFilterName(*(rhs->name));
            string newFilterID = AddFilter(lhs);

            if (newFilterID != INVALID_ID)
            {
               FilterSetList[filterSetID].LoadFilter(newFilterID);
            }
         }
#else // share filters
         const bool useFlag = true;
         FilterSetList[filterSetID].LoadFilter(filterID, useFlag);
#endif
      }
   }
}

void CFilterManager::ClearFilterSet(const string &filterSetID)
{
   // Don't create it if it doesn't exist because we don't have a name for it
   map<string,SFilterSet>::iterator iter = FilterSetList.find(filterSetID);
   if (iter != FilterSetList.end())
      FilterSetList[filterSetID].UnloadAllFilters();
}

void CFilterManager::DeleteFilterSet(const string &filterSetID)
{
   // Not allowed to delete the working set (aka the "default" set)
   if (filterSetID == AF_DEFAULT_FILTER_SET_ID)
      return;

   if (filterSetID == SHINE_DEFAULT_FILTER_SET_ID)
      return;

   if (filterSetID == ActiveFilterSetID)
      ActiveFilterSetID = INVALID_ID;

   map<string,SFilterSet>::iterator iter = FilterSetList.find(filterSetID);
   if (iter != FilterSetList.end())
      FilterSetList.erase(iter);
}

string CFilterManager::GetActiveFilterSetID()
{
   return ActiveFilterSetID;
}

string CFilterManager::UniquifyFilterName(const string &filterName)
{
   string retVal = filterName;

   // If we find a previous filter with the same name, tack on "(N)"
   // where N is the smallest integer that makes the name unique
   if (!retVal.empty())
   {
      if (INVALID_ID != FindFilterByName(retVal))
      {
         for (int ordinal = 2; ordinal < 1000; ++ ordinal)
         {
            MTIostringstream os;
            os << retVal << " (" << ordinal << ")";
            string tryName = os.str();
            if (INVALID_ID == FindFilterByName(tryName))
            {
               retVal = tryName;
               break;
            }
         }
      }
   }

   return retVal;
}

string CFilterManager::UniquifyFilterSetName(const string &filterSetName)
{
   string retVal = filterSetName;

   // If we find a previous filter set with the same name, tack on "(N)"
   // where N is the smallest integer that makes the name unique
   if (!retVal.empty())
   {
      if (INVALID_ID != FindFilterSetByName(retVal))
      {
         for (int ordinal = 2; ordinal < 1000; ++ ordinal)
         {
            MTIostringstream os;
            os << retVal << " (" << ordinal << ")";
            string tryName = os.str();
            if (INVALID_ID == FindFilterSetByName(tryName))
            {
               retVal = tryName;
               break;
            }
         }
      }
   }

   return retVal;
}

//---------------------------------------------------------------------------
