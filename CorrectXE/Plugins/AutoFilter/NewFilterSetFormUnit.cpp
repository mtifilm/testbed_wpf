//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "NewFilterSetFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNewFilterSetForm *NewFilterSetForm;
//---------------------------------------------------------------------------

__fastcall TNewFilterSetForm::TNewFilterSetForm(TComponent* Owner)
    : TForm(Owner), nameShadow("Saved Set")
{
}
//---------------------------------------------------------------------------

void TNewFilterSetForm::SetListOfFilterSetNames(
                       const vector<string> &newListOfNames)
{
   ComboBox->Clear();
   ComboBox->Items->Append("** Create new filter set **");
   for (unsigned i = 0; i < newListOfNames.size(); ++i)
   {
      ComboBox->Items->Append(newListOfNames[i].c_str());
   }
   ComboBox->ItemIndex = 0;
   EditLabel->Enabled = true;
   Edit->Enabled = EditLabel->Enabled;
   Edit->Text = nameShadow.c_str();
   Edit->Color = clWindow;
}
//---------------------------------------------------------------------------

int TNewFilterSetForm::GetSelectedIndex()
{
   // - 1 because we added a "Create new set" dummy entry first
   return ComboBox->ItemIndex - 1;
}
//---------------------------------------------------------------------------

void TNewFilterSetForm::SetDefaultName(const string &newName)
{
   Edit->Text = AnsiString(newName.c_str()).Trim();
   nameShadow = newName;
}
//---------------------------------------------------------------------------

string TNewFilterSetForm::GetNewSetName()
{
   // only valid if index returned from GetSelectedIndex() was -1
   return StringToStdString(Edit->Text.Trim());
}
//---------------------------------------------------------------------------

void __fastcall TNewFilterSetForm::EditEnter(TObject *Sender)
{
   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
}
//---------------------------------------------------------------------------
void __fastcall TNewFilterSetForm::ComboBoxChange(TObject *Sender)
{
   EditLabel->Enabled = (ComboBox->ItemIndex < 1);
   Edit->Enabled = EditLabel->Enabled;
   Edit->Text = (Edit->Enabled? nameShadow.c_str() : "");
   Edit->Color = (Edit->Enabled? clWindow : clBtnFace);
}
//---------------------------------------------------------------------------

