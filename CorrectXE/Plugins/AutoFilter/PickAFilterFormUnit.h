//---------------------------------------------------------------------------

#ifndef PickAFilterFormUnitH
#define PickAFilterFormUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <map>
#include <string>
#include <vector>
#include <utility>
using std::map;
using std::string;
using std::vector;
using std::pair;
//---------------------------------------------------------------------------

class TPickAFilterForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *PleaseSelectLabel;
    TButton *OkButton;
    TButton *CancelButton;
    TListBox *ListBox;
private:	// User declarations
    vector<string> FilterIDList;

public:		// User declarations
    __fastcall TPickAFilterForm(TComponent* Owner);

    void SetOpName(const string &newOpName);
    void SetFilterNameList(vector< pair<string,string> > &filterNames);
    int GetSelectedFilterIDs(vector<string> &filterIDs);
};
//---------------------------------------------------------------------------
extern PACKAGE TPickAFilterForm *PickAFilterForm;
//---------------------------------------------------------------------------
#endif
