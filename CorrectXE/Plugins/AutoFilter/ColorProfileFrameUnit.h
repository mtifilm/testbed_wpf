//---------------------------------------------------------------------------


#ifndef ColorProfileFrameUnitH
#define ColorProfileFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TrackEditFrameUnit.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class TColorProfileFrame : public TFrame
{
__published:	// IDE-managed Components
    TLabel *RLabel;
    TLabel *GLabel;
    TLabel *BLabel;
    TLabel *MinLabel;
    TTrackEditFrame *RMinTrackEdit;
    TTrackEditFrame *RMaxTrackEdit;
    TTrackEditFrame *GMaxTrackEdit;
    TTrackEditFrame *GMinTrackEdit;
    TTrackEditFrame *BMinTrackEdit;
    TTrackEditFrame *BMaxTrackEdit;
    TPanel *DisabledMinBackPanel;
    TPanel *DiabledMinFillerPanel;
    TPanel *DisabledMedBackPanel;
    TPanel *DiabledMedFillerPanel;
    TPanel *DisabledMaxBackPanel;
    TPanel *DiabledMaxFillerPanel;
    TPanel *RMedLabelPanel;
    TPanel *GMedLabelPanel;
    TPanel *BMedLabelPanel;
    TLabel *RMedLabel;
    TLabel *GMedLabel;
    TLabel *BMedLabel;
    TPanel *EyedropperPanel;
    TSpeedButton *EyedropperButton;
    TPanel *MinLabelPanel;
    TPanel *MaxLabelPanel;
    TLabel *MaxLabel;
    TCheckBox *ChangeNotifyWidget;
    TPanel *MinColorPatchPanel;
    TPanel *MedColorPatchPanel;
    TPanel *MaxColorPatchPanel;
    void __fastcall EyedropperButtonClick(TObject *Sender);
private:	// User declarations
    bool EnabledFlag;
    bool CapturedMouse;
    int FinalRValue;
    int FinalGValue;
    int FinalBValue;

    void UpdateColorPatches();

    int StashedMinR, StashedMedR, StashedMaxR;
    int StashedMinG, StashedMedG, StashedMaxG;
    int StashedMinB, StashedMedB, StashedMaxB;
    void StashValues();
    void UnstashValues();

    DEFINE_CBHOOK(ColorValueChanged, TColorProfileFrame);

public:		// User declarations
    __fastcall TColorProfileFrame(TComponent* Owner);

    void Enable(bool flag);

    bool IsInEyedropperMode();
    void CancelEyedropperMode();

    void GetValues(int &minR, int &medR, int &maxR,
                   int &minG, int &medG, int &maxG,
                   int &minB, int &medB, int &maxB);

    void SetValues(int minR, int medR, int maxR,
                   int minG, int redG, int maxG,
                   int minB, int medB, int maxB);

    bool IsFullRange();
    void SetFullRange();

    CBHook ColorProfileChange;
    CBHook EyedropperStateChange;
};
//---------------------------------------------------------------------------
//extern PACKAGE TColorProfileFrame *ColorProfileFrame;
//---------------------------------------------------------------------------
#endif
