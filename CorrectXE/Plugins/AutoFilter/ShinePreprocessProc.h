//---------------------------------------------------------------------------

#ifndef ShinePreprocessProcH
#define ShinePreprocessProcH
//---------------------------------------------------------------------------

#include "ShineToolParameters.h"

#include "ImageFormat3.h"
#include "IniFile.h"
#include "IppArray.h"
#include "ToolObject.h"

class AutoTrackingPreprocessor;
//---------------------------------------------------------------------------

class ShinePreprocessProc : public CToolProcessor
{
public:
	ShinePreprocessProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
			const bool *newEmergencyStopFlagPtr,
			IToolProgressMonitor *newToolProgressMonitor);

	virtual ~ShinePreprocessProc();

	int SetIOConfig(CToolIOConfig *toolIOConfig);
	int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
	// Tool's functions called from within context of the processing loop.
	int SetParameters(CToolParameters *toolParams);
	int BeginProcessing(SToolProcessingData &procData);
	int EndProcessing(SToolProcessingData &procData);
	int BeginIteration(SToolProcessingData &procData);
	int EndIteration(SToolProcessingData &procData);
	int DoProcess(SToolProcessingData &procData);
	int GetIterationCount(SToolProcessingData &procData);

private:
	ShineToolSettings _shineToolSettings;
	AutoTrackingPreprocessor  *_autoTrackingPreprocessor = nullptr;

	int _inFrameIndex = -1;        // Should we save the whole tool parameter object?
	int _outFrameIndex = -1;
	IppiSize _imageSize = {0,0};
	MTI_UINT16 _maxPixel;

	int _iterationCount = 0;
	int _framesRemaining = 0;         // Number of remaining output frames to be processed
	int _framesAlreadyProcessed = 0; // Number of output frames already processed
	int _framesToProcess = 0;       // Number of frames to process on current iteration

   int _frameToJumpToWhenDone = -1;
};

#endif
