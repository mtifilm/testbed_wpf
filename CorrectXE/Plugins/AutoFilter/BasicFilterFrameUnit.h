//---------------------------------------------------------------------------


#ifndef BasicFilterFrameUnitH
#define BasicFilterFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "PropX.h"
#include "ColorPanel.h"
#include <string>
using std::string;
//---------------------------------------------------------------------------

enum EFilterFrameState
{
   FFS_UNSELECTED,
   FFS_SELECTED,
   FFS_GANGED
};
//---------------------------------------------------------------------------

class TBasicFilterFrame : public TFrame
{
__published:	// IDE-managed Components
	TPanel *AF_ValuesPanel;
	TCheckBox *UseCheckBox;
	TLabel *FilterNameLabel;
	TLabel *AF_MaxMinDustValues;
	TLabel *AF_ContrastOrGrainValue;
	TLabel *AF_FixToleranceValue;
	TLabel *AF_EdgeBlurValue;
	TLabel *AF_MotionValue;
    TPanel *DisabledPanel;
    TLabel *NoFilterLoadedLabel;
	TColorPanel *BackPanel;
    TPanel *DisabledCheckboxPanel;
    TCheckBox *FilterClickedNotifyWidget;
	TPanel *SHINE_ValuesPanel;
	TLabel *SHINE_MaxMinValues;
	TLabel *SHINE_ContrastValue;
	TLabel *SHINE_AggressionValue;
	TPanel *UseAndFilterNamePanel;
    void __fastcall UseCheckBoxClick(TObject *Sender);
	void __fastcall FilterClick(TObject *Sender);

private:	// User declarations
   EFilterFrameState state;
   bool CheckedStateMayBeWrong;
   bool UseCheckedState;

public:		// User declarations
    __fastcall TBasicFilterFrame(TComponent* Owner);

    void Enable(bool flag);
    void SetState(EFilterFrameState newState);
    EFilterFrameState GetState();
    void Use(bool flag);
    void SetFilterName(const string &newName);
	 void AF_SetValues(int newMaxSize, int newMinSize, int newDustSize, int newContrast,
						 int newConfid, int newEdgeBlur, int newH, int newV);
	 void SHINE_SetValues(int newMaxSize, int newMinSize, int newContrast, int newAggression);
	 bool IsEnabled();
	 bool IsSelected();
    bool IsGanged();
    bool IsUsed();

    CBHook FilterClicked;
    CBHook FilterShiftClicked;
    CBHook FilterCtrlClicked;
    CBHook UseCheckBoxChange;
};
//---------------------------------------------------------------------------
extern PACKAGE TBasicFilterFrame *BasicFilterFrame;
//---------------------------------------------------------------------------
#endif
