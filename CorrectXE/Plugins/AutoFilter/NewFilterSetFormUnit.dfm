object NewFilterSetForm: TNewFilterSetForm
  Left = 1657
  Top = 336
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Save Filter Set'
  ClientHeight = 205
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    256
    205)
  PixelsPerInch = 96
  TextHeight = 13
  object EditLabel: TLabel
    Left = 12
    Top = 92
    Width = 237
    Height = 14
    Caption = 'Or enter a name to create a new filter set:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ComboBoxLabel: TLabel
    Left = 12
    Top = 28
    Width = 205
    Height = 14
    Caption = 'Please select a filter set to overwrite:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit: TEdit
    Left = 12
    Top = 116
    Width = 236
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Saved Set'
    OnEnter = EditEnter
  end
  object OkButton: TButton
    Left = 49
    Top = 166
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 142
    Top = 166
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object ComboBox: TComboBox
    Left = 12
    Top = 52
    Width = 236
    Height = 21
    Style = csDropDownList
    TabOrder = 3
    OnChange = ComboBoxChange
  end
end
