//---------------------------------------------------------------------------

#ifndef AutoFilterMultiProcH
#define AutoFilterMultiProcH
//---------------------------------------------------------------------------

#include "AlgorithmAF_MT.h"
#include "IniFile.h"
#include "RegionOfInterest.h"
#include "ToolObject.h"
//////////////////////////////////////////////////////////////////////

class CAutoFilterMultiProc : public CToolProcessor
{
public:
   CAutoFilterMultiProc(int newToolNumber,
                        const string &newToolName,
								CToolSystemInterface *newSystemAPI,
                        const bool *newEmergencyStopFlagPtr,
								IToolProgressMonitor *newToolProgressMonitor,
                        bool doPastOnlyHack);

	virtual ~CAutoFilterMultiProc();

   CAlgorithmAF_MT  *AlgorithmAF;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
	int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
	int GetIterationCount(SToolProcessingData &procData);

private:
	CToolFrameRange::SFrameRange inputFrameRange;
	int inFrameIndex;        // Should we save the whole tool parameter object?
	int outFrameIndex;
   int iterationCount;

	int processingThreadCount;
   int maxInputFrames;      // Maximum number of input frames per iteration
	int maxOutputFrames;     // Maximum number of output frames per iteration
	int framesRemaining;     // Number of remaining output frames to be processed
   int framesAlreadyProcessed; // Number of output frames already processed
	int framesToProcess;     // Number of frames to process on current iteration

   long laaFrameIndex[AF_MAX_THREAD_COUNT][AF_NUM_SRC_FRAMES];

	bool doPastOnlyHackFlag;

	// Mask Region
   bool quickMaskMode;
	CRegionOfInterest maskROI[AF_MAX_THREAD_COUNT];
};
//---------------------------------------------------------------------------

#endif
