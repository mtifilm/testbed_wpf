///////////////////////////////////////////////////////////////////////
// AutoFilter.cpp: implementation of the CAutoFilter class.
///////////////////////////////////////////////////////////////////////

#include "AutoFilterTool.h"
#include "AutoFilterSingleProc.h"
#include "AutoFilterMultiProc.h"
#include "AutofilterGUIUnit.h"
#include "ShinePreprocessProc.h"
#include "ShineProc.h"

#include "ClipAPI.h"
#include "err_auto_filter.h"
#include "FileSweeper.h"
#include "FilterStatsForm.h"
#include "HRTimer.h"
#include "ImageFormat3.h"
#include "JobManager.h"
#include "LineEngine.h"
#include "MTIDialogs.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "PDL.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "SynchronousThreadRunner.h"
#include "SysInfo.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"

#include <exception>
using std::tie;


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 AutoFilterNumber = AUTOFILTER_TOOL_NUMBER;

// -------------------------------------------------------------------------
// AutoFilter Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem AutoFilterDefaultConfigItems[] =
{
   // AutoFilter Tool Command             Modifiers      Action
   // + Key

 { AF_CMD_FOCUS_ON_MAX_SIZE,                 " 1                KeyDown    " },
 { AF_CMD_FOCUS_ON_MIN_SIZE,                 " 2                KeyDown    " },
 { AF_CMD_FOCUS_ON_MIN_CONTRAST,             " 3                KeyDown    " },
 { AF_CMD_FOCUS_ON_GRAIN_FILTER_PARAM,       " 4                KeyDown    " },
 { AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE,       " 5                KeyDown    " },
 { AF_CMD_FOCUS_ON_MOTION,                   " 6                KeyDown    " },
 { AF_CMD_FOCUS_ON_CORRECTION_EDGE_BLUR,     " 7                KeyDown    " },
 { AF_CMD_FOCUS_ON_DUST_SIZE,                " 8                KeyDown    " },

 { AF_CMD_TOGGLE_GRAIN,                      " 9                KeyDown    " },
 { AF_CMD_RESET_GRAIN,                       " Shift+9          KeyDown    " },
 { AF_CMD_SELECT_PREV_GRAIN_SETTING,         " Up               KeyDown    " },
 { AF_CMD_SELECT_NEXT_GRAIN_SETTING,         " Down             KeyDown    " },

 { AF_CMD_TOGGLE_USE_SELECTED_FILTER,        " 0                KeyDown    " },
 { AF_CMD_NEW_FILTER,                        " =                KeyDown    " },
 { AF_CMD_LOAD_FILTER,                       " Ctrl+L           KeyDown    " },
 { AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS,  " Ctrl+S           KeyDown    " },
 {AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS,  " Shift+C          KeyDown   "},

 {AF_CMD_REJECT_FIXES,                       " A                KeyDown    "},

 { EXEC_BUTTON_PREVIEW_FRAME,                " D                KeyDown    " },
 { EXEC_BUTTON_PREVIEW_ALL,                  " Shift+D          KeyDown    " },
 { EXEC_BUTTON_RENDER_FRAME,                 " G                KeyDown    " },
 { EXEC_BUTTON_RENDER_ALL,                   " Shift+G          KeyDown    " },
 { EXEC_BUTTON_STOP,                         " Ctrl+Space       KeyDown    " },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              " Space            KeyDown    " },
 { EXEC_BUTTON_SET_RESUME_TC,                " Return           KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL,                  " ,                KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       " Shift+,          KeyDown    " },

 // Toggle Original and either Highlights or Fixes
 {AF_CMD_TOGGLE_PROVISIONAL_AND_ORIG,        " T                KeyDown    "},
 {AF_CMD_TOGGLE_HIGHLIGHTS_AND_FIXES,        " Shift+T          KeyDown    "},

 // Toggle red Detection Visualization overlay
 {AF_CMD_TOGGLE_RED_OVERLAY,                 " Ctrl+T           KeyDown    "},
 // { AF_CMD_TOGGLE_CONTINUOUS_PREVIEW_MODE,    " Alt+T            KeyDown    " },

 // Repeat Fix
 { AF_CMD_REPEAT_LAST_GOV_OR_ROI,            " `                KeyDown    " },
 { AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI,          " Shift+`          KeyDown    " },

 { AF_CMD_SHOW_FILTER_STATS,                 " Ctrl+Shift+/     KeyDown    " },

 {AF_CMD_ALT_ON,                             " Alt+Alt          KeyDown    "},
 {AF_CMD_ALT_OFF,                            " Alt              KeyUp      "},
};

static CUserInputConfiguration *AutoFilterDefaultUserInputConfiguration =
      new CUserInputConfiguration(sizeof(AutoFilterDefaultConfigItems) / sizeof(CUserInputConfiguration::SConfigItem),
      AutoFilterDefaultConfigItems);

// -------------------------------------------------------------------------
// AutoFilter Tool Command Table

static CToolCommandTable::STableEntry AutoFilterCommandTableEntries[] =
{
   // AutoFilter Tool Command            AutoFilter Tool Command String
   // -------------------------------------------------------------
 { AF_CMD_NOOP,                               "AF_CMD_NOOP" },
 { AF_CMD_TOGGLE_HIGHLIGHTS_AND_FIXES,        "AF_CMD_TOGGLE_HIGHLIGHTS_AND_FIXES" },
 { AF_CMD_TOGGLE_PROVISIONAL_AND_ORIG,        "AF_CMD_TOGGLE_PROVISIONAL_AND_ORIG" },
 { AF_CMD_FOCUS_ON_MAX_SIZE,                  "AF_CMD_FOCUS_ON_MAX_SIZE" },
 { AF_CMD_FOCUS_ON_DUST_SIZE,                 "AF_CMD_FOCUS_ON_DUST_SIZE" },
 { AF_CMD_FOCUS_ON_MIN_CONTRAST,              "AF_CMD_FOCUS_ON_MIN_CONTRAST" },
   {AF_CMD_FOCUS_ON_GRAIN_FILTER_PARAM, "AF_CMD_FOCUS_ON_GRAIN_FILTER_PARAM"},
   {AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE, "AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE"},
   {AF_CMD_FOCUS_ON_MOTION, "AF_CMD_FOCUS_ON_MOTION"},
   {AF_CMD_FOCUS_ON_CORRECTION_EDGE_BLUR, "AF_CMD_FOCUS_ON_CORRECTION_EDGE_BLUR"},
 { AF_CMD_TOGGLE_DEBRIS_CONTRAST,             "AF_CMD_TOGGLE_DEBRIS_CONTRAST" },
 { AF_CMD_CYCLE_DETECT_CHANNEL,               "AF_CMD_CYCLE_DETECT_CHANNEL" },
   {AF_CMD_FOCUS_ON_PROCESS_CHANNELS, "AF_CMD_FOCUS_ON_PROCESS_CHANNELS"},
   {AF_CMD_TOGGLE_COLOR_PROFILE_RANGE, "AF_CMD_TOGGLE_COLOR_PROFILE_RANGE"},
   {AF_CMD_FOCUS_ON_COLOR_PROFILE_SLIDERS, "AF_CMD_FOCUS_ON_COLOR_PROFILE_SLIDERS"},
 { AF_CMD_SELECT_PREVIOUS_FILTER,             "AF_CMD_SELECT_PREVIOUS_FILTER" },
 { AF_CMD_SELECT_NEXT_FILTER,                 "AF_CMD_SELECT_NEXT_FILTER" },
 { AF_CMD_TOGGLE_USE_SELECTED_FILTER,         "AF_CMD_TOGGLE_USE_SELECTED_FILTER" },
 { AF_CMD_NEW_FILTER,                         "AF_CMD_NEW_FILTER" },
 { AF_CMD_LOAD_FILTER,                        "AF_CMD_LOAD_FILTER" },
 { AF_CMD_UNLOAD_FILTER,                      "AF_CMD_UNLOAD_FILTER" },
 { AF_CMD_DELETE_FILTER,                      "AF_CMD_DELETE_FILTER" },
 { AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS,   "AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS" },
   {AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS, "AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS"},
 { AF_CMD_CREATE_FILTER_SET,                  "AF_CMD_CREATE_FILTER_SET" },
 { AF_CMD_UPDATE_FILTER_SET,                  "AF_CMD_UPDATE_FILTER_SET" },
 { AF_CMD_LOAD_FILTER_SET,                    "AF_CMD_LOAD_FILTER_SET" },
 { AF_CMD_DELETE_FILTER_SET,                  "AF_CMD_DELETE_FILTER_SET" },
 { AF_CMD_REJECT_FIXES,                       "AF_CMD_REJECT_FIXES" },
 { AF_CMD_REPEAT_LAST_GOV_OR_ROI,             "AF_CMD_REPEAT_LAST_GOV_OR_ROI" },
 { AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI,           "AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI" },
 { AF_CMD_TOGGLE_GRAIN,                       "AF_CMD_TOGGLE_GRAIN" },
 { AF_CMD_RESET_GRAIN,                        "AF_CMD_RESET_GRAIN" },
 { AF_CMD_SELECT_PREV_GRAIN_SETTING,          "AF_CMD_SELECT_PREV_GRAIN_SETTING" },
 { AF_CMD_SELECT_NEXT_GRAIN_SETTING,          "AF_CMD_SELECT_NEXT_GRAIN_SETTING" },
 { AF_CMD_SHOW_FILTER_STATS,                  "AF_CMD_SHOW_FILTER_STATS" },
 { AF_CMD_TOGGLE_RED_OVERLAY,                 "AF_CMD_TOGGLE_RED_OVERLAY"},
 { AF_CMD_TOGGLE_CONTINUOUS_PREVIEW_MODE,     "AF_CMD_TOGGLE_CONTINUOUS_PREVIEW_MODE" },
 { AF_CMD_ALT_ON,                             "AF_CMD_ALT_ON"},
 { AF_CMD_ALT_OFF,                            "AF_CMD_ALT_OFF"},

};

static CToolCommandTable *AutoFilterCommandTable
   = new CToolCommandTable(sizeof(AutoFilterCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           AutoFilterCommandTableEntries);

CThreadLock CAutoFilter::_previewHackLock;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAutoFilter::CAutoFilter(const string &newToolName,
                         MTI_UINT32 **newFeatureTable)
: CToolObject(newToolName
, newFeatureTable)
, currentUserLassoPointer(0)
, currentUserBezierPointer(0)
, maskIsInROIMode(false)
, maskWasInROIMode(false)
, lastActionWasGOV(false)
, inhibitTildeKeyProcessing(true)
, usePastFramesOnlyFlag(false)
, oldMultiUsePastFramesOnlyFlag(false)
, oldSingleUsePastFramesOnlyFlag(false)
{
   currentUserRect.left = 0;
   currentUserRect.top = 0;
   currentUserRect.right = 0;
   currentUserRect.bottom = 0;

   // WTF is this?
   string junk;
}

CAutoFilter::~CAutoFilter()
{
   // Tell the detection map thread to end.
   _currentDetectionVisualizationState = false;
   CHRTimer timer;
   while (_detectionMapThreadIsRunning && timer.Read() < 5000)
   {
      MTImillisleep(10);
   }
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the AutoFilter Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CAutoFilter::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CAutoFilter's base class, i.e., CToolObject
   // This must be done before the CAutoFilter initializes itself
   retVal = initBaseToolObject(AutoFilterNumber, newSystemAPI, AutoFilterCommandTable, AutoFilterDefaultUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in AutoFilter Tool, initBaseToolObject failed " << retVal);
      return retVal;
   }

   // Tool-specific initialization here

   // Create the Tool's GUI
   CreateAutoFilterGUI();

   // Update the tool's GUI to match the tool processing initial state
   UpdateExecutionButtons(GetToolProcessingControlState(), false);

   // ROI mode replaces quick mask mode
   maskWasInROIMode = false;

   // What to do when we want to clear out the provisional frame
   MTIassert(!getSystemAPI()->IsProvisionalPending());
   provisionalAction = PA_ShouldNotBePending;

   return 0;

} // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CAutoFilter::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   // Make sure displayed frame is clean
   SetUserDesiredDetectionVisualizationState(false);
   getSystemAPI()->refreshFrameCached();

   // Enable mask tool ROI mode and auto-activate-ROI
   getSystemAPI()->SetMaskAllowed(true, true, maskWasInROIMode);
   getSystemAPI()->SetMaskToolAutoActivateRoiModeOnDraw(true);

   // Tell GOV to do tilde 'repeat fix or GOV' hack... should rename this mode!
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_DRS_MODE);

   // What to do with a pending provisional
   MTIassert(!getSystemAPI()->IsProvisionalPending());
   provisionalAction = PA_ShouldNotBePending;

   // Initialize some state
   _wasPlayerReallyPlayingWhenWeLastLooked = false;
   _wasRedOverlayOnWhenPlayerStarted = false;

   JobManager jobManager;
   jobManager.SetCurrentToolCode("af");

   getSystemAPI()->SetToolNameForUserGuide("AutoFilterTool");

   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
   ++SHINE_clipChangeCounter;  // to fix shine exec buttons

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CAutoFilter::toolDeactivate()
{
   // Get rid of any review regions that might be present on the screen
   resolveProvisional();

   DestroyAllToolSetups(true);

   GetToolProgressMonitor()->SetIdle();
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   // Turn off the tilde key 'repeat fix or GOV' hack
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_OTHER_MODE);
   inhibitTildeKeyProcessing = true;

   // Remember if mask is in ROI mode and then disable ROI mode
   maskWasInROIMode = getSystemAPI()->IsMaskInRoiMode();

   SetUserDesiredDetectionVisualizationState(false);
   if (_inPreviewHackMode)
   {
      // DBTRACE("LOCK_previewHackLock");
      CAutoThreadLocker lock(_previewHackLock);

      _inPreviewHackMode = false;
      getSystemAPI()->exitPreviewHackMode();
      // DBTRACE("UNLOCK_previewHackLock");
   }

   {
      CAutoThreadLocker lock(_previewHackLock);
      clearStashedStuff();
   }

   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolHideQuery
//
// Description: Called before switching to another tool
//
// Arguments:   None
//
// Returns:     true - always allow the switch
//
// ===================================================================
bool CAutoFilter::toolHideQuery()
{
   resolveProvisional();

   return true; // OK to hide us
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// AutoFilter Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CAutoFilter::toolShutdown()
{
   int retVal = 0;

   // Provisional cannot be pending because it was reso0lved in toolDeactivate
   // CAREFUL - I don't think you can call getSystemAPI anymore...
   MTIassert(provisionalAction == PA_ShouldNotBePending);

   // Destroy AutoFilter's GUI
   DestroyAutoFilterGUI();

   GAutoFilter = NULL;

   // Shutdown the AutoFilter Tool's base class, i.e., CToolObject.
   // This must be done after the AutoFilter shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
   {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
   }

   return retVal;
} // end toolShutdown
// ===================================================================

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// Description: Override ToolObject to make a frame procesor
//
// Arguments:   newEmergencyStopFlagPtr - I don't think that works!
//
// Returns:     return an instance of our ToolProcessor variant
//
// ===================================================================

CToolProcessor* CAutoFilter::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
	CToolProcessor *newToolProcessor = nullptr;

	switch (_whichAutoFilterTypeToMake)
	{
		case AutoFilterToolSetupType::OLDAF_SINGLE:
			newToolProcessor = new CAutoFilterSingleProc(
                                    GetToolNumber(),
                                    GetToolName(),
                                    getSystemAPI(),
                                    newEmergencyStopFlagPtr,
                                    GetToolProgressMonitor(),
					                     usePastFramesOnlyFlag,
                                    _afStats,
                                    _stashedGrainFilters,
                                    _modifiedPixelMask.data());

			oldSingleUsePastFramesOnlyFlag = usePastFramesOnlyFlag;
			break;

		case AutoFilterToolSetupType::OLDAF_MULTI:
			newToolProcessor = new CAutoFilterMultiProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr, GetToolProgressMonitor(),
					usePastFramesOnlyFlag);

			oldMultiUsePastFramesOnlyFlag = usePastFramesOnlyFlag;
			break;

		case AutoFilterToolSetupType::SHINE_PREPROCESS:
			newToolProcessor = new ShinePreprocessProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr, GetToolProgressMonitor());
			break;

		case AutoFilterToolSetupType::SHINE_PROCESS:
			newToolProcessor = new ShineProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr, GetToolProgressMonitor(), _modifiedPixelMask);
			break;

		default:
			MTIassert(false);
			break;
	}

	return newToolProcessor;
}
// ===================================================================

// ===================================================================
//
// Function:    onToolCommand
//
// Description: AutoFilter Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CAutoFilter::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   int commandNumber = toolCommand.getCommandNumber();
   int result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

   const CToolCommandTable *commandTable = getCommandTable();
   const char* commandName = commandTable->findCommandName(commandNumber);
   if (commandName != NULL)
   {
      TRACE_2(errout << "INFO: AF Command: " << commandName << " (" << commandNumber << ")");
   }
   else
   {
      TRACE_3(errout << "INFO: AF ignored command: " << commandNumber);
   }

   // Give the GUI first crack at it

   UserInputCommandConsumed = false;
   GotUserInputCommandNumber = commandNumber;
   GotUserInput.Notify();
   if (UserInputCommandConsumed)
   {
      // Don't resolve the provisional for GUI input

      return result;
   }

   // Dispatch loop for AutoFilter Tool commands

   switch (commandNumber)
   {
   case EXEC_BUTTON_PREVIEW_FRAME:
      if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
      {
         _MTIErrorDialog("The command is not available because the tool is in ROI mode and no region was drawn.");
         return false;
      }

      if (!RunExecButtonsCommand(EXEC_BUTTON_PREVIEW_FRAME))
      {
         result = TOOL_RETURN_CODE_NO_ACTION;
      }

      return TOOL_RETURN_CODE_EVENT_CONSUMED;

      break;

   case EXEC_BUTTON_RENDER_FRAME:
      // A bit tricky - determine how the provisional should be resolved
      if (getSystemAPI()->IsProvisionalPending())
      {
         acceptFix();

         GetToolProgressMonitor()->SetStatusMessage("Fixes accepted");
      }
      else if (!IsMaskInROIMode())
      {
         if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
         {
            result = TOOL_RETURN_CODE_NO_ACTION;
         }
      }
      else
      {
         // We don't have to check for a region drawn here because
         // we can't get here if there was one (G key accepted fixes)
         _MTIErrorDialog("The command is not available because the tool is in ROI mode and no region was drawn.");
         result = TOOL_RETURN_CODE_NO_ACTION;
      }
      break;

   case EXEC_BUTTON_PREVIEW_ALL:
   case EXEC_BUTTON_RENDER_ALL:

      if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
      {
         _MTIErrorDialog("The command is not available because the tool is in ROI mode and no region was drawn.");
      }

      if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
      {
         result = TOOL_RETURN_CODE_NO_ACTION;
      }

      break;

   case EXEC_BUTTON_STOP:
   case EXEC_BUTTON_PAUSE_OR_RESUME:
   case EXEC_BUTTON_SET_RESUME_TC:
   case EXEC_BUTTON_CAPTURE_PDL:
   case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      {
         if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
         {
            result = TOOL_RETURN_CODE_NO_ACTION;
         }

         break;
      }

   case AF_CMD_TOGGLE_HIGHLIGHTS_AND_FIXES: // T key
      ToggleProvisionalHighlightsAndFixes();
      break;

   case AF_CMD_TOGGLE_PROVISIONAL_AND_ORIG: // Shift+T
      getSystemAPI()->ToggleProvisional();
      break;

   case AF_CMD_TOGGLE_RED_OVERLAY:
      SetUserDesiredDetectionVisualizationState(!_userDesiredDetectVisualizationState);
      break;

   case AF_CMD_TOGGLE_CONTINUOUS_PREVIEW_MODE:
      SetUserDesiredContinuousPreviewState(!_userDesiredContinuousPreviewState);
      break;

   case AF_CMD_REJECT_FIXES:
      if (_currentContinuousPreviewState == true)
      {
         SetUserDesiredContinuousPreviewState(false);
      }

      // Do this before rejecting!
      ClearModifiedPixelMap();

      rejectFix();

      // CAREFUL: don't "consume" the 'A' - The Mask Tool needs it
      // to inactivate a mask region
      result = TOOL_RETURN_CODE_NO_ACTION;

      break;

   case AF_CMD_REPEAT_LAST_GOV_OR_ROI:
   case AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI:
      if (inhibitTildeKeyProcessing || !IsMaskInROIMode())
      {
         result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }
      if ((commandNumber == AF_CMD_REPEAT_LAST_GOV_OR_ROI && lastActionWasGOV == true) ||
            (commandNumber == AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI && lastActionWasGOV == false))
      {
         // Wrong phase, ignore this command
         result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

      resolveProvisional();

      getSystemAPI()->RestorePreviousROIShape();
      if (getSystemAPI()->IsMaskAvailable())
      {
         if (!RunExecButtonsCommand((EExecButtonId) EXEC_BUTTON_PREVIEW_FRAME))
         {
            result = TOOL_RETURN_CODE_NO_ACTION;
         }
      }
      break;

   case AF_CMD_SHOW_FILTER_STATS:
      if (FilterStatsForm == nullptr)
      {
         FilterStatsForm = new TFilterStatsForm(nullptr, _afStats);
      }

      FilterStatsForm->Show();
      break;

   case AF_CMD_ALT_OFF:
      setAltKeyState(false);
      break;

   case AF_CMD_ALT_ON:
      setAltKeyState(true);
      break;

   default:
      result = TOOL_RETURN_CODE_NO_ACTION;
      break;
   }

   return result;

} // onToolCommand
// ---------------------------------------------------------------------------

void CAutoFilter::preprocessSingleFramePreview()
{
   if (IsContinuousPreviewAllowedNow())
   {
      SetUserDesiredContinuousPreviewState(true);
   }

//   _isItOkToDrawRedStuffOnTheProvisional = true;

   ClearModifiedPixelMap();
}
// ---------------------------------------------------------------------------

int CAutoFilter::onChangingClip()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // This function is called just before the current clip is unloaded.

   resolveProvisional();

   // Copied this from DRS... don't know why it's here - maybe to get
   // GOV to a known state
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_OTHER_MODE);
   inhibitTildeKeyProcessing = true;

   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onNewClip()
{
   if (!(IsLicensed() && IsShowing())) // NOT IsDisabled(), might want to Enable!
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // Wait a minute... do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
   {
      getSystemAPI()->EnableTool();
   }
   else
   {
      getSystemAPI()->DisableTool(getToolDisabledReason());
   }

   // Make sure we're starting fresh
   getSystemAPI()->ClearProvisional(false);

   // If necessary, clear "Paused" state from GUI
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   // Copied this from DRS... enable or re-enable the tilde key
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_DRS_MODE);
   inhibitTildeKeyProcessing = true;

   ClearModifiedPixelMap();

   {
      CAutoThreadLocker lock(_previewHackLock);
      clearStashedStuff();
   }

//   // Clear old cached shine preprocessor output.
//   auto directoryChain = getSystemAPI()->getClipMetadataDirectoryChain("Analysis\\Motion");
//   if (directoryChain.size() == 0)
//   {
//       TRACE_0(errout << "ERROR: Could not compute path for motion analysis folder!");
//   }
//   else
//   {
//#ifdef _DEBUG
//      for (int i = 0; i < directoryChain.size(); ++i)
//      {
//         TRACE_1(errout << "directoryChain[" << i << "] = " << directoryChain[i]);
//      }
//#endif
//
//      // The chain is ordered from this level up. We will write all new entries
//      // at this level, so make sure the directory exists!
//      MakeDirectoryBackwards(directoryChain[0]);
//   }
//
//   SHINE_preprocOutputCache = std::make_unique<ShinePreprocOutputCache>(directoryChain, "ofd");
   ++SHINE_clipChangeCounter;

   // This allows the tool to notify the gui
   ClipChange.Notify();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onNewMarks()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // Don't resolve the provisional!

   // Notify the gui
   MarksChange.Notify();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onChangingFraming()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // In case a fix is pending
   resolveProvisional();

   // If necessary, clear "Paused" state from GUI
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   DestroyAllToolSetups(false);

   {
      CAutoThreadLocker lock(_previewHackLock);
      clearStashedStuff();
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onSwitchSubtool()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

// Replaced this with the toolActivate/Deactivate calls, below.
//   // In case a fix is pending
//   resolveProvisional();
//
//   // If necessary, clear "Paused" state from GUI
//   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
//
//   DestroyAllToolSetups(false);
   if (!getSystemAPI()->CheckToolProcessing())
   {
      toolDeactivate();
      ToggleOpMode();
      toolActivate();
   }

   {
      CAutoThreadLocker lock(_previewHackLock);
      clearStashedStuff();
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

void CAutoFilter::clearStashedStuff()
{
   // LOCK _previewHackLock BEFORE CALLING THIS;
   // Caller MUST set  _stashedPreviewHackFrameIndex to the current frame index!

   MTIfree(_stashedPreviewHackFrameBits);
   _stashedPreviewHackFrameBits = nullptr;
   _stashedPreviewHackFrameBitsSize = 0;
   _stashedPreviewHackFrameIndex = -1;

   delete _stashedBlobPixelArrayWrapper;
   _stashedBlobPixelArrayWrapper = nullptr;

   _stashedGrainFilters.clear();
   _stashedPreviewHackDetectionMap.clear();
}
// ---------------------------------------------------------------------------

int CAutoFilter::onRedraw(int frameIndex)
{
   CAutoThreadLocker lock(_previewHackLock);
   if (_stashedPreviewHackFrameIndex != frameIndex)
   {
      clearStashedStuff();
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}
// ---------------------------------------------------------------------------

namespace
{

const int NumberOfSlices = 32;

IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
{
   IppiRect sliceRoi;
   int nominalRowsPerSlice = height / NumberOfSlices;
   int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
   sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
   sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
   sliceRoi.x = 0;
   sliceRoi.width = width;

   return sliceRoi;
}

struct RedCrapParams
{
   Ipp8uArray detectionMap;
   MTI_UINT16 *frameBits;
   MTI_UINT8 *modifiedPixelMap;
   MTI_UINT16 maxVal;
};

int RunThreadToDrawRedCrap(void *vp, int sliceNumber, int totalJobs)
{
   if (vp == nullptr)
   {
      return -1;
   }

   auto params = static_cast<RedCrapParams*>(vp);
   auto sliceRoi = computeSliceRoiFromSliceNumber(params->detectionMap.getRows(), params->detectionMap.getCols(), sliceNumber);
   Ipp8uArray detectionMapSlice = params->detectionMap(sliceRoi);
   MTI_UINT16 *frameBits = params->frameBits;
   MTI_UINT8 *modifiedPixelMap = params->modifiedPixelMap;
   int frameBitsRowColOffset = sliceRoi.y * sliceRoi.width;
   MTI_UINT16 maxVal = params->maxVal;
   int nRowCol = sliceRoi.height * sliceRoi.width;

   auto mapIter = detectionMapSlice.begin();
   for (auto rowCol = 0; rowCol < nRowCol; ++rowCol, ++mapIter)
   {
      if (*mapIter == 0)
      {
         continue;
      }

      if (modifiedPixelMap && modifiedPixelMap[rowCol + frameBitsRowColOffset])
      {
         continue;
      }

      int frameBitsIndex = (rowCol + frameBitsRowColOffset) * 3;
      frameBits[frameBitsIndex + 0] = maxVal;
      frameBits[frameBitsIndex + 1] = 0;
      frameBits[frameBitsIndex + 2] = 0; // maxVal;
   }

   return 0;
}

} // anonymous namespace
// ---------------------------------------------------------------------------

void CAutoFilter::stashFrameBitsForDetectionVisualizer(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes)
{
   frameIndex = (frameIndex >= 0) ? frameIndex : currentlyDisplayedFrameIndex();

   // Check if bits were already stashed.
   if (_stashedPreviewHackFrameBits != nullptr && _stashedPreviewHackFrameIndex == frameIndex)
   {
      // Nothing to do!
      return;
   }

   if (frameBitsSizeInBytes == 0)
   {
      auto nativeImageFormat = getSystemAPI()->getVideoClipImageFormat();
      if (nativeImageFormat == nullptr)
      {
         // No clip loaded!
         return;
      }

      CImageFormat internalImageFormat;
      internalImageFormat.setToInternalFormat(*nativeImageFormat);
      frameBitsSizeInBytes = internalImageFormat.getBytesPerField();
   }

   // Adjust size of the copy buffer if necessary.
   if (frameBitsSizeInBytes > _stashedPreviewHackFrameBitsSize)
   {
      MTIfree(_stashedPreviewHackFrameBits);
      _stashedPreviewHackFrameBits = (MTI_UINT16*) MTImalloc(frameBitsSizeInBytes);
      _stashedPreviewHackFrameBitsSize = frameBitsSizeInBytes;
   }

   // If frameBits were passed in, just make a copy of those, else read and
   // stash the bits for the currently displayed frame.
   if (frameBits != nullptr)
   {
      MTImemcpy(_stashedPreviewHackFrameBits, frameBits, frameBitsSizeInBytes);
   }
   else
   {
      getSystemAPI()->getToolFrame(_stashedPreviewHackFrameBits, frameIndex);
   }

   _stashedPreviewHackFrameIndex = frameIndex;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onPreviewHackDraw(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes)
{
   CAutoThreadLocker lock(_previewHackLock);

   // If we moved to a new frame, invalidate all the stashed stuff.
   if (_stashedPreviewHackFrameIndex != frameIndex)
   {
      clearStashedStuff();
   }

   // Bail if we don't want to show red stuff at this time (disabled or playing).
   if (_currentDetectionVisualizationState == false
   || getSystemAPI()->IsPlayerReallyPlaying())
   {
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   bool showingProvisional = getSystemAPI()->IsProvisionalPending()
                              && getSystemAPI()->GetProvisionalFrameIndex() == getSystemAPI()->getLastFrameIndex();

   // Need to create or draw the red stuff.
   if (_stashedPreviewHackDetectionMap.isEmpty())
   {
      // We have to create a new detection mask to show. This will be done in a
      // background thread, which will require us doing a refresh of the displayed
      // frame so we re-enter here and draw the red stuff.
      CHRTimer STASH_PREVIEW_BITS_TIMER;

      // DO NOT USE THE frameBits ARG WHEN THE PROVISIONAL IS SHOWING!
      // We need the original bits for the frame!
      frameBits = showingProvisional ? nullptr : frameBits;

      stashFrameBitsForDetectionVisualizer(frameIndex, frameBits, frameBitsSizeInBytes);
      KickTheDetectionMapCreationThread();

      // DBTRACE(STASH_PREVIEW_BITS_TIMER.ReadAsString());
   }
   else
   {
      // We have a cached detection map. Just show that!
		const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
		if (imageFormat == nullptr)
		{
			return TOOL_RETURN_CODE_EVENT_CONSUMED;
		}

      CHRTimer DRAW_RED_CRAP_TIMER;

      bool skipModifiedPixels = !_modifiedPixelMask.isEmpty() && showingProvisional;

      MTI_UINT16 maxVals[3];
      imageFormat->getComponentValueMax(maxVals);
      RedCrapParams redCrapParams = {
                        _stashedPreviewHackDetectionMap,
                        frameBits,
                        skipModifiedPixels ? _modifiedPixelMask.data() : nullptr,
                        maxVals[0]};

      SynchronousThreadRunner multithread(NumberOfSlices, &redCrapParams, RunThreadToDrawRedCrap);
      multithread.Run();

      // DBTRACE(DRAW_RED_CRAP_TIMER.ReadAsString());

      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

void CAutoFilter::DetectionMapCreationThreadTrampoline(void *aThreadArg, void *aThreadHandle)
{
   CAutoFilter *this_ = reinterpret_cast<CAutoFilter*>(aThreadArg);
   this_->_detectionMapThreadIsRunning = true;

   BThreadBegin(aThreadHandle);

   if (this_ != NULL)
   {
      this_->RunDetectionMapCreationThread();
   }
}
// ---------------------------------------------------------------------------

void CAutoFilter::RunDetectionMapCreationThread()
{
   // DBTRACE("LOCK_previewHackLock");
   CAutoThreadLocker lock(_previewHackLock);
   while (_currentDetectionVisualizationState == true)
   {
      _detectionMapEvent.reset();
      if (_stashedPreviewHackDetectionMap.isEmpty()
      && _stashedPreviewHackFrameBits != nullptr
      && GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
      {
         try
         {
            _stashedPreviewHackDetectionMap =
                  CreateDetectionMap(_stashedPreviewHackFrameBits, _stashedGrainFilters, &_stashedBlobPixelArrayWrapper);
         }
         catch (...)
         {
            _stashedPreviewHackDetectionMap.clear();
            TRACE_0(errout << "CreateDetectionMap threw an exception!");
         }

         _needToRefreshTheDisplayedFrame = true;
      }

      {
         // DBTRACE("UNLOCK_previewHackLock");
         CAutoThreadUnlocker unlock(_previewHackLock);
         _detectionMapEvent.wait(500);
         // DBTRACE("LOCK_previewHackLock");
      }
   }

   _detectionMapThreadIsRunning = false;
   // DBTRACE("UNLOCK_previewHackLock");
}
// ---------------------------------------------------------------------------

Ipp8uArray CAutoFilter::CreateDetectionMap(MTI_UINT16 *frameBits, GrainFilterSet &grainFilters,
      BlobPixelArrayWrapper **blobPixelArrayWrapper)
{
//   DBCOMMENT("");
//   DBCOMMENT("-----------------------------");

   CHRTimer CreateDetectionMap_TIMER;

   // LOCK _previewHackLock BEFORE CALLING!
//   CAlgorithmAF_MT algorithmAF;
   CAlgorithmAF algorithmAF;
   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == 0)
   {
      return Ipp8uArray(); // No image format, can't happen
   }

   RECT activePictureRect = imageFormat->getActivePictureRect();
   MTI_UINT16 maxDataValue[3];
   imageFormat->getComponentValueMax(maxDataValue);
// MT
//   int processingThreadCount = 1;
//   int retVal = algorithmAF.Initialize(processingThreadCount, false, imageFormat->getTotalFrameHeight(), imageFormat->getTotalFrameWidth(),
//         imageFormat->getComponentCountExcAlpha(), activePictureRect.top, activePictureRect.bottom + 1, activePictureRect.left,
//         activePictureRect.right + 1);
   long lNRow = imageFormat->getTotalFrameHeight();
   long lNCol = imageFormat->getTotalFrameWidth();
   long lNRowCol = lNCol * lNRow;
   long lNCom = imageFormat->getComponentCountExcAlpha();
   long lActiveStartRow = activePictureRect.top;
   long lActiveStopRow = activePictureRect.bottom + 1;
   long lActiveStartCol = activePictureRect.left;
   long lActiveStopCol = activePictureRect.right + 1;

   if (lNRowCol > ROW_COL_10K)
   {
      TRACE_0(errout << "ERROR: Image is too large (" << lNCol << "x" << lNRow << " > " << ROW_COL_10K << ")");
      return Ipp8uArray(); // TOO BIG!
   }

   CHRTimer PREALLOC_MEMORY_TIMER;
   std::unique_ptr<AF_PreallocedMemory> preallocedMemory(
         (lNRowCol <= ROW_COL_2K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_2K) :
         ((lNRowCol <= ROW_COL_4K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_4K) :
         ((lNRowCol <= ROW_COL_6K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_6K) :
         ((lNRowCol <= ROW_COL_8K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_8K) :
         (AF_PreallocedMemory*)(new AF_PreallocedMemory_10K)))));
//   DBTRACE(PREALLOC_MEMORY_TIMER.ReadAsString());

   algorithmAF.Initialize(true, preallocedMemory.get());
   int retVal = algorithmAF.SetDimension(lNRow, lNCol, lNCom, lActiveStartRow, lActiveStopRow, lActiveStartCol, lActiveStopCol);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: AlgorithmAF->SetDimension FAIL (" << retVal << ")");
      return Ipp8uArray(); // TOO BIG!
   }

   MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
   MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];
   imageFormat->getComponentValueMin(cvMin);
   imageFormat->getComponentValueMax(cvMax);
   algorithmAF.SetImageBounds(cvMin, cvMax);

   algorithmAF.SetAlphaMatteType(imageFormat->getAlphaMatteType(), imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR);
   algorithmAF.SetEmergencyStopFlagPtr(nullptr);

   CAutoFilterToolParameters afToolParams;
	OLDAF_GatherParameters(&afToolParams, true);
   retVal = algorithmAF.AssignParameter(&afToolParams.getParameterRef());

   MTI_UINT16 *uspaSrc[AF_NUM_SRC_FRAMES] = {frameBits, frameBits, frameBits};
   long laFrameIndexInInputSet[AF_NUM_SRC_FRAMES] = {-1, 0, 1};
   algorithmAF.SetSrcImgPtr(uspaSrc, laFrameIndexInInputSet);
   algorithmAF.SetDstImgPtr(frameBits);
   algorithmAF.SetPixelRegionList(nullptr);
   algorithmAF.SetModifiedPixelMap(nullptr);

   Ipp8uArray detectionMap = algorithmAF.MakeDetectionMap(grainFilters, blobPixelArrayWrapper);

//    DBTRACE(CreateDetectionMap_TIMER.ReadAsString());

   return detectionMap;
}
// ---------------------------------------------------------------------------

void CAutoFilter::CheckIfDetectionVisualizationIsAllowed()
{
   _isDetectionVisualizationAllowedNow =
         (QueryOpMode() == DebrisToolOpMode::SHINE)
          ? SHINE_isDetectionVisualizationAllowed()
	       : OLDAF_isDetectionVisualizationAllowed();

   UpdateDetectionVisualizationState();
}
// ---------------------------------------------------------------------------

bool CAutoFilter::OLDAF_isDetectionVisualizationAllowed()
{
   CAutoFilterParameters toolParams;
   GatherFilterParameters(toolParams, true);
   return toolParams.lNSetting == 1
            && toolParams.bUseAlphaMatteToFindDebris[0]
            && getSystemAPI()->IsPlayerReallyPlaying() == false
            && getSystemAPI()->IsGOVInProgress() == false
            && GetNumberOfSelectedFilters() == 1;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_isDetectionVisualizationAllowed()
{
   ShineToolSettings shineSettings;
   GatherShineGuiParameters(shineSettings);
   return shineSettings.detectAlpha
            && getSystemAPI()->IsPlayerReallyPlaying() == false
            && getSystemAPI()->IsGOVInProgress() == false
            && GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED;
}
// ---------------------------------------------------------------------------

void CAutoFilter::SetUserDesiredDetectionVisualizationState(bool enableFlag)
{
   _userDesiredDetectVisualizationState = enableFlag;
   if (enableFlag)
   {
//      _isItOkToDrawRedStuffOnTheProvisional = true;
      _needToRefreshTheDisplayedFrame = true;
   }

   UpdateDetectionVisualizationState();
}
// ---------------------------------------------------------------------------

void CAutoFilter::UpdateDetectionVisualizationState()
{
   // Don't want to hang the main thread. onHeartbeat will eventually get to do this.
   // DBTRACE("TRY TO LOCK_previewHackLock");
   if (!_previewHackLock.TryToLock())
   {
      // DBTRACE("TRY TO LOCK_previewHackLock FAIL");
      return;
   }

   bool showingProvisional = getSystemAPI()->IsProvisionalPending()
                              && getSystemAPI()->GetProvisionalFrameIndex() == getSystemAPI()->getLastFrameIndex();
   bool newDetectionVisualizationState = _userDesiredDetectVisualizationState
                                          && _isDetectionVisualizationAllowedNow;
//                                          && (!showingProvisional || _isItOkToDrawRedStuffOnTheProvisional);

   if (newDetectionVisualizationState == _currentDetectionVisualizationState)
   {
      // DBTRACE("UNLOCK_previewHackLock");
      _previewHackLock.UnLock();
      return;
   }

   _currentDetectionVisualizationState = newDetectionVisualizationState;
   SetRedEyeButtonState(_isDetectionVisualizationAllowedNow, _currentDetectionVisualizationState);

   // Why is this here? I guess it can't hurt! QQQ
   if (_stashedPreviewHackFrameIndex != currentlyDisplayedFrameIndex())
   {
      clearStashedStuff();
   }

   stashFrameBitsForDetectionVisualizer();
   KickTheDetectionMapCreationThread();

   if (!getSystemAPI()->IsPlayerReallyPlaying())
   {
      getSystemAPI()->refreshFrameCached();
   }

   _previewHackLock.UnLock();
}
// ---------------------------------------------------------------------------

bool CAutoFilter::IsContinuousPreviewAllowedNow()
{
   return (QueryOpMode() == DebrisToolOpMode::SHINE)
            ? SHINE_isContinuousPreviewAllowedNow()
            : OLDAF_isContinuousPreviewAllowedNow();
}
// ---------------------------------------------------------------------------

bool CAutoFilter::OLDAF_isContinuousPreviewAllowedNow()
{
   CAutoFilterParameters toolParams;
   GatherFilterParameters(toolParams, true);
   return toolParams.lNSetting == 1
            && toolParams.bUseAlphaMatteToFindDebris[0]
            && getSystemAPI()->IsPlayerReallyPlaying() == false
            && IsMaskInROIMode() == false;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_isContinuousPreviewAllowedNow()
{
//   ShineToolSettings shineSettings;
//   GatherShineGuiParameters(shineSettings);
//   return shineSettings.detectAlpha
//            && getSystemAPI()->IsPlayerReallyPlaying() == false
//            && IsMaskInROIMode() == false
//            );
   // Not yet implemented
   return false;
}
// ---------------------------------------------------------------------------

void CAutoFilter::SetUserDesiredContinuousPreviewState(bool enabled)
{
   _userDesiredContinuousPreviewState = IsContinuousPreviewAllowedNow() ? enabled : false;
   UpdateContinuousPreviewState();
}
// ---------------------------------------------------------------------------

void CAutoFilter::UpdateContinuousPreviewState()
{
   bool isContinuousPreviewAllowed = IsContinuousPreviewAllowedNow();

   if (getSystemAPI()->IsPlayerReallyPlaying())
   {
      // Playing cancels continuous preview mode.
      _userDesiredContinuousPreviewState = false;
   }

   bool newContinuousPreviewState = isContinuousPreviewAllowed && _userDesiredContinuousPreviewState;
   if (newContinuousPreviewState == _currentContinuousPreviewState)
   {
      return;
   }

   _currentContinuousPreviewState = newContinuousPreviewState;

   SetGreenEyeButtonState(isContinuousPreviewAllowed, _userDesiredContinuousPreviewState);

   bool isToolProcessing = GetToolProcessingControlState() != TOOL_CONTROL_STATE_STOPPED;
   if (newContinuousPreviewState == true)
   {
      GetToolProgressMonitor()->SetStatusMessage("Start continuous preview");

      if (isToolProcessing == false)
      {
         _oldContinuousPreviewSerialNumber = _continuousPreviewSerialNumber;
         _oldContinuousPreviewFrameIndex = currentlyDisplayedFrameIndex();
         provisionalAction = PA_AutoReject;
         RunExecButtonsCommand(EXEC_BUTTON_PREVIEW_FRAME);
      }
      else
      {
         GetToolProgressMonitor()->SetStatusMessage("Start continuous preview");

         // onHeartbeat() will run process after the tool stops.
         BumpContinuousPreviewSerialNumber();
      }
   }
   else
   {
      GetToolProgressMonitor()->SetStatusMessage("Stop continuous preview");
      resolveProvisional();
   }
}
// ---------------------------------------------------------------------------

void CAutoFilter::KickTheDetectionMapCreationThread()
{
   if (_currentDetectionVisualizationState == true && _detectionMapThreadIsRunning == false)
   {
      auto threadId = BThreadSpawn(DetectionMapCreationThreadTrampoline, this);
      if (threadId == NULL)
      {
         TRACE_0(errout <<
               "INTERNAL ERROR: CAutoFilter::KickTheDetectionMapCreationThread" " Can't launch Detection Map Creation Thread");
      }
   }

   _detectionMapEvent.signal();
}
// ---------------------------------------------------------------------------

void CAutoFilter::CreateDetectionMapIfNecessary()
{
   // DBTRACE("LOCK_previewHackLock");
   CAutoThreadLocker lock(_previewHackLock);

   if (_stashedPreviewHackDetectionMap.isEmpty())
   {
      UpdateDetectionMap();
   }

   // DBTRACE("UNLOCK_previewHackLock");
}
// ---------------------------------------------------------------------------

bool CAutoFilter::UpdateDetectionMap()
{
   // DBTRACE("LOCK_previewHackLock");
   CAutoThreadLocker lock(_previewHackLock);

   // Akways clear the detection map and the grain filters!
   _stashedPreviewHackDetectionMap.clear();
//   _stashedGrainFilters.clear();  // I don't think this should be cleared.

   if (_currentDetectionVisualizationState == false)
   {
      // DBTRACE("UNLOCK_previewHackLock");
      return false;
   }

   stashFrameBitsForDetectionVisualizer();
   KickTheDetectionMapCreationThread();

   // DBTRACE("UNLOCK_previewHackLock");
   return true;
}
//// ---------------------------------------------------------------------------
//
//void CAutoFilter::ClearDetectionMapAndInhibitUpdates()
//{
//   // DBTRACE("LOCK_previewHackLock");
//   CAutoThreadLocker lock(_previewHackLock);
//
//   _stashedPreviewHackDetectionMap.clear();
//   _inhibitDetectionMapUpdates = true;
//   // DBTRACE("UNLOCK_previewHackLock");
//}
// ---------------------------------------------------------------------------

void CAutoFilter::ClearModifiedPixelMap()
{
	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == nullptr)
	{
		return;
	}

   auto matchesExistingMask = imageFormat->getTotalFrameWidth() == _modifiedPixelMask.getWidth()
                              && imageFormat->getTotalFrameHeight() == _modifiedPixelMask.getHeight();
   if (!matchesExistingMask)
   {
      _modifiedPixelMask = Ipp8uArray({int(imageFormat->getTotalFrameWidth()), int(imageFormat->getTotalFrameHeight())});
   }

   _modifiedPixelMask.zero();
}
// ---------------------------------------------------------------------------
int CAutoFilter::onHeartbeat()
{
	// Hide red and green overlays while running, then restore them when stopped.
	if (_wasPlayerReallyPlayingWhenWeLastLooked)
	{
		if (getSystemAPI()->IsPlayerReallyPlaying() == false)
		{
			SetUserDesiredDetectionVisualizationState(_wasRedOverlayOnWhenPlayerStarted);
			_wasPlayerReallyPlayingWhenWeLastLooked = false;
		}
	}
	else if (getSystemAPI()->IsPlayerReallyPlaying())
	{
		_wasRedOverlayOnWhenPlayerStarted = _currentDetectionVisualizationState;
		SetUserDesiredDetectionVisualizationState(false);
		SetUserDesiredContinuousPreviewState(false);
		_wasPlayerReallyPlayingWhenWeLastLooked = true;
	}

	if (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
   {
      if (_oldModifiedPixelMapFrame != currentlyDisplayedFrameIndex())
      {
         ClearModifiedPixelMap();
         _oldModifiedPixelMapFrame = currentlyDisplayedFrameIndex();

         if (getSystemAPI()->IsProvisionalPending()
         && getSystemAPI()->GetProvisionalFrameIndex() == currentlyDisplayedFrameIndex())
         {
            _needToRefreshTheDisplayedFrame = true;
         }
      }

      if (_needToRefreshTheDisplayedFrame
      && !getSystemAPI()->IsPlayerReallyPlaying()
      && (getSystemAPI()->IsProvisionalPending() == false
         || getSystemAPI()->GetProvisionalFrameIndex() != currentlyDisplayedFrameIndex()))
      {
         _needToRefreshTheDisplayedFrame = false;
         getSystemAPI()->refreshFrameCached();
      }
   }

	CheckIfDetectionVisualizationIsAllowed();

	if (_inPreviewHackMode != _currentDetectionVisualizationState && _previewHackLock.TryToLock())
	{
		// DBTRACE("UNLOCK _previewHackLock");
		if (_currentDetectionVisualizationState)
		{
			_inPreviewHackMode = true;
			getSystemAPI()->enterPreviewHackMode();
		}
		else
		{
			_inPreviewHackMode = false;
			getSystemAPI()->exitPreviewHackMode();
		}

		// DBTRACE("UNLOCK_previewHackLock");
		_previewHackLock.UnLock();
	}

	UpdateContinuousPreviewState();

	if (_currentContinuousPreviewState == true
   && _oldContinuousPreviewFrameIndex == currentlyDisplayedFrameIndex())
	{
//		bool updatePreview = false;
//		if (_oldContinuousPreviewSerialNumber != _continuousPreviewSerialNumber)
//		{
//			// Always update if a relevant parameter changed.
//			updatePreview = true;
//		}
//		else if (_oldContinuousPreviewFrameIndex != currentlyDisplayedFrameIndex())
//		{
//			// Cancel continuous preview when we move to a different frame.
//         SetUserDesiredContinuousPreviewState(false);
//		}
//
//      if (updatePreview)

//      if (enabled && down)
//      {
//         AF_GreenEyeButton->Visible = true;
//      }
      if (_oldContinuousPreviewSerialNumber != _continuousPreviewSerialNumber)
      {
			switch (GetToolProcessingControlState())
         {
         case TOOL_CONTROL_STATE_STOPPED:
            _oldContinuousPreviewSerialNumber = _continuousPreviewSerialNumber;
				_oldContinuousPreviewFrameIndex = currentlyDisplayedFrameIndex();
            didExecAutoStop = false;
            getSystemAPI()->ClearProvisional(false); // don't redraw
            provisionalAction = PA_AutoReject;
				CToolObject::StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, IsShowHighlight());
            break;

         case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
				// if (!didExecAutoStop)
            // {
            // didExecAutoStop = true;
            // StopToolProcessing();
				// }

            break;

			default:
            break;
         }
      }
	}
   else
   {
      UpdateContinuousPreviewState();
	}

	return (QueryOpMode() == DebrisToolOpMode::SHINE)
					? SHINE_onHeartbeat()
					: OLDAF_onHeartbeat();
}
// ---------------------------------------------------------------------------

int CAutoFilter::StartSingleFramePreview(bool newHighlightFlag)
{
	if (IsMaskInROIMode())
	{
		// HACK!! Oy, my aching head... if we get a D key during ROI mode,
		// it means the user wants to REJECT the pending fixes and
		// RE-RENDER, *not* preview fixes (presumablly with new settings)!!!
		if (!getSystemAPI()->IsMaskAvailable())
		{
         // If we are in ROI mode and there is no mask, D key is a no-op!
         return 0;
      }

      rejectFix(false); // Don't clobber ROI!

      // There normally isn't any difference between render one frame and
      // preview one frame except for the action that is taken when
      // later resolving the provisional frame... in ROI mode they
      // end up being the same
      provisionalAction = PA_AutoAccept;
      return CToolObject::StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, newHighlightFlag);
   }

	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StartSingleFramePreview(newHighlightFlag)
						: OLDAF_StartSingleFramePreview(newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::StartSingleFrameRender(bool newHighlightFlag)
{
	resolveProvisional();  // QQQ WHY DOESN'T StartSingleFramePreview DO THIS??

	// HACK - Let the provisional stay pending in ROI mode
	if (IsMaskInROIMode())
	{
		if (!getSystemAPI()->IsMaskAvailable())
      {
			// If we are in ROI mode and there is no mask, G key is a no-op!
         return 0;
		}

      // Why does StartSingleFramePreview() do rejectFix(false); but not here?? QQQ

      // In ROI mode, the provisional is left pending after rendering
      // the frame, and will eventually be auto-accepted unless we get
      // an A key
      provisionalAction = PA_AutoAccept;
      return CToolObject::StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, newHighlightFlag);
	}

	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StartSingleFrameRender(newHighlightFlag)
						: OLDAF_StartSingleFrameRender(newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::StartMultiFramePreview(bool newHighlightFlag)
{
	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StartMultiFramePreview(newHighlightFlag)
						: OLDAF_StartMultiFramePreview(newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::StartMultiFrameRender(bool newHighlightFlag)
{
	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StartMultiFrameRender(newHighlightFlag)
						: OLDAF_StartMultiFrameRender(newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag)
{
	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StartToolProcessing(processingType, newHighlightFlag)
						: OLDAF_StartToolProcessing(processingType, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::StopSingleFrameProcessing()
{
	return (QueryOpMode() == DebrisToolOpMode::SHINE)
						? SHINE_StopSingleFrameProcessing()
						: OLDAF_StopSingleFrameProcessing();
}
// ---------------------------------------------------------------------------

void CAutoFilter::DestroyAllToolSetups(bool notIfPaused)
{
	// This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

	if (OLDAF_multiFrameToolSetupHandle >= 0)
   {
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(OLDAF_multiFrameToolSetupHandle) == AT_STATUS_PAUSED);
      if (!(notIfPaused && isPaused))
      {
			if (OLDAF_multiFrameToolSetupHandle == activeToolSetup)
         {
            getSystemAPI()->SetActiveToolSetup(-1);
			}

			getSystemAPI()->DestroyToolSetup(OLDAF_multiFrameToolSetupHandle);
			OLDAF_multiFrameToolSetupHandle = -1;
      }
   }

	if (OLDAF_singleFrameToolSetupHandle >= 0)
	{
		if (OLDAF_singleFrameToolSetupHandle == activeToolSetup)
		{
			getSystemAPI()->SetActiveToolSetup(-1);
		}

		getSystemAPI()->DestroyToolSetup(OLDAF_singleFrameToolSetupHandle);
		OLDAF_singleFrameToolSetupHandle = -1;
	}

	if (SHINE_preprocessToolSetupHandle >= 0)
	{
		if (SHINE_preprocessToolSetupHandle == activeToolSetup)
		{
			getSystemAPI()->SetActiveToolSetup(-1);
		}

		getSystemAPI()->DestroyToolSetup(SHINE_preprocessToolSetupHandle);
		SHINE_preprocessToolSetupHandle = -1;
	}

	if (SHINE_processToolSetupHandle >= 0)
	{
		if (SHINE_processToolSetupHandle == activeToolSetup)
		{
			getSystemAPI()->SetActiveToolSetup(-1);
		}

		getSystemAPI()->DestroyToolSetup(SHINE_processToolSetupHandle);
		SHINE_processToolSetupHandle = -1;
	}
}
// ----------------------------------------------------------------------

bool CAutoFilter::IsMaskInROIMode()
{
   return maskIsInROIMode;
}
// ----------------------------------------------------------------------

bool CAutoFilter::NotifyStartROIMode()
{
   // Can't switch into ROI mode while running
   if (GetToolProcessingControlState() != TOOL_CONTROL_STATE_STOPPED)
   {
      return false;
   }

   // Things get very confusing if we don't turn off continuous preview!
   if (_currentContinuousPreviewState == true)
   {
      SetUserDesiredContinuousPreviewState(false);
   }

   resolveProvisional();
   maskIsInROIMode = true;
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   return true;
}
// ----------------------------------------------------------------------

bool CAutoFilter::NotifyUserDrewROI()
{
   bool retVal = false;

   if (IsMaskInROIMode())
   {
      resolveProvisional(false); // Don't clear the ROI that was just drawn!

      // Make sure a mask was actually drawn and not canceled
      if (getSystemAPI()->IsMaskAvailable())
      {
         RunExecButtonsCommand((EExecButtonId) EXEC_BUTTON_RENDER_FRAME);
      }

      retVal = true;
   }
   else
   {
      GetToolProgressMonitor()->SetIdle();
   }

   return retVal;
}
// ----------------------------------------------------------------------

void CAutoFilter::NotifyEndROIMode()
{
   // OY, what if we're executing?  QQQ
   if (IsMaskInROIMode())
   {
      resolveProvisional();

      maskIsInROIMode = false;
      UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
   }
}
// ----------------------------------------------------------------------

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CAutoFilter::toolShow()
{
   ShowAutoFilterGUI();

   if (!isItOkToUseToolNow())
   {
      getSystemAPI()->DisableTool(getToolDisabledReason());
   }

   // Just in case these did change
   onNewClip();
   onNewMarks();

   return 0;
}
// ---------------------------------------------------------------------------

void CAutoFilter::UpdateStatusBar()
{
}
// ---------------------------------------------------------------------------

bool CAutoFilter::IsShowing(void)
{
   return (IsToolVisible());
}
// ---------------------------------------------------------------------------

bool CAutoFilter::NotifyGOVStarting(bool *cancelStretch)
{
   bool retVal = false;

   if (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
   {
      resolveProvisional();

      // Give our parent a crack at it (LAST)
      retVal = CToolObject::NotifyGOVStarting(cancelStretch);
   }

   return retVal;
}
// --------------------------------------------------------------------------

void CAutoFilter::NotifyGOVShapeChanged(ESelectedRegionShape shape, const RECT &rect, const POINT *lasso)
{
   // NOTE! if shape is SELECTED_REGION_SHAPE_NONE, that means that the
   // GOV tool ran using OUR old shape, so just leave it alone!!
   if (IsMaskInROIMode() && (shape == SELECTED_REGION_SHAPE_RECT || shape == SELECTED_REGION_SHAPE_LASSO))
   {
      getSystemAPI()->SetROIShape(shape, rect, lasso, NULL); // never bezier!
   }

   inhibitTildeKeyProcessing = false;
   lastActionWasGOV = true;
}
// --------------------------------------------------------------------------

void CAutoFilter::SetGOVRepeatShape()
{
   // ONLY works in ROI mode!!
   if (!IsMaskInROIMode())
   {
      return;
   }

   RECT roiRect;
   POINT roiLasso[MAX_LASSO_PTS];
   BEZIER_POINT roiBezier[MAX_BEZIER_PTS];

   ESelectedRegionShape roiShape = getSystemAPI()->GetROIShape(roiRect, roiLasso, roiBezier);

   if (roiShape == SELECTED_REGION_SHAPE_RECT)
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_RECT, roiRect, NULL, NULL);
      inhibitTildeKeyProcessing = false; // SIDE EFFECT #1
   }
   else if (roiShape == SELECTED_REGION_SHAPE_LASSO)
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_RECT, roiRect, roiLasso, NULL);
      inhibitTildeKeyProcessing = false; // SIDE EFFECT #1
   }
   else if (roiShape == SELECTED_REGION_SHAPE_BEZIER)
   {
      // Gov tool doesn't do Beziers, but it does do pixel regions!
      const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
      if (imageFormat != NULL)
      {
         int totalFrameWidth = imageFormat->getTotalFrameWidth();
         int totalFrameHeight = imageFormat->getTotalFrameHeight();
         int pxlComponentCount = imageFormat->getComponentCountExcAlpha();
         CBezierPixelRegion* bezierRegion = new CBezierPixelRegion(roiBezier, NULL, // Not populated!
               totalFrameWidth, totalFrameHeight, pxlComponentCount);
         CPixelRegionList pixelRegionList;
         pixelRegionList.Add(bezierRegion);
         getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_REGION, bezierRegion->GetRect(), NULL, &pixelRegionList);
         inhibitTildeKeyProcessing = false; // SIDE EFFECT #1
      }
   }
   else
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_NONE, roiRect, NULL, NULL);
      inhibitTildeKeyProcessing = true; // SIDE EFFECT #1
   }

   // SIDE EFFECT #2
   lastActionWasGOV = false;
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CAutoFilter::toolHide()
{
   HideAutoFilterGUI();
   doneWithTool();
   return 0;
}

// ===================================================================
//
// Function:    TryToResolveProvisional
//
// Description: We are given a chance to auto reject before
// the system puts up an obnoxious dialog
//
// Arguments:   None
//
// Returns:     TOOL_RETURN_CODE_NO_ACTION if we didn't have a fix pending,
// else TOOL_RETURN_CODE_EVENT_CONSUMED
//
// ===================================================================
int CAutoFilter::TryToResolveProvisional()
{
   if (getSystemAPI()->IsProvisionalPending())
   {
      resolveProvisional();
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
// Override some CToolObject methods
// ===================================================================

int CAutoFilter::RunFramePreprocessing()
{
   return (QueryOpMode() == DebrisToolOpMode::SHINE)
               ? SHINE_RunFramePreprocessing()
               : CToolObject::RunFramePreprocessing();
}

int CAutoFilter::ProcessSingleFrame(EToolSetupType toolSetupType)
{
   return (QueryOpMode() == DebrisToolOpMode::SHINE)
               ? SHINE_ProcessSingleFrame(toolSetupType)
               : OLDAF_ProcessSingleFrame(toolSetupType);
}

int CAutoFilter::RunFrameProcessing(int newResumeFrame)
{
   return (QueryOpMode() == DebrisToolOpMode::SHINE)
               ? SHINE_RunFrameProcessing(newResumeFrame)
               : OLDAF_RunFrameProcessing(newResumeFrame);
}

// ===================================================================

int CAutoFilter::OLDAF_ProcessSingleFrame(EToolSetupType toolSetupType)
{
   int retVal = 0;
   string errorFunc("<UNKNOWN>");
   string errorMsg;

   if (IsDisabled())
   {
      TRACE_0(errout << "Autofilter internal error, tried to run when disabled ");
      return -999;
   }

   try
   {

      if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
      {
         // Something is running, so can't do Single Frame processing right now
         return -1;
      }

      // PAST ONLY MODE HACK
      if (oldSingleUsePastFramesOnlyFlag != usePastFramesOnlyFlag)
      {
         DestroyAllToolSetups(false);
      }

      // Check if we have a tool setup yet
      if (OLDAF_singleFrameToolSetupHandle < 0)
      {
         // Very conservative
         DestroyAllToolSetups(false);

         // Hack to grab correct tool processor
			_whichAutoFilterTypeToMake = AutoFilterToolSetupType::OLDAF_SINGLE;

         // Create the tool setup
         CToolIOConfig ioConfig(1, 1);
			OLDAF_singleFrameToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Debris", TOOL_SETUP_TYPE_SINGLE_FRAME, &ioConfig);
         if (OLDAF_singleFrameToolSetupHandle < 0)
         {
            retVal = -1;
				throw(errorFunc = "MakeSimpleToolSetup");
         }
      }

      // Set the active tool setup (this is different than the active tool)
		retVal = getSystemAPI()->SetActiveToolSetup(OLDAF_singleFrameToolSetupHandle);
      if (retVal != 0)
      {
         throw(errorFunc = "SetActiveToolSetup");
      }

      // The frame we want to process (last frame displayed)
      int indexOfFrameToProcess = currentlyDisplayedFrameIndex();
      CToolFrameRange frameRange(1, 1); // (in port count, out port count)
      frameRange.inFrameRange[0].randomAccess = true;

      // We need 5 frames, where the middle frame is the one being processed.
      // Here "inFrameIndex" refers to the first INPUT frame, not the first
      // PROCESSED frame! Apparently it is OK to specify frames before the
      // first clip frame!
      frameRange.inFrameRange[0].inFrameIndex = indexOfFrameToProcess - 2;
      frameRange.inFrameRange[0].outFrameIndex = indexOfFrameToProcess + 2;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         throw(errorFunc = "SetToolFrameRange");
      }

      // Set provisional highlighting flag
      getSystemAPI()->SetProvisionalHighlight(highlightFlag);

      // Set AutoFilter tool's parameters
      CAutoFilterToolParameters *toolParams = new CAutoFilterToolParameters; // deleted by tool infrastructure
      if (toolParams == NULL)
      {
         retVal = ERR_PLUGIN_AF_MALLOC;
         throw(errorFunc = "new CAutoFilterToolParameters");
      }

      for (int i = 0; i < AF_MAX_SETTINGS; ++i)
      {
         _afStats[i].Clear();
      }

		retVal = OLDAF_GatherParameters(toolParams, _currentContinuousPreviewState);
      if (retVal != 0)
      {
         throw(errorFunc = "GatherParameters");
      }
      retVal = getSystemAPI()->SetToolParameters(toolParams);
      if (retVal != 0)
      {
         throw(errorFunc = "SetToolParameters");
      }

      // Start the image processing a-runnin'
      errorFunc = "RunActiveToolSetup";
      getSystemAPI()->RunActiveToolSetup();

      return 0;
   }
   catch (string)
   {
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
         errorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         errorMsg = "No alpha channel - cannot find defect map";
         break;
      case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
         break;
      default:
         errorMsg = "Internal Error";
         break;
      }
   }
   catch (std::bad_alloc)
   {
      errorMsg = "OUT OF MEMORY";
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (std::exception ex)
   {
      errorMsg = string("EXCEPTION CAUGHT: ") + string(ex.what());
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_1;
   }
   catch (...)
   {
      errorMsg = "UNHANDLED EXCEPTION";
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_1;
   }
   {
      CAutoErrorReporter autoErr("CAutoFilter::ProcessTrainingFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      autoErr.errorCode = retVal;
      autoErr.msg << errorMsg << endl << " in function " << errorFunc << endl << "Autofilter tool processing aborted";
   }

   return retVal; // ERROR
}
// ===================================================================

int CAutoFilter::SHINE_ProcessSingleFrame(EToolSetupType toolSetupType)
{
   int retVal = 0;
   string errorFunc("<UNKNOWN>");
   string errorMsg;

   if (IsDisabled())
   {
      TRACE_0(errout << "Autofilter internal error, tried to run when disabled ");
      return -999;
   }

   try
   {
      if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
      {
         // Something is running, so can't do Single Frame processing right now
         return -1;
      }

      const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
      if (imageFormat == 0)
      {
         return -1;
      } // No image format, probably clip isn't loaded

      int pixelsPerLine = imageFormat->getPixelsPerLine();

      // PAST ONLY MODE HACK
      if (oldSingleUsePastFramesOnlyFlag != usePastFramesOnlyFlag)
      {
         DestroyAllToolSetups(false);
      }

      // Check if we have a tool setup yet
      if (SHINE_processToolSetupHandle < 0)
      {
         // Very conservative
         DestroyAllToolSetups(false);

         // Hack to grab correct tool processor
			_whichAutoFilterTypeToMake = AutoFilterToolSetupType::SHINE_PROCESS;

         // Create the tool setup
         CToolIOConfig ioConfig(1, 1);
			SHINE_processToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Debris", TOOL_SETUP_TYPE_SINGLE_FRAME, &ioConfig);
         if (SHINE_processToolSetupHandle < 0)
         {
            retVal = -1;
				throw(errorFunc = "MakeSimpleToolSetup");
         }
      }

      // Set the active tool setup (this is different than the active tool)
		retVal = getSystemAPI()->SetActiveToolSetup(SHINE_processToolSetupHandle);
      if (retVal != 0)
      {
         throw(errorFunc = "SetActiveToolSetup");
      }

      // The frame we want to process (last frame displayed)
      int indexOfFrameToProcess = currentlyDisplayedFrameIndex();
      CToolFrameRange frameRange(1, 1); // (in port count, out port count)
      frameRange.inFrameRange[0].randomAccess = true;

      // We need 9 frames per iteration, where the middle frame is the one being processed.
      // There may be fewer frames in the range if we're close to the in or out of the scene.

      int shotIn;
      int shotOut;
      tie(shotIn, shotOut) = getCurrentShotBoundaries();
      MTIassert(shotIn >= 0 && shotOut >= 0);
      frameRange.inFrameRange[0].inFrameIndex = std::max<int>(shotIn, indexOfFrameToProcess - 4);
      frameRange.inFrameRange[0].outFrameIndex = std::min<int>(shotOut, indexOfFrameToProcess + 4 + 1);
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         throw(errorFunc = "SetToolFrameRange");
      }

      // Set provisional highlighting flag
      getSystemAPI()->SetProvisionalHighlight(highlightFlag);

      // Set AutoFilter tool's parameters
      ShineToolParameters *toolParams = new ShineToolParameters; // deleted by tool infrastructure
      if (toolParams == NULL)
      {
         retVal = ERR_PLUGIN_AF_MALLOC;
         throw(errorFunc = "new ShineToolParameters");
      }

		retVal = SHINE_GatherParameters(toolParams, currentlyDisplayedFrameIndex(), true);
      if (retVal != 0)
      {
         throw(errorFunc = "GatherParameters");
      }

      retVal = getSystemAPI()->SetToolParameters(toolParams);
      if (retVal != 0)
      {
         throw(errorFunc = "SetToolParameters");
      }

      // Start the image processing a-runnin'
      errorFunc = "RunActiveToolSetup";
      getSystemAPI()->RunActiveToolSetup();

      return 0;
   }
   catch (string)
   {
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
         errorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         errorMsg = "No alpha channel - cannot find defect map";
         break;
      case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
         break;
      default:
         errorMsg = "Internal Error";
         break;
      }
   }
   catch (std::bad_alloc)
   {
      errorMsg = "OUT OF MEMORY";
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (std::exception ex)
   {
      errorMsg = string("EXCEPTION CAUGHT: ") + string(ex.what());
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_1;
   }
   catch (...)
   {
      errorMsg = "UNHANDLED EXCEPTION";
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_1;
   }
   {
      CAutoErrorReporter autoErr("CAutoFilter::ProcessTrainingFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      autoErr.errorCode = retVal;
      autoErr.msg << errorMsg << endl << " in function " << errorFunc << endl << "Autofilter tool processing aborted";
   }

   return retVal; // ERROR
}

string AfErrorFunc("<UNKNOWN>");
string AfErrorMsg;
int CAutoFilter::OLDAF_RunFrameProcessing(int newResumeFrame)
{
   int retVal = 0;

   if (IsDisabled())
   {
      TRACE_0(errout << "Autofilter internal error, tried to run when disabled ");
      return -999;
   }

   try
   {

      // TBD:  Fix correctly.  The call to refreshFrame partially fixes
      // the problem with the timeline slider getting stuck during
      // tool processing
      AfErrorFunc = "refreshFrame";
      getSystemAPI()->refreshFrame();


      // PAST ONLY MODE HACK
      if (oldMultiUsePastFramesOnlyFlag != usePastFramesOnlyFlag)
      {
         AfErrorFunc = "DestroyAllToolSetups";
         DestroyAllToolSetups(false);
      }

      if (OLDAF_multiFrameToolSetupHandle < 0) // we have no tool setup
      {
         // Very conservative!
         AfErrorFunc = "DestroyAllToolSetups";
         DestroyAllToolSetups(false);

			// Hack to grab correct tool processor
			_whichAutoFilterTypeToMake = AutoFilterToolSetupType::OLDAF_MULTI;

         // Create the tool setup
         AfErrorFunc = "ioConfig";
         CToolIOConfig ioConfig(1, 1);

         // Compute the maximum number of frames we want to process in parallel
         // (restrict it for memory conservation with > 2K  frames
         int maxToolThreadCount = AF_MAX_THREAD_COUNT;

         // Default is one, so only need to set it if we want more than one
         if (maxToolThreadCount >= 2)
         {
            ioConfig.processingThreadCount = maxToolThreadCount;
         }

         TRACE_2(errout << "AF_MT: " << ioConfig.processingThreadCount << " threads");

         AfErrorFunc = "MakeSimpleToolSetup";
			OLDAF_multiFrameToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Debris", TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
			if (OLDAF_multiFrameToolSetupHandle < 0)
         {
            retVal = -1;
            throw(AfErrorFunc = "MakeSimpleToolSetup failed");
         }
      }

      // Set the active tool setup (this is different than the active tool)
      AfErrorFunc = "SetActiveToolSetup";
      retVal = getSystemAPI()->SetActiveToolSetup(OLDAF_multiFrameToolSetupHandle);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "SetActiveToolSetup");
      }

      if (newResumeFrame < 0)
      {
         // Starting from Stop, so set processing frame range to in and out marks
         AfErrorFunc = "Various_1";
         CToolFrameRange frameRange(1, 1);
			frameRange.inFrameRange[0].randomAccess = false;
         int markIn = getSystemAPI()->getMarkIn();
         int markOut = getSystemAPI()->getMarkOut();
         if (markIn < 0 || markOut < 0)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark In and Out must be set to Preview or Render";
            return -1;
         }
         else if (markOut <= markIn)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be beyond Mark In";
            return -1;
         }

         // We need 3 frames per iteration, where the middle frame is the one being processed.
         // Here "inFrameIndex" refers to the first INPUT frame, not the first
         // PROCESSED frame, and outFrameIndex refers to one past the last INPUT
         // frame! Apparently it is OK to specify frames before the
         // first clip frame or after the clip out frame!
         frameRange.inFrameRange[0].inFrameIndex = markIn - 1;
         frameRange.inFrameRange[0].outFrameIndex = markOut + 1;

         AfErrorFunc = "SetToolFrameRange";
         retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
         if (retVal != 0)
         {
            throw(AfErrorFunc = "SetToolFrameRange");
         }
      }
      else
      {
         // Resuming from Pause state, so set processing frame range
         // from newResumeFrame to out mark
         AfErrorFunc = "Various_2";
         CToolFrameRange frameRange(1, 1);
         frameRange.inFrameRange[0].randomAccess = false;
         int markOut = getSystemAPI()->getMarkOut();
         if (markOut < 0)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be set to Preview or Render";
            return -1;
         }
         else if (markOut <= newResumeFrame)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be beyond the Resume frame";
            return -1;
         }

         frameRange.inFrameRange[0].inFrameIndex = newResumeFrame - 1;
         frameRange.inFrameRange[0].outFrameIndex = markOut + 1;
         AfErrorFunc = "SetToolFrameRange";
         retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
         if (retVal != 0)
         {
            throw(AfErrorFunc = "SetToolFrameRange");
         }
      }

      // Set provisional highlighting flag
      AfErrorFunc = "SetProvisionalHighlight";
      getSystemAPI()->SetProvisionalHighlight(highlightFlag);

      // Set render/preview
      AfErrorFunc = "SetProvisionalRender";
      getSystemAPI()->SetProvisionalRender(renderFlag);

      // Set AutoFilter's tool parameters
      // Params get deleted by tool infrastructure
      AfErrorFunc = "SetProvisionalRender";
      CAutoFilterToolParameters *toolParams = new CAutoFilterToolParameters;
      AfErrorFunc = "GatherParameters";
		retVal = OLDAF_GatherParameters(toolParams);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "GatherParameters");
      }
      retVal = getSystemAPI()->SetToolParameters(toolParams);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "SetToolParameters");
      }

      // Start the image processing a-runnin'
      AfErrorFunc = "RunActiveToolSetup";
      getSystemAPI()->RunActiveToolSetup();

      return 0;
   }
   catch (string)
   {
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
         AfErrorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         AfErrorMsg = "No alpha channel - cannot find defect map";
         break;
      case ERR_PLUGIN_AF_MALLOC:
         AfErrorMsg = "Out of memory";
         break;
      default:
         AfErrorMsg = "Internal Error";
         break;
      }
   }
   catch (std::bad_alloc)
   {
      AfErrorMsg = "OUT OF MEMORY";
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (std::exception ex)
   {
      AfErrorMsg = string("EXCEPTION CAUGHT: ") + string(ex.what());
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_2;
   }
   catch (...)
   {
      AfErrorMsg = "UNHANDLED EXCEPTION";
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_2;
   }
   {
      CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      autoErr.errorCode = retVal;
      autoErr.msg << AfErrorMsg << endl << " in function " << AfErrorFunc << endl << "Autofilter tool processing aborted";
   } return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_RunFrameProcessing(int newResumeFrame)
{
   int retVal = 0;

   if (IsDisabled())
   {
      TRACE_0(errout << "Autofilter internal error, tried to run when disabled ");
      return -999;
   }

   try
   {
      if (SHINE_processToolSetupHandle < 0)
      {
         // Very conservative!
         AfErrorFunc = "DestroyAllToolSetups";
         DestroyAllToolSetups(false);

			// Hack to grab correct tool processor
			_whichAutoFilterTypeToMake = AutoFilterToolSetupType::SHINE_PROCESS;

         // Create the tool setup
         AfErrorFunc = "ioConfig";
         CToolIOConfig ioConfig(1, 1);

         // Compute the maximum number of frames we want to process in parallel
         // (restrict it for memory conservation with > 2K  frames
         int maxToolThreadCount = AF_MAX_THREAD_COUNT;

         // SHINE shines with just one thread!
         ioConfig.processingThreadCount = 1;

         TRACE_2(errout << "SHINE_MT: " << ioConfig.processingThreadCount << " threads");

         AfErrorFunc = "MakeSimpleToolSetup";
			SHINE_processToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Debris", TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
			if (SHINE_processToolSetupHandle < 0)
         {
            retVal = -1;
            throw(AfErrorFunc = "MakeSimpleToolSetup failed");
         }
      }

      // Set the active tool setup (this is different than the active tool)
      AfErrorFunc = "SetActiveToolSetup";
      retVal = getSystemAPI()->SetActiveToolSetup(SHINE_processToolSetupHandle);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "SetActiveToolSetup");
      }

      if (newResumeFrame < 0)
      {
         // Starting from Stop, so set processing frame range to in and out marks
         AfErrorFunc = "Various_1";
         CToolFrameRange frameRange(1, 1);
			frameRange.inFrameRange[0].randomAccess = false;
         int markIn = getSystemAPI()->getMarkIn();
         int markOut = getSystemAPI()->getMarkOut();
         if (markIn < 0 || markOut < 0)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark In and Out must be set to Preview or Render";
            return -1;
         }
         else if (markOut <= markIn)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be beyond Mark In";
            return -1;
         }

         // We need 9 frames per iteration, where the middle frame is the one being processed.
         // Here "inFrameIndex" refers to the first INPUT frame, not the first
         // PROCESSED frame, and outFrameIndex refers to one past the last INPUT
         // frame! Apparently it is OK to specify frames before the
         // first clip frame or after the clip out frame!
         // >>>>>>>>> Well, NO, apparently it is NOT OK to specify frames outside the
         // >>>>>>>>> clip range, so clip (markIn-4) to lower shot boundary and
         // >>>>>>>>> and clip (markOut+4) the the upper shot boundary, which
         // >>>>>>>>> includes the clip out!
         vector<int> cutList;
         getSystemAPI()->getCutList(cutList);
         int shotIn;
         int shotOut;
         tie(shotIn, shotOut) = getShotBoundaries(cutList, markIn);
         MTIassert(shotIn >= 0 && shotOut >= 0);
         frameRange.inFrameRange[0].inFrameIndex = std::max<int>(shotIn, markIn - 4);;
         tie(shotIn, shotOut) = getShotBoundaries(cutList, markOut - 1);
         MTIassert(shotIn >= 0 && shotOut >= 0);
         frameRange.inFrameRange[0].outFrameIndex = std::min<int>(shotOut, markOut + 4 + 1);

         AfErrorFunc = "SetToolFrameRange";
         retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
         if (retVal != 0)
         {
            throw(AfErrorFunc = "SetToolFrameRange");
         }
      }
      else
      {
         // Resuming from Pause state, so set processing frame range
         // from newResumeFrame to out mark
         AfErrorFunc = "Various_2";
         CToolFrameRange frameRange(1, 1);
         frameRange.inFrameRange[0].randomAccess = false;
         int markOut = getSystemAPI()->getMarkOut();
         if (markOut < 0)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be set to Preview or Render";
            return -1;
         }
         else if (markOut <= newResumeFrame)
         {
            CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_NO_TRACE);
            autoErr.errorCode = -1;
            autoErr.msg << "Mark Out must be beyond the Resume frame";
            return -1;
         }

         frameRange.inFrameRange[0].inFrameIndex = newResumeFrame - 4;
         frameRange.inFrameRange[0].outFrameIndex = markOut; // + 4;
         AfErrorFunc = "SetToolFrameRange";
         retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
         if (retVal != 0)
         {
            throw(AfErrorFunc = "SetToolFrameRange");
         }
      }

      // Set provisional highlighting flag
      AfErrorFunc = "SetProvisionalHighlight";
      getSystemAPI()->SetProvisionalHighlight(highlightFlag);

      // Set render/preview
      AfErrorFunc = "SetProvisionalRender";
      getSystemAPI()->SetProvisionalRender(renderFlag);

      // Set AutoFilter's tool parameters
      // Params get deleted by tool infrastructure
      AfErrorFunc = "SetProvisionalRender";
      ShineToolParameters *toolParams = new ShineToolParameters;
      AfErrorFunc = "GatherParameters";
		retVal = SHINE_GatherParameters(toolParams);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "GatherParameters");
      }

      retVal = getSystemAPI()->SetToolParameters(toolParams);
      if (retVal != 0)
      {
         throw(AfErrorFunc = "SetToolParameters");
      }

      // Start the image processing a-runnin'
      AfErrorFunc = "RunActiveToolSetup";
      getSystemAPI()->RunActiveToolSetup();

      return 0;
   }
   catch (string)
   {
      switch (retVal)
      {
      case ERR_PLUGIN_AF_NO_FILTERS:
         AfErrorMsg = "No valid filters found in active filter set";
         break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         AfErrorMsg = "No alpha channel - cannot find defect map";
         break;
      case ERR_PLUGIN_AF_MALLOC:
         AfErrorMsg = "Out of memory";
         break;
      default:
         AfErrorMsg = "Internal Error";
         break;
      }
   }
   catch (std::bad_alloc)
   {
      AfErrorMsg = "OUT OF MEMORY";
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
   catch (std::exception ex)
   {
      AfErrorMsg = string("EXCEPTION CAUGHT: ") + string(ex.what());
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_2;
   }
   catch (...)
   {
      AfErrorMsg = "UNHANDLED EXCEPTION";
      retVal = ERR_PLUGIN_AF_UNHANDLED_EXCEPTION_2;
   }
   {
      CAutoErrorReporter autoErr("CAutoFilter::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      autoErr.errorCode = retVal;
      autoErr.msg << AfErrorMsg << endl << " in function " << AfErrorFunc << endl << "Autofilter tool processing aborted";
   } return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_GatherParameters(CAutoFilterToolParameters *toolParams, bool selectedFilterOnly)
{
	toolParams->SetRegionOfInterest(getSystemAPI()->GetMaskRoi());

	CAutoFilterParameters &toolParamRef = toolParams->getParameterRef();

	GatherFilterParameters(toolParamRef, selectedFilterOnly);
	toolParamRef.lFrameIndex = currentlyDisplayedFrameIndex();

   if (toolParamRef.lNSetting == 0)
	{
		return ERR_PLUGIN_AF_NO_FILTERS;
	}

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_GatherParameters(
   ShineToolParameters *toolParams,
   int firstRepairFrameIndex,
   bool singleFrame)
{
	toolParams->SetRegionOfInterest(getSystemAPI()->GetMaskRoi());


	ShineToolSettings &shineToolSettingsRef = toolParams->getShineToolSettingsRef();
//   shineToolSettingsRef.currentFrame = currentlyDisplayedFrameIndex();
   shineToolSettingsRef.firstRepairFrameIndex = (firstRepairFrameIndex == INVALID_FRAME_INDEX)
                                                   ? getSystemAPI()->getMarkIn()
                                                   : firstRepairFrameIndex;
   shineToolSettingsRef.lastRepairFrameIndex = singleFrame ? firstRepairFrameIndex : getSystemAPI()->getMarkOut() - 1;

   shineToolSettingsRef.shinePreprocOutputList = &SHINE_preprocOutputList;
   shineToolSettingsRef.singleShotListIndex = getShotIndexFromFrameIndex(SHINE_preprocOutputList, firstRepairFrameIndex);

   GatherShineGuiParameters(shineToolSettingsRef);
   shineToolSettingsRef.dump();

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::StopFrameProcessing()
{
   int retVal = 0;

   // Kludge so we don't get stuck if the following sequence is followed
   // Preview -> Pause -> Preview 1 -> Stop
   // We really need some better logic here, as this prevents Stop button
   // from acting on Preview 1 processing
   if (OLDAF_multiFrameToolSetupHandle >= 0)
   {
		getSystemAPI()->SetActiveToolSetup(OLDAF_multiFrameToolSetupHandle);
   }

   // Presumably need to do this for SHINE as well?
   if (SHINE_processToolSetupHandle >= 0)
   {
		getSystemAPI()->SetActiveToolSetup(SHINE_processToolSetupHandle);
   }

   retVal = CToolObject::StopFrameProcessing();
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}

// ---------------------------------------------------------------------------
// This is only called from the Tool Infrastructure after tool processing
// is complete -- DO NOT CALL IT FROM ANYWHERE ELSE
void CAutoFilter::UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
   UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);

   // For RENDER_1 write out the modified frame
   if (toolProcessingControlState == TOOL_CONTROL_STATE_STOPPED && _whichAutoFilterTypeToMake != SHINE_PREPROCESS)
   {
      if (provisionalAction == PA_ImmediatelyAccept)
      {
         acceptFix();
      }
      else if (provisionalAction == PA_AutoReject)
      {
         GetToolProgressMonitor()->SetStatusMessage("Previewing fixes");
      }
      else if (provisionalAction == PA_AutoAccept)
      {
         GetToolProgressMonitor()->SetStatusMessage("Fixes are pending");
      }
      else
      {
         MTIassert(!getSystemAPI()->IsProvisionalPending());
      }

      // OK to do ROIs again now!
      if (IsMaskInROIMode())
      {
         getSystemAPI()->LockROI(false);
      }

      if (FilterStatsForm && FilterStatsForm->Visible)
      {
         FilterStatsForm->UpdateDisplay();
      }
   }
}
// ---------------------------------------------------------------------------

void CAutoFilter::SetHighlightFlag(bool newHighlightFlag)
{
   if (highlightFlag != newHighlightFlag)
   {
      highlightFlag = newHighlightFlag;

      // if (highlightFlag == false && getSystemAPI()->IsProvisionalPending())
      // {
      // // We're going to show original values, turn OFF the red overlay.
      // _isItOkToDrawRedStuffOnTheProvisional = false;
      // SetUserDesiredDetectionVisualizationState(false);
      // }

      if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING || getSystemAPI()->IsProvisionalPending())
      {
         // There is a processed image on the screen, so update the
         // Show Highlight/Fix mode immediately
         getSystemAPI()->SetProvisionalHighlight(highlightFlag);
      }
   }
}
// ---------------------------------------------------------------------------

void CAutoFilter::SetUsePastFramesOnlyFlag(bool newUsePastFramesOnlyFlag)
{
   usePastFramesOnlyFlag = newUsePastFramesOnlyFlag;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::GetUsePastFramesOnlyFlag()
{
   return usePastFramesOnlyFlag;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onCapturePDLEntry(CPDLElement &toolParams)
{
   if (IsDisabled())
   {
      return 0;
   }

   WriteAllFiltersToPDLEntry(toolParams);

   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (IsDisabled())
   {
      return 0;
   }

   if (QueryOpMode() == DebrisToolOpMode::SHINE)
   {
      ToggleOpMode();
   }

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   ReadAllFiltersfromPDLEntry(*toolAttribRoot);

   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::onPDLExecutionComplete()
{
   if (IsDisabled())
   {
      return 0;
   }

   CleanUpAfterPDLExecution();

   return 0;
}
// -----------------------------------------------------------------------------

bool CAutoFilter::DoesClipHaveAlpha()
{
   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == NULL)
   { // I guess no clip loaded?
      return false;
   }

   return (imageFormat->getAlphaMatteType() != IF_ALPHA_MATTE_NONE);
}
// -----------------------------------------------------------------------------

void CAutoFilter::ToggleProvisionalHighlightsAndFixes()
{
   bool newSetting = !IsShowHighlight();

   // Switch the GUI control.
   SetShowHighlights(newSetting);

   // Tell the displayer.
   getSystemAPI()->SetProvisionalHighlight(newSetting);

   // If the Provisional is pending, always show Processed frame
   // after switching modes.
   //
   if ((getSystemAPI()->IsProvisionalPending()) && (!getSystemAPI()->IsProcessedVisible()))
   {
      getSystemAPI()->ToggleProvisional();
   }
}
// ----------------------------------------------------------------------

void CAutoFilter::acceptFix(bool clearROI)
{
   getSystemAPI()->AcceptGOV(); // Make sure it's not a GOV pending!

   if (getSystemAPI()->IsProvisionalPending())
   {
      // It's ours!
      getSystemAPI()->AcceptProvisional(true); // true = show the frame

      GetToolProgressMonitor()->SetStatusMessage("Fixes accepted");

      // Tell GOV tool what the latest fix region is
      SetGOVRepeatShape();
   }

   provisionalAction = PA_ShouldNotBePending;

   if (clearROI && IsMaskInROIMode())
   {
      getSystemAPI()->LockROI(false);
      getSystemAPI()->ClearMask();
   }
}
// ----------------------------------------------------------------------

void CAutoFilter::rejectFix(bool clearROI)
{
   if (getSystemAPI()->IsProvisionalPending())
   {
      ClearModifiedPixelMap();

      getSystemAPI()->RejectProvisional(true); // true = jump back to the frame!

      GetToolProgressMonitor()->SetStatusMessage("Fixes rejected");

      if (clearROI && IsMaskInROIMode())
      {
         getSystemAPI()->LockROI(false);
         getSystemAPI()->ClearMask();
      }

      // Tell GOV tool to forget any shape we passed it
      RECT dummyRect; // Bad API design!
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_NONE, dummyRect, NULL, NULL);
   }

   provisionalAction = PA_ShouldNotBePending;
}
// ----------------------------------------------------------------------

void CAutoFilter::resolveProvisional(bool clearROI)
{
   // Do nothing if we are presently running
   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
   {
      return;
   }

   // HAHAHA I just came here to add thia very thing. Why is this commented out??
   //////////////////////////////////////////////////////////////////////////////
   // If we are in Continuous Preview mode, exit from that mode and always
   // clear the provisional.
   // if (_currentContinuousPreviewState)
   // {
   // SetUserDesiredContinuousPreviewState(false);
   // getSystemAPI()->RejectProvisional(false); // don't go to the frame
   // provisionalAction = PA_ShouldNotBePending;
   // return;
   // }

   getSystemAPI()->AcceptGOV();

   ClearModifiedPixelMap();

   if (getSystemAPI()->IsProvisionalPending())
   {
      // It's our provisional
      MTIassert(provisionalAction != PA_ShouldNotBePending);

      RECT dummyRect; // Bad API design!

      switch (provisionalAction)
      {
      case PA_AutoAccept:
      case PA_ImmediatelyAccept:
         getSystemAPI()->AcceptProvisional(false); // don't go to the frame
         SetGOVRepeatShape(); // Tell GOV tool what the latest fix region is
         break;

      case PA_AutoReject:
      default:
         getSystemAPI()->RejectProvisional(false); // don't go to the frame
         getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_NONE, dummyRect, NULL, NULL);
         break;
      }

      if (clearROI && IsMaskInROIMode())
      {
         getSystemAPI()->LockROI(false);
         getSystemAPI()->ClearMask();
      }

      GetToolProgressMonitor()->SetIdle();

      // I don't think we need to refresh the frame here...
   }

   provisionalAction = PA_ShouldNotBePending;
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_onHeartbeat()
{
   static int old_showAlphaState = -1;
   auto showAlphaState = getSystemAPI()->getShowAlphaState();
   if (old_showAlphaState != showAlphaState)
   {
      old_showAlphaState = showAlphaState;
      SetShowAlphaButtonState(showAlphaState);
   }

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StartSingleFramePreview(bool newHighlightFlag)
{
   preprocessSingleFramePreview();
   resolveProvisional();
   provisionalAction = PA_AutoReject;

   return CToolObject::StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StartSingleFrameRender(bool newHighlightFlag)
{
   // In non-ROI mode, the provisional is NOT left pending, it is
   // accepted as soon as processing is done!
   provisionalAction = PA_ImmediatelyAccept;

   return OLDAF_StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StartMultiFramePreview(bool newHighlightFlag)
{
	if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
	{
      return 0;
   }

   resolveProvisional(false); // Dont clear ROI, we may want to preview that!
   provisionalAction = PA_AutoReject;

   return OLDAF_StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StartMultiFrameRender(bool newHighlightFlag)
{
	if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
	{
      return 0;
   }

   // HACK = NOT rejectFix()!! (don't want to clear ROI)
   getSystemAPI()->RejectProvisional(false); // don't go to the frame
   provisionalAction = PA_ImmediatelyAccept;

   return OLDAF_StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag)
{
	MTIassert(!getSystemAPI()->IsProvisionalPending()); // should not be
//   MTIassert(provisionalAction != PA_ShouldNotBePending);  Can happen if run from PDL
	MTIassert((!IsMaskInROIMode()) || getSystemAPI()->IsMaskAvailable());

	if (IsMaskInROIMode())
	{
		getSystemAPI()->LockROI(true);
	}

	// Turn off red crap.
	_userDesiredDetectVisualizationState = false;

	return CToolObject::StartToolProcessing(processingType, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::OLDAF_StopSingleFrameProcessing()
{
	// Prevent partially rendered frame from being written out
	getSystemAPI()->SetProvisionalRender(false);

	// Tell the tool processor to stop itself
	getSystemAPI()->EmergencyStopActiveToolSetup();

	// Do normal stop processing
	return CToolObject::StopSingleFrameProcessing();
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

ShineControlState CAutoFilter::SHINE_getControlState()
{
	return SHINE_controlState;
}
// ---------------------------------------------------------------------------

void CAutoFilter::SHINE_setControlState(ShineControlState newControlState)
{
	SHINE_controlState = newControlState;
	UpdateExecutionButtons(GetToolProcessingControlState(), GetToolProcessingRenderFlag());
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_onHeartbeat()
{
	switch (SHINE_getControlState())
	{
	default:
	case SHINE_STATE_IDLE:
		// Do nothing, duh
		break;

	case SHINE_STATE_GOT_AN_ERROR:
		StopToolProcessing();
		SHINE_setControlState(SHINE_STATE_IDLE);
		break;

	case SHINE_STATE_NEED_TO_RUN_PREPROCESSOR:
		SHINE_StartPreprocessing();
		break;

	case SHINE_STATE_PREPROCESS_ANOTHER_SHOT:
   {
      // Check if it was successful? TODO
      MTIassert(SHINE_preprocShotIndex >= 0);
      MTIassert(SHINE_preprocShotIndex < SHINE_preprocOutputList.size());
      auto &listItem = SHINE_preprocOutputList[SHINE_preprocShotIndex];
      if (listItem.preprocOutput && SHINE_preprocOutputCache)
      {
         auto cacheID = ShinePreprocOutputCache::makeCacheId(listItem.inFrame, listItem.outFrame);
         SHINE_preprocOutputCache->addPreprocOutputToCache(cacheID, listItem.preprocOutput);
      }

      SHINE_RunPreprocessingOnNextUnpreprocessedShot();
		break;
   }

	case SHINE_STATE_PREPROCESSING:
		break;

	case SHINE_STATE_PREPROCESSING_PAUSED:
		GetToolProgressMonitor()->SetStatusMessage("Preprocessing paused");
		break;

	case SHINE_STATE_PREPROCESSING_COMPLETE:
   {
      // **************** If aborted or error, go IDLE instead of next state + error message? TODO *******8
		SHINE_setControlState(SHINE_nextStateAfterAllPreprocessingIsComplete);
		SHINE_nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE;
      if (SHINE_frameToJumpToAfterAllPreprocessingIsComplete != INVALID_FRAME_INDEX)
      {
         getSystemAPI()->goToFrameSynchronous(SHINE_frameToJumpToAfterAllPreprocessingIsComplete);
         SHINE_frameToJumpToAfterAllPreprocessingIsComplete = INVALID_FRAME_INDEX;
      }

		break;
   }

	case SHINE_STATE_NEED_TO_START_SINGLE_FRAME_PREVIEW:
		SHINE_StartSingleFramePreview(true);
		break;

	case SHINE_STATE_SINGLE_FRAME_PREVIEWING:
		// CHECK IF SINGLE FRAME PREVIEWING IS COMPLETE?
		break;

	case SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE:
		SHINE_setControlState(SHINE_STATE_IDLE);
		break;

	case SHINE_STATE_NEED_TO_START_SINGLE_FRAME_RENDER:
		SHINE_StartSingleFrameRender(true);
		break;

	case SHINE_STATE_SINGLE_FRAME_RENDERING:
		// CHECK IF SINGLE FRAME RENDERING IS COMPLETE?
		break;

	case SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE:
		SHINE_setControlState(SHINE_STATE_IDLE);
		break;

	case SHINE_STATE_NEED_TO_START_MULTI_FRAME_PREVIEW:
		SHINE_StartMultiFramePreviewAfterPreprocessing();
		break;

	case SHINE_STATE_MULTI_FRAME_PREVIEWING:
		// CHECK IF SINGLE FRAME PREVIEWING IS COMPLETE?
		break;

	case SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE:
		SHINE_setControlState(SHINE_STATE_IDLE);
		break;

	case SHINE_STATE_NEED_TO_START_MULTI_FRAME_RENDER:
		SHINE_StartMultiFrameRenderAfterPreprocessing();
		break;

	case SHINE_STATE_MULTI_FRAME_RENDERING:
		// Do nothing - monitored by MonitorFrameProcessing() ??? QQQ
		break;

	case SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE:
		//FinishRendering();
		SHINE_setControlState(SHINE_STATE_IDLE);
		break;
	}

   SHINE_updateGuiBasedOnPreprocessingState();

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
// ----------------------------------------------------------------------------

void CAutoFilter::SHINE_updateGuiBasedOnPreprocessingState()
{
   static bool old_allExecButtonsDisabled = false;
   static int old_clipChangeCounter = 0;
   static int old_currentFrame = INVALID_FRAME_INDEX;
   static int old_markIn = INVALID_FRAME_INDEX;
   static int old_markOut = INVALID_FRAME_INDEX;
   static int old_clipIn = INVALID_FRAME_INDEX;
   static int old_clipOut = INVALID_FRAME_INDEX;
   static int old_cacheGeneration = 0;
   static bool old_detectEnabled = true;
   static bool old_alphaAvailable = false;
   static bool old_alphaSelected = false;
   static int old_showAlphaState = -1;

   if (getSystemAPI()->IsPlayerReallyPlaying()
   || SHINE_getControlState() != SHINE_STATE_IDLE)
   {
      if (!old_allExecButtonsDisabled)
      {
         EnableAnalyzeClip(false);
         EnableAnalyzeMarked(false);
         EnableAnalyzeShot(false);
         EnableResetClip(false);
         EnableResetMarked(false);
         EnableResetShot(false);
         EnableSingleProc(false);
         EnableMultiProc(false);
	      SetRedEyeButtonState(false, _currentDetectionVisualizationState);
         EnableSubtoolSwitching(false);

         old_allExecButtonsDisabled = true;
      }

      return;
   }

   ShineToolSettings shineParams;
   GatherShineGuiParameters(shineParams);
   auto detectEnabled = shineParams.detectDark || shineParams.detectBright || shineParams.detectAlpha;
   auto showAlphaState = getSystemAPI()->getShowAlphaState(); // Tri-state (-1 = disabled, 0 = off, 1 = on)
   auto alphaAvailable = showAlphaState != -1;
   auto alphaSelected = shineParams.detectAlpha;

   auto currentFrame = currentlyDisplayedFrameIndex();
   auto markIn = getSystemAPI()->getMarkIn();
   auto markOut = getSystemAPI()->getMarkOut();
   int clipIn, clipOut;
   tie(clipIn, clipOut) = getClipBoundaries();
   auto cacheGeneration = SHINE_preprocOutputCache->getGeneration();

   if (!old_allExecButtonsDisabled
   && old_detectEnabled == detectEnabled
   && old_alphaAvailable == alphaAvailable
   && old_alphaSelected == alphaSelected
   && old_showAlphaState == showAlphaState
   && old_clipChangeCounter == SHINE_clipChangeCounter
   && old_currentFrame == currentFrame
   && old_markIn == markIn
   && old_markOut == markOut
   && old_clipIn == clipIn
   && old_clipOut == clipOut
   && old_cacheGeneration == cacheGeneration)
   {
      return;
   }

   old_allExecButtonsDisabled = false;
   old_detectEnabled = detectEnabled;
   old_alphaAvailable = alphaAvailable;
   old_alphaSelected = alphaSelected;
   old_showAlphaState = showAlphaState;
   old_clipChangeCounter = SHINE_clipChangeCounter;
   old_currentFrame = currentFrame;
   old_markIn = markIn;
   old_markOut = markOut;
   old_clipIn = clipIn;
   old_clipOut = clipOut;
   old_cacheGeneration = cacheGeneration;

   ShinePreprocOutputList prereqList;
   SHINE_constructPrereqList(prereqList, clipIn, clipOut);
   auto allClipPreprocessingIsDone = SHINE_areAllPreprocDataAvailable(prereqList);
   auto someClipPreprocessingIsDone = SHINE_areAnyPreprocDataAvailable(prereqList);
   auto shotPreprocessingIsDone = SHINE_areAllPreprocDataAvailable(prereqList, currentFrame);
   auto marksAreValid = markIn != INVALID_FRAME_INDEX && markOut != INVALID_FRAME_INDEX && markOut > markIn;
   auto allMarkedRangePreprocessingIsDone = marksAreValid && SHINE_areAllPreprocDataAvailable(prereqList, markIn, markOut);
   auto someMarkedRangePreprocessingIsDone = marksAreValid && SHINE_areAnyPreprocDataAvailable(prereqList, markIn, markOut);

   EnableAnalyzeClip(!allClipPreprocessingIsDone);
   EnableAnalyzeMarked(marksAreValid && !allMarkedRangePreprocessingIsDone);
   EnableAnalyzeShot(!shotPreprocessingIsDone);
   EnableResetClip(someClipPreprocessingIsDone);
   EnableResetMarked(marksAreValid && someMarkedRangePreprocessingIsDone);
   EnableResetShot(shotPreprocessingIsDone);
   EnableSingleProc(shotPreprocessingIsDone && detectEnabled);
   EnableMultiProc(marksAreValid && allMarkedRangePreprocessingIsDone && detectEnabled);
   SetRedEyeButtonState(alphaAvailable, _currentDetectionVisualizationState);
   SetShowAlphaButtonState(showAlphaState);
   SetShineDetectEnabledState(alphaAvailable
                              ? (alphaSelected
                                 ? ShineDetectEnableFlags::ALPHA
                                 : (ShineDetectEnableFlags::DARK | ShineDetectEnableFlags::BRIGHT | ShineDetectEnableFlags::ALPHA))
                              : (ShineDetectEnableFlags::DARK | ShineDetectEnableFlags::BRIGHT));
   EnableSubtoolSwitching(true);
}
// ----------------------------------------------------------------------------

void CAutoFilter::SHINE_StartPreprocessing()
{
   resolveProvisional();
	provisionalAction = PA_AutoReject;
   SHINE_preprocessingWasAborted = false;

   // SHINE_preprocOutputList got constructed in SHINE_runPreprocessing()

	_whichAutoFilterTypeToMake = SHINE_PREPROCESS;
   SHINE_setControlState(SHINE_STATE_PREPROCESSING);
   SHINE_StartToolProcessing(TOOL_PROCESSING_CMD_PREPROCESS, false);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartSingleFramePreview(bool newHighlightFlag)
{
   auto currentFrame = currentlyDisplayedFrameIndex();
   SHINE_constructPrereqList(SHINE_preprocOutputList);
   if (!SHINE_areAllPreprocDataAvailable(SHINE_preprocOutputList, currentFrame))
	{
      SHINE_RunPreprocessing(AnalysisScope::CURRENT_SHOT, SHINE_STATE_NEED_TO_START_SINGLE_FRAME_PREVIEW);
		return 0;
	}

	resolveProvisional();
	provisionalAction = PA_AutoReject;

   SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEWING);
	return SHINE_StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartSingleFrameRender(bool newHighlightFlag)
{
   auto currentFrame = currentlyDisplayedFrameIndex();
   SHINE_constructPrereqList(SHINE_preprocOutputList);
   if (!SHINE_areAllPreprocDataAvailable(SHINE_preprocOutputList, currentFrame))
	{
      SHINE_RunPreprocessing(AnalysisScope::CURRENT_SHOT, SHINE_STATE_NEED_TO_START_SINGLE_FRAME_RENDER);
		return 0;
	}

	resolveProvisional();
	provisionalAction = PA_ImmediatelyAccept;

   SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDERING);
	return SHINE_StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, newHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartMultiFramePreview(bool newHighlightFlag)
{
   if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
   {
      // Can't do anything in ROI mode if there's no mask!!
      return 0;
   }

   SHINE_savedHighlightFlag = newHighlightFlag;

   auto markIn = getSystemAPI()->getMarkIn();
   auto markOut = getSystemAPI()->getMarkOut();
   if (markIn == INVALID_FRAME_INDEX || markOut == INVALID_FRAME_INDEX || markOut <= markIn)
   {
      return ERR_PLUGIN_AF_BAD_MARKS;
   }

   SHINE_constructPrereqList(SHINE_preprocOutputList, markIn, markOut);
   if (!SHINE_areAllPreprocDataAvailable(SHINE_preprocOutputList, markIn, markOut))
   {
      SHINE_RunPreprocessing(AnalysisScope::MARKED_RANGE, SHINE_STATE_NEED_TO_START_MULTI_FRAME_PREVIEW, markIn);
      return 0;
   }

   return SHINE_StartMultiFramePreviewAfterPreprocessing();
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartMultiFramePreviewAfterPreprocessing()
{
   SHINE_setControlState(SHINE_STATE_MULTI_FRAME_PREVIEWING);

	resolveProvisional(false); // Dont clear ROI, we may want to preview that!
	provisionalAction = PA_AutoReject;

	return SHINE_StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, SHINE_savedHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartMultiFrameRender(bool newHighlightFlag)
{
   if (IsMaskInROIMode() && !getSystemAPI()->IsMaskAvailable())
   {
      // Can't do anything in ROI mode if there's no mask!!
      return 0;
   }

   SHINE_savedHighlightFlag = newHighlightFlag;

   auto markIn = getSystemAPI()->getMarkIn();
   auto markOut = getSystemAPI()->getMarkOut();
   if (markIn == INVALID_FRAME_INDEX || markOut == INVALID_FRAME_INDEX || markOut <= markIn)
   {
      return ERR_PLUGIN_AF_BAD_MARKS;
   }

   SHINE_constructPrereqList(SHINE_preprocOutputList, markIn, markOut);
   if (!SHINE_areAllPreprocDataAvailable(SHINE_preprocOutputList, markIn, markOut))
   {
      SHINE_RunPreprocessing(AnalysisScope::MARKED_RANGE, SHINE_STATE_NEED_TO_START_MULTI_FRAME_RENDER, markIn);
      return 0;
   }

   return SHINE_StartMultiFrameRenderAfterPreprocessing();
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartMultiFrameRenderAfterPreprocessing()
{
   SHINE_setControlState(SHINE_STATE_MULTI_FRAME_RENDERING);

	// HACK = NOT rejectFix()!! (don't want to clear ROI)
	getSystemAPI()->RejectProvisional(false); // don't go to the frame
	provisionalAction = PA_ImmediatelyAccept;

	return SHINE_StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, SHINE_savedHighlightFlag);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag)
{
	int retVal = 0;

//	// NASTY hack to get PDL to track and/or smooth brfore rendering
//	if (getSystemAPI()->IsPDLRendering()
//	&& !yetAnotherStupidHackFlag
//	&& processingType == TOOL_PROCESSING_CMD_RENDER)
//	{
//		// stupid fricken hack flag because we will reenter here after tracking!!
//		yetAnotherStupidHackFlag = true;
//		setNextStateAfterSmoothing(STABILIZER_STATE_NEED_TO_RUN_RENDERER);
//		_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_RENDER;
//
//		// When PDL, need to always start with tracking! Tracking may not be needed, but smoothing always is!
//		SHINE_setControlState(isInAutoTrack()
//								? STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1
//								: STABILIZER_STATE_NEED_TO_RUN_TRACKER);
//	}
//	else
//	{
		retVal = CToolObject::StartToolProcessing(processingType, newHighlightFlag);
      // In case we are canceled
		if (retVal == 1)
      {
			SHINE_setControlState(SHINE_STATE_IDLE);
		}
//	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_StopSingleFrameProcessing()
{
	return 0;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_loadPreprocOutputForCurrentShot()
{
   return SHINE_loadPreprocOutputForShot(currentlyDisplayedFrameIndex());
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_loadPreprocOutputForShot(int frameIndex)
{
   SHINE_preprocShotIndex = getShotIndexFromFrameIndex(SHINE_preprocOutputList, frameIndex);
   MTIassert(SHINE_preprocShotIndex >= 0);
   if (SHINE_preprocShotIndex < 0)
   {
      TRACE_0(errout << "INTERNAL ERROR: SHINE_preprocOutputList was not initialized");
      return false;
   }

   auto &listItem = SHINE_preprocOutputList[SHINE_preprocShotIndex];
   bool nothingToDo = listItem.preprocOutput != nullptr;
   if (nothingToDo)
   {
      return true;
   }

   bool foundIt = false;
   if (SHINE_preprocOutputCache)
   {
      string cacheId = ShinePreprocOutputCache::makeCacheId(listItem.inFrame, listItem.outFrame);
      listItem.preprocOutput = SHINE_preprocOutputCache->retrievePreprocOutputFromCache(cacheId);
      foundIt = listItem.preprocOutput != nullptr;
   }

   return foundIt;
}
// ---------------------------------------------------------------------------

int CAutoFilter::getNumberOfFramesInCurrentShot()
{
	int shotIn;
	int shotOut;
   tie(shotIn, shotOut) = getCurrentShotBoundaries();
	return (shotIn != INVALID_FRAME_INDEX && shotOut != INVALID_FRAME_INDEX && shotOut > shotIn)
						? shotOut - shotIn
						: 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_RunPreprocessing(
   AnalysisScope scope,
   ShineControlState nextStateAfterAllPreprocessingIsComplete,
   int frameToJumpToAfterAllPreprocessingIsComplete)
{
   int in = INVALID_FRAME_INDEX;
   int out = INVALID_FRAME_INDEX;
   switch (scope)
   {
      default:
      case AnalysisScope::FULL_CLIP:
      {
         tie(in, out) = getClipBoundaries();
         break;
      }

      case AnalysisScope::MARKED_RANGE:
         in = getSystemAPI()->getMarkIn();
         out = getSystemAPI()->getMarkOut();
         break;

      case AnalysisScope::CURRENT_SHOT:
         in = currentlyDisplayedFrameIndex();
         out = in + 1;
         break;
   }

   if (in == INVALID_FRAME_INDEX || out == INVALID_FRAME_INDEX)
   {
      TRACE_0(errout << "INTERNAL ERROR: bad scope!");
      return -1;
   }

   SHINE_nextStateAfterAllPreprocessingIsComplete = nextStateAfterAllPreprocessingIsComplete;
   SHINE_frameToJumpToAfterAllPreprocessingIsComplete =
                  (frameToJumpToAfterAllPreprocessingIsComplete == INVALID_FRAME_INDEX)
                  ? currentlyDisplayedFrameIndex()
                  : frameToJumpToAfterAllPreprocessingIsComplete;

   SHINE_constructPrereqList(SHINE_preprocOutputList, in, out);
   SHINE_prereqIndex = -1;
   SHINE_preprocessingWasAborted = false;
   SHINE_RunPreprocessingOnNextUnpreprocessedShot();

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_ResetPreprocessing(AnalysisScope scope)
{
   int in = INVALID_FRAME_INDEX;
   int out = INVALID_FRAME_INDEX;
   switch (scope)
   {
      default:
      case AnalysisScope::FULL_CLIP:
      {
         tie(in, out) = getClipBoundaries();
         break;
      }

      case AnalysisScope::MARKED_RANGE:
         in = getSystemAPI()->getMarkIn();
         out = getSystemAPI()->getMarkOut();
         break;

      case AnalysisScope::CURRENT_SHOT:
         in = currentlyDisplayedFrameIndex();
         out = in + 1;
         break;
   }

   if (in == INVALID_FRAME_INDEX || out == INVALID_FRAME_INDEX)
   {
      TRACE_0(errout << "INTERNAL ERROR: bad scope!");
      return -1;
   }

   ShinePreprocOutputList list;
   SHINE_constructPrereqList(list, in, out);
   for (auto listItem : list)
   {
      SHINE_resetPreprocOutput(listItem);
   }

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_resetPreprocOutput(ShinePreprocessingOutputItem item)
{
   if (item.preprocOutput && SHINE_preprocOutputCache)
   {
      item.preprocOutput->makeDataInvalid();
      auto cacheID = ShinePreprocOutputCache::makeCacheId(item.inFrame, item.outFrame);
      SHINE_preprocOutputCache->addPreprocOutputToCache(cacheID, item.preprocOutput);
   }

   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_RunPreprocessingOnNextUnpreprocessedShot()
{
   if (SHINE_preprocessingWasAborted)
   {
      SHINE_setControlState(SHINE_STATE_IDLE);
      SHINE_nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE;
      SHINE_frameToJumpToAfterAllPreprocessingIsComplete = INVALID_FRAME_INDEX;
      return 0;
   }

   for (++SHINE_prereqIndex;
   SHINE_preprocOutputList[SHINE_prereqIndex].preprocOutput
      && SHINE_prereqIndex < SHINE_preprocOutputList.size();
   ++SHINE_prereqIndex)
   {
      if (!(SHINE_preprocOutputList[SHINE_prereqIndex].preprocOutput
           && SHINE_preprocOutputList[SHINE_prereqIndex].preprocOutput->isDataValid()))
      {
         break;
      }
   }

   if (SHINE_prereqIndex < SHINE_preprocOutputList.size())
   {
      // Need to run preprocessing on this shot.
      getSystemAPI()->goToFrameSynchronous(SHINE_preprocOutputList[SHINE_prereqIndex].inFrame);
//      SHINE_nextStateAfterProcessingOneShot = SHINE_STATE_PREPROCESS_ANOTHER_SHOT;
      SHINE_setControlState(SHINE_STATE_NEED_TO_RUN_PREPROCESSOR);
   }
   else
   {
      // We're done!
      SHINE_setControlState(SHINE_nextStateAfterAllPreprocessingIsComplete);
      SHINE_nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE;
//      SHINE_nextStateAfterProcessingOneShot = SHINE_STATE_IDLE;
      if (SHINE_frameToJumpToAfterAllPreprocessingIsComplete != INVALID_FRAME_INDEX)
      {
         getSystemAPI()->goToFrameSynchronous(SHINE_frameToJumpToAfterAllPreprocessingIsComplete);
      }
   }

   return 0;
}

// ===================================================================
//
// Function:    MonitorFrameProcessing
//
// Description: Overrides MonitorFrameProcessing in CToolObject so that
// the different passes of SHINE can be monitored by the
// PDL rendering.  This function is very similar to the
// base version but has some key differences.
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CAutoFilter::MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus, int newResumeFrame)
{
	return (QueryOpMode() == DebrisToolOpMode::SHINE)
					? SHINE_MonitorFrameProcessing(newToolCommand, newToolStatus, newResumeFrame)
					: CToolObject::MonitorFrameProcessing(newToolCommand, newToolStatus, newResumeFrame);
}
// ===================================================================

int CAutoFilter::SHINE_MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus, int newResumeFrame)
{
   int retVal = 0;
   EAutotoolStatus newToolProcessingStatus = newToolStatus.status;
   /* */
   bool updateGUI = false;
   /* */

   EToolControlState nextToolControlState;

   // Tool Processing Control State Machine
	nextToolControlState = toolProcessingControlState;
	switch (toolProcessingControlState)
   {
   case TOOL_CONTROL_STATE_WAITING_TO_RUN:
      if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
	/* FALL THROUGH */
    case TOOL_CONTROL_STATE_STOPPED:
      if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
      {
         toolProcessingErrorCode = 0; // clear any old errors
			retVal = PreviewSingleFrame();                      // << ----- ??
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED;
            // was nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = false;
         /* */
			_processSingleFrameIndex = -1; // Not previewing a single frame
         /* */
         retVal = RunFramePreviewing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = true;
         retVal = RenderSingleFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = true;
         /* */
         _processSingleFrameIndex = -1; // Not rendering a single frame
         /* */
         retVal = RunFrameProcessing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREPROCESS)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = false;
         /* */
         _processSingleFrameIndex = -1; // Not preprocessing a single frame
         /* */
			retVal = RunFramePreprocessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_WAITING_TO_STOP:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
			/* <<<<<<<< */
			if (SHINE_getControlState() == SHINE_STATE_PREPROCESSING)
			{
            updateGUI = true;
				SHINE_setControlState(SHINE_STATE_PREPROCESS_ANOTHER_SHOT);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE);
			}
			/* >>>>>>>> */

			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
	  break;

   case TOOL_CONTROL_STATE_RUNNING:
      if (newToolProcessingStatus == AT_STATUS_PROCESS_ERROR)
      {
         toolProcessingErrorCode = newToolStatus.errorCode;

         nextToolControlState = SHINE_nextStateAfterAllPreprocessingIsComplete != SHINE_STATE_IDLE
                                    ? TOOL_CONTROL_STATE_WAITING_TO_RUN
                                    : TOOL_CONTROL_STATE_STOPPED;
      }
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
			/* <<<<<<<< */
			if (SHINE_getControlState() == SHINE_STATE_PREPROCESSING)
			{
				SHINE_setControlState(SHINE_STATE_PREPROCESS_ANOTHER_SHOT);

				// ahahaha this is HIDEOUS! But don't want to change to STOPPED
				// else the PDL monitor gets very confused!
				nextToolControlState = SHINE_nextStateAfterAllPreprocessingIsComplete != SHINE_STATE_IDLE
                                       ? TOOL_CONTROL_STATE_WAITING_TO_RUN
                                       : TOOL_CONTROL_STATE_STOPPED;


            // Even though the tool control state has not changed,
            // we still want to update the GUI to reflect the new
            // processing pass
            updateGUI = true;
         }
         else
			{
				if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_PREVIEWING)
				{
               updateGUI = true;
					SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE);
				}
				else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_PREVIEWING)
				{
					SHINE_setControlState(SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE);
				}
				else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_RENDERING)
				{
					SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE);
				}
				else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_RENDERING)
				{
					SHINE_setControlState(SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE);
				}
				/* >>>>>>>> */

				nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
         }
		}
      else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
			/* <<<<<<<< */
			if (SHINE_getControlState() == SHINE_STATE_PREPROCESSING)
         {
            updateGUI = true;
            SHINE_nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE;
            SHINE_preprocessingWasAborted = true;
				SHINE_setControlState(SHINE_STATE_PREPROCESSING_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE);
			}
			/* >>>>>>>> */
			retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PAUSE)
      {
         retVal = PauseFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			/* <<<<<<<< */
			if (SHINE_getControlState() == SHINE_STATE_PREPROCESSING)
			{
            updateGUI = true;
				SHINE_setControlState(SHINE_STATE_PREPROCESSING_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_MULTI_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE);
			}
			/* >>>>>>>> */

			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
      else if (newToolProcessingStatus == AT_STATUS_PAUSED)
      {
         nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_PAUSED:
      if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_CONTINUE)
      {
         retVal = RunFrameProcessing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
      {
         retVal = ProcessTrainingFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
      {
         retVal = RenderSingleFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
			/* <<<<<< */
			if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_PREVIEWING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE);
			}
			else if (SHINE_getControlState() == SHINE_STATE_SINGLE_FRAME_RENDERING)
			{
				SHINE_setControlState(SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE);
			}
			/* >>>>>> */

         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
      break;
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
         nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
      }
      break;
   default:
      MTIassert(false);
      break;
   }

   if (toolProcessingControlState != nextToolControlState)
   {
      // Control State has changed
      toolProcessingControlState = nextToolControlState;
      /* */
		updateGUI = true;
      /* */
      /* UpdateExecutionGUI(toolProcessingControlState, renderFlag); */
   }

   /* */
	if (updateGUI)
   {
	  UpdateExecutionGUI(toolProcessingControlState, renderFlag);
   }
   /* */

   return retVal;
}
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------


// NOTE: This runs preprocessing on the single shot that contains the currently
// displayed frame, so you need to jump to a frame that's in the shot you want
// to preprocess BEFORE calling StartToolProcessing(TOOL_PROCESSING_CMD_PREPROCESS)!
int CAutoFilter::SHINE_RunFramePreprocessing()
{
	int retVal;
	CAutoErrorReporter autoErr("CAutoFilterTool::SHINE_RunFramePreprocessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
   {
      autoErr.errorCode = -999;
		autoErr.msg << "Shine internal error, tried to run when disabled ";
      return autoErr.errorCode;
   }

   if (!getSystemAPI()->isAClipLoaded())
   {
      autoErr.errorCode = -1;
      autoErr.msg << "No clip open";
	   SHINE_setControlState(SHINE_STATE_GOT_AN_ERROR);
      return -1; // QQQ
   }

   // Check if we have a preprocess tool setup yet
	if (SHINE_preprocessToolSetupHandle < 0)
   {
      // Create the tool setup
		CToolIOConfig ioConfig(1, 1);

      // SHINE shines with just one thread!
      ioConfig.processingThreadCount = 1;

      SHINE_preprocessToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Debris", TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
      if (SHINE_preprocessToolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "SHINE internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
         SHINE_setControlState(SHINE_STATE_GOT_AN_ERROR);
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(SHINE_preprocessToolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
		autoErr.msg << "SHINE internal error: SetActiveToolSetup(" << SHINE_preprocessToolSetupHandle << ") failed with return code " << retVal;
		SHINE_setControlState(SHINE_STATE_GOT_AN_ERROR);
      return retVal;
	}

	// Must preprocess the entire range - you can't start auto-tracking in the middle!
//	MTIassert(newResumeFrame < 1);

	// Starting from Stop, so set processing frame range to in and out marks
	CToolFrameRange frameRange(1, 1);
	frameRange.inFrameRange[0].randomAccess = false;

   // Simple! Need just one frame per iteration!
   int in, out;
   tie(in, out) = getCurrentShotBoundaries();
	frameRange.inFrameRange[0].inFrameIndex = in;
	frameRange.inFrameRange[0].outFrameIndex = out;
	retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "SHINE internal error: SetToolFrameRange" << " failed with return code " << retVal;
		return retVal;
	}

	// Set render/preview flag for tool infrastructure.
	getSystemAPI()->SetProvisionalRender(false);

	// Set Shines's tool parameters
	ShineToolParameters *toolParams = new ShineToolParameters; // deleted by tool infrastructure
   SHINE_GatherParameters(toolParams, currentlyDisplayedFrameIndex(), true);
   SHINE_preprocShotIndex = toolParams->getShineToolSettingsRef().singleShotListIndex;
   if (SHINE_preprocShotIndex < 0)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "Stabilizer internal error: preprocess range confusion." << retVal;
		SHINE_setControlState(SHINE_STATE_GOT_AN_ERROR);
      return -1;
   }

   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilizer internal error: SetToolParameters" << " failed with return code " << retVal;
		SHINE_setControlState(SHINE_STATE_GOT_AN_ERROR);
      return retVal;
   }

//	// hacks
//   SingleFrameFlag = false;

	// Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

     return 0;
}
// ---------------------------------------------------------------------------

tuple<int, int> CAutoFilter::getCurrentShotBoundaries()
{
   vector<int> cutList;
   getSystemAPI()->getCutList(cutList);

   return getShotBoundaries(cutList, currentlyDisplayedFrameIndex());
}
// ---------------------------------------------------------------------------

tuple<int, int> CAutoFilter::getShotBoundaries(const vector<int> &cutList, int frameIndex)
{
   // Check for no cuts (cut list always has cuts at clip in and out frames).
   // Cut list might be empty if we couldn't find it!
   if (cutList.size() < 2)
   {
      return tuple<int, int>(INVALID_FRAME_INDEX, INVALID_FRAME_INDEX);
   }

   int shotIn = cutList.front();
   int shotOut = cutList.back();
   for (auto i = 0; i < cutList.size(); ++i)
   {
      if (cutList[i] > frameIndex)
      {
         shotOut = cutList[i];
         break;
      }

      shotIn = cutList[i];
   }

   return tuple<int, int>(shotIn, shotOut);
}
// ---------------------------------------------------------------------------

bool CAutoFilter::areShotBoundariesSet()
{
   vector<int> cutList;
   getSystemAPI()->getCutList(cutList);

   // Cut list always has cuts at clip in and out frames.
   return cutList.size() < 3;
}
// ---------------------------------------------------------------------------

tuple<int, int> CAutoFilter::getClipBoundaries()
{
   int clipIn = INVALID_FRAME_INDEX;
   int clipout = INVALID_FRAME_INDEX;
   auto frameList = getSystemAPI()->getVideoFrameList();
   if (frameList != nullptr)
   {
      clipIn = 0;
      clipout = frameList->getTotalFrameCount();
   }

   return tuple<int, int>(clipIn, clipout);
}
// ---------------------------------------------------------------------------

int CAutoFilter::currentlyDisplayedFrameIndex()
{
   return getSystemAPI()->getLastFrameIndex();
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_constructPrereqListFromMarks()
{
   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();

   if (markIn == INVALID_FRAME_INDEX || markOut == INVALID_FRAME_INDEX)
   {
      return ERR_PLUGIN_AF_BAD_MARKS;
   }

   return SHINE_constructPrereqList(SHINE_preprocOutputList, markIn, markOut);
}
// ---------------------------------------------------------------------------

int CAutoFilter::SHINE_constructPrereqList(ShinePreprocOutputList &prereqList, int inFrameIndex, int outFrameIndex)
{
   if (inFrameIndex == INVALID_FRAME_INDEX)
   {
      inFrameIndex = currentlyDisplayedFrameIndex();
   }

   if (outFrameIndex == INVALID_FRAME_INDEX)
   {
      outFrameIndex = inFrameIndex + 1;
   }

   vector<int> cutList;
   getSystemAPI()->getCutList(cutList);
   prereqList.clear();
   int frameIndex = inFrameIndex;
   while (true)
   {
      int shotIn;
      int shotOut;
      tie(shotIn, shotOut) = getShotBoundaries(cutList, frameIndex);

      // Check for no clip loaded.
      if (shotIn == INVALID_FRAME_INDEX || shotOut == INVALID_FRAME_INDEX)
      {
         break;
      }

      ShinePreprocOutputSharedPtr preprocOutput;
      if (SHINE_preprocOutputCache)
      {
         auto cacheId = ShinePreprocOutputCache::makeCacheId(shotIn, shotOut);
         preprocOutput = SHINE_preprocOutputCache->retrievePreprocOutputFromCache(cacheId);
      }

      prereqList.emplace_back(shotIn, shotOut, preprocOutput);

      if (shotOut >= outFrameIndex)
      {
         break;
      }

      frameIndex = shotOut;
   }

   return 0;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_areAllPreprocDataAvailable(int inFrameIndex, int outFrameIndex)
{
   ShinePreprocOutputList prereqList;

   if (SHINE_constructPrereqList(prereqList, inFrameIndex, outFrameIndex) != 0)
   {
      return false;
   }

   return SHINE_areAllPreprocDataAvailable(prereqList, inFrameIndex, outFrameIndex);
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_areAllPreprocDataAvailable(
   const ShinePreprocOutputList &prereqList,
   int inFrameIndex,
   int outFrameIndex)
{
   if (prereqList.size() == 0)
   {
      return false;
   }

   if (inFrameIndex == INVALID_FRAME_INDEX)
   {
      inFrameIndex = prereqList.front().inFrame;
      outFrameIndex = prereqList.back().outFrame;
   }
   else if (outFrameIndex == INVALID_FRAME_INDEX)
   {
      outFrameIndex = inFrameIndex + 1;
   }

   for (auto &listItem : prereqList)
   {
      if (inFrameIndex >= 0 && listItem.outFrame <= inFrameIndex)
      {
         // Shot is before range to check.
         continue;
      }

      if (outFrameIndex > 0 && listItem.inFrame >= outFrameIndex)
      {
         // The list is ordered, so we know we're beyond the frames to check.
         break;
      }

      if (listItem.preprocOutput == nullptr)
      {
         // Found an unpreprocessed shot.
         return false;
      }

      if (!listItem.preprocOutput->isDataValid())
      {
         // Found an unpreprocessed shot.
         return false;
      }
   }

   // All shots covering the range are preprocessed.
   return true;
}
// ---------------------------------------------------------------------------

bool CAutoFilter::SHINE_areAnyPreprocDataAvailable(
   const ShinePreprocOutputList &prereqList,
   int inFrameIndex,
   int outFrameIndex)
{
   if (prereqList.size() == 0)
   {
      return false;
   }

   if (inFrameIndex == INVALID_FRAME_INDEX)
   {
      inFrameIndex = prereqList.front().inFrame;
      outFrameIndex = prereqList.back().outFrame;
   }
   else if (outFrameIndex == INVALID_FRAME_INDEX)
   {
      outFrameIndex = inFrameIndex + 1;
   }

   for (auto &listItem : prereqList)
   {
      if (inFrameIndex >= 0 && listItem.outFrame <= inFrameIndex)
      {
         // Shot is before range to check.
         continue;
      }

      if (outFrameIndex > 0 && listItem.inFrame >= outFrameIndex)
      {
         // The list is ordered, so we know we're beyond the frames to check.
         break;
      }

      if (listItem.preprocOutput == nullptr)
      {
         // Found an unpreprocessed shot.
         continue;
      }

      if (listItem.preprocOutput->isDataValid())
      {
         // Found a preprocessed shot.
         return true;
      }
   }

   // No shots covering the range are preprocessed.
   return false;
}
// ---------------------------------------------------------------------------

int CAutoFilter::getCurrentShotIndex(ShinePreprocOutputList &prereqList)
{
   return getShotIndexFromFrameIndex(prereqList, currentlyDisplayedFrameIndex());
}
// ---------------------------------------------------------------------------

int CAutoFilter::getShotIndexFromFrameIndex(ShinePreprocOutputList &prereqList, int frameIndex)
{
   int shotIndex = -1;
   auto &preprocOutputListItem = prereqList[SHINE_prereqIndex];
   for (auto i = 0; i < prereqList.size(); ++i)
   {
      auto &listItem = prereqList[i];
      if (frameIndex >= listItem.inFrame && frameIndex < listItem.outFrame)
      {
         shotIndex = i;
         break;
      }
   }

   return shotIndex;
}
// ---------------------------------------------------------------------------


