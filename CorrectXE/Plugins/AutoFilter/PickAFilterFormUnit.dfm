object PickAFilterForm: TPickAFilterForm
  Left = 1922
  Top = 84
  Anchors = [akLeft, akTop, akRight, akBottom]
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'PickAFilterForm'
  ClientHeight = 235
  ClientWidth = 261
  Color = clBtnFace
  Constraints.MinHeight = 269
  Constraints.MinWidth = 269
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  ExplicitWidth = 320
  ExplicitHeight = 240
  DesignSize = (
    261
    235)
  PixelsPerInch = 96
  TextHeight = 13
  object PleaseSelectLabel: TLabel
    Left = 12
    Top = 12
    Width = 221
    Height = 14
    Caption = 'Please select one or more filters to load:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object OkButton: TButton
    Left = 48
    Top = 194
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 140
    Top = 194
    Width = 75
    Height = 21
    Anchors = [akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object ListBox: TListBox
    Left = 12
    Top = 40
    Width = 237
    Height = 135
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Items.Strings = (
      'White'
      'Black'
      'Red'
      'Green'
      'Blue'
      'Alpha')
    MultiSelect = True
    ParentFont = False
    TabOrder = 2
  end
end
