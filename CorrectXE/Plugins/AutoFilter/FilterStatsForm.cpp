//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FilterStatsForm.h"
#include "AlgorithmAF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFilterStatsForm *FilterStatsForm;
//---------------------------------------------------------------------------
__fastcall TFilterStatsForm::TFilterStatsForm(TComponent* Owner, StatsAF *afStats)
	: TForm(Owner)
	, _afStats(afStats)
	, _currentFilterIndex(0)
{
}
//---------------------------------------------------------------------------
void __fastcall TFilterStatsForm::FilterButtonClick(TObject *Sender)
{
	TSpeedButton *filterButton = dynamic_cast<TSpeedButton *>(Sender);
	if (filterButton == nullptr)
	{
		return;
	}

	filterButton->Down = true;
	_currentFilterIndex = filterButton->Tag;
	UpdateDisplay();
}
//---------------------------------------------------------------------------
void __fastcall TFilterStatsForm::CloseButtonClick(TObject *Sender)
{
	Hide();
}
//---------------------------------------------------------------------------
void __fastcall TFilterStatsForm::FormShow(TObject *Sender)
{
	if (!Visible)
	{
		return;
	}

	UpdateDisplay();
}
//---------------------------------------------------------------------------
void TFilterStatsForm::UpdateDisplay()
{
	StatsAF &afStats = _afStats[_currentFilterIndex];
	MTIostringstream os;
	os << "BLOB STATS FOR FILTER " << (_currentFilterIndex + 1);
	Caption = os.str().c_str();

	os.str("");
	os << endl
	   << "  INITIAL DETECTION:    "                                                        << endl
		<< "       " << afStats.totalBlobsDetected << " total blobs detected"                << endl
		<< "       largest = " << afStats.sizeOfLargestBlobDetected                          << endl
		<< "       smallest = " << afStats.sizeOfSmallestBlobDetected                        << endl
		<< "       too many blobs? " << (afStats.blobCountLimitExceeded ? "YES" : "NO")      << endl << endl

		<< "  AFTER MERGING BLOBS:  "                                                        << endl
		<< "       " << afStats.totalBlobsAfterConsolidation << " blobs remaining"           << endl
		<< "       largest = " << afStats.sizeOfLargestBlobAfterConsolidation                << endl
		<< "       smallest = " << afStats.sizeOfSmallestBlobAfterConsolidation              << endl << endl

		<< "  AFTER SIZE CHECK (RANGE " << afStats.sizeRangeMin << " - " << afStats.sizeRangeMax << ")" << endl
		<< "       " << afStats.totalBlobsAfterSizeRangeCheck << " blobs remaining"          << endl
		<< "       largest = " << afStats.sizeOfLargestBlobAfterSizeRangeCheck               << endl
		<< "       smallest = " << afStats.sizeOfSmallestBlobAfterSizeRangeCheck             << endl << endl

		<< "  AFTER TOLERANCE TEST: "                                                        << endl
		<< "       " << afStats.totalBlobsActuallyProcessed << " blobs remaining"            << endl
		<< "       largest = " << afStats.finalSizeOfLargestBlob                             << endl
		<< "       smallest = " << afStats.finalSizeOfSmallestBlob                           << endl
		<< "       (of which " << afStats.totalDustBlobsInpainted << " were inpainted dust)" << endl;

	 TextBox->Lines->Text = os.str().c_str();
}
//---------------------------------------------------------------------------

