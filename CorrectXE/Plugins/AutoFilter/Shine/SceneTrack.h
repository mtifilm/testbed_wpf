#pragma once
#ifndef SceneTrackH
#define SceneTrackH

#include "Ippheaders.h"
#include "OpticalFlowData.h"
#include <array>

//  A delta contains the scene index and center point of "box" of optical flow
//  proxy image and subpixel accuracy for the full image
struct TestDelta
{
	int sceneIndex = 0;
	int proxyDeltaX = 0;
	int proxyDeltaY = 0;
	float fullDeltaX = 0.0f;
	float fullDeltaY = 0.0f;
};

struct ShotInfoBox
{
	std::array<int, 5> frame;
	std::array<MtiPoint, 5> offset;
	vector<MtiPoint> center;
};

// Resulting structures after processing
struct OpticalFlowTrack
{
	OpticalFlowTrack() = default;

	OpticalFlowTrack(int index, const IppiPoint_32f &pixelPoint, const IppiPoint_32f &proxyPixelPoint)
	{
		startSceneIndex = index;
		addPoint(pixelPoint, proxyPixelPoint);
	}

	int startSceneIndex = -1;
	int indexIntoContainer = -1;
	vector<IppiPoint_32f> subPixelTrack;
	vector<IppiPoint_32f> proxyPixelTrack;

	int getSize() const { return int(subPixelTrack.size());  }
	int getInclusiveEndSceneIndex() const { return startSceneIndex + getSize() - 1; }
	void addPoint(const IppiPoint_32f &subPixelPoint, const IppiPoint_32f &proxyPixelPoint)
	{
		subPixelTrack.push_back(subPixelPoint);
		proxyPixelTrack.push_back(proxyPixelPoint);
	}
};

typedef vector<OpticalFlowTrack> OpticalFlowTracks;

struct CenterTrackBox : public MtiRect
{
	MtiPoint center;
	float xSubpixelCenter = -1;
	float ySubpixelCenter = -1;

	CenterTrackBox() = default;

	CenterTrackBox(const MtiPoint &boxCenter, const MtiSize &boxRadius)
	{
		center = boxCenter;
		width = 2 * boxRadius.width + 1;
		height = 2 * boxRadius.height + 1;

		x = boxCenter.x - boxRadius.width;
		y = boxCenter.y - boxRadius.height;
	}

	// Note: this is not correct as the
	// center is not necessarily the 1/2 point
	CenterTrackBox(const MtiRect &rect, const MtiPoint &pesudoCenter)
	{
		x = rect.x;
		y = rect.y;
		width = rect.width;
		height = rect.height;
		center = pesudoCenter;
	}

	CenterTrackBox expandBox(double scaleX, double scaleY) const
	{
		// We really shouldn't be changing the size but just apply
		// the rx(x + 0.5) transform
		auto rx = scaleX;
		auto ry = scaleY;

		auto rw = int(std::round(rx * width));
		if ((rw % 2) == 0)
		{
			rw = rw + 1;
		}

		auto rh = int(std::round(ry * height));
		if ((rh % 2) == 0)
		{
			rh = rh + 1;
		}

		auto largeBox = *this;

		auto xc = int(std::round(rx * (double(center.x + 0.5))));
		auto yc = int(std::round(ry * (double(center.y + 0.5))));
		largeBox.center = { xc, yc };
		largeBox.width = int(rw);
		largeBox.height = int(rh);

		// the width, height are always odd
		largeBox.x = largeBox.center.x - int(rw / 2);
		largeBox.y = largeBox.center.y - int(rh / 2);

		// Create the large box
		return largeBox;
	}


	CenterTrackBox moveRelative(const MtiPoint &p) const
	{
		auto centerBox = *this;
		centerBox.x += p.x;
		centerBox.y += p.y;
		centerBox.center += p;

		if (xSubpixelCenter > 0)
		{
			centerBox.xSubpixelCenter += p.x;
		}

		if (ySubpixelCenter > 0)
		{
			centerBox.ySubpixelCenter += p.y;
		}

		return centerBox;
	}
};

struct OpticalFlowTrackInfoPerBox
{
	OpticalFlowTrack track;
	int boxSceneIndex = 0;
	vector<TestDelta> deltas;
	CenterTrackBox fullImageBox;
};

// SceneTrack stores the data for a track across a rect
// It tracks a feature of a scene at a start position
// It also includes the gray image of the start box.
// These images are need so the full rectangle is not stored
class SceneTrack
{
public:
	SceneTrack();
	~SceneTrack();

	vector<CenterTrackBox> boxes;
	int startFrameIndex = 1;
	bool active = true;

	// We need to search backwards, so this contains a copy of the backwards search area
	Ipp32fArray firstBoxSearchBackProxyImage;

	// This is the image that is the size of the box, it is a cheap copy
	// from firstBoxInflatedProxyImage
	Ipp32fArray firstBoxProxyImage;

	// This is used for subpixel accuracy. 
	Ipp32fArray firstBoxFullImage;
};

typedef vector<SceneTrack> SceneTracks;
typedef vector<CenterTrackBox> CenterTrackBoxes;
#endif
