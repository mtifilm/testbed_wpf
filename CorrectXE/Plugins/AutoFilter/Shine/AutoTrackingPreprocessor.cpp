#include "AutoTrackingPreprocessor.h"
#include "AutoTrackSupport.h"

bool AutoTrackingPreprocessor::processFrame(const Ipp32fArray& frameArray, int frameIndex)
{
	if (!_initialized)
	{
		processFirstFrame(frameArray, frameIndex);
		_opticalFlowData.clear();
		_sceneIndex = 1;
		_initialized = true;
		_opticalFlowAvailable = false;
	}
	else
	{
		processNextFrame(frameArray, frameIndex);
		_sceneIndex++;

		// Check for end
		if (_sceneIndex == _totalFrames)
		{
			// All tracks that are still active must be valid because we don't add any 
			// in the 10 frames
			for (auto &a : _activeTracks)
			{
				_validTracks.push_back(a);
			}

			trackDataToOpticalFlow();
			_opticalFlowAvailable = true;
			return true;
		}
	}

	return false;
}

TrackFrameInfo AutoTrackingPreprocessor::createTrackFrameInfo(const Ipp32fArray& frameArray, int frameIndex) const
{
	TrackFrameInfo trackFrameInfo;
	trackFrameInfo.fullGrayImage = frameArray; // .applyBilateralFilter(5);
	auto fullGrayImageC = trackFrameInfo.fullGrayImage;
	auto proxyBeforeScale = fullGrayImageC.resizeAntiAliasing(_params.getProxySize());
	std::tie(trackFrameInfo.proxyImage, trackFrameInfo.averageIntensity) = AutoTrackSupport::balanceIntensity(proxyBeforeScale, _params);
	//	std::cout << trackFrameInfo.averageIntensity << std::endl;
	auto proxyImageC = trackFrameInfo.proxyImage;
	return trackFrameInfo;
}

void AutoTrackingPreprocessor::processFirstFrame(const Ipp32fArray & frameArray, int frameIndex)
{
	_params.setFullImageSize(frameArray.getSize());
	_previousFrameInfo = createTrackFrameInfo(frameArray, frameIndex);

	//auto patchMinsC = AutoTrackSupport::findPatchMinimums(_previousFrameInfo.proxyImage, _params);
	auto patchMinsC = _patchMinsMT.computeMin(_previousFrameInfo.proxyImage, _params.getKernelBox());
	auto proxyImageC = _previousFrameInfo.proxyImage;
	
	//SaveVariablesToMatFile2(testFileName("patchMins", frameIndex), patchMinsC, patchMinsC1);

	auto patchCenters = AutoTrackSupport::findPatches(patchMinsC, _params);
	std::cout << "patchCenters found " << patchCenters.size() << std::endl;

	_activeTracks = AutoTrackSupport::makeStartTracks(_previousFrameInfo, patchCenters, 0, _params);
	_validTracks.clear();
}

void AutoTrackingPreprocessor::processNextFrame(const Ipp32fArray& frameArray, int frameIndex)
{
	_currentFrameInfo = createTrackFrameInfo(frameArray, frameIndex);

	AutoTrackSupport::computeTrackMotion(_previousFrameInfo, _currentFrameInfo, _sceneIndex, _activeTracks, _params);
	
	// Begin and end track lengths can be shorter
	auto minTrackLength = _params.getTrajectoryMinLength();
	auto minTrackLength2 = minTrackLength / 2;
	if ((frameIndex >= minTrackLength2) && (frameIndex < _params.getTrajectoryMinLength()))
	{
		minTrackLength = std::max<int>(frameIndex, minTrackLength2);;
	}
	else if (_totalFrames - frameIndex <= minTrackLength2)
	{
		minTrackLength = std::max<int>(_totalFrames - frameIndex, minTrackLength2);
	}

	std::cout << (minTrackLength) << std::endl;

	AutoTrackSupport::pruneActiveTracksAddToValidTracks(_activeTracks, _validTracks, minTrackLength, _params);

	// We don't need to add more tracks when close to end
	if ((_totalFrames - _sceneIndex) >= (_params.getTrajectoryMinLength() - 1))
	{
		//auto patchMinsC = AutoTrackSupport::findPatchMinimums(_currentFrameInfo.proxyImage, _params);
		auto patchMinsC = _patchMinsMT.computeMin(_previousFrameInfo.proxyImage, _params.getKernelBox());
		auto patchCenters = AutoTrackSupport::findPatches(patchMinsC, _params);
		std::cout << "patchCenters found " << patchCenters.size() << std::endl;

		auto newDisjointPatches = AutoTrackSupport::extractDisjointPatches(_activeTracks, patchCenters, _params);
//		std::cout << "patchCenters found " << patchCenters.size() << std::endl;

		auto newActiveTracks = AutoTrackSupport::makeStartTracks(_currentFrameInfo, newDisjointPatches, _sceneIndex, _params);
		for (auto &track : newActiveTracks)
		{
			_activeTracks.push_back(track);
		}
	}

	_previousFrameInfo = _currentFrameInfo;
}

void AutoTrackingPreprocessor::trackDataToOpticalFlow()
{
	MTIassert(!_validTracks.empty());

	// This takes the _valid tracks and copies to the opticalFlow structure
	_opticalFlowData.clear();

	// We now sort _validTracks by start frame
	sort(_validTracks.begin(),
		_validTracks.end(),
		[](const SceneTrack &t1, const SceneTrack &t2)
	{
		return t1.startFrameIndex < t2.startFrameIndex;
	});

	// add the header, for now the header is trackIndex which is total frame in scene, sceneStartFrame the total tracks,
	// the relative size of image to the other x,y
	_opticalFlowData.emplace_back(
		_totalFrames,
		_validTracks.size(),
		_params.getFullImageSize().width,
		_params.getFullImageSize().height,
		MtiPoint(_params.getProxySize().width, _params.getProxySize().height)
	);

	auto trackNumber = 0;
	for (auto &track : _validTracks)
	{
		for (auto &box : track.boxes)
		{
			_opticalFlowData.emplace_back(
				trackNumber,
				track.startFrameIndex,
				box.xSubpixelCenter,
				box.ySubpixelCenter,
				box.center);
		}

		trackNumber++;
	}
}


