#include "AutoTrackingHeaders.h"
#include "SceneTrack.h"
#include <algorithm>
#include "HRTimer.h"
#include "IpaStripeStream.h"

Ipp32fArray AutoTrackSupport::findPatchMinimums(const Ipp32fArray &inputImage, const AutoTrackParameters &params)
{
	auto radius = params.getBoxRadius();
	MtiRect roi(radius, radius, inputImage.getWidth() - 2 * radius, inputImage.getHeight() - 2 * radius);
	auto interiorImage = inputImage(roi);
	Ipp32fArray patchMinimums({ interiorImage.getWidth() - 2 * radius, interiorImage.getHeight() - 2 * radius });

	// Pick a large number
	patchMinimums.set({ IPP_MAXABS_32F });

	auto boxDiameter = 2 * radius + 1;
	MtiSize kernelSize(boxDiameter, boxDiameter);
	Ipp32fArray absDiffImage;
	Ipp8uArray scratchArray;
	Ipp32fArray normArray;

	// This can be multi threaded for speed, see above
	for (auto x = -radius; x <= radius; x++)
	{
		for (auto y = -radius; y <= radius; y++)
		{
			// Skip absolute zero point
			if ((x == 0) && (y == 0))
			{
				continue;
			}

			auto offsetImage = inputImage(roi + MtiPoint(x, y));
			interiorImage.absDiff(offsetImage, absDiffImage);
			absDiffImage.applyBoxFilterValid(kernelSize, normArray, scratchArray);
//			absDiffImage.applyBoxFilter(kernelSize, normArray2, scratchArray2);
//			MatIO::saveListToMatFile("C:\\temp\\test.mat", inputImage, "proxyImageC", absDiffImage, "absDiffImageC", offsetImage, "offsetImageC", interiorImage, "interiorImageC", normArray, "normArrayC");
			patchMinimums.minEveryInplace(normArray);
//			MatIO::appendListToMatFile("C:\\temp\\test.mat", patchMinimums, "patchMinimumsC");
		}
	}

	return patchMinimums;
}

struct QData
{
	QData() = default;
	QData(float newValue, const MtiPoint &corner, int newIndex)
		: value(newValue), centerBox(corner), index(newIndex)
	{}

	float value = 0;
	MtiPoint centerBox;  // Redundant, same a index
	int index = 0;
};

CenterTrackBoxes AutoTrackSupport::findPatches(const Ipp32fArray& patchMins, const AutoTrackParameters& params)
{
    // Just to make life easy
	auto threshold = params.getPatchThreshold();
	auto radius = params.getBoxRadius();
	auto boxEdgeSize = 2 * radius + 1;
	auto proxyWidth = params.getProxySize().width;
	auto proxyHeight = params.getProxySize().height;
	auto fullImageWidth = params.getFullImageSize().width;
	auto fullImageHeight = params.getFullImageSize().height;
	auto patchWidth = patchMins.getWidth();
	auto patchHeight = patchMins.getHeight();

	auto sortedIndexPairs = patchMins.sortWithIndex();

	vector<QData> Q;
	Q.reserve(sortedIndexPairs.size());

	for (auto &s : sortedIndexPairs)
	{
		if (s.first > threshold)
		{
			auto index = s.second;
			Q.emplace_back(s.first, MtiPoint( index % patchWidth, index / patchWidth), index);
		}
	}

	auto i = 0;
	auto n = int(Q.size());

	auto boxOffsetX = (proxyWidth - patchWidth) / 2;
	auto boxOffsetY = (proxyHeight - patchHeight) / 2;

	auto rx = double(fullImageWidth) / double(proxyWidth);
	auto ry = double(fullImageHeight) / double(proxyHeight);

	vector<CenterTrackBox> patchCenters;
	while (i < n)
	{
		// To reduce testing, we will zap Q later
		if (Q[i].value > threshold)
		{
			// Now zap every index that is near the
			auto r = Q[i].centerBox.y;
			auto c = Q[i].centerBox.x;

			CenterTrackBox patchCenter({ c + boxOffsetX, r + boxOffsetY }, { radius, radius });
			patchCenter.xSubpixelCenter = float(std::round(rx*(patchCenter.center.x + 0.5f)));
			patchCenter.ySubpixelCenter = float(std::round(ry*(patchCenter.center.y + 0.5f)));
			patchCenters.push_back(patchCenter);

			for (auto& q : Q)
			{
				if ((abs(q.centerBox.x - c) <= boxEdgeSize) && (abs(q.centerBox.y - r) <= boxEdgeSize))
				{
					q.value = -1;
				}
			}
		}

		i = i + 1;
	}

	return patchCenters;
}

std::pair<Ipp32fArray, float> AutoTrackSupport::balanceIntensity(const Ipp32fArray& sourceImage, const AutoTrackParameters& params)
{
	const auto proxyHeight = params.getProxySize().height;
	const auto proxyWidth = params.getProxySize().width;
	const auto cw = int(proxyWidth * params.getCenterScale());
	const auto ch = int(proxyHeight * params.getCenterScale());
	const auto centerRect = MtiRect((proxyWidth - cw) / 2, (proxyHeight - ch) / 2, cw, ch);
	auto centerImage = sourceImage(centerRect);

	auto centerAverageIntensity = float(centerImage.sum()[0] / centerImage.area());
	if (centerAverageIntensity == 0)
	{
		centerAverageIntensity = 0.5f;
	}

	return { sourceImage*(0.5f / centerAverageIntensity), centerAverageIntensity };
}

SceneTracks AutoTrackSupport::makeStartTracks(const TrackFrameInfo &frameInfo, vector<CenterTrackBox> patchBoxes, int frameIndex, const AutoTrackParameters& params)
{
	/// This just initializes a set of tracks
	//	A track has the start proxy patch image and original patch image, intensity scaled
	auto proxyImage = frameInfo.proxyImage;
	auto fullGrayImage = frameInfo.fullGrayImage;

	vector<SceneTrack> tracks(patchBoxes.size());
	for (auto patchIndex = 0; patchIndex < patchBoxes.size(); patchIndex++)
	{
		SceneTrack sceneTrack;
		auto patch = patchBoxes[patchIndex];
		sceneTrack.boxes.push_back(patch);

		// Make a copy. Otherwise whole frames will live in memory
		// We need to test back, so we need a larger image track back
		MtiRect trackBackBox = patch.inflate(params.getProxyRadiusSearch());

		sceneTrack.firstBoxSearchBackProxyImage <<= proxyImage(trackBackBox, MtiSelectMode::mirror);

		// We need a centered box on the big one
		auto xs = params.getProxyRadiusSearch().width;
		auto ys = params.getProxyRadiusSearch().height;

		MtiRect interiorBox({ xs, ys }, patch.getSize());
		// cannot be outside.
		sceneTrack.firstBoxProxyImage = sceneTrack.firstBoxSearchBackProxyImage(interiorBox);
		auto truth = proxyImage(patch);
		auto b = sceneTrack.firstBoxProxyImage == truth;

		sceneTrack.startFrameIndex = frameIndex;
		sceneTrack.active = true;

		// We need the full size start patch
		auto rx = double(fullGrayImage.getSize().width) / double(params.getProxySize().width);
		auto ry = double(fullGrayImage.getSize().height) / double(params.getProxySize().height);

		auto largeBox = patch.expandBox(ry, rx);
		auto averageIntensity = 0.5f / frameInfo.averageIntensity;

		// Copy here
		sceneTrack.firstBoxFullImage <<= frameInfo.fullGrayImage(largeBox);
		sceneTrack.firstBoxFullImage *= averageIntensity;

		// store the track in the vector
		tracks[patchIndex] = sceneTrack;
	}

	return tracks;
}

// Fucking borland doesn't support tuples
struct XCorrReturn
{
	Ipp32fArray P0;
	MtiPoint P1;
	float P2;
};

XCorrReturn validNormXCorr(const Ipp32fArray &probe, const Ipp32fArray &target)
{
	MTIassert(target.getComponents() == 1);

	auto crossNorm = target.crossCorrNorm(probe, IppiROIShape::ippiROIValid);
	Ipp32f maxValue;
	MtiPoint index;
	crossNorm.maxAndIndex(&maxValue, &index);

	// The return is center movement, so subtract center
	index -= MtiPoint(crossNorm.getSize().width/2, crossNorm.getSize().height/2);
  	return {crossNorm, index, maxValue };
}

void AutoTrackSupport::computeTrackMotion(const TrackFrameInfo& previousFrame, const TrackFrameInfo& currentFrame,
	int frameIndex, SceneTracks& activeTracks, const AutoTrackParameters& params)
{
	auto currentProxy = currentFrame.proxyImage;
	auto crossNormThreshold = params.getCrossNormThreshold();

	// usual ratios compute some ratios
	auto rx = double(params.getFullImageSize().width) / double(params.getProxySize().width);
	auto ry = double(params.getFullImageSize().height) / double(params.getProxySize().height);

	// Debugging info
	auto rejectedByNorm = 0;
	auto n = int(activeTracks.size());
	std::mutex loopMutex;
	auto f = [&](int i)
	{
		auto &track = activeTracks[i];
		//track = activeTracks(trackIndex);
		auto endBox = track.boxes.back();

		// Add in the search radius
		auto searchBox = endBox.inflate(params.getProxyRadiusSearch());
		auto searchImage = currentProxy(searchBox, MtiSelectMode::mirror);

		auto xcorrResult = validNormXCorr(track.firstBoxProxyImage, searchImage);
		Ipp32fArray normArray = xcorrResult.P0;
		MtiPoint maxNormPoint = xcorrResult.P1;
		float maxNormValue = xcorrResult.P2;

		if (maxNormValue < crossNormThreshold)
		{
			auto probe = track.firstBoxProxyImage;
			track.active = false;
			rejectedByNorm++;
			return; // continue in loop
		}

		auto newEndBox = endBox.moveRelative(maxNormPoint);

;		// Now compute the subpixel movement.
		// usual ratios compute some ratios

		auto boxIndex = frameIndex - track.startFrameIndex;

		// Yes, the boxIndex can be 0
		if (boxIndex == 0)
		{
			newEndBox.xSubpixelCenter = float(rx*(track.boxes[0].center.x + 0.5f));
			newEndBox.ySubpixelCenter = float(ry*(track.boxes[0].center.y + 0.5f));
		}
		else
		{
			// Now track back
			auto endBoxOriginalImage = currentProxy(newEndBox, MtiSelectMode::mirror);
			xcorrResult = validNormXCorr(endBoxOriginalImage, track.firstBoxSearchBackProxyImage);
			normArray = xcorrResult.P0;
			maxNormPoint = xcorrResult.P1;
			maxNormValue = xcorrResult.P2;

			// If the track back was perfect, maxNormPoint would be zero, reject if too big;
			auto d = std::sqrt((maxNormPoint.y) ^ 2 +  (maxNormPoint.x) ^ 2);
			if (d > params.getTrajectoryMaxDiscrepancy())
			{
				track.active = false;
				return; // continue in loop
			}

			// Create subpixel track for the new box
			auto fullEndBox = newEndBox.expandBox(rx, ry);

			// track using larger box but only search in small area
			// use the radius parameter as the area.
			auto searchRange = params.getBoxRadius();
			auto startFullImage = track.firstBoxFullImage;
			auto subPixelSearchBox = fullEndBox.inflate(searchRange);
			auto subPixelSearchImage = currentFrame.fullGrayImage(subPixelSearchBox, MtiSelectMode::mirror) * (0.5f / currentFrame.averageIntensity);
			auto nxc = subPixelSearchImage.crossCorrNorm(startFullImage, IppiROIShape::ippiROIValid);
	//		MatIO::saveListToMatFile(R"(c:\temp\test.mat)", startFullImage, "startFullImageC", subPixelSearchImage, "searchImageC",nxc, "nxcC");

			nxc.maxAndIndex(&maxNormValue, &maxNormPoint);
			maxNormPoint -= MtiPoint(nxc.getSize().width / 2, nxc.getSize().height / 2);

			// compute nxc maximum
			nxc = nxc.resize(nxc.getSize() * 5);
			nxc.maxAndIndex(&maxNormValue, &maxNormPoint);
			maxNormPoint -= MtiPoint(nxc.getSize().width / 2, nxc.getSize().height / 2);

			newEndBox.xSubpixelCenter = float(fullEndBox.center.x) + maxNormPoint.x / 5.0f;
			newEndBox.ySubpixelCenter = float(fullEndBox.center.y) + maxNormPoint.y / 5.0f;
			
		    // lock so we push correctly
			{
				std::lock_guard<std::mutex> lock(loopMutex);
				track.boxes.push_back(newEndBox);
			}
		}
	};

	IpaStripeStream iss;
	iss << n;
	iss << efu_job(f);
	iss.stripe();
}

void AutoTrackSupport::pruneActiveTracksAddToValidTracks(SceneTracks& activeTracks, SceneTracks& validTracks, int minLength, 
	const AutoTrackParameters& paramsx)
{
	// TODO We should use remove_if and just have one sort
	SceneTracks finishedTracks;
	for (auto &track : activeTracks)
	{
		if (!track.active)
		{
			finishedTracks.push_back(track);
		}
	}

	activeTracks.erase(
		std::remove_if(activeTracks.begin(), activeTracks.end(),
			[](const SceneTrack &t) { return !t.active; }),
		activeTracks.end());

	// Save any valid tracks
	for (auto &track : finishedTracks)
	{
		// reject any track that is less than min
		if (int(track.boxes.size()) < minLength)
		{
			continue;
		}

		// Not known if we really want to do this
		track.active = true;
		validTracks.push_back(track);
	}
}

CenterTrackBoxes AutoTrackSupport::extractDisjointPatches(const SceneTracks& activeTracks,
	const CenterTrackBoxes& patchCenters, const AutoTrackParameters& params)
{
	// ***WARNING*** this assumes all the active tracks end on the same frame index
	// This is true when processing frame by frame 
	CenterTrackBoxes slices;
	for (auto &track : activeTracks)
	{
		slices.push_back(track.boxes.back());
	}

	// we are assuming all boxes are same size
	//auto w = 2 * params.getBoxRadius() + 1;
	//auto h = 2 * params.getBoxRadius() + 1;

	vector<int> goodBoxIndices;
	CenterTrackBoxes validBoxes;
	for (auto index : range(patchCenters.size()))
	{
		//auto &box : patchCenters(index); 
	   // MtiRect p = patchCenters[index].inflate(params.getBoxRadius());
	    MtiRect p = patchCenters[index].inflate(0);
	
		// We could do this MUCH faster by sorting boxes and binary search
		auto good = true;
		for (auto &centerTrackBox : slices)
		{
			auto intersect = centerTrackBox & p;
			if (!intersect.isEmpty())
			{
				good = false;
				break;
			}
		}

		if (good)
		{
			validBoxes.push_back(patchCenters[index]);
		}
	}

	return validBoxes;
}
