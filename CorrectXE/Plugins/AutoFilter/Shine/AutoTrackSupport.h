#pragma once
#ifndef AutoTrackSupportH
#define AutoTrackSupportH

// This really should be sucked into a class but wasn't for historical reasons
#include "AutoTrackingHeaders.h"
#include "SceneTrack.h"

inline std::string testFileName(const std::string &simpleFileName, int frameIndex)
{
	std::ostringstream os;
	os << R"(c:\temp\)" << simpleFileName << "_" << frameIndex + 1 << ".mat";
	return os.str();
}

struct TrackFrameInfo
{
//	Ipp32fArray fullRgbImage;
	Ipp32fArray fullGrayImage;
	Ipp32fArray proxyImage;
	float averageIntensity = 0.0f;
	int frameIndex = -1;
};

class AutoTrackSupport
{
public:
	static Ipp32fArray findPatchMinimums(const Ipp32fArray &inputImage, const AutoTrackParameters &params);
	static CenterTrackBoxes findPatches(const Ipp32fArray &patchMins, const AutoTrackParameters &params);
	static std::pair<Ipp32fArray, float> balanceIntensity(const Ipp32fArray &sourceImage, const AutoTrackParameters &params);
	static SceneTracks makeStartTracks(const TrackFrameInfo &frameInfo, vector<CenterTrackBox> patchBoxes, int frameIndex, const AutoTrackParameters& params);

	static void computeTrackMotion(const TrackFrameInfo &previousFrame, const TrackFrameInfo &currentFrame, int frameIndex, SceneTracks &activeTracks, const AutoTrackParameters& params);
	static void pruneActiveTracksAddToValidTracks(SceneTracks &activeTracks, SceneTracks &validTracks, int minLength, const AutoTrackParameters &params);
	static CenterTrackBoxes extractDisjointPatches(const SceneTracks &activeTracks, const CenterTrackBoxes &patchCenters, const AutoTrackParameters &params);
};

#endif

