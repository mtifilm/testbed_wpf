#pragma once
#ifndef AutoTrackingPreprocessorH
#define AutoTrackingPreprocessorH

#include "Ippheaders.h"
#include "AutoTrackParameters.h"
#include "AutoTrackSupport.h"
#include "PatchMinsMT.h"

class AutoTrackingPreprocessor
{
public:
	AutoTrackingPreprocessor() = default;
	~AutoTrackingPreprocessor() = default;

	void initSceneTracking(int totalFrames)
	{
		_initialized = false;
		_totalFrames = totalFrames;
	}

	// frameArray is a 1 channel image normalized 0-1
	bool processFrame(const Ipp32fArray & frameArray, int frameIndex);

	bool getOpticalFlowAvailable() const { return _opticalFlowAvailable; }
	OpticalFlowData getOpticalFlow() const { return _opticalFlowData; }

private:
	AutoTrackParameters _params;
	bool _initialized = false;
	int _totalFrames = 0;
	int _sceneIndex = 0;

	TrackFrameInfo _currentFrameInfo;
	TrackFrameInfo _previousFrameInfo;
	SceneTracks _activeTracks;
	SceneTracks _validTracks;
	bool _opticalFlowAvailable = false;

	TrackFrameInfo createTrackFrameInfo(const Ipp32fArray &frameArray, int frameIndex) const;
	void processFirstFrame(const Ipp32fArray &frameArray, int frameIndex);
	void processNextFrame(const Ipp32fArray& frameArray, int frameIndex);

	void trackDataToOpticalFlow();
	OpticalFlowData _opticalFlowData;
	PatchMinsMT _patchMinsMT;
};

#endif