#ifndef PatchMinsMTH
#define PatchMinsMTH
#pragma once

#include "Ippheaders.h"

class PatchMinsMT
{
public:
	// 12 works for a 512 x 389 image, 8 processor machine.
	PatchMinsMT(int stripes = 12);
	Ipp32fArray computeMin(const Ipp32fArray &inputImage, const MtiSize &kernelSize);
	~PatchMinsMT();

	int getSlices() const { return _stripes; }
	void setSlices(int value) { _stripes = value; }

private:
	void updateScratchData(const MtiSize &inputSize);

private:
	int _stripes;
	Ipp8uArray _boxFilterScratch[2];
	Ipp32fArray _absDiffScratch[2];
	Ipp32fArray _normArray;
};

#endif


