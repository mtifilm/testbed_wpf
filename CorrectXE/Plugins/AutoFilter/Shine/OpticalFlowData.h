#ifndef OpticalFlowDataH
#define OpticalFlowDataH

#include "MtiIppExtensions.h" // for MtiPoint!
#include <vector>
using std::vector;

// Structure used for the analysis of optical flow
struct OpticalFlowDatum
{
	OpticalFlowDatum() = default;

	OpticalFlowDatum(int index, int startIndex, double x0, double y0, const MtiPoint center) :
		trackIndex(index), sceneStartIndex(startIndex), fullCenterX(float(x0)), fullCenterY(float(y0)), proxyCenterX(center.x), proxyCenterY(center.y)
	{}

	int trackIndex = -1;
	int sceneStartIndex = -1;
	float fullCenterX = -1;
	float fullCenterY = -1;
	int proxyCenterX = -1;
	int proxyCenterY = -1;
};

typedef vector<OpticalFlowDatum> OpticalFlowData;

#endif
