#include "ShineRepairMask.h"
#include "AutoTrackSupport.h"
#include "ImageBinSorter.h"
#include "LabelMarker.h"
#include "IpaStripeStream.h"
#include "HRTimer.h"
#include <numeric>
#include <atomic>
#include <filesystem>

// #define _DEBUG
// This weird function creates a set of tile boxes on the proxy with
// proxyNominalSize that cover the image. It also returns boxes on the
// full image that correspond but do not overlap.
// CenterTrackBoxes ShineRepairMask::createFullSizeTiles()
// {
// auto imageSize = _autoTrackParams.getFullImageSize();
// auto proxySize = _autoTrackParams.getProxySize();
//
// auto boxNominalSize = 2 * _shineParams.getProxyTileBoxRadius() + 1;
//
// // Use Array2D here
// int nx = int(ceil(double(proxySize.width) / boxNominalSize));
// int ny = int(ceil(double(proxySize.height) / boxNominalSize));
// _proxyBoxesSize = { nx, ny };
//
// auto width = imageSize.width;
// auto height = imageSize.height;
//
// auto scaleX = double(imageSize.width) / double(proxySize.width);
// auto scaleY = double(imageSize.height) / double(proxySize.height);
//
// // We should have tiled the centers here and then gone back DUH!
// // Translate the centers to the big images
// vector<int> cx(1, 0);
// for (auto i : range(nx - 1))
// {
// cx.push_back(int(std::round(_proxyBoxes[i].center.x + _proxyBoxes[i + 1].center.x) / 2 * scaleX));
// }
//
// cx.push_back(width);
// vector<int> cw;
// for (auto i : range(cx.size() - 1))
// {
// cw.push_back(cx[i + 1] - cx[i]);
// }
//
// vector<int>cy(1, 0);
// for (auto i : range(ny - 1))
// {
// auto yi = i * nx;
// cy.push_back(int(std::round(_proxyBoxes[yi].center.y + _proxyBoxes[yi + nx].center.y) / 2 * scaleY));
// }
// cy.push_back(height);
//
// vector<int> ch;
// for (auto i : range(cy.size() - 1))
// {
// ch.push_back(cy[i + 1] - cy[i]);
// }
//
// CenterTrackBoxes centers;
// for (auto r : range(ny))
// {
// for (auto c : range(nx))
// {
// auto center = _proxyBoxes[r * nx + c].center;
// center.x = int(center.x * scaleX);
// center.y = int(center.y * scaleY);
// MtiRect box(cx[c], cy[r], cw[c], ch[r]);
// centers.emplace_back(box, center);
// }
// }
//
// return centers;
// }

void ShineRepairMask::setOpticalFlow(const OpticalFlowData& opticalFlowData)
{
   _opticalFlowData = opticalFlowData;

   // Ugly we map first line to a header so fullCenter is imageSize
   MtiSize fullImageSize(int(_opticalFlowData[0].fullCenterX), int(opticalFlowData[0].fullCenterY));

   _autoTrackParams.setFullImageSize(fullImageSize);
   _totalFrames = _opticalFlowData[0].trackIndex;
   if (_totalFrames <= 0)
   {
      THROW_MTI_RUNTIME_ERROR("No total frames");
   }

   std::tie(_opticalFlowTracks, _constantTrack) = createOpticalFlowTracksFromOpticalFlow(opticalFlowData);

   _proxyBoxes = createEqualSizeTiles(_autoTrackParams.getProxySize(), _shineParams.getProxyTileBoxRadius());
   // _fullSizeBoxes = createFullSizeTiles();

   _detectionImages.clear();
   _detectionImages4.clear();
   _observedImages.clear();

   for (auto i = 0; i < 5; i++)
   {
      _detectionImages.emplace_back(fullImageSize);
      _observedImages.emplace_back(MtiSize(fullImageSize, 3));
   }

   for (auto i = 0; i < 4; i++)
   {
      _detectionImages4.emplace_back(fullImageSize);
   }

   _motionMask = Ipp16uArray(fullImageSize);
   _pseudoSdArray = Ipp32fArray(fullImageSize);

   _currentOncePerFrameStuffSceneIndex = -1;
   _currentFidelitySceneIndex = -1;
}

bool ShineRepairMask::pushFrames(const vector<Ipp16uArray> &frames, int startSceneIndex, int repairIndex)
{
   // Needed to fake Kevins ShotInfo
   _processFrameIdx = repairIndex;

   // Delete what we don't need
   auto sceneIndices = neededSceneIndices(repairIndex);

   // TO DO: use find etc..
   vector<int>keysToDelete;
   for (auto&frameMap : _workingFrames)
   {
      auto key = frameMap.first;
      auto it = find(sceneIndices.begin(), sceneIndices.end(), key);
      if (it == sceneIndices.end())
      {
         keysToDelete.push_back(key);
      }
   }

   for (auto key : keysToDelete)
   {
      _workingFrames.erase(key);
   }

   auto sceneIndex = startSceneIndex;
   for (auto&fullRgbFrame : frames)
   {
      auto it = _workingFrames.find(sceneIndex);
      if (it == _workingFrames.end())
      {
         if ((sceneIndex >= 0) && (sceneIndex < _totalFrames))
         {
            _workingFrames.emplace(sceneIndex, createShineFrameInfo(fullRgbFrame, sceneIndex));
         }
      }

      sceneIndex++;
   }

   return areAllFramesPushedForRepairIndex(repairIndex);
}

bool ShineRepairMask::areAllFramesPushedForRepairIndex(int repairIndex) const
{
   auto sceneIndices = neededSceneIndices(repairIndex);

   // See if we have enough
   for (auto index : sceneIndices)
   {
      if (_workingFrames.find(index) == _workingFrames.end())
      {
         return false;
      }
   }

   return true;
}

vector<int>ShineRepairMask::neededSceneIndices(int sceneIndex) const
{
   vector<int>result;
   result.reserve(9);

   // No reason to optimize and to see what is really needed, just assume up to nine are
   for (auto i : range(-4, 5))
   {
      auto k = sceneIndex - i;
      if ((k >= 0) && (k < _totalFrames))
      {
         result.push_back(k);
      }
   }

   return result;
}

Ipp8uArray ShineRepairMask::createTemporalMask(const Ipp32fArray &diffScale, float temporalContrastPos,
   float temporalContrastNeg)
{
   // We use the ContrastPos and ContrastNeg to make sure the scaled diff
   // value is sufficiently large

   // MIN_SCALED_CONTRAST = 2;
   const float MIN_SCALED_CONTRAST = 2;

   // MAX_SCALED_CONTRAST = 10;
   const float MAX_SCALED_CONTRAST = 10;

   // when ContrastVal = 0, ScaledThresh = MIN_SCALED_CONTRAST
   // when ContrastVal = 100, ScaledThresh = MAX_SCALED_CONTRAST
   // SlopeScale = (MAX_SCALED_CONTRAST - MIN_SCALED_CONTRAST) / (100 - 0);
   auto slopeScale = (MAX_SCALED_CONTRAST - MIN_SCALED_CONTRAST) / (100.0f - 0);

   // InterScale = MIN_SCALED_CONTRAST - SlopeScale * 0;
   auto interScale = MIN_SCALED_CONTRAST - slopeScale * 0;

   // Calculate the thresholds for the scaled diff
   // ScaleThreshPos = SlopeScale * TemporalContrastPos + InterScale;
   auto scaleThreshPos = slopeScale * temporalContrastPos + interScale;

   // ScaleThreshNeg = SlopeScale * TemporalContrastNeg + InterScale;
   auto scaleThreshNeg = slopeScale * temporalContrastNeg + interScale;

   // MaskTemporal = double(DiffScale > ScaleThreshPos) - double(DiffScale < -ScaleThreshNeg);
   auto temporalMask = diffScale.compareC(scaleThreshPos, ippCmpGreater);
   auto negativeMask = diffScale.compareC(-scaleThreshNeg, ippCmpLess) / 2;
   temporalMask += negativeMask;

   return temporalMask;
}

template<typename T, typename S>
S convertWithScaleMT(const T &image, float scale)
{
   S result(image.getSize());
   // Check size, th 5K is out of my ass
   if ((image.area() / 4) < 100 * 500)
   {
      result <<= image;
      result *= scale;
      return result;
   }

   IpaStripeStream iss;
   iss << image.getRoi();

   // We might be going from a bigger image into a smaller
   auto offset = image.getRoi().tl();
   auto f = [&](const MtiRect & roi)
   {
      auto stripe = image(roi);
      auto resultStripe = result(stripe.getRoi() - offset);
      resultStripe <<= stripe;
      resultStripe *= scale;
   };

   iss << efu_roi(f);
   iss.stripe(4);

   return result;
}

ShineFrameInfo ShineRepairMask::createShineFrameInfo(const Ipp16uArray &rgbImage, int index) const
{
   ShineFrameInfo shineFrameInfo;
   CHRTimer hrt;
   float scale = 1.0f / ((1 << rgbImage.getOriginalBits()) - 1);
   shineFrameInfo.fullRgbImage = Ipp32fArray(rgbImage.getSize()); // <<= rgbImage;

   // This is a bug in <<= which should carry original bits
   shineFrameInfo.fullRgbImage.setOriginalBits(rgbImage.getOriginalBits());

   // shineFrameInfo.fullRgbImage *= scale;
   // std::cout << "Index " << index << ", time " << hrt << std::endl;

   IpaStripeStream iss;
   iss << rgbImage;

   // DANGER, this only works if the the images start at same Top Left hand point
   MTIassert(rgbImage.getRoi().tl() == MtiPoint(0, 0));
   auto f = [&](Ipp16uArray & image)
   {
      auto outStripe = shineFrameInfo.fullRgbImage(image.getRoi());
      outStripe <<= image;
      outStripe *= scale;
   };

   iss << efu_16u(f);
   iss.stripe();
   // std::cout << "MT time " << hrt << std::endl;

   shineFrameInfo.fullGrayImage = shineFrameInfo.fullRgbImage.toGray();
   shineFrameInfo.sceneIndex = index;
   auto proxyBeforeScale = shineFrameInfo.fullGrayImage.resizeAntiAliasing(_autoTrackParams.getProxySize());
   std::tie(shineFrameInfo.proxyImage, shineFrameInfo.averageIntensity) =
      AutoTrackSupport::balanceIntensity(proxyBeforeScale, _autoTrackParams);

   return shineFrameInfo;
}

// Because this is called in a loop, we need to avoid reallocation (or use compiler optimization on stack)
// So we must give up functional to make it efficient

static inline bool findSceneIndicesForTrack(const OpticalFlowTrack &oft, int sceneIndex, RequiredIndices &ri)
{
   if ((oft.startSceneIndex > sceneIndex) || (sceneIndex > oft.getInclusiveEndSceneIndex()))
   {
      return false;
   }

   ri.sceneIndex = sceneIndex;
   auto deltaIndexFromStart = sceneIndex - oft.startSceneIndex;
   auto deltaIndexFromEnd = oft.getInclusiveEndSceneIndex() - sceneIndex;

   // See if we have room on both sides of track
   if ((deltaIndexFromStart > MIDDLE_SCENE) && (deltaIndexFromEnd >= (DELTA_BOX_SEG_SIZE - MIDDLE_SCENE)))
   {
      for (auto i = 0; i < MIDDLE_SCENE; i++)
      {
         ri.boxOffset[i] = i - MIDDLE_SCENE;
      }

      for (auto i = MIDDLE_SCENE; i < DELTA_BOX_SEG_SIZE; i++)
      {
         ri.boxOffset[i] = i - MIDDLE_SCENE + 1;
      }

      return true;
   }

   if (deltaIndexFromStart <= MIDDLE_SCENE)
   {
      for (auto i = 0; i < deltaIndexFromStart; i++)
      {
         ri.boxOffset[i] = i - deltaIndexFromStart;
      }

      for (auto i = deltaIndexFromStart; i < DELTA_BOX_SEG_SIZE; i++)
      {
         ri.boxOffset[i] = i - deltaIndexFromStart + 1;
      }

      return true;
   }

   // Only one case left, not enough track positions to the right
   // (Tracks are > DELTA_SEGMENT_SIZE)
   for (auto i = 0; i < deltaIndexFromEnd; i++)
   {
      ri.boxOffset[DELTA_BOX_SEG_SIZE - i - 1] = deltaIndexFromEnd - i;
   }

   for (auto i = deltaIndexFromEnd; i < DELTA_BOX_SEG_SIZE; i++)
   {
      ri.boxOffset[DELTA_BOX_SEG_SIZE - i - 1] = deltaIndexFromEnd - i - 1;
   }

   return true;
}

// Local defs to avoid exposing unneeded implementation structure
typedef vector<TestDelta>DeltaSegment;

DeltaSegment rollingIndex(OpticalFlowTrack track, int sceneIndex)
{
   DeltaSegment result;
   result.resize(4);

   auto startSceneIndex = track.startSceneIndex;

   RequiredIndices requiredIndices;
   findSceneIndicesForTrack(track, sceneIndex, requiredIndices);

   // now find the deltas
   auto startBoxIndex = sceneIndex - startSceneIndex;

   for (auto i : range(4))
   {
      TestDelta delta;
      delta.sceneIndex = requiredIndices.getSceneIndex(i);

      // di is delta of the frames
      auto di = delta.sceneIndex - sceneIndex;
      delta.fullDeltaX = track.subPixelTrack[di + startBoxIndex].x - track.subPixelTrack[startBoxIndex].x;
      delta.fullDeltaY = track.subPixelTrack[di + startBoxIndex].y - track.subPixelTrack[startBoxIndex].y;

      // Now scale and std::round
      delta.proxyDeltaX = int(track.proxyPixelTrack[di + startBoxIndex].x - track.proxyPixelTrack[startBoxIndex].x);
      delta.proxyDeltaY = int(track.proxyPixelTrack[di + startBoxIndex].y - track.proxyPixelTrack[startBoxIndex].y);

      result[i] = delta;
   }

   return result;
}

ShineFrameInfo &ShineRepairMask::getSceneFrameInfo(int sceneIndex)
{
   auto iter = _workingFrames.find(sceneIndex);
   if (iter == _workingFrames.end())
   {
      THROW_MTI_RUNTIME_ERROR("Attempt to ask for a frame not pushed");
   }

   return _workingFrames[sceneIndex];
}

bool deltasAreTheSame(const DeltaSegment& deltaSegment, vector < vector < TestDelta >> & processedDeltas)
{
   for (auto&deltas : processedDeltas)
   {
      auto same = true;
      for (auto i : range(deltas.size()))
      {
         if ((deltas[i].proxyDeltaX != deltaSegment[i].proxyDeltaY) ||
            (deltas[i].proxyDeltaY != deltaSegment[i].proxyDeltaY))
         {
            same = false;
            continue;
         }
      }

      // We have a match
      if (same)
      {
         return true;
      }
   }

   processedDeltas.push_back(deltaSegment);
   return false;
}

// This creates tiles of equal size that cover the image but overlap
CenterTrackBoxes ShineRepairMask::createEqualSizeTiles(const MtiSize& imageSize, int boxRadius) const
{
   auto ulx = boxRadius;
   auto uly = boxRadius;
   auto width = imageSize.width;
   auto height = imageSize.height;
   int boxNominalSize = 2 * boxRadius + 1;
   int nx = int(std::ceil(double(width) / boxNominalSize));
   int ny = int(std::ceil(double(height) / boxNominalSize));
   auto widthInc = double(width) / (nx);
   auto heightInc = double(height) / (ny);

   CenterTrackBoxes boxes;

   auto yt = uly;
   MtiSize boxSize(boxRadius, boxRadius);
   for (auto r : range(ny))
   {
      auto yl = int(uly + std::round(heightInc * r));
      for (auto c : range(nx))
      {
         auto x = int(ulx + std::round(widthInc * c));
         boxes.emplace_back(MtiPoint(x, yl), boxSize);
      }
   }

   return boxes;
}

std::pair<OpticalFlowTracks, OpticalFlowTrack>ShineRepairMask::createOpticalFlowTracksFromOpticalFlow
(const OpticalFlowData &opticalFlowData) const
{
   OpticalFlowTracks opticalFlowTracks;
   size_t i = 1;
   auto currentTrackIndex = -1;
   auto ofTrackNumber = 0;
   while (i < opticalFlowData.size())
   {
      // We are at a new scene
      auto ofd = opticalFlowData[i++];
      if (ofd.trackIndex != currentTrackIndex)
      {
         opticalFlowTracks.emplace_back(ofd.sceneStartIndex, IppiPoint_32f
            { Ipp32f(ofd.fullCenterX), Ipp32f(ofd.fullCenterY) }, IppiPoint_32f
            { Ipp32f(ofd.proxyCenterX), Ipp32f(ofd.proxyCenterY) });
         currentTrackIndex = ofd.trackIndex;
         opticalFlowTracks.back().indexIntoContainer = ofTrackNumber;
         ofTrackNumber++;
      }
      else
      {
         opticalFlowTracks.back().addPoint({ Ipp32f(ofd.fullCenterX), Ipp32f(ofd.fullCenterY) },
            { Ipp32f(ofd.proxyCenterX), Ipp32f(ofd.proxyCenterY) });
      }
   }

   // Create a constant track
   OpticalFlowTrack constantTrack(0, { 0, 0 }, { 0, 0 });
   opticalFlowTracks.back().indexIntoContainer = -2;
   for (auto j : range(_totalFrames - 1))
   {
      constantTrack.addPoint({ 0, 0 }, { 0, 0 });
   }

   return
   {
      opticalFlowTracks, constantTrack
   };
}

void ShineRepairMask::doOncePerFrameStuff(int repairSceneIndex)
{
   // Compute the observation contrast image if it hasn't already been done.
   if (_currentOncePerFrameStuffSceneIndex == repairSceneIndex)
   {
      // Already done!
      return;
   }

//   DBTIMER(doOncePerFrameStuff);

   if (!areAllFramesPushedForRepairIndex(repairSceneIndex))
   {
      THROW_MTI_RUNTIME_ERROR("Not enough frames to do repair")
   }

   MTIassert(_processFrameIdx == repairSceneIndex);
   CHRTimer hrt;

   createShotInfoBox();
   DBTRACE2("createShotInfoBox", hrt);

   auto SMOOTH_RAD = 4;
   _box5 = computePixelMotionOncePerFrame(_z5, SMOOTH_RAD);
   DBTRACE2("pixelMotion", hrt);

   std::tie(_diffRaw, _diffScale) = computeDiffsOncePerFrame(_z5); // blob1a
   DBTRACE2("computeDiffs", hrt);

   _currentOncePerFrameStuffSceneIndex = repairSceneIndex;
}

void ShineRepairMask::findDefectivePixels(Ipp8uArray defectivePixelMask, bool applyFidelityMask)
{
//   DBTIMER(findDefectivePixels);

   // Build defective pixel mask
   // Is IPP compare faster, maybe Also few 1's so set to zero
   defectivePixelMask.zero();

   auto hc = _shineInteractiveParams.highScaledContrast;
   auto lc = _shineInteractiveParams.lowScaledContrast;

   auto scaledContrastIterator = _scaledContrastImage.begin();

   if (applyFidelityMask)
   {
      auto fidelityMaskIterator = _fidelityMask.begin();

      for (auto&b : defectivePixelMask)
      {
         auto sc = *scaledContrastIterator++;
         auto fm = *fidelityMaskIterator++;

         if (((sc > (hc / 2)) || (sc < (lc / 2))) && (fm == 0))
         {
            b = 1;
         }
      }
   }
   else
   {
      for (auto&b : defectivePixelMask)
      {
         auto sc = *scaledContrastIterator++;

         if ((sc > (hc / 2)) || (sc < (lc / 2)))
         {
            b = 1;
         }
      }
   }
}

void ShineRepairMask::importDetectionMask(const Ipp8uArray &importDefectivePixelMask, bool applyFidelityMask)
{
   // Need to mask the imported mask by the fidelity mask!
   if (_detectionMask.getSize() != importDefectivePixelMask.getSize())
   {
      _detectionMask = Ipp32fArray(importDefectivePixelMask.getSize());
   }

   // Assume the importDefectivePixelMask mask is mostly zeros, which makes
   // clearing the array first faster!
   _detectionMask.zero();

   auto importDefectivePixelMaskIterator = importDefectivePixelMask.cbegin();
   if (applyFidelityMask)
   {
      auto fidelityMaskIterator = _fidelityMask.begin();
      for (auto&b : _detectionMask)
      {
         auto idpm = *importDefectivePixelMaskIterator++;
         auto fm = *fidelityMaskIterator++;
         if (idpm && !fm)
         {
            b = 1;
         }
      }
   }
   else
   {
      for (auto&b : _detectionMask)
      {
         if (*importDefectivePixelMaskIterator++)
         {
            b = 1;
         }
      }
   }
}

//Ipp16uArray ShineRepairMask::createRepairedImage(int sceneIndex)
//{
//   MTIassert(sceneIndex == _processFrameIdx);
//   auto repairedImage = getSceneFrameInfo(sceneIndex).fullRgbImage.duplicate();
//
//   // Bug in duplicate. when <<= is fixed this will work
//   auto originalBits = getSceneFrameInfo(sceneIndex).fullRgbImage.getOriginalBits();
//
//   auto medianImage = _observedRepairImage;
//
//   for (auto r = 0; r < repairedImage.getHeight(); r++)
//   {
//      auto rip = repairedImage.getRowPointer(r);
//      auto mip = medianImage.getRowPointer(r);
//      auto repairMaskRowPtr = _repairMask.getRowPointer(r);
//      for (auto c = 0; c < repairedImage.getWidth(); c++)
//      {
//         auto repairMaskValue = *repairMaskRowPtr++;
//         if (repairMaskValue > 0)
//         {
//            rip[0] = rip[0] * (1 - repairMaskValue) + repairMaskValue * mip[0];
//            rip[1] = rip[1] * (1 - repairMaskValue) + repairMaskValue * mip[1];
//            rip[2] = rip[2] * (1 - repairMaskValue) + repairMaskValue * mip[2];
//         }
//         else if (repairMaskValue < 0)
//         {
//            rip[0] = 1;
//            rip[1] = 0;
//            rip[2] = 0;
//         }
//
//         rip += 3;
//         mip += 3;
//      }
//   }
//
//   auto scale = (1 << originalBits);
//
//   Ipp16uArray repairedImage16u(repairedImage.getSize());
//
//   IpaStripeStream iss;
//   iss << repairedImage;
//
//   // DANGER, this only works if the the images start at same Top Left hand point
//   MTIassert(repairedImage.getRoi().tl() == MtiPoint(0, 0));
//   auto f = [&](Ipp32fArray & image)
//   {
//      auto roi = image.getRoi();
//      image *= Ipp32f(scale);
//      repairedImage16u(roi) <<= image;
//   };
//
//   iss << efu_32f(f);
//   iss.stripe();
//
//   return repairedImage16u;
//}

Ipp16uArray ShineRepairMask::repairImage(int sceneIndex, const ShineInteractiveParams &shineInteractiveParams,
   const Ipp8uArray &importedDefectivePixelMask, bool importedMaskNeedsExtraDilation,
   bool importedMaskNeedsFidelityMasking)
{
   // Should be first and only place this is set
   _processFrameIdx = sceneIndex;

   _shineInteractiveParams = shineInteractiveParams;

   DBTRACE(shineInteractiveParams.highScaledContrast);
   DBTRACE(shineInteractiveParams.lowScaledContrast);
   DBTRACE(shineInteractiveParams.highSpatialContrast);
   DBTRACE(shineInteractiveParams.lowSpatialContrast);
   DBTRACE(shineInteractiveParams.fidelity);
   DBTRACE(shineInteractiveParams.minSize);
   DBTRACE(shineInteractiveParams.maxSize);

   // Prerequisites
   doOncePerFrameStuff(sceneIndex);

   // Do the contrast

   CHRTimer repairImage;
   CHRTimer totalTime;

   auto temporalContrastPos = float(_shineInteractiveParams.highScaledContrast);
   auto temporalContrastNeg = float(_shineInteractiveParams.lowScaledContrast);
   auto temporalMask = createTemporalMask(_diffScale, temporalContrastPos, temporalContrastNeg);

   auto spatialContrastPos = _shineInteractiveParams.highSpatialContrast;
   auto spatialContrastNeg = _shineInteractiveParams.lowSpatialContrast;

   auto MIN_SIZE = int(_shineInteractiveParams.minSize);
   auto MAX_SIZE = int(_shineInteractiveParams.maxSize);

   auto spatialMask = !importedDefectivePixelMask.isEmpty() ?
      createSpatialMask(importedDefectivePixelMask * 255, 1, MAX_SIZE, 1, 1) : // blob1b
      createSpatialMask(temporalMask, MIN_SIZE, MAX_SIZE, spatialContrastPos, spatialContrastNeg); // blob1b
   DBTRACE2("createSpatialMask", repairImage);

   auto box4 = createDetectionMask(_box5, spatialMask); // blob2
   DBTRACE2("createBox4Motions", repairImage);
   auto motionTolerance = float(_shineInteractiveParams.fidelity);

   Ipp8uArray motionMask;
   // if (motionTolerance < 100)
   // {
   motionMask = createMotionMask(box4, motionTolerance); // blob3
   // }
   // else
   // {
   // motionMask = Ipp8uArray(spatialMask.getSize());
   // motionMask.zero();
   // }

   DBTRACE2("createMotionMask", repairImage);

   auto processMask = createProcessMask(spatialMask, motionMask); // blob4

   DBTRACE2("createProcessMask", repairImage);

   std::tie(_observedRepairImage, _repairMask) = createRepairedImage(box4, processMask); // blob5
   DBTRACE2("createRepairedImage", repairImage);

   auto originalBits = getSceneFrameInfo(sceneIndex).fullRgbImage.getOriginalBits();
   auto scale = (1 << originalBits);

   Ipp16uArray repairedImage16u(_observedRepairImage.getSize());

   IpaStripeStream iss;
   iss << _observedRepairImage;

   // DANGER, this only works if the the images start at same Top Left hand point
   // which is true if it is 0,0
   MTIassert(_observedRepairImage.getRoi().tl() == MtiPoint(0, 0));
   auto f = [&](Ipp32fArray & image)
   {
      auto roi = image.getRoi();
      image *= Ipp32f(scale);
      repairedImage16u(roi) <<= image;
   };

   iss << efu_32f(f);
   iss.stripe();

   DBTRACE(totalTime);
//    MatIO::saveListToMatFile("C:\\temp\\masks.mat", spatialMask, "spatialMask");
//   // MatIO::appendListToMatFile("C:\\temp\\masks.mat", _z5, "Z5Array");
//    AppendVariablesToMatFile1("C:\\temp\\masks.mat", temporalMask);
//    AppendVariablesToMatFile1("C:\\temp\\masks.mat", motionMask);
//    AppendVariablesToMatFile1("C:\\temp\\masks.mat", processMask);

   return repairedImage16u;

}

Ipp8uArray ShineRepairMask::getDefectivePixelMap(int sceneIndex, const ShineInteractiveParams &shineInteractiveParams)
{
   // _shineInteractiveParams = shineInteractiveParams;
   //
   // // Prerequisite
   // // NOTE: We don NOT apply the fidelity mask to the defective pixel mask.
   // // It will get applied later when repairImage() is called.
   // createObservationContrastImage(sceneIndex);
   //
   // // Create and return the defective pixel map.
   // Ipp8uArray defectivePixelMask(_scaledContrastImage.getSize());
   // findDefectivePixels(defectivePixelMask, false);
   // return defectivePixelMask;
   return Ipp8uArray();
}

static OpticalFlowTracks FindSelectTracksThroughFrameIndex(const OpticalFlowTracks &opticalFlow, int sceneIndex)
{
   OpticalFlowTracks result(opticalFlow.size());

   auto it = std::copy_if(opticalFlow.begin(), opticalFlow.end(), result.begin(),
      [sceneIndex](const OpticalFlowTrack & track)
      {return (track.startSceneIndex <= sceneIndex) && (sceneIndex <= track.getInclusiveEndSceneIndex()); });

   result.resize(std::distance(result.begin(), it)); // shrink container to new size

   return result;
}

template<class T>
T pixelByPixelSd(vector<T> &planes)
{
   T meanImage;
   const auto n = int(planes.size());

   meanImage <<= planes[0];
   for (auto i = 1; i < n; i++)
   {
      meanImage += planes[i];
   }

   meanImage /= float(n);

   T sd;
   sd <<= planes[0];
   sd -= planes[0];
   sd *= sd;

   T temp;
   for (auto i = 1; i < n; i++)
   {
      temp <<= planes[i];
      temp -= meanImage;
      temp *= temp;
      sd += temp;
   }

   return sd;
}

////////////////////////////////// Kevin's blob code

template<class T>
T legalize(T value, T minVal, T maxVal) { return std::min<T>(maxVal, std::max<T>(minVal, value)); }

void ShineRepairMask::createShotInfoBox()
{
   // Not Kevin code, we use a member variable here
   _shotInfoBoxes.clear();
   ShotInfoBox zeroMotionShotInfoBox;

   auto tracksAtFrameIndex = FindSelectTracksThroughFrameIndex(_opticalFlowTracks, _processFrameIdx);

   // Start of Kevin code

   // frameIndex = ShotInfo.ProcessFrameIdx;
   auto frameIndex = _processFrameIdx;
   //
   // %  The first BOX is the zeros motion offset.  This is the artificial
   // %  offset that is applied throughout the frame
   //
   // % the five nearest frames
   // iIdx0 = frameIndex-2;
   auto iIdx0 = frameIndex - 2;

   // iIdx1 = frameIndex+2;
   auto iIdx1 = frameIndex + 2;

   //
   // % legalize the frames
   // while iIdx0 < 1
   // iIdx0 = iIdx0 + 1;
   // iIdx1 = iIdx1 + 1;
   // end
   while (iIdx0 < 0)
   {
      iIdx0 = iIdx0 + 1;
      iIdx1 = iIdx1 + 1;
   }

   // while iIdx1 > numel(ShotInfo.FRAMES)
   // iIdx1 = iIdx1-1;
   // iIdx0 = iIdx0-1;
   // end
   while (iIdx1 >= _totalFrames)
   {
      iIdx0 = iIdx0 - 1;
      iIdx1 = iIdx1 - 1;
   }

   // ShotInfo.BOX{1}.Frame = iIdx0:iIdx1;
   // ShotInfo.BOX{1}.Offset = zeros(5,2);  % (x,y) offset
   for (auto i : range(5))
   {
      zeroMotionShotInfoBox.frame[i] = iIdx0 + i;
      zeroMotionShotInfoBox.offset[i] =
      { 0, 0 };
      zeroMotionShotInfoBox.center.emplace_back(-1, -1);
   }

   // ShotInfo.BOX{1}.Center = [-1, -1];  % artificial center
   _shotInfoBoxes.push_back(zeroMotionShotInfoBox);

   // % extract the legitimate box centers in this frame
   // %   BoxCen:  high resolution boxes
   // %   boxCen:  low resolution boxes
   // %   flowIndex:  frames that make up the local flow
   // %   flowFlag:  flag to indicate valid or artificial flow

   // for iTrk = 1:numel(tracksAtFrameIndex)
   for (size_t iTrk = 0; iTrk < tracksAtFrameIndex.size(); iTrk++)
   {
      ShotInfoBox shotInfoBox;
      // % find the five nearest frames
      // iIdx = frameIndex - tracksAtFrameIndex(iTrk).startFrameIndex + 1;
      auto iIdx = frameIndex - tracksAtFrameIndex[iTrk].startSceneIndex;

      // iIdx0 = iIdx-2;
      // iIdx1 = iIdx+2;
      iIdx0 = iIdx - 2;

      // % legalize the frames
      // while iIdx0 < 1
      // iIdx0 = iIdx0 + 1;
      // iIdx1 = iIdx1 + 1;
      // end
      iIdx0 = std::max<int>(0, iIdx0);

      // while iIdx1 > numel(tracksAtFrameIndex(iTrk).boxes)
      // iIdx1 = iIdx1-1;
      // iIdx0 = iIdx0-1;
      // end
      iIdx0 = std::min<int>(int(tracksAtFrameIndex[iTrk].subPixelTrack.size()) - 5, iIdx0);

      // BoxCen = zeros(5,2);
      IppiPoint_32f boxCen[5] =
      { {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0} };

      // for i = iIdx0:iIdx1
      for (auto i = 0; i < 5; i++)
      {
         boxCen[i] =
         { tracksAtFrameIndex[iTrk].subPixelTrack[i + iIdx0].x, tracksAtFrameIndex[iTrk].subPixelTrack[i + iIdx0].y };
         // end
      }

      // %  Offset = std::round(BoxCen - BoxCen(iIdx-iIdx0+1,:));
      for (auto i = 0; i < 5; i++)
      {
         shotInfoBox.offset[i].x = int(std::round(boxCen[i].x - boxCen[iIdx - iIdx0].x));
         shotInfoBox.offset[i].y = int(std::round(boxCen[i].y - boxCen[iIdx - iIdx0].y));
         // end
      }

      // Center = std::round(BoxCen(iIdx-iIdx0+1,:));
      MtiPoint center =
      { int(std::round(boxCen[iIdx - iIdx0].x)), int(std::round(boxCen[iIdx - iIdx0].y)) };

      // IdxList = (iIdx0:iIdx1) + tracksAtFrameIndex(iTrk).startFrameIndex-1;
      // Frame = IdxList;
      for (auto i = 0; i < 5; i++)
      {
         shotInfoBox.frame[i] = iIdx0 + i + tracksAtFrameIndex[iTrk].startSceneIndex;
      }

      // % does this BOX already exist?
      // ExistFlag = false;
      auto existFlag = false;
      // for iBox = 1:numel(ShotInfo.BOX)
      for (size_t boxIndex = 0; boxIndex < _shotInfoBoxes.size(); boxIndex++)
      {
         auto &testShotInfoBox = _shotInfoBoxes[boxIndex];

         // OffsetDiff = sum(sum(abs(ShotInfo.BOX{iBox}.Offset - Offset)));
         // FrameDiff = sum(abs(ShotInfo.BOX{iBox}.Frame - Frame));
         auto sameMotion = true;
         // Note: frame and center sizes are fixed at 5
         for (auto i = 0; i < 5; i++)
         {
            if (testShotInfoBox.frame[i] != shotInfoBox.frame[i])
            {
               sameMotion = false;
               break;
            }

            if ((testShotInfoBox.offset[i].x != shotInfoBox.offset[i].x) ||
               (testShotInfoBox.offset[i].y != shotInfoBox.offset[i].y))
            {
               sameMotion = false;
               break;
            }
         }

         // if (OffsetDiff + FrameDiff) == 0
         if (sameMotion)
         {
            // % this motion already exist in the list
            // ExistFlag = true;
            existFlag = true; // Yes, I know this is the same as
            // CenterDiff = sum(abs(ShotInfo.BOX{iBox}.Center(1,:) - [-1,-1]));
            auto centerDiffZero = (testShotInfoBox.center[0].x == -1) && (testShotInfoBox.center[0].y == -1);
            // if CenterDiff == 0
            if (centerDiffZero)
            {
               // % this motion is already addressed by the
               // % artificial zero motion
               // else
            }
            else
            {
               // % this offset already exists.  Add to the list of centers
               // ShotInfo.BOX{iBox}.Center(end+1,:) = Center;
               testShotInfoBox.center.push_back(center);
               // end
            }
            // end
         }
         // end
      }
      //
      // if ExistFlag == false
      if (!existFlag)
      {
         // % we have not seen this offset yet, so add it
         // iBox = numel(ShotInfo.BOX) + 1;
         // ShotInfo.BOX{iBox}.Frame = Frame;
         // ShotInfo.BOX{iBox}.Offset = Offset;
         // ShotInfo.BOX{iBox}.Center = Center;
         // end
         MTIassert(shotInfoBox.center.size() == 0);
         shotInfoBox.center.push_back(center);
         _shotInfoBoxes.push_back(shotInfoBox);
      }
   }

   // end
}

template<class T, size_t N>
void legalizeIndex(T(&v)[N])
{
   auto r = *std::min_element(std::begin(v), std::end(v));
   if (r < 0)
   {
      for (auto k = 0; k < N; k++)
      {
         v[k] -= r;
      }
   }
}

template<class T, size_t N>
void legalizeIndex(T(&v)[N], int mx)
{
   auto r = *std::max_element(std::begin(v), std::end(v));
   if (r >= mx)
   {
      for (auto k = 0; k < N; k++)
      {
         v[k] += mx - r - 1;
      }
   }
}

// pixelMotion
Ipp16uArray ShineRepairMask::computePixelMotionOncePerFrame(MtiPlanar<Ipp32fArray> &Z5, int smoothRadius)
{
   // CHRTimer pixelMotion;
   // %
   // %  This function assigns a BOX ID to each pixel and stores the result in
   // %  BOX5.  The 5 frames (including the target frame) are motion corrected and
   // %  placed in Z5.  The 4 frames (not including the target frame) are motion
   // %  corrected and placed in Z4.
   //
   // NOTE: ShotInfo is broken into three pieces
   // ShineFrameInfo -> Contains info about frames
   // _processFrameIdx -> ShotInfo.ProcessFrameIdx
   // _shotInfoBoxes -> ShotInfo.BOX that is Info about the boxes around repair frame
   //
   // Furthermore, Z5 is passed in to avoid reallocation
   // This is done for historical reasons but probably should have been done differently
   //
   // frameIndex = ShotInfo.ProcessFrameIdx;
   auto frameIndex = _processFrameIdx;
   //
   // nX = ShotInfo.FullSize(2);
   auto fullSize = getSceneFrameInfo(frameIndex).fullGrayImage.getSize(); // Note: 1 channel
   auto nX = fullSize.width;

   // nY = ShotInfo.FullSize(1);
   auto nY = fullSize.height;

   // % create the 5 replacement frames
   // Z5 = zeros(nY,nX,5);
   if (Z5.getPlanarSize() != MtiSize(fullSize, 5))
   {
      Z5.clear();
      Z5 = MtiPlanar<Ipp32fArray>({ fullSize, 5 });
   }

   // MSE = zeros(nY,nX);
   Ipp32fArray MSE(fullSize);
   MSE.zero();

   // BOX5 = ones(nY,nX);
   Ipp16uArray BOX5({ nX, nY });
   BOX5.zero(); // Matlab starts at 1, C++ at 0

   //
   //
   // % the SMOOTH_RAD can be 0 or larger.  Larger values will reduce effect of
   // % grain noise on motion assignment.
   // MSE_Smooth = ones(2*SMOOTH_RAD+1, 2*SMOOTH_RAD+1);
   Ipp32fArray MSE_Smooth({ 2 * smoothRadius + 1, 2 * smoothRadius + 1 });
   MSE_Smooth.set({ 1.0f / MSE_Smooth.area() });

   //
   // % initialize MSE5 with the zero offset
   // fullMean = zeros(nY,nX);
   Ipp32fArray fullMean(fullSize);
   Ipp32fArray fullDiff(fullSize);

   // for i = 1:5
   // idx = ShotInfo.BOX{1}.Frame(i);
   // Z5(:,:,i) = ShotInfo.Y{idx};
   // fullMean = fullMean + ShotInfo.Y{idx};
   // end
   auto fmse = [&](const MtiRect roi)
   {
      auto Z5roi = Z5(roi);
      auto fullMeanRoi = fullMean(roi);
      auto fullDiffRoi = fullDiff(roi);
      auto MSEroi = MSE(roi);

      Z5roi[0] <<= getSceneFrameInfo(_shotInfoBoxes[0].frame[0]).fullGrayImage(roi);
      fullMeanRoi <<= Z5roi[0];
      for (auto i = 1; i < 5; i++)
      {
         Z5roi[i] <<= getSceneFrameInfo(_shotInfoBoxes[0].frame[i]).fullGrayImage(roi);
         fullMeanRoi += Z5roi[i];
      }

      // fullMean = fullMean / numel(ShotInfo.BOX{1}.Frame);
      fullMeanRoi /= 5.0f; // Always 5

      // We can use = not duplicate() here because the temp array on the right
      fullDiffRoi <<= Z5roi[0];
      fullDiffRoi -= fullMeanRoi;
      fullDiffRoi *= fullDiffRoi; // Saves a copy
      MSEroi += fullDiffRoi;
      for (auto i = 1; i < 5; i++)
      {
         // We can us = not <<= here because the temp array on the right
         fullDiffRoi <<= Z5roi[i];
         fullDiffRoi -= fullMeanRoi;
         fullDiffRoi *= fullDiffRoi; // Saves a copy
         MSEroi += fullDiffRoi;
      }

      // MSE = sqrt(MSE/numel(ShotInfo.BOX{1}.Frame));
      MSEroi /= 5;
      MSEroi.sqrtInPlace();
   };

   IpaStripeStream iss0;
   iss0 << getSceneFrameInfo(_shotInfoBoxes[0].frame[0]).fullGrayImage.getRoi();
   iss0 << efu_roi(fmse);
   iss0.stripe();

   // MSE = conv2(MSE, MSE_Smooth, 'same');
   smoothRadius = 2 * smoothRadius + 1;
   MSE = MSE.applyBoxFilter({ smoothRadius, smoothRadius });
   //
   // % enlarge the box for search purposes

   // boxRad = std::round(SEARCH_FACTOR * proxyBoxWidth * ShotInfo.FACTOR_SCALE);
   auto SEARCH_FACTOR = 3.0;
   auto proxyBoxWidth = _autoTrackParams.getBoxRadius() + 0.5f;
   auto scaleX = _autoTrackParams.getScaleX();
   auto boxRad = int(std::round(SEARCH_FACTOR * proxyBoxWidth * scaleX));

   // Mask = zeros(nY,nX);
   // Apparent mask was used for debugging
   // for iBox = 2:numel(ShotInfo.BOX)
   // for (auto iBox : range(1, int(_shotInfoBoxes.size())))

   auto f = [&](int iBox)
   {

      // % motion defined by this box
      // Frame = ShotInfo.BOX{iBox}.Frame;
      auto &shotInfoBox = _shotInfoBoxes[iBox];
      auto frame = shotInfoBox.frame;

      // xoff = ShotInfo.BOX{iBox}.Offset(:,1);
      // yoff = ShotInfo.BOX{iBox}.Offset(:,2);
      int xoff[5];
      int yoff[5];

      for (auto i = 0; i < 5; i++)
      {
         xoff[i] = int(shotInfoBox.offset[i].x);
         yoff[i] = int(shotInfoBox.offset[i].y);
      }

      // % Where does the current frame land in the five frame trajectory
      // iCurr = find(Frame == frameIndex);
      auto iCurr = std::distance(frame.begin(), std::find(frame.begin(), frame.end(), frameIndex));

      // % where in the full resolution image can this motion be applied
      // for iCenter = 1:size(ShotInfo.BOX{iBox}.Center,1)
      // for (auto iCenter : range(shotInfoBox.center.size()))
      for (auto center : shotInfoBox.center)
      {
         // Center = ShotInfo.BOX{iBox}.Center(iCenter,:);
         // % define the search region for this box
         // x0 = zeros(5,1); x1 = zeros(5,1); y0 = zeros(5,1); y1 = zeros(5,1);
         int x0[5], x1[5], y0[5], y1[5];
         // for j = 1:5
         for (auto j = 0; j < 5; j++)
         {
            // x0(j) = Center(1) - BoxRad + xoff(j);
            x0[j] = center.x - boxRad + xoff[j];
            // x1(j) = Center(1) + BoxRad + xoff(j);
            x1[j] = center.x + boxRad + xoff[j];
            // y0(j) = Center(2) - BoxRad + yoff(j);
            y0[j] = center.y - boxRad + yoff[j];
            // y1(j) = Center(2) + BoxRad + yoff(j);
            y1[j] = center.y + boxRad + yoff[j];
            // end
         }

         //
         // % legalize the regions
         // if min(x0) < 1
         // delta = 1-min(x0);
         // x0 = x0 + delta;
         // end
         legalizeIndex(x0);
         //
         // if max(x1) > nX
         // delta = nX-max(x1);
         // x1 = x1 + delta;
         // end
         legalizeIndex<int>(x1, nX);
         // if min(y0) < 1
         // delta = 1-min(y0);
         // y0 = y0 + delta;
         // end
         legalizeIndex(y0);
         // if max(y1) > nY
         // delta = nY-max(y1);
         // y1 = y1 + delta;
         // end
         legalizeIndex<int>(y1, nY);
         //
         // xyDat = zeros(y1(1)-y0(1)+1,x1(1)-x0(1)+1,5);
         vector<Ipp32fArray>xyDat(5); // Will be set later
         //
         // % extract the five regions
         // for j = 1:5
         // xx = x0(j):x1(j);
         // yy = y0(j):y1(j);
         // xyDat(:,:,j) = ShotInfo.Y{Frame(j)}(yy,xx);
         // end
         for (auto j = 0; j < 5; j++)
         {
            MtiRect roi(x0[j], y0[j], x1[1] - x0[1] + 1, y1[1] - y0[1] + 1);
            // We are not modifying xy, so make a copy here
            xyDat[j] = getSceneFrameInfo(frame[j]).fullGrayImage(roi);
         }

         if (xyDat[0].isEmpty())
         {
            continue;
         }

         //
         // % average the five pixels
         // xySum = zeros(size(xyDat,1), size(xyDat,2));
         // for j = 1:5
         // xySum = xySum + squeeze(xyDat(:,:,j));
         // end
         auto xySum = xyDat[0].duplicate();
         for (auto j = 1; j < 5; j++)
         {
            xySum += xyDat[j];
         }

         // xyMean = xySum / 5;
         xySum /= 5.0f; // Always 5
         auto xyMean = xySum; // Save a copy
         //
         // % at each pixel find the squared error to the mean
         // xyMSE = zeros(size(xyDat,1),size(xyDat,2));
         // for j = 1:5
         // xyDiff = squeeze(xyDat(:,:,j)) - xyMean;
         // xyMSE = xyMSE + xyDiff.*xyDiff;
         // end
         auto xyDiff = xyDat[0] - xyMean;
         auto xyMSE = xyDiff.duplicate();
         xyMSE *= xyDiff;
         for (auto j = 1; j < 5; j++)
         {
            xyDiff = xyDat[j] - xyMean;
            xyDiff *= xyDiff;
            xyMSE += xyDiff;
         }

         // xyMSE = sqrt(xyMSE/5);
         xyMSE /= 5;
         xyMSE.sqrtInPlace();
         // xyMSE = conv2(xyMSE, MSE_Smooth, 'same');
         // xyMSE = xyMSE.applyBoxFilter({ smoothRadius, smoothRadius });
         xyMSE = xyMSE.applyBoxFilter({ smoothRadius, smoothRadius });

         //
         // % for each pixel count number of motions considered
         // xxCurr = x0(iCurr):x1(iCurr);
         // yyCurr = y0(iCurr):y1(iCurr);
         auto mseRoi = xyDat[iCurr].getRoi();

         // Mask(yyCurr,xxCurr) = Mask(yyCurr,xxCurr) + 1;
         //
         // % keep track of the best MSE
         // mse = MSE(yyCurr,xxCurr);
         auto mse = MSE(mseRoi);
         auto bx = BOX5(mseRoi);
         // idx = find(xyMSE < mse);
         // mse(idx) = xyMSE(idx);
         // MSE(yyCurr,xxCurr) = mse;
         // bx = BOX5(yyCurr,xxCurr);
         // bx(idx) = iBox;
         // BOX5(yyCurr,xxCurr) = bx;
         // NOTE: mse and MSE address the same data, bx and BOX5 address same data
         Ipp32fArray mseZ5[] =
         { Z5[0](mseRoi), Z5[1](mseRoi), Z5[2](mseRoi), Z5[3](mseRoi), Z5[4](mseRoi) };

         for (auto r = 0; r < mse.getHeight(); r++)
         {
            auto bxRow = bx.getRowPointer(r);
            auto mseRow = mse.getRowPointer(r);
            auto xyMSERow = xyMSE.getRowPointer(r);

            for (auto c = 0; c < mse.getWidth(); c++)
            {
               if (xyMSERow[c] < mseRow[c])
               {
                  mseRow[c] = xyMSERow[c];
                  bxRow[c] = iBox;
                  auto k = mseZ5[0].sub2ind({ c, r });
                  for (auto j = 0; j < 5; j++)
                  {
                     mseZ5[j][k] = xyDat[j][k];
                  }
               }
            }
         }

         // end
      }
      // end
   };

   IpaStripeStream iss;
   iss << _shotInfoBoxes.size() - 1;
   iss << efu_job(f);
   iss.stripe();
   return BOX5;
}

// blob1a
std::pair<Ipp32fArray, Ipp32fArray>ShineRepairMask::computeDiffsOncePerFrame(const MtiPlanar<Ipp32fArray> &Z5)
{
   // Assume we have found the pixel motion frames and
   //
   // The input to this function is :
   // ContrastPos - the value between 0 and 100 for the positive contrast
   // ContrastNeg - the value between 0 and 100 for the negative contrast
   //
   // BlobCode1 applies a threshold to the contrast image. Pixels that satisfy
   // the threshold are candidates for repair.
   //
   //
   // This function returns the MaskContrast, the Mask of candidate pixels
   // based on contrast.
   //
   // _processFrameIdx = repairSceneIndex;

   // // Y = ShotInfo.Y{ ShotInfo.ProcessFrameIdx };
   auto Y = getSceneFrameInfo(_processFrameIdx).fullGrayImage;

   // The pixel motion returns the median at 2
   // Median = median(Z5, 3);
   // auto medianZ5 = Z5.median();
   // auto diffRaw = Y - medianZ5;

   Ipp32fArray medianZ5(Y.getSize());
   Ipp32fArray diffRaw(Y.getSize());

   auto mf = [&](const MtiRect & roi)
   {
      auto m = medianZ5(roi);
      m <<= Z5(roi).median();
      diffRaw(roi) <<= Y(roi);
      diffRaw(roi) -= m;
   };

   IpaStripeStream iss2;
   iss2 << medianZ5.getSize() << efu_roi(mf);

   // We don't want the strips to get too small.
   // Not bad for 4k file
   iss2.stripe(16);

   const int NBIN = 100;
   ImageBinSorter imageBinSorter;
   auto myBins = imageBinSorter.sortDataToBins(medianZ5, diffRaw, NBIN);
   auto myBinIndex = imageBinSorter.getIndexBins();

   // DiffScale = zeros(size(DiffRaw));
   Ipp32fArray diffScale(diffRaw.getSize());

   // The range of difference values depends on the median intensity.e.g.
   // brighter areas typically have a larger range than darker areas.
   // Normalize the difference to create a contrast image.
   auto f = [&](int i)
   {
      auto &mb = myBins[i];
      auto &idx = myBinIndex[i];

      auto range = mb[int(std::round(.8 * mb.area()))] - mb[int(std::round(.2 * mb.area()))];
      // DiffScale(idx) = DiffRaw(idx) / range(i);
      for (auto k = size_t(0); k < idx.getWidth(); k++)
      {
         diffScale[idx[k]] = diffRaw[idx[k]] / range;
      }

      // end
   };

   IpaStripeStream iss;
   iss << NBIN << efu_job(f);
   iss.stripe();

   //SaveVariablesToMatFile1(R"(c:\temp\masks.mat)", medianZ5);
   return
   {
      diffRaw, diffScale
   };
}

// blob1b
// Ipp8uArray ShineRepairMask::createContrastMask(const Ipp32fArray &diffRaw, const Ipp32fArray &diffScale, const double contrastPos, const double contrastNeg)
// {
// //% We use the ContrastPos and ContrastNeg in two ways :
// //%
// //%  1.  Make sure the raw diff value is sufficiently large
// //%  2.  Make sure the scaled diff value is sufficiently large
//
// // MIN_RAW_CONTRAST = .1;
// const float MIN_RAW_CONTRAST = 0.01f;
//
// // MAX_RAW_CONTRAST = .3;
// const float MAX_RAW_CONTRAST = 0.20f;
//
// // MIN_SCALED_CONTRAST = 1;
// const float MIN_SCALED_CONTRAST = 1.0f;
//
// // MAX_SCALED_CONTRAST = 10;
// //const float MAX_SCALED_CONTRAST = 10.0f;
// const float MAX_SCALED_CONTRAST = 8.5f;  // Braca's empirical numbers!
//
// //% when ContrastVal = 0, RawThresh = MIN_RAW_CONTRAST
// //% when ContrastVal = 100, RawThresh = MAX_RAW_CONTRAST
//
// //SlopeRaw = (MAX_RAW_CONTRAST - MIN_RAW_CONTRAST) / (100 - 0);
// auto slopeRaw = (MAX_RAW_CONTRAST - MIN_RAW_CONTRAST) / (100 - 0);
//
// //InterRaw = MIN_RAW_CONTRAST - SlopeRaw * 0;
// auto interRaw = MIN_RAW_CONTRAST - slopeRaw * 0;
//
// //% when ContrastVal = 0, ScaledThresh = MIN_SCALED_CONTRAST
// //% when ContrastVal = 100, ScaledThresh = MAX_SCALED_CONTRAST
// //SlopeScale = (MAX_SCALED_CONTRAST - MIN_SCALED_CONTRAST) / (100 - 0);
// auto slopeScale = (MAX_SCALED_CONTRAST - MIN_SCALED_CONTRAST) / (100 - 0);
// auto interScale = MIN_SCALED_CONTRAST - slopeScale * 0;
//
// //% Calculate the thresholds for the raw diff
// // RawThreshPos = SlopeRaw * ContrastPos + InterRaw;
// auto rawThreshPos = slopeRaw * contrastPos + interRaw;
//
// // RawThreshNeg = SlopeRaw * ContrastNeg + InterRaw;
// auto rawThreshNeg = slopeRaw * contrastNeg + interRaw;
//
// //% Calculate the thresholds for the scaled diff
// // ScaleThreshPos = SlopeScale * ContrastPos + InterScale;
// auto scaleThreshPos = slopeScale * contrastPos + interScale;
//
// //ScaleThreshNeg = SlopeScale * ContrastNeg + InterScale;
// auto scaleThreshNeg = slopeScale * contrastNeg + interScale;
//
// // % create the masks
// // MaskRaw = (DiffRaw > RawThreshPos) | (DiffRaw < -RawThreshNeg);
// // MaskScale = (DiffScale > ScaleThreshPos) | (DiffScale < -ScaleThreshNeg);
// // MaskContrast = MaskRaw & MaskScale;
//
// auto maskContrast = Ipp8uArray(diffRaw.getSize());
// maskContrast.zero();
// for (auto r = 0; r < maskContrast.getHeight(); ++r)
// {
// auto maskRawRow = diffRaw.getRowPointer(r);
// auto maskScaleRow = diffScale.getRowPointer(r);
// auto maskContrastRow = maskContrast.getRowPointer(r);
//
// // Monochrome image, so one component per pixel.
// for (auto c = 0; c < maskContrast.getWidth(); ++c)
// {
// //if ((maskRawRow[c] > rawThreshPos || maskRawRow[c] < -rawThreshNeg)
// //   && (maskScaleRow[c] > scaleThreshPos || maskScaleRow[c] < -scaleThreshNeg))
// //{
// //   maskContrastRow[c] = 1;
// //}
// if (maskScaleRow[c] > scaleThreshPos || maskScaleRow[c] < -scaleThreshNeg)
// {
// maskContrastRow[c] = 1;
// }
// }
// }
// // end
// return maskContrast;
// }

Ipp8uArray ShineRepairMask::createSpatialMask(const Ipp8uArray& temporalMask, const int minSize, const int maxSize,
   const double spatialContrastPos, const double spatialContrastNeg)
{
   //
   // The input to this function is :
   // MaskTemporal - the mask of temporal contrast(1 = positive, 127 = negative)
   // MinSize - the value of the minimum debris size
   // MaxSize - the value of the maximum debris size
   // SpatialContrastPos - the value between 0 and 100 for the positive
   // spatial contrast
   // SpatialContrastNeg - the value between 0 and 100 for the negative
   // spatial contrast
   //
   // Connected components of candidate pixels are analyzed and the nearest
   // non - candidate pixel is found.The nearest non - candidate pixel is
   // used for three purposes :
   // (a)compute debris size
   // (b)compute spatial contrast
   //
   // This function returns :
   // MaskSpatial - the mask of defective pixels
   //

   // Y = ShotInfo.Y{ ShotInfo.ProcessFrameIdx };
   auto Y = getSceneFrameInfo(_processFrameIdx).fullGrayImage;
   Ipp8uArray maskSpatial(Y.getSize());
   maskSpatial.zero();
   Ipp32fArray contrSpatial(Y.getSize());
   contrSpatial.zero();

   // calculate the distance to candidate pixels
   auto distToBlob = temporalMask.bwdist(_scratchArray);

   // connected components
   // stats = regionprops(DistToBlob <= 1, 'PixelIdxList');
   LabelMarker labelMarker;
   auto labelMask = labelMarker.label(distToBlob.compareC(1, ippCmpLessEq));
   auto numberOfLabels = labelMarker.getNumberOfLabels();
   vector < vector < int >> clusterIndices = labelMarker.getClusterIndices(labelMask, numberOfLabels, minSize,
      maxSize);

   // We use the ContrastPos and ContrastNeg to make sure the scaled diff
   // value is sufficiently large

   // MIN_SPATIAL_CONTRAST = 0;
   auto MIN_SPATIAL_CONTRAST = 0.0f;

   // MAX_SPATIAL_CONTRAST = .5;
   auto MAX_SPATIAL_CONTRAST = 0.5f;

   // when ContrastVal = 0, SpatialThresh = MIN_SCALED_CONTRAST
   // when ContrastVal = 100, SpatialThresh = MAX_SCALED_CONTRAST
   // SlopeContrast = (MAX_SPATIAL_CONTRAST - MIN_SPATIAL_CONTRAST) / (100 - 0);
   auto slopeContrast = (MAX_SPATIAL_CONTRAST - MIN_SPATIAL_CONTRAST) / (100.0f - 0.0f);

   // InterContrast = MIN_SPATIAL_CONTRAST - SlopeContrast * 0;
   auto interContrast = MIN_SPATIAL_CONTRAST - slopeContrast * 0;

   // Calculate the thresholds for the spatial diff
   // SpatialThreshPos = SlopeContrast * SpatialContrastPos + InterContrast;
   auto spatialThreshPos = slopeContrast * spatialContrastPos + interContrast;
   auto spatialThreshNeg = slopeContrast * spatialContrastNeg + interContrast;

   // Use the size of the defect to promote the average spatial contrast.
   // Small defects are evaluated on their observed contrast.
   // Large defects are evaluated on a promoted value of the average contrast.

   // MIN_PROMOTE_SIZE = 1;
   auto const MIN_PROMOTE_SIZE = 1.0f;

   // MAX_PROMOTE_SIZE = 100;
   auto const MAX_PROMOTE_SIZE = 100.0f;

   // MIN_PROMOTE_VAL = 1;
   auto const MIN_PROMOTE_VAL = 1.0f;

   // MAX_PROMOTE_VAL = 10;
   auto const MAX_PROMOTE_VAL = 10.0f;

   // SlopePromote = (MAX_PROMOTE_VAL - MIN_PROMOTE_VAL) / ...
   // (MAX_PROMOTE_SIZE - MIN_PROMOTE_SIZE);
   auto slopePromote = (MAX_PROMOTE_VAL - MIN_PROMOTE_VAL) / (MAX_PROMOTE_SIZE - MIN_PROMOTE_SIZE);

   // InterPromote = MIN_PROMOTE_VAL - SlopePromote * MIN_PROMOTE_SIZE;
   auto interPromote = MIN_PROMOTE_VAL - slopePromote * MIN_PROMOTE_SIZE;

   // process each connected component independently
   // for i = 1:numel(stats)
   vector<int>pixInt;
   vector<int>pixExt;
   for (auto blob : range(clusterIndices.size()))
   {
      pixInt.clear();
      pixExt.clear();

      // Idx = stats(i).PixelIdxList;
      auto idx = clusterIndices[blob];
      // PixInt = Idx(DistToBlob(Idx) == 0);    // detected(interior) pixels
      // PixExt = Idx(DistToBlob(Idx) > 0);     // exterior pixels
      //
      for (auto blobPixelIndex = 0; blobPixelIndex < idx.size(); ++blobPixelIndex)
      {
         auto linearIndex = idx[blobPixelIndex];
         if (distToBlob[linearIndex] == 0.F)
         {
            pixInt.push_back(linearIndex);
         }
         else
         {
            pixExt.push_back(linearIndex);
         }
      }

      // keep track of the size of the debris
      // DebrisSize = 0;
      auto debrisSize = 0.0f;
      //
      // // for each interior pixel, find the nearest exterior
      // [rowInt, colInt] = ind2sub(size(DistToBlob), PixInt);
      // [rowExt, colExt] = ind2sub(size(DistToBlob), PixExt);
      // nPos = 0;
      auto nPos = 0;

      // nNeg = 0;
      auto nNeg = 0;

      // SpatialCuml = 0;
      auto spatialCuml = 0.0f;

      // for j = 1:numel(PixInt)
      // rInt = rowInt(j);
      // cInt = colInt(j);
      // delta = [rowExt, colExt] - [rInt, cInt];
      // dist = sqrt(delta(:, 1). ^ 2 + delta(:, 2). ^ 2);
      // [MinDist, k] = min(dist);
      // Brute force
      for (size_t intIndex = 0; intIndex < pixInt.size(); ++intIndex)
      {
         auto closestExtIndex = intIndex;
         auto minDist = double(INT_MAX); // Can't remember how to get FLOAT_MAX!
         auto xyInt = Y.ind2sub(pixInt[intIndex]);
         for (auto extIndex = 0; extIndex < pixExt.size(); ++extIndex)
         {
            auto xyExt = Y.ind2sub(pixExt[extIndex]);
            auto delta = xyExt - xyInt;
            // No need to take sqrt
            // auto dist = sqrt((delta.x * delta.x) + (delta.y * delta.y));
            auto dist = (delta.x * delta.x) + (delta.y * delta.y);
            if (dist < minDist)
            {
               minDist = dist;
               closestExtIndex = pixExt[extIndex];
            }
         }
         //
         // (rowExt(k), colExt(k)) is the closest exterior point.
         // rExt = rowExt(k);
         // cExt = colExt(k);
         //
         // // the size of the debris is the largest distance to the exterior
         // // this is useful for long pieces of hair since many pixels may
         // // be involved, but locally the hair always appears small.
         // DebrisSize = max(DebrisSize, MinDist);
         debrisSize = std::max<float>(debrisSize, std::sqrt(minDist));

         //
         // the spatial contrast
         // if MaskTemporal(rInt, cInt) > 0
         auto spatialDiff = 0.0f;
         if (temporalMask[xyInt] == 255)
         {
            // nPos = nPos + 1;
            nPos = nPos + 1;
            // SpatialDiff = Y(rInt, cInt) - Y(rExt, cExt);
            spatialDiff = Y[xyInt] - Y[closestExtIndex];
         }
         // elseif MaskTemporal(rInt, cInt) < 0
         else if (temporalMask[xyInt] > 0)
         {
            // nNeg = nNeg + 1;
            // SpatialDiff = Y(rExt, cExt) - Y(rInt, cInt);
            spatialDiff = Y[closestExtIndex] - Y[xyInt];
         }
         // else
         else
         {
            // SpatialDiff = 0;
            spatialDiff = 0;
         }
         //
         // SpatialCuml = SpatialCuml + SpatialDiff;
         spatialCuml = spatialCuml + spatialDiff;
         // ContrSpatial(rInt, cInt) = SpatialDiff;
         contrSpatial[xyInt] = spatialDiff;
      }

      // decide if this candidate should be processed
      auto useIt = true;
      // if (DebrisSize < MinSize) || (DebrisSize > MaxSize)
     // DBTRACE2(maxSize, debrisSize);
      if ((debrisSize <= minSize) || (debrisSize > maxSize))
      {
         // UseIt = false;
         useIt = false;
      }

      // SpatialAve = SpatialCuml / numel(PixInt);
      auto spatialAve = spatialCuml / pixInt.size();
      // SpatialPromote = min(MAX_PROMOTE_VAL, SlopePromote * numel(PixInt) + InterPromote);
      auto spatialPromote = std::min<float>(MAX_PROMOTE_VAL, slopePromote * pixInt.size() + interPromote);
      auto spatialVal = spatialPromote * spatialAve;

      // since a blob can contain both positive and negative contrast pixels,
      // use majority rules to decide which threshold to use
      // if nPos > nNeg
      if (nPos > nNeg)
      {
         // // mostly a positive contrast defect
         // if SpatialVal < SpatialThreshPos
         if (spatialVal < spatialThreshPos)
         {
            useIt = false;
         }
      }
      // else
      else
      {
         // mostly a negative contrast defect
         // if SpatialVal < SpatialThreshNeg
         if (spatialVal < -spatialThreshNeg)
         {
            useIt = false;
         }
      }
      //
      // if UseIt == true
      // MaskSpatial(PixInt) = true;
      // end
      if (useIt)
      {
         for (auto i : pixInt)
         {
            maskSpatial[i] = true;
         }
      }
   }

   return maskSpatial;
}

// [BOX4, MaskDetect] = BlobCode2(ShotInfo, BOX5, MaskContrast, MinSize, MaxSize)
// ShotInfo is in private variables
// blob2
Ipp16uArray ShineRepairMask::createDetectionMask(const Ipp16uArray &box5, const Ipp8uArray &spatialMask)
{
   // %
   // % The input to this function is :
   // %    ShotInfo - information about the shot
   // %    BOX5 - the five - frame motion box assigned at each pixel
   // %    MaskContrast - the mask of candidate defects
   // %    MinSize - the value of the minimum debris size
   // %    MaxSize - the value of the maximum debris size
   // %
   // % Connected components of candidate pixels are analyzed and the nearest
   // % non - candidate pixel is found.The nearest non - candidate pixel is
   // % used for two purposes : (a)compute debris size and
   // % (b)update the motion vectors.
   // %
   // % This function returns :
   // %     BOX4 - the four - frame motion box assigned to each pixel
   // %     MaskDefect - the mask of defective pixels
   // %     MaskFidelity - the mask with low motion fidelity
   // %
   //
   // Y = ShotInfo.Y{ ShotInfo.ProcessFrameIdx };
   DBTRACE2("createDetectionMask", _processFrameIdx);
   auto Y = getSceneFrameInfo(_processFrameIdx).fullGrayImage;

   // BOX4 = BOX5;
   auto box4 = box5.duplicate();

   // MaskDetect = false(size(Y));
   auto maskDetect = Ipp8uArray(Y.getSize());
   maskDetect.zero();

   // % calculate the distance to candidate pixels
   // DistToBlob = bwdist(MaskContrast);

   CHRTimer createDetectionMask;
   auto distToBlob = spatialMask.bwdist(_scratchArray);
   DBTRACE2("distToBlob", createDetectionMask)
      // % connected components
      // stats = regionprops(DistToBlob <= 3, 'PixelIdxList');
      auto maskOfDistsBelowThreshold = distToBlob.compareC(3.F, ippCmpLessEq);
   DBTRACE2("maskOfDistsBelowThreshold", createDetectionMask)

      LabelMarker labelMarker;
   auto labelMask = labelMarker.label(maskOfDistsBelowThreshold);
   auto numberOfLabels = labelMarker.getNumberOfLabels();
   DBTRACE2(numberOfLabels, createDetectionMask) auto clusterIndices =
      labelMarker.getClusterIndices(labelMask, numberOfLabels);
   DBTRACE2(clusterIndices.size(), createDetectionMask)

      // % process each connected component independently
      // for i = 1:numel(stats)
      // for (auto blob = 0; blob < numberOfLabels; ++blob)
      auto f = [&](int blob)
   {
      auto idx = clusterIndices[blob];
      vector<int>pixInt;
      pixInt.reserve(idx.size());
      vector<int>pixExt;
      pixExt.reserve(idx.size());
      // Idx = stats(i).PixelIdxList;
      // PixDetect = Idx(DistToBlob(Idx) == 0); % the detected pixels
      // PixInt = Idx(DistToBlob(Idx) <= 2);    % interior pixels
      // PixExt = Idx(DistToBlob(Idx) > 2);     % exterior pixels
      for (auto blobPixelIndex = 0; blobPixelIndex < idx.size(); ++blobPixelIndex)
      {
         auto linearIndex = idx[blobPixelIndex];
         if (distToBlob[linearIndex] <= 2.F)
         {
            pixInt.push_back(linearIndex);
         }
         else
         {
            pixExt.push_back(linearIndex);
         }
      }

      // % for each interior pixel, find the nearest exterior
      // [rowInt, colInt] = ind2sub(size(DistToBlob), PixInt);
      // [rowExt, colExt] = ind2sub(size(DistToBlob), PixExt);
      // for j = 1:numel(rowInt)
      // rInt = rowInt(j);
      // cInt = colInt(j);
      // % replace jam
      // delta = [rowExt - rInt, colExt - cInt];
      // %        delta = [rowExt, colExt] - [rInt, cInt];
      // dist = sqrt(delta(:, 1). ^ 2 + delta(:, 2). ^ 2);
      // [MinDist, k] = min(dist);

      // Brute force
      for (auto intIndex = 0; intIndex < pixInt.size(); ++intIndex)
      {
         auto closestExtIndex = intIndex;
         auto minDist = double(INT_MAX); // Can't remember how to get FLOAT_MAX!
         auto xyInt = Y.ind2sub(pixInt[intIndex]);
         for (int extIndex : pixExt)
         {
            auto xyExt = Y.ind2sub(extIndex);
            auto delta = xyExt - xyInt;
            // No need to take sqrt
            // auto dist = sqrt((delta.x * delta.x) + (delta.y * delta.y));
            auto dist = (delta.x * delta.x) + (delta.y * delta.y);
            if (dist < minDist)
            {
               minDist = dist;
               closestExtIndex = extIndex;
            }
         }

         // % (rowExt(k), colExt(k)) is the closest exterior point.
         // % Assign the nearest neighbor motion
         // rExt = rowExt(k);
         // cExt = colExt(k);
         // BOX4(rInt, cInt) = BOX5(rExt, cExt);
         box4[pixInt[intIndex]] = box5[closestExtIndex];
      }

      // end
   };

   if (numberOfLabels > 0)
   {
   IpaStripeStream iss;
   iss << numberOfLabels << efu_job(f);
   iss.stripe();
   }
   DBTRACE2("Striping", createDetectionMask);
   // end
   return box4;
}

// function MaskMotion = BlobCode3 (ShotInfo, BOX4, MotionTolerance)
// blob3
Ipp8uArray ShineRepairMask::createMotionMask(const Ipp16uArray &box4, float motionTolerance)
{
   // %
   // % The input to this function is:
   // %    ShotInfo - information about the shot
   // %    BOX4 - the four-frame motion box assigned at each pixel
   // %    MaskSize - the mask of candidate defects
   // %    MotionTolerance - the value between 0 and 100 for the motion tolerance
   // %
   // % This function computes Z4, four motion compensated frames where the
   // % current frame is not included.  Z4 is used to generate the motion
   // % difference frame.
   // %
   // % The MaskMotion image is returned.  It indicates areas with unreliable
   // % motion.
   //
   //
   // Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
   auto &Y = getSceneFrameInfo(_processFrameIdx).fullGrayImage;
   // [nRow, nCol] = size(Y);
   // Z4 = zeros(nRow,nCol,4);
   MtiSize Z4Size(Y.getSize(), 4);
   MtiPlanar<Ipp32fArray>Z4(Z4Size);
   Z4.zero();
   // % create the Z4 from the boxes assigned to the four frame offsets.
   auto box4MaxValue = int(_shotInfoBoxes.size());

   // for box = 1:max(BOX4(:))
   // TODO see if making this a private variable reduces allocation
   LabelMarker labelMarker;
   auto equivalenceIndices = labelMarker.getEquivalenceIndices(box4);
   // for (auto &idx : equivalenceIndices)
   auto fLabel = [&](int index)
   {
      auto &idx = equivalenceIndices[index];
      auto box = box4[idx[0]];
      // Frame = ShotInfo.BOX{box}.Frame;
      // xoff = ShotInfo.BOX{box}.Offset(:,1);
      // yoff = ShotInfo.BOX{box}.Offset(:,2);
      auto &shotInfoBox = _shotInfoBoxes[box];
      auto frame = shotInfoBox.frame;
      int xOff[5];
      int yOff[5];

      for (auto i = 0; i < 5; i++)
      {
         xOff[i] = int(shotInfoBox.offset[i].x);
         yOff[i] = int(shotInfoBox.offset[i].y);
      }

      // i4 = 0;
      // for i5 = 1:5
      auto i4 = -1;
      for (auto i5 = 0; i5 < 5; ++i5)
      {
         // if Frame(i5) ~= ShotInfo.ProcessFrameIdx
         if (frame[i5] == _processFrameIdx)
         {
            continue;
         }

         auto &YLocal = getSceneFrameInfo(frame[i5]).fullGrayImage; // pulled this out of the loop, below.

         // i4 = i4 + 1;
         ++i4;
         auto z4 = Z4[i4]; // pulled this out of the loop, below.

         vector<int>srcRowCol; // CAREFUL, these are [x (col), y (row)] !
         auto maxX = YLocal.getSize().width;
         auto maxY = YLocal.getSize().height;
         for (auto idxIndex : idx)
         {
            auto rc = YLocal.ind2sub(idxIndex);
            auto rowOff = legalize<int>(rc.y + yOff[i5], 0, maxY - 1);
            auto colOff = legalize<int>(rc.x + xOff[i5], 0, maxX - 1);

            // idxOff = sub2ind([nRow,nCol], rowOff, colOff);
            auto idxOff = YLocal.sub2ind(MtiPoint(colOff, rowOff));

            // z4 = Z4(:,:,i4);      // Moved out of loop, above!
            // z4(idx) = ShotInfo.Y{Frame(i5)}(idxOff);
            z4[idxIndex] = YLocal[idxOff];
            // Z4[i4][idxIndex] = Y[idxOff];  SAME AS ABOVE
            // end
         }

         // Since z4 IS Z4[i4], we don't have to copy.
         // Z4(:,:,i4) = z4;
         // Z4[i4] = z4;

         // end
      }
      // end
   };

   IpaStripeStream iss;
   iss << equivalenceIndices.size() << efu_job(fLabel);
   iss.stripe();

   //
   // % the motion image is maximal discrepancy found in Z4.  The value is
   // % small when the motion is reliable.  The value is large when the motion
   // % is unreliable.
   //
   // MotionRaw = max(Z4,[],3) - min(Z4,[],3);
   // MatIO::saveListToMatFile(R"(c:\temp\truth\motionRaw.mat)", motionRaw, "MotionRaw_Truth", Z4, "Z4_ Truth");
   // MatIO::saveListToMatFile(R"(c:\temp\truth\motionRaw.mat)", motionRaw, "MotionRaw_Truth", Z4, "Z4_ Truth");

   // % The range of motion values depends on the median intensity.  e.g.
   // % brighter areas typically have a larger range than darker areas.
   // % Normalize the difference to create a scaled motion image.
   // Median = median (Z4, 3);

   /////////////////////////////////
   // CAREFUL THIS DESTROYS Z4 !! //
   // ASSUMES 4 FRAMES!!!         //
   /////////////////////////////////
   auto fSort = [&](const MtiRect & roi)
   {
      Z4(roi).sortInPlace();
   };

   iss << Z4.getPlaneSize() << efu_roi(fSort);
   iss.stripe();

   auto medianImage = Z4[1]; // Z4[1] is destroyed
   medianImage += Z4[2];
   medianImage /= 2.f;

   auto motionRaw = Z4[3];
   motionRaw -= Z4[0];

   const int NBIN = 100;

   /////////////////////////////////////////////////////////////////
   ImageBinSorter imageBinSorter;
   auto myBins = imageBinSorter.sortDataToBins(medianImage, motionRaw, NBIN);
   auto myBinIndex = imageBinSorter.getIndexBins();

   /////////////////////////////////////////////////////////////////

   auto motionScale = Ipp32fArray(motionRaw.getSize());

   // for i = 1:NBIN
   auto f = [&](int i)
      // for (auto i = 1; i <= NBIN; i++)
   {
      auto &mb = myBins[i];
      auto &idx = myBinIndex[i];
      // mot20(i) = mot_sort (std::round(.2*numel(mot_sort)));
      // auto mot20 = mot_sort[int(std::round(0.2F * mot_sort.size()))];
      auto mot20 = mb[int(std::round(0.2F * mb.area()))];

      // mot80(i) = mot_sort (std::round(.8*numel(mot_sort)));
      // auto mot80 = mot_sort[int(std::round(0.8F * mot_sort.size()))];
      auto mot80 = mb[int(std::round(0.8F * mb.area()))];

      // range(i) = mot80(i) - mot20(i);
      auto motRange = mot80 - mot20;
      // MotionScale(idx) = MotionRaw(idx) / range(i);

      for (auto k = size_t(0); k < idx.area(); k++)
      {
         motionScale[idx[k]] = motionRaw[idx[k]] / motRange;
      }
      // end
   };

   // IpaStripeStream iss;
   iss << NBIN;
   iss << efu_job(f);
   iss.stripe();

   auto motionSmooth = motionScale.applyBoxFilter({ 9, 9 });
   // MotionSmooth = conv2(MotionScale, MOTION_SMOOTH, 'same');
   //
   //
   // % We use the MotionVal to set a threshold for MotionSmooth
   // %
   //
   // MIN_MOTION_THRESH = 1;
   auto MIN_MOTION_THRESH = 1.f;

   // MAX_MOTION_THRESH = 3;
   auto MAX_MOTION_THRESH = 3.f;
   //
   // % when MotionVal = 0, Thresh = MIN_MOTION_THRESH
   // % when MotionVal = 100, Thresh = MAX_MOTION_THRESH
   // Slope = (MAX_MOTION_THRESH-MIN_MOTION_THRESH) / (100-0);
   auto Slope = (MAX_MOTION_THRESH - MIN_MOTION_THRESH) / (100.0f - 0.0f);

   // Inter = MIN_MOTION_THRESH - Slope*0;
   auto Inter = MIN_MOTION_THRESH - Slope * 0;
   //
   // Thresh = Slope*MotionTolerance + Inter;
   auto Thresh = Slope * motionTolerance + Inter;

   //
   // MaskMotion = MotionSmooth > Thresh;
   auto maskMotion = motionSmooth.compareC(Thresh, ippCmpGreater);
   return maskMotion;
}

//// function MaskProcess = BlobCode4 (MaskDetect, MaskMotion)
//// blob4
Ipp8uArray ShineRepairMask::createProcessMask(const Ipp8uArray &detectMask, const Ipp8uArray &motionMask)
{
   // %
   // % The input to this function is:
   // %    MaskDetect - the mask of candidate defects
   // %    MaskMotion - the mask of unreliable motion
   // %
   // % The MaskDefect image is returned.  It indicates areas which should be
   // % processed.

   // MaskProcess = false(size(MaskDetect));
   Ipp8uArray processMask(detectMask.getSize());
   processMask.zero();

   // % calculate the distance to candidate pixels
   // DistToBlob = bwdist (MaskDetect);

   auto largeDisk = Ipp8uArray::createDisk(10);
   auto smallDisk = Ipp8uArray::createDisk(2);
   auto expandedMotion = motionMask.erode(smallDisk, _scratchArray);
   expandedMotion = expandedMotion.dilate(largeDisk, _scratchArray);

   auto detectMask2 = detectMask.dilate(smallDisk, _scratchArray);

   for (auto r = 0; r < motionMask.getHeight(); r++)
   {
      auto processP = processMask.getRowPointer(r);
      auto motionP = expandedMotion.getRowPointer(r);
      auto detectP = detectMask2.getRowPointer(r);
      for (auto c = 0; c < motionMask.getWidth(); c++)
      {
         if (motionP[c] == 0 && detectP[c] != 0)
         {
            processP[c] = 1;
         }
      }
   }

   // IpaStripeStream iss;
   // iss << stats.size() << efu_job(f);
   // iss.stripe();
   return processMask;
   // end
}

// KEVIN's ORIGINAL code
//Ipp8uArray ShineRepairMask::createProcessMask(const Ipp8uArray &maskDetect, const Ipp8uArray &maskMotion)
//{
//   //%
//   //% The input to this function is:
//   //%    MaskDetect - the mask of candidate defects
//   //%    MaskMotion - the mask of unreliable motion
//   //%
//   //% The MaskDefect image is returned.  It indicates areas which should be
//   //% processed.
//
//   // MaskProcess = false(size(MaskDetect));
//   Ipp8uArray maskProcess(maskDetect.getSize());
//   maskProcess.zero();
//
//   //% calculate the distance to candidate pixels
//   // DistToBlob = bwdist (MaskDetect);
//   Ipp8uArray scratch;
//   auto distToBlob = maskDetect.bwdist(scratch);
//
//   //% connected components
//   // stats = regionprops(DistToBlob <= 2, 'PixelIdxList');
//   auto regionMask = distToBlob.compareC(2, ::ippCmpLessEq);
//
//   LabelMarker labelMarker;
//   auto labelMask = labelMarker.label(regionMask);
//   auto stats = labelMarker.getClusterIndices(labelMask, labelMarker.getNumberOfLabels(), 0, INT_MAX);
//
//   // for i = 1:numel(stats)
//   // Idx = stats(i).PixelIdxList;
//   // auto f = [&](int index)
//   for (auto& idx : stats)
//   {
//      //		if sum(MaskMotion(Idx)) == 0
//      auto motion = std::accumulate(idx.begin(), idx.end(), 0.0f, [&](float a, int i) { return a + maskMotion[i]; });
//      if (motion == 0)
//      {
//         // % this blob does not intersect an area of bad motion
//         // maskProcess[Idx] = true;
//         for (auto i : idx)
//         {
//            maskProcess[i] = true;
//         }
//         //    end
//      }
//      // end
//   }
//
//   //IpaStripeStream iss;
//   //iss << stats.size() << efu_job(f);
//   //iss.stripe();
//   return maskProcess;
//   //end
//}

template<typename T, typename S>void bubbleSortAbyB_simple(std::vector<T>&a, std::vector<S>&b) {
   auto n = int(b.size());
   for (auto i = 0; i < n - 1; i++)
   {
      for (auto j = 0; j < n - i - 1; j++)
      {
         if (b[j] > b[j + 1])
         {
            std::swap(b[j], b[j + 1]); std::swap(a[j], a[j + 1]);
         }
      }
   }
}

// function Dst = BlobCode5 (ShotInfo, BOX4, MaskProcess)
// blob5

#define NUM_PICK_UNIQUE  40
#define NUM_PICK_INDEX 32
#define PICK_TABLE_DIM 16

int _pickPattern[PICK_TABLE_DIM][PICK_TABLE_DIM] = {
        0,  0,  0,  1,  1,  1,  2,  2,  3,  3,  3,  4,  4,  4,  5,  5,
        0,  0,  0,  1,  1,  6,  6,  6,  3,  3,  3,  4,  4,  7,  7,  7,  
        0,  0,  8,  8,  8,  6,  6,  6,  3,  3,  9,  9,  9,  7,  7,  7,
       10, 10,  8,  8,  8,  6,  6, 11, 11, 11,  9,  9,  9,  7, 7,  10,
       10, 10,  8,  8, 12, 12, 12, 11, 11, 11,  9,  9, 13, 13, 13, 10, 
       10, 14, 14, 14, 12, 12, 12, 11, 11, 15, 15, 15, 13, 13, 13, 10,
       16, 14, 14, 14, 12, 12, 17, 17, 17, 15, 15, 15, 13, 13, 16, 16,
       16, 14, 14, 18, 18, 18, 17, 17, 17, 15, 15, 19, 19, 19, 16, 16, 
       20, 20, 20, 18, 18, 18, 17, 17, 21, 21, 21, 19, 19, 19, 16, 16, 
       20, 20, 20, 18, 18, 22, 22, 22, 21, 21, 21, 19, 19, 23, 23, 23, 
       20, 20, 24, 24, 24, 22, 22, 22, 21, 21, 25, 25, 25, 23, 23, 23,
       26, 26, 24, 24, 24, 22, 22, 27, 27, 27, 25, 25, 25, 23, 23, 26, 
       26, 26, 24, 24, 28, 28, 28, 27, 27, 27, 25, 25, 29, 29, 29, 26,
       26, 30, 30, 30, 28, 28, 28, 27, 27, 31, 31, 31, 29, 29, 29, 26,
        5, 30, 30, 30, 28, 28,  2,  2,  2, 31, 31, 31, 29, 29,  5,  5,
        5, 30, 30,  1,  1,  1,  2,  2,  2, 31, 31,  4,  4,  4,  5, 5 };

// TODO: move to member variables
int _pickRow = 0;
int _pickCol = 0;
Ipp8uArray _pickValues({ NUM_PICK_INDEX, NUM_PICK_UNIQUE });

std::pair<Ipp32fArray, Ipp8uArray>ShineRepairMask::createRepairedImage(const Ipp16uArray & box4,
   const Ipp8uArray & maskProcess) {
#ifdef _DEBUG
   auto randomizationSeed = 1234u;
#else
   auto randomizationSeed = static_cast<unsigned int>(time(nullptr));
#endif

   _randomRange.seed(randomizationSeed);
   auto upSample = 3.667;
   auto numberPicks = int(upSample*NUM_PICK_UNIQUE);
   auto pickTableDim = int(upSample*PICK_TABLE_DIM);

   // TODO, fix seed generations for IPP arrays
   _pickValues.uniformDistribution(0, 255);

   // %
   // % The input to this function is:
   // %    ShotInfo - information about the shot
   // %    BOX4 - the four-frame motion box assigned at each pixel
   // %    MaskProcess - the mask of pixels to process
   // %
   // % This function computes Z4, four motion compensated frames where the
   // % current frame is not included.  Z4 is used to generate the motion
   // % difference frame.
   // %
   // % The MaskMotion image is returned.  It indicates areas with unreliable
   // % motion.
   // %
   // % The Dst image is returned.
   //
   // [nRow, nCol] = size(MaskProcess);
   auto nCol = maskProcess.getSize().width; auto nRow = maskProcess.getSize().height;
   //
   // % calculate the distance to candidate pixels
   // DistToBlob = bwdist (MaskProcess);
   Ipp8uArray scratch; auto distToBlob = maskProcess.bwdist(scratch);

   //
   // % connected components.  Group close neighbors into a single blob
   // stats = regionprops(DistToBlob<=8, 'PixelIdxList');
   auto regionMask = distToBlob.compareC(6, ::ippCmpLessEq);

   LabelMarker labelMarker; auto labelMask = labelMarker.label(regionMask);
   auto stats = labelMarker.getClusterIndices(labelMask, labelMarker.getNumberOfLabels(), 0, INT_MAX);

   // Src = ShotInfo.RGB{ShotInfo.ProcessFrameIdx};
   const auto src = getSceneFrameInfo(_processFrameIdx).fullRgbImage;

   // Dst = ShotInfo.RGB{ShotInfo.ProcessFrameIdx};
   auto dst = getSceneFrameInfo(_processFrameIdx).fullRgbImage.duplicate();
   //
   // % when distance = 2, the weight is 1
   // % when distance = 6, the weight is 0
   auto slopeSmall = (1.0f - 0.0f) / (1.0f - 2.0f); auto interSmall = 1.0f - slopeSmall * 1;

   // auto slopeLarge = (1.0f - 0.0f) / (2.0f - 6.0f);  // Original but 6 in compareC above must be change to 8
   auto slopeLarge = (1.0f - 0.0f) / (1.0f - 4.0f); auto interLarge = 1.0f - slopeLarge * 2;
   //
   //
   // for i = 1:numel(stats)
   // Idx = stats(i).PixelIdxList;
   regionMask.zero(); 
   auto grainImage = regionMask.duplicate();
   for ( auto&defectIndices : stats)
   //auto f = [&](int blob)
   {
   //   auto &defectIndices : stats[blob];
      // % there will be four observations for every pixel to be processed
      // RGB4 = zeros(4, 3);
      Ipp32fArray RGB4({ 4, 3 }); // Don't swap rows and columns
      RGB4.zero(); // DO WE NEED THIS???

      // Y4 = zeros(4, 1);
      vector<float>Y4(4, 0);

      // for j = 1:numel(Idx
      auto defectSubscripts = maskProcess.getSize().ind2sub(defectIndices);
      auto defectSize = defectIndices.size();

      // To maintain coherence, we need to sweep through a box, so find bounding box
      MtiPoint lowPoint = { IPP_MAX_32S, IPP_MAX_32S }; 
      MtiPoint highPoint = { 0, 0 };
      for (auto&defectSubscript : defectSubscripts)
      {
         lowPoint.x = std::min<int>(lowPoint.x, defectSubscript.x);
         lowPoint.y = std::min<int>(lowPoint.y, defectSubscript.y);
         highPoint.x = std::max<int>(highPoint.x, defectSubscript.x);
         highPoint.y = std::max<int>(highPoint.y, defectSubscript.y);
      }

      auto defectBoundingBox = MtiRect(lowPoint.x, lowPoint.y, highPoint.x - lowPoint.x,
         highPoint.y - lowPoint.y);

    ///  DBTRACE2(defectSubscripts.size(), defectBoundingBox);
      if (std::max<int>(defectBoundingBox.width, defectBoundingBox.height) >= _shineInteractiveParams.maxSize)
      {
         //continue;
      }

      //auto defectGrainBox = grainImage();

      // now add coherence, in reality, we should do entire frame
      auto pickRowLocal = _pickRow; 
      auto pickColLocal = _pickCol;
      for (auto r = lowPoint.y; r < highPoint.y; r++) 
      {
         pickColLocal = _pickCol;
         for (auto c = lowPoint.x; c < highPoint.x; c++)
         {
            auto pickIndex = _pickPattern[int((pickRowLocal % pickTableDim)/upSample)][int((pickColLocal % pickTableDim)/upSample)];

            if (_pickValues[{pickIndex, int((_pickCol % numberPicks)/upSample)}] < 128)
            {
               grainImage[{c, r}] = 127;
            }
            else
            {
               grainImage[{c, r}] = 255;
            }

            pickColLocal++;
         }

         pickRowLocal++;
      }

      _pickRow = pickRowLocal % pickTableDim;
      _pickCol = pickColLocal % pickTableDim;

      for (auto j : range(defectSize))
      {
         // idx = Idx(j);
         auto idx = defectIndices[j];

         // This is ad hoc, we need more steps
         auto slope = defectSize <= 18 * 18 ? slopeSmall : slopeLarge;
         auto inter = defectSize <= 18 * 18 ? interSmall : interLarge;
         // Wei = min(1, max(0, Slope*DistToBlob(idx) + Inter));

         auto wei = std::min<float>(1, std::max<float>(0, slope * distToBlob[idx] + inter));
         // % only process pixels with positive weight
         // if Wei > 0
         if (wei > 0)
         {
            // box = BOX4(idx);
            auto box = box4[idx];

            // Frame = ShotInfo.BOX{box}.Frame;
            auto &shotInfoBox = _shotInfoBoxes[box]; auto frame = shotInfoBox.frame;

            // xoff = ShotInfo.BOX{box}.Offset(:,1);
            // yoff = ShotInfo.BOX{box}.Offset(:,2);
            int xoff[5], yoff[5];
            for (auto i : range(5))
            {
               xoff[i] = shotInfoBox.offset[i].x; yoff[i] = shotInfoBox.offset[i].y;
            }

            // [rowSrc,colSrc] = ind2sub([nRow,nCol], idx);
            auto rc = maskProcess.getSize().ind2sub(idx);
            auto colSrc = rc.x;
            auto rowSrc = rc.y;

            regionMask[{colSrc, rowSrc}] = 1;
            // i4 = 0;
            auto i4 = -1;

            // for i5 = 1:5
            for (auto i5 : range(5))
            {
               // if Frame(i5) ~= ShotInfo.ProcessFrameIdx
               if (frame[i5] != _processFrameIdx)
               {
                  // i4 = i4 + 1;
                  i4 = i4 + 1;
                  //
                  // rowOff = rowSrc + yoff(i5);
                  auto rowOff = rowSrc + yoff[i5];

                  // colOff = colSrc + xoff(i5);
                  auto colOff = colSrc + xoff[i5];
                  //
                  // % legalize offset pixel
                  // rowOff = min(max(1,rowOff),nRow);
                  rowOff = std::min<int>(std::max<int>(0, rowOff), nRow - 1);

                  // colOff = min(max(1,colOff),nCol);
                  colOff = std::min<int>(std::max<int>(0, colOff), nCol - 1);

                  // RGB4(i4,:) = ShotInfo.RGB{Frame(i5)}(rowOff,colOff,:);
                  // TODO replace with pointers.  No copy required
                  auto rgbFrame = getSceneFrameInfo(frame[i5]).fullRgbImage;
                  RGB4[{i4, 0}] = rgbFrame[{colOff, rowOff, 0}];
                  RGB4[{i4, 1}] = rgbFrame[{colOff, rowOff, 1}];
                  RGB4[{i4, 2}] = rgbFrame[{colOff, rowOff, 2}];

                  // Y4(i4) = ShotInfo.Y{Frame(i5)}(rowOff,colOff);
                  Y4[i4] = getSceneFrameInfo(frame[i5]).fullGrayImage[{colOff, rowOff}];
                  // end
               }
               // end
            }

            // % sort Y4 and randomly select from middle two
            // [~, idx0] = sort(Y4);
            vector<int>idx0 = { 0, 1, 2, 3 };
            //bubbleSortAbyB_simple(idx0, Y4);

            // Should we waste time on coherence for small index??
            /* auto v1 = _uniformIntDistribution2(_randomRange);
             auto sourceIndex = v1 == 0 ? 1 : 2; */

            auto sourceIndex = grainImage[{colSrc, rowSrc}] == 255 ? 1 : 2;

            // Dst(rowSrc,colSrc,:) = Wei*RGB4(idx(2),:)' + ...
            // (1-Wei)*squeeze(Src(rowSrc,colSrc,:));
            dst[{colSrc, rowSrc, 0}] = wei * RGB4[{idx0[sourceIndex], 0}] + (1 - wei) * src[MtiPoint{ colSrc, rowSrc, 0 }];
            dst[{colSrc, rowSrc, 1}] = wei * RGB4[{idx0[sourceIndex], 1}] + (1 - wei) * src[MtiPoint{ colSrc, rowSrc, 1 }];
            dst[{colSrc, rowSrc, 2}] = wei * RGB4[{idx0[sourceIndex], 2}] + (1 - wei) * src[MtiPoint{ colSrc, rowSrc, 2 }];
         }

         // end
      }
         // end
   }

  //// MatIO::saveListToMatFile(R"(c:\temp\grain.mat)", grainImage, "grainImage", _pickValues, "pickValues");

   return { dst, regionMask };

}
