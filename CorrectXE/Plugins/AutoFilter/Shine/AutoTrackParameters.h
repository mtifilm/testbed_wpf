#pragma once
#ifndef AutoTrackParametersH
#define AutoTrackParametersH

#include "Ippheaders.h"

class AutoTrackParameters
{
public:
	AutoTrackParameters() = default;

	void setFullImageSize(const MtiSize &size)
	{
		_fullImageSize = size;
		auto ratio = double(_fullImageSize.width) / double(_fullImageSize.height);
		_proxyHeight = int(_proxyWidth / ratio);
	}

	MtiSize getFullImageSize() const { return _fullImageSize; }
	MtiSize getProxySize() const { return { _proxyWidth, _proxyHeight }; }

	double getCenterScale() const { return _centerScale; }
	void setCenterScale(double value) { _centerScale = value; }

	double getPatchThreshold() const { return _patchThreshold; }
	void setPatchThreshold(double value) { _patchThreshold = value; }

	double getCrossNormThreshold() const { return _crossNormThreshold; }
	void setCrossNormThreshold(double value) { _crossNormThreshold = value; }

	int getBoxRadius() const { return _boxRadius; }
	void setBoxRadius(int value) { _boxRadius = value; }

	int getTrajectoryMinLength() const { return _trajectoryMinLength; }
	void setTrajectoryMinLength(int value) { _trajectoryMinLength = value; }

	int getTrajectoryMaxDiscrepancy() const { return _trajectoryMaxDiscrepancy; }
	void setTrajectoryMaxDiscrepancy(int value) { _trajectoryMaxDiscrepancy = value; }

	MtiSize getProxyRadiusSearch() const { return _proxyRadiusSearch; }
	void setProxyRadiusSearch(const MtiSize &value) { _proxyRadiusSearch = value; }

	float getScaleX() const { return float(_fullImageSize.width) / float(_proxyWidth); }
	float getScaleY() const { return float(_fullImageSize.height) / float(_proxyHeight); }
	MtiSize getKernelBox() const { return { 2 * _boxRadius + 1, 2 * _boxRadius + 1 }; }
	
private:
	MtiSize _fullImageSize;
	int _proxyWidth = 512;
	int _proxyHeight = 389;

	double _centerScale = 0.75;
	int _boxRadius = 10;
	double _patchThreshold = 0.02;
	MtiSize _proxyRadiusSearch{ 15, 15 };
	int _trajectoryMinLength = 10;
	int _trajectoryMaxDiscrepancy = 2;
	double _crossNormThreshold = 0.6;
};

//static_assert(std::is_trivially_copyable<AutoTrackParameters>::value,"Need to make life simple");
#endif

