#include "PatchMinsMT.h"
#include "IpaStripeStream.h"

PatchMinsMT::PatchMinsMT(int stripes)
{
	_stripes = stripes;
}

Ipp32fArray PatchMinsMT::computeMin(const Ipp32fArray& inputImage, const MtiSize& kernelSize)
{
	if (inputImage.isEmpty())
	{
		return Ipp32fArray();
	}

	auto widthRadius = kernelSize.width / 2;
	auto heightRadius = kernelSize.height / 2;

	MtiRect interiorRoi(widthRadius, heightRadius, inputImage.getWidth() - 2 * widthRadius, inputImage.getHeight() - 2 * heightRadius);
	auto inputSize = interiorRoi.getSize();
	Ipp32fArray patchMinimums({ interiorRoi.width - 2 * widthRadius, interiorRoi.height - 2 * heightRadius });

	// Make sure the scratch is correct size
	updateScratchData(inputImage.getSize());
	patchMinimums.set({ IPP_MAXABS_32F });

	// This ROI is the data used for analysis, it is smaller than the
	// full size by twice kernel radius because the ROI is moved around to find min.
	IpaStripeStream iss;

	// This is the valid filter ROI
	iss << interiorRoi.inflate(-widthRadius, -heightRadius);
	auto func0 = [&](const MtiRect &stripedRoi, int jobNumber)
	{
		auto validRoi = stripedRoi;
		auto inputRoi = stripedRoi.inflate(widthRadius, heightRadius);

		// The scratch images needs to be the same size as the input but the
		// inflated strips overlap, so we must use two of them
		auto absDiffImage = _absDiffScratch[jobNumber % 2](inputRoi);
		auto scratchArray = _boxFilterScratch[jobNumber % 2](inputRoi);

		// No overlap so we only need 1
		auto normArray = _normArray(validRoi);
		auto interiorImage = inputImage(inputRoi);
		
		// we could do this with relative sizes but 
		auto patchMinimumsTemp = patchMinimums(validRoi - MtiPoint(2* widthRadius, 2* heightRadius));

		// Down sample by a factor of 2
		// We should really have a random down sampling
		// or a checkerboard one.  
		// However, since we are only picking boxes to track, the actual tracking
		// is done at an upper level
		for (auto x = -widthRadius; x <= widthRadius; x+=2)
		{
			for (auto y = -heightRadius; y <= heightRadius; y+=2)
			{
				// Skip absolute zero point
				// Note: when down sampling = 2, the center of radius one is excluded
				//auto z = x * x + y * y;
				// if (z <= 1)
				if ((x == 0) && (y == 0))
				{
					continue;
				}

				auto offsetImage = inputImage(inputRoi + MtiPoint(x, y));
				interiorImage.absDiff(offsetImage, absDiffImage);
				absDiffImage.applyBoxFilterValid(kernelSize, normArray, scratchArray);
				patchMinimumsTemp.minEveryInplace(normArray);
			}
		}
	};

	// WARNING: this is set up for 512 x 389 image
	// The stripes + kernel size CANNOT overlap into the alternate stripe
	//   A kernel of size 21 will go 10 above or below, so min strip is 21 pixels
	// or about 19 jobs. 
	// 12 jobs seems good on 8 and above processor machines but really needs to be
	// computed correctly.
	iss << efu_roi_job(func0);  // CAST is only because Embarcardo has a bug in its resolution
	//iss.run();
	iss.stripe(_stripes);

	return patchMinimums;
}


PatchMinsMT::~PatchMinsMT()
{
}

void PatchMinsMT::updateScratchData(const MtiSize &inputSize)
{
	if (inputSize == _normArray.getSize())
	{
		return;
	}

	// The strips are expanded so the box filter computations are correct
	// So the threads may collide, by using alternating arrays, we don't have to lock
	_boxFilterScratch[0] = Ipp8uArray(inputSize);
	_boxFilterScratch[1] = Ipp8uArray(inputSize);
	_absDiffScratch[0] = Ipp32fArray(inputSize);
	_absDiffScratch[1] = Ipp32fArray(inputSize);
	_normArray = Ipp32fArray(inputSize);
}
