#pragma once
#ifndef TrackDataH
#define TrackDataH

#include "Ippheaders.h"

struct TrackBox
{
	MtiRect box;
	MtiPoint center;
	double xSubpixelCenter = -1;
	double ySubpixelCenter = -1;

	MtiRect expandBox(double rx, double ry) const
	{
		// This expands a box around the center with size sz
		auto rw = int(round(rx * box.width));
		if ((rw % 2) == 0)
		{
			rw = rw + 1;
		}

		int rh = int(round(ry * box.height));
		if ((rh % 2) == 0)
		{
			rh = rh + 1;
		}

		// Create the large box
		MtiRect largeBox;

		//largeBox.center = { int(rx * center.x), yc = int(ry * center.y) };
		largeBox.width = rw;
		largeBox.height = rh;

		// the width, height are always odd
		// point = x + width
		// thus we want floor here.
		largeBox.x = int(center.x - floor(rw / 2));
		largeBox.y = int(center.y - floor(rh / 2));

		return largeBox;
	}
};

struct TrackFrameInfo
{
	Ipp32fArray fullRgbImage;
	Ipp32fArray fullGrayImage;
	Ipp32fArray proxyImage;
	double averageIntensity;
};

// Track data stores the data for a track
// It includes the start box image
//

class SceneTrack
{
public:
	SceneTrack();
	~SceneTrack();

	vector<TrackBox> boxes;
	int startFrameIndex = 1;
	bool active = true;
	Ipp32fArray startProxyImage;
	Ipp32fArray startFullImage;
};

typedef vector<SceneTrack> SceneTracks;
#endif