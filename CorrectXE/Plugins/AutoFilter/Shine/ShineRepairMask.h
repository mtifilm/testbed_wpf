#pragma once
#ifndef ShineRepairMaskH
#define ShineRepairMaskH

#include "SceneTrack.h"
#include "AutoTrackParameters.h"
#include "ShineParameters.h"
#include <map>
#include "Array2D.h"

struct ShineInteractiveParams
{
	double highScaledContrast = 25;
	double lowScaledContrast = 25;
   double highSpatialContrast = 25;
   double lowSpatialContrast = 25;
	double fidelity = 35;
	double minSize = 1;
	double maxSize = IPP_MAXABS_32F; // float max
};

// This is almost the same as TrackFrameInfo, we should
// refactor to a common info
struct ShineFrameInfo
{
	Ipp32fArray fullRgbImage;
	Ipp32fArray fullGrayImage;
	Ipp32fArray proxyImage;
	float averageIntensity = 1;
	int sceneIndex = -1;
};

// We can't afford to allocate lots of small storage
// so we are going to keep this around in loops
// ****WARNING***** DO NOT CHANGE DELTA_SEGMENT_SIZE WITHOUT CHANGING CODE
#define DELTA_BOX_SEG_SIZE 4
#define MIDDLE_SCENE (DELTA_BOX_SEG_SIZE / 2)  // Odd scenes track forwards

struct RequiredIndices
{
	int sceneIndex = 0;
	int boxOffset[DELTA_BOX_SEG_SIZE] = { 0, 0, 0, 0 };
	int getSceneIndex(int offset) const { return sceneIndex + boxOffset[offset]; }
};

class ShineRepairMask
{
public:
	ShineRepairMask() = default;

	inline static int getMinimumShineRepairSceneSize() { return 5; } // frames

	void setOpticalFlow(const OpticalFlowData &opticalFlowData);
	bool pushFrames(const vector<Ipp16uArray> &frames, int startSceneIndex, int repairIndex);
	bool areAllFramesPushedForRepairIndex(int repairIndex) const;
	vector<int> neededSceneIndices(int sceneIndex) const;

	Ipp16uArray repairImage(
		int sceneIndex,
		const ShineInteractiveParams &shineInteractiveParams,
		const Ipp8uArray &importedDefectivePixelMask = Ipp8uArray(),
		bool importedMaskNeedsExtraDilation = false,
		bool importedMaskNeedsFidelityMasking = false);

	Ipp8uArray getDefectivePixelMap(
		int sceneIndex,
		const ShineInteractiveParams &shineInteractiveParams);

	Ipp8uArray getRepairMask() const { return _repairMask; }

   Ipp32fArray getRepairedNormalizedImage() const { return _observedRepairImage; }

private:
	OpticalFlowData _opticalFlowData; // not used delete after debugging
	OpticalFlowTracks _opticalFlowTracks;
	OpticalFlowTrack _constantTrack;

	std::map<int, ShineFrameInfo> _workingFrames;
	vector<ShotInfoBox> _shotInfoBoxes;

   Ipp32fArray _diffRaw, _diffScale;
   Ipp16uArray _box5;

	// We are using parts of the auto params for shine.
	// In reality those parts should be duplicated
	// This needs to be fixed.
	AutoTrackParameters _autoTrackParams;
	ShineParameters _shineParams;
	ShineInteractiveParams _shineInteractiveParams;

	CenterTrackBoxes _proxyBoxes;  // REPLACE these three with Array2D<CenterTrackBoxes> _proxyBoxes2D;
	MtiSize _proxyBoxesSize;

 //	CenterTrackBoxes _fullSizeBoxes;
	int _totalFrames = 0;
	int _currentOncePerFrameStuffSceneIndex = -1;
	int _currentFidelitySceneIndex = -1;
	double _currentFidelitySetting = -1;

	ShineFrameInfo &getSceneFrameInfo(int sceneIndex);
	Array2D<OpticalFlowTrackInfoPerBox> findProxyMotionBlocks(int sceneIndex);
	Array2D<OpticalFlowTrackInfoPerBox> findProxyMotionBlocksFull(int sceneIndex);

	CenterTrackBoxes createFullSizeTiles();
	CenterTrackBoxes createEqualSizeTiles(const MtiSize &imageSize, int boxRadius) const;
	std::pair<OpticalFlowTracks, OpticalFlowTrack> createOpticalFlowTracksFromOpticalFlow(const OpticalFlowData &opticalFlowData) const;

	void doOncePerFrameStuff(int sceneIndex);
	ShineFrameInfo createShineFrameInfo(const Ipp16uArray &rgbImage, int index) const;
//	void createFidelityMask(int sceneIndex);
	void findDefectivePixels(Ipp8uArray defectivePixelMask, bool applyFidelityMask = true);
//	void createDetectionMask();
	void importDetectionMask(const Ipp8uArray &importDefectivePixelMask, bool applyFidelityMask = true);
	void createRepairMask(int dilationRadius);

	// Kevin's new code

	void createShotInfoBox();

   // pixelMotion
	Ipp16uArray computePixelMotionOncePerFrame(MtiPlanar<Ipp32fArray>& Z5, int smoothRadius);

   // blob1a is broken into two functions
   std::pair<Ipp32fArray, Ipp32fArray> computeDiffsOncePerFrame(const MtiPlanar<Ipp32fArray> &Z5);
   static Ipp8uArray createTemporalMask(const Ipp32fArray &diffScale, float temporalContrastPos, float temporalContrastNeg);

   // blob1b
   Ipp8uArray createSpatialMask(
                  const Ipp8uArray &temporalMask,
                  const int minSize,
                  const int maxSize,
                  const double spatialContrastPos,  // 0 - 100 (or a huge number to disable bright)
                  const double spatialContrastNeg); // 0 - 100 (or a huge number to disable dark)

   // blob2
	Ipp16uArray createDetectionMask(
                  const Ipp16uArray& box5,
                  const Ipp8uArray& spatialMask
                  );

   // blob3
	Ipp8uArray createMotionMask(const Ipp16uArray& box4, float motionTolerance);

   // blob4
	Ipp8uArray createProcessMask(const Ipp8uArray& detectMask, const Ipp8uArray& motionMask);

   // blob5
	std::pair<Ipp32fArray, Ipp8uArray> createRepairedImage(const Ipp16uArray& box4, const Ipp8uArray& maskProcess);

	////////////////////
	Ipp16uArray createRepairedImage(int sceneIndex);

	MtiPlanar<Ipp32fArray> _z5;
	vector<Ipp32fArray> _detectionImages;
	vector<Ipp32fArray> _detectionImages4;
	vector<Ipp32fArray> _observedImages;
	Ipp32fArray _observedRepairImage;      // this is created from _observedImages

	Ipp32fArray _observedFidelity;
	Ipp32fArray _scaledContrastImage;
	Ipp8uArray _repairMask;

	Ipp8uArray _fidelityMask;
	Ipp8uArray _defectivePixelMask;
	Ipp8uArray _scratchArray;
	Ipp32fArray _detectionMask;

	// For detection and full motion
	Ipp16uArray _motionMask;
	Ipp32fArray _pseudoSdArray;

	// Some Kevin variables
	int _processFrameIdx = -1;

	// Save a random number generator
	std::mt19937 _randomRange{ std::random_device{}() };
	std::uniform_int_distribution<std::mt19937::result_type> _uniformIntDistribution2{0, 1};
};

#endif
