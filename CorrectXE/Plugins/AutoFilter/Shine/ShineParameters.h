#pragma once
#ifndef ShineParametersH
#define ShineParametersH

class ShineParameters
{
public:
	ShineParameters() = default;

//	// These are the dynamic  parameters
//	double getBrightContrast() const { return _brightContrast; }
//	void setBrightContrast(double value) { _brightContrast = value; }
//
//	double getDarkContrast() const { return _darkContrast; }
//	void setDarkContrast(double value) { _darkContrast = value; }
//
//	double getFidelity() const { return _fidelity; }
//	void setFidelity(double value) { _fidelity = value; }

	// These really do not change
	double getTrackCloseness() const { return _trackCloseness; }
	void setTrackCloseness(double value) { _trackCloseness = value; }

	int getProxyTileBoxRadius() const { return _proxyTileBoxRadius; }
	void setProxyTileBoxRadius(int value) { _proxyTileBoxRadius = value; }

	int getContrastBins() const { return _contrastBins; }
	void setContrastBins(int value) { _contrastBins = value; }


private:
	// These are mask generation parameters
	int _proxyTileBoxRadius = 8;
	double _trackCloseness = 0.065;
	int _contrastBins = 100;

	// These are interactive parameters
//	double _brightContrast = 25;
//	double _darkContrast = 25;
//	double _fidelity = 35;

};

#endif

