/*
	File:		RunAF.cpp
	Written by:	Kevin Manbeck
	Date:		April 26, 2003

	This file is used to call the auto process auto filter library.

*/
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "machine.h"
#ifndef _WINDOWS
#include <sys/time.h>
#include <unistd.h>
#endif
#include <math.h>
#include "systemid.h"
#include "MTIio.h"
#include "getopt.h"
#include "IniFile.h"
#include "AlgorithmAF.h"
#include "MediaFileIO.h"
#include "ImageFormat3.h"
#include "ImageFileMediaAccess.h"
#include "Extractor.h"

#define NULL_VALUE -1000

void printUsage (char *cpArgV)
{
  fprintf (stderr, "Usage: %s <options>\n", cpArgV);
  fprintf (stderr, "List of options:\n");
  fprintf (stderr, "\t-a spatial contrast\n");
  fprintf (stderr, "\t-c command file\n");
  fprintf (stderr, "\t-d contrast direction\n");
  fprintf (stderr, "\t-e severity\n");
  fprintf (stderr, "\t-h this help message\n");
  fprintf (stderr, "\t-l 3 layer separation flag\n");
  fprintf (stderr, "\t-m component mask\n");
  fprintf (stderr, "\t-o horizontal motion\n");
  fprintf (stderr, "\t-p principal component\n");
//  fprintf (stderr, "\t-r principal tolerance\n");
  fprintf (stderr, "\t-s size\n");
  fprintf (stderr, "\t-t temporal contrast\n");
  fprintf (stderr, "\t-v vertical motion\n");
  fprintf (stderr, "\t-A input file name\n");
  fprintf (stderr, "\t-B output file name\n");
  fprintf (stderr, "\t-F first frame index\n");
  fprintf (stderr, "\t-L last frame index\n");
  fprintf (stderr, "\t-P to print out run time information\n");
  fprintf (stderr, "\t-V to print out version\n");

  fprintf (stderr,
	"\nNote:  command line arguments override command file values\n");
}  /* printUsage */

int ParseCommandFile (string &strCommandFile, CAutoFilterParameters &paramAF,
	string &strSampleFileIn, string &strSampleFileOut, long &lFirstFrameIndex, 
	long &lLastFrameIndex)
{

  // set paramAF to its default values
  paramAF.InitGeneral ();
  paramAF.InitSetting (0);
 
/*
	If no command file, return
*/

  if (strCommandFile == "")
   {
    return 0;
   }

  if (strstr (strCommandFile.c_str(), ".cmd") == NULL)
    strCommandFile += ".cmd";
  CIniFile *ini = CreateIniFile (strCommandFile);
  if (ini == NULL)
   {
    return -1;
   }

  // Init the general settings
  paramAF.InitGeneral ();

  // read in the general values
  paramAF.lPrincipalComponent = ini->ReadInteger ("General",
	"PrincipalComponent", paramAF.lPrincipalComponent);
  paramAF.lComponentMask = ini->ReadInteger ("General",
	"ComponentMask", paramAF.lComponentMask);
  paramAF.bSeparateLayerFlag = ini->ReadBool ("General",
	"SeparateLayerFlag", paramAF.bSeparateLayerFlag);
  strSampleFileIn = ini->ReadFileNameCreate("General", "FileNameIn", strSampleFileIn);
  strSampleFileOut = ini->ReadFileNameCreate("General", "FileNameOut", strSampleFileOut);
  lFirstFrameIndex = ini->ReadIntegerCreate ("General", "FirstFrame", 0);
  lLastFrameIndex = ini->ReadIntegerCreate ("General", "LastFrame", 0);

  // read in the setting specific settings
  int iSetting = 0;
  char caSection[16];
  sprintf (caSection, "AutoFilter%d", iSetting);
  while (ini->SectionExists(caSection))
   {
    paramAF.InitSetting (iSetting);

    paramAF.faMinVal[iSetting][0] = ini->ReadDouble (caSection,
		"MinVal0", paramAF.faMinVal[iSetting][0]);
    paramAF.faMaxVal[iSetting][0] = ini->ReadDouble (caSection,
		"MaxVal0", paramAF.faMaxVal[iSetting][0]);
    paramAF.faMinVal[iSetting][1] = ini->ReadDouble (caSection,
		"MinVal1", paramAF.faMinVal[iSetting][1]);
    paramAF.faMaxVal[iSetting][1] = ini->ReadDouble (caSection,
		"MaxVal1", paramAF.faMaxVal[iSetting][1]);
    paramAF.faMinVal[iSetting][2] = ini->ReadDouble (caSection,
		"MinVal2", paramAF.faMinVal[iSetting][2]);
    paramAF.faMaxVal[iSetting][2] = ini->ReadDouble (caSection,
		"MaxVal2", paramAF.faMaxVal[iSetting][2]);

    paramAF.faContrastSpatial[iSetting] = ini->ReadDouble (caSection,
		"ContrastSpatial", paramAF.faContrastSpatial[iSetting]);
    paramAF.faContrastTemporal[iSetting] = ini->ReadDouble (caSection,
		"ContrastTemporal", paramAF.faContrastTemporal[iSetting]);
    paramAF.laContrastDirection[iSetting] = ini->ReadInteger (caSection,
		"ContrastDirection", paramAF.laContrastDirection[iSetting]);

    paramAF.faMotionOffsetRow[iSetting] = ini->ReadDouble (caSection,
		"MotionOffsetRow", paramAF.faMotionOffsetRow[iSetting]);
    paramAF.faMotionOffsetCol[iSetting] = ini->ReadDouble (caSection,
		"MotionOffsetCol", paramAF.faMotionOffsetCol[iSetting]);
    paramAF.faMotionThresh[iSetting] = ini->ReadDouble (caSection,
		"MotionThresh", paramAF.faMotionThresh[iSetting]);
    paramAF.faSize[iSetting] = ini->ReadDouble (caSection,
		"Size", paramAF.faSize[iSetting]);
    paramAF.faMaster[iSetting] = ini->ReadDouble (caSection,
		"Master", paramAF.faMaster[iSetting]);
    paramAF.faSeverity[iSetting] = ini->ReadDouble (caSection,
		"Severity", paramAF.faSeverity[iSetting]);
    iSetting++;
    sprintf (caSection, "AutoFilter%d", iSetting);
   }

  delete ini;

  return 0;
}  /* ParseCommandFile */

int InitializeAF (CAlgorithmAF &algoAF, CAutoFilterParameters &paramAF,
	const string &strSampleNameIn, long lFirstFrameIndex)
{
  string strFileName;
  int iRet;
  CImageFileIO ifIO;

  // make call to initialize algoAF
  algoAF.Initialize ();

  // determine the image size
  CImageFileMediaAccess::GenerateFileName (strSampleNameIn, strFileName,
                                          lFirstFrameIndex);
  iRet = ifIO.InitFromExistingFile (strFileName);
  if (iRet)
    return iRet;
  iRet = algoAF.SetDimension (
		ifIO.getImageFormatPtr()->getLinesPerFrame(),
		ifIO.getImageFormatPtr()->getPixelsPerLine(),
		3,  // should really ask the image format for this value
		ifIO.getImageFormatPtr()->getActivePictureRect().top,
		ifIO.getImageFormatPtr()->getActivePictureRect().bottom + 1,
		ifIO.getImageFormatPtr()->getActivePictureRect().left,
		ifIO.getImageFormatPtr()->getActivePictureRect().right + 1
                             );
  if (iRet)
    return iRet;

  // set the image bounds
  MTI_UINT16 usaMin[3], usaMax[3];
  ifIO.getImageFormatPtr()->getComponentValueMin (usaMin);
  ifIO.getImageFormatPtr()->getComponentValueMax (usaMax);
  iRet = algoAF.SetImageBounds (usaMin, usaMax);
  if (iRet)
    return iRet;

  // set the parameters
  iRet = algoAF.AssignParameter (&paramAF);
  if (iRet)
    return iRet;

  // do not keep track of original values
  algoAF.SetPixelRegionList (NULL);

  return 0;
}  /* InitializeAF */

int RunProcess (CAlgorithmAF &algoAF, const string &strSampleNameIn,
	const string &strSampleNameOut, long lFirstFrameIndex,
	long lLastFrameIndex)
{
  CImageFileIO ifIO;
  MTI_UINT8 *ucpRawData;
  MTI_UINT16 *uspUnpackedData[5];
  long laUnpackedIndex[5];
  MTI_UINT16 *uspProcessData[3];
  long laProcessIndex[3];
  int iRet = 0;
  int iCnt;
  string strFileName;
  long lFrame;
  CExtractor extractor;
  int iNeg2, iNeg1, iZero, iPos1, iPos2, iDiff;

  ucpRawData = NULL;
  for (iCnt = 0; iCnt < 5; iCnt++)
    uspUnpackedData[iCnt] = NULL;

  // get ImageFormat from first frame
  CImageFileMediaAccess::GenerateFileName (strSampleNameIn, strFileName,
                                          lFirstFrameIndex);
  iRet = ifIO.InitFromExistingFile (strFileName);
  if (iRet)
    goto free_and_return;

  // allocate storage for the raw file IO
  ucpRawData = (MTI_UINT8 *) malloc (
		ifIO.getImageFormatPtr()->getBytesPerField());
  if (ucpRawData == NULL)
    goto free_and_return;

  // allocate storage for the unpacked data
  for (iCnt = 0; iCnt < 5; iCnt++)
   {
    uspUnpackedData[iCnt] = (MTI_UINT16 *) malloc (
		ifIO.getImageFormatPtr()->getLinesPerFrame() *
		ifIO.getImageFormatPtr()->getPixelsPerLine() *
		3 * sizeof (MTI_UINT16));
    if (uspUnpackedData[iCnt] == NULL)
      goto free_and_return;
   }

  // init the Unpacked indicies
  for (iCnt = 0; iCnt < 5; iCnt++)
    laUnpackedIndex[iCnt] = NULL_VALUE;

  // run through all the frames requested
  for (lFrame = lFirstFrameIndex; lFrame <= lLastFrameIndex; lFrame++)
   {
    // read in 5 frames
    for (long lF = lFrame - 2; lF <= lFrame + 2; lF++)
     {
      // is this frame already read in?
      bool bFoundIt = false;
      for (iCnt = 0; iCnt < 5; iCnt++)
       {
        if (laUnpackedIndex[iCnt] < lFrame - 2)
         {
          laUnpackedIndex[iCnt] = NULL_VALUE;
         }
        else if (laUnpackedIndex[iCnt] > lFrame + 2)
         {
          laUnpackedIndex[iCnt] = NULL_VALUE;
         }
        else if (laUnpackedIndex[iCnt] == lF)
         {
          bFoundIt = true;
         }
       }

      if (bFoundIt == false && lF >= 0)
       {
        // must read it in now.
        CImageFileMediaAccess::GenerateFileName (strSampleNameIn, strFileName,
                                                lF);
printf ("Reading from %s\n", strFileName.c_str());
        iRet = ifIO.ReadImage (strFileName, &ucpRawData);
        if (iRet)
         {
          // if this outside the requested range, do not return an error
          if (lF >= lFirstFrameIndex && lF <= lLastFrameIndex)
            goto free_and_return;
         }
        else
         {
          // find an empty buffer
          for (iCnt = 0; iCnt < 5; iCnt++)
           {
            if (laUnpackedIndex[iCnt] == NULL_VALUE)
              break;
           }

          if (iCnt == 5)
           {
            iRet = -1;
            goto free_and_return;
           }
          
          iRet = extractor.extractSubregionWithinFrame (
		uspUnpackedData[iCnt], &ucpRawData, 
		ifIO.getImageFormatPtr(), 
		ifIO.getImageFormatPtr()->getActivePictureRect());
          laUnpackedIndex[iCnt] = lF;
         }
       }
     }

    // decide which 3 frames to pass to the AutoFilter alogrithm

    iNeg2 = NULL_VALUE;
    iNeg1 = NULL_VALUE;
    iZero = NULL_VALUE;
    iPos1 = NULL_VALUE;
    iPos2 = NULL_VALUE;

    for (iCnt = 0; iCnt < 5; iCnt++)
     {
      iDiff = lFrame - laUnpackedIndex[iCnt];
      switch (iDiff)
       {
        case -2:
          iNeg2 = iCnt;
        break;
        case -1:
          iNeg1 = iCnt;
        break;
        case 0:
          iZero = iCnt;
        break;
        case 1:
          iPos1 = iCnt;
        break;
        case 2:
          iPos2 = iCnt;
        break;
       }
     }

    if (iNeg1 != NULL_VALUE && iZero != NULL_VALUE && iPos1 != NULL_VALUE)
     {
      laProcessIndex[0] = -1;
      laProcessIndex[1] = 0;
      laProcessIndex[2] = 1;
      uspProcessData[0] = uspUnpackedData[iNeg1];
      uspProcessData[1] = uspUnpackedData[iZero];
      uspProcessData[2] = uspUnpackedData[iPos1];
     }
    else if (iNeg2 != NULL_VALUE && iNeg1 != NULL_VALUE && iZero != NULL_VALUE)
     {
      laProcessIndex[0] = -2;
      laProcessIndex[1] = -1;
      laProcessIndex[2] = 0;
      uspProcessData[0] = uspUnpackedData[iNeg2];
      uspProcessData[1] = uspUnpackedData[iNeg1];
      uspProcessData[2] = uspUnpackedData[iZero];
     }
    else if (iZero != NULL_VALUE && iPos1 != NULL_VALUE && iPos2 != NULL_VALUE)
     {
      laProcessIndex[0] = 0;
      laProcessIndex[1] = 1;
      laProcessIndex[2] = 2;
      uspProcessData[0] = uspUnpackedData[iZero];
      uspProcessData[1] = uspUnpackedData[iPos1];
      uspProcessData[2] = uspUnpackedData[iPos2];
     }
    else
     {
      iRet = -1;
      goto free_and_return;
     }

    // call AutoFilter processing

    iRet = algoAF.SetSrcImgPtr (uspProcessData, laProcessIndex);
    if (iRet)
      goto free_and_return;
    algoAF.SetDstImgPtr (uspUnpackedData[iZero]);

    algoAF.Process();

    // write out the results
    iRet = extractor.replaceSubregionWithinFrame (&ucpRawData, 
		ifIO.getImageFormatPtr(), 
		ifIO.getImageFormatPtr()->getActivePictureRect(),
		uspUnpackedData[iZero]);
    if (iRet)
      goto free_and_return;

    CImageFileMediaAccess::GenerateFileName (strSampleNameOut, strFileName,
                                             lFrame);
printf ("Writing to %s\n", strFileName.c_str());
    iRet = ifIO.CreateNewImage (strFileName, &ucpRawData);
    if (iRet)
     {
      goto free_and_return;
     }
   }


free_and_return:
  free (ucpRawData);
  for (iCnt = 0; iCnt < 5; iCnt++)
   {
    free (uspUnpackedData[iCnt]);
   }

  return iRet;
}  /* RunProcess */

int CheckSystem ()
{
  MTI_INT64 ullSysID;
  ullSysID = sysid64();
  if (
      ullSysID != 2952857735ULL &&	/* mti onyx */
      ullSysID != 896434483358ULL &&	/* MTI rho */
      ullSysID != 12399737352ULL &&	/* MTI hydra */
      ullSysID != 12399728858ULL &&	/* MTI hydra02 */
      ullSysID != 12399580149ULL &&	/* MTI hydra03 */
      ullSysID != 12399592113ULL &&	/* MTI hydra04 */
      ullSysID != 1762430938ULL &&      /* MTI oxy */
      ullSysID != 27305198700ULL &&     /* MTI sigma */
      ullSysID != 74141609919ULL  &&   // MTI blaise
      ullSysID != 207368728281ULL &&   // WB Linux cl1
      ullSysID != 207368728196ULL &&   // WB Linux cl2
      ullSysID != 207368728523ULL &&   // WB Linux cl3
      ullSysID != 207368728870ULL &&   // WB Linux cl4
      ullSysID != 207368728641ULL &&   // WB Linux cl5
      ullSysID != 207368727453ULL &&   // WB Linux cl6
      ullSysID != 207368728723ULL &&   // WB Linux cl7
      ullSysID != 207368728599ULL &&   // WB Linux cl8
      ullSysID != 207368776932ULL &&   // WB Linux cl9
      ullSysID != 207368776789ULL &&   // WB Linux cl10
      ullSysID != 207368777179ULL &&   // WB Linux cl11
      ullSysID != 207368776917ULL &&   // WB Linux cl12
      ullSysID != 207368776937ULL &&   // WB Linux cl13
      ullSysID != 207368776851ULL &&   // WB Linux cl14
      ullSysID != 207368776393ULL &&   // WB Linux cl15
      ullSysID != 207368724852ULL &&   // WB Linux cl16
      ullSysID != 33974569345ULL  &&   // WB Linux mn1
      ullSysID != 962481166441ULL &&   // WB Linux arjunpc
      ullSysID != 70242581190ULL  &&   // WB Linux andy-test
      ullSysID != 345929565266ULL &&   // WB Linux andyc-2
      ullSysID != 207368746484ULL &&   // WB Linux wbvo-gateway
      ullSysID != 2952927254ULL &&     // WB SGI ltlbob
      ullSysID != 2952950072ULL &&     // WB SGI ltlbill
      ullSysID != 3523225603ULL &&     // WB SGI newbill
      ullSysID != 3523225602ULL &&     // WB SGI nas
      ullSysID != 2952885268ULL &&     // WB SGI sp1
      ullSysID != 2952950648ULL &&     // WB SGI sp2
      ullSysID != 2952864403ULL &&     // WB SGI onyx2
      ullSysID != 2952948019ULL &&     // WB SGI mti2
      ullSysID != 1762571071ULL &&     // WB SGI andyc
      ullSysID != 1762653465ULL        // WB SGI arjunSgi
     )
   {
    return -1;
   }
  else
   {
    return 0;
   }
}  /* CheckSystem */

main (int iArgC, char **cpArgV)
{
  char caVersion[] = "AutoFilter, version 030426";
  CAlgorithmAF algoAF;
  CAutoFilterParameters paramAF;
  string strSampleNameIn, strSampleNameOut;
  int iRet;
  long lFirstFrameIndex, lLastFrameIndex;

  CreateApplicationName (cpArgV[0]);

  if (iArgC < 2)
   {
    printUsage (cpArgV[0]);
    exit (1);
   }

  if (CheckSystem())
   {
//    fprintf (stderr, "An unlicensed machine\n");
//    exit (1);
   }

/*
	Check for user input
*/

  string strCommandFile = "";
  long lDirectionContrast = NULL_VALUE;
  float fSeverity = NULL_VALUE;
  float fMotionH = NULL_VALUE;
  long lComponentMask = NULL_VALUE;
  long lSeparateLayerFlag = NULL_VALUE;
  long lPrincipalComponent = NULL_VALUE;
  float fPrincipalTol = NULL_VALUE;
  long lSize = NULL_VALUE;
  float fContrastTemporal = NULL_VALUE;
  float fContrastSpatial = NULL_VALUE;
  float fMotionV = NULL_VALUE;
  string strSampleNameInCommandLine = "";
  string strSampleNameOutCommandLine = "";
  long lFirstFrameIndexCommandLine = NULL_VALUE;
  long lLastFrameIndexCommandLine = NULL_VALUE;
  

  int iOpt;
  while ( (iOpt = getopt (iArgC, cpArgV, "a:c:d:e:h:l:m:o:p:r:s:t:v:A:B:F:L:PV"))
		!= EOF)
   {
    switch (iOpt)
     {
      case 'a':
        fContrastSpatial = atof(optarg);
      break;
      case 'c':
        strCommandFile = optarg;
      break;
      case 'd':
        lDirectionContrast = atoi(optarg);
      break;
      case 'e':
        fSeverity = atof(optarg);
      break;
      case 'l':
        lSeparateLayerFlag = atoi(optarg);
      break;
      case 'm':
        lComponentMask = atoi(optarg);
      break;
      case 'o':
        fMotionH = atof (optarg);
      break;
      case 'p':
        fPrincipalTol = atof(optarg);
      break;
      case 's':
        lSize = atoi(optarg);
      break;
      case 't':
        fContrastTemporal = atof(optarg);
      break;
      case 'v':
        fMotionV = atof(optarg);
      break;
      case 'A':
        strSampleNameInCommandLine = optarg;
      break;
      case 'B':
        strSampleNameOutCommandLine = optarg;
      break;
      case 'F':
        lFirstFrameIndexCommandLine = atoi(optarg);
      break;
      case 'L':
        lLastFrameIndexCommandLine = atoi(optarg);
      break;
      case 'V':
        fprintf (stderr, "%s\n", caVersion);
      break;
      case 'P':
        algoAF.setPrintOut (true);
      break;
      case 'h':
      default:
        printUsage(cpArgV[0]);
        exit (1);
     }
   }

  // parse the command file
  if (ParseCommandFile (strCommandFile, paramAF, strSampleNameIn,
	strSampleNameOut, lFirstFrameIndex, lLastFrameIndex))
   {
    fprintf (stderr, "Unable to parse the command file: %s\n",
		strCommandFile.c_str());
    exit (1);
   }

  // override any parameter values with the command line arguments
  for (int iSetting = 0; iSetting < paramAF.lNSetting; iSetting++)
   {
    if (lDirectionContrast != NULL_VALUE)
     {
      paramAF.laContrastDirection[iSetting] = lDirectionContrast;
     }
    if (fSeverity != NULL_VALUE)
     {
      paramAF.faSeverity[iSetting] = fSeverity;
     }
    if (fMotionH != NULL_VALUE)
     {
      paramAF.faMotionOffsetCol[iSetting] = fMotionH;
     }
    if (fPrincipalTol != NULL_VALUE)
     {
     }
    if (lSize != NULL_VALUE)
     {
      paramAF.faSize[iSetting] = lSize;
     }
    if (fContrastTemporal != NULL_VALUE)
     {
      paramAF.faContrastTemporal[iSetting] = fContrastTemporal;
     }
    if (fContrastSpatial != NULL_VALUE)
     {
      paramAF.faContrastSpatial[iSetting] = fContrastSpatial;
     }
    if (fMotionV != NULL_VALUE)
     {
      paramAF.faMotionOffsetRow[iSetting] = fMotionV;
     }
   }

  if (lComponentMask != NULL_VALUE)
   {
    paramAF.lComponentMask = lComponentMask;
   }
  if (lSeparateLayerFlag != NULL_VALUE)
   {
    paramAF.bSeparateLayerFlag = lSeparateLayerFlag;
   }
  if (lPrincipalComponent != NULL_VALUE)
   {
    paramAF.lPrincipalComponent = lPrincipalComponent;
   }
  if (strSampleNameInCommandLine != "")
   {
    strSampleNameIn = strSampleNameInCommandLine;
   }
  if (strSampleNameOutCommandLine != "")
   {
    strSampleNameOut = strSampleNameOutCommandLine;
   }
  if (lFirstFrameIndexCommandLine != NULL_VALUE)
   {
    lFirstFrameIndex = lFirstFrameIndexCommandLine;
   }
  if (lLastFrameIndexCommandLine != NULL_VALUE)
   {
    lLastFrameIndex = lLastFrameIndexCommandLine;
   }
  
  if (lLastFrameIndex < lFirstFrameIndex)
   {
    fprintf (stderr, "Illegal frame range:  %d to %d\n", lFirstFrameIndex,
	lLastFrameIndex);
    exit (1);
   }

  if (strSampleNameIn == "" || strSampleNameOut == "")
   {
    fprintf (stderr, "Illegal sample frame names\n");
    exit (1);
   }

  // initialize the AlgorithmAF class
  iRet = InitializeAF (algoAF, paramAF, strSampleNameIn, lFirstFrameIndex);
  if (iRet)
   {
    fprintf (stderr, "Unable to initialize: %d\n", iRet);
    exit (1);
   }

  // run the processing
  iRet = RunProcess (algoAF, strSampleNameIn, strSampleNameOut,
	lFirstFrameIndex, lLastFrameIndex);
  if (iRet)
   {
    fprintf (stderr, "Unable to complete the processing: %d\n", iRet);
    exit (1);
   }


  // all done, so exit
  fprintf (stderr, "Terminated normally\n");
  exit (0);
}  /* main */
