//---------------------------------------------------------------------------

#ifndef ShinePreprocOutputCacheH
#define ShinePreprocOutputCacheH
//---------------------------------------------------------------------------

#include "ShinePreprocOutput.h"
#include "ThreadLocker.h"

#include <map>
#include <string>
using std::map;
using std::string;
//---------------------------------------------------------------------------

class ShinePreprocOutputCache
{
public:
   ShinePreprocOutputCache(const vector<string> &directoryChain, const string &filenameExtension);
   ~ShinePreprocOutputCache();

   void addPreprocOutputToCache(const string &id, ShinePreprocOutputSharedPtr dataToCache);
   void removePreprocOutputFromCache(const string &id);
   ShinePreprocOutputSharedPtr retrievePreprocOutputFromCache(const string &id);
   int getGeneration() { return _generation; }

   static string makeCacheId(int inFrame, int outFrame);

private:
   map<string, ShinePreprocOutputSharedPtr> _cacheMap;
   CSpinLock _cacheLock;
   vector<string> _directoryChain;
   string _filenameExtension;
   int _generation = 0;

   string makeCacheEntryFileName(int chainLinkNumber, const string &id);
   void clearMainMemoryCache();

   int writeCacheEntryToDisk(const string &id, ShinePreprocOutputSharedPtr &dataToCache);
   int deleteCacheEntryFromDisk(const string &id);
   ShinePreprocOutputSharedPtr readCacheEntryFromDisk(const string &id);
};
//---------------------------------------------------------------------------

#endif
