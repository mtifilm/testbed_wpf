#ifndef AutoFilterToolH
#define AutoFilterToolH

// AutoFilter.h: interface for the CAutoFilter class.
//
/*
$Header: /usr/local/filmroot/Plugins/AutoFilter/AutoFilterTool.h,v 1.40.2.22 2009/07/15 15:12:06 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "AlgorithmAF_MT.h"
#include "AutoFilterToolParameters.h"
#include "ShinePreprocOutput.h"
#include "ShinePreprocOutputCache.h"
#include "ShineToolParameters.h"

#include "Event.h"
#include "IniFile.h"
#include "HRTimer.h"
#include "RegionOfInterest.h"
#include "ToolObject.h"

#include <atomic>
#include <tuple>
using std::tuple;

//////////////////////////////////////////////////////////////////////

// AutoFilter's Tool Number (16-bit number that uniquely identifies this tool)
#define AUTOFILTER_TOOL_NUMBER    6

// AutoFilter Tool Command Numbers
#define AF_CMD_NOOP                               0
#define AF_CMD_TOGGLE_HIGHLIGHTS_AND_FIXES        1
#define AF_CMD_TOGGLE_PROVISIONAL_AND_ORIG        2
#define AF_CMD_FOCUS_ON_MAX_SIZE                  3
#define AF_CMD_FOCUS_ON_DUST_SIZE                 4
#define AF_CMD_FOCUS_ON_MIN_CONTRAST              5
#define AF_CMD_FOCUS_ON_MIN_FIX_CONFIDENCE        6
#define AF_CMD_FOCUS_ON_MOTION                    7
// Available                                      8
#define AF_CMD_FOCUS_ON_CORRECTION_EDGE_BLUR      9
#define AF_CMD_CYCLE_DETECT_CHANNEL              10
#define AF_CMD_FOCUS_ON_PROCESS_CHANNELS         11
#define AF_CMD_TOGGLE_COLOR_PROFILE_RANGE        12
#define AF_CMD_FOCUS_ON_COLOR_PROFILE_SLIDERS    13
#define AF_CMD_SELECT_PREVIOUS_FILTER            14
#define AF_CMD_SELECT_NEXT_FILTER                15
#define AF_CMD_TOGGLE_USE_SELECTED_FILTER        16
#define AF_CMD_NEW_FILTER                        17
#define AF_CMD_LOAD_FILTER                       18
#define AF_CMD_UNLOAD_FILTER                     19
#define AF_CMD_DELETE_FILTER                     20
#define AF_CMD_SAVE_FILTER_SETTINGS_AS_DEFAULTS  21
#define AF_CMD_RESET_FILTER_SETTINGS_TO_DEFAULTS 22
#define AF_CMD_CREATE_FILTER_SET                 23
#define AF_CMD_UPDATE_FILTER_SET                 24
#define AF_CMD_LOAD_FILTER_SET                   25
#define AF_CMD_DELETE_FILTER_SET                 26
#define AF_CMD_REJECT_FIXES                      27
#define AF_CMD_REPEAT_LAST_GOV_OR_ROI            28
#define AF_CMD_REPEAT_TOGGLE_GOV_OR_ROI          29
#define AF_CMD_TOGGLE_DEBRIS_CONTRAST            30
#define AF_CMD_TOGGLE_GRAIN                      31
#define AF_CMD_RESET_GRAIN                       32
#define AF_CMD_SELECT_PREV_GRAIN_SETTING         33
#define AF_CMD_SELECT_NEXT_GRAIN_SETTING         34
#define AF_CMD_FOCUS_ON_MIN_SIZE                 35
#define AF_CMD_SHOW_FILTER_STATS                 36
#define AF_CMD_FOCUS_ON_GRAIN_FILTER_PARAM       37
#define AF_CMD_TOGGLE_RED_OVERLAY                38
#define AF_CMD_TOGGLE_CONTINUOUS_PREVIEW_MODE    39
#define AF_CMD_TOGGLE_OP_MODE                    40
#define AF_CMD_ALT_ON                            41
#define AF_CMD_ALT_OFF                           42


//////////////////////////////////////////////////////////////////////
// Pixel Mask bit flags (copied from Repair.h)

#define RM_NONE 0x0        // do not use this pixel
#define RM_SELECTED 0x1    // defective pixel should be replaced
#define RM_SURROUND 0x2    // area around defective area
#define RM_BORDER 0x10     // border region that gets blended

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;
class CRegionOfInterest;

//////////////////////////////////////////////////////////////////////
// Encryption for "CORRECT-AUTOFILTER"
extern MTI_UINT32 FEATURE_CORRECT_AUTOFILTER[];
#define ENV_CORRECT_AUTOFILT "9d9c31657498f76de8275b0de413ffced808e790bff7779503325bf42c0b6a1d"
#define ENK_CORRECT_AUTOFILT 0xf9218e4

// Encryption for "CORRECT-ALPHAFILTER"
extern MTI_UINT32 FEATURE_CORRECT_ALPHAFILTER[];
#define ENV_CORRECT_ALPHAFILT "9d9c31657498f76de8275b0de413ffced808e790bff7779503325bf42c0b6a1d"
#define ENK_CORRECT_ALPHAFILT 0xf9218e4

//////////////////////////////////////////////////////////////////////

enum AnalysisScope
{
   CURRENT_SHOT,
   MARKED_RANGE,
   FULL_CLIP
};

#define RED_FILTER_CHANNEL   0
#define GREEN_FILTER_CHANNEL 1
#define BLUE_FILTER_CHANNEL  2
#define ALPHA_FILTER_CHANNEL 3

#define INVALID_FRAME_INDEX (-1)


enum ShineControlState
{
   SHINE_STATE_IDLE,
   SHINE_STATE_GOT_AN_ERROR,
   SHINE_STATE_NEED_TO_RUN_PREPROCESSOR,
   SHINE_STATE_PREPROCESS_ANOTHER_SHOT,
   SHINE_STATE_PREPROCESSING,
   SHINE_STATE_PREPROCESSING_PAUSED,
   SHINE_STATE_PREPROCESSING_COMPLETE,
   SHINE_STATE_NEED_TO_START_SINGLE_FRAME_PREVIEW,
   SHINE_STATE_SINGLE_FRAME_PREVIEWING,
   SHINE_STATE_SINGLE_FRAME_PREVIEW_COMPLETE,
   SHINE_STATE_NEED_TO_START_SINGLE_FRAME_RENDER,
   SHINE_STATE_SINGLE_FRAME_RENDERING,
   SHINE_STATE_SINGLE_FRAME_RENDER_COMPLETE,
   SHINE_STATE_NEED_TO_START_MULTI_FRAME_PREVIEW,
   SHINE_STATE_MULTI_FRAME_PREVIEWING,
   SHINE_STATE_MULTI_FRAME_PREVIEW_COMPLETE,
   SHINE_STATE_NEED_TO_START_MULTI_FRAME_RENDER,
   SHINE_STATE_MULTI_FRAME_RENDERING,
   SHINE_STATE_MULTI_FRAME_RENDER_COMPLETE,
};


//////////////////////////////////////////////////////////////////////

class CAutoFilter : public CToolObject
{
public:
   CAutoFilter(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CAutoFilter();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool toolHideQuery();
   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   // Tool GUI
   virtual int toolHide();    // Hide the tool's GUI
   virtual int toolShow();    // Show the tool's GUI
   virtual bool IsShowing(void);
   virtual int TryToResolveProvisional();

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onNewMarks();
	virtual int onChangingFraming();
   virtual int onSwitchSubtool();
   virtual int onRedraw(int frameindex);
	virtual int onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes);
	virtual int onHeartbeat();

   // Tool Execution
	int StartSingleFramePreview(bool newHighlightFlag);
	int StartSingleFrameRender(bool newHighlightFlag);
	int StartMultiFramePreview(bool newHighlightFlag);
	int StartMultiFrameRender(bool newHighlightFlag);

	virtual int StartToolProcessing(EToolProcessingCommand processingType,
											  bool newHighlightFlag);
	virtual int StopSingleFrameProcessing();

	virtual int MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus, int newResumeFrame);
	int SHINE_MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus, int newResumeFrame);


   // PDL Interface
   virtual int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   virtual int onGoToPDLEntry(CPDLEntry &pdlEntry);
	virtual int onPDLExecutionComplete();

   // GOV interaction
   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   virtual void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                      const RECT &rect, const POINT *lasso);
   virtual void SetGOVRepeatShape();

   // Mask interaction
   bool IsMaskInROIMode();
   virtual bool NotifyStartROIMode();
   virtual bool NotifyUserDrewROI();
   virtual void NotifyEndROIMode();

   void SetHighlightFlag(bool newHighlightFlag);
   void SetUsePastFramesOnlyFlag(bool newUsePastFramesOnlyFlag);
   bool GetUsePastFramesOnlyFlag();
   void resolveProvisional(bool clearROI=true);

   CBHook ClipChange;            // Needed to call back on a change
   CBHook MarksChange;           // Needed to call back on a change

   // These implement a protocol for user input to be passed to the GUI
   CBHook GotUserInput;
   int WhatCommandNumberDidYouGet()   { return GotUserInputCommandNumber;  };
   void SetGotUserInputNotifyResult(bool result)
                                      { UserInputCommandConsumed = result; };

   void setAltKeyState(bool flag) { _altKeyState = flag; }
   bool getAltKeyState() { return _altKeyState; }
private:
      bool _altKeyState = false;
      int GotUserInputCommandNumber;
      bool UserInputCommandConsumed;  // true if input was handled in GUI
public:
   /**/

   bool DoesClipHaveAlpha();

   void CheckIfDetectionVisualizationIsAllowed();
   void SetUserDesiredDetectionVisualizationState(bool enabled);
   void UpdateDetectionVisualizationState();
   bool GetDetectionVisualizationState() { return _currentDetectionVisualizationState; }
   void CreateDetectionMapIfNecessary();
   bool UpdateDetectionMap();
//   void ClearDetectionMapAndInhibitUpdates();

   bool IsContinuousPreviewAllowedNow();
   void SetUserDesiredContinuousPreviewState(bool enabled);
   void UpdateContinuousPreviewState();
   bool GetContinuousPreviewState() { return _currentContinuousPreviewState; }
   void BumpContinuousPreviewSerialNumber() { ++_continuousPreviewSerialNumber; }

   void ClearModifiedPixelMap();

   bool _wasPlayerReallyPlayingWhenWeLastLooked = false;
   bool _wasRedOverlayOnWhenPlayerStarted = false;
//   bool _wasInContinuousPreviewStateWhenPlayerStarted = false;
	void preprocessSingleFramePreview();

	int SHINE_RunPreprocessing(
               AnalysisScope scope,
               ShineControlState nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE,
               int frameToJumpToAfterAllPreprocessingIsComplete = INVALID_FRAME_INDEX);

   int SHINE_ResetPreprocessing(AnalysisScope scope);

private:
   int RunFramePreprocessing();
	int RunFrameProcessing(int newResumeFrame);
	int StopFrameProcessing();
	int ProcessSingleFrame(EToolSetupType toolSetupType);
	void UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                           bool processingRenderFlag);

   void UpdateStatusBar();
   void DestroyAllToolSetups(bool notIfPaused);
   void ToggleProvisionalHighlightsAndFixes();
   void acceptFix(bool clearROI=true);
   void rejectFix(bool clearROI=true);

   int currentlyDisplayedFrameIndex();

private:
//
//  Misc variables

   RECT currentUserRect;
   POINT *currentUserLassoPointer;
   BEZIER_POINT *currentUserBezierPointer;

	bool lastActionWasGOV;
   bool inhibitTildeKeyProcessing;
   bool usePastFramesOnlyFlag;
   bool oldMultiUsePastFramesOnlyFlag;
   bool oldSingleUsePastFramesOnlyFlag;

   bool maskIsInROIMode;
	bool maskWasInROIMode;

   static void DetectionMapCreationThreadTrampoline(void *aThreadArg, void *aThreadHandle);
   void RunDetectionMapCreationThread();
   Ipp8uArray CreateDetectionMap(MTI_UINT16 *frameBits, GrainFilterSet &grainFilters, BlobPixelArrayWrapper **blobPixelArrayWrapperPtrPtr);

   // This is static just to avoid crash on program exit.
   // It's a ThreadLock instead of a SpinLock so I can use the TryLock() method.
   static CThreadLock _previewHackLock;

	bool _currentDetectionVisualizationState = false;
   bool _userDesiredDetectVisualizationState = false;
   bool _isDetectionVisualizationAllowedNow = false;
   bool _isItOkToDrawRedStuffOnTheProvisional = false;
   bool _inPreviewHackMode = false;

   void stashFrameBitsForDetectionVisualizer(int frameIndex = -1, MTI_UINT16 *frameBits = nullptr, int frameBitsSizeInBytes = 0);
   int _stashedPreviewHackFrameIndex = -1;
   int _stashedPreviewHackFrameBitsSize = 0;
   MTI_UINT16* _stashedPreviewHackFrameBits = nullptr;
   Ipp8uArray _stashedPreviewHackDetectionMap;
   GrainFilterSet _stashedGrainFilters;
   BlobPixelArrayWrapper *_stashedBlobPixelArrayWrapper = nullptr;
   BlobPixelArrayWrapper *_stashedConsolidatedBlobPixelArrayWrapper = nullptr;

   MotionCacheMap _bestMotionCacheMap;
   bool _needToClearTheBestMotionCacheMap = false;
   CHRTimer _timeSinceWeNoticedThatWeMovedToANewFrame;

   void KickTheDetectionMapCreationThread();
   bool _detectionMapThreadIsRunning = false;
   Event _detectionMapEvent;
//   int _detectionMapGeneration = 0;
//   int _oldDetectionMapGeneration = 0;
   std::atomic_bool _needToRefreshTheDisplayedFrame = false;

	bool _currentContinuousPreviewState = false;
   bool _userDesiredContinuousPreviewState = false;
   long long _continuousPreviewSerialNumber = 0;
   long long _oldContinuousPreviewSerialNumber = 0;
   int _oldContinuousPreviewFrameIndex = -1;
   bool didExecAutoStop = false;

	int _oldModifiedPixelMapFrame = -1;
   Ipp8uArray _modifiedPixelMask;

	int getNumberOfFramesInCurrentShot();

	StatsAF _afStats[AF_MAX_SETTINGS];

   void clearStashedStuff();

   // Keep track of what to do with a frickin' pending Provisional
   enum EProvisionalAction
   {
		PA_ShouldNotBePending,
      PA_AutoAccept,
      PA_AutoReject,
      PA_ImmediatelyAccept
   };
	EProvisionalAction provisionalAction = PA_ShouldNotBePending;

	enum AutoFilterToolSetupType
	{
		INVALID,
		OLDAF_SINGLE,
		OLDAF_MULTI,
		SHINE_PREPROCESS,
//		SHINE_SINGLE,
//		SHINE_MULTI
      SHINE_PROCESS
	};
	AutoFilterToolSetupType _whichAutoFilterTypeToMake = AutoFilterToolSetupType::INVALID;

	int OLDAF_singleFrameToolSetupHandle = -1;
	int OLDAF_multiFrameToolSetupHandle = -1;
	int SHINE_preprocessToolSetupHandle = -1;
	int SHINE_processToolSetupHandle = -1;

	int OLDAF_onHeartbeat();
	int OLDAF_GatherParameters(CAutoFilterToolParameters *toolParams, bool selectedFilterOnly = false);
	int OLDAF_StartSingleFramePreview(bool newHighlightFlag);
	int OLDAF_StartSingleFrameRender(bool newHighlightFlag);
	int OLDAF_StartMultiFramePreview(bool newHighlightFlag);
	int OLDAF_StartMultiFrameRender(bool newHighlightFlag);
	int OLDAF_StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag);
	int OLDAF_StopSingleFrameProcessing();
	int OLDAF_ProcessSingleFrame(EToolSetupType toolSetupType);
   int OLDAF_RunFrameProcessing(int newResumeFrame);

   bool OLDAF_isDetectionVisualizationAllowed();
   bool OLDAF_isContinuousPreviewAllowedNow();

	int SHINE_onHeartbeat();
	int SHINE_GatherParameters(
                  ShineToolParameters *toolParams,
                  int firstRepairFrameIndex = -1,
                  bool singleFrame = false);
	void SHINE_StartPreprocessing();
	int SHINE_StartSingleFramePreview(bool newHighlightFlag);
	int SHINE_StartSingleFrameRender(bool newHighlightFlag);
	int SHINE_StartMultiFramePreview(bool newHighlightFlag);
	int SHINE_StartMultiFrameRender(bool newHighlightFlag);
	int SHINE_StartMultiFramePreviewAfterPreprocessing();
	int SHINE_StartMultiFrameRenderAfterPreprocessing();
	int SHINE_StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag);
	int SHINE_StopSingleFrameProcessing();
	int SHINE_ProcessSingleFrame(EToolSetupType toolSetupType);
   int SHINE_RunFramePreprocessing();
   int SHINE_RunFrameProcessing(int newResumeFrame);
   int SHINE_RunPreprocessingOnNextUnpreprocessedShot();

   bool SHINE_isDetectionVisualizationAllowed();
   bool SHINE_isContinuousPreviewAllowedNow();

	ShineControlState SHINE_controlState = SHINE_STATE_IDLE;
	ShineControlState SHINE_nextStateAfterAllPreprocessingIsComplete = SHINE_STATE_IDLE;
	ShineControlState SHINE_getControlState();
	void SHINE_setControlState(ShineControlState newState);

   int SHINE_clipChangeCounter = 0;
   int SHINE_preprocShotIndex = -1;
   void SHINE_updateGuiBasedOnPreprocessingState();

   bool SHINE_preprocessingWasAborted = false;
	bool SHINE_loadPreprocOutputForCurrentShot();
   bool SHINE_loadPreprocOutputForShot(int frameIndex);
   std::unique_ptr<ShinePreprocOutputCache> SHINE_preprocOutputCache;
//   int SHINE_frameToJumpToAfterPreprocessingOneShot = INVALID_FRAME_INDEX;
   int SHINE_frameToJumpToAfterAllPreprocessingIsComplete = INVALID_FRAME_INDEX;
   int SHINE_savedHighlightFlag = true;
   ShinePreprocOutputList SHINE_preprocOutputList;
   vector<int> SHINE_cutList;
   vector<int> SHINE_procShotList;
   int SHINE_prereqIndex = -1;
   int SHINE_constructPrereqListFromMarks();
   int SHINE_constructPrereqList(
               ShinePreprocOutputList &prereqList,
               int inFrameIndex = INVALID_FRAME_INDEX,
               int outFrameIndex = INVALID_FRAME_INDEX);
   bool SHINE_areAllPreprocDataAvailable(
               int inFrameIndex = INVALID_FRAME_INDEX,
               int outFrameIndex = INVALID_FRAME_INDEX);
   bool SHINE_areAllPreprocDataAvailable(
               const ShinePreprocOutputList &prereqList,
               int inFrameIndex = INVALID_FRAME_INDEX,
               int outFrameIndex = INVALID_FRAME_INDEX);
   bool SHINE_areAnyPreprocDataAvailable(
               const ShinePreprocOutputList &prereqList,
               int inFrameIndex = INVALID_FRAME_INDEX,
               int outFrameIndex = INVALID_FRAME_INDEX);

   tuple<int, int> getCurrentShotBoundaries();
   tuple<int, int> getShotBoundaries(const vector<int> &cutList, int frameIndex);
   tuple<int, int> getClipBoundaries();
   bool areShotBoundariesSet();
   int getCurrentShotIndex(ShinePreprocOutputList &prereqList);
   int getShotIndexFromFrameIndex(ShinePreprocOutputList &prereqList, int frameIndex);
   int _processSingleFrameIndex = INVALID_FRAME_INDEX;
   int SHINE_resetPreprocOutput(ShinePreprocessingOutputItem item);
};

extern CAutoFilter *GAutoFilter;       // Lives in AutoFilterPlugin.cpp

//////////////////////////////////////////////////////////////////////

#endif // !defined(AUTOFILTER_TOOL_H)
