//---------------------------------------------------------------------------

#ifndef ShinePreprocessorOutputH
#define ShinePreprocessorOutputH
//---------------------------------------------------------------------------

struct ShinePreprocessorOutput;
typedef shared_ptr<ShinePreprocessorOutput>  ShinePreprocessorOutputSharedPtr;


struct ShinePreprocessorOutput
{
	void *data = nullptr;

	ShinePreprocessorOutput(void *opaqueData);
	~ShinePreprocessorOutput();

   static int ReadFromFile(const string &path, ShinePreprocessorOutputSharedPtr &shinePreprocessorOutput);
   static int WriteToFile(const string &path, const ShinePreprocessorOutputSharedPtr &shinePreprocessorOutput);
};


#endif
