//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PickAFilterFormUnit.h"
#include "MTIstringstream.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TPickAFilterForm *PickAFilterForm;
//---------------------------------------------------------------------------

__fastcall TPickAFilterForm::TPickAFilterForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TPickAFilterForm::SetOpName(const string &newOpName)
{
   MTIostringstream os1, os2;
   string capOpName = newOpName;

   if (capOpName.size() >= 1)
      capOpName[0] += ('A' - 'a');

   os1 << capOpName << " Filters";
   Caption = os1.str().c_str();

   os2 << "Please select one or more filters to " << newOpName;
   PleaseSelectLabel->Caption = os2.str().c_str();
}
//---------------------------------------------------------------------------

void TPickAFilterForm::SetFilterNameList(
                       vector< pair<string,string> > &filterNames)
{

   FilterIDList.clear();
   ListBox->Items->Clear();
   // pair is <name, id> !!
   vector< pair<string,string> >::iterator iter;

   for (iter = filterNames.begin(); iter != filterNames.end(); ++iter)
   {
      ListBox->Items->Append(iter->first.c_str());
      FilterIDList.push_back(iter->second);
   }
   ListBox->ItemIndex = -1;
}
//---------------------------------------------------------------------------

int TPickAFilterForm::GetSelectedFilterIDs(vector<string> &filterIDs)
{
   filterIDs.clear();
   int remaining = ListBox->SelCount;
   int itemIndex = 0;

   while (remaining > 0)
   {
      if (ListBox->Selected[itemIndex])
      {
          filterIDs.push_back(FilterIDList[itemIndex]);
          --remaining;
      }
      ++itemIndex;
   }

   return ListBox->SelCount;
}
//---------------------------------------------------------------------------

