//---------------------------------------------------------------------------

#pragma hdrstop

#include "AutoFilterSingleProc.h"
#include "AutoFilterToolParameters.h"
#include "err_auto_filter.h"

#include "Clip3.h"
#include "ImageFormat3.h"
#include "ToolProgressMonitor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

///////////////////////////////////////////////////////////////////////////////
// AUTOFILTER SINGLE-FRAME TOOL PROCESSOR
///////////////////////////////////////////////////////////////////////////////

CAutoFilterSingleProc::CAutoFilterSingleProc(
   int newToolNumber,
   const string &newToolName,
   CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr,
   IToolProgressMonitor *newToolProgressMonitor,
   bool doPastOnlyHack,
   StatsAF *afStats,
   GrainFilterSet &grainFilters,
   MTI_UINT8 *modifiedPixelMap)
: CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
, doPastOnlyHackFlag(doPastOnlyHack)
, _afStats(afStats)
, _cachedGrainFilters(&grainFilters)
, _modifiedPixelMap(modifiedPixelMap)
{
   AlgorithmAF = new CAlgorithmAF();
	StartProcessThread(); // Required by Tool Infrastructure
}

CAutoFilterSingleProc::~CAutoFilterSingleProc()
{
	delete _preallocedMemory;
	delete AlgorithmAF;
	AlgorithmAF = NULL;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	int retVal = 0;
	string errorFunc("<UNKNOWN>");

	try
   {
		// Tell Tool Infrastructure, via CToolProcessor supertype, about the
      // maximum number of input and output frames
		int maxInputFrames = 3; // Processed frame + 1 on either side
      int maxOutputFrames = 1;
		SetInputMaxFrames(0, maxInputFrames, maxOutputFrames); // Input Port 0
      SetOutputNewFrame(0, 0); // Output to new frame with same image format
		// as the input
      SetOutputMaxFrames(0, maxOutputFrames); // Output Port 0

		// Tell algorithm about image dimensions, active area, etc.
      const CImageFormat *imageFormat = GetSrcImageFormat(0);
      RECT activePictureRect = imageFormat->getActivePictureRect();
		MTI_UINT16 maxDataValue[3];
      imageFormat->getComponentValueMax(maxDataValue);

		long lNRow = imageFormat->getTotalFrameHeight();
      long lNCol = imageFormat->getTotalFrameWidth();
      long lNRowCol = lNCol * lNRow;
		long lNCom = imageFormat->getComponentCountExcAlpha();
      long lActiveStartRow = activePictureRect.top;
      long lActiveStopRow = activePictureRect.bottom + 1;
		long lActiveStartCol = activePictureRect.left;
      long lActiveStopCol = activePictureRect.right + 1;

		MTIassert(lNRowCol <= ROW_COL_10K);

      // size_t preallocSize = (lNRowCol <= ROW_COL_2K)
		// ? sizeof(AF_PreallocedMemory_2K)
      // : ((lNRowCol <= ROW_COL_4K)
      // ? sizeof(AF_PreallocedMemory_4K)
		// : ((lNRowCol <= ROW_COL_6K)
      // ? sizeof(AF_PreallocedMemory_6K)
      // : ((lNRowCol <= ROW_COL_8K)
		// ? sizeof(AF_PreallocedMemory_8K)
      // : sizeof(AF_PreallocedMemory_10K))));

		_preallocedMemory = (lNRowCol <= ROW_COL_2K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_2K) :
            ((lNRowCol <= ROW_COL_4K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_4K) :
            ((lNRowCol <= ROW_COL_6K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_6K) :
				((lNRowCol <= ROW_COL_8K) ? (AF_PreallocedMemory*)(new AF_PreallocedMemory_8K) :
            (AF_PreallocedMemory*)(new AF_PreallocedMemory_10K))));

		AlgorithmAF->Initialize(true, _preallocedMemory);
      retVal = AlgorithmAF->SetDimension(lNRow, lNCol, lNCom, lActiveStartRow, lActiveStopRow, lActiveStartCol, lActiveStopCol);
      if (retVal != 0)
		{
         throw(errorFunc = "SetDimension in SetIOConfig");
      }

      // Component bounds
      MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
		MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];
      imageFormat->getComponentValueMin(cvMin);
      imageFormat->getComponentValueMax(cvMax);
		retVal = AlgorithmAF->SetImageBounds(cvMin, cvMax);
      if (retVal != 0)
      {
			throw(errorFunc = "SetImageBounds in SetIOConfig");
      }

		AlgorithmAF->SetAlphaMatteType(imageFormat->getAlphaMatteType(), imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR);
      AlgorithmAF->SetEmergencyStopFlagPtr(GetEmergencyStopFlagPtr());

		return 0; // SUCCESS1
   }
   catch (string)
	{; // do nothing here
   }
   catch (std::bad_alloc)
	{
      retVal = ERR_PLUGIN_AF_MALLOC;
   }
	catch (...)
   {
      retVal = 666; // meaningless
	}

   try
	{
      CAutoErrorReporter autoErr("CAutoFilterSingleProc::SetIOConfig", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		string errorMsg;
      switch (retVal)
      {
		case ERR_PLUGIN_AF_NO_FILTERS:
         errorMsg = "No valid filters found in active filter set";
         break;
		case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
         errorMsg = "No alpha channel - cannot find defect map";
         break;
		case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
         break;
		default:
         errorMsg = "Internal Error";
         break;
		}

      autoErr.errorCode = retVal;
		autoErr.msg << errorMsg << endl << " in function " << errorFunc;
   }
   catch (...)
	{
      TRACE_0(errout << "CAutoFilterSingleProc::SetIOConfig: FATAL ERROR, code " << retVal << endl <<
            "CAUGHT EXCEPTION when trying to display error dialog!");
	}

	return retVal; // ERROR!
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   // Gaaa, here inframe index means "first frame to process" on the LHS and
	// "first input frame" on the RHS
   inFrameIndex = frameRange.inFrameIndex + 2; // we lied about the range
	outFrameIndex = inFrameIndex + 1; // exclusive (not used)

	// Processing one frame in one iteration
   framesRemaining = 1;
	framesAlreadyProcessed = 0;

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::SetParameters(CToolParameters *toolParams)
{
	int retVal = 0;
   string errorFunc("<UNKNOWN>");

   try
	{
		CAutoFilterToolParameters *afpToolParams = static_cast<CAutoFilterToolParameters*>(toolParams);

		retVal = AlgorithmAF->AssignParameter(&afpToolParams->getParameterRef());
      if (retVal != 0)
		{
         throw(errorFunc = "AssignParameter");
		}

		return 0;
   }
	catch (string)
   {; // do nothing here
	}
   catch (std::bad_alloc)
	{
      retVal = ERR_PLUGIN_AF_MALLOC;
	}
   catch (...)
	{
      retVal = 666; // meaningless
	}
   try
	{
      CAutoErrorReporter autoErr("CAutoFilterSingleProc::SetParameters", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      string errorMsg;
		switch (retVal)
      {
		case ERR_PLUGIN_AF_NO_FILTERS:
         errorMsg = "No valid filters found in active filter set";
			break;
      case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
			errorMsg = "No alpha channel - cannot find defect map";
         break;
		case ERR_PLUGIN_AF_MALLOC:
         errorMsg = "Out of memory";
			break;
      default:
			errorMsg = "Internal Error";
         break;
		}

		autoErr.errorCode = retVal;
      autoErr.msg << errorMsg << endl << " in function " << errorFunc;
	}
   catch (...)
	{
      TRACE_0(errout << "CAutoFilterSingleProc::SetParameters: FATAL ERROR, code " << retVal << endl <<
				"CAUGHT EXCEPTION when trying to display error dialog!");
   }
	return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::BeginProcessing(SToolProcessingData &procData)
{
	int retVal = 0;
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
	RECT activePictureRect = imageFormat->getActivePictureRect();

	retVal = GetSystemAPI()->GetMaskRoi(inFrameIndex, maskROI);
   if (retVal != 0)
	{
      return retVal;
	}

	AlgorithmAF->setRegionOfInterestPtr(maskROI.getBlendPtr(), maskROI.getLabelPtr());

	GetToolProgressMonitor()->SetFrameCount(1);
   GetToolProgressMonitor()->SetStatusMessage("Processing");
	GetToolProgressMonitor()->StartProgress();

	// Success, continue
   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::BeginIteration(SToolProcessingData &procData)
{
   int inputFrameIndexList[3];
	int outputFrameIndexList[1];
   int indexOfFrameToProcess = inFrameIndex;
	CVideoFrameList *frameList = GetSystemAPI()->getVideoFrameList();

	framesToProcess = 1;

	// Tell the Tool Infrastructure which frames we want as input
   // during this iteration
	if (doPastOnlyHackFlag)
   {
		// In "past-only" mode we always look backward two frames.
      // HACK ALERT: well... except at the frame that is at index 'mark in + 1'
		// where we want to only look back ONE frame...
      // NOTE: if we are at the mark in frame we should do NOTHING!

		if (indexOfFrameToProcess == (GetSystemAPI()->getMarkIn() + 1) || indexOfFrameToProcess == (frameList->getInFrameIndex() + 1))
		{
         // HACK frame at markIn+1 looks 1 backward  - TWICE!
			laFrameIndexInInputSet[0] = -1;
         laFrameIndexInInputSet[1] = -1;
			laFrameIndexInInputSet[2] = 0;
      }
		else
      {
			// Rest of the frames look 2 backward
         laFrameIndexInInputSet[0] = -2;
			laFrameIndexInInputSet[1] = -1;
         laFrameIndexInInputSet[2] = 0;
		}
   }
	else if (indexOfFrameToProcess == GetSystemAPI()->getMarkIn() || indexOfFrameToProcess == frameList->getInFrameIndex())
   {
		// first frame looks 2 forward
      laFrameIndexInInputSet[0] = 0;
		laFrameIndexInInputSet[1] = 1;
      laFrameIndexInInputSet[2] = 2;
	}
   else if (indexOfFrameToProcess == (GetSystemAPI()->getMarkOut() - 1) || indexOfFrameToProcess == (frameList->getOutFrameIndex() - 1))
	{
      // last frame looks 2 backward
		laFrameIndexInInputSet[0] = -2;
      laFrameIndexInInputSet[1] = -1;
		laFrameIndexInInputSet[2] = 0;
   }
	else
   {
		// most frames look 1 backward and 1 forward
      laFrameIndexInInputSet[0] = -1;
		laFrameIndexInInputSet[1] = 0;
      laFrameIndexInInputSet[2] = 1;
	}
   inputFrameIndexList[0] = indexOfFrameToProcess + laFrameIndexInInputSet[0];
	inputFrameIndexList[1] = indexOfFrameToProcess + laFrameIndexInInputSet[1];
   inputFrameIndexList[2] = indexOfFrameToProcess + laFrameIndexInInputSet[2];
	SetInputFrames(0, 3, inputFrameIndexList);

	// When finished processing, release all the frames
   SetFramesToRelease(0, 3, inputFrameIndexList);

   // Set output frame ( same as processed frame )
	outputFrameIndexList[0] = indexOfFrameToProcess;
   SetOutputFrames(0, 1, outputFrameIndexList);

   return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
	// how many processing iterations to do before stopping

	return 1;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::DoProcess(SToolProcessingData &procData)
{
	int retVal = 0;
   string errorFunc("<UNKNOWN>");

   // HACK short circuit
	if (doPastOnlyHackFlag && ((inFrameIndex + laFrameIndexInInputSet[0]) < 0) && ((inFrameIndex + laFrameIndexInInputSet[1]) < 0))
   {
		// No past frames to fix from!! Do nothing!
      return 0;
	}

	try
   {

      MTI_UINT16 *uspaSrcArg[3];
		MTI_UINT16 *uspDstArg;
      CToolFrameBuffer *inToolFrameBuffer, *outToolFrameBuffer;

		// Get pointers to the input frame buffers
		for (int i = 0; i < 3; ++i)
		{
			inToolFrameBuffer = GetRequestedInputFrame(0, i);
			uspaSrcArg[i] = inToolFrameBuffer->GetVisibleFrameBufferPtr();
		}

		// Tell the algorithm about the input frame buffer pointers
		// and relative indices
		retVal = AlgorithmAF->SetSrcImgPtr(uspaSrcArg, laFrameIndexInInputSet);
		if (retVal != 0)
		{
			throw(errorFunc = "SetSrcImgPtr");
		}

		// Get a pointer to the output frame buffer provided by the
		// Tool Infrastructure
		outToolFrameBuffer = GetRequestedOutputFrame(0, 0);

		//
		// The -laFrameIndexInInputSet[0] will give us the index within the
		// input set of the frame to be processed, or in doPastOnlyHack mode
		// the index is always 2
		//
		int inputSetIndexOfFrameToFix = -laFrameIndexInInputSet[0];
		if (doPastOnlyHackFlag)
		{
			inputSetIndexOfFrameToFix = 2;
		}
		inToolFrameBuffer = GetRequestedInputFrame(0, inputSetIndexOfFrameToFix);

		// If we are doing the "past-only" hack, we will modify the input
		// frame in place, then later copy it to the output frame
		// GAAAH I can't get 'modifyInPlace' to work, so as a HACK let
		// the tool modify the output frame, then copy that back to the
		// input frame!!!
		// if (doPastOnlyHackFlag)
		// {
		//// Give the algorithm the pointer to the output image
		// uspDstArg = inToolFrameBuffer->GetVisibleFrameBufferPtr();
		// }
		// else
		{
			// Copy the original image, including invisible fields, to the
			// output frame.  Note: this may have to wait to allocate memory
			CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBuffer, outToolFrameBuffer);

			// Give the algorithm the pointer to the output image
			uspDstArg = outToolFrameBuffer->GetVisibleFrameBufferPtr();
		}

		// Give the algorithm the pointer to the output image
		AlgorithmAF->SetDstImgPtr(uspDstArg);

		// Now setup the pixel region list that will receive the original
		// values of pixels changed by the algorithm
		//
		AlgorithmAF->SetPixelRegionList(outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr());

		// Now setup the "modified pixel map" that will record which pixels were
		// changed by the algorithm
		//
		AlgorithmAF->SetModifiedPixelMap(_modifiedPixelMap);

		// // Clear the motion cache if necessary (e.g. motion sliders changed)
		// if (_needToClearTheBestMotionCacheMap != nullptr && *_needToClearTheBestMotionCacheMap)
		// {
		// *_needToClearTheBestMotionCacheMap = false;
		// if (_bestMotionCacheMap != nullptr)
		// {
		// _bestMotionCacheMap->clear();
		// }
		// }

		// Finally, render the output frame
		retVal = AlgorithmAF->Process(*_cachedGrainFilters);
		if (retVal != 0)
		{
			throw(errorFunc = "AutofilterProcess");
		}

		// HACK - if we're in "past-only" mode, we need to copy the modified bits
		// from the output buffer back to the input buffer.
		// No need to loop here, we know that past-only mode is single threded
		// because it needs to use the output of the previous iteration as input
		// to the current iteration!

		if (doPastOnlyHackFlag)
		{
			// Overwrite the input frame bits with the modified output bits
			///////////////////////////////////////////////////////////////
			// CAREFUL!!! WE ARE ASSUMING THAT inToolFrameBuffer AND
			// outToolFrameBuffer ARE STILL SET CORRECTLY FROM ABOVE!!!!!!
			///////////////////////////////////////////////////////////////
			inToolFrameBuffer->CopyAllImageBitsOnly(*outToolFrameBuffer);
		}

		AlgorithmAF->GetStats(_afStats);

		return 0; // SUCCESS
	}
	catch (string)
	{; // do nothing here
	}
	catch (std::bad_alloc)
	{
		retVal = ERR_PLUGIN_AF_MALLOC;
	}
	catch (...)
	{
		retVal = 666; // meaningless
	}
	try
	{
		CAutoErrorReporter autoErr("CAutoFilterSingleProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		string errorMsg;
		switch (retVal)
		{
		case ERR_PLUGIN_AF_NO_FILTERS:
			errorMsg = "No valid filters found in active filter set";
			break;
		case ERR_PLUGIN_AF_NO_ALPHA_CHANNEL:
			errorMsg = "No alpha channel - cannot find defect map";
			break;
		case ERR_PLUGIN_AF_MALLOC:
			errorMsg = "Out of memory";
			break;
		default:
			errorMsg = "Internal Error";
			break;
		}

		autoErr.errorCode = retVal;
		autoErr.msg << errorMsg << endl << " in function " << errorFunc;
	}
	catch (...)
	{
		TRACE_0(errout << "CAutoFilterSingleProc::DoProcess: FATAL ERROR, code " << retVal << endl <<
				"CAUGHT EXCEPTION when trying to display error dialog!");
	}

	return retVal; // ERROR
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::EndIteration(SToolProcessingData &procData)
{
	framesRemaining -= 1;
	framesAlreadyProcessed += 1;

	GetToolProgressMonitor()->BumpProgress(1);

	return 0;
}
// ---------------------------------------------------------------------------

int CAutoFilterSingleProc::EndProcessing(SToolProcessingData &procData)
{
	// DeleteMask();
	if (framesRemaining == 0)
	{
		GetToolProgressMonitor()->SetStatusMessage("Processing complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	return 0;
}
// ---------------------------------------------------------------------------
