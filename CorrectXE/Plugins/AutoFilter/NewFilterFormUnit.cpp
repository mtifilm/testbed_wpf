//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "NewFilterFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNewFilterForm *NewFilterForm;
//---------------------------------------------------------------------------
__fastcall TNewFilterForm::TNewFilterForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TNewFilterForm::SetDefaultName(const string &newName)
{
   Edit->Text = AnsiString(newName.c_str()).Trim();
}
//---------------------------------------------------------------------------

string TNewFilterForm::GetNewFilterName()
{
   return StringToStdString(Edit->Text.Trim());
}
//---------------------------------------------------------------------------


void __fastcall TNewFilterForm::EditEnter(TObject *Sender)
{
   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
}
//---------------------------------------------------------------------------

