//---------------------------------------------------------------------------

#ifndef ShineToolParametersH
#define ShineToolParametersH
//---------------------------------------------------------------------------

#include "AutoFilterToolParameters.h"
#include "ShinePreprocOutput.h"

#include "ImageFormat3.h"
#include "ToolObject.h"
#include "Ippheaders.h"
//////////////////////////////////////////////////////////////////////////////

const int SHINE_DEFAULT_MAX_SIZE_SETTING          = 50;
const int SHINE_DEFAULT_MIN_SIZE_SETTING          =  1;
const int SHINE_DEFAULT_TEMPORAL_CONTRAST_SETTING = 50;
const int SHINE_DEFAULT_SPATIAL_CONTRAST_SETTING  = 50;
const int SHINE_DEFAULT_GRAIN_SUPPRESSION_SETTING = 50;
const int SHINE_DEFAULT_MOTION_TOLERANCE_SETTING  = 50;

struct ShineToolSettings
{
   int firstRepairFrameIndex = -1;
   int lastRepairFrameIndex = -1;
   ShinePreprocOutputList *shinePreprocOutputList;
   int singleShotListIndex = -1;

   // GUI settings
   // THESE VALUES DEFINE THE DEFAULT SETTINGS!
   bool detectDark           = false;
   bool detectBright         = false;
   bool detectAlpha          = false;
   int darkMaxSize           = SHINE_DEFAULT_MAX_SIZE_SETTING;
   int darkMinSize           = SHINE_DEFAULT_MIN_SIZE_SETTING;
   int darkSpatialContrast   = SHINE_DEFAULT_SPATIAL_CONTRAST_SETTING;
   int darkTemporalContrast  = SHINE_DEFAULT_TEMPORAL_CONTRAST_SETTING;
   int darkMotionTolerance   = SHINE_DEFAULT_MOTION_TOLERANCE_SETTING;
   int brightMaxSize         = SHINE_DEFAULT_MAX_SIZE_SETTING;
   int brightMinSize         = SHINE_DEFAULT_MIN_SIZE_SETTING;
   int brightSpatialContrast = SHINE_DEFAULT_SPATIAL_CONTRAST_SETTING;
   int brightTemporalContrast= SHINE_DEFAULT_TEMPORAL_CONTRAST_SETTING;
   int brightMotionTolerance = SHINE_DEFAULT_MOTION_TOLERANCE_SETTING;
   int alphaMaxSize          = SHINE_DEFAULT_MAX_SIZE_SETTING;
   int alphaGrainSuppression = SHINE_DEFAULT_GRAIN_SUPPRESSION_SETTING;
   int alphaMotionTolerance  = SHINE_DEFAULT_MOTION_TOLERANCE_SETTING;

   void dump()
   {
#ifdef _DEBUG
      if (detectDark)
      {
         TRACE_0(errout << "Detect DARK: max=" << darkMaxSize
            << ", min=" << darkMinSize
            << ", temporal=" << darkTemporalContrast
            << ", spatial=" << darkSpatialContrast
            << ", motion=" << darkMotionTolerance);
      }
      else
      {
         TRACE_0(errout << "Detect DARK is OFF.");
      }

      if (detectBright)
      {
         TRACE_0(errout << "Detect BRIGHT: max=" << brightMaxSize
            << ", min=" << brightMinSize
            << ", temporal=" << brightTemporalContrast
            << ", spatial=" << brightSpatialContrast
            << ", motion=" << brightMotionTolerance);
      }
      else
      {
         TRACE_0(errout << "Detect BRIGHT is OFF.");
      }

      if (detectAlpha)
      {
         TRACE_0(errout << "Detect ALPHA: max=" << alphaMaxSize
            << ", grain=" << alphaGrainSuppression
            << ", motion=" << alphaMotionTolerance);
      }
      else
      {
         TRACE_0(errout << "Detect ALPHA is OFF.");
      }
#endif
   }
};

class ShineToolParameters : public CToolParameters
{
public:
	ShineToolParameters() : CToolParameters(1, 1)
	{
	}

	virtual ~ShineToolParameters() {}

	ShineToolSettings& getShineToolSettingsRef() { return _shineToolSettings; }

private:
	ShineToolSettings _shineToolSettings;
};

#endif
