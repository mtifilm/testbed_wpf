object BasicFilterFrame: TBasicFilterFrame
  Left = 0
  Top = 0
  Width = 471
  Height = 24
  Color = 6974058
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object BackPanel: TColorPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 24
    Color = 6974058
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      472
      24)
    object AF_ValuesPanel: TPanel
      Left = 0
      Top = 0
      Width = 472
      Height = 24
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Color = 6974058
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = FilterClick
      DesignSize = (
        472
        24)
      object AF_MaxMinDustValues: TLabel
        Left = 169
        Top = 6
        Width = 65
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'MaxSize'
        Transparent = True
        OnClick = FilterClick
      end
      object AF_ContrastOrGrainValue: TLabel
        Left = 238
        Top = 6
        Width = 57
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Contrast'
        Transparent = True
        OnClick = FilterClick
      end
      object AF_FixToleranceValue: TLabel
        Left = 291
        Top = 6
        Width = 59
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'Confidence'
        Transparent = True
        OnClick = FilterClick
      end
      object AF_EdgeBlurValue: TLabel
        Left = 415
        Top = 6
        Width = 57
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'EdgeBlur'
        Transparent = True
        OnClick = FilterClick
        ExplicitLeft = 424
      end
      object AF_MotionValue: TLabel
        Left = 351
        Top = 6
        Width = 65
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'H'
        Transparent = True
        OnClick = FilterClick
        ExplicitLeft = 359
      end
    end
    object FilterClickedNotifyWidget: TCheckBox
      Left = -100
      Top = 4
      Width = 97
      Height = 17
      Caption = 'FilterClickedNotifyWidget'
      TabOrder = 1
    end
    object SHINE_ValuesPanel: TPanel
      Left = 0
      Top = 0
      Width = 472
      Height = 24
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Color = 6974058
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      OnClick = FilterClick
      DesignSize = (
        472
        24)
      object SHINE_MaxMinValues: TLabel
        Left = 232
        Top = 6
        Width = 80
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'MaxSize'
        Transparent = True
        OnClick = FilterClick
      end
      object SHINE_ContrastValue: TLabel
        Left = 312
        Top = 6
        Width = 80
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Contrast'
        Transparent = True
        OnClick = FilterClick
      end
      object SHINE_AggressionValue: TLabel
        Left = 392
        Top = 6
        Width = 80
        Height = 13
        Alignment = taCenter
        Anchors = []
        AutoSize = False
        Caption = 'Confidence'
        Transparent = True
        OnClick = FilterClick
      end
    end
    object UseAndFilterNamePanel: TPanel
      Left = 0
      Top = 0
      Width = 232
      Height = 24
      BevelOuter = bvNone
      TabOrder = 3
      OnClick = FilterClick
      object FilterNameLabel: TLabel
        Left = 32
        Top = 6
        Width = 145
        Height = 13
        AutoSize = False
        Caption = 'FilterName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnClick = FilterClick
      end
      object UseCheckBox: TCheckBox
        Left = 8
        Top = 4
        Width = 16
        Height = 17
        Hint = 'Enable this filter ('
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnClick = UseCheckBoxClick
      end
    end
  end
  object DisabledPanel: TPanel
    Left = 0
    Top = 1
    Width = 472
    Height = 24
    BevelOuter = bvNone
    Color = 6974058
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object NoFilterLoadedLabel: TLabel
      Left = 32
      Top = 4
      Width = 122
      Height = 13
      AutoSize = False
      Caption = 'No filter loaded'
      Color = 6974058
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsItalic]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object DisabledCheckboxPanel: TPanel
      Left = 8
      Top = 4
      Width = 13
      Height = 13
      BevelOuter = bvLowered
      Color = 6974058
      Ctl3D = False
      ParentBackground = False
      ParentCtl3D = False
      TabOrder = 0
    end
  end
end
