#ifndef FilterManagerH
#define FilterManagerH
// ---------------------------------------------------------------------------

#include "guid_mti.h"
#include "IniFile.h"

#include <string>
#include <map>
#include <vector>
using std::string;
using std::map;
using std::vector;
// ---------------------------------------------------------------------------

#define INVALID_SLOT_NUMBER -1
#define NUMBER_OF_FILTERS_PER_SET 6

// These assume prevailing convention
#define RED_FILTER_CHANNEL   0
#define GREEN_FILTER_CHANNEL 1
#define BLUE_FILTER_CHANNEL  2
#define ALPHA_FILTER_CHANNEL 3

/* some GUIDs for predefined filters and sets */
#define INVALID_ID                "_____________INVALID_ID_____________"

#define AF_DEFAULT_FILTER_SET_ID       "2cb60f89-fced-11dc-95ff-0800200c9a66"
#define AF_DEFAULT_FILTER_SET_NAME     "__Working__Set__"
#define AF_NORMAL_FILTER_SET_ID        "2cb60f90-fced-11dc-95ff-0800200c9a66"
#define AF_NORMAL_FILTER_SET_NAME      "Normal"
#define AF_THREE_LAYER_FILTER_SET_ID   "2cb60f91-fced-11dc-95ff-0800200c9a66"
#define AF_THREE_LAYER_FILTER_SET_NAME "3 Layer Separation"
#define AF_ALPHA_FILTER_SET_ID         "2cb60f92-fced-11dc-95ff-0800200c9a66"
#define AF_ALPHA_FILTER_SET_NAME       "Alpha channel"

#define SHINE_DEFAULT_FILTER_SET_ID       "3cb60f89-fced-11dc-95ff-0800200c9a66"
#define SHINE_DEFAULT_FILTER_SET_NAME     "__Working__Set__"
#define SHINE_NORMAL_FILTER_SET_ID        "3cb60f90-fced-11dc-95ff-0800200c9a66"
#define SHINE_NORMAL_FILTER_SET_NAME      "Normal"
#define SHINE_THREE_LAYER_FILTER_SET_ID   "3cb60f91-fced-11dc-95ff-0800200c9a66"
#define SHINE_THREE_LAYER_FILTER_SET_NAME "3 Layer Separation"

#define PDL_FILTER_SET_ID           "4cb60f93-fced-11dc-95ff-0800200c9a66"
#define PDL_FILTER_SET_NAME         "[[PDL]]"

#define AF_DEFAULT_FILTER_ID         "2cb636a0-fced-11dc-95ff-0800200c9a66"
#define AF_DEFAULT_FILTER_NAME       "Default"
#define AF_WHITE_FILTER_ID           "2cb636a1-fced-11dc-95ff-0800200c9a66"
#define AF_WHITE_FILTER_NAME         "White"
#define AF_BLACK_FILTER_ID           "2cb636a2-fced-11dc-95ff-0800200c9a66"
#define AF_BLACK_FILTER_NAME         "Black"
#define AF_RED_LIGHT_FILTER_ID       "2cb636a3-fced-11dc-95ff-0800200c9a66"
#define AF_RED_LIGHT_FILTER_NAME     "Red/Light"
#define AF_GREEN_LIGHT_FILTER_ID     "2cb636a4-fced-11dc-95ff-0800200c9a66"
#define AF_GREEN_LIGHT_FILTER_NAME   "Green/Light"
#define AF_BLUE_LIGHT_FILTER_ID      "2cb636a5-fced-11dc-95ff-0800200c9a66"
#define AF_BLUE_LIGHT_FILTER_NAME    "Blue/Light"
#define AF_ALPHA_FILTER_ID           "2cb636a6-fced-11dc-95ff-0800200c9a66"
#define AF_ALPHA_FILTER_NAME         "Alpha"
#define AF_RED_DARK_FILTER_ID        "2cb636a7-fced-11dc-95ff-0800200c9a66"
#define AF_RED_DARK_FILTER_NAME      "Red/Dark"
#define AF_GREEN_DARK_FILTER_ID      "2cb636a8-fced-11dc-95ff-0800200c9a66"
#define AF_GREEN_DARK_FILTER_NAME    "Green/Dark"
#define AF_BLUE_DARK_FILTER_ID       "2cb636a9-fced-11dc-95ff-0800200c9a66"
#define AF_BLUE_DARK_FILTER_NAME     "Blue/Dark"

#define SHINE_DEFAULT_FILTER_ID         "3cb636a0-fced-11dc-95ff-0800200c9a66"
#define SHINE_DEFAULT_FILTER_NAME       "Default"
#define SHINE_WHITE_FILTER_ID           "3cb636a1-fced-11dc-95ff-0800200c9a66"
#define SHINE_WHITE_FILTER_NAME         "White"
#define SHINE_BLACK_FILTER_ID           "3cb636a2-fced-11dc-95ff-0800200c9a66"
#define SHINE_BLACK_FILTER_NAME         "Black"
#define SHINE_RED_LIGHT_FILTER_ID       "3cb636a3-fced-11dc-95ff-0800200c9a66"
#define SHINE_RED_LIGHT_FILTER_NAME     "Red/Light"
#define SHINE_GREEN_LIGHT_FILTER_ID     "3cb636a4-fced-11dc-95ff-0800200c9a66"
#define SHINE_GREEN_LIGHT_FILTER_NAME   "Green/Light"
#define SHINE_BLUE_LIGHT_FILTER_ID      "3cb636a5-fced-11dc-95ff-0800200c9a66"
#define SHINE_BLUE_LIGHT_FILTER_NAME    "Blue/Light"
#define SHINE_RED_DARK_FILTER_ID        "3cb636a7-fced-11dc-95ff-0800200c9a66"
#define SHINE_RED_DARK_FILTER_NAME      "Red/Dark"
#define SHINE_GREEN_DARK_FILTER_ID      "3cb636a8-fced-11dc-95ff-0800200c9a66"
#define SHINE_GREEN_DARK_FILTER_NAME    "Green/Dark"
#define SHINE_BLUE_DARK_FILTER_ID       "3cb636a9-fced-11dc-95ff-0800200c9a66"
#define SHINE_BLUE_DARK_FILTER_NAME     "Blue/Dark"

#define PDL_0_FILTER_ID           "4cb636b0-fced-11dc-95ff-0800200c9a66"
#define PDL_0_FILTER_NAME         "[[PDL]]0"
#define PDL_1_FILTER_ID           "4cb636b1-fced-11dc-95ff-0800200c9a66"
#define PDL_1_FILTER_NAME         "[[PDL]]1"
#define PDL_2_FILTER_ID           "4cb636b2-fced-11dc-95ff-0800200c9a66"
#define PDL_2_FILTER_NAME         "[[PDL]]2"
#define PDL_3_FILTER_ID           "4cb636b3-fced-11dc-95ff-0800200c9a66"
#define PDL_3_FILTER_NAME         "[[PDL]]3"
#define PDL_4_FILTER_ID           "4cb636b4-fced-11dc-95ff-0800200c9a66"
#define PDL_4_FILTER_NAME         "[[PDL]]4"
#define PDL_5_FILTER_ID           "4cb636b5-fced-11dc-95ff-0800200c9a66"
#define PDL_5_FILTER_NAME         "[[PDL]]5"
/* */

// ---------------------------------------------------------------------------

struct SFilter
{
	string *name;

	// Basic parameters
	int maxSize;
	int minSize;
	int minContrast; // AF only
	int luminanceThreshold; // AF only
	int minFixConfidence; // AF only
	int rgbMotion; // AF only
	int alphaMotion; // AF only
	int correctionEdgeBlur; // AF only
	int shineSpatialContrast; // SHINE only
	int shineTemporalContrast; // SHINE only
	int shineMotionTolerance; // SHINE only
	bool brightOnDarkContrast;
	int dustMaxSize;
	bool useHardMotionOnDust;
	bool usePrecomputedMotions;
	bool useAlphaAsMask;
	bool useGrainFilter;
	int grainFilterParam;

	// Advanced parameters
	int detectChannel;
	bool processChannels[3];
	// bool userDefinedColorProfile;   // determined from values
	// int minR, medR, maxR;
	// int minG, medG, maxG;
	// int minB, medB, maxB;

	SFilter();
	SFilter(const SFilter &rhs);
	~SFilter();
	SFilter& operator = (const SFilter & rhs);

	void SetName(const string &newName);
	string GetName(void);

	void ReadFromIniFile(CIniFile *iniFile, const string &sectionName);
	void WriteToIniFile(CIniFile *iniFile, const string &sectionName);

	bool operator == (const SFilter& rhs);
	bool operator != (const SFilter& rhs);
};
// ---------------------------------------------------------------------------

struct SFilterSlot
{
	bool loaded;
	bool used;
	string filterID;

	SFilterSlot() : loaded(false), used(false), filterID(INVALID_ID) {};
};
// ---------------------------------------------------------------------------

struct SFilterSet
{
	string name;
	SFilterSlot filterSlots[NUMBER_OF_FILTERS_PER_SET];

	SFilterSet();
	SFilterSet(const string &iniFileName, const string &filterID);
	~SFilterSet();

	SFilterSlot GetSlotInfo(int slotNumber);
	int FindSlot(const string &filterID);
	int LoadFilter(const string &filterID, bool useFlag);
	void UnloadFilter(int slotNumber);
	void UnloadFilter(const string &filterID);
	void UnloadAllFilters();
	void ToggleUsed(const string &filterID);
	void ToggleUsed(int slotNumber);
	string GetFilterIDFromSlotNumber(int slotNumber);
	string GetNextLoadedFilterID(const string &filterID, int direction);
	int GetNextLoadedSlotNumber(int slotNumber, int direction);

	void SetName(const string &newName);
	string GetName(void);

	void ReadFromIniFile(CIniFile *iniFile, const string &sectionName);
	void WriteToIniFile(CIniFile *iniFile, const string &sectionName);
};
// ---------------------------------------------------------------------------

class CFilterManager
{
public:
	CFilterManager(bool alphaIsOKArg = false);
	~CFilterManager();

	// Filter Manager ops
	void ReadFromIniFile(const string &iniFileName);
	void WriteToIniFile(const string &iniFileName);
	void GetListOfFilterNames(map<string, string> &filterNames);
	void GetListOfFilterSetNames(map<string, string> &filterSetNames);
	// in the following: pair(name, id)
	void GetSortedListOfFilterNames(vector<std::pair<string, string> > &sortedList);
	void GetSortedListOfFilterSetNames(vector<std::pair<string, string> > &sortedList);
	string FindFilterByName(const string &name);
	string FindFilterSetByName(const string &name);

	// Filter ops
	string AddFilter(const SFilter &filter);
	int LoadFilter(const string &filterID, bool useFlag = true);
	void UnloadFilter(const string &filterID);
	int CopyFilter(const string &filterID);
	void DeleteFilter(const string &filterID);
	void ToggleFilterUsed(const string &filterID);
	SFilter GetFilterDetails(const string &filterID);
	SFilter *GetFilter(const string &filterID);
	bool IsFilterLoaded(const string &filterID);
	bool IsFilterUsed(const string &filterID);
	void SaveFilterDefaults(const string &filterID);
	void RestoreFilterDefaults(const string &filterID);
	bool DoesFilterDifferFromDefaults(const string &filterID);
	string UniquifyFilterName(const string &filterName);

	// Filter Set ops
	void ActivateFilterSet(const string &setID);
	string GetActiveFilterSetID();
	string CreateAnEmptyFilterSet(const string &newName);
	string CopyActiveFilterSetByName(const string &newName);
	void CopyActiveFilterSetByID(const string &filterSetID);
	void ClearFilterSet(const string &filterSetID);
	void DeleteFilterSet(const string &filterSetID);
	string UniquifyFilterSetName(const string &filterSetName);

	// Loaded filters
	int GetFilterSlot(const string &filterID);
	SFilterSlot GetActiveFilterSlotInfo(int slot);
	string GetFilterIDFromSlotNumber(int slotNumber);
	string GetNextLoadedFilterID(const string &filterID, int direction);
	int GetCountOfLoadedFilters();

private:
	bool alphaIsOK;
	string ActiveFilterSetID;
	map<string, SFilterSet>FilterSetList;
	map<string, SFilter>FilterList;
	map<string, SFilter>DefaultsList;

	void CreateBuiltInFilters(void);
	void CreateBuiltInFilterSets(void);
	void CreateABuiltInFilter(const string &filterID, const string &name, int detectChannel, bool bright);
	void CreateABuiltInFilterSet(const string &setID, const string &name, const vector<string>filterIDs);
};
// ---------------------------------------------------------------------------

#endif // FILTER_MANAGER_H
