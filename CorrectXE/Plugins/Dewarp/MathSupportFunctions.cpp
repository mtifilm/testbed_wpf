//---------------------------------------------------------------------------


#pragma hdrstop

#include "MathSupportFunctions.h"
#include "TheError.h"
#include "err_dewarp.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

bool Smooth3(valarray<double> &MC, int nSFixed, int nEFixed, double Alpha)
//
//  nSFixed and nEFixed can only be 0, 1, or 2
//
//
{
   // Don't do anything if alpha is negative
   // This means no smoothing
   if (Alpha < 0) return true;

   // Constant if Alpha is 0
   if (Alpha == 0)
     {
       for (unsigned i=1; i < MC.size(); i++) MC[i] = MC[0];
       return true;
     }

   // If either nSFixed or nEFixed are zero, smoothing required at least 4 points
   int T = MC.size();
   if (((nSFixed == 0) || (nEFixed == 0)) && (T < 4))
     {
       theError.set(DEWARP_ERROR_SMOOTHING_TOO_FEW_FRAMES, "Operation failed: To render this shot, the marked frame range must be greater than four frames");
       return false;
     }

   // Both are anchored
   if (T <= (nSFixed+nEFixed))
     {
       theError.set(DEWARP_ERROR_SMOOTHING_TOO_FEW_FRAMES, "Operation failed: To render this shot, the marked frame range must be greater than (At In + At Out)");
       return false;
     }

   //  If T = 3, then the anchor size are 1 and 1, so we just do a fake smoothing by
   //  duplicating the anchor points.  The reason for this is I don't have
   //  the time to solve this mathematically
   if ((T == 3) && (nSFixed == 1) && (nEFixed == 1))
     {
       matrix<long double> L(5, 5);
       L.zero();

       Alpha = 4*Alpha;
       L[0][0] = Alpha;
       L[1][1] = Alpha;

       L[2][0] = 1;
       L[2][1] = -4;
       L[2][2] = 6 + Alpha;
       L[2][3] = -4;
       L[2][4] = 1;

       L[3][3] = Alpha;
       L[4][4] = Alpha;

       mvector<int> mvIndex(5);
       matrix<long double> LU = L;
       double d;

      // Decompose the matrix
      if (!ludcmp<long double>(LU, mvIndex, d))
      {
         theError.set(DEWARP_ERROR_SMOOTHING_MATRIX_SINGULAR, "Internal Error: Cannot invert matrix");
         return false;
      }

      mvector<long double> Wx(5);
      for (unsigned i=0; i < 3; i++) Wx[i+1] = Alpha*MC[i];
      Wx[0] = Wx[1];
      Wx[4] = Wx[3];

      lubksb<long double>(LU, mvIndex, Wx);
      MC[1] = Wx[2];
      return true;
     }

   // T >= 4 and this is the correct solution.
   matrix<long double> L(T, T);
   L.zero();

   // Do the first two rows
   if (nSFixed > 0)
     {
       L[0][0] = Alpha;
     }
   else
     {
       L[0][0] = 1 + Alpha;
       L[0][1] = -2;
       L[0][2] = 1;
     }

   if (nSFixed > 1)
     {
        L[1][1] = Alpha;
     }
   else
     {
       L[1][0] = -2;
       L[1][1] = 5 + Alpha;
       L[1][2] = -4;
       L[1][3] = 1;
     }

   // Now do the middle of the matrix
   for (int i = 2; i < T-2; i++)
     {
       L[i][i-2] = 1;
       L[i][i-1] = -4;
       L[i][i]   = 6 + Alpha;
       L[i][i+1] = -4;
       L[i][i+2] = 1;
     }


   // Do the last two rows
   if (nEFixed > 1)
     {
       L[T-2][T-2] = Alpha;
     }
   else
     {
       L[T-2][T-4] = 1;
       L[T-2][T-3] = -4;
       L[T-2][T-2] = 5 + Alpha;
       L[T-2][T-1] = -2;
     }

   if (nEFixed > 0)
     {
       L[T-1][T-1] = Alpha;
     }
   else
     {
       L[T-1][T-3] = 1;
       L[T-1][T-2] = -2;
       L[T-1][T-1] = 1 + Alpha;
     }

   mvector<int> mvIndex(T);
   matrix<long double> LU = L;
   double d;

   // Decompose the matrix
    if (!ludcmp<long double>(LU, mvIndex, d))
      {
         theError.set(DEWARP_ERROR_SMOOTHING_MATRIX_SINGULAR, "Internal Error: Cannot invert matrix");
         return false;
      }

    mvector<long double> Wx(T);
    for (unsigned i=0; i < MC.size(); i++) Wx[i] = Alpha*MC[i];

    lubksb<long double>(LU, mvIndex, Wx);

    for (unsigned i=0; i < MC.size(); i++) MC[i] = Wx[i];

    return true;
}


float UEuclidean(float X0, float Y0, float X1, float Y1)
// Computes the Euclidean distance
{
   float r0 = (X0 - X1);
   float r1 = (Y0 - Y1);
   return sqrt((float)(r0*r0 + r1*r1));
}

float UThinPlate(float X0, float Y0, float X1, float Y1)
// Computes the thin spline Distance
{
   float r0 = (X0 - X1);
   float r1 = (Y0 - Y1);
   float r = sqrt(r0*r0 + r1*r1);
   if (r == 0) return 0;
   if (r <= 10E-100) r=10E-100;
   return r*r*log(r);
}

float ULogrithmic(float X0, float Y0, float X1, float Y1)
{
   float r0 = (X0 - X1);
   float r1 = (Y0 - Y1);
   float r = sqrt(r0*r0 + r1*r1);
   if (r == 0) return 0;
   if (r <= 10E-100) r=10E-100;
   return r*log(r);
}

float UIdenity(float X0, float Y0, float X1, float Y1)
{
   return 0;
}

int L1Norm(MTI_UINT16 *p1, MTI_UINT16 *p2, int N)
{
  int Result = 0;
  for (int i=0; i < N; i++)
      Result += abs(p1[i] - p2[i]);

  return Result;
}

double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y)
{
   double dRet = 0;
   MTI_UINT16 *pB = pBase;
   MTI_UINT16 *pN = pNext + y*nN + x;
   for (int i=0; i < nB; i++)
     {
        dRet += L1Norm(pB, pN, nB);
        pB += nB;
        pN += nN;
     }

   return dRet;
}

 void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow, double x, double y, int N, MTI_UINT16 *imgOut)
{
//   int ix = tp.x + 0.5;
//   int iy = tp.y + 0.5;
   int ix = x;
   int iy = y;

   // Copy over the data, points outside of the image are defined as 0
   int iSC = ix - N;
   if (iSC >= nCol) return;
   int iEC = ix + N;
   if (iEC < 0) return;

   int iSR = iy - N;
   if (iSR >= nRow) return;
   int iER = iy + N;
   if (iER < 0) return;

   // Set the extent of the copy
   if (iSC < 0) iSC = 0;
   if (iEC >= nCol) iEC = nCol-1;
   if (iSR < 0) iSR = 0;
   if (iER >= nRow) iER = nRow-1;

   int mPPerR = 2*N+1;

   MTI_UINT16 *pB = imgBase + iSR*nCol + iSC;
   MTI_UINT16 *pN = imgOut + (iSR - (iy-N))*mPPerR + (iSC - (ix - N));

   for (int iR = iSR; iR <= iER; iR++)
     {
        memmove(pN, pB, (iEC - iSC + 1)*sizeof(MTI_UINT16));
        pB += nCol;
        pN += mPPerR;
     }

   // Now fill in any missing points
   // We don't have to be fast here as it doesn't happen often

   // Fill them at the bottom
   pB = imgOut + (iER-iSR)*mPPerR;
   pN = imgOut + (iER-iSR+1)*mPPerR;
   for (int iR = iER+1; iR <= iy+N; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them in at the top
   pB = imgOut + (iSR - (iy-N))*mPPerR;
   pN = imgOut;
   for (int iR = iy-N; iR < iSR ; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them out at the left side
   if (iSC == 0)
     {
       pN = imgOut;
       for (int iR = -N; iR <= N ; iR++)
         {
           for (int iC=0; iC < iSC - (ix-N); iC++)
             pN[iC] = pN[iSC - (ix-N)];

           pN += mPPerR;
         }
     }

   // Fill them out at the left side
   if (iEC == (nCol-1))
     {
       pN = imgOut;
       for (int iR = -N; iR <= N ; iR++)
         {
           for (int iC=nCol; iC < ix+N; iC++)
             pN[iC-iSC] = pN[nCol-iSC-1];

           pN += mPPerR;
         }
     }

}

void CreateSubPixelMatrix2(MTI_UINT16 *pIn, int nCol, double x, double y, int N, MTI_UINT16 *pOut)

// Computes the fixed (4 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 4 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - N)*nCol + (int)x - N;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 4*(2*N + 1);

   for (int iR = 0; iR < (2*N+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*N+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            // y = 0
            *p0++ = d;
            *p0++ = 0.25*a + d;
            *p0++ = 0.50*a + d;
            *p0++ = 0.75*a + d;

            // y = .25
            *p1++ = 0.25*b + d;
            *p1++ = 0.25*a + 0.25*b + (0.25*0.25)*c + d;
            *p1++ = 0.50*a + 0.25*b + (0.50*0.25)*c + d;
            *p1++ = 0.75*a + 0.25*b + (0.75*0.25)*c + d;

            // y = .50
            *p2++ = 0.50*b + d;
            *p2++ = 0.25*a + 0.50*b + (0.25*0.50)*c + d;
            *p2++ = 0.50*a + 0.50*b + (0.50*0.50)*c + d;
            *p2++ = 0.75*a + 0.50*b + (0.75*0.50)*c + d;

            // y = .75
            *p3++ = 0.75*b + d;
            *p3++ = 0.25*a + 0.75*b + (0.25*0.75)*c + d;
            *p3++ = 0.50*a + 0.75*b + (0.50*0.75)*c + d;
            *p3++ = 0.75*a + 0.75*b + (0.75*0.75)*c + d;

             pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 4*IntraColSize;
     }
}

RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB)

// This finds the best MAT box for a monochrome image
//   pIn is the image of size nCol, nRow
//   Return is the Mat Box
//
//*****************************************************************************
{

   RECT dRect = inRect;
   if (RGB) Threshold = 3*Threshold;

   // Now test the top row
   for (int iR=dRect.top; iR < nRow/2; iR++)
    {
      MTI_UINT16 *p = pIn + iR*nCol*3;
      int m = 0;
      for (int iC=dRect.left; iC < dRect.right; iC++)
        if (RGB)
          m += *p++ + *p++ + *p++;
        else
         { m += *p; p += 3;}

      m = m/(dRect.right - dRect.left + 1);
      if (m > Threshold)
            {
              dRect.top = iR;
              break;
            }

    }

   // Now test the bottom row
   for (int iR=dRect.bottom; iR > nRow/2; iR--)
    {
      MTI_UINT16 *p = pIn + iR*nCol*3;
      int m = 0;
      for (int iC=dRect.left; iC < dRect.right; iC++)
        if (RGB)
          m += *p++ + *p++ + *p++;
        else
         { m += *p; p += 3;}

      m = m/(dRect.right - dRect.left + 1);
      if (m > Threshold)
        {
          dRect.bottom = iR;
          break;
        }
    }

    // Now test the left
    for (int iC=dRect.left; iC < nCol/2; iC++)
    {
      int m = 0;
      for (int iR=dRect.top; iR < dRect.bottom; iR++)
        {
          MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
          if (RGB)
            m += *p++ + *p++ + *p++;
          else
            m += *p;
         }

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.left = iC;
           break;
         }

    }

    // Now test the right
    for (int iC=dRect.right; iC > nCol/2; iC--)
    {
      int m = 0;
      for (int iR=dRect.top; iR < dRect.bottom; iR++)
        {
          MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
          if (RGB)
            m += *p++ + *p++ + *p++;
          else
            m += *p;
         }

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.right = iC;
           break;
         }
    }

   return dRect;

}

#define FIND_MATTE_BASED_ON_LUMINANCE_ONLY 1

RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB, int R, int G, int B)

// This finds the best MAT box for a monochrome image
//   pIn is the image of size nCol, nRow
//   Return is the Mat Box
//
//*****************************************************************************
{

   RECT dRect = inRect;
   int Ylow = int(R*0.29 + G*0.60 + B*0.11) - Threshold/2;
   int Yhigh = Ylow + Threshold;

   if (RGB)
   {
#ifdef FIND_MATTE_BASED_ON_LUMINANCE_ONLY
      // Now test the top row
      for (int iR=dRect.top; iR < nRow/2; iR++)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         int y = int(r*0.29 + g*0.60 + b*0.11) / (dRect.right - dRect.left + 1);
         if (y < Ylow || y > Yhigh)
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=dRect.bottom; iR > nRow/2; iR--)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         int y = int(r*0.29 + g*0.60 + b*0.11) / (dRect.right - dRect.left + 1);
         if (y < Ylow || y > Yhigh)
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Now test the left side
      for (int iC=dRect.left; iC < nCol/2; iC++)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         int y = int(r*0.29 + g*0.60 + b*0.11) / (dRect.bottom - dRect.top + 1);
         if (y < Ylow || y > Yhigh)
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC=dRect.right; iC > nCol/2; iC--)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         int y = int(r*0.29 + g*0.60 + b*0.11) / (dRect.bottom - dRect.top + 1);
         if (y < Ylow || y > Yhigh)
         {
            dRect.right = iC;
            break;
         }
      }

#else // find matte based on actual color

      int Rlow = R - Threshold/2;
      int Rhigh = Rlow + Threshold;
      int Glow = G - Threshold/2;
      int Ghigh = Glow + Threshold;
      int Blow = B - Threshold/2;
      int Bhigh = Blow + Threshold;

      // Now test the top row
      for (int iR=dRect.top; iR < nRow/2; iR++)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         r /= (dRect.right - dRect.left + 1);
         g /= (dRect.right - dRect.left + 1);
         b /= (dRect.right - dRect.left + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=dRect.bottom; iR > nRow/2; iR--)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         r /= (dRect.right - dRect.left + 1);
         g /= (dRect.right - dRect.left + 1);
         b /= (dRect.right - dRect.left + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Now test the left side
      for (int iC=dRect.left; iC < nCol/2; iC++)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         r /= (dRect.bottom - dRect.top + 1);
         g /= (dRect.bottom - dRect.top + 1);
         b /= (dRect.bottom - dRect.top + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC=dRect.right; iC > nCol/2; iC--)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         r /= (dRect.bottom - dRect.top + 1);
         g /= (dRect.bottom - dRect.top + 1);
         b /= (dRect.bottom - dRect.top + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.right = iC;
            break;
         }
      }
#endif
   }
   else // !RGB
   {
      // Now test the top row
      for (int iR=dRect.top; iR < nRow/2; iR++)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int y = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            y += p[0];
            p += 3;
         }
         int yAvg = y / (dRect.right - dRect.left + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=dRect.bottom; iR > nRow/2; iR--)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int y = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            y += p[0];
            p += 3;
         }
         int yAvg = y / (dRect.right - dRect.left + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Now test the left side
      for (int iC=dRect.left; iC < nCol/2; iC++)
      {
         int y = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            y += p[0];
         }
         int yAvg = y / (dRect.bottom - dRect.top + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC=dRect.right; iC > nCol/2; iC--)
      {
         int y = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            y += p[0];
         }
         int yAvg = y / (dRect.bottom - dRect.top + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.right = iC;
            break;
         }
      }
   }

   return dRect;

}

double BiLinear( double x, double y, double C00, double C10, double C01, double C11)
{
      double a = -C00 + C10;
      double b = -C00 + C01;
      double c =  C00 - C10 - C01 + C11;
      double d =  C00;

      x = x - (int)x;
      y = y - (int)y;
      return a*x + b*y + x*y*c + d;
}

void MakeMonoChromeImageRGB(MTI_UINT16 *pIn, int nCol, int nRow, MTI_UINT16 *pOut)
{
  MTI_UINT16 *p1 = pIn;
  MTI_UINT16 *p0 = pOut;
  for (int i=0; i < nRow*nCol; i++)
    {
       double R = *p1++;
       double G = *p1++;
       double B = *p1++;
       *p0++ = 0.33*(R + G + B);
    }
}

void MakeMonoChromeImageYUV(MTI_UINT16 *pIn, int nCol, int nRow, MTI_UINT16 *pOut)
{
  MTI_UINT16 *p1 = pIn;
  MTI_UINT16 *p0 = pOut;
  for (int i=0; i < nRow*nCol; i++)
    {
       *p0++ = *p1++;
       p1++;
       p1++;
    }
}

void MakeMonoChromeImage(MTI_UINT16 *pIn, int nCol, int nRow, MTI_UINT16 *pOut, bool RGB)
{
   if (RGB)
     MakeMonoChromeImageRGB(pIn, nCol, nRow, pOut);
   else
     MakeMonoChromeImageYUV(pIn, nCol, nRow, pOut);
}

#define TINY 1.0e-30

void EstimateMinFrom9Points(double D[9], double &x, double &y)
{
  double a = D[0] - 2*D[1] + D[2] + D[3] - 2*D[4] + D[5] + D[6] - 2*D[7] + D[8];
  double b = D[0] + D[1] + D[2] - 2*D[3] - 2*D[4] - 2*D[5] + D[6] + D[7] + D[8];
  double c = 1.5*(D[0] - D[2] - D[6] + D[8]);
  double d = -D[0] + D[2] - D[3] + D[5] - D[6] + D[8];
  double e = -D[0] - D[1] - D[2] + D[6] + D[7] + D[8];

  double g = 4*a*b - c*c;

  // No 2D quad solution if g is small
  if (abs(g) <= TINY)
    {
       // See if the 1D solution is ok
       if (abs(a) <= TINY)
         x = 0;  // No return the center
       else
         x = d/(2*a);

       // See if the 1D solution is ok
       if (abs(b) <= TINY)
         y = 0;  // No return the center
       else
         y = e/(2*b);
       return;
    }

  x = (e*c - 2*b*d)/g;
  y = (d*c - 2*a*e)/g;
}


#define MAXIT 100
#define EPS 3.0e-7
#define FPMIN 1.0e-30

double betacf(double a, double b, double x)
{
//	void nrerror();
	int m,m2;
	double aa,c,d,del,h,qab,qam,qap;

	qab=a+b;
	qap=a+1.0;
	qam=a-1.0;
	c=1.0;
	d=1.0-qab*x/qap;
	if (fabs(d) < FPMIN) d=FPMIN;
	d=1.0/d;
	h=d;
	for (m=1;m<=MAXIT;m++) {
		m2=2*m;
		aa=m*(b-m)*x/((qam+m2)*(a+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		h *= d*c;
		aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		del=d*c;
		h *= del;
		if (fabs(del-1.0) < EPS) break;
	}
	if (m > MAXIT) throw("a or b too big, or MAXIT too small in betacf");
	return h;
}
#undef MAXIT
#undef EPS
#undef FPMIN

double betai(double a, double b, double x)
{
	double betacf(double a, double b, double x),gammln(double x);
//	void nrerror();
	double bt;

	if (x < 0.0 || x > 1.0) throw("Bad x in routine betai");
	if (x == 0.0 || x == 1.0) bt=0.0;
	else
		bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.0-x));
	if (x < (a+1.0)/(a+b+2.0))
		return bt*betacf(a,b,x)/a;
	else
		return 1.0-bt*betacf(b,a,1.0-x)/b;
}



void pearsn(double x[], double y[], unsigned long n, double &r, double &prob, double &z)
{
	double betai(double a, double b, double x);
	unsigned long j;
	double yt,xt,t,df;
	double syy=0.0,sxy=0.0,sxx=0.0,ay=0.0,ax=0.0;

	for (j=1;j<=n;j++) {
		ax += x[j];
		ay += y[j];
	}
	ax /= n;
	ay /= n;
	for (j=1;j<=n;j++) {
		xt=x[j]-ax;
		yt=y[j]-ay;
		sxx += xt*xt;
		syy += yt*yt;
		sxy += xt*yt;
	}
	r=sxy/(sqrt(sxx*syy)+TINY);
	z=0.5*log((1.0+(r)+TINY)/(1.0-(r)+TINY));
//	df=n-2;
//	t=(r)*sqrt(df/((1.0-(r)+TINY)*(1.0+(r)+TINY)));
        prob = 0;
//	prob=betai(0.5*df,0.5,df/(df+t*t));
}
#undef TINY