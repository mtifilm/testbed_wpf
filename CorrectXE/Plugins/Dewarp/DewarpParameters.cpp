//---------------------------------------------------------------------------


#pragma hdrstop

#include "DewarpParameters.h"
#include "PDL.h"
#pragma package(smart_init)

static const SIniAssociation UFunctionData[] =
{
	{"U_Fcn_Euclidean", U_FUNCTION_TYPE_LINEAR},
	{"U_Fcn_Logrithmic", U_FUNCTION_TYPE_R_LOGR},
	{"U_Fcn_Thin_Plate", U_FUNCTION_TYPE_R2_LOGR},
	{"U_Fcn_None", U_FUNCTION_TYPE_NONE},
        {"", -1}
};

static const SIniAssociation MissingFillData[] =
{
	{"MISSING_DATA_White",MISSING_DATA_WHITE},
	{"MISSING_DATA_Black", MISSING_DATA_BLACK},
	{"MISSING_DATA_Previous_Frame", MISSING_DATA_PREVIOUS},
        {"", -1}
};

//

CIniAssociations UFunctionAssociations(UFunctionData, "U_Fcn_");
CIniAssociations UMissingDataAssociations(MissingFillData, "MISSING_DATA");

//---------------------------------------------------------------------------
CDewarpParameters::CDewarpParameters()
{
}

CDewarpParameters::CDewarpParameters(const CDewarpParameters &rhs)
{
	*this = rhs;
}

//----------------------ReadParameters-----------------------------April 2006---

    bool CDewarpParameters::ReadParameters(const string &strFileName)

//  Reads in the parametet from the ini file of name strFileName
//    return true if it succeeds
//           false on failure
//
//******************************************************************************
{
   CIniFile *IniFile = CreateIniFile(strFileName);
   if (IniFile == NULL)
      {
         TRACE_0(errout << "ERROR: CDewarpParameters::ReadParameters: Could not read "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());
         return false;
      }

	ReadParameters(IniFile, "Dewarp");

   if (!DeleteIniFile(IniFile))
     {
         TRACE_0(errout << "ERROR: CDewarpParameters::ReadParameters: Could not close "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());

         return false;
     }

   return true;
}

//----------------------WriteParameters----------------------------April 2006---

    bool CDewarpParameters::WriteParameters(const string &strFileName) const

//  Write out the parametet to the ini file of name strFileName
//    return true if it succeeds
//           false on failure
//
//******************************************************************************
{
   CIniFile *IniFile = CreateIniFile(strFileName);
   if (IniFile == NULL)
      {
         TRACE_0(errout << "ERROR: CDewarpParameters::WriteParameters: Could not read "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());
         return false;
      }

	WriteParameters(IniFile, "Dewarp");

   if (!DeleteIniFile(IniFile))
     {
         TRACE_0(errout << "ERROR: CDewarpParameters::WriteParameters: Could not close "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());

         return false;
     }

   return true;
}

//----------------ReadParameters---------------------John Mertus-----Jan 2001---

	void CDewarpParameters::ReadParameters(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   TrackBoxExtent = ini->ReadInteger(IniSection, "TrackBoxExtent", TrackBoxExtent);
   SearchBoxExtent = ini->ReadInteger(IniSection, "SearchBoxExtent", SearchBoxExtent);
   GoodFramesIn = ini->ReadInteger(IniSection, "GoodFramesIn", GoodFramesIn);
   GoodFramesOut = ini->ReadInteger(IniSection, "GoodFramesOut", GoodFramesOut);
   Alpha2 = ini->ReadDouble(IniSection, "Alpha2", Alpha2);
   UFunction = (EUFunctionType)ini->ReadAssociation(UFunctionAssociations, IniSection, "UFunction", UFunction);
   MissingDataFill = (EMissingDataFillType)ini->ReadAssociation(UMissingDataAssociations, IniSection, "MissingDataFill", MissingDataFill);
   UseMatting = ini->ReadBool(IniSection, "UseMatting", UseMatting);
   KeepOriginalMatValues = ini->ReadBool(IniSection, "KeepOriginalMatValues", KeepOriginalMatValues);
}

//----------------WriteSParameters----------------John Mertus-----Jan 2001---

	void CDewarpParameters::WriteParameters(CIniFile *ini, const string &IniSection) const

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
   ini->WriteInteger(IniSection, "TrackBoxExtent", TrackBoxExtent);
   ini->WriteInteger(IniSection, "SearchBoxExtent", SearchBoxExtent);
   ini->WriteInteger(IniSection, "GoodFramesIn", GoodFramesIn);
   ini->WriteInteger(IniSection, "GoodFramesOut", GoodFramesOut);
   ini->WriteDouble(IniSection, "Alpha2", Alpha2);
   ini->WriteAssociation(UFunctionAssociations, IniSection, "UFunction", UFunction);
   ini->WriteAssociation(UMissingDataAssociations, IniSection, "MissingDataFill", MissingDataFill);
   ini->WriteBool(IniSection, "UseMatting", UseMatting);
   ini->WriteBool(IniSection, "KeepOriginalMatValues", KeepOriginalMatValues);
}

//---------------------------------------------------------------------------

void CDewarpParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
   // Read Dewarp Parameters from a PDL Entry's Tool Attributes

   TrackBoxExtent = pdlToolAttribs->GetAttribInteger("TrackBoxExtent",
                                                     TrackBoxExtent);
   SearchBoxExtent = pdlToolAttribs->GetAttribInteger("SearchBoxExtent",
                                                      SearchBoxExtent);
   GoodFramesIn = pdlToolAttribs->GetAttribInteger("GoodFramesIn",
                                                   GoodFramesIn);
   GoodFramesOut = pdlToolAttribs->GetAttribInteger("GoodFramesOut",
                                                    GoodFramesOut);
   Alpha2 = pdlToolAttribs->GetAttribDouble("Alpha2", Alpha2);

   string valueStr, defaultStr;
   int newValue;

   defaultStr = UFunctionAssociations.SymbolicName(UFunction);
   valueStr = pdlToolAttribs->GetAttribString("UFunction", defaultStr);
   if (UFunctionAssociations.ValueBySymbolicName(valueStr, newValue))
      UFunction =  (EUFunctionType)newValue;
      
   defaultStr = UMissingDataAssociations.SymbolicName(MissingDataFill);
   valueStr = pdlToolAttribs->GetAttribString("MissingDataFill", defaultStr);
   if (UMissingDataAssociations.ValueBySymbolicName(valueStr, newValue))
      MissingDataFill =  (EMissingDataFillType)newValue;

   UseMatting = pdlToolAttribs->GetAttribBool("UseMatting", UseMatting);
   KeepOriginalMatValues = pdlToolAttribs->GetAttribBool("KeepOriginalMatValues",
                                                         KeepOriginalMatValues);

}

void CDewarpParameters::WritePDLEntry(CPDLElement &parent)
{
   // Write Dewarp Parameters to a PDL Entry's Tool Attributes
   
   CPDLElement *pdlToolAttribs = parent.MakeNewChild("DewarpParameters");

   pdlToolAttribs->SetAttribInteger("TrackBoxExtent", TrackBoxExtent);
   pdlToolAttribs->SetAttribInteger("SearchBoxExtent", SearchBoxExtent);
   pdlToolAttribs->SetAttribInteger("GoodFramesIn", GoodFramesIn);
   pdlToolAttribs->SetAttribInteger("GoodFramesOut", GoodFramesOut);
   pdlToolAttribs->SetAttribDouble("Alpha2", Alpha2);
   string UFunctionStr = UFunctionAssociations.SymbolicName(UFunction);
   pdlToolAttribs->SetAttribString("UFunction", UFunctionStr);
   string MissingDataFillStr = UMissingDataAssociations.SymbolicName(MissingDataFill);
   pdlToolAttribs->SetAttribString("MissingDataFill", MissingDataFillStr);
   pdlToolAttribs->SetAttribBool("UseMatting", UseMatting);
   pdlToolAttribs->SetAttribBool("KeepOriginalMatValues",
                                 KeepOriginalMatValues);
}
//---------------------------------------------------------------------------

bool CDewarpParameters::AreParametersTheSame(const IPresetParameters &presetParameter) const
{
	auto rhs = (CDewarpParameters *)(&presetParameter);

   // NOTE: WE DO NOT COMPARE THE Tag!

	return
		TrackBoxExtent == rhs->TrackBoxExtent
		&& SearchBoxExtent == rhs->SearchBoxExtent
		&& GoodFramesIn == rhs->GoodFramesIn
		&& GoodFramesOut == rhs->GoodFramesOut
		&& Alpha2 == rhs->Alpha2
		&& UFunction == rhs->UFunction
		&& MissingDataFill == rhs->MissingDataFill
		&& UseMatting == rhs->UseMatting
		&& KeepOriginalMatValues == rhs->KeepOriginalMatValues;
}
//---------------------------------------------------------------------------

bool CDewarpParameters::operator == (const CDewarpParameters &rhs) const
{
	return AreParametersTheSame(rhs);
}
//---------------------------------------------------------------------------

bool CDewarpParameters::operator != (const CDewarpParameters &rhs) const
{
	return !(*this == rhs);
}
//---------------------------------------------------------------------------

CDewarpParameters &CDewarpParameters::operator = (const CDewarpParameters &rhs)
{
	if (this == &rhs)
	{
		return *this;
	}

	TrackBoxExtent = rhs.TrackBoxExtent;
	SearchBoxExtent = rhs.SearchBoxExtent;
	GoodFramesIn = rhs.GoodFramesIn;
	GoodFramesOut = rhs.GoodFramesOut;
	Alpha2 = rhs.Alpha2;
	UFunction = rhs.UFunction;
	MissingDataFill = rhs.MissingDataFill;
	UseMatting = rhs.UseMatting;
	KeepOriginalMatValues = rhs.KeepOriginalMatValues;
	Tag = rhs.Tag;

	return *this;
}
//---------------------------------------------------------------------------


