//---------------------------------------------------------------------------

#include "TrackingPoints.h"
#include <valarray>
#include "PDL.h"

#pragma hdrstop
// Create Associations
static const SIniAssociation AnchorAssociationsData[] =
{
	{"None", EANCHOR_NONE},
	{"Anchor_Horizontal", EANCHOR_HORZIONAL},
	{"Anchor_Vertical", EANCHOR_VERTICAL},
	{"Anchor_Both", EANCHOR_ALL},
	{"Fix_Horizontal", EFIXED_HORIZIONAL},
	{"Fix_Vertical", EFIXED_VERTICAL},
   {"Fix_Both", EFIXED_ALL},
   {"", -1}
};

//

CIniAssociations AnchorAssociations(AnchorAssociationsData, "");


//---------------------------------------------------------------------------

// Since there could be many tracking points, the PDL entries' XML element
// names are kept deliberately short

const string groupKey = "Gr";
const string smoothingEnabledKey = "SmEn";
const string smoothingKey = "Sm";
const string alphaKey = "Al";
const string anchorKey = "An";
const string anchorIndexKey = "AnIn";

const string indexKey = "TpNdx";
const string frameIndexKey = "FrNdx";

const string XKey = "X";
const string YKey = "Y";

//---------------------------------------------------------------------------

#pragma package(smart_init)

//-----------CTrackPoint constructors----------------------------Feb 2005---

      CTrackPoint::CTrackPoint(void)

//  Default constructor creates an invalid point
//
//*****************************************************************************
{
   _reset();
}

      CTrackPoint::CTrackPoint(double dX, double dY, bool sel)

// Explicit constructors create a valid point
//
//*****************************************************************************
{
   _reset();
   x = dX;
   y = dY;
   selected = sel;
   _valid = true;
}


      CTrackPoint::CTrackPoint(CTrackPoint const &tp)

// Explicit constructors create a valid point
//
//*****************************************************************************
{
   assign(tp);
}

      CTrackPoint::CTrackPoint(const DSPOINT  &dp)

// Explicit constructors create a valid point
//
//*****************************************************************************
{
   assign(dp);
}

      CTrackPoint::CTrackPoint(const DPOINT &dp)

// Explicit constructors create a valid point
//
//*****************************************************************************
{
   assign(dp);
}

//--------------------~CTrackPoint------------------------------Feb 2005---

      CTrackPoint::~CTrackPoint(void)

//  Usual destructor, do nothing
//
//*****************************************************************************
{
}

//-------------------------_reset----------------------------------Feb 2005---

      void CTrackPoint::_reset(void)

//  Reset creates an invalid Track point
//
//*****************************************************************************
{
    x = 0;
    y = 0;
    selected = false;
    _valid = false;
}

//-------------------------assign----------------------------------Feb 2005---

    void CTrackPoint::assign(const DPOINT &dp)

//  Assign is just a deep copy
//
//*****************************************************************************
{
   _reset();

   x = dp.x;
   y = dp.y;
   _valid = true;

}

//-------------------------assign----------------------------------Feb 2005---

    void CTrackPoint::assign(const DSPOINT &dsp)

//  Assign is just a deep copy
//
//*****************************************************************************
{
   _reset();
   x = dsp.x;
   y = dsp.y;
   selected = dsp.selected;
   _valid = true;
}

//-------------------------assign----------------------------------Feb 2005---

    void CTrackPoint::assign(const CTrackPoint &tp)

//  Assign is just a deep copy
//
//*****************************************************************************
{
   x = tp.x;
   y = tp.y;
   selected = tp.selected;
   _valid = tp._valid;
}

//------------------------isValid----------------------------------------------

   bool CTrackPoint::isValid(void) const

//  Returns if the point is a valid point
//
//*****************************************************************************
{
   return _valid;
}

#if 0
//------------------------setSelected------------------------------------------

     void CTrackPoint::setSelected(bool newsel)

//  Sets select state of point
//
//*****************************************************************************
{
   selected = newsel;
}

//------------------------isSelected-------------------------------------------

     bool CTrackPoint::isSelected(void)

//  Returns if the point is a selected point
//
//*****************************************************************************
{
   return selected;
}
#endif

//--------------------~operator == --------------------------------Feb 2005---

      bool CTrackPoint::operator==(const CTrackPoint &rhs) const

// Usual equality operator, define in terms of !=
//
//*****************************************************************************
{
   return !(*this != rhs);
}

//--------------------~operator != --------------------------------Feb 2005---

      bool CTrackPoint::operator!=(const CTrackPoint &rhs) const

// Usual not equal operator
//
//*****************************************************************************
{
   return (x != rhs.x) || (y != rhs.y) || (isValid() != rhs.isValid());
}

//--------------------~operator = --------------------------------Feb 2005---

      CTrackPoint &CTrackPoint::operator=(const CTrackPoint &tp)

// Copy operator
//
//*****************************************************************************
{
   assign(tp);
   return *this;
}

//--------------------~operator POINT------------------------------Feb 2005---

      CTrackPoint::operator POINT(void) const

// Convert a Track point into a point
//
//*****************************************************************************
{
   POINT p;
   p.x = x;
   p.y = y;
   return p;
}


//--------------------~operator =POINT------------------------------Feb 2005---

      CTrackPoint const &CTrackPoint::operator=(const POINT &iP)


// Assigns a point to a Track point, tags and frame numbers are unaffe ted
//
//*****************************************************************************
{
  x = iP.x;
  y = iP.y;
  _valid = true;
  return *this;
}

//--------------------~operator =DSPOINT-----------------------------Feb 2005---

      CTrackPoint const &CTrackPoint::operator=(const DSPOINT &dP)


// Assigns a point to a Track point, tags and frame numbers are unaffe ted
//
//*****************************************************************************
{
  x = dP.x;
  y = dP.y;
  selected = dP.selected;
  _valid = true;
  return *this;
}

//--------------------~operator =DPOINT------------------------------Feb 2005---

      CTrackPoint const &CTrackPoint::operator=(const DPOINT &dP)


// Assigns a point to a Track point, tags and frame numbers are unaffe ted
//
//*****************************************************************************
{
  x = dP.x;
  y = dP.y;
  selected = false;
  _valid = true;
  return *this;
}

int CTrackPoint::ReadPDLEntry(CPDLElement *pdlTP)
{
   int index = pdlTP->GetAttribInteger(indexKey, 0);

   x = pdlTP->GetAttribInteger(XKey, x);
   y = pdlTP->GetAttribInteger(YKey, y);
   _valid = true;

   return index;
}

void CTrackPoint::AddToPDLEntry(CPDLElement &parent, int index)
{
   CPDLElement *pdlTP = parent.MakeNewChild("TP");

   pdlTP->SetAttribInteger(indexKey, index);
   pdlTP->SetAttribDouble(XKey, x);
   pdlTP->SetAttribDouble(YKey, y);
}

//--------------------~operator <<-----------------------------------Apr 2005---

    std::ostream& operator<<(std::ostream &theStream, const CTrackPoint &tp)
{
   theStream << "(" << tp.x << ", " << tp.y << ")";
   return theStream;
}

//--------------------~operator >>-----------------------------------Apr 2005---

    std::istream& operator>>(std::istream &theStream, CTrackPoint &tp)
{
   char p = '\0';
   double x, y;

   theStream >> p;
   if (p != '(')
     {
        return theStream;
     }

   // Do the x
   if (!(theStream >> x))
     {
        return theStream;
     }

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }

   // Do the y
   if (!(theStream >> y))
     {
        return theStream;
     }

   // eat the final )
   theStream >> p;
   if (p != ')')
     {
        return theStream;
     }

   tp = CTrackPoint(x,y);
   return theStream;
}

#if 0
//--------------------getAllowedMovement-----------------------------Feb 2005---

      tpMovement CTrackPoint::getAllowedMovement(void)

// Just gets the movement flag
//
//*****************************************************************************
{
    return _allowedMovement;
}

//--------------------setAllowedMovement-----------------------------Feb 2005---

      void CTrackPoint::setAllowedMovement(tpMovement &newMove)

// Just sets the movement flag
//
//*****************************************************************************
{
    _allowedMovement = newMove;
}

//--------------------getSmoothing-----------------------------Apr 2006---

      int CTrackPoint::getSmoothing(void)

// Just gets the smoothing flag
//
//*****************************************************************************
{
    return _smoothing;
}

//--------------------setSmoothing-----------------------------Apr 2006---

      void CTrackPoint::setSmoothing(int newSmooth)

// Just sets the smoothing flag
//
//*****************************************************************************
{
    _smoothing = newSmooth;
}

//-------------------------getGroup----------------------------------Apr 2006---

      int CTrackPoint::getGroup(void)

// Just gets the group flag
//
//*****************************************************************************
{
    return _group;
}



//-------------------------setGroup----------------------------------Apr 2006---

      void CTrackPoint::setGroup(int newGroup)

// Just sets the group flag
//
//*****************************************************************************
{
    _group = newGroup;
}
#endif

//////////////////////////// CTrackingPoints //////////////////////////////////

//------------------------isValid----------------------------------------------

    bool CTrackingPoints::isValid(int tag) const

//  Decide if the tag represents an actual point
//
//*****************************************************************************
{
   // See if key exists
//  For unknown reasons, this does not work
   // See if key exists
   if (((find(tag) != end()) != find(tag)->second.isValid()))
    {
      return false;
    }

   return find(tag)->second.isValid();
}

//------------------------beginIndex-------------------------------------------

   int CTrackingPoints::beginIndex(void) const

//  Just decide where the smallest tag exists.
//
//*****************************************************************************
{
   if (size() == 0) return 0;
   return begin()->first;
}

//------------------------endIndex--------------------------------------------

   int CTrackingPoints::endIndex(void) const

//  Just finds the index that is one past the last index of the array
//
//*****************************************************************************
{
   if (size() == 0) return 0;

   CTrackingPoints::const_iterator i = end();
   i--;

   return i->first + 1;
}

//-------------------------------at--------------------------------------------

   CTrackPoint &CTrackingPoints::at(int tag)

//  Just return the point in the array
//
//*****************************************************************************
{
   return (*this)[tag];
}

//-------------------------------at--------------------------------------------

   const CTrackPoint &CTrackingPoints::at(int tag) const

//  Just return the point in the array
//
//*****************************************************************************
{
   for (CTrackingPoints::const_iterator i = begin(); i !=end(); ++i)
     if (i->first == tag) return i->second;

   return begin()->second;
}

//------------------------------add--------------------------------------------

   int CTrackingPoints::add(double x, double y, bool sel)

//  Just add a point to end of array
//
//*****************************************************************************
{
    CTrackPoint tp(x,y, sel);
    return add(tp);
}
//------------------------------add--------------------------------------------

   int CTrackingPoints::add(CTrackPoint &dp)

//  Just add a point to end of array
//
//*****************************************************************************
{
    int n = endIndex();
    at(n) = dp;
    return n;
}

//-------------------------collapse------------------------------------------

   int CTrackingPoints::collapse(void)

//  Collapse is a function that removes all invalid tracking points.
//  This is very dangerous to call and will mess up the tags in the tracking
//  array.
//
//  Return is new size
//
//*****************************************************************************
{
    unsigned k=0;
    for (unsigned i=0; i < size(); i++)
      if (at(i).isValid())
        {
          at(k) = at(i);
          k++;
        }

    // Note: This works because CTrackingpoints are maps not vectors
    int oldSize = size();
    for (int i=k; i < oldSize; i++)
      erase(i);


    return k;
}

//-------------------------validSize------------------------------------------

   unsigned int CTrackingPoints::validSize(void)

//  Compute the number of the points that are valid.
//
//*****************************************************************************
{
    int n = 0;
    for (CTrackingPoints::iterator i=begin(); i != end(); i++)
      if (i->second.isValid()) n++;

    return n;
}

//--------------------~operator == --------------------------------Feb 2005---

      bool CTrackingPoints::operator==(const CTrackingPoints &rhs) const

// Usual equality operator
//
//*****************************************************************************
{
   // Sizes much match
   if (rhs.size() != size()) return false;
   if (rhs.beginIndex() != beginIndex()) return false;

   // Now check all the points, fall out early if a failure
   for (int i=beginIndex(); i < endIndex(); i++)
     {
       if (rhs.isValid(i) != isValid(i)) return false;
       if (isValid(i))
         if (rhs.at(i) != at(i))
              return false;
     }

   return true;

}

//--------------------~operator != --------------------------------Feb 2005---

      bool CTrackingPoints::operator!=(const CTrackingPoints &rhs) const

// Usual not equal operator, defined in terms of equal
//
//*****************************************************************************
{
   return !(*this == rhs);
}

int CTrackingPoints::ReadPDLEntry(CPDLElement *pdlTPS)
{
   int frameIndex = pdlTPS->GetAttribInteger(frameIndexKey, 0);

   for (unsigned int i = 0; i < pdlTPS->GetChildElementCount(); ++i)
      {
      CPDLElement *pdlTP = pdlTPS->GetChildElement(i);
      if (pdlTP->GetElementName() != "TP")
         continue;

      CTrackPoint tp;
      int index = tp.ReadPDLEntry(pdlTP);
      (*this)[index] = tp;
      }

   return frameIndex;
}

void CTrackingPoints::AddToPDLEntry(CPDLElement &parent, int frameIndex)
{
   CPDLElement *pdlTPS = parent.MakeNewChild("TPS");

   pdlTPS->SetAttribInteger(frameIndexKey, frameIndex);

   for (int i = beginIndex(); i < endIndex(); ++i)
      {
      if (find(i) != end())
         {
         at(i).AddToPDLEntry(*pdlTPS, i);
         }
      }
}

//---------------------operator <<-----------------------------------Apr 2005---

    std::ostream &operator<<(std::ostream &theStream, const CTrackingPoints &tps)
{
   theStream << '[';
   for (int i = tps.beginIndex(); i < tps.endIndex(); i++)
     if (tps.at(i).isValid())
       {
         if ((i % 10) != 0) theStream << " ";
         theStream << i << tps.at(i);
         if ((i % 10) == 9) theStream << endl;
       }

   theStream << ']';
   return theStream;
}

//---------------------operator >>-----------------------------------Apr 2005---

    std::istream &operator>>(std::istream &theStream, CTrackingPoints &tps)

//  Usual input stream opeator
//
//******************************************************************************
{
   tps.clear();
   char p = '\0';
   theStream >> p;
   if (p != '[')
     {
        theStream.setstate(theStream.badbit);
        return theStream;
     }

   int idx;

   // Find the index
   while (theStream.good())
     {
        // Read the next bit
        // Note: Do not use peek here
        if(!(theStream >> p)) return theStream;
        if (p == ']') return theStream;
        theStream.putback(p);

        // This is not an error
        if (!(theStream >> idx)) return theStream;
        if (!(theStream >> tps[idx])) return theStream;
     }

   return theStream;
}
///////////////////////////// CTrackAttribute //////////////////////////////////


//------------------------CTrackAttribute----------------------------------------

  CTrackAttribute::CTrackAttribute(void)
  : group(GROUP_INDEX_NONE),
    smoothing(SMOOTHING_2AN),
    anchor(EANCHOR_NONE),
    anchorIndex(INVALID_TRACKING_POINT_TAG),
    alpha(0.0001),
    smoothingEnabled(false),
    trackStatus(tpsUNKNOWN)

//  Default constructor
//
//******************************************************************************
{
}
//------------------------------------------------------------------------------

int CTrackAttribute::ReadPDLEntry(CPDLElement *pdlTA)
{
   int index = pdlTA->GetAttribInteger(indexKey, 0);

   group = pdlTA->GetAttribInteger(groupKey, group);
   smoothingEnabled = pdlTA->GetAttribBool(smoothingEnabledKey,
                                           smoothingEnabled);
   smoothing = pdlTA->GetAttribInteger(smoothingKey, smoothing);
   alpha = pdlTA->GetAttribDouble(alphaKey, alpha);
   string defaultStr = AnchorAssociations.DisplayName(anchor);
   string valueStr = pdlTA->GetAttribString(anchorKey, defaultStr);
   int tmpValue;
   if(AnchorAssociations.ValueBySymbolicName(valueStr, tmpValue))
      anchor = (tpAnchor)tmpValue;
   anchorIndex = pdlTA->GetAttribInteger(anchorIndexKey, anchorIndex);

   return index;
}
//------------------------------------------------------------------------------

void CTrackAttribute::AddToPDLEntry(CPDLElement &parent, int index)
{
   CPDLElement *pdlTA = parent.MakeNewChild("TA");

   pdlTA->SetAttribInteger(indexKey, index);
   pdlTA->SetAttribInteger(groupKey, group);
   pdlTA->SetAttribBool(smoothingEnabledKey, smoothingEnabled);
   pdlTA->SetAttribInteger(smoothingKey, smoothing);
   pdlTA->SetAttribDouble(alphaKey, alpha);
   pdlTA->SetAttribString(anchorKey, AnchorAssociations.SymbolicName(anchor));
   pdlTA->SetAttribInteger(anchorIndexKey, anchorIndex);
}

//---------------------operator >>-----------------------------------Apr 2005---

  std::istream &operator>>(std::istream &theStream, CTrackAttribute &ta)

// Usual input operator
//
//******************************************************************************
{
   char p = '\0';
   double x, y;

   theStream >> p;
   if (p != '(')
     {
        return theStream;
     }

   // Do the group
   if (!(theStream >> ta.group))
     {
        return theStream;
     }

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }

   // Do the smoothingEnabled
   if (!(theStream >> ta.smoothingEnabled))
     {
        return theStream;
     }

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }


   // Do the smoothing
   if (!(theStream >> ta.smoothing))
     {
        return theStream;
     }

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }

   // Do the alpha
   if (!(theStream >> ta.alpha))
     {
        return theStream;
     }

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }

   // Do the anchor
   int ianchor;
   if (!(theStream >> ianchor))
     {
        return theStream;
     }

   ta.anchor = (tpAnchor)ianchor;

   // Do the ,
   theStream >> p;
   if (p != ',')
     {
        return theStream;
     }

   // Do the anchor
   if (!(theStream >> ta.anchorIndex))
     {
        return theStream;
     }

   // eat the final )
   theStream >> p;
   if (p != ')')
     {
        return theStream;
     }

   return theStream;

}

//---------------------operator <<-----------------------------------Apr 2005---

  std::ostream& operator<<(std::ostream &theStream, const CTrackAttribute &ta)

// Usual output operator
//
//******************************************************************************
{
   theStream << "(" << ta.group << ',' << ta.smoothingEnabled << ',' << ta.smoothing << ','
             << ta.alpha << ',' << ta.anchor << ',' << ta.anchorIndex <<  ")";

   return theStream;
}

///////////////////////////// CTrackingAttributes //////////////////////////////////


//----------------------CTrackingAttributes-------------------------------------

  CTrackingAttributes::CTrackingAttributes(void)

//  Default constructor
//
//******************************************************************************
{
}

//-------------------------------at--------------------------------------------

   CTrackAttribute &CTrackingAttributes::at(int tag)

//  Just return a reference to the tag attribute.
//
//*****************************************************************************
{
   return (*this)[tag];
}

//-------------------------------at--------------------------------------------

   const CTrackAttribute &CTrackingAttributes::at(int tag) const

//  Just return a reference to the tag attribute.
//
//*****************************************************************************
{
   for (CTrackingAttributes::const_iterator i = begin(); i !=end(); ++i)
     if (i->first == tag) return i->second;

   return begin()->second;
}

//------------------------beginIndex-------------------------------------------

   int CTrackingAttributes::beginIndex(void) const

//  Just decide where the smallest tag exists.
//
//*****************************************************************************
{
   if (size() == 0) return 0;
   return begin()->first;
}

//------------------------endIndex--------------------------------------------

   int CTrackingAttributes::endIndex(void) const

//  Just finds the index that is one past the last index of the array
//
//*****************************************************************************
{
   if (size() == 0) return 0;

   CTrackingAttributes::const_iterator i = end();
   i--;

   return i->first + 1;
}

//--------------------~operator == --------------------------------Feb 2005---

      bool CTrackingAttributes::operator==(const CTrackingAttributes &rhs) const

// Usual equality operator
//
//*****************************************************************************
{
   // Sizes much match
   if (rhs.size() != size()) return false;
   if (rhs.beginIndex() != beginIndex()) return false;

   // Now check all the points, fall out early if a failure
   for (int i=beginIndex(); i < endIndex(); i++)
     {
        if (rhs.at(i) != at(i)) return false;
     }

   return true;

}

//--------------------~operator != --------------------------------Feb 2005---

      bool CTrackingAttributes::operator!=(const CTrackingAttributes &rhs) const

// Usual not equal operator, defined in terms of equal
//
//*****************************************************************************
{
   return !(*this == rhs);
}

void CTrackingAttributes::ReadPDLEntry(CPDLElement *pdlTAS)
{
   for (unsigned int i = 0; i < pdlTAS->GetChildElementCount(); ++i)
      {
      CPDLElement *pdlTA = pdlTAS->GetChildElement(i);
      if (pdlTA->GetElementName() != "TA")
         continue;

      CTrackAttribute ta;
      int index = ta.ReadPDLEntry(pdlTA);
      (*this)[index] = ta;
      }
}

void CTrackingAttributes::AddToPDLEntry(CPDLElement &parent)
{
   CPDLElement *pdlTAS = parent.MakeNewChild("TAS");

   for (int i = beginIndex(); i < endIndex(); ++i)
      {
      if (find(i) != end())
         {
         at(i).AddToPDLEntry(*pdlTAS, i);
         }
      }
}

//---------------------operator <<-----------------------------------Apr 2005---

  std::ostream& operator<<(std::ostream &theStream, const CTrackingAttributes &tas)

// Usual output operator
//
//******************************************************************************
{
   theStream << '[';
   for (int i = tas.beginIndex(); i < tas.endIndex(); i++)
     if (tas.find(i) != tas.end())
       {
         if ((i % 10) != 0) theStream << " ";
         theStream << i << tas.at(i);
         if ((i % 10) == 9) theStream << endl;
       }

   theStream << ']';
   return theStream;
}

//---------------------operator >>-----------------------------------Apr 2005---

    std::istream &operator>>(std::istream &theStream, CTrackingAttributes &tas)

//  Usual input stream opeator
//
//******************************************************************************
{
   tas.clear();
   char p = '\0';
   theStream >> p;
   if (p != '[')
     {
        theStream.setstate(theStream.badbit);
        return theStream;
     }

   int idx;

   // Find the index
   while (theStream.good())
     {
        // Read the next bit
        // Note: Do not use peek here
        if(!(theStream >> p)) return theStream;
        if (p == ']') return theStream;
        theStream.putback(p);

        // This is not an error
        if (!(theStream >> idx)) return theStream;
        if (!(theStream >> tas[idx])) return theStream;
     }

   return theStream;
}

///////////////////////////// CTrackAttribute //////////////////////////////////

//--------------------~operator == --------------------------------Feb 2005---

      bool CTrackAttribute::operator==(const CTrackAttribute &rhs) const

// Usual equality operator
//
//*****************************************************************************
{
   return   (rhs.group == group) &&
            (rhs.smoothingEnabled == smoothingEnabled) &&
            (rhs.smoothing == smoothing) &&
            (rhs.alpha == alpha) &&
            (rhs.anchor == anchor) &&
            (rhs.anchorIndex == anchorIndex);
}

//--------------------~operator != --------------------------------Feb 2005---

      bool CTrackAttribute::operator!=(const CTrackAttribute &rhs) const

// Usual not equal operator, defined in terms of equal
//
//*****************************************************************************
{
   return !(*this == rhs);
}

///////////////////////////// CTrackingArray //////////////////////////////////

//------------------------CTrackingArray----------------------------------------

   CTrackingArray::CTrackingArray(void)
{
   useBoundingBox = false;
   boundingBox.left = 0;
   boundingBox.right = 0;
   boundingBox.top = 0;
   boundingBox.bottom = 0;
   trackBoxExtentX = -1;
   trackBoxExtentY = -1;
   searchBoxExtentLeft = -1;
   searchBoxExtentRight = -1;
   searchBoxExtentUp = -1;
   searchBoxExtentDown = -1;
   color = ETPRed;
}

   CTrackingArray::~CTrackingArray(void)
{
}

//------------------------isValid----------------------------------------------

     bool CTrackingArray::isValid(int tag) const

//  Decide if the tag represents an actual point
//
//*****************************************************************************
{
//   return (find(tag) != end());
 //WTF???  return (find(tag)->first != end()->first);
    return (count(tag) > 0);
}

//------------------------beginIndex-------------------------------------------

   int CTrackingArray::beginIndex(void) const

//  Just decide where the smallest tag exists, or zero if none
//
//*****************************************************************************
{
   if (size() == 0) return 0;
   return begin()->first;
}

//--------------------------endIndex-------------------------------------------

   int CTrackingArray::endIndex(void) const

//  Just decide the last index of the array
//
//*****************************************************************************
{
   if (size() == 0) return 0;

   CTrackingArray::const_iterator i = end();
   i--;
   return i->first + 1;
}

//------------------------eraseAllBut------------------------------------------

   void CTrackingArray::eraseAllBut(int tag)

//  This clears everything but the tag points
//
//*****************************************************************************
{
   CTrackingPoints tpSave = (*this)[tag];
   CTrackingAttributes taSave = TrackingAttributes;
   clear();
   (*this)[tag] = tpSave;
   TrackingAttributes = taSave;
}

//-------------------------------at--------------------------------------------

   CTrackingPoints &CTrackingArray::at(int tag)

//  Just return a reference to the point in the array at tag
//
//*****************************************************************************
{
   return (*this)[tag];
}

//-------------------------------at--------------------------------------------

   const CTrackingPoints &CTrackingArray::at(int tag) const

//  Just return a reference to the point in the array at tag
//
//*****************************************************************************
{
#if 0
   for (CTrackingArray::const_iterator i = begin(); i !=end(); ++i)
     if (i->first == tag) return i->second;

   return begin()->second;
#else
   CTrackingArray::const_iterator i = find(tag);
   if (i == end())
      return begin()->second;

   return i->second;
#endif
}


//------------------------eraseRangeSlice---------------------------------------

     void CTrackingArray::eraseRangeSlice(int beg, int end)

//  This deletes a range of frames in a tracking array
//
//*****************************************************************************
{
   bool changed = false;
   bool loppingExtremity = (beg == beginIndex() || end == endIndex() || end == ALL_TO_END);

   for (int i=std::max(beg, beginIndex()); i<std::min(end, endIndex());i++)
   {
      erase(i);
      changed = true;
   }

   // UPDATE: DON'T BUMP CHANGE STAMP if we are erasing frames on one
   // end or the other of the range - presumably the remaining frames are
   // still valid!!
   if (changed && !loppingExtremity)
   {
      setColor(ETPRed);  // invalid
   }
}

//--------------------------pruneEmpty-----------------------------------------

     void CTrackingArray::pruneEmpty(void)

// This gets rid of all the references to tags with no entries
// Note: if a point does not exist, nothing is returned
//
//*****************************************************************************
{
   bool changed = false;

   if (!empty())
   {
      for (int i=beginIndex(); i < endIndex(); i++)
      {
         if (at(i).validSize() == 0)
         {
            erase(i);
            changed = true;
         }
      }
   }

   // Hmmm presumably tis operation should not change whether or not
   // the tracking data is valid... pprobably shouldn't do this... QQQ
   if (changed)
   {
      //setColor(ETPRed);
   }
}

//--------------------------pruneStart-----------------------------------------

     void CTrackingArray::pruneStart(void)

// This gets rid of all the references to tags with no entries
// at the beginning of the array.
//
//*****************************************************************************
{
   bool changed = false;

   if (!empty())
   {
      for (int i=beginIndex(); i < endIndex(); i++)
      {
         if (at(i).validSize() == 0)
         {
            erase(i);
            changed = true;
         }
         else
         {
            // Found the first non-empty one
            break;
         }
      }
   }

   // Hmmm presumably tis operation should not change whether or not
   // the tracking data is valid... pprobably shouldn't do this... QQQ
   if (changed)
   {
       //setColor(ETPRed);
   }
}

//--------------------------TSlice---------------------------------------------

     const CTrackingPoints CTrackingArray::TSlice(int tag)

// This returns the temporal slice through the array
// Note: if a point does not exist, nothing is returned
//
//*****************************************************************************
{
   CTrackingPoints Result;
   for (int i = beginIndex(); i != endIndex(); i++)
     if (at(i).isValid(tag))
        Result[i] = (*this)[i][tag];

   return Result;
}


//-------------------------IsTSliceValid--------------------------------------

     bool CTrackingArray::IsTSliceValid(int tag, int beg, int end)

// This returns true if every tracking point tag between beg and end is valid
// Never valid if end points are not valid
//
//*****************************************************************************
{
   if (end <= beg) return false;
   if (beg < 0) return false;
   
   for (int i = beg; i < end; i++)
     if (!at(i).isValid(tag)) return false;

   return true;
}

//--------------------setSmoothing----------------------------------Apr 2005---

  void CTrackingArray::setSmoothing(int tag, int newSmooth)

// This sets the smoothing parameter for all the track points with the given
// tag tag to newSmooth.
//
//*****************************************************************************
{
    TrackingAttributes[tag].smoothing = newSmooth;
}

//--------------------getSmoothing----------------------------------Apr 2005---

  int CTrackingArray::getSmoothing(int tag)

// Return the smoothing value of the tab.
// If there is no value, the return is  SMOOTHING_INVALID
// tag is the tag to return
//
//*****************************************************************************
{
   if (!isTemporalTagValid(tag)) return SMOOTHING_INVALID;
   return TrackingAttributes[tag].smoothing;
}

//-------------------setSmoothingEnabled----------------------------Apr 2005---

  void CTrackingArray::setSmoothingEnabled(int tag, bool newEnabled)

// This sets the smoothing parameter for all the track points with the given
// tag tag to newSmooth.
//
//*****************************************************************************
{
    TrackingAttributes[tag].smoothingEnabled = newEnabled;
}

//------------------getSmoothingEnabled-----------------------------Apr 2005---

  bool CTrackingArray::getSmoothingEnabled(int tag)

// Return the smoothing value fo the tag
// If there is no value, the return is false
// tag is the tag to look up
//
//*****************************************************************************
{
   if (!isTemporalTagValid(tag)) return false;
   return TrackingAttributes[tag].smoothingEnabled;
}

//-------------------setSmoothingAlpha------------------------------Apr 2005---

  void CTrackingArray::setSmoothingAlpha(int tag, double newAlpha)

// This sets the alpha smoothing parameter for all the track points with the given
// tag tag to newSmooth.
//
//*****************************************************************************
{
   TrackingAttributes[tag].alpha = newAlpha;
}

//-------------------getSmoothingAlpha------------------------------Apr 2005---

  double CTrackingArray::getSmoothingAlpha(int tag)

// Return the smoothing alpha associated with the tag
// If there is no value, the return is 0.0001
// tag is the tag to look up.
//
//*****************************************************************************
{
   if (!isTemporalTagValid(tag)) return 0.0001;
   return TrackingAttributes[tag].alpha;
}

//--------------------------getAnchor-------------------------------Apr 2005---

  tpAnchor CTrackingArray::getAnchor(int tag)

// Return the smoothing value of the tab.
// If there is no value, the return is  SMOOTHING_INVALID
// tag is the tag to return
//
//*****************************************************************************
{
   if (!isTemporalTagValid(tag)) return EANCHOR_NONE;
   return TrackingAttributes[tag].anchor;
}

//--------------------------setAnchor-------------------------------Apr 2005---

  void CTrackingArray::setAnchor(int tag, tpAnchor newAnchor)

// This sets the anchor for the anchor value with tag tag to newAnchor
//
//*****************************************************************************
{
  TrackingAttributes[tag].anchor = newAnchor;
}

//------------------------getAnchorIndex-----------------------------Apr 2005---

  int CTrackingArray::getAnchorIndex(int tag)

// Return the smoothing value of the tab.
// If there is no value, the return is INVALID_TRACKING_POINT_TAG
// tag is the point tag to return
//
//*****************************************************************************
{
   if (!isTemporalTagValid(tag)) return INVALID_TRACKING_POINT_TAG;
   return TrackingAttributes[tag].anchorIndex;
}

//------------------------setAnchorIndex-----------------------------Apr 2005---

  bool CTrackingArray::setAnchorIndex(int tag, int newAnchorIndex)

// This sets the AnchorIndex value with tag tag to newAnchorIndex
//  Return is true if the index is set, false otherwise
//
//  Note: Setting an Anchor Index may result in a recursive loop, so first
//  check this is not the case.
//
//*****************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

  // Begin to walk back, checking if we ever link back to tag
  int TestIndex = newAnchorIndex;
  while ((TestIndex != INVALID_TRACKING_POINT_TAG) &&
         ((TrackingAttributes[TestIndex].anchor == EANCHOR_HORZIONAL) ||
          (TrackingAttributes[TestIndex].anchor == EANCHOR_VERTICAL) ||
          (TrackingAttributes[TestIndex].anchor == EANCHOR_ALL)))
         {
            TestIndex = TrackingAttributes[TestIndex].anchorIndex;
            // Check to see if we have looped back
            if (TestIndex == tag) return false;
         }

  // Not a transitive loop, set it
  TrackingAttributes[tag].anchorIndex = newAnchorIndex;
  return true;
}

//-------------------------setGroup----------------------------------Apr 2006---

      void CTrackingArray::setGroup(int tag, int newGroup)

// Just sets all the group flag for tag nTag
// See getSmoothing for additional comments
//
//*****************************************************************************
{
   TrackingAttributes[tag].group = newGroup;
}

//-------------------------getGroup----------------------------------Apr 2006---

      int CTrackingArray::getGroup(int tag)

// Just gets the group flag for tag nTag
// See getSmoothing for additional comments
//
//*****************************************************************************
{
   return TrackingAttributes[tag].group;
}

//-----------------------getNewGroup---------------------------------Apr 2006---

      int CTrackingArray::getNewGroup(void)

// Just gets the group flag for tag nTag
// See getSmoothing for additional comments
//
//*****************************************************************************
{
   vector<int> G;

   for (int i = beginIndex(); i != endIndex(); i++)
     if (isValid(i))
	   {
         int ng = getGroup(i);
		 vector<int>::iterator it1 = std::find(G.begin(),G.end(),ng);
		 if (it1 == G.end()) G.push_back(ng);
	   }

   int k=1;
   while (std::find(G.begin(), G.end(), k) != G.end()) k++;

   return k;
}

//-------------------removeOrphans----------------------------------Apr 2006---

   bool CTrackingArray::removeOrphans(void)

//  An orphan is a point with an anchor type that points to a non existing tag
//  Just reset any orphans to EANCHOR_NONE
//
//  The logic of the program should restrict orphans, but who knows
//  Return is true if an orphan has been removed.
//
//*****************************************************************************
{
    bool bRet = false;

    // Remove any anchors that match the tab
    for (CTrackingAttributes::iterator i = TrackingAttributes.begin(); i != TrackingAttributes.end(); i++)
     {
        switch ((i->second).anchor)
          {
             case EANCHOR_HORZIONAL:
             case EANCHOR_VERTICAL:
             case EANCHOR_ALL:
               if (!isTemporalTagValid((i->second).anchorIndex))
                 {
                    (i->second).anchor = EANCHOR_NONE;
                    (i->second).anchorIndex = INVALID_TRACKING_POINT_TAG;
                    bRet = true;
                 }
               break;

             // This cleanup is not necessary but I'm compulsive
             default:
               (i->second).anchorIndex = INVALID_TRACKING_POINT_TAG;
               break;
          }
     }

    return bRet;
}

//--------------------------collapse--------------------------------------------

   int CTrackingArray::collapse(void)

//  Collapse is a function that removes all invalid tracking points.
//  Collapse will only work if there are the name number of valid points
//  in each frame and the tags are the same.
//
//  Return is new size or -1 if it could not collapse the array
//
//*****************************************************************************
{
   bool changed = false;

   // Get rid of the empty tags
   pruneEmpty();
   if (size() == 0)
      return 0;   // Nothing to do

   // See if all the points are valid
   CTrackingPoints &TP = at(beginIndex());
   int k = 0;

   for (int iTag = TP.beginIndex(); iTag < TP.endIndex(); iTag++)
   {
      if (TP.isValid(iTag))
      {
         if (iTag != k)
         {
            move(iTag, k);
            changed = true;
         }
         k++;
      }
   }

   if (changed)
   {
      //setColor(ETPRed);
   }

   return TP.size();
}

//--------------------~operator == --------------------------------Feb 2005---

      bool CTrackingArray::operator==(const CTrackingArray &rhs) const

// Usual equality operator
//
//*****************************************************************************
{

   // Sizes much match
   if (rhs.size() != size()) return false;
   if (rhs.beginIndex() != beginIndex()) return false;

   // Now check all the points, fall out early if a failure
   for (int i=beginIndex(); i < endIndex(); i++)
     {
       if (rhs.at(i) != at(i)) return false;
     }

   if (rhs.TrackingAttributes != TrackingAttributes) return false;

   return true;

}

//--------------------~operator != --------------------------------Feb 2005---

      bool CTrackingArray::operator!=(const CTrackingArray &rhs) const

// Usual not equal operator, defined in terms of equal
//
//*****************************************************************************
{
   return !(*this == rhs);
}

void CTrackingArray::ReadPDLEntry(CPDLElement *pdlTrackingArray)
{
   for (unsigned int i = 0; i < pdlTrackingArray->GetChildElementCount(); ++i)
      {
      CPDLElement *pdlElement = pdlTrackingArray->GetChildElement(i);
      if (pdlElement->GetElementName() == "TPS")
         {
         // Read Tracking Points elements
         CTrackingPoints tps;
         int frameIndex = tps.ReadPDLEntry(pdlElement);
         (*this)[frameIndex] = tps;
         }
      else if (pdlElement->GetElementName() == "TAS")
         {
         // Read Tracking Attributes elements
         TrackingAttributes.ReadPDLEntry(pdlElement);
         }
      else if (pdlElement->GetElementName() == "BoundingBox")
         {
         // Read bounding box elements
         boundingBox = pdlElement->GetAttribRect("BoundingBox", boundingBox);
         }
      else if (pdlElement->GetElementName() == "TrackBox")
         {
         // Read track box elements
         RECT tmpRect;
         tmpRect.left =  tmpRect.right =  tmpRect.top =  tmpRect.bottom = 0;
         tmpRect = pdlElement->GetAttribRect("TrackBox", tmpRect);

         // Track box is symmetric, so we only need to look at one pt
         trackBoxExtentX = -tmpRect.left;
         trackBoxExtentY = -tmpRect.top;
         }
      else if (pdlElement->GetElementName() == "SearchBox")
         {
         // Read search box elements
         RECT tmpRect;
         tmpRect.left =  tmpRect.right =  tmpRect.top =  tmpRect.bottom = 0;
         tmpRect = pdlElement->GetAttribRect("SearchBox", tmpRect);

         // Search has no intrinsic symmetry
         searchBoxExtentLeft = -tmpRect.left;
         searchBoxExtentRight = tmpRect.right-1;
         searchBoxExtentUp = -tmpRect.top;
         searchBoxExtentDown = tmpRect.bottom-1;
         }
      }
}

void CTrackingArray::AddToPDLEntry(CPDLElement &parent)
{
   CPDLElement *pdlTrackingArray = parent.MakeNewChild("TrackingArray");

   // Create elements for tracking points
   for (int i = beginIndex(); i < endIndex(); ++i)
      {
      if (isValid(i))
         {
         at(i).AddToPDLEntry(*pdlTrackingArray, i);
         }
      }

   // Create elements for tracking attributes
   TrackingAttributes.AddToPDLEntry(*pdlTrackingArray);

   // Create elements for bounding box
   CPDLElement *pdlBB = pdlTrackingArray->MakeNewChild("BoundingBox");
   pdlBB->SetAttribRect("BoundingBox", boundingBox);

   // Create elements for track box
   RECT tmpRect;
   tmpRect.left = -trackBoxExtentX;
   tmpRect.right = trackBoxExtentX+1;
   tmpRect.top = -trackBoxExtentY;
   tmpRect.bottom = trackBoxExtentY+1;
   CPDLElement *pdlTB = pdlTrackingArray->MakeNewChild("TrackBox");
   pdlTB->SetAttribRect("TrackBox", tmpRect);

   // Create elements for search box
   tmpRect.left = -searchBoxExtentLeft;
   tmpRect.right = searchBoxExtentRight+1;
   tmpRect.top = -searchBoxExtentUp;
   tmpRect.bottom = searchBoxExtentDown+1;
   CPDLElement *pdlSB = pdlTrackingArray->MakeNewChild("SearchBox");
   pdlSB->SetAttribRect("SearchBox", tmpRect);

}

//--------------------~operator <<-----------------------------------Apr 2005---

    std::ostream &operator<<(std::ostream &theStream, const CTrackingArray &ta)
{
   theStream << '{';
   for (int i = ta.beginIndex(); i < ta.endIndex(); i++)
     if (ta.isValid(i))
       {
         theStream << i << ta.at(i) << endl;
       }

   theStream << '}' << endl;
   theStream << ta.TrackingAttributes << endl;

   // Now write the bounding box
   theStream << ta.getUseBoundingBox() << ' ';
   RECT bb = ta.getBoundingBox();
   theStream << bb.left << ' ' << bb.right << ' ' << bb.top << ' ' << bb.bottom << endl;
   theStream << ((ta.getColor() == CTrackingArray::ETPBlue)? 'B' :
                 ((ta.getColor() == CTrackingArray::ETPGreen)? 'G' : 'R')) << endl;

   return theStream;
}

//--------------------~operator >>-----------------------------------Apr 2006---

    std::istream &operator>>(std::istream &theStream, CTrackingArray &ta)
{
   ta.setColor(CTrackingArray::ETPRed);

   ta.clear();
   char p = '\0';
   theStream >> p;
   if (p != '{')
     {
        theStream.setstate(theStream.badbit);
        return theStream;
     }

   int idx;

   // Find the index
   while (theStream.good())
     {
        // Read the next bit
        if (!(theStream >> p)) return theStream;
       if (p == '}') break;

        theStream.putback(p);

        // This is not an error
        if (!(theStream >> idx)) return theStream;
        if (!(theStream >> ta[idx])) return theStream;
     }

   if (!(theStream >> ta.TrackingAttributes)) return theStream;

   // Finish reading the bounding box information
   bool bUsebb;
   if (!(theStream >> bUsebb)) return theStream;
   ta.setUseBoundingBox(bUsebb);

   RECT bb;
   if (!(theStream >> bb.left)) return theStream;
   if (!(theStream >> bb.right)) return theStream;
   if (!(theStream >> bb.top)) return theStream;
   if (!(theStream >> bb.bottom)) return theStream;
   ta.setBoundingBox(bb);

   char colorChar;
   ta.setColor(CTrackingArray::ETPRed);
   if (!(theStream >> colorChar)) return theStream;
   if (colorChar == 'B') ta.setColor(CTrackingArray::ETPBlue);
   if (colorChar == 'G') ta.setColor(CTrackingArray::ETPGreen);

   return theStream;
}

//----------------------------clear----------------------------------Apr 2006---

     void CTrackingArray::clear(void)

//  Just clear the data from the two arrayes
//
//******************************************************************************
{
    TrackingArrayType::clear();
    TrackingAttributes.clear();

    // Tracking no longer valid
    color = ETPRed;
}

//---------------------eraseTemporalTag-----------------------------Apr 2006---

     void CTrackingArray::eraseTemporalTag(int tag)

//  Delete the tag, this involves in deleting the tag from the two arrays
//  and all references
//
//******************************************************************************
{
    // Remove any anchors that match the tab
    for (CTrackingAttributes::iterator i = TrackingAttributes.begin(); i != TrackingAttributes.end(); i++)
      if ((i->second).anchorIndex == tag) (i->second).anchor = EANCHOR_NONE;

    TrackingAttributes.erase(tag);

    //  Erase all the tag points
    for (int i = beginIndex(); i < endIndex(); i++)
      at(i).erase(tag);

    // Tracking no longer valid
    color = ETPRed;
}

//--------------------isTemporalTagValid-----------------------------Apr 2006---

    bool CTrackingArray::isTemporalTagValid(int tag) const

//  This is a shorthand for finding if the tag of a point exists in the
//  tracking array.
//
//******************************************************************************
{
   for (int idx = beginIndex(); idx < endIndex(); idx++)
      if (at(idx).isValid(tag)) return true;

   return false;
}

//-------------------getSelectedTemporalTags-------------------------Apr 2006---

    IntegerList CTrackingArray::getSelectedTemporalTags(int tag) const

//  Find all the selected points on the frame tag
//
//******************************************************************************
{
   IntegerList ilRet;
   if (empty()) return ilRet;
   if (!isValid(tag)) return ilRet;

   for (int idx = at(tag).beginIndex(); idx < at(tag).endIndex(); idx++)
      if (at(tag).at(idx).selected) ilRet.push_back(idx);

   return ilRet;
}

//-------------------------setSelected------------------------------Apr 2006---

     void CTrackingArray::setSelected(int tag, bool sel)

//  Set the points defined by the tag to the sel
//
//******************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

   for (int idx = beginIndex(); idx < endIndex(); idx++)
     if (at(idx).isValid(tag)) at(idx)[tag].selected = sel;

}

//--------------------------move-------------------------------------Apr 2006---

     void CTrackingArray::move(int TagFrom, int TagTo)

//  This takes all the points indexed by TagFrom and moves them to TagTo
//  The Attributes as well as the Anchors are properly updated
//   NOTE: it is up to call caller to make sure the TagFRom is valid
//
//******************************************************************************
{
    if (TagFrom == TagTo) return;   // Nothing to do

    setColor(ETPRed);   // make invalid??

    // Remove TagTo
    eraseTemporalTag(TagTo);

    // Move all the anchors
    for (CTrackingAttributes::iterator i = TrackingAttributes.begin(); i != TrackingAttributes.end(); i++)
      if ((i->second).anchorIndex == TagFrom) (i->second).anchorIndex = TagTo;

    // Move the points.  Note because we erased the TagTo, all the TagTo points
    // are invalid.
    for (int i = beginIndex(); i < endIndex(); i++)
      if (at(i)[TagFrom].isValid())
         at(i)[TagTo] = at(i)[TagFrom];

    // Move the attribute
    TrackingAttributes[TagTo] = TrackingAttributes[TagFrom];

    // Now delete the the TagFrom
    eraseTemporalTag(TagFrom);

    // Tracking no longer valid
    // QQQ shouldn't we clear the "may be valid" flag also?
    color = ETPRed;
}

//----------moveTrackingPointsToNewBeginIndex----------------------Jul 2010---

     void CTrackingArray::moveTrackingPointsToNewBeginIndex(int TagTo)

//  This erases all points except at the beginIndex, then moves those
//  points to a new beginIndex (so all tracking data is lost except the
//  intial positions
//
//******************************************************************************
{
   int TagFrom = beginIndex();

   // Save points at old beginIndex and save the attributes
   CTrackingPoints tpSave = (*this)[TagFrom];
   CTrackingAttributes taSave = TrackingAttributes;

   // Reset everything
   clear();

   // Restore the points to
   (*this)[TagTo] = tpSave;    // Moved!
   TrackingAttributes = taSave;
}

//-------------------CreateAnchorTree--------------------------------Apr 2006---

    IntegerList CTrackingArray::CreateAnchorTree(void)

//  Highly specialized routine.  It takes the tracking points and lists them
//  in an order that the tracking algorithm can use to find the correct anchor
//  points
//
//******************************************************************************
{
    collapse();

    // V will be the list of tracking tags that have yet to be sorted
    IntegerList V;
    for (unsigned int i=0; i < TrackingAttributes.size(); i++)
      V.push_back(i);

    // R will be the anchor tree
    IntegerList RetVec;
    IntegerList RemainingVec;

    // Now find the lowest anchors
    for (IntegerList::iterator i=V.begin(); i != V.end(); i++)
       if (!((TrackingAttributes[*i].anchor == EANCHOR_HORZIONAL) ||
             (TrackingAttributes[*i].anchor == EANCHOR_VERTICAL) ||
             (TrackingAttributes[*i].anchor == EANCHOR_ALL)))
             {
               RetVec.push_back(*i);
             }
           else
             {
               RemainingVec.push_back(*i);
             }

    // Loop over the other anchors
    V.swap(RemainingVec);
    RemainingVec.clear();
    while (V.size() > 0)
      {
         // See if the anchorindex is in the RetVec
		 for (unsigned int i=0; i < V.size(); i++)
		 {
		   int index = TrackingAttributes[V[i]].anchorIndex;
		   IntegerList::iterator it1 = std::find(RetVec.begin(),RetVec.end(),index);
		   if (it1 != RetVec.end())
			 RetVec.push_back(V[i]);
		   else
			 RemainingVec.push_back(V[i]);
		 }
         V.swap(RemainingVec);
		 RemainingVec.clear();
      }

    return RetVec;
}

//-----------------numberTrackPointsPerFrame-------------------------Jul 2006---

     int CTrackingArray::numberTrackPointsPerFrame(void)

// This returns the number of tracking points defined on the first frame
//
//******************************************************************************
{
   int retVal;
   pruneStart();
   if (empty())
   {
      retVal = 0;
   }
   else
   {
      retVal = at(beginIndex()).validSize();
   }

   return retVal;
}


//---------------------getTrackedStatus------------------------------Oct 2006---

     tpTrackedStatus CTrackingArray::getTrackedStatus(int tag)

// This returns the tracking status of the tag.  If the status does not exist
// a tpsUNDEFINED is returned.
//
//******************************************************************************
{
   // Check if in range
   if ((tag < TrackingAttributes.beginIndex()) || (tag >= TrackingAttributes.endIndex()))
      return tpsUNDEFINED;

   return TrackingAttributes.at(tag).trackStatus;

}

//---------------------setTrackedStatus------------------------------Oct 2006---

     void CTrackingArray::setTrackedStatus(int tag, tpTrackedStatus ts)

// This sets the tracking status of tag to ts
//
//******************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

   // Check if in range
   if ((tag < TrackingAttributes.beginIndex()) || (tag >= TrackingAttributes.endIndex()))
      return;

   TrackingAttributes[tag].trackStatus = ts;

}

//--------------------setAllTrackedStatus----------------------------Oct 2006---

     void CTrackingArray::setAllTrackedStatus(tpTrackedStatus ts)

// This sets every tracked status to ts
//
//******************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

   for (int i = TrackingAttributes.beginIndex(); i < TrackingAttributes.endIndex(); i++)
      TrackingAttributes[i].trackStatus = ts;
}

//----------------------setColor-------------------------------------Sep 2008---

void CTrackingArray::setColor(ETPColor newColor)

//******************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

   color = newColor;
}

//----------------------getColor-------------------------------------Sep 2008---

CTrackingArray::ETPColor CTrackingArray::getColor(void) const

//******************************************************************************
{
   return color;
}

//--------------------setExtents-------------------------------------Apr 2009---

void CTrackingArray::setExtents(
                     int newTrackBoxExtentX, int newTrackBoxExtentY,
                     int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
                     int newSearchBoxExtentUp, int newSearchBoxExtentDown)

//******************************************************************************
{
   // ***************** HACK ALERT *******************************************
   // Bump changed stamp when attributes change?? I think it's OK not to.
   // ***************** HACK ALERT *******************************************

   trackBoxExtentX      = newTrackBoxExtentX;
   trackBoxExtentY      = newTrackBoxExtentY;
   searchBoxExtentLeft  = newSearchBoxExtentLeft;
   searchBoxExtentRight = newSearchBoxExtentRight;
   searchBoxExtentUp    = newSearchBoxExtentUp;
   searchBoxExtentDown  = newSearchBoxExtentDown;
}

//--------------------getExtents-------------------------------------Apr 2009---

void CTrackingArray::getExtents(
                     int &refTrackBoxExtentX, int &refTrackBoxExtentY,
                     int &refSearchBoxExtentLeft, int &refSearchBoxExtentRight,
                     int &refSearchBoxExtentUp, int &refSearchBoxExtentDown)

//******************************************************************************
{
   refTrackBoxExtentX      = trackBoxExtentX;
   refTrackBoxExtentY      = trackBoxExtentY;
   refSearchBoxExtentLeft  = searchBoxExtentLeft;
   refSearchBoxExtentRight = searchBoxExtentRight;
   refSearchBoxExtentUp    = searchBoxExtentUp;
   refSearchBoxExtentDown  = searchBoxExtentDown;
}

//-------------------areExtentsDifferent-----------------------------Apr 2009---

bool CTrackingArray::areExtentsDifferent(
                     int newTrackBoxExtentX, int newTrackBoxExtentY,
                     int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
                     int newSearchBoxExtentUp, int newSearchBoxExtentDown)

//******************************************************************************
{
   return (newTrackBoxExtentX      != trackBoxExtentX      ||
           newTrackBoxExtentY      != trackBoxExtentY      ||
           newSearchBoxExtentLeft  != searchBoxExtentLeft  ||
           newSearchBoxExtentRight != searchBoxExtentRight ||
           newSearchBoxExtentUp    != searchBoxExtentUp    ||
           newSearchBoxExtentDown  != searchBoxExtentDown);
}
//------------------------------------------------------------------------------




