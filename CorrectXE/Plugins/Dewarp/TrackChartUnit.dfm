object TrackChartForm: TTrackChartForm
  Left = 895
  Top = 150
  Width = 669
  Height = 313
  Caption = 'Tracking Points'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object VTrackGraph: TChart
    Left = 329
    Top = 2
    Width = 320
    Height = 273
    Hint = 
      'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
      'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
      'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
      'downwards to zoom in, upwards to zoom out'
    BackImageInside = True
    BackWall.Brush.Color = clWhite
    BackWall.Color = clBtnFace
    MarginRight = 8
    Title.AdjustFrame = False
    Title.Brush.Color = clBlue
    Title.Color = clBtnFace
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clMenuText
    Title.Font.Height = -13
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Frame.Width = 2
    Title.Text.Strings = (
      '       Vertical Movement')
    BackColor = clBtnFace
    BottomAxis.Axis.Visible = False
    BottomAxis.Grid.Visible = False
    BottomAxis.Increment = 5
    BottomAxis.LabelsSeparation = 20
    BottomAxis.TickLength = 6
    BottomAxis.Title.Caption = 'Timecode'
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Visible = False
    LeftAxis.Grid.Visible = False
    LeftAxis.Maximum = 300
    LeftAxis.Title.Caption = 'Pixels'
    Legend.Alignment = laTop
    Legend.Color = clBtnFace
    Legend.Font.Charset = DEFAULT_CHARSET
    Legend.Font.Color = clWhite
    Legend.Font.Height = -11
    Legend.Font.Name = 'Arial'
    Legend.Font.Style = []
    Legend.HorizMargin = 10
    Legend.ShadowColor = clGray
    Legend.Visible = False
    View3D = False
    BevelOuter = bvNone
    TabOrder = 0
    object Series20: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      Title = 'Tracking Points'
      Pointer.Brush.Color = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series21: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      Title = 'Smoothed Points'
      Pointer.Brush.Color = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object HTrackGraph: TChart
    Left = 9
    Top = 2
    Width = 320
    Height = 273
    Hint = 
      'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
      'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
      'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
      'downwards to zoom in, upwards to zoom out'
    BackImageInside = True
    BackWall.Brush.Color = clWhite
    BackWall.Color = clBtnFace
    MarginRight = 8
    Title.AdjustFrame = False
    Title.Brush.Color = clBlue
    Title.Color = clBtnFace
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clMenuText
    Title.Font.Height = -13
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Frame.Width = 2
    Title.Text.Strings = (
      '     Horizontal Movement')
    BackColor = clBtnFace
    BottomAxis.Axis.Visible = False
    BottomAxis.Grid.Visible = False
    BottomAxis.Increment = 5
    BottomAxis.LabelsSeparation = 20
    BottomAxis.TickLength = 6
    BottomAxis.Title.Caption = 'Timecode'
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Visible = False
    LeftAxis.Grid.Visible = False
    LeftAxis.Maximum = 300
    LeftAxis.Title.Caption = 'Pixels'
    Legend.Alignment = laTop
    Legend.Color = clBtnFace
    Legend.Font.Charset = DEFAULT_CHARSET
    Legend.Font.Color = clWhite
    Legend.Font.Height = -11
    Legend.Font.Name = 'Arial'
    Legend.Font.Style = []
    Legend.HorizMargin = 10
    Legend.ShadowColor = clGray
    Legend.Visible = False
    View3D = False
    BevelOuter = bvNone
    TabOrder = 1
    object LineSeries1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      Title = 'Tracking Points'
      Pointer.Brush.Color = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object LineSeries2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      Title = 'Smoothed Points'
      Pointer.Brush.Color = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
end
