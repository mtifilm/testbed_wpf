//---------------------------------------------------------------------------

#ifndef TrackPointsH
#define TrackPointsH

//  A TrackPoint is DPOINT with a frame number and an identifier
//
//  This is a simple base class for a collection of (x,y) points.  The points
// access is not very fast but is designed for a few hundred points per frame

#include "IniFile.h"
#include "SafeClasses.h"
#include <valarray>
#include <map>

class CPDLElement;

typedef enum _tpMovement
{
   tpmNONE,
   tpmHORZIONAL,
   tpmVERTICAL,
   tpmALL
} tpMovement;

typedef enum _tpAnchor
{
   EANCHOR_NONE,
   EANCHOR_HORZIONAL,
   EANCHOR_VERTICAL,
   EANCHOR_ALL,
   EFIXED_HORIZIONAL,
   EFIXED_VERTICAL,
   EFIXED_ALL
} tpAnchor;

typedef enum _tpTrackedStatus
{
   tpsUNDEFINED,
   tpsUNKNOWN,
   tpsVALID,
   tpsNOTTRACKED,
   tpsMOVED
} tpTrackedStatus;

// Special integers for smoothing
#define SMOOTHING_2AN 0             // Smooth by second order smoother
#define SMOOTHING_FLAT -1           // Keep the first value
#define SMOOTHING_NONE -2           // Keep the point value
#define SMOOTHING_INVALID -3        // Not a valid value

#define GROUP_INDEX_NONE 0
#define INVALID_TRACKING_POINT_TAG -1

// Erase information
#define ALL_TO_END     0x7FFFFFFF
#define ALL_FROM_START 0

// Tracking information for temporal information
// Pretend we are a structure
class CTrackAttribute
{
   public:

   CTrackAttribute(void);
   int  group;

   bool smoothingEnabled;           // True to use this smoothing value
   int  smoothing;                  // Smoothing type
   double alpha;                    // Alpha if Smoothing Type is SMOOTHING_2AN

   tpAnchor anchor;                 // Type of anchor to use
   int  anchorIndex;                // Index of anchor point
   tpTrackedStatus trackStatus;     // Is the tracking data valid

   bool operator==(const CTrackAttribute &rhs) const;
   bool operator!=(const CTrackAttribute &rhs) const;

   int ReadPDLEntry(CPDLElement *pdlTA);
   void AddToPDLEntry(CPDLElement &parent, int index);

   friend std::ostream& operator<<(std::ostream &theStream, const CTrackAttribute &ta);
   friend std::istream& operator>>(std::istream &theStream, CTrackAttribute &ta);

};

// List of the point attributes, this do not change over time
class CTrackingAttributes : public std::map <int, CTrackAttribute>
{
   public:
     CTrackingAttributes(void);
     int beginIndex(void) const;
     int endIndex(void) const;
     CTrackAttribute &at(int tag);
     const CTrackAttribute &at(int tag) const;

     bool operator==(const CTrackingAttributes &rhs) const;
     bool operator!=(const CTrackingAttributes &rhs) const;

     void ReadPDLEntry(CPDLElement *pdlTA);
     void AddToPDLEntry(CPDLElement &parent);
     
     friend std::ostream& operator<<(std::ostream &theStream, const CTrackingAttributes &tas);
     friend std::istream& operator>>(std::istream &theStream, CTrackingAttributes &tas);

};

struct DSPOINT : public DPOINT
{
   bool selected;
};


class CTrackPoint : public DSPOINT
{
    public:
      // Constructors
      CTrackPoint(void);
      CTrackPoint(double dX, double dY, bool sel=false);
      CTrackPoint(CTrackPoint const &tp);
      CTrackPoint(const DPOINT &dp);
      CTrackPoint(const DSPOINT &dp);
      ~CTrackPoint(void);

      // Operators
      CTrackPoint &operator=(const CTrackPoint &tp);
      CTrackPoint const &operator=(const POINT &iP);
      CTrackPoint const &operator=(const DPOINT &dP);
      CTrackPoint const &operator=(const DSPOINT &dP);
      bool operator==(const CTrackPoint &rhs) const;
      bool operator!=(const CTrackPoint &rhs) const;

      int ReadPDLEntry(CPDLElement *pdlTP);
      void AddToPDLEntry(CPDLElement &parent, int index);

      friend std::ostream& operator<<(std::ostream &theStream, const CTrackPoint &tp);
      friend std::istream& operator>>(std::istream &theStream, CTrackPoint &tp);

      operator POINT(void) const;


      // Support functions
      void assign(const CTrackPoint &tp);
      void assign(const DPOINT &dp);
      void assign(const DSPOINT &dsp);
      bool isValid(void) const;

    private:
      void _reset(void);
      bool _valid;
};

typedef std::pair<int, CTrackPoint> CTrackingPointsEntry;

// A List of Track points
class CTrackingPoints : public std::map <int, CTrackPoint>
{
   public:
     int beginIndex(void) const;
     int endIndex(void) const;
     int add(double x, double y, bool sel=false);
     int add(CTrackPoint &dp);
     unsigned int validSize(void);
     CTrackPoint &at(int tag);
     const CTrackPoint &at(int tag) const;

     int collapse(void);

     bool operator==(const CTrackingPoints &rhs) const;
     bool operator!=(const CTrackingPoints &rhs) const;

     int ReadPDLEntry(CPDLElement *pdlTP);
     void AddToPDLEntry(CPDLElement &parent, int frameIndex);
     
     friend std::ostream& operator<<(std::ostream &theStream, const CTrackingPoints &tps);
     friend std::istream& operator>>(std::istream &theStream, CTrackingPoints &tps);

     bool isValid(int tag) const;
};

typedef std::pair<int, CTrackingPoints> CTrackingArrayEntry;
typedef std::map <int, CTrackingPoints> TrackingArrayType;

// A Track Array is very much like a 3 dimension array of Points over the
// entire track but there is an attribute array attached for ea
class CTrackingArray : public TrackingArrayType
  {
    public:
     CTrackingArray(void);
     ~CTrackingArray(void);

     int beginIndex(void) const;
     int endIndex(void) const;
     CTrackingPoints &at(int tag);
     const CTrackingPoints &at(int tag) const;

     bool isValid(int tag) const;
     bool isTemporalTagValid(int tag) const;
     void eraseTemporalTag(int tag);

     void eraseAllBut(int tag);
     void eraseRangeSlice(int beg, int end);
     bool IsTSliceValid(int tag, int beg, int end);
     int collapse(void);
     void move(int TagFrom, int TagTo);
     void moveTrackingPointsToNewBeginIndex(int TagTo);
     void pruneEmpty(void);
     void pruneStart(void);
     bool removeOrphans(void);

     IntegerList getSelectedTemporalTags(int tag) const;

     // Deal with temporal slices
     const CTrackingPoints TSlice(int tag);

     void setSmoothingEnabled(int tag, bool newEnabled);
     bool getSmoothingEnabled(int tag);
     void setSmoothing(int tag, int newSmooth);
     int getSmoothing(int tag);
     void setSmoothingAlpha(int tag, double newAlpha);
     double getSmoothingAlpha(int tag);
     void setSelected(int tag, bool sel);

     void setAnchor(int tag, tpAnchor newAnchor);
     tpAnchor getAnchor(int tag);

     bool setAnchorIndex(int tag, int newAnchorIndex);
     int getAnchorIndex(int tag);

     void setGroup(int tag, int newGroup);
     int getGroup(int tag);
     int getNewGroup(void);

     bool operator==(const CTrackingArray &rhs) const;
     bool operator!=(const CTrackingArray &rhs) const;

     void ReadPDLEntry(CPDLElement *pdlTP);
     void AddToPDLEntry(CPDLElement &parent);

     friend std::ostream& operator<<(std::ostream &theStream, const CTrackingArray &ta);
     friend std::istream& operator>>(std::istream &theStream, CTrackingArray &ta);

     int numberTrackPointsPerFrame();

     // Bounding box
     RECT getBoundingBox(void) const {return boundingBox; }
     void setBoundingBox(const RECT &bb) {boundingBox = bb; }
     bool getUseBoundingBox(void) const {return useBoundingBox; }
     void setUseBoundingBox(const bool &ubb) {useBoundingBox = ubb; }

     // Extents of track box size & motion search
     void setExtents(int newTrackBoxExtentX, int newTrackBoxExtentY,
                     int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
                     int newSearchBoxExtentUp, int newSearchBoxExtentDown);
     void getExtents(int &refTrackBoxExtentX, int &refTrackBoxExtentY,
                     int &refSearchBoxExtentLeft, int &refSearchBoxExtentRight,
                     int &refSearchBoxExtentUp, int &refSearchBoxExtentDown);
     bool areExtentsDifferent(int newTrackBoxExtentX, int newTrackBoxExtentY,
                     int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
                     int newSearchBoxExtentUp, int newSearchBoxExtentDown);


     // Exported from base class
     void clear(void);

     using TrackingArrayType::empty;
     using TrackingArrayType::size;
     using TrackingArrayType::max_size;
     using TrackingArrayType::erase;
     using TrackingArrayType::operator[];

     // Internal algorithms for tracking points
     IntegerList CreateAnchorTree(void);

     tpTrackedStatus getTrackedStatus(int tag);
     void setTrackedStatus(int tag, tpTrackedStatus ts);
     void setAllTrackedStatus(tpTrackedStatus ts);

     // Colors hack
     enum ETPColor
     {
         ETPRed,
         ETPGreen,
         ETPBlue
     };
     void setColor(ETPColor newColor);
     ETPColor getColor(void) const;

    private:
	  CTrackingAttributes TrackingAttributes;
      RECT boundingBox;
      bool useBoundingBox;
      ETPColor color;

      int trackBoxExtentX, trackBoxExtentY;
      int searchBoxExtentLeft, searchBoxExtentRight;
      int searchBoxExtentUp, searchBoxExtentDown;
  };

extern CIniAssociations AnchorAssociations;

//---------------------------------------------------------------------------
#endif
