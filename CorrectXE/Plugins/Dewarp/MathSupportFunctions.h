//---------------------------------------------------------------------------

#ifndef MathSupportFunctionsH
#define MathSupportFunctionsH

#include <valarray.h>
#include "machine.h"
#include "tarray.h"

//---------------------------------------------------------------------------
// General mathematical functions

bool Smooth3(valarray<double> &MC, int nSFixed, int nEFixed, double Alpha);

// U Functions
//float UEuclidean(float X0, float Y0, float X1, float Y1);
//float UThinPlate(float X0, float Y0, float X1, float Y1);
//float ULogrithmic(float X0, float Y0, float X1, float Y1);
//float UIdenity(float X0, float Y0, float X1, float Y1);

int L1Norm(MTI_UINT16 *p1, MTI_UINT16 *p2, int N);
double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y);
void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow, double x, double y, int N, MTI_UINT16 *imgOut);
void CreateSubPixelMatrix2(MTI_UINT16 *pIn, int nCol, double x, double y, int N, MTI_UINT16 *pOut);
double BiLinear( double x, double y, double C00, double C10, double C01, double C11);
void MakeMonoChromeImage(MTI_UINT16 *pIn, int nCol, int nRow, MTI_UINT16 *pOut, bool RGB);
void pearsn(double x[], double y[], unsigned long n, double &r, double &prob, double &z);
RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect);
RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB, int R, int G, int B);
void EstimateMinFrom9Points(double D[9], double &x, double &y);

#endif


