//---------------------------------------------------------------------------

#ifndef DewarpCoreUnitH
#define DewarpCoreUnitH
#include "Windows.h"
//---------------------------------------------------------------------------

  void FindDewarpMotionLinCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindDewarpMotionRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindDewarpMotionRRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindDewarpMotionLinSSE4(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindDewarpMotionLinSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindDewarpMotionRLogSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindDewarpMotionRRLogSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindDewarpMotionIdenity(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

#endif

