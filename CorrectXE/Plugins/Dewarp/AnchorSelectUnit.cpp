//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PropX.h"
#include "AnchorSelectUnit.h"
#include "TrackingPointsEditor.h"
#include "DewarpGuiWin.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAnchorSelectForm *AnchorSelectForm;
//---------------------------------------------------------------------------
__fastcall TAnchorSelectForm::TAnchorSelectForm(TComponent* Owner)
        : TForm(Owner)
{

}

//---------------------------------------------------------------------------
void __fastcall TAnchorSelectForm::FormShow(TObject *Sender)
{
   SelectedTag = INVALID_TRACKING_POINT_TAG;
   SET_CBHOOK(AnchorTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
   CurrentTag = GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag;
}

//---------------------------------------------------------------------------
void __fastcall TAnchorSelectForm::FormHide(TObject *Sender)
{
   REMOVE_CBHOOK(AnchorTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
}

//---------------------------------------------------------------------------
void TAnchorSelectForm::AnchorTagChanged(void *Sender)
{
  SelectedTag = GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag;
  // See if adding it is recursive
  if (!GDewarpTool->TrackingArray.setAnchorIndex(CurrentTag, SelectedTag))
    {
       MessageDlg("Anchor point creates an infinitely recursive loop\nPlease select another point", mtError, TMsgDlgButtons() << mbOK, 0);
       return;
    }
    
  PostMessage(Handle, WM_CLOSE, 0, 0);
}
void __fastcall TAnchorSelectForm::CanClick(TObject *Sender)
{
  Close();        
}
//---------------------------------------------------------------------------
