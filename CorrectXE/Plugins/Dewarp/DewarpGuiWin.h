//---------------------------------------------------------------------------

#ifndef DewarpGuiWinH
#define DewarpGuiWinH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "VTimeCodeEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>

#include "machine.h"
#include "DewarpTool.h"
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
////#include <Chartfx3.hpp>
#include <OleCtrls.hpp>
#include "cspin.h"
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Dialogs.hpp>
#include "ExecButtonsFrameUnit.h"
#include "ToolSystemInterface.h"
#include "ExecStatusBarUnit.h"
#include "TrackEditFrameUnit.h"
#include "SpinEditFrameUnit.h"
#include <Vcl.ToolWin.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include "ColorPanel.h"
#include <System.ImageList.hpp>
#include "PresetsUnit.h"

//---------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateDewarpToolGUI(void);
extern void DestroyDewarpToolGUI(void);
extern bool ShowDewarpToolGUI(void);
extern bool HideDewarpToolGUI(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                                   bool processingRenderFlag);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void GatherGUIParameters(CDewarpParameters &toolParams);
extern void SetGUIParameters(CDewarpParameters &toolParams);
extern void ToggleEditAnchorsMode(void);
extern bool IsToolVisible(void);
extern void SetControlFocus(int controlCommand);
extern void SwapInOut(void);
extern void ToggleClearOnShotChange(void);
extern void ToggleTrackPointVisibility(void);
extern void SetTrackPointsVisibility(bool flag);
extern void ToggleProcRegionVisibility(void);
extern void ToggleProcRegionOperation(void);
extern string GetFilenameForSavingTrackingPoints(const string &suggestedFilename);
extern string GetFilenameForLoadingTrackingPoints(const string &suggestedFilename);
extern void ToggleColorPicker(void);
extern bool AreWeInAnchorSettingMode(void);
extern bool AreWeInAnchorEditingMode(void);
extern void CancelAnchorSettingMode(void);

class TDewarpForm : public TMTIForm
{
__published:	// IDE-managed Components
		TImageList *ButtonImageList;
		TCoolBar *TBDock1;
		TToolBar *TBToolbar1;
		TToolButton *PreviewFrameButton;
		TToolButton *PreviewAllButton;
		TToolButton *RenderButton;
		TToolButton *StopButton;
		TToolButton *HighLightButton;
        TToolButton *PDLCaptureTBItem;
        TBevel *BottomBevel;
        TLabel *WarpLabel;
        TComboBox *UFunctionComboBox;
        TRadioGroup *RenderRadioBox;
        TBitBtn *AdvancedButton;
        TTimer *UpdateTimer;
        TStatusBar *StatusBar;
        TImageList *BitBtnImageList;
        TOpenDialog *OpenTrackingPointsDialog;
        TSaveDialog *SaveTrackingPointsDialog;
        TTimer *DownTimer;
	TColorPanel *DewarpControlPanel;
	TColorPanel *ToolbarContainerPanel;
    TGroupBox *ResumeGroupBox;
    TSpeedButton *GotoTCButton;
    TSpeedButton *SetStartTCButton;
    VTimeCodeEdit *ResumeTCEdit;
   TGroupBox *ProcessingRegionGroupBox;
    TSpeedButton *TopUpButton;
    TSpeedButton *TopDownButton;
    TSpeedButton *BottomUpButton;
    TSpeedButton *BottomDownButton;
    TSpeedButton *LeftLeftButton;
    TSpeedButton *LeftRightButton;
    TSpeedButton *RightLeftButton;
    TSpeedButton *RightRightButton;
    TBitBtn *AllMatButton;
    TCheckBox *ShowMattingBoxCBox;
    TGroupBox *TrackingPointsGroupBox;
   TBitBtn *DeletePointButton;
    TCheckBox *ShowPointCBox;
    TCheckBox *ShowRenderPointCBox;
    TCheckBox *AutoSaveCBox;
    TCheckBox *AutoLoadCBox;
    TChart *VTrackGraph;
    TLineSeries *Series20;
    TLineSeries *Series21;
    TChart *HTrackGraph;
    TLineSeries *LineSeries1;
    TLineSeries *LineSeries2;
   TGroupBox *AnchorFramesGroupBox;
    TLabel *Label6;
    TLabel *Label7;
    TGroupBox *IndividualSmoothingGroupBox;
    TLabel *IndividualSmoothLabelMin;
    TLabel *IndividualSmoothLabelMax;
    TCheckBox *IndividualSmoothCBox;
    TComboBox *AnchorMotionComboBox;
    TLabel *MotionRestrictionLabel;
    TCheckBox *WarnMarkOutMoveCheckBox;
    TLabel *VerticalMatchLabel;
    TLabel *HorziontalMatchLabel;
    TPanel *Panel1;
    TLabel *Label1;
    TExecButtonsFrame *ExecButtonsToolbar;
    TBitBtn *SavePointsButton;
    TBitBtn *LoadPointsButton;
    TBitBtn *TrackButton;
        TBitBtn *NextPointButton;
        TBitBtn *PrevPointButton;
        TBevel *Bevel1;
        TLabel *Label2;
        TSpeedButton *ColorPickButton;
    TExecStatusBarFrame *ExecStatusBar;
        TCheckBox *ClearOnShotChangeCBox;
    TPanel *IndividualSmoothClippingPanel;
    TTrackEditFrame *IndividualSmoothTrackEdit;
	TBitBtn *SetMatButton;
	TPanel *XGraphLabelPanel;
	TPanel *YGraphLabelPanel;
	TPanel *TrackWarnPanel;
	TCheckBox *UseMattingCBox;
	TPanel *TabStopDoNotDeletePanel;
	TTimer *ColorPickerStateTimer;
	TBitBtn *SwapButton;
   TGroupBox *TrackingSettingsGroupBox;
   TLabel *SmoothMaxLabel;
   TLabel *SmoothMinLabel;
   TLabel *TrackingBoxSizeMinusLabel;
   TLabel *TrackingBoxSizePlusLabel;
   TLabel *MotionMinusLabel;
   TLabel *MotionPlusLabel;
   TTrackEditFrame *SmoothTrackEdit;
   TTrackEditFrame *TrackingBoxSizeTrackEdit;
   TTrackEditFrame *MotionTrackEdit;
   TGroupBox *DataFillGroupBox;
   TRadioButton *MissingDataBlackRadioButton;
   TRadioButton *MissingDataWhiteRadioButton;
   TRadioButton *MissingDataPrevFrameRadioButton;
   TCheckBox *EditAnchorsCheckBox;
   TColorPanel *HorizontalFixedFlagPanel;
   TColorPanel *HorizontalAnchoredFlagPanel;
   TColorPanel *VerticalAnchoredFlagPanel;
   TColorPanel *VerticalFixedFlagPanel;
	TPresetsFrame *PresetsFrame;
   TRadioButton *AnchorNoneRadioButton;
   TRadioButton *AnchorBothRadioButton;
   TRadioButton *AnchorLastRadioButton;
   TRadioButton *AnchorFirstRadioButton;
        void __fastcall SetStartTCButtonClick(TObject *Sender);
        void __fastcall RenderButtonClick(TObject *Sender);
        void __fastcall StopButtonClick(TObject *Sender);
        void __fastcall GotoTCButtonClick(TObject *Sender);
        void __fastcall SmoothTrackBarChange(TObject *Sender);
        void __fastcall IndividualSmoothCBoxClick(TObject *Sender);
        void __fastcall DeletePointButtonClick(TObject *Sender);
        void __fastcall TrackingTrackBarChange(TObject *Sender);
        void __fastcall SearchTrackBarChange(TObject *Sender);
        void __fastcall TrackButtonClick(TObject *Sender);
        void __fastcall IndividualSmoothTrackBarChange(TObject *Sender);
        void __fastcall TopUpButtonClick(TObject *Sender);
        void __fastcall TopDownButtonClick(TObject *Sender);
        void __fastcall BottomDownButtonClick(TObject *Sender);
        void __fastcall BottomUpButtonClick(TObject *Sender);
        void __fastcall LeftLeftButtonClick(TObject *Sender);
        void __fastcall RightLeftButtonClick(TObject *Sender);
        void __fastcall RightRightButtonClick(TObject *Sender);
        void __fastcall LeftRightButtonClick(TObject *Sender);
        void __fastcall AllMatButtonClick(TObject *Sender);
        void __fastcall UseMattingCBoxClick(TObject *Sender);
        void __fastcall SetMatButtonClick(TObject *Sender);
        void __fastcall AdvancedButtonClick(TObject *Sender);
        void __fastcall UpdateTimerTimer(TObject *Sender);
        void __fastcall AnchorMotionComboBoxChange(TObject *Sender);
        void __fastcall ShowPointCBoxClick(TObject *Sender);
        void __fastcall ShowMattingBoxCBoxClick(TObject *Sender);
        void __fastcall PreviewFrameButtonClick(TObject *Sender);
        void __fastcall LoadPointsButtonClick(TObject *Sender);
        void __fastcall SavePointsButtonClick(TObject *Sender);
        void __fastcall DownTimerTimer(TObject *Sender);
        void __fastcall AutoMoveButtonMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall AutoMoveButtonMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall WarnMarkOutMoveCheckBoxClick(TObject *Sender);
   void __fastcall PDLCaptureTBItemClick(TObject *Sender);
    void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(
          TObject *Sender);
        void __fastcall NextPointButtonClick(TObject *Sender);
        void __fastcall PrevPointButtonClick(TObject *Sender);
        void __fastcall ColorPickButtonClick(TObject *Sender);
        void __fastcall ColorPickerStateTimerTimer(TObject *Sender);
        void __fastcall ClearOnShotChangeCBoxClick(TObject *Sender);
	void __fastcall TabToBegining(TObject *Sender);
	void __fastcall SwapButtonClick(TObject *Sender);
   void __fastcall EditAnchorsCheckBoxClick(TObject *Sender);
	void __fastcall MissingDataRadioButtonClick(TObject *Sender);
   void __fastcall AnchorRadioButtonClick(TObject *Sender);
private:	// User declarations

   bool _bGUIUpdateFlag;
   int  _OldSelectedTag;
   int  _RestoreHeight;
   int  _RestoreWidth;
   int  _KeyDownCount;
   bool _bAdvancedPanelOn;
   bool _toolDisabled;
   bool _TabStopDoNotDeletePanelVisible;
   EToolDisabledReason toolDisabledReason;
   bool _nextSelectedTagIsANewTrackingPoint;
   int  _mostRecentlySelectedAnchor;
   int  _hackTagToPreventBogusReanchoring;
   int  _tagOfTrackingPointGettingAnchored;

   void UpdatePearson(int newTag);
   void HideShowCharts(bool bShowCharts);

   DEFINE_CBHOOK(ClipHasChanged, TDewarpForm);
   DEFINE_CBHOOK(ClipChanging, TDewarpForm);
   DEFINE_CBHOOK(MarksHaveChanged, TDewarpForm);

   void SetResumeTCToDefault(void);
   void RenderPreviewProcessing(TObject *Sender);
   void EnablePreview1(bool On);
   EToolProcessingCommand GetButtonCommand(TToolButton *button);
   void SetButtonCommand(TToolButton *button, EToolProcessingCommand newCmd);

   bool ReadSettings(CIniFile *ini, const string &IniSection);
	bool WriteSettings(CIniFile *ini, const string &IniSection);
	void UpdateGuiFromCurrentPreset();
	void UpdatePresetCurrentParameters();

	// Called when a the selected tag has changed
	DEFINE_CBHOOK(selectedTagChanged, TDewarpForm);
	DEFINE_CBHOOK(rangeChanged, TDewarpForm);
	DEFINE_CBHOOK(anchorChanged, TDewarpForm);
	DEFINE_CBHOOK(handleNewTrackingPointNotification, TDewarpForm);
	DEFINE_CBHOOK(CurrentPresetHasChanged, TDewarpForm);

   double FindExtentX(void);
   double FindExtentY(void);
   void UpdateTrackingPointGUIStuff();
   void DrawChartsTag(int Tag);
   void UpdateIndividualSmoothingDisplay(void);
   void UpdateIndividualSmoothingGUIFromTag(int nTag);
   void UpdateIndividualSmoothingTagFromGUI(int nTag);
//   void UpdateAnchorMotionGUIFromTag(int nTag);
   int  LastSelectedTag(void);
   void SetAdvancedPanel(void);

   TObject *GlobalSender;

   bool ShowPointsOldRenderingFlag;
   bool ShowPointsOldCheckedState;

   tpAnchor getSelectedAnchorTypeFromComboBox();
   void enterAnchorSettingMode(int trackingPointTag);
   void exitFromAnchorSettingMode();
   void cancelAnchorSettingMode();
   friend void CancelAnchorSettingMode();
   bool areWeInAnchorSettingMode(void);
   friend bool AreWeInAnchorSettingMode();
   bool areWeInAnchorEditingMode(void);
   friend bool AreWeInAnchorEditingMode();

   // Exec buttons toolbar
   void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
   void ExecStop(void);
   void ExecCaptureToPDL(void);
   void ExecCaptureAllEventsToPDL(void);
   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToCurrent(void);
   void ExecSetResumeTimecodeToDefault(void);
   void ExecGoToResumeTimecode(void);
   void UpdateExecutionButtonToolbar(
         EToolControlState toolProcessingControlState,
         bool processingRenderFlag);

   void setBusyCursor(void);
   void setIdleCursor(void);

public:		// User declarations
        __fastcall TDewarpForm(TComponent* Owner);
   bool doesFormUseOwnWindow() { return false; };

   void formCreate(void);
   void formShow(void);
   void formHide(void);

   void UpdateExecutionButtons(EToolControlState toolProcessingControlState,bool processingRenderFlag);
   void GatherParameters(CDewarpParameters &DewarpParameters);
   void UpdateToolStatusBar(int iPanel, const string &strMessage);
   bool runExecButtonsCommand(EExecButtonId command);

   void SetParameters(CDewarpParameters &DewarpParameters);
	void CaptureParameters(CPDLElement &toolParams);

   void ToggleEditAnchorsCheckBox();

   bool LoadTrackingPoints(const String &sName, bool restoreMarks=true);
   bool SaveTrackingPoints(const String &sName);
   void SaveTrackingPointIfNecessary(void);

   void UpdateStatusBarInfo(void);
   void RedrawSmoothedChart(void);
   void UpdateTrackingPointButtons(void);
   void UpdateTrackingCharts(void);

   void setControlFocus(int controlCommand);
   void swapInOut(void);
   void toggleClearOnShotChange(void);
   void toggleTrackPointVisibility(void);
   void setTrackPointsVisibility(bool flag);
   void toggleProcRegionVisibility(void);
   void toggleProcRegionOperation(void);
   string getSaveFilename(const string &suggestedFilename);
   string getLoadFilename(const string &suggestedFilename);
};
//---------------------------------------------------------------------------
extern PACKAGE TDewarpForm *DewarpForm;
extern MTI_UINT32 *DewarpFeatureTable[];

//---------------------------------------------------------------------------
#endif
