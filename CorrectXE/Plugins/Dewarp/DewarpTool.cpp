// Dewarp.cpp: implementation of the CDewarpTool class.

#include "DewarpTool.h"
#include "DewarpCoreUnit.h"
#include "err_dewarp.h"
#include "MathFunctions.h"
#include "MoveInMarkDialogUnit.h"

#include "bthread.h"
#include "ClipAPI.h"
#include "ColorPickTool.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIsleep.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "NavigatorTool.h"
#include "PDL.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "SysInfo.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingPointsEditor.h"
#include "MtiMkl.h"
#include "DewarpGuiWin.h"
#include "GpuAccessor.h"

#include "IpaStripeStream.h"

#define ANNOYING_DIALOG_TRACK_TIME_THRESHOLD 60 // seconds
#define RESTORE_TRACK_DATA_ON_LOAD 1

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 DewarpToolNumber = DEWARP_TOOL_NUMBER;

// -------------------------------------------------------------------------
// DewarpTool Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem DewarpToolDefaultConfigItems[] =
{
	// DewarpTool Tool Command             Modifiers      Action
	// + Key
	// ----------------------------------------------------------------
	// ### = stuff removed per Larry's 2/21/2014 spec
	// ----------------------------------------------------------------
	{DWRP_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT, " LButton         ButtonDown  "},
	{DWRP_CMD_STOP_DRAGGING_TRACKPOINT, " LButton         ButtonUp    "},
	{DWRP_CMD_SELECT_ANOTHER_TRACKPOINT, " Ctrl+LButton    ButtonDown  "},
	{DWRP_CMD_STOP_DRAGGING_TRACKPOINT, " Ctrl+LButton    ButtonUp    "},

	{DWRP_CMD_LOAD_TRACKPOINTS, " Ctrl+L           KeyDown    "},
	{DWRP_CMD_LOAD_TRACKPOINTS_NOT_MARKS, " Shift+L          KeyDown    "},
	{DWRP_CMD_SAVE_TRACKPOINTS, " Ctrl+S           KeyDown    "},
	{DWRP_CMD_CLEAR_TRACKPOINTS, " Ctrl+Backspace   KeyDown    "},
	{DWRP_CMD_CLEAR_TRACKPOINTS, " Shift+A          KeyDown    "},
	{DWRP_CMD_DELETE_SELECTED_TRACKPOINTS, " Backspace        KeyDown    "},
	{DWRP_CMD_DELETE_SELECTED_TRACKPOINTS, " A                KeyDown    "},

	// Per Larry, D now toggles the anchor mode on/off
	// { EXEC_BUTTON_PREVIEW_FRAME,                " D                KeyDown    " },
	{EXEC_BUTTON_PREVIEW_NEXT_FRAME, " Ctrl+D           KeyDown    "},
	{EXEC_BUTTON_PREVIEW_ALL, " Shift+D          KeyDown    "},
	{EXEC_BUTTON_RENDER_FRAME, " G                KeyDown    "},
	{EXEC_BUTTON_RENDER_NEXT_FRAME, " Ctrl+G           KeyDown    "},
	{EXEC_BUTTON_RENDER_ALL, " Shift+G          KeyDown    "}, {EXEC_BUTTON_STOP, " Ctrl+Space       KeyDown    "},
	{EXEC_BUTTON_PAUSE_OR_RESUME, " Space            KeyDown    "},
	{EXEC_BUTTON_SET_RESUME_TC, " Return           KeyDown    "},
	{EXEC_BUTTON_CAPTURE_PDL, " ,                KeyDown    "},
	{EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS, " Shift+,          KeyDown    "},

	{DWRP_CMD_TRACK, " T                KeyDown    "},
	{DWRP_CMD_TOGGLE_TRACK_POINT_VISIBILITY, " Shift+T          KeyDown    "},
	{DWRP_CMD_TOGGLE_PROC_REGION_VISIBILITY, " Ctrl+T           KeyDown    "},

	{DWRP_CMD_SET_IN_PT, " E                KeyDown    "}, {DWRP_CMD_SET_OUT_PT, " R                KeyDown    "},
	{DWRP_CMD_BEGIN_PAN, " U                KeyDown    "}, {DWRP_CMD_END_PAN, " U                KeyUp      "},

	{DWRP_CMD_PRESET_1, " 1                KeyDown    "}, {DWRP_CMD_PRESET_2, " 2                KeyDown    "},
	{DWRP_CMD_PRESET_3, " 3                KeyDown    "}, {DWRP_CMD_PRESET_4, " 4                KeyDown    "},
	{DWRP_CMD_TOGGLE_PROC_REGION_OPERATION, " 5                KeyDown    "},
	{DWRP_CMD_CYCLE_ANCHOR_MODES, " 6                KeyDown    "},
	{DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE, " 7                KeyDown    "},
	{DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA, " 8                KeyDown    "},
	{DWRP_CMD_FOCUS_ON_SMOOTHING, " 9                KeyDown    "},
	// { DWRP_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE,     " 0                KeyDown    " },
	{DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT, " 0                KeyDown    "},

	{DWRP_CMD_TOGGLE_EDIT_ANCHORS_MODE, " D                KeyDown    "},

	{DWRP_CMD_TURN_COLOR_PICKER_ON, " Ctrl+Shift+Shift KeyDown    "},
	{DWRP_CMD_TURN_COLOR_PICKER_ON, " Ctrl+Shift+Ctrl  KeyDown    "},
	{DWRP_CMD_TURN_COLOR_PICKER_OFF, " Ctrl+Shift       KeyUp      "},
	{DWRP_CMD_TURN_COLOR_PICKER_OFF, " Shift+Ctrl       KeyUp      "},

	{DWRP_CMD_SHIFT_IS_ON, " Shift+Shift      KeyDown    "}, {DWRP_CMD_SHIFT_IS_OFF, " Shift            KeyUp      "},

	{DWRP_CMD_CANCEL, " Esc              KeyDown    "}, };

static CUserInputConfiguration *DewarpToolDefaultUserInputConfiguration =
	 new CUserInputConfiguration(sizeof(DewarpToolDefaultConfigItems) / sizeof(CUserInputConfiguration::SConfigItem),
	 DewarpToolDefaultConfigItems);

// -------------------------------------------------------------------------
// DewarpTool Tool Command Table

static CToolCommandTable::STableEntry DewarpToolCommandTableEntries[] =
{
	// DewarpTool Tool Command            DewarpTool Tool Command String
	// -------------------------------------------------------------

	{DWRP_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT, "DWRP_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT"},
	{DWRP_CMD_STOP_DRAGGING_TRACKPOINT, "DWRP_CMD_STOP_DRAGGING_TRACKPOINT"},
	{DWRP_CMD_SELECT_ANOTHER_TRACKPOINT, "DWRP_CMD_SELECT_ANOTHER_TRACKPOINT"},
	{DWRP_CMD_STOP_DRAGGING_TRACKPOINT, "DWRP_CMD_STOP_DRAGGING_TRACKPOINT"},

	{DWRP_CMD_LOAD_TRACKPOINTS, "DWRP_CMD_LOAD_TRACKPOINTS"},
	{DWRP_CMD_LOAD_TRACKPOINTS_NOT_MARKS, "DWRP_CMD_LOAD_TRACKPOINTS_NOT_MARKS"},
	{DWRP_CMD_SAVE_TRACKPOINTS, "DWRP_CMD_SAVE_TRACKPOINTS"}, {DWRP_CMD_CLEAR_TRACKPOINTS, "DWRP_CMD_CLEAR_TRACKPOINTS"},
	{DWRP_CMD_CLEAR_TRACKPOINTS, "DWRP_CMD_CLEAR_TRACKPOINTS"},
	{DWRP_CMD_DELETE_SELECTED_TRACKPOINTS, "DWRP_CMD_DELETE_SELECTED_TRACKPOINTS"},
	{DWRP_CMD_DELETE_SELECTED_TRACKPOINTS, "DWRP_CMD_DELETE_SELECTED_TRACKPOINTS"},
	{DWRP_CMD_SET_MARKS_FROM_TRACK_ARRAY, "DWRP_CMD_SET_MARKS_FROM_TRACK_ARRAY"},

	{EXEC_BUTTON_PREVIEW_FRAME, "EXEC_BUTTON_PREVIEW_FRAME"},
	{EXEC_BUTTON_PREVIEW_NEXT_FRAME, "EXEC_BUTTON_PREVIEW_NEXT_FRAME"},
	{EXEC_BUTTON_PREVIEW_ALL, "EXEC_BUTTON_PREVIEW_ALL"}, {EXEC_BUTTON_RENDER_FRAME, "EXEC_BUTTON_RENDER_FRAME"},
	{EXEC_BUTTON_RENDER_NEXT_FRAME, "EXEC_BUTTON_RENDER_NEXT_FRAME"}, {EXEC_BUTTON_RENDER_ALL, "EXEC_BUTTON_RENDER_ALL"},
	{EXEC_BUTTON_STOP, "EXEC_BUTTON_STOP"}, {EXEC_BUTTON_PAUSE_OR_RESUME, "EXEC_BUTTON_PAUSE_OR_RESUME"},
	{EXEC_BUTTON_SET_RESUME_TC, "EXEC_BUTTON_SET_RESUME_TC"}, {EXEC_BUTTON_CAPTURE_PDL, "EXEC_BUTTON_CAPTURE_PDL"},
	{EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS, "EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS"},

	{DWRP_CMD_TOGGLE_FIX, "DWRP_CMD_TOGGLE_FIX"},
	{DWRP_CMD_TOGGLE_TRACK_POINT_VISIBILITY, "DWRP_CMD_TOGGLE_TRACK_POINT_VISIBILITY"},
	{DWRP_CMD_TOGGLE_PROC_REGION_VISIBILITY, "DWRP_CMD_TOGGLE_PROC_REGION_VISIBILITY"},
	{DWRP_CMD_TOGGLE_PROC_REGION_OPERATION, "DWRP_CMD_TOGGLE_PROC_REGION_OPERATION"},
	{DWRP_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE, "DWRP_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE"},

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE
	{DWRP_CMD_SET_IN_PT, "DWRP_CMD_SET_IN_PT"}, {DWRP_CMD_SET_OUT_PT, "DWRP_CMD_SET_OUT_PT"},
#endif
	{DWRP_CMD_BEGIN_PAN, "DWRP_CMD_BEGIN_PAN"}, {DWRP_CMD_END_PAN, "DWRP_CMD_END_PAN"},

	// { DWRP_CMD_EXECUTE_PDL,                     "DWRP_CMD_EXECUTE_PDL" },

	{DWRP_CMD_TRACK, "DWRP_CMD_TRACK"}, {DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE, "DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE"},
	{DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA, "DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA"},
	{DWRP_CMD_FOCUS_ON_SMOOTHING, "DWRP_CMD_FOCUS_ON_SMOOTHING"},
	{DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT, "DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT"},
	{DWRP_CMD_CYCLE_MISSING_DATA_FILL_PREV, "DWRP_CMD_CYCLE_MISSING_DATA_FILL_PREV"},
	// { DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_IN,       "DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_IN" },
	// { DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_OUT,      "DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_OUT" },
	{DWRP_CMD_FOCUS_ON_INDIVIDUAL_SMOOTHING, "DWRP_CMD_FOCUS_ON_INDIVIDUAL_SMOOTHING"},
	{DWRP_CMD_FOCUS_ON_MOTION_RESTRICTION, "DWRP_CMD_FOCUS_ON_MOTION_RESTRICTION"},

	{DWRP_CMD_TURN_COLOR_PICKER_ON, "DWRP_CMD_TURN_COLOR_PICKER_ON"},
	{DWRP_CMD_TURN_COLOR_PICKER_ON, "DWRP_CMD_TURN_COLOR_PICKER_ON"},
	{DWRP_CMD_TURN_COLOR_PICKER_OFF, "DWRP_CMD_TURN_COLOR_PICKER_OFF"},
	{DWRP_CMD_TURN_COLOR_PICKER_OFF, "DWRP_CMD_TURN_COLOR_PICKER_OFF"},

	{DWRP_CMD_SHIFT_IS_ON, "DWRP_CMD_SHIFT_IS_ON"}, {DWRP_CMD_SHIFT_IS_OFF, "DWRP_CMD_SHIFT_IS_OFF"},

	{DWRP_CMD_CANCEL, "DWRP_CMD_CANCEL"}, {DWRP_CMD_TOGGLE_EDIT_ANCHORS_MODE, "DWRP_CMD_TOGGLE_EDIT_ANCHORS_MODE"},
	{DWRP_CMD_CYCLE_ANCHOR_MODES, "DWRP_CMD_CYCLE_ANCHOR_MODES"}, };

static CToolCommandTable *DewarpToolCommandTable =
	 new CToolCommandTable(sizeof(DewarpToolCommandTableEntries) / sizeof(CToolCommandTable::STableEntry),
	 DewarpToolCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// CDewarpTool:
//////////////////////////////////////////////////////////////////////

CDewarpTool::CDewarpTool(const string & newToolName, MTI_UINT32 * *newFeatureTable)
	 : CToolObject(newToolName, newFeatureTable), toolSetupHandle(-1), trainingToolSetupHandle(-1), _toolActive(false),
	 bNeedToReTrack(true), mouseUpDeferredTrackPointSelectionHackTag(-1) // awful hack, don't ask!
	 , _smoothDataIsValid(false)
{
	// TTT Tool-specific constructors go here

	_resumeFrameIndex = -1;
	_previewFrameIndex = -1;
	_dewarpControlState = DEWARP_STATE_ALLOCATE;
	_displayRect = true;
	_WarningDialogs = true;
	_BoundingBoxDefined = false;
	ProcessedTrackBoxExtent = -1;
	ProcessedSearchBoxExtent = -1;

	IniFileName = CPMPIniFileName("Dewarp");

	// allocate tracking points editor and color picker
	TrkPtsEditor = new CTrackingPointsEditor;
	ColorPickTool = new CColorPickTool;

	CurrentTrackBoxExtentX = 10;
	CurrentTrackBoxExtentY = 10;
	CurrentSearchBoxExtentLeft = 10;
	CurrentSearchBoxExtentRight = 10;
	CurrentSearchBoxExtentUp = 10;
	CurrentSearchBoxExtentDown = 10;

	// Annoying dialog stuff
	HowLongItTookToTrackInSecs = 0;
	ShowedInMarkMoveMessage = false;
	ShowedOutMarkMoveMessage = false;
	TrackingDataHasBeenUsedToRenderAll = false;
	SingleFrameFlag = false;
	shiftIsOn = false;
	needToAutoSave = false;

	// PRESETS!
	_userPresets = new PresetParameterModel<CDewarpParameters>("DewarpParameters", 4);
	_userPresets->ReadPresetsFromIni(IniFileName);
	_userPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
}

CDewarpTool::~CDewarpTool()
{
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the DewarpTool Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CDewarpTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
	int retVal;

	JobManager jobManager;
	jobManager.SetCurrentToolCode("dw");

	// Initialize the CDewarpTool's base class, i.e., CToolObject
	// This must be done before the CDewarpTool initializes itself
	retVal = initBaseToolObject(DewarpToolNumber, newSystemAPI, DewarpToolCommandTable,
		 DewarpToolDefaultUserInputConfiguration);
	if (retVal != 0)
	{
		// ERROR: initBaseToolObject failed
		TRACE_0(errout << "Error in DewarpTool Tool, initBaseToolObject failed " << retVal);
		return retVal;
	}

	// Create the Tool's GUI
	CreateDewarpToolGUI();

	// Load the system API into the TrkPtsEditor and ColorPickTool
	TrkPtsEditor->loadSystemAPI(newSystemAPI);
	ColorPickTool->loadSystemAPI(newSystemAPI);

	// Load the tool's tracking array into the editor
	_CurrentClipName = getSystemAPI()->getClipFilename();
	TrkPtsEditor->setTrackingArray(&TrackingArray);
	InvalidateSmoothing();

	DewarpForm->UpdateStatusBarInfo();
	DewarpForm->UpdateTrackingPointButtons();
	DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

	// Deferred event flags
	_deferredClipChangeFlag = true;
	_deferredMarkChangeFlag = false;

	SET_CBHOOK(ColorPickerColorChanged, ColorPickTool->newColorHoveredOver);
	SET_CBHOOK(ColorPickerColorPicked, ColorPickTool->newColorPicked);

	return 0;

} // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CDewarpTool::toolActivate()
{
	// First
	int retVal = CToolObject::toolActivate();
	if (retVal != 0)
	{
		return retVal;
	}

	_toolActive = true;

	JobManager jobManager;
	jobManager.SetCurrentToolCode("dw");

	getSystemAPI()->SetToolNameForUserGuide("DewarpTool");

	// These go AFTER _toolActive = true!!!
	if (_deferredClipChangeFlag)
	{
		onNewClip();
	}
	else if (_deferredMarkChangeFlag || _deferredClipChangeFlag)
	{
		// Brute force clear tracking if marks or clip changed outside of tool
		if (_clearTrackingStuffOnNewShot)
		{
			ClearTrackingPoints();
		}
		onNewMarks();
	}
	_deferredClipChangeFlag = false;
	_deferredMarkChangeFlag = false;

	// Refresh the frame to force drawing of tracking points
	GetTrackingPointsEditor()->refreshTrackingPointsFrame();

	getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

	// Read in the bounding box in case it got changed in another tool
	auto curClip = getSystemAPI()->getClip();
	if (curClip != nullptr)
		readBoundingBoxFromDesktopIni(curClip);
	else
		_BoundingBoxDefined = false; // yuck QQQ

	SET_CBHOOK(SetNeedToAutoSave, TrkPtsEditor->TrackingParameterChange);

	return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CDewarpTool::toolDeactivate()
{
	REMOVE_CBHOOK(SetNeedToAutoSave, TrkPtsEditor->TrackingParameterChange);

	// Auto-accept GOV if pending
	getSystemAPI()->AcceptGOV();

	// Get rid of any review regions that might be present on the screen
	getSystemAPI()->ClearProvisional(true);

	DestroyAllToolSetups(true);
	_toolActive = false;

	auto curClip = getSystemAPI()->getClip();
	if (curClip != nullptr)
		writeBoundingBoxToDesktopIni(curClip);

	if (IsInColorPickMode())
		ToggleColorPicker();

	GetToolProgressMonitor()->SetIdle();
	getSystemAPI()->SetGOVToolProgressMonitor(nullptr);

	// Last
	return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// DewarpTool Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CDewarpTool::toolShutdown()
{
	int retVal;

	// Auto-accept GOV if pending
	getSystemAPI()->AcceptGOV();

	// Destroy DewarpTool's GUI
	DestroyDewarpToolGUI();

	GDewarpTool = nullptr; // Say we are destroyed

	delete TrkPtsEditor;
	delete ColorPickTool;

	// TTT Tool-specific destructors go here

	// Shutdown the DewarpTool Tool's base class, i.e., CToolObject.
	// This must be done after the DewarpTool shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
}

bool CDewarpTool::DoesToolPreserveHistory()
{
	return false;
}

CToolProcessor* CDewarpTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
	return new CDewarpProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr,
		 GetToolProgressMonitor());

}

// ===================================================================
//
// Function:    onRedraw
//
// Description: DewarpTool Tool's redraw handler
//
// Arguments:   frame index
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CDewarpTool::onRedraw(int frameindex)
{
	if (IsDisabled() || !IsActive())
		return TOOL_RETURN_CODE_NO_ACTION;

	TrkPtsEditor->drawTrackingPointsCurrentFrame(AreWeInAnchorEditingMode() ? SOLID_YELLOW : SOLID_RED);

	if (AreWeInAnchorSettingMode())
	{
		TrkPtsEditor->drawLineFromSelectedTrackingPointToMousePosition();
	}

	DisplayRectangle();

	return TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: DewarpTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CDewarpTool::onToolCommand(CToolCommand &toolCommand)
{
	if (IsDisabled() || !IsActive())
		return TOOL_RETURN_CODE_NO_ACTION;

	int commandNumber = toolCommand.getCommandNumber();
	int Result = TOOL_RETURN_CODE_NO_ACTION;
	TRACE_3(errout << "Dewarp Tool command number " << commandNumber);

	// Dispatch loop for Dewarp Tool commands

	switch (commandNumber)
	{

	case EXEC_BUTTON_PREVIEW_FRAME:
	case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
	case EXEC_BUTTON_PREVIEW_ALL:
	case EXEC_BUTTON_RENDER_FRAME:
	case EXEC_BUTTON_RENDER_NEXT_FRAME:
	case EXEC_BUTTON_RENDER_ALL:
	case EXEC_BUTTON_STOP:
	case EXEC_BUTTON_PAUSE_OR_RESUME:
	case EXEC_BUTTON_SET_RESUME_TC:
	case EXEC_BUTTON_CAPTURE_PDL:
	case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
		{
			bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
			if (consumed)
				Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
			break;
		}

	case DWRP_CMD_TOGGLE_FIX:
		getSystemAPI()->ToggleProvisional();
		Result = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
		break;

	case DWRP_CMD_TOGGLE_TRACK_POINT_VISIBILITY:
		ToggleTrackPointVisibility();
		break;

	case DWRP_CMD_TOGGLE_PROC_REGION_VISIBILITY:
		ToggleProcRegionVisibility();
		break;

	case DWRP_CMD_TOGGLE_PROC_REGION_OPERATION:
		ToggleProcRegionOperation();
		break;

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE

	case DWRP_CMD_SET_IN_PT:
		if (_toolActive)
		{
			if (canInMarkBeMoved())
			{
				TrkPtsEditor->setRangeFromMarks();
			}
			getSystemAPI()->setMainWindowFocus(); // TTT
		}
		// NOT Consumed! need to pass through to NavigatorTool
		break;

	case DWRP_CMD_SET_OUT_PT:
		if (_toolActive)
		{
			if (canOutMarkBeMoved())
			{
				TrkPtsEditor->setRangeFromMarks();
			}
			getSystemAPI()->setMainWindowFocus(); // TTT
		}
		// NOT Consumed! need to pass through to NavigatorTool
		break;

#else

	case DWRP_CMD_SET_MARKS_FROM_TRACK_ARRAY:
		if (TrackingArray.getColor() != CTrackingArray::ETPRed)
		{
			// OUT, then IN - I think that has a better chance of success!!
			getSystemAPI()->setMarkOut(TrackingArray.endIndex());
			getSystemAPI()->setMarkIn(TrackingArray.beginIndex());
			getSystemAPI()->goToFrameSynchronous(getSystemAPI()->getMarkIn());
		}
		break;

#endif // !DECOUPLE_MARKS_FROM_TRACKED_RANGE

	case DWRP_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT:

		getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending
		mouseUpDeferredTrackPointSelectionHackTag = -1; // awful hack, don't ask!

		TrkPtsEditor->setShift(false);
		TrkPtsEditor->mouseDown();

		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_SELECT_ANOTHER_TRACKPOINT:

		getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

		// WTF! STUPID TRACK POINT EDITOR API - set SHIFT, not CTRL!!
		TrkPtsEditor->setShift(true);
		TrkPtsEditor->mouseDown();

		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_STOP_DRAGGING_TRACKPOINT:

		TrkPtsEditor->mouseUp();

		// Awful hack - can select or create an anchor point on mouse up!!
		if (AreWeInAnchorSettingMode())
		{
			// Bwahh-ha-haaaaaa!!!!
			TrkPtsEditor->setShift(false);
			TrkPtsEditor->setCtrl(true);
			TrkPtsEditor->mouseDown();
			TrkPtsEditor->mouseUp();
		}

		// Awful hack to be able to select a point from the selectedTagChanged callback!
		if (mouseUpDeferredTrackPointSelectionHackTag >= 0)
		{
			TrkPtsEditor->setUniqueSelection(mouseUpDeferredTrackPointSelectionHackTag);
			mouseUpDeferredTrackPointSelectionHackTag = -1;
		}

		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_BEGIN_PAN:

		TrkPtsEditor->setKeyU(true);
		// NOT Consumed! need to pass through to NavigatorTool
		break;

	case DWRP_CMD_END_PAN:

		TrkPtsEditor->setKeyU(false);
		// NOT Consumed! need to pass through to NavigatorTool
		break;

	case DWRP_CMD_DELETE_SELECTED_TRACKPOINTS:

		DeleteSelectedTrackingPoints();

		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

		break;

	case DWRP_CMD_CLEAR_TRACKPOINTS:

		ClearTrackingPoints();
		break;

	case DWRP_CMD_LOAD_TRACKPOINTS:

		getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

		LoadTrackingPointsFromFile();
		DewarpForm->UpdateTrackingPointButtons();
		DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_LOAD_TRACKPOINTS_NOT_MARKS:

		getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

		LoadTrackingPointsFromFile(false); // false = "Don't restore marks"
		DewarpForm->UpdateTrackingPointButtons();
		DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_SAVE_TRACKPOINTS:
		SaveTrackingPointsToFile();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_TRACK:

		getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

		if (TrackingArray.numberTrackPointsPerFrame() == 0)
		{
			// Do nothing if there are no tracking points.
			break;
		}

		if (TrackingArray.numberTrackPointsPerFrame() < 3)
		{
			// Complain if there are fewer than 3 tracking points.
			_MTIErrorDialog("This tool requires at least 3 tracking points!");
			break;
		}

		bTrackOnly = true;
		bNeedToReTrack = true;
		InvalidateSmoothing();
		ButtonCommandHandler(TOOL_PROCESSING_CMD_PREVIEW);

		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE:
	case DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA:
	case DWRP_CMD_FOCUS_ON_SMOOTHING:
	case DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT:
	case DWRP_CMD_CYCLE_MISSING_DATA_FILL_PREV:
	case DWRP_CMD_CYCLE_ANCHOR_MODES:
	case DWRP_CMD_FOCUS_ON_INDIVIDUAL_SMOOTHING:
	case DWRP_CMD_FOCUS_ON_MOTION_RESTRICTION:
		SetControlFocus(commandNumber);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_SWAP_IN_OUT:
		SwapInOut();
		break;

	case DWRP_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE:
		ToggleClearOnShotChange();
		break;

	case DWRP_CMD_TURN_COLOR_PICKER_ON:
		if (!IsInColorPickMode())
			ToggleColorPicker();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;
	case DWRP_CMD_TURN_COLOR_PICKER_OFF:
		if (IsInColorPickMode())
			ToggleColorPicker();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_SHIFT_IS_ON:
		shiftIsOn = true;
		break;

	case DWRP_CMD_SHIFT_IS_OFF:
		shiftIsOn = false;
		break;

	case DWRP_CMD_TOGGLE_EDIT_ANCHORS_MODE:
		ToggleEditAnchorsMode();
		break;

	case DWRP_CMD_CANCEL:
		if (AreWeInAnchorSettingMode())
		{
			CancelAnchorSettingMode();
		}
		break;

		// PRESETS!
	case DWRP_CMD_PRESET_1:
		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_PRESET_2:
		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(1);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_PRESET_3:
		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(2);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case DWRP_CMD_PRESET_4:
		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(3);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	default:

		break;
	}

	if (IsInColorPickMode() && ((Result & TOOL_RETURN_CODE_EVENT_CONSUMED) != 0) &&
		 (commandNumber != DWRP_CMD_TURN_COLOR_PICKER_ON))
		ToggleColorPicker();

	return (Result);

} // onToolCommand

// ===================================================================
//
// Function:    onUserInput
//
// Description: Navigator Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDewarpTool::onUserInput(CUserInput &userInput)
{
	// default: user input will be passed back to the tool manager
	int retVal = TOOL_RETURN_CODE_NO_ACTION;

	if (IsDisabled() || !IsActive())
	{
		return retVal;
	}

	if ((userInput.action == USER_INPUT_ACTION_KEY_DOWN || userInput.action == USER_INPUT_ACTION_KEY_UP)
		 && userInput.key == MTK_TAB)
	{
		TWinControl *baseToolForm = getSystemAPI()->getBaseToolForm();
		if (baseToolForm->Visible)
		{
			getSystemAPI()->getBaseToolForm()->SetFocus();
		}

		SetControlFocus(DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_IN);

		return retVal;
	}

	// If we're disabled or there's no clip, don't do anything
	if (!getSystemAPI()->isAClipLoaded())
	{
		return retVal;
	}

	// pass the event to the colorpicker
	switch (userInput.action)
	{
	case USER_INPUT_ACTION_MOUSE_BUTTON_DOWN:
		retVal = ColorPickTool->onMouseDown(userInput);
		break;
	case USER_INPUT_ACTION_MOUSE_BUTTON_UP:
		retVal = ColorPickTool->onMouseUp(userInput);
		break;

	default:
		break;
	}

	// If we didn't handle the INPUT event, feed it back through the
	// onToolCommand interface!
	if (retVal == TOOL_RETURN_CODE_NO_ACTION)
	{
		retVal = CToolObject::onUserInput(userInput);

		// Gaack. We know that left mouse button will try to make or
		// manipulate tracking points, so make sure they're visible!!!
		if (userInput.action == USER_INPUT_ACTION_MOUSE_BUTTON_DOWN && userInput.key == MTK_LBUTTON)
		{
			SetTrackPointsVisibility(true);
		}
	}

	return retVal;
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Navigator Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDewarpTool::onMouseMove(CUserInput &userInput)
{
	// If we're disabled or there's no clip, don't do anything
	if ((!IsDisabled()) && IsShowing() && getSystemAPI()->isAClipLoaded())
	{
		int retVal = ColorPickTool->onMouseMove(userInput);
		if (retVal == TOOL_RETURN_CODE_NO_ACTION)
		{
			TrkPtsEditor->mouseMove(userInput.windowX, userInput.windowY);
		}

		if (AreWeInAnchorSettingMode())
		{
			getSystemAPI()->refreshFrameCached();
		}
	}

	// Always pass through the mouse move so panning will continue to work
	return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDewarpTool::onNewMarks()
{
	if (!IsLicensed()) // NOT IsDisabled(), which, WTF, is true when invisible
			 return TOOL_RETURN_CODE_NO_ACTION;

	if (IsDisabled() || !IsActive())
	{
		_deferredMarkChangeFlag = true;
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	AutoSaveTrackingPoints();

	// CAREFUL!!! canMarksBeMoved HAS SIDE EFFECTS - for example, if it
	// decides the mark CAN'T be moved it sets it back to the old setting!!
	if (canMarksBeMoved())
		TrkPtsEditor->setRangeFromMarks();

	MarksChange.Notify();

	return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDewarpTool::onChangingClip()
{
	if (IsDisabled()) // !IsActive() is OK!!!
			 return TOOL_RETURN_CODE_NO_ACTION;

	AutoSaveTrackingPoints();

	ClipChanging.Notify();

	auto curClip = getSystemAPI()->getClip();
	if (curClip != nullptr)
		writeBoundingBoxToDesktopIni(curClip);

	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending
	getSystemAPI()->ClearProvisional(false);

	DestroyAllToolSetups(false);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onNewClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDewarpTool::onNewClip()
{
	if (!IsLicensed()) // NOT IsDisabled(), might want to Enable!
			 return TOOL_RETURN_CODE_NO_ACTION;

	if (!(_toolActive /* && IsShowing() */))
	{
		_deferredClipChangeFlag = true;
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	// Do we need to disable or re-enable the tool?
	if (isItOkToUseToolNow())
		getSystemAPI()->EnableTool();
	else
		getSystemAPI()->DisableTool(getToolDisabledReason());

	// Make sure we're starting fresh - probably unnecessary
	getSystemAPI()->ClearProvisional(false);

	// these are inited anew
	_CurrentClipName = getSystemAPI()->getClipFilename();

	TrkPtsEditor->resetTrackingArray();
	TrkPtsEditor->setRangeFromMarks();

	if (IsInColorPickMode())
		ToggleColorPicker();

	// When a clip is loaded, try to read bounding box from the desktop.ini file
	auto curClip = getSystemAPI()->getClip();
	if (curClip != nullptr)
		readBoundingBoxFromDesktopIni(curClip);
	else
		_BoundingBoxDefined = false; // yuck QQQ

	// This allows the tool to notify the gui
	ClipChange.Notify();

	// Re-autofind bounding box if there wasn't one associated with the clip
	if (!BoundingBoxDefined())
		FindBoundingBox();

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CDewarpTool::onChangingFraming()
{
	if (IsDisabled() || !IsActive())
		return TOOL_RETURN_CODE_NO_ACTION;

	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending
	getSystemAPI()->ClearProvisional(true);

	DestroyAllToolSetups(false);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDewarpTool::toolShow()
{
	// Because Dewarp is sensitive to the order setting marks and tracking
	// points, it gets confused when loading a PDL entry.
	// To avoid this problem, Dewarp needs to know when a PDL Entry
	// is being loaded (as opposed to being setup by a human).
	if (getSystemAPI()->IsPDLEntryLoading())
		TrkPtsEditor->resetTrackingArray();

	ShowDewarpToolGUI();

	if (!isItOkToUseToolNow())
		getSystemAPI()->DisableTool(getToolDisabledReason());

	// in MTIWinInterface, the cursor type is
	// 0 = default, 1 = arrows, 2 = cross
	_oldBusyCursor = CBusyCursor::getBusyCursorType();
	if (_oldBusyCursor != 1)
		SetBusyCursorToBusyArrows();

	return 0;
}

bool CDewarpTool::IsShowing(void)
{
	return IsToolVisible();
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDewarpTool::toolHide()
{
	HideDewarpToolGUI();
	doneWithTool();

	if (_oldBusyCursor == 0)
		SetBusyCursorToDefault();
	else if (_oldBusyCursor == 2)
		SetBusyCursorToBusyCross();

	return (0);
}

void CDewarpTool::getStatusMessage(string &sMessage)
{
	_MessageLocker.Lock("CDewarpTool::getStatusMessage");
	sMessage = _StatusMessage;
	_MessageLocker.UnLock();
}

void CDewarpTool::setStatusMessage(const string &NewMessage)
{
	_MessageLocker.Lock("CDewarpTool::setStatusMessage");
	_StatusMessage = NewMessage;
	_MessageLocker.UnLock();
}

void CDewarpTool::getTimeMessage(string &sMessage)
{
	_TimeMessageLocker.Lock("CDewarpTool::getTimeMessage");
	sMessage = _TimeMessage;
	_TimeMessageLocker.UnLock();
}

void CDewarpTool::setTimeMessage(const string &NewMessage)
{
	_TimeMessageLocker.Lock("CDewarpTool::setTimeMessage");
	_TimeMessage = NewMessage;
	_TimeMessageLocker.UnLock();
}

// ---------------------------------------------------------------------------

bool CDewarpTool::FindBoundingBox(int R, int G, int B, int X, int Y, int slack)
{
	const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt == nullptr)
		return false;

	// Get some value
	int _pxlComponentCount = imgFmt->getComponentCountExcAlpha();
	int nPicCol = imgFmt->getPixelsPerLine();
	int nPicRow = imgFmt->getLinesPerFrame();
	bool bRGB = (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_Y) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY);

	RECT _ActiveBox = imgFmt->getActivePictureRect();

	// Hack - assume slack was for 10-bit data, so if it's 8, divide by 2
	// (dividing by 4 makes it too narrow, I think
	if (imgFmt->getPixelPacking() == IF_PIXEL_PACKING_8Bits_IN_1Byte)
		slack /= 2;

	// Allocate buffer
	int pxlCnt = nPicRow * nPicCol;
	int totWds = (2 * pxlCnt * _pxlComponentCount + ((pxlCnt + 7) / 8) + 1) / 2;
	MTI_UINT16 *BaseImage = new MTI_UINT16[totWds];

	getSystemAPI()->getLastFrameAsIntermediate(BaseImage);
	setBoundingBoxInternal(FindMatBox(BaseImage, nPicCol, nPicRow, slack, _ActiveBox, bRGB, R, G, B, X, Y));
	delete[]BaseImage;
	return true;
}

int CDewarpTool::readDewarpParameters()
{
	if (IniFileName.empty())
		return 0;

	CIniFile* ini = CreateIniFile(IniFileName);
	if (ini == nullptr)
		return 0;

	delete ini;

	return 0;
}

void CDewarpTool::writeDewarpParameters()
{
	if (IniFileName.empty())
		return;

	CIniFile* ini = CreateIniFile(IniFileName);
	if (ini == nullptr)
		return;

	delete ini;
}
// ---------------------------------------------------------------------------

void CDewarpTool::setTrackingBoxSizes(int newTrackBoxXRadius, int newTrackBoxYRadius, int newSearchLeftDistance,
	 int newSearchRightDistance, int newSearchUpDistance, int newSearchDownDistance)
{
	CurrentTrackBoxExtentX = newTrackBoxXRadius;
	CurrentTrackBoxExtentY = newTrackBoxYRadius;
	CurrentSearchBoxExtentLeft = newSearchLeftDistance;
	CurrentSearchBoxExtentRight = newSearchRightDistance;
	CurrentSearchBoxExtentUp = newSearchUpDistance;
	CurrentSearchBoxExtentDown = newSearchDownDistance;
	if (TrackingArray.areExtentsDifferent(CurrentTrackBoxExtentX, CurrentTrackBoxExtentY, CurrentSearchBoxExtentLeft,
		 CurrentSearchBoxExtentRight, CurrentSearchBoxExtentUp, CurrentSearchBoxExtentDown))
	{
		TrackingArray.setExtents(CurrentTrackBoxExtentX, CurrentTrackBoxExtentY, CurrentSearchBoxExtentLeft,
			 CurrentSearchBoxExtentRight, CurrentSearchBoxExtentUp, CurrentSearchBoxExtentDown);
		getSystemAPI()->refreshFrameCached();
	}
}
// ---------------------------------------------------------------------------

int CDewarpTool::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
	if (IsDisabled())
		return 0;

	CaptureGUIParameters(pdlEntryToolParams);

	// Write Tracking Points
	TrackingArray.setBoundingBox(getBoundingBox());
	TrackingArray.AddToPDLEntry(pdlEntryToolParams);

	return 0;
}

void CDewarpTool::DisplayRectangle(void)
{
	if (_displayRect)
	{
		getSystemAPI()->setGraphicsColor(SOLID_BLUE);
		getSystemAPI()->drawRectangleFrame(&_BoundingBox);
	}

}

int CDewarpTool::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
	if (IsDisabled())
		return 0;

	int retVal;

	CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
	CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

	CDewarpParameters toolParams;
	CTrackingArray newTrackingArray;

	for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
	{
		CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(i);
		string elementName = toolAttribs->GetElementName();
		if (elementName == "DewarpParameters")
		{
			toolParams.ReadPDLEntry(toolAttribs);
			SetGUIParameters(toolParams);
		}
		else if (elementName == "TrackingArray")
		{
			newTrackingArray.ReadPDLEntry(toolAttribs);

			setBoundingBox(newTrackingArray.getBoundingBox());

			SetTrackingPoints(newTrackingArray);

			GetTrackingPointsEditor()->lastSelectedTag = -1;
			GetTrackingPointsEditor()->refreshTrackingPointsFrame();
			DewarpForm->RedrawSmoothedChart();
			DewarpForm->UpdateStatusBarInfo();
			DewarpForm->UpdateTrackingPointButtons();
			DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
		}
	}

	// This seems to fix bug 2649 - force full track then render;
	// previously these values were being referred to without ever
	// having been set!
	bNeedToReTrack = true;
	bTrackOnly = false;
	// --//
	InvalidateSmoothing();

	return 0;
}

void CDewarpTool::ResetResumeFrameIndex(int resumeFrame)
{
	_resumeFrameIndex = resumeFrame;
}

// ---------------------------------------------------------------------------

void CDewarpTool::ButtonCommandHandler(EToolProcessingCommand buttonCommand)
{
	switch (buttonCommand)
	{
	case TOOL_PROCESSING_CMD_PREVIEW_1:
		StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1, false);
		break;
	case TOOL_PROCESSING_CMD_RENDER:
		StartToolProcessing(buttonCommand, false);
		break;
	case TOOL_PROCESSING_CMD_PREVIEW:
		StartToolProcessing(buttonCommand, false);
		break;
	case TOOL_PROCESSING_CMD_PAUSE:
		PauseToolProcessing();
		break;
	case TOOL_PROCESSING_CMD_CONTINUE:
		// resume frame was set when the pause button was hit
		ContinueToolProcessing(_resumeFrameIndex);
		break;
	case TOOL_PROCESSING_CMD_STOP:
		StopToolProcessing();
		break;

	default:
		break;
	}

} // ButtonCommandHandler()
// ----------------------------------------------------------------------------

EDewarpControlState CDewarpTool::GetControlState()
{
	return _dewarpControlState;
}
// ----------------------------------------------------------------------------

void CDewarpTool::SetControlState(EDewarpControlState newControlState)
{
	_dewarpControlState = newControlState;
}
// ----------------------------------------------------------------------------

CTrackingPointsEditor *CDewarpTool::GetTrackingPointsEditor(void)
{
	return TrkPtsEditor;
}
// ----------------------------------------------------------------------------

void CDewarpTool::SetTrackingPoints(const CTrackingArray &TA)
{
	// Load the tool's tracking array into the editor
	TrackingArray = TA;
	TrkPtsEditor->setTrackingArray(&TrackingArray, true);

	DewarpForm->UpdateStatusBarInfo();
	DewarpForm->UpdateTrackingPointButtons();
}
// ----------------------------------------------------------------------------

bool CDewarpTool::isAllTrackDataValid()
{
	if (TrackingArray.numberTrackPointsPerFrame() < 1)
		return false;

	return GetTrackingPointsEditor()->isTrackingDataValidOverEntireRange();
}
// ----------------------------------------------------------------------------

void CDewarpTool::SetMouseUpDeferredTrackPointSelectionHackTag(int nTag)
{
	// awful hack, don't ask!
	mouseUpDeferredTrackPointSelectionHackTag = nTag;
}
// ----------------------------------------------------------------------------

bool CDewarpTool::IsNecessaryTrackDataAvailable()
{
	int markIn = getSystemAPI()->getMarkIn();
	int markOut = getSystemAPI()->getMarkOut();
	bool marksAreOK = markIn >= 0 && markOut > markIn && TrackingArray.beginIndex() <=
		 markIn && TrackingArray.endIndex() >= markOut;

	return (isAllTrackDataValid() && marksAreOK);
}
// ----------------------------------------------------------------------------

string CDewarpTool::GetSuggestedTrackingDataSaveFilename()
{
	// Suggests that the current save file be overwritten, unless
	// we were informed to start a new generation
	JobManager jobManager;
	string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::dewarpType);
	string suggestedFilePath = jobManager.GetJobFolderFileSavePath(suggestedFolder, "", false, "pts");
	return suggestedFilePath;
}
// ----------------------------------------------------------------------------

string CDewarpTool::GetSuggestedTrackingDataLoadFilename()
{
	// Suggests the latest version for these marks.
	JobManager jobManager;
	string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::dewarpType);
	string suggestedFilePath = jobManager.GetJobFolderFileLoadPath(suggestedFolder, "", false, "pts");
	return suggestedFilePath;
}

// ----------------------------------------------------------------------------
//
// Get a filename from the user, and save the tracking points in the file
//
void CDewarpTool::SaveTrackingPointsToFile()
{
	string suggestedFilename = GetSuggestedTrackingDataSaveFilename();
	string filename = GetFilenameForSavingTrackingPoints(suggestedFilename);
	if (filename.empty())
	{
		return;
	}

	SaveTrackingPoints(filename);
}

// ----------------------------------------------------------------------------
//
// Auto-save the tracking points to the file suggested by the Job Manager
//
void CDewarpTool::AutoSaveTrackingPoints()
{
	if (!needToAutoSave || TrackingArray.numberTrackPointsPerFrame() < 1)
	{
		return;
	}

	needToAutoSave = false;
	SaveTrackingPoints(GetSuggestedTrackingDataSaveFilename());
}

// ----------------------------------------------------------------------------
//
void CDewarpTool::SetNeedToAutoSave(void *Sender)
{
	// Careful - this may come from a thread!
	needToAutoSave = true;
}

// ----------------------------------------------------------------------------
//
// Get a filename from the user, and load the tracking points in the file
//
void CDewarpTool::LoadTrackingPointsFromFile(bool restoreMarks)
{
	string suggestedFilename = GetSuggestedTrackingDataLoadFilename();
	string filename = GetFilenameForLoadingTrackingPoints(suggestedFilename);
	if (filename.empty())
	{
		return;
	}

	LoadTrackingPoints(filename, restoreMarks);
	InvalidateSmoothing();
}

// ---------------------------------------------------------------------------
//
// SaveTrackingPoints
// Called by GUI in response to the tracking point Save button
//
bool CDewarpTool::SaveTrackingPoints(const string &sName)
{
	bool retVal = true;

	ofstream sFile(sName.c_str());
	if (!sFile.good())
	{
		TRACE_0(errout << "ERROR: Dewarp: Can't create tracking point file " << sName);
		retVal = false;
	}
	else
	{
		// Set the bounding box
		TrackingArray.setBoundingBox(GDewarpTool->getBoundingBox());
		TrackingArray.setUseBoundingBox(DewarpForm->UseMattingCBox->Checked);

		if (!(sFile << TrackingArray))
		{
			TRACE_0(errout << "ERROR: Dewarp: Can't save tracking points to file " << sName);
			retVal = false;
		}
		sFile.close();
	}

	return retVal; ;
}

// ----------------------------------------------------------------------------
//
// LoadTrackingPoints
// Called by GUI in response to the tracking point Load button
//
bool CDewarpTool::LoadTrackingPoints(const string &sName, bool restoreMarks)
{
	ifstream sFile(sName.c_str());

	if (!sFile.good())
	{
		TRACE_0(errout << "ERROR: Dewarp: Can't open tracking point file " << sName);
		return false;
	}

	// Clear out any garbage
	CTrackingArray TA;
	if (!(sFile >> TA))
	{
		TRACE_0(errout << "ERROR: Dewarp: Can't load tracking points from file " << sName);
		sFile.close();
		return false;
	}

	// Done with file I/O
	sFile.close();

	// Auto-save the current tracking data
	AutoSaveTrackingPoints();

	// Change tracking point region
	// This exists in Stabilizer but not in Dewarp:
	// EnableProcessingRegionGUI(TA.getUseBoundingBox());
	// replaced by:
	DewarpForm->UseMattingCBox->Checked = TA.getUseBoundingBox();
	/* */
	setBoundingBox(TA.getBoundingBox());

	if (TA.numberTrackPointsPerFrame() > 0)
	{
		if (restoreMarks)
		{
			int oldMarkIn = getSystemAPI()->getMarkIn();
			int oldMarkOut = getSystemAPI()->getMarkOut();

			getSystemAPI()->setMarkIn(TA.beginIndex());
			getSystemAPI()->setMarkOut(TA.endIndex());

			// these will be different if the in saved indexes are out of range
			int newMarkIn = getSystemAPI()->getMarkIn();
			int newMarkOut = getSystemAPI()->getMarkOut();

			// If either of the saved marks was out of range, we put back
			// the old marks, then we will move the saved track points to the
			// current mark IN frame, or the current frame if there is no IN

			if (newMarkIn != TA.beginIndex() || newMarkOut != TA.endIndex())
			{
				// Bad marks range for this clip!

				restoreMarks = false;
				getSystemAPI()->setMarkIn(oldMarkIn);
				getSystemAPI()->setMarkOut(oldMarkOut);
			}
			else
			{
				// Saved marks were successfully restored;
				// go to the new mark IN frame
				getSystemAPI()->goToFrameSynchronous(newMarkIn);

#if RESTORE_TRACK_DATA_ON_LOAD

				// Preserve complete tracking data if it's present
				if (TA.getColor() == CTrackingArray::ETPBlue || TA.getColor() == CTrackingArray::ETPGreen)
				{
					// Full tracking data is available! Woo hoo!
					TA.setColor(CTrackingArray::ETPGreen); // Never leave it blue
					// This exists in Stabilizer, but not in Dewarp:
					// NotifyTrackedCompleteRange();

					HowLongItTookToTrackInSecs = 0;
					ShowedInMarkMoveMessage = false;
					ShowedOutMarkMoveMessage = false;
					TrackingDataHasBeenUsedToRenderAll = false;
					bNeedToReTrack = false;
				}

#else
				TA.eraseAllBut(newMarkIn);

#endif
			}
		}

		// If we're not restoring the saved marks, we need to move the
		// tracking points to a different frame - current mark IN frame
		// or current frame if IN isn't set, and we need to erase all
		// tracking data other than the original points!

		if (!restoreMarks)
		{
			int markInFrame = getSystemAPI()->getMarkIn();
			if (markInFrame == -1)
			{
				// No IN mark - set it at the current frame
				markInFrame = getSystemAPI()->getLastFrameIndex();
				getSystemAPI()->setMarkIn(markInFrame);
				// We don't do anything with the OUT mark
			}
			else
			{
				// Show the mark IN frame so we can see the points
				getSystemAPI()->goToFrameSynchronous(markInFrame);
			}
			// Note: we may or may not have a valid OUT mark at this point....

			// Erase all the saved trackpoints and move them to the new mark IN
			TA.moveTrackingPointsToNewBeginIndex(markInFrame);

			// Fingers crossed!!
		}
	}

	// OTHER OPTIONS

	// 1. Ignore the tracking points
	// ResetTracking();

	// 2 Delete the saved tracking points
	// ResetTracking();
	// if (DoesFileExist(sName.c_str()))
	// if (!DeleteFile(sName.c_str()))
	// TRACE_0(errout << "ERROR: Stabilizer: Could not remove "
	// + sName.c_str());

	// 3. Move the tracking points to the new IN frame
	// CTrackingPoints tps = TA[TA.beginIndex()];
	// ResetTracking();
	// TA[getSystemAPI()->getMarkIn()] = tps;

	TA.setExtents(CurrentTrackBoxExtentX, CurrentTrackBoxExtentY, CurrentSearchBoxExtentLeft,
		 CurrentSearchBoxExtentRight, CurrentSearchBoxExtentUp, CurrentSearchBoxExtentDown);

	SetTrackingPoints(TA);
	// CAREFUL! TA was copied to real Tracking Array - DON'T REFER TO TA
	// ANYMORE AFTER THIS!

	TrkPtsEditor->lastSelectedTag = -1; // INVALID_FRAME_INDEX;
	TrkPtsEditor->refreshTrackingPointsFrame();
	DewarpForm->UpdateStatusBarInfo();
	UpdateExecutionGUI(TOOL_CONTROL_STATE_STOPPED, false);

	return true;
}
// ----------------------------------------------------------------------------

void CDewarpTool::DeleteSelectedTrackingPoints()
{
	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

	TrkPtsEditor->setCtrl(true);
	TrkPtsEditor->setKeyD(true);
	TrkPtsEditor->setKeyD(false);
	TrkPtsEditor->setCtrl(false);
	DewarpForm->UpdateTrackingPointButtons();
	DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
	InvalidateSmoothing();
	SelectTrackingPointRelative(0);
}
// ----------------------------------------------------------------------------

void CDewarpTool::ClearTrackingPoints()
{
	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

	GetTrackingPointsEditor()->resetTrackingArray();
	InvalidateSmoothing();
	SetTrackPointsVisibility(true); // per Larry
	GetTrackingPointsEditor()->refreshTrackingPointsFrame();
	DewarpForm->UpdateStatusBarInfo();
	DewarpForm->UpdateTrackingPointButtons();
	DewarpForm->UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}
// ----------------------------------------------------------------------------

bool CDewarpTool::isShiftOn()
{
	return shiftIsOn;
}
// -----------------GatherParameters-------------------------------------------

int CDewarpTool::GatherParameters(CDewarpToolParameters &toolParams)
{
	toolParams.SetRegionOfInterest(getSystemAPI()->GetMaskRoi());

	GatherGUIParameters(toolParams.dewarpParameters);
	_dewarpParameters = toolParams.dewarpParameters;
	return 0;
}

int CDewarpTool::CheckForAnyParameterChanges()
{
	CAutoErrorReporter autoErr("CDewarpTool::CheckForAnyParameterChanges", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// check Marks In/Out

	int iMarkIn = getSystemAPI()->getMarkIn();
	int iMarkOut = getSystemAPI()->getMarkOut();

	if (iMarkIn < 0 || iMarkOut < 0)
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "Set marks to designate the material to be processed";
		return -1;
	}

	if (iMarkIn >= iMarkOut)
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "Mark IN must be earlier than mark OUT";
		return -1;
	}

	// check index of single preview frame
	if (_previewFrameIndex >= 0)
	{
		if (_previewFrameIndex < iMarkIn || _previewFrameIndex > iMarkOut)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Frame must be between the marks";
			return -1;
		}
	}

	// if state is complete, change back to smooth the data
	if (GetControlState() == DEWARP_STATE_COMPLETED)
	{
		SetControlState(DEWARP_STATE_ALLOCATE);
	}

	return 0;
} // CheckForAnyParameterChanges()

int CDewarpTool::RunFrameProcessing(int newResumeFrame)
{

	int retVal;
	CAutoErrorReporter autoErr("CDewarpTool::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (IsDisabled())
	{
		autoErr.errorCode = -999;
		autoErr.msg << "Dewarp internal error, tried to run when disabled ";
		return autoErr.errorCode;
	}

	retVal = CheckForAnyParameterChanges();
	if (retVal != 0)
		return retVal; // params likely unset

	// (re-)allocate and advance the state (if necessary)
	if (_dewarpControlState == DEWARP_STATE_ALLOCATE)
	{
		if (IsNecessaryTrackDataAvailable() && !bNeedToReTrack)
		{
			_dewarpControlState = DEWARP_STATE_SMOOTH;
		}
		else
		{
			_dewarpControlState = DEWARP_STATE_TRACK;

			TrkPtsEditor->setRangeFromMarks();
			TrkPtsEditor->invalidateAllTrackingData();
			InvalidateSmoothing();

			// We want to remember how long tracking took so we can decide
			// later whether it's worth putting up annoying dialogs when
			// marks are moved
			TrackingTimer.Start();
		}
	}

	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == 0)
		return -1; // No image format, probably clip isn't loaded

   GpuAccessor gpuAccessor;
   if (_isGpuUsed != gpuAccessor.useGpu())
   {
      DestroyAllToolSetups(true);
      _isGpuUsed = gpuAccessor.useGpu();
   }

	// Check if we have a tool setup yet
	if (toolSetupHandle < 0)
	{
		// Create the tool setup
		CToolIOConfig ioConfig(1, 1);
		ioConfig.processingThreadCount = SysInfo::AvailableProcessorCoreCount();

		toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Dewarp", TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
		if (toolSetupHandle < 0)
		{
			autoErr.errorCode = -1;
			autoErr.msg << "Dewarp internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			return -1;
		}
	}

	// Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Dewarp internal error: SetActiveToolSetup(" << toolSetupHandle <<
			 ") failed with return code " << retVal;
		return retVal;
	}

	if (newResumeFrame < 0)
	{
		// Starting from Stop, so set processing frame range to in and out marks
		CToolFrameRange frameRange(1, 1);
		frameRange.inFrameRange[0].randomAccess = false;
		int markIn = getSystemAPI()->getMarkIn();
		int markOut = getSystemAPI()->getMarkOut();
		if (markIn < 0 || markOut < 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark In and Out must be set to Preview or Render";
			return -1;
		}
		else if (markOut <= markIn)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be beyond Mark In";
			return -1;
		}

		// Check to see if this is a preview frame
		if ((_previewFrameIndex >= 0) && (_dewarpControlState == DEWARP_STATE_SMOOTH))
		{
			// The tool processes between the good frames, so the range of the preview
			// must include them.

			// Good lord... WTF is this crap??
			// frameRange.inFrameRange[0].inFrameIndex = _previewFrameIndex - _dewarpParameters.GoodFramesIn;
			// frameRange.inFrameRange[0].outFrameIndex = markOut - _dewarpParameters.GoodFramesOut;
			// frameRange.inFrameRange[0].outFrameIndex = markOut;

			// Try this instead -- looks a lot saner!!!!!
			// frameRange.inFrameRange[0].inFrameIndex = _previewFrameIndex;
			// frameRange.inFrameRange[0].outFrameIndex = _previewFrameIndex + 1;
			// Hopefully now 'Preview 1' will really preview just 1 frame

			// WTF!!! That didn't work - the stupid renderer is applying
			// 'good frames' to our render!!! Work around the botch:
			frameRange.inFrameRange[0].inFrameIndex =
				 max<int>(markIn, _previewFrameIndex - _dewarpParameters.GoodFramesIn);
			frameRange.inFrameRange[0].outFrameIndex =
				 min<int>(markOut, _previewFrameIndex + 1 + _dewarpParameters.GoodFramesOut);
			// hahah i hope that works

		}
		else
		{
			frameRange.inFrameRange[0].inFrameIndex = markIn;
			// Different amount of processing needed
			// TTT          if (_dewarpControlState == DEWARP_STATE_TRACK)
			frameRange.inFrameRange[0].outFrameIndex = markOut;
			// TTT          else
			// TTT             frameRange.inFrameRange[0].outFrameIndex = markOut - _dewarpParameters.GoodFramesOut;
		}
		retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "Dewarp internal error: SetToolFrameRange" << " failed with return code " << retVal;
			return retVal;
		}
	}
	else
	{
		// Resuming from Pause state, so set processing frame range
		// from newResumeFrame to out mark
		CToolFrameRange frameRange(1, 1);
		frameRange.inFrameRange[0].randomAccess = false;
		int markOut = getSystemAPI()->getMarkOut();
		if (markOut < 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be set to Preview or Render";
			return -1;
		}
		else if (markOut <= newResumeFrame)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be beyond the Resume frame";
			return -1;
		}
		frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
		frameRange.inFrameRange[0].outFrameIndex = markOut;
		retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "Dewarp internal error: SetToolFrameRange" << " failed with return code " << retVal;
			return retVal;
		}
	}

	// Set render/preview flag for tool infrastructure.
	// Don't ever render during the acquire pass!
	if ((_dewarpControlState == DEWARP_STATE_TRACK) || !renderFlag)
		getSystemAPI()->SetProvisionalRender(false);
	else
		getSystemAPI()->SetProvisionalRender(true);

	// Set Dewarp's tool parameters
	CDewarpToolParameters *toolParams = new CDewarpToolParameters; // deleted by tool infrastructure
	GatherParameters(*toolParams);
	toolParams->dewarpToolObject = this;

	retVal = getSystemAPI()->SetToolParameters(toolParams);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Dewarp internal error: SetToolParameters" << " failed with return code " << retVal;
		return retVal;
	}

	// Start the image processing a-runnin'
	getSystemAPI()->RunActiveToolSetup();

	return 0;
} // RunFrameProcessing()

// ===================================================================
//
// Function:    MonitorFrameProcessing
//
// Description: Overrides MonitorFrameProcessing in CToolObject so that
// the different passes of dewarp can be monitored by the
// PDL rendering.  This function is very similar to the
// base version but has some key differences.
// ===================================================================
int CDewarpTool::MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus,
	 int newResumeFrame)
{
	int retVal = 0;

	bool updateGUI = false;
	EAutotoolStatus newToolProcessingStatus = newToolStatus.status;

	EToolControlState nextToolControlState;

	// Tool Processing Control State Machine
	nextToolControlState = toolProcessingControlState;
	switch (toolProcessingControlState)
	{
	case TOOL_CONTROL_STATE_STOPPED:
		if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
		{
			_previewFrameIndex = getSystemAPI()->getLastFrameIndex();
			retVal = ProcessTrainingFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW)
		{
			renderFlag = false;
			_previewFrameIndex = -1; // Not previewing a single frame
			retVal = RunFrameProcessing(newResumeFrame);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER)
		{
			renderFlag = true;
			_previewFrameIndex = -1; // Not previewing a single frame
			retVal = RunFrameProcessing(newResumeFrame);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}
		break;
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
		break;
	case TOOL_CONTROL_STATE_RUNNING:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			if (_dewarpControlState == DEWARP_STATE_TRACK)
			{
				// Even though the tool control state has not changed,
				// we still want to update the GUI to reflect the new
				// processing pass
				updateGUI = true;

				// Tell tracking points editor that we just finished tracking
				TrkPtsEditor->notifyTrackingComplete();
				bNeedToReTrack = false; // this is frickin' hideous

				// Remember how long it took
				HowLongItTookToTrackInSecs = int(TrackingTimer.Read() / 1000.0);
				ShowedInMarkMoveMessage = false;
				ShowedOutMarkMoveMessage = false;
				TrackingDataHasBeenUsedToRenderAll = false;

				if (bTrackOnly || !IsNecessaryTrackDataAvailable())
				{
					_dewarpControlState = DEWARP_STATE_COMPLETED;
					nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
				}
				else
				{
					// Acquire pass completed successfully;
					// Start Smoothing (then Render) after updating the GUI
					_dewarpControlState = DEWARP_STATE_SMOOTH;

					// Start next pass
					retVal = RunFrameProcessing(newResumeFrame);
					if (retVal != 0)
						nextToolControlState = TOOL_CONTROL_STATE_STOPPED;

				}
				// Store tracking array
				TrkPtsEditor->notifyTrackingComplete();
				if (!getSystemAPI()->IsPDLEntryLoading())
				{
					AutoSaveTrackingPoints();
				}
			}
			else
			{
				if (_dewarpControlState == DEWARP_STATE_RENDER)
				{
					_dewarpControlState = DEWARP_STATE_COMPLETED;
					TrkPtsEditor->notifyRenderComplete();

					// Per larry 2010-06, turn "Show tracking points" off,
					// and return to the begin frame
					SetTrackPointsVisibility(false);
					getSystemAPI()->goToFrameSynchronous(getSystemAPI()->getMarkIn());

					TrackingDataHasBeenUsedToRenderAll = true;
				}

				nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			if (_dewarpControlState == DEWARP_STATE_TRACK)
			{
				// return state to allocate (mainly for status bar)
				_dewarpControlState = DEWARP_STATE_ALLOCATE;
			}
			else if (_dewarpControlState == DEWARP_STATE_RENDER)
				_dewarpControlState = DEWARP_STATE_COMPLETED;

			retVal = StopFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PAUSE)
		{
			retVal = PauseFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
		}
		break;
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
		else if (newToolProcessingStatus == AT_STATUS_PAUSED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			retVal = StopFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		break;
	case TOOL_CONTROL_STATE_PAUSED:
		if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			retVal = StopFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_CONTINUE)
		{
			retVal = RunFrameProcessing(newResumeFrame);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
		{
			retVal = ProcessTrainingFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED;
			}
		}
		break;
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			if (_dewarpControlState == DEWARP_STATE_TRACK)
			{
				// Even though the tool control state has not changed,
				// we still want to update the GUI to reflect the new
				// processing pass
				updateGUI = true;

				// Acquire pass completed successfully;
				// Start Smoothing (then Render) after updating the GUI
				_dewarpControlState = DEWARP_STATE_SMOOTH;

				// Start next pass
				retVal = ProcessTrainingFrame();

				// Store tracking array
				TrkPtsEditor->notifyTrackingComplete();
			}
			else
			{
				// is this right for preview mode? I really have no clue!
				if (_dewarpControlState == DEWARP_STATE_RENDER)
					_dewarpControlState = DEWARP_STATE_COMPLETED;

				if (toolProcessingControlState == TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED)
					nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
				else
					nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
			}
		}
		break;

	default:
		break;
	}

	if (toolProcessingControlState != nextToolControlState)
	{
		// Control State has changed
		toolProcessingControlState = nextToolControlState;

		updateGUI = true;
	}

	if (updateGUI)
		UpdateExecutionGUI(toolProcessingControlState, renderFlag);

	return retVal;
}

int CDewarpTool::ProcessTrainingFrame()
{
	int retVal;

	// This is simply too evil for me to ignore!!!! Moved this to just before
	// the FIRST CALL because we don't want to keep resetting it! Sheesh.
	// _previewFrameIndex = getSystemAPI()->getLastFrameIndex();

	retVal = RunFrameProcessing(-1);
	if (retVal != 0)
		return retVal;

	return 0;
}

void CDewarpTool::DestroyAllToolSetups(bool notIfPaused)
{
	// DRS has three tool setups: DRS, Review Tool and Move Tool.
	// This function should not be called unless all of the tool setups
	// are stopped and there is no provisional pending.

	int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

	if (toolSetupHandle >= 0)
	{
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(toolSetupHandle) == AT_STATUS_PAUSED);
		if (!(notIfPaused && isPaused))
		{
			if (toolSetupHandle == activeToolSetup)
				getSystemAPI()->SetActiveToolSetup(-1);
			getSystemAPI()->DestroyToolSetup(toolSetupHandle);
			toolSetupHandle = -1;
		}
	}
} // DestroyAllToolSetups()

// ===================================================================
// Function:    UpdateExecutionGUI
// Description: Update the GUI according to the process control state from the
// tool infrastructure (overrides CToolObject version). Also use
// it to initiate another pass through the frames since Dewarp
// processing requires an an acquire pass followed by a render pass.
// Arguments: TI control state + render/preview flag.
// Returns:
// ===================================================================

void CDewarpTool::UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
	UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);

} // UpdateExecutionGUI()

void CDewarpTool::setBoundingBox(const RECT &newBox)
{
	if (IsInColorPickMode())
		ToggleColorPicker();
	setBoundingBoxInternal(newBox);
}
// ---------------------------------------------------------------------------

void CDewarpTool::setBoundingBoxInternal(const RECT &newBox)
{
	_BoundingBox = newBox;
	_BoundingBoxDefined = true;
	TrackingArray.setBoundingBox(newBox);
	getSystemAPI()->refreshFrameCached();
}
// ---------------------------------------------------------------------------

void CDewarpTool::readBoundingBoxFromDesktopIni(ClipSharedPtr &clip)
{
	CBinManager binManager;
	_BoundingBoxDefined = false; // yuck QQQ
	CIniFile *desktopIni = binManager.openClipDesktop(clip);
	const string iniSection = "BlueBoundingBox";
	if (desktopIni != nullptr)
	{
		RECT bbox;
		bbox.left = desktopIni->ReadInteger(iniSection, "Left", -1);
		bbox.top = desktopIni->ReadInteger(iniSection, "Top", -1);
		bbox.right = desktopIni->ReadInteger(iniSection, "Right", -1);
		bbox.bottom = desktopIni->ReadInteger(iniSection, "Bottom", -1);
		if (bbox.left >= 0 && bbox.top >= 0 && bbox.right > bbox.left && bbox.bottom > bbox.top)
		{
			setBoundingBox(bbox);
		}
		DeleteIniFileDiscardChanges(desktopIni);
	}
}
// ---------------------------------------------------------------------------

void CDewarpTool::writeBoundingBoxToDesktopIni(ClipSharedPtr &clip)
{
	CBinManager binManager;
	CIniFile *desktopIni = binManager.openClipDesktop(clip);
	const string iniSection = "BlueBoundingBox";
	if (desktopIni != nullptr)
	{
		if (_BoundingBoxDefined)
		{
			RECT bbox = GDewarpTool->getBoundingBox();
			desktopIni->WriteInteger(iniSection, "Left", bbox.left);
			desktopIni->WriteInteger(iniSection, "Top", bbox.top);
			desktopIni->WriteInteger(iniSection, "Right", bbox.right);
			desktopIni->WriteInteger(iniSection, "Bottom", bbox.bottom);
		}
		else
		{
			desktopIni->EraseSection(iniSection);
		}
		DeleteIniFile(desktopIni);
	}
}

void CDewarpTool::setDisplayRect(const bool newDisp)
{
	if (_displayRect == newDisp)
		return;

	_displayRect = newDisp;
	getSystemAPI()->refreshFrameCached();
}

bool CDewarpTool::getDisplayTrackingPoints(void)
{
	return TrkPtsEditor->displayTrackingPoints;
}

void CDewarpTool::setDisplayTrackingPoints(const bool newDisp)
{
	TrkPtsEditor->displayTrackingPoints = newDisp;
}

// ------------------------SmoothTrackingPoints----------------------Apr 2006---

bool CDewarpTool::SmoothTrackingPoints(int iGoodStart, int iGoodEnd, double Alpha)
{
	CBusyCursor busyCursor(true);
	double ActualAlpha;

	// Don't smooth bogus data
	if (!isAllTrackDataValid())
	{
		// Clean out smoothed array
		SmoothedArray.clear();
		return false;
	}

	// Don't smooth if already smooth
	if (_smoothDataIsValid)
		return true;

	// Collapse the array - returns the size of the collapsed array
	if (!TrackingArray.collapse())
	{
		theError.set(DEWARP_ERROR_TRACKING_COLLAPSE, "Internal Error: A frame is missing a tracking point");
		setStatusMessage("Error");
		return false;
	}

	// This may have to change
	int NumberOfFrames = TrackingArray.endIndex() - TrackingArray.beginIndex();

	if (NumberOfFrames <= (iGoodStart + iGoodEnd))
	{
		theError.set(DEWARP_ERROR_SMOOTHING_TOO_FEW_FRAMES,
			 "Operation failed: To render this shot, the marked frame range must be greater than (At In + At Out)");
		setStatusMessage("Error");
		return false;
	}

	std::valarray<double>xTP(NumberOfFrames);
	std::valarray<double>yTP(NumberOfFrames);

	SmoothedArray = TrackingArray;
	int NumOfPointsPerFrame = TrackingArray.numberTrackPointsPerFrame();
	// Was: TrackingArray[TrackingArray.beginIndex()].size();

	for (int iTP = 0; iTP < NumOfPointsPerFrame; iTP++)
	{
		CTrackingPoints sTP = TrackingArray.TSlice(iTP);

		// Get them
		int k = 0;

		for (int i = sTP.beginIndex(); i < sTP.endIndex(); i++)
		{
			xTP[k] = sTP[i].x;
			yTP[k] = sTP[i].y;
			k++;
		}

		// Decide to use the global or individual tracking alpha
		if (TrackingArray.getSmoothingEnabled(iTP))
		{
			// Individual smoothing
			if (TrackingArray.getSmoothing(iTP) == SMOOTHING_FLAT)
				ActualAlpha = 0;
			else if (TrackingArray.getSmoothing(iTP) == SMOOTHING_NONE)
				ActualAlpha = -1;
			else
				ActualAlpha = TrackingArray.getSmoothingAlpha(iTP);
		}
		else
			ActualAlpha = Alpha;

		if (!Smooth3(xTP, iGoodStart, iGoodEnd, ActualAlpha))
		{
			setStatusMessage("");
			return false;
		}
		if (!Smooth3(yTP, iGoodStart, iGoodEnd, ActualAlpha))
		{
			setStatusMessage("");
			return false;
		}

		// Put them into the smoothed array
		for (int iFrame = TrackingArray.beginIndex(); iFrame < TrackingArray.endIndex(); iFrame++)
		{
			CTrackPoint newTP(xTP[iFrame - TrackingArray.beginIndex()], yTP[iFrame - TrackingArray.beginIndex()]);
			SmoothedArray.at(iFrame).at(iTP) = newTP;
		}

	}
	setStatusMessage("");
	setTimeMessage("");

	_smoothDataIsValid = true;

	return true;
}

// ------------------------BoundingBoxDefined-------------------------Oct 2006---

bool CDewarpTool::BoundingBoxDefined(void)
{
	return _BoundingBoxDefined;
}

// -------------------SetClearTrackingStuffOnNewShot------------------Jun 2010---

void CDewarpTool::SetClearTrackingStuffOnNewShot(bool flag)
{
	_clearTrackingStuffOnNewShot = flag;
}

// ---------------------------canMarksBeMoved-------------------------Oct 2006---

bool CDewarpTool::canMarksBeMoved()
{
	// Although return canInMarkBeMoved() & canOutMakeBeMoved(); should work
	// we want to make sure that some optimizer doesn't remove the second call
	// if the first one fails.

	if (!canInMarkBeMoved())
		return false;
	return canOutMarkBeMoved();
}

// ---------------------------canInMarkBeMoved------------------------Oct 2006---

bool CDewarpTool::canInMarkBeMoved()
{

	// MAJOR LEAGUE KLUDGE  TTT
	// Because of the way the notifications are process, a mark change is done while
	// the clip is being loaded and before the ClipHasChange is called.  Thus the
	// dewarp thinks the marks come from the old clip, not the new one.
	// To fix this, one would need an AboutToChangeClip Notification, but for now
	// attach a name to a set of tracking points, if the names don't match assume
	// we have a clip change
	//
	if (getSystemAPI()->getClipFilename() != _CurrentClipName)
		return true;
	//
	// END MAJOR LEAGUE KLUDGE TTT

	// ONE GOOD HACK DESERVES ANOTHER
	// Because Dewarp is sensitive to the order setting marks and tracking
	// points, it gets confused when loading a PDL entry.
	// To avoid this problem, Dewarp needs to know when a PDL Entry
	// is being loaded (as opposed to being setup by a human).
	if (getSystemAPI()->IsPDLEntryLoading())
		return true;
	//
	// END OF ANOTHER HACK

	switch (TrkPtsEditor->canInMarkChange())
	{
		// We do nothing if marks can change
	case TRK_IN_MARK_CAN_CHANGE:
		return true;

	case TRK_IN_MARK_IS_DELETED:
		if ((isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll)) || (TrackingArray.numberTrackPointsPerFrame() > 1))
		{
			if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
				 "All tracking points will be deleted " "if you delete your IN mark.    \nContinue anyway?"))
			{
				// Undo mark in move
				getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
				return false;
			}
		}
		return true;

	case TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL:

		///////////////////////////////////////////////////////////////////
		// Hack added per Larry 2010-06
		// If we think the user is trying to go to a new shot, clear all the
		// tracking stuff if the "Clear on new shot" checkbox is set;
		// "new shot" is heeby defined as moving the IN mark to someplace
		// OUTSIDE of the previous IN-OUT range!
		//
		if (_clearTrackingStuffOnNewShot)
		{
			ClearTrackingPoints();
			// Added benefit of this mode is to not annoy you with a dialog!
			return true;
		}
		else if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll))
		{
			if (!ShowedInMarkMoveMessage)
			{
				if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
					 "All tracking data will be lost " "if you move your IN mark.    \nContinue anyway?"))
				{
					// Undo mark in move
					getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
					return false;
				}
				else
				{
					ShowedInMarkMoveMessage = true;
				}
			}
		}

		// TrackingEditor will move the points to the new mark
		return true;

	case TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL:

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll))
		{
			if (!ShowedInMarkMoveMessage)
			{
				if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
					 "Some tracking data will be lost " "if you move your IN mark.    \nContinue anyway?"))
				{
					// Undo mark in move
					getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
					return false;
				}
				else
				{
					ShowedInMarkMoveMessage = true;
				}
			}
		}

		// TrackingEditor will erase tracking data before the new
		// mark if tracking data is valid, else to move the points
		// to the new mark

#endif // !DECOUPLE_MARKS_FROM_TRACKED_RANGE

		return true;
	}

	return true;
}

// --------------------------canOutMarkBeMoved------------------------Oct 2006---

bool CDewarpTool::canOutMarkBeMoved()
{

	// MAJOR LEAGUE KLUDGE  TTT
	// Because of the way the notifications are process, a mark change is done while
	// the clip is being loaded and before the ClipHasChange is called.  Thus the
	// dewarp thinks the marks come from the old clip, not the new one.
	// To fix this, one would need an AboutToChangeClip Notification, but for now
	// attach a name to a set of tracking points, if the names don't match assume
	// we have a clip change
	//
	if (getSystemAPI()->getClipFilename() != _CurrentClipName)
		return true;
	//
	// END MAJOR LEAGUE KLUDGE TTT

	// ONE GOOD HACK DESERVES ANOTHER
	// Because Dewarp is sensitive to the order setting marks and tracking
	// points, it gets confused when loading a PDL entry.
	// To avoid this problem, Dewarp needs to know when a PDL Entry
	// is being loaded (as opposed to being setup by a human).
	if (getSystemAPI()->IsPDLEntryLoading())
		return true;

	switch (TrkPtsEditor->canOutMarkChange())
	{
		// We do nothing if marks can change
	case TRK_OUT_MARK_CAN_CHANGE:
		return true;

	case TRK_OUT_MARK_IS_DELETED:
		if (!_WarningDialogs)
			return true;

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll))
		{
			switch (_MTIConfirmationDialog(Handle,
				 "Tracking information will be lost if you delete your OUT mark.\nContinue anyway?"))
			{
			case MTI_DLG_OK:
				return true;

			case MTI_DLG_CANCEL:
				getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
				return false;
			}
		}
		return true;

	case TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL:
		if (!_WarningDialogs)
			return true;

		// Ask the user
		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll))
		{
			switch (_MTIConfirmationDialog(Handle, "All points will need to be re-tracked.    \nContinue anyway?"))
			{
			case MTI_DLG_OK:
				ShowedOutMarkMoveMessage = true;
				return true;

			case MTI_DLG_CANCEL:
				getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
				return false;
			}
		}
		return true;

	case TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL:

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE

		// Handle without asking
		if (!_WarningDialogs)
			return true;

		// Ask the user

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
			 (!TrackingDataHasBeenUsedToRenderAll))
		{
			switch (_MTIConfirmationDialog(Handle, "Tracking data will be truncated to new OUT mark.\nContinue anyway?"))
			{
			case MTI_DLG_OK:
				ShowedOutMarkMoveMessage = true;
				return true;

			case MTI_DLG_CANCEL:
				getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
				return false;
			}
		}

#endif // !DECOUPLE_MARKS_FROM_TRACKED_RANGE

		return true;
	}

	return true;
}

// --------------------------ResetLastSelected------------------------Oct 2006---

void CDewarpTool::ResetLastSelected(void)

	 // Just check to see if we are still selected, if not, deselect last selected
{
	TrkPtsEditor->resetLastSelected();
}

// ----------------------------------------------------------------------------
//
// See if any tracked point in TrackingArray has changed since the last
// Track operation; compare the array slice representing the point through
// time with a snapshot taken after the last Track operation. I hate this.
//
bool CDewarpTool::DoesTrackingPointNeedToBeRetracked(int nTag)
{
	return GetTrackingPointsEditor()->doesTrackingPointNeedToBeRetracked(nTag);
}

// ----------------------------------------------------------------------------
//
void CDewarpTool::InvalidateSmoothing()
{
	_smoothDataIsValid = false;
}

// ----------------------------------------------------------------------------
//
// SelectTrackingPointRelative
// Called by GUI in response to the tracking point Prev/Next buttons
//
void CDewarpTool::SelectTrackingPointRelative(int plusOrMinus)
{
	if (TrackingArray.numberTrackPointsPerFrame() < 1)
		return;

	int maxTag = TrackingArray.at(TrackingArray.beginIndex()).size() - 1;
	int currentTag = GetTrackingPointsEditor()->lastSelectedTag + plusOrMinus;

	if (currentTag > maxTag)
		currentTag = 0;
	if (currentTag < 0)
		currentTag = maxTag;

	GetTrackingPointsEditor()->setUniqueSelection(currentTag);
}
// ----------------------------------------------------------------------------

void CDewarpTool::ColorPickerColorChanged(void *Sender)
{
	// We don't do anything here yet
}
// ----------------------------------------------------------------------------

void CDewarpTool::ColorPickerColorPicked(void *Sender)
{
	int R, G, B, X, Y;
	ColorPickTool->getColor(R, G, B);
	ColorPickTool->getXY(X, Y);
	FindBoundingBox(R, G, B, X, Y);
}
// ----------------------------------------------------------------------------

void CDewarpTool::EnterColorPickMode(void)
{
	ColorPickTool->SetEnabled(true);
}
// ----------------------------------------------------------------------------

void CDewarpTool::ExitColorPickMode(void)
{
	ColorPickTool->SetEnabled(false);
}
// ----------------------------------------------------------------------------

bool CDewarpTool::IsInColorPickMode(void)
{
	return ColorPickTool->IsEnabled();
}
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CDewarpProc::CDewarpProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
	 const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor)
	 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor),
	 maskROI(0)
{
	_TrackBoxData = nullptr;
	_SearchBoxData = nullptr;
	_dewarpToolObject = nullptr;
	_previousFixedImage = nullptr;
	_MonoImages[0] = nullptr;
	_MonoImages[1] = nullptr;
	_MonoImageIndex = -1;
	StartProcessThread(); // Required by Tool Infrastructure
}

CDewarpProc::~CDewarpProc()
{
}

float(*UEnergy)(float X0, float Y0, float X1, float Y1);

float UEnergyPoint(const DPOINT &iDP, const DPOINT &jDP)
{
	return UEnergy(iDP.x, iDP.y, jDP.x, jDP.y);
}

int CDewarpProc::DoProcessRenderGpu(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("CDewarpProc::DoProcessGpu", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
	_BaseOrigImage = inToolFrameBufferB->GetVisibleFrameBufferPtr();
	CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
	_outputImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();

	auto imageFormat = GetSrcImageFormat(0);
	auto width = int(imageFormat->getPixelsPerLine());
	auto height = int(imageFormat->getLinesPerFrame());

	Ipp16uArray imageIn({width, height, 3}, _BaseOrigImage);
	Ipp16uArray imageOut({width, height, 3}, _outputImage);

   _imageDataIsHalfs = inToolFrameBufferB->ContainsFloatData();

	// mbraca added this crap on 2008-12-04 to fix "invisible fields
	// getting overwritten with totally unrelated fields" bug!
	// This is pretty stupid... we don't actually want to copy anything
	// here because the full field will get copied to the invisible field
	// after the render, but we need the side effect that sets the output
	// buffer to have the same number of invisible fields as the input buffer
	CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);

	// Set the bounding box
	if (procData.iteration == 0)
	{
		if (!_dewarpParameters.UseMatting)
			_BoundingBox = _ActiveBox;
		else
			_BoundingBox = _dewarpToolObject->getBoundingBox();

      IppiRect roi = {_BoundingBox.left, _BoundingBox.top, _BoundingBox.right - _BoundingBox.left, _BoundingBox.bottom - _BoundingBox.top};
      _thinplateSplineWarper->setInputRoi(roi);
      _thinplateSplineWarper->setOutputRoi(roi);
	}

	// Here comes the ugly part, we must get the last good start frame, if one
	// exists, otherwise color with red.  None of this counts if we don't want
	// fill in missing data
	// UPDATE: Got rid of red, just fill first frame with the original
	// undewarped data
	if (_dewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
	{
		if ((_dewarpParameters.GoodFramesIn <= 0) && (inToolFrameBufferB->GetFrameIndex() == _inFrameIndex))
		{
			// Initialize the previous image buffer
			_thinplateSplineWarper->setAndLoadAncillaryArrayAsync(imageIn);
			_thinplateSplineWarper->wait();
		}
		else
		{
			if (inToolFrameBufferB->GetFrameIndex() == (_inFrameIndex + _dewarpParameters.GoodFramesIn - 1))
			{
				// Initialize the previous image buffer
				_thinplateSplineWarper->setAndLoadAncillaryArrayAsync(imageIn);
				_thinplateSplineWarper->wait();
			}
		}
	}

   // ugly code to translate Nova fill type into cu fill type
   int fill = -1;
   switch (_dewarpParameters.MissingDataFill)
   {
	   case MISSING_DATA_WHITE:
  		fill = _WhitePixel[0];
      break;

      case MISSING_DATA_BLACK:
		fill = _BlackPixel[0];
      break;

      case MISSING_DATA_PREVIOUS:
      fill = -1;
      break;

      default:
      fill = -1;
   }

	// We do not want to process the good frames
	if (procData.iteration < _dewarpParameters.GoodFramesIn)
	{
		outToolFrameBuffer->SetForceWrite(false);
	}
	else
	{
		// if a frame dewarp fails, then we must abort the processing
		if (inToolFrameBufferB->GetFrameIndex() >= (_outFrameIndex - _dewarpParameters.GoodFramesOut))
		{
			// this is a physical copy
			imageOut <<= imageIn;
		}
		else
		{
         auto Frame = outToolFrameBuffer->GetFrameIndex();
         CTrackingPoints tpO = _dewarpToolObject->TrackingArray.at(Frame);
         CTrackingPoints tpS = _dewarpToolObject->SmoothedArray.at(Frame);
         vector<IppiPoint_32f> cpO;
         vector<IppiPoint_32f> cpS;

			for (auto i = 0; i < tpO.size(); i++)
			{
				cpO.push_back({float(tpO[i].x), float(tpO[i].y)});
				cpS.push_back({float(tpS[i].x), float(tpS[i].y)});
			}

         vector<double>VWx;
         vector<double>VWy;

         // Compute the warp values
         if (!MtiMkl::SolveForWeights(cpS, cpO, VWx, VWy))
         {
            autoErr.traceAction = AUTO_ERROR_NO_TRACE;
            autoErr.errorCode = theError.getError();
            autoErr.msg << "Dewarp frame failed: " << theError.getMessage();
            return -1;
         }

         vector<vector<float>> weights(2);
         for (auto i = 0; i < VWx.size(); i++)
         {
            weights[0].push_back(float(VWx[i]));
            weights[1].push_back(float(VWy[i]));
         }

         _thinplateSplineWarper->warpLocation(imageIn, imageOut, weights, cpS, fill, 2);
		}

		// Smart way to force write to buffer
		outToolFrameBuffer->SetForceWrite(true);

		// mbraca added this crap on 2008-12-04 to fix "invisible fields
		// getting overwritten with totally unrelated fields" bug!
		// This is pretty stupid... the Tool Infrastructure uses the
		// "original values" region list to figure out what pixels to
		// copy to the invisible field from the appropriate visible field.
		// No original pixels are involved because the tool does not
		// preserve history!! If you don't do this, the invisible field
		// would be written with whatever was sitting in the buffer (often
		// the last invisible field in the marked range from running a
		// different tool)!
		//
		const CImageFormat *imageFormat = GetSrcImageFormat(0);
		CPixelRegionList* originalValues = outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr();
		originalValues->Add(_ActiveBox, // Always copy the full frame
			 nullptr, // Nothing to save
			 imageFormat->getPixelsPerLine(), imageFormat->getLinesPerFrame(), imageFormat->getComponentCountExcAlpha());
	}

	return 0;
}

int CDewarpProc::DoProcess(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("CDewarpProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	long lFrame, lRet = 0;

	// TRACE_0(errout << "TTT " << "Enter DoProcess");
	unsigned long dewarpControlState = _dewarpToolObject->GetControlState();
	if (dewarpControlState == DEWARP_STATE_TRACK)
	{
		if (_iterationCount != (procData.iteration + 1))
		{
			CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
			CToolFrameBuffer *inToolFrameBufferN = GetRequestedInputFrame(0, 1);
			_BaseOrigImage = inToolFrameBufferB->GetVisibleFrameBufferPtr();
			if (_BaseOrigImage == nullptr)
				return -8006;
			_NextOrigImage = inToolFrameBufferN->GetVisibleFrameBufferPtr();
			if (_NextOrigImage == nullptr)
				return -8006;

			TrackNextFrame(inToolFrameBufferB->GetFrameIndex());

			CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
			_outputImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();
			// Create an output frame
			CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);
		}
		else
		{
			CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
			CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
			// Create an output frame
			CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);
		}
	}
	else if (dewarpControlState == DEWARP_STATE_RENDER)
	{
		GpuAccessor gpuAccessor;
		if (gpuAccessor.useGpu())
		{
         try
			{
         	return DoProcessRenderGpu(procData);
         }
         catch (const std::exception &ex)
         {
            TRACE_0(errout << "DoProcessRenderGpu failed: " << ex.what());
            return -1;
         }
         catch (...)
         {
            TRACE_0(errout << "DoProcessRenderGpu failed because of unknown error");
            return -1;
         }
		}

		CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
		_BaseOrigImage = inToolFrameBufferB->GetVisibleFrameBufferPtr();
		CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
		_outputImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();

		_imageDataIsHalfs = inToolFrameBufferB->ContainsFloatData();

		// mbraca added this crap on 2008-12-04 to fix "invisible fields
		// getting overwritten with totally unrelated fields" bug!
		// This is pretty stupid... we don't actually want to copy anything
		// here because the full field will get copied to the invisible field
		// after the render, but we need the side effect that sets the output
		// buffer to have the same number of invisible fields as the input buffer
		CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);

		// Set the bounding box
		if (procData.iteration == 0)
		{
			if (!_dewarpParameters.UseMatting)
				_BoundingBox = _ActiveBox;
			else
				_BoundingBox = _dewarpToolObject->getBoundingBox();
		}

		// Here comes the ugly part, we must get the last good start frame, if one
		// exists, otherwise color with red.  None of this counts if we don't want
		// fill in missing data
		// UPDATE: Got rid of red, just fill first frame with the original
		// undewarped data
		if (_dewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
		{
			if ((_dewarpParameters.GoodFramesIn <= 0) && (inToolFrameBufferB->GetFrameIndex() == _inFrameIndex))
			{
#ifdef FILL_MISSING_DATA_WITH_RED
				// Fill with red
				MTI_UINT16 *pOrig = _previousFixedImage;
				if (_pxlComponentCount == 3)
				{
					for (int i = 0; i < nPicRow * nPicCol; i++)
					{
						*pOrig++ = 1023;
						*pOrig++ = 0;
						*pOrig++ = 0;
					}
				}
				else
				{
					// Fill with black
					for (int i = 0; i < nPicRow * nPicCol; i++)
						for (int j = 0; j < _pxlComponentCount; j++)
						{
							*pOrig++ = 0;
						}
				}
#else
				// Fill woth original data
				MTImemcpy(_previousFixedImage, _BaseOrigImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
#endif
			}
			else
			{
				if (inToolFrameBufferB->GetFrameIndex() == (_inFrameIndex + _dewarpParameters.GoodFramesIn - 1))
				{
					MTImemcpy(_previousFixedImage, _BaseOrigImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
				}
			}
		}

		// We do not want to process the good frames
		if (procData.iteration < _dewarpParameters.GoodFramesIn)
		{
			outToolFrameBuffer->SetForceWrite(false);
		}
		else
		{
			// if a frame dewarp fails, then we must abort the processing
			if (inToolFrameBufferB->GetFrameIndex() >= (_outFrameIndex - _dewarpParameters.GoodFramesOut))
			{
				MTImemcpy(_outputImage, _BaseOrigImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
			}

			else if (!DewarpFrame(outToolFrameBuffer->GetFrameIndex()))
			{
				return -1;
			}

			// Copy fixed data over for more information
			if (_dewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
				MTImemcpy(_previousFixedImage, _outputImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));

			// Smart way to force write to buffer
			outToolFrameBuffer->SetForceWrite(true);

			// mbraca added this crap on 2008-12-04 to fix "invisible fields
			// getting overwritten with totally unrelated fields" bug!
			// This is pretty stupid... the Tool Infrastructure uses the
			// "original values" region list to figure out what pixels to
			// copy to the invisible field from the appropriate visible field.
			// No original pixels are involved because the tool does not
			// preserve history!! If you don't do this, the invisible field
			// would be written with whatever was sitting in the buffer (often
			// the last invisible field in the marked range from running a
			// different tool)!
			//
			const CImageFormat *imageFormat = GetSrcImageFormat(0);
			CPixelRegionList* originalValues = outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr();
			originalValues->Add(_ActiveBox, // Always copy the full frame
				 nullptr, // Nothing to save
				 imageFormat->getPixelsPerLine(), imageFormat->getLinesPerFrame(),
				 imageFormat->getComponentCountExcAlpha());
			//
		}
	}
	// TRACE_0(errout << "TTT " << "Exit DoProcess");
	return lRet;
}

int CDewarpProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	// Make sure the number of the algorithm processing threads
	// is reasonable
	_processingThreadCount = toolIOConfig->processingThreadCount;
	if (_processingThreadCount < 1)
		_processingThreadCount = 1;
	else if (_processingThreadCount > DWRP_MAX_THREAD_COUNT)
		_processingThreadCount = DWRP_MAX_THREAD_COUNT;

	// Tell Tool Infrastructure, via CToolProcessor supertype, about the
	// number of input and output frames
	SetInputMaxFrames(0, 2, 2);
	SetOutputMaxFrames(0, 1);
	SetOutputNewFrame(0, 0);

	return 0;
}

int CDewarpProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
	_inFrameIndex = frameRange.inFrameIndex;
	_outFrameIndex = frameRange.outFrameIndex;

	return 0;
}

int CDewarpProc::SetParameters(CToolParameters *toolParams)
{
	int retVal = 0;

	CDewarpToolParameters *dewarpToolParams = static_cast<CDewarpToolParameters*>(toolParams);

	maskROI = toolParams->GetRegionOfInterest();

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_dewarpParameters = dewarpToolParams->dewarpParameters;
	_dewarpToolObject = dewarpToolParams->dewarpToolObject;
	UEnergy = MtiMkl::ULogrithmic;
	// Set the dewarp function

	// Find the iteration count
	_iterationCount = _outFrameIndex - _inFrameIndex;

	// Done
	return retVal;
}

int CDewarpProc::BeginProcessing(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("CDewarpProc::BeginProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   GpuAccessor gpuAccessor;

	int retVal = 0;
	int nTmp;

	// Dewarp does not save to history, since it is a "full frame" tool
	SetSaveToHistory(false);

	// Just save some values for later

	const CImageFormat *imageFormat = GetSrcImageFormat(0);
	_pxlComponentCount = imageFormat->getComponentCountExcAlpha();
	nPicCol = imageFormat->getPixelsPerLine();
	nPicRow = imageFormat->getLinesPerFrame();
	imageFormat->getComponentValueBlackFill(_BlackPixel);
	imageFormat->getComponentValueWhiteFill(_WhitePixel);
	_RGB = (imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB) ||
		 (imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA) ||
		 (imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_Y) ||
		 (imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY);

	_ActiveBox = imageFormat->getActivePictureRect();

	switch (_dewarpToolObject->GetControlState())
	{
	case DEWARP_STATE_TRACK:
		// Allocate some useful memory for tracking
		_TrackBoxExtent = _dewarpParameters.TrackBoxExtent;
		_TrackBoxSize = 2 * _TrackBoxExtent + 1;
		_TrackBoxPoints = _TrackBoxSize * _TrackBoxSize;

		// Boxes must be bigger because we need a point to perform a binlinear interpolation
		nTmp = 2 * (_TrackBoxExtent + 1) + 1;
		delete[]_TrackBoxData;
		_TrackBoxData = new Pixel[nTmp * nTmp];

		_SearchBoxExtent = _dewarpParameters.SearchBoxExtent + _TrackBoxExtent;
		_SearchBoxSize = 2 * _SearchBoxExtent + 1;
		_SearchBoxPoints = _SearchBoxSize * _SearchBoxSize;

		// Boxes must be bigger because we need point to perform a binlinear interpolation
		nTmp = 2 * (_SearchBoxExtent + 1) + 1;
		delete[]_SearchBoxData;
		_SearchBoxData = new Pixel[nTmp * nTmp];

		// Copy the tracking points to start
		TrueTrack = GDewarpTool->TrackingArray.at(_inFrameIndex);

		// Kill all tracking points but the start
		GDewarpTool->TrackingArray.eraseAllBut(_inFrameIndex);
		GDewarpTool->TrackingArray.setColor(CTrackingArray::ETPRed);

		// Now say we must alloc the monochrome images
		_MonoImageIndex = -1;
		_AnchorList = GDewarpTool->TrackingArray.CreateAnchorTree();

		GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
		GetToolProgressMonitor()->SetStatusMessage("Tracking");
		GetToolProgressMonitor()->StartProgress();

		_dewarpToolObject->setStatusMessage("Tracking...");
		break;

	case DEWARP_STATE_SMOOTH:
		// Smooth the tracking points
		GetToolProgressMonitor()->SetStatusMessage("Smoothing");
		if (!_dewarpToolObject->SmoothTrackingPoints(_dewarpParameters.GoodFramesIn, _dewarpParameters.GoodFramesOut,
			 _dewarpParameters.Alpha2))
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = theError.getError();
			autoErr.msg << "Smoothing failed: " << theError.getMessage();
			_dewarpToolObject->SetControlState(DEWARP_STATE_COMPLETED);
			GetToolProgressMonitor()->SetStatusMessage("Smoothing failed");
			return -1;
		}

		_dewarpToolObject->SetControlState(DEWARP_STATE_RENDER);

		// Now allocate a data buffer for the fill in
		if (_dewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
		{
			_previousFixedImage = new MTI_UINT16[_pxlComponentCount * nPicRow * nPicCol];
		}

		if (gpuAccessor.useGpu())
		{
			delete _thinplateSplineWarper;

			_thinplateSplineWarper = new CudaThinplateSpline16u(gpuAccessor.getDefaultGpu());
			auto imageFormat = GetSrcImageFormat(0);
			MTI_UINT16 maxValues[3], minValues[3];
			imageFormat->getComponentValueMax(&maxValues[0]);
			imageFormat->getComponentValueMin(&minValues[0]);
			_thinplateSplineWarper->setMinMax(minValues[0], maxValues[0]);
		}

		GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
		GetToolProgressMonitor()->SetStatusMessage("Rendering");
		GetToolProgressMonitor()->StartProgress();

		_dewarpToolObject->setStatusMessage("Rendering");
		break;

		// Debugging, should never happen
	default:
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = _dewarpToolObject->GetControlState();
		autoErr.msg << "Internal error Illegal dewarp state";
		retVal = -1;
		break;
	}

	// Success, continue
	return retVal;
}

int CDewarpProc::EndProcessing(SToolProcessingData &procData)
{

	switch (_dewarpToolObject->GetControlState())
	{
	case DEWARP_STATE_TRACK:
		delete[]_MonoImages[0];
		_MonoImages[0] = nullptr;
		delete[]_MonoImages[1];
		_MonoImages[1] = nullptr;
		_MonoImageIndex = -1; ;

		delete[]_TrackBoxData;
		_TrackBoxData = nullptr;

		delete[]_SearchBoxData;
		_SearchBoxData = nullptr;

		// Save some data
		_dewarpToolObject->ProcessedSearchBoxExtent = _dewarpParameters.SearchBoxExtent;
		_dewarpToolObject->ProcessedTrackBoxExtent = _dewarpParameters.TrackBoxExtent;

		// Tracking is complete, process it
		_dewarpToolObject->TrackingArray.setAllTrackedStatus(tpsVALID);
		break;

	case DEWARP_STATE_RENDER:
		delete[]_previousFixedImage;
		_previousFixedImage = nullptr;

		// Clean up any GPU
		delete _thinplateSplineWarper;
		_thinplateSplineWarper = nullptr;

       _dewarpToolObject->setStatusMessage("Render Completed");

		break;

	default:
		break;
	}

	// Did render run to completion or was it aborted?
	if (procData.iteration == _iterationCount)
	{
		GetToolProgressMonitor()->SetStatusMessage((_dewarpToolObject->GetControlState() == DEWARP_STATE_RENDER) ?
			 "Render complete" : "Tracking complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	return 0;
}

int CDewarpProc::BeginIteration(SToolProcessingData &procData)
{
	int inputFrameIndexList[2];

	// Set the status
	switch (_dewarpToolObject->GetControlState())
	{
	case DEWARP_STATE_TRACK:
		if (_iterationCount != (procData.iteration + 1))
		{
			inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
			inputFrameIndexList[1] = _inFrameIndex + procData.iteration + 1;
			// inputFrameIndexList now contains the frame indices we want as input
			SetInputFrames(0, 2, inputFrameIndexList);
		}
		else
		{
			inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
			// inputFrameIndexList now contains the frame indices we want as input
			SetInputFrames(0, 1, inputFrameIndexList);
		}
		break;

	case DEWARP_STATE_RENDER:
		inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
		SetInputFrames(0, 1, inputFrameIndexList);
		break;

	default:
		break;
	}

	// These are the frames that will be the output

	int outputFrameIndexList[1];
	outputFrameIndexList[0] = _inFrameIndex + procData.iteration;
	SetOutputFrames(0, 1, outputFrameIndexList);

	// When finished processing, release the oldest frame.
	SetFramesToRelease(0, 1, inputFrameIndexList);

	return 0;
}

int CDewarpProc::EndIteration(SToolProcessingData &procData)
{
#if 0 // good lord, what a crock
	MTIostringstream os;
	int percentDone;

	// Set the status
	switch (_dewarpToolObject->GetControlState())
	{
	case DEWARP_STATE_TRACK:
		percentDone = (int)(100.0 * (procData.iteration) / (_iterationCount - 1));
		if (percentDone > 100)
			percentDone = 100;
		os << percentDone << "% done, " << std::fixed << setprecision(2) << HRT.Read() / (procData.iteration + 1)
			 << " ms/frame";
		_dewarpToolObject->setTimeMessage(os.str());
		os.str("");
		os << "Tracking " << (int)HRT.Read() << " ms";
		_dewarpToolObject->setStatusMessage(os.str());
		break;

	case DEWARP_STATE_RENDER:
		// Remember render's good frames are not rendered
		if (procData.iteration >= _dewarpParameters.GoodFramesIn)
		{
			int RenderIterations = procData.iteration + 1 - _dewarpParameters.GoodFramesIn;
			percentDone = (int)(100.0 * (RenderIterations) / (_iterationCount - _dewarpParameters.GoodFramesIn - 1));
			if (percentDone > 100)
				percentDone = 100;
			os << percentDone << "% done, " << std::fixed << setprecision(2) << (int)(HRT.Read() / RenderIterations)
				 << " ms/frame";
			_dewarpToolObject->setTimeMessage(os.str());
			os.str("");
			os << "Render " << std::fixed << setprecision(3) << HRT.Read() / 1000. << " secs";
			_dewarpToolObject->setStatusMessage(os.str());
		}
		else
		{
			HRT.Start();
		}
		break;

	default:
		os << (int)(100.0 * (procData.iteration + 1) / (_iterationCount + 1)) << "% done, " << std::fixed << setprecision
			 (2) << HRT.Read() / (procData.iteration + 1) << "ms/iteration";
		_dewarpToolObject->setTimeMessage(os.str());
		break;

	}
#else
	// TODO : take into account the anchor frames having 0 processing time!
	GetToolProgressMonitor()->BumpProgress();
#endif
	return 0;
}

int CDewarpProc::GetIterationCount(SToolProcessingData &procData)
{
	// This function is called by the Tool Infrastructure so it knows
	// how many processing iterations to do before stopping
	// Calculate the number of iterations, depends on the state

	return _iterationCount;
}

///////////////////////////////////////////////////////////////////////////////
//
// Tracking routines, should be moved to own algorithm
//

// This function extracts an image with center at point tp,
CTrackPoint CDewarpProc::FindBestFirstMatch(const CTrackPoint &tp)
{
	CTrackPoint tpR(0, 0);
	int tbe = _TrackBoxExtent;
	int sbe = _SearchBoxExtent;

	CopySubImage(_BaseMonoImage, nPicCol, nPicRow, tp.x, tp.y, tbe, _TrackBoxData);
	CopySubImage(_NextMonoImage, nPicCol, nPicRow, tp.x, tp.y, sbe, _SearchBoxData);

	// The largest this can be is
	double minL1 = _TrackBoxPoints * 65536.0;

	// We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
	// from the actual track point
	for (int i = -sbe; i <= sbe - 2 * tbe; i++)
		for (int j = -sbe; j <= sbe - 2 * tbe; j++)
		{
			double L1 = ComputeL1BoxNorm(_TrackBoxData, _TrackBoxSize, _SearchBoxData, _SearchBoxSize, i + sbe, j + sbe);
			if (L1 < minL1)
			{
				minL1 = L1;
				// Restore the proper track box extent
				tpR.x = tp.x + i + tbe;
				tpR.y = tp.y + j + tbe;
			}
		}

	return tpR;
}

CTrackPoint CDewarpProc::FindBestMatch(CTrackPoint &tp)
{
	CTrackPoint tpR(0, 0);
	int tbe = _TrackBoxExtent;
	int sbe = _SearchBoxExtent;

	CopySubImage(_BaseMonoImage, nPicCol, nPicRow, tp.x, tp.y, tbe, _TrackBoxData);
	CopySubImage(_NextMonoImage, nPicCol, nPicRow, tp.x, tp.y, sbe, _SearchBoxData);

	// The largest this can be is
	double minL1 = _TrackBoxPoints * 65565.0;
	int N = 2 * sbe - 2 * tbe + 1;
	int iX, iY;

	double *DL1 = new double[N * N];

	// We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
	// from the actual track point
	for (int i = -sbe; i <= sbe - 2 * tbe; i++)
		for (int j = -sbe; j <= sbe - 2 * tbe; j++)
		{
			double L1 = ComputeL1BoxNorm(_TrackBoxData, _TrackBoxSize, _SearchBoxData, _SearchBoxSize, i + sbe, j + sbe);
			DL1[(j + sbe) * N + i + sbe] = L1;
			if (L1 < minL1)
			{
				minL1 = L1;
				// Restore the proper track box extent
				iX = i + sbe;
				iY = j + sbe;
				tpR.x = tp.x + i + tbe;
				tpR.y = tp.y + j + tbe;
			}
		}

	// Note: if j is at the end, then there is a major problem with the tracking
	// region too small, so just punt
	if ((iX == 0) || (iX == (2*sbe - 2*tbe)))
		return tpR;
	if ((iY == 0) || (iY == (2*sbe - 2*tbe)))
		return tpR;

	// Find the subpixel resolution
	double D[9];
	double xdelta;
	double ydelta;

	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++)
			D[(j + 1) * 3 + i + 1] = DL1[(iY + j) * N + iX + i];

	EstimateMinFrom9Points(D, xdelta, ydelta);

	// xdelta and ydelta should not be larger than 1.5, if so, the interpolation is
	// wrong. So just use this value
	if (xdelta < -1.5)
		xdelta = -1.5;
	if (xdelta > 1.5)
		xdelta = 1.5;
	if (ydelta < -1.5)
		ydelta = -1.5;
	if (ydelta > 1.5)
		ydelta = 1.5;

	tpR.x = tpR.x + xdelta;
	tpR.y = tpR.y + ydelta;

	delete[]DL1;
	return tpR;
}

CTrackPoint CDewarpProc::FindBestMatchX(CTrackPoint &tp)
{
	CTrackPoint tpR(0, 0);
	int tbe = _TrackBoxExtent;
	int sbe = _SearchBoxExtent;

	CopySubImage(_BaseMonoImage, nPicCol, nPicRow, tp.x, tp.y, tbe, _TrackBoxData);
	CopySubImage(_NextMonoImage, nPicCol, nPicRow, tp.x, tp.y, sbe, _SearchBoxData);

	// The largest this can be is
	double minL1 = _TrackBoxPoints * 65536.0;
	int N = 2 * sbe - 2 * tbe + 1;
	int iX, iY;

	double *DL1 = new double[N * N];

	// We first start tracking at at the row
	// from the actual track point
	for (int i = -sbe; i <= sbe - 2 * tbe; i++)
	{
		double L1 = ComputeL1BoxNorm(_TrackBoxData, _TrackBoxSize, _SearchBoxData, _SearchBoxSize, i + sbe, sbe);
		DL1[sbe * N + i + sbe] = L1;
		if (L1 < minL1)
		{
			minL1 = L1;
			// Restore the proper track box extent
			iX = i + sbe;
			iY = sbe;
			tpR.x = tp.x + i + tbe;
			tpR.y = tp.y;
		}
	}

	// Note: if j is at the end, then there is a major problem with the tracking
	// region too small, so just punt
	if ((iX == 0) || (iX == (2*sbe - 2*tbe)))
		return tpR;

	// Find the subpixel resolution
	double D[9];
	double xdelta;
	double ydelta;

	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++)
			D[(j + 1) * 3 + i + 1] = DL1[iY * N + iX + i];

	EstimateMinFrom9Points(D, xdelta, ydelta);

	tpR.x = tpR.x + xdelta;
	tpR.y = tp.y;

	delete[]DL1;
	return tpR;
}

CTrackPoint CDewarpProc::FindBestMatchY(CTrackPoint &tp)
{
	CTrackPoint tpR(0, 0);
	int tbe = _TrackBoxExtent;
	int sbe = _SearchBoxExtent;

	CopySubImage(_BaseMonoImage, nPicCol, nPicRow, tp.x, tp.y, tbe, _TrackBoxData);
	CopySubImage(_NextMonoImage, nPicCol, nPicRow, tp.x, tp.y, sbe, _SearchBoxData);

	// The largest this can be is
	double minL1 = _TrackBoxPoints * 65536.0;
	int N = 2 * sbe - 2 * tbe + 1;
	int iX, iY;

	double *DL1 = new double[N * N];

	// We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
	// from the actual track point
	for (int j = -sbe; j <= sbe - 2 * tbe; j++)
	{
		double L1 = ComputeL1BoxNorm(_TrackBoxData, _TrackBoxSize, _SearchBoxData, _SearchBoxSize, sbe, j + sbe);
		DL1[(j + sbe) * N + sbe] = L1;
		if (L1 < minL1)
		{
			minL1 = L1;
			// Restore the proper track box extent
			iX = sbe;
			iY = j + sbe;
			tpR.y = tp.y + j + tbe;
			tpR.x = tp.x;
		}
	}

	// Note: if j is at the end, then there is a major problem with the tracking
	// region too small, so just punt
	if ((iY == 0) || (iY == (2*sbe - 2*tbe)))
		return tpR;

	// Find the subpixel resolution
	double D[9];
	double xdelta;
	double ydelta;

	for (int i = -1; i <= 1; i++)
		for (int j = -1; j <= 1; j++)
			D[(j + 1) * 3 + i + 1] = DL1[(iY + j) * N + iX];

	EstimateMinFrom9Points(D, xdelta, ydelta);

	tpR.x = tp.x;
	tpR.y = tpR.y + ydelta;

	delete[]DL1;
	return tpR;
}

CTrackPoint CDewarpProc::FindBestRefinedMatch(const CTrackPoint &dpB, Pixel *pBase, const CTrackPoint &dpN,
	 Pixel *pNext)
{
	CTrackPoint tpR(0, 0);
	int tbe = _TrackBoxExtent;
	int sbe = _SearchBoxExtent;

	int BSearchBoxExtent = tbe + 1;
	int nBase = 4 * _TrackBoxSize;
	int nNext = 4 * (2 * BSearchBoxExtent + 1);
	int iF, jF;

	CopySubImage(_BaseMonoImage, nPicCol, nPicRow, dpB.x, dpB.y, tbe + 1, _TrackBoxData);
	CopySubImage(_NextMonoImage, nPicCol, nPicRow, dpN.x, dpN.y, sbe + 1, _SearchBoxData);

	CreateSubPixelMatrix2(_TrackBoxData, 2*tbe + 3, tbe + 1, tbe + 1, tbe, pBase);
	CreateSubPixelMatrix2(_SearchBoxData, 2*sbe + 3, sbe + 1, sbe + 1, BSearchBoxExtent, pNext);

	// NOTE: This is not exactly the right way to search since we are searching around the
	// center pixel and not the min position.  However, let us assume that not much
	// differes
	double minL1 = 1024 * 8 * 8 * BSearchBoxExtent * BSearchBoxExtent;
	for (int i = -4; i <= 4; i++)
		for (int j = -4; j <= 4; j++)
		{
			double L1 = ComputeL1BoxNorm(pBase, nBase, pNext, nNext, i + 4, j + 4);
			if (L1 < minL1)
			{
				minL1 = L1;
				iF = i;
				jF = j;
			}
		}

	// NOTE: the iF/4.0 is relative movement of the center pixel, assign that
	// movement to the minimum pixel.
	tpR.x = dpN.x + iF / 4.0;
	tpR.y = dpN.y + jF / 4.0;

	return tpR;
}

void CDewarpProc::TrackNextFrame(int CurrentFrame)
{
	int NextFrame = CurrentFrame + 1;
	_dewarpToolObject->TrackingArray.at(NextFrame).clear();

	// See if this is the first Track, if so allocate the monochromo images
	if (_MonoImageIndex < 0)
	{
		_MonoImages[0] = new MTI_UINT16[nPicCol * nPicRow];
		_MonoImages[1] = new MTI_UINT16[nPicCol * nPicRow];
		_MonoImageIndex = 0;
		MakeMonoChromeImage(_BaseOrigImage, nPicCol, nPicRow, 16, (_RGB ? -1 : 0), nullptr, _MonoImages[1]);
	}

	_MonoImageIndex = (_MonoImageIndex + 1) % 2;
	_BaseMonoImage = _MonoImages[_MonoImageIndex];
	_NextMonoImage = _MonoImages[(_MonoImageIndex + 1) % 2];
	MakeMonoChromeImage(_NextOrigImage, nPicCol, nPicRow, 16, (_RGB ? -1 : 0), nullptr, _NextMonoImage);

	// Now swap the monochrome images

	// Allocate some storage
	int tbe = _TrackBoxExtent;

	int nBase = 4 * (2 * tbe + 1);
	int nNext = 4 * (2 * (tbe + 1) + 1);
	MTI_UINT16 *pRBase = new MTI_UINT16[nBase * nBase];
	MTI_UINT16 *pRNext = new MTI_UINT16[nNext * nNext];

	// Here is the problem, we have tracking points that can only move in certain
	// directions or are fixed relative to others.  We must process the ones with
	// the most freedom first, then continue to the less ones.
	// This is taken care of via the _AnchorList.

	CTrackingArray &TrackingArray = _dewarpToolObject->TrackingArray;
	CTrackingPoints &InputPoints = _dewarpToolObject->TrackingArray.at(NextFrame);
	CTrackingPoints OldPoints = _dewarpToolObject->TrackingArray.at(CurrentFrame);

//   CHRTimer hrt;
	// All points are valid
	for (unsigned i = 0; i < _AnchorList.size(); i++)
	{
		// An anchor list contains the indexes of the tracking points.  The list is sorted
		// so that every track point that is anchored, anchor has been computed.
		int iTag = _AnchorList[i];
		CTrackPoint tp = _dewarpToolObject->TrackingArray.at(CurrentFrame)[iTag];
		CTrackPoint dp(0, 0); // Must use 0,0 (or something else) here!!!

		// Now check for any relative or fixed tracking point
		switch (TrackingArray.getAnchor(iTag))
		{
		case EANCHOR_NONE:
			dp = FindBestMatch(tp);
			break;

		case EANCHOR_HORZIONAL:
			// Note: There are no orphans so this is legal
			tp.x += InputPoints[TrackingArray.getAnchorIndex(iTag)].x - OldPoints[TrackingArray.getAnchorIndex(iTag)].x;
			dp = FindBestMatchY(tp);
			break;

		case EANCHOR_VERTICAL:
			tp.y += InputPoints[TrackingArray.getAnchorIndex(iTag)].y - OldPoints[TrackingArray.getAnchorIndex(iTag)].y;
			dp = FindBestMatchX(tp);
			break;

		case EANCHOR_ALL:
			tp.x += InputPoints[TrackingArray.getAnchorIndex(iTag)].x - OldPoints[TrackingArray.getAnchorIndex(iTag)].x;
			tp.y += InputPoints[TrackingArray.getAnchorIndex(iTag)].y - OldPoints[TrackingArray.getAnchorIndex(iTag)].y;
			dp = tp;
			break;

		case EFIXED_HORIZIONAL:
			dp = FindBestMatchX(tp);
			break;

			// This is NOT correct we must fix it
		case EFIXED_VERTICAL:
			dp = FindBestMatchY(tp);
			break;

			// Waste of CPU time, but this will never be used
		case EFIXED_ALL:
			dp = tp;
			break;
		}

		_dewarpToolObject->TrackingArray.at(NextFrame)[iTag] = dp;
	}

//   DBTRACE(hrt);
	delete[]pRBase;
	delete[]pRNext;
}

#define NUM_COMPONENTS 3

bool SolveForWeights2(CTrackingPoints &tpS, CTrackingPoints &tpO, vector<double> &Wx, vector<double> &Wy)
{
	if (tpO.size() != tpS.size())
	{
		theError.set(DEWARP_ERROR_DEWARP_INVALID_POINTS,
			 "Internal Error: Some smoothed tracking points are marked invalid");
		return false;
	}

	// Building the matrices for solving the L*(w|a)=(v|0) problem with L={[K|P];[P'|0]}
	// Building K and P (Need to build L)
	auto N0 = (int)tpO.size();
	auto N3 = N0 + 3;
	vector<double>LU(N3*N3);

	auto sub2ind = [N3](int i, int j)
	{
		return i*N3 + j;
	};

	for (auto i = 0; i < N0; i++)
	{
		for (int j = 0; j < N0; j++)
		{
			if (i == j)
			{
				LU[sub2ind(i, j)] = 0;
			}
			else
			{
				auto d = UEnergyPoint(tpS[i], tpS[j]);
				LU[sub2ind(i, j)] = (float)d;
			}
		}

		LU[sub2ind(i, N0 + 0)] = 1;
		LU[sub2ind(i, N0 + 1)] = tpS[i].x;
		LU[sub2ind(i, N0 + 2)] = tpS[i].y;

		LU[sub2ind(N0 + 0, i)] = LU[sub2ind(i, N0 + 0)];
		LU[sub2ind(N0 + 1, i)] = LU[sub2ind(i, N0 + 1)];
		LU[sub2ind(N0 + 2, i)] = LU[sub2ind(i, N0 + 2)];
	}

	for (auto i = 0; i < 3; i++)
	{
		LU[sub2ind(N0 + 0, N0 + i)] = 0;
		LU[sub2ind(N0 + 1, N0 + i)] = 0;
		LU[sub2ind(N0 + 2, N0 + i)] = 0;
	}

	// Find the results
	Wx.resize(N3);
	Wy.resize(N3);

	for (int i = 0; i < N0; i++)
	{
		Wx[i] = tpO[i].x;
		Wy[i] = tpO[i].y;
	}

	for (int i = N0; i < N3; i++)
	{
		Wx[i] = 0;
		Wy[i] = 0;
	}

	// Now solve for the weights
	vector<int>mvIndex(N3);
	double d;

	// Decompose the matrix
	if (MtiMkl::ludcmp(N3, LU.data(), N3, mvIndex.data()) != 0)
	{
		theError.set(DEWARP_ERROR_DEWARP_MATRIX_SINGULAR, "Tracking points are close to a stright line");
		return false;
	}

	MtiMkl::lubksb(LU.data(), N3, mvIndex.data(), Wx.data());
	MtiMkl::lubksb(LU.data(), N3, mvIndex.data(), Wy.data());
	return true;
}

bool SolveForWeights(CTrackingPoints &tpS, CTrackingPoints &tpO, mvector<long double> &Wx, mvector<long double> &Wy)

	 // This creates the weights of the transform from the tracking points tpS to tpO
	 // Wx and Wy return with the weights of of the x and y tranformation.
	 //
	 // ******************************************************************************
{
	// Check for bad logic
	if (tpO.size() != tpO.validSize())
	{
		theError.set(DEWARP_ERROR_DEWARP_INVALID_POINTS,
			 "Internal Error: Some original tracking points are marked invalid");
		return false;
	}

	if (tpS.size() != tpS.validSize())
	{
		theError.set(DEWARP_ERROR_DEWARP_INVALID_POINTS,
			 "Internal Error: Some smoothed tracking points are marked invalid");
		return false;
	}

	int N0 = tpO.size();
	int N3 = N0 + 3;
	matrix<long double>L(N3, N3);

	// Begin to fill in the matrix, compute K
	for (int iR = 0; iR < N0; iR++)
		for (int iC = 0; iC < N0; iC++)
		{
			L[iR][iC] = UEnergyPoint(tpS[iR], tpS[iC]);
		}

	// fill with P and tranposed
	for (int i = 0; i < N0; i++)
	{
		L[i][N0] = 1;
		L[i][N0 + 1] = tpS[i].x;
		L[i][N0 + 2] = tpS[i].y;

		L[N0][i] = 1;
		L[N0 + 1][i] = tpS[i].x;
		L[N0 + 2][i] = tpS[i].y;
	}

	// Now do the 3 zeros
	for (int iR = N0; iR < N3; iR++)
		for (int iC = N0; iC < N3; iC++)
			L[iR][iC] = 0;

	// Finde the results
	mvector<long double>Hx(N3);
	mvector<long double>Hy(N3);

	for (int i = 0; i < N0; i++)
	{
		Hx[i] = tpO[i].x;
		Hy[i] = tpO[i].y;
	}

	for (int i = N0; i < N3; i++)
	{
		Hx[i] = 0;
		Hy[i] = 0;
	}

	// Now solve for the weights
	mvector<int>mvIndex(N3);
	matrix<long double>LU = L;
	double d;

	// Decompose the matrix
	if (!ludcmp<long double>(LU, mvIndex, d))
	{
		theError.set(DEWARP_ERROR_DEWARP_MATRIX_SINGULAR, "Tracking points are close to a stright line");
		return false;
	}

	Wx = Hx;
	lubksb<long double>(LU, mvIndex, Wx);

	Wy = Hy;
	lubksb<long double>(LU, mvIndex, Wy);
	return true;
}

int CDewarpProc::BiInterpolate(double x, double y, int iC, int iR)
{
	double R, G, B;
	double C00, C10, C01, C11;
	double a, b, c, d;
	MTI_UINT16 *pRI;
	MTI_UINT16 *pRO;

	if ((iC < _BoundingBox.left) || (iR < _BoundingBox.top) || (iC > _BoundingBox.right) || (iR > _BoundingBox.bottom))
	{
		return -1;
	}

	int nPicOffset = NUM_COMPONENTS * nPicCol;
	int nTotalOffset = NUM_COMPONENTS * (((int)y) * nPicCol + (int)x);

	// case 1, See if we are out of image
	if (((int)x < _BoundingBox.left) || ((int)y < _BoundingBox.top) || ((int)x > _BoundingBox.right) ||
		 ((int)y > _BoundingBox.bottom))
	{
		pRO = _outputImage + nPicOffset * iR + NUM_COMPONENTS * iC;
		if (_dewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
		{
			pRI = _previousFixedImage + nPicOffset * iR + NUM_COMPONENTS * iC;
			*pRO++ = *pRI++;
			*pRO++ = *pRI++;
			*pRO = *pRI;
		}
		else if (_dewarpParameters.MissingDataFill == MISSING_DATA_BLACK)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO = _BlackPixel[2];
		}
		// Assume white
		else
		{
			*pRO++ = _WhitePixel[0];
			*pRO++ = _WhitePixel[1];
			*pRO = _WhitePixel[2];
		}

		return 1;
	}

	// Case 2, we are at the bottom, we need to interpolate in x direction
	if (((int)x < _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
	{
		pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);
		R = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		pRI++;
		G = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		pRI++;
		B = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		*pRO++ = R;
		*pRO++ = G;
		*pRO = B;
		return 2;
	}
	// Case 3, we are at the right, we need to interpolate in y direction
	else if (((int)x == _BoundingBox.right) && ((int)y < _BoundingBox.bottom))
	{
		pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);
		R = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		pRI++;
		G = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		pRI++;
		B = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		*pRO++ = R;
		*pRO++ = G;
		*pRO = B;
		return 3;
	}
	// Case 4, we are at bottom right corner, just copy.
	else if (((int)x == _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
	{
		pRI = pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + nPicOffset * iR + NUM_COMPONENTS * iC;
		*pRO++ = *pRI++;
		*pRO++ = *pRI++;
		*pRO = *pRI;
		return 4;
	}

	// Case 3, we have real data
	pRI = _BaseOrigImage + nTotalOffset;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	R = a * x + b * y + x * y * c + d;

	pRI++;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	G = a * x + b * y + x * y * c + d;

	pRI++;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	B = a * x + b * y + x * y * c + d;

	// BiInterpolate puts result in _outputImage
	pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);

	*pRO++ = floatToImageDatum(R);
	*pRO++ = floatToImageDatum(G);
	*pRO = floatToImageDatum(B);

	return 0;
}

void CDewarpProc::FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut)
{
	// Note, this is currently only valid for RGB or YUV formats
	// Must be modified for non-3 pixel formats
	if (_pxlComponentCount != 3)
		return;

	int BytesPerRow = _pxlComponentCount * nPicCol;

	// Clear the top
	for (int iR = 0; iR < Box.top; iR++)
	{
		MTI_UINT16 *pRO = pOut + Box.left * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.left; iC <= Box.right; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the bottom
	for (int iR = Box.top + 1; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + Box.left * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.left; iC <= Box.right; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the left
	for (int iR = 0; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + iR * BytesPerRow;
		for (int iC = 0; iC < Box.left; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the right
	for (int iR = 0; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + (Box.right + 1) * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.right + 1; iC < nPicCol; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}
}

bool CDewarpProc::DewarpSlice(RECT Box)
	 // Note: This procedure must be reentrent so make sure Box remains local
{
	CHRTimer _HRT;

	// Timing information
	double t = _HRT.Read();

	// First step, choose the dewarping function
   _dewarpParameters.UFunction = U_FUNCTION_TYPE_R_LOGR;
	switch (_dewarpParameters.UFunction)
	{
	case U_FUNCTION_TYPE_LINEAR:
#ifdef _WIN64
		{
			// DBTRACE(_N0);
			// for (auto i = 0; i < _N0; i++)
			// {
			// TRACE_0(errout << "\nresult[" << i <<"] = {" << _tpSX[i] << ", " << _tpSY[i] << "};");
			// }

//			CHRTimer motionTimer;
			FindDewarpMotionLinCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#else
		FindDewarpMotionLinSSE4(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#endif
//		DBTRACE(motionTimer);
	}
	break;

case U_FUNCTION_TYPE_R_LOGR:
	// Don't use assembly code in 64 bits
#ifdef _WIN64
	FindDewarpMotionRLogCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#else
	FindDewarpMotionRLogSSE(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#endif
	break;

case U_FUNCTION_TYPE_R2_LOGR:
	FindDewarpMotionRRLogCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
	break;

case U_FUNCTION_TYPE_NONE:
	FindDewarpMotionIdenity(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
	break;

default:
	throw;
}

// This is not really correct as _dDewarpTime should be made thread safe
// but we don't really care.
_dMotionTime = _HRT.Read() - t;
t = _HRT.Read();
DewarpImage(_xVec, _yVec, Box);
_dDewarpTime = _HRT.Read() - t;

return true;
}

void CDewarpProc::DewarpImage(float *xVec, float *yVec, RECT &Box)
{
// Dewarp the bounding box
for (int iR = Box.top; iR <= Box.bottom; iR++)
{
	float *xVecT = xVec + iR * nPicCol + (int)Box.left;
	float *yVecT = yVec + iR * nPicCol + (int)Box.left;
	for (int iC = (int)Box.left; iC <= (int)Box.right; iC++)
	{
		BiInterpolate(*xVecT++, *yVecT++, iC, iR);
	}
}
}

// This structure is designed to pass information to the thread
class CDewarpSliceThreadParameters
{
public:
CDewarpSliceThreadParameters(CDewarpProc *newDewarpProc) : dewarpProc(newDewarpProc)
{
	Count = 0;
}

CDewarpProc *dewarpProc;
RECT Box;
int Count;
};

// This automatically deallocs the thread
class CAutoCountingWaitRelease
{
public:
CAutoCountingWaitRelease(CCountingWait &cw)
{
	_countingWait = &cw;
}

~CAutoCountingWaitRelease(void)
{
	_countingWait->ReleaseOne();
}

void Finishup(void)
{
}

private:
CCountingWait *_countingWait;
};

// Translate the static to class
void DewarpSliceThreadStart(void *vpAppData, void *vpReserved)
{
// Cast the pointer
CDewarpSliceThreadParameters dewarpSliceThreadParameters = *reinterpret_cast<CDewarpSliceThreadParameters*>(vpAppData);

// TRACE_0(errout << "TTT  DewarpSliceThreadStart begin" << dewarpSliceThreadParameters.Count);

if (BThreadBegin(vpReserved))
{
	dewarpSliceThreadParameters.dewarpProc->CountingWait.ReleaseOne();
	return;
}

try
{
	dewarpSliceThreadParameters.dewarpProc->DewarpSlice(dewarpSliceThreadParameters.Box);
}
catch (...)
{
	TRACE_0(errout << "DewarpSliceThreadStart: Dewarp Slice created major error");
}

dewarpSliceThreadParameters.dewarpProc->CountingWait.ReleaseOne();
}

bool CDewarpProc::DewarpFrame(int Frame)

	 // Before calling, the following must be set each time
	 // _outputImage pointer to put the information
	 // _BaseOrigImage pointer to the image to dewarp
	 //
	 // If use previous image for missing data is set then
	 // _previousFixedImage must contain the previous image.
	 //
	 //
	 // Before starting calling loop, the following must be set once
	 // _BoundingBox
	 //
{
CAutoErrorReporter autoErr("CDewarpTool::DewarpFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

CTrackingPoints tpO = _dewarpToolObject->TrackingArray.at(Frame);
CTrackingPoints tpS = _dewarpToolObject->SmoothedArray.at(Frame);
vector<double>VWx;
vector<double>VWy;

// Compute the warp functions
if (!SolveForWeights2(tpS, tpO, VWx, VWy))
{
	autoErr.traceAction = AUTO_ERROR_NO_TRACE;
	autoErr.errorCode = theError.getError();
	autoErr.msg << "Dewarp frame failed: " << theError.getMessage();
	return false;
}

_N0 = tpO.size();
int N1 = _N0 + 3;

// Change the vects into floats
_Wx = new float[N1];
_Wy = new float[N1];

for (int i = 0; i < N1; i++)
{
	_Wx[i] = VWx[i];
	_Wy[i] = VWy[i];
}

// Chage the smoothed points into float vects
_tpSX = new float[_N0];
_tpSY = new float[_N0];
for (int i = 0; i < _N0; i++)
{
	_tpSX[i] = tpS[i].x;
	_tpSY[i] = tpS[i].y;
}

// Outside is always filled black
if (!_dewarpParameters.KeepOriginalMatValues)
	FillOutsideBlack(_BoundingBox, _outputImage);

// Copy over the data so we are
_xVec = (float *)MTImemalign(16, nPicRow * nPicCol*sizeof(float));
_yVec = (float *)MTImemalign(16, nPicRow * nPicCol*sizeof(float));

// Fire up a thread to work on each slice.
CountingWait.SetCount(_processingThreadCount);

RECT Box = _BoundingBox;
int sliceSize = (_BoundingBox.bottom - _BoundingBox.top) / _processingThreadCount;
Box.bottom = -1;

// Do _processingThreadCount-1 slices
CDewarpSliceThreadParameters dewarpSliceThreadParameters(this);
// Note: This starts the count at 0

vector<void *>bThreadStructs;
for (int i = 0; i < _processingThreadCount - 1; i++)
{
	Box.top = Box.bottom + 1;
	Box.bottom += sliceSize;
	dewarpSliceThreadParameters.Box = Box;
	auto ts = BThreadSpawn(DewarpSliceThreadStart, &dewarpSliceThreadParameters);
	MTIassert(ts != nullptr);
	bThreadStructs.push_back(ts);
	dewarpSliceThreadParameters.Count++;
}

// Do last one, this avoids round off errors
Box.top = Box.bottom + 1;
Box.bottom = _BoundingBox.bottom;
dewarpSliceThreadParameters.Box = Box;
auto tsl = BThreadSpawn(DewarpSliceThreadStart, &dewarpSliceThreadParameters);
MTIassert(tsl != nullptr);
bThreadStructs.push_back(tsl);

// Now wait for it to finish
CountingWait.Wait(bThreadStructs, __func__, __LINE__);

// Free it all up and mark not allocated
delete[]_Wx;
delete[]_Wy;
delete[]_tpSX;
delete[]_tpSY;
MTIfree(_xVec);
MTIfree(_yVec);

// Null them out
_Wx = _Wy = nullptr;
_tpSX = _tpSY = nullptr;
_xVec = _yVec = nullptr;

// TRACE_0(errout << "TTT exit DewarpFrame ");
return true;
}
