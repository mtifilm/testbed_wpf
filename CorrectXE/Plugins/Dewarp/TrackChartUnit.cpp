//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TrackChartUnit.h"
#include "pTimeCode.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTrackChartForm *TrackChartForm;

// Missing constants for TChartfx
#define CT_3D 0x00000100
#define CT_HORZ 0x00000200
#define CT_TOOL 0x00000400
#define CT_PALETTE 0x00000800
#define CT_LEGEND 0x00001000
#define CT_TOGETHER 0x00002000
#define CT_POINTS 0x00004000
#define CT_SHOWZERO 0x00008000
#define CT_EACHBAR 0x00010000
#define CT_CLUSTER 0x00020000
#define CT_SHOWDATA 0x00040000
#define CT_DLGGRAY 0x00080000
#define CT_PATTERN 0x00100000
#define CT_SHOWVALUES 0x00200000
#define CT_MENU 0x00400000
#define CT_SCATTERLINE	0x00800000
#define CT_COLORLINE 0x01000000
#define CT_NOAREALINE 0x02000000
#define CT_NOBORDERS 0x04000000
#define CT_PIEVALUES 0x08000000

//---------------------------------------------------------------------------
__fastcall TTrackChartForm::TTrackChartForm(TComponent* Owner)
        : TForm(Owner)
{

}

   double TTrackChartForm::FindExtentX(void)
{
   double dTMax = 10;

   CTrackingPoints TPS;
   CTrackingPoints SPS;
   int iS = _TrackingArray->at(_TrackingArray->beginIndex()).beginIndex();
   int iE = _TrackingArray->at(_TrackingArray->beginIndex()).endIndex();

   for (int iTag = iS; iTag < iE; iTag++)
   {
     TPS = _TrackingArray->TSlice(iTag);
     SPS = _SmoothedArray->TSlice(iTag);

     double dMin = 100000;
     double dMax = 0;

     for (int i=TPS.beginIndex(); i < TPS.endIndex(); i++)
       if (TPS[i].isValid())
         {
            if (TPS[i].x > dMax) dMax = TPS[i].x;
            if (TPS[i].x < dMin) dMin = TPS[i].x;
         }

     for (int i=SPS.beginIndex(); i < SPS.endIndex(); i++)
       if (SPS[i].isValid())
         {
            if (SPS[i].x > dMax) dMax = SPS[i].x;
            if (SPS[i].x < dMin) dMin = SPS[i].x;
         }

     dMin = ((int)dMin/10)*10;
     dMax = ((int)dMax/10 + 1)*10;

     if (dTMax < (dMax-dMin)) dTMax = (dMax-dMin);
    }
    return dTMax;
}

   double TTrackChartForm::FindExtentY(void)
{
   double dTMax = 10;

   CTrackingPoints TPS;
   CTrackingPoints SPS;
   int iS = _TrackingArray->at(_TrackingArray->beginIndex()).beginIndex();
   int iE = _TrackingArray->at(_TrackingArray->beginIndex()).endIndex();

   for (int iTag = iS; iTag < iE; iTag++)
   {
     TPS = _TrackingArray->TSlice(iTag);
     SPS = _SmoothedArray->TSlice(iTag);

     double dMin = 100000;
     double dMax = 0;

     for (int i=TPS.beginIndex(); i < TPS.endIndex(); i++)
       if (TPS[i].isValid())
         {
            if (TPS[i].y > dMax) dMax = TPS[i].y;
            if (TPS[i].y < dMin) dMin = TPS[i].y;
         }

     for (int i=SPS.beginIndex(); i < SPS.endIndex(); i++)
       if (SPS[i].isValid())
         {
            if (SPS[i].y > dMax) dMax = SPS[i].y;
            if (SPS[i].y < dMin) dMin = SPS[i].y;
         }

     dMin = ((int)dMin/10)*10;
     dMax = ((int)dMax/10 + 1)*10;

     if (dTMax < (dMax-dMin)) dTMax = (dMax-dMin);
    }

    return dTMax;
}

//---------------------------------------------------------------------------

   void TTrackChartForm::SetTrackTag(int Tag)
{
    _Tag = Tag;
    _TrackingPoints = _TrackingArray->TSlice(_Tag);
    _SmoothedPoints = _SmoothedArray->TSlice(_Tag);

    int tOffset = _TrackingPoints.beginIndex();
    int sOffset = _SmoothedPoints.beginIndex();

   // To make the charts have the same scale, we must find the largest movement
   double fxExtent = FindExtentX();
   double fyExtent = FindExtentY();

//  Now fill in the Horizontal graph

   double dMin = 100000;
   double dMax = 0;

   PTimecode ptc(false, 30);

   HTrackGraph->Series[0]->Clear();
   for (int i=_TrackingPoints.beginIndex(); i < _TrackingPoints.endIndex(); i++)
     {
        ptc.Frame = i + 30*60*60;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].x;;
        if (d > 0)
           HTrackGraph->Series[0]->AddXY(i, d, TCS);
        else
           HTrackGraph->Series[0]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   HTrackGraph->Series[1]->Clear();
   for (int i=_SmoothedPoints.beginIndex(); i < _SmoothedPoints.endIndex(); i++)
     {
        String TCS = i;
        double d =  _SmoothedPoints[i].x;;
        if (d > 0)
           HTrackGraph->Series[1]->AddXY(i, d, TCS);
        else
           HTrackGraph->Series[1]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   // Stupid way of setting axis to avoid min being greater than max
   int iMin = ((int)dMin/10)*10;
   HTrackGraph->LeftAxis->Minimum = min(iMin,HTrackGraph->LeftAxis->Maximum);
   HTrackGraph->LeftAxis->Maximum = iMin + fyExtent;
   HTrackGraph->LeftAxis->Minimum = iMin;

   // Do the vertical graph
   dMin = 100000;
   dMax = 0;

   VTrackGraph->Series[0]->Clear();
   for (int i=_TrackingPoints.beginIndex(); i < _TrackingPoints.endIndex(); i++)
     {
        ptc.Frame = i + 30*60*60;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].y;;
        if (d > 0)
           VTrackGraph->Series[0]->AddXY(i, d, TCS);
        else
           VTrackGraph->Series[0]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   VTrackGraph->Series[1]->Clear();
   for (int i=_SmoothedPoints.beginIndex(); i < _SmoothedPoints.endIndex(); i++)
     {
        String TCS = i;
        double d =  _SmoothedPoints[i].y;;
        if (d > 0)
           VTrackGraph->Series[1]->AddXY(i, d, TCS);
        else
           VTrackGraph->Series[1]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   // Stupid way of setting axis to avoid min being greater than max
   iMin = ((int)dMin/10)*10;
   VTrackGraph->LeftAxis->Minimum = min(iMin,VTrackGraph->LeftAxis->Maximum);
   VTrackGraph->LeftAxis->Maximum = iMin + fyExtent;
   VTrackGraph->LeftAxis->Minimum = iMin;
}

   void TTrackChartForm::SetTrackingArray(CTrackingArray *newTrackingArray, CTrackingArray *newSmoothedArray)
{
   _TrackingArray = newTrackingArray;
   _SmoothedArray = newSmoothedArray;
}


