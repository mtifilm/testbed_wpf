//---------------------------------------------------------------------------

#ifndef DewarpParametersH
#define DewarpParametersH     

#include "IniFile.h"
#include "IPresetParameters.h"

//---------------------------------------------------------------------------
  enum EUFunctionType
  {
     U_FUNCTION_TYPE_INVALID = -1,
     U_FUNCTION_TYPE_LINEAR = 0,
     U_FUNCTION_TYPE_R_LOGR = 1,
     U_FUNCTION_TYPE_NONE = 2,
     U_FUNCTION_TYPE_R2_LOGR = 3
  };

  enum EMissingDataFillType
  {
     MISSING_DATA_INVALID = -1,
	 MISSING_DATA_WHITE = 1,
     MISSING_DATA_BLACK = 0,
     MISSING_DATA_PREVIOUS = 2
  };

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

//////////////////////////////////////////////////////////////////////
// This is almost a struc
class CDewarpParameters : public IPresetParameters
{
public:
	CDewarpParameters();
	CDewarpParameters(const CDewarpParameters &rhs);

   bool ReadParameters(const string &strFileName);
	bool WriteParameters(const string &strFileName) const;

	virtual void ReadParameters(CIniFile *ini, const string &IniSection);
	virtual void WriteParameters(CIniFile *ini, const string &IniSection) const;
	virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const;

	bool operator == (const CDewarpParameters &rhs) const;
	bool operator != (const CDewarpParameters &rhs) const;
	CDewarpParameters &operator = (const CDewarpParameters &rhs);

   void ReadPDLEntry(CPDLElement *pdlToolAttribs);
   void WritePDLEntry(CPDLElement &parent);

public:
   // All member variables are public for easy access.

	int TrackBoxExtent = 40;
	int SearchBoxExtent = 80;
	int GoodFramesIn = 2;
	int GoodFramesOut = 2;
	double Alpha2 = 0.0001;
	EUFunctionType UFunction = U_FUNCTION_TYPE_R_LOGR;
	EMissingDataFillType MissingDataFill = MISSING_DATA_PREVIOUS;

	// Use a predefined mat box
	bool UseMatting = true;
	bool KeepOriginalMatValues = true;

private:
};

extern CIniAssociations UFunctionAssociations;
extern CIniAssociations UMissingDataAssociations;

//---------------------------------------------------------------------------
#endif


