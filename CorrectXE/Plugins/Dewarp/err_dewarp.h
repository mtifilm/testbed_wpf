/*
   File Name:  err_dewarp.h
   Created: July 18, 2006

   This contains all the error codes for all core_code functions
*/
#ifndef _ERR_DEWARP_H
#define _ERR_DEWARP_H

//-------------------------------------------------------------------------
//
//  Dewarp Erros  DEWARP_ERROR__XXX
//
#define DEWARP_ERROR_TRACKING_COLLAPSE -8000
//  Internal error, this happens when the tracking array is malformed,
//  which usually means that the number of points is NOT the same for
//  each frame.

#define DEWARP_ERROR_SMOOTHING_TOO_FEW_FRAMES -8001
//  This happens when the number of points to smooth is < 3.
//  Should never happen because the number of frames is checked eariler.

#define DEWARP_ERROR_SMOOTHING_MATRIX_SINGULAR -8002
//  Internal error, happens when the smoothing matrix cannot be
//  inverted, most likely do to finite arithmetic errors

#define DEWARP_ERROR_DEWARP_MATRIX_SINGULAR -8003
//  If there are too few tracking points (caught before the function
//  call) or the points are on a line, this error happens.

#define DEWARP_ERROR_DEWARP_INVALID_POINTS -8004
//  Internal error, this happens if the original or smoothed tracking
//  points have a point that is marked as invalid.  Normally the points
//  are collapsed before dewarping, marks a flaw in the program logic.

#define DEWARP_ERROR_DEWARP_TOO_FEW_TRACKPOINTS -8005
//  At least 3 tracking points must be defined on a frame.

#endif