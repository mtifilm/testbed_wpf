object DewarpForm: TDewarpForm
  Left = 1069
  Top = 179
  BorderStyle = bsToolWindow
  Caption = 'Dewarp'
  ClientHeight = 720
  ClientWidth = 512
  Color = clBtnFace
  Constraints.MaxHeight = 749
  Constraints.MaxWidth = 528
  Constraints.MinHeight = 744
  Constraints.MinWidth = 518
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    512
    720)
  PixelsPerInch = 96
  TextHeight = 13
  object BottomBevel: TBevel
    Left = 2
    Top = 505
    Width = 506
    Height = 10
    Anchors = [akLeft, akTop, akRight]
    Shape = bsTopLine
  end
  object WarpLabel: TLabel
    Left = 208
    Top = 619
    Width = 70
    Height = 13
    Caption = 'Warp Function'
  end
  object UFunctionComboBox: TComboBox
    Left = 207
    Top = 631
    Width = 178
    Height = 21
    Style = csDropDownList
    TabOrder = 0
  end
  object RenderRadioBox: TRadioGroup
    Left = 16
    Top = 40
    Width = 177
    Height = 41
    Caption = 'Render Destination'
    Columns = 2
    Enabled = False
    ItemIndex = 0
    Items.Strings = (
      'Source Clip'
      'New Clip')
    TabOrder = 1
    Visible = False
  end
  object AdvancedButton: TBitBtn
    Left = 272
    Top = 238
    Width = 89
    Height = 25
    Caption = 'More...'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      04000000000080000000120B0000120B00001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADA7000
      0ADAADADAD707DA0ADADDADAD07ADA0ADADAADAD77ADA0ADADADDADA0ADA77DA
      DADAADA0ADAD0DADADADDAD0DADA0ADADADAADA0ADAD0DADADADDAD0DADA0ADA
      DADAADA0ADAD0DADADADDAD0DADA0ADADADAADAD0DAD77ADADADDADA77DAD0DA
      DADAADADA07DAD0DADADDADADA707AD0DADAADADADAD70000DAD}
    TabOrder = 2
    OnClick = AdvancedButtonClick
  end
  object DewarpControlPanel: TColorPanel
    Left = 0
    Top = 0
    Width = 512
    Height = 720
    BevelOuter = bvNone
    Color = 6974058
    Constraints.MaxHeight = 720
    Constraints.MaxWidth = 512
    Constraints.MinHeight = 720
    Constraints.MinWidth = 512
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object MotionRestrictionLabel: TLabel
      Left = 285
      Top = 564
      Width = 85
      Height = 13
      Caption = 'Motion Restriction'
      Visible = False
    end
    object VerticalMatchLabel: TLabel
      Left = 283
      Top = 622
      Width = 68
      Height = 13
      Caption = 'Vertical Match'
      Visible = False
    end
    object HorziontalMatchLabel: TLabel
      Left = 283
      Top = 606
      Width = 83
      Height = 13
      Caption = 'Horizontal Match '
      Visible = False
    end
    object Bevel1: TBevel
      Left = 32
      Top = 544
      Width = 457
      Height = 3
      Visible = False
    end
    object Label2: TLabel
      Left = 44
      Top = 538
      Width = 111
      Height = 13
      Caption = ' Advanced Parameters '
      Visible = False
    end
    object TrackingSettingsGroupBox: TGroupBox
      Left = 304
      Top = 52
      Width = 177
      Height = 158
      Caption = ' Tracking Parameters '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object SmoothMaxLabel: TLabel
        Left = 123
        Top = 136
        Width = 18
        Height = 15
        Alignment = taRightJustify
        Caption = '___'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial Black'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object SmoothMinLabel: TLabel
        Left = 14
        Top = 141
        Width = 18
        Height = 15
        Caption = '/\/\/\'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial Black'
        Font.Style = []
        ParentFont = False
      end
      object TrackingBoxSizeMinusLabel: TLabel
        Left = 14
        Top = 44
        Width = 9
        Height = 20
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object TrackingBoxSizePlusLabel: TLabel
        Left = 128
        Top = 44
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object MotionMinusLabel: TLabel
        Left = 14
        Top = 90
        Width = 9
        Height = 20
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object MotionPlusLabel: TLabel
        Left = 128
        Top = 90
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      inline SmoothTrackEdit: TTrackEditFrame
        Left = 0
        Top = 108
        Width = 184
        Height = 41
        Hint = 
          'How much smoothing to apply to the tracking point motion'#13#10'Press ' +
          '9 to select'#13#10'Move mouse wheel or use arrow keys to change'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 0
        ExplicitTop = 108
        ExplicitWidth = 184
        ExplicitHeight = 41
        inherited MinLabel: TLabel
          Width = 3
          Caption = ''
          ExplicitWidth = 3
        end
        inherited MaxLabel: TLabel
          Left = 117
          Width = 3
          Caption = ''
          ExplicitLeft = 117
          ExplicitWidth = 3
        end
        inherited TitleLabel: TColorLabel
          Top = 2
          Width = 56
          Caption = '  Smoothing'
          Font.Color = clBtnText
          ParentFont = False
          ExplicitTop = 2
          ExplicitWidth = 56
        end
        inherited TrackBar: TTrackBar
          Width = 140
          Enabled = False
          SliderVisible = False
          ExplicitWidth = 140
        end
        inherited TrackBarDisabledPanel: TPanel
          Width = 128
          Visible = True
          ExplicitWidth = 128
        end
        inherited EditAlignmentPanel: TPanel
          Left = 144
          Height = 41
          ExplicitLeft = 144
          ExplicitHeight = 41
          inherited Edit: TEdit
            Width = 24
            ExplicitWidth = 24
          end
          inherited EditDisabledPanel: TPanel
            Width = 24
            Visible = True
            ExplicitWidth = 24
            inherited EditDisabledLabel: TLabel
              Left = 4
              Width = 23
              Alignment = taLeftJustify
              Caption = ''
              Enabled = True
              Font.Color = clBtnShadow
              Font.Height = -11
              ExplicitLeft = 4
              ExplicitWidth = 23
            end
          end
        end
        inherited NotifyWidget: TCheckBox
          OnClick = SmoothTrackBarChange
        end
        inherited TrackBarMinPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        inherited TrackBarMaxPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
      inline TrackingBoxSizeTrackEdit: TTrackEditFrame
        Left = 0
        Top = 16
        Width = 184
        Height = 41
        Hint = 
          'Size of the area tracked by each tracking point'#13#10'Press 7 to sele' +
          'ct'#13#10'Move mouse wheel or use arrow keys to change'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        ExplicitTop = 16
        ExplicitWidth = 184
        ExplicitHeight = 41
        inherited MinLabel: TLabel
          Width = 3
          Caption = ''
          ExplicitWidth = 3
        end
        inherited MaxLabel: TLabel
          Left = 117
          Width = 3
          Caption = ''
          ExplicitLeft = 117
          ExplicitWidth = 3
        end
        inherited TitleLabel: TColorLabel
          Top = 2
          Width = 46
          Caption = '  Box Size'
          Font.Color = clBtnText
          ParentFont = False
          ExplicitTop = 2
          ExplicitWidth = 46
        end
        inherited TrackBar: TTrackBar
          Width = 140
          Enabled = False
          SliderVisible = False
          TabStop = False
          ExplicitWidth = 140
        end
        inherited TrackBarDisabledPanel: TPanel
          Width = 128
          Visible = True
          ExplicitWidth = 128
        end
        inherited EditAlignmentPanel: TPanel
          Left = 144
          Height = 41
          ExplicitLeft = 144
          ExplicitHeight = 41
          inherited Edit: TEdit
            Width = 24
            ExplicitWidth = 24
          end
          inherited EditDisabledPanel: TPanel
            Width = 24
            Visible = True
            ExplicitWidth = 24
            inherited EditDisabledLabel: TLabel
              Left = 4
              Width = 23
              Alignment = taLeftJustify
              Caption = ''
              Enabled = True
              Font.Color = clBtnShadow
              ExplicitLeft = 4
              ExplicitWidth = 23
            end
          end
        end
        inherited NotifyWidget: TCheckBox
          OnClick = TrackingTrackBarChange
        end
        inherited TrackBarMinPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        inherited TrackBarMaxPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
      inline MotionTrackEdit: TTrackEditFrame
        Left = 0
        Top = 62
        Width = 184
        Height = 41
        Hint = 
          'How far a tracking point can move between frames'#13#10'Press 8 to sel' +
          'ect'#13#10'Move mouse wheel or use arrow keys to change'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 2
        ExplicitTop = 62
        ExplicitWidth = 184
        ExplicitHeight = 41
        inherited MinLabel: TLabel
          Width = 3
          Caption = ''
          ExplicitWidth = 3
        end
        inherited MaxLabel: TLabel
          Left = 117
          Width = 3
          Caption = ''
          ExplicitLeft = 117
          ExplicitWidth = 3
        end
        inherited TitleLabel: TColorLabel
          Top = 2
          Width = 100
          Caption = '  Motion Search Area'
          Font.Color = clBtnText
          ParentFont = False
          ExplicitTop = 2
          ExplicitWidth = 100
        end
        inherited TrackBar: TTrackBar
          Width = 140
          Enabled = False
          Max = 50
          Min = 5
          Position = 20
          SliderVisible = False
          TabStop = False
          ExplicitWidth = 140
        end
        inherited TrackBarDisabledPanel: TPanel
          Width = 128
          Visible = True
          ExplicitWidth = 128
        end
        inherited EditAlignmentPanel: TPanel
          Left = 144
          Height = 41
          ExplicitLeft = 144
          ExplicitHeight = 41
          inherited Edit: TEdit
            Width = 24
            ExplicitWidth = 24
          end
          inherited EditDisabledPanel: TPanel
            Width = 24
            Visible = True
            ExplicitWidth = 24
            inherited EditDisabledLabel: TLabel
              Left = 4
              Width = 23
              Alignment = taLeftJustify
              Caption = ''
              Enabled = True
              Font.Color = clBtnShadow
              Font.Height = -11
              ExplicitLeft = 4
              ExplicitWidth = 23
            end
          end
        end
        inherited NotifyWidget: TCheckBox
          OnClick = SearchTrackBarChange
        end
        inherited TrackBarMinPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        inherited TrackBarMaxPositionTick: TColorPanel
          Width = 0
          Height = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
    end
    object IndividualSmoothCBox: TCheckBox
      Left = 71
      Top = 558
      Width = 121
      Height = 17
      Caption = 'Individual Smoothing'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      Visible = False
      OnClick = IndividualSmoothCBoxClick
    end
    object IndividualSmoothingGroupBox: TGroupBox
      Left = 43
      Top = 560
      Width = 177
      Height = 53
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Visible = False
      object IndividualSmoothLabelMin: TLabel
        Left = 10
        Top = 36
        Width = 30
        Height = 13
        Caption = '/\/\/\'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object IndividualSmoothLabelMax: TLabel
        Left = 116
        Top = 32
        Width = 21
        Height = 13
        Caption = '___'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object IndividualSmoothClippingPanel: TPanel
        Left = 2
        Top = 14
        Width = 173
        Height = 23
        BevelOuter = bvNone
        TabOrder = 0
        inline IndividualSmoothTrackEdit: TTrackEditFrame
          Left = -4
          Top = -12
          Width = 184
          Height = 41
          Color = clBtnFace
          ParentColor = False
          TabOrder = 0
          ExplicitLeft = -4
          ExplicitTop = -12
          ExplicitWidth = 184
          ExplicitHeight = 41
          inherited MinLabel: TLabel
            Width = 3
            Caption = ''
            ExplicitWidth = 3
          end
          inherited MaxLabel: TLabel
            Left = 117
            Width = 3
            Caption = ''
            ExplicitLeft = 117
            ExplicitWidth = 3
          end
          inherited TitleLabel: TColorLabel
            Top = 2
            Width = 3
            Caption = ''
            Font.Color = clBtnShadow
            Font.Name = 'MS Sans Serif'
            ParentFont = False
            ExplicitTop = 2
            ExplicitWidth = 3
          end
          inherited TrackBar: TTrackBar
            Width = 140
            Enabled = False
            SliderVisible = False
            Visible = False
            ExplicitWidth = 140
          end
          inherited TrackBarDisabledPanel: TPanel
            Width = 128
            ExplicitWidth = 128
          end
          inherited EditAlignmentPanel: TPanel
            Left = 144
            Height = 41
            ExplicitLeft = 144
            ExplicitHeight = 41
            inherited Edit: TEdit
              Width = 24
              ExplicitWidth = 24
            end
            inherited EditDisabledPanel: TPanel
              Width = 24
              Visible = True
              ExplicitWidth = 24
              inherited EditDisabledLabel: TLabel
                Left = 4
                Width = 23
                Alignment = taLeftJustify
                Caption = ''
                Enabled = True
                Font.Color = clBtnShadow
                Font.Height = -11
                ExplicitLeft = 4
                ExplicitWidth = 23
              end
            end
          end
          inherited NotifyWidget: TCheckBox
            OnClick = IndividualSmoothTrackBarChange
          end
          inherited TrackBarMinPositionTick: TColorPanel
            Width = 0
            Height = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          inherited TrackBarMaxPositionTick: TColorPanel
            Width = 0
            Height = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
        end
      end
    end
    object AnchorMotionComboBox: TComboBox
      Tag = -1
      Left = 344
      Top = 269
      Width = 137
      Height = 21
      Hint = 'Choose a constraint type for the'#13#10'track point being anchored'
      Style = csDropDownList
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnChange = AnchorMotionComboBoxChange
    end
    object SetMatButton: TBitBtn
      Left = 351
      Top = 529
      Width = 34
      Height = 34
      Caption = '&Auto'
      TabOrder = 16
      Visible = False
      OnClick = SetMatButtonClick
    end
    object WarnMarkOutMoveCheckBox: TCheckBox
      Left = 284
      Top = 583
      Width = 169
      Height = 17
      Hint = 'Enables/Disables warning when the mark out is moved.'
      Caption = 'Warn on Mark Out Change'
      TabOrder = 11
      Visible = False
      OnClick = WarnMarkOutMoveCheckBoxClick
    end
    object VTrackGraph: TChart
      Left = 10
      Top = 470
      Width = 500
      Height = 170
      Hint = 
        'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
        'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
        'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
        'downwards to zoom in, upwards to zoom out'
      BackImage.Inside = True
      BackWall.Color = clBtnFace
      BackWall.Pen.Color = 4605510
      Legend.Alignment = laTop
      Legend.Color = clBtnFace
      Legend.Font.Color = clWhite
      Legend.HorizMargin = 10
      Legend.Shadow.Color = clGray
      Legend.Visible = False
      MarginRight = 8
      Title.Brush.Color = clBlue
      Title.Color = clBtnFace
      Title.Font.Color = clMenuText
      Title.Font.Height = -13
      Title.Frame.Width = 2
      Title.Text.Strings = (
        '       Vertical Movement')
      Title.Visible = False
      Title.AdjustFrame = False
      BottomAxis.Axis.Color = 4605510
      BottomAxis.Axis.Visible = False
      BottomAxis.Grid.Visible = False
      BottomAxis.Increment = 5.000000000000000000
      BottomAxis.LabelsFormat.Font.Color = 15527148
      BottomAxis.LabelsSeparation = 20
      BottomAxis.TickLength = 6
      DepthAxis.Axis.Color = 4605510
      DepthTopAxis.Axis.Color = 4605510
      Frame.Color = 4605510
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.Axis.Color = 4605510
      LeftAxis.Axis.Visible = False
      LeftAxis.LabelsFormat.Font.Color = 15527148
      LeftAxis.Maximum = 20.000000000000000000
      LeftAxis.Minimum = -20.000000000000000000
      LeftAxis.Ticks.Visible = False
      LeftAxis.Title.Font.Color = 15527148
      LeftAxis.Title.Font.Style = [fsBold]
      RightAxis.Axis.Color = 4605510
      TopAxis.Axis.Color = 4605510
      View3D = False
      Zoom.Pen.Mode = pmNotXor
      BevelOuter = bvNone
      Color = 6974058
      TabOrder = 9
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
      object YGraphLabelPanel: TPanel
        Left = 459
        Top = 3
        Width = 25
        Height = 25
        BevelOuter = bvNone
        Caption = 'V'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object VerticalAnchoredFlagPanel: TColorPanel
        Left = 461
        Top = 64
        Width = 21
        Height = 21
        Caption = 'A'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object VerticalFixedFlagPanel: TColorPanel
        Left = 461
        Top = 64
        Width = 21
        Height = 21
        Hint = 
          'Anchored to another tracking point ("A")'#13#10'or not allowed to move' +
          ' in this dimension ("F")'
        Caption = 'F'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        Visible = False
      end
      object Series20: TLineSeries
        SeriesColor = clBlack
        Title = 'Tracking Points'
        Brush.BackColor = clDefault
        Pointer.Brush.Color = clYellow
        Pointer.Brush.Gradient.EndColor = clBlack
        Pointer.Gradient.EndColor = clBlack
        Pointer.InflateMargins = True
        Pointer.Style = psCircle
        Pointer.Visible = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object Series21: TLineSeries
        SeriesColor = clBlack
        Title = 'Smoothed Points'
        Brush.BackColor = clDefault
        Pointer.Brush.Color = clAqua
        Pointer.Brush.Gradient.EndColor = clBlack
        Pointer.Gradient.EndColor = clBlack
        Pointer.InflateMargins = True
        Pointer.Style = psCircle
        Pointer.Visible = True
        XValues.DateTime = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
    object HTrackGraph: TChart
      Left = 8
      Top = 290
      Width = 500
      Height = 170
      Hint = 
        'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
        'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
        'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
        'downwards to zoom in, upwards to zoom out'
      BackImage.Inside = True
      BackWall.Color = clBtnFace
      BackWall.Pen.Color = 4605510
      Legend.Alignment = laTop
      Legend.Color = clBtnFace
      Legend.Font.Color = clWhite
      Legend.HorizMargin = 10
      Legend.Shadow.Color = clGray
      Legend.Visible = False
      MarginRight = 8
      Title.Brush.Color = clBlue
      Title.Color = clBtnFace
      Title.Font.Color = clMenuText
      Title.Font.Height = -13
      Title.Frame.Width = 2
      Title.Text.Strings = (
        '     Horizontal Movement')
      Title.Visible = False
      Title.AdjustFrame = False
      BottomAxis.Axis.Color = clWhite
      BottomAxis.Axis.Visible = False
      BottomAxis.Grid.Visible = False
      BottomAxis.Increment = 5.000000000000000000
      BottomAxis.LabelsFormat.Font.Color = 14737632
      BottomAxis.LabelsSeparation = 20
      BottomAxis.TickLength = 6
      Frame.Color = 4605510
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.Axis.Color = clWhite
      LeftAxis.Axis.Visible = False
      LeftAxis.LabelsFormat.Font.Color = 14737632
      LeftAxis.Maximum = 20.000000000000000000
      LeftAxis.Minimum = -20.000000000000000000
      LeftAxis.Title.Font.Color = 14737632
      LeftAxis.Title.Font.Style = [fsBold]
      LeftAxis.Title.Pen.Color = clWhite
      View3D = False
      Zoom.Pen.Mode = pmNotXor
      BevelOuter = bvNone
      Color = 6974058
      TabOrder = 10
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
      object XGraphLabelPanel: TPanel
        Left = 459
        Top = 3
        Width = 25
        Height = 25
        BevelOuter = bvNone
        Caption = 'H'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object HorizontalFixedFlagPanel: TColorPanel
        Left = 461
        Top = 64
        Width = 21
        Height = 21
        Caption = 'F'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object HorizontalAnchoredFlagPanel: TColorPanel
        Left = 461
        Top = 64
        Width = 21
        Height = 21
        Hint = 
          'Anchored to another tracking point ("A")'#13#10'or not allowed to move' +
          ' in this dimension ("F")'
        Caption = 'A'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -14
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        Visible = False
      end
      object LineSeries1: TLineSeries
        SeriesColor = clBlack
        Title = 'Tracking Points'
        Brush.BackColor = clDefault
        Pointer.Brush.Color = clYellow
        Pointer.Brush.Gradient.EndColor = clBlack
        Pointer.Gradient.EndColor = clBlack
        Pointer.InflateMargins = True
        Pointer.Style = psCircle
        Pointer.Visible = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object LineSeries2: TLineSeries
        SeriesColor = clBlack
        Title = 'Smoothed Points'
        Brush.BackColor = clDefault
        Pointer.Brush.Color = clAqua
        Pointer.Brush.Gradient.EndColor = clBlack
        Pointer.Gradient.EndColor = clBlack
        Pointer.InflateMargins = True
        Pointer.Style = psCircle
        Pointer.Visible = True
        XValues.DateTime = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
    object StatusBar: TStatusBar
      Left = 0
      Top = 700
      Width = 512
      Height = 20
      Panels = <
        item
          Width = 200
        end
        item
          Width = 200
        end
        item
          Width = 50
        end>
    end
    object ToolbarContainerPanel: TColorPanel
      Left = 20
      Top = 636
      Width = 340
      Height = 30
      AutoSize = True
      BevelOuter = bvNone
      Caption = 'ToolbarContainerPanel'
      TabOrder = 7
      Visible = False
      object TBDock1: TCoolBar
        Left = 0
        Top = 0
        Width = 340
        Height = 30
        Bands = <>
        object TBToolbar1: TToolBar
          Left = 0
          Top = 0
          Width = 150
          Height = 29
          ButtonHeight = 26
          ButtonWidth = 27
          Caption = 'Flicker '
          Images = ButtonImageList
          TabOrder = 0
          object PreviewFrameButton: TToolButton
            Left = 0
            Top = 0
            Hint = 'Preview One Frame'
            Caption = 'Preview 1'
            ImageIndex = 0
            OnClick = PreviewFrameButtonClick
          end
          object PreviewAllButton: TToolButton
            Left = 27
            Top = 0
            Hint = 'Preview between the marks'
            Caption = 'Preview'
            ImageIndex = 2
            OnClick = RenderButtonClick
          end
          object RenderButton: TToolButton
            Left = 54
            Top = 0
            Hint = 'Render to disk between the marks'
            Caption = 'Render '
            ImageIndex = 1
            OnClick = RenderButtonClick
          end
          object StopButton: TToolButton
            Left = 81
            Top = 0
            Hint = 'Stop after the next frame is done'
            Caption = 'Stop   '
            ImageIndex = 3
            OnClick = StopButtonClick
          end
          object HighLightButton: TToolButton
            Left = 108
            Top = 0
            Hint = 'Show highligh or fix'
            Caption = 'Highlight'
            ImageIndex = 5
            Visible = False
          end
          object PDLCaptureTBItem: TToolButton
            Left = 135
            Top = 0
            Hint = 'Add event to the PDL'
            Caption = 'PDL'
            OnClick = PDLCaptureTBItemClick
          end
        end
      end
    end
    object ResumeGroupBox: TGroupBox
      Left = 358
      Top = 636
      Width = 150
      Height = 45
      Caption = 'Resume'
      TabOrder = 8
      Visible = False
      object GotoTCButton: TSpeedButton
        Left = 14
        Top = 15
        Width = 23
        Height = 22
        Hint = 'Go to this timecode'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333FF3333333333333003333333333333F77F33333333333009033
          333333333F7737F333333333009990333333333F773337FFFFFF330099999000
          00003F773333377777770099999999999990773FF33333FFFFF7330099999000
          000033773FF33777777733330099903333333333773FF7F33333333333009033
          33333333337737F3333333333333003333333333333377333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        OnClick = GotoTCButtonClick
      end
      object SetStartTCButton: TSpeedButton
        Left = 110
        Top = 15
        Width = 23
        Height = 22
        Hint = 'Set to displayed timecode'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFF8888FFFFFFFFFFFF8888FFFFFFFFFF88111178F
          FFFFFFF88111178FFFFFFF8711111178FFFFFF8717771178FFFFF88119191117
          FFFFF88177777717FFFFF811979191118FFFF817777777718FFFF819D9791911
          8FFFF817787777718FFFF81DDBD791118FFFF817887777718FFFF81DB8BD7911
          8FFFF818888777118FFFFF71DBD79117FFFFFF7188777717FFFFFF811D791118
          FFFFFF8117777718FFFFFFF87111178FFFFFFFF87111178FFFFFFFFFF8888FFF
          FFFFFFFFF8888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
        OnClick = SetStartTCButtonClick
      end
      object ResumeTCEdit: VTimeCodeEdit
        Left = 38
        Top = 15
        Width = 72
        Height = 19
        TabOrder = 0
        AllowVTRCopy = False
      end
    end
    object ProcessingRegionGroupBox: TGroupBox
      Left = 31
      Top = 159
      Width = 120
      Height = 104
      Caption = ' Processing Region '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      object TopUpButton: TSpeedButton
        Left = 44
        Top = 33
        Width = 17
        Height = 17
        Hint = 'Move top edge up'#13#10'(press Shift for faster, Ctrl for slower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A36363636363636363636363636363636363636
          36363636363636363636363636363636363636366A6A6A6A6A6A5E5E5E5E5E5E
          5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E
          5E6A6A6A00006A6A6A6A6A6A3636360000000000000000000000000000000000
          000000000000000000003636366A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252
          52525252525252525252525252525252525252525252525E5E5E6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A363636000000000000000000000000000000000000
          0000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252
          525252525252525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252
          52525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = TopUpButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object TopDownButton: TSpeedButton
        Left = 60
        Top = 33
        Width = 17
        Height = 17
        Hint = 'Move top edge down'#13#10'(press Shift for faster, Ctrl for slower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A36
          36366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636360000003636
          366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000363636
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A3636360000000000000000000000000000000000000000003636366A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252525252525252
          52525252525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A36363600
          00000000000000000000000000000000000000000000000000003636366A6A6A
          6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252525252525252525252
          525252525252525E5E5E6A6A6A6A6A6A00006A6A6A3636363636363636363636
          363636363636363636363636363636363636363636363636363636366A6A6A6A
          6A6A5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E
          5E5E5E5E5E5E5E5E5E6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = TopDownButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object BottomUpButton: TSpeedButton
        Left = 44
        Top = 84
        Width = 17
        Height = 17
        Hint = 'Move bottom edge up'#13#10'(press Shift for faster, Ctrl for slower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A36363636363636363636363636363636363636
          36363636363636363636363636363636363636366A6A6A6A6A6A5E5E5E5E5E5E
          5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E
          5E6A6A6A00006A6A6A6A6A6A3636360000000000000000000000000000000000
          000000000000000000003636366A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252
          52525252525252525252525252525252525252525252525E5E5E6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A363636000000000000000000000000000000000000
          0000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252
          525252525252525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252
          52525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = BottomUpButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object BottomDownButton: TSpeedButton
        Left = 60
        Top = 84
        Width = 17
        Height = 17
        Hint = 'Move bottom edge down'#13#10'(press Shift for faster, Ctrl for slower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A36
          36366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636360000003636
          366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000363636
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A3636360000000000000000000000000000000000000000003636366A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252525252525252
          52525252525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A36363600
          00000000000000000000000000000000000000000000000000003636366A6A6A
          6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252525252525252525252
          525252525252525E5E5E6A6A6A6A6A6A00006A6A6A3636363636363636363636
          363636363636363636363636363636363636363636363636363636366A6A6A6A
          6A6A5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E
          5E5E5E5E5E5E5E5E5E6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = BottomDownButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object LeftLeftButton: TSpeedButton
        Left = 8
        Top = 50
        Width = 17
        Height = 17
        Hint = 
          'Move left edge to the left'#13#10'(press Shift for faster, Ctrl for sl' +
          'ower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A3636363636366A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A36
          36360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636360000000000
          000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E5252525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A3636360000000000000000000000003636366A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252
          52525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252
          525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A3636360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A3636363636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = LeftLeftButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object LeftRightButton: TSpeedButton
        Left = 8
        Top = 65
        Width = 17
        Height = 17
        Hint = 
          'Move left edge to the right'#13#10'(press Shift for faster, Ctrl for s' +
          'lower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636
          363636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A363636000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A36363600000000000036
          36366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636360000000000000000003636
          366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000000000363636
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252
          525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A3636360000000000000000000000003636366A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252525252
          52525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525E5E5E6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636
          360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A363636000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636363636366A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = LeftRightButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object RightLeftButton: TSpeedButton
        Left = 95
        Top = 50
        Width = 17
        Height = 17
        Hint = 
          'Move right edge to the left'#13#10'(press Shift for faster, Ctrl for s' +
          'lower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A3636363636366A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A36
          36360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3636360000000000
          000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E5252525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A3636360000000000000000000000003636366A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252
          52525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252
          525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A3636360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A3636360000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A3636363636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = RightLeftButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object RightRightButton: TSpeedButton
        Left = 95
        Top = 66
        Width = 17
        Height = 17
        Hint = 
          'Move right edge to the right'#13#10'(press Shift for faster, Ctrl for ' +
          'slower)'
        Flat = True
        Glyph.Data = {
          9A050000424D9A0500000000000036000000280000001E0000000F0000000100
          18000000000064050000CE0E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636
          363636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A363636000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A36363600000000000036
          36366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636360000000000000000003636
          366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A363636000000000000000000000000363636
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252
          525252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A3636360000000000000000000000000000003636366A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A3636360000000000000000000000003636366A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E52525252525252525252
          52525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A3636360000000000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525E5E5E6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636
          360000000000003636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A363636000000
          3636366A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636363636366A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A3636366A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        NumGlyphs = 2
        OnClick = RightRightButtonClick
        OnMouseDown = AutoMoveButtonMouseDown
        OnMouseUp = AutoMoveButtonMouseUp
      end
      object ColorPickButton: TSpeedButton
        Left = 26
        Top = 50
        Width = 34
        Height = 34
        Hint = 
          'Use color picker to set region to exclude from processing'#13#10'(or h' +
          'old Ctrl+Shift and click on the matte)'
        AllowAllUp = True
        GroupIndex = 1
        Glyph.Data = {
          D2080000424DD208000000000000360000002800000026000000130000000100
          1800000000009C080000C30E0000C30E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A5252522A2A2A5252526A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5E5E5E5E5E5E5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A0000004848484848486A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252
          525E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A525252C8
          C8C82A2A2A4848485252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A5252525252525252
          526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A000000FFFFFF4848480000002A
          2A2A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5252526A6A6A5E5E5E5252525252526A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A484848C8C8C8FFFFFFC8C8C80000002A2A2A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
          6A6A6A6A6A6A6A6A6A5252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A5E5E5E0000
          00C8C8C8FFFFFFC8C8C80000002A2A2A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A
          5252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A5E5E5E000000C8C8C8FFFFFFC8C8
          C80000002A2A2A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A5252525252526A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5E5E5E000000C8C8C8FFFFFFC8C8C80000002A2A2A5252
          524848482A2A2A4848486A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E5252526A6A6A6A6A6A6A6A6A5252525252525E5E5E5E5E5E5252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          5E5E5E000000C8C8C8FFFFFFC8C8C80000000000002A2A2A0000002A2A2A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A
          6A6A6A6A6A5252525252525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E000000C8C8C8
          FFFFFF0000000000000000000000004848486A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A52525252525252
          52525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E000000000000000000000000000000
          2A2A2A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A5E5E5E5252525252525252525252525252525E5E5E6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A0000000000000000000000000000000000000000005252526A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252
          525252525252525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A00006A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252520000000000002A2A2A00
          00000000000000000000000000005252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252525252525252525252
          525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5252520000000000002A2A2A2A2A2A00000000000000000000
          00000000005252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
          5252525252525E5E5E5252525252525252525252525252525252525E5E5E6A6A
          6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A5252520000000000000000000000000000004848486A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
          5252525252525252525252525252525E5E5E6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252
          520000000000000000000000005252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E525252525252525252
          5252525E5E5E6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252520000000000005252
          526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        Layout = blGlyphRight
        NumGlyphs = 2
        OnClick = ColorPickButtonClick
      end
      object AllMatButton: TBitBtn
        Left = 60
        Top = 50
        Width = 34
        Height = 34
        Hint = 'Reset processing region to be full frame'
        Caption = 'All'
        TabOrder = 0
        TabStop = False
        OnClick = AllMatButtonClick
      end
      object ShowMattingBoxCBox: TCheckBox
        Left = 62
        Top = 16
        Width = 57
        Height = 17
        Hint = 'Show the processing region outline (Ctrl-T)'
        TabStop = False
        Caption = 'Show'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        OnClick = ShowMattingBoxCBoxClick
      end
      object UseMattingCBox: TCheckBox
        Left = 13
        Top = 16
        Width = 47
        Height = 17
        Hint = 'Enable the processing region (5 key)'
        TabStop = False
        Caption = 'Use'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = UseMattingCBoxClick
      end
    end
    object TrackingPointsGroupBox: TGroupBox
      Left = 32
      Top = 52
      Width = 252
      Height = 106
      Caption = ' Tracking Points '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object DeletePointButton: TBitBtn
        Left = 92
        Top = 20
        Width = 73
        Height = 25
        Hint = 'Delete selected tracking points (A)'
        Caption = 'Delete'
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000CE0E0000D80E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252
          520000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525E5E5E6A6A6A6A6A6A6A6A6A
          5252520000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252
          523A3A3A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252526A6A6A6A6A6A6A6A6A525252
          0000000000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252520000
          005252526A6A6A6A6A6A6A6A6A5252525252525252525252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A525252
          0000000000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A5252520000005E5E
          5E6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          5252520000000000000000005E5E5E6A6A6A6A6A6A5252520000000000005E5E
          5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525E5E5E6A
          6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5252520000000000000000005E5E5E5252520000000000005E5E5E6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525E
          5E5E5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A5252520000000000000000000000000000005E5E5E6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525252525252525252
          52525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5252520000000000000000005E5E5E6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525252525252
          52525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A5252520000000000000000000000000000005E5E5E6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52525252525252525252
          52525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A5252520000000000000000005E5E5E5252520000005E5E5E6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525E
          5E5E5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A525252
          0000000000000000000000005E5E5E6A6A6A6A6A6A5252520000000000005E5E
          5E6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525252525E5E5E6A
          6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A000000
          0000000000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A5252520000000000
          005E5E5E6A6A6A6A6A6A6A6A6A5252525252525252525252525E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A000000
          0000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252520000
          003A3A3A5E5E5E6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
        NumGlyphs = 2
        TabOrder = 3
        TabStop = False
        OnClick = DeletePointButtonClick
      end
      object ShowPointCBox: TCheckBox
        Left = 13
        Top = 84
        Width = 57
        Height = 17
        Hint = 'Show tracking boxes (Shift+T)'
        TabStop = False
        Caption = 'Show'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        OnClick = ShowPointCBoxClick
      end
      object ShowRenderPointCBox: TCheckBox
        Left = 80
        Top = 51
        Width = 105
        Height = 17
        TabStop = False
        Caption = 'Show in Render'
        TabOrder = 10
        Visible = False
      end
      object AutoSaveCBox: TCheckBox
        Left = 92
        Top = 103
        Width = 97
        Height = 17
        Caption = 'Auto Save'
        Checked = True
        State = cbChecked
        TabOrder = 8
        Visible = False
      end
      object AutoLoadCBox: TCheckBox
        Left = 172
        Top = 103
        Width = 71
        Height = 17
        Caption = 'Auto Load'
        TabOrder = 9
        Visible = False
      end
      object SavePointsButton: TBitBtn
        Left = 172
        Top = 20
        Width = 73
        Height = 25
        Hint = 'Save tracking points (Ctrl+S)'
        Caption = 'Save'
        Glyph.Data = {
          CE070000424DCE07000000000000360000002800000024000000120000000100
          18000000000098070000000000000000000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000000000000000
          000000000000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A52
          5252525252525252525252525252525252525252525252525252525252525252
          5252525252525252526A6A6A6A6A6A6A6A6A6A6A6A000000000000909090C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C800000052
          52526A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A6A
          6A0000002A2A2A000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C89090900000006A6A6A6A6A6A6A6A6A5252525E5E5E525252
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E5252526A6A6A6A6A6A6A6A6A0000003A3A3A000000C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C80000006A6A6A6A6A6A
          6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A0000003A3A3A00
          0000909090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C80000005252526A6A6A6A6A6A5252526A6A6A5252525E5E5E6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A
          6A6A6A6A6A0000003A3A3A2A2A2A000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C89090900000006A6A6A6A6A6A5252526A6A
          6A5E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A0000003A3A3A3A3A3A000000C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C80000
          006A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A000000
          3A3A3A3A3A3A0000000000000000000000000000000000000000000000000000
          000000000000000000000000006A6A6A6A6A6A5252526A6A6A6A6A6A52525252
          5252525252525252525252525252525252525252525252525252525252525252
          5252526A6A6A6A6A6A0000003A3A3A3A3A3A3A3A3A3A3A3A000000909090C8C8
          C8C8C8C8C8C8C80000003A3A3A3A3A3A0000006A6A6A6A6A6A6A6A6A6A6A6A52
          52526A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A6A6A525252
          6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000000000000000002A2A
          2A3A3A3A3A3A3A000000C8C8C8C8C8C8C8C8C80000000000000000000000006A
          6A6A6A6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A525252
          6A6A6A6A6A6A6A6A6A5252525252525252525252526A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A000000000000000000909090C8C8C8C8C8C8C8C8C800
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000909090C8
          C8C89090900000009090900000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A5E5E5E5252525E5E
          5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A000000909090C8C8C89090900000006A6A6A0000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A
          6A5E5E5E5252526A6A6A5252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A000000909090C8C8C89090900000006A6A6A6A6A6A
          6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A5252525E5E5E6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A5252526A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000909090
          0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E5252526A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
          52526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
        NumGlyphs = 2
        TabOrder = 4
        TabStop = False
        OnClick = SavePointsButtonClick
      end
      object LoadPointsButton: TBitBtn
        Left = 172
        Top = 52
        Width = 73
        Height = 25
        Hint = 'Load tracking points (Ctrl+L)'
        Caption = 'Load'
        Glyph.Data = {
          CE070000424DCE07000000000000360000002800000024000000120000000100
          18000000000098070000000000000000000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000
          000000000000000000000000000000000000000000000000000000000000006A
          6A6A6A6A6A6A6A6A6A6A6A525252525252525252525252525252525252525252
          5252525252525252525252525252525252525252526A6A6A6A6A6A6A6A6A6A6A
          6A000000000000909090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C89090900000005E5E5E6A6A6A6A6A6A6A6A6A5252525252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252
          525E5E5E6A6A6A6A6A6A6A6A6A000000525252000000C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C89090900000006A6A6A6A6A6A
          6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A00000052525200
          0000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C80000006A6A6A6A6A6A6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A
          6A6A6A6A6A000000525252000000909090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C80000005E5E5E6A6A6A6A6A6A5252526A6A
          6A5252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A000000525252525252000000C8C8C8
          C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C89090900000
          006A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A000000
          525252525252000000909090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
          C8C8C8C8C8C8C8C8C8C80000006A6A6A6A6A6A5252526A6A6A6A6A6A5252525E
          5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          5252526A6A6A6A6A6A0000005252525252520000000000000000000000000000
          000000000000000000000000000000000000000000000000006A6A6A6A6A6A52
          52526A6A6A6A6A6A525252525252525252525252525252525252525252525252
          5252525252525252525252525252526A6A6A6A6A6A0000005252525252525252
          525252525252525252525E5E5EC8C8C890909000000052525252525252525200
          00006A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          5E5E5E6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A
          6A000000000000525252525252525252525252000000000000909090C8C8C890
          90900000005252520000000000006A6A6A6A6A6A6A6A6A5252525252526A6A6A
          6A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A5E5E5E5252526A6A6A5252
          525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000006A
          6A6A6A6A6A000000909090C8C8C89090900000009090900000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5252525252525252525252526A6A6A6A6A6A5252525E5E
          5E6A6A6A5E5E5E5252525E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000909090C8C8C8C8C8C8
          C8C8C80000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A6A6A5252526A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A000000C8C8C8C8C8C8C8C8C80000006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A
          6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A000000909090C8C8C8C8C8C8C8C8C80000006A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A5252525E5E5E6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000
          000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A525252525252525252525252525252525252525252
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
        NumGlyphs = 2
        TabOrder = 7
        TabStop = False
        OnClick = LoadPointsButtonClick
      end
      object TrackButton: TBitBtn
        Left = 12
        Top = 20
        Width = 73
        Height = 25
        Hint = 'Start tracking (T)'
        Caption = 'Track'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          CE070000424DCE07000000000000360000002800000024000000120000000100
          18000000000098070000120B0000120B000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A1010100505053939396A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A555555
          5353535E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A02020200000000000015151532
          32323B3B3B4C4C4C6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5252525252525252525656565D5D5D5E5E5E6262626A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A4747471717176A6A6A2A
          2A2A0000000000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6161615757576A6A6A5B5B5B5252525252525252
          525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A4A
          4A4A0000000000001111116A6A6A080808000000000000000000000000000000
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6262625252525252525656
          566A6A6A5454545252525252525252525252525252526A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A2F2F2F0000000000000E0E0E6A6A6A1C1C1C000000
          0000000000001212126A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A5C5C5C5252525252525555556A6A6A5858585252525252525252525656566A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A333333000000000000
          4242426A6A6A3F3F3F0000000000002323236A6A6A1515150B0B0B4D4D4D6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A5D5D5D5252525252526060606A6A6A5F5F5F52
          52525252525959596A6A6A5656565454546262626A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A4141411A1A1A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3B3B
          3B0000000000002525256A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6060605858586A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525A5A5A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A1A1A1A6A6A6A6A6A6A0000000404046A6A
          6A6A6A6A6A6A6A6A6A6A3B3B3B0000000000001515156A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5858586A6A6A6A6A6A5252525353536A6A6A6A6A6A6A6A6A6A6A6A
          5E5E5E5252525252525656566A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A1212
          120000000000000505056A6A6A3A3A3A0000001C1C1C6A6A6A0303030000002A
          2A2A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A565656525252525252535353
          6A6A6A5E5E5E5252525858586A6A6A5353535252525B5B5B6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A4B4B4B0000000000000000002A2A2A51515100000000000000
          00006A6A6A4040402525252E2E2E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A626262
          5252525252525252525B5B5B6363635252525252525252526A6A6A6060605A5A
          5A5C5C5C6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A54545401010100000000000052
          52523E3E3E0000000000000000004040406A6A6A1E1E1E6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6464645252525252525252526363635F5F5F5252525252
          525252526060606A6A6A5858586A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A2F2F2F0A0A0A3333336A6A6A3E3E3E0000000000000000004B4B4B6A6A6A
          3535356A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5C5C5C5454545D5D
          5D6A6A6A5F5F5F5252525252525252526262626A6A6A5D5D5D6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A1313134646466A6A6A6A6A6A5454543A3A3A
          0000001E1E1E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A5656566161616A6A6A6A6A6A6464645E5E5E5252525858586A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A2F2F2F6A6A6A
          6A6A6A6A6A6A6A6A6A4343433737376A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5C5C5C6A6A6A6A6A6A6A6A6A6A6A6A60
          60605E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E2A2A2A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A5E5E5E4343436A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        TabStop = False
        OnClick = TrackButtonClick
      end
      object NextPointButton: TBitBtn
        Left = 92
        Top = 52
        Width = 73
        Height = 25
        Hint = 'Select next tracking point'
        Caption = 'Next'
        Enabled = False
        Glyph.Data = {
          E2040000424DE20400000000000036000000280000001E0000000D0000000100
          180000000000AC040000C40E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E
          5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E
          5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A0000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A0000000000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A0000000000000000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252525252525E5E5E6A6A
          6A6A6A6A00006A6A6A0000000000000000000000000000000000000000000000
          000000000000000000000000005E5E5E6A6A6A6A6A6A52525252525252525252
          52525252525252525252525252525252525252525252525252525E5E5E6A6A6A
          00006A6A6A000000000000000000000000000000000000000000000000000000
          0000000000000000000000005E5E5E6A6A6A5252525252525252525252525252
          525252525252525252525252525252525252525252525252525E5E5E00006A6A
          6A00000000000000000000000000000000000000000000000000000000000000
          00000000005E5E5E6A6A6A6A6A6A525252525252525252525252525252525252
          5252525252525252525252525252525252525E5E5E6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000005E5E
          5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
          52525252525252525252525E5E5E6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000005E5E5E6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252
          525252525E5E5E6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A0000000000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525E5E5E
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A0000005E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A0000}
        NumGlyphs = 2
        TabOrder = 6
        TabStop = False
        OnClick = NextPointButtonClick
      end
      object PrevPointButton: TBitBtn
        Left = 13
        Top = 51
        Width = 73
        Height = 25
        Hint = 'Select previous tracking point'
        Caption = ' Prev'
        Enabled = False
        Glyph.Data = {
          E2040000424DE20400000000000036000000280000001E0000000D0000000100
          180000000000AC040000C40E0000C40E000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A5E5E5E0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E0000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252526A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A5E5E5E000000000000
          0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A5E5E5E5252525252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A5E5E5E0000000000000000000000006A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
          5252525252525252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A00006A6A6A5E5E5E0000000000000000000000000000000000000000
          000000000000000000000000000000006A6A6A6A6A6A5E5E5E52525252525252
          52525252525252525252525252525252525252525252525252525252526A6A6A
          00005E5E5E000000000000000000000000000000000000000000000000000000
          0000000000000000000000006A6A6A5E5E5E5252525252525252525252525252
          525252525252525252525252525252525252525252525252526A6A6A00006A6A
          6A5E5E5E00000000000000000000000000000000000000000000000000000000
          00000000000000006A6A6A6A6A6A5E5E5E525252525252525252525252525252
          5252525252525252525252525252525252525252526A6A6A00006A6A6A6A6A6A
          5E5E5E0000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252525252526A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A5E
          5E5E0000000000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252525252526A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A5E5E
          5E0000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A5E5E5E5252525252526A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E
          0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E5E6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A5E5E5E6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A0000}
        NumGlyphs = 2
        TabOrder = 5
        TabStop = False
        OnClick = PrevPointButtonClick
      end
      object ClearOnShotChangeCBox: TCheckBox
        Left = 91
        Top = 84
        Width = 145
        Height = 17
        Hint = 'Auto-delete all tracking points'#13#10'when marks are changed (0 key)'
        TabStop = False
        Caption = 'Clear on Shot Change'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 2
        OnClick = ClearOnShotChangeCBoxClick
      end
    end
    object AnchorFramesGroupBox: TGroupBox
      Left = 164
      Top = 159
      Width = 120
      Height = 104
      Hint = 
        'Tracking points on anchor frames are used as reference.'#13#10'Anchor ' +
        'frames are not dewarped.'
      Caption = ' Anchor Frames '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label6: TLabel
        Left = 128
        Top = 18
        Width = 24
        Height = 13
        Caption = 'At In'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 168
        Top = 18
        Width = 32
        Height = 13
        Caption = 'At Out'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object SwapButton: TBitBtn
        Left = 125
        Top = 66
        Width = 63
        Height = 25
        Caption = 'Swap'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000130B0000130B000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A4D
          4D4D6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0E0E0E5555556A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A65656513131313131313131314
          14141414145A5A5A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1AB8B8B86A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A21212121212121212121212121
          21212121219C9C9C6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A2828286A6A6A6A6A6A6A6A6A282828A0A0A06A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A3030306A6A6A6A6A6A6A6A6AA3
          A3A36A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A5A5A5A6A6A6A6A6A6A6A6A6A3030306A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5A5A5A2828286A6A6A6A6A
          6A6A6A6A2828286A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A5A5A5A2121212121212121212121212121212121216A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6AB8B8B81A1A1A1A1A1A1A1A1A1A1A1A1A1A
          1A1A1A1A1A1A1A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A9C9C9C1414141414141313131313131313136565656A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6AA0A0A00E0E0E6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6AA0A0A06A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
        TabOrder = 0
        TabStop = False
        OnClick = SwapButtonClick
      end
      object AnchorNoneRadioButton: TRadioButton
        Left = 12
        Top = 78
        Width = 105
        Height = 17
        Hint = 
          'No reference frame - all frames are processed'#13#10'Press Shift+2 cyc' +
          'les through Anchor options'
        Caption = 'None'
        TabOrder = 1
        OnClick = AnchorRadioButtonClick
      end
      object AnchorBothRadioButton: TRadioButton
        Left = 12
        Top = 58
        Width = 105
        Height = 17
        Hint = 
          'Anchor at both the first and last frames'#13#10'Press Shift+2 cycles t' +
          'hrough Anchor options'
        Caption = 'Both'
        Checked = True
        TabOrder = 2
        TabStop = True
        OnClick = AnchorRadioButtonClick
      end
      object AnchorLastRadioButton: TRadioButton
        Left = 12
        Top = 38
        Width = 105
        Height = 17
        Hint = 
          'Use last frame as reference'#13#10'Press Shift+2 cycles through Anchor' +
          ' options'
        Caption = 'Last Frame'
        TabOrder = 3
        OnClick = AnchorRadioButtonClick
      end
      object AnchorFirstRadioButton: TRadioButton
        Left = 12
        Top = 18
        Width = 105
        Height = 17
        Hint = 
          'Use first frame as reference'#13#10'Press Shift+2 cycles through Ancho' +
          'r options'
        Caption = 'First Frame'
        TabOrder = 4
        OnClick = AnchorRadioButtonClick
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 512
      Height = 32
      BevelOuter = bvNone
      TabOrder = 12
      object Label1: TLabel
        Left = 224
        Top = 0
        Width = 63
        Height = 19
        Caption = 'Dewarp'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
    end
    inline ExecButtonsToolbar: TExecButtonsFrame
      Left = 12
      Top = 634
      Width = 480
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      ExplicitLeft = 12
      ExplicitTop = 634
      ExplicitWidth = 480
      inherited ButtonPressedNotifier: TSpeedButton
        OnClick = ExecButtonsToolbar_ButtonPressedNotifier
      end
    end
    inline ExecStatusBar: TExecStatusBarFrame
      Left = 16
      Top = 682
      Width = 480
      Height = 38
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      ExplicitLeft = 16
      ExplicitTop = 682
      inherited BackPanel: TPanel
        inherited ReadoutPanel: TPanel
          inherited ProgressPanelLight: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited PercentValue1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue1: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue1: TLabel
              Anchors = [akLeft]
            end
          end
          inherited ProgressPanelDark: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited RemainingLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue2: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PercentValue2: TLabel
              Anchors = [akLeft]
            end
          end
        end
      end
    end
    object TrackWarnPanel: TPanel
      Left = 49
      Top = 438
      Width = 419
      Height = 30
      BevelOuter = bvNone
      Caption = 'Tracking data not available. Point(s) have not been tracked.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 17
    end
    object TabStopDoNotDeletePanel: TPanel
      Left = 10
      Top = 94
      Width = 21
      Height = 41
      BevelOuter = bvNone
      Ctl3D = True
      ParentBackground = False
      ParentColor = True
      ParentCtl3D = False
      TabOrder = 2
      TabStop = True
      OnEnter = TabToBegining
    end
    object DataFillGroupBox: TGroupBox
      Left = 304
      Top = 211
      Width = 177
      Height = 52
      Caption = ' Missing Data Fill '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 19
      DesignSize = (
        177
        52)
      object MissingDataBlackRadioButton: TRadioButton
        Left = 24
        Top = 12
        Width = 45
        Height = 17
        Hint = 'Fill missing areas with black'
        Caption = 'Black'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = MissingDataRadioButtonClick
      end
      object MissingDataWhiteRadioButton: TRadioButton
        Left = 24
        Top = 30
        Width = 49
        Height = 17
        Hint = 'Fill missing areas with white'
        Anchors = [akLeft, akTop, akBottom]
        Caption = 'White'
        TabOrder = 1
        OnClick = MissingDataRadioButtonClick
      end
      object MissingDataPrevFrameRadioButton: TRadioButton
        Left = 88
        Top = 12
        Width = 81
        Height = 17
        Hint = 'Fill missing areas with pixels'#13#10'from the previous frame'
        Anchors = [akLeft, akTop, akBottom]
        Caption = 'Prev Frame'
        Checked = True
        TabOrder = 2
        TabStop = True
        OnClick = MissingDataRadioButtonClick
      end
    end
    object EditAnchorsCheckBox: TCheckBox
      Left = 247
      Top = 271
      Width = 80
      Height = 17
      Hint = 
        'Enable constraints for the selected track'#13#10'point, or for the nex' +
        't one drawn'
      Caption = 'Edit Anchors'
      TabOrder = 20
      OnClick = EditAnchorsCheckBoxClick
    end
    inline PresetsFrame: TPresetsFrame
      Left = 32
      Top = 267
      Width = 177
      Height = 22
      AutoSize = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
      ExplicitLeft = 32
      ExplicitTop = 267
    end
  end
  object ButtonImageList: TImageList
    Height = 20
    Width = 20
    Left = 68
    Bitmap = {
      494C010109000E00040014001400FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000500000003C0000000100200000000000004B
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000A4A0A000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C0000000000000000000C0C0C000C0DCC000C0C0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000A4A0A000C0C0C000C0C0C000C0C0C000000000000000
      0000C0C0C000C0C0C000A4A0A000C0C0C000C0C0C000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0DC
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00F0FB
      FF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF00C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF00C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000F0FBFF00F0FBFF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000FFFFFF00F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00C0C0C000C0C0C000C0C0C000C0C0C000A4A0A000C0C0C000FFFFFF00F0FB
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000A4A0A000C0C0C000C0C0C000C0C0C000F0FBFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000F0FBFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F0FB
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000C0C0C0000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000000000
      0000000000000000000000000000808080000000000080808000000000008080
      8000000000000000000000FF0000000000000000000000000000000000008080
      8000000000008080800000000000000000000000000000000000000000008080
      800000000000808080000000000080808000000000000000000000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      800080808000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000080808000000000008080800000000000000000008080
      8000000000000000000000000000000000008080800000000000808080000000
      0000808080000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000808080000000000000000000000000000000
      0000808080000000000080808000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000000000000000000000FF00000000000000FF000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      000000000000808080000000000080808000000000000000000000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000FF000000FF000000FF000000
      0000000000000000000000000000808080000000000000000000808080000000
      00000000000000FF000000000000000000000000000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000080808000000000000000000000FF0000000000000000
      0000000000000000000000000000806060004020200000000000000000000000
      0000402020000000000000000000806060000000000000000000000000000000
      0000402020000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000FF000000FF000000FF000000
      0000000000000000000000000000808080000000000080808000000000000000
      FF000000FF000000FF0000FF00000000000000FF000000000000000000008080
      8000000000008080800000000000000000000000000000000000000000008080
      800000000000808080000000000080808000000000000000000000FF00000000
      000000000000000000000000000080808000A4A0A00080606000808080008080
      8000000000000000000000000000808080008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000FF000000FF000000FF000000
      0000000000000000000080808000808080000000000000000000808080000000
      FF000000FF000000FF0000000000000000000000000000FF0000000000000000
      0000808080000000000080808000000000000000000000000000808080008080
      8000000000000000000080808000000000000000000000FF0000000000000000
      000000000000000000000000000080808000F0FBFF00A4A0A000A4A0A0008080
      800000000000000000000000000080808000F0FBFF00A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000080808000000000000000
      0000000000000000000080808000808080000000000080808000000000000000
      FF000000FF000000FF0000FF00000000000000FF000000000000000000008080
      8000000000008080800000000000000000000000000000000000808080008080
      80000000000080808000000000000000000000FF00000000000000FF00000000
      0000000000000000000000000000808080000000000080808000806060008080
      8000000000000000000000000000808080000000000080808000806060008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      00000000000000FF000000000000000000000000000000FF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000FF0000000000000000
      0000000000000000000000000000808080000000000080808000808080008080
      800000000000000000000000000080808000F0FBFF0080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      000000FF00000000000000FF00000000000000FF000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000080808000000000000000000000FF00000000000000FF00000000
      00000000000000000000000000008080800000000000A4A0A000808080008080
      80000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      80000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      80000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      80000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0DCC000C0DCC0008080
      80000000000000000000000000008080800000000000C0DCC000C0DCC0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000F0FBFF00F0FBFF008080
      80000000000000000000000000008080800000000000F0FBFF00F0FBFF008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000008080
      8000000000000000000000000000808080000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000A4A0A0008080800080808000808080008080
      8000808080000000000000000000808080008080800080808000808080008080
      8000A4A0A0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000FFFF00000000008080
      80000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000008080800000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      80008080800080808000808080008080800080808000C0C0C000C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008080800000000000000000008080800000000000808080000000
      0000000000000000000000000000000000008080800000000000000000000000
      000000000000000000000000000000000000000000000000000080808000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000808080000000000000000000FFFF00008080800080808000000000000000
      0000000000000000000000000000808080000000800000008000000080000000
      8000000080000000800000008000000080000000800000008000000080008080
      800000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000008080800000000000000000000000
      000000000000808080008080800000008000000080000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000080000000
      800080808000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFF000000000000000000008080800000000000000000000000
      0000808080008080800000008000000080000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      800000008000808080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000808080000000
      0000000000008080800000000000808080000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF000000000000FFFF0000FFFF0000000000000000000000000000FFFF0000FF
      FF000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000080808000FFFF0000FFFF0000000000008080800080808000000000000000
      00008080800000008000000080000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000000080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000000000000000
      0000FFFF00008080800080808000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000000000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008080800000000000000000008080800000000000000000000000
      0000000080000000FF000000FF00C0C0C0000000FF000000FF000000FF008080
      80000000FF000000FF0080808000808080000000FF000000FF000000FF000000
      FF000000FF00000080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000FFFF0000FFFF00000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000080000000FF00000000000000FF00000000000000FF000000FF000000
      00000000FF000000FF0000000000C0C0C000808080000000FF00C0C0C0000000
      FF000000FF00000080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF00000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000080000000FF000000FF000000FF00000000000000FF000000FF000000
      00000000FF00808080000000FF000000FF00000000000000FF00C0C0C0000000
      FF000000FF00000080000000800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000FFFF0000FFFF
      0000000000008080800080808000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000008080000000FF00C0C0C00000000000808080000000FF000000FF000000
      00000000FF00C0C0C0000000FF000000FF00000000000000FF00C0C0C0000000
      000000000000008080000000800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000808080000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF000000000000FFFF0000FFFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000008080000000FF0000000000808080000000FF000000FF000000FF000000
      00000000FF00C0C0C0000000FF000000FF00000000000000FF00C0C0C0008080
      800080808000808080000000800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000080000000FF00C0C0C0000000FF00000000000000FF000000FF000000
      00000000FF000000FF00C0C0C0000000FF00C0C0C0000000FF00C0C0C0000000
      FF000000FF00808080000000800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008080000000FF00C0C0C000000000000000FF00C0C0C000C0C0C000C0C0
      C000C0C0C00000008000C0C0C000C0C0C0000000FF000000FF00C0C0C000C0C0
      C000C0C0C000000080000000800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000FFFF0000FFFF0000FFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00000080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000000000000000000000000000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C000C0C0C0000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000800000008000C0C0C00000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C000C0C0C0000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C0000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000080000000
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000800000008000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C0000000
      FF000000FF000000FF000000FF000000FF000000800000008000C0C0C0000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000500000003C0000000100010000000000D00200000000000000000000
      000000000000000000000000FFFFFF00FFFFF0000000000000000000CFFFF000
      0000000000000000C3FFF0000000000000000000C0FFF0000000000000000000
      C03FF0000000000000000000C00FF0000000000000000000C003F00000000000
      00000000C000F0000000000000000000C00030000000000000000000C0001000
      0000000000000000C00010000000000000000000C00030000000000000000000
      C000F0000000000000000000C003F0000000000000000000C00FF00000000000
      00000000C03FF0000000000000000000C0FFF0000000000000000000C3FFF000
      0000000000000000CFFFF0000000000000000000FFFFF0000000000000000000
      F000F8000180001FFFFF0000F801F82E0382E03FFFFF0000FE27F47E0547E05F
      FFFF0000FF2FF81E0381E03E06070000FF3FF40E0140E05E06070000FE07F80C
      0380C0BE06070000FC03F4000140005E86870000FCFFF81C0B81C0BE86070000
      FCCFF4270542705E86870000FC8FFC7B0FC7B0FE86870000FE07FC670FC670FE
      86870000FF07FC6F0FC6F0FE86870000FF87FE6F0FE6F0FE86870000FF87FE54
      0FE540FE86870000FF87FE3B0FE3B0FEE6E70000FFC7F83F07E7F0FE06070000
      FFCFF83F07F7F0FFFFFF0000FFCFF83E17F3E1FFFFFF0000FFCFF8003FF803FF
      FFFF0000FFCFFFC07FFC07FFFFFF0000FFFFFFFFFFFC004FFFFF0000FFFFFFF8
      FFFC000F801F0000FFFFFFF07FE0031F000F0000C0033FE03FE0061E000F0000
      C0023FC01FE00F5800070000C0007F800F000B5000030000C018FF0007000110
      00030000C030FE000300033000010000C07AFC000100007292010000C05AF800
      0000003090810000C008F8000000003110990000C019F8000100017210810000
      C003FC00030000F090010000C003FE00070001F100010000C003FF000F000BF0
      00010000C017FF801F0007F000010000C00FFFC03F000FF800030000C01FFFE0
      7F005FFC00070000FFFFFFF0FF003FFF000F0000FFFFFFF9FF007FFF801F0000
      00000000000000000000000000000000000000000000}
  end
  object UpdateTimer: TTimer
    Interval = 500
    OnTimer = UpdateTimerTimer
    Left = 92
    Top = 65528
  end
  object BitBtnImageList: TImageList
    Left = 134
    Top = 2
    Bitmap = {
      494C01012F003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000C0000000010020000000000000C0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004080C00040A0
      C00080C0E000C0DCC000F0FBFF00C0A0E000F0FBFF00C0DCC000C0DCC000C0C0
      C00080A0C00080A0C00080A0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000060400000602000006020000060400000602000006020000060
      20000000000000000000000000000000000000000000000000004080A0004080
      A00040A0A00040A0C0004080C00040A0C00040A0C00040A0A00040A0A0000080
      80004080A0004080A0004080A00000000000000000000000000080C0E0004080
      C00080C0C00080A0C00080A0C00080C0C00080A0C00080C0C00080A0C00080A0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000604000406040004060
      40004080600080C0A000C0C0A000C0C0A000C0C0A000C0C0A000C0DCC00080C0
      A00000604000406040004080400040806000000000000000000040A0C0004080
      C00080C0C00080E0C00080C0E00080C0C00080E0E000C0DCC00080E0E00080E0
      E00080C0E00040A0A00080A0C000000000000000000000000000000000004080
      A00040A0A00040A0C0004080A00040A0A00040A0C00040A0A00040A0C0004080
      A000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000080A000006020000060
      20000060200000804000006040000060400040A0600000604000408060000060
      200000604000006040000080200000808000000000000000000080A0C00080A0
      E00040A0A00040A0A00080A0C00040A0E00080A0C0004080C00040A0C0004080
      C0004060800080A0A00000000000000000000000000000000000C0DCC00040A0
      C00080A0C00080C0C00080C0E00080C0C00080C0C00080C0C00080C0E00080A0
      C00080A0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000060400040C0
      8000006020000060400000604000006040000060200000604000406040000060
      400040A080004080800000804000000000000000000000000000000000000000
      00008080C0004080C00080C0E00080C0C000C0DCC00040A0C00080C0C0004080
      A00000000000000000000000000000000000000000000000000000000000C0DC
      C00080A0C00040A0C00080C0E00080C0C00080A0C00040A0C0004080C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000060400080C0
      A000C0C0A000F0FBFF00C0DCC000C0DCC000C0DCC000C0DCC000C0C0C000F0FB
      FF0080A0A000C0C0C00000804000000000000000000000000000000000000000
      0000000000008080C00040A0C00080E0C000C0DCC00080C0E0004080A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080C00080C0C00080E0C00080C0C00040A0C000C0DCC0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000060400080C0
      A000A4A0A000C0C0C000C0C0C000C0C0C000C0DCC000C0DCC000C0DCC00080C0
      A00040A0A00080C0800000802000000000000000000000000000000000000000
      0000000000000000000080A0E00080E0C000C0DCC00040A0C0004080A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080C0E00080E0C00080C0C0004080A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006040000060
      40000080400000602000006040008080A00080C0800080A08000406040004060
      200040A0A0000040400000604000000000000000000000000000000000000000
      000000000000000000008080E00080C0E000C0A0E0004080C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080A0E00080C0E00080A0C000C0DCC000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006040000060
      20004080600000000000000000000080600080E0A00000604000000000000000
      0000006040000060200000804000000000000000000000000000000000000000
      0000000000000000000080A0E0004080A0000080800040A0A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080A0E0004080A00040A0A000C0DCC000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006040000060
      4000000000000000000000000000806060000060400040606000000000000000
      0000000000000060200000604000000000000000000000000000000000000000
      000080A0C00080A0C00080E0E000C0A0E000F0FBFF0080C0E00040A0A00040A0
      A000A4A0A0000000000000000000000000000000000000000000000000000000
      000080A0C00080A0C00080C0C000C0A0E00080C0E00040A0C00040A0A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006020000060
      4000000000000000000000000000000000000060200000000000000000000000
      0000000000000060200000604000000000000000000000000000000000000000
      00008080C00080A0C0004080800040A0C00080A0C00080A0C00080C0E00080C0
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000080A0C00040A0A00040A0C00080A0C00040A0C000C0DCC0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006040000080
      6000000000000000000000000000000000000060400000000000000000000000
      0000000000004080400000806000000000000000000000000000000000000000
      00000000000080A0C00080C0E00040A0C0004080A00040C0C0000060A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0DCC00080A0C00040A0C0004080C00040A0C0004080A00080C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000806000006060000080
      4000000000000000000000000000000000000060400000000000000000000000
      0000000000004080400000604000008080000000000000000000000000000000
      00008080C00040A0E00040A0C0004080C0004080E0004080E0004080A0004060
      8000000000000000000000000000000000000000000000000000000000000000
      000080A0C00040A0C00080C0C00080C0E00080C0E00080C0C0004080A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000006020000080
      2000000000000000000000000000000000000080400000000000000000000000
      0000000000000060200000402000008060000000000000000000000000000000
      000080A0C00040A0C000C0C0C000C0DCC000F0FBFF0080C0C00080E0E0004060
      A000000000000000000000000000000000000000000000000000000000000000
      000080A0C00080C0E000F0FBFF00F0FBFF00C0DCC000C0DCC0004080C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006040000080
      4000000000000000000000000000008040000040400000602000000000000000
      0000000000000080600000606000000000000000000000000000000000000000
      00004080A00080C0E000F0FBFF00F0FBFF00F0FBFF00C0DCC000C0DCC0000060
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000080C0E00080C0C00080E0E00080E0E00080A0C000C0DCC0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000080C0E00080A0A00080E0E00080E0E00080C0E0004080A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000008484840084848400848484008484840084848400000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000004040400040402000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00F0FB
      FF00FFFFFF00FFFFFF00F0FBFF0000000000C0A0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000000000000000000000000000008484000084
      8400000000008484840084848400848484008484840084848400000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004020200040202000404020000000
      0000000000008020200080404000402020008040400040404000804040008020
      20004020400080404000402020004040400000000000F0FBFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000000000000000000000000000008484000084
      8400000000008484840084848400848484008484840084848400000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000402020008040
      400080404000402020004020200040202000F0CAA60080606000404040008040
      40004020200040202000FFFFFF004020200000000000FFFFFF0080808000A4A0
      A00080808000C0C0C000FFFFFF000000000080C0C000C0C0C000C0C0C0000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000008040
      4000C0808000FFFFFF00FFFFFF00806060004020200080404000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF004020200000000000FFFFFF00F0FBFF00C0DC
      C000FFFFFF00FFFFFF00C0DCC00000000000C0C0C000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000004020
      2000C0606000F0FBFF00FFFFFF00FFFFFF00F0FBFF0040202000808080008040
      40004040200040202000FFFFFF004020200000000000FFFFFF00808080008080
      800080808000FFFFFF00F0FBFF0040000000C0C0C0000000000000FFFF000020
      4000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00000000000000000000000000FFFF
      FF0000000000000000000000000000000000000000000000000040202000FFFF
      FF00C0606000FFFFFF00FFFFFF0040202000808060008040400040202000F0FB
      FF00FFFFFF00FFFFFF00F0FBFF008020400000000000FFFFFF00FFFFFF00F0FB
      FF00FFFFFF00FFFFFF00FFFFFF00000000004000200000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000000000000000000000000000040404000FFFF
      FF00C0606000FFFFFF00F0FBFF00FFFFFF00FFFFFF00FFFFFF00402020008080
      80004020200040202000FFFFFF008020400000000000FFFFFF00808080008080
      800080808000C0C0C000F0FBFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF0000E0E000808080000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000040202000FFFF
      FF00C0606000FFFFFF00F0FBFF004040400080808000F0CAA60040402000F0FB
      FF00FFFFFF00FFFFFF00F0FBFF008040400000000000FFFFFF00FFFFFF00F0FB
      FF00FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FFFF00F0FB
      FF00F0FBFF0000FFFF00FFFFFF00808080000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000000000000000000000000000040404000F0FB
      FF00C0606000FFFFFF00F0FBFF00FFFFFF00F0FBFF00F0FBFF00404040008060
      80008020400040202000FFFFFF008020200000000000FFFFFF00408080008080
      800080808000FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00F0FBFF0000FFFF00C08080000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000004040
      4000C0606000FFFFFF00FFFFFF0080808000C0DCC00040202000FFFFFF00F0FB
      FF00FFFFFF00FFFFFF00FFFFFF004020200000000000FFFFFF00F0FBFF00F0FB
      FF00F0FBFF00FFFFFF00FFFFFF00000000000000000080E0E00000FFFF008080
      8000808080008080800080808000808080000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000080404000804020004040200040404000FFFFFF00FFFFFF00F0FBFF00F0FB
      FF0080204000F0FBFF00FFFFFF004020200000000000FFFFFF00F0FBFF00F0FB
      FF00FFFFFF00F0FBFF00F0FBFF0000000000C0C0C000C0C0C00000000000C080
      8000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000080404000FFFFFF00F0FBFF00FFFFFF00FFFFFF00F0FBFF00FFFF
      FF0080404000F0FBFF00402020000000000000000000F0FBFF00F0FBFF00F0FB
      FF00F0FBFF00F0FBFF00FFFFFF0000000000C0DCC000C0C0A000C0C0C000C080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000004040400040202000404040004020200080404000402020004040
      4000402020004040400000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      8400008484008484840000848400848484000084840084848400008484008484
      8400008484008484840000848400000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400848484000084840084848400008484008484840000848400848484000084
      8400848484000084840084848400000000000000000000000000000000000000
      00000000000000000000000000000000FF00FFFF00000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008484840000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      8400008484008484840000848400848484000084840084848400008484008484
      840000848400848484000084840000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFF0000FFFF00000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400848484000084840084848400008484008484840000848400848484000084
      840084848400008484008484840000000000000000000000FF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      8400008484008484840000848400848484000084840084848400008484008484
      840000848400848484000084840000000000000000000000FF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      00000000FF00FF000000FF000000FF0000008484840084000000840000000000
      0000000000008484840084000000840000008484840000000000840000008400
      0000848484000000000000000000000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400848484000084840084848400008484008484840000848400848484000084
      840084848400008484008484840000000000000000000000FF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFFFF00FFFF00000000
      FF00FFFFFF00FFFFFF00FFFFFF00FF0000008400000084000000848484000000
      0000000000000000000084000000840000008400000000000000840000008400
      0000848484000000000000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      8400008484008484840000848400848484000084840084848400008484008484
      840000848400848484000084840000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFF0000FFFF00000000FF008484
      8400FFFFFF0084848400FFFFFF00FF0000008400000084000000848484000000
      0000000000000000000084000000840000008400000000000000000000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400848484000084840084848400008484008484840000848400848484000084
      8400848484000084840084848400000000000000000000000000000000000000
      00000000000000000000000000000000FF00FFFF00000000FF00FFFFFF008484
      8400FFFFFF0084848400FFFFFF00FF0000000000000084000000840000000000
      0000000000008484840084000000840000008484840000000000000000000000
      0000840000008400000084848400000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      8400008484008484840000848400848484000084840084848400008484008484
      8400008484008484840000848400000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF00FF0000000000000000000000840000008400
      0000840000008400000084000000848484000000000084000000000000000000
      0000848484008400000084000000848484000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000848484000084840084848400000000000000000000000000000000000000
      00000000000000000000000000000000FF00FF000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FF0000000000000000000000000000000000
      0000000000000000000000000000840000008400000084848400000000000000
      0000000000008400000084000000840000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000008484008484840000848400000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000840000008400000084848400000000000000
      0000000000008400000084000000840000000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008484008484
      8400848484000000000000FFFF00000000000000000000FFFF00000000000084
      8400848484000084840084848400000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000848484008400000084000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840084848400C6C6C600848484000000000084848400C6C6C6008484
      84008484840000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600848484000000000084848400C6C6C600C6C6
      C600C6C6C600C6C6C600000000000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000FFFF00FF000000FF00000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      84008484840084848400C6C6C600C6C6C60000000000C6C6C600C6C6C6008484
      84008484840084848400000000000000000000000000FF000000FF00000000FF
      FF00FF000000FF000000FF000000FF000000FF000000FF000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00FFFF00000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000084848400FF00000000FF
      FF00FF000000FF000000FF000000FF00000000FFFF00FF000000848484000000
      00000000000000000000000000000000000000000000000000000000FF00FFFF
      0000FFFF00000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400000000000000000000000000848484008484
      8400848484008484840000000000000000000000000084848400FF000000FF00
      0000FF00000000FFFF00FF000000FF000000FF000000FF000000000000008484
      840000000000000000000000000000000000000000000000FF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000FF000000FF00
      0000FF00000000FFFF00FF000000FF000000FF00000000000000000000008484
      8400000000000000000000000000000000000000FF00FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      FF000000FF000000FF000000FF000000FF008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FF00
      0000FF000000FF000000FF000000FF000000FF00000000000000000000000000
      000084848400000000000000000000000000000000000000FF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000000000000000000000000000000000000000000C6C6
      C60000000000000000000000000000000000000000000000000084848400FF00
      FF0000FFFF00FF00000000FFFF00FF0000000000000000000000000000000000
      00008484840000000000000000000000000000000000000000000000FF00FFFF
      0000FFFF00000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF0084848400FFFFFF000000FF008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000000000000000000000000000000000000000000C6C6
      C60000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF00000000FFFF00FF0000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      FF00FFFF00000000FF00000000000000FF00FFFFFF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000000000000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000000000008484
      8400FF000000FF000000FF000000000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF000000000000000000000000000000
      00000000FF000000FF00000000000000FF00FFFFFF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF000000FF008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      00008484840084848400C6C6C600000000000000000000000000C6C6C6008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000FF00FF0000FFFF00FF000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000FF00000000000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000848484000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000FF0000008484
      8400848484000000000000000000FF000000FF0000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000C6C6C600C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C6000000000000000000C6C6C600C6C6C6000000000000000000000000000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000FF000000FF0000008484840084848400FF00
      0000FF000000000000000000000000000000000000000000000000000000C6C6
      C6000000000000000000C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      0000C6C6C600C6C6C6000000000000000000C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000084848400FF000000FF000000FF000000FF00
      0000848484000000000000000000000000000000000000000000C6C6C6000000
      0000C6C6C600C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C60000000000C6C6
      C6000000000000000000C6C6C600C6C6C6000000000000000000C6C6C600C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400FF000000FF000000FF0000008484
      84000000000000000000000000000000000000000000C6C6C60000000000C6C6
      C6000000000000000000C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      0000C6C6C600C6C6C6000000000000000000C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000C6C6C6000000
      0000C6C6C600C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C60000000000C6C6
      C6008484840084848400C6C6C600C6C6C6000000000000000000C6C6C600C6C6
      C600000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000008484840000FFFF0000FFFF000000
      00008484840000000000848484000000000000000000C6C6C60000000000C6C6
      C6000000000000000000C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      840000000000000000008484840084848400C6C6C600C6C6C600000000000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400848484000000000000000000C6C6C600C6C6C60000FFFF0000FF
      FF00848484008484840000000000000000000000000000000000C6C6C6000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600848484000000
      000000FFFF0000FFFF0000000000000000008484840084848400C6C6C600C6C6
      C600000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400848484000000000084848400C6C6C60000FFFF0000FF
      FF008484840000000000000000000000000000000000C6C6C6000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000C6C6C600000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008484840000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF00C6C6
      C600000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600C6C6C600C6C6C60000FFFF00C6C6
      C6000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      00000000000000000000000000000000000084000000FFFFFF00840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0084848400000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF008484840000000000000000000000000084848400FFFF
      FF0000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00000000008484840000000000000000000000000000000000000000008484
      84000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60084848400000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C6000000000084848400000000000000000000000000000000000000
      0000000000000000000000FFFF00000000000000000000FFFF0000FFFF000000
      0000848484000000000000000000000000000000000000000000848484000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF0084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C600848484000000000084848400000000000000000000000000000000000000
      0000000000000000000000FFFF000000000000FFFF0000FFFF0000FFFF000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF00000000000000FF000000FF000000FF0084848400000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C600848484000000000084848400FFFFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00000000008484840084848400000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000008484
      84000000000000000000000000000000000000000000000000000000FF000000
      FF000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00848484000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840000000000C6C6C60084848400000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000FF000000FF00848484000000
      0000000000000000000000000000000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60084848400000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      84008484840000FFFF00848484000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF0084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C600848484000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF008484
      8400000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848484000000000000000000000000000000000000FF
      FF0000FFFF000000000000FFFF00000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000000FF000000
      FF00000000000000000000000000000000000000000084848400C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60084848400848484008484
      8400848484008484840084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C600FFFFFF0084848400848484008484
      84008484840084848400848484000000000000000000000000000000000000FF
      FF00000000008484840000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF0084848400000000000000000000000000000000000000000084848400C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C6008484840000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000848484008484840000000000848484000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF000000FF000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000848484000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF0000FF000000FF00000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084000000840000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000848484000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FFFF0000840000000000000000000000FF00
      0000840000000000000000000000000000000000000000000000000000008484
      84008400000084000000FF000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008400000084000000FF0000008400000000000000000000000000
      00000000FF008484840084848400000000000000000000000000848484000000
      0000000000000000000084848400000000008484840084848400000000000000
      00000000000000000000000000000000000000000000FF000000000000000000
      0000000000000000000000000000FF0000008400000000000000FF0000008400
      0000FFFFFF00FF00000084000000000000000000000000000000000000008400
      000084000000FF000000FF000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000FF000000FF00000084000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400000000008484840000000000000000000000
      000000000000000000000000000000000000FF00000084000000FF0000008400
      0000840000000000000000000000FF0000008400000000000000000000008484
      8400000000000000000000000000848484000000000000000000840000008400
      0000FF000000FF000000FF000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      000084000000FF000000FF000000FF0000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000848484000000000000000000FF0000008400000000000000000000000000
      0000848484000000000084848400000000000000000084000000848484008484
      840084848400FF000000FF000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008484
      84008484840084848400FF000000FF0000008400000000000000848484008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000008484840000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000848484000000000000000000FF0000008400000000000000000000000000
      000084848400000000008484840000000000000000008400000084000000FF00
      0000FFFFFF00FF000000FF000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000FF000000FFFFFF00FF000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000FF0000008400000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000008400
      00008400000000000000FF000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000FF0000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000008400000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400840000008400000000000000FF00000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000848484008484840000000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000FF0000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008400000084000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848484008400000084000000FF00000000000000000000000000
      0000848484000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000008484840084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400840000008400000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000084848400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000FF0000000000
      00000000000000000000FF000000FF000000FF000000FF000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008400000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000848484000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000084000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000084000000000000008400000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF008400000084000000840000008400
      00008400000084000000FFFFFF00840000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000084000000000000008400000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0084000000000000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000000000008400000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00840000008400000084000000FFFF
      FF008400000084000000840000008400000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000000000008400000084000000840000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000000000000000
      000000000000FFFFFF0084000000000000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000000000000000000084000000000000008400000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008400000084000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000FFFFFF008400
      0000840000008400000084000000000000000000000000848400848484000084
      8400848484000084840084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008400
      0000FFFFFF008400000000000000000000000000000084848400008484008484
      8400008484008484840000848400848484000084840084848400008484008484
      84000084840000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008400
      0000840000000000000000000000000000000000000000848400848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000084000000840000008400000084000000840000008400
      0000000000000000000000000000000000000000000084848400848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      84000000000000FFFF00000000000000000000FFFF0000000000848484000084
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF0000000000C6C6C600000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000084848400840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000840000008400000084848400000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000C6C6C600FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000840000008400000084848400000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000084848400840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000084848400848484008484840000FFFF0000FFFF0084848400848484008484
      84008484840000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000008484
      8400000000000000000000000000848484000000000000FFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000848484000000000000FFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000008484
      840084848400848484008484840084848400000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400848484008484840084848400000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF00000000000000000000000000848484000000
      0000FFFFFF0000000000FFFFFF00000000008484840000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000000000FFFFFF0000000000FFFFFF008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF00848484008484840000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000084848400848484000000
      0000000000000000000000000000000000008484840084848400FFFFFF000000
      0000FFFFFF000000FF00FFFFFF0000000000FFFFFF0084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF0084848400000000000000
      000000000000000000000000000000000000000000008484840000000000FFFF
      FF00000000000000FF0000000000FFFFFF000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000084848400000000000000
      FF000000FF000000FF000000FF000000FF000000000084848400000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF0084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000FF
      FF00000000000000000000000000000000000000000084848400FFFFFF000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF0084848400000000000000
      000000000000000000000000000000000000000000008484840000000000FFFF
      FF00000000000000FF0000000000FFFFFF000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF00000000000000000000000000848484008484840000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000084848400848484000000
      0000000000000000000000000000000000008484840084848400FFFFFF000000
      0000FFFFFF000000FF00FFFFFF0000000000FFFFFF0084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000848484000000
      0000FFFFFF0000000000FFFFFF00000000008484840000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000000000FFFFFF0000000000FFFFFF008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000008484
      8400000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000000000000000
      0000000000000000000000000000000000000000000084000000000000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000000000000840000008400000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      FF00000084000000FF0000000000008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400008484000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400008484000000000000000000000000000000
      00000000000000FF00000000000000FF000000FFFF0000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF00000000000000FF000000FFFF0000FF0000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400000000000000000000000000000000000000
      000000FF000000FF00000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF00000000000000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      000000000000FFFFFF00000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000FF000000FF00000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF00000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000848484000000
      0000FFFFFF000000000084848400000000000000000000000000000084000000
      84000000840000008400000084000000000000000000000000000000000000FF
      000000FF000000FF0000000000000000000000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF000000FF0000000000000000000000FFFF0000FFFF00000000000000
      00000000FF000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000848400008484000084
      8400000000000000000000000000000000000000000000FF00000000000000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000FF00000000000000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF0000FF00000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000084840000848400008484000000
      0000000000000000000000000000000000000000000000FF00000000000000FF
      000000FF000000FF00000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000FF00000000000000FF
      000000FF000000FF00000000000000FFFF000000000000FFFF0000FF00000000
      00000000FF000000FF000000FF000000FF000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000008484000084840000848400000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF0000FF00000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000848400008484000084840000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF00000000000000
      00000000FF000000000000000000000000000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000084840000848400008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FFFF0000FFFF00000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400000000000000000000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000000000FF000000FF000000FFFF00000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000FF000000FF000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FF000000FF000000FF000000FF0000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000848484000000000000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF000000000000FF000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000C00000000100010000000000000600000000000000000000
      000000000000000000000000FFFFFF00FFFFC001FFFF0000F80FC001C00F0000
      8000C001E00F00008000C003C0070000C001F00FE01F0000C001F81FF81F0000
      C001FC1FFC3F0000C001FC3FFC3F0000C631FC3FFC3F0000CE39F007F01F0000
      CF79F00FF81F0000CF79F81FF01F00008F78F00FF01F00008F78F00FF01F0000
      CE39F00FF81F0000FFFFF81FF83F0000FFFFFFFFFFFFFFFFC001FFFFFFFF0007
      8031FFF93FFF00078031E7FF180000078031C3F3C00000078001C3E7E0000007
      8001E1C7E00000078001F08FC00000008FF1F81FC00000008FF1FC3FC0000000
      8FF1F81FC00000008FF1F09FE00000008FF1C1C7F00000078FF583E3F8010007
      80018FF1F8030007FFFFFFFFFFFFFFFFFFFFE3FF9FFFFFFFFFFFE3FFEFFF8001
      FEFFFFFFDC010000FE7FE3FF8FFF0000FE3FE3C7FFFF0000801FF3C78FFF0000
      800FF8FFDFFF000080001847EC01000080001C479FFF000080001C67FFFF0000
      FE0098718FFF0000FE00C0B0DFFF0000FE00FE389C010000FE00FE38DFFF0000
      FE00FF30FFFF8001FFFFFF81FFFFFC3FFFFFFC1FFF3FFFFFFFFFF007C13FFFFF
      6111E003801FFBFFFFFFC001800FF3FF6111C001800FE3FFFFFFC0018007C00F
      6111C0018027800FFFFFC001C06300006111E003C0738000FFFFF1C7C0F1C000
      6111F1C7E0F9E200FFFFF1C7E1F8F2000000F007F1FCFA00FFFFF80FF3FFFE00
      E111FC1FF3FFFE00FFFFFFFFF3FFFFFFF8F3FFFFF3FFFFFFF867F3FFE0FF1F1F
      0667E000C03F11118E07C0FE800F3F3FDE0780820007FFFFFE0F00FE800FFFFF
      FF1C808200071F1FF31100FE800F1111F303809000073F3FF90700F1800FFFFF
      FC0F80F3800FFFFFFC0F000700071F1FFE0F800F800F1111FFC7E01FE01F3F3F
      FFC7F83FF83FFFFFFFE7FE7FFE7FFFFFFFFFFFFFFFFFFFFFFFFBFFFFC000E000
      FFF1F9FF8000C000FFE3F0FF8000C000FC83E0FF80008000FC07C07F80008000
      F80FC43F80000000F00FCE3F80000000E01FFE1F80000000C03FFF1F80008000
      C03FFF8F80008000C07FFF8F80018001C0FFFFC7C07FC07FC4FFFFE7E0FFE0FF
      FFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FF3F8002FEFFFF67FE3FF01F
      FCFFFE4FFE3FF81FF8FFFC7FFA3FFC3FF0FFF87FE23FFC21E0FFF071C03F8640
      80FFC04FC07F026E80FFC07FF87FB67580FFC040F83FB67580FFC07FF03FEE7B
      84FFC27FF03FEE7FE2FFF14FF07FDE63F0FFF873FC7FDC03F8FFFC7DFC7FC01F
      FCFFFE5FFC7FC3FFFEFFFF4FFEFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFC00FFFF
      ED9FFC018000001FED6FFC010000000FED6FFC0100000007F16F000100000003
      FD1F000100010001FC7F000100030000FEFF00010003001FFC7F00030003001F
      FD7F00070003001FF93F000F0FC38FF1FBBF00FF0003FFF9FBBF01FF8007FF75
      FBBF03FFF87FFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00FE3F
      FFFFFFFFFC00FE3FFFFFFFFFFC00FFFFFEFFF83FFC00FE3FFC7FF83FEC00FE3F
      F83FF83FE400FF3FF01FF83FE000FF8FE00FC0070000F987C007E00F0001F1C7
      F83FF01F0003F1C7F83FF83F0007F987F83FFC7F000FFC0FF83FFEFFE3FFFFFF
      FFFFFFFFE7FFFFFFFFFFFFFFEFFFFFFFFFFFFF7EFFFDFFFDFFFF9001FFF8FFF8
      FFFFC003FFF1FFF1C3FFE003FFE3FFE399FFE003FFC7FFC799FFE003E08FE08F
      99FFE003C01FC01F99FF0001953F8A3F99FF80002A9F111F99FFE007151F2A9F
      99FFE00F209F001F99FFE00F151F2A9FC3FFE0272A9F111FFFFFC073953F8A3F
      FFFF9E79C07FC07FFFFF7EFEE0FFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFE7FF81FFC3FFFFFFE7FF9FFF99FFDFFFE7FF9FFFF9FF
      DF07E7FFCFFFF9FFBF87E7FFE7FFF9FFBFC7E7FFF3FFE3FFBFA7E7FFF9FFF9FF
      DE77E7FF99FFF9FFE1FF87FF99FF99FFFFFFE7FFC3FFC3FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF862FFFFFFFFFFFF80E0FC7FFC7BFFFF
      01E0F83FF837FFFF01E0F03FF03EFFFF31E1E01FE01DFFFF31C1E01FE01BFFF7
      C181801F8017C1F7C307001F001FC3FBFE17001F0010C7FBCC37001F001FCBFB
      A877801F8017DCF740F7E01FE01BFF0F01E3E01FE01DFFFFC1E3F03FF03EFFFF
      C0E3F83FF837FFFFC83FFC7FFC7BFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenTrackingPointsDialog: TOpenDialog
    DefaultExt = 'points'
    Filter = 'Tracking Points|*.pts;*.points|All Files|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Load Tracking Points'
    Left = 336
    Top = 8
  end
  object SaveTrackingPointsDialog: TSaveDialog
    DefaultExt = 'points'
    Filter = 'Tracking Points|*.pts|All Files|*.*'
    Title = 'Save Tracking Points'
    Left = 360
    Top = 8
  end
  object DownTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = DownTimerTimer
    Left = 164
    Top = 65528
  end
  object ColorPickerStateTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ColorPickerStateTimerTimer
    Left = 471
    Top = 13
  end
end
