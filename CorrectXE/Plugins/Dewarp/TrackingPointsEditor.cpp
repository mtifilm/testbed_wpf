//----------------------------------------------------------------------------
#include "TrackingPointsEditor.h"

#include <math.h>
#include "ImageFormat3.h"
#include "ToolSystemInterface.h"
//----------------------------------------------------------------------------

#define INVALID_FRAME_INDEX (-1)
//----------------------------------------------------------------------------

// Clicking with the mouse:
//
// if mouse is TRACKING
//    if pt is not captured do
//       create new selected pt
//    elseif pt is captured
//       if shift key up
//          unselect all pts
//          select captured pt
//          if ctrl key dn
//             collapse slice to captured pt
//       else
//          select captured pt
//    goto drag selected pts
//----------------------------------------------------------------------------

CTrackingPointsEditor::CTrackingPointsEditor()
{
   // initialize the modkeys
   modKeys = 0;

   // no tracking array yet
   trackingArray = NULL;

   // init min and max
   startFrameIndex = INVALID_FRAME_INDEX;
   stopFrameIndex = INVALID_FRAME_INDEX;
   m_MouseDown = false;

   // We don't actually need these anymore since I moved the boxes into the
   // TrackArray
   trackBoxExtentX = -1;
   trackBoxExtentY = -1;
   searchBoxExtentLeft = -1;
   searchBoxExtentRight = -1;
   searchBoxExtentUp = -1;
   searchBoxExtentDown = -1;

   // init panning flag - we don't bother refreshing the screen during panning
   panEnabled = false;

   //  Init the properties
   SPROPERTY_INIT(lastSelectedTag, _lastSelectedTag, setLastSelectedTag);
   SPROPERTY_INIT(displayTrackingPoints, _displayTrackingPoints, setDisplayTrackingPoints);
   //wtf SPROPERTY_INIT(maxPoints, _maxPoints, setMaxPoints);

   _lastSelectedTag = -2;
   _displayTrackingPoints = true;
   //wtf _maxPoints = -1;   // no limit

   // editor state vble
   majorState = MAJORSTATE_TRACK;

   // Mouse position within frame
   mouseXFrame = -1;
   mouseYFrame = -1;

   systemAPI = NULL;

   ignoringMarks = false;
}
//----------------------------------------------------------------------------

CTrackingPointsEditor::~CTrackingPointsEditor()
{
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::loadSystemAPI(CToolSystemInterface *newSystemAPI)
{
   systemAPI = newSystemAPI;

   // we can init these two variables now
   startFrameIndex = INVALID_FRAME_INDEX;
   stopFrameIndex = INVALID_FRAME_INDEX;

   // Frickin' hack because we try to draw the points initially
   // BEFORE SETTING THE SYSTEM API POINTER, which needs this pointer
   if (trackingArray != NULL)
      drawTrackingPointsCurrentFrame();
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::setTrackingArray(CTrackingArray *newArray,
                                             bool preserveTrackingData)
{
   trackingArray = newArray;

   if (preserveTrackingData)
   {
      if (trackingArray->getColor() == CTrackingArray::ETPBlue)
         trackingArray->setColor(CTrackingArray::ETPGreen);
   }
   else
   {
      trackingArray->setColor(CTrackingArray::ETPRed);
      if (!ignoringMarks)
         setRangeFromMarks();
      TrackingParameterChange.Notify();
   }

   lastSelectedTag = INVALID_TRACKING_POINT_TAG;
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::resetTrackingArray()
{
   startFrameIndex = INVALID_FRAME_INDEX;
   stopFrameIndex = INVALID_FRAME_INDEX;

   deleteAllTrackingPoints();
}

CTrackingArray *CTrackingPointsEditor::getTrackingArray()
{
   return trackingArray;
}
//----------------------------------------------------------------------------
//
// Tool calls this to let us know that the tracking array is completely
// full of valid data
//
void CTrackingPointsEditor::notifyTrackingComplete()
{
   // Take a snapshot of the fully tracked array
   if (trackingArray != NULL)
   {
      trackingArray->setColor(CTrackingArray::ETPGreen);

      TrackingPointStateChange.Notify();
   }
}

//----------------------------------------------------------------------------
//
// Tool calls this to let us know that the tracking array was used for a
// complete render
//
void CTrackingPointsEditor::notifyRenderComplete()
{
   // Take a snapshot of the array used for complete rendering
   if (trackingArray != NULL)
   {
      trackingArray->setColor(CTrackingArray::ETPBlue);
   }
}
//----------------------------------------------------------------------------
//
// See if a particular tracked point in TrackingArray has changed since the
// last Track operation; compare the array slice representing the point
// through time with a snapshot taken after the last Track operation.
// I hate this.
//
bool CTrackingPointsEditor::doesTrackingPointNeedToBeRetracked(int nTag)
{
   // SINCE WE DO NOT HAVE THE ABILITY TO TRACK POINTS INDEPENDENTLY,
   // ALL POINTS NEED TO BE RETRACKED IF ANY ONE OF THE POINTS IS INVALID!!!
   //return hasTrackingArrayChangedSinceLastTracked();
   return !isTrackingDataValidOverEntireRange();
}
//----------------------------------------------------------------------------
//
// Just use the color to determine validity (RED = invalid)
//
bool CTrackingPointsEditor::isTrackingDataValidOverEntireRange()
{
   if (trackingArray == NULL)
      return false;

   bool boundsAreOK = startFrameIndex >= 0 &&
                      stopFrameIndex > startFrameIndex &&
                      startFrameIndex >= trackingArray->beginIndex() &&
                      stopFrameIndex <= trackingArray->endIndex();

   bool pointCountIsOK = (trackingArray->numberTrackPointsPerFrame() >= 1);

   bool trackedOK = (trackingArray->getColor() != CTrackingArray::ETPRed);

   return (boundsAreOK && pointCountIsOK && trackedOK);
}
//----------------------------------------------------------------------------

bool CTrackingPointsEditor::isFrameTrackingDataValid(int frameIndex)
{
   if (trackingArray == NULL)
      return false;

   bool boundsAreOK = frameIndex >= 0 &&
                      frameIndex >= trackingArray->beginIndex() &&
                      frameIndex < trackingArray->endIndex();

   bool pointCountIsOK = (trackingArray->numberTrackPointsPerFrame() >= 1);

   bool trackedOK = (trackingArray->getColor() != CTrackingArray::ETPRed);

   return (boundsAreOK && pointCountIsOK && trackedOK);
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::invalidateAllTrackingData()
{
   if (trackingArray != NULL)
   {
      trackingArray->setColor(CTrackingArray::ETPRed);

      if (ignoringMarks && startFrameIndex != INVALID_FRAME_INDEX)
      {
         trackingArray->eraseAllBut(startFrameIndex);
      }
      else
      {
         trackingArray->eraseAllBut(systemAPI->getMarkIn());
      }
   }

   TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::deleteAllTrackingPoints()
{
   if (trackingArray == NULL) return;

   trackingArray->clear();
   lastSelectedTag = INVALID_TRACKING_POINT_TAG;
   trackingArray->setColor(CTrackingArray::ETPRed);
   TrackingParameterChange.Notify();
   TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::deleteTrackingPointsRange(unsigned int beg, unsigned int end)
{
   if (trackingArray == NULL) return;

   trackingArray->eraseRangeSlice(beg, end);
   trackingArray->setColor(CTrackingArray::ETPRed);
   TrackingParameterChange.Notify();
   TrackingPointStateChange.Notify();
}
//----------------------------------------------------------------------------

int CTrackingPointsEditor::canInMarkChange()
{
   // See if we were initialized properly
   if (trackingArray == NULL || systemAPI == NULL)
      return START_FRAME_CAN_CHANGE;

   // See if we actually care about the marks!
   if (ignoringMarks)
      return START_FRAME_CAN_CHANGE;   // No!

   return checkStartFrameChange(systemAPI->getMarkIn());
}
//----------------------------------------------------------------------------

int CTrackingPointsEditor::checkStartFrameChange(int newStartFrameIndex)
{
   // See if we were initialized properly
   if (trackingArray == NULL || systemAPI == NULL)
      return START_FRAME_CAN_CHANGE;

   // See if there aren't any tracking points yet
   if (trackingArray->numberTrackPointsPerFrame() == 0)
      return START_FRAME_CAN_CHANGE;

   // See if the start frame is not really changing
   if (newStartFrameIndex == startFrameIndex)
      return START_FRAME_CAN_CHANGE;

   // See if the start frame was deleted
   if (newStartFrameIndex == INVALID_FRAME_INDEX)
      return START_FRAME_IS_DELETED;

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   // See if we are moving to a frame with valid tracked data
   if (isFrameTrackingDataValid(newStartFrameIndex))
      return START_FRAME_IS_MOVED_INSIDE_INTERVAL;

   // Otherwise there isn't valid tracking data or we moved outside the range
   return START_FRAME_IS_MOVED_OUTSIDE_INTERVAL;

#else

   // See if we moved outside of the previous start-stop range
   if (newStartFrameIndex < startFrameIndex ||
       newStartFrameIndex >= stopFrameIndex)
      return START_FRAME_IS_MOVED_OUTSIDE_INTERVAL;

   // Otherwise we must have moved to within the previous start-stop range
   return START_FRAME_IS_MOVED_INSIDE_INTERVAL;

#endif

}
//----------------------------------------------------------------------------

int CTrackingPointsEditor::canOutMarkChange()
{
   // See if we were initialized properly
   if (trackingArray == NULL || systemAPI == NULL)
      return STOP_FRAME_CAN_CHANGE;

   // See if we actually care about the marks!
   if (ignoringMarks)
      return STOP_FRAME_CAN_CHANGE;   // No!

   return checkStopFrameChange(systemAPI->getMarkOut());
}
//------------------------------------------------------------------------------

int CTrackingPointsEditor::checkStopFrameChange(int newStopFrameIndex)
{
   // See if we were initialized properly yet
   if (trackingArray == NULL || systemAPI == NULL)
      return STOP_FRAME_CAN_CHANGE;

   // If the tracking array is empty, allow all
   if (trackingArray->numberTrackPointsPerFrame() == 0)
      return STOP_FRAME_CAN_CHANGE;

   // If the tracking array is only on the start frame, allow all
   if ((trackingArray->beginIndex() == startFrameIndex) &&
       (trackingArray->size() <= 1))
       return STOP_FRAME_CAN_CHANGE;

   // See if the stop frame is not really changing
   if (newStopFrameIndex == stopFrameIndex)
      return STOP_FRAME_CAN_CHANGE;

   // See if the stop frame was deleted
   if (newStopFrameIndex == INVALID_FRAME_INDEX)
      return STOP_FRAME_IS_DELETED;

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   // See if we are moving to a frame with valid tracked data
   if (isFrameTrackingDataValid(newStopFrameIndex))
      return START_FRAME_IS_MOVED_INSIDE_INTERVAL;

   // Otherwise there isn't valid tracking data or we moved outside the range
   return START_FRAME_IS_MOVED_OUTSIDE_INTERVAL;

#else

   // See if we moved outside of the previous start-stop range
   if (newStopFrameIndex <= startFrameIndex ||
       newStopFrameIndex > stopFrameIndex)
      return STOP_FRAME_IS_MOVED_OUTSIDE_INTERVAL;

   // Otherwise we must have moved to within the previous start-stop range
   return STOP_FRAME_IS_MOVED_INSIDE_INTERVAL;

#endif

}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::setRange(int newStartFrame, int newStopFrame)
{
   if (trackingArray == NULL || systemAPI == NULL)
      return;

   // for better readability
   int &oldStartFrame = startFrameIndex;
   int &oldStopFrame = stopFrameIndex;

   // See if we have changed, if not, just ignore everything
   if ((newStartFrame == oldStartFrame) && (newStopFrame == oldStopFrame))
      return;

   ////////////////////////////////
   // Handle START frame changes //
   ////////////////////////////////

   // CASE 1: Old or new START Frame is invalid (usually mark in was deleted)
   if (newStartFrame == INVALID_FRAME_INDEX ||
       oldStartFrame == INVALID_FRAME_INDEX )
   {
      // Nuke all tracking info
      resetTrackingArray();
   }
   // CASE 2: START frame didn't move
   else if (newStartFrame == oldStartFrame)
   {
      // Do nothing!
   }

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   // CASE 3: new start frame has valid tracking data
   else if (isFrameTrackingDataValid(newStartFrame))
   {
      // Do nothing!
   }

#else

   // CASE 3: START frame moved right, but is still less than STOP frame
   else if (newStartFrame > oldStartFrame && newStartFrame < oldStopFrame)
   {
      // CASE 3a: TrackingData is VALID
      if (isTrackingDataValidOverEntireRange() )
      {
         // Erase frames before the new start frame
         trackingArray->eraseRangeSlice(oldStartFrame, newStartFrame);
      }
      // CASE 3b: TrackingData is INVALID
      else
      {
         // Move track points from old START frame to the new one
         trackingArray->moveTrackingPointsToNewBeginIndex(newStartFrame);
      }
   }

#endif // DECOUPLE_MARKS_FROM_TRACKED_RANGE

   // CASE 4: START frame moved to left of old START or right of old STOP
   else
   {
      // Move track points from old START frame to the new one
      // NOTE: It is up to the caller to clear all the tracking
      // data before calling us if that's the desired behavior
      trackingArray->moveTrackingPointsToNewBeginIndex(newStartFrame);
   }


   ///////////////////////////////
   // Handle STOP frame changes //
   ///////////////////////////////

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   // The only thing we do is remove tracking data if the stop frame is
   // deleted, and I don't know why I bother doing even that!!
   if (newStopFrame == INVALID_FRAME_INDEX)
   {
      // Nuke all tracking info except the positons of the track points
      // at the start frame
      trackingArray->eraseAllBut(newStartFrame);
   }

#else

   // CASE 1: Old or new STOP Frame is invalid
   if (newStopFrame == INVALID_FRAME_INDEX ||
       oldStopFrame == INVALID_FRAME_INDEX )
   {
      // Nuke all tracking info except the positons of the track points
      // at the start frame
      trackingArray->eraseAllBut(newStartFrame);
   }

   // CASE 2: STOP frame didn't move
   else if (newStopFrame == oldStopFrame)
   {
      // Do nothing!
   }

   // CASE 3: STOP frame moved LEFT, but is still right of the START frame
   else if (newStopFrame < oldStopFrame && newStopFrame > oldStartFrame)
   {
      // Erase all data from the new STOP frame and beyond
      trackingArray->eraseRangeSlice(newStopFrame, ALL_TO_END);
   }

   // CASE 4: STOP frame moved to left of old START or right of old STOP
   else
   {
      // Nuke all tracking info except the positons of the track points
      // at the start frame
      trackingArray->eraseAllBut(newStartFrame);
   }

#endif

   // Update the current start/stop
   startFrameIndex = newStartFrame;
   stopFrameIndex = newStopFrame;

   // wtf?? invalidateAllTrackingData();
   // wtf?? findTrackedValue();
   rangeChange.Notify();
   systemAPI->refreshFrameCached();
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::setRangeFromMarks()
{
   setRange(systemAPI->getMarkIn(), systemAPI->getMarkOut());
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::getRange(int &startFrame, int &stopFrame)
{
   startFrame = startFrameIndex;
   stopFrame = stopFrameIndex;
}
//----------------------------------------------------------------------------

void CTrackingPointsEditor::setIgnoringMarksFlag(bool newIgnoreFlag)
{
   ignoringMarks = newIgnoreFlag;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setShift(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_SHIFT;
   else
      modKeys &= ~MOD_KEY_SHIFT;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setAlt(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_ALT;
   else
      modKeys &= ~MOD_KEY_ALT;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setCtrl(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_CTRL;
   else
      modKeys &= ~MOD_KEY_CTRL;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setKeyA(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
      selectAllTrackingPoints();
      refreshTrackingPointsFrame();
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setKeyD(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
      deleteSelectedTrackingPointsAllFrames();
      refreshTrackingPointsFrame();
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setKeyU(bool keydown)
{
   panEnabled = keydown;
}

//------------------------------------------------------------------------------

void CTrackingPointsEditor::setTrackingBoxSizes(
   int newTrackBoxExtentX,     int newTrackBoxExtentY,
   int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
   int newSearchBoxExtentUp,   int newSearchBoxExtentDown
   )
{
   if (trackingArray == NULL)
      return;

   if (trackingArray->areExtentsDifferent(
                     newTrackBoxExtentX, newTrackBoxExtentY,
                     newSearchBoxExtentLeft, newSearchBoxExtentRight,
                     newSearchBoxExtentUp, newSearchBoxExtentDown)
      )
   {
      trackingArray->setExtents(
                     newTrackBoxExtentX, newTrackBoxExtentY,
                     newSearchBoxExtentLeft, newSearchBoxExtentRight,
                     newSearchBoxExtentUp, newSearchBoxExtentDown);

      refreshTrackingPointsFrame();
      if (trackingArray != NULL)
      {
         trackingArray->setColor(CTrackingArray::ETPRed);
         TrackingParameterChange.Notify();
      }

      TrackingPointStateChange.Notify();
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::mouseDown()
{
   if (trackingArray == NULL)
   {
      return;
   }

   // I think this is just asking in a roundabout way if any clip is selected.
   int icurFrame = systemAPI->getLastFrameIndex();
   if (icurFrame < 0)
   {
      return;
   }

   m_MouseDown = true;

   if (majorState == MAJORSTATE_DRAG)
   {
      // Work around a state bug - should not be able to get a mouse down
      // if we are dragging a box!!
      majorState = MAJORSTATE_TRACK;
   }

   // See if user clicked on a tracking point.
   trackingArray->getExtents(trackBoxExtentX, trackBoxExtentY,
                             searchBoxExtentLeft, searchBoxExtentRight,
                             searchBoxExtentUp, searchBoxExtentDown);
   DRECT searchBox;
   searchBox.left   = mouseXFrame - trackBoxExtentX;
   searchBox.top    = mouseYFrame - trackBoxExtentY;
   searchBox.right  = mouseXFrame + trackBoxExtentX;
   searchBox.bottom = mouseYFrame + trackBoxExtentY;
   int trackingPointIndex = findTrackingPointInBox(searchBox);

   // NOTE: Tools actually set the MOD_KEY_SHIFT when CTRL is pressed!
   const bool leaveOnlyOneSelectedPoint = (modKeys & MOD_KEY_SHIFT) == 0;
   if (leaveOnlyOneSelectedPoint)
   {
      unselectAllTrackingPoints();
   }
   else if (trackingPointIndex == lastSelectedTag)
   {
       // We ctrl-clicked on a selected tag. Just de-select it!
      unselectTrackingPointAllFrames(trackingPointIndex);
      lastSelectedTag = INVALID_TRACKING_POINT_TAG;
      return;
   }

   if (trackingPointIndex == INVALID_TRACKING_POINT_TAG)
   {
      // Create a new selected tracking point.
      trackingPointIndex = createTrackingPoint(mouseXFrame, mouseYFrame, true);

      // Obviously, this must precede the setting of the lastSelectedTag property!
      NextSelectedTagIsANewTrackingPoint.Notify();
      lastSelectedTag = trackingPointIndex;
   }
   else
   {
      // Select an existing tracking point.
      selectTrackingPointAllFrames(trackingPointIndex);
      lastSelectedTag = trackingPointIndex;
   }

   // CAREFUL: This doesn't mean the user pressed control - it's only set
   // programmaticaly for this fucked-up API!
   const bool okToDrag = (modKeys & MOD_KEY_CTRL) == 0;
   if (trackingPointIndex != -1 && okToDrag)
   {
      majorState = MAJORSTATE_DRAG;
   }

   systemAPI->refreshFrameCached();
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::mouseMove(int x, int y)
{
   mouseXClient = x;
   mouseYClient = y;

   int newMouseXFrame  = systemAPI->dscaleXClientToFrame(x);
   int newMouseYFrame  = systemAPI->dscaleYClientToFrame(y);

   deltaMouseXFrame = newMouseXFrame - mouseXFrame;
   deltaMouseYFrame = newMouseYFrame - mouseYFrame;

   // Exit if we are doing nothing
   if (deltaMouseXFrame == 0 && deltaMouseYFrame == 0) return;

   mouseXFrame = newMouseXFrame;
   mouseYFrame = newMouseYFrame;

//TRACE_2(errout << "=== (" << mouseXFrame << "," << mouseYFrame << ")");

   if (!m_MouseDown)
      return;

   switch(majorState) {

      case MAJORSTATE_TRACK:
      break;

      case MAJORSTATE_DRAG:

         if (systemAPI->getLastFrameIndex() != startFrameIndex)
         {
            theError.set("Tracking points can only be moved at the IN frame");
            theError.Notify();
            systemAPI->refreshFrameCached();
            majorState = MAJORSTATE_TRACK;
            m_MouseDown = false;
         }
         else
         {
           translateSelectedTrackingPointsCurrentFrame(deltaMouseXFrame,
                                                       deltaMouseYFrame);

           if (!panEnabled)
              systemAPI->refreshFrameCached();
           if (trackingArray != NULL)
           {
              trackingArray->setColor(CTrackingArray::ETPRed);
              TrackingParameterChange.Notify();
           }

           TrackingPointStateChange.Notify();
         }
      break;
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::mouseUp()
{
   m_MouseDown = false;

   switch(majorState) {

      case MAJORSTATE_TRACK:

      break;

      case MAJORSTATE_DRAG:

         systemAPI->refreshFrameCached();

         majorState = MAJORSTATE_TRACK;

      break;

   }

   // WTF is this doing here??? QQQ
   // God only knows what problems will result from taking this out!!
//   lastSelectedTag.Notify();
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::findTrackedValue(int tag)
{
    if (trackingArray == NULL) return;

    if (!trackingArray->IsTSliceValid(tag, startFrameIndex, stopFrameIndex))
         trackingArray->setTrackedStatus(tag, tpsNOTTRACKED);
    else if (doesTrackingPointNeedToBeRetracked(tag))
        trackingArray->setTrackedStatus(tag, tpsMOVED);
    else
         trackingArray->setTrackedStatus(tag, tpsVALID);
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::findTrackedValue(void)
{
   if (trackingArray == NULL) return;
   if (trackingArray->numberTrackPointsPerFrame() == 0) return;

   CTrackingPoints &frmTrkPts = trackingArray->at(trackingArray->beginIndex());

   for (int i = frmTrkPts.beginIndex(); i < frmTrkPts.endIndex(); i++)
      findTrackedValue(i);
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::drawTrackingPointsCurrentFrame(int defaultColor)
{
   if (trackingArray == NULL)
      return;
   if (trackingArray->numberTrackPointsPerFrame() == 0)
      return;
   if (systemAPI == NULL)
      return;
   if (!_displayTrackingPoints)
      return;

   int curFrame = systemAPI->getLastFrameIndex();
   if (trackingArray->isValid(curFrame) == false)
      return;

   // I hope the presently set color is right!!
   CTrackingArray::ETPColor color = trackingArray->getColor();
   int graphicsColor =
         (color == CTrackingArray::ETPGreen)? SOLID_GREEN :
            ((color == CTrackingArray::ETPBlue)? SOLID_BLUE :
                                                    defaultColor);

   if (!ignoringMarks)
   {
      // Only show tracking points between the marks (or on the mark IN if
      // there is no mark OUT defined)
      int markInFrameIndex = systemAPI->getMarkIn();
      int markOutFrameIndex = systemAPI->getMarkOut();
      bool onlyShowingPointsOnMarkIn = false;

      if (markInFrameIndex == -1)
         return;
      if (markOutFrameIndex == -1 || markOutFrameIndex <= markInFrameIndex)
      {
         if (curFrame != markInFrameIndex)
            return;

         onlyShowingPointsOnMarkIn = true;
      }
      else
      {
         if (curFrame < markInFrameIndex || curFrame >= markOutFrameIndex)
            return;
      }

      if (onlyShowingPointsOnMarkIn ||
          (markInFrameIndex < trackingArray->beginIndex()) ||
          (markOutFrameIndex > trackingArray->endIndex()))
      {
		 graphicsColor = defaultColor;
      }
   }

   // Find the size
   const CImageFormat *imgFmt = systemAPI->getVideoClipImageFormat();
   if (imgFmt == NULL)
      return;

   // Create the arrow sizes normalized by the image size
   int nWidth = imgFmt->getPixelsPerLine()/60;
   int nLen = 1.5*nWidth;

   CTrackingPoints &frmTrkPts = trackingArray->at(curFrame);
   trackingArray->getExtents(trackBoxExtentX, trackBoxExtentY,
                             searchBoxExtentLeft, searchBoxExtentRight,
                             searchBoxExtentUp, searchBoxExtentDown);

   for (int i = frmTrkPts.beginIndex(); i < frmTrkPts.endIndex(); i++)
   {
      if (frmTrkPts.isValid(i))
      {
         CTrackPoint& frmTrkPt = frmTrkPts.at(i);

         // Ideally the tracked values would be set when tracking, but we are
         // to do it here to avoid rewritting too much code
         if (trackingArray->getTrackedStatus(i) == tpsUNKNOWN)
         {
            findTrackedValue(i);
         }

         systemAPI->setGraphicsColor(graphicsColor);

         if (frmTrkPt.selected)
           {

             // There may be multiple selected points, only the last
             // gets a motion box              u
             if (i == lastSelectedTag)
               {
                 systemAPI->drawTrackingBoxNoFlush(frmTrkPt.x, frmTrkPt.y,
                                     trackBoxExtentX, trackBoxExtentY,
                                     searchBoxExtentLeft+trackBoxExtentX,
                                     searchBoxExtentUp+trackBoxExtentY,
                                     //searchBoxExtentRight+trackBoxExtentX,
                                     //searchBoxExtentDown+trackBoxExtentY,
                                     true); // show crosshairs
              }
             else
               {
                 systemAPI->drawTrackingBoxNoFlush(frmTrkPt.x, frmTrkPt.y,
                                     trackBoxExtentX, trackBoxExtentY,
                                     0, 0, //0, 0,  // no motion box
                                     true); // show crosshairs
               }
           }
         else
           {
             // Not selected - just a plain box
             systemAPI->drawTrackingBoxNoFlush(frmTrkPt.x, frmTrkPt.y,
                                     trackBoxExtentX, trackBoxExtentY,
                                     0, 0, //0, 0,   // no motion box
                                     false); // no crosshairs
           }

         // See if we are linked to another box
         tpAnchor anchorType = (trackingArray->getAnchorIndex(i) == INVALID_TRACKING_POINT_TAG)
                               ? EANCHOR_NONE
                               : trackingArray->getAnchor(i);
         systemAPI->setGraphicsColor(SOLID_YELLOW);
         if ((anchorType == EANCHOR_HORZIONAL) ||
             (anchorType == EANCHOR_VERTICAL) ||
             (anchorType == EANCHOR_ALL))
         {
             CTrackPoint& frmBaseTrkPt = frmTrkPts[trackingArray->getAnchorIndex(i)];
             systemAPI->drawArrowFrameNoFlush(frmTrkPt.x, frmTrkPt.y, frmBaseTrkPt.x, frmBaseTrkPt.y, nWidth, nLen);
         }
      }
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::drawLineFromSelectedTrackingPointToMousePosition()
{
   if (trackingArray == NULL)
   {
      return;
   }

   if (trackingArray->numberTrackPointsPerFrame() == 0)
   {
      return;
   }

   if (systemAPI == NULL)
   {
      return;
   }

   int curFrame = systemAPI->getLastFrameIndex();
   if (trackingArray->isValid(curFrame) == false)
   {
      return;
   }

   int tag = lastSelectedTag;
   if (tag == INVALID_TRACKING_POINT_TAG)
   {
      return;
   }

   // Find the size
   const CImageFormat *imgFmt = systemAPI->getVideoClipImageFormat();
   if (imgFmt == NULL)
   {
      return;
   }

   if (mouseXFrame < 0 || mouseXFrame > imgFmt->getPixelsPerLine()
   || mouseYFrame < 0 || mouseYFrame > imgFmt->getLinesPerFrame())
   {
      // Mouse moved out of the frame - don't draw the yellow line
      return;
   }

   // Create the arrow sizes normalized by the image size
   int nWidth = imgFmt->getPixelsPerLine()/60;
   int nLen = 1.5*nWidth;

   // Draw the arrow line
   systemAPI->setGraphicsColor(SOLID_YELLOW);
   CTrackingPoints &frmTrkPts = trackingArray->at(curFrame);
   CTrackPoint& trackPoint = frmTrkPts[lastSelectedTag];
   systemAPI->drawArrowFrameNoFlush(trackPoint.x, trackPoint.y, mouseXFrame, mouseYFrame, nWidth, nLen);
   systemAPI->flushGraphics();  // Haha there is no drawArrowFrame()!
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::refreshTrackingPointsFrame()
{
   if (trackingArray == NULL)
      return;
   if (systemAPI == NULL)
      return;

   //************** SIDE EFFECT ALERT !!! **************************//
   if (trackingArray->numberTrackPointsPerFrame() <= 0)
      lastSelectedTag = INVALID_TRACKING_POINT_TAG;
   //************** SIDE EFFECT ALERT !!! **************************//

   systemAPI->refreshFrameCached();
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::unselectAllTrackingPoints()
{
   if (trackingArray == NULL) return;

   for (int i = trackingArray->beginIndex(); i < trackingArray->endIndex(); ++i)
   {
      CTrackingPoints &trackingPoints = trackingArray->at(i);

      for (int j = trackingPoints.beginIndex(); j < trackingPoints.endIndex(); ++j)
      {
         CTrackPoint& trackPoint = trackingPoints[j];

         trackPoint.selected = false;
      }
   }

   lastSelectedTag = INVALID_TRACKING_POINT_TAG;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::selectAllTrackingPoints()
{
   // This is untested - I got tired of seeing an empty method, so I stole
   // code from unslectAllTrackingPoints() and changed false to true
   if (trackingArray == NULL) return;

   for (int i = trackingArray->beginIndex(); i < trackingArray->endIndex(); ++i)
   {
      CTrackingPoints &trackingPoints = trackingArray->at(i);

      for (int j = trackingPoints.beginIndex(); j < trackingPoints.endIndex(); ++j)
      {
         CTrackPoint& trackPoint = trackingPoints[j];

         trackPoint.selected = true;
      }
   }
}
//------------------------------------------------------------------------------

// Given a rectangular subset of the image as defined by selbox, return
// the index of a random tracking point whose position falls inside the
// the rectangle (well it isn't really random - it returns the lowest-indexed
// point if there are more than one)

int CTrackingPointsEditor::findTrackingPointInBox(DRECT &selbox)
{
   if (trackingArray == NULL || trackingArray->numberTrackPointsPerFrame() == 0)
      return INVALID_FRAME_INDEX;

   int curFrame = systemAPI->getLastFrameIndex();

   if ((curFrame == startFrameIndex) ||
       (curFrame > startFrameIndex && curFrame < stopFrameIndex))
   {
      CTrackingPoints &frmTrkPts = trackingArray->at(curFrame);

      for (int index = frmTrkPts.beginIndex(); index < frmTrkPts.endIndex(); ++index)
      {
         CTrackPoint& frmTrkPt = frmTrkPts[index];

         if ((frmTrkPt.x >= selbox.left) && (frmTrkPt.x <= selbox.right) &&
             (frmTrkPt.y >= selbox.top ) && (frmTrkPt.y <= selbox.bottom))
         {
            return index;
         }
      }
   }

   // No tracking points found in the selection box
   return INVALID_FRAME_INDEX;
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::selectTrackingPointAllFrames(unsigned int selindx)
{
   if (trackingArray == NULL) return;

   for (int i=trackingArray->beginIndex();i<trackingArray->endIndex();i++) {

      if (trackingArray->isValid(i)) {

         CTrackingPoints &frmTrkPts = trackingArray->at(i);

         if (frmTrkPts.isValid(selindx)) {

            CTrackPoint& frmTrkPt = frmTrkPts.at(selindx);

            frmTrkPt.selected = true;
         }
      }
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::unselectTrackingPointAllFrames(unsigned int selindx)
{
   if (trackingArray == NULL) return;

   for (int i=trackingArray->beginIndex();i<trackingArray->endIndex();i++) {

      if (trackingArray->isValid(i)) {

         CTrackingPoints &frmTrkPts = trackingArray->at(i);

         if (frmTrkPts.isValid(selindx)) {

            CTrackPoint& frmTrkPt = frmTrkPts.at(selindx);

            frmTrkPt.selected = false;
         }
      }
   }
}
//------------------------------------------------------------------------------

// Here is the cryptic old comment for this method, which doesn't make sense:
// "Just check to see if we are still selected, if not, deselect last selected"
// What it really seems to be doing is ensuring consistency of selection of
// the tracking points temporally with respect to the selection state at the
// start frame, and making sure that the 'lastSlectedTag' property is not
// set if there aren't any tracking points selected!
// Of course, I can't understand why we allow tracking points to be selected
// at some frame indices and not others! It seems like ther should be a
// single SELECTED attribute per tracking point, NOT per tracking point per
// frame!

void CTrackingPointsEditor::resetLastSelected(void)
{
   if (trackingArray == NULL) return;
   if (trackingArray->numberTrackPointsPerFrame() == 0) return;

   CTrackingPoints &TP = trackingArray->at(trackingArray->beginIndex());

   bool bOneSelected = false;

   for (int i = TP.beginIndex(); i < TP.endIndex(); i++)
      if (TP[i].selected)
        {
           selectTrackingPointAllFrames(i);
           bOneSelected = true;
        }

   systemAPI->refreshFrameCached();
   if (bOneSelected) return;

   lastSelectedTag =  INVALID_TRACKING_POINT_TAG;

   // Is this needed? It's done in the lastSelectedTag property setter if the tag actually changes
   TrackingPointStateChange.Notify();

}
//------------------------------------------------------------------------------

int CTrackingPointsEditor::createTrackingPoint(double x, double y, bool sel)
{
	int NewTag = INVALID_TRACKING_POINT_TAG;

   if (trackingArray == NULL)
      return NewTag;

#if OLD_SHIT
   int currentFrameIndex = systemAPI->getLastFrameIndex();
   int markInFrameIndex = systemAPI->getMarkIn();

   // We only allow marks to be added at the startFrameIndex which
   // may be the Mark In frame, unless the ignoringMarks flag is set;
   // first establish the startFrameIndex if it isn't presently set
   if (startFrameIndex == INVALID_FRAME_INDEX)
     {
       if (ignoringMarks)
         {
           // This establishes the start index at the current frame
			  startFrameIndex = currentFrameIndex;
         }
       else
         {
           // Else the new startFrameIndex is the current Mark In frame
           if (markInFrameIndex >= 0)
             {
               startFrameIndex = markInFrameIndex;
             }
           else
             {
               theError.set("Please set marks before creating tracking points!");
               theError.Notify();
               majorState = MAJORSTATE_TRACK;
               m_MouseDown = false;
               return NewTag;
             }
         }
     }

   // New tracking points can only be added at the startFrameIndex, or maybe
   // on the Paint tool reference frame if it's earlier than the Mark In frame!
   if (currentFrameIndex != startFrameIndex)
     {
       if (ignoringMarks)
         {
			  theError.set("Tracking points can only be added at the mark IN frame  \n"
							  "or at the first reference frame if it is earlier.");
           theError.Notify();
           majorState = MAJORSTATE_TRACK;
           m_MouseDown = false;
			  return NewTag;
         }
       else
         {
           // If the current frame is mark IN frame, move the startFrameIndex
           if (currentFrameIndex == markInFrameIndex)
             {
               startFrameIndex = currentFrameIndex;
             }
           else
             {
               theError.set("Tracking points can only be added at the Mark IN frame");
					theError.Notify();
					majorState = MAJORSTATE_TRACK;
					m_MouseDown = false;
					return NewTag;
             }
         }
	  }
#endif

	// New shit.
	if (!ignoringMarks)
	{
		setRangeFromMarks();
	}

	if (startFrameIndex == INVALID_FRAME_INDEX
	|| stopFrameIndex ==  INVALID_FRAME_INDEX
	|| startFrameIndex >= stopFrameIndex)
	{
		theError.set("Please set valid marks before creating tracking points!");
		theError.Notify();
		majorState = MAJORSTATE_TRACK;
		m_MouseDown = false;
		return NewTag;
	}

	int currentFrame = systemAPI->getLastFrameIndex();
	if (currentFrame != startFrameIndex)
	{
		theError.set("Tracking points can only be added at the first frame in the tracking range");
		theError.Notify();
		majorState = MAJORSTATE_TRACK;
		m_MouseDown = false;
		return NewTag;
	}
   // End of new shit.

   trackingArray->eraseAllBut(startFrameIndex);
   NewTag = trackingArray->at(startFrameIndex).add(x, y, sel);
   trackingArray->setColor(CTrackingArray::ETPRed);
   TrackingParameterChange.Notify();
   return NewTag;
}
//------------------------------------------------------------------------------

// Translate the selected tr points by an amount [delx, dely],
// They may wander outside the clip's active rectangle
//
void CTrackingPointsEditor::translateSelectedTrackingPointsCurrentFrame(
                                                double delx, double dely)
{
   if (trackingArray == NULL)
      return;

   unsigned int curFrame = systemAPI->getLastFrameIndex();
   IntegerList sl = trackingArray->getSelectedTemporalTags(curFrame);
   for (unsigned i=0; i < sl.size(); i++)
     {
       // Note: The selected list is always valid, so we don't have to check
       // at any level.
       CTrackPoint& frmTrkPt = trackingArray->at(curFrame)[sl[i]];
       if (frmTrkPt.selected)
         {
           frmTrkPt.x += delx;
           frmTrkPt.y += dely;
           trackingArray->setTrackedStatus(sl[i], tpsMOVED);
         }
     }
   trackingArray->setColor(CTrackingArray::ETPRed);
   TrackingParameterChange.Notify();
   TrackingPointStateChange.Notify();
}
//------------------------------------------------------------------------------

// Delete all the selected tr points (through all frames)
void CTrackingPointsEditor::deleteSelectedTrackingPointsAllFrames()
{
   if (trackingArray == NULL)
      return;

   IntegerList sl = trackingArray->getSelectedTemporalTags(systemAPI->getLastFrameIndex());
   for (unsigned i=0; i < sl.size(); i++)
      trackingArray->eraseTemporalTag(sl[i]);

   lastSelectedTag = INVALID_TRACKING_POINT_TAG;
   trackingArray->setColor(CTrackingArray::ETPRed);
   TrackingPointStateChange.Notify();
}
//------------------------------------------------------------------------------

// Setter for the 'lastSelectedTag' property - don't call directly
void CTrackingPointsEditor::setLastSelectedTag(int newLastSelectedTag)
{
   if (_lastSelectedTag != newLastSelectedTag)
   {
      _lastSelectedTag = newLastSelectedTag;
      TrackingPointStateChange.Notify();
   }
}
//------------------------------------------------------------------------------

// Setter for the 'displayTrackingPoints' property - don't call directly
void CTrackingPointsEditor::setDisplayTrackingPoints(bool newDisp)
{
   if (newDisp != _displayTrackingPoints)
   {
      _displayTrackingPoints = newDisp;
      refreshTrackingPointsFrame();
   }
}
//------------------------------------------------------------------------------

void CTrackingPointsEditor::setUniqueSelection(int tpIndex)
{
    unselectAllTrackingPoints();
    selectTrackingPointAllFrames(tpIndex);
    lastSelectedTag = tpIndex;
    TrackingPointStateChange.Notify();
    systemAPI->refreshFrameCached();
}
//------------------------------------------------------------------------------
