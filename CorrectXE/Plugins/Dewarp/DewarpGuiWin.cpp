#pragma link "MTIUNIT"
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AnchorSelectUnit.h"
#include "ClipAPI.h"
#include "DewarpGuiWin.h"
#include "err_dewarp.h"
#include "LoadTrackingBoxDialogUnit.h"
#include "JobManager.h"
#include "MathFunctions.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "ShowModalDialog.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "TrackingPointsEditor.h"

#include <fstream>
#include <math.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
//#pragma link "CSPIN"
#pragma link "ExecButtonsFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorPanel"
#pragma link "SpinEditFrameUnit"
#pragma link "ColorPanel"
#pragma link "PresetsUnit"
#pragma resource "*.dfm"
TDewarpForm *DewarpForm;

// Status bar panels
#define STATUS_BAR_STATUS 0
#define STATUS_BAR_MESSAGE 1
#define STATUS_BAR_INFO 2

#define PLUGIN_TAB_INDEX 7

// Range of tracking and search sliders
#define TRACKING_SLIDER_MAX_2K 60
#define TRACKING_SLIDER_MIN_2K 5
#define TRACKING_SLIDER_MAX_4K (TRACKING_SLIDER_MAX_2K * 2)
#define TRACKING_SLIDER_MIN_4K (TRACKING_SLIDER_MIN_2K * 2)
#define SEARCH_SLIDER_MAX_2K   100
#define SEARCH_SLIDER_MIN_2K   5
#define SEARCH_SLIDER_MAX_4K   (SEARCH_SLIDER_MAX_2K * 2)
#define SEARCH_SLIDER_MIN_4K   (SEARCH_SLIDER_MIN_2K * 2)

//---------------------------------------------------------------------------

//------------------CreateDewarpGUI---------------------

  void CreateDewarpToolGUI(void)

// This creates the Dewarp GUI if one does not already exist.
// Note: only one can be created.
//
//****************************************************************************
{
   if (DewarpForm != NULL) return;         // Already created

   DewarpForm = new TDewarpForm(Application);   // Create it

   DewarpForm->formCreate();
   DewarpForm->RestoreProperties();

   // Reparent the controls to the UniTool
   extern const char *PluginName;

   GDewarpTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>( DewarpForm->DewarpControlPanel ),
         NULL);
 }

//-----------------DestroyDewarpGUI------------------

  void DestroyDewarpToolGUI(void)

//  This destroys then entire GUI interface
//
//***************************************************************************
{
   if (DewarpForm == NULL) return;         // Already destroyed

   // Reparent the controls back to us before destroying them
   if (GDewarpTool != NULL)
      GDewarpTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
   DewarpForm->DewarpControlPanel->Parent = DewarpForm;

   DewarpForm->Free();
   DewarpForm = NULL;
}

//-------------------ShowDewarpGUI-------------------

    bool ShowDewarpToolGUI(void)

//  This creates the GUI if not already exists and then shows it
//
//****************************************************************************
{
   CreateDewarpToolGUI();            // Create the gui if necessary

   if (DewarpForm == NULL || GDewarpTool == NULL)
      return false;

   DewarpForm->DewarpControlPanel->Visible = true;
   DewarpForm->formShow();

   return true;
}

//-------------------HideDewarpGUI-------------------

    bool  HideDewarpToolGUI(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
   if (DewarpForm == NULL || GDewarpTool == NULL) return false;

   DewarpForm->DewarpControlPanel->Visible = false;
   DewarpForm->formHide();

   return false;  // controls are NOT visible !!?!
}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
   if (DewarpForm == NULL) return(false);         // Not created
   return DewarpForm->DewarpControlPanel->Visible;
}

void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                            bool processingRenderFlag)

{
   if (DewarpForm == 0) return;
   DewarpForm->UpdateExecutionButtons(toolProcessingControlState,
                                           processingRenderFlag);
}

void GatherGUIParameters(CDewarpParameters &dewarpParameters)
{
   DewarpForm->GatherParameters(dewarpParameters);
}

void CaptureGUIParameters(CPDLElement &pdlToolParams)
{
   DewarpForm->CaptureParameters(pdlToolParams);
}

void SetGUIParameters(CDewarpParameters &dewarpParameters)
{
   if (DewarpForm == 0)
      return;  // form not available

   DewarpForm->SetParameters(dewarpParameters);
}

void ToggleEditAnchorsMode()
{
   if (DewarpForm == 0)
      return;  // form not available

   DewarpForm->ToggleEditAnchorsCheckBox();
}

//---------------------------------------------------------------------------
__fastcall TDewarpForm::TDewarpForm(TComponent* Owner)
        : TMTIForm(Owner)
{
//	this->GoodInSpinEdit->OnChange = xGoodInSpinEditChange;
//	this->GoodOutSpinEdit->OnChange = xGoodInSpinEditChange;
	_TabStopDoNotDeletePanelVisible = false;
}

class BoolString
{
  public:
     bool State;
     String Name;
};

typedef vector<BoolString> CStateMemory;

   void DisableAllControls(TControl *panel, CStateMemory &StateMemory)
{
   TWinControl *twc;
   BoolString bs;

   // If already disabled, do  nothing
   if (!panel->Enabled) return;

   // Disable the component
   bs.Name = panel->Name;
   bs.State = panel->Enabled;
   StateMemory.push_back(bs);
   panel->Enabled = false;

   // Ignore some components
   if (panel->InheritsFrom(__classid(TRadioGroup))) return;

   // Special case: TrackEdits need to have a method called
   TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame *> (panel);
   if (trackEdit != NULL)
   {
      trackEdit->Enable(false);
      return; // Don't descend into the control - it's all set
   }

   // Window panels can have subcontrols
   if (!panel->InheritsFrom(__classid(TWinControl))) return;
   twc = dynamic_cast<TWinControl *>(panel);

   // Descend into the component
   for (int i=0; i < twc->ControlCount; i++)
     {
       DisableAllControls(twc->Controls[i], StateMemory);
     }
}

   void EnableAllControls(TControl *panel, CStateMemory &StateMemory)
{
   // Enable the controls
    for (unsigned j = 0; j < StateMemory.size(); j++)
       if (panel->Name == StateMemory[j].Name)
          {
             panel->Enabled = StateMemory[j].State;
             StateMemory.erase(StateMemory.begin() + j);
             break;
          }
   // Ignore some components
   if (panel->InheritsFrom(__classid(TRadioGroup))) return;

   // Special case: TrackEdits need to have a method called
   TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame *> (panel);
   if (trackEdit != NULL)
   {
      trackEdit->Enable(false);
      return; // Don't descend into the control - it's all set
   }

   if (!panel->InheritsFrom(__classid(TWinControl))) return;
   TWinControl *twc = dynamic_cast<TWinControl *>(panel);

   // Descend into the component
   for (int i=0; i < twc->ControlCount; i++)
     {
        EnableAllControls(twc->Controls[i], StateMemory);
     }
}

//---------------------------------------------------------------------------

void TDewarpForm::UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                                             bool processingRenderFlag)
{
   if (GDewarpTool == NULL) return;

   int previewFrameIndex = GDewarpTool->GetPreviewFrameIndex();
   EDewarpControlState dewarpControlState = GDewarpTool->GetControlState();
   bool bEnablePause = false;

   if (dewarpControlState == DEWARP_STATE_TRACK)
      _bGUIUpdateFlag = true;

   if (dewarpControlState == DEWARP_STATE_RENDER)
      bEnablePause = true;

   switch(toolProcessingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button
         EnablePreview1(true);

         SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PREVIEW);
         PreviewAllButton->Enabled = true;

         SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_RENDER);
         RenderButton->Enabled = true;

         SetResumeTCToDefault();
         GDewarpTool->ResetLastSelected();
         RedrawSmoothedChart();
         UpdateTrackingPointGUIStuff();
         break;

      case TOOL_CONTROL_STATE_RUNNING :
         // Preview1, Preview or Render is now running

         EnablePreview1(false);

         if (previewFrameIndex < 0 ) {
            if (processingRenderFlag) {
               // Rendering
               PreviewAllButton->Enabled = false;
               if (bEnablePause) {
                  // only allow pause during render
                  RenderButton->Enabled = true;
                  SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_PAUSE);
               }
               else
                  RenderButton->Enabled = false;
            }
            else {
               // Previewing
               RenderButton->Enabled = false;
               if (bEnablePause) {
                  // only allow pause during render
                  PreviewAllButton->Enabled = true;
                  SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PAUSE);
               }
               else
                  PreviewAllButton->Enabled = false;
            } // if (processingRenderFlag)
         } // if (previewFrameIndex)
         else {
            // Previewing One Frame
            PreviewAllButton->Enabled = false;
            RenderButton->Enabled = false;
         }
         break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused
         EnablePreview1(false);

         if (processingRenderFlag)
            {
            // Paused while Rendering
            PreviewAllButton->Enabled = false;

            SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_CONTINUE);
            RenderButton->Enabled = true;
            }
         else
            {
            // Paused while Previewing
            RenderButton->Enabled = false;

            SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_CONTINUE);
            PreviewAllButton->Enabled = true;
            }

         // Now go load the current timecode
         SetStartTCButtonClick(NULL);

         break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
         // Previewing 1 frame, disable everything
         EnablePreview1(false);
         PreviewAllButton->Enabled = false;
         RenderButton->Enabled = false;
         break;

      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         // In transition to Pause or Stop, disable everything
         EnablePreview1(false);
         PreviewAllButton->Enabled = false;
         RenderButton->Enabled = false;
         break;

      default:
         MTIassert(false);
         break;
      }

   UpdateExecutionButtonToolbar(toolProcessingControlState,
                                processingRenderFlag);

} // UpdateExecutionButtons()

//------------------SetResumeTCToDefault----------John Mertus----Nov 2002-----
//  This just returns the ResumeTC to the mark in or the start of the file.

void TDewarpForm::SetResumeTCToDefault(void)
{
   CVideoFrameList *frameList;
   frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;
   int Frame = GDewarpTool->getSystemAPI()->getMarkIn();
   if (Frame < 0) Frame = frameList->getInFrameIndex();
   ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(Frame);
   GDewarpTool->ResetResumeFrameIndex(-1);
}

//------------------ClipChanging----------------John Mertus----Nov 2006-----

    void TDewarpForm::ClipChanging(void *Sender)

//  This is called when the clip is about to change.  Note the clip change
// may fail so do nothing that will leave the clip half open
//
//****************************************************************************
{
  SaveTrackingPointIfNecessary();
}

//------------------ClipHasChanged----------------John Mertus----Nov 2002-----

    void TDewarpForm::ClipHasChanged(void *Sender)

//  This is called when the clip is loaded into the main window
//
//****************************************************************************
{
   if (GDewarpTool == NULL) return;

   // Per Larry - double range of tracking box and search sliders for 4K


   int desiredTrackingBoxSize = TrackingBoxSizeTrackEdit->GetValue();
   int desiredSearchSize = MotionTrackEdit->GetValue();
   int sizeMin, sizeMax, motionMin, motionMax;
   TrackingBoxSizeTrackEdit->GetMinAndMax(sizeMin, sizeMax);
   MotionTrackEdit->GetMinAndMax(motionMin, motionMax);

   const CImageFormat *imageFormat = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == NULL || imageFormat->getPixelsPerLine() <= 2048)
   {
      if (sizeMax == TRACKING_SLIDER_MAX_4K)
      {
         desiredTrackingBoxSize /= (TRACKING_SLIDER_MAX_4K
                                           / TRACKING_SLIDER_MAX_2K);
      }
      TrackingBoxSizeTrackEdit->SetMinAndMax(TRACKING_SLIDER_MIN_2K,
                                             TRACKING_SLIDER_MAX_2K);

      if (motionMax == SEARCH_SLIDER_MAX_4K)
      {
         desiredSearchSize /= (SEARCH_SLIDER_MAX_4K
                                         / SEARCH_SLIDER_MAX_2K);
      }

      MotionTrackEdit->SetMinAndMax(SEARCH_SLIDER_MIN_2K,
                                    SEARCH_SLIDER_MAX_2K);
   }
   else // 4K Clip
   {
      if (sizeMax == TRACKING_SLIDER_MAX_2K)
      {
         desiredTrackingBoxSize *= (TRACKING_SLIDER_MAX_4K
                                           / TRACKING_SLIDER_MAX_2K);
      }

      TrackingBoxSizeTrackEdit->SetMinAndMax(TRACKING_SLIDER_MIN_4K,
                                             TRACKING_SLIDER_MAX_4K);

      if (motionMax == SEARCH_SLIDER_MAX_2K)
      {
		 desiredSearchSize *= (SEARCH_SLIDER_MAX_4K
                                         / SEARCH_SLIDER_MAX_2K);
      }

	  MotionTrackEdit->SetMinAndMax(SEARCH_SLIDER_MIN_4K,
                                    SEARCH_SLIDER_MAX_4K);
   }
   TrackingBoxSizeTrackEdit->SetValue(desiredTrackingBoxSize);
   MotionTrackEdit->SetValue(desiredSearchSize);

   // Apparently setting the values doesn't automatically trigger these
   TrackingTrackBarChange(NULL);
   SearchTrackBarChange(NULL);

   // End of range-doubling hack

   GDewarpTool->TrackingArray.clear();
   GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag = -1;
   GDewarpTool->InvalidateSmoothing();

   string sName = GDewarpTool->GetSuggestedTrackingDataSaveFilename();
   OpenTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());
   SaveTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());

   if (AutoLoadCBox->Checked &&
       !GDewarpTool->getSystemAPI()->IsPDLEntryLoading())
    {
      if (DoesFileExist(sName)) LoadTrackingPoints(sName.c_str());
    }

   UpdateStatusBarInfo();
   UpdateTrackingPointButtons();
   SetResumeTCToDefault();

   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}

//------------------MarksHaveChanged-----------------------------Mar 2008-----

    void TDewarpForm::MarksHaveChanged(void *Sender)

//  This is called when the clip is loaded into the main window
//
//****************************************************************************
{
   if (GDewarpTool == NULL) return;

   DewarpForm->UpdateTrackingPointButtons();

   EToolControlState toolControlState = GDewarpTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      UpdateExecutionButtonToolbar(toolControlState, false);
      setTrackPointsVisibility(true);  // per Larry 2010-06
   }
}


//------------------EnablePreview1----------------John Mertus----Nov 2002-----

    void TDewarpForm::EnablePreview1(bool On)

//  This enables/disables the program's ability to use the Preview1 button
// It also enables/disables routines that act like a Preview1 button
//
//****************************************************************************
{
     PreviewFrameButton->Enabled = On;

    // Show we are processing the data
    if (On)
      Screen->Cursor = crDefault;
    else
      Screen->Cursor = crAppStart;
}

EToolProcessingCommand TDewarpForm::GetButtonCommand(TToolButton *button)
{
   // Get the current tool processing command from the button's User Data

   return (EToolProcessingCommand)(button->Tag);
}

void TDewarpForm::SetButtonCommand(TToolButton *button,
                                       EToolProcessingCommand newCmd)
{
   // Set the tool processing command in the button's Tag and set the
   // the matching text label and icon for the button

   // Save the tool processing command in the button's Tag
   button->Tag = newCmd;

   string buttonString;
   System::Uitypes::TImageIndex iconIndex;
   switch (newCmd)
      {
      case TOOL_PROCESSING_CMD_PREVIEW :
         buttonString = "Preview";
         iconIndex = 2;
         break;
      case TOOL_PROCESSING_CMD_RENDER :
         buttonString = "Render";
         iconIndex = 1;
         break;
      case TOOL_PROCESSING_CMD_PAUSE :
         buttonString = "Pause";
         iconIndex = 7;
         break;
      case TOOL_PROCESSING_CMD_CONTINUE :
         buttonString = "Continue";
         iconIndex = 8;
         break;
      default :
         buttonString = "ERROR!";  // Shouldn't ever happen
         iconIndex = 3; // Stop sign
         break;
      }

   button->Caption = buttonString.c_str();
   button->ImageIndex = iconIndex;
}

//---------------- Resume Timecode Functions -------------------------------------------

void __fastcall TDewarpForm::SetStartTCButtonClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   // This gets the current time code
   CVideoFrameList *frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   int currentFrameIndex = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   GDewarpTool->ResetResumeFrameIndex(currentFrameIndex);
   ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(currentFrameIndex);
}

void TDewarpForm::CaptureParameters(CPDLElement &pdlEntryToolParams)
{
   // Get the Flicker parameters in the GUI to include in a PDL Entry
   CDewarpParameters toolParams;

   // Get the settings from the GUI
   GatherParameters(toolParams);

   // Add the settings to the PDL Entry
   toolParams.WritePDLEntry(pdlEntryToolParams);
}
//---------------------------------------------------------------------------

// PRESETS!
void __fastcall TDewarpForm::RenderButtonClick(TObject *Sender)
{
   RenderPreviewProcessing(Sender);
}
//---------------------------------------------------------------------------

// PRESETS!
void TDewarpForm::CurrentPresetHasChanged(void *Sender)
{
	UpdateGuiFromCurrentPreset();
}
// ---------------------------------------------------------------------------

// PRESETS!
void TDewarpForm::UpdateGuiFromCurrentPreset()
{
	auto presets = GDewarpTool->GetPresets();
	auto parameters = presets->GetCurrentPreset();
	SetParameters(parameters);
}
//---------------------------------------------------------------------------

// PRESETS!
void TDewarpForm::UpdatePresetCurrentParameters()
{
	CDewarpParameters parameters;
	GatherParameters(parameters);
	GDewarpTool->GetPresets()->SetCurrent(parameters);
}

//------------------RenderPreviewProcessing------------------------------------
//  Call this when either render/preview button is entered
//  Sender is the button that was pressed
void TDewarpForm::RenderPreviewProcessing(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   TToolButton *button = dynamic_cast<TToolButton*>(Sender);
   if (button == 0)
      return;

   // Consistence check
   int markIn = GDewarpTool->getSystemAPI()->getMarkIn();
   // int markOut = GDewarpTool->getSystemAPI()->getMarkOut();

   // Check the tracking points
   if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() < 3
       || GDewarpTool->TrackingArray.at(markIn).validSize() < 2)  //??? 2???? QQQ
     {
       theError.set(DEWARP_ERROR_DEWARP_TOO_FEW_TRACKPOINTS, "At least three tracking points must be chosen.");
       _MTIErrorDialog(Handle, theError.getFmtError());
       return;
     }

   // Let see what fired us off
   EToolProcessingCommand buttonCommand = GetButtonCommand(button);

   // Did the resume timecode change?
   if (buttonCommand == TOOL_PROCESSING_CMD_CONTINUE)
      GotoTCButtonClick(NULL);

   // Pass the button's tool processing command to Process Handler
   GDewarpTool->bTrackOnly = false;   // Ugly as sin
   GDewarpTool->bNeedToReTrack =
      (!GDewarpTool->IsNecessaryTrackDataAvailable()) ||
      (TrackingBoxSizeTrackEdit->GetValue() != GDewarpTool->ProcessedTrackBoxExtent) ||
      (MotionTrackEdit->GetValue() != GDewarpTool->ProcessedSearchBoxExtent);
   if (GDewarpTool->bNeedToReTrack)
      GDewarpTool->InvalidateSmoothing();

   GDewarpTool->ButtonCommandHandler(buttonCommand);
}

void __fastcall TDewarpForm::StopButtonClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;
   GDewarpTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_STOP);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::GotoTCButtonClick(TObject *Sender)
{
   ResumeTCEdit->VExit(0);
   if (GDewarpTool == NULL) return;
   CVideoFrameList *frameList;
   frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // NO! GDewarpTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   int Frame = frameList->getFrameIndex(ResumeTCEdit->tcP);
   GDewarpTool->getSystemAPI()->goToFrameSynchronous(Frame);
   Frame = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   GDewarpTool->ResetResumeFrameIndex(Frame);
   ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(Frame);

}
//---------------------------------------------------------------------------

void TDewarpForm::SaveTrackingPointIfNecessary(void)
{
    string sAutoName = GDewarpTool->GetSuggestedTrackingDataSaveFilename();
    if ((AutoSaveCBox->Checked) && (GDewarpTool != NULL))
      if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() > 0)
        {
          SaveTrackingPoints(sAutoName.c_str());
        }
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
      else
        {
          if (DoesFileExist(sAutoName))
            // Larry says it's OK to get rid of this confusing dialog
            //if (_MTIConfirmationDialog(Handle, "No tracking points defined\nDo you wish to remove saved tracking points?") == MTI_DLG_OK)
              if (!DeleteFile(sAutoName.c_str()))
                 _MTIErrorDialog(Handle, "Could not remove " + sAutoName);
        }
#endif
}
//---------------------------------------------------------------------------

bool TDewarpForm::SaveTrackingPoints(const String &sName)
{
  if (GDewarpTool != NULL)
     return GDewarpTool->SaveTrackingPoints(StringToStdString(sName));
  return false;
}

bool TDewarpForm::LoadTrackingPoints(const String &sName, bool restoreMarks)
{
  bool retVal = false;
  if (GDewarpTool != NULL)
  {
     retVal = GDewarpTool->LoadTrackingPoints(StringToStdString(sName), restoreMarks);
     if (retVal && !ShowPointCBox->Checked)
     {
        ShowPointCBox->Checked = true;
     }
  }

  return retVal;
}
//---------------------------------------------------------------------------

void TDewarpForm::SetParameters(CDewarpParameters &DewarpParameters)
{
   // Display the flicker parameters
   UFunctionComboBox->ItemIndex = UFunctionAssociations.Index(DewarpParameters.UFunction);
		 MissingDataWhiteRadioButton->Checked = DewarpParameters.MissingDataFill == MISSING_DATA_WHITE;
		 MissingDataBlackRadioButton->Checked = DewarpParameters.MissingDataFill == MISSING_DATA_BLACK;
		 MissingDataPrevFrameRadioButton->Checked = DewarpParameters.MissingDataFill == MISSING_DATA_PREVIOUS;
//   switch (DewarpParameters.MissingDataFill)
//   {
//
//	  case MISSING_DATA_WHITE:
//
//		 MissingDataFillRadioGroup->ItemIndex = MISSING_DATA_WHITE;
//		 break;
//	  case MISSING_DATA_BLACK:
//		 MissingDataFillRadioGroup->ItemIndex = MISSING_DATA_BLACK;
//
//		 break;
//	  case MISSING_DATA_PREVIOUS:
//		 MissingDataFillRadioGroup->ItemIndex = MISSING_DATA_PREVIOUS;
//		 break;
//	  default:
//		 MTIassert(false);
//		 break;


   // Frame begin and end
//   GoodInSpinEdit->Value = DewarpParameters.GoodFramesIn;
//   GoodOutSpinEdit->Value = DewarpParameters.GoodFramesOut;
   AnchorFirstRadioButton->Checked = DewarpParameters.GoodFramesIn > 0  && DewarpParameters.GoodFramesOut == 0;
   AnchorLastRadioButton->Checked  = DewarpParameters.GoodFramesIn == 0 && DewarpParameters.GoodFramesOut > 0;
   AnchorBothRadioButton->Checked  = DewarpParameters.GoodFramesIn > 0  && DewarpParameters.GoodFramesOut > 0;
   AnchorNoneRadioButton->Checked  = DewarpParameters.GoodFramesIn == 0  && DewarpParameters.GoodFramesOut == 0;

   // Smoothing
   int smoothMin, smoothMax;
   SmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

   // HACK 1e-99 is because we set alpha to 1e-100 in case of max smoothing
   // with start and end frames anchor.
   if (DewarpParameters.Alpha2 < 1e-99)
     SmoothTrackEdit->SetValue(smoothMax);
   else if (DewarpParameters.Alpha2 < 0)
     SmoothTrackEdit->SetValue(smoothMin);
   else
     SmoothTrackEdit->SetValue((log(DewarpParameters.Alpha2)/log(10.0)/(-0.75) + 1.5));

   // Size of the tracking block
   TrackingBoxSizeTrackEdit->SetValue(DewarpParameters.TrackBoxExtent);
   MotionTrackEdit->SetValue(DewarpParameters.SearchBoxExtent);

   // Use a mat box or not
   UseMattingCBox->Checked = DewarpParameters.UseMatting;
   // Removed: KeepOriginalValuesCBox->Checked = DewarpParameters.KeepOriginalMatValues;

}

void TDewarpForm::GatherParameters(CDewarpParameters &DewarpParameters)
{
   // Warp Function
   int n;
   if (UFunctionAssociations.ValueByDisplayName(StringToStdString(UFunctionComboBox->Text), n))
	 {
	   DewarpParameters.UFunction = (EUFunctionType)n;
	 }
   else
     {
	  //// MessageDlg("Unknown Warp Function\n" + UFunctionComboBox->Text, mtError,  TMsgDlgButtons() << mbOK, 0);
		   DewarpParameters.UFunction = (EUFunctionType)0;
     }

   if (MissingDataWhiteRadioButton->Checked)
   {
	  DewarpParameters.MissingDataFill = MISSING_DATA_WHITE;
   }
   else if (MissingDataBlackRadioButton->Checked)
   {
	  DewarpParameters.MissingDataFill = MISSING_DATA_BLACK;
   }
   else
   {
	  DewarpParameters.MissingDataFill = MISSING_DATA_PREVIOUS;
   }

   // Frame begin and end
//   DewarpParameters.GoodFramesIn = GoodInSpinEdit->Value;
//   DewarpParameters.GoodFramesOut = GoodOutSpinEdit->Value;
   DewarpParameters.GoodFramesIn = (AnchorFirstRadioButton->Checked || AnchorBothRadioButton->Checked) ? 1 : 0;
   DewarpParameters.GoodFramesOut = (AnchorLastRadioButton->Checked || AnchorBothRadioButton->Checked) ? 1 : 0;

   // Global Smoothing
   int smoothMin, smoothMax;
   SmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

   if (SmoothTrackEdit->GetValue() == smoothMin)
     {
       // No smoothing
       DewarpParameters.Alpha2 = -1;
     }
   else if (SmoothTrackEdit->GetValue() == smoothMax)
     {
       // Straight line either flat or connecting the before and after anchor
       // frame points ("good" frames) if they are both specified.
       // Careful! when restoring smoothing slider value from Alpha2 stored
       // in .ini file, we compare it with 1e-99 to see if it wants to be
       // set at the max value!
       DewarpParameters.Alpha2 = (DewarpParameters.GoodFramesIn > 0 && DewarpParameters.GoodFramesOut > 0) ? 1e-100 : 0;
     }
   else
     {
       // Do the exponent
       DewarpParameters.Alpha2 = pow(10.0,-0.75*(SmoothTrackEdit->GetValue() - 1));
     }

   // Tracking box size
   DewarpParameters.TrackBoxExtent = TrackingBoxSizeTrackEdit->GetValue();

   // Search box size
   DewarpParameters.SearchBoxExtent = MotionTrackEdit->GetValue();

   // Use a mat box or not
   DewarpParameters.UseMatting = UseMattingCBox->Checked;
   DewarpParameters.KeepOriginalMatValues = true; // Removed: KeepOriginalValuesCBox->Checked;
}

void TDewarpForm::ToggleEditAnchorsCheckBox()
{
   if (EditAnchorsCheckBox->Enabled == false)
   {
      return;
   }

   EditAnchorsCheckBox->Checked = !EditAnchorsCheckBox->Checked;
}

//----------------ReadSettings---------------------John Mertus-----Apr 2005---

	bool TDewarpForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
	 CDewarpParameters  DewarpParameters;
	 DewarpParameters.ReadParameters(ini, "Dewarp");

	 // HACK! Per Larry - ALWAYS START IN MissingFill = Previous MODE!
	 DewarpParameters.MissingDataFill = MISSING_DATA_PREVIOUS;

	 ///// Larry says to NOT remember the "UseMatting" setting!!
	 DewarpParameters.UseMatting = false;
	 /////

	 SetParameters(DewarpParameters);

    // Local parameters
    _bAdvancedPanelOn = ini->ReadBool(IniSection, "AdvancedPanel", _bAdvancedPanelOn);
    ShowPointCBox->Checked = ini->ReadBool(IniSection, "ShowTrackPoints", ShowPointCBox->Checked);
    ShowRenderPointCBox->Checked = ini->ReadBool(IniSection, "ShowWhileRender", ShowRenderPointCBox->Checked);
    ClearOnShotChangeCBox->Checked = ini->ReadBool(IniSection, "ClearOnShotChange", ClearOnShotChangeCBox->Checked);
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
    AutoSaveCBox->Checked = ini->ReadBool(IniSection, "AutoSaveTrackPoints", AutoSaveCBox->Checked);
    AutoLoadCBox->Checked = ini->ReadBool(IniSection, "AutoLoadTrackPoints", AutoLoadCBox->Checked);
#else
    AutoSaveCBox->Checked = true;
    AutoLoadCBox->Checked = false;
#endif
    WarnMarkOutMoveCheckBox->Checked = ini->ReadBool(IniSection, "MarkOutWarnings", WarnMarkOutMoveCheckBox->Checked);

    string sName = GDewarpTool->GetSuggestedTrackingDataSaveFilename();
    OpenTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());
    SaveTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());

    // Just to be safe
    if ((AutoLoadCBox->Checked) && (GDewarpTool != NULL) &&
        !GDewarpTool->getSystemAPI()->IsPDLEntryLoading())
      {
        if (DoesFileExist(sName)) LoadTrackingPoints(sName.c_str());
      }

    SetAdvancedPanel();
    return true;
}

//----------------WriteSettings----------------John Mertus-----Apr 2005---

	bool TDewarpForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
    CDewarpParameters  DewarpParameters;
    GatherParameters(DewarpParameters);

	 ///// Larry says to NOT remember the "UseMatting" setting!!
	 DewarpParameters.UseMatting = false;
	 /////

	 DewarpParameters.WriteParameters(ini, "Dewarp");

    // Local parameters
    ini->WriteBool(IniSection, "AdvancedPanel", _bAdvancedPanelOn);
    ini->WriteBool(IniSection, "ShowTrackPoints", ShowPointCBox->Checked);
    ini->WriteBool(IniSection, "ShowWhileRender", ShowRenderPointCBox->Checked);
    ini->WriteBool(IniSection, "ClearOnShotChange", ClearOnShotChangeCBox->Checked);
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
    ini->WriteBool(IniSection, "AutoSaveTrackPoints", AutoSaveCBox->Checked);
    ini->WriteBool(IniSection, "AutoLoadTrackPoints", AutoLoadCBox->Checked);
#else
    ini->WriteBool(IniSection, "AutoSaveTrackPoints", true);
    ini->WriteBool(IniSection, "AutoLoadTrackPoints", false);
#endif
    ini->WriteBool(IniSection, "MarkOutWarnings", WarnMarkOutMoveCheckBox->Checked);

    // Save the tracking points
    string sAutoName = GDewarpTool->GetSuggestedTrackingDataSaveFilename();

    //  See if we want to autosave them
    if ((AutoSaveCBox->Checked) && (GDewarpTool != NULL))
      if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() > 0)
        {
          SaveTrackingPoints(sAutoName.c_str());
        }
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
      else
        {
          if (DoesFileExist(sAutoName))
            // Larry says it's OK to get rid of this confusing dialog
//            if (_MTIConfirmationDialog(Handle, "No tracking points defined\nDo you wish to remove saved tracking points?") == MTI_DLG_OK)
              if (!DeleteFile(sAutoName.c_str()))
                 _MTIErrorDialog(Handle, "Could not remove " + sAutoName);
        }
#endif

    return true;
}

void __fastcall TDewarpForm::SmoothTrackBarChange(TObject *Sender)
{
	GDewarpTool->InvalidateSmoothing();
	RedrawSmoothedChart();
	UpdatePresetCurrentParameters();
}

void TDewarpForm::RedrawSmoothedChart(void)
{
   CDewarpParameters DewarpParameters;
   GatherParameters(DewarpParameters);
   GDewarpTool->SmoothTrackingPoints(DewarpParameters.GoodFramesIn, DewarpParameters.GoodFramesOut, DewarpParameters.Alpha2);
   DrawChartsTag(LastSelectedTag());
}

//---------------------------------------------------------------------------



void __fastcall TDewarpForm::IndividualSmoothCBoxClick(TObject *Sender)
{
// Change for the entire selection list.
   unsigned int curFrame = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   IntegerList sl = GDewarpTool->TrackingArray.getSelectedTemporalTags(curFrame);
   for (unsigned int n=0; n < sl.size(); n++)
     UpdateIndividualSmoothingTagFromGUI(sl[n]);

   UpdateIndividualSmoothingDisplay();
	GDewarpTool->InvalidateSmoothing();
   RedrawSmoothedChart();
}

//---------------------------------------------------------------------------

void __fastcall TDewarpForm::DeletePointButtonClick(TObject *Sender)
{
   GDewarpTool->getSystemAPI()->setMainWindowFocus();
   GDewarpTool->DeleteSelectedTrackingPoints();
   GDewarpTool->getSystemAPI()->getBaseToolForm()->SetFocus();
}

//---------------------------------------------------------------------------

void __fastcall TDewarpForm::TrackingTrackBarChange(TObject *Sender)
{
   GDewarpTool->setTrackingBoxSizes(TrackingBoxSizeTrackEdit->GetValue(),
                                    TrackingBoxSizeTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue());
   GDewarpTool->bNeedToReTrack = true;  // this is hideous
	GDewarpTool->InvalidateSmoothing();
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::SearchTrackBarChange(TObject *Sender)
{
   GDewarpTool->GetTrackingPointsEditor()->setTrackingBoxSizes(
                                    TrackingBoxSizeTrackEdit->GetValue(),
                                    TrackingBoxSizeTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue(),
                                    MotionTrackEdit->GetValue());
   GDewarpTool->bNeedToReTrack = true;  // this is hideous
   GDewarpTool->InvalidateSmoothing();
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdatePearson(int newTag)
{
   if (GDewarpTool == NULL) return;       // Just ot be safe
   if (_OldSelectedTag == newTag) return; // Nothing to do
   if (!GDewarpTool->TrackingArray.isTemporalTagValid(newTag)) return;  // Ignore invalid tags
   if (!GDewarpTool->TrackingArray.isTemporalTagValid(_OldSelectedTag))
     {
        _OldSelectedTag = newTag;
        return;
     }

   CTrackingPoints tpN = GDewarpTool->TrackingArray.TSlice(newTag);
   CTrackingPoints tpO = GDewarpTool->TrackingArray.TSlice(_OldSelectedTag);

   // Create arrays
   int N = tpN.validSize();
   double *x1 = new double [N];
   double *y1 = new double [N];
   double *x2 = new double [N];
   double *y2 = new double [N];
   double mx1=0;
   double my1=0;
   double mx2=0;
   double my2=0;

   int k=0;
   for (int i = tpN.beginIndex(); i < tpN.endIndex(); i++)
     if (tpN.isValid(i))
       {
          x1[k] = tpN[i].x;
          mx1 += x1[k];
          y1[k] = tpN[i].y;
          my1 += y1[k];
          x2[k] = tpO[i].x;
          mx2 += x2[k];
          y2[k] = tpO[i].y;
          my2 += y2[k];
          k++;
       }

   // Note k should equal N
   mx1 /= N;
   my1 /= N;
   mx2 /= N;
   my2 /= N;

   double rx, ry, p1, zx, zy;

   rx=0;
   for (int i=0; i < N; i++)
     rx += (x2[i] - x1[i] - mx2 + mx1 )*(x2[i] - x1[i] - mx2 + mx1);
   rx = sqrt(rx/N);

   ry=0;
   for (int i=0; i < N; i++)
     ry += (y2[i] - y1[i] - my2 + my1 )*(y2[i] - y1[i] - my2 + my1);
   ry = sqrt(ry/N);

   MTIostringstream os;
   os << "[" << newTag << ", " << _OldSelectedTag << "]"  << " tracks horiziontal match = " << std::setprecision(3) << rx;
   HorziontalMatchLabel->Caption = os.str().c_str();

   os.str("");
   os << "[" << newTag << ", " << _OldSelectedTag << "]"  << " tracks vertical match = " << std::setprecision(3) << ry;
   VerticalMatchLabel->Caption = os.str().c_str();

   // Done, go home
   delete [] x1;
   delete [] y1;
   delete [] x2;
   delete [] y2;

   _OldSelectedTag = newTag;
}

//---------------------------------------------------------------------------

   void TDewarpForm::UpdateIndividualSmoothingGUIFromTag(int nTag)
{
   CTrackingArray &TA = GDewarpTool->TrackingArray;
   IndividualSmoothCBox->Enabled = TA.isTemporalTagValid(nTag);

   if (!IndividualSmoothCBox->Enabled)
     {
       UpdateIndividualSmoothingDisplay();
       return;
     }

   // Valid tag
   int nSmooth = TA.getSmoothing(nTag);

   // Inhibit callbacks
   TNotifyEvent oldChange = IndividualSmoothTrackEdit->NotifyWidget->OnClick;
   TNotifyEvent oldClick = IndividualSmoothCBox->OnClick;
   IndividualSmoothTrackEdit->NotifyWidget->OnClick = NULL;
   IndividualSmoothCBox->OnClick = NULL;

   int smoothMin, smoothMax;
   IndividualSmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

   switch (nSmooth)
     {
       case SMOOTHING_FLAT:      // Keep the first value
          IndividualSmoothTrackEdit->SetValue(smoothMax);
          break;

       case SMOOTHING_NONE:
          IndividualSmoothTrackEdit->SetValue(smoothMin);
          break;

       case SMOOTHING_INVALID:
          break;

       // Now set the alpha value
       default:
          IndividualSmoothTrackEdit->SetValue((log(TA.getSmoothingAlpha(nTag))/log(10.0)/(-0.75) + 1.5) + smoothMin);
          break;

     }

   IndividualSmoothCBox->Checked = TA.getSmoothingEnabled(nTag);
   UpdateIndividualSmoothingDisplay();

   IndividualSmoothTrackEdit->NotifyWidget->OnClick = oldChange;
   IndividualSmoothCBox->OnClick = oldClick;

}

//---------------------------------------------------------------------------
void TDewarpForm::UpdateStatusBarInfo(void)
{
  // Now update the individual smoothing
  CTrackingArray &TA = GDewarpTool->TrackingArray;
  String sTmp = (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() == 0)?
                                       0 : TA.at(TA.beginIndex()).validSize();
  StatusBar->Panels->Items[STATUS_BAR_INFO]->Text = sTmp + " Points";
  ExecStatusBar->SetToolMessage(StringToStdString(sTmp + " Points").c_str());

}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateTrackingPointButtons(void)
{
CTrackingArray &TA = GDewarpTool->TrackingArray;
   int validCount =
                (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() == 0)?
                                       0 : TA.at(TA.beginIndex()).validSize();
   if (validCount > 0)
   {
      TrackButton->Enabled = true;
      PrevPointButton->Enabled = ((validCount == 1)? false : true);
      NextPointButton->Enabled = PrevPointButton->Enabled;
      SavePointsButton->Enabled = true;
   }
   else
   {
      TrackButton->Enabled = false;
      PrevPointButton->Enabled = false;
      NextPointButton->Enabled = false;
      SavePointsButton->Enabled = false;
   }

   DeletePointButton->Enabled = (GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag != -1);
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateTrackingCharts(void)
{
    UpdateTrackingPointGUIStuff();
}
//---------------------------------------------------------------------------

void TDewarpForm::HideShowCharts(bool bShowCharts)
{
  HTrackGraph->Visible = true; //bShowCharts;
  VTrackGraph->Visible = true; //bShowCharts;
  if (!bShowCharts)
  {
     HTrackGraph->Series[0]->Clear();
     HTrackGraph->Series[1]->Clear();
     VTrackGraph->Series[0]->Clear();
     VTrackGraph->Series[1]->Clear();
  }
  TrackWarnPanel->Visible = !bShowCharts;

  UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);

}
//---------------------------------------------------------------------------

void TDewarpForm::rangeChanged(void *Sender)
{
	if (GDewarpTool == NULL) return;       // Just ot be safe
	GDewarpTool->InvalidateSmoothing();
	RedrawSmoothedChart();
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void TDewarpForm::anchorChanged(void *Sender)
{
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AnchorRadioButtonClick(TObject *Sender)
{
   GDewarpTool->InvalidateSmoothing();
   RedrawSmoothedChart();
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void TDewarpForm::handleNewTrackingPointNotification(void *Sender)
{
   _nextSelectedTagIsANewTrackingPoint = true;
}
//---------------------------------------------------------------------------

void TDewarpForm::selectedTagChanged(void *Sender)
{
   if (GDewarpTool == NULL) return;       // Just to be safe
   int selectedTag = LastSelectedTag();
   CTrackingArray &TA = GDewarpTool->TrackingArray;
   bool needToUpdateTrackingPointGuiStuff = true;

   // The following call returns EANCHOR_NONE if EditAnchorCheckBox is unchecked!
   tpAnchor anchorType = getSelectedAnchorTypeFromComboBox();

   if (areWeInAnchorSettingMode())
   {
      // Ignore de-selection of the anchor point
      if (selectedTag == INVALID_TRACKING_POINT_TAG)
      {
         return;
      }
      else if (selectedTag != _tagOfTrackingPointGettingAnchored)
      {
         // Haha stupid hack to re-select the anchored point without
         // re-entering this callback handler!
         GDewarpTool->SetMouseUpDeferredTrackPointSelectionHackTag(_tagOfTrackingPointGettingAnchored);
         needToUpdateTrackingPointGuiStuff = false;

         // Try to set the anchor.
         TA.setAnchor(_tagOfTrackingPointGettingAnchored, anchorType);
         if (TA.setAnchorIndex(_tagOfTrackingPointGettingAnchored, selectedTag))
         {
            // Hack to avoid anchoring an anchor!
            _mostRecentlySelectedAnchor = selectedTag;
            _hackTagToPreventBogusReanchoring = _tagOfTrackingPointGettingAnchored;

            // Careful - this resets _tagOfTrackingPointGettingAnchored!
            exitFromAnchorSettingMode();
         }
         else
         {
            // There's an anchoring loop - not allowed!
            _MTIErrorDialog(Handle, "Anchor loop is not allowed!");
            TA.setAnchor(_tagOfTrackingPointGettingAnchored, EANCHOR_NONE);
         }
      }
   }
   else if (EditAnchorsCheckBox->Checked
         && selectedTag != INVALID_TRACKING_POINT_TAG
         && selectedTag != _mostRecentlySelectedAnchor
         && selectedTag != _hackTagToPreventBogusReanchoring)
   {
      switch (anchorType)
      {
         case EANCHOR_HORZIONAL:
         case EANCHOR_VERTICAL:
         case EANCHOR_ALL:
            // Anchor type will really be set after we know which point is the anchor!
            TA.setAnchor(selectedTag, EANCHOR_NONE);
            enterAnchorSettingMode(selectedTag);
            break;

         case EFIXED_HORIZIONAL:
         case EFIXED_VERTICAL:
         case EFIXED_ALL:
         case EANCHOR_NONE:
            TA.setAnchor(selectedTag, anchorType);
            break;

         default:
            // Bwahhh! misspelled a combo box entry!!
            const bool GotValidAnchorValueFromComboBox = false;
            MTIassert(GotValidAnchorValueFromComboBox);
            TA.setAnchor(selectedTag, EANCHOR_NONE);
            break;
      }
   }

   // These are one-shot hacks.
   if (_mostRecentlySelectedAnchor == selectedTag)
   {
      _mostRecentlySelectedAnchor = INVALID_TRACKING_POINT_TAG;
   }

   if (_hackTagToPreventBogusReanchoring == selectedTag)
   {
      _hackTagToPreventBogusReanchoring = INVALID_TRACKING_POINT_TAG;
   }

   // Not used at present
   _nextSelectedTagIsANewTrackingPoint = false;

   if (needToUpdateTrackingPointGuiStuff)
   {
      UpdateTrackingPointGUIStuff();
      GDewarpTool->getSystemAPI()->refreshFrameCached();
   }
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateTrackingPointGUIStuff()
{
   if (GDewarpTool == NULL) return;       // Just to be safe
   int selectedTag = LastSelectedTag();
   CTrackingArray &TA = GDewarpTool->TrackingArray;

   // The following call returns EANCHOR_NONE if EditAnchorCheckBox is unchecked!
   tpAnchor anchorType = getSelectedAnchorTypeFromComboBox();


   // Visual anchored/fixed flags per chart
   tpAnchor selectedTagAnchorType = TA.getAnchor(selectedTag);
   bool hIsAnchored = selectedTag != INVALID_TRACKING_POINT_TAG
                      && (selectedTagAnchorType == EANCHOR_HORZIONAL
                          || selectedTagAnchorType == EANCHOR_ALL);
   bool hIsFixed =  selectedTag != INVALID_TRACKING_POINT_TAG
                      && (selectedTagAnchorType == EFIXED_HORIZIONAL
                          || selectedTagAnchorType == EFIXED_ALL);
   bool vIsAnchored =  selectedTag != INVALID_TRACKING_POINT_TAG
                      && (selectedTagAnchorType == EANCHOR_VERTICAL
                          || selectedTagAnchorType == EANCHOR_ALL);
   bool vIsFixed =  selectedTag != INVALID_TRACKING_POINT_TAG
                      && (selectedTagAnchorType == EFIXED_VERTICAL
                          || selectedTagAnchorType == EFIXED_ALL);

   HorizontalAnchoredFlagPanel->Visible = hIsAnchored;
   VerticalAnchoredFlagPanel->Visible   = vIsAnchored;
   HorizontalFixedFlagPanel->Visible    = hIsFixed;
   VerticalFixedFlagPanel->Visible      = vIsFixed;

   // We want to update even if not valid
   UpdateIndividualSmoothingGUIFromTag(selectedTag);
   UpdateStatusBarInfo();
   UpdateTrackingPointButtons();

   // Now update the individual smoothing
   if (TA.numberTrackPointsPerFrame() < 1)
   {
      TrackWarnPanel->Caption = "Tracking data not available.";
      HideShowCharts(false);
      return;
   }

   if (!GDewarpTool->isAllTrackDataValid())
   {
      TrackWarnPanel->Caption = AnsiString("Tracking data not available. Point")
         + ((TA.numberTrackPointsPerFrame() == 1)? "" : "s") + " not tracked.";
      HideShowCharts(false);
      return;
   }

   if (!TA.isTemporalTagValid(selectedTag))
   {
      TrackWarnPanel->Caption = "Tracking data available. Please select a point.";
      HideShowCharts(false);
      return;
   }

   // Valid tag
   HideShowCharts(true);
   UpdatePearson(selectedTag);

   // If we haven't been smoothed, smooth it
   if (GDewarpTool->SmoothedArray.numberTrackPointsPerFrame() > 0)
   {
      CDewarpParameters DewarpParameters;
      GatherParameters(DewarpParameters);
      GDewarpTool->SmoothTrackingPoints(DewarpParameters.GoodFramesIn, DewarpParameters.GoodFramesOut, DewarpParameters.Alpha2);
   }

   // Display the chart
   DrawChartsTag(selectedTag);

   // Enable it if it has been disabled
   if (selectedTag > 0)
   {
      ShowPointCBox->Checked = true;
   }
}
//---------------------------------------------------------------------------

double TDewarpForm::FindExtentX(void)
{
   if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() < 1) return 0.0;

   double dTMax = 10;

   CTrackingPoints TPS;
   CTrackingPoints SPS;
   int iS = GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).beginIndex();
   int iE = GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).endIndex();

   for (int iTag = iS; iTag < iE; iTag++)
   {
     TPS = GDewarpTool->TrackingArray.TSlice(iTag);
     SPS = GDewarpTool->SmoothedArray.TSlice(iTag);

     double dMin = 100000;
     double dMax = 0;

     for (int i=TPS.beginIndex(); i < TPS.endIndex(); i++)
       if (TPS[i].isValid())
         {
            if (TPS[i].x > dMax) dMax = TPS[i].x;
            if (TPS[i].x < dMin) dMin = TPS[i].x;
         }

     for (int i=SPS.beginIndex(); i < SPS.endIndex(); i++)
       if (SPS[i].isValid())
         {
            if (SPS[i].x > dMax) dMax = SPS[i].x;
            if (SPS[i].x < dMin) dMin = SPS[i].x;
         }

     dMin = ((int)dMin/10)*10;
     dMax = ((int)dMax/10 + 1)*10;

     if (dTMax < (dMax-dMin)) dTMax = (dMax-dMin);
    }
    return dTMax;
}
//---------------------------------------------------------------------------

double TDewarpForm::FindExtentY(void)
{
   if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() < 1) return 0.0;

   double dTMax = 10;

   CTrackingPoints TPS;
   CTrackingPoints SPS;
   int iS = GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).beginIndex();
   int iE = GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).endIndex();

   for (int iTag = iS; iTag < iE; iTag++)
   {
     TPS = GDewarpTool->TrackingArray.TSlice(iTag);
     SPS = GDewarpTool->SmoothedArray.TSlice(iTag);

     double dMin = 100000;
     double dMax = 0;

     for (int i=TPS.beginIndex(); i < TPS.endIndex(); i++)
       if (TPS[i].isValid())
         {
            if (TPS[i].y > dMax) dMax = TPS[i].y;
            if (TPS[i].y < dMin) dMin = TPS[i].y;
         }

     for (int i=SPS.beginIndex(); i < SPS.endIndex(); i++)
       if (SPS[i].isValid())
         {
            if (SPS[i].y > dMax) dMax = SPS[i].y;
            if (SPS[i].y < dMin) dMin = SPS[i].y;
         }

     dMin = ((int)dMin/10)*10;
     dMax = ((int)dMax/10 + 1)*10;

     if (dTMax < (dMax-dMin))
         dTMax = (dMax-dMin);
    }

    return dTMax;
}
//---------------------------------------------------------------------------

void TDewarpForm::DrawChartsTag(int Tag)
{
   if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() < 1) return;

    if ((Tag < GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).beginIndex()) ||
        (Tag > GDewarpTool->TrackingArray.at(GDewarpTool->TrackingArray.beginIndex()).endIndex())) return;

    CTrackingPoints _TrackingPoints = GDewarpTool->TrackingArray.TSlice(Tag);
    CTrackingPoints _SmoothedPoints = GDewarpTool->SmoothedArray.TSlice(Tag);

   // To make the charts have the same scale, we must find the largest movement
   double fxExtent = FindExtentX();
   double fyExtent = FindExtentY();

   // Find the time code
   CVideoFrameList *frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   PTimecode ptcBase = frameList->getInTimecode();
   PTimecode ptc(ptcBase);

   double dMin = 100000;
   double dMax = 0;

   HTrackGraph->Series[0]->Clear();
   for (int i=_TrackingPoints.beginIndex(); i < _TrackingPoints.endIndex(); i++)
     {
        ptc = ptcBase + i;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].x;;
        if (d > 0)
           HTrackGraph->Series[0]->AddXY(i, d, TCS);
        else
           HTrackGraph->Series[0]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   HTrackGraph->Series[1]->Clear();
   for (int i=_SmoothedPoints.beginIndex(); i < _SmoothedPoints.endIndex(); i++)
     {
        double d =  _SmoothedPoints[i].x;;
        if (d > 0)
           HTrackGraph->Series[1]->AddXY(i, d);
        else
           HTrackGraph->Series[1]->AddNullXY(i, d,"");

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   // Stupid way of setting axis to avoid min being greater than max
   int iMin = ((int)dMin/10)*10;
   HTrackGraph->LeftAxis->Minimum = min<long>(iMin,HTrackGraph->LeftAxis->Maximum);
   HTrackGraph->LeftAxis->Maximum = iMin + fxExtent;
   HTrackGraph->LeftAxis->Minimum = iMin;

   // Do the vertical graph
   dMin = 100000;
   dMax = 0;

   VTrackGraph->Series[0]->Clear();
   for (int i=_TrackingPoints.beginIndex(); i < _TrackingPoints.endIndex(); i++)
     {
        ptc = ptcBase + i;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].y;;
        if (d > 0)
           VTrackGraph->Series[0]->AddXY(i, d, TCS);
        else
           VTrackGraph->Series[0]->AddNullXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   VTrackGraph->Series[1]->Clear();
   for (int i=_SmoothedPoints.beginIndex(); i < _SmoothedPoints.endIndex(); i++)
     {
        double d =  _SmoothedPoints[i].y;;
        if (d > 0)
           VTrackGraph->Series[1]->AddXY(i, d);
        else
           VTrackGraph->Series[1]->AddNullXY(i, d, "");

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   // Stupid way of setting axis to avoid min being greater than max
   iMin = ((int)dMin/10)*10;
   VTrackGraph->LeftAxis->Minimum = min<long>(iMin,VTrackGraph->LeftAxis->Maximum);
   VTrackGraph->LeftAxis->Maximum = iMin + fyExtent;
   VTrackGraph->LeftAxis->Minimum = iMin;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::TrackButtonClick(TObject *Sender)
{
   GDewarpTool->bTrackOnly = true;
   GDewarpTool->bNeedToReTrack = true;
   GDewarpTool->InvalidateSmoothing();

   GDewarpTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_PREVIEW);
}
//---------------------------------------------------------------------------

  void TDewarpForm::UpdateIndividualSmoothingTagFromGUI(int nTag)
{
  CTrackingArray &TA = GDewarpTool->TrackingArray;
  if (!TA.isTemporalTagValid(nTag)) return;

  TA.setSmoothingEnabled(nTag, IndividualSmoothCBox->Checked);

  // Only change if we are enabled.
  if (IndividualSmoothCBox->Checked)
    {
      int smoothMin, smoothMax;
      IndividualSmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

      if (IndividualSmoothTrackEdit->GetValue() == smoothMin)
        {
           // No smoothing
           TA.setSmoothing(nTag, SMOOTHING_NONE);
        }
       else if (IndividualSmoothTrackEdit->GetValue() == smoothMax)
        {
           // Constant smoothing
           TA.setSmoothing(nTag, SMOOTHING_FLAT);
        }
       else
         {
           // Do the exponent
           TA.setSmoothing(nTag, SMOOTHING_2AN);
           TA.setSmoothingAlpha(nTag, pow(10.0,-0.75*(IndividualSmoothTrackEdit->GetValue() - 1)));
         }
    }
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateIndividualSmoothingDisplay(void)
{
   // Disable/enable the Individual box
   static CStateMemory StateMemory;
   if (IndividualSmoothCBox->Checked && IndividualSmoothCBox->Enabled)
      EnableAllControls( IndividualSmoothingGroupBox, StateMemory);
   else
      DisableAllControls(IndividualSmoothingGroupBox, StateMemory);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::IndividualSmoothTrackBarChange(
      TObject *Sender)
{
   // Set the selected point
   unsigned int curFrame = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   IntegerList sl = GDewarpTool->TrackingArray.getSelectedTemporalTags(curFrame);

   for (unsigned int n=0; n < sl.size(); n++)
     UpdateIndividualSmoothingTagFromGUI(sl[n]);

   // For consistency
   UpdateIndividualSmoothingDisplay();
   GDewarpTool->InvalidateSmoothing();
   RedrawSmoothedChart();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::TopUpButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    RECT bb = GDewarpTool->getBoundingBox();
    bb.top = max<long>(0,bb.top-m);
    GDewarpTool->setBoundingBox(bb);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::TopDownButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    const CImageFormat* imgFmt = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
    RECT bb = GDewarpTool->getBoundingBox();
    bb.top = min<long>(imgFmt->getLinesPerFrame()-1, bb.top+m);
    GDewarpTool->setBoundingBox(bb);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::BottomDownButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    const CImageFormat* imgFmt = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
    RECT bb = GDewarpTool->getBoundingBox();
    bb.bottom = min<long>(imgFmt->getLinesPerFrame()-1, bb.bottom+m);
    GDewarpTool->setBoundingBox(bb);

}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::BottomUpButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    RECT bb = GDewarpTool->getBoundingBox();
    bb.bottom = max<long>(0,bb.bottom-m);
    GDewarpTool->setBoundingBox(bb);

}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::LeftLeftButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    RECT bb = GDewarpTool->getBoundingBox();
    bb.left = max<long>(0,bb.left-m);
    GDewarpTool->setBoundingBox(bb);

}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::RightLeftButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    RECT bb = GDewarpTool->getBoundingBox();
    bb.right = max<long>(0,bb.right-m);
    GDewarpTool->setBoundingBox(bb);

}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::RightRightButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    const CImageFormat* imgFmt = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
    RECT bb = GDewarpTool->getBoundingBox();
    bb.right = min<long>(imgFmt->getPixelsPerLine()-1, bb.right+m);
    GDewarpTool->setBoundingBox(bb);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::LeftRightButtonClick(TObject *Sender)
{
    // This avoids the button up causing a move
    if (GlobalSender == NULL) return;

    int m = 1;
    if (HIWORD(GetKeyState(VK_SHIFT)) != 0) m = 10*m;

    const CImageFormat* imgFmt = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
    RECT bb = GDewarpTool->getBoundingBox();
    bb.left = min<long>(imgFmt->getPixelsPerLine()-1, bb.left+m);
    GDewarpTool->setBoundingBox(bb);

}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AllMatButtonClick(TObject *Sender)
{
    const CImageFormat* imgFmt = GDewarpTool->getSystemAPI()->getVideoClipImageFormat();
    RECT bb;
    bb.left = 0;
    bb.top = 0;
    bb.right = imgFmt->getPixelsPerLine()-1;
    bb.bottom = imgFmt->getLinesPerFrame()-1;
    GDewarpTool->setBoundingBox(bb);
}
//---------------------------------------------------------------------------


void __fastcall TDewarpForm::UseMattingCBoxClick(TObject *Sender)
{
	// Disable/enable the matting box
   static CStateMemory StateMemory;
   if (UseMattingCBox->Checked)
   {
      EnableAllControls(ProcessingRegionGroupBox, StateMemory);
   }
   else
   {
      DisableAllControls(ProcessingRegionGroupBox, StateMemory);
      ProcessingRegionGroupBox->Enabled = true;
      UseMattingCBox->Enabled = true;
   }

   // Set the parameters
   ShowMattingBoxCBox->Enabled = UseMattingCBox->Checked;
   GDewarpTool->setDisplayRect(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::ShowMattingBoxCBoxClick(TObject *Sender)
{
  GDewarpTool->setDisplayRect(ShowMattingBoxCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::SetMatButtonClick(TObject *Sender)
{
	GDewarpTool->FindBoundingBox();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AdvancedButtonClick(TObject *Sender)
{
   _bAdvancedPanelOn = !_bAdvancedPanelOn;
   SetAdvancedPanel();
}

  void ClearBltBtnGlyph(TBitBtn *button)
{
    RECT aRect;
    aRect.left = 0;
    aRect.top = 0;
    aRect.right = button->Glyph->Width;
    aRect.bottom = button->Glyph->Height;
    button->Glyph->Canvas->Brush->Style = bsClear;
    button->Glyph->Canvas->Brush->Color = clBtnFace;
    button->Glyph->Canvas->FillRect(aRect);
}
//---------------------------------------------------------------------------

void TDewarpForm::SetAdvancedPanel(void)
{
   if (_bAdvancedPanelOn)
     {
       ClientHeight = SavePointsButton->Top + SavePointsButton->Height + StatusBar->Height + 8;
       AdvancedButton->Caption = "Hide";
       ClearBltBtnGlyph(AdvancedButton);
       BitBtnImageList->GetBitmap(43, AdvancedButton->Glyph);
     }
   else
     {
       ClientHeight = BottomBevel->Top + StatusBar->Height;
       AdvancedButton->Caption = "More...";
       ClearBltBtnGlyph(AdvancedButton);
       BitBtnImageList->GetBitmap(42, AdvancedButton->Glyph);
     }
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateToolStatusBar(int iPanel, const string &strMessage)
{
   StatusBar->Panels->Items[iPanel]->Text = strMessage.c_str();
}

void __fastcall TDewarpForm::UpdateTimerTimer(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   string StatusMessage;
   string TimeMessage;

   MTIostringstream timeMsg,statusMsg;
   timeMsg.setf(ios::fixed, ios::floatfield);
   timeMsg.str("");
   statusMsg.str("");

   EDewarpControlState dewarpControlState = GDewarpTool->GetControlState();

   switch (dewarpControlState)
     {
   case DEWARP_STATE_RENDER:
      if (!ShowPointsOldRenderingFlag)
      {
         ShowPointsOldRenderingFlag = true;
         setTrackPointsVisibility(false);
      }
      if (_bGUIUpdateFlag) {
         _bGUIUpdateFlag = false;
      }
      break;

   default:
      if (ShowPointsOldRenderingFlag)
      {
         ShowPointsOldRenderingFlag = false;
         setTrackPointsVisibility(false); // Per larry 2010-06, leave this OFF!
      }
      break;
   } // switch

   GDewarpTool->getStatusMessage(StatusMessage);
   GDewarpTool->getTimeMessage(TimeMessage);
   UpdateToolStatusBar(STATUS_BAR_STATUS, StatusMessage);
   UpdateToolStatusBar(STATUS_BAR_MESSAGE, TimeMessage);
}
//---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
   if (DewarpForm == NULL)  return false;

   return DewarpForm->runExecButtonsCommand(command);
}
//---------------------------------------------------------------------------

bool TDewarpForm::runExecButtonsCommand(EExecButtonId command)
{
   return ExecButtonsToolbar->RunCommand(command);
}
//---------------------------------------------------------------------------

void TDewarpForm::enterAnchorSettingMode(int trackingPointTag)
{
   _tagOfTrackingPointGettingAnchored = trackingPointTag;
   _mostRecentlySelectedAnchor = INVALID_TRACKING_POINT_TAG;

   // Haha STUPID tracking points editor API - tells it to not allow dragging
   // of the next selected point!
   GDewarpTool->GetTrackingPointsEditor()->setCtrl(true);
}
//---------------------------------------------------------------------------

void TDewarpForm::exitFromAnchorSettingMode()
{
   _tagOfTrackingPointGettingAnchored = INVALID_TRACKING_POINT_TAG;

   // Erase stretchy yellow line
   GDewarpTool->getSystemAPI()->refreshFrameCached();

   // Haha STUPID tracking points editor API - tells it to allow dragging
   // of the next selected point!
   GDewarpTool->GetTrackingPointsEditor()->setCtrl(false);
}
//---------------------------------------------------------------------------

void TDewarpForm::cancelAnchorSettingMode()
{
   exitFromAnchorSettingMode();
   _mostRecentlySelectedAnchor = INVALID_TRACKING_POINT_TAG;
   _hackTagToPreventBogusReanchoring = INVALID_TRACKING_POINT_TAG;
}
//---------------------------------------------------------------------------

bool TDewarpForm::areWeInAnchorSettingMode()
{
   return _tagOfTrackingPointGettingAnchored != INVALID_TRACKING_POINT_TAG;
}
//---------------------------------------------------------------------------

bool AreWeInAnchorSettingMode()
{
    return DewarpForm->areWeInAnchorSettingMode();
}
//---------------------------------------------------------------------------

bool TDewarpForm::areWeInAnchorEditingMode()
{
   return EditAnchorsCheckBox->Enabled && EditAnchorsCheckBox->Checked;
}
//---------------------------------------------------------------------------

bool AreWeInAnchorEditingMode()
{
    return DewarpForm->areWeInAnchorEditingMode();
}
//---------------------------------------------------------------------------

void CancelAnchorSettingMode()
{
    DewarpForm->cancelAnchorSettingMode();
}
//---------------------------------------------------------------------------

tpAnchor TDewarpForm::getSelectedAnchorTypeFromComboBox()
{
   if (!EditAnchorsCheckBox->Checked)
   {
      return EANCHOR_NONE;
   }

   // Translate selected combo box text to anchor type
   int n;
   string selected = StringToStdString(AnchorMotionComboBox->Text);
   bool successfullyTranslatedComboBoxTextToAnchorType = AnchorAssociations.ValueByDisplayName(selected, n);
   MTIassert(successfullyTranslatedComboBoxTextToAnchorType);
   if (!successfullyTranslatedComboBoxTextToAnchorType)
   {
      // return "None".
      n = 0;
   }

   return (tpAnchor) n;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::EditAnchorsCheckBoxClick(TObject *Sender)
{
   if (EditAnchorsCheckBox->Checked)
   {
      // Old value is remembered in the tag on the control!
      if (AnchorMotionComboBox->Tag == -1)
      {
         // Always show something when enabled.
         AnchorMotionComboBox->Tag = 0;
      }

      AnchorMotionComboBox->ItemIndex = AnchorMotionComboBox->Tag;
      AnchorMotionComboBox->Enabled = true;

      // For less confusion, deselect all points at this time
      GDewarpTool->GetTrackingPointsEditor()->unselectAllTrackingPoints();
      GDewarpTool->GetTrackingPointsEditor()->resetLastSelected();
      GDewarpTool->getSystemAPI()->refreshFrameCached();
   }
   else
   {
      AnchorMotionComboBox->Tag = AnchorMotionComboBox->ItemIndex;
      AnchorMotionComboBox->ItemIndex = -1;
      AnchorMotionComboBox->Enabled = false;
   }

   // Force frame redraw to get tracking points redrawn in correct color.
   GDewarpTool->getSystemAPI()->refreshFrameCached();

   // Any anchoring in progress is hereby canceled.
   cancelAnchorSettingMode();
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AnchorMotionComboBoxChange(TObject *Sender)
{
   // Set shadow copy of present setting.
   AnchorMotionComboBox->Tag = AnchorMotionComboBox->ItemIndex;

   if (!areWeInAnchorSettingMode())
   {
      // To avoid user confusion on whether or not the selected point is
      // being changed, unselect all points.
      GDewarpTool->GetTrackingPointsEditor()->unselectAllTrackingPoints();
      GDewarpTool->GetTrackingPointsEditor()->resetLastSelected();
      GDewarpTool->getSystemAPI()->refreshFrameCached();

      // Other than that, don't do anything
      return;
   }

   // We only need to do anything if we are changing from anchored mode
   // to any of the fixed modes while we're in the middle of setting the anchor.
   tpAnchor anchorType = getSelectedAnchorTypeFromComboBox();
   switch (anchorType)
   {
   case EANCHOR_HORZIONAL:
   case EANCHOR_VERTICAL:
   case EANCHOR_ALL:
      // Don't do anything, stay in "anchor setting" mode.
      break;

   case EFIXED_HORIZIONAL:
   case EFIXED_VERTICAL:
   case EFIXED_ALL:
   case EANCHOR_NONE:
      {
         // Just set the "fixed" restriction! Do this FIRST because
         //exitFromAnchorSettingMode() resets _tagOfTrackingPointGettingAnchored!
         GDewarpTool->TrackingArray.setAnchor(_tagOfTrackingPointGettingAnchored, anchorType);

         cancelAnchorSettingMode();

         break;
      }

   default:
      // Bwahhh! Coding error!
      const bool GotValidAnchorValueFromComboBox = false;
      MTIassert(GotValidAnchorValueFromComboBox);
   }


//   int n;
//   tpAnchor AnchorValue;
//   if (!AnchorAssociations.ValueByDisplayName(StringToStdString(AnchorMotionComboBox->Text), n))
//   {
//      MessageDlg("Unknown Anchor Value\n" + AnchorMotionComboBox->Text, mtError, TMsgDlgButtons() << mbOK, 0);
//      return;
//   }
//
//   int nTag = LastSelectedTag();
//   int anchorTag = INVALID_TRACKING_POINT_TAG;
//
//   AnchorValue = (tpAnchor)n;
//
//   CTrackingArray &TA = GDewarpTool->TrackingArray;
//   if (!TA.isTemporalTagValid(nTag))
//   {
//      return;
//   }
//
//   // In some cases, we must ask to specify the anchor tag.
//   switch (AnchorValue)
//   {
//   case EANCHOR_HORZIONAL:
//   case EANCHOR_VERTICAL:
//   case EANCHOR_ALL:
//      // Ugly, ugly code
//      if (AnchorSelectForm == NULL)
//      {
//         AnchorSelectForm = new TAnchorSelectForm(this);
//      }
//      REMOVE_CBHOOK(selectedTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
//      AnchorSelectForm->Show();
//      do
//      {
//         Application->ProcessMessages();
//         Sleep(10);
//      }
//      while (AnchorSelectForm->Visible);
//
//      anchorTag = AnchorSelectForm->SelectedTag;
//      if (anchorTag != INVALID_TRACKING_POINT_TAG)
//      {
//         TA.setAnchor(nTag, AnchorValue);
//         TA.setAnchorIndex(nTag, anchorTag);
//      }
//
//      GDewarpTool->SetMouseUpDeferredTrackPointSelectionHackTag(nTag);
//
//      SET_CBHOOK(selectedTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
//      break;
//
//   default:
//      TA.setAnchor(nTag, AnchorValue);
//      break;
//   }
}
//---------------------------------------------------------------------------

//void TDewarpForm::UpdateAnchorMotionGUIFromTag(int nTag)
//{
//   CTrackingArray &TA = GDewarpTool->TrackingArray;
//   MotionRestrictionLabel->Enabled = TA.isTemporalTagValid(nTag);
//   AnchorMotionComboBox->Enabled = MotionRestrictionLabel->Enabled;
//   if (!AnchorMotionComboBox->Enabled)
//   {
//      // No point is selected, so go dark.
//      AnchorRestrictionLabel->Caption = "";
//      AnchorMotionComboBox->ItemIndex = -1;
//      return;
//   }
//
//   // Inhibit callbacks
//   TNotifyEvent oldChange = AnchorMotionComboBox->OnChange;
//   AnchorMotionComboBox->OnChange = NULL;
//   AnchorMotionComboBox->ItemIndex = AnchorAssociations.Index(TA.getAnchor(nTag));
//   switch (TA.getAnchor(nTag))
//     {
//       case EANCHOR_VERTICAL:
//       case EANCHOR_HORZIONAL:
//       case EANCHOR_ALL:
//         AnchorRestrictionLabel->Caption = Format("Point %d motion restriction (anchored to point %d):",ARRAYOFCONST((nTag, TA.getAnchorIndex(nTag))));
//         break;
//
//       default:
//         AnchorRestrictionLabel->Caption = Format("Point %d motion restriction:",ARRAYOFCONST((nTag)));
//         break;
//     }
//   AnchorMotionComboBox->OnChange = oldChange;
//}
//---------------------------------------------------------------------------

int  TDewarpForm::LastSelectedTag(void)
{
   if (GDewarpTool == NULL) return INVALID_TRACKING_POINT_TAG;
   return  GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::ShowPointCBoxClick(TObject *Sender)
{
    GDewarpTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::PreviewFrameButtonClick(TObject *Sender)
{
     // Preview a single frame
   RenderPreviewProcessing(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::LoadPointsButtonClick(TObject *Sender)
{
   // Do this FIRST!
   bool restoreMarks = !GDewarpTool->isShiftOn();

   string suggestedName = GDewarpTool->GetSuggestedTrackingDataLoadFilename();
   OpenTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedName).c_str();

   bool retVal = OpenTrackingPointsDialog->Execute();
   if (retVal)
   {
      LoadTrackingPoints(OpenTrackingPointsDialog->FileName, restoreMarks);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::SavePointsButtonClick(TObject *Sender)
{
   string suggestedName = GDewarpTool->GetSuggestedTrackingDataSaveFilename();
   SaveTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedName).c_str();

   bool retVal = SaveTrackingPointsDialog->Execute();
   if (retVal)
   {
     SaveTrackingPoints(SaveTrackingPointsDialog->FileName);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::DownTimerTimer(TObject *Sender)
{
    if (GlobalSender == NULL)
      {
        DownTimer->Enabled = false;
        return;
      }

    TSpeedButton *sb = dynamic_cast<TSpeedButton *>(GlobalSender);

    sb->OnClick(sb);
    DownTimer->Interval = 75;

}

//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AutoMoveButtonMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    GlobalSender = Sender;
    TSpeedButton *sb = dynamic_cast<TSpeedButton *>(GlobalSender);
    sb->OnClick(sb);

    DownTimer->Interval = 250;
    DownTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::AutoMoveButtonMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    GlobalSender = NULL;
    DownTimer->Enabled = false;
}

//---------------------------------------------------------------------------

void __fastcall TDewarpForm::WarnMarkOutMoveCheckBoxClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;
   GDewarpTool->setWarningDialogs(WarnMarkOutMoveCheckBox->Checked);
}
//---------------------------------------------------------------------------

void TDewarpForm::formCreate(void)
{
	// PRESETS!
	auto iniFileName = CPMPIniFileName("Dewarp");
	auto presets = GDewarpTool->GetPresets();
	SET_CBHOOK(CurrentPresetHasChanged, presets->SelectedIndexChanged);
	PresetsFrame->Initialize(presets, iniFileName);

	_bGUIUpdateFlag = false;
   _RestoreHeight = Height;
   _RestoreWidth = Width;
   _KeyDownCount = 0;
   _bAdvancedPanelOn = false;
   _toolDisabled = false;
   _nextSelectedTagIsANewTrackingPoint = INVALID_TRACKING_POINT_TAG;
   _mostRecentlySelectedAnchor = INVALID_TRACKING_POINT_TAG;
   _tagOfTrackingPointGettingAnchored = INVALID_TRACKING_POINT_TAG;

//   GoodInSpinEdit->SetRange(0, 1);
//   GoodInSpinEdit->SetValue(1);
//   GoodOutSpinEdit->SetRange(0, 1);
//   GoodOutSpinEdit->SetValue(1);
   AnchorBothRadioButton->Checked = true;

   // Fill in the tags
   PreviewFrameButton->Tag = TOOL_PROCESSING_CMD_PREVIEW_1;
   PreviewAllButton->Tag = TOOL_PROCESSING_CMD_PREVIEW;
   RenderButton->Tag = TOOL_PROCESSING_CMD_RENDER;
   StopButton->Tag = TOOL_PROCESSING_CMD_STOP;
   TrackButton->Tag = TOOL_PROCESSING_CMD_PREVIEW;

   UFunctionComboBox->Items->Clear();
   for (int i=0; i < UFunctionAssociations.Size(); i++)
     UFunctionComboBox->Items->Add(UFunctionAssociations.DisplayNameByIndex(i).c_str());
   UFunctionComboBox->ItemIndex = 0;

   MissingDataPrevFrameRadioButton->Checked = true;

   AnchorMotionComboBox->Items->Clear();

   // In the following, we start at 1 to skip >None" because we don't need/want
   // it due to new check box to control anchor mode on/off.
   for (int i = 0; i < AnchorAssociations.Size(); i++)
   {
      AnchorMotionComboBox->Items->Add(AnchorAssociations.DisplayNameByIndex(i).c_str());
   }

   AnchorMotionComboBox->ItemIndex = -1;
   AnchorMotionComboBox->Tag = AnchorAssociations.Index(EANCHOR_ALL);  // Effectively sets the default setting.

   // Either display or don't display warp function
#ifdef _DEBUG
   UFunctionComboBox->Visible = true;
   WarpLabel->Visible = true;
#else
	UFunctionComboBox->Visible = false;
   WarpLabel->Visible = false;
#endif

   ShowPointsOldRenderingFlag = false;
   ShowPointsOldCheckedState = ShowPointCBox->Checked;

   TrackingBoxSizeTrackEdit->Enable(true);

// This and MotionTrackEdit was removed because if we set the default to be
// 2k and the initial clip was 4k, each startup would see that the range was
// set to 2k and then double the size.  This doubled size would be saved and the
// next startup double it again.  If the min and max are not set to 2k, then no
// change in size is made on startup regarless if it is 2k or 4k
//
//   TrackingBoxSizeTrackEdit->SetMinAndMax(TRACKING_SLIDER_MIN_2K,
//										  TRACKING_SLIDER_MAX_2K);
   TrackingBoxSizeTrackEdit->Edit->Font->Style = TFontStyles();
   TrackingBoxSizeTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();

   MotionTrackEdit->Enable(true);

//   MotionTrackEdit->SetMinAndMax(SEARCH_SLIDER_MIN_2K,
//                                 SEARCH_SLIDER_MAX_2K);
   MotionTrackEdit->Edit->Font->Style = TFontStyles();
   MotionTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();

   SmoothTrackEdit->Enable(true);
   SmoothTrackEdit->SetMinAndMax(0, 10);
   SmoothTrackEdit->Edit->Font->Style = TFontStyles();
   SmoothTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();
   SmoothTrackEdit->SetDontNotifyIfMouseButtonIsStillDown(true);

   IndividualSmoothTrackEdit->Enable(IndividualSmoothCBox->Checked);
   IndividualSmoothTrackEdit->SetMinAndMax(0, 10);
   IndividualSmoothTrackEdit->Edit->Font->Style = TFontStyles();
   IndividualSmoothTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();

   GDewarpTool->SetToolProgressMonitor(ExecStatusBar);
}
//---------------------------------------------------------------------------

void TDewarpForm::formShow(void)
{
   _tagOfTrackingPointGettingAnchored = INVALID_TRACKING_POINT_TAG;

   // Set the parameters
   if (GDewarpTool != NULL)
     {
       GDewarpTool->setDisplayRect(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
	    GDewarpTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
       GDewarpTool->SetClearTrackingStuffOnNewShot(ClearOnShotChangeCBox->Checked);
       TrackingTrackBarChange(NULL);
       SearchTrackBarChange(NULL);

       // called from toolactivate now: ClipHasChanged(NULL);
       SET_CBHOOK(selectedTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
		 SET_CBHOOK(rangeChanged, GDewarpTool->GetTrackingPointsEditor()->rangeChange);
		 SET_CBHOOK(handleNewTrackingPointNotification, GDewarpTool->GetTrackingPointsEditor()->NextSelectedTagIsANewTrackingPoint);
//		 SET_CBHOOK(anchorChanged, GoodInSpinEdit->SpinEditValueChange);
//		 SET_CBHOOK(anchorChanged, GoodOutSpinEdit->SpinEditValueChange);
		 SET_CBHOOK(ClipHasChanged, GDewarpTool->ClipChange);
       SET_CBHOOK(ClipChanging, GDewarpTool->ClipChanging);
       SET_CBHOOK(MarksHaveChanged, GDewarpTool->MarksChange);
    }

   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
   _TabStopDoNotDeletePanelVisible = true;
   _nextSelectedTagIsANewTrackingPoint = false;
   _mostRecentlySelectedAnchor = INVALID_TRACKING_POINT_TAG;
}
//---------------------------------------------------------------------------

void TDewarpForm::formHide(void)
{
   // Turn off tracking points and windows
   if (GDewarpTool != NULL)
     {
       GDewarpTool->setDisplayRect(false);
       GDewarpTool->setDisplayTrackingPoints(false);
       REMOVE_CBHOOK(selectedTagChanged, GDewarpTool->GetTrackingPointsEditor()->lastSelectedTag);
		 REMOVE_CBHOOK(rangeChanged, GDewarpTool->GetTrackingPointsEditor()->rangeChange);
		 REMOVE_CBHOOK(handleNewTrackingPointNotification, GDewarpTool->GetTrackingPointsEditor()->NextSelectedTagIsANewTrackingPoint);
//		 REMOVE_CBHOOK(anchorChanged, GoodInSpinEdit->SpinEditValueChange);
//		 REMOVE_CBHOOK(anchorChanged, GoodOutSpinEdit->SpinEditValueChange);
       REMOVE_CBHOOK(ClipHasChanged, GDewarpTool->ClipChange);
       REMOVE_CBHOOK(ClipChanging, GDewarpTool->ClipChanging);
       REMOVE_CBHOOK(MarksHaveChanged, GDewarpTool->MarksChange);
     }

   // Do this BEFORE saving anchor points!
   cancelAnchorSettingMode();

   //  See if we want to autosave the tracking points
   SaveTrackingPointIfNecessary();

   ColorPickButton->Down = false;
   ColorPickerStateTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::PDLCaptureTBItemClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GDewarpTool->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------------ EXEC BUTTONS TOOLBAR -----------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::ExecButtonsToolbar_ButtonPressedNotifier(
      TObject *Sender)
{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
      default:
      case EXEC_BUTTON_NONE:
         // No-op
      break;

      case EXEC_BUTTON_PREVIEW_FRAME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW_1);
      break;

      case EXEC_BUTTON_PREVIEW_ALL:
         GDewarpTool->AutoSaveTrackingPoints();
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
      break;

      case EXEC_BUTTON_RENDER_FRAME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER_1);
      break;

      case EXEC_BUTTON_RENDER_ALL:
         GDewarpTool->AutoSaveTrackingPoints();
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
      break;

      case EXEC_BUTTON_PAUSE_OR_RESUME:
         if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
         else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_PAUSE:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
      break;

      case EXEC_BUTTON_RESUME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_STOP:
         ExecStop();
      break;

      case EXEC_BUTTON_CAPTURE_PDL:
         ExecCaptureToPDL();
      break;

      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         ExecCaptureAllEventsToPDL();
      break;

      case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         ExecGoToResumeTimecode();
      break;

      case EXEC_BUTTON_SET_RESUME_TC:
         ExecSetResumeTimecodeToCurrent();
      break;
   }
}

//---------------- Resume Timecode Functions --------------------------------

void TDewarpForm::ExecSetResumeTimecode(int frameIndex)
{
   CVideoFrameList *frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   if (frameIndex < 0)
      frameIndex = frameList->getInFrameIndex();

   GDewarpTool->ResetResumeFrameIndex(frameIndex);
   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecSetResumeTimecodeToCurrent(void)
{
   if (GDewarpTool == NULL) return;

   int frameIndex = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecSetResumeTimecodeToDefault(void)
{
   if (GDewarpTool == NULL) return;

   int frameIndex = GDewarpTool->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);

   // NOTE!! Old way used to do:
   //      GDewarpTool->ResetResumeFrameIndex(-1)
   // but we just let it be set to the mark in frame by ExecSetResumeTimecode!
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecGoToResumeTimecode(void)
{
   if (GDewarpTool == NULL) return;

   CVideoFrameList *frameList;
   frameList = GDewarpTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // No! GDewarpTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GDewarpTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = GDewarpTool->getSystemAPI()->getLastFrameIndex();
   GDewarpTool->ResetResumeFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(
                        frameList->getTimecodeForFrameIndex(frameIndex));
}

//------------------ ExecRenderOrPreview ------------------------------------
//
//  Call this when any render or preview button is clicked
//  buttonCommand identifies the button that was pressed
//
void TDewarpForm::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
   // Consistence check
   int markIn = GDewarpTool->getSystemAPI()->getMarkIn();
   //int markOut = GDewarpTool->getSystemAPI()->getMarkOut();

   if (GDewarpTool->TrackingArray.numberTrackPointsPerFrame() < 3 ||
       GDewarpTool->TrackingArray.at(markIn).validSize() < 2)    // WTF? 2??? QQQ
     {
       theError.set(DEWARP_ERROR_DEWARP_TOO_FEW_TRACKPOINTS, "At least three tracking points must be chosen.");
       _MTIErrorDialog(Handle, theError.getFmtError());
       return;
     }

   // Check the tracking points

   if (GDewarpTool == NULL) return;

   // Did the resume timecode change?
   if (buttonCommand == TOOL_PROCESSING_CMD_CONTINUE)
      ExecGoToResumeTimecode();

   // Pass the button's tool processing command to Process Handler
   GDewarpTool->bTrackOnly = false;   // Ugly as sin
   GDewarpTool->bNeedToReTrack =
      (!GDewarpTool->IsNecessaryTrackDataAvailable()) ||
      (TrackingBoxSizeTrackEdit->GetValue() != GDewarpTool->ProcessedTrackBoxExtent) ||
      (MotionTrackEdit->GetValue() != GDewarpTool->ProcessedSearchBoxExtent);
   if (GDewarpTool->bNeedToReTrack)
      GDewarpTool->InvalidateSmoothing();

   GDewarpTool->ButtonCommandHandler(buttonCommand);
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecStop(void)
{
   if (GDewarpTool == NULL) return;

   GDewarpTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_STOP);
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecCaptureToPDL(void)
{
   if (GDewarpTool == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GDewarpTool->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------

void TDewarpForm::ExecCaptureAllEventsToPDL(void)
{
   if (GDewarpTool == NULL) return;

   GDewarpTool->getSystemAPI()->CapturePDLEntry(true);
}
//---------------------------------------------------------------------------

void TDewarpForm::UpdateExecutionButtonToolbar(
      EToolControlState toolProcessingControlState,
      bool processingRenderFlag)
{
   if (GDewarpTool == NULL) return;

   int previewFrameIndex = GDewarpTool->GetPreviewFrameIndex();
   bool processingSingleFrameFlag = (previewFrameIndex >= 0 );
   EDewarpControlState dewarpState = GDewarpTool->GetControlState();

   if (GDewarpTool->IsDisabled())
   {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                 "Can't preview - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                 "Can't render - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                 "Can't add to PDL - tool is disabled");
         return;
   }

   switch(toolProcessingControlState)
   {
      case TOOL_CONTROL_STATE_STOPPED :

         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button

         setIdleCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);


         if ((!GDewarpTool->IsNecessaryTrackDataAvailable()) ||
               GDewarpTool->bNeedToReTrack)
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                    "Can't preview - invalid tracking data");

            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                    "Can't render - invalid tracking data");

            if (TrackButton->Enabled)
            {
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Add to PDL (, key)");
            }
            else
            {
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Can't add to PDL - no tracking points");
            }
         }
         else
         {
            if (GDewarpTool->AreMarksValid())
            {
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);

//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                       "Preview marked range (Shift+D)");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                       "Render marked range (Shift+G)");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                       "Add to PDL (, key)");
            }
            else
            {
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                       "Can't preview - marks are invalid");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                       "Can't render - marks are invalid");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                       "Can't add to PDL - marks are invalid");
            }
         }

         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

         ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

         ExecButtonsToolbar->EnableResumeWidget();
         ExecSetResumeTimecodeToDefault();

      break;

      case TOOL_CONTROL_STATE_RUNNING :

         // Preview1, Render1, Preview or Render is now running

         setBusyCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         if (processingSingleFrameFlag || (dewarpState == DEWARP_STATE_TRACK))
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         else
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (!processingRenderFlag)
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
         else
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

      break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused

         setIdleCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         // Now go load the current timecode
         ExecSetResumeTimecodeToCurrent();

      break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :

         // Processing single frame or waiting to stop or pause --
         // disable everything
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE
          && toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_STOP)
         {
            if (!processingRenderFlag)
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
            else
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
         }

      break;

      default:
         MTIassert(false);

      break;
   }

   UpdateStatusBarInfo();
   UpdateTrackingPointButtons();

} // UpdateExecutionButton()
//---------------------------------------------------------------------------

void TDewarpForm::setIdleCursor(void)
{
   if (GDewarpTool == NULL) return;

   if (!GDewarpTool->IsInColorPickMode())
       Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void TDewarpForm::setBusyCursor(void)
{
   Screen->Cursor = crAppStart;
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::NextPointButtonClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   GDewarpTool->SelectTrackingPointRelative(+1);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::PrevPointButtonClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   GDewarpTool->SelectTrackingPointRelative(-1);
}
//---------------------------------------------------------------------------

void ToggleTrackPointVisibility(void)
{
   if (DewarpForm == NULL) return;

   DewarpForm->toggleTrackPointVisibility();
}

void TDewarpForm::toggleTrackPointVisibility(void)
{
   if (GDewarpTool == NULL) return;

   if (ShowPointCBox->Enabled)
   {
      ShowPointCBox->Checked = !ShowPointCBox->Checked;
      GDewarpTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
   }
}
//---------------------------------------------------------------------------

void SetTrackPointsVisibility(bool flag)
{
   if (DewarpForm == NULL) return;

   DewarpForm->setTrackPointsVisibility(flag);
}

void TDewarpForm::setTrackPointsVisibility(bool flag)
{
   if (GDewarpTool == NULL) return;

   ShowPointCBox->Checked = flag;
   GDewarpTool->setDisplayTrackingPoints(flag);
}
//---------------------------------------------------------------------------

void ToggleProcRegionVisibility(void)
{
   if (DewarpForm == NULL) return;

   DewarpForm->toggleProcRegionVisibility();
}

void TDewarpForm::toggleProcRegionVisibility(void)
{
   if (GDewarpTool == NULL) return;

   ShowMattingBoxCBox->Checked = !ShowMattingBoxCBox->Checked;
   GDewarpTool->setDisplayRect(ShowMattingBoxCBox->Checked);
}
//---------------------------------------------------------------------------

void ToggleProcRegionOperation(void)
{
   if (DewarpForm == NULL) return;

   DewarpForm->toggleProcRegionOperation();
}

void TDewarpForm::toggleProcRegionOperation(void)
{
   if (GDewarpTool == NULL) return;

   UseMattingCBox->Checked = !UseMattingCBox->Checked;
}
//---------------------------------------------------------------------------

void SetControlFocus(int controlCommand)
{
   if (DewarpForm == NULL) return;

   DewarpForm->setControlFocus(controlCommand);
}

void TDewarpForm::setControlFocus(int controlCommand) {
   if ((GDewarpTool == NULL) || GDewarpTool->IsDisabled() || !IsToolVisible())
      return;

   TObject *newFocusedObject = NULL;

   switch (controlCommand)
   {
      case DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE:
         newFocusedObject = TrackingBoxSizeTrackEdit->TrackBar;
         break;

      case DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA:
         newFocusedObject = MotionTrackEdit->TrackBar;
         break;

      case DWRP_CMD_FOCUS_ON_SMOOTHING:
         newFocusedObject = SmoothTrackEdit->TrackBar;
         break;

      case DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT:
         if (MissingDataBlackRadioButton->Checked)
         {
            MissingDataWhiteRadioButton->Checked = true;
			newFocusedObject = DataFillGroupBox;
         }
         else if (MissingDataPrevFrameRadioButton->Checked)
         {
            MissingDataBlackRadioButton->Checked = true;
			newFocusedObject = DataFillGroupBox;
         }
         else // default
         {
            MissingDataPrevFrameRadioButton->Checked = true;
            newFocusedObject = DataFillGroupBox;
         }
         break;

      case DWRP_CMD_CYCLE_MISSING_DATA_FILL_PREV:
         if (MissingDataWhiteRadioButton->Checked)
         {
            MissingDataBlackRadioButton->Checked = true;
			newFocusedObject = DataFillGroupBox;
         }
         else if (MissingDataPrevFrameRadioButton->Checked)
         {
            MissingDataWhiteRadioButton->Checked = true;
			newFocusedObject = DataFillGroupBox;
         }
         else
         {
            MissingDataPrevFrameRadioButton->Checked = true;
            newFocusedObject = DataFillGroupBox;
         }
         break;

      case DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_IN:
//         newFocusedObject = GoodInSpinEdit->Edit;
         newFocusedObject = AnchorFirstRadioButton;
         break;

      case DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_OUT:
//         newFocusedObject = GoodOutSpinEdit->Edit;
         newFocusedObject = AnchorFirstRadioButton;
         break;

      case DWRP_CMD_CYCLE_ANCHOR_MODES:
         if (AnchorFirstRadioButton->Checked)
         {
            AnchorLastRadioButton->Checked = true;
				newFocusedObject = AnchorFramesGroupBox;
			}
         else if (AnchorLastRadioButton->Checked)
         {
            AnchorBothRadioButton->Checked = true;
			   newFocusedObject = AnchorFramesGroupBox;
			}
#ifdef CYCLE_ANCHOR_INCLUDES_NONE
			else if (AnchorBothRadioButton->Checked)
			{
				AnchorNoneRadioButton->Checked = true;
				newFocusedObject = AnchorFramesGroupBox;
			}
#endif
			else
         {
            AnchorFirstRadioButton->Checked = true;
            newFocusedObject = AnchorFramesGroupBox;
         }
         break;

      case DWRP_CMD_FOCUS_ON_INDIVIDUAL_SMOOTHING:
         if (IndividualSmoothCBox->Checked)
         {
            newFocusedObject = IndividualSmoothTrackEdit->TrackBar;
         }
         else
         {
            newFocusedObject = IndividualSmoothCBox;
         }
         break;

      case DWRP_CMD_FOCUS_ON_MOTION_RESTRICTION:
         newFocusedObject = AnchorMotionComboBox;
         break;

      default:

         break;
   }

   if (newFocusedObject != NULL)
   {
      GDewarpTool->getSystemAPI()->FocusAdoptedControl(reinterpret_cast<int *>(newFocusedObject));
   }
}
//---------------------------------------------------------------------------

string GetFilenameForSavingTrackingPoints(const string &suggestedFilename)
{
   if (DewarpForm == NULL) return string("");

   return DewarpForm->getSaveFilename(suggestedFilename);
}

string TDewarpForm::getSaveFilename(const string &suggestedFilename)
{
   SaveTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedFilename).c_str();
//   string suggestedPath = RemoveDirSeparator(GetFilePath(suggestedFilename));
//   if (!suggestedPath.empty())
//   {
//      SaveTrackingPointsDialog->InitialDir = GetFilePath(suggestedFilename).c_str();
//   }
   bool retVal = SaveTrackingPointsDialog->Execute();
   if (!retVal || SaveTrackingPointsDialog->FileName.IsEmpty())
   {
      return string("");
   }

   return StringToStdString(SaveTrackingPointsDialog->FileName);
}
//---------------------------------------------------------------------------

string GetFilenameForLoadingTrackingPoints(const string &suggestedFilename)
{
   if (DewarpForm == NULL) return string("");

   return DewarpForm->getLoadFilename(suggestedFilename);
}

string TDewarpForm::getLoadFilename(const string &suggestedFilename)
{
   OpenTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedFilename).c_str();
//   string suggestedPath = RemoveDirSeparator(GetFilePath(suggestedFilename));
//   if (!suggestedPath.empty())
//   {
//      OpenTrackingPointsDialog->InitialDir = GetFilePath(suggestedFilename).c_str();
//   }
   bool retVal = OpenTrackingPointsDialog->Execute();
   if (!retVal || OpenTrackingPointsDialog->FileName.IsEmpty())
   {
      return string("");
   }

   return StringToStdString(OpenTrackingPointsDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::ColorPickButtonClick(TObject *Sender)
{
   if (GDewarpTool == NULL) return;

   if (GDewarpTool->IsInColorPickMode())
   {
      GDewarpTool->ExitColorPickMode();
      ColorPickButton->Down = false;
      ColorPickerStateTimer->Enabled = false;
   }
   else
   {
      GDewarpTool->EnterColorPickMode();
      ColorPickButton->Down = true;
      ColorPickerStateTimer->Enabled = true;
   }
}
//---------------------------------------------------------------------------

void ToggleColorPicker(void)
{
   if (DewarpForm == NULL) return;
   DewarpForm->ColorPickButtonClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::ColorPickerStateTimerTimer(TObject *Sender)
{
   if ((GDewarpTool == NULL) || !GDewarpTool->IsInColorPickMode())
   {
      ColorPickButton->Down = false;
      ColorPickerStateTimer->Enabled = false;
   }

}
//---------------------------------------------------------------------------


void __fastcall TDewarpForm::ClearOnShotChangeCBoxClick(TObject *Sender)
{
  if (GDewarpTool == NULL) return;

  GDewarpTool->SetClearTrackingStuffOnNewShot(ClearOnShotChangeCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::TabToBegining(TObject *Sender)
{
	// This is called before the spinedit control is made visible
	try
	{
		if (_TabStopDoNotDeletePanelVisible)
		{
//			GoodInSpinEdit->Edit->SetFocus();
			AnchorFirstRadioButton->SetFocus();
		}
	}
	catch (...)
	{
	}
}
//---------------------------------------------------------------------------

void __fastcall TDewarpForm::SwapButtonClick(TObject *Sender)
{
//	SwapInOut();
}

void SwapInOut()
{
//   if (DewarpForm == NULL) return;
//
//   DewarpForm->swapInOut();
}

void TDewarpForm::swapInOut()
{
//   if (AnchorLastRadioButton->Checked)
//   {
//      AnchorFirstRadioButton->Checked = true;
//      newFocusedObject = AnchorFramesGroupBox;
//   }
//   else if (AnchorFirstRadioButton->Checked)
//   {
//      AnchorLastRadioButton->Checked = true;
//      newFocusedObject = AnchorFramesGroupBox;
//   }
}
//---------------------------------------------------------------------------

void ToggleClearOnShotChange()
{
   if (DewarpForm == NULL) return;

   DewarpForm->toggleClearOnShotChange();
}

void TDewarpForm::toggleClearOnShotChange()
{
   ClearOnShotChangeCBox->Checked = !ClearOnShotChangeCBox->Checked;
}
//---------------------------------------------------------------------------



void __fastcall TDewarpForm::MissingDataRadioButtonClick(TObject *Sender)
{
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

