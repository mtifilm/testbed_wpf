//---------------------------------------------------------------------------

#ifndef AnchorSelectUnitH
#define AnchorSelectUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>

#include "PropX.h"
//---------------------------------------------------------------------------
class TAnchorSelectForm : public TForm
{
__published:	// IDE-managed Components
        TBitBtn *Can;
        TLabel *Label1;
        TImage *Image1;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormHide(TObject *Sender);
		void __fastcall CanClick(TObject *Sender);

private:	// User declarations
		DEFINE_CBHOOK(AnchorTagChanged, TAnchorSelectForm);
		int CurrentTag;

public:		// User declarations
        __fastcall TAnchorSelectForm(TComponent* Owner);
        int SelectedTag;
};
//---------------------------------------------------------------------------
extern PACKAGE TAnchorSelectForm *AnchorSelectForm;
//---------------------------------------------------------------------------
#endif
