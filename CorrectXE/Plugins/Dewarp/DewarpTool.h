#ifndef DewarpToolH
#define DewarpToolH

#include "DewarpParameters.h"
#include "HRTimer.h"
#include "ImageDatumConvert.h"
#include "IniFile.h"
#include "PresetParameterModel.h"
#include "ToolObject.h"
#include "TrackingPoints.h"
#include "UserInput.h"
#include "MtiCudaCoreInterface.h"

#include <fstream>
#include "bthread.h"

//////////////////////////////////////////////////////////////////////

// Dewarp Tool Number (16-bit number that uniquely identifies this tool)
// TTT
#define DEWARP_TOOL_NUMBER    20

// Dewarp Tool Command Numbers
#define DWRP_CMD_DELETE_ALL_PTS 12

#define DWRP_CMD_NO_OP                            0
#define DWRP_CMD_SET_IN_PT                        1
#define DWRP_CMD_SET_OUT_PT                       2
#define DWRP_CMD_BEGIN_PAN                        3
#define DWRP_CMD_END_PAN                          4
#define DWRP_CMD_TOGGLE_FIX                       5
#define DWRP_CMD_TOGGLE_TRACK_POINT_VISIBILITY    6
#define DWRP_CMD_TOGGLE_PROC_REGION_VISIBILITY    7
#define DWRP_CMD_TOGGLE_PROC_REGION_OPERATION     8

#define DWRP_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT    9
#define DWRP_CMD_STOP_DRAGGING_TRACKPOINT        10
#define DWRP_CMD_SELECT_ANOTHER_TRACKPOINT       11
#define DWRP_CMD_SAVE_TRACKPOINTS                12
#define DWRP_CMD_LOAD_TRACKPOINTS                13
#define DWRP_CMD_CLEAR_TRACKPOINTS               14
#define DWRP_CMD_DELETE_SELECTED_TRACKPOINTS     15

#define DWRP_CMD_TRACK                           16

#define DWRP_CMD_FOCUS_ON_TRACK_BOX_SIZE         17
#define DWRP_CMD_FOCUS_ON_MOTION_SEARCH_AREA     18
#define DWRP_CMD_FOCUS_ON_SMOOTHING              19
#define DWRP_CMD_CYCLE_MISSING_DATA_FILL_NEXT    20
#define DWRP_CMD_CYCLE_MISSING_DATA_FILL_PREV    21
#define DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_IN       22
#define DWRP_CMD_FOCUS_ON_ANCHOR_FRAMES_OUT      23
#define DWRP_CMD_FOCUS_ON_INDIVIDUAL_SMOOTHING   24
#define DWRP_CMD_FOCUS_ON_MOTION_RESTRICTION     25

#define DWRP_CMD_ADD_EVENT_TO_PDL                26
#define DWRP_CMD_EXECUTE_PDL                     27

#define DWRP_CMD_TURN_COLOR_PICKER_ON            28
#define DWRP_CMD_TURN_COLOR_PICKER_OFF           29

#define DWRP_CMD_LOAD_TRACKPOINTS_NOT_MARKS      30
#define DWRP_CMD_SET_MARKS_FROM_TRACK_ARRAY      31

#define DWRP_CMD_SHIFT_IS_ON                     32
#define DWRP_CMD_SHIFT_IS_OFF                    33

#define DWRP_CMD_SWAP_IN_OUT                     34
#define DWRP_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE     35

#define DWRP_CMD_CANCEL                          36

#define DWRP_CMD_TOGGLE_EDIT_ANCHORS_MODE        37

#define DWRP_CMD_PRESET_1                        38
#define DWRP_CMD_PRESET_2                        39
#define DWRP_CMD_PRESET_3                        40
#define DWRP_CMD_PRESET_4                        41

#define DWRP_CMD_CYCLE_ANCHOR_MODES              42

#define DWRP_MAX_THREAD_COUNT   30       // Maximum number of processing threads

// Maximum number of input and output frames per processing iteration
#define DWRP_MAX_OUTPUT_FRAMES   (DWRP_MAX_THREAD_COUNT)
#define DWRP_MAX_ADJACENT_FRAMES 2
#define DWRP_MAX_INPUT_FRAMES (DWRP_MAX_OUTPUT_FRAMES + 2*DWRP_MAX_ADJACENT_FRAMES)

//////////////////////////////////////////////////////////////////////

// Dewarp Tool Control States
enum EDewarpControlState {
   DEWARP_STATE_ALLOCATE = 0,
   DEWARP_STATE_TRACK,          //  Start is a track
   DEWARP_STATE_SMOOTH,         //  Followed by a smoothing of track points
   DEWARP_STATE_RENDER,         //  Finally the render
   DEWARP_STATE_COMPLETED
};

typedef MTI_UINT16 Pixel;
typedef Pixel * Image;


//////////////////////////////////////////////////////////////////////
// Forward Declarations
//
class CTrackingPointsEditor;
class CDisplayer;
class CDewarpToolParameters;
class CColorPickTool;

//////////////////////////////////////////////////////////////////////

class CDewarpTool : public CToolObject
{
public:

   CDewarpTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CDewarpTool();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool DoesToolPreserveHistory();
   virtual CToolProcessor* makeToolProcessorInstance(const bool *);

   // Tool GUI
   virtual int toolHide();
   virtual int toolShow();
   virtual bool IsShowing(void);

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onUserInput(CUserInput &userInput);
   virtual int onNewMarks();
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onChangingFraming();
   virtual int onRedraw(int frameIndex);

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);

   // Called from the GUI
   void ButtonCommandHandler(EToolProcessingCommand buttonCommand);
   void ResetResumeFrameIndex(int resumeFrame);
   bool BoundingBoxDefined(void);
	void SetClearTrackingStuffOnNewShot(bool flag);
	PresetParameterModel<CDewarpParameters> *GetPresets() { return _userPresets; };

   void AutoSaveTrackingPoints();
   void SaveTrackingPointsToFile();
   void LoadTrackingPointsFromFile(bool restoreMarks=true);
   bool SaveTrackingPoints(const string &sName);
   bool LoadTrackingPoints(const string &sName, bool restoreMarks=true);
   void DeleteSelectedTrackingPoints();
   void ClearTrackingPoints();

   CBHook ClipChanging;   // Needed to call back on a change
   CBHook ClipChange;     // Needed to call back on a change
   CBHook MarksChange;    // Needed to call back on a change
   DEFINE_CBHOOK(SetNeedToAutoSave, CDewarpTool);
   bool needToAutoSave;

   // Status information
   void getStatusMessage(string &sMessage);
   void setStatusMessage(const string &NewMessage);
   void getTimeMessage(string &sMessage);
   void setTimeMessage(const string &NewMessage);

   int  readDewarpParameters();
   void writeDewarpParameters();

   void setTrackingBoxSizes(
         int newTrackBoxXRadius,    int newTrackBoxYRadius,
         int newSearchLeftDistance, int newSearchRightDistance,
         int newSearchUpDistance,   int newSearchDownDistance);

   EDewarpControlState GetControlState(void);
   void SetControlState(EDewarpControlState newControlState);

   int GetPreviewFrameIndex(void) { return _previewFrameIndex; }

   CTrackingArray TrackingArray;
   //CTrackingArray ProcessedTrackingArray;
   CTrackingArray SmoothedArray;

   int ProcessedTrackBoxExtent;
   int ProcessedSearchBoxExtent;

   bool bTrackOnly;
   bool bNeedToReTrack;
   int  mouseUpDeferredTrackPointSelectionHackTag;

   void UnselectAllTrackingPoints();
   void SelectTrackingPointAllFrames(int);
   void CreateSelectedTrackingPointAllFrames();
   void TranslateSelectedTrackingPoints(double, double, int);
   CTrackingPointsEditor *GetTrackingPointsEditor(void);
   bool SmoothTrackingPoints(int iGoodStart, int iGoodEnd, double Alpha);
   void SetTrackingPoints(const CTrackingArray &TA);
   bool IsNecessaryTrackDataAvailable();
   bool isAllTrackDataValid();
   void SetMouseUpDeferredTrackPointSelectionHackTag(int nTag);

   void DisplayRectangle(void);

   RECT &getBoundingBox(void) { return _BoundingBox; }
   void setBoundingBox(const RECT &newBox);
   bool FindBoundingBox(int R=25, int G=25, int B=25, int X=0, int Y=0, int slack=50);
   void EnterColorPickMode(void);
   void ExitColorPickMode(void);
   bool IsInColorPickMode(void);

   bool getDisplayRect(void) { return _displayRect; }
   void setDisplayRect(const bool newDisp);

   bool getDisplayTrackingPoints(void);
   void setDisplayTrackingPoints(const bool newDisp);

   void setWarningDialogs(bool value) {_WarningDialogs = value; }
   bool getWarningDialogs(void) {return _WarningDialogs; }

   void ResetLastSelected(void);

   bool HasTrackingArrayChangedSinceLastTracked();
   bool DoesTrackingPointNeedToBeRetracked(int nTag);
   void SelectTrackingPointRelative(int plusOrMinus);
   void InvalidateSmoothing();

   bool isShiftOn();

   string GetSuggestedTrackingDataLoadFilename();
   string GetSuggestedTrackingDataSaveFilename();

protected:
   int ProcessTrainingFrame(void);
   int RunFrameProcessing(int newResumeFrame);
   void DestroyAllToolSetups(bool notIfPaused);
   int MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
                              const CAutotoolStatus& newToolStatus,
                              int newResumeFrame);
   void UpdateExecutionGUI(EToolControlState toolProcessingControlState,bool processingRenderFlag);

   // GUI Related methods
   int CheckForAnyParameterChanges(void);

private:
   RECT _BoundingBox;
   bool _BoundingBoxDefined;
   CThreadLock _MessageLocker;
   string _StatusMessage;
   CThreadLock _TimeMessageLocker;
   string _TimeMessage;
   CDewarpParameters _dewarpParameters;
   string _CurrentClipName;
   bool _toolActive;
   int _oldBusyCursor;
   bool _smoothDataIsValid;
   bool shiftIsOn;     // for Larry's Shift+(click the LOAD button) HACK
   bool _isGpuUsed = false;

   CColorPickTool *ColorPickTool;

   void readBoundingBoxFromDesktopIni(ClipSharedPtr &clip);
   void writeBoundingBoxToDesktopIni(ClipSharedPtr &clip);
   void setBoundingBoxInternal(const RECT &newBox);

   DEFINE_CBHOOK(ColorPickerColorChanged, CDewarpTool);
   DEFINE_CBHOOK(ColorPickerColorPicked, CDewarpTool);

   int GatherParameters(CDewarpToolParameters &toolParams);

   int _resumeFrameIndex;           // to resume after a pause
   int _previewFrameIndex;
   bool _displayRect = true;;
   bool _deferredClipChangeFlag;
   bool _deferredMarkChangeFlag;
   bool _clearTrackingStuffOnNewShot;

   EDewarpControlState _dewarpControlState;
   int toolSetupHandle;    // Init to -1
   int trainingToolSetupHandle;

	string IniFileName;
	PresetParameterModel<CDewarpParameters> *_userPresets;

   // TEMPORARY - remember the state of the Mask Tool, enabled or disabled.
   bool previousMaskToolEnabled;
   // This are just constant strings that are initialized via statis.
   //static string title;

   bool canMarksBeMoved();
   bool canInMarkBeMoved();
   bool canOutMarkBeMoved();
   CTrackingPointsEditor *TrkPtsEditor;
   bool _WarningDialogs;

   int CurrentTrackBoxExtentX;
   int CurrentTrackBoxExtentY;
   int CurrentSearchBoxExtentLeft;
   int CurrentSearchBoxExtentRight;
   int CurrentSearchBoxExtentUp;
   int CurrentSearchBoxExtentDown;

   CHRTimer TrackingTimer;
   int HowLongItTookToTrackInSecs;
   bool ShowedInMarkMoveMessage;
   bool ShowedOutMarkMoveMessage;
   bool TrackingDataHasBeenUsedToRenderAll;
   bool SingleFrameFlag;
};

////////////////////////////////////////////////////////////////////////


class CDewarpToolParameters : public CToolParameters
{
public:
   CDewarpTool *dewarpToolObject;
   CDewarpToolParameters() : CToolParameters(1, 1) {};
   virtual ~CDewarpToolParameters() { };

public:
   CDewarpParameters dewarpParameters;
};

////////////////////////////////////////////////////////////////////////

class CDewarpProc : public CToolProcessor
{
public:
   CDewarpProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CDewarpProc();

   CHRTimer HRT;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);
   bool DewarpSlice(RECT Box);
   CCountingWait CountingWait;

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int DoProcessRenderGpu(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;

   CudaThinplateSpline16u *_thinplateSplineWarper = nullptr;

   // These are used for store in the multithreading model
   // Note:  No locking is done because it is the responsibility
   // of the setup program never to overlap xVec and yVec
   float *_Wx;
   float *_Wy;
   float *_tpSX;
   float *_tpSY;
   float *_xVec;
   float *_yVec;
   int   _N0;

   // Some timing information
   double _dMotionTime;
   double _dDewarpTime;

   // Temporary storage for tracking etc
   int _TrackBoxExtent;      //  the "extent" from the center point of the track box
   int _TrackBoxSize;        //  Always 2*_TrackBoxExtent+1
   int _TrackBoxPoints;      //  Always (_TrackBoxSize)^2
   Image _TrackBoxData;      //  Temp storage to hold box for tracking
   CTrackingPoints TrueTrack;

   int _SearchBoxExtent;     //  the "extent" from the center point of the search box
   int _SearchBoxSize;       //  Always 2*_SearchBoxExtent+1
   int _SearchBoxPoints;     //  Always (_SearchBoxSize)^2
   Image _SearchBoxData;     //  Temp storage to hold box for tracking

   RECT _BoundingBox;       // Area to dewarp
   RECT _ActiveBox;
   DRECT _DewarpBox;         // Largest rectange dewarped over all frames
   IntegerList _AnchorList;
   
   // variables to support multithreading of algorithm
   int _processingThreadCount;

   // Mask Region
   CRegionOfInterest *maskROI;
   CDewarpParameters _dewarpParameters;    // private copy of parameters
   CDewarpTool *_dewarpToolObject;  // pointer to CDewarpTool object [EVIL!!!!]

   // Tracking functions, to be moved later
   MTI_UINT16 *_BaseOrigImage;
   MTI_UINT16 *_BaseMonoImage;
   MTI_UINT16 *_NextOrigImage;
   MTI_UINT16 *_NextMonoImage;
   MTI_UINT16 *_MonoImages[2];
   int _MonoImageIndex;

   MTI_UINT16 *_outputImage;
   MTI_UINT16 *_previousFixedImage;

   bool _imageDataIsHalfs;
   int nPicCol;
   int nPicRow;
   int _pxlComponentCount;
   int _RGB;
   MTI_UINT16 _BlackPixel[3];
   MTI_UINT16 _WhitePixel[3];

   CTrackPoint FindBestMatch(CTrackPoint &tp);
   CTrackPoint FindBestMatchX(CTrackPoint &tp);
   CTrackPoint FindBestMatchY(CTrackPoint &tp);
   CTrackPoint FindBestMatch2(CTrackPoint &tp);
   CTrackPoint FindBestFirstMatch(const CTrackPoint &tp);
   CTrackPoint FindBestRefinedMatch(const CTrackPoint &dpB, Pixel *pBase, const CTrackPoint &dpN, Pixel *pNext);
   void TrackNextFrame(int CurrentFrame);

   int BiInterpolate(double x, double y, int iC, int iR);
   bool DewarpFrame(int Frame);
   void FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut);
   void DewarpImage(float *xVec, float *yVec, RECT &Box);

	inline float imageDatumToFloat(unsigned short datum)
	{
		return ImageDatum::toFloat(datum, _imageDataIsHalfs);
	}

	inline unsigned short floatToImageDatum(float f)
	{
		return ImageDatum::fromFloat(f, _imageDataIsHalfs);
	}
};


extern CDewarpTool *GDewarpTool;    // Global from DewarpTool.cpp

//////////////////////////////////////////////////////////////////////

#endif // !defined(DEWARPTOOL_H)

