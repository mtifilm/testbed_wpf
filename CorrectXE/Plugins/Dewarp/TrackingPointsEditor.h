#ifndef TRACKING_POINTS_EDITOR
#define TRACKING_POINTS_EDITOR
//----------------------------------------------------------------------------

#include "machine.h"
#include "TrackingPoints.h"
//----------------------------------------------------------------------------

class CTrackingArray;
class CToolSystemInterface;
//----------------------------------------------------------------------------

#define DECOUPLE_MARKS_FROM_TRACKED_RANGE 0
//----------------------------------------------------------------------------

class CTrackingPointsEditor
{
public:

   CTrackingPointsEditor();

   ~CTrackingPointsEditor();

   void loadSystemAPI(CToolSystemInterface *newSystemAPI);
   void setIgnoringMarksFlag(bool newIgnoreFlag);

   void setTrackingBoxSizes(
            int newTrackBoxExtentX,     int newTrackBoxExtentY,
            int newSearchBoxExtentLeft, int newSearchBoxExtentRight,
            int newSearchBoxExtentUp,   int newSearchBoxExtentDown);

   void setTrackingArray(CTrackingArray *newArray,
                         bool preserveTrackingData=false);
   CTrackingArray *getTrackingArray();
   void resetTrackingArray();
   void deleteAllTrackingPoints();
   void deleteTrackingPointsRange(unsigned int, unsigned int);
   void invalidateAllTrackingData();
   bool doesTrackingPointNeedToBeRetracked(int nTag);
   bool isTrackingDataValidOverEntireRange();
   bool isFrameTrackingDataValid(int frameIndex);

   void notifyTrackingComplete();
   void notifyTrackingInvalid();
   void notifyRenderComplete();

   void setRange(int newStartFrame, int newStopFrame);
   void setRangeFromMarks();
   void getRange(int &startFrame, int &stopFrame);

// WHAT IS THIS "FINDING"? It doesn't return anything!!
   void findTrackedValue(int tag);
   void findTrackedValue(void);

// Returns for canXXMarkChange
// I HATE THIS SH*T - 0 (FALSE) Means you CAN change the marks!!!

#define TRK_IN_MARK_CAN_CHANGE                 0
#define TRK_IN_MARK_IS_DELETED                 1
#define TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL   2
#define TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL  3
   int  canInMarkChange();

#define TRK_OUT_MARK_CAN_CHANGE                0
#define TRK_OUT_MARK_IS_DELETED                1
#define TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL  2
#define TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL 3
   int  canOutMarkChange();

#define START_FRAME_CAN_CHANGE                 TRK_IN_MARK_CAN_CHANGE
#define START_FRAME_IS_DELETED                 TRK_IN_MARK_IS_DELETED
#define START_FRAME_IS_MOVED_INSIDE_INTERVAL   TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL
#define START_FRAME_IS_MOVED_OUTSIDE_INTERVAL  TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL
   int  checkStartFrameChange(int newStartFrame);

#define STOP_FRAME_CAN_CHANGE                  TRK_OUT_MARK_CAN_CHANGE
#define STOP_FRAME_IS_DELETED                  TRK_OUT_MARK_IS_DELETED
#define STOP_FRAME_IS_MOVED_INSIDE_INTERVAL    TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL
#define STOP_FRAME_IS_MOVED_OUTSIDE_INTERVAL   TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL
   int  checkStopFrameChange(int newStopFrame);

   int  getRangeBegin() {return startFrameIndex; }
   int  getRangeEnd() {return stopFrameIndex; }

// I REAAALLY hate this sh*t
   void setShift(bool);
   void setAlt(bool);
   void setCtrl(bool);
   void setKeyA(bool);
   void setKeyD(bool);
   void setKeyU(bool);

   void mouseDown();
   void mouseMove(int x, int y);
   void mouseUp  ();

   void drawTrackingPointsCurrentFrame(int defaultColor = SOLID_RED);
   void drawLineFromSelectedTrackingPointToMousePosition();
   void refreshTrackingPointsFrame();
   void resetLastSelected(void);
   void setUniqueSelection(int tpIndex);
   void unselectAllTrackingPoints();
   void selectAllTrackingPoints();

   SPROPERTY(int, lastSelectedTag, _lastSelectedTag, setLastSelectedTag, CTrackingPointsEditor);
   SPROPERTY(bool, displayTrackingPoints, _displayTrackingPoints, setDisplayTrackingPoints, CTrackingPointsEditor);
   CBHook rangeChange;    // Call on a range change
   CBHook TrackingPointStateChange; // Call when any state changes
   CBHook TrackingParameterChange;  // Call when points or range change
   CBHook NextSelectedTagIsANewTrackingPoint; // Call before setting lastSelectedTag for a new tracking point

private:
   int  trackBoxExtentX;
   int  trackBoxExtentY;
   int  searchBoxExtentLeft;
   int  searchBoxExtentRight;
   int  searchBoxExtentUp;
   int  searchBoxExtentDown;

   int  findTrackingPointInBox(DRECT &selbox);
   void unselectTrackingPointAllFrames(unsigned int selindx);
   void selectTrackingPointAllFrames(unsigned int);
   int  createTrackingPoint(double x, double y, bool sel);
   void translateSelectedTrackingPointsCurrentFrame(double delx, double dely);
   void deleteSelectedTrackingPointsAllFrames();

   // setters for properties
   void setLastSelectedTag(int newlastSelectedTag);
   void setDisplayTrackingPoints(bool newDisp);

   int  _lastSelectedTag;
   bool _displayTrackingPoints;

   CToolSystemInterface *systemAPI;

   CTrackingArray *trackingArray;

   int startFrameIndex;
   int stopFrameIndex;
   bool m_MouseDown;

   // mouse client coords
   int mouseXClient,
       mouseYClient;

   // mouse frame coords
   double mouseXFrame,
          mouseYFrame;

   // mouse deltas
   double deltaMouseXFrame,
          deltaMouseYFrame;

#define MOD_KEY_NONE  0
#define MOD_KEY_SHIFT 1
#define MOD_KEY_ALT   2
#define MOD_KEY_CTRL  4
   int modKeys;

   // when simultaneously panning and
   // dragging, let Displayer do refresh
   bool panEnabled;

#define MAJORSTATE_TRACK              0
#define MAJORSTATE_DRAG               1
   int majorState;

   // a hack flag
   bool ignoringMarks;

};

#endif
