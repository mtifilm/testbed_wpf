
#include "PaintAlgo.h"
#include "stdlib.h"
#include "MTImalloc.h"

CPaint::CPaint()
{
  uspOrigRefFrame = 0;
  uspOrigCurFrame = 0;

  iUVZero = -1;
}

CPaint::~CPaint()
{
  Free ();
}

void CPaint::setDimension (int newRow, int newCol, int newPitch, int newCom,
		EPixelComponents newPixelComponents, int newBitDepth)
{
  Free ();
  iNRow = newRow;
  iNCol = newCol;
  iPitch = newPitch;
  iNCom = newCom;

  iBytePerImage = iNRow * iPitch * iNCom * sizeof (MTI_UINT16);

  // the U and V values are normally centered about zero.  For data
  // packing, these values are shifted to fit in an unsigned value.
  iUVZero = -1;
  if (
	newPixelComponents == IF_PIXEL_COMPONENTS_YUV422 ||
	newPixelComponents == IF_PIXEL_COMPONENTS_YUV4224 ||
	newPixelComponents == IF_PIXEL_COMPONENTS_YUV444
     )
   {
    if (newBitDepth == 8)
     {
      iUVZero = 128;
     }
    else if (newBitDepth == 10)
     {
      iUVZero = 512;
     }
   }
}

int CPaint::setRefFrame (MTI_UINT16 *newRef)
{
  // if necessary allocate new storage
  if (uspOrigRefFrame == 0)
   {
    uspOrigRefFrame = (MTI_UINT16 *) malloc (iBytePerImage);
    if (uspOrigRefFrame == 0)
     {
      return -1;
     }
   }

  // copy over the original reference frame
  MTImemcpy ((void *)uspOrigRefFrame, (void *)newRef, iBytePerImage);

  return 0;
}

int CPaint::setCurFrame (MTI_UINT16 *newCur)
{
  // if necessary allocate new storage
  if (uspOrigCurFrame == 0)
   {
    uspOrigCurFrame = (MTI_UINT16 *) malloc (iBytePerImage);
    if (uspOrigCurFrame == 0)
     {
      return -1;
     }
   }

  // copy over the original current frame
  MTImemcpy ((void *)uspOrigCurFrame, (void *)newCur, iBytePerImage);

  return 0;
}

void CPaint::setOffset (int newOffsetRow, int newOffsetCol)
{
  iOffsetRow = newOffsetRow;
  iOffsetCol = newOffsetCol;
}

void CPaint::startPaint (CBrush *brush)
{
  bpBrush = brush;
}

void CPaint::pixelPaint (int newRow, int newCol, MTI_UINT16 *frame)
{
  if (uspOrigRefFrame == 0)
    return;

  MTI_UINT16 *uspDst = frame + ( ((newRow*iPitch) + newCol) * iNCom );
  for (int iCom = 0; iCom < iNCom; iCom++)
   {
    *uspDst++ = 0;
   }
}

void CPaint::endPaint ()
{
}

void CPaint::getOnionFrame (MTI_UINT16 *frame)
{
  if (uspOrigRefFrame == 0 || uspOrigCurFrame == 0)
    return;

  // run through the pixels reduce the intensity of the current frame
  for (int iRow = 0; iRow < iNRow; iRow++)
   {
    MTI_UINT16 *uspSrc = uspOrigCurFrame + (iRow * iPitch * iNCom);
    MTI_UINT16 *uspDst = frame + (iRow * iPitch * iNCom);

    if (iUVZero == -1)
     {
      // RGB data, make each component half as big
      for (int iColCom = 0; iColCom < iNCol*iNCom; iColCom++)
       {
        *uspDst++ = (*uspSrc++ >> 1);
       }
     }
    else
     {
      // YUV data, make each component half as big
      for (int iColCom = 0; iColCom < iNCol*iNCom; iColCom+=iNCom)
       {
        // the Y value
        *uspDst++ = (*uspSrc++ >> 1);

        // the U,V values
        *uspDst++ = ( (*uspSrc++ + iUVZero) >> 1 );
        *uspDst++ = ( (*uspSrc++ + iUVZero) >> 1 );
       }
     }
   }

  int iRowStart, iRowStop, iColStart, iColStop;

  if (iOffsetRow > 0)
   {
    iRowStart = 0;
    iRowStop = iNRow - iOffsetRow;
   }
  else
   {
    iRowStart = -iOffsetRow;
    iRowStop = iNRow;
   }
  
  if (iOffsetCol > 0)
   {
    iColStart = 0;
    iColStop = iNCol - iOffsetCol;
   }
  else
   {
    iColStart = -iOffsetCol;
    iColStop = iNCol;
   }
  
  // run through the pixels add in the reference frame
  for (int iRow = iRowStart; iRow < iRowStop; iRow++)
   {
    MTI_UINT16 *uspSrc = uspOrigRefFrame +
		( ((iRow+iOffsetRow) * iPitch + iOffsetCol) * iNCom);
    MTI_UINT16 *uspDst = frame + (iRow * iPitch * iNCom);

    if (iUVZero == -1)
     {
      // RGB data, make each component half as big
      for (int iColCom = iColStart * iNCom; iColCom < iColStop*iNCom; iColCom++)
       {
        *uspDst++ += (*uspSrc++ >> 1);
       }
     }
    else
     {
      // YUV data, make each component half as big
      for (int iColCom = iColStart * iNCom; iColCom < iColStop*iNCom;
		iColCom+=iNCom)
       {
        // the Y value
        *uspDst++ += (*uspSrc++ >> 1);

        // the U,V values
        *uspDst++ += ( (*uspSrc++ + iUVZero) >> 1 );
        *uspDst++ += ( (*uspSrc++ + iUVZero) >> 1 );
       }
     }
   }
  
}

void CPaint::Free ()
{
  free (uspOrigRefFrame);
  uspOrigRefFrame = 0;
  free (uspOrigCurFrame);
  uspOrigCurFrame = 0;
}

