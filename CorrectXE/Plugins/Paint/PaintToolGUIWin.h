// ---------------------------------------------------------------------------

#ifndef PaintToolGUIWinH
#define PaintToolGUIWinH
// ---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include "MTITrackBar.h"
#include <Buttons.hpp>
#include "ExecStatusBarUnit.h"
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "VTimeCodeEdit.h"
#include "BrushAlgo.h"
#include <Menus.hpp>
#include <Graphics.hpp>
#include "VDoubleEdit.h"
#include "cspin.h"
#include <Dialogs.hpp>

#include "PaintTool.h"
#include "ToolSystemInterface.h"
#include "ExecStatusBarUnit.h"
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include "ColorPanel.h"
#include <VclTee.TeeGDIPlus.hpp>
#include "TrackEditFrameUnit.h"
#include <Vcl.Imaging.pngimage.hpp>
#include "ColorLabel.h"

#include <gl\gl.h>
#include <gl\glu.h>
#include <string>
using std::string;

#define NUM_BRUSH 10

/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreatePaintToolGUI(void);
extern void DestroyPaintToolGUI(void);
extern bool ShowPaintToolGUI(void);
extern bool HidePaintToolGUIQuery();
extern bool HidePaintToolGUI(void);
// extern void NewClip();   // Nobody calls this
extern bool IsToolVisible(void);
extern void SetAltSourceClipInfo(const string &newAltSourceClipName, int newAltSourceClipInfo);

class CConvert;

// ---------------------------------------------------------------------------
class TPaintToolForm : public TMTIForm
{
__published: // IDE-managed Components
   TLabel *Label1;
   TGroupBox *ReferenceFrameBox;
   TGroupBox *OriginalFrameBox;
   TButton *AcceptButton;
   TButton *RejectButton;
   TSpeedButton *ThruButton;
   TSpeedButton *EraseButton;
   TSpeedButton *PositionImportButton;
   TSpeedButton *ColorSkinButton;
   TSpeedButton *DiffSkinButton;
   TSpeedButton *OrigMixSkinButton;
   TSpeedButton *OrigDiffSkinButton;
   TButton *InvertButton;
   TSpeedButton *QuickAlignButton;
   ////        TTBDock *TBDockMask;
   TTimer *RotateStretchTimer;
   TLabel *XLabel;
   TLabel *YLabel;
   TEdit *XEditBox;
   TEdit *YEditBox;
   TEdit *HEditBox;
   TEdit *VEditBox;
   TLabel *HLabel;
   TLabel *VLabel;
   TEdit *SkewEditBox;
   TImage *SkewImage;
   TImage *RotationImage;
   TEdit *RotationEditBox;
   TTrackBar *MixingTrackBar;
   TLabel *RefLabel;
   TLabel *TgtLabel;
   TEdit *ReferenceFrameOffsetEdit;
   TSpeedButton *ReferenceFrameOffsetButton;
   TSpeedButton *ReferenceFrameLockButton;
   TPaintBox *OrigProxyPaintBox;
   TEdit *ReferenceFrameTimecodeEdit;
   TGroupBox *PendingStrokesBox;
   TButton *UndoLastButton;
   TButton *UndoThruSelectButton;
   TListBox *PendingStrokesListBox;
   TLabel *CloneXLabel;
   TEdit *CloneXEditBox;
   TLabel *CloneYLabel;
   TEdit *CloneYEditBox;
   TLabel *CloneDelXLabel;
   TEdit *CloneDelXEditBox;
   TLabel *CloneDelYLabel;
   TEdit *CloneDelYEditBox;
   TSpeedButton *SetClonePointSpeedButton;
   TSpeedButton *ClonePointOffsetSpeedButton;
   TSpeedButton *ClonePointLockSpeedButton;
   TLabel *CloneModeLabel;
   TColorDialog *ColorDialog1;
   TGroupBox *ColorPickerBox;
   TPaintBox *ColorPalettePaintBox;
   TPaintBox *HueSaturationPaintBox;
   TPaintBox *ValueMeterPaintBox;
   TSpeedButton *DropperSpeedButton;
   TEdit *RYEditBox;
   TEdit *GUEditBox;
   TEdit *BVEditBox;
   TLabel *RYLabel;
   TLabel *GULabel;
   TLabel *BVLabel;
   TSpeedButton *BucketFillSpeedButton;
   TTrackBar *ToleranceTrackBar;
   TLabel *TolHeading;
   TLabel *ToleranceLabel;
   TComboBox *AvgSizeComboBox;
   TCheckBox *AutoAcceptCheckBox;
   TColorPanel *PaintControlPanel;
   TPanel *ToolColorPanel;
   TLabel *Label2;
   TGroupBox *BrushGroupBox;
   TLabel *RadiusLabel;
   TLabel *OpacityLabel;
   TLabel *BlendLabel;
   TLabel *AspectLabel;
   TLabel *AngleLabel;
   TLabel *RadiusValueLabel;
   TLabel *OpacityValueLabel;
   TLabel *BlendValueLabel;
   TLabel *AspectValueLabel;
   TLabel *AngleValueLabel;
   TPaintBox *BrushPreviewPaintBox;
   TTrackBar *RadiusTrackBar;
   TTrackBar *OpacityTrackBar;
   TTrackBar *BlendTrackBar;
   TTrackBar *AspectTrackBar;
   TTrackBar *AngleTrackBar;
   TPaintBox *BrushSelectPaintBox_01;
   TPaintBox *BrushSelectPaintBox_02;
   TPaintBox *BrushSelectPaintBox_03;
   TPaintBox *BrushSelectPaintBox_04;
   TPaintBox *BrushSelectPaintBox_05;
   TPaintBox *BrushSelectPaintBox_06;
   TPaintBox *BrushSelectPaintBox_07;
   TPaintBox *BrushSelectPaintBox_08;
   TPaintBox *BrushSelectPaintBox_09;
   TPaintBox *BrushSelectPaintBox_10;
   TGroupBox *PaintModeGroupBox;
   TSpeedButton *RevealButton;
   TSpeedButton *CloneButton;
   TSpeedButton *ColorButton;
   TSpeedButton *PlayStrokeSpeedButton;
   TSpeedButton *RangeStrokeSpeedButton;
   TSpeedButton *MaskButton;
   TSpeedButton *StopSpeedButton;
   TSpeedButton *DensitySpeedButton;
   TSpeedButton *GrainSpeedButton;
   TColorPanel *ResetDensityPanel;
   TColorPanel *ResetGrainPanel;
   TSpeedButton *TrackingButton;
   TGroupBox *TrackingGroupBox;
   TGroupBox *TrackingPointsGroupBox;
   TBitBtn *TrackingDeletePointButton;
   TBitBtn *TrackingTrackButton;
   TBitBtn *TrackingPrevPointButton;
   TBitBtn *TrackingNextPointButton;
   TPanel *TrackingCheckGroupBox;
   TCheckBox *TrackingShowPointCBox;
   TGroupBox *xxxTrackingSizeGroupBox;
   TLabel *TrackingLabel1;
   TLabel *TrackingLabel2;
   TTrackBar *xxxTrackingTrackBar;
   TEdit *xxxTrackingEdit;
   TGroupBox *xxxTrackingMotionGroupBox;
   TLabel *Label4;
   TLabel *Label5;
   TTrackBar *xxxSearchTrackBar;
   TEdit *xxxMotionEdit;
   TPanel *TrackingStatusPanel;
   TExecStatusBarFrame *ExecStatusBar;
   TChart *HorizontalChart;
   TLineSeries *LineSeries5;
   TLineSeries *LineSeries6;
   TChart *VerticalChart;
   TLineSeries *LineSeries7;
   TLineSeries *LineSeries8;
   TPanel *ChartPanel;
   TSpeedButton *TrackAlignButton;
   TPanel *MixingTrackBarPanel;
   TPanel *TrackingDataAvailableMessagePanel;
   TImage *TrackDataOkImage;
   TImage *TrackDataCautionImage;
   TImage *TrackDataInvalidImage;
   TImage *TrackDataDisabledImage;
   TTimer *TrackingPointsChangedTimer;
   TSpeedButton *OrigValuesButton;
   TTrackBar *OrigMixingTrackBar;
   TLabel *OrigTimecodeLabel;
   TLabel *OrigLabel;
   TLabel *CurLabel;
   TSpeedButton *CompareSpeedButton;
   TSpeedButton *RndSpeedButton;
   TSpeedButton *SqSpeedButton;
   TCheckBox *CurTransformCheckBox;
   TCheckBox *TrackedMacroCheckBox;
   TLabel *ShowingLabel;
   TLabel *ViewLabel;
   TGroupBox *TrackingBoxSizeGroupBox;
   TLabel *TrackingBoxSizePlusLabel;
   TLabel *TrackingBoxSizeMinusLabel;
   TPanel *TrackingBoxSizeClippingPanel;
   TTrackEditFrame *TrackingBoxSizeTrackEdit;
   TGroupBox *TrackingMotionSearchGroupBox;
   TLabel *TrackingMotionSearchMinusLabel;
   TLabel *TrackingMotionSearchPlusLabel;
   TPanel *TrackingMotionSearchClippingPanel;
   TTrackEditFrame *TrackingMotionSearchTrackEdit;
   TPanel *VLabelPanel;
   TPanel *HLabelPanel;
   TImage *RangeWarningImage;
   TColorPanel *RangeWarningPanel;
   TPanel *ProxyPaintBoxPanel;
   TPaintBox *ProxyPaintBox;
   TPanel *ColorPalettePaintBoxPanel;
   TColorPanel *ColorPanel1;
   TColorPanel *ColorPanel2;
   TPanel *DummyFocusPanel;
   TPanel *GrainPresetsPanel;
   TSpeedButton *GrainPreset1Button;
   TSpeedButton *GrainPreset2Button;
   TRadioButton *GrainPresetsProjectRadioButton;
   TRadioButton *GrainPresetsClipRadioButton;
   TPanel *GrainUsePresetsPanel;
   TCheckBox *GrainUsePresetsCheckBox;
   TCheckBox *ReverseCheckBox;
   TLabel *Label3;
   TColorPanel *RedAltClipSourcePanel;
   TSpeedButton *AlternateClipButton;
   TGroupBox *AltClipFrameBox;
   TSpeedButton *AltClipCompareButton;
   TSpeedButton *AltClipMixButton;
   TSpeedButton *AltClipDiffButton;
   TSpeedButton *AltClipSelectClipButton;
   TSpeedButton *AltClipClearClipButton;
   TLabel *AltClipSrcLabel;
   TLabel *AltClipTgtLabel;
   TColorPanel *AltClipSourceRedPanel;
   TEdit *AltClipFrameOffsetEdit;
   TEdit *AltClipFrameTimecodeEdit;
   TPanel *AltClipMixingTrackBarPanel;
   TTrackBar *AltClipMixingTrackBar;
   TColorPanel *AltClipOutOfRangePanel;
   TImage *AltClipOutOfRangeImage;
   TPanel *AltClipProxyPanel;
   TPaintBox *AltClipProxyPaintBox;
   TColorLabel *AltSourceClipNameLabel;
   TSpeedButton *AltClipSyncTimecodeButton;
   TSpeedButton *AltClipSyncFrameIndexButton;
   TSpeedButton *AltClipSyncMarkInButton;
   TPanel *OrigProxyPanel;
   TTrackBar *AltClipDiffThresholdTrackBar;
   TPanel *AltClipDiffThresholdTrackBarPanel;
   TTimer *AltClipDiffThresholdTimer;
   TColorLabel *AltClipDiffTresholdValueLabel;

   void __fastcall RadiusTrackBarChange(TObject *Sender);
   void __fastcall OpacityTrackBarChange(TObject *Sender);
   void __fastcall BlendTrackBarChange(TObject *Sender);
   void __fastcall AspectTrackBarChange(TObject *Sender);
   void __fastcall AngleTrackBarChange(TObject *Sender);
   void __fastcall BrushSelectPaintBox_01Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_02Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_03Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_04Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_05Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_06Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_07Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_08Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_09Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_10Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_11Click(TObject *Sender);
   void __fastcall BrushSelectPaintBox_12Click(TObject *Sender);
   void __fastcall ThruButtonClick(TObject *Sender);
   void __fastcall EraseButtonClick(TObject *Sender);
   void __fastcall PositionImportButtonClick(TObject *Sender);
   void __fastcall RevealButtonClick(TObject *Sender);
   void __fastcall AcceptButtonClick(TObject *Sender);
   void __fastcall RejectButtonClick(TObject *Sender);
   void __fastcall RadiusTrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall OpacityTrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall BlendTrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall AspectTrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall AngleTrackBarKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall AlwaysOnTopMenuItemClick(TObject *Sender);
   void __fastcall PaintPopupMenuPopup(TObject *Sender);
   void __fastcall RadiusTrackBarKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall OpacityTrackBarKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall BlendTrackBarKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall AspectTrackBarKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall AngleTrackBarKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall MaskButtonClick(TObject *Sender);
   void __fastcall MaskToolButtonClick(TObject *Sender);
   void __fastcall MaskBorderMenuItemClick(TObject *Sender);
   void __fastcall InvDelClrButtonClick(TObject *Sender);
   void __fastcall ColorSkinButtonClick(TObject *Sender);
   void __fastcall DiffSkinButtonClick(TObject *Sender);
   void __fastcall RotateStretchTimerTimer(TObject *Sender);
   void __fastcall AcceptTransformSpeedButtonClick(TObject *Sender);
   void __fastcall RejectTransformSpeedButtonClick(TObject *Sender);
   void __fastcall EditKeyPress(TObject *Sender, char &Key);
   void __fastcall ReferenceFrameTimecodeEditExit(TObject *Sender);
   void __fastcall ReferenceFrameOffsetEditExit(TObject *Sender);
   void __fastcall ReferenceFrameLockButtonClick(TObject *Sender);
   void __fastcall ReferenceFrameOffsetButtonClick(TObject *Sender);
   void __fastcall MixingTrackBarChange(TObject *Sender);
   void __fastcall RotationEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall RotationEditBoxExit(TObject *Sender);
   void __fastcall SkewEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall SkewEditBoxExit(TObject *Sender);
   void __fastcall HEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall HEditBoxExit(TObject *Sender);
   void __fastcall VEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall VEditBoxExit(TObject *Sender);
   void __fastcall XEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall XEditBoxExit(TObject *Sender);
   void __fastcall YEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall YEditBoxExit(TObject *Sender);
   void __fastcall UndoLastButtonClick(TObject *Sender);
   void __fastcall UndoThruSelectButtonClick(TObject *Sender);
   void __fastcall CloneButtonClick(TObject *Sender);
   void __fastcall ColorButtonClick(TObject *Sender);
   void __fastcall ClonePointOffsetSpeedButtonClick(TObject *Sender);
   void __fastcall ClonePointLockSpeedButtonClick(TObject *Sender);
   void __fastcall SetClonePointSpeedButtonClick(TObject *Sender);
   void __fastcall CloneXEditBoxExit(TObject *Sender);
   void __fastcall CloneXEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall CloneYEditBoxExit(TObject *Sender);
   void __fastcall CloneYEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall ColorPalettePaintBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
   void __fastcall ColorPalettePaintBoxClick(TObject *Sender);
   void __fastcall CloneDelXEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall CloneDelXEditBoxExit(TObject *Sender);
   void __fastcall CloneDelYEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall CloneDelYEditBoxExit(TObject *Sender);
   void __fastcall HueSaturationPaintBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
   void __fastcall HueSaturationPaintBoxClick(TObject *Sender);
   void __fastcall ValueMeterMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
   void __fastcall ValueMeterMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
   void __fastcall ValueMeterMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
   void __fastcall DropperSpeedButtonClick(TObject *Sender);
   void __fastcall RYEditBoxExit(TObject *Sender);
   void __fastcall RYEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall GUEditBoxExit(TObject *Sender);
   void __fastcall GUEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall BVEditBoxExit(TObject *Sender);
   void __fastcall BVEditBoxKeyPress(TObject *Sender, char &Key);
   void __fastcall ToleranceTrackBarChange(TObject *Sender);
   void __fastcall ColorPickSampleSizeChange(TObject *Sender);
   void __fastcall BucketFillSpeedButtonClick(TObject *Sender);
   void __fastcall AutoAcceptCheckBoxClick(TObject *Sender);
   // void __fastcall RecordStrokeSpeedButtonClick(TObject *Sender);
   void __fastcall PlayStrokeSpeedButtonClick(TObject *Sender);
   // void __fastcall ClearStrokeButtonClick(TObject *Sender);
   void __fastcall RangeStrokeSpeedButtonClick(TObject *Sender);
   void __fastcall PaintBoxPaint(TObject *Sender);
   void __fastcall ClearButtonClick(TObject *Sender);
   void __fastcall StopSpeedButtonClick(TObject *Sender);
   void __fastcall InvertButtonClick(TObject *Sender);
   void __fastcall QuickAlignButtonClick(TObject *Sender);
   // void __fastcall DensityTrackBarChange(TObject *Sender);
   void __fastcall DensitySpeedButtonClick(TObject *Sender);
   void __fastcall GrainSpeedButtonClick(TObject *Sender);
   void __fastcall ResetDensityPanelClick(TObject *Sender);
   void __fastcall ResetGrainPanelClick(TObject *Sender);
   void __fastcall MixingTrackBarEnter(TObject *Sender);
   void __fastcall RadiusTrackBarEnter(TObject *Sender);
   void __fastcall OpacityTrackBarEnter(TObject *Sender);
   void __fastcall BlendTrackBarEnter(TObject *Sender);
   void __fastcall AspectTrackBarEnter(TObject *Sender);
   void __fastcall AngleTrackBarEnter(TObject *Sender);
   void __fastcall EditBoxEnter(TObject *Sender);
   void __fastcall ToleranceTrackBarEnter(TObject *Sender);
   void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
   void __fastcall TrackingDeletePointButtonClick(TObject *Sender);
   void __fastcall TrackingPrevPointButtonClick(TObject *Sender);
   void __fastcall TrackingNextPointButtonClick(TObject *Sender);
   void __fastcall TrackingShowPointCBoxClick(TObject *Sender);
   void __fastcall TrackingTrackButtonClick(TObject *Sender);
   void __fastcall TrackingButtonClick(TObject *Sender);
   void __fastcall TrackAlignButtonClick(TObject *Sender);
   void __fastcall ReferenceFrameTimecodeEditChange(TObject *Sender);
   void __fastcall TrackingPointsChangedTimerTimer(TObject *Sender);
   void __fastcall OrigValuesButtonClick(TObject *Sender);
   void __fastcall OrigMixButtonClick(TObject *Sender);
   void __fastcall OrigDiffSkinButtonClick(TObject *Sender);
   void __fastcall OrigMixingTrackBarEnter(TObject *Sender);
   void __fastcall OrigMixingTrackBarChange(TObject *Sender);
   void __fastcall CompareSpeedButtonClick(TObject *Sender);
   void __fastcall RndSpeedButtonClick(TObject *Sender);
   void __fastcall SqSpeedButtonClick(TObject *Sender);
   void __fastcall GrainUsePresetsCheckBoxClick(TObject *Sender);
   void __fastcall GrainPresetsButtonClick(TObject *Sender);
   void __fastcall AlternateClipButtonClick(TObject *Sender);
   void __fastcall AltClipSelectClipButtonClick(TObject *Sender);
   void __fastcall AltClipClearClipButtonClick(TObject *Sender);
   void __fastcall AltClipSyncTimecodeButtonClick(TObject *Sender);
   void __fastcall AltClipSyncFrameIndexButtonClick(TObject *Sender);
   void __fastcall AltClipSyncMarkInButtonClick(TObject *Sender);
   void __fastcall AltClipFrameTimecodeEditChange(TObject *Sender);
   void __fastcall AltClipFrameTimecodeEditExit(TObject *Sender);
   void __fastcall AltClipFrameOffsetEditExit(TObject *Sender);
   void __fastcall AltClipCompareButtonClick(TObject *Sender);
   void __fastcall AltClipDiffThresholdTrackBarChange(TObject *Sender);
   void __fastcall AltClipDiffThresholdTimerTimer(TObject *Sender);
   void __fastcall AltClipMixingTrackBarChange(TObject *Sender);
   void __fastcall AltClipMixingTrackBarEnter(TObject *Sender);
   void __fastcall AltClipDiffButtonClick(TObject *Sender);
   void __fastcall AltClipMixButtonClick(TObject *Sender);

private: // User declarations

         Graphics::TBitmap *bitmap_big;
   Graphics::TBitmap *bitmap[NUM_BRUSH];

   CBrush *constBrush;
   CBrush *brush[NUM_BRUSH];

   Graphics::TBitmap *importProxyBitmap;

   TPaintBox *importProxy;

   int importProxyWdth, importProxyHght;

   Graphics::TBitmap *proxyBitmap;

   Graphics::TBitmap *origProxyBitmap;

#define BLACK 0x00000000
#define WHITE 0x00ffffff

#define COL_ROWS 4
#define COL_COLS 7
#define COL_WDTH 20
#define COL_HGHT 20
#define COL_BETW  4

   Graphics::TBitmap *palFrm;

   int componentCount;
   int maxComponentValue;

   int palMouseX;
   int palMouseY;
   int palSelectedRow;
   int palSelectedCol;

   Graphics::TBitmap *palCol[COL_ROWS][COL_COLS];

   float redCol[COL_ROWS][COL_COLS];
   float grnCol[COL_ROWS][COL_COLS];
   float bluCol[COL_ROWS][COL_COLS];
#define INI_VAL 0.75
   float valCol[COL_ROWS][COL_COLS];

   string redStr[COL_ROWS][COL_COLS];
   string grnStr[COL_ROWS][COL_COLS];
   string bluStr[COL_ROWS][COL_COLS];
   string valStr[COL_ROWS][COL_COLS];

   int hueMouseX;
   int hueMouseY;

   Graphics::TBitmap *hueSat;

   float colorHue;
   float colorSat;
   float colorVal;

   int valY;
   int valWdth;
   int valHght;
   int valMouseX;
   int valMouseY;
   bool adjLevel;
#define MWD  7
#define MHT 13

   Graphics::TBitmap *valArrow;
   Graphics::TBitmap *valBlank;
   Graphics::TBitmap *valBlklin;
   Graphics::TBitmap *valMeter;

   int currentBrush;
   int previousBrush;
#define BRUSH_MODE_THRU   1
#define BRUSH_MODE_ERASE  2
#define BRUSH_MODE_ORIG   3
   int brushMode;

   // these are settling timers for
   // the corresponding trackbars to
   // affect the density & tolerance
   int lastTime;
   int mainTime;
   int loadTimer;
   int densityTimer;
   int toleranceTimer;

   int localImportTrackBarPos;
   int localImportFrame;
   int localImportFrameOffset;

   double localOnionSkinTargetWgt = 0.5;
   double localOrigOnionSkinTargetWgt = 0.5;
   double localAltClipOnionSkinTargetWgt = 0.5;

   double localRotation;
   double localStretchX;
   double localStretchY;
   double localSkew;
   double localOffsetX;
   double localOffsetY;
#define ONIONSKIN_MODE_OFF    0
#define ONIONSKIN_MODE_COLOR  1
#define ONIONSKIN_MODE_POSNEG (2+4)
   int localOnionSkinMode;
   int localOrigOnionSkinMode;
   int localAltClipOnionSkinMode;
   int localCloneX;
   int localCloneY;
   int localCloneDelX;
   int localCloneDelY;
   bool localStrokeRelative;
   float localRGB[3];
   int localColorPickSampleSize;
   int localFillTolerance;
   int localDensityStrength;
   int localGrainStrength;

   string _localImportFrameClipName;

   int _localImportFrameClipOffset = 0;

#define TRKVLD_INVALID_STATE                            -1
#define TRKVLD_NEED_TO_RETRACK                           0
#define TRKVLD_TRACK_DATA_IS_VALID_EXCEPT_FOR_REF_FRAME  1
#define TRKVLD_ALL_TRACK_DATA_IS_VALID                   2

   int oldTrackValidState;

#define TRKBTN_INVALID_STATE                            -1
#define TRKBTN_DISABLED_STATE                            0
#define TRKBTN_ENABLED_STATE                             1
   int oldTrackingButtonEnabledState;

   int _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;

   bool InhibitTrackAlignButtonClick; // HACK to cut off recursion
   bool InhibitTrackingButtonClick; // HACK to cut off recursion

   bool stupidFrickenHackToNotTryToFocusOnTheMainWindow = true;

   void LoseTheFocus();

   void ChangeDisplay();
   void DrawPreviewDisplay();
   void DrawSmallDisplay(int);
   void DrawDisplay();
   void ConvertHueSat2RGB(float hue, float sat, float *r = NULL, float *g = NULL, float *b = NULL);
   MTI_UINT32 AdjustColorValue(float r, float g, float b, float val);
   void BitmapDrawLine(Graphics::TBitmap *, unsigned int col, int x1, int y1, int x2, int y2);
   MTI_UINT32 BitmapGetBoxRGB(Graphics::TBitmap *);
   void BitmapDrawBox(Graphics::TBitmap *, unsigned int col, bool sel);
   void BitmapSelectBox(Graphics::TBitmap *, bool);
   void ColorPaletteDrawFrame();
   void ColorPaletteDrawBox(int row, int col);
   void ColorPaletteSelectBox(int row, int col, bool sel);
   int HueSaturationGetRGB(int x, int y, float *red = NULL, float *grn = NULL, float *blu = NULL);
   void ValueMeterSetLevel(float v);
   void InitColorGUI();

   string BrowseForSourceClip();

   void CockTheDensityTimer();

   bool ToInteger(AnsiString, int *);
   bool ToDouble(AnsiString, double *);

public: // User declarations
   __fastcall TPaintToolForm(TComponent* Owner);

   bool doesFormUseOwnWindow()
   {
      return false;
   };

   void formCreate();
   void formShow();
   void formDestroy();
   void formPaint();
   bool formCloseQuery();
   bool formKeyDown(WORD &Key, TShiftState Shift);
   bool formKeyUp(WORD &Key, TShiftState Shift);
   void formMouseMove(TShiftState Shift, int X, int Y);
   bool HandleToolEvent(const FosterParentEventInfo &eventInfo);

   void setMaskState(bool);
   void setTrackingState(bool);
   void setMouseWheelState(int);
   void setMajorState(int);
   void setAlignState(int);
   void AllocateBrushes();
   void FreeBrushes();
   void LoadBrush();
   void RecalculateBrush();
   void SetCurrentBrush(int);
   int GetCurrentBrushIndex();
   void ResetCurrentBrush();
   void ResetRadiusTrackBar();
   void ChangeRadius(float, int);
   void ChangeOpacity(int);
   void ChangeBlend(int);
   void ChangeAspect(int);
   void ChangeAngle(int);
   void ToggleEllipseRect();
   void AdjustBrushSizesForNewClip();
   void SelectBrushBitmap(int, bool);
   void InitGUIBrushPanel();
   void LoadBrushBitmap(int);
   void LoadActiveBitmap(int);
   void LoadPreviewBitmap();
   void LoadBrushBitmaps();
   void UpdateGUIFromBrush();
   CBrush **GetBrushes();
   CBrush *GetCurrentBrush();
   CBrush *GetBrush(int);
   void SetOnionSkinMode(int);
   int GetOnionSkinMode();
   void SetOrigOnionSkinMode(int);
   int GetOrigOnionSkinMode();
   void SetAltClipOnionSkinMode(int);
   int GetAltClipOnionSkinMode();
   void SetBrushMode(int);
   int GetBrushMode();
   void SetCloneBrushRelative(bool);
   bool CloneBrushIsRelative();
   void ClearBrushThresholds();
   void UpdateStateOfAcceptRejectUndoAndMacroButtons(bool);
   void UpdateStateOfTrackAlignButton(bool);
   void ShowRejectMessage();
   void ShowAcceptMessage();
   void SetPickedColor(float *);
   int GetFillTolerance();
   void RangeMarksErrorDialog();
   int ConvertImportFrameEditTextToFrameIndex();
   int ConvertAltClipFrameEditTextToFrameIndex();

   void SetProxy(TPaintBox *, Graphics::TBitmap *);
   void DrawProxy(unsigned int *);

   void UpdateImportFrameMode(int);
   void UpdateImportFrameTimecode(int);
   void UpdateTargetFrameTimecode(int);
   void UpdateImportFrameOffset(int);
   void UpdateOnionSkinTargetWgt(double);
   void UpdateOrigOnionSkinTargetWgt(double);
   void UpdateAltClipOnionSkinTargetWgt(double tgtwgt);
   void UpdateRotation(double);
   void UpdateSkew(double);
   void UpdateHStretch(double);
   void UpdateVStretch(double);
   void UpdateXOffset(double);
   void UpdateYOffset(double);
   void UpdateTransform(double rot, double strx, double stry, double skew, double offx, double offy);
   void UpdateCloneX(int);
   void UpdateCloneY(int);
   void UpdateCloneDelX(int);
   void UpdateCloneDelY(int);
   void UpdateCloneMessage(int);
   void UpdateRY(int);
   void UpdateGU(int);
   void UpdateBV(int);
   void UpdateRGB();
   void UpdateSelectedBoxColor();
   void UpdateColorMessage(int);
   void UpdateColorPickSampleSize();
   void UpdateFillTolerance();
   void UpdateDensityStrength(int);
   void UpdateGrainStrength(int);

   void InitImportMode();

   bool DensityEstablished();

   void LoadImportFrameMode();
   void LoadImportFrameTimecode();
   void LoadImportFrameOffset();
   void LoadAltClipFrameTimecode();
   void LoadAltClipFrameOffset();
   void LoadRotation();
   void LoadSkew();
   void LoadStretchX();
   void LoadStretchY();
   void LoadOffsetX();
   void LoadOffsetY();
   void LoadCloneX();
   void LoadCloneY();
   void LoadCloneDelX();
   void LoadCloneDelY();
   void LoadRY();
   void LoadGU();
   void LoadBV();

   int GetLocalImportFrame();

   void ClearLoadTimer();
   void SetLoadTimer();

   void ClearStrokeStack();
   void SetStrokeEntry(int, int);

   int ReadPaletteColors();
   int WritePaletteColors();

   // tracking crap
   void makeTrackPointsVisible();
   void toggleTrackPointVisibility();
   void UpdateTrackingPointButtons();
   void UpdateTrackingDataValidLight();
   void UpdateTrackingCharts();
   int LastSelectedTag(void);

   void UpdateGrainPresetButtons();

   DEFINE_CBHOOK(TrackingPointsChanged, TPaintToolForm);
   DEFINE_CBHOOK(TrackingTrackBarChanged, TPaintToolForm);

   void InitTrackingBoxSizeTrackBar(int min, int max, int pos);
   void InitTrackingMotionSearchTrackBar(int min, int max, int pos);

   // these were supposed to be CBHooks
   void selectedTagChanged(void *Sender);
   void MarksHaveChanged(void *Sender);

   void HideShowCharts(bool bShowCharts);
   double FindExtentXForOnePoint(int iTag);
   double FindExtentYForOnePoint(int iTag);
   void DrawChartsTag(int Tag);
   void GetOutOfTrackAlignMode();
   // end of tracking crap

   // grain crap
   int _grainPresetOverrideCurrentValue;

   DEFINE_CBHOOK(GrainPresetsChanged, TPaintToolForm);

   void SetNewAlternateSourceClip(const string &newAlternateSourceClipName, int newAlternateSourceClipOffset =
         InvalidAlternateSourceClipOffsetValue);
   void StartRevealingFromAlternateSourceClip();
   void StopRevealingFromAlternateSourceClip();
   bool _savedDisplayerImportFrameStateFlag = false;
   bool _doubleAltStateSavePreventerHackFlag = false;
   void SaveAndClearImportFromAltSourceClipState();
   void RestoreImportFromAltSourceClipState();
   void EnterAlternateClipSourceMode();

   bool InhibitGuiUpdateHackFlag; // HACK to not update while recycling
};

// ---------------------------------------------------------------------------
extern PACKAGE TPaintToolForm *PaintToolForm;
extern MTI_UINT32 *PaintFeatureTable[];
// ---------------------------------------------------------------------------
#endif
