
/*
	Here is a description of the public functions in CPaint:

	setDimension - is use to tell the paint algorithm the height,
		width, pitch, and number of components in the image

	setRefFrame - causes the paint algorithm to make its own internal
		copy of the reference frame

	setCurFrame - causes the paint algorithm to make its own internal
		copy of the current frame.  Useful only for subsequent
		use by onion skin function.

	setOffset - causes the paint algorithm to start using new values
		for the horizontal and vertical offsets.  The paint function
		replaces the output pixel at (R,C) with the reference
		pixel at (R+OffsetRow, C+OffsetCol).

	startPaint - tells the algorithm to begin painting

	pixelPaint - tells the algorithm to paint the indicated pixel

	stopPaint  - tells the algorithm to stop painting

	getOnionFrame - combines the reference frame and the current
		frame to create an onion skin frame.
*/

#include "ImageInfo.h"
#include "BrushAlgo.h"


class CPaint
{
public:
  CPaint();
  ~CPaint();

  void setDimension (int newRow, int newCol, int newPitch, int newCom, 
		EPixelComponents newPixelComponents, int newBitDepth);
  int setRefFrame (MTI_UINT16 *newRef);
  int setCurFrame (MTI_UINT16 *newCur);
  void setOffset (int newOffsetRow, int newOffsetCol);

  void startPaint (CBrush *brush);
  void pixelPaint (int newRow, int newCol, MTI_UINT16 *frame);
  void endPaint ();

  void getOnionFrame (MTI_UINT16 *frame);

private:
  int
    iNRow,
    iNCol,
    iPitch,
    iNCom;

  int
    iOffsetRow,
    iOffsetCol;

  int
    iUVZero,
    iBytePerImage;

  MTI_UINT16
    *uspOrigRefFrame,	// a copy of the original reference frame
    *uspOrigCurFrame;   // a copy of the original current frame

  CBrush
    *bpBrush;

  void Free ();
};
