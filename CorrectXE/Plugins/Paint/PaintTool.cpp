// Paint.cpp: implementation of the CPaintTool class.

#include "PaintTool.h"
#include "NavigatorTool.h"
#include "BrushAlgo.h"
#include "ClipAPI.h"
#include "Displayer.h"
#include "JobManager.h"
#include "MathFunctions.h"
#include "MTIDialogs.h"
#include "MTImalloc.h" // for FlyingSpaghettiBuffer hack
#include "MTIsleep.h"
#include "PixelRegions.h"
#include "tarray.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolFrameBuffer.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingBoxManager.h"

#ifdef __BORLANDC__
#include "PaintToolGUIWin.h"
#else
#include "GuiSgi/PaintGuiSgi.h"
#endif

#define NEW_BRUSHES

static MTI_UINT16 PaintToolNumber = PAINT_TOOL_NUMBER;

// -------------------------------------------------------------------------
// PaintTool Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem PaintToolDefaultConfigItems[] =
{
   // PaintTool Tool Command             Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------

   // Navigator Commands with CTRL-SHIFT

   { NAV_CMD_JOG_FWD_1_FRAME,             " Ctrl+Shift+F         KeyDown   " },
   { NAV_CMD_JOG_REV_1_FRAME,             " Ctrl+Shift+S         KeyDown   " },
   { NAV_CMD_GOTO_IN,                     " Ctrl+Shift+O         KeyDown   " },
   { NAV_CMD_GOTO_OUT,                    " Ctrl+Shift+P         KeyDown   " },
   { NAV_CMD_GOTO_IN,                     " Ctrl+Shift+[         KeyDown   " },
   { NAV_CMD_GOTO_OUT,                    " Ctrl+Shift+]         KeyDown   " },
   { NAV_CMD_GOTO_MARK_IN,                " Ctrl+Shift+L         KeyDown   " },
   { NAV_CMD_GOTO_MARK_OUT_MINUS_ONE,     " Ctrl+Shift+;         KeyDown   " },
   { NAV_CMD_GOTO_MARK_IN,                " Ctrl+Shift+E         KeyDown   " },
   { NAV_CMD_GOTO_MARK_OUT_MINUS_ONE,     " Ctrl+Shift+R         KeyDown   " },

   // Paint often jumps more than one frame so we override 'continuous' funcs
   { NAV_CMD_JOG_FWD_1_FRAME,             " F                    KeyDown   " },
   { NAV_CMD_JOG_REV_1_FRAME,             " S                    KeyDown   " },

	// Paint II Commands
   //
   { PNT_CMD_EDIT_MASK,                   " Shift+4              KeyDown   " },
   { PNT_CMD_EDIT_TRACKING,               " Ctrl+4               KeyDown   " },
   { PNT_CMD_TOGGLE_TRACK_ALIGN_MODE,     " Ctrl+Q               KeyDown   " },
   { PNT_CMD_TOGGLE_AUTO_ALIGN_MODE,      " Alt+Q                KeyDown   " },

   { PNT_CMD_SHOW_IMPORT_FRAME,           " Ctrl+Shift+Shift     KeyDown   " },
   { PNT_CMD_SHOW_IMPORT_FRAME,           " Ctrl+Shift+Ctrl      KeyDown   " },
	{ PNT_CMD_SHOW_TARGET_FRAME,           " Ctrl+Shift           KeyUp     " },
	{ PNT_CMD_SHOW_TARGET_FRAME,           " Shift+Ctrl           KeyUp     " },
	{ PNT_CMD_SHOW_IMPORT_FRAME,           " MButton              ButtonDown" },
	{ PNT_CMD_SHOW_TARGET_FRAME,           " MButton              ButtonUp  " },

   { PNT_CMD_ENTER_REPOSITION_MODE,       " Alt+Alt              KeyDown   " },
   { PNT_CMD_LEAVE_REPOSITION_MODE,       " Alt                  KeyUp     " },
   { PNT_CMD_ENABLE_TOLERANCE,            " Ctrl+T               KeyDown   " },
   { PNT_CMD_TOGGLE_AUTO_ALIGN_MODE,      " Shift+5              KeyDown   " },
   { PNT_CMD_TOGGLE_DIFFERENCE_BLEND,     " Shift+T              KeyDown   " },
   { PNT_CMD_TOGGLE_DIFFERENCE_BLEND,     " 6                    KeyDown   " },
   { PNT_CMD_TOGGLE_DIFFERENCE_BLEND,     " Alt+6                KeyDown   " },
   { PNT_CMD_RESET_TRANSFORM,             " Ctrl+Shift+C         KeyDown   " },
   { PNT_CMD_LEAVE_MODE_REVERT,           " Esc                  KeyDown   " },
   { PNT_CMD_LEAVE_MODE_KEEP,             " Return               KeyUp     " },

   { PNT_CMD_BEG_REPOSITION,              " Alt+LButton          ButtonDown" },
   { PNT_CMD_END_REPOSITION,              " Alt+LButton          ButtonUp  " },
   { PNT_CMD_BEG_MOVE_CLONE_PT,           " Ctrl+Shift+LButton   ButtonDown" },
   { PNT_CMD_END_MOVE_CLONE_PT,           " Ctrl+Shift+LButton   ButtonUp  " },
   { PNT_CMD_BEG_POSITION_PAINT,          " LButton              ButtonDown" },
   { PNT_CMD_END_POSITION_PAINT,          " LButton              ButtonUp  " },
   { PNT_CMD_BEG_QUICK_ERASE,             " RButton              ButtonDown" },
   { PNT_CMD_END_QUICK_ERASE,             " RButton              ButtonUp  " },

   { PNT_CMD_POSITION_LFT,                " Left                 KeyDown   " },
   { PNT_CMD_POSITION_RGT,                " Right                KeyDown   " },
   { PNT_CMD_POSITION_UP,                 " Up                   KeyDown   " },
   { PNT_CMD_POSITION_DN,                 " Down                 KeyDown   " },
   { PNT_CMD_SHIFT_POS_LFT,               " Shift+Left           KeyDown   " },
   { PNT_CMD_SHIFT_POS_RGT,               " Shift+Right          KeyDown   " },
   { PNT_CMD_SHIFT_POS_UP,                " Shift+Up             KeyDown   " },
   { PNT_CMD_SHIFT_POS_DN,                " Shift+Down           KeyDown   " },
   { PNT_CMD_CTRL_POS_LFT,                " Ctrl+Left            KeyDown   " },
   { PNT_CMD_CTRL_POS_RGT,                " Ctrl+Right           KeyDown   " },
   { PNT_CMD_CTRL_POS_UP,                 " Ctrl+Up              KeyDown   " },
   { PNT_CMD_CTRL_POS_DN,                 " Ctrl+Down            KeyDown   " },
   { PNT_CMD_ALT_POS_LFT,                 " Alt+Left             KeyDown   " },
   { PNT_CMD_ALT_POS_RGT,                 " Alt+Right            KeyDown   " },
   { PNT_CMD_ALT_POS_UP,                  " Alt+Up               KeyDown   " },
   { PNT_CMD_ALT_POS_DN,                  " Alt+Down             KeyDown   " },
   { PNT_CMD_ALT_SHIFT_POS_LFT,           " Alt+Shift+Left       KeyDown   " },
   { PNT_CMD_ALT_SHIFT_POS_RGT,           " Alt+Shift+Right      KeyDown   " },
   { PNT_CMD_ALT_SHIFT_POS_UP,            " Alt+Shift+Up         KeyDown   " },
   { PNT_CMD_ALT_SHIFT_POS_DN,            " Alt+Shift+Down       KeyDown   " },
   { PNT_CMD_ALT_CTRL_POS_LFT,            " Ctrl+Alt+Left        KeyDown   " },
   { PNT_CMD_ALT_CTRL_POS_RGT,            " Ctrl+Alt+Right       KeyDown   " },
   { PNT_CMD_ALT_CTRL_POS_UP,             " Ctrl+Alt+Up          KeyDown   " },
   { PNT_CMD_ALT_CTRL_POS_DN,             " Ctrl+Alt+Down        KeyDown   " },

   { PNT_CMD_BEG_WIDGET_SCALE,            " Shift+LButton        ButtonDown" },
   { PNT_CMD_END_WIDGET_SCALE,            " Shift+Lbutton        ButtonUp  " },
   { PNT_CMD_REJECT_TARGET_FRAME,         " Shift+A              KeyDown   " },
   { PNT_CMD_ACCEPT_TARGET_FRAME,         " G                    KeyDown   " },
   { PNT_CMD_ACCEPT_TARGET_IN_PLACE,      " Ctrl+G               KeyDown   " },
   { PNT_CMD_BEG_WIDGET_SKEW,             " Ctrl+LButton         ButtonDown" },
   { PNT_CMD_END_WIDGET_SKEW,             " Ctrl+LButton         ButtonUp  " },

   { PNT_CMD_RESET_IMPORT_FRAME,          " Ctrl+Shift+Space     KeyDown   " },
   { PNT_CMD_TOGGLE_IMPORT_MODE,          " Ctrl+Shift+T         KeyDown   " },
   { PNT_CMD_SELECT_REVEAL_MODE,          " 1                    KeyDown   " },
   { PNT_CMD_SELECT_CLONE_MODE,           " 2                    KeyDown   " },
   { PNT_CMD_SELECT_COLOR_MODE,           " 3                    KeyDown   " },
   { PNT_CMD_SELECT_ORIG_OR_ALTCLIP_MODE, " 4                    KeyDown   " },
   { PNT_CMD_TOGGLE_CLONE_ABS_REL,        " Ctrl+Alt+T           KeyDown   " },
   { PNT_CMD_REPEAT_LAST_STROKES,         " `                    KeyDown   " },
   { PNT_CMD_UNDO_LAST_STROKE,            " A                    KeyDown" },

   { PNT_CMD_TOGGLE_PROCESSED_ORIGINAL,   " T                    KeyDown   " },
   { PNT_CMD_TOGGLE_SHOW_IMPORT_DIFFS,    " Ctrl+W               KeyDown   " },

   { PNT_CMD_SELECT_DENSITY_ADJUST,       " 7                    KeyDown   " },
   { PNT_CMD_RESET_DENSITY,               " Shift+7              KeyDown   " },
   { PNT_CMD_BRUSH_DISPLAY_OFF,           " D                    KeyDown   " },
   { PNT_CMD_BRUSH_DISPLAY_ON,            " D                    KeyUp     " },
   { PNT_CMD_SELECT_GRAIN_ADJUST,         " 9                    KeyDown   " },
   { PNT_CMD_RESET_GRAIN,                 " Shift+9              KeyDown   " },
   { PNT_CMD_RESET_DENSITY_OR_GRAIN,      " Shift+C              KeyDown   " },
   { PNT_CMD_SUSPEND_RANGE_MACRO,         " Ctrl+Space           KeyDown   " },
   { PNT_CMD_EXECUTE_RANGE_MACRO,         " Shift+G              KeyDown   " },

   { PNT_CMD_ENABLE_CONSTRAINT_X,         " Shift+Shift          KeyDown   " },
   { PNT_CMD_DISABLE_CONSTRAINT_X,        " Shift                KeyUp     " },
   { PNT_CMD_ENABLE_CONSTRAINT_Y,         " Ctrl+Ctrl            KeyDown   " },
   { PNT_CMD_DISABLE_CONSTRAINT_Y,        " Ctrl                 KeyUp     " },
   { PNT_CMD_ENABLE_CONSTRAINT_X,         " Alt+Shift+Shift      KeyDown   " },
   { PNT_CMD_DISABLE_CONSTRAINT_X,        " Alt+Shift            KeyUp     " },
   { PNT_CMD_ENABLE_CONSTRAINT_Y,         " Ctrl+Alt+Ctrl        KeyDown   " },
   { PNT_CMD_DISABLE_CONSTRAINT_Y,        " Alt+Ctrl             KeyUp     " },
   { PNT_CMD_ENABLE_TOLERANCE,            " 5                    KeyDown   " },
   { PNT_CMD_MASK_SELECT_ALL_REGIONS,     " Ctrl+A               KeyDown   " },

   { PNT_CMD_ENABLE_OPACITY,              " 0                    KeyDown   " },
	{ PNT_CMD_ENABLE_BLEND,                " 8                    KeyDown   " },
   { PNT_CMD_ENABLE_ASPECT,               " -                    KeyDown   " },
   { PNT_CMD_ENABLE_ANGLE,                " =                    KeyDown   " },

	{ PNT_CMD_GO_TO_FIRST_TRACKING_FRAME,  " Shift+E              KeyDown   " },
	{ PNT_CMD_PAUSE_OR_RESUME_TRACKING,    " Space                KeyDown   " },
};

static CUserInputConfiguration *PaintToolDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(PaintToolDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               PaintToolDefaultConfigItems);

// -------------------------------------------------------------------------
// PaintTool Tool Command Table

static CToolCommandTable::STableEntry PaintToolCommandTableEntries[] =
{
   // PaintTool Tool Command            PaintTool Tool Command String
   // -------------------------------------------------------------

   { NAV_CMD_JOG_FWD_1_FRAME,             "NAV_CMD_JOG_FWD_1_FRAME" },
   { NAV_CMD_JOG_REV_1_FRAME,             "NAV_CMD_JOG_REV_1_FRAME" },
   { NAV_CMD_GOTO_IN,                     "NAV_CMD_GOTO_IN" },
   { NAV_CMD_GOTO_OUT,                    "NAV_CMD_GOTO_OUT" },
   { NAV_CMD_GOTO_MARK_IN,                "NAV_CMD_GOTO_MARK_IN" },
   { NAV_CMD_GOTO_MARK_OUT_MINUS_ONE,     "NAV_CMD_GOTO_MARK_OUT_MINUS_ONE" },
   { NAV_CMD_JOG_FWD_1_FRAME,             "NAV_CMD_JOG_FWD_1_FRAME" },
   { NAV_CMD_JOG_REV_1_FRAME,             "NAV_CMD_JOG_REV_1_FRAME" },

   { PNT_CMD_EDIT_MASK,                   "PNT_CMD_EDIT_MASK" },
   { PNT_CMD_EDIT_TRACKING,               "PNT_CMD_EDIT_TRACKING" },
   { PNT_CMD_TOGGLE_TRACK_ALIGN_MODE,     "PNT_CMD_TOGGLE_TRACK_ALIGN_MODE" },

   { PNT_CMD_SHOW_IMPORT_FRAME,           "PNT_CMD_SHOW_IMPORT_FRAME" },
   { PNT_CMD_SHOW_TARGET_FRAME,           "PNT_CMD_SHOW_TARGET_FRAME" },

   { PNT_CMD_ENTER_REPOSITION_MODE,       "PNT_CMD_ENTER_REPOSITION_MODE" },
   { PNT_CMD_LEAVE_REPOSITION_MODE,       "PNT_CMD_LEAVE_REPOSITION_MODE" },
   { PNT_CMD_ENABLE_TOLERANCE,            "PNT_CMD_ENABLE_TOLERANCE" },
   { PNT_CMD_TOGGLE_AUTO_ALIGN_MODE,      "PNT_CMD_TOGGLE_AUTO_ALIGN_MODE" },
   { PNT_CMD_TOGGLE_DIFFERENCE_BLEND,     "PNT_CMD_TOGGLE_DIFFERENCE_BLEND" },
   { PNT_CMD_RESET_TRANSFORM,             "PNT_CMD_RESET_TRANSFORM" },
   { PNT_CMD_LEAVE_MODE_REVERT,           "PNT_CMD_LEAVE_MODE_REVERT" },
   { PNT_CMD_LEAVE_MODE_KEEP,             "PNT_CMD_LEAVE_MODE_KEEP" },

   { PNT_CMD_BEG_REPOSITION,              "PNT_CMD_BEG_REPOSITION" },
   { PNT_CMD_END_REPOSITION,              "PNT_CMD_END_REPOSITION" },
   { PNT_CMD_BEG_MOVE_CLONE_PT,           "PNT_CMD_BEG_MOVE_CLONE_PT" },
   { PNT_CMD_END_MOVE_CLONE_PT,           "PNT_CMD_END_MOVE_CLONE_PT" },
   { PNT_CMD_BEG_POSITION_PAINT,          "PNT_CMD_BEG_POSITION_PAINT" },
   { PNT_CMD_END_POSITION_PAINT,          "PNT_CMD_END_POSITION_PAINT" },
   { PNT_CMD_BEG_QUICK_ERASE,             "PNT_CMD_BEG_QUICK_ERASE" },
   { PNT_CMD_END_QUICK_ERASE,             "PNT_CMD_END_QUICK_ERASE" },

   { PNT_CMD_POSITION_LFT,                "PNT_CMD_POSITION_LFT" },
   { PNT_CMD_POSITION_RGT,                "PNT_CMD_POSITION_RGT" },
   { PNT_CMD_POSITION_UP,                 "PNT_CMD_POSITION_UP" },
   { PNT_CMD_POSITION_DN,                 "PNT_CMD_POSITION_DN" },
   { PNT_CMD_SHIFT_POS_LFT,               "PNT_CMD_SHIFT_POS_LFT" },
   { PNT_CMD_SHIFT_POS_RGT,               "PNT_CMD_SHIFT_POS_RGT" },
   { PNT_CMD_SHIFT_POS_UP,                "PNT_CMD_SHIFT_POS_UP" },
   { PNT_CMD_SHIFT_POS_DN,                "PNT_CMD_SHIFT_POS_DN" },
   { PNT_CMD_CTRL_POS_LFT,                "PNT_CMD_CTRL_POS_LFT" },
   { PNT_CMD_CTRL_POS_RGT,                "PNT_CMD_CTRL_POS_RGT" },
   { PNT_CMD_CTRL_POS_UP,                 "PNT_CMD_CTRL_POS_UP" },
   { PNT_CMD_CTRL_POS_DN,                 "PNT_CMD_CTRL_POS_DN" },
   { PNT_CMD_ALT_POS_LFT,                 "PNT_CMD_ALT_POS_LFT" },
   { PNT_CMD_ALT_POS_RGT,                 "PNT_CMD_ALT_POS_RGT" },
   { PNT_CMD_ALT_POS_UP,                  "PNT_CMD_ALT_POS_UP" },
   { PNT_CMD_ALT_POS_DN,                  "PNT_CMD_ALT_POS_DN" },
   { PNT_CMD_ALT_SHIFT_POS_LFT,           "PNT_CMD_ALT_SHIFT_POS_LFT" },
   { PNT_CMD_ALT_SHIFT_POS_RGT,           "PNT_CMD_ALT_SHIFT_POS_RGT" },
   { PNT_CMD_ALT_SHIFT_POS_UP,            "PNT_CMD_ALT_SHIFT_POS_UP" },
   { PNT_CMD_ALT_SHIFT_POS_DN,            "PNT_CMD_ALT_SHIFT_POS_DN" },
   { PNT_CMD_ALT_CTRL_POS_LFT,            "PNT_CMD_ALT_CTRL_POS_LFT" },
   { PNT_CMD_ALT_CTRL_POS_RGT,            "PNT_CMD_ALT_CTRL_POS_RGT" },
   { PNT_CMD_ALT_CTRL_POS_UP,             "PNT_CMD_ALT_CTRL_POS_UP" },
   { PNT_CMD_ALT_CTRL_POS_DN,             "PNT_CMD_ALT_CTRL_POS_DN" },

   { PNT_CMD_BEG_WIDGET_SCALE,            "PNT_CMD_BEG_WIDGET_SCALE" },
   { PNT_CMD_END_WIDGET_SCALE,            "PNT_CMD_END_WIDGET_SCALE" },
   { PNT_CMD_REJECT_TARGET_FRAME,         "PNT_CMD_REJECT_TARGET_FRAME" },
   { PNT_CMD_ACCEPT_TARGET_FRAME,         "PNT_CMD_ACCEPT_TARGET_FRAME" },
   { PNT_CMD_ACCEPT_TARGET_IN_PLACE,      "PNT_CMD_ACCEPT_TARGET_IN_PLACE" },
   { PNT_CMD_BEG_WIDGET_SKEW,             "PNT_CMD_BEG_WIDGET_SKEW" },
   { PNT_CMD_END_WIDGET_SKEW,             "PNT_CMD_END_WIDGET_SKEW" },

   { PNT_CMD_CYCLE_BRUSH_FWD,             "PNT_CMD_CYCLE_BRUSH_FWD" },
   { PNT_CMD_CYCLE_BRUSH_BWD,             "PNT_CMD_CYCLE_BRUSH_BWD" },
   { PNT_CMD_TOGGLE_ELLIPSE_RECT,         "PNT_CMD_TOGGLE_ELLIPSE_RECT" },

   { PNT_CMD_RESET_IMPORT_FRAME,          "PNT_CMD_RESET_IMPORT_FRAME" },
   { PNT_CMD_TOGGLE_IMPORT_MODE,          "PNT_CMD_TOGGLE_IMPORT_MODE" },
   { PNT_CMD_SELECT_REVEAL_MODE,          "PNT_CMD_SELECT_REVEAL_MODE" },
   { PNT_CMD_SELECT_CLONE_MODE,           "PNT_CMD_SELECT_CLONE_MODE" },
   { PNT_CMD_SELECT_COLOR_MODE,           "PNT_CMD_SELECT_COLOR_MODE" },
   { PNT_CMD_SELECT_ORIG_OR_ALTCLIP_MODE, "PNT_CMD_SELECT_ORIG_OR_ALTCLIP_MODE" },
   { PNT_CMD_TOGGLE_CLONE_ABS_REL,        "PNT_CMD_TOGGLE_CLONE_ABS_REL" },
   { PNT_CMD_REPEAT_LAST_STROKES,         "PNT_CMD_REPEAT_LAST_STROKES" },
   { PNT_CMD_UNDO_LAST_STROKE,            "PNT_CMD_UNDO_LAST_STROKE" },

   { PNT_CMD_TOGGLE_PROCESSED_ORIGINAL,   "PNT_CMD_TOGGLE_PROCESSED_ORIGINAL" },
   { PNT_CMD_TOGGLE_SHOW_IMPORT_DIFFS,    "PNT_CMD_TOGGLE_SHOW_IMPORT_DIFFS" },

   { PNT_CMD_SELECT_DENSITY_ADJUST,       "PNT_CMD_SELECT_DENSITY_ADJUST" },
   { PNT_CMD_RESET_DENSITY,               "PNT_CMD_RESET_DENSITY" },
   { PNT_CMD_BRUSH_DISPLAY_OFF,           "PNT_CMD_BRUSH_DISPLAY_OFF" },
   { PNT_CMD_BRUSH_DISPLAY_ON,            "PNT_CMD_BRUSH_DISPLAY_ON" },
   { PNT_CMD_SELECT_GRAIN_ADJUST,         "PNT_CMD_SELECT_GRAIN_ADJUST" },
   { PNT_CMD_RESET_GRAIN,                 "PNT_CMD_RESET_GRAIN" },
   { PNT_CMD_RESET_DENSITY_OR_GRAIN,      "PNT_CMD_RESET_DENSITY_OR_GRAIN" },
   { PNT_CMD_SUSPEND_RANGE_MACRO,         "PNT_CMD_SUSPEND_RANGE_MACRO" },
   { PNT_CMD_EXECUTE_RANGE_MACRO,         "PNT_CMD_EXECUTE_RANGE_MACRO" },

   { PNT_CMD_ENABLE_CONSTRAINT_X,         "PNT_CMD_ENABLE_CONSTRAINT_X" },
   { PNT_CMD_DISABLE_CONSTRAINT_X,        "PNT_CMD_DISABLE_CONSTRAINT_X" },
   { PNT_CMD_ENABLE_CONSTRAINT_Y,         "PNT_CMD_ENABLE_CONSTRAINT_Y" },
   { PNT_CMD_DISABLE_CONSTRAINT_Y,        "PNT_CMD_DISABLE_CONSTRAINT_Y" },
   { PNT_CMD_ENABLE_CONSTRAINT_X,         "PNT_CMD_ENABLE_CONSTRAINT_X" },
   { PNT_CMD_DISABLE_CONSTRAINT_X,        "PNT_CMD_DISABLE_CONSTRAINT_X" },
   { PNT_CMD_ENABLE_CONSTRAINT_Y,         "PNT_CMD_ENABLE_CONSTRAINT_Y" },
   { PNT_CMD_DISABLE_CONSTRAINT_Y,        "PNT_CMD_DISABLE_CONSTRAINT_Y" },
   { PNT_CMD_ENABLE_TOLERANCE,            "PNT_CMD_ENABLE_TOLERANCE" },
   { PNT_CMD_MASK_SELECT_ALL_REGIONS,     "PNT_CMD_MASK_SELECT_ALL_REGIONS" },

   { PNT_CMD_ENABLE_OPACITY,              "PNT_CMD_ENABLE_OPACITY" },
   { PNT_CMD_ENABLE_BLEND,                "PNT_CMD_ENABLE_BLEND" },
   { PNT_CMD_ENABLE_ASPECT,               "PNT_CMD_ENABLE_ASPECT" },
   { PNT_CMD_ENABLE_ANGLE,                "PNT_CMD_ENABLE_ANGLE" },

	{ PNT_CMD_GO_TO_FIRST_TRACKING_FRAME,  "PNT_CMD_GO_TO_FIRST_TRACKING_FRAME " },
	{ PNT_CMD_PAUSE_OR_RESUME_TRACKING,    "PNT_CMD_PAUSE_OR_RESUME_TRACKING " },

};

static CToolCommandTable *PaintToolCommandTable
   = new CToolCommandTable(sizeof(PaintToolCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           PaintToolCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// CRealToolPaintInterface
//////////////////////////////////////////////////////////////////////

CRealToolPaintInterface::CRealToolPaintInterface(CPaintTool *pttool)
{
   paintTool = pttool;
}

CRealToolPaintInterface::~CRealToolPaintInterface()
{

}

void CRealToolPaintInterface::setTransform(double rot,
                                           double strx, double stry,
                                           double skew,
                                           double offx, double offy)
{
   PaintToolForm->UpdateTransform(rot, strx, stry, skew, offx, offy);
}

void CRealToolPaintInterface::setImportFrameMode(int impmod)
{
   if (GPaintTool != NULL)
   {
      PaintToolForm->UpdateImportFrameMode(impmod);
      PaintToolForm->UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();
   }
}

void CRealToolPaintInterface::setImportFrameOffset(int frmoff)
{
   if (GPaintTool != NULL)
   {
      PaintToolForm->UpdateImportFrameOffset(frmoff);
      PaintToolForm->UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();
   }
}

void CRealToolPaintInterface::setImportFrameTimecode(int impfrm)
{
   if (GPaintTool != NULL)
   {
      PaintToolForm->UpdateImportFrameTimecode(impfrm);
      PaintToolForm->UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();
   }
}

void CRealToolPaintInterface::setTargetFrameTimecode(int trgfrm)
{
   if (GPaintTool != NULL)
   {
      PaintToolForm->UpdateTargetFrameTimecode(trgfrm);
      PaintToolForm->UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();
   }
}

void CRealToolPaintInterface::drawProxy(unsigned int *img)
{
   PaintToolForm->DrawProxy(img);
}

void CRealToolPaintInterface::clearStrokeStack()
{
   PaintToolForm->ClearStrokeStack();
}

void CRealToolPaintInterface::setStrokeEntry(int ordnum, int brmod)
{
   PaintToolForm->SetStrokeEntry(ordnum, brmod);
}

void CRealToolPaintInterface::setPickedColor(float *colrcomp)
{
   PaintToolForm->SetPickedColor(colrcomp);
}

void CRealToolPaintInterface::onLoadNewTargetFrame()
{
   return paintTool->OnLoadNewTargetFrame();
}

void CRealToolPaintInterface::setFrameLoadTimer()
{
   PaintToolForm->SetLoadTimer();
}

//////////////////////////////////////////////////////////////////////
// CPaintTool:
//////////////////////////////////////////////////////////////////////

string CPaintTool::titlekey        = "Paint Parameters";
string CPaintTool::currentbrushkey = "CurrentBrush";
string CPaintTool::maxradiuskey    = "MaxRadius";
string CPaintTool::radiuskey       = "Radius";
string CPaintTool::opacitykey      = "Opacity";
string CPaintTool::blendkey        = "Blend";
string CPaintTool::aspectkey       = "Aspect";
string CPaintTool::anglekey        = "Angle";
string CPaintTool::shapekey        = "Shape";
string CPaintTool::typekey         = "Type";

string CPaintTool::colorKey = "Color";

string CPaintTool::revealModeImportFrameModeKey = "RevealModeImportFrameMode";
string CPaintTool::revealModeImportFrameKey = "RevealModeImportFrame";
string CPaintTool::revealModeImportFrameOffsetKey = "RevealModeImportFrameOffset";
string CPaintTool::revealModeRotationKey = "RevealModeRotation";
string CPaintTool::revealModeSkewKey = "RevealModeSkew";
string CPaintTool::revealModeStretchXKey = "RevealModeStretchX";
string CPaintTool::revealModeStretchYKey = "RevealModeStretchY";
string CPaintTool::revealModeOffsetXKey = "RevealModeOffsetX";
string CPaintTool::revealModeOffsetYKey = "RevealModeOffsetY";
string CPaintTool::revealModeOnionSkinModeKey = "RevealModeOnionSkinMode";

string CPaintTool::cloneModeImportFrameModeKey = "CloneModeImportFrameMode";
string CPaintTool::cloneModeImportFrameKey = "CloneModeImportFrame";
string CPaintTool::cloneModeImportFrameOffsetKey = "CloneModeImportFrameOffset";
string CPaintTool::cloneModePointModeKey = "CloneModePointMode";
string CPaintTool::cloneModeXKey = "CloneModeX";
string CPaintTool::cloneModeYKey = "CloneModeY";
string CPaintTool::cloneModeOffsetXKey = "CloneModeOffsetX";
string CPaintTool::cloneModeOffsetYKey = "CloneModeOffsetY";

string CPaintTool::origValuesModeOnionSkinModeKey = "OrigValuesModeOnionSkinMode";
string CPaintTool::altClipModeOnionSkinModeKey = "AltClipModeOnionSkinMode";

string CPaintTool::paintModeKey = "PaintModeKey";
string CPaintTool::autoAcceptModeKey = "AutoAcceptModeKey";

CPaintTool::CPaintTool(const string &newToolName, MTI_UINT32 **newFeatureTable) :
   CToolObject(newToolName, newFeatureTable), toolSetupHandle(-1),
   trainingToolSetupHandle(-1)

{
   // Tool-specific constructors go here

   // no clip to start
   curClip = NULL;

   // brush strings
   brushstr[0]  = "Brush1";
   brushstr[1]  = "Brush2";
   brushstr[2]  = "Brush3";
   brushstr[3]  = "Brush4";
   brushstr[4]  = "Brush5";
   brushstr[5]  = "Brush6";
   brushstr[6]  = "Brush7";
   brushstr[7]  = "Brush8";
   brushstr[8]  = "Brush9";
   brushstr[9]  = "Brush10";

   IniFileName = CPMPIniFileName("Paint");

   paintMode = PAINT_MODE_REVEAL;

   mouseWheelState = -1;

   majorState = MAJORSTATE_INVALID;
   alignState = ALIGNSTATE_MANUAL;

   paintInterface = NULL;

   captureOffset = false;

   mouseConstraint = MOUSE_CONSTRAINT_NONE;

   mixDelta = 5;
   positionDelta = 5;     // fifth  pixels
   autoAlignBoxSizeDelta = 4;
   toleranceDelta = 5;
   brushRadiusFactor = 1.1;
   brushRadiusDelta = 0;  // single pixels
   brushOpacityDelta = 5;
   brushBlendDelta = 5;
   brushAspectDelta = 5;
   brushAngleDelta = 5;
   brushDensityDelta = 5;
   brushGrainDelta = 5;

   brushDensityStrength = 0;
   brushGrainStrength = 0;

   autoAlignBoxSize = 64;

   quickEraseStartIsPending = false;

   repeatMacroBegFrame = -1;
   rangeMacroBegFrame = -1;
   rangeMacroExecuting = false;

   pndStrFileHandle = -1;
   prvStrFileHandle = -1;

   // no capture
   strCapture = false;

   // queue for catching stroke points
   strFill  = 0; strEmpty = 0;
   strokeQueue = new STRPNT[MAX_STROKE_POINTS];

   // buffer for capturing macro points
   pndStrBuf = new MTI_UINT8[MAX_MACRO_POINTS*sizeof(STRPNT)];
   pndStrBufEnd = pndStrBuf + (MAX_MACRO_POINTS*sizeof(STRPNT));

   // buffer for playing back macro points
   prvStrBuf = new MTI_UINT8[MAX_MACRO_POINTS*sizeof(STRPNT)];
   prvStrBufEnd = prvStrBuf + (MAX_MACRO_POINTS*sizeof(STRPNT));

   // tracking points editor
   TrkPtsEditor = new TrackingBoxManager;

   // for caqpturing tracking data for use by TRACKED MACRO
   trkPts.clear();
   trkBegFrame = trkEndFrame = -1;
   trackedMacroFlag = false;

   // other tracking crap
   bNeedToReTrack = true;
   _WarningDialogs = true;

   // control variables for background threads
   disablePainter = false;
   disableHealer = false;
   healerOn = false;

   // semaphores for main thread to block on while threads are ending
   painterThreadEndSem = SemAlloc();
   fifoResetSem = SemAlloc();
   healerThreadEndSem = SemAlloc();

   // progress monitor
   needToClearProgressMonitor = false;
}

CPaintTool::~CPaintTool()
{
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the PaintTool Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CPaintTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CPaintTool's base class, i.e., CToolObject
   // This must be done before the CPaintTool initializes itself
    retVal = initBaseToolObject(PaintToolNumber, newSystemAPI,
                               PaintToolCommandTable,
                               PaintToolDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in PaintTool Tool, initBaseToolObject failed "
                     << retVal);
      return retVal;
      }

   // Initialize the tracking points editor
   TrkPtsEditor->loadSystemAPI(getSystemAPI());
   TrkPtsEditor->setIgnoringMarksFlag(true);
   TrkPtsEditor->setTrackingArray(&TrackingArray);

   // Create the Tool's GUI
   CreatePaintToolGUI();

   paintInterface = new CRealToolPaintInterface(this);
   getSystemAPI()->SetToolPaintInterface(paintInterface);

   readGeneralSettings();

   // Per Larry, reset import frame and clone offsets at app start
   int lastFrame = getSystemAPI()->getLastFrameIndex();
   revealModeImportFrameMode = IMPORT_MODE_RELATIVE;
   revealModeImportFrame = lastFrame;
   revealModeImportFrameOffset = -2;
   revealModeRotation = 0.0;
   revealModeStretchX = 1.0;
   revealModeStretchY = 1.0;
   revealModeOffsetX = 0.0;
   revealModeOffsetY = 0.0;
   revealModeOnionSkinMode = ONIONSKIN_MODE_COLOR;
   PaintToolForm->SetOnionSkinMode(revealModeOnionSkinMode);

   cloneModeImportFrameMode = IMPORT_MODE_RELATIVE;
   cloneModeImportFrame = lastFrame;
   cloneModeImportFrameOffset = 0;

   altClipModeImportFrame = lastFrame;
   altClipModeImportFrameOffset = 0;

   clModePointMode = true;
   origValuesModeOnionSkinMode = ONIONSKIN_MODE_POSNEG;
   PaintToolForm->SetOrigOnionSkinMode(origValuesModeOnionSkinMode);
   altClipModeOnionSkinMode = ONIONSKIN_MODE_POSNEG;
   PaintToolForm->SetAltClipOnionSkinMode(altClipModeOnionSkinMode);
   InitClonePt();

   writeImportFrameAndOffset();

   firstActivationFlag = true;

   return 0;

}   // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CPaintTool::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   // launch background painter thread
   BThreadSpawn(CPaintTool::BackgroundPainterCallback,this);

   // launch background healer thread
   BThreadSpawn(CPaintTool::BackgroundHealerCallback,this);

   // Disable the tool infrastructure's cache because we steal the
   // preallocated buffers that belong to the TI, plus we write frames
   // behind the TI's back!
   getSystemAPI()->GetToolFrameCache()->EnableCaching(false);

   // enable autofocus on entry to MainWindow
   getSystemAPI()->setMainWindowAutofocus(false);

   // use of the provisional buffer for writeback
   // mandates clearing the provisional frame index --
   // otherwise the Player may pass the provisional
   // buffer back to Paint instead of a clip frame!
   getSystemAPI()->ClearProvisional(false);

   // important flag settings
   getSystemAPI()->SetMaskAllowed(true);
   getSystemAPI()->EnableMaskOutlineDisplay(true);
   getSystemAPI()->enableBrushDisplay(true);
   getSystemAPI()->enableForceTargetLoad(false);
   getSystemAPI()->enableForceImportLoad(false);

   // now switch Displayer over to PAINT mode
   getSystemAPI()->enterPaintMode();

   // make sure to allocate brushes before possibly
   // discovering the case that no clip is loaded
   // - in the following maxBrushRadius is OUTPUT (read from ini file)
   int curbrush = allocateBrushesAndReadBrushParametersFromIni
                     (PaintToolForm->GetBrushes(), maxBrushRadius);
   PaintToolForm->SetCurrentBrush(curbrush);

   // set up the current clip
   // TELL IT TO NOT CLEAR THE FRICKIN MACRO (except the first time!)
   retVal = InitClip(!firstActivationFlag);
   if (retVal != 0)
      return retVal;

   // read the palette colors and update localRGB
   // -- needed before first call to LoadBrush
   PaintToolForm->ReadPaletteColors();

   // BUG: this needed to be moved above InitClip()
   // in case Paint is entered with no clip loaded
   //
   //int curbrush = allocateBrushesAndReadBrushParametersFromIni
   //                  (PaintToolForm->GetBrushes());
   //PaintToolForm->SetCurrentBrush(curbrush);

   // draws the whole upper half of the GUI
   // forces BrushGroupBox to be visible
   PaintToolForm->InitGUIBrushPanel();

   // read import frame and offset data
   retVal = readImportFrameAndOffset();
   if (retVal != 0)
      return retVal;

   // enter the initial mode
   SelectPaintMode(paintMode);

   // refresh from disk
   getSystemAPI()->refreshFrame();

   // ensure mouse wheel adjusts brush radius
   PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
   PaintToolForm->DensitySpeedButton->Down = false;
   PaintToolForm->GrainSpeedButton->Down   = false;

   // a hack to coordinate tilde key operation with GOV tool
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_PAINT_MODE);

   // Grab and init the tracking tool
   bool success = getSystemAPI()->LockTrackingTool();
   MTIassert(success);
   getSystemAPI()->SetTrackingToolEditor(TrkPtsEditor);
	getSystemAPI()->SetTrackingToolProgressMonitor(GetToolProgressMonitor());

   GetToolProgressMonitor()->SetStatusMessage((paintMode == PAINT_MODE_CLONE)?
                                              "Clone pixels" : "Paint pixels");

   getSystemAPI()->setGrainPresets(&GetGrainPresets());

   GetToolProgressMonitor()->SetIdle(false);
   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   resetLastMousePos = true;

   onNewClipInternal(!firstActivationFlag);
   firstActivationFlag = false;

   JobManager jobManager;
   jobManager.SetCurrentToolCode("pn");

	getSystemAPI()->SetToolNameForUserGuide("PaintTool");

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CPaintTool::toolDeactivate()
{
   // terminate background painter thread
   disablePainter = true;
   SemSuspend(painterThreadEndSem);   // wait until it's done

   // terminate background healer thread
   disableHealer = true;
   SemSuspend(healerThreadEndSem);   // wait until it's done

   // if executing a range macro suspend it
   SuspendRangeMacro();

   // make sure this is cleared
   getSystemAPI()->enableForceTargetLoad(false);
   getSystemAPI()->enableForceImportLoad(false);

   // Get rid of any review regions that might be present on the screen
   getSystemAPI()->ClearProvisional(true);

   // Bounce out of "Edit Tracking Point" mode if we're in it
	if (getMajorState() == MAJORSTATE_EDIT_TRACKING
   || majorState == MAJORSTATE_TRACKING_DONE
   || majorState == MAJORSTATE_TRACKING_PAUSED)
   {
      LeaveEditTrackingState();
   }

   // write misc settings
   writeGeneralSettings();

   // write import frame and offset
   writeImportFrameAndOffset();

   // write the brush parameters
   int curbrsh = PaintToolForm->GetCurrentBrushIndex();
   writeBrushParametersToIniAndFreeBrushes
      (PaintToolForm->GetBrushes(), curbrsh, maxBrushRadius);

   // write these out
   PaintToolForm->WritePaletteColors();

   // make sure that the displayer's brush pointer is zeroed
   getSystemAPI()->unloadPaintBrush();

   // these were moved here from HidePaintToolGUI() - we want to make sure
   // all processing is done before changing state!

   getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
   getSystemAPI()->exitPaintMode();
   setMajorState(MAJORSTATE_GUI_CREATED);

   // a hack to coordinate tilde key operation with GOV tool
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_OTHER_MODE);

   // Release the tracking tool
   getSystemAPI()->DeactivateTrackingTool();
   getSystemAPI()->SetTrackingToolEditor(NULL);
   getSystemAPI()->SetTrackingToolProgressMonitor(NULL);
   getSystemAPI()->ReleaseTrackingTool();
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   getSystemAPI()->setGrainPresets(nullptr);

   // causes  CORRECT  to hang when exiting
   // the app while PAINT is the active tool
   //
   // refresh from disk
   //getSystemAPI()->refreshFrame();

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              PaintTool Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CPaintTool::toolShutdown()
{
	int retVal;

	DestroyPaintToolGUI(); // Destroy the GUI  -- FIRST!!
	GPaintTool = NULL; // Say we are destroyed

	delete[]prvStrBuf;
	delete[]pndStrBuf;
	delete[]strokeQueue;
	delete TrkPtsEditor;

	// semaphores for main thread to block on while threads are ending
	SemFree(painterThreadEndSem);
	SemFree(fifoResetSem);
	SemFree(healerThreadEndSem);

	// Shutdown the PaintTool Tool's base class, i.e., CToolObject.
	// This must be done after the PaintTool shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
}

CToolProcessor* CPaintTool::makeToolProcessorInstance(
														 const bool *newEmergencyStopFlagPtr)
{
	return NULL;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description: PaintTool Tool's redraw handler
//
// Arguments:   frame index
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CPaintTool::onRedraw(int frameindex)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // if tracked macro is possible enable the checkbox
   PaintToolForm->TrackedMacroCheckBox->Enabled = TrackedMacroIsEnabled();

   // note that when navigating to a new import frame from
   // edit-transform mode, the widget is not displayed
   if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

      getSystemAPI()->RedrawTransform();
      return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
   }

	if (majorState == MAJORSTATE_EDIT_TRACKING
	|| majorState == MAJORSTATE_TRACKING_IN_PROGRESS
	|| majorState == MAJORSTATE_TRACKING_DONE
	|| majorState == MAJORSTATE_TRACKING_PAUSED)
	{
		if (getDisplayTrackingPoints())
		{
			TrkPtsEditor->drawTrackingPointsCurrentFrame();
		}
	}

   // HACK - because in tracking alignment mode the transform is
   // determined dynamically by the Displayer, we need to grab the
   // transform for the current frame ... DOUBLE HACK we know that
   // in this mode there is no rotation, scaling or skewing!
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
         ((alignState == ALIGNSTATE_TRACK_SHOWING_TARGET)||
          (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH))) {

      double rot=0.0, strx=1.0, stry=1.0, skew=0.0, offx, offy;
      getSystemAPI()->getTrackingAlignOffsets(frameindex, offx, offy);
      PaintToolForm->UpdateTransform(rot, strx, stry, skew, offx, offy);
   }

   if (rangeMacroExecuting) {

      // mask setup is now done in "loadTargetFrame"
      // getSystemAPI()->CalculateMaskROI(frameindex);

      // NEW: ensure the import frame is loaded
      // QQQQQQ? PaintToolForm->ClearLoadTimer();
      // QQQQQQ? getSystemAPI()->forceTargetAndImportFrameLoaded();

      // 'displayFrame' already having been called with the
      // 'fast' arg false, the virgin target frame has been
      // loaded and the context for the stroke is all set.
      // STILL TRUE because 'forceTargetLoad' is enabled

      int retVal = RangeCapturedStroke();

      bool macroTerminated = false;
      if (retVal != 0) {

         TRACE_0(errout << "ERROR: Paint range macro terminated due to error "
                        << retVal);

         // during "toolPlay" only this command can get thru
         getSystemAPI()->fullStop();

         _MTIErrorDialog("Macro terminated because of error!    ");

         macroTerminated = true;
      }

      // update progress monitor
      GetToolProgressMonitor()->BumpProgress();

      // if this is the final frame of the sequence
		// clear the Player and pop the button up
		int stopFrameIndex = strReverse ? strBegFrame : strEndFrame - 1;
		if ((frameindex == stopFrameIndex) || macroTerminated) {

         // CAREFUL!! THIS IS DUPLICATE OF CODE IN onPlayerStop() !!!

         // back to normal behavior
         getSystemAPI()->enableForceTargetLoad(false);
         getSystemAPI()->enableForceImportLoad(false);

         // reload the last modified target frame
         getSystemAPI()->reloadModifiedTargetFrame();

         // context after the operation
         LoadStrokeContext(savedContext);

         // restore the clone brush if needed
         if (majorState == MAJORSTATE_CLONE_MODE_HOME)
            ReloadCloneBrush();

         // reset the macro buttons
			PaintToolForm->RangeStrokeSpeedButton->Down = false;
			PaintToolForm->StopSpeedButton->Down = false;
         PaintToolForm->StopSpeedButton->Enabled = false;

         // state is reset
         rangeMacroExecuting = false;

         /////////////
         // mbraca added this to obliterate the "original" data
         // lurking for the last frame of a range macro execution.
         // It looks like a no-op, but it's not -- a fresh copy of
         // the painted frame is always read from disk!!!!!!
         //
         // CAREFUL!!! THIS IS BAD BECAUSE WE GET REENTERED HERE (onRedraw)
         //            SO THIS CALL MUST FOLLOW "rangeMacroExecuting = false"!!
         //
         getSystemAPI()->goToFrameSynchronous(frameindex);
         ////////////

         // update status readout
         GetToolProgressMonitor()->SetStatusMessage("Processing complete");
         GetToolProgressMonitor()->StopProgress(true);
         needToClearProgressMonitor = true;
      }
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlayerStop
//
// Description: PaintTool Tool's onPlayerStop handler
//
// Arguments:   none
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CPaintTool::onPlayerStop()
{
   // when the Player is halted with a "stopAndSynchronize"
   // this method is called by the ToolManager to clean up
   //
   if (rangeMacroExecuting) {

      // CAREFUL!! THIS IS DUPLICATE OF CODE IN onRedraw() !!!

      // back to normal behavior
      getSystemAPI()->enableForceTargetLoad(false);
      getSystemAPI()->enableForceImportLoad(false);

      // reload the last modified target frame
      getSystemAPI()->reloadModifiedTargetFrame();

      // context after the operation
      LoadStrokeContext(savedContext);

      // restore the clone brush if needed
      if (majorState == MAJORSTATE_CLONE_MODE_HOME)
         ReloadCloneBrush();

      // pop the buttons back up
		PaintToolForm->RangeStrokeSpeedButton->Down = false;
		PaintToolForm->StopSpeedButton->Down = false;
      PaintToolForm->StopSpeedButton->Enabled = false;

      // state is reset
      rangeMacroExecuting = false;

      /////////////
      // mbraca added this to obliterate the "original" data
      // lurking for the last frame of a range macro execution.
      // It looks like a no-op, but it's not -- a fresh copy of
      // the painted frame is always read from disk!!!!!!
      //
      // CAREFUL!!! THIS IS BAD BECAUSE WE GET REENTERED AT onRedraw()
      //            SO THIS CALL MUST FOLLOW "rangeMacroExecuting = false"!!
      //
      getSystemAPI()->goToFrameSynchronous(getSystemAPI()->getLastFrameIndex());
      ////////////

      // update progress monitor
      GetToolProgressMonitor()->SetStatusMessage("Stopped");
      GetToolProgressMonitor()->StopProgress(false);
      needToClearProgressMonitor = true;
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlaybackFilterChange
//
// Description: PaintTool Tool's onPlaybackFilterChange handler
//
// Arguments:   none
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CPaintTool::onPlaybackFilterChange(int fltr)
{
////   if (fltr == PLAYBACK_FILTER_NONE || fltr == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS) {
   if (fltr != PLAYBACK_FILTER_ORIGINAL_VALUES) {

      if (showState == SHOW_ORIGINAL) {

         getSystemAPI()->setDisplayProcessed(true);

         showState = SHOW_PROCESSED;

         PaintToolForm->ViewLabel->Caption = "Processed";
         PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
      }
   }
   else { // boxes and/or ORIG values

      if (showState == SHOW_PROCESSED) {

         getSystemAPI()->setDisplayProcessed(false);

         showState = SHOW_ORIGINAL;

         PaintToolForm->ViewLabel->Caption = "Original";
         PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
      }
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: PaintTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CPaintTool::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // Get the command number and maybe output the command name to trace log
   int commandNumber = toolCommand.getCommandNumber();
   const CToolCommandTable *commandTable = getCommandTable();
   const char* commandName = commandTable->findCommandName(commandNumber);
   if (commandName == 0)
   {
      commandName = "UNKNOWN";
   }

   TRACE_2(errout << "Processing command: " << commandName << " (" << commandNumber << ")");

   // Assume we are going to handle the command!
   int Result = TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK;

   // any command except left-mouse-butn is shorted out in GOV mode
   if (commandNumber != PNT_CMD_BEG_POSITION_PAINT)
   {
      if ((majorState == MAJORSTATE_GET_ORIGINAL_VALUES)
      && (commandNumber != NAV_CMD_JOG_FWD_1_FRAME)
      && (commandNumber != NAV_CMD_JOG_REV_1_FRAME))
      {
         // swallow the command
         return Result;
      }
   }

	// Only one command accepted when tracking is in progress (other input is handled
	// by the Tracking Tool or navigator Tool
	if (majorState == MAJORSTATE_TRACKING_IN_PROGRESS)
	{
		// Larry spacebar "pause/resule tracking" hack-a-rama.
		if (commandNumber == PNT_CMD_PAUSE_OR_RESUME_TRACKING)
		{
			// DO THIS BEFORE STOPPING TRACKING OR MAJORSTATE WILL GET CLOBBERED!!!
			setMajorState(MAJORSTATE_TRACKING_PAUSED);
			PaintToolForm->setMajorState(MAJORSTATE_TRACKING_PAUSED);
			PaintToolForm->UpdateTrackingPointButtons();
			GetToolProgressMonitor()->SetStatusMessage("Tracking stopped");

			getSystemAPI()->StopTracking();

			return Result;
		}

		return TOOL_RETURN_CODE_NO_ACTION;
	}

   // Just a couple of commands accepted when in "edit tracking" mode
   // (most input is handled by the Tracking Tool or navigator Tool
	if (majorState == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED)
   {
		switch (commandNumber)
      {

      case PNT_CMD_TOGGLE_TRACK_ALIGN_MODE:
      {
         // Track Align button is always UP when in Edit Tracking mode!
         // Get out of Edit Tracking mode and push Track Align DOWN

         // Do nothing if the button is not enabled!
         if (PaintToolForm->TrackAlignButton->Enabled)
         {
            PaintToolForm->TrackAlignButton->Down = true;

            // "Click" it in any case to make sure we get into track align mode
            // IMPORTANT: Pretend the button was Sender - do NOT use NULL !
            PaintToolForm->TrackAlignButtonClick(PaintToolForm->TrackAlignButton);
         }
         break;
      }

      case PNT_CMD_EDIT_TRACKING:

         // This bounces us out of EDIT TRACKING mode
         PaintToolForm->TrackingButton->Down = false;
         PaintToolForm->TrackingButtonClick(NULL);
         break;

      case PNT_CMD_GO_TO_FIRST_TRACKING_FRAME:

         goToTrackingPointInsertionFrame();
			break;

		// Larry spacebar "pause/resume tracking" hack-a-rama.
		case PNT_CMD_PAUSE_OR_RESUME_TRACKING:

			if (majorState == MAJORSTATE_TRACKING_PAUSED)
			{
				getSystemAPI()->StartTracking();
			}
			else
			{
				Result = TOOL_RETURN_CODE_NO_ACTION;
			}

			break;

      default:
         // Give the Navigator tool a go at it.
			Result = TOOL_RETURN_CODE_NO_ACTION;
			break;
      }

      return Result;
   }

   // Navigator commands prefixed by CTRL-SHIFT
   if (commandNumber <= NAV_CMD_MAX_REAL_NAV_CMD)
   {
      if (majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME
      && !getSystemAPI()->getImportFrameClipName().empty())
      {
         int targetFrameIndex;
         int importFrameIndex;
         getSystemAPI()->getPaintFrames(&targetFrameIndex, &importFrameIndex);
         int importFrameOffset = getSystemAPI()->getImportFrameOffset();

         switch (commandNumber)
         {
            case NAV_CMD_JOG_FWD_1_FRAME:
               ++importFrameOffset;
               break;

            case NAV_CMD_JOG_REV_1_FRAME:
               --importFrameOffset;
               break;

            case NAV_CMD_GOTO_IN:
               importFrameOffset -= importFrameIndex;
               break;

            case NAV_CMD_GOTO_OUT:
            {
               CVideoFrameList *videoFrameList = getSystemAPI()->getImportClipVideoFrameList();
               if (!videoFrameList)
               {
                  break;
               }

               int outFrameIndex = videoFrameList->getOutFrameIndex();
               importFrameOffset += outFrameIndex - importFrameIndex;
               break;
            }

            case NAV_CMD_GOTO_MARK_IN:
            {
               int markIn, markOut;
               auto importClip = getSystemAPI()->getImportClip();
               getClipMarkIndices(importClip, markIn, markOut);
               if (markIn < 0)
               {
                  break;
               }

               importFrameOffset += markIn - importFrameIndex;
               break;
            }

            case NAV_CMD_GOTO_MARK_OUT_MINUS_ONE:
            {
               int markIn, markOut;
               auto importClip = getSystemAPI()->getImportClip();
               getClipMarkIndices(importClip, markIn, markOut);
               if (markOut < 0)
               {
                  break;
               }

               importFrameOffset += markOut - 1 - importFrameIndex;
               break;
            }
         }

         getSystemAPI()->setImportFrameOffset(importFrameOffset);
         goToImportFrame();
      }
      else if (majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE
      && !getSystemAPI()->getImportFrameClipName().empty())
      {
         int targetFrameIndex;
         int importFrameIndex;
         getSystemAPI()->getPaintFrames(&targetFrameIndex, &importFrameIndex);
         int importFrameOffset = getSystemAPI()->getImportFrameOffset();

         switch (commandNumber)
         {
            case NAV_CMD_JOG_FWD_1_FRAME:
            case NAV_CMD_JOG_REV_1_FRAME:
            case NAV_CMD_GOTO_IN:
            case NAV_CMD_GOTO_OUT:
            case NAV_CMD_GOTO_MARK_IN:
            case NAV_CMD_GOTO_MARK_OUT_MINUS_ONE:
               getSystemAPI()->executeNavigatorToolCommand(commandNumber);
               break;

            default:
               break;
         }

         setTargetFrame(getSystemAPI()->getLastFrameIndex());
         getSystemAPI()->setImportFrameOffset(importFrameOffset);
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT
      || majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT
      || majorState == MAJORSTATE_SELECTING_IMPORT_FRAME)
      {
         getSystemAPI()->executeNavigatorToolCommand(commandNumber);
      }
      else
      {
         // these are normal Navigator commands mapped in Paint
         // which cause execution of single-frame jogs fwd & rev

         // Apparently these are ok in any state...
         if ((commandNumber == NAV_CMD_JOG_FWD_1_FRAME)
         || (commandNumber == NAV_CMD_JOG_REV_1_FRAME))
         {
            getSystemAPI()->executeNavigatorToolCommand(commandNumber);
         }
      }

      return TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK;
   }

   if ((majorState == MAJORSTATE_EDIT_MASK) || (majorState == MAJORSTATE_MASK_TRANSFORM))
   {
      if ((commandNumber != PNT_CMD_EDIT_MASK)
      && (commandNumber != PNT_CMD_ENTER_TRANSFORM_MODE)
      && (commandNumber != PNT_CMD_TOGGLE_DIFFERENCE_BLEND)
      && (commandNumber != PNT_CMD_ENABLE_TOLERANCE))
      {
         return TOOL_RETURN_CODE_NO_ACTION;
      }
   }

   // COMMAND HANDLER MEGA_SWITCH!

   switch (commandNumber)
   {

   case PNT_CMD_EDIT_MASK:

      if (majorState == MAJORSTATE_EDIT_MASK)
      {
         // leave mask edit mode
         LeaveMaskRevert();
      }
      else if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         // enter mask edit mode
         SelectMask();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         SelectMaskTransform();
      }
      else if (majorState == MAJORSTATE_MASK_TRANSFORM)
      {
         SelectTransform();
      }

      goToTargetFrame();

      break;

   case PNT_CMD_EDIT_TRACKING:

      if (majorState == MAJORSTATE_REVEAL_MODE_HOME)
      {
         EnterEditTrackingState();
         goToTrackingPointInsertionFrame();
      }
      else
      {
         // IS THIS RIGHT ??? QQQ
         Result = TOOL_RETURN_CODE_NO_ACTION;
      }

      break;

      // Depending on state, toggle TRACk or AUTO align mode
   case PNT_CMD_TOGGLE_TRACK_ALIGN_MODE:

      // Haha this is bloody awful! I think this takes the interaction
      // between the tool and the form to a whole new level!!
      if (majorState == MAJORSTATE_REVEAL_MODE_HOME && PaintToolForm->TrackAlignButton->Enabled)
      {
         bool toggle = !PaintToolForm->TrackAlignButton->Down;
         PaintToolForm->TrackAlignButton->Down = toggle;
         PaintToolForm->TrackAlignButtonClick(PaintToolForm->TrackAlignButton);
      }

      break;

   case PNT_CMD_SHOW_IMPORT_FRAME:

      // NEW: ensure the import frame is loaded
      PaintToolForm->ClearLoadTimer();
      getSystemAPI()->forceTargetAndImportFrameLoaded();

      _oldPlaybackFilter = (_oldPlaybackFilter == PLAYBACK_FILTER_INVALID) ? getSystemAPI()->getPlaybackFilter() : _oldPlaybackFilter;
      getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);

      // prevent extra cycles from interfering with Navigation
      switch (majorState)
      {
         case MAJORSTATE_REVEAL_MODE_HOME:
         case MAJORSTATE_EDIT_TRANSFORM:

            // test for auto accept and do it if enabled
            AutoAcceptConditional();

            setNextMajorState(majorState);

            setMajorState(MAJORSTATE_SELECTING_IMPORT_FRAME);

            // prevent us from leaving reveal mode
            PaintToolForm->RevealButton->Enabled        = true;
            PaintToolForm->CloneButton->Enabled         = false;
            PaintToolForm->ColorButton->Enabled         = false;
            PaintToolForm->OrigValuesButton->Enabled    = false;
            PaintToolForm->AlternateClipButton->Enabled = false;

            // remove any clone cursor
            getSystemAPI()->unloadCloneBrush();

            // when navigating, import frame will be changed
            getSystemAPI()->setCaptureMode(CAPTURE_MODE_IMPORT);

            // go to it
            goToImportFrame();

            break;

         case MAJORSTATE_CLONE_MODE_HOME:

            // test for auto accept and do it if enabled
            // prev omitted and made BUG found by LC 4/7/08
            AutoAcceptConditional();

            SelectChooseClonePtAlt();

            break;

         case MAJORSTATE_COLOR_MODE_HOME:

            SelectPickColor();

            break;

         case MAJORSTATE_ORIG_MODE_HOME:

            ////////////////////////////////////////////////////
            // WORKAROUND FOR A DISPLAYER BUG                 //
            // Need to avoid running this code if the current //
            // frame has no history, else the displayer shows //
            // the wrong frickin' frame!                      //
            ////////////////////////////////////////////////////
            if (!getSystemAPI()->doesFrameHaveAnyHistory(getSystemAPI()->getLastFrameIndex()))
            {
               break;
            }
            ////////////////////////////////////////////////////
            ////////////////////////////////////////////////////

            setNextMajorState(majorState);

            setMajorState(MAJORSTATE_SELECTING_ORIG_FRAME);

            // prevent us from leaving orig mode
            PaintToolForm->RevealButton->Enabled     = false;
            PaintToolForm->CloneButton->Enabled      = false;
            PaintToolForm->ColorButton->Enabled      = false;
            PaintToolForm->OrigValuesButton->Enabled = true;
            PaintToolForm->AlternateClipButton->Enabled = false;

            // when navigating, import frame will be changed
            getSystemAPI()->setCaptureMode(CAPTURE_MODE_ORIG_IMPORT);

            // go to it
            goToImportFrame();

            break;

         case MAJORSTATE_ALTCLIP_MODE_HOME:

            // Don't do anything if no alternate clip is selected!
            if (!getSystemAPI()->getImportFrameClipName().empty())
            {
               // test for auto accept and do it if enabled
               AutoAcceptConditional();

               setNextMajorState(majorState);

               setMajorState(MAJORSTATE_SELECTING_ALTCLIP_FRAME);
               PaintToolForm->setMajorState(majorState);

               // prevent us from leaving reveal mode
               PaintToolForm->RevealButton->Enabled        = false;
               PaintToolForm->CloneButton->Enabled         = false;
               PaintToolForm->ColorButton->Enabled         = false;
               PaintToolForm->OrigValuesButton->Enabled    = false;
               PaintToolForm->AlternateClipButton->Enabled = true;

               // remove any clone cursor
               getSystemAPI()->unloadCloneBrush();

               // when navigating, import frame will be changed
               getSystemAPI()->setCaptureMode(CAPTURE_MODE_ALTCLIP_IMPORT);

               // go to it
               goToImportFrame();
            }

            break;
      }

      break;

   case PNT_CMD_RESET_IMPORT_FRAME:

      // CTRL-SHIFT-SPACE resets reference frame to
      // target in both REVEAL mode and CLONE mode
      if ((majorState == MAJORSTATE_SELECTING_IMPORT_FRAME) || (majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT))
      {
         // test for auto accept and do it if enabled
         // AutoAcceptConditional();

         goToTargetFrame();
      }

      // In ALTCLIP mode it syncs the clip timecodes.
      if (majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME
      || getMajorState() == MAJORSTATE_PAINT_ALTCLIP_COMPARE)
      {
         getSystemAPI()->syncImportFrameTimecode();
         goToImportFrame();
      }

      break;

   case PNT_CMD_TOGGLE_IMPORT_MODE:

      // CTRL-SHIFT-T toggles the import mode
      if ((majorState == MAJORSTATE_SELECTING_IMPORT_FRAME)
      || (majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT))
      {
         ToggleAbsoluteRelative();
      }

      break;

   case PNT_CMD_SHOW_TARGET_FRAME:

      if (_oldPlaybackFilter != PLAYBACK_FILTER_INVALID)
      {
         getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
         _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
      }

      if (majorState == MAJORSTATE_SELECTING_IMPORT_FRAME
      || majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME)
      {
         setMajorState(nextMajorState);

         // restore these
         PaintToolForm->RevealButton->Enabled     = true;
         PaintToolForm->CloneButton->Enabled      = true;
         PaintToolForm->ColorButton->Enabled      = true;
         PaintToolForm->OrigValuesButton->Enabled = true;
         PaintToolForm->AlternateClipButton->Enabled = true;
         // PaintToolForm->QuickAlignButton->Enabled = true;
         // PaintToolForm->TrackAlignButton->Enabled = true;

         // reload clone brush
         if (majorState == MAJORSTATE_CLONE_MODE_HOME)
         {
            ReloadCloneBrush();
         }

         if ((majorState == MAJORSTATE_REVEAL_MODE_HOME) && (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH))
         {
            EnterTrackingAlignShowBothMode();
         }
         else
         {
            // when navigating, target frame will be changed
            getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);
         }

         // go to it
         goToTargetFrame();
      }
      else if ((majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT) || (majorState == MAJORSTATE_MOVE_CLONE_PT))
      {
         EndChooseClonePtAlt();
      }
      else if (majorState == MAJORSTATE_PICK_COLOR)
      {
         EscPickColor();
      }
      else if (majorState == MAJORSTATE_SELECTING_ORIG_FRAME)
      {
         setMajorState(nextMajorState);

         // restore these
         PaintToolForm->RevealButton->Enabled     = true;
         PaintToolForm->CloneButton->Enabled      = true;
         PaintToolForm->ColorButton->Enabled      = true;
         PaintToolForm->OrigValuesButton->Enabled = true;
         PaintToolForm->AlternateClipButton->Enabled = true;

         getSystemAPI()->setCaptureMode(CAPTURE_MODE_ORIG_TARGET);

         // go to it
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME
      || getMajorState() == MAJORSTATE_PAINT_ALTCLIP_COMPARE)
      {
         setMajorState(nextMajorState);

         // restore these
         PaintToolForm->RevealButton->Enabled     = true;
         PaintToolForm->CloneButton->Enabled      = true;
         PaintToolForm->ColorButton->Enabled      = true;
         PaintToolForm->OrigValuesButton->Enabled = true;
         PaintToolForm->AlternateClipButton->Enabled = true;

         // when navigating, target frame will be changed
         getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

         // go to it
         goToTargetFrame();
      }

      break;

   case PNT_CMD_ENTER_REPOSITION_MODE:

      // NEW: ensure the import frame is loaded
      PaintToolForm->ClearLoadTimer();
      getSystemAPI()->forceTargetAndImportFrameLoaded();

      _oldPlaybackFilter = (_oldPlaybackFilter == PLAYBACK_FILTER_INVALID) ? getSystemAPI()->getPlaybackFilter() : _oldPlaybackFilter;
      getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);

      if (majorState == MAJORSTATE_REVEAL_MODE_HOME)
      {
         // test for auto accept and do it if enabled
         AutoAcceptConditional();

         // grey out these
         PaintToolForm->CloneButton->Enabled = false;
         PaintToolForm->ColorButton->Enabled = false;
         PaintToolForm->OrigValuesButton->Enabled = false;
         PaintToolForm->AlternateClipButton->Enabled = false;

         SelectReposition();

//         getSystemAPI()->setMainWindowFocus();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME)
      {
         SelectPickSeed();
      }
      else if (majorState == MAJORSTATE_PICK_SEED)
      {
         EscPickSeed();
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME)
      {
         CompareOriginalFrame();
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)
      {
         CompareAltClipFrame();
      }

      break;

   case PNT_CMD_BEG_REPOSITION:

      if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)
      {
         // mouse handler sorts 'em out
         getSystemAPI()->mouseDn();
      }
      else if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)
      {
         // mouse minor state is ready
         getSystemAPI()->mouseDn();

         // revert to edit transform
         setMajorState(MAJORSTATE_POSITION_IMPORT_FRAME);
         PaintToolForm->setMajorState(MAJORSTATE_POSITION_IMPORT_FRAME);

         // go to target frame
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_PICK_SEED)
      {
         // pass the event to the Displayer
         getSystemAPI()->mouseDn();
      }

      break;

   case PNT_CMD_BEG_MOVE_CLONE_PT:

      if (majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT)
      {
         setMajorState(MAJORSTATE_MOVE_CLONE_PT);

         // clone brush follows the mouse
         LoadCloneBrush(true, 0, 0);

         // calculate the clone pt
         tentCloneX = lastMouseFrameX;
         tentCloneY = lastMouseFrameY;

         // update the GUI
         PaintToolForm->UpdateCloneX(tentCloneX);
         PaintToolForm->UpdateCloneY(tentCloneY);

         goToImportFrame();
      }
      else if (majorState == MAJORSTATE_PICK_COLOR)
      {
         // mouse handler sorts 'em out
         getSystemAPI()->mouseDn();
      }

      break;

   case PNT_CMD_TOGGLE_CLONE_ABS_REL:

      if (majorState == MAJORSTATE_CLONE_MODE_HOME)
      {
         ToggleCloneAbsoluteRelative();
      }

      break;

   case PNT_CMD_TOGGLE_PROCESSED_ORIGINAL:

      if ((majorState == MAJORSTATE_EDIT_MASK)
      || (majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         ToggleProcessedOriginal();
		}
		else if (majorState == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED)
		{
         StartTracking();
		}
		else
      {
			Result = TOOL_RETURN_CODE_NO_ACTION;
      }

      break;

   case PNT_CMD_TOGGLE_SHOW_IMPORT_DIFFS:
      if (majorState == MAJORSTATE_ORIG_MODE_HOME || majorState == MAJORSTATE_ALTCLIP_MODE_HOME)
      {
         ToggleShowImportFrameDiffs();
      }
		else
      {
			Result = TOOL_RETURN_CODE_NO_ACTION;
      }

      break;

   case PNT_CMD_BRUSH_DISPLAY_OFF:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_EDIT_TRANSFORM)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         // '8' Key returns mouse wheel to brush radius
         PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         PaintToolForm->DensitySpeedButton->Down = false;
         PaintToolForm->GrainSpeedButton->Down   = false;

         setNextMajorState(majorState);
         setMajorState(MAJORSTATE_BRUSH_OFF);
         getSystemAPI()->enableBrushDisplay(false);
         getSystemAPI()->refreshFrameCached(true);
      }

      break;

   case PNT_CMD_BRUSH_DISPLAY_ON:

      if (majorState == MAJORSTATE_BRUSH_OFF)
      {
         setMajorState(nextMajorState);
         PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         getSystemAPI()->enableBrushDisplay(true);
         getSystemAPI()->setMainWindowFocus();
         getSystemAPI()->refreshFrameCached(true);
      }

      break;

   case PNT_CMD_ENABLE_OPACITY:

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_OPACITY);
      break;

   case PNT_CMD_ENABLE_BLEND:

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_BLEND);
      break;

   case PNT_CMD_ENABLE_ASPECT:

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_ASPECT);
      break;

   case PNT_CMD_ENABLE_ANGLE:

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_ANGLE);
      break;

   case PNT_CMD_END_REPOSITION:

      if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)
      {
         getSystemAPI()->mouseUp();
      }
      else if (majorState == MAJORSTATE_PICK_SEED)
      {
         // now examine the target frame and see if
         // it's really modified from the original
         ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
         PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
         getSystemAPI()->mouseUp();
      }

      // for some reason ALT screws up the focus, so let's just put
      // it back on RADIUS so the mousewheel doesn't go unresponsive
      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);

      break;

   case PNT_CMD_END_MOVE_CLONE_PT:

      if (majorState == MAJORSTATE_MOVE_CLONE_PT)
      {
         // as if mouse button went back up
         setMajorState(MAJORSTATE_CHOOSE_CLONE_PT_ALT);

         // tentative clone pt becomes real clone pt
         cloneX = tentCloneX;
         cloneY = tentCloneY;

         // drop the clone pt for refresh
         LoadCloneBrush(false, cloneX, cloneY);

         // update the GUI with coords
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
      }

      break;

   case PNT_CMD_LEAVE_REPOSITION_MODE:

      if (_oldPlaybackFilter != PLAYBACK_FILTER_INVALID)
      {
         getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
         _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
      }

      if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)
      {
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)
      {
         ExitFromChooseAutoAlignPointMode();

         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_SELECTING_ORIG_FRAME)
      {
         CompareOriginalFrame();
      }
      else if (getMajorState() == MAJORSTATE_PAINT_ALTCLIP_COMPARE)
      {
         CompareAltClipFrame();
      }

      // for some reason ALT screws up the focus, so let's just put
      // it back on RADIUS so the mousewheel doesn't go unresponsive
      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);

      break;

   case PNT_CMD_ENTER_TRANSFORM_MODE:

      if (((majorState == MAJORSTATE_REVEAL_MODE_HOME) && (alignState == ALIGNSTATE_MANUAL)) || (majorState == MAJORSTATE_EDIT_MASK))
      {
         // contains logic for AutoAccept
         SelectTransform();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         // same as enter
         LeaveTransformKeep();
         goToTargetFrame();
      }

      break;

   case PNT_CMD_TOGGLE_DIFFERENCE_BLEND:

      // toggle difference blend
      ToggleColorPosNeg();
      break;

   case PNT_CMD_RESET_TRANSFORM:

      // We have to check for MAJORSTATE_SELECTING_IMPORT_FRAME because
      // Larry changed the shortcut to be Ctrl-Shift-C and when you press
      // the Ctrl+Shift you go into MAJORSTATE_SELECTING_IMPORT_FRAME. D'oh!
      if (majorState == MAJORSTATE_EDIT_TRANSFORM ||
         (majorState == MAJORSTATE_SELECTING_IMPORT_FRAME && nextMajorState == MAJORSTATE_EDIT_TRANSFORM))
      {
         getSystemAPI()->ClearTransform(true);
      }
      else if ((majorState == MAJORSTATE_REVEAL_MODE_HOME ||
            (majorState == MAJORSTATE_SELECTING_IMPORT_FRAME && nextMajorState == MAJORSTATE_REVEAL_MODE_HOME))
         && alignState == ALIGNSTATE_MANUAL)
      {
         getSystemAPI()->ClearTransform(false);
      }

      break;

   case PNT_CMD_LEAVE_MODE_REVERT:

      if (majorState == MAJORSTATE_EDIT_MASK)
      {
         LeaveMaskRevert();
      }
      else if ((majorState == MAJORSTATE_EDIT_TRANSFORM) || (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT))
      {
         LeaveTransformRevert();
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT)
      {
         EscChooseClonePt();
      }

      break;

   case PNT_CMD_LEAVE_MODE_KEEP:

      if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         LeaveTransformKeep();
         goToTargetFrame();
      }

      break;

   case PNT_CMD_BEG_POSITION_PAINT:

      // we're UNSHIFTED if we get here
      mouseConstraint = MOUSE_CONSTRAINT_NONE;

      // when doing Get Original Values, left-mouse-butn does a 'G'
      if (majorState == MAJORSTATE_GET_ORIGINAL_VALUES)
      {
         // restores majorState to REVEAL, CLONE, or COLOR
         getSystemAPI()->AcceptGOV();
      }

      // NEW: ensure the import frame is loaded
      PaintToolForm->ClearLoadTimer();
      getSystemAPI()->forceTargetAndImportFrameLoaded();

      // now pick up with the above-possibly-modified majorState
      if ((majorState == MAJORSTATE_EDIT_MASK) || (majorState == MAJORSTATE_MASK_TRANSFORM))
      {
         // pass the event thru to MaskTool
         Result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

      BeginPaintOrQuickErase();

      break;

   case PNT_CMD_BEG_QUICK_ERASE:

      // NEW: ensure the import frame is loaded
      PaintToolForm->ClearLoadTimer();
      getSystemAPI()->forceTargetAndImportFrameLoaded();
      SelectProcessed();

      // now pick up with the above-possibly-modified majorState
      if ((majorState == MAJORSTATE_EDIT_MASK) || (majorState == MAJORSTATE_MASK_TRANSFORM))
      {
         // pass the event thru to MaskTool
         Result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

      quickEraseStartIsPending = true;

      break;

   case PNT_CMD_END_QUICK_ERASE:
   case PNT_CMD_END_POSITION_PAINT:
   case PNT_CMD_END_WIDGET_SCALE:
   case PNT_CMD_END_WIDGET_SKEW:

      // when editing the Mask, pass this thru
      if ((majorState == MAJORSTATE_EDIT_MASK) || (majorState == MAJORSTATE_MASK_TRANSFORM))
      {
         // pass the event thru to MaskTool
         Result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

      // Broke out some code here so it can be called by NotifyRightClicked
      HandleSomeButtonUps();

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         // Line up the brush with the Borland cursor position -
         // the net effect of this code is that after the constrained
         // stroke, the curso pops to where Borland thinks the cursor
         // should be instead of staying at the end of the completed
         // stroke... oh well...
         userInputWindowX = lastMouseWindowX = realMouseWindowX;
         userInputWindowY = lastMouseWindowY = realMouseWindowY;
         userInputFrameX  = lastMouseFrameX = realMouseFrameX;
         userInputFrameY  = lastMouseFrameY = realMouseFrameY;
         getSystemAPI()->mouseMv(userInputWindowX, userInputWindowY);
      }

      if ((commandNumber == PNT_CMD_END_QUICK_ERASE)
      && ((prevBrushMode == BRUSH_MODE_THRU) || (prevBrushMode == BRUSH_MODE_ORIG)))
      {
         SelectThru();
      }

      break;

   case PNT_CMD_UNDO_LAST_STROKE:

      PopStrokesToTarget(1);
      break;

   case PNT_CMD_REPEAT_LAST_STROKES:
      {
         // set the transform which obtained at the time of the
         // stroke provided 'Use Current Transform' is unchecked
         //
         bool setXfm = !PaintToolForm->CurTransformCheckBox->Checked;
         PlayCapturedStroke(setXfm);
      }

      break;

   case PNT_CMD_POSITION_LFT:
   case PNT_CMD_SHIFT_POS_LFT:
   case PNT_CMD_CTRL_POS_LFT:
   case PNT_CMD_ALT_POS_LFT:
   case PNT_CMD_ALT_SHIFT_POS_LFT:
   case PNT_CMD_ALT_CTRL_POS_LFT:

      ArrowKeyMouseWheel(ARROW_LFT);
      break;

   case PNT_CMD_POSITION_RGT:
   case PNT_CMD_SHIFT_POS_RGT:
   case PNT_CMD_CTRL_POS_RGT:
   case PNT_CMD_ALT_POS_RGT:
   case PNT_CMD_ALT_SHIFT_POS_RGT:
   case PNT_CMD_ALT_CTRL_POS_RGT:

      ArrowKeyMouseWheel(ARROW_RGT);
      break;

   case PNT_CMD_POSITION_UP:
   case PNT_CMD_SHIFT_POS_UP:
   case PNT_CMD_CTRL_POS_UP:
   case PNT_CMD_ALT_POS_UP:
   case PNT_CMD_ALT_SHIFT_POS_UP:
   case PNT_CMD_ALT_CTRL_POS_UP:

      ArrowKeyMouseWheel(ARROW_UP);
      break;

   case PNT_CMD_POSITION_DN:
   case PNT_CMD_SHIFT_POS_DN:
   case PNT_CMD_CTRL_POS_DN:
   case PNT_CMD_ALT_POS_DN:
   case PNT_CMD_ALT_SHIFT_POS_DN:
   case PNT_CMD_ALT_CTRL_POS_DN:

      ArrowKeyMouseWheel(ARROW_DN);
      break;

   case PNT_CMD_SELECT_REVEAL_MODE:

      SelectRevealMode();
      break;

   case PNT_CMD_SELECT_CLONE_MODE:

      SelectCloneMode();
      break;

   case PNT_CMD_SELECT_COLOR_MODE:

      SelectColorMode();
      break;

   case PNT_CMD_SELECT_ORIG_OR_ALTCLIP_MODE:

      if (majorState == MAJORSTATE_EDIT_MASK)
      {
         LeaveMaskRevert();
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_MASK_TRANSFORM)
      {
         SelectTransform();
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME)
      {
         SelectAltClipMode();
      }
      else
      {
         SelectOrigValuesMode();
      }

      break;

   case PNT_CMD_SELECT_THRU:

      SelectThru();
      break;

   case PNT_CMD_SELECT_ERASE:

      SelectErase();
      break;

   case PNT_CMD_SELECT_HEAL:

      // SelectHeal();
      break;

   case PNT_CMD_REJECT_TARGET_FRAME:

      RejectProcessedTargetFrame();
      break;

	case PNT_CMD_ACCEPT_TARGET_FRAME:

		if (getSystemAPI()->targetFrameIsModified())
		{
			// pending macro becomes previous
			RenamePendingAsPrevious();
		}

      // refreshes the target frame after accept
		AcceptProcessedTargetFrame(true);

      break;

   case PNT_CMD_ACCEPT_TARGET_IN_PLACE:

      if (getSystemAPI()->targetFrameIsModified())
      {
         // pending macro becomes previous
			RenamePendingAsPrevious();
      }

      // don't refresh target frame inside here
		AcceptProcessedTargetFrame(false);

      // refresh the frame you're ON
      getSystemAPI()->refreshFrame();

      break;

      // these commands control the mode of widget editing

   case PNT_CMD_BEG_WIDGET_SCALE:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         // Hahaha wtf - this is really a PNT_CMD_BEG_POSITION_PAINT
         // with X constraint!

         mouseConstraint = MOUSE_CONSTRAINT_X;

         // This code stolen from PNT_CMD_BEG_POSITION_PAINT case!

         // when doing Get Original Values, left-mouse-butn does a 'G'
         if (majorState == MAJORSTATE_GET_ORIGINAL_VALUES)
         {
            // restores majorState to REVEAL, CLONE, or COLOR
            getSystemAPI()->AcceptGOV();
         }

         // NEW: ensure the import frame is loaded
         PaintToolForm->ClearLoadTimer();
         getSystemAPI()->forceTargetAndImportFrameLoaded();

         BeginPaintOrQuickErase();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         getSystemAPI()->TransformShiftMouseDown();
      }
      else if ((majorState == MAJORSTATE_EDIT_MASK) || (majorState == MAJORSTATE_MASK_TRANSFORM))
      {
         // pass the event thru to MaskTool
         Result = TOOL_RETURN_CODE_NO_ACTION;
      }
      else if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)
      {
         if (alignState == ALIGNSTATE_MANUAL)
         {
            // pass the event to the Displayer
            // to execute the autoalignment
            getSystemAPI()->mouseDn();

            // revert to position-import-frame or edit-transform
            setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
            PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

            // go to target frame
            goToTargetFrame();
         }
      }
      else if ((majorState == MAJORSTATE_PICK_COLOR) || (majorState == MAJORSTATE_PICK_SEED))
      {
         // pass the event to the Displayer
         getSystemAPI()->mouseDn();
      }

      break;

   case PNT_CMD_BEG_WIDGET_SKEW:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         // Hahaha wtf - this is really a PNT_CMD_BEG_POSITION_PAINT
         // with Y constraint!

         mouseConstraint = MOUSE_CONSTRAINT_Y;

         // This code stolen from PNT_CMD_BEG_POSITION_PAINT case!

         // when doing Get Original Values, left-mouse-butn does a 'G'
         if (majorState == MAJORSTATE_GET_ORIGINAL_VALUES)
         {
            // restores majorState to REVEAL, CLONE, or COLOR
            getSystemAPI()->AcceptGOV();
         }

         // NEW: ensure the import frame is loaded
         PaintToolForm->ClearLoadTimer();
         getSystemAPI()->forceTargetAndImportFrameLoaded();

         BeginPaintOrQuickErase();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         getSystemAPI()->TransformCtrlMouseDown();
      }

      break;

   case PNT_CMD_CYCLE_BRUSH_FWD:

      CycleBrush(1);
      break;

   case PNT_CMD_CYCLE_BRUSH_BWD:

      CycleBrush(-1);
      break;

   case PNT_CMD_TOGGLE_ELLIPSE_RECT:

      PaintToolForm->ToggleEllipseRect();
      break;

   case PNT_CMD_EXECUTE_RANGE_MACRO:

      ExecuteRangeMacro();
      break;

   case PNT_CMD_SUSPEND_RANGE_MACRO:

      SuspendRangeMacro();
      break;

      // these commands are for constraining the mouse during a stroke

   case PNT_CMD_ENABLE_CONSTRAINT_X: // Shift key

      mixDelta = 10;
      positionDelta         = 25; // fifth  pixels
      autoAlignBoxSizeDelta = 8;
      toleranceDelta        = 10;
      brushRadiusFactor     = 1.0;
      brushRadiusDelta      = 10; // single pixels
      brushOpacityDelta     = 10;
      brushBlendDelta       = 10;
      brushAspectDelta      = 10;
      brushAngleDelta       = 10;
      brushDensityDelta     = 10;
      brushGrainDelta       = 10;

      // if we're already painting, cloning or coloring, start constraining
      if ((majorState == MAJORSTATE_PAINT_PIXELS)
      || (majorState == MAJORSTATE_CLONE_PIXELS)
      || (majorState == MAJORSTATE_COLOR_PIXELS)
      || (majorState == MAJORSTATE_PAINT_ORIG_PIXELS)||
          (majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS))
      {
         mouseConstraint = MOUSE_CONSTRAINT_X;
      }

      break;

   case PNT_CMD_ENABLE_CONSTRAINT_Y: // Ctrl key

      mixDelta = 1;
      positionDelta         = 1; // fifth  pixels
      autoAlignBoxSizeDelta = 1;
      toleranceDelta        = 1;
      brushRadiusFactor     = 1.0;
      brushRadiusDelta      = 1; // single pixels
      brushOpacityDelta     = 1;
      brushBlendDelta       = 1;
      brushAspectDelta      = 1;
      brushAngleDelta       = 1;
      brushDensityDelta     = 1;
      brushGrainDelta       = 1;

      // if we're already painting, cloning or coloring, start constraining
      if ((majorState == MAJORSTATE_PAINT_PIXELS)
      || (majorState == MAJORSTATE_CLONE_PIXELS)
      || (majorState == MAJORSTATE_COLOR_PIXELS)
      || (majorState == MAJORSTATE_PAINT_ORIG_PIXELS)||
          (majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS))
      {
         mouseConstraint = MOUSE_CONSTRAINT_Y;
      }

      break;

   case PNT_CMD_DISABLE_CONSTRAINT_X:
   case PNT_CMD_DISABLE_CONSTRAINT_Y:

      mixDelta = 5;
      positionDelta         = 5; // fifth  pixels
      autoAlignBoxSizeDelta = 4;
      toleranceDelta        = 5;
      brushRadiusFactor     = 1.1;
      brushRadiusDelta      = 0; // single pixels
      brushOpacityDelta     = 5;
      brushBlendDelta       = 5;
      brushAspectDelta      = 5;
      brushAngleDelta       = 5;
      brushDensityDelta     = 5;
      brushGrainDelta       = 5;

      // It's OK to kill constraints here, painting will continue
      // unconstrainedly (note: only PAINT/CLONE/COLOR_PIXEL state
      // are actually valid here!)
      if ((majorState == MAJORSTATE_PAINT_PIXELS)
      || (majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_PIXELS)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_PIXELS)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_PAINT_ORIG_PIXELS)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)||
          (majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS))
      {
         mouseConstraint = MOUSE_CONSTRAINT_NONE;
      }

      break;

   case PNT_CMD_ENABLE_TOLERANCE: // the CTRL-T / 5 hotkey

      if ((majorState == MAJORSTATE_COLOR_MODE_HOME) || (majorState == MAJORSTATE_PICK_SEED))
      {
         // causes call to ToleranceTrackBarEnter
         PaintToolForm->ToleranceTrackBar->SetFocus();
      }
      else if (majorState == MAJORSTATE_REVEAL_MODE_HOME)
      {
         if (alignState == ALIGNSTATE_MANUAL)
         {
            // contains logic for AutoAccept
            SelectTransform();
            goToTargetFrame();
         }
         else if (alignState == ALIGNSTATE_TRACK_SHOWING_TARGET)
         {
            // contains logic for AutoAccept
            EnterTrackingAlignShowBothMode();

            // Sync the GUI
            PaintToolForm->PositionImportButton->Down = true;
         }
         else if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH)
         {
            // contains logic for AutoAccept
            LeaveTrackingAlignShowBothMode();

            // Sync the GUI
            PaintToolForm->PositionImportButton->Down = false;
         }

         // Shouldn't be necessary - key input should work with tool palette focused QQQ
         getSystemAPI()->setMainWindowFocus();
     }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM)
      {
         // same as enter
         LeaveTransformKeep();
         goToTargetFrame();

         // Shouldn't be necessary - key input should work with tool palette focused QQQ
         getSystemAPI()->setMainWindowFocus();
      }
      else if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)
      {
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         goToTargetFrame();

         // Shouldn't be necessary - key input should work with tool palette focused QQQ
         getSystemAPI()->setMainWindowFocus();
      }
      else if (majorState == MAJORSTATE_EDIT_MASK)
      {
         SelectMaskTransform();
         goToTargetFrame();

         // Shouldn't be necessary - key input should work with tool palette focused QQQ
         getSystemAPI()->setMainWindowFocus();
      }
      else if (majorState == MAJORSTATE_MASK_TRANSFORM)
      {
         SelectMask();
         setNextMajorState(MAJORSTATE_REVEAL_MODE_HOME);
         goToTargetFrame();

         // Shouldn't be necessary - key input should work with tool palette focused QQQ
         getSystemAPI()->setMainWindowFocus();
      }

      break;

   case PNT_CMD_TOGGLE_AUTO_ALIGN_MODE:

      // If we are in TRACKING ALIGN mode, jump out of that
      if (isTrackingAlignmentEnabled())
      {
         // Uggggh.
         PaintToolForm->TrackAlignButton->Down = false;
         PaintToolForm->TrackAlignButtonClick(PaintToolForm->TrackAlignButton);
      }

      ToggleChooseAutoAlignPointMode();

      break;

   case PNT_CMD_SELECT_DENSITY_ADJUST:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         if (mouseWheelState != MOUSE_WHEEL_ADJUST_DENSITY)
         {
            SelectDensityAdjust();
         }
         else if (mouseWheelState == MOUSE_WHEEL_ADJUST_DENSITY)
         {
            EscDensityAdjust();
         }
      }

      break;

   case PNT_CMD_RESET_DENSITY:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         SetDensityStrength(0);
      }

      break;

   case PNT_CMD_SELECT_GRAIN_ADJUST:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         if (mouseWheelState != MOUSE_WHEEL_ADJUST_GRAIN)
         {
            SelectGrainAdjust();
         }
         else if (mouseWheelState == MOUSE_WHEEL_ADJUST_GRAIN)
         {
            EscGrainAdjust();
         }
      }

      break;

   case PNT_CMD_RESET_GRAIN:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         SetGrainStrength(0);
      }

      break;

   case PNT_CMD_RESET_DENSITY_OR_GRAIN:

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
      || (majorState == MAJORSTATE_CLONE_MODE_HOME)
      || (majorState == MAJORSTATE_COLOR_MODE_HOME)
      || (majorState == MAJORSTATE_ORIG_MODE_HOME)
      || (majorState == MAJORSTATE_ALTCLIP_MODE_HOME))
      {
         if (mouseWheelState == MOUSE_WHEEL_ADJUST_DENSITY)
         {
            SetDensityStrength(0);
         }
         else if (mouseWheelState == MOUSE_WHEEL_ADJUST_GRAIN)
         {
            SetGrainStrength(0);
         }
      }

      break;

   case PNT_CMD_MASK_SELECT_ALL_REGIONS:

      if (majorState == MAJORSTATE_EDIT_MASK)
      {
         // Pass the input to the mask tool!
         Result = TOOL_RETURN_CODE_NO_ACTION;
      }

      break;

      // these commands are needed because the Mask pulldown may be used
      // TTT review this

   case NAV_CMD_MASK_TOOL_ENABLE:

      Result = TOOL_RETURN_CODE_NO_ACTION;
      break;

   case NAV_CMD_MASK_TOOL_DISABLE:

      if (majorState == MAJORSTATE_EDIT_MASK)
      {
         PaintToolForm->setMaskState(false);
         setMajorState(MAJORSTATE_EDIT_TRANSFORM);
         PaintToolForm->setMajorState(MAJORSTATE_EDIT_TRANSFORM);
      }

      Result = TOOL_RETURN_CODE_NO_ACTION;
      break;

   default:

      Result = TOOL_RETURN_CODE_NO_ACTION;
      break;
   }

   if (needToClearProgressMonitor && (Result & TOOL_RETURN_CODE_EVENT_CONSUMED))
   {
      needToClearProgressMonitor = false;
      GetToolProgressMonitor()->SetIdle(false);
   }

   return Result;

} // onToolCommand

void CPaintTool::BeginPaintOrQuickErase()
{
   if ((majorState == MAJORSTATE_PICK_COLOR)||
       (majorState == MAJORSTATE_PICK_SEED)) {

      // pass the event to the Displayer
      getSystemAPI()->mouseDn();
   }

   if (showState == SHOW_PROCESSED) {

#ifdef TRY_TO_WARP_CURSOR

      resetLastMousePos = true;

#else

      // reset mouse coord state in case we are operating under constraints
      // (avoid drawing from the end of the previous stroke if we moved the
      // cursor since then while holding down the SHIFT or CTRL keys
      userInputWindowX = lastMouseWindowX = realMouseWindowX;
      userInputWindowY = lastMouseWindowY = realMouseWindowY;
      userInputFrameX  = lastMouseFrameX  = realMouseFrameX;
      userInputFrameY  = lastMouseFrameY  = realMouseFrameY;

#endif

      if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

         // logic for Auto Accept
         int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
         if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
            _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2763");
         }
#else
         getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

         if (!autoAccept&&(currentFrame != targetFrame)) {

            // go back and refresh the true target frame
            getSystemAPI()->goToFrameSynchronous(targetFrame);
         }
         else {

				if (autoAccept&&(currentFrame != targetFrame)) {

					// pending.str becomes previous.str
					RenamePendingAsPrevious();

					// write target frame to media
					AcceptProcessedTargetFrame(false);

               // reload the current frame from media
               getSystemAPI()->loadTargetAndImportFrames(currentFrame);
            }

            // get the new target and import frames

#ifdef INSTR
            if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
               _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2786");
            }
#else
            getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
            // the user can see where he's paint-
            // ing, so it's okay to initiate it
            setMajorState(MAJORSTATE_PAINT_PIXELS);

            // opens stroke capture channel for append
            begStrokeFileLength = OpenPendingStrokeChannelForWrite();

            // init to capture stroke pts
            ResetBackgroundPainter();

            // queue up first pt & paint the brush there
            BeginStroke(lastMouseFrameX, lastMouseFrameY);

            // set the density strength
            getSystemAPI()->setDensityStrength(brushDensityStrength);

            // set the grain strength
            getSystemAPI()->setGrainStrength(brushGrainStrength);

            // pass the event to the Displayer
            getSystemAPI()->mouseDn();
         }
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         // logic for Auto Accept
         int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
         if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
            _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2818");
         }
#else
         getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
         if (!autoAccept&&(currentFrame != targetFrame)) {

            getSystemAPI()->goToFrameSynchronous(targetFrame);
         }
         else {

				if (autoAccept&&(currentFrame != targetFrame)) {

					// pending.str becomes previous.str
					RenamePendingAsPrevious();

					// write target frame to media
					AcceptProcessedTargetFrame(false);

               // reload the current frame from media
               getSystemAPI()->loadTargetAndImportFrames(currentFrame);
            }

#ifdef INSTR
            if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
               _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2838");
            }
#else
            getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
            // the user can see where he's paint-
            // ing, so it's okay to initiate it
            setMajorState(MAJORSTATE_CLONE_PIXELS);

            // opens stroke capture channel for append
            begStrokeFileLength = OpenPendingStrokeChannelForWrite();

            // init to capture stroke pts
            ResetBackgroundPainter();

            if (captureOffsetOnce||captureOffset) { // begin stroke

               // frame coordinates of the mouse
               captureX = lastMouseFrameX;
               captureY = lastMouseFrameY;

               // the offset of the clone pt from the capture pt
               clOffsetX = cloneX - captureX;
               clOffsetY = cloneY - captureY;

               // offset the import frame accordingly
               getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

               // clone brush now relative
               LoadCloneBrush(true, clOffsetX, clOffsetY);

               captureOffsetOnce = false;
            }

            // queue up first pt & paint the brush there
            BeginStroke(lastMouseFrameX, lastMouseFrameY);

            // set the density strength
            getSystemAPI()->setDensityStrength(brushDensityStrength);

            // set the grain strength
            getSystemAPI()->setGrainStrength(brushGrainStrength);

            // pass the event to the Displayer
            getSystemAPI()->mouseDn();
         }
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {

         // finished navigating to import frame
         getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

         // drop the clone brush here
         cloneX = lastMouseFrameX;
         cloneY = lastMouseFrameY;

         // update the GUI
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);

         // final position of clone brush before paint operation
         LoadCloneBrush(false, cloneX, cloneY);

         // get ready to capture the offset
         captureOffsetOnce = PaintToolForm->CloneBrushIsRelative();
         captureOffset     = !captureOffsetOnce;

         // load the paint brush
         PaintToolForm->LoadBrush();

         // back to clone or erase
         // turn these on
         PaintToolForm->RevealButton->Enabled = true;
         PaintToolForm->ColorButton->Enabled  = true;
         PaintToolForm->OrigValuesButton->Enabled    = true;
         PaintToolForm->AlternateClipButton->Enabled = true;
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // reset the button
         PaintToolForm->SetClonePointSpeedButton->Down = false;

         // reset the message
         PaintToolForm->UpdateCloneMessage(MAJORSTATE_CLONE_MODE_HOME);

         // resets Displayer to "Acquiring Pixels"
         //getSystemAPI()->paintPixels();
         getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

         // back to target frame
         goToTargetFrame();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

         // logic for Auto Accept
         int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
         if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
            _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2931");
         }
#else
         getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
         if (!autoAccept&&(currentFrame != targetFrame)) {

            getSystemAPI()->goToFrameSynchronous(targetFrame);
         }
         else {

            if (autoAccept&&(currentFrame != targetFrame)) {

					// pending.str becomes previous.str
					RenamePendingAsPrevious();

               // write target frame to media
					AcceptProcessedTargetFrame(false);

               // reload the current frame from media
               getSystemAPI()->loadTargetAndImportFrames(currentFrame);

#ifdef INSTR
               if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
                  _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2950");
               }
#else
               getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
            }

            // the user can see where he's paint-
            // ing, so it's okay to initiate it
            setMajorState(MAJORSTATE_COLOR_PIXELS);

            // open stroke capture channel for append
            begStrokeFileLength = OpenPendingStrokeChannelForWrite();

            // init to capture stroke pts
            ResetBackgroundPainter();

            // queue up first pt & paint the brush there
            BeginStroke(lastMouseFrameX, lastMouseFrameY);

            // set the density strength
            getSystemAPI()->setDensityStrength(brushDensityStrength);

            // set the grain strength
            getSystemAPI()->setGrainStrength(brushGrainStrength);

            // pass the event to the Displayer
            getSystemAPI()->mouseDn();
         }
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

         // logic for Auto Accept
         int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
         if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
            _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2983");
         }
#else
         getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
         if (!autoAccept&&(currentFrame != targetFrame)) {

            // go back and refresh the true target frame
            getSystemAPI()->goToFrameSynchronous(targetFrame);
         }
         else {

            if (autoAccept&&(currentFrame != targetFrame)) {

					// pending.str becomes previous.str
					RenamePendingAsPrevious();

               // write target frame to media
					AcceptProcessedTargetFrame(false);

               // reload the current frame from media
               getSystemAPI()->loadTargetAndImportFrames(currentFrame);
            }

#ifdef INSTR
            if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
               _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 3004");
            }
#else
            getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
            // the user can see where he's paint-
            // ing, so it's okay to initiate it
            setMajorState(MAJORSTATE_PAINT_ORIG_PIXELS);

            // opens stroke capture channel for append
            begStrokeFileLength = OpenPendingStrokeChannelForWrite();

            // init to capture stroke pts
            ResetBackgroundPainter();

            // queue up first pt & paint the brush there
            BeginStroke(lastMouseFrameX, lastMouseFrameY);

            // set the density strength
            getSystemAPI()->setDensityStrength(brushDensityStrength);

            // set the grain strength
            getSystemAPI()->setGrainStrength(brushGrainStrength);

            // pass the event to the Displayer
            getSystemAPI()->mouseDn();
         }
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

         // logic for Auto Accept
         int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
         if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
            _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2763");
         }
#else
         getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

         if (!autoAccept&&(currentFrame != targetFrame)) {

            // go back and refresh the true target frame
            getSystemAPI()->goToFrameSynchronous(targetFrame);
         }
         else {

				if (autoAccept&&(currentFrame != targetFrame)) {

					// pending.str becomes previous.str
					RenamePendingAsPrevious();

					// write target frame to media
					AcceptProcessedTargetFrame(false);

               // reload the current frame from media
               getSystemAPI()->loadTargetAndImportFrames(currentFrame);
            }

            // get the new target and import frames

#ifdef INSTR
            if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
               _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 2786");
            }
#else
            getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
            // the user can see where he's paint-
            // ing, so it's okay to initiate it
            setMajorState(MAJORSTATE_PAINT_ALTCLIP_PIXELS);

            // opens stroke capture channel for append
            begStrokeFileLength = OpenPendingStrokeChannelForWrite();

            // init to capture stroke pts
            ResetBackgroundPainter();

            // queue up first pt & paint the brush there
            BeginStroke(lastMouseFrameX, lastMouseFrameY);

            // set the density strength
            getSystemAPI()->setDensityStrength(brushDensityStrength);

            // set the grain strength
            getSystemAPI()->setGrainStrength(brushGrainStrength);

            // pass the event to the Displayer
            getSystemAPI()->mouseDn();
         }
      }
      else if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME) {

         // pass the event to the Displayer
         getSystemAPI()->TransformPositionImportFrame();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

         // pass the event to Transform Editor
         getSystemAPI()->TransformMouseDown();
      }
      else if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT) {

         // pass the event to the Displayer
         // to execute the autoalignment
         getSystemAPI()->mouseDn();

         ExitFromChooseAutoAlignPointMode();
      }
   }
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: PaintTool Tool's Mouse Move Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CPaintTool::onMouseMove(CUserInput &userInput)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   int retVal = TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK;

   // must keep cursor from refreshing during SHIFT+G render
   if (rangeMacroExecuting)
      return retVal;

   // Save real mouse coords (other variables get messed up by constraints
   realMouseWindowX = userInput.windowX;
   realMouseFrameX = (int)(getSystemAPI()->dscaleXClientToFrame(userInput.windowX) + 0.5);
   realMouseWindowY = userInput.windowY;
   realMouseFrameY = (int)(getSystemAPI()->dscaleYClientToFrame(userInput.windowY) + 0.5);

   // mouse constraints - only restricted during REVEAL, CLONE, & COLOR home;
   // resetLastMousePos is a hack for handling mouse-up/mouse-down while
   // shift or ctrl is down
   if (resetLastMousePos || (mouseConstraint != MOUSE_CONSTRAINT_Y)) {
      userInputWindowX = realMouseWindowX;
      userInputFrameX = realMouseFrameX;
      whereBorlandCursorShouldBeX = Mouse->CursorPos.x;
   }
   if (resetLastMousePos || (mouseConstraint != MOUSE_CONSTRAINT_X)) {
      userInputWindowY = realMouseWindowY;
      userInputFrameY = realMouseFrameY;
      whereBorlandCursorShouldBeY = Mouse->CursorPos.y;
   }
   resetLastMousePos = false;

   if (showState == SHOW_PROCESSED) {

      if ((majorState == MAJORSTATE_PAINT_PIXELS)||
          (majorState == MAJORSTATE_CLONE_PIXELS)||
          (majorState == MAJORSTATE_COLOR_PIXELS)||
          (majorState == MAJORSTATE_PAINT_ORIG_PIXELS)||
          (majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS)) {

         int delta = abs(userInputFrameX - lastMouseFrameX) +
                     abs(userInputFrameY - lastMouseFrameY);

         strokeSizeThreshold =
            getStrokeSizeThreshold(PaintToolForm->GetCurrentBrush()->getRadius());

         if (delta >= strokeSizeThreshold) {

            QueueUpStrokePoint(userInputFrameX, userInputFrameY, MID_STROKE);

            if (majorState == MAJORSTATE_CLONE_PIXELS) {

               // update the clone pt coordinates
               PaintToolForm->UpdateCloneX(userInputFrameX + clOffsetX);
               PaintToolForm->UpdateCloneY(userInputFrameY + clOffsetY);
            }
         }

         // show cursor motion
         getSystemAPI()->mouseMv(userInputWindowX, userInputWindowY);
      }
      else if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME) {

         // use the machinery of Paint I for simple
         // onionskin displacement. Displayer has a
         // callback to Transform editor which xmits
         // the current offsets
         //
         getSystemAPI()->mouseMv(userInput.windowX, userInput.windowY);
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

         getSystemAPI()->TransformMouseMove(userInput.windowX, userInput.windowY);
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         if (captureOffsetOnce||captureOffset) { // not captured yet

            // set the capture coords
            captureX = userInputFrameX;
            captureY = userInputFrameY;

            // the offset of the clone pt from the capture pt
            clOffsetX = cloneX - captureX;
            clOffsetY = cloneY - captureY;

            // update the offset of clone pt from brush
            PaintToolForm->UpdateCloneDelX(clOffsetX);
            PaintToolForm->UpdateCloneDelY(clOffsetY);
         }
         else {

            // update the clone pt coordinates
            PaintToolForm->UpdateCloneX(userInputFrameX + clOffsetX);
            PaintToolForm->UpdateCloneY(userInputFrameY + clOffsetY);
         }

         getSystemAPI()->mouseMv(userInput.windowX, userInput.windowY);
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {

         // drop the clone brush here
         cloneX = userInputFrameX;
         cloneY = userInputFrameY;

         // update the GUI
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);

         getSystemAPI()->mouseMv(userInput.windowX, userInput.windowY);
      }
      else if (majorState == MAJORSTATE_MOVE_CLONE_PT) {

         // a new tentative clone pt
         tentCloneX = userInputFrameX;
         tentCloneY = userInputFrameY;

         // update the GUI
         PaintToolForm->UpdateCloneX(tentCloneX);
         PaintToolForm->UpdateCloneY(tentCloneY);

         getSystemAPI()->mouseMv(userInput.windowX, userInput.windowY);
      }
      else if ((majorState == MAJORSTATE_PICK_COLOR)||
               (majorState == MAJORSTATE_PICK_SEED)) {

         getSystemAPI()->mouseMv(userInput.windowX, userInput.windowY);
      }
      else
         retVal = TOOL_RETURN_CODE_NO_ACTION;

   }      // if show processed
   else { // show original

      retVal = TOOL_RETURN_CODE_NO_ACTION;
   }

   // update last mouse window coords
   lastMouseWindowX = userInputWindowX;
   lastMouseWindowY = userInputWindowY;

   // update last mouse frame coords
   lastMouseFrameX = userInputFrameX;
   lastMouseFrameY = userInputFrameY;

   return(retVal);
}

//--------------------------------------------------------------------------
void CPaintTool::ArrowKeyMouseWheel(int dir)
{
   int mixDel              = mixDelta;
   int positionDel         = positionDelta;
   int autoAlignBoxSizeDel = autoAlignBoxSizeDelta;
   int toleranceDel        = toleranceDelta;
   float brushRadiusFact   = brushRadiusFactor;
   int brushRadiusDel      = brushRadiusDelta;
   int brushOpacityDel     = brushOpacityDelta;
   int brushBlendDel       = brushBlendDelta;
   int brushAspectDel      = brushAspectDelta;
   int brushAngleDel       = brushAngleDelta;
   int brushDensityDel     = brushDensityDelta;
   int brushGrainDel       = brushGrainDelta;
   if ((dir == ARROW_DN)||(dir == ARROW_LFT)) {
      mixDel              = -mixDelta;
      positionDel         = -positionDelta;
      autoAlignBoxSizeDel = -autoAlignBoxSizeDelta;
      toleranceDel        = -toleranceDelta;
      brushRadiusFact     = 1.0/brushRadiusFact;
      brushRadiusDel      = -brushRadiusDelta;
      brushOpacityDel     = -brushOpacityDelta;
      brushBlendDel       = -brushBlendDelta;
      brushAspectDel      = -brushAspectDelta;
      brushAngleDel       = -brushAngleDelta;
      brushDensityDel     = -brushDensityDelta;
      brushGrainDel       = -brushGrainDelta;
   }

   switch(mouseWheelState) {

      case MOUSE_WHEEL_ADJUST_MIX :

         PaintToolForm->MixingTrackBar->Position += mixDel;

      break;

      case MOUSE_WHEEL_ADJUST_ORIG_MIX :

         PaintToolForm->OrigMixingTrackBar->Position += mixDel;

      break;

      case MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX :

         PaintToolForm->AltClipMixingTrackBar->Position += mixDel;

      break;

      case MOUSE_WHEEL_ADJUST_OFFSET :

         if ((dir == ARROW_UP)||(dir == ARROW_DN)) {
            getSystemAPI()->moveImportFrame(0, -positionDel);
         }
         else if ((dir == ARROW_LFT)||(dir == ARROW_RGT)) {
            getSystemAPI()->moveImportFrame(positionDel, 0);
         }

      break;

      case MOUSE_WHEEL_ADJUST_AUTOALIGN_BOX :

         ChangeAutoAlignBoxSize(autoAlignBoxSizeDel);

      break;

      case MOUSE_WHEEL_ADJUST_TOLERANCE :

         PaintToolForm->ToleranceTrackBar->Position += toleranceDel;

      break;

      case MOUSE_WHEEL_ADJUST_RADIUS :

         PaintToolForm->ChangeRadius(brushRadiusFact, brushRadiusDel);

      break;

      case MOUSE_WHEEL_ADJUST_OPACITY :

         PaintToolForm->ChangeOpacity(brushOpacityDel);

      break;

      case MOUSE_WHEEL_ADJUST_BLEND :

         PaintToolForm->ChangeBlend(brushBlendDel);

      break;

      case MOUSE_WHEEL_ADJUST_ASPECT :

         PaintToolForm->ChangeAspect(brushAspectDel);

      break;

      case MOUSE_WHEEL_ADJUST_ANGLE :

         PaintToolForm->ChangeAngle(brushAngleDel);

      break;

      case MOUSE_WHEEL_ADJUST_DENSITY :

         if (majorState != MAJORSTATE_ORIG_MODE_HOME
         || majorState != MAJORSTATE_ALTCLIP_MODE_HOME) {

            SetDensityStrength(brushDensityStrength + brushDensityDel);
         }

      break;

      case MOUSE_WHEEL_ADJUST_GRAIN :

         if (majorState != MAJORSTATE_ORIG_MODE_HOME
         || majorState != MAJORSTATE_ALTCLIP_MODE_HOME) {

            SetGrainStrength(brushGrainStrength + brushGrainDel);
         }

      break;

      default:

      break;
   }
}
//--------------------------------------------------------------------------

void CPaintTool::CycleBrush(int delta)
{
   int brush = PaintToolForm->GetCurrentBrushIndex();
   brush = (brush+delta);
   if (brush <           0) brush = NUM_BRUSH-1;
   if (brush > NUM_BRUSH-1) brush = 0;
   PaintToolForm->SetCurrentBrush(brush);
}
//--------------------------------------------------------------------------

void CPaintTool::ToggleChooseAutoAlignPointMode()
{
   if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT) {

      ExitFromChooseAutoAlignPointMode();
   }
   else if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

      // test for auto accept and do it if enabled
      AutoAcceptConditional();

      prevShowState = showState;

      getSystemAPI()->SaveTransform();

      setMajorState(MAJORSTATE_CHOOSE_AUTOALIGN_PT);
      PaintToolForm->setMajorState(MAJORSTATE_CHOOSE_AUTOALIGN_PT);

      // State to go to when we exit from the current one
      setNextMajorState(MAJORSTATE_REVEAL_MODE_HOME);

      // need this to have the mouse wheel work
//      getSystemAPI()->setMainWindowFocus();

      goToTargetFrame();
   }
   else if ((majorState == MAJORSTATE_POSITION_IMPORT_FRAME)||
            (majorState == MAJORSTATE_EDIT_TRANSFORM)) {

      setMajorState(MAJORSTATE_CHOOSE_AUTOALIGN_PT);
      PaintToolForm->setMajorState(MAJORSTATE_CHOOSE_AUTOALIGN_PT);

      // Always go to the 'position' state when done because the user is
      // holding down the Alt key and will get there anyhow, but we want
		// to avoid flashing the yellow transform editor.
      setNextMajorState(MAJORSTATE_POSITION_IMPORT_FRAME);

      // need this to have the mouse wheel work
//      getSystemAPI()->setMainWindowFocus();

      goToTargetFrame();
   }
}

void CPaintTool::ChangeAutoAlignBoxSize(int sizedelta)
{
   autoAlignBoxSize += sizedelta;
   if (autoAlignBoxSize < AUTOALIGN_BOX_MIN)
      autoAlignBoxSize = AUTOALIGN_BOX_MIN;
   else if (autoAlignBoxSize > AUTOALIGN_BOX_MAX)
      autoAlignBoxSize = AUTOALIGN_BOX_MAX;

   getSystemAPI()->setAutoAlignBoxSize(autoAlignBoxSize);

   goToTargetFrame();
}

void CPaintTool::ExitFromChooseAutoAlignPointMode()
{
   if (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT) {

      setMajorState(nextMajorState);
      PaintToolForm->setMajorState(nextMajorState);

      goToTargetFrame();
   }
}
//--------------------------------------------------------------------------

void CPaintTool::InitClonePt()
{
   // set the clone coords in the middle of the frame
   cloneX = (curFrameRect.left + curFrameRect.right + 1)/2;
   cloneY = (curFrameRect.top + curFrameRect.bottom + 1)/2;

   // and set up with some modest offsets
   clOffsetX = (curFrameRect.right + 1 - curFrameRect.left)/16;
   clOffsetY = 0;
}

void CPaintTool::SelectChooseClonePt()
{
   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      // turn these off
      PaintToolForm->RevealButton->Enabled = false;
      PaintToolForm->ColorButton->Enabled  = false;
      PaintToolForm->OrigValuesButton->Enabled    = false;
      PaintToolForm->AlternateClipButton->Enabled = false;

      // enter mode of choosing clone pt
      setMajorState(MAJORSTATE_CHOOSE_CLONE_PT);

      // speed button goes down
      PaintToolForm->SetClonePointSpeedButton->Down = true;

      // post message "Choose clone point"
      PaintToolForm->UpdateCloneMessage(majorState);

      // follow the cursor
      LoadCloneBrush(true, 0, 0);

      // no raster cursor
      //getSystemAPI()->trackCloneBrush();
      getSystemAPI()->setMouseMinorState(TRACKING_CLONE_PT);

      // when navigating, import frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_IMPORT);

      // go to it
      goToImportFrame();
   }
   else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {

      // select pt button already down
      EscChooseClonePt();
   }
   else if ((majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT)||
            (majorState == MAJORSTATE_MOVE_CLONE_PT)) {

      PaintToolForm->SetClonePointSpeedButton->Down = true;
   }
}

void CPaintTool::EscChooseClonePt()
{
   if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {

      // turn 'em back on
      PaintToolForm->RevealButton->Enabled = true;
      PaintToolForm->ColorButton->Enabled  = true;
      PaintToolForm->OrigValuesButton->Enabled    = true;
      PaintToolForm->AlternateClipButton->Enabled = true;

      // exit mode of choosing clone pt
      setMajorState(MAJORSTATE_CLONE_MODE_HOME);

      // speed button goes back up
      PaintToolForm->SetClonePointSpeedButton->Down = false;

      // post message "Clone pixels"
      PaintToolForm->UpdateCloneMessage(majorState);

      // no capture is called for
      captureOffsetOnce = captureOffset = false;

      // reset the import frame offsets
      clOffsetX = 0; clOffsetY = 0;
      getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

      // update the GUI with offsets
      PaintToolForm->UpdateCloneDelX(clOffsetX);
      PaintToolForm->UpdateCloneDelY(clOffsetY);

      // follow the cursor
      LoadCloneBrush(true, 0, 0);

      // back to normal
      //getSystemAPI()->paintPixels();
      getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::SelectChooseClonePtAlt()
{
   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      // turn these off
      PaintToolForm->RevealButton->Enabled = false;
      PaintToolForm->ColorButton->Enabled  = false;
      PaintToolForm->OrigValuesButton->Enabled    = false;
      PaintToolForm->AlternateClipButton->Enabled = false;

      // enter mode of choosing clone pt
      setMajorState(MAJORSTATE_CHOOSE_CLONE_PT_ALT);

      // speed button goes down
      PaintToolForm->SetClonePointSpeedButton->Down = true;

      // post message "Choose clone point"
      PaintToolForm->UpdateCloneMessage(majorState);

      // display the clone brush @ (cloneX, cloneY)
      LoadCloneBrush(false, cloneX, cloneY);

      // no raster cursor
      //getSystemAPI()->trackCloneBrush();
      getSystemAPI()->setMouseMinorState(TRACKING_CLONE_PT);

      // when navigating, import frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_IMPORT);

      // go to it
      goToImportFrame();
   }
}

void CPaintTool::EndChooseClonePtAlt()
{
   // ALT key up before mouse button up
   if (majorState == MAJORSTATE_MOVE_CLONE_PT) {

      // as if mouse button went back up
      setMajorState(MAJORSTATE_CHOOSE_CLONE_PT_ALT);

      // tentative clone pt becomes real clone pt
      cloneX = tentCloneX;
      cloneY = tentCloneY;

      // drop the clone pt for refresh
      LoadCloneBrush(false, cloneX, cloneY);

      // update the GUI with coords
      PaintToolForm->UpdateCloneX(cloneX);
      PaintToolForm->UpdateCloneY(cloneY);
   }

   // now we're ready to end non-sticky mode
   if (majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT) {

      // turn these back on
      PaintToolForm->RevealButton->Enabled = true;
      PaintToolForm->ColorButton->Enabled  = true;
      PaintToolForm->OrigValuesButton->Enabled    = true;
      PaintToolForm->AlternateClipButton->Enabled = true;

      // exit mode of choosing clone pt
      setMajorState(MAJORSTATE_CLONE_MODE_HOME);

      // speed button goes back up
      PaintToolForm->SetClonePointSpeedButton->Down = false;

      // post message "Clone pixels"
      PaintToolForm->UpdateCloneMessage(majorState);

      // get ready to capture the offset
      captureOffsetOnce = PaintToolForm->CloneBrushIsRelative();
      captureOffset     = !captureOffsetOnce;

      // back to normal
      //getSystemAPI()->paintPixels();
      getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::SelectPickColor()
{
   if (majorState == MAJORSTATE_PICK_SEED) {

      EscPickSeed();
   }
   if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

      // turn these off
      PaintToolForm->RevealButton->Enabled = false;
      PaintToolForm->CloneButton->Enabled  = false;
      PaintToolForm->OrigValuesButton->Enabled    = false;
      PaintToolForm->AlternateClipButton->Enabled = false;

      // enter mode of picking color
      setMajorState(MAJORSTATE_PICK_COLOR);

      // speed button goes down
      PaintToolForm->DropperSpeedButton->Down = true;

      // update the message
      PaintToolForm->UpdateColorMessage(majorState);

      // display the dropper
      getSystemAPI()->setMouseMinorState(PICKING_COLOR);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // Change cursor to eyedropper
      getSystemAPI()->enterCursorOverrideMode(PickerCursor());

      // QQQ THIS WASN'T HERE - was it supposed to be?
      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::EscPickColor()
{
   if (majorState == MAJORSTATE_PICK_COLOR) {

      // turn 'em back on
      PaintToolForm->RevealButton->Enabled = true;
      PaintToolForm->CloneButton->Enabled  = true;
      PaintToolForm->OrigValuesButton->Enabled    = true;
      PaintToolForm->AlternateClipButton->Enabled = true;

      // exit mode of choosing clone pt
      setMajorState(MAJORSTATE_COLOR_MODE_HOME);

      // speed button goes back up
      PaintToolForm->DropperSpeedButton->Down = false;

      // back to "paint pixels" or "erase pixels"
      PaintToolForm->UpdateColorMessage(majorState);

      // back to normal
      getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // Restore default cursor
      getSystemAPI()->exitCursorOverrideMode();

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::SelectPickSeed()
{
   if (majorState == MAJORSTATE_PICK_SEED) {

      EscPickSeed();
   }
   if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

      // test for auto accept and do it if enabled
      AutoAcceptConditional();

      // turn these off
      PaintToolForm->RevealButton->Enabled = false;
      PaintToolForm->CloneButton->Enabled  = false;
      PaintToolForm->OrigValuesButton->Enabled    = false;
      PaintToolForm->AlternateClipButton->Enabled = false;

      PaintToolForm->ToleranceTrackBar->Enabled = true;

      // enter mode of picking color
      setMajorState(MAJORSTATE_PICK_SEED);

      // speed button goes down
      PaintToolForm->BucketFillSpeedButton->Down = true;

      // update the message
      PaintToolForm->UpdateColorMessage(majorState);

      // set the tolerance
      getSystemAPI()->setFillTolerance(PaintToolForm->GetFillTolerance());

      // clear the fill seed
      getSystemAPI()->clearFillSeed();

      // display the fill brush
      getSystemAPI()->setMouseMinorState(PICKING_SEED);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::EscPickSeed()
{
   if (majorState == MAJORSTATE_PICK_SEED) {

      // turn 'em back on
      PaintToolForm->RevealButton->Enabled = true;
      PaintToolForm->CloneButton->Enabled  = true;
      PaintToolForm->OrigValuesButton->Enabled    = true;
      PaintToolForm->AlternateClipButton->Enabled = true;
      PaintToolForm->ToleranceTrackBar->Enabled = true;

      // exit mode of choosing clone pt
      setMajorState(MAJORSTATE_COLOR_MODE_HOME);

      // speed button goes back up
      PaintToolForm->BucketFillSpeedButton->Down = false;

      // back to "paint pixels" or "erase pixels"
      PaintToolForm->UpdateColorMessage(majorState);

      // clear fill seed so trackbar adjs do nothing
      getSystemAPI()->clearFillSeed();

      // back to normal
      getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      // when navigating, target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::PositionImportFrame()
{
   if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

      if (alignState == ALIGNSTATE_TRACK_SHOWING_TARGET) {

         // contains logic for AutoAccept
         EnterTrackingAlignShowBothMode();
      }
      else if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {

         // contains logic for AutoAccept
         LeaveTrackingAlignShowBothMode();
      }
      else { // (alignState == ALIGNSTATE_MANUAL)

         // contains logic for AutoAccept
         SelectTransform();

         goToTargetFrame();
      }
   }
   else if ((majorState == MAJORSTATE_EDIT_TRANSFORM)||
            (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)) {

      // same as enter
      LeaveTransformKeep();

      goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_EDIT_MASK) {

      SelectMaskTransform();

      goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_MASK_TRANSFORM) {

      SelectMask();

      setNextMajorState(MAJORSTATE_REVEAL_MODE_HOME);

      goToTargetFrame();
	}
	else if (majorState == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED)
	{
		// WTF?? QQQ
		setNextMajorState(majorState);

      goToTargetFrame();
   }

   // set focus to main window
   getSystemAPI()->setMainWindowFocus();
}

void CPaintTool::SelectReposition()
{
   // NEW: ensure the import frame is loaded
   PaintToolForm->ClearLoadTimer();
   getSystemAPI()->forceTargetAndImportFrameLoaded();

   setMajorState(MAJORSTATE_POSITION_IMPORT_FRAME);
   PaintToolForm->setMajorState(MAJORSTATE_POSITION_IMPORT_FRAME);

   goToTargetFrame();
}

void CPaintTool::SelectTransform()
{
   // NEW: ensure the import frame is loaded
   PaintToolForm->ClearLoadTimer();
   getSystemAPI()->forceTargetAndImportFrameLoaded();

   if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

      PaintToolForm->PositionImportButton->Down = true;
   }
   else if (((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
             (alignState == ALIGNSTATE_MANUAL))||
            (majorState == MAJORSTATE_MASK_TRANSFORM)) {

      // test for auto accept and do it if enabled
      AutoAcceptConditional();

      prevShowState = showState;

      getSystemAPI()->SaveTransform();

      setMajorState(MAJORSTATE_EDIT_TRANSFORM);
      PaintToolForm->setMajorState(MAJORSTATE_EDIT_TRANSFORM);
   }

   goToTargetFrame();
}

void CPaintTool::LeaveTransformRevert()
{
   getSystemAPI()->RestoreTransform();

   setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
   PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

   if (prevShowState == SHOW_ORIGINAL)
      SelectOriginal();
   else if (prevShowState == SHOW_PROCESSED)
      SelectProcessed();

   goToTargetFrame();
}

void CPaintTool::LeaveTransformKeep(bool refresh)
{
   setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
   PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

   if (prevShowState == SHOW_ORIGINAL)
      SelectOriginal();
   else if (prevShowState == SHOW_PROCESSED)
      SelectProcessed();

   if (refresh)
      goToTargetFrame();
}

void CPaintTool::CompareOriginalFrame()
{
   if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

      setNextMajorState(majorState);

      setMajorState(MAJORSTATE_SELECTING_ORIG_FRAME);

      // prevent us from leaving orig mode
      PaintToolForm->RevealButton->Enabled     = false;
      PaintToolForm->CloneButton->Enabled      = false;
      PaintToolForm->ColorButton->Enabled      = false;
      PaintToolForm->OrigValuesButton->Enabled = true;
      PaintToolForm->AlternateClipButton->Enabled = false;

      PaintToolForm->CompareSpeedButton->Down    = true;
      PaintToolForm->OrigDiffSkinButton->Enabled = true;
      PaintToolForm->OrigMixSkinButton->Enabled  = true;
      PaintToolForm->OrigMixingTrackBar->Enabled = true;
      PaintToolForm->OrigLabel->Enabled          = true;
      PaintToolForm->CurLabel->Enabled           = true;

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_ORIG_MIX);

      getSystemAPI()->setOnionSkinMode(origValuesModeOnionSkinMode);

      // Turn off magenta overlay.
      _savedRedOverlayState = getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS;
      if (_savedRedOverlayState)
      {
         getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
      }

      // go to it
      goToImportFrame();
   }
   else if (majorState == MAJORSTATE_SELECTING_ORIG_FRAME) {

      setMajorState(nextMajorState);

      // restore these
      PaintToolForm->RevealButton->Enabled        = true;
      PaintToolForm->CloneButton->Enabled         = true;
      PaintToolForm->ColorButton->Enabled         = true;
      PaintToolForm->OrigValuesButton->Enabled    = true;
      PaintToolForm->AlternateClipButton->Enabled = true;

      PaintToolForm->CompareSpeedButton->Down     = false;
      PaintToolForm->OrigDiffSkinButton->Enabled  = false;
      PaintToolForm->OrigMixSkinButton->Enabled   = false;
      PaintToolForm->OrigMixingTrackBar->Enabled  = false;
      PaintToolForm->OrigLabel->Enabled           = false;
      PaintToolForm->CurLabel->Enabled            = false;

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);

      //getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::CompareAltClipFrame()
{
   if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME
   || majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME) {

      setNextMajorState(majorState);

      setMajorState(MAJORSTATE_PAINT_ALTCLIP_COMPARE);

      // prevent us from leaving orig mode
      PaintToolForm->RevealButton->Enabled          = false;
      PaintToolForm->CloneButton->Enabled           = false;
      PaintToolForm->ColorButton->Enabled           = false;
      PaintToolForm->OrigValuesButton->Enabled      = false;
      PaintToolForm->AlternateClipButton->Enabled   = true;

      PaintToolForm->AltClipCompareButton->Down     = true;
      PaintToolForm->AltClipMixButton->Enabled      = true;
      PaintToolForm->AltClipDiffButton->Enabled     = true;
      PaintToolForm->AltClipMixingTrackBar->Enabled = true;
      PaintToolForm->AltClipSrcLabel->Enabled       = true;
      PaintToolForm->AltClipTgtLabel->Enabled       = true;

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX);

      getSystemAPI()->setOnionSkinMode(altClipModeOnionSkinMode);

      // when navigating, both target frame and import frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_BOTH);

      // go to it
      goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE) {

      setMajorState(nextMajorState);

      // restore these
      PaintToolForm->RevealButton->Enabled          = true;
      PaintToolForm->CloneButton->Enabled           = true;
      PaintToolForm->ColorButton->Enabled           = true;
      PaintToolForm->OrigValuesButton->Enabled      = true;
      PaintToolForm->AlternateClipButton->Enabled   = true;

      PaintToolForm->AltClipCompareButton->Down     = false;
      PaintToolForm->AltClipMixButton->Enabled      = false;
      PaintToolForm->AltClipDiffButton->Enabled     = false;
      PaintToolForm->AltClipMixingTrackBar->Enabled = false;
      PaintToolForm->AltClipSrcLabel->Enabled       = false;
      PaintToolForm->AltClipTgtLabel->Enabled       = false;

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);

      // when navigating, the target frame will be changed
      getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      //getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

      // go to it
      goToTargetFrame();
   }
}

void CPaintTool::SelectMask()
{
   if (majorState == MAJORSTATE_EDIT_MASK) {

      //PaintToolForm->MaskButton->Down = true;
   }
   else {

      setNextMajorState(majorState);
      prevShowState = showState;
      setMajorState(MAJORSTATE_EDIT_MASK);
      PaintToolForm->setMajorState(MAJORSTATE_EDIT_MASK);
   }

   goToTargetFrame();
}

void CPaintTool::LeaveMaskRevert(bool refresh)
{
   int curframe = getSystemAPI()->getLastFrameIndex();
   getSystemAPI()->CalculateMaskROI(curframe);

   setMajorState(nextMajorState);
   PaintToolForm->setMajorState(nextMajorState);

   if (prevShowState == SHOW_ORIGINAL)
      SelectOriginal();
   else if (prevShowState == SHOW_PROCESSED)
      SelectProcessed();

   if (refresh)
      getSystemAPI()->refreshFrameCached(true);
}

void CPaintTool::SelectMaskQuickly()
{
   if (majorState != MAJORSTATE_EDIT_MASK) {

      //PaintToolForm->setMajorState(MAJORSTATE_EDIT_MASK);
      setMajorState(MAJORSTATE_EDIT_MASK);
   }
}

void CPaintTool::SelectMaskTransform()
{
   if ((majorState == MAJORSTATE_EDIT_TRANSFORM)||
       (majorState == MAJORSTATE_EDIT_MASK)) {

      setMajorState(MAJORSTATE_MASK_TRANSFORM);
      PaintToolForm->setMajorState(MAJORSTATE_MASK_TRANSFORM);
   }
}

void CPaintTool::EnterTrackingAlignShowBothMode()
{
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
             (alignState == ALIGNSTATE_TRACK_SHOWING_TARGET));

   // test for auto accept and do it if enabled
   AutoAcceptConditional();

   setAlignState(ALIGNSTATE_TRACK_SHOWING_BOTH);
   PaintToolForm->setAlignState(ALIGNSTATE_TRACK_SHOWING_BOTH);

   // when navigating, we'll show both import and target frames
   getSystemAPI()->setCaptureMode(CAPTURE_MODE_BOTH);
   getSystemAPI()->setOnionSkinMode(revealModeOnionSkinMode);

   // refresh the display
   getSystemAPI()->refreshFrameCached(true);
}

void CPaintTool::LeaveTrackingAlignShowBothMode()
{
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
             (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH));

   // test for auto accept and do it if enabled
   AutoAcceptConditional();

   setAlignState(ALIGNSTATE_TRACK_SHOWING_TARGET);
   PaintToolForm->setAlignState(ALIGNSTATE_TRACK_SHOWING_TARGET);

   // go back to normal display
   getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);
   getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

   // refresh the display
   getSystemAPI()->refreshFrameCached(true);
}

void CPaintTool::SelectDensityAdjust()
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_DENSITY);
   }
   else if (majorState == MAJORSTATE_ORIG_MODE_HOME||
         (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      PaintToolForm->DensitySpeedButton->Down = false;
   }
}

void CPaintTool::EscDensityAdjust()
{
   PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
}

void CPaintTool::SelectGrainAdjust()
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_GRAIN);
   }
   else if (majorState == MAJORSTATE_ORIG_MODE_HOME||
         (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      PaintToolForm->GrainSpeedButton->Down = false;
   }
}

void CPaintTool::EscGrainAdjust()
{
   PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
}

string CPaintTool::GetBinPath()
{
   return clipBinPath;
}

string CPaintTool::GetClipName()
{
   return clipName;
}

void CPaintTool::PopStrokesToTarget(int strcnt)
{
   int macrofileleng = -1;

   // NEW: don't change the state of the GUI because of popping the stroke
   // stack - gets too confusing when there are both CLONE and REVEAL mode
   // strokes stacked up
   STRCTXT theContext;         // Don't use global savedContext
   PaintToolForm->InhibitGuiUpdateHackFlag = true;
   GetStrokeContext(theContext);

   if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

      getSystemAPI()->SaveTransform();

      // pop the specified number of strokes
      macrofileleng = getSystemAPI()->popStrokesToTarget(strcnt);

      getSystemAPI()->RestoreTransform();
   }
   else {

      // pop the specified number of strokes
      macrofileleng = getSystemAPI()->popStrokesToTarget(strcnt);
   }

   // Restore context that matches GUI state
   LoadStrokeContext(theContext);
   PaintToolForm->InhibitGuiUpdateHackFlag = false;

   // if appropriate, pop strokes from macro file
   if (macrofileleng != -1) {

      RewindPendingStrokeCaptureChannel(macrofileleng);

      PaintToolForm->PlayStrokeSpeedButton->Enabled = (macrofileleng > 0)||
                                       IsPreviousStrokeChannelNonEmpty();
   }

   // now examine the target frame and see if
   // it's really modified from the original.
   // Enable/disable the Accept & Reject buttons
   // accordingly
   ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
   PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

   // refresh the target frame
   goToTargetFrame();
}

//--------------------------------------------------------------------------
// stroke segments are drawn between consecutive
// mouse pts (stroke pts). Here we capture them
//
void CPaintTool::QueueUpStrokePoint(int x, int y, int typ)
{
   while ((strFill - strEmpty) >= MAX_STROKE_POINTS) MTImillisleep(1);

   int index = strFill%MAX_STROKE_POINTS;

   strokeQueue[index].x      = x;
   strokeQueue[index].y      = y;
   strokeQueue[index].header = typ;

   strFill++;
}

void CPaintTool::BeginStroke(int x, int y)
{
   // this will result in drawing at a
   // single brush position [x, y]
   lastStrokeX = x;
   lastStrokeY = y;

   if (strCapture) {

      // wait for completion (important for next step)
      while (strEmpty < strFill) MTImillisleep(1);

      // the FIFO is now empty - ready for the new context
      STRCTXT theContext;
      GetStrokeContext(theContext);

      // capture it into the MACRO file
      CaptureStrokeContext(theContext);
   }

   // NOW the first point into the fifo
   QueueUpStrokePoint(x, y, BEG_STROKE);
}

void CPaintTool::EndStroke(int x, int y)
{
   // QueueUpStrokePoint(x, y, MID_STROKE);
   QueueUpStrokePoint(x, y, END_STROKE);
}

void CPaintTool::AbortStrokePainting()
{
   getSystemAPI()->enableBrushDisplay(false);

   strEmpty = strFill;
}

////////////////////////////////////////////////////////////////////////////////

void CPaintTool::OnLoadNewTargetFrame()
{
   // rewind the pending-strokes dataset
   RewindPendingStrokeCaptureChannel(0);

   // grey out these buttons
   PaintToolForm->UndoLastButton->Enabled       = false;
   PaintToolForm->UndoThruSelectButton->Enabled = false;
   PaintToolForm->AcceptButton->Enabled         = false;
   PaintToolForm->RejectButton->Enabled         = false;
}

int CPaintTool::RewindPendingStrokeCaptureChannel(int newleng)
{
   int retVal = 0;

   retVal = truncate64(pendingFilename.c_str(), newleng);

   PaintToolForm->RangeStrokeSpeedButton->Enabled = (newleng != 0)&&MarksAreValid();

   return retVal;
}

int CPaintTool::OpenPendingStrokeChannelForWrite()
{
   if ((pndStrFileHandle = open(pendingFilename.c_str(), O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1)
      return(-1);

   // open for append
   pndStrFilePtr    = lseek64(pndStrFileHandle, 0, SEEK_END);

   // set ptr to top of buf
   pndStrBufPtr = pndStrBuf;

   // set ptr to end of buf
   pndStrBufEnd = pndStrBufPtr + MAX_MACRO_POINTS;

   // turn this on
   strCapture = true;

   return(pndStrFilePtr);
}

void CPaintTool::GetStrokeContext(STRCTXT& strctx)
{
   strctx.header = STROKE_CONTEXT;

   strctx.paintMode = paintMode;

   int trg, imp;
   getSystemAPI()->getPaintFrames(&trg, &imp);

   strctx.importMode        = getSystemAPI()->getImportMode();
   strctx.importFrame       = getSystemAPI()->getImportFrameTimecode();
   strctx.importFrameOffset = getSystemAPI()->getImportFrameOffset();

   getSystemAPI()->GetTransform(&strctx.rotation,
                                &strctx.stretchX,
                                &strctx.stretchY,
                                &strctx.skew,
                                &strctx.offsetX,
                                &strctx.offsetY);

   strctx.currentBrush = PaintToolForm->GetCurrentBrushIndex();

   strctx.brushMode = PaintToolForm->GetBrushMode();

   CBrush *curbrush = getSystemAPI()->getPaintBrush(&strctx.darkerthreshold,
                                                    &strctx.lighterthreshold,
                                                    &strctx.usingColor,
                                                    strctx.color);
   if (curbrush != NULL) {
      strctx.radius  = curbrush->getRadius();
      strctx.blend   = curbrush->getBlend();
      strctx.opacity = curbrush->getOpacity();
      strctx.aspect  = curbrush->getAspect();
      strctx.angle   = curbrush->getAngle();
      strctx.shape   = curbrush->getShape();
      strctx.type    = curbrush->getType();
   }
}

int CPaintTool::CaptureStrokeContext(STRCTXT& strctx)
{
   if (pndStrBufPtr > (pndStrBufEnd - sizeof(STRCTXT))) {

      int icnt = (pndStrBufPtr - pndStrBuf);
      if (write(pndStrFileHandle,pndStrBuf,icnt)!=icnt)
         return -1;
      pndStrBufPtr = pndStrBuf;
   }

   *((STRCTXT *)pndStrBufPtr) = strctx;
   pndStrBufPtr  += sizeof(STRCTXT);
   pndStrFilePtr += sizeof(STRCTXT);

   return(0);
}

int CPaintTool::CaptureStrokePoint(STRPNT& strpt)
{
   if (pndStrBufPtr > (pndStrBufEnd - sizeof(STRPNT))) {

      int icnt = (pndStrBufPtr - pndStrBuf);
      if (write(pndStrFileHandle,pndStrBuf,icnt)!=icnt)
         return -1;
      pndStrBufPtr = pndStrBuf;
   }

   *((STRPNT *)pndStrBufPtr) = strpt;
   pndStrBufPtr  += sizeof(STRPNT);
   pndStrFilePtr += sizeof(STRPNT);

   return(0);
}

int  CPaintTool::ClosePendingStrokeChannelAfterWrite()
{
   int retVal = 0;

   // write out residue in buf
   int icnt = (pndStrBufPtr - pndStrBuf);
   int ocnt = write(pndStrFileHandle, pndStrBuf, icnt);
   if (ocnt!=icnt)
      retVal |= 1;

   // close the file
   if (close(pndStrFileHandle) != 0)
      retVal |= 2;

   // indicator
   pndStrFileHandle = -1;

   // turn this off
   strCapture = false;

   if (pndStrFilePtr > 0) {
      PaintToolForm->PlayStrokeSpeedButton->Enabled  = true;
      PaintToolForm->RangeStrokeSpeedButton->Enabled = MarksAreValid();
   }

   return retVal;
}

int CPaintTool::OpenPreviousStrokeChannelForRead()
{
   if ((prvStrFileHandle = open(previousFilename.c_str(), O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
      return(-1);

   prvStrFileLength = lseek64(prvStrFileHandle, 0, SEEK_END);
   prvStrFilePtr    = lseek64(prvStrFileHandle, 0, SEEK_SET);

   int icnt = prvStrFileLength - prvStrFilePtr;
   if (icnt > (int)(MAX_MACRO_POINTS*sizeof(STRPNT)))
      icnt = MAX_MACRO_POINTS*sizeof(STRPNT);
   int ocnt = read(prvStrFileHandle,(MTI_UINT8 *)prvStrBuf,icnt);
   if (ocnt != icnt)
      return -1; // error

   prvStrBufPtr = prvStrBuf;
   prvStrBufEnd = prvStrBuf + ocnt;

   return(prvStrFileLength);
}

bool CPaintTool::MarksAreValid()
{
	strBegFrame = getSystemAPI()->getMarkIn();
	strEndFrame = getSystemAPI()->getMarkOut();

	return ((strBegFrame != -1)&&(strEndFrame != -1)&&(strBegFrame < strEndFrame));
}

bool CPaintTool::IsPreviousStrokeChannelNonEmpty()
{
	// open the file
	if ((prvStrFileHandle = open(previousFilename.c_str(), O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
		return(false);

	// check its length
   bool retVal = (lseek64(prvStrFileHandle, 0, SEEK_END) > 0);

   // close it up
   close(prvStrFileHandle);

   // indicate whether there's a macro to play
   // FUCK NO! CALLER MUST SET IT! PaintToolForm->PlayStrokeSpeedButton->Enabled  = retVal;

   return retVal;
}

bool CPaintTool::LoadStrokeContext(STRCTXT& strctx, bool setxfm)
{
   // return value TRUE if stroke is to create history
   bool retVal = (strctx.paintMode != PAINT_MODE_ORIG);

   if (paintMode != strctx.paintMode) { // need to switch modes
      SelectPaintModeFast(strctx.paintMode);
   }

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
		 (majorState == MAJORSTATE_ORIG_MODE_HOME)||
       (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

       // need an import frame

      int targetFrame, importFrame, tentativeImportFrame;

#ifdef INSTR
      if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
         _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 4332");
      }
#else
		getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

      double rot, strx, stry, skew, offx, offy;
      getSystemAPI()->GetTransform(&rot, &strx, &stry, &skew, &offx, &offy);

      if (!setxfm) { // don't set xform from stroke context

          // replace the xform from the stroke context
          // with the xform which currently obtains
          strctx.rotation = rot;
          strctx.stretchX = strx;
          strctx.stretchY = stry;
          strctx.skew     = skew;
          strctx.offsetX  = offx;
          strctx.offsetY  = offy;
      }

      if (strctx.importMode == IMPORT_MODE_RELATIVE
      || strctx.importMode == IMPORT_MODE_ALTCLIP)
      {
         tentativeImportFrame = targetFrame + strctx.importFrameOffset;
      }
      else if (strctx.importMode == IMPORT_MODE_ABSOLUTE)
      {
         tentativeImportFrame = strctx.importFrame;
      }
      else if (strctx.importMode == IMPORT_MODE_ORIGINAL)
      {
         tentativeImportFrame = targetFrame;
      }

//      DBCOMMENT("");
//      DBTRACE(targetFrame);
//      DBTRACE(importFrame);
//      DBTRACE(tentativeImportFrame);
//		DBTRACE(revealModeImportFrame);

      if ((importFrame != tentativeImportFrame)||
          (strctx.rotation != rot)||
          (strctx.stretchX != strx)||
          (strctx.stretchY != stry)||
          (strctx.skew != skew)||
          (strctx.offsetX != offx)||
          (strctx.offsetY != offy)) {

//         DBCOMMENT("PRELOAD");
         getSystemAPI()->preloadImportFrame(tentativeImportFrame,
                                            strctx.importMode,
                                            strctx.rotation,
                                            strctx.stretchX, strctx.stretchY,
                                            strctx.skew,
                                            strctx.offsetX, strctx.offsetY);
      }
      else {
         // work around a bug in the Displayer where the offset gets out
         // of synch with the import/target frame -- QQQ SHOULD FIX DISPLAYER
         //getSystemAPI()->setImportFrameOffset(strctx.importFrameOffset);
         getSystemAPI()->setImportFrameTimecode(tentativeImportFrame);
//         DBCOMMENT("WORKAROUND");
      }

      getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
//    	DBTRACE(importFrame);
   }

   CBrush *curbrush = PaintToolForm->GetBrush(strctx.currentBrush);

   curbrush->setBrushParameters(maxBrushRadius,
                                strctx.radius,
                                strctx.blend,
                                strctx.opacity,
                                strctx.aspect,
                                strctx.angle,
                                strctx.shape,
                                strctx.type);

   // very important to do this here to get
   // the brush conistent with parameters
   curbrush->getWeight(0,0);

   float *color = (strctx.usingColor? strctx.color: NULL);
   getSystemAPI()->loadPaintBrush(curbrush,
                                  strctx.brushMode,
                                  strctx.darkerthreshold,
                                  strctx.lighterthreshold,
                                  color);
   return retVal;
}

int CPaintTool::PlayStrokeFileToken(bool setxfm)
{
   if ( ((((STRPNT  *)prvStrBufPtr)->header & (BEG_STROKE+MID_STROKE+END_STROKE))&&(prvStrBufPtr > (prvStrBufEnd - sizeof(STRPNT)) )) ||
        ((((STRCTXT *)prvStrBufPtr)->header & STROKE_CONTEXT)&&(prvStrBufPtr > (prvStrBufEnd - sizeof(STRCTXT))))) {

      prvStrFilePtr += prvStrBufPtr - prvStrBuf;
      int icnt = prvStrFileLength - prvStrFilePtr;
      if (icnt == 0)
         return -1; // done

      if (icnt > (int)(MAX_MACRO_POINTS*sizeof(STRPNT)))
         icnt = MAX_MACRO_POINTS*sizeof(STRPNT);
      lseek64(prvStrFileHandle, prvStrFilePtr, SEEK_SET);
      int ocnt = read(prvStrFileHandle,(MTI_UINT8 *)prvStrBuf,icnt);
      if (ocnt != icnt)
         return -1; // error

      prvStrBufPtr = prvStrBuf;
      prvStrBufEnd = prvStrBuf + ocnt;
   }

   if (((STRCTXT *)prvStrBufPtr)->header & STROKE_CONTEXT )  {

      // wait for completion (important for next step)
      //while (strEmpty < strFill) MTImillisleep(1);

      begStrokeFileLength = pndStrFilePtr;

      // load the new STROKE CONTEXT
      LoadStrokeContext(*(STRCTXT *)prvStrBufPtr, setxfm);

      if (strCapture)
         CaptureStrokeContext(*(STRCTXT *)prvStrBufPtr);

      prvStrBufPtr += sizeof(STRCTXT);

      return(0);
   }
   else if (((STRPNT *)prvStrBufPtr)->header & (BEG_STROKE+MID_STROKE+END_STROKE)) {

      // in trk mode translate the stroke to follow the tracked feature
      //
      int trnslX = 0;
      int trnslY = 0;
      if (trackedMacroFlag) { // is a tracked macro

         int thisFrame = getSystemAPI()->getLastFrameIndex();

         trnslX =
           trkPts[thisFrame - trkBegFrame].x - trkPts[repeatMacroBegFrame - trkBegFrame].x;

         trnslY =
           trkPts[thisFrame - trkBegFrame].y - trkPts[repeatMacroBegFrame - trkBegFrame].y;
      }

      // translate the stroke pt before queueing it up
      QueueUpStrokePoint(((STRPNT *)prvStrBufPtr)->x + trnslX,
                         ((STRPNT *)prvStrBufPtr)->y + trnslY,
                         ((STRPNT *)prvStrBufPtr)->header);

      if (((STRPNT *)prvStrBufPtr)->header & END_STROKE) {

         // wait for completion (important for next step)
         while (strEmpty < strFill) MTImillisleep(1);

         // now that all's painted, push original pixels
         getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

         // turn on accept/reject
         ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
         PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
      }

      prvStrBufPtr += sizeof(STRPNT);

      return(0);
   }

   else return(-1);
}

int CPaintTool::RangeStrokeFileToken()
{
   if ( ((((STRPNT  *)prvStrBufPtr)->header & (BEG_STROKE+MID_STROKE+END_STROKE))&&(prvStrBufPtr > (prvStrBufEnd - sizeof(STRPNT)) )) ||
        ((((STRCTXT *)prvStrBufPtr)->header & STROKE_CONTEXT)&&(prvStrBufPtr > (prvStrBufEnd - sizeof(STRCTXT))))) {

      prvStrFilePtr += prvStrBufPtr - prvStrBuf;
      int icnt = prvStrFileLength - prvStrFilePtr;
      if (icnt == 0)
         return -1; // done

      if (icnt > (int)(MAX_MACRO_POINTS*sizeof(STRPNT)))
         icnt = MAX_MACRO_POINTS*sizeof(STRPNT);
      lseek64(prvStrFileHandle, prvStrFilePtr, SEEK_SET);
      int ocnt = read(prvStrFileHandle,(MTI_UINT8 *)prvStrBuf,icnt);
      if (ocnt != icnt)
         return -1; // error

      prvStrBufPtr = prvStrBuf;
      prvStrBufEnd = prvStrBuf + ocnt;
   }

   if (((STRCTXT *)prvStrBufPtr)->header & STROKE_CONTEXT )  {

      // wait for completion (important for next step)
      while (strEmpty < strFill) MTImillisleep(1);

      // load the new STROKE CONTEXT
      bool useHistory = LoadStrokeContext(*(STRCTXT *)prvStrBufPtr);

      prvStrBufPtr += sizeof(STRCTXT);

      return(useHistory? 1:0);
   }
   else if (((STRPNT *)prvStrBufPtr)->header & (BEG_STROKE+MID_STROKE+END_STROKE)) {

      // in trk mode translate the stroke to follow the tracked feature
      //
      int trnslX = 0;
      int trnslY = 0;
      if (trackedMacroFlag) { // is a tracked macro

         int thisFrame = getSystemAPI()->getLastFrameIndex();

         trnslX =
           trkPts[thisFrame - trkBegFrame].x - trkPts[rangeMacroBegFrame - trkBegFrame].x;

         trnslY =
           trkPts[thisFrame - trkBegFrame].y - trkPts[rangeMacroBegFrame - trkBegFrame].y;
      }

      // translate the stroke pt before queueing it up
      QueueUpStrokePoint(((STRPNT *)prvStrBufPtr)->x + trnslX,
                         ((STRPNT *)prvStrBufPtr)->y + trnslY,
                         ((STRPNT *)prvStrBufPtr)->header);

      if (((STRPNT *)prvStrBufPtr)->header & END_STROKE) {

         // wait for completion (important for next step)
         while (strEmpty < strFill) MTImillisleep(1);

         // hard edge phenomenon fixed here
         getSystemAPI()->completeMacroStroke();
      }

      prvStrBufPtr += sizeof(STRPNT);

      return(0);
   }

   else return(-1);
}

int  CPaintTool::ClosePreviousStrokeChannelAfterRead()
{
   int retVal = close(prvStrFileHandle);

   // indicator
   prvStrFileHandle = -1;

   return retVal;
}

bool CPaintTool::StrokeChannelIsNonEmpty()
{
   int retVal = OpenPreviousStrokeChannelForRead();
   ClosePreviousStrokeChannelAfterRead();
   return(retVal > 0);
}

int  CPaintTool::ClearCapturedStroke()
{
   int retVal = 0;

   // if file exists, truncates to 0 length
   retVal = truncate64(previousFilename.c_str(), 0);

   // state of buttons shows we have no captured stroke
   PaintToolForm->PlayStrokeSpeedButton->Enabled  = false;

   return(retVal);
}

int  CPaintTool::PlayCapturedStroke(bool setxfm)
{
   int retVal;

   if (strCapture ||
       !((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
         (majorState == MAJORSTATE_CLONE_MODE_HOME)||
         (majorState == MAJORSTATE_COLOR_MODE_HOME)||
         (majorState == MAJORSTATE_ORIG_MODE_HOME)||
         (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)))
      return(-1);

   if (PaintToolForm->PendingStrokesListBox->Count > 0) {
      int imp;
      getSystemAPI()->getPaintFrames(&repeatMacroBegFrame, &imp);
   }

   // if tracked macro is possible and checkbox is checked...
   trackedMacroFlag = TrackedMacroIsEnabled() && (repeatMacroBegFrame != -1) &&
                      PaintToolForm->TrackedMacroCheckBox->Checked;

   // NEW: ensure the import frame is loaded
   PaintToolForm->ClearLoadTimer();
   getSystemAPI()->forceTargetAndImportFrameLoaded();

   // test for auto accept and do it if enabled
   AutoAcceptConditional();

   // context before the operation
   GetStrokeContext(savedContext);

   retVal = OpenPendingStrokeChannelForWrite();

   // if can't open stroke channel or if it's empty return
   retVal = OpenPreviousStrokeChannelForRead();
   if (retVal <= 0) {
      retVal = ClosePreviousStrokeChannelAfterRead();
      ClosePendingStrokeChannelAfterWrite();
      return(-1);
   }

   // initialize the point FIFO
   ResetBackgroundPainter();

   strCapture = true;

   // read and process the stroke tokens,
   // consisting of CONTEXT, BEG_STROKE,
   // MID_STROKE,..., END_STROKE, etc.
   while ((retVal = PlayStrokeFileToken(setxfm)) == 0);

   strCapture = false;

   // close the stroke channel after finishing
   retVal = ClosePreviousStrokeChannelAfterRead();

   ClosePendingStrokeChannelAfterWrite();

   // refresh the display
   // mbraca moved this down to below the LoadStrokeContext() so the brush
   // doesn't flash at the wrong size!
   //getSystemAPI()->refreshFrameCached();

   // now examine the target frame and see if
   // it's really modified from the original
   ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
   PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

   // context after the operation
   LoadStrokeContext(savedContext);

   // refresh the display
   getSystemAPI()->refreshFrameCached(true);  // moved here from above

   return retVal;
}

int  CPaintTool::RangeCapturedStroke()
{
   int retVal;

   // accept processed target frame
//      DBTRACE(importFrame);

#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 4606");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
//      DBTRACE(importFrame);
//      DBTRACE(targetFrame);

   if (!rangeMacroExecuting||strCapture)
      return(-1);

   // if can't open stroke channel or if it's empty return
   retVal = OpenPreviousStrokeChannelForRead();
   if (retVal <= 0) {
		retVal = ClosePreviousStrokeChannelAfterRead();
      return(-1);
   }

   // initialize the point FIFO
   ResetBackgroundPainter();

   strCapture = false;

   // read and process the stroke tokens,
   // consisting of CONTEXT, BEG_STROKE,
   // MID_STROKE,..., END_STROKE, etc.
   int useHistory = 0;
   while ((retVal = RangeStrokeFileToken()) >= 0) {
      useHistory += retVal;
   };

   strCapture = false;

   // close the stroke channel after finishing
	ClosePreviousStrokeChannelAfterRead();

   // wait for completion (important for next step)
	while (strEmpty < strFill) MTImillisleep(1);

	// VVV Do NOT RenamePendingAsPrevious() here -
	// VVV we did it once at the start of the range processing!
	retVal = AcceptTargetFrameModifications(useHistory == 0);

	if (retVal != 0) // write media error
		return retVal;

	// not needed - we go right to the next frame
	// getSystemAPI()->reloadModifiedTargetFrame();

	// redraw the target from its intermediate
	getSystemAPI()->redrawTargetFrame();

	return retVal;
}

bool CPaintTool::TrackedMacroIsEnabled()
{
   int trg, imp;
   getSystemAPI()->getPaintFrames(&trg, &imp);

   if ((trg >= trkBegFrame)&&(trg < trkEndFrame)) {

      int inFrm = getSystemAPI()->getMarkIn();
      int outFrm = getSystemAPI()->getMarkOut();

      if ((inFrm >= trkBegFrame)&&(outFrm <= trkEndFrame)) {

         return true;
      }
   }

   return false;
}

// sends a "play-between-marks" command to the Navigator
//
int CPaintTool::ExecuteRangeMacro()
{
   // do nothing if we are already executing range macro
   if (rangeMacroExecuting) return 0;

   // do nothing if we're in an oddball major state
	if ((majorState != MAJORSTATE_REVEAL_MODE_HOME)&&
		 (majorState != MAJORSTATE_CLONE_MODE_HOME)&&
		 (majorState != MAJORSTATE_COLOR_MODE_HOME)&&
		 (majorState != MAJORSTATE_ORIG_MODE_HOME)&&
		 (majorState != MAJORSTATE_ALTCLIP_MODE_HOME)) {

		return 0;
   }

   // there are situations in which the user will have made
   // strokes which he'd like to have accepted across a
   // range of frames, but where the strokes don't modify
   // the target frame they're made on. For example, if the
   // strokes were already accepted on the initial frame,
   // bringing them up again won't modify that frame. Another
   // example is if we have PAINT strokes and ERASE strokes
   // which cancel each other out. In any case, we will no
   // longer require that the strokes modify the frame -- only
   // that there be at least one stroke to accept with SHIFT-G
   //
   //if (!getSystemAPI()->targetFrameIsModified()) return 0;
   //
	if (PaintToolForm->PendingStrokesListBox->Count == 0)
	{
		return 0;
	}

	if (!MarksAreValid()) {

      // IN and OUT marks must be correctly set
      PaintToolForm->RangeMarksErrorDialog();

      // set the focus back to the Main window
		getSystemAPI()->setMainWindowFocus();

      return 0;
	}

   int imp;
   getSystemAPI()->getPaintFrames(&rangeMacroBegFrame, &imp);

	// if tracked macro is possible and checkbox is checked...
	trackedMacroFlag = TrackedMacroIsEnabled() &&
                      PaintToolForm->TrackedMacroCheckBox->Checked;

   if (isTrackingAlignmentEnabled()) {

      CAutoErrorReporter autoErr("CPaintTool::ExecuteRangeMacro",
                           AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

		switch (getTrackingDataValidity()) {

         case TRKVLD_ALL_TRACK_DATA_IS_VALID:
            // Sweet!
            break;

         case TRKVLD_TRACK_DATA_IS_VALID_EXCEPT_FOR_REF_FRAME:

				if (_MTIConfirmationDialog(
							 "Some frames do not have tracking data;    \n"
							 "those frames will not be aligned!\n"
							 "Proceed?")
					 != MTI_DLG_OK) {

               return 0;
            }
            break;

         case TRKVLD_NEED_TO_RETRACK:
            _MTIErrorDialog("Tracking data is invalid - "
                            "cannot proceed in Track Align mode   ");
            return 0;
      }
   }

   // this pops up when you're done
   PaintToolForm->RangeStrokeSpeedButton->Down = true;

	// pending.str becomes previous.str
	RenamePendingAsPrevious();

   // only accept between the marks
   RejectProcessedTargetFrame();

   if (StrokeChannelIsNonEmpty()) { // redundant?

      // context before the operation
      GetStrokeContext(savedContext);

      // this forces each frame of the sequence to
      // be loaded as target, without needing a 'sync'
      // and without needing to examine whether the
      // previous target frame has been modified. It
      // also suppresses the initial draw of the just-
      // loaded frame - in the case of
      getSystemAPI()->enableForceTargetLoad(true);

      //getSystemAPI()->enableForceImportLoad(isTrackingAlignmentEnabled());
      getSystemAPI()->enableForceImportLoad(true);

      // remove brushes
      getSystemAPI()->unloadPaintBrush();
      getSystemAPI()->unloadCloneBrush();

      rangeMacroExecuting = true;

      // set up progress monitor
		GetToolProgressMonitor()->SetFrameCount(strEndFrame - strBegFrame);
      GetToolProgressMonitor()->SetStatusMessage("Processing range macro");
      GetToolProgressMonitor()->StartProgress();

      // need to be able to stop this thing!
      PaintToolForm->StopSpeedButton->Enabled = true;

      // the system plays between the marks, and the "onRedraw"
      // method contains the processing for individual frames
      // WAS: getSystemAPI()->executeNavigatorToolCommand(NAV_CMD_PLAY_MARKS_TOOL);
      // NEW: use this call because it makes sure provisional frame index is clear
		// to avoid having SHIFT-G screw up the frames
		strReverse = PaintToolForm->ReverseCheckBox->Checked;
		int firstFrameIndex = strReverse ? strEndFrame - 1 : strBegFrame;
		int lastFrameIndex = strReverse ? strBegFrame : strEndFrame - 1;
		getSystemAPI()->toolPlayClipFrameRangeInclusive(firstFrameIndex, lastFrameIndex);
	}

   getSystemAPI()->setMainWindowFocus();

   return 0;
}

bool CPaintTool::IsRangeMacroExecuting()
{
   return rangeMacroExecuting;
}

// sends a "stop-and-synchronize" command to the Navigator
//
void CPaintTool::SuspendRangeMacro()
{
   if (rangeMacroExecuting) {

      getSystemAPI()->executeNavigatorToolCommand(NAV_CMD_STOP);

		//PaintToolForm->SetRangeStrokeSpeedButton(false);
   }
}

void CPaintTool::RecycleCurrentStrokes()
{
   int strcnt = PaintToolForm->PendingStrokesListBox->Count;
   if (strcnt > 0) {

      // save current context
      STRCTXT currentContext;            // Don't use global savedContext!
      GetStrokeContext(currentContext);

		// pending strokes become previous
		RenamePendingAsPrevious();

      // reverse the effects of the current strokes
      getSystemAPI()->popStrokesToTarget(strcnt);

      // set the new density strength
      getSystemAPI()->setDensityStrength(brushDensityStrength);

      // set the new grain strength
      getSystemAPI()->setGrainStrength(brushGrainStrength);

      // replay them with the new strengths
      PlayCapturedStroke(true);

      // restore current context
      LoadStrokeContext(currentContext);

      goToTargetFrame();
   }
}

////////////////////////////////////////////////////////////////////////////////

void CPaintTool::SetCloneBrushRelative(bool isrel)
{
   LoadCloneBrush(false, cloneX, cloneY);

   captureOffsetOnce = isrel;
   captureOffset     = !captureOffsetOnce;
}

void CPaintTool::LoadCloneBrush(bool rel, int x, int y)
{
   lastCloneRel = rel;
   lastCloneX = x;
   lastCloneY = y;
   getSystemAPI()->loadCloneBrush(PaintToolForm->GetCurrentBrush(),
                                  lastCloneRel,
                                  lastCloneX,
                                  lastCloneY);
}

void CPaintTool::SetCloneX(int clx)
{
   cloneX = clx;

   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      if (captureOffsetOnce || captureOffset) {

         // clone brush is sitting still @ [cloneX, cloneY]

         LoadCloneBrush(false, cloneX, cloneY);
      }
      else {

         if (lastCloneRel) {

            // clone brush is traveling with the paint
            // brush and we need to reset its offset

            // the offset of the clone pt from the capture pt
            clOffsetX = cloneX - captureX;
            clOffsetY = cloneY - captureY;

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // update the GUI with offset
            PaintToolForm->UpdateCloneDelX(clOffsetX);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
      }
   }

   goToTargetFrame();
}

void CPaintTool::SetCloneY(int cly)
{
   cloneY = cly;

   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      if (captureOffsetOnce || captureOffset) {

         // clone brush is sitting still @ [cloneX, cloneY]

         LoadCloneBrush(false, cloneX, cloneY);
      }
      else {

         if (lastCloneRel) {

            // clone brush is traveling with the paint
            // brush and we need to reset its offset

            // the offset of the clone pt from the capture pt
            clOffsetX = cloneX - captureX;
            clOffsetY = cloneY - captureY;

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // update the GUI with offset
            PaintToolForm->UpdateCloneDelY(clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
      }
   }

   goToTargetFrame();
}

void CPaintTool::SetCloneDelX(int clx)
{
   clOffsetX = clx;

   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      if (lastCloneRel) {

         // clone brush is traveling with the paint
         // brush and we need to reset its offset

         // offset the import frame accordingly
         getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

         // update the GUI with offset
         //PaintToolForm->UpdateCloneDelX(clOffsetX);

         // clone brush now relative
         LoadCloneBrush(true, clOffsetX, clOffsetY);
      }
   }

   goToTargetFrame();
}

void CPaintTool::SetCloneDelY(int cly)
{
   clOffsetY = cly;

   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      if (lastCloneRel) {

         // clone brush is traveling with the paint
         // brush and we need to reset its offset

         // offset the import frame accordingly
         getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

         // update the GUI with offset
         //PaintToolForm->UpdateCloneDelY(clOffsetX);

         // clone brush now relative
         LoadCloneBrush(true, clOffsetX, clOffsetY);
      }
   }

   goToTargetFrame();
}

void CPaintTool::ReloadCloneBrush()
{
   getSystemAPI()->loadCloneBrush(PaintToolForm->GetCurrentBrush(),
                                  lastCloneRel,
                                  lastCloneX,
                                  lastCloneY);
}

void CPaintTool::EnableAutoAccept(bool onoff)
{
   // set the flag here
   autoAccept = onoff;

   // and in the Displayer
   getSystemAPI()->enableAutoAccept(onoff);

   // and make the checkbox consistent
	PaintToolForm->AutoAcceptCheckBox->Checked = onoff;
}

void CPaintTool::ResetBackgroundPainter()
{
   // make sure brush wgts are computed
   // before putting strokes into FIFO
   PaintToolForm->RecalculateBrush();

   if ((strEmpty != 0)||(strFill != 0))
   {
      resetFIFO = true;
      SemSuspend(fifoResetSem);
   }
}

bool CPaintTool::IsBackgroundPainterPainting()
{
   return (strEmpty < strFill);
}

// this background thread empties the queue of stroke
// pts, drawing segments between consecutive points
//
void CPaintTool::BackgroundPainter(void *vr)
{
   disablePainter = false;
   strFill = 0;
   strEmpty = 0;
   resetFIFO = false;

   BThreadBegin(vr);

   while (!disablePainter) {

      // yield CPU to foreground  - do this FIRST so we finish processing
      // before disabling when coming out of sleep
      MTImillisleep(1);

      if (resetFIFO) {

         strFill  = 0;
         strEmpty = 0;
         resetFIFO = false;
         SemResume(fifoResetSem);
      }
      else {

         while (strEmpty < strFill) {

            int index = strEmpty%MAX_STROKE_POINTS;

            // begin stroke resets [lastStrokeX, lastStrokeY]
            if (strokeQueue[index].header & BEG_STROKE) {

               getSystemAPI()->paintOneBrushPositionFrame(strokeQueue[index].x,
                                                          strokeQueue[index].y);
            }
            else if ((strokeQueue[index].x != lastStrokeX)||
                     (strokeQueue[index].y != lastStrokeY)) {

               getSystemAPI()->paintOneStrokeSegmentFrame(lastStrokeX,
                                                          lastStrokeY,
                                                          strokeQueue[index].x,
                                                          strokeQueue[index].y);
            }

            // setup for next stroke
            lastStrokeX = strokeQueue[index].x;
            lastStrokeY = strokeQueue[index].y;

            if (strCapture) { // capture enabled
               CaptureStrokePoint(strokeQueue[index]);
            }

            strEmpty++;
         }
      }

      // yield CPU to foreground
      //MTImillisleep(1);            moved to top of loop
   }

   // clean up
   disablePainter = false;
   strFill = 0;
   strEmpty = 0;
   resetFIFO = false;

   // signal thread has ended
   SemResume(painterThreadEndSem);   // unblock the main thread
}

void CPaintTool::BackgroundPainterCallback(void *vp, void *vr)
{
   CPaintTool *p = static_cast <CPaintTool *> (vp);
   p->BackgroundPainter(vr);
}

void CPaintTool::SetDensityStrength(int denstrng)
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

      if (denstrng < -99)
         denstrng = -99;
      else if (denstrng > 100)
         denstrng = 100;
      brushDensityStrength = denstrng;

      // upload to Displayer
      getSystemAPI()->setDensityStrength(brushDensityStrength);

      // make GUI consistent & cock the timer
      PaintToolForm->UpdateDensityStrength(brushDensityStrength);
   }
}

int CPaintTool::GetDensityStrength()
{
   return brushDensityStrength;
}

void CPaintTool::SetGrainStrength(int grstrng)
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

      if (grstrng < 0)
         grstrng = 0;
      else if (grstrng > 100)
         grstrng = 100;
      brushGrainStrength = grstrng;

      // upload to Displayer
      getSystemAPI()->setGrainStrength(brushGrainStrength);

      // make GUI consistent & cock the timer
      PaintToolForm->UpdateGrainStrength(brushGrainStrength);
   }
}

int CPaintTool::GetGrainStrength()
{
   return brushGrainStrength;
}

void CPaintTool::SaveDensityAndGrain()
{
   povSavedMouseWheelState = getMouseWheelState();
   if ((povSavedMouseWheelState == MOUSE_WHEEL_ADJUST_DENSITY)||
       (povSavedMouseWheelState == MOUSE_WHEEL_ADJUST_GRAIN)) {

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
   }

   povSavedBrushDensityStrength = brushDensityStrength;
   brushDensityStrength = 0;
   getSystemAPI()->setDensityStrength(brushDensityStrength);
   PaintToolForm->UpdateDensityStrength(brushDensityStrength);

   povSavedBrushGrainStrength = brushGrainStrength;
   brushGrainStrength = 0;
   getSystemAPI()->setGrainStrength(brushGrainStrength);
   PaintToolForm->UpdateGrainStrength(brushGrainStrength);
}

void CPaintTool::RestoreDensityAndGrain()
{
   SetDensityStrength(povSavedBrushDensityStrength);

   SetGrainStrength(povSavedBrushGrainStrength);

   if ((povSavedMouseWheelState == MOUSE_WHEEL_ADJUST_DENSITY)||
       (povSavedMouseWheelState == MOUSE_WHEEL_ADJUST_GRAIN)) {

      PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
   }
}

// enable the healer
//
void CPaintTool::EnableHealer(bool enbl)
{
   healerOn = enbl;
}

// this background thread iterates the healer
// algorithm over the area defined by the mask
//
void CPaintTool::BackgroundHealer(void *vr)
{
   disableHealer = false;
   healerOn = false;

   BThreadBegin(vr);

   while (!disableHealer) {

      // yield CPU to foreground  - do this FIRST so we finish processing
      // before disabling when coming out of sleep
      MTImillisleep(1);

      while (true) {

         if ((healerOn==false)||
             (getSystemAPI()->iterateOneInpaintCycle() == -1))
            break;
      }

      // yield CPU to foreground
      //MTImillisleep(1);      moved to top of loop
   }

   // clean up
   disableHealer = false;
   healerOn = false;

   // signal thread has ended
   SemResume(healerThreadEndSem);   // unblock the main thread
}

void CPaintTool::BackgroundHealerCallback(void *vp, void *vr)
{
   CPaintTool *p = static_cast <CPaintTool *> (vp);
   p->BackgroundHealer(vr);
}

//--------------------------------------------------------------------------

// It appears the the "fast" mode switches are intended for playing back
// macros, where you don't need all the GUI stuff changing
void CPaintTool::SelectRevealModeFast()
{
   // Sanity check
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
             (majorState == MAJORSTATE_CLONE_MODE_HOME)||
             (majorState == MAJORSTATE_COLOR_MODE_HOME)||
             (majorState == MAJORSTATE_ORIG_MODE_HOME));

   if ((majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)){

      setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
   }

   paintMode = PAINT_MODE_REVEAL;
}

void CPaintTool::SelectRevealMode()
{
   if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

      PaintToolForm->RevealButton->Down = true;
   }
   else {

#if 0
      // GAG ME - make sure we are called from a newMajorState that is handled!!
      MTIassert((majorState == MAJORSTATE_EDIT_MASK)||
                (majorState == MAJORSTATE_GUI_CREATED)||
                (majorState == MAJORSTATE_CLONE_MODE_HOME)||
                (majorState == MAJORSTATE_COLOR_MODE_HOME)||
                (majorState == MAJORSTATE_ORIG_MODE_HOME)||
                (majorState == MAJORSTATE_CHOOSE_CLONE_PT));
#endif

      if (majorState == MAJORSTATE_EDIT_MASK) {

         LeaveMaskRevert();
      }
      else if (majorState == MAJORSTATE_GUI_CREATED) {

         // cut off painting
         AbortStrokePainting();

         getSystemAPI()->ClearTransform(false);

         // TOOL newMajorState
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(revealModeRotation,
                                        revealModeStretchX,
                                        revealModeStretchY,
                                        revealModeSkew,
                                        revealModeOffsetX,
                                        revealModeOffsetY);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->PositionImportButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(revealModeImportFrameMode);

         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE)
         {
            getSystemAPI()->setImportFrameOffset(revealModeImportFrameOffset);
         }
         else
         {
            getSystemAPI()->setImportFrameTimecode(revealModeImportFrame);
         }

         // now set up the transform from PaintClip.ini
         getSystemAPI()->SetTransform(revealModeRotation,
                                      revealModeStretchX,
                                      revealModeStretchY,
                                      revealModeSkew,
                                      revealModeOffsetX,
                                      revealModeOffsetY);

         // Start with tracking alignment OFF
         alignState = ALIGNSTATE_MANUAL;
         PaintToolForm->TrackAlignButton->Down = false;
         PaintToolForm->TrackAlignButtonClick(PaintToolForm->TrackAlignButton);

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         cloneModeImportFrameMode = getSystemAPI()->getImportMode();
         cloneModeImportFrame = PaintToolForm->GetLocalImportFrame();

         if (alignState == ALIGNSTATE_TRACK_SHOWING_TARGET) {

            // fixes bug disabling clone mode 1/11/10
            getSystemAPI()->useTrackingAlignOffsets(true);
         }

         // TOOL newMajorState
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(revealModeRotation,
                                        revealModeStretchX,
                                        revealModeStretchY,
                                        revealModeSkew,
                                        revealModeOffsetX,
                                        revealModeOffsetY);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->PositionImportButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(revealModeImportFrameMode);
         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(revealModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(revealModeImportFrame);

         // xform the import frame
         getSystemAPI()->SetTransform(revealModeRotation,
                                      revealModeStretchX,
                                      revealModeStretchY,
                                      revealModeSkew,
                                      revealModeOffsetX,
                                      revealModeOffsetY);

          // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(revealModeRotation,
                                        revealModeStretchX,
                                        revealModeStretchY,
                                        revealModeSkew,
                                        revealModeOffsetX,
                                        revealModeOffsetY);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->PositionImportButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(revealModeImportFrameMode);
         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(revealModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(revealModeImportFrame);

         // xform the import frame
         getSystemAPI()->SetTransform(revealModeRotation,
                                      revealModeStretchX,
                                      revealModeStretchY,
                                      revealModeSkew,
                                      revealModeOffsetX,
                                      revealModeOffsetY);

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
			RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // TOOL newMajorState
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(revealModeRotation,
                                        revealModeStretchX,
                                        revealModeStretchY,
                                        revealModeSkew,
                                        revealModeOffsetX,
                                        revealModeOffsetY);
         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // restore densty and grain
         RestoreDensityAndGrain();

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->PositionImportButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(revealModeImportFrameMode);
         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(revealModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(revealModeImportFrame);

         // xform the import frame
         getSystemAPI()->SetTransform(revealModeRotation,
                                      revealModeStretchX,
                                      revealModeStretchY,
                                      revealModeSkew,
                                      revealModeOffsetX,
                                      revealModeOffsetY);

        // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
			RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // save this
         altClipModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(revealModeRotation,
                                        revealModeStretchX,
                                        revealModeStretchY,
                                        revealModeSkew,
                                        revealModeOffsetX,
                                        revealModeOffsetY);
         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

         // restore densty and grain
         RestoreDensityAndGrain();

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->PositionImportButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(revealModeImportFrameMode);
         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(revealModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(revealModeImportFrame);

         // xform the import frame
         getSystemAPI()->SetTransform(revealModeRotation,
                                      revealModeStretchX,
                                      revealModeStretchY,
                                      revealModeSkew,
                                      revealModeOffsetX,
                                      revealModeOffsetY);

        // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {
         // haha wtf is this? how does this get called from CHOOSE_CLONE_PT
         // state?
         PaintToolForm->CloneButton->Down = true;
      }
   }

   // WAS getSystemAPI()->refreshFrameCached();
   getSystemAPI()->refreshFrame();

   paintMode = PAINT_MODE_REVEAL;
}

// It appears the the "fast" mode switches are intended for playing back
// macros, where you don't need all the GUI stuff changing
void CPaintTool::SelectCloneModeFast()
{
   // Sanity check
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
             (majorState == MAJORSTATE_CLONE_MODE_HOME)||
             (majorState == MAJORSTATE_COLOR_MODE_HOME)||
             (majorState == MAJORSTATE_ORIG_MODE_HOME));

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)){

      setMajorState(MAJORSTATE_CLONE_MODE_HOME);
   }

   paintMode = PAINT_MODE_CLONE;
}

void CPaintTool::SelectCloneMode()
{
   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      PaintToolForm->CloneButton->Down = true;
   }
   else {

#if 0
      // GAG ME - if we get called from a major state that isn't
      // handled in this switch statement, clone mode won't work!!
      MTIassert((majorState == MAJORSTATE_EDIT_MASK)||
                (majorState == MAJORSTATE_GUI_CREATED)||
                (majorState == MAJORSTATE_REVEAL_MODE_HOME)||
                (majorState == MAJORSTATE_SELECTING_IMPORT_FRAME)||
                (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)||
                (majorState == MAJORSTATE_EDIT_TRANSFORM)||
                (majorState == MAJORSTATE_GET_ORIGINAL_VALUES)||
                (majorState == MAJORSTATE_COLOR_MODE_HOME)||
                (majorState == MAJORSTATE_ORIG_MODE_HOME)||
                (majorState == MAJORSTATE_ALTCLIP_MODE_HOME));
#endif

      if (majorState == MAJORSTATE_EDIT_MASK) {

         LeaveMaskRevert();
      }
      else if (majorState == MAJORSTATE_GUI_CREATED) {

         // cut off painting
         AbortStrokePainting();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

         // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // save these
         revealModeImportFrameMode = getSystemAPI()->getImportMode();
         revealModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // bounce out of "show both" mode
         if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {
            LeaveTrackingAlignShowBothMode();
         }
         else if (alignState == ALIGNSTATE_TRACK_SHOWING_TARGET) {

            // fixes bug disabling clone mode 1/11/10
            getSystemAPI()->useTrackingAlignOffsets(false);
         }

         // and the transform
         getSystemAPI()->GetTransform(&revealModeRotation,
                                      &revealModeStretchX,
                                      &revealModeStretchY,
                                      &revealModeSkew,
                                      &revealModeOffsetX,
                                      &revealModeOffsetY);
         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

          // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

          // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

          // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // restore densty and grain
         RestoreDensityAndGrain();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // save this
         altClipModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

          // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // restore densty and grain
         RestoreDensityAndGrain();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
      else if ((majorState == MAJORSTATE_SELECTING_IMPORT_FRAME)||
               (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)) {

         PaintToolForm->RevealButton->Down = true;

         // don't want to go to target frame here (!)
         return;
      }
      else if ((majorState == MAJORSTATE_EDIT_TRANSFORM)||
               (majorState == MAJORSTATE_GET_ORIGINAL_VALUES)) {

         // save these
         revealModeImportFrameMode = getSystemAPI()->getImportMode();
         revealModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // clone mode wants this cleared
         getSystemAPI()->ClearTransform(false);

         // clone brush mode
         PaintToolForm->SetCloneBrushRelative(clModePointMode);

         // load initial values into GUI edit boxes
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
         PaintToolForm->UpdateCloneDelX(clOffsetX);
         PaintToolForm->UpdateCloneDelY(clOffsetY);

         // before making them visible here
         PaintToolForm->setMajorState(MAJORSTATE_CLONE_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->SetClonePointSpeedButton->Repaint();

         // this sequence generates the proxy
         getSystemAPI()->setImportMode(cloneModeImportFrameMode);
         if (cloneModeImportFrameMode == IMPORT_MODE_RELATIVE)
            getSystemAPI()->setImportFrameOffset(cloneModeImportFrameOffset);
         else
            getSystemAPI()->setImportFrameTimecode(cloneModeImportFrame);

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // on re-entering clone mode where the last context was
         // RELATIVE, we can continue painting where we left off

         captureOffsetOnce = false;
         if (PaintToolForm->CloneBrushIsRelative()) {

            // offset the import frame accordingly
            getSystemAPI()->SetOffsetXY( -clOffsetX, -clOffsetY);

            // clone brush now relative
            LoadCloneBrush(true, clOffsetX, clOffsetY);
         }
         else { // re-entering absolute mode

            captureOffset = true;

            // display clone brush at last clone pt
            LoadCloneBrush(false, cloneX, cloneY);
         }

         // moved from above 10/20/09
         SelectThru();
      }
   }

   // WAS getSystemAPI()->refreshFrameCached();
   getSystemAPI()->refreshFrame();

   paintMode = PAINT_MODE_CLONE;
}

// It appears the the "fast" mode switches are intended for playing back
// macros, where you don't need all the GUI stuff changing
void CPaintTool::SelectColorModeFast()
{
   // Sanity check
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
             (majorState == MAJORSTATE_CLONE_MODE_HOME)||
             (majorState == MAJORSTATE_COLOR_MODE_HOME)||
             (majorState == MAJORSTATE_ORIG_MODE_HOME));

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)){

      setMajorState(MAJORSTATE_COLOR_MODE_HOME);
   }

   paintMode = PAINT_MODE_COLOR;
}

void CPaintTool::SelectColorMode()
{
   if ((majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_PICK_COLOR)||
       (majorState == MAJORSTATE_PICK_SEED)) {

      setMajorState(MAJORSTATE_COLOR_MODE_HOME);
      PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);
   }
   else {

#if 0
      // GAG ME - make sure we are called from a newMajorState that is handled!!
      MTIassert((majorState == MAJORSTATE_EDIT_MASK)||
                (majorState == MAJORSTATE_GUI_CREATED)||
                (majorState == MAJORSTATE_REVEAL_MODE_HOME)||
                (majorState == MAJORSTATE_EDIT_TRANSFORM)||
                (majorState == MAJORSTATE_CLONE_MODE_HOME)||
                (majorState == MAJORSTATE_ORIG_MODE_HOME));
#endif

      if (majorState == MAJORSTATE_EDIT_MASK) {

         LeaveMaskRevert();
      }
      else if (majorState == MAJORSTATE_GUI_CREATED) {

         // cut off painting
         AbortStrokePainting();

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // TOOL newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // save these
         revealModeImportFrameMode = getSystemAPI()->getImportMode();
         revealModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // bounce out of "show both" mode
         if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {
            LeaveTrackingAlignShowBothMode();
         }

         getSystemAPI()->GetTransform(&revealModeRotation,
                                      &revealModeStretchX,
                                      &revealModeStretchY,
                                      &revealModeSkew,
                                      &revealModeOffsetX,
                                      &revealModeOffsetY);
         // TOOL newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // save these
         cloneModeImportFrameMode = getSystemAPI()->getImportMode();
         cloneModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // TOOl newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // TOOl newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // restore densty and grain
         RestoreDensityAndGrain();

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // save these
         altClipModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();

         // TOOl newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // restore densty and grain
         RestoreDensityAndGrain();

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

         // User may want to go right into color mode.
         // The edit transform operation is aborted and
         // the transform is not changed.
         //
         // TOOL newMajorState
         setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // overlap rectangle must be right!
         getSystemAPI()->ClearTransform(false);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

         // no import frame in COLOR mode
         getSystemAPI()->setImportMode(IMPORT_MODE_NONE);

         // speeds up GUI repaint!
         PaintToolForm->DropperSpeedButton->Repaint();

         // moved 10/20/09
         SelectThru();
      }
   }

   // WAS getSystemAPI()->refreshFrameCached();
   getSystemAPI()->refreshFrame();

   paintMode = PAINT_MODE_COLOR;
}

void CPaintTool::SelectOrigValuesModeFast()
{
   // Sanity check
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
             (majorState == MAJORSTATE_CLONE_MODE_HOME)||
             (majorState == MAJORSTATE_COLOR_MODE_HOME)||
             (majorState == MAJORSTATE_ORIG_MODE_HOME));

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

      setMajorState(MAJORSTATE_ORIG_MODE_HOME);
   }

   paintMode = PAINT_MODE_ORIG;
}

void CPaintTool::SelectOrigValuesMode()
{
   paintMode = PAINT_MODE_ORIG;

   if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

      setMajorState(MAJORSTATE_ORIG_MODE_HOME);
      PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

      // WAS PaintToolForm->OrigValuesButton->Down = true;
   }
   else {

      if (majorState == MAJORSTATE_EDIT_MASK) {

         LeaveMaskRevert();
      }
      else if (majorState == MAJORSTATE_GUI_CREATED) {

         // cut off painting
         AbortStrokePainting();

         getSystemAPI()->ClearTransform(false);

         // disable density and grain overrides
         SaveDensityAndGrain();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(0.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        0.0,
                                        0.0);
         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->CompareSpeedButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(IMPORT_MODE_ORIGINAL);

         // now set up the transform
         getSystemAPI()->SetTransform(0.0,
                                      1.0,
                                      1.0,
                                      0.0,
                                      0.0,
                                      0.0);

         // Start with tracking alignment OFF
         alignState = ALIGNSTATE_MANUAL;
         PaintToolForm->TrackAlignButton->Down = false;
         PaintToolForm->TrackAlignButtonClick(PaintToolForm->TrackAlignButton);

         // frames should be read afresh
         getSystemAPI()->clearPaintFrames();

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // save these
         revealModeImportFrameMode = getSystemAPI()->getImportMode();
         revealModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();
         revealModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // bounce out of "show both" mode
         if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {

            LeaveTrackingAlignShowBothMode();
         }

         // and the transform
         getSystemAPI()->GetTransform(&revealModeRotation,
                                      &revealModeStretchX,
                                      &revealModeStretchY,
                                      &revealModeSkew,
                                      &revealModeOffsetX,
                                      &revealModeOffsetY);

         // disable density and grain overrides
         SaveDensityAndGrain();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(0.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        0.0,
                                        0.0);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->CompareSpeedButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(IMPORT_MODE_ORIGINAL);

         // xform the import frame
         getSystemAPI()->SetTransform(0.0,
                                      1.0,
                                      1.0,
                                      0.0,
                                      0.0,
                                      0.0);

         // frames should be read afresh
         getSystemAPI()->clearPaintFrames();

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         cloneModeImportFrameMode = getSystemAPI()->getImportMode();
         cloneModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();
         cloneModeImportFrame = PaintToolForm->GetLocalImportFrame();

         // disable density and grain overrides
         SaveDensityAndGrain();

         setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(0.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        0.0,
                                        0.0);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->CompareSpeedButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(IMPORT_MODE_ORIGINAL);

         // xform the import frame
         getSystemAPI()->SetTransform(0.0,
                                      1.0,
                                      1.0,
                                      0.0,
                                      0.0,
                                      0.0);

         // target & import must be read afresh!
         getSystemAPI()->clearPaintFrames();

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // disable density and grain overrides
         SaveDensityAndGrain();

         // TOOL newMajorState
         setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(0.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        0.0,
                                        0.0);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->CompareSpeedButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(IMPORT_MODE_ORIGINAL);

         // xform the import frame
         getSystemAPI()->SetTransform(0.0,
                                      1.0,
                                      1.0,
                                      0.0,
                                      0.0,
                                      0.0);

         // target & import must be read afresh!
         getSystemAPI()->clearPaintFrames();

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

         // cut off painting
         AbortStrokePainting();

         // juggle macro file
         RenamePendingAsPrevious();

         // accept current strokes
         AcceptProcessedTargetFrame(false);

         // save this
         altClipModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();

         // disable density and grain overrides
         SaveDensityAndGrain();

         setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // fill these in before making GUI visible
         PaintToolForm->UpdateTransform(0.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        0.0,
                                        0.0);

         // GUI newMajorState
         PaintToolForm->setMajorState(MAJORSTATE_ORIG_MODE_HOME);

         // Supposedly speeds up the repaint of the entire control box...
         PaintToolForm->CompareSpeedButton->Repaint();

         // this sequence generates the reference frame proxy
         getSystemAPI()->setImportMode(IMPORT_MODE_ORIGINAL);

         // xform the import frame
         getSystemAPI()->SetTransform(0.0,
                                      1.0,
                                      1.0,
                                      0.0,
                                      0.0,
                                      0.0);

         // target & import must be read afresh!
         getSystemAPI()->clearPaintFrames();

         // reload the current frame from media
         int currentFrame = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->loadTargetAndImportFrames(currentFrame);

         // moved 10/20/09
         SelectThru();
      }
      else if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {
         // haha wtf is this? how does this get called from CHOOSE_CLONE_PT
         // state?
         PaintToolForm->CloneButton->Down = true;
      }
   }

   getSystemAPI()->refreshFrame();    // NOT refreshFrameCached() - don't know why
}

void CPaintTool::SelectAltClipModeFast()
{
   // Sanity check
   MTIassert((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
             (majorState == MAJORSTATE_CLONE_MODE_HOME)||
             (majorState == MAJORSTATE_COLOR_MODE_HOME)||
             (majorState == MAJORSTATE_ORIG_MODE_HOME)||
             (majorState == MAJORSTATE_ALTCLIP_MODE_HOME));

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)) {

      setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);
   }

   paintMode = PAINT_MODE_ALTCLIP;
}

void CPaintTool::SelectAltClipMode()
{
   if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

      setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);
      PaintToolForm->setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);
      getSystemAPI()->refreshFrame();    // NOT refreshFrameCached() - don't know why
      paintMode = PAINT_MODE_ALTCLIP;
      return;
   }

   if (majorState == MAJORSTATE_CHOOSE_CLONE_PT) {
      // haha wtf is this? how does this get called from CHOOSE_CLONE_PT
      // state?
      PaintToolForm->CloneButton->Down = true;
      getSystemAPI()->refreshFrame();    // NOT refreshFrameCached() - don't know why
      paintMode = PAINT_MODE_ALTCLIP;
      return;
   }

   if (majorState == MAJORSTATE_EDIT_MASK) {

      LeaveMaskRevert();
      getSystemAPI()->refreshFrame();    // NOT refreshFrameCached() - don't know why
      paintMode = PAINT_MODE_ALTCLIP;
      return;
   }

   paintMode = PAINT_MODE_ALTCLIP;

   if (majorState == MAJORSTATE_GUI_CREATED) {

      // NOT SURE HOW TO GET HERE. I guess the tool remembers the last state,
      // so it comes here first when you enter the tool. But why AbortStrokePainting()?
      // cut off painting
      AbortStrokePainting();

      getSystemAPI()->ClearTransform(false);
   }
   else {

      // cut off painting
      AbortStrokePainting();

      // juggle macro file
      RenamePendingAsPrevious();

      // accept current strokes
      AcceptProcessedTargetFrame(false);

      if (majorState == MAJORSTATE_REVEAL_MODE_HOME) {

         // save these
         revealModeImportFrameMode = getSystemAPI()->getImportMode();
         revealModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();
         revealModeImportFrame = PaintToolForm->GetLocalImportFrame();

//         DBTRACE(revealModeImportFrame);

         // bounce out of "show both" mode
         if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {

            LeaveTrackingAlignShowBothMode();
         }

         // and the transform
         getSystemAPI()->GetTransform(&revealModeRotation,
                                      &revealModeStretchX,
                                      &revealModeStretchY,
                                      &revealModeSkew,
                                      &revealModeOffsetX,
                                      &revealModeOffsetY);
      }
      else if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         cloneModeImportFrameMode = getSystemAPI()->getImportMode();
         cloneModeImportFrameOffset = getSystemAPI()->getImportFrameOffset();
         cloneModeImportFrame = PaintToolForm->GetLocalImportFrame();
      }
      else if (majorState == MAJORSTATE_COLOR_MODE_HOME) {

         // Nothing to do here!
      }
      else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

         // Nothing to do here!
      }
   }

   // disable density and grain overrides
   SaveDensityAndGrain();

   // TOOL newMajorState
   setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);

   // GUI newMajorState
   PaintToolForm->setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);

   // Supposedly speeds up the repaint of the entire control box...
   PaintToolForm->AltClipCompareButton->Repaint();

   // xform the import frame
   // QQQ How is this different from ClearTransform(()?
   getSystemAPI()->SetTransform(0.0,
                                1.0,
                                1.0,
                                0.0,
                                0.0,
                                0.0);

   // frames should be read afresh
   getSystemAPI()->clearPaintFrames();

   // reload the current frame from media
   int currentFrame = getSystemAPI()->getLastFrameIndex();
   getSystemAPI()->loadTargetAndImportFrames(currentFrame);

   // moved 10/20/09
   SelectThru();

   getSystemAPI()->refreshFrame();    // NOT refreshFrameCached() - don't know why
}

// It appears that the "fast" mode switches are intended for playing back
// macros, where you don't need all the GUI stuff changing
void CPaintTool::SelectPaintModeFast(int newmode)
{
   switch(newmode) {

      case PAINT_MODE_REVEAL:

         SelectRevealModeFast();
         break;

      case PAINT_MODE_CLONE:

         SelectCloneModeFast();
         break;

      case PAINT_MODE_COLOR:

         SelectColorModeFast();
         break;

      case PAINT_MODE_ORIG:

         SelectOrigValuesModeFast();
         break;

      case PAINT_MODE_ALTCLIP:

         SelectAltClipModeFast();
         break;

      default:

         MTIassert(false);
         SelectRevealModeFast();
         break;
   }
}

void CPaintTool::SelectPaintMode(int newmode)
{
   switch(newmode) {

      case PAINT_MODE_REVEAL:

         SelectRevealMode();
         break;

      case PAINT_MODE_CLONE:

         SelectCloneMode();
         break;

      case PAINT_MODE_COLOR:

         SelectColorMode();
         break;

      case PAINT_MODE_ORIG:

         SelectOrigValuesMode();
         break;

      case PAINT_MODE_ALTCLIP:

         SelectAltClipMode();
         break;

      default:

         MTIassert(false);
         SelectRevealMode();
         break;
   }
}

void CPaintTool::SelectOnionSkinMode(int onionmode, bool update)
{
   if (((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
        (alignState != ALIGNSTATE_TRACK_SHOWING_BOTH))||
       (majorState == MAJORSTATE_EDIT_MASK)) {

      revealModeOnionSkinMode = onionmode;

      if (update)
         PaintToolForm->SetOnionSkinMode(onionmode);
   }
   else if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
            (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)||
            (majorState == MAJORSTATE_EDIT_TRANSFORM)||
            (majorState == MAJORSTATE_MASK_TRANSFORM)||
            (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)) {

      // QQQ ?????????????? Doesn't reset it?
      if (onionmode == ONIONSKIN_MODE_OFF)
         return;

      if (majorState != MAJORSTATE_REVEAL_MODE_HOME ||
          alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {

         getSystemAPI()->setOnionSkinMode(onionmode);
      }

      PaintToolForm->UpdateOnionSkinTargetWgt(0.50);

      revealModeOnionSkinMode = onionmode;

      if (update)
         PaintToolForm->SetOnionSkinMode(onionmode);
   }
   else if ((majorState == MAJORSTATE_ORIG_MODE_HOME)||
            (majorState == MAJORSTATE_SELECTING_ORIG_FRAME)) {

      if (onionmode == ONIONSKIN_MODE_OFF)
         return;

      if (majorState != MAJORSTATE_ORIG_MODE_HOME)
         getSystemAPI()->setOnionSkinMode(onionmode);

      PaintToolForm->UpdateOrigOnionSkinTargetWgt(0.50);

      origValuesModeOnionSkinMode = onionmode;

      if (update)
         PaintToolForm->SetOrigOnionSkinMode(onionmode);
   }
   else if ((majorState == MAJORSTATE_ALTCLIP_MODE_HOME)||
            (majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME)||
            (majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE)) {

      if (onionmode == ONIONSKIN_MODE_OFF)
         return;

      if (majorState != MAJORSTATE_ALTCLIP_MODE_HOME)
         getSystemAPI()->setOnionSkinMode(onionmode);

      PaintToolForm->UpdateAltClipOnionSkinTargetWgt(0.50);

      altClipModeOnionSkinMode = onionmode;

      if (update)
         PaintToolForm->SetOrigOnionSkinMode(onionmode);  // xyzzy
   }
}

void CPaintTool::ToggleColorPosNeg()
{
   int onionskinmode = PaintToolForm->GetOnionSkinMode();
   if (onionskinmode == ONIONSKIN_MODE_COLOR) {
      onionskinmode = ONIONSKIN_MODE_POSNEG;
   }
   else if (onionskinmode == ONIONSKIN_MODE_POSNEG) {
      onionskinmode = ONIONSKIN_MODE_COLOR;
   }

   int origonionskinmode = PaintToolForm->GetOrigOnionSkinMode();
   if (origonionskinmode == ONIONSKIN_MODE_COLOR) {
      origonionskinmode = ONIONSKIN_MODE_POSNEG;
   }
   else if (origonionskinmode == ONIONSKIN_MODE_POSNEG) {
      origonionskinmode = ONIONSKIN_MODE_COLOR;
   }

   int altcliponionskinmode = PaintToolForm->GetAltClipOnionSkinMode();
   if (altcliponionskinmode == ONIONSKIN_MODE_COLOR) {
      altcliponionskinmode = ONIONSKIN_MODE_POSNEG;
   }
   else if (altcliponionskinmode == ONIONSKIN_MODE_POSNEG) {
      altcliponionskinmode = ONIONSKIN_MODE_COLOR;
   }

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_EDIT_MASK)) {

      PaintToolForm->UpdateOnionSkinTargetWgt(0.50);

      revealModeOnionSkinMode = onionskinmode;

      PaintToolForm->SetOnionSkinMode(onionskinmode);

   }
   else if ((majorState == MAJORSTATE_EDIT_TRANSFORM)||
            (majorState == MAJORSTATE_CHOOSE_AUTOALIGN_PT)||
            (majorState == MAJORSTATE_MASK_TRANSFORM)) {

      PaintToolForm->UpdateOnionSkinTargetWgt(0.50);

      revealModeOnionSkinMode = onionskinmode;

      SelectOnionSkinMode(onionskinmode, true);

      goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

      PaintToolForm->UpdateOrigOnionSkinTargetWgt(0.50);

      origValuesModeOnionSkinMode = origonionskinmode;

      PaintToolForm->SetOrigOnionSkinMode(origonionskinmode);
   }
   else if (majorState == MAJORSTATE_SELECTING_ORIG_FRAME) {

      PaintToolForm->UpdateOrigOnionSkinTargetWgt(0.50);

      origValuesModeOnionSkinMode = origonionskinmode;

      SelectOnionSkinMode(origonionskinmode, true);

      goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

      PaintToolForm->UpdateAltClipOnionSkinTargetWgt(0.50);

      altClipModeOnionSkinMode = altcliponionskinmode;

      PaintToolForm->SetAltClipOnionSkinMode(altcliponionskinmode);
   }
   else if (majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME||
            majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE) {

      PaintToolForm->UpdateAltClipOnionSkinTargetWgt(0.50);

      altClipModeOnionSkinMode = altcliponionskinmode;

      SelectOnionSkinMode(altcliponionskinmode, true);

      goToTargetFrame();
   }
}

void CPaintTool::SelectThru()
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      PaintToolForm->ThruButton->Down  = true;
      PaintToolForm->EraseButton->Down = false;

      PaintToolForm->SetBrushMode(BRUSH_MODE_THRU);
      PaintToolForm->ResetCurrentBrush();

      GetToolProgressMonitor()->SetStatusMessage("Paint pixels");
   }
   else if (majorState == MAJORSTATE_ORIG_MODE_HOME) {

      PaintToolForm->ThruButton->Down  = true;
      PaintToolForm->EraseButton->Down = false;

      PaintToolForm->SetBrushMode(BRUSH_MODE_ORIG);
      PaintToolForm->ResetCurrentBrush();

      GetToolProgressMonitor()->SetStatusMessage("Paint original pixels");
   }
}

void CPaintTool::SelectErase()
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)||
       (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      PaintToolForm->EraseButton->Down = true;
      PaintToolForm->ThruButton->Down  = false;

      PaintToolForm->SetBrushMode(BRUSH_MODE_ERASE);
      PaintToolForm->ResetCurrentBrush();

      //PaintToolForm->Label1->Caption = "Erase pixels" ;
      GetToolProgressMonitor()->SetStatusMessage("Erase pixels");
   }
}

#if 0
void CPaintTool::ToggleThruErase()
{
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)||
       (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      int brmode = PaintToolForm->GetBrushMode();

      if ((brmode == BRUSH_MODE_THRU)||
          (brmode == BRUSH_MODE_ORIG)) {

         SelectErase();
      }
      else if (brmode == BRUSH_MODE_ERASE) {

         SelectThru();
      }
   }
}
#endif

void CPaintTool::StartHealing()
{
   // get ready
   setMajorState(MAJORSTATE_HEAL_PIXELS);
   PaintToolForm->setMajorState(MAJORSTATE_HEAL_PIXELS);

   // get set
   goToTargetFrame();

   // go!
   EnableHealer(true);

   // set busy cursor
   getSystemAPI()->setMouseMinorState(HEALING_PIXELS);
}

void CPaintTool::StopHealing()
{
   // set default cursor
   Screen->Cursor = crDefault;

   // stop!
   EnableHealer(false);

   // get set
   goToTargetFrame();

   // get ready
   setMajorState(MAJORSTATE_COLOR_MODE_HOME);
   PaintToolForm->setMajorState(MAJORSTATE_COLOR_MODE_HOME);

   // KKKKK code -1 used for FILL
   getSystemAPI()->pushStrokeFromTarget(-1);

   // turn on accept/reject
   ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
   PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
}

void CPaintTool::SelectAbsolute()
{
   getSystemAPI()->setImportMode(IMPORT_MODE_ABSOLUTE);
   //PaintToolForm->ReferenceFrameLockButton->Down = true;
}

void CPaintTool::SelectRelative()
{
   getSystemAPI()->setImportMode(IMPORT_MODE_RELATIVE);
   //PaintToolForm->ReferenceFrameOffsetButton->Down = true;
}

void CPaintTool::ToggleAbsoluteRelative()
{
   int newmode = IMPORT_MODE_ABSOLUTE;
   if (getSystemAPI()->getImportMode() == IMPORT_MODE_ABSOLUTE)
      newmode = IMPORT_MODE_RELATIVE;
   getSystemAPI()->setImportMode(newmode);
}

void CPaintTool::ToggleCloneAbsoluteRelative()
{
   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      bool isRel = PaintToolForm->CloneBrushIsRelative();

      PaintToolForm->SetCloneBrushRelative(!isRel);
      SetCloneBrushRelative(!isRel);
      goToTargetFrame();
   }
}

void CPaintTool::SetImportFrameMode(int impmod)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeImportFrameMode = impmod;

      break;

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         cloneModeImportFrameMode = impmod;

      break;

      default:
      break;
   }
}

void CPaintTool::SetImportFrameTimecode(int imptim)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeImportFrame = imptim;

//         DBTRACE(revealModeImportFrame);

      break;

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         cloneModeImportFrame = imptim;

      break;

      case MAJORSTATE_ALTCLIP_MODE_HOME:
      case MAJORSTATE_PAINT_ALTCLIP_PIXELS:

         altClipModeImportFrame = imptim;

      break;

      default:
      break;
   }
}

void CPaintTool::SetImportFrameOffset(int impoff)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeImportFrameOffset = impoff;

      break;

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         cloneModeImportFrameOffset = impoff;

      break;

      case MAJORSTATE_ALTCLIP_MODE_HOME:
      case MAJORSTATE_PAINT_ALTCLIP_PIXELS:

         altClipModeImportFrameOffset = impoff;

      break;

      default:
      break;
   }
}

void CPaintTool::SetRotation(double rot)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeRotation = rot;

      break;

      default:
      break;
   }
}

void CPaintTool::SetSkew(double skw)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeSkew = skw;

      break;

      default:
      break;
   }
}

void CPaintTool::SetStretchX(double hstr)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeStretchX = hstr;

      break;

      default:
      break;
   }
}

void CPaintTool::SetStretchY(double vstr)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeStretchY = vstr;

      break;

      default:
      break;
   }
}

void CPaintTool::SetOffsetX(double offx)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeOffsetX = offx;

      break;

      default:
      break;
   }
}

void CPaintTool::SetOffsetY(double offy)
{
   switch(majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
      case MAJORSTATE_POSITION_IMPORT_FRAME:
      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_EDIT_TRANSFORM:

         revealModeOffsetY = offy;

      break;

      default:
      break;
   }
}

int CPaintTool::GetImportFrameMode()
{
   int retVal = revealModeImportFrameMode;

   switch(majorState) {

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         retVal = cloneModeImportFrameMode;

      break;

      default:
      break;
   }

   return retVal;
}

int CPaintTool::GetImportFrameTimecode()
{
   int retVal = revealModeImportFrame;

   switch(majorState) {

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         retVal = cloneModeImportFrame;

      break;

      default:
      break;
   }

   return retVal;
}

int CPaintTool::GetImportFrameOffset()
{
   int retVal = revealModeImportFrameOffset;

   switch(majorState) {

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_CHOOSE_CLONE_PT:
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
      case MAJORSTATE_MOVE_CLONE_PT:
      case MAJORSTATE_CLONE_PIXELS:

         retVal = cloneModeImportFrameOffset;

      break;

      default:
      break;
   }

   return retVal;
}

double CPaintTool::GetRotation()
{
   return revealModeRotation;
}

double CPaintTool::GetSkew()
{
   return revealModeSkew;
}

double CPaintTool::GetStretchX()
{
   return revealModeStretchX;
}

double CPaintTool::GetStretchY()
{
   return revealModeStretchY;
}

double CPaintTool::GetOffsetX()
{
   return revealModeOffsetX;
}

double CPaintTool::GetOffsetY()
{
   return revealModeOffsetY;
}

void CPaintTool::SelectProcessed()
{
   // If the playback filter has "Original Values" set, turn it off.
   int playbackFilter = getSystemAPI()->getPlaybackFilter();
   if ((playbackFilter & PLAYBACK_FILTER_ORIGINAL_VALUES) != 0)
   {
      getSystemAPI()->setPlaybackFilter(playbackFilter &~ PLAYBACK_FILTER_ORIGINAL_VALUES);
   }

   getSystemAPI()->setDisplayProcessed(true);

   showState = SHOW_PROCESSED;

   PaintToolForm->ViewLabel->Caption = "Processed";
   PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
}

void CPaintTool::SelectOriginal()
{
   getSystemAPI()->setDisplayProcessed(false);

   showState = SHOW_ORIGINAL;

   PaintToolForm->ViewLabel->Caption = "Original";
   PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
}

void CPaintTool::ToggleProcessedOriginal()
{
   if (showState == SHOW_PROCESSED)
   {
      SelectOriginal();
   }
   else
   {
      SelectProcessed();
   }

   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
   if (targetFrame != -1)
   {
      getSystemAPI()->goToFrameSynchronous(targetFrame);
   }
}

void CPaintTool::ToggleShowImportFrameDiffs()
{
   int wasSelected = getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS;
   getSystemAPI()->setPlaybackFilter(wasSelected ? PLAYBACK_FILTER_NONE : PLAYBACK_FILTER_IMPORT_FRAME_DIFFS);
}

bool CPaintTool::EnableAcceptReject()
{
   return (showState == SHOW_PROCESSED)&&getSystemAPI()->targetFrameIsModified();
}

bool CPaintTool::ShouldImportFrameTimecodeBeRed(int frameIndex)
{
   bool retVal = false;

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
             ((alignState == ALIGNSTATE_TRACK_SHOWING_TARGET)||
              (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH))) {

      retVal = !hasFrameBeenTracked(frameIndex);
   }

   return retVal;
}

int CPaintTool::RenamePendingAsPrevious()
{
   DeleteFile(previousFilename.c_str());
   int retVal = RenameFile(pendingFilename.c_str(), previousFilename.c_str());

   // check if the new one is non-empty
   PaintToolForm->PlayStrokeSpeedButton->Enabled = IsPreviousStrokeChannelNonEmpty();

   return retVal;
}

int CPaintTool::AcceptProcessedTargetFrame(bool refreshtarget)
{
//      DBTRACE(importFrame);

#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 6994");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

//      DBTRACE(importFrame);
//      DBTRACE(targetFrame);

	if (targetFrame == -1)
	{
		return 0;
	}

	if (PaintToolForm->PendingStrokesListBox->Count > 0)
	{
		repeatMacroBegFrame = targetFrame;

		int retVal;

		// VVV Do NOT rename the stroke file here - make the caller do it if it's needed!
		retVal = AcceptTargetFrameModifications(majorState == MAJORSTATE_ORIG_MODE_HOME);

		if (retVal != 0)
		{
			return retVal;
		}

		// now get set to continue - the reload
      // does NOT change the Displayer alignState
      getSystemAPI()->reloadModifiedTargetFrame();

      // QQQ SHOULDN'T THIS BE
      // majorState == MAJORSTATE_REVEAL_MODE_HOME || majorState == MAJORSTATE_CLONE_MODE_HOME
      if (majorState != MAJORSTATE_ORIG_MODE_HOME
      && majorState != MAJORSTATE_ALTCLIP_MODE_HOME
      && importFrame == targetFrame)
      {
         getSystemAPI()->reloadModifiedImportFrame();
      }

      // clear the stroke stack
      getSystemAPI()->clearStrokeStack();
   }

   PaintToolForm->ShowAcceptMessage();

   if (showState != SHOW_PROCESSED) {
      SelectProcessed();
   }

   // refresh the frame conditionally
   if (refreshtarget)
      getSystemAPI()->goToFrameSynchronous(targetFrame);

   return 0;
}

// this is the conditional AutoAccept sequence
int CPaintTool::AutoAcceptConditional()
{
	int retVal = 0;

	// logic for Auto Accept
	int currentFrame = getSystemAPI()->getLastFrameIndex();

#ifdef INSTR
	if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame))
	{
		_MTIErrorDialog("TARGET FRAME INCONSISTENCY line 7058");
	}
#else
	getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

	if (autoAccept && (currentFrame != targetFrame) && (PaintToolForm->PendingStrokesListBox->Count > 0))
	{
		// pending.str becomes previous.str
		RenamePendingAsPrevious();

		retVal = AcceptTargetFrameModifications(false);
		if (retVal != 0)
		{ // write media error

			return retVal;
		}

		// needed so that old target comes up unmodified
		getSystemAPI()->reloadModifiedTargetFrame();

		// current frame is different from old target, but
		// import frame may be the same as old target, so
		// it must reflect the modifications just made
		//
		if (importFrame == targetFrame)
		{
			getSystemAPI()->reloadModifiedImportFrame();
		}

		// reload the current frame from media
		getSystemAPI()->loadTargetAndImportFrames(currentFrame);

		// get the new target and import frames
//      DBTRACE(importFrame);

#ifdef INSTR
		if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame))
		{
			_MTIErrorDialog("TARGET FRAME INCONSISTENCY line 7109");
		}
#else
		getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif
//      DBTRACE(importFrame);
//      DBTRACE(targetFrame);

	}

	return retVal;
}

int CPaintTool::AcceptTargetFrameModifications(bool doingGOV)
{
	int retVal;

	// Not really sure what this comment is referring to!
	// if there are modifications, accept them - but if the
	// net effect of thru & erase strokes is nothing, don't
	MTI_UINT16 *targetIntermediate = getSystemAPI()->getTargetIntermediate();
	MTI_UINT16 *targetOriginalIntermediate = getSystemAPI()->getTargetOriginalIntermediate();

	if (doingGOV)
	{

		CPixelRegionList nullList;
		CPixelRegionList& originalPixels = nullList;

		retVal = getSystemAPI()->putToolFrame(targetIntermediate, targetOriginalIntermediate, originalPixels, targetFrame);
	}
	else
	{
		// REVEAL, CLONE, or COLOR stroke
		MTI_UINT16 *targetPreStrokeIntermediate = getSystemAPI()->getTargetPreStrokeIntermediate();

		// ...and this is done in the Player
		retVal = getSystemAPI()->putToolFrame(targetOriginalIntermediate, targetIntermediate, targetPreStrokeIntermediate, targetFrame);
	}

	if (retVal != 0)
	{
		// write media error
		// refresh the frame
		getSystemAPI()->goToFrameSynchronous(targetFrame);
	}

	return retVal;
}

int CPaintTool::RejectProcessedTargetFrame()
{
   int targetFrame = getTargetFrame();

   if (targetFrame == -1) return 0;

   // now get set to continue - the reload
   // does NOT change the Displayer minorState
   getSystemAPI()->reloadUnmodifiedTargetFrame();

   // grey these out for the unmodified target
   PaintToolForm->RejectButton->Enabled = false;
   PaintToolForm->AcceptButton->Enabled = false;

   // clear stroke stack, clear the GUI listbox
   getSystemAPI()->clearStrokeStack();

   // grey these out for the cleared stroke stack
   PaintToolForm->UndoLastButton->Enabled       = false;
   PaintToolForm->UndoThruSelectButton->Enabled = false;

   // rewind the pending strokes dataset
   RewindPendingStrokeCaptureChannel(0);

   // grey out the "~" button
   PaintToolForm->PlayStrokeSpeedButton->Enabled = IsPreviousStrokeChannelNonEmpty();

   // have target and import frames
   PaintToolForm->ShowRejectMessage();

   // refresh the frame
   getSystemAPI()->goToFrameSynchronous(targetFrame);

   return 0;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CPaintTool::onNewMarks()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   int retVal;

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_CLONE_MODE_HOME)||
		 (majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_ORIG_MODE_HOME)||
       (majorState == MAJORSTATE_ALTCLIP_MODE_HOME)) {

      PaintToolForm->RangeStrokeSpeedButton->Enabled =
           (PaintToolForm->PendingStrokesListBox->Count > 0)&&
			  MarksAreValid();
   }

   // Tracking info is no longer valid!
   // Just change the light instead
	setTrackingRange();
	PaintToolForm->UpdateTrackingDataValidLight();

   // if tracked macro is possible enable the checkbox
   PaintToolForm->TrackedMacroCheckBox->Enabled = TrackedMacroIsEnabled();

   return 0;
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CPaintTool::onChangingClip()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   int retVal;

   // auto-accept any pending strokes
	AcceptProcessedTargetFrame(false);

   retVal = writeImportFrameAndOffset();
   if (retVal != 0) return retVal;

   retVal = PaintToolForm->WritePaletteColors();
   if (retVal != 0) return retVal;

   return 0;
}

// ===================================================================
//
// Function:    onNewClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CPaintTool::onNewClip()
{
   // Frickin' toolActivate calls this method. I would like to restructure all
   // this init stuff because toolActivate itself calls InitClip() then it
   // calls here, where we also call InitClip()! But that's way too risky
   // so for now I have added this hack flag to prevent the macro from getting
   // cleared.
   onNewClipInternal(false);
   return 0;
}

int CPaintTool::onNewClipInternal(bool itsNotReallyANewFrickinClip)
{
   // We update the grain presets whether or not this tool is active.
   string clipName = getSystemAPI()->getClipFilename();
   int frameWidth = 0;
   int frameHeight = 0;
   int bitsPerComponent = 0;
	bool isDataReallyHalfs = false;
	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat != NULL)
   {
      frameWidth = imageFormat->getTotalFrameWidth();
      frameHeight = imageFormat->getTotalFrameHeight();
      bitsPerComponent = imageFormat->getBitsPerComponent();
		isDataReallyHalfs = imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR;
	}

   GetGrainPresets().SetClipData(clipName, frameWidth, frameHeight, bitsPerComponent, !isDataReallyHalfs);

   // hmm was this
   //if (!IsToolVisible())
      //return TOOL_RETURN_CODE_EVENT_CONSUMED;
   if (!(IsLicensed() && IsShowing())) //NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;

   // Tracking info is no longer valid if we actually are changing clips!
   if (!itsNotReallyANewFrickinClip)
   {
      ResetTracking();
   }

	// pop out of tracking mode
	if (majorState == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED)
	{
      LeaveEditTrackingState();
   }

   // Do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   int retVal;

   // use of the provisional buffer for writeback
   // mandates clearing the provisional frame index --
   // otherwise the Player may pass the provisional
   // buffer back to Paint instead of a clip frame!
   getSystemAPI()->ClearProvisional(false);

   // important flag settings
   getSystemAPI()->SetMaskAllowed(true);
   getSystemAPI()->EnableMaskOutlineDisplay(true);
   getSystemAPI()->enableBrushDisplay(true);
   getSystemAPI()->enableForceTargetLoad(false);
   getSystemAPI()->enableForceImportLoad(false);

   // set up the new clip
   retVal = InitClip(itsNotReallyANewFrickinClip);
   if (retVal != 0)
      return TOOL_RETURN_CODE_NO_ACTION;

   // read the palette colors and update localRGB
   // -- needed before first call to LoadBrush
   PaintToolForm->ReadPaletteColors();

   // based on max brush radius (set in InitClip), adjust brush sizes
   adjustBrushSizesForClip(PaintToolForm->GetBrushes(), maxBrushRadius);

   // draws the whole upper half of the GUI
   PaintToolForm->InitGUIBrushPanel();

   // read import frame and offset data
   retVal = readImportFrameAndOffset();
   if (retVal != 0)
      return TOOL_RETURN_CODE_NO_ACTION;

   // enter the initial mode
   SelectPaintMode(paintMode);

   // belongs here
   getSystemAPI()->clearPaintFrames();

   // refresh the display
   getSystemAPI()->refreshFrame();

   // this allows the tool to notify the gui
   ClipChange.Notify();

   // ensure mouse wheel adjusts brush radius
   PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CPaintTool::onNewFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   getSystemAPI()->ClearProvisional(true);

   //DestroyAllToolSetups(false);

   // Tracking info is no longer valid!
   ResetTracking();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CPaintTool::toolShow()
{
   ShowPaintToolGUI();

   if (!isItOkToUseToolNow())
      getSystemAPI()-> DisableTool(getToolDisabledReason());

   return 0;
}

bool CPaintTool::IsShowing(void)
{
   return(IsToolVisible());
}

bool CPaintTool::NotifyGOVStarting(bool *cancelStretch)
{
   // must be in one of these states
   if ((majorState != MAJORSTATE_REVEAL_MODE_HOME)&&
       (majorState != MAJORSTATE_CLONE_MODE_HOME)&&
       (majorState != MAJORSTATE_COLOR_MODE_HOME)&&
       (majorState != MAJORSTATE_ORIG_MODE_HOME)&&
       (majorState != MAJORSTATE_ALTCLIP_MODE_HOME)) {

      return false;
   }

   // frame you're on
   int currentFrame = getSystemAPI()->getLastFrameIndex();
   if (currentFrame == -1)
      return false;

   // current target and import frames

#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 7370");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

   if (targetFrame == -1)
      return false;

   // key is whether target frame is modified
   bool targetModified = getSystemAPI()->targetFrameIsModified();

   bool enterGOVMode = false;

	// if not on target frame, do an autoaccept of any changes
	if (currentFrame != targetFrame)
	{
		if (targetModified)
		{
			if (!AcceptTargetFrameModifications(false))
			{
				return false;
         }
		}

		enterGOVMode = true;
	}
	else { // you're on the target frame

		// if target frame has been modified, drop into Quick-Erase mode
		if (targetModified && quickEraseStartIsPending) {

			prevBrushMode = PaintToolForm->GetBrushMode();

			SelectErase();

			BeginPaintOrQuickErase();

			getSystemAPI()->refreshFrameCached(true);
		}
		else { // target frame not modified (net effect of strokes is null!)

			enterGOVMode = true;
		}
	}
	quickEraseStartIsPending = false;

   // not on target frame, or you are & there are no net mods to it
   if (enterGOVMode) {

      // clear the stroke stack in any case
      getSystemAPI()->clearStrokeStack();

      // Save some state to restore when GOV is done.
      _oldPlaybackFilter = (_oldPlaybackFilter == PLAYBACK_FILTER_INVALID) ? getSystemAPI()->getPlaybackFilter() : _oldPlaybackFilter;
      if (_oldPlaybackFilter == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
      {
          getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
      }

      // set DisplayFrame to work normally
      getSystemAPI()->exitPaintMode();

      // switch PAINT to this state
      setMajorState(MAJORSTATE_GET_ORIGINAL_VALUES);
      PaintToolForm->setMajorState(MAJORSTATE_GET_ORIGINAL_VALUES);

      return true;
   }

   return false;
}

void CPaintTool::NotifyGOVDone()
{
   MTIassert(majorState == MAJORSTATE_GET_ORIGINAL_VALUES);

   // back into PAINT
   getSystemAPI()->enterPaintMode();

   // restore context
   setMajorState(nextMajorState);
   PaintToolForm->setMajorState(nextMajorState);

   // this was disrupted by stretch rectangle
   getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

   // we'll need the Provisional buffer now
   getSystemAPI()->ClearProvisional(false);

   // forces reload of target and import frames
   getSystemAPI()->clearPaintFrames();

   // Restore playback filter.
   getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
   _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;

   // reload the current frame from media
   int currentFrame = getSystemAPI()->getLastFrameIndex();
   getSystemAPI()->loadTargetAndImportFrames(currentFrame);

   // set these up and you're ready

#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 7489");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

   // Update status
   needToClearProgressMonitor = true;

   // restore the clone brush if needed
   if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

      ReloadCloneBrush();
   }

   getSystemAPI()->refreshFrameCached(true);
}

bool CPaintTool::NotifyTrackingStarting()
{
	if (!IsItOkToStartTracking())
	{
		return false;
	}

	setMajorState(MAJORSTATE_TRACKING_IN_PROGRESS);
	PaintToolForm->setMajorState(MAJORSTATE_TRACKING_IN_PROGRESS);
	GetToolProgressMonitor()->SetStatusMessage("Tracking in progress");

	// THIS MUST BE CALLED AFTER SETTING MAJORSTATE!
	PaintToolForm->TrackingPointsChanged(nullptr);

   return true;
}

void CPaintTool::NotifyTrackingDone(int errorCode)
{
   CAutoErrorReporter autoErr("CPaintTool::NotifyTrackingDone",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (majorState != MAJORSTATE_TRACKING_PAUSED)
	{
		setMajorState(MAJORSTATE_TRACKING_DONE);
		PaintToolForm->setMajorState(MAJORSTATE_TRACKING_DONE);
		PaintToolForm->UpdateTrackingPointButtons();
		GetToolProgressMonitor()->SetStatusMessage("Tracking stopped");
	}

   if (errorCode != 0)
	{
      autoErr.errorCode = errorCode;
      autoErr.msg << "Tracking Failed!    ";
   }
   else {

      ComputeRobustPoints();
      setTrackingAlignmentOffsets();
      bNeedToReTrack = false;
      PaintToolForm->UpdateTrackingDataValidLight();
   }
}

void CPaintTool::NotifyRightClicked()
{
   if (getSystemAPI()->targetFrameIsModified()&&
       quickEraseStartIsPending) {

      prevBrushMode = PaintToolForm->GetBrushMode();
      SelectErase();
      BeginPaintOrQuickErase();
      getSystemAPI()->refreshFrameCached(true);

      HandleSomeButtonUps();

      if ((prevBrushMode == BRUSH_MODE_THRU)||
          (prevBrushMode == BRUSH_MODE_ORIG)) {

          SelectThru();
      }

      quickEraseStartIsPending = false;
   }
}
// ===================================================================
//
// Function:    toolHideQuery
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

bool CPaintTool::toolHideQuery()
{
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   return(HidePaintToolGUIQuery());
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CPaintTool::toolHide()
{
   HidePaintToolGUI();
   doneWithTool();
   return(0);
}

//---------------------------------------------------------------------------

void CPaintTool::SetMaskTransformEditorIdleRefreshMode()
{
   // Tell mask transform editor to always refresh display on mouse moves
   getSystemAPI()->SetMaskTransformEditorIdleRefreshMode(true);
}

//---------------------------------------------------------------------------

int CPaintTool::InitClip(bool dontClearTheFrickinMacro)
{
   // values on failure

   clipBinPath = "";
   clipName = "";
   previousFilename = "";
   pendingFilename = "";

   curFrameRect.left   = 0;
   curFrameRect.top    = 0;
   curFrameRect.right  = 720;
   curFrameRect.bottom = 486;

   minFrame = -1;
   maxFrame = -1;

   // now get what's needed

   curClip = getSystemAPI()->getClip();
   if (curClip == nullptr)
      return -1;

   curImageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (curImageFormat == nullptr)
      return -1;

   curFrameList = getSystemAPI()->getVideoFrameList();
   if (curFrameList == nullptr)
      return -1;

   // get the bin path
   clipBinPath = curClip->getBinPath();
   int leng = clipBinPath.size();
   if (clipBinPath[leng-1] != '\\') {
      clipBinPath = clipBinPath + "\\";
   }

   // and clip name
   clipName    = curClip->getClipName();

   // init the paint history for painting original values
   getSystemAPI()->initPaintHistory();

   // make the stroke stack for saving/restoring pixels
   getSystemAPI()->makeStrokeStack();

   // for macro feature
   // Only clear the saved macro if we're reall changing clips
   // (not when activating the tool!)
   previousFilename = clipBinPath + clipName + "\\" + "previous.str";
   if (!dontClearTheFrickinMacro)
   {
      truncate64(previousFilename.c_str(), 0);
   }

   pendingFilename = clipBinPath + clipName + "\\" + "pending.str";
   truncate64(pendingFilename.c_str(), 0);

   // for clone pt and brush radius
   curFrameRect = curImageFormat->getActivePictureRect();

   // max brush radius
   maxBrushRadius = (curFrameRect.right - curFrameRect.left + 1)/2;

   // frame limits of the clip
   minFrame = curFrameList->getInFrameIndex();
   maxFrame = curFrameList->getOutFrameIndex() - 1;

   // with 4K material use 2X tracking box and search range
   bool large = (curImageFormat->getPixelsPerLine() > 2048);

   // set up Tracking trackbar
   int boxSize = TRACKING_BOX_DEFAULT;
   int min = TRACKING_BOX_MIN;
   int max = TRACKING_BOX_MAX;
   if (large) {
      boxSize *= 2;
      min *= 2;
      max *= 2;
   }
   PaintToolForm->InitTrackingBoxSizeTrackBar(min, max, boxSize);

   // set up Search trackbar
   int searchSize = SEARCH_RANGE_DEFAULT;
   min = SEARCH_RANGE_MIN;
   max = SEARCH_RANGE_MAX;
   if (large) {
      searchSize *= 2;
      min *= 2;
      max *= 2;
   }
   PaintToolForm->InitTrackingMotionSearchTrackBar(min, max, searchSize);

   SetTrackingBoxSizes(boxSize, boxSize, searchSize, searchSize, searchSize, searchSize);

   SetAltSourceClipInfo(ReadAlternateSourceClipNameFromDesktopIni(), ReadAlternateSourceClipOffsetFromDesktopIni());

   return 0;
}

//---------------------------------------------------------------------------
void CPaintTool::WriteAlternateSourceClipNameToDesktopIni(const string &alternateSourceClipName)
{
   // Write the Alternate Source Clip name to the desktop.ini file.
   CBinManager binManager;
   CIniFile *iniFile = binManager.openClipDesktop(curClip);
   if (!iniFile)
   {
         return;
   }

   iniFile->WriteString("ClipLinks", "AltSourceClipName", alternateSourceClipName);

   DeleteIniFile(iniFile);
}

//---------------------------------------------------------------------------
string CPaintTool::ReadAlternateSourceClipNameFromDesktopIni()
{
   // Read the Alternate Source Clip name from the desktop.ini file.
   CBinManager binManager;
   CIniFile *iniFile = binManager.openClipDesktop(curClip);
   if (!iniFile)
   {
         return "";
   }

   string altSourceClipName = iniFile->ReadString("ClipLinks", "AltSourceClipName", "");

   DeleteIniFile(iniFile);

   return altSourceClipName;
}

//---------------------------------------------------------------------------
void CPaintTool::WriteAlternateSourceClipOffsetToDesktopIni(int alternateSourceClipOffset)
{
   // Write the Alternate Source Clip offset to the desktop.ini file.
   CBinManager binManager;
   CIniFile *iniFile = binManager.openClipDesktop(curClip);
   if (!iniFile)
   {
         return;
   }

   iniFile->WriteInteger("ClipLinks", "AltSourceClipOffset", alternateSourceClipOffset);

   DeleteIniFile(iniFile);
}

//---------------------------------------------------------------------------
int CPaintTool::ReadAlternateSourceClipOffsetFromDesktopIni()
{
   // Read the Alternate Source Clip offset from the desktop.ini file.
   CBinManager binManager;
   CIniFile *iniFile = binManager.openClipDesktop(curClip);
   if (!iniFile)
   {
         return InvalidAlternateSourceClipOffsetValue;
   }

   int alternateSourceClipOffset = iniFile->ReadInteger("ClipLinks", "AltSourceClipOffset", InvalidAlternateSourceClipOffsetValue);

   DeleteIniFile(iniFile);

   return alternateSourceClipOffset;
}

//---------------------------------------------------------------------------
void CPaintTool::setMajorState(int newMajorState)
{
   // maybe bounce out of "show both" mode
   if ((newMajorState != MAJORSTATE_REVEAL_MODE_HOME)&&
       (newMajorState != MAJORSTATE_PAINT_PIXELS)) {

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
          (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH)) {

         LeaveTrackingAlignShowBothMode();
      }
   }


   switch(newMajorState) {

      case MAJORSTATE_GUI_CREATED:

         majorState = newMajorState;

         // unload any target and import frames loaded
         getSystemAPI()->unloadTargetAndImportFrames();

         // no target frame yet
         targetFrame = -1;

         // no import frame yet
         importFrame = -1;

      break;

      case MAJORSTATE_REVEAL_MODE_HOME:

         if ((majorState != MAJORSTATE_REVEAL_MODE_HOME)&&
             (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH)) {

            getSystemAPI()->setOnionSkinMode(revealModeOnionSkinMode);

            // when navigating, target frame will be changed
            getSystemAPI()->setCaptureMode(CAPTURE_MODE_BOTH);
         }
         else {

            getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

            // when navigating, target frame will be changed
            getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);
         }

         majorState = newMajorState;

         // default displays mask outlines
		 // NOW CONTROLED BY BUTTON getSystemAPI()->EnableMaskOutlineDisplay(true);

         // if present mouse wheel state is incompatible, change to RADIUS
         if (!isMouseWheelStateOK()) {

            PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         }

         // Turn off red overlay.
         if (getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
         {
            getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
         }

         // display processed target frame
         SelectProcessed();

         // back to normal
         getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      break;


      case MAJORSTATE_COLOR_MODE_HOME:

         /* Fall through */

      case MAJORSTATE_CLONE_MODE_HOME:

         majorState = newMajorState;

         // default displays mask outlines
		 // NOW CONTROLED BY BUTTON getSystemAPI()->EnableMaskOutlineDisplay(true);

         // if present mouse wheel state is incompatible, change to RADIUS
         if (!isMouseWheelStateOK()) {

            PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         }

         // Turn off red overlay.
         if (getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
         {
            getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
         }

         // onionskin off
         getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

         // display processed target frame
         SelectProcessed();

         // back to normal
         getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

         // when navigating, target frame will be changed
         getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);

      break;

      case MAJORSTATE_ORIG_MODE_HOME:

         majorState = newMajorState;

         // when navigating, target frame will be changed
         getSystemAPI()->setCaptureMode(CAPTURE_MODE_ORIG_TARGET);
         getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

         // default displays mask outlines
		 // NOW CONTROLED BY BUTTON getSystemAPI()->EnableMaskOutlineDisplay(true);

         // if present mouse wheel state is incompatible, change to RADIUS
         if (!isMouseWheelStateOK()) {

            PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         }

         // display processed target frame
         SelectProcessed();

         // back to normal
         getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      break;

      case MAJORSTATE_ALTCLIP_MODE_HOME:

         majorState = newMajorState;

         // when navigating, target frame will be changed
         getSystemAPI()->setCaptureMode(CAPTURE_MODE_TARGET);
         getSystemAPI()->setImportMode(IMPORT_MODE_ALTCLIP);
         getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

         // default displays mask outlines
		 // NOW CONTROLED BY BUTTON getSystemAPI()->EnableMaskOutlineDisplay(true);

         // if present mouse wheel state is incompatible, change to RADIUS
         if (!isMouseWheelStateOK()) {

            PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
         }

         // display processed target frame
         SelectProcessed();

         // back to normal
         getSystemAPI()->setMouseMinorState(ACQUIRING_PIXELS);

      break;

      case MAJORSTATE_PAINT_PIXELS:
      case MAJORSTATE_PAINT_ALTCLIP_PIXELS:

         majorState = newMajorState;

      break;

      case MAJORSTATE_POSITION_IMPORT_FRAME:

         majorState = newMajorState;

         PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_OFFSET);

         SelectOnionSkinMode(revealModeOnionSkinMode, true);

         getSystemAPI()->setMouseMinorState(ACQUIRING_IMPORT_FRAME);

         // Turn off red overlay.
         if (getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
         {
            getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
         }

      break;

      case MAJORSTATE_EDIT_TRANSFORM:

         majorState = newMajorState;

         PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_OFFSET);

         SelectOnionSkinMode(revealModeOnionSkinMode, true);

         getSystemAPI()->setMouseMinorState(ACQUIRING_IMPORT_FRAME);

         // Turn off red overlay.
         if (getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
         {
            getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
         }

      break;

      case MAJORSTATE_CHOOSE_AUTOALIGN_PT:

         majorState = newMajorState;

         // mouse wheel to AutoAlignBox
         PaintToolForm->setMouseWheelState(MOUSE_WHEEL_ADJUST_AUTOALIGN_BOX);

         SelectOnionSkinMode(revealModeOnionSkinMode, true);

         // display the autoalign box
         getSystemAPI()->setAutoAlignBoxSize(autoAlignBoxSize);
         getSystemAPI()->setMouseMinorState(PICKING_AUTOALIGN);

      break;

      case MAJORSTATE_EDIT_MASK:

         majorState = newMajorState;

         // turn off onionskin mode
         getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

         // this'll get the white cross cursor
         getSystemAPI()->editMask();

      break;

      case MAJORSTATE_MASK_TRANSFORM:

         majorState = newMajorState;

         SelectOnionSkinMode(revealModeOnionSkinMode, true);

         // this'll get the white cross cursor
         getSystemAPI()->editMask();

      break;

      case MAJORSTATE_GET_ORIGINAL_VALUES:

         setNextMajorState(majorState);
         majorState = newMajorState;

      break;

		case MAJORSTATE_EDIT_TRACKING:
		case MAJORSTATE_TRACKING_DONE:
		case MAJORSTATE_TRACKING_PAUSED:

         setNextMajorState(majorState);
         majorState = newMajorState;

         // turn off onionskin mode
         getSystemAPI()->setOnionSkinMode(ONIONSKIN_MODE_OFF);

         // Turn off red overlay.
         if (getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
         {
            getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
         }

      break;

      case MAJORSTATE_TRACKING_IN_PROGRESS:

         majorState = newMajorState;

      break;

      default:

         majorState = newMajorState;

      break;
   }

#ifndef NDEBUG
   string stateName;

   switch (majorState) {
      case MAJORSTATE_GUI_CREATED:
         stateName = "GUI_CREATED";
         break;
      case MAJORSTATE_SELECTING_BRUSH:
         stateName = "SELECTING_BRUSH";
         break;
      case MAJORSTATE_REVEAL_MODE_HOME:
         stateName = "REVEAL_MODE_HOME";
         break;
      case MAJORSTATE_SELECTING_IMPORT_FRAME:
         stateName = "SELECTING_IMPORT_FRAME";
         break;
      case MAJORSTATE_POSITION_IMPORT_FRAME:
         stateName = "POSITION_IMPORT_FRAME";
         break;
      case MAJORSTATE_PAINT_PIXELS:
         stateName = "PAINT_PIXELS";
         break;
      case MAJORSTATE_CLONE_MODE_HOME:
         stateName = "CLONE_MODE_HOME";
         break;
      case MAJORSTATE_CHOOSE_CLONE_PT:
         stateName = "CHOOSE_CLONE_PT";
         break;
      case MAJORSTATE_CHOOSE_CLONE_PT_ALT:
         stateName = "CHOOSE_CLONE_PT_ALT";
         break;
      case MAJORSTATE_MOVE_CLONE_PT:
         stateName = "MOVE_CLONE_PT";
         break;
      case MAJORSTATE_CLONE_PIXELS:
         stateName = "CLONE_PIXELS";
         break;
      case MAJORSTATE_COLOR_MODE_HOME:
         stateName = "COLOR_MODE_HOME";
         break;
      case MAJORSTATE_PICK_COLOR:
         stateName = "PICK_COLOR";
         break;
      case MAJORSTATE_PICK_SEED:
         stateName = "PICK_SEED";
         break;
      case MAJORSTATE_COLOR_PIXELS:
         stateName = "COLOR_PIXELS";
         break;
      case MAJORSTATE_HEAL_PIXELS:
         stateName = "HEAL_PIXELS";
         break;
      case MAJORSTATE_STOPPING_HEALING:
         stateName = "STOPPING_HEALING";
         break;
      case MAJORSTATE_EDIT_TRANSFORM:
         stateName = "EDIT_TRANSFORM";
         break;
      case MAJORSTATE_CHOOSE_AUTOALIGN_PT:
         stateName = "CHOOSE_AUTOALIGN_PT";
         break;
      case MAJORSTATE_CHOOSE_AUTOALIGN_ALT:
         stateName = "CHOOSE_AUTOALIGN_ALT";
         break;
      case MAJORSTATE_EDIT_MASK:
         stateName = "EDIT_MASK";
         break;
      case MAJORSTATE_MASK_TRANSFORM:
         stateName = "MASK_TRANSFORM";
         break;
      case MAJORSTATE_BRUSH_OFF:
         stateName = "BRUSH_OFF";
         break;
      case MAJORSTATE_EXECUTING_MACRO:
         stateName = "EXECUTING_MACRO";
         break;
      case MAJORSTATE_EXECUTING_RANGE_MACRO:
         stateName = "EXECUTING_RANGE_MACRO";
         break;
      case MAJORSTATE_GET_ORIGINAL_VALUES:
         stateName = "GET_ORIGINAL_VALUES";
         break;
		case MAJORSTATE_EDIT_TRACKING:
			stateName = "EDIT_TRACKING";
			break;
		case MAJORSTATE_TRACKING_IN_PROGRESS:
			stateName = "TRACKING_IN_PROGRESS";
			break;
		case MAJORSTATE_TRACKING_DONE:
			stateName = "TRACKING_DONE";
			break;
		case MAJORSTATE_TRACKING_PAUSED:
			stateName = "TRACKING_PAUSED";
			break;
		case MAJORSTATE_ORIG_MODE_HOME:
         stateName = "ORIG_MODE_HOME";
         break;
		case MAJORSTATE_SELECTING_ORIG_FRAME:
         stateName = "SELECTING_ORIG_FRAME";
         break;
      case MAJORSTATE_PAINT_ORIG_PIXELS:
         stateName = "PAINT_ORIG_PIXELS";
         break;
		case MAJORSTATE_ALTCLIP_MODE_HOME:
         stateName = "ALTCLIP_MODE_HOME";
         break;
      case MAJORSTATE_SELECTING_ALTCLIP_FRAME:
         stateName = "SELECTING_ALTCLIP_FRAME";
         break;
      case MAJORSTATE_PAINT_ALTCLIP_COMPARE:
         stateName = "COMPARING_ALTCLIP_FRAMES";
         break;
      case MAJORSTATE_PAINT_ALTCLIP_PIXELS:
         stateName = "PAINT_ALTCLIP_PIXELS";
         break;
      default:
         stateName = "UNKNOWN STATE";
         break;
   }
   TRACE_2(errout << "New PAINT state: " << stateName);
#endif
}

int CPaintTool::getMajorState()
{
   return majorState;
}

void CPaintTool::setNextMajorState(int nextState)
{
   nextMajorState = nextState;
}

int CPaintTool::getNextMajorState()
{
   return nextMajorState;
}

void CPaintTool::setAlignState(int alnstate)
{
   alignState = alnstate;
}

int CPaintTool::getAlignState()
{
   return alignState;
}

void CPaintTool::setMouseWheelState(int mousewheelstate)
{
   mouseWheelState = mousewheelstate;
}

int CPaintTool::getMouseWheelState()
{
   return mouseWheelState;
}

bool CPaintTool::isMouseWheelStateOK()
{
   // THIS ONLY WORKS FOR MAJOR HOME STATES

   bool ok = true;

   switch (majorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:

         if (mouseWheelState == MOUSE_WHEEL_ADJUST_TOLERANCE) {

            ok = false;
         }

         break;

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_ORIG_MODE_HOME:
      case MAJORSTATE_ALTCLIP_MODE_HOME:

         if ((mouseWheelState == MOUSE_WHEEL_ADJUST_MIX)||
             (mouseWheelState == MOUSE_WHEEL_ADJUST_OFFSET)||
             (mouseWheelState == MOUSE_WHEEL_ADJUST_TOLERANCE)) {

            ok = false;
         }

         break;

      case MAJORSTATE_COLOR_MODE_HOME:

         if ((mouseWheelState == MOUSE_WHEEL_ADJUST_MIX)||
             (mouseWheelState == MOUSE_WHEEL_ADJUST_OFFSET)) {

            ok = false;
         }

         break;

      default:

         ok = true;

         break;

   }

   return ok;
}

void CPaintTool::setTargetFrame(int target)
{
   targetFrame = target;
}

int CPaintTool::getTargetFrame()
{
   return targetFrame;
}

void CPaintTool::goToTargetFrame(bool renew)
{
#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 8112");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

   if (targetFrame == -1) {
      getSystemAPI()->refreshFrame();
   }
   else if (targetFrame == getSystemAPI()->getLastFrameIndex()) {

      if (renew) {
         getSystemAPI()->goToFrameSynchronous(targetFrame);
      }
      else {
         getSystemAPI()->refreshFrameCached(true);
      }
   }
   else {
      getSystemAPI()->goToFrameSynchronous(targetFrame);
   }
}

void CPaintTool::goToImportFrame()
{
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
   getSystemAPI()->loadTargetAndImportFrames(targetFrame);

   // It seems weird, but if we are importing frames from another clip,
   // in order to show the right import frame from the other clip,
   // we need to tell the player to go to the TARGET frame!
   bool areWeImportingFromAnotherClip = getMajorState() == MAJORSTATE_ALTCLIP_MODE_HOME
                                        || getMajorState() == MAJORSTATE_PAINT_ALTCLIP_PIXELS
                                        || getMajorState() == MAJORSTATE_SELECTING_ALTCLIP_FRAME
                                        || getMajorState() == MAJORSTATE_PAINT_ALTCLIP_COMPARE;
   int whichFrameToShow = areWeImportingFromAnotherClip ? targetFrame : importFrame;
   int lastFrame = getSystemAPI()->getLastFrameIndex();

   if (lastFrame == whichFrameToShow)
   {
      getSystemAPI()->refreshFrameCached();
   }
   else
   {
      getSystemAPI()->goToFrameSynchronous(whichFrameToShow);
   }
}

bool CPaintTool::setTrackingRangePossiblyMovingTheMarks()
{
	int startFrame = getSystemAPI()->getMarkIn();
	int stopFrame = getSystemAPI()->getMarkOut();

	if  (startFrame == -1 || stopFrame == -1 || startFrame >= stopFrame)
	{
		_MTIErrorDialog("Marks are invalid.");
		return false;
	}

	if (getSystemAPI()->getImportMode() == IMPORT_MODE_RELATIVE
	&& getSystemAPI()->getImportFrameOffset() != 0)
	{
		int importFrame = getImportIndexFromTargetIndex(startFrame, majorStateBeforeTracking);
		startFrame = min<int>(startFrame, importFrame);
		importFrame = getImportIndexFromTargetIndex(stopFrame, majorStateBeforeTracking);
		stopFrame = max<int>(stopFrame, importFrame);
	}

	CVideoFrameList *vflp = getSystemAPI()->getVideoFrameList();
	if (vflp == nullptr)
	{
		// No clip loaded - can't happem.
		return false;
	}

	int frameCount = vflp->getTotalFrameCount();

	if (startFrame < 0)
	{
		int validMarkIn = -startFrame;
		MTIostringstream os;
		os << "Invalid import frame (index " << startFrame << ") at start of clip.  \n"
			<< "Move the mark in point to frame index " << validMarkIn << "?";
		switch (_MTICancelConfirmationDialog(os.str()))
		{
			case MTI_DLG_OK:
				getSystemAPI()->setMarkIn(validMarkIn);
				break;

			case MTI_DLG_CANCEL:
				return false;

			default:
			case MTI_DLG_NO:
				startFrame = 0;
				break;
		}
	}
	else if (stopFrame > frameCount)
	{
		int validMarkOut = frameCount - (stopFrame - frameCount);
		MTIostringstream os;
		os << "Invalid import frame (index " << stopFrame << ") at end of clip.  \n"
			<< "Move the mark out point to frame index " << validMarkOut << "?";
		switch (_MTICancelConfirmationDialog(os.str()))
		{
			case MTI_DLG_OK:
				getSystemAPI()->setMarkOut(validMarkOut);
				break;

			case MTI_DLG_CANCEL:
				return false;

			default:
			case MTI_DLG_NO:
				stopFrame = frameCount;
				break;
		}
	}

	// Recheck mark validity.
	if  (startFrame == -1 || stopFrame == -1 || startFrame >= stopFrame)
	{
		_MTIErrorDialog("Unable to set valid marks.");
		return false;
	}

	setTrackingRange();

	return true;
}

void CPaintTool::setTrackingRange()
{
	CVideoFrameList *vflp = getSystemAPI()->getVideoFrameList();
	if (vflp == nullptr)
	{
		// No clip loaded - can't happen.
		TrkPtsEditor->setRange(-1, -1);
		return;
	}

	// Can't track if mark in or mark out is not set or they're invalid.
	int startFrame = getSystemAPI()->getMarkIn();
	int stopFrame = getSystemAPI()->getMarkOut();
	if (startFrame == -1 || stopFrame == -1 || startFrame >= stopFrame)
	{
		TrkPtsEditor->setRange(-1, -1);
		return;
	}

	// Check for RELATIVE mode and non-zero offset, which extends the tracking range.
	if (getSystemAPI()->getImportMode() == IMPORT_MODE_RELATIVE
	&& getSystemAPI()->getImportFrameOffset() != 0)
	{
		// Start tracking at import frame if valid and earlier than the mark in frame.
		// NOTE: computeDesiredImportFrame silently returns only valid frame indices!
		//       But I leave the check in there because it doesn't hurt.
		int importFrame = getSystemAPI()->computeDesiredImportFrame(startFrame);
		if (importFrame >= 0)
		{
			startFrame = min<int>(importFrame, startFrame);
		}

		// Stop tracking after import frame if it's later than the frame before
		// the mark out frame index (stop is EXCLUSIVE).
		// NOTE: computeDesiredImportFrame silently returns only valid frame indices!
		//       But I leave the check in there because it doesn't hurt.
		importFrame = getSystemAPI()->computeDesiredImportFrame(stopFrame - 1);
		stopFrame = min<int>(max<int>(importFrame + 1, stopFrame), vflp->getTotalFrameCount());
	}

	TrkPtsEditor->setRange(startFrame, stopFrame);
}

void CPaintTool::goToTrackingPointInsertionFrame()
{
   // QQQ need to autoaccept !?!

   // insertion frame will be at mark in unless the import offset is negative
   // -- as a side effect, we will always set the target frame to be the mark
   // IN frame and set the import frame as well

   int markInFrame = getSystemAPI()->getMarkIn();

   // since everything is relative to the in mark, punt if there isn't one
   if (markInFrame == -1) {

      getSystemAPI()->refreshFrameCached();
   }
   else {  // mark IN is set

      int insertionFrame = markInFrame;

		int tntImportFrame = getSystemAPI()->computeDesiredImportFrame(markInFrame);

      // insertion frame may be the import frame
      if ((tntImportFrame != -1)&&(tntImportFrame < markInFrame)) {

         insertionFrame = tntImportFrame;
      }

      // now go to the insertion frame
      getSystemAPI()->goToFrameSynchronous(insertionFrame);
   }
}

int CPaintTool::readImportFrameAndOffset()
{
#if 0 // Larry says these should be GLOBAL, not PER-CLIP!
   string iniFileName = AddDirSeparator(clipBinPath) +
                        AddDirSeparator(clipName) +
                        "PaintClip.ini";             // QQQ Manifest constant
#else
   string iniFileName = IniFileName;
#endif
   CIniFile *iniFile = CreateIniFile(iniFileName);
   if (iniFile == nullptr) {
      TRACE_1(errout << "CPaintTool::readImportFrameAndOffset: cannot open " << iniFileName);
      return -1;
   }

   string sectionName = "ImportFrameData";

   int lastFrame = getSystemAPI()->getLastFrameIndex();

   revealModeImportFrameMode =   iniFile->ReadInteger(sectionName,
                                                      revealModeImportFrameModeKey,
                                                      IMPORT_MODE_RELATIVE);

   revealModeImportFrame =       iniFile->ReadInteger(sectionName,
                                                      revealModeImportFrameKey,
                                                      lastFrame);

   revealModeImportFrameOffset = iniFile->ReadInteger(sectionName,
                                                      revealModeImportFrameOffsetKey,
                                                      -2);

   revealModeRotation          = iniFile->ReadDouble(sectionName,
                                                     revealModeRotationKey,
                                                     0.0);

   revealModeStretchX          = iniFile->ReadDouble(sectionName,
                                                     revealModeStretchXKey,
                                                     1.0);

   revealModeStretchY          = iniFile->ReadDouble(sectionName,
                                                     revealModeStretchYKey,
                                                     1.0);

   revealModeSkew              = iniFile->ReadDouble(sectionName,
                                                     revealModeSkewKey,
                                                     0.0);

   revealModeOffsetX           = iniFile->ReadDouble(sectionName,
                                                     revealModeOffsetXKey,
                                                     0.0);

   revealModeOffsetY           = iniFile->ReadDouble(sectionName,
                                                     revealModeOffsetYKey,
                                                     0.0);

   revealModeOnionSkinMode     = iniFile->ReadInteger(sectionName,
                                                      revealModeOnionSkinModeKey,
                                                      ONIONSKIN_MODE_COLOR);

   PaintToolForm->SetOnionSkinMode(revealModeOnionSkinMode);


   cloneModeImportFrameMode =    iniFile->ReadInteger(sectionName,
                                                      cloneModeImportFrameModeKey,
                                                      IMPORT_MODE_RELATIVE);

   cloneModeImportFrame =        iniFile->ReadInteger(sectionName,
                                                      cloneModeImportFrameKey,
                                                      lastFrame);

   cloneModeImportFrameOffset =  iniFile->ReadInteger(sectionName,
                                                      cloneModeImportFrameOffsetKey,
                                                      0);

   clModePointMode             = iniFile->ReadBool(sectionName,
                                                   cloneModePointModeKey,
                                                   true);

   origValuesModeOnionSkinMode = iniFile->ReadInteger(sectionName,
                                                      origValuesModeOnionSkinModeKey,
                                                      ONIONSKIN_MODE_POSNEG);

   PaintToolForm->SetOrigOnionSkinMode(origValuesModeOnionSkinMode);

   altClipModeOnionSkinMode = iniFile->ReadInteger(sectionName,
                                                      altClipModeOnionSkinModeKey,
                                                      ONIONSKIN_MODE_POSNEG);

   PaintToolForm->SetAltClipOnionSkinMode(altClipModeOnionSkinMode);


   // xyzzy - Alt Clip mode stuff gets written to the desktop.ini - should move it to here?



   // initialize these to reasonable values in case you're on a brand-new clip
   //
   InitClonePt();

   cloneX                      = iniFile->ReadInteger(sectionName,
                                                      cloneModeXKey,
                                                      cloneX);

   cloneY                      = iniFile->ReadInteger(sectionName,
                                                      cloneModeYKey,
                                                      cloneY);

   clOffsetX                   = iniFile->ReadInteger(sectionName,
                                                      cloneModeOffsetXKey,
                                                      clOffsetX);

   clOffsetY                   = iniFile->ReadInteger(sectionName,
                                                      cloneModeOffsetYKey,
                                                      clOffsetY);

// Forget the Paint mode - always start in REVEAL mode!
//   paintMode                   = iniFile->ReadInteger(sectionName,
//                                                      paintModeKey,
//                                                      PAINT_MODE_REVEAL);

   if (paintMode < PAINT_MODE_MIN || paintMode > PAINT_MODE_MAX)
   {
      paintMode = PAINT_MODE_REVEAL;
   }

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::readImportFrameAndOffset: cannot close " << iniFileName);
   }

   return 0;
}

int CPaintTool::readGeneralSettings()
{
   CIniFile *iniFile = CreateIniFile(IniFileName);
   if (iniFile == nullptr)
   {
      TRACE_1(errout << "CPaintTool::readGeneralSettings: cannot open " << IniFileName);
      return -1;
   }

   string sectionName = "General";

   EnableAutoAccept(iniFile->ReadBool(sectionName,
                                      autoAcceptModeKey,
                                      true));
   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::readGeneralSettings: cannot close " << IniFileName);
   }

   return 0;
}

int CPaintTool::writeImportFrameAndOffset()
{
   if (curClip == nullptr)
      return 0;

   // WTF! a call is being made to "onChangingClip"
   // when there was no clip to change from - ergo
   // shit is being written back to PaintClip.ini
   if ((paintMode != PAINT_MODE_REVEAL)&&
       (paintMode != PAINT_MODE_CLONE)&&
       (paintMode != PAINT_MODE_COLOR)&&
       (paintMode != PAINT_MODE_ORIG)&&
       (paintMode != PAINT_MODE_ALTCLIP))
      return -1;

   if ((revealModeImportFrameMode != IMPORT_MODE_ABSOLUTE)&&
       (revealModeImportFrameMode != IMPORT_MODE_RELATIVE))
      return -1;

   if ((cloneModeImportFrameMode != IMPORT_MODE_ABSOLUTE)&&
       (cloneModeImportFrameMode != IMPORT_MODE_RELATIVE))
      return -1;

   /////////////////////////////////////////////////////////////////////////////

#if 0 // Larry says these should be GLOBAL, not PER-CLIP!
   string iniFileName = AddDirSeparator(clipBinPath) +
                        AddDirSeparator(clipName) +
                        "PaintClip.ini";             // QQQ Manifest constant
#else
   string iniFileName = IniFileName;
#endif
   CIniFile *iniFile = CreateIniFile(iniFileName);
   if (iniFile == nullptr) {
      TRACE_1(errout << "CPaintTool::writeImportFrameAndOffset: cannot open " << iniFileName);
      return -1;
   }

   // TODO: MOVE ALTCLIP MODE INFO HERE, instead of in desktop.ini ! QQQ

   string sectionName = "ImportFrameData";

   iniFile->WriteInteger(sectionName,
                         paintModeKey,
                         paintMode);

   iniFile->WriteInteger(sectionName,
                         revealModeImportFrameModeKey,
                         revealModeImportFrameMode);

   iniFile->WriteInteger(sectionName,
                         revealModeImportFrameKey,
                         revealModeImportFrame);

   iniFile->WriteInteger(sectionName,
                         revealModeImportFrameOffsetKey,
                         revealModeImportFrameOffset);

   iniFile->WriteDouble(sectionName,
                        revealModeRotationKey,
                        revealModeRotation);

   iniFile->WriteDouble(sectionName,
                        revealModeStretchXKey,
                        revealModeStretchX);

   iniFile->WriteDouble(sectionName,
                        revealModeStretchYKey,
                        revealModeStretchY);

   iniFile->WriteDouble(sectionName,
                        revealModeSkewKey,
                        revealModeSkew);

   iniFile->WriteDouble(sectionName,
                        revealModeOffsetXKey,
                        revealModeOffsetX);

   iniFile->WriteDouble(sectionName,
                        revealModeOffsetYKey,
                        revealModeOffsetY);

   iniFile->WriteInteger(sectionName,
                         revealModeOnionSkinModeKey,
                         revealModeOnionSkinMode);

   iniFile->WriteInteger(sectionName,
                         cloneModeImportFrameModeKey,
                         cloneModeImportFrameMode);

   iniFile->WriteInteger(sectionName,
                         cloneModeImportFrameKey,
                         cloneModeImportFrame);

   iniFile->WriteInteger(sectionName,
                         cloneModeImportFrameOffsetKey,
                         cloneModeImportFrameOffset);

   iniFile->WriteBool(sectionName,
                      cloneModePointModeKey,
                      PaintToolForm->CloneBrushIsRelative());

   iniFile->WriteInteger(sectionName,
                         cloneModeXKey,
                         cloneX);

   iniFile->WriteInteger(sectionName,
                         cloneModeYKey,
                         cloneY);

   iniFile->WriteInteger(sectionName,
                         cloneModeOffsetXKey,
                         clOffsetX);

   iniFile->WriteInteger(sectionName,
                         cloneModeOffsetYKey,
                         clOffsetY);

   iniFile->WriteInteger(sectionName,
                         origValuesModeOnionSkinModeKey,
                         origValuesModeOnionSkinMode);

   iniFile->WriteInteger(sectionName,
                         altClipModeOnionSkinModeKey,
                         altClipModeOnionSkinMode);

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::writeImportFrameAndOffset: cannot close " << iniFileName);
   }

   return 0;
}

int CPaintTool::writeGeneralSettings()
{
   CIniFile *iniFile = CreateIniFile(IniFileName);
   if (iniFile == nullptr)
   {
      TRACE_1(errout << "CPaintTool::writeGeneralSettings: cannot open " << IniFileName);
      return -1;
   }

   string sectionName = "General";

   iniFile->WriteBool(sectionName,
                      autoAcceptModeKey,
                      autoAccept);

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::writeGeneralSettings: cannot close " << IniFileName);
   }

   return 0;
}

int CPaintTool::allocateBrushesAndReadBrushParametersFromIni(CBrush *brush[], int &maxBrushRadiusOut)
{
   // create the brushes with max radius 2K
   PaintToolForm->AllocateBrushes();

   if (IniFileName.empty())
      return 1;

   CIniFile* ini = CreateIniFile(IniFileName);
   if (ini == nullptr)
      return 1;

   int curbrush = ini->ReadInteger(titlekey, currentbrushkey, 1);
   maxBrushRadiusOut = ini->ReadInteger(titlekey, maxradiuskey, 2048);

   for (int i=0;i<NUM_BRUSH;i++) {

      brush[i]->setBrushParameters( maxBrushRadiusOut,
                                    ini->ReadInteger(brushstr[i], radiuskey, 100),
                                    ini->ReadInteger(brushstr[i], blendkey,   50),
                                    ini->ReadInteger(brushstr[i], opacitykey,100),
                                    ini->ReadInteger(brushstr[i], aspectkey, 100),
                                    ini->ReadInteger(brushstr[i], anglekey,    0),
                                    ini->ReadBool   (brushstr[i], shapekey,  false));
   }

   DeleteIniFile(ini);

   return(curbrush);
}

void CPaintTool::adjustBrushSizesForClip(CBrush *brush[], int maxradius)
{
   for (int i=0;i<NUM_BRUSH;i++) {

      if (brush[i] != nullptr) {

         brush[i]->setMaxRadius(maxradius);
      }
   }
}

void CPaintTool::writeBrushParametersToIniAndFreeBrushes(CBrush *brush[], int curbrsh, int maxbrushradius)
{
  if (curClip == nullptr)
     return;

  if (!IniFileName.empty()) {

     CIniFile* ini = CreateIniFile(IniFileName);
     if (ini != nullptr) {

        ini->WriteInteger(titlekey, currentbrushkey, curbrsh);
        ini->WriteInteger(titlekey, maxradiuskey, maxbrushradius);

        for (int i=0;i<NUM_BRUSH;i++) {

           if (brush[i] != nullptr) { // shouldn't happen if curClip != nullptr

              // the saved radius value is percent of 10% of the image
              ini->WriteInteger (brushstr[i], radiuskey,  brush[i]->getRadius());
              ini->WriteInteger (brushstr[i], blendkey,   brush[i]->getBlend());
              ini->WriteInteger (brushstr[i], opacitykey, brush[i]->getOpacity());
              ini->WriteInteger (brushstr[i], aspectkey,  brush[i]->getAspect());
              ini->WriteInteger (brushstr[i], anglekey,   brush[i]->getAngle());
              ini->WriteBool    (brushstr[i], shapekey,   brush[i]->getShape());
           }
        }

        DeleteIniFile(ini);
     }
  }

  // destroy the brushes
  PaintToolForm->FreeBrushes();
}
//------------------------------------------------------------------------------

int CPaintTool::getStrokeSizeThreshold(int radius)
{
   return (radius/256) + 1;
}

//------------------------------------------------------------------------------

void CPaintTool::EnterEditTrackingState()
{
	MTIassert(majorState != MAJORSTATE_EDIT_TRACKING);
	if (getMajorState() == MAJORSTATE_EDIT_TRACKING)
      return; // already in "Edit Tracking" mode!!

   // return here after obtaining tracking data
   majorStateBeforeTracking = getMajorState();

   // This stuff stolen from NotifyGOVStarting() - figure out what's necessary
   // frame you're on
   int currentFrame = getSystemAPI()->getLastFrameIndex();
   if (currentFrame == -1) return;

   // current target and import frames

#ifdef INSTR
   if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
      _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 8495");
   }
#else
   getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

   if (targetFrame == -1) return;

   // key is whether target frame is modified
   bool targetModified = getSystemAPI()->targetFrameIsModified();

	// if there are modifications, accept them - but if the
   // net effect of thru & erase strokes is nothing, don't
	if (targetModified)
	{
		// QQQ shouldn't this pset the doingGOV flag to tru if we are in GOV mode?
	   // QQQ Should this move pending strokes to previous?
		AcceptTargetFrameModifications(false);
	}

	// clear the stroke stack in any case
   getSystemAPI()->clearStrokeStack();

   // switch PAINT to this state
   prevShowState = showState;
	setMajorState(MAJORSTATE_EDIT_TRACKING);
	PaintToolForm->setMajorState(MAJORSTATE_EDIT_TRACKING);

   // set DisplayFrame to work normally
   getSystemAPI()->exitPaintMode();

   // enable tracking
	getSystemAPI()->ActivateTrackingTool();
	setTrackingRange();

	// if tracking points are valid, don't move, else go to the frame where
	// you can add tracking points
	if (!isAllTrackDataValid())
	{
		int startTrackingIndex;
		int stopTrackingIndex;
		TrkPtsEditor->getRange(startTrackingIndex, stopTrackingIndex);
		if (startTrackingIndex >= 0)
		{
			getSystemAPI()->goToFrameSynchronous(startTrackingIndex);
      }
	}

	// Show points if any were created in a previous incarnation
	TrkPtsEditor->refreshTrackingPointsFrame();

	GetToolProgressMonitor()->SetStatusMessage("Edit tracking points");
}
//------------------------------------------------------------------------------

void CPaintTool::LeaveEditTrackingState()
{
	MTIassert(majorState == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED);
	if (getMajorState() == MAJORSTATE_EDIT_TRACKING || majorState == MAJORSTATE_TRACKING_DONE || majorState == MAJORSTATE_TRACKING_PAUSED) {

      ///////////////////////////////////////////////

		TrackingBoxSet *trkArray = TrkPtsEditor->getTrackingArray();
		trkPts.clear();
		if (trkArray->getNumberOfTrackingBoxes() > 0)
		{
			TrackingBox trkPoints = trkArray->getTrackingBoxByIndex(0);

			// indices for the point lists
			int ibeg = trkArray->getInFrame();
			int iend = trkArray->getOutFrame();

			trkBegFrame = ibeg;
			trkEndFrame = iend;

			if (trkPoints.isValidAtFrame(ibeg))
			{
				POINT dumPt = trkPoints[ibeg];
				trkPts.push_back(dumPt);

				for (int i=(ibeg+1); i<iend; i++) {

					if (trkPoints.isValidAtFrame(i)) {

						dumPt = POINT(trkPoints[i]);
					}

					trkPts.push_back(dumPt);
				}
			}
		}

		// if tracked macro possible enable checkbox
      PaintToolForm->TrackedMacroCheckBox->Enabled = TrackedMacroIsEnabled();

      ///////////////////////////////////////////////

      // now you can deactivate the tracking tool
		getSystemAPI()->DeactivateTrackingTool();

      // back into PAINT
      getSystemAPI()->enterPaintMode();

      // exiting TRACKING back to previous majorState
      setMajorState(majorStateBeforeTracking);
      PaintToolForm->setMajorState(majorStateBeforeTracking);

      if (prevShowState == SHOW_ORIGINAL)
         SelectOriginal();
      else if (prevShowState == SHOW_PROCESSED)
         SelectProcessed();

      getSystemAPI()->refreshFrameCached();

      // we'll need the Provisional buffer now
      getSystemAPI()->ClearProvisional(false);

      // forces reload of target and import frames
      getSystemAPI()->clearPaintFrames();

      // reload the current frame from media
      int currentFrame = getSystemAPI()->getLastFrameIndex();
      getSystemAPI()->loadTargetAndImportFrames(currentFrame);

      // set these up and you're ready

#ifdef INSTR
      if (!getSystemAPI()->getPaintFrames(&targetFrame, &importFrame)) {
         _MTIErrorDialog("TARGET FRAME INCONSISTENCY line 8619");
      }
#else
      getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
#endif

      // Update status
      GetToolProgressMonitor()->SetStatusMessage((paintMode == PAINT_MODE_CLONE)?
                                                 "Clone pixels" : "Paint pixels");
      needToClearProgressMonitor = true;
   }
}
//------------------------------------------------------------------------------

void CPaintTool::UpdateTrackingPointGUI(void)
{
   PaintToolForm->UpdateTrackingPointButtons();
   PaintToolForm->UpdateTrackingCharts();
   //UpdateExecutionButtons(GetToolProcessingControlState(),
                          //GetToolProcessingRenderFlag());

}
//------------------------------------------------------------------------------

TrackingBoxSet &CPaintTool::GetTrackingArrayRef()
{
   return TrackingArray;
}
//------------------------------------------------------------------------------

TrackingBoxSet &CPaintTool::GetRobustArrayRef()
{
   return RobustArray;
}
//------------------------------------------------------------------------------

bool CPaintTool::getDisplayTrackingPoints(void)
{
   return TrkPtsEditor->displayTrackingPoints;
}
//------------------------------------------------------------------------------

void CPaintTool::setDisplayTrackingPoints(const bool newDisp)
{
   if (newDisp != TrkPtsEditor->displayTrackingPoints)
   {
      TrkPtsEditor->displayTrackingPoints = newDisp;
   }
}
//------------------------------------------------------------------------------

void CPaintTool::SetTrackingBoxSizes(
         int newTrackBoxXRadius,    int newTrackBoxYRadius,
         int newSearchLeftDistance, int newSearchRightDistance,
         int newSearchUpDistance,   int newSearchDownDistance)
{
//   if (TrackingArray.areExtentsDifferent(
//         newTrackBoxXRadius, newTrackBoxYRadius,
//         newSearchLeftDistance, newSearchRightDistance,
//         newSearchUpDistance, newSearchDownDistance)
//      )
   if (newTrackBoxXRadius != TrackingArray.getTrackingBoxRadius() ||
       newSearchLeftDistance != TrackingArray.getSearchBoxRadius())
   {
//      TrackingArray.setExtents(
//         newTrackBoxXRadius, newTrackBoxYRadius,
//         newSearchLeftDistance, newSearchRightDistance,
//         newSearchUpDistance, newSearchDownDistance);
      TrackingArray.setTrackingBoxRadius(newTrackBoxXRadius);
      TrackingArray.setSearchBoxRadius(newSearchLeftDistance);

      bNeedToReTrack = true;
      getSystemAPI()->refreshFrameCached();
   }
}
//----------------------------------------------------------------------------

void CPaintTool::DeleteSelectedTrackingPoints(void)
{
   TrkPtsEditor->deleteSelectedTrackingPoints();
   TrkPtsEditor->refreshTrackingPointsFrame();

   PaintToolForm->UpdateTrackingPointButtons();

   ////// If there are no more tracking buttons, jump to the MARK IN frame
   ////// else Larry gets confused by the error message when he tries to add
   ////// a new tracking point! Bwaa-haa-haaaa!
   ////if (TrackingArray.getNumberOfTrackingBoxes() < 1)
   ////{
      ////goToTrackingPointInsertionFrame();
   ////}
}
//------------------------------------------------------------------------------
#define INVALID_FRAME_INDEX (-1)
void CPaintTool::ResetTracking(void)
{
   TrackingArray.clear();
   RobustArray.clear();

	setTrackingRange();
   TrkPtsEditor->lastSelectedTag = INVALID_FRAME_INDEX;
   TrkPtsEditor->refreshTrackingPointsFrame();

   PaintToolForm->UpdateTrackingPointButtons();
}
//------------------------------------------------------------------------------

   int CPaintTool::getCountOfValidTrackingPoints(void)
{
   return TrackingArray.getNumberOfTrackingBoxes();
}
//----------------------------------------------------------------------------

int CPaintTool::GetLastSelectedTag()
{
   return TrkPtsEditor->lastSelectedTag;
}
//----------------------------------------------------------------------------

void CPaintTool::SelectNextTrackingPoint()
{
    if (TrackingArray.getNumberOfTrackingBoxes() < 1) return;

    auto tags = TrackingArray.getTrackingBoxTags();
    auto currentTag = TrkPtsEditor->lastSelectedTag;
    auto iter = find(tags.begin(), tags.end(), currentTag);
    if (iter == tags.end() || ++iter == tags.end())
    {
       iter = tags.begin();
    }

    TrkPtsEditor->setUniqueSelection(*iter);
}
//----------------------------------------------------------------------------

void CPaintTool::SelectPreviousTrackingPoint()
{
    if (TrackingArray.getNumberOfTrackingBoxes() < 1) return;

    auto tags = TrackingArray.getTrackingBoxTags();
    auto currentTag = TrkPtsEditor->lastSelectedTag;
    auto iter = find(tags.begin(), tags.end(), currentTag);
    if (iter == tags.begin())
    {
       iter = tags.end();
    }

    TrkPtsEditor->setUniqueSelection(*(--iter));
}
//----------------------------------------------------------------------------

bool CPaintTool::isAllTrackDataValid()
{
   if (TrackingArray.getNumberOfTrackingBoxes() < 1)
      return false;

	return TrkPtsEditor->isTrackingDataValidOverEntireRange();
}
//----------------------------------------------------------------------------

bool CPaintTool::hasFrameBeenTracked(int frameIndex)
{
	return  (isAllTrackDataValid() &&
              (frameIndex >= TrackingArray.getInFrame()) &&
              (frameIndex <  TrackingArray.getOutFrame()));
}
//------------------------------------------------------------------------------

int CPaintTool::getTrackingDataValidity()
{
	// First check that all frames from mark in to out have been tracked.
   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();

	bool rangeIsInvalid = bNeedToReTrack
								|| TrackingArray.getNumberOfTrackingBoxes() < 1
								|| !isAllTrackDataValid()
								|| (markIn >= 0 && TrackingArray.getInFrame() > markIn)
								|| (markOut >= 0 && TrackingArray.getOutFrame() < markOut);

	if (rangeIsInvalid)
	{
      return TRKVLD_NEED_TO_RETRACK;
   }

	// Now check if there are any import frames outside the marked range,
	// and if there are, make sure those have been tracked.
	switch (getSystemAPI()->getImportMode())
	{
      case IMPORT_MODE_ABSOLUTE:
		{
			bool success = getSystemAPI()->getPaintFrames(&targetFrame, &importFrame);
			MTIassert(success);
			if (importFrame == -1 || !hasFrameBeenTracked(importFrame))
			{
            return TRKVLD_TRACK_DATA_IS_VALID_EXCEPT_FOR_REF_FRAME;
			}

         break;
		}

      case IMPORT_MODE_RELATIVE:
		{
			int offset = getSystemAPI()->getImportFrameOffset();
			int firstImportFrame = markIn + offset;
			int lastImportFrame = markOut - 1 + offset;
			if (markIn < 0
			|| markOut < 0
			|| !hasFrameBeenTracked(firstImportFrame)
			|| !hasFrameBeenTracked(lastImportFrame))
			{
            return TRKVLD_TRACK_DATA_IS_VALID_EXCEPT_FOR_REF_FRAME;
			}

			break;
		}
   }

   return TRKVLD_ALL_TRACK_DATA_IS_VALID;
}
//------------------------------------------------------------------------------

bool CPaintTool::ComputeRobustPoints(int refFrameIndex)
{
   int firstTrackingFrameIndex = TrackingArray.getInFrame();
   int lastTrackingFrameIndex =  TrackingArray.getOutFrame() - 1;
   int numberOfTrackPointsPerFrame = TrackingArray.getNumberOfTrackingBoxes();

   RobustArray.clear();

   if (numberOfTrackPointsPerFrame < 1)
      return false;

   // If no reference frame indicated, just use the first frame
   if (refFrameIndex < 0)
      refFrameIndex = firstTrackingFrameIndex;

   if (numberOfTrackPointsPerFrame == 1)
   {
      // Can't get any more robust!
      RobustArray = TrackingArray;
   }
   else if (numberOfTrackPointsPerFrame == 2)
   {
      // for two points, just take the average position - not very robust!
      for (int iFrameIndex = firstTrackingFrameIndex; iFrameIndex <= lastTrackingFrameIndex; ++iFrameIndex)
      {
         auto trackingPoint = TrackingArray.getInFrame();
         double x = (TrackingArray.getTrackingBoxByIndex(0)[iFrameIndex].x + TrackingArray.getTrackingBoxByIndex(1)[iFrameIndex].x) / 2.0;
         double y = (TrackingArray.getTrackingBoxByIndex(0)[iFrameIndex].y + TrackingArray.getTrackingBoxByIndex(1)[iFrameIndex].y) / 2.0;
         FPOINT newPt(x, y);

         RobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(iFrameIndex, newPt);
      }
   }
   else // true robust regression for 3 or more points
   {
      for (int iFrameIndex = firstTrackingFrameIndex;
           iFrameIndex <= lastTrackingFrameIndex;
           ++iFrameIndex)
      {
         ComputeRobustTrackNoRotation(iFrameIndex, refFrameIndex);
      }
   }
   return true;
}
//------------------------------------------------------------------------------

bool CPaintTool::ComputeRobustTrackNoRotation(int nextFrameIndex, int refFrameIndex)
{
   if (TrackingArray.getNumberOfTrackingBoxes() < 1) return false;

    mvector<double> vec_b(2);
    vec_b.zero();

    if (refFrameIndex < TrackingArray.getInFrame())
       refFrameIndex = TrackingArray.getInFrame();
    if (refFrameIndex >= TrackingArray.getOutFrame())
       refFrameIndex = TrackingArray.getOutFrame() - 1;

    if (nextFrameIndex != refFrameIndex)
    {
       int n = TrackingArray.getNumberOfTrackingBoxes(); //TrackingArray.isValid(nextFrameIndex)?
                //(int) TrackingArray[nextFrameIndex].size() : 0;
       mvector<double> vec_y(n * 2);
       matrix<double> matrix_X(n * 2, 2);
       double threshold = 0.001;
       auto InputPoints = TrackingArray.getAllTrackPointsAtFrame(nextFrameIndex);
       auto RefPoints = TrackingArray.getAllTrackPointsAtFrame(refFrameIndex);

       TRACE_3(errout << endl << "=== FRAME " << nextFrameIndex << " ====");

       for (int iTag = 0; iTag < n; ++iTag)
       {
          vec_y[iTag] = InputPoints[iTag].x - RefPoints[iTag].x;
          matrix_X[iTag][0] = 1;
          matrix_X[iTag][1] = 0;

          vec_y[iTag+n] = InputPoints[iTag].y - RefPoints[iTag].y;
          matrix_X[iTag+n][0] = 0;
          matrix_X[iTag+n][1] = 1;

          TRACE_3(errout << "IN[" << iTag << "] = (" << vec_y[iTag] << ","
                                                    << vec_y[iTag+n] << ")");
       }

       if (!robust(vec_y, matrix_X, threshold, vec_b))
       {
          TRACE_1(errout << "WARNING: Check frame " << nextFrameIndex
                         << " -- regression did not reach convergence");
       }
    }

    TRACE_3(errout << "OUT = (" << vec_b[0] << "," << vec_b[1] << ")");

    FPOINT newPt(vec_b[0], vec_b[1]);
    RobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(nextFrameIndex, newPt);

    return true;
}
// ---------------------------------------------------------------------------

bool CPaintTool::IsItOkToStartTracking()
{
	CAutoErrorReporter autoErr("CPaintTool::StartTracking", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (majorState == MAJORSTATE_TRACKING_IN_PROGRESS)
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Tracking is already in progress!    ";
		return false;
	}

	// must be in one of these states
	if (majorState != MAJORSTATE_EDIT_TRACKING && majorState != MAJORSTATE_TRACKING_DONE && majorState != MAJORSTATE_TRACKING_PAUSED) {
		autoErr.errorCode = -1;
		autoErr.msg << "Internal error: Bad tracking state!    ";
		return false;
	}

	if (!AreMarksValid() != 0)
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Marks are invalid!    ";
		return false;
	}

	if (!getSystemAPI()->isAClipLoaded())
	{
		autoErr.errorCode = -1;
		autoErr.msg << "No clip is open   ";
		return false;
	}

	if (getSystemAPI()->targetFrameIsModified())
	{
		if (PaintToolForm->AutoAcceptCheckBox->Checked)
		{
			if (AcceptProcessedTargetFrame(false) != 0)
			{
				autoErr.errorCode = -1;
				autoErr.msg << "Auto-accept failed!";
				return false;
			}
		}
		else
		{
			autoErr.errorCode = -1;
			autoErr.msg << "You must accept or reject the pending fix.";
			return false;
		}
	}

	return true;
}
// ---------------------------------------------------------------------------

void CPaintTool::StartTracking()
{
	if (majorState == MAJORSTATE_TRACKING_IN_PROGRESS
	|| !setTrackingRangePossiblyMovingTheMarks())
	{
		return;
	}

	// Larry says to always start from scratch.
	TrkPtsEditor->invalidateAllTrackingData();
	getSystemAPI()->StartTracking();
}
//------------------------------------------------------------------------------

void CPaintTool::SelectTrackingAlignment(bool enable)
{
	// First!
   if (!enable) { // see below 'RestoreTransform'
      getSystemAPI()->SaveTransform();
   }

   // bounce out of "show both mode" if we're in it
   if (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH) {
      LeaveTrackingAlignShowBothMode();
   }

   alignState = enable? ALIGNSTATE_TRACK_SHOWING_TARGET
                      : ALIGNSTATE_MANUAL;
   getSystemAPI()->clearAllTrackingAlignOffsets();
   getSystemAPI()->useTrackingAlignOffsets(enable);

   // AFTER useTrackingAlignOffsets()!
   if (enable) {
      getSystemAPI()->SetTransform(0, 1, 1, 0, 0, 0); // rot,strx,stry,skw,x,y
   }
   else {

      // see above 'SaveTransform'
      getSystemAPI()->RestoreTransform();
   }

	if (enable && isAllTrackDataValid()) {

      ComputeRobustPoints();
      setTrackingAlignmentOffsets();
	}

   // If tracking data isn't valid yet, similar code will get run elsewhere
   // that computes the robust points and passes them to the displayer

   // NEW: ensure the import frame is loaded
   PaintToolForm->ClearLoadTimer();
   getSystemAPI()->forceTargetAndImportFrameLoaded();

   // Sync the GUI
   double rot, strx, stry, skew, offx, offy;
   getSystemAPI()->GetTransform(&rot, &strx, &stry, &skew, &offx, &offy);

   PaintToolForm->UpdateTransform(rot, strx, stry, skew, offx, offy);
}
//------------------------------------------------------------------------------

bool CPaintTool::isTrackingAlignmentEnabled()
{
   return ((majorState == MAJORSTATE_REVEAL_MODE_HOME)&&
             ((alignState == ALIGNSTATE_TRACK_SHOWING_TARGET)||
              (alignState == ALIGNSTATE_TRACK_SHOWING_BOTH)));
}
//------------------------------------------------------------------------------

void CPaintTool::setTrackingAlignmentOffsets()
{
   if (isTrackingAlignmentEnabled()) {

      int startFrameIndex = getSystemAPI()->getMarkIn();
      int stopFrameIndex = getSystemAPI()->getMarkOut();
      if (startFrameIndex == INVALID_FRAME_INDEX)
         startFrameIndex = RobustArray.getInFrame();
      if (stopFrameIndex == INVALID_FRAME_INDEX)
			stopFrameIndex  = RobustArray.getOutFrame();

		CVideoFrameList *vflp = getSystemAPI()->getVideoFrameList();
		int frameCount = (vflp == nullptr) ? stopFrameIndex : vflp->getTotalFrameCount();

      for (int targetFrameIndex = startFrameIndex;
           targetFrameIndex < stopFrameIndex;
           targetFrameIndex++)
      {
			int importFrameIndex = getImportIndexFromTargetIndex(targetFrameIndex, majorState);
			if (importFrameIndex >= 0 && importFrameIndex < frameCount)
			{
            DPOINT targetOffsets = RobustArray.getTrackingBoxByIndex(0)[targetFrameIndex];
            DPOINT importOffsets = RobustArray.getTrackingBoxByIndex(0)[importFrameIndex];

            // For some reason I don't understand at all,
            getSystemAPI()->setTrackingAlignOffsets(targetFrameIndex,
                                       targetOffsets.x - importOffsets.x,
													targetOffsets.y - importOffsets.y);
         }
      }
   }
   else {

      getSystemAPI()->clearAllTrackingAlignOffsets();
   }

}
//------------------------------------------------------------------------------

int CPaintTool::getImportIndexFromTargetIndex(int targetFrameIndex, int virtualMajorState)
{
   int retVal = -1;

   switch(virtualMajorState) {

      case MAJORSTATE_REVEAL_MODE_HOME:

         if (revealModeImportFrameMode == IMPORT_MODE_RELATIVE) {

            retVal = targetFrameIndex + revealModeImportFrameOffset;
         }
         else {

            retVal = revealModeImportFrame;
         }

         break;

      case MAJORSTATE_CLONE_MODE_HOME:
      case MAJORSTATE_COLOR_MODE_HOME:
      case MAJORSTATE_ORIG_MODE_HOME:

         retVal = targetFrameIndex;

         break;

      case MAJORSTATE_ALTCLIP_MODE_HOME:

         retVal = targetFrameIndex + altClipModeImportFrameOffset;

         break;

      default:

         retVal = -1;

         break;

   }

   // Normalized 'invalid' frame index
	//// NO!!
	////if (retVal < -1)
	////   retVal = -1;

   return retVal;
}
//------------------------------------------------------------------------------

void CPaintTool::HandleSomeButtonUps()
{
   if (majorState==MAJORSTATE_PAINT_PIXELS) {

      // paint brush at last mouse position by
      // queueing up 2 copies of last point
      EndStroke(lastMouseFrameX, lastMouseFrameY);

      // wait for completion (important for next step)
      while (strEmpty < strFill) {
         getSystemAPI()->displayFrameAndBrush();
         MTImillisleep(40);
      }

      // close the macro after writing the stroke
      ClosePendingStrokeChannelAfterWrite();

      // now that all's painted, push original pixels
      getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

      // now examine the target frame and see if
      // it's really modified from the original
      ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

      // now you're ready for the next one
      setMajorState(MAJORSTATE_REVEAL_MODE_HOME);

      getSystemAPI()->mouseUp();
   }
   else if (majorState==MAJORSTATE_CLONE_PIXELS) {

      if (!PaintToolForm->CloneBrushIsRelative()) { // snap back

         // absolute brush at [cloneX, cloneY]
         LoadCloneBrush(false, cloneX, cloneY);

         // update the clone pt coordinates
         PaintToolForm->UpdateCloneX(cloneX);
         PaintToolForm->UpdateCloneY(cloneY);
      }

      // paint brush at last mouse position by
      // queueing up 2 copies of last point
      EndStroke(lastMouseFrameX, lastMouseFrameY);

      // wait for completion (important for next step)
      while (strEmpty < strFill) {
         getSystemAPI()->displayFrameAndBrush();
         MTImillisleep(40);
      }

      // close the macro after writing the stroke
      ClosePendingStrokeChannelAfterWrite();

      // now that all's painted, push original pixels
      getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

      // now examine the target frame and see if
      // it's really modified from the original
      ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

      // now you're ready for the next one
      setMajorState(MAJORSTATE_CLONE_MODE_HOME);

      getSystemAPI()->mouseUp();
   }
   else if (majorState==MAJORSTATE_COLOR_PIXELS) {

      // paint brush at last mouse position by
      // queueing up 2 copies of last point
      EndStroke(lastMouseFrameX, lastMouseFrameY);

      // wait for completion (important for next step)
      while (strEmpty < strFill) {
         getSystemAPI()->displayFrameAndBrush();
         MTImillisleep(40);
      }

      // close the macro after writing the stroke
      ClosePendingStrokeChannelAfterWrite();

      // now that all's painted, push original pixels
		getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

      // now examine the target frame and see if
      // it's really modified from the original
      ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

      // now you're ready for the next one
      setMajorState(MAJORSTATE_COLOR_MODE_HOME);

      getSystemAPI()->mouseUp();
   }
   else if (majorState==MAJORSTATE_PAINT_ORIG_PIXELS) {

      // paint brush at last mouse position by
      // queueing up 2 copies of last point
      EndStroke(lastMouseFrameX, lastMouseFrameY);

      // wait for completion (important for next step)
      while (strEmpty < strFill) {
         getSystemAPI()->displayFrameAndBrush();
         MTImillisleep(40);
      }

      // close the macro after writing the stroke
      ClosePendingStrokeChannelAfterWrite();

      // now that all's painted, push original pixels
      getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

      // now examine the target frame and see if
      // it's really modified from the original
      ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

      // now you're ready for the next one
      setMajorState(MAJORSTATE_ORIG_MODE_HOME);

      getSystemAPI()->mouseUp();
   }
   else if (majorState==MAJORSTATE_PAINT_ALTCLIP_PIXELS) {

      // paint brush at last mouse position by
      // queueing up 2 copies of last point
      EndStroke(lastMouseFrameX, lastMouseFrameY);

      // wait for completion (important for next step)
      while (strEmpty < strFill) {
         getSystemAPI()->displayFrameAndBrush();
         MTImillisleep(40);
      }

      // close the macro after writing the stroke
      ClosePendingStrokeChannelAfterWrite();

      // now that all's painted, push original pixels
      getSystemAPI()->pushStrokeFromTarget(begStrokeFileLength);

      // now examine the target frame and see if
      // it's really modified from the original
      ////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);

      // now you're ready for the next one
      setMajorState(MAJORSTATE_ALTCLIP_MODE_HOME);

      getSystemAPI()->mouseUp();
   }

   else if (majorState == MAJORSTATE_PICK_SEED) {

      // now examine the target frame and see if
      // it's really modified from the original
		////PaintToolForm->EnableAcceptReject(getSystemAPI()->targetFrameIsModified());
      PaintToolForm->UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
      getSystemAPI()->mouseUp();
   }
   else if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME) {

      getSystemAPI()->TransformMouseUp();
   }
   else if (majorState == MAJORSTATE_EDIT_TRANSFORM) {

      getSystemAPI()->TransformMouseUp();
   }
}
//------------------------------------------------------------------------------

void CPaintTool::getClipMarkIndices(ClipSharedPtr &clip, int &markIn, int &markOut)
{
   markIn = -1;
   markOut = -1;

   if (!clip || clip == getSystemAPI()->getClip())
   {
      // Current clip - get the info from the player.
      markIn = getSystemAPI()->getMarkIn();
      markOut = getSystemAPI()->getMarkOut();
      return;
   }

   CVideoFrameList *videoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
   if (!videoFrameList)
   {
      return;
   }

   CBinManager bm;
   string sectionName = bm.getDesktopSectionUniqueName();;
   bool usingBinMarks = false;
   string markInTCStr;
   string markOutTCStr;

   CIniFile* binDesktopIni  = bm.openBinDesktop(clip);
   if (binDesktopIni)
   {
      usingBinMarks = binDesktopIni->ReadBool("General",
                                              "ClipsShareInAndOutMarks",
                                              false);
      if (usingBinMarks)
      {
         markInTCStr = binDesktopIni->ReadString(sectionName, "MarkIn", markInTCStr);
         markOutTCStr = binDesktopIni->ReadString(sectionName, "MarkOut", markOutTCStr);
      }

      if (markInTCStr.empty() && markOutTCStr.empty())
      {
         usingBinMarks = false;
      }

      DeleteIniFile(binDesktopIni, true);
   }

   if (!usingBinMarks)
   {
      CIniFile* clipDesktopIni = bm.openClipDesktop(clip);
      if (clipDesktopIni)
      {
         markInTCStr = clipDesktopIni->ReadString(sectionName, "MarkIn", markInTCStr);
         markOutTCStr = clipDesktopIni->ReadString(sectionName, "MarkOut", markOutTCStr);
         DeleteIniFile(clipDesktopIni, true);
      }
   }

   if (!markInTCStr.empty())
   {
      CTimecode markTimecode;
      markTimecode.setTimeASCII(markInTCStr.c_str());
      markIn = videoFrameList->getFrameIndex(markTimecode);
   }

   if (!markOutTCStr.empty())
   {
      CTimecode markTimecode;
      markTimecode.setTimeASCII(markOutTCStr.c_str());
      markOut = videoFrameList->getFrameIndex(markTimecode);
   }

   int inFrameIndex = videoFrameList->getInFrameIndex();
   int outFrameIndex = videoFrameList->getOutFrameIndex();
   markIn = (markIn < 0) ? markIn : max<int>(inFrameIndex, min<int>(outFrameIndex, markIn));
   markOut = (markOut < 0) ? markOut : max<int>(inFrameIndex, min<int>(outFrameIndex, markOut));
}
//------------------------------------------------------------------------------

void CPaintTool::SetMainWindowFocus()
{
   getSystemAPI()->setMainWindowFocus();
}
//------------------------------------------------------------------------------

void CPaintTool::SetToolFocus()
{
   TWinControl *baseToolForm = getSystemAPI()->getBaseToolForm();
   if (baseToolForm->Visible)
   {
      getSystemAPI()->getBaseToolForm()->SetFocus();
   }
}
//------------------------------------------------------------------------------
