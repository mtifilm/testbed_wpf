#ifndef PaintToolH
#define PaintToolH

#include "UserInput.h"
#include "ToolObject.h"
#include "ToolPaintInterface.h"
#include "TrackingBox.h"
#include "IniFile.h"
#include "BrushAlgo.h"
#include "GrainPresets.h"
//#include "PaintReduction_MT.h"
#include <vector>
using std::vector;
//////////////////////////////////////////////////////////////////////

// Paint Tool Number (16-bit number that uniquely identifies this tool)
#define PAINT_TOOL_NUMBER    19

#define NUM_BRUSH 10

// Paint Tool Navigator Commands

/////////////////////////////////////////////////////////////////////////
// CAREFUL this number must be LARGER THAN THE MAX NAV COMMAND NUMBER  //
//         (see NavigatorTool.h)                                       //
/////////////////////////////////////////////////////////////////////////
// CAREFUL this number must be SMALLER THAN OR EQUAL TO THE MIN PAINT  //
//         command number (see below)                                  //
/////////////////////////////////////////////////////////////////////////
#define PNT_CMD_LOWEST_NUMBER  180
/////////////////////////////////////////////////////////////////////////


// Brush Selection Commands
#define PNT_CMD_SELECT_BRUSH_1            180
#define PNT_CMD_SELECT_BRUSH_2            181
#define PNT_CMD_SELECT_BRUSH_3            182
#define PNT_CMD_SELECT_BRUSH_4            183
#define PNT_CMD_SELECT_BRUSH_5            184
#define PNT_CMD_SELECT_BRUSH_6            185
#define PNT_CMD_SELECT_BRUSH_7            186
#define PNT_CMD_SELECT_BRUSH_8            187
#define PNT_CMD_SELECT_BRUSH_9            188
#define PNT_CMD_SELECT_BRUSH_10           189
#define PNT_CMD_SELECT_BRUSH_11           190
#define PNT_CMD_SELECT_BRUSH_12           191

// Paint Tool Command Numbers
#define PNT_CMD_SHOW_IMPORT_FRAME         192
#define PNT_CMD_SHOW_TARGET_FRAME         193

#define PNT_CMD_ENTER_REPOSITION_MODE     194
#define PNT_CMD_LEAVE_REPOSITION_MODE     195
#define PNT_CMD_ENTER_TRANSFORM_MODE      196
#define PNT_CMD_TOGGLE_AUTO_ALIGN_MODE    197
#define PNT_CMD_TOGGLE_DIFFERENCE_BLEND   198
#define PNT_CMD_RESET_TRANSFORM           199
#define PNT_CMD_LEAVE_MODE_REVERT         200
#define PNT_CMD_LEAVE_MODE_KEEP           201

#define PNT_CMD_BEG_REPOSITION            202
#define PNT_CMD_END_REPOSITION            203
#define PNT_CMD_BEG_MOVE_CLONE_PT         204
#define PNT_CMD_END_MOVE_CLONE_PT         205
#define PNT_CMD_BEG_POSITION_PAINT        206
#define PNT_CMD_BEG_POSITION_PAINT_X      207
#define PNT_CMD_BEG_POSITION_PAINT_Y      208
#define PNT_CMD_END_POSITION_PAINT        209
#define PNT_CMD_END_POSITION_PAINT_X      210
#define PNT_CMD_END_POSITION_PAINT_Y      211
#define PNT_CMD_BEG_QUICK_ERASE           212
#define PNT_CMD_END_QUICK_ERASE           213
#define PNT_CMD_UNDO_STROKE               214
#define PNT_CMD_POSITION_LFT              215
#define PNT_CMD_POSITION_RGT              216
#define PNT_CMD_POSITION_UP               217
#define PNT_CMD_POSITION_DN               218
#define PNT_CMD_SHIFT_POS_LFT             219
#define PNT_CMD_SHIFT_POS_RGT             220
#define PNT_CMD_SHIFT_POS_UP              221
#define PNT_CMD_SHIFT_POS_DN              222
#define PNT_CMD_CTRL_POS_LFT              223
#define PNT_CMD_CTRL_POS_RGT              224
#define PNT_CMD_CTRL_POS_UP               225
#define PNT_CMD_CTRL_POS_DN               226
#define PNT_CMD_ALT_POS_LFT               227
#define PNT_CMD_ALT_POS_RGT               228
#define PNT_CMD_ALT_POS_UP                229
#define PNT_CMD_ALT_POS_DN                230
#define PNT_CMD_ALT_SHIFT_POS_LFT         231
#define PNT_CMD_ALT_SHIFT_POS_RGT         232
#define PNT_CMD_ALT_SHIFT_POS_UP          233
#define PNT_CMD_ALT_SHIFT_POS_DN          234
#define PNT_CMD_ALT_CTRL_POS_LFT          235
#define PNT_CMD_ALT_CTRL_POS_RGT          236
#define PNT_CMD_ALT_CTRL_POS_UP           237
#define PNT_CMD_ALT_CTRL_POS_DN           238
#define PNT_CMD_CYCLE_BRUSH_FWD           239
#define PNT_CMD_TOGGLE_ELLIPSE_RECT       240
#define PNT_CMD_TOGGLE_POS_MASK_PAINT     241
#define PNT_CMD_TOGGLE_FIX                242
#define PNT_CMD_BEG_WIDGET_SCALE          243
#define PNT_CMD_END_WIDGET_SCALE          244
#define PNT_CMD_BEG_WIDGET_SKEW           245
#define PNT_CMD_END_WIDGET_SKEW           246
#define PNT_CMD_REJECT_TARGET_FRAME       247
#define PNT_CMD_ACCEPT_TARGET_FRAME       248
#define PNT_CMD_ACCEPT_TARGET_IN_PLACE    249
#define PNT_CMD_RESET_IMPORT_FRAME        250
#define PNT_CMD_TOGGLE_IMPORT_MODE        251
#define PNT_CMD_SELECT_THRU               252
#define PNT_CMD_SELECT_ERASE              253
#define PNT_CMD_SELECT_HEAL               254
#define PNT_CMD_SELECT_REVEAL_MODE        255
#define PNT_CMD_SELECT_CLONE_MODE         256
#define PNT_CMD_SELECT_COLOR_MODE         257
#define PNT_CMD_SELECT_ORIG_OR_ALTCLIP_MODE 258
#define PNT_CMD_TOGGLE_CLONE_ABS_REL      259
#define PNT_CMD_REPEAT_LAST_STROKES       260
#define PNT_CMD_UNDO_LAST_STROKE          261
#define PNT_CMD_BRUSH_DISPLAY_OFF         262
#define PNT_CMD_BRUSH_DISPLAY_ON          263
#define PNT_CMD_EXECUTE_RANGE_MACRO       264
#define PNT_CMD_SUSPEND_RANGE_MACRO       265
#define PNT_CMD_EDIT_MASK                 266
#define PNT_CMD_ENABLE_CONSTRAINT_X       267
#define PNT_CMD_DISABLE_CONSTRAINT_X      268
#define PNT_CMD_ENABLE_CONSTRAINT_Y       269
#define PNT_CMD_DISABLE_CONSTRAINT_Y      270
#define PNT_CMD_ENABLE_TOLERANCE          271
#define PNT_CMD_MASK_SELECT_ALL_REGIONS   272
#define PNT_CMD_SELECT_DENSITY_ADJUST     273
#define PNT_CMD_RESET_DENSITY             274
#define PNT_CMD_SELECT_GRAIN_ADJUST       275
#define PNT_CMD_RESET_GRAIN               276
#define PNT_CMD_ENABLE_OPACITY            277
#define PNT_CMD_ENABLE_BLEND              278
#define PNT_CMD_ENABLE_ASPECT             279
#define PNT_CMD_ENABLE_ANGLE              280
#define PNT_CMD_TOGGLE_PROCESSED_ORIGINAL 281
#define PNT_CMD_GO_TO_FIRST_TRACKING_FRAME 282
#define PNT_CMD_EDIT_TRACKING             283
#define PNT_CMD_TOGGLE_TRACK_ALIGN_MODE   284
#define PNT_CMD_CYCLE_BRUSH_BWD           285
#define PNT_CMD_RESET_DENSITY_OR_GRAIN    286
#define PNT_CMD_PAUSE_OR_RESUME_TRACKING  287
#define PNT_CMD_TOGGLE_SHOW_IMPORT_DIFFS  288

// Paint Tool Default Parameters
#define PNT_DEFAULT_SIZE 2
#define PNT_DEFAULT_SUPPORT 200
#define PNT_DEFAULT_CONTRAST 20.0
#define PNT_DEFAULT_PAST 2
#define PNT_DEFAULT_FUTURE 2

#define PNT_MAX_THREAD_COUNT      8       // Maximum number of grain
                                          // algorithm processing threads

// Maximum number of input and output frames per processing iteration
#define PNT_MAX_OUTPUT_FRAMES   (PNT_MAX_THREAD_COUNT)
#define PNT_MAX_ADJACENT_FRAMES 2
#define PNT_MAX_INPUT_FRAMES (PNT_MAX_OUTPUT_FRAMES + 2*PNT_MAX_ADJACENT_FRAMES)


//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CBrush;
class CImageFormat;
class CVideoFrameList;
class CPaintTool;
class TrackingBoxManager;

const int InvalidAlternateSourceClipOffsetValue = 0x80000000;


////////////////////////////////////////////////////////////////////

class CRealToolPaintInterface: public CToolPaintInterface
{

public:

   CRealToolPaintInterface(CPaintTool *);
   virtual ~CRealToolPaintInterface();

public:

   // CAREFUL!! THE ORDER OF THESE ARGS DO NOT MATCH THE ORDER OF
   // getSysAPI->GetTransform() & getSysAPI->SetTransform()
   virtual void setTransform(double rot,
                             double strx, double stry,
                             double skew,
                             double offx, double offy);

   virtual void setImportFrameMode(int);

   virtual void setImportFrameTimecode(int);

   virtual void setTargetFrameTimecode(int);

   virtual void setImportFrameOffset(int);

   virtual void drawProxy(unsigned int *);

   virtual void clearStrokeStack();

   virtual void setStrokeEntry(int, int);

   virtual void setPickedColor(float *);

   virtual void onLoadNewTargetFrame();

   virtual void setFrameLoadTimer();

private:

   CPaintTool *paintTool;
};

#define STROKE_CONTEXT 1
struct STRCTXT {

   short header;

   short paintMode;

   int importMode;
   int importFrame;
   int importFrameOffset;

   double rotation;
   double stretchX;
   double stretchY;
   double skew;
   double offsetX;
   double offsetY;

   int currentBrush;

   int brushMode;

   int  radius;
   int  blend;
   int  opacity;
   int  aspect;
   int  angle;
   bool shape;
   EBrushType type;

   int darkerthreshold;
   int lighterthreshold;
   bool usingColor;
   float color[3];

   CBrush *cloneBrush;

};

#define MID_STROKE 2
#define BEG_STROKE 4
#define END_STROKE 8
struct STRPNT {
   short header;
   short x;
   short y;
};

//////////////////////////////////////////////////////////////////////

class CPaintTool : public CToolObject
{
public:

   CPaintTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CPaintTool();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual CToolProcessor* makeToolProcessorInstance(const bool *);

   // Tool GUI
   virtual bool toolHideQuery();
   virtual int toolHide();
   virtual int toolShow();
   virtual bool IsShowing(void);
   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   virtual void NotifyGOVDone();
   virtual void NotifyRightClicked();
   virtual bool NotifyTrackingStarting();
   virtual void NotifyTrackingDone(int errorCode);
   virtual void SetMaskTransformEditorIdleRefreshMode();

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onNewMarks();
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onNewFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onPlayerStop();
   virtual int onPlaybackFilterChange(int);

   CBHook ClipChange;   // Needed to call back on a change

   int  InitClip(bool dontClearTheFrickinMacro);
   int  onNewClipInternal(bool itsNotReallyANewFrickinClip);
   void BeginPaintOrQuickErase();

   void setMajorState(int);
   int  getMajorState();
   void setNextMajorState(int);
   int  getNextMajorState();
   void setAlignState(int);
   int  getAlignState();
   void setMouseWheelState(int);
   int  getMouseWheelState();
   bool isMouseWheelStateOK();

#define ARROW_LFT 0
#define ARROW_RGT 1
#define ARROW_UP  2
#define ARROW_DN  3
   void ArrowKeyMouseWheel(int);
   void CycleBrush(int);
   void ToggleChooseAutoAlignPointMode();
   void ChangeAutoAlignBoxSize(int);
   void ExitFromChooseAutoAlignPointMode();
   void InitClonePt();
   void SelectChooseClonePt();
   void EscChooseClonePt();
   void SelectChooseClonePtAlt();
   void EndChooseClonePtAlt();
   void SelectPickColor();
   void EscPickColor();
   void SelectPickSeed();
   void EscPickSeed();
   void PositionImportFrame();
   void SelectReposition();
   void SelectTransform();
   void LeaveTransformRevert();
   void LeaveTransformKeep(bool rfrsh = true);
   void CompareOriginalFrame();
   void CompareAltClipFrame();
   void SelectMask();
   void LeaveMaskRevert(bool rfrsh = true);
   void SelectMaskQuickly();
   void SelectMaskTransform();
   void EnterTrackingAlignShowBothMode();
   void LeaveTrackingAlignShowBothMode();
   void SelectDensityAdjust();
   void EscDensityAdjust();
   void SelectGrainAdjust();
   void EscGrainAdjust();
   string GetBinPath();
   string GetClipName();

   void PopStrokesToTarget(int);
   void QueueUpStrokePoint(int, int, int);
   void BeginStroke(int, int);
   void EndStroke(int, int);
   void AbortStrokePainting();
   void OnLoadNewTargetFrame();
   int  RewindPendingStrokeCaptureChannel(int);
   int  OpenPendingStrokeChannelForWrite();
   void GetStrokeContext(STRCTXT&);
   int  CaptureStrokeContext(STRCTXT&);
   int  CaptureStrokePoint(STRPNT&);
   int  ClosePendingStrokeChannelAfterWrite();
   int  OpenPreviousStrokeChannelForRead();
	bool MarksAreValid();
	bool setTrackingRangePossiblyMovingTheMarks();
   bool IsPreviousStrokeChannelNonEmpty();
   bool LoadStrokeContext(STRCTXT&, bool setxfm = true);
   int  PlayStrokeFileToken(bool setxfm);
   int  RangeStrokeFileToken();
   int  ClosePreviousStrokeChannelAfterRead();
   bool StrokeChannelIsNonEmpty();
   int  ClearCapturedStroke();
   int  PlayCapturedStroke(bool setxfm);
   int  PlayPendingStroke();
   int  RangeCapturedStroke();
   bool TrackedMacroIsEnabled();
   int  ExecuteRangeMacro();
   bool IsRangeMacroExecuting();
   void SuspendRangeMacro();
   void RecycleCurrentStrokes();

   void SetCloneBrushRelative(bool);
   void LoadCloneBrush(bool, int, int);
   void SetCloneX(int);
   void SetCloneY(int);
   void SetCloneDelX(int);
   void SetCloneDelY(int);
   void ReloadCloneBrush();
   void EnableAutoAccept(bool);
   void ResetBackgroundPainter();
   bool IsBackgroundPainterPainting();
   void BackgroundPainter(void *);
   static void BackgroundPainterCallback(void *, void *);

   void SetDensityStrength(int);
   int  GetDensityStrength();
   void SetGrainStrength(int);
   int  GetGrainStrength();
   void SaveDensityAndGrain();
   void RestoreDensityAndGrain();

   void EnableHealer(bool);
   void BackgroundHealer(void *);
   static void BackgroundHealerCallback(void *, void *);

   void SelectRevealModeFast();
   void SelectRevealMode();
   void SelectCloneModeFast();
   void SelectCloneMode();
   void SelectColorModeFast();
   void SelectColorMode();
   void SelectOrigValuesModeFast();
   void SelectOrigValuesMode();
   void SelectAltClipModeFast();
   void SelectAltClipMode();
   void SelectPaintModeFast(int);
   void SelectPaintMode(int);

   void SelectOnionSkinMode(int, bool);
   void ToggleColorPosNeg();

   void SelectThru();
   void SelectErase();
   void ToggleThruErase();

   void StartHealing();
   void StopHealing();

   void SelectAbsolute();
   void SelectRelative();
   void ToggleAbsoluteRelative();
   void ToggleCloneAbsoluteRelative();

   int GetImportFrameMode();
   int GetImportFrameTimecode();
   int GetImportFrameOffset();
   double GetRotation();
   double GetSkew();
   double GetStretchX();
   double GetStretchY();
   double GetOffsetX();
   double GetOffsetY();

   void SetImportFrameMode(int);
   void SetImportFrameTimecode(int);
   void SetImportFrameOffset(int);
   void SetRotation(double);
   void SetSkew(double);
   void SetStretchX(double);
   void SetStretchY(double);
   void SetOffsetX(double);
   void SetOffsetY(double);

   void SelectProcessed();
   void SelectOriginal();
   void ToggleProcessedOriginal();
   void ToggleShowImportFrameDiffs();
   bool EnableAcceptReject();
   bool ShouldImportFrameTimecodeBeRed(int frameIndex);

   int  RenamePendingAsPrevious();
   int  AcceptProcessedTargetFrame(bool);
   int  AutoAcceptConditional();
	int  AcceptTargetFrameModifications(bool doingGOV);
	int  RejectProcessedTargetFrame();

   void setTargetFrame(int target);
   int  getTargetFrame();
   void goToTargetFrame(bool renew=false);
   void goToImportFrame();
   void goToTrackingPointInsertionFrame();
   void setTrackingRange();

   int readImportFrameAndOffset();
   int writeImportFrameAndOffset();
   int readGeneralSettings();
   int writeGeneralSettings();

   int  allocateBrushesAndReadBrushParametersFromIni(CBrush *brush[], int &maxBrushRadiusOut);
   void adjustBrushSizesForClip(CBrush *brush[], int maxradius);
   void writeBrushParametersToIniAndFreeBrushes(CBrush *brush[], int curbrush, int maxbrushradius);
	int  getStrokeSizeThreshold(int);

   // tracking crap
   void EnterEditTrackingState();
   void LeaveEditTrackingState();
   void UpdateTrackingPointGUI();
   TrackingBoxSet &GetTrackingArrayRef();
   TrackingBoxSet &GetRobustArrayRef();
   bool getDisplayTrackingPoints();
   void setDisplayTrackingPoints(const bool newDisp);
#define TRACKING_BOX_DEFAULT 30
#define TRACKING_BOX_MIN      5
#define TRACKING_BOX_MAX     60
#define SEARCH_RANGE_DEFAULT 35
#define SEARCH_RANGE_MIN      5
#define SEARCH_RANGE_MAX     100
   void SetTrackingBoxSizes(
            int newTrackBoxXRadius,    int newTrackBoxYRadius,
            int newSearchLeftDistance, int newSearchRightDistance,
            int newSearchUpDistance,   int newSearchDownDistance);
   void DeleteSelectedTrackingPoints();
   void ResetTracking();
   void SelectPreviousTrackingPoint();
   void SelectNextTrackingPoint();
   int GetLastSelectedTag();
   int getCountOfValidTrackingPoints();
   bool isAllTrackDataValid();
   bool hasFrameBeenTracked(int frameIndex);
   bool ComputeRobustPoints(int refFrameIndex=-1);
   bool ComputeRobustTrackNoRotation(int nextFrameIndex, int refFrameIndex);
	bool IsItOkToStartTracking();
	void StartTracking();
   TrackingBoxManager *getTrackingPointsEditor() { return TrkPtsEditor; };
   void SelectTrackingAlignment(bool enable);
   bool isTrackingAlignmentEnabled();
   int getImportIndexFromTargetIndex(int targetFrameIndex, int majorstate);
   void setTrackingAlignmentOffsets();
   int getTrackingDataValidity();

   TrackingBoxSet TrackingArray;
   TrackingBoxSet RobustArray;
   TrackingBoxManager *TrkPtsEditor;
   string _CurrentClipName;
   bool _WarningDialogs;
   bool bNeedToReTrack;
   bool trackingAlignmentIsEnabled;
   // end of tracking crap

   // grain crap
   GrainPresets &GetGrainPresets() {return _grainPresets;}

   void WriteAlternateSourceClipNameToDesktopIni(const string &alternateSourceClipName);
   string ReadAlternateSourceClipNameFromDesktopIni();
   void WriteAlternateSourceClipOffsetToDesktopIni(int alternateSourceClipOffset);
   int ReadAlternateSourceClipOffsetFromDesktopIni();

   // I had to break this out of the gigundo onToolCommand switch megastatement
   void HandleSomeButtonUps();

   void getClipMarkIndices(ClipSharedPtr &clip, int &markIn, int &markOut);

   void SetMainWindowFocus();
   void SetToolFocus();

private:

   bool firstActivationFlag;

   ClipSharedPtr curClip;
   string clipBinPath;
   string clipName;
   const CImageFormat *curImageFormat;

   RECT curFrameRect;

   int maxBrushRadius;

   CVideoFrameList *curFrameList;

   int minFrame;
   int maxFrame;

   int userInputWindowX;
   int userInputWindowY;

   int userInputFrameX;
   int userInputFrameY;

   bool resetLastMousePos;

   int lastMouseWindowX;
   int lastMouseWindowY;

   int lastMouseFrameX;
   int lastMouseFrameY;

   int realMouseWindowX;
   int realMouseWindowY;
   
   int realMouseFrameX;
   int realMouseFrameY;

   int strokeSizeThreshold;

   int whereBorlandCursorShouldBeX;
   int whereBorlandCursorShouldBeY;

#define MOUSE_CONSTRAINT_NONE 0
#define MOUSE_CONSTRAINT_X    1
#define MOUSE_CONSTRAINT_Y    2
   int mouseConstraint;

   int mixDelta;
   int positionDelta;
   int autoAlignBoxSizeDelta;
   int toleranceDelta;
   float brushRadiusFactor;
   int brushRadiusDelta;
   int brushOpacityDelta;
   int brushBlendDelta;
   int brushAspectDelta;
   int brushAngleDelta;
   int brushDensityDelta;
   int brushGrainDelta;

#define AUTOALIGN_BOX_MIN   8
#define AUTOALIGN_BOX_MAX 128
   int autoAlignBoxSize;

   int povSavedMouseWheelState;

   int brushDensityStrength;
   int povSavedBrushDensityStrength;
   bool povSavedBrushDensitySelected;

   int brushGrainStrength;
   int povSavedBrushGrainStrength;
   bool povSavedBrushGrainSelected;

   CToolPaintInterface *paintInterface;

   string brushstr[NUM_BRUSH];

   int strokesThisFrame;

#define MAX_STROKE_POINTS 16384
   int lastStrokeX;
   int lastStrokeY;
   bool disablePainter;
   bool resetFIFO;
   unsigned int strFill;
   unsigned int strEmpty;

   bool disableHealer;

   STRPNT *strokeQueue;

   void *painterThreadEndSem;
   void *fifoResetSem;
   void *healerThreadEndSem;

#define MAX_MACRO_POINTS 16384

   int begStrokeFileLength;
   bool strCapture;

   string pendingFilename;
   int pndStrFileHandle;
   int pndStrFilePtr;
   int pndStrFileLength;
   MTI_UINT8 *pndStrBufPtr;
   MTI_UINT8 *pndStrBuf;
   MTI_UINT8 *pndStrBufEnd;

   string previousFilename;
   int prvStrFileHandle;
   int prvStrFilePtr;
   int prvStrFileLength;
   MTI_UINT8 *prvStrBufPtr;
   MTI_UINT8 *prvStrBuf;
   MTI_UINT8 *prvStrBufEnd;

   int strBegFrame;
	int strEndFrame;
	bool strReverse;

   static string titlekey;
   static string currentbrushkey;
   static string maxradiuskey;
   static string radiuskey;
   static string blendkey;
   static string opacitykey;
   static string aspectkey;
   static string anglekey;
   static string shapekey;
   static string typekey;

   static string colorKey;

   static string revealModeImportFrameModeKey;
   static string revealModeImportFrameKey;
   static string revealModeImportFrameOffsetKey;
   static string revealModeRotationKey;
   static string revealModeSkewKey;
   static string revealModeStretchXKey;
   static string revealModeStretchYKey;
   static string revealModeOffsetXKey;
   static string revealModeOffsetYKey;
   static string revealModeOnionSkinModeKey;

   static string cloneModeImportFrameModeKey;
   static string cloneModeImportFrameKey;
   static string cloneModeImportFrameOffsetKey;
   static string cloneModePointModeKey;
   static string cloneModeXKey;
   static string cloneModeYKey;
   static string cloneModeOffsetXKey;
   static string cloneModeOffsetYKey;

   static string origValuesModeOnionSkinModeKey;
   static string altClipModeOnionSkinModeKey;

   static string paintModeKey;
   static string autoAcceptModeKey;

   bool maskEnabled;

   bool rangeMacroExecuting;

#define MAJORSTATE_INVALID                   -1
#define MAJORSTATE_GUI_CREATED                0
#define MAJORSTATE_SELECTING_BRUSH            1
#define MAJORSTATE_REVEAL_MODE_HOME           2
#define   MAJORSTATE_SELECTING_IMPORT_FRAME   3
#define   MAJORSTATE_POSITION_IMPORT_FRAME    4
#define   MAJORSTATE_PAINT_PIXELS             5
#define MAJORSTATE_CLONE_MODE_HOME            6
#define   MAJORSTATE_CHOOSE_CLONE_PT          7
#define   MAJORSTATE_CHOOSE_CLONE_PT_ALT      8
#define     MAJORSTATE_MOVE_CLONE_PT          9
#define   MAJORSTATE_CLONE_PIXELS            10
#define MAJORSTATE_COLOR_MODE_HOME           11
#define   MAJORSTATE_PICK_COLOR              12
#define   MAJORSTATE_PICK_SEED               13
#define   MAJORSTATE_COLOR_PIXELS            14
#define   MAJORSTATE_HEAL_PIXELS             15
#define     MAJORSTATE_STOPPING_HEALING      16
#define MAJORSTATE_ORIG_MODE_HOME            17
#define   MAJORSTATE_SELECTING_ORIG_FRAME    18
#define   MAJORSTATE_PAINT_ORIG_PIXELS       19
#define MAJORSTATE_EDIT_TRANSFORM            20
#define   MAJORSTATE_CHOOSE_AUTOALIGN_PT     21
#define   MAJORSTATE_CHOOSE_AUTOALIGN_ALT    22
#define MAJORSTATE_EDIT_MASK                 23
#define MAJORSTATE_MASK_TRANSFORM            24
#define MAJORSTATE_BRUSH_OFF                 25
#define MAJORSTATE_EXECUTING_MACRO           26
#define MAJORSTATE_EXECUTING_RANGE_MACRO     27
#define MAJORSTATE_GET_ORIGINAL_VALUES       28
#define MAJORSTATE_EDIT_TRACKING             29
#define MAJORSTATE_TRACKING_IN_PROGRESS      30
#define MAJORSTATE_TRACKING_DONE             31
#define MAJORSTATE_TRACKING_PAUSED           32
#define MAJORSTATE_ALTCLIP_MODE_HOME         33
#define   MAJORSTATE_SELECTING_ALTCLIP_FRAME 34
#define   MAJORSTATE_PAINT_ALTCLIP_PIXELS    35
#define   MAJORSTATE_PAINT_ALTCLIP_COMPARE   36
	int majorState;

   int majorStateBeforeTracking;

#define ALIGNSTATE_INVALID                 -1
#define ALIGNSTATE_MANUAL                   0
#define ALIGNSTATE_TRACK_SHOWING_TARGET     1
#define ALIGNSTATE_TRACK_SHOWING_BOTH       2
   int alignState;

   int nextMajorState;

   int prevBrushMode;

#define SHOW_PROCESSED 0
#define SHOW_ORIGINAL  1
   int showState;

   int prevShowState;

#define MOUSE_WHEEL_ADJUST_MIX            0
#define MOUSE_WHEEL_ADJUST_ORIG_MIX       1
#define MOUSE_WHEEL_ADJUST_OFFSET         2
#define MOUSE_WHEEL_ADJUST_AUTOALIGN_BOX  3
#define MOUSE_WHEEL_ADJUST_TOLERANCE      4
#define MOUSE_WHEEL_ADJUST_RADIUS         5
#define MOUSE_WHEEL_ADJUST_OPACITY        6
#define MOUSE_WHEEL_ADJUST_BLEND          7
#define MOUSE_WHEEL_ADJUST_ASPECT         8
#define MOUSE_WHEEL_ADJUST_ANGLE          9
#define MOUSE_WHEEL_ADJUST_DENSITY       10
#define MOUSE_WHEEL_ADJUST_GRAIN         11
#define MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX  12
   int mouseWheelState;

   int lastFrame;

   bool autoAccept;

	int targetFrame;
   int importFrame;

#define PAINT_MODE_REVEAL  1
#define PAINT_MODE_CLONE   2
#define PAINT_MODE_COLOR   3
#define PAINT_MODE_ORIG    4
#define PAINT_MODE_ALTCLIP 5
#define PAINT_MODE_MIN PAINT_MODE_REVEAL
#define PAINT_MODE_MAX PAINT_MODE_ALTCLIP
   int paintMode;

   int paletteColor;

   int revealModeImportFrameMode;
   int revealModeImportFrame;
   int revealModeImportFrameOffset;
   double revealModeRotation;
   double revealModeSkew;
   double revealModeStretchX;
   double revealModeStretchY;
   double revealModeOffsetX;
   double revealModeOffsetY;
   int revealModeOnionSkinMode;

   int cloneModeImportFrameMode;
   int cloneModeImportFrame;
   int cloneModeImportFrameOffset;

   int origValuesModeOnionSkinMode;
   int altClipModeOnionSkinMode;

   int altClipModeImportFrame = 0;
   int altClipModeImportFrameOffset = 0;


#define STROKE_TYPE_THRU  0
#define STROKE_TYPE_HEAL  1
#define STROKE_TYPE_ERASE 2
   //int strokeType;

   //double rotation;
   //double stretchX;
   //double stretchY;
   //double skew;

   bool displayProcessed;

   int toolSetupHandle;    // Init to -1
   int trainingToolSetupHandle;

   string IniFileName;

   bool lastCloneRel;
   int lastCloneX;
   int lastCloneY;
   bool captureOffset;
   bool captureOffsetOnce;
   int captureX;
   int captureY;
   bool clModePointMode;
   int clOffsetX;
   int clOffsetY;
   int cloneX;
   int cloneY;
   int tentCloneX;
   int tentCloneY;

   bool healerOn;
   bool quickEraseStartIsPending;

   // TEMPORARY - remember the state of the Mask Tool, enabled or disabled.
   bool previousMaskToolEnabled;

   bool frameSelectKeyDown;

   STRCTXT savedContext;

   bool needToClearProgressMonitor;

   int repeatMacroBegFrame;
   int rangeMacroBegFrame;
   int trkBegFrame;
   int trkEndFrame;
   bool trackedMacroFlag;

   vector <POINT> trkPts;

   GrainPresets _grainPresets;
   int _overrideGrainStrength;

   int _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
   bool _savedRedOverlayState = false;
};

extern CPaintTool *GPaintTool;    // Global from PaintToolPlugin.cpp

//////////////////////////////////////////////////////////////////////

#endif // !defined(PAINTTOOL_H)

