//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "BinManagerGUIUnit.h"
#include "IniFile.h"
#include "MTIBitmap.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "NavigatorTool.h"
#include "PaintToolGUIWin.h"
#include "PaintTool.h"
#include "ClipAPI.h"
#include "MTIstringstream.h"
#include "ToolSystemInterface.h"
#include "TrackingBoxManager.h"
#include "MTIsleep.h"
#include <math.h>
#include <iostream>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "MTITrackBar"
#pragma link "VTimeCodeEdit"
#pragma link "VDoubleEdit"
//#pragma link "CSPIN"
#pragma link "ExecStatusBarUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "ColorPanel"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorLabel"
#pragma resource "*.dfm"

#define IMPORT_FRAME_LOAD_THRESHOLD 10
#define TRACKBAR_LOAD_THRESHOLD     10
#define DENSITY_LOAD_THRESHOLD       5

#define PLUGIN_TAB_INDEX 2

#define NEW_BRUSHES
#define MIN_CHART_EXTENT 2
#define DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE 50

TPaintToolForm *PaintToolForm;

//---------------------------------------------------------------------------
__fastcall TPaintToolForm::TPaintToolForm(TComponent* Owner)
 : TMTIForm(Owner)
{
   // initialize strings for reading & writing color palette
   char vbl[20];
   for (int i=0; i<COL_ROWS; i++) {
      for (int j=0; j<COL_COLS; j++) {
          sprintf(vbl, "RedColor[%d][%d]",i,j);
          redStr[i][j] = vbl;
          sprintf(vbl, "GrnColor[%d][%d]",i,j);
          grnStr[i][j] = vbl;
          sprintf(vbl, "BluColor[%d][%d]",i,j);
          bluStr[i][j] = vbl;
          sprintf(vbl, "Value[%d][%d]",i,j);
          valStr[i][j] = vbl;
      }
   }

   densityTimer = DENSITY_LOAD_THRESHOLD + 1;

   toleranceTimer = TRACKBAR_LOAD_THRESHOLD + 1;

   _grainPresetOverrideCurrentValue = DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE;
}

//---------------------------------------------------------------------------

void TPaintToolForm::formCreate(void)
{
   // allocate big preview bitmap
   bitmap_big = new Graphics::TBitmap;
   bitmap_big->PixelFormat = pf32bit;
   bitmap_big->Height = 89;
   bitmap_big->Width  = 89;

   // allocate small preview bitmaps
   for (int i = 0; i < NUM_BRUSH; i++)
   {
    bitmap[i] = new Graphics::TBitmap;
    bitmap[i]->PixelFormat = pf32bit;
    bitmap[i]->Height = 89;
    bitmap[i]->Width  = 89;
   }

   // allocate the wainscoting for the color palette
   palFrm = new Graphics::TBitmap;
   palFrm->PixelFormat = pf32bit;
   palFrm->Width  = COL_COLS*COL_WDTH + (COL_COLS-1)*COL_BETW;
   palFrm->Height = COL_ROWS*COL_HGHT + (COL_ROWS-1)*COL_BETW;

   // fill it with clrBtnFace
   int rows = palFrm->Height;
   int cols = palFrm->Width;
   for (int i=0; i<rows;i++) {
      MTI_UINT32 *dst = (MTI_UINT32 *)palFrm->ScanLine[i];
      for (int j=0;j<cols;j++)
          dst[j] = 0x00f0f0f0;
   }

   // allocate the color palette..
   for (int i = 0; i < COL_ROWS; i++) {
      for (int j = 0; j < COL_COLS; j++) {
         palCol[i][j] = new Graphics::TBitmap;
         palCol[i][j]->PixelFormat = pf32bit;
         palCol[i][j]->Width  = COL_WDTH;
         palCol[i][j]->Height = COL_HGHT;
      }
   }

   // no color is selected
   palSelectedRow = 0;
   palSelectedCol = 0;

   // the hue-saturation box..
   hueSat = new Graphics::TBitmap;
   hueSat->PixelFormat = pf32bit;
   hueSat->Width  = HueSaturationPaintBox->Width;
   hueSat->Height = HueSaturationPaintBox->Height;

   // and the value-meter
   valMeter = new Graphics::TBitmap;
   valMeter->PixelFormat = pf32bit;
   valMeter->Width  = valWdth = ValueMeterPaintBox->Width;
   valMeter->Height = valHght = ValueMeterPaintBox->Height;

   valArrow = new Graphics::TBitmap;
   valArrow->PixelFormat = pf32bit;
   valArrow->Width  = MWD;
   valArrow->Height = MHT;

   valBlank = new Graphics::TBitmap;
   valBlank->PixelFormat = pf32bit;
   valBlank->Width  = MWD;
   valBlank->Height = MHT;

   valBlklin = new Graphics::TBitmap;
   valBlklin->PixelFormat = pf32bit;
   valBlklin->Width  = MWD;
   valBlklin->Height = 1;

   maxComponentValue = 255;

   // now init the above GUI elements
   // with color
   InitColorGUI();

   proxyBitmap = new Graphics::TBitmap;
   proxyBitmap->PixelFormat = pf32bit;
   proxyBitmap->Width  = ProxyPaintBox->Width;
   proxyBitmap->Height = ProxyPaintBox->Height;

   origProxyBitmap = new Graphics::TBitmap;
   origProxyBitmap->PixelFormat = pf32bit;
   origProxyBitmap->Width  = OrigProxyPaintBox->Width;
   origProxyBitmap->Height = OrigProxyPaintBox->Height;

   SetOnionSkinMode(ONIONSKIN_MODE_POSNEG);
   SetOrigOnionSkinMode(ONIONSKIN_MODE_POSNEG);
   SetAltClipOnionSkinMode(ONIONSKIN_MODE_POSNEG);

   previousBrush = 0;
   brushMode = BRUSH_MODE_THRU;

   localStrokeRelative = true;

   localDensityStrength = 0;

   SET_CBHOOK(TrackingPointsChanged, GPaintTool->getTrackingPointsEditor()->TrackingPointStateChange);
   SET_CBHOOK(GrainPresetsChanged, GPaintTool->GetGrainPresets().GrainPresetsStateChange);

   oldTrackValidState = TRKVLD_INVALID_STATE;
   oldTrackingButtonEnabledState = TRKBTN_INVALID_STATE;

   InhibitTrackAlignButtonClick = false;  // HACK
   InhibitTrackingButtonClick = false;    // HACK
   InhibitGuiUpdateHackFlag = false;      // HACK

   RangeWarningPanel->ControlStyle << csOpaque;
   RangeWarningImage->ControlStyle << csOpaque;
}
//---------------------------------------------------------------------------

void TPaintToolForm::formShow()
{
   stupidFrickenHackToNotTryToFocusOnTheMainWindow = true;

   // this sequence makes the active mask immediately useable
   // by cycling through MAJORSTATE_EDIT_MASK. Make SURE that
   // the subsequent setting of MAJORSTATE_GUI_CREATED stays!
   GPaintTool->SelectMaskQuickly();
   GPaintTool->setMajorState(MAJORSTATE_GUI_CREATED);
   setMajorState(MAJORSTATE_GUI_CREATED);

   // initialize for import proxy generation
   GPaintTool->getSystemAPI()->initImportProxy(PaintToolForm->ProxyPaintBox->Width,
                                               PaintToolForm->ProxyPaintBox->Height);

	// Grain presets
	auto grainPresetsBase = GPaintTool->GetGrainPresets().GetSelectedBase();
	if (grainPresetsBase == GP_BASE_CLIP)
	{
		GrainPresetsClipRadioButton->Checked = true;
	}
	else
	{
		GrainPresetsProjectRadioButton->Checked = true;
	}

	// CAREFUL! ugly off-by one index.
	const int GRAIN_PRESET_2_INDEX = 1;
	auto grainPresetsIndex = GPaintTool->GetGrainPresets().GetSelectedIndex();
	if (grainPresetsIndex == GRAIN_PRESET_2_INDEX)
	{
		GrainPreset2Button->Down = true;
	}
	else
	{
		GrainPreset1Button->Down = true;
	}

   GrainUsePresetsCheckBoxClick(nullptr);

   stupidFrickenHackToNotTryToFocusOnTheMainWindow = false;
}
//---------------------------------------------------------------------------

void TPaintToolForm::formDestroy(void)
{
   delete proxyBitmap;

   for (int i = NUM_BRUSH-1 ; i >= 0 ; i--) {
      delete bitmap[i];
   }

   delete bitmap_big;

   REMOVE_CBHOOK(TrackingPointsChanged, GPaintTool->getTrackingPointsEditor()->TrackingPointStateChange);
   REMOVE_CBHOOK(GrainPresetsChanged, GPaintTool->GetGrainPresets().GrainPresetsStateChange);
}
//---------------------------------------------------------------------------

void TPaintToolForm::formPaint(void)
{
   // brush GUI
   DrawDisplay();

   // proxy GUI
   TRect rect_proxy;
   rect_proxy.left   = 0;
   rect_proxy.top    = 0;
   rect_proxy.right  = importProxyWdth;
   rect_proxy.bottom = importProxyHght;

   if ((importProxy != NULL)&&importProxy->Visible) {
      importProxy->Canvas->StretchDraw (rect_proxy, importProxyBitmap);
   }
}
//---------------------------------------------------------------------------

bool TPaintToolForm::formCloseQuery(void)
{
   bool CanClose = true;

   if (GPaintTool == NULL) return CanClose;

   if (GPaintTool->getSystemAPI()->targetFrameIsModified()) {

      _MTIErrorDialog(Handle, "You must accept or reject the pending fix.");

      // snap back to target frame when dialog goes down
      ViewLabel->Caption = "Processed";

      GPaintTool->getSystemAPI()->setDisplayProcessed(true);

      //int trg, imp;
      //GPaintTool->getSystemAPI()->getPaintFrames(&trg, &imp);
      //GPaintTool->getSystemAPI()->goToFrameSynchronous(trg);

      GPaintTool->getSystemAPI()->goToFrameSynchronous(GPaintTool->getTargetFrame());

      CanClose = false;
   }
   else

     // tell system that user wants to deactivate PAINT.  Returns false
     // if a fix is pending, etc., so cannot close
     CanClose = GPaintTool->getSystemAPI()->DeactivateTool(GPaintTool);

   return CanClose;
}

//---------------------------------------------------------------------------
//##############                                       ######################
//##############    Basic Functions copied from Grain  ######################
//##############                                       ######################
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

bool HandleToolEventTrampoline(const FosterParentEventInfo &eventInfo)
{
    return PaintToolForm->HandleToolEvent(eventInfo);
}

//------------------CreatePaintToolGUI--------------------

   void CreatePaintToolGUI(void)

// This creates the PaintTool GUI if one does not already exist.
// Note: only one can be created.
//
//****************************************************************************
{
   if (PaintToolForm == NULL) {

      PaintToolForm = new TPaintToolForm(Application);   // Create it

      PaintToolForm->formCreate();     // unitool

      PaintToolForm->RestoreProperties();

      // Move all our controls to the unified tabbed tool window
      extern const char *PluginName;
      GPaintTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
            reinterpret_cast<int *>( PaintToolForm->PaintControlPanel ),
            &HandleToolEventTrampoline);

   }

   GPaintTool->setMajorState(MAJORSTATE_GUI_CREATED);
   PaintToolForm->setMajorState(MAJORSTATE_GUI_CREATED);

   // For now the status bar is only used during tracking
   GPaintTool->SetToolProgressMonitor(PaintToolForm->ExecStatusBar);

}

//-----------------DestroyPaintToolGUI------------------

  void DestroyPaintToolGUI(void)

// This destroys the entire GUI interface. It is called more than
// once, so be careful...
//
//***************************************************************************
{
   if ((GPaintTool != NULL)&&(PaintToolForm != NULL)) {

      // important! we can't have the timer going off
      // and calling the tool when GPaintTool = NULL
      PaintToolForm->RotateStretchTimer->Enabled = false;

      // This was already done in HidePaintToolGUI!
      //GPaintTool->getSystemAPI()->exitPaintMode();

      GPaintTool->setMajorState(MAJORSTATE_GUI_CREATED);
      PaintToolForm->setMajorState(MAJORSTATE_GUI_CREATED);

      // Reparent the controls back to us before destroying them
      if (GPaintTool != NULL)
         GPaintTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);

      PaintToolForm->PaintControlPanel->Parent = PaintToolForm;

      PaintToolForm->formDestroy();     // unitool

      PaintToolForm->Free();

      PaintToolForm = NULL;
   }
}

//-------------------ShowPaintToolGUI-------------------

   bool ShowPaintToolGUI(void)

//  This creates the GUI and then shows it
//
//****************************************************************************
{
   CreatePaintToolGUI();            // Create the gui if necessary

   if (PaintToolForm == NULL) return false;

   PaintToolForm->formShow();


   // make PAINT visible
   PaintToolForm->PaintControlPanel->Visible = true;


   // enter the special display mode
   // KKKKKK GPaintTool->getSystemAPI()->enterPaintMode();

   return true;  // was: (PaintToolForm->Visible);
}


//-----------------HidePaintToolGUIQuery----------------

    bool HidePaintToolGUIQuery(void)

// This is called by the tool manager to test hideability
//
//****************************************************************************
{
   if (PaintToolForm == NULL) return(true);

   bool canClose = true;

   if (GPaintTool->getSystemAPI()->targetFrameIsModified()) {

      if (PaintToolForm->AutoAcceptCheckBox->Checked) {

          if (GPaintTool->AcceptProcessedTargetFrame(false) != 0) {

             _MTIErrorDialog("Auto-accept failed!");
             canClose = false;
          }

          // make sure that calls to refreshFrameCached in
          // tool beng transferred to have the new frame
          // already in the Player's FIFO
          // BUT ONLY IF IT"S THE CURRENTLY DISPLAYED FRAME!!

          int trg, imp;
          GPaintTool->getSystemAPI()->getPaintFrames(&trg, &imp);
          if (trg == GPaintTool->getSystemAPI()->getLastFrameIndex())
          {
             GPaintTool->getSystemAPI()->goToFrameSynchronous(trg);
          }
      }
      else {

          _MTIErrorDialog("You must accept or reject the pending fix.");
          canClose = false;
      }
  }
  if (!canClose) {

      // snap back to target frame when dialog goes down
      PaintToolForm->ViewLabel->Caption = "Processed";

      GPaintTool->getSystemAPI()->setDisplayProcessed(true);

      GPaintTool->goToTargetFrame();
   }

   return(canClose);
}

//-------------------HidePaintToolGUI-------------------

    bool  HidePaintToolGUI(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
   if (PaintToolForm == NULL) return(false);

   PaintToolForm->PaintControlPanel->Visible = false;

   if (GPaintTool    != NULL) {

		if (GPaintTool->getMajorState() == MAJORSTATE_EDIT_TRACKING
		|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_DONE
		|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_PAUSED)
		{
         GPaintTool->LeaveEditTrackingState();
      }
	}

   PaintToolForm->setMajorState(MAJORSTATE_GUI_CREATED);

	return false;
}

// Nobody calls this
////-------------------------NewClip--------------------------------------------
//
//  void NewClip()
//
//{
//   // absolute brush radii change, but GUI bitmaps don't
//   PaintToolForm->AdjustBrushSizesForNewClip();
//   GPaintTool->InitClonePt();
//   PaintToolForm->SetCloneBrushRelative(true);
//   PaintToolForm->ReadPaletteColors();
//   GPaintTool->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
//   PaintToolForm->setMajorState(MAJORSTATE_REVEAL_MODE_HOME);
//   GPaintTool->setAlignState(ALIGNSTATE_MANUAL);
//   PaintToolForm->setAlignState(ALIGNSTATE_MANUAL);
//   GPaintTool->readImportFrameAndOffset();
//}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
    if (PaintToolForm == NULL) return(false);         // Not created
    return PaintToolForm->PaintControlPanel->Visible;
          // was: (PaintToolForm->Visible);
}

//---------------------------------------------------------------------------
//##############                                       ######################
//##############      Handling User Changes to GUI     ######################
//##############                                       ######################
//---------------------------------------------------------------------------
//###################  Brush Type Change ####################################
//---------------------------------------------------------------------------
void TPaintToolForm::LoseTheFocus()
{
   if (stupidFrickenHackToNotTryToFocusOnTheMainWindow)
   {
      return;
   }

   // Activate a random non-responsive control
   DummyFocusPanel->SetFocus();

   // Move the focus to the main window
   GPaintTool->getSystemAPI()->setMainWindowFocus();
}

//---------------------------------------------------------------------------
//###################  Radius Track Bar Change ##############################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RadiusTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_RADIUS);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RadiusTrackBarChange(TObject *Sender)
{
   int newRadius = RadiusTrackBar->Position;

   // set the label to show the value of radius
   char val[5];
   sprintf(val,"%4d", newRadius);
   RadiusValueLabel->Caption = val;
   RadiusValueLabel->Repaint();

   // update brush
   brush[currentBrush]->setRadius(newRadius);

////   if (RadiusTrackBar->Focused())
   {
      // center brush in window
      GPaintTool->getSystemAPI()->forceMouseToWindowCenter();
   }

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RadiusTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyDown(Key, Shift);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RadiusTrackBarKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyUp(Key, Shift);
}

//---------------------------------------------------------------------------
//###################  Blend Track Bar Change ###############################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BlendTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_BLEND);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BlendTrackBarChange(TObject *Sender)
{
   int newBlend = BlendTrackBar->Position;

   // set the label to show the value of blend
   char val[4];
   sprintf(val,"%3d", newBlend);
   BlendValueLabel->Caption = val;
   BlendValueLabel->Repaint();

   // update brush
   constBrush->setBlend(newBlend);
   brush[currentBrush]->setBlend(newBlend);

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BlendTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyDown(Key, Shift);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BlendTrackBarKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyUp(Key, Shift);
}

//---------------------------------------------------------------------------

//###################  Opacity Track Bar Change ###############################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::OpacityTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_OPACITY);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::OpacityTrackBarChange(TObject *Sender)
{
   int newOpacity = OpacityTrackBar->Position;

   // set the label to show the value of blend
   char val[4];
   sprintf(val,"%3d", newOpacity);
   OpacityValueLabel->Caption = val;
   OpacityValueLabel->Repaint();

   // update brush
   constBrush->setOpacity(newOpacity);
   brush[currentBrush]->setOpacity(newOpacity);

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::OpacityTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyDown(Key, Shift);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::OpacityTrackBarKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyUp(Key, Shift);
}

//---------------------------------------------------------------------------

//###################  Aspect Track Bar Change ###############################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AspectTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_ASPECT);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AspectTrackBarChange(TObject *Sender)
{
   int newAspect = AspectTrackBar->Position;

   // set the label to show the value of blend
   char val[4];
   sprintf(val,"%3d", newAspect);
   AspectValueLabel->Caption = val;
   AspectValueLabel->Repaint();

   // update brush
   constBrush->setAspect(newAspect);
   brush[currentBrush]->setAspect(newAspect);

////   if (AspectTrackBar->Focused())
   {
      // center brush in window
      GPaintTool->getSystemAPI()->forceMouseToWindowCenter();
   }

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AspectTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyDown(Key, Shift);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AspectTrackBarKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyUp(Key, Shift);
}

//---------------------------------------------------------------------------

//###################  Angle Track Bar Change ###############################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AngleTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_ANGLE);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AngleTrackBarChange(TObject *Sender)
{
   int newAngle = AngleTrackBar->Position;

   // set the label to show the value of blend
   char val[4];
   sprintf(val,"%3d", newAngle);
   AngleValueLabel->Caption = val;
   AngleValueLabel->Repaint();

   // update brush
   constBrush->setAngle(newAngle);
   brush[currentBrush]->setAngle(newAngle);

////   if (AngleTrackBar->Focused())
   {
      // center brush in window
      GPaintTool->getSystemAPI()->forceMouseToWindowCenter();
   }

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AngleTrackBarKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyDown(Key, Shift);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AngleTrackBarKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   formKeyUp(Key, Shift);
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::RndSpeedButtonClick(TObject *Sender)
{
   RndSpeedButton->Down = true;

   constBrush->setShape(false);
   brush[currentBrush]->setShape(false);

   ChangeDisplay();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::SqSpeedButtonClick(TObject *Sender)
{
   SqSpeedButton->Down = true;

   constBrush->setShape(true);
   brush[currentBrush]->setShape(true);

   ChangeDisplay();
}
//---------------------------------------------------------------------------

void TPaintToolForm::AllocateBrushes()
{
   // allocate constant-sized brush for big preview
   constBrush = new CBrush;

   // allocate brushes
   for (int i = 0; i < NUM_BRUSH; i++) {
      brush[i] = new CBrush;
   }
}

void TPaintToolForm::FreeBrushes()
{
   // free brushes
   for (int i = NUM_BRUSH-1; i >= 0; i--) {
      delete brush[i];
      brush[i] = NULL;
   }

   // free constant-sized brush for big preview
   delete constBrush;
   constBrush = NULL;
}

//---------------------------------------------------------------------------
void TPaintToolForm::LoadBrush()
{
   float *colorcomp = NULL;

   int majorState     = GPaintTool->getMajorState();
   int nextMajorState = GPaintTool->getNextMajorState();

   // DO NOT mess with the brush while we are in the middle of drawing a
   // color stroke! This was causing the painting to switch into REVEAL mode
   // in mid-stroke!
   if (majorState == MAJORSTATE_COLOR_PIXELS)
   {
      return;
   }

   if ((majorState == MAJORSTATE_COLOR_MODE_HOME)||
       (majorState == MAJORSTATE_PICK_COLOR)||
       (majorState == MAJORSTATE_PICK_SEED)||
       ((majorState == MAJORSTATE_GET_ORIGINAL_VALUES)&&
        (nextMajorState == MAJORSTATE_COLOR_MODE_HOME))) {

       colorcomp = localRGB;
   }

   // load the brush, mode, and thresholds
   GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush],
                                              brushMode,
                                              0, 0,
                                              colorcomp);
   // load the clone brush, if displayed
   if ((majorState == MAJORSTATE_CLONE_MODE_HOME)||
       (majorState == MAJORSTATE_CHOOSE_CLONE_PT)||
       (majorState == MAJORSTATE_CHOOSE_CLONE_PT_ALT)||
       (majorState == MAJORSTATE_MOVE_CLONE_PT)||
       (majorState == MAJORSTATE_CLONE_PIXELS))

      GPaintTool->ReloadCloneBrush();
}

//---------------------------------------------------------------------------
void TPaintToolForm::RecalculateBrush()
{
   brush[currentBrush]->getWeight( 0, 0);
}

//---------------------------------------------------------------------------
void TPaintToolForm::SetCurrentBrush(int newbrush)
{
   if (currentBrush >= NUM_BRUSH)
      currentBrush = NUM_BRUSH - 1;
   previousBrush = currentBrush;

   if (newbrush >= NUM_BRUSH)
      newbrush     = NUM_BRUSH - 1;
   currentBrush  = newbrush;

   // grab the current brush
   constBrush->setBrushParametersFromBrush(brush[currentBrush]);

   // and change its radius to a constant
   constBrush->setRadius(100.);

   // load the brush into the Displayer
   LoadBrush();

   // populate gui with brush 1 parameters
   UpdateGUIFromBrush();

   // update brush preview tbitmap
   ChangeDisplay ();
}

//---------------------------------------------------------------------------
int  TPaintToolForm::GetCurrentBrushIndex()
{
   return currentBrush;
}

//---------------------------------------------------------------------------
CBrush *TPaintToolForm::GetCurrentBrush()
{
   return(brush[currentBrush]);
}

//---------------------------------------------------------------------------
CBrush *TPaintToolForm::GetBrush(int index)
{
   return(brush[index]);
}

//---------------------------------------------------------------------------
void TPaintToolForm::ResetCurrentBrush()
{
   SetCurrentBrush(currentBrush);
}

//---------------------------------------------------------------------------
void TPaintToolForm::ResetRadiusTrackBar()
{
   const CImageFormat *imgFmt;
   RECT frmRect;

   // max brush radius = 50% of frame
   imgFmt = GPaintTool->getSystemAPI()->getVideoClipImageFormat();
   frmRect = imgFmt->getActivePictureRect();

   RadiusTrackBar->OnChange = NULL;
   RadiusTrackBar->Max = (frmRect.right - frmRect.left + 1)/2;
   RadiusTrackBar->OnChange = RadiusTrackBarChange;
}

void TPaintToolForm::ChangeRadius(float fctr, int updn)
{
   // inhibit the extra cycle here
   // when the trackbar is focused
   // and arrow key is being used
   if ((mainTime - lastTime) < 1) {
      lastTime = mainTime;
      return;
   }
   lastTime = mainTime;

    // change trackbar pos, triggering label update
   int newRadius = RadiusTrackBar->Position;
   newRadius = newRadius*fctr + updn;
   if (fctr > 1.0) newRadius++;
   if (newRadius < 1) newRadius = 1;

   RadiusTrackBar->Position = newRadius;
}

void TPaintToolForm::ChangeBlend(int updn)
{
   // inhibit the extra cycle here
   // when the trackbar is focused
   // and arrow key is being used
   if ((mainTime - lastTime) < 1) {
      lastTime = mainTime;
      return;
   }
   lastTime = mainTime;

   int newBlend = BlendTrackBar->Position;
   newBlend += updn;

   BlendTrackBar->Position = newBlend;
}


void TPaintToolForm::ChangeOpacity(int updn)
{
   // inhibit the extra cycle here
   // when the trackbar is focused
   // and arrow key is being used
   if ((mainTime - lastTime) < 1) {
      lastTime = mainTime;
      return;
   }
   lastTime = mainTime;

   int newOpacity = OpacityTrackBar->Position;
   newOpacity += updn;

   OpacityTrackBar->Position = newOpacity;
}

void TPaintToolForm::ChangeAspect(int updn)
{
   // inhibit the extra cycle here
   // when the trackbar is focused
   // and arrow key is being used
   if ((mainTime - lastTime) < 1) {
      lastTime = mainTime;
      return;
   }
   lastTime = mainTime;

   int newAspect = AspectTrackBar->Position;
   newAspect += updn;

   AspectTrackBar->Position = newAspect;
}

void TPaintToolForm::ChangeAngle(int updn)
{
   // inhibit the extra cycle here
   // when the trackbar is focused
   // and arrow key is being used
   if ((mainTime - lastTime) < 1) {
      lastTime = mainTime;
      return;
   }
   lastTime = mainTime;

   int newAngle = AngleTrackBar->Position;
   newAngle += updn;

   AngleTrackBar->Position = newAngle;
}

void TPaintToolForm::ToggleEllipseRect()
{
   bool isSquare = SqSpeedButton->Down;
   constBrush->setShape(!isSquare);
   brush[currentBrush]->setShape(!isSquare);

   RndSpeedButton->Down =  isSquare;
   SqSpeedButton->Down  = !isSquare;

   ChangeDisplay();
}

void TPaintToolForm::AdjustBrushSizesForNewClip()
{
   const CImageFormat *imgFmt;
   RECT frmRect;
   int oldTrackBarMax;
   int oldTrackBarPos;
   int newTrackBarMax;

   oldTrackBarMax = RadiusTrackBar->Max;
   oldTrackBarPos = RadiusTrackBar->Position;

   // max brush radius = 10% of frame
   imgFmt = GPaintTool->getSystemAPI()->getVideoClipImageFormat();
   frmRect = imgFmt->getActivePictureRect();

   // WAS newTrackBarMax = (((frmRect.right - frmRect.left)/10)/10)*10;

   newTrackBarMax = (((frmRect.right - frmRect.left)/2 + 9)/10)*10;

   for (int i=0;i<10;i++) {
      // the call to setRadius frees the storage associated with the brush
      brush[i]->setRadius(((brush[i]->getRadius()/(double)oldTrackBarMax)*newTrackBarMax));
   }

   // don't call the "OnChange" method
   RadiusTrackBar->OnChange = NULL;
   RadiusTrackBar->Max = newTrackBarMax;
   RadiusTrackBar->Position = RadiusTrackBar->Max * ((double)oldTrackBarPos / (double)oldTrackBarMax);
   RadiusTrackBar->OnChange = RadiusTrackBarChange;
}

//---------------------------------------------------------------------------
CBrush **TPaintToolForm::GetBrushes()
{
   return brush;
}

//---------------------------------------------------------------------------

void TPaintToolForm::SetOnionSkinMode(int newmode)
{
   if (newmode == ONIONSKIN_MODE_COLOR) {
      PaintToolForm->ColorSkinButton->Down  = true;
      PaintToolForm->DiffSkinButton->Down   = false;
   }
   if (newmode == ONIONSKIN_MODE_POSNEG) {
      PaintToolForm->ColorSkinButton->Down  = false;
      PaintToolForm->DiffSkinButton->Down   = true;
   }

   localOnionSkinMode = newmode;
}

//---------------------------------------------------------------------------

int TPaintToolForm::GetOnionSkinMode()
{
   return localOnionSkinMode;
}

//---------------------------------------------------------------------------

void TPaintToolForm::SetOrigOnionSkinMode(int newmode)
{
   if (newmode == ONIONSKIN_MODE_COLOR) {
      PaintToolForm->OrigMixSkinButton->Down  = true;
      PaintToolForm->OrigDiffSkinButton->Down = false;
   }
   if (newmode == ONIONSKIN_MODE_POSNEG) {
      PaintToolForm->OrigMixSkinButton->Down  = false;
      PaintToolForm->OrigDiffSkinButton->Down = true;
   }

   localOrigOnionSkinMode = newmode;
}

//---------------------------------------------------------------------------

int TPaintToolForm::GetOrigOnionSkinMode()
{
   return localOrigOnionSkinMode;
}
//---------------------------------------------------------------------------

void TPaintToolForm::SetAltClipOnionSkinMode(int newmode)
{
   if (newmode == ONIONSKIN_MODE_COLOR) {
      PaintToolForm->AltClipMixButton->Down  = true;
      PaintToolForm->AltClipDiffButton->Down = false;
   }
   if (newmode == ONIONSKIN_MODE_POSNEG) {
      PaintToolForm->AltClipMixButton->Down  = false;
      PaintToolForm->AltClipDiffButton->Down = true;
   }

   localAltClipOnionSkinMode = newmode;
}

//---------------------------------------------------------------------------

int TPaintToolForm::GetAltClipOnionSkinMode()
{
   return localAltClipOnionSkinMode;
}

//---------------------------------------------------------------------------

void TPaintToolForm::SetBrushMode(int newmode)
{
   brushMode = newmode;
}

//---------------------------------------------------------------------------

int TPaintToolForm::GetBrushMode()
{
   return brushMode;
}

//---------------------------------------------------------------------------

void TPaintToolForm::SetCloneBrushRelative(bool rel)
{
   localStrokeRelative = rel;
   ClonePointOffsetSpeedButton->Down = localStrokeRelative;
   ClonePointLockSpeedButton->Down = !localStrokeRelative;
}

bool TPaintToolForm::CloneBrushIsRelative()
{
   return localStrokeRelative;
}

//---------------------------------------------------------------------------

void TPaintToolForm::UpdateStateOfAcceptRejectUndoAndMacroButtons(bool okToEnable)
{
   if (okToEnable)
   {
      // these are lit if the frame WAS changed, regardless
      // of the contents of the stroke stack (which might
      // contain ERASE strokes)
      bool enableAcceptReject = GPaintTool->EnableAcceptReject();
      AcceptButton->Enabled = enableAcceptReject;
      RejectButton->Enabled = enableAcceptReject;

      // these reflect the contents of the stroke stack
      bool stackNonEmpty = (PaintToolForm->PendingStrokesListBox->Count > 0);
      UndoLastButton->Enabled         = stackNonEmpty;
      UndoThruSelectButton->Enabled   = stackNonEmpty;
      RangeStrokeSpeedButton->Enabled = stackNonEmpty && GPaintTool->MarksAreValid();

      // This is maintained elsewhere, it seems. Uggh.
		////PlayStrokeSpeedButton->Enabled  = GPaintTool->IsPreviousStrokeChannelNonEmpty();

		// HACK to inform FrameCompareTool of pending state.
		GPaintTool->getSystemAPI()->setPaintToolChangesPending(stackNonEmpty);
	}
   else
   {
      AcceptButton->Enabled           = false;
		RejectButton->Enabled           = false;
      UndoLastButton->Enabled         = false;
      UndoThruSelectButton->Enabled   = false;
      //PlayStrokeSpeedButton->Enabled  = false;
      RangeStrokeSpeedButton->Enabled = false;
		GPaintTool->getSystemAPI()->setPaintToolChangesPending(false);
	}
}

//---------------------------------------------------------------------------

void TPaintToolForm::UpdateStateOfTrackAlignButton(bool okToEnable)
{
   bool wantTrackAlignEnabled;
   if (okToEnable)
   {
      int trackValidState = GPaintTool->getTrackingDataValidity();
      wantTrackAlignEnabled = (trackValidState != TRKVLD_NEED_TO_RETRACK);
   }
   else
   {
      wantTrackAlignEnabled = false;
      GetOutOfTrackAlignMode();
   }

   if (!wantTrackAlignEnabled)
   {
      GetOutOfTrackAlignMode();
   }

   TrackAlignButton->Enabled = wantTrackAlignEnabled;
}

//---------------------------------------------------------------------------

void TPaintToolForm::ShowRejectMessage()
{
   //AnsiString s = "Processed frame rejected - ";
   string s = "Processed frame rejected - ";

   switch(GPaintTool->getMajorState()) {

      case MAJORSTATE_EDIT_TRANSFORM:

          s = s + "edit import transform";

      break;

      case MAJORSTATE_REVEAL_MODE_HOME:
      case MAJORSTATE_ALTCLIP_MODE_HOME:

         switch(brushMode) {

            case BRUSH_MODE_THRU:

               s = s + "paint pixels";

            break;

            case BRUSH_MODE_ERASE:

               s = s + "erase pixels";

            break;
         }

      break;

      case MAJORSTATE_ORIG_MODE_HOME:

         switch(brushMode) {

            case BRUSH_MODE_ORIG:

               s = s + "paint pixels";

            break;

            case BRUSH_MODE_ERASE:

               s = s + "erase pixels";

            break;
         }

      break;
   }

   //Label1->Caption = s;
   ExecStatusBar->SetStatusMessage(s.c_str());

}

//---------------------------------------------------------------------------

void TPaintToolForm::ShowAcceptMessage()
{
   //AnsiString s = "Processed frame accepted - ";
   string s = "Processed frame accepted - ";

   switch(GPaintTool->getMajorState()) {

      case MAJORSTATE_EDIT_TRANSFORM:

          s = s + "edit import transform";

      break;

      case MAJORSTATE_PAINT_PIXELS:

         switch(brushMode) {

            case BRUSH_MODE_THRU:

               s = s + "paint pixels";

            break;

            case BRUSH_MODE_ERASE:

               s = s + "erase pixels";

            break;
         }

      break;

      case MAJORSTATE_PAINT_ORIG_PIXELS:

         switch(brushMode) {

            case BRUSH_MODE_ORIG:

               s = s + "paint pixels";

            break;

            case BRUSH_MODE_ERASE:

               s = s + "erase pixels";

            break;
         }

      break;
   }

   //Label1->Caption = s;
   ExecStatusBar->SetStatusMessage(s.c_str());
}

//---------------------------------------------------------------------------
//###################  Select Brush 1 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_01Click(TObject *Sender)
{
   // set current brush to brush 1
   SetCurrentBrush(0);

   // reset the focus
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Brush 2 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_02Click(TObject *Sender)
{
   // set current brush to brush 2
   SetCurrentBrush(1);

   // reset the focus
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Brush 3 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_03Click(TObject *Sender)
{
   // set current brush to brush 3
   SetCurrentBrush(2);

   // reset the focus
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Brush 4 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_04Click(TObject *Sender)
{
   // set current brush to brush 4
   SetCurrentBrush(3);

   // reset the focus
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Brush 5 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_05Click(TObject *Sender)
{
   // set current brush to brush 5
   SetCurrentBrush(4);

   // reset the focus
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Brush 6 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_06Click(
      TObject *Sender)
{
   // set current brush to brush 6
   SetCurrentBrush(5);

   // reset the focus
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 7 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_07Click(
      TObject *Sender)
{
   // set current brush to brush 7
   SetCurrentBrush(6);

   // reset the focus
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 8 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_08Click(
      TObject *Sender)
{
   // set current brush to brush 8
   SetCurrentBrush(7);

   // reset the focus
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 9 #######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_09Click(
      TObject *Sender)
{
   // set current brush to brush 9
   SetCurrentBrush(8);

   // reset the focus
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 10 ######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_10Click(
      TObject *Sender)
{
   // set current brush to brush 10
   SetCurrentBrush(9);

   // reset the focus
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 11 ######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_11Click(
      TObject *Sender)
{
   // set current brush to brush 11
   //SetCurrentBrush(10);

   // reset the focus
   //LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select Brush 12 ######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::BrushSelectPaintBox_12Click(
      TObject *Sender)
{
   // set current brush to brush 12
   //SetCurrentBrush(11);

   // reset the focus
   //LoseTheFocus();
}

//---------------------------------------------------------------------------
//##############                                       ######################
//##############        Brush Display Functions        ######################
//##############                                       ######################
//---------------------------------------------------------------------------
void TPaintToolForm::SelectBrushBitmap(int brush, bool select)
{
  int red, grn, blu;
  if (select) {
     red = 255;
     grn = 0;
     blu = 0;
  }
  else {
     red = 255;
     grn = 255;
     blu = 255;
  }

  TRGBQuad *rgb;

  rgb = (TRGBQuad *)bitmap[brush]->ScanLine[0];
  for (int i=0; i<=88; i++) {
     rgb[i].rgbRed      = red;
     rgb[i].rgbGreen    = grn;
     rgb[i].rgbBlue     = blu;
     rgb[i].rgbReserved = 0;
  }

  for (int i=1; i<=87; i++) {

     rgb = (TRGBQuad *)bitmap[brush]->ScanLine[i];
     rgb[0].rgbRed      = red;
     rgb[0].rgbGreen    = grn;
     rgb[0].rgbBlue     = blu;
     rgb[0].rgbReserved = 0;

     rgb = (TRGBQuad *)bitmap[brush]->ScanLine[i];
     rgb[88].rgbRed      = red;
     rgb[88].rgbGreen    = grn;
     rgb[88].rgbBlue     = blu;
     rgb[88].rgbReserved = 0;
  }

  rgb = (TRGBQuad *)bitmap[brush]->ScanLine[88];
  for (int i=0; i<=88; i++) {
     rgb[i].rgbRed      = red;
     rgb[i].rgbGreen    = grn;
     rgb[i].rgbBlue     = blu;
     rgb[i].rgbReserved = 0;
  }
}

void TPaintToolForm::LoadBrushBitmap(int brushnum)
{
   // use this coefficient so that small previews change size
   double a = (double) RadiusTrackBar->Max / (double)44.;

   double c = 255. / UNITY;

   // un-select previous brush (redraw brush without select box)
   for (int iRow = -44; iRow <= 44; iRow++)
   {
      TRGBQuad *rgb = (TRGBQuad*)bitmap[brushnum]->ScanLine[iRow + 44];
      for (int iCol = -44; iCol <= 44; iCol++)
      {
         int iWeight = (UNITY - brush[brushnum]->getWeight((int)(a * iRow), (int)(a * iCol))) * c;
         rgb->rgbRed = iWeight;
         rgb->rgbGreen = iWeight;
         rgb->rgbBlue = iWeight;
         rgb->rgbReserved = 0;
         rgb++;
      }
   }
}

void TPaintToolForm::LoadActiveBitmap(int brushnum)
{
  // use this coefficient to get big preview constant size
  //double b = (double) 144. / (double) 44.;
  double b = (double) RadiusTrackBar->Max / (double) 44.;
  b *= 100./brush[brushnum]->getRadius();

  double c = 255./UNITY;

  // fill bitmap for big preview
  for (int iRow = -44; iRow <= 44; iRow++)
   {
    TRGBQuad *rgb = (TRGBQuad *)bitmap[brushnum]->ScanLine[iRow+44];
    for (int iCol = -44; iCol <= 44; iCol++)
     {
      int iWeight = (UNITY-constBrush->getWeight ((int)(b*iRow), (int)(b*iCol)))*c;
      rgb->rgbRed = iWeight;
      rgb->rgbGreen = iWeight;
      rgb->rgbBlue = iWeight;
      rgb->rgbReserved = 0;
      rgb++;
     }
   }
}

void TPaintToolForm::LoadPreviewBitmap()
{
  // use this coefficient to get big preview constant size
  double b = (double) 144. / (double) 44.;

  double c = 255./UNITY;

  // fill bitmap for big preview
  for (int iRow = -44; iRow <= 44; iRow++)
   {
    TRGBQuad *rgb = (TRGBQuad *)bitmap_big->ScanLine[iRow+44];
    for (int iCol = -44; iCol <= 44; iCol++)
     {
      int iWeight = (UNITY-constBrush->getWeight ((int)(b*iRow), (int)(b*iCol)))*c;
      rgb->rgbRed = iWeight;
      rgb->rgbGreen = iWeight;
      rgb->rgbBlue = iWeight;
      rgb->rgbReserved = 0;
      rgb++;
     }
   }
}

void TPaintToolForm::LoadBrushBitmaps ()
{
   LoadPreviewBitmap();

   for (int i = 0; i < NUM_BRUSH; i++)
   {
      LoadBrushBitmap(i);
   }

   SelectBrushBitmap(currentBrush, true);
}

void TPaintToolForm::DrawPreviewDisplay()
{
  TRect rect_big;
  rect_big.top = 0;
  rect_big.bottom = BrushPreviewPaintBox->Height;
  rect_big.left = 0;
  rect_big.right = BrushPreviewPaintBox->Width;

  BrushPreviewPaintBox->Canvas->StretchDraw (rect_big, bitmap_big);
}

void TPaintToolForm::DrawSmallDisplay(int brushnum)
{
  if ((brushnum < 0)||(brushnum >= NUM_BRUSH)) return;

  TRect rect_small;
  rect_small.top = 0;
  rect_small.bottom = BrushSelectPaintBox_01->Height;
  rect_small.left = 0;
  rect_small.right = BrushSelectPaintBox_01->Width;

  switch(brushnum) {

     case 0:
        BrushSelectPaintBox_01->Canvas->StretchDraw (rect_small, bitmap[0]);
     break;
     case 1:
        BrushSelectPaintBox_02->Canvas->StretchDraw (rect_small, bitmap[1]);
     break;
     case 2:
        BrushSelectPaintBox_03->Canvas->StretchDraw (rect_small, bitmap[2]);
     break;
     case 3:
        BrushSelectPaintBox_04->Canvas->StretchDraw (rect_small, bitmap[3]);
     break;
     case 4:
        BrushSelectPaintBox_05->Canvas->StretchDraw (rect_small, bitmap[4]);
     break;
     case 5:
        BrushSelectPaintBox_06->Canvas->StretchDraw (rect_small, bitmap[5]);
     break;
     case 6:
        BrushSelectPaintBox_07->Canvas->StretchDraw (rect_small, bitmap[6]);
     break;
     case 7:
        BrushSelectPaintBox_08->Canvas->StretchDraw (rect_small, bitmap[7]);
     break;
     case 8:
        BrushSelectPaintBox_09->Canvas->StretchDraw (rect_small, bitmap[8]);
     break;
     case 9:
        BrushSelectPaintBox_10->Canvas->StretchDraw (rect_small, bitmap[9]);
     break;
#if 0
     case 10:
        BrushSelectPaintBox_11->Canvas->StretchDraw (rect_small, bitmap[10]);
     break;
     case 11:
        BrushSelectPaintBox_12->Canvas->StretchDraw (rect_small, bitmap[11]);
     break;
#endif
     default:
     break;
  }
}

void TPaintToolForm::DrawDisplay ()
{
  // load the large display
  DrawPreviewDisplay();

  // load each of the small ones
  for (int i=0; i<NUM_BRUSH; i++)
     DrawSmallDisplay(i);

  // draw the entire 7 x 5 color palette  from the bitmap array
  ColorPaletteDrawFrame();
  for (int i=0;i<COL_ROWS;i++) {
	 for (int j=0;j<COL_COLS;j++) {
		ColorPaletteDrawBox(i,j);
	 }
  }

  ColorPaletteSelectBox(palSelectedRow, palSelectedCol, true);
  //
  // must precede LoadBrush
  //
  UpdateRGB();
  UpdateColorPickSampleSize();
  UpdateFillTolerance();

  // brush -> Displayer - must FOLLOW UpdateRGB
  LoadBrush();

  TRect rect_hsv;
  rect_hsv.left   =  0;
  rect_hsv.top    =  0;
  rect_hsv.right  = HueSaturationPaintBox->Width;
  rect_hsv.bottom = HueSaturationPaintBox->Height;

  HueSaturationPaintBox->Canvas->StretchDraw (rect_hsv, hueSat);

  TRect rect_val;
  rect_val.left   = 0;
  rect_val.top    = 0;
  rect_val.right  = valWdth;
  rect_val.bottom = valHght;

  ValueMeterPaintBox->Canvas->StretchDraw (rect_val, valMeter);

  ValueMeterSetLevel(INI_VAL);
}

void TPaintToolForm::InitGUIBrushPanel()
{
   // the repaints below do no good unless
   // the following is set visible - dep.
   // on EditTrackingState being disabled - but
   // TrackingPtsEditor is not yet inited
   //
   BrushGroupBox->Visible = true;

   // get these to show up
   ThruButton->Repaint();
   EraseButton->Repaint();
   MaskButton->Repaint();
   TrackingButton->Repaint();

   // reset the range here
   ResetRadiusTrackBar();

   // update the GUI elements
   UpdateGUIFromBrush();

   // load the bitmaps
   LoadBrushBitmaps();

   // now display 'em
   DrawDisplay();
}

void TPaintToolForm::ConvertHueSat2RGB( float h, float s,
                                        float *red, float *grn, float *blu)
{
   int i;
   float r, g, b;
   float f, p, q, t, v;

   if( s == 0 ) {

      // achromatic (grey)
      r = g = b = 1.0;
   }
   else {

      h /= 60;			// sector 0 to 5
      i = floor(h);
      f = h - i;		// factorial part
      p = ( 1 - s );
      q = ( 1 - s * f );
      t = ( 1 - s * ( 1 - f ) );
      v = 1.0;
      switch( i ) {
   	 case 0:
    	    r = v;
   	    g = t;
   	    b = p;
   	 break;
   	 case 1:
   	    r = q;
   	    g = v;
   	    b = p;
   	 break;
   	 case 2:
   	    r = p;
   	    g = v;
   	    b = t;
   	 break;
   	 case 3:
   	    r = p;
   	    g = q;
   	    b = v;
   	 break;
   	 case 4:
   	    r = t;
   	    g = p;
   	    b = v;
   	 break;
   	 default:
   	    r = v;
	    g = p;
	    b = q;
	 break;
      }
   }

   if (red != NULL) *red = r;
   if (grn != NULL) *grn = g;
   if (blu != NULL) *blu = b;
}

MTI_UINT32 TPaintToolForm::AdjustColorValue(float r, float g, float b, float val)
{
   r *= val; g *= val; b *= val;

   return (MTI_UINT32)(b*255) + ((MTI_UINT32)(g*255)<<8) + ((MTI_UINT32)(r*255)<<16);
}

void TPaintToolForm::BitmapDrawLine(Graphics::TBitmap *bm, unsigned int col,
                                 int x1, int y1, int x2, int y2)
{
  unsigned int *dst;

  if      (y1 == y2) { // horz

     int xi = x1; int xf = x2;
     if (x1 > x2) {
        xi = x2; xf = x1;
     }

     dst = (unsigned int *)bm->ScanLine[y1];
     for (int i=xi; i<=xf; i++) {
        dst[i] = col;
     }

  }
  else if (x1 == x2) { // vert

     int yi = y1; int yf = y2;
     if (y1 > y2) {
        yi = y2; yf = y1;
     }

     for (int j=yi; j<=yf; j++) {
        dst = (unsigned int *)bm->ScanLine[j];
        dst[x1] = col;
     }
  }
}

MTI_UINT32 TPaintToolForm::BitmapGetBoxRGB(Graphics::TBitmap *bm)
{
   unsigned int *dst = (unsigned int *)bm->ScanLine[3];
   return dst[3];
}

void TPaintToolForm::BitmapDrawBox(Graphics::TBitmap *bm,
                                   unsigned int col, bool sel)
{
   unsigned int *dst;
   for (int i=0; i<COL_HGHT; i++) {
      dst = (unsigned int *)bm->ScanLine[i];
      for (int j=0; j<COL_WDTH; j++)
         dst[j] = col;
   }

   BitmapDrawLine(bm, BLACK, 0, 0, COL_WDTH-1, 0);
   BitmapDrawLine(bm, BLACK, COL_WDTH-1, 0, COL_WDTH-1, COL_HGHT-1);
   BitmapDrawLine(bm, BLACK, 0, COL_HGHT-1, COL_WDTH-1, COL_HGHT-1);
   BitmapDrawLine(bm, BLACK, 0, 0, 0, COL_HGHT-1);

   if (sel) {

      BitmapDrawLine(bm, WHITE, 1, 1, COL_WDTH-2, 1);
      BitmapDrawLine(bm, WHITE, COL_WDTH-2, 1, COL_WDTH-2, COL_HGHT-2);
      BitmapDrawLine(bm, WHITE, 1, COL_HGHT-2, COL_WDTH-2, COL_HGHT-2);
      BitmapDrawLine(bm, WHITE, 1, 1, 1, COL_HGHT-2);

      BitmapDrawLine(bm, BLACK, 2, 2, COL_WDTH-3, 2);
      BitmapDrawLine(bm, BLACK, COL_WDTH-3, 2, COL_WDTH-3, COL_HGHT-3);
      BitmapDrawLine(bm, BLACK, 2, COL_HGHT-3, COL_WDTH-3, COL_HGHT-3);
      BitmapDrawLine(bm, BLACK, 2, 2, 2, COL_HGHT-3);
   }
}

void TPaintToolForm::BitmapSelectBox(Graphics::TBitmap *bm, bool sel)
{
   unsigned int col = BitmapGetBoxRGB(bm);
   BitmapDrawBox(bm, col, sel);
}

void TPaintToolForm::ColorPaletteDrawFrame()
{
   TRect rect_col;
   rect_col.left   = 0;
   rect_col.top    = 0;
   rect_col.right  = ColorPalettePaintBox->Width;
   rect_col.bottom = ColorPalettePaintBox->Height;

   ColorPalettePaintBox->Canvas->StretchDraw (rect_col, palFrm);
}

void TPaintToolForm::ColorPaletteDrawBox(int row, int col)
{
   if ((row<0)||(row>=COL_ROWS)) return;
   if ((col<0)||(col>=COL_COLS)) return;

   // draw the color palette
   TRect rect_col;
   rect_col.left   =  col*(COL_WDTH+COL_BETW);
   rect_col.top    =  row*(COL_HGHT+COL_BETW);
   rect_col.right  =  rect_col.left + COL_WDTH;
   rect_col.bottom =  rect_col.top  + COL_HGHT;

   ColorPalettePaintBox->Canvas->StretchDraw (rect_col, palCol[row][col]);
}

void TPaintToolForm::ColorPaletteSelectBox(int row, int col, bool sel)
{
   if ((row<0)||(row>=COL_ROWS)) return;
   if ((col<0)||(col>=COL_COLS)) return;

   BitmapSelectBox(palCol[row][col], sel);
   ColorPaletteDrawBox(row, col);
}

int TPaintToolForm::HueSaturationGetRGB(int x, int y,
                                        float *red, float *grn, float *blu)
{
  int hsvwdth = hueSat->Width;
  int hsvhght = hueSat->Height;

  if ((x>0)&&(x<(hsvwdth-1))&&(y>0)&&(y<(hsvhght-1))) {

     float h = 360.*(x-1) / (float)(hsvwdth-2);
     float s = (float)(hsvhght-1-y)/(hsvhght-2);

     ConvertHueSat2RGB( h, s, red, grn, blu);

     return 0;
  }

  return -1;
}

void TPaintToolForm::ValueMeterSetLevel(float v)
{
  if (v < 0.0) v = 0.0;
  if (v > 1.0) v = 1.0;

  int y = 1 + (1.0 - v) * (valHght-3);

  TRect rect_arr;
  rect_arr.left   = MWD+2;
  rect_arr.top    = valY - MWD + 1;
  rect_arr.right  = 2*MWD+1;
  rect_arr.bottom = valY + MWD - 1;

  ValueMeterPaintBox->Canvas->StretchDraw (rect_arr, valBlank);

  rect_arr.top    = y - MWD + 1;
  rect_arr.bottom = y + MWD - 1;

  ValueMeterPaintBox->Canvas->StretchDraw (rect_arr, valArrow);

  rect_arr.left   = MWD+2;
  rect_arr.top    = 0;
  rect_arr.right  = 2*MWD+2;
  rect_arr.bottom = 1;

  ValueMeterPaintBox->Canvas->StretchDraw (rect_arr, valBlklin);

  rect_arr.top    = ValueMeterPaintBox->Height-1;
  rect_arr.bottom = rect_arr.top + 1;

  ValueMeterPaintBox->Canvas->StretchDraw (rect_arr, valBlklin);

  valY = y;

  return;
}

void TPaintToolForm::InitColorGUI()
{
   // a 7 x 5 HSV palette
   for (int i=0; i<COL_ROWS; i++) {

      float s = (float)(4 - i)/4.;
      for (int j=0; j<COL_COLS; j++) {

         float h = 360.*j/(float)COL_COLS;
         ConvertHueSat2RGB( h, s, &redCol[i][j],
                                  &grnCol[i][j],
                                  &bluCol[i][j]);
         valCol[i][j] = INI_VAL;

         MTI_UINT32 col = AdjustColorValue(redCol[i][j],
                                           grnCol[i][j],
                                           bluCol[i][j], INI_VAL);

         BitmapDrawBox(palCol[i][j], col, false);
      }
   }

   // analog HSV selector
   int hsvwdth = hueSat->Width;
   int hsvhght = hueSat->Height;
   for (int i=1;i<hsvhght-1;i++) {
      unsigned int *dst = (unsigned int *)hueSat->ScanLine[i];
      for (int j=1;j<hsvwdth-1;j++) {
         float r, g, b;
         HueSaturationGetRGB(j, i, &r, &g, &b);
         r *= INI_VAL;
         g *= INI_VAL;
         b *= INI_VAL;
         dst[j] = ((MTI_UINT32)(255*b)) +
                  (((MTI_UINT32)(255*g))<<8) +
                  (((MTI_UINT32)(255*r))<<16);
      }
   }

   BitmapDrawLine(hueSat, BLACK,   0, 0, hsvwdth-1, 0);
   BitmapDrawLine(hueSat, BLACK,   hsvwdth-1, 0, hsvwdth-1, hsvhght-1);
   BitmapDrawLine(hueSat, BLACK,   hsvwdth-1, hsvhght-1, 0, hsvhght-1);
   BitmapDrawLine(hueSat, BLACK,   0, hsvhght-1, 0, 0);

   // make an indicator arrow
   for (int i=0;i<MWD;i++) {

      unsigned int *dst = (unsigned int *)valArrow->ScanLine[MWD-1-i];
      int j;
      for (j=0;j<i;j++) {
         dst[j] = WHITE;
      }
      for (;j<MWD;j++) {
         dst[j] = BLACK;
      }
      dst =               (unsigned int *)valArrow->ScanLine[MWD-1+i];
      for (j=0;j<i;j++) {
         dst[j] = WHITE;
      }
      for (;j<MWD;j++) {
         dst[j] = BLACK;
      }
   }

   // make a blank for erasing same
   for (int i=0;i<MHT;i++) {

      unsigned int *dst = (unsigned int *)valBlank->ScanLine[i];
      for (int j=0;j<MWD;j++) {
         dst[j] = WHITE;
      }
   }

   // make a black line
   unsigned int *dst = (unsigned int *)valBlklin->ScanLine[0];
   for (int j=0;j<MWD;j++) {
      dst[j] = BLACK;
   }

   // now make the shades of gray meter
   for (int i=1;i<valHght-1;i++) {

      unsigned int val = (float)(valHght-1-i)*255/(float)(valHght-2);
      val = val + (val<<8) + (val<<16);

      unsigned int *dst = (unsigned int *)valMeter->ScanLine[i];
      for (int j=1;j<valWdth/2;j++) {
         dst[j] = val;
      }
   }

   BitmapDrawLine(valMeter, BLACK,   0, 0, valWdth-1, 0);
   BitmapDrawLine(valMeter, BLACK,   valWdth-1, 0, valWdth-1, valHght-1);
   BitmapDrawLine(valMeter, BLACK,   valWdth-1, valHght-1, 0, valHght-1);
   BitmapDrawLine(valMeter, BLACK,   0, valHght-1, 0, 0);
}

int TPaintToolForm::ReadPaletteColors()
{
   string iniFileName = GPaintTool->GetBinPath() + "Palette.ini";
   CIniFile *iniFile = CreateIniFile(iniFileName);
   if (iniFile == NULL) {
      TRACE_1(errout << "CPaintTool::readPaletteColors: cannot open Palette.ini");
      return -1;
   }

   string sectionName = "PaletteColors";

   for (int i=0; i < COL_ROWS; i++) {

      float s = (float)(4 - i)/4.;

      for (int j=0; j < COL_COLS; j++) {

         float h = 360.*j/(float)COL_COLS;

         ConvertHueSat2RGB( h, s, &redCol[i][j],
                                  &grnCol[i][j],
                                  &bluCol[i][j]);
         valCol[i][j] = INI_VAL;

         // now read from Palette.ini

         redCol[i][j] = iniFile->ReadDouble(sectionName,
                                            redStr[i][j],
                                            redCol[i][j]);

         grnCol[i][j] = iniFile->ReadDouble(sectionName,
                                            grnStr[i][j],
                                            grnCol[i][j]);

         bluCol[i][j] = iniFile->ReadDouble(sectionName,
                                            bluStr[i][j],
                                            bluCol[i][j]);

         valCol[i][j] = iniFile->ReadDouble(sectionName,
                                            valStr[i][j],
                                            valCol[i][j]);

         MTI_UINT32 col = AdjustColorValue(redCol[i][j],
                                           grnCol[i][j],
                                           bluCol[i][j],
                                           valCol[i][j]);

         BitmapDrawBox(palCol[i][j], col, false);
      }
   }

   sectionName = "SelectedColor";

   string rowStr     = "SelectedRow";
   string colStr     = "SelectedCol";
   string colPickStr = "ColorPickSampleSize";
   string fillTolStr = "FillTolerance";

   palSelectedRow = iniFile->ReadInteger(sectionName,
                                         rowStr,
                                         0);

   palSelectedCol = iniFile->ReadInteger(sectionName,
                                         colStr,
                                         0);

   localColorPickSampleSize = iniFile->ReadInteger(sectionName,
                                                   colPickStr,
                                                   1);

   localFillTolerance = iniFile->ReadInteger(sectionName,
                                             fillTolStr,
                                             10);
   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::readPaletteColors: cannot close Palette.ini");
   }

	const CImageFormat * imgFmt = GPaintTool->getSystemAPI()->getVideoClipImageFormat();
	componentCount = imgFmt->getComponentCountExcAlpha();
   maxComponentValue = ((int)1<<(imgFmt->getBitsPerComponent()))-1;

   // essential to call this before LoadBrush
   UpdateRGB();

   return 0;
}

int TPaintToolForm::WritePaletteColors()
{
   string iniFileName = GPaintTool->GetBinPath() + "Palette.ini";
   CIniFile *iniFile = CreateIniFile(iniFileName);
   if (iniFile == NULL) {
      TRACE_1(errout << "CPaintTool::writePaletteColors: cannot open Palette.ini");
      return -1;
   }

   string sectionName = "PaletteColors";

   for (int i=0; i < COL_ROWS; i++) {

      for (int j=0; j< COL_COLS; j++) {

         iniFile->WriteDouble(sectionName,
                              redStr[i][j],
                              redCol[i][j]);

         iniFile->WriteDouble(sectionName,
                              grnStr[i][j],
                              grnCol[i][j]);

         iniFile->WriteDouble(sectionName,
                              bluStr[i][j],
                              bluCol[i][j]);

         iniFile->WriteDouble(sectionName,
                              valStr[i][j],
                              valCol[i][j]);
      }
   }

   sectionName = "SelectedColor";

   string rowStr     = "SelectedRow";
   string colStr     = "SelectedCol";
   string colPickStr = "ColorPickSampleSize";
   string fillTolStr = "FillTolerance";

   iniFile->WriteInteger(sectionName,
                        rowStr,
                        palSelectedRow);

   iniFile->WriteInteger(sectionName,
                        colStr,
                        palSelectedCol);

   iniFile->WriteInteger(sectionName,
                         colPickStr,
                         localColorPickSampleSize);

   iniFile->WriteInteger(sectionName,
                         fillTolStr,
                         localFillTolerance);

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CPaintTool::readPaletteColors: cannot write Palette.ini");
   }

   return 0;
}

void TPaintToolForm::SetPickedColor(float *comp)
{
   float val = 0.;
   for (int i=0;i<3;i++) {
      if (comp[i] > val) val = comp[i];
   }

   redCol[palSelectedRow][palSelectedCol] = comp[0]/val;
   grnCol[palSelectedRow][palSelectedCol] = comp[1]/val;
   bluCol[palSelectedRow][palSelectedCol] = comp[2]/val;
   valCol[palSelectedRow][palSelectedCol] = val;

   MTI_UINT32 color = AdjustColorValue(redCol[palSelectedRow][palSelectedCol],
                                       grnCol[palSelectedRow][palSelectedCol],
                                       bluCol[palSelectedRow][palSelectedCol],
                                       valCol[palSelectedRow][palSelectedCol]);

   BitmapDrawBox(palCol[palSelectedRow][palSelectedCol], color, true);
   ColorPaletteDrawBox(palSelectedRow, palSelectedCol);

   ValueMeterSetLevel(val);

   UpdateRGB();
   LoadBrush();
}

int TPaintToolForm::GetFillTolerance()
{
   return localFillTolerance;
}

void TPaintToolForm::RangeMarksErrorDialog()
{
   _MTIErrorDialog(Handle, "IN and OUT marks must be correctly set");
}

int TPaintToolForm::ConvertImportFrameEditTextToFrameIndex()
{
//   TRACE_3(errout << "ENTER");
   int impfrm = -1;

   // first get a timecode with the right fps
   CVideoFrameList *frameList;
   frameList = GPaintTool->getSystemAPI()->getImportClipVideoFrameList();
   if (frameList != NULL) {

      CTimecode imptim = frameList->getTimecodeForFrameIndex(0);

      // now set the timecode from the edit box string
      char impstr[32];
      strcpy(impstr, StringToStdString(ReferenceFrameTimecodeEdit->Text).c_str());
      char *strpt = impstr;
      while (*strpt == ' ') strpt++;
      if (imptim.setTimeASCII(strpt)) {

         // get the new import frame index from the timecode
         impfrm = frameList->getFrameIndex(imptim);
      }
   }
//   TRACE_3(errout << "EXIT");

   return impfrm;
}

void TPaintToolForm::SetProxy(TPaintBox *proxy, Graphics::TBitmap *proxyBitmap)
{
   importProxy = proxy;
   importProxyBitmap = proxyBitmap;

   importProxyWdth = 128;
   importProxyHght = 96;
   if (proxy != NULL) {
      importProxyWdth = proxy->Width;
      importProxyHght = proxy->Height;
   }

   GPaintTool->getSystemAPI()->initImportProxy(importProxyWdth, importProxyHght);
}

void TPaintToolForm::DrawProxy(unsigned int *proxyImg)
{
   if ((proxyImg == NULL)||(importProxyBitmap == NULL)) return;

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   TRect rect_proxy;
   rect_proxy.left   = 0;
   rect_proxy.top    = 0;
   rect_proxy.right  = importProxyWdth;
   rect_proxy.bottom = importProxyHght;

   // fill proxy bitmap from proxy img
   unsigned char *src = (unsigned char *)proxyImg;
   for (int j = 0; j < importProxyHght; j++) {
     unsigned char *dst = (unsigned char *)importProxyBitmap->ScanLine[j];
     for (int i = 0; i < importProxyWdth; i++) {
        dst[0] = src[2];
        dst[1] = src[1];
        dst[2] = src[0];
        dst[3] = 0;

        src += 4;
        dst += 4;

     }
   }

   if ((importProxy != NULL)&&(importProxy->Visible))
      importProxy->Canvas->StretchDraw (rect_proxy, importProxyBitmap);
}

void TPaintToolForm::ChangeDisplay()
{
  // unselect the previous brush
  SelectBrushBitmap(previousBrush, false);
  DrawSmallDisplay(previousBrush);

  // load the new current brush & select it
  LoadActiveBitmap(currentBrush);
  SelectBrushBitmap(currentBrush, true);
  DrawSmallDisplay(currentBrush);

  // load the preview from const brush
  LoadPreviewBitmap();
  DrawPreviewDisplay();

  // display the frame with the new brush visible
  int majorstate = GPaintTool->getMajorState();
  if ((majorstate == MAJORSTATE_SELECTING_BRUSH) ||
      (majorstate == MAJORSTATE_REVEAL_MODE_HOME)||
      (majorstate == MAJORSTATE_CLONE_MODE_HOME) ||
        (majorstate == MAJORSTATE_CHOOSE_CLONE_PT)||
        (majorstate == MAJORSTATE_CHOOSE_CLONE_PT_ALT)||
        (majorstate == MAJORSTATE_MOVE_CLONE_PT)||
        (majorstate == MAJORSTATE_CLONE_PIXELS)||
      (majorstate == MAJORSTATE_COLOR_MODE_HOME)||
      (majorstate == MAJORSTATE_ORIG_MODE_HOME)||
      (majorstate == MAJORSTATE_ALTCLIP_MODE_HOME)) {

    if (!GPaintTool->getSystemAPI()->isDisplaying()) {
       GPaintTool->getSystemAPI()->refreshFrameCached(true);
    }
  }


 }

// ###################  Updates the GUI with brush parameters ##################
//
void TPaintToolForm::UpdateGUIFromBrush ()
{
   if (brush[currentBrush] == nullptr)
   {
      return;
   }

   // RADIUS
   //
   RadiusLabel->Repaint();

   int localRadius = brush[currentBrush]->getRadius();

   char ra_val[4];
   sprintf(ra_val,"%2d", localRadius);
   RadiusValueLabel->Caption = ra_val;
   RadiusValueLabel->Repaint();

   RadiusTrackBar->OnChange = NULL;
   RadiusTrackBar->Position = localRadius;
   RadiusTrackBar->OnChange = RadiusTrackBarChange;
   RadiusTrackBar->Repaint();

   // BLEND
   //
   BlendLabel->Repaint();

   int localBlend = brush[currentBrush]->getBlend();

   char bl_val[4];
   sprintf(bl_val,"%2d", localBlend);
   BlendValueLabel->Caption = bl_val;
   BlendValueLabel->Repaint();

   BlendTrackBar->OnChange = NULL;
   BlendTrackBar->Position = localBlend;
   BlendTrackBar->OnChange = BlendTrackBarChange;
   BlendTrackBar->Repaint();

   // OPACITY
   //
   OpacityLabel->Repaint();

   int localOpacity = brush[currentBrush]->getOpacity();

   char op_val[4];
   sprintf(op_val,"%2d", localOpacity);
   OpacityValueLabel->Caption = op_val;
   OpacityValueLabel->Repaint();

   OpacityTrackBar->OnChange = NULL;
   OpacityTrackBar->Position = localOpacity;
   OpacityTrackBar->OnChange = OpacityTrackBarChange;
   OpacityTrackBar->Repaint();

   // ASPECT
   //
   AspectLabel->Repaint();

   int localAspect = brush[currentBrush]->getAspect();

   char as_val[4];
   sprintf(as_val,"%2d", localAspect);
   AspectValueLabel->Caption = as_val;
   AspectValueLabel->Repaint();

   AspectTrackBar->OnChange = NULL;
   AspectTrackBar->Position = localAspect;
   AspectTrackBar->OnChange = AspectTrackBarChange;
   AspectTrackBar->Repaint();

   // ANGLE
   //
   AngleLabel->Repaint();

   int localAngle = brush[currentBrush]->getAngle();

   char an_val[4];
   sprintf(an_val,"%2d", localAngle);
   AngleValueLabel->Caption = an_val;
   AngleValueLabel->Repaint();

   AngleTrackBar->OnChange = NULL;
   AngleTrackBar->Position = localAngle;
   AngleTrackBar->OnChange = AngleTrackBarChange;
   AngleTrackBar->Repaint();

   // SHAPE
   bool newSquare = brush[currentBrush]->getShape();
   RndSpeedButton->Down = !newSquare;
   SqSpeedButton->Down  = newSquare;
}
//###############  End UpdateGUIFromBrush #######################

//---------------------------------------------------------------------------
//######################  Set Mask State ####################################
//---------------------------------------------------------------------------
void TPaintToolForm::setMaskState(bool enbl)
{
#if 0
   DrawRectSpeedButton->Enabled = enbl;
   DrawLassoSpeedButton->Enabled = enbl;
   DrawBezierSpeedButton->Enabled = enbl;
   IncludeSpeedButton->Enabled = enbl;
   ExcludeSpeedButton->Enabled = enbl;
   InvertButton->Enabled = enbl;
   DeleteRegionButton->Enabled = enbl;
   ClearAllButton->Enabled = enbl;
   BorderButton->Enabled = enbl;
#endif

   //MaskButton->Down = enbl;
   //MaskButton->Repaint();
}

//---------------------------------------------------------------------------
//##################  Set Tracking State ####################################
//---------------------------------------------------------------------------
void TPaintToolForm::setTrackingState(bool enbl)
{
   if (TrackingGroupBox->Visible == enbl)
   {
      return;
   }

   BrushGroupBox->Visible    = !enbl;
   TrackingGroupBox->Visible =  enbl;
   //TrackingStatusPanel->Visible = enbl;
   UpdateTrackingPointButtons();
   UpdateTrackingCharts();
   UpdateTrackingDataValidLight();
   TrackingTrackBarChanged(NULL);
}

//---------------------------------------------------------------------------
//#################  Set Mouse WheelState ###################################
//---------------------------------------------------------------------------

void TPaintToolForm::setMouseWheelState(int newMouseWheelState)
{
   int oldMouseWheelState = GPaintTool->getMouseWheelState();

   if (newMouseWheelState != oldMouseWheelState) {

      switch(oldMouseWheelState) {

         case MOUSE_WHEEL_ADJUST_MIX :

            RefLabel->Font->Style = (TFontStyles());
            TgtLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_ORIG_MIX :

            OrigLabel->Font->Style = (TFontStyles());
            CurLabel->Font->Style = (TFontStyles());

         case MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX :

            AltClipSrcLabel->Font->Style = (TFontStyles());
            AltClipTgtLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_OFFSET :

            XLabel->Font->Style = (TFontStyles());
            YLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_TOLERANCE :

            TolHeading->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_RADIUS :

            RadiusLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_OPACITY :

            OpacityLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_BLEND :

            BlendLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_ASPECT :

            AspectLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_ANGLE :

            AngleLabel->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_DENSITY :

            DensitySpeedButton->Down = false;
            DensitySpeedButton->Font->Style = (TFontStyles());

         break;

         case MOUSE_WHEEL_ADJUST_GRAIN :

            GrainSpeedButton->Down = false;
            GrainSpeedButton->Font->Style = (TFontStyles());
            UpdateGrainPresetButtons();

         break;
      }


      switch(newMouseWheelState) {

         case MOUSE_WHEEL_ADJUST_MIX :
            RefLabel->Font->Style = (TFontStyles() << fsBold);
            TgtLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_ORIG_MIX :
            OrigLabel->Font->Style = (TFontStyles() << fsBold);
            CurLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX :
            AltClipSrcLabel->Font->Style = (TFontStyles() << fsBold);
            AltClipTgtLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_OFFSET :
            XLabel->Font->Style = (TFontStyles() << fsBold);
            YLabel->Font->Style = (TFontStyles() << fsBold);
            break;

        case MOUSE_WHEEL_ADJUST_TOLERANCE :
            TolHeading->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_RADIUS :
            RadiusLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_OPACITY :
            OpacityLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_BLEND :
            BlendLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_ASPECT :
            AspectLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_ANGLE :
            AngleLabel->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_DENSITY :
            DensitySpeedButton->Down = true;
            DensitySpeedButton->Font->Style = (TFontStyles() << fsBold);
            break;

         case MOUSE_WHEEL_ADJUST_GRAIN :
            GrainSpeedButton->Down = true;
            GrainSpeedButton->Font->Style = (TFontStyles() << fsBold);
            UpdateGrainPresetButtons();
            break;
      }

      // make it the new state
      GPaintTool->setMouseWheelState(newMouseWheelState);
   }
}

//---------------------------------------------------------------------------
//###################  Set GUI MajorState ###################################
//---------------------------------------------------------------------------
void TPaintToolForm::setMajorState(int majorstate)
{
   int saveCurrentBrush;

   bool acceptReject;

   const CImageFormat *imgFmt;

   // Always clear the progress bar when we change major states
   ExecStatusBar->SetIdle(false);

   switch(majorstate) {

      case MAJORSTATE_GUI_CREATED:

         setMaskState(false);
         MaskButton->Enabled = true;
         MaskButton->Down    = false;
         MaskButton->Visible = true;
         MaskButton->Repaint();

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         TrackingButton->Visible = true;
         setTrackingState(false);
         TrackingButton->Repaint();

         RevealButton->Enabled = true;
         RevealButton->Visible = true;
         RevealButton->Repaint();

         CloneButton->Enabled = true;
         CloneButton->Visible = true;
         CloneButton->Repaint();

         ColorButton->Enabled = true;
         ColorButton->Visible = true;
         ColorButton->Repaint();

         OrigValuesButton->Enabled = true;
         OrigValuesButton->Visible = true;
         OrigValuesButton->Repaint();

         AlternateClipButton->Enabled = true;
         AlternateClipButton->Visible = true;
         AlternateClipButton->Repaint();

		 // ReferenceFrameBox
         ReferenceFrameBox->Visible = false;

         ProxyPaintBox->Visible              = false;
         ReferenceFrameTimecodeEdit->Visible = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameLockButton->Visible   = false;
         ReferenceFrameOffsetButton->Visible = false;
         ReferenceFrameOffsetEdit->Visible   = false;

         // REVEAL mode elements

         QuickAlignButton->Visible = false;
         TrackAlignButton->Visible = false;
         InvertButton->Visible     = false;

         PositionImportButton->Visible = false;
         DiffSkinButton->Visible       = false;
         ColorSkinButton->Visible      = false;
         RefLabel->Visible             = false;
         // MixingTrackBar->Visible = false;
         MixingTrackBarPanel->Visible = false;
         TgtLabel->Visible            = false;

         RotationImage->Visible   = false;
         RotationEditBox->Visible = false;
         SkewImage->Visible       = false;
         SkewEditBox->Visible     = false;
         HLabel->Visible          = false;
         HEditBox->Visible        = false;
         VLabel->Visible          = false;
         VEditBox->Visible        = false;
         XLabel->Visible          = false;
         XEditBox->Visible        = false;
         YLabel->Visible          = false;
         YEditBox->Visible        = false;

         // CLONE mode elements

         CloneModeLabel->Visible              = false;
         SetClonePointSpeedButton->Visible    = false;
         ClonePointOffsetSpeedButton->Visible = false;
         ClonePointLockSpeedButton->Visible   = false;
         CloneXLabel->Visible                 = false;
         CloneXEditBox->Visible               = false;
         CloneYLabel->Visible                 = false;
         CloneYEditBox->Visible               = false;
         CloneDelXLabel->Visible              = false;
         CloneDelXEditBox->Visible            = false;
         CloneDelYLabel->Visible              = false;
         CloneDelYEditBox->Visible            = false;

         // ColorPickerBox

         ColorPickerBox->Visible = false;

         // COLOR mode elements

         ColorPalettePaintBox->Enabled  = true;
         ColorPalettePaintBox->Visible  = true;
         HueSaturationPaintBox->Enabled = true;
         HueSaturationPaintBox->Visible = true;
         ValueMeterPaintBox->Enabled    = true;
         ValueMeterPaintBox->Visible    = true;
         RYLabel->Visible               = true;
         RYEditBox->Enabled             = true;
         RYEditBox->Visible             = true;
         GULabel->Visible               = true;
         GUEditBox->Enabled             = true;
         GUEditBox->Visible             = true;
         BVLabel->Visible               = true;
         BVEditBox->Enabled             = true;
         BVEditBox->Visible             = true;
         DropperSpeedButton->Enabled    = true;
         DropperSpeedButton->Visible    = true;
         AvgSizeComboBox->Enabled       = true;
         AvgSizeComboBox->Visible       = true;
         BucketFillSpeedButton->Enabled = true;
         BucketFillSpeedButton->Visible = true;
         TolHeading->Visible            = true;
         ToleranceLabel->Visible        = true;
         ToleranceTrackBar->Enabled     = true;
         ToleranceTrackBar->Visible     = true;

         ReferenceFrameBox->Visible = true;
         ColorPickerBox->Visible = false;
         OriginalFrameBox->Visible = false;
         AltClipFrameBox->Visible = false;

         BrushGroupBox->Visible = false;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Accept / reject / undo / macros
         ClearStrokeStack();
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         StopSpeedButton->Enabled = false;
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         // Main Timer
         RotateStretchTimer->Enabled = true;

         // Maybe turn off "alt source clip import"
         SaveAndClearImportFromAltSourceClipState();

      break;

      case MAJORSTATE_REVEAL_MODE_HOME:

         setAlignState(GPaintTool->getAlignState());

         // Maybe turn off "alt source clip import"
         SaveAndClearImportFromAltSourceClipState();

      break;

      case MAJORSTATE_CLONE_MODE_HOME:

         MaskButton->Enabled = true;
         MaskButton->Down = false;
         setMaskState(false);

         TrackingButton->Enabled = true;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Clone Mode is enabled, down
         RevealButton->Enabled     = true;
         CloneButton->Down         = true;
         CloneButton->Enabled      = true;
         ColorButton->Enabled      = true;
         OrigValuesButton->Enabled = true;
         AlternateClipButton->Enabled = true;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = true;
         ResetDensityPanel->Enabled  = true;
         GrainSpeedButton->Enabled   = true;
         ResetGrainPanel->Enabled    = true;

         // Brush mode buttons
         ThruButton->Enabled  = true;
         EraseButton->Enabled = true;

//         ColorPickerBox->Visible = false;

         // ReferenceFrameBox

         SetProxy(ProxyPaintBox, proxyBitmap);
         ProxyPaintBox->Visible              = true;
         ReferenceFrameTimecodeEdit->Enabled = true;
         ReferenceFrameTimecodeEdit->Visible = true;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameLockButton->Enabled   = true;
         ReferenceFrameLockButton->Visible   = true;
         ReferenceFrameOffsetButton->Enabled = true;
         ReferenceFrameOffsetButton->Visible = true;
         ReferenceFrameOffsetEdit->Enabled   = true;
         ReferenceFrameOffsetEdit->Visible   = true;

         // REVEAL mode elements off

         QuickAlignButton->Visible = false;
         TrackAlignButton->Visible = false;
         InvertButton->Visible     = false;

         PositionImportButton->Visible = false;
         DiffSkinButton->Visible       = false;
         ColorSkinButton->Visible      = false;
         RefLabel->Visible             = false;
         // MixingTrackBar->Visible = false;
         MixingTrackBarPanel->Visible = false;
         TgtLabel->Visible            = false;

         RotationImage->Visible   = false;
         RotationEditBox->Visible = false;
         SkewImage->Visible       = false;
         SkewEditBox->Visible     = false;
         HLabel->Visible          = false;
         HEditBox->Visible        = false;
         VLabel->Visible          = false;
         VEditBox->Visible        = false;
         XLabel->Visible          = false;
         XEditBox->Visible        = false;
         YLabel->Visible          = false;
         YEditBox->Enabled        = false;
         YEditBox->Visible        = false;

         // CLONE mode elements on

         CloneModeLabel->Visible              = true;
         SetClonePointSpeedButton->Enabled    = true;
         SetClonePointSpeedButton->Visible    = true;
         ClonePointOffsetSpeedButton->Enabled = true;
         ClonePointOffsetSpeedButton->Visible = true;
         ClonePointLockSpeedButton->Enabled   = true;
         ClonePointLockSpeedButton->Visible   = true;
         CloneXLabel->Visible                 = true;
         CloneXEditBox->Enabled               = true;
         CloneXEditBox->Visible               = true;
         CloneYLabel->Visible                 = true;
         CloneYEditBox->Enabled               = true;
         CloneYEditBox->Visible               = true;
         CloneDelXLabel->Visible              = true;
         CloneDelXEditBox->Enabled            = true;
         CloneDelXEditBox->Visible            = true;
         CloneDelYLabel->Visible              = true;
         CloneDelYEditBox->Enabled            = true;
         CloneDelYEditBox->Visible            = true;

         ReferenceFrameBox->Visible = true;
         ColorPickerBox->Visible = false;
         OriginalFrameBox->Visible = false;
         AltClipFrameBox->Visible = false;

         BrushGroupBox->Visible = true;

         ViewLabel->Caption = "Processed";

         UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
         UpdateGrainPresetButtons();

         // paint brush
         GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush],
                                                    brushMode,
                                                    0, // darker  threshold,
                                                    0, // lighter threshold,
                                                    0);
         // "Clone pixels" or "Erase pixels"
         UpdateCloneMessage(MAJORSTATE_CLONE_MODE_HOME);

         RotateStretchTimer->Enabled = true;

         // Maybe turn off "alt source clip import"
         SaveAndClearImportFromAltSourceClipState();

      break;

      case MAJORSTATE_COLOR_MODE_HOME:

         // Color Mode is enabled, down
         RevealButton->Enabled     = true;
         CloneButton->Enabled      = true;
         ColorButton->Down         = true;
         ColorButton->Enabled      = true;
         OrigValuesButton->Enabled = true;
         AlternateClipButton->Enabled = true;

         // Brush mode buttons
         ThruButton->Enabled  = true;
         EraseButton->Enabled = true;

         // Mask and Tracking buttons
         MaskButton->Enabled = true;
         MaskButton->Down = false;
         setMaskState(false);

         TrackingButton->Enabled = true;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = true;
         ResetDensityPanel->Enabled  = true;
         GrainSpeedButton->Enabled   = true;
         ResetGrainPanel->Enabled    = true;

//         ReferenceFrameBox->Visible = false;

         // need proxy inited for color mode
         SetProxy(NULL, NULL);

//         OriginalFrameBox->Visible = false;

         ColorPalettePaintBox->Enabled  = true;
         HueSaturationPaintBox->Enabled = true;
         ValueMeterPaintBox->Enabled    = true;

         // use these values to adjust the RGB edit box behavior
         imgFmt = GPaintTool->getSystemAPI()->getVideoClipImageFormat();
         componentCount = imgFmt->getComponentCountExcAlpha();
         maxComponentValue = imgFmt->getBitsPerComponent();
         maxComponentValue = ((int)1 << maxComponentValue) - 1;

         // and to adjust the bucket fill tolerance trackbar
         ToleranceTrackBar->Max = (maxComponentValue + 1) >> 3;

         if (componentCount == 1
         || imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_Y) {

            // monochrome
            RYLabel->Visible   = false;
            RYEditBox->Visible = false;

            GULabel->Caption   = "Lum";
            GULabel->Enabled   = true;
            GULabel->Visible   = true;
            GUEditBox->Enabled = true;
            GUEditBox->Visible = true;

            BVLabel->Visible   = false;
            BVEditBox->Visible = false;
         }
         else { // color

            RYLabel->Enabled   = true;
            RYLabel->Visible   = true;
            RYEditBox->Enabled = true;
            RYEditBox->Visible = true;

            GULabel->Caption   = "G";
            GULabel->Enabled   = true;
            GULabel->Visible   = true;
            GUEditBox->Enabled = true;
            GUEditBox->Visible = true;

            BVLabel->Enabled   = true;
            BVLabel->Visible   = true;
            BVEditBox->Enabled = true;
            BVEditBox->Visible = true;
         }

         DropperSpeedButton->Down = false;
         DropperSpeedButton->Enabled    = true;
         AvgSizeComboBox->Enabled       = true;
         BucketFillSpeedButton->Down    = false;
         BucketFillSpeedButton->Enabled = true;
         ToleranceTrackBar->Enabled     = true;

         ColorPickerBox->Visible = true;
         ReferenceFrameBox->Visible = false;
         OriginalFrameBox->Visible = false;
         AltClipFrameBox->Visible = false;

         BrushGroupBox->Visible = true;

         ///////////////////////////////////////////////////////////////////////

         // Processed / original
         ViewLabel->Caption = "Processed";

         UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
         UpdateGrainPresetButtons();

         // init this
         adjLevel = false;

         // paint brush only
         GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush],
               brushMode, 0, // darker  threshold,
               0, // lighter threshold,
               localRGB);

         // no clone brush
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         // "Paint pixels" or "Erase pixels"
         UpdateColorMessage(MAJORSTATE_COLOR_MODE_HOME);

         RotateStretchTimer->Enabled = true;

         // Maybe turn off "alt source clip import"
         SaveAndClearImportFromAltSourceClipState();

      break;

      case MAJORSTATE_ORIG_MODE_HOME:

         // Orig Values Mode is enabled, down
         RevealButton->Enabled = true;
         CloneButton->Enabled      = true;
         ColorButton->Enabled      = true;
         OrigValuesButton->Enabled = true;
         OrigValuesButton->Down    = true;
         AlternateClipButton->Enabled = true;

         // Brush mode buttons
         ThruButton->Enabled  = true;
         EraseButton->Enabled = true;

         // Mask and Tracking buttons
         MaskButton->Enabled = true;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = true;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Invisible while diddling controls
//         OriginalFrameBox->Visible = false;

         CompareSpeedButton->Enabled = true;
         CompareSpeedButton->Down    = false;
         OrigDiffSkinButton->Enabled = false;
         OrigMixSkinButton->Enabled  = false;
         OrigDiffSkinButton->Down = localOrigOnionSkinMode == ONIONSKIN_MODE_POSNEG;
         OrigMixSkinButton->Down = localOrigOnionSkinMode != ONIONSKIN_MODE_POSNEG;
         OrigMixingTrackBar->Enabled = false;
         OrigLabel->Enabled          = false;
         CurLabel->Enabled           = false;

         SetProxy(OrigProxyPaintBox, origProxyBitmap);
         OrigProxyPaintBox->Visible = true;

         OriginalFrameBox->Visible = true;
         ReferenceFrameBox->Visible = false;
         ColorPickerBox->Visible = false;
         AltClipFrameBox->Visible = false;

         BrushGroupBox->Visible = true;

         ViewLabel->Caption = "Processed";

         UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
         UpdateGrainPresetButtons();

         // paint brush only
         GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush],
                                                    brushMode,
                                                    0, // darker  threshold
                                                    0, // lighter threshold
                                                    0);

         if (brushMode == BRUSH_MODE_ORIG)
         {
            ExecStatusBar->SetStatusMessage("Paint original pixels");
         }
         else if (brushMode == BRUSH_MODE_ERASE)
         {
            ExecStatusBar->SetStatusMessage("Erase pixels");
         }

         GPaintTool->getSystemAPI()->unloadCloneBrush();

         RotateStretchTimer->Enabled = true;

         // Maybe turn off "alt source clip import"
         SaveAndClearImportFromAltSourceClipState();

      break;

      case MAJORSTATE_ALTCLIP_MODE_HOME:

         // Orig Values Mode is enabled, down
         RevealButton->Enabled        = true;
         CloneButton->Enabled         = true;
         ColorButton->Enabled         = true;
         OrigValuesButton->Enabled    = true;
         AlternateClipButton->Enabled = true;
         AlternateClipButton->Down    = true;

         // Brush mode buttons
         ThruButton->Enabled  = true;
         EraseButton->Enabled = true;

         // Mask and Tracking buttons
         MaskButton->Enabled = true;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = true;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Make invisible while diddling controls.
//         AltClipFrameBox->Visible = false;

         SetProxy(AltClipProxyPaintBox, proxyBitmap);
         AltClipProxyPaintBox->Visible = true;

         AltClipSelectClipButton->Enabled     = true;
         AltClipClearClipButton->Enabled      = true;
         AltClipCompareButton->Enabled        = true;
         AltClipCompareButton->Down           = false;
         AltClipDiffButton->Enabled           = false;
         AltClipMixButton->Enabled            = false;
         AltClipDiffButton->Down = localAltClipOnionSkinMode == ONIONSKIN_MODE_POSNEG;
         AltClipMixButton->Down = localAltClipOnionSkinMode != ONIONSKIN_MODE_POSNEG;
         AltClipSrcLabel->Enabled             = false;
         AltClipMixingTrackBar->Enabled       = false;
         AltClipTgtLabel->Enabled             = false;
         AltClipSyncFrameIndexButton->Enabled = true;
         AltClipSyncTimecodeButton->Enabled   = true;
         AltClipSyncMarkInButton->Enabled     = true;
         AltClipFrameOffsetEdit->Enabled      = true;
         AltClipFrameTimecodeEdit->Enabled    = true;

         AltClipFrameBox->Visible = true;
         ReferenceFrameBox->Visible = false;
         ColorPickerBox->Visible = false;
         OriginalFrameBox->Visible = false;

         BrushGroupBox->Visible = true;

         // Reset the onionskin mix
         UpdateAltClipOnionSkinTargetWgt(localAltClipOnionSkinTargetWgt);

         ViewLabel->Caption = "Processed";

         UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
         UpdateGrainPresetButtons();

         // paint brush only
         GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush],
                                                    brushMode,
                                                    0, // darker  threshold
                                                    0, // lighter threshold
                                                    0);

         if (brushMode == BRUSH_MODE_THRU)
         {
            ExecStatusBar->SetStatusMessage("Paint pixels");
         }
         else if (brushMode == BRUSH_MODE_ERASE)
         {
            ExecStatusBar->SetStatusMessage("Erase pixels");
         }
         else if (brushMode == BRUSH_MODE_ORIG)
         {
            ExecStatusBar->SetStatusMessage("Paint original pixels");  // Can't be this! QQQ
         }

         GPaintTool->getSystemAPI()->unloadCloneBrush();

         RotateStretchTimer->Enabled = true; // WTF?? : inManualMode;

         ////ReferenceFrameTimecodeEditChange(NULL);

         EnterAlternateClipSourceMode();

         break;

      case MAJORSTATE_EDIT_MASK:

         // Only the button that was pressed when we entered mask mode is enabled
         RevealButton->Enabled = RevealButton->Down;
         CloneButton->Enabled      = CloneButton->Down;
         ColorButton->Enabled      = ColorButton->Down;
         OrigValuesButton->Enabled = OrigValuesButton->Down;
         AlternateClipButton->Enabled = AlternateClipButton->Down;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         MaskButton->Enabled = true;
         MaskButton->Down    = true;
         setMaskState(true);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = false;
         ReferenceFrameOffsetButton->Enabled = false;
         QuickAlignButton->Enabled           = false;
         InvertButton->Enabled     = false;
         UpdateStateOfTrackAlignButton(true);   // NEVER false

         // Reveal mode elements
         PositionImportButton->Down    = false;
         PositionImportButton->Enabled = true;
         PositionImportButton->Caption = "Transform";
         ColorSkinButton->Enabled      = true;
         DiffSkinButton->Enabled       = true;
         MixingTrackBar->Enabled       = false;
         RefLabel->Enabled             = false;
         TgtLabel->Enabled             = false;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         // Clone mode elements
         CloneDelXEditBox->Enabled            = false;
         CloneDelYEditBox->Enabled            = false;
         CloneXEditBox->Enabled               = false;
         CloneYEditBox->Enabled               = false;
         SetClonePointSpeedButton->Enabled    = false;
         ClonePointOffsetSpeedButton->Enabled = false;
         ClonePointLockSpeedButton->Enabled   = false;

         // Color mode elements
         ColorPalettePaintBox->Enabled  = false;
         HueSaturationPaintBox->Enabled = false;
         ValueMeterPaintBox->Enabled    = false;

         RYEditBox->Enabled = false;
         GUEditBox->Enabled = false;
         BVEditBox->Enabled = false;

         DropperSpeedButton->Enabled    = false;
         BucketFillSpeedButton->Enabled = false;
         ToleranceTrackBar->Enabled     = false;
         AvgSizeComboBox->Enabled       = false;

         // AltClip elements
         AltClipSelectClipButton->Enabled     = false;
         AltClipClearClipButton->Enabled      = false;
         AltClipCompareButton->Enabled        = false;
         AltClipCompareButton->Down           = false;
         AltClipDiffButton->Enabled           = false;
         AltClipMixButton->Enabled            = false;
         AltClipSrcLabel->Enabled             = false;
         AltClipMixingTrackBar->Enabled       = false;
         AltClipTgtLabel->Enabled             = false;
         AltClipSyncFrameIndexButton->Enabled = false;
         AltClipSyncTimecodeButton->Enabled   = false;
         AltClipSyncMarkInButton->Enabled     = false;
         AltClipFrameOffsetEdit->Enabled      = false;
         AltClipFrameTimecodeEdit->Enabled    = false;

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         // Label1->Caption = "Edit mask";
         ExecStatusBar->SetStatusMessage("Edit mask");

         RotateStretchTimer->Enabled = false;

      break;

      case MAJORSTATE_POSITION_IMPORT_FRAME:

         // Modes are disabled
         RevealButton->Enabled = false;
         CloneButton->Enabled      = false;
         ColorButton->Enabled      = false;
         OrigValuesButton->Enabled = false;
         AlternateClipButton->Enabled = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         // Mask and Tracking buttons
         MaskButton->Enabled = false;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = false;
         ReferenceFrameOffsetButton->Enabled = false;
         QuickAlignButton->Down              = false;
         QuickAlignButton->Enabled           = true;
         InvertButton->Enabled               = true;
         UpdateStateOfTrackAlignButton(true);   // NEVER false

         PositionImportButton->Enabled = false;
         PositionImportButton->Caption = "Transform";
         MixingTrackBar->Enabled       = true;
         RefLabel->Enabled             = true;
         TgtLabel->Enabled             = true;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         UpdateOnionSkinTargetWgt(0.50);

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         //Label1->Caption = "Reposition reference frame";
         ExecStatusBar->SetStatusMessage("Reposition source frame");

      break;

		case MAJORSTATE_EDIT_TRANSFORM:

         // Modes are disabled
         RevealButton->Enabled = true;
         CloneButton->Enabled      = false;
         ColorButton->Enabled      = false;
         OrigValuesButton->Enabled = false;
         AlternateClipButton->Enabled = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         // Mask and Tracking
         MaskButton->Enabled = true;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = true;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = true;
         ReferenceFrameLockButton->Enabled   = true;
         ReferenceFrameOffsetButton->Enabled = true;

         QuickAlignButton->Down    = false;
         QuickAlignButton->Enabled = true;
         InvertButton->Enabled     = true;
         UpdateStateOfTrackAlignButton(true);

         PositionImportButton->Down    = true;
         PositionImportButton->Enabled = true;
         PositionImportButton->Caption = "Transform";
         MixingTrackBar->Enabled       = true;
         RefLabel->Enabled             = true;
         TgtLabel->Enabled             = true;

         RotationEditBox->Enabled = true;
         SkewEditBox->Enabled     = true;
         HEditBox->Enabled        = true;
         VEditBox->Enabled        = true;
         XEditBox->Enabled        = true;
         YEditBox->Enabled        = true;

         UpdateOnionSkinTargetWgt(0.50);

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         //Label1->Caption = "Transform reference frame";
         ExecStatusBar->SetStatusMessage("Transform source frame");

		 RotateStretchTimer->Enabled = true;

      break;

      case MAJORSTATE_CHOOSE_AUTOALIGN_PT :

         // Modes are disabled
         RevealButton->Enabled = true;
         CloneButton->Enabled      = false;
         ColorButton->Enabled      = false;
         OrigValuesButton->Enabled = false;
         AlternateClipButton->Enabled = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         // Mask and Tracking
         MaskButton->Enabled = true;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = true;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = true;
         ReferenceFrameLockButton->Enabled   = true;
         ReferenceFrameOffsetButton->Enabled = true;

         QuickAlignButton->Down    = true;
         QuickAlignButton->Enabled = true;
         InvertButton->Enabled     = true;
         GetOutOfTrackAlignMode();
         UpdateStateOfTrackAlignButton(true);

         PositionImportButton->Down    = true;
         PositionImportButton->Enabled = true;
         PositionImportButton->Caption = "Transform";
         MixingTrackBar->Enabled       = true;
         RefLabel->Enabled             = true;
         TgtLabel->Enabled             = true;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         UpdateOnionSkinTargetWgt(0.50);

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         //Label1->Caption = "Choose autoalign point";
         ExecStatusBar->SetStatusMessage("Choose autoalign point");

         RotateStretchTimer->Enabled = true;

      break;

      case MAJORSTATE_MASK_TRANSFORM :

         // Modes are disabled
         RevealButton->Enabled = RevealButton->Down;
         CloneButton->Enabled      = CloneButton->Down;
         ColorButton->Enabled      = ColorButton->Down;
         OrigValuesButton->Enabled = OrigValuesButton->Down;
         AlternateClipButton->Enabled = AlternateClipButton->Down;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         MaskButton->Enabled = true;
         MaskButton->Down    = true;
         setMaskState(true);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = false;
         ReferenceFrameOffsetButton->Enabled = false;
         QuickAlignButton->Enabled           = false;
         InvertButton->Enabled     = false;
         UpdateStateOfTrackAlignButton(true);   // NEVER false

         // Transform Panel
         PositionImportButton->Enabled = true;
         PositionImportButton->Caption = "Transform";
         PositionImportButton->Down    = true;
         DiffSkinButton->Enabled       = true;
         ColorSkinButton->Enabled      = true;
         MixingTrackBar->Enabled       = true;
         RefLabel->Enabled             = true;
         TgtLabel->Enabled             = true;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         RotateStretchTimer->Enabled = false;

      break;

		case MAJORSTATE_EDIT_TRACKING :
		case MAJORSTATE_TRACKING_DONE :
		case MAJORSTATE_TRACKING_PAUSED :

         // Mode buttons are disabled
         RevealButton->Enabled = false;
         CloneButton->Enabled      = false;
         ColorButton->Enabled      = false;
         OrigValuesButton->Enabled = false;
         AlternateClipButton->Enabled = false;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         MaskButton->Enabled = false;
         MaskButton->Down    = false;
         setMaskState(true);

         TrackingButton->Enabled = true;
         TrackingButton->Down    = true;
         setTrackingState(true);

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = false;
         ReferenceFrameOffsetButton->Enabled = false;
         QuickAlignButton->Enabled = false;
         InvertButton->Enabled     = false;
         UpdateStateOfTrackAlignButton(true);   // NEVER false

         // Transform Panel
         PositionImportButton->Enabled = false;
         PositionImportButton->Caption = "Transform";
         PositionImportButton->Down    = false;
         DiffSkinButton->Enabled       = false;
         ColorSkinButton->Enabled      = false;
         MixingTrackBar->Enabled       = false;
         RefLabel->Enabled             = false;
         TgtLabel->Enabled             = false;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         RotateStretchTimer->Enabled = false;

      break;

		case MAJORSTATE_TRACKING_IN_PROGRESS:

         MaskButton->Enabled = false;
         MaskButton->Down = false;
         setMaskState(false);

			TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         // setTrackingState(false);    Already set - leave it be

         // Modes are disabled
         RevealButton->Enabled     = false;
         CloneButton->Enabled      = false;
         ColorButton->Enabled      = false;
         OrigValuesButton->Enabled = false;
         AlternateClipButton->Enabled = false;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = false;
         ReferenceFrameOffsetButton->Enabled = false;
         QuickAlignButton->Down    = false;
         QuickAlignButton->Enabled = false;
         InvertButton->Enabled     = false;
         TrackAlignButton->Down    = false;
         UpdateStateOfTrackAlignButton(false);  // reenabled after tracking

         PositionImportButton->Enabled = false;
         PositionImportButton->Caption = "Transform";
         PositionImportButton->Down    = false;
         MixingTrackBar->Enabled       = true; // don't care...
         RefLabel->Enabled             = true;
         TgtLabel->Enabled             = true;

         // Just leave these alone
         // XEditBox->Enabled = true;
         // YEditBox->Enabled = true;

         // AltClip elements
         AltClipSelectClipButton->Enabled     = false;
         AltClipClearClipButton->Enabled      = false;
         AltClipCompareButton->Enabled        = false;
         AltClipCompareButton->Down           = false;
         AltClipDiffButton->Enabled           = false;
         AltClipMixButton->Enabled            = false;
         AltClipSrcLabel->Enabled             = false;
         AltClipMixingTrackBar->Enabled       = false;
         AltClipTgtLabel->Enabled             = false;
         AltClipSyncFrameIndexButton->Enabled = false;
         AltClipSyncTimecodeButton->Enabled   = false;
         AltClipSyncMarkInButton->Enabled     = false;
         AltClipFrameOffsetEdit->Enabled      = false;
         AltClipFrameTimecodeEdit->Enabled    = false;

         UpdateOnionSkinTargetWgt(0.50);

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         // Label1->Caption = "Show Reference and Target Frames";
         ExecStatusBar->SetStatusMessage("Show Reference and Target Frames");

      break;

      case MAJORSTATE_GET_ORIGINAL_VALUES:

         // Modes are disabled
         RevealButton->Enabled        = RevealButton->Down;
         CloneButton->Enabled         = CloneButton->Down;
         ColorButton->Enabled         = ColorButton->Down;
         OrigValuesButton->Enabled    = OrigValuesButton->Down;
         AlternateClipButton->Enabled = AlternateClipButton->Down;

         // Density and Grain speed buttons
         DensitySpeedButton->Enabled = false;
         ResetDensityPanel->Enabled  = false;
         GrainSpeedButton->Enabled   = false;
         ResetGrainPanel->Enabled    = false;

         // Brush mode buttons
         ThruButton->Enabled  = false;
         EraseButton->Enabled = false;

         MaskButton->Enabled = false;
         MaskButton->Down    = false;
         setMaskState(false);

         TrackingButton->Enabled = false;
         TrackingButton->Down    = false;
         setTrackingState(false);

         // Reference Frame panel
         ReferenceFrameTimecodeEdit->Enabled = false;
         ReferenceFrameTimecodeEditChange(NULL);
         ReferenceFrameOffsetEdit->Enabled   = false;
         ReferenceFrameLockButton->Enabled   = ReferenceFrameLockButton->Down;
         ReferenceFrameOffsetButton->Enabled = ReferenceFrameOffsetButton->Down;
         QuickAlignButton->Down    = false;
         QuickAlignButton->Enabled = false;
         InvertButton->Enabled     = false;
         TrackAlignButton->Down = false;
         UpdateStateOfTrackAlignButton(true);   // NEVER false

         PositionImportButton->Enabled = false;
         PositionImportButton->Caption = "Transform";
         PositionImportButton->Down    = false;
         MixingTrackBar->Enabled       = false;
         RefLabel->Enabled             = false;
         TgtLabel->Enabled             = false;

         RotationEditBox->Enabled = false;
         SkewEditBox->Enabled     = false;
         HEditBox->Enabled        = false;
         VEditBox->Enabled        = false;
         XEditBox->Enabled        = false;
         YEditBox->Enabled        = false;

         UpdateOnionSkinTargetWgt(0.50);

         // Accept / reject / undo / macros
         UpdateStateOfAcceptRejectUndoAndMacroButtons(false);
         UpdateGrainPresetButtons();

         GPaintTool->getSystemAPI()->unloadPaintBrush();
         GPaintTool->getSystemAPI()->unloadCloneBrush();

         // Label1->Caption = "Get original values";
         ExecStatusBar->SetStatusMessage("Get original values");

      break;
   }
}

//---------------------------------------------------------------------------
//###################  Set GUI AlignState ###################################
//---------------------------------------------------------------------------
void TPaintToolForm::setAlignState(int alignstate)
{
   if (GPaintTool->getMajorState() != MAJORSTATE_REVEAL_MODE_HOME)
      return;

   bool inTrackMode = (alignstate == ALIGNSTATE_TRACK_SHOWING_TARGET) ||
         (alignstate == ALIGNSTATE_TRACK_SHOWING_BOTH);
   bool inManualMode  = !inTrackMode;
   bool showingBoth   = (alignstate == ALIGNSTATE_TRACK_SHOWING_BOTH);
   bool showingTarget = !showingBoth;

   MaskButton->Enabled = true;
   MaskButton->Down    = false;
   setMaskState(false);

   TrackingButton->Enabled = !showingBoth;
   TrackingButton->Down    = false;
	setTrackingState(false);

   // Reveal Mode is enabled, down
   RevealButton->Down        = true;
   RevealButton->Enabled     = true;
   CloneButton->Enabled      = showingTarget;
   ColorButton->Enabled      = showingTarget;
   OrigValuesButton->Enabled = showingTarget;
   AlternateClipButton->Enabled = showingTarget;

   // Density and Grain speed buttons
   DensitySpeedButton->Enabled = showingTarget;
   ResetDensityPanel->Enabled  = showingTarget;
   GrainSpeedButton->Enabled   = showingTarget;
   ResetGrainPanel->Enabled    = showingTarget;

   // Brush mode buttons
   ThruButton->Enabled  = showingTarget;
   EraseButton->Enabled = showingTarget;

   ColorPickerBox->Visible = false;

   OriginalFrameBox->Visible = false;

   // ReferenceFrameBox

//   ReferenceFrameBox->Visible = false;

   SetProxy(ProxyPaintBox, proxyBitmap);
   ProxyPaintBox->Visible              = true;
   ReferenceFrameTimecodeEdit->Enabled = true;
   ReferenceFrameTimecodeEdit->Visible = true;
   ReferenceFrameTimecodeEditChange(NULL);
   ReferenceFrameLockButton->Enabled   = true;
   ReferenceFrameLockButton->Visible   = true;
   ReferenceFrameOffsetButton->Enabled = true;
   ReferenceFrameOffsetButton->Visible = true;
   ReferenceFrameOffsetEdit->Enabled   = true;
   ReferenceFrameOffsetEdit->Visible   = true;

   // CLONE mode elements off

   CloneModeLabel->Visible              = false;
   SetClonePointSpeedButton->Visible    = false;
   ClonePointOffsetSpeedButton->Visible = false;
   ClonePointLockSpeedButton->Visible   = false;
   CloneXLabel->Visible                 = false;
   CloneXEditBox->Visible               = false;
   CloneYLabel->Visible                 = false;
   CloneYEditBox->Visible               = false;
   CloneDelXLabel->Visible              = false;
   CloneDelXEditBox->Visible            = false;
   CloneDelYLabel->Visible              = false;
   CloneDelYEditBox->Visible            = false;

   // REVEAL mode elements on

   QuickAlignButton->Down    = false;
   QuickAlignButton->Enabled = true;
   QuickAlignButton->Visible = true;

   if (!InhibitTrackAlignButtonClick) // Awful HACK
            TrackAlignButton->Down = inTrackMode;
   UpdateStateOfTrackAlignButton(true);
   TrackAlignButton->Visible = true;
   TrackAlignButton->Enabled = true;

   InvertButton->Enabled = inManualMode;
   InvertButton->Visible = true;

   PositionImportButton->Down    = showingBoth;
   PositionImportButton->Caption = AnsiString(inManualMode ? "Transform" : "Compare");
   PositionImportButton->Enabled = true;
   PositionImportButton->Visible = true;
   DiffSkinButton->Enabled       = true;
   DiffSkinButton->Visible       = true;
   ColorSkinButton->Enabled      = true;
   ColorSkinButton->Visible      = true;
   MixingTrackBar->Enabled       = showingBoth;
   RefLabel->Enabled             = MixingTrackBar->Enabled;
   TgtLabel->Enabled             = MixingTrackBar->Enabled;
   MixingTrackBarPanel->Visible  = true;
   MixingTrackBarPanel->Enabled = true;
   RefLabel->Visible             = true;
   TgtLabel->Visible             = true;

   RotationEditBox->Enabled = true;
   SkewEditBox->Enabled     = true;
   HEditBox->Enabled        = true;
   VEditBox->Enabled        = true;
   XEditBox->Enabled        = true;
   YEditBox->Enabled        = true;

   RotationImage->Enabled   = true;
   SkewImage->Enabled       = true;
   HLabel->Enabled          = true;
   VLabel->Enabled          = true;
   XLabel->Enabled          = true;
   YLabel->Enabled          = true;

   // I prefer to set ReadOnly to true instead of Enabled to false
   // because I want the numbers to be black, not greyed out
   TColor transformColor = inTrackMode ? clBtnFace : clWindow;

   RotationEditBox->ReadOnly = inTrackMode;
   RotationEditBox->Color    = transformColor;
   SkewEditBox->ReadOnly     = inTrackMode;
   SkewEditBox->Color        = transformColor;
   HEditBox->ReadOnly        = inTrackMode;
   HEditBox->Color           = transformColor;
   VEditBox->ReadOnly        = inTrackMode;
   VEditBox->Color           = transformColor;
   XEditBox->ReadOnly        = inTrackMode;
   XEditBox->Color           = transformColor;
   YEditBox->ReadOnly        = inTrackMode;
   YEditBox->Color           = transformColor;

   RotationImage->Visible   = true;
   RotationEditBox->Visible = true;
   SkewImage->Visible       = true;
   SkewEditBox->Visible     = true;
   HLabel->Visible          = true;
   HEditBox->Visible        = true;
   VLabel->Visible          = true;
   VEditBox->Visible        = true;
   XLabel->Visible          = true;
   XEditBox->Visible        = true;
   YLabel->Visible          = true;
   YEditBox->Visible        = true;

   ReferenceFrameBox->Caption = " Source Frame ";
   ReferenceFrameBox->Visible = true;

   ReferenceFrameBox->Visible = true;
   ColorPickerBox->Visible = false;
   OriginalFrameBox->Visible = false;
   AltClipFrameBox->Visible = false;

   ColorPickerBox->Visible = false;

   BrushGroupBox->Visible = true;

   // Reset the onionskin mix
   UpdateOnionSkinTargetWgt(0.50);

   ViewLabel->Caption = "Processed";

   UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
   UpdateGrainPresetButtons();

   if (showingTarget) {

      // paint brush only
      GPaintTool->getSystemAPI()->loadPaintBrush(brush[currentBrush], brushMode,
            0, // darker  threshold
            0, // lighter threshold
            0);

      if (brushMode == BRUSH_MODE_THRU) {
         // Label1->Caption = "Paint  pixels" ;
         ExecStatusBar->SetStatusMessage("Paint pixels");
      }
      else if (brushMode == BRUSH_MODE_ERASE) {
         // Label1->Caption = "Erase pixels";
         ExecStatusBar->SetStatusMessage("Erase pixels");
      }
      else if (brushMode == BRUSH_MODE_ORIG) {
         // Label1->Caption = "Paint original pixels";
         ExecStatusBar->SetStatusMessage("Paint original pixels");
      }
   }
   else {

      GPaintTool->getSystemAPI()->unloadPaintBrush();

      // Label1->Caption = "Show Reference and Target Frames";
      ExecStatusBar->SetStatusMessage("Show Reference and Target Frames");
   }
   GPaintTool->getSystemAPI()->unloadCloneBrush();

   RotateStretchTimer->Enabled = true; // WTF?? : inManualMode;

   ReferenceFrameTimecodeEditChange(NULL);
}

//---------------------------------------------------------------------------
//###################  Position Import Frame ################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::PositionImportButtonClick(TObject *Sender)
{
   // Handle the "Transform" button click
   GPaintTool->PositionImportFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AcceptTransformSpeedButtonClick(
      TObject *Sender)
{
   // return to reveal mode & keep transform
   GPaintTool->LeaveTransformKeep();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RejectTransformSpeedButtonClick(
      TObject *Sender)
{
   // return to reveal mode & revert to orig transform
   GPaintTool->LeaveTransformRevert();

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Color Mode ####################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ColorSkinButtonClick(TObject *Sender)
{
   localOnionSkinMode = ONIONSKIN_MODE_COLOR;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_COLOR, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------
//###################  Select PosNeg Mode ###################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::DiffSkinButtonClick(TObject *Sender)
{
   localOnionSkinMode = ONIONSKIN_MODE_POSNEG;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_POSNEG, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//#########################  Edit Mask ######################################
//---------------------------------------------------------------------------
 void __fastcall TPaintToolForm::MaskButtonClick(TObject *Sender)
{
   int majorstate = GPaintTool->getMajorState();

   if (majorstate == MAJORSTATE_EDIT_MASK) {

      GPaintTool->LeaveMaskRevert();
   }
   else if ((majorstate == MAJORSTATE_REVEAL_MODE_HOME)||
            (majorstate == MAJORSTATE_CLONE_MODE_HOME)||
            (majorstate == MAJORSTATE_COLOR_MODE_HOME)||
            (majorstate == MAJORSTATE_ORIG_MODE_HOME)
            || majorstate == MAJORSTATE_ALTCLIP_MODE_HOME) {

      GPaintTool->SelectMask();
   }
   else if (majorstate == MAJORSTATE_EDIT_TRANSFORM) {

      GPaintTool->SelectMaskTransform();
   }
   else if (majorstate == MAJORSTATE_MASK_TRANSFORM) {

      GPaintTool->SelectTransform();
   }

   GPaintTool->getSystemAPI()->ExecuteMaskToolCommand(NAV_CMD_MASK_TOOL_ENABLE);
   GPaintTool->getSystemAPI()->EnableMaskOutlineDisplay(true);
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//#########################  Tracking  ######################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::TrackingButtonClick(TObject *Sender)
{
	// HACK to cut off recursion
	if (InhibitTrackingButtonClick)
	{
		return;
	}

	InhibitTrackingButtonClick = true;

	int majorstate = GPaintTool->getMajorState();

	if (majorstate == MAJORSTATE_EDIT_TRACKING
	|| majorstate == MAJORSTATE_TRACKING_DONE
	|| majorstate == MAJORSTATE_TRACKING_PAUSED)
	{

		GPaintTool->LeaveEditTrackingState();
	}
	else if (majorstate == MAJORSTATE_REVEAL_MODE_HOME
	|| majorstate == MAJORSTATE_CLONE_MODE_HOME
	|| majorstate == MAJORSTATE_COLOR_MODE_HOME
	|| majorstate == MAJORSTATE_ORIG_MODE_HOME)
	{
		GPaintTool->EnterEditTrackingState();
		GPaintTool->goToTrackingPointInsertionFrame();
	}

	// set focus to main window
	LoseTheFocus();

	InhibitTrackingButtonClick = false;
}

//---------------------------------------------------------------------------
//##########################  Paint #########################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RevealButtonClick(TObject *Sender)
{
   // implemented in the tool
   GPaintTool->SelectRevealMode();

   // set focus to main window
   //LoseTheFocus();
}

void __fastcall TPaintToolForm::CloneButtonClick(TObject *Sender)
{
   // implemented in the tool
   GPaintTool->SelectCloneMode();

   // set focus to main window
   LoseTheFocus();
}

void __fastcall TPaintToolForm::ColorButtonClick(TObject *Sender)
{
   // implemented in the tool
   GPaintTool->SelectColorMode();

   // set focus to main window
   LoseTheFocus();
}

void __fastcall TPaintToolForm::OrigValuesButtonClick(TObject *Sender)
{
   // implemented in the tool
   GPaintTool->SelectOrigValuesMode();

   // set focus to main window
   LoseTheFocus();
}

void __fastcall TPaintToolForm::AlternateClipButtonClick(TObject *Sender)
{
   // implemented in the tool
   GPaintTool->SelectAltClipMode();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Thru Mode ###################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ThruButtonClick(TObject *Sender)
{
   GPaintTool->SelectThru();

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Select Erase Mode ####################################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::EraseButtonClick(TObject *Sender)
{
   GPaintTool->SelectErase();

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Accept Processed Target Frame ########################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AcceptButtonClick(TObject *Sender)
{
   // mbraca added this to prevent zombie strokes appearing after using
   // the Accept button to acept pending strokes (as opposed to using G key
   // or autoaccepting) - I don't know why this isn't done
   // in AcceptProcessedTargetFrame()!!!
   //
   // pending macro becomes previous
   GPaintTool->RenamePendingAsPrevious();
   //

   // accept the frame - notify user if write failed
   if (GPaintTool->AcceptProcessedTargetFrame(true) != 0) {

      _MTIErrorDialog(Handle, "Error writing media   ");
   }

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
//###################  Reject Processed Target Frame ########################
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RejectButtonClick(TObject *Sender)
{
   GPaintTool->RejectProcessedTargetFrame();

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
bool TPaintToolForm::formKeyDown(WORD &Key, TShiftState Shift)
{
   int delta = 0;
   float fct = 0;
   int rdel;

   if (Key == MTK_UP) {
      delta = +1;
      rdel = +1;
      fct = 1.1;
   }
   if (Key == MTK_DOWN) {
      delta = -1;
      rdel = -1;
      fct = 1/1.1;
   }
   if (Key == MTK_LEFT) {
      delta = -1;
      rdel = -1;
      fct = 1/1.1;
   }
   if (Key == MTK_RIGHT) {
      delta = +1;
      rdel = +1;
      fct = 1.1;
   }

   if (delta != 0) {

      int keycod = 0;
      if (Shift.Contains(ssShift)) keycod |= 4;
      if (Shift.Contains(ssCtrl))  keycod |= 2;
      if (Shift.Contains(ssAlt))   keycod |= 1;

      switch(keycod) {

         case 4: // shift
            delta *= 10;
            fct = 1.0;
            rdel *= 10;
            Key = 0;
         break;

         case 2: // ctrl
            delta *=  1;
            fct = 1.0;
            rdel *= 1;
            Key = 0;;
         break;

         case 0: // none
            delta *=  5;
            rdel = 0;
            Key = 0;
         break;

         default:
            delta = 0;
            fct = 1.0;
            rdel = 0;
            Key = 0;
         break;

      }

      if (MixingTrackBar->Focused()) {
         MixingTrackBar->Position    += delta;
         return true;
      }
      if (ToleranceTrackBar->Focused()) {
         ToleranceTrackBar->Position += delta;
         return true;
      }
      if (RadiusTrackBar->Focused()) {
         //RadiusTrackBar->Position    += delta;
         ChangeRadius(fct, rdel);
         return true;
      }
      if (BlendTrackBar->Focused()) {
         //BlendTrackBar->Position     += delta;
         ChangeBlend(delta);
         return true;
      }
      if (OpacityTrackBar->Focused()) {
         //OpacityTrackBar->Position   += delta;
         ChangeOpacity(delta);
         return true;
      }
      if (AspectTrackBar->Focused()) {
         //AspectTrackBar->Position    += delta;
         ChangeAspect(delta);
         return true;
      }
      if (AngleTrackBar->Focused()) {
         //AngleTrackBar->Position     += delta;
         ChangeAngle(delta);
         return true;
      }

      // xyzzy Orig & AltClip mix track bars?

      // not handled
      return false;
   }

   // these are all handled
   if (RotationEditBox->Focused()) return true;
   if (HEditBox->Focused()) return true;
   if (VEditBox->Focused()) return true;
   if (SkewEditBox->Focused()) return true;
   if (XEditBox->Focused()) return true;
   if (YEditBox->Focused()) return true;
   if (ReferenceFrameOffsetEdit->Focused()) return true;
   if (ReferenceFrameTimecodeEdit->Focused()) return true;
   if (CloneXEditBox->Focused()) return true;
   if (CloneYEditBox->Focused()) return true;
   if (CloneDelXEditBox->Focused()) return true;
   if (CloneDelYEditBox->Focused()) return true;
   if (RYEditBox->Focused()) return true;
   if (GUEditBox->Focused()) return true;
   if (BVEditBox->Focused()) return true;

   // not handled
   return false;
}

//--------------------------------------------------------------------------
bool TPaintToolForm::formKeyUp(WORD &Key, TShiftState Shift)
{
   if (RotationEditBox->Focused()) return true;
   if (HEditBox->Focused()) return true;
   if (VEditBox->Focused()) return true;
   if (SkewEditBox->Focused()) return true;
   if (XEditBox->Focused()) return true;
   if (YEditBox->Focused()) return true;
   if (ReferenceFrameOffsetEdit->Focused()) return true;
   if (ReferenceFrameTimecodeEdit->Focused()) return true;
   if (CloneXEditBox->Focused()) return true;
   if (CloneYEditBox->Focused()) return true;
   if (CloneDelXEditBox->Focused()) return true;
   if (CloneDelYEditBox->Focused()) return true;
   if (ReferenceFrameTimecodeEdit->Focused()) return true;
   if (RYEditBox->Focused()) return true;
   if (GUEditBox->Focused()) return true;
   if (BVEditBox->Focused()) return true;

   return false;  // not handled
}

//---------------------------------------------------------------------------

void TPaintToolForm::formMouseMove(TShiftState Shift, int X, int Y)
{
   if (!Active) {

      //SetFocus(); // KKKKK
   }
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AlwaysOnTopMenuItemClick(TObject *Sender)
{
   if (FormStyle == Vcl::Forms::fsNormal) {
      FormStyle = Vcl::Forms::fsStayOnTop;
   }
   else {
	  FormStyle = Vcl::Forms::fsNormal;
   }
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::PaintPopupMenuPopup(TObject *Sender)
{
   //AlwaysOnTopMenuItem->Checked = (FormStyle == fsStayOnTop);
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::MaskToolButtonClick(TObject *Sender)
{
   TSpeedButton *spbutn = dynamic_cast<TSpeedButton*>(Sender);
   GPaintTool->getSystemAPI()->ExecuteMaskToolCommand(spbutn->Tag);
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::InvDelClrButtonClick(TObject *Sender)
{
   TSpeedButton *spbutn = dynamic_cast<TSpeedButton*>(Sender);
   GPaintTool->getSystemAPI()->ExecuteMaskToolCommand(spbutn->Tag);
   spbutn->Down = false;
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::MaskBorderMenuItemClick(TObject *Sender)
{
   //KT*****BorderButton->Down = false;
}

#define PI 3.1415927
#define ONEDEG (PI/180.)
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::RotateStretchTimerTimer(TObject *Sender)
{
   // simple counter
   mainTime++;

   int majorState = GPaintTool->getMajorState();
   bool rangeMacroExecuting = GPaintTool->IsRangeMacroExecuting();

   if  (!rangeMacroExecuting&&
       ((majorState == MAJORSTATE_PAINT_PIXELS)||
        (majorState == MAJORSTATE_CLONE_PIXELS)||
        (majorState == MAJORSTATE_COLOR_PIXELS)||
        (majorState == MAJORSTATE_PAINT_ORIG_PIXELS)||
        (majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS))) {

      // refreshes the brushstroke as it's drawn
      // WAS GPaintTool->getSystemAPI()->refreshFrameCached(true);
      GPaintTool->getSystemAPI()->displayFrameAndBrush();
   }
   else if ((majorState == MAJORSTATE_EDIT_TRANSFORM)||
            (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)) {

      // this will smooth out the widget motion
      GPaintTool->getSystemAPI()->TransformTimer();
   }

   // if import frame has been constant for 15
   // ticks, then load it
   if (loadTimer < 0x7fffffff)
   {
      loadTimer++;
   }

   if (loadTimer == IMPORT_FRAME_LOAD_THRESHOLD) {

      if (majorState == MAJORSTATE_REVEAL_MODE_HOME ||
          majorState == MAJORSTATE_CLONE_MODE_HOME  ||
          majorState == MAJORSTATE_COLOR_MODE_HOME  ||
          majorState == MAJORSTATE_ALTCLIP_MODE_HOME) {

          // load import frame from media if it's not already there
          ClearLoadTimer();
          GPaintTool->getSystemAPI()->forceTargetAndImportFrameLoaded();
      }
   }

   // if frame offset trackbar has been
   // constant for 20 ticks, load it
   if (densityTimer < 0x7fffffff)
   {
      densityTimer++;
   }

   if (densityTimer == DENSITY_LOAD_THRESHOLD) {

      if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
          (majorState == MAJORSTATE_CLONE_MODE_HOME)||
          (majorState == MAJORSTATE_COLOR_MODE_HOME)) {

         InhibitGuiUpdateHackFlag = true;

         // set new density and recycle last strokes
         GPaintTool->RecycleCurrentStrokes();

         InhibitGuiUpdateHackFlag = false;
      }
   }

   // if tolerance trackbar has been
   // constant for 20 ticks, load it
   toleranceTimer++;
   if (toleranceTimer == 0x7fffffff)
      toleranceTimer = TRACKBAR_LOAD_THRESHOLD + 1;

   if (toleranceTimer == TRACKBAR_LOAD_THRESHOLD) {

      if (majorState == MAJORSTATE_PICK_SEED) { // important

         // upload the tolerance to the Displayer
         GPaintTool->getSystemAPI()->setFillTolerance(GetFillTolerance());

         // and recycle the last fill seed
         GPaintTool->getSystemAPI()->recycleFillSeed();
      }
   }

}

//---------------------------------------------------------------------------
void TPaintToolForm::UpdateImportFrameMode(int impmod)
{
   // mode specific save
   GPaintTool->SetImportFrameMode(impmod);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   if (impmod == IMPORT_MODE_ABSOLUTE) {

      ReferenceFrameLockButton->Down = true;
   }
   else if (impmod == IMPORT_MODE_RELATIVE) {

      ReferenceFrameOffsetButton->Down = true;
   }
}

void TPaintToolForm::UpdateImportFrameTimecode(int impfrm)
{
   // mode specific save
   GPaintTool->SetImportFrameTimecode(impfrm);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag)
   {
      return;
   }

   localImportFrame = impfrm;

   CVideoFrameList *frameList;
   frameList = GPaintTool->getSystemAPI()->getImportClipVideoFrameList();
   if (frameList == NULL)
   {
      return;
   }

   CTimecode imptim = frameList->getTimecodeForFrameIndex(impfrm);

   char impstr[24];
   impstr[0] = ' ';
   impstr[1] = ' ';
   impstr[2] = ' ';
   impstr[3] = ' ';
   impstr[4] = ' ';
   impstr[5] = ' ';
   imptim.getTimeASCII(impstr+6);

   ReferenceFrameTimecodeEdit->Text = impstr;
   ReferenceFrameTimecodeEditChange(NULL);
   ReferenceFrameTimecodeEdit->Repaint();

   AltClipFrameTimecodeEdit->Text = impstr;
   AltClipFrameTimecodeEditChange(nullptr);
   AltClipFrameTimecodeEdit->Repaint();

   char *tmcd = impstr;
   while (*tmcd == ' ') tmcd++;
   OrigTimecodeLabel->Caption = tmcd;
}

void TPaintToolForm::UpdateTargetFrameTimecode(int trgfrm)
{
   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   char str[16];
   sprintf(str, "LOADED TARGET FRAME = %5d", trgfrm+1);
   //Label1->Caption = str;
   ExecStatusBar->SetStatusMessage(str);

   //Label1->Repaint();
}

void TPaintToolForm::UpdateImportFrameOffset(int frmoff)
{
   // mode specific save
   GPaintTool->SetImportFrameOffset(frmoff);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localImportFrameOffset = frmoff;

   char str[16];
   sprintf(str, "             %+2d", frmoff);

   if (ReferenceFrameOffsetEdit->Text != str) {

      ReferenceFrameOffsetEdit->Text = str;
      ReferenceFrameOffsetEdit->Repaint();
   }

   if (AltClipFrameOffsetEdit->Text != str) {

      AltClipFrameOffsetEdit->Text = str;
      AltClipFrameOffsetEdit->Repaint();
   }
}

void TPaintToolForm::UpdateOnionSkinTargetWgt(double tgtwgt)
{
   GPaintTool->getSystemAPI()->setOnionSkinTargetWgt(tgtwgt);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag)
      return;

   localOnionSkinTargetWgt = tgtwgt;

   // trackbar reflects current mix
   MixingTrackBar->OnChange = NULL;
   MixingTrackBar->Position = localOnionSkinTargetWgt *
                                 (MixingTrackBar->Max - MixingTrackBar->Min);
   MixingTrackBar->OnChange = MixingTrackBarChange;
}

void TPaintToolForm::UpdateOrigOnionSkinTargetWgt(double tgtwgt)
{
   GPaintTool->getSystemAPI()->setOnionSkinTargetWgt(tgtwgt);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag)
      return;

   localOrigOnionSkinTargetWgt = tgtwgt;

   // trackbar reflects current mix
   OrigMixingTrackBar->OnChange = NULL;
   OrigMixingTrackBar->Position = localOrigOnionSkinTargetWgt *
                                 (OrigMixingTrackBar->Max - OrigMixingTrackBar->Min);
   OrigMixingTrackBar->OnChange = OrigMixingTrackBarChange;
}

void TPaintToolForm::UpdateAltClipOnionSkinTargetWgt(double tgtwgt)
{
   GPaintTool->getSystemAPI()->setOnionSkinTargetWgt(tgtwgt);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag)
      return;

   localAltClipOnionSkinTargetWgt = tgtwgt;

   // trackbar reflects current mix
   AltClipMixingTrackBar->OnChange = NULL;
   AltClipMixingTrackBar->Position = localAltClipOnionSkinTargetWgt *
                                 (AltClipMixingTrackBar->Max - AltClipMixingTrackBar->Min);
   AltClipMixingTrackBar->OnChange = AltClipMixingTrackBarChange;
}

void TPaintToolForm::UpdateRotation(double rot)
{
   while (rot <  0  ) rot += 2*PI;
   while (rot > 2*PI) rot -= 2*PI;
   if (rot > PI) rot -= 2*PI;

   // mode specific save
   GPaintTool->SetRotation(rot);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localRotation = rot;

   char str[16];
   sprintf(str, "%+7.2f", rot*180/PI);
   RotationEditBox->Text = str;

   RotationEditBox->Repaint();
}

void TPaintToolForm::UpdateSkew(double skw)
{
   // mode specific save
   GPaintTool->SetSkew(skw);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localSkew = skw;

   char str[16];
   sprintf(str, "%7.3f", skw);
   SkewEditBox->Text = str;

   SkewEditBox->Repaint();
}

void TPaintToolForm::UpdateHStretch(double hstr)
{
   // mode specific save
   GPaintTool->SetStretchX(hstr);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localStretchX = hstr;

   char str[16];
   sprintf(str, "%7.2f", hstr*100.);
   HEditBox->Text = str;

   HEditBox->Repaint();
}

void TPaintToolForm::UpdateVStretch(double vstr)
{
   // mode specific save
   GPaintTool->SetStretchY(vstr);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localStretchY = vstr;

   char str[16];
   sprintf(str, "%7.2f", vstr*100.);
   VEditBox->Text = str;

   VEditBox->Repaint();
}

void TPaintToolForm::UpdateXOffset(double xoffs)
{
   // mode specific save
   GPaintTool->SetOffsetX(xoffs);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localOffsetX = xoffs;

   char str[16];
   sprintf(str, "%+6.1f", xoffs);
   XEditBox->Text = str;

   XEditBox->Repaint();
}

void TPaintToolForm::UpdateYOffset(double yoffs)
{
   // mode specific save
   GPaintTool->SetOffsetY(yoffs);

   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localOffsetY = yoffs;

   char str[16];
   sprintf(str, "%+6.1f", yoffs);
   YEditBox->Text = str;

   YEditBox->Repaint();
}

void TPaintToolForm::UpdateTransform(double rot,
                                     double hstr, double vstr,
                                     double skew,
                                     double xoff, double yoff)
{
   UpdateRotation(rot);
   UpdateSkew(skew);
   UpdateHStretch(hstr);
   UpdateVStretch(vstr);
   UpdateXOffset(xoff);
   UpdateYOffset(yoff);
}

void TPaintToolForm::UpdateCloneX(int clx)
{
   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localCloneX = clx;

   char str[16];
   sprintf(str, "%4d", clx);
   CloneXEditBox->Text = str;

   CloneXEditBox->Repaint();
}

void TPaintToolForm::UpdateCloneY(int cly)
{
   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localCloneY = cly;

   char str[16];
   sprintf(str, "%4d", cly);
   CloneYEditBox->Text = str;

   CloneYEditBox->Repaint();
}

void TPaintToolForm::UpdateCloneDelX(int clx)
{
   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localCloneX = clx;

   char str[16];
   sprintf(str, "%4d", clx);
   CloneDelXEditBox->Text = str;

   CloneDelXEditBox->Repaint();
}

void TPaintToolForm::UpdateCloneDelY(int cly)
{
   // No update if recycling strokes
   if (InhibitGuiUpdateHackFlag) return;

   localCloneY = cly;

   char str[16];
   sprintf(str, "%4d", cly);
   CloneDelYEditBox->Text = str;

   CloneDelYEditBox->Repaint();
}

void TPaintToolForm::UpdateCloneMessage(int majorstate)
{
   if (majorstate == MAJORSTATE_CLONE_MODE_HOME) {

      if (brushMode == BRUSH_MODE_THRU) {
         //Label1->Caption = "Clone pixels" ;
         ExecStatusBar->SetStatusMessage("Clone pixels");
      }
      else if (brushMode == BRUSH_MODE_ERASE) {
         //Label1->Caption = "Erase pixels";
         ExecStatusBar->SetStatusMessage("Erase pixels");
      }
   }
   else if ((majorstate == MAJORSTATE_CHOOSE_CLONE_PT)||
            (majorstate == MAJORSTATE_CHOOSE_CLONE_PT_ALT)) {

      //Label1->Caption = "Choose reference frame and clone point" ;
      ExecStatusBar->SetStatusMessage("Choose source frame and clone point");
   }
}

void TPaintToolForm::UpdateColorMessage(int majorstate)
{
   if (majorstate == MAJORSTATE_COLOR_MODE_HOME) {

      if (brushMode == BRUSH_MODE_THRU) {
         //Label1->Caption = "Paint pixels" ;
         ExecStatusBar->SetStatusMessage("Paint pixels");
      }
      else if (brushMode == BRUSH_MODE_ERASE) {
         //Label1->Caption = "Erase pixels";
         ExecStatusBar->SetStatusMessage("Erase pixels");
      }
   }
   else if (majorstate == MAJORSTATE_PICK_COLOR) {

      //Label1->Caption = "Choose color from target frame" ;
      ExecStatusBar->SetStatusMessage("Choose color from target frame");
   }
   else if (majorstate == MAJORSTATE_PICK_SEED) {

      //Label1->Caption = "Choose fill seed point in target frame" ;
      ExecStatusBar->SetStatusMessage("Choose fill seed point in target frame");
   }
   else if (majorstate == MAJORSTATE_CHOOSE_AUTOALIGN_PT) {

      //Label1->Caption = "Choose autoalign point in reference frame" ;
      ExecStatusBar->SetStatusMessage("Choose auto-align point in source frame");
   }
}

void TPaintToolForm::UpdateRY(int ry)
{
   localRGB[0] = (float)ry/(float)maxComponentValue;

   char str[16];
   sprintf(str, "%4d", ry);
   RYEditBox->Text = str;

   RYEditBox->Repaint();
}

void TPaintToolForm::UpdateGU(int gu)
{
   localRGB[1] = (float)gu/(float)maxComponentValue;

   char str[16];
   sprintf(str, "%4d", gu);
   GUEditBox->Text = str;

   GUEditBox->Repaint();
}

void TPaintToolForm::UpdateBV(int bv)
{
   localRGB[2] = (float)bv/(float)maxComponentValue;

   char str[16];
   sprintf(str, "%4d", bv);
   BVEditBox->Text = str;

   BVEditBox->Repaint();
}

void TPaintToolForm::UpdateRGB()
{
   // load the edit boxes
   UpdateRY(redCol[palSelectedRow][palSelectedCol]*valCol[palSelectedRow][palSelectedCol]*
                   maxComponentValue);
   UpdateGU(grnCol[palSelectedRow][palSelectedCol]*valCol[palSelectedRow][palSelectedCol]*
                   maxComponentValue);
   UpdateBV(bluCol[palSelectedRow][palSelectedCol]*valCol[palSelectedRow][palSelectedCol]*
                   maxComponentValue);
}

void TPaintToolForm::UpdateSelectedBoxColor()
{
   float maxrgb = localRGB[0];
   if (localRGB[1] > maxrgb) maxrgb = localRGB[1];
   if (localRGB[2] > maxrgb) maxrgb = localRGB[2];

   redCol[palSelectedRow][palSelectedCol] = (float)localRGB[0] / (float) maxrgb;
   grnCol[palSelectedRow][palSelectedCol] = (float)localRGB[1] / (float) maxrgb;
   bluCol[palSelectedRow][palSelectedCol] = (float)localRGB[2] / (float) maxrgb;
   valCol[palSelectedRow][palSelectedCol] = (float)maxrgb;

   MTI_UINT32 color = AdjustColorValue(redCol[palSelectedRow][palSelectedCol],
                                       grnCol[palSelectedRow][palSelectedCol],
                                       bluCol[palSelectedRow][palSelectedCol],
                                       valCol[palSelectedRow][palSelectedCol]);

   BitmapDrawBox(palCol[palSelectedRow][palSelectedCol], color, true);
   ColorPaletteDrawBox(palSelectedRow, palSelectedCol);

   ValueMeterSetLevel(valCol[palSelectedRow][palSelectedCol]);
}

void TPaintToolForm::UpdateColorPickSampleSize()
{
   if (localColorPickSampleSize < 0) localColorPickSampleSize = 0;
   if (localColorPickSampleSize > 2) localColorPickSampleSize = 2;

   GPaintTool->getSystemAPI()->setColorPickSampleSize(localColorPickSampleSize);

   AvgSizeComboBox->OnChange = NULL;
   AvgSizeComboBox->ItemIndex = localColorPickSampleSize;
   AvgSizeComboBox->OnChange = ColorPickSampleSizeChange;
}

void TPaintToolForm::UpdateFillTolerance()
{
   // clip value to permitted range
   if (localFillTolerance < 0)
      localFillTolerance =   0;
   if (localFillTolerance > ToleranceTrackBar->Max)
      localFillTolerance = ToleranceTrackBar->Max;

   // update the explicit tolerance value
   char tolrnc[6];

   sprintf(tolrnc, "%d", localFillTolerance);
   ToleranceLabel->Caption = tolrnc;
   ToleranceLabel->Repaint();

   // change the trackbar position (without feedback!)
   ToleranceTrackBar->OnChange = NULL;
   ToleranceTrackBar->Position = localFillTolerance;
   ToleranceTrackBar->OnChange = ToleranceTrackBarChange;

   // if in pick-seed mode clear the settling timer
   int majorState = GPaintTool->getMajorState();
   if (majorState == MAJORSTATE_PICK_SEED)
      toleranceTimer = 0;
}

static const TColor HiliteColor = TColor(0x0000F0FF);// slightly orangey-yellow
static const TColor LoliteColor = TColor(0x006A6A6A);// standard theme BG

void TPaintToolForm::UpdateDensityStrength(int denstrng)
{
   int majorState = GPaintTool->getMajorState();
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME) ||
         (majorState == MAJORSTATE_CLONE_MODE_HOME) ||
         (majorState == MAJORSTATE_COLOR_MODE_HOME))
   {
      localDensityStrength = denstrng;

      // update the explicit density value
      char densit[6];
      sprintf(densit, "%3d", localDensityStrength);
      ResetDensityPanel->Caption = densit;
      ResetDensityPanel->Color = LoliteColor;
      if (localDensityStrength != 0)
      {
         ResetDensityPanel->Color = HiliteColor;
      }

      ResetDensityPanel->Repaint();
      CockTheDensityTimer();
   }
}

void TPaintToolForm::CockTheDensityTimer()
{
   // cock the timer to recycle strokes
   // This takes too fricken long to do the first update:
   // densityTimer = 0;
   //
   // instead try to be a bit more intelligent about it:
   if (densityTimer >= DENSITY_LOAD_THRESHOLD)
   {
      if (densityTimer < (DENSITY_LOAD_THRESHOLD * 2))
      {
         densityTimer -= DENSITY_LOAD_THRESHOLD;
      }
      else
      {
         densityTimer = DENSITY_LOAD_THRESHOLD - 1;
      }
   }
}

void TPaintToolForm::UpdateGrainStrength(int grstrng)
{
   int majorState = GPaintTool->getMajorState();
   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)
   ||  (majorState == MAJORSTATE_CLONE_MODE_HOME)
   ||  (majorState == MAJORSTATE_COLOR_MODE_HOME))
   {
      localGrainStrength = grstrng;

      if (GrainUsePresetsCheckBox->Checked)
      {
         _grainPresetOverrideCurrentValue = localGrainStrength;
      }

      // update the explicit grain value
      char grain[6];
      sprintf(grain, "%3d", localGrainStrength);
      ResetGrainPanel->Caption = grain;
      ResetGrainPanel->Color = LoliteColor;
      if (localGrainStrength != 0)
      {
         ResetGrainPanel->Color = HiliteColor;
      }

      ResetGrainPanel->Repaint();
      CockTheDensityTimer();
   }
}

void TPaintToolForm::InitImportMode()
{
   int impmode = GPaintTool->getSystemAPI()->getImportMode();
   if      (impmode == IMPORT_MODE_ABSOLUTE)
      ReferenceFrameLockButton->Down = true;
   else if (impmode == IMPORT_MODE_RELATIVE)
      ReferenceFrameOffsetButton->Down = true;
}

//---------------------------------------------------------------------------
bool TPaintToolForm::ToInteger(AnsiString txt, int *val)
{
   *val = 0;
   int count = txt.Length();
   int state = 0;
   int whole = 0;
   bool neg = false;

   for (int i=1; i<=count; i++) {

      char thechar = txt[i];

      switch(state) {

         case 0: // leading spaces

            if (thechar == '+') {
               state = 1;
            }
            else if (thechar == '-') {

               neg = true;
               state = 1;
            }
            else if ((thechar >= '0')&&(thechar <= '9')) {

               whole = thechar - '0';
               state = 1;
            }
            else if (thechar == '.') {

               state = 2;
            }
            else if (thechar != ' ')
               return(false);

         break;

         case 1: // integer part

            if ((thechar >= '0')&&(thechar <= '9')) {

               whole = whole*10 + (thechar - '0');
            }
            else if (thechar == ' ') {

               state = 2;
            }
            else {
               return(false);
            }

         break;

         case 2:  // trailing spaces

            if (thechar != ' ')
               return(false);

         break;
      }
   }

   *val = whole;
   if (neg)
      *val = -(*val);

   return(true);
}

bool TPaintToolForm::ToDouble(AnsiString txt, double *val)
{
   *val = 0.0;
   int count = txt.Length();
   int state = 0;
   int whole = 0;
   int frn   = 0;
   int frnplcs = 1;
   bool neg = false;

   for (int i=1; i<=count; i++) {

      char thechar = txt[i];

      switch(state) {

         case 0: // leading spaces

            if (thechar == '+') {
               state = 1;
            }
            else if (thechar == '-') {

               neg = true;
               state = 1;
            }
            else if ((thechar >= '0')&&(thechar <= '9')) {

               whole = thechar - '0';
               state = 1;
            }
            else if (thechar == '.') {

               state = 2;
            }
            else if (thechar != ' ')
               return(false);

         break;

         case 1: // integer part

            if ((thechar >= '0')&&(thechar <= '9')) {

               whole = whole*10 + (thechar - '0');
            }
            else if (thechar == '.') {

               state = 2;
            }
            else {
               return(false);
            }

         break;

         case 2: // fractional part

            if ((thechar >= '0')&&(thechar <= '9')) {

               frn = frn*10 + (thechar - '0');
               frnplcs *= 10;
            }
            else if (thechar == ' ') {

               state = 3;
            }
            else {
               return(false);
            }

         break;

         case 3:  // trailing spaces

            if (thechar != ' ')
               return(false);

         break;
      }
   }

   *val = ( (double)whole + (double)frn / (double)frnplcs );
   if (neg)
      *val = -(*val);

   return(true);
}

//---------------------------------------------------------------------------
bool TPaintToolForm::DensityEstablished()
{
   return (!((densityTimer >= 0)&& (densityTimer <= TRACKBAR_LOAD_THRESHOLD)));
}

void TPaintToolForm::LoadImportFrameMode()
{
   if (ReferenceFrameLockButton->Down) {

      GPaintTool->getSystemAPI()->setImportMode(IMPORT_MODE_ABSOLUTE);
   }
   else if (ReferenceFrameOffsetButton->Down) {

      GPaintTool->getSystemAPI()->setImportMode(IMPORT_MODE_RELATIVE);
   }
   UpdateTrackingDataValidLight();
   GPaintTool->setTrackingAlignmentOffsets();
}

void TPaintToolForm::LoadImportFrameTimecode()
{
   int impfrm = ConvertImportFrameEditTextToFrameIndex();
   if (impfrm >= 0) {

      GPaintTool->getSystemAPI()->setImportFrameTimecode(impfrm);

      // if we're on the import frame, navigate to it
      if (GPaintTool->getSystemAPI()->getCaptureMode() == CAPTURE_MODE_IMPORT) {

         GPaintTool->goToImportFrame();
      }
      else {

         GPaintTool->goToTargetFrame();
      }
      UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();
   }
   else { // (impfrm < 0)
      UpdateImportFrameTimecode(localImportFrame);
   }
}

void TPaintToolForm::LoadImportFrameOffset()
{
   int value;

   if (ToInteger(ReferenceFrameOffsetEdit->Text, &value)) {

      GPaintTool->getSystemAPI()->setImportFrameOffset(value);

      // if we're on the import frame, navigate to it
      if (GPaintTool->getSystemAPI()->getCaptureMode() == CAPTURE_MODE_IMPORT) {

         GPaintTool->goToImportFrame();
      }
      else {

         GPaintTool->goToTargetFrame();
      }

      UpdateTrackingDataValidLight();
      GPaintTool->setTrackingAlignmentOffsets();

   }
   else
      UpdateImportFrameOffset(localImportFrameOffset);
}

void TPaintToolForm::LoadRotation()
{
   double value;
   if (ToDouble(RotationEditBox->Text, &value)) {

      value = value*PI/180.;

      GPaintTool->getSystemAPI()->SetRotation(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateRotation(localRotation);
}

void TPaintToolForm::LoadSkew()
{
   double value;
   if (ToDouble(SkewEditBox->Text, &value)) {

      GPaintTool->getSystemAPI()->SetSkew(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateSkew(localSkew);
}

void TPaintToolForm::LoadStretchX()
{
   double value;
   if (ToDouble(HEditBox->Text, &value)) {

      value /= 100.;

      GPaintTool->getSystemAPI()->SetStretchX(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateHStretch(localStretchX);
}

void TPaintToolForm::LoadStretchY()
{
   double value;
   if (ToDouble(VEditBox->Text, &value)) {

      value /= 100.;

      GPaintTool->getSystemAPI()->SetStretchY(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateVStretch(localStretchY);
}

void TPaintToolForm::LoadOffsetX()
{
   double value;
   if (ToDouble(XEditBox->Text, &value)) {

      GPaintTool->getSystemAPI()->SetOffsetX(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateXOffset(localOffsetX);
}

void TPaintToolForm::LoadOffsetY()
{
   double value;
   if (ToDouble(YEditBox->Text, &value)) {

      GPaintTool->getSystemAPI()->SetOffsetY(value);
      GPaintTool->goToTargetFrame();
   }
   else
      UpdateYOffset(localOffsetY);
}

void TPaintToolForm::LoadCloneX()
{
   int value;
   if (ToInteger(CloneXEditBox->Text, &value)) {

      GPaintTool->SetCloneX(value);
      localCloneX = value;
   }
   UpdateCloneX(localCloneX);
}

void TPaintToolForm::LoadCloneY()
{
   int value;
   if (ToInteger(CloneYEditBox->Text, &value)) {

      GPaintTool->SetCloneY(value);
      localCloneY = value;
   }
   UpdateCloneY(localCloneY);
}

void TPaintToolForm::LoadCloneDelX()
{
   int value;
   if (ToInteger(CloneDelXEditBox->Text, &value)) {

      GPaintTool->SetCloneDelX(value);
      localCloneDelX = value;
   }
   UpdateCloneDelX(localCloneDelX);
}

void TPaintToolForm::LoadCloneDelY()
{
   int value;
   if (ToInteger(CloneDelYEditBox->Text, &value)) {

      GPaintTool->SetCloneDelY(value);
      localCloneDelY = value;
   }
   UpdateCloneDelY(localCloneDelY);
}

void TPaintToolForm::LoadRY()
{
   int value;
   if (ToInteger(RYEditBox->Text, &value)) {

      if (value > maxComponentValue) value = maxComponentValue;

      localRGB[0] = (float)value/(float)maxComponentValue;

      int majorState = GPaintTool->getMajorState();

      if ((majorState == MAJORSTATE_COLOR_MODE_HOME)||
          (majorState == MAJORSTATE_PICK_COLOR))
         LoadBrush();
   }
   UpdateRY(value);

   UpdateSelectedBoxColor();
}

void TPaintToolForm::LoadGU()
{
   int value;
   if (ToInteger(GUEditBox->Text, &value)) {

      if (value > maxComponentValue) value = maxComponentValue;

      localRGB[1] = (float)value/(float)maxComponentValue;

      int majorState = GPaintTool->getMajorState();

      if ((majorState == MAJORSTATE_COLOR_MODE_HOME)||
          (majorState == MAJORSTATE_PICK_COLOR))
         LoadBrush();
   }
   UpdateGU(value);

   UpdateSelectedBoxColor();
}

void TPaintToolForm::LoadBV()
{
   int value;
   if (ToInteger(BVEditBox->Text, &value)) {

      if (value > maxComponentValue) value = maxComponentValue;

      localRGB[2] = (float)value/(float)maxComponentValue;

      int majorState = GPaintTool->getMajorState();

      if ((majorState == MAJORSTATE_COLOR_MODE_HOME)||
          (majorState == MAJORSTATE_PICK_COLOR))
         LoadBrush();
   }
   UpdateBV(value);

   UpdateSelectedBoxColor();
}

int TPaintToolForm::GetLocalImportFrame()
{
   return localImportFrame;
}

void TPaintToolForm::ClearLoadTimer()
{
   // load timer orbits between threshold + 1 and 0x7fffffff
   loadTimer = IMPORT_FRAME_LOAD_THRESHOLD + 1;
}

void TPaintToolForm::SetLoadTimer()
{
   // load timer counts up to threshold, then loads target & import frame
   loadTimer = 0;
}

void TPaintToolForm::ClearStrokeStack()
{
   PendingStrokesListBox->Items->Clear();

   // NOTE this presumes that if it wasn't OK to enable these buttons,
   // we couldn't do anything to get called here!
   UpdateStateOfAcceptRejectUndoAndMacroButtons(true);
}

void TPaintToolForm::SetStrokeEntry(int ordnum, int brmod)
{
   char strokestr[32];

   if (brmod == BRUSH_MODE_THRU)
      sprintf(strokestr, "Stroke %2d: Thru", ordnum);
   else if (brmod == BRUSH_MODE_ERASE)
      sprintf(strokestr, "Stroke %2d: Erase", ordnum);
   else if (brmod == BRUSH_MODE_ORIG)
      sprintf(strokestr, "Stroke %2d: Original", ordnum);

   PendingStrokesListBox->Items->Add(strokestr);

   PendingStrokesListBox->Selected[0] = true;
}

//--------------------------------------------------------------------------
void __fastcall TPaintToolForm::EditBoxEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_OFFSET);
}
//--------------------------------------------------------------------------

void __fastcall TPaintToolForm::EditKeyPress(
      TObject *Sender, char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::ReferenceFrameTimecodeEditExit(
      TObject *Sender)
{
   LoadImportFrameTimecode();
   UpdateTrackingDataValidLight();
}

void __fastcall TPaintToolForm::ReferenceFrameTimecodeEditChange(
      TObject *Sender)
{
   int impfrm = ConvertImportFrameEditTextToFrameIndex();

   if (((impfrm < 0)||GPaintTool->ShouldImportFrameTimecodeBeRed(impfrm))
       && ReferenceFrameTimecodeEdit->Visible
       && ReferenceFrameTimecodeEdit->Enabled) {
      ////ReferenceFrameTimecodeEdit->Color = clRed;
      RangeWarningPanel->Visible = true;
   }
   else {
      ////ReferenceFrameTimecodeEdit->Color = clWindow;
      RangeWarningPanel->Visible = false;
   }

   UpdateTrackingDataValidLight();
}
//--------------------------------------------------------------------------

void __fastcall TPaintToolForm::ReferenceFrameOffsetEditExit(
      TObject *Sender)
{
   LoadImportFrameOffset();
   UpdateTrackingDataValidLight();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ReferenceFrameLockButtonClick(
      TObject *Sender)
{
   GPaintTool->SelectAbsolute();
   UpdateTrackingDataValidLight();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ReferenceFrameOffsetButtonClick(
      TObject *Sender)
{
   GPaintTool->SelectRelative();
   UpdateTrackingDataValidLight();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::MixingTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_MIX);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::MixingTrackBarChange(TObject *Sender)
{
	int newTargetWgt = MixingTrackBar->Position;
	int mixingRange = MixingTrackBar->Max - MixingTrackBar->Min;
	GPaintTool->getSystemAPI()->setOnionSkinTargetWgt((double)newTargetWgt / (double)mixingRange);

	// refresh if you're in the right mode
	int majorState = GPaintTool->getMajorState();
	if (majorState == MAJORSTATE_EDIT_TRANSFORM
	|| majorState == MAJORSTATE_POSITION_IMPORT_FRAME
	|| majorState == MAJORSTATE_MASK_TRANSFORM
	|| majorState == MAJORSTATE_EDIT_TRACKING
	|| majorState == MAJORSTATE_TRACKING_DONE
	|| majorState == MAJORSTATE_TRACKING_PAUSED
	|| ((majorState == MAJORSTATE_REVEAL_MODE_HOME) && (GPaintTool->getAlignState() == ALIGNSTATE_TRACK_SHOWING_BOTH)))
	{
		// GPaintTool->goToTargetFrame();
		GPaintTool->getSystemAPI()->refreshFrameCached();
	}
}

//--------------------------------------------------------------------------
void __fastcall TPaintToolForm::RotationEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::RotationEditBoxExit(TObject *Sender)
{
   LoadRotation();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::SkewEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::SkewEditBoxExit(TObject *Sender)
{
   LoadSkew();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::HEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::HEditBoxExit(TObject *Sender)
{
   LoadStretchX();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::VEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::VEditBoxExit(TObject *Sender)
{
   LoadStretchY();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::XEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN)
   {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::XEditBoxExit(TObject *Sender)
{
   LoadOffsetX();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::YEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::YEditBoxExit(TObject *Sender)
{
   LoadOffsetY();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::CloneXEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoadCloneX();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::CloneXEditBoxExit(TObject *Sender)
{
   LoadCloneX();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::CloneYEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::CloneYEditBoxExit(TObject *Sender)
{
   LoadCloneY();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::CloneDelXEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoadCloneDelX();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::CloneDelXEditBoxExit(TObject *Sender)
{
   LoadCloneDelX();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::CloneDelYEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::CloneDelYEditBoxExit(TObject *Sender)
{
   LoadCloneDelY();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RYEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoadRY();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::RYEditBoxExit(TObject *Sender)
{
   LoadRY();
}

void __fastcall TPaintToolForm::GUEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::GUEditBoxExit(TObject *Sender)
{
   LoadGU();
}

void __fastcall TPaintToolForm::BVEditBoxKeyPress(TObject *Sender,
      char &Key)
{
   if (Key == MTK_RETURN) {
      LoseTheFocus();
      Key = 0;
   }
}

void __fastcall TPaintToolForm::BVEditBoxExit(TObject *Sender)
{
   LoadBV();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::UndoLastButtonClick(TObject *Sender)
{
   // this button undoes the last stroke made by the user
   //
   // this generates a callback to update the listbox entries
   GPaintTool->PopStrokesToTarget(1);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::UndoThruSelectButtonClick(TObject *Sender)
{
   // this button undoes the strokes up to and including the selected entry
   //
   // look for the selected entry
   int isel = -1;
   for (int i=0; i < PendingStrokesListBox->Count; i++) {
      if (PendingStrokesListBox->Selected[i]) {
         isel = i;
         break;
      }
   }

   // if selected was found, undo strokes THRU the selected entry
   if (isel >= 0) {

      // this generates a callback to update the listbox entries
      GPaintTool->PopStrokesToTarget(isel+1);
   }

   bool stackNonEmpty = (PendingStrokesListBox->Count > 0);
   UndoLastButton->Enabled       = stackNonEmpty;
   UndoThruSelectButton->Enabled = stackNonEmpty;
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ClonePointOffsetSpeedButtonClick(
      TObject *Sender)
{
   if (ClonePointOffsetSpeedButton->Down) {

      localStrokeRelative = true;

      int majorState = GPaintTool->getMajorState();
      if (majorState == MAJORSTATE_CLONE_MODE_HOME) {
         GPaintTool->SetCloneBrushRelative(localStrokeRelative);
         GPaintTool->goToTargetFrame();
      }
   }

   ClonePointLockSpeedButton->Down = !localStrokeRelative;
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ClonePointLockSpeedButtonClick(
      TObject *Sender)
{
   if (ClonePointLockSpeedButton->Down) {

      localStrokeRelative = false;

      int majorState = GPaintTool->getMajorState();
      if (majorState == MAJORSTATE_CLONE_MODE_HOME) {

         GPaintTool->SetCloneBrushRelative(localStrokeRelative);
         GPaintTool->goToTargetFrame();
      }
   }

   ClonePointOffsetSpeedButton->Down = localStrokeRelative;
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::SetClonePointSpeedButtonClick(TObject *Sender)
{
   GPaintTool->SelectChooseClonePt();
}

//---------------------------------------------------------------------------
//
//                        C O L O R   M O D E
//
void __fastcall TPaintToolForm::ColorPalettePaintBoxMouseMove(
      TObject *Sender, TShiftState Shift, int X, int Y)
{
   palMouseX = X;
   palMouseY = Y;
}

void __fastcall TPaintToolForm::ColorPalettePaintBoxClick(TObject *Sender)
{
   int row = palMouseY / (COL_HGHT+COL_BETW);
   int col = palMouseX / (COL_WDTH+COL_BETW);

   int xcel = palMouseX % (COL_WDTH+COL_BETW);
   int ycel = palMouseY % (COL_HGHT+COL_BETW);

   if ((xcel>0)&&(xcel<COL_WDTH)&&(ycel>0)&&(ycel<COL_HGHT)) {

      ColorPaletteSelectBox(palSelectedRow, palSelectedCol, false);
      ColorPaletteSelectBox(row, col, true);
      ValueMeterSetLevel(valCol[row][col]);
      palSelectedRow = row; palSelectedCol = col;
   }

   UpdateRGB();
   LoadBrush();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::HueSaturationPaintBoxMouseMove(
      TObject *Sender, TShiftState Shift, int X, int Y)
{
  hueMouseX = X;
  hueMouseY = Y;
}

void __fastcall TPaintToolForm::HueSaturationPaintBoxClick(TObject *Sender)
{
   int retVal = HueSaturationGetRGB(hueMouseX, hueMouseY,
                                    &redCol[palSelectedRow][palSelectedCol],
                                    &grnCol[palSelectedRow][palSelectedCol],
                                    &bluCol[palSelectedRow][palSelectedCol]);


   if (retVal != -1) {

      MTI_UINT32 color = AdjustColorValue(redCol[palSelectedRow][palSelectedCol],
                                          grnCol[palSelectedRow][palSelectedCol],
                                          bluCol[palSelectedRow][palSelectedCol],
                                          valCol[palSelectedRow][palSelectedCol]);

      BitmapDrawBox(palCol[palSelectedRow][palSelectedCol], color, true);
      ColorPaletteDrawBox(palSelectedRow, palSelectedCol);

      UpdateRGB();
      LoadBrush();
   }
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ValueMeterMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   adjLevel = ((valMouseX >= (MWD+2))&&(valMouseX <= (2*MWD+1))&&
               (valMouseY >= (valY-MWD+1))&&(valMouseY <= (valY+MWD-1)));
}

void __fastcall TPaintToolForm::ValueMeterMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   if (adjLevel) {

      int y = Y;
      if (y < 1) y = 1;
      if (y > (valHght - 2)) y = (valHght - 2);

      float val = (float)(valHght - 2 - y)/(float)(valHght-3);
      ValueMeterSetLevel(val);

      valCol[palSelectedRow][palSelectedCol] = val;

      MTI_UINT32 col = AdjustColorValue(redCol[palSelectedRow][palSelectedCol],
                                        grnCol[palSelectedRow][palSelectedCol],
                                        bluCol[palSelectedRow][palSelectedCol],
                                        val);

      BitmapDrawBox(palCol[palSelectedRow][palSelectedCol], col, true);
	  ColorPaletteDrawBox(palSelectedRow, palSelectedCol);

      UpdateRGB();
      LoadBrush();
   }

   valMouseX = X;
   valMouseY = Y;
}

void __fastcall TPaintToolForm::ValueMeterMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   adjLevel = false;
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::DropperSpeedButtonClick(TObject *Sender)
{
   if (DropperSpeedButton->Down) {

      GPaintTool->SelectPickColor();
   }
   else {

      GPaintTool->EscPickColor();
   }
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ToleranceTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_TOLERANCE);
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ToleranceTrackBarChange(TObject *Sender)
{
   localFillTolerance = ToleranceTrackBar->Position;
   UpdateFillTolerance();
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ColorPickSampleSizeChange(TObject *Sender)
{
   localColorPickSampleSize = AvgSizeComboBox->ItemIndex;
   UpdateColorPickSampleSize();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::BucketFillSpeedButtonClick(TObject *Sender)
{
   if (BucketFillSpeedButton->Down) {

      GPaintTool->SelectPickSeed();
   }
   else {

      GPaintTool->EscPickSeed();
   }
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AutoAcceptCheckBoxClick(TObject *Sender)
{
   GPaintTool->EnableAutoAccept(AutoAcceptCheckBox->Checked);
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::PlayStrokeSpeedButtonClick(TObject *Sender)
{
   GPaintTool->PlayCapturedStroke(true);

   // pop the speed button back up
   PlayStrokeSpeedButton->Down = false;
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::RangeStrokeSpeedButtonClick(TObject *Sender)
{
   if (RangeStrokeSpeedButton->Down) {

      GPaintTool->ExecuteRangeMacro();
   }
}

//---------------------------------------------------------------------------

bool TPaintToolForm::HandleToolEvent(const FosterParentEventInfo &eventInfo)
{
   bool retVal = false;  // assume not handled
   WORD Key = eventInfo.Key;
   TShiftState ShiftState;
   int X = eventInfo.X;
   int Y = eventInfo.Y;

   if (eventInfo.Shift)  ShiftState << ssShift;
   if (eventInfo.Alt)    ShiftState << ssAlt;
   if (eventInfo.Ctrl)   ShiftState << ssCtrl;
   if (eventInfo.Left)   ShiftState << ssLeft;
   if (eventInfo.Right)  ShiftState << ssRight;
   if (eventInfo.Middle) ShiftState << ssMiddle;
   if (eventInfo.Double_Clicked)
                         ShiftState << ssDouble;

   switch (eventInfo.Id)
   {
      case FPE_FormPaint:
         formPaint();
         retVal = true;
      break;

      case FPE_FormKeyDown:
         retVal = formKeyDown(Key, ShiftState);
      break;

      case FPE_FormKeyUp:
         retVal = formKeyUp(Key, ShiftState);
      break;

      case FPE_FormMouseMove:
         formMouseMove(ShiftState, X, Y);
         retVal = true;
      break;

      default:
         // Do nothing
      break;
   }

   return retVal;
}
//---------------------------------------------------------------------------


void __fastcall TPaintToolForm::PaintBoxPaint(TObject *Sender)
{
   formPaint();
}
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::ClearButtonClick(TObject *Sender)
{
  GPaintTool->ClearCapturedStroke();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::StopSpeedButtonClick(TObject *Sender)
{
   GPaintTool->SuspendRangeMacro();
}

//---------------------------------------------------------------------------
// TEST - will be revised
void __fastcall TPaintToolForm::InvertButtonClick(TObject *Sender)
{
   double rot, strx, stry, skew, offx, offy;

   // current xform
   GPaintTool->getSystemAPI()->GetTransform(&rot, &strx, &stry, &skew, &offx, &offy);

   int majorState = GPaintTool->getMajorState();

#if 0
   if (majorState == MAJORSTATE_POSITION_IMPORT_FRAME) {

      // invert just the offsets
      GPaintTool->getSystemAPI()->SetTransform( rot, strx, stry, skew, -offx, -offy);

      // show the target frame
      GPaintTool->goToTargetFrame();
   }
   else if (majorState == MAJORSTATE_EDIT_TRANSFORM) {
#endif

   if ((majorState == MAJORSTATE_REVEAL_MODE_HOME)||
       (majorState == MAJORSTATE_POSITION_IMPORT_FRAME)||
       (majorState == MAJORSTATE_EDIT_TRANSFORM)) {

      // the transform is realized as ROTATION x SCALE x SKEW, where
      // the ROTATION is
      //
      //  |  cos   sin  |
      //  |             |
      //  | -sin   cos  |
      //
      // the SCALE is
      //
      //  | strX   0    |
      //  |             |
      //  |       strY  |
      //
      // and the SKEW is
      //
      //  |  1    skew  |
      //  |             |
      //  |  0     1    |
      //
      // SKEW is applied first, then SCALE, and finally ROTATION
      //
      // Each is these is easily inverted, enabling us to write
      //
      // INV(transform) =  INV(SKEW) x INV(SCALE) x INV(ROTATION)
      //
      // to get, finally,
      //
      // INV(TRANSFORM) =
      //
      // |  cos/strX - skew * sin / strY     -sin/strX - skew * cos / strY   |
      // |                                                                   |
      // |                                                                   |
      // |             sin / strY                        cos / strY          |
      //
      // it is THIS matrix that we want to realize as a product of
      //
      // ROTATION x SCALE x SKEW
      //                           |  s    u  |
      // setting INV(TRANSFORM) =  |          |
      //                           |  t    v  |
      //
      // we consider the effect of the inverse transform applied to a unit
      // square centered on the origin. The vector (1, 0) representing the
      // (delx ,dely) of the top edge maps to ( s, t ), while the vector
      // (0, 1) representing the (delx, dely) of the left edge maps to (u, v)
      //
      // By applying the ROTATION'
      //                             |  s/l    t/l |
      //                             |             |
      //                             | -t/l    s/l |
      //
      // where l is the length of (s, t)
      //
      // followed by the SCALING'
      //                             |  1/l        0       |
      //                             |                     |
      //                             |   0   l/(s*v + t*u) |
      //
      // followed by the SKEW'
      //                             |   1   (t*v - s*u)/(s*s + t*t)  |
      //                             |                                |
      //                             |   0                1           |
      //
      // Each of the above is easily invertible, enabling us to write
      //
      // INV(TRANSFORM) = INV(ROTATION') x INV(SCALING') x INV(SKEW')
      //
      // which is the required form
      //
      double rotcos = cos(rot);
      double rotsin = sin(rot);

      // 1st column of inverse matrix
      double s = (rotcos/strx) - (skew*rotsin/stry);
      double t = (rotsin/stry);
      double l = sqrt(s*s + t*t);

      // 2nd column of inverse matrix
      double u = -(rotsin/strx) - (skew*rotcos/stry);
      double v = (rotcos/stry);

      // in terms of the above
      double iskew = (s*u + t*v)/(s*s + t*t);

      double istrx = l;
      double istry = (s*v - t*u)/l;

      double irotcos =   s/l;
      //double irotsin =  -t/leng;
      double irot = acos(fabs(irotcos));

      // now the contribution of the offsets
      double ioffx = -(s*offx + u*offy);
      double ioffy = -(t*offx + v*offy);

      // map to quadrants
      if ((s>=0)&&(t<0))  irot = irot;
      if ((s>=0)&&(t>=0)) irot = -irot;
      if ((s< 0)&&(t<0))  irot = PI - irot;
      if ((s< 0)&&(t>=0)) irot = -PI + irot;

      // now ready to set the completed INVERSE
      GPaintTool->getSystemAPI()->SetTransform(irot, istrx, istry, iskew, ioffx, ioffy);

      // show the target frame
      GPaintTool->goToTargetFrame();
   }
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::QuickAlignButtonClick(TObject *Sender)
{
   if (QuickAlignButton->Down) {

      if (Sender != NULL) {
         GetOutOfTrackAlignMode();
      }

      GPaintTool->setNextMajorState(GPaintTool->getMajorState());
      GPaintTool->ToggleChooseAutoAlignPointMode();
   }
   else {

      GPaintTool->ExitFromChooseAutoAlignPointMode();
   }
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackAlignButtonClick(TObject *Sender)
{
   // HACK to cut off recursion
   if (InhibitTrackAlignButtonClick)  return;
   InhibitTrackAlignButtonClick = true;

   if (GPaintTool == NULL) return;

   // Pop out of "Edit Tracking" mode if we were in it, but only if this is
   // a responsr to a REAL click!
	if (Sender != NULL
	&& (GPaintTool->getMajorState() == MAJORSTATE_EDIT_TRACKING
		|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_DONE
		|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_PAUSED))
	{
		// Pretend we clicked the "Tracking" button!
      TrackingButton->Down = false;
      TrackingButtonClick(NULL);

      // Don't know why I have to do this, but make sure the button is DOWN!!
      TrackAlignButton->Down = true;
   }

   // this needs to be done before we do anything else
   GPaintTool->SelectTrackingAlignment(TrackAlignButton->Down);

   if (Sender != NULL) {

      // if this button is down, "click" it to get it out of that state
      if (PositionImportButton->Down) {

         PositionImportButton->Down = false;
         PositionImportButtonClick(NULL);
      }

      // if we were in the middle of doing a "quick align", pop out of that
      if (QuickAlignButton->Down) {

         QuickAlignButton->Down = false;
         QuickAlignButtonClick(NULL);
      }
   }

   if (TrackAlignButton->Down) {

      PositionImportButton->Caption = "Show SRC and TGT";

      InvertButton->Enabled = false;

      // I prefer to set ReadOnly to true instead of Enabled to false
      // because I want the numbers to be black, not greyed out
      RotationEditBox->ReadOnly = true;
      RotationEditBox->Color = clBtnFace;
      SkewEditBox->ReadOnly = true;
      SkewEditBox->Color = clBtnFace;
      HEditBox->ReadOnly = true;
      HEditBox->Color = clBtnFace;
      VEditBox->ReadOnly = true;
      VEditBox->Color = clBtnFace;
      XEditBox->ReadOnly = true;
      XEditBox->Color = clBtnFace;
      YEditBox->ReadOnly = true;
      YEditBox->Color = clBtnFace;
   }
   else {

      PositionImportButton->Caption = "Transform";

      ReferenceFrameTimecodeEdit->Enabled = true;
      ReferenceFrameTimecodeEditChange(NULL);
      ReferenceFrameLockButton->Enabled = true;
      ReferenceFrameOffsetButton->Enabled = true;
      ReferenceFrameOffsetEdit->Enabled = true;

      QuickAlignButton->Enabled = true;
      InvertButton->Enabled = true;

      RotationEditBox->ReadOnly = false;
      RotationEditBox->Color = clWindow;
      SkewEditBox->ReadOnly = false;
      SkewEditBox->Color = clWindow;
      HEditBox->ReadOnly = false;
      HEditBox->Color = clWindow;
      VEditBox->ReadOnly = false;
      VEditBox->Color = clWindow;
      XEditBox->ReadOnly = false;
      XEditBox->Color = clWindow;
      YEditBox->ReadOnly = false;
      YEditBox->Color = clWindow;
   }

   ReferenceFrameTimecodeEditChange(NULL);

   InhibitTrackAlignButtonClick = false;
}

//---------------------------------------------------------------------------
void TPaintToolForm::GetOutOfTrackAlignMode()
{
         // If we were in tracking align mode, get out of it by
         // pretending to click on the button
         if (TrackAlignButton->Down && !InhibitTrackAlignButtonClick) {

            TrackAlignButton->Down = false;
            TrackAlignButtonClick(NULL);
         }
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::DensitySpeedButtonClick(TObject *Sender)
{
   if (DensitySpeedButton->Down) {

      GPaintTool->SelectDensityAdjust();
   }
   else {

      GPaintTool->EscDensityAdjust();
   }

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::GrainSpeedButtonClick(TObject *Sender)
{
   if (GrainSpeedButton->Down) {

		GrainPresetsPanel->Enabled = true;
		GrainPresetsProjectRadioButton->Enabled = true;
		GrainPresetsClipRadioButton->Enabled = true;
		GrainPreset1Button->Enabled = true;
		GrainPreset2Button->Enabled = true;

		GPaintTool->SelectGrainAdjust();
   }
   else {

		GrainPresetsPanel->Enabled = false;
		GrainPresetsProjectRadioButton->Enabled = false;
		GrainPresetsClipRadioButton->Enabled = false;
		GrainPreset1Button->Enabled = false;
		GrainPreset2Button->Enabled = false;

		GPaintTool->EscGrainAdjust();
   }

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ResetDensityPanelClick(TObject *Sender)
{
   // reset to zero
   GPaintTool->SetDensityStrength(0);

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::ResetGrainPanelClick(TObject *Sender)
{
   // reset to zero
   GPaintTool->SetGrainStrength(0);

   // set focus to main window
   LoseTheFocus();
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::FormMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   /////////////////////////////////////////////
   //                                         //
   // WW            WW  WWWWWWWW  FFFFFFF  !! //
   //  WW    WW    WW      WW     FF       !! //
   //   WW  WWWW  WW       WW     FFFFF    !! //
   //    WWWW  WWWW        WW     FF          //
   //     WW    WW         WW     FF       !! //
   //                                         //
   /////////////////////////////////////////////
//   if (!DummyEdit->Focused()&&
//       !MixingTrackBar->Focused()&&
//       !ToleranceTrackBar->Focused()&&
//       !RadiusTrackBar->Focused()&&
//       !OpacityTrackBar->Focused()&&
//       !BlendTrackBar->Focused()&&
//       !AspectTrackBar->Focused()&&
//       !AngleTrackBar->Focused()) {
//
//      // focusing this will allow the TrackBarEnter
//      // callbacks to function on re-entry to panel
//      DummyEdit->SetFocus();
//   }
}
//---------------------------------------------------------------------------

void TPaintToolForm::InitTrackingBoxSizeTrackBar(int min, int max, int pos)
{
   REMOVE_CBHOOK(TrackingTrackBarChanged, TrackingBoxSizeTrackEdit->TrackEditValueChange);
   TrackingBoxSizeTrackEdit->SetMinAndMax(min, max);
   TrackingBoxSizeTrackEdit->SetValue(pos);
   SET_CBHOOK(TrackingTrackBarChanged, TrackingBoxSizeTrackEdit->TrackEditValueChange);
}
//---------------------------------------------------------------------------

void TPaintToolForm::InitTrackingMotionSearchTrackBar(int min, int max, int pos)
{
   REMOVE_CBHOOK(TrackingTrackBarChanged, TrackingMotionSearchTrackEdit->TrackEditValueChange);
   TrackingMotionSearchTrackEdit->SetMinAndMax(min, max);
   TrackingMotionSearchTrackEdit->SetValue(pos);
   SET_CBHOOK(TrackingTrackBarChanged, TrackingMotionSearchTrackEdit->TrackEditValueChange);
}
//---------------------------------------------------------------------------

void TPaintToolForm::TrackingTrackBarChanged(void *Sender)
{
   int boxSize = TrackingBoxSizeTrackEdit->GetValue();
   int searchSize = TrackingMotionSearchTrackEdit->GetValue();
   GPaintTool->SetTrackingBoxSizes(boxSize, boxSize, searchSize, searchSize, searchSize, searchSize);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingDeletePointButtonClick(
	  TObject *Sender)
{
   GPaintTool->DeleteSelectedTrackingPoints();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingPrevPointButtonClick(
      TObject *Sender)
{
   GPaintTool->SelectPreviousTrackingPoint();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingNextPointButtonClick(
      TObject *Sender)
{
    GPaintTool->SelectNextTrackingPoint();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingShowPointCBoxClick(TObject *Sender)
{
   GPaintTool->setDisplayTrackingPoints(TrackingShowPointCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingTrackButtonClick(TObject *Sender)
{
	GPaintTool->StartTracking();
}
//---------------------------------------------------------------------------

void TPaintToolForm::makeTrackPointsVisible()
{
   if (GPaintTool == NULL) return;

   TrackingShowPointCBox->Checked = true;
   GPaintTool->setDisplayTrackingPoints(true);
}
//---------------------------------------------------------------------------

void TPaintToolForm::toggleTrackPointVisibility()
{
   if (GPaintTool == NULL) return;

   if (TrackingShowPointCBox->Enabled)
   {
      TrackingShowPointCBox->Checked = !TrackingShowPointCBox->Checked;
      GPaintTool->setDisplayTrackingPoints(TrackingShowPointCBox->Checked);
   }
}
//----------------------------------------------------------------------------

void TPaintToolForm::TrackingPointsChanged(void *Sender)
{
   // Moved this stuff to a one-shot timer so it executes in main thread
   TrackingPointsChangedTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void TPaintToolForm::UpdateTrackingPointButtons(void)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   int validCount = GPaintTool->getCountOfValidTrackingPoints();
	if (validCount > 0 && GPaintTool->getMajorState() != MAJORSTATE_TRACKING_IN_PROGRESS)
	{
      TrackingTrackButton->Enabled = true;
	   TrackingDeletePointButton->Enabled = LastSelectedTag() != -1;
      TrackingPrevPointButton->Enabled = ((validCount == 1)? false : true);
		TrackingNextPointButton->Enabled = TrackingPrevPointButton->Enabled;
	}
   else
   {
		TrackingTrackButton->Enabled = false;
      TrackingDeletePointButton->Enabled = false;
      TrackingPrevPointButton->Enabled = false;
		TrackingNextPointButton->Enabled = false;
   }

	if (GPaintTool->getMajorState() == MAJORSTATE_TRACKING_IN_PROGRESS
	|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_PAUSED)
	{
		LoseTheFocus();
	}
	else if (GPaintTool->getMajorState() == MAJORSTATE_EDIT_TRACKING
	|| GPaintTool->getMajorState() == MAJORSTATE_TRACKING_DONE)
	{
		// I don't know why we do this!!
		GPaintTool->SetToolFocus();
	}

}
//---------------------------------------------------------------------------

void TPaintToolForm::UpdateTrackingDataValidLight()
{
//   TRACE_3(errout << "ENTER");
   int newTrackValidState = GPaintTool->getTrackingDataValidity();
   int newTrackingButtonEnabledState = TrackingButton->Enabled?
                                                         TRKBTN_ENABLED_STATE :
                                                         TRKBTN_DISABLED_STATE;

   if (newTrackValidState != oldTrackValidState ||
       newTrackingButtonEnabledState != oldTrackingButtonEnabledState)
   {
      oldTrackValidState = newTrackValidState;
      oldTrackingButtonEnabledState = newTrackingButtonEnabledState;

      // button glyph moves one pixel right and down when pressed
      int xyOffset = (TrackingButton->Down)? 5 : 4;

      TrackDataOkImage->Visible = false;
      TrackDataCautionImage->Visible = false;
      TrackDataInvalidImage->Visible = false;

      TrackDataDisabledImage->Top = TrackingButton->Top + xyOffset;
      TrackDataDisabledImage->Left = TrackingButton->Left + xyOffset;
      TrackDataDisabledImage->Visible = true;

      if (TrackingButton->Enabled) {

         switch (newTrackValidState) {

            case TRKVLD_ALL_TRACK_DATA_IS_VALID:
               TrackDataOkImage->Top = TrackingButton->Top + xyOffset;
               TrackDataOkImage->Left = TrackingButton->Left + xyOffset;
               TrackDataOkImage->Visible = true;
            break;

            case TRKVLD_TRACK_DATA_IS_VALID_EXCEPT_FOR_REF_FRAME:
               TrackDataCautionImage->Top = TrackingButton->Top + xyOffset;
               TrackDataCautionImage->Left = TrackingButton->Left + xyOffset;
               TrackDataCautionImage->Visible = true;
            break;

            case TRKVLD_NEED_TO_RETRACK:
               TrackDataInvalidImage->Top = TrackingButton->Top + xyOffset;
               TrackDataInvalidImage->Left = TrackingButton->Left + xyOffset;
               TrackDataInvalidImage->Visible = true;
            break;
         }

         TrackDataDisabledImage->Visible = false;
      }
   }

   // SIDE EFFECT ALERT
   // sync 'track align' button enabledness with light
   UpdateStateOfTrackAlignButton(true);
//   TRACE_3(errout << "EXIT");
}
//---------------------------------------------------------------------------

void TPaintToolForm::UpdateTrackingCharts(void)
{
    selectedTagChanged(NULL); // this should work
}
//----------------------------------------------------------------------------

void TPaintToolForm::selectedTagChanged(void *Sender)
{
   if (GPaintTool == NULL)
      return;

   int nTag = LastSelectedTag();

   // UpdateStatusBarInfo();

   // Now update the individual smoothing
   TrackingBoxSet &TA = GPaintTool->GetTrackingArrayRef();

   if (TA.getNumberOfTrackingBoxes() < 1) {
      TrackingDataAvailableMessagePanel->Caption = "Tracking data not available.";
      HideShowCharts(false);
      return;
   }

   if (!GPaintTool->isAllTrackDataValid()) {
      TrackingDataAvailableMessagePanel->Caption =
         AnsiString("Tracking data not available. Point")
         + ((TA.getNumberOfTrackingBoxes() == 1)? "" : "s") + " not tracked.";
      HideShowCharts(false);
      return;
   }

   // See if a point is selected
   if (!TA.isValidTrackingBoxTag(nTag)) {
      TrackingDataAvailableMessagePanel->Caption =
            "Tracking data available. Please select a point.";
      HideShowCharts(false);
      return;
   }

   HideShowCharts(true);
   DrawChartsTag(nTag);

   ////if (GPaintTool->isAllTrackDataValid()) {

      TrackingBoxSet trackingArray = GPaintTool->GetTrackingArrayRef();
      CVideoFrameList *frameList = GPaintTool->getSystemAPI()->getVideoFrameList();

      if (frameList != NULL) {

         PTimecode ptcBase = frameList->getInTimecode();
         PTimecode ptcFirst(ptcBase + trackingArray.getInFrame());
         PTimecode ptcLast(ptcBase + (trackingArray.getOutFrame() - 1));

         MTIostringstream os;
         os << "Tracking data available for range "
            << StringTrimLeft(ptcFirst.String()) << " - "
            << StringTrimLeft(ptcLast.String());
        TrackingDataAvailableMessagePanel->Caption = os.str().c_str();
	  }
   ////}
   ////else {
	  ////TrackingDataAvailableMessagePanel->Caption = "";
   ////}
}
//---------------------------------------------------------------------------

void TPaintToolForm::MarksHaveChanged(void *Sender)
{
   if (GPaintTool == NULL)
      return;

   EToolControlState toolControlState = GPaintTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      UpdateTrackingPointButtons();
      UpdateTrackingCharts();
      UpdateTrackingDataValidLight();
   }
}
//---------------------------------------------------------------------------

int  TPaintToolForm::LastSelectedTag(void)
{
   return  GPaintTool->GetLastSelectedTag();
}
//---------------------------------------------------------------------------

void TPaintToolForm::HideShowCharts(bool bShowCharts)
{
  if (bShowCharts && TrackingDataAvailableMessagePanel->Visible)
    {
	   TrackingDataAvailableMessagePanel->Visible = false;
    }
  else if ((!bShowCharts) && (!TrackingDataAvailableMessagePanel->Visible))
    {
       HorizontalChart->Series[0]->Clear();
       HorizontalChart->Series[1]->Clear();
       VerticalChart->Series[0]->Clear();
       VerticalChart->Series[1]->Clear();
	   TrackingDataAvailableMessagePanel->Visible = true;
    }
}
//---------------------------------------------------------------------------

double TPaintToolForm::FindExtentXForOnePoint(int iTag)
{
   TrackingBoxSet &TA = GPaintTool->GetTrackingArrayRef();
   TrackingBoxSet &RA = GPaintTool->GetRobustArrayRef();

   if (TA.getNumberOfTrackingBoxes() < 1)
      return 0.0;

   double dMin = 100000;
   double dMax = 0;
   TrackingBox TPS = TA.getTrackingBoxByTag(iTag);
   TrackingBox RPS = RA.getTrackingBoxByIndex(0);
   double dTRef = TPS[TPS.getStartFrame()].x;
   double dRRef = RPS[RPS.getStartFrame()].x;

   for (int i=TA.getInFrame(); i < TA.getOutFrame(); i++)
     if (TPS.isValidAtFrame(i))
       {
	  double value = TPS[i].x - dTRef - dRRef; // was TPS[0].x
	  if (value > dMax) dMax = value;
	  if (value < dMin) dMin = value;
       }

   dMin = ((int)dMin/MIN_CHART_EXTENT)*MIN_CHART_EXTENT;
   dMax = ((int)dMax/MIN_CHART_EXTENT + 1)*MIN_CHART_EXTENT;

   return (dMax-dMin);
}
//---------------------------------------------------------------------------

double TPaintToolForm::FindExtentYForOnePoint(int iTag)
{
   TrackingBoxSet &TA = GPaintTool->GetTrackingArrayRef();
   TrackingBoxSet &RA = GPaintTool->GetRobustArrayRef();

   if (TA.getNumberOfTrackingBoxes() < 1)
      return 0.0;

   double dMin = 100000;
   double dMax = 0;
   TrackingBox TPS = TA.getTrackingBoxByTag(iTag);
   TrackingBox RPS = RA.getTrackingBoxByIndex(0);
   double dTRef = TPS[TPS.getStartFrame()].y;
   double dRRef = RPS[RPS.getStartFrame()].y;

   for (int i=TA.getInFrame(); i < TA.getOutFrame(); i++)
     if (TPS.isValidAtFrame(i))
       {
          double value = TPS[i].y - dTRef - dRRef; // was: - TPS[0].y;
          if (value > dMax) dMax = value;
          if (value < dMin) dMin = value;
       }

   dMin = ((int)dMin/MIN_CHART_EXTENT)*MIN_CHART_EXTENT;
   dMax = ((int)dMax/MIN_CHART_EXTENT + 1)*MIN_CHART_EXTENT;

   return (dMax-dMin);
}

//---------------------------------------------------------------------------

void TPaintToolForm::DrawChartsTag(int Tag)
{
   TrackingBoxSet &TA = GPaintTool->GetTrackingArrayRef();
   TrackingBoxSet &RA = GPaintTool->GetRobustArrayRef();

   if (TA.getNumberOfTrackingBoxes() < 1)
      return;

   if (!TA.isValidTrackingBoxTag(Tag))
       return;

   TrackingBox _TrackingPoints = TA.getTrackingBoxByTag(Tag);
   TrackingBox _RobustPoints   = RA.getTrackingBoxByIndex(0);

   // To make the charts have the same scale, we must find the largest movement
   double fxExtent = FindExtentXForOnePoint(Tag);
   double fyExtent = FindExtentYForOnePoint(Tag);

   // Find the time code
   CVideoFrameList *frameList = GPaintTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   PTimecode ptcBase = frameList->getInTimecode();
   PTimecode ptc(ptcBase);

   double dMin = 0.0;
   double dMax = 0.0;
   double dRef;
   bool bFirstTimeFlag;

   HorizontalChart->Series[0]->Clear();
   bFirstTimeFlag = true;
   for (int i=TA.getInFrame(); i < TA.getOutFrame(); i++)
     {
        ptc = ptcBase + i;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].x;
        if (bFirstTimeFlag)
         {
           bFirstTimeFlag = false;
           dRef = d;
         }
        d -= dRef;
        HorizontalChart->Series[0]->AddXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   int iMinH = ((int) ( min<double>(dMin, -fxExtent / 2) - 12.5) / 10) * 10;
   int iMaxH = ((int) ( max<double>(dMax,  fxExtent / 2) + 12.5) / 10) * 10;

   // Do the vertical graph
   dMin = 0.0;
   dMax = 0.0;

   VerticalChart->Series[0]->Clear();
   bFirstTimeFlag = true;
   for (int i=TA.getInFrame(); i < TA.getOutFrame(); i++)
     {
        ptc = ptcBase + i;
        String TCS = StringTrimLeft(ptc.String()).c_str();
        double d =  _TrackingPoints[i].y;
        if (bFirstTimeFlag)
         {
           bFirstTimeFlag = false;
           dRef = d;
         }
        d -= dRef;
        VerticalChart->Series[0]->AddXY(i, d, TCS);

        if (d > dMax) dMax = d;
        if (d< dMin) dMin = d;
     }

   // Stupid way of setting axis to avoid min being greater than max
   int iMinV = ((int) ( min<double>(dMin, -fyExtent / 2) - 7.5) / 5) * 5;
   int iMaxV = ((int) ( max<double>(dMax,  fyExtent / 2) + 7.5) / 5) * 5;

   // Stupid way of setting axis to avoid min being greater than max
   int iMin = min<int>(iMinH, iMinV);
   int iMax = max<int>(iMaxH, iMaxV);
   HorizontalChart->LeftAxis->Minimum = min<long>(iMin,HorizontalChart->LeftAxis->Maximum) - 1;
   HorizontalChart->LeftAxis->Maximum = iMax;
   HorizontalChart->LeftAxis->Minimum = iMin - 1;  // -1 to show bottom axis label
   VerticalChart->LeftAxis->Minimum = min<long>(iMin,VerticalChart->LeftAxis->Maximum) - 1;
   VerticalChart->LeftAxis->Maximum = iMax;
   VerticalChart->LeftAxis->Minimum = iMin - 1;  // -1 to show bottom axis label
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::TrackingPointsChangedTimerTimer(TObject *Sender)
{
   TrackingPointsChangedTimer->Enabled = false;

   UpdateTrackingPointButtons();
   UpdateTrackingCharts();
   UpdateTrackingDataValidLight();
   // No! Because of stupid side effects, causes circularity
   //GPaintTool->getTrackingPointsEditor()->refreshTrackingPointsFrame();
   GPaintTool->getSystemAPI()->refreshFrameCached();   // better
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::OrigMixButtonClick(TObject *Sender)
{
   localOrigOnionSkinMode = ONIONSKIN_MODE_COLOR;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_COLOR, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::OrigDiffSkinButtonClick(TObject *Sender)
{
   localOrigOnionSkinMode = ONIONSKIN_MODE_POSNEG;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_POSNEG, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::OrigMixingTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_ORIG_MIX);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::OrigMixingTrackBarChange(TObject *Sender)
{
   auto mixingTrackBar = dynamic_cast<TTrackBar *>(Sender);
   if (!mixingTrackBar)
   {
      return;
   }

   int newTargetWgt         = mixingTrackBar->Position;
   int mixingRange          = mixingTrackBar->Max - mixingTrackBar->Min;
   GPaintTool->getSystemAPI()->setOnionSkinTargetWgt((double)newTargetWgt / (double)mixingRange);

   // refresh if you're in the right mode
   int majorState = GPaintTool->getMajorState();
   if (majorState == MAJORSTATE_SELECTING_ORIG_FRAME
   || majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME
   || majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE) {

      //GPaintTool->goToTargetFrame();
      GPaintTool->getSystemAPI()->refreshFrameCached();
   }
}

//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::CompareSpeedButtonClick(TObject *Sender)
{
   if (CompareSpeedButton->Down)
   {
      _oldPlaybackFilter = (_oldPlaybackFilter == PLAYBACK_FILTER_INVALID)
                           ? GPaintTool->getSystemAPI()->getPlaybackFilter()
                           : _oldPlaybackFilter;
      GPaintTool->getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
   }
   else if (_oldPlaybackFilter != PLAYBACK_FILTER_INVALID)
   {
      GPaintTool->getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
      _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
   }

   if (_oldPlaybackFilter != PLAYBACK_FILTER_INVALID)
   {
      GPaintTool->getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
      _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
   }

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipCompareButtonClick(TObject *Sender)
{
   if (AltClipCompareButton->Down)
   {
      _oldPlaybackFilter = (_oldPlaybackFilter == PLAYBACK_FILTER_INVALID)
                           ? GPaintTool->getSystemAPI()->getPlaybackFilter()
                           : _oldPlaybackFilter;
      GPaintTool->getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
   }
   else if (_oldPlaybackFilter != PLAYBACK_FILTER_INVALID)
   {
      GPaintTool->getSystemAPI()->setPlaybackFilter(_oldPlaybackFilter);
      _oldPlaybackFilter = PLAYBACK_FILTER_INVALID;
   }

   GPaintTool->CompareAltClipFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::GrainPresetsButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

	// CAREFUL! ugly off-by one index.
	const int GRAIN_PRESET_1_INDEX = 0;
	const int GRAIN_PRESET_2_INDEX = 1;
	auto grainPresetsBase = GrainPresetsProjectRadioButton->Checked ? GP_BASE_PROJECT : GP_BASE_CLIP;
	auto grainPresetsIndex = GrainPreset1Button->Down ? GRAIN_PRESET_1_INDEX : GRAIN_PRESET_2_INDEX;

   GPaintTool->GetGrainPresets().SetSelectedBase(grainPresetsBase);
   GPaintTool->GetGrainPresets().SetSelectedIndex(grainPresetsIndex);

   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::GrainUsePresetsCheckBoxClick(TObject *Sender)
{
   // Per Larry - always start preset mode at strength 50.
   // We'll set it back to zero when turning off "use presets" mode.
   // We'll also remember the last setting and restore that instead of blindly setting to 50.
   // But if the last setting was zero, use 50 instead.
   GPaintTool->SetGrainStrength(
         GrainUsePresetsCheckBox->Checked
         ? ((_grainPresetOverrideCurrentValue == 0)
             ? DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE
             : _grainPresetOverrideCurrentValue)
         : 0);

   // Let's auto-select Grain Override, so mouse wheel works
   GrainSpeedButton->Down = true;

   UpdateGrainPresetButtons();
   GPaintTool->GetGrainPresets().SetEnabled(GrainUsePresetsCheckBox->Checked);

   if (GrainUsePresetsCheckBox->Checked)
   {
      // This is an ugly hack. To make sure the grain generator is loaded with the preset values,
      // I have to set it to the wrong setting, then switch it back to the correct setting!
      auto correctBase = GPaintTool->GetGrainPresets().GetSelectedBase();
      auto wrongBase = (correctBase == GP_BASE_PROJECT) ? GP_BASE_CLIP : GP_BASE_PROJECT;
      GPaintTool->GetGrainPresets().SetSelectedBase(wrongBase);
      GPaintTool->GetGrainPresets().SetSelectedBase(correctBase);
   }

   LoseTheFocus();
}
//----------------------------------------------------------------------------

void TPaintToolForm::GrainPresetsChanged(void *Sender)
{
   UpdateGrainPresetButtons();
   CockTheDensityTimer();
}
//---------------------------------------------------------------------------

void TPaintToolForm::UpdateGrainPresetButtons()
{
   GrainUsePresetsCheckBox->Enabled        = GrainSpeedButton->Enabled;
	bool updatingGrainOverrideUsingPresets  = GrainUsePresetsCheckBox->Enabled && GrainUsePresetsCheckBox->Checked;
	GrainPresetsPanel->Enabled              = updatingGrainOverrideUsingPresets;
	GrainPresetsProjectRadioButton->Enabled = updatingGrainOverrideUsingPresets;
	GrainPresetsClipRadioButton->Enabled    = updatingGrainOverrideUsingPresets;
	GrainPreset1Button->Enabled             = updatingGrainOverrideUsingPresets;
	GrainPreset2Button->Enabled             = updatingGrainOverrideUsingPresets;
}
//---------------------------------------------------------------------------

string TPaintToolForm::BrowseForSourceClip()
{
   string retVal = "";

   // Stole this code from somewhere else. Don't know why the bin needs
   // to be saved/restored!
   string lastBin = (BinManagerForm == nullptr) ? "" : BinManagerForm->GetSelectedBin();

   // Hide the Project manager because we use the same windoe for clip selection!!
   // Cryptic otiginal comment: 3rd argument is false for fsNormal Bin Manager
   ShowBinManagerGUI(false, BM_MODE_SELECT_CLIP);

   if (!lastBin.empty())
   {
      BinManagerForm->SetSelectedBin(lastBin);
   }

   if (BinManagerForm->ShowModal() == mrOk)
   {
      StringList sl = BinManagerForm->SelectedClipNames();
      if (!sl.empty())
      {
         retVal = sl[0];
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

void TPaintToolForm::SetNewAlternateSourceClip(const string &newAlternateSourceClipName, int newAlternateSourceClipOffset)
{
   bool replacing = !_localImportFrameClipName.empty();
   _localImportFrameClipName = newAlternateSourceClipName;
   _localImportFrameClipOffset = newAlternateSourceClipOffset;

   if (_localImportFrameClipName.empty())
   {
      // We're actually clearing the clip.
      if (replacing)
      {
         StopRevealingFromAlternateSourceClip();
      }

      AltSourceClipNameLabel->Caption = "No alternate clip selected";
      _localImportFrameClipOffset = InvalidAlternateSourceClipOffsetValue;
      return;
   }

   // If Paint is in Alt Clip mode, tell the displayer about the new clip.
   if (GPaintTool->getMajorState() == MAJORSTATE_ALTCLIP_MODE_HOME)
   {
      StartRevealingFromAlternateSourceClip();
   }

   ClipIdentifier altClipId(_localImportFrameClipName);
   AltSourceClipNameLabel->Caption = altClipId.GetClipName().c_str();
}
//---------------------------------------------------------------------------

void TPaintToolForm::StartRevealingFromAlternateSourceClip()
{
	if (GPaintTool == NULL)
	{
		return;
	}

   if (!GPaintTool->getSystemAPI()->getImportFrameClipName().empty() || _localImportFrameClipName.empty())
   {
      // We're already in alt source clip mode, or no alt clip was selected.
      return;
   }

   UpdateTransform(0.0, 1.0, 1.0, 0.0, 0.0, 0.0);

   // QQQ Bad design. When we set the clip name in the Displayer, the Displayer
   // computes a default import frame offset, which we may or may not override.
   // Really the code to compute the default should be here, not in the Displayer.
   GPaintTool->getSystemAPI()->setImportFrameClipName(_localImportFrameClipName);
   if (_localImportFrameClipOffset == InvalidAlternateSourceClipOffsetValue)
   {
      _localImportFrameClipOffset = GPaintTool->getSystemAPI()->getImportFrameOffset();
   }
   else
   {
      GPaintTool->getSystemAPI()->setImportFrameOffset(_localImportFrameClipOffset);
   }

   GPaintTool->getSystemAPI()->clearPaintFrames();
   GPaintTool->getSystemAPI()->ClearTransform(false);
   GPaintTool->getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_IMPORT_FRAME_DIFFS);
}
//---------------------------------------------------------------------------

void TPaintToolForm::StopRevealingFromAlternateSourceClip()
{
	if (GPaintTool == NULL)
	{
		return;
	}

   if (GPaintTool->getSystemAPI()->getImportFrameClipName().empty())
   {
      // We're not in alt source clip mode.
      return;
   }

//   RedAltClipSourcePanel->Visible = false;
//   AltSourceClipNameLabel->Visible = false;

   if (GPaintTool->getSystemAPI()->getPlaybackFilter() == PLAYBACK_FILTER_IMPORT_FRAME_DIFFS)
   {
      GPaintTool->getSystemAPI()->setPlaybackFilter(PLAYBACK_FILTER_NONE);
   }

   // Save offset in case it was manually changed.
   _localImportFrameClipOffset = GPaintTool->getSystemAPI()->getImportFrameOffset();

   GPaintTool->getSystemAPI()->setImportFrameClipName("");
   GPaintTool->getSystemAPI()->setImportFrameOffset(0);
   GPaintTool->getSystemAPI()->clearPaintFrames();
}
//---------------------------------------------------------------------------

void TPaintToolForm::SaveAndClearImportFromAltSourceClipState()
{
   if (_doubleAltStateSavePreventerHackFlag)
   {
      return;
   }

   _savedDisplayerImportFrameStateFlag = !GPaintTool->getSystemAPI()->getImportFrameClipName().empty();
   if (!_savedDisplayerImportFrameStateFlag)
   {
      return;
   }

   StopRevealingFromAlternateSourceClip();
   _doubleAltStateSavePreventerHackFlag = true;
}
//---------------------------------------------------------------------------

void TPaintToolForm::RestoreImportFromAltSourceClipState()
{
   if (!_savedDisplayerImportFrameStateFlag)
   {
      return;
   }

   StartRevealingFromAlternateSourceClip();
   _doubleAltStateSavePreventerHackFlag = false;
}
//---------------------------------------------------------------------------

void SetAltSourceClipInfo(const string &altSourceClipName, int altSourceClipOffset)
{
    if (PaintToolForm == NULL)
    {
      return;
    }

    PaintToolForm->SetNewAlternateSourceClip(altSourceClipName, altSourceClipOffset);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipSelectClipButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   GPaintTool->AutoAcceptConditional();

   string newAlternateSourceClipName = BrowseForSourceClip();
   if (newAlternateSourceClipName.empty())
   {
      // Cancelled.
      return;
   }

   SetNewAlternateSourceClip(newAlternateSourceClipName);

   GPaintTool->WriteAlternateSourceClipNameToDesktopIni(_localImportFrameClipName);
   GPaintTool->WriteAlternateSourceClipOffsetToDesktopIni(_localImportFrameClipOffset);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipClearClipButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   GPaintTool->AutoAcceptConditional();

   SetNewAlternateSourceClip("");

   GPaintTool->WriteAlternateSourceClipNameToDesktopIni("");
   GPaintTool->WriteAlternateSourceClipOffsetToDesktopIni(InvalidAlternateSourceClipOffsetValue);
}
//---------------------------------------------------------------------------

void TPaintToolForm::EnterAlternateClipSourceMode()
{
   // Called when the "Alternate Clip" button os pressed.
	if (GPaintTool == NULL)
	{
		return;
	}

   GPaintTool->AutoAcceptConditional();

   if (_localImportFrameClipName.empty())
   {
      AltSourceClipNameLabel->Caption = "No alternate clip selected";
      return;
   }

   // If it's not our first time here, restore the old state.
   if (_savedDisplayerImportFrameStateFlag)
   {
      RestoreImportFromAltSourceClipState();
   }
   else
   {
      StartRevealingFromAlternateSourceClip();
   }
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipSyncTimecodeButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   GPaintTool->getSystemAPI()->syncImportFrameTimecode();
   GPaintTool->goToImportFrame();
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipSyncFrameIndexButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   GPaintTool->getSystemAPI()->setImportFrameOffset(0);
   GPaintTool->goToImportFrame();
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipSyncMarkInButtonClick(TObject *Sender)
{
	if (GPaintTool == NULL)
	{
		return;
	}

   int clipMarkIn, clipMarkOut;
   int importMarkIn, importMarkOut;
   auto currentClip = GPaintTool->getSystemAPI()->getClip();
   auto importClip = GPaintTool->getSystemAPI()->getImportClip();
   GPaintTool->getClipMarkIndices(currentClip, clipMarkIn, clipMarkOut);
   GPaintTool->getClipMarkIndices(importClip, importMarkIn, importMarkOut);

   clipMarkIn = max<int>(clipMarkIn, 0);
   importMarkIn = max<int>(importMarkIn, 0);

   GPaintTool->getSystemAPI()->setImportFrameOffset(importMarkIn - clipMarkIn);
   GPaintTool->goToImportFrame();
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipFrameTimecodeEditChange(TObject *Sender)
{
   int impfrm = ConvertAltClipFrameEditTextToFrameIndex();

   // xyzzy - need to check against range of other clip!
   bool warnTimecodeOutOfRange = false;
   AltClipOutOfRangePanel->Visible = warnTimecodeOutOfRange;
}
//---------------------------------------------------------------------------

int TPaintToolForm::ConvertAltClipFrameEditTextToFrameIndex()
{
   int impfrm = -1;

   // first get a timecode with the right fps
   CVideoFrameList *frameList;
   frameList = GPaintTool->getSystemAPI()->getImportClipVideoFrameList();
   if (frameList != NULL) {

      CTimecode imptim = frameList->getTimecodeForFrameIndex(0);

      // now set the timecode from the edit box string
      char impstr[32];
      strcpy(impstr, StringToStdString(AltClipFrameTimecodeEdit->Text).c_str());
      char *strpt = impstr;
      while (*strpt == ' ') strpt++;
      if (imptim.setTimeASCII(strpt)) {

         // get the new import frame index from the timecode
         impfrm = frameList->getFrameIndex(imptim);
      }
   }

   return impfrm;
}
//---------------------------------------------------------------------------

void TPaintToolForm::LoadAltClipFrameTimecode()
{
   int impfrm = ConvertAltClipFrameEditTextToFrameIndex();
   if (impfrm >= 0)
   {
      GPaintTool->getSystemAPI()->setImportFrameTimecode(impfrm);

      // if we're on the import frame, navigate to it
      if (GPaintTool->getSystemAPI()->getCaptureMode() == CAPTURE_MODE_ALTCLIP_IMPORT) {

         GPaintTool->goToImportFrame();
      }
      else {

         GPaintTool->goToTargetFrame();
      }

   }
   else
   {
      // (impfrm < 0)
      UpdateImportFrameTimecode(localImportFrame);
   }
}
//---------------------------------------------------------------------------

void TPaintToolForm::LoadAltClipFrameOffset()
{
      int value;

      if (ToInteger(AltClipFrameOffsetEdit->Text, &value))
		{
			GPaintTool->getSystemAPI()->setImportFrameOffset(value);

         // if we're on the import frame, navigate to it
         if (GPaintTool->getSystemAPI()->getCaptureMode() == CAPTURE_MODE_ALTCLIP_IMPORT)
         {
            GPaintTool->goToImportFrame();
         }
         else
         {
            GPaintTool->goToTargetFrame();
         }
      }
      else
      {
         UpdateImportFrameOffset(localImportFrameOffset);
      }
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipFrameTimecodeEditExit(TObject *Sender)
{
   LoadAltClipFrameTimecode();
}
//---------------------------------------------------------------------------


void __fastcall TPaintToolForm::AltClipFrameOffsetEditExit(TObject *Sender)
{
   LoadAltClipFrameOffset();
}
//---------------------------------------------------------------------------
void __fastcall TPaintToolForm::AltClipDiffThresholdTrackBarChange(TObject *Sender)
{
	int sliderValue = AltClipDiffThresholdTrackBar->Position;
	MTIassert(sliderValue >= 0 && sliderValue <= 100);
	if (sliderValue == 0)
	{
		AltClipDiffTresholdValueLabel->Caption = "0%";
	}
	else if (sliderValue == 100)
	{
		AltClipDiffTresholdValueLabel->Caption = "10%";
	}
	else
	{
		int integer = sliderValue / 10;
		int tenths = sliderValue % 10;
		MTIostringstream os;
		os << integer << '.' << tenths << '%';
		AltClipDiffTresholdValueLabel->Caption = os.str().c_str();
	}

	AltClipDiffThresholdTimer->Interval = 500;
	AltClipDiffThresholdTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipDiffThresholdTimerTimer(TObject *Sender)
{
	AltClipDiffThresholdTimer->Enabled = false;
	int sliderValue = AltClipDiffThresholdTrackBar->Position;
	float diffThreshold = sliderValue / 1000.f;
	GPaintTool->getSystemAPI()->setAltClipDiffThreshold(diffThreshold);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipMixingTrackBarChange(TObject *Sender)
{
   auto mixingTrackBar = dynamic_cast<TTrackBar *>(Sender);
   if (!mixingTrackBar)
   {
      return;
   }

   int newTargetWgt         = mixingTrackBar->Position;
   int mixingRange          = mixingTrackBar->Max - mixingTrackBar->Min;
   localAltClipOnionSkinTargetWgt = (double)newTargetWgt / (double)mixingRange;
   GPaintTool->getSystemAPI()->setOnionSkinTargetWgt(localAltClipOnionSkinTargetWgt);

   // refresh if you're in the right mode
   int majorState = GPaintTool->getMajorState();
   if (majorState == MAJORSTATE_ALTCLIP_MODE_HOME
   || majorState == MAJORSTATE_SELECTING_ALTCLIP_FRAME
   || majorState == MAJORSTATE_PAINT_ALTCLIP_COMPARE
   || majorState == MAJORSTATE_PAINT_ALTCLIP_PIXELS)
   {
      //GPaintTool->goToTargetFrame();
      GPaintTool->getSystemAPI()->refreshFrameCached();
   }

}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipMixingTrackBarEnter(TObject *Sender)
{
   setMouseWheelState(MOUSE_WHEEL_ADJUST_ALT_CLIP_MIX);
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipDiffButtonClick(TObject *Sender)
{
   localAltClipOnionSkinMode = ONIONSKIN_MODE_POSNEG;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_POSNEG, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------

void __fastcall TPaintToolForm::AltClipMixButtonClick(TObject *Sender)
{
   localAltClipOnionSkinMode = ONIONSKIN_MODE_COLOR;

   GPaintTool->SelectOnionSkinMode(ONIONSKIN_MODE_COLOR, false);

   // to target frame & refresh
   GPaintTool->goToTargetFrame();

   // set focus to main window
   LoseTheFocus();
}
//---------------------------------------------------------------------------
