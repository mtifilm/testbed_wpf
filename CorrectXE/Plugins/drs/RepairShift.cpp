/*
	Name:	RepairShift.cpp
	By:	Kevin Manbeck
	Date:	Mar 19, 2002

	The RepairShift class is derived from the Repair class.  It is
	reponsible for shifting a region of interest.
*/
#include "IniFile.h"
#include "MTIio.h"
#include "MTImalloc.h"
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <math.h>
#include <vector>

#include "RepairShift.h"
#include "mt_math.h"
#include "cpmp_err.h"

CRepairShift::CRepairShift():CRepair()
{
}  /* CRepairShift */

CRepairShift::~CRepairShift()
{
}  /* ~CRepairShift */

void CRepairShift::SetShift(float fRowShiftArg, float fColShiftArg)
{
  fRowShift = fRowShiftArg;
  fColShift = fColShiftArg;

  // round the floating point shift
  lRowShift = floor(fRowShift);
  lColShift = floor(fColShift);

  return;
}  /* SetShift */

void CRepairShift::GetShift(long *lpRowShift, long *lpColShift)
/*
	Sub pixel shifts are allowed, however the number of modified
	pixels in uspResult will be equal to the number of pixels 
	in the RepairMask.

	Upon return, the values of lpRowShift and lpColShift are set to
	indicate how RepairMask has been shifted.
*/
{
  *lpRowShift = lRowShift;
  *lpColShift = lColShift;
  return;
}  /* GetShift */


int CRepairShift::PerformShift ()
/*
	This function peforms the following steps:
	1.  Put back original values by copying from uspData[lRepairFrame] to
		uspResult.
	2.  Create a new fix by copying from uspData[1 - lRepairFrame] to
		uspResult.
*/
{
  long lRow, lCol, lRowCol, lRowColCom,lComponent,
	laNeiRow[2], laNeiCol[2], lRowStart, lRowStop, lRowStep,
	lColStart, lColStop, lColStep, lR, lC;
  float faWeiRow[2], faWeiCol[2], faNumerator[MAX_COMPONENT],
		fDenominator;
  long lRowColShift, lRowColNei, lRowColComNei, lRowEdge, lColEdge,
  	lRowColComShift;

	// PerformShift needs two frames:  lRepairFrame is the UNPROCESSED
	// image, the other is the PROCESSED image.

  if (lNFrame != 2)
    return -1;

  if (lRepairFrame < 0 || lRepairFrame >= lNFrame)
    return -1;

	// make certain uspResult and uspData are different
  if (uspResult == uspData[0] || uspResult == uspData[1])
    return -1;

	// copy from uspData to uspResult
  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
  for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
   {
    lRowCol = lRow * lPitch + lCol;
    lRowColCom = lRowCol * lNCom;
    for (lComponent = 0; lComponent < lNCom; lComponent++)
     {
      uspResult[lRowColCom] = uspData[lRepairFrame][lRowColCom];
      lRowColCom++;
     }
    uspRepairMask[lRowCol] &= ~(RM_MODIFIED | RM_SHIFT_EDGE);
   }

/*
    Make a list of pixels that will be modified.  

    When doing sub-pixel moves, we take averages of 2x2 neighborhoods.
    This means the right edge and bottom edge do not blend into the 
    background properly unless we set the right edge and the bottom
    edge to the original image values.
    
    The values in uspData[1-RepairFrame] in the BORDER region are randomized.
    Don't use BORDER values to initialize uspResult.
*/

  // run index backwards to look for right and bottom edges
  for (lRow = lRegionLRCRow-1; lRow >= lRegionULCRow; lRow--)
  for (lCol = lRegionLRCCol-1; lCol >= lRegionULCCol; lCol--)
   {
    lRowCol = lRow * lPitch + lCol;
    if (uspRepairMask[lRowCol] & RM_SELECTED)
     {
      // only modify pixel if it falls within the region
      if (
		(lRowShift + lRow) >= lRegionULCRow &&
		(lRowShift + lRow) < lRegionLRCRow &&
		(lColShift + lCol) >= lRegionULCCol &&
		(lColShift + lCol) < lRegionLRCCol
         )
       {
        lRowColShift = (lRowShift + lRow) * lPitch + (lColShift + lCol);

	uspRepairMask[lRowColShift] |= RM_MODIFIED;

        // if pixel to right or below is in the region and not modified,
        // this is an edge pixel
        lRowEdge = lRow + lRowShift + 1;
        lColEdge = lCol + lColShift + 1;
        if (
	  ( (lRowEdge < lRegionLRCRow) &&
	    ((uspRepairMask[lRowColShift+lPitch] & RM_MODIFIED) == 0) ) ||
	  ( (lColEdge < lRegionLRCCol) &&
	    ((uspRepairMask[lRowColShift+1] & RM_MODIFIED) == 0) )
  	   )
         {
          uspRepairMask[lRowColShift] |= RM_SHIFT_EDGE;
         }
       }
     }
   }

  // copy from uspData[1-lRepairFrame] to uspResult
  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
  for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
   {
    lRowColShift = lRow * lPitch + lCol;
    if (uspRepairMask[lRowColShift] & RM_MODIFIED)
     {
      lRowCol = (lRow - lRowShift) * lPitch + (lCol - lColShift);

      lRowColCom = lRowCol * lNCom;
      lRowColComShift = lRowColShift * lNCom;

      // do not modify the value of edge pixels.
      if ((uspRepairMask[lRowColShift] & RM_SHIFT_EDGE) == 0)
       {
        // initialize with processed image
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          uspResult[lRowColComShift] = uspData[1-lRepairFrame][lRowColCom];
          lRowColCom++;
          lRowColComShift++;
         }
       }
     }
   }

/*
	Now apply the sub-pixel part of the shift.

	Each pixel in the region will be replaced with a weighted
	average of a 2x2 neighborhood.

	We must loop through the region in the correct order so that
	the changes to uspResult to not become iterative
*/

		// determine the neighbors and weights
  faWeiRow[0] = fRowShift - (float)lRowShift;

  // use pixel above ( row - 1 )
  laNeiRow[0] = -1;

	// step rows from bottom to top
  lRowStart = lRegionLRCRow-1;
  lRowStop = lRegionULCRow-1;
  lRowStep = -1;

  faWeiRow[1] = 1. - faWeiRow[0];
  laNeiRow[1] = 0;

  faWeiCol[0] = fColShift - (float)lColShift;

  // use pixel to left( col - 1 )
  laNeiCol[0] = -1;

  // step cols from right to left
  lColStart = lRegionLRCCol-1;
  lColStop = lRegionULCCol-1;
  lColStep = -1;

  faWeiCol[1] = 1. - faWeiCol[0];
  laNeiCol[1] = 0;

	// run through all pixels and adjust
  for (lRow = lRowStart; lRow != lRowStop; lRow += lRowStep)
  for (lCol = lColStart; lCol != lColStop; lCol += lColStep)
   {
    lRowColShift = lRow * lPitch + lCol;
    if (uspRepairMask[lRowColShift] & RM_MODIFIED)
     {
      for (lComponent = 0; lComponent < lNCom; lComponent++)
       {
        faNumerator[lComponent] = 0.;
       }
      fDenominator = 0.;

		// run through the 2x2 neighborhood
      for (lR = 0; lR < 2; lR++)
      for (lC = 0; lC < 2; lC++)
       {
        if (
		(lRow + laNeiRow[lR]) >= lRegionULCRow &&
		(lRow + laNeiRow[lR]) < lRegionLRCRow &&
		(lCol + laNeiCol[lC]) >= lRegionULCCol &&
		(lCol + laNeiCol[lC]) < lRegionLRCCol
           )
         {
          lRowColNei = (lRow+laNeiRow[lR]) * lPitch + (lCol+laNeiCol[lC]);
          lRowColComNei = lRowColNei * lNCom;
          float fWei = faWeiRow[lR] * faWeiCol[lC];
	  // allow all MODIFIED but not SHIFT_EDGE pixels to contribute to the
	  // weighted average
	  if (
		(laNeiRow[lR] == 0 && laNeiCol[lC] == 0) ||
	  	( (uspRepairMask[lRowColNei] & (RM_MODIFIED | RM_SHIFT_EDGE))
				== RM_MODIFIED )
	     )
	   {
            for (lComponent = 0; lComponent < lNCom; lComponent++)
             {
              faNumerator[lComponent] += (float)uspResult[lRowColComNei] * fWei;
              lRowColComNei++;
             }
	    fDenominator += fWei;
           }
         }
       }

	// replace the value of uspResult
      if (fDenominator)
       {
        lRowColComShift = lRowColShift * lNCom;
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          uspResult[lRowColComShift] = ((faNumerator[lComponent] /
              		fDenominator) + 0.5);
          lRowColComShift++;
         }
       }
     }
   }

  // conform image to single field constraint
  PerformFieldRepeat();

  return 0;
}  /* PerformShift */
