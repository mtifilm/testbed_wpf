//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DRSTool.h"
#include "UnitPreferences.h"
#include "ImageFormat3.h"
#include "MTIDialogs.h"
#include "ToolSystemInterface.h"


#define LOAD_FILE 0
#define LOAD_ACTIVE 1
#define LOAD_FACTORY 2

//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma link "SpinEditFrameUnit"
#pragma resource "*.dfm"
TFormDRSPreferences *FormDRSPreferences;

#define MAX_IMAGE_RESOLUTION_HANDLED IF_IMAGE_RESOLUTION_4K_DATA

//---------------------------------------------------------------------------
__fastcall TFormDRSPreferences::TFormDRSPreferences(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::FormShow(TObject *Sender)
{
   // Ugly global state flags
   bUpdatingGUI = false;   // Stupid callback inhibitor flag

   // Recall which page we were on last time we exited this dialog
   CIniFile *ini = CreateIniFile(CPMPIniFileName("DRS"), true);  // true=R/O
   if (ini != NULL)
   {
      int iPage = ini->ReadInteger ("Preferences", "Page", 0);
      if (iPage < 0 || iPage >= PageControl->PageCount)
         iPage = 0;   // Sanity
      PageControl->ActivePage = PageControl->Pages[iPage];

      PreloadInputCheckBox->Checked = ini->ReadBool("Preferences", "PreloadInput", true);
      AutoAcceptCheckBox->Checked = ini->ReadBool("Preferences", "AutoAccept", true);

      DeleteIniFile(ini);
   }

   // Load up initial values for active and factory settings
   for (int iRes = 0; iRes < IF_IMAGE_RESOLUTION_COUNT; ++iRes)
   {
   // NOTE!!! It used to be true that the 'ACTIVE' and 'FILE' versions of
   // the parameters were allowed to be out-of-sync, but now we TRY LIKE HELL
   // to keep then 'N-SYNC!!!
      dipaCurrent[iRes] = GDRSTool->DRSParam.getActiveIniParameters(iRes);
      dipaOrig[iRes] = dipaCurrent[iRes];
      dipaFactory[iRes] = GDRSTool->DRSParam.getFactoryIniParameters(iRes);
   }

   // Initially show values corresponding to the resolution of the loaded clip
   // EVIL! The resolution indexes MUST MATCH between CImageInfo and the radio
   // button GUI component!!! gaag
   iCurrentRes = GDRSTool->GetImageResolution ();
   if (iCurrentRes < 0 || iCurrentRes > MAX_IMAGE_RESOLUTION_HANDLED)
      iCurrentRes = MAX_IMAGE_RESOLUTION_HANDLED;
   ImageResolutionRadioGroup->ItemIndex = iCurrentRes;

   // Initialize state of the GUI
   UpdateGUI();

   SetSpinEditHooks();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::FormHide(TObject *Sender)
{
   RemoveSpinEditHooks();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   CanClose = CheckOkToCloseWithoutSavingChanges();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   // save current page to ini file
   CIniFile *ini = CreateIniFile (CPMPIniFileName ("DRS"));
   if (ini != 0)
   {
      ini->WriteInteger ("Preferences", "Page", PageControl->TabIndex);
      ini->WriteBool ("Preferences", "AutoAccept", AutoAcceptCheckBox->Checked);
      ini->WriteBool ("Preferences", "PreloadInput", PreloadInputCheckBox->Checked);
      DeleteIniFile(ini);
   }
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TFormDRSPreferences::UpdateGUI(void)
{
   char caMessage[16];
   SDRSIniParameters &dipGUI = dipaCurrent[iCurrentRes];
   bUpdatingGUI = true;

   // motion values
   MotionLowSpinEdit->SetValue(dipGUI.lMotionOffsetLow);
   MotionHighSpinEdit->SetValue(dipGUI.lMotionOffsetHigh);
   ImportLowSpinEdit->SetValue(dipGUI.lImportOffsetLow);
   ImportHighSpinEdit->SetValue(dipGUI.lImportOffsetHigh);

   // quick select values
   switch (dipGUI.iQuickSelect)
   {
      case 0:
         QSRect0RadioButton->Checked = true;
         break;
      case 1:
         QSRect1RadioButton->Checked = true;
         break;
      case 2:
         QSRect2RadioButton->Checked = true;
         break;
      case 3:
         QSEllipse0RadioButton->Checked = true;
         break;
      case 4:
         QSEllipse1RadioButton->Checked = true;
         break;
      case 5:
         QSEllipse2RadioButton->Checked = true;
         break;
      default:
         QSRect0RadioButton->Checked = true;
         dipGUI.iQuickSelect = 0;
         break;
   }

   QSRect0SpinEdit->SetValue(dipGUI.iQSRect0Size);
   QSRect1SpinEdit->SetValue(dipGUI.iQSRect1Size);
   QSRect2SpinEdit->SetValue(dipGUI.iQSRect2Size);
   QSEllipse0SpinEdit->SetValue(dipGUI.iQSEllipse0Size);
   QSEllipse1SpinEdit->SetValue(dipGUI.iQSEllipse1Size);
   QSEllipse2SpinEdit->SetValue(dipGUI.iQSEllipse2Size);

   // sizing values
   PaddingLowSpinEdit->SetValue(dipGUI.lPaddingAmountLow);
   PaddingHighSpinEdit->SetValue(dipGUI.lPaddingAmountHigh);
   GrainSizeSpinEdit->SetValue(dipGUI.lGrainSize);
   BorderMaxSpinEdit->SetValue(dipGUI.lBorderSize);
   BorderMinSpinEdit->SetValue(dipGUI.lBorderMin);
   BorderPercentSpinEdit->SetValue(dipGUI.lBorderPercent);
   bUpdatingGUI = false;

   UpdateButtons();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::UpdateButtons(void)
{
  // Cancel is only available if something changed
  CancelButton->Enabled = DidAnySettingsChange();

  // GetFactorySettingsButton applies only to the current resolution,
  // and is available only if the settings for the presently selected
  // resolution do not match the factory settings for that resolution.

  GetFactorySettingsButton->Enabled =
        AreSettingsDifferentFromFactoryValues(iCurrentRes);

  switch (iCurrentRes)
  {
     case 0:
        GetFactorySettingsButton->Caption = "Get SD Factory Settings";
        break;
     case 1:
        GetFactorySettingsButton->Caption = "Get HD Factory Settings";
        break;
     case 2:
        GetFactorySettingsButton->Caption = "Get 2K Factory Settings";
        break;
     default:
     case 3:
        GetFactorySettingsButton->Caption = "Get 4K Factory Settings";
        break;
  }
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

bool TFormDRSPreferences::AreSettingsDifferentFromFactoryValues(int iRes)
{
   return !AreSettingsTheSame(dipaCurrent[iRes], dipaFactory[iRes]);
}
//---------------------------------------------------------------------------

bool TFormDRSPreferences::DidAnySettingsChange(void)
{
   bool bRet = false;

   for (int iRes = 0; (!bRet) && (iRes <= MAX_IMAGE_RESOLUTION_HANDLED); ++iRes)
      bRet = DidSettingsChangeForResolution(iRes);

   return bRet;
}
//---------------------------------------------------------------------------

bool TFormDRSPreferences::DidSettingsChangeForResolution(int iRes)
{
   return !AreSettingsTheSame(dipaCurrent[iRes], dipaOrig[iRes]);
}
//---------------------------------------------------------------------------

bool TFormDRSPreferences::AreSettingsTheSame(
    const SDRSIniParameters &dipA,
    const SDRSIniParameters &dipB)
{
   bool bRet = true;

   // Compare motion information
   if (dipA.lMotionOffsetLow  != dipB.lMotionOffsetLow  ||
       dipA.lMotionOffsetHigh != dipB.lMotionOffsetHigh ||
       dipA.lImportOffsetLow  != dipB.lImportOffsetLow  ||
       dipA.lImportOffsetHigh != dipB.lImportOffsetHigh )
     bRet = false;

   // Compare Quick Select
   if (dipA.iQuickSelect != dipB.iQuickSelect ||
       dipA.iQSRect0Size != dipB.iQSRect0Size ||
       dipA.iQSRect1Size != dipB.iQSRect1Size ||
       dipA.iQSRect2Size != dipB.iQSRect2Size ||
       dipA.iQSEllipse0Size != dipB.iQSEllipse0Size ||
       dipA.iQSEllipse1Size != dipB.iQSEllipse1Size ||
       dipA.iQSEllipse2Size != dipB.iQSEllipse2Size )
     bRet = false;

   // Compare sizing information
   if (dipA.lPaddingAmountLow  != dipB.lPaddingAmountLow  ||
       dipA.lPaddingAmountHigh != dipB.lPaddingAmountHigh ||
       dipA.lGrainSize     != dipB.lGrainSize  ||
       dipA.lBorderSize    != dipB.lBorderSize ||
       dipA.lBorderMin     != dipB.lBorderMin  ||
       dipA.lBorderPercent != dipB.lBorderPercent )
     bRet = false;

   return bRet;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TFormDRSPreferences::MotionChange(void)
{
   if (bUpdatingGUI)
      return;

   SDRSIniParameters &dipGUI = dipaCurrent[iCurrentRes];

   dipGUI.lMotionOffsetLow = MotionLowSpinEdit->GetValue();
   dipGUI.lMotionOffsetHigh = MotionHighSpinEdit->GetValue();

   // No more offset on replace
   //sscanf(ImportLowSpinEdit->Text.c_str(), "%d", &dipGUI.lImportOffsetLow);
   //sscanf(ImportHighSpinEdit->Text.c_str(), "%d", &dipGUI.lImportOffsetHigh);
   dipGUI.lImportOffsetLow = 0;
   dipGUI.lImportOffsetHigh = 0;

   UpdateButtons();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::QuickSelectChange(void)
{
   if (bUpdatingGUI)
      return;

   SDRSIniParameters &dipGUI = dipaCurrent[iCurrentRes];

   dipGUI.iQSRect0Size = QSRect0SpinEdit->GetValue();
   dipGUI.iQSRect1Size = QSRect1SpinEdit->GetValue();
   dipGUI.iQSRect2Size = QSRect2SpinEdit->GetValue();
   dipGUI.iQSEllipse0Size = QSEllipse0SpinEdit->GetValue();
   dipGUI.iQSEllipse1Size = QSEllipse1SpinEdit->GetValue();
   dipGUI.iQSEllipse2Size = QSEllipse2SpinEdit->GetValue();

   if (QSRect0RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 0;
   }
   else if (QSRect1RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 1;
   }
   else if (QSRect2RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 2;
   }
   else if (QSEllipse0RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 3;
   }
   else if (QSEllipse1RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 4;
   }
   else if (QSEllipse2RadioButton->Checked)
   {
      dipGUI.iQuickSelect = 5;
   }
   else
   {
      dipGUI.iQuickSelect = 0;
   }

   UpdateButtons ();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::SizingChange(TSpinEditFrame *spinEdit)
{
   if (bUpdatingGUI)
      return;
      
   SDRSIniParameters &dipGUI = dipaCurrent[iCurrentRes];

   dipGUI.lPaddingAmountLow = PaddingLowSpinEdit->GetValue();
   dipGUI.lPaddingAmountHigh = PaddingHighSpinEdit->GetValue();
   dipGUI.lGrainSize = GrainSizeSpinEdit->GetValue();
   dipGUI.lBorderSize = BorderMaxSpinEdit->GetValue();
   dipGUI.lBorderMin = BorderMinSpinEdit->GetValue();
   dipGUI.lBorderPercent = BorderPercentSpinEdit->GetValue();

   // If we're changing MIN and it now exceeds the MAX, change the MAX
   if (spinEdit == BorderMinSpinEdit && dipGUI.lBorderSize < dipGUI.lBorderMin)
   {
      dipGUI.lBorderSize = dipGUI.lBorderMin;
      bUpdatingGUI = true;
      BorderMaxSpinEdit->SetValue(dipGUI.lBorderSize);
      bUpdatingGUI = false;
   }

   // If we're changing MAX and it now is less than the MIN, change the MIN
   if (spinEdit == BorderMinSpinEdit && dipGUI.lBorderMin > dipGUI.lBorderSize)
   {
      dipGUI.lBorderMin = dipGUI.lBorderSize;
      bUpdatingGUI = true;
      BorderMinSpinEdit->SetValue(dipGUI.lBorderMin);
      bUpdatingGUI = false;
   }

   UpdateButtons();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

bool TFormDRSPreferences::CheckOkToCloseWithoutSavingChanges(void)
{
   bool retVal = true;

   // if some values have changed, ask operator to save them
   if (DidAnySettingsChange())
   {
      int iRet = _MTIWarningDialog(Handle,
                        "All of the changes you have made will be lost!    \n"
	                    "OK to proceed?");

      if (iRet != MTI_DLG_OK)
         retVal = false;
   }

   return retVal;
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::SaveAllValues(void)
{
   if (GDRSTool == NULL)
      return;

   if (!DidAnySettingsChange())
      return;

   // Copy out the all the current settings
   for (int iRes = 0; iRes <= MAX_IMAGE_RESOLUTION_HANDLED; iRes++)
   {
      GDRSTool->DRSParam.setFileIniParameters(iRes, dipaCurrent[iRes]);
      GDRSTool->DRSParam.setActiveIniParameters(iRes, dipaCurrent[iRes]);

      // Reset "original" values so DidSettingsChange will work correctly
      dipaOrig[iRes] = dipaCurrent[iRes];
   }

   // write out the new values
   GDRSTool->DRSParam.writeIniParameters();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::OKButtonClick(TObject *Sender)
{
   SaveAllValues();
   Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::CancelButtonClick(TObject *Sender)
{
   // NOTE: CheckOKToCloseWithoutSavingChanges() will get called in the
   //       FormCloseQuery() callback, so Close could be aborted

   Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::GetFactorySettingsButtonClick(
   TObject *Sender)
{
   int iRet = _MTIWarningDialog(Handle,
                 "This will restore factory values for ALL settings for   \n"
                 "this resolution (not just the settings on this page)    \n"
                 "OK to proceed?");

   if (iRet != MTI_DLG_OK)
      return;

   dipaCurrent[iCurrentRes] = dipaFactory[iCurrentRes];

   UpdateGUI();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::ImageResolutionRadioGroupClick(
   TObject *Sender)
{
   iCurrentRes = ImageResolutionRadioGroup->ItemIndex;

   // Really, this would be an error!
   if (iCurrentRes < 0 || iCurrentRes > MAX_IMAGE_RESOLUTION_HANDLED)
      iCurrentRes = MAX_IMAGE_RESOLUTION_HANDLED;

   UpdateGUI();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::MotionSpinEditChange(void *Sender)
{
   MotionChange();
}
//---------------------------------------------------------------------------

void __fastcall TFormDRSPreferences::QSRadioButtonClick(TObject *Sender)
{
   QuickSelectChange ();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::QSSpinEditChange(void *Sender)
{
   QuickSelectChange ();
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::SizingSpinEditChange(void *Sender)
{
   TSpinEditFrame *spinEdit = (TSpinEditFrame *) Sender;
   SizingChange (spinEdit);
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::SetSpinEditHooks()
{
   SET_CBHOOK(MotionSpinEditChange, MotionLowSpinEdit->SpinEditValueChange);
   SET_CBHOOK(MotionSpinEditChange, MotionHighSpinEdit->SpinEditValueChange);
   SET_CBHOOK(MotionSpinEditChange, ImportHighSpinEdit->SpinEditValueChange);
   SET_CBHOOK(MotionSpinEditChange, ImportLowSpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSRect0SpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSRect1SpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSRect2SpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSEllipse2SpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSEllipse1SpinEdit->SpinEditValueChange);
   SET_CBHOOK(QSSpinEditChange, QSEllipse0SpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, PaddingLowSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, PaddingHighSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, GrainSizeSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, BorderMinSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, BorderMaxSpinEdit->SpinEditValueChange);
   SET_CBHOOK(SizingSpinEditChange, BorderPercentSpinEdit->SpinEditValueChange);
}
//---------------------------------------------------------------------------

void TFormDRSPreferences::RemoveSpinEditHooks()
{
   REMOVE_CBHOOK(MotionSpinEditChange, MotionLowSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(MotionSpinEditChange, MotionHighSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(MotionSpinEditChange, ImportHighSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(MotionSpinEditChange, ImportLowSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSRect0SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSRect1SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSRect2SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSEllipse2SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSEllipse1SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(QSSpinEditChange, QSEllipse0SpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, PaddingLowSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, PaddingHighSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, GrainSizeSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, BorderMinSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, BorderMaxSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(SizingSpinEditChange, BorderPercentSpinEdit->SpinEditValueChange);
}
//---------------------------------------------------------------------------

