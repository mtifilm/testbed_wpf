#ifndef DRSToolH
#define DRSToolH

#include "DRSParameters.h"
#include "GrainPresets.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "MTIio.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "RepairDRS.h"
#include "RepairShift.h"
#include "SafeClasses.h"
#include "ThreadLocker.h"    // Foir CSpinLock
#include "ToolObject.h"

#include <list>
using std::list;

//////////////////////////////////////////////////////////////////////

// DRS Tool Number (16-bit number that uniquely identifies this tool)
#define DRS_TOOL_NUMBER    2

// Values that are currently hardcoded, but need to be made
// more general eventually.
#define ORIGINAL_RECT_BORDER 3 // size of larger rectangle around original fix

// DRS Tool Command Numbers
#define DRS_CMD_INVALID                     -1
//efine DRS_CMD_??                           1  // available
#define DRS_CMD_REV_PLAY_OR_REJECT_FIX       2
#define DRS_CMD_START_SELECT_OR_TOGGLE_FIX   3
#define DRS_CMD_END_SELECT                   4
#define DRS_CMD_SELECT_ADD_DN                5
#define DRS_CMD_SELECT_ADD_UP                6
#define DRS_CMD_SELECT_CTRL_DN               7
#define DRS_CMD_SELECT_CTRL_UP               8
#define DRS_CMD_FWD_PLAY_OR_ACCEPT_FIX       9
#define DRS_CMD_QUICK_SELECT                 10
#define DRS_CMD_OVERRIDE_UP                  11
#define DRS_CMD_OVERRIDE_DOWN                12
#define DRS_CMD_OVERRIDE_LEFT                13
#define DRS_CMD_OVERRIDE_RIGHT               14
#define DRS_CMD_OVERRIDE_UP_LARGE            15
#define DRS_CMD_OVERRIDE_DOWN_LARGE          16
#define DRS_CMD_OVERRIDE_LEFT_LARGE          17
#define DRS_CMD_OVERRIDE_RIGHT_LARGE         18
#define DRS_CMD_OVERRIDE_UP_SMALL            19
#define DRS_CMD_OVERRIDE_DOWN_SMALL          20
#define DRS_CMD_OVERRIDE_LEFT_SMALL          21
#define DRS_CMD_OVERRIDE_RIGHT_SMALL         22
#define DRS_CMD_LOW_MOTION                   23
#define DRS_CMD_HIGH_MOTION                  24
#define DRS_CMD_HARD_MOTION                  25
////#define DRS_CMD_ADJUST_COLOR                 26
#define DRS_CMD_NO_MOTION                    27
#define DRS_CMD_NO_MOTION_PAST               28
#define DRS_CMD_NO_MOTION_FUTURE             29
#define DRS_CMD_NO_MOTION_IMPORT             30
#define DRS_CMD_COLOR_BUMP                   31
#define DRS_CMD_COLOR_BUMP_PAST              32
#define DRS_CMD_COLOR_BUMP_FUTURE            33
#define DRS_CMD_COLOR_BUMP_KEY               34
#define DRS_CMD_MASK_IMAGE                   35
#define DRS_CMD_LOW_MOTION_PAST              36
#define DRS_CMD_HIGH_MOTION_PAST             37
#define DRS_CMD_LOW_MOTION_FUTURE            38
#define DRS_CMD_HIGH_MOTION_FUTURE           39
#define DRS_CMD_LOW_MOTION_IMPORT            40
#define DRS_CMD_HIGH_MOTION_IMPORT           41
#define DRS_CMD_HARD_MOTION_EDGE             42
#define DRS_CMD_LOW_MOTION_KEY               43
#define DRS_CMD_HIGH_MOTION_KEY              44
#define DRS_CMD_HARD_MOTION_KEY              45
#define DRS_CMD_REPLACE_NONE_KEY             46
#define DRS_CMD_REPLACE_LOW_KEY              47
#define DRS_CMD_REPLACE_HIGH_KEY             48
#define DRS_CMD_COLOR_BUMP_MONO              49
#define DRS_CMD_COLOR_BUMP_PAST_MONO         50
#define DRS_CMD_COLOR_BUMP_FUTURE_MONO       51
#define DRS_CMD_RECTANGLE_MODE               52
#define DRS_CMD_LASSO_MODE                   53
#define DRS_CMD_BEZIER_MODE                  54
#define DRS_CMD_SELECT_SHAPE_KEY             55
#define DRS_CMD_SWITCH_SELECT_SHAPE          56
#define DRS_CMD_SELECT_DELETE                57
//efine DRS_CMD_??                           58  // available
#define DRS_CMD_ALL_STOP                     59
#define DRS_CMD_CHANGE_BORDER                60
#define DRS_CMD_MOVE_OVER                    61
#define DRS_CMD_MOVE_UNDER                   62
//efine DRS_CMD_??                           63  // available
#define DRS_CMD_CHANGE_GRAIN                 64
#define DRS_CMD_ROTATE_OVER                  65
#define DRS_CMD_ROTATE_UNDER                 66
//efine DRS_CMD_??                           67  // available
#define DRS_CMD_DITHER_BORDER                68
#define DRS_CMD_BLEND_BORDER                 69
#define DRS_CMD_CHANGE_TRANSPARENCY          70
#define DRS_CMD_SELECT_BORDER_KEY            71
#define DRS_CMD_SWITCH_BORDER                72
#define DRS_CMD_SELECT_OVERRIDE_KEY          73
#define DRS_CMD_SWITCH_OVERRIDE              74
#define DRS_CMD_CHANGE_COLOR_BRIGHTNESS      75
#define DRS_CMD_CHANGE_COLOR_SATURATION      76
#define DRS_CMD_CHANGE_COLOR_RED             77
#define DRS_CMD_CHANGE_COLOR_GREEN           78
#define DRS_CMD_CHANGE_COLOR_BLUE            79
//efine DRS_CMD_??                           80  // available
//efine DRS_CMD_??                           81  // available
//efine DRS_CMD_??                           82  // available
//efine DRS_CMD_??                           83  // available
//efine DRS_CMD_??                           84  // available
#define DRS_CMD_START_SELECT                 85
#define DRS_CMD_START_SELECT_ZOOM            86
#define DRS_CMD_QUICK_SELECT_ZOOM            87
#define DRS_CMD_UNZOOM                       88
#define DRS_CMD_REPEAT_LAST_GOV_OR_DRS       89
#define DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS     90
#define DRS_CMD_REJECT_FIX                   91
#define DRS_CMD_TOGGLE_FIX                   92
#define DRS_CMD_TOGGLE_FIX_ZOOM              93
#define DRS_CMD_ACCEPT_FIX                   94
#define DRS_CMD_TOGGLE_BORDER                95
#define DRS_CMD_NOOP                         96
//efine DRS_CMD_??                           97  // available
//efine DRS_CMD_??                           98  // available
//efine DRS_CMD_??                           99  // available
//efine DRS_CMD_??                          100  // available
//efine DRS_CMD_??                          101  // available
//efine DRS_CMD_??                          102  // available
//efine DRS_CMD_??                          103  // available
//efine DRS_CMD_??                          104  // available
#define DRS_CMD_ACCEPT_IN_PLACE             105
#define DRS_CMD_ACCEPT_AND_APPLY_ALL        106
#define DRS_CMD_MOTION_SUBMODE_1            107
#define DRS_CMD_MOTION_SUBMODE_2            108
#define DRS_CMD_MOTION_SUBMODE_3            109
#define DRS_CMD_REPLACE_KEY                 110
#define DRS_CMD_TOGGLE_MOVE_KEYS            111
#define DRS_CMD_RESET_MOVE_VALUES           112
#define DRS_CMD_TOGGLE_ROTATE_KEYS          113
#define DRS_CMD_RESET_ROTATE_VALUES         114
#define DRS_CMD_CHANGE_COLOR_KEY            115
#define DRS_CMD_RESET_COLOR_VALUES          116
#define DRS_CMD_CHANGE_BORDER_KEY           117
#define DRS_CMD_RESET_BORDER_VALUE          118
#define DRS_CMD_CHANGE_GRAIN_KEY            119
#define DRS_CMD_RESET_GRAIN_VALUE           120
#define DRS_CMD_CHANGE_TRANSPARENCY_KEY     121
#define DRS_CMD_RESET_TRANSPARENCY_VALUE    122
#define DRS_CMD_STOP_ALL_PROCESSING         123
#define DRS_CMD_RESET_OVERRIDES             124
#define DRS_CMD_MOVE_OVER_KEY               125
#define DRS_CMD_MOVE_UNDER_KEY              126
#define DRS_CMD_ROTATE_OVER_KEY             127
#define DRS_CMD_ROTATE_UNDER_KEY            128
#define DRS_CMD_PAST_ONLY_KEY               129
#define DRS_CMD_FUTURE_ONLY_KEY             130
#define DRS_CMD_BOTH_PAST_AND_FUTURE_KEY    131
#define DRS_CMD_HARD_BLUR_KEY               132
#define DRS_CMD_HARD_EDGE_PRESERVE_KEY      133
#define DRS_CMD_SELECT_OR_TOGGLE_BORDER     134
#define DRS_CMD_SHOW_PREFERENCES            135

// Define main process types
#define DRS_PROCESS_NOOP         0   // Do "null fix" to load cache, etc
#define DRS_PROCESS_LOW_MOTION   1
#define DRS_PROCESS_HIGH_MOTION  2
#define DRS_PROCESS_HARD_MOTION  3
#define DRS_PROCESS_NO_MOTION    4
#define DRS_PROCESS_COLOR_BUMP   5

// Modifier (subprocess) types
#define DRS_SUBPROCESS_NONE      0
#define DRS_SUBPROCESS_A         1
#define DRS_SUBPROCESS_B         2
#define DRS_SUBPROCESS_C         3
#define DRS_SUBPROCESS_D         4
#define DRS_SUBPROCESS_E         5
#define DRS_SUBPROCESS_F         6
#define DRS_SUBPROCESS_G         7
#define DRS_SUBPROCESS_H         8
#define DRS_SUBPROCESS_I         9
#define DRS_SUBPROCESS_J        10
#define DRS_SUBPROCESS_K        11
#define DRS_SUBPROCESS_L        12

// Shift fix step sizes, in pixels
#define DRS_MOVE_SMALL           0.2
#define DRS_MOVE_NORMAL          1.0
#define DRS_MOVE_LARGE           5.0

// Change color step sizes, on scale of 0 to 100
#define DRS_CHANGE_COLOR_SMALL 1
#define DRS_CHANGE_COLOR_NORMAL 5
#define DRS_CHANGE_COLOR_LARGE 15

// Change grain step sizes, on scale of 0 to 100
#define DRS_CHANGE_GRAIN_SMALL 1
#define DRS_CHANGE_GRAIN_NORMAL 5
#define DRS_CHANGE_GRAIN_LARGE 15

// Change border step sizes, in pixels
#define DRS_CHANGE_BORDER_SMALL 1
#define DRS_CHANGE_BORDER_NORMAL 2
#define DRS_CHANGE_BORDER_LARGE 5

// Change roate step sizes, in degrees
#define DRS_ROTATE_SMALL .1
#define DRS_ROTATE_NORMAL 1.
#define DRS_ROTATE_LARGE 5.

// Change transparency step sizes, in pixels
#define DRS_CHANGE_TRANSPARENCY_SMALL 1
#define DRS_CHANGE_TRANSPARENCY_NORMAL 5
#define DRS_CHANGE_TRANSPARENCY_LARGE 15

// Where to get source frames for "replace" mode fixes.
#define DRS_REPLSRCFRM_INVALID      -1
#define DRS_REPLSRCFRM_USE_MARKS     0
#define DRS_REPLSRCFRM_PREV_1        1
#define DRS_REPLSRCFRM_PREV_2        2
#define DRS_REPLSRCFRM_NEXT_1        3
#define DRS_REPLSRCFRM_NEXT_2        4
#define DRS_REPLSRCFRM_PREV_AND_NEXT 5
#define DRS_REPLSRCFRM_CURRENT_FRAME 6
#define DRS_REPLSRCFRM_2_FRM_AUTO    7
#define DRS_REPLSRCFRM_3_FRM_AUTO    8

// define override modes
#define OM_NONE 0
#define OM_MOVE_UNDER 1
#define OM_MOVE_OVER 2
#define OM_CHANGE_GRAIN 3
#define OM_CHANGE_COLOR 4
#define OM_CHANGE_BORDER 5
#define OM_ROTATE_OVER 6
#define OM_ROTATE_UNDER 7
#define OM_CHANGE_TRANSPARENCY 8
#define OM_CHANGE_COLOR_BRIGHTNESS 9
#define OM_CHANGE_COLOR_SATURATION 10
#define OM_CHANGE_COLOR_RED 11
#define OM_CHANGE_COLOR_GREEN 12
#define OM_CHANGE_COLOR_BLUE 13

enum ESelectShape
{
   SELECT_REGION_RECT,
   SELECT_REGION_LASSO,
   SELECT_REGION_BEZIER
};

enum EDRSMouseMode
{
   DRS_MOUSE_MODE_NONE,
   DRS_MOUSE_MODE_STRETCH,
   DRS_MOUSE_MODE_SELECT_ADD,
   DRS_MOUSE_MODE_SELECT_CTRL
};

struct SOverrides
{
   float fOverrideMoveOverX;
   float fOverrideMoveOverY;
   float fOverrideMoveUnderX;
   float fOverrideMoveUnderY;
   float fOverrideRotateOver;
   float fOverrideRotateUnder;
   int iOverrideGrainStrength;
   int iOverrideBorderRadius;
	int iOverrideTransparency;
	int iOverrideGrainStrengthDefault = 0;
   int iOverrideColorBrightness = 0;
   int iOverrideColorSaturation = 0;
   int iOverrideColorRed = 0;
   int iOverrideColorGreen = 0;
   int iOverrideColorBlue = 0;

   SOverrides() { reset(); } ;

   void reset(bool bMoveOverIsSticky = false,
              bool bMoveUnderIsSticky = false,
              bool bRotateOverIsSticky = false,
              bool bRotateUnderIsSticky = false,
              bool bColorIsSticky = false,
              bool bBorderIsSticky = false,
              bool bGrainIsSticky = false,
              bool bTransparencyIsSticky = false
             )
   {
      if (!bMoveOverIsSticky)
         fOverrideMoveOverX = fOverrideMoveOverY = 0.F;
      if (!bMoveUnderIsSticky)
         fOverrideMoveUnderX = fOverrideMoveUnderY = 0.F;
      if (!bRotateOverIsSticky)
         fOverrideRotateOver = 0.F;
      if (!bRotateUnderIsSticky)
         fOverrideRotateUnder = 0.F;
      if (!bGrainIsSticky)
			iOverrideGrainStrength = iOverrideGrainStrengthDefault;
      if (!bBorderIsSticky)
         iOverrideBorderRadius = 0;
      if (!bTransparencyIsSticky)
         iOverrideTransparency = 0;
      if (!bColorIsSticky)
      {
         iOverrideColorBrightness = 0;
         iOverrideColorSaturation = 0;
         iOverrideColorRed = 0;
         iOverrideColorGreen = 0;
         iOverrideColorBlue = 0;
      }
   };

   bool areAnySet()
   {
     return !( iOverrideGrainStrength   == 0   &&
               iOverrideBorderRadius    == 0   &&
               fOverrideMoveOverX       == 0.F &&
               fOverrideMoveOverY       == 0.F &&
               fOverrideMoveUnderX      == 0.F &&
               fOverrideMoveUnderY      == 0.F &&
               fOverrideRotateOver      == 0.F &&
               fOverrideRotateUnder     == 0.F &&
               iOverrideTransparency    == 0   &&
               iOverrideColorBrightness == 0   &&
               iOverrideColorSaturation == 0   &&
               iOverrideColorRed        == 0   &&
               iOverrideColorGreen      == 0   &&
               iOverrideColorBlue       == 0   );
   };
};

//////////////////////////////////////////////////////////////////////

struct SDRSParameters
{
   int  mainProcessType;    // Low, High & Hard Motion
   int  subProcessType;     // For Low & High Motion: Normal, Past-only, future-only
                            // For Hard Motion: Regular, edge-preserving

   RECT userRect;
   bool useLasso;
   POINT userLassoPoints[MAX_LASSO_PTS];
   BEZIER_POINT *userBezierPointer;
   CRepairDRS *pRepairDRS;
   CPixelRegionList **pWhereToCopyTheFixRegionListTo;
   long *pCurrentFixRegionSerial;
   long myFixRegionSerial;

   int markInIndex;
   int markOutIndex;

   bool commandFileFlag;
   bool bReprocessingOverrides;
   bool bReprocessingFullFix;

   // Parameters from ini file
   long lBorderSize;        // Really BorderMax
   long lBorderMin;
   long lBorderPercent;
   EBlendType lBlendType;
   bool bUseGrainPresets;
   long lMotionOffsetLow;
   long lMotionOffsetHigh;
   long lPaddingAmountLow;
   long lPaddingAmountHigh;
   long lSurroundRadius;
   long lRowAccel;
   long lColAccel;
   bool bFieldRepeat;
   long lGrainSize;

   // DRS overrides
	SOverrides Overrides;

   // Debugging
   bool IAmDeleted;
   bool doubleDelete;

   // For color breathing
   vector<int> cutList;

   // Initialization
   SDRSParameters()
   : mainProcessType(DRS_PROCESS_NOOP)
   , subProcessType(DRS_SUBPROCESS_NONE)
   , useLasso(false)
   , userBezierPointer(NULL)
   , pRepairDRS(NULL)
   , pWhereToCopyTheFixRegionListTo(NULL)
   , pCurrentFixRegionSerial(NULL)
   , myFixRegionSerial(-1)
   , markInIndex(-1)
   , markOutIndex(-1)
   , commandFileFlag(false)
   , bReprocessingOverrides(false)
   , bReprocessingFullFix(false)
   , lBorderSize(0)
   , lBorderMin(0)
   , lBorderPercent(0)
   , lBlendType(BLEND_TYPE_NONE)
   , lMotionOffsetLow(0)
   , lMotionOffsetHigh(0)
   , lPaddingAmountLow(0)
   , lPaddingAmountHigh(0)
   , lSurroundRadius(0)
   , lRowAccel(0)
   , lColAccel(0)
   , bFieldRepeat(0)
   , lGrainSize(0)
   , IAmDeleted(false)
   , doubleDelete(false)
   {
      userRect.top = 0;
      userRect.bottom = 0;
      userRect.left = 0;
      userRect.right = 0;
   };

   ~SDRSParameters()
   {
      if(IAmDeleted)
         doubleDelete = true;
      IAmDeleted = true;

      // Sorry this is wrong....
      // Prevent a leak; note it is important to NULL out the pointer to
      // the deleted list because it can point to a DRSTool member variable!
      //if (pWhereToCopyTheFixRegionListTo != NULL)
      //{
         //delete *pWhereToCopyTheFixRegionListTo;
         //*pWhereToCopyTheFixRegionListTo = NULL;
      //}
   }
};

// Background fix tracking
enum EBGFixTrackingStatus
{
   DRS_BGFTS_INVALID=-1,
   DRS_BGFTS_PRELOAD_QUEUED=100,
   DRS_BGFTS_PRELOADING=101,
   DRS_BGFTS_PRELOAD_COMPLETE=102,
   DRS_BGFTS_FIX_QUEUED=200,
   DRS_BGFTS_FIXING=201,
   DRS_BGFTS_FIX_COMPLETE=202,
   DRS_BGFTS_FIX_RESOLVED=203
};

struct SBGFixTracker
{
   EBGFixTrackingStatus status;
   int outlineColor;            // machine.h colors
   bool suppressOutline;        // don't draw outline (for reprocessed fixes)
   double firstDrawnTime;
   int frameIndex;
   SDRSParameters *drsParams;
   POINT *drsLasso;

   SBGFixTracker()
   : status(DRS_BGFTS_INVALID)
   , outlineColor(INVALID_COLOR)
   , suppressOutline(false)
   , firstDrawnTime(0.0)
   , frameIndex(-1)
   , drsParams(NULL)
   , drsLasso(NULL)
   {};

   ~SBGFixTracker()
   {
      delete drsParams;
      delete drsLasso;
   };
};

// Background thread commands
enum EBGTCommand
{
   DRS_BGTC_SHUTDOWN = -1,   // Terminate thread
   DRS_BGTC_SYNCHRONIZE,     // For synchronizing foreground & background
   DRS_BGTC_PRELOAD_INPUT,   // Preload the input frames
   DRS_BGTC_PROCESS_FIX,     // Process a DRS fix
   DRS_BGTC_REPROCESS_FIX,   // Reprocess a DRS fix
   DRS_BGTC_ACCEPT_FIX,          // Accept a fix in the background
   DRS_BGTC_ACCEPT_AND_GOTO_FIX, // Accept fix and goto that frame
   DRS_BGTC_REJECT_FIX,          // Reject fix in the background
   DRS_BGTC_REJECT_AND_GOTO_FIX  // Reject fix and goto that frame
};

struct SBGTCommand
{
   EBGTCommand command;
   SBGFixTracker *fixTracker;

   SBGTCommand()
   : command(DRS_BGTC_SYNCHRONIZE)
   , fixTracker(NULL)
   {};

};


//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CDRSToolParameters;

//////////////////////////////////////////////////////////////////////


class CDRSTool : public CToolObject
{
public:
   string IniFileName;          // Configuration file
   string IniSection;           // Section in file

   CDRSTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CDRSTool();

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   // Tool Event Handlers
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onChangingFraming();
   virtual int onNewFraming();
   virtual int onNewMarks();
   virtual int onRedraw(int frameIndex);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onHeartbeat();

   virtual bool toolHideQuery();
   virtual int toolHide();
   virtual int toolShow();
   virtual bool IsShowing();
   virtual int toolActivate();
   virtual int toolDeactivate();

   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   virtual void NotifyGOVDone();
   virtual void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                      const RECT &rect, const POINT *lasso);
   virtual void NotifyRightClicked();

   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);
   virtual int TryToResolveProvisional();

   //  Getters for DRS States
   int GetMainProcessType()      { return DRSState.mainProcessType;   }
   int GetSubProcessType()       { return DRSState.subProcessType;    }
//   int GetAdjustDensity()        { return DRSState.bAdjustDensity;    }
   int GetMaskImage()            { return DRSState.bMaskImage;        }
   ESelectShape GetSelectShape() { return DRSState.selectShape;       }
   EBlendType GetBorder()        { return DRSState.iBlendType;        }
   bool GetAutoAccept()          { return DRSState.bAutoAccept;       }
   bool GetPreloadInput()        { return DRSState.bPreloadInput;     }
   bool wasGOVLastAction()       { return DRSState.bLastActionWasGOV; }

   int GetOverrideColorBrightness(void) {return(DRSState.Overrides.iOverrideColorBrightness);}
   int GetOverrideColorSaturation(void) {return(DRSState.Overrides.iOverrideColorSaturation);}
   int GetOverrideColorRed(void) {return(DRSState.Overrides.iOverrideColorRed);}
   int GetOverrideColorGreen(void) {return(DRSState.Overrides.iOverrideColorGreen);}
   int GetOverrideColorBlue(void) {return(DRSState.Overrides.iOverrideColorBlue);}
   int GetOverrideMode(void) {return(DRSState.iOverrideMode);}
   int GetOverrideGrainStrength(void) {return(DRSState.Overrides.iOverrideGrainStrength);}
   int GetOverrideBorderRadius(void) {return(DRSState.Overrides.iOverrideBorderRadius);}
   float GetOverrideMoveOverX(void) {return(DRSState.Overrides.fOverrideMoveOverX);}
   float GetOverrideMoveOverY(void) {return(DRSState.Overrides.fOverrideMoveOverY);}
   float GetOverrideMoveUnderX(void) {return(DRSState.Overrides.fOverrideMoveUnderX);}
   float GetOverrideMoveUnderY(void) {return(DRSState.Overrides.fOverrideMoveUnderY);}
   float GetOverrideRotateOver(void) {return(DRSState.Overrides.fOverrideRotateOver);}
   float GetOverrideRotateUnder(void) {return(DRSState.Overrides.fOverrideRotateUnder);}
   int GetOverrideTransparency(void) {return(DRSState.Overrides.iOverrideTransparency);}
   void SetOverrideColorBrightness(int newValue);
   void SetOverrideColorSaturation(int newValue);
   void SetOverrideColorRed(int newValue);
   void SetOverrideColorGreen(int newValue);
   void SetOverrideColorBlue(int newValue);
	void SetOverrideGrainStrength(int newValue);
   void SetOverrideGrainStrengthDefault(int newValue);
	void SetOverrideBorderRadius(int newValue);
   void SetOverrideMoveOverX(float newOffset);
   void SetOverrideMoveOverY(float newOffset);
   void SetOverrideMoveUnderX(float newOffset);
   void SetOverrideMoveUnderY(float newOffset);
   void SetOverrideRotateOver(float newOffset);
   void SetOverrideRotateUnder(float newOffset);
   void SetOverrideTransparency(int newValue);
   bool GetBorderEnabled() {return (DRSState.iBlendType != BLEND_TYPE_NONE);}
   void SetBorderEnabled(bool flag);
   void SelectGrainPreset(GrainParameterBaseType grainParametersBase, int grainParametersPresetIndex);
   SOverrides GetOverrides();
   void SetOverrides(const SOverrides &overrides);
   bool GetOverrideStickiness();
   bool GetOverrideStickiness(int which);
   void SetOverrideStickiness(bool bSticky);
	void SetOverrideStickiness(int which, bool bSticky);

	GrainPresets &GetGrainPresets() { return _grainPresets; }
   bool GetUseGrainPresets()       { return _grainPresets.IsEnabled();  }
   void SetUseGrainPresets(bool flag);

   // This is used to select which set of preferences to use
   int GetImageResolution (void);

   void LoadDRSStatePreferences();
   bool AutoAccept();

   // New macro stuff
   void StartMacro();
   void StopMacro();
   bool IsOkToStartMacro();
   bool IsOkToStopMacro();

   // Auto Replace
   int currentAutoReplaceMarkIn = -1;
   int currentAutoReplaceFixFrame = -1;
   int currentAutoReplaceMarkOut = -1;
   void RunAutoReplace();

  // tool configuration values
  CDRSParameters
    DRSParam;

 bool reportColorbumpError = false;

private:
//
//  Misc variables

   bool isInitingFixEng;

   // Remember the state of the "subprocess" buttons
   int oldHighLowSubprocessState;
   int oldHardSubprocessState;
	int oldReplaceProcessType;
	int oldReplaceModeSourceFrameOption = DRS_REPLSRCFRM_INVALID;

//
//  This is a class that defines the internal DRSStates
//  When instantiated, the states are set to fixed types
//  ResetStates() also resets the states to the fixed type
//  See class for these types
class CDRSState
{
  public:
     // Constructor, just set to known states
     CDRSState() { ResetStates();}

     //  This resets all the states to a known value.
     void ResetStates()
      {
         fixBeingProcessed = false;
         fixingMultipleFrames = false;
         notReallyFixingAnything = false;
         fixIsPending = false;
         fixReprocessPending = false;
         doFixReprocessNow = false;
         updateGUIButtonsNow = false;
         foregroundIsActive = false;
         backgroundIsActive = false;
         foregroundIsWaitingForBackground = false;
         backgroundAcceptInProgress = false;
         needToRefreshFrame = -1;
         wantToPreloadNow = false;
         goToFixFrameNow = false;
         inhibitRepeatFixOp = false;


         mainProcessType = DRS_PROCESS_LOW_MOTION;
         subProcessType = DRS_SUBPROCESS_NONE;

         pendingMainProcessType = DRS_PROCESS_NOOP;
         pendingSubProcessType = DRS_SUBPROCESS_NONE;

//         bAdjustDensity = false;
         bMaskImage = false;
         selectShape = SELECT_REGION_RECT;
         mouseMode = DRS_MOUSE_MODE_NONE;
         iBlendType = BLEND_TYPE_LINEAR;
         LoadPreferences();
         bAutoAcceptOnNextMouseMove = false;
         iMouseX = -1;
         iMouseY = -1;

         bLastActionWasGOV = false;

         iOverrideMode = OM_NONE;
         ResetOverrides();
         bOverridesAreSticky = true;
         bMoveOverIsSticky = true;
         bMoveUnderIsSticky = true;
         bRotateOverIsSticky = true;
         bRotateUnderIsSticky = true;
         bColorIsSticky = true;
         bBorderIsSticky = true;
         bGrainIsSticky = true;
         bTransparencyIsSticky = true;
      }

     // reset the overrides values to defaults
     inline void ResetOverrides()
      {
         Overrides.reset();
      }

     // reset the overrides values to defaults if not sticky
     inline void ResetOverridesIfNotSticky()
      {
         if (bOverridesAreSticky)
         {
            Overrides.reset(bMoveOverIsSticky,
                            bMoveUnderIsSticky,
                            bRotateOverIsSticky,
                            bRotateUnderIsSticky,
                            bColorIsSticky,
                            bBorderIsSticky,
                            bGrainIsSticky,
                            bTransparencyIsSticky
                           );
         }
         else
         {
				Overrides.reset();
         }
      }

      inline void LoadPreferences()
      {
        // check the state of auto-accept
        CIniFile *ini = CreateIniFile (CPMPIniFileName ("DRS"));
        if (ini != 0)
         {
           bAutoAccept = ini->ReadBool("Preferences", "AutoAccept", true);
           bPreloadInput = ini->ReadBool("Preferences", "PreloadInput", true);
           DeleteIniFile(ini);
         }
      }

     //
     //  Internal DRS States

     bool fixBeingProcessed;       // true between end stretching the box and
                                   // the return of the fix from the library
     bool fixingMultipleFrames;    // Only valid if fixBeingProcessed is true
     bool notReallyFixingAnything; // We're just preloading frames, not fixing
     bool fixIsPending;            // true between stretching the box and
                                   // accept or reject
     bool fixReprocessPending;     // Override was changed while processing
     bool doFixReprocessNow;       // Tells heartbeat to start up a reprocess

     bool bLastActionWasGOV;       // ~ key hack
     bool updateGUIButtonsNow;     // background processing thread hack
     bool foregroundIsActive;      // while stretching rect until BG cmd Q'ed
     bool backgroundIsActive;      // while background thread is not waiting
     bool foregroundIsWaitingForBackground; // HACK to avoid deadlock
     bool backgroundAcceptInProgress; // foreground/background synch hack
     int  needToRefreshFrame;      // frame index of frame that needs refresh
     bool wantToPreloadNow;        // deferred preload flag
     bool goToFixFrameNow;         // BG sets this so FG jumps to fix frame
     bool inhibitRepeatFixOp;      // Hack flag to prevent ~ op after reject

     int  mainProcessType;
     int  subProcessType;

     int pendingMainProcessType;   // Main process type of most recent fix
     int pendingSubProcessType;

//     bool bAdjustDensity;
     bool bMaskImage;
     ESelectShape selectShape;   // current select shape: rect, lasso or bezier
     EDRSMouseMode mouseMode;
     EBlendType iBlendType;     // See EBlendType for values.
     bool bAutoAccept;          // true means auto accept fixes
     bool bPreloadInput;        // true means preload 5 input frames
     bool bAutoAcceptOnNextMouseMove;
     int iMouseX;
     int iMouseY;
     int iOverrideMode;         // see OM_* for values
     bool bOverridesAreSticky;  // If true, don't clear overrides after fix
     bool bMoveOverIsSticky;
     bool bMoveUnderIsSticky;
     bool bRotateOverIsSticky;
     bool bRotateUnderIsSticky;
     bool bColorIsSticky;
     bool bBorderIsSticky;
     bool bGrainIsSticky;
     bool bTransparencyIsSticky;
     bool bReprocessingOverrides; // We are only adjusting overrides
     bool bReprocessingFullFix;   // We are redoing fix in new mode

	  SOverrides Overrides;

};

  void SaveStatesToIniFile();

// Instance Variables of DRS Tool
  CDRSState DRSState;
  RECT currentUserRect;
  POINT *currentUserLassoPointer;
  POINT *globalLassoCopy;
  POINT *quickSelectLassoPointer;
  int quickSelectLassoPointCount;
  BEZIER_POINT *currentUserBezierPointer;
  CPixelRegionList *fixedPixelRegionList;
  long fixedPixelRegionListSerial;
  CRepairDRS repairDRS;    // NOTE: This takes a LOOOONG time to construct!!
  bool needToClearStatusBar;

  GrainPresets _grainPresets;

  int fixFrameIndex;       // Index of frame that is/was fixed

  int iModifierSubProcessType;                  // modifiers used to translate keys (1+F1) into past only etc.

  int preloadToolSetupHandle;                    // just preload, don't fix
  bool preloadToolSetupHandleIsValid;
  int singleFrameToolSetupHandle;                // DRS repair - single frame
  bool singleFrameToolSetupHandleIsValid;
  int multiFrameToolSetupHandle;                 // DRS repair - multi frame
  bool multiFrameToolSetupHandleIsValid;

  // Timer used to measure DRS performance
  CHRTimer hrTimer;
  double dStartTime;

   bool IsDRSActive(void);
   bool IsDRSFixPending(void);
   bool IsGovActive(void);
   bool IsGovPending(void);
   bool IsProvisionalPending(void);

  //  Private members that process DRS commands
   int DRSProcessTypeKeyCommand(int toolCommand);
   int DRSProcessTypeCommand(int toolCommand);
   int DRSProcessChange(int toolCommand);

   int OverrideCommand(int toolCommand);
   int MoveFixCommand(int toolCommand, bool bMoveOver);
   int ChangeColorBrightnessCommand(int toolCommand);
   int ChangeColorSaturationCommand(int toolCommand);
   int ChangeColorRedCommand(int toolCommand);
   int ChangeColorGreenCommand(int toolCommand);
   int ChangeColorBlueCommand(int toolCommand);
	int ChangeGrainCommand(int toolCommand);
   int ChangeBorderCommand(int toolCommand);
   int ChangeTransparencyCommand(int toolCommand);
   int BorderBlendCommand();
   int RotateCommand(int toolCommand, bool bOverFlag);
   void InitStates();

   virtual int ProcessSingleFrame(EToolSetupType toolSetupType);
   int ProcessSingleFrameInternal(EToolSetupType toolSetupType,
                                  SDRSParameters &drsParams);
   virtual int RunFrameProcessing(int newResumeFrame);
   int GatherDRSParameters(SDRSParameters &drsParams);
   void DestroyAllToolSetups();

   int BeginUserSelect(ESelectShape selectShape, bool goZoom);
   bool userSelectGoZoom;
   int EndUserSelect();
   int SelectShiftDown();
   int SelectCtrlDown();

   POINT* CopyLasso(const POINT *srcLasso, POINT *dstLasso);

   void deleteCurrentBezier();
   void getNewBezier();

   EToolSetupType currentToolSetupType;
   void PreMakeSimpleToolSetups();

   int TogglePendingFix(bool goZoom);
   int AcceptFix(bool goToFrame=false);
   void AcceptFixInBackground(bool goToFrame);
   int AcceptFixFromBackgroundThread();
   int RejectFix(bool goToFrame=false);
   void RejectFixInBackground(bool goToFrame);
   int RejectFixFromBackgroundThread();
   int ApplyPendingFixToMarkedRange();

   int CollectInfoForFix(void);
   RECT GetPaddedRect(RECT rect, int rowPadding, int colPadding);
   void GetMainProcessTypeString(string &);
   void GetFullProcessTypeString(string &);
   int ProcessNewFix();
   int ReprocessFix(bool doingOverridesOnly);
   int SetupAndProcessFix(SDRSParameters &drsParams);
   void WaitForProcessingToFinish();

   bool GetNewUserRectangle(); // for G key implementation
   void SetGOVRepeatShape();

   void CleanUpBeforeLeaving();

   CHRTimer drawingTimer;

   //////////////////////////////////////////////////////////////////////
   // Background processor

   // Input to ProcessFixInBackground
   enum EReprocessFixType
   {
      REPROCESS_TYPE_NONE,
      REPROCESS_TYPE_OVERRIDES,
      REPROCESS_TYPE_REDO_COMPLETELY
   };

   void *backgroundThreadID;
   CRingBuffer<SBGTCommand> backgroundCommandQueue;
   void *backgroundSynchSemaphore;
   int mostRecentInputLoadedFrameIndex;
   list<SBGFixTracker *> fixesInProgressList;
   CSpinLock fixesInProgressListLock;
   bool _needToRestartTheBackgroundThread = false;

   void SpawnBackgroundThread();
   void KillBackgroundThread();
   void WaitUntilBackgroundThreadIsIdle();
   static void StartBackgroundThread (void *vpAppData, void *vpReserved);
   void RunBackgroundThread();
   void ProcessNewFixInBackground();
   void PreloadInputFramesInBackground();
   void ReprocessFixInBackground(bool overridesOnly);
   void ProcessFixInBackground(EReprocessFixType reprocessFixType);
   bool AreAnyOtherFixesQueuedOrInProgress(SBGFixTracker *fixTracker);
   int  ComputeFixOutlineColor(SBGFixTracker *fixTracker,
                               bool minDisplayTimeElapsed);
   void DrawFixOutline(SBGFixTracker *fixTracker, int color);
   void DrawOutlinesOfFixesInProgress();
   void ClearFixesInProgressList();
   void ChangePendingToResolvedInProgressList();
   void setTrackerStatusFromBackground(SBGFixTracker *fixTracker,
                                       EBGFixTrackingStatus newStatus);
   //////////////////////////////////////////////////////////////////////
};

extern CDRSTool *GDRSTool;       // Lives in DRSUnit

class CDRSToolParameters : public CToolParameters
{
public:
   CDRSToolParameters() : CToolParameters(1, 1) {};
   virtual ~CDRSToolParameters() { };

   SDRSParameters drsParams;

};

//////////////////////////////////////////////////////////////////////

class CDRSToolProc : public CToolProcessor
{
public:
   CDRSToolProc(int newToolNumber, const string &newToolName,
                CToolSystemInterface *newSystemAPI,
                const bool *newEmergencyStopFlagPtr,
                IToolProgressMonitor *newToolProgressMonitor,
					 int numberOfOutputPorts,
					 GrainPresets *grainPresets);
   virtual ~CDRSToolProc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

   void CalculateMotionOffsets(int configOffset, long *lRowMotionOffset,
                               long *lColMotionOffset);
   void CalculatePaddingPixels(int configPadding, long *lRowPadding,
                               long *lColPadding);
   RECT GetPaddedRect(const RECT &rect, int rowPadding, int colPadding);
   int GetComponentMinMax(const CImageFormat *imageFormat, long laMinValue[],
                          long laMaxValue[], long laFillValue[]);

private:

   int iInFrameIndex, iOutFrameIndex;     // Frames to fix
   int iFrameCount;

	SDRSParameters drsParams;
	GrainPresets *_grainPresets;

   int iNumProcessFrames;
   int iFrameToFix;       // frame to be fixed (0-MAX_FRAME-1)
   long lRPadding;        // Size of padding for Vert direction
   long lCPadding;        // Size of padding for Horz direction
   long lRAccel;          // Accelleration in Vert direction
   long lCAccel;          // Accelleration in Horz direction
   long lRMotionOffset;   // Motion offset in Vert direction
   long lCMotionOffset;   // Motion offset in Horz direction
   int iFixType;          // translate low motion into RT_ORDINARY etc.
   bool bIsMonochrome;    // "is monochrome" flag (Color Bump)
   int iDirection;        // Direction indicator for color bump (-1=Past, 0=Auto, 1=Future)
   int _validFrameMask;   // What frames are valid in DRS
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(DRSTOOL_H)
