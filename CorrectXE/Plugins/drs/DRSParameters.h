#ifndef DRS_PARAMETERS_H
#define DRS_PARAMETERS_H

#include <string>
using std::string;

#include "ImageInfo.h"
#include "IniFile.h"
#include "Repair.h"

// types of Quick Selection (e.g. the 'G' key)
#define QS_SELECT_RECT 0
#define QS_SELECT_RECT_HALF_WIDTH 1
#define QS_SELECT_RECT_HALF_HEIGHT 2
#define QS_SELECT_ELLIPSE 3
#define QS_SELECT_ELLIPSE_HALF_WIDTH 4
#define QS_SELECT_ELLIPSE_HALF_HEIGHT 5

// Values that need to be transfered to initialization files
#define DEFAULT_SURROUND_RADIUS       10
#define DEFAULT_ROW_ACCEL          5  // motion acceleration constants
#define DEFAULT_COL_ACCEL          5
#define DEFAULT_BORDER_MAX_SD      6
#define DEFAULT_BORDER_MIN_SD      3
#define DEFAULT_BORDER_MAX_HD     14
#define DEFAULT_BORDER_MIN_HD      6
#define DEFAULT_BORDER_MAX_2K     15
#define DEFAULT_BORDER_MIN_2K      6
#define DEFAULT_BORDER_MAX_4K     30
#define DEFAULT_BORDER_MIN_4K     10
#define DEFAULT_BORDER_MAX_6K     50
#define DEFAULT_BORDER_MIN_6K     20
#define DEFAULT_BORDER_MAX_8K    100
#define DEFAULT_BORDER_MIN_8K     40
#define DEFAULT_BORDER_PERCENT    10  // Resolution independent
#define DEFAULT_MOTION_OFFSET_LOW  2
#define DEFAULT_MOTION_OFFSET_HIGH 5
#define DEFAULT_IMPORT_OFFSET_LOW  0   // No more motion allowed on replace
#define DEFAULT_IMPORT_OFFSET_HIGH 0   // No more motion allowed on replace
#define DEFAULT_PADDING_AMOUNT_LOW 15
#define DEFAULT_PADDING_AMOUNT_HIGH 20
#define DEFAULT_SMALL_BOX_SIZE 10

// Parameters that are resolution dependent
struct SDRSIniParameters
{
   // Parameters from ini file
   long lBorderSize;                 // Really BorderMax
   long lBorderMin;
   long lBorderPercent;
   long lBlendType;
   long lMotionOffsetLow;
   long lMotionOffsetHigh;
   long lImportOffsetLow;
   long lImportOffsetHigh;
   long lPaddingAmountLow;
   long lPaddingAmountHigh;
   long lGrainSize;
   long lSurroundRadius;
   long lRowAccel;
   long lColAccel;
   bool bFieldRepeat;
   bool bCommandFile;
   int iQuickSelect;
   int iQSRect0Size;
   int iQSRect1Size;
   int iQSRect2Size;
   int iQSEllipse0Size;
   int iQSEllipse1Size;
   int iQSEllipse2Size;

};

// parameters that are resolution independent
struct SDRSStateParameters
{
  int iMainProcessType;
  int iSubProcessType;
  int iSelectMode;
  EBlendType iBlendType;
  bool bUseGrainPresets;
  int iOverrideMode;
  bool bOverridesAreSticky;
  bool bMoveOverIsSticky;
  bool bMoveUnderIsSticky;
  bool bRotateOverIsSticky;
  bool bRotateUnderIsSticky;
  bool bColorIsSticky;
  bool bBorderIsSticky;
  bool bGrainIsSticky;
  bool bTransparencyIsSticky;
};

class CDRSParameters
{
public:
  CDRSParameters();
  ~CDRSParameters();

  // setters
  void setFileIniParameters (int iRes, const SDRSIniParameters &dip);
  void setActiveIniParameters (int iRes, const SDRSIniParameters &dip);

  void setFileStateParameters (const SDRSStateParameters &dsp);

  
  // getters
  SDRSIniParameters getFileIniParameters (int iRes);
  SDRSIniParameters getFactoryIniParameters (int iRes);
  SDRSIniParameters getActiveIniParameters (int iRes);

  SDRSStateParameters getFileStateParameters ();

  // file IO
  bool writeIniParameters (int iRes);
  bool writeIniParameters ();
  bool readIniParameters (int iRes);
  bool readIniParameters ();

  bool writeStateParameters ();
  bool readStateParameters ();
  
private:
  SDRSIniParameters
    dipaFile[IF_IMAGE_RESOLUTION_COUNT],
    dipaFactory[IF_IMAGE_RESOLUTION_COUNT],
    dipaActive[IF_IMAGE_RESOLUTION_COUNT];

  SDRSStateParameters
    dspFile;

  bool factoryIniParameters (int iRes);
  bool factoryIniParameters ();


  CIniFile * openIni (int iRes, string &strParamSection);
  CIniFile * openIni (string &strControlSection);
};
#endif
