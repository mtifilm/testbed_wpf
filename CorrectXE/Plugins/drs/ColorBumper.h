#ifndef ColorBumperH
#define ColorBumperH

#include "Ippheaders.h"

// NOTE: this link is temporary until we decide where the crap should go
#include "../ColorBreathing/SpatialFlicker/ThinPlateSplineSurface.h"
#include "../ColorBreathing/SpatialFlicker/ZonalParameters.h"

struct ResizeSourceParams;

enum ColorBumpDirection
{
   PAST_ONLY = -1,
   AUTO = 0,
   FUTURE_ONLY = 1
};

class ColorBumper
{
public:

   int Bump(MTI_UINT16 **inputData, int pitch, int inputDataSize, MTI_UINT16 maxValue, bool isMonochrome, ColorBumpDirection direction, const RECT &roiRect, MTI_UINT16 *outputData);
   int flashFrame(const std::vector<Ipp16uArray> &innputFrames, Ipp16uArray &outputFrame, const MtiRect &repairRoi, bool isMonochrome = false);
private:

   void ResizeSourceFrames(ResizeSourceParams *params);

   Ipp32fArray ExtractRoiAsFloat(Ipp16u* imageData);
	void ReplaceRoiInImage(Ipp16u* dest, int destrowPitch, Ipp32f* roiData, int roiRowPitch);
	void MakeRoiWhite(Ipp16u* dest, int destrowPitch);

   Ipp16u **_inputData;
   Ipp16u *_outputData;
   IppiPoint _roiOffset;
   IppiSize _roiSize;
   int _imagePitchInPixels;
   Ipp16u _maxValue;

   // We need one for each channel
   ThinPlateSplineSurface _thinPlateSplineSurface;
   DeflickerParamsStruct _currentZonalParamsStruct;
};

#endif
