// DRSTool.cpp: implementation of the CDRSTool class.
//
/*
$Header: /usr/local/filmroot/Plugins/drs/DRSTool.cpp,v 1.139.2.74 2009/1013 17:56:15 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////


#include "DRSTool.h"
#include "GuiWin.h"

#include "bthread.h"
#include "Clip3.h"
#include "cpmp_err.h"  // for ERR_ALLOC
#include "err_clip.h"
#include "ImageFormat3.h"
#include "IniFile.h" // for TRACE_N()
#include "JobManager.h"
#include "MTIsleep.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"
#include "PixelRegions.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include <math.h>

#include <locale.h>

//#define PRINT_TIME
#define IMPORT_BOTH_IN_AND_OUT_FRAMES 1

#define FRAME_STOP_THRESHOLD  1000.0 // one second
#define D_KEY_BOX_VISIBLE_TIME 250.0 // quarter second

#define QUEUED_COLOR      DASHED_GREEN
#define IN_PROGRESS_COLOR DASHED_CYAN
// I didn't like the outline changing to blue, so I just leave it cyan now
#define COMPLETED_COLOR   DASHED_CYAN //DASHED_BLUE



//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 drsToolNumber = DRS_TOOL_NUMBER;

// -------------------------------------------------------------------------
// DRS Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem drsDefaultConfigItems[] =
{
   // DRS Command                          Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------

   // Disable middle button
   { DRS_CMD_NOOP,                        " MButton      ButtonDown" },

   // Don't move off frame if a fix is pending

   // Accept/Reject Fix
   { DRS_CMD_ACCEPT_FIX,                  " G            KeyDown   " },
   { DRS_CMD_ACCEPT_IN_PLACE,             " Ctrl+G       KeyDown   " },
   { DRS_CMD_ACCEPT_AND_APPLY_ALL,        " Shift+G      KeyDown   " },
   { DRS_CMD_STOP_ALL_PROCESSING,         " Ctrl+Space   KeyDown   " },
   { DRS_CMD_REJECT_FIX,                  " A            KeyDown   " },

   // Repeat Fix
   { DRS_CMD_REPEAT_LAST_GOV_OR_DRS,      " `            KeyDown   " },
   { DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS,    " Shift+`      KeyDown   " },

   // Toggle Pending Fix
   { DRS_CMD_TOGGLE_FIX,                  " T            KeyDown   " },
   { DRS_CMD_TOGGLE_FIX_ZOOM,             " Shift+T      KeyDown   " },

   // Selection control
   { DRS_CMD_START_SELECT,               " LButton       ButtonDown" },
   { DRS_CMD_END_SELECT,                 " LButton       ButtonUp  " },
   { DRS_CMD_START_SELECT_ZOOM,          " Shift+LButton ButtonDown" },
   { DRS_CMD_END_SELECT,                 " Shift+LButton ButtonUp  " },
   { DRS_CMD_SELECT_ADD_DN,              " Shift+LButton ButtonDown" },
   { DRS_CMD_SELECT_ADD_UP,              " Shift+LButton ButtonUp  " },
   { DRS_CMD_SELECT_CTRL_DN,             " Ctrl+LButton  ButtonDown" },
   { DRS_CMD_SELECT_CTRL_UP,             " Ctrl+LButton  ButtonUp  " },
   { DRS_CMD_SELECT_DELETE,              " esc           KeyDown   " },

   // Small Box Select and Fix:  aka "Quick Select"
   { DRS_CMD_QUICK_SELECT,                   " D            KeyDown   " },
   { DRS_CMD_QUICK_SELECT_ZOOM,              " Shift+D      KeyDown   " },

   // DRS overrides in Normal Size Steps with Arrow Keys
   { DRS_CMD_OVERRIDE_UP,                 " Up           KeyDown   " },
   { DRS_CMD_OVERRIDE_DOWN,               " Down         KeyDown   " },
   { DRS_CMD_OVERRIDE_LEFT,               " Left         KeyDown   " },
   { DRS_CMD_OVERRIDE_RIGHT,              " Right        KeyDown   " },

   // DRS overrides in Large Size Steps with <Shift> + Arrow Keys
   { DRS_CMD_OVERRIDE_UP_LARGE,           " Shift+Up     KeyDown   " },
   { DRS_CMD_OVERRIDE_DOWN_LARGE,         " Shift+Down   KeyDown   " },
   { DRS_CMD_OVERRIDE_LEFT_LARGE,         " Shift+Left   KeyDown   " },
   { DRS_CMD_OVERRIDE_RIGHT_LARGE,        " Shift+Right  KeyDown   " },

   // DRS overrides in Small Size Steps with <Ctrl> + Arrow Keys
   { DRS_CMD_OVERRIDE_UP_SMALL,           " Ctrl+Up      KeyDown   " },
   { DRS_CMD_OVERRIDE_DOWN_SMALL,         " Ctrl+Down    KeyDown   " },
   { DRS_CMD_OVERRIDE_LEFT_SMALL,         " Ctrl+Left    KeyDown   " },
   { DRS_CMD_OVERRIDE_RIGHT_SMALL,        " Ctrl+Right   KeyDown   " },

   // Help
///{ DRS_CMD_SHOW_HELP,                   " F1           KeyDown   " },
///{ DRS_CMD_SHOW_HOTKEY_HELP,            " Shift+F1     KeyDown   " },
   { DRS_CMD_SHOW_PREFERENCES,            " Ctrl+F1      KeyDown   " },

   // Process Type Choices for Keys
   { DRS_CMD_LOW_MOTION_KEY,              " 1            KeyDown   " },
   { DRS_CMD_HIGH_MOTION_KEY,             " 2            KeyDown   " },
   { DRS_CMD_HARD_MOTION_KEY,             " 3            KeyDown   " },
   { DRS_CMD_REPLACE_KEY,                 " 4            KeyDown   " },
   { DRS_CMD_COLOR_BUMP_KEY,              " Shift+4      KeyDown   " },
   { DRS_CMD_MOTION_SUBMODE_1,            " Shift+1      KeyDown   " },
   { DRS_CMD_MOTION_SUBMODE_2,            " Shift+2      KeyDown   " },
   { DRS_CMD_MOTION_SUBMODE_3,            " Shift+3      KeyDown   " },

   { DRS_CMD_TOGGLE_MOVE_KEYS,            " 5            KeyDown   " },
   { DRS_CMD_RESET_MOVE_VALUES,           " Shift+5      KeyDown   " },
   { DRS_CMD_TOGGLE_ROTATE_KEYS,          " 6            KeyDown   " },
   { DRS_CMD_RESET_ROTATE_VALUES,         " Shift+6      KeyDown   " },
   { DRS_CMD_CHANGE_COLOR_KEY,            " 7            KeyDown   " },
   { DRS_CMD_RESET_COLOR_VALUES,          " Shift+7      KeyDown   " },
   { DRS_CMD_SELECT_OR_TOGGLE_BORDER,     " 8            KeyDown   " },
   { DRS_CMD_RESET_BORDER_VALUE,          " Shift+8      KeyDown   " },
   { DRS_CMD_CHANGE_GRAIN_KEY,            " 9            KeyDown   " },
   { DRS_CMD_RESET_GRAIN_VALUE,           " Shift+9      KeyDown   " },
   { DRS_CMD_CHANGE_TRANSPARENCY_KEY,     " 0            KeyDown   " },
   { DRS_CMD_RESET_TRANSPARENCY_VALUE,    " Shift+0      KeyDown   " },

   { DRS_CMD_RESET_OVERRIDES,             " Shift+C      KeyDown   " },

   { DRS_CMD_SELECT_SHAPE_KEY,            " Q            KeyDown   " },

};


static CUserInputConfiguration *drsDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(drsDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               drsDefaultConfigItems);

// -------------------------------------------------------------------------
// DRS Command Table

static CToolCommandTable::STableEntry drsCommandTableEntries[] =
{
   // DRS Command                         DRS Command String
   // -------------------------------------------------------------

   // No-op
   { DRS_CMD_NOOP,                        "DRS_CMD_NOOP" },

   // Accept/Reject Fix
   { DRS_CMD_ACCEPT_FIX,                  "DRS_CMD_ACCEPT_FIX" },
   { DRS_CMD_ACCEPT_IN_PLACE,             "DRS_CMD_ACCEPT_IN_PLACE" },
   { DRS_CMD_ACCEPT_AND_APPLY_ALL,        "DRS_CMD_ACCEPT_AND_APPLY_ALL" },
   { DRS_CMD_STOP_ALL_PROCESSING,         "DRS_CMD_STOP_ALL_PROCESSING" },
   { DRS_CMD_REJECT_FIX,                  "DRS_CMD_REJECT_FIX" },

   // Repeat Fix
   { DRS_CMD_REPEAT_LAST_GOV_OR_DRS,      "DRS_CMD_REPEAT_LAST_GOV_OR_DRS" },
   { DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS,    "DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS" },

   // Toggle Pending Fix
   { DRS_CMD_TOGGLE_FIX,                  "DRS_CMD_TOGGLE_FIX" },
   { DRS_CMD_TOGGLE_FIX_ZOOM,             "DRS_CMD_TOGGLE_FIX_ZOOM" },

   // Selection control
   { DRS_CMD_START_SELECT,                "DRS_CMD_START_SELECT" },
   { DRS_CMD_START_SELECT_ZOOM,           "DRS_CMD_START_SELECT_ZOOM" },
   { DRS_CMD_SELECT_ADD_DN,               "DRS_CMD_SELECT_ADD_DN" },
   { DRS_CMD_SELECT_ADD_UP,               "DRS_CMD_SELECT_ADD_UP" },
   { DRS_CMD_SELECT_CTRL_DN,              "DRS_CMD_SELECT_CTRL_DN" },
   { DRS_CMD_SELECT_CTRL_UP,              "DRS_CMD_SELECT_CTRL_UP" },
   { DRS_CMD_SELECT_DELETE,               "DRS_CMD_SELECT_DELETE" },
   { DRS_CMD_END_SELECT,                  "DRS_CMD_END_SELECT" },

   // Small Box Select and Fix:  aka "Quick Select"
   { DRS_CMD_QUICK_SELECT,                "DRS_CMD_QUICK_SELECT" },
   { DRS_CMD_QUICK_SELECT_ZOOM,           "DRS_CMD_QUICK_SELECT_ZOOM" },

   // DRS overrides in Normal Size Steps with Arrow Keys
   { DRS_CMD_OVERRIDE_UP,                 "DRS_CMD_OVERRIDE_UP" },
   { DRS_CMD_OVERRIDE_DOWN,               "DRS_CMD_OVERRIDE_DOWN" },
   { DRS_CMD_OVERRIDE_LEFT,               "DRS_CMD_OVERRIDE_LEFT" },
   { DRS_CMD_OVERRIDE_RIGHT,              "DRS_CMD_OVERRIDE_RIGHT" },

   // DRS overrides in Large Size Steps with <Shift> + Arrow Keys
   { DRS_CMD_OVERRIDE_UP_LARGE,           "DRS_CMD_OVERRIDE_UP_LARGE" },
   { DRS_CMD_OVERRIDE_DOWN_LARGE,         "DRS_CMD_OVERRIDE_DOWN_LARGE" },
   { DRS_CMD_OVERRIDE_LEFT_LARGE,         "DRS_CMD_OVERRIDE_LEFT_LARGE" },
   { DRS_CMD_OVERRIDE_RIGHT_LARGE,        "DRS_CMD_OVERRIDE_RIGHT_LARGE" },

   // DRS overrides in Small Size Steps with <Ctrl> + Arrow Keys
   { DRS_CMD_OVERRIDE_UP_SMALL,           "DRS_CMD_OVERRIDE_UP_SMALL" },
   { DRS_CMD_OVERRIDE_DOWN_SMALL,         "DRS_CMD_OVERRIDE_DOWN_SMALL" },
   { DRS_CMD_OVERRIDE_LEFT_SMALL,         "DRS_CMD_OVERRIDE_LEFT_SMALL" },
   { DRS_CMD_OVERRIDE_RIGHT_SMALL,        "DRS_CMD_OVERRIDE_RIGHT_SMALL" },

   // Prefs
   { DRS_CMD_SHOW_PREFERENCES,            "DRS_CMD_SHOW_PREFERENCES" },

   // Process Type Choices for Keys
   { DRS_CMD_LOW_MOTION_KEY,              "DRS_CMD_LOW_MOTION_KEY" },
   { DRS_CMD_HIGH_MOTION_KEY,             "DRS_CMD_HIGH_MOTION_KEY" },
   { DRS_CMD_HARD_MOTION_KEY,             "DRS_CMD_HARD_MOTION_KEY" },
   { DRS_CMD_MOTION_SUBMODE_1,            "DRS_CMD_MOTION_SUBMODE_1" },
   { DRS_CMD_MOTION_SUBMODE_2,            "DRS_CMD_MOTION_SUBMODE_2" },
   { DRS_CMD_MOTION_SUBMODE_3,            "DRS_CMD_MOTION_SUBMODE_3" },

   { DRS_CMD_REPLACE_KEY,                 "DRS_CMD_REPLACE_KEY" },
   { DRS_CMD_COLOR_BUMP,                  "DRS_CMD_COLOR_BUMP" },
   { DRS_CMD_COLOR_BUMP_PAST,             "DRS_CMD_COLOR_BUMP_PAST" },
   { DRS_CMD_COLOR_BUMP_FUTURE,           "DRS_CMD_COLOR_BUMP_FUTURE" },
   { DRS_CMD_COLOR_BUMP_MONO,             "DRS_CMD_COLOR_BUMP_MONO" },
   { DRS_CMD_COLOR_BUMP_PAST_MONO,        "DRS_CMD_COLOR_BUMP_PAST_MONO" },
   { DRS_CMD_COLOR_BUMP_FUTURE_MONO,      "DRS_CMD_COLOR_BUMP_FUTURE_MONO" },
   { DRS_CMD_COLOR_BUMP_KEY,              "DRS_CMD_COLOR_BUMP_KEY" },
   { DRS_CMD_TOGGLE_MOVE_KEYS,            "DRS_CMD_TOGGLE_MOVE_KEYS" },
   { DRS_CMD_RESET_MOVE_VALUES,           "DRS_CMD_RESET_MOVE_VALUES" },
   { DRS_CMD_TOGGLE_ROTATE_KEYS,          "DRS_CMD_TOGGLE_ROTATE_KEYS" },
   { DRS_CMD_RESET_ROTATE_VALUES,         "DRS_CMD_RESET_ROTATE_VALUES" },
   { DRS_CMD_CHANGE_COLOR_KEY,            "DRS_CMD_CHANGE_COLOR_KEY" },
   { DRS_CMD_RESET_COLOR_VALUES,           "DRS_CMD_RESET_COLOR_VALUES" },
   { DRS_CMD_SELECT_OR_TOGGLE_BORDER,     "DRS_CMD_SELECT_OR_TOGGLE_BORDER" },
   { DRS_CMD_RESET_BORDER_VALUE,          "DRS_CMD_RESET_BORDER_VALUE" },
   { DRS_CMD_CHANGE_GRAIN_KEY,            "DRS_CMD_CHANGE_GRAIN_KEY" },
   { DRS_CMD_RESET_GRAIN_VALUE,           "DRS_CMD_RESET_GRAIN_VALUE" },
   { DRS_CMD_CHANGE_TRANSPARENCY_KEY,     "DRS_CMD_CHANGE_TRANSPARENCY_KEY" },
   { DRS_CMD_RESET_TRANSPARENCY_VALUE,    "DRS_CMD_RESET_TRANSPARENCY_VALUE" },

   { DRS_CMD_RESET_OVERRIDES,             "DRS_CMD_RESET_OVERRIDES" },

   { DRS_CMD_SELECT_SHAPE_KEY,            "DRS_CMD_SELECT_SHAPE_KEY" },

   { DRS_CMD_CHANGE_COLOR_BRIGHTNESS,          "DRS_CMD_CHANGE_COLOR_BRIGHTNESS" },
   { DRS_CMD_CHANGE_COLOR_SATURATION,           "DRS_CMD_CHANGE_COLOR_SATURATION" },
   { DRS_CMD_CHANGE_COLOR_RED,          "DRS_CMD_CHANGE_COLOR_RED" },
   { DRS_CMD_CHANGE_COLOR_GREEN,          "DRS_CMD_CHANGE_COLOR_GREEN" },
   { DRS_CMD_CHANGE_COLOR_BLUE,          "DRS_CMD_CHANGE_COLOR_BLUE" },


};

static CToolCommandTable *drsCommandTable
   = new CToolCommandTable(sizeof(drsCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           drsCommandTableEntries);

//////////////////////////////////////////////////////////////////////

bool CDRSTool::IsDRSActive(void)
{
   return (DRSState.foregroundIsActive ||
           DRSState.backgroundIsActive ||
           (!backgroundCommandQueue.Empty()) ||
           DRSState.fixBeingProcessed ||
           DRSState.fixingMultipleFrames);
}

bool CDRSTool::IsDRSFixPending(void)
{
   return (IsProvisionalPending() && DRSState.fixIsPending);
}

bool CDRSTool::IsGovActive(void)
{
   return getSystemAPI()->IsGOVInProgress();
}

bool CDRSTool::IsGovPending(void)
{
   return getSystemAPI()->IsGOVPending();
}

bool CDRSTool::IsProvisionalPending(void)
{
   return getSystemAPI()->IsProvisionalPending();
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDRSTool::CDRSTool(const string &newToolName, MTI_UINT32 **newFeatureTable)
: CToolObject(newToolName, newFeatureTable)
{
   IniFileName = CPMPIniFileName("DRS");
   IniSection = "DRSParameters";

   currentUserRect.top=0;
   currentUserRect.bottom=0;
   currentUserRect.left=0;
   currentUserRect.right = 0;

   currentUserLassoPointer = 0;
   quickSelectLassoPointer = 0;
   quickSelectLassoPointCount = 0;
   currentUserBezierPointer = 0;
   globalLassoCopy = new POINT[MAX_LASSO_PTS];
   fixedPixelRegionList = 0;
   fixedPixelRegionListSerial = -1;

   iModifierSubProcessType = DRS_SUBPROCESS_NONE;

   // Tool Setup handles

   singleFrameToolSetupHandle = -1;
   singleFrameToolSetupHandleIsValid = false;
   preloadToolSetupHandle = -1;
   preloadToolSetupHandleIsValid = false;
   multiFrameToolSetupHandle = -1;
   multiFrameToolSetupHandleIsValid = false;

   if (!DRSParam.readIniParameters())
      _MTIErrorDialog((string)"Could not read DRS ini file\n"
                     + theError.getMessage());

   userSelectGoZoom = false;

   oldHighLowSubprocessState = DRS_SUBPROCESS_NONE;
   oldHardSubprocessState = DRS_SUBPROCESS_NONE;
   oldReplaceProcessType = DRS_PROCESS_NO_MOTION;
	oldReplaceModeSourceFrameOption = DRS_REPLSRCFRM_INVALID;

   needToClearStatusBar = false;

   backgroundThreadID = NULL;
   backgroundSynchSemaphore = NULL;

	mostRecentInputLoadedFrameIndex = -1;

   drawingTimer.Start();
}

CDRSTool::~CDRSTool()
{
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the DRS Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CDRSTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CDRSTool's base class, i.e., CToolObject
   // This must be done before the CDRSTool initializes itself
   retVal = initBaseToolObject(drsToolNumber, newSystemAPI, drsCommandTable,
                               drsDefaultUserInputConfiguration);
   if (retVal != 0)
      {
        // ERROR: initBaseToolObject failed
        TRACE_0(errout << "Error in DRSTOOL, initBaseToolObject failed "
                       << retVal);
      return retVal;
      }

   InitStates();

   // Spawn the background thread
   backgroundSynchSemaphore = SemAlloc();
   SpawnBackgroundThread();

   mostRecentInputLoadedFrameIndex = -1;

   // Create the DRS Tool's GUI
   CreateDRSGui();

   UpdateDRSGuiButtons();

   return retVal;
}   // end toolInitialize


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              DRS Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CDRSTool::toolShutdown()
{
   int retVal;

   // Synch with and kill the background thread
   KillBackgroundThread();
   TRACE_3(errout << "DRS_BGT: toolShutdown is destroying backgroundSynchSemaphore");
   SemFree(backgroundSynchSemaphore);

   // Destroy DRS' GUI
   DestroyDRSGui();

	GDRSTool = NULL;                 // Say we are destroyed

   delete [] quickSelectLassoPointer;
	delete [] globalLassoCopy;

   // Shutdown the DRS Tool's base class, i.e., CToolObject.
   // This must be done after the DRS Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
}

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// ===================================================================

CToolProcessor* CDRSTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   CToolProcessor *newToolProc;
   int numberOfOutputPorts = (currentToolSetupType == TOOL_SETUP_TYPE_PRELOAD_ONLY)? 0 : 1;

   // Make a new tool processor, called by MakeSimpleToolSetup
	newToolProc =  new CDRSToolProc(
								GetToolNumber(),
								GetToolName(),
								getSystemAPI(),
								newEmergencyStopFlagPtr,
								GetToolProgressMonitor(),
								numberOfOutputPorts,
								&_grainPresets);

   return newToolProc;
}

// ===================================================================
//
// Function:    TryToResolveProvisional
//
// Description: We are given a chance to try to auto accept before
//              the system puts up an obnoxious dialog
//
// Arguments:   None
//
// Returns:     TOOL_RETURN_CODE_NO_ACTION if we didn't have a fix pending,
//              else TOOL_RETURN_CODE_EVENT_CONSUMED
//
// ===================================================================
int CDRSTool::TryToResolveProvisional()
{
   // QQQ METHINKS THIS SHOULD BE DONE IN BACKGROUND THREAD; for now just synch

   TRACE_3(errout << "DRS_BGT: Synchronizing from TryToResolveProvisional()");
   WaitUntilBackgroundThreadIsIdle();

   if (IsDRSFixPending())
   {
      AutoAccept();
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
// Function:    GetImageResolution
// ===================================================================
int CDRSTool::GetImageResolution (void)
{
  const CImageFormat *ifp;
  ifp = getSystemAPI()->getVideoClipImageFormat();

  if (ifp == 0)
   {
    return -1;
   }
  else
   {
    return ifp->getImageResolution();
   }
}

// ===================================================================
// Function:    LoadDRSStatePreferences
// ===================================================================
void CDRSTool::LoadDRSStatePreferences()
{
  DRSState.LoadPreferences();
}


// ===================================================================
//
// Function:    BeginUserSelect
//
// Description: Called when the user first clicks and holds the button/key
//              for drawing a drs fix rectangle, lasso or bezier point.
//
// Arguments:   None
//
// Returns:     If successful event consumed OK, if error event refused.
//
// ===================================================================
int CDRSTool::BeginUserSelect(ESelectShape selectShape, bool goZoom)
{
   userSelectGoZoom = goZoom;

   // The mouseMode flag filters out auto-repeat on PC and
   // multiple key + mouse button presses
   // do nothing if invalid modes
   if (DRSState.mouseMode != DRS_MOUSE_MODE_NONE)
      return TOOL_RETURN_CODE_EVENT_CONSUMED;

   // Because GOV's are done in foreground, we can't start a fix while
   // GOV tool is actually running
   if (IsGovActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // set the color for RECTs, LASSOs, and BEZIERs
   getSystemAPI()->setGraphicsColor(SOLID_YELLOW);

   if (selectShape == SELECT_REGION_RECT
       || selectShape == SELECT_REGION_LASSO)
      {
      // Stupid state flag to lock out preloader
      DRSState.foregroundIsActive = true;

      // Can't Accept until we know the user's intention, so
      // defer the accept until the mouse moves with the left
      // button down
      DRSState.bAutoAcceptOnNextMouseMove = true;

      // Clear any review regions
      getSystemAPI()->ClearReviewRegions(true);

      if (selectShape == SELECT_REGION_LASSO)
         getSystemAPI()->setDRSLassoMode(true);
      else
         getSystemAPI()->setDRSLassoMode(false);

       // begin the RECT stretch or LASSO loop
      getSystemAPI()->setStretchRectangleColor(SOLID_GREEN);
      getSystemAPI()->startStretchRectangleOrLasso();

      // Clear the system's bezier
      getSystemAPI()->ClearBezier(false);

      DRSState.mouseMode = DRS_MOUSE_MODE_STRETCH;
      }
   else if (selectShape == SELECT_REGION_BEZIER)
      {

      // Frickin bezier must be done synchronously because there's
      // only frickin one of them in the entire frickin system!!!!!
      // Does ANYBODY use beziers??

      WaitUntilBackgroundThreadIsIdle();

      // For bezier, do the auto accept immediately
      AutoAccept();

      // Clear any review regions
      getSystemAPI()->ClearReviewRegions(true);

      // Bezier is drawn in GRAPHICS COLOR, not STRETCH COLOR!!
      getSystemAPI()->setStretchRectangleColor(SOLID_GREEN);

      // begin the BEZIER edit
      getSystemAPI()->BezierMouseDown();

      DRSState.mouseMode = DRS_MOUSE_MODE_STRETCH;
      }

   GetToolProgressMonitor()->SetStatusMessage("Selecting fix area");

   return(TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    EndUserSelect
//
// Description: Called to end the selection of a drs fix box or lasso
//              or the button/key release while creating/editing a bezier.
//              When the user completes the rectangle, lasso or closes
//              the bezier, the coordinates are collected and
//              SetupAndProcessFix is called to organize the other information
//              and parameters needed for a fix.
//
// Arguments:   None
//
// Returns:     If succeeded returns event consumed OK, else returns
//              event consumed, operation refused.  Should probably be
//              error codes instead
//
// ===================================================================
int CDRSTool::EndUserSelect()
{
   if (DRSState.selectShape == SELECT_REGION_RECT
       || DRSState.selectShape == SELECT_REGION_LASSO)
      {
      // Filter out auto-repeat on PC and multiple key & mouse button presses
      if (DRSState.mouseMode != DRS_MOUSE_MODE_STRETCH)
         return TOOL_RETURN_CODE_EVENT_CONSUMED;

      // Ask the Base System to stop the stretching rectangle
      getSystemAPI()->stopStretchRectangleOrLasso();
      DRSState.mouseMode = DRS_MOUSE_MODE_NONE;

      // Ask the Base System for the coordinates of the rectangle
      // in image/frame coordinate space
      RECT imageRect;
      POINT *lassoPtr;
      bool selectRegionAvailable;
      if(getSystemAPI()->getStretchRectangleCoordinates(imageRect, lassoPtr)==0)
         selectRegionAvailable = true;
      else
         selectRegionAvailable = false;

      // OK, if no select region is available, we take that to mean that the
      // previous change should be rejected! (only if in "manual accept" mode)
      if (!selectRegionAvailable)
         {
         DRSState.foregroundIsActive = false;  // Nothing to do !
         if (!GetAutoAccept())
            {
            WaitUntilBackgroundThreadIsIdle();
            getSystemAPI()->RejectGOV();
            RejectFix(false);
            }
         }
      else  // Select region IS available
         {
         // Good coordinates
         currentUserRect = imageRect;
         currentUserLassoPointer = CopyLasso(lassoPtr, globalLassoCopy);
         deleteCurrentBezier();

          // Maybe zoom first
         if (userSelectGoZoom)
            getSystemAPI()->zoom(imageRect);

         if (GetAutoAccept())
            {
            // GOV must be accepted in foreground... although it shouldn't be
            // pending at this point...
            if (IsGovPending())
               getSystemAPI()->AcceptGOV();

            // Our job is done - let the background thread do its thing
            DRSState.foregroundIsActive = false;

            // If we are in auto-accept mode, perform the fix in the background
            ProcessNewFixInBackground();
            }
         else
            {
            // AutoAccept will pop up a dialog asking what to do
            AutoAccept();
            ProcessNewFix();

            // Our job is done
            DRSState.foregroundIsActive = false;
            }

         } // end if good coordinates
      }
   else if (DRSState.selectShape == SELECT_REGION_BEZIER)
      {
      // Background thread should be idle at this point because
      // we synched up at the start of the bezier drawing

      // Filter out auto-repeat on PC and multiple key & mouse button presses
      if (DRSState.mouseMode != DRS_MOUSE_MODE_STRETCH
          && DRSState.mouseMode != DRS_MOUSE_MODE_SELECT_ADD
          && DRSState.mouseMode != DRS_MOUSE_MODE_SELECT_CTRL)
         return TOOL_RETURN_CODE_EVENT_CONSUMED;

      // pass event to system's bezier editor
      getSystemAPI()->BezierMouseUp();

      DRSState.mouseMode = DRS_MOUSE_MODE_NONE;

      // if there's a new Bezier and if it's closed
      if (getSystemAPI()->IsBezierClosed())
         {
         // At this point, make sure nothing is pending!
         AutoAccept();

         if (!IsProvisionalPending())
            {
            // no lasso here
            currentUserLassoPointer = NULL;

            // the new bezier might not overlap the frame (!)
            getNewBezier();

            // Clear the system's bezier
            getSystemAPI()->ClearBezier(false);

            if (currentUserBezierPointer != NULL) // overlaps frame
               {
               // valid Bezier region
               ProcessNewFix();
               }
            }
         }
      }

   // Flush this - will get reset to true on next button down event
   DRSState.bAutoAcceptOnNextMouseMove = false;

   // No longer doing anything in the foreground!
   DRSState.foregroundIsActive = false;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

POINT* CDRSTool::CopyLasso(const POINT *srcLasso, POINT *dstLasso)
{
   // Make a copy of the  lasso

   if(srcLasso == NULL || dstLasso == NULL)
      return NULL;    // oops, no lasso, so just return NULL pointer

   for (int i = 0; ; ++i)
      {
      dstLasso[i] = srcLasso[i];
      if (i > 2
          && srcLasso[i].x == srcLasso[0].x
          && srcLasso[i].y == srcLasso[0].y)
         break;
      }

   return dstLasso;  // return ptr to new copy
}

int CDRSTool::SelectShiftDown()
{
   // Start of shift-click or shift-drag for editing the bezier

   // do nothing if invalid modes
   if (DRSState.selectShape != SELECT_REGION_BEZIER
       || DRSState.mouseMode != DRS_MOUSE_MODE_NONE)
      return TOOL_RETURN_CODE_EVENT_CONSUMED;

   // pass event to system's bezier editor
   getSystemAPI()->BezierShiftMouseDown();

   DRSState.mouseMode = DRS_MOUSE_MODE_SELECT_ADD;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CDRSTool::SelectCtrlDown()
{
   // Start of ctrl-click or ctrl-drag for editing the bezier

   // do nothing if invalid modes
   if (DRSState.selectShape != SELECT_REGION_BEZIER
       || DRSState.mouseMode != DRS_MOUSE_MODE_NONE)
      return TOOL_RETURN_CODE_EVENT_CONSUMED;

   // pass event to system's bezier editor
   getSystemAPI()->BezierCtrlMouseDown();

   DRSState.mouseMode = DRS_MOUSE_MODE_SELECT_CTRL;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

void CDRSTool::deleteCurrentBezier()
{
   // Delete the current bezier
   getSystemAPI()->FreeBezier(currentUserBezierPointer);
   currentUserBezierPointer = NULL;
}

void CDRSTool::getNewBezier()
{
   // Get the most recently closed bezier from the system's bezier editor
   // Sets currentUserBezierPointer = NULL
   deleteCurrentBezier();

   // have the bezier editor give you the extent of the
   // current spline, ANDed with the frame (because this
   // will be the dashed box you want to see). If the
   // spline didn't overlap the frame at all, exit with
   // currentUserBezierPointer = NULL
   if (!getSystemAPI()->GetBezierExtent(currentUserRect))
      return;

   // now get the spline itself, which may be NULL
   currentUserBezierPointer = getSystemAPI()->GetBezier();
}

// ===================================================================
//
// Function:    GetPaddedRect
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
RECT CDRSTool::GetPaddedRect(RECT rect, int rowPadding, int colPadding)
{
   RECT newRect;
   newRect.top = rect.top - rowPadding;
   newRect.bottom = rect.bottom + rowPadding;
   newRect.left = rect.left - colPadding;
   newRect.right = rect.right + colPadding;

   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == 0)
      return newRect;

   RECT activeRect = imageFormat->getActivePictureRect();

   // Clip padded rectangle to the active image area, exclusive of
   // vertical or horizontal interval areas
   if (newRect.top < activeRect.top)
      newRect.top = activeRect.top;
   if (newRect.bottom > activeRect.bottom)
      newRect.bottom = activeRect.bottom;
   if (newRect.left < activeRect.left)
      newRect.left = activeRect.left;
   if (newRect.right > activeRect.right)
      newRect.right = activeRect.right;
   return (newRect);
}

// ===================================================================
//
// Function:    TogglePendingFix
//
// Description:
//
// Arguments:   bool goZoom    zoom with fix centered if true
//
// Returns:
//
// ===================================================================
int CDRSTool::TogglePendingFix(bool goZoom)
{
   // ALWAYS synch with the background thread before mucking with
   // the provisional        ... should be ok, just changes display
   //WaitUntilBackgroundThreadIsIdle();

   getSystemAPI()->ToggleProvisional(goZoom);

   return(TOOL_RETURN_CODE_EVENT_CONSUMED);
}

// ===================================================================
//
// Function:    AutoAccept
//
// Description: If a fix is pending and we are in auto-accept mode,
//              accept the fix
//
// Arguments:   None
//
// Returns:     True if fix was accepted or there was no fix, else false
//
// ===================================================================
bool CDRSTool::AutoAccept()
{
   // DO NOT RUN THIS FROM THE BACKGROUND THREAD -
   // Only call AcceptFixFromBackgroundThread from the thread
   WaitUntilBackgroundThreadIsIdle();

   bool retVal = true;

   MTIassert(! (IsGovPending() && DRSState.fixIsPending));

   // Per Dave, we don't need the GOV tool to obey auto-accept
   getSystemAPI()->AcceptGOV();

   if (IsDRSFixPending())
   {
      if (GetAutoAccept())
      {
         AcceptFix(false);
      }
      else
      {
         switch (getSystemAPI()->ShowAcceptOrRejectDialog(&DRSState.bAutoAccept))
         {
            case PENDING_CHANGE_ACCEPT:
               //getSystemAPI()->AcceptGOV();    // no longer conditional
               AcceptFix(false);
               break;
            case PENDING_CHANGE_REJECT:
               //getSystemAPI()->RejectGOV();    // no longer do this
               RejectFix(false);
               break;
            case PENDING_CHANGE_NO_ACTION:
            default:
               retVal = false;  // Wasn't resolved
               break;
         }
         if (GetAutoAccept())
         {
            CIniFile *ini = CreateIniFile (CPMPIniFileName ("DRS"));
            if (ini != 0)
            {
               ini->WriteBool ("Preferences", "AutoAccept", true);
               DeleteIniFile(ini);
            }
         }
      }
   }
   else if (DRSState.fixIsPending == false)
   {
      // In case this is pending from a 'null' fix
      getSystemAPI()->ClearProvisional(false);
   }
   else
   {
      // This "can't happen"
//      MTIassert(IsProvisionalPending() == true);
      DRSState.fixIsPending = false;
   }

   return retVal;
}

// ===================================================================
//
// Function:    AcceptFix
//
// Description: Keep the pending fix
//
// Arguments:   bool goToFrame      if true, go to the frame with the fix
//                                  if false, stay on the current frame
//
// Returns:
//
// ===================================================================
int CDRSTool::AcceptFix(bool goToFrame)
{
   // DO NOT RUN THIS FROM THE BACKGROUND THREAD -
   // Only call AcceptFixFromBackgroundThread from the thread

   if (GetAutoAccept() && IsDRSActive())
   {
      AcceptFixInBackground(goToFrame);
   }
   else
   {
      WaitUntilBackgroundThreadIsIdle();
      WaitForProcessingToFinish();

      // GOV SHOULD NOT BE PENDING AT THIS POINT!
      bool isFixOrGovPending = IsProvisionalPending();
      bool fixIsPending = DRSState.fixIsPending;
      bool govIsPending = IsGovPending();
      MTIassert(isFixOrGovPending == (fixIsPending || govIsPending));

      if (IsDRSFixPending())
      {
         //int provisionalFrameIndex = getSystemAPI()->GetProvisionalFrameIndex();
         //bool showingProvisionalFrame =
             //(provisionalFrameIndex == getSystemAPI()->getLastFrameIndex());

         ChangePendingToResolvedInProgressList(); // Before the AcceptProvisional

         getSystemAPI()->AcceptProvisional(goToFrame);
         GetToolProgressMonitor()->SetStatusMessage("Fix accepted");
         GetToolProgressMonitor()->SetToolMessage(NULL);

         // refresh frame to show the user
         // ONLY DO THIS IF goToFrame IS FALSE AND WE ARE PRESENTLY SHOWING
         // THE ACCEPTED FRAME! (If goToFrame is true, the Provisional will
         // take care of refreshing)
         // Hey, wait... why do we need to do this at all? QQQ
         //if (showingProvisionalFrame && !goToFrame)
            //getSystemAPI()->refreshFrame();

         DRSState.ResetOverridesIfNotSticky ();

		 repairDRS.ReleaseMemory();

         // Tell GOV tool what the latest fix region is
         SetGOVRepeatShape();

         // QQQ check to make sure somthing above stopped the player
         getSystemAPI()->refreshFrameCached();  // redraw to get rid of outline

         DRSState.inhibitRepeatFixOp = false;
      }

      DRSState.fixIsPending = false;  // outside "if" in case provis not pending
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    AcceptFixFromBackgroundThread
//
// Description: Keep the pending fix - this version to be used when
//              doing the accepting from a background thread
//
// ===================================================================
int CDRSTool::AcceptFixFromBackgroundThread()
{
   // GAG ME! We need to spin until the main thread receives the
   // onToolProcessingStatus() callback!

   int count = 1000;  // a huge number
   while (DRSState.fixBeingProcessed && (--count > 0))
   {
      MTImillisleep(1);
   }
   MTIassert(DRSState.fixBeingProcessed == false);

   // foreground synch hack
   DRSState.backgroundAcceptInProgress = true;

   // GOV SHOULD NOT BE PENDING AT THIS POINT!
   bool isFixOrGovPending = IsProvisionalPending();
   bool fixIsPending = DRSState.fixIsPending;
   bool govIsPending = IsGovPending();
   MTIassert(isFixOrGovPending == (fixIsPending || govIsPending));

   if (IsDRSFixPending())
   {
      //int provisionalFrameIndex = getSystemAPI()->GetProvisionalFrameIndex();
      //bool showingProvisionalFrame =
          //(provisionalFrameIndex == getSystemAPI()->getLastFrameIndex());

      ChangePendingToResolvedInProgressList(); // Before the AcceptProvisional

      getSystemAPI()->AcceptProvisionalInBackground();

      //// These need to be done by the caller, else they might get applied
      //// incorrectly to the fix that forced this auto-accept because they
      //// are cleared after that fix gathers its parameters!
      //// (I leave them here anyhow for paranoia)
      DRSState.ResetOverridesIfNotSticky ();
      DRSState.updateGUIButtonsNow = true;

      repairDRS.ReleaseMemory();

      // Tell GOV tool what the latest fix region is
      SetGOVRepeatShape();

      // Stupid hack because Larry doesn't want ~ key to work after reject!
      DRSState.inhibitRepeatFixOp = false;

      GetToolProgressMonitor()->SetStatusMessage("Fix accepted");
      GetToolProgressMonitor()->SetToolMessage(NULL);
   }

   DRSState.fixIsPending = false;  // outside "if" in case provis not pending
   DRSState.backgroundAcceptInProgress = false;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    RejectFix
//
// Description: Reject the pending fix.
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CDRSTool::RejectFix(bool goToFrame)
{
   // DO NOT RUN THIS FROM THE BACKGROUND THREAD -
   // Only call RejectFixFromBackgroundThread from the thread

   if (GetAutoAccept() && IsDRSActive())
   {
      RejectFixInBackground(goToFrame);
   }
   else
   {
      WaitUntilBackgroundThreadIsIdle();
      WaitForProcessingToFinish();

      // GOV SHOULD NOT BE PENDING AT THIS POINT!
      bool isFixOrGovPending = IsProvisionalPending();
      bool fixIsPending = DRSState.fixIsPending;
      bool govIsPending = IsGovPending();
      MTIassert(isFixOrGovPending == (fixIsPending || govIsPending));

      if (IsDRSFixPending())
      {
         ChangePendingToResolvedInProgressList(); // Before the RejectProvisional

         getSystemAPI()->RejectProvisional(goToFrame);
         GetToolProgressMonitor()->SetStatusMessage("Fix rejected");
         GetToolProgressMonitor()->SetToolMessage(NULL);

         // refresh frame to show the user
         getSystemAPI()->refreshFrame();

         DRSState.ResetOverridesIfNotSticky ();
         DRSState.updateGUIButtonsNow = true;

         repairDRS.ReleaseMemory();

         // Tell GOV tool to forget any shape we passed it
         getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_NONE, currentUserRect,
                                           NULL, NULL);

         // Stupid hack because Larry doesn't want ~ key to work after reject!
         DRSState.inhibitRepeatFixOp = true;
      }

      DRSState.fixIsPending = false;  // outside "if" in case provis not pending
   }

   return(TOOL_RETURN_CODE_EVENT_CONSUMED);
}

// ===================================================================
//
// Function:    RejectFixFromBackgroundThread
//
// Description: Cancel the pending fix - this version to be used when
//              doing the rejection from a background thread
//
// ===================================================================
int CDRSTool::RejectFixFromBackgroundThread()
{
   if (IsDRSFixPending())
   {
      //int provisionalFrameIndex = getSystemAPI()->GetProvisionalFrameIndex();
      //bool showingProvisionalFrame =
          //(provisionalFrameIndex == getSystemAPI()->getLastFrameIndex());

      ChangePendingToResolvedInProgressList();

      getSystemAPI()->RejectProvisionalInBackground();

      DRSState.ResetOverridesIfNotSticky ();

      repairDRS.ReleaseMemory();

      // Tell GOV tool what the latest fix region is
      SetGOVRepeatShape();

      ChangePendingToResolvedInProgressList();

      // Stupid hack because Larry doesn't want ~ key to work after reject!
      DRSState.inhibitRepeatFixOp = true;
   }

   DRSState.fixIsPending = false;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// New macro stuff
//
// ===================================================================

void CDRSTool::StartMacro()
{
   ApplyPendingFixToMarkedRange();
   UpdateDRSGuiButtons();
}

void CDRSTool::StopMacro()
{
   StopToolProcessing();
}

bool CDRSTool::IsOkToStartMacro()
{
//   WaitUntilBackgroundThreadIsIdle();    DON'T HANG MAIN THREAD HERE!

   // Short circuit if nothing to rerun!
   if ((!IsDRSFixPending()) || getSystemAPI()->IsGOVPending())
      return false;

   // Only allow 'hard motion' fixes to be applied to multiple frames
   if (GetMainProcessType() != DRS_PROCESS_HARD_MOTION)
      return false;

   // Check Mark In and Mark Out
   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();
   if (markIn < 0 || markOut < 0 || markOut <= markIn)
   {
      return false;
   }


   return true;
}

bool CDRSTool::IsOkToStopMacro()
{
   return (DRSState.fixBeingProcessed && DRSState.fixingMultipleFrames);
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: DRS Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CDRSTool::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   if (IsGovActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   if (needToClearStatusBar)
   {
      needToClearStatusBar = false;
      SetStatusIdle();
   }

   int Result = TOOL_RETURN_CODE_EVENT_CONSUMED;
   SOverrides overrides;
   int commandNumber = toolCommand.getCommandNumber();
   const CToolCommandTable *commandTable = getCommandTable();
   const char* commandName = commandTable->findCommandName(commandNumber);

   if (commandName == NULL)
      commandName = "????";
   TRACE_3(errout << "DRS command: "
                  << ((commandName == NULL)? "????" : commandName)
                  << " (" << commandNumber << ")" );

   switch (commandNumber)
   {
      //case DRS_CMD_ACCEPT_FIX:          now done in background
      //case DRS_CMD_ACCEPT_IN_PLACE:     now done in background
      case DRS_CMD_ACCEPT_AND_APPLY_ALL:
      //case DRS_CMD_REJECT_FIX:          now done in background
      case DRS_CMD_REPEAT_LAST_GOV_OR_DRS:
      case DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS:
      //case DRS_CMD_TOGGLE_FIX:            ok?
      //case DRS_CMD_TOGGLE_FIX_ZOOM:       ok?
      case DRS_CMD_SELECT_ADD_DN :
      case DRS_CMD_SELECT_CTRL_DN :
         // These need to wait until the fix is done

         // Well we're just going to ignore these if processing is active
         // (keys: Shift-G, ~, Shift-~,  Shift-LeftMouseButton, Ctrl-LMB)
         // Better than appearing to hang the app!!
         //WaitUntilBackgroundThreadIsIdle();
         //WaitForProcessingToFinish();     // if fix was done in forground
         if (IsDRSActive())
            return TOOL_RETURN_CODE_NO_ACTION;
         break;

      default:
         break;
   }

   // Dispatch loop for drs commands

   bool bWriteIni = false;

   switch (commandNumber)
   {
     case DRS_CMD_TOGGLE_FIX:
       TogglePendingFix(false);
       break;

     case DRS_CMD_TOGGLE_FIX_ZOOM:
       getSystemAPI()->zoom(currentUserRect);
       TogglePendingFix(false);
       break;

     case DRS_CMD_START_SELECT:
       BeginUserSelect(DRSState.selectShape, false);
       break;

     case DRS_CMD_START_SELECT_ZOOM:
       BeginUserSelect(DRSState.selectShape, true);
       break;

// Shift+LButton is overloaded
     case DRS_CMD_SELECT_ADD_DN :
       if (DRSState.selectShape != SELECT_REGION_BEZIER)
        {
         BeginUserSelect(DRSState.selectShape, true);
        }
       if (!IsProvisionalPending())
        {
         SelectShiftDown();
        }
       break;

     case DRS_CMD_SELECT_CTRL_DN :
       // If we're in RECT mode, temporarily switch to LASSO mode
       if (DRSState.selectShape == SELECT_REGION_RECT)
       {
          BeginUserSelect(SELECT_REGION_LASSO, false);
       }
       // If we're in LASSO mode, temporarily switch to RECT mode
       if (DRSState.selectShape == SELECT_REGION_LASSO)
       {
          BeginUserSelect(SELECT_REGION_RECT, false);
       }
       else if (!IsProvisionalPending())
        {
         SelectCtrlDown();
        }
       break;

     case DRS_CMD_END_SELECT:
     case DRS_CMD_SELECT_ADD_UP:
     case DRS_CMD_SELECT_CTRL_UP:
        EndUserSelect();
     break;

     case DRS_CMD_SELECT_DELETE:
       if (DRSState.mouseMode == DRS_MOUSE_MODE_NONE)
          getSystemAPI()->ClearBezier(true);
       break;

     case DRS_CMD_ACCEPT_FIX:
       AcceptFix(true);
       break;

     case DRS_CMD_ACCEPT_IN_PLACE:
       AcceptFix(false);
       break;

     case DRS_CMD_ACCEPT_AND_APPLY_ALL:
       if ((DRSState.mainProcessType == DRS_PROCESS_LOW_MOTION
            || DRSState.mainProcessType == DRS_PROCESS_HIGH_MOTION
            || DRSState.mainProcessType == DRS_PROCESS_NO_MOTION)
       && DRSState.subProcessType == DRS_SUBPROCESS_C)
       {
         RunAutoReplace();
       }
       else
       {
         StartMacro();
       }

       break;

     case DRS_CMD_STOP_ALL_PROCESSING:
       StopMacro();
       break;

     case DRS_CMD_REJECT_FIX:
       RejectFix(true);
       break;

     case DRS_CMD_REPEAT_LAST_GOV_OR_DRS:
     case DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS:

       if (DRSState.inhibitRepeatFixOp)
       {
           // Operation is inhibited becaue either there was no fix or
           // the previous fix was rejected
           break;
       }
       getSystemAPI()->ClearReviewRegions(true);
       if ((commandNumber == DRS_CMD_REPEAT_LAST_GOV_OR_DRS &&
             DRSState.bLastActionWasGOV == true)
          ||
           (commandNumber == DRS_CMD_REPEAT_TOGGLE_GOV_OR_DRS &&
             DRSState.bLastActionWasGOV == false)
          )
       {
          // Wrong phase, ignore this command
          break;
       }
       // If we are in auto-accept mode, perform the fix in the background.
       // The DRS params should all be set up as desired at this point
       // (we are either re-using the previous DRS params, or GOV has notified
       // us of a new shape and we replaced that already for TOGGLE version)!
       if (GetAutoAccept())
       {
          // GOV must be accepted in foreground for now
          if (IsGovPending())
             getSystemAPI()->AcceptGOV();

          ProcessNewFixInBackground();
       }
       else
       {
          AutoAccept();  // will actually pop up a dialog in this mode
          ProcessNewFix();
       }
       break;

     case DRS_CMD_QUICK_SELECT_ZOOM:

       // Need to clear GOV in the foreground! DRS fix will get accepted in
       // in the background so we can stack them up
       if (IsGovPending())
          getSystemAPI()->AcceptGOV();

       getSystemAPI()->ClearReviewRegions(true);

       // this call either sets the currentUserLassoPointer
       // to NULL or creates an elliptical lasso
       if (GetNewUserRectangle()) // sets currentUserLassoPointer
        {
         deleteCurrentBezier();  // forget leftover bezier
         // Clear the system's bezier
         getSystemAPI()->ClearBezier(false);

         // Zoom first
         getSystemAPI()->zoom(currentUserRect);

         // normal DRS call
         ProcessNewFixInBackground();
        }
       else
        {
         // Cursor is not inside the displayed image
         Result |= TOOL_RETURN_CODE_ERROR;
        }
       break;

     case DRS_CMD_QUICK_SELECT:

       // Need to clear GOV in the foreground! DRS fix will get accepted in
       // in the background so we can stack them up
       if (IsGovPending())
          getSystemAPI()->AcceptGOV();

       getSystemAPI()->ClearReviewRegions(true);

       // this call either sets the currentUserLassoPointer
       // to NULL or creates an elliptical lasso
       if (GetNewUserRectangle()) // sets currentUserLassoPointer
        {
         deleteCurrentBezier();  // forget leftover bezier
         // Clear the system's bezier
         getSystemAPI()->ClearBezier(false);

         // normal DRS call
        ProcessNewFixInBackground();
        }
       else
        {
         // Cursor is not inside the displayed image
         Result |= TOOL_RETURN_CODE_ERROR;
        }
       break;


//     case DRS_CMD_ADJUST_COLOR:
//       DRSState.bAdjustColorBrightness = !DRSState.bAdjustColorBrightness;
//       break;

     case DRS_CMD_MASK_IMAGE :
       DRSState.bMaskImage = !DRSState.bMaskImage;
       break;

     case DRS_CMD_RECTANGLE_MODE:
       DRSState.selectShape = SELECT_REGION_RECT;
       getSystemAPI()->ClearBezier(false);  // Clear the system's bezier
       bWriteIni = true;
       break;

     case DRS_CMD_LASSO_MODE:
       DRSState.selectShape = SELECT_REGION_LASSO;
       getSystemAPI()->ClearBezier(false);  // Clear the system's bezier
       bWriteIni = true;
       break;

     case DRS_CMD_BEZIER_MODE:
       DRSState.selectShape = SELECT_REGION_BEZIER;
       bWriteIni = true;
       break;

     case DRS_CMD_SWITCH_SELECT_SHAPE:
       // Cycle through the different region selection shapes
       switch(GetSelectShape())
         {
         case SELECT_REGION_RECT:
            DRSState.selectShape = SELECT_REGION_LASSO;
            break;
         case SELECT_REGION_LASSO:
            DRSState.selectShape = SELECT_REGION_BEZIER;
            break;
         case SELECT_REGION_BEZIER:
            DRSState.selectShape = SELECT_REGION_RECT;
            getSystemAPI()->ClearBezier(false);  // Clear the system's bezier
            break;
         }
       bWriteIni = true;
       break;

     case DRS_CMD_TOGGLE_BORDER:
       // Cycle blend border on/off
       switch(GetBorder())
         {
         case BLEND_TYPE_LINEAR:
            DRSState.iBlendType = BLEND_TYPE_NONE;
            break;
         case BLEND_TYPE_NONE:
            DRSState.iBlendType = BLEND_TYPE_LINEAR;
			break;

		 default:
		    break;
         }
       bWriteIni = true;
       OverrideCommand(commandNumber);
       break;

     case DRS_CMD_SELECT_OR_TOGGLE_BORDER:
       if (DRSState.iOverrideMode != OM_CHANGE_BORDER)
       {
          DRSState.iOverrideMode = OM_CHANGE_BORDER;
       }
       else
       {
          // Cycle blend border on/off
          switch(GetBorder())
            {
            case BLEND_TYPE_LINEAR:
               DRSState.iBlendType = BLEND_TYPE_NONE;
               break;
            case BLEND_TYPE_NONE:
               DRSState.iBlendType = BLEND_TYPE_LINEAR;
			   break;
			default:
			   break;
            }
          // Execute DRS_CMD_TOGGLE_BORDER, not current commandNumber
          OverrideCommand(DRS_CMD_TOGGLE_BORDER);
       }
       bWriteIni = true;
       break;

     case DRS_CMD_MOVE_UNDER:
     case DRS_CMD_MOVE_UNDER_KEY:
       DRSState.iOverrideMode = OM_MOVE_UNDER;
       bWriteIni = true;
       break;

     case DRS_CMD_MOVE_OVER:
     case DRS_CMD_MOVE_OVER_KEY:
       DRSState.iOverrideMode = OM_MOVE_OVER;
       bWriteIni = true;
       break;

     case DRS_CMD_TOGGLE_MOVE_KEYS:
       if (DRSState.iOverrideMode == OM_MOVE_OVER)
          DRSState.iOverrideMode = OM_MOVE_UNDER;
       else
          DRSState.iOverrideMode = OM_MOVE_OVER;
       bWriteIni = true;
       break;

     case DRS_CMD_CHANGE_COLOR_KEY:
       switch (DRSState.iOverrideMode)
       {
         case OM_CHANGE_COLOR_BRIGHTNESS:
            DRSState.iOverrideMode = OM_CHANGE_COLOR_SATURATION;
            break;
         case OM_CHANGE_COLOR_SATURATION:
            DRSState.iOverrideMode = OM_CHANGE_COLOR_RED;
            break;
         case OM_CHANGE_COLOR_RED:
            DRSState.iOverrideMode = OM_CHANGE_COLOR_GREEN;
            break;
         case OM_CHANGE_COLOR_GREEN:
            DRSState.iOverrideMode = OM_CHANGE_COLOR_BLUE;
            break;
         default:
         case OM_CHANGE_COLOR_BLUE:
            DRSState.iOverrideMode = OM_CHANGE_COLOR_BRIGHTNESS;
            break;
       }

       bWriteIni = true;
       break;

     case DRS_CMD_CHANGE_COLOR_BRIGHTNESS:
       DRSState.iOverrideMode = OM_CHANGE_COLOR_BRIGHTNESS;
       bWriteIni = true;
       break;
     case DRS_CMD_CHANGE_COLOR_SATURATION:
       DRSState.iOverrideMode = OM_CHANGE_COLOR_SATURATION;
       bWriteIni = true;
       break;
     case DRS_CMD_CHANGE_COLOR_RED:
       DRSState.iOverrideMode = OM_CHANGE_COLOR_RED;
       bWriteIni = true;
       break;
     case DRS_CMD_CHANGE_COLOR_GREEN:
       DRSState.iOverrideMode = OM_CHANGE_COLOR_GREEN;
       bWriteIni = true;
       break;
     case DRS_CMD_CHANGE_COLOR_BLUE:
       DRSState.iOverrideMode = OM_CHANGE_COLOR_BLUE;
       bWriteIni = true;
       break;

     case DRS_CMD_CHANGE_GRAIN:
     case DRS_CMD_CHANGE_GRAIN_KEY:
       DRSState.iOverrideMode = OM_CHANGE_GRAIN;
       bWriteIni = true;
       break;

     case DRS_CMD_CHANGE_BORDER:
     case DRS_CMD_CHANGE_BORDER_KEY:
       DRSState.iOverrideMode = OM_CHANGE_BORDER;
       bWriteIni = true;
       break;

     case DRS_CMD_ROTATE_OVER:
     case DRS_CMD_ROTATE_OVER_KEY:
       DRSState.iOverrideMode = OM_ROTATE_OVER;
       bWriteIni = true;
       break;

     case DRS_CMD_ROTATE_UNDER:
     case DRS_CMD_ROTATE_UNDER_KEY:
       DRSState.iOverrideMode = OM_ROTATE_UNDER;
       bWriteIni = true;
       break;

     case DRS_CMD_TOGGLE_ROTATE_KEYS:
       if (DRSState.iOverrideMode == OM_ROTATE_OVER)
          DRSState.iOverrideMode = OM_ROTATE_UNDER;
       else
          DRSState.iOverrideMode = OM_ROTATE_OVER;
       bWriteIni = true;
       break;

     case DRS_CMD_CHANGE_TRANSPARENCY:
     case DRS_CMD_CHANGE_TRANSPARENCY_KEY:
       DRSState.iOverrideMode = OM_CHANGE_TRANSPARENCY;
       bWriteIni = true;
       break;

     case DRS_CMD_RESET_OVERRIDES:
       DRSState.ResetOverrides ();      //
		 ReprocessFix(true);
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     case DRS_CMD_RESET_MOVE_VALUES:
       overrides = GetOverrides();
       overrides.fOverrideMoveOverX = 0.F;
       overrides.fOverrideMoveOverY = 0.F;
       overrides.fOverrideMoveUnderX = 0.F;
       overrides.fOverrideMoveUnderY = 0.F;
       SetOverrides(overrides);                // all at once!
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     case DRS_CMD_RESET_ROTATE_VALUES:
       overrides = GetOverrides();
       overrides.fOverrideRotateOver = 0.F;
       overrides.fOverrideRotateUnder = 0.F;
       SetOverrides(overrides);                // all at once!
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     case DRS_CMD_RESET_COLOR_VALUES:
       SetOverrideColorBrightness(0);
       SetOverrideColorSaturation(0);
       SetOverrideColorRed(0);
       SetOverrideColorGreen(0);
       SetOverrideColorBlue(0);
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     case DRS_CMD_RESET_BORDER_VALUE:
       SetOverrideBorderRadius(0);
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     case DRS_CMD_RESET_GRAIN_VALUE:
		 SetOverrideGrainStrength(DRSState.Overrides.iOverrideGrainStrengthDefault);
       bWriteIni = true;            // ? Are overrides written? QQQ
		 break;

     case DRS_CMD_RESET_TRANSPARENCY_VALUE:
       SetOverrideTransparency(0);
       bWriteIni = true;            // ? Are overrides written? QQQ
       break;

     // DRS Process Type Keys
     //  work in combination with process type modifier commands
     // NOTE: subprocess must be set indirectly via iModifierSubProcessType,
     //       NOT directly by whacking DRSState.subProcessType
     case DRS_CMD_LOW_MOTION_KEY:
     case DRS_CMD_HIGH_MOTION_KEY:
     case DRS_CMD_HARD_MOTION_KEY:
	  case DRS_CMD_REPLACE_KEY:
	  case DRS_CMD_COLOR_BUMP_KEY:
	  case DRS_CMD_MOTION_SUBMODE_1:
     case DRS_CMD_MOTION_SUBMODE_2:
     case DRS_CMD_MOTION_SUBMODE_3:
     case DRS_CMD_SELECT_SHAPE_KEY:
     case DRS_CMD_SELECT_BORDER_KEY:
     case DRS_CMD_SELECT_OVERRIDE_KEY:
	   DRSProcessTypeKeyCommand(commandNumber);
       bWriteIni = true;
       break;

     // DRS Process Type Commands
     // These commands may be originate directly from the DRS GUI or
     // indirectly from Process type and Process type modifier keys
     case DRS_CMD_NO_MOTION:
     case DRS_CMD_LOW_MOTION:
     case DRS_CMD_HIGH_MOTION:
     case DRS_CMD_HARD_MOTION:
     case DRS_CMD_NO_MOTION_PAST:
     case DRS_CMD_LOW_MOTION_PAST:
     case DRS_CMD_HIGH_MOTION_PAST:
     case DRS_CMD_NO_MOTION_FUTURE:
     case DRS_CMD_LOW_MOTION_FUTURE:
     case DRS_CMD_HIGH_MOTION_FUTURE:
     case DRS_CMD_NO_MOTION_IMPORT:
	  case DRS_CMD_LOW_MOTION_IMPORT:
     case DRS_CMD_HIGH_MOTION_IMPORT:
	  case DRS_CMD_HARD_MOTION_EDGE:
	  case DRS_CMD_COLOR_BUMP:
	  case DRS_CMD_COLOR_BUMP_PAST:
	  case DRS_CMD_COLOR_BUMP_FUTURE:
	  case DRS_CMD_COLOR_BUMP_MONO:
	  case DRS_CMD_COLOR_BUMP_PAST_MONO:
	  case DRS_CMD_COLOR_BUMP_FUTURE_MONO:
	    DRSProcessTypeCommand(commandNumber);
       bWriteIni = true;
       break;

     // Move Fix (Arrow Keys) Commands
     case DRS_CMD_OVERRIDE_UP:
     case DRS_CMD_OVERRIDE_DOWN:
     case DRS_CMD_OVERRIDE_LEFT:
     case DRS_CMD_OVERRIDE_RIGHT:
     case DRS_CMD_OVERRIDE_UP_LARGE:
     case DRS_CMD_OVERRIDE_DOWN_LARGE:
     case DRS_CMD_OVERRIDE_LEFT_LARGE:
     case DRS_CMD_OVERRIDE_RIGHT_LARGE:
     case DRS_CMD_OVERRIDE_UP_SMALL:
     case DRS_CMD_OVERRIDE_DOWN_SMALL:
     case DRS_CMD_OVERRIDE_LEFT_SMALL:
     case DRS_CMD_OVERRIDE_RIGHT_SMALL:
       OverrideCommand(commandNumber);
       break;

     case DRS_CMD_SHOW_PREFERENCES:
       ShowPreferencesWindow();
       LoadDRSStatePreferences();
       break;

     default:
       Result = TOOL_RETURN_CODE_NO_ACTION;
     break;
   }

   // Update which GUI buttons appear "pressed"
   UpdateDRSGuiButtons();

   if (bWriteIni)
    {
     SaveStatesToIniFile();
    }

   return Result;
}

void CDRSTool::SaveStatesToIniFile()
{
    CDRSParameters dp;
    SDRSStateParameters dsp;

    dsp.iMainProcessType = DRSState.mainProcessType;
    dsp.iSubProcessType = DRSState.subProcessType;
    dsp.iSelectMode = GetSelectShape();
    dsp.iBlendType = DRSState.iBlendType;
    dsp.bUseGrainPresets = GetUseGrainPresets();
    dsp.iOverrideMode = DRSState.iOverrideMode;
    dsp.bOverridesAreSticky = DRSState.bOverridesAreSticky;
    dsp.bMoveOverIsSticky = DRSState.bMoveOverIsSticky;
    dsp.bMoveUnderIsSticky = DRSState.bMoveUnderIsSticky;
    dsp.bRotateOverIsSticky = DRSState.bRotateOverIsSticky;
    dsp.bRotateUnderIsSticky = DRSState.bRotateUnderIsSticky;
    dsp.bColorIsSticky = DRSState.bColorIsSticky;
    dsp.bBorderIsSticky = DRSState.bBorderIsSticky;
    dsp.bGrainIsSticky = DRSState.bGrainIsSticky;
    dsp.bTransparencyIsSticky = DRSState.bTransparencyIsSticky;

    dp.setFileStateParameters (dsp);
    if (dp.writeStateParameters() == false)
    {
       TRACE_0 (errout << "DRS: Unable to write State Parameters");
    }
}

// ===================================================================
//
// Function:    DRSProcessTypeKeyCommand
//
// Description: DRS Tool's Command Handler for DRS Process Type key commands.
//              Called by onToolCommand.  Combines basic process type
//              implied by command with the process type modifier stored
//              in iModifierSubProcessType.
//
//              Re-entrantly calls onToolCommand with main and subprocess
//              types
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::DRSProcessTypeKeyCommand(int toolCommand)
{
   int Result = TOOL_RETURN_CODE_EVENT_CONSUMED;

   int commandNumber = -1;   // bogus value used as sentinel

   // Combine tool command (from key press) with the sub-process type
   // to arrive at a new tool command
   switch(toolCommand)
      {
	 case DRS_CMD_LOW_MOTION_KEY:
		if ((DRSState.mainProcessType != DRS_PROCESS_LOW_MOTION) ||
			(DRSState.subProcessType == DRS_SUBPROCESS_C))   // Import
		{
           // NOTE: "Import" is now a whole separate button called "replace"
           switch (oldHighLowSubprocessState)
           {
              default:
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_LOW_MOTION;
                 break;
              case DRS_SUBPROCESS_A:
                 commandNumber = DRS_CMD_LOW_MOTION_PAST;
                 break;
              case DRS_SUBPROCESS_B:
                 commandNumber = DRS_CMD_LOW_MOTION_FUTURE;
                 break;
           }
           iModifierSubProcessType = oldHighLowSubprocessState;
        }
        else
        {
		   // Here, case DRS_SUBPROCESS_C (Import) is deliberately left out
           // according to the GUI, cycle order is A-NONE-B
           switch (DRSState.subProcessType)
           {
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_LOW_MOTION_FUTURE;
                 iModifierSubProcessType = DRS_SUBPROCESS_B;
                 break;
              default:
              case DRS_SUBPROCESS_A:
                 commandNumber = DRS_CMD_LOW_MOTION;
                 iModifierSubProcessType = DRS_SUBPROCESS_NONE;
                 break;
              case DRS_SUBPROCESS_B:
                 commandNumber = DRS_CMD_LOW_MOTION_PAST;
                 iModifierSubProcessType = DRS_SUBPROCESS_A;
                 break;
           }
           oldHighLowSubprocessState = iModifierSubProcessType;
	}
        break;

     case DRS_CMD_HIGH_MOTION_KEY:
		if ((DRSState.mainProcessType != DRS_PROCESS_HIGH_MOTION) ||
			(DRSState.subProcessType == DRS_SUBPROCESS_C))   // Import
        {
           // NOTE: "Import" is now a whole separate button called "replace"
           switch (oldHighLowSubprocessState)
           {
              default:
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_HIGH_MOTION;
                 break;
              case DRS_SUBPROCESS_A:
                 commandNumber = DRS_CMD_HIGH_MOTION_PAST;
                 break;
              case DRS_SUBPROCESS_B:
                 commandNumber = DRS_CMD_HIGH_MOTION_FUTURE;
                 break;
           }
           iModifierSubProcessType = oldHighLowSubprocessState;
        }
        else
        {
		   // Here, case DRS_SUBPROCESS_C (Import) is deliberately left out
           switch (DRSState.subProcessType)
           {
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_HIGH_MOTION_FUTURE;
                 iModifierSubProcessType = DRS_SUBPROCESS_B;
                 break;
              default:
              case DRS_SUBPROCESS_A:
                 commandNumber = DRS_CMD_HIGH_MOTION;
                 iModifierSubProcessType = DRS_SUBPROCESS_NONE;
                 break;
              case DRS_SUBPROCESS_B:
                 commandNumber = DRS_CMD_HIGH_MOTION_PAST;
                 iModifierSubProcessType = DRS_SUBPROCESS_A;
                 break;
           }
           oldHighLowSubprocessState = iModifierSubProcessType;
        }
        break;

	 case DRS_CMD_HARD_MOTION_KEY:
        if (DRSState.mainProcessType != DRS_PROCESS_HARD_MOTION)
        {
           switch (oldHardSubprocessState)
           {
              default:
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_HARD_MOTION_EDGE;
                 break;
              case DRS_SUBPROCESS_A:
                 commandNumber = DRS_CMD_HARD_MOTION;
                 break;
           }
           iModifierSubProcessType = oldHardSubprocessState;
        }
        else
        {
           switch (DRSState.subProcessType)
           {
              case DRS_SUBPROCESS_NONE:
                 commandNumber = DRS_CMD_HARD_MOTION;      // _A NOT _NONE!!!
                 iModifierSubProcessType = DRS_SUBPROCESS_A;
                 break;
              case DRS_SUBPROCESS_A:
              default:
                 commandNumber = DRS_CMD_HARD_MOTION_EDGE;
                 iModifierSubProcessType = DRS_SUBPROCESS_NONE;
                 break;
           }
           oldHardSubprocessState = iModifierSubProcessType;
        }
        break;

	 case DRS_CMD_REPLACE_KEY:
        // Turns into NO, LOW or HIGH MOTION - IMPORT
		if ((DRSState.subProcessType == DRS_SUBPROCESS_C) &&
            (DRSState.mainProcessType == DRS_PROCESS_NO_MOTION ||
             DRSState.mainProcessType == DRS_PROCESS_LOW_MOTION ||
             DRSState.mainProcessType == DRS_PROCESS_HIGH_MOTION)
           )
        {
           // Cycle through NO, LOW and HIGH
           switch (DRSState.mainProcessType)
           {
              case DRS_PROCESS_NO_MOTION:
                 commandNumber = DRS_CMD_LOW_MOTION_IMPORT;
					  oldReplaceProcessType = DRS_PROCESS_LOW_MOTION;
                 break;
              case DRS_PROCESS_LOW_MOTION:
                 commandNumber = DRS_CMD_HIGH_MOTION_IMPORT;
                 oldReplaceProcessType = DRS_PROCESS_HIGH_MOTION;
                 break;
              case DRS_PROCESS_HIGH_MOTION:
				 commandNumber = DRS_CMD_NO_MOTION_IMPORT;
                 oldReplaceProcessType = DRS_PROCESS_NO_MOTION;
                 break;
           }
        }
        else
        {
           // Set to IMPORT mode, and NO, LOW or HIGH motion depending on
           // last setting
			  switch (oldReplaceProcessType)
           {
              case DRS_PROCESS_NO_MOTION:
				 commandNumber = DRS_CMD_NO_MOTION_IMPORT;
				 break;
              case DRS_PROCESS_LOW_MOTION:
                 commandNumber = DRS_CMD_LOW_MOTION_IMPORT;
                 break;
              case DRS_PROCESS_HIGH_MOTION:
                 commandNumber = DRS_CMD_HIGH_MOTION_IMPORT;
                 break;
           }
        }

		iModifierSubProcessType = DRS_SUBPROCESS_C;
		break;

	  case DRS_CMD_COLOR_BUMP_KEY:
         commandNumber = DRS_CMD_COLOR_BUMP;
         iModifierSubProcessType = DRSState.subProcessType;
         break;

     // Shift-1
     case DRS_CMD_MOTION_SUBMODE_1:
        switch (DRSState.mainProcessType)
        {
           case DRS_PROCESS_LOW_MOTION:
              commandNumber = DRS_CMD_LOW_MOTION_PAST;
              iModifierSubProcessType = DRS_SUBPROCESS_A;    // Past only
              break;
           case DRS_PROCESS_HIGH_MOTION:
              commandNumber = DRS_CMD_HIGH_MOTION_PAST;
              iModifierSubProcessType = DRS_SUBPROCESS_A;    // Past only
              break;
           case DRS_PROCESS_HARD_MOTION:
              commandNumber = DRS_CMD_HARD_MOTION;
              iModifierSubProcessType = DRS_SUBPROCESS_A;    // Blur
              break;
        }
        break;

     // Shift-2
     case DRS_CMD_MOTION_SUBMODE_2:
        switch (DRSState.mainProcessType)
        {
           case DRS_PROCESS_LOW_MOTION:
              commandNumber = DRS_CMD_LOW_MOTION;
              iModifierSubProcessType = DRS_SUBPROCESS_NONE;    // Both
              break;
           case DRS_PROCESS_HIGH_MOTION:
              commandNumber = DRS_CMD_HIGH_MOTION;
              iModifierSubProcessType = DRS_SUBPROCESS_NONE;    // Both
              break;
           case DRS_PROCESS_HARD_MOTION:
              commandNumber = DRS_CMD_HARD_MOTION_EDGE;
              iModifierSubProcessType = DRS_SUBPROCESS_NONE; // Edge Preserving
              break;
        }
        break;

     // Shift-3
     case DRS_CMD_MOTION_SUBMODE_3:
        switch (DRSState.mainProcessType)
        {
           case DRS_PROCESS_LOW_MOTION:
              commandNumber = DRS_CMD_LOW_MOTION_FUTURE;
              iModifierSubProcessType = DRS_SUBPROCESS_B;    // Future only
              break;
           case DRS_PROCESS_HIGH_MOTION:
              commandNumber = DRS_CMD_HIGH_MOTION_FUTURE;
              iModifierSubProcessType = DRS_SUBPROCESS_B;    // Future only
              break;
        }
        break;

      case DRS_CMD_SELECT_SHAPE_KEY :
         // always cycle through the 3 modes
         commandNumber = DRS_CMD_SWITCH_SELECT_SHAPE;
         break;

      case DRS_CMD_SELECT_BORDER_KEY :

         switch(iModifierSubProcessType)
            {
            case DRS_SUBPROCESS_NONE :
               commandNumber = DRS_CMD_SWITCH_BORDER;
               break;
            case DRS_SUBPROCESS_A:
               commandNumber = DRS_CMD_BLEND_BORDER;
               break;
            case DRS_SUBPROCESS_B:
               commandNumber = DRS_CMD_DITHER_BORDER;
               break;
            }
         break;

      case DRS_CMD_SELECT_OVERRIDE_KEY :
         switch(iModifierSubProcessType)
            {
            case DRS_SUBPROCESS_NONE :
               commandNumber = DRS_CMD_NOOP;
               break;
            case DRS_SUBPROCESS_A:
               commandNumber = DRS_CMD_MOVE_OVER;
               break;
            case DRS_SUBPROCESS_B:
               commandNumber = DRS_CMD_MOVE_UNDER;
               break;
            case DRS_SUBPROCESS_C:
               commandNumber = DRS_CMD_ROTATE_OVER;
               break;
            case DRS_SUBPROCESS_D:
               commandNumber = DRS_CMD_ROTATE_UNDER;
               break;
            case DRS_SUBPROCESS_E:
               commandNumber = DRS_CMD_CHANGE_COLOR_BRIGHTNESS;
               break;
            case DRS_SUBPROCESS_F:
               commandNumber = DRS_CMD_CHANGE_GRAIN;
               break;
            case DRS_SUBPROCESS_G:
               commandNumber = DRS_CMD_CHANGE_BORDER;
               break;
            case DRS_SUBPROCESS_H:
               commandNumber = DRS_CMD_CHANGE_TRANSPARENCY;
               break;
            case DRS_SUBPROCESS_I:
               commandNumber = DRS_CMD_CHANGE_COLOR_SATURATION;
               break;
            case DRS_SUBPROCESS_J:
               commandNumber = DRS_CMD_CHANGE_COLOR_RED;
               break;
            case DRS_SUBPROCESS_K:
               commandNumber = DRS_CMD_CHANGE_COLOR_GREEN;
               break;
            case DRS_SUBPROCESS_L:
               commandNumber = DRS_CMD_CHANGE_COLOR_BLUE;
               break;
            }
         break;

      }

   // "Execute" the command, which re-entrantly calls onToolCommand
   // to process the composite DRS process type command
   if (commandNumber != -1)
      {
      CToolCommand toolCommand(this, commandNumber);
      Result = toolCommand.execute();
      }

   return Result;
}

// ===================================================================
//
// Function:    DRSProcessTypeCommand
//
// Description: DRS Tool's Command Handler for DRS Process Type commands.
//              Called by onToolCommand.  Sets new DRS process type and
//              updates GUI button appearance (pressed or up) and
//              process type message in main window's status bar.
//
//              If a fix is pending when the function is called, the
//              fix will be reprocessed with the new DRS process type.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::DRSProcessTypeCommand(int toolCommand)
{
	bool drsFixProcessType = true;
   int Result = TOOL_RETURN_CODE_EVENT_CONSUMED;

   if ((DRSState.pendingMainProcessType != DRS_PROCESS_NOOP) &&
         !getSystemAPI()->CheckProvisionalUnresolved())
      return Result;

   // Remember previous processing type so we can see if it changes
   int previousMainProcessType = DRSState.mainProcessType;
	int previousSubProcessType = DRSState.subProcessType;

	// Special hacks for replace mode. We need to reprocess a fix if a new
	// replace source option is selected, or we need to auto-accept a
	// pending fix if one of the oddball "auto-full-frame replace" options
	// gets selected.
	bool forceReprocess = false;
	if (toolCommand == DRS_CMD_NO_MOTION_IMPORT
	|| toolCommand == DRS_CMD_LOW_MOTION_IMPORT
	|| toolCommand == DRS_CMD_HIGH_MOTION_IMPORT)
	{
		int replaceModeSourceFrameOption = GetReplaceModeSourceFrameOption();
		if (replaceModeSourceFrameOption == DRS_REPLSRCFRM_2_FRM_AUTO
		|| replaceModeSourceFrameOption == DRS_REPLSRCFRM_3_FRM_AUTO)
		{
			WaitUntilBackgroundThreadIsIdle();
			WaitForProcessingToFinish();
			AutoAccept();
		}
		else if (replaceModeSourceFrameOption != oldReplaceModeSourceFrameOption)
		{
			forceReprocess = true;
		}

		oldReplaceModeSourceFrameOption = replaceModeSourceFrameOption;
	}

   // DRS Process Type Commands

	switch (toolCommand)
      {
		case DRS_CMD_LOW_MOTION:
         DRSState.mainProcessType = DRS_PROCESS_LOW_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_NONE;
         break;

      case DRS_CMD_HIGH_MOTION:
         DRSState.mainProcessType = DRS_PROCESS_HIGH_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_NONE;
         break;

      case DRS_CMD_HARD_MOTION:
         DRSState.mainProcessType = DRS_PROCESS_HARD_MOTION;
         oldHardSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_A;
         break;

      case DRS_CMD_LOW_MOTION_PAST:
         DRSState.mainProcessType = DRS_PROCESS_LOW_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_A;
         break;

      case DRS_CMD_HIGH_MOTION_PAST:
         DRSState.mainProcessType = DRS_PROCESS_HIGH_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_A;
         break;

      case DRS_CMD_LOW_MOTION_FUTURE:
         DRSState.mainProcessType = DRS_PROCESS_LOW_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_B;
         break;

      case DRS_CMD_HIGH_MOTION_FUTURE:
         DRSState.mainProcessType = DRS_PROCESS_HIGH_MOTION;
         oldHighLowSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_B;
         break;

		case DRS_CMD_NO_MOTION_IMPORT:
         DRSState.mainProcessType = DRS_PROCESS_NO_MOTION;
			DRSState.subProcessType = DRS_SUBPROCESS_C;
			break;

	  case DRS_CMD_LOW_MOTION_IMPORT:
         DRSState.mainProcessType = DRS_PROCESS_LOW_MOTION;
         DRSState.subProcessType = DRS_SUBPROCESS_C;
         break;

		case DRS_CMD_HIGH_MOTION_IMPORT:
         DRSState.mainProcessType = DRS_PROCESS_HIGH_MOTION;
         DRSState.subProcessType = DRS_SUBPROCESS_C;
         break;

      case DRS_CMD_HARD_MOTION_EDGE:
         DRSState.mainProcessType = DRS_PROCESS_HARD_MOTION;
         oldHardSubprocessState = DRSState.subProcessType = DRS_SUBPROCESS_NONE;
         break;

	  case DRS_CMD_COLOR_BUMP:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_NONE;
         break;

	  case DRS_CMD_COLOR_BUMP_PAST:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_A;
         break;

	  case DRS_CMD_COLOR_BUMP_FUTURE:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_B;
         break;

	  case DRS_CMD_COLOR_BUMP_MONO:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_C;
         break;

	  case DRS_CMD_COLOR_BUMP_PAST_MONO:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_D;
         break;

	  case DRS_CMD_COLOR_BUMP_FUTURE_MONO:
         DRSState.mainProcessType = DRS_PROCESS_COLOR_BUMP;
         DRSState.subProcessType = DRS_SUBPROCESS_E;
         break;

		default :
         drsFixProcessType = false;
		}

   // Update which GUI buttons appear "pressed"
   UpdateDRSGuiButtons();

   // Update DRS's Process Type on the Status Bar
   string sProcessType;
   GetFullProcessTypeString(sProcessType);
   getSystemAPI()->UpdateStatusBar(sProcessType, 1);

   if (isInitingFixEng) {

      // clear this flag
      isInitingFixEng = false;
   }
   else { // the original pathway

		if (forceReprocess
			|| (drsFixProcessType
				  && (previousMainProcessType != DRSState.mainProcessType
						|| previousSubProcessType != DRSState.subProcessType)))
		{
			ReprocessFix(false);
		}
   }

   return Result;
}

// ===================================================================
//
// Function:    OverrideCommand
//
// Description: DRS Tool's Command Handler for DRS Override commands.
//              Called by onToolCommand.  Sets new DRS override mode and
//              updates GUI button appearance (pressed or up) and
//              process type message in main window's status bar.
//
//              If a fix is pending when the function is called, the
//              fix will be reprocessed with the new DRS override mode.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::OverrideCommand(int toolCommand)
{
  int iRet;

  switch (GetOverrideMode())
   {
      case OM_MOVE_OVER:
         iRet = MoveFixCommand(toolCommand, true);
         break;
      case OM_MOVE_UNDER:
         iRet = MoveFixCommand(toolCommand, false);
         break;
      case OM_CHANGE_COLOR_BRIGHTNESS:
         iRet = ChangeColorBrightnessCommand(toolCommand);
         break;
      case OM_CHANGE_COLOR_SATURATION:
         iRet = ChangeColorSaturationCommand(toolCommand);
         break;
      case OM_CHANGE_COLOR_RED:
         iRet = ChangeColorRedCommand(toolCommand);
         break;
      case OM_CHANGE_COLOR_GREEN:
         iRet = ChangeColorGreenCommand(toolCommand);
         break;
      case OM_CHANGE_COLOR_BLUE:
         iRet = ChangeColorBlueCommand(toolCommand);
         break;
      case OM_CHANGE_GRAIN:
         iRet = ChangeGrainCommand(toolCommand);
         break;
      case OM_CHANGE_BORDER:
         iRet = ChangeBorderCommand(toolCommand);
         break;
      case OM_ROTATE_OVER:
         iRet = RotateCommand(toolCommand, true);
         break;
      case OM_ROTATE_UNDER:
         iRet = RotateCommand(toolCommand, false);
         break;
      case OM_CHANGE_TRANSPARENCY:
         iRet = ChangeTransparencyCommand(toolCommand);
         break;
      default:
         iRet = TOOL_RETURN_CODE_NO_ACTION; // unknown command
   }

  return iRet;
}

// ===================================================================
//
// Function:    MoveFixCommand
//
// Description: DRS Tool's Command Handler for Move (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand, bool bMoveOver
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::MoveFixCommand(int toolCommand, bool bMoveOver)
{
   // Handle all move fix (e.g. arrow key) tool commands here

   float moveX = 0.0;
   float moveY = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
         moveY = -DRS_MOVE_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
         moveY = DRS_MOVE_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_LEFT:
         moveX = -DRS_MOVE_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_RIGHT:
         moveX = DRS_MOVE_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
         moveY = -DRS_MOVE_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
         moveY = DRS_MOVE_LARGE;
         break;
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         moveX = -DRS_MOVE_LARGE;
         break;
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         moveX = DRS_MOVE_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
         moveY = -DRS_MOVE_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
         moveY = DRS_MOVE_SMALL;
         break;
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         moveX = -DRS_MOVE_SMALL;
         break;
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         moveX = DRS_MOVE_SMALL;
         break;
      }

   if (bMoveOver)
    {
     DRSState.Overrides.fOverrideMoveOverX += moveX;
     DRSState.Overrides.fOverrideMoveOverY -= moveY;
    }
   else
    {
     DRSState.Overrides.fOverrideMoveUnderX += moveX;
     DRSState.Overrides.fOverrideMoveUnderY -= moveY;
    }

#if 0   // ReprocessFix wants to decide this now

   // Do this stuff synchronously for now!
   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();    // moved this out from the if statement
                                   // because 'fix pending' is set at the end
                                   // of processing

#endif

   // QQQ always done synchronously, OK?
   ReprocessFix(true);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeColorBrightnessCommand
//
// Description: DRS Tool's Command Handler for ColorBrightness (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeColorBrightnessCommand(int toolCommand)
{
   // Handle all change ColorBrightness (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_COLOR_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_COLOR_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_COLOR_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_COLOR_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_COLOR_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_COLOR_SMALL;
         break;
      }

   SetOverrideColorBrightness(GetOverrideColorBrightness() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeColorSaturationCommand
//
// Description: DRS Tool's Command Handler for ColorSaturation (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeColorSaturationCommand(int toolCommand)
{
   // Handle all change ColorSaturation (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_COLOR_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_COLOR_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_COLOR_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_COLOR_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_COLOR_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_COLOR_SMALL;
         break;
      }

   SetOverrideColorSaturation(GetOverrideColorSaturation() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeColorRedCommand
//
// Description: DRS Tool's Command Handler for ColorRed (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeColorRedCommand(int toolCommand)
{
   // Handle all change ColorRed (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_COLOR_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_COLOR_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_COLOR_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_COLOR_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_COLOR_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_COLOR_SMALL;
         break;
      }

   SetOverrideColorRed(GetOverrideColorRed() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeColorGrrenCommand
//
// Description: DRS Tool's Command Handler for ColorGreen (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeColorGreenCommand(int toolCommand)
{
   // Handle all change ColorGreen (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_COLOR_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_COLOR_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_COLOR_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_COLOR_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_COLOR_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_COLOR_SMALL;
         break;
      }

   SetOverrideColorGreen(GetOverrideColorGreen() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeColorBlueCommand
//
// Description: DRS Tool's Command Handler for ColorBlue (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeColorBlueCommand(int toolCommand)
{
   // Handle all change ColorBlue (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_COLOR_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_COLOR_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_COLOR_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_COLOR_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_COLOR_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_COLOR_SMALL;
         break;
      }

   SetOverrideColorBlue(GetOverrideColorBlue() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeGrainCommand
//
// Description: DRS Tool's Command Handler for Grain (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeGrainCommand(int toolCommand)
{
   // Handle all change grain (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_GRAIN_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_GRAIN_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_GRAIN_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_GRAIN_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_GRAIN_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_GRAIN_SMALL;
         break;
      }

	SetOverrideGrainStrength(GetOverrideGrainStrength() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    RotateCommand
//
// Description: DRS Tool's Command Handler for Rotate (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::RotateCommand(int toolCommand, bool bOverFlag)
{
   // Handle all rotate (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_ROTATE_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_ROTATE_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_ROTATE_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_ROTATE_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_ROTATE_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_ROTATE_SMALL;
         break;
      }

   if (bOverFlag)
    {
     SetOverrideRotateOver(GetOverrideRotateOver() + delta);
    }
   else
    {
     SetOverrideRotateUnder(GetOverrideRotateUnder() + delta);
    }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeBorderCommand
//
// Description: DRS Tool's Command Handler for ColorBrightness (arrow key) commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeBorderCommand(int toolCommand)
{
   // Handle all change border (e.g. arrow key) tool commands here

   float delta = 0.0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_BORDER_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_BORDER_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_BORDER_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_BORDER_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_BORDER_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_BORDER_SMALL;
         break;
      }

   SetOverrideBorderRadius(GetOverrideBorderRadius() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    ChangeTransparencyCommand
//
// Description: DRS Tool's Command Handler for Transparency commands.
//              Called by OverrideCommand.
//
// Arguments:   int toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::ChangeTransparencyCommand(int toolCommand)
{
   // Handle all change transparency (e.g. arrow key) tool commands here

   int delta = 0;

   switch (toolCommand)
      {
      case DRS_CMD_OVERRIDE_UP:
      case DRS_CMD_OVERRIDE_RIGHT:
         delta = +DRS_CHANGE_TRANSPARENCY_NORMAL;
         break;
      case DRS_CMD_OVERRIDE_DOWN:
      case DRS_CMD_OVERRIDE_LEFT:
         delta = -DRS_CHANGE_TRANSPARENCY_NORMAL;
         break;

      case DRS_CMD_OVERRIDE_UP_LARGE:
      case DRS_CMD_OVERRIDE_RIGHT_LARGE:
         delta = DRS_CHANGE_TRANSPARENCY_LARGE;
         break;
      case DRS_CMD_OVERRIDE_DOWN_LARGE:
      case DRS_CMD_OVERRIDE_LEFT_LARGE:
         delta = -DRS_CHANGE_TRANSPARENCY_LARGE;
         break;

      case DRS_CMD_OVERRIDE_UP_SMALL:
      case DRS_CMD_OVERRIDE_RIGHT_SMALL:
         delta = DRS_CHANGE_TRANSPARENCY_SMALL;
         break;
      case DRS_CMD_OVERRIDE_DOWN_SMALL:
      case DRS_CMD_OVERRIDE_LEFT_SMALL:
         delta = -DRS_CHANGE_TRANSPARENCY_SMALL;
         break;
      }

   SetOverrideTransparency(GetOverrideTransparency() + delta);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    BorderBlendCommand
//
// Description: DRS Tool's Command Handler for BorderBlend command
//              Called by OverrideCommand.
//
// Arguments:   none
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CDRSTool::BorderBlendCommand()
{

#if 0   // ReprocessFix wants to decide this now

   // Do this stuff synchronously for now!
   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();    // moved this out from the if statement
                                   // because 'fix pending' is set at the end
                                   // of processing
   //???

#endif

   ReprocessFix(true);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

void CDRSTool::InitStates()
{
  // Reset the DRSState
  DRSState.ResetStates();

  // load states from ini file
  CDRSParameters dp;
  if (dp.readStateParameters())
   {
    SDRSStateParameters dsp = dp.getFileStateParameters ();
    DRSState.mainProcessType = dsp.iMainProcessType;
    DRSState.subProcessType = dsp.iSubProcessType;
    switch(dsp.iSelectMode)
      {
      case SELECT_REGION_LASSO :
         DRSState.selectShape = SELECT_REGION_LASSO;
         break;
      case SELECT_REGION_BEZIER :
         DRSState.selectShape = SELECT_REGION_BEZIER;
         break;
      case SELECT_REGION_RECT :
      default :
         DRSState.selectShape = SELECT_REGION_RECT;
         break;
      }
    DRSState.iBlendType = BLEND_TYPE_LINEAR;     // Always LINEAR
    LoadDRSStatePreferences();
    DRSState.iOverrideMode = dsp.iOverrideMode;
    DRSState.bOverridesAreSticky = dsp.bOverridesAreSticky;
    DRSState.bMoveOverIsSticky = dsp.bMoveOverIsSticky;
    DRSState.bMoveUnderIsSticky = dsp.bMoveUnderIsSticky;
    DRSState.bRotateOverIsSticky = dsp.bRotateOverIsSticky;
    DRSState.bRotateUnderIsSticky = dsp.bRotateUnderIsSticky;
    DRSState.bColorIsSticky = dsp.bColorIsSticky;
    DRSState.bBorderIsSticky = dsp.bBorderIsSticky;
	 DRSState.bGrainIsSticky = dsp.bGrainIsSticky;
    DRSState.bTransparencyIsSticky = dsp.bTransparencyIsSticky;
   }
  else
   {
    TRACE_0 (errout << "Unable to read State Parameters");
   }


  UpdateDRSGuiButtons();
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDRSTool::toolShow()
{
   ShowDRSGui();

   if (!isItOkToUseToolNow())
      getSystemAPI()-> DisableTool(getToolDisabledReason());

   string sProcessType;
   GetFullProcessTypeString(sProcessType);
   getSystemAPI()->UpdateStatusBar(sProcessType, 1);

   if (getSystemAPI()->isMasterClipLoaded() && !getSystemAPI()->doesClipHaveVersions() && IsColorBumpSelected())
   {
      reportColorbumpError = true;
   }

   return 0;
}

// ===================================================================
//
// Function:    toolHideQuery
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

bool CDRSTool::toolHideQuery()
{
   // These don't really belong here

   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();
   AutoAccept();
   CleanUpBeforeLeaving();

   MTIassert(!IsProvisionalPending());

   return !IsProvisionalPending();
}


// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CDRSTool::toolHide()
{
   HideDRSGui();
   doneWithTool();
   return 0;
}

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CDRSTool::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   // Disable the Reticle Tool
   getSystemAPI()->InternalDisableReticleTool();

   // Clear left-over bezier
   getSystemAPI()->ClearBezier(false);  // don't redraw, we redraw below

   // Make sure onHeartbeat() will kick off a "preload inputs" operation
   mostRecentInputLoadedFrameIndex = -1;

   // Turn on the tilde key 'repeat DRS or GOV' hack
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_DRS_MODE);

   // GOV tool is the only thing that uses our status bar at present
   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   // Make sure displayed frame is clean
   getSystemAPI()->refreshFrameCached();

   JobManager jobManager;
   jobManager.SetCurrentToolCode("dr");

	getSystemAPI()->SetToolNameForUserGuide("DRSTool");

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CDRSTool::toolDeactivate()
{
   // Get rid of any review regions that might be present on the screen
   getSystemAPI()->ClearReviewRegions(true);

   // Restore the Reticle Tool
   getSystemAPI()->InternalEnableReticleTool();

   // Cleanup tool infrastructure stuff
   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();

   AutoAccept();

   getSystemAPI()->ClearProvisional(false);

   DestroyAllToolSetups();

   // Turn off the tilde key 'repeat DRS or GOV' hack
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_OTHER_MODE);
   DRSState.inhibitRepeatFixOp = true;

   // Turn off GOV tool access to our status bar
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    onRedraw
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CDRSTool::onRedraw(int frameIndex)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // Skip any drawing if we're not showing the frame being fixed
   if (frameIndex == fixFrameIndex)
   {
      // If we are processing some fixes in the background now,
      // show them on the frame
      DrawOutlinesOfFixesInProgress();
   }

   // What the hell is this doing out here???? Show bezier even if we're not
   // on the frame?? QQQ
   if (DRSState.selectShape == SELECT_REGION_BEZIER)
   {
      getSystemAPI()->setGraphicsColor(DASHED_GREEN);
      getSystemAPI()->RedrawBezier();
	}

   return(TOOL_RETURN_CODE_EVENT_CONSUMED);
}

// ===================================================================
//
// Stuff for drawing fix outlines
//
// ===================================================================

bool CDRSTool::AreAnyOtherFixesQueuedOrInProgress(SBGFixTracker *fixTracker)
{
   CAutoSpinLocker lock(fixesInProgressListLock);
   bool retVal = false;

   list<SBGFixTracker *>::iterator iter = fixesInProgressList.begin();
   list<SBGFixTracker *>::iterator stop = fixesInProgressList.end();

   for (; iter != stop && retVal == false; ++iter)
   {
      SBGFixTracker *iterTracker = *iter;

      // Look for at least one other fix queued or in progress
      if ((iterTracker != fixTracker) &&
          (iterTracker->status == DRS_BGFTS_FIX_QUEUED ||
             iterTracker->status == DRS_BGFTS_FIXING))
      {
         retVal = true;
      }
   }

   return retVal;
}

int CDRSTool::ComputeFixOutlineColor(SBGFixTracker *fixTracker,
                                     bool minDisplayTimeElapsed)
{
   CAutoSpinLocker lock(fixesInProgressListLock);
   int outlineColor = INVALID_COLOR;

   if (!fixTracker->suppressOutline)
   {
      switch (fixTracker->status)
      {
         default:
         case DRS_BGFTS_INVALID:
         case DRS_BGFTS_PRELOAD_QUEUED:
         case DRS_BGFTS_PRELOADING:
         case DRS_BGFTS_PRELOAD_COMPLETE:
            // Never show these
            break;


         case DRS_BGFTS_FIX_QUEUED:
         {
            // Will be IN_PROGRESS_COLOR if it's the the only fix queued and
            // no fix is presently in progress, else QUEUED_COLOR

            outlineColor = QUEUED_COLOR;

            // HACK: once the color goes to "in progress", never change it back
            if ((fixTracker->outlineColor == IN_PROGRESS_COLOR) ||
                (!AreAnyOtherFixesQueuedOrInProgress(fixTracker)))
            {
               outlineColor = IN_PROGRESS_COLOR;
            }
            break;
         }

         case DRS_BGFTS_FIXING:

            // Always "in progress" color
            outlineColor = IN_PROGRESS_COLOR;

            break;

         case DRS_BGFTS_FIX_RESOLVED:
         case DRS_BGFTS_FIX_COMPLETE:

            // We want the outline to disappear (return INVALID_COLOR) when
            // we reach either of these states and we have been showing the
            // outline for a minimum amount of time; once the color goes to
            // "invalid" never change it back
            if ((fixTracker->outlineColor != INVALID_COLOR) &&
                 !minDisplayTimeElapsed)
            {
               outlineColor = COMPLETED_COLOR;
            }

            break;
      }
   }
   return outlineColor;
}

void CDRSTool::DrawFixOutline(SBGFixTracker *fixTracker, int color)
{
   if (fixTracker == NULL || fixTracker->drsParams == NULL)
      return;

   CAutoSpinLocker lock(fixesInProgressListLock);
   // MUST COPY the rect so original doesn't get fattened!
   RECT rect = fixTracker->drsParams->userRect;

   // If setting to an valid color and we have a valid rectangle,
   // draw a "fattened" rectangle (increases the width/height of
   // the rectangle so you can see the fix better)

   if (color != INVALID_COLOR &&
       rect.right > rect.left && rect.bottom > rect.top)
   {
      getSystemAPI()->fattenRectangleFrame(&rect, ORIGINAL_RECT_BORDER);
      getSystemAPI()->setGraphicsColor(color);
      getSystemAPI()->drawRectangleFrame(&rect);
   }

   // Remember the color we drew the outline in
   fixTracker->outlineColor = color;
}

void CDRSTool::DrawOutlinesOfFixesInProgress()
{
   CAutoSpinLocker lock(fixesInProgressListLock);

   if (fixesInProgressList.empty() == false)
   {
      list<SBGFixTracker *>::iterator iter = fixesInProgressList.begin();
      list<SBGFixTracker *>::iterator stop = fixesInProgressList.end();

      for (; iter != stop; ++iter)
      {
         SBGFixTracker *iterTracker = *iter;

         // Only draw if the fix was made on the current frame;
         // Once a fix goes to INVALID_COLOR, it is never drawn again
         if (iterTracker->frameIndex == getSystemAPI()->getLastFrameIndex() &&
             iterTracker->outlineColor != INVALID_COLOR)
         {
            // NOTE! DrawFixOutline sets outlineColor in the tracker!
            DrawFixOutline(iterTracker,
                           ComputeFixOutlineColor(iterTracker, false));
         }
      }
   }
   if (IsDRSFixPending())
   {
      // Show green highlights if we're not looking at "processed image"
      // or if we are running in multiframe mode
      if ((DRSState.fixBeingProcessed && DRSState.fixingMultipleFrames) ||
          (!getSystemAPI()->IsProcessedVisible()))
      {
         // Original portion of fix being shown, so draw the rectangle
         RECT tempRect = currentUserRect;

         getSystemAPI()->fattenRectangleFrame(&tempRect,
                                              ORIGINAL_RECT_BORDER);
         getSystemAPI()->setGraphicsColor(DASHED_GREEN);
         getSystemAPI()->drawRectangleFrame(&tempRect);
      }
   }
}

// ===================================================================
//
// Event Handlers
//
// ===================================================================

int CDRSTool::onChangingClip()
{
   // This function is called just before the current clip is unloaded.

   if (IsDisabled())          // !IsActive() is OK!!!
      return TOOL_RETURN_CODE_NO_ACTION;

   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();

   AutoAccept();

   getSystemAPI()->ClearProvisional(false);

   // Turn off the tilde key 'repeat DRS or GOV' hack
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_OTHER_MODE);
   DRSState.inhibitRepeatFixOp = true;

   DestroyAllToolSetups();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CDRSTool::onNewClip()
{
   // We update the grain presets whether or not this tool is active.
   string clipName = getSystemAPI()->getClipFilename();
   int frameWidth = 0;
   int frameHeight = 0;
   int bitsPerComponent = 0;
	bool isDataReallyHalfs = false;
	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat != NULL)
   {
      frameWidth = imageFormat->getTotalFrameWidth();
      frameHeight = imageFormat->getTotalFrameHeight();
		bitsPerComponent = imageFormat->getBitsPerComponent();
		isDataReallyHalfs = imageFormat->getColorSpace() == IF_COLOR_SPACE_EXR;
	}

	GetGrainPresets().SetClipData(clipName, frameWidth, frameHeight, bitsPerComponent, !isDataReallyHalfs);

   if (!(IsLicensed() && IsShowing())) //NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;

   // do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   // Make sure we're starting fresh - probably unnecessary
   getSystemAPI()->ClearProvisional(false);

   // Turn on the tilde key 'repeat DRS or GOV' hack
   getSystemAPI()->SetTildeKeyHackMode(TILDE_KEY_HACK_DRS_MODE);
   DRSState.inhibitRepeatFixOp = true;

   InitStates();

   // Make sure onHeartbeat() will kick off a "preload inputs" operation
   // to cache the input frames for the current frame in the new clip,
   // as well as re-premake the single frame tool setup.
   mostRecentInputLoadedFrameIndex = -1;

   MarksChanged();

   if (getSystemAPI()->isMasterClipLoaded() && !getSystemAPI()->doesClipHaveVersions() && IsColorBumpSelected())
   {
      MessageBox(NULL, "Color Bump does not preserve history.\r\nConsider running it in a version clip instead!", "No History Warning", MB_ICONWARNING | MB_OK);
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CDRSTool::onChangingFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   WaitUntilBackgroundThreadIsIdle();
   WaitForProcessingToFinish();
   AutoAccept();

   getSystemAPI()->ClearProvisional(true);

   DestroyAllToolSetups();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CDRSTool::onNewFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // Make sure onHeartbeat() will kick off a "preload inputs" operation
   // to cache the input frames for the current frame in the new framing
   // scheme, as well as re-premake the single frame tool setup.
   mostRecentInputLoadedFrameIndex = -1;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}


int CDRSTool::onNewMarks()
{
   if (!(IsLicensed() && IsShowing())) //NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;

   MarksChanged();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}


bool CDRSTool::NotifyGOVStarting(bool *cancelStretch)
{
   bool retVal = true;

   // If we are in the middle of stretching the rectangle for a fix,
   // we must tell the GOV Tool "No dice!"
   if (DRSState.foregroundIsActive)
   {
      retVal = false;
   }
   else
   {
      // Sigh, this really sucks - would be nice to have a unified "background
      // processing" strategy.... QQQ
      WaitUntilBackgroundThreadIsIdle();
      WaitForProcessingToFinish();

      // We don't want GOV tool to continue if we are going to pop up a dialog
      // because that'll mess up his 'stretch rectangle' operation
      if (IsDRSFixPending() && (!GetAutoAccept()))
      {
         retVal = false;
      }

      // Save the pending fix before turning control over to GOV
      AutoAccept();
      WaitUntilBackgroundThreadIsIdle();   // SYNCHRONOUS WITH THIS CALL!

      // If we didn't already nix GOV continuing, test again
      if (retVal)
      {
         retVal = !IsDRSFixPending();   // Did user simply 'cancel' the dialog?
      }

      // Give our parent a crack at it (LAST)
      if (retVal)
      {
         retVal = CToolObject::NotifyGOVStarting(cancelStretch);
      }
   }

   return retVal;
}

void CDRSTool::NotifyGOVDone()
{
   needToClearStatusBar = true;
   CToolObject::NotifyGOVDone();
}


void CDRSTool::NotifyRightClicked()
{
   if (!(GetAutoAccept() || DRSState.foregroundIsActive))
   {
      WaitUntilBackgroundThreadIsIdle();
      AcceptFix(false);
   }
}


void CDRSTool::NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                        const RECT &rect, const POINT *lasso)
{
   // HACK ! If the GOV tool passes in SELECTED_REGION_SHAPE_NONE for the shape,
   // that means it used whatever we last passed to it, so we don't change
   // anything!
   if (shape != SELECTED_REGION_SHAPE_NONE)
   {
      // NO! DO NOT CHANGE THE selectShape - that's a reflection of the
      // state of the GUI buttons - the code will figure out what to use
      // based on whether or not the lasso and bezier pointer are NULL!! Gagggh!
      //DRSState.selectShape = (shape == SELECTED_REGION_SHAPE_LASSO)?
                                   //SELECT_REGION_RECT : SELECT_REGION_LASSO;
      currentUserRect = rect;
      currentUserLassoPointer = CopyLasso(lasso, globalLassoCopy);
      deleteCurrentBezier();   // GOV doesn't do bezier
   }

// ***************** SIDE EFFECT ALERT !!! ***********************
   DRSState.bLastActionWasGOV = true;
   DRSState.inhibitRepeatFixOp = false;
// ***************************************************************
}

// ===================================================================
//
// Fix processing stuff
//
// ===================================================================

void CDRSTool::PreMakeSimpleToolSetups()
{

   if (!singleFrameToolSetupHandleIsValid)
   {
      // Create the REAL FIX tool setup
      CToolIOConfig ioConfig(1, 1);

      // HACK - Stupid Tool Infrastructure does not pass setup type
      // to our callback makeToolProcessorInstance, so we stick it in a global
      currentToolSetupType = TOOL_SETUP_TYPE_SINGLE_FRAME;
      //

      singleFrameToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(
                                                 GetToolName(),
                                                 TOOL_SETUP_TYPE_SINGLE_FRAME,
                                                 &ioConfig);
      MTIassert(singleFrameToolSetupHandle >= 0);
      singleFrameToolSetupHandleIsValid = (singleFrameToolSetupHandle >= 0);
   }

   if (!preloadToolSetupHandleIsValid)
   {
      // Create the PRELOAD INPUT FRAMES tool setup
      CToolIOConfig ioConfig(1, 0);     // input port only!!

      // HACK - Stupid Tool Infrastructure does not pass setup type
      // to our callback makeToolProcessorInstance, so we stick it in a global
      currentToolSetupType = TOOL_SETUP_TYPE_PRELOAD_ONLY;
      //

      preloadToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(
                                                 GetToolName(),
                                                 TOOL_SETUP_TYPE_PRELOAD_ONLY,
                                                 &ioConfig);
      MTIassert(preloadToolSetupHandle >= 0);
      preloadToolSetupHandleIsValid = (preloadToolSetupHandle >= 0);
   }
}


void CDRSTool::DestroyAllToolSetups()
{
   // DRS has two tool setups: single frame and multi frame.
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   MTIassert(getSystemAPI()->IsProvisionalPending() == false);
   MTIassert(DRSState.fixBeingProcessed == false);
   MTIassert(DRSState.fixIsPending == false);

   if (singleFrameToolSetupHandle >= 0)
      {
      getSystemAPI()->DestroyToolSetup(singleFrameToolSetupHandle);
      }
   singleFrameToolSetupHandle = -1;
   singleFrameToolSetupHandleIsValid = false;

   if (preloadToolSetupHandle >= 0)
      {
      getSystemAPI()->DestroyToolSetup(preloadToolSetupHandle);
      }
   preloadToolSetupHandle = -1;
   preloadToolSetupHandleIsValid = false;

   if (multiFrameToolSetupHandle >= 0)
      {
      getSystemAPI()->DestroyToolSetup(multiFrameToolSetupHandle);
      }
   multiFrameToolSetupHandle = -1;
   multiFrameToolSetupHandleIsValid = false;
}

void CDRSTool::GetFullProcessTypeString(string &sResult)
{
  string sMain;
  string sSub;
  GetMainProcessTypeString(sMain);
  switch (DRSState.subProcessType)
    {
    case DRS_SUBPROCESS_NONE:
      sSub = "";
	  if (DRSState.mainProcessType == DRS_PROCESS_HARD_MOTION)
        sSub = ", Edge Preserving";
      break;
    case DRS_SUBPROCESS_A:
      sSub = ", Past Only";
      if (DRSState.mainProcessType == DRS_PROCESS_HARD_MOTION)
        sSub = "";
      break;
    case DRS_SUBPROCESS_B:
      sSub = ", Future Only";
	  break;
	case DRS_SUBPROCESS_C:
	  sSub = ", Replace Region";
	  break;
	}
  sResult = sMain + sSub;

}


void CDRSTool::GetMainProcessTypeString(string &sResult)
{
  switch (DRSState.mainProcessType)
    {
    case DRS_PROCESS_NO_MOTION:
      sResult = "No Motion";
      break;
    case DRS_PROCESS_LOW_MOTION:
      sResult = "Low Motion";
      break;
    case DRS_PROCESS_HIGH_MOTION:
      sResult = "High Motion";
      break;
	case DRS_PROCESS_HARD_MOTION:
	  sResult = "Hard Motion";
	  break;
	case DRS_PROCESS_COLOR_BUMP:
	  sResult = "Color Bump";
	  break;
	}
}

int CDRSTool::ProcessSingleFrame(EToolSetupType toolSetupType)
{
   SDRSParameters drsParams;
   int retVal = GatherDRSParameters(drsParams);
   if (retVal != 0)
   {
      return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }
   return ProcessSingleFrameInternal(TOOL_SETUP_TYPE_SINGLE_FRAME, drsParams);
}

int CDRSTool::ProcessSingleFrameInternal(EToolSetupType toolSetupType,
                                         SDRSParameters &drsParams)
{
   int retVal = 0;
   string errorFunc;
   bool reprocessFlag = false;

   if (IsDisabled())
   {
      TRACE_0(errout << "DRS internal error, tried to run when disabled ");
      return -999;
   }

   try
   {

      // now runs in background, don't hack cursor!
      // CBusyCursor busyCursor(true);   // Busy cursor until busyCursor goes
      // out-of-scope, e.g., this function returns

      dStartTime = hrTimer.Read();

      if (toolSetupType != TOOL_SETUP_TYPE_PRELOAD_ONLY)
      {
         // If a fix is pending, then we will reprocess.
         reprocessFlag = IsDRSFixPending();
      }

      // If reprocessing a previous fix, then the frame-to-fix is the
      // same as the frame with the initial fix.  If processing for the
      // first time, then the frame-to-fix is the currently displayed frame
      if (reprocessFlag == false)
      {
         errorFunc = "getLastFrameIndex";
         fixFrameIndex = getSystemAPI()->getLastFrameIndex();
         MTIassert(fixFrameIndex >= 0);
         if (fixFrameIndex < 0)
         {
            retVal = RET_FAILURE;
            throw errorFunc;
         }
      }

      // Make the regular fix setup and the "null fix" setup
      errorFunc = "Initial Tool Setup";
      PreMakeSimpleToolSetups();

      // Note following makes a REFERENCE to the handle
      int &toolSetupHandle = (toolSetupType == TOOL_SETUP_TYPE_SINGLE_FRAME) ? singleFrameToolSetupHandle : preloadToolSetupHandle;
      bool &toolSetupHandleIsValid = (toolSetupType == TOOL_SETUP_TYPE_SINGLE_FRAME) ? singleFrameToolSetupHandleIsValid :
            preloadToolSetupHandleIsValid;

      MTIassert(toolSetupHandleIsValid && (toolSetupHandle >= 0));
      if ((!toolSetupHandleIsValid) || (toolSetupHandle < 0))
      {
         retVal = RET_FAILURE;
         throw errorFunc;
      }

      // Set the active tool setup (this is different than the active tool)
      errorFunc = "SetActiveToolSetup";
//      DBCOMMENT("INTERRDIAG: Calling SetActiveToolSetup()");
//      DBTRACE(toolSetupHandle);
      retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
//      DBCOMMENT("INTERRDIAG: Return from SetActiveToolSetup()");
//      DBTRACE(retVal);
      if (retVal != 0)
      {
         DBTRACE(retVal);
         throw errorFunc;
      }

      // Set processing frame range
      errorFunc = "SetToolFrameRange";
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = true;
      frameRange.inFrameRange[0].inFrameIndex = fixFrameIndex;
      frameRange.inFrameRange[0].outFrameIndex = fixFrameIndex + 1;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         DBTRACE(retVal);
         throw errorFunc;
      }

      // Set tool's parameters
      CToolParameters *toolParams;
      errorFunc = "new CDRSToolParameters";
      CDRSToolParameters *drsToolParams = new CDRSToolParameters;

      drsToolParams->drsParams = drsParams;
      toolParams = drsToolParams;

      // Note: system will delete the CToolParameter instance
      // when it is done with it.
      errorFunc = "SetToolParameters";
      retVal = getSystemAPI()->SetToolParameters(toolParams);
      if (retVal != 0)
      {
         DBTRACE(retVal);
         throw errorFunc;
      }

      errorFunc = "SetProvisionalReview";
      getSystemAPI()->SetProvisionalReview(false);
      errorFunc = "SetProvisionalReprocess";
      getSystemAPI()->SetProvisionalReprocess(reprocessFlag);

      // Start the image processing a-runnin'
      errorFunc = "RunActiveToolSetup";
      getSystemAPI()->RunActiveToolSetup();

      return 0;
   }
   catch (string s)
   {
      TRACE_0(errout << "CAUGHT EXCEPTION: " << s); // do nothing here
   }
   catch (...)
   {
      retVal = 666; // meaningless
   }
   try
   {
      CAutoErrorReporter autoErr("CDRSTool::ProcessSingleFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      autoErr.errorCode = ERR_AP_BAD_PARAMETER;
      if (retVal == ERR_ALLOC)
      {
         autoErr.msg << "OUT OF MEMORY" << endl << " in function " << errorFunc << endl <<
               "Please try again with a smaller region of the image";
      }
      else
      {
         autoErr.msg << "INTERNAL ERROR (" << retVal << ")" << endl << " in function " << errorFunc;
      }
   }
   catch (...)
   {
      TRACE_0(errout << "CDRSTool::ProcessSingleFrame: FATAL ERROR, code " << retVal << endl <<
            "CAUGHT EXCEPTION when trying to display error dialog!");
   }

   return retVal; // ERROR
}
//---------------------------------------------------------------------------

namespace {  // atart anonymous namespace

// stolen from ExecStatusBar - should stash it somewhere accessible to all
string formatShortDuration(double durationInSeconds)
{
   string retVal;
   int seconds = int(durationInSeconds);
   MTIostringstream os;

   if (seconds > 999)
   {
      os << seconds;
   }
   else if (seconds > 99)
   {
      int deciseconds  = int(durationInSeconds * 10.0) % 10;
      os << seconds << "." << setw(1) << deciseconds;
   }
   else if (seconds > 9)
   {
      int centiseconds = int(durationInSeconds * 100.0) % 100;
      os << seconds << "." << setw(2) << setfill('0') << centiseconds;
   }
   else // seconds <= 9
   {
      int milliseconds = int(durationInSeconds * 1000.0) % 1000;
      os << seconds << "." << setw(3) << setfill('0')  << milliseconds;
   }

   retVal = os.str();

   return retVal;
}

}; // end anonymous namepsace

int CDRSTool::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   if (DRSState.fixBeingProcessed && (autotoolStatus.status == AT_STATUS_STOPPED))
   {
      if (DRSState.fixingMultipleFrames)
      {
         DRSState.fixBeingProcessed = false;
         TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = FALSE (1)");
         DRSState.fixingMultipleFrames = false;
         DRSState.fixIsPending = false;
         SetGOVRepeatShape();
         DRSState.ResetOverridesIfNotSticky();
         // Recreate single-frame setup in a background thread
         DestroyAllToolSetups();

         // Make sure onHeartbeat() will kick off a "preload inputs"
         // operation to re-cache the input frames for the current
         // frame as well as re-premake the single frame tool setup.
         mostRecentInputLoadedFrameIndex = -1;
      }
      else if (DRSState.notReallyFixingAnything)
      {
         //DRSState.fixIsPending = false;       // didn't fix anything!
         DRSState.fixBeingProcessed = false;  // processing is done
         TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = FALSE (2)");
         GetToolProgressMonitor()->SetIdle();
      }
      else
      {
         // Report the time it took to do this DRS fix, move, etc.
         double dProcessingTimeInSecs = (hrTimer.Read() - dStartTime) / 1000.0;
         MTIostringstream os;
         os << formatShortDuration(dProcessingTimeInSecs) << " sec";
         TRACE_2(errout << "INFO: DRS Processing time: " << os.str());
         DRSState.fixIsPending = true;   // BEFORE fixBeingProcessed = false!
         //MTIassert(IsDRSFixPending());
         DRSState.fixBeingProcessed = false;  // processing is done
         TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = FALSE (3)");
         GetToolProgressMonitor()->SetStatusMessage("Processing complete - fix is pending");
         GetToolProgressMonitor()->SetToolMessage(os.str().c_str());

         // To be able to see the fix, we need to turn off the
         // "ORIGINAL VALUES" display filter mode!
         int oldFilter = getSystemAPI()->getPlaybackFilter();
         if ((oldFilter & PLAYBACK_FILTER_ORIGINAL_VALUES) != 0)
         {
            getSystemAPI()->setPlaybackFilter(
                    (oldFilter & ~PLAYBACK_FILTER_ORIGINAL_VALUES),
                    /* refresh frame = */ true);
         }

         // See is we have any reprocessings stacked up
         if (DRSState.fixReprocessPending)
         {
            DRSState.fixReprocessPending = false;
            DRSState.doFixReprocessNow = true;
         }
      }
   }

   // Call parent class' function
   CToolObject::onToolProcessingStatus(autotoolStatus);

   // Schedule updating of GUI buttons - not allowed from thread
   DRSState.updateGUIButtonsNow = true;

   return TOOL_RETURN_CODE_PROCESSED_OK;
}

int CDRSTool::onMouseMove(CUserInput &userInput)
{
   int mouseX = userInput.windowX;
   int mouseY = userInput.windowY;

   // We seem to get called here even if the GOV tool is doing something, so
   // let's just ignore the calls in that case except to keep track of the
   // mouse x,y position
   if (!IsGovActive())
   {
      if ((DRSState.iMouseX != mouseX || DRSState.iMouseY != mouseY) &&
          DRSState.bAutoAcceptOnNextMouseMove)
      {
         // NOTE: Cannot accept GOV in background
         if (IsGovPending())
         {
            getSystemAPI()->AcceptGOV();
         }
         // in following, don't check IsDRSFixPending, because may not be done yet!
         else if (GetAutoAccept() /* && IsDRSFixPending() */)
         {
            // In AutoAccept mode, background thread does the accepting...
            // The theory here is that we want to overlap the AutoAccept operation
            // with the time that the user takes to draw the rectangle!!
            AcceptFixInBackground(false);
         }
         else if (IsDRSFixPending())
         {
            // Synchronize with background thread
            WaitUntilBackgroundThreadIsIdle();
            WaitForProcessingToFinish();

            // We're going to pop up a dialog, so cancel whatever the
            // mouse is doing
            getSystemAPI()->stopStretchRectangleOrLasso();
            DRSState.mouseMode = DRS_MOUSE_MODE_NONE;

            // This will accept only after asking the user if it's OK
            AutoAccept();
         }

         DRSState.bAutoAcceptOnNextMouseMove = false;

         GetToolProgressMonitor()->SetStatusMessage("Selecting new fix area");
      }
   }

   // Always update the saved mouse position
   DRSState.iMouseX = mouseX;
   DRSState.iMouseY = mouseY;

   // Always pass through the mouse move so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}

int CDRSTool::onHeartbeat()
{

   // Check if an override was changed during fix processing
   if (DRSState.doFixReprocessNow)
   {
      DRSState.doFixReprocessNow = false;
      ReprocessFix(true);
   }

   // Check if another thread thinks the GUI might be out of date
   if (DRSState.updateGUIButtonsNow)
   {
      DRSState.updateGUIButtonsNow = false;
      UpdateDRSGuiButtons();
   }

   if (!getSystemAPI()->isDisplaying())
   {
      int currentFrameIndex = getSystemAPI()->getLastFrameIndex();

      // See if we need to jump to the fix frame
      if (DRSState.goToFixFrameNow)
      {
         DRSState.goToFixFrameNow = false;

         // If we're not already there... jump!
         if (fixFrameIndex != currentFrameIndex)
            getSystemAPI()->goToFrame(fixFrameIndex);
      }

      // Check if we are sitting on a frame for which we've already loaded
      // inputs (either by preloading or by doing a DRS fix)
      if (currentFrameIndex == mostRecentInputLoadedFrameIndex)
      {
         CAutoSpinLocker lock(fixesInProgressListLock);

         // We haven't moved to a different frame
         DRSState.wantToPreloadNow = false;

         // Check for background fix status updates
         list<SBGFixTracker *>::iterator iter = fixesInProgressList.begin();
         list<SBGFixTracker *>::iterator stop = fixesInProgressList.end();

         while (iter != stop)
         {
            SBGFixTracker &fixTracker = *(*iter);

            // We enforce a minimum amount of time to display the outline,
            // in case of really fast D-key fixes
            bool minDisplayTimeElapsed = drawingTimer.Read() >
                        (fixTracker.firstDrawnTime + D_KEY_BOX_VISIBLE_TIME);

            // If the state is RESOLVED and the minimum display time has
            // elapsed, or if this is tracking a PRELOAD that has completed,
            // delete the tracker
            if ((fixTracker.status == DRS_BGFTS_PRELOAD_COMPLETE) ||
                ((fixTracker.status == DRS_BGFTS_FIX_RESOLVED) &&
                    minDisplayTimeElapsed))
            {
               // list::erase returns an iterator to the next item
               iter = fixesInProgressList.erase(iter);

               // We need to refresh the frame to remove the outline
               // (unless we just preloaded, which doesn't draw anything)
               if (fixTracker.status != DRS_BGFTS_PRELOAD_COMPLETE)
                  DRSState.needToRefreshFrame = currentFrameIndex;

               // All done with this now!
               delete &fixTracker;

               // Process the next list item, if any
               continue;
            }

            // See if a fix color has changed or if fix outline
            // needs to be erased - this is where we respond to
            // status changes made in the background thread!

            int oldOutlineColor = fixTracker.outlineColor;
            int newOutlineColor = ComputeFixOutlineColor(&fixTracker,
                                                         minDisplayTimeElapsed);
            if (newOutlineColor != oldOutlineColor)
            {
               // draw the outline in the new color (handles INVALID_COLOR ok)
               // NOTE! DrawFixOutline sets outlineColor in the tracker!
               DrawFixOutline(&fixTracker, newOutlineColor);

               // If the color is invalid, means we don't want to show outline
               if (newOutlineColor == INVALID_COLOR)
               {
                  // Have to redraw the frame to get rid of outlines
                  DRSState.needToRefreshFrame = currentFrameIndex;
               }
            }

            ++iter;
         }

         // Try to refresh the frame now (if not, we'll get it on a future
         // heartbeat) - note we avoid refreshing if the provisional isn't
         // pending because the player frame caching doesn't seem to work
         // very well, and we want to avoid reading from disk; seems to work
         // out OK!
         if ((DRSState.needToRefreshFrame == currentFrameIndex) &&
             !DRSState.backgroundAcceptInProgress)
         {
            // If we eliminated one or more outlines, we need to actually refresh
            // the frame to erase them, then drawing will happen in onRedraw()
            if (IsProvisionalPending())
            {
               // If the provisional is pending, just redraw that
               getSystemAPI()->RefreshProvisional();
            }
            else if (!AreAnyOtherFixesQueuedOrInProgress(NULL))
            {
               // We accepted the last fix, so redraw the actual frame,
               // hopefully from a cached copy rather than from disk
               getSystemAPI()->refreshFrameCached();
            }
            DRSState.needToRefreshFrame = -1;
         }
      }
      else // if we're not on the last frame where inputs were loaded
      {
         // If we've been sitting on this frame for a while,
         // preload the frames DRS would need for that frame
         if (!DRSState.wantToPreloadNow)
         {
            // We must've just moved off frame that needed to be redisplayed!
            if (DRSState.needToRefreshFrame != currentFrameIndex)
               DRSState.needToRefreshFrame = -1;

            // Check that we've been sitting at this frame for long enough
            if (getSystemAPI()->getIdleTime() > FRAME_STOP_THRESHOLD)
            {
               // Deferred preload flag
               DRSState.wantToPreloadNow = true;
            }
         }
         if (DRSState.wantToPreloadNow)
         {
            // We can't preload while anything else is going on!
            if (! (IsDRSActive() || IsGovActive() || IsProvisionalPending()))
            {
               // Should be safe to preload right now!
               DRSState.wantToPreloadNow = false;
               PreloadInputFramesInBackground();
            }
         }
      }
   }

   if (_needToRestartTheBackgroundThread)
   {
      _needToRestartTheBackgroundThread = false;
      SpawnBackgroundThread();
   }

   return TOOL_RETURN_CODE_NO_ACTION;

}

void CDRSTool::CleanUpBeforeLeaving()
{
   CAutoSpinLocker lock(fixesInProgressListLock);

   // Remove all trackers and their outlines
   list<SBGFixTracker *>::iterator iter = fixesInProgressList.begin();
   list<SBGFixTracker *>::iterator stop = fixesInProgressList.end();

   for (; iter != stop; ++iter)
   {
      SBGFixTracker &fixTracker = *(*iter);

      if (fixTracker.status != DRS_BGFTS_PRELOAD_COMPLETE &&
                fixTracker.status != DRS_BGFTS_FIX_RESOLVED)
      {
         TRACE_0(errout << "INTERNAL ERROR: BAD FIX TRACKER STATUS AT EXIT: "
                        << ((fixTracker.status == DRS_BGFTS_PRELOAD_QUEUED)
                           ? "DRS_BGFTS_PRELOAD_QUEUED"
                           : ((fixTracker.status == DRS_BGFTS_PRELOADING)
                             ? "DRS_BGFTS_PRELOADING"
                             : ((fixTracker.status == DRS_BGFTS_FIX_QUEUED)
                               ? "DRS_BGFTS_FIX_QUEUED"
                               : ((fixTracker.status == DRS_BGFTS_FIXING)
                                 ? "DRS_BGFTS_FIXING"
                                 : ((fixTracker.status == DRS_BGFTS_FIX_COMPLETE)
                                   ? "DRS_BGFTS_FIX_COMPLETE"
                                   : "DRS_BGFTS_INVALID"))))));
      }

      // list::erase returns an iterator to the next item
      iter = fixesInProgressList.erase(iter);
      delete &fixTracker;
   }

   // refresh in case there were any outlines showing
   getSystemAPI()->refreshFrameCached();
   DRSState.needToRefreshFrame = -1;
}

void CDRSTool::ChangePendingToResolvedInProgressList()
{
   CAutoSpinLocker lock(fixesInProgressListLock);

   list<SBGFixTracker *>::iterator iter = fixesInProgressList.begin();
   list<SBGFixTracker *>::iterator stop = fixesInProgressList.end();

   while (iter != stop)
   {
      SBGFixTracker &fixTracker = *(*iter);
      if (fixTracker.status == DRS_BGFTS_FIX_COMPLETE)
      {
         fixTracker.status = DRS_BGFTS_FIX_RESOLVED;
      }
      ++iter;
   }
}

void CDRSTool::RunAutoReplace()
{
   // Stupid state flag to lock out preloader
   DRSState.foregroundIsActive = true;

   if (GetReplaceModeSourceFrameOption() !=  DRS_REPLSRCFRM_2_FRM_AUTO
   && GetReplaceModeSourceFrameOption() !=  DRS_REPLSRCFRM_3_FRM_AUTO)
   {
      return;
   }

   int autoModeIndex = (GetReplaceModeSourceFrameOption() ==  DRS_REPLSRCFRM_3_FRM_AUTO) ? 1 : 0;

   CAutoErrorReporter autoErr("CDRSTool::RunAutoReplace", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   AutoAccept();

   int frameOne = getSystemAPI()->getMarkIn();
   if (frameOne < 0)
   {
      autoErr.errorCode = RET_FAILURE;
      autoErr.msg << "MARK IN NOT SET" << endl
                  << "Please try again with mark in set to the first frame of the "
                  <<  ((autoModeIndex == 0) ? "four" : "five") << " frame sequence.";
      return;
   }

   CVideoFrameList *frameList = getSystemAPI()->getVideoFrameList();
   if (frameList == 0)
   {
      autoErr.errorCode = RET_FAILURE;
      autoErr.msg << "INTERNAL ERROR" << endl << "No clip loaded?";
      return;
   }

   int maxFrameIndex = frameOne + ((autoModeIndex == 0) ? 4 : 6);
   if (maxFrameIndex >= frameList->getOutFrameIndex())
   {
      autoErr.errorCode = RET_FAILURE;
      autoErr.msg << "TOO CLOSE TO END OF CLIP" << endl << "Mark In must be at least " << ((autoModeIndex == 0) ? 4 : 5) << " frames from the end of the clip!";
      return;
   }

   MTIassert(DRSState.mainProcessType == DRS_PROCESS_NO_MOTION
            || DRSState.mainProcessType == DRS_PROCESS_LOW_MOTION
            || DRSState.mainProcessType == DRS_PROCESS_HIGH_MOTION);
   MTIassert(DRSState.subProcessType == DRS_SUBPROCESS_C);

   // Left and top need to be negative to no-op the blend border.
   currentUserRect.left = -1000;
   currentUserRect.top = -1000;
   currentUserRect.right = 10000;
   currentUserRect.bottom = 10000;
   currentUserLassoPointer = NULL;
   currentUserBezierPointer = NULL;

   currentAutoReplaceMarkIn = frameOne;
   currentAutoReplaceMarkOut = frameOne + 3 + autoModeIndex;
   int nIterations = 2 + autoModeIndex;

   for (auto pass = 1; pass <= nIterations; ++pass)
   {
      MTIostringstream os;
      os << "Fixing frame " << pass << " of " << nIterations << "  ";
      GetToolProgressMonitor()->SetToolMessage(os.str().c_str());

      currentAutoReplaceFixFrame = frameOne + pass;

      getSystemAPI()->goToFrameSynchronous(currentAutoReplaceFixFrame);
      ProcessNewFix();

      // Having trouble getting the first two operations to be accepted,
      // so try synchronizing.
      WaitUntilBackgroundThreadIsIdle();
      WaitForProcessingToFinish();
      AcceptFix(false);
      WaitUntilBackgroundThreadIsIdle();
   }

   DRSState.foregroundIsActive = false;
   GetToolProgressMonitor()->SetToolMessage(NULL);
}

int CDRSTool::GatherDRSParameters(SDRSParameters &drsParams)
{
   drsParams.mainProcessType = DRSState.mainProcessType;
   drsParams.subProcessType = DRSState.subProcessType;

   drsParams.userRect = currentUserRect;
   drsParams.useLasso = false;
   if (currentUserLassoPointer != NULL)
   {
      CopyLasso(currentUserLassoPointer, drsParams.userLassoPoints);
      drsParams.useLasso = true;
   }
   drsParams.userBezierPointer = currentUserBezierPointer;

   // Parameters from the DRS Ini file.
   // Note from JS: I'm not sure if the DRS Parameters
   // is a good place to pass these values.  May be better to have the
   // CDRSToolProc access the DRS ini file directly for use with PDL or
   // whatever
   // note from KM:  the contents of the ini file are dynamic.  The
   // operator may change the values from time to time.  It is important
   // for the PDL tool to have the ini file values in force when the region
   // was selected.
   SDRSIniParameters dip = DRSParam.getActiveIniParameters(GetImageResolution());

   drsParams.lBorderSize = dip.lBorderSize;
   drsParams.lBorderMin = dip.lBorderMin;
   drsParams.lBorderPercent = dip.lBorderPercent;
   drsParams.commandFileFlag = dip.bCommandFile;
   drsParams.lMotionOffsetLow = dip.lMotionOffsetLow;
   drsParams.lMotionOffsetHigh = dip.lMotionOffsetHigh;
	drsParams.lPaddingAmountLow = dip.lPaddingAmountLow;
	drsParams.lPaddingAmountHigh = dip.lPaddingAmountHigh;
	drsParams.lSurroundRadius = dip.lSurroundRadius;
	drsParams.lRowAccel = dip.lRowAccel;
	drsParams.lColAccel = dip.lColAccel;
	drsParams.lGrainSize = dip.lGrainSize;

	if ((DRSState.mainProcessType == DRS_PROCESS_NO_MOTION
		  || DRSState.mainProcessType == DRS_PROCESS_LOW_MOTION
		  || DRSState.mainProcessType == DRS_PROCESS_HIGH_MOTION)
		&& DRSState.subProcessType == DRS_SUBPROCESS_C)
	{
		// Import Frames
		int currentIndex = getSystemAPI()->getLastFrameIndex(); // Stupid function name
		drsParams.markInIndex = -1;
		drsParams.markOutIndex = -1;
		string errorMessage = "Cannot Replace Region without a Mark In or Mark Out";
		bool checkIfProperMarksWereSet = true;
		auto videoFrameList = getSystemAPI()->getVideoFrameList();
		auto outFrameIndex = videoFrameList->getOutFrameIndex();

		switch (GetReplaceModeSourceFrameOption())
		{
			default:
				errorMessage = "INTERNAL ERROR: invalid Replace Mode source";
				break;

			case DRS_REPLSRCFRM_USE_MARKS:
				drsParams.markInIndex = getSystemAPI()->getMarkIn();
				drsParams.markOutIndex = getSystemAPI()->getMarkOut();
				break;

         case DRS_REPLSRCFRM_PREV_1:
				if (currentIndex < 1)
				{
					errorMessage = "No previous frame, so can't do Prev 1 Replace!";
					break;
				}

				drsParams.markInIndex = currentIndex - 1;
				break;

			case DRS_REPLSRCFRM_PREV_2:
				if (currentIndex < 2)
				{
					errorMessage = "Not enough previous frames to do Prev 2 Replace!";
					break;
				}

				drsParams.markInIndex = currentIndex - 2;
				drsParams.markOutIndex = currentIndex - 1;
				break;

         case DRS_REPLSRCFRM_NEXT_1:
				if (currentIndex > (outFrameIndex - 1))
				{
					errorMessage = "No next frame, so can't do Next 1 Replace!";
					break;
				}

				drsParams.markInIndex = currentIndex + 1;
				break;

			case DRS_REPLSRCFRM_NEXT_2:
				if (currentIndex > (outFrameIndex - 2))
				{
					errorMessage = "Not enough next frames to do Next 2 Replace!";
					break;
				}

				drsParams.markInIndex = currentIndex + 1;
            drsParams.markOutIndex = currentIndex + 2;
            break;

			case DRS_REPLSRCFRM_PREV_AND_NEXT:
				if (currentIndex < 1)
				{
					errorMessage = "No previous frame, so can't do Prev and Next Replace!";
					break;
				}

				if (currentIndex > (outFrameIndex - 1))
				{
					errorMessage = "No next frame, so can't do Prev and Next Replace!";
					break;
				}

				drsParams.markInIndex = currentIndex - 1;
				drsParams.markOutIndex = currentIndex + 1;
				break;

			case DRS_REPLSRCFRM_CURRENT_FRAME:
				drsParams.markInIndex = currentIndex;
				break;

         case DRS_REPLSRCFRM_2_FRM_AUTO:
         case DRS_REPLSRCFRM_3_FRM_AUTO:
            drsParams.markInIndex = currentAutoReplaceMarkIn;
				drsParams.markOutIndex = currentAutoReplaceMarkOut;
				checkIfProperMarksWereSet = false;
				break;
      }

		if (checkIfProperMarksWereSet && drsParams.markInIndex == -1 && drsParams.markOutIndex == -1)
		{
			_MTIErrorDialog(errorMessage);
			return -1;
		}

    drsParams.lMotionOffsetLow = dip.lImportOffsetLow;
    drsParams.lMotionOffsetHigh = dip.lImportOffsetHigh;
   }

   // get the framing info so that we can instruct algorithm to work in
   // single field mode, if necessary

   int videoFramingIndex = getSystemAPI()->getVideoFramingIndex();
   if (videoFramingIndex == FRAMING_SELECT_FIELD_1_ONLY
       || videoFramingIndex == FRAMING_SELECT_FIELD_2_ONLY
       || videoFramingIndex == FRAMING_SELECT_FIELDS_1_2)
      {
      drsParams.bFieldRepeat = true;
      }
   else
      {
      drsParams.bFieldRepeat = false;
      }

   drsParams.bReprocessingOverrides = DRSState.bReprocessingOverrides;
   drsParams.bReprocessingFullFix = DRSState.bReprocessingFullFix;
   drsParams.Overrides = DRSState.Overrides;
   drsParams.lBlendType = DRSState.iBlendType;
   drsParams.bUseGrainPresets = GetUseGrainPresets();

   drsParams.pRepairDRS = &repairDRS;

   drsParams.pWhereToCopyTheFixRegionListTo = &fixedPixelRegionList;
   drsParams.pCurrentFixRegionSerial = &fixedPixelRegionListSerial;
   drsParams.myFixRegionSerial = ++fixedPixelRegionListSerial;
   CPixelRegionList *tmpPRL = fixedPixelRegionList;
   fixedPixelRegionList = NULL;
   delete tmpPRL;

	// This is the wrong place to read the scene cuts, they should be cached outside
	if (drsParams.mainProcessType == DRS_PROCESS_COLOR_BUMP)
	{
		getSystemAPI()->getCutList(drsParams.cutList);
	}
	// END WRONG PLACE

   // update which buttons pressed
   UpdateDRSGuiButtons();
   return 0;
}

int CDRSTool::ProcessNewFix()
{
   DRSState.bReprocessingOverrides = false;    // Fugly internal state flag
   DRSState.bReprocessingFullFix   = false;    // Fugly internal state flag
   SDRSParameters drsParams;
	int retVal = GatherDRSParameters(drsParams);
   if (retVal != 0)
   {
      return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }

   // only redraw the rectangle if there isn't already a fix pending
   if (!IsDRSFixPending())
   {
      getSystemAPI()->setGraphicsColor(IN_PROGRESS_COLOR);
      getSystemAPI()->drawRectangleFrame(&currentUserRect);
   }

   return SetupAndProcessFix(drsParams);
}

int CDRSTool::ReprocessFix(bool doingOverridesOnly)
{
   // Reprocess is inhibited if we are stretching a new rectangle
   if (DRSState.mouseMode != DRS_MOUSE_MODE_NONE)
   {
      return 0;
   }

   // If something's going on right now, just queue the reprocess command
   // to be done by the background thread.
   // NOTE Can't do Beziers in the background because there's only one of them!
   if (GetAutoAccept() && IsDRSActive()
   && (DRSState.selectShape != SELECT_REGION_BEZIER))
   {
      ReprocessFixInBackground(doingOverridesOnly);
      return 0;
   }

   // Otherwise we're going to try to do the reprocess synchronously

   if (!IsDRSFixPending())
   {
      return 0;   // no fix pending to reprocess
   }

   /**************** HACK ******************************************/
   // if we were asked to reprocess a fix and the fix-pending frame is
   // not currently being displayed, then JUST ACCEPT THE FIX - DO NOT
   // REPROCESS IT. That of course is what to do in auto-accept mode,
   // if we're not in auto accept mode then pop up the obnoxious dialog
   // CAREFUL!! provisional frame index may be -1 while processing a fix
   int provisionalFrameIndex = getSystemAPI()->GetProvisionalFrameIndex();
   if (provisionalFrameIndex != -1 &&
       provisionalFrameIndex != getSystemAPI()->getLastFrameIndex())
   {
      AutoAccept();
      return 0; // DON'T REPROCESS!
   }
   /**************** HACK ******************************************/

   if (DRSState.fixBeingProcessed)
   {
      DRSState.fixReprocessPending = true;
      return 0;
   }

   // Set correct reprocessing state
   DRSState.bReprocessingOverrides = doingOverridesOnly;  // Fugly flag
   DRSState.bReprocessingFullFix = !doingOverridesOnly;   // Fuglier flag

   // If the mode changed we need to go back to square 1
   if (DRSState.bReprocessingFullFix)
   {
      return ProcessNewFix();
   }

   SDRSParameters drsParams;
   int retVal = GatherDRSParameters(drsParams);
   if (retVal != 0)
   {
      return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }

   // only redraw the rectangle if there isn't already a fix pending
   if (!IsDRSFixPending())
   {
      getSystemAPI()->setGraphicsColor(IN_PROGRESS_COLOR);
      getSystemAPI()->drawRectangleFrame(&currentUserRect);
   }

   return SetupAndProcessFix(drsParams);
}

// This function assumes that the rectangle coordinates for the
// fix have already been determined.  If the process types get changed
// this will be called again even though the fix is still pending.
int CDRSTool::SetupAndProcessFix(SDRSParameters &drsParams)
{
   int retVal;
   EToolSetupType toolSetupType;

   DRSState.fixBeingProcessed = true;
   TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = TRUE (1)");
   if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
   {
      isInitingFixEng = true;
      GetToolProgressMonitor()->SetStatusMessage("Initializing fix engine");
      toolSetupType = TOOL_SETUP_TYPE_PRELOAD_ONLY;
      DRSState.notReallyFixingAnything = true;
   }
   else
   {
      isInitingFixEng = false;
      GetToolProgressMonitor()->SetStatusMessage("Processing fix");
      toolSetupType = TOOL_SETUP_TYPE_SINGLE_FRAME;
      DRSState.notReallyFixingAnything = false;
   }

   retVal = ProcessSingleFrameInternal(toolSetupType, drsParams);
   if (retVal == 0)
   {
      retVal = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
   }
   else
   {
      // The background process was not started
      DRSState.fixBeingProcessed = false;
      TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = FALSE (4)");
      DRSState.updateGUIButtonsNow = true;
      //getSystemAPI()->cancelStretchRectangle();
      retVal = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }
   if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
      GetToolProgressMonitor()->SetIdle(false);

   return retVal;

}  // end SetupAndProcessFix

bool CDRSTool::GetNewUserRectangle()
{
  // returns false if the most recent mouse position is outside of the
  // displayed image

  currentUserLassoPointer = NULL;

  // get the center of the user's Quick Select region
  int newx, newy;
  if (!getSystemAPI()->getMousePositionFrame(&newx, &newy))
   {
    return false;
   }

  SDRSIniParameters dip = DRSParam.getActiveIniParameters(GetImageResolution());

  // assume that we're set for RECTANGLE select
  int iWidth, iHeight;
  bool useLasso = false;
  switch (dip.iQuickSelect)
   {
    case QS_SELECT_RECT:
      iWidth = dip.iQSRect0Size;
      iHeight = dip.iQSRect0Size;
    break;
    case QS_SELECT_RECT_HALF_WIDTH:
      iWidth = dip.iQSRect1Size/2;
      iHeight = dip.iQSRect1Size;
    break;
    case QS_SELECT_RECT_HALF_HEIGHT:
      iWidth = dip.iQSRect2Size;
      iHeight = dip.iQSRect2Size/2;
    break;
    case QS_SELECT_ELLIPSE:
      useLasso = true;
      iWidth = dip.iQSEllipse0Size;
      iHeight = dip.iQSEllipse0Size;
    break;
    case QS_SELECT_ELLIPSE_HALF_WIDTH:
      useLasso = true;
      iWidth = dip.iQSEllipse1Size/2;
      iHeight = dip.iQSEllipse1Size;
    break;
    case QS_SELECT_ELLIPSE_HALF_HEIGHT:
      useLasso = true;
      iWidth = dip.iQSEllipse2Size;
      iHeight = dip.iQSEllipse2Size/2;
    break;
   }

  currentUserRect.left = newx - iWidth/2;
  currentUserRect.top = newy - iHeight/2;
  currentUserRect.right = currentUserRect.left + iWidth;
  currentUserRect.bottom = currentUserRect.top + iHeight;

  // if an ellipse is called for, we'll use (newx,newy)
  // and (iWidth,iHeight) to calculate it. But either
  // way, we'll want to clip the bounding rectangle
  getSystemAPI()->clipRectangleFrame(&currentUserRect);

  double radius = sqrt((double)(iWidth*iHeight));
  if (radius < 6)
     useLasso = false;

  if (useLasso) // need elliptical lasso
   {
     // get the number of chords per quadrant
     int n = sqrt(radius)/2.0;
     if (n < 2)
        n=2;

     // allocate storage for the lasso, but only if necessary
     int newPointCount = 4 * n + 1;
     if (newPointCount > quickSelectLassoPointCount)
      {
       delete [] quickSelectLassoPointer;    // delete previous quick lasso
       quickSelectLassoPointer = new POINT[newPointCount];
       quickSelectLassoPointCount = newPointCount;
      }

     // generate the vertices as points on an ellipse
     for (int i=0;i<4*n;i++) {
        quickSelectLassoPointer[i].x = newx + (iWidth/2 )*cos(1.57*i/n);
        quickSelectLassoPointer[i].y = newy - (iHeight/2)*sin(1.57*i/n);
     }

     // make sure the lasso is explicitly closed (!)
     quickSelectLassoPointer[4*n] = quickSelectLassoPointer[0];

     // clip the lasso vertices to the frame - the resultant
     // lasso will fall within the clipped currentUserRect
     getSystemAPI()->clipLassoFrame(quickSelectLassoPointer);

     currentUserLassoPointer = quickSelectLassoPointer;
   }

   return true;
}

void CDRSTool::WaitForProcessingToFinish()
{
   // Wait for processing to complete
   if (DRSState.fixBeingProcessed)
   {
      getSystemAPI()->WaitForToolProcessing();

      int count = 1000; // a huge number
      while (DRSState.fixBeingProcessed && (--count > 0))
      {
          MTImillisleep(1);
      }

      MTIassert( DRSState.fixBeingProcessed == false );
   }
}

bool CDRSTool::IsShowing(void)
{
   return(IsToolVisible());
}

SOverrides CDRSTool::GetOverrides()
{
   SOverrides result;

   result.iOverrideColorBrightness = DRSState.Overrides.iOverrideColorBrightness;
   result.iOverrideGrainStrength = DRSState.Overrides.iOverrideGrainStrength;
   result.iOverrideBorderRadius = DRSState.Overrides.iOverrideBorderRadius;
   result.fOverrideMoveOverX = DRSState.Overrides.fOverrideMoveOverX;
   result.fOverrideMoveOverY = DRSState.Overrides.fOverrideMoveOverY;
   result.fOverrideMoveUnderX = DRSState.Overrides.fOverrideMoveUnderX;
   result.fOverrideMoveUnderY = DRSState.Overrides.fOverrideMoveUnderY;
   result.fOverrideRotateOver = DRSState.Overrides.fOverrideRotateOver;
   result.fOverrideRotateUnder = DRSState.Overrides.fOverrideRotateUnder;
   result.iOverrideTransparency = DRSState.Overrides.iOverrideTransparency;

   return SOverrides(result);
}

void CDRSTool::SetOverrides(const SOverrides &newOverrides)
{
   if (
      DRSState.Overrides.iOverrideColorBrightness != newOverrides.iOverrideColorBrightness ||
      DRSState.Overrides.iOverrideGrainStrength != newOverrides.iOverrideGrainStrength ||
      DRSState.Overrides.iOverrideBorderRadius != newOverrides.iOverrideBorderRadius ||
      DRSState.Overrides.fOverrideMoveOverX != newOverrides.fOverrideMoveOverX ||
      DRSState.Overrides.fOverrideMoveOverY != newOverrides.fOverrideMoveOverY ||
      DRSState.Overrides.fOverrideMoveUnderX != newOverrides.fOverrideMoveUnderX ||
      DRSState.Overrides.fOverrideMoveUnderY != newOverrides.fOverrideMoveUnderY ||
      DRSState.Overrides.fOverrideRotateOver != newOverrides.fOverrideRotateOver ||
      DRSState.Overrides.fOverrideRotateUnder != newOverrides.fOverrideRotateUnder ||
      DRSState.Overrides.iOverrideTransparency != newOverrides.iOverrideTransparency
      )
   {
      DRSState.Overrides.iOverrideColorBrightness = newOverrides.iOverrideColorBrightness;
      DRSState.Overrides.iOverrideGrainStrength = newOverrides.iOverrideGrainStrength;
      DRSState.Overrides.iOverrideBorderRadius = newOverrides.iOverrideBorderRadius;
      DRSState.Overrides.fOverrideMoveOverX = newOverrides.fOverrideMoveOverX;
      DRSState.Overrides.fOverrideMoveOverY = newOverrides.fOverrideMoveOverY;
      DRSState.Overrides.fOverrideMoveUnderX = newOverrides.fOverrideMoveUnderX;
      DRSState.Overrides.fOverrideMoveUnderY = newOverrides.fOverrideMoveUnderY;
      DRSState.Overrides.fOverrideRotateOver = newOverrides.fOverrideRotateOver;
      DRSState.Overrides.fOverrideRotateUnder = newOverrides.fOverrideRotateUnder;
      DRSState.Overrides.iOverrideTransparency = newOverrides.iOverrideTransparency;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}

void CDRSTool::SetOverrideColorBrightness(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideColorBrightness;

   DRSState.Overrides.iOverrideColorBrightness = newValue;
   if (DRSState.Overrides.iOverrideColorBrightness < -100)
     DRSState.Overrides.iOverrideColorBrightness = -100;
   else if (DRSState.Overrides.iOverrideColorBrightness > 100)
     DRSState.Overrides.iOverrideColorBrightness = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideColorBrightness != oldValue)
      ReprocessFix(true);
}

void CDRSTool::SetOverrideColorSaturation(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideColorSaturation;

   DRSState.Overrides.iOverrideColorSaturation = newValue;
   if (DRSState.Overrides.iOverrideColorSaturation < -100)
     DRSState.Overrides.iOverrideColorSaturation = -100;
   else if (DRSState.Overrides.iOverrideColorSaturation > 100)
     DRSState.Overrides.iOverrideColorSaturation = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideColorSaturation != oldValue)
      ReprocessFix(true);
}

void CDRSTool::SetOverrideColorRed(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideColorRed;

   DRSState.Overrides.iOverrideColorRed = newValue;
   if (DRSState.Overrides.iOverrideColorRed < -100)
     DRSState.Overrides.iOverrideColorRed = -100;
   else if (DRSState.Overrides.iOverrideColorRed > 100)
     DRSState.Overrides.iOverrideColorRed = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideColorRed != oldValue)
      ReprocessFix(true);
}

void CDRSTool::SetOverrideColorGreen(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideColorGreen;

   DRSState.Overrides.iOverrideColorGreen = newValue;
   if (DRSState.Overrides.iOverrideColorGreen < -100)
     DRSState.Overrides.iOverrideColorGreen = -100;
   else if (DRSState.Overrides.iOverrideColorGreen > 100)
     DRSState.Overrides.iOverrideColorGreen = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideColorGreen != oldValue)
      ReprocessFix(true);
}

void CDRSTool::SetOverrideColorBlue(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideColorBlue;

   DRSState.Overrides.iOverrideColorBlue = newValue;
   if (DRSState.Overrides.iOverrideColorBlue < -100)
     DRSState.Overrides.iOverrideColorBlue = -100;
   else if (DRSState.Overrides.iOverrideColorBlue > 100)
     DRSState.Overrides.iOverrideColorBlue = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideColorBlue != oldValue)
      ReprocessFix(true);
}

void CDRSTool::SetOverrideGrainStrength(int newValue)
{
	int oldValue = DRSState.Overrides.iOverrideGrainStrength;

	DRSState.Overrides.iOverrideGrainStrength = newValue;
	if (DRSState.Overrides.iOverrideGrainStrength < 0)
	{
		DRSState.Overrides.iOverrideGrainStrength = 0;
	}
	else if (DRSState.Overrides.iOverrideGrainStrength > 100)
	{
		DRSState.Overrides.iOverrideGrainStrength = 100;
	}

	// If there is a fix pending, re-do it with new overrides
	if (DRSState.Overrides.iOverrideGrainStrength != oldValue)
	{
		ReprocessFix(true);
	}
}

void CDRSTool::SetOverrideGrainStrengthDefault(int newValue)
{
	int oldValue = DRSState.Overrides.iOverrideGrainStrengthDefault;

	DRSState.Overrides.iOverrideGrainStrengthDefault = newValue;
	if (DRSState.Overrides.iOverrideGrainStrengthDefault < 0)
	{
		DRSState.Overrides.iOverrideGrainStrengthDefault = 0;
	}
	else if (DRSState.Overrides.iOverrideGrainStrengthDefault > 100)
	{
		DRSState.Overrides.iOverrideGrainStrengthDefault = 100;
	}
}

void CDRSTool::SetOverrideBorderRadius(int newValue)
{
   if (DRSState.Overrides.iOverrideBorderRadius != newValue)
   {
      DRSState.Overrides.iOverrideBorderRadius = newValue;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideMoveOverX(float newOffset)
{
   if (DRSState.Overrides.fOverrideMoveOverX != newOffset)
   {
      DRSState.Overrides.fOverrideMoveOverX = newOffset;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideMoveOverY(float newOffset)
{
   if (DRSState.Overrides.fOverrideMoveOverY != newOffset)
   {
      DRSState.Overrides.fOverrideMoveOverY = newOffset;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideMoveUnderX(float newOffset)
{
   if (DRSState.Overrides.fOverrideMoveUnderX != newOffset)
   {
      DRSState.Overrides.fOverrideMoveUnderX = newOffset;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideMoveUnderY(float newOffset)
{
   if (DRSState.Overrides.fOverrideMoveUnderY != newOffset)
   {
      DRSState.Overrides.fOverrideMoveUnderY = newOffset;

       // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideRotateOver(float newOffset)
{
   if (DRSState.Overrides.fOverrideRotateOver != newOffset)
   {
      DRSState.Overrides.fOverrideRotateOver = newOffset;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideRotateUnder(float newOffset)
{
   if (DRSState.Overrides.fOverrideRotateUnder != newOffset)
   {
      DRSState.Overrides.fOverrideRotateUnder = newOffset;

      // If there is a fix pending, re-do it with new overrides
      ReprocessFix(true);
   }
}


void CDRSTool::SetOverrideTransparency(int newValue)
{
   int oldValue = DRSState.Overrides.iOverrideTransparency;

   DRSState.Overrides.iOverrideTransparency = newValue;
   if (DRSState.Overrides.iOverrideTransparency < 0)
     DRSState.Overrides.iOverrideTransparency = 0;
   if (DRSState.Overrides.iOverrideTransparency > 100)
     DRSState.Overrides.iOverrideTransparency = 100;

   // If there is a fix pending, re-do it with new overrides
   if (DRSState.Overrides.iOverrideTransparency != oldValue)
      ReprocessFix(true);
}

bool CDRSTool::GetOverrideStickiness()
{
   return(DRSState.bOverridesAreSticky);
}

bool CDRSTool::GetOverrideStickiness(int which)
{
   switch (which)
   {
      case OM_MOVE_UNDER:
         return DRSState.bMoveUnderIsSticky;
      case OM_MOVE_OVER:
         return DRSState.bMoveOverIsSticky;
      case OM_CHANGE_GRAIN:
         return DRSState.bGrainIsSticky;
      case OM_CHANGE_BORDER:
         return DRSState.bBorderIsSticky;
      case OM_ROTATE_OVER:
         return DRSState.bRotateOverIsSticky;
      case OM_ROTATE_UNDER:
         return DRSState.bRotateUnderIsSticky;
      case OM_CHANGE_TRANSPARENCY:
         return DRSState.bTransparencyIsSticky;
      case OM_CHANGE_COLOR:
         return DRSState.bColorIsSticky;
   }

   return false;
}

void CDRSTool::SetBorderEnabled(bool flag)
{
   DRSState.iBlendType = flag? BLEND_TYPE_LINEAR : BLEND_TYPE_NONE;

   // If there is a fix pending, re-do it with new overrides
   ReprocessFix(true);
}

void CDRSTool::SetUseGrainPresets(bool flag)
{
	_grainPresets.SetEnabled(flag);
	SetOverrideGrainStrengthDefault(flag ? 50 : 0);

   // If there is a fix pending, re-do it with new overrides
   ReprocessFix(true);
}

void CDRSTool::SelectGrainPreset(GrainParameterBaseType grainParametersBase, int grainParametersPresetIndex)
{
   GetGrainPresets().SetSelectedBase(grainParametersBase);
   GetGrainPresets().SetSelectedIndex(grainParametersPresetIndex);
   ReprocessFix(true);
}

void CDRSTool::SetOverrideStickiness(bool bSticky)
{
   DRSState.bOverridesAreSticky = bSticky;
   SaveStatesToIniFile();
}

void CDRSTool::SetOverrideStickiness(int which, bool bSticky)
{
   switch (which)
   {
      case OM_MOVE_UNDER:
         DRSState.bMoveUnderIsSticky = bSticky;
         break;
      case OM_MOVE_OVER:
         DRSState.bMoveOverIsSticky = bSticky;
         break;
      case OM_CHANGE_GRAIN:
         DRSState.bGrainIsSticky = bSticky;
         break;
      case OM_CHANGE_BORDER:
         DRSState.bBorderIsSticky = bSticky;
         break;
      case OM_ROTATE_OVER:
         DRSState.bRotateOverIsSticky = bSticky;
         break;
      case OM_ROTATE_UNDER:
         DRSState.bRotateUnderIsSticky = bSticky;
         break;
      case OM_CHANGE_TRANSPARENCY:
         DRSState.bTransparencyIsSticky = bSticky;
         break;
      case OM_CHANGE_COLOR:
         DRSState.bColorIsSticky = bSticky;
         break;
   }

   SaveStatesToIniFile();
}

//--------------------------------------------------------------------------

int CDRSTool::ApplyPendingFixToMarkedRange()
{
   int retVal = 0;

   // Short circuit if nothing to rerun!
   if (!IsDRSFixPending())
      return retVal;

   // Only allow 'hard motion' fixes to be applied to multiple frames
   if (GetMainProcessType() != DRS_PROCESS_HARD_MOTION)
      return retVal;

   // Check Mark In and Mark Out
   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();
   if (markIn < 0 || markOut < 0 || markOut <= markIn)
   {
      // Marks are bogus, so complain to user and refuse to do anything
      _MTIErrorDialog("Cannot apply fix to marked range    \n"
                      "because the marks are not valid");
      return -1;
   }

   // Next have user confirm intentions
   MTIostringstream os;
   int nFrames = markOut - markIn;
   os << "You are about to apply the pending fix to "
      << nFrames << " frame"  << ((nFrames == 1)? "" : "s") << ".   \n"
      << "Do you wish to proceed?    ";
   retVal = _MTIWarningDialog(os.str());
   if (retVal != MTI_DLG_OK)
      return 0;

   // THE FOLLOWING LINE IS NOT RIGHT IF THE PROVISIONAL FRAME INDEX LIES
   // OUTSIDE THE RANGE markIn to markOut !!! QQQ

   // REJECT the frame we just did  because we will re-do this frame as
   // part of the marked range sequence
   getSystemAPI()->RejectProvisional(true);  // true = go to frame - WHY?
   DRSState.fixIsPending = false;

   // Redraw the rectangle;
   // not sure if this is needed - it's done in onRedraw()
   RECT tempRect = currentUserRect;
   getSystemAPI()->fattenRectangleFrame(&tempRect,
                                        ORIGINAL_RECT_BORDER);
   getSystemAPI()->setGraphicsColor(IN_PROGRESS_COLOR);
   getSystemAPI()->drawRectangleFrame(&tempRect);

   // All systems go
   retVal = StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);

   getSystemAPI()->setMainWindowFocus();

   return retVal;
}
//--------------------------------------------------------------------------

int CDRSTool::RunFrameProcessing(int newResumeFrame)
{
   int retVal;

   // Make sure nothing's going on in the background!
	WaitUntilBackgroundThreadIsIdle();
	WaitForProcessingToFinish();
	AutoAccept();
	getSystemAPI()->ClearProvisional(true);

   // Replace the single-frame tool setup with a multi-frame one
   DestroyAllToolSetups();
   CToolIOConfig ioConfig(1, 1);

   // HACK - Stupid Tool Infrastructure does not pass setup type
   // to our callback makeToolProcessorInstance, so we stick it in a global
   currentToolSetupType = TOOL_SETUP_TYPE_MULTI_FRAME;
   //

   multiFrameToolSetupHandle =
             getSystemAPI()->MakeSimpleToolSetup(GetToolName(),
                                                 TOOL_SETUP_TYPE_MULTI_FRAME,
                                                 &ioConfig);
   if (multiFrameToolSetupHandle < 0)
      return -1;

   multiFrameToolSetupHandleIsValid = true;

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(multiFrameToolSetupHandle);
   if (retVal != 0)
      return retVal;

   // Huh - why the heck wouldn't it be stopped??
   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_STOPPED)
   {
      // Set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      frameRange.inFrameRange[0].inFrameIndex = getSystemAPI()->getMarkIn();
      frameRange.inFrameRange[0].outFrameIndex = getSystemAPI()->getMarkOut();
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         return retVal;
   }

   // Set to render
   getSystemAPI()->SetProvisionalRender(true);
   getSystemAPI()->SetProvisionalReview(false);       // maybe want true? QQQ
   getSystemAPI()->SetProvisionalReprocess(false);

   // Set tool's parameters
   CToolParameters *toolParams;
   CDRSToolParameters *drsToolParams = new CDRSToolParameters;
   retVal = GatherDRSParameters(drsToolParams->drsParams);
   toolParams = drsToolParams;
   if (retVal != 0)
      return retVal;
   // Note: system will delete the CToolParameter instance
   //       when it is done with it.
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      return retVal;

   // Set some state
   DRSState.fixBeingProcessed = true;
   TRACE_3(errout << "!!!!!!!!!!! fixBeingProcessed = TRUE (2)");
   DRSState.fixingMultipleFrames = true;

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return retVal;
}
//--------------------------------------------------------------------------

void CDRSTool::SetGOVRepeatShape()
{
   // NOTE! Don't use DRSState.selectShape - it's just a reflection of the
   // GUI buttons - instead need to look at lasso and bezier pointers
   if (fixedPixelRegionList != NULL)
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_REGION,
                                        fixedPixelRegionList->GetBoundingBox(),
                                        NULL,
                                        fixedPixelRegionList);
   }
   else if (currentUserLassoPointer != NULL)
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_LASSO,
                                        currentUserRect,
                                        currentUserLassoPointer,
                                        NULL);
   }
   else if (currentUserBezierPointer != NULL)
   {
      // Gov tool doesn't do Beziers, but it does do pixel regions!
      const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
      if (imageFormat != NULL)
      {
         int totalFrameWidth = imageFormat->getTotalFrameWidth();
         int totalFrameHeight = imageFormat->getTotalFrameHeight();
         int pxlComponentCount = imageFormat->getComponentCountExcAlpha();
         CBezierPixelRegion* bezierRegion = new CBezierPixelRegion(
                                                   currentUserBezierPointer,
                                                   NULL,   // Not populated!
                                                   totalFrameWidth,
                                                   totalFrameHeight,
                                                   pxlComponentCount);
         CPixelRegionList pixelRegionList;
         pixelRegionList.Add(bezierRegion);
         getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_REGION,
                                           bezierRegion->GetRect(),
                                           NULL,
                                           &pixelRegionList);
      }
   }
   else // Rect
   {
      getSystemAPI()->SetGOVRepeatShape(SELECTED_REGION_SHAPE_RECT,
                                        currentUserRect, NULL, NULL);
   }
   DRSState.bLastActionWasGOV = false;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void CDRSTool::SpawnBackgroundThread()
{
   MTIassert(backgroundThreadID == NULL);

   TRACE_3(errout << "DRS FGD: SPAWN BACKGROUND THREAD");

   if (backgroundThreadID == NULL)
   {
      BThreadSpawn(StartBackgroundThread, reinterpret_cast<void *>(this));
   }
}

void CDRSTool::KillBackgroundThread()
{
   MTIassert(backgroundThreadID != NULL);

   // To ensure the background thread is idle, we send it a NOOP command
   // with a semaphore to signal when done.

   if (backgroundThreadID != NULL)
   {
      SBGTCommand command;

      command.command = DRS_BGTC_SHUTDOWN;
      backgroundCommandQueue.Add(command);
      CHRTimer timer;
      TRACE_3(errout << "DRS_BGTC_SHUTDOWN suspending...");
      SemSuspend(backgroundSynchSemaphore);  // synchronous
      TRACE_3(errout << "DRS_BGTC_SHUTDOWN woke up after " << timer.ReadAsString() << " msec");
   }
}

void CDRSTool::WaitUntilBackgroundThreadIsIdle()
{
   MTIassert(backgroundThreadID != NULL);

   // The background thread will signal us when it reaches this command

   if (backgroundThreadID != NULL)
   {
      SBGTCommand command;

      DRSState.foregroundIsWaitingForBackground = true; // HACK to avoid deadlock

      command.command = DRS_BGTC_SYNCHRONIZE;
      backgroundCommandQueue.Add(command);
      CHRTimer timer;
      TRACE_3(errout << "DRS_BGTC_SYNCHRONIZE suspending...");
      SemSuspend(backgroundSynchSemaphore);
      TRACE_3(errout << "DRS_BGTC_SYNCHRONIZE woke up after " << timer.ReadAsString() << " msec");

      DRSState.foregroundIsWaitingForBackground = false;
   }
}

/* static */
void CDRSTool::StartBackgroundThread (void *vpAppData, void *vpReserved)
{
   CDRSTool *_this = static_cast<CDRSTool*>(vpAppData);

   _this->backgroundThreadID = vpReserved;

   if (BThreadBegin(vpReserved))
   {
      exit(1); // say what???
   }

   int errorCode = 0;
   bool sehError = false;
   auto tryFilterException = [&errorCode](int code)
   {
      return EXCEPTION_EXECUTE_HANDLER;
   };

   CAutoErrorReporter autoErr("DRS Background Fix Thread", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   __try
   {
      try
      {
         _this->RunBackgroundThread();
      }
      catch (const std::exception &ex)
      {
         TRACE_0(errout << "***ERROR**** DRS BACKGROUND REPAIR THREAD CRASHED: " << ex.what());
         autoErr.errorCode = -10100;
         autoErr.msg << "DRS BACKGROUND REPAIR THREAD CRASHED! Reason: " << endl << ex.what();

         _this->backgroundThreadID = NULL; // signal we have exited
         _this->_needToRestartTheBackgroundThread = true;
      }
   }
   __except (tryFilterException(GetExceptionCode()))
   {
      errorCode = GetExceptionCode();
      sehError = true;
   }

   // Any I/O in the __except could crash system
   if (sehError)
   {
      TRACE_0(errout << "***ERROR**** DRS BACKGROUND REPAIR THREAD CRASHED: non-standard error: " << std::hex << errorCode);
      autoErr.errorCode = -10101;
      autoErr.msg << "DRS BACKGROUND REPAIR THREAD CRASHED - reason: NON_STANDARD EXCEPTION: " << std::hex << errorCode;

      _this->_needToRestartTheBackgroundThread = true;
   }

   _this->backgroundThreadID = NULL; // signal we have exited
}
//-------------------------------------------------------------------------

void CDRSTool::setTrackerStatusFromBackground(SBGFixTracker *fixTracker,
                                              EBGFixTrackingStatus newStatus)
{
   fixTracker->status = newStatus;
   getSystemAPI()->FireRefreshTimerFromBackground();
}
//-------------------------------------------------------------------------

void CDRSTool::RunBackgroundThread()
{
   bool keepGoing = true;
   SBGTCommand bgtCommand;
   setlocale(LC_ALL, "C");

   while (keepGoing)
   {
      // Sleep until there's something to do
      if (backgroundCommandQueue.Empty())
      {
         DRSState.backgroundIsActive = false;
         backgroundCommandQueue.Wait();
      }

      // Pull the command out of the queue
      // NOTE: doesn't apply here, but if there are ever multiple
      // background threads, you have to use a try-catch to capture
      // the "empty queue" exception if another thread grabs the
      // command first!
      bgtCommand = backgroundCommandQueue.Take();

      // These two cases are always handled without delay
      switch (bgtCommand.command)
      {
         case DRS_BGTC_SYNCHRONIZE:
         {
            CHRTimer timer;
            TRACE_3(errout << "DRS_BGTC_SYNCHRONIZE resuming main thread");
            SemResume(backgroundSynchSemaphore);
            TRACE_3(errout << "DRS_BGTC_SYNCHRONIZE returned from resume after " << timer.ReadAsString() << " msec");

            continue;
         }

         case DRS_BGTC_SHUTDOWN:
         {
            CHRTimer timer;
            TRACE_3(errout << "DRS_BGTC_SHUTDOWN resuming main thread");
            SemResume(backgroundSynchSemaphore);
            TRACE_3(errout << "DRS_BGTC_SHUTDOWN returned from resume after " << timer.ReadAsString() << " msec");

            keepGoing = false;
			   continue;
         }

		   default:
		      break;
      }

      MTIassert(bgtCommand.command == DRS_BGTC_PRELOAD_INPUT
             || bgtCommand.command == DRS_BGTC_PROCESS_FIX
             || bgtCommand.command == DRS_BGTC_REPROCESS_FIX
             || bgtCommand.command == DRS_BGTC_ACCEPT_FIX
             || bgtCommand.command == DRS_BGTC_ACCEPT_AND_GOTO_FIX
             || bgtCommand.command == DRS_BGTC_REJECT_FIX
             || bgtCommand.command == DRS_BGTC_REJECT_AND_GOTO_FIX);

      // Make sure there isn't any fixing going on that was fired up
      // directly from the foreground thread. I should've moved ALL
      // the damned processing to the background. Feh.
      while ((!DRSState.foregroundIsWaitingForBackground)
      && (DRSState.foregroundIsActive || DRSState.fixBeingProcessed))
      {
         // Hopefully avoid deadlock with the foreground...
         DRSState.backgroundIsActive = false;
         MTImillisleep(1);
      }

      DRSState.backgroundIsActive = true;

      // Handle the rest of the commands
      switch (bgtCommand.command)
      {
         case DRS_BGTC_PRELOAD_INPUT:

            TRACE_3(errout << "DRS BGT: PRELOAD START");
            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_PRELOADING);
            SetupAndProcessFix(*bgtCommand.fixTracker->drsParams);
            WaitForProcessingToFinish();  // Synchronous in background thread!
            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_PRELOAD_COMPLETE);
            TRACE_3(errout << "DRS BGT: PRELOAD DONE");
            break;

         case DRS_BGTC_PROCESS_FIX:

            TRACE_3(errout << "DRS BGT: PROCESS START");
            AcceptFixFromBackgroundThread();

            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_FIXING);
            SetupAndProcessFix(*bgtCommand.fixTracker->drsParams);
            WaitForProcessingToFinish();  // Synchronous in background thread!
            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_FIX_COMPLETE);
            TRACE_3(errout << "DRS BGT: PROCESS DONE");
            break;

         case DRS_BGTC_REPROCESS_FIX:

            TRACE_3(errout << "DRS BGT: REPROCESS START");

            // Consolidate "reprocess" commands because there's really no
            // point doing one if the next command is also a 'reprocess'!!
            while (!backgroundCommandQueue.Empty())
            {
               // Get current oldest ring buffer element
               SBGTCommand peekCommand = backgroundCommandQueue.Front();
               if (peekCommand.command != DRS_BGTC_REPROCESS_FIX)
               {
                  // The next command is not a 'reprocess', so bail out here
                  break;
               }
               else // Next is 'reprocess'
               {
                  // CAREFUL! If we encounter a bReprocessingFullFix while
                  // coalescing, we need to be sure to preserve it for the
                  // final command we settle on!
                  bool previousReprocessingFullFix =
                     bgtCommand.fixTracker->drsParams->bReprocessingFullFix;

                   // Pretend we did the 'reprocess' and proceed to the next!
                  setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                                 DRS_BGFTS_FIX_COMPLETE);
                  bgtCommand = backgroundCommandQueue.Take();

                  // OK, move the 'full fix' flag forward
                  if (previousReprocessingFullFix)
                  {
                     bgtCommand.fixTracker->drsParams->bReprocessingFullFix = true;
                     bgtCommand.fixTracker->drsParams->bReprocessingOverrides = false;
                  }
               }
            }

            // Return to the originally scheduled processing
            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_FIXING);
            SetupAndProcessFix(*bgtCommand.fixTracker->drsParams);
            WaitForProcessingToFinish();  // Synchronous in background thread!
            setTrackerStatusFromBackground(bgtCommand.fixTracker,
                                           DRS_BGFTS_FIX_COMPLETE);
            TRACE_3(errout << "DRS BGT: REPROCESS DONE");
            break;

         case DRS_BGTC_ACCEPT_FIX:
            TRACE_3(errout << "DRS BGT: ACCEPT");
            AcceptFixFromBackgroundThread();
            break;

         case DRS_BGTC_ACCEPT_AND_GOTO_FIX:
            TRACE_3(errout << "DRS BGT: ACCEPT & GOTO");
            AcceptFixFromBackgroundThread();
            DRSState.goToFixFrameNow = true;
            break;

         case DRS_BGTC_REJECT_FIX:
            TRACE_3(errout << "DRS BGT: REJECT");
            RejectFixFromBackgroundThread();
            break;

         case DRS_BGTC_REJECT_AND_GOTO_FIX:
            TRACE_3(errout << "DRS BGT: REJECT & GOTO");
            RejectFixFromBackgroundThread();
            DRSState.goToFixFrameNow = true;
            break;

         default:
            MTIassert(false);
            break;
      }
   }
}
//-------------------------------------------------------------------------

void CDRSTool::ProcessNewFixInBackground()
{
   ProcessFixInBackground(REPROCESS_TYPE_NONE);
}
//-------------------------------------------------------------------------

void CDRSTool::PreloadInputFramesInBackground()
{
   // Short circuit is something is pending - generally means you
   // don't need to preload anyhow!
   // I THINK THIS IS OK BECAUSE PRELOADING DOES NOT AFFECT THE PROVISIONAL
   //if (IsProvisionalPending())
      //return;

   // Don't bother preloading if the tool is not enabled!!
   if (IsDisabled())
   return;

   // Skip this if user requested no preloading
   if (!GetPreloadInput())
      return;

   int currentFrameIndex = getSystemAPI()->getLastFrameIndex();
   MTIassert(currentFrameIndex >= 0);
   if (currentFrameIndex < 0)
   {
      // Don't know why this happens - probably a bug in the Player
      static bool oneshot = false;
      if (!oneshot)
      {
         oneshot = true;
         _MTIErrorDialog("IN DRS/PRELOAD: currentFrameIndex < 0");
      }
      return;
   }

   mostRecentInputLoadedFrameIndex = currentFrameIndex;

   MTIassert(backgroundThreadID != NULL);

   if (backgroundThreadID != NULL)
   {
      SBGFixTracker *bgFixTracker = new SBGFixTracker;
      // gaack - since we are pretending to do a fix, need to assemble params
      bgFixTracker->drsParams = new SDRSParameters;
      DRSState.bReprocessingOverrides = false; // Fugly internal state flag
      DRSState.bReprocessingFullFix = false; // Fugly internal state flag
      // DOUBLE GAAACK - need to bugger DRSState to avoid unpleasant
      // side effects in GatherDRSParameters (dialog pops up complaining
      // that the marks aren't set right if DRS mode is REPLACE !!!!)
      int saveFrickenProcessType = DRSState.mainProcessType;
      DRSState.mainProcessType = DRS_PROCESS_NOOP;
      int retVal = GatherDRSParameters(*bgFixTracker->drsParams);
      DRSState.mainProcessType = saveFrickenProcessType;
      MTIassert(retVal == 0);
      if (retVal != 0)
         return;
      bgFixTracker->status = DRS_BGFTS_PRELOAD_QUEUED;
      bgFixTracker->outlineColor = INVALID_COLOR;
      bgFixTracker->suppressOutline = true;
      bgFixTracker->firstDrawnTime = 0.0;     // outline never drawn
      bgFixTracker->frameIndex = currentFrameIndex;
      MTIassert(bgFixTracker->frameIndex >= 0);
      {
         CAutoSpinLocker lock(fixesInProgressListLock);
         fixesInProgressList.push_back(bgFixTracker);
      }

      SBGTCommand bgtCommand;
      bgtCommand.command = DRS_BGTC_PRELOAD_INPUT;
      bgtCommand.fixTracker = bgFixTracker;
      backgroundCommandQueue.Add(bgtCommand);
   }
}
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
//NOTE!!!! FIXES ARE PRESENTLY REPROCESSED ONLY IN FOREGROUND (i.e. this
//         method isn't called) only because I ran out of time to test
//         repocessing in the background, and it's seems to be pretty quick
//         now to do it in the foreground. SO THIS CODE PATH IS NOT TESTED!
//-------------------------------------------------------------------------

void CDRSTool::ReprocessFixInBackground(bool overridesOnly)
{
   EReprocessFixType type = overridesOnly? REPROCESS_TYPE_OVERRIDES
                                         : REPROCESS_TYPE_REDO_COMPLETELY;
   ProcessFixInBackground(type);
}
//-------------------------------------------------------------------------

void CDRSTool::ProcessFixInBackground(EReprocessFixType reprocessFixType)
{
   int currentFrameIndex = getSystemAPI()->getLastFrameIndex();
   MTIassert(currentFrameIndex >= 0);
   if (currentFrameIndex < 0)
   {
      // Don't know why this happens - probably a bug in the Player
      static bool oneshot = false;
      if (!oneshot)
      {
         oneshot = true;
         _MTIErrorDialog("IN DRS/PROCESSFIX: currentFrameIndex < 0");
      }
      return;
   }

   mostRecentInputLoadedFrameIndex = currentFrameIndex;

   MTIassert(backgroundThreadID != NULL);

   if (backgroundThreadID != NULL)
   {
      SBGFixTracker *bgFixTracker = new SBGFixTracker;
      SBGTCommand bgtCommand;

      ////////////////////////////////////////////////////
      // Fugly internal state flags
      // MUST BE SET BEFORE CALLING GatherDRSParameters()!
      ////////////////////////////////////////////////////
      DRSState.bReprocessingOverrides = false;
      DRSState.bReprocessingFullFix = false;
      if (reprocessFixType == REPROCESS_TYPE_OVERRIDES)
      {
         DRSState.bReprocessingOverrides = true;
      }
      else if (reprocessFixType == REPROCESS_TYPE_REDO_COMPLETELY)
      {
         DRSState.bReprocessingFullFix = true;
      }
      ////////////////////////////////////////////////////

      bgFixTracker->drsParams = new SDRSParameters;
      int retVal = GatherDRSParameters(*(bgFixTracker->drsParams));
      if (retVal != 0)
         return;
      bgFixTracker->status = DRS_BGFTS_FIX_QUEUED;
      bgFixTracker->outlineColor = INVALID_COLOR;   // will be fixed below
      bgFixTracker->suppressOutline = (reprocessFixType != REPROCESS_TYPE_NONE);
      bgFixTracker->firstDrawnTime = drawingTimer.Read();
      bgFixTracker->frameIndex = currentFrameIndex;
      MTIassert(bgFixTracker->frameIndex >= 0);
      {
         CAutoSpinLocker lock(fixesInProgressListLock);
         fixesInProgressList.push_back(bgFixTracker);

         // Outline will be IN_PROGRESS_COLOR if it's the the only fix
         // queued and no fix is presently in progress, else QUEUED_COLOR
         // NOTE! DrawFixOutline sets outlineColor in the tracker!
         DrawFixOutline(bgFixTracker,
                        ComputeFixOutlineColor(bgFixTracker, false));
      }

      // Never do Beziers in background because there's only one of them
      // in the entire system!
      MTIassert(bgFixTracker->drsParams->userBezierPointer == NULL);

     // QQQ Need to do anything with these drsParams members???
	 // CRepairDRS *pRepairDRS;
     // CPixelRegionList **pWhereToCopyTheFixRegionListTo;

		bgtCommand.command = (reprocessFixType == REPROCESS_TYPE_NONE)?
                                                    DRS_BGTC_PROCESS_FIX :
                                                    DRS_BGTC_REPROCESS_FIX;
      bgtCommand.fixTracker = bgFixTracker;

      backgroundCommandQueue.Add(bgtCommand);
   }
}
//-------------------------------------------------------------------------

void CDRSTool::AcceptFixInBackground(bool goToFrame)
{
   MTIassert(backgroundThreadID != NULL);

   if (backgroundThreadID != NULL)
   {
      SBGTCommand bgtCommand;
      bgtCommand.command = goToFrame? DRS_BGTC_ACCEPT_AND_GOTO_FIX
                                    : DRS_BGTC_ACCEPT_FIX;

      backgroundCommandQueue.Add(bgtCommand);

      // Have to clear non-sticky overrides immediately, not in thread, else
      // they might not get cleared by the time the next fix is gathering
      // its parameters!
      DRSState.ResetOverridesIfNotSticky ();
      DRSState.updateGUIButtonsNow = true;
   }
}
//-------------------------------------------------------------------------

void CDRSTool::RejectFixInBackground(bool goToFrame)
{
   MTIassert(backgroundThreadID != NULL);

   if (backgroundThreadID != NULL)
   {
      SBGTCommand bgtCommand;
      bgtCommand.command = goToFrame? DRS_BGTC_REJECT_AND_GOTO_FIX
                                    : DRS_BGTC_REJECT_FIX;

      backgroundCommandQueue.Add(bgtCommand);

      // Have to clear non-sticky overrides immediately, not in thread, else
      // they might not get cleared by the time the next fix is gathering
      // its parameters!
      DRSState.ResetOverridesIfNotSticky ();
      DRSState.updateGUIButtonsNow = true;
   }
}
//-------------------------------------------------------------------------



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CDRSToolProc::CDRSToolProc(int newToolNumber, const string &newToolName,
                           CToolSystemInterface *newSystemAPI,
                           const bool *newEmergencyStopFlagPtr,
                           IToolProgressMonitor *newToolProgressMonitor,
                           int numberOfOutputPorts,
									GrainPresets *grainPresets)
 : CToolProcessor(newToolNumber, newToolName, 1, numberOfOutputPorts, newSystemAPI,
						newEmergencyStopFlagPtr, newToolProgressMonitor)
 , _grainPresets(grainPresets)
{
   StartProcessThread();
}

CDRSToolProc::~CDRSToolProc()
{

}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

int CDRSToolProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   SetInputMaxFrames(0, 5, 1);  // Input Port 0: 5 frames, replace 1 each iter
   SetOutputModifyInPlace(0, 0, false);
   SetOutputMaxFrames(0, 1);    // Output Port 0 = 1 frame

   return 0;
}

int CDRSToolProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   iInFrameIndex = frameRange.inFrameIndex;
   iOutFrameIndex = frameRange.outFrameIndex;
   iFrameCount = iOutFrameIndex - iInFrameIndex;

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (iFrameCount > 1 && progressMonitor != NULL)
   {
      progressMonitor->SetStatusMessage("Fixing all marked frames");
      progressMonitor->SetFrameCount(iFrameCount);
   }

   return 0;
}

int CDRSToolProc::SetParameters(CToolParameters *toolParams)
{
   CDRSToolParameters *drsToolParams
                               = static_cast<CDRSToolParameters*>(toolParams);

   // Remember the DRS parameters
   drsParams = drsToolParams->drsParams;

   MTIassert(drsParams.mainProcessType == DRS_PROCESS_NOOP        ||
             drsParams.mainProcessType == DRS_PROCESS_NO_MOTION   ||
             drsParams.mainProcessType == DRS_PROCESS_LOW_MOTION  ||
             drsParams.mainProcessType == DRS_PROCESS_HIGH_MOTION ||
             drsParams.mainProcessType == DRS_PROCESS_HARD_MOTION ||
             drsParams.mainProcessType == DRS_PROCESS_COLOR_BUMP);

   switch(drsParams.mainProcessType)
      {
      case DRS_PROCESS_NOOP:
         iFixType = RT_NONE;
         break;

      case DRS_PROCESS_NO_MOTION:
         // Don't need padding if in "no motion" mode! Only one choice of
         // fix source!
         // NOT TRUE!! Need padding so that 'move over' overrides work properly!
         CalculatePaddingPixels(drsParams.lPaddingAmountLow, &lRPadding, &lCPadding);

         // No motion, so no acceleration!
         lRAccel = 0;
         lCAccel = 0;

         // Set motion offsets to 0 as well
         CalculateMotionOffsets(0, &lRMotionOffset, &lCMotionOffset);

			iFixType = (drsParams.subProcessType == DRS_SUBPROCESS_C) ? RT_REPLACE : RT_ORDINARY;
			break;

      case DRS_PROCESS_LOW_MOTION:
         CalculatePaddingPixels(drsParams.lPaddingAmountLow, &lRPadding, &lCPadding);
         lRAccel = drsParams.lRowAccel;
         lCAccel = drsParams.lColAccel;
         CalculateMotionOffsets(drsParams.lMotionOffsetLow, &lRMotionOffset, &lCMotionOffset);
			iFixType = (drsParams.subProcessType == DRS_SUBPROCESS_C) ? RT_REPLACE : RT_ORDINARY;
			break;

	  case DRS_PROCESS_HIGH_MOTION:
         CalculatePaddingPixels(drsParams.lPaddingAmountHigh, &lRPadding, &lCPadding);
         lRAccel = drsParams.lRowAccel;
         lCAccel = drsParams.lColAccel;
         CalculateMotionOffsets(drsParams.lMotionOffsetHigh, &lRMotionOffset, &lCMotionOffset);
			iFixType = (drsParams.subProcessType == DRS_SUBPROCESS_C) ? RT_REPLACE : RT_ORDINARY;
         break;

	  case DRS_PROCESS_HARD_MOTION:
         // TTT need to find out the correct values for Hard Motion
         CalculatePaddingPixels(drsParams.lPaddingAmountLow, &lRPadding, &lCPadding);
         lRAccel = drsParams.lRowAccel;
         lCAccel = drsParams.lColAccel;
         CalculateMotionOffsets(drsParams.lMotionOffsetLow, &lRMotionOffset, &lCMotionOffset);
         switch (drsParams.subProcessType)
         {
            case DRS_SUBPROCESS_NONE:
               iFixType = RT_HARD_MOTION_2;
               break;
            case DRS_SUBPROCESS_A:
               iFixType = RT_HARD_MOTION;
               break;
         }

         break;

	  case DRS_PROCESS_COLOR_BUMP:
         iFixType = RT_COLOR_BUMP;

         // No padding, motion or acceleration.
         lRPadding = 0;
         lCPadding = 0;
         lRAccel = 0;
         lCAccel = 0;
         CalculateMotionOffsets(0, &lRMotionOffset, &lCMotionOffset);

         bIsMonochrome = drsParams.subProcessType == DRS_SUBPROCESS_C
                        || drsParams.subProcessType == DRS_SUBPROCESS_D
                        || drsParams.subProcessType == DRS_SUBPROCESS_E;
         iDirection = (drsParams.subProcessType == DRS_SUBPROCESS_A
                        || drsParams.subProcessType == DRS_SUBPROCESS_D)
                        ? -1
                        : ((drsParams.subProcessType == DRS_SUBPROCESS_B
                           || drsParams.subProcessType == DRS_SUBPROCESS_E)
                           ? 1
                           : 0);
         break;

     default:
         iFixType = RT_NONE;
         break;
     }

   return 0;
}

void CDRSToolProc::CalculateMotionOffsets(int configOffset,
                                          long *lRowOffset, long *lColOffset)
{
   // Take number from the configuration file (which will
   // be in percent and translate it to a number of pixels
   // to search based on the clip format
   const CImageFormat *imageFormat = GetSrcImageFormat(0);

   *lRowOffset = ((double)configOffset/100.0)
                  * (double)(imageFormat->getLinesPerFrame());
   *lColOffset = ((double)configOffset/100.0)
                  * (double)(imageFormat->getPixelsPerLine());
}

void CDRSToolProc::CalculatePaddingPixels(int configPadding,
                                          long *lRowPadding, long *lColPadding)
{
   // Take number from the config file.  Should the amount of padding
   // be based on the number of pixels in the frame?? Anything else??
   const CImageFormat *imageFormat = GetSrcImageFormat(0);

   *lRowPadding = ((double)configPadding/100.0)
                   * (double)(imageFormat->getLinesPerFrame());
   *lColPadding = ((double)configPadding/100.0)
                   * (double)(imageFormat->getPixelsPerLine());
}

RECT CDRSToolProc::GetPaddedRect(const RECT &rect, int rowPadding,
                                 int colPadding)
{
   RECT newRect;
   newRect.top = rect.top - rowPadding;
   newRect.bottom = rect.bottom + rowPadding;
   newRect.left = rect.left - colPadding;
   newRect.right = rect.right + colPadding;

   const CImageFormat *imageFormat = GetSrcImageFormat(0);
   RECT activePictureRect = imageFormat->getActivePictureRect();

   // Clip the padded rectangle to the active picture region
   if (newRect.top < activePictureRect.top)
      newRect.top = activePictureRect.top;
   if (newRect.bottom > activePictureRect.bottom)
      newRect.bottom = activePictureRect.bottom;
   if (newRect.left <  activePictureRect.left)
      newRect.left = activePictureRect.left;
   if (newRect.right >  activePictureRect.right)
      newRect.right = activePictureRect.right;

   return (newRect);
}

int CDRSToolProc::BeginProcessing(SToolProcessingData &procData)
{
  // If this is a null fix, don't do anything!!
  if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
     return 0;

  int retVal;
  CRepairDRS &repairDRS = *drsParams.pRepairDRS;
  //currentPaddedRect = currentUserRect;

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL && iFrameCount > 1)
   {
     progressMonitor->StartProgress();
   }

  if (!drsParams.bReprocessingOverrides)
   {
    const CImageFormat *imageFormat = GetSrcImageFormat(0);

    // sets up write to debugging file...
    repairDRS.setCommandFileFlag(drsParams.commandFileFlag);

    // set the size of all the information to be sent to library
    // including the extra information needed for motion estimation
    RECT paddedRect = GetPaddedRect(drsParams.userRect, lRPadding, lCPadding);
    int totalFrameHeight = imageFormat->getTotalFrameHeight();
    int totalFrameWidth = imageFormat->getTotalFrameWidth();
    retVal = repairDRS.setImageDimension(
               paddedRect.top, paddedRect.left,
               paddedRect.bottom+1, paddedRect.right+1,
               totalFrameHeight, totalFrameWidth);
    if (retVal != 0)
      return retVal;    // ERROR

    retVal = repairDRS.setPixelComponents (imageFormat->getPixelComponents());
    if (retVal != 0)
      return retVal;

    // set ranges for the components (yuv, rgb) of the image.
    long laMinValue[MAX_COMPONENT_COUNT];
    long laMaxValue[MAX_COMPONENT_COUNT];
    long laFillValue[MAX_COMPONENT_COUNT];
    GetComponentMinMax(imageFormat, laMinValue, laMaxValue, laFillValue);
    long lNCom = imageFormat->getComponentCountExcAlpha();
    retVal = repairDRS.setImageComponent (lNCom, 0, laMinValue, laMaxValue,
                                         laFillValue);
    if (retVal != 0)
      return retVal;    // ERROR

    //currentPaddedRect = paddedRect;
   }

  // scale the border size to match the shape of the box
  long lMinDim, lBorder, lWidth, lHeight;
  lHeight = drsParams.userRect.bottom - drsParams.userRect.top;
  lWidth =  drsParams.userRect.right - drsParams.userRect.left;
  lMinDim = std::min<long>(lHeight, lWidth);
  lBorder = lMinDim * (drsParams.lBorderPercent / 100.0F);

   if (lBorder < drsParams.lBorderSize)
   {
      drsParams.lBorderSize = lBorder;
   }

   if (drsParams.lBorderMin > drsParams.lBorderSize)
   {
      drsParams.lBorderSize = drsParams.lBorderMin;
   }

   // + 1 for off by 1 err
   if (drsParams.lBorderSize > (lMinDim / 4 + 1))
   {
      drsParams.lBorderSize = lMinDim / 4 + 1;
   }

  // set all the override values (which may have been reset if not sticky)
  repairDRS.OverrideColorBrightness (drsParams.Overrides.iOverrideColorBrightness);
  repairDRS.OverrideColorSaturation (drsParams.Overrides.iOverrideColorSaturation);
  repairDRS.OverrideColorRed (drsParams.Overrides.iOverrideColorRed);
  repairDRS.OverrideColorGreen (drsParams.Overrides.iOverrideColorGreen);
  repairDRS.OverrideColorBlue (drsParams.Overrides.iOverrideColorBlue);
  repairDRS.OverrideGrainSize (drsParams.lGrainSize);
  repairDRS.OverrideGrainStrength (drsParams.Overrides.iOverrideGrainStrength);
  repairDRS.OverrideMoveOver (drsParams.Overrides.fOverrideMoveOverY, drsParams.Overrides.fOverrideMoveOverX);
  repairDRS.OverrideMoveUnder (drsParams.Overrides.fOverrideMoveUnderY, drsParams.Overrides.fOverrideMoveUnderX);
  repairDRS.OverrideRotateOver (drsParams.Overrides.fOverrideRotateOver);
  repairDRS.OverrideRotateUnder (drsParams.Overrides.fOverrideRotateUnder);
  repairDRS.OverrideTransparency (drsParams.Overrides.iOverrideTransparency);

  // set dimensions for the mask
  int iBorderSizeLocal = drsParams.lBorderSize + drsParams.Overrides.iOverrideBorderRadius;
   if (iBorderSizeLocal < 0)
   {
      iBorderSizeLocal = 0;
   }

  repairDRS.setBorderValues (iBorderSizeLocal, drsParams.lBlendType, drsParams.lSurroundRadius);
  repairDRS.setUseGrainPresets (drsParams.bUseGrainPresets);

  return 0;
}

int CDRSToolProc::BeginIteration(SToolProcessingData &procData)
{
   int inputFrameIndexList[MAX_FRAME];
   int releaseFrameIndexList[MAX_FRAME];
   int releaseFrameCount;
   int fixFrameIndex = iInFrameIndex + procData.iteration;

   // Figure out the frame indices of the frames we want to be the input
   // frames to DRS library functions
   if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
      {
      // No-op: all 5 input frames get released!
      int firstFrameIndex = fixFrameIndex -(MAX_FRAME/2);
      if (fixFrameIndex < 0)
         fixFrameIndex = 0;

      for (int i = 0; i < MAX_FRAME; ++i)
         {
         inputFrameIndexList[i] = releaseFrameIndexList[i] = firstFrameIndex + i;
         }
      iFrameToFix = -1;   // no fix!
      iNumProcessFrames = 5;
	   releaseFrameCount = 5;
	   }
   else if (drsParams.mainProcessType == DRS_PROCESS_HARD_MOTION)
      {
      // Single input frame
      inputFrameIndexList[0] = fixFrameIndex;
      iNumProcessFrames = 1;
      iFrameToFix = 0;
	   releaseFrameCount = 0;
	   }
   else if (drsParams.mainProcessType == DRS_PROCESS_COLOR_BUMP)
	   {
      // Color bump - 5 frames with frame-to-fix in the middle.
      iFrameToFix = 2;
      iNumProcessFrames = 5;
      releaseFrameCount = 0;

      for (int i = -2; i <= 2; ++i)
         {
         int frameIndex = fixFrameIndex + i;
         int listIndex = i + 2;
         inputFrameIndexList[listIndex] = frameIndex;

         // Release all but the fix frame.
         if (i != 0)
            {
            releaseFrameIndexList[releaseFrameCount++] = frameIndex;
            }
         }
	   }
   else // Low & High Motion
      {
      int j = 0;
      int frameIndex;

      switch (drsParams.subProcessType)
         {
         case DRS_SUBPROCESS_NONE :
            // Normal - 5 frames with frame-to-fix in the middle
            iFrameToFix = MAX_FRAME / 2;   // middle frame
            releaseFrameCount = 0;
            for (int i = -(MAX_FRAME/2); i <= (MAX_FRAME/2); ++i)
               {
               frameIndex = fixFrameIndex + i;
               inputFrameIndexList[j] = frameIndex;
               if (j != iFrameToFix)
                 {
                 releaseFrameIndexList[releaseFrameCount] = frameIndex;
                 ++releaseFrameCount;
                 }
               ++j;
               }
            iNumProcessFrames = j;
            break;
		   case DRS_SUBPROCESS_A :
            // Past-Only, 3 frames with frame-to-fix the last frame
            iFrameToFix = MAX_FRAME / 2; // last frame
            releaseFrameCount = 0;
            for (int i = -(MAX_FRAME/2); i <= 0; ++i)
               {
               frameIndex = fixFrameIndex + i;
               inputFrameIndexList[j] = frameIndex;
               if (j != iFrameToFix)
                 {
                 releaseFrameIndexList[releaseFrameCount] = frameIndex;
                 ++releaseFrameCount;
                 }
               ++j;
               }
            iNumProcessFrames = j;
            break;
         case DRS_SUBPROCESS_B :
            // Future-only, 3 frames with frame-to-fix the first frame
            iFrameToFix = 0;       // first frame
            releaseFrameCount = 0;
            for (int i = 0; i <= (MAX_FRAME/2); ++i)
               {
               frameIndex = fixFrameIndex + i;
               inputFrameIndexList[j] = frameIndex;
               if (j != iFrameToFix)
                  {
                  releaseFrameIndexList[releaseFrameCount] = frameIndex;
                  ++releaseFrameCount;
                  }
               ++j;
               }
            iNumProcessFrames = j;
            break;
		   case DRS_SUBPROCESS_C :
            // Import frames
            inputFrameIndexList[1] = fixFrameIndex;
            iFrameToFix = 1;

#if IMPORT_BOTH_IN_AND_OUT_FRAMES

            if (drsParams.markInIndex < 0 && drsParams.markOutIndex < 0)
               return -1;  // ERROR: both mark in and mark out are missing
            iNumProcessFrames = 2;
				releaseFrameCount = 0;

				// There's a bug in the motion calculation where it messes up if
				// the 'in' and the 'out' are the same frame, so since it doesn't make
				// any visible difference, pretend that only the 'in' frame was set.
				if (drsParams.markInIndex >= 0)
					{
               // At least mark in is available
					inputFrameIndexList[0] = drsParams.markInIndex;
					if (drsParams.markOutIndex >= 0 && drsParams.markInIndex != drsParams.markOutIndex)
						{
                  // Mark out is also available
                  inputFrameIndexList[2] = drsParams.markOutIndex;
                  iNumProcessFrames = 3;   // Both mark in and out are available
                  }
               }
            else
               {
               // Only mark out is available
               inputFrameIndexList[0] = drsParams.markOutIndex;
               }

#else // IMPORT ONLY MARK IN FRAME

            if (drsParams.markInIndex < 0)
               return -1;  // ERROR: mark in is missing
            iNumProcessFrames = 2;
            releaseFrameCount = 0;
            inputFrameIndexList[0] = drsParams.markInIndex;

#endif

   			break;
         }
	    }

   // inputFrameIndexList now contains the frame indices we want as input
   SetInputFrames(0, iNumProcessFrames, inputFrameIndexList);

   // These are the frames that will be the output
   if (fixFrameIndex == -1)
      {
      // No-op - there is no output!
      SetOutputFrames(0, 0, NULL);
      }
   else
      {
      int outputFrameIndexList[1];
      outputFrameIndexList[0] = fixFrameIndex;
      SetOutputFrames(0, 1, outputFrameIndexList);
      }

   // Frames to release
   if (releaseFrameCount > 0)
      SetFramesToRelease(0, releaseFrameCount, releaseFrameIndexList);


  // OVERRIDE MOTION on AUTO

   if (drsParams.mainProcessType == DRS_PROCESS_COLOR_BUMP)
   {
      // Create a valid frame mask

      auto &cutList = drsParams.cutList;
      auto cutIndex = -1;
      for (size_t i = 0; i < cutList.size()-1; i++)
      {
         if ((cutList[i] <= fixFrameIndex) && (cutList[i+1] > fixFrameIndex))
         {
            cutIndex = i;
            break;
         }
      }

      MTIassert(cutIndex != -1);
      if (cutIndex == -1) cutIndex = 0;

      // Now see if frames exist
		_validFrameMask = 0;
      auto validBit = 1;
      for (auto i=-2; i <= 2; i++)
      {
         auto testIndex = fixFrameIndex + i;
         if ((cutList[cutIndex] <= testIndex) && (cutList[cutIndex+1] > testIndex))
         {
            _validFrameMask |= validBit;
         }

         validBit *= 2;
      }
   }

   return 0;
}

int CDRSToolProc::GetIterationCount(SToolProcessingData &procData)
{
   return iFrameCount;
}

int CDRSToolProc::GetComponentMinMax(const CImageFormat *imageFormat,
                                     long laMinValue[], long laMaxValue[],
                                     long laFillValue[])
{
   MTI_UINT16 componentValues[MAX_COMPONENT_COUNT];

   // Get the number of components in the image
   int componentCount = imageFormat->getComponentCountExcAlpha();
   int componentIndex;

   // Get the component minimum, maximum and fill values from the
   // image format and then copy them to the caller's buffers

   imageFormat->getComponentValueMin(componentValues);
   for (componentIndex = 0; componentIndex < componentCount; ++componentIndex)
      laMinValue[componentIndex] = (long)componentValues[componentIndex];

   imageFormat->getComponentValueMax(componentValues);
   for (componentIndex = 0; componentIndex < componentCount; ++componentIndex)
      laMaxValue[componentIndex] = (long)componentValues[componentIndex];

   imageFormat->getComponentValueFill(componentValues);
   for (componentIndex = 0; componentIndex < componentCount; ++componentIndex)
      laFillValue[componentIndex] = (long)componentValues[componentIndex];

   return 0;
}

int CDRSToolProc::DoProcess(SToolProcessingData &procData)
{
	int retVal = 0;
	MTI_UINT16 *visibleFrameBuffer;
	const CImageFormat *imageFormat = GetSrcImageFormat(0);
	CRepairDRS &repairDRS = *drsParams.pRepairDRS;

	// If this is a null fix, don't do anything!!
	if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
	{
		return retVal;
	}

	try
	{
		if (!drsParams.bReprocessingOverrides)
		{
			retVal = repairDRS.setNFrame(iNumProcessFrames);
			if (retVal != 0)
			{
				throw retVal;
			}

			// iFrameToFix is relative to the group of images passed to the
			// library NOT to the clip.  The range is typically 0 to MAX_FRAME
			retVal = repairDRS.setRepairFrame(iFrameToFix);
			if (retVal != 0)
			{
				throw retVal;
			}

			bool isImageDataReallyHalfFloat = false;

			// Get the several input frames and hand them to DRS library
			for (int i = 0; i < iNumProcessFrames; ++i)
			{
				CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, i);
				retVal = inToolFrameBuffer->GetErrorFlag();
				if (retVal != 0 && (retVal != CLIP_ERROR_INVALID_FRAME_NUMBER || i == iFrameToFix))
				{
					throw retVal;
				}

				if (i == 0)
				{
					isImageDataReallyHalfFloat = inToolFrameBuffer->ContainsFloatData();
				}

				visibleFrameBuffer = inToolFrameBuffer->GetVisibleFrameBufferPtr();
				int frameIndex = inToolFrameBuffer->GetFrameIndex();
				retVal = repairDRS.setDataPointer(i, visibleFrameBuffer, frameIndex);
				if (retVal != 0)
				{
					throw retVal;
				}
			}

			// Set hint to fixer that the 16-bit data in the buffer are really
			// half floats, not unsigned shorts.
			repairDRS.setDataTypeIsReallyHalfFloat(isImageDataReallyHalfFloat);

			// Tell the DRS library about the output buffer, which is the input buffer
			// that will be modified in place
			CToolFrameBuffer *targetFrameBuffer = GetRequestedInputFrame(0, iFrameToFix);
			visibleFrameBuffer = targetFrameBuffer->GetVisibleFrameBufferPtr();
			retVal = repairDRS.setResultPointer(visibleFrameBuffer);
			if (retVal != 0)
			{
				throw retVal;
			}

			// Tell the GrainGenerator about the new source data.

			const float *grainPresetValues = nullptr;
			if (_grainPresets != nullptr && _grainPresets->IsEnabled())
			{
				int frameIndex = targetFrameBuffer->GetFrameIndex();
				grainPresetValues = _grainPresets->GetNormalizedGrainValues(visibleFrameBuffer, frameIndex);
			}

			repairDRS.setGrainPresetValues(grainPresetValues);

			// Set DRS specific data

			retVal = repairDRS.setMotionAcceleration(lRAccel, lCAccel);
			if (retVal != 0)
			{
				throw retVal;
			}

			retVal = repairDRS.setMotionOffset(lRMotionOffset, lCMotionOffset);
			if (retVal != 0)
			{
				throw retVal;
			}

			repairDRS.setFieldRepeatFlag(drsParams.bFieldRepeat);

			// Remember the original pixels in the region we're about to fix
			// (This may eventually be in the DRS library function)
			// Save the pixels in the region to be modified.  Should eventually
			// be generalized to a mask
			CPixelRegionList *originalValues = (iFixType != RT_COLOR_BUMP) ? targetFrameBuffer->GetOriginalValuesPixelRegionListPtr() : NULL;
			int totalFrameWidth = imageFormat->getTotalFrameWidth();
			int totalFrameHeight = imageFormat->getTotalFrameHeight();
			int pxlComponentCount = imageFormat->getComponentCountExcAlpha();

			// Need to enlarge the original values area if there are moveOver or
			// rotateOver overrides
			if (drsParams.Overrides.fOverrideMoveOverX != 0.F || drsParams.Overrides.fOverrideMoveOverY != 0.F ||
					drsParams.Overrides.fOverrideRotateOver != 0.F)
			{
				RECT bounds = drsParams.userRect;

				// Determine the bounding rectangle of the selection so we can pad it
				if (drsParams.useLasso)
				{
					CLassoPixelRegion *pxlRgn = new CLassoPixelRegion(drsParams.userLassoPoints, visibleFrameBuffer, totalFrameWidth,
							totalFrameHeight, pxlComponentCount);
					bounds = pxlRgn->GetRect();
					delete pxlRgn;
				}
				else if (drsParams.userBezierPointer != 0)
				{
					CBezierPixelRegion* pxlRgn = new CBezierPixelRegion(drsParams.userBezierPointer, visibleFrameBuffer, totalFrameWidth,
							totalFrameHeight, pxlComponentCount);
					bounds = pxlRgn->GetRect();
					delete pxlRgn;
				}

				// Grab the original pixels from the padded rectangle
				RECT paddedRect = GetPaddedRect(bounds, lRPadding, lCPadding);

				if (originalValues != NULL)
				{
					originalValues->Add(paddedRect, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
				}

				// eventually, we will always pass in a valid pointer.  The DRS
				// algorithm handles MASK entries
				if (*drsParams.pCurrentFixRegionSerial == drsParams.myFixRegionSerial)
				{
					*drsParams.pWhereToCopyTheFixRegionListTo = new CPixelRegionList;
					repairDRS.setPixelRegionList(*drsParams.pWhereToCopyTheFixRegionListTo, drsParams.pCurrentFixRegionSerial,
							drsParams.myFixRegionSerial);
				}
				else
				{
					repairDRS.setPixelRegionList(0);
				}
			}
			else if (drsParams.useLasso)
			{

				if (originalValues != NULL)
				{
					// Save original values within user's lasso
					originalValues->Add(drsParams.userLassoPoints, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
				}

				// eventually, we will always pass in a valid pointer.  The DRS
				// algorithm handles MASK entries
				repairDRS.setPixelRegionList(0);
			}
			else if (drsParams.userBezierPointer != 0)
			{

				if (originalValues != NULL)
				{
					// Save original values withing user's bezier
					originalValues->Add(drsParams.userBezierPointer, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
				}

				// eventually, we will always pass in a valid pointer.  The DRS
				// algorithm handles MASK entries
				repairDRS.setPixelRegionList(0);
			}
			else
			{

				if (originalValues != NULL)
				{
					// Save original values within user's rectangle
					originalValues->Add(drsParams.userRect, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
				}

				// eventually, we will always pass in a valid pointer.  The DRS
				// algorithm handles MASK entries
				repairDRS.setPixelRegionList(0);
			}

			// set up the masks
			if (drsParams.useLasso)
			{
				retVal = repairDRS.setSelectRegion(drsParams.userLassoPoints);
			}
			else if (drsParams.userBezierPointer != 0)
			{
				retVal = repairDRS.setSelectRegion(drsParams.userBezierPointer);
			}
			else
			{
				retVal = repairDRS.setSelectRegion(drsParams.userRect);
			}
			if (retVal != 0)
			{
				throw retVal;
			}

			// set up color bump crap
			repairDRS.setColorBumpCrap(bIsMonochrome, iDirection, _validFrameMask);

			// Finally, do the fix!
			retVal = repairDRS.PerformFix(iFixType);
			if (retVal != 0)
			{
				throw retVal;
			}

		}
		else // we are reprocessing with new overrides
		{
			bool stupidOverrideHackFlag = false;
			// Tell the DRS library about the output buffer, which is the input buffer
			// the will be modified in place
			CToolFrameBuffer *targetFrameBuffer = GetRequestedInputFrame(0, iFrameToFix);
			visibleFrameBuffer = targetFrameBuffer->GetVisibleFrameBufferPtr();
			retVal = repairDRS.setResultPointer(visibleFrameBuffer);
			if (retVal != 0)
			{
				throw retVal;
			}

			// This may not have been set yet if we ran originally with use presets off.
			const float *grainPresetValues = nullptr;
			if (_grainPresets != nullptr && _grainPresets->IsEnabled())
			{
				int frameIndex = targetFrameBuffer->GetFrameIndex();
				grainPresetValues = _grainPresets->GetNormalizedGrainValues(visibleFrameBuffer, frameIndex);
			}

			repairDRS.setGrainPresetValues(grainPresetValues);

			// Remember the original pixels in the region we're about to fix
			CPixelRegionList *originalValues = (iFixType != RT_COLOR_BUMP) ? targetFrameBuffer->GetOriginalValuesPixelRegionListPtr() : NULL;
			if (originalValues != NULL)
			{
				originalValues->Clear();
			}

			// moving OVER will change the RECT or LASSO, so we need to use a
			// MASK to hold original values
			// EVENTUALLY, the MASK Pixel Region List will determine shapes and
			// we will always let DRS library save the mask to PixelRegionList
			if (drsParams.Overrides.fOverrideMoveOverX != 0.F || drsParams.Overrides.fOverrideMoveOverY != 0.F ||
					drsParams.Overrides.fOverrideRotateOver != 0.F)
			{
				// Remember the original pixels in the region we're about to fix
				repairDRS.setPixelRegionList(originalValues, drsParams.pCurrentFixRegionSerial, drsParams.myFixRegionSerial);
				stupidOverrideHackFlag = true;
			}
			else
			{
				if (originalValues != NULL)
				{
					// Remember the original pixels in the region we're about to fix
					// (This may eventually be in the DRS library function)
					// Save the pixels in the region to be modified.  Should eventually
					// be generalized to a mask
					int totalFrameWidth = imageFormat->getTotalFrameWidth();
					int totalFrameHeight = imageFormat->getTotalFrameHeight();
					int pxlComponentCount = imageFormat->getComponentCountExcAlpha();
					if (drsParams.useLasso)
					{
						// Save original values within user's lasso
						originalValues->Add(drsParams.userLassoPoints, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
					}
					else if (drsParams.userBezierPointer != 0)
					{
						// Save original values with user's bezier
						originalValues->Add(drsParams.userBezierPointer, visibleFrameBuffer, totalFrameWidth, totalFrameHeight,
						pxlComponentCount);
					}
					else
					{
						// Save original values within user's rectangle
						originalValues->Add(drsParams.userRect, visibleFrameBuffer, totalFrameWidth, totalFrameHeight, pxlComponentCount);
					}
				}

				// eventually, we will always pass in a valid pointer.  The DRS
				// algorithm handles MASK entries
				repairDRS.setPixelRegionList(0);
			}

			if (retVal != 0)
			{
				throw retVal;
			}

			// perform the override
			repairDRS.OverrideUpdate();

			if (stupidOverrideHackFlag == true && originalValues != NULL)
			{
				if (*drsParams.pCurrentFixRegionSerial == drsParams.myFixRegionSerial)
				{
					*drsParams.pWhereToCopyTheFixRegionListTo = new CPixelRegionList(*originalValues);
				}
			}
		}

		return 0;
	}
	catch (int)
	{; // do nothing here;
	}
	catch (std::bad_alloc&)
	{
		CAutoErrorReporter autoErr("CDRSToolProc::DoProcess", AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.errorCode = retVal;
		autoErr.msg << "OUT OF MEMORY - bad_alloc";
	}
	catch (const std::exception & e)
	{
		CAutoErrorReporter autoErr("CDRSToolProc::DoProcess", AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.errorCode = retVal;
		autoErr.msg << "INTERNAL ERROR - caught exception ( " << e.what() << ")";
	}

	catch (...)
	{
		if (retVal == 0)
		{
			retVal = 666;
		} // We have absolutely no idea what this is
	}
	try
	{
		CAutoErrorReporter autoErr("CDRSToolProc::DoProcess", AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.errorCode = retVal;
		if (retVal == ERR_ALLOC)
		{
			autoErr.msg << "OUT OF MEMORY - cannot continue DRS processing" << endl <<
					"Please select a smaller region of the image and try again";
		}
		else
		{
			autoErr.msg << "Repair failed - unable to process DRS request";
		}
	}
	catch (...)
	{
		TRACE_0(errout << "CDRSToolProc::DoProcess: FATAL ERROR, code " << retVal << endl <<
				"CAUGHT EXCEPTION when trying to display error dialog!");
	}
	return retVal; // ERROR
}

int CDRSToolProc::EndIteration(SToolProcessingData &procData)
{
  // If this is a null fix, don't do anything!!
  if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
	  return 0;

	// Always auto accept each fix in multiframe mode
	if (iFrameCount > 1)
	{
		GetSystemAPI()->AcceptProvisional(false);

		drsParams.pRepairDRS->ReleaseMemory();
	}

	// Update the status
	IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
	if (progressMonitor != NULL && iFrameCount > 1)
	{
	  progressMonitor->BumpProgress();
	}

	return 0;
}

int CDRSToolProc::EndProcessing(SToolProcessingData &procData)
{
  // If this is a null fix, don't do anything!!
  if (drsParams.mainProcessType == DRS_PROCESS_NOOP)
     return 0;

   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
      int iterCount = GetIterationCount(procData);
      progressMonitor->SetStatusMessage(
                        (iFixType == RT_NONE)? "Initialization complete" :
                             ((iterCount > 1)? "Fixes complete" :
                                               "Fix pending"));
      progressMonitor->StopProgress(procData.iteration == iterCount);
   }

   return 0;
}

///////////////////////////////////////////////////////////////////////////////
