#include "ColorBumpUtil.h"
#include <vector>
#include "IniFile.h"
#include "mthread.h"
#include "SysInfo.h"
#include "IppArray.h"
#include <cmath>
#include <cfloat>
#include <vector>
#include <numeric>

#define MAX_OFFSET_FACTOR 0.15F

namespace
{

MTHREAD_STRUCT BM_mthreadStruct;
bool BM_mthreadstruct_was_initialized = false;

struct BM_threadParams_struct
{
   Ipp32fArray y0;
   Ipp32fArray y1;
   Ipp32fArray y2;
   BMStruct *BM;
};


//*******************************************************************
void MatchOneBlock(int i, int j, const Ipp32fArray& y0, const Ipp32fArray& y1, const Ipp32fArray& y2, BMStruct& BM)
{
   auto NumBlockV = BM.nRows;
   auto NumBlockH = BM.nCols;
   auto nRow = y0.getRoi().height;
   auto nCol = y0.getRoi().width;
	const long MaxOffset = my_lround(MAX_OFFSET_FACTOR * nCol);

   // % the upper-left-corner of the block
   // row0 = round((i-1)/NUM_BLOCK_V * nRow) + 1;
   // col0 = round((j-1)/NUM_BLOCK_H * nCol) + 1;
   auto row0 = my_lround((i / float(NumBlockV)) * nRow);
   auto col0 = my_lround((j / float(NumBlockH)) * nCol);

   // % the lower-right-corner of the block
   // row1 = round(i/NUM_BLOCK_V * nRow);
   // col1 = round(j/NUM_BLOCK_H * nCol);
   auto row1 = std::min<long>(y0.getRoi().height, my_lround(((i + 1) / float(NumBlockV)) * nRow) - 1);
   auto col1 = std::min<long>(y0.getRoi().width, my_lround(((j + 1) / float(NumBlockH)) * nCol) - 1);

   // BM.row0(i,j) = row0;
   // BM.row1(i,j) = row1;
   // BM.col0(i,j) = col0;
   // BM.col1(i,j) = col1;
   BM.row0[i][j] = row0;
   BM.row1[i][j] = row1;
   BM.col0[i][j] = col0;
   BM.col1[i][j] = col1;

   // rows = 0:(row1-row0);
   // cols = 0:(col1-col0);
   // Don't need these.

   // % the block in y0
   // blk0 = y0(row0+rows, col0+cols);
   IppiRect roi;
   roi.x = col0;
   roi.y = row0;
   roi.width = (col1 - col0) + 1;
   roi.height = (row1 - row0) + 1;
   Ipp32fArray blk0 = y0(roi);

   // % the allowed offsets for the block
   // row_min_off = -MAX_OFFSET;
   // row_max_off = MAX_OFFSET;
   // col_min_off = -MAX_OFFSET;
   // col_max_off = MAX_OFFSET;

   // % the offset from y0 to y2 is POSITIVE
   // %
   // % make certain the offsets do not allow the block fall outside
   // % of y2
   // if (row0+row_min_off) < 1,
   //     row_min_off = 1-row0;
   // end
   auto rowMinOff = (row0 - MaxOffset) < 0 ? -row0 : -MaxOffset;

   // if (col0+col_min_off) < 1,
   //     col_min_off = 1-col0;
   // end
   auto colMinOff = (col0 - MaxOffset) < 0 ? -col0 : -MaxOffset;

   // if (row1+row_max_off) > nRow,
   //     row_max_off = nRow-row1;
   // end
   auto rowMaxOff = (row1 + MaxOffset) >= nRow ? nRow - row1 - 1 : MaxOffset;

   // if (col1+col_max_off) > nCol,
   //     col_max_off = nCol-col1;
   // end
   auto colMaxOff = (col1 + MaxOffset) >= nCol ? nCol - col1 - 1 : MaxOffset;

   // best_diff = 1000000;
   Ipp64f bestDiff = 1000000.0;
   auto bestRowOff1 = 0;
   auto bestColOff1 = 0;
   auto bestRowOff2 = 0;
   auto bestColOff2 = 0;

   MTIassert(y0.getComponents() == 1);
   Ipp64f l1NormDiff01[1];
   Ipp64f l1NormDiff12[1];

   // %  step through offsets in y2
   // 	for ii2 = row_min_off:row_max_off,
   // 		for jj2 = col_min_off : col_max_off,
   for (auto ii2 = rowMinOff; ii2 <= rowMaxOff; ++ii2)
   {
      for (auto jj2 = colMinOff; jj2 <= colMaxOff; ++jj2)
      {
         // % the offset in y1 is half the offset to y2
         // ii1 = round(ii2/2);
         // jj1 = round(jj2/2);
         auto ii1 = (ii2 + 1) / 2;
         auto jj1 = (jj2 + 1) / 2;

         // % the block in y1
         // blk1 = y1(row0+ii1+rows, col0+jj1+cols);
         roi.x = col0 + jj1;
         roi.y = row0 + ii1;
		 auto blk1 = y1(roi);

         // % the block in y2
         // blk2 = y2(row0+ii2+rows, col0+jj2+cols);
         roi.x = col0 + jj2;
         roi.y = row0 + ii2;
		 auto blk2 = y2(roi);

         // diff01 = blk1 - blk0;
         // diff12 = blk2 - blk1;
         ////auto diff01 = blk1 - blk0;
         ////auto diff12 = blk2 - blk1;
         blk1.L1NormDiff(blk0, l1NormDiff01);
         blk2.L1NormDiff(blk1, l1NormDiff12);
         auto sumDiff01 = l1NormDiff01[0];
         auto sumDiff12 = l1NormDiff12[0];

         ////% since blk0 is corrupted, give it small weight.
         ////diff = .2*diff01 + .8*diff12;
         ////auto diff = (0.2F * diff01) + (0.8F * diff12);

         // sum_diff = sum(abs(diff(:)));
         ////auto sumDiff = diff.L1Norm();
         auto sumDiff = (0.2F * sumDiff01) + (0.8F * sumDiff12);

         // if sum_diff < best_diff,
         if (sumDiff < bestDiff)
         {
            // best_diff = sum_diff;
            // best_row_off1 = ii1;
            // best_col_off1 = jj1;
            // best_row_off2 = ii2;
            // best_col_off2 = jj2;
            bestDiff = sumDiff;
            bestRowOff1 = ii1;
            bestColOff1 = jj1;
            bestRowOff2 = ii2;
            bestColOff2 = jj2;
         // end
         }
      // end
      }
   // end
   }

   //	BM.row_off(i,j) = best_row_off1;
   //	BM.col_off(i,j) = best_col_off1;
   BM.rowOff[i][j] = bestRowOff1;
   BM.colOff[i][j] = bestColOff1;

   //	% the score is based on the match to y0
   //	blk0 = y0(row0+rows, col0+cols);
   //	blk1 = y1(row0+best_row_off1+rows, col0+best_col_off1+cols);
   //	blk2 = y2(row0+best_row_off2+rows, col0+best_col_off2+cols);
   //	--- blk0 doesn't need to chang

   roi.x = col0 + bestColOff1;
   roi.y = row0 + bestRowOff1;
   auto blk1 = y1(roi);

   roi.x = col0 + bestColOff2;
   roi.y = row0 + bestRowOff2;
   auto blk2 = y2(roi);

   //	diff = blk0 - .5*(blk1+blk2);
   auto diff = blk0 - ((blk1 + blk2) * 0.5F);

   //	% final score is the average absolute difference
   //	% between the block extrapolated to y0 and the blocks
   //	% in y1 and y2
   //	BM.score(i,j) = sum(abs(diff(:)))/numel(diff);
   auto l1Norm = diff.L1Norm();
   auto l1NormTotal = accumulate(l1Norm.begin(), l1Norm.end(), 0.0);
   BM.score[i][j] = Ipp32f(l1NormTotal / diff.volume());
}

//*******************************************************************
void BM_thread_trampoline(void *vp, int iJob)
{
   BM_threadParams_struct *BM_threadParams = (BM_threadParams_struct *)vp;
   int i = iJob / (BM_threadParams->BM->nCols);
   int j = iJob % (BM_threadParams->BM->nCols);
   MatchOneBlock(i, j, BM_threadParams->y0, BM_threadParams->y1, BM_threadParams->y2, *BM_threadParams->BM);
}

//*******************************************************************
bool checkForInfiniteOrNan(const Ipp32fArray &a)
{
   for (auto row = 0; row < a.getRoi().height; ++row)
   {
      for (auto col = 0; col < a.getRoi().width; ++col)
      {
         for (auto comp = 0; comp < a.getComponents(); ++comp)
         {
            auto element = a[{col, row, comp}];
            if (_isnan(element) || std::_isinf(element))
            {
               return true;
            }
         }
      }
   }

   return false;
}

//*******************************************************************
void SortMatrixColumn(MklMatrix32f &m, int col)
{
   if (m.cols() <= col)
   {
      throw std::exception("Bad column number passed to SortMatrixColumn!");
   }

   std::vector<float> v(m.rows());
   for (auto i = 0; i < m.rows(); ++i)
   {
      v[i] = m[i][col];
   }

   std::sort(v.begin(), v.end());

   for (auto i = 0; i < m.rows(); ++i)
   {
      m[i][col] = v[i];
   }
}

//*******************************************************************
// function Value = GreenFunction (Xi, Xj)
// % Green's function for tps solutions
float GreenFunction(const MklVector32f &Xi, const MklVector32f &Xj)
{
   if (Xi.length() != Xj.length())
   {
      throw std::exception("Bad arguments passed to GreenFunction()");
   }

   auto retVal = 0.0F;

   float dist;
   if (Xi.length() == 2)
   {
      // % Euclidean distance between points
      // delta = Xi - Xj;
      // dist = sqrt(delta*delta');
      auto delta1 = Xi[0] - Xj[0];
      auto delta2 = Xi[1] - Xj[1];
      auto norm2 = (delta1 * delta1) + (delta2 * delta2);
      ippsSqrt_32f(&norm2, &dist, 1);
   }
   else
   {
      // % Euclidean distance between points
      // delta = Xi - Xj;
      // dist = sqrt(delta*delta');
      auto delta = Xi - Xj;
      dist = delta.euclideanNorm();
   }
   ////TRACE_3(errout << "delta: " << endl << delta);

   // if dist <= 0,
   //    Value = 0;
   // else
   //    Value = dist * dist * log(dist);
   // end
   if (dist > 0)
   {
      retVal = dist * dist * log(dist);
   }

   ////TRACE_3(errout << "temp=" << temp[0][0] << " dist=" << dist << " retVal=" << retVal);

// end
   return retVal;
}

//*******************************************************************
// function Yhat=tps(X,Y,p,Xhat)
// %
// %  Use a Thin Plate Spline to fit a surface.  Usage:
// %
// %      Yhat = tps (X, Y, p, Xhat)
// %
// %  X is a nx2 list of locations in the plane, normally a list of (row,col)
// %  Y is the nx1 list of observed values at each location
// %  p is the smoothing paramter:  p=0 maximal smoothing (solution is a plane)
// %                                p=1 minimal smoothing
// %  Xhat is the nhatx2 list of locations where values are interpolated
// %  Yhat is the nhatx1 list of interpolated values
void tps(const MklMatrix32f &X, const MklMatrix32f &Y, float p, const MklMatrix32f &Xhat, MklMatrix32f &Yhat)
{
   TRACE_3(errout << "ENTER function tps");
   //if nargin < 4 || nargout > 1
   //    fprintf ('Error:  Illegal number of arguments\n');
   //    return;
   //end
   //
   //if size(X,1) <= 0 || size(X,2) ~= 2,
   //    fprintf ('Error:  Illegal dimension for X argument\n');
   //    return;
   //end
   if (X.rows() < 1 || X.cols() != 2)
   {
      throw std::exception("tps: Illegal dimension for X argument");
   }

   //if size(Y,1) ~= size(X,1) || size(Y,2) ~= 1,
   //    fprintf ('Error:  Illegal dimension for Y argument\n');
   //    return;
   //end
   if (Y.rows() != X.rows() || Y.cols() != 1)
   {
      throw std::exception("tps: Illegal dimension for Y argument");
   }

   //if p < 0 || p > 1,
   //    fprintf ('Error:  Illegal value for p argument\n');
   //    return;
   //end
   if (p < 0.0F || p > 1.0F)
   {
      throw std::exception("tps: Illegal dimension for p argument");
   }

   //if size(Xhat,1) <= 0 || size(Xhat,2) ~= 2,
   //    fprintf ('Error:  Illegal valu for Xhat argument\n');
   //    return;
   //end
   if (Xhat.rows() < 1 || Xhat.cols() != 2)
   {
      throw std::exception("tps: Illegal dimension for Xhat argument");
   }

   //n = size(X,1);
   //nhat = size(Xhat,1);
   auto n = X.rows();
   auto nhat = Xhat.rows();

   //N = [ones(n, 1) X];
   // It's easier to fill rows and transpose than to fill columns!
   MklMatrix32f temp1(1 + X.cols(), n);
   temp1[0].fill(1.0F);
   auto X_t = X.transpose();
   for (auto i = 0; i < X.cols(); ++i)
   {
      temp1[1 + i].copyDataFrom(X_t[i]);
   }

   auto N = temp1.transpose();

   ////TRACE_3(errout << "N: " << endl << N);

   //Nhat = [ones(nhat, 1) Xhat];
   // It's easier to fill rows and transpose than to fill columns!
   MklMatrix32f temp2(1 + Xhat.cols(), nhat);
   temp2[0].fill(1.0F);
   auto Xhat_t = Xhat.transpose();
   for (auto i = 0; i < Xhat.cols(); ++i)
   {
      temp2[1 + i].copyDataFrom(Xhat_t[i]);
   }

   auto Nhat = temp2.transpose();

   ////TRACE_3(errout << "Nhat: " << endl << Nhat);

   //if p == 0 %infinite regularization (no curvature allowed), solution is a regression plane
   MklMatrix32f Beta(n, 1);
   MklMatrix32f Alpha(n, 1);
   if (p == 0.0F)
   {
      // %Beta = inv(N) * y;
      // Beta = N \ y;
      Beta = N.mldivide(Y);

      // Alpha = zeros(n, 1);
      Alpha.zero();
   }
   //else
   else
   {
      // %coefficient matrices
      // %need pairwise distances between points
      // M = zeros(n);
      MklMatrix32f M(n, n);
      M.zero();

      // %  Note:  M is symmetric since distance from X(i,:) to X(j,:)
      // %  is the same as from X(j,:) to X(i,:).
      // %  Also, M is zero on the diagonal since distance from X(i,:)
      // %  to X(i,:) is zero.
      // for i = 1:(n-1)
      //     for j = i+1:n,
      for (auto i = 0; i < (n - 1); ++i)
      {
         for (auto j = i + 1; j < n; ++j)
         {
            // val = GreenFunction (X(i,:), X(j,:));
            auto val = GreenFunction(X[i], X[j]);

            // M(i,j) = val;
            // M(j,i) = val;
            M[i][j] = val;
            M[j][i] = val;

         // end
         }
      // end
      }

      TRACE_3(errout << "AFTER many GreenFunction()s");
      ////TRACE_3(errout << "M: " << endl << M);

      // % convert p to lambda
      // lambda = (1-p) / p;
      auto lambda = (1.0F - p) / p;

      // % Add regularization term to the diagonal
      // M = M + lambda*eye(n);
      MklMatrix32f eye(n, n);
      eye.ident();
      M = M + (lambda * eye);

      ////TRACE_3(errout << "M + lambda: " << endl << M);

      // M_inv = pinv(M);
      TRACE_3(errout << "BEFORE invertMatrix(M)");
      auto M_inv = M.invert();
      TRACE_3(errout << "AFTER invertMatrix(M)");

      ////TRACE_3(errout << endl << "M_inv: " << M_inv);

      // Beta = pinv(N' * M_inv * N) * N' * M_inv * Y;
      auto N_t = N.transpose();
      auto temp3 = N_t * M_inv * N;
      TRACE_3(errout << "BEFORE invertMatrix(temp3)");
      Beta = temp3.invert() * N_t * M_inv * Y;
      TRACE_3(errout << "AFTER invertMatrix(temp3)");

      // Alpha = M_inv * (Y - N*Beta);
      Alpha = M_inv * (Y - (N * Beta));

   //end
   }

   ////TRACE_3(errout << "Alpha=" << endl << Alpha);
   ////TRACE_3(errout << "Beta=" << endl << Beta);

   // %Compute the thin plate spline values at Xhat
   // Yhat = Nhat * Beta;
   Yhat = Nhat * Beta;

   ////TRACE_3(errout << "Yhat BEFORE =" << endl << Yhat);

   // for i = 1:nhat
      // for j = 1:n
   for (auto i = 0; i < nhat; ++i)
   {
      for (auto j = 0; j < n; ++j)
      {
         // Yhat(i) = Yhat(i) + Alpha(j)*GreenFunction(X(j,:), Xhat(i,:));
         Yhat[i][0] = Yhat[i][0] + (Alpha[j][0] * GreenFunction(X[j], Xhat[i]));
      // end
      }
   //end
   }
//end

   ////TRACE_3(errout << "Yhat AFTER =" << endl << Yhat);
   TRACE_3(errout << "EXIT function tps");
}

} // anonymous namespace

//*******************************************************************
// namespace ColorBumpUtil
//*******************************************************************
// function BM = BlockMatch (src0, src1, src2)
// %  The block matching motion estimates the motion from src0 to src1 to
// %  src2.
// %
// %  When correcting light leaks and other color bumps, src0 is corrupted
// %  and cannot be used directly to estimate motion.  For every motion
// %  offset, we emphasize the data values in src1 and src2
BMStruct* ColorBumpUtil::BlockMatch(const Ipp32fArray& src0, const Ipp32fArray& src1, const Ipp32fArray& src2)
{
	// nRow = size(src0,1);
	// nCol = size(src0,2);
	// nChan = size(src0,3);
	auto nRow = src0.getRoi().height;
	auto nCol = src0.getRoi().width;
	auto nChan = src0.getComponents();

	// MAX_OFFSET = round(.15*nCol);
	// NUM_BLOCK_H = 25;
	// NUM_BLOCK_V = round(nRow*NUM_BLOCK_H/nCol);
	const long NumBlockH = 25;
	const long NumBlockV = std::max<long>(1, my_lround(NumBlockH * (nRow / float(nCol))));

	//%  Extract the y signal
	//if nChan == 3,
	//% convert from RGB
	// y0 = squeeze(.2*src0(:,:,1) + .7*src0(:,:,2) + .1*src0(:,:,3));
	// y1 = squeeze(.2*src1(:,:,1) + .7*src1(:,:,2) + .1*src1(:,:,3));
	// y2 = squeeze(.2*src2(:,:,1) + .7*src2(:,:,2) + .1*src2(:,:,3));
	//else
	//% already black and white
	// y0 = src0;
	// y1 = src1;
	// y2 = src2;
	//end
   auto y0 = src0;
	auto y1 = src1;
	auto y2 = src2;
   if (nChan == 3)
   {
      y0 = ExtractYSignal(src0, false);
      y1 = ExtractYSignal(src1, false);
      y2 = ExtractYSignal(src2, false);
   }

	// BM.row0 = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.row1 = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.col0 = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.col1 = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.row_off = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.col_off = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	// BM.diff = zeros(NUM_BLOCK_V,NUM_BLOCK_H);
	auto BM = new BMStruct(NumBlockV, NumBlockH);

	// for i = 1:NUM_BLOCK_V,
	//    for j = 1:NUM_BLOCK_H,
   BM_threadParams_struct BM_threadParams;
   BM_threadParams.y0 = y0;
   BM_threadParams.y1 = y1;
   BM_threadParams.y2 = y2;
   BM_threadParams.BM = BM;
   int nJobs = NumBlockV * NumBlockH;
   SynchronousMthreadRunner mthreads(nJobs, &BM_threadParams, &BM_thread_trampoline);
   mthreads.Run();

   return BM;
//end
}

//*******************************************************************
// function Direction=ChooseMotion (BMB, BMA)
//%
//%  Determine which block matching result is better.
//%  BMB contains the motion to the past
//%  BMA contains the motion to the future
//%
//%  Return -1 for past-only motion
//%  Return +1 for future-only motion
//
//% find the number of blocks where BMB has a better score
//% and the number of blocks where BMA has a better score
MotionDirection ColorBumpUtil::ChooseMotion(const BMStruct& BmB, const BMStruct& BmA)
{
	//cntB = 0;
	//cntA = 0;
	auto cntB = 0;
	auto cntA = 0;

	//for i = 1:size(BMB.score,1),
	//	for j = 1:size(BMB.score,2),
	for (auto i = 0; i < BmB.nRows; ++i)
	{
		for (auto j = 0; j < BmB.nCols; ++j)
		{
			//if BMB.score(i, j) < BMA.score(i, j),
			//	cntB = cntB + 1;
			//elseif BMA.score(i, j) < BMB.score(i, j),
			//	cntA = cntA + 1;
			//end
			if (BmB.score[i][j] < BmA.score[i][j])
			{
				++cntB;
			}
			else if (BmA.score[i][j] < BmB.score[i][j])
			{
				++cntA;
			}

		//end
		}
	//end
   	}

	//if cntB > cntA,
	//	% past only is the best
	//	Direction = -1;
	//elseif cntA > cntB,
	//	% future only is the best
	//	Direction = 1;
	//else
	//	% neither direction is best
	//	Direction = 0;
	//end
	return (cntB > cntA) ? PastOnly : ((cntA > cntB) ? FutureOnly : Neither);
//end
}

//*******************************************************************
//function RAT = FindRatio (srcC, srcT, BM)
//% For each block find the ratio between srcC (the current frame)
//% and srcT (the nearest temporal neighbor)
RatioStruct* ColorBumpUtil::FindRatio(const Ipp32fArray& srcC, const Ipp32fArray& srcT, const BMStruct& BM)
{
	//RAT.row = zeros(size(BM.row0));
	//RAT.col = zeros(size(BM.row0));
	//RAT.mean = zeros(size(BM.row0));
	auto RAT = new RatioStruct(BM.nRows, BM.nCols);

	//for i = 1:size(BM.row0, 1),
	for (auto i = 0; i < BM.nRows; ++i)
	{
	   //	for j = 1 : size(BM.row0, 2),
		for (auto j = 0; j < BM.nCols; ++j)
		{
			//rows = BM.row0(i, j) : BM.row1(i, j);
			//cols = BM.col0(i, j) : BM.col1(i, j);
         int top    = BM.row0[i][j];
         int bottom = BM.row1[i][j];
         int left   = BM.col0[i][j];
         int right  = BM.col1[i][j];

			//% the block in the current frame
			//	blkC = srcC(rows, cols);
         IppiRect roi;
         roi.x = left;
         roi.y = top;
         roi.width = right - left + 1;
         roi.height = bottom - top + 1;
			auto blkC = srcC(roi);

         if (checkForInfiniteOrNan(blkC))
         {
            TRACE_0(errout << "Found non-finite element in blkC");
         }

			//% the block in the temporal neighbor
			//	blkT = srcT(rows + BM.row_off(i, j), cols + BM.col_off(i, j));
         roi.x += BM.colOff[i][j];
         roi.y += BM.rowOff[i][j];
			auto blkT = srcT(roi);

         if (checkForInfiniteOrNan(blkT))
         {
            TRACE_0(errout << "Found non-finite element in blkT");
         }

			//% the ratio of the correct value to the corrupted value.
			//	ratio = blkT . / blkC;
			auto ratio = blkT / blkC;

         if (checkForInfiniteOrNan(ratio))
         {
            TRACE_0(errout << "Found non-finite element in ratio");
         }

			//%  assign the ratio to the middle of the block
			//	RAT.row(i, j) = .5*(BM.row0(i, j) + BM.row1(i, j));
			// RAT.col(i, j) = .5*(BM.col0(i, j) + BM.col1(i, j));
			RAT->row[i][j] = 0.5F * (BM.row0[i][j] + BM.row1[i][j]);
			RAT->col[i][j] = 0.5F * (BM.col0[i][j] + BM.col1[i][j]);

			//RAT.mean(i, j) = mean(ratio(:));
            auto means = ratio.mean();
			RAT->mean[i][j] = accumulate(means.begin(), means.end(), 0.0f);

		// end
		}
	//	end
	}

	return RAT;
//end
}

#if OLD_WAY
//*******************************************************************
// function Surface = FindSurface (RAT)
// % Perform a robust estimate of the surface
Ipp32fArray ColorBumpUtil::FindSurface(const RatioStruct& RAT)
{
	//Y = zeros(numel(RAT.mean), 1);
	//X = zeros(numel(RAT.mean), 6);
	//Surface = zeros(size(RAT.mean));
	auto numel = RAT.nRows * RAT.nCols;
   MklMatrix32f Y(numel, 1);
   Y.zero();
   MklMatrix32f X(numel, 6);
   X.zero();
	Ipp32fArray Surface(RAT.nRows, RAT.nCols, 1);

	//cnt = 0;
	auto cnt = 0;

	//for i = 1:size(RAT.mean, 1),
	//	for j = 1 : size(RAT.mean, 2),
	for (auto i = 0; i < RAT.nRows; ++i)
	{
		for (auto j = 0; j < RAT.nCols; ++j)
		{
			//cnt = cnt + 1;
			//Y(cnt) = RAT.mean(i, j);
         // cnt increment done later because origin is 0, not 1.
			Y[cnt][0] = RAT.mean[i][j];

			//r = RAT.row(i, j);
			//c = RAT.col(i, j);
         // +1 here because we're zero-based and matlab is 1-based!
			auto r = RAT.row[i][j] + 1;
			auto c = RAT.col[i][j] + 1;

			//x = [r*r c*c r c r*c 1];
			//X(cnt, :) = x;
			float values[6] = { r*r, c*c, r, c, r*c, 1 };
         MklVector32f x(6, values);
			X[cnt++] = x;
		//end
		}
	//end
	}

 	//Beta = (X'*X) \ (X'*Y);
	auto XTranspose = X.transpose();
	auto lhs = XTranspose * X;
	auto rhs = XTranspose * Y;
	auto Beta = lhs.mldivide(rhs);

	//for i = 1:size(RAT.mean, 1),
	//	for j = 1 : size(RAT.mean, 2),
   for (auto i = 0; i < RAT.nRows; ++i)
   {
      for (auto j = 0; j < RAT.nCols; ++j)
      {
	      //	r = RAT.row(i, j);
	      //	c = RAT.col(i, j);
         // +1 here because we're zero-based and matlab is 1-based!
         auto r = RAT.row[i][j] + 1;
         auto c = RAT.col[i][j] + 1;

	      //	x = [r*r c*c r c r*c 1];
			float values[6] = { r*r, c*c, r, c, r*c, 1 };
         MklVector32f xvec(6, values);
         MklMatrix32f x(1, 6);
         x[0] = xvec;

	      // Surface(i, j) = x * Beta;
         *(Surface.GetPointerToElement(i, j)) = Ipp32f((x * Beta)[0][0]);
	   // end
      }
	// end
   }

	// % Remove the outliers

	// diff = abs(Y - X*Beta);
   auto diff = mabs(Y - (X * Beta));

   SortMatrixColumn(diff, 0);

   ////TRACE_3(errout << "Y: " << Y);
   ////TRACE_3(errout << "X: " << X);
   ////TRACE_3(errout << "Beta: " << Beta);
   ////TRACE_3(errout << "DIFF: " << diff);

	// cutoff = diff(round(.75*numel(diff(:))));
   auto cutoff = Ipp32f(diff[my_lround(0.75F * diff.rows())][0]);

   ////TRACE_3(errout << "CUTOFF = " << cutoff);

   // cnt = 0;
   cnt = 0;

	// for i = 1:size(RAT.mean, 1),
		// for j = 1 : size(RAT.mean, 2),
   for (auto i = 0; i < RAT.nRows; ++i)
   {
      for (auto j = 0; j < RAT.nCols; ++j)
      {
			// diff = abs(Surface(i, j) - RAT.mean(i, j));
         auto diff2 = fabs(*(Surface.GetPointerToElement(i, j)) - RAT.mean[i][j]);

			//if diff < cutoff,
         if (diff2 < cutoff)
         {
				// cnt = cnt + 1;
				// Y(cnt) = RAT.mean(i, j);
            // cnt increment done later because origin is 0, not 1.
            Y[cnt][0] = RAT.mean[i][j];

				// r = RAT.row(i, j);
				// c = RAT.col(i, j);
            // +1 here because we're zero-based and matlab is 1-based!
            auto r = RAT.row[i][j] + 1;
            auto c = RAT.col[i][j] + 1;

				// x = [r*r c*c r c r*c 1];
            float values[6] = { r*r, c*c, r, c, r*c, 1 };
            MklVector32f x(6, values);

				// X(cnt, :) = x;
            X[cnt++] = x;
			// end
         }
		// end
      }
	// end
   }

   TRACE_3(errout << "NON-OUTLIER COUNT = " << cnt);

	// % re - estimate the surface by ignoring the outliers

	// X = X(1:cnt, : );
	// Y = Y(1:cnt, : );
   auto X2 = MklMatrix32f(X, cnt);
   auto Y2 = MklMatrix32f(Y, cnt);

	//Beta = (X'*X) \ (X'*Y);
	auto XTranspose2 = X2.transpose();
	lhs = XTranspose2 * X2;
	rhs = XTranspose2 * Y2;
	Beta = lhs.mldivide(rhs);

	//for i = 1:size(RAT.mean, 1),
	//	for j = 1 : size(RAT.mean, 2),
   for (auto i = 0; i < RAT.nRows; ++i)
   {
      for (auto j = 0; j < RAT.nCols; ++j)
      {
	      //	r = RAT.row(i, j);
	      //	c = RAT.col(i, j);
         // +1 here because we're zero-based and matlab is 1-based!
         auto r = RAT.row[i][j] + 1;
         auto c = RAT.col[i][j] + 1;

	      //	x = [r*r c*c r c r*c 1];
			float values[6] = { r*r, c*c, r, c, r*c, 1 };
         MklVector32f xvec(6, values);
         MklMatrix32f x(1, 6);
         x[0] = xvec;

	      // Surface(i, j) = x * Beta;
         auto xTimesBeta = x * Beta;
         *(Surface.GetPointerToElement(i, j)) = Ipp32f(xTimesBeta[0][0]);
	   // end
      }
	// end
   }

//end
   return Surface;
}
#endif // OLD_WAY

//*******************************************************************
//function Surface = FindSurfaceTPS (RAT, scale)
//% Perform a robust estimate of the surface
Ipp32fArray ColorBumpUtil::FindSurfaceTPS(const RatioStruct& RAT, double scale)
{
   TRACE_3(errout << "ENTER function FindSurfaceTPS");
   // nI = size(RAT.mean,1);
   // nJ = size(RAT.mean,2);
   // nK = size(RAT.mean,3);
   auto nI = RAT.nRows;
   auto nJ = RAT.nCols;

   // Y = zeros (nI*nJ, 1);
   // X = zeros (nI*nJ, 2);
   MklMatrix32f Y(nI * nJ, 1);
   Y.zero();
   MklMatrix32f X(nI * nJ, 2);
   X.zero();

   //Surface = zeros(nI,nJ,nK);
	Ipp32fArray Surface({RAT.nCols, RAT.nRows});

   // for k = 1:nK,   WE DON'T DO PLANES HERE (caller calls us 3x)
   //    cnt = 0;
   auto cnt = -1;       // 0-based indices vs 1-based in Matlab

   // for i = 1:nI,
   //   for j = 1:nJ,
   for (auto i = 0; i < nI; ++i)
   {
      for (auto j = 0; j < nJ; ++j)
      {
         // cnt = cnt + 1;
         // Y(cnt) = RAT.mean(i,j,k);
         cnt = cnt + 1;
         Y[cnt][0] = RAT.mean[i][j];

         // % scale the (row,col) values to roughly the same scale
         // % as the Y values.  This makes the smoothing parameter
         // % more intuitive.
         // X(cnt,1) = RAT.row(i,j)/scale;
         // X(cnt,2) = RAT.col(i,j)/scale;
         X[cnt][0] = float(RAT.row[i][j] / scale);
         X[cnt][1] = float(RAT.col[i][j] / scale);
      // end
      }
   // end
   }

   ////TRACE_3(errout << "Y: " << Y);
   ////TRACE_3(errout << "X: " << X);

   // Yhat = tps (X, Y, .95, X);
   MklMatrix32f Yhat;
   tps(X, Y, 0.95F, X, Yhat);

   ///TRACE_3(errout << "Yhat: " << Yhat);

   // cnt = 0;
   cnt = -1;       // 0-based indices vs 1-based in Matlab

   // for i = 1:nI,
   //    for j = 1:nJ,
   for (auto i = 0; i < nI; ++i)
   {
      for (auto j = 0; j < nJ; ++j)
      {
         // cnt = cnt + 1;
         // Surface(i,j,k) = Yhat(cnt);
         cnt = cnt + 1;
         Surface[{j, i}] = Ipp32f(Yhat[cnt][0]);

      // end
      }
   // end
   }
//end    - NO k
//end
   TRACE_3(errout << "EXIT function FindSurfaceTPS");
   return Surface;
}

//*******************************************************************
Ipp32fArray ColorBumpUtil::ExtractYSignal(const Ipp32fArray& src, bool isMonochrome)
{
   if (src.getComponents() != 3)
   {
      throw std::exception("Input to ExtractYSignal must have three components!");
   }

   Ipp32fArray dest(src.getRoi().getSize());
   IppiSize size = src.getRoi().getSize();

   // If the source is deemed monochrome, we want to use just the green channel,
   // else combine all the channels.
   Ipp32f coeffsY[3] = {0.2F, 0.7F, 0.1F};
   Ipp32f coeffsGreen[3] = {0.0F, 1.0F, 0.0F};

   ippiColorToGray_32f_C3C1R(
      src.data(),
      src.getRowPitchInBytes(),
      dest.data(),
      dest.getRowPitchInBytes(),
      size,
      isMonochrome ? coeffsGreen : coeffsY);

   return dest;
}

//*******************************************************************
Ipp32fArray ColorBumpUtil::ConvertYToRGB(const Ipp32fArray& src)
{
   if (src.getComponents() != 1)
   {
      throw std::exception("Input to ConvertYToRGB must have only one component!");
   }

   Ipp32fArray dest({src.getRoi().getSize(), 3});
   IppiSize size = {src.getRoi().width, src.getRoi().height};

   ippiCopy_32f_C1C3R(
      src.data(),
      src.getRowPitchInBytes(),
      dest.data(),
      dest.getRowPitchInBytes(),
      size);

   return dest;
}

//*******************************************************************

#include "HRTimer.h"
#include "RepairDRS.h"
void wtfCopyInput(COPY_INPUT_STRUCT *cisp, int iJob, long &lRow, long &lCol, long &lSum, long &lRowStart, long &lRowStop, long &lFineRow, long &lFineCol, MTI_UINT16 *&uspD, MTI_UINT16 *&uspS)
{
   CHRTimer timer;
//   TRACE_0(errout << "Start job " << iJob);
   for (lRow = lRowStart; lRow < lRowStop; lRow++)
   {
      lCol = 0;
      uspD = cisp->uspDest + lRow * cisp->lNColMot + lCol;
      for (; lCol < cisp->lNColMot; lCol++)
      {
         lSum = 0;
         for (lFineRow = cisp->lCoarse * lRow; lFineRow < cisp->lCoarse * (lRow + 1); lFineRow++)
         {
            lFineCol = cisp->lCoarse * lCol;
            uspS = cisp->uspSource + ((lFineRow * cisp->lNFineCol + lFineCol) * cisp->lNFineCom);

// WTF?? Why would you put these here when the loop DOES THE EXACT SAME THING
// EXCEPT MAKES SURE YOU'RE NOT READING PAST THE END OF THE FRAME???????
//            lSum += (long) * uspS;
//            uspS += cisp->lNFineCom;
//            lFineCol++;
//            lSum += (long) * uspS;
//            uspS += cisp->lNFineCom;
//            lFineCol++;
            for (; lFineCol < cisp->lCoarse * (lCol + 1); lFineCol++)
            {
               lSum += (long) * uspS;
               uspS += cisp->lNFineCom;
            }
         }

         *uspD++ = (float)lSum / (float)cisp->lNCoarse + 0.5;
      }
   }

//   TRACE_0(errout << "End job " << iJob << ", " << timer.ReadAsString() << " msec");
}


