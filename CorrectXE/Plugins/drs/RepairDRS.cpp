/*
	Name:	RepairDRS.cpp
	By:	Kevin Manbeck
	Date:	June 29, 2001

	The RepairDRS class is derived from the Repair class.  It is
	reponsible for DRS-type repairs.
*/
#include "IniFile.h"	// for TRACE
#include "MTIio.h"
#include "MTImalloc.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <math.h>
#include <vector>

#include "RepairDRS.h"
#include "mt_math.h"
#include "cpmp_err.h"
#include "err_repair.h"
#include "MTImalloc.h"
#include "HRTimer.h"

//#define FSQR(a) ( (fsqrarg = (a)) == 0.0 ? 0.0 : fsqrarg*fsqrarg )
#define FSQR(a) ((a)*(a) )

#define DRS_OVERRIDE_PADDING_SMALL 20
////#define PRINT_OUT

void DoRegionScore (void *vp, int iJob);
void FinishRegionScore (void *vp);
void DoCalcScore (void *vp, int iJob);
void FinishCalcScore (void *vp);
void DoCalcSurroundScore (void *vp, int iJob);
void FinishCalcSurroundScore (void *vp);
void DoCopyInput (void *vp, int iJob);
void CallDoHardMotionEdgePreserving (void *vp, int iJob);
void CallDoHardMotionSmoothing (void *vp, int iJob);


CRepairDRS::CRepairDRS()
{
  int iRet;

  lAcclRow = 0;
  lAcclCol = 0;
  lOffsetRow = 0;
  lOffsetCol = 0;

//  set algorithm parameters to defaults

  lNDim = 5;
  lDimension = 2;
  lPickRow = 0;
  lPickCol = 0;
  lPickFrame = 0;
  lHardMotionIters = 250;
  fUseAveragePercent = 0.05;
  fUseOriginalPercent = 0.01;
  bUseGradient = true;

  _grainPresetValues = nullptr;
  _useGrainPresets = false;

  clp = new CCoarseLevel();

  lpBoundaryRow = NULL;
  lpBoundaryCol = NULL;
  lpBoundaryData = NULL;
  lNBoundaryAlloc = 0;

  fpBuffer = NULL;
  lNBufferAlloc = 0;

/*
	Set up the multithread structures
*/

		// structure used for RegionScore()
  msRegionScore.PrimaryFunction = &DoRegionScore;
  msRegionScore.SecondaryFunction = &FinishRegionScore;
  msRegionScore.CallBackFunction = NULL;
  msRegionScore.vpApplicationData = &rss;
  msRegionScore.iNThread = lNProcessor;
  msRegionScore.iNJob = lNProcessor? lNProcessor : 1;

  iRet = MThreadAlloc (&msRegionScore);
  if (iRet)
   {
    assert (false);
   }

		// structure used for CalcScore()
  msCalcScore.PrimaryFunction = &DoCalcScore;
  msCalcScore.SecondaryFunction = &FinishCalcScore;
  msCalcScore.CallBackFunction = NULL;
  msCalcScore.vpApplicationData = &css;
  msCalcScore.iNThread = lNProcessor;
  msCalcScore.iNJob = lNProcessor? lNProcessor : 1;

  iRet = MThreadAlloc (&msCalcScore);
  if (iRet)
   {
    assert (false);
   }

		// structure used for CalcSurroundScore()
  msCalcSurroundScore.PrimaryFunction = &DoCalcSurroundScore;
  msCalcSurroundScore.SecondaryFunction = &FinishCalcSurroundScore;
  msCalcSurroundScore.CallBackFunction = NULL;
  msCalcSurroundScore.vpApplicationData = &csss;
  msCalcSurroundScore.iNThread = lNProcessor;
  msCalcSurroundScore.iNJob = lNProcessor? lNProcessor : 1;

  iRet = MThreadAlloc (&msCalcSurroundScore);
  if (iRet)
   {
    assert (false);
   }

		// structure used for CopyInput()
  msCopyInput.PrimaryFunction = &DoCopyInput;
  msCopyInput.SecondaryFunction = NULL;
  msCopyInput.CallBackFunction = NULL;
  msCopyInput.vpApplicationData = &cis;
  msCopyInput.iNThread = lNProcessor;
  msCopyInput.iNJob = lNProcessor? lNProcessor : 1;

  iRet = MThreadAlloc (&msCopyInput);
  if (iRet)
   {
    assert (false);
   }

		// structure used for HardMotionEdgePreserving()
  msHardMotionEdgePreserving.PrimaryFunction = &CallDoHardMotionEdgePreserving;
  msHardMotionEdgePreserving.SecondaryFunction = NULL;
  msHardMotionEdgePreserving.CallBackFunction = NULL;
  msHardMotionEdgePreserving.vpApplicationData = this;
  msHardMotionEdgePreserving.iNThread = lNProcessor;
  msHardMotionEdgePreserving.iNJob = (lNProcessor? lNProcessor : 1) * 2;

  iRet = MThreadAlloc (&msHardMotionEdgePreserving);
  if (iRet)
   {
    assert (false);
   }

		// structure used for HardMotionSmoothing()
  msHardMotionSmoothing.PrimaryFunction = &CallDoHardMotionSmoothing;
  msHardMotionSmoothing.SecondaryFunction = NULL;
  msHardMotionSmoothing.CallBackFunction = NULL;
  msHardMotionSmoothing.vpApplicationData = this;
  msHardMotionSmoothing.iNThread = lNProcessor;
  msHardMotionSmoothing.iNJob = (lNProcessor? lNProcessor : 1) * 2;

  iRet = MThreadAlloc (&msHardMotionSmoothing);
  if (iRet)
   {
    assert (false);
   }


  OverrideMoveOver (0., 0.);
  OverrideMoveUnder (0., 0.);
  OverrideColorBrightness (50);
  OverrideColorSaturation (50);
  OverrideColorRed (50);
  OverrideColorGreen (50);
  OverrideColorBlue (50);
  OverrideGrainSize (0);
  OverrideGrainStrength (0);
  OverrideRotateOver (0.);
  OverrideRotateUnder (0.);
  OverrideTransparency (0);

  fOldSinOver = 0.F;
  fOldSinUnder = 0.F;

  lGrainSeed = 8311315;
}  /* CRepairDRS */

CRepairDRS::~CRepairDRS()
{
  int iRet;

  delete clp;
  clp = NULL;

  MTIfree (lpBoundaryRow);
  lpBoundaryRow = NULL;
  MTIfree (lpBoundaryCol);
  lpBoundaryCol = NULL;
  MTIfree (lpBoundaryData);
  lpBoundaryData = NULL;
  lNBoundaryAlloc = 0;

  MTIfree (fpBuffer);
  fpBuffer = NULL;
  lNBufferAlloc = 0;

  iRet = MThreadFree (&msRegionScore);
  if (iRet)
   {
    assert (false);
   }

  iRet = MThreadFree (&msCalcScore);
  if (iRet)
   {
    assert (false);
   }

  iRet = MThreadFree (&msCalcSurroundScore);
  if (iRet)
   {
    assert (false);
   }

  iRet = MThreadFree (&msCopyInput);
  if (iRet)
   {
    assert (false);
   }

  iRet = MThreadFree (&msHardMotionEdgePreserving);
  if (iRet)
   {
    assert (false);
   }

  iRet = MThreadFree (&msHardMotionSmoothing);
  if (iRet)
   {
    assert (false);
   }

}  /* ~CRepairDRS */

void CRepairDRS::ReleaseMemory()
{
  clp->Free ();
}

int CRepairDRS::setMotionAcceleration (long AcclRow, long AcclCol)
{
  if (AcclRow < 0 || AcclCol < 0)
    return REPAIR_ERROR_BAD_PARAMETER;

  lAcclRow = AcclRow;
  lAcclCol = AcclCol;

  return 0;
}  /* setMotionAcceleration */

int CRepairDRS::setMotionOffset (long OffsetRow, long OffsetCol)
{
  if (OffsetRow < 0 || OffsetCol < 0)
    return REPAIR_ERROR_BAD_PARAMETER;

  lOffsetRow = OffsetRow;
  lOffsetCol = OffsetCol;

  return 0;
}  /* setMotionOffset */

int CRepairDRS::setDimension (long NDim, long Dimension)
{
  if (NDim < 0 || Dimension < 0)
    return REPAIR_ERROR_BAD_PARAMETER;

  lNDim = NDim;
  lDimension = Dimension;

  return 0;
}  /* setDimension */

int CRepairDRS::setPick (long PickRow, long PickCol, long PickFrame)
{
  return 0;
}  /* setPick */

int CRepairDRS::setIterations (long HardMotionIters)
{
  if (HardMotionIters < 0)
    return REPAIR_ERROR_BAD_PARAMETER;

  lHardMotionIters = HardMotionIters;

  return 0;
}  /* setIterations */

int CRepairDRS::setPercent (float UseAveragePercent, float UseOriginalPercent)
{
  if (UseAveragePercent < 0. || UseAveragePercent > 1. ||
	UseOriginalPercent < 0. || UseOriginalPercent > 1.)
    return REPAIR_ERROR_BAD_PARAMETER;

  fUseAveragePercent = UseAveragePercent;
  fUseOriginalPercent = UseOriginalPercent;

  return 0;
}  /* setPercent */

void CRepairDRS::setGradient (bool UseGradient)
{
  bUseGradient = UseGradient;

  return;
}  /* setGradient */

void CRepairDRS::setGrainPresetValues (const float *grainPresetValues)
{
  _grainPresetValues = grainPresetValues;
}

void CRepairDRS::setUseGrainPresets(bool flag)
{
   _useGrainPresets = flag;
}

void CRepairDRS::setColorBumpCrap(bool monochromeFlag, int direction, int validFrameMask)
{
   bIsMonochrome = monochromeFlag;
   cbdDirection = (direction < 0)
                   ? PAST_ONLY
                   : ((direction > 0)
                      ? FUTURE_ONLY
                      : AUTO);

   _validFrameMask = validFrameMask;
}

#define NULL_FIX 0

int CRepairDRS::PerformFix(long RepairType)
{
#if !NULL_FIX

	int iRet;

	lRepairType = RepairType;

	renderMask();
	OverridePrepare();

	// Pad
	lSrcPaddedTop = lSrcBoundTop - DRS_OVERRIDE_PADDING_SMALL;
	if (lSrcPaddedTop < lRegionULCRow)
	{
		lSrcPaddedTop = lRegionULCRow;
	}

	lSrcPaddedBottom = lSrcBoundBottom + DRS_OVERRIDE_PADDING_SMALL;
	if (lSrcPaddedBottom > lRegionLRCRow)
	{
		lSrcPaddedBottom = lRegionLRCRow;
	}

	lSrcPaddedLeft = lSrcBoundLeft - DRS_OVERRIDE_PADDING_SMALL;
	if (lSrcPaddedLeft < lRegionULCCol)
	{
		lSrcPaddedLeft = lRegionULCCol;
	}

	lSrcPaddedRight = lSrcBoundRight + DRS_OVERRIDE_PADDING_SMALL;
	if (lSrcPaddedRight > lRegionLRCCol)
	{
		lSrcPaddedRight = lRegionLRCCol;
	}

	lDstPaddedTop = lDstBoundTop - DRS_OVERRIDE_PADDING_SMALL;
	if (lDstPaddedTop < lRegionULCRow)
	{
		lDstPaddedTop = lRegionULCRow;
	}

	lDstPaddedBottom = lDstBoundBottom + DRS_OVERRIDE_PADDING_SMALL;
	if (lDstPaddedBottom > lRegionLRCRow)
	{
		lDstPaddedBottom = lRegionLRCRow;
	}
	lDstPaddedLeft = lDstBoundLeft - DRS_OVERRIDE_PADDING_SMALL;
	if (lDstPaddedLeft < lRegionULCCol)
	{
		lDstPaddedLeft = lRegionULCCol;
	}
	lDstPaddedRight = lDstBoundRight + DRS_OVERRIDE_PADDING_SMALL;
	if (lDstPaddedRight > lRegionLRCCol)
	{
		lDstPaddedRight = lRegionLRCCol;
	}

	iRet = MakeCommandFile();
	if (iRet)
	{
		return iRet;
	}

	/*
	 Check for errors in the RepairType
	 */

	if (lRepairType != RT_ORDINARY && lRepairType != RT_HARD_MOTION && lRepairType != RT_ADJUST_DENSITY && lRepairType != RT_HARD_MOTION_2 &&
			lRepairType != RT_COLOR_BUMP && lRepairType != RT_REPLACE)
	{
		return REPAIR_ERROR_UNKNOWN_REPAIR_TYPE;
	}

	if (lRepairType & (RT_ORDINARY | RT_REPLACE | RT_ADJUST_DENSITY))
	{
		iRet = OrdinaryMotion();
		if (iRet)
		{
			return iRet;
		}
	}
	else if (lRepairType & RT_COLOR_BUMP)
	{
		iRet = ColorBump();
		if (iRet)
		{
			return iRet;
		}
	}
	else
	{
		iRet = HardMotion();
		if (iRet)
		{
			return iRet;
		}
	}

	// Apply the override values
	OverrideApply();

#endif

	return 0;

} /* PerformFix */

int CRepairDRS::MakeCommandFile ()
{
  long lFrame, lNByte, lRow;
  int iFD;
  FILE *fp;
  char caFileName[128];

  if (bCommandFileFlag == false)
    return 0;

  fp = fopen ("drs.cmd", "w");
  if (fp == NULL)
   {
    return REPAIR_ERROR_OPENING_COMMAND_FILE;
   }

  fprintf (fp, "DRS Command file\n");
  fprintf (fp, "RepairType: %ld\n", lRepairType);
  fprintf (fp, "AcclRow: %ld\n", lAcclRow);
  fprintf (fp, "AcclCol: %ld\n", lAcclCol);
  fprintf (fp, "OffsetRow: %ld\n", lOffsetRow);
  fprintf (fp, "OffsetCol: %ld\n", lOffsetCol);
  fprintf (fp, "NDim: %ld\n", lNDim);
  fprintf (fp, "Dimension: %ld\n", lDimension);
  fprintf (fp, "PickRow: %ld\n", lPickRow);
  fprintf (fp, "PickCol: %ld\n", lPickCol);
  fprintf (fp, "PickFrame: %ld\n", lPickFrame);
  fprintf (fp, "HardMotionIters: %ld\n", lHardMotionIters);
  fprintf (fp, "UseAveragePercent: %f\n", fUseAveragePercent);
  fprintf (fp, "UseOriginalPercent: %f\n", fUseOriginalPercent);
  fprintf (fp, "UseGradient: %d\n", bUseGradient);

  fprintf (fp, "NRow: %ld\n", lRegionLRCRow - lRegionULCRow);
  fprintf (fp, "NCol: %ld\n", lRegionLRCCol - lRegionULCCol);
  fprintf (fp, "NCom: %ld\n", lNCom);
  fprintf (fp, "PrincipalCom: %ld\n", lPrincipalCom);
  fprintf (fp, "MinVal: %ld %ld %ld\n", laMinValue[0], laMinValue[1],
		laMinValue[2]);
  fprintf (fp, "MaxVal: %ld %ld %ld\n", laMaxValue[0], laMaxValue[1],
		laMaxValue[2]);
  fprintf (fp, "FillVal: %ld %ld %ld\n", laFillValue[0], laFillValue[1],
		laFillValue[2]);
  fprintf (fp, "NFrame: %ld\n", lNFrame);
  fprintf (fp, "RepairFrame: %ld\n", lRepairFrame);
  fprintf (fp, "TemporalIndex: %ld %ld %ld %ld %ld\n",
		laTemporalIndex[0], laTemporalIndex[1], laTemporalIndex[2],
		laTemporalIndex[3], laTemporalIndex[4]);
  fclose (fp);

  for (lFrame = 0; lFrame < lNFrame; lFrame++)
   {
	sprintf (caFileName, "drs%ld.dat", lFrame);
    iFD = open (caFileName, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0644);
    if (iFD == -1)
      return REPAIR_ERROR_OPENING_COMMAND_DATA_FILE;

    lNByte = (lRegionLRCCol - lRegionULCCol) * sizeof (MTI_UINT16) * lNCom;
    for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
     {
	  if (write (iFD, uspData[lFrame] +
		((lRow * lPitch + lRegionULCCol) * lNCom), lNByte) != lNByte)
       {
        return REPAIR_ERROR_WRITING_COMMAND_DATA;
       }
     }

    close (iFD);
   }

  sprintf (caFileName, "drs.lm");
  iFD = open (caFileName, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0644);
  if (iFD == -1)
    return REPAIR_ERROR_OPENING_COMMAND_DATA_FILE;

  lNByte = (lRegionLRCCol - lRegionULCCol) * sizeof (MTI_UINT8);
  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    if (write (iFD, roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol),
		lNByte) != lNByte)
     {
      return REPAIR_ERROR_WRITING_COMMAND_DATA;
     }
   }
  close (iFD);

  sprintf (caFileName, "drs.bm");
  iFD = open (caFileName, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0644);
  if (iFD == -1)
    return REPAIR_ERROR_OPENING_COMMAND_DATA_FILE;

  lNByte = (lRegionLRCCol - lRegionULCCol) * sizeof (MTI_UINT8);
  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    if (write (iFD, roi.getBlendPtr() + (lRow * lPitch + lRegionULCCol),
		lNByte) != lNByte)
     {
      return REPAIR_ERROR_WRITING_COMMAND_DATA;
     }
   }
  close (iFD);

  return 0;
} /* MakeCommandFile */

int CRepairDRS::EstablishParameters ()
{
/*
	What is
*/

  return 0;
}  /* EstablishParameters */

int CRepairDRS::EstimateMotion ()
{
  int iRet;
  long lDim;

  // FULL FRAME FIXES CANNOT DO MOTION!
  if (lRegionULCRow <= 0 && lRegionLRCRow >= lNImageRows && lRegionULCCol <= 0 && lRegionLRCCol >= lPitch)
   {
     lAcclRow = 0;
     lAcclCol = 0;
     lOffsetRow = 0;
     lOffsetCol = 0;
   }

  iRet = CopyInput ();
  if (iRet)
    return iRet;

  for (lDim = 0; lDim < clp->lNDim; lDim++)
   {
    if (bUseGradient)
     {
      GradientImage (lDim);
     }

    iRet = MakeRegion (clp->rdaRepair + lDim);
    if (iRet)
      return iRet;
   }

  if (bUseGradient)
   {
    iRet = MakeRegion (&clp->rdOrig);
    if (iRet)
      return iRet;
   }


#ifdef DEBUG_D
{
 long lFrame, lRow, lCol, lRowColCom;
 char caFName[64];
 FILE *fp;

 for (lFrame = 0; lFrame < lNFrame; lFrame++)
  {
   sprintf (caFName, "r%d.dat", lFrame);
   fp = fopen (caFName, "w");

  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
  for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
   {
    lRowColCom = (lRow * lPitch + lCol) * lNCom + lPrincipalCom;
    fprintf (fp, "%d ", (int)uspData[lFrame][lRowColCom]);
   }
  fprintf (fp, "\n");
  }
  fclose(fp);
 }
}
#endif

#ifdef DEBUG_D
{
 long lFrame, lRow, lCol;
 char caFName[64];
 FILE *fp;

 for (lFrame = 0; lFrame < lNFrame; lFrame++)
 for (lDim = 0; lDim < clp->lNDim; lDim++)
  {
   sprintf (caFName, "gr%d_%d.dat", lFrame, lDim);
   fp = fopen (caFName, "w");

  for (lRow = 0; lRow < clp->rdaRepair[lDim].lNRowMot; lRow++)
   {
  for (lCol = 0; lCol < clp->rdaRepair[lDim].lNColMot; lCol++)
   {
    fprintf (fp, "%d ", (int)clp->rdaRepair[lDim].uspMotData[lFrame]
		[lRow*clp->rdaRepair[lDim].lNColMot+lCol]);
   }
  fprintf (fp, "\n");
  }
  fclose(fp);
 }
}
#endif

/*
	Find motion at the coarsest resolution
*/

  iRet = CalcSurroundScore (clp->rdaRepair + clp->lNDim - 1);
  if (iRet)
    return iRet;

  iRet = CreateMotionPairs ();
  if (iRet)
    return iRet;

  iRet = FindBestMotion ();
  if (iRet)
    return iRet;

/*
	At this point we have motion estimates for the coarse resolution.
	Continue refining best motions for each allowed motion pair.
*/

  for (lDim = clp->lNDim - 2; lDim >= 0; lDim--)
   {
    iRet = RefineLinearMotion (lDim);
    if (iRet)
      return iRet;
   }

/*
	At this stage, we have the best linear motion.  Allow for
	acceleration.
*/

  iRet = DetermineAcceleration ();
  if (iRet)
    return iRet;

  return RET_SUCCESS;
}  /* EstimateMotion */

int CRepairDRS::ApplyFix()
{

	/*
	 If the debug flag is true, replace the defective region with
	 fill values.
	 */

	if (bDebugFlag)
	{
		UseFillValue();
	}
	else
	{
		if (lRepairType == RT_ORDINARY || lRepairType == RT_REPLACE)
		{
			ApplyFixOrdinary();
		}
//		else if (lRepairType == RT_ADJUST_DENSITY)
//		{
//			ApplyFixAdjustDensity();
//		}
	}

	return 0;
} /* ApplyFix */

void CRepairDRS::ApplyFixOrdinary()
{
	MTI_UINT8 ucaPickPattern[] =
	{0,   0,  0,  1,  1,  1,  2,  2,  3,  3,  3,  4,  4,  4,  5,  5,
    0,   0,  0,  1,  1,  6,  6,  6,  3,  3,  3,  4,  4,  7,  7,  7,
    0,   0,  8,  8,  8,  6,  6,  6,  3,  3,  9,  9,  9,  7,  7,  7,
    10, 10,  8,  8,  8,  6,  6, 11, 11, 11,  9,  9,  9,  7,  7, 10,
    10, 10,  8,  8, 12, 12, 12, 11, 11, 11,  9,  9, 13, 13, 13, 10,
    10, 14, 14, 14, 12, 12, 12, 11, 11, 15, 15, 15, 13, 13, 13, 10,
    16, 14, 14, 14, 12, 12, 17, 17, 17, 15, 15, 15, 13, 13, 16, 16,
    16, 14, 14, 18, 18, 18, 17, 17, 17, 15, 15, 19, 19, 19, 16, 16,
    20, 20, 20, 18, 18, 18, 17, 17, 21, 21, 21, 19, 19, 19, 16, 16,
    20, 20, 20, 18, 18, 22, 22, 22, 21, 21, 21, 19, 19, 23, 23, 23,
    20, 20, 24, 24, 24, 22, 22, 22, 21, 21, 25, 25, 25, 23, 23, 23,
    26, 26, 24, 24, 24, 22, 22, 27, 27, 27, 25, 25, 25, 23, 23, 26,
    26, 26, 24, 24, 28, 28, 28, 27, 27, 27, 25, 25, 29, 29, 29, 26,
    26, 30, 30, 30, 28, 28, 28, 27, 27, 31, 31, 31, 29, 29, 29, 26,
     5, 30, 30, 30, 28, 28,  2,  2,  2, 31, 31, 31, 29, 29,  5,  5,
     5, 30, 30,  1,  1,  1,  2,  2,  2, 31, 31,  4,  4,  4,  5,  5};

	long lFrame0 = clp->laFrame0[clp->lBestPair];
	long lFrame1 = clp->laFrame1[clp->lBestPair];

	// Establish the intergral shifts
	long lRowShift0 = clp->mBestAccel.lMotionRow0;
	long lColShift0 = clp->mBestAccel.lMotionCol0;
	long lRowShift1 = clp->mBestAccel.lMotionRow1;
	long lColShift1 = clp->mBestAccel.lMotionCol1;

	MTI_UINT16 *uspData0 = uspData[lFrame0] + (lRowShift0 * lPitch + lColShift0) * lNCom;
	MTI_UINT16 *uspData1 = uspData[lFrame1] + (lRowShift1 * lPitch + lColShift1) * lNCom;
	MTI_UINT16 *uspDataOrig = uspData[lRepairFrame];

	const long lMinRow0 = lRegionULCRow - lRowShift0;
	const long lMaxRow0 = lRegionLRCRow - lRowShift0;
	const long lMinCol0 = lRegionULCCol - lColShift0;
	const long lMaxCol0 = lRegionLRCCol - lColShift0;
	const long lMinRow1 = lRegionULCRow - lRowShift1;
	const long lMaxRow1 = lRegionLRCRow - lRowShift1;
	const long lMinCol1 = lRegionULCCol - lColShift1;
	const long lMaxCol1 = lRegionLRCCol - lColShift1;
	const long lMinRow = ((lMinRow0 < lMinRow1) ? lMinRow0 : lMinRow1);
	const long lMaxRow = ((lMaxRow0 > lMaxRow1) ? lMaxRow0 : lMaxRow1);
	const long lMinCol = ((lMinCol0 < lMinCol1) ? lMinCol0 : lMinCol1);
	const long lMaxCol = ((lMaxCol0 > lMaxCol1) ? lMaxCol0 : lMaxCol1);

   long lTime0 = laTemporalIndex[lFrame0];
   long lTime1 = laTemporalIndex[lFrame1];
   long lTime = laTemporalIndex[lRepairFrame];

   // HACK - here we treat FULL FRAME REPLACE differently from
   // normal replace. In FFR we linearly  factor in the distance
   // of the fix frame from the two source frames and blend the
   // source values accordingly. In normal replace mode we always
   // blend them 50-50.
	bool bReplace = lRepairType == RT_REPLACE;
	bool bReplacingFullFrame = bReplace && lMinRow <= 0 && lMaxRow >= lNImageRows && lMinCol <= 0 && lMaxCol >= lPitch;
	float fDist0 = abs(lTime - lTime0);
	float fDist1 = abs(lTime - lTime1);

   // Note: in the following. "fDist1/" because smaller distance gets higher fraction!
   // Also the non-full-frame replace mode ALWAYS averages the values from the two marked frames for legacy reasons,
   // whereas in full-frame-replace mode we weight the values proportionally to how close the source frame is to the target.
   // Also note that all three frames are always defined - if there is only one source frame, both 0 and 1 wiull point
	// to it. It's also possible that the source frame is the target, so all three frames may point to the target!!
	float sourceFrameBlendFraction0 = (bReplace && (fDist0 + fDist1) == 0.F)
													? 1.F
													: (bReplacingFullFrame
														? (fDist1 / (fDist0 + fDist1))
														: 0.5F);
	float sourceFrameBlendFraction1 = 1.F - sourceFrameBlendFraction0;

	/*
	 Compute the average value in Region0 and Region1.  Use this
	 average to adjust for fades
	 */
	double faSum0[MAX_COMPONENT];
	double faSum1[MAX_COMPONENT];

	for (long lComponent = 0; lComponent < lNCom; lComponent++)
	{
		faSum0[lComponent] = 0.0;
		faSum1[lComponent] = 0.0;
	}

	long lNSelected = 0;

	const MTI_UINT8 *ucpRoiLabelPtr = roi.getLabelPtr();

	for (long lRow = lSrcPaddedTop; lRow < lSrcPaddedBottom; lRow++)
	{
		const long lRowCol0 = lRow * lPitch + lSrcPaddedLeft;
		const MTI_UINT8 *ucpRoiLabelPtr0 = ucpRoiLabelPtr + lRowCol0;
		const long lColStop = lSrcPaddedRight - lSrcPaddedLeft;

		for (long lCol = 0; lCol < lColStop; lCol++)
		{
			if (ucpRoiLabelPtr0[lCol] & LABEL_INTERIOR)
			{
				const long lRowColCom0 = (lRowCol0 + lCol) * lNCom;

				for (long lComponent = 0; lComponent < lNCom; lComponent++)
				{
               faSum0[lComponent] += imageDatumToFloat(uspData0[lRowColCom0 + lComponent]);
               faSum1[lComponent] += imageDatumToFloat(uspData1[lRowColCom0 + lComponent]);
				}
				lNSelected++;
			}
		}
	}

	float faAve0[MAX_COMPONENT];
	float faAve1[MAX_COMPONENT];
	float faAve[MAX_COMPONENT];

	for (long lComponent = 0; lComponent < lNCom; lComponent++)
	{
		faAve0[lComponent] = (float) faSum0[lComponent] / (float) lNSelected;
		faAve1[lComponent] = (float) faSum1[lComponent] / (float) lNSelected;
	}

	/*
	 Compute average for the new frame
	 */

	if (lFrame0 == lFrame1 || laTemporalIndex[lFrame0] == laTemporalIndex[lFrame1])
	{
		for (long lComponent = 0; lComponent < lNCom; lComponent++)
		{
			faAve[lComponent] = faAve0[lComponent];
		}
	}
	else
	{
		for (long lComponent = 0; lComponent < lNCom; lComponent++)
		{
			float fSlope = (faAve1[lComponent] - faAve0[lComponent]) / (float)(lTime1 - lTime0);
			float fInter = faAve0[lComponent] - fSlope * (float)lTime0;

			faAve[lComponent] = fSlope * (float)lTime + fInter;

			if (faAve0[lComponent] == 0.)
			{
				faAve0[lComponent] = 1.;
			}
			if (faAve1[lComponent] == 0.)
			{
				faAve1[lComponent] = 1.;
			}
		}
	}

#ifdef PRINT_OUT
	{
		char caMessage[128];
		sprintf(caMessage, "In ApplyFixOrdinary the best motion is %f (%ld, %ld) (%ld, %ld) pair: %ld %ld repair frame: %ld\n",
				clp->mBestAccel.fScore, clp->mBestAccel.lMotionRow0, clp->mBestAccel.lMotionCol0, clp->mBestAccel.lMotionRow1,
				clp->mBestAccel.lMotionCol1, lFrame0, lFrame1, lRepairFrame);
		TRACE_0(errout << caMessage);
	}
#endif
	long lRowStart = lSrcPaddedTop; // (lSrcPaddedTop < lDstPaddedTop)? lSrcPaddedTop : lDstPaddedTop;
	long lRowStop = lSrcPaddedBottom; // (lSrcPaddedBottom > lDstPaddedBottom)? lSrcPaddedBottom : lDstPaddedBottom;
	long lColStart = lSrcPaddedLeft; // (lSrcPaddedLeft < lDstPaddedLeft)? lSrcPaddedLeft : lDstPaddedLeft;
	long lColStop = lSrcPaddedRight; // (lSrcPaddedRight > lDstPaddedRight)? lSrcPaddedRight : lDstPaddedRight;

	for (long lRow = lRowStart; lRow < lRowStop; lRow++)
	{
		bool bRowInBounds = (lRow >= lMinRow && lRow < lMaxRow);

		for (long lCol = lColStart; lCol < lColStop; lCol++)
		{
			long lRowCol = lRow * lPitch + lCol;
			long lRowColCom = lRowCol * lNCom;

			lPickRow = (lRow + lPickRow) % PICK_TABLE_DIM;
			lPickCol = (lCol + lPickCol) % PICK_TABLE_DIM;
			long lPickRowCol = lPickRow * PICK_TABLE_DIM + lPickCol;
			long lPick = ucaPickPattern[lPickRowCol];

			float fNew;
			bool bRowColInBounds = bRowInBounds && (lCol >= lMinCol && lCol < lMaxCol);

			for (long lComponent = 0; lComponent < lNCom; lComponent++)
			{
				float fOrig = imageDatumToFloat(uspDataOrig[lRowColCom]);

				if (bRowColInBounds)
				{
					float fData0 = imageDatumToFloat(uspData0[lRowColCom]);
					float fData1 = imageDatumToFloat(uspData1[lRowColCom]);
					fData0 *= (faAve[lComponent] / faAve0[lComponent]);
					fData1 *= (faAve[lComponent] / faAve1[lComponent]);

					/*
					 Examine the image values to decide whether to pick fData0 or
					 fData1, or average fData0 and fData1.
					 */

               if (bReplace)
               {
                  fNew = (fData0 * sourceFrameBlendFraction0) + (fData1 * sourceFrameBlendFraction1);
               }
               else
               {
                  float fDiff = fData0 - fData1;
                  if (fDiff < 0)
                  {
                     fDiff *= -1.;
                  }
                  float fAve = (fData0 + fData1) / 2.;
                  float fPercent;

                  if (fAve == 0.)
                  {
                     fPercent = 1.;
                  }
                  else
                  {
                     fPercent = fDiff / fAve;
                  }

                  if (fPercent > fUseAveragePercent)
                  {
                     fNew = fAve;
                  }
                  else /* use pick */
                  {
                     if (clp->laPickValue[lPickFrame % NUM_PICK_UNIQUE][lPick] == PICK_A)
                     {
                        fNew = fData0;
                     }
                     else
                     {
                        fNew = fData1;
                     }
                  }

                  /*
                   Examine the image values to decide whether to modify the value
                   or stick with the original
                   */

                  fDiff = fNew - fOrig;
                  if (fDiff < 0.)
                  {
                     fDiff *= -1.;
                  }
                  if (fOrig == 0.)
                  {
                     fPercent = 1.;
                  }
                  else
                  {
                     fPercent = fDiff / fOrig;
                  }

                  if (fPercent <= fUseOriginalPercent)
                  {
                     fNew = fOrig;
                  }
               }
				}
				else
				{
					fNew = fOrig;
				}

				uspDefaultResult[lRowColCom] = floatToImageDatum(fNew);

				lRowColCom++;
			}
		}
	}

	return;
} /* ApplyFixOrdinary */

//void CRepairDRS::ApplyFixAdjustDensity ()
//{
//printf ("TTT Should perform fix\n");
//}  /* ApplyFixAdjustDensity */

int CRepairDRS::OrdinaryMotion ()
{
  int iRet = 0;

/*
	Free the previous CoarseLevel storage
*/

  clp->Free ();

/*
	Determine the motion associated with this debris region
*/

  if (iRet == 0)
	{
	iRet = EstimateMotion ();
   }

/*
	Perform the fix
*/

  if (iRet == 0)
   {
	iRet = ApplyFix ();
   }

  return iRet;
}  /* OrdinaryMotion */

CCoarseLevel::CCoarseLevel()
{
  long lFrame, lDim, lPair;
  REPAIR_DEBRIS *rdp;

/*
	Build the pick tables
*/

  BuildPickTable();

  for (lPair = 0; lPair < MAX_FRAME_PAIR; lPair++)
   {
    mpMotion[lPair] = NULL;
   }

  for (lDim = 0; lDim < MAX_DIM + 1; lDim++)
   {
    if (lDim < MAX_DIM)
     {
      rdp = rdaRepair + lDim;
     }
    else
     {
      rdp = &rdOrig;
     }

    rdp->lpRegionRowCol = NULL;
    rdp->lpSurroundRC = NULL;
    rdp->lpSurroundData = NULL;

    for (lFrame = 0; lFrame < MAX_FRAME; lFrame++)
     {
      rdp->uspMotData[lFrame] = NULL;
      rdp->fpSurroundScore[lFrame] = NULL;
     }

    rdp->ucpLabMask = NULL;
    rdp->lpSelectRowCol = NULL;
    rdp->lpSurroundRowCol = NULL;
   }

  return;
}  /* CCoarseLevel */

CCoarseLevel::~CCoarseLevel()
{
  Free();
}  /* ~CoarseLevel */

void CCoarseLevel::Free()
{
  long lFrame, lDim, lRow, lPair;
  REPAIR_DEBRIS *rdp;

  for (lPair = 0; lPair < MAX_FRAME_PAIR; lPair++)
   {
    MTIfree (mpMotion[lPair]);
    mpMotion[lPair] = NULL;
   }

  for (lDim = 0; lDim < MAX_DIM + 1; lDim++)
   {
    if (lDim < MAX_DIM)
     {
      rdp = rdaRepair + lDim;
     }
    else
     {
      rdp = &rdOrig;
     }

    MTIfree (rdp->lpRegionRowCol);
    rdp->lpRegionRowCol = NULL;
    MTIfree (rdp->lpSurroundRC);
    rdp->lpSurroundRC = NULL;
    MTIfree (rdp->lpSurroundData);
    rdp->lpSurroundData = NULL;

    for (lFrame = 0; lFrame < MAX_FRAME; lFrame++)
     {
      MTIfree (rdp->uspMotData[lFrame]);
      rdp->uspMotData[lFrame] = NULL;
      if (rdp->fpSurroundScore[lFrame])
       {
        for (lRow = rdp->lMinMotionRow; lRow <= rdp->lMaxMotionRow; lRow++)
         {
          rdp->fpSurroundScore[lFrame][lRow] += rdp->lMinMotionCol;
          MTIfree (rdp->fpSurroundScore[lFrame][lRow]);
          rdp->fpSurroundScore[lFrame][lRow] = NULL;
         }
        rdp->fpSurroundScore[lFrame] += rdp->lMinMotionRow;
        MTIfree (rdp->fpSurroundScore[lFrame]);
        rdp->fpSurroundScore[lFrame] = NULL;
       }
     }

    MTIfree (rdp->ucpLabMask);
    rdp->ucpLabMask = NULL;
    MTIfree (rdp->lpSelectRowCol);
    rdp->lpSelectRowCol = NULL;
    MTIfree (rdp->lpSurroundRowCol);
    rdp->lpSurroundRowCol = NULL;
   }

  return;
}  /* Free */

void CCoarseLevel::BuildPickTable ()
{
  long lPick, lIndex;
  long lSeed=1353857;

  for (lPick = 0; lPick < NUM_PICK_UNIQUE; lPick++)
   {
    for (lIndex = 0; lIndex < NUM_PICK_INDEX; lIndex++)
     {
      if (ran266(&lSeed) < 0.5)
       {
        laPickValue[lPick][lIndex] = PICK_A;
       }
      else
       {
        laPickValue[lPick][lIndex] = PICK_B;
       }
     }
   }

  return;
}  /* BuildPickTable */

int CRepairDRS::CopyInput ()
{
  long lFrame, lRow, lCol, lR, lC, lFineRow, lFineCol,
		/* lNNone, */ lNSelect, lNSurround, lDim;
  MTI_UINT8 *ucpLM;
  REPAIR_DEBRIS *rdp, *rdpOrig;
  int iRet;

  clp->lNDim = lNDim;
  clp->laDimension[0] = 1;
  for (lDim = 1; lDim < clp->lNDim; lDim++)
   {
    clp->laDimension[lDim] = clp->laDimension[lDim-1] * lDimension;
   }

  // only do motion estimation work on the selected region to improve speed
  lMotionULCRow = roi.getLabelBox().top - lOffsetRow;
  if (lMotionULCRow < lRegionULCRow)
    lMotionULCRow = lRegionULCRow;
  lMotionLRCRow = roi.getLabelBox().bottom + lOffsetRow;
  if (lMotionLRCRow > lRegionLRCRow)
    lMotionLRCRow = lRegionLRCRow;
  lMotionULCCol = roi.getLabelBox().left - lOffsetCol;
  if (lMotionULCCol < lRegionULCCol)
    lMotionULCCol = lRegionULCCol;
  lMotionLRCCol = roi.getLabelBox().right + lOffsetCol;
  if (lMotionLRCCol > lRegionLRCCol)
    lMotionLRCCol = lRegionLRCCol;
  long lNRowMot = lMotionLRCRow - lMotionULCRow;
  long lNColMot = lMotionLRCCol - lMotionULCCol;

  for (lDim = 0; lDim < clp->lNDim; lDim++)
   {
    rdp = clp->rdaRepair + lDim;

    rdp->lNRowMot = lNRowMot/clp->laDimension[lDim];
    rdp->lNColMot = lNColMot/clp->laDimension[lDim];

    // fix bug 2180
    // if motion is too small, clip dimensions;
    // in practice this happens near the edges of SD images - we end up
    // setting clp->lNDim to 4 in that case instead of the default 5
    //
    if (rdp->lNRowMot == 0 || rdp->lNColMot == 0)
     {
      clp->lNDim = lDim;
      break;
     }

#ifdef PRINT_OUT
{
char caMessage[128];
sprintf (caMessage, "lNRowMot: %ld  lNColMot: %ld  for lDim: %ld\n", rdp->lNRowMot, rdp->lNColMot, lDim);
TRACE_0 (errout << caMessage);
}
#endif

    rdp->lMinOffsetRow = floor(
		(float)(-lOffsetRow)/(float)clp->laDimension[lDim]);
    rdp->lMinOffsetCol = floor(
		(float)(-lOffsetCol)/(float)clp->laDimension[lDim]);
    rdp->lMaxOffsetRow = ceil(
		(float)lOffsetRow/(float)clp->laDimension[lDim]);
    rdp->lMaxOffsetCol = ceil(
		(float)lOffsetCol/(float)clp->laDimension[lDim]);

    for (lFrame = 0; lFrame < lNFrame; lFrame++)
     {
      if (rdp->uspMotData[lFrame] != NULL)
        return ERR_MEMORY;
      rdp->uspMotData[lFrame] = (MTI_UINT16 *) MTImalloc(
            rdp->lNColMot * rdp->lNRowMot * sizeof(MTI_UINT16));
      if (rdp->uspMotData[lFrame] == NULL && (rdp->lNColMot*rdp->lNRowMot))
        return ERR_ALLOC;
     }

    cis.lNRowMot = rdp->lNRowMot;
    cis.lNColMot = rdp->lNColMot;
    cis.lNJob = msCopyInput.iNJob;

    for (lFrame = 0; lFrame < lNFrame; lFrame++)
     {
      if (lDim == 0)
       {
		  cis.uspSource = uspData[lFrame] + (lMotionULCRow * lPitch +
		lMotionULCCol) * lNCom + lPrincipalCom;
        cis.lNFineCol = lPitch;
        cis.lNFineCom = lNCom;
        cis.lCoarse = 1;
       }
      else
       {
        cis.uspSource = clp->rdaRepair[lDim-1].uspMotData[lFrame];
        cis.lNFineCol = clp->rdaRepair[lDim-1].lNColMot;
        cis.lNFineCom = 1;
        cis.lCoarse = clp->laDimension[lDim]/clp->laDimension[lDim-1];
       }

      cis.lNCoarse = cis.lCoarse * cis.lCoarse;
		cis.uspDest = rdp->uspMotData[lFrame];

//      CHRTimer timer;
//      TRACE_0(errout << "RepairDRS COPYINPUT - start threads for frame " << lFrame);
//      DBTRACE(cis.lNRowMot);
//      DBTRACE(cis.lNColMot);
//      DBTRACE(cis.lNJob);
//      DBTRACE(cis.lCoarse);
//      DBTRACE(cis.lNCoarse);
//      DBTRACE(cis.lNFineCol);
//      DBTRACE(cis.lNFineCom);
//      DBTRACE(cis.uspDest);
//      DBTRACE(cis.uspSource);

      iRet = MThreadStart (&msCopyInput);

//      TRACE_0(errout << "RepairDRS COPYINPUT - completed, took " << timer.ReadAsString() << " msec");

      if (iRet)
        return iRet;
     }

    if (rdp->ucpLabMask != NULL)
      return ERR_MEMORY;
    rdp->ucpLabMask = (MTI_UINT8 *) MTImalloc(
             rdp->lNRowMot * rdp->lNColMot * sizeof(MTI_UINT8));
    if (rdp->ucpLabMask == NULL && (rdp->lNRowMot*rdp->lNColMot))
      return ERR_ALLOC;

    ucpLM = roi.getLabelPtr() +  lMotionULCRow * lPitch +
		lMotionULCCol;
    lFineRow = 0;
    for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
     {
      lFineCol = 0;
      for (lCol = 0; lCol < rdp->lNColMot; lCol++)
       {
/*
	If a region has 1 INTERIOR pixel, call interior
        else if it has 1 SURROUND pixel, call it surround
        else call it NONE
*/
        // lNNone = 0;      Not used
        lNSelect = 0;
        lNSurround = 0;
		for (lR = 0; lR < clp->laDimension[lDim]; lR++)
        for (lC = 0; lC < clp->laDimension[lDim]; lC++)
         {
          switch (ucpLM[(lFineRow+lR)*lPitch+(lFineCol+lC)] &
		(LABEL_INTERIOR|LABEL_SURROUND))
           {
            case LABEL_NONE:
              // lNNone++;       Not used
            break;
            case LABEL_INTERIOR:
              lNSelect++;
            break;
			case LABEL_SURROUND:
              lNSurround++;
            break;
           }
         }

        if (lNSelect)
          rdp->ucpLabMask[lRow * rdp->lNColMot + lCol] = LABEL_INTERIOR;
        else if (lNSurround)
          rdp->ucpLabMask[lRow * rdp->lNColMot + lCol] = LABEL_SURROUND;
        else
          rdp->ucpLabMask[lRow * rdp->lNColMot + lCol] = LABEL_NONE;

        lFineCol += clp->laDimension[lDim];
       }
      lFineRow += clp->laDimension[lDim];
     }
   }


/*
	If we will be using the gradient to estimate motion, copy over
	the uspMotData
*/

  if (bUseGradient)
   {
    rdp = clp->rdaRepair;
    rdpOrig = &clp->rdOrig;
    rdpOrig->lNRowMot = rdp->lNRowMot;
    rdpOrig->lNColMot = rdp->lNColMot;

    if (rdpOrig->ucpLabMask != NULL)
      return ERR_MEMORY;
    rdpOrig->ucpLabMask = (MTI_UINT8 *) MTImalloc(
             rdpOrig->lNRowMot * rdpOrig->lNColMot * sizeof(MTI_UINT8));
    if (rdpOrig->ucpLabMask == NULL && (rdpOrig->lNRowMot*rdpOrig->lNColMot))
      return ERR_ALLOC;

    memcpy(rdpOrig->ucpLabMask,rdp->ucpLabMask,
      rdpOrig->lNRowMot * rdpOrig->lNColMot * sizeof(MTI_UINT8));

    for (lFrame = 0; lFrame < lNFrame; lFrame++)
     {
      if (rdpOrig->uspMotData[lFrame] != NULL)
        return ERR_MEMORY;
      rdpOrig->uspMotData[lFrame] = (MTI_UINT16 *) MTImalloc(
               rdp->lNRowMot * rdp->lNColMot * sizeof(MTI_UINT16));
      if (rdpOrig->uspMotData[lFrame] == NULL && (rdp->lNRowMot*rdp->lNColMot))
        return ERR_ALLOC;

      for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
      for (lCol = 0; lCol < rdp->lNColMot; lCol++)
       {
        rdpOrig->uspMotData[lFrame][lRow*rdp->lNColMot + lCol] =
		rdp->uspMotData[lFrame][lRow*rdp->lNColMot + lCol];
       }
     }
   }

  return RET_SUCCESS;
}  /* CopyInput */

namespace
{

CSpinLock DebugLock;

}

void wtfCopyInput(COPY_INPUT_STRUCT *cisp, int iJob, long &lRow, long &lCol, long &lSum, long &lRowStart, long &lRowStop, long &lFineRow, long &lFineCol, MTI_UINT16 *&uspD, MTI_UINT16 *&uspS);

void DoCopyInput(void *vp, int iJob)
{
   long lRow, lCol, lSum, lRowStart, lRowStop, lFineRow, lFineCol;
   MTI_UINT16 *uspD, *uspS;
   COPY_INPUT_STRUCT *cisp;

   cisp = (COPY_INPUT_STRUCT*)vp;

   lRowStart = (iJob * cisp->lNRowMot) / cisp->lNJob;
   lRowStop = ((iJob + 1) * cisp->lNRowMot) / cisp->lNJob;

//   {
//      CAutoSpinLocker lock(DebugLock);
//      TRACE_0(errout << "------------------- JOB " << iJob << " -------------------");
//      DBTRACE(lRowStart);
//      DBTRACE(lRowStop);
//      TRACE_0(errout << "----------------------------------------------------------");
//   }


   int errorCode = 0;
   bool sehError = false;
   auto tryFilterException = [&errorCode](int code)
   {
      return EXCEPTION_EXECUTE_HANDLER;
   };

   CAutoErrorReporter autoErr("DRS Background Fix Thread", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   __try
   {
      try
      {
wtfCopyInput(cisp, iJob, lRow, lCol, lSum, lRowStart, lRowStop, lFineRow, lFineCol, uspD, uspS);
//         CHRTimer timer;
//         TRACE_0(errout << "Start job " << iJob);
//         for (lRow = lRowStart; lRow < lRowStop; lRow++)
//         {
//            lCol = 0;
//            uspD = cisp->uspDest + lRow * cisp->lNColMot + lCol;
//            for (; lCol < cisp->lNColMot; lCol++)
//            {
//               lSum = 0;
//               for (lFineRow = cisp->lCoarse * lRow; lFineRow < cisp->lCoarse * (lRow + 1); lFineRow++)
//               {
//                  lFineCol = cisp->lCoarse * lCol;
//                  uspS = cisp->uspSource + ((lFineRow * cisp->lNFineCol + lFineCol) * cisp->lNFineCom);
//
//             //   lSum += (long) * uspS;
//             //   uspS += cisp->lNFineCom;
//             //   lFineCol++;
//             //   lSum += (long) * uspS;
//             //   uspS += cisp->lNFineCom;
//             //   lFineCol++;
//                  for (; lFineCol < cisp->lCoarse * (lCol + 1); lFineCol++)
//                  {
//                     lSum += (long) * uspS;
//                     uspS += cisp->lNFineCom;
//                  }
//               }
//               *uspD++ = (float)lSum / (float)cisp->lNCoarse + 0.5;
//            }
//         }
//
//         TRACE_0(errout << "End job " << iJob << ", " << timer.ReadAsString() << " msec");
      }
      catch (const std::exception &ex)
      {
         TRACE_0(errout << "***ERROR**** DRS DoCopyInput THREAD CRASHED: " << ex.what());
         autoErr.errorCode = -10100;
         autoErr.msg << "DRS DoCopyInput THREAD CRASHED! Reason: " << endl << ex.what();
         {
            CAutoSpinLocker lock(DebugLock);
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXX JOB " << iJob << " XXXXXXXXXXXXXXXXXX");
            DBTRACE(lRow);
            DBTRACE(lCol);
            DBTRACE(lFineRow);
            DBTRACE(lFineCol);
            DBTRACE(uspS);
            DBTRACE(uspD);
            TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
         }
      }
   }
   __except (tryFilterException(GetExceptionCode()))
   {
      errorCode = GetExceptionCode();
      sehError = true;
   }

   // Any I/O in the __except could crash system
   if (sehError)
   {
      TRACE_0(errout << "***ERROR**** DRS DoCopyInput THREAD CRASHED: non-standard error: " << std::hex << errorCode);
      autoErr.errorCode = -10101;
      autoErr.msg << "DRS DoCopyInput THREAD CRASHED - reason: NON_STANDARD EXCEPTION: " << std::hex << errorCode;

      {
         CAutoSpinLocker lock(DebugLock);
         TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXX JOB " << iJob << " XXXXXXXXXXXXXXXXXX");
         DBTRACE(lRow);
         DBTRACE(lCol);
         DBTRACE(lFineRow);
         DBTRACE(lFineCol);
         DBTRACE(uspS);
         DBTRACE(uspD);
         TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
      }
   }
} /* DoCopyInput */

void CRepairDRS::GradientImage (long lDim)
{
  long lFrame, lRow, lCol, lDiff0, lDiff1;
  MTI_UINT16 *uspMotData, *uspMD0, *uspMD1, *uspMD2, *uspMD3;
  REPAIR_DEBRIS *rdp;

  rdp = clp->rdaRepair + lDim;

/*
	Run through all frames and replace lpMotData with the
	Roberts cross gradient at each pixel.

	MD0 is the motion data at (lRow,lCol)
	MD1 is the motion data at (lRow+2,lCol+2)
	MD2 is the motion data at (lRow+2,lCol)
	MD3 is the motion data at (lRow,lCol+2)
*/

  for (lFrame = 0; lFrame < lNFrame; lFrame++)
   {
    uspMotData = rdp->uspMotData[lFrame];
    for (lRow = 0; lRow < rdp->lNRowMot - 2; lRow++)
     {
      uspMD0 = uspMotData + (lRow * rdp->lNColMot);
      uspMD1 = uspMotData + ((lRow+2) * rdp->lNColMot + 2);
      uspMD2 = uspMotData + ((lRow+2) * rdp->lNColMot);
      uspMD3 = uspMotData + ((lRow) * rdp->lNColMot + 2);
      for (lCol = 0; lCol < rdp->lNColMot - 2; lCol++)
       {
        lDiff0 = (long)*uspMD0 - (long)*uspMD1++;
        if (lDiff0 < 0)
          lDiff0 = -lDiff0;

        lDiff1 = (long)*uspMD2++ - (long)*uspMD3++;
        if (lDiff1 < 0)
          lDiff1 = -lDiff1;

        if (lDiff0 > lDiff1)
          *uspMD0++ = lDiff0;
        else
          *uspMD0++ = lDiff1;
       }

      if (lCol > 0)
       {
        // fill in right side by copying from nearest column
        for ( ; lCol < rdp->lNColMot; lCol++)
         {
          *uspMD0 = uspMD0[-1];
          uspMD0++;
         }
       }
      else
       {
        // set to zero
        for (lCol = 0; lCol < rdp->lNColMot; lCol++)
         {
          *uspMD0++ = 0;
         }
       }
     }


    if (lRow > 0)
     {
      // fill in bottom rows by copying nearest row
      for ( ; lRow < rdp->lNRowMot; lRow++)
      for (lCol = 0; lCol < rdp->lNColMot; lCol++)
       {
        *uspMD0 = uspMD0[-rdp->lNColMot];
        uspMD0++;
       }
      }
     else
      {
       // set to zero
       uspMD0 = uspMotData;
       for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
       for (lCol = 0; lCol < rdp->lNColMot; lCol++)
        {
         *uspMD0++ = 0;
        }
      }
   }

  return;
}  /* GradientImage */

int CRepairDRS::MakeRegion (REPAIR_DEBRIS *rdp)
{
  long lRow, lCol, *lpRegionRowCol, *lpSurroundRC, lRowCol,
	lNRowCol, lMinRow, lMaxRow, lMinCol, lMaxCol;
  long *lpSurroundData;
  MTI_UINT8 *ucpLabMask, *ucpLM;

/*
	Count the number of entries in the Region and Surround
*/

  ucpLabMask = rdp->ucpLabMask;

#ifdef DEBUG_D
{
 long lRow, lCol, lRowCol;
 char caFName[64];
 FILE *fp;

   sprintf (caFName, "rep.dat");
   fp = fopen (caFName, "w");

  lRowCol = 0;
  for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
   {
  for (lCol = 0; lCol < rdp->lNColMot; lCol++)
   {
    fprintf (fp, "%d ", (int)ucpLabMask[lRowCol]);
    lRowCol++;
   }
  fprintf (fp, "\n");
  }
  fclose(fp);
}
#endif


/*
	Find first pixel used.  This is used to initialize
	min and maximum values used in motion estimation
*/
  lMinRow = -1;
  lNRowCol = rdp->lNRowMot * rdp->lNColMot;
  for (lRowCol = 0; lRowCol < lNRowCol && lMinRow == -1; lRowCol++)
   {
    if (ucpLabMask[lRowCol] & (LABEL_INTERIOR|LABEL_SURROUND))
     {
      lMinRow = lRowCol/rdp->lNColMot;
      lMinCol = lRowCol%rdp->lNColMot;
      lMaxRow = lRowCol/rdp->lNColMot;
      lMaxCol = lRowCol%rdp->lNColMot;
     }
   }

  if (lMinRow == -1)
    return REPAIR_ERROR_NO_USABLE_PIXELS;

  rdp->lNRegion = 0;
  rdp->lNSurround = 0;
  for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
  for (lCol = 0; lCol < rdp->lNColMot; lCol++)
   {
    lRowCol = lRow * rdp->lNColMot + lCol;
    if (ucpLabMask[lRowCol] & LABEL_INTERIOR)
     {
      rdp->lNRegion++;

      if (lRow < lMinRow)
        lMinRow = lRow;
      else if (lRow > lMaxRow)
        lMaxRow = lRow;
      if (lCol < lMinCol)
        lMinCol = lCol;
      else if (lCol > lMaxCol)
        lMaxCol = lCol;
     }
    else if (ucpLabMask[lRowCol] & LABEL_SURROUND)
     {
      rdp->lNSurround++;

      if (lRow < lMinRow)
        lMinRow = lRow;
      else if (lRow > lMaxRow)
        lMaxRow = lRow;
      if (lCol < lMinCol)
        lMinCol = lCol;
      else if (lCol > lMaxCol)
        lMaxCol = lCol;
     }
   }

  if (rdp->lpRegionRowCol != NULL)
    return ERR_MEMORY;
  rdp->lpRegionRowCol = (long *) MTImalloc(rdp->lNRegion * sizeof(long));
  if (rdp->lpRegionRowCol == NULL && rdp->lNRegion)
    return ERR_ALLOC;

  if (rdp->lpSurroundRC != NULL)
    return ERR_MEMORY;
  rdp->lpSurroundRC = (long *) MTImalloc(rdp->lNSurround * sizeof(long));
  if (rdp->lpSurroundRC == NULL && rdp->lNSurround)
    return ERR_ALLOC;
  if (rdp->lpSurroundData != NULL)
    return ERR_MEMORY;
  rdp->lpSurroundData = (long *) MTImalloc(rdp->lNSurround * sizeof(long));
  if (rdp->lpSurroundData == NULL && rdp->lNSurround)
    return ERR_ALLOC;

  lpRegionRowCol = rdp->lpRegionRowCol;
  lpSurroundRC = rdp->lpSurroundRC;
  lpSurroundData = rdp->lpSurroundData;

  ucpLM = ucpLabMask;
  for (lRow = 0; lRow < rdp->lNRowMot; lRow++)
  for (lCol = 0; lCol < rdp->lNColMot; lCol++)
   {
    if (*ucpLM & LABEL_INTERIOR)
     {
      *lpRegionRowCol++ = lRow*rdp->lNColMot + lCol;
     }
    else if (*ucpLM & LABEL_SURROUND)
     {
      lRowCol = lRow * rdp->lNColMot + lCol;
      *lpSurroundRC++ = lRowCol;
      *lpSurroundData++ = rdp->uspMotData[lRepairFrame][lRowCol];
     }
    ucpLM++;
   }

/*
	Set values MinMotion and MaxMotion
*/

  if (rdp->lMinOffsetRow + lMinRow < 0)
   {
    rdp->lMinMotionRow = -lMinRow;
   }
  else
   {
    rdp->lMinMotionRow = rdp->lMinOffsetRow;
   }
  if (rdp->lMaxOffsetRow + lMaxRow >= rdp->lNRowMot)
   {
    rdp->lMaxMotionRow = rdp->lNRowMot - lMaxRow - 1;
   }
  else
   {
    rdp->lMaxMotionRow = rdp->lMaxOffsetRow;
   }

  if (rdp->lMinOffsetCol + lMinCol < 0)
   {
    rdp->lMinMotionCol = -lMinCol;
   }
  else
   {
    rdp->lMinMotionCol = rdp->lMinOffsetCol;
   }
  if (rdp->lMaxOffsetCol+ lMaxCol >= rdp->lNColMot)
   {
    rdp->lMaxMotionCol = rdp->lNColMot - lMaxCol - 1;
   }
  else
   {
    rdp->lMaxMotionCol = rdp->lMaxOffsetCol;
   }

/*
	Allocate storage for Surround and Select addresses
*/

  rdp->lNSelectRowCol = 0;
  rdp->lNSurroundRowCol = 0;
  for (lRowCol = 0; lRowCol < rdp->lNRowMot * rdp->lNColMot; lRowCol++)
   {
    if (rdp->ucpLabMask[lRowCol] == LABEL_INTERIOR)
     {
      rdp->lNSelectRowCol++;
     }
    else if (rdp->ucpLabMask[lRowCol] == LABEL_SURROUND)
     {
      rdp->lNSurroundRowCol++;
     }
   }

  if (rdp->lpSelectRowCol != NULL)
    return ERR_MEMORY;
  rdp->lpSelectRowCol = (long *) MTImalloc(rdp->lNSelectRowCol * sizeof(long));
  if (rdp->lpSelectRowCol == NULL && rdp->lNSelectRowCol)
    return ERR_ALLOC;

  if (rdp->lpSurroundRowCol != NULL)
    return ERR_MEMORY;
  rdp->lpSurroundRowCol = (long *) MTImalloc(rdp->lNSurroundRowCol * sizeof(long));
  if (rdp->lpSurroundRowCol == NULL && rdp->lNSurroundRowCol)
    return ERR_ALLOC;

  rdp->lNSelectRowCol = 0;
  rdp->lNSurroundRowCol = 0;
  for (lRowCol = 0; lRowCol < rdp->lNRowMot * rdp->lNColMot; lRowCol++)
   {
    if (rdp->ucpLabMask[lRowCol] == LABEL_INTERIOR)
     {
      rdp->lpSelectRowCol[rdp->lNSelectRowCol++] = lRowCol;
     }
    else if (rdp->ucpLabMask[lRowCol] == LABEL_SURROUND)
     {
      rdp->lpSurroundRowCol[rdp->lNSurroundRowCol++] = lRowCol;
     }
   }

  return RET_SUCCESS;
}  /* MakeRegion */


int CRepairDRS::CalcSurroundScore (REPAIR_DEBRIS *rdp)
{
  long lFrame, lNAlloc, lMotionRow, lMotionCol;
  int iRet;

/*
	Allocate storage for the surround scores
*/

  for (lFrame = 0; lFrame < lNFrame; lFrame++)
  if (lFrame != lRepairFrame)
   {
    lNAlloc = (rdp->lMaxMotionRow - rdp->lMinMotionRow + 1) * sizeof (float *);
    if (rdp->fpSurroundScore[lFrame] != NULL)
      return ERR_MEMORY;
    rdp->fpSurroundScore[lFrame] = (float **) MTImalloc(lNAlloc);
    if (rdp->fpSurroundScore[lFrame] == NULL && lNAlloc)
      return ERR_ALLOC;

    rdp->fpSurroundScore[lFrame] -= rdp->lMinMotionRow;
    for (lMotionRow = rdp->lMinMotionRow; lMotionRow <= rdp->lMaxMotionRow;
		lMotionRow++)
     {
      rdp->fpSurroundScore[lFrame][lMotionRow] = NULL;
     }

    for (lMotionRow = rdp->lMinMotionRow; lMotionRow <= rdp->lMaxMotionRow;
		lMotionRow++)
     {
      lNAlloc = (rdp->lMaxMotionCol - rdp->lMinMotionCol + 1) * sizeof (float);

      if (rdp->fpSurroundScore[lFrame][lMotionRow] != NULL)
        return ERR_MEMORY;
      rdp->fpSurroundScore[lFrame][lMotionRow] = (float *) MTImalloc(lNAlloc);
      if (rdp->fpSurroundScore[lFrame][lMotionRow] == NULL && lNAlloc)
        return ERR_ALLOC;

      rdp->fpSurroundScore[lFrame][lMotionRow] -= rdp->lMinMotionCol;
     }
   }

/*
	Compute difference between lRepairFrame and each other frame
*/

  csss.lNSurround = rdp->lNSurround;
  csss.lNJob = msCalcSurroundScore.iNJob;
  for (lFrame = 0; lFrame < lNFrame; lFrame++)
  if (lFrame != lRepairFrame)
   {
    csss.uspMotData = rdp->uspMotData[lFrame];
    for (lMotionRow = rdp->lMinMotionRow; lMotionRow <= rdp->lMaxMotionRow;
		lMotionRow++)
    for (lMotionCol = rdp->lMinMotionCol; lMotionCol <= rdp->lMaxMotionCol;
		lMotionCol++)
     {
      csss.lMotionRC = lMotionRow * rdp->lNColMot + lMotionCol;
      csss.lpSurroundRC = rdp->lpSurroundRC;
      csss.lpSurroundData = rdp->lpSurroundData;

      iRet = MThreadStart (&msCalcSurroundScore);
      if (iRet)
       {
        return iRet;
       }

      if (csss.lNSurround)
       {
        rdp->fpSurroundScore[lFrame][lMotionRow][lMotionCol] =
		(float)csss.lSum/(float)csss.lNSurround;
       }
      else
       {
        rdp->fpSurroundScore[lFrame][lMotionRow][lMotionCol] = INFINITY_FLOAT;
       }
     }
#ifdef DEBUG_D
{
 char caFName[64];
 FILE *fp;

   sprintf (caFName, "ss%d.dat", lFrame);
   fp = fopen (caFName, "w");

  for (lMotionRow = rdp->lMinMotionRow; lMotionRow <= rdp->lMaxMotionRow;
		lMotionRow++)
   {
  for (lMotionCol = rdp->lMinMotionCol; lMotionCol <= rdp->lMaxMotionCol;
		lMotionCol++)
   {
    fprintf (fp, "%f ", rdp->fpSurroundScore[lFrame][lMotionRow][lMotionCol]);
   }
  fprintf (fp, "\n");
  }
  fclose(fp);
}
#endif
   }

  return RET_SUCCESS;
}  /* CalcSurroundScore */

void DoCalcSurroundScore (void *vp, int iJob)
{
  long lSurround, lSurroundStart, lSurroundStop, lSum, lRowCol, lDiff,
	*lpSurroundRC, *lpSurroundData, lMotionRC, lStop;
  CALC_SURROUND_SCORE_STRUCT *csssp;
  MTI_UINT16 *uspMotData;

  csssp = (CALC_SURROUND_SCORE_STRUCT *)vp;

  lSurroundStart = (iJob * csssp->lNSurround) / csssp->lNJob;
  lSurroundStop = ((iJob + 1) * csssp->lNSurround) / csssp->lNJob;

  lpSurroundRC = csssp->lpSurroundRC + lSurroundStart;
  lpSurroundData = csssp->lpSurroundData + lSurroundStart;

  uspMotData = csssp->uspMotData;
  lMotionRC = csssp->lMotionRC;

  lSum = 0;
  lStop = lSurroundStop - lSurroundStart;
  lStop /= 10;
  lStop *= 10;
  lStop += lSurroundStart;
  for (lSurround = lSurroundStart; lSurround < lStop; lSurround+=10)
   {
		// 0
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

	 if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 1
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 2
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 3
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 4
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 5
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 6
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 7
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 8
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 9
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;
   }

  for ( ; lSurround < lSurroundStop; lSurround++)
   {
    lRowCol = *lpSurroundRC++;
    lDiff = *lpSurroundData++ - (long)uspMotData[lRowCol + lMotionRC];

    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;
   }

  csssp->laSum[iJob] = lSum;
}  /* DoCalcSurroundScore */

void FinishCalcSurroundScore (void *vp)
{
  CALC_SURROUND_SCORE_STRUCT *csssp;
  long lJob;

  csssp = (CALC_SURROUND_SCORE_STRUCT *) vp;
  csssp->lSum = 0;
  for (lJob = 0; lJob < csssp->lNJob; lJob++)
   {
    csssp->lSum += csssp->laSum[lJob];
   }
}  /* FinishCalcSurroundScore */


int CRepairDRS::CreateMotionPairs ()
{
  long lPair, lFrame, lTime0, lTime1, lTime2, lT0, lT1,
		lMinRow0, lMinCol0, lMaxRow0, lMaxCol0, lFrame0,
		lMinRow1, lMinCol1, lMaxRow1, lMaxCol1, lFrame1,
		lRow0, lRow1, lCol0, lCol1, laFramePair[MAX_FRAME_PAIR][2],
		lF0, lF1;
  long lMinTime, lTime, lP;
  float **fpSurroundScore0, **fpSurroundScore1, fFactor;
  MOTION *mpMotion;
  REPAIR_DEBRIS *rdp;
  bool bAlreadySeen;

  rdp = clp->rdaRepair + clp->lNDim - 1;

/*
	Motion is usally determined by comparing the RepairFrame to a
	several pairs of reference frames.

	Determine the number of pairs to use.

	A special case is when we import from a single frame
*/


  if (lNFrame == 2)	// import from a single frame
   {
    clp->lNPair = 1;
/*
	Find the import frame
*/
    for (lFrame = 0; lFrame < lNFrame; lFrame++)
     {
      if (lFrame != lRepairFrame)
        clp->laFrame0[0] = lFrame;
     }

    clp->laFrame1[0] = clp->laFrame0[0];
    clp->faFactor[0] = 1.;
   }
  else if (lNFrame == 3 || lNFrame == 4 || lNFrame == 5)
   {
    clp->lNPair = lNFrame - 2;

/*
	Make a list of the of the various frame pair combinations.
	We always select the pair combinations that are the closest to
	the RepairFrame.
*/

    lPair = 0;
    while (lPair < clp->lNPair)
     {
		// find the closest pair that has not yet been used

      lMinTime = -1;
      for (lF0 = 0; lF0 < lNFrame; lF0++)
      for (lF1 = lF0+1; lF1 < lNFrame; lF1++)
      if (lF0 != lRepairFrame && lF1 != lRepairFrame)
       {
        lTime0 = laTemporalIndex[lRepairFrame] - laTemporalIndex[lF0];
        lTime1 = laTemporalIndex[lRepairFrame] - laTemporalIndex[lF1];
        lTime2 = laTemporalIndex[lF1] - laTemporalIndex[lF0];
        if (lTime0 < 0)
          lTime = -lTime0;
        else
          lTime = lTime0;

        if (lTime1 < 0)
          lTime -= lTime1;
        else
          lTime += lTime1;

        if (lTime2 < 0)
          lTime -= lTime2;
        else
          lTime += lTime2;

        if (lTime < lMinTime || lMinTime == -1)
         {
		// have we already seen this combination?

          bAlreadySeen = false;
          for (lP = 0; lP < lPair; lP++)
           {
            if (laFramePair[lP][0] == lF0 && laFramePair[lP][1] == lF1)
              bAlreadySeen = true;
           }

          if (bAlreadySeen == false)
           {
            lMinTime = lTime;
            laFramePair[lPair][0] = lF0;
            laFramePair[lPair][1] = lF1;
           }
         }
       }

      lPair++;
     }

	// sort the pairs so that laFrame0 is farther to RepairFrame

    for (lPair = 0; lPair < clp->lNPair; lPair++)
     {
      lTime0 = laTemporalIndex[laFramePair[lPair][0]] -
		laTemporalIndex[lRepairFrame];
      lTime1 = laTemporalIndex[laFramePair[lPair][1]] -
		laTemporalIndex[lRepairFrame];
      lT0 = lTime0;
      lT1 = lTime1;
      if (lT0 < 0)
        lT0 = -lT0;
      if (lT1 < 0)
        lT1 = -lT1;

      if (lT0 > lT1)
       {
        clp->laFrame0[lPair] = laFramePair[lPair][0];
        clp->laFrame1[lPair] = laFramePair[lPair][1];
        clp->faFactor[lPair] = (float)lTime1 / (float)lTime0;
       }
      else
       {
        clp->laFrame0[lPair] = laFramePair[lPair][1];
        clp->laFrame1[lPair] = laFramePair[lPair][0];
        clp->faFactor[lPair] = (float)lTime0 / (float)lTime1;
       }
     }
   }
  else
   {
    return REPAIR_ERROR_BAD_NUMBER_FRAMES;
   }

/*
	Count the number of motion pairs
*/

  for (lPair = 0; lPair < clp->lNPair; lPair++)
   {
    lMinRow0 = rdp->lMinMotionRow;
    lMaxRow0 = rdp->lMaxMotionRow;
    lMinCol0 = rdp->lMinMotionCol;
    lMaxCol0 = rdp->lMaxMotionCol;

    clp->laNMotion[lPair] = (lMaxRow0-lMinRow0+1) * (lMaxCol0-lMinCol0+1);

    if (clp->mpMotion[lPair] != NULL)
      return ERR_MEMORY;
    clp->mpMotion[lPair] = (MOTION *) MTImalloc(clp->laNMotion[lPair] * sizeof(MOTION));
    if (clp->mpMotion[lPair] == NULL && clp->laNMotion[lPair])
      return ERR_ALLOC;
   }

/*
	Enter values into the mpMotion array
*/

  for (lPair = 0; lPair < clp->lNPair; lPair++)
   {
    lFrame0 = clp->laFrame0[lPair];
    lFrame1 = clp->laFrame1[lPair];
    fFactor = clp->faFactor[lPair];
    mpMotion = clp->mpMotion[lPair];

    lMinRow0 = rdp->lMinMotionRow;
    lMaxRow0 = rdp->lMaxMotionRow;
    lMinCol0 = rdp->lMinMotionCol;
    lMaxCol0 = rdp->lMaxMotionCol;
    lMinRow1 = rdp->lMinMotionRow;
    lMaxRow1 = rdp->lMaxMotionRow;
    lMinCol1 = rdp->lMinMotionCol;
    lMaxCol1 = rdp->lMaxMotionCol;

    fpSurroundScore0 = rdp->fpSurroundScore[lFrame0];
    fpSurroundScore1 = rdp->fpSurroundScore[lFrame1];

    for (lRow0 = lMinRow0; lRow0 <= lMaxRow0; lRow0++)
    for (lCol0 = lMinCol0; lCol0 <= lMaxCol0; lCol0++)
     {
      lRow1 = NINT(lRow0 * fFactor);
      lCol1 = NINT(lCol0 * fFactor);
      if (lRow1 < lMinRow1)
        lRow1 = lMinRow1;
      else if (lRow1 > lMaxRow1)
        lRow1 = lMaxRow1;

      if (lCol1 < lMinCol1)
        lCol1 = lMinCol1;
      else if (lCol1 > lMaxCol1)
        lCol1 = lMaxCol1;

      mpMotion->lMotionRow0 = lRow0;
      mpMotion->lMotionCol0 = lCol0;
      mpMotion->lMotionRow1 = lRow1;
      mpMotion->lMotionCol1 = lCol1;
      mpMotion->fSurroundScore = (fpSurroundScore0[lRow0][lCol0] +
		fpSurroundScore1[lRow1][lCol1]) / 2.0;
      mpMotion++;
     }

#ifdef DEBUG_D
{
 char caFName[64];
 FILE *fp;

   sprintf (caFName, "ps%d.dat", lPair);
   fp = fopen (caFName, "w");

  mpMotion = clp->mpMotion[lPair];
  for (lRow0 = lMinRow0; lRow0 <= lMaxRow0; lRow0++)
   {
  for (lCol0 = lMinCol0; lCol0 <= lMaxCol0; lCol0++)
   {
    fprintf (fp, "%f ", mpMotion->fSurroundScore);
    mpMotion++;
   }
  fprintf (fp, "\n");
  }
  fclose(fp);
}
#endif
   }

  return RET_SUCCESS;
}  /* CreateMotionPairs */

int CRepairDRS::FindBestMotion ()
{
  long lKeepGoing, lBestPair, lBestSurround, lPair, lNMotion,
		lMotion;
#ifdef PRINT_OUT
  long lCnt;
#endif
  float fBestScore, fBestSurroundScore;
  int iRet;
  MOTION *mpMotion;
  REPAIR_DEBRIS *rdp;

  rdp = clp->rdaRepair + clp->lNDim - 1;

/*
	The idea is to find the best SurroundScore and then compute
	the corresponding region score.

	Once the best SurroundScore is already too large, quit
*/


  for (lPair = 0; lPair < clp->lNPair; lPair++)
   {
    lNMotion = clp->laNMotion[lPair];
    lKeepGoing = TRUE;
    fBestScore = INFINITY_FLOAT;
    lBestPair = 0;
#ifdef PRINT_OUT
    lCnt = 0;
#endif

    do
     {
/*
	Find the best SurroundScore
*/

      mpMotion = clp->mpMotion[lPair];
      fBestSurroundScore = mpMotion->fSurroundScore;
      lBestSurround = 0;
      for (lMotion = 0; lMotion < lNMotion; lMotion++)
       {
        if (mpMotion->fSurroundScore < fBestSurroundScore)
         {
          fBestSurroundScore = mpMotion->fSurroundScore;
          lBestSurround = lMotion;
         }
        mpMotion++;
       }

      if (fBestSurroundScore >= fBestScore)
       {
        lKeepGoing = FALSE;
       }
      else
       {
/*
	Calculate the region score
*/

        iRet = RegionScore(rdp, lBestSurround, lPair);
        if (iRet)
          return iRet;

        clp->mpMotion[lPair][lBestSurround].fSurroundScore = INFINITY_FLOAT;
        if (clp->mpMotion[lPair][lBestSurround].fScore < fBestScore)
         {
          fBestScore = clp->mpMotion[lPair][lBestSurround].fScore;
          lBestPair = lBestSurround;
         }
       }
#ifdef PRINT_OUT
      lCnt++;
#endif
     } while (lKeepGoing);

#ifdef PRINT_OUT
{
char caMessage[128];
sprintf (caMessage, "Visited %ld pairs out of %ld\n", lCnt, lNMotion);
TRACE_0 (errout << caMessage);
}
#endif

    clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow0  =
		clp->mpMotion[lPair][lBestPair].lMotionRow0;
    clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol0  =
		clp->mpMotion[lPair][lBestPair].lMotionCol0;
    clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow1  =
		clp->mpMotion[lPair][lBestPair].lMotionRow1;
    clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol1  =
		clp->mpMotion[lPair][lBestPair].lMotionCol1;
    clp->maBestMotion[clp->lNDim - 1][lPair].fScore = fBestScore;

#ifdef PRINT_OUT
{
char caMessage[128];
if (clp->maBestMotion[clp->lNDim - 1][lPair].fScore == INFINITY_FLOAT)
sprintf (caMessage, "The best motion is INFINITY (%ld, %ld) (%ld, %ld)\n",
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow0,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol0,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow1,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol1);
else
sprintf (caMessage, "The best motion is %f (%ld, %ld) (%ld, %ld)\n",
	clp->maBestMotion[clp->lNDim - 1][lPair].fScore,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow0,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol0,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionRow1,
	clp->maBestMotion[clp->lNDim - 1][lPair].lMotionCol1);

TRACE_0 (errout << caMessage);
}
#endif
   }

  return RET_SUCCESS;
}  /* FindBestMotion */

int CRepairDRS::RegionScore (REPAIR_DEBRIS *rdp, long lIndex, long lPair)
{
  long lFrame0, lFrame1, lRow0, lCol0, lRow1, lCol1, lTimeDiff;
  MOTION *mpMotion;
  int iRet;

  mpMotion = clp->mpMotion[lPair] + lIndex;

  lFrame0 = clp->laFrame0[lPair];
  lFrame1 = clp->laFrame1[lPair];
  lRow0 = mpMotion->lMotionRow0;
  lCol0 = mpMotion->lMotionCol0;
  lRow1 = mpMotion->lMotionRow1;
  lCol1 = mpMotion->lMotionCol1;

  if (lFrame0 == lFrame1)
    return 0;	/* no region computation when importing from single frame */

/*
	Compute difference between Region portion of two frames
*/

  rss.uspMotData0 = rdp->uspMotData[lFrame0] + (lRow0 * rdp->lNColMot + lCol0);
  rss.uspMotData1 = rdp->uspMotData[lFrame1] + (lRow1 * rdp->lNColMot + lCol1);

  rss.lNRegion = rdp->lNRegion;
  rss.lpRegionRowCol = rdp->lpRegionRowCol;

  rss.lNJob = msRegionScore.iNJob;

  iRet = MThreadStart (&msRegionScore);
  if (iRet)
   {
    return iRet;
   }

  lTimeDiff = laTemporalIndex[lFrame0] - laTemporalIndex[lFrame1];
  if (lTimeDiff < 0)
    lTimeDiff = -lTimeDiff;

  if (rss.lNRegion * lTimeDiff > 0)
   {
    mpMotion->fScore = mpMotion->fSurroundScore +
		((float)rss.lSum / (float)(rss.lNRegion * lTimeDiff));
   }
  else
   {
    mpMotion->fScore = mpMotion->fSurroundScore;
   }

  return 0;
}  /* RegionScore */

void DoRegionScore (void *vp, int iJob)
{
  long lRegion, lRegionStart, lRegionStop, lDiff, lSum, *lpRegionRowCol,
		lStop;
  REGION_SCORE_STRUCT *rssp;
  MTI_UINT16 *uspMotData0, *uspMotData1;

  rssp = (REGION_SCORE_STRUCT *) vp;

  lRegionStart = (iJob * rssp->lNRegion) / rssp->lNJob;
  lRegionStop = ((iJob+1) * rssp->lNRegion) / rssp->lNJob;

  lpRegionRowCol = rssp->lpRegionRowCol + lRegionStart;

  uspMotData0 = rssp->uspMotData0;
  uspMotData1 = rssp->uspMotData1;

  lSum = 0;
  lStop = lRegionStop - lRegionStart;
  lStop /= 10;
  lStop *= 10;
  lStop += lRegionStart;
  for (lRegion = lRegionStart; lRegion < lStop; lRegion+=10)
   {
		// 0
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 1
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 2
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 3
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 4
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 5
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 6
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 7
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 8
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;

		// 9
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;
   }

  for ( ; lRegion < lRegionStop; lRegion++)
   {
    lDiff = (long)uspMotData0[*lpRegionRowCol] -
		(long)uspMotData1[*lpRegionRowCol];
    lpRegionRowCol++;
    if (lDiff < 0)
      lSum -= lDiff;
    else
      lSum += lDiff;
   }

  rssp->laSum[iJob] = lSum;
}  /* DoRegionScore */

void FinishRegionScore (void *vp)
{
  REGION_SCORE_STRUCT *rssp;
  long lJob;

  rssp = (REGION_SCORE_STRUCT *) vp;
  rssp->lSum = 0;
  for (lJob = 0; lJob < rssp->lNJob; lJob++)
   {
    rssp->lSum += rssp->laSum[lJob];
   }
}  /* FinishRegionScore */

int CRepairDRS::HardMotion ()
{
  int iRet;

  // copy the original to initialize the result
  CopyImage (uspDefaultResult, uspData[lRepairFrame]);

/*
	If the debug flag is true, replace the defective region with
	fill values.
*/

  if (bDebugFlag)
   {
    UseFillValue ();
   }
  else
   {
    if (lRepairType == RT_HARD_MOTION)
     {
      iRet = HardMotionSmoothing();
      if (iRet)
        return iRet;
     }
    else if (lRepairType == RT_HARD_MOTION_2)
     {
      iRet = HardMotionEdgePreserving();
      if (iRet)
        return iRet;
     }
    else
     {
      return REPAIR_ERROR_UNKNOWN_REPAIR_TYPE;
     }
   }

  return 0;
}  /* HardMotion */

int CRepairDRS::HardMotionSmoothing ()
/*
	This function replaces the defective region with a
	weighted average of pixels on the boundary of the
	defective region.

	The weights are based on distance.  Closer pixels
	on the boundary have a higher weight.
*/
{
  long lRow, lCol, *lpTmp, lR, lC;
  MTI_UINT8 *ucpLM;
  bool bBoundaryFlag;
  int iRet;

/*
	Make a list of the boundary pixels.
*/

  lNBoundary = 0;
  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    ucpLM = roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol);
    for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
     {
      // find all pixels on the edge of the selected region
      if ((*ucpLM++ & LABEL_INTERIOR)  == 0)
       {
        bBoundaryFlag = false;
        for (lR = -1; lR <= 1; lR++)
        for (lC = -1; lC <= 1; lC++)
         {
          if (
		(lRow+lR >= lRegionULCRow) &&
		(lRow+lR < lRegionLRCRow) &&
		(lCol+lC >= lRegionULCCol) &&
		(lCol+lC < lRegionLRCCol) &&
		(roi.getLabelPtr()[(lRow+lR)*lPitch + (lCol+lC)] &
				LABEL_INTERIOR)
             )
           {
            bBoundaryFlag = true;
           }
         }

        if (bBoundaryFlag)
         {
          if (lNBoundary == lNBoundaryAlloc)
           {
            lNBoundaryAlloc += BOUNDARY_ALLOC;
            lpTmp = (long *) realloc (lpBoundaryRow,
			lNBoundaryAlloc * sizeof(long));
            if (lpTmp == NULL)
             {
              return ERR_ALLOC;
             }
            lpBoundaryRow = lpTmp;

            lpTmp = (long *) realloc (lpBoundaryCol,
			lNBoundaryAlloc * sizeof(long));
            if (lpTmp == NULL)
             {
              return ERR_ALLOC;
             }
            lpBoundaryCol = lpTmp;

            lpTmp = (long *) realloc (lpBoundaryData,
			lNBoundaryAlloc * sizeof(long) * lNCom);
            if (lpTmp == NULL)
             {
              return ERR_ALLOC;
             }
            lpBoundaryData = lpTmp;

           }

          lpBoundaryRow[lNBoundary] = lRow;
          lpBoundaryCol[lNBoundary] = lCol;
          for (long lCom = 0; lCom < lNCom; lCom++)
           {
            lpBoundaryData[lNCom*lNBoundary + lCom] =
		uspData[lRepairFrame][(lRow * lPitch + lCol) * lNCom + lCom];
           }
          lNBoundary++;
         }
       }
     }
   }

  iRet = MThreadStart (&msHardMotionSmoothing);
  if (iRet)
    return iRet;

  return 0;
}  /* HardMotionSmoothing */

void CallDoHardMotionSmoothing (void *vp, int iJob)
{
  CRepairDRS *rdp;

  rdp = (CRepairDRS *) vp;

  rdp->DoHardMotionSmoothing (iJob);

}  /* CallDoHardMotionSmoothing */

void CRepairDRS::DoHardMotionSmoothing (int iJob)
/*
	This function replaces the defective region with a
	weighted average of pixels on the boundary of the
	defective region.

	The weights are based on distance.  Closer pixels
	on the boundary have a higher weight.
*/
{
  long lRow, lCol, lBoundary, lCom, *lpBR, *lpBC, *lpBD;
  MTI_UINT16 *uspRes, *uspD;
  MTI_UINT8 *ucpLM;
  float fTotalDist, faSum[MAX_COMPONENT], fDist;
  long lNum, lStart, lStop;

/*
	For each INTERIOR pixel, find the weighted average of the
	boundaries.
*/

  lNum = lRegionLRCRow - lRegionULCRow;
  lStart = (iJob * lNum) / msHardMotionSmoothing.iNJob;
  lStop = ((iJob+1) * lNum) / msHardMotionSmoothing.iNJob;

  for (lRow = lRegionULCRow + lStart; lRow < lRegionULCRow + lStop; lRow++)
   {
    ucpLM = roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol);
    uspRes = uspDefaultResult + (lRow * lPitch + lRegionULCCol) * lNCom;
    for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
     {
      if (*ucpLM & LABEL_INTERIOR)
       {
        fTotalDist = 0.;
        for (lCom = 0; lCom < lNCom; lCom++)
         {
          faSum[lCom] = 0.;
         }
        lpBR = lpBoundaryRow;
        lpBC = lpBoundaryCol;
        lpBD = lpBoundaryData;
        for (lBoundary = 0; lBoundary < lNBoundary; lBoundary++)
         {
          fDist = CalcDist (lRow, lCol, *lpBR, *lpBC);
          if (fDist > 0.)
           {
            // take square of fDist to give more weight to nearby pixels
            fDist = 1. / (fDist * fDist);
            fTotalDist += fDist;
            for (lCom = 0; lCom < lNCom; lCom++)
             {
              faSum[lCom] += fDist * (*lpBD++);
             }
            lpBR++;
            lpBC++;
           }
         }

        for (lCom = 0; lCom < lNCom; lCom++)
         {
          if (fTotalDist > 0.)
           {
            *uspRes++ = (faSum[lCom] / fTotalDist) + 0.5;
           }
          else
           {
            // this will only happen on a full frame hard motion
            *uspRes++ = laFillValue[lCom];
           }
         }
       }
      else
       {
        uspRes += lNCom;
       }
      ucpLM++;
     }
   }

  return;
}  /* DoHardMotionSmoothing */

float CRepairDRS::CalcDist (long lRow0, long lCol0, long lRow1, long lCol1)
{
  float fDist;
  long lDiff;

  fDist = 0.;

  lDiff = lRow1 - lRow0;
  if (lDiff < 0)
    fDist -= (float)lDiff;
  else
    fDist += (float)lDiff;

  lDiff = lCol1 - lCol0;
  if (lDiff < 0)
    fDist -= (float)lDiff;
  else
    fDist += (float)lDiff;

  return fDist;
}  /* CalcDist */

/*
 * New Hard Motion Algorithm
 * Added by: Kevin Kochanek [02/01/2002]
 * Algorithm Description: variant of harmonic inpainting.
 * Minimize the functional J[u(x,y)] = Int{|grad*u(x,y)|dxdy}
 * over the defect region D with u constrained to be the image
 * outside of D. The following is a numerical scheme for solving
 * the associated Euler-Lagrange equations.
 */
int CRepairDRS::HardMotionEdgePreserving()
{
	int iRet;
	long lRow, lCol, lCom, lIter;
	MTI_UINT16 *uspRes, *uspD;
	MTI_UINT8 *ucpLM;
	long lBuffer;
	float *fpCenter;

	/* set the initial condition */

	// QQQ THIS IS BOGUS! WHY DOES IT ALLOCATE THIS BUFFER PIECEMEAL
	// FOR FULL FRAME 4K FIX, IT WILL ALLOCATE 150 MB BY REALLOC'ING
	// 40,000 BYTES AT A TIME 3,750 TIMES !!!!

#ifdef __EXTREME_BOGOSITY___

	float *fpTmp;
	lBuffer = 0;
	for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
	{
		uspD = &uspData[lRepairFrame][(lRow * lPitch + lRegionULCCol) * lNCom];
		for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
		{
			for (lCom = 0; lCom < lNCom; lCom++)
			{
				if (lBuffer == lNBufferAlloc)
				{
					lNBufferAlloc += BUFFER_ALLOC;
					fpTmp = (float *)realloc(fpBuffer, lNBufferAlloc*sizeof(float));
					if (fpTmp == NULL)
					{
						return ERR_ALLOC;
					}
					fpBuffer = fpTmp;
				}
				fpBuffer[lBuffer++] = ((float)(*uspD++));
			}
		}
	}

#else // Much quicker

	lBuffer = 0;

	// Make sure the existing floating point sub-image buffer is large enough
	// for this fix
	long lAllocNeeded = lNCom * (lRegionLRCRow - lRegionULCRow) * (lRegionLRCCol - lRegionULCCol);
	if (lAllocNeeded > lNBufferAlloc)
	{
		// need to increase the size of the buffer...
		// Do NOT realloc because we don't care about the buffer's contents
		lNBufferAlloc = 0;
		MTIfree(fpBuffer);
		fpBuffer = (float *) MTImalloc(lAllocNeeded*sizeof(float));
		if (fpBuffer == NULL)
		{
			return ERR_ALLOC;
		}
		lNBufferAlloc = lAllocNeeded;
	}

	// Copy the subimage, converting each componet to floating point
	for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
	{
		uspD = &uspData[lRepairFrame][(lRow * lPitch + lRegionULCCol) * lNCom];
		for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
		{
			for (lCom = 0; lCom < lNCom; lCom++)
			{
				fpBuffer[lBuffer++] = imageDatumToFloat(*uspD++);
			}
		}
	}

#endif

	/* fpBuffer has:
	 * height = lRegionLRCRow - lRegionULCRow
	 * width = lRegionLRCCol - lRegionULCCol
	 * fields = lNCom
	 ********************** */

	/* iterate the scheme */

	for (lIter = 1; lIter <= lHardMotionIters; lIter++)
	{
		iRet = MThreadStart(&msHardMotionEdgePreserving);
		if (iRet)
		{
			return iRet;
		}
	}

	/* return the result */

	fpCenter = fpBuffer;
	for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
	{
		ucpLM = roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol);
		uspRes = uspDefaultResult + (lRow * lPitch + lRegionULCCol) * lNCom;
		for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++, ucpLM++)
		{
			if (*ucpLM & LABEL_INTERIOR)
			{
				for (lCom = 0; lCom < lNCom; lCom++)
				{
					////*uspRes++ = *fpCenter++ + 0.5;
					*uspRes++ = floatToImageDatum(*fpCenter++);
				} /* lCom */
			} /* check RM: if */
			else
			{
				uspRes += lNCom;
				fpCenter += lNCom;
			} /* check RM: else */
		} /* lCol */
	} /* lRow */

	return 0;
} /* HardMotionEdgePreserving */

void CallDoHardMotionEdgePreserving (void *vp, int iJob)
{
  CRepairDRS *rdp;

  rdp = (CRepairDRS *) vp;

  rdp->DoHardMotionEdgePreserving (iJob);

}  /* CallDoHardMotionEdgePreserving */

void CRepairDRS::DoHardMotionEdgePreserving (int iJob)
{
  long lNum, lStart, lStop;
  long lRow,lCol,lCom;
  long xpitch,ypitch;
  float fEast,fWest,fNorth,fSouth,fNorm,fsqrarg,stabilizer=1e-8;
  MTI_UINT8 *ucpLM;
  float *fpCenter, *fpEast, *fpWest, *fpNorth, *fpSouth, *fpNorthEast,
		*fpNorthWest, *fpSouthEast, *fpSouthWest;

  xpitch = lNCom;
  ypitch = (lRegionLRCCol - lRegionULCCol)*lNCom;

  lNum = lRegionLRCRow - lRegionULCRow;
  lStart = (iJob * lNum) / msHardMotionEdgePreserving.iNJob;
  lStop = ((iJob+1) * lNum) / msHardMotionEdgePreserving.iNJob;

  fpCenter = fpBuffer + lStart * lNCom * (lRegionLRCCol - lRegionULCCol);

  for (lRow = lRegionULCRow+lStart; lRow < lRegionULCRow+lStop; lRow++)
   {
    ucpLM = roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol);
    for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
     {
      if (*ucpLM++ & LABEL_INTERIOR)
       {
        // do not allow East, West, North, and South to fall off edge of
        // picture
        if (lRow > lRegionULCRow)
          fpNorth = fpCenter - ypitch;
        else
          fpNorth = fpCenter;
        if (lRow < lRegionLRCRow-1)
          fpSouth = fpCenter + ypitch;
        else
          fpSouth = fpCenter;

        if (lCol < lRegionLRCCol-1)
         {
          fpEast = fpCenter + xpitch;
          fpNorthEast = fpNorth + xpitch;
          fpSouthEast = fpSouth + xpitch;
         }
        else
         {
          fpEast = fpCenter;
          fpNorthEast = fpNorth;
          fpSouthEast = fpSouth;
         }
        if (lCol > lRegionULCCol)
         {
          fpWest = fpCenter - xpitch;
          fpNorthWest = fpNorth - xpitch;
          fpSouthWest = fpSouth - xpitch;
         }
        else
         {
          fpWest = fpCenter;
          fpNorthWest = fpNorth;
          fpSouthWest = fpSouth;
         }

        for (lCom = 0; lCom < lNCom; lCom++)
         {
          fEast = FSQR(*fpCenter - *fpEast) +
	            FSQR((*fpNorthEast - *fpSouthEast)/2.0);
           fWest = FSQR(*fpCenter - *fpWest) +
                     FSQR((*fpNorthWest - *fpSouthWest)/2.0);
           fNorth = FSQR(*fpCenter - *fpNorth) +
                      FSQR((*fpNorthEast - *fpNorthWest)/2.0);
           fSouth = FSQR(*fpCenter - *fpSouth) +
                      FSQR((*fpSouthEast - *fpSouthWest)/2.0);
           fEast = 1.0/sqrt(stabilizer+fEast);
           fWest = 1.0/sqrt(stabilizer+fWest);
           fNorth = 1.0/sqrt(stabilizer+fNorth);
           fSouth = 1.0/sqrt(stabilizer+fSouth);
           fNorm = fEast + fWest + fNorth + fSouth;
                fEast /= fNorm;
           fWest /= fNorm;
           fNorth /= fNorm;
           fSouth /= fNorm;
           *fpCenter = fEast*(*fpEast) +
	                       fWest*(*fpWest) +
	                       fNorth*(*fpNorth) +
	                       fSouth*(*fpSouth);
          fpCenter++;
          fpEast++;
          fpWest++;
          fpNorth++;
          fpNorthEast++;
          fpNorthWest++;
          fpSouth++;
          fpSouthEast++;
          fpSouthWest++;
         } /* lCom */
       } /* check RM: if */
      else
       {
        fpCenter += lNCom;
       } /* check RM: else */
     } /* lCol */
   } /* lRow */

  return;
}  /* DoHardMotionEdgePreserving */

//int CRepairDRS::ColorBump ()
//{
//   // copy the original to initialize the result
//   CopyImage (uspDefaultResult, uspData[lRepairFrame]);
//
//   if (bDebugFlag)
//   {
//      UseFillValue ();
//      return 0;
//   }
//
//   int inputDataSize = lNRow * lPitch * lNCom * sizeof(MTI_UINT16);
//   MTI_UINT16 maxValue = (MTI_UINT16)laMaxValue[0];
//   RECT roiRect;
//   roiRect.left = lRegionULCCol;
//   roiRect.top = lRegionULCRow;
//   roiRect.right = lRegionLRCCol;
//   roiRect.bottom = lRegionLRCRow;
//
//	return colorBumper.Bump(uspData, lPitch, inputDataSize, maxValue, bIsMonochrome, cbdDirection, roiRect, uspDefaultResult);
//}
///xxxxxx
int CRepairDRS::ColorBump()
{
	try
	{
		// For debugging remove when done
		Ipp16uArray fullOutputFrame({lPitch, lNImageRows, lNCom}, uspData[lRepairFrame], true);

		// Massage the data to a better form
		auto bitsPerPixelComponent = 0;
		MTI_UINT16 maxPixel = (MTI_UINT16)laMaxValue[0];
		for (auto mask = 1; mask < maxPixel; mask <<= 1)
		{
			++bitsPerPixelComponent;
		}

      // This doesn't have to be set, but always should be
      fullOutputFrame.setOriginalBits(bitsPerPixelComponent);

		std::vector<Ipp16uArray>fullInputFrames;
		for (auto i : range(5))
		{
			fullInputFrames.emplace_back(Ipp16uArray({lPitch, lNImageRows, lNCom}, uspData[i]));
			fullInputFrames.back().setOriginalBits(bitsPerPixelComponent);
		}

		// Some of these fullInputFrames may not be valid, use the reflected ones
		for (auto i : range(5))
		{
			auto validBit = 1 << i;
			if ((validBit & _validFrameMask) == 0)
			{
				fullInputFrames[i] = fullInputFrames[4 - i];
			}
		}

      // We have 5 valid frames, but we might want to force a direction
      // just duplicate the ones on the other side
      // At boundries this is a noop.
      switch (cbdDirection)
      {
         case PAST_ONLY:
         fullInputFrames[4] = fullInputFrames[0];
         fullInputFrames[3] = fullInputFrames[1];
         break;

         case FUTURE_ONLY:
         fullInputFrames[0] = fullInputFrames[4];
         fullInputFrames[1] = fullInputFrames[3];
         break;

         case AUTO:
         break;
      }

		MtiRect repairRoi(lRegionULCCol, lRegionULCRow, lNCol, lNRow);

   // Note: there is no direction as the 5 fullInputFrames are valid and the center one is repair
   // E.g. everything is treated as a flash frame.
		auto stat = colorBumper.flashFrame(fullInputFrames, fullOutputFrame, repairRoi);
		CopyImage(uspDefaultResult, fullOutputFrame.data());
      return stat;
	}
	catch (const std::exception &ex)
	{
		DBTRACE(ex.what());
		return -1;
	}

}


int CRepairDRS::RefineLinearMotion (long lDim)
{
  long lPair, lMinRow0, lMinCol0, lMinRow1, lMinCol1, lMaxRow0, lMaxCol0,
		lMaxRow1, lMaxCol1, lMotionRow0, lMotionCol0,
		lFrame0, lFrame1, lRow0, lCol0, lRow1, lCol1,
		lBestRow0, lBestCol0, lBestRow1, lBestCol1, lDimension;
  float fBestScore, fScore, fFactor;
  REPAIR_DEBRIS *rdpNew;
  int iRet;

/*
	At this point we have the best motion for a coarse resolution.
	Refine this estimate.
*/

  rdpNew = &clp->rdaRepair[lDim];
  lDimension = clp->laDimension[lDim+1] / clp->laDimension[lDim];

  for (lPair = 0; lPair < clp->lNPair; lPair++)
   {
    lFrame0 = clp->laFrame0[lPair];
    lFrame1 = clp->laFrame1[lPair];
    fFactor = clp->faFactor[lPair];

    lMotionRow0 = clp->maBestMotion[lDim+1][lPair].lMotionRow0 * lDimension;
    lMotionCol0 = clp->maBestMotion[lDim+1][lPair].lMotionCol0 * lDimension;

    lMinRow0 = lMotionRow0 - lDimension/2;
    lMaxRow0 = lMotionRow0 + 3*lDimension/2;
    lMinCol0 = lMotionCol0 - lDimension/2;
    lMaxCol0 = lMotionCol0 + 3*lDimension/2;

    if (lMinRow0 < rdpNew->lMinMotionRow)
     {
      lMinRow0 = rdpNew->lMinMotionRow;
     }
    else if (lMinRow0 > rdpNew->lMaxMotionRow)
     {

      lMinRow0 = rdpNew->lMaxMotionRow;
     }

    if (lMaxRow0 < rdpNew->lMinMotionRow)
     {
      lMaxRow0 = rdpNew->lMinMotionRow;
     }
    else if (lMaxRow0 > rdpNew->lMaxMotionRow)
     {
      lMaxRow0 = rdpNew->lMaxMotionRow;
     }

    if (lMinCol0 < rdpNew->lMinMotionCol)
     {
      lMinCol0 = rdpNew->lMinMotionCol;
     }
    else if (lMinCol0 > rdpNew->lMaxMotionCol)
     {
      lMinCol0 = rdpNew->lMaxMotionCol;
     }

    if (lMaxCol0 < rdpNew->lMinMotionCol)
     {
      lMaxCol0 = rdpNew->lMinMotionCol;
     }
    else if (lMaxCol0 > rdpNew->lMaxMotionCol)
     {
      lMaxCol0 = rdpNew->lMaxMotionCol;
     }

    lMinRow1 = rdpNew->lMinMotionRow;
    lMaxRow1 = rdpNew->lMaxMotionRow;
    lMinCol1 = rdpNew->lMinMotionCol;
    lMaxCol1 = rdpNew->lMaxMotionCol;

    fBestScore = INFINITY_FLOAT;
    for (lRow0 = lMinRow0; lRow0 <= lMaxRow0; lRow0++)
    for (lCol0 = lMinCol0; lCol0 <= lMaxCol0; lCol0++)
     {
      lRow1 = NINT(lRow0 * fFactor);
      lCol1 = NINT(lCol0 * fFactor);
      if (lRow1 < lMinRow1)
        lRow1 = lMinRow1;
      else if (lRow1 > lMaxRow1)
        lRow1 = lMaxRow1;

      if (lCol1 < lMinCol1)
        lCol1 = lMinCol1;
      else if (lCol1 > lMaxCol1)
        lCol1 = lMaxCol1;

      iRet = CalcScore (rdpNew, lFrame0, lRow0, lCol0, lFrame1,
		lRow1, lCol1, &fScore);
      if (iRet)
        return iRet;

      if (fScore < fBestScore)
       {
        fBestScore = fScore;
        lBestRow0 = lRow0;
        lBestCol0 = lCol0;
        lBestRow1 = lRow1;
        lBestCol1 = lCol1;
       }
     }

    if (fBestScore == INFINITY_FLOAT)
     {
      return ERR_NO_BEST_VALUE;
     }

    clp->maBestMotion[lDim][lPair].lMotionRow0 = lBestRow0;
    clp->maBestMotion[lDim][lPair].lMotionCol0 = lBestCol0;
    clp->maBestMotion[lDim][lPair].lMotionRow1 = lBestRow1;
    clp->maBestMotion[lDim][lPair].lMotionCol1 = lBestCol1;
    clp->maBestMotion[lDim][lPair].fScore = fBestScore;

#ifdef PRINT_OUT
{
char caMessage[128];
sprintf (caMessage, "Dim:  %ld best motion %f (%ld, %ld) and (%ld, %ld)\n",
	lDim, fBestScore, lBestRow0, lBestCol0, lBestRow1, lBestCol1);
TRACE_0 (errout << caMessage);
}
#endif
   }

  return RET_SUCCESS;
}  /* RefineLinearMotion */

int CRepairDRS::DetermineAcceleration ()
{
  long lPair, lMotionRow0, lMotionCol0, lMotionRow1, lMotionCol1,
		lFrame0, lFrame1, lRow0, lCol0, lRow1, lCol1,
		lBestRow0, lBestCol0, lBestRow1, lBestCol1,
		lMinRow0, lMinCol0, lMaxRow0, lMaxCol0, lBestPair,
		lMinRow1, lMinCol1, lMaxRow1, lMaxCol1, lKeepGoing,
		lRowDiff0, lColDiff0, lRowDiff1, lColDiff1;
  float fBestScore, fScore;
  REPAIR_DEBRIS *rdp;
  int iRet;

  rdp = &clp->rdaRepair[0];

/*
	If this was generated using the image gradient, update scores
	with original image values
*/

  if (bUseGradient)
   {
    for (lPair = 0; lPair < clp->lNPair; lPair++)
     {
      iRet = CalcScore (&clp->rdOrig,
		clp->laFrame0[lPair],
		clp->maBestMotion[0][lPair].lMotionRow0,
		clp->maBestMotion[0][lPair].lMotionCol0,
		clp->laFrame1[lPair],
		clp->maBestMotion[0][lPair].lMotionRow1,
		clp->maBestMotion[0][lPair].lMotionCol1,
		&clp->maBestMotion[0][lPair].fScore);
      if (iRet)
        return iRet;
     }
   }

/*
	Now determine which lPair has the lowest score.
*/

  fBestScore = clp->maBestMotion[0][0].fScore;
  lBestPair = 0;
  for (lPair = 1; lPair < clp->lNPair; lPair++)
   {
    if (
	(clp->maBestMotion[0][lPair].fScore < fBestScore) ||
	((clp->maBestMotion[0][lPair].fScore  == fBestScore) &&
		((clp->laFrame0[lPair] < lRepairFrame &&
		  clp->laFrame1[lPair] > lRepairFrame) ||
		 (clp->laFrame0[lPair] > lRepairFrame &&
		  clp->laFrame1[lPair] < lRepairFrame)))
       )
     {
      fBestScore = clp->maBestMotion[0][lPair].fScore;
      lBestPair = lPair;
     }
   }
  clp->lBestPair = lBestPair;

/*
	Now perform acceleration for best pair
*/

  lMotionRow0 = clp->maBestMotion[0][lBestPair].lMotionRow0;
  lMotionCol0 = clp->maBestMotion[0][lBestPair].lMotionCol0;
  lMotionRow1 = clp->maBestMotion[0][lBestPair].lMotionRow1;
  lMotionCol1 = clp->maBestMotion[0][lBestPair].lMotionCol1;
  lFrame0 = clp->laFrame0[lBestPair];
  lFrame1 = clp->laFrame1[lBestPair];

  lMinRow0 = rdp->lMinMotionRow;
  lMaxRow0 = rdp->lMaxMotionRow;
  lMinCol0 = rdp->lMinMotionCol;
  lMaxCol0 = rdp->lMaxMotionCol;
  lMinRow1 = rdp->lMinMotionRow;
  lMaxRow1 = rdp->lMaxMotionRow;
  lMinCol1 = rdp->lMinMotionCol;
  lMaxCol1 = rdp->lMaxMotionCol;

  fBestScore = clp->maBestMotion[0][lBestPair].fScore;
  lBestRow0 = lMotionRow0;
  lBestCol0 = lMotionCol0;
  lBestRow1 = lMotionRow1;
  lBestCol1 = lMotionCol1;

/*
	Allow for acceleration
*/

  do
   {
    lKeepGoing = FALSE;

    for (lRow0 = lBestRow0 - 1; lRow0 <= lBestRow0 + 1; lRow0++)
     {
      lRowDiff0 = lRow0 - lMotionRow0;
      if (lRowDiff0 < 0) lRowDiff0 = -lRowDiff0;
      if (
		lRowDiff0 <= lAcclRow &&
		lRow0 >= lMinRow0 &&
		lRow0 <= lMaxRow0
         )
       {
        for (lCol0 = lBestCol0 - 1; lCol0 <= lBestCol0 + 1; lCol0++)
         {
          lColDiff0 = lCol0 - lMotionCol0;
          if (lColDiff0 < 0) lColDiff0 = -lColDiff0;
          if (
			lColDiff0 <= lAcclCol &&
			lCol0 >= lMinCol0 &&
			lCol0 <= lMaxCol0
             )
           {
            for (lRow1 = lBestRow1 - 1; lRow1 <= lBestRow1 + 1; lRow1++)
             {
              lRowDiff1 = lRow1 - lMotionRow1;
              if (lRowDiff1 < 0) lRowDiff1 = -lRowDiff1;
              if (
				lRowDiff1 <= lAcclRow &&
				lRow1 >= lMinRow1 &&
				lRow1 <= lMaxRow1
                 )
               {
                for (lCol1 = lBestCol1 - 1; lCol1 <= lBestCol1 + 1; lCol1++)
                 {
                  lColDiff1 = lCol1 - lMotionCol1;
                  if (lColDiff1 < 0) lColDiff1 = -lColDiff1;

                  if (
				lColDiff1 <= lAcclCol &&
				lCol1 >= lMinCol1 &&
				lCol1 <= lMaxCol1
                     )
                   {

                    if (
				lRow0 != lBestRow0 || lCol0 != lBestCol0 ||
		 		lRow1 != lBestRow1 || lCol1 != lBestCol1
                       )
                     {
                      iRet = CalcScore (rdp, lFrame0, lRow0,
				lCol0, lFrame1, lRow1, lCol1, &fScore);
                      if (iRet)
                        return iRet;
                      if (fScore < fBestScore)
                       {
                        lKeepGoing = TRUE;
                        fBestScore = fScore;
                        lBestRow0 = lRow0;
                        lBestCol0 = lCol0;
                        lBestRow1 = lRow1;
                        lBestCol1 = lCol1;
                       }
                     }
                   }
                 }
               }
             }
           }
         }
       }
     }
   } while (lKeepGoing);

  clp->mBestAccel.lMotionRow0 = lBestRow0;
  clp->mBestAccel.lMotionCol0 = lBestCol0;
  clp->mBestAccel.lMotionRow1 = lBestRow1;
  clp->mBestAccel.lMotionCol1 = lBestCol1;
  clp->mBestAccel.fScore = fBestScore;


  return RET_SUCCESS;
}  /* DetermineAcceleration */

int CRepairDRS::CalcScore (REPAIR_DEBRIS *rdp, long lFrame0, long lRow0,
	long lCol0, long lFrame1, long lRow1, long lCol1, float *fpScore)
{
  long lNColMot, lTimeDiff0, lTimeDiff1, lTimeDiff01,
		lRC0, lRC1;
  float fScoreSurr0, fScoreSurr1, fScoreSelect;
  int iRet;

  lNColMot = rdp->lNColMot;
  lRC0 = lRow0 * lNColMot + lCol0;
  lRC1 = lRow1 * lNColMot + lCol1;

  css.lpSelectRowCol = rdp->lpSelectRowCol;
  css.lpSurroundRowCol = rdp->lpSurroundRowCol;
  css.uspMotData = rdp->uspMotData[lRepairFrame];
  css.uspMotData0 = rdp->uspMotData[lFrame0] + lRC0;
  css.uspMotData1 = rdp->uspMotData[lFrame1] + lRC1;

  css.lNSurround = rdp->lNSurroundRowCol;
  css.lNSelect = rdp->lNSelectRowCol;

  css.lNJob = msCalcScore.iNJob;

  iRet = MThreadStart (&msCalcScore);
  if (iRet)
    return iRet;

  lTimeDiff01 = laTemporalIndex[lFrame0] - laTemporalIndex[lFrame1];
  if (lTimeDiff01 < 0)
    lTimeDiff01 = -lTimeDiff01;

  lTimeDiff0 = laTemporalIndex[lRepairFrame] - laTemporalIndex[lFrame0];
  if (lTimeDiff0 < 0)
    lTimeDiff0 = -lTimeDiff0;

  lTimeDiff1 = laTemporalIndex[lRepairFrame] - laTemporalIndex[lFrame1];
  if (lTimeDiff1 < 0)
    lTimeDiff1 = -lTimeDiff1;

  if (css.lNSurround*lTimeDiff0)
    fScoreSurr0 = (float)css.lSurroundSum0 / (float)(css.lNSurround*lTimeDiff0);
  else
    fScoreSurr0 = 0.;

  if (css.lNSurround*lTimeDiff1)
    fScoreSurr1 = (float)css.lSurroundSum1 / (float)(css.lNSurround*lTimeDiff1);
  else
    fScoreSurr1 = 0.;

  if (lTimeDiff0 == 0 && lTimeDiff1 == 0 && css.lNSurround > 0)
   {
    fScoreSurr0 = (float)css.lSurroundSum0 / (float)(2*css.lNSurround);
    fScoreSurr1 = (float)css.lSurroundSum1 / (float)(2*css.lNSurround);
   }


  if (css.lNSelect*lTimeDiff01)
    fScoreSelect = (float)css.lSelectSum / (float)(css.lNSelect*lTimeDiff01);
  else
    fScoreSelect = 0.;

  *fpScore = fScoreSurr0 + fScoreSurr1 + fScoreSelect;

  return 0;
}  /* CalcScore */

#ifdef __OLD_WAY__

  void getPixel(float fDstRow, float fDstCol, float *fpSrcRow, float *fpSrcCol,
		bool bOverFlag);

#else
  void CRepairDRS::getPixel_moveRotateOver (float fDstRow, float fDstCol,
                                      float &fpSrcRow, float &fpSrcCol)
  {
    // this function maps a moved pixel back to its original
    // pixel location using the MOVE/ROTATE OVER parameters.
    // Rotation is about the "center of mass" of the fix

    // basic formula is SrcX = cos * DstX - sin * DstY + offX
    //                  SrcY = sin * DstX + cos * DstY + offY

    float fXDst = fDstCol - fCenterCol;
    float fYDst = fDstRow - fCenterRow;

    float fXSrc = fCosOver * fXDst - fSinOver * fYDst - fMoveOverCol;
    float fYSrc = fSinOver * fXDst + fCosOver * fYDst - fMoveOverRow;

    fpSrcRow = fCenterRow + fYSrc;
    fpSrcCol = fCenterCol + fXSrc;

  }

  void CRepairDRS::getPixel_moveRotateUnder (float fDstRow, float fDstCol,
                                        float &fpSrcRow, float &fpSrcCol)
  {
    // this function maps a moved pixel back to its original
    // pixel location using the MOVE/ROTATE UNDER parameters.
    // Rotation is about the "center of mass" of the fix

    // basic formula is SrcX = cos * DstX - sin * DstY + offX
    //                  SrcY = sin * DstX + cos * DstY + offY

    float fXDst = fDstCol - fCenterCol;
    float fYDst = fDstRow - fCenterRow;

    float fXSrc = fCosUnder * fXDst - fSinUnder * fYDst - fMoveUnderCol;
    float fYSrc = fSinUnder * fXDst + fCosUnder * fYDst - fMoveUnderRow;

    fpSrcRow = fCenterRow + fYSrc;
    fpSrcCol = fCenterCol + fXSrc;

  }

  void CRepairDRS::getPixel_moveOverOnly (float fDstRow, float fDstCol,
                                     float &fpSrcRow, float &fpSrcCol)
  {
    // this function maps a moved pixel back to its original
    // pixel location using the MOVE OVER parameters (NO ROTATION).

    // basic formula is SrcX = DstX + offX
    //                  SrcY = DstY + offY

    fpSrcCol = fDstCol - fMoveOverCol;
    fpSrcRow = fDstRow - fMoveOverRow;

  }

  void CRepairDRS::getPixel_moveUnderOnly (float fDstRow, float fDstCol,
                                      float &fpSrcRow, float &fpSrcCol)
  {
    // this function maps a moved pixel back to its original
    // pixel location using the MOVE UNDER parameters (NO ROTATION).

    // basic formula is SrcX = DstX + offX
    //                  SrcY = DstY + offY

    fpSrcCol = fDstCol - fMoveUnderCol;
    fpSrcRow = fDstRow - fMoveUnderRow;

  };

#endif // getPixel speedup

void DoCalcScore (void *vp, int iJob)
{
  long lSelectSum, lSurroundSum0, lSurroundSum1, lDiff0, lDiff1, lDiff,
		*lpSelectRowCol, *lpSurroundRowCol, lSelect, lSurround;
  long lSurroundStart, lSurroundStop, lSelectStart, lSelectStop, lStop;
  MTI_UINT16 *uspMotData, *uspMotData0, *uspMotData1;
  CALC_SCORE_STRUCT *cssp;

  cssp = (CALC_SCORE_STRUCT *) vp;

  uspMotData = cssp->uspMotData;
  uspMotData0 = cssp->uspMotData0;
  uspMotData1 = cssp->uspMotData1;

  lSurroundStart = (iJob * cssp->lNSurround) / cssp->lNJob;
  lSurroundStop = ((iJob+1) * cssp->lNSurround) / cssp->lNJob;
  lSelectStart = (iJob * cssp->lNSelect) / cssp->lNJob;
  lSelectStop = ((iJob+1) * cssp->lNSelect) / cssp->lNJob;

  lpSurroundRowCol = cssp->lpSurroundRowCol + lSurroundStart;
  lpSelectRowCol = cssp->lpSelectRowCol + lSelectStart;

  lSurroundSum0 = 0;
  lSurroundSum1 = 0;
  for (lSurround = lSurroundStart; lSurround < lSurroundStop; lSurround++)
   {
    lDiff0 = (long)uspMotData[*lpSurroundRowCol] -
		(long)uspMotData0[*lpSurroundRowCol];
    lDiff1 = (long)uspMotData[*lpSurroundRowCol] -
		(long)uspMotData1[*lpSurroundRowCol];

    if (lDiff0 < 0)
      lSurroundSum0 -= lDiff0;
    else
      lSurroundSum0 += lDiff0;

    if (lDiff1 < 0)
      lSurroundSum1 -= lDiff1;
    else
      lSurroundSum1 += lDiff1;

     lpSurroundRowCol++;
   }

  lSelectSum = 0;
  lStop = lSelectStop - lSelectStart;
  lStop /= 10;
  lStop *= 10;
  lStop += lSelectStart;
  for (lSelect = lSelectStart; lSelect < lStop; lSelect+=10)
   {
		// 0
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 1
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 2
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 3
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 4
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 5
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 6
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 7
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 8
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;

		// 9
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;
   }

  for ( ; lSelect < lSelectStop; lSelect++)
   {
    lDiff = (long)uspMotData0[*lpSelectRowCol] -
		(long)uspMotData1[*lpSelectRowCol];

    if (lDiff < 0)
      lSelectSum -= lDiff;
    else
      lSelectSum += lDiff;

    lpSelectRowCol++;
   }

  cssp->laSurroundSum0[iJob] = lSurroundSum0;
  cssp->laSurroundSum1[iJob] = lSurroundSum1;
  cssp->laSelectSum[iJob] = lSelectSum;
}  /* DoCalcScore */

void FinishCalcScore (void *vp)
{
  CALC_SCORE_STRUCT *cssp;
  long lJob;

  cssp = (CALC_SCORE_STRUCT *) vp;
  cssp->lSurroundSum0 = 0;
  cssp->lSurroundSum1 = 0;
  cssp->lSelectSum = 0;
  for (lJob = 0; lJob < cssp->lNJob; lJob++)
   {
    cssp->lSurroundSum0 += cssp->laSurroundSum0[lJob];
    cssp->lSurroundSum1 += cssp->laSurroundSum1[lJob];
    cssp->lSelectSum += cssp->laSelectSum[lJob];
   }
}  /* FinishCalcScore */

void CRepairDRS::UseFillValue ()
{
  long lCom, lRow, lCol;
  MTI_UINT16 *uspRes;
  MTI_UINT8 *ucpLM;

  for (lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    ucpLM = roi.getLabelPtr() + (lRow * lPitch + lRegionULCCol);
    uspRes = uspResult + (lRow * lPitch + lRegionULCCol) * lNCom;
    for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
     {
      if (*ucpLM++ & LABEL_INTERIOR)
       {
        for (lCom = 0; lCom < lNCom; lCom++)
         {
          *uspRes++ = laFillValue[lCom];
         }
       }
      else
       {
        uspRes += lNCom;
       }
     }
   }

  return;
}  /* UseFillValue */

void CRepairDRS::CopyImage (MTI_UINT16 *Dst, const MTI_UINT16 *Src)
{
  // initialize the result
  long lNByte = (lRegionLRCCol - lRegionULCCol) * lNCom *
                 sizeof (MTI_UINT16);
  long lRowJump = lPitch * lNCom;

  Src += (lRegionULCRow * lPitch + lRegionULCCol) * lNCom;
  Dst += (lRegionULCRow * lPitch + lRegionULCCol) * lNCom;

  for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    MTImemcpy ((void *)Dst, (void *)Src, lNByte);
    Src += lRowJump;
    Dst += lRowJump;
   }
}


void CRepairDRS::OverridePrepare ()
{
  CenterOfMass ();

  lGrainSeed += 1234;

  // is this material YUV or RGB?
   if (iPixelComponents == IF_PIXEL_COMPONENTS_RGB
   || iPixelComponents == IF_PIXEL_COMPONENTS_RGBA
   || iPixelComponents == IF_PIXEL_COMPONENTS_BGR
   || iPixelComponents == IF_PIXEL_COMPONENTS_BGRA
   || iPixelComponents == IF_PIXEL_COMPONENTS_Y
   || iPixelComponents == IF_PIXEL_COMPONENTS_YYY)
   {
      bYUV = false;
   }
   else
   {
      bYUV = true;
   }

  // re-generate the masks (if necessary)
  renderMask();

  // copy this to local for speed
  MTI_UINT8 *ucpRoiLabelPtr = roi.getLabelPtr();

#ifdef __OLD_WAY__
  for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
  for (long lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
   {
    float fBeforeMoveRow, fBeforeMoveCol;
    long lBeforeMoveRow, lBeforeMoveCol;

    // where does this pixel fall in the original fix
    getPixel ((float)lRow, (float)lCol, &fBeforeMoveRow, &fBeforeMoveCol, true);

    lBeforeMoveRow = floor (fBeforeMoveRow);
    lBeforeMoveCol = floor (fBeforeMoveCol);

    // does the before move pixel need processing?
      if (lBeforeMoveRow >= lRegionULCRow && lBeforeMoveRow < lRegionLRCRow && lBeforeMoveCol >= lRegionULCCol && lBeforeMoveCol <
         lRegionLRCCol && ucpRoiLabelPtr[lBeforeMoveRow * lPitch + lBeforeMoveCol] & LABEL_INTERIOR)
      {
         ucpRoiLabelPtr[lRow * lPitch + lCol] |= LABEL_MODIFIED;
      }
      else
      {
         ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;
      }
   }

#else // NEW, FASTER WAY

  // optimizations
  const bool bNoRotates = (fSinOver == 0.F) && (fSinUnder == 0.F);
  const bool bNoMoves = (fMoveOverCol == 0.F) && (fMoveOverRow == 0.F) &&
                       (fMoveUnderCol == 0.F) && (fMoveUnderRow == 0.F);

  // These will capture the bounding rectangle of the pixels to be modified
  lDstBoundLeft   = lRegionLRCCol;
  lDstBoundRight  = lRegionULCCol;
  lDstBoundTop    = lRegionLRCRow;
  lDstBoundBottom = lRegionULCRow;

  // make a mask of pixels to process

  if (bNoMoves && bNoRotates)
   {
    // Hack because lCol will start at 0 (=lRegionULCCol)
    lDstBoundLeft -= lRegionULCCol;
    lDstBoundRight -= lRegionULCCol;

    for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
     {
      MTI_UINT8 *ucpLabel = &ucpRoiLabelPtr[lRow * lPitch + lRegionULCCol];
      const long lColStop = lRegionLRCCol - lRegionULCCol;

      for (long lCol = 0; lCol < lColStop; lCol++)
       {
        // does the pixel need processing?
        MTI_UINT8 &ucLabel = *(ucpLabel + lCol);

        if (ucLabel & LABEL_INTERIOR)
         {
          ucLabel |= LABEL_MODIFIED;

          if (lRow < lDstBoundTop)
            lDstBoundTop = lRow;
          if (lRow >= lDstBoundBottom)
            lDstBoundBottom = lRow + 1;
          if (lCol < lDstBoundLeft)
            lDstBoundLeft = lCol;
          if (lCol >= lDstBoundRight)
            lDstBoundRight = lCol + 1;
         }
        else
         {
          ucLabel &= (~LABEL_MODIFIED);
         }
       }
     }
    // Adjust
    lDstBoundLeft += lRegionULCCol;
    lDstBoundRight += lRegionULCCol;

    // Since there are no overrides, the source bounding rect is the same
    // as the destination's
    lSrcBoundTop = lDstBoundTop;
    lSrcBoundBottom = lDstBoundBottom;
    lSrcBoundLeft = lDstBoundLeft;
    lSrcBoundRight = lDstBoundRight;

   }
  else if (bNoRotates)
   {
    long lRowStart = lRegionULCRow;
    long lRowStop = lRegionLRCRow;

    // We will iterate over the destination pixels, so make sure a priori
    // that we won't go outside the large padded rectangle
    // QQQ Should consider MoveUnder's here as well?
    if (fMoveOverRow > 0.F)
     {
      lRowStart += floor (fMoveOverRow);
     }
    else if (fMoveOverRow < 0.F)
     {
      lRowStop += floor (fMoveOverRow);
     }

    // Initialize the no-man's-land at top if any
    for (long lRow = lRegionULCRow; lRow < lRowStart; ++lRow)
    for (long lCol = lRegionULCCol; lCol < lRegionLRCCol; ++lCol)
      ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;

    for (long lRow = lRowStart; lRow < lRowStop; lRow++)
     {
      long lColStart = lRegionULCCol;
      long lColStop = lRegionLRCCol;

      // QQQ Should consider MoveUnder's here as well?
      if (fMoveOverCol > 0.F)
       {
        lColStart += floor (fMoveOverCol);
       }
      else if (fMoveOverCol < 0.F)
       {
        lColStop += floor (fMoveOverCol);
       }

      // Initialize the no-man's-land at left of this row if any
      for (long lCol = lRegionULCCol; lCol < lColStart; ++lCol)
        ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;

      for (long lCol = lColStart; lCol < lColStop; lCol++)
       {
        long lBeforeMoveRow, lBeforeMoveCol;

        // where does this pixel fall in the original fix
        lBeforeMoveRow = lRow - floor (fMoveOverRow);
        lBeforeMoveCol = lCol - floor (fMoveOverCol);

        // made this an assert because theoretically we have prevented
        // any of these cases from being false by adjusting the iteration
        // parameters
        MTIassert(
            lBeforeMoveRow >= lRegionULCRow &&
            lBeforeMoveRow < lRegionLRCRow &&
            lBeforeMoveCol >= lRegionULCCol &&
            lBeforeMoveCol < lRegionLRCCol);

        // does the before move pixel need processing?

        if (ucpRoiLabelPtr[lBeforeMoveRow * lPitch + lBeforeMoveCol] & LABEL_INTERIOR)
         {
          ucpRoiLabelPtr[lRow * lPitch + lCol] |= LABEL_MODIFIED;

          // We keep separate bounds rects for src and destination even
          // though they only differ by a constant linear combination of
          // the move over and under values.

          // lRow and lCol are iterations over the destination pixels
          if (lRow < lDstBoundTop)
            lDstBoundTop = lRow;
          if (lRow >= lDstBoundBottom)
            lDstBoundBottom = lRow + 1;
          if (lCol < lDstBoundLeft)
            lDstBoundLeft = lCol;
          if (lCol >= lDstBoundRight)
            lDstBoundRight = lCol + 1;
         }
        else
         {
          ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;
         }
       }

      // Initialize the no-man's-land at right of this row if any
      for (long lCol = lColStop; lCol < lRegionLRCCol; ++lCol)
        ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;
     }

    // Initialize the no-man's-land at bottom if any
    for (long lRow = lRowStop; lRow < lRegionLRCRow; ++lRow)
    for (long lCol = lRegionULCCol; lCol < lRegionLRCCol; ++lCol)
      ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;

    // Compute source bounds (same rectangle as dest, but offset by moves
    lSrcBoundTop = lDstBoundTop - floor (fMoveOverRow) - floor (fMoveUnderRow);
    lSrcBoundBottom = lDstBoundBottom - floor (fMoveOverRow) - floor (fMoveUnderRow);
    lSrcBoundLeft = lDstBoundLeft - floor (fMoveOverCol) - floor (fMoveUnderCol);
    lSrcBoundRight = lDstBoundRight - floor (fMoveOverCol) - floor (fMoveUnderCol);

    // Of course subject to global bounds
    // QQQ I don't like this - means the dst and source rectangles won't
    // be same size - should we adjust iteration parameters so this can't
    // happen?
    if (lSrcBoundTop < lRegionULCRow)
       lSrcBoundTop = lRegionULCRow;
    if (lSrcBoundBottom > lRegionLRCRow)
       lSrcBoundBottom = lRegionLRCRow;
    if (lSrcBoundLeft < lRegionULCCol)
       lSrcBoundLeft = lRegionULCCol;
    if (lSrcBoundRight > lRegionLRCCol)
       lSrcBoundRight = lRegionLRCCol;

   }
  else // Full-blown MOVE + ROTATION case; not optimized ... SORRY!!
   {
    for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
    for (long lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
     {
      float fBeforeMoveRow, fBeforeMoveCol;
      long lBeforeMoveRow, lBeforeMoveCol;

      // where does this pixel fall in the original fix
      getPixel_moveRotateOver ((float)lRow, (float)lCol, fBeforeMoveRow, fBeforeMoveCol);

      lBeforeMoveRow = floor (fBeforeMoveRow);
      lBeforeMoveCol = floor (fBeforeMoveCol);

      // does the before move pixel need processing?
      // TODO fix iteration parameters so we don't need the 4 checks! QQQ
      if (
          lBeforeMoveRow >= lRegionULCRow &&
          lBeforeMoveRow < lRegionLRCRow &&
          lBeforeMoveCol >= lRegionULCCol &&
          lBeforeMoveCol < lRegionLRCCol &&
          ucpRoiLabelPtr[lBeforeMoveRow * lPitch + lBeforeMoveCol] &
                          LABEL_INTERIOR
         )
       {
        ucpRoiLabelPtr[lRow * lPitch + lCol] |= LABEL_MODIFIED;
       }
      else
       {
        ucpRoiLabelPtr[lRow * lPitch + lCol] &= ~LABEL_MODIFIED;
       }
     }

    // We punt for now on figuring out the real bounding rectangles
    // TODO: Figure out the real bounding rectangles, if we decide we want
    // to speed up the "rotate over" and "rotate under" cases! QQQ
    lSrcBoundTop = lDstBoundTop = lRegionULCRow;
    lSrcBoundBottom = lDstBoundBottom = lRegionLRCRow;
    lSrcBoundLeft = lDstBoundLeft = lRegionULCCol;
    lSrcBoundRight = lDstBoundRight = lRegionLRCCol;
   }

#endif  // getPixel speedup
}

void CRepairDRS::OverrideApply()
{
   MTI_UINT8 *ucpRoiLabelPtr = roi.getLabelPtr();
   const MTI_UINT8 *ucpRoiBlendPtr = roi.getBlendPtr();

   fOldSinOver = fSinOver;
   fOldSinUnder = fSinUnder;

   const bool noOverRotation = (fSinOver == 0.F);
   const bool noUnderRotation = (fSinUnder == 0.F);
   // const bool noOverMotion = noOverRotation && (fMoveOverCol == 0.F) && (fMoveOverRow == 0.F);
   // const bool noUnderMotion = noUnderRotation && (fMoveUnderCol == 0.F) && (fMoveUnderRow == 0.F);

   // add this mask to the pPixelRegionList
   MTIassert(pPixelRegionList != NULL_pPixelRegionList);
   if (pPixelRegionList != 0 && pPixelRegionList != NULL_pPixelRegionList && *lpGlobalPixelRegionListSerial == lLocalPixelRegionListSerial)
   {
      // QQQ DON'T WE WANT THIS TO BE THE PADDDED BOUND RECT?
      RECT rect;
      rect.left = lRegionULCCol;
      rect.top = lRegionULCRow;
      rect.bottom = lRegionLRCRow - 1;
      rect.right = lRegionLRCCol - 1;
      // The pixel region list for a MASK has changed to require
      // frame height in addition to frame width.  The CRepair class
      // does not have frame height as one of its values, so just
      // use lRegionLRCRow.  Adding to the MASK with lRegionLRCRow
      // works correctly.
      pPixelRegionList->Add(rect, ucpRoiLabelPtr, uspResult, lPitch, lRegionLRCRow, lNCom, LABEL_MODIFIED);
   }

   // grain stuff
   long lSeed = lGrainSeed;
   const float *grainPresetValues = _useGrainPresets ? _grainPresetValues : nullptr;

   // copy the DefaultResult to the result
   for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
      for (long lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
      {
         if ((ucpRoiLabelPtr[lRow * lPitch + lCol] & LABEL_MODIFIED) == 0)
         {
            continue;
         }

         float fBeforeMoveRow, fBeforeMoveCol;

         // where does this pixel fall in the original fix
         if (noOverRotation)
         {
            getPixel_moveOverOnly((float)lRow, (float)lCol, fBeforeMoveRow, fBeforeMoveCol);
         }
         else
         {
            getPixel_moveRotateOver((float)lRow, (float)lCol, fBeforeMoveRow, fBeforeMoveCol);
         }

         // process the before move pixel
         // account for MOVE UNDER to decide which pixel actually contributes
         float fOrigRow, fOrigCol;
         long lOrigRow, lOrigCol;
         float fBlendWeight;
         float faWei[2][2], faNew[MAX_COMPONENT];

         if (noUnderRotation)
         {
            getPixel_moveUnderOnly(fBeforeMoveRow, fBeforeMoveCol, fOrigRow, fOrigCol);
         }
         else
         {
            getPixel_moveRotateUnder(fBeforeMoveRow, fBeforeMoveCol, fOrigRow, fOrigCol);
         }

         // what are the weights for this 2x2
         getPixelAndWeight(fOrigRow, fOrigCol, &lOrigRow, &lOrigCol, faWei);

         int iStopInterpRow = (lOrigRow < (lRegionLRCRow - 1)) ? 2 : 1;
         int iStopInterpCol = (lOrigCol < (lRegionLRCCol - 1)) ? 2 : 1;

         faNew[0] = 0.F;
         faNew[1] = 0.F;
			faNew[2] = 0.F;
         for (int iR = 0; iR < iStopInterpRow; iR++)
         {
            for (int iC = 0; iC < iStopInterpCol; iC++)
            {
					long lOrigRowColCom = ((lOrigRow + iR) * lPitch + (lOrigCol + iC)) * lNCom;
               for (long lComponent = 0; lComponent < lNCom; lComponent++)
               {
						faNew[lComponent] += imageDatumToFloat(uspDefaultResult[lOrigRowColCom++]) * faWei[iR][iC];
               }
            }
         }

			fBlendWeight = (float)ucpRoiBlendPtr[(int)fBeforeMoveRow * lPitch + (int)fBeforeMoveCol] / 255.F;

         float fRand = ran266(&lSeed);
         float fGrainFactor = 1.;
         if (fRand < fGrainSize)
         {
            if (fRand < .7 * fGrainSize)
            {
               fGrainFactor = fGrainStrength;
            }
            else
            {
               fGrainFactor = 1. / fGrainStrength;
            }
         }

			float fPresetGrainFactor =  2.F * ((fGrainStrength - MIN_GRAIN_STRENGTH) / (GRAIN_STRENGTH_LOW_LIMIT - MIN_GRAIN_STRENGTH));

         long lRowColCom = (lRow * lPitch + lCol) * lNCom;
         float fY = 0.F;
         for (long lComponent = 0; lComponent < lNCom; lComponent++)
         {
				float fNew = faNew[lComponent];
				float fOrig = imageDatumToFloat(uspResult[lRowColCom]);

            if (fColorBrightness != 1.)
            {
               // apply density adjustment to either Y or R,G,B
               if ((lComponent == 0 && bYUV == true) || (bYUV == false))
               {
						fNew *= fColorBrightness;
               }
            }

            if (bYUV == false && (fColorRed != 1.F || fColorGreen != 1.F || fColorBlue != 1.F))
            {
               // apply RGB adjustments to R,G,B
               switch (lComponent)
               {
                  case 0:
                     fNew *= fColorRed;
                     break;
                  case 1:
                     fNew *= fColorGreen;
                     break;
                  case 2:
                     fNew *= fColorBlue;
                     break;
               }
            }

            if (bYUV == false && lNCom == 3 && fColorSaturation != 1.F)
            {
               if (lComponent == 0)
               {
                  const float KR = 0.2126f;
                  const float KG = 0.7152f;
                  const float KB = 0.0722f;
                  float fR = imageDatumToFloat(uspResult[lRowColCom + 0]);
                  float fG = imageDatumToFloat(uspResult[lRowColCom + 1]);
                  float fB = imageDatumToFloat(uspResult[lRowColCom + 2]);
                  fY = fR * KR + fG * KG + fB * KB;
               }

               fNew = fY + (fColorSaturation * (fNew - fY));
            }

            if (fGrainStrength != MIN_GRAIN_STRENGTH)
            {
					if (grainPresetValues != nullptr)
					{
						// QQQ FIX ME - this value should be imageDatumToFloat(white point - black point)!
						float grainRange = bDataTypeIsReallyHalfFloat ? 1.0F : (float)laMaxValue[lComponent];

						fNew += grainPresetValues[lRowColCom] * grainRange * fPresetGrainFactor;
					}
               else
               {
                  // apply grain adjustment to either Y or R,G,B
                  if ((lComponent == 0 && bYUV == true) || (bYUV == false))
                  {
                     fNew *= fGrainFactor;
                  }
               }
            }

            // apply transparency
				if (fTransparencyOrig > 0.)
            {
               fNew = fTransparencyProc * fNew + fTransparencyOrig * fOrig;
            }

            // apply the blend
            if (fBlendWeight != 1.)
            {
					fNew = fBlendWeight * fNew + (1. - fBlendWeight) * fOrig;
            }

#ifdef _OLD_WAY_
				long lResult = fNew + 0.5;

				// ensure lResult is an allowed value
				if (lResult < laMinValue[lComponent])
            {
					lResult = laMinValue[lComponent];
            }
				if (lResult > laMaxValue[lComponent])
            {
               lResult = laMaxValue[lComponent];
            }

				uspResult[lRowColCom++] = lResult;
#endif
				uspResult[lRowColCom++] = clamp<unsigned short>(floatToImageDatum(fNew), laMinValue[lComponent], laMaxValue[lComponent]);
			}
      }
   }

   // conform image to single field constraint
   PerformFieldRepeat();
}

int CRepairDRS::OverrideUpdate ()
{
  int iRet = 0;

  renderMask ();
  OverridePrepare ();

  // If we exceeded padding or if rotation changed, we must re-do the fix!
  if (
       lSrcBoundTop < lSrcPaddedTop       ||
       lSrcBoundBottom > lSrcPaddedBottom ||
       lSrcBoundLeft < lSrcPaddedLeft     ||
       lSrcBoundRight > lSrcPaddedRight   /*** ||
       fSinOver != fOldSinOver            ||
       fSinUnder != fOldSinUnder  ***/
     )
   {
    fOldSinOver = fSinOver;
    fOldSinUnder = fSinUnder;

    // Previous fix wasn't big enough to cover overrides!
    iRet = PerformFix (lRepairType);
   }
  else
   {
    // Apply the override values
    OverrideApply ();
   }

  return iRet;

}  /* OverridesUpdate */

void CRepairDRS::OverrideMoveOver (float fRow, float fCol)
{
  //******************************************************************
  //*** NOTE! THESE VALUES ARE THE "MOTION" FROM THE DESTINATION PIXEL
  //*** BACK TO THE SOURCE PIXEL. Opposite signs because '+' is UP for
  //*** rows, opposite of the coordinate system
  //******************************************************************
  fMoveOverRow = -fRow;
  fMoveOverCol = +fCol;
}  /* OverrideMoveOver */

void CRepairDRS::OverrideMoveUnder (float fRow, float fCol)
{
  fMoveUnderRow = -fRow;
  fMoveUnderCol = +fCol;
}  /* OverrideMoveUnder */

void CRepairDRS::OverrideColorBrightness (int newBrightness)
{
   if (newBrightness > 0.F)
   {
      fColorBrightness = (newBrightness / 100.F) * (MAX_DENSITY - 1.F) + 1.F;
   }
   else
   {
      fColorBrightness = (newBrightness / 100.F) * (1.F - MIN_DENSITY) + 1.F;
      if (fColorBrightness < 0.F)
      {
         fColorBrightness = 0.F;
      }
   }
}

void CRepairDRS::OverrideColorSaturation (int newSaturation)
{
   if (newSaturation > 0.F)
   {
      fColorSaturation = (newSaturation / 100.F) * (MAX_DENSITY - 1.F) + 1.F;
   }
   else
   {
      fColorSaturation = (newSaturation / 100.F) * (1.F - MIN_DENSITY) + 1.F;
      if (fColorSaturation < 0.F)
      {
         fColorSaturation = 0.F;
      }
   }
}

void CRepairDRS::OverrideColorRed (int newRed)
{
   if (newRed > 0.F)
   {
      fColorRed = (newRed / 100.F) * (MAX_DENSITY - 1.F) + 1.F;
   }
   else
   {
      fColorRed = (newRed / 100.F) * (1.F - MIN_DENSITY) + 1.F;
      if (fColorRed < 0.F)
      {
         fColorRed = 0.F;
      }
   }
}

void CRepairDRS::OverrideColorGreen (int newGreen)
{
   if (newGreen > 0.F)
   {
      fColorGreen = (newGreen / 100.F) * (MAX_DENSITY - 1.F) + 1.F;
   }
   else
   {
      fColorGreen = (newGreen / 100.F) * (1.F - MIN_DENSITY) + 1.F;
      if (fColorGreen < 0.F)
      {
         fColorGreen = 0.F;
      }
   }
}

void CRepairDRS::OverrideColorBlue (int newBlue)
{
   if (newBlue > 0.F)
   {
      fColorBlue = (newBlue / 100.F) * (MAX_DENSITY - 1.F) + 1.F;
   }
   else
   {
      fColorBlue = (newBlue / 100.F) * (1.F - MIN_DENSITY) + 1.F;
      if (fColorBlue < 0.F)
      {
         fColorBlue = 0.F;
      }
   }
}

void CRepairDRS::OverrideGrainSize (int newSize)
{
  fGrainSize = ((float)newSize / 100.) *
	(MAX_GRAIN_SIZE - MIN_GRAIN_SIZE) + MIN_GRAIN_SIZE;
}  /* OverrideGrainSize */

void CRepairDRS::OverrideGrainStrength (int newStrength)
{
  fGrainStrength = ((float)newStrength / 100.) * (GRAIN_STRENGTH_LOW_LIMIT - MIN_GRAIN_STRENGTH) + MIN_GRAIN_STRENGTH;
}  /* OverrideGrainStrength */

void CRepairDRS::OverrideRotateOver (float newRotate)
{
  // mbraca added the '-' to make the rotation go clockwise for positive angles
  fRotateOver = -newRotate * 3.14159 / 180.;

  fSinOver = sin (fRotateOver);
  fCosOver = cos (fRotateOver);
}  /* OverrideRotateOver */

void CRepairDRS::OverrideRotateUnder (float newRotate)
{
  // mbraca added the '-' to make the rotation go clockwise for positive angles
  fRotateUnder = -newRotate * 3.14159 / 180.;

  fSinUnder = sin (fRotateUnder);
  fCosUnder = cos (fRotateUnder);
}  /* OverrideRotateUnder */

void CRepairDRS::OverrideTransparency (int newTransparency)
{
  fTransparencyOrig = (float)newTransparency / 100.;
  fTransparencyProc = 1. - fTransparencyOrig;
}  /* OverrideRotateUnder */

void CRepairDRS::CenterOfMass ()
{
  fCenterRow = 0.;
  fCenterCol = 0.;
  long lNPixel = 0;
  MTI_UINT8 *ucpRoiLabelPtr = roi.getLabelPtr();

  for (long lRow = lRegionULCRow; lRow < lRegionLRCRow; lRow++)
   {
    MTI_UINT8 *ucpRoiLabelPtr0 = ucpRoiLabelPtr + (lRow * lPitch + lRegionULCCol);
    long lColStop = lRegionLRCCol - lRegionULCCol;

    for (long lCol = 0; lCol < lColStop; lCol++)
     {
      if (ucpRoiLabelPtr0[lCol] & LABEL_INTERIOR)
       {
        fCenterRow += (float)lRow;
        fCenterCol += (float)lCol;   // Remember: this is off by -lRegionULCCol
        lNPixel++;
       }
     }
   }

  // Fix the "off by lRegionULCCol"
  fCenterCol += (float) (lNPixel * lRegionULCCol);

  if (lNPixel != 0)
   {
    fCenterRow /= (float)lNPixel;
    fCenterCol /= (float)lNPixel;
   }

}  /* CenterOfMass */

void CRepairDRS::getPixelAndWeight (float fRow, float fCol,
		long *lpRow, long *lpCol, float faWei[2][2])
{
  // bounds check
  if (fRow < lRegionULCRow)
   {
    fRow = lRegionULCRow;
   }
  if (fRow >= lRegionLRCRow - 1)
   {
    //fRow = lRegionLRCRow - 2;		// -2 means do not worry about 2x2 ave
    fRow = lRegionLRCRow - 1;		// mbraca: that creates other worries!
   }

  if (fCol < lRegionULCCol)
   {
    fCol = lRegionULCCol;
   }
  if (fCol >= lRegionLRCCol - 1)
   {
    //fCol = lRegionLRCCol - 2;		// -2 means do not worry about 2x2 ave
    fCol = lRegionLRCCol - 1;		// mbraca: that creates other worries!
   }

  *lpRow = floor (fRow);
  *lpCol = floor (fCol);

  // sub pixel moves are calculated with a 2x2 average.
  float fSubPixelRow = fRow - (float)*lpRow;
  float fSubPixelCol = fCol - (float)*lpCol;

  faWei[0][0] = (1.0F - fSubPixelRow) * (1.0F - fSubPixelCol);
  faWei[0][1] = (1.0F - fSubPixelRow) * fSubPixelCol;
  faWei[1][0] = fSubPixelRow * (1.0F - fSubPixelCol);
  faWei[1][1] = fSubPixelRow * fSubPixelCol;

}  /* getPixelAndWeight */
