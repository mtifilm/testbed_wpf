/*
   File Name:  drsDLL.h
   Created July 13/2004 by Aaron Geman (adapted from imgToolDLL.h)

   This just defines the export/import macros
   if MTI_DRSDLL is defined, exports are defined
   else imports are defined
*/

#ifndef DRSDLL_H
#define DRSDLL_H

//////////////////////////////////////////////////////////////////////

#ifdef _WIN32                  // Window compilers need export/import

// Define Import/Export for windows
#ifdef MTI_DRSDLL
#define MTI_DRSDLL_API  __declspec(dllexport)
#else
////TODO understand this #define MTI_DRSDLL_API  __declspec(dllimport)
#define MTI_DRSDLL_API
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_DRSDLL_API
#endif   // End of UNIX

#endif   // Header file


