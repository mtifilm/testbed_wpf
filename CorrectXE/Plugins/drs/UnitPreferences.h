//---------------------------------------------------------------------------

#ifndef UnitPreferencesH
#define UnitPreferencesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include <Buttons.hpp>
#include "SpinEditFrameUnit.h"

//---------------------------------------------------------------------------
class TFormDRSPreferences : public TForm
{
__published:	// IDE-managed Components
    TButton *OKButton;
    TButton *CancelButton;
    TBevel *Bevel1;
    TButton *GetFactorySettingsButton;
    TPageControl *PageControl;
        TTabSheet *TabSheet4;
        TPanel *Panel9;
        TLabel *Label7;
        TPanel *Panel10;
        TLabel *Label8;
        TPanel *Panel11;
        TLabel *Label9;
        TPanel *Panel7;
        TLabel *Label5;
        TTabSheet *TabSheet3;
        TGroupBox *GroupBoxDebris;
        TLabel *Label1;
        TLabel *Label2;
        TGroupBox *GroupBoxImport;
        TLabel *Label3;
        TLabel *Label4;
        TTabSheet *TabSheet1;
        TPanel *Panel1;
        TShape *Shape1;
    TRadioButton *QSRect0RadioButton;
    TRadioButton *QSRect1RadioButton;
        TPanel *Panel2;
        TShape *Shape2;
    TRadioButton *QSRect2RadioButton;
        TPanel *Panel3;
        TShape *Shape3;
        TPanel *Panel6;
        TShape *Shape6;
    TRadioButton *QSEllipse2RadioButton;
        TPanel *Panel5;
        TShape *Shape5;
    TRadioButton *QSEllipse1RadioButton;
        TPanel *Panel4;
        TShape *Shape4;
    TRadioButton *QSEllipse0RadioButton;
    TRadioGroup *ImageResolutionRadioGroup;
    TPanel *Panel8;
    TLabel *Label6;
    TPanel *Panel12;
    TLabel *Label10;
        TPanel *NewQuickSelectSetupPanel;
        TSpeedButton *SquareShapeButton;
        TSpeedButton *VRectShapeButton;
        TSpeedButton *HRectShapeButton;
        TSpeedButton *CircleShapeButton;
        TSpeedButton *VEllipseShapeButton;
        TSpeedButton *HEllipseShapeButton;
        TTrackBar *TrackBar1;
        TCheckBox *AutoAcceptCheckBox;
        TCheckBox *PreloadInputCheckBox;
   TSpinEditFrame *QSRect0SpinEdit;
   TSpinEditFrame *QSRect1SpinEdit;
   TSpinEditFrame *QSRect2SpinEdit;
   TSpinEditFrame *QSEllipse2SpinEdit;
   TSpinEditFrame *QSEllipse1SpinEdit;
   TSpinEditFrame *QSEllipse0SpinEdit;
   TPanel *QuickSelectSetupPanel;
   TSpinEditFrame *MotionLowSpinEdit;
   TSpinEditFrame *MotionHighSpinEdit;
   TSpinEditFrame *ImportHighSpinEdit;
   TSpinEditFrame *ImportLowSpinEdit;
   TPanel *MotionBackgroundPanel;
   TPanel *SizingBackgroundPanel;
   TSpinEditFrame *PaddingHighSpinEdit;
   TSpinEditFrame *GrainSizeSpinEdit;
   TSpinEditFrame *BorderMinSpinEdit;
   TSpinEditFrame *BorderMaxSpinEdit;
   TSpinEditFrame *BorderPercentSpinEdit;
   TSpinEditFrame *PaddingLowSpinEdit;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall QSRadioButtonClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ImageResolutionRadioGroupClick(TObject *Sender);
    void __fastcall OKButtonClick(TObject *Sender);
    void __fastcall CancelButtonClick(TObject *Sender);
    void __fastcall GetFactorySettingsButtonClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall FormHide(TObject *Sender);

private:	// User declarations
    bool bUpdatingGUI;
    int iCurrentRes;
    SDRSIniParameters dipaCurrent[IF_IMAGE_RESOLUTION_COUNT];
    SDRSIniParameters dipaOrig[IF_IMAGE_RESOLUTION_COUNT];
    SDRSIniParameters dipaFactory[IF_IMAGE_RESOLUTION_COUNT];
    bool bAutoAccept;
    bool bPreloadInput;

    void UpdateGUI(void);
    void UpdateButtons(void);
    bool AreSettingsDifferentFromFactoryValues(int iRes);
    bool DidAnySettingsChange(void);
    bool DidSettingsChangeForResolution(int iRes);
    bool AreSettingsTheSame(
                const SDRSIniParameters &dipA,
                const SDRSIniParameters &dipB);
    void MotionChange(void);
    void QuickSelectChange(void);
    void SizingChange(TSpinEditFrame *spinEdit);
    bool CheckOkToCloseWithoutSavingChanges(void);
    void SaveAllValues(void);

    void SetSpinEditHooks();
    void RemoveSpinEditHooks();
    DEFINE_CBHOOK(MotionSpinEditChange, TFormDRSPreferences);
    DEFINE_CBHOOK(QSSpinEditChange, TFormDRSPreferences);
    DEFINE_CBHOOK(SizingSpinEditChange, TFormDRSPreferences);

public:		// User declarations
    __fastcall TFormDRSPreferences(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDRSPreferences *FormDRSPreferences;
//---------------------------------------------------------------------------
#endif
