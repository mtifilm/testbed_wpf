#include "MklMatrix.h"
#include "MTImalloc.h"

#define ALIGNMENT_BOUNDARY 64
#define ALIGN_TO_BOUNDARY(x) (((x + (ALIGNMENT_BOUNDARY - 1)) / ALIGNMENT_BOUNDARY) * ALIGNMENT_BOUNDARY)

//*******************************************************************
MklDataWrapper::MklDataWrapper(int nRows, int nCols)
: _nRows(nRows)
, _nCols(nCols)
, _blob(nullptr)
, _origin(nullptr)
, _rowPitchInFloats(0)
{
   if (nRows < 1 || nCols < 1)
   {
      return;
   }

   auto rowPitchInBytes =
#ifdef ALIGN_EACH_ROW
      (nCols <= (ALIGNMENT_BOUNDARY / sizeof(float))) ? nCols * sizeof(float) : ALIGN_TO_BOUNDARY(_nCols * sizeof(float));
#else
      nCols * sizeof(float);
#endif
   _rowPitchInFloats = rowPitchInBytes / sizeof(float);

   _blob = std::make_shared<void *>(mkl_malloc(_nRows * rowPitchInBytes, ALIGNMENT_BOUNDARY));

   _origin = reinterpret_cast<float *>(*_blob);
}

//*******************************************************************
MklDataWrapper::MklDataWrapper(const MklDataWrapper &orig)
: _nRows(orig._nRows)
, _nCols(orig._nCols)
, _blob(orig._blob)
, _origin(orig._origin)
, _rowPitchInFloats(orig._rowPitchInFloats)
{
}

//*******************************************************************
MklDataWrapper::~MklDataWrapper()
{
   _blob = nullptr;
}

//*******************************************************************
float* MklDataWrapper::operator[](int row) const
{
   if (row < 0 || row >= _nRows)
   {
      throw std::runtime_error("MklDataWrapper: row index out of range");
   }

   return _origin + (row * _rowPitchInFloats);
}

//*******************************************************************
MklDataWrapper& MklDataWrapper::operator =(const MklDataWrapper &rhs)
{
   _nRows = rhs._nRows;
   _nCols = rhs._nCols;
   _rowPitchInFloats = rhs._rowPitchInFloats;
   _blob = rhs._blob;
   _origin = rhs._origin;

   return *this;
}

//*******************************************************************
void MklDataWrapper::zero()
{
   if (_origin == nullptr || _nRows < 1 || _nCols < 1)
   {
      return;
   }

   MTImemset(_origin, 0, _nRows * _rowPitchInFloats * sizeof(float));
}

//*******************************************************************
void MklDataWrapper::ident()
{
   if (_nRows != _nCols)
   {
      throw std::runtime_error("Identity matrix must be square!");
   }

   if (_origin == nullptr || _nRows < 1 || _nCols < 1)
   {
      return;
   }

   zero();
   auto diagLen = _nRows;
   for (auto i = 0; i < diagLen; ++i)
   {
      (*this)[i][i] = 1.0F;
   }
}

//*******************************************************************
MklDataWrapper MklDataWrapper::clone(int nRows) const
{
   if (nRows < 1 || nRows > _nRows)
   {
      nRows = _nRows;
   }

   MklDataWrapper result(nRows, _nCols);

   if (_rowPitchInFloats == result._rowPitchInFloats)
   {
      MTImemcpy(result._origin, _origin, nRows * _rowPitchInFloats * sizeof(float));
   }
   else
   {
      for (auto row = 0; row < nRows; ++row)
      {
         MTImemcpy(result[row], (*this)[row], _nCols * sizeof(float));
      }
   }

   return result;
}

//*******************************************************************
//*******************************************************************
MklVector32f::MklVector32f()
{
}

//*******************************************************************
MklVector32f::MklVector32f(int n)
: _data(n)
{
}

//*******************************************************************
MklVector32f::MklVector32f(int n, const float *values)
: _data(n)
{
   MTImemcpy(dataPtr(), values, n * sizeof(float));
}

#ifndef ALIGN_EACH_MKLMATRIX_ROW
//*******************************************************************
MklVector32f::MklVector32f(const MklDataWrapper &data)
: _data(data)
{
   if (data._nRows < 1 || data._nRows < 1)
   {
      return;
   }

   _data._nRows *= _data._nCols;
   _data._nCols = 1;
   _data._rowPitchInFloats = 1;
}
#endif

//*******************************************************************
MklVector32f::MklVector32f(const MklDataWrapper &data, int row)
: _data(data)
{
   if (data._nRows < 1 || data._nRows < 1)
   {
      return;
   }

   if (row < 0 || row >= _data._nRows)
   {
      throw std::runtime_error("Index out of range for vector!");
   }

   if (row > 0)
   {
      _data._origin += row * data._rowPitchInFloats;
   }

   if (data._nCols != 1)
   {
      _data._nRows = _data._nCols;
      _data._nCols = 1;
      _data._rowPitchInFloats = 1;
   }
}

//*******************************************************************
MklVector32f::MklVector32f(const MklVector32f &v)
: _data(v._data)
{
}

//*******************************************************************
MklVector32f& MklVector32f::operator =(const MklVector32f &rhs)
{
   if (&rhs != this)
   {
      _data = rhs._data;
   }

   return *this;
}

//*******************************************************************
MklVector32f MklVector32f::operator+(const MklVector32f &rhs) const
{
   if (length() != rhs.length())
   {
      throw std::runtime_error("Vector add length mismatch!");
   }

   MklVector32f result(rhs.clone());

   auto n = length();
   auto a = 1.0F;
   auto x = dataPtr();
   auto incx = 1;
   auto y = result.dataPtr();
   auto incy = 1;
   cblas_saxpy(n, a, x, incx, y, incy);

   return result;
}

//*******************************************************************
MklVector32f MklVector32f::operator-(const MklVector32f &rhs) const
{
   if (length() != rhs.length())
   {
      throw std::runtime_error("Vector add length mismatch!");
   }

   // We need our data to fill the y vector, whose contents get destroyed.
   MklVector32f result(clone());

   auto n = length();
   auto a = -1.0F;
   auto x = rhs.dataPtr();
   auto incx = 1;
   auto y = result.dataPtr();
   auto incy = 1;
   cblas_saxpy(n, a, x, incx, y, incy);

   return result;
}

//*******************************************************************
MklVector32f MklVector32f::operator*(float scalar) const
{
   MklVector32f result(length());
   result.zero();

   auto n = length();
   auto a = scalar;
   auto x = dataPtr();
   auto incx = 1;
   auto y = result.dataPtr();
   auto incy = 1;
   cblas_saxpy(n, a, x, incx, y, incy);

   return result;
}

//*******************************************************************
MklVector32f operator*(float scalar, MklVector32f &rhs)
{
   // It's commutative.
   return rhs * scalar;
}

//*******************************************************************
bool MklVector32f::operator==(const MklVector32f &rhs) const
{
   if (length() != rhs.length())
   {
      return false;
   }

   for (auto i = 0; i < length(); ++i)
   {
      if ((*this)[i] != rhs[i])
      {
         return false;
      }
   }

   return true;
}

//*******************************************************************
void MklVector32f::zero(void)
{
   _data.zero();
}

//*******************************************************************
void MklVector32f::fill(float value)
{
   for (auto i = 0; i < length(); ++i)
   {
      (*this)[i] = value;
   }
}

//*******************************************************************
float MklVector32f::euclideanNorm() const
{
   auto n = length();
   auto x = dataPtr();
   auto incx = 1;
   auto result = cblas_snrm2(n, x, incx);

   return result;
}

//*******************************************************************
MklVector32f MklVector32f::clone() const
{
   return MklVector32f(_data.clone());
}

//*******************************************************************
void MklVector32f::copyDataFrom(const MklVector32f &v)
{
   auto copyBytes = std::min<int>(length(), v.length()) * sizeof(float);
   MTImemcpy(dataPtr(), v.dataPtr(), copyBytes);
}

//*******************************************************************
const MklDataWrapper &MklVector32f::getMklData() const
{
   return _data;
}


//*******************************************************************
//*******************************************************************
MklMatrix32f::MklMatrix32f()
{
}

//*******************************************************************
MklMatrix32f::MklMatrix32f(int r, int c)
: _data(r, c)
{
}

//*******************************************************************
MklMatrix32f::MklMatrix32f(const MklDataWrapper &data, int nRows)
: _data(data)
{
   if (nRows < 1 || nRows > data._nRows)
   {
      nRows = data._nRows;
   }

   _data._nRows = nRows;
}

//*******************************************************************
MklMatrix32f::MklMatrix32f(const MklMatrix32f &s)
: _data(s._data)
{
}

//*******************************************************************
MklMatrix32f::MklMatrix32f(const MklVector32f &v)
: _data(v.getMklData())
{
}

//*******************************************************************
MklMatrix32f& MklMatrix32f::operator =(const MklMatrix32f& rhs)
{
   if (&rhs != this)
   {
      _data = rhs._data;
   }

   return *this;
}

//*******************************************************************
MklVector32f MklMatrix32f::operator[](const int row) const
{
   // This will SHARE MEMORY with the matrix!
   return MklVector32f(_data, row);
}

//*******************************************************************
MklVector32f MklMatrix32f::operator*(const MklVector32f &rhs) const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklVector32f();
   }

   if (rhs.length() != cols())
   {
      throw std::runtime_error("For M*v, length of vector must equal number of columns in M!");
   }

   auto m = rows();
   auto n = 1;
   auto k = cols();
   MklVector32f result(rows());
   result.zero();

   auto alpha = 1.0F;
   auto beta = 0.0F;
   auto A = dataPtr();
   auto B = rhs.dataPtr();
   auto C = result.dataPtr();
   auto lda_A = lda();
   auto lda_B = 1;
   auto lda_C = 1;

   cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans, m, n, k, alpha, A, lda_A, B, lda_B, beta, C, lda_C);

   return result;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::operator*(float scalar) const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   auto m = rows();
   auto n = cols();
   auto k = rows();
   MklMatrix32f eye(m, k);
   eye.ident();
   MklMatrix32f result(m, n);
   result.zero();

   auto alpha = scalar;
   auto beta = 0.0F;
   auto A = eye.dataPtr();
   auto B = dataPtr();
   auto C = result.dataPtr();
   auto lda_A = eye.lda();
   auto lda_B = lda();
   auto lda_C = result.lda();

   cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A, lda_A, B, lda_B, beta, C, lda_C);

   return result;
}

//*******************************************************************
MklMatrix32f operator*(const float &scalar, const MklMatrix32f &rhs)
{
   // It's commutative.
   return rhs * scalar;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::operator*(const MklMatrix32f& rhs) const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   if (rhs.rows() != cols())
   {
      throw std::runtime_error("For matrix multiplication A*B, A.cols must equal B.rows!");
   }

   auto m = rows();
   auto n = rhs.cols();
   auto k = cols();
   MklMatrix32f result(m, n);
   result.zero();

   auto alpha = 1.0F;
   auto beta = 0.0F;
   auto A = dataPtr();
   auto B = rhs.dataPtr();
   auto C = result.dataPtr();
   auto lda_A = lda();
   auto lda_B = rhs.lda();
   auto lda_C = result.lda();

   cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A, lda_A, B, lda_B, beta, C, lda_C);

   return result;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::operator+(const MklMatrix32f& rhs) const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   if (rhs.rows() != rows() || rhs.cols() != cols())
   {
      throw std::runtime_error("For matrix addition A+B, A and B dimensions must match!");
   }


   MklMatrix32f result(rows(), cols());

   const char ordering = 'R';  // row-major
   const char transa = 'N';    // no transpose
   const char transb = 'N';    // no transpose
   auto m = size_t(rows());
   auto n = size_t(cols());
   const float alpha = 1.0F;
   const float beta = 1.0F;
   auto A = dataPtr();
   auto lda_a = lda();
   auto B = rhs.dataPtr();
   auto lda_b = rhs.lda();
   auto C = result.dataPtr();
   auto lda_c = result.lda();

   mkl_somatadd(ordering, transa, transb, m, n, alpha, A, lda_a, beta, B, lda_b, C, lda_c);

   return result;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::operator-(const MklMatrix32f& rhs) const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   if (rhs.rows() != rows() || rhs.cols() != cols())
   {
      throw std::runtime_error("For matrix subtraction A-B, A and B dimensions must match!");
   }

   MklMatrix32f result(rows(), cols());

   const char ordering = 'R';  // row-major
   const char transa = 'N';    // no transpose
   const char transb = 'N';    // no transpose
   auto m = size_t(rows());
   auto n = size_t(cols());
   const float alpha = 1.0F;
   const float beta = -1.0F;   // The only difference from addition
   auto A = dataPtr();
   auto lda_a = lda();
   auto B = rhs.dataPtr();
   auto lda_b = rhs.lda();
   auto C = result.dataPtr();
   auto lda_c = result.lda();

   mkl_somatadd(ordering, transa, transb, m, n, alpha, A, lda_a, beta, B, lda_b, C, lda_c);

   return result;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::transpose() const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   MklMatrix32f result(cols(), rows());

   const char ordering = 'R';  // row-major
   const char trans = 'T';     // do transpose
   auto r = size_t(rows());
   auto c = size_t(cols());
   const float alpha = 1.0F;
   auto A = dataPtr();
   auto lda_a = lda();
   auto B = result.dataPtr();
   auto lda_b = result.lda();

   mkl_somatcopy(ordering, trans, r, c, alpha, A, lda_a, B, lda_b);

   return result;
}

//*******************************************************************
MklMatrix32f MklMatrix32f::mldivide(const MklMatrix32f &rhs) const
{
   if (rows() < 1 || cols() < 1 || rhs.rows() < 1 || rhs.cols() < 1)
   {
      return MklMatrix32f();
   }

   if (rows() != cols() || rows() != rhs.rows())
   {
      throw std::runtime_error("Can only mldivide a square matrix by one with same number of rows");
   }

   // clone data because LAPACK calls are destructive.
   MklMatrix32f A(clone());
   MklMatrix32f B(rhs.clone());

   auto n = rows();
   auto nrhs = rhs.cols();
   auto a = A.dataPtr();
   auto b = B.dataPtr();
   auto lda_a = lda();
   auto lda_b = rhs.lda();
   const char noTranspose = 'N';
	lapack_int *ipiv = (lapack_int *)malloc(n * sizeof(lapack_int));

   // Replace A with LU factorization of A.
   LAPACKE_sgetrf(LAPACK_ROW_MAJOR, n, n, a, lda_a, ipiv);

   // Solve for B, replacing B. One set of sulutions for each column of B.
   LAPACKE_sgetrs(LAPACK_ROW_MAJOR, noTranspose, n, nrhs, a, lda_a, ipiv, b, lda_b);

   free(ipiv);
   return B;
}

//*******************************************************************
MklVector32f MklMatrix32f::mldivide(const MklVector32f &rhs) const
{
   if (rows() < 1 || cols() < 1 || rhs.length() < 1)
   {
      return MklVector32f();
   }

   if (rows() != cols() || rows() != rhs.length())
   {
      throw std::runtime_error("Can only mldivide a square matrix by a vector with length = rows");
   }

   // Turn the vector into a 1-col matrix
   MklMatrix32f B(rhs);

   auto result = mldivide(B);
   return MklVector32f(result._data);
}

//*******************************************************************
MklMatrix32f MklMatrix32f::invert() const
{
   if (rows() < 1 || cols() < 1)
   {
      return MklMatrix32f();
   }

   if (rows() != cols())
   {
      throw std::runtime_error("Matrix to invert must be square!");
   }

   // Clone data because LAPACK calls are destructive.
   MklMatrix32f Ainv(clone());

   auto n = rows();
   auto a = Ainv.dataPtr();
   auto lda_a = Ainv.lda();
	lapack_int *ipiv = (lapack_int *)malloc(n * sizeof(lapack_int));
   LAPACKE_sgetrf(LAPACK_ROW_MAJOR, n, n, a, lda_a, ipiv);
   LAPACKE_sgetri(LAPACK_ROW_MAJOR, n, a, lda_a, ipiv);
   free(ipiv);

   return Ainv;
}

//*******************************************************************
void MklMatrix32f::zero()
{
   if (rows() < 1 || cols() < 1)
   {
      return;
   }

   _data.zero();
}

//*******************************************************************
void MklMatrix32f::ident()
{
   if (rows() < 1 || cols() < 1)
   {
      return;
   }

   _data.ident();
}

//*******************************************************************
MklMatrix32f MklMatrix32f::clone() const
{
   return MklMatrix32f(_data.clone());
}

//*******************************************************************

