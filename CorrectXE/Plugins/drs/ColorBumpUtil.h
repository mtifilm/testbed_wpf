#ifndef __COLOR_BUMP_UTIL_H__
#define __COLOR_BUMP_UTIL_H__

#include "IppArray.h"
#include "tarray.h"
#include "Ippheaders.h"
#include "MklMatrix.h"

struct BMStruct
{
	int nRows;
	int nCols;
	matrix<int> row0;
	matrix<int> row1;
	matrix<int> col0;
	matrix<int> col1;
	matrix<int> rowOff;
	matrix<int> colOff;
	////matrix<int> diff;
	matrix<double> score;

	BMStruct(int rows, int cols)
		: nRows(rows)
		, nCols(cols)
      , row0(rows, cols)
		, row1(rows, cols)
		, col0(rows, cols)
		, col1(rows, cols)
		, rowOff(rows, cols)
		, colOff(rows, cols)
		////, diff(rows, cols)
		, score(rows, cols)
	{
		row0.zero();
		row1.zero();
		col0.zero();
		col1.zero();
		rowOff.zero();
		colOff.zero();
		////diff.zero();
		score.zero();
	}
};

enum MotionDirection
{
	PastOnly = -1,
	Neither = 0,
	FutureOnly = 1
};

struct RatioStruct
{
	int nRows;
	int nCols;
	MklMatrix32f row;
	MklMatrix32f col;
	MklMatrix32f mean;

	RatioStruct(int rows, int cols)
		: nRows(rows)
		, nCols(cols)
      , row(rows, cols)
		, col(rows, cols)
		, mean(rows, cols)
	{
		row.zero();
		col.zero();
		mean.zero();
	}
};

namespace ColorBumpUtil
{
   BMStruct* BlockMatch(const Ipp32fArray& src0, const Ipp32fArray& src1, const Ipp32fArray& src2);
   MotionDirection ChooseMotion(const BMStruct& BmB, const BMStruct& BmA);
   RatioStruct* FindRatio(const Ipp32fArray& srcC, const Ipp32fArray& srcT, const BMStruct& BM);
   ////Ipp32fArray FindSurface(const RatioStruct& RAT);
   Ipp32fArray FindSurfaceTPS(const RatioStruct& RAT, double scale);

   Ipp32fArray ExtractYSignal(const Ipp32fArray& src, bool isMonochrome);
   Ipp32fArray ConvertYToRGB(const Ipp32fArray& src);
   IppStatus IppResize_C3R(Ipp32f* pSrc, IppiSize srcSize, Ipp32s srcStep, Ipp32f* pDst, IppiSize dstSize, Ipp32s dstStep);
}

#endif