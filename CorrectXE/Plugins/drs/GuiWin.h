//---------------------------------------------------------------------------

#ifndef GuiWinH
#define GuiWinH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "MTIWinInterface.h"
#include "ExecStatusBarUnit.h"
#include "cspin.h"
#include "ColorPanel.h"
#include <Vcl.Graphics.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <System.ImageList.hpp>
#include "CompactTrackEditFrameUnit.h"

// The public interface
// No need to class this because we link in when compilied
//
extern bool ShowDRSGui(void);
//extern bool HideDRSGUIQuery(void);
extern bool HideDRSGui(void);
extern void DestroyDRSGui(void);
extern void CreateDRSGui(void);
extern void UpdateDRSGuiButtons(void);
extern bool IsToolVisible(void);
extern void MarksChanged(void);
extern void ShowPreferencesWindow(void);
extern int  GetReplaceModeSourceFrameOption();
extern void SetStatusIdle();
extern bool IsColorBumpSelected();
//---------------------------------------------------------------------------

class TDRSForm : public TMTIForm
{
__published:	// IDE-managed Components
        TPopupMenu *ToolButtonOptions;
   TToolButton *ReviewLastButton;
   TToolButton *ReviewNextButton;
    TMenuItem *PreferencesMenuItem;
	TColorPanel *DRSControlPanel;
    TPanel *ControlCenteringPanel;
        TPanel *TitlePanel;
    TLabel *Label1;
    TPanel *ReviewTBContainerPanel;
    TToolBar *ReviewToolBar;
    TPanel *DrawTBContainerPanel;
    TPanel *OverrideTBContainerPanel;
    TPanel *DRSTBContainerPanel;
	TGroupBox *ButtonContainerPanel;
    TSpeedButton *LowMotionButton;
    TSpeedButton *HighMotionButton;
    TSpeedButton *HardMotionButton;
    TSpeedButton *ReplaceButton;
    TSpeedButton *PastButton;
    TSpeedButton *BothButton;
    TSpeedButton *FutureButton;
    TSpeedButton *BlurButton;
    TSpeedButton *EdgePreserveButton;
    TSpeedButton *DrawRectangleButton;
    TSpeedButton *DrawLassoButton;
    TSpeedButton *DrawBezierButton;
    TSpeedButton *MoveOverButton;
	TGroupBox *HR1;
    TSpeedButton *MoveUnderButton;
   TSpeedButton *GrainButton;
    TSpeedButton *RotateOverButton;
    TSpeedButton *RotateUnderButton;
   TSpeedButton *BorderButton;
   TSpeedButton *TransparencyButton;
	TGroupBox *HR3;
    TPanel *DisabledPastFuturePanel;
    TPanel *DisabledBlurEdgePreservePanel;
        TCheckBox *BorderOnCheckbox;
        TColorPanel *MoveOverOverrideXPanel;
        TLabel *MoveOverXValueLabel;
        TColorPanel *MoveOverOverrideYPanel;
        TLabel *MoveOverYValueLabel;
        TColorPanel *RotateOverOverridePanel;
        TColorPanel *MoveUnderOverrideXPanel;
        TColorPanel *MoveUnderOverrideYPanel;
        TColorPanel *RotateUnderOverridePanel;
        TColorPanel *BorderOverridePanel;
        TColorPanel *GrainOverridePanel;
        TColorPanel *TransparencyOverridePanel;
        TLabel *RotateOverValueLabel;
        TLabel *MoveUnderXValueLabel;
        TLabel *MoveUnderYValueLabel;
        TLabel *RotateUnderValueLabel;
        TLabel *BorderValueLabel;
        TLabel *GrainValueLabel;
        TLabel *TransparencyValueLabel;
    TCheckBox *StickyOverridesCheckBox;
    TLabel *EditPreferencesLabel;
        TCheckBox *MoveOverStickyCheckBox;
        TCheckBox *MoveUnderStickyCheckBox;
        TCheckBox *GrainStickyCheckBox;
        TCheckBox *RotateOverStickyCheckBox;
        TCheckBox *RotateUnderStickyCheckBox;
        TCheckBox *BorderStickyCheckBox;
        TCheckBox *TransparencyStickyCheckBox;
        TPanel *MoveUnderStickyDisabledPanel;
        TPanel *GrainStickyDisabledPanel;
        TPanel *RotateOverStickyDisabledPanel;
        TPanel *RotateUnderStickyDisabledPanel;
   TPanel *BorderStickyDisabledPanel;
        TPanel *TransparencyStickyDisabledPanel;
        TPanel *PastFuturePanel;
        TPanel *DisabledNoneLowHighPanel;
        TPanel *NoneLowHighPanel;
        TSpeedButton *ReplaceLowButton;
        TSpeedButton *ReplaceHighButton;
        TSpeedButton *ReplaceNoneButton;
        TPanel *BlurEdgePreservePanel;
	TGroupBox *HR4;
	TGroupBox *HR6;
	TGroupBox *HR7;
	TGroupBox *PseudoHRPanel;
        TPanel *MotionButtonPanel;
        TGroupBox *MacroGroupBox;
        TSpeedButton *RunMacroButton;
        TSpeedButton *StopMacroButton;
   TGroupBox *HR8;
	TPanel *MoveOverStickyDisabledPanel;
	TImage *BlurEdgePreserveImage;
	TImage *PastBothFutureImage;
	TImage *NoneLowHighImage;
   TExecStatusBarFrame *ExecStatusBar;
	TSpeedButton *ColorBumpButton;
   TPanel *DisabledCBPastAutoFuturePanel;
   TPanel *CBOptionsPanel;
   TSpeedButton *CBFutureOnlyButton;
   TSpeedButton *CBPastOnlyButton;
   TGroupBox *HR9;
   TImage *DisabledCBPastAutoFutureImage;
   TSpeedButton *CBAutoButton;
   TComboBox *ReplaceSrcFramesComboBox;
   TLabel *ReplaceSrcFramesLabel;
	TPanel *GrainPresetsPanel;
	TRadioButton *GrainPresetsProjectRadioButton;
	TRadioButton *GrainPresetsClipRadioButton;
	TSpeedButton *GrainPreset1Button;
	TSpeedButton *GrainPreset2Button;
   TCheckBox *GrainOverrideUsePresetsCheckBox;
   TGroupBox *AutoReplaceGroupBox;
   TSpeedButton *RunAutoReplaceSpeedButton;
   TSpeedButton *StopAutoReplaceSpeedButton;
   TSpeedButton *ColorButton;
   TCheckBox *ColorStickyCheckBox;
   TPanel *ColorStickyDisabledPanel;
   TPanel *ColorSubOverridesPanel;
   TSpeedButton *ColorBrightnessButton;
   TSpeedButton *ColorSaturationButton;
   TSpeedButton *ColorRedButton;
   TSpeedButton *ColorGreenButton;
   TSpeedButton *ColorBlueButton;
   TColorPanel *ColorBrightnessOverridePanel;
   TLabel *ColorBrightnessValueLabel;
   TColorPanel *ColorSaturationOverridePanel;
   TLabel *ColorSaturationValueLabel;
   TColorPanel *ColorRedOverridePanel;
   TLabel *ColorRedValueLabel;
   TColorPanel *ColorGreenOverridePanel;
   TLabel *ColorGreenValueLabel;
   TColorPanel *ColorBlueOverridePanel;
   TLabel *ColorBlueValueLabel;
        void __fastcall GenericButtonClick(TObject *Sender);
    void __fastcall LowHighMotionButtonClick(TObject *Sender);
    void __fastcall HardMotionButtonClick(TObject *Sender);
        void __fastcall ClearMoveOverOverrideXButtonClick(TObject *Sender);
        void __fastcall ClearMoveOverOverrideYButtonClick(TObject *Sender);
        void __fastcall ClearMoveUnderOverrideXButtonClick(
          TObject *Sender);
        void __fastcall ClearMoveUnderOverrideYButtonClick(
          TObject *Sender);
        void __fastcall ClearRotateOverOverrideButtonClick(
          TObject *Sender);
        void __fastcall ClearRotateUnderOverrideButtonClick(
          TObject *Sender);
        void __fastcall ClearColorBrightnessOverrideButtonClick(TObject *Sender);
        void __fastcall ClearBorderWidthButtonClick(TObject *Sender);
        void __fastcall ClearGrainOverrideButtonClick(TObject *Sender);
        void __fastcall ClearTransparencyOverrideButtonClick(
          TObject *Sender);
        void __fastcall BorderOnCheckboxClick(TObject *Sender);
    void __fastcall StickyOverridesCheckBoxClick(TObject *Sender);
    void __fastcall DRSControlPanelEnter(TObject *Sender);
    void __fastcall EditPreferencesLabelMouseEnter(TObject *Sender);
    void __fastcall EditPreferencesLabelMouseLeave(TObject *Sender);
    void __fastcall EditPreferencesClick(TObject *Sender);
        void __fastcall DRSControlPanelMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall OverrideButtonMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall OverrideButtonMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall OverrideButtonMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall DRSControlPanelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall DRSControlPanelMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall SingleStickyCheckBoxClick(TObject *Sender);
        void __fastcall ReplaceButtonClick(TObject *Sender);
        void __fastcall RunMacroButtonClick(TObject *Sender);
        void __fastcall StopMacroButtonClick(TObject *Sender);
	void __fastcall ColorBumpButtonClick(TObject *Sender);
   void __fastcall CBAutoDirCheckBoxClick(TObject *Sender);
   void __fastcall CBFutureOnlyButtonClick(TObject *Sender);
   void __fastcall CBPastOnlyButtonClick(TObject *Sender);
   void __fastcall CBAutoButtonClick(TObject *Sender);
   void __fastcall ReplaceSrcFramesComboBoxChange(TObject *Sender);
	void __fastcall GrainPresetsButtonClick(TObject *Sender);
   void __fastcall GrainOverrideUsePresetsCheckBoxClick(TObject *Sender);
   void __fastcall StopAutoReplaceSpeedButtonClick(TObject *Sender);
   void __fastcall RunAutoReplaceSpeedButtonClick(TObject *Sender);
   void __fastcall ClearColorSaturationOverrideButtonClick(TObject *Sender);
   void __fastcall ClearColorRedOverrideButtonClick(TObject *Sender);
   void __fastcall ClearColorGreenOverrideButtonClick(TObject *Sender);
   void __fastcall ClearColorBlueOverrideButtonClick(TObject *Sender);

private:	// User declarations

    bool GUIInhibit;
    bool MouseCapturedByOverrideButton;
    int ReplaceSrcFrameOldItemMode;
    int _grainPresetOverrideCurrentValue;
    DEFINE_CBHOOK(GrainPresetsChanged, TDRSForm);

    void ExecuteToolCommand(int command);
    void UpdateOneClearButtonAppearance(TPanel *backPanel,
                                        TSpeedButton *clearButton,
                                        bool enableClear);
    void UpdateClearButtonAppearances();
    void ConformAutoReplaceControls();

public:	// User declarations

    // These are called from the stupid global entry points.
	 void formCreate();
	 void formShow();
	 void formDestroy();
	 void UpdateVisualButtons(void);
	 void MarksChanged(void);

	 __fastcall TDRSForm(TComponent* Owner);

};

//---------------------------------------------------------------------------
extern PACKAGE TDRSForm *DRSForm;
//---------------------------------------------------------------------------
#endif
