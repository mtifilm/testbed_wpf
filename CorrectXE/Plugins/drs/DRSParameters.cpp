#include "DRSTool.h"
#include "DRSParameters.h"


CDRSParameters::CDRSParameters()
{
}  /* CDRSParameters */

CDRSParameters::~CDRSParameters()
{
}  /* ~CDRSParameters */

void CDRSParameters::setFileIniParameters (int iRes,
		const SDRSIniParameters &dip)
{
  dipaFile[iRes] = dip;
}  /* setFileIniParameters */

void CDRSParameters::setFileStateParameters (const SDRSStateParameters &dsp)
{
  dspFile = dsp;
}  /* setFileStateParameters */

SDRSIniParameters CDRSParameters::getFileIniParameters (int iRes)
{
  return dipaFile[iRes];
}  /* getFileIniParameters */

SDRSStateParameters CDRSParameters::getFileStateParameters ()
{
  return dspFile;
}  /* getFileStateParameters */

void CDRSParameters::setActiveIniParameters (int iRes,
		const SDRSIniParameters &dip)
{
  dipaActive[iRes] = dip;
}  /* setActiveIniParameters */

SDRSIniParameters CDRSParameters::getActiveIniParameters (int iRes)
{
  return dipaActive[iRes];
}  /* getActiveIniParameters */

SDRSIniParameters CDRSParameters::getFactoryIniParameters (int iRes)
{
  return dipaFactory[iRes];
}  /* getFactoryIniParameters */

bool CDRSParameters::writeIniParameters (int iRes)
{
  string strParamSection;
  CIniFile *ini = openIni (iRes, strParamSection);

  if (ini == 0)
    return false;

  // write out the generic parameters
  ini->WriteInteger (strParamSection, "BorderSize",   // Really BorderMax
		dipaFile[iRes].lBorderSize);
  ini->WriteInteger (strParamSection, "BorderMin",
		dipaFile[iRes].lBorderMin);
  ini->WriteInteger (strParamSection, "BorderPercent",
		dipaFile[iRes].lBorderPercent);
  ini->WriteInteger (strParamSection, "MotionOffsetLow",
		dipaFile[iRes].lMotionOffsetLow);
  ini->WriteInteger (strParamSection, "MotionOffsetHigh",
		dipaFile[iRes].lMotionOffsetHigh);
  ////////
  // Import motion offset is now the same as regular motion offset
  ini->WriteInteger (strParamSection, "ImportOffsetLow",
		dipaFile[iRes].lMotionOffsetLow /* lImportOffsetLow */);
  ini->WriteInteger (strParamSection, "ImportOffsetHigh",
		dipaFile[iRes].lMotionOffsetHigh /* lImportOffsetHigh */);
  //////
  ini->WriteInteger (strParamSection, "PaddingAmountLow",
		dipaFile[iRes].lPaddingAmountLow);
  ini->WriteInteger (strParamSection, "PaddingAmountHigh",
		dipaFile[iRes].lPaddingAmountHigh);
  ini->WriteInteger (strParamSection, "SurroundRow",
		dipaFile[iRes].lSurroundRadius);
  ini->WriteInteger (strParamSection, "RowAccel",
		dipaFile[iRes].lRowAccel);
  ini->WriteInteger (strParamSection, "ColAccel",
		dipaFile[iRes].lColAccel);
  ini->WriteBool (strParamSection, "FieldRepeat",
		dipaFile[iRes].bFieldRepeat);
  ini->WriteBool(strParamSection, "CommandFile",
        dipaFile[iRes].bCommandFile); 
  ini->WriteInteger(strParamSection, "GrainSize",
        dipaFile[iRes].lGrainSize); 
  ini->WriteInteger (strParamSection, "QuickSelect",
        dipaFile[iRes].iQuickSelect);
  ini->WriteInteger (strParamSection, "QSRect0Size",
        dipaFile[iRes].iQSRect0Size);
  ini->WriteInteger (strParamSection, "QSRect1Size",
        dipaFile[iRes].iQSRect1Size);
  ini->WriteInteger (strParamSection, "QSRect2Size",
        dipaFile[iRes].iQSRect2Size);
  ini->WriteInteger (strParamSection, "QSEllipse0Size",
		dipaFile[iRes].iQSEllipse0Size);
  ini->WriteInteger (strParamSection, "QSEllipse1Size",
		dipaFile[iRes].iQSEllipse1Size);
  ini->WriteInteger (strParamSection, "QSEllipse2Size",
		dipaFile[iRes].iQSEllipse2Size);


  delete ini;

  return true;
}  /* writeIniParameters */

bool CDRSParameters::writeIniParameters ()
{
  for (int iRes = 0; iRes < IF_IMAGE_RESOLUTION_COUNT; iRes++)
   {
    if (!writeIniParameters (iRes))
      return false;
   }
  return true;
}  /* writeIniParameters */

bool CDRSParameters::readIniParameters (int iRes)
{
  // set to factor defaults
  if (!factoryIniParameters (iRes))
    return false;

  // read in from ini file
  string strParamSection;
  CIniFile *ini = openIni (iRes, strParamSection);

  if (ini == 0)
    return false;

  // read in the generic parameters
  dipaFile[iRes].lBorderSize = ini->ReadIntegerCreate (strParamSection,
		"BorderSize", dipaFactory[iRes].lBorderSize);
  dipaFile[iRes].lBorderMin = ini->ReadIntegerCreate (strParamSection,
		"BorderMin", dipaFactory[iRes].lBorderMin);
  dipaFile[iRes].lBorderPercent = ini->ReadIntegerCreate (strParamSection,
		"BorderPercent", dipaFactory[iRes].lBorderPercent);
  dipaFile[iRes].lMotionOffsetLow = ini->ReadIntegerCreate (strParamSection,
		"MotionOffsetLow", dipaFactory[iRes].lMotionOffsetLow);
  dipaFile[iRes].lMotionOffsetHigh = ini->ReadIntegerCreate (strParamSection,
		"MotionOffsetHigh", dipaFactory[iRes].lMotionOffsetHigh);
  /////////////////
  // Import motion offsets are now the same as regular motion offsets
  //dipaFile[iRes].lImportOffsetLow = ini->ReadIntegerCreate (strParamSection,
		//"ImportOffsetLow", dipaFactory[iRes].lImportOffsetLow);
  //dipaFile[iRes].lImportOffsetLow = ini->ReadIntegerCreate (strParamSection,
		//"ImportOffsetLow", dipaFactory[iRes].lImportOffsetLow);
  dipaFile[iRes].lImportOffsetLow = dipaFile[iRes].lMotionOffsetLow ;
  dipaFile[iRes].lImportOffsetHigh = dipaFile[iRes].lMotionOffsetHigh;
  ////////////////

  dipaFile[iRes].lPaddingAmountLow = ini->ReadIntegerCreate (strParamSection,
		"PaddingAmountLow", dipaFactory[iRes].lPaddingAmountLow);
  dipaFile[iRes].lPaddingAmountHigh = ini->ReadIntegerCreate (strParamSection,
		"PaddingAmountHigh", dipaFactory[iRes].lPaddingAmountHigh);
  dipaFile[iRes].lSurroundRadius = ini->ReadIntegerCreate (strParamSection,
		"SurroundRow", dipaFactory[iRes].lSurroundRadius);
  dipaFile[iRes].lRowAccel = ini->ReadIntegerCreate (strParamSection,
		"RowAccel", dipaFactory[iRes].lRowAccel);
  dipaFile[iRes].lColAccel = ini->ReadIntegerCreate (strParamSection,
		"ColAccel", dipaFactory[iRes].lColAccel);
  dipaFile[iRes].bFieldRepeat = ini->ReadBoolCreate (strParamSection,
		"FieldRepeat", dipaFactory[iRes].bFieldRepeat);
  dipaFile[iRes].bCommandFile = ini->ReadBoolCreate (strParamSection,
		"CommandFile", dipaFactory[iRes].bCommandFile);
  dipaFile[iRes].lGrainSize = ini->ReadIntegerCreate (strParamSection,
		"GrainSize", dipaFactory[iRes].lGrainSize);
  dipaFile[iRes].iQuickSelect = ini->ReadIntegerCreate (strParamSection,
		"QuickSelect", dipaFactory[iRes].iQuickSelect);
  dipaFile[iRes].iQSRect0Size = ini->ReadIntegerCreate (strParamSection,
		"QSRect0Size", dipaFactory[iRes].iQSRect0Size);
  dipaFile[iRes].iQSRect1Size = ini->ReadIntegerCreate (strParamSection,
		"QSRect1Size", dipaFactory[iRes].iQSRect1Size);
  dipaFile[iRes].iQSRect2Size = ini->ReadIntegerCreate (strParamSection,
		"QSRect2Size", dipaFactory[iRes].iQSRect2Size);
  dipaFile[iRes].iQSEllipse0Size = ini->ReadIntegerCreate (strParamSection,
		"QSEllipse0Size", dipaFactory[iRes].iQSEllipse0Size);
  dipaFile[iRes].iQSEllipse1Size = ini->ReadIntegerCreate (strParamSection,
		"QSEllipse1Size", dipaFactory[iRes].iQSEllipse1Size);
  dipaFile[iRes].iQSEllipse2Size = ini->ReadIntegerCreate (strParamSection,
		"QSEllipse2Size", dipaFactory[iRes].iQSEllipse2Size);

  // copy over the file values to the active
  dipaActive[iRes] = dipaFile[iRes];

  delete ini;

  return true;
}  /* readIniParameters */

bool CDRSParameters::readIniParameters ()
{
  for (int iRes = 0; iRes < IF_IMAGE_RESOLUTION_COUNT; iRes++)
   {
    if (!readIniParameters (iRes))
      return false;
   }

  return true;
}  /* readIniParameters */

bool CDRSParameters::writeStateParameters ()
{
  string strParamSection;
  CIniFile *ini = openIni (strParamSection);

  if (ini == 0)
    return false;

  // write out the tag parameters
  ini->WriteInteger (strParamSection, "MainProcessType",
		dspFile.iMainProcessType);
  ini->WriteInteger (strParamSection, "SubProcessType",
		dspFile.iSubProcessType);
  ini->WriteInteger (strParamSection, "SelectMode", dspFile.iSelectMode);
  ini->WriteInteger (strParamSection, "BlendType", dspFile.iBlendType);
  ini->WriteInteger (strParamSection, "OverrideMode", dspFile.iOverrideMode);
  ini->WriteBool (strParamSection, "StickyOverrides",
                dspFile.bOverridesAreSticky);
  ini->WriteBool (strParamSection, "StickyMoveOver",
                dspFile.bMoveOverIsSticky);
  ini->WriteBool (strParamSection, "StickyMoveUnder",
                dspFile.bMoveUnderIsSticky);
  ini->WriteBool (strParamSection, "StickyRotateOver",
                dspFile.bRotateOverIsSticky);
  ini->WriteBool (strParamSection, "StickyRotateUnder",
                dspFile.bRotateUnderIsSticky);
  ini->WriteBool (strParamSection, "StickyColor",
                dspFile.bColorIsSticky);
  ini->WriteBool (strParamSection, "StickyBorder",
                dspFile.bBorderIsSticky);
  ini->WriteBool (strParamSection, "StickyGrain",
                dspFile.bGrainIsSticky);
  ini->WriteBool (strParamSection, "StickyTransparency",
                dspFile.bTransparencyIsSticky);

  delete ini;

  return true;
}  /* writeStateParameters */

bool CDRSParameters::readStateParameters ()
{
  // read in from ini file
  string strParamSection;
  CIniFile *ini = openIni (strParamSection);

  if (ini == 0)
    return false;

  dspFile.iMainProcessType = ini->ReadInteger (strParamSection,
		"MainProcessType", DRS_PROCESS_LOW_MOTION);
  dspFile.iSubProcessType = ini->ReadInteger (strParamSection,
		"SubProcessType", DRS_SUBPROCESS_NONE);
  dspFile.iSelectMode = ini->ReadInteger (strParamSection, "SelectMode",
                                          SELECT_REGION_RECT);
#ifdef FRAMEWRX
  dspFile.iBlendType = BLEND_TYPE_LINEAR;    // Always LINEAR
#else
  dspFile.iBlendType = (EBlendType)ini->ReadInteger (strParamSection,
                "BlendType", BLEND_TYPE_LINEAR);
#endif // FRAMEWRX
  dspFile.iOverrideMode = ini->ReadInteger (strParamSection, "OverrideMode",
		OM_MOVE_UNDER);

  dspFile.bOverridesAreSticky = ini->ReadBool (strParamSection,
        "StickyOverrides", true);
  dspFile.bMoveOverIsSticky = ini->ReadBool (strParamSection,
        "StickyMoveOver", true);
  dspFile.bMoveUnderIsSticky = ini->ReadBool (strParamSection,
        "StickyMoveUnder", true);
  dspFile.bRotateOverIsSticky = ini->ReadBool (strParamSection,
        "StickyRotateOver", true);
  dspFile.bRotateUnderIsSticky = ini->ReadBool (strParamSection,
        "StickyRotateUnder", true);
  dspFile.bColorIsSticky = ini->ReadBool (strParamSection,
        "StickyColor", true);
  dspFile.bBorderIsSticky = ini->ReadBool (strParamSection,
        "StickyBorder", true);
  dspFile.bGrainIsSticky = ini->ReadBool (strParamSection,
        "StickyGrain", true);
  dspFile.bTransparencyIsSticky = ini->ReadBool (strParamSection,
        "StickyTransparency", true);

  delete ini;

  return true;
}  /* readStateParameters */

bool CDRSParameters::factoryIniParameters (int iRes)
{
  // set the generic parameters
  dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_SD;
  dipaFactory[iRes].lBorderMin = DEFAULT_BORDER_MIN_SD;
  dipaFactory[iRes].lBorderPercent = DEFAULT_BORDER_PERCENT;
  dipaFactory[iRes].lMotionOffsetLow = DEFAULT_MOTION_OFFSET_LOW;
  dipaFactory[iRes].lMotionOffsetHigh = DEFAULT_MOTION_OFFSET_HIGH;
  dipaFactory[iRes].lImportOffsetLow = DEFAULT_MOTION_OFFSET_LOW; //DEFAULT_IMPORT_OFFSET_LOW;
  dipaFactory[iRes].lImportOffsetHigh = DEFAULT_MOTION_OFFSET_HIGH; //DEFAULT_IMPORT_OFFSET_HIGH;
  dipaFactory[iRes].lPaddingAmountLow = DEFAULT_PADDING_AMOUNT_LOW;
  dipaFactory[iRes].lPaddingAmountHigh = DEFAULT_PADDING_AMOUNT_HIGH;
  dipaFactory[iRes].lSurroundRadius = DEFAULT_SURROUND_RADIUS;
  dipaFactory[iRes].lRowAccel = DEFAULT_ROW_ACCEL;
  dipaFactory[iRes].lColAccel = DEFAULT_COL_ACCEL;
  dipaFactory[iRes].bFieldRepeat = false;
  dipaFactory[iRes].bCommandFile = false;
  dipaFactory[iRes].lGrainSize = 50;
  dipaFactory[iRes].iQuickSelect = QS_SELECT_RECT;
  dipaFactory[iRes].iQSRect0Size = 10;
  dipaFactory[iRes].iQSRect1Size = 10;
  dipaFactory[iRes].iQSRect2Size = 10;
  dipaFactory[iRes].iQSEllipse0Size = 10;
  dipaFactory[iRes].iQSEllipse1Size = 10;
  dipaFactory[iRes].iQSEllipse2Size = 10;

  // set override values
  switch (iRes)
   {
    case IF_IMAGE_RESOLUTION_STANDARD_DEF:
    break;
    case IF_IMAGE_RESOLUTION_HIGH_DEF:
      dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_HD;
      dipaFactory[iRes].lBorderMin  = DEFAULT_BORDER_MIN_HD;
      // dipaFactory[iRes].lBorderPercent += 0;     // Resolution-independent
      dipaFactory[iRes].lSurroundRadius *= 2;
      dipaFactory[iRes].iQSRect0Size += 6;
      dipaFactory[iRes].iQSRect1Size += 6;
      dipaFactory[iRes].iQSRect2Size += 6;
      dipaFactory[iRes].iQSEllipse0Size += 6;
      dipaFactory[iRes].iQSEllipse1Size += 6;
      dipaFactory[iRes].iQSEllipse2Size += 6;
    break;
    case IF_IMAGE_RESOLUTION_2K_DATA:
      dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_2K;
      dipaFactory[iRes].lBorderMin  = DEFAULT_BORDER_MIN_2K;
      // dipaFactory[iRes].lBorderPercent += 0;     // Resolution-independent
      dipaFactory[iRes].lSurroundRadius *= 3;
      dipaFactory[iRes].iQSRect0Size += 8;
      dipaFactory[iRes].iQSRect1Size += 8;
      dipaFactory[iRes].iQSRect2Size += 8;
      dipaFactory[iRes].iQSEllipse0Size += 8;
      dipaFactory[iRes].iQSEllipse1Size += 8;
      dipaFactory[iRes].iQSEllipse2Size += 8;
    break;
    case IF_IMAGE_RESOLUTION_4K_DATA:
      dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_4K;
      dipaFactory[iRes].lBorderMin  = DEFAULT_BORDER_MIN_4K;
      // dipaFactory[iRes].lBorderPercent += 0;     // Resolution-independent
      dipaFactory[iRes].lSurroundRadius *= 4;
      dipaFactory[iRes].iQSRect0Size += 12;
      dipaFactory[iRes].iQSRect1Size += 12;
      dipaFactory[iRes].iQSRect2Size += 12;
      dipaFactory[iRes].iQSEllipse0Size += 12;
      dipaFactory[iRes].iQSEllipse1Size += 12;
      dipaFactory[iRes].iQSEllipse2Size += 12;
    break;
    case IF_IMAGE_RESOLUTION_6K_DATA:
      dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_6K;
      dipaFactory[iRes].lBorderMin  = DEFAULT_BORDER_MIN_6K;
      // dipaFactory[iRes].lBorderPercent += 0;     // Resolution-independent
      dipaFactory[iRes].lSurroundRadius *= 4;
      dipaFactory[iRes].iQSRect0Size += 16;
      dipaFactory[iRes].iQSRect1Size += 16;
      dipaFactory[iRes].iQSRect2Size += 16;
      dipaFactory[iRes].iQSEllipse0Size += 16;
      dipaFactory[iRes].iQSEllipse1Size += 16;
      dipaFactory[iRes].iQSEllipse2Size += 16;
    break;
    case IF_IMAGE_RESOLUTION_8K_DATA:
      dipaFactory[iRes].lBorderSize = DEFAULT_BORDER_MAX_8K;
      dipaFactory[iRes].lBorderMin  = DEFAULT_BORDER_MIN_8K;
      // dipaFactory[iRes].lBorderPercent += 0;     // Resolution-independent
      dipaFactory[iRes].lSurroundRadius *= 4;
      dipaFactory[iRes].iQSRect0Size += 22;
      dipaFactory[iRes].iQSRect1Size += 22;
      dipaFactory[iRes].iQSRect2Size += 22;
      dipaFactory[iRes].iQSEllipse0Size += 22;
      dipaFactory[iRes].iQSEllipse1Size += 22;
      dipaFactory[iRes].iQSEllipse2Size += 22;
    break;
    default:
    break;
   }

  return true;

}  /* factoryIniParameters */

bool CDRSParameters::factoryIniParameters ()
{
  for (int iRes = 0; iRes < IF_IMAGE_RESOLUTION_COUNT; iRes++)
   {
    if (!factoryIniParameters (iRes))
      return false;
   }

  return true;
}  /* factoryIniParameters */

CIniFile * CDRSParameters::openIni (int iRes, string &strParamSection)
{
  string strIniFileName = CPMPIniFileName ("DRS");

  switch (iRes)
   {
    case IF_IMAGE_RESOLUTION_STANDARD_DEF:
      strParamSection = "PreferencesSD";
    break;
    case IF_IMAGE_RESOLUTION_HIGH_DEF:
      strParamSection = "PreferencesHD";
    break;
    case IF_IMAGE_RESOLUTION_2K_DATA:
      strParamSection = "Preferences2K";
    break;
    case IF_IMAGE_RESOLUTION_4K_DATA:
      strParamSection = "Preferences4K";
    break;
    case IF_IMAGE_RESOLUTION_6K_DATA:
      strParamSection = "Preferences6K";
    break;
    case IF_IMAGE_RESOLUTION_8K_DATA:
      strParamSection = "Preferences8K";
    break;
    default:
      strParamSection = "";
   }

  if (strParamSection == "")
    return 0;

  CIniFile *ini;
  ini = CreateIniFile (strIniFileName);
  if (ini == 0)
   {
    TRACE_0 (errout << "Could not load values from " << strIniFileName);
   }

  return ini;
}  /* openIni */

CIniFile * CDRSParameters::openIni (string &strControlSection)
{
  string strIniFileName = CPMPIniFileName ("DRS");

  strControlSection = "ControlSettings";

  CIniFile *ini;
  ini = CreateIniFile (strIniFileName);
  if (ini == 0)
   {
    TRACE_0 (errout << "Could not load values from " << strIniFileName);
   }

  return ini;
}  /* openIni */
