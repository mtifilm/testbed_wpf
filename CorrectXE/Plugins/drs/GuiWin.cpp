/*
   File Name:  GuiWin.cpp
   Created:  June 25, 2001 by John Mertus

   This contains all the toolbar components for the DRS

*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GuiWin.h"
#include "ClipAPI.h"
#include "DRSTool.h"
#include "ExecStatusBarUnit.h"
#include "ToolCommand.h"
#include "ToolSystemInterface.h"
#include "UnitPreferences.h"
#include "ShowModalDialog.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "ExecStatusBarUnit"
//#pragma link "cspin"
#pragma link "ColorPanel"
#pragma link "CompactTrackEditFrameUnit"
#pragma resource "*.dfm"

#define BUTTON_WIDTH 20
TDRSForm *DRSForm;

#define PLUGIN_TAB_INDEX 0
#define DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE 50


//------------------CreateDRSGui------------------John Mertus----Aug 2001----

  void CreateDRSGui(void)

//  This creates the GUI if one does not exist.
// Note: only one can be created.
//
//****************************************************************************
{
    if (DRSForm == NULL)
     {
      DRSForm = new TDRSForm(Application);   // Create it
      DRSForm->RestoreSettings();

      // Move all our controls to the unified tabbed tool window
      extern char PluginName[];
      GDRSTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
                       reinterpret_cast<int *>( DRSForm->DRSControlPanel ),
                       NULL);

		DRSForm->formCreate();
	  }

    if (FormDRSPreferences == NULL)
     {
      FormDRSPreferences = new TFormDRSPreferences(Application);
     }
}

//-----------------DestroyDRSGui------------------John Mertus----Aug 2001---

  void DestroyDRSGui(void)

//  This destroys then entire GUI interface
//
//***************************************************************************
{
   if ((GDRSTool != NULL)&&(DRSForm != NULL)) {

      DRSForm->formDestroy();

	  // Grab back the control panel so we can properly destroy it
      GDRSTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
      DRSForm->DRSControlPanel->Parent = DRSForm;

      DRSForm->Free();
      DRSForm = NULL;

      FormDRSPreferences->Free();
      FormDRSPreferences = NULL;
   }
}

//-------------------ShowDRSGui-------------------John Mertus----Aug 2001-----

    bool ShowDRSGui(void)

//  This creates the GUI if not already exists and then shows it
//
//****************************************************************************
{
   CreateDRSGui();            // Create the gui if necessary
   if (DRSForm == NULL || GDRSTool == NULL)
      return false;

	DRSForm->DRSControlPanel->Visible = true;

	DRSForm->formShow();

   return true; // controls ARE visible !!?!
}

//-------------------UpdateDRSGuiButtons----------John Mertus----Aug 2001-----

    void UpdateDRSGuiButtons(void)

//  This creates the GUI if not already exists and then shows it
//
//****************************************************************************
{
   if (DRSForm == NULL) return;

   DRSForm->UpdateVisualButtons();

   if (GDRSTool->reportColorbumpError)
   {
      GDRSTool->reportColorbumpError = false;
      MessageBox(DRSForm->Handle,  "Color Bump does not preserve history.\r\nConsider running it in a version clip instead!", "No History Warning", MB_ICONWARNING | MB_OK);
   }

       ///  MessageBox(nullptr,  "Color Bump does not preserve history.\r\nConsider running it in a version clip instead!", "No History Warning", MB_ICONWARNING | MB_OK);

}

//-------------------HideDRSGui-------------------John Mertus----Aug 2001-----

    bool  HideDRSGui(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
   if (DRSForm == NULL || GDRSTool == NULL)
      return false;

    //Hide the panel that contains all of our controls
    DRSForm->DRSControlPanel->Visible = false;
    // DRSForm->formHide();

    return false; // controls are NOT visible !!?!
}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
    if (DRSForm == NULL) return(false);         // Not created

    return DRSForm->DRSControlPanel->Visible;   // was: (DRSForm->Visible);
}

//---------------------------------------------------------------------------



__fastcall TDRSForm::TDRSForm(TComponent* Owner)
        : TMTIForm(Owner), MouseCapturedByOverrideButton(false)
{
   fSaveOnExit = true;

   LowMotionButton->Tag = DRS_CMD_LOW_MOTION_KEY;
   HighMotionButton->Tag = DRS_CMD_HIGH_MOTION_KEY;
   HardMotionButton->Tag = DRS_CMD_HARD_MOTION_KEY;
   ReplaceButton->Tag = DRS_CMD_REPLACE_KEY;
   ColorBumpButton->Tag = DRS_CMD_COLOR_BUMP_KEY;

   PastButton->Tag = DRS_CMD_PAST_ONLY_KEY;
   FutureButton->Tag = DRS_CMD_FUTURE_ONLY_KEY;
   BothButton->Tag = DRS_CMD_BOTH_PAST_AND_FUTURE_KEY;
   PastButton->Down = false;
   FutureButton->Down = false;
   BothButton->Down = true;

   BlurButton->Tag = DRS_CMD_HARD_BLUR_KEY;
   EdgePreserveButton->Tag = DRS_CMD_HARD_EDGE_PRESERVE_KEY;
   BlurButton->Down = true;
   EdgePreserveButton->Down = false;

   ReplaceNoneButton->Tag = DRS_CMD_REPLACE_NONE_KEY;
   ReplaceLowButton->Tag = DRS_CMD_REPLACE_LOW_KEY;
   ReplaceHighButton->Tag = DRS_CMD_REPLACE_HIGH_KEY;
   ReplaceNoneButton->Down = false;
   ReplaceLowButton->Down = false;
   ReplaceHighButton->Down = true;

   DrawRectangleButton->Tag = DRS_CMD_RECTANGLE_MODE;
   DrawLassoButton->Tag = DRS_CMD_LASSO_MODE;
   DrawBezierButton->Tag = DRS_CMD_BEZIER_MODE;
   DrawRectangleButton->Down = true;
   DrawLassoButton->Down = false;
   DrawBezierButton->Down = false;

   MoveOverButton->Tag = DRS_CMD_MOVE_OVER_KEY;
   MoveOverOverrideXPanel->Tag = DRS_CMD_MOVE_OVER_KEY;
   MoveOverXValueLabel->Tag = DRS_CMD_MOVE_OVER_KEY;
   MoveOverOverrideYPanel->Tag = DRS_CMD_MOVE_OVER_KEY;
   MoveOverYValueLabel->Tag = DRS_CMD_MOVE_OVER_KEY;

   MoveUnderButton->Tag = DRS_CMD_MOVE_UNDER_KEY;
   MoveUnderOverrideXPanel->Tag = DRS_CMD_MOVE_UNDER_KEY;
   MoveUnderXValueLabel->Tag = DRS_CMD_MOVE_UNDER_KEY;
   MoveUnderOverrideYPanel->Tag = DRS_CMD_MOVE_UNDER_KEY;
   MoveUnderYValueLabel->Tag = DRS_CMD_MOVE_UNDER_KEY;

   RotateOverButton->Tag = DRS_CMD_ROTATE_OVER_KEY;
   RotateOverOverridePanel->Tag = DRS_CMD_ROTATE_OVER_KEY;
   RotateOverValueLabel->Tag = DRS_CMD_ROTATE_OVER_KEY;

   RotateUnderButton->Tag = DRS_CMD_ROTATE_UNDER_KEY;
   RotateUnderOverridePanel->Tag = DRS_CMD_ROTATE_UNDER_KEY;
   RotateUnderValueLabel->Tag = DRS_CMD_ROTATE_UNDER_KEY;

   ColorButton->Tag = DRS_CMD_CHANGE_COLOR_KEY;
   ColorBrightnessButton->Tag = DRS_CMD_CHANGE_COLOR_BRIGHTNESS;
   ColorSaturationButton->Tag = DRS_CMD_CHANGE_COLOR_SATURATION;
   ColorRedButton->Tag = DRS_CMD_CHANGE_COLOR_RED;
   ColorGreenButton->Tag = DRS_CMD_CHANGE_COLOR_GREEN;
   ColorBlueButton->Tag = DRS_CMD_CHANGE_COLOR_BLUE;
   ColorBrightnessOverridePanel->Tag = DRS_CMD_CHANGE_COLOR_BRIGHTNESS;
   ColorBrightnessValueLabel->Tag = DRS_CMD_CHANGE_COLOR_BRIGHTNESS;
   ColorSaturationOverridePanel->Tag = DRS_CMD_CHANGE_COLOR_SATURATION;
   ColorSaturationValueLabel->Tag = DRS_CMD_CHANGE_COLOR_SATURATION;
   ColorRedOverridePanel->Tag = DRS_CMD_CHANGE_COLOR_RED;
   ColorRedValueLabel->Tag = DRS_CMD_CHANGE_COLOR_RED;
   ColorGreenOverridePanel->Tag = DRS_CMD_CHANGE_COLOR_GREEN;
   ColorGreenValueLabel->Tag = DRS_CMD_CHANGE_COLOR_GREEN;
   ColorBlueOverridePanel->Tag = DRS_CMD_CHANGE_COLOR_BLUE;
   ColorBlueValueLabel->Tag = DRS_CMD_CHANGE_COLOR_BLUE;

   GrainButton->Tag = DRS_CMD_CHANGE_GRAIN_KEY;
   GrainOverridePanel->Tag = DRS_CMD_CHANGE_GRAIN_KEY;
   GrainValueLabel->Tag = DRS_CMD_CHANGE_GRAIN_KEY;

   BorderButton->Tag = DRS_CMD_CHANGE_BORDER_KEY;
   BorderOverridePanel->Tag = DRS_CMD_CHANGE_BORDER_KEY;
   BorderValueLabel->Tag = DRS_CMD_CHANGE_BORDER_KEY;

   TransparencyButton->Tag = DRS_CMD_CHANGE_TRANSPARENCY_KEY;
   TransparencyOverridePanel->Tag = DRS_CMD_CHANGE_TRANSPARENCY_KEY;
   TransparencyValueLabel->Tag = DRS_CMD_CHANGE_TRANSPARENCY_KEY;

   MoveOverButton->Down = true;
   MoveUnderButton->Down = false;
   RotateOverButton->Down = false;
   RotateUnderButton->Down = false;
   ColorButton->Down = false;
   GrainButton->Down = false;
   BorderButton->Down = false;
   TransparencyButton->Down = false;

   MoveOverStickyCheckBox->Tag = OM_MOVE_OVER;
   MoveUnderStickyCheckBox->Tag = OM_MOVE_UNDER;
   ColorStickyCheckBox->Tag = OM_CHANGE_COLOR;
   GrainStickyCheckBox->Tag = OM_CHANGE_GRAIN;
   RotateOverStickyCheckBox->Tag = OM_ROTATE_OVER;
   RotateUnderStickyCheckBox->Tag = OM_ROTATE_UNDER;
   BorderStickyCheckBox->Tag = OM_CHANGE_BORDER;
   TransparencyStickyCheckBox->Tag = OM_CHANGE_TRANSPARENCY;

   DisabledPastFuturePanel->Visible = !(LowMotionButton->Down ||
                                        HighMotionButton->Down);
   DisabledBlurEdgePreservePanel->Visible = !HardMotionButton->Down;
   DisabledNoneLowHighPanel->Visible = !ReplaceButton->Down;

   DisabledCBPastAutoFuturePanel->Visible = !ColorBumpButton->Down;

   ReplaceSrcFramesComboBox->Items->Clear();
   ReplaceSrcFramesComboBox->Items->AddObject("Use marks",   (TObject*)DRS_REPLSRCFRM_USE_MARKS);
   ReplaceSrcFramesComboBox->Items->AddObject("Prev 1",      (TObject*)DRS_REPLSRCFRM_PREV_1);
   ReplaceSrcFramesComboBox->Items->AddObject("Prev 2",      (TObject*)DRS_REPLSRCFRM_PREV_2);
   ReplaceSrcFramesComboBox->Items->AddObject("Next 1",      (TObject*)DRS_REPLSRCFRM_NEXT_1);
	ReplaceSrcFramesComboBox->Items->AddObject("Next 2",      (TObject*)DRS_REPLSRCFRM_NEXT_2);
   ReplaceSrcFramesComboBox->Items->AddObject("Prev & next", (TObject*)DRS_REPLSRCFRM_PREV_AND_NEXT);
	ReplaceSrcFramesComboBox->Items->AddObject("Cur frame",   (TObject*)DRS_REPLSRCFRM_CURRENT_FRAME);
	ReplaceSrcFramesComboBox->Items->AddObject("2fr Auto",    (TObject*)DRS_REPLSRCFRM_2_FRM_AUTO);
   ReplaceSrcFramesComboBox->Items->AddObject("3fr Auto",    (TObject*)DRS_REPLSRCFRM_3_FRM_AUTO);

   int itemIndex = 0;
   CIniFile *ini = CreateIniFile (CPMPIniFileName("DRS"), true);
   if (ini != 0)
   {
      int mode = ini->ReadInteger("Preferences", "ReplaceSrcFramesMode", -1);
      if (itemIndex < 0 || itemIndex >= ReplaceSrcFramesComboBox->Items->Count)
      {
         itemIndex = 0;
      }
      else
      {
         for (int i = 0; i < ReplaceSrcFramesComboBox->Items->Count; ++i)
         {
            if (mode == (int) ReplaceSrcFramesComboBox->Items->Objects[i])
            {
               itemIndex = i;
               break;
            }
         }
      }

      DeleteIniFile(ini);
   }

   ReplaceSrcFramesComboBox->ItemIndex = itemIndex;

   _grainPresetOverrideCurrentValue = DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE;

   GDRSTool->getSystemAPI()->setMainWindowFocus();
   GUIInhibit = false;
}
//---------------------------------------------------------------------------

void TDRSForm::formCreate()
{
   SET_CBHOOK(GrainPresetsChanged, GDRSTool->GetGrainPresets().GrainPresetsStateChange);
   GDRSTool->SetToolProgressMonitor(ExecStatusBar);
}
//---------------------------------------------------------------------------

void TDRSForm::formShow()
{
	// Grain presets
	auto grainParametersBase = GDRSTool->GetGrainPresets().GetSelectedBase();
	if (grainParametersBase == GP_BASE_CLIP)
	{
		GrainPresetsClipRadioButton->Checked = true;
	}
	else
	{
		GrainPresetsProjectRadioButton->Checked = true;
	}

	// CAREFUL! ugly off-by one index.
	const int GRAIN_PRESET_2_INDEX = 1;
	auto grainParametersPresetIndex = GDRSTool->GetGrainPresets().GetSelectedIndex();
	if (grainParametersPresetIndex == GRAIN_PRESET_2_INDEX)
	{
		GrainPreset2Button->Down = true;
	}
	else
	{
		GrainPreset1Button->Down = true;
	}

   ConformAutoReplaceControls();
}
//---------------------------------------------------------------------------

void TDRSForm::formDestroy()
{
   REMOVE_CBHOOK(GrainPresetsChanged, GDRSTool->GetGrainPresets().GrainPresetsStateChange);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::GenericButtonClick(TObject *Sender)
{
	 TButton *button = (TButton *)Sender;
// TRACE_0(errout << button->Tag);    // xyzzy
    ExecuteToolCommand(button->Tag);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::LowHighMotionButtonClick(TObject *Sender)
{
    // CAREFUL! PAST, BOTH and FUTURE buttons need to come here too!!!
    if (GDRSTool == NULL) return;

    ConformAutoReplaceControls();

    int command(DRS_CMD_INVALID);

    if (LowMotionButton->Down)
    {
       if (PastButton->Down)
          command = DRS_CMD_LOW_MOTION_PAST;
       else if (BothButton->Down)
          command = DRS_CMD_LOW_MOTION;
       else if (FutureButton->Down)
          command = DRS_CMD_LOW_MOTION_FUTURE;
    }
    else if (HighMotionButton->Down)
    {
       if (PastButton->Down)
          command = DRS_CMD_HIGH_MOTION_PAST;
       else if (BothButton->Down)
          command = DRS_CMD_HIGH_MOTION;
       else if (FutureButton->Down)
          command = DRS_CMD_HIGH_MOTION_FUTURE;
    }

    ExecuteToolCommand(command);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::HardMotionButtonClick(TObject *Sender)
{
    // CAREFUL! BLUR and EDGE PRESERVE buttons need to come here too!!!
    if (GDRSTool == NULL) return;

    ConformAutoReplaceControls();

    int command(DRS_CMD_INVALID);

    if (HardMotionButton->Down)
    {
       if (BlurButton->Down)
          command = DRS_CMD_HARD_MOTION;
       else if (EdgePreserveButton->Down)
          command = DRS_CMD_HARD_MOTION_EDGE;
    }

	 ExecuteToolCommand(command);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ReplaceButtonClick(TObject *Sender)
{
	// CAREFUL! REPLACE-NONE, REPLACE-LOW and REPLACE-HIGH buttons
	// need to come here too!!!
	if (GDRSTool == NULL) return;

	ConformAutoReplaceControls();

	int command(DRS_CMD_INVALID);

	if (ReplaceButton->Down)
	{
	   if (ReplaceNoneButton->Down)
		  command = DRS_CMD_NO_MOTION_IMPORT;
	   else if (ReplaceLowButton->Down)
		  command = DRS_CMD_LOW_MOTION_IMPORT;
	   else if (ReplaceHighButton->Down)
		  command = DRS_CMD_HIGH_MOTION_IMPORT;
	}

	ExecuteToolCommand(command);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ColorBumpButtonClick(TObject *Sender)
{
	if (GDRSTool == NULL || !ColorBumpButton->Down)
	{
	   return;
	}

   ConformAutoReplaceControls();

   // Warn if trying to run Color Bump on master clip.
   if (GDRSTool->getSystemAPI()->isMasterClipLoaded())
   {
      MessageBox(NULL, "Color Bump does not preserve history.\r\nConsider running it in a version clip instead!", "No History Warning", MB_ICONWARNING | MB_OK);
   }

   DisabledCBPastAutoFuturePanel->Visible = false;

	int command(DRS_CMD_INVALID);

   if (CBPastOnlyButton->Down)
   {
     command = /* CBMonochromeCheckBox->Checked ? DRS_CMD_COLOR_BUMP_PAST_MONO : */ DRS_CMD_COLOR_BUMP_PAST;
   }
   else if (CBFutureOnlyButton->Down)
   {
     command = /* CBMonochromeCheckBox->Checked ? DRS_CMD_COLOR_BUMP_FUTURE_MONO : */ DRS_CMD_COLOR_BUMP_FUTURE;
   }
   else
   {
     command = /* CBMonochromeCheckBox->Checked ? DRS_CMD_COLOR_BUMP_MONO : */ DRS_CMD_COLOR_BUMP;
   }

	ExecuteToolCommand(command);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::CBAutoDirCheckBoxClick(TObject *Sender)
{
   ColorBumpButtonClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::CBPastOnlyButtonClick(TObject *Sender)
{
   ColorBumpButtonClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::CBAutoButtonClick(TObject *Sender)
{
   ColorBumpButtonClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::CBFutureOnlyButtonClick(TObject *Sender)
{
   ColorBumpButtonClick(Sender);
}
//---------------------------------------------------------------------------

void TDRSForm::ExecuteToolCommand(int command)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   CToolCommand toolCommand(GDRSTool, command);
   toolCommand.execute();
}
//---------------------------------------------------------------------------

void TDRSForm::UpdateVisualButtons()
{
   if (GDRSTool == NULL) return;

   // Prevent us from calling back into the tool
   GUIInhibit = true;

   LowMotionButton->Down = false;
   HighMotionButton->Down = false;
   HardMotionButton->Down = false;
   ReplaceButton->Down = false;
   ColorBumpButton->Down = false;

   switch (GDRSTool->GetMainProcessType())
   {
      case DRS_PROCESS_NO_MOTION:
         switch (GDRSTool->GetSubProcessType())
         {
            case DRS_SUBPROCESS_NONE:
               // Not supported
               break;

            case DRS_SUBPROCESS_A:
               // Not supported
               break;

            case DRS_SUBPROCESS_B:
               // Not supported
               break;

			   case DRS_SUBPROCESS_C:
               ReplaceButton->Down = true;
               ReplaceNoneButton->Down = true;
               ReplaceLowButton->Down = false;
               ReplaceHighButton->Down = false;
			      break;

			   case DRS_SUBPROCESS_D:
               // Not supported
               break;
		   }
         break;

      case DRS_PROCESS_LOW_MOTION:
         switch (GDRSTool->GetSubProcessType())
         {
            case DRS_SUBPROCESS_NONE:
               LowMotionButton->Down = true;
               PastButton->Down = false;
               FutureButton->Down = false;
               BothButton->Down = true;
               break;

            case DRS_SUBPROCESS_A:
               LowMotionButton->Down = true;
               PastButton->Down = true;
               FutureButton->Down = false;
               BothButton->Down = false;
               break;

            case DRS_SUBPROCESS_B:
               LowMotionButton->Down = true;
               PastButton->Down = false;
               FutureButton->Down = true;
               BothButton->Down = false;
               break;

			case DRS_SUBPROCESS_C:
               ReplaceButton->Down = true;
               ReplaceNoneButton->Down = false;
               ReplaceLowButton->Down = true;
               ReplaceHighButton->Down = false;
               break;
		   }
         break;

      case DRS_PROCESS_HIGH_MOTION:
         switch (GDRSTool->GetSubProcessType())
         {
            case DRS_SUBPROCESS_NONE:
               HighMotionButton->Down = true;
               PastButton->Down = false;
               FutureButton->Down = false;
               BothButton->Down = true;
               break;

            case DRS_SUBPROCESS_A:
               HighMotionButton->Down = true;
               PastButton->Down = true;
               FutureButton->Down = false;
               BothButton->Down = false;
               break;

            case DRS_SUBPROCESS_B:
               HighMotionButton->Down = true;
               PastButton->Down = false;
               FutureButton->Down = true;
               BothButton->Down = false;
               break;

			case DRS_SUBPROCESS_C:
               ReplaceButton->Down = true;
               ReplaceNoneButton->Down = false;
               ReplaceLowButton->Down = false;
               ReplaceHighButton->Down = true;
               break;
		   }
         break;

      case DRS_PROCESS_HARD_MOTION:
         switch (GDRSTool->GetSubProcessType())
         {
            case DRS_SUBPROCESS_NONE:
               HardMotionButton->Down = true;
               BlurButton->Down = false;
               EdgePreserveButton->Down = true;
               break;

            case DRS_SUBPROCESS_A:
               HardMotionButton->Down = true;
               BlurButton->Down = true;
               EdgePreserveButton->Down = false;
               break;

            case DRS_SUBPROCESS_B:
               // Not supported
               break;

			   case DRS_SUBPROCESS_C:
              // Not supported
			      break;
         }
         break;

      case DRS_PROCESS_COLOR_BUMP:
         ColorBumpButton->Down = true;
         switch (GDRSTool->GetSubProcessType())
         {
            case DRS_SUBPROCESS_NONE:
//               CBMonochromeCheckBox->Checked = false;
               CBAutoButton->Down = true;
               break;

            // past
            case DRS_SUBPROCESS_A:
//               CBMonochromeCheckBox->Checked = false;
               CBPastOnlyButton->Down = true;
               break;

            // future
            case DRS_SUBPROCESS_B:
//               CBMonochromeCheckBox->Checked = false;
               CBFutureOnlyButton->Down = true;
               break;

            // mono
            case DRS_SUBPROCESS_C:
//               CBMonochromeCheckBox->Checked = true;
               CBAutoButton->Down = true;
               break;

            // mono + past
            case DRS_SUBPROCESS_D:
//               CBMonochromeCheckBox->Checked = true;
               CBPastOnlyButton->Down = true;
               break;

            // mono + future
            case DRS_SUBPROCESS_E:
//               CBMonochromeCheckBox->Checked = true;
               CBFutureOnlyButton->Down = true;
               break;

			   default:
              // Not supported
			      break;
		   }
         break;
   }

   DisabledPastFuturePanel->Visible = !(LowMotionButton->Down ||
                                        HighMotionButton->Down);
   DisabledBlurEdgePreservePanel->Visible = !HardMotionButton->Down;
   DisabledNoneLowHighPanel->Visible = !ReplaceButton->Down;
   DisabledCBPastAutoFuturePanel->Visible = !ColorBumpButton->Down;

   // "Press" the button corresponding to the current select region
   // shape
   switch(GDRSTool->GetSelectShape())
   {
      case SELECT_REGION_RECT :
         DrawRectangleButton->Down = true;
         DrawLassoButton->Down = false;
         DrawBezierButton->Down = false;
         break;
      case SELECT_REGION_LASSO :
         DrawRectangleButton->Down = false;
         DrawLassoButton->Down = true;
         DrawBezierButton->Down = false;
         break;
      case SELECT_REGION_BEZIER :
         DrawRectangleButton->Down = false;
         DrawLassoButton->Down = false;
         DrawBezierButton->Down = true;
         break;
   }

   MoveOverButton->Down = (GDRSTool->GetOverrideMode() == OM_MOVE_OVER);
   MoveUnderButton->Down = (GDRSTool->GetOverrideMode() == OM_MOVE_UNDER);
	GrainButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_GRAIN);
   BorderButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_BORDER);
   RotateOverButton->Down = (GDRSTool->GetOverrideMode() == OM_ROTATE_OVER);
   RotateUnderButton->Down = (GDRSTool->GetOverrideMode() == OM_ROTATE_UNDER);
   TransparencyButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_TRANSPARENCY);
////TRACE_0(errout << "overridesmode=" << GDRSTool->GetOverrideMode()); // xyzzy
   ColorBrightnessButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_COLOR_BRIGHTNESS);
////TRACE_0(errout << "ColorBrightnessButton->Down = " << (GDRSTool->GetOverrideMode() == 9) << " ? " << ColorBrightnessButton->Down); // xyzzy
   ColorSaturationButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_COLOR_SATURATION);
////TRACE_0(errout << "ColorSaturationButton->Down = " << (GDRSTool->GetOverrideMode() == 10) << " ? " << ColorSaturationButton->Down); // xyzzy
   ColorRedButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_COLOR_RED);
////TRACE_0(errout << "ColorRedButton->Down = " << (GDRSTool->GetOverrideMode() == 11) << " ? " << ColorRedButton->Down); // xyzzy
   ColorGreenButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_COLOR_GREEN);
////TRACE_0(errout << "ColorGreenButton->Down = " << (GDRSTool->GetOverrideMode() == 12) << " ? " << ColorGreenButton->Down); // xyzzy
   ColorBlueButton->Down = (GDRSTool->GetOverrideMode() == OM_CHANGE_COLOR_BLUE);
////TRACE_0(errout << "ColorBlueButton->Down = " << (GDRSTool->GetOverrideMode() == 13) << " ? " << (bool)ColorBlueButton->Down); // xyzzy
   ColorButton->Down = ColorBrightnessButton->Down
                           || ColorSaturationButton->Down
                           || ColorRedButton->Down
                           || ColorGreenButton->Down
                           || ColorBlueButton->Down;
////TRACE_0(errout << "ColorButton->Down = " << ColorButton->Down); // xyzzy

   BorderOnCheckbox->Checked = GDRSTool->GetBorderEnabled();


   char caMessage[64];

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideBorderRadius());
   BorderValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideColorBrightness());
   ColorBrightnessValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideColorSaturation());
   ColorSaturationValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideColorRed());
   ColorRedValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideColorGreen());
   ColorGreenValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideColorBlue());
   ColorBlueValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideGrainStrength());
   GrainValueLabel->Caption = caMessage;

   sprintf (caMessage, "% 5d", GDRSTool->GetOverrideTransparency());
   TransparencyValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideMoveOverX());
   MoveOverXValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideMoveOverY());
   MoveOverYValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideMoveUnderX());
   MoveUnderXValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideMoveUnderY());
   MoveUnderYValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideRotateOver());
   RotateOverValueLabel->Caption = caMessage;

   sprintf (caMessage, "%5.2f", GDRSTool->GetOverrideRotateUnder());
	RotateUnderValueLabel->Caption = caMessage;

	UpdateClearButtonAppearances();

	bool updatingGrainOverrideUsingPresets  = GrainOverrideUsePresetsCheckBox->Checked;
	GrainPresetsPanel->Enabled              = updatingGrainOverrideUsingPresets;
	GrainPresetsProjectRadioButton->Enabled = updatingGrainOverrideUsingPresets;
	GrainPresetsClipRadioButton->Enabled    = updatingGrainOverrideUsingPresets;
	GrainPreset1Button->Enabled             = updatingGrainOverrideUsingPresets;
	GrainPreset2Button->Enabled             = updatingGrainOverrideUsingPresets;

   if (GrainOverrideUsePresetsCheckBox->Checked)
   {
      _grainPresetOverrideCurrentValue = GDRSTool->GetOverrideGrainStrength();
   }

   StickyOverridesCheckBox->Checked = GDRSTool->GetOverrideStickiness();
   MoveUnderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   MoveOverStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   ColorStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   GrainStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   RotateOverStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   RotateUnderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   BorderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   TransparencyStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;

   MoveOverStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_MOVE_OVER);
   MoveUnderStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_MOVE_UNDER);
   GrainStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_CHANGE_GRAIN);
   RotateOverStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_ROTATE_OVER);
   RotateUnderStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_ROTATE_UNDER);
   BorderStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_CHANGE_BORDER);
   TransparencyStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_CHANGE_TRANSPARENCY);
   ColorStickyCheckBox->Checked = GDRSTool->GetOverrideStickiness(OM_CHANGE_COLOR);

   RunMacroButton->Enabled = GDRSTool->IsOkToStartMacro();
   RunMacroButton->Down = false;
   StopMacroButton->Enabled = GDRSTool->IsOkToStopMacro();
   StopMacroButton->Down = false;

   GUIInhibit = false;

}
//---------------------------------------------------------------------------

static const TColor HiliteColor = TColor(0x0000F0FF);// slightly orangey-yellow
static const TColor LoliteColor = TColor(0x006A6A6A);// standard theme BG

void TDRSForm::UpdateOneClearButtonAppearance(TPanel *backPanel,
                                              TSpeedButton *clearButton,
											  bool enableClear)
{
   if (clearButton == NULL)
   {
	  backPanel->Color = (enableClear? HiliteColor : LoliteColor);
	  backPanel->BevelInner = (enableClear? bvRaised : bvNone);
	  backPanel->BevelOuter = (enableClear? bvLowered : bvNone);
	  backPanel->BorderWidth = 0;
	  backPanel->BevelWidth = 1;
	  return;
   }

   if (clearButton->Enabled != enableClear)
   {
	  clearButton->Enabled = enableClear;
	  backPanel->Color = (enableClear? HiliteColor : LoliteColor);
	  backPanel->BevelInner = (enableClear? bvRaised : bvNone);
	  backPanel->BevelOuter = (enableClear? bvLowered : bvNone);
	  backPanel->BorderWidth = 0;
	  backPanel->BevelWidth = 1;
   }
}

void TDRSForm::UpdateClearButtonAppearances()
{
   UpdateOneClearButtonAppearance(BorderOverridePanel,       NULL, (GDRSTool->GetOverrideBorderRadius() != 0));
   UpdateOneClearButtonAppearance(GrainOverridePanel,        NULL, (GDRSTool->GetOverrideGrainStrength() != 0));
   UpdateOneClearButtonAppearance(TransparencyOverridePanel, NULL, (GDRSTool->GetOverrideTransparency() != 0));
   UpdateOneClearButtonAppearance(MoveOverOverrideXPanel,    NULL, (GDRSTool->GetOverrideMoveOverX() != 0.F));
   UpdateOneClearButtonAppearance(MoveOverOverrideYPanel,    NULL, (GDRSTool->GetOverrideMoveOverY() != 0.F));
   UpdateOneClearButtonAppearance(MoveUnderOverrideXPanel,   NULL, (GDRSTool->GetOverrideMoveUnderX() != 0.F));
   UpdateOneClearButtonAppearance(MoveUnderOverrideYPanel,   NULL, (GDRSTool->GetOverrideMoveUnderY() != 0.F));
   UpdateOneClearButtonAppearance(RotateOverOverridePanel,   NULL, (GDRSTool->GetOverrideRotateOver() != 0.F));
   UpdateOneClearButtonAppearance(RotateUnderOverridePanel,  NULL, (GDRSTool->GetOverrideRotateUnder() != 0.F));
   UpdateOneClearButtonAppearance(ColorBrightnessOverridePanel, NULL, (GDRSTool->GetOverrideColorBrightness() != 0));
   UpdateOneClearButtonAppearance(ColorSaturationOverridePanel, NULL, (GDRSTool->GetOverrideColorSaturation() != 0));
   UpdateOneClearButtonAppearance(ColorRedOverridePanel,     NULL, (GDRSTool->GetOverrideColorRed() != 0));
   UpdateOneClearButtonAppearance(ColorGreenOverridePanel,   NULL, (GDRSTool->GetOverrideColorGreen() != 0));
   UpdateOneClearButtonAppearance(ColorBlueOverridePanel,    NULL, (GDRSTool->GetOverrideColorBlue() != 0));
}
//---------------------------------------------------------------------------




void __fastcall TDRSForm::ClearMoveOverOverrideXButtonClick(
      TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   MoveOverXValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideMoveOverX(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearMoveOverOverrideYButtonClick(
	  TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   MoveOverYValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideMoveOverY(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearMoveUnderOverrideXButtonClick(
	  TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   MoveUnderXValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideMoveUnderX(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearMoveUnderOverrideYButtonClick(
	  TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   MoveUnderYValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideMoveUnderY(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearRotateOverOverrideButtonClick(
	  TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   RotateOverValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideRotateOver(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearRotateUnderOverrideButtonClick(
	  TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   RotateUnderValueLabel->Caption = "0.00";
   GDRSTool->SetOverrideRotateUnder(0.F);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearColorBrightnessOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   ColorBrightnessValueLabel->Caption = "0";
   GDRSTool->SetOverrideColorBrightness(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------
void __fastcall TDRSForm::ClearColorSaturationOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   ColorSaturationValueLabel->Caption = "0";
   GDRSTool->SetOverrideColorSaturation(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearColorRedOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   ColorRedValueLabel->Caption = "0";
   GDRSTool->SetOverrideColorRed(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearColorGreenOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   ColorGreenValueLabel->Caption = "0";
   GDRSTool->SetOverrideColorGreen(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearColorBlueOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   ColorBlueValueLabel->Caption = "0";
   GDRSTool->SetOverrideColorBlue(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearBorderWidthButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   BorderValueLabel->Caption = "0";
   GDRSTool->SetOverrideBorderRadius(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearGrainOverrideButtonClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   GrainValueLabel->Caption = "0";
   GDRSTool->SetOverrideGrainStrength(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ClearTransparencyOverrideButtonClick(
      TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   TransparencyValueLabel->Caption = "0";
   GDRSTool->SetOverrideTransparency(0);
   UpdateClearButtonAppearances();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::BorderOnCheckboxClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();

   BorderValueLabel->Enabled = BorderOnCheckbox->Checked;
   GDRSTool->SetBorderEnabled(BorderOnCheckbox->Checked);
}
//---------------------------------------------------------------------------

void MarksChanged(void)
{
   if (DRSForm == NULL) return;

   DRSForm->MarksChanged();
}

void TDRSForm::MarksChanged(void)
{

}
//---------------------------------------------------------------------------

void ShowPreferencesWindow(void)
{
   if (DRSForm == NULL) return;

   DRSForm->EditPreferencesClick(NULL);
}
//---------------------------------------------------------------------------

int  GetReplaceModeSourceFrameOption()
{
	int itemIndex = DRSForm->ReplaceSrcFramesComboBox->ItemIndex;
   if (itemIndex < 0)
   {
      return 0;
   }

   return (int)(DRSForm->ReplaceSrcFramesComboBox->Items->Objects[itemIndex]);
}
//---------------------------------------------------------------------------

void SetStatusIdle()
{
   if (DRSForm == NULL) return;

   DRSForm->ExecStatusBar->SetIdle(true);
}
//---------------------------------------------------------------------------

bool IsColorBumpSelected()
{
   if (DRSForm == NULL) return false;

   return DRSForm->ColorBumpButton->Down;
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::StickyOverridesCheckBoxClick(TObject *Sender)
{
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();
   GDRSTool->SetOverrideStickiness(StickyOverridesCheckBox->Checked);

   MoveUnderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   MoveOverStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   ColorStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   GrainStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   RotateOverStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   RotateUnderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   BorderStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
   TransparencyStickyDisabledPanel->Visible = !StickyOverridesCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::SingleStickyCheckBoxClick(TObject *Sender)
{
	if (GDRSTool == NULL) return;

   TCheckBox *checkBox = dynamic_cast<TCheckBox *>( Sender );
   if (checkBox != NULL)
   {
      GDRSTool->getSystemAPI()->setMainWindowFocus();
		GDRSTool->SetOverrideStickiness(checkBox->Tag, checkBox->Checked);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::DRSControlPanelEnter(TObject *Sender)
{
   // Always try to send focus back to the main window so frickin' arrow
   // keys work!!!
   if (GDRSTool == NULL) return;
   GDRSTool->getSystemAPI()->setMainWindowFocus();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::EditPreferencesLabelMouseEnter(TObject *Sender)
{
   EditPreferencesLabel->Font->Color = clBlue;
   EditPreferencesLabel->Font->Style = TFontStyles()/* << fsBold */ << fsUnderline;
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::EditPreferencesLabelMouseLeave(TObject *Sender)
{
   EditPreferencesLabel->Font->Color = clBlack;
   EditPreferencesLabel->Font->Style = TFontStyles()/* << fsBold */;
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::EditPreferencesClick(TObject *Sender)
{
   if (GDRSTool == NULL)
      return;

   if (GDRSTool->getSystemAPI()->CheckProvisionalUnresolvedAndToolProcessing())
      return;

   ShowModalDialog(FormDRSPreferences);
   GDRSTool->LoadDRSStatePreferences();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::DRSControlPanelMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   if (GDRSTool == NULL)
      return;

   if (Shift.Contains(ssMiddle))
   {
      MTIMouseMoveEventHandler mainMouseMoveHandler =
           GDRSTool->getSystemAPI()->getMouseMoveEventHandler();

      (mainMouseMoveHandler)(Sender, Shift, X, Y);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::OverrideButtonMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   if (Shift.Contains(ssMiddle))
   {
      // Tag property lives at TComponent level
      TControl *control = dynamic_cast<TControl *>(Sender);
      if (control != NULL)
      {
         DRSControlPanelMouseMove(Sender, Shift, X, Y);
		 Mouse->Capture = (HWND)control;
      }
   }

}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::OverrideButtonMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   if (Button == mbMiddle)
   {
      // Tag property lives at TComponent level, from which TControl is derived
      TControl *control = dynamic_cast<TControl *>(Sender);
      if (control != NULL)
      {
			ExecuteToolCommand(control->Tag);
		 Mouse->Capture = (HWND)control;
      }
      MTIMouseButtonEventHandler mainMouseButtonDownHandler =
           GDRSTool->getSystemAPI()->getMouseButtonDownEventHandler();

      (mainMouseButtonDownHandler)(Sender, Button, Shift, X, Y);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::OverrideButtonMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   if (Button == mbMiddle)
   {
      TControl *control = dynamic_cast<TControl *>(Sender);
      if (control != NULL && Mouse->Capture == (HWND)control)
      {
         Mouse->Capture = NULL;
      }
      MTIMouseButtonEventHandler mainMouseButtonUpHandler =
           GDRSTool->getSystemAPI()->getMouseButtonUpEventHandler();

      (mainMouseButtonUpHandler)(Sender, Button, Shift, X, Y);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::DRSControlPanelMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   if (Button == mbMiddle && Mouse->Capture == NULL)
   {
      if (MoveOverButton->Down)
      {
         Mouse->Capture = (HWND)MoveOverButton;
      }
      else if (MoveUnderButton->Down)
      {
         Mouse->Capture = (HWND)MoveUnderButton;
      }
      else if (RotateOverButton->Down)
      {
         Mouse->Capture = (HWND)RotateOverButton;
      }
      else if (RotateUnderButton->Down)
      {
         Mouse->Capture = (HWND)RotateUnderButton;
      }
      else if (ColorButton->Down)
      {
         Mouse->Capture = (HWND)ColorButton;
      }
      else if (BorderButton->Down)
      {
         Mouse->Capture = (HWND)BorderButton;
      }
      else if (GrainButton->Down)
      {
         Mouse->Capture = (HWND)GrainButton;
      }
      else if (TransparencyButton->Down)
      {
         Mouse->Capture = (HWND)TransparencyButton;
      }

      MouseCapturedByOverrideButton = true;

      MTIMouseButtonEventHandler mainMouseButtonDownHandler = GDRSTool->getSystemAPI()->getMouseButtonDownEventHandler();

      (mainMouseButtonDownHandler)(Sender, Button, Shift, X, Y);
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::DRSControlPanelMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   // Actually.. SHOULD NEVER GET HERE!
   if (MouseCapturedByOverrideButton)
   {
      Mouse->Capture = NULL;
      MouseCapturedByOverrideButton = false;
   }

   MTIMouseButtonEventHandler mainMouseButtonUpHandler =
        GDRSTool->getSystemAPI()->getMouseButtonUpEventHandler();

   (mainMouseButtonUpHandler)(Sender, Button, Shift, X, Y);
}
//---------------------------------------------------------------------------


void __fastcall TDRSForm::RunMacroButtonClick(TObject *Sender)
{
   if (GDRSTool != NULL)
   {
		GDRSTool->StartMacro();
   }
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::StopMacroButtonClick(TObject *Sender)
{
	if (GDRSTool != NULL)
	{
		GDRSTool->StopMacro();
	}
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::ReplaceSrcFramesComboBoxChange(TObject *Sender)
{
   int mode = (int) ReplaceSrcFramesComboBox->Items->Objects[ReplaceSrcFramesComboBox->ItemIndex];
   if (mode != ReplaceSrcFrameOldItemMode)
   {
      ReplaceSrcFrameOldItemMode = mode;

      CIniFile *ini = CreateIniFile (CPMPIniFileName ("DRS"));
      if (ini != 0)
      {
         ini->WriteInteger("Preferences", "ReplaceSrcFramesMode", mode);
         DeleteIniFile(ini);
      }

		ReplaceButtonClick(NULL);
   }
}
//---------------------------------------------------------------------------

void TDRSForm::ConformAutoReplaceControls()
{
   int mode = (int) ReplaceSrcFramesComboBox->Items->Objects[ReplaceSrcFramesComboBox->ItemIndex];
   bool weAreInAutoMode = ReplaceButton->Down && (mode == DRS_REPLSRCFRM_2_FRM_AUTO || mode == DRS_REPLSRCFRM_3_FRM_AUTO);
   RunAutoReplaceSpeedButton->Enabled = weAreInAutoMode;
   AutoReplaceGroupBox->Visible = weAreInAutoMode;
   MacroGroupBox->Visible = !weAreInAutoMode;
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::GrainPresetsButtonClick(TObject *Sender)
{
	if (GDRSTool == NULL)
	{
		return;
	}

	// CAREFUL! ugly off-by one index.
	const int GRAIN_PRESET_1_INDEX = 0;
	const int GRAIN_PRESET_2_INDEX = 1;
	auto grainParametersBase = GrainPresetsProjectRadioButton->Checked ? GP_BASE_PROJECT : GP_BASE_CLIP;
	auto grainParametersPresetIndex = GrainPreset1Button->Down ? GRAIN_PRESET_1_INDEX : GRAIN_PRESET_2_INDEX;

   GDRSTool->SelectGrainPreset(grainParametersBase, grainParametersPresetIndex);

   GDRSTool->getSystemAPI()->setMainWindowFocus();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::GrainOverrideUsePresetsCheckBoxClick(TObject *Sender)
{
	if (GDRSTool == NULL)
	{
		return;
	}

   GDRSTool->SetUseGrainPresets(GrainOverrideUsePresetsCheckBox->Checked);

   // Per Larry - always start preset mode at strength 50.
   // We'll set it back to zero when turning off "use presets" mode.
   // We'll also remember the last setting and restore that instead of blindly setting to 50.
   // But if the last setting was zero, use 50 instead.
   GDRSTool->SetOverrideGrainStrength(
         GrainOverrideUsePresetsCheckBox->Checked
         ? ((_grainPresetOverrideCurrentValue == 0)
             ? DEFAULT_GRAIN_PRESET_OVERRIDE_VALUE
             : _grainPresetOverrideCurrentValue)
         : 0);

	// Let's auto-select Grain Override, so mouse wheel works
   GrainButton->Down = true;
	Mouse->Capture = (HWND)GrainButton;

   UpdateVisualButtons();

	GrainPresetsButtonClick(nullptr);
}
//----------------------------------------------------------------------------

void TDRSForm::GrainPresetsChanged(void *Sender)
{
   UpdateVisualButtons();
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::StopAutoReplaceSpeedButtonClick(TObject *Sender)
{
//placeholder
}
//---------------------------------------------------------------------------

void __fastcall TDRSForm::RunAutoReplaceSpeedButtonClick(TObject *Sender)
{
   GDRSTool->RunAutoReplace();
}
//---------------------------------------------------------------------------
