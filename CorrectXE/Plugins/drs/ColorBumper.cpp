﻿// CImageFormat mImageFormat;
// MTI_UINT16 *mInputData[];
// MTI_UINT16 *mOutputData[];
// RECT mRoi;

#include "ColorBumper.h"
#include "ColorBumpUtil.h"
#include "HRTimer.h"
#include "mthread.h"
#include "IpaStripeStream.h"

#define WORKING_WIDTH 512
////#define WORKING_WIDTH 640
////#define WORKING_WIDTH 128
#define NUM_COMPONENTS 3

struct ResizeSourceParams
{
	Ipp32fArray src[5];
	Ipp32fArray dst[5];
	IppiSize dstSize;
};

int ColorBumper::Bump(MTI_UINT16 **inputData, int pitch, int inputDataSize, MTI_UINT16 maxValue, bool isMonochrome,
	 ColorBumpDirection direction, const RECT &roiRect, MTI_UINT16 *outputData)
{
	TRACE_3(errout << "CBCBCBCBCBCBCB ENTER Bump (0 msec) CBCBCBCBCBCBCB");
	CHRTimer stopwatch;
	CHRTimer stopwatch2;

	_inputData = inputData;
	_outputData = outputData;

	// For IPP, make sure left and width are even (align to 4 bytes).
	_roiOffset.x = roiRect.left - (roiRect.left & 1);
	_roiSize.width = (roiRect.right - roiRect.left) + (roiRect.right & 1);
	_roiOffset.y = roiRect.top;
	_roiSize.height = roiRect.bottom - roiRect.top;
	_imagePitchInPixels = pitch;
	_maxValue = maxValue;

#if 1
	auto SrcP2 = ExtractRoiAsFloat(_inputData[0]);
	auto SrcP1 = ExtractRoiAsFloat(_inputData[1]);
	auto SrcC0 = ExtractRoiAsFloat(_inputData[2]);
	auto SrcF1 = ExtractRoiAsFloat(_inputData[3]);
	auto SrcF2 = ExtractRoiAsFloat(_inputData[4]);

	TRACE_3(errout << "CBCBCBCBCBCBCB After ROI extraction (" << int(stopwatch.Read(true)) << " msec) CBCBCBCBCBCBCB");

	ResizeSourceParams resizeSourceParams;
	resizeSourceParams.src[0] = SrcP2;
	resizeSourceParams.src[1] = SrcP1;
	resizeSourceParams.src[2] = SrcC0;
	resizeSourceParams.src[3] = SrcF1;
	resizeSourceParams.src[4] = SrcF2;
	int workingHeight = int((WORKING_WIDTH / Ipp32f(_roiSize.width)) * _roiSize.height);
	resizeSourceParams.dstSize =
	{WORKING_WIDTH, workingHeight};

	ResizeSourceFrames(&resizeSourceParams);

	auto srcP2 = resizeSourceParams.dst[0];
	auto srcP1 = resizeSourceParams.dst[1];
	auto srcC0 = resizeSourceParams.dst[2];
	auto srcF1 = resizeSourceParams.dst[3];
	auto srcF2 = resizeSourceParams.dst[4];

	TRACE_3(errout << "CBCBCBCBCBCBCB After shrinking (" << int(stopwatch.Read(true)) << " msec) CBCBCBCBCBCBCB");

	BMStruct *BMF = nullptr;
	BMStruct *BMP = nullptr;

	{
		auto yC0 = ColorBumpUtil::ExtractYSignal(srcC0, isMonochrome);

		if (direction == AUTO || direction == FUTURE_ONLY)
		{
			auto yF1 = ColorBumpUtil::ExtractYSignal(srcF1, isMonochrome);
			auto yF2 = ColorBumpUtil::ExtractYSignal(srcF2, isMonochrome);
			TRACE_3(errout << "CBCBCBCBCBCBCB After ExtractYSignal(F) (" << int(stopwatch.Read(true))
				 << " msec) CBCBCBCBCBCBCB");

			// % perform block matching from C0 to A1 to A2
			// BMF = BlockMatch (srcC0, srcF1, srcF2);
			BMF = ColorBumpUtil::BlockMatch(yC0, yF1, yF2);

			TRACE_3(errout << "CBCBCBCBCBCBCB After BlockMatch(F) (" << int(stopwatch.Read(true))
				 << " msec) CBCBCBCBCBCBCB");
		}

		if (direction == AUTO || direction == PAST_ONLY)
		{
			auto yP2 = ColorBumpUtil::ExtractYSignal(srcP2, isMonochrome);
			auto yP1 = ColorBumpUtil::ExtractYSignal(srcP1, isMonochrome);
			TRACE_3(errout << "CBCBCBCBCBCBCB After ExtractYSignal(P) (" << int(stopwatch.Read(true))
				 << " msec) CBCBCBCBCBCBCB");

			// %  perform block matching from C0 to B1 to B2
			// BMP = BlockMatch (srcC0, srcP1, srcP2);
			BMP = ColorBumpUtil::BlockMatch(yC0, yP1, yP2);

			TRACE_3(errout << "CBCBCBCBCBCBCB After BlockMatch(P) (" << int(stopwatch.Read(true))
				 << " msec) CBCBCBCBCBCBCB");
		}
	}

	if (direction == AUTO)
	{
		// %  choose the best block matching direction.  Is it past-only
		// %  for future-only?
		// Direction = ChooseMotion (BMP, BMF);
		switch (ColorBumpUtil::ChooseMotion(*BMP, *BMF))
		{
		case -1:
			direction = PAST_ONLY;
			break;

		case 1:
			direction = FUTURE_ONLY;
			break;

		default:
			TRACE_2(errout << "COLOR BUMP FAIL: Can't determine motion!");
			return -1;
		}
	}

	// if Direction == -1,
	// % past only motion
	// BM = BMP;
	// src1 = srcP1;
	// elseif Direction == 1,
	// % future only motion
	// BM = BMF;
	// src1 = srcF1;
	// else
	// %error
	// return;
	// end
	BMStruct* BM = nullptr;
	Ipp32fArray src1;
	switch (direction)
	{
	case PAST_ONLY:
		BM = BMP;
		src1 = srcP1;
		break;

	default:
	case FUTURE_ONLY:
		BM = BMF;
		src1 = srcF1;
		break;
	}

	vector<Ipp32fArray>surfaces(3);

	auto srcC0Planes = srcC0.copyToPlanar();
	auto src1Planes = src1.copyToPlanar();

	const int redChan = 0;
	const int greenChan = 1;
	auto firstChan = isMonochrome ? greenChan : redChan;
	auto numChans = isMonochrome ? 1 : 3;

	for (auto chan = firstChan; chan < (firstChan + numChans); ++chan)
	{
		// % find the ratio between the current frame and its nearest
		// % temporal neighbor.  The nearest temporal neighbor is either
		// % one frame before or one frame after current frame
		// RAT = FindRatio (srcC0, src1, BM);
		auto RAT = ColorBumpUtil::FindRatio(srcC0Planes[chan], src1Planes[chan], *BM);

		// Surface = FindSurface (RAT);
		double mysteriousScaleFactor = WORKING_WIDTH;
		surfaces[chan] = ColorBumpUtil::FindSurfaceTPS(*RAT, mysteriousScaleFactor);
		delete RAT;
	}

	Ipp32fArray Surface({surfaces[firstChan].getSize(), 3});
	if (isMonochrome)
	{
		Surface.Y2RGB(surfaces[1]);
	}
	else
	{
		Surface.copyFromPlanar(surfaces);
	}

	TRACE_3(errout << "CBCBCBCBCBCBCB After computing surfaces (" << int(stopwatch.Read(true))
		 << " msec) CBCBCBCBCBCBCB");

	// % the correction at the full resolution
	// Corr = imresize(Surface,[size(SrcC0,1),size(SrcC0,2)], 'lanczos3');
	auto Corr = Surface.resize(_roiSize);

	TRACE_3(errout << "CBCBCBCBCBCBCB After Resize back (" << int(stopwatch.Read(true)) << " msec) CBCBCBCBCBCBCB");

	// Res = SrcC0.*Corr;
	auto Res = SrcC0 * Corr;

	TRACE_3(errout << "CBCBCBCBCBCBCB After Correction (" << int(stopwatch.Read(true)) << " msec) CBCBCBCBCBCBCB");

	// Out = ImgC0;
	// NOT APPLICABLE - either it's in-place of the caller did the copy!

	// Out(ROWS,COLS) = Res
	// Out = min(max(round(1024*Out), 0), 1023);
	Ipp16u* dest = outputData + (((_roiOffset.y * _imagePitchInPixels) + _roiOffset.x) * NUM_COMPONENTS);
	int destRowPitchInBytes = _imagePitchInPixels * NUM_COMPONENTS*sizeof(Ipp16u);
	ReplaceRoiInImage(dest, destRowPitchInBytes, Res.data(), Res.getRowPitchInBytes());

	TRACE_3(errout << "CBCBCBCBCBCBCB After replacing ROI (" << int(stopwatch.Read(true)) << " msec) CBCBCBCBCBCBCB");

	delete BMP;
	delete BMF;

	Ipp32fArray nil;
	Res = nil;
	Corr = nil;
	Surface = nil;
	srcP2 = nil;
	srcP1 = nil;
	srcC0 = nil;
	srcF1 = nil;
	srcF2 = nil;
	SrcP2 = nil;
	SrcP1 = nil;
	SrcC0 = nil;
	SrcF1 = nil;
	SrcF2 = nil;

#else

	Ipp16u* dest = outputData + (((_roiOffset.y * _imagePitchInPixels) + _roiOffset.x) * NUM_COMPONENTS);
	int destRowPitchInBytes = _imagePitchInPixels * NUM_COMPONENTS*sizeof(Ipp16u);
	MakeRoiWhite(dest, destRowPitchInBytes);

#endif

	TRACE_3(errout << "CBCBCBCBCBCBCB EXIT Bump (" << int(stopwatch2.Read()) << " msec) CBCBCBCBCBCBCB");

	return 0;
}

namespace
{

	void Convert16uToNormalized32f(Ipp16u* src, int srcStep, Ipp32f *dst, int dstStep, IppiSize size, Ipp16u maxValue)
	{
		// Convert to floats.
		ippiConvert_16u32f_C3R(src, srcStep, dst, dstStep, size);

		// To avoid divide by zero errors, add one to component values!
		Ipp32f ones[3] =
		{1.F, 1.F, 1.F};
		ippiAddC_32f_C3R(dst, dstStep, ones, dst, dstStep, size);

		// Normalize to range 0.0 < N <= 1.0 .
		auto scale = Ipp32f(maxValue + 1); // presumably maxValue is (power of 2) - 1
		Ipp32f scales[3] =
		{scale, scale, scale};
		ippiDivC_32f_C3R(dst, dstStep, scales, dst, dstStep, size);
	}

	struct ConvertParams
	{
		Ipp16u *src; // Pointer to first source element of interest
		int srcStep; // Source row pitch in bytes
		Ipp32f *dst; // Pointer to first destination element
		int dstStep; // Destination row pitch in bytes
		IppiSize size; // Witdh and height of pixels to be converted
		Ipp16u maxValue; // The maximum source value: must be (power of 2) - 1
		int numberOfJobs; // The total number of jobs for segmentation calculation
	};

	void RunConversionJob(void *vp, int jobNumber)
	{
		if (vp == nullptr)
		{
			return;
		}

		// Image segmented by blocks of rows.
		auto params = static_cast<ConvertParams*>(vp);
		IppiSize jobSize;
		jobSize.width = params->size.width;

		// Evenly distribute n extra rows over the first n jobs.
		auto minRowsPerJob = params->size.height / params->numberOfJobs;
		auto extraRows = params->size.height % params->numberOfJobs;
		auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
		auto jobSrc = params->src + (jobY * (params->srcStep / sizeof(Ipp16u)));
		auto jobDst = params->dst + (jobY * (params->dstStep / sizeof(Ipp32f)));
		jobSize.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

		// Do this job!
		Convert16uToNormalized32f(jobSrc, params->srcStep, jobDst, params->dstStep, jobSize, params->maxValue);
	}

	void RunResizeJob(void *vp, int jobNumber)
	{
		if (vp == nullptr || jobNumber > 4)
		{
			return;
		}

		auto params = static_cast<ResizeSourceParams*>(vp);
		params->dst[jobNumber] = params->src[jobNumber].resize(params->dstSize);
	}

} // Anonymous namespace

Ipp32fArray ColorBumper::ExtractRoiAsFloat(Ipp16u* imageData)
{
	ConvertParams params;
	params.maxValue = _maxValue;
	params.size.width = _roiSize.width;
	params.size.height = _roiSize.height;

	params.src = imageData + (((_roiOffset.y * _imagePitchInPixels) + _roiOffset.x) * NUM_COMPONENTS);
	params.srcStep = _imagePitchInPixels * NUM_COMPONENTS*sizeof(Ipp16u);

	Ipp32fArray result({_roiSize, NUM_COMPONENTS});
	params.dst = result.data();
	params.dstStep = result.getRowPitchInBytes();

	SynchronousMthreadRunner mthreads(&params, RunConversionJob);
	params.numberOfJobs = mthreads.GetNumberOfThreads();

	mthreads.Run();

	return result;
}

void ColorBumper::ResizeSourceFrames(ResizeSourceParams *params)
{
	SynchronousMthreadRunner mthreads(5, params, RunResizeJob);
	mthreads.Run();
}

void ColorBumper::ReplaceRoiInImage(Ipp16u* dest, int destrowPitch, Ipp32f* roiData, int roiRowPitch)
{
	// Convert ROI components back to full range.
	// First convert to values in the range (0 - 65535)
	// then to the final depth, so clamping works right.
	int floatPitch;
	int intPitch;
	Ipp32f* floatTemp = ippiMalloc_32f_C3(_roiSize.width, _roiSize.height, &floatPitch);
	Ipp16u* intTemp = ippiMalloc_16u_C3(_roiSize.width, _roiSize.height, &intPitch);

	// Scale floats to full 16-bit integer range (0 < N <= 65536).
	Ipp32f fullScale[3] =
	{65536.F, 65536.F, 65536.F};
	ippiMulC_32f_C3R(roiData, roiRowPitch, fullScale, floatTemp, floatPitch, _roiSize);

	// Remember that one we added during extraction to avoid divide by zero errors?
	// It needs to get subtracted off before converting back to ints!
	Ipp32f scaledOne = 65536.F / (_maxValue + 1); // Max value presumed to be (power of 2) - 1
	Ipp32f scaledOnes[3] =
	{scaledOne, scaledOne, scaledOne};
	ippiSubC_32f_C3R(floatTemp, floatPitch, scaledOnes, floatTemp, floatPitch, _roiSize);

	// Convert to integers.
	ippiConvert_32f16u_C3R(floatTemp, floatPitch, intTemp, intPitch, _roiSize, ippRndNear);

	// Replace ROI in image with correctly scaled ROI data
	Ipp16u maxScale = Ipp16u(65536 / (_maxValue + 1)); // Max value presumed to be (power of 2) - 1
	Ipp16u maxScales[3] =
	{maxScale, maxScale, maxScale};
	ippiDivC_16u_C3RSfs(intTemp, intPitch, maxScales, dest, destrowPitch, _roiSize, 0);

	ippiFree(floatTemp);
	ippiFree(intTemp);
}

void ColorBumper::MakeRoiWhite(Ipp16u* dest, int destrowPitch)
{
	// Convert ROI components back to full range.
	// First convert to values in the range (0 - 65535)
	// then to the final depth, so clamping works right.
	int floatPitch;
	int intPitch;
	Ipp32f* floatTemp = ippiMalloc_32f_C3(_roiSize.width, _roiSize.height, &floatPitch);
	Ipp16u* intTemp = ippiMalloc_16u_C3(_roiSize.width, _roiSize.height, &intPitch);

	Ipp32f* roiData = ippiMalloc_32f_C3(_roiSize.width, _roiSize.height, &floatPitch);
	ippiSet_32f_C3CR(1.0F, roiData, floatPitch, _roiSize);

	// Scale floats to full 16-bit integer range.
	Ipp32f fullScale[3] =
	{65536.F, 65536.F, 65536.F};
	ippiMulC_32f_C3R(roiData, floatPitch, fullScale, floatTemp, floatPitch, _roiSize);

	// Convert to integers.
	ippiConvert_32f16u_C3R(floatTemp, floatPitch, intTemp, intPitch, _roiSize, ippRndNear);

	// Replace ROI in image with correctly scaled ROI data
	Ipp16u maxScale = 65536 / (_maxValue + 1); // Max value presumed to be (power of 2) - 1
	Ipp16u maxScales[3] =
	{maxScale, maxScale, maxScale};
	ippiDivC_16u_C3RSfs(intTemp, intPitch, maxScales, dest, destrowPitch, _roiSize, 0);

	ippiFree(floatTemp);
	ippiFree(intTemp);

	ippiFree(roiData);
}

int ColorBumper::flashFrame(const std::vector<Ipp16uArray> &inputFrames, Ipp16uArray &outputFrame,	 const MtiRect &repairRoi, bool isMonochrome)
{
//   DBCOMMENT("Start flashFrame");
	auto originalBits = inputFrames[0].getOriginalBits();
	auto maxValue = (1 << originalBits) - 1;
   auto imageSize = inputFrames[0].getSize();

   // Now loop the five frames
//	CHRTimer analysisTimer;
  //	auto factor = 1 << (16 - originalBits);

	// The C++ one frameinterface requires 16 bit images with no downsampling
	// We need a C++ interface that is like the C interface of ZonalFlickerBseWrapper
	// For now, use the C interface

//   auto processRoi = repairRoi; //inputFrames[0].getRoi();
   auto processRoi = inputFrames[0].getRoi();
//   DBTRACE2(processRoi, repairRoi);

	_currentZonalParamsStruct.Width = processRoi.width;
	_currentZonalParamsStruct.Height = processRoi.height;
	_currentZonalParamsStruct.TotalFrames = 5;
	_currentZonalParamsStruct.ProcessType = 3;
   _currentZonalParamsStruct.ccols = 24;
	_currentZonalParamsStruct.crows = ZonalParameters::verticalFromHorizontalBlocks(_currentZonalParamsStruct.ccols, MtiSize(processRoi));
	_currentZonalParamsStruct.FrctOverlap = 0.25;
	_currentZonalParamsStruct.nbins = 1024;
	_currentZonalParamsStruct.FiltRad = 50;
	_currentZonalParamsStruct.MaxFlicker = 1000;
	_currentZonalParamsStruct.NumIntensityLevels = 65536;
	_currentZonalParamsStruct.TMotion = 0.0707;
	_currentZonalParamsStruct.PdfSig = 20;
	_currentZonalParamsStruct.SmoothSig = 6;
   _currentZonalParamsStruct.SmoothRad = int(round(3.5*_currentZonalParamsStruct.SmoothSig));
	_currentZonalParamsStruct.Downsample = 4;

	// Params not defined by matlab
	_currentZonalParamsStruct.InFrame = 0;
	_currentZonalParamsStruct.OutFrame = 5;
	_currentZonalParamsStruct.bitShift = 0; // Replaced by OriginalBits
  	_currentZonalParamsStruct.Roi = processRoi; // Not used

//   DBTRACE(analysisTimer);

	for (auto p : range(3))
	{
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseStart();
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseInit(_currentZonalParamsStruct);
	}

//      DBTRACE(analysisTimer);
   int lutSize = 1024;
//   auto logLut = Ipp32sArray::logLut(lutSize, 65535, CONTRAST_LOG_OFFSET );
//   auto axisLut = Ipp32sArray::linspace(0.0f, 65535.0f, CONTRAST_LUT_SIZE);
   ScratchArray scratch;

	for (auto frameIndex : range(int(_currentZonalParamsStruct.TotalFrames)))
	{
      auto analysisFrame16u = inputFrames[frameIndex](processRoi).duplicate();
      //analysisFrame16u *= factor;

      // move to 16 bits
//      analysisTimer.Start();

      auto analysisFrame = Ipp32fArray(analysisFrame16u);

      // BUG IN () for now
      analysisFrame.setOriginalBits(16);
		auto planes = analysisFrame.copyToPlanar();

      auto f = ([&](int i) {ThinPlateSplineSurface::zonalFlickerBaseWrapper[i].deflickerBaseProcessOneFrame
				 (_currentZonalParamsStruct, planes[i]);});

      IpaStripeStream iss2;
	   iss2 << 3 << efu_job(f);
      iss2.stripe();

//		std:: thread t0([&]() {ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseProcessOneFrame
//				 (_currentZonalParamsStruct, planes[0]);});
//
//		std:: thread t1([&]() {ThinPlateSplineSurface::zonalFlickerBaseWrapper[1].deflickerBaseProcessOneFrame
//				 (_currentZonalParamsStruct, planes[1]);});
//
//		std:: thread t2([&]() {ThinPlateSplineSurface::zonalFlickerBaseWrapper[2].deflickerBaseProcessOneFrame
//				 (_currentZonalParamsStruct, planes[2]);});
//		t0.joinxxx();
//		t1.joinxxx();
//		t2.joinxxx();

//		DBTRACE2("done", analysisTimer);
	}

//   DBTRACE(analysisTimer);
	vector < vector < float >> componentDeltas;
	vector < vector < float >> componentSmoothed;
	for (auto p : range(3))
	{
		int n = ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseGetDeltaSize(_currentZonalParamsStruct);

		vector<float>deltaValues(n);
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseGetDeltaValues(_currentZonalParamsStruct,
			 deltaValues.data());
		componentDeltas.push_back(deltaValues);

      vector<int> anchors = {2};
      // TODO :: Decide what smoothvalue to use
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseGetSmoothedValues(_currentZonalParamsStruct, anchors,
			 deltaValues.data(), 0);
		componentSmoothed.push_back(deltaValues);
	}

//   DBTRACE(analysisTimer);
	std::vector<int>ctrlRows(int(_currentZonalParamsStruct.crows));
	std::vector<int>ctrlCols(int(_currentZonalParamsStruct.ccols));

//	ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseFindControlPosition(_currentZonalParamsStruct,
//		 repairRoi.x, ctrlCols, repairRoi.y, ctrlRows);

	ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseFindControlPosition(_currentZonalParamsStruct,
		0, ctrlCols, 0, ctrlRows, MtiSize(processRoi));

//	 DBTRACE(dumpVector(ctrlCols));
//	 DBTRACE(dumpVector(ctrlRows));
//
//	DBTRACE(repairRoi);

	// COMPUTE REPAIR
	// We have the deltas, this should be moved into thinPlateSplineSurface

	// Create the control points
	vector<IppiPoint>controlPoints;

	// These come from matlab so rows move fastest
	for (auto c : ctrlCols)
	{
		for (auto r : ctrlRows)
		{
			controlPoints.push_back({c, r});
		}
	}

//   DBTRACE(analysisTimer);

	auto controlPointsPerFrame = controlPoints.size();

	auto repairFrame16u = inputFrames[2](processRoi).duplicate();
  //	repairFrame16u *= factor;
  //	repairFrame16u.applyLutInPlace({logLut}, axisLut, scratch);
	auto repairFrameFloat = Ipp32fArray(repairFrame16u);
	auto repairPlanes = repairFrameFloat.copyToPlanar();

	auto f = [&](int p)
	{
		ThinPlateSplineSurface tps;
		tps.reinitialize(controlPoints, imageSize);
		auto repairFrameOffset = componentSmoothed[p].data() + 2 * controlPointsPerFrame;
		auto repairFrameOffset2 = componentSmoothed[p].data() + 3 * controlPointsPerFrame;
		vector<float>heights;
		for (auto i : range(controlPointsPerFrame))
		{
			heights.push_back((repairFrameOffset[i] - repairFrameOffset2[i]) / 65536);
		}

		tps.computeWeightsAndSurface(heights, repairPlanes[p].getSize(), repairPlanes[p].getRoi(), nullptr);
		tps.applyDeltaSurfaceInPlace(repairPlanes[p], p, repairPlanes[p].getRoi(), 65535);
	};

	IpaStripeStream iss;
	iss << 3 << efu_job(f);
//   DBTRACE(analysisTimer);
	iss.stripe();
//   DBTRACE(analysisTimer);

	auto repairedFrame = Ipp16uArray(repairPlanes.toArray());
  // MatIO::saveListToMatFile("c:\\temp\\test.mat", repairedFrame, "repairFrame", axisLut, "axisLut", logLut, "logLut");

 //  repairedFrame.applyLutInPlace({axisLut}, logLut, scratch);

//   repairedFrame /= factor;
 	outputFrame(repairRoi) <<= repairedFrame(repairRoi);
//   DBTRACE(analysisTimer);
   for (auto p : range(3))
	{
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseEnd();
	}

//   DBTRACE(analysisTimer);
	return 0;
}
