/*
	File:	RepairShift.h
	By:	Kevin Manbeck
	Date:	Mar 19, 2002

	The RepairShift class is use to shift regions from one location
	to another.

	Before performing a Shift operation, the application programmer must
	call the following base class functions:

	1.  setImageDimension ()
	2.  setImageComponent ();
	3.  setNFrame ();
	4.  setRepairFrame ();
	5.  setDataPointer ();
	6.  setOutputPointer ();
	7.  setMaskPointer ();

	See CRepair for details and list of optional functions.

	Shift will shift a part of one frame.  It needs to access 2
	input frames.  One frame is the UNPROCESSED image, the other frame
	in the ORIGINAL PROCESSED image.  setNFrame should be 2. 
	lRepairFrame is the UNPROCESSED image and the other is ORIGINAL
	PROCESSED image.

	The Mask image should be filled in by the application programmer.
	The application programmer can use InitMask from DRS or can
	read in MASK values from the history.

	The following functions are part of the RepairDRS interface:

	************************************
		MANDITORY FUNCTIONS
	************************************

	-----------------------------------------------
	PerformShift ();
	-----------------------------------------------

	PerformShift takes the repaired values stored in one of the 
	input frames and moves them to a new location.

	This function requires that ResultFrame not share space with 
	RepairFrame, since this function copies the original values 
	back from RepairFrame.  See setResultPointer(), setDataPointer().


	************************************
		OPTIONAL FUNCTIONS
	************************************

	-----------------------------------------------------
	SetShift (float *fpRowShift, float *fpColShift);
	-----------------------------------------------------
	fRowShift and and fColShift are the amount of pixels to shift
	the region.  Positive RowShift is DOWN.  Positive ColShift is
	to the RIGHT.

	---------------------------------------------
	GetShift (long *lpRowShift, long *lpColShift);
	---------------------------------------------

	Upon return, GetShift() sets the values of lpRowShift
	and lpColShift.  These values can be by the caller to determine
	the current integral amount of shift.
*/

#ifndef REPAIR_SHIFT_H
#define REPAIR_SHIFT_H

#include "Repair.h"
#include "details.h"

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations


/////////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CRepairShift:public CRepair
{
public:
  CRepairShift();
  ~CRepairShift();

	// manditory functions:  *MUST* be called by application
  int PerformShift ();

	// optional functions
  void SetShift (float fRowShift, float fColShift);
  void GetShift (long *lpRowShift, long *lpColShift);

private:
  long
    lRowShift,
    lColShift;

  float
    fRowShift,
    fColShift;
};
#endif
