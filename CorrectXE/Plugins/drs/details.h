/*
	File:		details.h
	Written by:	Kevin Manbeck
	Date:		September 8, 1998

	This header file contains low-level information necessary to
	repair defects.  This should not be included in the main 
	program.
*/
#ifndef DETAILS_H
#define DETAILS_H

#define NINT(x)     (( (x)>0 )?((long)((x)+0.5)):((long)((x)-0.5)))
#define INFINITY_FLOAT		1.0E10
#define INFINITY_LONG		268435456      /* 2^28 */
#define KKSQR(a) ( (kksqrarg=(a)) == 0 ? 0.0 : kksqrarg*kksqrarg )

#define MAX_DIM 20
#define MAX_FRAME_PAIR 3
#define CORRECTION_ORDER 2

/*
	Values used by the pick table
*/

#define PICK_TABLE_DIM 16
#define NUM_PICK_INDEX 32
#define NUM_PICK_UNIQUE 40
#define PICK_A 0x0
#define PICK_B 0x1

#include "Repair.h"    // Ugggh QQQ... for MAX_FRAME


/*
	Detailed information used by ordinary debris removal
*/

typedef struct
 {
  long
    lMotionRow0,
    lMotionCol0,
    lMotionRow1,
    lMotionCol1;

  float
    fSurroundScore,	/* score due to surround motion */
    fScore;		/* score of surround and interior region */
 }  MOTION;

typedef struct
 {
  long
    lNRegion,
    *lpRegionRowCol,	/* defective region */
    lNSurround,
    *lpSurroundRC;	/* surrounding area */

  long
    *lpSurroundData;

  float
    **fpSurroundScore[MAX_FRAME];

  long
    lMinOffsetRow,	// The offsets allowed by the caller
    lMinOffsetCol,
    lMaxOffsetRow,
    lMaxOffsetCol,
    lMinMotionRow,	// The motions allowed by the current image
    lMinMotionCol,	// may be different than Offset if region is
    lMaxMotionRow,	// near the edge of picture
    lMaxMotionCol;

  long
    lNRowMot,
    lNColMot;

  MTI_UINT8
    *ucpLabMask;

  MTI_UINT16
    *uspMotData[MAX_FRAME];	/* values used to estimate motion */

  long
    lNSelectRowCol,
    lNSurroundRowCol,
    *lpSelectRowCol,
    *lpSurroundRowCol;
 } REPAIR_DEBRIS;

class CCoarseLevel
{
public:
  CCoarseLevel();
  ~CCoarseLevel();

  long
    lNDim,			/* number of coarse dimensions */
    laDimension[MAX_DIM],	/* list of coarse dimensions */
    lBestPair,
    lNPair;			/* number of frame pairs */

  REPAIR_DEBRIS
    rdaRepair[MAX_DIM],		/* a separate REPAIR_DEBRIS structure for
				each resolution */
    rdOrig;			/* used to estimate motion when gradients
				are computed */

  MOTION
    maBestMotion[MAX_DIM][MAX_FRAME_PAIR],
    mBestAccel;

  long
    laNMotion[MAX_FRAME_PAIR];

  MOTION
    *mpMotion[MAX_FRAME_PAIR];

  long
    laFrame0[MAX_FRAME_PAIR],
    laFrame1[MAX_FRAME_PAIR];

  float
    faFactor[MAX_FRAME_PAIR];

  long
    laPickValue[NUM_PICK_UNIQUE][NUM_PICK_INDEX];

  void BuildPickTable();
  void Free();
};
#endif
