#ifndef MKLMATRIX_H
#define MKLMATRIX_H

#include "mkl.h"

#include "MTIstringstream.h"
#include "IniFile.h"
#include <cstddef>
#include <iostream>
#include <math.h>
#include <memory>
#include <stdlib.h>

//#define ALIGN_EACH_MKLMATRIX_ROW

struct MklDataWrapper
{
   std::shared_ptr<void *> _blob;
   float *_origin;
   int _rowPitchInFloats;
   int _nRows;
   int _nCols;

   MklDataWrapper(int nRows = 0, int nCols = 1);
   MklDataWrapper(const MklDataWrapper &orig);
   ~MklDataWrapper();

   float *operator[](int row) const;
   MklDataWrapper& operator =(const MklDataWrapper &rhs);
   MklDataWrapper operator*(const MklDataWrapper &rhs);

   void zero();
   void ident();
   MklDataWrapper clone(int nRows = -1) const;
};

//*******************************************************
//*******************************************************************
class MklVector32f
{
   MklDataWrapper _data;

public:
   MklVector32f();
   MklVector32f(int n);
   MklVector32f(int n, const float *values);
#ifndef ALIGN_EACH_MKLMATRIX_ROW
	MklVector32f(const MklDataWrapper &data);
#endif
	MklVector32f(const MklDataWrapper &data, int row);
	MklVector32f(const MklVector32f &v);

   int length() const { return _data._nRows; };
   float *dataPtr() const { return _data._origin; };

	MklVector32f& operator =(const MklVector32f &rhs);
	float& operator[](int i) const { return _data[0][i]; };
	MklVector32f operator+(const MklVector32f &rhs) const;
	MklVector32f operator-(const MklVector32f &rhs) const;
	MklVector32f operator*(float scalar) const;
	friend MklVector32f operator*(float scalar, MklVector32f &rhs );
	bool operator==(const MklVector32f &v) const;

   float euclideanNorm() const;

   void zero(void);
   void fill(float value);

	MklVector32f clone() const;
   void copyDataFrom(const MklVector32f &v);
   const MklDataWrapper &getMklData() const;
};

//*******************************************************
class MklMatrix32f
{
   MklDataWrapper _data;

public:
	MklMatrix32f();
	MklMatrix32f(int r, int c);
   MklMatrix32f(const MklDataWrapper &data, int nRows = -1);
	MklMatrix32f(const MklMatrix32f &s);
	MklMatrix32f(const MklVector32f &s);

	int rows() const { return _data._nRows; };
   int cols() const { return _data._nCols; };
   int lda() const  { return _data._rowPitchInFloats; };
   float *dataPtr() const { return _data._origin; };

	MklMatrix32f& operator =(const MklMatrix32f& rhs);
	MklVector32f operator[](const int row) const;
	MklVector32f operator*(const MklVector32f&) const;
	MklMatrix32f operator*(float scalar) const;
	friend MklMatrix32f operator*(const float &scalar, const MklMatrix32f &m);
	MklMatrix32f operator*(const MklMatrix32f& rhs) const;
	MklMatrix32f operator+(const MklMatrix32f& rhs) const;
	MklMatrix32f operator-(const MklMatrix32f& rhs) const;
   MklMatrix32f mldivide(const MklMatrix32f &rhs) const;
   MklVector32f mldivide(const MklVector32f &rhs) const;

	MklMatrix32f transpose() const;
	MklMatrix32f invert() const;
	MklMatrix32f clone() const;

   void zero();
   void ident();
};

#endif // MKLMATRIX_H

