#ifndef RGBFrameDatumH
#define RGBFrameDatumH

#define TL_CHAN_COUNT 3
#define TL_QUANTILE_COUNT 10
#define TL_QUANTILE_COUNT2 (TL_QUANTILE_COUNT+2)

struct RGBFrameDatum
{
   double Quantile[TL_CHAN_COUNT][TL_QUANTILE_COUNT];
};

#endif
