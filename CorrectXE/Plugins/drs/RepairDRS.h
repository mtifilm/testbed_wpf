#ifndef RepairDrsH
#define RepairDrsH

/*
	File:	RepairDRS.h
	By:	Kevin Manbeck
	Date:	June 29, 2001

	The RepairDRS class is use to perform DRS type repairs.

	Before performing a DRS repair, the application programmer must
	call the following base class functions:

	1.  setImageDimension ()
	2.  setImageComponent ();
	3.  setNFrame ();
	4.  setRepairFrame ();
	5.  setDataPointer ();
	6.  setResultPointer ();
	7.  setMaskPointer ();
    8.  InitMask();

	See CRepair for details and list of optional functions.

	DRS will repair one frame.  It can use between 1 and 5 frames to
	do the repair.

	The following functions are part of the RepairDRS interface:

	************************************
		MANDATORY FUNCTIONS
	************************************

	--------------------------------------------
	setMotionAcceleration (long AcclRow, long AcclCol);
	---------------------------------------------

	Estimating motion is an important aspect of repairing dirt.
	AcclRow and AcclCol are the allowed acceleration in the row and
	column directions.

	-------------------------------------------
	setMotionOffset (long OffsetRow, long OffsetCol);
	-------------------------------------------

	Set the maximum amount of motion that will be detected by motion
	estimation.

	Note:  if OffsetRow or OffsetCol is so large that the debris region
	would fall outside the region of interest, the RepairDRS class
	will automatically reduce the OffsetRow or OffsetCol.
	The application programmer does not need to worry about this condition.


	***************************************
		OPTIONAL FUNCTIONS
	***************************************

	---------------------------------------------------
	setDimension (long NDim, long Dimension);
	---------------------------------------------------

	The constructor sets this to a default value.  Override with values
	taken from config file.

	----------------------------------------------------
	setPick (long PickRow, long PickCol, long PickFrame);
	----------------------------------------------------

	The constructor sets this to a default value.  Override with values
	taken from config file.

	-------------------------------------
	setIterations (long HardMotionIters);
	-------------------------------------

	The constructor sets this to a default value.  Override with values
	taken from config file.

	--------------------------------------------------------------
	setPercent (float UseAveragePercent, float UseOriginalPercent);
	--------------------------------------------------------------

	The constructor sets this to a default value.  Override with values
	taken from config file.

	------------------------------
	setGradient (bool UseGradient);
	------------------------------

	The constructor sets this to a default value.  Override with values
	taken from config file.

	------------------------------
	setGrainPresetValues (const float *grainPresetValues);
	------------------------------

	The constructor sets this to NULL.  Override with a pointer to a grain
	preset values if you want to use the grain tool's grain generation.

	-----------------------------
	PerformFix (long RepairType);
	-----------------------------

	PerformFix actually performs the DRS repair.  The RepairType
	argument can be:

		RT_ORDINARY  (note the value of offset determines whether this
				will be a "high motion" or "low motion" fix.)
		RT_HARD_MOTION
		RT_ADJUST_DENSITY
		RT_HARD_MOTION_2

	----------------------------------------
	MoveFix (float fMoveRow, float fMoveCol)
	----------------------------------------

	MoveFix moves the previous fix by the amount specified.  The box or
	lasso remains in the same location, but the contents are shifted.

	MoveFix will return an error if the RepairType is not RT_ORDINARY.

*/

#include "ColorBumper.h"
#include "details.h"
#include "drsDLL.h"
//#include "GrainCoreCommon.h"
#include "ImageDatumConvert.h"
#include "mthread.h"
#include "Repair.h"

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations


/////////////////////////////////////////////////////////////////////////

#define BOUNDARY_ALLOC 1000
#define BUFFER_ALLOC 10000

#define MIN_DENSITY 0.
#define MAX_DENSITY 2.
#define MIN_GRAIN_SIZE .05
#define MAX_GRAIN_SIZE .45
#define MIN_GRAIN_STRENGTH 1.
#define GRAIN_STRENGTH_LOW_LIMIT .5

typedef struct
 {
  long 
    lNRegion,
    lSum,
    laSum[MAX_JOB],
    lNJob,
    *lpRegionRowCol;

  MTI_UINT16
    *uspMotData0,
    *uspMotData1;
 } REGION_SCORE_STRUCT;

typedef struct
 {
  long 
    lNSurround,
    lNSelect,
    lSurroundSum0,
    lSurroundSum1,
    lSelectSum,
    laSurroundSum0[MAX_JOB],
    laSurroundSum1[MAX_JOB],
    laSelectSum[MAX_JOB],
    lNJob,
    *lpSurroundRowCol,
    *lpSelectRowCol;

  MTI_UINT16
    *uspMotData,
    *uspMotData0,
    *uspMotData1;
 } CALC_SCORE_STRUCT;

typedef struct
 {
  long 
    lNSurround,
    lSum,
    laSum[MAX_JOB],
    lNJob,
    *lpSurroundRC,
    *lpSurroundData,
    lMotionRC;

  MTI_UINT16
    *uspMotData;
 } CALC_SURROUND_SCORE_STRUCT;

typedef struct
 {
  long
    lNRowMot,
    lNColMot,
    lNJob,
    lCoarse,
    lNCoarse,
    lNFineCol,
    lNFineCom;

  MTI_UINT16
    *uspDest,
    *uspSource;
 } COPY_INPUT_STRUCT;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class MTI_DRSDLL_API CRepairDRS:public CRepair
{
public:
  CRepairDRS();
  ~CRepairDRS();

  // For optimization we steal this call to save pointers locally

	// mandatory functions:  *MUST* be called by application
  int setMotionAcceleration (long AcclRow, long AcclCol);
  int setMotionOffset (long OffsetRow, long OffsetCol);

	// optional functions:  *MAY* be called by application
  int setDimension (long NDim, long Dimension);
  int setPick (long PickRow, long PickCol, long PickFrame);
  int setIterations (long HardMotionIters);
  int setPercent (float UseAveragePercent, float UseOriginalPercent);
  void setGradient (bool UseGradient);
  void setGrainPresetValues (const float *grainPresetValues);
  void setUseGrainPresets(bool flag);
  void setColorBumpCrap(bool monochromeFlag, int direction, int validFrameMask);
  int PerformFix (long RepairType);
  int MoveFix (float fMoveRow, float fMoveCol);
  void ReleaseMemory();

  void DoHardMotionEdgePreserving (int iJob);
  void DoHardMotionSmoothing (int iJob);

  // functions used to override the default result
  void OverrideMoveOver (float newRow, float newCol);
  void OverrideMoveUnder (float newRow, float newCol);
  void OverrideColorBrightness (int newBrightness);
  void OverrideColorSaturation (int newSaturation);
  void OverrideColorRed (int newRed);
  void OverrideColorGreen (int newGreen);
  void OverrideColorBlue (int newBlue);
  void OverrideGrainSize (int newSize);
  void OverrideGrainStrength (int newStrength);
  void OverrideRotateOver (float newRotate);
  void OverrideRotateUnder (float newRotate);
  void OverrideTransparency (int newTransparency);
  void CopyImage (MTI_UINT16 *Dst, const MTI_UINT16 *Src);
  void OverridePrepare ();
  void OverrideApply ();
  int  OverrideUpdate ();

private:
	/* parameters that control motion estimation */
  long
    lAcclRow,		// allowed motion acceleration
    lAcclCol,
    lOffsetRow,		// allowed motion offset
    lOffsetCol,
    lDebrisULCRow,
    lDebrisULCCol,
    lDebrisLRCRow,
    lDebrisLRCCol;

	/* esoteric parameters that control algorithms */
  long
    lRepairType,	// see RT_* for values
    lNDim,		// used to speed computations
    lDimension,		// used to speed computations
    lPickRow,		// used to control frame selection for repairs
    lPickCol,
    lPickFrame,
    lHardMotionIters;	// number of interations for hard motion 2 

  long
    lMotionULCRow,	// ULC used for motion
    lMotionULCCol,
    lMotionLRCRow,	// LRC used for motion
    lMotionLRCCol,

    lSrcBoundTop,
    lSrcBoundBottom,
    lSrcBoundLeft,
    lSrcBoundRight,

    lDstBoundTop,
    lDstBoundBottom,
    lDstBoundLeft,
    lDstBoundRight,

    lSrcPaddedTop,
    lSrcPaddedBottom,
    lSrcPaddedLeft,
    lSrcPaddedRight,

    lDstPaddedTop,
    lDstPaddedBottom,
    lDstPaddedLeft,
    lDstPaddedRight;

	/* esoteric parameters that control algorithms */
  float
    fUseAveragePercent,	// threshold for using average vs pick
    fUseOriginalPercent;// threshold for using new vs original

	/* esoteric parameters that control algorithms */
  bool
    bUseGradient;	// use the gradient for motion search

  bool
    bIsMonochrome; // Data looks like RGB but is relly monochrome

  ColorBumpDirection
	 cbdDirection;

  int _validFrameMask = 0;

  const float
	 *_grainPresetValues;

  bool
    _useGrainPresets;


	/*
		Internal structures used to perform repairs
	*/

  CCoarseLevel
    *clp;

  long
    lNBoundary,
    lNBoundaryAlloc,	// used by smoothing hard motion
    *lpBoundaryRow,
    *lpBoundaryCol,
    *lpBoundaryData;

  float
    *fpBuffer;		// used by edge preserving hard motion

  long
    lNBufferAlloc;

	// structures used to multithread RegionScore()
  MTHREAD_STRUCT
    msRegionScore;

  REGION_SCORE_STRUCT
    rss;

	// structures used to multithread CalcScore()
  MTHREAD_STRUCT
    msCalcScore;

  CALC_SCORE_STRUCT
    css;

	// structures used to multithread CalcSurroundScore()
  MTHREAD_STRUCT
    msCalcSurroundScore;

  CALC_SURROUND_SCORE_STRUCT
    csss;

	// structures used to multithread CopyInput()
  MTHREAD_STRUCT
    msCopyInput;

  COPY_INPUT_STRUCT
    cis;

	// structure used to multithread HardMotionEdgePreserving
  MTHREAD_STRUCT
    msHardMotionEdgePreserving;

	// structure used to multithread HardMotion
  MTHREAD_STRUCT
    msHardMotionSmoothing;

  // used by Overrides
  long
    lGrainSeed;

  // Used when applying Color Bump fix
  ColorBumper colorBumper;

  float
    fMoveOverRow,
    fMoveOverCol,
    fMoveUnderRow,
    fMoveUnderCol,
    fColorBrightness,
    fColorSaturation,
    fColorRed,
    fColorGreen,
    fColorBlue,
    fGrainSize,
    fGrainStrength,
    fRotateOver,
    fRotateUnder,
    fCenterRow,
    fCenterCol,
    fSinOver,
    fCosOver,
    fSinUnder,
    fCosUnder,
    fOldSinOver,
    fOldSinUnder,
    fTransparencyOrig,
    fTransparencyProc;

  bool bYUV;

  int EstablishParameters();
  int EstimateMotion ();
  int ApplyFix ();
  int MakeCommandFile ();
  void ApplyFixOrdinary ();
  void ApplyFixAdjustColorBrightness ();
  void ApplyFixAdjustColorSaturation ();
  void ApplyFixAdjustColorRed ();
  void ApplyFixAdjustColorGreen ();
  void ApplyFixAdjustColorBlue ();
  int OrdinaryMotion ();
  int HardMotion ();
  int HardMotionSmoothing ();
  int ColorBump ();
  float CalcDist (long lRow0, long lCol0, long lRow1, long lCol1);
  int HardMotionEdgePreserving ();
  int CopyInput ();
  void GradientImage (long lDim);
  int MakeRegion (REPAIR_DEBRIS *rdp);
  int CalcSurroundScore (REPAIR_DEBRIS *rdp);
  int CreateMotionPairs ();
  int FindBestMotion ();
  int RegionScore (REPAIR_DEBRIS *rdp, long lIndex, long lPair);
  int RefineLinearMotion (long lDim);
  int DetermineAcceleration ();
  int CalcScore (REPAIR_DEBRIS *rdp, long lFrame0, long lRow0, long lCol0,
		long lFrame1, long lRow1, long lCol1, float *fpScore);
  void UseFillValue ();

  float getDefaultResult (long lRow, long lCol, long lCom);
  void CenterOfMass ();
  void getPixelAndWeight (float fRow, float fCol, long *lpRow, long *lpCol,
		float faWei[2][2]);

#ifdef __OLD_WAY__

  void getPixel(float fDstRow, float fDstCol, float *fpSrcRow, float *fpSrcCol,
		bool bOverFlag);

#else
  void getPixel_moveRotateOver (float fDstRow, float fDstCol,
									  float &fpSrcRow, float &fpSrcCol);


  void getPixel_moveRotateUnder (float fDstRow, float fDstCol,
										float &fpSrcRow, float &fpSrcCol);

  void getPixel_moveOverOnly (float fDstRow, float fDstCol,
									 float &fpSrcRow, float &fpSrcCol);

  void getPixel_moveUnderOnly (float fDstRow, float fDstCol,
									  float &fpSrcRow, float &fpSrcCol);

#endif // getPixel speedup

inline float imageDatumToFloat(unsigned short datum)
{
	return ImageDatum::toFloat(datum, bDataTypeIsReallyHalfFloat);
}

inline unsigned short floatToImageDatum(float f)
{
	return ImageDatum::fromFloat(f, bDataTypeIsReallyHalfFloat);
}

};
#endif
