#ifndef ScratchToolH
#define ScratchToolH

// ScratchTool.h: interface for the CScratch class.

#include "ToolObject.h"
#include "IniFile.h"

#define MAX_SETTING   5
#define MAX_COMPONENT 3

//////////////////////////////////////////////////////////////////////

// Scratch's Tool Number (16-bit number that uniquely identifies this tool)
#define SCRATCH_TOOL_NUMBER    11

// Scratch Tool Command Numbers
#define SC_CMD_DUMMY            1
//

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CAlgorithmPS;
class CColorSpaceConvert;
class CImageFormat;
class CPDLElement;
class CScratchParameters;

//////////////////////////////////////////////////////////////////////

class CScratch : public CToolObject
{
public:
   CScratch(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CScratch(void);

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual bool toolHideQuery(void);
   virtual int toolShutdown(void);
   virtual int toolActivate(void);
   virtual int toolDeactivate(void);
   virtual CToolProcessor* makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr);

   // Tool GUI
   virtual int toolHide(void);    // Hide the tool's GUI
   virtual int toolShow(void);    // Show the tool's GUI
   virtual bool IsShowing(void);
   virtual int TryToResolveProvisional();

   // Tool Event Handlers
   virtual int onUserInput(CUserInput &userInput);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onChangingClip();
   virtual int onNewClip(void);
   virtual int onChangingFraming(void);
   virtual int onNewMarks(void);
   virtual int onRedraw(int frameIndex);
   virtual int onMouseMove(CUserInput &userInput);

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);
   
   // Tool Configuration Save and Restore
   void ReadSettings(CIniFile *iniFile, const string &section);
   void WriteSettings(CIniFile *iniFile, const string &section);

   CBHook ClipChange;            // Needed to call back on a change
   CBHook MarksChange;           // Needed to call back on a change

   void beginStretchingRectangle(void);
   void endStretchingRectangle(bool extendToFrameEdges=false);
   void clearScratchRectangle(void);
   void setScratchRectangle(const RECT &newRect);
   bool isScratchRectangleAvailable(void);

   void resetScratchLimits();
   void changeScratchLimits(int deltaLeft, int deltaRight);
   void autoLocateScratchLimits(bool updateWhileSearching = true);
   void guessWhereTheScratchIs(int &outLeft, int &outRight, bool updateWhileSearching);

   void beginMovingMagnifier(int x, int y, int jumpFlag);
   void endMovingMagnifier(void);
   void moveMagnifierWindow(int newCenterX, int newCenterY,
                            bool constrain=false);

   void onLeftButton(int);
   void onRightButton(int);
   void onLeftArrow(bool shiftDown, bool ctrlDown);
   void onRightArrow(bool shiftDown, bool ctrlDown);
   void onUpArrow(bool shiftDown, bool ctrlDown);
   void onDownArrow(bool shiftDown, bool ctrlDown);
   
   void togglePreview(void);
   void acceptFix(void);
   void acceptGOV(void);
   void acceptFixOrGOV(void);
   void rejectFix(void);
   void clearPreview(void);

   void hideOverlays(void);
   void showOverlays(void);
   void hideSidecar();
   void showSidecar();

   void setMajorState(int majorstate);
   int  getMajorState(void);

   CColorSpaceConvert *getToRGBConverter(void);
   CColorSpaceConvert *getFmRGBConverter(void);

   void setAutoAcceptNextRenderFlag();

private:

   void MiddleButtonDown(void);
   void MiddleButtonUp(void);
   int RunFrameProcessing(int newResumeFrame);
   int StopFrameProcessing(void);
   //int ProcessTrainingFrame(void);
   int ProcessSingleFrame(EToolSetupType setupType);
   void UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                           bool processingRenderFlag);
   bool changeScratchLimitsVertical(int deltaLeft, int deltaRight);
   bool changeScratchLimitsHorizontal(int deltaBottom, int deltaTop);

   void DestroyAllToolSetups(bool notIfPaused);

public:

private:

   int toolSetupHandle;    // Init to -1
   int trainingToolSetupHandle;
   int singleFrameToolSetupHandle;

#define MAJORSTATE_GUI_CREATED               0
#define MAJORSTATE_SHOWING_SCRATCH_TOOL      1
#define MAJORSTATE_SHOWING_LOCATE_SCRATCH    2
#define MAJORSTATE_SHOWING_SET_SCRATCH_WIDTH 3
#define MAJORSTATE_SHOWING_SELECT_SIGNALS    4
#define MAJORSTATE_SHOWING_PROCESS_SCRATCH   5
   int majorState;

   // parameters

   // image format
   const CImageFormat *imgFmt;

   // active picture RECT
   RECT activeRect;

   // image format of 16-bit RGB intermediate
   CImageFormat *intFmt;

   // converter to go from native to 16-bit RGB intermediate fmt
   CColorSpaceConvert *toRGBColorConverter;

   // converter to go from 16-bit RGB intermediate fmt to native
   CColorSpaceConvert *fmRGBColorConverter;

   // track the mouse here so you always have last coords
   int mouseX, mouseY;

   // Offset from mouse to center of magnifier when the magnifier is grabbed
   int magnifierOffsetX, magnifierOffsetY;

   bool nowStretching;
   bool scratchRectangleAvailable;
   RECT scratchRect;
   RECT scratchLimitRect;

   bool nowMovingMagnifier;

#define MAX_TEMPLATE 48

   bool nowAdjustingLeft;
   bool nowAdjustingRight;

   bool hidingOverlays;

   // HACK - Larry wants 'render frame' to ALWAYS auto-accept IMMEDIATELY
   bool AutoAcceptNextRenderFlag;

   // Hack so G key will only accept if we auto-fixed
   bool NextGKeyShouldOnlyAcceptFlag;

   // Hack flag to lock out controls during processing
   bool controlLockFlag;
};

extern CScratch *GScratchTool;    // Global from ScratchToolPlugin.cpp

//////////////////////////////////////////////////////////////////////

class CScratchParameters : public CToolParameters
{
public:
   CScratchParameters(void) : CToolParameters(1, 1) {};
   virtual ~CScratchParameters(void) {};

   void ReadPDLEntry(CPDLElement *pdlToolAttribs);
   void WritePDLEntry(CPDLElement &parent);

   //
   //  parameters to control rounds of processing
   //
   long
     lNSetting;

   //
   //  parameters for Scratch Tool
   //
#define SCRATCH_STATIONARY false
#define SCRATCH_MOVES      true
   bool scratchMoves;

#define SCRATCH_TYPE_SURFACE 0
#define SCRATCH_TYPE_DEEP    1
   int  scratchType;

#define SCRATCH_ORIENTATION_VERTICAL   false
#define SCRATCH_ORIENTATION_HORIZONTAL true
   bool scratchHorizontal;

   RECT surroundingRectangle;

   // the clip frame index where the template
   // has been derived
   int templateFrame;

   // when scratch moves, the bounding box
   // of a rectangle containing a represen-
   // tative template of the scratch.
   RECT templateWindow;

   // limits of scratch within template
   RECT limitRectangle;
   int edgeTemplate0,
       edgeTemplate1;

   // accurate scratch template
   float faTemplate[MAX_TEMPLATE];

#define SELECT_SIGNAL_RED   0x01
#define SELECT_SIGNAL_GREEN 0x02
#define SELECT_SIGNAL_BLUE  0x04
#define SELECT_SIGNAL_Y     0x07
   int  scratchSelectSignal;

#define MODIFY_SIGNAL_RED   0x01
#define MODIFY_SIGNAL_GREEN 0x02
#define MODIFY_SIGNAL_BLUE  0x04
   int scratchModifySignal;

#define SCRATCH_DETAIL_RANGE 100
   int scratchDetail;

private:
   const static string movesAttr;         // true/false
   const static string typeAttr;          // Surface or Deep
   const static string typeSurface;
   const static string typeDeep;
   const static string orientationAttr;   // Horizontal or Vertical (typical)
   const static string orientationVert;
   const static string orientationHoriz;
   const static string scratchRectangleAttr;
   const static string templateFrameAttr;
   const static string limitRectangleAttr;
   const static string magnifierRectangleAttr;
   const static string selectSignalAttr;  // Component to detect scratch
   const static string modifySignalAttr;  // Component to modify: R, G & B
   const static string detailAttr;        // Integer
};

//////////////////////////////////////////////////////////////////////

class CScratchProc : public CToolProcessor
{
public:
   CScratchProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CScratchProc();

   CAlgorithmPS *AlgorithmPS;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int homeFrameIndex;      // where we were when the tool was started
   int inFrameIndex;        // Should we save the whole tool parameter object?
   int outFrameIndex;
   int iterationCount;
   CVideoFrameList *srcVideoFrameList;
};


enum EScratchCmd
{
   SCRATCH_CMD_NOOP,
   SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH,
   SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG,
   SCRATCH_CMD_START_STRETCH,
   SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH,
   SCRATCH_CMD_WARP_THEN_DRAG_MAG,
   SCRATCH_CMD_END_STRETCH_THEN_AUTOFIX,
   SCRATCH_CMD_SNAP_END_STRETCH_THEN_AUTOFIX,
   SCRATCH_CMD_REJECT_FIX,
   SCRATCH_CMD_TOGGLE_PROVISIONAL,
   SCRATCH_CMD_TOGGLE_RETICLES,
   SCRATCH_CMD_TOGGLE_SIDECAR,
   SCRATCH_CMD_PREVIEW_FRAME,
   SCRATCH_CMD_PREVIEW_ALL,
   SCRATCH_CMD_RENDER_FRAME,
   SCRATCH_CMD_RENDER_ALL,
   SCRATCH_CMD_ACCEPT_FIX,
   SCRATCH_CMD_ADD_EVENT_TO_PDL,
   SCRATCH_CMD_EXECUTE_PDL,
   SCRATCH_CMD_STOP_RENDER,
   SCRATCH_CMD_PAUSE_OR_RESUME_RENDER,

   SCRATCH_CMD_FOCUS_ON_MOVEMENT_GROUP,
   SCRATCH_CMD_FOCUS_ON_DETECT_GROUP,
   SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP,
   SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW,
   SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER,
   SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER,
   SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF,
   SCRATCH_CMD_RESET_BLEND_SLIDER,

   SCRATCH_CMD_WIDEN_LIMITS_NORMAL,
   SCRATCH_CMD_WIDEN_LIMITS_MORE,
   SCRATCH_CMD_WIDEN_LIMITS_LESS,
   SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL,
   SCRATCH_CMD_SQUEEZE_LIMITS_MORE,
   SCRATCH_CMD_SQUEEZE_LIMITS_LESS,
   SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL,
   SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE,
   SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS,
   SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL,
   SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE,
   SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS,
   SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT,
   SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT,
   SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT,
   SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT,
   SCRATCH_CMD_AUTOLOCATE_SCRATCH,
   SCRATCH_CMD_RESET_SCRATCH_LIMITS,

   SCRATCH_CMD_TOGGLE_DENSITY,
   SCRATCH_CMD_TOGGLE_ORIENTATION,
   SCRATCH_CMD_TOGGLE_MOVEMENT,
   SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP,
   SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP,
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(SCRATCH_H)


