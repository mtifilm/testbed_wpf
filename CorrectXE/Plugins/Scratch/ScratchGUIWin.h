/*
   File Name:  ScratchGUIWin.h
   Header file for the Windows-based (Borland) Scratch GUI
*/
//---------------------------------------------------------------------------

#ifndef ScratchGUIWinH
#define ScratchGUIWinH
//---------------------------------------------------------------------------
#include "ScratchTool.h"
#include "HRTimer.h"
#include "MTIUNIT.h"
#include "VTimeCodeEdit.h"

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <Mask.hpp>
#include <Menus.hpp>
#include "cspin.h"

#include "ToolSystemInterface.h"
#include "ExecButtonsFrameUnit.h"
#include "TrackEditFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include "ColorPanel.h"
#include <System.ImageList.hpp>

//---------------------------------------------------------------------------

enum EScratchLimitFocus
{
   SCRATCH_LIMIT_FOCUS_NONE=0,
   SCRATCH_LIMIT_FOCUS_LEFT,
   SCRATCH_LIMIT_FOCUS_RIGHT,
   SCRATCH_LIMIT_FOCUS_BOTH
};
//---------------------------------------------------------------------------

class CMagnifier;

//---------------------------------------------------------------------------

class TScratchForm : public TMTIForm
{
__published:	// IDE-managed Components
	TImageList *ButtonImageList;
        TPanel *ToolbarPanel;
        TLabel *DoNotDeleteLable;
    TGroupBox *ResumeGroupBox;
        TSpeedButton *GotoTCButton;
        TSpeedButton *SetStartTCButton;
        VTimeCodeEdit *ResumeTCEdit;
	TToolBar *TBToolbar1;
	TToolButton *PreviewFrameButton;
	TToolButton *PreviewAllButton;
	TToolButton *RenderAllButton;
	TToolButton *StopButton;
	TToolButton *HighLightButton;
	TToolButton *PDLCaptureButton;
        TGroupBox *MoveGroupBox;
        TRadioButton *NoMoveButton;
        TRadioButton *YesMoveButton;
        TGroupBox *OrientationGroupBox;
        TRadioButton *VerticalButton;
        TRadioButton *HorizontalButton;
        TPanel *ChannelControlPanel;
        TLabel *YLabel;
        TLabel *RLabel;
        TLabel *GLabel;
        TLabel *BLabel;
        TLabel *ImageLabel;
    TLabel *DetectLabel;
    TLabel *ProcessLabel;
    TPanel *BackgroundDetailPanel;
        TLabel *RangeMax;
        TLabel *RangeMin;
        TLabel *BackgroundToleranceLabel;
        TLabel *BackgroundToleranceValueLabel;
        TTrackBar *BackgroundToleranceTrackBar;
    TPanel *EdgeBlurPanel;
        TLabel *Label5;
        TLabel *Label6;
    TLabel *EdgeBlurLabel;
        TLabel *Label8;
    TTrackBar *EdgeBlurTrackBar;
    TPanel *StatusPanel;
    TImage *RGraphImage;
    TImage *GGraphImage;
    TImage *BGraphImage;
    TImage *MagnifierImage;
    TPanel *DummyGraphRPanel;
    TPanel *DummyGraphGPanel;
    TPanel *DummyGraphBPanel;
    TPanel *DummyGraphYPanel;
    TPanel *DummyGraphImagePanel;
    TPanel *ScratchWidthReadoutPanel;
    TLabel *ScratchWidthLabel;
        TLabel *ScratchWidthValueLabel;
    TLabel *PixelsLabel;
    TPanel *TopDummyPanel;
    TPanel *TabCoverUpPanel;
    TPanel *BottomRightClickPanel;
    TPanel *NarrowWidthGaugePanel;
    TPanel *RightSideRightClickPanel;
    TLabel *ScratchWindowLabel1;
    TLabel *ScratchWindowLabel2;
    TPanel *DetectButtonPanel;
    TRadioButton *RDetectButton;
    TRadioButton *GDetectButton;
    TRadioButton *BDetectButton;
    TRadioButton *YDetectButton;
    TPanel *ProcessCheckBoxPanel;
    TCheckBox *RCheckBox;
    TCheckBox *GCheckBox;
    TCheckBox *BCheckBox;
	TToolButton *RenderFrameButton;
    TCheckBox *ShowReticlesCheckBox;
    TStaticText *NotSetYetText;
    TPanel *MarkInWarningPanel;
    TLabel *WarningLabel;
        TLabel *WarningMessage;
	TColorPanel *ScratchControlPanel;
    TImage *YGraphImage;
   TLabel *ScratchNameLabel;
   TPanel *ScratchNamePanel;
    TExecButtonsFrame *ExecButtonsToolbar;
    TTrackEditFrame *BackgroundDetailTrackEdit;
    TExecStatusBarFrame *ExecStatusBar;
   TTimer *UpdateTimer;
   TPanel *NotSetYetPanel;
   TGroupBox *DensityGroupBox;
   TRadioButton *LightDensityButton;
   TRadioButton *DarkDensityButton;
   TTimer *SidecarTimer;
   TCheckBox *MirrorGraphsCheckBox;
   TPanel *ScratchWindowFocusPanel;
        void __fastcall PreviewAllButtonClick(TObject *Sender);
        void __fastcall PreviewFrameButtonClick(TObject *Sender);
        void __fastcall RenderAllButtonClick(TObject *Sender);
        void __fastcall StopButtonClick(TObject *Sender);
        void __fastcall PDLCaptureButtonClick(TObject *Sender);
        void __fastcall SetStartTCButtonClick(TObject *Sender);
        void __fastcall GotoTCButtonClick(TObject *Sender);
        void __fastcall BackgroundToleranceTrackBarChange(TObject *Sender);
        void __fastcall ImageMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall HorizontalButtonClick(TObject *Sender);
        void __fastcall VerticalButtonClick(TObject *Sender);
    void __fastcall ShowOrHidePDLButtonClick(TObject *Sender);
    void __fastcall ScratchWindowFocusPanelEnter(TObject *Sender);
    void __fastcall ScratchWindowFocusPanelExit(TObject *Sender);
    void __fastcall RGBCheckBoxEnter(TObject *Sender);
    void __fastcall RGBCheckBoxExit(TObject *Sender);
    void __fastcall DetectButtonEnter(TObject *Sender);
    void __fastcall DetectButtonExit(TObject *Sender);
    void __fastcall OrientationEnter(TObject *Sender);
    void __fastcall OrientationExit(TObject *Sender);
    void __fastcall MovementEnter(TObject *Sender);
    void __fastcall MovementExit(TObject *Sender);
    void __fastcall BackgroundToleranceTrackBarEnter(TObject *Sender);
    void __fastcall BackgroundToleranceTrackBarExit(TObject *Sender);
    void __fastcall EdgeBlurTrackBarEnter(TObject *Sender);
    void __fastcall EdgeBlurTrackBarExit(TObject *Sender);
    void __fastcall RenderFrameButtonClick(TObject *Sender);
    void __fastcall ShowReticlesCheckBoxClick(TObject *Sender);
    void __fastcall MovementButtonClick(TObject *Sender);
    void __fastcall UpdateTimerTimer(TObject *Sender);
        void __fastcall DetectButtonClick(TObject *Sender);
        void __fastcall ScratchWindowLabelClick(TObject *Sender);
        void __fastcall DetectLabelClick(TObject *Sender);
        void __fastcall ProcessLabelClick(TObject *Sender);
    void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(
		  TObject *Sender);
   void __fastcall DarkDensityButtonClick(TObject *Sender);
   void __fastcall DensityEnter(TObject *Sender);
   void __fastcall DensityExit(TObject *Sender);
   void __fastcall LightDensityButtonClick(TObject *Sender);
   void __fastcall SidecarTimerTimer(TObject *Sender);
   void __fastcall MirrorGraphsCheckBoxClick(TObject *Sender);
   void __fastcall BackgroundPanelClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TScratchForm(TComponent* Owner);
        __fastcall virtual ~TScratchForm(void);
private:	// User declarations
   string ScratchIniFileName;
   CHRTimer HRT; // why in gui ??
   void SetResumeTCToDefault(void);
   void RenderPreviewProcessing(TObject *Sender);
   DEFINE_CBHOOK(ClipHasChanged, TScratchForm);
   DEFINE_CBHOOK(MarksHaveChanged, TScratchForm);
   DEFINE_CBHOOK(EnterKeyPressedOnTrackBar, TScratchForm);
   void EnableOrDisableMostControls(bool On);
   void PreviewFrame(void);
   void RenderFrame(void);
   EToolProcessingCommand GetButtonCommand(TToolButton *button);
   void SetButtonCommand(TToolButton *button, EToolProcessingCommand newCmd);

   RECT blueRectScreenCoords;
   TPoint oldSidecarAnchor;
   RECT oldClientRect;
   void moveSidecar(const RECT &rectangle);
public:
   void HandleBlueBoxStateChange(void);
   void UpdateExecutionButtons(EToolControlState toolProcessingControlState,bool processingRenderFlag);
   void GatherParameters(CScratchParameters &toolParams);
   virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
   virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.
   void __fastcall FocusControl(Controls::TWinControl* Control);

protected:
        void __fastcall CMDialogKey(TMessage &Msg);
#if 0 // This crashes a lot and I don't understand it at all
        BEGIN_MESSAGE_MAP
           MESSAGE_HANDLER(CM_DIALOGKEY, TMessage, CMDialogKey)
        END_MESSAGE_MAP(TMTIForm)
#endif // 0

public:		// User declarations
   bool doesFormUseOwnWindow() { return false; };

   bool formCreate(void);
   bool formShow(void);
   void formHide(void);
   
   void initPanel(void);
   void initMagnifier(void);
   void initLocateScratch(void);
   void activateMagnifier(void);
   void deactivateMagnifier(void);
   void showMagnifier(void);
   void hideMagnifier(void);
   static void imageMagnifierRefreshCallback(void *obj, void *mag);
   void imageMagnifierClear();
   void imageMagnifierRefresh(void *mag);
   CMagnifier *getImageMagnifier(void);
   void showSidecar();
   void hideSidecar();
   void toggleMirrorGraphsCheckBox();
   void clearImage(TImage *timg, bool hilite=false);
   void drawGraph(TImage *image, MTI_UINT16 *values,
                  MTI_UINT16 minValue, MTI_UINT16 range, bool hilite);
   //void setScratchMoves(bool mov);
   //void setScratchHorz(bool horz);
   void setScratchRectangle(const RECT &newScratchRect);
   void setScratchFrame(int frame);
   void captureScratchSignals(void);
   void setScratchLimitRectangle(const RECT &newLimitRect);
   void setScratchLimitFocusSide(EScratchLimitFocus focusSide);
   RECT getScratchRectangle(void);
   void drawScratchLimitLine(int offset);
   void drawBlueScratchSearchLimitLine(int offset);
   void drawScratchLimitLineOnOneImage(TImage *image,int offset,
                                       TColor color, TPenStyle style);
   void focusOnScratchWindow(void);
   void focusOnOrientationControls(void);
   void focusOnMovementControls(void);
   void focusOnDetectControls(void);
   void focusOnProcessControls(void);
   void focusOnBackgroundDetailControl(void);
   void toggleDensity(void);
   void toggleOrientation(void);
   void toggleMovement(void);
   void cycleUpThroughDetectControls(void);
   void cycleDownThroughDetectControls(void);
   int getScratchFrame(void);
   RECT getScratchLimitRectangle(void);
   RECT getMagnifierRectangle(void);
   void setDeferredUpdateFlag(void);

   void doPreProcessing(bool procesingSingleFrameFlag);
   void doPostProcessing(void);
   MTI_UINT16 *getPtrToMagValuesForDetectSignal();
   bool simulatePauseOrResumeButtonClick(void);

   void checkAbilityToPreviewAllOrRenderAll(void);
   void disableShowReticlesCheckBox(void);
   void enableShowReticlesCheckBox(void);
   void toggleShowReticlesCheckBox(void);

   bool HandleToolEvent(const FosterParentEventInfo &eventInfo);

   MTI_UINT16* getYVal(void);
   MTI_UINT16* getRVal(void);
   MTI_UINT16* getGVal(void);
   MTI_UINT16* getBVal(void);

   bool areAllSystemsGo(bool singleFrameFlag);

   int processingSingleFrameFlag; // Can get this from the tool infrastructure?
   int homeFrameIndex;

   // Exec buttons toolbar
   void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
   void ExecStop(void);
   void ExecCaptureToPDL(void);
   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToCurrent(void);
   void ExecSetResumeTimecodeToDefault(void);
   void ExecGoToResumeTimecode(void);
   void UpdateExecutionButtonToolbar(
         EToolControlState toolProcessingControlState,
         bool processingRenderFlag);
   bool runExecButtonsCommand(EExecButtonId command);

   void setBusyCursor(void);
   void setIdleCursor(void);

private:

   Graphics::TBitmap *ClearGrayBitmap;
   Graphics::TBitmap *ClearWhiteBitmap;

   RECT initMagWin;
   CMagnifier *imageMagnifier;
   RECT magRect;

   //bool scratchMoves;
   //bool scratchHorz;

   bool deferredUpdateFlag;
   bool OkToPreviewAllOrRenderAll;
   EScratchLimitFocus scratchLimitFocusSide;

   int limFrame;

   RECT scrRect;
   RECT limRect;

   // the fixed-point components of the oscillographs
   MTI_UINT16 iYVal[MAX_TEMPLATE];
   MTI_UINT16 iRVal[MAX_TEMPLATE];
   MTI_UINT16 iGVal[MAX_TEMPLATE];
   MTI_UINT16 iBVal[MAX_TEMPLATE];

   // the same, captured when leaving Panel 3 ("setting scratch limits");
   MTI_UINT16 iYCaptVal[MAX_TEMPLATE];
   MTI_UINT16 iRCaptVal[MAX_TEMPLATE];
   MTI_UINT16 iGCaptVal[MAX_TEMPLATE];
   MTI_UINT16 iBCaptVal[MAX_TEMPLATE];

};
//---------------------------------------------------------------------------
extern PACKAGE TScratchForm *GScratchForm;
//---------------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateScratchGUI(void);
extern void DestroyScratchGUI(void);
extern bool ShowScratchGUI(void);
extern void HandleBlueBoxStateChange(void);
extern bool HideScratchGUI(void);
extern bool IsToolVisible(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                                   bool processingRenderFlag);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void GatherParameters(CScratchParameters &toolParams);
extern void SetGUIParameters(CScratchParameters &toolParams);
extern void SetFormMode(TFormStyle);
extern bool AreAllSystemsGo(bool singleFrameFlag);
extern void DoPreProcessing(bool processingSingleFrameFlag);
extern void DoPostProcessing(void);

// Simulate button clicks (for hotkeys)
extern void PreviewFrameButtonClick(void);
extern void PreviewAllButtonClick(void);
extern void RenderAllButtonClick(void);
extern void RenderFrameButtonClick(void);
extern void PDLCaptureButtonClick(void);
extern bool PauseOrResumeButtonClick(void);

// GUI content
//bool ScratchMoves(void);

#define SCRATCH_DEPTH_SURFACE 0
#define SCRATCH_DEPTH_DEEP    1
int  ScratchDepth(void);

bool MovesButtonOn(void);
bool IsScratchVertical(void);
void SetScratchVertical(bool flag);
bool IsDensityDark(void);

void SetScratchRectangle(const RECT &scrrct);
void SetScratchLimitRectangle(const RECT &limrct);
void SetScratchLimitFocusSide(EScratchLimitFocus focusSide);
RECT GetScratchRectangle(void);
RECT GetScratchLimitRectangle(void);

MTI_UINT16 *GetPtrToMagValuesForDetectSignal();

void FocusOnScratchWindow(void);
void FocusOnOrientationControls(void);
void FocusOnMovementControls(void);
void FocusOnDetectControls(void);
void FocusOnProcessControls(void);
void FocusOnBackgroundDetailControl(void);
void ToggleDensity(void);
void ToggleOrientation(void);
void ToggleMovement(void);
void CycleUpThroughDetectControls(void);
void CycleDownThroughDetectControls(void);

#define MAGF 4 // magnification factor

void ActivateMagnifier(void);
void DeactivateMagnifier(void);
void ShowMagnifier(void);
void HideMagnifier(void);
int  SetMagnifierWindow(const RECT &);
void RecenterMagnifier(int x, int y);
RECT GetMagnifierWindow(void);
void ShowSidecar();
void HideSidecar();
void ToggleMirrorGraphsCheckBox();

MTI_UINT16 *GetPtrToMagValuesForDetectSignal();

void CheckAbilityToPreviewAllOrRenderAll(void);
void DisableShowReticlesCheckBox(void);
void EnableShowReticlesCheckBox(void);
void ToggleShowReticlesCheckBox(void);

void RefreshGraphics(void);

extern MTI_UINT32 *ScratchFeatureTable[];

#endif

