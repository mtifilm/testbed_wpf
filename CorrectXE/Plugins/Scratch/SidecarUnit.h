//---------------------------------------------------------------------------

#ifndef SidecarWindowUnitH
#define SidecarWindowUnitH
//---------------------------------------------------------------------------
#include "machine.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "ColorPanel.h"
//---------------------------------------------------------------------------

class TSidecarWindow : public TForm
{
__published:	// IDE-managed Components
   TColorPanel *SidecarImagePanel;
   TImage *MagnifierImage;
   TImage *RGraphImage;
   TImage *GGraphImage;
   TImage *BGraphImage;
   TImage *YGraphImage;

private:	// User declarations
   TMouseEvent externalImageMouseDownHandler;

public:		// User declarations
    __fastcall TSidecarWindow(TComponent* Owner, TMouseEvent imageMouseDownHandler);
    __fastcall ~TSidecarWindow();
};
//---------------------------------------------------------------------------
extern PACKAGE TSidecarWindow *SidecarWindow;
//---------------------------------------------------------------------------
#endif
