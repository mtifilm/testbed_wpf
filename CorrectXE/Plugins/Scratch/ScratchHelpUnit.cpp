//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ScratchHelpUnit.h"
#include "ScratchGUIWin.h"

#include "IniFile.h"
#include "MTIio.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "SHDocVw_OCX"
#pragma resource "*.dfm"
TScratchHelpForm *ScratchHelpForm;
//---------------------------------------------------------------------------

namespace {
   const int NumTabs(2);
   const string HelpFilenames[NumTabs] = {"ScratchOperHelp_1.html",
                                          "ScratchFkeyHelp_1.html"
                                         };

   const string ScratchOperHelp_HTML(
      "<html>\n"
      "<head>\n"
      "<style type=\"text/css\">\n"
      "  li {margin-bottom: 5px}\n"
      "  td {font-size: 80%}\n"     // Don't know why it doesn't inherit from body
      "  body {font-family: tahoma, arial, sans-serif;\n"
      "        font-size: 80%;\n"
      "        margin-top: 20px}\n"
      "</style>\n"
      "</head>\n"
      "<body style=\"font-family: tahoma, arial, sans-serif; font-size: 80%\">\n"
      "<ol>\n"
      "<li>Establish IN and OUT marks for processing</li>\n"
      "<li>Orientation: is the scratch <b>Vertical</b> or <b>Horizontal</b>?</li>\n"
      "<li>Movement: is the scratch moving?</li>\n"
      "<li>On the Image: Use the LEFT mouse button to draw a box around the scratch.\n"
          " Use SHIFT to extend box to the limits of the frame.\n"
          "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n"
          "<tr><td nowrap valign=\"top\">Hint 1:&nbsp;</td>"
          "    <td>Draw box so that only <b>one</b> scratch is included.</td></tr>\n"
          "<tr><td nowrap valign=\"top\">Hint 2:&nbsp;</td>"
          "    <td>If there is movement, draw the box wide enough (or high enough)\n"
          " to include the scratch at <b>all</b> points.</td></tr></table>\n"
          "After drawing the box, a red <b>magnifier</b> will appear at the top of it.</li>\n"
      "<li>Use the LEFT mouse button to click on and drag the <b>magnifier</b>\n"
          "over the scratch. The Scratch Window on the tool will react to the positioning.\n"
          "<br>Use SHIFT to constrain movement to one axis.\n"
          "<br>Use CTRL to click outside of the magnifier and have it jump to the cursor.</li>\n"
      "<li>On the Scratch Tool: Within the Scratch Window, use LEFT and RIGHT\n"
          "mouse button clicks on either side of the scratch to define its limits.\n"
          "Use arrow keys to fine-tune.</li>\n"
      "<li>If desired, select signal to <b>detect</b> scratch\n"
      "<br>(default is Y - luminance).</li>\n"
      "<li>If desired, select signal(s) to <b>process</b>\n"
      "<br>(default is R, G & B).</li>\n"
      "<li>Use <b>Background Detail</b> slider to preserve detail.</li>\n"
      "<li>Use <b>Edge Blur</b> slider to blur edges of fix.</li>\n"
      "</body></html>\n"
   );

   const string ScratchFkeyHelp_HTML(
      "<i>Someday, a table of shortcuts will appear here!<i>\n"
   );

   string MakeHelpFilePath(const string &filename)
   {
      // forward slashes work for wither file names or URLs
      string dir = GetMTIEnv("CPMP_USER_DIR");
      unsigned i;
      for (i = 0; i < dir.size(); ++i)
         if (dir[i] == '\\')
            dir[i] = '/';

      return dir + ((dir[i-1] == '/')? "" : "/") + filename;
   };

   string MakeHelpFileURL(const string &filename)
   {
      return string("file:///") + MakeHelpFilePath(filename);
   };

   void CreateHelpFile(const string &filename, const string &contents)
   {
      string path = MakeHelpFilePath(filename);
      int fd = open(path.c_str(), O_WRONLY | O_TRUNC | O_CREAT);
      if (fd < 0)
      {
         TRACE_0(errout << "ERROR: Can't create scratch help file " << path);
         return;
      }

      if (write (fd, contents.c_str(), contents.size()) == -1)
      {
         TRACE_0(errout << "ERROR: Can't write scratch help file" << path);
      }

      close (fd);
      return;
   };

};

//---------------------------------------------------------------------------

__fastcall TScratchHelpForm::TScratchHelpForm(TComponent* Owner)
    : TForm(Owner)
{
   CreateHelpFile(HelpFilenames[0], ScratchOperHelp_HTML);
   CreateHelpFile(HelpFilenames[1], ScratchFkeyHelp_HTML);
}
//---------------------------------------------------------------------------

void __fastcall TScratchHelpForm::FormShow(TObject *Sender)
{
   if (GScratchForm != NULL)
   {
       RECT newBounds;
       newBounds.left = GScratchForm->Left - Width;
       newBounds.top = GScratchForm->Top + (GScratchForm->Height - Height)/2;
       newBounds.right = newBounds.left + Width;
       newBounds.bottom = newBounds.top + Height;
       BoundsRect = newBounds;
   }

   ShowPage();
}
//---------------------------------------------------------------------------

void TScratchHelpForm::ShowPage()
{
   int tabIndex = HelpTabControl->TabIndex;
   if (tabIndex >= NumTabs)
   {
      TRACE_0(errout << "ERROR: Unexpected number of help tabs");
      return;
   }

   string url = MakeHelpFileURL(HelpFilenames[tabIndex]);
   TVariant page = { url.c_str() };
   HTMLHelpPage->Navigate2(page);
}
//---------------------------------------------------------------------------

void __fastcall TScratchHelpForm::HelpTabControlChange(TObject *Sender)
{
   ShowPage();
}
//---------------------------------------------------------------------------

void __fastcall TScratchHelpForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   // Pretend we clicked the 'Help' button on the main form so it stays
   // synched with our state
   if (Visible)
      GScratchForm->ShowOrHideHelpButtonClick(this);
}
//---------------------------------------------------------------------------

