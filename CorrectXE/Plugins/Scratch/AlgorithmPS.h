/*
	AlgorithmPS.h	- header file for Persistent Scratch algorithm
	Kevin Manbeck
	Feb 7, 2003

	The AlgorithmPS class is used to perform vertical and horizontal
	scratch removal.  Here is the list of public functions.

        NOTE:  These functions should be called in the order described here.

        Initialize ()
		-
		- this is used to initialize the PS class. 
		- 
		- It should be called once after the AlgorithmPS class is
		- constructed.
		-
		- It should be called from CScratchProc::SetIOConfig()

	SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg)
		-
		- this is used to describe the image characteristics.
		-
		- The lNRowArg and lNColArg are used to describe the total
		- size of the image (including any vertical or horizontal
		- interval).
		-
		- The lActive arguments are used to describe the coordinates
		- of the active portion of the image.  The Start coordinates
        	- are INCLUSIVE.  The Stop coordinates are EXCLUSIVE.
		-
		- It should be called from CScratchProc::SetIOConfig()

	SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[])
		-
		- lower bound is the smallest value allowed in this format
		- upper bound is the largest value allowed in this format
		-
		- It should be called from CScratchProc::SetIOConfig()

	SetColorSpaceConvert (CColorSpaceConvert *cscpNativeToGUIArg,
		CColorSpaceConvert *cscpGUIToNativeArg)
		-
		- cscpNativeToGUIArg is the CColorSpaceConvert which
		- converts from native format to the GUI format (usually
		- RGB)
		-
		- cscpGUIToNativeArg is the CColorSpaceConvert which
		- converts from GUI format to the native format
		-
		- It should be called from CScratchProc::SetIOConfig()

	AssignParameter (CScratchParameters *afp)
		-
		- this copies user parameter out of the tool's paramter
		- class into the internal parameter structure.
		-
		- It should be called from CScratchProc::SetParameters().

	SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex)
		-
		- this is used to tell the vertical scratch algorithm where to
		- find the images to process.
		-
		- uspSrcArg is a list of 3 images.
		- lpFrameIndex is a list of relative frame indices.  Possible
		- values for lpFrameIndex:  (-2, -1, 0), (-1, 0, 1), and
		- (0, 1, 2).  These correspond to past-only, past and future,
		- and future-only.  The decision on which of these three
		- to use should depend on the output of the shot detector.
		-
		- AlgorithmPS will ignore all frames that have uspSrcArg[i]
		- set to NULL
		-
		- should be called by CScratchProc::DoProcess()

	SetDstImgPtr (MTI_UINT16 *uspDstArg)
		-
		- this is used to tell the vertical scratch algorithm where
		- to put the results
		-
		- should be called by CScratchProc::DoProcess()

	SetPixelRegionList (CPixelRegionList *prlpArg)
		-
		- this is used to tell the auto filter algorithm where
		- to put original values
		-
		- should be called by CScratchProc::DoProcess()

	Process ()
		- 
		- this is used to render the results of vertical scratch
		-
		- should be called by CScratchProc::DoProcess()
*/

#ifndef ALGORITHM_PS_H
#define ALGORITHM_PS_H
#include "ScratchTool.h"
#include "machine.h"

/////////////////////////////////////////////////////////
//  Forward declarations
class CPersistentScratchParameters;
class CPixelRegionList;
class CColorSpaceConvert;

#define MAX_COMPONENT 3
#define MAX_FRAME 3

// values used to calculate the number of background pixels to use
#define BACKGROUND_PERCENT_0   ( 5./486.)     // 5 pixels in a 525 image
#define BACKGROUND_PERCENT_50  (20./486.)     // 20 pixels in a 525 image
#define BACKGROUND_PERCENT_100 (50./486.)     // 50 pixels in a 525 image

#define SCRATCH_DEVIATION 1
#define HISTORY_SKEW 2
#define HISTORY_WIGGLE 6
#define MAX_SKEW 0.05
#define MAX_WIGGLE 0.02
#define SKEW_THRESH 1.05
//#define NULL_LEFT_COL (-1)
#define INFINITY_FLOAT		1.0E10
#define INFINITY_LONG		268435456      /* 2^28 */

typedef struct
 {
//  NOTE:  when adding new items to this structure, remember to update
//  CompareParameter() function.

  // coordinates of box which surrounds scratch in all frames.  These
  // coordinates are INCLUSIVE.
  long
    lSurroundLeft,
    lSurroundRight,
    lSurroundTop,
    lSurroundBottom;

  // coordinates of the box which contains the template.  These coordinates
  // are INCLUSIVE.
  long
    lTemplateLeft,
    lTemplateRight,
    lTemplateTop,
    lTemplateBottom;

  // the coordinates to use when processing.  See COM_*
  long
    lPrincipleComponent,// component used to track scratch
    lComponentMask;	// bit flag for components to track

  bool
    bMoving,		// true for moving, false for stationary.
    bHorizontal;        // true for horizontal, false for vertical

  long
    lType,		// see ST_* for values
    lBackgroundPixel;	// number of background pixels to use in doing repair

  // detailed template of the scratch (used to track moving scratch)
  long
    lNTemplate,		// number of entries in lspTemplate
    lFirstTemplate,	// index of first scratch pixel in fpTemplate
    lLastTemplate;	// index of last scratch pixel in fpTemplate

  float
    faTemplate[MAX_TEMPLATE];	// used to search for scratch


//  NOTE:  when adding new items to this structure, remember to update
//  CompareParameter() function.
 }  PERSISTENT_SCRATCH_SETTING;

typedef struct
 {
  long
    lPrevious;

  float
    fScore,
    fPosDeviation,
    fNegDeviation;
 } DYNAMIC_PROGRAM;

typedef struct
 {
  float
    faRoughVal[3],
    faRoughAdj[3],
    faOrigVal[3],
    faRefineVal[3];
 } WARP;

typedef struct
 {
  long
    lNRow,
    lNCol,
    *lpLeftCol;		/* left-most col of scratch */

  MTI_UINT16
    *uspYRGB;		// color space converted data

  long
    lNTemplate,		// size of the normalized template
    lWidth,		// portion of normalized template that is scratch
    lOutsideTemplate;	// number of pixels that are part of the template,
			// but are outside the scratch

  long
    lStationaryLeft;    // the pixel position non-moving scratch

  float
    *fpTemplate,	// normalized version of template
    *fpProfile,		// normalized version of image
    *fpCorr;		// correlation between fpTemplate and fpProfile

  DYNAMIC_PROGRAM
    **dppScratch;

  WARP
    **wpWarp;

  float
    fMaxSkew,
    fMaxWiggle;
 }  PERSISTENT_SCRATCH_ALGORITHM;


class CAlgorithmPS
 {
public:
  CAlgorithmPS();
  ~CAlgorithmPS();

  void Initialize ();
  int SetDimension (long lNRowArg, long lNColArg, long lNComArg,
		long lActiveStartRowArg, long lActiveStopRowArg,
		long lActiveStartColArg, long lActiveStopColArg);
  int SetImageBounds (MTI_UINT16 spLowerBoundArg[],
		MTI_UINT16 spUpperBoundArg[]);
  void SetColorSpaceConvert (CColorSpaceConvert *cscpNativeToGUIArg,
		CColorSpaceConvert *cscpGUIToNativeArg);
  int AssignParameter (CScratchParameters *afp);
  int SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex);
  void SetDstImgPtr (MTI_UINT16 *uspDstArg);
  void SetPixelRegionList (CPixelRegionList *prlpArg);
  int Process ();

private:
  //
  //  General purpose values
  //
  long
    lNRow,		// vertical dimension of image
    lNCol,		// horizontal dimension of image
    lNCom,		// number of components
    lActiveStartRow,	// location of active picture:  INCLUSIVE
    lActiveStopRow,	// location of active picture:  EXCLUSIVE
    lActiveStartCol,	// location of active picture:  INCLUSIVE
    lActiveStopCol,	// location of active picture:  EXCLUSIVE
    lNRowCol,		// total number of pixels
    lNRowColCom,	// total number of pixel*components
    laLowerBound[MAX_COMPONENT],	// minimum allowed image value
    laUpperBound[MAX_COMPONENT];	// maximum allowed image value

  //
  //  Parameter settings to control behavior of algorithm.  These can
  //  be changed as the algorithm runs.
  //
  PERSISTENT_SCRATCH_SETTING
    pssSetting;

  //
  //  Storage used internally
  //
  PERSISTENT_SCRATCH_ALGORITHM
    psaAlgorithm;

  CImageFormat
    *ifpNative,	  // native format, promoted to 16-bits
    *ifpGUI;	  // format used for GUI display

  // 
  // Used to convert between native color space and color space presented
  // in the GUI
  CColorSpaceConvert
    *cscpNativeToGUI,
    *cscpGUIToNative;

  MTI_UINT16
    *uspSrc[MAX_FRAME],
    *uspDst;

  CPixelRegionList
    *prlpOriginalValues;

  bool CompareParameter (const PERSISTENT_SCRATCH_SETTING *pssp1,
		const PERSISTENT_SCRATCH_SETTING *pssp2);
  void CopyParameter (const PERSISTENT_SCRATCH_SETTING *psspSrc,
		PERSISTENT_SCRATCH_SETTING *psspDst);
  int AllocAlgorithm ();
  int SetupAlgorithm ();
  void CopyBeforeScratch ();
  void FindScratch ();
  void CopyAfterScratch ();
  void RunThroughWarp (bool bForHistory);
  void NormalizeTemplate ();
  void NormalizeImage (long lRow, long lCol);
  void Normalize (float *fp);
  void FilterData ();
  void TrackScratch ();
  void DynamicProgram (float *fpBestScore, long *lpBestCol);
  void FreeGeneral ();
  void FreeAlgorithm ();
  void RepairScratch ();
  void WarpSides ();
  void LeastSquares (double daXpX[2][2], double daXpY[2], float faB[2]);
  void ApplyAdjustment ();

 };

#endif
