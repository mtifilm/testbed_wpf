object ScratchHelpForm: TScratchHelpForm
  Left = 172
  Top = 54
  BorderStyle = bsToolWindow
  Caption = 'Scratch Help'
  ClientHeight = 664
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HelpTabControl: TTabControl
    Left = 0
    Top = 0
    Width = 408
    Height = 664
    Align = alClient
    TabOrder = 0
    Tabs.Strings = (
      'Operation'
      'Function Keys')
    TabIndex = 0
    OnChange = HelpTabControlChange
    object HTMLHelpPage: TCppWebBrowser
      Left = 4
      Top = 24
      Width = 400
      Height = 636
      Align = alClient
      TabOrder = 0
      ControlData = {
        4C00000057290000BC4100000000000000000000000000000000000000000000
        000000004C000000000000000000000001000000E0D057007335CF11AE690800
        2B2E126208000000000000004C0000000114020000000000C000000000000046
        8000000000000000000000000000000000000000000000000000000000000000
        00000000000000000100000000000000000000000000000000000000}
    end
  end
end
