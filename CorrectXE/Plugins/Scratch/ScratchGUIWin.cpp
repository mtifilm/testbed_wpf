//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ScratchGUIWin.h"

#include "ClipAPI.h"
#include "ColorSpaceConvert.h"
#include "Magnifier.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "PDLViewerUnit.h"
#include "SidecarUnit.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"

#include <sstream>


//---------------------------------------------------------------------------
#pragma package(smart_init)

//#pragma link "CSPIN"
#pragma link "MTIUNIT"
#pragma link "VTimeCodeEdit"
#pragma link "ExecButtonsFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"


#define BUTTON_WIDTH 20

#define PLUGIN_TAB_INDEX 1

TScratchForm *GScratchForm;

//---------------------------------------------------------------------------

bool HandleToolEventTrampoline(const FosterParentEventInfo &eventInfo)
{
    return GScratchForm->HandleToolEvent(eventInfo);
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
__fastcall TScratchForm::TScratchForm(TComponent* Owner)
        : TMTIForm(Owner)
{
   ScratchIniFileName = "$(CPMP_USER_DIR)Scratch.ini";

   if (GScratchTool != NULL)
   {
     SET_CBHOOK(ClipHasChanged, GScratchTool->ClipChange);
     SET_CBHOOK(MarksHaveChanged, GScratchTool->MarksChange);
     SET_CBHOOK(EnterKeyPressedOnTrackBar, BackgroundDetailTrackEdit->TrackEditEnterKeyPressed);
   }

   imageMagnifier = NULL;
   ClearGrayBitmap = NULL;
   ClearWhiteBitmap = NULL;

   scratchLimitFocusSide = SCRATCH_LIMIT_FOCUS_NONE;
   OkToPreviewAllOrRenderAll = true;
   deferredUpdateFlag = false;

   RestoreProperties();

   processingSingleFrameFlag = false;

   // Try to prevent graphs from flashing when updated
   RGraphImage->ControlStyle << csOpaque;
   GGraphImage->ControlStyle << csOpaque;
   BGraphImage->ControlStyle << csOpaque;
   YGraphImage->ControlStyle << csOpaque;
   MagnifierImage->ControlStyle << csOpaque;

   // These are not really kosher....
   SidecarWindow = new TSidecarWindow(
                    (TForm *)GScratchTool->getSystemAPI()->GetMainWindowHandle(),
                    &(this->ImageMouseDown));
   oldSidecarAnchor = TPoint(-1, -1);
   oldClientRect = (RECT){-1, -1, -1, -1};
}
//---------------------------------------------------------------------------

__fastcall TScratchForm::~TScratchForm(void)
{
   hideSidecar();

   if (GScratchTool != NULL && imageMagnifier != NULL)
      GScratchTool->getSystemAPI()->freeMagnifier(imageMagnifier);

   delete ClearGrayBitmap;
   delete ClearWhiteBitmap;
}
//---------------------------------------------------------------------------

bool TScratchForm::formCreate(void)
{
   GScratchForm->YesMoveButton->Checked  = true;     // QQQ Remember these
   GScratchForm->VerticalButton->Checked = true;     // QQQ Remember these

   // Larry says to make this the default
   //SetFormMode(fsStayOnTop);

   // home your majorState    <-- wtf?
   GScratchTool->setMajorState(MAJORSTATE_GUI_CREATED);

   GScratchTool->SetToolProgressMonitor(ExecStatusBar);

   return true;
}
//---------------------------------------------------------------------------

bool TScratchForm::formShow(void)
{
   // init magnifier again to make
   // sure that image format is set
   initMagnifier();

   ScratchControlPanel->Visible = true;

   // old FormShow stuff
   KeyPreview = true;

   // Clear out old drek
   imageMagnifierClear();

   // GUI sync timer
   UpdateTimer->Enabled = true;

   // These used to be in ShowScratchGUI
   if (GScratchTool == NULL) return false;

   // set the scratch tool state
   GScratchTool->setMajorState(MAJORSTATE_SHOWING_SCRATCH_TOOL);

   // the main window will look right now
   GScratchTool->getSystemAPI()->refreshFrame();

   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);

   return true;
}
//---------------------------------------------------------------------------

void TScratchForm::formHide(void)
{
   // GUI sync timer
   UpdateTimer->Enabled = false;

   ScratchControlPanel->Visible = false;

   if (GScratchTool != NULL)
   {
      // home the majorState     ??!?
      GScratchTool->setMajorState(MAJORSTATE_GUI_CREATED);

      // make sure nothing's on the screen    ??!?
      GScratchTool->getSystemAPI()->refreshFrame();
   }

   hideSidecar();
}
//---------------------------------------------------------------------------

void TScratchForm::checkAbilityToPreviewAllOrRenderAll(void)
{
   bool oldFlag = OkToPreviewAllOrRenderAll;
   OkToPreviewAllOrRenderAll = false;

   // Can't proceed at all if the scratch was never located!
   if ((imageMagnifier == NULL) || !imageMagnifier->isActive())
      return;

   if (!YesMoveButton->Checked)
   {
      // Scratch is not moving - doesn't matter which frame we're on
      OkToPreviewAllOrRenderAll = true;
   }
   else
   {
      // Scratch is moving, need to be on the IN frame
      int currentFrameIndex = GScratchTool->getSystemAPI()->getLastFrameIndex();
      int markInIndex = GScratchTool->getSystemAPI()->getMarkIn();
      int markOutIndex = GScratchTool->getSystemAPI()->getMarkOut();

      if (markInIndex < 0 || markOutIndex < 0 || markInIndex >= markOutIndex)
      {
         WarningMessage->Caption = "Marks are invalid !";
      }
      else
      {
         WarningMessage->Caption = "Not on Mark IN frame !";
         if (currentFrameIndex == markInIndex)
            OkToPreviewAllOrRenderAll = true;
      }
   }

   if (OkToPreviewAllOrRenderAll != oldFlag)
   {
      imageMagnifierRefresh(imageMagnifier);
   }
}
//---------------------------------------------------------------------------

void TScratchForm::UpdateExecutionButtons(
                             EToolControlState toolProcessingControlState,
                             bool processingRenderFlag)
{
   if (GScratchTool == NULL) return;

   switch(toolProcessingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button
         EnableOrDisableMostControls(true);

         SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PREVIEW);
         SetButtonCommand(RenderAllButton, TOOL_PROCESSING_CMD_RENDER);
         PreviewAllButton->Enabled = true;
         RenderAllButton->Enabled = true;

         StopButton->Enabled = false;

         SetResumeTCToDefault();
         break;

      case TOOL_CONTROL_STATE_RUNNING :
         // Previewing or Rendering is now running
         EnableOrDisableMostControls(false);

         if (processingSingleFrameFlag)
         {
            PreviewAllButton->Enabled = false;
            RenderAllButton->Enabled = false;
         }
         else
         {
            if (processingRenderFlag)
            {
               // Rendering
               PreviewAllButton->Enabled = false;
               SetButtonCommand(RenderAllButton, TOOL_PROCESSING_CMD_PAUSE);
               RenderAllButton->Enabled = true;
            }
            else
            {
               // Previewing
               RenderAllButton->Enabled = false;
               SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PAUSE);
               PreviewAllButton->Enabled = true;
            }

            // Larry wants the 'reticles' to go away while running
            GScratchTool->hideOverlays();
            disableShowReticlesCheckBox();
         }

         StopButton->Enabled = true;

         break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused
         EnableOrDisableMostControls(false);

         if (processingRenderFlag)
         {
            // Paused while Rendering
            PreviewAllButton->Enabled = false;
            SetButtonCommand(RenderAllButton, TOOL_PROCESSING_CMD_CONTINUE);
            RenderAllButton->Enabled = true;
         }
         else
         {
            // Paused while Previewing
            RenderAllButton->Enabled = false;
            SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_CONTINUE);
            PreviewAllButton->Enabled = true;
         }

         // Unhide the blue and red boxeswhile paused
         GScratchTool->showOverlays();
         enableShowReticlesCheckBox();

         // Now go load the current timecode
         SetStartTCButtonClick(NULL);

         StopButton->Enabled = true;

         break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
         // Previewing or rendering 1 frame, disable everything
         EnableOrDisableMostControls(false);
         PreviewAllButton->Enabled = false;
         RenderAllButton->Enabled = false;

         StopButton->Enabled = false;    // Can't stop single frame commands

         break;

      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         // In transition to Pause or Stop, disable everything
         EnableOrDisableMostControls(false);
         PreviewAllButton->Enabled = false;
         RenderAllButton->Enabled = false;

         StopButton->Enabled = false;   // Already stopping or pausing

         break;

      default:
         MTIassert(false);

         break;
      }

   UpdateExecutionButtonToolbar(toolProcessingControlState,
                                processingRenderFlag);

   CheckAbilityToPreviewAllOrRenderAll();
}
//---------------------------------------------------------------------------

void TScratchForm::GatherParameters(CScratchParameters &toolParams)
{
   // does the scratch move?
   toolParams.scratchMoves = YesMoveButton->Checked;

   // is the scratch superficial or deep?
   //if (SurfaceButton->Checked)
      toolParams.scratchType = SCRATCH_TYPE_SURFACE;
   //else if (DeepButton->Checked)
      //toolParams.scratchType = SCRATCH_TYPE_DEEP;

   // is the scratch horizontal?
   toolParams.scratchHorizontal = HorizontalButton->Checked;

   // get the surrounding rectangle
   toolParams.surroundingRectangle = getScratchRectangle();

   // frame where the template was derived
   toolParams.templateFrame = getScratchFrame();

   // get the limiting rectangle - for a stationary scratch
   // this will be identical to the surrounding rectangle
   toolParams.limitRectangle = getScratchLimitRectangle();

   // the magnifier window is the template for the scratch
   toolParams.templateWindow = getMagnifierRectangle();

   // set up the edge template coords from the limiting rect
   if (!toolParams.scratchHorizontal) {

      toolParams.edgeTemplate0 =
         toolParams.limitRectangle.left - toolParams.templateWindow.left;
      toolParams.edgeTemplate1 =
         toolParams.limitRectangle.right - toolParams.templateWindow.left;
   }
   else {

      toolParams.edgeTemplate0 =
         toolParams.limitRectangle.top - toolParams.templateWindow.top;
      toolParams.edgeTemplate1 =
         toolParams.limitRectangle.bottom - toolParams.templateWindow.top;
   }

   // now select the signal used to detect the scratch. values were
   // captured when the operator left the scratch limits panel
   MTI_UINT16 *signal;
   if (YDetectButton->Checked) {
      toolParams.scratchSelectSignal = SELECT_SIGNAL_Y;
      signal = getYVal();
   }
   if (RDetectButton->Checked) {
      toolParams.scratchSelectSignal = SELECT_SIGNAL_RED;
      signal = getRVal();
   }
   if (GDetectButton->Checked) {
      toolParams.scratchSelectSignal = SELECT_SIGNAL_GREEN;
      signal = getGVal();
   }
   if (BDetectButton->Checked) {
      toolParams.scratchSelectSignal = SELECT_SIGNAL_BLUE;
      signal = getBVal();
   }

   // set up the accurate float template
   for (int i=0;i<MAX_TEMPLATE;i++) {
      toolParams.faTemplate[i] = (double)signal[i]/(double)65536;
   }

   // set up the modify mask
   toolParams.scratchModifySignal = 0;
   if (RCheckBox->Checked)
      toolParams.scratchModifySignal |= MODIFY_SIGNAL_RED;
   if (GCheckBox->Checked)
      toolParams.scratchModifySignal |= MODIFY_SIGNAL_GREEN;
   if (BCheckBox->Checked)
      toolParams.scratchModifySignal |= MODIFY_SIGNAL_BLUE;

   // set up the detail modulus (0-100)
   //toolParams.scratchDetail = BackgroundToleranceTrackBar->Position;
   toolParams.scratchDetail = BackgroundDetailTrackEdit->GetValue();
}
//---------------------------------------------------------------------------

void TScratchForm::PreviewFrame(void)
{
   // Preview a single frame

   if (GScratchTool == NULL) return;

   if (!areAllSystemsGo(true))       // = 'single frame'
      return;

   doPreProcessing(true);            // = 'single frame'

   // Start Preview processing for the current frame.  This function will
   // only act if Previewing/Rendering is stopped or paused.
   GScratchTool->StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1,false);

   // Single frame ops are pseudo-synchronous, so it's done by the
   // time we get back here
   // doPostProcessing();   let ScratchTool call it
}
//---------------------------------------------------------------------------

void TScratchForm::RenderFrame(void)
{
   // Render a single frame

   if (GScratchTool == NULL) return;

   if (!areAllSystemsGo(true))        // = 'single frame'
      return;

   GScratchTool->setAutoAcceptNextRenderFlag();

   doPreProcessing(true);             // = 'single frame'

   // Start Render processing for the current frame.  This function will
   // only act if Previewing/Rendering is stopped or paused.
   GScratchTool->StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, false);

   // Single frame ops are pseudo-synchronous, so it's done by the
   // time we get back here
   // doPostProcessing();   let ScratchTool call it
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::PreviewFrameButtonClick(TObject *Sender)
{
   PreviewFrame();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::PreviewAllButtonClick(TObject *Sender)
{
   RenderPreviewProcessing(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::RenderFrameButtonClick(TObject *Sender)
{
   RenderFrame();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::RenderAllButtonClick(TObject *Sender)
{
   RenderPreviewProcessing(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::StopButtonClick(TObject *Sender)
{
   if (GScratchTool == NULL) return;

   GScratchTool->StopToolProcessing();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::PDLCaptureButtonClick(TObject *Sender)
{
   if (GScratchTool == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GScratchTool->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------


bool TScratchForm::WriteSettings(CIniFile *ini, const string &IniSection)
{
  if (ScratchIniFileName == "")
    return(false);

  CIniFile* iniAF = CreateIniFile(ScratchIniFileName);
  if (iniAF == NULL)
    {
      TRACE_0(errout << "SettingsProperties: Could not create "
              << ScratchIniFileName << endl
              << "Because " << theError.getMessage());
      return(false);
    }

  iniAF->WriteString("General", "Test", "Off");

  return DeleteIniFile(iniAF);

}
//---------------------------------------------------------------------------

bool TScratchForm::ReadSettings(CIniFile *ini, const string &IniSection)
{
  static string test;

  if (ScratchIniFileName == "")
    return(false);

  CIniFile* iniAF = CreateIniFile(ScratchIniFileName);
  if (iniAF == NULL)
    {
      TRACE_0(errout << "SettingsProperties: Could not create "
              << ScratchIniFileName << endl
              << "Because " << theError.getMessage());
      return(false);
    }

  test = iniAF->ReadString("General", "Test", "Unknown");

  return DeleteIniFile(iniAF);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::FocusControl(Controls::TWinControl* Control)
{
   if ((GScratchTool == NULL) || GScratchTool->IsDisabled() || !IsToolVisible())
     return;

   GScratchTool->getSystemAPI()->
      FocusAdoptedControl(reinterpret_cast<int *>(Control));
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::SetStartTCButtonClick(TObject *Sender)
{
   if (GScratchTool == NULL)
     return;

   // This get the current time code
   CVideoFrameList *frameList;
   frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
   if (frameList != NULL)
     ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(
                           GScratchTool->getSystemAPI()->getLastFrameIndex());
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::GotoTCButtonClick(TObject *Sender)
{
   ResumeTCEdit->VExit(0);
   if (GScratchTool == NULL) return;
   CVideoFrameList *frameList;
   frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // No! GScratchTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   int Frame = frameList->getFrameIndex(ResumeTCEdit->tcP);
   GScratchTool->getSystemAPI()->goToFrameSynchronous(Frame);
   ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(
                            GScratchTool->getSystemAPI()->getLastFrameIndex());
}
//---------------------------------------------------------------------------

void TScratchForm::SetResumeTCToDefault(void)
{
   CVideoFrameList *frameList;
   frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;
   int Frame = GScratchTool->getSystemAPI()->getMarkIn();
   if (Frame < 0) Frame = frameList->getInFrameIndex();
   ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(Frame);
}
//---------------------------------------------------------------------------

void TScratchForm::RenderPreviewProcessing(TObject *Sender)
{
   if (GScratchTool == NULL) return;

   if (!areAllSystemsGo(false))       // = 'NOT single frame'
      return;

   TToolButton *button = dynamic_cast<TToolButton*>(Sender);
   if (button == 0)
      return;

   // Get the button's tool processing command
   EToolProcessingCommand buttonCommand = GetButtonCommand(button);

   switch (buttonCommand)
   {
      case TOOL_PROCESSING_CMD_PREVIEW :
      case TOOL_PROCESSING_CMD_RENDER :
         doPreProcessing(false);     // not single frame
         GScratchTool->StartToolProcessing(buttonCommand,false);
      break;

      case TOOL_PROCESSING_CMD_PAUSE :
         GScratchTool->PauseToolProcessing();
      break;

      case TOOL_PROCESSING_CMD_CONTINUE :
      {
         CVideoFrameList *frameList;
         frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
         int resumeFrame;
         if (frameList == NULL)
            resumeFrame = GScratchTool->getSystemAPI()->getMarkIn();
         else
            resumeFrame = frameList->getFrameIndex(ResumeTCEdit->tcP);

         GScratchTool->ContinueToolProcessing(resumeFrame);
      }
      break;

      default:
         MTIassert(false);
      break;
   }

   // Multi-frame ops are run asynchronously, so it's not done yet
}
//---------------------------------------------------------------------------

EToolProcessingCommand TScratchForm::GetButtonCommand(TToolButton *button)
{
   // Get the current tool processing command from the button's User Data

   return (EToolProcessingCommand)(button->Tag);
}
//---------------------------------------------------------------------------

void TScratchForm::SetButtonCommand(TToolButton *button,
                                       EToolProcessingCommand newCmd)
{
   // Set the tool processing command in the button's Tag and set the
   // the matching text label and icon for the button

   // Save the tool processing command in the button's Tag
   button->Tag = newCmd;

   string buttonString;
   System::Uitypes::TImageIndex iconIndex;
   switch (newCmd)
      {
      case TOOL_PROCESSING_CMD_PREVIEW :
         buttonString = "Preview";
         iconIndex = 1;
         break;
      case TOOL_PROCESSING_CMD_RENDER :
         buttonString = "Render";
         iconIndex = 3;
         break;
      case TOOL_PROCESSING_CMD_PAUSE :
         buttonString = "Pause";
         iconIndex = 4;
         break;
      case TOOL_PROCESSING_CMD_CONTINUE :
         buttonString = "Continue";
         iconIndex = 5;
         break;
      default :
         buttonString = "ERROR!";  // Shouldn't ever happen
         iconIndex = 6; // Stop sign
         break;
      }

   button->Caption = buttonString.c_str();
   button->ImageIndex = iconIndex;
}
//---------------------------------------------------------------------------

void TScratchForm::ClipHasChanged(void *Sender)
{
   if (GScratchTool == NULL)
      return;

   SetResumeTCToDefault();

#if 0 // WTF??!?
   // THIS WAS IN A SEPARATE FUNCTION CALLED NewClip()
   // NOT SURE IF IT'S USEFUL!!!
   // if the tool is visible but not Panel 1,
   // bring the tool down & back up again
   if (IsToolVisible())
   {
      // hide the tool
      HideScratchGUI();

      // ...and bring it back up
      ShowScratchGUI();
   }
#endif // wtf

   deactivateMagnifier();
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}
//---------------------------------------------------------------------------

void TScratchForm::MarksHaveChanged(void *Sender)
{
   if (GScratchTool == NULL)
      return;

   EToolControlState toolControlState = GScratchTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      UpdateExecutionButtonToolbar(toolControlState, false);
   }
}
//---------------------------------------------------------------------------

void TScratchForm::EnterKeyPressedOnTrackBar(void *Sender)
{
   if (GScratchTool == NULL)
      return;

   focusOnScratchWindow();
}
//---------------------------------------------------------------------------

void TScratchForm::EnableOrDisableMostControls(bool flag)
{
   if (GScratchTool == NULL)
      return;

     PreviewFrameButton->Enabled = flag;
     RenderFrameButton->Enabled = flag;

     // Move focus to the default place
     focusOnScratchWindow();

     // Disable 'Orientation' and 'Movement'
     TabCoverUpPanel->Enabled = flag;

     // Disable 'Detect' and 'Process'
     ChannelControlPanel->Enabled = flag;

     // Disable 'Background detail' slider
     //BackgroundDetailPanel->Enabled = flag;
     BackgroundDetailTrackEdit->Enabled = flag;

     // Disable 'Edge blend'
     EdgeBlurPanel->Enabled = flag;

     // Disable the 'Resume' controls
     ResumeGroupBox->Enabled = flag;

     // Disable the 'PDL' capture button
     PDLCaptureButton->Enabled = flag;

    // Show we are processing the data
    if (flag)
      Screen->Cursor = crDefault;
    else
      Screen->Cursor = crAppStart;

    // Turn off 'not on mark in' warning
    MarkInWarningPanel->Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::BackgroundToleranceTrackBarChange(
      TObject *Sender)
{
  //char bgnd[4];
  //sprintf(bgnd,"%3d",BackgroundToleranceTrackBar->Position);
  //BackgroundToleranceValueLabel->Caption = bgnd;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ImageMouseDown(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y)
{
   const int LEFT_MOUSE_BUTTON(0);
   const int RIGHT_MOUSE_BUTTON(1);

   if (GScratchTool == NULL)
   {
      return;
   }

   switch (Button)
   {
      case mbLeft:
         GScratchTool->onLeftButton(X / MAGF);
         focusOnScratchWindow();
         break;

      case mbRight:
         GScratchTool->onRightButton(X / MAGF);
         focusOnScratchWindow();
         break;

      default:
         // ignore eeverything else
         break;
   }

   // If sidecar got clicked on, it got the foecus and we don't want that!!!
   if (SidecarWindow->Active)
   {
      GScratchTool->getSystemAPI()->setMainWindowFocus();
   }

   NotSetYetPanel->Visible = false;

   // Execution button state may change
   EToolControlState toolControlState = GScratchTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      UpdateExecutionButtonToolbar(toolControlState, false);
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::HorizontalButtonClick(TObject *Sender)
{
   if (GScratchTool != NULL)
      GScratchTool->acceptFix();

   if (HorizontalButton->Checked)
   {
      // clear the scratch rectangle and magnifier
      GScratchTool->clearScratchRectangle();
      deactivateMagnifier();
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::VerticalButtonClick(TObject *Sender)
{
   if (GScratchTool != NULL)
      GScratchTool->acceptFix();

   if (VerticalButton->Checked)
   {
      // clear the scratch rectangle and magnifier
      GScratchTool->clearScratchRectangle();
      deactivateMagnifier();
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::LightDensityButtonClick(TObject *Sender)
{
   if (GScratchTool != NULL)
      GScratchTool->acceptFix();

   if (LightDensityButton->Checked)
   {
      DetectButtonClick(Sender);
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DarkDensityButtonClick(TObject *Sender)
{
   if (GScratchTool != NULL)
      GScratchTool->acceptFix();

   if (DarkDensityButton->Checked)
   {
      DetectButtonClick(Sender);
   }
}
//---------------------------------------------------------------------------

void TScratchForm::initMagnifier(void)
{
   // if a magnifier is allocated, free it
   if (imageMagnifier != NULL)
      GScratchTool->getSystemAPI()->freeMagnifier(imageMagnifier);

   // allocate an active magnifier which will draw the
   // magnifier window in the upper left of the frame

   initMagWin.left   =  0;
   initMagWin.top    =  0;
   initMagWin.right  = MagnifierImage->Width / MAGF - 1;
   initMagWin.bottom = MagnifierImage->Height/ MAGF - 1;

   imageMagnifier = GScratchTool->getSystemAPI()->
         allocateMagnifier(MagnifierImage->Width, MagnifierImage->Height,
                           &initMagWin);
}
//---------------------------------------------------------------------------

void TScratchForm::activateMagnifier(void)
{
   if ((imageMagnifier != NULL) && (!imageMagnifier->isActive()))
   {
      imageMagnifier->
        activateMagnifier(TScratchForm::imageMagnifierRefreshCallback,this);

      if (MirrorGraphsCheckBox->Checked)
      {
         showSidecar();
      }

      // AFTER showSidecar()!
      moveSidecar(blueRectScreenCoords);
   }

   // Show the red box
   if (GScratchTool != NULL)
      GScratchTool->showOverlays();

   // No idea what this is for!
   if (!ShowReticlesCheckBox->Checked)
      enableShowReticlesCheckBox();
}
//---------------------------------------------------------------------------

void TScratchForm::deactivateMagnifier(void)
{
   if (GScratchTool == NULL) return;

   if ((imageMagnifier != NULL) && (imageMagnifier->isActive()))
   {
      imageMagnifier->deactivateMagnifier();
      hideSidecar();
   }

   // Hide the red box
   GScratchTool->hideOverlays();

   // No idea what this is for!
   if (!ShowReticlesCheckBox->Checked)
      enableShowReticlesCheckBox();

   // hack until i fix the offsets - need to expose base window
   //ChannelControlPanel->Width = ClientWidth;

   // No scratch width available
   NotSetYetPanel->Visible = true;

   // No warning needed!
   MarkInWarningPanel->Visible = false;

   // Clear the scratch window images
   imageMagnifierClear();

   // Execution button state may change
   EToolControlState toolControlState = GScratchTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
      UpdateExecutionButtonToolbar(toolControlState, false);
}
//---------------------------------------------------------------------------

void TScratchForm::showMagnifier(void)
{
   if (imageMagnifier != NULL)
      imageMagnifier->show();
}
//---------------------------------------------------------------------------

void TScratchForm::hideMagnifier(void)
{
   if (imageMagnifier != NULL)
      imageMagnifier->hide();
}
//---------------------------------------------------------------------------

// the callback for refreshing the magnified image (MagnifierImage)
void TScratchForm::imageMagnifierRefreshCallback(void *obj, void *mag)
{
   if (GScratchForm == NULL)
      return;

   GScratchForm->imageMagnifierRefresh(mag);

   // QQQ FIND A BETTER PLACE FOR THIS!
   GScratchForm->setScratchFrame(GScratchTool->getSystemAPI()->getLastFrameIndex());
}
//---------------------------------------------------------------------------

void TScratchForm::imageMagnifierClear()
{
      clearImage(RGraphImage);
      clearImage(GGraphImage);
      clearImage(BGraphImage);
      clearImage(YGraphImage);
      clearImage(MagnifierImage);
      clearImage(SidecarWindow->RGraphImage);
      clearImage(SidecarWindow->GGraphImage);
      clearImage(SidecarWindow->BGraphImage);
      clearImage(SidecarWindow->YGraphImage);
      clearImage(SidecarWindow->MagnifierImage);
}
//---------------------------------------------------------------------------

void TScratchForm::imageMagnifierRefresh(void *mag)
{
   if (GScratchTool == NULL) return;

   if ((imageMagnifier == NULL) || (!IsToolVisible()))
      return;

   if (!GScratchTool->isScratchRectangleAvailable())
   {
      // No scratch selected - clear out old drek
      imageMagnifierClear();
      return;
   }

   int left, bottom, height;
   int i, j;

   CMagnifier *m = static_cast<CMagnifier *>(mag);
   magRect = m->getMagnifierWindow();

   // average the three components columnwise

   CColorSpaceConvert *clrcvt = GScratchTool->getToRGBConverter();

   MTI_UINT16 *xptr = m->getIntermediateFrameBuffer();

   // the pitch of the bitmap in MTI_UINT16's
   int pitch = (initMagWin.right + 1)*3;

   // the depth of the bitmap (number of rows)
   int depth = initMagWin.bottom + 1;

   // pick up the max and min of each component
   MTI_UINT16 ymax = 0;
   MTI_UINT16 ymin = 65535;
   MTI_UINT16 rmax = 0;
   MTI_UINT16 rmin = 65535;
   MTI_UINT16 gmax = 0;
   MTI_UINT16 gmin = 65535;
   MTI_UINT16 bmax = 0;
   MTI_UINT16 bmin = 65535;

   for (i = 0; i <= initMagWin.right; i++, xptr+=3)
   {
      int y = 0;
      int u = 0;
      int v = 0;

      MTI_UINT16 *xyptr = xptr;
      for (j = 0; j <= initMagWin.bottom; j++, xyptr+=pitch)
      {
         y += xyptr[0];
         u += xyptr[1];
         v += xyptr[2];
      }

      clrcvt->ConvertOneValue((MTI_UINT16)(y/depth),
                              (MTI_UINT16)(u/depth),
                              (MTI_UINT16)(v/depth),
                              &iRVal[i],
                              &iGVal[i],
                              &iBVal[i]);

      iYVal[i] = (((int)iRVal[i] + (int)iGVal[i] + (int)iBVal[i])/3);

      if (iYVal[i] > ymax)
         ymax = iYVal[i];
      if (iYVal[i] < ymin)
         ymin = iYVal[i];

      if (iRVal[i] > rmax)
         rmax = iRVal[i];
      if (iRVal[i] < rmin)
         rmin = iRVal[i];

      if (iGVal[i] > gmax)
         gmax = iGVal[i];
      if (iGVal[i] < gmin)
         gmin = iGVal[i];

      if (iBVal[i] > bmax)
         bmax = iBVal[i];
      if (iBVal[i] < bmin)
         bmin = iBVal[i];
   }

   if (ymax==ymin) {
      ymax = 65535;
      ymin = 0;
   }
   if (rmax==rmin) {
      rmax = 65535;
      rmin = 0;
   }
   if (gmax==gmin) {
      gmax = 65535;
      gmin = 0;
   }
   if (bmax==bmin) {
      bmax = 65535;
      bmin = 0;
   }

   // bracahack - normalize r, g and b as a group
   if (rmin > gmin) rmin = gmin; else gmin = rmin;
   if (rmin > bmin) rmin = bmin; else bmin = rmin;
   if (rmin > ymin) rmin = ymin; else ymin = rmin;
   if (gmin > bmin) gmin = bmin; else bmin = gmin;
   if (gmin > ymin) gmin = ymin; else ymin = gmin;
   if (bmin > ymin) bmin = ymin; else ymin = bmin;
   if (rmax < gmax) rmax = gmax; else gmax = rmax;
   if (rmax < bmax) rmax = bmax; else bmax = rmax;
   if (rmax < ymax) rmax = ymax; else ymax = rmax;
   if (gmax < bmax) gmax = bmax; else bmax = gmax;
   if (gmax < ymax) gmax = ymax; else ymax = gmax;
   if (bmax < ymax) bmax = ymax; else ymax = bmax;

   double yrange = ymax - ymin;
   double rrange = rmax - rmin;
   double grange = gmax - gmin;
   double brange = bmax - bmin;

   // Draw the four graphs (R, G, B and Y)
   drawGraph(RGraphImage, iRVal, rmin, rrange, RDetectButton->Checked);
   drawGraph(GGraphImage, iGVal, gmin, grange, GDetectButton->Checked);
   drawGraph(BGraphImage, iBVal, bmin, brange, BDetectButton->Checked);
   drawGraph(YGraphImage, iYVal, ymin, yrange, YDetectButton->Checked);
   drawGraph(SidecarWindow->RGraphImage, iRVal, rmin, rrange, RDetectButton->Checked);
   drawGraph(SidecarWindow->GGraphImage, iGVal, gmin, grange, GDetectButton->Checked);
   drawGraph(SidecarWindow->BGraphImage, iBVal, bmin, brange, BDetectButton->Checked);
   drawGraph(SidecarWindow->YGraphImage, iYVal, ymin, yrange, YDetectButton->Checked);

   // now the magnified image
   //BitBlt(screenDC,
         // MagnifierImage->Left,
         // MagnifierImage->Top,
         // MagnifierImage->Width,
         // MagnifierImage->Height,
         // m->getMagnifierDeviceContext(),
         // 0,
         // 0,
         // SRCCOPY);

   TRGBTriple q;
   Graphics::TBitmap *DataBitmap = new Graphics::TBitmap;

   DataBitmap->PixelFormat = pf24bit;
   DataBitmap->Height = MagnifierImage->Height;
   DataBitmap->Width = MagnifierImage->Width;

   MTI_UINT16 *srcPixels = m->getIntermediateFrameBuffer();
   const int numberOfComponentsShouldNOTBeHardwired = 3;
   int srcWidth = MagnifierImage->Width / MAGF;
   int srcHeight = MagnifierImage->Height / MAGF;
   int srcPitch = srcWidth * numberOfComponentsShouldNOTBeHardwired;

   // Ensure reasonable dynamic luminance range
	int shift;
	int yRange = ymax - ymin;
   for (shift = 0; shift < 8; ++ shift)
   {
		if (((yRange >> shift) & 0xFF00) == 0)
		{
			break;
		}
	}

   // Replicate the source image to MAGF x MAGF rectangles in the destination
   for (int srcRow = 0; srcRow < srcHeight; ++srcRow)
   {
      MTI_UINT16 *srcPtr = srcPixels + (srcRow * srcPitch);
      TRGBTriple *dstPtr;
      for (int srcCol = 0; srcCol < srcWidth; ++srcCol)
      {
         MTI_UINT16 r, g, b;

         // What the hell is this converting? Didn't the Magnifier already
         // do the ->RGB conversion? QQQ
         clrcvt->ConvertOneValue(srcPtr[0], srcPtr[1], srcPtr[2],
                                 &r, &g, &b);

         // Clamped scaling - don't know why Convert is giving me values
         // larger than the maximum, also don't allow 0,0,0 - that's transparent!
			r = (r < rmin) ? 0 : ((r - rmin) >> shift);
			g = (g < rmin) ? 0 : ((g - gmin) >> shift);
			b = (b < rmin) ? 0 : ((b - bmin) >> shift);

			q.rgbtRed   = (r > 0xFF)? 0xFF : ((r == 0) ? 1 : r);
			q.rgbtGreen = (g > 0xFF)? 0xFF : ((g == 0) ? 1 : g);
			q.rgbtBlue  = (b > 0xFF)? 0xFF : ((b == 0) ? 1 : b);

         for (int dstRow = (srcRow*MAGF); dstRow < ((srcRow+1)*MAGF); ++dstRow)
			{
            dstPtr = (TRGBTriple *)DataBitmap->ScanLine[dstRow];
            for (int dstCol = (srcCol*MAGF); dstCol < ((srcCol+1)*MAGF); ++dstCol)
            {
               dstPtr[dstCol] = q;
            }
         }
         srcPtr += numberOfComponentsShouldNOTBeHardwired;
      }
   }
   TCanvas *canvas1 = MagnifierImage->Canvas;
   TCanvas *canvas2 = SidecarWindow->MagnifierImage->Canvas;
   canvas1->Draw(0,0,DataBitmap);
   canvas2->Draw(0,0,DataBitmap);
   delete DataBitmap;

   // now display the refinement limits in red (or green)
   int redLeft, redRight;
   if (IsScratchVertical())
   {
      redLeft  = MAGF * (limRect.left      - magRect.left);
      redRight = MAGF * (limRect.right + 1 - magRect.left);
   }
   else // Horizontal
   {
      redLeft  = MAGF * (limRect.top        - magRect.top);
      redRight = MAGF * (limRect.bottom + 1 - magRect.top);
   }

   // now display the edges of the scratch limit rectangle
   drawScratchLimitLine(redLeft);
   drawScratchLimitLine(redRight);

   // now display the surrounding rectangle in the oscillographs (blue)
   int blueLeft, blueRight;
   if (IsScratchVertical())
   {
      blueLeft  = MAGF * (scrRect.left      - magRect.left);
      blueRight = MAGF * (scrRect.right + 1 - magRect.left);
   }
   else // Horizontal
   {
      blueLeft  = MAGF * (scrRect.top        - magRect.top);
      blueRight = MAGF * (scrRect.bottom + 1 - magRect.top);
   }
   drawBlueScratchSearchLimitLine(blueLeft);
   drawBlueScratchSearchLimitLine(blueRight);

   // Force the drawing to be seen immedioately
   RGraphImage->Update();
   GGraphImage->Update();
   BGraphImage->Update();
   YGraphImage->Update();
   MagnifierImage->Update();
   SidecarWindow->RGraphImage->Update();
   SidecarWindow->GGraphImage->Update();
   SidecarWindow->BGraphImage->Update();
   SidecarWindow->YGraphImage->Update();
   SidecarWindow->MagnifierImage->Update();

   // Set state of 'not on mark in' warning to match the color of limit lines
   // Man this is bogus, but I'm in a hurry
   if (PreviewAllButton->Enabled && RenderAllButton->Enabled)
   {
      MarkInWarningPanel->Visible = !OkToPreviewAllOrRenderAll;
   }
}
//---------------------------------------------------------------------------

CMagnifier * TScratchForm::getImageMagnifier(void)
{
   return(imageMagnifier);
}
//---------------------------------------------------------------------------

void TScratchForm::clearImage(TImage *image, bool hilite)
{
   Graphics::TBitmap *DataBitmap = NULL;

   if (hilite && (ClearWhiteBitmap != NULL))
      DataBitmap = ClearWhiteBitmap;
   else if ((!hilite) && (ClearGrayBitmap != NULL))
      DataBitmap = ClearGrayBitmap;

   TCanvas *canvas = image->Canvas;

   if (DataBitmap == NULL)
   {
      TRGBTriple q;
      DataBitmap = new Graphics::TBitmap;

      DataBitmap->PixelFormat = pf24bit;
      DataBitmap->Height = image->Height;
      DataBitmap->Width = image->Width;

      for (int r=0; r < DataBitmap->Height; r++) {
          TRGBTriple *ptr = (TRGBTriple *)DataBitmap->ScanLine[r];
          for (int c = 0; c < DataBitmap->Width; c++) {
              unsigned int pixel = (hilite? 0x00A0A0A0 : 0x00808080);
              q.rgbtRed   = (pixel >> 16) & 0xFF;
              q.rgbtGreen = (pixel >> 8) & 0xFF;
              q.rgbtBlue  = pixel & 0xFF;
              ptr[c] = q;
          }
      }
      if (hilite)
         ClearWhiteBitmap = DataBitmap;
      else
         ClearGrayBitmap = DataBitmap;
   }
   canvas->Draw(0,0,DataBitmap);
}

void TScratchForm::drawGraph(TImage *image, MTI_UINT16 *values,
                             MTI_UINT16 minValue, MTI_UINT16 range,
                             bool hilite)
{
   TCanvas *canvas = image->Canvas;
   TColor darkColor = TColor(0x00101010);
   TColor lightColor = TColor(0x00F0F0F0);
   canvas->Pen->Color = IsDensityDark()? darkColor : lightColor;
   canvas->Pen->Style = psSolid;

   clearImage(image, hilite);

   int height = image->Height - 4;
   int bottom = height + 1;
   canvas->MoveTo(0, bottom -
      height * ((double)(values[0] - minValue)) / range);

   for (int i = 0; i <= (image->Width / MAGF); ++i)
   {
      int x = i * MAGF;
      int y = bottom - height * (double)(values[i] - minValue) / range;
      canvas->LineTo(x, y);
      canvas->LineTo(x + MAGF, y);
   }
}

void TScratchForm::drawScratchLimitLineOnOneImage(TImage *image,
                                                  int offset,
                                                  TColor color,
                                                  TPenStyle style)
{
   if (offset >= 0 && offset < image->Width)
   {
      TCanvas *canvas = image->Canvas;
      canvas->Pen->Color = color;
      canvas->Pen->Style = style;
      canvas->MoveTo(offset, 0);
      canvas->LineTo(offset, image->Height);
   }
}

void TScratchForm::drawScratchLimitLine(int offset)
{
   // now display the edges of the scratch limit rectangle
   TColor color1 = IsDensityDark() ? TColor(0x00D0D0D0) : TColor(0x00303030);
   TColor color2 = OkToPreviewAllOrRenderAll ? clGreen : clRed;
   TPenStyle style = (PreviewAllButton->Enabled && RenderAllButton->Enabled) ? psSolid : psDot;
   drawScratchLimitLineOnOneImage(YGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(RGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(GGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(BGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(MagnifierImage, offset, color2, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->YGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(SidecarWindow->RGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(SidecarWindow->GGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(SidecarWindow->BGraphImage, offset, color1, style);
   drawScratchLimitLineOnOneImage(SidecarWindow->MagnifierImage, offset, color2, psSolid);
}

void TScratchForm::drawBlueScratchSearchLimitLine(int offset)
{
   drawScratchLimitLineOnOneImage(YGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(RGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(GGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(BGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(MagnifierImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->YGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->RGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->GGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->BGraphImage, offset, clBlue, psSolid);
   drawScratchLimitLineOnOneImage(SidecarWindow->MagnifierImage, offset, clBlue, psSolid);
}
//---------------------------------------------------------------------------

//void TScratchForm::setScratchMoves(bool mov)
//{
   //scratchMoves = mov;
//}
//---------------------------------------------------------------------------

//void TScratchForm::setScratchHorz(bool hor)
//{
   //scratchHorz = hor;
//}
//---------------------------------------------------------------------------

void TScratchForm::setScratchRectangle(const RECT &newScratchRect)
{
   scrRect = newScratchRect;
}
//---------------------------------------------------------------------------

void TScratchForm::setScratchFrame(int frame)
{
   limFrame = frame;
}
//---------------------------------------------------------------------------

void TScratchForm::captureScratchSignals(void)
{
   for (int i=0;i<MAX_TEMPLATE;i++) {

      iYCaptVal[i] = iYVal[i];
      iRCaptVal[i] = iRVal[i];
      iGCaptVal[i] = iGVal[i];
      iBCaptVal[i] = iBVal[i];
   }
}
//---------------------------------------------------------------------------

void TScratchForm::setScratchLimitFocusSide(EScratchLimitFocus focusSide)
{
   scratchLimitFocusSide = focusSide;
}
//---------------------------------------------------------------------------

void TScratchForm::setScratchLimitRectangle(const RECT &newLimitRect)
{
   if (GScratchTool == NULL) return;

   limRect = newLimitRect;

   int scratchWidth;

   if (IsScratchVertical())
      scratchWidth = limRect.right - limRect.left + 1;
   else // Horizontal
      scratchWidth = limRect.bottom - limRect.top + 1;

   MTIostringstream os;
   os << scratchWidth;
   ScratchWidthValueLabel->Caption = AnsiString(os.str().c_str());

   NotSetYetPanel->Visible = false;

   // Execution button state may change
   EToolControlState toolControlState = GScratchTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
      UpdateExecutionButtonToolbar(toolControlState, false);

}
//---------------------------------------------------------------------------

RECT TScratchForm::getScratchRectangle()
{
   return(scrRect);
}
//---------------------------------------------------------------------------

int TScratchForm::getScratchFrame(void)
{
   return(limFrame);
}
//---------------------------------------------------------------------------

RECT TScratchForm::getScratchLimitRectangle(void)
{
   return(limRect);
}
//---------------------------------------------------------------------------

RECT TScratchForm::getMagnifierRectangle(void)
{
   return(magRect);
}
//---------------------------------------------------------------------------

MTI_UINT16 *TScratchForm::getYVal(void)
{
   return(iYCaptVal);
}
//---------------------------------------------------------------------------

MTI_UINT16 *TScratchForm::getRVal(void)
{
   return(iRCaptVal);
}
//---------------------------------------------------------------------------

MTI_UINT16 *TScratchForm::getGVal(void)
{
   return(iGCaptVal);
}
//---------------------------------------------------------------------------

MTI_UINT16 *TScratchForm::getBVal(void)
{
   return(iBCaptVal);
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnScratchWindow(void)
{
   // ScratchWindowFocusPanel is an invisible panel (actually Visible,
   // but you can't see it because it has no borders) that we give focus
   // to after the useer clicks on the scratch windows to set the scratch
   // limits. This lets us pretend that the scratch windows have focus
   // and we make the scratch window label bold to reflect that.
   if (!GScratchTool->IsDisabled() && ScratchWindowFocusPanel->Showing)
   {
      FocusControl(ScratchWindowFocusPanel);
   }
   else
   {
      GScratchTool->getSystemAPI()->setMainWindowFocus();
   }
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnOrientationControls(void)
{
   if (HorizontalButton->Checked)
      FocusControl(HorizontalButton);
   else
      FocusControl(VerticalButton);
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnMovementControls(void)
{
   if (YesMoveButton->Checked)
      FocusControl(YesMoveButton);
   else
      FocusControl(NoMoveButton);
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnDetectControls(void)
{
   if (RDetectButton->Checked)
      FocusControl(RDetectButton);
   else if (GDetectButton->Checked)
      FocusControl(GDetectButton);
   else if (BDetectButton->Checked)
      FocusControl(BDetectButton);
   else
      FocusControl(YDetectButton);
}
//---------------------------------------------------------------------------
void TScratchForm::toggleDensity(void)
{
   if (DarkDensityButton->Checked)
      LightDensityButton->Checked = true;
   else
      DarkDensityButton->Checked = true;
}
//---------------------------------------------------------------------------
void TScratchForm::toggleOrientation(void)
{
   if (HorizontalButton->Checked)
      VerticalButton->Checked = true;
   else
      HorizontalButton->Checked = true;
}
//---------------------------------------------------------------------------

void TScratchForm::toggleMovement(void)
{
   if (YesMoveButton->Checked)
      NoMoveButton->Checked = true;
   else
      YesMoveButton->Checked = true;
}
//---------------------------------------------------------------------------

void TScratchForm::cycleDownThroughDetectControls(void)
{
   if (RDetectButton->Checked)
      GDetectButton->Checked = true;
   else if (GDetectButton->Checked)
      BDetectButton->Checked = true;
   else if (BDetectButton->Checked)
      YDetectButton->Checked = true;
   else
      RDetectButton->Checked = true;
}
//---------------------------------------------------------------------------

void TScratchForm::cycleUpThroughDetectControls(void)
{
   if (RDetectButton->Checked)
      YDetectButton->Checked = true;
   else if (GDetectButton->Checked)
      RDetectButton->Checked = true;
   else if (BDetectButton->Checked)
      GDetectButton->Checked = true;
   else
      BDetectButton->Checked = true;
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnBackgroundDetailControl(void)
{
   //FocusControl(BackgroundToleranceTrackBar);
   FocusControl(BackgroundDetailTrackEdit->TrackBar);
}
//---------------------------------------------------------------------------

void TScratchForm::focusOnProcessControls(void)
{
   if (RCheckBox->Checked)
      FocusControl(RCheckBox);
   else if (GCheckBox->Checked)
      FocusControl(GCheckBox);
   else
      FocusControl(BCheckBox);
}
//---------------------------------------------------------------------------

MTI_UINT16 *TScratchForm::getPtrToMagValuesForDetectSignal()
{
   if (RDetectButton->Checked)
      return iRVal;
   else if (GDetectButton->Checked)
      return iGVal;
   else if (BDetectButton->Checked)
      return iBVal;
   else
      return iYVal;
}
//---------------------------------------------------------------------------

// hook to keep left,rgt arrow keys to get through to 'OnKeyDown'
// CALLED FROM MESSAGE HANDLER

void __fastcall TScratchForm::CMDialogKey(TMessage &Msg)
{
    switch(Msg.WParam)
    {
        case MTK_LEFT:
        case MTK_UP:
        case MTK_RIGHT:
        case MTK_DOWN:
        {
            Msg.Result = false;
            break;
        }
        default: TMTIForm::Dispatch(&Msg);
    }
}
//---------------------------------------------------------------------------

void ShowSidecar()
{
   if (GScratchForm->MirrorGraphsCheckBox->Checked)
   {
      GScratchForm->showSidecar();
   }
}
//---------------------------------------------------------------------------

void HideSidecar()
{
   GScratchForm->hideSidecar();
}
//---------------------------------------------------------------------------

void ToggleMirrorGraphsCheckBox()
{
   GScratchForm->toggleMirrorGraphsCheckBox();
}
//---------------------------------------------------------------------------

void TScratchForm::showSidecar()
{
   // Stupid hack timer to remove the sidecar when the main window is moved,
   // because I don't feel like tracking the main window's movements
   oldClientRect = GScratchTool->getSystemAPI()->GetClientRectScreenCoords();
   SidecarTimer->Enabled = true;

   SidecarWindow->Show();
   GScratchTool->getSystemAPI()->setMainWindowFocus();
}
//---------------------------------------------------------------------------

void TScratchForm::hideSidecar()
{
   SidecarWindow->Hide();
}
//---------------------------------------------------------------------------

void TScratchForm::toggleMirrorGraphsCheckBox()
{
   MirrorGraphsCheckBox->Checked = !MirrorGraphsCheckBox->Checked;
}
//---------------------------------------------------------------------------

void TScratchForm::moveSidecar(const RECT &rectangle)
{
   int x = (rectangle.left + rectangle.right) / 2;
   int y = (rectangle.top + rectangle.bottom) / 2;
   TPoint sidecarAnchor(x, y);

   if (sidecarAnchor.x != oldSidecarAnchor.x || sidecarAnchor.y != oldSidecarAnchor.y)
   {
      int sidecarWidth = SidecarWindow->Width;
      int sidecarHeight = SidecarWindow->Height;
      oldSidecarAnchor.x = sidecarAnchor.x;
      oldSidecarAnchor.y = sidecarAnchor.y;
      int xOffset = ((rectangle.right - rectangle.left) / 2) + 24;

      RECT clientRect = GScratchTool->getSystemAPI()->GetClientRectScreenCoords();
      int smallestLeft = clientRect.left;
      int smallestTop = clientRect.top + 4;
      int biggestLeft = clientRect.right - sidecarWidth;
      int biggestTop = clientRect.bottom - (sidecarHeight + 4);

      // Normally we show the sidecar window to the right of the
      // blue limits rectangle, but we flip it to the left side if
      // the scratch is close to the right edge.
      int newLeft = sidecarAnchor.x + xOffset;
      if (newLeft > biggestLeft)
      {
         // Flip it if it will fit on the left side.
         int temp = sidecarAnchor.x - (xOffset + sidecarWidth);
         if (temp >= smallestLeft)
         {
            newLeft = temp;
         }
      }

      // Align center of window with the center of the blue rectangle.
      int newTop = sidecarAnchor.y - (sidecarHeight / 2);
      if (newTop < smallestTop)
         newTop = smallestTop;
      if (newTop > biggestTop)
         newTop = biggestTop;

      SidecarWindow->Left = newLeft;
      SidecarWindow->Top = newTop;
   }
}
//---------------------------------------------------------------------------

extern void PreviewFrameButtonClick(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->PreviewFrameButton->Click();
}
//---------------------------------------------------------------------------

extern void PreviewAllButtonClick(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->PreviewAllButton->Click();
}
//---------------------------------------------------------------------------

extern void RenderFrameButtonClick(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->RenderFrameButton->Click();
}
//---------------------------------------------------------------------------

extern void RenderAllButtonClick(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->RenderAllButton->Click();
}
//---------------------------------------------------------------------------

extern void PDLCaptureButtonClick(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->PDLCaptureButton->Click();
}
//---------------------------------------------------------------------------

void CreateScratchGUI(void)
{
   if (GScratchForm != NULL) return;

   GScratchForm = new TScratchForm(Application);

   GScratchForm->RestoreProperties();
   GScratchForm->formCreate();

   // Reparent the controls to the UniTool
   extern const char PluginName;
   GScratchTool->getSystemAPI()->AdoptPluginToolGUI("Scratch", PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>( GScratchForm->ScratchControlPanel ),
         &HandleToolEventTrampoline);
}
//---------------------------------------------------------------------------

void DestroyScratchGUI(void)
{
   if (GScratchForm != NULL)
   {
      // Reparent the controls back to us before destroying them
      if (GScratchTool != NULL)
         GScratchTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
      GScratchForm->ScratchControlPanel->Parent = GScratchForm;

      // May be causing crashes on exit...
      // let's just forget about cleaning up because at this point
      // we're just exiting anyhow...

      //GScratchForm->Release();

      // Oops, still crashing - try this:

      delete GScratchForm;

      GScratchForm = NULL;
   }
}
//---------------------------------------------------------------------------

bool ShowScratchGUI(void)
{
   CreateScratchGUI();            // Create the gui if necessary

   if (GScratchForm == NULL || GScratchTool == NULL)
      return false;

   GScratchForm->ScratchControlPanel->Visible = true;
   GScratchForm->formShow();

   return true;
}
//---------------------------------------------------------------------------

bool  HideScratchGUI(void)
{
   if (GScratchForm == NULL) return true;

   GScratchForm->ScratchControlPanel->Visible = false;
   GScratchForm->formHide();

   return false;
}
//---------------------------------------------------------------------------

bool AreAllSystemsGo(bool singleFrameFlag)
{
   if (GScratchForm == NULL) return false;

   return GScratchForm->areAllSystemsGo(singleFrameFlag);
}
//---------------------------------------------------------------------------

bool TScratchForm::areAllSystemsGo(bool singleFrameFlag)
{
   if (GScratchTool == NULL) return false;

   // if scratch rectangle is not defined issue dialog box
   if (!GScratchTool->isScratchRectangleAvailable()) {
      const string ScRectErrMsg = "You must stretch a rectangle defining the scratch";
      _MTIInformationDialog(ScRectErrMsg);
      return false;
   }

   if (!singleFrameFlag)
   {
      // if IN and OUT pts on timeline not defined issue dialog box
      int inPoint = GScratchTool->getSystemAPI()->getMarkIn();
      int outPoint = GScratchTool->getSystemAPI()->getMarkOut();
      if ((inPoint == -1) || (outPoint == -1) || (inPoint >= outPoint))
      {
         const string ScLimsErrMsg = "You must define the IN and OUT points and   \n"
                                     "IN point must be earlier than OUT point";
         _MTIInformationDialog(ScLimsErrMsg);
         return false;
      }
   }

   // scratch limits must be within bounds of magnifier
   RECT magRect = getMagnifierRectangle();
   RECT limRect = getScratchLimitRectangle();
   RECT scrRect = getScratchRectangle();

#if 0  // limits are ALWAYS valid now!
   // scratch limits must have been moved (they start with neither focused)
   bool valid = !(scratchLimitFocusSide == SCRATCH_LIMIT_FOCUS_NONE);
   if (!valid) {
      String ScRectErrMsg = "Please set the red lines that identify scratch edges";
      MessageDlg(ScRectErrMsg, mtError, TMsgDlgButtons() << mbOK, 0);
      return false;
   }
#endif

   // scratch
   bool valid = false;
   if (IsScratchVertical())
   {
      if ((limRect.left > magRect.left) && (limRect.right < magRect.right))
         valid = true;
   }
   else // Horizontal
   {
      if ((limRect.top > magRect.top) && (limRect.bottom < magRect.bottom))
         valid = true;
   }

   if (!valid)
   {
      String ScRectErrMsg = "Scratch limits must lie within magnifier box";
      MessageDlg(ScRectErrMsg, mtError, TMsgDlgButtons() << mbOK, 0);
      return false;
   }

   return true;
}
//---------------------------------------------------------------------------

void TScratchForm::doPreProcessing(bool newProcessingSingleFrameFlag)
{
   if (GScratchTool == NULL) return;

   // Make sure we aren't looking at the provisional frame - don't want to
   // capture that!!!
   GScratchTool->getSystemAPI()->ClearProvisional(false);

   // capture the signals as they appear to the operator at this moment
   captureScratchSignals();

   // Remember where we were so we can go back
   homeFrameIndex  = GScratchTool->getSystemAPI()->getLastFrameIndex();

   processingSingleFrameFlag = newProcessingSingleFrameFlag;

}
//---------------------------------------------------------------------------

void TScratchForm::doPostProcessing(void)
{
   if (GScratchTool == NULL) return;

   // Bring the reticles back
   GScratchTool->showOverlays();
   enableShowReticlesCheckBox();

   if (!processingSingleFrameFlag)
   {
      // Clear the provisional frame BEFORE the goToFrameSynchronous
      GScratchTool->clearPreview();

      // Move back to the original frame
      GScratchTool->getSystemAPI()->goToFrameSynchronous(GScratchForm->homeFrameIndex);
   }
}
//---------------------------------------------------------------------------

void DoPreProcessing(bool processingSingleFrameFlag)
{
   if (GScratchForm == NULL) return;

   GScratchForm->doPreProcessing(processingSingleFrameFlag);
}
//---------------------------------------------------------------------------

void DoPostProcessing(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->doPostProcessing();
}
//---------------------------------------------------------------------------

void HandleBlueBoxStateChange(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->HandleBlueBoxStateChange();
}
//---------------------------------------------------------------------------

void TScratchForm::HandleBlueBoxStateChange(void)
{
   if (GScratchTool == NULL) return;

   POINT *lassoPtr; // this ends up NULL
   RECT blueRect;
   if(GScratchTool->getSystemAPI()->getStretchRectangleCoordinates(blueRect,
                                                           lassoPtr) == 0 &&
      (blueRect.right - blueRect.left) >= 4 &&
      (blueRect.bottom - blueRect.top) >= 4)
   {
      // Blue box was just set or changed
      // based on orientation, set up the magnifier window
      blueRectScreenCoords = GScratchTool->getSystemAPI()->getStretchRectangleScreenCoords();

      RECT magWin = getImageMagnifier()->getMagnifierWindow();
      int magCenterX = (blueRect.right + blueRect.left) / 2;
      int magCenterY = (blueRect.bottom + blueRect.top) / 2;
      int halfWidth = (MagnifierImage->Width  / MAGF) / 2;
      int halfHeight = (MagnifierImage->Height  / MAGF) / 2;
      if (HorizontalButton->Checked) {
         halfWidth = (MagnifierImage->Height  / MAGF) / 2;
         halfHeight = (MagnifierImage->Width  / MAGF) / 2;
      }
      magWin.left = magCenterX - halfWidth;
      magWin.right = magCenterX + halfWidth - 1;
      magWin.top = magCenterY - halfHeight;
      magWin.bottom = magCenterY + halfHeight - 1;
      getImageMagnifier()->setMagnifierWindow(&magWin);
      // QQQ I don't think I need this
      GScratchTool->moveMagnifierWindow(magCenterX, magCenterY);

      // turn on the magnifier
      activateMagnifier();
   }
   else {
      // Blue Box was removed or is too small

      // turn off the magnifier
      deactivateMagnifier();
   }

   // Don't know if this is needed or not - can't hurt
   GScratchTool->getSystemAPI()->refreshFrame();
}
//---------------------------------------------------------------------------

bool IsToolVisible(void)
{

   return((GScratchForm != NULL) && GScratchForm->ScratchControlPanel->Visible);
}
//---------------------------------------------------------------------------

void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                            bool processingRenderFlag)

{
   if (GScratchForm != NULL)
       GScratchForm->UpdateExecutionButtons(toolProcessingControlState,
                                            processingRenderFlag);
}
//---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
   if (GScratchForm == NULL)  return false;

   return GScratchForm->runExecButtonsCommand(command);
}

bool TScratchForm::runExecButtonsCommand(EExecButtonId command)
{
   return ExecButtonsToolbar->RunCommand(command);
}

//---------------------------------------------------------------------------

void GatherParameters(CScratchParameters &toolParams)
{
   if (GScratchForm != NULL)
       GScratchForm->GatherParameters(toolParams);
}
//---------------------------------------------------------------------------

void CaptureGUIParameters(CPDLElement &pdlToolParams)
{
   // Get the Scratch parameters in the GUI to include in a PDL Entry
   CScratchParameters toolParams;

   // Get the settings from the GUI
   GatherParameters(toolParams);

   // Add the settings to the PDL Entry
   toolParams.WritePDLEntry(pdlToolParams);
}
//---------------------------------------------------------------------------

void SetGUIParameters(CScratchParameters &toolParams)
{
   if (GScratchForm == NULL) return;

   // Init Scratch Tool GUI, then show Panel1
   ShowScratchGUI();

   // Set user controls:

   // Set "Does scratch move?"
   GScratchForm->YesMoveButton->Checked = toolParams.scratchMoves;

   // Set "Select scratch type" and
   //if (toolParams.scratchType == SCRATCH_TYPE_SURFACE)
      //GScratchForm->SurfaceButton->Checked = true;
   //else if (toolParams.scratchType == SCRATCH_TYPE_DEEP)
      //GScratchForm->DeepButton->Checked = true;

   // Set "Select scratch orientation"
   GScratchForm->HorizontalButton->Checked = toolParams.scratchHorizontal;

   // Set the Scratch Rectangle
   GScratchTool->setScratchRectangle(toolParams.surroundingRectangle);

   // Set the red lines within the magnifier windows
   GScratchForm->setScratchLimitRectangle(toolParams.limitRectangle);
   GScratchForm->setScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_NONE);

   // Emulate "click" on Panel1's Next button
   //Panel1NextButtonClicked();
   HandleBlueBoxStateChange();

   // Now set that little red box which is the magnifier window
   SetMagnifierWindow(toolParams.templateWindow);

   // Refresh screen so that Magnifier calls the Panel3's callback and generates
   // the Y, R, G, and B signals (from which the template will be derived). NOW
   // we are particular about which frame we are on -- a saved tool parameter.
   // PREVIOUSLY: GScratchTool->getSystemAPI()->refreshFrameCached();
   GScratchTool->getSystemAPI()->goToFrameSynchronous(toolParams.templateFrame);

   // Emulate "click" on Panel3's Next Button
   //Panel3NextButtonClicked();

   // Emulate another "click" on Panel3's Next Button
   //Panel3NextButtonClicked();

   // Set the Detection components (signals)
   switch(toolParams.scratchSelectSignal)
      {
      case SELECT_SIGNAL_Y :
         GScratchForm->YDetectButton->Checked = true;
         break;
      case SELECT_SIGNAL_RED :
         GScratchForm->RDetectButton->Checked = true;
         break;
      case SELECT_SIGNAL_GREEN :
         GScratchForm->GDetectButton->Checked = true;
         break;
      case SELECT_SIGNAL_BLUE :
         GScratchForm->BDetectButton->Checked = true;
         break;
      }

   // Set the components to modify
   GScratchForm->RCheckBox->Checked = (toolParams.scratchModifySignal & MODIFY_SIGNAL_RED);
   GScratchForm->GCheckBox->Checked = (toolParams.scratchModifySignal & MODIFY_SIGNAL_GREEN);
   GScratchForm->BCheckBox->Checked = (toolParams.scratchModifySignal & MODIFY_SIGNAL_BLUE);

   // Emulate one last "click" on Panel3's Next Button, which will pop
   // us into Panel5
   //Panel3NextButtonClicked();

   // set up the detail modulus (0-100)
   //GScratchForm->BackgroundToleranceTrackBar->Position = toolParams.scratchDetail;
   GScratchForm->BackgroundDetailTrackEdit->TrackBar->Position = toolParams.scratchDetail;
}
//---------------------------------------------------------------------------

void SetFormMode(TFormStyle newstyle)
{
   if (GScratchForm == NULL) return;

   GScratchForm->FormStyle = newstyle;
}
//---------------------------------------------------------------------------

int ScratchDepth(void)
{
   //if (GScratchForm->SurfaceButton->Checked)
      return(SCRATCH_DEPTH_SURFACE);
   //if (GScratchForm->DeepButton->Checked)
      //return(SCRATCH_DEPTH_DEEP);
   //return(-1);
}
//---------------------------------------------------------------------------

bool MovesButtonOn(void)
{
   if (GScratchForm == NULL) return false;

   return(GScratchForm->YesMoveButton->Checked);
}
//---------------------------------------------------------------------------

bool IsScratchVertical(void)
{
   if (GScratchForm == NULL) return false;

   return(GScratchForm->VerticalButton->Checked);
}
//---------------------------------------------------------------------------

void SetScratchVertical(bool flag)
{
   if (GScratchForm == NULL) return;

   GScratchForm->VerticalButton->Checked = flag;
   GScratchForm->HorizontalButton->Checked = !flag;
}
//---------------------------------------------------------------------------

bool IsDensityDark(void)
{
   if (GScratchForm == NULL) return false;

   return(GScratchForm->DarkDensityButton->Checked);
}
//---------------------------------------------------------------------------

void SetDarkDensity(bool flag)
{
   if (GScratchForm == NULL) return;

   GScratchForm->DarkDensityButton->Checked = flag;
   GScratchForm->LightDensityButton->Checked = !flag;
}
//---------------------------------------------------------------------------

void SetScratchRectangle(const RECT &newScratchRect)
{
   if (GScratchForm == NULL) return;

   GScratchForm->setScratchRectangle(newScratchRect);
}
//---------------------------------------------------------------------------

void SetScratchLimitFocusSide(EScratchLimitFocus focusSide)
{
   if (GScratchForm == NULL) return;

   GScratchForm->setScratchLimitFocusSide(focusSide);
}
//---------------------------------------------------------------------------

void SetScratchLimitRectangle(const RECT &newLimitRect)
{
   if (GScratchForm == NULL) return;

   GScratchForm->setScratchLimitRectangle(newLimitRect);
}
//---------------------------------------------------------------------------

RECT GetScratchRectangle(void)
{
   if (GScratchForm == NULL)
   {
      RECT resRect;
      resRect.left = resRect.right = resRect.top = resRect.bottom = 0;
      return resRect;
   }

   return(GScratchForm->getScratchRectangle());
}
//---------------------------------------------------------------------------

RECT GetScratchLimitRectangle(void)
{
   if (GScratchForm == NULL)
   {
      RECT resRect;
      resRect.left = resRect.right = resRect.top = resRect.bottom = 0;
      return resRect;
   }
   return(GScratchForm->getScratchLimitRectangle());
}
//---------------------------------------------------------------------------

void ActivateMagnifier(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->activateMagnifier();
}
//---------------------------------------------------------------------------

void DeactivateMagnifier(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->deactivateMagnifier();
}
//---------------------------------------------------------------------------

void ShowMagnifier(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->showMagnifier();
}
//---------------------------------------------------------------------------

void HideMagnifier(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->hideMagnifier();
}
//---------------------------------------------------------------------------

int SetMagnifierWindow(const RECT &magwin)
{
   if (GScratchForm == NULL) return 0;
   // sigh - people don't use const when they should
   RECT *nonConstMagWin = const_cast<RECT *>( &magwin );

   return GScratchForm->getImageMagnifier()->setMagnifierWindow(nonConstMagWin);
}
//---------------------------------------------------------------------------

void RecenterMagnifier(int x, int y)
{
   if (GScratchForm == NULL) return;

   GScratchForm->setDeferredUpdateFlag();
   GScratchForm->getImageMagnifier()->recenterMagnifierWindow(x, y);
}
//---------------------------------------------------------------------------

RECT GetMagnifierWindow(void)
{
   if (GScratchForm == NULL)
   {
      RECT resRect;
      resRect.left = resRect.right = resRect.top = resRect.bottom = 0;
      return resRect;
   }
   return(GScratchForm->getImageMagnifier()->getMagnifierWindow());
}
//---------------------------------------------------------------------------

MTI_UINT16 *GetPtrToMagValuesForDetectSignal()
{
   if (GScratchForm == NULL) return NULL;

   return GScratchForm->getPtrToMagValuesForDetectSignal();
}
//---------------------------------------------------------------------------

void RefreshGraphics(void)
{
   if (GScratchTool == NULL) return;

   GScratchTool->getSystemAPI()->refreshFrame();
}
//---------------------------------------------------------------------------

void CheckAbilityToPreviewAllOrRenderAll(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->checkAbilityToPreviewAllOrRenderAll();
}
//---------------------------------------------------------------------------

void DisableShowReticlesCheckBox(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->disableShowReticlesCheckBox();
}
//---------------------------------------------------------------------------

void EnableShowReticlesCheckBox(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->enableShowReticlesCheckBox();
}
//---------------------------------------------------------------------------

void ToggleShowReticlesCheckBox(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->toggleShowReticlesCheckBox();
}
//---------------------------------------------------------------------------

void FocusOnScratchWindow(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnScratchWindow();
}
//---------------------------------------------------------------------------

void FocusOnOrientationControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnOrientationControls();
}
//---------------------------------------------------------------------------

void FocusOnMovementControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnMovementControls();
}
//---------------------------------------------------------------------------

void FocusOnDetectControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnDetectControls();
}
//---------------------------------------------------------------------------

void ToggleDensity(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->toggleDensity();
}
//---------------------------------------------------------------------------

void ToggleOrientation(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->toggleOrientation();
}
//---------------------------------------------------------------------------

void ToggleMovement(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->toggleMovement();
}
//---------------------------------------------------------------------------

void CycleUpThroughDetectControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->cycleUpThroughDetectControls();
}
//---------------------------------------------------------------------------

void CycleDownThroughDetectControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->cycleDownThroughDetectControls();
}
//---------------------------------------------------------------------------

void FocusOnProcessControls(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnProcessControls();
}
//---------------------------------------------------------------------------

void FocusOnBackgroundDetailControl(void)
{
   if (GScratchForm == NULL) return;

   GScratchForm->focusOnBackgroundDetailControl();
}
//---------------------------------------------------------------------------

bool PauseOrResumeButtonClick(void)
{
   if (GScratchForm == NULL) return false;

   return GScratchForm->simulatePauseOrResumeButtonClick();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ShowOrHidePDLButtonClick(TObject *Sender)
{
#if 0 // haha this pdl shit is fantastically complicated

   // Show the active PDL Viewer.  If one does not exist, creates it.
   // Sets focus to the active PDL Viewer

   CPDLViewerManager mgr;

   if (mgr.IsActivePDLVisible())
   {
      if (!mgr.CloseQueryAll())
      {
         _MTIErrorDialog(Handle, "Can't close PDL right now");
         return;
      }
      mgr.CloseAll();
   }
   else
   {
      mgr.ShowActivePDL();
   }
#endif // 0
}
//---------------------------------------------------------------------------

bool TScratchForm::simulatePauseOrResumeButtonClick(void)
{
   // This is moronic.
   if (RenderAllButton->Enabled && !PreviewAllButton->Enabled)
      RenderPreviewProcessing(RenderAllButton);
   else if (PreviewAllButton->Enabled && !RenderAllButton->Enabled)
      RenderPreviewProcessing(PreviewAllButton);
   else
      return false;  // Not presently processing, apparently!

   return true;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ScratchWindowFocusPanelEnter(
      TObject *Sender)
{
   // ScratchWindowFocusPanel is an invisible panel (actually Visible,
   // but you can't see it because it has no borders) that we give focus
   // to after the useer clicks on the scratch windows to set the scratch
   // limits. This lets us pretend that the scratch windows have focus
   // and we make the scratch window label bold to reflect that.
   ScratchWindowLabel1->Font->Style = (TFontStyles() << fsBold);
   ScratchWindowLabel2->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ScratchWindowFocusPanelExit(TObject *Sender)
{
   ScratchWindowLabel1->Font->Style = TFontStyles();    // Clear styles
   ScratchWindowLabel2->Font->Style = TFontStyles();    // Clear styles
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::RGBCheckBoxEnter(TObject *Sender)
{
   TCheckBox *cbox = reinterpret_cast<TCheckBox *>(Sender);
   if (cbox == NULL) return;

   cbox->Caption = "<-";
   ProcessLabel->Font->Style = (TFontStyles() << fsBold << fsUnderline);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::RGBCheckBoxExit(TObject *Sender)
{
   TCheckBox *cbox = reinterpret_cast<TCheckBox *>(Sender);
   if (cbox == NULL) return;

   cbox->Caption = "";
   ProcessLabel->Font->Style = (TFontStyles() << fsUnderline);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DetectButtonEnter(TObject *Sender)
{
   DetectLabel->Font->Style = (TFontStyles() << fsBold << fsUnderline);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DetectButtonExit(TObject *Sender)
{
   DetectLabel->Font->Style = (TFontStyles() << fsUnderline);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::OrientationEnter(TObject *Sender)
{
   OrientationGroupBox->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::OrientationExit(TObject *Sender)
{
   OrientationGroupBox->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::MovementEnter(TObject *Sender)
{
   MoveGroupBox->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::MovementExit(TObject *Sender)
{
   MoveGroupBox->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DensityEnter(TObject *Sender)
{
   DensityGroupBox->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DensityExit(TObject *Sender)
{
   DensityGroupBox->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::BackgroundToleranceTrackBarEnter(
      TObject *Sender)
{
   //BackgroundToleranceLabel->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::BackgroundToleranceTrackBarExit(
      TObject *Sender)
{
   //BackgroundToleranceLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::EdgeBlurTrackBarEnter(TObject *Sender)
{
   //EdgeBlurLabel->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::EdgeBlurTrackBarExit(TObject *Sender)
{
   //EdgeBlurLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ShowReticlesCheckBoxClick(TObject *Sender)
{
   if (GScratchTool == NULL) return;

   if (ShowReticlesCheckBox->Checked)
      GScratchTool->showOverlays();
   else
      GScratchTool->hideOverlays();
}
//---------------------------------------------------------------------------

void TScratchForm::disableShowReticlesCheckBox(void)
{
   ShowReticlesCheckBox->Enabled = false;
   ShowReticlesCheckBox->Checked = false;
}
//---------------------------------------------------------------------------

void TScratchForm::enableShowReticlesCheckBox(void)
{
   ShowReticlesCheckBox->Enabled = true;
   ShowReticlesCheckBox->Checked = true;
}
//---------------------------------------------------------------------------

void TScratchForm::toggleShowReticlesCheckBox(void)
{
   ShowReticlesCheckBox->Checked = !ShowReticlesCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::MovementButtonClick(TObject *Sender)
{
   if (GScratchTool != NULL)
      GScratchTool->acceptFix();

   checkAbilityToPreviewAllOrRenderAll();

   if (NoMoveButton->Checked)
   {
      YesMoveButton->TabStop = true;
      NoMoveButton->TabStop = false;
   }
   else
   {
      YesMoveButton->TabStop = false;
      NoMoveButton->TabStop = true;
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::UpdateTimerTimer(TObject *Sender)
{
   checkAbilityToPreviewAllOrRenderAll();

   if (deferredUpdateFlag)
   {
      imageMagnifierRefresh(imageMagnifier);
      deferredUpdateFlag = false;
      GScratchTool->autoLocateScratchLimits(false);
   }
}
//---------------------------------------------------------------------------


void __fastcall TScratchForm::DetectButtonClick(TObject *Sender)
{
   // Repaint scratch window to move yellow box highlight
   imageMagnifierRefresh(imageMagnifier);

   if (GScratchTool != NULL)
      GScratchTool->autoLocateScratchLimits();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ScratchWindowLabelClick(TObject *Sender)
{
   focusOnScratchWindow();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::DetectLabelClick(TObject *Sender)
{
   focusOnDetectControls();
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ProcessLabelClick(TObject *Sender)
{
   focusOnProcessControls();
}
//---------------------------------------------------------------------------

bool TScratchForm::HandleToolEvent(const FosterParentEventInfo &eventInfo)
{
   bool retVal = false;  // assume not handled

   if (GScratchTool == NULL) return retVal;

   switch (eventInfo.Id)
   {
      case FPE_FormPaint:
         RefreshGraphics();
         retVal = true;
      break;

      case 0: //FPE_FormKeyDown:
         switch(eventInfo.Key)
         {
            case MTK_LEFT:
               GScratchTool->onLeftArrow(eventInfo.Shift, eventInfo.Ctrl);
            break;

            case MTK_RIGHT:
               GScratchTool->onRightArrow(eventInfo.Shift, eventInfo.Ctrl);
            break;

            case MTK_UP:
               GScratchTool->onUpArrow(eventInfo.Shift, eventInfo.Ctrl);
            break;

            case MTK_DOWN:
               GScratchTool->onDownArrow(eventInfo.Shift, eventInfo.Ctrl);
            break;

            default:
               // Ignore everything else
            break;
         }
      break;

      default:
         // Do nothing
      break;
   }

   return retVal;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------------ EXEC BUTTONS TOOLBAR -----------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TScratchForm::ExecButtonsToolbar_ButtonPressedNotifier(
      TObject *Sender)
{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
      default:
      case EXEC_BUTTON_NONE:
         // No-op
      break;

      case EXEC_BUTTON_PREVIEW_FRAME:
         PreviewFrame();
      break;

      case EXEC_BUTTON_PREVIEW_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
      break;

      case EXEC_BUTTON_RENDER_FRAME:
         RenderFrame();
      break;

      case EXEC_BUTTON_RENDER_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
      break;

      case EXEC_BUTTON_PAUSE_OR_RESUME:
         if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
         else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_PAUSE:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
      break;

      case EXEC_BUTTON_RESUME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_STOP:
         ExecStop();
         break;

      case EXEC_BUTTON_CAPTURE_PDL:
         ExecCaptureToPDL();
      break;

      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         ExecCaptureToPDL();
      break;

      case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         ExecGoToResumeTimecode();
      break;

      case EXEC_BUTTON_SET_RESUME_TC:
         ExecSetResumeTimecodeToCurrent();
      break;
   }
}
//---------------- Resume Timecode Functions --------------------------------

void TScratchForm::ExecSetResumeTimecode(int frameIndex)
{
   CVideoFrameList *frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   if (frameIndex < 0)
      frameIndex = frameList->getInFrameIndex();

   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
//---------------------------------------------------------------------------

void TScratchForm::ExecSetResumeTimecodeToCurrent(void)
{
   if (GScratchTool == NULL) return;

   int frameIndex = GScratchTool->getSystemAPI()->getLastFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TScratchForm::ExecSetResumeTimecodeToDefault(void)
{
   if (GScratchTool == NULL) return;

   int frameIndex = GScratchTool->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TScratchForm::ExecGoToResumeTimecode(void)
{
   if (GScratchTool == NULL) return;

   CVideoFrameList *frameList;
   frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // No! GScratchTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GScratchTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = GScratchTool->getSystemAPI()->getLastFrameIndex();
   ExecButtonsToolbar->SetResumeTimecode(
                        frameList->getTimecodeForFrameIndex(frameIndex));
}

//------------------ ExecRenderOrPreview ------------------------------------
//
//  Call this when any render or preview button is clicked
//  buttonCommand identifies the button that was pressed
//
void TScratchForm::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
   if (GScratchTool == NULL) return;

   if (!areAllSystemsGo(false))       // = 'NOT single frame'
      return;

   switch (buttonCommand)
   {
      case TOOL_PROCESSING_CMD_PREVIEW :
      case TOOL_PROCESSING_CMD_RENDER :
         doPreProcessing(false);     // not single frame
         GScratchTool->StartToolProcessing(buttonCommand,false);
      break;

      case TOOL_PROCESSING_CMD_PAUSE :
         GScratchTool->PauseToolProcessing();
      break;

      case TOOL_PROCESSING_CMD_CONTINUE :
      {
         CVideoFrameList *frameList;
         frameList = GScratchTool->getSystemAPI()->getVideoFrameList();
         int resumeFrame;
         if (frameList == NULL)
            resumeFrame = GScratchTool->getSystemAPI()->getMarkIn();
         else
         {
            PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
            resumeFrame = frameList->getFrameIndex(resumeTimecode);
         }
         GScratchTool->ContinueToolProcessing(resumeFrame);
      }
      break;

      default:
         MTIassert(false);
      break;
   }

   // Multi-frame ops are run asynchronously, so it's not done yet
}
//---------------------------------------------------------------------------

void TScratchForm::ExecStop(void)
{
   if (GScratchTool == NULL) return;

   GScratchTool->StopToolProcessing();
}
//---------------------------------------------------------------------------

void TScratchForm::ExecCaptureToPDL(void)
{
   if (GScratchTool == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GScratchTool->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------

void TScratchForm::UpdateExecutionButtonToolbar(
      EToolControlState toolProcessingControlState,
      bool processingRenderFlag)
{
   if (GScratchTool == NULL) return;

   if (GScratchTool->IsDisabled())
   {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                 "Can't preview - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                 "Can't render - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                 "Can't add to PDL - tool is disabled");
         return;
   }

   switch(toolProcessingControlState)
   {
      case TOOL_CONTROL_STATE_STOPPED :

         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button

         setIdleCursor();

         if (NotSetYetPanel->Visible)
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
         }
         else
         {
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_FRAME);
            if (GScratchTool->AreMarksValid())
            {
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
               ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);

//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                       "Preview marked range (Shift+D)");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                       "Render marked range (Shift+G)");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                       "Add to PDL (, key)");
            }
            else
            {
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
               ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                       "Can't preview - marks are invalid");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                       "Can't render - marks are invalid");
//               ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                       "Can't add to PDL - marks are invalid");
            }
         }
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

         ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

         ExecButtonsToolbar->EnableResumeWidget();
         ExecSetResumeTimecodeToDefault();

      break;

      case TOOL_CONTROL_STATE_RUNNING :

         // Preview1, Render1, Preview or Render is now running

         setBusyCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         if (processingSingleFrameFlag)
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         else
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (!processingRenderFlag)
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
         else
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);


      break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused

         setIdleCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         // Now go load the current timecode
         ExecSetResumeTimecodeToCurrent();

      break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :

         // Processing single frame or waiting to stop or pause --
         // disable everything
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE
          && toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_STOP)
         {
            if (!processingRenderFlag)
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
            else
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
         }

      break;

      default:
         MTIassert(false);

      break;
   }

   //UpdateStatusBarInfo();

} // UpdateExecutionButton()
//---------------------------------------------------------------------------

void TScratchForm::setIdleCursor(void)
{
   Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void TScratchForm::setBusyCursor(void)
{
   Screen->Cursor = crAppStart;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::SidecarTimerTimer(TObject *Sender)
{
   if (SidecarWindow->Visible)
   {
      RECT clientRect = GScratchTool->getSystemAPI()->GetClientRectScreenCoords();
      if (clientRect.left != oldClientRect.left
      || clientRect.top != oldClientRect.top
      || clientRect.right != oldClientRect.right
      || clientRect.bottom != oldClientRect.bottom)
      {
         hideSidecar();
         oldClientRect = clientRect;
         HandleBlueBoxStateChange();
      }
   }

   if (!SidecarWindow->Visible)
   {
      SidecarTimer->Enabled = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::MirrorGraphsCheckBoxClick(TObject *Sender)
{
   if ((imageMagnifier == NULL) || !imageMagnifier->isActive())
   {
      return;
   }

   if (MirrorGraphsCheckBox->Checked)
   {
      showSidecar();

      // AFTER showSidecar()!
      moveSidecar(blueRectScreenCoords);
   }
   else
   {
      hideSidecar();
   }
}
//---------------------------------------------------------------------------

void TScratchForm::setDeferredUpdateFlag()
{
   deferredUpdateFlag = true;
}
//---------------------------------------------------------------------------

void __fastcall TScratchForm::BackgroundPanelClick(TObject *Sender)
{
   focusOnScratchWindow();
}
//---------------------------------------------------------------------------
