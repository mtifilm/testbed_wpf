/*
	AlgorithmPS.cpp   - source file for Persistent Scratch algorithm
        Kevin Manbeck
        Feb 9, 2003

        The AlgorithmPS class is used to repair persistent scratches.  See
	AlgorithmPS.h for more detailed description.
*/

#include "AlgorithmPS.h"
#include "PixelRegions.h"
#include "err_scratch.h"
#include "ScratchTool.h"
#include "ColorSpaceConvert.h"
#include "ImageFormat3.h"
#include <stdio.h>
#include <math.h>

CAlgorithmPS::CAlgorithmPS()
{
  psaAlgorithm.uspYRGB = NULL;
  psaAlgorithm.lpLeftCol = NULL;
  psaAlgorithm.fpTemplate = NULL;
  psaAlgorithm.fpProfile = NULL;
  psaAlgorithm.fpCorr = NULL;
  psaAlgorithm.dppScratch = NULL;
  psaAlgorithm.wpWarp = NULL;

  ifpNative = 0;
  ifpGUI = 0;
  cscpNativeToGUI = 0;
  cscpGUIToNative = 0;

  Initialize ();
}  /* CAlgorithmPS */

CAlgorithmPS::~CAlgorithmPS()
{
  FreeGeneral ();
  FreeAlgorithm ();
}  /* ~CAlgorithmPS */

void CAlgorithmPS::Initialize ()
{
  lNRow = -1;
  lNCol = -1;
  lNCom = -1;

  for (long lCom = 0; lCom < MAX_COMPONENT; lCom++)
   {
    laLowerBound[lCom] = -1;
    laUpperBound[lCom] = -1;
   }

  FreeGeneral ();
  FreeAlgorithm ();
}  /* Initialize */

int CAlgorithmPS::SetDimension (long lNRowArg, long lNColArg, long lNComArg,
	long lActiveStartRowArg, long lActiveStopRowArg,
	long lActiveStartColArg, long lActiveStopColArg)
{
  int iRet;

  FreeGeneral ();
  FreeAlgorithm ();

  if (lNRowArg <= 0)
   {
    return ERR_PLUGIN_PS_BAD_VERTICAL_DIMENSION;
   }

  if (lNColArg <= 0)
   {
    return ERR_PLUGIN_PS_BAD_HORIZONTAL_DIMENSION;
   }

  if (lNComArg <= 0)
   {
    return ERR_PLUGIN_PS_BAD_COMPONENT_DIMENSION;
   }

  if (lActiveStartRowArg < 0 || lActiveStopRowArg > lNRowArg ||
	lActiveStartRowArg >= lActiveStopRowArg)
   {
    return ERR_PLUGIN_PS_BAD_VERTICAL_ACTIVE;
   }

  if (lActiveStartColArg < 0 || lActiveStopColArg > lNColArg ||
	lActiveStartColArg >= lActiveStopColArg)
   {
    return ERR_PLUGIN_PS_BAD_HORIZONTAL_ACTIVE;
   }

  lNRow = lNRowArg;
  lNCol = lNColArg;
  lNCom = lNComArg;
  lNRowCol = lNRow * lNCol;
  lNRowColCom = lNRow * lNCol * lNCom;
  lActiveStartRow = lActiveStartRowArg;
  lActiveStopRow = lActiveStopRowArg;
  lActiveStartCol = lActiveStartColArg;
  lActiveStopCol = lActiveStopColArg;

  return 0;
}  /* SetDimension */

int CAlgorithmPS::SetImageBounds (MTI_UINT16 spLowerBoundArg[], MTI_UINT16 spUpperBoundArg[])
{
  long lCom;

  if (lNCom <= 0 || lNCom > MAX_COMPONENT)
    return ERR_PLUGIN_PS_BAD_COMPONENT_DIMENSION;

  for (lCom = 0; lCom < lNCom; lCom++)
   {
    laLowerBound[lCom] = spLowerBoundArg[lCom];
    laUpperBound[lCom] = spUpperBoundArg[lCom];
   }

  return 0;
}  /* SetImageBounds */

void CAlgorithmPS::SetColorSpaceConvert (
	CColorSpaceConvert *cscpNativeToGUIArg,
	CColorSpaceConvert *cscpGUIToNativeArg)
{
  cscpNativeToGUI = cscpNativeToGUIArg;
  cscpGUIToNative = cscpGUIToNativeArg;
}  /* SetColorSpaceConvert */


int CAlgorithmPS::AssignParameter (CScratchParameters *spp)
{
  int iRet;
  bool bNeedChange;
  PERSISTENT_SCRATCH_SETTING pssLocal;

  // large box which surrounds the scratch in all frames;
  // clip it to the active image area
  pssLocal.lSurroundLeft = spp->surroundingRectangle.left;
  if (pssLocal.lSurroundLeft < lActiveStartCol)
    pssLocal.lSurroundLeft = lActiveStartCol;
  pssLocal.lSurroundRight = spp->surroundingRectangle.right;
  if (pssLocal.lSurroundRight >= lActiveStopCol)
    pssLocal.lSurroundRight = lActiveStopCol - 1;
  pssLocal.lSurroundTop = spp->surroundingRectangle.top;
  if (pssLocal.lSurroundTop < lActiveStartRow)
    pssLocal.lSurroundTop = lActiveStartRow;
  pssLocal.lSurroundBottom = spp->surroundingRectangle.bottom;
  if (pssLocal.lSurroundBottom >= lActiveStopRow)
    pssLocal.lSurroundBottom = lActiveStopRow - 1;

  pssLocal.lTemplateLeft = spp->templateWindow.left;
  pssLocal.lTemplateRight = spp->templateWindow.right;
  pssLocal.lTemplateTop = spp->templateWindow.top;
  pssLocal.lTemplateBottom = spp->templateWindow.bottom;

  // components to process
  pssLocal.lPrincipleComponent = spp->scratchSelectSignal;
  pssLocal.lComponentMask = spp->scratchModifySignal;

  pssLocal.bMoving = spp->scratchMoves;
  pssLocal.bHorizontal = spp->scratchHorizontal;
  pssLocal.lType = spp->scratchType;

  // Persistent scratch removal can become confused by highly detailed
  // backgrounds.  The BackgroundPixel is the number of neighboring pixels
  // to use in repairing a scratched pixel.  When BackgroundPixel is large,
  // the scratch algorithm is more tolerant to background variation and
  // less tolerant of variations in the scratch.  When BackgroundPixel is
  // small, the scratch algorithm is less tolerant to background variation
  // but more tolerant to variations in the scratch.

  long lDetail = spp->scratchDetail;
  if (lDetail < 0) lDetail = 0;
  if (lDetail > 100) lDetail = 100;

  long lPixel;
  if (pssLocal.bHorizontal)
    lPixel = lNCol;
  else
    lPixel = lNRow;

  float fFactor;

  if (spp->scratchDetail < 50)
   {
    fFactor = ( (float)(lDetail-0) / (float)(50-0) ) * 
		(BACKGROUND_PERCENT_50 - BACKGROUND_PERCENT_0) +
			BACKGROUND_PERCENT_0;
   }
  else
   {
    fFactor = ( (float)(lDetail-50) / (float)(100-50) ) * 
		(BACKGROUND_PERCENT_100 - BACKGROUND_PERCENT_50) +
			BACKGROUND_PERCENT_50;
   }
  pssLocal.lBackgroundPixel = fFactor * (float)lPixel + 0.5;

  // set up the template characteristics
  pssLocal.lNTemplate =  MAX_TEMPLATE;
  pssLocal.lFirstTemplate = spp->edgeTemplate0;
  pssLocal.lLastTemplate = spp->edgeTemplate1;
  for (long lTemplate = 0; lTemplate < pssLocal.lNTemplate; lTemplate++)
   {
    pssLocal.faTemplate[lTemplate] = spp->faTemplate[lTemplate];
   }

  //  Check to see if we need to make any changes
  bNeedChange = CompareParameter (&pssLocal, &pssSetting);

  // only reassign parameter settings if a change is needed
  if (bNeedChange)
   {
    CopyParameter (&pssLocal, &pssSetting);
    iRet = AllocAlgorithm ();
    if (iRet)
     {
      return iRet;
     }
    iRet = SetupAlgorithm ();
    if (iRet)
     {
      return iRet;
     }
   }

  return 0;
}  /* AssignParameter */

int CAlgorithmPS::SetSrcImgPtr (MTI_UINT16 **uspSrcArg, long *lpFrameIndex)
{

/*
	Rearrange the Src pointers so that the image to process is at 0
*/

  if (lpFrameIndex[0]==-2 && lpFrameIndex[1]==-1 && lpFrameIndex[2]==0)
   {
    uspSrc[0] = uspSrcArg[2];
    uspSrc[1] = uspSrcArg[1];
    uspSrc[2] = uspSrcArg[0];
   }
  else if (lpFrameIndex[0]==-1 && lpFrameIndex[1]==0 && lpFrameIndex[2]==1)
   {
    uspSrc[0] = uspSrcArg[1];
    uspSrc[1] = uspSrcArg[0];
    uspSrc[2] = uspSrcArg[2];
   }
  else if (lpFrameIndex[0]==0 && lpFrameIndex[1]==1 && lpFrameIndex[2]==2)
   {
    uspSrc[0] = uspSrcArg[0];
    uspSrc[1] = uspSrcArg[1];
    uspSrc[2] = uspSrcArg[2];
   }
  
  return 0;
}  /* SetSrcImgPtr */

void CAlgorithmPS::SetDstImgPtr (MTI_UINT16 *uspDstArg)
{
  uspDst = uspDstArg;
}  /* SetDstImgPtr */

void CAlgorithmPS::SetPixelRegionList (CPixelRegionList *prlpArg)
{
  prlpOriginalValues = prlpArg;
}  /* SetPixelRegionList */

int CAlgorithmPS::Process ()
{
  // convert to the internal YRGB format.
  CopyBeforeScratch ();

  // find the scratch
  FindScratch ();

  // repair the scratch
  RepairScratch ();

  // convert back to the native format.
  CopyAfterScratch ();

  return 0;
}  /* Process */

bool CAlgorithmPS::CompareParameter (const PERSISTENT_SCRATCH_SETTING *pssp1,
		const PERSISTENT_SCRATCH_SETTING *pssp2)
/*
	If the parameter settings are different, return true
*/
{
  if (pssp1->lSurroundLeft != pssp2->lSurroundLeft)
    return true;
  if (pssp1->lSurroundRight != pssp2->lSurroundRight)
    return true;
  if (pssp1->lSurroundTop != pssp2->lSurroundTop)
    return true;
  if (pssp1->lSurroundBottom != pssp2->lSurroundBottom)
    return true;

  if (pssp1->lTemplateLeft != pssp2->lTemplateLeft)
    return true;
  if (pssp1->lTemplateRight != pssp2->lTemplateRight)
    return true;
  if (pssp1->lTemplateTop != pssp2->lTemplateTop)
    return true;
  if (pssp1->lTemplateBottom != pssp2->lTemplateBottom)
    return true;

  if (pssp1->lPrincipleComponent != pssp2->lPrincipleComponent)
    return true;
  if (pssp1->lComponentMask != pssp2->lComponentMask)
    return true;

  if (pssp1->bMoving != pssp2->bMoving)
    return true;
  if (pssp1->bHorizontal != pssp2->bHorizontal)
    return true;

  if (pssp1->lType != pssp2->lType)
    return true;
  if (pssp1->lBackgroundPixel != pssp2->lBackgroundPixel)
    return true;

  if (pssp1->lNTemplate != pssp2->lNTemplate)
    return true;
  if (pssp1->lFirstTemplate != pssp2->lFirstTemplate)
    return true;
  if (pssp1->lLastTemplate != pssp2->lLastTemplate)
    return true;

  for (long lTemplate = 0; lTemplate < pssp1->lNTemplate; lTemplate++)
   {
    if (pssp1->faTemplate[lTemplate] != pssp2->faTemplate[lTemplate])
      return true;
   }

  return false;
}  /* CompareParameter */

void CAlgorithmPS::CopyParameter (const PERSISTENT_SCRATCH_SETTING *psspSrc,
		PERSISTENT_SCRATCH_SETTING *psspDst)
{
  // large box which surrounds the scratch in all frames
  psspDst->lSurroundLeft = psspSrc->lSurroundLeft;
  psspDst->lSurroundRight = psspSrc->lSurroundRight;
  psspDst->lSurroundTop = psspSrc->lSurroundTop;
  psspDst->lSurroundBottom = psspSrc->lSurroundBottom;

  // box which contains template data
  psspDst->lTemplateLeft = psspSrc->lTemplateLeft;
  psspDst->lTemplateRight = psspSrc->lTemplateRight;
  psspDst->lTemplateTop = psspSrc->lTemplateTop;
  psspDst->lTemplateBottom = psspSrc->lTemplateBottom;
  
  // components to process
  psspDst->lPrincipleComponent = psspSrc->lPrincipleComponent;
  psspDst->lComponentMask = psspSrc->lComponentMask;

  psspDst->bMoving = psspSrc->bMoving;
  psspDst->bHorizontal = psspSrc->bHorizontal;
  psspDst->lType = psspDst->lType;

  psspDst->lBackgroundPixel = psspSrc->lBackgroundPixel;

  // set up the template characteristics
  psspDst->lNTemplate = psspSrc->lNTemplate;
  psspDst->lFirstTemplate = psspSrc->lFirstTemplate;
  psspDst->lLastTemplate = psspSrc->lLastTemplate;
  for (long lTemplate = 0; lTemplate < psspDst->lNTemplate; lTemplate++)
   {
    psspDst->faTemplate[lTemplate] = psspSrc->faTemplate[lTemplate];
   }

}  /* CopyParameter */

int CAlgorithmPS::AllocAlgorithm ()
/*
	This function uses the values in pssSetting to allocate the
	necessary storage
*/
{

  // Free previous call
  FreeAlgorithm();

  // the normalized template is twice as wide as the active portion of
  // the GUI template.
  psaAlgorithm.lWidth = pssSetting.lLastTemplate-pssSetting.lFirstTemplate+1;
  psaAlgorithm.lOutsideTemplate = psaAlgorithm.lWidth / 2;
  if (psaAlgorithm.lOutsideTemplate < 2)
    psaAlgorithm.lOutsideTemplate = 2;
  psaAlgorithm.lNTemplate = psaAlgorithm.lWidth + 2*psaAlgorithm.lOutsideTemplate;

  // horizontal scratches are rotated so that all calulations happen
  // from top to bottom
  if (pssSetting.bHorizontal)
   {
    psaAlgorithm.lNRow = pssSetting.lSurroundRight -
		pssSetting.lSurroundLeft + 1;
    psaAlgorithm.lNCol = pssSetting.lSurroundBottom -
		pssSetting.lSurroundTop + 1;
    psaAlgorithm.lStationaryLeft = pssSetting.lTemplateTop +
                pssSetting.lFirstTemplate - pssSetting.lSurroundTop;
   }
  else
   {
    psaAlgorithm.lNCol = pssSetting.lSurroundRight -
		pssSetting.lSurroundLeft + 1;
    psaAlgorithm.lNRow = pssSetting.lSurroundBottom -
		pssSetting.lSurroundTop + 1;
    psaAlgorithm.lStationaryLeft = pssSetting.lTemplateLeft +
                pssSetting.lFirstTemplate - pssSetting.lSurroundLeft;
   }

  // we allow the template to fall outside the surrounding box on the 
  // left and right sides
  psaAlgorithm.lNCol += 2 * psaAlgorithm.lOutsideTemplate;
  psaAlgorithm.lStationaryLeft += psaAlgorithm.lOutsideTemplate;


  // allocate storage to hold YRGB values
  psaAlgorithm.uspYRGB = (MTI_UINT16 *) malloc (4 * psaAlgorithm.lNRow *
	psaAlgorithm.lNCol * sizeof (MTI_UINT16));
  if (psaAlgorithm.uspYRGB == NULL)
    return ERR_PLUGIN_PS_MALLOC;

  // allocate storage to hold left edge of scratch at every row
  psaAlgorithm.lpLeftCol = (long *) malloc (psaAlgorithm.lNRow * sizeof (long));
  if (psaAlgorithm.lpLeftCol == NULL)
    return ERR_PLUGIN_PS_MALLOC;


  // allocate storage to hold normalized template
  psaAlgorithm.fpTemplate = (float *) malloc (psaAlgorithm.lNTemplate *
		sizeof(float));
  if (psaAlgorithm.fpTemplate == NULL)
    return ERR_PLUGIN_PS_MALLOC;

  // allocate storage to hold normalized data
  psaAlgorithm.fpProfile = (float *) malloc (psaAlgorithm.lNTemplate *
		sizeof(float));
  if (psaAlgorithm.fpProfile == NULL)
    return ERR_PLUGIN_PS_MALLOC;

  // allocate storage to hold correlation between template and image
  psaAlgorithm.fpCorr = (float *) malloc (psaAlgorithm.lNRow *
		psaAlgorithm.lNCol * sizeof (float));
  if (psaAlgorithm.fpCorr == NULL)
    return ERR_PLUGIN_PS_MALLOC;

  // allocate storage for the dynamic programming
  if (psaAlgorithm.dppScratch != NULL)
    return ERR_PLUGIN_PS_UNINITIALIZED_MEMORY;
  psaAlgorithm.dppScratch = (DYNAMIC_PROGRAM **) malloc
		(psaAlgorithm.lNRow * sizeof(DYNAMIC_PROGRAM *));
  if (psaAlgorithm.dppScratch == NULL)
    return ERR_PLUGIN_PS_MALLOC;
  for (long lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
   {
    psaAlgorithm.dppScratch[lRow] = NULL;
   }
  for (long lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
   {
    if (psaAlgorithm.dppScratch[lRow] != NULL)
      return ERR_PLUGIN_PS_UNINITIALIZED_MEMORY;
    psaAlgorithm.dppScratch[lRow] = (DYNAMIC_PROGRAM *) malloc
		(psaAlgorithm.lNCol * sizeof(DYNAMIC_PROGRAM));
    if (psaAlgorithm.dppScratch[lRow] == NULL)
      return ERR_PLUGIN_PS_MALLOC;
   }

  // allocate storage for the warp
  if (psaAlgorithm.wpWarp != NULL)
    return ERR_PLUGIN_PS_UNINITIALIZED_MEMORY;
  psaAlgorithm.wpWarp = (WARP **) malloc (psaAlgorithm.lNRow * sizeof(WARP *));
  if (psaAlgorithm.wpWarp == NULL)
    return ERR_PLUGIN_PS_MALLOC;
  for (long lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
   {
    psaAlgorithm.wpWarp[lRow] = NULL;
   }

  for (long lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
   {
    if (psaAlgorithm.wpWarp[lRow] != NULL)
      return ERR_PLUGIN_PS_UNINITIALIZED_MEMORY;
    psaAlgorithm.wpWarp[lRow] = (WARP *) malloc (psaAlgorithm.lNTemplate *
		sizeof (WARP));
    if (psaAlgorithm.wpWarp[lRow] == NULL)
      return ERR_PLUGIN_PS_MALLOC;
   }

  psaAlgorithm.fMaxSkew = MAX_SKEW;
  psaAlgorithm.fMaxWiggle = MAX_WIGGLE;

  return 0;
}  /* AllocAlgorithm */

int CAlgorithmPS::SetupAlgorithm ()
/*
	Initialize the storage used by the algorithm
*/
{
  long lRow, lCol;
  float *fpCorr;

  // initialize fpCorr
  fpCorr = psaAlgorithm.fpCorr;
  for (lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
  for (lCol = 0; lCol < psaAlgorithm.lNCol; lCol++)
   {
    *fpCorr++ = -1.;
   }

  // normalize the template
  NormalizeTemplate ();

  return 0;
}  /* SetupAlgorithm */

void CAlgorithmPS::CopyBeforeScratch ()
{
  long lRowSrc, lRowDst, lColSrc, lColDst, lRSrc, lCSrc, lRCSrc;
  MTI_UINT16 usSrc0, usSrc1, usSrc2, usDst0, usDst1, usDst2;
  MTI_UINT16 *uspYRGB;
  PERSISTENT_SCRATCH_SETTING *pssp;
  PERSISTENT_SCRATCH_ALGORITHM *psap;


  pssp = &pssSetting;
  psap = &psaAlgorithm;

/*
	Copy data in such a way that the scratch runs from top to bottom
*/

  if (pssp->bHorizontal == false)
   {
    uspYRGB = psap->uspYRGB;

    lRowSrc = pssp->lSurroundTop;
    for (lRowDst = 0; lRowDst < psap->lNRow; lRowDst++)
     {
      lRSrc = lRowSrc++;
      if (lRSrc < lActiveStartRow)
        lRSrc = lActiveStartRow;
      if (lRSrc >= lActiveStopRow)
        lRSrc = lActiveStopRow - 1;

      lColSrc = pssp->lSurroundLeft - psap->lOutsideTemplate;
      for (lColDst = 0; lColDst < psap->lNCol; lColDst++)
       {
        lCSrc = lColSrc++;
        if (lCSrc < lActiveStartCol)
          lCSrc = lActiveStartCol;
        if (lCSrc >= lActiveStopCol)
          lCSrc = lActiveStopCol - 1;

        lRCSrc = (lRSrc * lNCol + lCSrc) * lNCom;

        // extract the source component values from the source image
        usSrc0 = uspSrc[0][lRCSrc++];
        usSrc1 = uspSrc[0][lRCSrc++];
        usSrc2 = uspSrc[0][lRCSrc++];

        // use colorspace convert to change to RGB
        cscpNativeToGUI->ConvertOneValue (usSrc0, usSrc1, usSrc2,
		&usDst0, &usDst1, &usDst2);

        // load values into YRGB array
        *uspYRGB++ = (usDst0 + usDst1 + usDst2) / 3;   // the Y value
        *uspYRGB++ = usDst0;		     // the R value
        *uspYRGB++ = usDst1;		     // the G value
        *uspYRGB++ = usDst2;		     // the B value
       }
     }
   }
  else
   {
    uspYRGB = psap->uspYRGB;
    
    lColSrc = pssp->lSurroundLeft;
    for (lRowDst = 0; lRowDst < psap->lNRow; lRowDst++)
     {
      lCSrc = lColSrc++;
      if (lCSrc < lActiveStartCol)
        lCSrc = lActiveStartCol;
      if (lCSrc >= lActiveStopCol)
        lCSrc = lActiveStopCol - 1;

      lRowSrc = pssp->lSurroundTop - psap->lOutsideTemplate;
      for (lColDst = 0; lColDst < psap->lNCol; lColDst++)
       {
        lRSrc = lRowSrc++;
        if (lRSrc < lActiveStartRow)
          lRSrc = lActiveStartRow;
        if (lRSrc >= lActiveStopRow)
          lRSrc = lActiveStopRow - 1;

        lRCSrc = (lRSrc * lNCol + lCSrc) * lNCom;

        // extract the source component values from the source image
        usSrc0 = uspSrc[0][lRCSrc++];
        usSrc1 = uspSrc[0][lRCSrc++];
        usSrc2 = uspSrc[0][lRCSrc++];

        // use colorspace convert to change to RGB
        cscpNativeToGUI->ConvertOneValue (usSrc0, usSrc1, usSrc2,
		&usDst0, &usDst1, &usDst2);

        // load values into YRGB array
        *uspYRGB++ = (usDst0 + usDst1 + usDst2) / 3;   // the Y value
        *uspYRGB++ = usDst0;		     // the R value
        *uspYRGB++ = usDst1;		     // the G value
        *uspYRGB++ = usDst2;		     // the B value
       }
     }
   }

  return;
}  /* CopyBeforeScratch */

void CAlgorithmPS::FindScratch ()
/*
	Use dynamic programming to track a scratch signal from
	top-to-bottom.
*/
{

  if (pssSetting.bMoving)
   {
    FilterData ();

    TrackScratch ();
   }
  else
   {
    for (long lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
     {
      psaAlgorithm.lpLeftCol[lRow] = psaAlgorithm.lStationaryLeft;
     }
   }

  return;
}  /* FindScratch */

void CAlgorithmPS::CopyAfterScratch ()
{
  // make a list of pixels that have changed
  RunThroughWarp (true);

  // create the destination image
  RunThroughWarp (false);
}  /* CopyAfterScratch */

void CAlgorithmPS::RunThroughWarp (bool bForHistory)
/*
        Call this function twice:  first to create the history, second to
        create the destination image.

        By calling the function twice we can save the original values to
        the history file even in the case that the render is perfomed in place.
*/
{
  long lRowSrc, lRowDst, lColSrc, lColDst, lRCSrc;
  long lColSrcLast, lRowSrcLast, lWidth, lWarp;
  float fDst0, fDst1, fDst2;
  MTI_UINT16 usaSrc[MAX_COMPONENT], usDst0, usDst1, usDst2;
  MTI_UINT16 *uspS, *uspD;
  long lRowColCom, lRow, lCol, lCom;
  PERSISTENT_SCRATCH_SETTING *pssp;
  PERSISTENT_SCRATCH_ALGORITHM *psap;
  RECT rectPixelRegion;

  pssp = &pssSetting;
  psap = &psaAlgorithm;

  if ((uspSrc[0] != uspDst) && (bForHistory==false))
   {
    // source and destination image are not the same, so copy over source
    for (lRow = lActiveStartRow; lRow < lActiveStopRow; lRow++)
     {
      lRowColCom = (lRow * lNCol + lActiveStartCol) * lNCom;
      uspD = uspDst + lRowColCom;
      uspS = uspSrc[0] + lRowColCom;
      for (lCol = lActiveStartCol; lCol < lActiveStopCol; lCol++)
      for (lCom = 0; lCom < lNCom; lCom++)
       {
        *uspD++ = *uspS++;
       }
     }
   }

/*
	The scratch was repaired from top to bottom.  Copy data back to
	native orientation
*/

  if (pssp->bHorizontal == false)
   {
    lColSrcLast = -1;
    lRowSrc = pssp->lSurroundTop;
    for (lRowDst = 0; lRowDst < psap->lNRow; lRowDst++)
     {
      if (psap->lpLeftCol[lRowDst] >= psap->lOutsideTemplate)
       {
        lColSrc = pssp->lSurroundLeft - psap->lOutsideTemplate +
                  + psap->lpLeftCol[lRowDst];

        if (bForHistory)
         {
          // create an entry in the history list
          if (lColSrc == lColSrcLast)
           {
            // keep using the same pixel region
            rectPixelRegion.bottom = lRowSrc;
          }
          else
           {
            // save old pixel region
            if (lColSrcLast != -1)
             {
              prlpOriginalValues->Add (rectPixelRegion, uspSrc[0],
                                  lNCol, lNRow, lNCom);
             }
            // need to start a new pixel region
            lColSrcLast = lColSrc;
            rectPixelRegion.top = lRowSrc;
            rectPixelRegion.bottom = lRowSrc;
            rectPixelRegion.left = lColSrc;
            rectPixelRegion.right = lColSrc + psap->lWidth - 1;
           }
         }
        else
         {
          // create the entries in the destination image
          for (lWidth = 0; lWidth < psap->lWidth; lWidth++)
           {
            lWarp = lWidth + psap->lOutsideTemplate;
            // the RefineVal's are in RGB
            fDst0 = psap->wpWarp[lRowDst][lWarp].faRefineVal[0];
            fDst1 = psap->wpWarp[lRowDst][lWarp].faRefineVal[1];
            fDst2 = psap->wpWarp[lRowDst][lWarp].faRefineVal[2];

            if (fDst0 < 0.)
              fDst0 = 0.;
            else if (fDst0 > 65535.)
              fDst0 = 65535.;

            if (fDst1 < 0.)
              fDst1 = 0.;
            else if (fDst1 > 65535.)
              fDst1 = 65535.;

            if (fDst2 < 0.)
              fDst2 = 0.;
            else if (fDst2 > 65535.)
              fDst2 = 65535.;

            usDst0 = fDst0 + 0.5;
            usDst1 = fDst1 + 0.5;
            usDst2 = fDst2 + 0.5;

            // use colorspace convert
            cscpGUIToNative->ConvertOneValue (usDst0, usDst1, usDst2,
                  &usaSrc[0], &usaSrc[1], &usaSrc[2]);

            // put the values into range
            for (lCom = 0; lCom < lNCom; lCom++)
             {
              if (usaSrc[lCom] < laLowerBound[lCom])
                usaSrc[lCom] = laLowerBound[lCom];
              if (usaSrc[lCom] > laUpperBound[lCom])
                usaSrc[lCom] = laUpperBound[lCom];
             }

            // pixel in original data array
            lRCSrc = (lRowSrc * lNCol + lColSrc) * lNCom;

            for (lCom = 0; lCom < lNCom; lCom++)
             {
              // put these values into destination image
              uspDst[lRCSrc+lCom] = usaSrc[lCom];
             }

            // advance to next col
            lColSrc++;
           }
         }
       }

      // advance to next row
      lRowSrc++;
     }
   }
  else
   {
    lRowSrcLast = -1;
    lColSrc = pssp->lSurroundLeft;
    for (lRowDst = 0; lRowDst < psap->lNRow; lRowDst++)
     {
      if (psap->lpLeftCol[lRowDst] >= psap->lOutsideTemplate)
       {
        lRowSrc = pssp->lSurroundTop - psap->lOutsideTemplate +
                  psap->lpLeftCol[lRowDst];

        if (bForHistory)
         {
          // create an entry in the history list
          if (lRowSrc == lRowSrcLast)
           {
            // keep using the same pixel region
            rectPixelRegion.right = lColSrc;
           }
          else
           {
            // save old pixel region
            if (lRowSrcLast != -1)
             {
              prlpOriginalValues->Add (rectPixelRegion, uspSrc[0],
                                       lNCol, lNRow, lNCom);
             }
            // need to start a new pixel region
            lRowSrcLast = lRowSrc;
            rectPixelRegion.top = lRowSrc;
            rectPixelRegion.bottom = lRowSrc + psap->lWidth - 1;
            rectPixelRegion.left = lColSrc;
            rectPixelRegion.right = lColSrc;
           }
         }
        else
         {
          // create the destination image
          for (lWidth = 0; lWidth < psap->lWidth; lWidth++)
           {
            lWarp = lWidth + psap->lOutsideTemplate;
            // the RefineVal's are in RGB
            fDst0 = psap->wpWarp[lRowDst][lWarp].faRefineVal[0];
            fDst1 = psap->wpWarp[lRowDst][lWarp].faRefineVal[1];
            fDst2 = psap->wpWarp[lRowDst][lWarp].faRefineVal[2];

            if (fDst0 < 0.)
              fDst0 = 0.;
            else if (fDst0 > 65535.)
              fDst0 = 65535.;

            if (fDst1 < 0.)
              fDst1 = 0.;
            else if (fDst1 > 65535.)
              fDst1 = 65535.;

            if (fDst2 < 0.)
              fDst2 = 0.;
            else if (fDst2 > 65535.)
              fDst2 = 65535.;

            usDst0 = fDst0 + 0.5;
            usDst1 = fDst1 + 0.5;
            usDst2 = fDst2 + 0.5;

            // use colorspace convert
            cscpGUIToNative->ConvertOneValue (usDst0, usDst1, usDst2,
                  &usaSrc[0], &usaSrc[1], &usaSrc[2]);

            // put the values into range
            for (lCom = 0; lCom < lNCom; lCom++)
             {
              if (usaSrc[lCom] < laLowerBound[lCom])
                usaSrc[lCom] = laLowerBound[lCom];
              if (usaSrc[lCom] > laUpperBound[lCom])
                usaSrc[lCom] = laUpperBound[lCom];
             }

            // pixel in original data array
            lRCSrc = (lRowSrc * lNCol + lColSrc) * lNCom;

            for (lCom = 0; lCom < lNCom; lCom++)
             {
              // put these values into destination image
              uspDst[lRCSrc+lCom] = usaSrc[lCom];
             }

            // advance to next row
            lRowSrc++;
           }
         }
       }

      // advance to next col
      lColSrc++;
     }
   }

  if (bForHistory)
   {
    // save the final collection of original values
    prlpOriginalValues->Add (rectPixelRegion, uspSrc[0],
                             lNCol, lNRow, lNCom);
   }

  return;
}  /* RunThroughWarp */

void CAlgorithmPS::NormalizeTemplate ()
/*
	Copy the pssSetting template into psaAlgorithm template.  Then
	normalize.
*/
{
  long lTemplate, lIndex;

  for (lTemplate = 0; lTemplate < psaAlgorithm.lNTemplate; lTemplate++)
   {
    lIndex = lTemplate + pssSetting.lFirstTemplate;
    if (lIndex < 0)
     {
      lIndex = 0;
     }
    if (lIndex >= pssSetting.lNTemplate)
     {
      lIndex = pssSetting.lNTemplate - 1;
     }

    psaAlgorithm.fpTemplate[lTemplate] = pssSetting.faTemplate[lIndex];
   }

   #ifdef TTT
{   // TTT
  FILE *fp;
  fp = fopen ("temp.dat", "w");
  for (int i = 0; i < psaAlgorithm.lNTemplate; i++)
   {
    fprintf (fp, "%f\n", psaAlgorithm.fpTemplate[i]);
   }
  fclose (fp);
}
#endif

  Normalize (psaAlgorithm.fpTemplate);

  #ifdef TTT
{    // TTT
  FILE *fp;
  fp = fopen ("norm.dat", "w");
  for (int i = 0; i < psaAlgorithm.lNTemplate; i++)
   {
    fprintf (fp, "%f\n", psaAlgorithm.fpTemplate[i]);
   }
  fclose (fp);
}
#endif

  return;
}  /* NormalizeTemplate */

void CAlgorithmPS::NormalizeImage (long lRow, long lCol)
/*
	Copy the image data used for tracking into psaAlgorithm profile.  Then
	normalize.
*/
{
  long lTemplate;
  MTI_UINT16 *uspData;

  uspData = psaAlgorithm.uspYRGB + ((lRow * psaAlgorithm.lNCol + lCol) * 4);

  switch (pssSetting.lPrincipleComponent)
   {
    case SELECT_SIGNAL_Y:
      uspData += 0;
    break;
    case SELECT_SIGNAL_RED:
      uspData += 1;
    break;
    case SELECT_SIGNAL_GREEN:
      uspData += 2;
    break;
    case SELECT_SIGNAL_BLUE:
      uspData += 3;
    break;
   }

  for (lTemplate = 0; lTemplate < psaAlgorithm.lNTemplate; lTemplate++)
   {
    psaAlgorithm.fpProfile[lTemplate] = (float)(*uspData);
    uspData += 4;
   }

  Normalize (psaAlgorithm.fpProfile);

  return;
}  /* NormalizeData */

void CAlgorithmPS::Normalize (float *fp)
/*
	This function removes edge effects and forces the resulting
	values to have mean 0 and std err 1
*/
{
  long lCnt, lSumLeft, lSumRight;
  float fPosLeft, fPosRight, fSlope, fConstant, fSum, fAve, fStdDev;
  long *lpLeft, *lpMiddle, *lpRight;
  PERSISTENT_SCRATCH_ALGORITHM *psap;
  double daXpX[2][2], daXpY[2];
  float faB[2];
  long lOutside, lTemplate;
  double dX, dY;
  float fX, fY;

  psap = &psaAlgorithm;


/*
	Remove the edge effects

		MiddleAdj = B0 + B1*Middle

	Use least squares:

		B = (X'X)^(-1)*X'Y
*/


  daXpX[0][0] = 0.;
  daXpX[0][1] = 0.;
  daXpX[1][0] = 0.;
  daXpX[1][1] = 0.;
  daXpY[0] = 0.;
  daXpY[1] = 0.;

  // the pixels to the left of the scratch
  for (lOutside = 0; lOutside < psap->lOutsideTemplate; lOutside++)
   {
    dY = fp[lOutside];
    dX = (double)lOutside;

    daXpX[0][0] += 1.;
    daXpX[0][1] += dX;
    daXpX[1][0] += dX;
    daXpX[1][1] += (dX * dX);

    daXpY[0] += dY;
    daXpY[1] += (dX * dY);
   }

  // the pixels to the right of the scratch
  for (lOutside = psap->lNTemplate-psap->lOutsideTemplate; lOutside <
        psap->lNTemplate; lOutside++)
   {
    dY = fp[lOutside];
    dX = (double)lOutside;

    daXpX[0][0] += 1;
    daXpX[0][1] += dX;
    daXpX[1][0] += dX;
    daXpX[1][1] += (dX * dX);

    daXpY[0] += dY;
    daXpY[1] += (dX * dY);
   }


  // determine Least Squares Estimate
  LeastSquares (daXpX, daXpY, faB);


  // remove the edge effects
  for (lTemplate = 0; lTemplate < psap->lNTemplate; lTemplate++)
   {
    fX = (float)lTemplate;
    fY = faB[0] + faB[1] * fX;

    fp[lTemplate] -= fY;
   }

  // normalize the signal so it has mean 0, std 1
  fSum = 0.;
  for (lTemplate = 0; lTemplate < psap->lNTemplate; lTemplate++)
   {
    fSum += fp[lTemplate];
   }

  fAve = fSum / (float)psap->lNTemplate;

  for (lTemplate = 0; lTemplate < psap->lNTemplate; lTemplate++)
   {
    fp[lTemplate] -= fAve;
   }

  fSum = 0.;
  for (lTemplate = 0; lTemplate < psap->lNTemplate; lTemplate++)
   {
    fSum += fp[lTemplate] * fp[lTemplate];
   }

  fStdDev = sqrt (fSum / (float)(psap->lNTemplate-1));

  if (fStdDev)
   {
    for (lTemplate = 0; lTemplate < psap->lNTemplate; lTemplate++)
     {
      fp[lTemplate] /= fStdDev;
     }
   }

  return;
}  /* Normalize */

void CAlgorithmPS::FilterData ()
{
  long lScratchOutsideWidth, lRow, lCol, lNLeft, lNRight, lCnt,
		lCStart, lCStop;
  long **lpMotionData;
  float fValue, *fpF, **fpFilter, *fpProfile, *fpTemplate, fSum, *fpC;
  PERSISTENT_SCRATCH_ALGORITHM *psap;

  psap = &psaAlgorithm;

  fpProfile = psap->fpProfile;
  fpTemplate = psap->fpTemplate;
  fpC = psap->fpCorr;

  #ifdef TTT
{ // TTT
  FILE *fp = fopen ("ii.dat", "w");
  for (int i = 0; i < psap->lNRow; i++)
   {
    for (int j = 0; j < psap->lNCol; j++)
     {
      fprintf (fp, "%d ", psap->uspYRGB[(i*psap->lNCol + j)*4]);
     }
    fprintf (fp, "\n");
   }
  fclose (fp);
}
#endif

  for (lRow = 0; lRow < psap->lNRow; lRow++)
   {
    for (lCol = 0; lCol < psap->lNCol - psap->lNTemplate; lCol++)
     {
      NormalizeImage (lRow, lCol);

      fSum = 0.;
      for (lCnt = 0; lCnt < psap->lNTemplate; lCnt++)
       {
        fSum += fpProfile[lCnt] * fpTemplate[lCnt];
       }

      fValue = fSum / (float)psap->lNTemplate;
      *fpC++ = fValue;
     }

    for ( ; lCol < psap->lNCol; lCol++)
     {
      *fpC++ = -100.;
     }
   }

   #ifdef TTT

{  // TTT
  FILE *fp = fopen ("corr.dat", "w");
  int ij=0;
  for (int i = 0; i < psap->lNRow; i++)
   {
    for (int j = 0; j < psap->lNCol; j++)
     {
      fprintf (fp, "%f ", psap->fpCorr[ij++]);
     }
    fprintf (fp, "\n");
   }
  fclose (fp);
}
#endif

  return;
}  /* FilterData */

void CAlgorithmPS::TrackScratch ()
{
  long lBestCol, lNRow, lNWidth, lRow, lW;
  float fScore;
  long *lpRepMask;
  DYNAMIC_PROGRAM **dpp;

/*
	Perform the dynamic programming 
*/

  DynamicProgram (&fScore, &lBestCol);

/*
	Back-track to locate scratch
*/

  dpp = psaAlgorithm.dppScratch;
  for (lRow = psaAlgorithm.lNRow - 1; lRow >= 0; lRow--)
   {
    psaAlgorithm.lpLeftCol[lRow] = lBestCol;
    
    lBestCol = dpp[lRow][lBestCol].lPrevious;
   }

  return;
}  /* TrackScratch */

void CAlgorithmPS::DynamicProgram (float *fpBestScore, long *lpBestCol)
{
  long lNRowLocal, lNColLocal, lRow, lCol, lDeviation, lC, lBestCol,
		lScratchDeviation;
  float fNegDeviation, fPosDeviation,
		fBestNegDeviation, fBestPosDeviation, fMaxSkew, fMaxWiggle,
		fSkewFactor, fWiggleThresh, fSkewHistory, fWiggleHistory,
		fWiggle, *fpScore, fTransition, fScore, fBestScore;
  long *lpRM;
  long lNWidth;
  DYNAMIC_PROGRAM **dpp, *dppOld, *dppNew;
  float *fpCorr;

  lNRowLocal = psaAlgorithm.lNRow;
  lNColLocal = psaAlgorithm.lNCol;
  lNWidth = psaAlgorithm.lWidth;

/*
	Determine the skew parameters.

	We will tolerate fMaxSkew positive or negative deviations in
	fSkewHistory.  Thus, we will tolerate 1 positive or negative
	deviation each (fSkewHistory/fMaxSkew) steps.

	We calculate two skew values 
		fPosSkew[k] = lPosDeviation[k] + x * lPosDeviation[k-1];
		fNegSkew[k] = lNegDeviation[k] + x * lNegDeviation[k-1];

	We want x^(fSkewHistory/fMaxSkew) = (SKEW_THRESH - 1)
*/

  fMaxSkew = psaAlgorithm.fMaxSkew * (float)lNWidth;
  fSkewHistory = HISTORY_SKEW * (float)lNWidth;

  if (fMaxSkew == 0.)
   {
    fSkewFactor = 1.;
    lScratchDeviation = 0;
   }
  else
   {
    fSkewFactor = exp(log(SKEW_THRESH-1.) / (fSkewHistory/fMaxSkew));
    lScratchDeviation = SCRATCH_DEVIATION;
   }


/*
	Determine the wiggle parameters

	We tolerate fMaxWiggle deviations in fWiggleHistory steps.  This
	means we tolerate 1 wiggle deviation is (fWiggleHistory/fMaxWiggle)
	steps.

		fWiggle[k] = min(fPosSkew[k], fNegSkew[k])
*/

  fMaxWiggle = psaAlgorithm.fMaxWiggle * (float)lNWidth;
  fWiggleHistory = HISTORY_WIGGLE * (float)lNWidth;

  if (fMaxWiggle == 0.)
    fWiggleThresh = 0.;
  else
    fWiggleThresh = powf(fSkewFactor, (fWiggleHistory/fMaxWiggle));

  dpp = psaAlgorithm.dppScratch;

/*
	Initialize the first row
*/

  dppNew = dpp[0];
  fpCorr = psaAlgorithm.fpCorr;
  for (lCol = 0; lCol < lNColLocal; lCol++)
   {
    dppNew->lPrevious = 0;
    dppNew->fScore = *fpCorr++;
    dppNew->fPosDeviation = 0;
    dppNew->fNegDeviation = 0;
    dppNew++;
   }

/*
	Run through the rest of the rows
*/

  for (lRow = 1; lRow < lNRowLocal; lRow++)
   {
    dppNew = dpp[lRow];
    fpCorr = psaAlgorithm.fpCorr + (lRow * psaAlgorithm.lNCol);
    for (lCol = 0; lCol < lNColLocal; lCol++)
     {
      fTransition = *fpCorr++;
      dppOld = &dpp[lRow-1][lCol] - lScratchDeviation;
      fBestScore = -INFINITY_FLOAT;
      lBestCol = -INFINITY_LONG;
      for (lDeviation = -lScratchDeviation; lDeviation <= lScratchDeviation;
		lDeviation++)
       {
        lC = lCol + lDeviation;
        if (lC >= 0 && lC < lNColLocal)
         {
          if (lDeviation < 0)
           {
            fNegDeviation = fSkewFactor * dppOld->fNegDeviation - lDeviation;
            fPosDeviation = fSkewFactor * dppOld->fPosDeviation;
           }
          else
           {
            fNegDeviation = fSkewFactor * dppOld->fNegDeviation;
            fPosDeviation = fSkewFactor * dppOld->fPosDeviation + lDeviation;
           }

          if (fNegDeviation < fPosDeviation)
            fWiggle = fNegDeviation;
          else
            fWiggle = fPosDeviation;

          if (fWiggle <= fWiggleThresh && fNegDeviation <= SKEW_THRESH &&
		fPosDeviation <= SKEW_THRESH)
           {
            fScore = fTransition + dppOld->fScore;
            if (fScore > fBestScore)
             {
              fBestScore = fScore;
              lBestCol = lC;
              fBestNegDeviation = fNegDeviation;
              fBestPosDeviation = fPosDeviation;
             }
           }
         }
        dppOld++;
       }
      dppNew->lPrevious = lBestCol;
      dppNew->fScore = fBestScore;
      dppNew->fNegDeviation = fBestNegDeviation;
      dppNew->fPosDeviation = fBestPosDeviation;
      dppNew++;
     }
   }

/*
	Now determine the best score and best terminal location
*/

  *fpBestScore = -INFINITY_FLOAT;
  *lpBestCol = -1;

  dppNew = dpp[lNRowLocal - 1];
  for (lCol = 0; lCol < lNColLocal; lCol++)
   {
    if (dppNew->fScore > *fpBestScore)
     {
      *fpBestScore = dppNew->fScore;
      *lpBestCol = lCol;
     }
    dppNew++;
   }

  return;
}  /* DynamicProgram */

void CAlgorithmPS::FreeGeneral ()
{
  delete ifpNative;
  ifpNative = 0;
  delete ifpGUI;
  ifpGUI = 0;

// these guys don't belong to CAlgorithm
// & must be deleted by ScratchTool - KT
//
//  delete cscpNativeToGUI;
//  cscpNativeToGUI = 0;
//  delete cscpGUIToNative;
//  cscpGUIToNative = 0;

  return;
}  /* FreeGeneral */

void CAlgorithmPS::FreeAlgorithm ()
{
  long lRow, lComponent;
  PERSISTENT_SCRATCH_ALGORITHM *psap;

  psap = &psaAlgorithm;

  free (psap->lpLeftCol);
  psap->lpLeftCol = NULL;

  free (psap->uspYRGB);
  psap->uspYRGB =  NULL;

  free (psap->fpTemplate);
  psap->fpTemplate = NULL;
  free (psap->fpProfile);
  psap->fpProfile = NULL;
  free (psap->fpCorr);
  psap->fpCorr = NULL;

  if (psaAlgorithm.dppScratch)
   {
    for (lRow = 0; lRow < psap->lNRow; lRow++)
     {
      free (psap->dppScratch[lRow]);
      psap->dppScratch[lRow] = NULL;
     }
    free (psap->dppScratch);
    psap->dppScratch = NULL;
   }

  if (psap->wpWarp)
   {
    for (lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
     {
      free (psap->wpWarp[lRow]);
      psap->wpWarp[lRow] = NULL;
     }
    free (psap->wpWarp);
    psap->wpWarp = NULL;
   }

  return;
}  /* FreeAlgorithm */

void CAlgorithmPS::RepairScratch ()
{
  int iRet;
  long lComponent, lWidth;


  // straighten the scratch and assign a rough adjustment value
  WarpSides ();

  // refine the adjustment and apply
  ApplyAdjustment ();

  return;
}  /* RepairScratch */

void CAlgorithmPS::WarpSides ()
/*
	The WARP struct straightens the scratch so it runs from top to
	bottom.
*/
{
  long lRow, lWidth, *lpLeftCol, lNTemplate, lOutsideTemplate,
                lTemplate, lCol, lRowColCom, lCom, lCnt, lIndex,
                lNRowLocal, lNColLocal;
  MTI_UINT16 *uspYRGB;
  double daXpX[2][2], daXpY[2], dX, dY;
  float faB[2];

  lpLeftCol = psaAlgorithm.lpLeftCol;
  lNTemplate = psaAlgorithm.lNTemplate;
  lOutsideTemplate = psaAlgorithm.lOutsideTemplate;
  lNRowLocal = psaAlgorithm.lNRow;
  lNColLocal = psaAlgorithm.lNCol;
  uspYRGB = psaAlgorithm.uspYRGB;


  // make a copy of the original values
  for (lRow = 0; lRow < lNRowLocal; lRow++)
  //if (lpLeftCol[lRow] != NULL_LEFT_COL)
  if (lpLeftCol[lRow] >= lOutsideTemplate)
   {
    lCol = lpLeftCol[lRow] - lOutsideTemplate;
    for (lTemplate = 0; lTemplate < lNTemplate; lTemplate++)
     {
      lRowColCom = (lRow * lNColLocal + lCol) * 4 + 1;

      psaAlgorithm.wpWarp[lRow][lTemplate].faOrigVal[0] = uspYRGB[lRowColCom++];
      psaAlgorithm.wpWarp[lRow][lTemplate].faOrigVal[1] = uspYRGB[lRowColCom++];
      psaAlgorithm.wpWarp[lRow][lTemplate].faOrigVal[2] = uspYRGB[lRowColCom++];

      lCol++;
     }
   }

  // use Least Squares to calculate the rough value for each pixel in
  // the scratch

  for (lCom = 0; lCom < 3; lCom++)
  for (lRow = 0; lRow < lNRowLocal; lRow++)
  if (lpLeftCol[lRow] >= lOutsideTemplate)
   {
    daXpX[0][0] = 0.;
    daXpX[0][1] = 0.;
    daXpX[1][0] = 0.;
    daXpX[1][1] = 0.;

    daXpY[0] = 0.;
    daXpY[1] = 0.;

    for (lCnt = 0; lCnt < lOutsideTemplate; lCnt++)
     {
      // pixels to left of scratch
      lIndex = lCnt;
      dX = (double)lIndex;
      dY = psaAlgorithm.wpWarp[lRow][lIndex].faOrigVal[lCom];
      daXpX[0][0] += 1;
      daXpX[0][1] += dX;
      daXpX[1][0] += dX;
      daXpX[1][1] += (dX * dX);

      daXpY[0] += dY;
      daXpY[1] += (dX * dY);

      // pixels to right of scratch
      lIndex = (lNTemplate-1) - lCnt;
      dX = (double)lIndex;
      dY = psaAlgorithm.wpWarp[lRow][lIndex].faOrigVal[lCom];
      daXpX[0][0] += 1;
      daXpX[0][1] += dX;
      daXpX[1][0] += dX;
      daXpX[1][1] += (dX * dX);

      daXpY[0] += dY;
      daXpY[1] += (dX * dY);
     }

    // use least squares to estimate RoughVal for each pixel in template
    LeastSquares (daXpX, daXpY, faB);

    for (lTemplate = 0; lTemplate < lNTemplate; lTemplate++)
     {
      psaAlgorithm.wpWarp[lRow][lTemplate].faRoughVal[lCom] = faB[0] + faB[1] *
		(float)lTemplate;

      psaAlgorithm.wpWarp[lRow][lTemplate].faRoughAdj[lCom] =
		psaAlgorithm.wpWarp[lRow][lTemplate].faRoughVal[lCom] -
			psaAlgorithm.wpWarp[lRow][lTemplate].faOrigVal[lCom];
     }
   }

  return;
}  /* WarpSides */

void CAlgorithmPS::LeastSquares (double daXpX[2][2], double daXpY[2],
		float faB[2])
{
  long lCnt0, lCnt1;
  double dDet, daInvXpX[2][2], dSum;

/*
	Compute the inverse of XpX
*/

  dDet = daXpX[0][0] * daXpX[1][1] - daXpX[1][0] * daXpX[0][1];
  if (dDet == 0.)
   {
/*
	Since the determinant is zero, use the model Y = B0.
	This is equivalent to setting (X'X)^-1 to [1/N 0; 0 0].
*/
    if (daXpX[0][0] == 0.)
     {
      // no points involved in this least squares estimate
      faB[0] = 0.;
      faB[1] = 0.;
      return;
     }

    daInvXpX[0][0] = 1./daXpX[0][0];
    daInvXpX[0][1] = 0.;
    daInvXpX[1][0] = 0.;
    daInvXpX[1][1] = 0.;
   }
  else
   {
    daInvXpX[0][0] = daXpX[1][1] / dDet;
    daInvXpX[0][1] = -daXpX[0][1] / dDet;
    daInvXpX[1][0] = -daXpX[1][0] / dDet;
    daInvXpX[1][1] = daXpX[0][0] / dDet;
   }

/*
	Compute least squares estimates.
*/

  for (lCnt0 = 0; lCnt0 < 2; lCnt0++)
   {
    dSum = 0.;
    for (lCnt1 = 0; lCnt1 < 2; lCnt1++)
     {
      dSum += daInvXpX[lCnt0][lCnt1] * daXpY[lCnt1];
     }
    faB[lCnt0] = (float)dSum;
   }

  return;
}  /* LeastSquares */

void CAlgorithmPS::ApplyAdjustment ()
{
  long lRow, lRStart, lRStop, lR, lWarp;
  float fRefineAdj, fNewVal, fSumAdj;
  long lBackgroundRow, lWidth, lNWidth, lCom;

  lBackgroundRow = pssSetting.lBackgroundPixel;
  lNWidth = psaAlgorithm.lWidth;

/*
	Run through each row and calculate the refined adjustment from
	the coarse value.  Then apply the refined adjustment.
*/

  for (lRow = 0; lRow < psaAlgorithm.lNRow; lRow++)
  //if (psaAlgorithm.lpLeftCol[lRow] != NULL_LEFT_COL)
  if (psaAlgorithm.lpLeftCol[lRow] >= psaAlgorithm.lOutsideTemplate)
   {
    lRStart = lRow - (lBackgroundRow / 2);
    if (lRStart < 0) lRStart = 0;
    lRStop = lRStart + lBackgroundRow;
    if (lRStop >= psaAlgorithm.lNRow)
     {
      lRStop = psaAlgorithm.lNRow - 1;
      lRStart = lRStop - lBackgroundRow;
      if (lRStart < 0)
        lRStart = 0;
     }

    for (lWidth = 0; lWidth < lNWidth; lWidth++)
     {
      lWarp = lWidth + psaAlgorithm.lOutsideTemplate;
      for (lCom = 0; lCom < 3; lCom++)
       {
        fSumAdj = 0.;
        for (lR = lRStart; lR < lRStop; lR++)
         {
          fSumAdj += psaAlgorithm.wpWarp[lR][lWarp].faRoughAdj[lCom];
         }

        fRefineAdj = fSumAdj / (float)(lRStop - lRStart);

        // if we are not supposed to process this signal, set the RefineAdj
        // to zero

        switch (lCom)
         {
          case 0:
            if ((pssSetting.lComponentMask & MODIFY_SIGNAL_RED) == 0)
              fRefineAdj = 0.;
          break;
          case 1:
            if ((pssSetting.lComponentMask & MODIFY_SIGNAL_GREEN) == 0)
              fRefineAdj = 0.;
          break;
          case 2:
            if ((pssSetting.lComponentMask & MODIFY_SIGNAL_BLUE) == 0)
              fRefineAdj = 0.;
          break;
         }

        psaAlgorithm.wpWarp[lRow][lWarp].faRefineVal[lCom] =
		psaAlgorithm.wpWarp[lRow][lWarp].faOrigVal[lCom] + fRefineAdj;
       }
     }
   }

  return;
}  /* ApplyAdjustment */
