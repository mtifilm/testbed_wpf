//---------------------------------------------------------------------------

#ifndef ScratchHelpUnitH
#define ScratchHelpUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "SHDocVw_OCX.h"
#include <ComCtrls.hpp>
#include <OleCtrls.hpp>
#include <string>
using std::string;
//---------------------------------------------------------------------------
class TScratchHelpForm : public TForm
{
__published:	// IDE-managed Components
    TTabControl *HelpTabControl;
    TCppWebBrowser *HTMLHelpPage;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall HelpTabControlChange(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:	// User declarations
    bool ShowFKeys;
    void ShowPage();
public:		// User declarations
    __fastcall TScratchHelpForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TScratchHelpForm *ScratchHelpForm;
//---------------------------------------------------------------------------
#endif
