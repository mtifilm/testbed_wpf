//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SidecarUnit.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TSidecarWindow *SidecarWindow;
//---------------------------------------------------------------------------

__fastcall TSidecarWindow::TSidecarWindow(
   TComponent* Owner,
   TMouseEvent imageMouseDownHandler)
   : TForm(Owner), externalImageMouseDownHandler(imageMouseDownHandler)
{
   RGraphImage->ControlStyle << csOpaque;
   GGraphImage->ControlStyle << csOpaque;
   BGraphImage->ControlStyle << csOpaque;
   YGraphImage->ControlStyle << csOpaque;
   MagnifierImage->ControlStyle << csOpaque;
   SidecarImagePanel->ControlStyle << csOpaque;
   ControlStyle << csOpaque;

   RGraphImage->OnMouseDown = imageMouseDownHandler;
   GGraphImage->OnMouseDown = imageMouseDownHandler;
   BGraphImage->OnMouseDown = imageMouseDownHandler;
   YGraphImage->OnMouseDown = imageMouseDownHandler;
   MagnifierImage->OnMouseDown = imageMouseDownHandler;
}
//---------------------------------------------------------------------------

__fastcall TSidecarWindow::~TSidecarWindow(void)
{
}
//---------------------------------------------------------------------------

