object SidecarWindow: TSidecarWindow
  Left = 192
  Top = 132
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Sidecar'
  ClientHeight = 334
  ClientWidth = 198
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    198
    334)
  PixelsPerInch = 96
  TextHeight = 13
  object SidecarImagePanel: TColorPanel
    Left = 0
    Top = 0
    Width = 198
    Height = 334
    Anchors = []
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clBlack
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object MagnifierImage: TImage
      Left = 2
      Top = 266
      Width = 192
      Height = 64
    end
    object RGraphImage: TImage
      Left = 2
      Top = 2
      Width = 192
      Height = 64
    end
    object GGraphImage: TImage
      Left = 2
      Top = 68
      Width = 192
      Height = 64
    end
    object BGraphImage: TImage
      Left = 2
      Top = 134
      Width = 192
      Height = 64
    end
    object YGraphImage: TImage
      Left = 2
      Top = 200
      Width = 192
      Height = 64
    end
  end
end
