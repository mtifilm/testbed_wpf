// ScratchTool.cpp: implementation of the CScratch class.

#include "ScratchTool.h"

#include "AlgorithmPS.h"
#include "ColorSpaceConvert.h"
#include "HRTimer.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "JobManager.h"
#include "Magnifier.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "PDL.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"

//#include <priority_queue>
//using std::priority_queue;

#ifdef __BORLANDC__
#include "ScratchGUIWin.h"
#include <windows.h>
#else
#include "GuiSgi/ScratchGuiSgi.h"
#endif

namespace {
   const int LESS_WIDTH_DELTA(0);   // Really 1/2 !!
   const int NORMAL_WIDTH_DELTA(1);
   const int MORE_WIDTH_DELTA(2);
   const int LESS_SHIFT(1);
   const int NORMAL_SHIFT(2);
   const int MORE_SHIFT(4);
};

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 ScratchNumber = SCRATCH_TOOL_NUMBER;

// ----------------------------------------------------------------------------
// Scratch Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem ScratchDefaultConfigItems[] =
{
// Scratch Tool Command                        Modifiers        Action
//                                              + Key
// ----------------------------------------------------------------------------
//### - Eliminated per Larry's 2/25/2014 e-mail attachment
// ----------------------------------------------------------------------------
 { EXEC_BUTTON_PREVIEW_FRAME,                " D                KeyDown    " },
 { EXEC_BUTTON_PREVIEW_ALL,                  " Shift+D          KeyDown    " },
 { EXEC_BUTTON_RENDER_FRAME,                 " G                KeyDown    " },
 { EXEC_BUTTON_RENDER_ALL,                   " Shift+G          KeyDown    " },
 { EXEC_BUTTON_STOP,                         " Ctrl+Space       KeyDown    " },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              " Space            KeyDown    " },
 { EXEC_BUTTON_SET_RESUME_TC,                " Return           KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL,                  " ,                KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL,                  " Shift+,          KeyDown    " },
//### { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       " Shift+,          KeyDown    " },

 { SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH,    " LButton           ButtonDown" },
 { SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG,      " LButton           ButtonUp  " },
 { SCRATCH_CMD_START_STRETCH,                " Shift+LButton     ButtonDown" },
 { SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH, " Shift+LButton     ButtonUp  " },
 { SCRATCH_CMD_WARP_THEN_DRAG_MAG,           " Ctrl+LButton      ButtonDown" },
 { SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG,      " Ctrl+LButton      ButtonUp  " },
 { SCRATCH_CMD_START_STRETCH,                " Alt+LButton       ButtonDown" },
 { SCRATCH_CMD_END_STRETCH_THEN_AUTOFIX,     " Alt+LButton       ButtonUp  " },
 { SCRATCH_CMD_START_STRETCH,                " Alt+Shift+LButton ButtonDown" },
 { SCRATCH_CMD_SNAP_END_STRETCH_THEN_AUTOFIX," Alt+Shift+LButton ButtonUp  " },

 { SCRATCH_CMD_REJECT_FIX,                   " A                KeyDown    " },
 { SCRATCH_CMD_TOGGLE_PROVISIONAL,           " T                KeyDown    " },
 { SCRATCH_CMD_TOGGLE_RETICLES,              " Shift+T          KeyDown    " },
 { SCRATCH_CMD_TOGGLE_SIDECAR,               " Ctrl+T           KeyDown    " },
 { SCRATCH_CMD_ACCEPT_FIX,                   " Ctrl+G           KeyDown    " },
//### { SCRATCH_CMD_EXECUTE_PDL,                  " Shift+,          KeyDown    " },

 { SCRATCH_CMD_TOGGLE_DENSITY,               " 1                KeyDown    " },
//### { SCRATCH_CMD_TOGGLE_MOVEMENT,              " 2                KeyDown    " },
//### { SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP, " 3                KeyDown    " },
//### { SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP,   " Shift+3          KeyDown    " },
//### { SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP,       " 4                KeyDown    " },
//### { SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW,      " 5                KeyDown    " },
//### { SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER,   " 6                KeyDown    " },
//### { SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER,        " 8                KeyDown    " },
//### { SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF,       " Shift+8          KeyDown    " },
//### { SCRATCH_CMD_RESET_BLEND_SLIDER,           " Ctrl+8           KeyDown    " },

 { SCRATCH_CMD_WIDEN_LIMITS_NORMAL,          " Up               KeyDown    " },
 { SCRATCH_CMD_WIDEN_LIMITS_MORE,            " Shift+Up         KeyDown    " },
 { SCRATCH_CMD_WIDEN_LIMITS_LESS,            " Ctrl+Up          KeyDown    " },
 { SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL,        " Down             KeyDown    " },
 { SCRATCH_CMD_SQUEEZE_LIMITS_MORE,          " Shift+Down       KeyDown    " },
 { SCRATCH_CMD_SQUEEZE_LIMITS_LESS,          " Ctrl+Down        KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL,     " Left             KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE,       " Shift+Left       KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS,       " Ctrl+Left        KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL,    " Right            KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE,      " Shift+Right      KeyDown    " },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS,      " Ctrl+Right       KeyDown    " },
//### { SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT,         " Ctrl+Left        KeyDown    " },
//### { SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT,        " Ctrl+Shift+Left  KeyDown    " },
//### { SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT,        " Ctrl+Shift+Right KeyDown    " },
//### { SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT,       " Ctrl+Right       KeyDown    " },
//### { SCRATCH_CMD_AUTOLOCATE_SCRATCH,           " Ctrl+Down        KeyDown    " },
//### { SCRATCH_CMD_RESET_SCRATCH_LIMITS,         " Ctrl+Up          KeyDown    " },

};

static CUserInputConfiguration *ScratchDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(ScratchDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                              ScratchDefaultConfigItems);

// ----------------------------------------------------------------------------
// Scratch Tool Command Table

static CToolCommandTable::STableEntry ScratchCommandTableEntries[] =
{
// Scratch Tool Command                       Scratch Tool Command String
// ----------------------------------------------------------------------------
 { SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH,    "SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH" },
 { SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG,      "SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG" },
 { SCRATCH_CMD_START_STRETCH,                "SCRATCH_CMD_START_STRETCH" },
 { SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH, "SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH" },
 { SCRATCH_CMD_WARP_THEN_DRAG_MAG,           "SCRATCH_CMD_WARP_THEN_DRAG_MAG" },

 { SCRATCH_CMD_TOGGLE_PROVISIONAL,           "SCRATCH_CMD_TOGGLE_PROVISIONAL" },
 { SCRATCH_CMD_TOGGLE_RETICLES,              "SCRATCH_CMD_TOGGLE_RETICLES" },
 { SCRATCH_CMD_TOGGLE_SIDECAR,               "SCRATCH_CMD_TOGGLE_SIDECAR" },

 { SCRATCH_CMD_PREVIEW_FRAME,                "SCRATCH_CMD_PREVIEW_FRAME" },
 { SCRATCH_CMD_PREVIEW_ALL,                  "SCRATCH_CMD_PREVIEW_ALL" },
 { SCRATCH_CMD_RENDER_FRAME,                 "SCRATCH_CMD_RENDER_FRAME" },
 { SCRATCH_CMD_RENDER_ALL,                   "SCRATCH_CMD_RENDER_ALL" },
 { SCRATCH_CMD_REJECT_FIX,                   "SCRATCH_CMD_REJECT_FIX" },
 { SCRATCH_CMD_ACCEPT_FIX,                   "SCRATCH_CMD_ACCEPT_FIX" },
 { SCRATCH_CMD_ADD_EVENT_TO_PDL,             "SCRATCH_CMD_ADD_EVENT_TO_PDL" },
 { SCRATCH_CMD_EXECUTE_PDL,                  "SCRATCH_CMD_EXECUTE_PDL" },
 { SCRATCH_CMD_STOP_RENDER,                  "SCRATCH_CMD_STOP_RENDER" },
 { SCRATCH_CMD_PAUSE_OR_RESUME_RENDER,       "SCRATCH_CMD_PAUSE_OR_RESUME_RENDER" },

 { SCRATCH_CMD_FOCUS_ON_MOVEMENT_GROUP,      "SCRATCH_CMD_FOCUS_ON_MOVEMENT_GROUP" },
 { SCRATCH_CMD_FOCUS_ON_DETECT_GROUP,        "SCRATCH_CMD_FOCUS_ON_DETECT_GROUP" },
 { SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP,       "SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP" },
 { SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW,      "SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW" },
 { SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER,   "SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER" },
 { SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER,        "SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER" },
 { SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF,       "SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF" },
 { SCRATCH_CMD_RESET_BLEND_SLIDER,           "SCRATCH_CMD_RESET_BLEND_SLIDER" },

 { SCRATCH_CMD_WIDEN_LIMITS_NORMAL,          "SCRATCH_CMD_WIDEN_LIMITS_NORMAL" },
 { SCRATCH_CMD_WIDEN_LIMITS_MORE,            "SCRATCH_CMD_WIDEN_LIMITS_MORE" },
 { SCRATCH_CMD_WIDEN_LIMITS_LESS,            "SCRATCH_CMD_WIDEN_LIMITS_LESS" },
 { SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL,        "SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL" },
 { SCRATCH_CMD_SQUEEZE_LIMITS_MORE,          "SCRATCH_CMD_SQUEEZE_LIMITS_MORE" },
 { SCRATCH_CMD_SQUEEZE_LIMITS_LESS,          "SCRATCH_CMD_SQUEEZE_LIMITS_LESS" },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL,     "SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL" },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE,       "SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE" },
 { SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS,       "SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS" },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL,    "SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL" },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE,      "SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE" },
 { SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS,      "SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS" },
 { SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT,         "SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT" },
 { SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT,        "SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT" },
 { SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT,        "SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT" },
 { SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT,       "SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT" },
 { SCRATCH_CMD_AUTOLOCATE_SCRATCH,           "SCRATCH_CMD_AUTOLOCATE_SCRATCH" },
 { SCRATCH_CMD_RESET_SCRATCH_LIMITS,         "SCRATCH_CMD_RESET_SCRATCH_LIMITS" },

 { SCRATCH_CMD_TOGGLE_DENSITY,               "SCRATCH_CMD_TOGGLE_DENSITY" },
 { SCRATCH_CMD_TOGGLE_ORIENTATION,           "SCRATCH_CMD_TOGGLE_ORIENTATION" },
 { SCRATCH_CMD_TOGGLE_MOVEMENT,              "SCRATCH_CMD_TOGGLE_MOVEMENT" },
 { SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP,   "SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP" },
 { SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP, "SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP" },
};

static CToolCommandTable *ScratchCommandTable
   = new CToolCommandTable(sizeof(ScratchCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           ScratchCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// CScratchParameters Static Members

const string CScratchParameters::movesAttr = "Moves";
const string CScratchParameters::typeAttr = "Type";
const string CScratchParameters::typeSurface = "Surface";
const string CScratchParameters::typeDeep = "Deep";
const string CScratchParameters::orientationAttr = "Orientation";
const string CScratchParameters::orientationVert = "Vertical";
const string CScratchParameters::orientationHoriz = "Horizontal";
const string CScratchParameters::scratchRectangleAttr = "ScratchRect";
const string CScratchParameters::templateFrameAttr = "TemplateFrame";
const string CScratchParameters::limitRectangleAttr = "LimitRect";
const string CScratchParameters::magnifierRectangleAttr = "MagRect";
const string CScratchParameters::selectSignalAttr = "SelectSignal";
const string CScratchParameters::modifySignalAttr = "ModifySignal";
const string CScratchParameters::detailAttr = "Detail";

//////////////////////////////////////////////////////////////////////
// HACK ALERT! People want to be able to 'accept' (hit 'G' ) after
// running a PREVIEW 1 and have it 'Accept' the preview results without
// re-runhing the fix. So instead of doing a 'TRAINING' setup for
// Preview 1, we'll use the SINGLE FRAME setup and have it not do an
// Auto-Accept immediately after the fix, like RENDER 1 mode does.
#define DO_PREVIEW_G_KEY_HACK 1
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScratch::CScratch(const string &newToolName,
                   MTI_UINT32 **newFeatureTable)
: CToolObject(newToolName, newFeatureTable),
  toolSetupHandle(-1),
#if !DO_PREVIEW_G_KEY_HACK
  trainingToolSetupHandle(-1),
#endif
  singleFrameToolSetupHandle(-1),
  controlLockFlag(false)
{
   // the 16-bit 444 intermediate image format
   intFmt = new CImageFormat;

   // the to- and from- color space converters
   toRGBColorConverter = new CColorSpaceConvert;
   fmRGBColorConverter = new CColorSpaceConvert;
}

CScratch::~CScratch(void)
{
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the Scratch Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CScratch::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CScratch's base class, i.e., CToolObject
   // This must be done before the CScratch initializes itself
   retVal = initBaseToolObject(ScratchNumber, newSystemAPI,
                               ScratchCommandTable,
                               ScratchDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in Scratch Tool, initBaseToolObject failed "
                     << retVal);
      return retVal;
      }

   // Tool-specific initialization here

   // Create the Tool's GUI
   CreateScratchGUI();

   // Update the tool's GUI to match the tool processing initial state
   UpdateExecutionGUI(GetToolProcessingControlState(), false);

   // no stretch rectangle available yet
   scratchRectangleAvailable = false;

   // clear the stretching flag
   nowStretching = false;

   // clear the magnifier moving flag
   nowMovingMagnifier = false;

   // set the lasso mode to "rectangle"
   getSystemAPI()->setDRSLassoMode(false);

   // clear the magnifier positioning flag
   nowMovingMagnifier = false;

   // No magnifer:mouse offsets
  magnifierOffsetX = magnifierOffsetY = 0;

  // Hack to remove overlays during execution
  hidingOverlays = false;

  // Hack to make larry happy - always immediately auto-accept after 'render 1'
  AutoAcceptNextRenderFlag = false;
  NextGKeyShouldOnlyAcceptFlag = false;

  // Flag to lock out most keys during processing
  controlLockFlag = false;

   return 0;

}   // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CScratch::toolActivate(void)
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   // Just in case these did change.... could be more clever about it?
   onNewClip();
   onNewMarks();

   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   // Make sure displayed frame is clean
   getSystemAPI()->refreshFrameCached();

   JobManager jobManager;
   jobManager.SetCurrentToolCode("sc");

	getSystemAPI()->SetToolNameForUserGuide("ScratchTool");

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CScratch::toolDeactivate(void)
{
   // Get rid of any review regions that might be present on the screen
   clearPreview();

   DestroyAllToolSetups(true);

   GetToolProgressMonitor()->SetIdle();
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   // Last
   return CToolObject::toolDeactivate();
}


// ===================================================================
//
// Function:    toolHideQuery
//
// Description: Called we are going to switch to another tool -- first
//              we get to say if we allow that or not.
//
// Arguments:   None
//
// Returns:     If OK to switch, returns true, or false if there is
//              some processing going on.
//
// ===================================================================

bool CScratch::toolHideQuery(void)
{
   clearPreview();
   return true;
}


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              Scratch Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CScratch::toolShutdown(void)
{
	int retVal;

	// In case anything is pending (do we always get ToolDeactivated event?)  YES
	// acceptFixOrGOV();

	// Destroy Scratch's GUI
	DestroyScratchGUI();

	// Say we are destroyed
	GScratchTool = NULL;

	// delete converters
	delete fmRGBColorConverter;
	delete toRGBColorConverter;

	// and the 16-bit 444 intermediate image format
	delete intFmt;

	// Shutdown the Scratch Tool's base class, i.e., CToolObject.
	// This must be done after the Scratch shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
}

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// Description: Makes a new instance of the scratch tool processor
//
// Arguments:   bool *newEmergencyStopFlagPtr
//
// Returns:     new CScratchProc
//
// ===================================================================

CToolProcessor* CScratch::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   return new CScratchProc(GetToolNumber(), GetToolName(), getSystemAPI(),
                           newEmergencyStopFlagPtr, GetToolProgressMonitor());
}
//----------------------------------------------------------------------

namespace {
   inline bool isPointInRectangle(int x, int y, const RECT &rect)
   {
      if (x < rect.left || x > rect.right || y < rect.top || y > rect.bottom)
         return false;
      return true;
   };
};

// ===================================================================
//
// Function:    onToolCommand
//
// Description: StabilizerTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CScratch::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   int commandNumber = toolCommand.getCommandNumber();
   int result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                 TOOL_RETURN_CODE_PROCESSED_OK);

   TRACE_3(errout << "Scratch Tool command number " << commandNumber);

   // While processing, many commands are disabled:
   if (controlLockFlag)
   {
      switch (commandNumber)
      {
         case SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH:
         case SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG:
         case SCRATCH_CMD_START_STRETCH:
         case SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH:
         case SCRATCH_CMD_WARP_THEN_DRAG_MAG:
         case SCRATCH_CMD_END_STRETCH_THEN_AUTOFIX:
         case SCRATCH_CMD_SNAP_END_STRETCH_THEN_AUTOFIX:
         case SCRATCH_CMD_ACCEPT_FIX:
         case SCRATCH_CMD_REJECT_FIX:
         case SCRATCH_CMD_TOGGLE_PROVISIONAL:
         case SCRATCH_CMD_PREVIEW_FRAME:
         case SCRATCH_CMD_PREVIEW_ALL:
         case SCRATCH_CMD_RENDER_FRAME:
         case SCRATCH_CMD_RENDER_ALL:
         case SCRATCH_CMD_ADD_EVENT_TO_PDL:
         case SCRATCH_CMD_EXECUTE_PDL:
         case SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF:
         case SCRATCH_CMD_RESET_BLEND_SLIDER:
         case SCRATCH_CMD_WIDEN_LIMITS_NORMAL:
         case SCRATCH_CMD_WIDEN_LIMITS_MORE:
         case SCRATCH_CMD_WIDEN_LIMITS_LESS:
         case SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL:
         case SCRATCH_CMD_SQUEEZE_LIMITS_MORE:
         case SCRATCH_CMD_SQUEEZE_LIMITS_LESS:
         case SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL:
         case SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE:
         case SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS:
         case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL:
         case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE:
         case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS:
         case SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT:
         case SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT:
         case SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT:
         case SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT:
         case SCRATCH_CMD_AUTOLOCATE_SCRATCH:
         case SCRATCH_CMD_RESET_SCRATCH_LIMITS:
         case SCRATCH_CMD_TOGGLE_DENSITY:
         case SCRATCH_CMD_TOGGLE_ORIENTATION:
         case SCRATCH_CMD_TOGGLE_MOVEMENT:
         case SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP:
         case SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP:
         default:
            Beep();
            return result;

         // These are necessary!
         case SCRATCH_CMD_PAUSE_OR_RESUME_RENDER:
         case SCRATCH_CMD_STOP_RENDER:
         break;

         // These should be harmless, so allow them through
         case EXEC_BUTTON_PREVIEW_FRAME:
         case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
         case EXEC_BUTTON_PREVIEW_ALL:
         case EXEC_BUTTON_RENDER_FRAME:
         case EXEC_BUTTON_RENDER_NEXT_FRAME:
         case EXEC_BUTTON_RENDER_ALL:
         case EXEC_BUTTON_STOP:
         case EXEC_BUTTON_PAUSE_OR_RESUME:
         case EXEC_BUTTON_SET_RESUME_TC:
         case EXEC_BUTTON_CAPTURE_PDL:
         case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         case SCRATCH_CMD_TOGGLE_RETICLES:
         case SCRATCH_CMD_TOGGLE_SIDECAR:
         case SCRATCH_CMD_FOCUS_ON_DETECT_GROUP:
         case SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP:
         case SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW:
         case SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER:
         case SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER:
         break;
      }
   }

   // Dispatch switch for Scratch Commands

   switch (commandNumber)
   {
      case SCRATCH_CMD_DRAG_MAG_OR_START_STRETCH:
         clearPreview();
         if (isPointInRectangle(mouseX, mouseY, GetMagnifierWindow()))
            beginMovingMagnifier(mouseX, mouseY, false); // don't jump to mouse
         else
            beginStretchingRectangle();
      break;

      case SCRATCH_CMD_END_STRETCH_OR_MAG_DRAG:
         if (nowStretching)
            endStretchingRectangle(false); // don't extend to frame edges
         else if (nowMovingMagnifier)
            endMovingMagnifier();
      break;

      case SCRATCH_CMD_START_STRETCH:
         clearPreview();
         beginStretchingRectangle();
      break;

      case SCRATCH_CMD_SNAP_TO_EDGE_AND_END_STRETCH:
         endStretchingRectangle(true); // extend to frame edges
      break;

      case SCRATCH_CMD_WARP_THEN_DRAG_MAG:
         clearPreview();
         beginMovingMagnifier(mouseX, mouseY, true);  // center at mouse
         getSystemAPI()->refreshFrameCached();  // to show moved magnifier
      break;

      case SCRATCH_CMD_END_STRETCH_THEN_AUTOFIX:
         endStretchingRectangle(false); // don't extend to frame edges
         AutoAcceptNextRenderFlag = false;
         NextGKeyShouldOnlyAcceptFlag = true;
         RenderFrameButtonClick();
      break;

      case SCRATCH_CMD_SNAP_END_STRETCH_THEN_AUTOFIX:
         endStretchingRectangle(true); // extend to frame edges
         AutoAcceptNextRenderFlag = false;
         NextGKeyShouldOnlyAcceptFlag = true;
         RenderFrameButtonClick();
      break;

      case SCRATCH_CMD_ACCEPT_FIX:
         // acceptFixOrGOV();         oops want to go to the frame...
         getSystemAPI()->AcceptProvisional(true);  // go to the frame
      break;

      case SCRATCH_CMD_REJECT_FIX:
         rejectFix();
      break;

      case SCRATCH_CMD_TOGGLE_PROVISIONAL:
         togglePreview();
      break;

      case SCRATCH_CMD_TOGGLE_RETICLES:
         ToggleShowReticlesCheckBox();
      break;

      case SCRATCH_CMD_TOGGLE_SIDECAR:
         ToggleMirrorGraphsCheckBox();
      break;

      case SCRATCH_CMD_PREVIEW_FRAME:
         clearPreview();
         AutoAcceptNextRenderFlag = false;
         PreviewFrameButtonClick();
         // Following must go AFTER processing
         NextGKeyShouldOnlyAcceptFlag = true; // ONLY ACCEPT on next 'G'
      break;

      case SCRATCH_CMD_PREVIEW_ALL:
         clearPreview();
         PreviewAllButtonClick();
      break;

      case SCRATCH_CMD_RENDER_FRAME:
      {
         bool onlyAccept = NextGKeyShouldOnlyAcceptFlag;
         acceptFixOrGOV();    // clears NextGKeyShouldOnlyAcceptFlag
         if (!onlyAccept)
         {
            AutoAcceptNextRenderFlag = true;
            RenderFrameButtonClick();
         }
         else
         {
            GetToolProgressMonitor()->SetStatusMessage("Fix accepted");
         }
      }
      break;

      case SCRATCH_CMD_RENDER_ALL:
         clearPreview();
         RenderAllButtonClick();
      break;

      case SCRATCH_CMD_ADD_EVENT_TO_PDL:
         rejectFix();
         PDLCaptureButtonClick();
      break;

      case SCRATCH_CMD_EXECUTE_PDL:
         clearPreview();
         Beep();
      break;

      case SCRATCH_CMD_STOP_RENDER:
         StopToolProcessing();
      break;

      case SCRATCH_CMD_PAUSE_OR_RESUME_RENDER:
         if (!PauseOrResumeButtonClick())
            return TOOL_RETURN_CODE_NO_ACTION;  // Let Navigator handle it
      break;

      case SCRATCH_CMD_TOGGLE_DENSITY:
         ToggleDensity();
      break;

      case SCRATCH_CMD_TOGGLE_ORIENTATION:
         ToggleOrientation();
      break;

      case SCRATCH_CMD_TOGGLE_MOVEMENT:
         ToggleMovement();
      break;

      case SCRATCH_CMD_CYCLE_DOWN_THRU_DETECT_GROUP:
         CycleDownThroughDetectControls();
      break;

      case SCRATCH_CMD_CYCLE_UP_THRU_DETECT_GROUP:
         CycleUpThroughDetectControls();
      break;

      case SCRATCH_CMD_FOCUS_ON_PROCESS_GROUP:
         FocusOnProcessControls();
      break;

      case SCRATCH_CMD_FOCUS_ON_SCRATCH_WINDOW:
         FocusOnScratchWindow();
      break;

      case SCRATCH_CMD_FOCUS_ON_BACKGROUND_SLIDER:
         FocusOnBackgroundDetailControl();
      break;

      case SCRATCH_CMD_FOCUS_ON_BLEND_SLIDER:
         Beep();
      break;

      case SCRATCH_CMD_BLEND_SLIDER_ON_OR_OFF:
         Beep();
      break;

      case SCRATCH_CMD_RESET_BLEND_SLIDER:
         Beep();
      break;

      case SCRATCH_CMD_WIDEN_LIMITS_NORMAL:
         onUpArrow(false, false);
      break;

      case SCRATCH_CMD_WIDEN_LIMITS_MORE:
         onUpArrow(true, false);
      break;

      case SCRATCH_CMD_WIDEN_LIMITS_LESS:
         onUpArrow(false, true);
      break;

      case SCRATCH_CMD_SQUEEZE_LIMITS_NORMAL:
         onDownArrow(false, false);
      break;

      case SCRATCH_CMD_SQUEEZE_LIMITS_MORE:
         onDownArrow(true, false);
      break;

      case SCRATCH_CMD_SQUEEZE_LIMITS_LESS:
         onDownArrow(false, true);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_LEFT_NORMAL:
         onLeftArrow(false, false);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_LEFT_MORE:
         onLeftArrow(true, false);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_LEFT_LESS:
         onLeftArrow(false, true);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_NORMAL:
         onRightArrow(false, false);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_MORE:
         onRightArrow(true, false);
      break;

      case SCRATCH_CMD_SHIFT_LIMITS_RIGHT_LESS:
         onRightArrow(false, true);
      break;

      case SCRATCH_CMD_SHIFT_LEFT_SIDE_LEFT:
         clearPreview();
         changeScratchLimits(-NORMAL_SHIFT, 0);
      break;

      case SCRATCH_CMD_SHIFT_LEFT_SIDE_RIGHT:
         clearPreview();
         changeScratchLimits(NORMAL_SHIFT, 0);
      break;

      case SCRATCH_CMD_SHIFT_RIGHT_SIDE_LEFT:
         clearPreview();
         changeScratchLimits(0, -NORMAL_SHIFT);
      break;

      case SCRATCH_CMD_SHIFT_RIGHT_SIDE_RIGHT:
         clearPreview();
         changeScratchLimits(0, NORMAL_SHIFT);
      break;

      case SCRATCH_CMD_AUTOLOCATE_SCRATCH:
         clearPreview();
         autoLocateScratchLimits();
      break;

      case SCRATCH_CMD_RESET_SCRATCH_LIMITS:
         clearPreview();
         resetScratchLimits();
      break;

      case EXEC_BUTTON_PREVIEW_FRAME:
      //case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
         clearPreview();
         AutoAcceptNextRenderFlag = false;
         if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
            result = TOOL_RETURN_CODE_NO_ACTION;
         // Following must go AFTER the processing
         NextGKeyShouldOnlyAcceptFlag = true;  // ONLY ACCEPT on next 'G'
         break;

      case EXEC_BUTTON_RENDER_FRAME:
      //case EXEC_BUTTON_RENDER_NEXT_FRAME:
      {
         bool onlyAccept = NextGKeyShouldOnlyAcceptFlag;
         acceptFixOrGOV();    // clears NextGKeyShouldOnlyAcceptFlag
         if (!onlyAccept)
         {
            AutoAcceptNextRenderFlag = true;
            if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
               result = TOOL_RETURN_CODE_NO_ACTION;
         }
         else
         {
            GetToolProgressMonitor()->SetStatusMessage("Fix accepted");
         }
         break;
      }

      case EXEC_BUTTON_PREVIEW_ALL:
      case EXEC_BUTTON_RENDER_ALL:
      case EXEC_BUTTON_STOP:
      case EXEC_BUTTON_PAUSE_OR_RESUME:
      case EXEC_BUTTON_SET_RESUME_TC:
      case EXEC_BUTTON_CAPTURE_PDL:
      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      {
         clearPreview();
         bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
         if (!consumed)
            result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

      default:
         // Not handled
         result = TOOL_RETURN_CODE_NO_ACTION;
      break;
   }

   return result;
}

// ===================================================================
//
// Function:    onUserInput
//
// Description: Scratch Tool's Command Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CScratch::onUserInput(CUserInput &userInput)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   // Save mouse position - can't get to it in onToolCommand!!!
   // Actually, I'm pretty sure that I can just remember it in
   // onMouseMove.. QQQ
   mouseX = userInput.imageX;
   mouseY = userInput.imageY;

   // Go back to the default handler
   int retVal = CToolObject::onUserInput(userInput);

   return retVal;
}
//----------------------------------------------------------------------

void CScratch::beginStretchingRectangle(void)
{
   // if rect there clear it
   if (scratchRectangleAvailable)
   {
      scratchRectangleAvailable = false;
      DeactivateMagnifier();
      getSystemAPI()->refreshFrameCached();
   }

   // clear any review regions
   getSystemAPI()->ClearReviewRegions(true);

   // start stretching the box - stretch blue box not red
   getSystemAPI()->setDRSLassoMode(false);
   getSystemAPI()->setStretchRectangleColor(SOLID_BLUE);
   getSystemAPI()->startStretchRectangleOrLasso();
   nowStretching = true;
}
//----------------------------------------------------------------------

void CScratch::endStretchingRectangle(bool extendToFrameEdges)
{
   // stop stretching the box - but tell displayer to NOT erase the temp box!
   getSystemAPI()->stopStretchRectangleOrLasso(false);
   nowStretching = false;

   // get box in image coords
   POINT *lassoPtr; // hopefully, this ends up NULL
   if (getSystemAPI()->getStretchRectangleCoordinates(scratchRect, lassoPtr)
                                                                       == 0)
   {
      // Make sure the orientation matches the aspect ratio of the box
      // that the user drew
      if ((IsScratchVertical() && ((scratchRect.right - scratchRect.left) >
                                   (scratchRect.bottom - scratchRect.top)))
      || ((!IsScratchVertical()) && ((scratchRect.right - scratchRect.left) <
                                      (scratchRect.bottom - scratchRect.top))))
      {
         SetScratchVertical(!IsScratchVertical());
      }

      HandleBlueBoxStateChange();

      // init the limit rectangle
      RECT magRect = GetMagnifierWindow();
      scratchLimitRect = magRect;

      if (IsScratchVertical())
      {
         // Make the limit lines visible in the scratch window
         scratchLimitRect.left += 20;
         scratchLimitRect.right -=20;
         if (scratchLimitRect.left <= scratchRect.left)
            scratchLimitRect.left = scratchRect.left + 1;
         if (scratchLimitRect.right >= scratchRect.right)
            scratchLimitRect.right = scratchRect.right - 1;

         if (extendToFrameEdges)
         {
            // Extend top and bottom to match frame height
            scratchRect.top = scratchLimitRect.top = activeRect.top;
            scratchRect.bottom = scratchLimitRect.bottom = activeRect.bottom;
         }
      }
      else // Horizontal
      {
         // Make the limit lines visible in the scratch window
         scratchLimitRect.top += 20;
         scratchLimitRect.bottom -= 20;
         if (scratchLimitRect.top <= scratchRect.top)
            scratchLimitRect.top = scratchRect.top + 1;
         if (scratchLimitRect.bottom >= scratchRect.bottom)
            scratchLimitRect.bottom = scratchRect.bottom - 1;

         if (extendToFrameEdges)
         {
            // Extend left and right to match frame width
            scratchRect.left = scratchLimitRect.left = activeRect.left;
            scratchRect.right = scratchLimitRect.right = activeRect.right;
         }
      }

      // check the size of the scratch rectangle
      int scratchRectWidth = scratchRect.right - scratchRect.left + 1;
      int scratchRectHeight = scratchRect.bottom - scratchRect.top + 1;

      if ((scratchRectWidth >= 4) && (scratchRectHeight >= 4))
      {
         // it's big enough
         SetScratchRectangle(scratchRect);
         SetScratchLimitRectangle(scratchLimitRect);
         nowAdjustingLeft = false;
         nowAdjustingRight = false;
         SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_NONE);
         scratchRectangleAvailable = true;
         FocusOnScratchWindow();

         // blue box will be drawn in onRedraw callback instiogated by this:
         getSystemAPI()->refreshFrameCached();

         // Run the auto scratch finder
         autoLocateScratchLimits();
      }
      else
      {
         // not big enough
         scratchRectangleAvailable = false;
         DeactivateMagnifier();
      }
   }
   else
   {
      // the stretch rectangle was removed
      scratchRectangleAvailable = false;
      DeactivateMagnifier();
   }
}
//----------------------------------------------------------------------

void CScratch::hideOverlays(void)
{
   // Force redraw to get rid of the rectangle
   // We will refrain from drawing it in the callback
   hidingOverlays = true;
   HideMagnifier();
   ////HideSidecar();
   getSystemAPI()->refreshFrameCached();
}
//----------------------------------------------------------------------

void CScratch::showOverlays(void)
{
   // Force redraw to draw the rectangle
   hidingOverlays = false;
   ShowMagnifier();
////   ShowSidecar();
   getSystemAPI()->refreshFrameCached();
}
//----------------------------------------------------------------------

void CScratch::hideSidecar(void)
{
   HideSidecar();
}
//----------------------------------------------------------------------

void CScratch::showSidecar(void)
{
   ShowSidecar();
}
//----------------------------------------------------------------------

void CScratch::clearScratchRectangle(void)
{
   scratchRectangleAvailable = false;
   hideOverlays();
   hideSidecar();
}
//----------------------------------------------------------------------

void CScratch::setScratchRectangle(const RECT &newRect)
{
   scratchRect = newRect;
   showOverlays();
   showSidecar();

   // Tell GUI - QQQ should get rid of parallel rectangles
   SetScratchRectangle(scratchRect);

   scratchRectangleAvailable = true;
}
//----------------------------------------------------------------------

bool CScratch::isScratchRectangleAvailable(void)
{
   return(scratchRectangleAvailable);
}
//----------------------------------------------------------------------

void CScratch::beginMovingMagnifier(int x, int y, int jumpFlag)
{
   if (jumpFlag)
   {
      magnifierOffsetX = 0;
      magnifierOffsetY = 0;
      moveMagnifierWindow(x, y);
   }
   else
   {
      // Remember the offset, so we can make it look like we grabbed the
      // magnifier at any point within it and are dragging from that point
      RECT magnifierRect = GetMagnifierWindow();
      magnifierOffsetX = x - (magnifierRect.left + magnifierRect.right)/2;
      magnifierOffsetY = y - (magnifierRect.top + magnifierRect.bottom)/2;
   }

   nowMovingMagnifier = true;
}
//----------------------------------------------------------------------

void CScratch::endMovingMagnifier(void)
{
   nowMovingMagnifier = false;
   autoLocateScratchLimits();
}
//----------------------------------------------------------------------

void CScratch::moveMagnifierWindow(int newCenterX, int newCenterY, bool constrain)
{
   if (!scratchRectangleAvailable)
      return; // No blue rectangle, so there should be no magnifier either

   RECT magWin = GetMagnifierWindow();
   RECT blueRect(scratchRect);
   int magWidth = magWin.right - magWin.left + 2;
   int magHeight = magWin.bottom - magWin.top + 2;
   int magCenterX = magWin.left + magWidth/2;
   int magCenterY = magWin.top  + magHeight/2;
   int blueWidth = blueRect.right - blueRect.left + 2;
   int blueHeight = blueRect.bottom - blueRect.top + 2;
   int blueCenterX = blueRect.left + blueWidth/2;
   int blueCenterY = blueRect.top  + blueHeight/2;

   // If SHIFT is on, constrain motion to one axis -
   // y if scratch is vertical, or x if scratch is horizontal
   if (constrain)
   {
      if (IsScratchVertical())
         magCenterY = newCenterY;
      else // Horizontal
         magCenterX = newCenterX;
   }
   else
   {
      magCenterX = newCenterX;
      magCenterY = newCenterY;
   }

   // Constrain magnifier to be almost totally inside the blue
   // rectangle if it fits, else just concentric with it

   const int hangover(2); // how many pixels the magnifier can stick out

   // Width fits?
   if (blueWidth <= magWidth)
   {
      // No - center it
      magCenterX = blueCenterX;
   }
   // Hanging out on left?
   else if (blueRect.left > (magCenterX - magWidth/2 - hangover))
   {
      // Yes - line up left edges
      magCenterX = blueRect.left + magWidth/2 - hangover;
   }
   // Hanging out on right?
   else if (blueRect.right < (magCenterX + magWidth/2 + hangover))
   {
      // Yes - line up right edges
      magCenterX = blueRect.right - magWidth/2 + hangover;
   }

   // Height fits?
   if (blueHeight <= magHeight)
   {
      // No - center it
      magCenterY = blueCenterY;
   }
   // Hanging out on top?
   else if (blueRect.top > (magCenterY - magHeight/2 - hangover))
   {
      // Yes - line up top edges
      magCenterY = blueRect.top + magHeight/2 - hangover;
   }
   // Hanging out on bottom?
   else if (blueRect.bottom < magCenterY + magHeight/2 + hangover)
   {
      // Yes - line up bottom edges
      magCenterY = blueRect.bottom - magHeight/2 + hangover;
   }

   // Whew! OK, now move the damned thing!
   RecenterMagnifier(magCenterX, magCenterY);
}
//----------------------------------------------------------------------

int CScratch::onChangingClip(void)
{
   // This function is called just before the current clip is unloaded.

   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   // In case we were doing 'preview 1'
   clearPreview();

   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//----------------------------------------------------------------------

int CScratch::onNewClip(void)
{
   if (!(IsLicensed() && IsShowing())) //NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;

   // This function is called after a new clip is loaded.

   // Do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   // Make sure we start fresh (probably unnecessary)
   clearPreview();

   // get the image format of the new clip
   imgFmt = getSystemAPI()->getVideoClipImageFormat();
   if (imgFmt != NULL)
   {
      // set up the intermediate image format
      *intFmt = *imgFmt;
      intFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
      intFmt->setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

      // get the active picture rect
      activeRect = imgFmt->getActivePictureRect();

      // initialize to- and from- color space converters
      toRGBColorConverter->Init(imgFmt, intFmt);
      fmRGBColorConverter->Init(intFmt, imgFmt);
   }

   // This allows the tool to notify the gui
   ClipChange.Notify();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//----------------------------------------------------------------------

int CScratch::onChangingFraming(void)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   clearPreview();
   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//----------------------------------------------------------------------

int CScratch::onNewMarks(void)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   // hmmm, why are we doing this?? QQQ
   //DestroyAllToolSetups(false);

   MarksChange.Notify();  // tell the GUI
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//----------------------------------------------------------------------

int CScratch::onRedraw(int frameIndex)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   if (scratchRectangleAvailable && (!hidingOverlays))
   {
         getSystemAPI()->setGraphicsColor(SOLID_BLUE);
         getSystemAPI()->drawRectangleFrame(&scratchRect);
   }
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//----------------------------------------------------------------------

int CScratch::onMouseMove(CUserInput &userInput)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   mouseX = userInput.imageX;
   mouseY = userInput.imageY;
   bool shiftDown = ((userInput.shift & USER_INPUT_SHIFT_MASK_SHIFT) != 0);

   if (nowMovingMagnifier)
   {
      moveMagnifierWindow(mouseX - magnifierOffsetX, mouseY - magnifierOffsetY,
                          shiftDown);  /* constrain? */
      getSystemAPI()->refreshFrameCached();
   }
   // WTF? Why not CONSUMED???
   return TOOL_RETURN_CODE_NO_ACTION;
}
//----------------------------------------------------------------------

void CScratch::onLeftButton(int x)
{
   if (IsDisabled())
      return;

   scratchRect      = GetScratchRectangle();
   scratchLimitRect = GetScratchLimitRectangle();
   RECT magnifierRect = GetMagnifierWindow();

   nowAdjustingLeft  = true;
   nowAdjustingRight = false;

   if (IsScratchVertical())
   {
      int left = magnifierRect.left + x;

      if (left < scratchRect.left)
         left = scratchRect.left;
      if (left >= scratchLimitRect.right)
         left = scratchLimitRect.right - 1;
      scratchLimitRect.left = left;
   }
   else // Horizontal
   {
      int top = magnifierRect.top + x;

      if (top < scratchRect.top)
         top = scratchRect.top;
      if (top >= scratchLimitRect.bottom)
         top = scratchLimitRect.bottom - 1;
      scratchLimitRect.top = top;
   }

   SetScratchRectangle(scratchRect);
   SetScratchLimitRectangle(scratchLimitRect);
   SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_LEFT);
   getSystemAPI()->refreshFrameCached();
}
//----------------------------------------------------------------------

void CScratch::onRightButton(int x)
{
   if (IsDisabled())
      return;

   scratchRect      = GetScratchRectangle();
   scratchLimitRect = GetScratchLimitRectangle();
   RECT magnifierRect = GetMagnifierWindow();

   nowAdjustingLeft  = false;
   nowAdjustingRight = true;

   if (IsScratchVertical())
   {
      int right = magnifierRect.left + x;

      if (right > scratchRect.right)
         right = scratchRect.right;
      if (right <= scratchLimitRect.left)
         right = scratchLimitRect.left + 1;
       scratchLimitRect.right = right;
   }
   else // Horizontal
   {
      int bottom = magnifierRect.top + x;

      if (bottom > scratchRect.bottom)
         bottom = scratchRect.bottom;
      if (bottom <= scratchLimitRect.top)
         bottom = scratchLimitRect.top + 1;
      scratchLimitRect.bottom = bottom;
   }

   SetScratchRectangle(scratchRect);
   SetScratchLimitRectangle(scratchLimitRect);
   SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_RIGHT);
   getSystemAPI()->refreshFrameCached();
}
//----------------------------------------------------------------------
// Shift scratch bounds left.
void CScratch::onLeftArrow(bool shiftDown, bool ctrlDown)
{
   if (IsDisabled())
      return;

   clearPreview();

   if ((ctrlDown && shiftDown) || (!ctrlDown && !shiftDown))
   {
      changeScratchLimits(-NORMAL_SHIFT, -NORMAL_SHIFT);
   }
   else if (shiftDown)
   {
      changeScratchLimits(-MORE_SHIFT, -MORE_SHIFT);
   }
   else // (ctrlDown)
   {
      changeScratchLimits(-LESS_SHIFT, -LESS_SHIFT);
   }
}
//----------------------------------------------------------------------
// Shift scratch bounds right.
void CScratch::onRightArrow(bool shiftDown, bool ctrlDown)
{
   if (IsDisabled())
      return;

   clearPreview();

   if ((ctrlDown && shiftDown) || (!ctrlDown && !shiftDown))
   {
      changeScratchLimits(NORMAL_SHIFT, NORMAL_SHIFT);
   }
   else if (shiftDown)
   {
      changeScratchLimits(MORE_SHIFT, MORE_SHIFT);
   }
   else // (ctrlDown)
   {
      changeScratchLimits(LESS_SHIFT, LESS_SHIFT);
   }
}
//----------------------------------------------------------------------
// Widen scratch bounds
void CScratch::onUpArrow(bool shiftDown, bool ctrlDown)
{
   if (IsDisabled())
      return;

   clearPreview();

   if ((ctrlDown && shiftDown) || (!ctrlDown && !shiftDown))
   {
      changeScratchLimits(-NORMAL_WIDTH_DELTA, NORMAL_WIDTH_DELTA);
   }
   else if (shiftDown)
   {
      changeScratchLimits(-MORE_WIDTH_DELTA, MORE_WIDTH_DELTA);
   }
   else // (ctrlDown)
   {
      int changeLeft = -LESS_WIDTH_DELTA;
      int changeRight = LESS_WIDTH_DELTA;
      if (LESS_WIDTH_DELTA <= 0)
      {
         bool odd = ((IsScratchVertical()
                      ? (scratchLimitRect.right - scratchLimitRect.left)
                      : (scratchLimitRect.bottom - scratchLimitRect.top))
                     % 2) == 1;
         changeLeft = odd? -1 : 0;
         changeRight = odd?  0 : 1;
      }

      changeScratchLimits(changeLeft, changeRight);
   }
}
//----------------------------------------------------------------------
// Squeeze scratch bounds
void CScratch::onDownArrow(bool shiftDown, bool ctrlDown)
{
   if (IsDisabled())
      return;

   clearPreview();

   if ((ctrlDown && shiftDown) || (!ctrlDown && !shiftDown))
   {
      changeScratchLimits(NORMAL_WIDTH_DELTA, -NORMAL_WIDTH_DELTA);
   }
   else if (shiftDown)
   {
      changeScratchLimits(MORE_WIDTH_DELTA, -MORE_WIDTH_DELTA);
   }
   else // (ctrlDown)
   {
      int changeLeft = LESS_WIDTH_DELTA;
      int changeRight = -LESS_WIDTH_DELTA;
      if (LESS_WIDTH_DELTA <= 0)
      {
         bool odd = ((IsScratchVertical()
                      ? (scratchLimitRect.right - scratchLimitRect.left)
                      : (scratchLimitRect.bottom - scratchLimitRect.top))
                     % 2) == 1;
         changeLeft = odd? 0 : 1;
         changeRight = odd?  -1 : 0;
      }

      changeScratchLimits(changeLeft, changeRight);
   }
}
//----------------------------------------------------------------------

void CScratch::togglePreview(void)
{
   getSystemAPI()->ToggleProvisional(false); // don't zoom
}
//----------------------------------------------------------------------

void CScratch::acceptFix(void)
{
   getSystemAPI()->AcceptProvisional(false);  // don't go to the frame
   NextGKeyShouldOnlyAcceptFlag = false;
}
//----------------------------------------------------------------------

void CScratch::acceptGOV(void)
{
   getSystemAPI()->AcceptGOV();
   NextGKeyShouldOnlyAcceptFlag = false;
}
//----------------------------------------------------------------------

void CScratch::acceptFixOrGOV(void)
{
   acceptGOV();
   acceptFix();
}
//----------------------------------------------------------------------

void CScratch::rejectFix(void)
{
   // GOV tool already had a crack at it
   getSystemAPI()->RejectProvisional(false);  // don't go to the frame
   NextGKeyShouldOnlyAcceptFlag = false;
}
//----------------------------------------------------------------------

void CScratch::clearPreview(void)
{
   acceptGOV();   // Auto-accept GOV if pending
   getSystemAPI()->ClearProvisional(false); // don't go to last displayed frame
   NextGKeyShouldOnlyAcceptFlag = false;
}
//----------------------------------------------------------------------

void CScratch::resetScratchLimits()
{
   if (scratchRectangleAvailable)
   {
      // init the limit rectangle
      RECT magRect = GetMagnifierWindow();
      scratchLimitRect = magRect;

      if (IsScratchVertical())
      {
         // Make the limit lines visible in the scratch window
         scratchLimitRect.left += 20;
         scratchLimitRect.right -=20;
         if (scratchLimitRect.left <= scratchRect.left)
            scratchLimitRect.left = scratchRect.left + 1;
         if (scratchLimitRect.right >= scratchRect.right)
            scratchLimitRect.right = scratchRect.right - 1;

         // Top and bottom match blue scratch rect
         scratchLimitRect.top = scratchRect.top;
         scratchLimitRect.bottom = scratchRect.bottom;
      }
      else // Horizontal
      {
         // Make the limit lines visible in the scratch window
         scratchLimitRect.top += 20;
         scratchLimitRect.bottom -= 20;
         if (scratchLimitRect.top <= scratchRect.top)
            scratchLimitRect.top = scratchRect.top + 1;
         if (scratchLimitRect.bottom >= scratchRect.bottom)
            scratchLimitRect.bottom = scratchRect.bottom - 1;

         // Left and right match blue scratch rect
         scratchLimitRect.left = scratchRect.left;
         scratchLimitRect.right = scratchRect.right;
      }
      SetScratchLimitRectangle(scratchLimitRect);
      nowAdjustingLeft = false;
      nowAdjustingRight = false;
      SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_NONE);

      // Stupid way to update the GUI - refresh the display frame
      getSystemAPI()->refreshFrameCached();
   }
}

//----------------------------------------------------------------------

void CScratch::changeScratchLimits(int deltaLeft, int deltaRight)
{
   bool retVal = false;

   if (IsScratchVertical())
      retVal = changeScratchLimitsVertical(deltaLeft, deltaRight);
   else // Horizontal
      retVal = changeScratchLimitsHorizontal(deltaLeft, deltaRight);

   if (!retVal)
   {
      // Didn't move
      Beep();
   }
   else
   {
      SetScratchLimitRectangle(scratchLimitRect);
      SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_BOTH);

      // Stupid way to update the GUI - refresh the display frame
      getSystemAPI()->refreshFrameCached();
   }
}
//----------------------------------------------------------------------

bool CScratch::changeScratchLimitsVertical(int deltaLeft, int deltaRight)
{
   // NOTE!! The various rectangles have INCLUSIVE right/bottom,
   // so for example if scratchLimitRect has EQUAL left and right, then
   // the scratch is one pixel wide (right boundary is .right + 1)

   if (!scratchRectangleAvailable) return false;

   int dominance = 0;    // equal or non-opposing forces, so no dominance
   if (deltaLeft > 0 && deltaRight == 0)
      dominance = -1;    // left more powerful, will push right
   if (deltaLeft == 0 && deltaRight < 0)
      dominance = 1;     // right more powerful, will push left

   int oldLeft = scratchLimitRect.left;
   int oldRight = scratchLimitRect.right;
   RECT magRect = GetMagnifierWindow();

   scratchLimitRect.left  += deltaLeft;
   scratchLimitRect.right += deltaRight;

   if (scratchLimitRect.left > scratchLimitRect.right)
      switch (dominance)
      {
         case -1:
           scratchLimitRect.right = scratchLimitRect.left;
         break;

         case 1:
            scratchLimitRect.left = scratchLimitRect.right;
         break;

         case 0:
            scratchLimitRect.left = scratchLimitRect.right =
               ((scratchLimitRect.left + scratchLimitRect.right) / 2);
         break;
      }

   if (scratchLimitRect.left <= magRect.left)
   {
      scratchLimitRect.left = magRect.left + 1;
      dominance = -1;   // left needs to push back when it hits a limit
   }
   if (scratchLimitRect.left <= scratchRect.left)
   {
      scratchLimitRect.left = scratchRect.left + 1;
      dominance = -1;   // left needs to push back when it hits a limit
   }

   if (scratchLimitRect.right >= magRect.right)
   {
      scratchLimitRect.right = magRect.right - 1;
      dominance = 1;   // right needs to push back when it hits a limit
   }
   if (scratchLimitRect.right >= scratchRect.right)
   {
      scratchLimitRect.right = scratchRect.right - 1;
      dominance = 1;   // right needs to push back when it hits a limit
   }

   if (scratchLimitRect.left > scratchLimitRect.right)
      switch (dominance)
      {
         case -1:
           scratchLimitRect.right = scratchLimitRect.left;
         break;

         case 1:
            scratchLimitRect.left = scratchLimitRect.right;
         break;

         case 0:
            scratchLimitRect.left = scratchLimitRect.right =
               ((scratchLimitRect.left + scratchLimitRect.right) / 2);
         break;
      }

   return (oldLeft != scratchLimitRect.left ||
           oldRight != scratchLimitRect.right);
}
//----------------------------------------------------------------------

bool CScratch::changeScratchLimitsHorizontal(int deltaTop, int deltaBottom)
{
   // NOTE!! The various rectangles have INCLUSIVE right/bottom,
   // so for example if scratchLimitRect has EQUAL left and right, then
   // the scratch is one pixel wide (right boundary is .right + 1)

   if (!scratchRectangleAvailable) return false;

   int dominance = 0;    // equal or non-opposing forces, so no dominance
   if (deltaTop > 0 && deltaBottom == 0)
      dominance = -1;    // top more powerful, will push bottom
   if (deltaTop == 0 && deltaBottom < 0)
      dominance = 1;     // bottom more powerful, will push top

   int oldTop = scratchLimitRect.top;
   int oldBottom = scratchLimitRect.bottom;
   RECT magRect = GetMagnifierWindow();

   scratchLimitRect.top  += deltaTop;
   scratchLimitRect.bottom += deltaBottom;

   if (scratchLimitRect.top > scratchLimitRect.bottom)
      switch (dominance)
      {
         case -1:
           scratchLimitRect.bottom = scratchLimitRect.top;
         break;

         case 1:
            scratchLimitRect.top = scratchLimitRect.bottom;
         break;

         case 0:
            scratchLimitRect.top = scratchLimitRect.bottom =
               ((scratchLimitRect.top + scratchLimitRect.bottom) / 2);
         break;
      }

   if (scratchLimitRect.top <= magRect.top)
   {
      scratchLimitRect.top = magRect.top + 1;
      dominance = -1;   // top needs to push back when it hits a limit
   }
   if (scratchLimitRect.top <= scratchRect.top)
   {
      scratchLimitRect.top = scratchRect.top + 1;
      dominance = -1;   // top needs to push back when it hits a limit
   }

   if (scratchLimitRect.bottom >= magRect.bottom)
   {
      scratchLimitRect.bottom = magRect.bottom - 1;
      dominance = 1;   // bottom needs to push back when it hits a limit
   }
   if (scratchLimitRect.bottom >= scratchRect.bottom)
   {
      scratchLimitRect.bottom = scratchRect.bottom - 1;
      dominance = 1;   // bottom needs to push back when it hits a limit
   }

   if (scratchLimitRect.top > scratchLimitRect.bottom)
      switch (dominance)
      {
         case -1:
           scratchLimitRect.bottom = scratchLimitRect.top;
         break;

         case 1:
            scratchLimitRect.top = scratchLimitRect.bottom;
         break;

         case 0:
            scratchLimitRect.top = scratchLimitRect.bottom =
               ((scratchLimitRect.top + scratchLimitRect.bottom) / 2);
         break;
      }

   return (oldTop != scratchLimitRect.top ||
           oldBottom != scratchLimitRect.bottom);
}
//----------------------------------------------------------------------

void CScratch::autoLocateScratchLimits(bool updateWhileSearching)
{
      int left=0, right=0;
      guessWhereTheScratchIs(left, right, updateWhileSearching);

      if (IsScratchVertical())
      {
         scratchLimitRect.left = left;
         scratchLimitRect.right = right;
      }
      else // Horizontal
      {
         scratchLimitRect.top = left;
         scratchLimitRect.bottom = right;
      }
      SetScratchLimitRectangle(scratchLimitRect);
      SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_BOTH);

      // Stupid way to update the GUI - refresh the display frame
      getSystemAPI()->refreshFrameCached();
}
//----------------------------------------------------------------------


   struct STestResult
   {
      float score;
      int left;
      int right;

      STestResult(float newScore, int newLeft, int newRight)
      : score(newScore), left(newLeft), right(newRight)
      {};
   };

//Overload the < operator.
bool operator< (const STestResult &result1, const STestResult &result2)
{
	return result1.score > result2.score;
}
//Overload the > operator.
bool operator> (const STestResult &result1, const STestResult &result2)
{
	return result1.score < result2.score;
}
//----------------------------------------------------------------------

void CScratch::guessWhereTheScratchIs(int &outLeft, int& outRight, bool updateWhileSearching)
{
   if (!scratchRectangleAvailable) return;

   // Find the data for the channel to be used for detection
   int left, right;
   RECT magRect = GetMagnifierWindow();
   int magLeft = magRect.left;
   int magRight = magRect.right;
   if (!IsScratchVertical())
   {
      magLeft = magRect.top;
      magRight = magRect.bottom;
   }
   int scratchLeft = scratchRect.left;
   int scratchRight = scratchRect.right;
   if (!IsScratchVertical())
   {
      scratchLeft = scratchRect.top;
      scratchRight = scratchRect.bottom;
   }

   int numberOfValues = magRight - magLeft + 1;
   int leftEdge = 2;                         // leave room for shoulder
   int rightEdge = (numberOfValues - 1) - 2; // leave room for shoulder
   MTI_UINT16 *signal = GetPtrToMagValuesForDetectSignal();
   if (signal == NULL) return;

   MTI_UINT16 signalMin = 65535;
   MTI_UINT16 signalMax = 0;
   for (int i = 0; i < numberOfValues; ++i)
   {
      signalMin = std::min<MTI_UINT16>(signalMin, signal[i]);
      signalMax = std::max<MTI_UINT16>(signalMax, signal[i]);
   }

   // Normalize signal to range 0.0 - 1.0.
   float *normalizedSignal = new float[numberOfValues];
   for (int i = 0; i < numberOfValues; ++i)
   {
      normalizedSignal[i] =
         (signalMin < signalMax)
            ? (float(signal[i] - signalMin) / float(signalMax - signalMin))
            : 0.5;
   }

   float *integral = new float[numberOfValues];
   integral[0] = normalizedSignal[0];
   for (int i = 1; i < numberOfValues; ++i)
   {
      integral[i] = integral[i-1] + normalizedSignal[i];
   }

   float average = integral[numberOfValues - 1] / numberOfValues;

   int i, j;
   const int minScratchWidth(3);
   int maxScratchWidth = 17 + (18 * ((imgFmt->getPixelsPerLine() - 1) / 2048));
   bool scratchIsDark = IsDensityDark();

   STestResult bestResult(0, 20, 27);

   // Constrain to BLUE rectangle limits
   if (scratchLeft > magLeft)
      leftEdge += scratchLeft - magLeft;
   if (scratchRight < magRight)
      rightEdge += scratchRight - magRight;

//   MTIostringstream os;
//   for (i = leftEdge; i <= rightEdge; ++i)
//      os << i << ":" << normalizedSignal[i] << " ";
//   TRACE_0(errout << os.str());

   // At each point
   for (left = leftEdge;
        left < (rightEdge - minScratchWidth);
        ++left)
   {
      // for each scratch width
      for (right = (left + minScratchWidth - 1);
           right <= (left + maxScratchWidth - 1) && right < rightEdge;
           ++right)
      {
//         TRACE_0(errout << "+++++++++ " << left << ", " << right << " ++++++++++" << endl);

         // Preconditions: All values must be respect the density relative to
         // left and right values, and relative to the average of the signal.
         bool punt = false;
         auto lightMin = (normalizedSignal[left] < normalizedSignal[right]) ? normalizedSignal[left] : normalizedSignal[right];
         auto darkMax = (normalizedSignal[left] > normalizedSignal[right]) ? normalizedSignal[left] : normalizedSignal[right];
         for (int i = left + 1; i <= right - 1; ++i)
         {
            if ((scratchIsDark && (normalizedSignal[i] > darkMax))
               || (!scratchIsDark && (normalizedSignal[i] < lightMin)))
            {
               punt = true;
               break;
            }
         }

         if (punt)
         {
            break;
         }

         auto integralLeftToRight = integral[right] - ((left == leftEdge) ? 0 : integral[left - 1]);
         auto averageLeftToRight = integralLeftToRight / (right - left + 1);
         if ((scratchIsDark && (averageLeftToRight > average))
            || (!scratchIsDark && (averageLeftToRight < average)))
         {
            break;
         }

         // 1. Reward large launch and landing delta slopes
         auto launchSlopeLeft = (normalizedSignal[left] - normalizedSignal[left-1]);
         auto launchSlopeRight = (normalizedSignal[left+1] - normalizedSignal[left]);
         if (launchSlopeRight < 0)
         {
            launchSlopeRight = -launchSlopeRight;
            launchSlopeLeft = -launchSlopeLeft;
         }

         auto landingSlopeLeft = normalizedSignal[right-1] - normalizedSignal[right];
         auto landingSlopeRight = normalizedSignal[right] - normalizedSignal[right+1];
         if (landingSlopeLeft < 0)
         {
            landingSlopeLeft = -landingSlopeLeft;
            landingSlopeRight = -landingSlopeRight;
         }

         auto leftAngleReward = launchSlopeRight - launchSlopeLeft;
         int rightAngleReward = landingSlopeLeft - landingSlopeRight;

//         TRACE_0(errout << "leftAngleReward: " << leftAngleReward << " = "
//                        << launchSlopeRight << " - " << launchSlopeLeft << endl
//                        << "rightAngleReward: " << rightAngleReward << " = "
//                        << landingSlopeRight << " - " << landingSlopeLeft);

         // 2. Reward large rise and fall with well-defined shoulders
         auto monotonicLeftSlope = 0.F;
         auto previousSlope = 0.F;
         for (i = left+1; i <= right-1; ++i)
         {
            auto newSlope = normalizedSignal[i] - normalizedSignal[i-1];
            if ((previousSlope >= 0 && newSlope >= 0) || (previousSlope <= 0 && newSlope <= 0))
            {
               monotonicLeftSlope += newSlope;
               previousSlope = newSlope;
            }
            else
            {
               break;
            }
         }

         auto monotonicRightSlope = 0.F;
         previousSlope = 0;
         for (i = right-1; i >= left+1; --i)
         {
            auto newSlope = normalizedSignal[i] - normalizedSignal[i+1];
            if ((previousSlope >= 0 && newSlope >= 0) || (previousSlope <= 0 && newSlope <= 0))
            {
               monotonicRightSlope += newSlope;
               previousSlope = newSlope;
            }
            else
            {
               break;
            }
         }

         // 3. Penalize rise and fall of shoulders if same sign
         auto leftShoulderLeftRise = normalizedSignal[left-1] - normalizedSignal[left-2];
         auto leftShoulderRightRise = normalizedSignal[left] - normalizedSignal[left-1];
         auto leftShoulderRise = leftShoulderLeftRise + leftShoulderRightRise;

//         TRACE_0(errout << "leftShoulder: " << leftShoulderRise
//                                << " = " << leftShoulderLeftRise << " + "
//                                         << leftShoulderRightRise << "    "
//            << "monotonicLeftSlope = " << monotonicLeftSlope);

         // Don't want shoulder to be bigger than the rise
         if ((monotonicLeftSlope < 0 && (leftShoulderRise < monotonicLeftSlope ||
                                       (-leftShoulderRise) < monotonicLeftSlope))
             || (monotonicLeftSlope > 0 && (leftShoulderRise > monotonicLeftSlope ||
                                          (-leftShoulderRise) > monotonicLeftSlope))
             || (monotonicLeftSlope == 0))
         {
            monotonicLeftSlope = leftShoulderRise = 0;
         }

         auto leftSideScore = monotonicLeftSlope - leftShoulderRise
                             + ((monotonicLeftSlope > 0)? leftAngleReward
                                                       : -leftAngleReward);

         auto rightShoulderLeftRise = normalizedSignal[right] - normalizedSignal[right+1];
         auto rightShoulderRightRise = normalizedSignal[right+1] - normalizedSignal[right+2];
         auto rightShoulderRise = rightShoulderLeftRise + rightShoulderRightRise;

//         TRACE_0(errout << "rightShoulder: " << rightShoulderRise
//                        << " = " << rightShoulderLeftRise << " + "
//                                 << rightShoulderRightRise << "    "
//                 << "monotonicRightSlope = " << monotonicRightSlope);

         // Don't want shoulder to be bigger than the rise
         if ((monotonicRightSlope < 0 && (rightShoulderRise < monotonicRightSlope ||
                                        (-rightShoulderRise) < monotonicRightSlope))
             || (monotonicRightSlope > 0 && (rightShoulderRise > monotonicRightSlope ||
                                           (-rightShoulderRise) > monotonicRightSlope))
             || (monotonicRightSlope == 0))
         {
            monotonicRightSlope = rightShoulderRise = 0;
         }

         auto rightSideScore = monotonicRightSlope - rightShoulderRise
                              + ((monotonicRightSlope > 0)? rightAngleReward
                                                         : -rightAngleReward);

         float totalScore = leftSideScore * rightSideScore;

//         TRACE_0(errout
//            << "leftSideScore: " << leftSideScore << " = "
//               << monotonicLeftSlope << " - " << leftShoulderRise
//               << " + "  << leftAngleReward << "  "
//            << "rightSideScore: " << rightSideScore << " = "
//               << monotonicRightSlope << " - " << rightShoulderRise
//               << " + " << rightAngleReward << "  "
//            << "TOTAL SCORE = " << totalScore);

         // Reward based on preferred density

         if (totalScore > bestResult.score)
         {
//            TRACE_0(errout << "WINNER, WINNER CHICKEN DINNER");
            scratchLimitRect.left = magLeft + left;
            scratchLimitRect.right = magLeft + right;
            if (updateWhileSearching)
            {
               SetScratchLimitRectangle(scratchLimitRect);
               SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_BOTH);
               getSystemAPI()->refreshFrameCached();
            }

            bestResult = STestResult(totalScore, left, right);
         }
      }
   }

   if (!updateWhileSearching)
   {
      SetScratchLimitRectangle(scratchLimitRect);
      SetScratchLimitFocusSide(SCRATCH_LIMIT_FOCUS_BOTH);
      getSystemAPI()->refreshFrameCached();
   }

   outLeft = magLeft + bestResult.left;
   outRight = magLeft + bestResult.right;
   delete[] normalizedSignal;
   delete[] integral;
}
//----------------------------------------------------------------------

void CScratch::setMajorState(int majorstate)
{
   majorState = majorstate;

   switch(majorstate) {

      case MAJORSTATE_GUI_CREATED:

         // get the image format of the new clip
         imgFmt = getSystemAPI()->getVideoClipImageFormat();

         if (imgFmt != NULL) { // init on image format

            // set up the intermediate image format
            *intFmt = *imgFmt; // obligatory to get dims the same (!)
            intFmt->setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
            intFmt->setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

            // get the active picture rect
            activeRect = imgFmt->getActivePictureRect();

            // initialize to- and from- color space converters
            toRGBColorConverter->Init(imgFmt, intFmt);
            fmRGBColorConverter->Init(intFmt, imgFmt);
         }

         // Update the tool's GUI to match the tool processing initial state
         UpdateExecutionGUI(GetToolProcessingControlState(), false);

         // no stretch rectangle available yet
         scratchRectangleAvailable = false;

         // clear the stretching flag
         nowStretching = false;

         // set the lasso mode to "rectangle"
         getSystemAPI()->setDRSLassoMode(false);

         // clear the magnifier positioning flag
         nowMovingMagnifier = false;

         // make sure magnifier not displayed
         DeactivateMagnifier();

         break;

      case MAJORSTATE_SHOWING_SCRATCH_TOOL:

         nowStretching = false;

         break;

      case MAJORSTATE_SHOWING_LOCATE_SCRATCH:

         //magnifierRect = GetMagnifierWindow();
         nowMovingMagnifier = false;

         break;

      case MAJORSTATE_SHOWING_SET_SCRATCH_WIDTH:

         nowAdjustingLeft  = false;
         nowAdjustingRight = false;

         break;

      case MAJORSTATE_SHOWING_SELECT_SIGNALS:

         break;

      case MAJORSTATE_SHOWING_PROCESS_SCRATCH:

         break;

      default:

         break;
   }
}
//----------------------------------------------------------------------

int CScratch::getMajorState(void)
{
   return(majorState);
}
//----------------------------------------------------------------------

CColorSpaceConvert* CScratch::getToRGBConverter(void)
{
   return(toRGBColorConverter);
}
//----------------------------------------------------------------------

CColorSpaceConvert* CScratch::getFmRGBConverter(void)
{
   return(fmRGBColorConverter);
}
//----------------------------------------------------------------------
void CScratch::setAutoAcceptNextRenderFlag()
{
   AutoAcceptNextRenderFlag = true;
}
//----------------------------------------------------------------------

void CScratch::DestroyAllToolSetups(bool notIfPaused)
{
   // DRS has three tool setups: DRS, Review Tool and Move Tool.
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (toolSetupHandle >= 0)
      {
      bool isPaused
      = (getSystemAPI()->GetToolSetupStatus(toolSetupHandle)==AT_STATUS_PAUSED);
      if (!(notIfPaused && isPaused))
         {
         if (toolSetupHandle == activeToolSetup)
            getSystemAPI()->SetActiveToolSetup(-1);
         getSystemAPI()->DestroyToolSetup(toolSetupHandle);
         toolSetupHandle = -1;
         }
      }

#if !DO_PREVIEW_G_KEY_HACK

   if (trainingToolSetupHandle >= 0)
      {
      if (trainingToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(trainingToolSetupHandle);
      trainingToolSetupHandle = -1;
      }

#endif

   if (singleFrameToolSetupHandle >= 0)
      {
      if (singleFrameToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(singleFrameToolSetupHandle);
      singleFrameToolSetupHandle = -1;
      }
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CScratch::toolShow(void)
{
   ShowScratchGUI();

   if (!isItOkToUseToolNow())
      getSystemAPI()-> DisableTool(getToolDisabledReason());

   return 0;
}
//----------------------------------------------------------------------

bool CScratch::IsShowing(void)
{
   return(IsToolVisible());
}

// ===================================================================
//
// Function:    TryToResolveProvisional
//
// Description: We are given a chance to auto reject before
//              the system puts up an obnoxious dialog
//
// Arguments:   None
//
// Returns:     TOOL_RETURN_CODE_NO_ACTION if we didn't have a fix pending,
//              else TOOL_RETURN_CODE_EVENT_CONSUMED
//
// ===================================================================
int CScratch::TryToResolveProvisional()
{
   if (getSystemAPI()->IsProvisionalPending())
   {
      clearPreview();
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CScratch::toolHide(void)
{
   HideScratchGUI();
   hideOverlays();
   hideSidecar();
   doneWithTool();

   return 0;
}

// ===================================================================
//
// Function:    ProcessSingleFrame
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CScratch::ProcessSingleFrame(EToolSetupType setupType)
{
   CBusyCursor busyCursor(true);   // Busy cursor until busyCursor goes
                                   // out-of-scope, e.g., this function returns
   int retVal;
   CAutoErrorReporter autoErr("CScratch::ProcessSingleFrame",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

#if DO_PREVIEW_G_KEY_HACK

   setupType = TOOL_SETUP_TYPE_SINGLE_FRAME;  // Override TRAINING!!
   int &toolSetupHandle = singleFrameToolSetupHandle;

#else

   int &toolSetupHandle = (setupType == TOOL_SETUP_TYPE_SINGLE_FRAME)?
                                  singleFrameToolSetupHandle :
                                  trainingToolSetupHandle;
#endif

   if (IsDisabled())
      {
      autoErr.errorCode = -999;
      autoErr.msg << "Scratch internal error, tried to run when disabled ";
      return autoErr.errorCode;
      }

   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
   {
      // Something is running, so can't do single frame processing right now
      return -1;
   }

   // Check if we have a tool setup yet
   if (toolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Scratch",
                                                   setupType, &ioConfig);
      if (toolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "Scratch internal error: MakeSimpleToolSetup failed";
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetActiveToolSetup("
                     << toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
      }

   // Set processing frame range to the current frame index
   CToolFrameRange frameRange(1, 1);
   frameRange.inFrameRange[0].randomAccess = true;

// TTT
   int lastFrameIndex = getSystemAPI()->getLastFrameIndex();
   // Is it too much bother to put in a comment here explaining this shit??
   // Like, why isn't in=lastFrameIndex and out=lastFrameIndex+1 ?????????????
//   frameRange.inFrameRange[0].inFrameIndex  = lastFrameIndex - 2;
//   frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 3; // 3 or 2?
   frameRange.inFrameRange[0].inFrameIndex  = lastFrameIndex - 1;
   frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 2;
//   frameRange.inFrameRange[0].inFrameIndex  = lastFrameIndex;   // OK Let's
//   frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 1;  // try it!!

   retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetToolFrameRange"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Set Scratch tool's parameters
   CScratchParameters *toolParams = new CScratchParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   // Wait for processing to complete
   // For some reason, this just seems to hang... not sure why it works for
   // DRS but not here.....!  QQQ  -- hmmm I think I fixed it....
   // broken again...
   //getSystemAPI()->WaitForToolProcessing();

   return 0;
}

// ===================================================================
//
// Function:    RunFrameProcessing
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CScratch::RunFrameProcessing(int newResumeFrame)
{
   int retVal;
   CAutoErrorReporter autoErr("CScratch::RunFrameProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
      {
      autoErr.errorCode = -999;
      autoErr.msg << "Scratch internal error, tried to run when disabled ";
      return autoErr.errorCode;
      }

// TBD:  Fix correctly.  The call to refreshFrameCached partially fixes
//       the problem with the timeline slider getting stuck during
//       tool processing
    getSystemAPI()->refreshFrameCached();

   // Check if we have a tool setup yet
   if (toolSetupHandle < 0)
      {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Scratch",
                                                   TOOL_SETUP_TYPE_MULTI_FRAME,
                                                            &ioConfig);
      if (toolSetupHandle < 0)
         {
         autoErr.errorCode = -1;
         autoErr.msg << "Scratch internal error: MakeSimpleToolSetup failed";
         return -1;
         }
      }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetActiveToolSetup("
                     << toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
      }

   if (newResumeFrame < 0)
      {
      // Starting from Stop, so set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markIn = getSystemAPI()->getMarkIn();
      int markOut = getSystemAPI()->getMarkOut();
      if (markIn < 0 || markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark In and Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= markIn)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond Mark In";
         return -1;
         }

// TTT
//    frameRange.inFrameRange[0].inFrameIndex  = markIn  - 2;
//    frameRange.inFrameRange[0].outFrameIndex = markOut + 2;
      frameRange.inFrameRange[0].inFrameIndex  = markIn  - 1;
      frameRange.inFrameRange[0].outFrameIndex = markOut + 1;

      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "Scratch internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }
   else
      {
      // Resuming from Pause state, so set processing frame range
      // from newResumeFrame to out mark
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markOut = getSystemAPI()->getMarkOut();
      if (markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= newResumeFrame)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond the Resume frame";
         return -1;
         }

// TTT
//      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame - 2 ;
//      frameRange.inFrameRange[0].outFrameIndex = markOut + 2;
      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
      frameRange.inFrameRange[0].outFrameIndex = markOut;

      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "Scratch internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }

   // Set render/preview
   getSystemAPI()->SetProvisionalRender(renderFlag);

   // Set Scratch's tool parameters
   CScratchParameters *toolParams = new CScratchParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}

//
// this overrides the method in ToolObject,
// resetting the active tool setup using its
// handle and then calling the base class
//
int CScratch::StopFrameProcessing()
{
   int retVal;

   // Kludge so we don't get stuck if the following sequence is followed
   //   Preview -> Pause -> Preview 1 -> Stop
   // We really need some better logic here, as this prevents Stop button
   // from acting on Preview 1 processing
   if (toolSetupHandle >= 0)
      getSystemAPI()->SetActiveToolSetup(toolSetupHandle);

   retVal = CToolObject::StopFrameProcessing();
   if (retVal != 0)
      return retVal;

   return 0;
}

void CScratch::UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                                     bool processingRenderFlag)
{
   UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);

   // Awful hack to make Larry happy - immediately auto-accept
   // following a 'render 1'. This, unfortunately, is the best place
   // I can figure out to do this.
   switch(toolProcessingControlState)
      {
      case TOOL_CONTROL_STATE_STOPPED :
         if (AutoAcceptNextRenderFlag)
         {
            acceptFixOrGOV();
            AutoAcceptNextRenderFlag = false;  // one shot
         }
         if (controlLockFlag)
         {
            DoPostProcessing();
            controlLockFlag = false;
         }
         break;

      default:
      case TOOL_CONTROL_STATE_RUNNING :
      case TOOL_CONTROL_STATE_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
         controlLockFlag = true;
         break;
      }
}

//---------------------------------------------------------------------------

int CScratch::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
   CaptureGUIParameters(pdlEntryToolParams);

   return 0;
}

int CScratch::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   int retVal;

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   CScratchParameters toolParams;

   for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
      {
      CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(i);
      if (toolAttribs->GetElementName() == "ScratchParameters")
         {
         toolParams.ReadPDLEntry(toolAttribs);
         break;
         }
      }

   SetGUIParameters(toolParams);

   return 0;
}

//---------------------------------------------------------------------------

void CScratch::WriteSettings(CIniFile *iniFile, const string &section)

//  This writes the ini files, saving the Tool's platform-independent
//  parameters.  Called by Tool's platform-dependent GUI.
//
//******************************************************************************
{
  return;
}

//-----------------------------------------------------------------------------

void CScratch::ReadSettings(CIniFile *iniFile, const string &section)

//  This reads the init file and sets the tool's platform-independent
//  parameters.  Called by the Tool's platform-dependent GUI.
//******************************************************************************
{
  return;
}

//---------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void CScratchParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
   // Read Scratch Parameters from a PDL Entry's Tool Attributes

   scratchMoves = pdlToolAttribs->GetAttribBool(movesAttr, scratchMoves);

   string typeStr = pdlToolAttribs->GetAttribString(typeAttr, typeAttr);
   if (typeStr == typeSurface)
      scratchType = SCRATCH_TYPE_SURFACE;
   else if (typeStr == typeDeep)
      scratchType = SCRATCH_TYPE_DEEP;

   string orientationStr = pdlToolAttribs->GetAttribString(orientationAttr,
														   orientationVert);
   if (orientationStr == orientationVert)
      scratchHorizontal = false;
   else if (orientationStr == orientationHoriz)
      scratchHorizontal = true;

   string selectSignalStr = pdlToolAttribs->GetAttribString(selectSignalAttr,
															"Y");
   if (selectSignalStr == "Y")
      scratchSelectSignal = SELECT_SIGNAL_Y;
   else if (selectSignalStr == "R")
      scratchSelectSignal = SELECT_SIGNAL_RED;
   else if (selectSignalStr == "G")
      scratchSelectSignal = SELECT_SIGNAL_GREEN;
   else if (selectSignalStr == "B")
      scratchSelectSignal = SELECT_SIGNAL_BLUE;

   string modifySignalStr = pdlToolAttribs->GetAttribString(modifySignalAttr,
                                                            "RGB");
   scratchModifySignal = 0;
   if (modifySignalStr.find('R') != string::npos)
      scratchModifySignal |= MODIFY_SIGNAL_RED;
   if (modifySignalStr.find('G') != string::npos)
      scratchModifySignal |= MODIFY_SIGNAL_GREEN;
   if (modifySignalStr.find('B') != string::npos)
      scratchModifySignal |= MODIFY_SIGNAL_BLUE;

   scratchDetail = pdlToolAttribs->GetAttribInteger(detailAttr, scratchDetail);

   surroundingRectangle = pdlToolAttribs->GetAttribRect(scratchRectangleAttr,
                                                        surroundingRectangle);

   templateFrame = pdlToolAttribs->GetAttribInteger(templateFrameAttr, templateFrame);

   limitRectangle = pdlToolAttribs->GetAttribRect(limitRectangleAttr,
                                                  limitRectangle);
   templateWindow = pdlToolAttribs->GetAttribRect(magnifierRectangleAttr,
                                                  templateWindow);
}

void CScratchParameters::WritePDLEntry(CPDLElement &parent)
{
   // Write Scratch Parameters to a PDL Entry's Tool Attributes

   CPDLElement *pdlToolAttribs = parent.MakeNewChild("ScratchParameters");

   pdlToolAttribs->SetAttribBool(movesAttr, scratchMoves);

   string typeStr;
   if (scratchType == SCRATCH_TYPE_SURFACE)
      typeStr = typeSurface;
   else if (scratchType == SCRATCH_TYPE_DEEP)
      typeStr = typeDeep;
   else
      typeStr = "Invalid";
   pdlToolAttribs->SetAttribString(typeAttr, typeStr);

   string orientationStr;
   if (scratchHorizontal)
      orientationStr = orientationHoriz;
   else
      orientationStr = orientationVert;
   pdlToolAttribs->SetAttribString(orientationAttr, orientationStr);

   string selectSignalStr;
   switch(scratchSelectSignal)
      {
      case SELECT_SIGNAL_Y :
         selectSignalStr = "Y";
         break;
      case SELECT_SIGNAL_RED :
         selectSignalStr = "R";
         break;
      case SELECT_SIGNAL_GREEN :
         selectSignalStr = "G";
         break;
      case SELECT_SIGNAL_BLUE :
         selectSignalStr = "B";
         break;
      }
   pdlToolAttribs->SetAttribString(selectSignalAttr, selectSignalStr);

   string modifySignalStr;
   if (scratchModifySignal & MODIFY_SIGNAL_RED)
      modifySignalStr += 'R';
   if (scratchModifySignal & MODIFY_SIGNAL_GREEN)
      modifySignalStr += 'G';
   if (scratchModifySignal & MODIFY_SIGNAL_BLUE)
      modifySignalStr += 'B';
   pdlToolAttribs->SetAttribString(modifySignalAttr, modifySignalStr);

   pdlToolAttribs->SetAttribInteger(detailAttr, scratchDetail);

   pdlToolAttribs->SetAttribRect(scratchRectangleAttr, surroundingRectangle);
   pdlToolAttribs->SetAttribInteger(templateFrameAttr, templateFrame);
   pdlToolAttribs->SetAttribRect(limitRectangleAttr, limitRectangle);
   pdlToolAttribs->SetAttribRect(magnifierRectangleAttr, templateWindow);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CScratchProc::CScratchProc(int newToolNumber, const string &newToolName,
                                 CToolSystemInterface *newSystemAPI,
                                 const bool *newEmergencyStopFlagPtr,
                                 IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor)
{
  AlgorithmPS = new CAlgorithmPS;
  StartProcessThread(); // Required by Tool Infrastructure
}

CScratchProc::~CScratchProc()
{
  delete AlgorithmPS;
  AlgorithmPS = NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////


int CScratchProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   int iRet;

/* TTT
   SetInputMaxFrames(0,1);
   SetOutputModifyInPlace(0, 0, false);
   SetOutputMaxFrames(0, 1);
*/

   SetInputMaxFrames(0, 3, 1);// Input Port 0 = 5 frame
   SetOutputNewFrame(0, 0);   // Output to new frame with same image format
   SetOutputMaxFrames(0, 1);  // One frame out

   // initialize the Peristent Scratch Algorithm
   AlgorithmPS->Initialize();

   const CImageFormat *imageFormat = GetSrcImageFormat(0);

   // Note: the number of components is hardcoded
   // TBD: Read this form image format
   RECT Rect = imageFormat->getActivePictureRect();

   iRet = AlgorithmPS->SetDimension(imageFormat->getTotalFrameHeight(),
        imageFormat->getTotalFrameWidth(),
        imageFormat->getComponentCountExcAlpha(),
        Rect.top, Rect.bottom+1, Rect.left, Rect.right+1);

   if (iRet != 0)
     {
        MTIostringstream os;
        os << "Scratch SetDimension in BeginProcess failed, Error: " << iRet;
        _MTIErrorDialog(os.str());
        return iRet;
     }

   MTI_UINT16 cvMin[MAX_COMPONENT_COUNT];
   MTI_UINT16 cvMax[MAX_COMPONENT_COUNT];

   imageFormat->getComponentValueMin(cvMin);
   imageFormat->getComponentValueMax(cvMax);

   iRet = AlgorithmPS->SetImageBounds(cvMin, cvMax);
   if (iRet != 0)
     {
        MTIostringstream os;
        os << "Scratch SetImageBounds in BeginProcess failed, Error: " << iRet;
        _MTIErrorDialog(os.str());
        return iRet;
     }

   // pass a pair of color space converters appropriate to the image format
   AlgorithmPS->SetColorSpaceConvert(
                   GScratchTool->getToRGBConverter(),
                   GScratchTool->getFmRGBConverter());
   return 0;

}

int CScratchProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];

/* TTT
   inFrameIndex  = frameRange.inFrameIndex;
   outFrameIndex = frameRange.outFrameIndex;
*/

   inFrameIndex = frameRange.inFrameIndex + 1;
   outFrameIndex = frameRange.outFrameIndex - 1;

   // Iteration Count
   iterationCount = outFrameIndex - inFrameIndex;  // Exclusive out frame

   return 0;
}

int CScratchProc::SetParameters(CToolParameters *toolParams)
{
   CScratchParameters *afpToolParams
                             = static_cast<CScratchParameters*>(toolParams);

   // Call the parameter update in the tool
   int iRet = AlgorithmPS->AssignParameter(afpToolParams);
   if (iRet != 0)
     {
        MTIostringstream os;
        os << "Scratch Set Parameters failed, Error: " << iRet;
        _MTIErrorDialog(os.str());
        return iRet;
     }

   return 0;
}

int CScratchProc::BeginProcessing(SToolProcessingData &procData)
{
   // This tool processes one frame per iteration
   GetToolProgressMonitor()->SetFrameCount(iterationCount);
   GetToolProgressMonitor()->SetStatusMessage("Processing");
   GetToolProgressMonitor()->StartProgress();

   // Success, continue
   return 0;
}

int CScratchProc::BeginIteration(SToolProcessingData &procData)
{
/* TTT
   int inputFrameIndexList[1];
   inputFrameIndexList[0] = inFrameIndex + procData.iteration;
   SetInputFrames(0, 1, inputFrameIndexList);
*/

   int inputFrameIndexList[3];

   int j = 0;
   for (int i = -1; i <= 1; ++i)
     {
       inputFrameIndexList[j] = inFrameIndex + procData.iteration + i;
       ++j;
     }

   // inputFrameIndexList now contains the frame indices we want as input
   SetInputFrames(0, 3, inputFrameIndexList);

   // when finished processing, release the oldest frame
   SetFramesToRelease(0, 1, inputFrameIndexList);

   // These are the frames that will be the output
   int outputFrameIndexList[1];
   outputFrameIndexList[0] = inFrameIndex + procData.iteration;
   SetOutputFrames(0, 1, outputFrameIndexList);

   return 0;
}

int CScratchProc::GetIterationCount(SToolProcessingData &procData)
{
   return iterationCount;
}

int CScratchProc::DoProcess(SToolProcessingData &procData)
{
// int frameIndex = inFrameIndex + procData.iteration;

   CAutoErrorReporter autoErr("CScratchProc::DoProcess",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   MTI_UINT16 *uspaSrcArg[MAX_FRAME];
   long laFrameIndex[MAX_FRAME];

   // Define the pointers to the Persistent Scratch library
   laFrameIndex[0] = -1;
   laFrameIndex[1] =  0;
   laFrameIndex[2] =  1;

   //  TBD:  analyze the shot detection list and select the correct 3 frames
   //  for now always take frame -1, 0, 1
   for (int i=0; i < 3; i++)
    {
       CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, i);
       uspaSrcArg[i] = inToolFrameBuffer->GetVisibleFrameBufferPtr();
    }

   int iRet = AlgorithmPS->SetSrcImgPtr(uspaSrcArg, laFrameIndex);
   if (iRet != 0)
      {
      autoErr.errorCode = iRet;
      autoErr.msg << "Scratch SetSrcImgPtr in DoProcess failed, Error: " << iRet;
      return iRet;
      }

   // Define the destination frame to the process
   CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
   CToolFrameBuffer *inToolFrameBuffer  = GetRequestedInputFrame(0, 1);

   // Copy the original image, including invisible fields, to the output frame
   CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBuffer, outToolFrameBuffer);

   AlgorithmPS->SetDstImgPtr(outToolFrameBuffer->GetVisibleFrameBufferPtr());

   // Now setup the pixel region list
   AlgorithmPS->SetPixelRegionList(
                    outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr());

   iRet = AlgorithmPS->Process();
   if (iRet != 0)
      {
      autoErr.errorCode = iRet;
      autoErr.msg << "Scratch Processing failed, Error: " << iRet;
      return iRet;
      }

   return 0;
}

int CScratchProc::EndIteration(SToolProcessingData &procData)
{
   GetToolProgressMonitor()->BumpProgress();

   return 0;
}

int CScratchProc::EndProcessing(SToolProcessingData &procData)
{
   if (procData.iteration == iterationCount)
   {
      GetToolProgressMonitor()->SetStatusMessage("Processing complete");
      GetToolProgressMonitor()->StopProgress(true);
   }
   else
   {
      GetToolProgressMonitor()->SetStatusMessage("Stopped");
      GetToolProgressMonitor()->StopProgress(false);
   }
   return 0;
}
