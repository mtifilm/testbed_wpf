object ScratchForm: TScratchForm
  Left = 585
  Top = 207
  BorderStyle = bsToolWindow
  Caption = 'Scratch Tool - MTI'
  ClientHeight = 715
  ClientWidth = 512
  Color = clBtnFace
  Constraints.MaxHeight = 744
  Constraints.MaxWidth = 518
  Constraints.MinHeight = 744
  Constraints.MinWidth = 518
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ScratchControlPanel: TColorPanel
    Left = 0
    Top = 0
    Width = 512
    Height = 720
    BevelOuter = bvNone
    Color = 6974058
    Constraints.MaxHeight = 720
    Constraints.MaxWidth = 512
    Constraints.MinHeight = 720
    Constraints.MinWidth = 512
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      512
      720)
    object ScratchWindowLabel2: TLabel
      Left = 280
      Top = 138
      Width = 193
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Scratch Window'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = ScratchWindowLabelClick
    end
    object EdgeBlurPanel: TPanel
      Left = 128
      Top = 540
      Width = 237
      Height = 81
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Visible = False
      OnClick = BackgroundPanelClick
      object Label5: TLabel
        Left = 207
        Top = 53
        Width = 18
        Height = 13
        Caption = '100'
        Enabled = False
      end
      object Label6: TLabel
        Left = 15
        Top = 53
        Width = 6
        Height = 13
        Caption = '0'
        Enabled = False
      end
      object EdgeBlurLabel: TLabel
        Left = 19
        Top = 15
        Width = 45
        Height = 13
        Caption = 'Edge Blur'
        Enabled = False
      end
      object Label8: TLabel
        Left = 116
        Top = 15
        Width = 12
        Height = 13
        Caption = '50'
        Enabled = False
      end
      object EdgeBlurTrackBar: TTrackBar
        Left = 12
        Top = 35
        Width = 215
        Height = 17
        Enabled = False
        Max = 100
        Position = 50
        TabOrder = 0
        TabStop = False
        ThumbLength = 15
        TickMarks = tmBoth
        TickStyle = tsNone
        OnEnter = EdgeBlurTrackBarEnter
        OnExit = EdgeBlurTrackBarExit
      end
    end
    object BackgroundDetailPanel: TPanel
      Left = 136
      Top = 546
      Width = 237
      Height = 81
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object RangeMax: TLabel
        Left = 207
        Top = 53
        Width = 18
        Height = 13
        Caption = '100'
      end
      object RangeMin: TLabel
        Left = 15
        Top = 57
        Width = 6
        Height = 13
        Caption = '0'
      end
      object BackgroundToleranceLabel: TLabel
        Left = 19
        Top = 15
        Width = 85
        Height = 13
        Caption = 'Background detail'
      end
      object BackgroundToleranceValueLabel: TLabel
        Left = 148
        Top = 15
        Width = 12
        Height = 13
        Caption = '50'
      end
      object BackgroundToleranceTrackBar: TTrackBar
        Left = 12
        Top = 35
        Width = 215
        Height = 17
        Max = 100
        Position = 50
        TabOrder = 0
        ThumbLength = 15
        TickMarks = tmBoth
        TickStyle = tsNone
        OnChange = BackgroundToleranceTrackBarChange
        OnEnter = BackgroundToleranceTrackBarEnter
        OnExit = BackgroundToleranceTrackBarExit
      end
    end
    object BottomRightClickPanel: TPanel
      Left = 0
      Top = 500
      Width = 512
      Height = 189
      BevelOuter = bvNone
      TabOrder = 7
      OnClick = BackgroundPanelClick
      inline BackgroundDetailTrackEdit: TTrackEditFrame
        Left = 136
        Top = 60
        Width = 217
        Height = 51
        Hint = 'Specify how much background detail'#13#10'to preserve in the fix area'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        ExplicitLeft = 136
        ExplicitTop = 60
        inherited MinLabel: TLabel
          Width = 6
          Caption = '0'
          ExplicitWidth = 6
        end
        inherited MaxLabel: TLabel
          Left = 152
          Width = 18
          Caption = '100'
          ExplicitLeft = 152
          ExplicitWidth = 18
        end
        inherited TitleLabel: TColorLabel
          Width = 86
          Caption = 'Background Detail'
          ExplicitWidth = 86
        end
        inherited EditAlignmentPanel: TPanel
          Anchors = [akRight, akBottom]
        end
        inherited DeferredUpdateTimer: TTimer
          Left = 102
          Top = 10
        end
      end
      object MirrorGraphsCheckBox: TCheckBox
        Left = 82
        Top = 1
        Width = 148
        Height = 17
        Hint = 'Show the scratch window graph overlay on the image (Ctrl+T)'
        Caption = 'Mirror graphs as overlay'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 0
        OnClick = MirrorGraphsCheckBoxClick
      end
    end
    object ToolbarPanel: TPanel
      Left = 0
      Top = 636
      Width = 505
      Height = 57
      AutoSize = True
      BevelOuter = bvNone
      BorderWidth = 6
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Visible = False
      object DoNotDeleteLable: TLabel
        Left = 156
        Top = 29
        Width = 3
        Height = 13
      end
      object ResumeGroupBox: TGroupBox
        Left = 358
        Top = 6
        Width = 141
        Height = 45
        Caption = 'Resume'
        TabOrder = 1
        Visible = False
        object GotoTCButton: TSpeedButton
          Left = 10
          Top = 15
          Width = 23
          Height = 22
          Hint = 'Go to this timecode'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333FF3333333333333003333333333333F77F33333333333009033
            333333333F7737F333333333009990333333333F773337FFFFFF330099999000
            00003F773333377777770099999999999990773FF33333FFFFF7330099999000
            000033773FF33777777733330099903333333333773FF7F33333333333009033
            33333333337737F3333333333333003333333333333377333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = GotoTCButtonClick
        end
        object SetStartTCButton: TSpeedButton
          Left = 106
          Top = 15
          Width = 23
          Height = 22
          Hint = 'Set to displayed timecode'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFF8888FFFFFFFFFFFF8888FFFFFFFFFF88111178F
            FFFFFFF88111178FFFFFFF8711111178FFFFFF8717771178FFFFF88119191117
            FFFFF88177777717FFFFF811979191118FFFF817777777718FFFF819D9791911
            8FFFF817787777718FFFF81DDBD791118FFFF817887777718FFFF81DB8BD7911
            8FFFF818888777118FFFFF71DBD79117FFFFFF7188777717FFFFFF811D791118
            FFFFFF8117777718FFFFFFF87111178FFFFFFFF87111178FFFFFFFFFF8888FFF
            FFFFFFFFF8888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          OnClick = SetStartTCButtonClick
        end
        object ResumeTCEdit: VTimeCodeEdit
          Left = 34
          Top = 15
          Width = 72
          Height = 19
          TabStop = False
          TabOrder = 0
          AllowVTRCopy = False
        end
      end
      object TBToolbar1: TToolBar
        Left = 6
        Top = 6
        Width = 493
        Height = 26
        Caption = 'Scratch '
        TabOrder = 0
        Visible = False
        object PreviewFrameButton: TToolButton
          Left = 0
          Top = 0
          Hint = 'Preview one frame (D)'
          ImageIndex = 15
          OnClick = PreviewFrameButtonClick
        end
        object PreviewAllButton: TToolButton
          Left = 23
          Top = 0
          Hint = 'Preview between the marks (Shift+D)'
          Caption = 'Preview   '
          ImageIndex = 16
          OnClick = PreviewAllButtonClick
        end
        object RenderFrameButton: TToolButton
          Left = 46
          Top = 0
          Hint = 'Render One Frame (G)'
          ImageIndex = 13
          OnClick = RenderFrameButtonClick
        end
        object RenderAllButton: TToolButton
          Left = 69
          Top = 0
          Hint = 'Render to disk between the marks (Shift+G)'
          Caption = 'Render   '
          ImageIndex = 3
          OnClick = RenderAllButtonClick
        end
        object StopButton: TToolButton
          Left = 92
          Top = 0
          Hint = 'Stop after the next frame is done (Ctrl+Space)'
          Caption = 'Stop  '
          ImageIndex = 11
          OnClick = StopButtonClick
        end
        object HighLightButton: TToolButton
          Left = 115
          Top = 0
          Hint = 'Show highlight or fix'
          Caption = 'Highlight'
          ImageIndex = 5
          Visible = False
        end
        object PDLCaptureButton: TToolButton
          Left = 138
          Top = 0
          Hint = 'Add event to PDL'
          Caption = 'PDL'
          ImageIndex = 12
          OnClick = PDLCaptureButtonClick
        end
      end
    end
    object StatusPanel: TPanel
      Left = -640
      Top = 699
      Width = 1152
      Height = 20
      Anchors = [akLeft, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvLowered
      TabOrder = 4
    end
    object ScratchWidthReadoutPanel: TPanel
      Left = 280
      Top = 496
      Width = 193
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = BackgroundPanelClick
      object ScratchWidthLabel: TLabel
        Left = 20
        Top = 4
        Width = 74
        Height = 13
        Caption = 'Scratch Width: '
      end
      object ScratchWidthValueLabel: TLabel
        Left = 92
        Top = 4
        Width = 45
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = '????'
      end
      object PixelsLabel: TLabel
        Left = 143
        Top = 4
        Width = 27
        Height = 13
        Caption = 'pixels'
      end
      object NotSetYetPanel: TPanel
        Left = 92
        Top = 3
        Width = 78
        Height = 15
        BevelOuter = bvNone
        Color = 6974058
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        object NotSetYetText: TStaticText
          Left = 0
          Top = 1
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Not set yet'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object MarkInWarningPanel: TPanel
      Left = 280
      Top = 516
      Width = 193
      Height = 17
      BevelOuter = bvNone
      TabOrder = 9
      OnClick = BackgroundPanelClick
      object WarningLabel: TLabel
        Left = 18
        Top = 2
        Width = 47
        Height = 13
        Caption = 'Warning: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object WarningMessage: TLabel
        Left = 66
        Top = 2
        Width = 111
        Height = 14
        Alignment = taRightJustify
        Caption = 'Not on Mark IN frame !  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MotOnMarkInLabel'
        Font.Style = []
        ParentFont = False
      end
    end
    object ChannelControlPanel: TPanel
      Left = 0
      Top = 110
      Width = 512
      Height = 390
      BevelOuter = bvNone
      TabOrder = 1
      OnClick = BackgroundPanelClick
      object YLabel: TLabel
        Left = 248
        Top = 278
        Width = 13
        Height = 13
        AutoSize = False
        Caption = 'Y'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object RLabel: TLabel
        Left = 248
        Top = 71
        Width = 13
        Height = 13
        AutoSize = False
        Caption = 'R'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object GLabel: TLabel
        Left = 248
        Top = 140
        Width = 13
        Height = 13
        AutoSize = False
        Caption = 'G'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BLabel: TLabel
        Left = 248
        Top = 209
        Width = 17
        Height = 13
        AutoSize = False
        Caption = 'B'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ImageLabel: TLabel
        Left = 220
        Top = 347
        Width = 45
        Height = 24
        AutoSize = False
        Caption = 'Image'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DetectLabel: TLabel
        Left = 56
        Top = 28
        Width = 65
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Detect'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = DetectLabelClick
      end
      object ProcessLabel: TLabel
        Left = 136
        Top = 28
        Width = 73
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Process'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = ProcessLabelClick
      end
      object ScratchWindowLabel1: TLabel
        Left = 280
        Top = 28
        Width = 193
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Scratch Window'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = ScratchWindowLabelClick
      end
      object DummyGraphYPanel: TPanel
        Left = 278
        Top = 252
        Width = 194
        Height = 66
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Color = clWhite
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 5
        object YGraphImage: TImage
          Left = 0
          Top = 0
          Width = 192
          Height = 64
          OnMouseDown = ImageMouseDown
        end
      end
      object NarrowWidthGaugePanel: TPanel
        Left = 3
        Top = 311
        Width = 269
        Height = 21
        BevelOuter = bvNone
        TabOrder = 8
      end
      object DummyGraphRPanel: TPanel
        Left = 278
        Top = 45
        Width = 194
        Height = 66
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Color = clWhite
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        object RGraphImage: TImage
          Left = 0
          Top = 0
          Width = 192
          Height = 64
          OnMouseDown = ImageMouseDown
        end
      end
      object DummyGraphGPanel: TPanel
        Left = 278
        Top = 114
        Width = 194
        Height = 66
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Color = clWhite
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        object GGraphImage: TImage
          Left = 0
          Top = 0
          Width = 192
          Height = 64
          OnMouseDown = ImageMouseDown
        end
      end
      object DummyGraphBPanel: TPanel
        Left = 278
        Top = 183
        Width = 194
        Height = 66
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Color = clWhite
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 7
        object BGraphImage: TImage
          Left = 0
          Top = 0
          Width = 192
          Height = 64
          OnMouseDown = ImageMouseDown
        end
      end
      object DummyGraphImagePanel: TPanel
        Left = 278
        Top = 321
        Width = 194
        Height = 66
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Color = clWhite
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 6
        object MagnifierImage: TImage
          Left = 0
          Top = 0
          Width = 192
          Height = 64
          OnMouseDown = ImageMouseDown
        end
      end
      object RightSideRightClickPanel: TPanel
        Left = 476
        Top = 20
        Width = 37
        Height = 393
        BevelOuter = bvNone
        TabOrder = 9
        OnClick = BackgroundPanelClick
      end
      object DetectButtonPanel: TPanel
        Left = 67
        Top = 56
        Width = 67
        Height = 249
        BevelOuter = bvNone
        Color = 6974058
        Ctl3D = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 2
        object RDetectButton: TRadioButton
          Left = 15
          Top = 15
          Width = 15
          Height = 15
          Hint = 'Detect scratch in RED channel'
          Constraints.MaxWidth = 15
          TabOrder = 1
          OnClick = DetectButtonClick
          OnEnter = DetectButtonEnter
          OnExit = DetectButtonExit
        end
        object GDetectButton: TRadioButton
          Left = 15
          Top = 84
          Width = 15
          Height = 15
          Hint = 'Detect scratch in GREEN channel'
          Constraints.MaxWidth = 15
          TabOrder = 2
          OnClick = DetectButtonClick
          OnEnter = DetectButtonEnter
          OnExit = DetectButtonExit
        end
        object BDetectButton: TRadioButton
          Left = 15
          Top = 153
          Width = 15
          Height = 15
          Hint = 'Detect scratch in BLUE channel'
          Constraints.MaxWidth = 15
          TabOrder = 3
          OnClick = DetectButtonClick
          OnEnter = DetectButtonEnter
          OnExit = DetectButtonExit
        end
        object YDetectButton: TRadioButton
          Left = 15
          Top = 222
          Width = 15
          Height = 15
          Hint = 'Detect scratch by RGB combined luminance'
          Checked = True
          Constraints.MaxWidth = 15
          TabOrder = 0
          TabStop = True
          OnClick = DetectButtonClick
          OnEnter = DetectButtonEnter
          OnExit = DetectButtonExit
        end
      end
      object ProcessCheckBoxPanel: TPanel
        Left = 140
        Top = 56
        Width = 69
        Height = 249
        BevelOuter = bvNone
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 3
        DesignSize = (
          69
          249)
        object RCheckBox: TCheckBox
          Left = 26
          Top = 15
          Width = 41
          Height = 15
          Hint = 'Apply fixes on the RED channel'
          Anchors = [akTop, akRight]
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          State = cbChecked
          TabOrder = 1
          OnEnter = RGBCheckBoxEnter
          OnExit = RGBCheckBoxExit
        end
        object GCheckBox: TCheckBox
          Left = 26
          Top = 84
          Width = 41
          Height = 15
          Hint = 'Apply fixes on the GREEN channel'
          TabStop = False
          Anchors = [akTop, akRight]
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          State = cbChecked
          TabOrder = 2
          OnEnter = RGBCheckBoxEnter
          OnExit = RGBCheckBoxExit
        end
        object BCheckBox: TCheckBox
          Left = 26
          Top = 153
          Width = 41
          Height = 15
          Hint = 'Apply fixes on the BLUE channel'
          TabStop = False
          Anchors = [akTop, akRight]
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnEnter = RGBCheckBoxEnter
          OnExit = RGBCheckBoxExit
        end
      end
      object ShowReticlesCheckBox: TCheckBox
        Left = 82
        Top = 368
        Width = 97
        Height = 17
        Hint = 'Show the blue and red overlays on the image (Shift+T)'
        TabStop = False
        Caption = 'Show Reticles'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 4
        OnClick = ShowReticlesCheckBoxClick
      end
    end
    object TabCoverUpPanel: TPanel
      Left = 0
      Top = 45
      Width = 512
      Height = 84
      BevelOuter = bvNone
      TabOrder = 0
      OnClick = BackgroundPanelClick
      object OrientationGroupBox: TGroupBox
        Left = 33
        Top = 19
        Width = 182
        Height = 45
        Caption = ' Orientation '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Visible = False
        object VerticalButton: TRadioButton
          Left = 22
          Top = 20
          Width = 55
          Height = 15
          Caption = 'Vertical'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          TabStop = True
          OnClick = VerticalButtonClick
          OnEnter = OrientationEnter
          OnExit = OrientationExit
        end
        object HorizontalButton: TRadioButton
          Left = 102
          Top = 20
          Width = 77
          Height = 15
          Caption = 'Horizontal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = HorizontalButtonClick
          OnEnter = OrientationEnter
          OnExit = OrientationExit
        end
      end
      object MoveGroupBox: TGroupBox
        Left = 289
        Top = 19
        Width = 182
        Height = 45
        Caption = ' Movement '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object NoMoveButton: TRadioButton
          Left = 102
          Top = 20
          Width = 32
          Height = 15
          Hint = 'Select '#39'no'#39' if the scratch does not move significantly'
          Caption = 'No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Pitch = fpFixed
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = MovementButtonClick
          OnEnter = MovementEnter
          OnExit = MovementExit
        end
        object YesMoveButton: TRadioButton
          Left = 22
          Top = 20
          Width = 39
          Height = 15
          Hint = 'Select '#39'yes'#39' if the scratch moves  significantly'
          Caption = 'Yes'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TabStop = True
          OnClick = MovementButtonClick
          OnEnter = MovementEnter
          OnExit = MovementExit
        end
      end
      object DensityGroupBox: TGroupBox
        Left = 33
        Top = 19
        Width = 182
        Height = 45
        Caption = ' Density '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object LightDensityButton: TRadioButton
          Left = 22
          Top = 20
          Width = 55
          Height = 15
          Hint = 'Detect light scratches'#13#10'Press 1 to toggle between light and dark'
          Caption = 'Light'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          TabStop = True
          OnClick = LightDensityButtonClick
          OnEnter = DensityEnter
          OnExit = DensityExit
        end
        object DarkDensityButton: TRadioButton
          Left = 102
          Top = 20
          Width = 77
          Height = 15
          Hint = 'Detect dark scratches'#13#10'Press 1 to toggle between light and dark'
          Caption = 'Dark'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = DarkDensityButtonClick
          OnEnter = DensityEnter
          OnExit = DensityExit
        end
      end
    end
    object TopDummyPanel: TPanel
      Left = 0
      Top = -4
      Width = 512
      Height = 53
      BevelOuter = bvNone
      TabOrder = 10
      object ScratchNamePanel: TPanel
        Left = 0
        Top = 4
        Width = 512
        Height = 32
        BevelOuter = bvNone
        TabOrder = 0
        object ScratchNameLabel: TLabel
          Left = 232
          Top = 0
          Width = 60
          Height = 19
          Caption = 'Scratch'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBtnText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
      end
    end
    inline ExecButtonsToolbar: TExecButtonsFrame
      Left = 12
      Top = 636
      Width = 480
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      ExplicitLeft = 12
      ExplicitTop = 636
      ExplicitWidth = 480
      inherited ButtonPressedNotifier: TSpeedButton
        OnClick = ExecButtonsToolbar_ButtonPressedNotifier
      end
      inherited ResumeGroupBox: TGroupBox
        Left = 343
        ExplicitLeft = 343
      end
    end
    inline ExecStatusBar: TExecStatusBarFrame
      Left = 16
      Top = 682
      Width = 512
      Height = 38
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      ExplicitLeft = 16
      ExplicitTop = 682
      ExplicitWidth = 512
    end
    object ScratchWindowFocusPanel: TPanel
      Left = 256
      Top = 29
      Width = 10
      Height = 10
      BevelOuter = bvSpace
      TabOrder = 6
      TabStop = True
      OnEnter = ScratchWindowFocusPanelEnter
      OnExit = ScratchWindowFocusPanelExit
    end
  end
  object ButtonImageList: TImageList
    Height = 20
    Width = 20
    Left = 92
    Top = 4
    Bitmap = {
      494C010111001300040014001400FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000050000000640000000100200000000000007D
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000C0C0C00000000000000000008080800080808000000000008080
      80000000000080808000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000C0C0C00000000000FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      00008080800000000000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000D8E9EC00D8E9EC00D8E9EC0080808000C0C0
      C000D8E9EC0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000D8E9EC00FFFF0000D8E9EC0080808000C0C0
      C000D8E9EC0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000080808000FFFF0000FFFF000080808000C0C0
      C0008080800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000080808000D8E9EC00808080008080
      800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000C0C0C0000000000000000000C0C0C0000000000000000000C0C0
      C0000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000C0C0C0000000000000000000C0C0C0000000000000000000C0C0
      C0000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5B1B00000000000D5E9ED008B81720088765F0000000000C7D9DC00969E
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080008080800080808000808080008080
      800080808000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009B9F
      9A00BAA28500999B9400A0AAA900A993790098846D00969E9A008B8A82008B72
      560098A09E000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A2AD
      AB00C2AA9000C6AE9400C7B09700CDB9A100C7B29B00BAA28800B1998100A28B
      72009AA29F000000000000000000000000000000000000000000000000008080
      8000000080000000800000008000000080000000800000008000000080000000
      8000000080000000800000008000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000009EA7A300A0A8A500A29B
      8A00D5C0A900E4D3C000DFCDB900DDCBB800D9C5B200D2BFAA00C4AE9500BBA3
      8900998E7C009DA8A6009AA1A000000000000000000080808000808080000000
      8000000080000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000008000000080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B29F8600C9B19500E0CB
      B600E8D6C400E1CFBD00D2BEA700BBA89200B7A68D00C3AB9200C0A88E00C6B3
      9D00BEA78F00AE92780098806500D1E6EA008080800080808000000080000000
      80000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000080000000800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DAC5AF00E9D9
      C600DDCCB700C6AE9800000000000000000000000000A6B1B000BAA28700C5AF
      9700CAB7A200AB957D00A8B5B600000000008080800000008000000080000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000800000008000000000000000
      0000000000000000000000000000C0C0C0000000000000000000C0C0C0000000
      0000808080008080800000000000808080000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000AAB5B500D7C2AA00E7D7
      C400C4B096009EA6A400C0C0C0000000000000000000000000009EA4A000BFA7
      8C00CBB7A200A895800080FFFF0000000000000080000000FF000000FF00C0C0
      C0000000FF000000FF000000FF00808080000000FF000000FF00808080008080
      80000000FF000000FF000000FF000000FF000000FF0000008000000000000000
      0000000000000000000000000000C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFF0000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000C0C0C00000000000BCAA9500D7C3AA00E6D0BD00E5D3
      C000A6917A0000000000C0C0C000000000000000000000000000C0C0C000BCA4
      8900CCB9A300BCA28400AE947900947F6700000080000000FF00000000000000
      FF00000000000000FF000000FF00000000000000FF000000FF0000000000C0C0
      C000808080000000FF00C0C0C0000000FF000000FF0000008000000000000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF0000000000D8E9
      EC00D8E9EC00D8E9EC00FFFFFF0080808000D8E9EC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00A4A09400C2B4A100DAC6AD00E4D1
      BD00A38C730000000000C0C0C00000000000000000000000000000000000C2AA
      8B00CCBAA500BBA28500A39077008C877B00000080000000FF000000FF000000
      FF00000000000000FF000000FF00000000000000FF00808080000000FF000000
      FF00000000000000FF00C0C0C0000000FF000000FF0000008000000080000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF0000000000D8E9
      EC00FFFF0000D8E9EC00FFFFFF0080808000D8E9EC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00D6EAEE00C7D8DC00CAB39A00F2E2
      D000BFAA9000959B9800C0C0C0000000000000000000000000009DA19B00C7B0
      9700CEBCA600BCA386000000000000000000008080000000FF00C0C0C0000000
      0000808080000000FF000000FF00000000000000FF00C0C0C0000000FF000000
      FF00000000000000FF00C0C0C000000000000000000000808000000080000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00000000008080
      8000FFFF0000FFFF0000FFFFFF00808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00A09E9500D8C2AA00EEDF
      CD00E0CFBC00B49F83008C8B820000000000000000009A958800C7AE9500D5C5
      B200D3C2AE00BEA287008F8E850000000000008080000000FF00000000008080
      80000000FF000000FF000000FF00000000000000FF00C0C0C0000000FF000000
      FF00000000000000FF00C0C0C000808080008080800080808000000080000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000080808000D8E9EC00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00CDE0E300C2AF9600D5C0A700E2CE
      BA00F2E2D200DFCDBB00C7B39A00B89F8300B9A08400CBB59C00CDB9A500D3BF
      AB00C9B29700BCA18600A68C720000000000000080000000FF00C0C0C0000000
      FF00000000000000FF000000FF00000000000000FF000000FF00C0C0C0000000
      FF00C0C0C0000000FF00C0C0C0000000FF000000FF0080808000000080000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00B3C2C400B1C0C000A9B4
      B300D7C2AC00EFDFCE00ECDCCB00ECDBCA00E7D6C300DECCB600D5C4AD00CBB5
      9A009EA39F00AFBCBD000000000000000000008080000000FF00C0C0C0000000
      00000000FF00C0C0C000C0C0C000C0C0C000C0C0C00000008000C0C0C000C0C0
      C0000000FF000000FF00C0C0C000C0C0C000C0C0C00000008000000080000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A9B2
      B100D0B89D00D7C3AB00DBC7B100E6D1BD00E3CDB700C7B09500C5AE9400BEA8
      8F00A9B2B200000000000000000000000000808080000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF0000008000000000000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00B0A38F00ACB6B600CAD9DC00C2AD9600CBB49900BAC8C800A1A6A200AF9D
      880000000000000000000000000000000000C0C0C000C0C0C0000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000800000008000C0C0C0000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000DAEBEE00A69D8D00B8A48E00DAEBEE00000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C0000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000008000C0C0C000000000000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C000C0C0
      C0000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000800000008000C0C0C00000000000000000000000
      0000000000000000000000000000C0C0C0000000000000000000C0C0C0000000
      000000000000C0C0C00000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000C0C0C00000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000800000008000C0C0C0000000000000000000000000000000
      0000000000000000000000000000C0C0C0000000000000000000C0C0C0000000
      000000000000C0C0C00000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000C0C0C00000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C000C0C0C0000000FF000000FF000000FF000000FF000000
      FF000000800000008000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006060FF000000
      80000000800000008000000080000000800000008000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000A4A0A000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF006060FF006060FF000000
      FF000000FF000000FF000000FF000000FF000000800000008000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      00000000000000000000FFFFFF00FFFFFF006060FF006060FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000800000008000FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000C0C0
      C0000000000000000000C0C0C000C0DCC000C0C0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C00000000000000000000000
      0000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0
      C000000000000000000000000000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF00FFFFFF006060FF006060FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000080000000
      8000FFFFFF00FFFFFF000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0C0000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF006060FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      800000008000FFFFFF000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C0000000
      00000000000000000000C0C0C000C0C0C000000000000000000000000000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000000000000000000000000000C0C0C000C0C0C000000000000000
      000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000FFFFFF000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000A4A0A000C0C0C000C0C0C000C0C0C000000000000000
      0000C0C0C000C0C0C000A4A0A000C0C0C000C0C0C000C0C0C000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000C0C0C000C0C0C00000000000000000000000
      0000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000000000000000000000000000C0C0C000C0C0C00000000000000000000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000FFFFFF000000000000000000000000000000000000000000C0DC
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00F0FB
      FF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C0000000
      00000000000000000000C0C0C000C0C0C000000000000000000000000000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000000000000000000000000000C0C0C000C0C0C000000000000000
      000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000FFFFFF000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF00C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF00000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0C0000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000FFFFFF000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF00C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C00000000000000000000000
      0000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000C0C0C000C0C0
      C000000000000000000000000000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008000FFFFFF000000000000000000000000000000000000000000C0C0
      C000F0FBFF00F0FBFF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000FFFFFF00F0FBFF0000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF006060FF006060FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      800000008000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00C0C0C000C0C0C000C0C0C000C0C0C000A4A0A000C0C0C000FFFFFF00F0FB
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      000000000000FFFFFF00FFFFFF006060FF006060FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000080000000
      8000FFFFFF00FFFFFF000000000000000000000000000000000000000000C0C0
      C000A4A0A000C0C0C000C0C0C000C0C0C000F0FBFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF006060FF006060FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000800000008000FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF006060FF006060FF000000
      FF000000FF000000FF000000FF000000FF000000800000008000FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000C0C0
      C000F0FBFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006060FF006060
      FF006060FF006060FF006060FF006060FF0000008000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000F0FB
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000C0C0C0000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000000000
      0000000000000000000000000000808080000000000080808000000000008080
      8000000000000000000000FF0000000000000000000000000000000000008080
      8000000000008080800000000000000000000000000000000000000000008080
      800000000000808080000000000080808000000000000000000000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      800080808000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000080808000000000008080800000000000000000008080
      8000000000000000000000000000000000008080800000000000808080000000
      0000808080000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000808080000000000000000000000000000000
      0000808080000000000080808000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000000000000000000000FF00000000000000FF000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      000000000000808080000000000080808000000000000000000000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000FF000000FF000000FF000000
      0000000000000000000000000000808080000000000000000000808080000000
      00000000000000FF000000000000000000000000000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000080808000000000000000000000FF0000000000000000
      0000000000000000000000000000806060004020200000000000000000000000
      0000402020000000000000000000806060000000000000000000000000000000
      0000402020000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000FF000000FF000000FF000000
      0000000000000000000000000000808080000000000080808000000000000000
      FF000000FF000000FF0000FF00000000000000FF000000000000000000008080
      8000000000008080800000000000000000000000000000000000000000008080
      800000000000808080000000000080808000000000000000000000FF00000000
      000000000000000000000000000080808000A4A0A00080606000808080008080
      8000000000000000000000000000808080008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000FF000000FF000000FF000000
      0000000000000000000080808000808080000000000000000000808080000000
      FF000000FF000000FF0000000000000000000000000000FF0000000000000000
      0000808080000000000080808000000000000000000000000000808080008080
      8000000000000000000080808000000000000000000000FF0000000000000000
      000000000000000000000000000080808000F0FBFF00A4A0A000A4A0A0008080
      800000000000000000000000000080808000F0FBFF00A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000080808000000000000000
      0000000000000000000080808000808080000000000080808000000000000000
      FF000000FF000000FF0000FF00000000000000FF000000000000000000008080
      8000000000008080800000000000000000000000000000000000808080008080
      80000000000080808000000000000000000000FF00000000000000FF00000000
      0000000000000000000000000000808080000000000080808000806060008080
      8000000000000000000000000000808080000000000080808000806060008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      00000000000000FF000000000000000000000000000000FF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000FF0000000000000000
      0000000000000000000000000000808080000000000080808000808080008080
      800000000000000000000000000080808000F0FBFF0080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      000000FF00000000000000FF00000000000000FF000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000080808000000000000000000000FF00000000000000FF00000000
      00000000000000000000000000008080800000000000A4A0A000808080008080
      80000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      80000000000000000000000000008080800000000000A4A0A000A4A0A0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      80000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      80000000000000000000000000008080800000000000C0C0C000C0C0C0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000C0DCC000C0DCC0008080
      80000000000000000000000000008080800000000000C0DCC000C0DCC0008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      00000000000000000000000000008080800000000000F0FBFF00F0FBFF008080
      80000000000000000000000000008080800000000000F0FBFF00F0FBFF008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000008080
      8000000000000000000000000000808080000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000A4A0A0008080800080808000808080008080
      8000808080000000000000000000808080008080800080808000808080008080
      8000A4A0A0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000FFFF00000000008080
      80000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000008080800000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5B1B00000000000D5E9ED008B81720088765F0000000000C7D9DC00969E
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000000000000000
      0000808080000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009B9F
      9A00BAA28500999B9400A0AAA900A993790098846D00969E9A008B8A82008B72
      560098A09E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000808080000000000000000000FFFF
      0000808080008080800000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A2AD
      AB00C2AA9000C6AE9400C7B09700CDB9A100C7B29B00BAA28800B1998100A28B
      72009AA29F00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009EA7A300A0A8A500A29B
      8A00D5C0A900E4D3C000DFCDB900DDCBB800D9C5B200D2BFAA00C4AE9500BBA3
      8900998E7C009DA8A6009AA1A00000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B29F8600C9B19500E0CB
      B600E8D6C400E1CFBD00D2BEA700BBA89200B7A68D00C3AB9200C0A88E00C6B3
      9D00BEA78F00AE92780098806500D1E6EA00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000808080000000
      0000000000008080800000000000808080000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000080808000FFFF0000FFFF00000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000FFFF0000FFFF000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DAC5AF00E9D9
      C600DDCCB700C6AE9800ABB9BA000000000000000000A6B1B000BAA28700C5AF
      9700CAB7A200AB957D00A8B5B60000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000000000000000
      0000FFFF00008080800080808000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      00000000000000000000C0C0C0000000000000000000AAB5B500D7C2AA00E7D7
      C400C4B096009EA6A40000000000C0C0C00000000000000000009EA4A000BFA7
      8C00CBB7A200A895800080FFFF0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF00000000000000
      00000000000000000000C0C0C00000000000BCAA9500D7C3AA00E6D0BD00E5D3
      C000A6917A000000000000000000C0C0C0000000000000000000C0C0C000BCA4
      8900CCB9A300BCA28400AE947900947F6700000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF00000000
      0000000000008080800000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000000000C0C0C000FFFFFF00FFFFFF00A4A09400C2B4A100DAC6AD00E4D1
      BD00A38C730000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00C2AA
      8B00CCBAA500BBA28500A39077008C877B00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000FFFF0000FFFF
      0000000000008080800080808000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      000000000000C0C0C000FFFFFF00FFFFFF00D6EAEE00C7D8DC00CAB39A00F2E2
      D000BFAA9000959B9800C0C0C000FFFFFF00FFFFFF00FFFFFF009DA19B00C7B0
      9700CEBCA600BCA3860000000000C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000808080000000
      0000000000008080800000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF000000000000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00A09E9500D8C2AA00EEDF
      CD00E0CFBC00B49F83008C8B8200FFFFFF00FFFFFF009A958800C7AE9500D5C5
      B200D3C2AE00BEA287008F8E8500C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00CDE0E300C2AF9600D5C0A700E2CE
      BA00F2E2D200DFCDBB00C7B39A00B89F8300B9A08400CBB59C00CDB9A500D3BF
      AB00C9B29700BCA18600A68C7200C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00B3C2C400B1C0C000A9B4
      B300D7C2AC00EFDFCE00ECDCCB00ECDBCA00E7D6C300DECCB600D5C4AD00CBB5
      9A009EA39F00AFBCBD0000000000C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A9B2
      B100D0B89D00D7C3AB00DBC7B100E6D1BD00E3CDB700C7B09500C5AE9400BEA8
      8F00A9B2B200FFFFFF0000000000C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00B0A38F00ACB6B600CAD9DC00C2AD9600CBB49900BAC8C800A1A6A200AF9D
      8800FFFFFF00FFFFFF0000000000C0C0C000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000DAEBEE00A69D8D00B8A48E00DAEBEE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000C0C0C0000000000000000000C0C0C0000000000000000000C0C0C0000000
      000000000000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000C0C0C0000000000000000000C0C0C0000000000000000000C0C0C0000000
      000000000000C0C0C0000000000000000000424D3E000000000000003E000000
      2800000050000000640000000100010000000000B00400000000000000000000
      000000000000000000000000FFFFFF00FFFFF0000000000000000000FFFFF000
      0000000000000000FFFE70000000000000000000FFFC70000000000000000000
      FFF8F0000000000000000000FF01F00000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FFFFF0000000000000000000
      FFFFFFFFFFFFFFFFFFFF0000F000FFF44FF801FFFFFF0000F000FFE007F000FF
      FFE70000F000FFE007E000FFFFC70000F000FF800180007FFF8F0000F000FF80
      0000003FF01F0000F000FFC381000038003F0000F000F001C1000018003F0000
      F000F001C0292018003F0000F000F001E0090818003F0000F000F001C3110998
      003F0000F000F00181210818007F0000F000F0000109001800FF0000F000F000
      0310001800FF0000F000F0000700001800FF0000F000F0000F00001800FF0000
      F000F0003F80003800FF0000F000F001FFC0007800FF0000F000F001FFF000F8
      00FF0000FFFFF001FFF801FFFFFF0000FFFFFFFFFFFFFFFFFFFF0000CFFFFFFF
      FFFFFFFF801F0000C3FFFFFFFFFFFFFF800F0000C0FFF8000180001F00070000
      C03FF8000180001E00030000C00FF8000180001C00010000C003F80001800018
      00010000C000F8000180001800010000C00038000180001800010000C0001800
      0180001800010000C00018000180001800010000C00038000180001800010000
      C000F8000180001800010000C003F8000180001800010000C00FF80001800018
      00010000C03FF8000180001C00030000C0FFFFFFFFFFFFFE00070000C3FFFFFF
      FFFFFFFF000F0000CFFFFFFFFFFFFFFF801F0000FFFFFFFFFFFFFFFFFFFF0000
      F000F8000180001FFFFF0000F801F82E0382E03FFFFF0000FE27F47E0547E05F
      FFFF0000FF2FF81E0381E03E06070000FF3FF40E0140E05E06070000FE07F80C
      0380C0BE06070000FC03F4000140005E86870000FCFFF81C0B81C0BE86070000
      FCCFF4270542705E86870000FC8FFC7B0FC7B0FE86870000FE07FC670FC670FE
      86870000FF07FC6F0FC6F0FE86870000FF87FE6F0FE6F0FE86870000FF87FE54
      0FE540FE86870000FF87FE3B0FE3B0FEE6E70000FFC7F83F07E7F0FE06070000
      FFCFF83F07F7F0FFFFFF0000FFCFF83E17F3E1FFFFFF0000FFCFF8003FF803FF
      FFFF0000FFCFFFC07FFC07FFFFFF0000FFFFFFC004FFFFFFFFFF0000FFFFFFC0
      00FF8FFFF44F0000FFFFFE0031FF07FFE0070000C0033E0061FE03FFE0070000
      C0023E00F5FC01FF80010000C0007000B5F800FF80000000C018F00011F0007F
      C1810000C030F00033E0003000000000C07AF00007C0001000000000C05AF000
      0380000000000000C008F0000380000000000000C019F0001780001000000000
      C003F0000FC0003000000000C003F0001FE0007000000000C003F000BFF000F0
      00000000C017F0007FF801F000000000C00FF000FFFC03F000000000C01FF005
      FFFE07F000000000FFFFF003FFFF0FF000000000FFFFF007FFFF9FF000000000
      00000000000000000000000000000000000000000000}
  end
  object UpdateTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = UpdateTimerTimer
    Left = 176
    Top = 4
  end
  object SidecarTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = SidecarTimerTimer
    Left = 418
    Top = 14
  end
end
