// GrainToolPlugin.cpp
//
//  Created: December 9, 2002 by John Starr (cloned from AutoFilterPlugin.cpp)
//
//  Interface that loads the GrainTool Plugin Tool.
//
/* CVS Info:
$Header: /usr/local/filmroot/Plugins/Grain/GrainToolPlugin.cpp,v 1.9.14.4 2009/01/21 16:21:39 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------


#include "machine.h"
#include "CPlugin.h"
#include "GrainTool.h"
#define IsToolLicensed(a,b) true
#include "MTIstringstream.h"

//---------------------------Globals---------------------------------
//
//  Note:  The interface defines two ways of using the ToolNameTable
//  first is the number of tools PIN_RES_TOOL_NUMBER
//  the second is the ToolNameTable ENDS with a blank line
//
const char *ToolNameTable[] = {"Grain", ""};
#define NAME_TABLE_SIZE ((sizeof(ToolNameTable)/sizeof(char*))-1)
const char *PluginName = "Grain";

// Encryption for "CORRECT-NOISE-GRAIN"
MTI_UINT32 FEATURE_CORRECT_NOISE_GRAIN[] = {32, 0x15efda1c, 0x00e6b7e7, 0xadf77a7c, 0x4f5d96f0, 0xb2c24971, 0x52c01a65, 0x17d81874, 0x0e0b31cf, 0xf16c9fe7};
MTI_UINT32 *FeatureTable[] = {FEATURE_CORRECT_NOISE_GRAIN, 0};

CGrainTool *GGrainTool = NULL;           // only allow one tool
extern "C" void GrainToolShutdown(void);   // Code when DLL or so terminates


//**************************WINDOWS CODE*****************************
#ifdef WIN32
#include <windows.h>
#include <vcl.h>
#include "DllSupport.h"
#define EXPORT __declspec(dllexport)

#pragma hdrstop

//
// Windows reads the version from the DLL itself
// So do all the necessary stuff here
MTI_INT32 Version[] = {-1, -1, -1, -1};
char *Copyright = NULL;
char *TradeMark = NULL;
char *Company = NULL;

//
//  Will contain the global instance of the DLL
HINSTANCE hGInst;
//
//  This needs only be used if the DLL does not share the Application
// pointer.  Compiled under dynamic RTL and build using libraries
// the application pointers will be the same.
TApplication *GApp = NULL;         // Parent of DLL

//------------------ReadVersion-------------John Mertus--June 2001------

    void ReadVersion(void)

//  This returns the information about the plugin
//  The return is in the global Version. for example, "1.2.45.12"
//     V[0] - Major version number
//     V[1] - Minor version number
//     V[2] - Release number
//     V[3] - Build number
//
//***********************************************************************
{
    if (Copyright == NULL)
    {
	   char PluginDll[MAX_PATH];
	   GetModuleFileName(hGInst, PluginDll, MAX_PATH);
	   string Result = GetDLLVersion(PluginDll);
       sscanf(Result.c_str(),"%d.%d.%d.%d",&Version[0],&Version[1],&Version[2],&Version[3]);

	   string S = GetDLLInfo(PluginDll, "LegalCopyright");
       Copyright = MTIstrdup(S.c_str());

	   S = GetDLLInfo(PluginDll, "LegalTradeMarks");
       TradeMark = MTIstrdup(S.c_str());

       S = GetDLLInfo(PluginDll, "CompanyName");
       Company = MTIstrdup(S.c_str());
    }
}

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
  switch (reason)
    {
    case DLL_PROCESS_ATTACH:              // On Attach thread or process
	case DLL_THREAD_ATTACH:
      GApp = Application;
      hGInst = hinst;                     // save the instance for version and resources
	  ReadVersion();
      break;

    case DLL_THREAD_DETACH:               // On release
      break;
      
	case DLL_PROCESS_DETACH:
      GrainToolShutdown();                     // Common destroy code
      if (GApp != NULL) Application = GApp;
      MTIfree(Copyright);
      Copyright = NULL;
      MTIfree(Company);
      Company = NULL;
      MTIfree(TradeMark);
      TradeMark = NULL;
      break;
    }

  return TRUE;
}

//------------------SetParent-----------------John Mertus--July 2001------

   int  SetParent(void *Msg)

// For windows, Msg is a pointer to the Application
// Only needed if Application pointer is not shared between DLL and main
//
//***********************************************************************
{
   if (Msg == NULL)
      return 0;   // do nothing if caller's argument is NULL

   if (GApp == NULL) GApp = Application;
   Application = (TApplication *)Msg;
   return(0);
}

#else
//*****************************UNIX/LINUX CODE*******************************

#define EXPORT    // No export in so's

MTI_INT32 Version[] = {0,9,0,1};
char Copyright[] = "Copyright 2002 by Mathematical Technologies Inc.";
char TradeMark[] = "IntelliDeck";
char Company[] = "Mathematical Technologies";

//------------------SetParent-----------------John Mertus--July 2001------

   int  SetParent(void *Msg)

// For Unix, Msg should be a parent widget
//
//***********************************************************************
{
   return(0);
}


#endif

//**************************SYSTEM INDEPENDENT CODE****************************

//---------------------------------------------------------------------------
//
//  Plugin Functions
//    Note:  All function are exported with "C"
//    Borland will append an _ before the function
//
//---------------------------------------------------------------------------

extern "C" EXPORT void *Create(string Name);
extern "C" EXPORT bool MTI_PluginVersion(int *V);
extern "C" EXPORT void *Resource(int n);
extern "C" EXPORT int Properties(void);
extern "C" EXPORT bool WriteProperties(string FName, string Section);
extern "C" EXPORT bool ReadProperties(string FName, string Section);
extern "C" EXPORT int SendTheMessage(int nMsg, void *Msg);
extern "C" EXPORT PluginStateStruct CurrentState(void);

//------------------MTI_PluginVersion-------------John Mertus--June 2001------

	bool MTI_PluginVersion(int *V)

//  This returns the information about the plugin
//  The return is the version string; for example, "1.2.45.12"
//  V is an array or null, if an array the return is
//     V[0] - Major version number
//     V[1] - Minor version number
//     V[2] - Release number
//     V[3] - Build number
//
// IT IS THE EXISTANCE OF FUNCTION THAT DETERMINES IF AN SO OR DLL
// IS A MTI PLUGIN
//
//***********************************************************************
{
   // See if parsing is wanted
   if (V != NULL)
	  {
	  V[0] = Version[0];
	  V[1] = Version[1];
	  V[2] = Version[2];
	  V[3] = Version[3];
	  }

   return true;
}

//------------------Create-------------

    void *Create(string Name)

//  The should return a specific version of a base class that is
//  associated with name.
//
//  A pointer to the class is returned if there is such a base class
//  a NULL otherwise.
//
//***********************************************************************
{
   if (Name == (string)ToolNameTable[0])
      {
      if (GGrainTool != NULL)
         return(NULL);
      GGrainTool = new CGrainTool(Name, FeatureTable);

      return((void *)GGrainTool);
      }

   return(NULL);
}

//----------------Properties-------------------------John Mertus-----Jan 2001---

    int Properties(void)

//
//   This activates a form to change the properties of the plugin
//
//******************************************************************************
{
#ifdef _DEBUG
//  int Result = MessageBox(NULL, "Properties", "Properties form", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return(true);
};

//----------------WriteProperties-----------------John Mertus-----Jan 2001---

    bool WriteProperties(string IniName, string Section)

//
//   This writes all the necessary properties to the ini file FName
//   under section Section
//
//******************************************************************************
{
#ifdef _DEBUG
//  MessageBox(NULL, (IniName + "\n" + Section + "\n" + PluginName).c_str(), "Writing properties", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return(true);
};

//----------------ReadProperties-----------------John Mertus-----Jan 2001---

    bool ReadProperties(string IniName, string Section)

//
//   This Reads all the necessary properties to the ini file FName
//   under section Section
//
//******************************************************************************
{
#ifdef _DEBUG
//  MessageBox(NULL, (IniName + "\n" + Section + "\n" + PluginName).c_str(), "Reading properties", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return(true);
};

//----------------Resource-----------------John Mertus-----Jan 2001---

    void *Resource(int N)
//
//  Non classe data is reported via the resource section
//  N is the index into the resource, will be abstracted later
//
//******************************************************************************
{
  void *Result;

  switch (N)
   {
      case PIN_RES_TOOL_NAMES:
         Result = (void *)ToolNameTable;
         break;

      case PIN_RES_TOOL_NUMBER:
         Result = (void *)NAME_TABLE_SIZE;
         break;

      case PIN_RES_PLUGIN_NAME:
         Result = (void *)PluginName;
         break;

      case PIN_RES_TRADEMARK:
         Result = (void *)TradeMark;
         break;

      case PIN_RES_COPYRIGHT:
         Result = (void *)Copyright;
         break;

      case PIN_RES_COMPANY:
         Result = (void *)Company;
         break;

      case PIN_RES_FEATURE:
         Result = (void *)FeatureTable;
         break;

      default:
         Result = NULL;
   }

  return(Result);
};

//-------------SendTheMessage-----------------John Mertus--July 2001------

   int SendTheMessage(int nMsg, void *Msg)

// This processes messages, some are specific to the compiler and
// operating system
//
//***********************************************************************
{
   int Result;

   switch (nMsg)
    {
      case PIN_MSG_SET_PARENT:
        Result = SetParent(Msg);
        break;

      default:
        Result = 1;                    // Message not Processed

    }

   return(Result);
}


//------------------GrainToolShutdown-------------------John Mertus----Aug 2001-----

  void GrainToolShutdown(void)

//  This is called whenever the SO is unloaded
//
//****************************************************************************
{
  delete GGrainTool;
  GGrainTool = NULL;
}

//----------------CurrentState-----------------John Mertus-----Jan 2001---

    PluginStateStruct CurrentState(void)

//   This returns the current state of the tool.  State is determined by
// the licensing file
//
//******************************************************************************
{
   CPluginStateWrapper cps;
   string Result;

#if defined(_WINDOWS) && !defined(NO_LICENSING)
#ifdef DS1961S_DONGLE
   // Check if license is featured
   // New - we retry twice on failure and if all three attempts fail,
   // then we actually let the tool run; but if you get a triple
   // failure three times consecutively (the three strikes), THEN
   // we disable the tool!
   CRsrcCtrl rsrcCtrl;
   static bool itWorkedOnce = false;
   static int numberOfStrikes = 0;

   int numberOfTries;
   bool itJustWorked = false;
   for (numberOfTries = 0; (numberOfTries < 3) && (!itJustWorked); ++numberOfTries)
   {
      if (rsrcCtrl.IsFeatureLicensed(FeatureTable[0], Result))
      {
         cps.State(PLUGIN_STATE_ENABLED);
         cps.strReason(Result);
         itJustWorked = true;
      }
   }
   if (itJustWorked)
   {
      if (numberOfTries == 2)
      {
         TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX PLUGIN GLITCHED ONCE XXXXXXXXXXXXXXXXXXXXXX");
      }
      if (numberOfTries == 3)
      {
         TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX PLUGIN GLITCHED TWICE XXXXXXXXXXXXXXXXXXXXXX");
      }

      itWorkedOnce = true;
      if (numberOfStrikes > 0)
      {
         TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX PLUGIN OK XXXXXXXXXXXXXXXXXXXXXX");
         numberOfStrikes = 0;
      }
   }
   else
   {
      if (itWorkedOnce && (++numberOfStrikes < 3))
      {
          TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX PLUGIN STRIKE " << numberOfStrikes << " XXXXXXXXXXXXXXXXXXXXXX");
          cps.State(PLUGIN_STATE_ENABLED);
          cps.strReason(Result);
      }
      else
      {
          TRACE_0(errout << "XXXXXXXXXXXXXXXXXXXXX PLUGIN STRIKE 3 - YER OUT! XXXXXXXXXXXXXXXXXXXXXX");
          cps.State(PLUGIN_STATE_DISABLED);
          cps.strReason(Result);
      }
   }
#else
   if (IsToolLicensed(ToolNameTable[0], Result))
   {
      cps.State(PLUGIN_STATE_ENABLED);
      cps.strReason(Result);
   }
   else
   {
      cps.State(PLUGIN_STATE_DISABLED);
      cps.strReason(Result);
   }
#endif
#else
   cps.State(PLUGIN_STATE_ENABLED);
   cps.strReason("Licensed");
#endif

   return cps.getWrappedPluginState();
}

