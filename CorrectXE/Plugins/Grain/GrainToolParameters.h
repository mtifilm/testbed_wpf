//---------------------------------------------------------------------------

#ifndef GrainToolParametersH
#define GrainToolParametersH
//---------------------------------------------------------------------------

#include "IniFile.h"
#include "GrainReduction_MT.h"
#include "GrainCoreCommon.h"
#include "RegionOfInterest.h"
#include "ToolObject.h"

class CPDLElement;
//---------------------------------------------------------------------------

// Grain Tool Default Parameters
#define GRN_DEFAULT_SIZE 1
#define GRN_DEFAULT_SUPPORT 200
#define GRN_DEFAULT_CONTRAST 20.0
#define GRN_DEFAULT_PAST 2
#define GRN_DEFAULT_FUTURE 2
#define GRN_DEFAULT_TEMP_WEIGHT 12.5   //0 = off, 1 = low, 2 = high

//////////////////////////////////////////////////////////////////////
// Grain Tool's Parameters
//  This class holds that Grain's parameters that are passed between
//  the GUI, tool object and tool processor classes

class CGrainToolParameters : public CToolParameters
{
public:
   CGrainToolParameters();
   virtual ~CGrainToolParameters();

   void Reset();

   void ReadSettings(const string &IniFileName);
   void WriteSettings(const string &IniFileName);

   void ReadPDLEntry(CPDLElement *pdlToolAttribs);
   void WritePDLEntry(CPDLElement &parent);

   void setWrapping(CToolSystemInterface *systemAPI, int previewFrame);

public:
   // public data members
   long lGrainRow, lGrainCol;
   long lTemporalSupport;
   long lNFrames;
   long lPastFrames, lFutureFrames;
   long lInWrap, lOutWrap;
   float faContrast[MAX_COMPONENT];
   int lTempWeight;

   bool bReduce;
   bool bGenerate;

   // Kludge, basically for PDL
   GrainParameters grainParameters;

// Static strings for keywords, etc. used in ini file and PDL entry
private:
   static string sizeKey;
   static string contrast0Key;
   static string contrast1Key;
   static string contrast2Key;
   static string tempweightKey;

public:
   static string iniSectionName;   // public so that tool can use it
   static string reduceKey;
   static string generateKey;
};

#endif
