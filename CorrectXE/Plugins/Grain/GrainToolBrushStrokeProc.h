//---------------------------------------------------------------------------

#ifndef GrainToolBrushStrokeProcH
#define GrainToolBrushStrokeProcH
//---------------------------------------------------------------------------

#include "IniFile.h"
#include "GrainCoreCommon.h"
#include "GrainTool.h"   // NO!!!
#include "ToolObject.h"
//---------------------------------------------------------------------------

class CGrainToolBrushStrokeProc : public CToolProcessor
{
public:

   CGrainToolBrushStrokeProc(
      int newToolNumber,
      const string &newToolName,
      CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr,
      IToolProgressMonitor *newToolProgressMonitor,
      GrainGeneratorModel *grainGeneratorModel);

   virtual ~CGrainToolBrushStrokeProc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   CHRTimer HRT;

private:

   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:

   MTI_UINT16 *GenerateBrushStrokeGrainImage(GrainGenerator *grainGenerator,
                                             MTI_UINT16 *visibleFrameBuffer);

   int inFrameIndex;
   int iterationCount;

   GrainGeneratorModel *_grainGeneratorModel;
   int _grainGenerationFrameIndex;
   CGrainToolParameters grainParams;
   BrushStrokes _previousBrushStrokes;

//*** WHY ARE THESE GLOBAL?
   int concurrentFramesCount;
   int maxInputFrames;      // Maximum number of input frames per iteration
   int maxOutputFrames;     // Maximum number of output frames per iteration
//***
   int framesRemaining;     // Number of remaining output frames to be processed
   int framesAlreadyProcessed; // Number of output frames already processed
   int framesToProcess;     // Number of frames to process on current iteration

   // Mask Region
   CRegionOfInterest maskROI[GRN_MAX_CONCURRENT_FRAMES_COUNT];

};

#endif
