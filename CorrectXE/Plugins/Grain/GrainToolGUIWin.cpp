/*
   File Name:  GrainToolGUIWin.cpp
   This contains all the toolbar components for the GrainTool
*/

// FAKE define as it cannnot be changed
#define COMPONENTS 3

#include <vcl.h>
#pragma hdrstop

#include "ClipAPI.h"
#include "GrainToolGUIWin.h"
#include "MTIKeyDef.h"
#include "PDL.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"
#include "JobManager.h"
#include "GrainGenerator.h"  // for channel mask bits

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
//#pragma link "CSPIN"
#pragma link "VTimeCodeEdit"
#pragma link "ExecButtonsFrameUnit"
#pragma link "CompactTrackEditFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"

#define BUTTON_WIDTH 20
TGrainToolForm *GrainToolForm;

#define PLUGIN_TAB_INDEX 8

namespace {
   const string GrainIniFileName("$(CPMP_USER_DIR)Grain.ini");
   const string sizeKey("Size");
   const string contrast0Key("Contrast0");
   const string contrast1Key("Contrast1");
   const string contrast2Key("Contrast2");
   const string tempweightKey("TemporalWeight");
};

//------------------CreateGrainToolGUI--------------------

  void CreateGrainToolGUI(void)

// This creates the GrainTool GUI if one does not already exist.
// Note: only one can be created.
//
//****************************************************************************
{
   if (GrainToolForm != NULL) return;         // Already created

   GrainToolForm = new TGrainToolForm(Application);   // Create it
   GrainToolForm->formCreate();

   GrainToolForm->RestoreProperties();

   // Reparent the controls to the UniTool
   extern char *PluginName;
   GGrainTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>( GrainToolForm->GrainControlPanel ),
         NULL);
}

//-----------------DestroyGrainToolGUI------------------

  void DestroyGrainToolGUI(void)

//  This destroys then entire GUI interface
//
//***************************************************************************
{
   if (GrainToolForm == NULL) return;         // Already destroyed

   // Reparent the controls back to us before destroying them
   if (GGrainTool != NULL)
      GGrainTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
   GrainToolForm->GrainControlPanel->Parent = GrainToolForm;

   GrainToolForm->Free();
   GrainToolForm = NULL;
}

//-------------------ShowGrainToolGUI-------------------

    bool ShowGrainToolGUI(void)

//  This creates the GUI if not already exists and then shows it
//
//****************************************************************************
{
   CreateGrainToolGUI();            // Create the gui if necessary

   if (GrainToolForm == NULL || GGrainTool == NULL)
      return false;

   GrainToolForm->GrainControlPanel->Visible = true;
   GrainToolForm->formShow();

   return true;
}

//-------------------HideGrainToolGUI-------------------

    bool  HideGrainToolGUI(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
    if (GrainToolForm == NULL) return false;

    GrainToolForm->GrainControlPanel->Visible = false;
    // GrainToolForm->formHide();

    return false;  // controls are NOT visible !!?!
}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
    if (GrainToolForm == NULL) return(false);         // Not created
    return GrainToolForm->GrainControlPanel->Visible;
}
//---------------------------------------------------------------------------

void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                            bool processingRenderFlag)

{
   if (GrainToolForm == 0) return;
   GrainToolForm->UpdateExecutionButtons(toolProcessingControlState,
										 processingRenderFlag);
}
//---------------------------------------------------------------------------

void GatherGUIParameters(CGrainToolParameters &toolParams)
{
   if (GrainToolForm == 0) return;
   GrainToolForm->GatherParameters(toolParams);
}
//---------------------------------------------------------------------------

void CaptureGUIParameters(CPDLElement &pdlToolParams)
{
   GrainToolForm->CaptureParameters(pdlToolParams);
}
//---------------------------------------------------------------------------

void SetGUIParameters(CGrainToolParameters &toolParams)
{
   if (GrainToolForm == 0)
      return;  // form not available

   GrainToolForm->SetParameters(toolParams);
}
//---------------------------------------------------------------------------

__fastcall TGrainToolForm::TGrainToolForm(TComponent* Owner)
 : TMTIForm(Owner)
{
}
//---------------------------------------------------------------------------

void TGrainToolForm::formCreate(void)
{
	this->ProjectRadioButton->Tag = GP_BASE_PROJECT;
	this->ClipRadioButton->Tag = GP_BASE_CLIP;

   // load the grain parameters
   SizeTrackEdit->SetValue(GGrainTool->grainParams.lGrainRow);

   int c0 = GGrainTool->grainParams.faContrast[0];
   int c1 = GGrainTool->grainParams.faContrast[1];
   int c2 = GGrainTool->grainParams.faContrast[2];

   ContrastGangCheckBox->Checked = (c0 == c1 && c1 == c2);

   updatingRContrast = true;
   ContrastRTrackEdit->SetValue(c0);
   updatingRContrast = false;
   updatingGContrast = true;
   ContrastGTrackEdit->SetValue(c1);
   updatingGContrast = false;
   updatingBContrast = true;
   ContrastBTrackEdit->SetValue(c2);
   updatingBContrast = false;

   int twtemp = GGrainTool->grainParams.lTempWeight;
   if (twtemp < 0 || twtemp > 100)
      twtemp = GGrainTool->grainParams.lTempWeight = 50;

   if (twtemp == 0)
   {
      TemporalWeightOffRadioButton->Checked = true;
   }
   else if (twtemp > 0 && twtemp <= 50)
   {
      TemporalWeightMediumRadioButton->Checked = true;
      TemporalWeightMediumTrackEdit->SetValue(twtemp  * 2);
   }

   else // (twtemp >50 &&  twtemp <= 100)
   {
      TemporalWeightHighRadioButton->Checked = true;
      TemporalWeightHighTrackEdit->SetValue(twtemp);
   }

   // For Preview
	int pw = BrushPreviewPaintBox->Width;
	int ph = BrushPreviewPaintBox->Height;
	this->_previewBrushRevealer = new BrushRevealer(IppiSize {pw, ph});
	this->_prevewGrayImageNormalize = ippsMalloc_32f(COMPONENTS * pw * ph);
	IppThrowOnError(ippsSet_32f(0xC0, this->_prevewGrayImageNormalize, COMPONENTS * pw * ph));

   TemporalWeightRadioButtonClick(NULL);
   AutoAcceptCheckBoxClick(NULL);
   UseBrushCheckBoxClick(NULL);

   GGrainTool->SetToolProgressMonitor(ExecStatusBar);
}
//---------------------------------------------------------------------------

void TGrainToolForm::formShow(void)
{
	if (GGrainTool == NULL)
   {
		return;
   }

	SET_CBHOOK(ClipHasChanged, GGrainTool->ClipChange);
	SET_CBHOOK(MarksHaveChanged, GGrainTool->MarksChange);
	SET_CBHOOK(BrushStrokesHaveChanged, GGrainTool->BrushStrokesChange);
	SET_CBHOOK(GrainParametersHaveChanged, GGrainTool->GetGrainGeneratorModel()->GrainParametersChange);
	SET_CBHOOK(PresetHasChanged, GGrainTool->GetGrainGeneratorModel()->PresetChange);
	SET_CBHOOK(BrushShapeTrackEditGotFocus, this->RadiusTrackEditFrame->TrackEditGotFocus);
	SET_CBHOOK(BrushShapeTrackEditGotFocus, this->AspectTrackEditFrame->TrackEditGotFocus);
	SET_CBHOOK(BrushShapeTrackEditGotFocus, this->AngleTrackEditFrame->TrackEditGotFocus);

   if (this->GrainPageControl->ActivePage == this->CreateTabSheet && !this->CreateCheckBox->Checked)
   {
	  this->GrainPageControl->ActivePage = this->ReduceTabSheet;
   }
   else if (this->GrainPageControl->ActivePage == this->ReduceTabSheet && !this->ReduceCheckBox->Checked)
   {
	  this->GrainPageControl->ActivePage = this->CreateTabSheet;
   }

	UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);

   this->UpdateBrushRadiusRange();
	this->UpdatePreviewBrush();

   // QQQ if a trackbar is focused, do
   // TCompactTrackEditFrame::NotifyTrackBarEntered(focusedTrackBar);
}

// ---------------------------------------------------------------------------

void TGrainToolForm::formHide(void)
{
	if (GGrainTool == NULL)
		return;

	REMOVE_CBHOOK(ClipHasChanged, GGrainTool->ClipChange);
	REMOVE_CBHOOK(MarksHaveChanged, GGrainTool->MarksChange);
	REMOVE_CBHOOK(BrushStrokesHaveChanged, GGrainTool->BrushStrokesChange);
	REMOVE_CBHOOK(GrainParametersHaveChanged, GGrainTool->GetGrainGeneratorModel()->GrainParametersChange);
	REMOVE_CBHOOK(PresetHasChanged, GGrainTool->GetGrainGeneratorModel()->PresetChange);
	REMOVE_CBHOOK(BrushShapeTrackEditGotFocus, this->RadiusTrackEditFrame->TrackEditGotFocus);
   REMOVE_CBHOOK(BrushShapeTrackEditGotFocus, this->RadiusTrackEditFrame->TrackEditGotFocus);
	REMOVE_CBHOOK(BrushShapeTrackEditGotFocus, this->AspectTrackEditFrame->TrackEditGotFocus);
	REMOVE_CBHOOK(BrushShapeTrackEditGotFocus, this->AngleTrackEditFrame->TrackEditGotFocus);
}

//---------------------------------------------------------------------------

void TGrainToolForm::UpdateExecutionButtons(EToolControlState toolProcessingControlState,
	bool processingRenderFlag)
{
	UpdateExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag);

	AnsiString c3 = "Grain Time: ";
	SpeedLabel->Caption = c3 + GGrainTool->GrainTime.c_str();
}

//---------------------------------------------------------------------------

void TGrainToolForm::GatherParameters(CGrainToolParameters &toolParams)
{
   // copy GUI parameters into the grain tool
   toolParams.lGrainRow = SizeTrackEdit->GetValue();
   toolParams.lGrainCol = SizeTrackEdit->GetValue();
   toolParams.faContrast[0] = (float)ContrastRTrackEdit->GetValue();
   toolParams.faContrast[1] = (float)ContrastGTrackEdit->GetValue();
   toolParams.faContrast[2] = (float)ContrastBTrackEdit->GetValue();

   // If the Temporal Analysis "Off" radio button is selected,
   // then lTempWeight is set to zero
   // If the Temporal Analysis "Medium" radio button is selected,
   // then lTempWeight is set to half of the value in the Track edit control
   // directly under the "Medium" radio button.
   // If the Temporal Analysis "High" radio button is selected,
   // then lTempWeight is set to half of the value in the Track edit control
   // directly under the "Medium" radio button plus 50.

   if (TemporalWeightOffRadioButton->Checked)
   {
      toolParams.lTempWeight = 0;
   }
   else if (TemporalWeightMediumRadioButton->Checked)
   {
      int value = TemporalWeightMediumTrackEdit->GetValue();
      //if (value < TemporalWeightMediumTrackEdit->MinValue)
         //value = TemporalWeightMediumTrackEdit->MinValue;
      //if (value > TemporalWeightMediumTrackEdit->MaxValue)
         //value = TemporalWeightMediumTrackEdit->MaxValue;

      if (value <= 1)
         toolParams.lTempWeight = 1;
      else
         toolParams.lTempWeight = value / 2;
   }
   else if (TemporalWeightHighRadioButton->Checked)
   {
      int value = TemporalWeightHighTrackEdit->GetValue();
      //if (value < TemporalWeightHighTrackEdit->MinValue)
         //value = TemporalWeightHighTrackEdit->MinValue;
      //if (value > TemporalWeightHighTrackEdit->MaxValue)
         //value = TemporalWeightHighTrackEdit->MaxValue;

      if (value <= 1)
         toolParams.lTempWeight = 51;
      else
        toolParams.lTempWeight = value / 2 + 50;
   }

   toolParams.bReduce = this->ReduceCheckBox->Checked;
   toolParams.bGenerate = this->CreateCheckBox->Checked;

   GGrainTool->GetGrainGeneratorModel()->SetGrainParameters(ReadGrainParametersFromGUI());
}
//---------------------------------------------------------------------------

void TGrainToolForm::CaptureParameters(CPDLElement &pdlEntryToolParams)
{
   // Get the Grain parameters in the GUI to include in a PDL Entry
   CGrainToolParameters toolParams;

   // Get the settings from the GUI
   GatherParameters(toolParams);

   // Add the settings to the PDL Entry
   toolParams.grainParameters = GGrainTool->GetGrainGeneratorModel()->GetGrainParameters();
   toolParams.WritePDLEntry(pdlEntryToolParams);
}
//---------------------------------------------------------------------------

void TGrainToolForm::SetParameters(CGrainToolParameters &toolParams)
{
   // load the grain parameters
   SizeTrackEdit->SetValue(toolParams.lGrainRow);
   ContrastRTrackEdit->SetValue(toolParams.faContrast[0]);
   ContrastGTrackEdit->SetValue(toolParams.faContrast[1]);
   ContrastBTrackEdit->SetValue(toolParams.faContrast[2]);

   // lTempWeight == 0 implies Temporal Analysis Off
   // 0 < lTempWeight <= 50. implies Medium
   // 50 < lTempWeight implies High
   if (toolParams.lTempWeight <= 0)
   {
      TemporalWeightOffRadioButton->Checked = true;
   }
   else if (toolParams.lTempWeight <= 50)
   {
      TemporalWeightMediumRadioButton->Checked = true;
      if (toolParams.lTempWeight == 1)
         TemporalWeightMediumTrackEdit->SetValue(toolParams.lTempWeight);
      else
         TemporalWeightMediumTrackEdit->SetValue(toolParams.lTempWeight * 2);
   }
   else
   {
      TemporalWeightHighRadioButton->Checked = true;
      if (toolParams.lTempWeight == 51)
         TemporalWeightHighTrackEdit->SetValue(toolParams.lTempWeight - 50);
      else
         TemporalWeightHighTrackEdit->SetValue((toolParams.lTempWeight - 50) * 2);
   }

   this->SetGrainParametersToGUI(toolParams.grainParameters);

   this->ReduceCheckBox->Checked = toolParams.bReduce;
   this->CreateCheckBox->Checked = toolParams.bGenerate;

   if (toolParams.bReduce && !toolParams.bGenerate)
   {
      if (this->GrainPageControl->ActivePage != this->ReduceTabSheet)
      {
        this->GrainPageControl->ActivePage = this->ReduceTabSheet;
      }
   }
   else if (!toolParams.bReduce && toolParams.bGenerate)
   {
      if (this->GrainPageControl->ActivePage != this->CreateTabSheet)
      {
        this->GrainPageControl->ActivePage = this->CreateTabSheet;
      }
   }

   GGrainTool->GetGrainGeneratorModel()->SetGrainParameters(ReadGrainParametersFromGUI());
}
//---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
   if (GrainToolForm == NULL)  return false;

   return GrainToolForm->runExecButtonsCommand(command);
}

bool TGrainToolForm::runExecButtonsCommand(EExecButtonId command)
{
   return ExecButtonsToolbar->RunCommand(command);
}
//---------------------------------------------------------------------------

void TGrainToolForm::PreviewFrame(void)
{
   // Preview a single frame

   if (GGrainTool == NULL) return;     // Never can happen

   // Start Preview processing for the current frame.  This function will
   // only act if Previewing/Rendering is stopped or paused.
   GGrainTool->StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1,false);

   // TTT - This function was set up to catch parameter changes while
   //       Previewing or Rendering was running.  It only beeped, but
   //       could have sent new parameters to the GrainTool processing.
   //       This capability should probably be somewhere else.

}
//---------------------------------------------------------------------------

EToolProcessingCommand TGrainToolForm::GetButtonCommand(TToolButton *button)
{
   // Get the current tool processing command from the button's User Data

   return (EToolProcessingCommand)(button->Tag);
}
//---------------------------------------------------------------------------

void TGrainToolForm::SetButtonCommand(TToolButton *button,
                                       EToolProcessingCommand newCmd)
{
   // Set the tool processing command in the button's Tag and set the
   // the matching text label and icon for the button

   // Save the tool processing command in the button's Tag
   button->Tag = newCmd;

   string buttonString;
   System::Uitypes::TImageIndex iconIndex;
   switch (newCmd)
      {
      case TOOL_PROCESSING_CMD_PREVIEW :
         buttonString = "Preview";
         iconIndex = 2;
         break;
      case TOOL_PROCESSING_CMD_RENDER :
         buttonString = "Render";
         iconIndex = 1;
         break;
      case TOOL_PROCESSING_CMD_PAUSE :
         buttonString = "Pause";
         iconIndex = 7;
         break;
      case TOOL_PROCESSING_CMD_CONTINUE :
         buttonString = "Continue";
         iconIndex = 8;
         break;
      default :
         buttonString = "ERROR!";  // Shouldn't ever happen
         iconIndex = 3; // Stop sign
         break;
      }

   button->Caption = buttonString.c_str();
   button->ImageIndex = iconIndex;
}
//---------------------------------------------------------------------------

//------------------ClipHasChanged----------------John Mertus----Nov 2002-----

	void TGrainToolForm::ClipHasChanged(void *Sender)

//  This is called when the clip is loaded into the main window
//  Just disconnect the remote display
//
//****************************************************************************
{
   if (GGrainTool == NULL) return;

   ExecSetResumeTimecodeToDefault();

   // Clip has changed, reset all the ini files
   GrainGeneratorModel *grainGeneratorModel = GGrainTool->GetGrainGeneratorModel();

   grainGeneratorModel->SaveToGrainIni(GrainIniFileName);

   JobManager jobManager;
   grainGeneratorModel->SetMasterClipFolderPath(jobManager.GetMasterClipFolderPath());
   std::string jobRootFolder = jobManager.GetJobRootFolderPath();
   if (jobRootFolder == "")
   {
	  jobRootFolder = jobManager.GetMasterClipFolderPath();
   }

   grainGeneratorModel->SetProjectRootFolderPath(jobRootFolder);
   grainGeneratorModel->LoadFromGrainIni(GrainIniFileName);

   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   this->UpdateBrushRadiusRange();
}

//------------------MarksHaveChanged----------------------------Mar 2008-----

	void TGrainToolForm::MarksHaveChanged(void *Sender)

//  This is called when the clip is loaded into the main window
//  Just disconnect the remote display       hahah i LOVE this comment!
//
//****************************************************************************
{
   if (GGrainTool == NULL) return;

   EToolControlState toolControlState = GGrainTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
      UpdateExecutionButtonToolbar(toolControlState, false);
}
//---------------------------------------------------------------------------

void TGrainToolForm::BrushStrokesHaveChanged(void *Sender)
{
   if (GGrainTool == NULL)
   {
      return;
   }

   this->EnableOrDisableBrushStuff();

   BrushStrokes brushStrokes;
   GGrainTool->GetBrushStrokes(brushStrokes);
   this->BrushStrokeListBox->Clear();

   for (int i = 0; i < brushStrokes.size(); ++i)
   {
      int strokeNumber = i + 1;
      char buffer[100];
      sprintf(buffer, "Stroke %d: %s grain", strokeNumber,
               (brushStrokes[i].IsErasing()? "Erase" : "Add"));
      this->BrushStrokeListBox->AddItem(buffer, NULL);
   }
}
//---------------------------------------------------------------------------

void TGrainToolForm::BrushShapeTrackEditGotFocus(void *Sender)
{
   GGrainTool->CenterBrushOnFrame();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::SEditOnKeyPress(TObject *Sender, char &Key)
{
  bool isValid = false;
  if ( ((Key >= '0') && (Key <= '9')) ||
       ((Key < 0x32) && (Key != Char(VK_RETURN))) )
       isValid = true;

  if (!isValid) {
    if (Key == Char(VK_RETURN))
 //      ValueHasChanged(Sender);
       ;
    else
      MessageBeep(0);
    Key = 0;
  }
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::TemporalWeightRadioButtonClick(TObject *Sender)
{
   if (TemporalWeightOffRadioButton->Checked)
   {
      TemporalWeightMediumTrackEdit->Enable(false);
      TemporalWeightHighTrackEdit->Enable(false);
   }
   else if (TemporalWeightMediumRadioButton->Checked)
   {
      TemporalWeightMediumTrackEdit->Enable(true);
      TemporalWeightHighTrackEdit->Enable(false);
   }
   else if (TemporalWeightHighRadioButton->Checked)
   {
      TemporalWeightMediumTrackEdit->Enable(false);
      TemporalWeightHighTrackEdit->Enable(true);
   }
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------------ EXEC BUTTONS TOOLBAR -----------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ExecButtonsToolbar_ButtonPressedNotifier(
      TObject *Sender)
{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
      default:
      case EXEC_BUTTON_NONE:
         // No-op
      break;

      case EXEC_BUTTON_PREVIEW_FRAME:
         PreviewFrame();
      break;

      case EXEC_BUTTON_PREVIEW_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
      break;

      case EXEC_BUTTON_RENDER_FRAME:
#if 0 // FIX ME
         RenderFrame();
#endif
      break;

      case EXEC_BUTTON_RENDER_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
      break;

      case EXEC_BUTTON_PAUSE_OR_RESUME:
         if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
         else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_PAUSE:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
      break;

      case EXEC_BUTTON_RESUME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_STOP:
         ExecStop();
         break;

      case EXEC_BUTTON_CAPTURE_PDL:
         ExecCaptureToPDL();
      break;

      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         ExecCaptureAllEventsToPDL();
      break;

      case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         ExecGoToResumeTimecode();
      break;

      case EXEC_BUTTON_SET_RESUME_TC:
         ExecSetResumeTimecodeToCurrent();
      break;
   }
}
//---------------- Resume Timecode Functions --------------------------------

void TGrainToolForm::ExecSetResumeTimecode(int frameIndex)
{
   CVideoFrameList *frameList = GGrainTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   if (frameIndex < 0)
      frameIndex = frameList->getInFrameIndex();

   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecSetResumeTimecodeToCurrent(void)
{
   if (GGrainTool == NULL) return;

   int frameIndex = GGrainTool->getSystemAPI()->getLastFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecSetResumeTimecodeToDefault(void)
{
   if (GGrainTool == NULL) return;

   int frameIndex = GGrainTool->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecGoToResumeTimecode(void)
{
   if (GGrainTool == NULL) return;

   CVideoFrameList *frameList;
   frameList = GGrainTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // No! GGrainTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GGrainTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = GGrainTool->getSystemAPI()->getLastFrameIndex();
   ExecButtonsToolbar->SetResumeTimecode(
                        frameList->getTimecodeForFrameIndex(frameIndex));
}

//------------------ ExecRenderOrPreview ------------------------------------
//
//  Call this when any render or preview button is clicked
//  buttonCommand identifies the button that was pressed
//
void TGrainToolForm::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
   switch (buttonCommand)
      {
      // OLD_RENDER_DEST_HACK
      case TOOL_PROCESSING_CMD_RENDER :
         GGrainTool->getSystemAPI()->initRenderDestinationClip(0 /*RenderRadioBox->ItemIndex*/);
         /* FALL THROUGH */
      case TOOL_PROCESSING_CMD_PREVIEW :
         GGrainTool->StartToolProcessing(buttonCommand,false);
         break;

      case TOOL_PROCESSING_CMD_PAUSE :
         GGrainTool->PauseToolProcessing();
         break;

      case TOOL_PROCESSING_CMD_CONTINUE :
         {
         CVideoFrameList *frameList;
         frameList = GGrainTool->getSystemAPI()->getVideoFrameList();
         int resumeFrame;
         if (frameList == NULL)
            resumeFrame = GGrainTool->getSystemAPI()->getMarkIn();
         else
         {
            PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
            resumeFrame = frameList->getFrameIndex(resumeTimecode);
         }

         GGrainTool->ContinueToolProcessing(resumeFrame);
         }
         break;

      default:
         MTIassert(false);
         break;
      }
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecStop(void)
{
   if (GGrainTool == NULL) return;

   GGrainTool->StopToolProcessing();
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecCaptureToPDL(void)
{
   if (GGrainTool == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GGrainTool->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------

void TGrainToolForm::ExecCaptureAllEventsToPDL(void)
{
   if (GGrainTool == NULL) return;

   GGrainTool->getSystemAPI()->CapturePDLEntry(true);
}
//---------------------------------------------------------------------------

void TGrainToolForm::UpdateExecutionButtonToolbar(
	  EToolControlState toolProcessingControlState,
	  bool processingRenderFlag)
{
   if (GGrainTool == NULL) return;

   if (GGrainTool->IsDisabled()
   || (this->UseBrushCheckBox->Checked && !GGrainTool->AreBrushStrokesPending()))
   {
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		 ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		 ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
		 ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//		 ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//								 UseBrushCheckBox->Checked
//                         ? "Can't preview - using brush"
//                         : "Can't preview - tool is disabled");
//		 ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//								 UseBrushCheckBox->Checked
//                         ? "Can't render - using brush"
//                         : "Can't render - tool is disabled");
//		 ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//								 UseBrushCheckBox->Checked
//                         ? "Can't add to PDL - using brush"
//                         : "Can't add to PDL - tool is disabled");
		 return;
   }

   if (this->UseBrushCheckBox->Checked
   && GGrainTool->AreBrushStrokesPending()
   && toolProcessingControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      setIdleCursor();

      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Can't preview - using brush");
//      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Apply brush strokes to all marked frames");
//      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't add to PDL - using brush");

      return;
   }

   switch(toolProcessingControlState)
   {
      case TOOL_CONTROL_STATE_STOPPED :

         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button

         setIdleCursor();

         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
         // Don't know why Larry wants this to never be enabled
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);

         if (GGrainTool->AreMarksValid())
         {
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);

//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                    "Preview marked range (Shift+D)");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                    "Render marked range (Shift+G)");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Add to PDL (, key)");
         }
         else
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                    "Can't preview - marks are invalid");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                    "Can't render - marks are invalid");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Can't add to PDL - marks are invalid");
         }

         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

         ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

         ExecButtonsToolbar->EnableResumeWidget();
         ExecSetResumeTimecodeToDefault();

      break;

      case TOOL_CONTROL_STATE_RUNNING :

         // Preview1, Render1, Preview or Render is now running

         setBusyCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
#if 0 // FIX ME
         if (processingSingleFrameFlag)
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         else
#endif
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);


         if (!processingRenderFlag)
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
         else
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

      break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused

         setIdleCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         // Now go load the current timecode
         ExecSetResumeTimecodeToCurrent();

      break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :

         // Processing single frame or waiting to stop or pause --
         // disable everything
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE
          && toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_STOP)
         {
            if (!processingRenderFlag)
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
            else
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
         }

         break;

      default:
         MTIassert(false);

         break;
   }

	this->UpdateProvisionButtons();

} // UpdateExecutionButton()
//---------------------------------------------------------------------------

void TGrainToolForm::setIdleCursor(void)
{
   Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void TGrainToolForm::setBusyCursor(void)
{
   Screen->Cursor = crAppStart;
}
//---------------------------------------------------------------------------

void SetFocusedControl(int commandNumber)
{
   if (GrainToolForm == NULL) return;

   GrainToolForm->setFocusedControl(commandNumber);
}

void TGrainToolForm::setFocusedControl(int commandNumber)
{
   if ((GGrainTool == NULL) || GGrainTool->IsDisabled() || !IsToolVisible())
   {
	  return;
   }

   TObject *focusedObject = reinterpret_cast<TObject *>(GGrainTool->getSystemAPI()->GetFocusedAdoptedControl());
   TObject *newFocusedObject = NULL;

   // Handle the controls that are visible no matter which tab is selected
   switch(commandNumber)
   {
      case GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT:
         if (focusedObject == this->ReduceCheckBox)
         {
            newFocusedObject = this->CreateCheckBox;
         }
         else if (focusedObject == this->CreateCheckBox)
         {
            newFocusedObject = this->CreateCheckBox->Checked
                               ? this->UseBrushCheckBox
                               : this->ReduceCheckBox;
         }
         else
         {
            newFocusedObject = this->UseBrushCheckBox->Checked
                               ? this->CreateCheckBox
                               : this->ReduceCheckBox;
         }

         break;

      case GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV:
         if (focusedObject == this->UseBrushCheckBox)
         {
            newFocusedObject = this->UseBrushCheckBox->Checked
                               ? this->CreateCheckBox
                               : this->ReduceCheckBox;
         }
         else if (focusedObject == this->CreateCheckBox)
         {
            newFocusedObject = this->CreateCheckBox->Checked
                               ? this->UseBrushCheckBox
                               : this->ReduceCheckBox;
         }
         else
         {
            newFocusedObject = this->CreateCheckBox->Checked
                               ? this->UseBrushCheckBox
                               : this->CreateCheckBox;
         }
         break;

      default:
         break;
   }

   if (newFocusedObject == NULL)
   {
      newFocusedObject = (this->GrainPageControl->ActivePage == this->ReduceTabSheet)
                         ? this->computeFocusedControlReduce(commandNumber, focusedObject)
                         : this->computeFocusedControlGenerate(commandNumber, focusedObject);
   }

   if (newFocusedObject == NULL)
   {
      // Pick something innocuous to focus on
      newFocusedObject = this->EnableGroupBox;
   }

   GGrainTool->getSystemAPI()->FocusAdoptedControl(reinterpret_cast<int *>(newFocusedObject));
}

TObject *TGrainToolForm::computeFocusedControlReduce(int commandNumber, TObject *focusedObject)
{
   TObject *newFocusedObject = NULL;

   switch(commandNumber)
   {
      case GRN_CMD_CYCLE_PARAMETERS_NEXT:
         if (focusedObject == SizeTrackEdit->TrackBar ||
             focusedObject == SizeTrackEdit->Edit)
         {
            newFocusedObject = ContrastRTrackEdit->TrackBar;
         }
         else if ((focusedObject == ContrastRTrackEdit->TrackBar ||
             focusedObject == ContrastRTrackEdit->Edit) &&
             !ContrastGangCheckBox->Checked)
         {
            newFocusedObject = ContrastGTrackEdit->TrackBar;
         }
         else if ((focusedObject == ContrastGTrackEdit->TrackBar ||
             focusedObject == ContrastGTrackEdit->Edit) &&
             !ContrastGangCheckBox->Checked)
         {
            newFocusedObject = ContrastBTrackEdit->TrackBar;
         }
         else
         {
            newFocusedObject = SizeTrackEdit->TrackBar;
         }
         break;

      case GRN_CMD_CYCLE_PARAMETERS_PREV:
         if ((focusedObject == ContrastBTrackEdit->TrackBar ||
             focusedObject == ContrastBTrackEdit->Edit) &&
             !ContrastGangCheckBox->Checked)
         {
            newFocusedObject = ContrastGTrackEdit->TrackBar;
         }
         else if ((focusedObject == ContrastGTrackEdit->TrackBar ||
             focusedObject == ContrastGTrackEdit->Edit) &&
             !ContrastGangCheckBox->Checked)
         {
            newFocusedObject = ContrastRTrackEdit->TrackBar;
         }
         else if (focusedObject == ContrastRTrackEdit->TrackBar ||
             focusedObject == ContrastRTrackEdit->Edit)
         {
            newFocusedObject = SizeTrackEdit->TrackBar;
         }
         else if (ContrastGangCheckBox->Checked)
         {
            newFocusedObject = ContrastRTrackEdit->TrackBar;
         }
         else
         {
            newFocusedObject = ContrastBTrackEdit->TrackBar;
         }
         break;

      // SIDE EFFECT ALERT: This changes the actual SETTING as well as the focus!
      case GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT:
         if (TemporalWeightOffRadioButton->Checked)
         {
            TemporalWeightMediumRadioButton->Checked = true;
            newFocusedObject = TemporalWeightMediumTrackEdit->TrackBar;
         }
         else if (TemporalWeightMediumRadioButton->Checked)
         {
            TemporalWeightHighRadioButton->Checked = true;
            newFocusedObject = TemporalWeightHighTrackEdit->TrackBar;
         }
         else // default
         {
            TemporalWeightOffRadioButton->Checked = true;
            newFocusedObject = TemporalAnalysisGroupBox;
         }
         break;

      // SIDE EFFECT ALERT: This changes the actual SETTING as well as the focus!
      case GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV:
         if (TemporalWeightHighRadioButton->Checked)
         {
            TemporalWeightMediumRadioButton->Checked = true;
            newFocusedObject = TemporalWeightMediumTrackEdit->TrackBar;
         }
         else if (TemporalWeightMediumRadioButton->Checked)
         {
            TemporalWeightOffRadioButton->Checked = true;
            newFocusedObject = TemporalAnalysisGroupBox;
         }
         else
         {
            TemporalWeightHighRadioButton->Checked = true;
            newFocusedObject = TemporalWeightHighTrackEdit->TrackBar;
         }
         break;

      default:
         break;
   }

   return newFocusedObject;
}

TObject *TGrainToolForm::computeFocusedControlGenerate(int commandNumber, TObject *focusedObject)
{
   TObject *newFocusedObject = NULL;

   switch(commandNumber)
   {
      case GRN_CMD_CYCLE_PARAMETERS_NEXT:
         if (focusedObject == SizeTrackEditFrame->TrackBar
         || focusedObject == SizeTrackEditFrame->Edit)
         {
            newFocusedObject = LowlightsTrackEditFrame->TrackBar;
         }
         else if (focusedObject == LowlightsTrackEditFrame->TrackBar
         || focusedObject == LowlightsTrackEditFrame->Edit)
         {
            newFocusedObject = HighlightsTrackEditFrame->TrackBar;
         }
         else if (focusedObject == HighlightsTrackEditFrame->TrackBar
         || focusedObject == HighlightsTrackEditFrame->Edit)
         {
            newFocusedObject = SaturationTrackEditFrame->TrackBar;
         }
         else if (focusedObject == SaturationTrackEditFrame->TrackBar
         || focusedObject == SaturationTrackEditFrame->Edit)
         {
            newFocusedObject = RoughnessTrackEditFrame->TrackBar;
         }
         else
         {
            newFocusedObject = SizeTrackEditFrame->TrackBar;
         }
         break;

         case GRN_CMD_CYCLE_PARAMETERS_PREV:
         if (focusedObject == SizeTrackEditFrame->TrackBar
         || focusedObject == SizeTrackEditFrame->Edit)
         {
            newFocusedObject = RoughnessTrackEditFrame->TrackBar;
         }
         else if (focusedObject == LowlightsTrackEditFrame->TrackBar
         || focusedObject == LowlightsTrackEditFrame->Edit)
         {
            newFocusedObject = SizeTrackEditFrame->TrackBar;
         }
         else if (focusedObject == HighlightsTrackEditFrame->TrackBar
         || focusedObject == HighlightsTrackEditFrame->Edit)
         {
            newFocusedObject = LowlightsTrackEditFrame->TrackBar;
         }
         else if (focusedObject == SaturationTrackEditFrame->TrackBar
         || focusedObject == SaturationTrackEditFrame->Edit)
         {
            newFocusedObject = HighlightsTrackEditFrame->TrackBar;
         }
         else if (focusedObject == RoughnessTrackEditFrame->TrackBar
         || focusedObject == RoughnessTrackEditFrame->Edit)
         {
            newFocusedObject = SaturationTrackEditFrame->TrackBar;
         }
         else
         {
            newFocusedObject = RoughnessTrackEditFrame->TrackBar;
         }
         break;

     // SIDE EFFECT ALERT! This actually changes the SETTING, not the focus!
      case GRN_CMD_CREATE_TOGGLE_CLIP_PROJECT:
         if (this->ProjectRadioButton->Checked)
         {
            this->ClipRadioButton->Checked = TRUE;
         }
         else
         {
            this->ProjectRadioButton->Checked = TRUE;
         }

         break;

     // SIDE EFFECT ALERT! This actually has nothing to do with setting the focus!
	  case GRN_CMD_CREATE_SAVE_PRESETS:
 	  	  this->SavePresets();
        break;

      default:
         break;
   }

   return newFocusedObject;
}

//---------------------------------------------------------------------------

void ToggleContrastGang()
{
   if (GrainToolForm == NULL) return;

   GrainToolForm->toggleContrastGang();
}

void TGrainToolForm::toggleContrastGang()
{
   if ((GGrainTool == NULL) || GGrainTool->IsDisabled() || !IsToolVisible())
	  return;

   if (this->GrainPageControl->ActivePage == this->ReduceTabSheet)
   {
	  ContrastGangCheckBox->Checked = !ContrastGangCheckBox->Checked;
   }
   else if (this->GrainPageControl->ActivePage == this->CreateTabSheet)
   {
	  // Must be changed if more than 2 presets are defined
	  if (this->Preset1Button->Down)
	  {
		 this->Preset2Button->OnClick(this->Preset2Button);
	  }
	  else
	  {
		 this->Preset1Button->OnClick(this->Preset1Button);
      }
   }
}

//---------------------------------------------------------------------------

void FocusOnBrushParameterTrackEdit(int whichTrackEdit)
{
   if (GrainToolForm == NULL)
   {
      return;
   }

   GrainToolForm->focusOnBrushParameterTrackEdit(whichTrackEdit);
}

void TGrainToolForm::focusOnBrushParameterTrackEdit(int whichTrackEdit)
{
   if ((GGrainTool == NULL) || GGrainTool->IsDisabled() || !IsToolVisible())
   {
	  return;
   }

   if (!this->UseBrushCheckBox->Enabled || !this->UseBrushCheckBox->Checked)
   {
      return;
   }

   if (this->GrainPageControl->ActivePage == this->ReduceTabSheet)
   {
	  this->GrainPageControl->ActivePage = this->CreateTabSheet;
   }

   switch (whichTrackEdit)
   {
      case GRN_CMD_SELECT_BRUSH_RADIUS:
         this->RadiusTrackEditFrame->TakeFocus();
         break;

      case GRN_CMD_SELECT_BRUSH_OPACITY:
         this->OpacityTrackEditFrame->TakeFocus();
         break;

      case GRN_CMD_SELECT_BRUSH_BLEND:
         this->BlendTrackEditFrame->TakeFocus();
         break;

      case GRN_CMD_SELECT_BRUSH_ASPECT:
         this->AspectTrackEditFrame->TakeFocus();
         break;

      case GRN_CMD_SELECT_BRUSH_ANGLE:
         this->AngleTrackEditFrame->TakeFocus();
         break;

      default:
         break;
   }

}
//---------------------------------------------------------------------------

void ToggleAutoAccept()
{
   if (GrainToolForm == NULL)
   {
      return;
   }

   GrainToolForm->AutoAcceptCheckBox->Checked = !GrainToolForm->AutoAcceptCheckBox->Checked;
}
//---------------------------------------------------------------------------
int  GetChannelMask()
{
   if (GrainToolForm == NULL)
   {
      return GrainGenerator::ChannelMaskBits::All;
   }

   int redFlag = GrainToolForm->RedChannelButton->Down
            ? (int) GrainGenerator::ChannelMaskBits::Red
            : 0;
   int greenFlag = GrainToolForm->GreenChannelButton->Down
            ? (int) GrainGenerator::ChannelMaskBits::Green
            : 0;
   int blueFlag = GrainToolForm->BlueChannelButton->Down
            ? (int) GrainGenerator::ChannelMaskBits::Blue
            : 0;

   return redFlag | greenFlag | blueFlag;
}
//---------------------------------------------------------------------------

void SwitchSubtool()
{
   if (GrainToolForm == NULL)
   {
      return;
   }

   GrainToolForm->switchSubtool();
}

void TGrainToolForm::switchSubtool()
{
   if (this->GrainPageControl->ActivePage == this->CreateTabSheet)
   {
	  this->GrainPageControl->ActivePage = this->ReduceTabSheet;
   }
   else
   {
	  this->GrainPageControl->ActivePage = this->CreateTabSheet;
   }
}
//---------------------------------------------------------------------------

bool TGrainToolForm::ReadSettings(CIniFile *ini, const string &iniSectionName)
{
	CGrainToolParameters toolParams;
	GatherParameters(toolParams);

	CIniFile* iniAF = CreateIniFile(GrainIniFileName);
	if (iniAF == NULL)
	{
		TRACE_0(errout << "ERROR: TGrainToolForm::ReadSettings: Could not create " <<
			GrainIniFileName << endl << "Because " << theError.getMessage());
		return (false);
	}

	if (GGrainTool != NULL)
	{
		GGrainTool->GetGrainGeneratorModel()->LoadFromGrainIni(iniAF);
	}

	toolParams.lGrainRow = iniAF->ReadInteger(iniSectionName, sizeKey, GRN_DEFAULT_SIZE);
	toolParams.lGrainCol = toolParams.lGrainRow;
	toolParams.faContrast[0] = iniAF->ReadDouble(iniSectionName, contrast0Key,
		GRN_DEFAULT_CONTRAST);
	toolParams.faContrast[1] = iniAF->ReadDouble(iniSectionName, contrast1Key,
		GRN_DEFAULT_CONTRAST);
	toolParams.faContrast[2] = iniAF->ReadDouble(iniSectionName, contrast2Key,
		GRN_DEFAULT_CONTRAST);
	toolParams.lTempWeight = iniAF->ReadInteger(iniSectionName, tempweightKey,
		(long)GRN_DEFAULT_TEMP_WEIGHT);

	toolParams.bReduce = iniAF->ReadBool(iniSectionName, CGrainToolParameters::reduceKey, TRUE);
	toolParams.bGenerate = iniAF->ReadBool(iniSectionName, CGrainToolParameters::generateKey, TRUE);

	SetParameters(toolParams);

	return DeleteIniFile(iniAF);
}
//---------------------------------------------------------------------------

bool TGrainToolForm::WriteSettings(CIniFile *ini, const string &iniSectionName)
{
	CIniFile* iniAF = CreateIniFile(GrainIniFileName);
	if (iniAF == NULL)
	{
		TRACE_0(errout << "ERROR: TGrainToolForm::WriteSettings: Could not create " <<
			GrainIniFileName << endl << "Because " << theError.getMessage());
		return false;
	}

	CGrainToolParameters toolParams;
	GatherParameters(toolParams);

	iniAF->WriteInteger(iniSectionName, sizeKey, toolParams.lGrainRow);
	iniAF->WriteDouble(iniSectionName, contrast0Key, toolParams.faContrast[0]);
	iniAF->WriteDouble(iniSectionName, contrast1Key, toolParams.faContrast[1]);
	iniAF->WriteDouble(iniSectionName, contrast2Key, toolParams.faContrast[2]);
	iniAF->WriteInteger(iniSectionName, tempweightKey, toolParams.lTempWeight);
	iniAF->WriteBool(iniSectionName, CGrainToolParameters::reduceKey, toolParams.bReduce);
	iniAF->WriteBool(iniSectionName, CGrainToolParameters::generateKey, toolParams.bGenerate);

	if (GGrainTool != NULL)
	{
		GGrainTool->GetGrainGeneratorModel()->SaveToGrainIni(iniAF);
	}

	return DeleteIniFile(iniAF);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ContrastGangCheckBoxClick(TObject *Sender)
{
   if (ContrastGangCheckBox->Checked)
   {
      // if two of the three values agree, use that value, else use average
      int setting = (ContrastRTrackEdit->GetValue() +
                     ContrastGTrackEdit->GetValue() +
                     ContrastBTrackEdit->GetValue() ) / 3;

      if (ContrastRTrackEdit->GetValue() == ContrastGTrackEdit->GetValue() ||
          ContrastRTrackEdit->GetValue() == ContrastBTrackEdit->GetValue())
      {
         setting = ContrastRTrackEdit->GetValue();
      }
      else if (ContrastGTrackEdit->GetValue() == ContrastBTrackEdit->GetValue())
      {
         setting = ContrastGTrackEdit->GetValue();
      }

      updatingRContrast = true;
      ContrastRTrackEdit->SetValue(setting);
      ContrastGTrackEdit->SetValue(setting);
      ContrastBTrackEdit->SetValue(setting);
      updatingRContrast = false;
   }

}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ContrastRTrackEditChange(
      TObject *Sender)
{
   if (ContrastGangCheckBox->Checked && !updatingRContrast)
   {
      updatingRContrast = true;
      ContrastGTrackEdit->SetValue(ContrastRTrackEdit->GetValue());
      ContrastBTrackEdit->SetValue(ContrastRTrackEdit->GetValue());
      updatingRContrast = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ContrastGTrackEditChange(
      TObject *Sender)
{
   if (ContrastGangCheckBox->Checked && !updatingGContrast)
   {
      updatingGContrast = true;
      ContrastRTrackEdit->SetValue(ContrastGTrackEdit->GetValue());
      ContrastBTrackEdit->SetValue(ContrastGTrackEdit->GetValue());
      updatingGContrast = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ContrastBTrackEditChange(
      TObject *Sender)
{
   if (ContrastGangCheckBox->Checked && !updatingBContrast)
   {
      updatingBContrast = true;
      ContrastRTrackEdit->SetValue(ContrastBTrackEdit->GetValue());
      ContrastGTrackEdit->SetValue(ContrastBTrackEdit->GetValue());
      updatingBContrast = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::GrainGeneratorTrackBarNotifyWidgetClick(TObject *Sender)
{
	 GGrainTool->GetGrainGeneratorModel()->SetGrainParameters(ReadGrainParametersFromGUI());
}

//---------------------------------------------------------------------------

 const GrainParameters TGrainToolForm::ReadGrainParametersFromGUI() const
 {
	 GrainParameters grainParameters;
	 grainParameters.GrainSize = (this->SizeTrackEditFrame->TrackBar->Position + 19) / 20.00;
	 grainParameters.Lowlights = this->LowlightsTrackEditFrame->TrackBar->Position;
	 grainParameters.Highlights = this->HighlightsTrackEditFrame->TrackBar->Position;
	 grainParameters.Saturation = this->SaturationTrackEditFrame->TrackBar->Position;
	 grainParameters.Roughness = this->RoughnessTrackEditFrame->TrackBar->Position;

	 return grainParameters;
 }

 void TGrainToolForm::SetGrainParametersToGUI(const GrainParameters &grainParameters)
 {
	 this->SizeTrackEditFrame->TrackBar->Position = (20 * grainParameters.GrainSize) - 19;
	 this->LowlightsTrackEditFrame->TrackBar->Position = grainParameters.Lowlights;
	 this->HighlightsTrackEditFrame->TrackBar->Position = grainParameters.Highlights;
	 this->SaturationTrackEditFrame->TrackBar->Position = grainParameters.Saturation;
	 this->RoughnessTrackEditFrame->TrackBar->Position = grainParameters.Roughness;
 }

void __fastcall TGrainToolForm::PresetButtonClick(TObject *Sender)
{
	TSpeedButton *speedButton = (TSpeedButton *)Sender;
	int newPreset = (int)speedButton->Tag;
	GGrainTool->GetGrainGeneratorModel()->SetPreset(newPreset);
	this->PresetHasChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::SavePresetButtonClick(TObject *Sender)
{
	this->SavePresets();
}
//---------------------------------------------------------------------------

void TGrainToolForm::SavePresets()
{
	GGrainTool->GetGrainGeneratorModel()->SetCurrentToSelectedPreset();
	GGrainTool->GetGrainGeneratorModel()->WritePresetsToIni();
	this->SavePresetButton->Enabled = !GGrainTool->GetGrainGeneratorModel()->DoesCurrentGrainParametersMatchPreset();
}

//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::PresetRadioButtonClick(TObject *Sender)
{
	TControl *control = (TControl *)Sender;
	GrainParameterBaseType base = (GrainParameterBaseType)control->Tag;
	GGrainTool->GetGrainGeneratorModel()->SetGrainParametersBase(base);
}
//---------------------------------------------------------------------------

void TGrainToolForm::GrainParametersHaveChanged(void *sender)
{
	 this->SavePresetButton->Enabled = !GGrainTool->GetGrainGeneratorModel()->DoesCurrentGrainParametersMatchPreset();
}
//---------------------------------------------------------------------------

 void TGrainToolForm::PresetHasChanged(void *sender)
 {
	 // Just update the GUI
	 switch (GGrainTool->GetGrainGeneratorModel()->GetGrainParametersBase())
	 {
	 case GP_BASE_PROJECT:
		 this->ProjectRadioButton->Checked = true;
		 break;

	 case GP_BASE_CLIP:
		 this->ClipRadioButton->Checked = true;
		 break;

	 default:
		 break;
	 }

	 // Just update the GUI
	 switch (GGrainTool->GetGrainGeneratorModel()->GetPreset())
	 {
	 case 0:
		 this->Preset1Button->Down = true;
		 break;

	 case 1:
		 this->Preset2Button->Down = true;
		 break;

	 }

	 // Set the GUI
	 this->SetGrainParametersToGUI(GGrainTool->GetGrainGeneratorModel()->GetSelectedGrainParameters());

	 // Always should return false but check for logical error
	 this->SavePresetButton->Enabled = !GGrainTool->GetGrainGeneratorModel()->DoesCurrentGrainParametersMatchPreset();
 }
//---------------------------------------------------------------------------

//void __fastcall TGrainToolForm::ToggleButtonClick(TObject *Sender)
//{
//   GGrainTool->GetGrainGeneratorModel()->ReadPresetsFromIni();
//}
//---------------------------------------------------------------------------

void TGrainToolForm::UpdateProvisionButtons()
{
   if (GGrainTool->getSystemAPI()->GetProvisionalFrameIndex() != GGrainTool->getSystemAPI()->getLastFrameIndex())
   {
	   ShowOriginalSpeedButton->Enabled = false;
	   ShowProcessedSpeedButton->Enabled = false;
	   return;
   }

   if (GGrainTool->getSystemAPI()->IsProvisionalPending())
   {
	   ShowOriginalSpeedButton->Enabled = true;
	   ShowProcessedSpeedButton->Enabled = true;

	   ShowProcessedSpeedButton->Down = GGrainTool->getSystemAPI()->IsProcessedVisible();
	   ShowOriginalSpeedButton->Down = !ShowProcessedSpeedButton->Down;
   }
   else
   {
	   // No provisions
	   ShowOriginalSpeedButton->Enabled = false;
	   ShowProcessedSpeedButton->Enabled = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ShowOriginalSpeedButtonClick(TObject *Sender)
{
	if (!GGrainTool->getSystemAPI()->IsProvisionalPending())
	{
		return;
	}

	if (GGrainTool->getSystemAPI()->IsProcessedVisible())
	{
		GGrainTool->getSystemAPI()->ToggleProvisional();
	}

	EnableGroupBox->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ShowProcessedSpeedButtonClick(TObject *Sender)
{
	if (!GGrainTool->getSystemAPI()->IsProvisionalPending())
	{
		return;
	}

	if (!GGrainTool->getSystemAPI()->IsProcessedVisible())
	{
		GGrainTool->getSystemAPI()->ToggleProvisional();
	}

	EnableGroupBox->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::RefreshTimerTimer(TObject *Sender)
{
  // MAJOR LEAGUE KLUDGE, see if frame has changed
   if ((GGrainTool == NULL) || GGrainTool->IsDisabled() || !IsToolVisible())
	  return;

  this->UpdateProvisionButtons();
}
//---------------------------------------------------------------------------

void TGrainToolForm::UpdatePreviewBrush()
{
	auto previewBrush = new CFloatBrush();
	previewBrush->setBrushParameters(100, 35, BlendTrackEditFrame->TrackBar->Position,
		OpacityTrackEditFrame->TrackBar->Position, AspectTrackEditFrame->TrackBar->Position,
		AngleTrackEditFrame->TrackBar->Position, RectSpeedButton->Down);

	this->_previewBrushRevealer->ResetMask();

	int pw = BrushPreviewPaintBox->Width;
	int ph = BrushPreviewPaintBox->Height;
	this->_previewBrushRevealer->StampMask(previewBrush, pw / 2, ph / 2);

	auto previewSizeInComponents = COMPONENTS * pw * ph;
	auto previewBrushImage = ippsMalloc_32f(previewSizeInComponents);
	IppThrowOnError(ippsSet_32f(0.5f, previewBrushImage, previewSizeInComponents));

	MTI_REAL32 *tempImage = this->_previewBrushRevealer->AlphaBlend(this->_prevewGrayImageNormalize,
		previewBrushImage);

	auto tempProcessImage = ippsMalloc_8u(previewSizeInComponents);
	ippsConvert_32f8u_Sfs(tempImage, tempProcessImage, previewSizeInComponents, ippRndNear, 0);

	TBitmap *b = this->BrushPreviewPaintBox->Picture->Bitmap;
	b->Width = pw;
	b->Height = ph;
	b->PixelFormat = pf24bit;
	for (int i = 0; i < ph; i++)
	{
		auto p1 = reinterpret_cast<unsigned char*>(b->ScanLine[i]);
		ippsCopy_8u(tempProcessImage + i * COMPONENTS*pw, (Ipp8u*)p1, COMPONENTS*pw);
	}

	this->BrushPreviewPaintBox->Picture->Bitmap = b;

	ippsFree(previewBrushImage);
	delete previewBrush;
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::onBrushShapeChange(TObject *Sender)

{
   // For parameters that change the shape of the brush, center the brush
   // so you can see it.
   GGrainTool->CenterBrushOnFrame();
   this->onBrushParameterChange(Sender);
}

void __fastcall TGrainToolForm::onBrushParameterChange(TObject *Sender)
{
	this->UpdatePreviewBrush();

   int minRadius;
   int maxRadius;
   RadiusTrackEditFrame->GetMinAndMax(minRadius, maxRadius);

   GGrainTool->setBrushParameters(
      maxRadius,
      RadiusTrackEditFrame->GetValue(),
      BlendTrackEditFrame->GetValue(),
		OpacityTrackEditFrame->GetValue(),
      AspectTrackEditFrame->GetValue(),
		AngleTrackEditFrame->GetValue(),
      RectSpeedButton->Down);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::BrushTabSheetShow(TObject *Sender)
{
	this->UpdatePreviewBrush();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::RejectButtonClick(TObject *Sender)
{
	GGrainTool->RejectBrushStrokes(true);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::AcceptButtonClick(TObject *Sender)
{
	GGrainTool->AcceptBrushStrokes(true);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::UndoLastButtonClick(TObject *Sender)
{
	GGrainTool->UndoLastBrushStroke();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ReduceCheckBoxClick(TObject *Sender)
{
   EnableOrDisableReduceStuff();
   ReduceOrCreateCheckBoxClick(Sender);
}
//---------------------------------------------------------------------------

void TGrainToolForm::EnableOrDisableReduceStuff()
{
   bool doReduce(ReduceCheckBox->Checked);

	SizeTrackEdit->Enable(doReduce);
	ContrastRTrackEdit->Enable(doReduce);
	ContrastGTrackEdit->Enable(doReduce);
	ContrastBTrackEdit->Enable(doReduce);
	TemporalWeightHighTrackEdit->Enable(doReduce);
	TemporalWeightMediumTrackEdit->Enable(doReduce);
	ContrastGangCheckBox->Enabled = doReduce;
	TemporalAnalysisGroupBox->Enabled = doReduce;
	TemporalWeightOffRadioButton->Enabled = doReduce;
	TemporalWeightMediumRadioButton->Enabled = doReduce;
	TemporalWeightHighRadioButton->Enabled = doReduce;
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::CreateCheckBoxClick(TObject *Sender)
{
   UseBrushCheckBox->Enabled = CreateCheckBox->Checked;
   UseBrushLabel->Enabled = UseBrushCheckBox->Enabled;
   if (!UseBrushCheckBox->Enabled)
   {
      UseBrushCheckBox->Checked = false;
   }

   EnableOrDisableBrushStuff();
   EnableOrDisableCreateStuff();
   ReduceOrCreateCheckBoxClick(Sender);
}
//---------------------------------------------------------------------------

void TGrainToolForm::EnableOrDisableCreateStuff()
{
   bool doCreate = CreateCheckBox->Checked;

   SizeTrackEditFrame->Enable(doCreate);
	LowlightsTrackEditFrame->Enable(doCreate);
	HighlightsTrackEditFrame->Enable(doCreate);
	SaturationTrackEditFrame->Enable(doCreate);
	RoughnessTrackEditFrame->Enable(doCreate);
	Preset1Button->Enabled = doCreate;
	Preset2Button->Enabled = doCreate;
	SavePresetButton->Enabled = doCreate && !GGrainTool->GetGrainGeneratorModel()->DoesCurrentGrainParametersMatchPreset();
	ProjectRadioButton->Enabled = doCreate;
	ClipRadioButton->Enabled = doCreate;
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ReduceOrCreateCheckBoxClick(TObject *Sender)
{
	bool reduce = this->ReduceCheckBox->Checked;
	bool generate = this->CreateCheckBox->Checked;

	bool canDo = reduce || generate;

	if (!canDo)
	{
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//		ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Can't preview - select reduce or create");
//		ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Can't render - select reduce or create");
//		ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't add to PDL - select reduce or create");
		ExecButtonsToolbar->DisableResumeWidget();
	}
	else
	{
		ExecButtonsToolbar->EnableResumeWidget();
		this->UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
	}
}
//---------------------------------------------------------------------------
void __fastcall TGrainToolForm::UseBrushCheckBoxClick(TObject *Sender)
{
   bool useBrush = UseBrushCheckBox->Enabled && UseBrushCheckBox->Checked;
   GGrainTool->GetGrainGeneratorModel()->SetUseBrushMask(useBrush);
   onBrushParameterChange(NULL);
   GGrainTool->SetBrushActive(useBrush);

   // QQQ need to disable the brush check box while execution is in progress
   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);

   // Can't enable Reduce if using brush
   ReduceCheckBox->Enabled = !useBrush;
   ReduceGrainLabel->Enabled = ReduceCheckBox->Enabled;
   if (!ReduceCheckBox->Enabled)
   {
      ReduceCheckBox->Checked = false;
      EnableOrDisableReduceStuff();
   }

   EnableOrDisableBrushStuff();

   if (useBrush)
   {
      GGrainTool->CenterBrushOnFrame();
   }
}
//---------------------------------------------------------------------------
void TGrainToolForm::EnableOrDisableBrushStuff()
{
   bool useBrush = UseBrushCheckBox->Enabled && UseBrushCheckBox->Checked;
   bool brushStrokesArePending = GGrainTool->AreBrushStrokesPending();

   this->BlendTrackEditFrame->Enable(useBrush);
   this->OpacityTrackEditFrame->Enable(useBrush);
   this->RadiusTrackEditFrame->Enable(useBrush);
   this->AngleTrackEditFrame->Enable(useBrush);
   this->AspectTrackEditFrame->Enable(useBrush);
   this->RectSpeedButton->Enabled = useBrush;
   this->EllipseSpeedButton->Enabled = useBrush;
   this->BrushStrokeListBox->Enabled = useBrush;
   this->PendingStrokesLabel->Enabled = useBrush;
   this->AutoAcceptCheckBox->Enabled = useBrush;
   this->BrushPreviewPaintBox->Visible = useBrush;
   this->UndoLastButton->Enabled = brushStrokesArePending;
   this->RejectButton->Enabled = brushStrokesArePending;
   this->AcceptButton->Enabled = brushStrokesArePending;
   this->RepeatStrokesSpeedButton->Enabled = GGrainTool->AreThereRepeatableStrokes();

   // When brush is active, execution buttons may change as well
   EToolControlState toolControlState = GGrainTool->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      this->UpdateExecutionButtonToolbar(toolControlState, false);
   }

}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::AutoAcceptCheckBoxClick(TObject *Sender)
{
   GGrainTool->SetAutoAccept(AutoAcceptCheckBox->Checked);
}
//---------------------------------------------------------------------------

void TGrainToolForm::UpdateBrushRadiusRange()
{
   const CImageFormat *imageFormat = GGrainTool->getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == NULL)
   {
      return;
   }

   int maxRadius = imageFormat->getTotalFrameWidth() / 2;
   int oldMin;
   int oldMax;
   this->RadiusTrackEditFrame->GetMinAndMax(oldMin, oldMax);
   if (oldMin == 0 && oldMax == maxRadius)
   {
      return;
   }

   this->RadiusTrackEditFrame->SetMinAndMax(0, maxRadius);
   onBrushParameterChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TGrainToolForm::ReduceCheckBoxEnter(TObject *Sender)
{
   ReduceGrainLabel->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ReduceCheckBoxExit(TObject *Sender)
{
   ReduceGrainLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::CreateCheckBoxEnter(TObject *Sender)
{
   CreateGrainLabel->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::CreateCheckBoxExit(TObject *Sender)
{
   CreateGrainLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::UseBrushCheckBoxEnter(TObject *Sender)
{
   UseBrushLabel->Font->Style = (TFontStyles() << fsBold);
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::UseBrushCheckBoxExit(TObject *Sender)
{
   UseBrushLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::RepeatStrokesSpeedButtonClick(TObject *Sender)
{
   GGrainTool->RepeatBrushStrokes();
}
//--------------------------------------------------------------------------

bool TGrainToolForm::formKeyDown(WORD &Key, TShiftState Shift)
{
//   if (TCompactTrackEditFrame::ProcessKeyDown(Key, Shift))
//   {
//      Key = 0;
//      return true;
//   }
//
   return false;
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::ChannelButtonClick(TObject *Sender)
{
   bool redIsDown = RedChannelButton->Down;
   bool greenIsDown = GreenChannelButton->Down;
   bool blueIsDown = BlueChannelButton->Down;
   NoChannelsSelectedWarningLabel->Visible = !(redIsDown || greenIsDown || blueIsDown);
   AllChannelsButton->AllowAllUp = !(redIsDown && greenIsDown && blueIsDown);
   AllChannelsButton->Down = redIsDown && greenIsDown && blueIsDown;
}
//---------------------------------------------------------------------------

void __fastcall TGrainToolForm::AllChannelsButtonClick(TObject *Sender)
{
   RedChannelButton->Down = true;
   GreenChannelButton->Down = true;
   BlueChannelButton->Down = true;
   NoChannelsSelectedWarningLabel->Visible = false;
   AllChannelsButton->AllowAllUp = false;
   AllChannelsButton->Down = true;
}
//---------------------------------------------------------------------------


