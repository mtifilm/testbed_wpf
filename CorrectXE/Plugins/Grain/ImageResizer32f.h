//---------------------------------------------------------------------------

#ifndef ImageResizer32fH
#define ImageResizer32fH

// This is a class using IPP to resize a 3 channel float 32 image.
// It is assumed the image stepsize is exactly the width*3*sizeof(float)
// Once this class is created, the resize function may be called multipel times

#include "Ippheaders.h"

class ImageResizer32f
{
public:
	ImageResizer32f(const IppiSize sourceSize,const IppiSize destinationSize);
	~ImageResizer32f();

	void Resize(const Ipp32f *source, Ipp32f *destination);
    Ipp32f* CreateAndResize(const Ipp32f *source);

private:
	// This sets up the resize
	void InitializeResize_C3(const IppiSize sourceSize,const IppiSize dstSize);
	void FreeInternalMemory();

	IppiResizeSpec_32f* _ippiResizeSpec;
	Ipp8u* _resizeBuffer;
	IppiSize _sourceSize;
	IppiSize _destinationSize;
};

//---------------------------------------------------------------------------
#endif
