// err_grain.h

#ifndef ERR_GRAIN_H
#define ERR_GRAIN_H

// allowed Grain Reduction range for error values:  -7149 to -7100

#define ERR_GRAIN_BAD_THREAD -7100     // thread index out-of-range

#endif
