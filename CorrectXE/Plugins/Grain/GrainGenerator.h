//---------------------------------------------------------------------------

#ifndef GrainGeneratorH
#define GrainGeneratorH

#include "GrainCoreApi.h"
#include "GrainParameters.h"
#include "ImageResizer32f.h"
#include "NormalRandomImageC3.h"

#include "Ippheaders.h"
#include "ThreadLocker.h"

MTI_GRAINCORE_API class GrainGenerator
{
	public:
	   GrainGenerator(const IppiSize &size);
	   GrainGenerator(const IppiSize &size, const GrainParameters &parameters);
	   ~GrainGenerator();

	   void SetGrainSize(double grainSize);
//	   double GetGrainSize();

	   void SetLowlights(double lowlights);
//	   double GetLowlights();

	   void SetHighlights(double highlights);
//	   double GetHighlights();

	   void SetRoughness(double roughness);
//	   double GetRoughness();

	   void SetSaturation(double saturation);
//		double GetSaturation();

	   void SetGrainParameters(const GrainParameters &parameters);
		const GrainParameters &GetGrainParameters() const;

		MTI_UINT16 *ApplyGrain(const MTI_UINT16 *source, int maxValue, int channelMask, bool sourceDataIsNew, bool generateNewSeed, bool dataTypeIsUnsignedShort);

		void SetSourceImage(const MTI_UINT16* source, int maxValue, bool dataIsUnsignedShorts);
		void SetSeed(unsigned int seed);
		void UnsetSeed();
		unsigned int GetSeed();
		const float* GetNormalizedGrainValues();

	   // Were grain parameters changed since the last calculation?
		bool AreGrainValuesInvalid() const;

		int GetImageSizeInPixels() const;
		int GetImageSizeInComponents() const;
		int GetImageSizeInBytes() const;

	   IppiSize GetImageSize() const;

      enum ChannelMaskBits
      {
         None = 0,
         Red = 1,
         Green = 2,
         Blue = 4,
         All = 7
      };

private:
	ImageResizer32f *_imageResizer = nullptr;
	NormalRandomImageC3 *_normalRandomImage = nullptr;
	unsigned int _randomImageSeed = 0xFFFFFFFFU;
	bool _randomImageSeedWasSet = false;
	bool _randomImageSeedWasChanged = false;
   bool _dataIsUnsignedShorts = true;
	IppiSize _destinationImageSize;      // Initialized by the constructor.
	IppiSize _randomImageSize = {0, 0};

	float *_grainImage = nullptr;
//	float *_factors = nullptr;
	float *_roughnessImage = nullptr;
   double _roughnessImageRoughness = 1.0;
	float *_normalizedSourceImage = nullptr;
	unsigned short *_internalFormatImage = nullptr;
	float *_internalDestinationImage = nullptr;
	float *_normalizedGrainValues = nullptr;
   float *_channelMaskedGrainValues = nullptr;

	bool	_areGrainValuesInvalid = true;

	GrainParameters _grainParameters; // CAREFUL - only initialized by one of the two constructors!!
	GrainParameters _grainParametersNew;  // Initialized by the constructor.

	CSpinLock _lock;

	MTI_UINT16 *ApplyGrainToUnsignedShortData(const MTI_UINT16 *source, int maxValue, int channelMask, bool sourceDataIsNew, bool generateNewSeed);
	MTI_UINT16 *ApplyGrainToHalfData(const MTI_UINT16 *source, int channelMask, bool sourceDataIsNew, bool generateNewSeed);
	float *ApplyGrain(const float *source, int channelMask, bool sourceDataIsNew, bool generateNewSeed);
	const Ipp32f* GetNormalizedGrainValues(const Ipp32f* normalizedSource, bool sourceDataIsNew, bool generateNewSeed);
};

//---------------------------------------------------------------------------
#endif
