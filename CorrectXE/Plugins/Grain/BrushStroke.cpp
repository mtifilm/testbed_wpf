// ---------------------------------------------------------------------------
#include "BrushStroke.h"
// ---------------------------------------------------------------------------

BrushStroke::BrushStroke(bool erasing)
: _isErasing(erasing)
{
}

BrushStroke::BrushStroke(CBrush *brush, bool erasing)
: _isErasing(erasing)
{
	this->_brushParameters.MaxRadius = brush->getMaxRadius();
	this->_brushParameters.Radius = brush->getRadius();
	this->_brushParameters.Blend = brush->getBlend();
	this->_brushParameters.Opacity = brush->getOpacity();
	this->_brushParameters.Aspect = brush->getAspect();
	this->_brushParameters.Angle = brush->getAngle();
	this->_brushParameters.Shape = brush->getShape();
	this->_brushParameters.Type = brush->getType();
}

BrushStroke::BrushStroke(const BrushParameters &parameters, bool erasing)
: _isErasing(erasing)
{
	this->_brushParameters = parameters;
}

BrushStroke::BrushStroke(const BrushStroke &brushStroke)
{
	*this = brushStroke;
}

BrushStroke::~BrushStroke()
{
}

void BrushStroke::SetBrushParameters(const BrushParameters &parameters)
{
	this->_brushParameters = parameters;
}

const BrushParameters &BrushStroke::GetBrushParameters() const
{
	return this->_brushParameters;
}

bool BrushStroke::IsErasing() const
{
   return this->_isErasing;
}

bool BrushStroke::HasSameParametersAndMode(const BrushStroke &bs) const
{
   return this->_brushParameters == bs._brushParameters
          && this->_isErasing == bs._isErasing;
}

bool BrushStroke::operator==(const BrushStroke &bs) const
{
   bool pointsAreEqual = this->Points.size() == bs.Points.size();
   if (pointsAreEqual)
   {
      for (int i; i < this->Points.size(); ++i)
      {
         if (this->Points[i].x != bs.Points[i].x
         || this->Points[i].y != bs.Points[i].y)
         {
            pointsAreEqual = false;
            break;
         }
      }
   }

   return pointsAreEqual && this->HasSameParametersAndMode(bs);
}

bool BrushStroke::operator!=(const BrushStroke &bs) const
{
   return !(*this == bs);
}

BrushStroke &BrushStroke::operator=(const BrushStroke &brushStroke)
{
   if (this == &brushStroke)
   {
      return *this;
   }

   this->_brushParameters = brushStroke._brushParameters;
   this->_isErasing = brushStroke._isErasing;
   this->Points = brushStroke.Points;
   return *this;
}

