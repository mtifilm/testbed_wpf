// ---------------------------------------------------------------------------

#ifndef BrushStrokeH
#define BrushStrokeH

#include <vector>
#include "GrainCoreApi.h"
#include "FloatBrush.h"

MTI_GRAINCORE_API class BrushStroke
{
public:
	BrushStroke(bool erasing = false);
	BrushStroke(CBrush *brush, bool erasing = false);
	BrushStroke(const BrushParameters &parameters, bool erasing = false);
	BrushStroke(const BrushStroke &brushStroke);
	~BrushStroke();

   bool operator==(const BrushStroke &bs) const;
   bool operator!=(const BrushStroke &bs) const;
   BrushStroke &operator=(const BrushStroke &bs);

	std::vector<POINT> Points;

	void SetBrushParameters(const BrushParameters &parameters);
	const BrushParameters &GetBrushParameters() const ;
   bool IsErasing() const ;
   bool HasSameParametersAndMode(const BrushStroke &bs) const;

private:
	BrushParameters _brushParameters;
   bool _isErasing;
};

typedef std::vector<BrushStroke> BrushStrokes;

// ---------------------------------------------------------------------------
#endif
