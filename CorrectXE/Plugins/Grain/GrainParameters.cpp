#include "GrainCoreCommon.h"

#include "IniFile.h"
#include "PDL.h"

#define GRAIN_SIZE_DEFAULT 4
#define LOWLIGHTS_DEFAULT 0
#define HIGHLIGHTS_DEFAULT 0
#define SATURATION_DEFAULT 0
#define ROUGHNESS_DEFAULT 41
#define OUT_OF_RANGE_VALUE -1

GrainParameters::GrainParameters()
{
	this->GrainSize = OUT_OF_RANGE_VALUE;
	this->Lowlights = OUT_OF_RANGE_VALUE;
	this->Highlights = OUT_OF_RANGE_VALUE;
	this->Saturation = OUT_OF_RANGE_VALUE;
	this->Roughness = OUT_OF_RANGE_VALUE;
}

GrainParameters::GrainParameters(const GrainParameters &gp)
{
   *this = gp;
}

void GrainParameters::SetDefaults()
{
	this->GrainSize = GRAIN_SIZE_DEFAULT;
	this->Lowlights = LOWLIGHTS_DEFAULT;
	this->Highlights = HIGHLIGHTS_DEFAULT;
	this->Saturation = SATURATION_DEFAULT;
	this->Roughness = ROUGHNESS_DEFAULT;
}

bool GrainParameters::operator == (const GrainParameters &g) const
{
	return (g.GrainSize == this->GrainSize) && (g.Lowlights == this->Lowlights) &&
		(g.Highlights == this->Highlights) && (g.Saturation == this->Saturation) &&
		(g.Roughness == this->Roughness);
}

bool GrainParameters:: operator != (const GrainParameters &g) const
{
	return !(*this == g);
}

GrainParameters& GrainParameters::operator = (const GrainParameters &gp)
{
   if (&gp == this)
   {
      return *this;
   }

	this->GrainSize = gp.GrainSize;
	this->Lowlights = gp.Lowlights;
	this->Highlights = gp.Highlights;
	this->Saturation = gp.Saturation;
	this->Roughness = gp.Roughness;

	return *this;
}

void GrainParameters::ReadSettings(CIniFile *ini, const std::string &grainName)
{
	std::string iniSectionName = "GrainParameters." + grainName;
	this->GrainSize = ini->ReadDouble(iniSectionName, "GrainSize", GRAIN_SIZE_DEFAULT);
	this->Lowlights = ini->ReadDouble(iniSectionName, "Lowlights", LOWLIGHTS_DEFAULT);
	this->Highlights = ini->ReadDouble(iniSectionName, "Highlights", HIGHLIGHTS_DEFAULT);
	this->Saturation = ini->ReadDouble(iniSectionName, "Saturation", SATURATION_DEFAULT);
	this->Roughness = ini->ReadDouble(iniSectionName, "Roughness", ROUGHNESS_DEFAULT);
}

void GrainParameters::WriteSettings(CIniFile *ini, const std::string &grainName) const
{
	std::string iniSectionName = "GrainParameters." + grainName;

	ini->WriteDouble(iniSectionName, "GrainSize", this->GrainSize);
	ini->WriteDouble(iniSectionName, "Lowlights", this->Lowlights);
	ini->WriteDouble(iniSectionName, "Highlights", this->Highlights);
	ini->WriteDouble(iniSectionName, "Saturation", this->Saturation);
	ini->WriteDouble(iniSectionName, "Roughness", this->Roughness);
}

void GrainParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
	this->GrainSize = pdlToolAttribs->GetAttribDouble("GrainSize", GRAIN_SIZE_DEFAULT);
	this->Lowlights = pdlToolAttribs->GetAttribDouble("Lowlights", LOWLIGHTS_DEFAULT);
	this->Highlights = pdlToolAttribs->GetAttribDouble("Highlights", HIGHLIGHTS_DEFAULT);
	this->Saturation = pdlToolAttribs->GetAttribDouble("Saturation", SATURATION_DEFAULT);
	this->Roughness = pdlToolAttribs->GetAttribDouble("Roughness", ROUGHNESS_DEFAULT);
}

void GrainParameters::WritePDLEntry(CPDLElement *pdlToolAttribs) const
{
	pdlToolAttribs->SetAttribDouble("GrainSize", this->GrainSize);
	pdlToolAttribs->SetAttribDouble("Lowlights", this->Lowlights);
	pdlToolAttribs->SetAttribDouble("Highlights", this->Highlights);
	pdlToolAttribs->SetAttribDouble("Saturation", this->Saturation);
	pdlToolAttribs->SetAttribDouble("Roughness", this->Roughness);
}
