//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainToolFullFrameProc.h"

#include "GrainToolGUIWin.h"  // NO! NO! NO!

#include "ImageFormat3.h"
#include "ToolProgressMonitor.h"

#include <thread>         // std::thread
#include <mutex>          // std::mutex, std::lock


//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
CGrainToolFullFrameProc::CGrainToolFullFrameProc(
   int newToolNumber,
   const string &newToolName,
   CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr,
   IToolProgressMonitor *newToolProgressMonitor,
   GrainGeneratorModel *grainGeneratorModel)

 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor),
   _grainGeneratorModel(grainGeneratorModel)
{
  this->_grainReductionAlgorithm = new CGrainReduction_MT();
  StartProcessThread(); // Required by Tool Infrastructure
}
//---------------------------------------------------------------------------

CGrainToolFullFrameProc::~CGrainToolFullFrameProc()
{
   delete this->_grainReductionAlgorithm;
   this->_grainReductionAlgorithm = NULL;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Make sure the number of Grain algorithm processing threads
   // is reasonable
   concurrentFramesCount = toolIOConfig->processingThreadCount;
   if (concurrentFramesCount < 1)
      concurrentFramesCount = 1;
   else if (concurrentFramesCount > GRN_MAX_CONCURRENT_FRAMES_COUNT)
      concurrentFramesCount = GRN_MAX_CONCURRENT_FRAMES_COUNT;

   // Maximum number of input and output frames per processing iteration
   maxOutputFrames = concurrentFramesCount;
   maxInputFrames = maxOutputFrames + 2*GRN_MAX_ADJACENT_FRAMES;

   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // maximum number of input and output frames
   SetInputMaxFrames(0, maxInputFrames, maxOutputFrames);   // Input Port 0
   SetOutputNewFrame(0,0);
   SetOutputMaxFrames(0, maxOutputFrames); // Output Port 0

   const CImageFormat *pIF = GetSrcImageFormat(0);
   if (pIF == NULL)
      return -1;

   // initialize the Grain Algorithm
   //
   // NOTE!
   // mbraca added the last arg bNoAssemblerHack to tell the algorithm to
   // not use assembler code because the assembler code does not work when
   // the data range uses the whole 16 bits!!!
   //
   this->_grainReductionAlgorithm->Initialize(concurrentFramesCount,
                         toolIOConfig->processingThreadsPerFrame,
                         toolIOConfig->doRowSlices,
                         toolIOConfig->slicesPerThread,
                         toolIOConfig->noopProcessing,
                         (pIF->getBitsPerComponent() == 16));

   // Set image format dependent stuff
   int iNumComponents = pIF->getComponentCountExcAlpha();

   long lWidth, lHeight;
   MTI_UINT16 caMin[MAX_COMPONENT];
   MTI_UINT16 caMax[MAX_COMPONENT];
   MTI_UINT16 caFill[MAX_COMPONENT];

   lWidth = pIF->getTotalFrameWidth();
   lHeight = pIF->getTotalFrameHeight();
   pIF->getComponentValueMin(caMin);
   pIF->getComponentValueMax(caMax);
   pIF->getComponentValueFill(caFill);

   this->_grainReductionAlgorithm->setImageSize(lWidth,lHeight);
   this->_grainReductionAlgorithm->setImageComponent(iNumComponents,0,caMin,caMax,caFill);

   this->_grainGeneratorModel->SetImageSize(lWidth,lHeight);

   // Not really necessary, but force a new randomization seed
   this->lCurrentFrameIndex = -1;
   return 0;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   inFrameIndex = frameRange.inFrameIndex + GRN_MAX_ADJACENT_FRAMES;
   outFrameIndex = frameRange.outFrameIndex - GRN_MAX_ADJACENT_FRAMES;

   // Calculate number of frames that need to be processed
   framesRemaining = outFrameIndex - inFrameIndex;  // Exclusive out frame
   framesAlreadyProcessed = 0;

   // Calculate the number of iterations, given that a maximum
   // of maxOutputFrames will be processed on each iteration.  Round up
   // to make sure that there is a last iteration for leftovers.
   iterationCount = (framesRemaining + maxOutputFrames - 1) / maxOutputFrames;

   return 0;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::SetParameters(CToolParameters *toolParams)
{
   CGrainToolParameters *afpToolParams
                               = static_cast<CGrainToolParameters*>(toolParams);

   // Note: we use a CGrainToolParameters default assignment operator
   grainParams = *afpToolParams;

   // ensure lNFrames <= GRN_MAX_INPUT_FRAMES
   if (grainParams.lNFrames > GRN_MAX_INPUT_FRAMES)
      return -1;

   return 0;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::BeginProcessing(SToolProcessingData &procData)
{
   long lRet;

   // adjusting the ROI requires image format info
   const CImageFormat *pIF = GetSrcImageFormat(0);
   if (pIF == NULL)
      return -1;

   int iNumComponents = pIF->getComponentCountExcAlpha();

   this->_grainReductionAlgorithm->setGrainNeighborhoodSize(grainParams.lGrainRow,
                                       grainParams.lGrainCol,
                                       grainParams.lTemporalSupport);

/*
   RECT roiRect = grainParams.GetRegionOfInterest()->getBlendBox();
   pGrainAlg->setROI(roiRect.left,roiRect.top,roiRect.right+1,roiRect.bottom+1);
*/

   float faPercentContrast[3];
   for(int i = 0; i < 3; i++)
     faPercentContrast[i] = grainParams.faContrast[i] / 10.0;

   lRet = this->_grainReductionAlgorithm->setContrast(faPercentContrast,iNumComponents);
   if (lRet)
      return (int) lRet;

   this->_grainReductionAlgorithm->setTempWeight(grainParams.lTempWeight);

   //////////////////////////////////////
   // don't save history - it's broken
   SetSaveToHistory(false);
   //////////////////////////////////////

   if (!GetSystemAPI()->IsMaskAnimated())
      {
      for (int threadIndex = 0; threadIndex < concurrentFramesCount; ++threadIndex)
         {
         lRet = GetSystemAPI()->GetMaskRoi(inFrameIndex,
                                           maskROI[threadIndex]);
         if (lRet != 0)
            return lRet;

         RECT roiRect = maskROI[threadIndex].getBlendBox();
		 this->_grainReductionAlgorithm->setROI(roiRect.left,roiRect.top,roiRect.right+1,
                           roiRect.bottom+1, threadIndex);

		 lRet = this->_grainReductionAlgorithm->setRegionOfInterestPtr(maskROI[threadIndex].getBlendPtr(),
                                                  maskROI[threadIndex].getLabelPtr(),
                                                  threadIndex);
         if (lRet)
            return lRet;
         }

	  lRet = this->_grainReductionAlgorithm->allocate();
      if (lRet)
         return (int) lRet;
      }

   GetToolProgressMonitor()->SetFrameCount(framesRemaining);
   GetToolProgressMonitor()->SetStatusMessage("Processing");
   GetToolProgressMonitor()->StartProgress();

   return 0; // continue processing
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::BeginIteration(SToolProcessingData &procData)
{
   // Determine the number of frames to process for this iteration.
   // Typically this will be maxOutputFrames, but on the last iteration
   // it will be framesRemaining (which will be less than maxOutputFrames)
   framesToProcess = std::min(framesRemaining, maxOutputFrames);

   // The input frame count for this iteration is the number of frames
   // to process (i.e., number of output frames) plus the past and
   // future frames
   int inputFrameCount = framesToProcess + grainParams.lPastFrames
                         + grainParams.lFutureFrames;

   // Index of first output frame
   int firstFrameToProcess = inFrameIndex + framesAlreadyProcessed;

   // Make a list of the frame indices that will be used as input.
   int inputFrameIndexList[GRN_MAX_INPUT_FRAMES];
   for (int i = 0; i < inputFrameCount; ++i)
      {
      inputFrameIndexList[i] = firstFrameToProcess + i
                               - grainParams.lPastFrames;
      }

   // Tell the Tool Infrastructure which frames we want as input
   // during this iteration
   SetInputFrames(0, inputFrameCount, inputFrameIndexList);

   // when finished processing, release the oldest frames that we
   // will no longer need for future iterations
   SetFramesToRelease(0, framesToProcess, inputFrameIndexList);

   // These are the frames that will be the output
   int outputFrameIndexList[GRN_MAX_OUTPUT_FRAMES];
   for (int i = 0; i < framesToProcess; ++i)
	  {
      outputFrameIndexList[i] = firstFrameToProcess + i;
	  }
   SetOutputFrames(0, framesToProcess, outputFrameIndexList);

   if (!SetPriorityClass(GetCurrentProcess(), BELOW_NORMAL_PRIORITY_CLASS))
	  TRACE_3(errout << "INFO: Unable to lower processing thread priority");

   return 0;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::GetIterationCount(SToolProcessingData &procData)
{
   return iterationCount;
}
//---------------------------------------------------------------------------

namespace
{

int nextPow2Minus1(long long n)
{
	n += (n == 0);
	n--;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n |= n >> 32;
	return n;
}

};
//---------------------------------------------------------------------------

static std::mutex g_i_mutex;

int CGrainToolFullFrameProc::DoProcess(SToolProcessingData &procData)
{
//   DBTRACE(procData.iteration);
//   DBTIMER(DO_PROCESS);
	int retVal, lRet;
	CAutoErrorReporter autoErr("CGrainToolFullFrameProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT,
		AUTO_ERROR_TRACE_0);
	MTI_UINT16 *inVisibleFrameBuffer;
	CToolFrameBuffer *inToolFB, *outToolFB;
	bool isImageDataReallyHalfs = false;

	const CImageFormat *imageFormat = GetSrcImageFormat(0);
	int componentCount = imageFormat->getComponentCountExcAlpha();
   int frameWidth = imageFormat->getTotalFrameWidth();
   int frameHeight = imageFormat->getTotalFrameHeight();
	if (grainParams.bReduce)
	{
//      DBTIMER(REDUCE_GRAIN);
		for (int threadIndex = 0; threadIndex < concurrentFramesCount; ++threadIndex)
		{
			if (threadIndex < framesToProcess)
			{
				long lLocalNFrames = 0;
				bool bFoundRepairFrame = false;
				for (int i = 0; i < grainParams.lNFrames; ++i)
				{
					inToolFB = GetRequestedInputFrame(0, i + threadIndex);

					// if frame is not available, do not include it in the filter
					retVal = inToolFB->GetErrorFlag();
					if (retVal)
               {
						continue;
					}

					isImageDataReallyHalfs = inToolFB->ContainsFloatData();

					inVisibleFrameBuffer = inToolFB->GetVisibleFrameBufferPtr();
					int frameIndex = inToolFB->GetFrameIndex();

					// if frame is outside wraparound region, do not add to list
					if (frameIndex < grainParams.lInWrap || frameIndex >= grainParams.lOutWrap)
               {
						continue;
               }

					// add input frame to the input list
					retVal = this->_grainReductionAlgorithm->setDataPointer(
                           lLocalNFrames,
						         inVisibleFrameBuffer,
                           frameIndex,
                           threadIndex);
					if (retVal != 0)
					{
						autoErr.errorCode = retVal;
						autoErr.msg << "Grain setDataPointer failed, Error = " << retVal;
						return retVal;
					}

					// is this the repair frame?
					if (i == grainParams.lPastFrames)
					{
						retVal = this->_grainReductionAlgorithm->setRepairFrame(
                              lLocalNFrames,
							         threadIndex);
						if (retVal != 0)
						{
							autoErr.errorCode = retVal;
							autoErr.msg << "Grain setRepairFrame failed, Error = " << retVal;
							return retVal;
						}

						bFoundRepairFrame = true;

						if (GetSystemAPI()->IsMaskAnimated())
						{
							// De-allocate grain algorithm (see NB below)
							this->_grainReductionAlgorithm->deallocate(threadIndex);

							lRet = GetSystemAPI()->GetMaskRoi(
                              frameIndex, // was inFrameIndex
							      	maskROI[threadIndex]);
							if (lRet != 0)
                     {
                        // QQQ Why no autoErr??
								return lRet;
                     }

							RECT roiRect = maskROI[threadIndex].getBlendBox();
							this->_grainReductionAlgorithm->setROI(
                              roiRect.left,
                              roiRect.top,
								      roiRect.right + 1,
                              roiRect.bottom + 1,
                              threadIndex);

							lRet = this->_grainReductionAlgorithm->setRegionOfInterestPtr(
                                    maskROI[threadIndex].getBlendPtr(),
								            maskROI[threadIndex].getLabelPtr(),
                                    threadIndex);
							if (lRet)
                     {
                        // QQQ Why no autoErr??
								return lRet;
                     }

							// Re-allocate grain algorithm
							// NB: this is probably horribly inefficent and probably
							// fragments memory.  Might be better to allocate
							// based on frame size rather than region-of-interest's
							// bounding box
							lRet = this->_grainReductionAlgorithm->allocate(threadIndex);
							if (lRet)
                     {
                        // QQQ Why no autoErr??
								return lRet;
                     }
						}
					}
#if 0
					char msg[200];
					sprintf(msg, "DoProc: index = %d", frameIndex);
					_MTIErrorDialog(Handle, msg);
#endif
					// increment the list size (or internal frame index)
					lLocalNFrames++;
				}

				if (lLocalNFrames == 0 || !bFoundRepairFrame)
				{
					autoErr.errorCode = -1;
					autoErr.msg << "Grain Processing failed, Error = " << retVal;
					return -1; // there's a problem!
				}

				// Note: the number of input frames doesn't affect allocate()
				// and may be variable (near the start or end of the clip adjacent
				// frames may not be available on one side).

				retVal = this->_grainReductionAlgorithm->setNFrame(lLocalNFrames, threadIndex);
				if (retVal != 0)
				{
					autoErr.errorCode = retVal;
					autoErr.msg << "Grain setNFrame failed, Error = " << retVal;
					return retVal;
				}

				// Copy the input buffer to the output buffer
				inToolFB = GetRequestedInputFrame(0, grainParams.lPastFrames + threadIndex);
				outToolFB = GetRequestedOutputFrame(0, threadIndex);
				CopyInputFrameBufferToOutputFrameBuffer(inToolFB, outToolFB);

				inVisibleFrameBuffer = outToolFB->GetVisibleFrameBufferPtr();
				retVal = this->_grainReductionAlgorithm->setResultPointer(inVisibleFrameBuffer,
					threadIndex);
				if (retVal != 0)
				{
					autoErr.errorCode = retVal;
					autoErr.msg << "Grain setResultPointer failed, Error = " << retVal;
					return retVal;
				}

				// Use Region of Interest blend mask to determine which pixels to
				// put in the original values pixel region list.
				// NB: We probably could be more efficient for single rects, but
				// this will do the trick for now
				CPixelRegionList* originalValues = outToolFB->GetOriginalValuesPixelRegionListPtr();
				originalValues->Add(maskROI[threadIndex].getBlendBox(),
                                maskROI[threadIndex].getBlendPtr(),
					                 NULL,
                                frameWidth,
                                frameHeight,
					                 componentCount,
                                0xff);
			}
			else
			{
				// tell this thread that it doesn't have anything to process
				retVal = this->_grainReductionAlgorithm->setResultPointer(NULL, threadIndex);
				if (retVal != 0)
				{
					autoErr.errorCode = retVal;
					autoErr.msg << "Grain setResultPointer failed, Error = " << retVal;
					return retVal;
				}
			}
		}

		retVal = (int) this->_grainReductionAlgorithm->doGrainFilter();
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "Grain Processing failed, Error = " << retVal;
			return retVal;
		}
	}

	if (grainParams.bGenerate)
	{
//      DBTIMER(GENERATE_GRAIN);

		// Temp code until this can be figured out
		MTI_UINT16 componentValues[3];
		imageFormat->getComponentValueMax(componentValues);
		int maxValue = nextPow2Minus1(componentValues[0]);

		for (int i = 0; i < framesToProcess; ++i)
		{
			this->HRT.Start();
//         DBTIMER(GENERATE_GRAIN_FOR_ONE_FRAME);
//         DBTRACE(i);
			outToolFB = GetRequestedOutputFrame(0, i);

			if (grainParams.bReduce)
			{
            // The input to generate is the output from the Reduce phase!
				inVisibleFrameBuffer = outToolFB->GetVisibleFrameBufferPtr();
			}
			else
			{
//            DBTIMER(COPY_INPUT_TO_OUTPUT);
            // For sanity, copy the input bits to the output buffer, but use
            // the input buffer as the source for grain generation.
				inToolFB = GetRequestedInputFrame(0, grainParams.lPastFrames + i);
				CopyInputFrameBufferToOutputFrameBuffer(inToolFB, outToolFB);
				inVisibleFrameBuffer = inToolFB->GetVisibleFrameBufferPtr();
				isImageDataReallyHalfs = inToolFB->ContainsFloatData();
			}

			auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();
			auto outVisibleFrameBuffer = outToolFB->GetVisibleFrameBufferPtr();

			int outFrameIndex = outToolFB->GetFrameIndex();
			bool needNewSeed = this->lCurrentFrameIndex != outFrameIndex;
			this->lCurrentFrameIndex = outFrameIndex;
			if (GetSystemAPI()->IsMaskAvailable())
			{
//            DBTIMER(GRAIN_GENERATOR_APPLY_GRAIN_USING_REGULAR_MASK);
            // Compute the full-frame image with grain added
  				const std::lock_guard<std::mutex> lock(g_i_mutex);
				MTI_UINT16 *grainImage = grainGenerator->ApplyGrain(
                                        inVisibleFrameBuffer,
                                        maxValue,
                                        GetChannelMask(),
                                        true,         // Source has changed
													 needNewSeed,  // If frame has changed
													 !isImageDataReallyHalfs);

            // Check the mask brush
            if (this->_grainGeneratorModel->GetUseBrushMask())
            {
//               DBTIMER(ALPHA_BLEND);

               // Replace the full-frame grain-added image with one that alters
               // only the brush-stroked pixels.
               this->_grainGeneratorModel->GetBrushRevealer()->AlphaBlendOver(
                     outVisibleFrameBuffer,
                     grainImage,
                     grainImage);
            }

            { // DBTIMER(MORE_BLENDING);
				CRegionOfInterest maskROI;
				int frameIndex = outToolFB->GetFrameIndex();
				GetSystemAPI()->GetMaskRoi(frameIndex, maskROI);
				auto box = maskROI.getBlendBox();
				auto blendPtr = maskROI.getBlendPtr();
				MTI_UINT16 *inPtr;
				MTI_UINT16 *outPtr;
				MTI_UINT8 *blend;
				int width = grainGenerator->GetImageSize().width;

				for (int r = box.top; r < box.bottom; r++)
				{
					inPtr = grainImage + r * width * componentCount;
					outPtr = outToolFB->GetVisibleFrameBufferPtr() + r * width * componentCount;
					blend = (MTI_UINT8*)blendPtr + r * width;
					for (int c = box.left; c < box.right; c++)
					{
						// Needs to be fixed for alpha
						float f = blend[c] / 255.0;
						auto firstComponentIndex = c * componentCount;
						for (auto i = firstComponentIndex; i < (firstComponentIndex + componentCount); ++i)
						{
							outPtr[i] = (1 - f) * outPtr[i] + f * inPtr[i];
						}
					}
				}
            }
			}
			else if (this->_grainGeneratorModel->GetUseBrushMask())
         {
//            DBTIMER(GRAIN_GENERATOR_APPLY_GRAIN_USING_BRUSH_MASK);
            // Compute the full-frame image with grain added
            const std::lock_guard<std::mutex> lock(g_i_mutex);
				MTI_UINT16 *grainImage = grainGenerator->ApplyGrain(
                                        inVisibleFrameBuffer,
                                        maxValue,
                                        GetChannelMask(),
                                        true,         // Source has changed
													 needNewSeed,  // If frame has changed
													 !isImageDataReallyHalfs);

            // Replace the full-frame grain-added image with one that alters
            // only the brush-stroked pixels.
            {
//            DBTIMER(ALPHA_BLEND);
            this->_grainGeneratorModel->GetBrushRevealer()->AlphaBlendOver(
                  outVisibleFrameBuffer,
                  grainImage,
                  outToolFB->GetVisibleFrameBufferPtr());
            }
         }
         else
         {
//            // No mask or brush strokes - no need for an intermediary grain image.
//            grainGenerator->ApplyGrain(
//                  inVisibleFrameBuffer,
//                  outToolFB->GetVisibleFrameBufferPtr(),
//                  maxValue,
//                  needNewSeed); // if frame has changed

//            DBTIMER(GRAIN_GENERATOR_APPLY_GRAIN_WITHOUT_MASK);
  				const std::lock_guard<std::mutex> lock(g_i_mutex);

				MTI_UINT16 *grainImage = grainGenerator->ApplyGrain(
                                        inVisibleFrameBuffer,
                                        maxValue,
                                        GetChannelMask(),
                                        true,         // Source has changed
													 needNewSeed,  // If frame has changed
													 !isImageDataReallyHalfs);

// ApplyGrain() now clamps.
//            auto out = outToolFB->GetVisibleFrameBufferPtr();
//            auto in = grainImage;
//            auto n = grainGenerator->GetImageSizeInComponents();
//            for (auto i = 0; i < n; i++)
//            {
//               auto v = *in++;
//               *out++ = v > maxValue ? maxValue : v;
//            }

            {
//            DBTIMER(MEMCPY);
            MTImemcpy(
                  outToolFB->GetVisibleFrameBufferPtr(),
                  grainImage,
                  sizeof(MTI_UINT16)*grainGenerator->GetImageSizeInComponents());
			   }
         }

			GGrainTool->GrainTime = this->HRT.ReadAsString();

         // If we reduced grain, then the original values region
         // has already been added, above.
			if (!grainParams.bReduce)
			{
//            DBTIMER(ORIGINAL_VALUES);
            auto originalValues = outToolFB->GetOriginalValuesPixelRegionListPtr();
            CRegionOfInterest maskROI;
            int frameIndex = outToolFB->GetFrameIndex();
            GetSystemAPI()->GetMaskRoi(frameIndex, maskROI);

            if (this->_grainGeneratorModel->GetUseBrushMask())
            {
               // Save original values within the intersection of the mask's
               // and the brush strokes' bounding rectangles.
               // QQQ should really save the actual pixels as a region
               RECT brushBox = this->_grainGeneratorModel->GetBrushRevealer()->GetMaskBoundingBox();
               RECT maskBox = maskROI.getBlendBox();
               RECT minBoundingRectangle {
                   std::max<int>(brushBox.left,   maskBox.left),
                   std::max<int>(brushBox.top,    maskBox.top),
                   std::min<int>(brushBox.right,  maskBox.right),
                   std::min<int>(brushBox.bottom, maskBox.bottom)
               };

               originalValues->Add(
                     minBoundingRectangle,
                     inVisibleFrameBuffer,
                     frameWidth,
                     frameHeight,
                     componentCount);
            }
            else
            {
               originalValues->Add(
                     maskROI.getBlendBox(),
                     maskROI.getBlendPtr(),
                     NULL,
                     frameWidth,
                     frameHeight,
                     componentCount,
                     0xff);
            }
			}
		}
	}

#if 0
	char msg[200];
	sprintf(msg, "DoProc: Success");
	_MTIErrorDialog(Handle, msg);
#endif

	return 0;
} /* DoProcess() */
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::EndIteration(SToolProcessingData &procData)
{
   SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);

   framesRemaining -= framesToProcess;
   framesAlreadyProcessed += framesToProcess;

   GetToolProgressMonitor()->BumpProgress(framesToProcess);

   return 0;
}
//---------------------------------------------------------------------------

int CGrainToolFullFrameProc::EndProcessing(SToolProcessingData &procData)
{
   // we're done processing
   if (framesRemaining == 0)
   {
	  GetToolProgressMonitor()->SetStatusMessage("Processing complete");
	  GetToolProgressMonitor()->StopProgress(true);
   }
   else
   {
	  GetToolProgressMonitor()->SetStatusMessage("Stopped");
	  GetToolProgressMonitor()->StopProgress(false);
   }

   return 0;
}
//---------------------------------------------------------------------------

