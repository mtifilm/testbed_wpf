//---------------------------------------------------------------------------

#ifndef GrainToolFullFrameProcH
#define GrainToolFullFrameProcH
//---------------------------------------------------------------------------

#include "IniFile.h"
#include "GrainCoreCommon.h"
#include "GrainReduction_MT.h"
#include "GrainTool.h"   // NO!!!
#include "ToolObject.h"
//---------------------------------------------------------------------------

class CGrainToolFullFrameProc : public CToolProcessor
{
public:
   CGrainToolFullFrameProc(
      int newToolNumber,
      const string &newToolName,
      CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr,
      IToolProgressMonitor *newToolProgressMonitor,
      GrainGeneratorModel *grainGeneratorModel);

   virtual ~CGrainToolFullFrameProc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   CHRTimer HRT;
private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int inFrameIndex;
   int outFrameIndex;
   int iterationCount;
   // algorithm
   CGrainReduction_MT *_grainReductionAlgorithm;

   //
   GrainGeneratorModel *_grainGeneratorModel;

   // parameters
   CGrainToolParameters grainParams;

   // This is used by grain generation to decide if a new randomization needs to be done
   long	lCurrentFrameIndex;

   // variables to support multithreading of algorithm

//*** WHY ARE THESE GLOBAL?
   int concurrentFramesCount;
   int maxInputFrames;      // Maximum number of input frames per iteration
   int maxOutputFrames;     // Maximum number of output frames per iteration
//***
   int framesRemaining;     // Number of remaining output frames to be processed
   int framesAlreadyProcessed; // Number of output frames already processed
   int framesToProcess;     // Number of frames to process on current iteration

   // Mask Region
   CRegionOfInterest maskROI[GRN_MAX_CONCURRENT_FRAMES_COUNT];

};
//---------------------------------------------------------------------------

#endif
