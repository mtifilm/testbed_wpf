#include "GrainPresets.h"

#include "GrainCoreCommon.h"
#include "IniFile.h"
#include "JobManager.h"
//---------------------------------------------------------------------------

GrainPresets::GrainPresets()
{
	SET_CBHOOK(HandleGrainPresetsChange, GetGrainGeneratorModel()->PresetChange);
	SET_CBHOOK(HandleGrainValuesChange, GetGrainGeneratorModel()->GrainChange);
}
//---------------------------------------------------------------------------

GrainPresets::~GrainPresets()
{
	REMOVE_CBHOOK(HandleGrainPresetsChange, GetGrainGeneratorModel()->PresetChange);
	REMOVE_CBHOOK(HandleGrainValuesChange, GetGrainGeneratorModel()->GrainChange);
}
//---------------------------------------------------------------------------

void GrainPresets::SetEnabled(bool flag)
{
   _isEnabled = flag;
}
//---------------------------------------------------------------------------

bool GrainPresets::IsEnabled()
{
   return _isEnabled;
}
//---------------------------------------------------------------------------

void GrainPresets::SetSelectedBase(GrainParameterBaseType presetsBase)
{
	GetGrainGeneratorModel()->SetGrainParametersBase(presetsBase);
	GetGrainGeneratorModel()->SetGrainParametesFromBaseAndPreset();
}
//---------------------------------------------------------------------------

GrainParameterBaseType GrainPresets::GetSelectedBase()
{
   return GetGrainGeneratorModel()->GetGrainParametersBase();
}
//---------------------------------------------------------------------------

void GrainPresets::SetSelectedIndex(int presetIndex)
{
   GetGrainGeneratorModel()->SetPreset(presetIndex);
	GetGrainGeneratorModel()->SetGrainParametesFromBaseAndPreset();
}
//---------------------------------------------------------------------------

int GrainPresets::GetSelectedIndex()
{
   return GetGrainGeneratorModel()->GetPreset();
}
//---------------------------------------------------------------------------

void GrainPresets::SetClipData(const string &clipName, int frameWidth, int frameHeight, int bitsPerComponent, bool dataIsUnsignedShorts)
{
   if (_currentClipName == clipName)
   {
      return;
   }

   _currentClipName = clipName;

   if (frameWidth < 0 || frameHeight < 0 || bitsPerComponent < 0)
   {
      _currentClipName = "";
      return;
   }

	// This function should be called from the tool's onNewClip() callback.
   _bitsPerComponent = bitsPerComponent;
	_seedBase = 0;
	_dataIsUnsignedShorts = dataIsUnsignedShorts;
   for (auto i = 0; i < clipName.size(); ++i)
   {
      _seedBase += (unsigned int)clipName[i];
   }


   GetGrainGeneratorModel()->SetImageSize(frameWidth, frameHeight);

   // Need to call this to force loading of clip and project presets!
	string GrainIniFileName = CPMPIniFileName("Grain");
   JobManager jobManager;
   std::string jobRootFolder = jobManager.GetJobRootFolderPath();
   if (jobRootFolder == "")
   {
	  jobRootFolder = jobManager.GetMasterClipFolderPath();
   }

   GetGrainGeneratorModel()->SetMasterClipFolderPath(jobManager.GetMasterClipFolderPath());
   GetGrainGeneratorModel()->SetProjectRootFolderPath(jobRootFolder);

   // NOTE: the call has the side effect of also loading the clip and project presets!
   GetGrainGeneratorModel()->LoadFromGrainIni(GrainIniFileName);

   _currentFrameNumber = -1;
}
//---------------------------------------------------------------------------

void GrainPresets::SetFrameData(const unsigned short* imageData, int frameNumber)
{
   if (_currentClipName.empty() || frameNumber < 0 ||  _bitsPerComponent < 0)
   {
      frameNumber = -1;
      return;
   }

   if (frameNumber == _currentFrameNumber)
   {
      return;
   }

   _currentFrameNumber = frameNumber;

   // Tell the GrainGenerator about the new source data.
   MTI_UINT16 maxDataValue = (MTI_UINT16)(((1U << (_bitsPerComponent - 1)) - 1) & 0xFFFFU);

   try
   {
         GetGrainGeneratorModel()->GetGrainGenerator()->SetSourceImage(imageData, maxDataValue, _dataIsUnsignedShorts);
   }
   catch (IppException ex)
   {
      frameNumber = -1;
      string wtf = ex.GetErrorMessage();
      TRACE_0(errout << "ERROR: Unable to set source image in GrainPresets::SetFrameData()");
      TRACE_0(errout << "       Reason: " << wtf);
      return;
   }

   try
   {
      GetGrainGeneratorModel()->GetGrainGenerator()->SetSeed(_seedBase + frameNumber);
   }
   catch (IppException ex)
   {
      frameNumber = -1;
      string wtf = ex.GetErrorMessage();
      TRACE_0(errout << "ERROR: Unable to set seed in GrainPresets::SetFrameData()");
      TRACE_0(errout << "       Reason: " << wtf);
      return;
   }
}
//---------------------------------------------------------------------------

void GrainPresets::HandleGrainPresetsChange(void *sender)
{
   GrainPresetsStateChange.Notify();
}
//---------------------------------------------------------------------------

void GrainPresets::HandleGrainValuesChange(void *sender)
{
   PresetGrainValuesChange.Notify();
}
//---------------------------------------------------------------------------

const float *GrainPresets::GetNormalizedGrainValues(const unsigned short* imageData, int frameNumber)
{
   SetFrameData(imageData, frameNumber);

   if (_currentClipName.empty() || _bitsPerComponent == 0 || _currentFrameNumber < 0)
   {
      return nullptr;
   }

   const float *grainValues;
   try
   {
      grainValues = GetGrainGeneratorModel()->GetGrainGenerator()->GetNormalizedGrainValues();
   }
   catch (IppException ex)
   {
      string wtf = ex.GetErrorMessage();
      TRACE_0(errout << "ERROR: Unable to get grain values in GrainPresets::GetNormalizedGrainValues()");
      TRACE_0(errout << "       Reason: " << wtf);
      return nullptr;
   }

   return grainValues;
}
//---------------------------------------------------------------------------

GrainGeneratorModel *GrainPresets::GetGrainGeneratorModel()
{
   return &(GrainGeneratorModel::GetGlobalInstanceRef());
}
//---------------------------------------------------------------------------

