// GrainTool.cpp: implementation of the CGrainTool class.

#include "GrainTool.h"
#include "GrainToolGUIWin.h"
#include "GrainToolFullFrameProc.h"
#include "GrainToolBrushStrokeProc.h"

#include "ClipAPI.h"
#include "ImageFormat3.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIsleep.h"
#include "PDL.h"
#include "PixelRegions.h"
#include "SysInfo.h"
#include "ThreadControlUnit.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 GrainToolNumber = GRAIN_TOOL_NUMBER;

// -------------------------------------------------------------------------
// GrainTool Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem GrainToolDefaultConfigItems[] =
{
   // GrainTool Tool Command             Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
 { GRN_CMD_HANDLE_D_KEY_DOWN,                " D                KeyDown    " },
 { GRN_CMD_HANDLE_D_KEY_UP,                  " D                KeyUp      " },
 { EXEC_BUTTON_PREVIEW_NEXT_FRAME,           " Ctrl+D           KeyDown    " },
 { EXEC_BUTTON_PREVIEW_ALL,                  " Shift+D          KeyDown    " },
 { EXEC_BUTTON_RENDER_FRAME,                 " G                KeyDown    " },
 { EXEC_BUTTON_RENDER_NEXT_FRAME,            " Ctrl+G           KeyDown    " },
 { EXEC_BUTTON_RENDER_ALL,                   " Shift+G          KeyDown    " },
 { EXEC_BUTTON_STOP,                         " Ctrl+Space       KeyDown    " },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              " Space            KeyDown    " },
 { EXEC_BUTTON_SET_RESUME_TC,                " Return           KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL,                  " ,                KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       " Shift+,          KeyDown    " },

 { GRN_CMD_HANDLE_A_KEY_DOWN,                " A                KeyDown    " },
 { GRN_CMD_CLEAR_PENDING_STROKES,            " Shift+A          KeyDown    " },
 { GRN_CMD_CYCLE_PARAMETERS_NEXT,            " 1                KeyDown    " },
 { GRN_CMD_CYCLE_PARAMETERS_PREV,            " Shift+1          KeyDown    " },
 { GRN_CMD_TOGGLE_CONTRAST_GANG,             " 2                KeyDown    " },
 { GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT,     " 3                KeyDown    " },
 { GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV,     " Shift+3          KeyDown    " },

 { GRN_CMD_TOGGLE_FIX,                       " T                KeyDown    " },
 { GRN_CMD_THREAD_CONTROL,                   " Ctrl+Alt+F12     KeyDown    " },
 { GRN_CMD_CREATE_SAVE_PRESETS,              " Ctrl+S		       KeyDown    " },

 { GRN_CMD_START_BRUSHING,                   " LButton          ButtonDown " },
 { GRN_CMD_STOP_BRUSHING,                    " LButton          ButtonUp   " },
 { GRN_CMD_START_ERASING,                    " RButton          ButtonDown " },
 { GRN_CMD_STOP_ERASING,                     " RButton          ButtonUp   " },

 { GRN_CMD_REPEAT_BRUSH_STROKES,             " `                KeyDown    " },

 { GRN_CMD_SELECT_BRUSH_OPACITY,             " 0                KeyDown   " },
 { GRN_CMD_SELECT_BRUSH_BLEND,               " 8                KeyDown   " },
 { GRN_CMD_SELECT_BRUSH_ASPECT,              " -                KeyDown   " },
 { GRN_CMD_SELECT_BRUSH_ANGLE,               " =                KeyDown   " },

 { GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT,      " 4                KeyDown   " },
 { GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV,      " Shift+4          KeyDown   " },

};

static CUserInputConfiguration *GrainToolDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(GrainToolDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               GrainToolDefaultConfigItems);

// -------------------------------------------------------------------------
// GrainTool Tool Command Table

static CToolCommandTable::STableEntry GrainToolCommandTableEntries[] =
{
   // GrainTool Tool Command            GrainTool Tool Command String
   // -------------------------------------------------------------

 { GRN_CMD_HANDLE_D_KEY_DOWN,                "GRN_CMD_HANDLE_D_KEY_DOWN" },
 { GRN_CMD_HANDLE_D_KEY_UP,                  "GRN_CMD_HANDLE_D_KEY_UP" },
 { EXEC_BUTTON_PREVIEW_FRAME,                "EXEC_BUTTON_PREVIEW_FRAME" },
 { EXEC_BUTTON_PREVIEW_ALL,                  "EXEC_BUTTON_PREVIEW_ALL" },
 { EXEC_BUTTON_RENDER_FRAME,                 "EXEC_BUTTON_RENDER_FRAME" },
 { EXEC_BUTTON_RENDER_ALL,                   "EXEC_BUTTON_RENDER_ALL" },
 { EXEC_BUTTON_STOP,                         "EXEC_BUTTON_STOP" },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              "EXEC_BUTTON_PAUSE_OR_RESUME" },
 { EXEC_BUTTON_SET_RESUME_TC,                "EXEC_BUTTON_SET_RESUME_TC" },
 { EXEC_BUTTON_CAPTURE_PDL,                  "EXEC_BUTTON_CAPTURE_PDL" },
 { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       "EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS" },

 { GRN_CMD_CYCLE_PARAMETERS_NEXT,            "GRN_CMD_CYCLE_PARAMETERS_NEXT" },
 { GRN_CMD_CYCLE_PARAMETERS_PREV,            "GRN_CMD_CYCLE_PARAMETERS_PREV" },
 { GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT,     "GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT" },
 { GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV,     "GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV" },

 { GRN_CMD_HANDLE_A_KEY_DOWN,                "GRN_CMD_HANDLE_A_KEY_DOWN" },
 { GRN_CMD_UNDO_ONE_PENDING_STROKE,          "GRN_CMD_UNDO_ONE_PENDING_STROKE" },
 { GRN_CMD_CLEAR_PENDING_STROKES,            "GRN_CMD_CLEAR_PENDING_STROKES" },
 { GRN_CMD_TOGGLE_FIX,                       "GRN_CMD_TOGGLE_FIX" },
 { GRN_CMD_THREAD_CONTROL,                   "GRN_CMD_THREAD_CONTROL" },

 { GRN_CMD_START_BRUSHING,                   "GRN_CMD_START_BRUSHING" },
 { GRN_CMD_STOP_BRUSHING,                    "GRN_CMD_STOP_BRUSHING" },
 { GRN_CMD_START_ERASING,                    "GRN_CMD_START_ERASING" },
 { GRN_CMD_STOP_ERASING,                     "GRN_CMD_STOP_ERASING" },
 { GRN_CMD_REPEAT_BRUSH_STROKES,             "GRN_CMD_REPEAT_BRUSH_STROKES" },

 { GRN_CMD_SELECT_BRUSH_OPACITY,             "GRN_CMD_SELECT_BRUSH_OPACITY" },
 { GRN_CMD_SELECT_BRUSH_BLEND,               "GRN_CMD_SELECT_BRUSH_BLEND" },
 { GRN_CMD_SELECT_BRUSH_ASPECT,              "GRN_CMD_SELECT_BRUSH_ASPECT" },
 { GRN_CMD_SELECT_BRUSH_ANGLE,               "GRN_CMD_SELECT_BRUSH_ANGLE" },

 { GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT,      "GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT" },
 { GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV,      "GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV" },

};

static CToolCommandTable *GrainToolCommandTable
   = new CToolCommandTable(sizeof(GrainToolCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           GrainToolCommandTableEntries);


//////////////////////////////////////////////////////////////////////
// CGrainTool:
//////////////////////////////////////////////////////////////////////

CGrainTool::CGrainTool(const string &newToolName,
					   MTI_UINT32 **newFeatureTable)
:
   CToolObject(newToolName, newFeatureTable),
   _toolSetupHandle(-1),
   _trainingToolSetupHandle(-1),
   _brushStrokeToolSetupHandle(-1),
   concurrentFrameCount(1),
   oldConcurrentFrameCount(-1),
   processingThreadsPerFrame(1),
   oldProcessingThreadsPerFrame(-1),
   doRowSlices(true),
   oldDoRowSlices(-1),
   slicesPerThread(1),
   oldSlicesPerThread(-1),
   noopProcessing(false),
   oldNoopProcessing(-1),
   _brushState(BRUSH_IS_INVISIBLE),
   _brushCenterX(-1),
   _brushCenterY(-1),
   _needToRunTheStrokeProcessor(false),
   _toolProcessorTypeToMake(FULL_FRAME_TOOL_PROCESSOR_TYPE),
   _needToAcceptBrushStrokes(false),
   _lastStrokedFrameIndex(-1),
   _previousBrushStrokeX(-1),
   _previousBrushStrokeY(-1),
   _multiFrameStrokingInProgress(false),
	_clipChangedWhileWeWereSleeping(true),
	_grainGeneratorModel(&GrainGeneratorModel::GetGlobalInstanceRef())
{
	IniFileName = CPMPIniFileName("Grain");
	grainParams.ReadSettings(IniFileName);
	this->_grainGeneratorModel->LoadFromGrainIni(IniFileName);

   this->_brush = new CFloatBrush();
}

CGrainTool::~CGrainTool()
{
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the GrainTool Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CGrainTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CGrainTool's base class, i.e., CToolObject
   // This must be done before the CGrainTool initializes itself
   retVal = initBaseToolObject(GrainToolNumber, newSystemAPI,
                               GrainToolCommandTable,
                               GrainToolDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in GrainTool Tool, initBaseToolObject failed "
                     << retVal);
      return retVal;
      }

   // Tool-specific initialization here

   // Create the Tool's GUI
   CreateGrainToolGUI();

   // Update the tool's GUI to match the tool processing initial state
   UpdateExecutionGUI(GetToolProcessingControlState(), false);

   return 0;

}   // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CGrainTool::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   this->_lastStrokedFrameIndex = -1;
   if (this->_brushState != BRUSH_IS_INVISIBLE)
   {
      SetBrushActive(true);
   }

   // Make sure displayed frame is clean
   getSystemAPI()->refreshFrameCached();

   JobManager jobManager;
   jobManager.SetCurrentToolCode("gn");

	getSystemAPI()->SetToolNameForUserGuide("GrainTool");

   if (this->_clipChangedWhileWeWereSleeping)
   {
	  _clipChangedWhileWeWereSleeping = false;
	  onNewClip();
   }

   this->_brushStrokeStash.clear();
	this->BrushStrokesChange.Notify();

	// Tell the grain generator it needs to generate its own seeds.
	_grainGeneratorModel->GetGrainGenerator()->UnsetSeed();

   // Enable mask tool
   getSystemAPI()->SetMaskAllowed(true);

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CGrainTool::toolDeactivate()
{
   TryToResolveProvisional();
   DestroyAllToolSetups(true);

   GetToolProgressMonitor()->SetIdle();
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   getSystemAPI()->showCursor(true);

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              GrainTool Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CGrainTool::toolShutdown()
{
   int retVal;

   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending
	grainParams.WriteSettings(IniFileName);

   // Destroy GrainTool's GUI
   DestroyGrainToolGUI();

	GGrainTool = NULL; // Say we are destroyed

   // Shutdown the GrainTool Tool's base class, i.e., CToolObject.
   // This must be done after the GrainTool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

	return retVal;
}

// ===================================================================
//
// Function:    DoesToolPreserveHistory
//
// Description: Called by tool infrastructure to see if it should
//              save frame changes in the history. This is a bit
//              problematic because we want to save history when brushing
//              but not when processing full frames!! I don't actually
//              know when and how often it's called...
//
// Arguments:   None
//
// Returns:     true if the brush is active, else false
//
// ===================================================================
bool CGrainTool::DoesToolPreserveHistory()
{
   return this->_brushState != BRUSH_IS_INVISIBLE /* || getSystemAPI()->IsMaskAvailable() */;
}

// ===================================================================
//
// Function:    onHeartbeat
//
// Description: Called periodically at frame refresh rate
//
// Arguments:
//
// Returns:     TOOL_RETURN_CODE_NO_ACTION (We never want to lock out
//              any other tools
//
// ===================================================================

int CGrainTool::onHeartbeat()
{
   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (this->_needToRunTheStrokeProcessor)
   {
      _needToRunTheStrokeProcessor = false;
      StartToolProcessing(TOOL_PROCESSING_CMD_RENDER_1, false);
   }
   else if (this->_needToAcceptBrushStrokes)
   {
      this->_needToAcceptBrushStrokes = false;
      AcceptBrushStrokes(false);
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

bool CGrainTool::AutoAcceptBrushStrokes()
{
   bool retVal = true;
   if (!this->AreBrushStrokesPending())
   {
      return true;
   }

   if (this->_autoAcceptingBrushStrokes || this->_needToAcceptBrushStrokes)
   {
      this->AcceptBrushStrokes(false);
   }
   else
   {
      //////////////////////// SIDE EFFECT ALERT ///////////////////////////
      // If the mouse button is down, we're gonna get mighty confused
      // because we'll never see the buttonUp event, so kill the brush
      // operation before popping up the dialog.
      if (this->_brushState == BRUSH_IS_PAINTING || this->_brushState == BRUSH_IS_ERASING)
      {
         this->_brushState = BRUSH_IS_IDLE;
      }
      //////////////////////// SIDE EFFECT ALERT ///////////////////////////

      switch (getSystemAPI()->ShowAcceptOrRejectDialog(&this->_autoAcceptingBrushStrokes))
      {
         case PENDING_CHANGE_ACCEPT:
            AcceptBrushStrokes(false);
            break;
         case PENDING_CHANGE_REJECT:
            RejectBrushStrokes(false);
            break;
         case PENDING_CHANGE_NO_ACTION:
         default:
            retVal = false;  // Wasn't resolved
            break;
      }

      if (this->_autoAcceptingBrushStrokes)
      {
         // User elected to turn on Auto-Accept, so tell the GUI to check the box.
         ToggleAutoAccept();
      }
   }

   return retVal;
}

void CGrainTool::AcceptBrushStrokes(bool goToFrame)
{
   getSystemAPI()->WaitForToolProcessing();
   getSystemAPI()->AcceptProvisional(goToFrame);
   this->GetToolProgressMonitor()->SetStatusMessage("Strokes accepted");

   this->_brushStrokeStash.clear();
   this->GetGrainGeneratorModel()->GetCopyOfBrushStrokes(this->_brushStrokeStash);
   this->GetGrainGeneratorModel()->ClearBrushStrokes();
   this->BrushStrokesChange.Notify();
}

void CGrainTool::RejectBrushStrokes(bool goToFrame)
{
   getSystemAPI()->WaitForToolProcessing();
   getSystemAPI()->RejectProvisional(goToFrame);
   this->GetToolProgressMonitor()->SetStatusMessage("Strokes rejected");

   this->GetGrainGeneratorModel()->ClearBrushStrokes();
	getSystemAPI()->refreshFrameCached();
   this->BrushStrokesChange.Notify();
}

void CGrainTool::SetUpMultiFrameStroking()
{
   getSystemAPI()->WaitForToolProcessing();
   getSystemAPI()->RejectProvisional(false);
   this->GetToolProgressMonitor()->SetStatusMessage("Applying strokes to all frames in marked range");
   this->GetGrainGeneratorModel()->GetCopyOfBrushStrokes(this->_brushStrokeStash);
}

bool CGrainTool::AreBrushStrokesPending()
{
   return this->GetGrainGeneratorModel()->GetStrokeCount() > 0;
}

bool CGrainTool::RepeatBrushStrokes()
{
   getSystemAPI()->WaitForToolProcessing();

   if (this->_brushState == BRUSH_IS_PAINTING || this->_brushState == BRUSH_IS_ERASING)
   {
      return false;
   }

   if (!AutoAcceptBrushStrokes())
   {
      return false;
   }

   if (this->_brushStrokeStash.empty())
   {
      return true;
   }

   this->GetGrainGeneratorModel()->SetBrushStrokes(this->_brushStrokeStash);
   _needToRunTheStrokeProcessor = true;
   this->BrushStrokesChange.Notify();

   return true;
}

bool CGrainTool::AreThereRepeatableStrokes()
{
   return !this->_brushStrokeStash.empty() || AreBrushStrokesPending();
}

void CGrainTool::CenterBrushOnFrame()
{
   // Only do this if the mouse is NOT in the navigator!
   if (getSystemAPI()->isMouseInFrame())
   {
      return;
   }

   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == NULL)
   {
      return;
   }

   this->_brushCenterX = imageFormat->getTotalFrameWidth() / 2;
   this->_brushCenterY = imageFormat->getTotalFrameHeight() / 2;

   getSystemAPI()->forceMouseToWindowCenter();
   getSystemAPI()->refreshFrameCached();
}

void CGrainTool::UndoLastBrushStroke()
{
   if (!this->AreBrushStrokesPending())
   {
      return;
   }

   // If exactly ONE brush stroke is pending, do a REJECT, not an UNDO!
   if (this->GetGrainGeneratorModel()->GetStrokeCount() == 1)
   {
      this->RejectBrushStrokes(true);
      return;
   }

   getSystemAPI()->WaitForToolProcessing();

   // If we had moved off the frame with the brush strokes, move back now.
   int frameIndex = getSystemAPI()->GetProvisionalFrameIndex();
   if (frameIndex != getSystemAPI()->getLastFrameIndex())
   {
      getSystemAPI()->goToFrameSynchronous(frameIndex);
   }

   if (this->GetGrainGeneratorModel()->UndoLastBrushStroke())
   {
      _needToRunTheStrokeProcessor = true;
      this->BrushStrokesChange.Notify();
   }
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: GrainTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CGrainTool::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   int commandNumber = toolCommand.getCommandNumber();
   int Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

   // Disambiguate the overloaded D key
   if (commandNumber == GRN_CMD_HANDLE_D_KEY_DOWN)
   {
      if (this->_brushState != BRUSH_IS_INVISIBLE)
      {
// Larry says to nuke this because sometimes we lose the KeyUp event in
// RELEASE mode so the brush stays hidden (seems to work fine in DEBUG mode!)
         // HACK - make brush disappear
         this->_hideBrushTemporarily = true;
         getSystemAPI()->enableBrushDisplay(false);
         getSystemAPI()->refreshFrameCached();
      }

      commandNumber = (this->_brushState == BRUSH_IS_INVISIBLE)
                      ? EXEC_BUTTON_PREVIEW_FRAME
                      : GRN_CMD_SELECT_BRUSH_RADIUS;
   }
   else if (commandNumber == GRN_CMD_HANDLE_D_KEY_UP)
   {
      if (this->_brushState != BRUSH_IS_INVISIBLE)
      {
// See above
         this->_hideBrushTemporarily = false;
         getSystemAPI()->enableBrushDisplay(true);
         getSystemAPI()->refreshFrameCached();
      }

      return Result;
   }

   // Disambiguate the overloaded A key
   if (commandNumber == GRN_CMD_HANDLE_A_KEY_DOWN)
   {
      commandNumber = (this->_brushState == BRUSH_IS_INVISIBLE)
                      ? GRN_CMD_CLEAR_FULL_FRAME_PREVIEW
                      : GRN_CMD_UNDO_ONE_PENDING_STROKE;
   }

   const char *commandName = getCommandTable()->findCommandName(commandNumber);
   TRACE_2(errout << "Command: " << ((commandName != NULL) ? commandName : "GRN_UNKNOWN")
                                 << " (" << commandNumber << ")");

   // Dispatch loop for Grain Tool commands
   switch (commandNumber) {

      case GRN_CMD_SELECT_BRUSH_RADIUS:
      case GRN_CMD_SELECT_BRUSH_OPACITY:
      case GRN_CMD_SELECT_BRUSH_BLEND:
      case GRN_CMD_SELECT_BRUSH_ASPECT:
      case GRN_CMD_SELECT_BRUSH_ANGLE:
         FocusOnBrushParameterTrackEdit(commandNumber);
         break;

      case EXEC_BUTTON_PREVIEW_FRAME:
      case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
      case EXEC_BUTTON_PREVIEW_ALL:
      case EXEC_BUTTON_RENDER_FRAME:
      case EXEC_BUTTON_RENDER_NEXT_FRAME:
      case EXEC_BUTTON_RENDER_ALL:
      case EXEC_BUTTON_STOP:
      case EXEC_BUTTON_PAUSE_OR_RESUME:
      case EXEC_BUTTON_SET_RESUME_TC:
      case EXEC_BUTTON_CAPTURE_PDL:
      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      {
         // Ignore all exec keys except G and Shift-G while in brush mode
         if (this->_brushState != BRUSH_IS_INVISIBLE
         && commandNumber != EXEC_BUTTON_RENDER_ALL)
         {
            // Ignore G key pressed while still painting!
            if (commandNumber == EXEC_BUTTON_RENDER_FRAME
            && this->_brushState == BRUSH_IS_IDLE)
            {
               // Manually selecting brush strokes
               AcceptBrushStrokes(true);
            }

            // Ignore all other keys!
         }
         else if (!RunExecButtonsCommand((EExecButtonId) commandNumber))
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }

         break;
      }

      case GRN_CMD_CYCLE_PARAMETERS_NEXT:
      case GRN_CMD_CYCLE_PARAMETERS_PREV:
      case GRN_CMD_CREATE_SAVE_PRESETS:
      case GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT:
      case GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV:
      case GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT:
      case GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV:
         SetFocusedControl(commandNumber);
         break;

      case GRN_CMD_TOGGLE_CONTRAST_GANG:
         ToggleContrastGang();
         break;

      case GRN_CMD_CLEAR_PENDING_STROKES:
         // Shift-A is overloaded with "Delete Mask", so be sure to pass on
         // the command if the Provisional isn't pending! Really the Mask Tool
         // should get first crack at the key, but sadly it was lumped in with
         // the default tool (Navigator) commands, which are done LAST!
         if (this->AreBrushStrokesPending())
         {
            this->RejectBrushStrokes(true);
         }
         else
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_CLEAR_FULL_FRAME_PREVIEW:
         // Shift-A is overloaded with "Delete Mask", so be sure to pass on
         // the command if the Provisional isn't pending! Really the Mask Tool
         // should get first crack at the key, but sadly it was lumped in with
         // the default tool (Navigator) commands, which are done LAST!
         if (getSystemAPI()->IsProvisionalPending())
         {
            getSystemAPI()->RejectProvisional(true); // true = show the frame
         }
         else
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_UNDO_ONE_PENDING_STROKE:
         // The A key is overloaded with "Delete Mask Point", so be sure to pass
         // on the command if no strokes are pending! Really the Mask Tool
         // should get first crack at the key, but sadly it was lumped in with
         // the default tool (Navigator) commands, which are done LAST!
         if (this->AreBrushStrokesPending())
         {
            this->UndoLastBrushStroke();
         }
         else
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_TOGGLE_FIX:
         if (getSystemAPI()->IsProvisionalPending())
         {
            // Provisional is pending, so this command is interpreted
            // as toggle
            getSystemAPI()->ToggleProvisional();
         }
         else
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }

         break;

      case GRN_CMD_START_BRUSHING:
         if (!StartBrushStroke(true))
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_START_ERASING:
         if (!this->AreBrushStrokesPending()
         || !StartBrushStroke(false))
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_STOP_ERASING:
      case GRN_CMD_STOP_BRUSHING:
         if (!EndBrushStroke())
         {
            Result = TOOL_RETURN_CODE_NO_ACTION;
         }
         break;

      case GRN_CMD_REPEAT_BRUSH_STROKES:
         RepeatBrushStrokes();
         break;

      case GRN_CMD_THREAD_CONTROL:
         if (ThreadControlDialog == NULL)
         {
            ThreadControlDialog = new TThreadControlDialog(GrainToolForm);
         }

         ThreadControlDialog->Show();
         break;

      default:
         Result = TOOL_RETURN_CODE_NO_ACTION;
         break;
   }

	if (GrainToolForm != 0)
	{
		GrainToolForm->UpdateProvisionButtons();
	}

	return Result;
} // onToolCommand
//--------------------------------------------------------------------------

bool CGrainTool::StartBrushStroke(bool isPainting)
{
   if (_brushState == BRUSH_IS_INVISIBLE)
   {
      return false;
   }

   // Auto-accept pending GOV
   if (getSystemAPI()->IsGOVPending())
   {
      getSystemAPI()->AcceptGOV();
   }

   // If we start stroking a different frame, auto-accept pending strokes.
   if (this->_lastStrokedFrameIndex != getSystemAPI()->getLastFrameIndex())
   {
      if (this->_lastStrokedFrameIndex >= 0)
      {
         if (!AutoAcceptBrushStrokes())
         {
            return false;
         }
      }

      this->_lastStrokedFrameIndex = getSystemAPI()->getLastFrameIndex();
   }

   _brushState = isPainting ? BRUSH_IS_PAINTING : BRUSH_IS_ERASING;

   BrushStroke brushStroke(this->_brush, !isPainting);
   brushStroke.Points.push_back(POINT {_brushCenterX, _brushCenterY});
   this->_previousBrushStrokeX = this->_brushCenterX;
   this->_previousBrushStrokeY = this->_brushCenterY;

   this->_grainGeneratorModel->AddBrushStroke(brushStroke);
   this->BrushStrokesChange.Notify();
   this->_needToRunTheStrokeProcessor = true;

   return true;
}
//--------------------------------------------------------------------------

bool CGrainTool::UpdateBrushStroke()
{
   if (_brushState != BRUSH_IS_PAINTING && _brushState != BRUSH_IS_ERASING)
   {
      return false;
   }

   this->_grainGeneratorModel->AddBrushStrokePoint(POINT {this->_brushCenterX, this->_brushCenterY});

   this->_previousBrushStrokeX = this->_brushCenterX;
   this->_previousBrushStrokeY = this->_brushCenterY;
   _needToRunTheStrokeProcessor = true;

   return true;
}
//--------------------------------------------------------------------------

bool CGrainTool::EndBrushStroke()
{
   if (_brushState != BRUSH_IS_PAINTING && _brushState != BRUSH_IS_ERASING)
   {
      return false;
   }

   if (this->_previousBrushStrokeX != this->_brushCenterX
   || this->_previousBrushStrokeY != this->_brushCenterY)
   {
      UpdateBrushStroke();
   }

   _brushState = BRUSH_IS_IDLE;
   this->_previousBrushStrokeX = -1;
   this->_previousBrushStrokeY = -1;

   getSystemAPI()->refreshFrameCached();

   return true;
}
//--------------------------------------------------------------------------

int CGrainTool::onChangingClip()
{
   // This function is called just before the current clip is unloaded.

   if (IsDisabled())                      // !IsActive() is OK!!!
      return TOOL_RETURN_CODE_NO_ACTION;

   TryToResolveProvisional();
   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

GrainGeneratorModel *CGrainTool::GetGrainGeneratorModel()
{
	return this->_grainGeneratorModel;
}
//--------------------------------------------------------------------------

int CGrainTool::onNewClip()
{
   if (!IsLicensed()) //NOT IsDisabled(), might want to Enable!
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (!IsActive())
   {
      this->_clipChangedWhileWeWereSleeping = true;
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   // Make sure we start fresh - probably not needed
   getSystemAPI()->ClearProvisional(false);

   // This allows the tool to notify the gui
   ClipChange.Notify();

   // Do this after the disabled check
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CGrainTool::onNewMarks()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // If necessary, clear "Paused" state from GUI
   // mbraca says " I don't understand this comment at all... why would
   // changing the marks affect PAUSE? In any case, the Notify() below
   // updates the execution buttons, but only if the tool is STOPPED!
   // So I'm nuking this line!!
   //UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   // This allows the tool to notify the gui
   MarksChange.Notify();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGrainTool::onChangingFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   TryToResolveProvisional();

   // If necessary, clear "Paused" state from GUI
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGrainTool::onMouseMove(CUserInput &userInput)
{
   // This actually gets called whether or not the mouse actually moved!
   int mouseX = userInput.imageX;
   int mouseY = userInput.imageY;
   bool mouseMoved = _brushCenterX != mouseX || _brushCenterY != mouseY;
   _brushCenterX = mouseX;
   _brushCenterY = mouseY;

   if (_brushState == BRUSH_IS_INVISIBLE || !mouseMoved)
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   UpdateBrushStroke();

   // Move the brush outline on the frame.
   getSystemAPI()->TellTheDisplayerToRedisplayTheCurrentFrame();

   // Always pass through the mouse move so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}
//--------------------------------------------------------------------------

int CGrainTool::onRedraw(int frameIndex)
{
   if (_brushState == BRUSH_IS_INVISIBLE || this->_hideBrushTemporarily)
   {
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   // Brush is GREEN for painting (or idle) and RED for erasing!
   getSystemAPI()->TellTheDisplayerToDrawAShape(
      _brushCenterX, _brushCenterY,
      brushShape, brushRadius, brushAspectRatio, brushAngle,
      ((_brushState == BRUSH_IS_ERASING) ? 255 : 0),
      ((_brushState != BRUSH_IS_ERASING) ? 255 : 0),
       0);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

bool CGrainTool::NotifyGOVStarting(bool *cancelStretch)
{
   // Return FALSE if we're using the brush, so GOV tool stays dormant.
   bool retVal = this->_brushState == BRUSH_IS_INVISIBLE
                 || !this->AreBrushStrokesPending();

   if (retVal)
   {
      // Give our parent a crack at it (LAST)
      retVal = CToolObject::NotifyGOVStarting();
   }

   if (retVal)
   {
      this->_hideBrushTemporarily = true;
      getSystemAPI()->showCursor(true);
      getSystemAPI()->refreshFrameCached();
   }

   return retVal;
}
//--------------------------------------------------------------------------

void CGrainTool::NotifyGOVDone()
{
   if (this->_brushState == BRUSH_IS_INVISIBLE)
   {
      return;
   }

   this->_hideBrushTemporarily = false;
   getSystemAPI()->showCursor(false);
   getSystemAPI()->refreshFrameCached();
}
//--------------------------------------------------------------------------

void CGrainTool::SetBrushActive(bool flag)
{
   if (flag)
   {
      this->_brushState = BRUSH_IS_IDLE;
      this->_brushCenterX = -1;
      this->_brushCenterY = -1;

      if (getSystemAPI()->IsGOVPending())
      {
         getSystemAPI()->AcceptGOV();
      }

      // Turning on brush processing -  kill D key mode.
      if (getSystemAPI()->IsProvisionalPending())
      {
         getSystemAPI()->RejectProvisional(false); // false = don't show the frame
      }

      getSystemAPI()->showCursor(false);
   }
   else
   {
      if (this->_lastStrokedFrameIndex >= 0)
      {
         if (!AutoAcceptBrushStrokes())
         {
            return;
         }
      }

      this->_brushState = BRUSH_IS_INVISIBLE;
   }

   // Show either the screen cursor or the brush outline on the frame.
   getSystemAPI()->showCursor(!flag);
   this->_hideBrushTemporarily = false;
   getSystemAPI()->refreshFrameCached();

   this->BrushStrokesChange.Notify();
   this->_lastStrokedFrameIndex = -1;
}
//--------------------------------------------------------------------------

void CGrainTool::setBrushParameters(int newMaxRadius, int newRadius, int newBlend, int newOpacity,
		int newAspect, int newAngle, bool newShape, EBrushType newType)
{
   this->_brush->setBrushParameters(newMaxRadius, newRadius, newBlend, newOpacity,
		newAspect, newAngle, newShape, newType);

   // TEMP
   this->brushShape = newShape ? SHAPE_HACK_RECTANGLE : SHAPE_HACK_ELLIPSE;
   this->brushRadius = newRadius;
   this->brushAspectRatio = newAspect;
   this->brushAngle = newAngle;
   //

   getSystemAPI()->refreshFrameCached();
}
//--------------------------------------------------------------------------

void CGrainTool::GetBrushStrokes(BrushStrokes &whereToCopyTo)
{
   return this->_grainGeneratorModel->GetCopyOfBrushStrokes(whereToCopyTo);
}
//--------------------------------------------------------------------------

void CGrainTool::SetAutoAccept(bool onOffFlag)
{
   _autoAcceptingBrushStrokes = onOffFlag;
}
//--------------------------------------------------------------------------

void CGrainTool::DestroyToolSetup(int &setupHandle, bool notIfPaused)
{
   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (setupHandle >= 0)
   {
      bool isPaused = getSystemAPI()->GetToolSetupStatus(setupHandle) == AT_STATUS_PAUSED;
      if (!(notIfPaused && isPaused))
      {
         if (setupHandle == activeToolSetup)
         {
            getSystemAPI()->SetActiveToolSetup(-1);
         }

         getSystemAPI()->DestroyToolSetup(setupHandle);
         setupHandle = -1;
      }
   }
}
//--------------------------------------------------------------------------

void CGrainTool::DestroyAllToolSetups(bool notIfPaused)
{
   // Grain has three tool setups: full frame preview, full frame render and brush stroke render.
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   DestroyToolSetup(_toolSetupHandle, notIfPaused);
   DestroyToolSetup(_trainingToolSetupHandle, notIfPaused);
   DestroyToolSetup(_brushStrokeToolSetupHandle, notIfPaused);
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CGrainTool::toolShow()
{
   ShowGrainToolGUI();

   if (!isItOkToUseToolNow())
      getSystemAPI()-> DisableTool(getToolDisabledReason());

   // Just in case these did change.... could be more clever about it?
   onNewMarks();

   return 0;
}

bool CGrainTool::IsShowing(void)
{
   return(IsToolVisible());
}

// ===================================================================
//
// Function:    toolHideQuery
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

bool CGrainTool::toolHideQuery()
{
   // These don't really belong here

   getSystemAPI()->WaitForToolProcessing();
   AutoAcceptBrushStrokes();

   return true;
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CGrainTool::toolHide()
{
   HideGrainToolGUI();
   doneWithTool();
   return 0;
}

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// Description: Called by tool infrastructure to build a "proc" instance
//              for frame processing
//
// Arguments:   newEmergencyStopFlagPtr - a pointer to a flag that tells
//              the :proc" to stop processing when it's true.
//
// Returns:     The newly created tool processor Instance.
//
// ===================================================================
CToolProcessor* CGrainTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   // We have separate processors for brush strokes vs full-frame
   if (this->_toolProcessorTypeToMake == BRUSH_STROKE_TOOL_PROCESSOR_TYPE)
   {
      return new CGrainToolBrushStrokeProc(
                                 GetToolNumber(),
                                 GetToolName(),
                                 getSystemAPI(),
                                 newEmergencyStopFlagPtr,
                                 GetToolProgressMonitor(),
                                 this->_grainGeneratorModel);
   }

   return new CGrainToolFullFrameProc(
                              GetToolNumber(),
                              GetToolName(),
                              getSystemAPI(),
                              newEmergencyStopFlagPtr,
                              GetToolProgressMonitor(),
                              this->_grainGeneratorModel);
}

// ===================================================================
//
// Function:    ProcessSingleFrame
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CGrainTool::ProcessSingleFrame(EToolSetupType setupType)
{
   int retVal;
   CAutoErrorReporter autoErr("CGrainTool::ProcessSingleFrame",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
   {
      // Something is running, so can't do Training processing right now
      return -1;
   }

   auto _toolSetupHandle = -1;
   auto currentFrameIndex = getSystemAPI()->getLastFrameIndex();
   auto inFrameIndex = currentFrameIndex;
   auto outFrameIndex = currentFrameIndex + 1;  // EXCLUSIVE!

   if (this->_brushState != BRUSH_IS_INVISIBLE)
   {
      if (this->_brushStrokeToolSetupHandle < 0)
      {
         this->_toolProcessorTypeToMake = BRUSH_STROKE_TOOL_PROCESSOR_TYPE;
         CToolIOConfig ioConfig(1, 1);
         ioConfig.processingThreadsPerFrame = 1;

         this->_brushStrokeToolSetupHandle =
            getSystemAPI()->MakeSimpleToolSetup("Grain", setupType, &ioConfig);

         if (this->_brushStrokeToolSetupHandle < 0)
         {
            autoErr.errorCode = -1;
            autoErr.msg << "Grain internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
            return -1;
         }
      }

      _toolSetupHandle = this->_brushStrokeToolSetupHandle;

      // Rigmarole to avoid duplicate history entries while stroking.
      // Stolen From DRSTool.
      getSystemAPI()->SetProvisionalReview(false);
      getSystemAPI()->SetProvisionalReprocess(this->AreBrushStrokesPending());
   }
   else
   {
      if (_trainingToolSetupHandle < 0)
      {
         // Create the tool setup
         this->_toolProcessorTypeToMake = FULL_FRAME_TOOL_PROCESSOR_TYPE;
         CToolIOConfig ioConfig(1, 1);
         ioConfig.processingThreadsPerFrame = SysInfo::AvailableProcessorCoreCount();

         _trainingToolSetupHandle =
            getSystemAPI()->MakeSimpleToolSetup("Grain", setupType, &ioConfig);

         if (_trainingToolSetupHandle < 0)
         {
            autoErr.errorCode = -1;
            autoErr.msg << "Grain internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
            return -1;
         }
      }

      _toolSetupHandle = this->_trainingToolSetupHandle;
      inFrameIndex -= GRN_MAX_ADJACENT_FRAMES;
      outFrameIndex += GRN_MAX_ADJACENT_FRAMES;

      getSystemAPI()->SetProvisionalReview(false);       // maybe want true? QQQ
      getSystemAPI()->SetProvisionalReprocess(false);
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(_toolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Grain internal error: SetActiveToolSetup("
                  << _toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
   }

   // Set processing frame range to the current frame index
   CToolFrameRange frameRange(1, 1);
   frameRange.inFrameRange[0].randomAccess = true;
   frameRange.inFrameRange[0].inFrameIndex = inFrameIndex;
   frameRange.inFrameRange[0].outFrameIndex = outFrameIndex;
   retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Grain internal error: SetToolFrameRange"
                  << " failed with return code " << retVal;
      return retVal;
   }

   // Query the GUI for parameter settings
   grainParams.Reset();
   GatherParameters(grainParams);
   grainParams.setWrapping(getSystemAPI(), currentFrameIndex);

   // Pass a copy of the parameters to the tool infrastructure (which will later delete it)
   // Note: we use the CGrainToolParameters default copy constructor [noted]
   CGrainToolParameters *toolParams = new CGrainToolParameters(grainParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Grain internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
   }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}

// ===================================================================
//
// Function:    RunFrameProcessing
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CGrainTool::RunFrameProcessing(int newResumeFrame)
{
   int retVal;
   CAutoErrorReporter autoErr("CGrainTool::RunFrameProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
   {
      autoErr.errorCode = -999;
      autoErr.msg << "Grain internal error, tried to run when disabled ";
      return autoErr.errorCode;
   }

   if (this->_brushState != BRUSH_IS_INVISIBLE)
   {
      if (!AreBrushStrokesPending())
      {
         // Internal error!
         autoErr.errorCode = -998;
         autoErr.msg << "Grain internal error, tried to run when no brush strokes pending ";
         return autoErr.errorCode;
      }

      this->SetUpMultiFrameStroking();
      this->_multiFrameStrokingInProgress = true;
   }

// TBD:  Fix correctly.  The call to refreshFrame partially fixes
//       the problem with the timeline slider getting stuck during
//       tool processing
   getSystemAPI()->refreshFrame();

   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == 0)
      return -1;  // No image format, probably clip isn't loaded

   int pixelsPerLine = imageFormat->getPixelsPerLine();

#ifndef _WIN64
   // If image size greater than 3K, then can only have one tool setup
   // around at a time because of memory constraints
   if (pixelsPerLine > 3072 && _trainingToolSetupHandle >= 0
       && _trainingToolSetupHandle == getSystemAPI()->GetActiveToolSetupHandle())
      {
      DestroyAllToolSetups(false);
      }
#endif

   // When RENDERING:
   //
   // On systems with 4 processors or more, process 4 frames concurrently.
   // Occupy the rest of the processors by slicing the frames.
   //
   // On systems with fewer processors, concurrent frames should match the
   // number of processor cores (NOT 'processors' which includes hyperthreads
   // which we don't want to use because of cache issues).
   //
   // When PREVIEWING or when the frames are larger than 3K:
   //
   // Only process one frame at a time. Specify the number of slices per
   // frame to match the number of processor cores. For previewing that
   // lets us see every frame, and for 4K frames, it gives a prayer of
   // not running out of memory!!
   //
   int nCores = SysInfo::AvailableProcessorCoreCount();
   concurrentFrameCount = (nCores > 4)? 4 : nCores;
   if (!GetToolProcessingRenderFlag())
   {
      concurrentFrameCount = 1;
   }
#ifndef _WIN64
   else if (pixelsPerLine > 3072)
   {
      concurrentFrameCount = 1;
   }
#endif

   processingThreadsPerFrame = SysInfo::AvailableProcessorCoreCount() /
                               concurrentFrameCount;
   // some other parameters we can play around with in our quest for speed
   slicesPerThread = 1;     // only useful for column slices
   doRowSlices = true;      // as opposed to column slices
   noopProcessing = false;  // for measuring overhead - suppresses processing

   // The parameters can be set manually (see GRN_CMD_THREAD_CONTROL)
   if ((ThreadControlDialog != NULL) && ThreadControlDialog->Visible)
      {
      concurrentFrameCount =
               ThreadControlDialog->ConcurrentFramesTrackEdit->GetValue();
      processingThreadsPerFrame =
               ThreadControlDialog->ProcessingThreadsTrackEdit->GetValue();
      doRowSlices = ThreadControlDialog->RowSlicesRadioButton->Checked;
      slicesPerThread =
               ThreadControlDialog->SlicesPerThreadTrackEdit->GetValue();
      noopProcessing = ThreadControlDialog->NoopCheckBox->Checked;
      }

   // Recreate the tool processing setup if any of the parameters changed
   if (_toolSetupHandle >= 0 && (
       concurrentFrameCount != oldConcurrentFrameCount ||
       processingThreadsPerFrame != oldProcessingThreadsPerFrame ||
       doRowSlices != oldDoRowSlices ||
       slicesPerThread != oldSlicesPerThread ||
       noopProcessing != oldNoopProcessing))
      {
      // Can't destroy just the one? oh well, doesn't really matter...
      DestroyAllToolSetups(false);
      }

   // Check if we have a tool setup yet
   if (_toolSetupHandle < 0)
      {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      ioConfig.processingThreadCount = concurrentFrameCount;
      ioConfig.processingThreadsPerFrame = processingThreadsPerFrame;
      ioConfig.doRowSlices = doRowSlices;
      ioConfig.slicesPerThread = slicesPerThread;
      ioConfig.noopProcessing = noopProcessing;
      oldConcurrentFrameCount = concurrentFrameCount;
      oldProcessingThreadsPerFrame = processingThreadsPerFrame;
      oldDoRowSlices = doRowSlices;
      oldSlicesPerThread = slicesPerThread;
      oldNoopProcessing = noopProcessing;
      this->_toolProcessorTypeToMake = FULL_FRAME_TOOL_PROCESSOR_TYPE;
      _toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Grain",
                                                   TOOL_SETUP_TYPE_MULTI_FRAME,
                                                            &ioConfig);
      if (_toolSetupHandle < 0)
         {
         autoErr.errorCode = -1;
         autoErr.msg << "Grain internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
         return -1;
         }
      }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(_toolSetupHandle);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Grain internal error: SetActiveToolSetup("
                  << _toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
      }

   if (newResumeFrame < 0)
      {
      // Starting from Stop, so set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markIn = getSystemAPI()->getMarkIn();
      int markOut = getSystemAPI()->getMarkOut();
      if (markIn < 0 || markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark In and Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= markIn)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond Mark In";
         return -1;
         }
      frameRange.inFrameRange[0].inFrameIndex = markIn - GRN_MAX_ADJACENT_FRAMES;
      frameRange.inFrameRange[0].outFrameIndex = markOut + GRN_MAX_ADJACENT_FRAMES;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "Grain internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }
   else
      {
      // Resuming from Pause state, so set processing frame range
      // from newResumeFrame to out mark
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markOut = getSystemAPI()->getMarkOut();
      if (markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= newResumeFrame)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond the Resume frame";
         return -1;
         }
      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame - GRN_MAX_ADJACENT_FRAMES;
      frameRange.inFrameRange[0].outFrameIndex = markOut + GRN_MAX_ADJACENT_FRAMES;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "Grain internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }

   // Set render/preview
   getSystemAPI()->SetProvisionalRender(GetToolProcessingRenderFlag());

   getSystemAPI()->SetProvisionalReview(false);       // maybe want true? QQQ
   getSystemAPI()->SetProvisionalReprocess(false);

   // Query the GUI for parameter settings
   GatherParameters(grainParams);
   grainParams.setWrapping(getSystemAPI(), -1);

   // Pass a copy of the parameters to the tool infrastructure (which will later delete it)
   // Note: we use a CGrainToolParameters default copy constructor
   CGrainToolParameters *toolParams = new CGrainToolParameters(grainParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Grain internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}
//--------------------------------------------------------------------------

void CGrainTool::GatherParameters(CGrainToolParameters &toolParams)
{
   toolParams.Reset();

   toolParams.SetRegionOfInterest(getSystemAPI()->GetMaskRoi());

   GatherGUIParameters(toolParams);
}
//--------------------------------------------------------------------------

int CGrainTool::StopFrameProcessing()
{
   int retVal;

   // Kludge so we don't get stuck if the following sequence is followed
   //   Preview -> Pause -> Preview 1 -> Stop
   // We really need some better logic here, as this prevents Stop button
   // from acting on Preview 1 processing
   if (_toolSetupHandle >= 0)
      getSystemAPI()->SetActiveToolSetup(_toolSetupHandle);

   retVal = CToolObject::StopFrameProcessing();
   if (retVal != 0)
      return retVal;

   return 0;
}
//--------------------------------------------------------------------------

int CGrainTool::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   if (this->_multiFrameStrokingInProgress
   && autotoolStatus.status == AT_STATUS_STOPPED)
   {
      this->_multiFrameStrokingInProgress = false;
      this->GetGrainGeneratorModel()->ClearBrushStrokes();
      this->BrushStrokesChange.Notify();
   }

   // Call parent class' function
   CToolObject::onToolProcessingStatus(autotoolStatus);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGrainTool::onSwitchSubtool()
{
   SwitchSubtool();
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

void CGrainTool::UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                                     bool processingRenderFlag)
{
   UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);
}
//---------------------------------------------------------------------------

int CGrainTool::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
   if (IsDisabled())
      return 0;

   CaptureGUIParameters(pdlEntryToolParams);

   return 0;
}

int CGrainTool::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (IsDisabled())
      return 0;

   int retVal;

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
      {
      CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(i);
      if (toolAttribs->GetElementName() == CGrainToolParameters::iniSectionName)
		 {
		 grainParams.ReadPDLEntry(toolAttribs);

		 // Ugly but quick
		 this->_grainGeneratorModel->SetGrainParameters(grainParams.grainParameters);
		 break;
         }
      }

   SetGUIParameters(grainParams);

   return 0;
}

//---------------------------------------------------------------------------

void CGrainTool::WriteSettings(CIniFile *iniFile, const string &section)

//  This writes the ini files, saving the Tool's platform-independent
//  parameters.  Called by Tool's platform-dependent GUI.
//
//******************************************************************************
{
  return;
}

//-----------------------------------------------------------------------------

void CGrainTool::ReadSettings(CIniFile *iniFile, const string &section)

//  This reads the init file and sets the tool's platform-independent
//  parameters.  Called by the Tool's platform-dependent GUI.
//******************************************************************************
{
  return;
}

//-----------------------------------------------------------------------------

int CGrainTool::TryToResolveProvisional()
{
   getSystemAPI()->WaitForToolProcessing();
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   if (this->_brushState != BRUSH_IS_INVISIBLE && this->AreBrushStrokesPending())
   {
      this->_needToAcceptBrushStrokes = false;
      this->AutoAcceptBrushStrokes();
   }
	else
   {
      getSystemAPI()->ClearProvisional(false);
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}




