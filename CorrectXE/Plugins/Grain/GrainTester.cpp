//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("GrainTesterUnit.cpp", GrainTesterForm);
USEFORM("..\..\Common_gui_win\TrackEditFrameUnit.cpp", TrackEditFrame); /* TFrame: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		TStyleManager::TrySetStyle("MTI Dark");
////        <VCL_Custom_Styles>&quot;MTI Dark|VCLSTYLE|../../nav_gui_win/MtiDarkStyle.vsf&quot;</VCL_Custom_Styles>
////		TStyleManager::TrySetStyle("MTI Dark");
		Application->CreateForm(__classid(TGrainTesterForm), &GrainTesterForm);
		Application->CreateForm(__classid(TTrackEditFrame), &TrackEditFrame);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
