

#include "GrainGeneratorModel.h"

const string GrainIniFileName("$(CPMP_USER_DIR)Grain.ini");

static const SIniAssociation GrainParameterBaseAssociationsData[] =
{ {"GP_BASE_INVALID", GP_BASE_INVALID}, {"GP_BASE_CLIP", GP_BASE_CLIP},
	{"GP_BASE_PROJECT", GP_BASE_PROJECT}, {"", -1}};

static CIniAssociations GrainBaseAssociations(GrainParameterBaseAssociationsData, "GP_BASE_");

GrainGeneratorModel GrainGeneratorModel::GlobalInstance(1024, 680);
GrainGeneratorModel &GrainGeneratorModel::GetGlobalInstanceRef()
{
	return GlobalInstance;
}

void GrainGeneratorModel::ReadPresetsFromIni(const string &iniFileName, GrainParameters presets[3])
{
	if (iniFileName == "")
	{
		return;
	}

	CIniFile *ini = CreateIniFile(iniFileName);
	presets[0].ReadSettings(ini, "Preset0");
	presets[1].ReadSettings(ini, "Preset1");
	presets[2].ReadSettings(ini, "Preset2");
	DeleteIniFile(ini);
}

void GrainGeneratorModel::WritePresetsToIni(const string &iniFileName, const GrainParameters presets[3]) const
{
	CIniFile *ini = CreateIniFile(iniFileName);
	presets[0].WriteSettings(ini, "Preset0");
	presets[1].WriteSettings(ini, "Preset1");
	presets[2].WriteSettings(ini, "Preset2");
	DeleteIniFile(ini);
}

void  GrainGeneratorModel::WritePresetsToIni() const
{
	if (_masterClipFolderPath != "")
	{
		 std::string iniFilename = AddDirSeparator(_masterClipFolderPath) + "Grain.clip";
		 this->WritePresetsToIni(iniFilename, this->_clipPresets);
	}

	if (_projectRootFolderPath != "")
	{
		 std::string iniFilename = AddDirSeparator(_projectRootFolderPath) + "Grain.project";
		 this->WritePresetsToIni(iniFilename, this->_projectPresets);
	}
}

void  GrainGeneratorModel::ReadPresetsFromIni()
{
	std::string iniFilename;
	if (_masterClipFolderPath != "")
	{
		 iniFilename = AddDirSeparator(_masterClipFolderPath) + "Grain.clip";
		 this->ReadPresetsFromIni(iniFilename, this->_clipPresets);
	}

	if (_projectRootFolderPath != "")
	{
		 iniFilename = AddDirSeparator(_projectRootFolderPath) + "Grain.project";
		 this->ReadPresetsFromIni(iniFilename, this->_projectPresets);
	}
}

void GrainGeneratorModel::LoadFromGrainIni(const string &iniFileName)
{
	CIniFile *ini = CreateIniFile(iniFileName);
	this->LoadFromGrainIni(ini);
	DeleteIniFile(ini);
}

void GrainGeneratorModel::SaveToGrainIni(const string &iniFileName)
{
	CIniFile *ini = CreateIniFile(iniFileName);
	this->SaveToGrainIni(ini);
	DeleteIniFile(ini);
}

void GrainGeneratorModel::LoadFromGrainIni(CIniFile *ini)
{
	this->ReadPresetsFromIni();

	this->_currentPreset = ini->ReadInteger("GrainGeneratorModel", "Preset", 0);
	this->_currentGrainParametersBase = (GrainParameterBaseType)ini->ReadAssociation
		(GrainBaseAssociations, "GrainGeneratorModel", "GrainBase", GP_BASE_PROJECT);
	if (this->_currentGrainParametersBase == GP_BASE_INVALID)
	{
	   this->_currentGrainParametersBase = GP_BASE_PROJECT;
	}
	this->PresetChange.Notify();
}

void GrainGeneratorModel::SaveToGrainIni(CIniFile *ini)
{
	ini->WriteInteger("GrainGeneratorModel", "Preset", this->_currentPreset);
	ini->WriteAssociation(GrainBaseAssociations, "GrainGeneratorModel", "GrainBase",
		this->_currentGrainParametersBase);

	this->WritePresetsToIni();
}

GrainGeneratorModel::~GrainGeneratorModel()
{
}

GrainGeneratorModel::GrainGeneratorModel(int width, int height)
: _grainGenerator(0)
, _currentPreset(0)
, _currentGrainParametersBase(GP_BASE_PROJECT)
, _masterClipFolderPath("")
, _projectRootFolderPath("")
, _useBrushMask(false)
{
	this->SetImageSize(width, height);

	// Not really necessary
	this->_clipPresets[0].SetDefaults();
	this->_clipPresets[1].SetDefaults();
	this->_clipPresets[2].SetDefaults();

	this->_projectPresets[0].SetDefaults();
	this->_projectPresets[1].SetDefaults();
	this->_projectPresets[2].SetDefaults();

	this->_brushRevealer = new BrushRevealer(IppiSize {width, height});

}

void GrainGeneratorModel::SetImageSize(int width, int height)
{
	// Do nothing if nothing to do
	if ((this->_imageSize.width == width) && (this->_imageSize.height == height))
	{
		return;
	}

	this->_imageSize = IppiSize
	{
		width, height
	};

	// We might want to use a flyweight class here
	delete this->_grainGenerator;
	this->_grainGenerator = new GrainGenerator(this->_imageSize);
	this->_grainGenerator->SetGrainParameters(this->GetGrainParameters());
	this->_brushRevealer = new BrushRevealer(this->_imageSize);
}

int GrainGeneratorModel::GetPreset() const
{
	return this->_currentPreset;
}

void GrainGeneratorModel::SetPreset(int preset)
{
	if (this->_currentPreset != preset)
	{
		this->_currentPreset = preset;
		this->SetGrainParametesFromBaseAndPreset();
		this->PresetChange.Notify();
	}
}

GrainParameterBaseType GrainGeneratorModel::GetGrainParametersBase() const
{
	return this->_currentGrainParametersBase;
}

void GrainGeneratorModel::SetGrainParametersBase(GrainParameterBaseType grainParametersBase)
{
	if (this->_currentGrainParametersBase != grainParametersBase)
	{
		this->_currentGrainParametersBase = grainParametersBase;
		this->SetGrainParametesFromBaseAndPreset();
		this->PresetChange.Notify();
	}
}

void GrainGeneratorModel::SetGrainParametesFromBaseAndPreset()
{
	// At some point a switch statment must exist because the ini files fork, do it here
	switch (this->_currentGrainParametersBase)
	{
	case GP_BASE_CLIP:
		this->_grainGenerator->SetGrainParameters(this->_clipPresets[this->_currentPreset]);
		break;

	case GP_BASE_PROJECT:
		this->_grainGenerator->SetGrainParameters(this->_projectPresets[this->_currentPreset]);
		break;

	default:
        break;
	}

	if (this->_grainGenerator->AreGrainValuesInvalid())
	{
		this->GrainChange.Notify();
	}
}

GrainGenerator* GrainGeneratorModel::GetGrainGenerator()
{
	return this->_grainGenerator;
}

void GrainGeneratorModel::SetGrainParameters(const GrainParameters &grainParameters)
{
	// See if any parameters have changed
	if (grainParameters == this->_grainGenerator->GetGrainParameters())
	{
		return;
	}

	this->_grainGenerator->SetGrainParameters(grainParameters);
	this->GrainParametersChange.Notify();

	if (this->_grainGenerator->AreGrainValuesInvalid())
	{
		this->GrainChange.Notify();
	}
}

const GrainParameters &GrainGeneratorModel::GetGrainParameters() const
{
	return this->_grainGenerator->GetGrainParameters();
}

bool GrainGeneratorModel::DoesCurrentGrainParametersMatchPreset()
{
	return this->_grainGenerator->GetGrainParameters() == this->GetSelectedGrainParameters();
}

const GrainParameters GrainGeneratorModel::GetSelectedGrainParameters() const
{
	GrainParameters grainParameters;

	switch (this->_currentGrainParametersBase)
	{
	case GP_BASE_CLIP:
		grainParameters = this->_clipPresets[this->_currentPreset];
		break;

	case GP_BASE_PROJECT:
		grainParameters = this->_projectPresets[this->_currentPreset];
		break;

	default:
		grainParameters.SetDefaults();
	}

	return grainParameters;
}

void GrainGeneratorModel::SetCurrentToSelectedPreset()
{
	switch (this->_currentGrainParametersBase)
	{
	case GP_BASE_CLIP:
		this->_clipPresets[this->_currentPreset] = this->_grainGenerator->GetGrainParameters();
		break;

	case GP_BASE_PROJECT:
		this->_projectPresets[this->_currentPreset] = this->_grainGenerator->GetGrainParameters();
		break;

	default:
		break;
	}
}

void GrainGeneratorModel::SetProjectRootFolderPath(const string &folderPath)
{
	this->_projectRootFolderPath = folderPath;
}

void GrainGeneratorModel::SetMasterClipFolderPath(const string &folderPath)
{
	this->_masterClipFolderPath = folderPath;
}

void GrainGeneratorModel::AddBrushStroke(const BrushStroke &bs)
{
   CAutoThreadLocker lock(this->_brushStrokesLock);

   this->_allBrushStrokes.push_back(bs);
   this->_unstampedBrushStrokes.push_back(bs);
}

void GrainGeneratorModel::AddBrushStrokePoint(POINT newPoint)
{
   auto count = this->_allBrushStrokes.size();
   if (count == 0)
   {
      // This is really an error QQQ
      return;
   }

   BrushStroke &lastStroke = this->_allBrushStrokes[count - 1];
   lastStroke.Points.push_back(newPoint);

   BrushStroke *lastUnstampedStroke;
   count = this->_unstampedBrushStrokes.size();
   if (count == 0)
   {
      // Oops, the last unstamped stroke was consumed - need to re-create it!
      this->_unstampedBrushStrokes.push_back(lastStroke);
      lastUnstampedStroke = &this->_unstampedBrushStrokes[0];
      lastUnstampedStroke->Points.clear();
   }
   else
   {
      lastUnstampedStroke = &this->_unstampedBrushStrokes[count - 1];
   }

   lastUnstampedStroke->Points.push_back(newPoint);
}

bool GrainGeneratorModel::UndoLastBrushStroke()
{
   // Returns true if there was anything to undo.

   CAutoThreadLocker lock(this->_brushStrokesLock);

   auto count = this->_allBrushStrokes.size();
   if (count == 0)
   {
      // Nothing to undo!
      return false;
   }

   // Uggh need to regenerate the mask from scratch!
   this->_allBrushStrokes.pop_back();
   this->_unstampedBrushStrokes = this->_allBrushStrokes;
   this->_brushRevealer->ResetMask();

   return true;
}

void GrainGeneratorModel::SetBrushStrokes(const BrushStrokes &newBrushStrokes)
{
   CAutoThreadLocker lock(this->_brushStrokesLock);

   this->_allBrushStrokes = newBrushStrokes;
   this->_unstampedBrushStrokes = newBrushStrokes;
   this->_brushRevealer->ResetMask();
}

void GrainGeneratorModel::ClearBrushStrokes()
{
   CAutoThreadLocker lock(this->_brushStrokesLock);

   this->_allBrushStrokes.clear();
   this->_unstampedBrushStrokes.clear();
   this->_brushRevealer->ResetMask();
}

void GrainGeneratorModel::GetCopyOfBrushStrokes(BrushStrokes &copy) const
{
   copy = this->_allBrushStrokes;
}

void GrainGeneratorModel::StampNewBrushStrokes()
{
   // Make a copy of the new strokes so we don't lock out the producer
   BrushStrokes localBrushStrokes;
   {
      CAutoThreadLocker lock(this->_brushStrokesLock);

      localBrushStrokes.swap(this->_unstampedBrushStrokes);
   }

   this->_brushRevealer->StampMask(localBrushStrokes, false);  // Don't reset mask!
}

size_t GrainGeneratorModel::GetStrokeCount()
{
   return this->_allBrushStrokes.size();
}

BrushRevealer *GrainGeneratorModel::GetBrushRevealer()
{
	return this->_brushRevealer;
}

void GrainGeneratorModel::SetUseBrushMask(bool useBrushMask)
{
	this->_useBrushMask = useBrushMask;
}

bool GrainGeneratorModel::GetUseBrushMask() const
{
	return this->_useBrushMask;
}


