#ifndef GrainGeneratorModelH
#define GrainGeneratorModelH

#include "GrainCoreApi.h"
#include "GrainParameters.h"
#include "GrainGenerator.h"
#include "BrushRevealer.h"
#include "BrushStroke.h"
#include "ThreadLocker.h"

enum GrainParameterBaseType
{
	GP_BASE_INVALID = 0,
	GP_BASE_CLIP = 1,
	GP_BASE_PROJECT = 2
};

MTI_GRAINCORE_API class GrainGeneratorModel
{
	static GrainGeneratorModel GlobalInstance;

public:
	GrainGeneratorModel(int width, int height);
	~GrainGeneratorModel();

	static GrainGeneratorModel &GetGlobalInstanceRef();

   void LoadFromGrainIni(const string &iniFileName);
   void SaveToGrainIni(const string &iniFileName);

   void LoadFromGrainIni(CIniFile *ini);
   void SaveToGrainIni(CIniFile *ini);

   int GetPreset() const;
   void SetPreset(int preset);

   GrainParameterBaseType GetGrainParametersBase() const;
   void SetGrainParametersBase(GrainParameterBaseType grainParametersBase);

   void SetGrainParameters(const GrainParameters &grainParameters);
   const GrainParameters &GetGrainParameters() const;

   GrainGenerator* GetGrainGenerator();

   const GrainParameters GetSelectedGrainParameters() const;
   void SetCurrentToSelectedPreset();

   bool DoesCurrentGrainParametersMatchPreset();

   void SetImageSize(int width, int height);
   void SetMasterClipFolderPath(const string &folderPath);
   void SetProjectRootFolderPath(const string &folderPath);

   CBHook GrainChange;
   CBHook PresetChange;
   CBHook GrainParametersChange;

   void ReadPresetsFromIni();
   void WritePresetsToIni() const;

   void AddBrushStroke(const BrushStroke &bs);
   void AddBrushStrokePoint(POINT newPoint);
   bool UndoLastBrushStroke();
   void SetBrushStrokes(const BrushStrokes &newBrushStrokes);
   void ClearBrushStrokes();
   void GetCopyOfBrushStrokes(BrushStrokes &copy) const;
   void StampNewBrushStrokes();
   size_t GetStrokeCount();

   BrushRevealer *GetBrushRevealer();

   void SetUseBrushMask(bool useBrushMask);
   bool GetUseBrushMask() const;

   void SetGrainParametesFromBaseAndPreset();

private:

   GrainGenerator *_grainGenerator;
   BrushRevealer *_brushRevealer;

   int _currentPreset;
   bool _useBrushMask;
   IppiSize _imageSize;
   GrainParameters _clipPresets[3];
   GrainParameters _projectPresets[3];
   GrainParameters _currentGrainParameters;
   GrainParameterBaseType _currentGrainParametersBase;

   BrushStrokes _allBrushStrokes;
   BrushStrokes _unstampedBrushStrokes;
   CThreadLock _brushStrokesLock;

   string _masterClipFolderPath;
   string _projectRootFolderPath;

   void ReadPresetsFromIni(const string &iniFileName, GrainParameters presets[3]);
   void WritePresetsToIni(const string &iniFileName, const GrainParameters presets[3]) const;
};

#endif
