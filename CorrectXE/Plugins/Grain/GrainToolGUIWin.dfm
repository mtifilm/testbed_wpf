object GrainToolForm: TGrainToolForm
  Left = 463
  Top = 162
  BiDiMode = bdLeftToRight
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsToolWindow
  Caption = 'Grain'
  ClientHeight = 720
  ClientWidth = 512
  Color = clBtnFace
  Constraints.MaxHeight = 749
  Constraints.MaxWidth = 518
  Constraints.MinHeight = 744
  Constraints.MinWidth = 518
  DefaultMonitor = dmDesktop
  DockSite = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  ParentBiDiMode = False
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object GrainControlPanel: TColorPanel
    Left = 0
    Top = 0
    Width = 518
    Height = 720
    BevelOuter = bvNone
    Color = 6974058
    Constraints.MaxHeight = 720
    Constraints.MaxWidth = 518
    Constraints.MinHeight = 720
    Constraints.MinWidth = 518
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object StatusBar: TPanel
      Left = 0
      Top = 701
      Width = 518
      Height = 19
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 0
      object FPSLabel: TLabel
        Left = 175
        Top = 2
        Width = 3
        Height = 13
      end
      object MSLabel: TLabel
        Left = 4
        Top = 2
        Width = 3
        Height = 13
      end
    end
    object TitlePanel: TPanel
      Left = 0
      Top = 0
      Width = 512
      Height = 32
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 228
        Top = 0
        Width = 44
        Height = 19
        Caption = 'Grain'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
    end
    inline ExecButtonsToolbar: TExecButtonsFrame
      Left = 12
      Top = 636
      Width = 480
      Height = 45
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 12
      ExplicitTop = 636
      ExplicitWidth = 480
      inherited ButtonPressedNotifier: TSpeedButton
        OnClick = ExecButtonsToolbar_ButtonPressedNotifier
      end
      inherited ResumeGroupBox: TGroupBox
        Left = 341
        ExplicitLeft = 341
      end
    end
    inline ExecStatusBar: TExecStatusBarFrame
      Left = 16
      Top = 682
      Width = 480
      Height = 38
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      ExplicitLeft = 16
      ExplicitTop = 682
      inherited BackPanel: TPanel
        inherited ReadoutPanel: TPanel
          inherited ProgressPanelLight: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited PercentValue1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue1: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingLabel1: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue1: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue1: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue1: TLabel
              Anchors = [akLeft]
            end
          end
          inherited ProgressPanelDark: TColorPanel
            Anchors = [akLeft, akTop, akRight]
            inherited RemainingLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalLabel2: TLabel
              Anchors = [akLeft]
            end
            inherited TotalValue2: TLabel
              Anchors = [akLeft]
            end
            inherited RemainingValue2: TLabel
              Anchors = [akLeft]
            end
            inherited ElapsedValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PerFrameValue2: TLabel
              Anchors = [akLeft]
            end
            inherited PercentValue2: TLabel
              Anchors = [akLeft]
            end
          end
        end
      end
    end
    object GrainPageControl: TPageControl
      Left = 22
      Top = 54
      Width = 465
      Height = 497
      ActivePage = ReduceTabSheet
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object ReduceTabSheet: TTabSheet
        Caption = 'Reduce'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object AnalysisParametersGroupBox: TGroupBox
          Left = 40
          Top = 22
          Width = 371
          Height = 187
          Caption = ' Analysis Parameters '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Label2: TLabel
            Left = 16
            Top = 72
            Width = 48
            Height = 16
            Caption = 'Contrast'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          inline SizeTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 24
            Width = 343
            Height = 22
            Hint = 
              'Indicate size of the grain to reduce'#13#10'Press 1 to cycle through A' +
              'nalysis Parameters'#13#10'Move mouse wheel or use arrow keys to change'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            ExplicitLeft = 16
            ExplicitTop = 24
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Caption = 'Size'
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Max = 10
              Min = 1
              Position = 10
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
          end
          inline ContrastRTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 96
            Width = 343
            Height = 22
            Hint = 
              'Indicate level of contrast in the RED channel'#13#10'Press 1 to cycle ' +
              'through Analysis Parameters'#13#10'Move mouse wheel or use arrow keys ' +
              'to change'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 16
            ExplicitTop = 96
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Caption = 'Red'
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Position = 100
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = ContrastRTrackEditChange
            end
          end
          inline ContrastGTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 118
            Width = 343
            Height = 22
            Hint = 
              'Indicate level of contrast in the GREEN channel'#13#10'Press 1 to cycl' +
              'e through Analysis Parameters'#13#10'Move mouse wheel or use arrow key' +
              's to change'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = 16
            ExplicitTop = 118
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Caption = 'Green'
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Position = 100
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = ContrastGTrackEditChange
            end
          end
          inline ContrastBTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 140
            Width = 343
            Height = 22
            Hint = 
              'Indicate level of contrast in the BLUE channel'#13#10'Press 1 to cycle' +
              ' through Analysis Parameters'#13#10'Move mouse wheel or use arrow keys' +
              ' to change'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ExplicitLeft = 16
            ExplicitTop = 140
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Caption = 'Blue'
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Position = 100
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = ContrastBTrackEditChange
            end
          end
          object ContrastGangCheckBox: TCheckBox
            Left = 166
            Top = 73
            Width = 58
            Height = 17
            Hint = 
              'Constrain contrast settings to be the same for all channels'#13#10'Pre' +
              'ss 2 to toggle'
            Caption = 'Gang'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 3
            OnClick = ContrastGangCheckBoxClick
          end
        end
        object TemporalAnalysisGroupBox: TGroupBox
          Left = 40
          Top = 225
          Width = 371
          Height = 104
          Caption = ' Temporal Analysis '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          inline TemporalWeightHighTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 68
            Width = 343
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            ExplicitLeft = 16
            ExplicitTop = 68
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Left = 40
              Width = 3
              Caption = ''
              ExplicitLeft = 40
              ExplicitWidth = 3
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Position = 100
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
          end
          inline TemporalWeightMediumTrackEdit: TCompactTrackEditFrame
            Left = 16
            Top = 46
            Width = 343
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            ExplicitLeft = 16
            ExplicitTop = 46
            ExplicitWidth = 343
            inherited TitleLabel: TLabel
              Left = 40
              Width = 3
              Caption = ''
              ExplicitLeft = 40
              ExplicitWidth = 3
            end
            inherited TrackBar: TTrackBar
              Width = 237
              Position = 100
              ExplicitWidth = 237
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 225
              ExplicitWidth = 225
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
          end
          object TemporalWeightOffRadioButton: TRadioButton
            Left = 22
            Top = 24
            Width = 57
            Height = 17
            Hint = 
              'Do not look at surrounding frames to help identify grain'#13#10'Press ' +
              '3 to cycle through Temporal settings'
            Caption = 'Off'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TabStop = True
            OnClick = TemporalWeightRadioButtonClick
          end
          object TemporalWeightMediumRadioButton: TRadioButton
            Left = 22
            Top = 46
            Width = 58
            Height = 17
            Hint = 
              'Faster, use fewer surrounding frames for analysis'#13#10'Press 3 to cy' +
              'cle through Temporal settings'
            Caption = 'Medium'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            TabStop = True
            OnClick = TemporalWeightRadioButtonClick
          end
          object TemporalWeightHighRadioButton: TRadioButton
            Left = 22
            Top = 68
            Width = 49
            Height = 17
            Hint = 
              'Higher quality, use more surrounding frames for analysis'#13#10'Press ' +
              '3 to cycle through Temporal settings'
            Caption = 'High'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            TabStop = True
            OnClick = TemporalWeightRadioButtonClick
          end
        end
      end
      object CreateTabSheet: TTabSheet
        Caption = 'Create'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object NoChannelsSelectedWarningLabel: TLabel
          Left = 289
          Top = 164
          Width = 155
          Height = 13
          Caption = 'Warning - No channels selected!'
          Visible = False
        end
        object GrainGroupBox: TGroupBox
          Left = 6
          Top = 10
          Width = 443
          Height = 148
          Caption = ' Grain '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object SpeedLabel: TLabel
            Left = 316
            Top = 109
            Width = 62
            Height = 13
            Caption = 'Grain Speed:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          inline SizeTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 20
            Width = 300
            Height = 25
            Hint = 
              'Average grain size'#13#10'Press 1 to cycle through grain creation sett' +
              'ings'#13#10'Use mouse wheel or arrow keys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            ExplicitLeft = 7
            ExplicitTop = 20
            ExplicitWidth = 300
            ExplicitHeight = 25
            inherited TitleLabel: TLabel
              Caption = 'Size'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Min = 1
              PageSize = 1
              Position = 10
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = GrainGeneratorTrackBarNotifyWidgetClick
            end
          end
          inline LowlightsTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 44
            Width = 300
            Height = 25
            Hint = 
              'Grain strength in the darker areas of the image'#13#10'Press 1 to cycl' +
              'e through grain creation settings'#13#10'Use mouse wheel or arrow keys' +
              ' to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 7
            ExplicitTop = 44
            ExplicitWidth = 300
            ExplicitHeight = 25
            inherited TitleLabel: TLabel
              Caption = 'Lowlights'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Position = 0
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = GrainGeneratorTrackBarNotifyWidgetClick
            end
          end
          inline HighlightsTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 68
            Width = 300
            Height = 25
            Hint = 
              'Grain strength in the brighter areas of the image'#13#10'Press 1 to cy' +
              'cle through grain creation settings'#13#10'Use mouse wheel or arrow ke' +
              'ys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = 7
            ExplicitTop = 68
            ExplicitWidth = 300
            ExplicitHeight = 25
            inherited TitleLabel: TLabel
              Caption = 'Highlights'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Position = 100
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = GrainGeneratorTrackBarNotifyWidgetClick
            end
          end
          inline SaturationTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 92
            Width = 300
            Height = 25
            Hint = 
              'Color intensity of the grain'#13#10'Press 1 to cycle through grain cre' +
              'ation settings'#13#10'Use mouse wheel or arrow keys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ExplicitLeft = 7
            ExplicitTop = 92
            ExplicitWidth = 300
            ExplicitHeight = 25
            inherited TitleLabel: TLabel
              Caption = 'Saturation'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Position = 0
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = GrainGeneratorTrackBarNotifyWidgetClick
            end
          end
          inline RoughnessTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 116
            Width = 300
            Height = 25
            Hint = 
              'Range of grain size variation'#13#10'Press 1 to cycle through grain cr' +
              'eation settings'#13#10'Use mouse wheel or arrow keys to modify setting' +
              's'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            ExplicitLeft = 7
            ExplicitTop = 116
            ExplicitWidth = 300
            ExplicitHeight = 25
            inherited TitleLabel: TLabel
              Caption = 'Roughness'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Position = 25
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              OnClick = GrainGeneratorTrackBarNotifyWidgetClick
            end
          end
        end
        object PresetGroupBox: TGroupBox
          Left = 316
          Top = 30
          Width = 126
          Height = 79
          Caption = 'Presets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object Preset1Button: TSpeedButton
            Left = 6
            Top = 42
            Width = 25
            Height = 25
            Hint = 'Use preset set #1'#13#10'Press 2 to cycle through presets'
            GroupIndex = 1
            Caption = '1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = PresetButtonClick
          end
          object Preset2Button: TSpeedButton
            Tag = 1
            Left = 34
            Top = 42
            Width = 25
            Height = 25
            Hint = 'Use preset set #2'#13#10'Press 2 to cycle through presets'
            GroupIndex = 1
            Caption = '2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = PresetButtonClick
          end
          object SavePresetButton: TBitBtn
            Left = 66
            Top = 42
            Width = 53
            Height = 25
            Hint = 'Save current settings to selected preset (Ctrl+S)'
            Caption = 'Save'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Glyph.Data = {
              CE070000424DCE07000000000000360000002800000024000000120000000100
              18000000000098070000000000000000000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000000000000000
              000000000000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A52
              5252525252525252525252525252525252525252525252525252525252525252
              5252525252525252526A6A6A6A6A6A6A6A6A6A6A6A000000000000909090C8C8
              C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C800000052
              52526A6A6A6A6A6A6A6A6A5252525252525E5E5E6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A6A
              6A0000002A2A2A000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
              C8C8C8C8C8C8C8C89090900000006A6A6A6A6A6A6A6A6A5252525E5E5E525252
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5E5E
              5E5252526A6A6A6A6A6A6A6A6A0000003A3A3A000000C8C8C8C8C8C8C8C8C8C8
              C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C80000006A6A6A6A6A6A
              6A6A6A5252526A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A0000003A3A3A00
              0000909090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
              C8C8C80000005252526A6A6A6A6A6A5252526A6A6A5252525E5E5E6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A
              6A6A6A6A6A0000003A3A3A2A2A2A000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
              C8C8C8C8C8C8C8C8C8C8C8C8C8C8C89090900000006A6A6A6A6A6A5252526A6A
              6A5E5E5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A5E5E5E5252526A6A6A6A6A6A0000003A3A3A3A3A3A000000C8C8C8
              C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C80000
              006A6A6A6A6A6A5252526A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A000000
              3A3A3A3A3A3A0000000000000000000000000000000000000000000000000000
              000000000000000000000000006A6A6A6A6A6A5252526A6A6A6A6A6A52525252
              5252525252525252525252525252525252525252525252525252525252525252
              5252526A6A6A6A6A6A0000003A3A3A3A3A3A3A3A3A3A3A3A000000909090C8C8
              C8C8C8C8C8C8C80000003A3A3A3A3A3A0000006A6A6A6A6A6A6A6A6A6A6A6A52
              52526A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A6A6A6A6A6A6A525252
              6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A0000000000000000002A2A
              2A3A3A3A3A3A3A000000C8C8C8C8C8C8C8C8C80000000000000000000000006A
              6A6A6A6A6A6A6A6A6A6A6A5252525252525252525E5E5E6A6A6A6A6A6A525252
              6A6A6A6A6A6A6A6A6A5252525252525252525252526A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A000000000000000000909090C8C8C8C8C8C8C8C8C800
              00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              5252525252525252525E5E5E6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000909090C8
              C8C89090900000009090900000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A6A5E5E5E5252525E5E
              5E5252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A000000909090C8C8C89090900000006A6A6A0000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E6A6A
              6A5E5E5E5252526A6A6A5252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A000000909090C8C8C89090900000006A6A6A6A6A6A
              6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A5252525E5E5E6A6A6A5E5E5E5252526A6A6A6A6A6A6A6A6A5252526A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A000000909090
              0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525E5E5E5252526A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A52
              52526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
            NumGlyphs = 2
            ParentFont = False
            TabOrder = 0
            TabStop = False
            OnClick = SavePresetButtonClick
          end
          object ProjectRadioButton: TRadioButton
            Left = 7
            Top = 17
            Width = 53
            Height = 17
            Hint = 'Use project-wide presets'#13#10'Press 1 to toggle to clip presets'
            Caption = 'Project'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            TabStop = True
            OnClick = PresetRadioButtonClick
          end
          object ClipRadioButton: TRadioButton
            Left = 73
            Top = 17
            Width = 38
            Height = 17
            Hint = 'Use clip specific presets'#13#10'Press 1 to toggle to project presets'
            Caption = 'Clip'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = PresetRadioButtonClick
          end
        end
        object BrushGroupBox: TGroupBox
          Left = 6
          Top = 180
          Width = 443
          Height = 283
          Caption = ' Brush '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          object RectSpeedButton: TSpeedButton
            Left = 372
            Top = 115
            Width = 45
            Height = 25
            Hint = 'Rectangular brush'
            GroupIndex = 20
            Caption = 'Rect'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = onBrushParameterChange
          end
          object EllipseSpeedButton: TSpeedButton
            Left = 330
            Top = 115
            Width = 44
            Height = 25
            Hint = 'Round brush'
            GroupIndex = 20
            Caption = 'Ellipse'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = onBrushParameterChange
          end
          object RepeatStrokesSpeedButton: TSpeedButton
            Left = 197
            Top = 199
            Width = 66
            Height = 25
            Hint = 
              'Execute macro (Shift+G)'#13#10'apply pending strokes to all frames in ' +
              'the marked range'
            AllowAllUp = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Glyph.Data = {
              36070000424D360700000000000036000000280000002A0000000E0000000100
              18000000000000070000000000000000000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6262623A3A3A0000000000000000
              000000003A3A3A6262626A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A626262525252525252525252
              5252526262626A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6262620000000000000000000000000000
              000000000000000000006262626A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A525252525252525252525252525252
              5252525252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6262620000000000000000003A3A3A6262626262
              623A3A3A0000000000000000006262626A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252526262626A6A6A6A6A6A
              6262625252525252525252526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000006262626A6A6A6A6A6A6A6A
              6A6A6A6A6262620000000000000000006262626A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A5252525252525252526A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A5252525252525252526A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A3A3A3A0000000000006262626A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6262620000000000000000006262626A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6262625252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A5252525252525252526A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6262620000000000003A3A3A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A5252525252526262626A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A5252525252526262626A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A0000000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A3A3A3A0000000000006A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A5252525252526A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6262625252525252526A6A6A00006A6A6A3A3A3A
              0000000000000000000000000000000000000000000000003A3A3A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000000000006A6A6A6A6A6A5A5A5A52
              52525252525252525252525252525252525252525252525A5A5A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252526A6A6A00006A6A6A6A6A6A
              3A3A3A0000000000000000000000000000000000003A3A3A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5A
              5A5A5252525252525252525252525252525252525A5A5A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A3A3A3A0000000000000000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A5A5A5A5252525252525252525252525A5A5A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A3A3A3A0000000000003A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A5A5A5A5252525252525A5A5A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A3A3A3A3A3A3A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A5A5A5A5A5A5A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
            NumGlyphs = 2
            ParentFont = False
            OnClick = RepeatStrokesSpeedButtonClick
          end
          object PendingStrokesLabel: TLabel
            Left = 57
            Top = 148
            Width = 89
            Height = 14
            Caption = 'Pending Strokes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object BrushPreviewPaintBox: TImage
            Left = 329
            Top = 20
            Width = 89
            Height = 89
          end
          inline BlendTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 44
            Width = 300
            Height = 22
            Hint = 
              'Brush edge blend area size'#13#10'Press 8 to select'#13#10'Use mouse wheel o' +
              'r arrow keys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 7
            ExplicitTop = 44
            ExplicitWidth = 300
            inherited TitleLabel: TLabel
              Caption = 'Blend'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              PageSize = 1
              Position = 25
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              Font.Height = -12
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              Font.Height = -12
              OnClick = onBrushParameterChange
            end
          end
          inline OpacityTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 67
            Width = 300
            Height = 22
            Hint = 
              'Brush opacity'#13#10'Press 0 [zero] to select'#13#10'Use mouse wheel or arro' +
              'w keys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = 7
            ExplicitTop = 67
            ExplicitWidth = 300
            inherited TitleLabel: TLabel
              Caption = 'Opacity'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              PageSize = 1
              Position = 100
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              Font.Height = -12
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              Font.Height = -12
              OnClick = onBrushParameterChange
            end
          end
          inline RadiusTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 20
            Width = 300
            Height = 22
            Hint = 
              'Brush size'#13#10'Press D to select'#13#10'Use mouse wheel or arrow keys to ' +
              'modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ExplicitLeft = 7
            ExplicitTop = 20
            ExplicitWidth = 300
            inherited TitleLabel: TLabel
              Caption = 'Radius'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Max = 1000
              Min = 1
              PageSize = 1
              Position = 200
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              Font.Height = -12
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              Font.Height = -12
              OnClick = onBrushShapeChange
            end
          end
          inline AngleTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 115
            Width = 300
            Height = 22
            Hint = 
              'Brush angle'#13#10'Press = to select;'#13#10'Use mouse wheel or arrow keys t' +
              'o modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            ExplicitLeft = 7
            ExplicitTop = 115
            ExplicitWidth = 300
            inherited TitleLabel: TLabel
              Caption = 'Angle'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              Max = 90
              Min = -90
              PageSize = 1
              Position = 0
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              Font.Height = -12
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              Font.Height = -12
              OnClick = onBrushShapeChange
            end
          end
          inline AspectTrackEditFrame: TCompactTrackEditFrame
            Left = 7
            Top = 91
            Width = 300
            Height = 22
            Hint = 
              'Brush aspect ratio'#13#10'Press - [minus] to select'#13#10'Use mouse wheel o' +
              'r arrow keys to modify settings'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            ExplicitLeft = 7
            ExplicitTop = 91
            ExplicitWidth = 300
            inherited TitleLabel: TLabel
              Caption = 'Aspect'
            end
            inherited TrackBar: TTrackBar
              Width = 192
              PageSize = 1
              Position = 100
              ExplicitWidth = 192
            end
            inherited TrackBarDisabledPanel: TPanel
              Width = 180
              Font.Height = -12
              ExplicitWidth = 180
            end
            inherited EditAlignmentPanel: TPanel
              inherited Edit: TEdit
                Height = 17
                ExplicitHeight = 17
              end
            end
            inherited NotifyWidget: TCheckBox
              Font.Height = -12
              OnClick = onBrushShapeChange
            end
          end
          object BrushStrokeListBox: TListBox
            Left = 55
            Top = 168
            Width = 134
            Height = 106
            Hint = 'List of strokes that haven'#39't been accepted yet'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 5
          end
          object UndoLastButton: TButton
            Left = 269
            Top = 199
            Width = 66
            Height = 25
            Hint = 'Discard most recent stroke (A)'
            Caption = 'Undo Last'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            OnClick = UndoLastButtonClick
          end
          object RejectButton: TButton
            Left = 197
            Top = 168
            Width = 66
            Height = 25
            Hint = 'Discard all pending strokes (Shift+A)'
            Caption = 'Reject'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            OnClick = RejectButtonClick
          end
          object AcceptButton: TButton
            Left = 269
            Top = 168
            Width = 66
            Height = 25
            Hint = 'Accept all pending strokes (G)'
            Caption = 'Accept'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = AcceptButtonClick
          end
          object AutoAcceptCheckBox: TCheckBox
            Left = 346
            Top = 172
            Width = 55
            Height = 17
            Hint = 
              'Auto-accept strokes on this frame when'#13#10'a stroke is applied to a' +
              'nother frame'
            Caption = 'Auto'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 9
            OnClick = AutoAcceptCheckBoxClick
          end
        end
        object ChannelPanel: TPanel
          Left = 75
          Top = 157
          Width = 208
          Height = 29
          BevelOuter = bvNone
          TabOrder = 3
          DesignSize = (
            208
            29)
          object HilitedBlueChannelButton: TSpeedButton
            Left = 183
            Top = 4
            Width = 21
            Height = 21
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 2
            Flat = True
            Glyph.Data = {
              CE070000424DCE07000000000000360000002800000024000000120000000100
              18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
              0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
              6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
              0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
              0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
              FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
              526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
              FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000
              FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A
              52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF00
              00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
              0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A5252
              6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF
              0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FFFF
              0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
              FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52
              526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000
              FF0000FF0000FF0000FF0000FF000000F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
            Margin = 0
            NumGlyphs = 2
            Visible = False
            ExplicitLeft = 176
          end
          object HilitedGreenChannelButton: TSpeedButton
            Left = 162
            Top = 4
            Width = 21
            Height = 21
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 2
            Flat = True
            Glyph.Data = {
              CE070000424DCE07000000000000360000002800000024000000120000000100
              18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
              B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
              526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
              B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
              B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
              4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
              52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
              4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB122
              4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A5252
              6A52526A52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1
              224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52
              526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224C
              B1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52
              526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224C
              B1224CB1224CB1224CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF4C
              B1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB122
              4CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A526A52526A52526A52526A
              52526A52526A52526A52526A52526A52526A52526A52526A526A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF4CB1224CB1224CB1224CB1224CB1224CB1224CB122
              4CB1224CB1224CB1224CB1224CB12200F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A526A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
            Margin = 0
            NumGlyphs = 2
            Visible = False
            ExplicitLeft = 155
          end
          object HilitedRedChannelButton: TSpeedButton
            Left = 141
            Top = 3
            Width = 21
            Height = 21
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 2
            Flat = True
            Glyph.Data = {
              CE070000424DCE07000000000000360000002800000024000000120000000100
              18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
              1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
              52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
              1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
              1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
              241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
              6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
              241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED
              241CED241CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF241CED241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52
              526A52526A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241C
              ED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED241CED24
              1CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A
              52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED24
              1CED241CED241CED241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF24
              1CED241CED241CED241CED241CED241CED241CED241CED241CED241CED241CED
              241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A52526A52526A52526A5252
              6A52526A52526A52526A52526A52526A52526A52526A52526A6A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF241CED241CED241CED241CED241CED241CED241CED
              241CED241CED241CED241CED241CED00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A52526A52526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
            Margin = 0
            NumGlyphs = 2
            Visible = False
            ExplicitLeft = 134
          end
          object HilitedGrayChannelButton: TSpeedButton
            Left = 106
            Top = 4
            Width = 21
            Height = 21
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 2
            Flat = True
            Glyph.Data = {
              CE070000424DCE07000000000000360000002800000024000000120000000100
              18000000000098070000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A000000
              0000000000000000000000000000000000000000000000000000000000000000
              006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000000000000000
              000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000
              000000000000000000000000000000000000000000000000006A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A00000000000000000000000000000000000000000000000000000000000000
              00000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A00000000000000000000000000000000000000
              00000000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A00000000000000
              00000000000000000000000000000000000000000000000000000000006A6A6A
              6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A
              6A6A6A6A6A000000000000000000000000000000000000000000000000000000
              0000000000000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00
              F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A000000000000000000000000000000
              0000000000000000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A
              6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A000000
              0000000000000000000000000000000000000000000000000000000000000000
              006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A
              6A6A6A6A6A6A6A6A6A0000000000000000000000000000000000000000000000
              000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A0000000000000000000000
              000000000000000000000000000000000000000000000000006A6A6A6A6A6A6A
              6A6A6A6A6A00F2FF00F2FF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A
              6A00000000000000000000000000000000000000000000000000000000000000
              00000000006A6A6A6A6A6A6A6A6A6A6A6A00F2FF00F2FF00F2FF00F2FF00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00F2FF
              00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2FF00F2
              FF00F2FF00F2FF00F2FF00F2FF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A}
            Margin = 0
            NumGlyphs = 2
            Visible = False
            ExplicitLeft = 104
          end
          object BlueChannelButton: TSpeedButton
            Tag = 3
            Left = 183
            Top = 4
            Width = 21
            Height = 21
            Hint = 'Process the BLUE channel'
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 3
            Down = True
            Glyph.Data = {
              B60A0000424DB60A00000000000036000000280000002A000000150000000100
              180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6AFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF0000FF0000FF00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A5252
              6A52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
            Margin = 0
            NumGlyphs = 2
            OnClick = ChannelButtonClick
            ExplicitLeft = 176
          end
          object GreenChannelButton: TSpeedButton
            Tag = 2
            Left = 162
            Top = 4
            Width = 21
            Height = 21
            Hint = 'Process the GREEN channel'
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 2
            Down = True
            Glyph.Data = {
              B60A0000424DB60A00000000000036000000280000002A000000150000000100
              180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A4CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1224CB1
              224CB1224CB1224CB1224CB1226A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A526A52526A52526A52526A52526A52526A52526A52526A52526A52
              526A52526A52526A52526A526A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
            Margin = 0
            NumGlyphs = 2
            OnClick = ChannelButtonClick
            ExplicitLeft = 155
          end
          object RedChannelButton: TSpeedButton
            Tag = 1
            Left = 141
            Top = 4
            Width = 21
            Height = 21
            Hint = 'Process the RED channel'
            AllowAllUp = True
            Anchors = [akTop, akRight]
            GroupIndex = 1
            Down = True
            Glyph.Data = {
              B60A0000424DB60A00000000000036000000280000002A000000150000000100
              180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A241CED241CED241CED241CED241CED241CED241CED241CED241C
              ED241CED241CED241CED241CED6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A52526A52526A52526A52526A52526A52526A52526A52526A52526A
              52526A52526A52526A52526A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
            Margin = 0
            NumGlyphs = 2
            OnClick = ChannelButtonClick
            ExplicitLeft = 134
          end
          object AllChannelsButton: TSpeedButton
            Left = 106
            Top = 4
            Width = 21
            Height = 21
            Hint = 'Click to select all channels for processing'
            Anchors = [akTop, akRight]
            GroupIndex = 4
            Down = True
            Glyph.Data = {
              B60A0000424DB60A00000000000036000000280000002A000000150000000100
              180000000000800A0000130B0000130B000000000000000000006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A2528E22528E22528E22528E254BB2254BB2254BB2254BB22FE00
              11FE0011FE0011FE0011FE00116A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A10127810127810127810127834731534731534731534731588000B
              88000B88000B88000B88000B6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
              6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
            Margin = 0
            NumGlyphs = 2
            OnClick = AllChannelsButtonClick
            ExplicitLeft = 104
          end
          object ChannelLabel: TLabel
            Left = 6
            Top = 8
            Width = 88
            Height = 13
            Caption = 'Process Channels:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
      end
    end
    object EnableGroupBox: TGroupBox
      Left = 38
      Top = 563
      Width = 199
      Height = 61
      Caption = 'Enable'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      TabStop = True
      object ReduceGrainLabel: TLabel
        Left = 35
        Top = 18
        Width = 64
        Height = 13
        Hint = 'Enable grain reduction'#13#10'Press 4 to cycle through check boxes'
        Caption = 'Reduce Grain'
      end
      object CreateGrainLabel: TLabel
        Left = 35
        Top = 37
        Width = 61
        Height = 13
        Hint = 'Enable grain creation'#13#10'Press 4 to cycle through check boxes'
        Caption = 'Create Grain'
      end
      object UseBrushLabel: TLabel
        Left = 134
        Top = 37
        Width = 48
        Height = 13
        Hint = 'Use brush for adding grain to parts of the frame'
        Caption = 'Use Brush'
      end
      object ReduceCheckBox: TCheckBox
        Left = 16
        Top = 16
        Width = 18
        Height = 17
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = ReduceCheckBoxClick
        OnEnter = ReduceCheckBoxEnter
        OnExit = ReduceCheckBoxExit
      end
      object CreateCheckBox: TCheckBox
        Left = 16
        Top = 35
        Width = 18
        Height = 17
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CreateCheckBoxClick
        OnEnter = CreateCheckBoxEnter
        OnExit = CreateCheckBoxExit
      end
      object UseBrushCheckBox: TCheckBox
        Left = 115
        Top = 35
        Width = 17
        Height = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = UseBrushCheckBoxClick
        OnEnter = UseBrushCheckBoxEnter
        OnExit = UseBrushCheckBoxExit
      end
    end
    object TargetViewGroupBox: TGroupBox
      Left = 306
      Top = 563
      Width = 165
      Height = 61
      Caption = ' Target View '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      object ShowOriginalSpeedButton: TSpeedButton
        Left = 13
        Top = 23
        Width = 60
        Height = 21
        Hint = 
          'Show pixels from before any pending strokes'#13#10'Press T to toggle t' +
          'o Processed'
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Original'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = ShowOriginalSpeedButtonClick
      end
      object ShowProcessedSpeedButton: TSpeedButton
        Left = 85
        Top = 23
        Width = 60
        Height = 21
        Hint = 
          'Show pixels after all pending strokes are applied'#13#10'Press T to to' +
          'ggle to Original'
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Processed'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = ShowProcessedSpeedButtonClick
      end
    end
  end
  object RefreshTimer: TTimer
    Interval = 200
    OnTimer = RefreshTimerTimer
    Left = 412
    Top = 20
  end
end
