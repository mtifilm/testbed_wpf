#ifndef GrainPresetsH
#define GrainPresetsH

#include "GrainCoreApi.h"
#include "GrainCoreCommon.h"
#include "machine.h"

MTI_GRAINCORE_API class GrainPresets
{
public:

   GrainPresets();
   ~GrainPresets();

   void SetEnabled(bool flag);
   bool IsEnabled();

   void SetSelectedBase(GrainParameterBaseType presetsBase);
   GrainParameterBaseType GetSelectedBase();

   void SetSelectedIndex(int presetIndex);
   int GetSelectedIndex();

   // Call this from the tools "onNewClip" callback
   void SetClipData(const string &clipName, int frameWidth, int frameHeight, int bitsPerComponent, bool dataIsUnsignedShorts);

   const float *GetNormalizedGrainValues(const unsigned short* imageData, int frameNumber);

   CBHook GrainPresetsStateChange;
   CBHook PresetGrainValuesChange;

private:

   GrainGeneratorModel *GetGrainGeneratorModel();

   void SetFrameData(const unsigned short* imageData, int frameNumber);
	DEFINE_CBHOOK(HandleGrainPresetsChange, GrainPresets);
	DEFINE_CBHOOK(HandleGrainValuesChange, GrainPresets);

   string _currentClipName;
   bool _isEnabled = false;
   int _bitsPerComponent = -1;
   unsigned int _seedBase = 0;
	int _currentFrameNumber = -1;
   bool _dataIsUnsignedShorts = true;
};

#endif
