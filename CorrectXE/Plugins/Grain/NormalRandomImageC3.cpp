// ---------------------------------------------------------------------------
#include <stdlib.h>
#include <time.h>

#include "GrainCoreApi.h"
#include "IniFile.h"
#include "Ippheaders.h"
#include "NormalRandomImageC3.h"
#include "SynchronousThreadRunner.h"

// ---------------------------------------------------------------------------
// RGB -> YCbCr coefficients are used to compute standard deviations based one
// standard deviation of 1.0 for R, G, and B. This lets us generate gaussian
// random numbers directly in YCbCr colorspace, eliminating the need to
// convert from random R, G and B values to Y, Cb, Cr for applying saturation.
// If we want to support settable mean and std deviation, this will have to be
// computed dynamically, for now these assume mean=0 and stddev=1.
//
// Y StdDev  = SQRT(.299**2 + .587**2 + .114**2) = 0.66855515853218872609219713762224
// Cb StdDev = SQRT((-.169**2) + (-.331**2) + .499**2) = 0.62219209252448716732075824206702
// CR StdDev = SQRT(.499**2 + (-.418**2) + (-.0813**2)) = 0.65599900152363036961836107736187
//
const float StdDevs[3] = { 0.668555, 0.622192, 0.655999 };

const unsigned int componentSeedOffsetMultiplier = 100000000;  // one hundred million
const unsigned int sliceSeedOffsetMultiplier = 1000000;       // one million
const int Y_INDEX = 0;
const int Cb_INDEX = 1;
const int Cr_INDEX = 2;
const int NumberOfSlices = 24;

#ifdef _DEBUG
//#define NO_MULTI
#endif
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

namespace
{

struct RandomValueGenerationSliceParams
{
   Ipp32f *outputValues;
   int totalLen;
   vector<IppsRandGaussState_32f *> *randGaussStates;
};
// ---------------------------------------------------------------------------

int GenerateRandomValuesForOneSlice(void *vp, int sliceNumber, int totalSlices)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<RandomValueGenerationSliceParams *>(vp);
   MTIassert(sliceNumber < params->randGaussStates->size());

   // Compute length of this slice.
   // Evenly distribute n extra values over the first n slices.
   auto nominalSliceLen = params->totalLen / totalSlices;
   auto extraLen = params->totalLen % totalSlices;
   auto sliceStartOffset = (nominalSliceLen * sliceNumber) + std::min<int>(extraLen, sliceNumber);
   auto sliceLen = nominalSliceLen + ((sliceNumber < extraLen) ? 1 : 0);

   auto slicePtr = params->outputValues + sliceStartOffset;
   auto &randGaussStates = *(params->randGaussStates);
   IppStatus retVal = ippsRandGauss_32f(slicePtr, sliceLen, randGaussStates[sliceNumber]);

   MTIassert(retVal == 0);
   return retVal;
}
// ---------------------------------------------------------------------------

void generateRandomValuesForOnePlane(
   Ipp32f *outputValues,
   IppiSize size,
   vector<IppsRandGaussState_32f *> randGaussStates)
{
   RandomValueGenerationSliceParams params = { outputValues, MtiSize(size).area(), &randGaussStates };
#ifdef NO_MULTI
   GenerateRandomValuesForOneSlice(&params, 0, 1);
#else
   SynchronousThreadRunner multithread(randGaussStates.size(), &params, GenerateRandomValuesForOneSlice);
   multithread.Run();
#endif
}
// ---------------------------------------------------------------------------

struct SaturateImageParams
{
   float saturation;
   Ipp32f *randomYValues;
   Ipp32f *randomCbValues;
   Ipp32f *randomCrValues;
   Ipp32fArray *saturatedRandomImage;
};
// ---------------------------------------------------------------------------

int SaturateImageForOneSlice(void *vp, int sliceNumber, int totalSlices)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<SaturateImageParams *>(vp);
   auto &saturatedRandomImage = *(params->saturatedRandomImage);

   // Compute length of this slice.
   // Evenly distribute n extra rows over the first n slices.
   auto totalRows = saturatedRandomImage.getHeight();
   auto nominalSliceRows = totalRows / totalSlices;
   auto extraRows = totalRows % totalSlices;
   auto sliceStartRow = (nominalSliceRows * sliceNumber) + std::min<int>(extraRows, sliceNumber);
   auto sliceRows = nominalSliceRows + ((sliceNumber < extraRows) ? 1 : 0);

   auto randomNumberStartOffset = sliceStartRow * saturatedRandomImage.getWidth();

   if (params->saturation == 0.F)
   {
      // Special case 0 saturation - all three RGB planes get Y!
	   auto randomYValuePtr = params->randomYValues + randomNumberStartOffset;
      IppiSize roiSize = { saturatedRandomImage.getWidth(), sliceRows };
      auto pDst = saturatedRandomImage.getRowPointer(sliceStartRow);
      int srcStep = saturatedRandomImage.getWidth() * sizeof(Ipp32f);
      int dstStep = saturatedRandomImage.getRowPitchInBytes();

//  ippiGrayToRGB is defined in a header, but is not found in the library!!
//      IppStatus status = ippiGrayToRGB_32f_C1C3R(randomYValuePtr, srcStep, pDst, dstStep, roiSize);
		auto stupidSrcArray = new Ipp32f*[3] { randomYValuePtr, randomYValuePtr, randomYValuePtr };
		IppStatus status = ippiCopy_32f_P3C3R(stupidSrcArray, srcStep, pDst, dstStep, roiSize);
      if (status != 0)
      {
         return int(status);
      }
   }
   else
   {
      auto randomValueIndex = randomNumberStartOffset;
      auto len = saturatedRandomImage.getWidth() * 3;
      auto sliceStopRow = sliceStartRow + sliceRows;
      for (auto row = sliceStartRow; row < sliceStopRow; ++row)
      {
         auto pOut = saturatedRandomImage.getRowPointer(row);
         for (auto i = 0; i < len; i += 3)
         {
            auto CbSat = params->saturation * params->randomCbValues[randomValueIndex];
            auto CrSat = params->saturation * params->randomCrValues[randomValueIndex];
            auto randomYValue = params->randomYValues[randomValueIndex++];

            pOut[i] = randomYValue + CrSat;                                   // R
            pOut[i + 1] = randomYValue - (0.1942 * CbSat) - (0.5092 * CrSat); // G
            pOut[i + 2] = randomYValue + CbSat;                               // B
         }
      }
   }

   return 0;
}

};
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// NOTE: Settable mean & stddev were removed so are not supported now.
// They are hard-coded to 0.0 and 1.0.
// ---------------------------------------------------------------------------

NormalRandomImageC3::NormalRandomImageC3(IppiSize size, const unsigned int *seedPtr)
	 : _size(MtiSize(size, 3))  // Settable mean & stddev not supported now
{
//   DBTIMER(NormalRandomImageC3_CONSTRUCTOR);

   // Where to put the Y / Cb / Cr random numbers.
   MTIassert(_size.depth == 3);
   _randomNumberVector = ippsMalloc_32f(_size.volume());
   _randomYValues = _randomNumberVector;
   _randomCbValues = _randomYValues + _size.area();
   _randomCrValues = _randomCbValues + _size.area();
   _needToGenerateRandomNumbersForY = true;
   _needToGenerateRandomNumbersForCbCr = true;

   // The Ipp32fArray that will be output.
   // Because of an Embarcadero bug, we can't combine both line
   auto tempArray = Ipp32fArray(_size);
   _saturatedRandomImage = tempArray;
   _imageSaturationPercent = 0.F;

   // Need one each of these randGauss state objects per array slice.
   // They are small (72 bytes) and take virtually no time to initialize (~0.02 msec).
	int randGaussStateSize;
	IppThrowOnError(ippsRandGaussGetSize_32f(&randGaussStateSize));
   for (int i = 0; i < NumberOfSlices; ++i)
   {
	   _pRandGaussStates[Y_INDEX].push_back((IppsRandGaussState_32f *) ippsMalloc_8u(randGaussStateSize));
	   _pRandGaussStates[Cb_INDEX].push_back((IppsRandGaussState_32f *) ippsMalloc_8u(randGaussStateSize));
	   _pRandGaussStates[Cr_INDEX].push_back((IppsRandGaussState_32f *) ippsMalloc_8u(randGaussStateSize));
   }

   // Compute the base seed - actual seeds used will be offset by the type of
	// value (y, Cb or Cr) and slice number.
	unsigned int seed;
	if (seedPtr == nullptr)
	{
		//std::srand(std::time(NULL));   // haha - NO - granularity is SECONDS!!
		static int seedBase = std::time(NULL);
		std::srand(seedBase);
		seedBase += 17;
		//
		seed = std::rand();
	}
	else
	{
		seed = *seedPtr;
   }

	setSeed(seed);
}
// ---------------------------------------------------------------------------

NormalRandomImageC3::~NormalRandomImageC3()
{
   for (auto i = 0; i < 3; ++i)
   {
      for (auto randGaussState : _pRandGaussStates[i])
      {
         ippsFree(randGaussState);
      }
   }

   ippsFree(_randomNumberVector);     // QQQ maybe cache these!
}
// ---------------------------------------------------------------------------

void NormalRandomImageC3::setSeed(unsigned int newSeedBase)
{
//   DBTIMER(SET_RANDOM_IMAGE_SEED);

	if (newSeedBase == _seedBase)
	{
      // Nothing to do!
      return;
   }

   _seedBase = newSeedBase;
   invalidate();

   // for each plane (Y, Cb, Cr)...
   for (auto i = 0; i < 3; ++i)
   {
      unsigned int componentSeedOffset = (i + 1) * componentSeedOffsetMultiplier;

      // for each slice...
      for (auto j = 0; j < _pRandGaussStates[i].size(); ++j)
      {
         // Reinitialize the rand gauss state.
         unsigned int sliceSeedOffset = j * sliceSeedOffsetMultiplier;
         unsigned int seed = componentSeedOffset + sliceSeedOffset + _seedBase;
         IppThrowOnError(
            ippsRandGaussInit_32f(_pRandGaussStates[i][j], _mean, StdDevs[i], seed)
         );
      }
   }
}
// ---------------------------------------------------------------------------

void NormalRandomImageC3::invalidate()
{
   _needToGenerateRandomNumbersForY = true;
   _needToGenerateRandomNumbersForCbCr = true;
   _imageSaturationPercent = 0.F;
}
// ---------------------------------------------------------------------------

Ipp32fArray NormalRandomImageC3::getRandomImage(float saturationPercent)
{
   // Short circuit if nothing changed.
   if (!_needToGenerateRandomNumbersForY
   && !_needToGenerateRandomNumbersForCbCr
   && saturationPercent == _imageSaturationPercent)
   {
      DBCOMMENT("getRandomImage SHORT CIRCUIT");
      return _saturatedRandomImage;
   }

   {

//   DBTIMER(COMPUTE_RANDOM_YCbCr_VALUES);

   // Compute random Y's separately, in case saturation == 0.
   if (_needToGenerateRandomNumbersForY)
   {
//      DBTIMER(GENERATE_RANDOM_VALUES_FOR_Y);

      generateRandomValuesForOnePlane(_randomYValues, IppiSize(_size), _pRandGaussStates[Y_INDEX]);
      _needToGenerateRandomNumbersForY = false;
   }
   }

   // Compute random Cb's and Cr's if needed.
   if (saturationPercent > 0.F && _needToGenerateRandomNumbersForCbCr)
   {
      {
//      DBTIMER(GENERATE_RANDOM_VALUES_FOR_CbCr);

      generateRandomValuesForOnePlane(_randomCbValues, IppiSize(_size), _pRandGaussStates[Cb_INDEX]);
      generateRandomValuesForOnePlane(_randomCrValues, IppiSize(_size), _pRandGaussStates[Cr_INDEX]);
      }
   }

   {
//   DBTIMER(CONSTRUCT_SATURATED_RANDOM_IMAGE_ARRAY);

   SaturateImageParams params = { saturationPercent / 100.F, _randomYValues, _randomCbValues, _randomCrValues, &_saturatedRandomImage };
#ifdef NO_MULTI
   SaturateImageForOneSlice(&params, 0, 1);
#else
   SynchronousThreadRunner multithread(&params, SaturateImageForOneSlice);
   multithread.Run();
#endif
   }

   _imageSaturationPercent = saturationPercent;

   return _saturatedRandomImage;
}
// ---------------------------------------------------------------------------

