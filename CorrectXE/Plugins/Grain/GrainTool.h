#ifndef GrainToolH
#define GrainToolH

#include "ToolObject.h"
#include "IniFile.h"
#include "GrainReduction_MT.h"
#include "GrainCoreCommon.h"
#include "GrainToolParameters.h"
#include "RegionOfInterest.h"
#include "HRTimer.h"

//////////////////////////////////////////////////////////////////////

// Grain Tool Number (16-bit number that uniquely identifies this tool)
#define GRAIN_TOOL_NUMBER    12

// Grain Tool Command Numbers
#define GRN_CMD_NOOP                             0
#define GRN_CMD_CYCLE_PARAMETERS_NEXT            1
#define GRN_CMD_CYCLE_PARAMETERS_PREV            2
#define GRN_CMD_TOGGLE_CONTRAST_GANG             3
#define GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT     4
#define GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_PREV     5
#define GRN_CMD_TOGGLE_FIX                       6
#define GRN_CMD_THREAD_CONTROL                   7
#define GRN_CMD_CLEAR_PENDING_STROKES            8
#define GRN_CMD_CREATE_SAVE_PRESETS              9
#define GRN_CMD_START_BRUSHING                  10
#define GRN_CMD_STOP_BRUSHING                   11
#define GRN_CMD_START_ERASING                   12
#define GRN_CMD_STOP_ERASING                    13
#define GRN_CMD_HANDLE_D_KEY_DOWN               14   // D is overloaded
#define GRN_CMD_HANDLE_D_KEY_UP                 15   // D is overloaded
#define GRN_CMD_SELECT_BRUSH_RADIUS             16   // D
#define GRN_CMD_SELECT_BRUSH_OPACITY            17
#define GRN_CMD_SELECT_BRUSH_BLEND              18
#define GRN_CMD_SELECT_BRUSH_ASPECT             19
#define GRN_CMD_SELECT_BRUSH_ANGLE              20
#define GRN_CMD_CYCLE_ENABLE_CHECK_BOX_NEXT     21
#define GRN_CMD_CYCLE_ENABLE_CHECK_BOX_PREV     22
#define GRN_CMD_REPEAT_BRUSH_STROKES            23
#define GRN_CMD_HANDLE_A_KEY_DOWN               24   // A is overloaded
#define GRN_CMD_UNDO_ONE_PENDING_STROKE         25   // A
#define GRN_CMD_CLEAR_FULL_FRAME_PREVIEW        26   // A


// WTF? Why don't these get their own numbers?? QQQ
#define GRN_CMD_CREATE_TOGGLE_CLIP_PROJECT GRN_CMD_CYCLE_TEMPORAL_ANALYSIS_NEXT
#define GRN_CMD_CREATE_CYCLE_PRESETS       GRN_CMD_TOGGLE_CONTRAST_GANG

#define GRN_MAX_CONCURRENT_FRAMES_COUNT      8  // Maximum number frames
                                                // processed in parallel

// Maximum number of input and output frames per processing iteration
#define GRN_MAX_OUTPUT_FRAMES   (GRN_MAX_CONCURRENT_FRAMES_COUNT)
#define GRN_MAX_ADJACENT_FRAMES 2
#define GRN_MAX_INPUT_FRAMES (GRN_MAX_OUTPUT_FRAMES + 2*GRN_MAX_ADJACENT_FRAMES)


//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

//////////////////////////////////////////////////////////////////////

class CGrainTool : public CToolObject
{
public:
   CGrainTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CGrainTool();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool DoesToolPreserveHistory();
   virtual CToolProcessor* makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr);

   // Tool GUI
   virtual bool toolHideQuery();
   virtual int toolHide();    // Hide the tool's GUI
   virtual int toolShow();    // Show the tool's GUI
   virtual bool IsShowing(void);
   virtual int TryToResolveProvisional();

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onNewMarks();
   virtual int onChangingFraming();
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onRedraw(int frameIndex);
   virtual int onHeartbeat();
   virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);
   virtual int onSwitchSubtool();
   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);
   virtual void NotifyGOVDone();

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);

   // Tool Configuration Save and Restore
   void ReadSettings(CIniFile *iniFile, const string &section);
   void WriteSettings(CIniFile *iniFile, const string &section);

   CBHook ClipChange;            // Needed to call back on a change
   CBHook MarksChange;           // Needed to call back on a change
   CBHook BrushStrokesChange;

   GrainGeneratorModel *GetGrainGeneratorModel();

   void SetBrushActive(bool flag);
	void setBrushParameters(int newMaxRadius, int newRadius, int newBlend, int newOpacity,
		int newAspect, int newAngle, bool newShape, EBrushType newType = BT_SMOOTH);
   void GetBrushStrokes(BrushStrokes &whereToCopyTo);
   void SetAutoAccept(bool onOffFlag);
   void AcceptBrushStrokes(bool goToFrame);
   void RejectBrushStrokes(bool goToFrame);
   bool AreBrushStrokesPending();
   void CenterBrushOnFrame();
   void UndoLastBrushStroke();
   bool RepeatBrushStrokes();
   bool AreThereRepeatableStrokes();

   string GrainTime;

private:
   // Tool Processing methods (virtual in CToolObject)
   int RunFrameProcessing(int newResumeFrame);
   int ProcessSingleFrame(EToolSetupType setupType);
   int StopFrameProcessing();
   void UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag);
   void DestroyAllToolSetups(bool notIfPaused);
   //
   
   void DestroyToolSetup(int &setupHandle, bool notIfPaused);
   void GatherParameters(CGrainToolParameters &toolParams);

   bool StartBrushStroke(bool isPainting);
   bool UpdateBrushStroke();
   bool EndBrushStroke();
   bool AutoAcceptBrushStrokes();
   void SetUpMultiFrameStroking();

   // GUI methods
   bool SendArrowsKeysToTrackBar(int commandNumber);

////   bool showProc;    ???
////   bool renderFlag;  wtf - hides ToolObject::renderFlag and is never set!!

   GrainGeneratorModel *_grainGeneratorModel;

   #define BRUSH_STROKE_TOOL_PROCESSOR_TYPE 0
   #define FULL_FRAME_TOOL_PROCESSOR_TYPE 1
   int _toolProcessorTypeToMake;

   enum BrushStatusType
   {
      BRUSH_IS_INVISIBLE,
      BRUSH_IS_IDLE,
      BRUSH_IS_PAINTING,
      BRUSH_IS_ERASING,
   };

   // Brush Strokes
   BrushStatusType _brushState;
   int _brushCenterX;
   int _brushCenterY;
   int brushShape;     // 0 == circle, 1 == square
   int brushRadius;
   int brushAspectRatio;
   int brushAngle;
   CFloatBrush *_brush;
   bool _hideBrushTemporarily;
   BrushStrokes _brushStrokeStash;

   bool _needToRunTheStrokeProcessor;
   bool _needToAcceptBrushStrokes;
   bool _autoAcceptingBrushStrokes;
   int _lastStrokedFrameIndex;
   int _previousBrushStrokeX;
   int _previousBrushStrokeY;
   bool _multiFrameStrokingInProgress;

public:
   CGrainToolParameters grainParams;

private:
   int _toolSetupHandle;            // Init to -1
   int _trainingToolSetupHandle;    // Init to -1
   int _brushStrokeToolSetupHandle; // Init to -1

   string IniFileName;
   bool _clipChangedWhileWeWereSleeping;

   // thread control
   int concurrentFrameCount;
   int processingThreadsPerFrame;
   bool doRowSlices;
   int slicesPerThread;
   bool noopProcessing;
   int oldConcurrentFrameCount;
   int oldProcessingThreadsPerFrame;
   bool oldDoRowSlices;
   int oldSlicesPerThread;
   bool oldNoopProcessing;
};

extern CGrainTool *GGrainTool;    // Global from GrainToolPlugin.cpp

//////////////////////////////////////////////////////////////////////

#endif // !defined(GRAINTOOL_H)


