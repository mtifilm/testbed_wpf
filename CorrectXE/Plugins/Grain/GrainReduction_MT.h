//
// File: GrainReduction_MT.h
//
//    Multithreaded interface for noise/grain reduction algorithms
//    See GrainReduction.h for more details.
//
//    John Starr, September 23, 2003
//
// ----------------------------------------------------------------------------
#ifndef GrainReduction_MTH
#define GrainReduction_MTH

#include "GrainReduction.h"
#include "mthread.h"

// ----------------------------------------------------------------------------

/***** class CGrainReduction_MT: Multithreaded Noise and Grain Reduction
 * Notes:
 * (1) setImageSize() is an alternative to the base class (CRepair)
 *     method setImageDimension(). It ensures that the image dimensions
 *     are correctly set to those of the entire image.
 * (2) See the CRepair interface for other key  methods. 
 *******/

// ----------------------------------------------------------------------------

class CGrainReduction_MT
{
public:
  CGrainReduction_MT();
  ~CGrainReduction_MT();

  // NOTE!
  // mbraca added the last arg bNoAssemblerHack to tell the algorithm to
  // not use assembler code because the assembler code does not work when
  // the data range uses the whole 16 bits!!!
  //
  void Initialize (long lNConcurrentFramesArg, long lNProcessingThreadsArg,
                   bool bDoRowSlicesArg, long lNSlicesPerThreadArg,
                   bool bNoopProcessingArg, bool bNoAssemblerHackArg);

  /* mandatory inputs: set prior to allocate() */

  void setImageSize(long lWidth, long lHeight);
  void setGrainNeighborhoodSize(long lRowRadius, long lColRadius,
                                long lTemporalSupport);
  void setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy);
  void setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy,
              long lThread);
  int setContrast(float *fpGrainContrast, long lNComponent);

  int setImageComponent (long NCom, long PrincipalCom, MTI_UINT16 *pMinValue,
                         MTI_UINT16 *pMaxValue, MTI_UINT16 *pFillValue);
  int setNFrame (long NFrame, long lThread);
  int setRepairFrame (long Frame, long lThread);
  int setDataPointer (long Frame, MTI_UINT16 *pData, long TemporalIndex,
                      long lThread);
  int setResultPointer (MTI_UINT16 *pData, long lThread);
  int setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel);
  int setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel,
                              long lThread);
  void setTempWeight(float weight);
  /* methods */

  long allocate();
  long allocate(long lThread);
  void deallocate(long lThread);
  long doGrainFilter();

private:
  CGrainReduction *agrp;            // one per thread

  MTHREAD_STRUCT msGrainReduction;

  long lNThread;
  int iProcessError;

  static void CallProcess (void *vp, int iJob);

}; /* CGrainReduction_MT */

// ----------------------------------------------------------------------------

#endif // GRAIN_REDUCTION_MT_H

