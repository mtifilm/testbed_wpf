//---------------------------------------------------------------------------

#ifndef GrainTesterUnitH
#define GrainTesterUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include "TrackEditFrameUnit.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
//---------------------------------------------------------------------------

#include "machine.h"
#include "GrainCoreCommon.h"
#include "GrainGeneratorModel.h"
#include "FloatBrush.h"

class TGrainTesterForm : public TForm
{
__published:	// IDE-managed Components
	TImage *DestImage;
	TPageControl *GrainPageControl;
	TTabSheet *CreateTabScheet;
	TTabSheet *ReduceTabSheet;
	TGroupBox *SizeGroupBox;
	TTrackEditFrame *SizeTrackEditFrame;
	TGroupBox *GrainGroupBox;
	TTrackEditFrame *LowlightsTrackEditFrame;
	TTrackEditFrame *HighlightsTrackEditFrame;
	TTrackEditFrame *SaturationTrackEditFrame;
	TTrackEditFrame *RoughnessTrackEditFrame;
	TGroupBox *PresetgroupBox;
	TSpeedButton *Preset1Button;
	TSpeedButton *Preset2Button;
	TBitBtn *SavePresetButton;
	TRadioButton *ProjectRadioButton;
	TRadioButton *ClipRadioButton;
	TGroupBox *GroupBox1;
	TLabel *SpeedLabel;
	TRadioGroup *ImageGroup;
	TButton *ToggleButton;
	TTabSheet *BrushTabSheet;
	TGroupBox *BrushGroupBox;
	TTrackEditFrame *BlendTrackEditFrame;
	TTrackEditFrame *OpacityTrackEditFrame;
	TTrackEditFrame *RadiusTrackEditFrame;
	TTrackEditFrame *AngleTrackEditFrame;
	TTrackEditFrame *AspectTrackEditFrame;
	TImage *BrushPreviewPaintBox;
	TSpeedButton *RndSpeedButton;
	TSpeedButton *SqSpeedButton;
	TButton *StampButton;
	TLabel *BrushSpeedLabel;
	TLabel *InsertSpeedLabel;
	TLabel *DrawSpeedLabel;
	TButton *Button1;
	void __fastcall ToggleButtonClick(TObject *Sender);
	void __fastcall ImageGroupClick(TObject *Sender);
	void __fastcall TrackEditFrameNotifyWidgetClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Preset1ButtonClick(TObject *Sender);
	void __fastcall ProjectRadioButtonClick(TObject *Sender);
	void __fastcall SavePresetButtonClick(TObject *Sender);
	void __fastcall RadiusTrackEditFrameNotifyWidgetClick(TObject *Sender);
	void __fastcall StampButtonClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall DestImageMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall DestImageMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall DestImageMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);


private:	// User declarations

   void DisplayImage(MTI_UINT8 * pImage);
   void ComputeDisplay(bool sourceChanged = true);
   GrainGeneratorModel *_grainGeneratorModel;
   BrushRevealer *_brushRevealer;
   BrushRevealer *_previewBrushRevealer;
   MTI_REAL32 *originalNormalized;
   MTI_REAL32 *originalWithGrainNormalized;
   MTI_UINT8* pOriginal;
   MTI_UINT8* pProcessed;
   MTI_UINT8  *_originalImage;
   MTI_REAL32 *pProcessed32f;
   MTI_UINT8  *_grayImage;
   BrushStrokes _brushStrokes;

   MTI_REAL32 *_prevewGrayImageNormalize;
   GrainGeneratorModel *_previewGrainGeneratorModel;

   bool processed;

   const GrainParameters ReadGrainParametersFromGUI() const;
   void SetGrainParametersToGUI(const GrainParameters &grainParameters);

   CFloatBrush *_brush;
   void ReadBrushParametersFromGui(CFloatBrush *brush);
   void StampBrush();
   void StampBrushOnImage(int x, int y);
   void UpdatePreviewBrush();

public:		// User declarations

   DEFINE_CBHOOK(GrainHasChanged, TGrainTesterForm);
   DEFINE_CBHOOK(PresetHasChanged, TGrainTesterForm);

	__fastcall TGrainTesterForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TGrainTesterForm *GrainTesterForm;
//---------------------------------------------------------------------------
#endif
