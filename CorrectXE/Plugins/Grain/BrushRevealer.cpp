// ---------------------------------------------------------------------------

#include "BrushRevealer.h"
#include "FloatBrush.h"
#include "Ippheaders.h"
#include "math.h"

// Fake components, cannot be changed
#define COMPONENTS 3

#define BOUNDING_BOX_RESET_RECT  RECT {99999, 99999, 0, 0}

BrushRevealer::BrushRevealer(const IppiSize &size)
 : _wholeImageSize(size)
 , _tempImage(0)
 , _maskBoundingBox(BOUNDING_BOX_RESET_RECT)
{
	this->_wholeImageMask = ippsMalloc_32f(size.width * size.height);
}

BrushRevealer::~BrushRevealer()
{
	ippsFree(this->_wholeImageMask);
	ippsFree(this->_tempImage);
}

void BrushRevealer::StampMask(CFloatBrush *brush, int col, int row, bool doErase)
{
   CAutoThreadLocker lock(this->_threadLock);

	MTI_REAL32 *brushWeights = brush->getFloatWeights();
	MTI_REAL32 *mask = this->_wholeImageMask;
	int bw = brush->getBrushWidth();
	int bh = brush->getBrushHeight();

	int startCol = col - bw / 2;
	int endCol = std::min(this->_wholeImageSize.width, col + bw / 2);

	int startRow = row - bh / 2;
	int endRow = std::min(this->_wholeImageSize.height, row + bh / 2);

	int maskStep = this->_wholeImageSize.width;
	int brushStep = bw;

	int brushRowStart = 0;
	int brushColStart = 0;

	// Now fixup if the start col or row is negative
	if (startCol < 0)
	{
		brushColStart = -startCol;
		startCol = 0;
	}

	if (startRow < 0)
	{
		brushRowStart = -startRow;
		startRow = 0;
	}

   if (startCol < this->_maskBoundingBox.left)
   {
      this->_maskBoundingBox.left = startCol;
   }

   if (startRow < this->_maskBoundingBox.top)
   {
      this->_maskBoundingBox.top = startRow;
   }

   if (endCol > this->_maskBoundingBox.right)
   {
      this->_maskBoundingBox.right = endCol;
   }

   if (endRow > this->_maskBoundingBox.bottom)
   {
      this->_maskBoundingBox.bottom = endRow;
   }

	MTI_REAL32 *brushStartPointer = brushWeights + brushColStart;
	MTI_REAL32 *maskStartPointer = mask + startCol;

	for (int r = startRow; r < endRow; r++)
	{
		MTI_REAL32 *bp = brushStartPointer + (r - startRow + brushRowStart) * brushStep;
		MTI_REAL32 *mp = maskStartPointer + r * maskStep;

      if (doErase)
      {
         for (int c = startCol; c < endCol; c++)
		 {
			auto m = std::max(0.0f, *mp - *bp++);
			*mp++ = m;
         }
      }
      else
      {
         for (int c = startCol; c < endCol; c++)
		 {
			auto m = std::min(1.0f, *bp++ + *mp);
			*mp++ = m;
         }
      }
	}
}

void BrushRevealer::AlphaBlendOver(MTI_REAL32 *primary, MTI_REAL32 *backgroundImage,
	MTI_REAL32 *destination)
{
   CAutoThreadLocker lock(this->_threadLock);

	int w = this->_wholeImageSize.width;
	int h = this->_wholeImageSize.height;

	auto pp = primary;
	auto bp = backgroundImage;
	auto mp = this->_wholeImageMask;
	auto dp = destination;

	for (int i = 0; i < w * h; i++)
	{

		float m = *mp++;

		float p = *pp++;
		float b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;
	}
}

	void BrushRevealer::AlphaBlendOver(MTI_UINT16 *primary, MTI_UINT16 *backgroundImage,
	MTI_UINT16 *destination )
{
   CAutoThreadLocker lock(this->_threadLock);

	int w = this->_wholeImageSize.width;
	int h = this->_wholeImageSize.height;

	auto pp = primary;
	auto bp = backgroundImage;
	auto mp = this->_wholeImageMask;
	auto dp = destination;

	for (int i = 0; i < w * h; i++)
	{

		float m = *mp++;

		float p = *pp++;
		float b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;
	}
}

MTI_REAL32 *BrushRevealer::AlphaBlend(MTI_REAL32 *primary, MTI_REAL32 *backgroundImage,
	MTI_REAL32 *destination)
{
   CAutoThreadLocker lock(this->_threadLock);

	int w = this->_wholeImageSize.width;
	int h = this->_wholeImageSize.height;

	if (destination == 0)
	{
		if (this->_tempImage == 0)
		{
			this->_tempImage = ippsMalloc_32f(COMPONENTS * w * h);
		}

		destination = this->_tempImage;
	}

	auto pp = primary;
	auto bp = backgroundImage;
	auto mp = this->_wholeImageMask;
	auto dp = destination;

	for (int i = 0; i < w * h; i++)
	{

		float m = *mp++;

		float p = *pp++;
		float b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;

		p = *pp++;
		b = *bp++;
		*dp++ = (m == 0) ? p : (1 - m) * p + m * b;
	}

	return destination;
}

int BrushRevealer::GetMaskSizeInBytes()
{
   CAutoThreadLocker lock(this->_threadLock);

	return sizeof(ipp32f) * this->GetMaskSizeInComponents();
}

int BrushRevealer::GetMaskSizeInPixels()
{
   CAutoThreadLocker lock(this->_threadLock);

	return this->_wholeImageSize.width*this->_wholeImageSize.height;
}

int BrushRevealer::GetMaskSizeInComponents()
{
   CAutoThreadLocker lock(this->_threadLock);

	return this->GetMaskSizeInPixels();
}

RECT BrushRevealer::GetMaskBoundingBox()
{
   CAutoThreadLocker lock(this->_threadLock);

   return this->_maskBoundingBox;
}

void BrushRevealer::ResetMask()
{
   CAutoThreadLocker lock(this->_threadLock);

	ippsSet_32f(0.0f, this->_wholeImageMask, this->GetMaskSizeInComponents());
   this->_maskBoundingBox = BOUNDING_BOX_RESET_RECT;
}

void BrushRevealer::StampMask(const BrushStroke &brushStroke)
{
   CAutoThreadLocker lock(this->_threadLock);

	CFloatBrush *brush = new CFloatBrush(brushStroke.GetBrushParameters());
//   std::vector<POINT> localPoints = brushStroke.Points;
	for (const auto &point: brushStroke.Points)
	{
		this->StampMask(brush, point.x, point.y, brushStroke.IsErasing());
	}

	delete brush;
}

void BrushRevealer::StampMask(const BrushStrokes &brushStrokes, bool resetMask)
{
   CAutoThreadLocker lock(this->_threadLock);

	if (resetMask)
	{
		this->ResetMask();
	}

	for (const auto &brushStroke : brushStrokes)
	{
		this->StampMask(brushStroke);
	}
}

// ---------------------------------------------------------------------------
