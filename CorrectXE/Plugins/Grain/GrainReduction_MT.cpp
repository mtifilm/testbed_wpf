//
// File: GrainReduction_MT.cpp
//
//    Multithreaded interface for noise/grain reduction algorithms
//    See GrainReduction.h for more details.
//
//    John Starr, September 23, 2003
// ----------------------------------------------------------------------------
#include "GrainReduction_MT.h"
#include "err_grain.h"
#include "assert.h"

// ----------------------------------------------------------------------------

CGrainReduction_MT::CGrainReduction_MT()
{
  agrp = 0;
  lNThread = 0;
}  /* CGrainReduction_MT */

CGrainReduction_MT::~CGrainReduction_MT()
{
  delete [] agrp;
  agrp = 0;
  lNThread = 0;

  if (MThreadFree (&msGrainReduction))
   {
    assert (false);
   }
}  /* ~CGrainReduction_MT */

// NOTE!
// mbraca added the last arg bNoAssemblerHack to tell the algorithm to
// not use assembler code because the assembler code does not work when
// the data range uses the whole 16 bits!!!
//
void CGrainReduction_MT::Initialize (
                   long lNConcurrentFramesArg, long lNProcessingThreadsArg,
                   bool bDoRowSlicesArg, long lNSlicesPerThreadArg,
                   bool bNoopProcessingArg, bool bNoAssemblerHackArg)
{
  if (lNConcurrentFramesArg > 0)
   {
    agrp = new CGrainReduction[lNConcurrentFramesArg];
   }

  lNThread = lNConcurrentFramesArg;
  for (int i = 0; i < lNThread; ++i)
     agrp[i].Initialize(lNProcessingThreadsArg, bDoRowSlicesArg,
                         lNSlicesPerThreadArg, bNoopProcessingArg,
                         bNoAssemblerHackArg);

  // set up the multithreading
  msGrainReduction.PrimaryFunction = &CallProcess;
  msGrainReduction.SecondaryFunction = NULL;
  msGrainReduction.CallBackFunction = NULL;
  msGrainReduction.vpApplicationData = this;
  msGrainReduction.iNThread = lNThread;
  msGrainReduction.iNJob = lNThread;

  if (MThreadAlloc (&msGrainReduction))
   {
    assert (false);
   }

}  /* Initialize */

void CGrainReduction_MT::setImageSize(long lWidth, long lHeight)
{
   for (long lThread = 0; lThread < lNThread; lThread++)
      {
      agrp[lThread].setImageSize(lWidth, lHeight);
      }
}  /* setImageSize */

int CGrainReduction_MT::setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend,
                                                MTI_UINT8 *ucpLabel)
{
   for (long lThread = 0; lThread < lNThread; lThread++)
      {
      agrp[lThread].setRegionOfInterestPtr (ucpBlend, ucpLabel);
      }

  return 0;
}  /* setRegionOfInterestPtr */

int CGrainReduction_MT::setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend,
                                                MTI_UINT8 *ucpLabel,
                                                long lThread)
{
   agrp[lThread].setRegionOfInterestPtr (ucpBlend, ucpLabel);

  return 0;
}  /* setRegionOfInterestPtr */

void CGrainReduction_MT::setGrainNeighborhoodSize(long lRowRadius,
                                                  long lColRadius,
                                                  long TemporalSupport)
{
   for (long lThread = 0; lThread < lNThread; lThread++)
      {
      agrp[lThread].setGrainNeighborhoodSize(lRowRadius, lColRadius,
                                             TemporalSupport);
      }
}  /* setGrainNeighborhoodSize */

void CGrainReduction_MT::setTempWeight(float weight)
{
   for (long lThread = 0; lThread < lNThread; lThread++)
      {
      agrp[lThread].setTempWeight(weight);
      }
}  /* setTempWeight */


void CGrainReduction_MT::setROI(long lUserULCx, long lUserULCy, long lUserLRCx,
                                long lUserLRCy)
{
   for (long lThread = 0; lThread < lNThread; lThread++)
      {
      agrp[lThread].setROI(lUserULCx, lUserULCy, lUserLRCx, lUserLRCy);
      }
}  /* setROI */

void CGrainReduction_MT::setROI(long lUserULCx, long lUserULCy, long lUserLRCx,
                                long lUserLRCy, long lThread)
{
   agrp[lThread].setROI(lUserULCx, lUserULCy, lUserLRCx, lUserLRCy);
   
}  /* setROI */

int CGrainReduction_MT::setContrast(float *fpGrainContrast, long lNComponent)
{
  int iRet;

  if (lNThread <= 0)
    return ERR_GRAIN_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = agrp[lThread].setContrast(fpGrainContrast, lNComponent);
    if (iRet)
      return iRet;
   }

  return 0;
}  /* setContrast */

long CGrainReduction_MT::allocate()
{
// TTT this allocates redundant repair masks, one for each thread.  This
//     is a waste of memory and may fail on 4K images
// NB: repair masks may not be redundant with animated masks

  long iRet;

  if (lNThread <= 0)
    return ERR_GRAIN_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = agrp[lThread].allocate();
    if (iRet)
      return iRet;
   }

  return 0;
}  /* allocate */

long CGrainReduction_MT::allocate(long lThread)
{
   long lRet;

   lRet = agrp[lThread].allocate();
   if (lRet)
      return lRet;

   return 0;
}

void CGrainReduction_MT::deallocate(long lThread)
{
  agrp[lThread].deallocate();
}

int CGrainReduction_MT::setImageComponent (long NCom, long PrincipalCom,
                                           MTI_UINT16 *pMinValue,
                                           MTI_UINT16 *pMaxValue,
                                           MTI_UINT16 *pFillValue)
{
  int iRet;

  if (lNThread <= 0)
    return ERR_GRAIN_BAD_THREAD;

  for (long lThread = 0; lThread < lNThread; lThread++)
   {
    iRet = agrp[lThread].setImageComponent(NCom, PrincipalCom, pMinValue,
                                           pMaxValue, pFillValue);
    if (iRet)
      return iRet;
   }

   return 0;
}

int CGrainReduction_MT::setDataPointer(long Frame, MTI_UINT16 *pData,
                                       long TemporalIndex, long lThread)
{
  if (lThread < 0 || lThread >= lNThread)
    return ERR_GRAIN_BAD_THREAD;   // thread index out-of-range

  return agrp[lThread].setDataPointer(Frame, pData, TemporalIndex);
}

int CGrainReduction_MT::setRepairFrame (long Frame, long lThread)
{
  if (lThread < 0 || lThread >= lNThread)
    return ERR_GRAIN_BAD_THREAD;   // thread index out-of-range

  return agrp[lThread].setRepairFrame(Frame);
}

int CGrainReduction_MT::setNFrame (long NFrame, long lThread)
{
  if (lThread < 0 || lThread >= lNThread)
    return ERR_GRAIN_BAD_THREAD;   // thread index out-of-range

  return agrp[lThread].setNFrame(NFrame);
}

int CGrainReduction_MT::setResultPointer (MTI_UINT16 *pData, long lThread)
{
  if (lThread < 0 || lThread >= lNThread)
    return ERR_GRAIN_BAD_THREAD;   // thread index out-of-range

  return agrp[lThread].setResultPointer(pData);
}

long CGrainReduction_MT::doGrainFilter()
{
  long iRet;

  if (lNThread <= 0)
    return ERR_GRAIN_BAD_THREAD;

  iProcessError = 0;

  iRet = MThreadStart(&msGrainReduction);
  if (iRet)
    return iRet;

  return iProcessError;
}  /* Process */

void CGrainReduction_MT::CallProcess (void *vp, int iJob)
{
  int iRet;
  CGrainReduction_MT *agrmt = (CGrainReduction_MT *)vp;

  iRet = agrmt->agrp[iJob].doGrainFilter();
  if (iRet!=0 && agrmt->iProcessError==0)
    agrmt->iProcessError = iRet;

}  /* CallProcess */
