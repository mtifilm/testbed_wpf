//---------------------------------------------------------------------------

#ifndef NormalRandomImageC3H
#define NormalRandomImageC3H

#include "Ippheaders.h"

//  Used to create a random RGB image with a specified saturation level,
//
//   normalImageC3 = new NormalRandomImageC3(IppiSize {1024, 760}, 0, 1.0);
//
//   This will return a pointer to the image data
//   Ipp32f* RandomImage;
//
//   To randomize image
//     RandomizeImage();
//

class NormalRandomImageC3
{
public:
	NormalRandomImageC3(IppiSize size, const unsigned int *seedPtr = nullptr);
	~NormalRandomImageC3();

	void setSeed(unsigned int newSeed);
   void invalidate();
   Ipp32fArray getRandomImage(float saturationPercent);

private:
  Ipp32f *_randomNumberVector = nullptr;
  Ipp32f *_randomYValues = nullptr;
  Ipp32f *_randomCbValues = nullptr;
  Ipp32f *_randomCrValues = nullptr;
  bool _needToGenerateRandomNumbersForY = true;
  bool _needToGenerateRandomNumbersForCbCr = true;

  Ipp32fArray _saturatedRandomImage;
  float _imageSaturationPercent = 0.F;

  vector<IppsRandGaussState_32f *> _pRandGaussStates[3]; // (Y, Cb, Cr) x slices

	unsigned int _seedBase = 0;
	MtiSize _size;
	double _mean = 0.0;
	double _stddev = 1.0;
};


//---------------------------------------------------------------------------
#endif
