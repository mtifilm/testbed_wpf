//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainToolBrushStrokeProc.h"

#include "GrainToolGUIWin.h"  // DOUBLE NO!!!

#include "ImageFormat3.h"
#include "ToolProgressMonitor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CGrainToolBrushStrokeProc::CGrainToolBrushStrokeProc(
   int newToolNumber,
   const string &newToolName,
   CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr,
   IToolProgressMonitor *newToolProgressMonitor,
   GrainGeneratorModel *grainGeneratorModel)

 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
 , _grainGeneratorModel(grainGeneratorModel)
 , _grainGenerationFrameIndex(-1)
{
  StartProcessThread(); // Required by Tool Infrastructure
}

CGrainToolBrushStrokeProc::~CGrainToolBrushStrokeProc()
{
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   SetInputMaxFrames(0, 1, 1);  // Input Port 0: 1 frames, replace 1 each iter
   SetOutputNewFrame(0,0);      // We generate a whole new frame
   SetOutputMaxFrames(0, 1);    // Output Port 0; 1 frame

   const CImageFormat *pIF = GetSrcImageFormat(0);
   if (pIF == NULL)
      return -1;

   // Set image format dependent stuff
   int iNumComponents = pIF->getComponentCountExcAlpha();

   long lWidth, lHeight;
   MTI_UINT16 caMin[MAX_COMPONENT];
   MTI_UINT16 caMax[MAX_COMPONENT];
   MTI_UINT16 caFill[MAX_COMPONENT];

   lWidth = pIF->getTotalFrameWidth();
   lHeight = pIF->getTotalFrameHeight();

   this->_grainGeneratorModel->SetImageSize(lWidth,lHeight);

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   inFrameIndex = frameRange.inFrameIndex;
   framesRemaining = 1;
   framesAlreadyProcessed = 0;
   iterationCount = 1;

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::SetParameters(CToolParameters *toolParams)
{
   CGrainToolParameters *afpToolParams = static_cast<CGrainToolParameters*>(toolParams);

   // Note: we use a CGrainToolParameters default assignment operator
   grainParams = *afpToolParams;

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::BeginProcessing(SToolProcessingData &procData)
{
   GetToolProgressMonitor()->SetFrameCount(framesRemaining);
   GetToolProgressMonitor()->SetStatusMessage("Stroking");
   GetToolProgressMonitor()->StopProgress(true);

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::BeginIteration(SToolProcessingData &procData)
{
   SetInputFrames(0, 1, &inFrameIndex);
   SetOutputFrames(0, 1, &inFrameIndex);
   SetFramesToRelease(0, 1, &inFrameIndex);

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::GetIterationCount(SToolProcessingData &procData)
{
   return 1;
}
//---------------------------------------------------------------------------

namespace
{

int nextPow2Minus1(long long n)
{
	n += (n == 0);
	n--;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n |= n >> 32;
	return n;
}

};
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::DoProcess(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CGrainToolBrushStrokeProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   this->HRT.Start();

   auto inToolFB = GetRequestedInputFrame(0, 0);
	auto outToolFB = GetRequestedOutputFrame(0, 0);
   auto inVisibleFrameBuffer = inToolFB->GetVisibleFrameBufferPtr();
   auto outVisibleFrameBuffer = outToolFB->GetVisibleFrameBufferPtr();

   auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();
   auto grainImage = GenerateBrushStrokeGrainImage(grainGenerator, inVisibleFrameBuffer);

   MTI_UINT16 componentValues[3];
  	const CImageFormat *imageFormat = GetSrcImageFormat(0);
   imageFormat->getComponentValueMax(componentValues);
   int maxValue = nextPow2Minus1(componentValues[0]);

   auto in = grainImage;
   auto n = grainGenerator->GetImageSizeInComponents();
   for (auto i = 0; i < n; i ++)
   {
      auto v = *in;
      *in ++ = v > maxValue ? maxValue : v;
   }

   MTIassert(grainImage != NULL);
   MTIassert(this->_grainGeneratorModel->GetUseBrushMask());

   if (this->_grainGeneratorModel->GetUseBrushMask()
   && grainImage != NULL)
   {
      this->_grainGeneratorModel->StampNewBrushStrokes();
      BrushRevealer *revealer = this->_grainGeneratorModel->GetBrushRevealer();
      RECT strokesAndMaskBoundingBox;

      if (GetSystemAPI()->IsMaskAvailable())
      {
         // Do not trash the grainImage, since it is cached in the GrainGenerator!
         MTI_UINT16 *tempBuffer = (MTI_UINT16*)
            MTImalloc(sizeof(MTI_UINT16) * grainGenerator->GetImageSizeInComponents());
         revealer->AlphaBlendOver(inVisibleFrameBuffer, grainImage, tempBuffer);
         CopyInputFrameBufferToOutputFrameBuffer(inToolFB, outToolFB);

         CRegionOfInterest maskROI;
         int frameIndex = outToolFB->GetFrameIndex();
         GetSystemAPI()->GetMaskRoi(frameIndex, maskROI);
         auto box      = maskROI.getBlendBox();
         auto blendPtr = maskROI.getBlendPtr();
			MTI_UINT16 *inPtr;
         MTI_UINT16 *outPtr;
         MTI_UINT8 *blend;
         int width = grainGenerator->GetImageSize().width;
         const CImageFormat *imageFormat = GetSrcImageFormat(0);
         int componentCount = imageFormat->getComponentCountExcAlpha();

         for (int r = box.top; r < box.bottom; r++)
         {
				inPtr  = tempBuffer + r * width * componentCount;
				outPtr = outToolFB->GetVisibleFrameBufferPtr() + r * width * componentCount;
				blend  = (MTI_UINT8*)blendPtr + r * width;
				for (int c = box.left; c < box.right; c++)
				{
					// Needs to be fixed for alpha
					float f = blend[c] / 255.0;
					auto firstComponentIndex = c * componentCount;
					for (auto i = firstComponentIndex; i < (firstComponentIndex + componentCount); ++i)
					{
						outPtr[i] = (1 - f) * outPtr[i] + f * inPtr[i];
					}
				}
         }

         MTIfree(tempBuffer);

         RECT brushBox = revealer->GetMaskBoundingBox();
         RECT maskBox = maskROI.getBlendBox();
         strokesAndMaskBoundingBox = RECT
         {
             std::max<int>(brushBox.left,   maskBox.left),
             std::max<int>(brushBox.top,    maskBox.top),
             std::min<int>(brushBox.right,  maskBox.right),
             std::min<int>(brushBox.bottom, maskBox.bottom)
         };
      }
      else
      {
         // Way simpler without a mask!
         revealer->AlphaBlendOver(inVisibleFrameBuffer, grainImage, outVisibleFrameBuffer);

         // No mask so just use the brush strokes bounding box.
         strokesAndMaskBoundingBox = revealer->GetMaskBoundingBox();
      }

      // Save original values within the intersection of the mask's
      // and the brush strokes' bounding rectangles.
      // QQQ should really save the actual pixels as a region
      auto imageFormat = GetSrcImageFormat(0);
      auto originalValues = outToolFB->GetOriginalValuesPixelRegionListPtr();
      originalValues->Add(
            strokesAndMaskBoundingBox,
            inVisibleFrameBuffer,
            imageFormat->getTotalFrameWidth(),
            imageFormat->getTotalFrameHeight(),
            imageFormat->getComponentCountExcAlpha());
   }
   else
   {
      CopyInputFrameBufferToOutputFrameBuffer(inToolFB, outToolFB);
   }

   GGrainTool->GrainTime = this->HRT.ReadAsString();

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::EndIteration(SToolProcessingData &procData)
{
   framesRemaining = 0;
   framesAlreadyProcessed = 1;

   GetToolProgressMonitor()->BumpProgress(1);

   return 0;
}
//-------------------------------------------------------------------------

int CGrainToolBrushStrokeProc::EndProcessing(SToolProcessingData &procData)
{
   GetToolProgressMonitor()->SetStatusMessage(
      (this->_grainGeneratorModel->GetStrokeCount() > 0)
      ? "Strokes are pending"
      : "");

   return 0;
}
//-------------------------------------------------------------------------

MTI_UINT16 *CGrainToolBrushStrokeProc::GenerateBrushStrokeGrainImage(
   GrainGenerator *grainGenerator,
   MTI_UINT16 *visibleFrameBuffer)
{
	MTIassert(grainGenerator != NULL);
   MTIassert(visibleFrameBuffer != NULL);

   MTI_UINT16 *grainImage;
   auto grainImageSourceChanged = inFrameIndex != this->_grainGenerationFrameIndex;
   this->_grainGenerationFrameIndex = inFrameIndex;

   // Temp code until this can be figured out
	const CImageFormat *imageFormat = GetSrcImageFormat(0);
   MTI_UINT16 componentValues[3];
	imageFormat->getComponentValueMax(componentValues);
   int maxValue = nextPow2Minus1(componentValues[0]);
   //

   try
   {
		grainImage = grainGenerator->ApplyGrain(
            visibleFrameBuffer,
            maxValue,
            GetChannelMask(),
            grainImageSourceChanged,
				grainImageSourceChanged,
				imageFormat->getColorSpace() != IF_COLOR_SPACE_EXR);
   }
   catch(...)
   {
      TRACE_0(errout << "ERROR got exception while generating grain image");
      grainImage = NULL;
   }

   return grainImage;
}
//-------------------------------------------------------------------------
