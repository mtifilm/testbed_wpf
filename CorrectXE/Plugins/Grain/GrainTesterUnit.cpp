// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Ippheaders.h"

#include "GrainCoreCommon.h"
#include "GrainTesterUnit.h"
#include "IniFile.h"
#include "HRTimer.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TrackEditFrameUnit"
#pragma resource "*.dfm"
TGrainTesterForm *GrainTesterForm;

const string GrainIniFileName("$(CPMP_USER_DIR)Grain.ini");
#define COMPONENTS 3
// ---------------------------------------------------------------------------
__fastcall TGrainTesterForm::TGrainTesterForm(TComponent* Owner) : TForm(Owner)
{
	this->_brush = new CFloatBrush();

	this->ProjectRadioButton->Tag = GP_BASE_PROJECT;
	this->ClipRadioButton->Tag = GP_BASE_CLIP;

	// Save the original image
	int w = this->DestImage->Picture->Bitmap->Width;
	int h = this->DestImage->Picture->Bitmap->Height;

	this->_originalImage = ippsMalloc_8u(3 * w * h);
	for (int i = 0; i < h; i++)
	{
		auto p1 = reinterpret_cast<unsigned char*>(this->DestImage->Picture->Bitmap->ScanLine[i]);
		IppThrowOnError(ippsCopy_8u((Ipp8u*)p1, this->_originalImage + i * 3*w, 3*w));
	}

	this->_grayImage = ippsMalloc_8u(3 * w * h);
	ippsSet_8u(128, this->_grayImage, 3*w*h);

	originalNormalized = ippsMalloc_32f(3 * w * h);
	IppThrowOnMemoryError(originalNormalized);

	pProcessed = ippsMalloc_8u(3 * w * h);
	IppThrowOnMemoryError(pProcessed);

	pProcessed32f = ippsMalloc_32f(3 * w * h);
	IppThrowOnMemoryError(pProcessed32f);

	pOriginal = this->_originalImage;
	IppThrowOnError(ippsConvert_8u32f(pOriginal, originalNormalized, 3*w*h));
	IppThrowOnError(ippsDivC_32f(originalNormalized, 256, originalNormalized, 3*w*h));

	this->_grainGeneratorModel = new GrainGeneratorModel(w, h);
	this->_grainGeneratorModel->SetMasterClipFolderPath("c:\\temp");
	this->_grainGeneratorModel->SetProjectRootFolderPath("c:\\temp");

	SET_CBHOOK(GrainHasChanged, this->_grainGeneratorModel->GrainChange);
	SET_CBHOOK(PresetHasChanged, this->_grainGeneratorModel->PresetChange);

	this->_grainGeneratorModel->LoadFromGrainIni(GrainIniFileName);
	this->_brushRevealer = new BrushRevealer(MTIImageSize {w, h});

	int pw = BrushPreviewPaintBox->Width;
	int ph = BrushPreviewPaintBox->Height;
	this->_previewBrushRevealer = new BrushRevealer(MTIImageSize {pw, ph});
	this->_prevewGrayImageNormalize = ippsMalloc_32f(COMPONENTS * pw * ph);
	IppThrowOnError(ippsSet_32f(0.5f, this->_prevewGrayImageNormalize, COMPONENTS * pw * ph));
	this->_previewGrainGeneratorModel = new GrainGeneratorModel(pw, ph);
}

const GrainParameters TGrainTesterForm::ReadGrainParametersFromGUI() const
{
	GrainParameters grainParameters;
	grainParameters.GrainSize = this->SizeTrackEditFrame->TrackBar->Position /100.0;
	grainParameters.Lowlights = this->LowlightsTrackEditFrame->TrackBar->Position;
	grainParameters.Highlights = this->HighlightsTrackEditFrame->TrackBar->Position;
	grainParameters.Saturation = this->SaturationTrackEditFrame->TrackBar->Position;
	grainParameters.Roughness = this->RoughnessTrackEditFrame->TrackBar->Position;

	return grainParameters;
}

void TGrainTesterForm::SetGrainParametersToGUI(const GrainParameters &grainParameters)
{
	this->SizeTrackEditFrame->TrackBar->Position = 100*grainParameters.GrainSize;
	this->LowlightsTrackEditFrame->TrackBar->Position = grainParameters.Lowlights;
	this->HighlightsTrackEditFrame->TrackBar->Position = grainParameters.Highlights;
	this->SaturationTrackEditFrame->TrackBar->Position = grainParameters.Saturation;
	this->RoughnessTrackEditFrame->TrackBar->Position = grainParameters.Roughness;
}

// ---------------------------------------------------------------------------

void TGrainTesterForm::DisplayImage(Ipp8u * pImage)
{
	int W = this->DestImage->Picture->Bitmap->Width;
	int H = this->DestImage->Picture->Bitmap->Height;
	int C = 3;

	TBitmap *b;

	b = this->DestImage->Picture->Bitmap;
	for (int i = 0; i < H; i++)
	{
		auto p1 = reinterpret_cast<unsigned char*>(b->ScanLine[i]);
		ippsCopy_8u(pImage + i * C*W, (Ipp8u*)p1, C*W);
	}

	this->DestImage->Picture->Bitmap = b;
}

void TGrainTesterForm::GrainHasChanged(void *sender)
{
	this->ComputeDisplay(false);
	this->SavePresetButton->Enabled =
		!this->_grainGeneratorModel->DoesCurrentGrainParametersMatchPreset();
}

void TGrainTesterForm::PresetHasChanged(void *sender)
{
	// Just update the GUI
	switch (this->_grainGeneratorModel->GetGrainParametersBase())
	{
	case GP_BASE_PROJECT:
		this->ProjectRadioButton->Checked = true;
		break;

	case GP_BASE_CLIP:
		this->ClipRadioButton->Checked = true;
		break;

	default:
		break;
	}

	// Just update the GUI
	switch (this->_grainGeneratorModel->GetPreset())
	{
	case 0:
		this->Preset1Button->Down = true;
		break;

	case 1:
		this->Preset2Button->Down = true;
		break;

	}

	// Set the GUI
	this->SetGrainParametersToGUI(this->_grainGeneratorModel->GetSelectedGrainParameters());

	// Always should return false but check for logical error
	this->SavePresetButton->Enabled =
		!this->_grainGeneratorModel->DoesCurrentGrainParametersMatchPreset();
}

void TGrainTesterForm::ComputeDisplay(bool sourceChanged)
{
	auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();

	if (!grainGenerator->IsDirty() && !sourceChanged)
	{
		return;
	}

	CHRTimer hrt;
	hrt.Start();
	this->originalWithGrainNormalized = grainGenerator->ApplyGrain(this->originalNormalized,
		sourceChanged, this->originalWithGrainNormalized, false);

	std::string result = hrt.ReadAsString();
	AnsiString c3 = "Grain Time: ";
	SpeedLabel->Caption = c3 + result.c_str();

	ippsMulC_32f(this->originalWithGrainNormalized, 256, pProcessed32f,
		grainGenerator->GetImageSizeInComponents());
	ippsConvert_32f8u_Sfs(pProcessed32f, pProcessed, grainGenerator->GetImageSizeInComponents(),
		ippRndNear, 0);

	processed = true;
	DisplayImage(pProcessed);
	ToggleButton->Caption = "Processed";
}

void __fastcall TGrainTesterForm::ToggleButtonClick(TObject *Sender)
{
	processed = !processed;
	if (processed)
	{
		DisplayImage(pProcessed);
		ToggleButton->Caption = "Processed";
	}
	else
	{
		DisplayImage(pOriginal);
		ToggleButton->Caption = "Original";
	}
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
void __fastcall TGrainTesterForm::ImageGroupClick(TObject *Sender)
{
	switch (this->ImageGroup->ItemIndex)
	{
	case 0:
		pOriginal = this->_grayImage;
		break;

	case 1:
		pOriginal = this->_originalImage;
		break;

	default:
		break;
	}

	// Should be made common code
	int len = this->_grainGeneratorModel->GetGrainGenerator()->GetImageSizeInComponents();
	IppThrowOnError(ippsConvert_8u32f(pOriginal, originalNormalized, len));
	IppThrowOnError(ippsDivC_32f(originalNormalized, 256, originalNormalized, len));
	ComputeDisplay(true);
	processed = false;

	DisplayImage(pOriginal);
	ToggleButton->Caption = "Original";
}

// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::TrackEditFrameNotifyWidgetClick(TObject *Sender)
{
	this->_grainGeneratorModel->SetGrainParameters(ReadGrainParametersFromGUI());
}

// ---------------------------------------------------------------------------
void __fastcall TGrainTesterForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	this->_grainGeneratorModel->SaveToGrainIni(GrainIniFileName);
	delete this->_grainGeneratorModel;
	this->_grainGeneratorModel = 0;
}
// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::Preset1ButtonClick(TObject *Sender)
{
	TSpeedButton *speedButton = (TSpeedButton*)Sender;
	int newPreset = (int)speedButton->Tag;
	this->_grainGeneratorModel->SetPreset(newPreset);
}
// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::ProjectRadioButtonClick(TObject *Sender)
{
	TControl *control = (TControl*)Sender;
	GrainParameterBaseType base = (GrainParameterBaseType)control->Tag;
	this->_grainGeneratorModel->SetGrainParametersBase(base);
}
// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::SavePresetButtonClick(TObject *Sender)
{
	this->_grainGeneratorModel->SetCurrentToSelectedPreset();
	this->SavePresetButton->Enabled =
		!this->_grainGeneratorModel->DoesCurrentGrainParametersMatchPreset();
}
// ---------------------------------------------------------------------------

void TGrainTesterForm::ReadBrushParametersFromGui(CFloatBrush *brush)
{
	brush->setBrushParameters(1000, RadiusTrackEditFrame->TrackBar->Position,
		BlendTrackEditFrame->TrackBar->Position, OpacityTrackEditFrame->TrackBar->Position,
		AspectTrackEditFrame->TrackBar->Position, AngleTrackEditFrame->TrackBar->Position,
		SqSpeedButton->Down);
}

void TGrainTesterForm::StampBrush()
{
	CHRTimer hrt;
	hrt.Start();

	ReadBrushParametersFromGui(this->_brush);

	int maxCols = this->_brush->getMaxCol();
	int minCols = this->_brush->getMinCol();
	int maxRows = this->_brush->getMaxRow();
	int minRows = this->_brush->getMinRow();

	int w = this->DestImage->Picture->Bitmap->Width;
	int h = this->DestImage->Picture->Bitmap->Height;

	ippsCopy_8u(pOriginal, pProcessed, 3*w*h);

	int bh = this->_brush->getBrushHeight();
	int bw = this->_brush->getBrushWidth();

	auto extractedGrainImage = ippsMalloc_32f(3 * bw * bh);

	std::string result = hrt.ReadAsString();
	AnsiString c3 = "Brush Time: ";
	c3 = c3 + result.c_str();

	int x = w / 2 + minCols;
	int y = h / 2 + minRows;

	int step = 3 * w*sizeof(float);
	ippiCopyManaged_8u_C1R((Ipp8u*)(this->originalWithGrainNormalized + 3 * w * y + 3 * x), step,
		(Ipp8u*)extractedGrainImage, 3 * bw*sizeof(float), IppiSize {12 * bw, bh
	}, IPP_TEMPORAL_COPY);

	MTI_UINT8 *p;
	MTI_REAL32 *nfp = this->_brush->getFloatWeights();

	MTI_REAL32 *egi = extractedGrainImage;
	for (int r = minRows; r < maxRows; r++)
	{
		p = pProcessed + 3 * (r + h / 2) * w + 3 * (minCols + w / 2);
		if (p < pProcessed)
		{
			Beep();
			return;
		}

		for (int c = minCols; c < maxCols; c++)
		{

			float m = *nfp++;
			*p++ = 255 * m * *egi++ + (1 - m) * *p;
			*p++ = 255 * m * *egi++ + (1 - m) * *p;
			*p++ = 255 * m * *egi++ + (1 - m) * *p;
		}
	}

	result = hrt.ReadAsString();
	AnsiString c4 = "Insert Time: ";
	c4 = c4 + result.c_str();

	ippsFree(extractedGrainImage);
	DisplayImage(pProcessed);

	result = hrt.ReadAsString();
	AnsiString c5 = "Display Time: ";
	c5 = c5 + result.c_str();

	ToggleButton->Caption = "Processed";

	BrushSpeedLabel->Caption = c3;
	InsertSpeedLabel->Caption = c4;
	DrawSpeedLabel->Caption = c5;
}
// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::RadiusTrackEditFrameNotifyWidgetClick(TObject *Sender)
{
	this->StampBrush();
	this->UpdatePreviewBrush();
}

// ---------------------------------------------------------------------------
void __fastcall TGrainTesterForm::StampButtonClick(TObject *Sender)
{
	this->StampBrush();
	this->_brushRevealer->ResetMask();
}
// ---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::Button1Click(TObject *Sender)
{
	ReadBrushParametersFromGui(this->_brush);
	auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();

	auto tempImage = ippsMalloc_32f(grainGenerator->GetImageSizeInComponents());
	IppThrowOnError(ippsConvert_8u32f(pOriginal, tempImage, grainGenerator->GetImageSizeInComponents()));

	ippsMulC_32f(this->originalWithGrainNormalized, 256, pProcessed32f,
	grainGenerator->GetImageSizeInComponents());

	BrushRevealer *br = this->_brushRevealer;
	br->StampMask(this->_brush, 250, 250);
	br->StampMask(this->_brush, 250, 500);
	br->StampMask(this->_brush, 400, 500);
	br->StampMask(this->_brush, 0, 0);
	br->StampMask(this->_brush, 1000, 750);

	br->AlphaBlendOver(tempImage, pProcessed32f, tempImage);


	ippsConvert_32f8u_Sfs(tempImage, pProcessed, grainGenerator->GetImageSizeInComponents(),
		ippRndNear, 0);

	DisplayImage(pProcessed);
	ToggleButton->Caption = "Processed";
}

void TGrainTesterForm::UpdatePreviewBrush()
{
	this->_previewGrainGeneratorModel->SetGrainParameters(ReadGrainParametersFromGUI());

	auto previewBrush = new CFloatBrush();
	previewBrush->setBrushParameters(100, 40, BlendTrackEditFrame->TrackBar->Position,
		OpacityTrackEditFrame->TrackBar->Position, AspectTrackEditFrame->TrackBar->Position,
		AngleTrackEditFrame->TrackBar->Position, SqSpeedButton->Down);

	this->_previewBrushRevealer->ResetMask();

	int pw = BrushPreviewPaintBox->Width;
	int ph = BrushPreviewPaintBox->Height;
	this->_previewBrushRevealer->StampMask(previewBrush, pw/2, ph/2);
	auto previewGrainGenerator = this->_previewGrainGeneratorModel->GetGrainGenerator();

	auto previewWithGrainNormalized = previewGrainGenerator->ApplyGrain(this->_prevewGrayImageNormalize,
		true, 0, false);

	MTI_REAL32 *tempImage = this->_previewBrushRevealer->AlphaBlend(this->_prevewGrayImageNormalize,
	 previewWithGrainNormalized);

	auto tempProcessImage = ippsMalloc_8u(previewGrainGenerator->GetImageSizeInComponents());
	ippsMulC_32f(tempImage, 256, tempImage, previewGrainGenerator->GetImageSizeInComponents());
	ippsConvert_32f8u_Sfs(tempImage, tempProcessImage, previewGrainGenerator->GetImageSizeInComponents(),
	ippRndNear, 0);

	TBitmap *b = this->BrushPreviewPaintBox->Picture->Bitmap;
	b->Width = pw;
	b->Height = ph;
	b->PixelFormat = pf24bit;
	for (int i = 0; i < pw; i++)
	{
		auto p1 = reinterpret_cast<unsigned char*>(b->ScanLine[i]);
		ippsCopy_8u(tempProcessImage + i * COMPONENTS*pw, (Ipp8u*)p1, COMPONENTS*pw);
	}

	this->BrushPreviewPaintBox->Picture->Bitmap = b;

	delete previewBrush;
}

// ---------------------------------------------------------------------------


//	int w = this->DestImage->Picture->Bitmap->Width;
//	int h = this->DestImage->Picture->Bitmap->Height;
//
//	ippsCopy_8u(pOriginal, pProcessed, 3*w*h);
//
//	ReadBrushParametersFromGui(this->_brush);
//	MTI_REAL32 *nfp = this->_brush->getFloatWeights();
//	int bw = this->_brush->getBrushWidth();
//	int bh = this->_brush->getBrushHeight();
//
//	Ipp8u *testImage = ippsMalloc_8u(4 * bh * bw);
//	Ipp8u *t = testImage;
//	for (int i = 0; i < this->_brush->getBrushSizeInFloats(); i++)
//	{
//			*t++ = 255**nfp++;
//		*t++ = 255;
//		*t++ = 255;
//		*t++ = 255;
//	}
//
//	int x = w / 2 + this->_brush->getMinCol();
//	int y = h / 2 + this->_brush->getMinRow();
//
//	ippiCopy_8u_AC4C3R(testImage, 4*bw, pProcessed + 3 * w * y + 3 * x,  3*w, IppiSize {bw, bh});
//	DisplayImage(pProcessed);
//	ToggleButton->Caption = "Processed";

void TGrainTesterForm::StampBrushOnImage(int x, int y)
{
	auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();
	this->_brushRevealer->StampMask(this->_brush, x, y);
}

bool mouseDown = false;

void __fastcall TGrainTesterForm::DestImageMouseMove(TObject *Sender, TShiftState Shift,
	int X, int Y)
{
	if (mouseDown)
	{
		////this->StampBrushOnImage(X, Y);
	BrushStroke *brushStroke = this->_brushStrokes.back();

	brushStroke->Points.push_back(POINT {X, Y});
	}
}

//---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::DestImageMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	mouseDown = false;

	this->_brushRevealer->StampMask(this->_brushStrokes);

	auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();
	MTI_REAL32 *tempImage = this->_brushRevealer->AlphaBlend(originalNormalized,
	this->originalWithGrainNormalized);

	ippsMulC_32f(tempImage, 256, tempImage, grainGenerator->GetImageSizeInComponents());
	ippsConvert_32f8u_Sfs(tempImage, pProcessed, grainGenerator->GetImageSizeInComponents(),
		ippRndNear, 0);

	DisplayImage(pProcessed);
	ToggleButton->Caption = "Processed";


}
//---------------------------------------------------------------------------

void __fastcall TGrainTesterForm::DestImageMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	this->ReadBrushParametersFromGui(this->_brush);
	mouseDown = true;

	BrushStroke *brushStroke = new BrushStroke(this->_brush);
	brushStroke->Points.push_back(POINT {X, Y});
	this->_brushStrokes.push_back(brushStroke);
//	this->StampBrushOnImage(X, Y);
//
//		auto grainGenerator = this->_grainGeneratorModel->GetGrainGenerator();
//		MTI_REAL32 *tempImage = this->_brushRevealer->AlphaBlend(originalNormalized,
//		this->originalWithGrainNormalized);
//
//	ippsMulC_32f(tempImage, 256, tempImage, grainGenerator->GetImageSizeInComponents());
//	ippsConvert_32f8u_Sfs(tempImage, pProcessed, grainGenerator->GetImageSizeInComponents(),
//		ippRndNear, 0);
//
//	DisplayImage(pProcessed);
//	ToggleButton->Caption = "Processed";
}
//---------------------------------------------------------------------------

