// ---------------------------------------------------------------------------

#include "GrainCoreApi.h"
#include "Ippheaders.h"
#include "ImageResizer32f.h"
#include "NormalRandomImageC3.h"

#pragma hdrstop

// Can never change
#define CHANNELS  3

/// <summary>
/// Initializes a new instance of the <see cref="ImageResizer32f" /> class.
/// </summary>
/// <param name="sourceSize">(w,h) size of source</param>
/// <param name="destinationSize">(w,h) size of source</param>
ImageResizer32f::ImageResizer32f(IppiSize sourceSize, IppiSize destinationSize)
	: _sourceSize(sourceSize), _destinationSize(destinationSize),
	_ippiResizeSpec(0), _resizeBuffer(0)
{
	InitializeResize_C3(sourceSize, destinationSize);
}

ImageResizer32f::~ImageResizer32f()
{
	this->FreeInternalMemory();
}

void ImageResizer32f::FreeInternalMemory()
{
	ippsFree(this->_ippiResizeSpec);
	this->_ippiResizeSpec = 0;
	ippsFree(this->_resizeBuffer);
	this->_resizeBuffer = 0;
}

/// <summary>
/// This resets everything and changes the size of the data
/// </summary>
/// <param name="sourceSize">(w,h) size of source</param>
/// <param name="destinationSize">(w,h) size of source</param>
void ImageResizer32f::InitializeResize_C3(const IppiSize sourceSize, const IppiSize destinationSize)
{
	// Free up any allocated memory
	this->FreeInternalMemory();

	// Store the new size
	this->_sourceSize = sourceSize;
	this->_destinationSize = destinationSize;

	int specSize = 0, initSize = 0, bufSize = 0;

	/* Spec and init buffer sizes */
	IppThrowOnError(ippiResizeGetSize_32f(this->_sourceSize, this->_destinationSize, ippCubic, 0,
		&specSize, &initSize));

	/* Memory allocation */
	this->_ippiResizeSpec = (IppiResizeSpec_32f*)ippsMalloc_8u(specSize);
	IppThrowOnMemoryError(this->_ippiResizeSpec);

	Ipp8u* pBuffer = ippsMalloc_8u(initSize);
	IppThrowOnMemoryError(pBuffer);

	/* Filter initialization */
////	IppStatus status = ippiResizeLinearInit_32f(this->_sourceSize, this->_destinationSize, this->_ippiResizeSpec);
	IppStatus status = ippiResizeCubicInit_32f(this->_sourceSize, this->_destinationSize, 1, 0, this->_ippiResizeSpec, pBuffer);
	if (status != ippStsNoErr)
	{
		this->FreeInternalMemory();
		throw IppException(status);
	}

	/* work buffer size */
	status = ippiResizeGetBufferSize_32f(_ippiResizeSpec, this->_destinationSize, CHANNELS,
		&bufSize);
	if (status != ippStsNoErr)
	{
		this->FreeInternalMemory();
		throw IppException(status);
	}

	this->_resizeBuffer = ippsMalloc_8u(bufSize);
	IppThrowOnMemoryError(this->_resizeBuffer);
}

void ImageResizer32f::Resize(const Ipp32f *source, Ipp32f *destination)
{
	int srcStep = CHANNELS * this->_sourceSize.width * sizeof(Ipp32f);
	int dstStep = CHANNELS * this->_destinationSize.width * sizeof(Ipp32f);
	IppiPoint dstOffset = {0, 0};
	IppiBorderType border = ippBorderRepl;

	/* Resize processing */
	if ((this->_sourceSize.width == this->_destinationSize.width) &&
		(this->_sourceSize.height == this->_destinationSize.height))
	{
	   int sizeInComponents = CHANNELS * this->_sourceSize.width * this->_sourceSize.height;
	   IppThrowOnError(ippsCopy_32f(source, destination, sizeInComponents));
	   return;
	}

//	 IppThrowOnError(ippiResizeLinear_32f_C3R(source, srcStep, destination, dstStep,
//		dstOffset, this->_destinationSize, border, 0, this->_ippiResizeSpec, this->_resizeBuffer));
	IppThrowOnError(ippiResizeCubic_32f_C3R(source, srcStep, destination, dstStep,
		dstOffset, this->_destinationSize, border, 0, this->_ippiResizeSpec, this->_resizeBuffer));
}

Ipp32f* ImageResizer32f::CreateAndResize(const Ipp32f *source)
{
	int imageSize =
		CHANNELS*this->_destinationSize.width*this->_destinationSize.height;

	Ipp32f* destination = ippsMalloc_32f(imageSize);
	if (destination == 0)
	{
		throw ippStsNoMemErr;
	}

	this->Resize(source, destination);
	return destination;
}



// }
// ---------------------------------------------------------------------------
#pragma package(smart_init)
