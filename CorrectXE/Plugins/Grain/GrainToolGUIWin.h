/*
 File Name:  GrainToolGUIWin.h
 Header file for the Windows-based (Borland) GrainTool GUI
 */

#ifndef GuiWinH
#define GuiWinH
// ---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include "GrainTool.h"
#include <Mask.hpp>
#include "VTimeCodeEdit.h"
#include "HRTimer.h"
#include "ExecButtonsFrameUnit.h"
#include "CompactTrackEditFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include "ColorPanel.h"

/////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateGrainToolGUI(void);
extern void DestroyGrainToolGUI(void);
extern bool ShowGrainToolGUI(void);
extern bool HideGrainToolGUI(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void GatherGUIParameters(CGrainToolParameters &pdlToolParams);
extern void SetGUIParameters(CGrainToolParameters &toolParams);
extern bool IsToolVisible(void);
extern void SetFocusedControl(int commandNumber);
extern void ToggleContrastGang(void);
extern void FocusOnBrushParameterTrackEdit(int whichTrackEdit);
extern void ToggleAutoAccept();
extern int  GetChannelMask();
extern void SwitchSubtool();

//
// ---------------------------------------------------------------------------
class TGrainToolForm : public TMTIForm
{
__published: // IDE-managed Components

	TPanel *StatusBar;
	TLabel *FPSLabel;
	TLabel *MSLabel;
	TColorPanel *GrainControlPanel;
	TPanel *TitlePanel;
	TLabel *Label1;
	TExecButtonsFrame *ExecButtonsToolbar;
	TExecStatusBarFrame *ExecStatusBar;
	TPageControl *GrainPageControl;
	TTabSheet *CreateTabSheet;
	TGroupBox *GrainGroupBox;
	TCompactTrackEditFrame *SizeTrackEditFrame;
	TCompactTrackEditFrame *LowlightsTrackEditFrame;
	TCompactTrackEditFrame *HighlightsTrackEditFrame;
	TCompactTrackEditFrame *SaturationTrackEditFrame;
	TCompactTrackEditFrame *RoughnessTrackEditFrame;
   TGroupBox *PresetGroupBox;
	TSpeedButton *Preset1Button;
	TSpeedButton *Preset2Button;
	TBitBtn *SavePresetButton;
	TRadioButton *ProjectRadioButton;
	TRadioButton *ClipRadioButton;
	TTabSheet *ReduceTabSheet;
	TGroupBox *AnalysisParametersGroupBox;
	TCompactTrackEditFrame *SizeTrackEdit;
	TCompactTrackEditFrame *ContrastRTrackEdit;
	TCompactTrackEditFrame *ContrastGTrackEdit;
	TCompactTrackEditFrame *ContrastBTrackEdit;
	TCheckBox *ContrastGangCheckBox;
	TGroupBox *TemporalAnalysisGroupBox;
	TCompactTrackEditFrame *TemporalWeightHighTrackEdit;
	TCompactTrackEditFrame *TemporalWeightMediumTrackEdit;
	TRadioButton *TemporalWeightOffRadioButton;
	TRadioButton *TemporalWeightMediumRadioButton;
	TRadioButton *TemporalWeightHighRadioButton;
   TGroupBox *EnableGroupBox;
	TCheckBox *ReduceCheckBox;
   TCheckBox *CreateCheckBox;
	TLabel *SpeedLabel;
	TGroupBox *TargetViewGroupBox;
	TSpeedButton *ShowOriginalSpeedButton;
	TSpeedButton *ShowProcessedSpeedButton;
	TTimer *RefreshTimer;
   TGroupBox *BrushGroupBox;
   TCompactTrackEditFrame *BlendTrackEditFrame;
   TCompactTrackEditFrame *OpacityTrackEditFrame;
   TCompactTrackEditFrame *RadiusTrackEditFrame;
   TCompactTrackEditFrame *AngleTrackEditFrame;
   TCompactTrackEditFrame *AspectTrackEditFrame;
   TLabel *Label2;
   TCheckBox *UseBrushCheckBox;
   TSpeedButton *RectSpeedButton;
   TSpeedButton *EllipseSpeedButton;
   TListBox *BrushStrokeListBox;
   TButton *UndoLastButton;
   TSpeedButton *RepeatStrokesSpeedButton;
   TButton *RejectButton;
   TButton *AcceptButton;
   TLabel *PendingStrokesLabel;
   TCheckBox *AutoAcceptCheckBox;
   TImage *BrushPreviewPaintBox;
   TLabel *ReduceGrainLabel;
   TLabel *CreateGrainLabel;
   TLabel *UseBrushLabel;
   TPanel *ChannelPanel;
   TSpeedButton *HilitedBlueChannelButton;
   TSpeedButton *HilitedGreenChannelButton;
   TSpeedButton *HilitedRedChannelButton;
   TSpeedButton *HilitedGrayChannelButton;
   TSpeedButton *BlueChannelButton;
   TSpeedButton *GreenChannelButton;
   TSpeedButton *RedChannelButton;
   TSpeedButton *AllChannelsButton;
   TLabel *ChannelLabel;
   TLabel *NoChannelsSelectedWarningLabel;

	void __fastcall SEditOnKeyPress(TObject *Sender, char &Key);
	void __fastcall TemporalWeightRadioButtonClick(TObject *Sender);
	void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender);
	void __fastcall ContrastGangCheckBoxClick(TObject *Sender);
	void __fastcall ContrastGTrackEditChange(TObject *Sender);
	void __fastcall ContrastBTrackEditChange(TObject *Sender);
	void __fastcall ContrastRTrackEditChange(TObject *Sender);
	void __fastcall GrainGeneratorTrackBarNotifyWidgetClick(TObject *Sender);
	void __fastcall PresetButtonClick(TObject *Sender);
	void __fastcall SavePresetButtonClick(TObject *Sender);
	void __fastcall PresetRadioButtonClick(TObject *Sender);
//	void __fastcall ToggleButtonClick(TObject *Sender);
	void __fastcall ReduceOrCreateCheckBoxClick(TObject *Sender);
	void __fastcall ShowOriginalSpeedButtonClick(TObject *Sender);
	void __fastcall ShowProcessedSpeedButtonClick(TObject *Sender);
	void __fastcall RefreshTimerTimer(TObject *Sender);
	void __fastcall onBrushParameterChange(TObject *Sender);
	void __fastcall BrushTabSheetShow(TObject *Sender);
	void __fastcall RejectButtonClick(TObject *Sender);
	void __fastcall UndoLastButtonClick(TObject *Sender);
	void __fastcall UseBrushCheckBoxClick(TObject *Sender);
   void __fastcall AutoAcceptCheckBoxClick(TObject *Sender);
   void __fastcall AcceptButtonClick(TObject *Sender);
   void __fastcall CreateCheckBoxClick(TObject *Sender);
   void __fastcall ReduceCheckBoxClick(TObject *Sender);
   void __fastcall onBrushShapeChange(TObject *Sender);
   void __fastcall ReduceCheckBoxEnter(TObject *Sender);
   void __fastcall ReduceCheckBoxExit(TObject *Sender);
   void __fastcall CreateCheckBoxEnter(TObject *Sender);
   void __fastcall CreateCheckBoxExit(TObject *Sender);
   void __fastcall UseBrushCheckBoxEnter(TObject *Sender);
   void __fastcall UseBrushCheckBoxExit(TObject *Sender);
   void __fastcall RepeatStrokesSpeedButtonClick(TObject *Sender);
   void __fastcall ChannelButtonClick(TObject *Sender);
   void __fastcall AllChannelsButtonClick(TObject *Sender);

private: // User declarations

	CHRTimer HRT;
	bool updatingRContrast, updatingGContrast, updatingBContrast;

	DEFINE_CBHOOK(ClipHasChanged, TGrainToolForm);
	DEFINE_CBHOOK(MarksHaveChanged, TGrainToolForm);
	DEFINE_CBHOOK(BrushStrokesHaveChanged, TGrainToolForm);
	DEFINE_CBHOOK(GrainParametersHaveChanged, TGrainToolForm);
	DEFINE_CBHOOK(PresetHasChanged, TGrainToolForm);
	DEFINE_CBHOOK(BrushShapeTrackEditGotFocus, TGrainToolForm);

	void PreviewFrame(void);
	EToolProcessingCommand GetButtonCommand(TToolButton *button);
	void SetButtonCommand(TToolButton *button, EToolProcessingCommand newCmd);

   void EnableOrDisableReduceStuff();
   void EnableOrDisableCreateStuff();
   void EnableOrDisableBrushStuff();

	// Exec buttons toolbar
	void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
	void ExecStop(void);
	void ExecCaptureToPDL(void);
	void ExecCaptureAllEventsToPDL(void);
	void ExecSetResumeTimecode(int frameIndex);
	void ExecSetResumeTimecodeToCurrent(void);
	void ExecSetResumeTimecodeToDefault(void);
	void ExecGoToResumeTimecode(void);
	void UpdateExecutionButtonToolbar(EToolControlState toolProcessingControlState,
		bool processingRenderFlag);

	void setBusyCursor(void);
	void setIdleCursor(void);

	// For Preview
   MTI_REAL32 *_prevewGrayImageNormalize;
   BrushRevealer *_previewBrushRevealer;
   void UpdatePreviewBrush();
   void UpdateBrushRadiusRange();

public: // User declarations
	__fastcall TGrainToolForm(TComponent* Owner);

	bool doesFormUseOwnWindow()
	{
		return false;
	};

	void formCreate(void);
	void formShow(void);
	void formHide(void);
   bool formKeyDown(WORD &Key, TShiftState Shift);

	virtual bool WriteSettings(CIniFile *ini, const string &IniSection);
	virtual bool ReadSettings(CIniFile *ini, const string &IniSection);

	void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
		bool processingRenderFlag);
	void CaptureParameters(CPDLElement &toolParams);
	void GatherParameters(CGrainToolParameters &pdlToolParams);
	void SetParameters(CGrainToolParameters &pdlToolParams);
	bool runExecButtonsCommand(EExecButtonId command);

	void setFocusedControl(int commandNumber);
	TObject *computeFocusedControlReduce(int commandNumber, TObject *focusedControl);
	TObject *computeFocusedControlGenerate(int commandNumber, TObject *focusedControl);

	void toggleContrastGang(void);
   void focusOnBrushParameterTrackEdit(int whichTrackEdit);
   void switchSubtool();

	void SetGrainParametersToGUI(const GrainParameters &grainParameters);
	const GrainParameters ReadGrainParametersFromGUI() const ;

	void UpdateProvisionButtons();
	void SavePresets();

};

// ---------------------------------------------------------------------------
extern PACKAGE TGrainToolForm *GrainToolForm;
extern MTI_UINT32 *FeatureTable[];
// ---------------------------------------------------------------------------
#endif
