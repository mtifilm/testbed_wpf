#ifndef GrainCoreCommonH
#define GrainCoreCommonH

#include <algorithm>
#include <mutex>
#include <stdlib>
#include <string>
#include "IniFile.h"

#include "GrainCoreApi.h"
#include "Ippheaders.h"
#include "GrainParameters.h"
#include "GrainGenerator.h"
#include "GrainGeneratorModel.h"
#include "BrushRevealer.h"
#include "BrushStroke.h"


#endif



