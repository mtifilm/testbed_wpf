//---------------------------------------------------------------------------

#ifndef BrushRevealerH
#define BrushRevealerH

#include "BrushStroke.h"
#include "FloatBrush.h"
#include "Ippheaders.h"
#include "ThreadLocker.h"

MTI_GRAINCORE_API class BrushRevealer
{
public:
	BrushRevealer(const IppiSize &size);
	~BrushRevealer();

	void StampMask(CFloatBrush *brush, int col, int row, bool doErase = false);
	void StampMask(const BrushStroke &brushStroke);
	void StampMask(const BrushStrokes &brushStrokes, bool resetMask = true);
	void ResetMask();

	// Compute (1-mask)*primary + mask*background
	// NOTE: Primary can be destination.
	void AlphaBlendOver(MTI_REAL32 *primary, MTI_REAL32 *backgroundImage, MTI_REAL32 *destination);
	void AlphaBlendOver(MTI_UINT16 *primary, MTI_UINT16 *backgroundImage, MTI_UINT16 *destination);

	MTI_REAL32 *AlphaBlend(MTI_REAL32 *primary, MTI_REAL32 *backgroundImage,
		MTI_REAL32 *destination = 0);

	int GetMaskSizeInBytes();
	int GetMaskSizeInPixels();
	int GetMaskSizeInComponents();
   RECT GetMaskBoundingBox();

private:
   IppiSize _wholeImageSize;
   MTI_REAL32 *_wholeImageMask;
   MTI_REAL32 *_tempImage;
   RECT _maskBoundingBox;
   CThreadLock _threadLock;
};

//---------------------------------------------------------------------------
#endif
