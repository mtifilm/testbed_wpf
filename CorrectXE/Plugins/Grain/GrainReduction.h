/* GrainReduction.h: algorithms for noise/grain reduction */

#ifndef GrainReductionH
#define GrainReductionH

#include "Repair.h"

#if !defined(SUCCESS)
#define SUCCESS 0
#endif 

#if !defined(FAILURE)
#define FAILURE -1
#endif 

/***** class CGrainReduction: Noise and Grain Reduction 
 * Notes:
 * (1) setImageSize() is an alternative to the base class (CRepair)
 *     method setImageDimension(). It ensures that the image dimensions
 *     are correctly set to those of the entire image.
 * (2) See the CRepair interface for other key  methods. 
 *******/

class CGrainReduction : public CRepair {
public:

  CGrainReduction();
  ~CGrainReduction();

  // NOTE!
  // mbraca added the last arg bNoAssemblerHack to tell the algorithm to
  // not use assembler code because the assembler code does not work when
  // the data range uses the whole 16 bits!!!
  //
  void Initialize(long lNProcessingThreadsArg, bool bDoRowSlicesArg,
                  long lNSlicesPerThreadArg, bool bNoopProcessingArg,
                  bool bNoAssemblerHackArg);

  /* mandatory inputs: set prior to allocate() */

  void setImageSize(long lWidth, long lHeight);
  void setGrainNeighborhoodSize(long lRowRadius, long lColRadius, long lTemporalSupport);
  void setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy);
  int setContrast(float *fpGrainContrast, long lNComponent);
  void setTempWeight(float weight);

  /* methods */

  long allocate();
  void deallocate();
  void doGrainFilterJob(int jobNum);  // external because called by trampoline
  long doGrainFilter();


private:

  long establishNeighborOffsets();
  long doGrainFilter(long lULCx, long lULCy, long lLRCx, long lLRCy);

private:
  /* inputs */
  float fWeight;
  long lGrainRow, lGrainCol;      // spatial radius of grain neighborhood
  long lTemporalSupport;          //temporal support of grain neighborhood
  float faContrastPercent[MAX_COMPONENT];   // contrast percent
  long laContrast[MAX_COMPONENT]; // 1 <= contrast <= (max[comp] - min[comp])

  long lULCx,lULCy,lLRCx,lLRCy;   /* rectangular ROI */

  long lNProcessingThreads;
  long lNSlicesPerThread;
  bool bDoRowSlices;
  bool bNoopProcessing;
  bool bNoAssemblerHack;

  /* internals */
  long *lpNOffset;        
  long **lpNeighborOffset;
  long lSeed;

}; /* CGrainReduction */

#endif // GRAIN_REDUCTION_H

