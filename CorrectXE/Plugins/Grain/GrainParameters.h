#ifndef GrainParametersH
#define GrainParametersH

#include "GrainCoreApi.h"

#include <algorithm>
#include <mutex>
#include <stdlib>
#include <string>
#include "IniFile.h"
#include "PDL.h"

// This structure defines the grain parameters
MTI_GRAINCORE_API struct GrainParameters
{
	double GrainSize;
	double Lowlights;
	double Highlights;
	double Saturation;
	double Roughness;

	GrainParameters();
    GrainParameters(const GrainParameters &gp);
	void SetDefaults();

	bool operator == (const GrainParameters &g) const;
	bool operator != (const GrainParameters &g) const;
	GrainParameters& operator = (const GrainParameters & g);

	void ReadSettings(CIniFile *ini, const std::string &grainName);
	void WriteSettings(CIniFile *ini, const std::string &grainName) const;

	void ReadPDLEntry(CPDLElement *pdlToolAttribs);
	void WritePDLEntry(CPDLElement *pdlToolAttribs) const;
};
#endif