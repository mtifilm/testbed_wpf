/* CGrainReduction.cpp: algorithms for noise/grain reduction */

#include "GrainReduction.h"
#include "IniFile.h"   // for MTIassert... uggh
#include "mt_math.h"
#include "math.h"
#include "mthread.h"

/* neighborhood labels */
#define LAB_NONE 0x0
#define LAB_USED 0x1
#define LAB_RESERVED 0x2
#define NUM_GRAIN_OFFSET 9

CGrainReduction::CGrainReduction() :
  CRepair()
{
  lGrainRow = 0; 
  lGrainCol = 0;  
  lTemporalSupport = 0;
  lULCx = 0;
  lULCy = 0;
  lLRCx = 0;
  lLRCy = 0;
  lSeed = 13151719;
  lpNOffset = NULL;
  lpNeighborOffset = NULL;
  for(long lComp = 0; lComp < MAX_COMPONENT; lComp++)
  {
    faContrastPercent[lComp] = 0.0;
    laContrast[lComp] = 1;    // 0 < contrast < (max[comp] - min[comp])
  }
  lNProcessingThreads = 1;
  bDoRowSlices = true;
  lNSlicesPerThread = 1;
  bNoopProcessing = false;
}

CGrainReduction::~CGrainReduction() {
  deallocate();
}

// NOTE!
// mbraca added the last arg bNoAssemblerHack to tell the algorithm to
// not use assembler code because the assembler code does not work when
// the data range uses the whole 16 bits!!!
//
void CGrainReduction::Initialize(long lNProcessingThreadsArg,
                                 bool bDoRowSlicesArg,
                                 long lNSlicesPerThreadArg,
                                 bool bNoopProcessingArg,
                                 bool bNoAssemblerHackArg)
{
  lNProcessingThreads = lNProcessingThreadsArg;
  bDoRowSlices = bDoRowSlicesArg;
  lNSlicesPerThread = lNSlicesPerThreadArg;
  bNoopProcessing = bNoopProcessingArg;
  bNoAssemblerHack = bNoAssemblerHackArg;

  if (lNProcessingThreads < 1)
     lNProcessingThreads = 1;
  bDoRowSlices = bDoRowSlicesArg;
  lNSlicesPerThread = lNSlicesPerThreadArg;
  if (lNSlicesPerThread < 1)
     lNSlicesPerThread = 1;
  bNoopProcessing = bNoopProcessingArg;
}

void CGrainReduction::setImageSize(long lWidth, long lHeight) {
  /* 
   * Provide this alternative to the base class (CRepair) method
   * setImageDimension() to ensure that the image dimensions are 
   * properly set to those of the entire image.
   */

  (void) setImageDimension(0,0,lHeight,lWidth,lHeight,lWidth);

}

void CGrainReduction::setGrainNeighborhoodSize(long lRowRadius, long lColRadius, long TemporalSupport) {
  lGrainRow = lRowRadius; 
  lGrainCol = lColRadius;   
  lTemporalSupport = TemporalSupport;  
}

void CGrainReduction::setROI(long lUserULCx,long lUserULCy, long lUserLRCx,
			     long lUserLRCy) {
  lULCx = lUserULCx;
  lULCy = lUserULCy;
  lLRCx = lUserLRCx;
  lLRCy = lUserLRCy;
}

int CGrainReduction::setContrast(float *fpGrainContrast, long lNComponent) {
  if (lNComponent < 0 || lNComponent > lNCom)
    return FAILURE; 
  for(long lComp = 0; lComp < lNComponent; lComp++) 
    faContrastPercent[lComp] = fpGrainContrast[lComp];
  return SUCCESS;  
}


void CGrainReduction::setTempWeight(float weight) {

     fWeight = (weight/100);
}

long CGrainReduction::allocate() {
  long lRet;

  /* check for bad/unset parameters */
 
  if (lNRow <= 0 || lNCol <= 0 || lNCom <= 0)
    return FAILURE;

  if (lGrainRow <= 0 || lGrainCol <= 0 || lTemporalSupport <= 0)
    return FAILURE;

  if (lULCx < 0 || lULCx >= lLRCx || lLRCx > lNCol ||
      lULCy < 0 || lULCy >= lLRCy || lLRCy > lNRow)
    return FAILURE;

  /* convert contrast from percentage of component range */
  // I DON'T KNOW WHY THIS IS DONE IN ALLOCATE() INSTEAD OF IN setContrast() QQQ
  for(long lComp = 0; lComp < lNCom; lComp++)
  {
    laContrast[lComp] = long(floor((faContrastPercent[lComp] / 100.0) *
                                   (laMaxValue[lComp] - laMinValue[lComp])));
    if (laContrast[lComp] < 1)
      laContrast[lComp] = 1;
  }

  /* allocate and compute the Neighbor Offsets */

  lRet = establishNeighborOffsets();
  if (lRet != SUCCESS)
    return lRet;

  return SUCCESS;
} /* allocate() */

long CGrainReduction::establishNeighborOffsets() {
  long lTNeighbor, lRC, lCount, lUsed, lPos, lPosRow, lPosCol, lR, lC,
       lNewPos, lNewPosRow, lNewPosCol;

  /* allocate the Neighbor Offsets */

  long lRowDim = 2*lGrainRow + 1;
  long lColDim = 2*lGrainCol + 1;

  lpNOffset = (long *) calloc(lTemporalSupport, sizeof(*lpNOffset));
  if (lpNOffset == NULL)
    return FAILURE;

  lpNeighborOffset = (long **) calloc(lTemporalSupport, sizeof(*lpNeighborOffset));
  if (lpNeighborOffset == NULL)
    return FAILURE;

  for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++)
    lpNeighborOffset[lTNeighbor] = NULL;

  for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++) {
    lpNeighborOffset[lTNeighbor] = (long *) calloc(lRowDim*lColDim, sizeof(**lpNeighborOffset));
    if (lpNeighborOffset[lTNeighbor] == NULL)
      return FAILURE; 
  }

  /* Now generate the lookup table for a random spatio-temporal neighborhood.
   * At each time step, a random set of elts is chosen from the entire
   * spatial neighborhood such that no elt in the set is a nearest neighbor
   * (spatially or temporally) of any other elt. Use the algorithm:
   * (1) choose a random position and label it as USED
   * (2) mark its nearest spatial and temporal neighbors RESERVED
   * (3) repeat until all positions are labelled
   * (4) translate USED positions into linear offsets
   */

  for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++)
    for (lRC = 0; lRC < lRowDim * lColDim; lRC++)
      lpNeighborOffset[lTNeighbor][lRC] = LAB_NONE;
  
  for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++) {

    /* the last time-step is special because it is the bridge 
     * between time-steps 0 and N-2. Keep only the double RESERVED
     * positions; these are elts that have neighbors at time-steps 0
     * and N-2.
     */
    
    if (lTNeighbor == lTemporalSupport - 1) {
      for (lRC = 0; lRC < lRowDim * lColDim; lRC++) {
	if (lpNeighborOffset[lTNeighbor][lRC] == LAB_RESERVED)
	  lpNeighborOffset[lTNeighbor][lRC] = LAB_NONE;
	else if (lpNeighborOffset[lTNeighbor][lRC] == LAB_RESERVED * 2)
	  lpNeighborOffset[lTNeighbor][lRC] = LAB_RESERVED;
      }
    }

    /* count the number of unlabeled positions at this time-step */
    lCount = 0;
    for (lRC = 0; lRC < lRowDim * lColDim; lRC++)
      if (lpNeighborOffset[lTNeighbor][lRC] != LAB_NONE)
        lCount++;
    
    /* construct a random subset elements of the entire spatial neighborhood 
     * at time-step t such that:
     * (a) #elts < NUM_GRAIN_OFFSET
     * (b) no elt is a nearest neighbor to another elt (spatially--(n,s,e,w))
     * (c) no elt is a temporal nearest neighbor to an elt at time-step t-1.   
     */
 
    lUsed = 0;
    while (lCount < lRowDim * lColDim && lUsed < NUM_GRAIN_OFFSET) {
      /* [select a random spatial element] */
      lPos = lRowDim * lColDim * ran266(&lSeed);
      if (lpNeighborOffset[lTNeighbor][lPos] == LAB_NONE) {
        lpNeighborOffset[lTNeighbor][lPos] = LAB_USED;
        lCount++;
        lUsed++;

        lPosRow = lPos / lColDim;
        lPosCol = lPos % lColDim;

	/* RESERVE the nearest spatial (n,s,e,w) neighbors (as yet unlabeled) */
        for (lR = -1; lR <= 1; lR++) {
	  for (lC = -1; lC <= 1; lC++) {
	    if (lR != 0 && lC != 0)
	      continue;
	    lNewPosRow = lR + lPosRow;
	    lNewPosCol = lC + lPosCol;
	    if ( lNewPosRow < 0 || lNewPosRow >= lRowDim || 
		 lNewPosCol < 0 || lNewPosCol >= lColDim )
	      continue;
	    lNewPos = lNewPosRow*lColDim + lNewPosCol;
	    if (lpNeighborOffset[lTNeighbor][lNewPos] == LAB_NONE) { 
	      lpNeighborOffset[lTNeighbor][lNewPos] = LAB_RESERVED;
	      lCount++;
	    }  
	  } /* lC */
	} /* lR */
	
	/* RESERVE the adjacent temporal neighbor at time-step t+1*/
	
        if (lTNeighbor < lTemporalSupport - 1)
	  lpNeighborOffset[lTNeighbor + 1][lPos] += LAB_RESERVED;
	
	/* the neighborhood structure at lTemporalSupport - 1
	 * must span the gap between 0 and lTemporalSupport - 2 
	 */

	/* if at time-step 0,
	 * RESERVE the adjacent temporal neighbor at time-step N-1 */

        if (lTNeighbor == 0)
	  lpNeighborOffset[lTemporalSupport - 1][lPos] += LAB_RESERVED;
      } /* if */
    } /* while */
  } /* lTNeighbor */ 

  /* Convert the USED labels into linear offsets */

  for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++) {
    lpNOffset[lTNeighbor] = 0;
    for (lRC = 0; lRC < lRowDim * lColDim; lRC++) {
      if (lpNeighborOffset[lTNeighbor][lRC] != LAB_USED)
	continue;
      lPosRow = (lRC/lColDim) - lGrainRow;
      lPosCol = (lRC%lColDim) - lGrainCol;
      lpNeighborOffset[lTNeighbor][lpNOffset[lTNeighbor]++] = (lPosRow * lNCol +
							       lPosCol) * lNCom;
     } /* lRC */
  } /* lTNeighbor */

  return SUCCESS;
} /* establishNeighborOffsets() */

void CGrainReduction::deallocate() {
  long lTNeighbor;

  free(lpNOffset);
  lpNOffset = NULL;

  if (lpNeighborOffset != NULL) {
    for (lTNeighbor = 0; lTNeighbor < lTemporalSupport; lTNeighbor++) {
      free(lpNeighborOffset[lTNeighbor]);
      lpNeighborOffset[lTNeighbor] = NULL;
    }
    free(lpNeighborOffset);
    lpNeighborOffset = NULL;
  }

}

/* doGrainFilter():
 * Inputs:
 * (1) Input Frames: see CRepair::setDataPointer()
 * (2) Output Frame: see CRepair::setResultPointer(),setRepairFrame()
 */

// Anonymous static thread start trampoline
static void trampoline(void *ptr, int jobNum)
{
  CGrainReduction *_this = (CGrainReduction *) ptr;

  _this->doGrainFilterJob(jobNum);
}

void CGrainReduction::doGrainFilterJob(int jobNum)
{

  int nJobs = lNSlicesPerThread * lNProcessingThreads;
  int startCol, stopCol, startRow, stopRow;

  if (bDoRowSlices)
  {
     int numRows = (lLRCy - lULCy) / nJobs;

     startRow = lULCy + (numRows * jobNum);
     stopRow = (jobNum == (nJobs - 1))? lLRCy : (startRow + numRows);
     startCol = lULCx;
     stopCol = lLRCx;
  }
  else
  {
     int numCols = (lLRCx - lULCx) /nJobs;

     startCol = lULCx + (numCols * jobNum);
     stopCol = (jobNum == (nJobs - 1))? lLRCx : (startCol + numCols);
     startRow = lULCy;
     stopRow = lLRCy;
  }

  doGrainFilter(startCol, startRow, stopCol, stopRow);

}


long CGrainReduction::doGrainFilter()
{

  // if the destination is NULL, this means simply return
  // The NULL value is used by GrainReduction_MT in order to only
  // process one frame.
  if (uspResult == NULL)
      return 0;

  // For testing, we let user specify no processing so we can measure
  // overhead per frame
  if (bNoopProcessing)
     return 0;

  // Detect mask all 100%
  // MOVE THIS TO GrainReduction_MT.h
  if (ucpROIBlend != 0)
  {
     bool all100percent = true;
     for(long lRow = lULCy; lRow < lLRCy; lRow++)
     {
       long lRowIndex = lRow*lNCol;
       for(long lCol = lULCx; lCol < lLRCx; lCol++)
       {
          if (ucpROIBlend[lRowIndex + lCol] != 255)
          {
             all100percent = false;
             break;
          }
       }
     }
     if (all100percent)
        ucpROIBlend = 0;
  }

  if (lNProcessingThreads < 2)
  {
     doGrainFilter(lULCx, lULCy, lLRCx, lLRCy);
  }
  else
  {
    MTHREAD_STRUCT tsThread;

    tsThread.PrimaryFunction   = (void (*)(void *,int)) &trampoline;
    tsThread.SecondaryFunction = NULL;
    tsThread.CallBackFunction  = NULL;
    tsThread.vpApplicationData = this;
    tsThread.iNThread          = lNProcessingThreads;
    tsThread.iNJob             = lNSlicesPerThread * lNProcessingThreads;

    MThreadAlloc(&tsThread);
    MThreadStart(&tsThread);
    MThreadFree(&tsThread);
  }

  return SUCCESS;
}

long CGrainReduction::doGrainFilter(long lULCx, long lULCy, long lLRCx, long lLRCy)
{
  float faWeight[5];

  // determine the weight for each frame
  for (int i = 0; i < 5; i++)
   {
    faWeight[i] = 0.;
   }

  for(long lFrame = 0; lFrame < lNFrame; lFrame++)
   {
    int iDiff = laTemporalIndex[lFrame] - laTemporalIndex[lRepairFrame];
    if (iDiff < 0)
      iDiff = -iDiff;

    if (iDiff == 0)
     {
      faWeight[lFrame] = 1.;
     }
    else if (iDiff == 1)
     {
      if (fWeight <= .5)
       {
        faWeight[lFrame] = fWeight / .5;
       }
      else
       {
        faWeight[lFrame] = 1.;
       }
     }
    else if (iDiff == 2)
     {
      if (fWeight <= .5)
       {
        faWeight[lFrame] = 0.;
       }
      else if (fWeight < 1.)
       {
        faWeight[lFrame] = (fWeight-.5) / .5;
       }
      else
       {
        faWeight[lFrame] = 1.;
       }
     }
   }

  long lLocPitch = lNCol*lNCom;
  long lMaxOffset = lNCol*lNRow*lNCom - 1;

  if (lNCom != 3)
  {
    for(long lComp = 0; lComp < lNCom; lComp++) {
      long lContrast = laContrast[lComp];
      long lRowOffset = (lULCy*lNCol + lULCx)*lNCom + lComp;
      for(long lRow = lULCy; lRow < lLRCy; lRow++) {
        bool bCheckOffset = false;
        if (lRow <= lGrainRow || lRow >= lNRow - lGrainRow - 1)
          bCheckOffset = true;

        MTI_UINT16 *uspIn = uspData[lRepairFrame] + lRowOffset;
        MTI_UINT16 *uspOut = uspResult + lRowOffset;
        const MTI_UINT8* ucpBM = 0;
        if (ucpROIBlend != 0)
          ucpBM = ucpROIBlend + lRowOffset/lNCom;
        lRowOffset += lLocPitch;
        for(long lCol = lULCx; lCol < lLRCx; lCol++) {
          if (ucpBM == 0 || *ucpBM > 0) {
            long lLowerBound = *uspIn - lContrast;
            long lUpperBound = *uspIn + lContrast;
            float fSum = *uspIn;
            float fNSum = 1.;
            for(long lFrame = 0; lFrame < lNFrame; lFrame++)
            if (faWeight[lFrame] > 0.)
             {
              long lTNeighbor = laTemporalIndex[lFrame] % lTemporalSupport;
              long lNOffset = lpNOffset[lTNeighbor];
              long *lpOffset = lpNeighborOffset[lTNeighbor];
              for(long lCnt = 0; lCnt < lNOffset; lCnt++) {
                long lOffset = (uspOut - uspResult) + lpOffset[lCnt];
                if ( bCheckOffset &&
                     (lOffset < 0 || lOffset > lMaxOffset) )
                  continue;
                long lVal = uspData[lFrame][lOffset];
                if (lVal > lLowerBound && lVal < lUpperBound) {
                  fSum += (float)lVal * faWeight[lFrame];
                  fNSum += faWeight[lFrame];
                }
              } /* lCnt */ 
            } /* lFrame */
            float fNew = fSum/fNSum;
            float fOld = (float)*uspIn;

            if (ucpBM != 0 && *ucpBM != 255)
             {
              float fWei = (float)(*ucpBM) / 255.;
              fNew = fWei * fNew + (1.-fWei)*fOld;
             }
            *uspOut = (MTI_UINT16) (fNew + 0.5);
          } /* if SELECTED */
          uspIn += lNCom;
          uspOut += lNCom;
          if (ucpBM != 0)
            ucpBM++;
        } /* lCol */
      } /* lRow */
    } /* lComp */
  }
  else if (ucpROIBlend == 0) //  && lNCom == 3
  {
    long lRowOffset0 = (lULCy*lNCol + lULCx)*lNCom;

    for (long lRow = lULCy; lRow < lLRCy; lRow++)
    {
      // Edge effects
      bool bCheckOffset = false;
      if (lRow <= lGrainRow || lRow >= lNRow - lGrainRow - 1)
        bCheckOffset = true;

      MTI_UINT16 *uspIn0 = uspData[lRepairFrame] + lRowOffset0;
      MTI_UINT16 *uspOut0 = uspResult + lRowOffset0;

      for (long lCol = lULCx; lCol < lLRCx; lCol++)
      {
        //for(lComp = 0; lComp < lNCom; lComp++)
        {
          float fSum[3];
          float fNSum[3];
          //fSum = float(uspIn[lComp]);
          fSum[0] = float(uspIn0[0]);
          fSum[1] = float(uspIn0[1]);
          fSum[2] = float(uspIn0[2]);
          //fNSum = 1.;
          fNSum[0] = fNSum[1] = fNSum[2] = 1.F;

          for (long lFrame = 0; lFrame < lNFrame; lFrame++)
          if (faWeight[lFrame] > 0.)
          {
            long lTNeighbor = laTemporalIndex[lFrame] % lTemporalSupport;
            long lNOffset = lpNOffset[lTNeighbor];
            long *lpOffset = lpNeighborOffset[lTNeighbor];
            for (long lCnt = 0; lCnt < lNOffset; lCnt++)
            {
              //lOffset = (uspOut + lComp - uspResult) + lpOffset[lCnt];
              long lOffset0 = (uspOut0 - uspResult) + lpOffset[lCnt];
              if (bCheckOffset)
              {
                if (lOffset0 < 0 || lOffset0 > lMaxOffset)
                continue;
              }

              MTI_UINT16 *uspVal0 = &uspData[lFrame][lOffset0];
              for (int iComp = 0; iComp < 3; ++iComp)
              {
                long lVal = uspVal0[iComp];
                if (lVal > (uspIn0[iComp] - laContrast[iComp]) &&
                lVal < (uspIn0[iComp] + laContrast[iComp]) )
                {
                  float fVal = faWeight[lFrame];
				  fNSum[iComp] += fVal;
                  fVal = fVal * (float) lVal;
                  fSum[iComp] += fVal;
                }
              }  /* iComp */
            } /* lCnt */
          } /* lFrame */

#ifdef _WIN64
 			uspOut0[0] = (MTI_UINT16) (fSum[0]/fNSum[0] + 0.5);
			uspOut0[1] = (MTI_UINT16) (fSum[1]/fNSum[1] + 0.5);
			uspOut0[2] = (MTI_UINT16) (fSum[2]/fNSum[2] + 0.5);
#else
		  if (bNoAssemblerHack)
		  {
			uspOut0[0] = (MTI_UINT16) (fSum[0]/fNSum[0] + 0.5);
			uspOut0[1] = (MTI_UINT16) (fSum[1]/fNSum[1] + 0.5);
			uspOut0[2] = (MTI_UINT16) (fSum[2]/fNSum[2] + 0.5);
          }
          else
          {
            float fSum0 = fSum[0];
            float fSum1 = fSum[1];
            float fSum2 = fSum[2];
            float fNSum0 = fNSum[0];
            float fNSum1 = fNSum[1];
            float fNSum2 = fNSum[2];
            unsigned short usVal;

            _asm { fld dword ptr [fSum0] }
            _asm { fld dword ptr [fNSum0] }
            _asm { fdiv }
            _asm { fistp word ptr [usVal] }
            *uspOut0 = usVal;

            _asm { fld dword ptr [fSum1] }
            _asm { fld dword ptr [fNSum1] }
            _asm { fdiv }
            _asm { fistp word ptr [usVal] }
            *(uspOut0+1) = usVal;

            _asm { fld dword ptr [fSum2] }
            _asm { fld dword ptr [fNSum2] }
            _asm { fdiv }
            _asm { fistp word ptr [usVal] };
			*(uspOut0+2) = usVal;
		  }
#endif
        } /* lComp */

        uspIn0 += lNCom;
        uspOut0 += lNCom;

      } /* lCol */

      lRowOffset0 += lLocPitch;

    } /* lRow */
  }
  else // ucpROIBlend != 0  && lNCom == 3
  {
    long lRowOffset0 = (lULCy*lNCol + lULCx)*lNCom;

    for(long lRow = lULCy; lRow < lLRCy; lRow++)
    {
      // Edge effects
      bool bCheckOffset = false;
      if (lRow <= lGrainRow || lRow >= lNRow - lGrainRow - 1)
        bCheckOffset = true;

      MTI_UINT16 *uspIn0 = uspData[lRepairFrame] + lRowOffset0;
      MTI_UINT16 *uspOut0 = uspResult + lRowOffset0;
      const MTI_UINT8 *ucpBM = ucpROIBlend + lRowOffset0/lNCom;

      for(long lCol = lULCx; lCol < lLRCx; lCol++)
      {
	for(long lComp = 0; lComp < lNCom; lComp++)
	{
	  if (*ucpBM > 0)
	  {
	    float fSum = float(uspIn0[lComp]);
	    float fNSum = 1.;
	    for(long lFrame = 0; lFrame < lNFrame; lFrame++)
	    if (faWeight[lFrame] > 0.)
	    {
	      long lTNeighbor = laTemporalIndex[lFrame] % lTemporalSupport;
	      long lNOffset = lpNOffset[lTNeighbor];
	      long *lpOffset = lpNeighborOffset[lTNeighbor];
	      for (long lCnt = 0; lCnt < lNOffset; lCnt++)
	      {
		long lOffset = (uspOut0 + lComp - uspResult) + lpOffset[lCnt];
		if ( bCheckOffset &&
		     (lOffset < 0 || lOffset > lMaxOffset) )
		  continue;

		long lVal = uspData[lFrame][lOffset];
		long lLowerBound = uspIn0[lComp] - laContrast[lComp];
		long lUpperBound = uspIn0[lComp] + laContrast[lComp];
		if (lVal > lLowerBound && lVal < lUpperBound)
		{
		  fSum += (float)lVal * faWeight[lFrame];
		  fNSum += faWeight[lFrame];
		}

	      } /* lCnt */
	    } /* lFrame */

	    float fNew = fSum/fNSum;

	    if (*ucpBM != 255)
	    {
	      float fOld = (float)uspIn0[lComp];
	      float fWei = (float)(*ucpBM) / 255.;
	      fNew = fWei * fNew + (1.-fWei)*fOld;
	    }

	    uspOut0[lComp] = (MTI_UINT16) (fNew + 0.5);

	  } /* if not masked out completely */
	} /* lComp */

	uspIn0 += lNCom;
	uspOut0 += lNCom;
	ucpBM++;

      } /* lCol */

      lRowOffset0 += lLocPitch;

    } /* lRow */
  } /* lNCom =? 3 */
  return SUCCESS;
}
