//---------------------------------------------------------------------------

#pragma hdrstop

#include "GrainToolParameters.h"

#include "ClipAPI.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)


// -------------------------------------------------------------------------
// Static strings for keywords, etc. used in Grain tool's ini file and PDL entry

string CGrainToolParameters::iniSectionName = "GrainParameters";
string CGrainToolParameters::sizeKey = "Size";
string CGrainToolParameters::contrast0Key = "Contrast0";
string CGrainToolParameters::contrast1Key = "Contrast1";
string CGrainToolParameters::contrast2Key = "Contrast2";
string CGrainToolParameters::tempweightKey = "TemperalWeight";

string CGrainToolParameters::reduceKey = "Reduce";
string CGrainToolParameters::generateKey = "Generate";
// -------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////
// CGrainToolParameters:
//////////////////////////////////////////////////////////////////////

CGrainToolParameters::CGrainToolParameters()
 : CToolParameters(1, 1)
{
   Reset();
}

CGrainToolParameters::~CGrainToolParameters()
{
}

//---------------------------------------------------------------------

void CGrainToolParameters::Reset()
{
   // initialize grain parameters
   lGrainRow = GRN_DEFAULT_SIZE;
   lGrainCol = GRN_DEFAULT_SIZE;
   lTemporalSupport = GRN_DEFAULT_SUPPORT;
   lPastFrames = GRN_DEFAULT_PAST;
   lFutureFrames = GRN_DEFAULT_FUTURE;
   lTempWeight = (long)GRN_DEFAULT_TEMP_WEIGHT;
   lInWrap = -1;
   lOutWrap = -1;
   lNFrames = lPastFrames + lFutureFrames + 1;
   for(long lComp = 0; lComp < MAX_COMPONENT; lComp++)
	  faContrast[lComp] = GRN_DEFAULT_CONTRAST;

   bReduce = true;
   bGenerate = true;
}

//---------------------------------------------------------------------

void CGrainToolParameters::ReadSettings(const string &IniFileName)
{
  if (IniFileName.empty())
     return;

  CIniFile* ini = CreateIniFile(IniFileName);
  if (ini == NULL)
     return;

   // read parameters from ini file
  lGrainRow = ini->ReadInteger(iniSectionName, sizeKey, GRN_DEFAULT_SIZE);
  lGrainCol = ini->ReadInteger(iniSectionName, sizeKey, GRN_DEFAULT_SIZE);
  faContrast[0] = ini->ReadDouble(iniSectionName, contrast0Key,
                                  GRN_DEFAULT_CONTRAST);
  faContrast[1] = ini->ReadDouble(iniSectionName, contrast1Key,
                                  GRN_DEFAULT_CONTRAST);
  faContrast[2] = ini->ReadDouble(iniSectionName, contrast2Key,
                                  GRN_DEFAULT_CONTRAST);
  lTempWeight = ini->ReadInteger(iniSectionName, tempweightKey,
								  (long)GRN_DEFAULT_TEMP_WEIGHT);

  this->bReduce = ini->ReadBool(iniSectionName, reduceKey, TRUE);
  this->bGenerate = ini->ReadBool(iniSectionName, generateKey, TRUE);
  delete ini;
}

void CGrainToolParameters::WriteSettings(const string &IniFileName)
{
  if (IniFileName.empty())
     return;

  CIniFile* ini = CreateIniFile(IniFileName);
  if (ini == NULL)
     return;

  // write parameters to ini file
  ini->WriteInteger(iniSectionName, sizeKey, lGrainRow);
  ini->WriteDouble(iniSectionName, contrast0Key, faContrast[0]);
  ini->WriteDouble(iniSectionName, contrast1Key, faContrast[1]);
  ini->WriteDouble(iniSectionName, contrast2Key, faContrast[2]);
  ini->WriteInteger(iniSectionName, tempweightKey, lTempWeight);

  ini->WriteBool(iniSectionName, reduceKey, this->bReduce);
  ini->WriteBool(iniSectionName, generateKey, this->bGenerate);
  delete ini;
}

//---------------------------------------------------------------------

void CGrainToolParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
	// Read Grain Parameters from a PDL Entry's Tool Attributes

	lGrainRow = pdlToolAttribs->GetAttribInteger(sizeKey, lGrainRow);
	lGrainCol = pdlToolAttribs->GetAttribInteger(sizeKey, lGrainCol);
	faContrast[0] = pdlToolAttribs->GetAttribDouble(contrast0Key, faContrast[0]);
	faContrast[1] = pdlToolAttribs->GetAttribDouble(contrast1Key, faContrast[1]);
	faContrast[2] = pdlToolAttribs->GetAttribDouble(contrast2Key, faContrast[2]);
	lTempWeight = pdlToolAttribs->GetAttribInteger(tempweightKey, lTempWeight);
	bReduce = pdlToolAttribs->GetAttribBool(reduceKey, bReduce);
	bGenerate = pdlToolAttribs->GetAttribBool(generateKey, bGenerate);

	grainParameters.ReadPDLEntry(pdlToolAttribs);
}

void CGrainToolParameters::WritePDLEntry(CPDLElement &parent)
{
	// Write Grain Parameters to a PDL Entry's Tool Attributes

	CPDLElement *pdlToolAttribs = parent.MakeNewChild(iniSectionName);

	pdlToolAttribs->SetAttribInteger(sizeKey, lGrainRow);
	pdlToolAttribs->SetAttribDouble(contrast0Key, faContrast[0]);
	pdlToolAttribs->SetAttribDouble(contrast1Key, faContrast[1]);
	pdlToolAttribs->SetAttribDouble(contrast2Key, faContrast[2]);
	pdlToolAttribs->SetAttribInteger(tempweightKey, lTempWeight);

	pdlToolAttribs->SetAttribBool(reduceKey, bReduce);
	pdlToolAttribs->SetAttribBool(generateKey, bGenerate);

	grainParameters.WritePDLEntry(pdlToolAttribs);
}

/* setWrapping(): determines the frame wrapping behavior of the grain tool.
 * previewFrame is the frame index of the frame for Preview 1 or -1
 * for Preview/Render.
 */

void CGrainToolParameters::setWrapping(CToolSystemInterface *systemAPI,
                                       int previewFrame)
{
   int markIn, markOut;

   // initialize
   lInWrap = lOutWrap = -1;

   markIn = systemAPI->getMarkIn();
   markOut = systemAPI->getMarkOut();

   CVideoFrameList *frameList;
   frameList = systemAPI->getVideoFrameList();
   if (frameList == NULL) return;

   if (markIn < 0)
      lInWrap = frameList->getInFrameIndex();
   else {
      if (previewFrame >= 0 && previewFrame < markIn) {
         lInWrap = frameList->getInFrameIndex();
         lOutWrap = frameList->getOutFrameIndex();
      }
      else
         lInWrap = markIn;
   }

   if (lOutWrap >= 0 || markOut < 0)
      lOutWrap = frameList->getOutFrameIndex();
   else {
      if (previewFrame >= 0 && previewFrame >= markOut) {
         lInWrap = frameList->getInFrameIndex();
         lOutWrap = frameList->getOutFrameIndex();
      }
	  else
		 lOutWrap = markOut;
   }

#if 0
   char msg[200];
   sprintf(msg,"Wrapping: in = %d out = %d",lInWrap,lOutWrap);
   _MTIErrorDialog(Handle, msg);
#endif
}
