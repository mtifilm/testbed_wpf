// ---------------------------------------------------------------------------
#include "GrainGenerator.h"

#include "HRTimer.h"
#include "ImageDatumConvert.h"
#include "SynchronousThreadRunner.h"

#ifdef _DEBUG
//#define NO_MULTI
#endif
// -----------------------------------------------------------------------------------------------

namespace
{
// -----------------------------------------------------------------------------------------------

struct ComputeRoughnessImageParams
{
   const Ipp32f* pOriginal;
   IppiSize dstSize;
	float lowLights;
   float highLights;
   float roughness;
   Ipp32f *roughnessImage; // output
};

int ComputeRoughnessImageForOneSlice(void *vp, int sliceNumber, int totalSlices)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<ComputeRoughnessImageParams *>(vp);
	int len = params->dstSize.height * params->dstSize.width;

   // Compute length of this slice.
   // Evenly distribute n extra values over the first n slices.
   auto nominalSliceLen = len / totalSlices;
   auto extraLen = len % totalSlices;
   auto sliceStartOffset = (nominalSliceLen * sliceNumber) + std::min<int>(extraLen, sliceNumber);
   auto sliceLen = nominalSliceLen + ((sliceNumber < extraLen) ? 1 : 0);

   MTIassert(params->roughnessImage != nullptr);

	float l = params->lowLights / 100.0;
	float h = params->highLights / 100.0;
	float r = params->roughness / 250.0;   // QQQ MAGIC NUMBER

	const Ipp32f* pSrc = params->pOriginal + sliceStartOffset;
   Ipp32f* pDst = params->roughnessImage + (sliceStartOffset * 3);

   float redFactor = (h - l) * 0.2989F * r;
   float greenFactor = (h - l) * 0.5870F * r;
   float blueFactor = (h - l) * 0.1140F * r;
   float baseFactor = l * r;

	for (int i = 0; i < (sliceLen * 3); i += 3)
	{
      float factor = baseFactor + (redFactor * pSrc[i]) + (greenFactor * pSrc[i + 1]) + (blueFactor * pSrc[i + 2]);
		pDst[i] = pDst[i + 1] = pDst[i + 2] = factor;
	}

   return 0;
}
// -----------------------------------------------------------------------------------------------

struct Multiply32fParams
{
   Ipp32f *src1;
   Ipp32f *src2;
   Ipp32f *dst;
   int len;
};

int Multiply32fForOneSlice(void *vp, int sliceNumber, int totalSlices)
{
   if (vp == nullptr)
   {
     return -1;
   }

   auto params = static_cast<Multiply32fParams *>(vp);

   // Compute length of this slice.
   // Evenly distribute n extra values over the first n slices.
   auto nominalSliceLen = params->len / totalSlices;
   auto extraLen = params->len % totalSlices;
   auto sliceStartOffset = (nominalSliceLen * sliceNumber) + std::min<int>(extraLen, sliceNumber);
   auto sliceLen = nominalSliceLen + ((sliceNumber < extraLen) ? 1 : 0);

   auto src1SliceStart = params->src1 + sliceStartOffset;
   auto src2SliceStart = params->src2 + sliceStartOffset;
   auto dstSliceStart = params->dst + sliceStartOffset;

   IppStatus status = ippsMul_32f(src1SliceStart, src2SliceStart, dstSliceStart, sliceLen);

   return int(status);
}

};

//----------------------------------------------------------------------
// QQQ CAREFUL - if you call this constructor directly, _grainParameters
// will be garbage because it's not initialized here!
//----------------------------------------------------------------------

GrainGenerator::GrainGenerator(const IppiSize &size)
: _destinationImageSize(size)
{
	// Set up the defaults
	_grainParametersNew.SetDefaults();

	_grainImage = ippsMalloc_32f(GetImageSizeInComponents());
	IppThrowOnMemoryError(_grainImage);

	_roughnessImage = ippsMalloc_32f(GetImageSizeInComponents());
	IppThrowOnMemoryError(_roughnessImage);

	_normalizedSourceImage = ippsMalloc_32f(GetImageSizeInComponents());
	IppThrowOnMemoryError(_normalizedSourceImage);

	_internalFormatImage = ippsMalloc_16u(GetImageSizeInComponents());
	IppThrowOnMemoryError(_internalFormatImage);
}
// -----------------------------------------------------------------------------------------------

GrainGenerator::GrainGenerator(const IppiSize &size, const GrainParameters &parameters)
	: GrainGenerator(size)
{
	_grainParameters = parameters;
}

void GrainGenerator::SetGrainParameters(const GrainParameters &parameters)
{
	_grainParametersNew = parameters;
	_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
}

const GrainParameters &GrainGenerator::GetGrainParameters() const
{
	return _grainParametersNew;
}
// -----------------------------------------------------------------------------------------------

GrainGenerator::~GrainGenerator()
{
	delete _normalRandomImage;
	_normalRandomImage = 0;

	delete _imageResizer;
	_imageResizer = 0;

	ippsFree(_grainImage);
	_grainImage = 0;

	ippsFree(_roughnessImage);
	_roughnessImage = 0;

	ippsFree(_normalizedSourceImage);
	_normalizedSourceImage = 0;

	ippsFree(_internalFormatImage);
	_internalFormatImage = 0;

	ippsFree(_internalDestinationImage);
	_internalDestinationImage = 0;

 	ippsFree(_channelMaskedGrainValues);
	_channelMaskedGrainValues = 0;

	ippsFree(_normalizedGrainValues);
	_normalizedGrainValues = 0;
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetGrainSize(double grainSize)
{
	CAutoSpinLocker lock(_lock);

	if (grainSize != _grainParametersNew.GrainSize)
	{
		_grainParametersNew.GrainSize = grainSize;
		_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
	}
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetLowlights(double lowlights)
{
	CAutoSpinLocker lock(_lock);

	if (lowlights != _grainParametersNew.Lowlights)
	{
		_grainParametersNew.Lowlights = lowlights;
		_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
	}
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetHighlights(double highlights)
{
	CAutoSpinLocker lock(_lock);

	if (highlights != _grainParametersNew.Highlights)
	{
		_grainParametersNew.Highlights = highlights;
		_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
	}
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetSaturation(double saturation)
{
	CAutoSpinLocker lock(_lock);

	if (saturation != _grainParametersNew.Saturation)
	{
		_grainParametersNew.Saturation = saturation;
		_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
	}
}

// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetRoughness(double roughness)
{
	CAutoSpinLocker lock(_lock);

	if (roughness != _grainParametersNew.Roughness)
	{
		_grainParametersNew.Roughness = roughness;
		_areGrainValuesInvalid = _grainParametersNew != _grainParameters;
	}
}
// -----------------------------------------------------------------------------------------------

MTI_UINT16 *GrainGenerator::ApplyGrain(const MTI_UINT16 *source, int maxValue, int channelMask, bool sourceDataIsNew, bool generateNewSeed, bool dataTypeIsUnsignedShort)
{
	return dataTypeIsUnsignedShort
		? GrainGenerator::ApplyGrainToUnsignedShortData(source, maxValue, channelMask, sourceDataIsNew, generateNewSeed)
		: GrainGenerator::ApplyGrainToHalfData(source, channelMask, sourceDataIsNew, generateNewSeed);
}
// -----------------------------------------------------------------------------------------------

MTI_UINT16 *GrainGenerator::ApplyGrainToUnsignedShortData(const MTI_UINT16 *source, int maxValue, int channelMask, bool sourceDataIsNew, bool generateNewSeed)
{
	if (!_areGrainValuesInvalid && !sourceDataIsNew && !generateNewSeed && _internalFormatImage != 0)
	{
		return _internalFormatImage;
	}

	int len = GetImageSizeInComponents();

	// Normalize data
	if (sourceDataIsNew)
	{
		IppThrowOnError(
			ippsConvert_16u32f(source, _normalizedSourceImage, len)
		);

		IppThrowOnError(
			ippsDivC_32f(_normalizedSourceImage, maxValue, _normalizedSourceImage, len)
		);
	}

	Ipp32f *tempDestinationImage = ApplyGrain(_normalizedSourceImage, channelMask, sourceDataIsNew, generateNewSeed);

	IppThrowOnError(
		ippsMulC_32f(tempDestinationImage, maxValue, tempDestinationImage, len)
	);

   // If maxValue is greater than 32767, we need to clamp in floating point
   // because there is no 16u version of ippsThreshold_GT ! If the maxValue is
   // 65535, we don't need to clamp at all.
   //
   // CAREFUL! tempDestinationImage always points to a member called
   // _internalDestinationImage. This means that sometimes the values in
   // _internalDestinationImage  are clamped, and sometimes not. But this
   // doesn't really matter at this point since this is the only place the values
   // are used, but things could get very confusing if that changes.
   //
   if (maxValue < 65535 && maxValue > 32767)
   {
      IppThrowOnError(
         ippsThreshold_GT_32f_I(tempDestinationImage, len, Ipp32f(maxValue))
	   );
   }

	IppThrowOnError(
		ippsConvert_32f16u_Sfs(tempDestinationImage, _internalFormatImage, len, ippRndNear, 0)
	);

   // Maybe need to clamp now.
   if (maxValue < 32768)
   {
      IppThrowOnError(
         ippsThreshold_GT_16s_I((Ipp16s *)_internalFormatImage, len, maxValue)
      );
   }

	return _internalFormatImage;
}
// -----------------------------------------------------------------------------------------------

MTI_UINT16 *GrainGenerator::ApplyGrainToHalfData(const MTI_UINT16 *source, int channelMask, bool sourceDataIsNew, bool generateNewSeed)
{
	if (!_areGrainValuesInvalid && !sourceDataIsNew && !generateNewSeed && _internalFormatImage != 0)
	{
		return _internalFormatImage;
	}

	int len = GetImageSizeInComponents();

	// Normalize data
	if (sourceDataIsNew)
	{
		for (int i = 0; i < len; ++i)
		{
			_normalizedSourceImage[i] = ImageDatum::toFloat(source[i], true);
		}
	}

	Ipp32f *tempDestinationImage = ApplyGrain(_normalizedSourceImage, channelMask, sourceDataIsNew, generateNewSeed);

	for (int i = 0; i < len; ++i)
	{
		_internalFormatImage[i] = ImageDatum::fromFloat(tempDestinationImage[i], true);
	}

	return _internalFormatImage;
}
// -----------------------------------------------------------------------------------------------

Ipp32f* GrainGenerator::ApplyGrain(const Ipp32f* normalizedSource, int channelMask, bool sourceDataIsNew, bool generateNewSeed)
{
//   DBTIMER(APPLY_GRAIN);

   MTIassert(channelMask >= 0 && channelMask <= 7);  // three planes, at least one must be selected.
	int len = GetImageSizeInComponents();
	if (_internalDestinationImage == 0)
	{
		_internalDestinationImage = ippsMalloc_32f(len);
	}

	const Ipp32f *grainValues = GetNormalizedGrainValues(normalizedSource, sourceDataIsNew, generateNewSeed);

   if (channelMask != ChannelMaskBits::All)
   {
      if (_channelMaskedGrainValues == 0)
      {
		   _channelMaskedGrainValues = ippsMalloc_32f(len);
      }

      for (auto i = 0; i < len; i += 3)
      {
         _channelMaskedGrainValues[i + 0] = (channelMask & ChannelMaskBits::Red) ? grainValues[i + 0] : 0;
         _channelMaskedGrainValues[i + 1] = (channelMask & ChannelMaskBits::Green) ? grainValues[i + 1] : 0;
         _channelMaskedGrainValues[i + 2] = (channelMask & ChannelMaskBits::Blue) ? grainValues[i + 2] : 0;
      }

      grainValues = _channelMaskedGrainValues;
   }

	IppThrowOnError(
		ippsAdd_32f(normalizedSource, grainValues, _internalDestinationImage, len)
	);

	return _internalDestinationImage;
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetSourceImage(const MTI_UINT16* source, int maxValue, bool dataIsUnsignedShorts)
{
//   DBTIMER(SET_SOURCE_IMAGE);

	int len = GetImageSizeInComponents();

	_dataIsUnsignedShorts = dataIsUnsignedShorts;
	if (dataIsUnsignedShorts)
	{
		IppThrowOnError(
			ippsConvert_16u32f(source, _normalizedSourceImage, len)
		);

		IppThrowOnError(
			ippsDivC_32f(_normalizedSourceImage, maxValue, _normalizedSourceImage, len)
		);
	}
	else
	{
		for (int i = 0; i < len; ++i)
		{
			_normalizedSourceImage[i] = ImageDatum::toFloat(source[i], true);
		}
	}

	_areGrainValuesInvalid = true;
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::SetSeed(unsigned int seed)
{
	_randomImageSeed = seed;
	_randomImageSeedWasSet = true;
	_randomImageSeedWasChanged = true;
	_areGrainValuesInvalid = true;
}
// -----------------------------------------------------------------------------------------------

void GrainGenerator::UnsetSeed()
{
	_randomImageSeedWasSet = false;
   _randomImageSeedWasChanged = true;
	_areGrainValuesInvalid = true;
}
// -----------------------------------------------------------------------------------------------

unsigned int GrainGenerator::GetSeed()
{
	return _randomImageSeed;
}
// -----------------------------------------------------------------------------------------------

const float *GrainGenerator::GetNormalizedGrainValues()
{
	MTIassert(_normalizedSourceImage != NULL);

	if ((!_areGrainValuesInvalid && _normalizedGrainValues != 0) || _normalizedSourceImage == NULL)
	{
		return _normalizedGrainValues;
	}

	const Ipp32f *grainValues = GetNormalizedGrainValues(_normalizedSourceImage, false, false);

	return grainValues;
}
// -----------------------------------------------------------------------------------------------

const Ipp32f* GrainGenerator::GetNormalizedGrainValues(const Ipp32f* source, bool sourceDataIsNew, bool generateNewSeed)
{
//   DBTIMER(GET_NORMALIZED_GRAIN_VALUES);

   // Lazy output array allocation.
	if (_normalizedGrainValues == 0)
	{
		_normalizedGrainValues = ippsMalloc_32f(GetImageSizeInComponents());
		_areGrainValuesInvalid = true;
	}

	// Short circuit if nothing changed.
   // NOTE: If the parameters change, _areGrainValuesInvalid will be set to true
   // so we don't have to check them here!
	if (!_areGrainValuesInvalid && !sourceDataIsNew && !generateNewSeed)
	{
      DBCOMMENT("GetNormalizedGrainValues SHORT-CIRCUITED");
		return _normalizedGrainValues;
	}

	CAutoSpinLocker lock(_lock);   // QQQ Needed?

   // QQQ Why make a local copy of this. And then why not just assign it???
   // Do we think that ImageResizer32f changes it???
	//auto destinationImageSize = IppiSize { _destinationImageSize.width, _destinationImageSize.height };

	// First see if new grain size dictates a new size for the random image.
   // The "grain size" might change but the size of the image might not also
   // change because of integer rounding.
	if (_grainParameters.GrainSize != _grainParametersNew.GrainSize)
	{
//      DBTIMER(GRAIN_VALUES_CHANGED);

		_grainParameters.GrainSize = _grainParametersNew.GrainSize;
		int newW = _destinationImageSize.width / _grainParametersNew.GrainSize;
		int newH = _destinationImageSize.height / _grainParametersNew.GrainSize;
		if (newW != _randomImageSize.width || newH != _randomImageSize.height)
      {
		   _randomImageSize = IppiSize { newW, newH };
         delete _normalRandomImage;
         _normalRandomImage = nullptr;
         delete _imageResizer;
         _imageResizer = nullptr;
         _areGrainValuesInvalid = true;
      }
	}

   if (_randomImageSeedWasChanged)
   {
      delete _normalRandomImage;
      _normalRandomImage = nullptr;
      _areGrainValuesInvalid = true;
   }

   if (generateNewSeed && _normalRandomImage != nullptr)
   {
      // This is a bit obscure. We don't actually request a new seed, we
      // just tell the normalRandomImage to regenerate the random numbers
      // without resetting the seeds from the previous pass, so just continues
      // those sequences.
		_normalRandomImage->invalidate();
		_areGrainValuesInvalid = true;
	}

	if (_grainParameters.Saturation != _grainParametersNew.Saturation)
	{
		_grainParameters.Saturation = _grainParametersNew.Saturation;
		_areGrainValuesInvalid = true;
	}

   if (_normalRandomImage == nullptr)
   {
		_normalRandomImage = new NormalRandomImageC3(_randomImageSize, _randomImageSeedWasSet ? &_randomImageSeed : nullptr);
		_areGrainValuesInvalid = true;
   }

   if (_imageResizer == nullptr)
   {
		_imageResizer = new ImageResizer32f(_randomImageSize, _destinationImageSize);
		_areGrainValuesInvalid = true;
   }

	// Resize the grain back to the original frame size.
	if (_areGrainValuesInvalid)
	{
      Ipp32fArray saturatedImage;
      {
//      DBTIMER(GENERATE_SATURATED_RANDOM_IMAGE);
        // A bug in embarcardeo prohibits us from combining the following two lines
        // The bug is the move operator is not found.
        auto tempArray = _normalRandomImage->getRandomImage(_grainParameters.Saturation);
        saturatedImage = tempArray;
      }
      {
//      DBTIMER(RANDOM_IMAGE_RESIZE);

      // Need to change resizer to take an Ipp32fArray QQQ
      MTIassert(saturatedImage.isContiguous());
      auto saturatedRandomImageData = saturatedImage.data();  // Not kosher! QQQ
		_imageResizer->Resize(saturatedRandomImageData, _grainImage);
      }
	}

	// Check for change in roughness.
   bool needToRecomputeRoughnessImage = _grainParameters.Roughness != _grainParametersNew.Roughness;
   _grainParameters.Roughness = _grainParametersNew.Roughness;

   // If new source, or the high/lowlights were changed, we need to build a
   // brand new roughness image. Also if the current image roughness is zero!
	if (sourceDataIsNew
   || _grainParameters.Lowlights != _grainParametersNew.Lowlights
   || _grainParameters.Highlights != _grainParametersNew.Highlights
   || _roughnessImageRoughness == 0)
	{
//      DBTIMER(COMPUTE_FACTORS_AND_ROUGHNESS);

		_grainParameters.Lowlights = _grainParametersNew.Lowlights;
		_grainParameters.Highlights = _grainParametersNew.Highlights;

		ComputeRoughnessImageParams params = {
				source,
            _destinationImageSize,
				float(_grainParameters.Lowlights),
				float(_grainParameters.Highlights),
            float(_grainParameters.Roughness),
            _roughnessImage // output
            };

#ifdef NO_MULTI
      ComputeRoughnessImageForOneSlice(&params, 0, 1);
#else
      SynchronousThreadRunner multithread(&params, ComputeRoughnessImageForOneSlice);
      multithread.Run();
#endif
      _roughnessImageRoughness = _grainParameters.Roughness;
		_areGrainValuesInvalid = true;
      needToRecomputeRoughnessImage = false;
	}

	if (needToRecomputeRoughnessImage)
	{
//      DBTIMER(ROUGHNESS_CHANGED);

      float roughnessChangeFactor = _grainParameters.Roughness / _roughnessImageRoughness;
		IppThrowOnError(
         ippsMulC_32f(_roughnessImage,
                      roughnessChangeFactor,
                      _roughnessImage,
                      GetImageSizeInComponents())
      );

      _roughnessImageRoughness = _grainParameters.Roughness;
      needToRecomputeRoughnessImage = false;
		_areGrainValuesInvalid = true;
	}

	// At this point _areGrainValuesInvalid would be false only if just the
   // grain size changed, but not enough to trigger a reandom image resize.
	if (_areGrainValuesInvalid)
	{
//      DBTIMER(APPLY_ROUGHNESS);

		int len = GetImageSizeInComponents();
//    for some reason, this doesn't compile...
//      Multiply32fParams params = { _grainImage, _roughnessImage, _normalizedGrainValues, len };
      Multiply32fParams params;
      params.src1 = _grainImage;
      params.src2 = _roughnessImage;
      params.dst = _normalizedGrainValues;
      params.len = len;

      // We don't bother with NO_MULTI case for this one!
      SynchronousThreadRunner multithread(2, &params, Multiply32fForOneSlice);
      multithread.Run();
	}

	_areGrainValuesInvalid = false;

	return _normalizedGrainValues;
}
// -----------------------------------------------------------------------------------------------

IppiSize GrainGenerator::GetImageSize() const
{
	return _destinationImageSize;
}
// -----------------------------------------------------------------------------------------------

bool GrainGenerator::AreGrainValuesInvalid() const
{
	return _areGrainValuesInvalid;
}
// -----------------------------------------------------------------------------------------------

int GrainGenerator::GetImageSizeInPixels() const
{
	return _destinationImageSize.width*_destinationImageSize.height;
}
// -----------------------------------------------------------------------------------------------

int GrainGenerator::GetImageSizeInComponents() const
{
	return 3 * GetImageSizeInPixels();
}
// -----------------------------------------------------------------------------------------------

int GrainGenerator::GetImageSizeInBytes() const
{
	return sizeof(Ipp32f)*GetImageSizeInComponents();
}
// -----------------------------------------------------------------------------------------------


