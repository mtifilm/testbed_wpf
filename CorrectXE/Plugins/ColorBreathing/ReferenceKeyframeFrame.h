// ---------------------------------------------------------------------------

#ifndef ReferenceKeyframeFrameH
#define ReferenceKeyframeFrameH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ImgList.hpp>

#include "ReferenceKeyframeData.h"
#include "SpinEditFrameUnit.h"
#include <Vcl.ExtCtrls.hpp>
#include "ColorPanel.h"
#include "PropX.h"
#include "timecode.h"
#include <System.ImageList.hpp>

// ---------------------------------------------------------------------------
class TReferenceKeyframeFrame : public TFrame
{
__published: // IDE-managed Components

   TSpinEditFrame *RedColorSpinEdit;
   TImageList *ColorImageList;
   TColorPanel *BasePanel;
   TColorPanel *FramePanel;
   TLabel *FrameLabel;
   TCheckBox *ColorBackwardsCheckBox;
   TCheckBox *ColorForwardsCheckBox;
   TCheckBox *SmoothingForwardsCheckBox;
   TCheckBox *SmoothingBackwardsCheckBox;
   TColorPanel *SpinEditsPositioningPanel;
   TSpinEditFrame *GreenColorSpinEdit;
   TSpinEditFrame *BlueColorSpinEdit;
   TBitBtn *DeleteButton;
   TSpeedButton *RedButton;
   TSpeedButton *GreenButton;
   TSpeedButton *BlueButton;

   void __fastcall RedButtonClick(TObject *Sender);
   void __fastcall GreenButtonClick(TObject *Sender);
   void __fastcall BlueButtonClick(TObject *Sender);
   void __fastcall ColorBackwardsCheckBoxClick(TObject *Sender);
   void __fastcall ColorForwardsCheckBoxClick(TObject *Sender);
   void __fastcall SmoothingBackwardsCheckBoxClick(TObject *Sender);
   void __fastcall SmoothingForwardsCheckBoxClick(TObject *Sender);
   void __fastcall FramePanelClick(TObject *Sender);
   void __fastcall ColorSpinEditUpButtonClick(TObject *Sender);
   void __fastcall ColorSpinEditDownButtonClick(TObject *Sender);

private: // User declarations

   ReferenceKeyframeData *_referenceKeyframeData = nullptr;

   void ConstructorInit();
   const ReferenceKeyframeData ReadFromGui() const ;

   DEFINE_CBHOOK(RedSpinEditChange, TReferenceKeyframeFrame);
   DEFINE_CBHOOK(GreenSpinEditChange, TReferenceKeyframeFrame);
   DEFINE_CBHOOK(BlueSpinEditChange, TReferenceKeyframeFrame);
   void UpdateGUI();
   void ReadGUI();

   CTimecode _inTimecode;
   bool _isSelected = false;

public: // User declarations
   __fastcall TReferenceKeyframeFrame(TComponent* Owner);
   __fastcall TReferenceKeyframeFrame(TComponent * Owner, ReferenceKeyframeData *referenceKeyframeData, const CTimecode &inTimecode);

   bool IsSelected() const;
   void SetIsSelected(const bool selected);

   const ReferenceKeyframeData GetReferenceKeyFrameData() const ;
   ReferenceKeyframeData *GetReferenceKeyFramePointer() const ;
   void SetReferenceKeyFrameData(ReferenceKeyframeData *referenceKeyframeData);
   void CopyToReferenceKeyFrameData(const ReferenceKeyframeData &referenceKeyframeData);
   void SetInTimecode(const CTimecode &startTimecode);
   void SetMasterEnabled(bool enabled);

   CBHook DataHasChanged;
   CBHook FrameClick;
};

// ---------------------------------------------------------------------------
extern PACKAGE TReferenceKeyframeFrame *referenceKeyframeFrame;
// ---------------------------------------------------------------------------
#endif
