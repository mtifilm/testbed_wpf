// ---------------------------------------------------------------------------

#pragma hdrstop

#include "GraphTimeline.h"

#include <cmath>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

//void* MTImalloc(size_t n);
//void MTImemcpy(void *pDst, const void *pSrc, size_t n);
//void* MTIcalloc(size_t n_elements, size_t element_size);
//void MTIfree(void *p);
#include "FileIO.h"
#include "IniFile.h"
#include "HRTimer.h"
#include "ThreadLocker.h"
#include "ipps.h"

#define WIDTH_FUDGE 6

static CSpinLock dataChangeLock;

CGraphTimeline::CGraphTimeline()
{
   iFlickerChannel = TL_FLICKER_CHANNELS_RGB;

   iCurrentFrame = -1;
   tvMaster.iFirst = -1;
   tvMaster.iLast = -1;
   tvGraph.iFirst = -1;
   tvGraph.iLast = -1;

   iMarkFirst = -1;
   iMarkLast = -1;

   for (int j = 0; j < TL_CHAN_COUNT; j++)
   {
      mdProcess.fpValOrig[j] = nullptr;
      mdProcess.fpValProc[j] = nullptr;
   }

   for (int i = 0; i < TL_CHAN_COUNT; i++)
      for (int j = 0; j < TL_QUANTILE_COUNT; j++)
      {
         mdProcess.fpQuantile[i][j] = nullptr;
         mdProcess.fpRepair[i][j] = nullptr;
      }

   ucpProcessFlag = nullptr;

   bAllocMeanData = false;
   fPixelPerFrame = (float)TL_PIXEL_PER_FRAME_DEFAULT;

   bShotListChanged = false;

}

CGraphTimeline::~CGraphTimeline()
{
   FreeMeanData();
   MTIfree(_cleanDataGraph);
}

void CGraphTimeline::FreeMeanData()
{
   bAllocMeanData = false;
   for (int j = 0; j < TL_CHAN_COUNT; j++)
   {
      MTIfree(mdProcess.fpValOrig[j]);
      mdProcess.fpValOrig[j] = nullptr;
      MTIfree(mdProcess.fpValOrig[j]);
      mdProcess.fpValProc[j] = nullptr;
   }

   for (int i = 0; i < TL_CHAN_COUNT; i++)
      for (int j = 0; j < TL_QUANTILE_COUNT; j++)
      {
         MTIfree(mdProcess.fpQuantile[i][j]);
         mdProcess.fpQuantile[i][j] = nullptr;
         MTIfree(mdProcess.fpRepair[i][j]);
         mdProcess.fpRepair[i][j] = nullptr; ;
      }

   MTIfree(_fpMovingAveScratch);
   _fpMovingAveScratch = nullptr;

   MTIfree(ucpProcessFlag);
   ucpProcessFlag = nullptr;
}

// void CGraphTimeline::Clear()
// {
// FreeMeanData();
//
// iCurrentFrame = -1;
// tvMaster.iFirst = -1;
// tvMaster.iLast = -1;
// tvGraph.iFirst = -1;
// tvGraph.iLast = -1;
//
// iMarkFirst = -1;
// iMarkLast = -1;
// }

void CGraphTimeline::setGraphDimension(int iWidth, int iHeight)
{
   if ((tvGraph.iWidth != iWidth) || (tvGraph.iHeight != iHeight))
   {
      tvGraph.iWidth = iWidth;
      tvGraph.iHeight = iHeight;
      SetGraphDirty();

      MTIfree(_cleanDataGraph);
      // The graph size does not include bottom line of 2 pixels
      _graphSizeinBytes = 4 * tvGraph.iWidth * (tvGraph.iHeight + 2);
      _cleanDataGraph = (unsigned char *)MTImalloc(_graphSizeinBytes);
   }
}

int CGraphTimeline::setFrames(int frames)
{
   if (frames <= 0)
   {
      return -1;
   }

   if (_totalFrames == frames)
   {
      return 0;
   }

   FreeMeanData();
   _totalFrames = frames;

   for (int i = 0; i < TL_CHAN_COUNT; i++)
      for (int j = 0; j < TL_QUANTILE_COUNT; j++)
      {
         // allocate a set of quantiles for each frame.
         mdProcess.fpQuantile[i][j] = (float *) MTImalloc((frames + 1)*sizeof(float));
         if (mdProcess.fpQuantile[i][j] == nullptr)
         {
            return -1;
         }

         // allocate a set of repairs for each frame.
         mdProcess.fpRepair[i][j] = (float *) MTImalloc((frames + 1)*sizeof(float));
         if (mdProcess.fpRepair[i][j] == nullptr)
         {
            return -1;
         }
      }

   for (int j = 0; j < TL_CHAN_COUNT; j++)
   {
      mdProcess.fpValOrig[j] = (float *)MTImalloc((frames + 1)*sizeof(float));
      if (mdProcess.fpValOrig[j] == nullptr)
      {
         return -1;
      }

      mdProcess.fpValProc[j] = (float *)MTImalloc((frames + 1)*sizeof(float));
      if (mdProcess.fpValProc[j] == nullptr)
      {
         return -1;
      }
   }

   _fpMovingAveScratch = (float *) MTImalloc((frames + 1)*sizeof(float));
   if (_fpMovingAveScratch == nullptr)
   {
      return -1;
   }

   ucpProcessFlag = (unsigned char *) MTImalloc((frames + 1)*sizeof(unsigned char));
   if (ucpProcessFlag == nullptr)
   {
      return -1;
   }

   bAllocMeanData = true;

   iGraphType = TL_GRAPH_TYPE_RGB;
   _flickerShotList.clear();
   bShotListChanged = false;
   tvGraph.fPixelPerFrame = fPixelPerFrame;

   InitializeMeanData();
   return 0;

}

void CGraphTimeline::setMarkRange(int iFirst, int iLast)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   if ((iMarkFirst != iFirst) || (iMarkLast != iLast))
   {
      iMarkFirst = iFirst;
      iMarkLast = iLast;
      SetGraphDirty();
   }
}

void CGraphTimeline::setDisplayRange(int iDisplayFirst, int iDisplayLast)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   //// iDisplayLast++;
   if (iDisplayFirst >= iDisplayLast)
   {
      return;
   }

   if ((_displayFirst == iDisplayFirst) && (_displayLast == iDisplayLast))
   {
      return;
   }

   _displayFirst = iDisplayFirst;
   _displayLast = iDisplayLast;
   SetGraphDirty();

   auto frames = _displayLast - _displayFirst;
   if (frames <= 0)
   {
      return;
   }

   fPixelPerFrame = (tvGraph.iWidth - WIDTH_FUDGE) / (double)frames;
   tvGraph.fPixelPerFrame = fPixelPerFrame;
   tvGraph.fFirstPixel = -(float)(_displayFirst) * fPixelPerFrame + TL_BORDER_PIX;
   tvGraph.iFirst = iDisplayFirst;
   tvGraph.iLast = iDisplayLast;
}

void CGraphTimeline::getSurroundingRange(int iFrame, int *ipIn, int *ipOut)
{
   *ipIn = -1;
   *ipOut = -1;

   int iShotIndex = _flickerShotList.getShotIndex(iFrame);

   if (iShotIndex == -1)
   {
      return;
   }

   *ipIn = _flickerShotList.getShotIn(iShotIndex);
   *ipOut = _flickerShotList.getShotOut(iShotIndex);
}

// void CGraphTimeline::setMasterPixel(int iPixel)
// {
// // the master pixel position has changed
// // the frame corresponding to this pixel is (iPixel-fFirstPixel) / fPixelPerFrame
// float fFrame = ((float)iPixel - tvMaster.fFirstPixel) / tvMaster.fPixelPerFrame;
// int   iFrameLocal = (int)(fFrame + .5);
// }

void CGraphTimeline::SetAllRgbData(const vector<RGBFrameDatum> &rgbData)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);
   auto frame = 0;
   for (auto rgb : rgbData)
   {
      SetFrameRgbData(frame++, rgb);
   }

   CorrectMeanData();
}

void CGraphTimeline::SetFrameRgbData(int iFrame, const RGBFrameDatum &rgbDatum)
{
   auto isProxy = (ucpProcessFlag[iFrame] & FIO_PROCESS_FLAG_PROXY) != 0;
   auto isRendered = (ucpProcessFlag[iFrame] & FIO_PROCESS_FLAG_RENDER) != 0;

   if (isRendered != rgbDatum.Rendered)
   {
      if (rgbDatum.Rendered)
      {
         ucpProcessFlag[iFrame] |= FIO_PROCESS_FLAG_RENDER;
      }
      else
      {
         ucpProcessFlag[iFrame] &= ~FIO_PROCESS_FLAG_RENDER;
      }

      _isRenderDirty = true;
   }

   bool rgbSame = (mdProcess.fpValOrig[0][iFrame] == rgbDatum.Red) && (mdProcess.fpValOrig[1][iFrame] == rgbDatum.Green) &&
       (mdProcess.fpValOrig[2][iFrame] == rgbDatum.Blue);

   // ***********************************
   // Copy everything, this should not be necessary but we are trying to track down a bug
   mdProcess.fpValOrig[0][iFrame] = rgbDatum.Red;
   mdProcess.fpValOrig[1][iFrame] = rgbDatum.Green;
   mdProcess.fpValOrig[2][iFrame] = rgbDatum.Blue;

   // Copy the repair values
   for (int i = 0; i < TL_CHAN_COUNT; i++)
   {
      mdProcess.fpValProc[i][iFrame] = mdProcess.fpValOrig[i][iFrame];
   }

   for (int i = 0; i < TL_CHAN_COUNT; i++)
      for (int j = 0; j < TL_QUANTILE_COUNT; j++)
      {
         mdProcess.fpQuantile[i][j][iFrame] = rgbDatum.Quantile[i][j];
         mdProcess.fpRepair[i][j][iFrame] = rgbDatum.Quantile[i][j];
      }

   // *************************************

   // if (isProxy == rgbDatum.Valid)
   // {
   // if (rgbSame)
   // {
   // return;
   // }
   // }

   if (rgbDatum.Valid)
   {
      ucpProcessFlag[iFrame] |= FIO_PROCESS_FLAG_PROXY;
      SetGraphDirty();
   }
   else
   {
      mdProcess.fpValOrig[0][iFrame] = -1;
      mdProcess.fpValOrig[1][iFrame] = -1;
      mdProcess.fpValOrig[2][iFrame] = -1;

      for (int i = 0; i < TL_CHAN_COUNT; i++)
         for (int j = 0; j < TL_QUANTILE_COUNT; j++)
         {
            mdProcess.fpQuantile[i][j][iFrame] = 0;
            mdProcess.fpRepair[i][j][iFrame] = 0;
         }

      ucpProcessFlag[iFrame] &= ~FIO_PROCESS_FLAG_PROXY;
   }
}

void CGraphTimeline::MarkRendered(int iFrame)
{
   ucpProcessFlag[iFrame] |= FIO_PROCESS_FLAG_RENDER;
   _isRenderDirty = true;
}

bool CGraphTimeline::IsRendered(int iFrame)
{
   return (ucpProcessFlag[iFrame] & FIO_PROCESS_FLAG_RENDER) != 0;
}

void CGraphTimeline::InitializeMeanData()
{
   for (int iFrame = 0; iFrame < _totalFrames; iFrame++)
   {
      for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
      {
         mdProcess.fpValOrig[iChan][iFrame] = -1.f;
      }

      ucpProcessFlag[iFrame] = FIO_PROCESS_FLAG_NONE;
   }
}

void CGraphTimeline::FindScaling(int iFirst, int iLast)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   // Find the original Mins/Maxes
   // find the max and min data values in the frame range
   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      float *fpValOrig = mdProcess.fpValOrig[iChan] + iFirst;
      float *fpMinOrig = &mdProcess.faMinOrig[iChan];
      float *fpMaxOrig = &mdProcess.faMaxOrig[iChan];

      *fpMinOrig = 256;
      *fpMaxOrig = -1;

      for (int iFrame = iFirst; iFrame <= iLast; iFrame++)
      {
         if (*fpValOrig != -1)
         {
            if (*fpValOrig < *fpMinOrig)
            {
               *fpMinOrig = *fpValOrig;
            }
            if (*fpValOrig > *fpMaxOrig)
            {
               *fpMaxOrig = *fpValOrig;
            }
         }

         fpValOrig++;
      }
   }

   // Find the prcessed Mins/Maxes
   // Ugly, Programming like Kurt
   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      float *fpValProc = mdProcess.fpValProc[iChan] + iFirst;
      float *fpMinProc = &mdProcess.faMinProc[iChan];
      float *fpMaxProc = &mdProcess.faMaxProc[iChan];

      *fpMinProc = 256;
      *fpMaxProc = -1;

      for (int iFrame = iFirst; iFrame <= iLast; iFrame++)
      {
         if (*fpValProc != -1)
         {
            if (*fpValProc < *fpMinProc)
            {
               *fpMinProc = *fpValProc;
            }
            if (*fpValProc > *fpMaxProc)
            {
               *fpMaxProc = *fpValProc;
            }
         }

         fpValProc++;
      }
   }
}

void CGraphTimeline::setGraphPixel(int iPixel)
{
   // the graph pixel position has changed
   // the frame corresponding to this pixel is (iPixel-fFirstPixel)/fPixelPerFrame
   iCurrentFrame = (int)(((float)iPixel - tvGraph.fFirstPixel) / tvGraph.fPixelPerFrame + .5);

   if (iCurrentFrame < tvMaster.iFirst)
   {
      iCurrentFrame = tvMaster.iFirst;
   }

   if (iCurrentFrame > tvMaster.iLast)
   {
      iCurrentFrame = tvMaster.iLast;
   }
}

int CGraphTimeline::getCursorPosition()
{
   return iCurrentFrame;
}

void CGraphTimeline::updateGraphCursorOnly(unsigned char *ucpArg)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);
   MTImemcpy(ucpArg, _cleanDataGraph, _graphSizeinBytes);
   displayCursor(ucpArg, &tvGraph);
   _isCursorPixelPositionDirty = false;
}

void CGraphTimeline::updateGraphData(unsigned char *ucpArg)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   // update the Graph timeline
   int iFirstPixel = 0;
   int iLastPixel = tvGraph.iWidth - 1;

   // initialize the background
   if ((iMarkFirst >= 0) && (iMarkLast > iMarkFirst))
   {
      auto markFirstPixel = (int)(((float)iMarkFirst) * tvGraph.fPixelPerFrame + tvGraph.fFirstPixel + .5);
      auto markLastPixel = (int)(((float)iMarkLast) * tvGraph.fPixelPerFrame + tvGraph.fFirstPixel + .5);
      FindScaling(_displayFirst, _displayLast - 1);
      displayBackground(ucpArg, &tvGraph, markFirstPixel, markLastPixel);
   }
   else
   {
      displayBackground(ucpArg, &tvGraph, -1, -1);
   }

   // show the shot boundaries
   displayShot(ucpArg, &tvGraph);

   // show the reference frames
   displayReference(ucpArg, &tvGraph);

   // show the data values, we are assuming RGB

   for (int iChan = 0; iChan < 3; iChan++)
   {
      displayValues(ucpArg, &tvGraph, 0, iChan);
   }

   for (int iChan = 0; iChan < 3; iChan++)
   {
      displayValues(ucpArg, &tvGraph, 1, iChan);
   }

   MTImemcpy(_cleanDataGraph, ucpArg, _graphSizeinBytes);
   _isGraphDataDirty = false;
   _isCursorPixelPositionDirty = true;
   _isRenderDirty = true;
}

void CGraphTimeline::updateGraph(unsigned char *ucpArg)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   ////    TRACE_0(errout << "TTT Data Dirty " <<  _isGraphDataDirty << ", Pos dirty " << _isCursorPixelPositionDirty << ", Render dirty " << _isRenderDirty );
   if (_isGraphDataDirty)
   {
      updateGraphData(ucpArg);

      // position of the cursor, this saves a memory copy
      displayCursor(ucpArg, &tvGraph);
      _isRenderDirty = true;
   }

   if (_isCursorPixelPositionDirty)
   {
      updateGraphCursorOnly(ucpArg);
      updateBottomLine(ucpArg, &tvGraph);
   }

   if (_isRenderDirty)
   {
      updateBottomLine(ucpArg, &tvGraph);
   }
}

int CGraphTimeline::getShotIndex(int iFrameArg)
{
   return _flickerShotList.getShotIndex(iFrameArg);
}

int CGraphTimeline::getReferenceCount(int iShotIndex)
{
   return _flickerShotList.getSettingCount(iShotIndex);
}

void CGraphTimeline::setFlickerChannel(int iArg)
{
   iFlickerChannel = iArg;

   CorrectMeanData();
}

void CGraphTimeline::displayBackground(unsigned char *ucpArg, STimelineValues *tvp, int iFirstPixel, int iLastPixel)
{
   unsigned char *ucp = ucpArg;
   unsigned char R, G, B;

   for (int r = 0; r < tvp->iHeight; r++)
      for (int c = 0; c < tvp->iWidth; c++)
      {
         if (r < TL_BORDER_PIX || r > tvp->iHeight - 1 - TL_BORDER_PIX || c < TL_BORDER_PIX || c > tvp->iWidth - 1 - TL_BORDER_PIX)
         {
            // box around the outside
            R = TL_COLOR_BORDER_R;
            G = TL_COLOR_BORDER_G;
            B = TL_COLOR_BORDER_B;
         }
         else if (c < iFirstPixel || c > iLastPixel)
         {
            R = TL_COLOR_DISPLAY_BG_R;
            G = TL_COLOR_DISPLAY_BG_G;
            B = TL_COLOR_DISPLAY_BG_B;
         }
         else
         {
            R = TL_COLOR_DISPLAY_FG_R;
            G = TL_COLOR_DISPLAY_FG_G;
            B = TL_COLOR_DISPLAY_FG_B;
         }

         // order is B G R
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
         ucp += 4;
      }

   // Stupid height is 2 less
   R = TL_COLOR_DISPLAY_WHITE_R;
   G = TL_COLOR_DISPLAY_WHITE_G;
   B = TL_COLOR_DISPLAY_WHITE_B;
   for (int r = 0; r < 2; r++)
   {
      ucp = ucpArg + 4 * tvp->iWidth * (tvp->iHeight + r);
      for (int c = 0; c < tvp->iWidth - WIDTH_FUDGE; c++)
      {
         // order is B G R
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
         ucp += 4;
      }
   }

   // Draw the corner as background
   R = TL_COLOR_DISPLAY_BG_R;
   G = TL_COLOR_DISPLAY_BG_G;
   B = TL_COLOR_DISPLAY_BG_B;
   for (int r = 0; r < 2; r++)
   {
      ucp = ucpArg + 4 * tvp->iWidth * (tvp->iHeight + r) + 4 * (tvp->iWidth - WIDTH_FUDGE);
      for (int c = 0; c < WIDTH_FUDGE; c++)
      {
         // order is B G R
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
         ucp += 4;
      }
   }
}

void CGraphTimeline::displayReference(unsigned char *ucpArg, STimelineValues *tvp)
{
   unsigned char *ucp;

   int iShotIndex = _flickerShotList.getShotIndex(iCurrentFrame);
   int iNShot = _flickerShotList.getShotCount();

   for (int iShot = 0; iShot < iNShot; iShot++)
   {
      int iSettingCount = _flickerShotList.getSettingCount(iShot);

      for (int iSetting = 0; iSetting < iSettingCount; iSetting++)
      {
         int iFrame;

         iFrame = _flickerShotList.getReferenceFrame(iShot, iSetting);

         if (iFrame != -1)
         {
            int iR, iG, iB;

            if (iShot == iShotIndex)
            {
               iR = TL_COLOR_REFERENCE_SELECT_R;
               iG = TL_COLOR_REFERENCE_SELECT_G;
               iB = TL_COLOR_REFERENCE_SELECT_B;
            }
            else
            {
               iR = TL_COLOR_REFERENCE_R;
               iG = TL_COLOR_REFERENCE_G;
               iB = TL_COLOR_REFERENCE_B;
            }

            // put up vertical lines at the reference frames
            int iPixel = (int)((float)iFrame * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);
            if (iPixel >= TL_BORDER_PIX && iPixel < tvp->iWidth - TL_BORDER_PIX)
            {
               ucp = ucpArg + 4 * (TL_BORDER_PIX * tvp->iWidth + iPixel);
               for (int r = TL_BORDER_PIX; r < tvp->iHeight - TL_BORDER_PIX; r++)
               {
                  // order is B G R
                  ucp[0] = iB;
                  ucp[1] = iG;
                  ucp[2] = iR;
                  ucp[3] = 0xFF;
                  ucp += 4 * tvp->iWidth;
               }
            }
         }
      }
   }
}

void CGraphTimeline::displayShot(unsigned char *ucpArg, STimelineValues *tvp)
{
   unsigned char *ucp;

   int iShotCount = _flickerShotList.getShotCount();

   for (int iShot = 0; iShot < iShotCount; iShot++)
   {
      int iFrame = _flickerShotList.getShotIn(iShot);

      // put up vertical lines at the reference frames
      int iPixel = (int)((float)iFrame * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);
      if (iPixel >= TL_BORDER_PIX && iPixel < tvp->iWidth - TL_BORDER_PIX)
      {
         ucp = ucpArg + 4 * (TL_BORDER_PIX * tvp->iWidth + iPixel);
         for (int r = TL_BORDER_PIX; r < tvp->iHeight - TL_BORDER_PIX; r++)
         {
            // order is B G R
            ucp[0] = TL_COLOR_SHOT_B;
            ucp[1] = TL_COLOR_SHOT_G;
            ucp[2] = TL_COLOR_SHOT_R;
            ucp[3] = 0xFF;
            ucp += 4 * tvp->iWidth;
         }
      }
   }

}

void CGraphTimeline::displayCursor(unsigned char *ucpArg, STimelineValues *tvp)
{
   unsigned char R, G, B;

   if (iCurrentFrame == -1 || iCurrentFrame < tvp->iFirst || iCurrentFrame > tvp->iLast)
   {
      // current frame is out of view
      return;
   }

   int iCursor = (int)((float)iCurrentFrame * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);

   if (iCursor >= TL_BORDER_PIX && iCursor < tvp->iWidth - TL_BORDER_PIX)
   {
      // put a double vertical line
      if (IsRendered(iCurrentFrame))
      {
         R = TL_COLOR_DISPLAY_RENDER_R;
         G = TL_COLOR_DISPLAY_RENDER_G;
         B = TL_COLOR_DISPLAY_RENDER_B;
      }
      else if (_cursorColorProcessed)
      {
         R = TL_COLOR_PROCESSED_CURSOR_R;
         G = TL_COLOR_PROCESSED_CURSOR_G;
         B = TL_COLOR_PROCESSED_CURSOR_B;
      }
      else
      {
         R = TL_COLOR_CURSOR_R;
         G = TL_COLOR_CURSOR_G;
         B = TL_COLOR_CURSOR_B;
      }

      auto ucp = ucpArg + 4 * (TL_BORDER_PIX * tvp->iWidth + iCursor);
      auto ucpRight = ucpArg + 4 * (TL_BORDER_PIX * (tvp->iWidth + 1) + iCursor);
      auto ucpLeft = ucpArg + 4 * (TL_BORDER_PIX * (tvp->iWidth - 1) + iCursor);

      // Kludge, if cursor is at beginning or end, just collapse it
      if (iCursor == 0)
      {
         ucpLeft = ucp;
      }
      else if (iCursor == (tvp->iWidth - 1))
      {
         ucpRight = ucp;
      }

      for (int r = TL_BORDER_PIX; r < tvp->iHeight - TL_BORDER_PIX; r++)
      {
         ucpRight[0] = B;
         ucpRight[1] = G;
         ucpRight[2] = R;
         ucpRight[3] = 0xFF;

         ucpLeft[0] = B;
         ucpLeft[1] = G;
         ucpLeft[2] = R;
         ucpLeft[3] = 0xFF;
         ucpRight += 4 * tvp->iWidth;
         ucpLeft += 4 * tvp->iWidth;
      }
   }
}

void CGraphTimeline::displayMarks(unsigned char *ucpArg, STimelineValues *tvp)
{
   unsigned char R, G, B;
   unsigned char *ucp;

   R = TL_COLOR_MARK_R;
   G = TL_COLOR_MARK_G;
   B = TL_COLOR_MARK_B;

   int iPixelFirst;
   int iPixelLast;

   if (iMarkFirst == -1)
   {
      iPixelFirst = -100;
   }
   else
   {
      iPixelFirst = (int)((float)iMarkFirst * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);
   }

   if (iMarkLast == -1)
   {
      iPixelLast = -100;
   }
   else
   {
      iPixelLast = (int)((float)iMarkLast * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);
   }

   // scale the mark for the Display timeline and Master timeline
   int iSize = tvp->iHeight / 3;
   int iOffBot = tvp->iHeight / 10;
   int iOffTop = iOffBot + TL_PROCESS_PIX;

   for (int r = 0; r < iSize; r++)
   {
      int rr;

      // MarkFirst
      if (iPixelFirst >= TL_BORDER_PIX && iPixelFirst < tvp->iWidth - TL_BORDER_PIX)
      {
         rr = tvp->iHeight - TL_BORDER_PIX - iOffBot - r;
         ucp = ucpArg + 4 * (rr * tvp->iWidth + iPixelFirst);
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
      }

      // MarkLast
      if (iPixelLast >= TL_BORDER_PIX && iPixelLast < tvp->iWidth - TL_BORDER_PIX)
      {
         rr = r + iOffTop;
         ucp = ucpArg + 4 * (rr * tvp->iWidth + iPixelLast);
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
      }
   }

   for (int c = 0; c < iSize; c++)
   {
      int rr;
      int cc;

      // MarkFirst
      rr = tvp->iHeight - TL_BORDER_PIX - iOffBot;
      cc = iPixelFirst + c;
      if (cc >= TL_BORDER_PIX && cc < tvp->iWidth - TL_BORDER_PIX)
      {
         ucp = ucpArg + 4 * (rr * tvp->iWidth + cc);
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
      }

      // MarkLast
      rr = iOffTop;
      cc = iPixelLast - c;
      if (cc >= TL_BORDER_PIX && cc < tvp->iWidth - TL_BORDER_PIX)
      {
         ucp = ucpArg + 4 * (rr * tvp->iWidth + cc);
         ucp[0] = B;
         ucp[1] = G;
         ucp[2] = R;
         ucp[3] = 0xFF;
      }
   }
}

void CGraphTimeline::displayBottomLine(unsigned char *ucpArg, STimelineValues *tvp, int frame, int c0, int c1)
{
   unsigned char R, G, B;
   auto f = std::max(0, frame - 1);
   auto flag = ucpProcessFlag[f];

   if (((flag & FIO_PROCESS_FLAG_RENDER) != 0) && ((flag & FIO_PROCESS_FLAG_PROXY) == 0))
   {
      R = TL_COLOR_DISPLAY_BAD_RENDER_R;
      G = TL_COLOR_DISPLAY_BAD_RENDER_G;
      B = TL_COLOR_DISPLAY_BAD_RENDER_B;
   }
   else if ((flag & FIO_PROCESS_FLAG_RENDER) != 0)
   {
      R = TL_COLOR_DISPLAY_RENDER_R;
      G = TL_COLOR_DISPLAY_RENDER_G;
      B = TL_COLOR_DISPLAY_RENDER_B;
   }
   else if ((flag & FIO_PROCESS_FLAG_PROXY) != 0)
   {
      R = TL_COLOR_DISPLAY_PROXY_R;
      G = TL_COLOR_DISPLAY_PROXY_G;
      B = TL_COLOR_DISPLAY_PROXY_B;
   }
   else
   {
      R = TL_COLOR_DISPLAY_WHITE_R;
      G = TL_COLOR_DISPLAY_WHITE_G;
      B = TL_COLOR_DISPLAY_WHITE_B;
   }

   auto ucp = ucpArg + 4 * (tvp->iWidth * (tvp->iHeight) + c0);
   auto w = 4 * tvp->iWidth;

   //
   if ((tvp->iWidth - c1) < WIDTH_FUDGE)
   {
      c1 = tvp->iWidth - WIDTH_FUDGE - 1;
   }

   for (auto i = c0; i <= c1; i++)
   {
      ucp[0] = ucp[0 + w] = B;
      ucp[1] = ucp[1 + w] = G;
      ucp[2] = ucp[2 + w] = R;
      ucp[3] = ucp[3 + w] = 0xFF;
      ucp += 4;
   }
}

void CGraphTimeline::updateBottomLine(unsigned char *ucpArg, STimelineValues *tvp)
{
   unsigned char R, G, B;
   bool bBox;

   int iPixPrev = -1;
   int iPixCurr;

   // loop over the frames and generate display
   //
   for (int iFrame = tvp->iFirst; iFrame <= tvp->iLast; iFrame++)
   {
      iPixCurr = (int)((float)(iFrame) * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);
      int c0 = iPixPrev;
      int c1 = iPixCurr;

      if (c1 >= tvp->iWidth)
      {
         c1 = tvp->iWidth - 1;
      }

      // Draw bottom line;
      // if (fValPrev != -1)
      if ((c0 >= 0) && (c1 < tvp->iWidth))
      {
         displayBottomLine(ucpArg, tvp, iFrame, c0, c1);
      }

      // update the previous values
      iPixPrev = iPixCurr;
   }

   _isRenderDirty = false;
}

void CGraphTimeline::displayValues(unsigned char *ucpArg, STimelineValues *tvp, int iList, int iChan)
{
   unsigned char R, G, B;
   bool bBox;

   // displayValues is only called for mdDisplay

   if (tvp->iFirst == -1 || tvp->iLast == -1 || iGraphType == TL_GRAPH_TYPE_NONE)
   {
      return;
   }

   // Kludge
   bBox = (iList == 0);

   if (iChan == 0)
   {
      R = TL_COLOR_RED_R;
      G = TL_COLOR_RED_G;
      B = TL_COLOR_RED_B;

      if ((iGraphType & TL_GRAPH_TYPE_0) == 0)
      {
         return;
      }
   }
   else if (iChan == 1)
   {
      // the G channel
      R = TL_COLOR_GREEN_R;
      G = TL_COLOR_GREEN_G;
      B = TL_COLOR_GREEN_B;

      if ((iGraphType & TL_GRAPH_TYPE_1) == 0)
      {
         return;
      }

   }
   else if (iChan == 2)
   {
      R = TL_COLOR_BLUE_R;
      G = TL_COLOR_BLUE_G;
      B = TL_COLOR_BLUE_B;

      if ((iGraphType & TL_GRAPH_TYPE_2) == 0)
      {
         return;
      }
   }

   // use the min/max found in the original list and the processed list
   float fMin, fMax;
   fMin = 255;
   fMax = 0;

   bool baUseChannel[3];
   baUseChannel[0] = iGraphType & TL_GRAPH_TYPE_0;
   baUseChannel[1] = iGraphType & TL_GRAPH_TYPE_1;
   baUseChannel[2] = iGraphType & TL_GRAPH_TYPE_2;

   for (int iC = 0; iC < 3; iC++)
   {
      // look at both original and processed lists
      if ((mdProcess.faMinOrig[iC] != -1) && (mdProcess.faMaxOrig[iC] != -1))
      {
         if (mdProcess.faMinOrig[iC] < fMin)
         {
            fMin = mdProcess.faMinOrig[iC];
         }
         if (mdProcess.faMaxOrig[iC] > fMax)
         {
            fMax = mdProcess.faMaxOrig[iC];
         }
      }

      if ((mdProcess.faMinProc[iC] != -1) && (mdProcess.faMaxProc[iC] != -1))
      {
         if (mdProcess.faMinProc[iC] < fMin)
         {
            fMin = mdProcess.faMinProc[iC];
         }
         if (mdProcess.faMaxProc[iC] > fMax)
         {
            fMax = mdProcess.faMaxProc[iC];
         }
      }
   }

   int iPixPrev = -1;
   int iPixCurr;
   float fValPrev = -1.;
   float fValCurr;
   float fVPrev;
   float fVCurr;

   // fValCurr is on the range fMeanMin to fMeanMax. Rescale so
   // fMin maps to .95*height
   // fMax maps to .05*height

   if ((fMax - fMin) < _minScale)
   {
      fMax += _minScale / 2;
      fMin -= _minScale / 2;

      // This should never happen
      if (fMin < 0)
         fMin = 0;
      if (fMax > 255)
         fMax = 255;
   }

   displayRangeValue(ucpArg, tvp, fMax, fMin);

   float fDispSlope = (.95f - .05f) * (float)tvp->iHeight / (fMin - fMax);
   float fDispInter = .05f * (float)tvp->iHeight - fDispSlope * fMax;

   float *fpVal[3];
   for (auto i = 0; i < TL_CHAN_COUNT; i++)
   {
      fpVal[i] = (iList == 0) ? mdProcess.fpValOrig[i] : mdProcess.fpValProc[i];
   }

   // loop over the frames and generate display
   //
   for (int iFrame = tvp->iFirst; iFrame <= tvp->iLast; iFrame++)
   {
      iPixCurr = (int)((float)(iFrame) * tvp->fPixelPerFrame + tvp->fFirstPixel + .5);

      // Kludge to use duplicate last value in display
      if (iFrame < _totalFrames)
      {
         fValCurr = fpVal[iChan][iFrame];
      }
      else
      {
         fValCurr = fpVal[iChan][_totalFrames - 1];
      }

      fVCurr = fDispSlope * fValCurr + fDispInter;
      int r0 = (int)(fVPrev + .5);
      int r1 = (int)(fVCurr + .5);
      int c0 = iPixPrev;
      int c1 = iPixCurr;

      // draw a line from last value to current value
      if (fValCurr != -1. && fValPrev != -1. && bBox == false)
      {
         float fSlope = (fVCurr - fVPrev) / (float)(iPixCurr - iPixPrev);
         float fInter = fVCurr - fSlope * (float)iPixCurr;

         if (std::abs(r1 - r0) > (c1 - c0))
         {
            // change in rows is larger
            int r = r0;
            int rDelta;
            if (r1 > r0)
            {
               rDelta = 1;
            }
            else
            {
               rDelta = -1;
            }

            while (r != r1)
            {
               int c = (int)(((float)r - fInter) / fSlope + .5);

               if (r >= TL_BORDER_PIX && r < tvp->iHeight - TL_BORDER_PIX && c >= TL_BORDER_PIX && c < tvp->iWidth - TL_BORDER_PIX)
               {
                  ucpArg[4 * (r * tvp->iWidth + c) + 0] = B;
                  ucpArg[4 * (r * tvp->iWidth + c) + 1] = G;
                  ucpArg[4 * (r * tvp->iWidth + c) + 2] = R;
                  ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
               }

               r += rDelta;
            }
         }
         else
         {
            // change in cols is larger
            for (int c = c0; c <= c1; c++)
            {
               int r = (int)(fSlope * (float)c + fInter + .5);

               if (r >= TL_BORDER_PIX && r < tvp->iHeight - TL_BORDER_PIX && c >= TL_BORDER_PIX && c < tvp->iWidth - TL_BORDER_PIX)
               {
                  ucpArg[4 * (r * tvp->iWidth + c) + 0] = B;
                  ucpArg[4 * (r * tvp->iWidth + c) + 1] = G;
                  ucpArg[4 * (r * tvp->iWidth + c) + 2] = R;
                  ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
               }
            }
         }
      }

      // put up a small box at this location
      if (fValCurr != -1. && bBox == true && iFrame < _totalFrames)
      {
         for (int j = -1; j <= 1; j++)
            for (int k = -1; k <= 1; k++)
            {
               int r = (int)(fVCurr + .5) + j;
               int c = iPixCurr + k;

               if (r > TL_BORDER_PIX && r < tvp->iHeight - TL_BORDER_PIX && c > TL_BORDER_PIX && c < tvp->iWidth - TL_BORDER_PIX)
               {
                  ucpArg[4 * (r * tvp->iWidth + c) + 0] = B;
                  ucpArg[4 * (r * tvp->iWidth + c) + 1] = G;
                  ucpArg[4 * (r * tvp->iWidth + c) + 2] = R;
                  ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
               }
            }
      }

      // update the previous values
      iPixPrev = iPixCurr;
      fValPrev = fValCurr;
      fVPrev = fVCurr;
   }

}

void CGraphTimeline::CorrectMeanData()
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);
   CHRTimer hrt;
   hrt.Start();

   //// TRACE_0(errout << "TTT Correct mean data" );
   // The processing is handled in two steps:
   //
   // 1. Adjustments to match the reference frames are done in RGB
   // 2. The flicker correction (high frequency temporal smoothing) is applied to either RGB or CrYCb
   //
   //

   // JAM removed most of the difference between display and processed data, however never finished
   // because of a subtle bug.  It was much easier just to force both to be called from above.
   // Although a few millieconds can be saved on start of render, it was not deamed worth it.
   int iMovingAveFirst, iMovingAveLast;
   int iRefFirst, iRefLast;

   iMovingAveFirst = 0;
   iMovingAveLast = _totalFrames - 1;
   iRefFirst = 0;
   iRefLast = _totalFrames - 1;

   // initialize the corrected RGB and corrected CrYCb list with original values
   for (int iFrame = iRefFirst; iFrame <= iRefLast; iFrame++)
   {
      for (int iChan = 0; iChan < 3; iChan++)
      {
         // RGB values
         mdProcess.fpValProc[iChan][iFrame] = mdProcess.fpValOrig[iChan][iFrame];
      }
   }

   // use the processing mean data
   if (iMovingAveFirst == -1 || iMovingAveLast == -1)
   {
      return;
   }

   // apply the reference frame correction
   ApplyReference(iRefFirst, iRefLast);

   // take a moving average
   if (iFlickerChannel & (TL_FLICKER_CHANNELS_RGB | TL_FLICKER_CHANNELS_R_G_B))
   {
      MovingAverageRGB(iRefFirst, iRefLast, iMovingAveFirst, iMovingAveLast);
      ////    TRACE_0(errout << "TTT MovingAverageRGB " << hrt.ReadAsString());
   }
   else
   {
      TRACE_0(errout << __func__ << "Cannot do CryB");
   }

   // refresh the scale on the corrected values if we are displaying results
   FindScaling(_displayFirst, _displayLast - 1);

   // process the quantile data
   CorrectQuantileData(iRefFirst, iRefLast, iMovingAveFirst, iMovingAveLast);

   //// TRACE_0(errout << "TTT done correct mean data " << hrt.ReadAsString());
}

void CGraphTimeline::displayRangeValue(unsigned char *ucpArg, STimelineValues *tvp, float fMax, float fMin)
{
   auto c0 = tvp->iWidth - WIDTH_FUDGE + 2;
   auto B = 200;
   auto G = 200;
   auto R = 200;

   int rf = (255.0 - fMax) * (tvp->iHeight) / 255;
   int rl = (255.0 - fMin) * (tvp->iHeight) / 255;

   for (int c = c0; c < c0 + 2; c++)
   {
      for (auto r = 0; r < rf; r++)
      {
         ucpArg[4 * (r * tvp->iWidth + c) + 0] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 1] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 2] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
      }

      for (auto r = rf; r < rl; r++)
      {
         ucpArg[4 * (r * tvp->iWidth + c) + 0] = B;
         ucpArg[4 * (r * tvp->iWidth + c) + 1] = G;
         ucpArg[4 * (r * tvp->iWidth + c) + 2] = R;
         ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
      }

      for (auto r = rl + 1; r < tvp->iHeight; r++)
      {
         ucpArg[4 * (r * tvp->iWidth + c) + 0] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 1] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 2] = 0;
         ucpArg[4 * (r * tvp->iWidth + c) + 3] = 0xFF;
      }
   }
}

void CGraphTimeline::ApplyReference(int iFirstArg, int iLastArg)
{
   int iShotIndexFirst = _flickerShotList.getShotIndex(iFirstArg);
   int iShotIndexLast = _flickerShotList.getShotIndex(iLastArg);

   for (int iShotIndex = iShotIndexFirst; iShotIndex <= iShotIndexLast; iShotIndex++)
   {
      int iShotIn = _flickerShotList.getShotIn(iShotIndex);
      int iShotOut = _flickerShotList.getShotOut(iShotIndex);

      // how many reference frames
      int iNRef = _flickerShotList.getSettingCount(iShotIndex);

      // use the reference frames to adjust some combination of R, G, B values

      float faRefR[iNRef + 2];
      float faRefG[iNRef + 2];
      float faRefB[iNRef + 2];
      int iaRefFrame[iNRef + 2];
      int iaRefType[iNRef + 2];

      for (int iRef = 0; iRef < iNRef; iRef++)
      {
         _flickerShotList.getSetting(iShotIndex, iRef, iaRefFrame + iRef, iaRefType + iRef, 0);
         faRefR[iRef] = mdProcess.fpValProc[0][iaRefFrame[iRef]];
         faRefG[iRef] = mdProcess.fpValProc[1][iaRefFrame[iRef]];
         faRefB[iRef] = mdProcess.fpValProc[2][iaRefFrame[iRef]];
      }

      // this loop does not adjust the reference frames
      for (int iRef = 0; iRef <= iNRef; iRef++)
      {
         int iFrame0, iFrame1;
         int iRef0, iRef1;

         // the corrected signal should have the value (fR0, fG0, fB0) at fFrame0
         // and  (fR1, fG1, fB1) at fFrame1

         if (iRef == 0)
         {
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_LOW_REV)
            {
               // extend the first reference back to beginning
               if (iaRefFrame[iRef] == iShotIn)
               {
                  iFrame0 = iaRefFrame[iRef] + 1; // do not process the reference
               }
               else
               {
                  iFrame0 = iShotIn;
               }
            }
            else
            {
               // do not extend the reference
               iFrame0 = iaRefFrame[iRef] + 1;
            }

            iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference

            iRef0 = iRef;
            iRef1 = iRef0;
         }
         else if (iRef == iNRef)
         {
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_LOW_FWD)
            {
               // extend the last reference to the end
               if (iaRefFrame[iRef - 1] == iShotOut)
               {
                  iFrame1 = iaRefFrame[iRef - 1] - 1; // do not process the reference
               }
               else
               {
                  iFrame1 = iShotOut;
               }
            }
            else
            {
               // do not extend
               iFrame1 = iaRefFrame[iRef - 1] - 1;
            }

            iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
            iRef0 = iRef - 1;
            iRef1 = iRef0;
         }
         else
         {
            // two reference frames.

            bool bFwd;
            bool bRev;

            // does the previous reference frame go forward?
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_LOW_FWD)
            {
               bFwd = true;
            }
            else
            {
               bFwd = false;
            }

            // does the next reference frame go backward?
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_LOW_REV)
            {
               bRev = true;
            }
            else
            {
               bRev = false;
            }

            // Different ways to combine the two reference frames
            if (bFwd == true && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef - 1;
               iRef1 = iRef;
            }
            else if (bFwd == false && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef;
               iRef1 = iRef0;
            }
            else if (bFwd == true && bRev == false)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef - 1;
               iRef1 = iRef0;
            }
            else
            {
               // don't process between these two reference points
               iFrame0 = -1;
               iFrame1 = -1;
               iRef0 = 0;
               iRef1 = 0;
            }
         }

         int iFirst = iFrame0;
         if (iFirst < iFirstArg)
         {
            iFirst = iFirstArg;
         }

         int iLast = iFrame1;
         if (iLast > iLastArg)
         {
            iLast = iLastArg;
         }

         for (int iFrame = iFirst; iFrame <= iLast; iFrame++)
         {

            float fOldR = mdProcess.fpValProc[0][iFrame];
            float fOldG = mdProcess.fpValProc[1][iFrame];
            float fOldB = mdProcess.fpValProc[2][iFrame];

            // how does the iRef0 create a new value
            float fNewR0, fNewG0, fNewB0;
            getNew(iaRefType[iRef0], fOldR, fOldG, fOldB, faRefR[iRef0], faRefG[iRef0], faRefB[iRef0], &fNewR0, &fNewG0, &fNewB0);

            // how does the iRef1 create a new value
            float fNewR1, fNewG1, fNewB1;
            getNew(iaRefType[iRef1], fOldR, fOldG, fOldB, faRefR[iRef1], faRefG[iRef1], faRefB[iRef1], &fNewR1, &fNewG1, &fNewB1);

            // the weights
            float fWei0;
            float fWei1;
            if (iaRefFrame[iRef0] == iaRefFrame[iRef1])
            {
               fWei1 = 1.;
            }
            else
            {
               fWei1 = (float)(iFrame - iaRefFrame[iRef0]) / (float)(iaRefFrame[iRef1] - iaRefFrame[iRef0]);
            }

            fWei0 = 1.f - fWei1;

            float fNewR, fNewG, fNewB;

            fNewR = fWei0 * fNewR0 + fWei1 * fNewR1;
            fNewG = fWei0 * fNewG0 + fWei1 * fNewG1;
            fNewB = fWei0 * fNewB0 + fWei1 * fNewB1;

            mdProcess.fpValProc[0][iFrame] = fNewR;
            mdProcess.fpValProc[1][iFrame] = fNewG;
            mdProcess.fpValProc[2][iFrame] = fNewB;
         }
      }
   }

}

void CGraphTimeline::getNew(int iRefType, float fOldR, float fOldG, float fOldB, float fRefR, float fRefG, float fRefB, float *fpNewR,
    float *fpNewG, float *fpNewB)
{

   bool bTypeR = false;
   bool bTypeG = false;
   bool bTypeB = false;

   if (iRefType & FS_REFERENCE_TYPE_LOW_R)
   {
      bTypeR = true;
   }
   if (iRefType & FS_REFERENCE_TYPE_LOW_G)
   {
      bTypeG = true;
   }
   if (iRefType & FS_REFERENCE_TYPE_LOW_B)
   {
      bTypeB = true;
   }

   // when 2 channels are supposed to change, we make the change relative to the 3rd channel
   if (bTypeR == true && bTypeG == true && bTypeB == false)
   {
      // keep B fixed and make relative changes to R and G
      *fpNewR = fOldB - fRefB + fRefR;
      *fpNewG = fOldB - fRefB + fRefG;
      *fpNewB = fOldB;
   }
   else if (bTypeR == true && bTypeG == false && bTypeB == true)
   {
      // keep G fixed and make relative changes to R and B
      *fpNewR = fOldG - fRefG + fRefR;
      *fpNewG = fOldG;
      *fpNewB = fOldG - fRefG + fRefB;
   }
   else if (bTypeR == false && bTypeG == true && bTypeB == true)
   {
      // keep R fixed and make relative changes to G and B
      *fpNewR = fOldR;
      *fpNewG = fOldR - fRefR + fRefG;
      *fpNewB = fOldR - fRefR + fRefB;
   }
   else
   {
      // change 1 or 3 channels
      if (bTypeR == true)
      {
         *fpNewR = fRefR;
      }
      else
      {
         *fpNewR = fOldR;
      }

      if (bTypeG == true)
      {
         *fpNewG = fRefG;
      }
      else
      {
         *fpNewG = fOldG;
      }

      if (bTypeB == true)
      {
         *fpNewB = fRefB;
      }
      else
      {
         *fpNewB = fOldB;
      }
   }

}

void CGraphTimeline::MovingAverageRGB(int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast)
{
   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      MovingAverage(iChan, iRefFirst, iRefLast, iMovingAveFirst, iMovingAveLast, mdProcess.fpValProc[iChan]);
   }
}

void CGraphTimeline::MovingAverage(int iChan, int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast, float *fp)
{
   // Clean out any garbage, may not be necessary
   for (int i = 0; i <= _totalFrames; i++)
   {
      _fpMovingAveScratch[i] = 0;
   }

   // copy over the data to scratch storage
   for (int iFrame = iRefFirst; iFrame <= iRefLast; iFrame++)
   {
      _fpMovingAveScratch[iFrame] = fp[iFrame];
   }

   // adjust the pointer to make access easier
   float *fpScratch = _fpMovingAveScratch;

   int iShotIndexFirst = _flickerShotList.getShotIndex(iRefFirst);
   int iShotIndexLast = _flickerShotList.getShotIndex(iRefLast);

   for (int iShotIndex = iShotIndexFirst; iShotIndex <= iShotIndexLast; iShotIndex++)
   {
      int iShotIn = _flickerShotList.getShotIn(iShotIndex);
      int iShotOut = _flickerShotList.getShotOut(iShotIndex);

      // how many reference frames
      int iNRef = _flickerShotList.getSettingCount(iShotIndex);

      int iNRefPlus1 = iNRef + 1;
      int iaRefFrame[iNRefPlus1];
      int iaRefType[iNRefPlus1];
      int iaMovingAve[iNRefPlus1][3];

      for (auto i = 0; i < iNRefPlus1; i++)
         for (auto j = 0; j < 3; j++)
         {
            iaMovingAve[i][j] = 0;
         }

      for (int iRef = 0; iRef < iNRef; iRef++)
      {
         _flickerShotList.getSetting(iShotIndex, iRef, iaRefFrame + iRef, iaRefType + iRef, iaMovingAve[iRef]);
      }

      // use the reference to set boundaries on the processing

      // this loop does adjust the reference frames
      for (int iRef = 0; iRef <= iNRef; iRef++)
      {
         int iFrame0, iFrame1;
         int iRef0, iRef1;

         // the corrected signal is a moving average of neighboring frames

         if (iRef == 0)
         {
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_HIGH_REV)
            {
               // extend the first reference back to beginning
               iFrame0 = iShotIn;
               iFrame1 = iaRefFrame[iRef];
            }
            else
            {
               // do not process this iteration
               iFrame0 = -1;
               iFrame1 = -1;
            }

            iRef0 = iRef;
            iRef1 = iRef;

         }
         else if (iRef == iNRef)
         {
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_HIGH_FWD)
            {
               // extend the last reference to the end
               iFrame0 = iaRefFrame[iRef - 1];
               iFrame1 = iShotOut;
            }
            else
            {
               // do not extend
               iFrame0 = -1;
               iFrame1 = -1;
            }

            iRef0 = iRef - 1;
            iRef1 = iRef0;

         }
         else
         {
            // two reference frames.
            bool bFwd;
            bool bRev;

            // does the previous reference frame go forward?
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_HIGH_FWD)
            {
               bFwd = true;
            }
            else
            {
               bFwd = false;
            }

            // does the next reference frame go backward?
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_HIGH_REV)
            {
               bRev = true;
            }
            else
            {
               bRev = false;
            }

            // Different ways to combine the two reference frames
            if (bFwd == true && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1];
               iFrame1 = iaRefFrame[iRef];
               iRef0 = iRef - 1;
               iRef1 = iRef;
            }
            else if (bFwd == false && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1;
               iFrame1 = iaRefFrame[iRef];
               iRef0 = iRef;
               iRef1 = iRef0;
            }
            else if (bFwd == true && bRev == false)
            {
               iFrame0 = iaRefFrame[iRef - 1];
               iFrame1 = iaRefFrame[iRef] - 1;
               iRef0 = iRef - 1;
               iRef1 = iRef0;
            }
            else
            {
               // don't process between these two reference points
               iFrame0 = -1;
               iFrame1 = -1;
               iRef0 = 0;
               iRef1 = 0;
            }

         }

         int iFirst = iFrame0;
         if (iFirst < iMovingAveFirst)
         {
            iFirst = iMovingAveFirst;
         }
         int iLast = iFrame1;
         if (iLast > iMovingAveLast)
         {
            iLast = iMovingAveLast;
         }

         // determine which frames can be used for the moving average
         int iLimit0 = iFrame0;
         int iLimit1 = iFrame1;

         int iR;

         iR = iRef;
         while (iR >= 0 && (iaRefType[iR] & FS_REFERENCE_TYPE_HIGH_REV))
         {
            if (iR > 0)
            {
               iLimit0 = iaRefFrame[iR - 1];
            }
            else
            {
               iLimit0 = iShotIn;
            }
            iR--;
         }

         iR = iRef;
         while (iR < iNRef && (iaRefType[iR] & FS_REFERENCE_TYPE_HIGH_FWD))
         {
            if (iR < iNRef - 1)
            {
               iLimit1 = iaRefFrame[iR + 1];
            }
            else
            {
               iLimit1 = iShotOut;
            }
            iR++;
         }

         if (iLimit0 < iRefFirst)
         {
            iLimit0 = iRefFirst;
         }

         if (iLimit1 > iRefLast)
         {
            iLimit1 = iRefLast;
         }

         // the radii used for the moving average
         int iRad0, iRad1;

         iRad0 = iaMovingAve[iRef0][iChan];
         iRad1 = iaMovingAve[iRef1][iChan];

         float fSlpRad, fIntRad;
         if (iFrame1 > iFrame0)
         {
            fSlpRad = (float)(iRad1 - iRad0) / (float)(iFrame1 - iFrame0);
            fIntRad = (float)iRad0 - fSlpRad * (float)iFrame0;
         }
         else
         {
            fSlpRad = 0.f;
            fIntRad = (float)iRad0;
         }

         for (int iFrame = iFirst; iFrame <= iLast; iFrame++)
         {
            float fRad = fSlpRad * (float)iFrame + fIntRad;
            int iRadius = (int)(fRad + .5f);

            // get the starting frame
            int iF0 = iFrame - iRadius;
            int iF1 = iFrame + iRadius;

            // legalize iF0 and iF1
            while (iF0 < iLimit0)
            {
               iF0++;
               iF1++;
            }

            while (iF1 > iLimit1)
            {
               iF1--;
               iF0--;
            }

            if (iF0 >= iLimit0 && iF1 <= iLimit1)
            {

               // find initialized data

               while (iF0 < iFrame && fpScratch[iF0] == -1)
               {
                  iF0++;
                  iF1++;
               }

               while (iF1 > iFrame && fpScratch[iF1] == -1)
               {
                  iF0--;
                  iF1--;
               }

               // at this point iF0 <= iFrame <= iF1

               float fSum = 0.;
               int nSum = 0;
               for (int iF = iF0; iF <= iF1; iF++)
               {
                  if (fpScratch[iF] != -1)
                  {
                     fSum += fpScratch[iF];
                     nSum++;
                  }
               }

               float fNewData;

               if (nSum == 2 * iRadius + 1)
               {
                  // we found enough initialized data to calculate a new value

                  fNewData = fSum / (float)(2 * iRadius + 1);
               }
               else
               {
                  fNewData = fpScratch[iFrame];
               }

               fp[iFrame] = fNewData;
            }

         }

      }
   }
}

void CGraphTimeline::ConvertToCrYCb(float *fpSrc, float *fpDst)
{
   float fR = fpSrc[0];
   float fG = fpSrc[1];
   float fB = fpSrc[2];

   float fY;
   float fCr;
   float fCb;

   if (fR == -1.f || fG == -1.f || fB == -1.f)
   {
      fY = -1.f;
      fCr = -1.f;
      fCb = -1.f;
   }
   else
   {
      fY = .2f * fR + .7f * fG + .1f * fB;
      fCr = fR - fY + 128.f;
      fCb = fB - fY + 128.f;
   }

   fpDst[0] = fCr;
   fpDst[1] = fY;
   fpDst[2] = fCb;
}

void CGraphTimeline::ConvertFromCrYCb(float *fpSrc, float *fpDst)
{

   float fCr = fpSrc[0];
   float fY = fpSrc[1];
   float fCb = fpSrc[2];

   float fR;
   float fG;
   float fB;

   if (fCr == -1.f || fY == -1.f || fCb == -1.f)
   {
      fR = -1.f;
      fG = -1.f;
      fB = -1.f;
   }
   else
   {
      fR = fY + (fCr - 128.f);
      fG = fY - .2857f * (fCr - 128.f) - .1429f * (fCb - 128.f);
      fB = fY + (fCb - 128.f);
   }

   fpDst[0] = fR;
   fpDst[1] = fG;
   fpDst[2] = fB;
}

void CGraphTimeline::setGraphType(int iType)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   if (iGraphType != iType)
   {
      iGraphType = iType;
      SetGraphDirty();
   }
}

void CGraphTimeline::getRGB(int iFrame, float *fpR, float *fpG, float *fpB)
{
   auto mdp = &mdProcess;
   *fpR = mdp->fpValOrig[0][iFrame];
   *fpG = mdp->fpValOrig[1][iFrame];
   *fpB = mdp->fpValOrig[2][iFrame];
}

void CGraphTimeline::getDelta(int iFrame, float *fpDeltaR, float *fpDeltaG, float *fpDeltaB)
{
   auto isProxy = (ucpProcessFlag[iFrame] & FIO_PROCESS_FLAG_PROXY) != 0;
   if (bAllocMeanData == false || (isProxy == false))
   {
      *fpDeltaR = 0;
      *fpDeltaG = 0;
      *fpDeltaB = 0;
      return;
   }

   SMeanData *mdp = &mdProcess;

   *fpDeltaR = mdp->fpValProc[0][iFrame] - mdp->fpValOrig[0][iFrame];
   *fpDeltaG = mdp->fpValProc[1][iFrame] - mdp->fpValOrig[1][iFrame];
   *fpDeltaB = mdp->fpValProc[2][iFrame] - mdp->fpValOrig[2][iFrame];
}

int CGraphTimeline::FrameToGraphPixel(int frame)
{
   return (int)((float)frame * tvGraph.fPixelPerFrame + tvGraph.fFirstPixel + .5);
}

void CGraphTimeline::setCursorPosition(int position)
{
   auto frames = tvGraph.iLast - tvGraph.iFirst;
   if (frames <= 0)
   {
      return;
   }

   tvGraph.fPixelPerFrame = (tvGraph.iWidth - WIDTH_FUDGE) / (double)frames;
   tvGraph.fFirstPixel = -(float)tvGraph.iFirst * tvGraph.fPixelPerFrame + TL_BORDER_PIX;

   // Do nothing if nothing to do
   if (iCurrentFrame == position)
   {
      return;
   }

   // See if pixel has changed
   if (FrameToGraphPixel(iCurrentFrame) != FrameToGraphPixel(position))
   {
      _isCursorPixelPositionDirty = true;
   }

   iCurrentFrame = position;
}

// ---------------------------------------------------------------------------
#pragma package(smart_init)

int CGraphTimeline::GetDisplayIn() const
{
   return _displayFirst;
}

int CGraphTimeline::GetDisplayOut() const
{
   return _displayLast;
}

void CGraphTimeline::SetGraphDirty()
{
   _isGraphDataDirty = true;
   _isCursorPixelPositionDirty = true;
   _isRenderDirty = true;
}

void CGraphTimeline::SetGraphRenderDirty()
{
   _isRenderDirty = true;
}

void CGraphTimeline::SetGraphCentered(bool centered)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   if (_centered != centered)
   {
      _centered = centered;
      SetGraphDirty();
   }
}

bool CGraphTimeline::IsGraphCentered() const
{
   return _centered;
}

void CGraphTimeline::setShotList(vector<CFlickerShot> &newShotList)
{
   if (_flickerShotList.setShotList(newShotList) == false)
   {
      SetGraphDirty();
   }
}

bool CGraphTimeline::IsGraphDataDirty() const
{
   return _isGraphDataDirty;
}

bool CGraphTimeline::IsCursorPixelPositionDirty() const
{
   return _isCursorPixelPositionDirty;
}

bool CGraphTimeline::IsGraphDirty() const
{
   return _isCursorPixelPositionDirty || _isGraphDataDirty || _isRenderDirty;
}

void CGraphTimeline::ShowCursorActive(bool processed)
{
   if (_cursorColorProcessed != processed)
   {
      _cursorColorProcessed = processed;
      _isCursorPixelPositionDirty = true;
   }
}

void CGraphTimeline::SetMinScale(double minScale)
{
   // This .001 is almost picked out of the hat
   // But basically a change this small would not be reflected in the graph
   if (std::fabs(minScale - _minScale) > 0.001)
   {
      _minScale = minScale;
      SetGraphDirty();
   }
}

void CGraphTimeline::getQuantileCorrection(int iFrame, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
    float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], int iaCount[])
{
   float fpDeltaR, fpDeltaG, fpDeltaB;
   getDelta(iFrame, &fpDeltaR, &fpDeltaG, &fpDeltaB);

   if ((bAllocMeanData == false) || ((fpDeltaR == 0) && (fpDeltaG == 0) && (fpDeltaB == 0)))
   {
      for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
      {
         iaCount[iChan] = 2;
         faX[iChan][0] = 0.f;
         faY[iChan][0] = 0.f;
         faX[iChan][1] = 1.f;
         faY[iChan][1] = 1.f;
      }

      return;
   }

   SMeanData *mdp = &mdProcess;

   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      faX[iChan][TL_QUANTILE_COUNT + 1] = 1;
   }

   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      int iCnt = 0;
      float fX0 = 0.f;
      float fY0 = 0.f;

      faX[iChan][iCnt] = fX0;
      faY[iChan][iCnt] = fY0;
      iCnt++;

      int iQuant = 0;
      while (iQuant <= TL_QUANTILE_COUNT)
      {
         float fX1;
         float fY1;

         if (iQuant < TL_QUANTILE_COUNT)
         {
            fX1 = mdp->fpQuantile[iChan][iQuant][iFrame];
            fY1 = mdp->fpRepair[iChan][iQuant][iFrame];
         }
         else
         {
            fX1 = 1.f;
            fY1 = 1.f;
         }

         if (fX1 > fX0)
         {
            // a valid quantile.  Use it
            faX[iChan][iCnt] = fX1;
            faY[iChan][iCnt] = fY1;
            iCnt++;

            // update the previous point
            fX0 = fX1;
            fY0 = fY1;
         }
         else
         {
            TRACE_0(errout << "Quantil not increasing " << iQuant << ", " << fX0 << ", " << fX1);
         }

         // advance to the next quantile
         iQuant++;
      }

      iaCount[iChan] = iCnt;
   }
}

void CGraphTimeline::CorrectQuantileData(int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast)
{
   SMeanData *mdp = &mdProcess;

   // The processing is handled in two steps:
   //
   // 1. Adjustments to match the reference frames are done in RGB
   // 2. The flicker correction (high frequency temporal smoothing) is applied to RGB
   //
   //
   // initialize the repair RGB  with original values
   for (int iChan = 0; iChan < 3; iChan++)
   {
      for (int iQuant = 0; iQuant < TL_QUANTILE_COUNT; iQuant++)
      {
         for (int iFrame = iRefFirst; iFrame <= iRefLast; iFrame++)
         {
            mdp->fpRepair[iChan][iQuant][iFrame] = mdp->fpQuantile[iChan][iQuant][iFrame];
         }
      }
   }

   if (iMovingAveFirst == -1 || iMovingAveLast == -1)
   {
      return;
   }

   // apply the reference frame correction
   ApplyReferenceQuantile(iRefFirst, iRefLast);

   // take a moving average
   if (iFlickerChannel & (TL_FLICKER_CHANNELS_RGB | TL_FLICKER_CHANNELS_R_G_B))
   {
      MovingAverageQuantile(iRefFirst, iRefLast, iMovingAveFirst, iMovingAveLast);
   }
}

void CGraphTimeline::ApplyReferenceQuantile(int iFirstArg, int iLastArg)
{
   SMeanData *mdp = &mdProcess;

   int iShotIndexFirst = _flickerShotList.getShotIndex(iFirstArg);
   int iShotIndexLast = _flickerShotList.getShotIndex(iLastArg);

   if ((iShotIndexFirst < 0) || (iShotIndexLast < 0))
   {
      return;
   }

   _flickerShotList.getShotIndex(iFirstArg);
   _flickerShotList.getShotIndex(iLastArg);

   for (int iShotIndex = iShotIndexFirst; iShotIndex <= iShotIndexLast; iShotIndex++)
   {
      int iShotIn = _flickerShotList.getShotIn(iShotIndex);
      int iShotOut = _flickerShotList.getShotOut(iShotIndex);

      // how many reference frames
      int iNRef = _flickerShotList.getSettingCount(iShotIndex);

      // Shortcut
      if (iNRef <= 0)
      {
         continue;
      }

      // use the reference frames to adjust some combination of R, G, B values

      float faRefR[iNRef][TL_QUANTILE_COUNT];
      float faRefG[iNRef][TL_QUANTILE_COUNT];
      float faRefB[iNRef][TL_QUANTILE_COUNT];
      int iaRefFrame[iNRef];
      int iaRefType[iNRef];

      for (int iRef = 0; iRef < iNRef; iRef++)
      {
         _flickerShotList.getSetting(iShotIndex, iRef, iaRefFrame + iRef, iaRefType + iRef, 0);
         for (int iQuant = 0; iQuant < TL_QUANTILE_COUNT; iQuant++)
         {
            faRefR[iRef][iQuant] = mdp->fpQuantile[0][iQuant][iaRefFrame[iRef]];
            faRefG[iRef][iQuant] = mdp->fpQuantile[1][iQuant][iaRefFrame[iRef]];
            faRefB[iRef][iQuant] = mdp->fpQuantile[2][iQuant][iaRefFrame[iRef]];
         }
      }

      // this loop does not adjust the reference frames
      for (int iRef = 0; iRef <= iNRef; iRef++)
      {
         int iFrame0, iFrame1;
         int iRef0, iRef1;

         // the corrected signal should have the value (fR0, fG0, fB0) at fFrame0
         // and  (fR1, fG1, fB1) at fFrame1

         if (iRef == 0)
         {
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_LOW_REV)
            {
               // extend the first reference back to beginning
               if (iaRefFrame[iRef] == iShotIn)
               {
                  iFrame0 = iaRefFrame[iRef] + 1; // do not process the reference
               }
               else
               {
                  iFrame0 = iShotIn;
               }
            }
            else
            {
               // do not extend the reference
               iFrame0 = iaRefFrame[iRef] + 1;
            }

            iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference

            iRef0 = iRef;
            iRef1 = iRef0;
         }
         else if (iRef == iNRef)
         {
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_LOW_FWD)
            {
               // extend the last reference to the end
               if (iaRefFrame[iRef - 1] == iShotOut)
               {
                  iFrame1 = iaRefFrame[iRef - 1] - 1; // do not process the reference
               }
               else
               {
                  iFrame1 = iShotOut;
               }
            }
            else
            {
               // do not extend
               iFrame1 = iaRefFrame[iRef - 1] - 1;
            }

            iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
            iRef0 = iRef - 1;
            iRef1 = iRef0;
         }
         else
         {
            // two reference frames.

            bool bFwd;
            bool bRev;

            // does the previous reference frame go forward?
            if (iaRefType[iRef - 1] & FS_REFERENCE_TYPE_LOW_FWD)
            {
               bFwd = true;
            }
            else
            {
               bFwd = false;
            }

            // does the next reference frame go backward?
            if (iaRefType[iRef] & FS_REFERENCE_TYPE_LOW_REV)
            {
               bRev = true;
            }
            else
            {
               bRev = false;
            }

            // Different ways to combine the two reference frames
            if (bFwd == true && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef - 1;
               iRef1 = iRef;
            }
            else if (bFwd == false && bRev == true)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef;
               iRef1 = iRef0;
            }
            else if (bFwd == true && bRev == false)
            {
               iFrame0 = iaRefFrame[iRef - 1] + 1; // do not process the reference
               iFrame1 = iaRefFrame[iRef] - 1; // do not process the reference
               iRef0 = iRef - 1;
               iRef1 = iRef0;
            }
            else
            {
               // don't process between these two reference points
               iFrame0 = -1;
               iFrame1 = -1;
               iRef0 = 0;
               iRef1 = 0;
            }
         }

         int iFirst = iFrame0;
         if (iFirst < iFirstArg)
         {
            iFirst = iFirstArg;
         }
         int iLast = iFrame1;
         if (iLast > iLastArg)
         {
            iLast = iLastArg;
         }
         for (int iFrame = iFirst; iFrame <= iLast; iFrame++)
         {
            for (int iQuant = 0; iQuant < TL_QUANTILE_COUNT; iQuant++)
            {
               float fOldR = mdp->fpQuantile[0][iQuant][iFrame];
               float fOldG = mdp->fpQuantile[1][iQuant][iFrame];
               float fOldB = mdp->fpQuantile[2][iQuant][iFrame];

               // how does the iRef0 create a new value
               float fNewR0, fNewG0, fNewB0;
               getNew(iaRefType[iRef0], fOldR, fOldG, fOldB, faRefR[iRef0][iQuant], faRefG[iRef0][iQuant], faRefB[iRef0][iQuant], &fNewR0,
                   &fNewG0, &fNewB0);

               // how does the iRef1 create a new value
               float fNewR1, fNewG1, fNewB1;
               getNew(iaRefType[iRef1], fOldR, fOldG, fOldB, faRefR[iRef1][iQuant], faRefG[iRef1][iQuant], faRefB[iRef1][iQuant], &fNewR1,
                   &fNewG1, &fNewB1);

               // the weights
               float fWei0;
               float fWei1;
               if (iaRefFrame[iRef0] == iaRefFrame[iRef1])
               {
                  fWei1 = 1.;
               }
               else
               {
                  fWei1 = (float)(iFrame - iaRefFrame[iRef0]) / (float)(iaRefFrame[iRef1] - iaRefFrame[iRef0]);
               }

               fWei0 = 1.f - fWei1;

               float fNewR, fNewG, fNewB;

               fNewR = fWei0 * fNewR0 + fWei1 * fNewR1;
               fNewG = fWei0 * fNewG0 + fWei1 * fNewG1;
               fNewB = fWei0 * fNewB0 + fWei1 * fNewB1;

               mdp->fpRepair[0][iQuant][iFrame] = fNewR;
               mdp->fpRepair[1][iQuant][iFrame] = fNewG;
               mdp->fpRepair[2][iQuant][iFrame] = fNewB;
            }
         }
      }
   }

}

void CGraphTimeline::MovingAverageQuantile(int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast)
{

   for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
   {
      for (int iQuant = 0; iQuant < TL_QUANTILE_COUNT; iQuant++)
      {
         MovingAverage(iChan, iRefFirst, iRefLast, iMovingAveFirst, iMovingAveLast, mdProcess.fpRepair[iChan][iQuant]);
      }
   }

}

void CGraphTimeline::SetQuantileMaximum(unsigned int dataMaximum)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   _histogramDataMaximum = dataMaximum + 1;

   for (int i = 0; i < TL_CHAN_COUNT; i++)
   {
      for (int j = 0; j <= HIST_COUNT; j++)
      {
         _histogramLevels[i][j] = (int)(((double)j * _histogramDataMaximum / (double)HIST_COUNT) + .5);
      };

      _histogramLevelsCount[i] = HIST_COUNT + 1;
   }
}

static_assert(TL_CHAN_COUNT == 3, "Number of channels must be three for this version");

IppStatus ippiHistogramRange_16u_C3R(const Ipp16u* pSrc, int srcStep, IppiSize roiSize, Ipp32u* pHist[3], const Ipp32s* poLevels[3],
    int noLevels[3], Ipp32f dataMax)
{
   // const int nBins = HIST_COUNT;
   // const int nChannels = 3;
   // int nLevels[] = { nBins+1, nBins+1, nBins+1 };
   // Ipp32f lowerLevel[] = {0, 0, 0};
   // Ipp32f upperLevel[] = {dataMax, dataMax, dataMax};
   // Ipp32f pLevels0[nBins+1], pLevels1[nBins+1], pLevels2[nBins+1],*ppLevels[3];
   // int sizeHistObj, sizeBuffer;
   //
   // IppiHistogramSpec* pHistObj;
   // Ipp8u* pBuffer;
   // Ipp32u *ppHistVec[3] = {pHist[0], pHist[1], pHist[2]};
   //
   // // get sizes for spec and buffer
   // ippiHistogramGetBufferSize(ipp16u, roiSize, nLevels, nChannels/*nChan*/, 1/*uniform*/, &sizeHistObj, &sizeBuffer);
   //
   // pHistObj = (IppiHistogramSpec*)ippsMalloc_8u( sizeHistObj );
   // pBuffer = (Ipp8u*)ippsMalloc_8u( sizeBuffer );
   //
   // // initialize spec
   // ippiHistogramUniformInit( ipp16u, lowerLevel, upperLevel, nLevels, nChannels, pHistObj );
   //
   // // check levels of bins
   // ppLevels[0] = pLevels0;
   // ppLevels[1] = pLevels1;
   // ppLevels[2] = pLevels2;
   // auto sts = ippiHistogramGetLevels( pHistObj, ppLevels );
   //
   // // calculate histogram
   // sts = ippiHistogram_16u_C3R(pSrc , srcStep, roiSize, ppHistVec, pHistObj, pBuffer );
   //
   // ippsFree( pHistObj );
   // ippsFree( pBuffer );
   //
   // return sts;
   const int nBins = HIST_COUNT;
   const int nChan = 3;
   int nLevels[] =
   {nBins + 1, nBins + 1, nBins + 1};
   Ipp32f lowerLevel[] =
   {0, 0, 0};
   Ipp32f upperLevel[] =
   {dataMax, dataMax, dataMax};
   Ipp32f pLevels0[nBins + 1], pLevels1[nBins + 1], pLevels2[nBins + 1], *ppLevels[3];
   int sizeHistObj, sizeBuffer;
   IppiHistogramSpec* pHistObj;
   Ipp8u* pBuffer;
   Ipp32u *pHistVec[3] =
   {pHist[0], pHist[1], pHist[2]};

   // get sizes for spec and buffer
   ippiHistogramGetBufferSize(ipp16u, roiSize, nLevels, nChan, 1, &sizeHistObj, &sizeBuffer);
   pHistObj = (IppiHistogramSpec*)ippsMalloc_8u(sizeHistObj);
   pBuffer = (Ipp8u*)ippsMalloc_8u(sizeBuffer);
   // initialize spec
   ippiHistogramUniformInit(ipp16u, lowerLevel, upperLevel, nLevels, nChan, pHistObj);
   // check levels of bins
   ppLevels[0] = pLevels0;
   ppLevels[1] = pLevels1;
   ppLevels[2] = pLevels2;
   auto sts = ippiHistogramGetLevels(pHistObj, ppLevels);

   // calculate histogram
   sts = ippiHistogram_16u_C3R(pSrc, srcStep, roiSize, pHistVec, pHistObj, pBuffer);

   ippsFree(pHistObj);
   ippsFree(pBuffer);

   return sts;
}

int CGraphTimeline::MakeQuantiles(const Ipp16u* source, int stepInBytes, IppiSize roiSize, RGBFrameDatum &rgbFrameDatum)
{
   CAutoSpinLocker dataChangeLocker(dataChangeLock);

   // The 3 is hardwired, canot be changed
   Ipp32u *ipHist[3];
   const Ipp32s *ipLevels[3];
   for (int i = 0; i < 3; i++)
   {
      ipHist[i] = (Ipp32u*)_histogram[i];
      ipLevels[i] = _histogramLevels[i];
   }

   auto status = ippiHistogramRange_16u_C3R(source, stepInBytes, roiSize, ipHist, ipLevels, _histogramLevelsCount, _histogramDataMaximum);
   if (status)
   {
      return -1;
   }

   _histogramSourceCount = roiSize.height * roiSize.width;

   for (int i = 0; i < 3; i++)
   {
      auto iRet = MakeQuantile(i, rgbFrameDatum.Quantile[i]);
      if (iRet != 0)
      {
         return -1;
      }
   }

   return 0;
}

int CGraphTimeline::MakeQuantile(int iChannel, double daQuant[TL_QUANTILE_COUNT])
{
   // convert to probabilities
   double daProb[HIST_COUNT];
   double daLevel[HIST_COUNT];
   for (int i = 0; i < HIST_COUNT; i++)
   {
      daProb[i] = (double)_histogram[iChannel][i] / (double)_histogramSourceCount;
      daLevel[i] = ((double)(_histogramLevels[iChannel][i] + _histogramLevels[iChannel][i + 1]) / 2.) / _histogramDataMaximum;
   }

   // zero the quantiles
   for (int i = 0; i < TL_QUANTILE_COUNT; i++)
   {
      daQuant[i] = 0.;
   }

   // find the average value in each quantile
   int iQuant = 0;
   double dSumProb = 0.f;
   double dTargetProb = 1. / (double)TL_QUANTILE_COUNT;
   int iLevel = 0;
   while (iQuant < TL_QUANTILE_COUNT && iLevel < HIST_COUNT)
   {
      double dProb = daProb[iLevel];
      if (dSumProb + dProb > dTargetProb)
      {
         dProb = dTargetProb - dSumProb;
      }

      daQuant[iQuant] += (dProb * daLevel[iLevel]);
      daProb[iLevel] -= dProb;
      dSumProb += dProb;

      if (daProb[iLevel] <= 0.)
      {
         // finished with this level.  Move to the next one
         iLevel++;
      }

      if (dSumProb >= dTargetProb || iLevel == HIST_COUNT)
      {
         // finished with this quantile.  Normalize to create average.
         daQuant[iQuant] /= dSumProb;

         // Move to the next quantile
         iQuant++;
         dSumProb = 0.f;
      }
   }

  double quant[TL_QUANTILE_COUNT];
  for (auto i = 0; i < TL_QUANTILE_COUNT; i++)
  {
     quant[i] = daQuant[i];
  }
   return 0;
}

void CGraphTimeline::DumpRGBData()
{
   auto file = fopen("c:\\temp\\RGBData.txt", "w");
   for (auto i = 0; i < _totalFrames; i++)
   {
      std::ostringstream os;
      os << i << ") ";
      for (auto c = 0; c < TL_CHAN_COUNT; c++)
      {
         os << mdProcess.fpValOrig[c][i] << " ";
      }

      os << "\n" << i << ") ";

      for (auto c = 0; c < TL_CHAN_COUNT; c++)
      {
         os << mdProcess.fpValProc[c][i] << " ";
      }

      fprintf(file, "%s\n\n", os.str().c_str());
   }

   fclose(file);
}

void CGraphTimeline::LoadRGBData()
{
   ifstream file("c:\\temp\\RGBData.txt");
   while (!file.eof())
   {
      string str;
      std::getline(file, str);
      if (str.size() < 2)
      {
         continue;
      }

      std::stringstream ss(str);

      int iFrame;
      float fVal;
      char p;
      ss >> iFrame;

      if (iFrame >= _totalFrames)
      {
         break;
      }

      ss >> p;
      ss >> mdProcess.fpValOrig[0][iFrame];
      ss >> mdProcess.fpValOrig[1][iFrame];
      ss >> mdProcess.fpValOrig[2][iFrame];

      std::getline(file, str);
      ss >> iFrame;
      ss >> p;
      ss >> mdProcess.fpValProc[0][iFrame];
      ss >> mdProcess.fpValProc[1][iFrame];
      ss >> mdProcess.fpValProc[2][iFrame];
   }

   SetGraphDirty();
}
