#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <Windows.h>
#include <System.SysUtils.hpp> // For Exception
#undef min  // defined in Windows.h
#undef max  // defined in Windows.h

#include "FlickerProcess.h"
#include "FileIO.h"
#include "IniFile.h"
#include "HRTimer.h"
#include "SysInfo.h"
#include "mthread.h"
#include "Ippheaders.h"

CFlickerProcess::CFlickerProcess()
{
}

CFlickerProcess::~CFlickerProcess()
{
}

void CFlickerProcess::setImageSize(int nRow, int nCol, int dataMax)
{
   _height = nRow;
   _width = nCol;
   _dataMax = dataMax;
}

static int filter_func(unsigned int code, struct _EXCEPTION_POINTERS *ep)
{
      return EXCEPTION_EXECUTE_HANDLER;
}

   static void SliceThreadStartV4(void *vpAppData, void *vpReserved)
   {
      CAutoErrorReporter autoErr("SliceThreadStartV4", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      // Cast the pointer
      auto jobData = reinterpret_cast<JobData*>(vpAppData);
      auto jobNumber = jobData->jobNumber;

      if (BThreadBegin(vpReserved))
      {
         TRACE_0(errout << "FlickerProcess: BThreadBegin failed");
         jobData->countingWait.ReleaseOne();
         return;
      }

      try
      {
         int exceptionCode = 0;
         try
         {
            jobData->flickerProcess->JobThreadCallV4(jobData, jobNumber);
         }
         catch (Exception &ex)
         {
            TRACE_0(errout << "FlickerProcess: Display Slice created major error");
				autoErr.errorCode = -9080;
            autoErr.msg << ex.ToString().c_str() << endl << " FlickerProcess: Display Slice created major error";

            jobData->countingWait.ReleaseOne();
         }

         // __except (filter_func(GetExceptionCode(), GetExceptionInformation()))
         // {
         // exceptionCode = GetExceptionCode();
         //
         // // TRACE_0(errout << "***ERROR**** a BThread has thrown an unknown error");
         // // TRACE_0(errout << "             BThread task created @ " << task->callerFilePathForDebugging << ":" <<
         // // // task->callerLineNumberForDebugging);
         // //
         //
         // // << "The thread was created at " << task->callerFilePathForDebugging << '[' << task->callerLineNumberForDebugging << ']';
         // jobData->countingWait.ReleaseOne();
         //
         // }

         // auto s = SysInfo::seDescription(exceptionCode);
			// autoErr.errorCode = -9082;
         // autoErr.msg << "THREAD CRASHED - SEH error" << endl << SysInfo::seDescription(exceptionCode);
         // jobData->countingWait.ReleaseOne();
      }
      catch (...)
      {
         TRACE_0(errout << "FlickerProcess: Display Slice created major error");
			autoErr.errorCode = -9081;
         autoErr.msg << "THREAD CRASHED (std::exception) - reason: " << endl << " FlickerProcess: Display Slice created major error";

         jobData->countingWait.ReleaseOne();
      }
   }


static void DisplaySliceThreadStartV4(void *vpAppData, void *vpReserved)
{
   CAutoErrorReporter autoErr("DisplaySliceThreadStartV4", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   // Cast the pointer
   auto jobData = reinterpret_cast<JobData *>(vpAppData);
   auto jobNumber = jobData->jobNumber;

   if (BThreadBegin(vpReserved))
   {
      TRACE_0(errout << "FlickerProcess: BThreadBegin failed");
      jobData->countingWait.ReleaseOne();
      return;
   }

   try
   {
      jobData->flickerProcess->DisplayJobThreadCallV4(jobData, jobNumber);
   }
   catch (...)
   {
      TRACE_0(errout << "FlickerProcess: Display Slice created major error");
      autoErr.errorCode = -9083;
      autoErr.msg << "THREAD CRASHED (std::exception) - reason: " << endl << " FlickerProcess: Display Slice created major error";
      jobData->countingWait.ReleaseOne();
   }
}

void CFlickerProcess::JobThreadCallV4(JobData *jobData, int iJob)
{
   auto height = jobData->height / jobData->slices;
   auto rowIn = height * iJob;
   auto rowOut = rowIn + height;

   if (iJob == jobData->slices - 1)
   {
      rowOut = jobData->height;
   }

   auto faX = static_cast<float (*)[12]>(jobData->fpX);
   auto faY = static_cast<float (*)[12]>(jobData->fpY);

   jobData->flickerProcess->RenderSliceV4(jobData->rgb, faX, faY,jobData->ipCount, rowIn, rowOut);
   jobData->countingWait.ReleaseOne();
}

void CFlickerProcess::DisplayJobThreadCallV4(JobData *jobData, int iJob)
{
   auto height = jobData->height / jobData->slices;
   auto rowIn = height * iJob;
   auto rowOut = rowIn + height;

   if (iJob == jobData->slices - 1)
   {
      rowOut = jobData->height;
   }
   auto faX = static_cast<float (*)[12]>(jobData->fpX);
   auto faY = static_cast<float (*)[12]>(jobData->fpY);

   jobData->flickerProcess->RenderDisplaySliceV4((unsigned char *)jobData->rgb,  faX, faY, jobData->ipCount, jobData->width, rowIn, rowOut);

   jobData->countingWait.ReleaseOne();
}

  int CFlickerProcess::RenderSliceV4(unsigned short *rgb, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT], int rowIn, int rowOut)
{
  IppStatus status;

  // make the LUT.
  int nLevels[3];
  Ipp32s *pValues16[3];
  Ipp32s *pLevels16[3];

  for (int iChan = 0; iChan < 3; iChan++)
  {
      int dataMax = _dataMax;
      nLevels[iChan] = iaCount[iChan];
      pValues16[iChan] = ippsMalloc_32s (_dataMax + 1);
      pLevels16[iChan] = ippsMalloc_32s (_dataMax + 1);

      for (int i = 0; i < iaCount[iChan]; i++)
      {
          pLevels16[iChan][i] = (int)(faX[iChan][i] * (float)dataMax + .5f);
 		  pValues16[iChan][i] = (int)(faY[iChan][i] * (float)dataMax + .5f);
      }
  }

  const Ipp32s *pV[3];
  pV[0] = pValues16[0];
  pV[1] = pValues16[1];
  pV[2] = pValues16[2];

  const Ipp32s *pL[3];
  pL[0] = pLevels16[0];
  pL[1] = pLevels16[1];
  pL[2] = pLevels16[2];

  auto roiLeft = _processingRoi.left;
  auto roiWidth =  _processingRoi.right - _processingRoi.left;
  rowIn = std::max<int>(rowIn, _processingRoi.top);
  rowOut = std::min<int>(rowOut, _processingRoi.bottom);

  // Do nothing if nothing to do
  if (rowOut <= rowIn)
  {
     return 0;
  }

  if (roiWidth <= 0)
  {
     return 0;
  }

  auto rgb2 = rgb + 3 * (rowIn * _width + roiLeft);

  IppiSize roiSize {roiWidth, rowOut - rowIn};

  // Replacement for 9.0
  int specSize;
  IppiLUT_Spec* pSpec;

  ippiLUT_GetSize( ippLinear, ipp16u, ippC3, roiSize, nLevels, &specSize );

   pSpec = (IppiLUT_Spec*)ippMalloc( specSize );

   ippiLUT_Init_16u( ippLinear, ippC3, roiSize, pV, pL, nLevels, pSpec );

   status = ippiLUT_16u_C3IR( rgb2, 6*_width, roiSize, pSpec );

// This is debugging code that puts in gray as render
//   Ipp16u value[3] = {500, 500, 500};
//   ippiSet_16u_C3R(value, rgb2, 6*_width, roiSize);

   ippFree( pSpec );

  if (status != ippStsNoErr)
  {
  	  TRACE_0(errout << "IPP Error in LUT #" << status);
      MTIassert(false);
  }

  // Free it up
  for (int iChan = 0; iChan < 3; iChan++)
  {
      ippsFree(pValues16[iChan]);
      ippsFree(pLevels16[iChan]);
  }

  if (status)
  {
      return -1;
  }

  return 0;
}

int CFlickerProcess::RenderDisplaySliceV4(unsigned char *rgba, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT], int width, int rowIn, int rowOut)
{
 IppStatus status;

  // make the LUT.
  int nLevels[3];
  Ipp32s *pValues8[3];
  Ipp32s *pLevels8[3];

  for (int iChan = 0; iChan < 3; iChan++)
  {
      int dataMax = IPP_MAX_8U;
      nLevels[iChan] = iaCount[iChan];
      pValues8[iChan] = ippsMalloc_32s (dataMax + 1);
      pLevels8[iChan] = ippsMalloc_32s (dataMax + 1);

      for (int i = 0; i < iaCount[iChan]; i++)
      {
          pLevels8[iChan][i] = (int)(faX[iChan][i] * (float)dataMax + .5f);
		  pValues8[iChan][i] = (int)(faY[iChan][i] * (float)dataMax + .5f);
          if ((iChan == 0) && (rowIn == 0))
          {
             TRACE_0(errout << i << ", " << pLevels8[iChan][i]);
          }
      }
  }

  const Ipp32s *pV[3];
  pV[0] = pValues8[0];
  pV[1] = pValues8[1];
  pV[2] = pValues8[2];

  const Ipp32s *pL[3];
  pL[0] = pLevels8[0];
  pL[1] = pLevels8[1];
  pL[2] = pLevels8[2];

  auto rgb2 = rgba + 4 * rowIn * width;

  IppiSize roiSize {width, rowOut - rowIn};
  // Replacement for 9.0
  int specSize;
  IppiLUT_Spec* pSpec;

  ippiLUT_GetSize( ippLinear, ipp8u, ippAC4, roiSize, nLevels, &specSize );

   pSpec = (IppiLUT_Spec*)ippMalloc( specSize );

   ippiLUT_Init_8u( ippLinear, ippAC4, roiSize, pV, pL, nLevels, pSpec );

   status = ippiLUT_8u_AC4IR( rgb2, 6*roiSize.width, roiSize, pSpec );

   ippFree( pSpec );
  // End of 9.0 replacement

//  status = ippiLUT_Linear_8u_AC4R(rgb2, 4*roiSize.width, rgb2, 4*roiSize.width,
//               roiSize, pV, pL, nLevels);

  if (status != ippStsNoErr)
  {
  	  TRACE_0(errout << "IPP Error in LUT #" << status);
      MTIassert(false);
  }

  // Free it up
  for (int iChan = 0; iChan < 3; iChan++)
  {
      ippsFree(pValues8[iChan]);
      ippsFree(pLevels8[iChan]);
  }

  if (status)
  {
      return -1;
  }

  return 0;
}

int CFlickerProcess::RenderDisplayV4(unsigned char *rgba, int width, int height,
   float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
   int iaCount[TL_CHAN_COUNT])
{
	int threads = SysInfo::AvailableProcessorCount() - 2;

	_jobData.flickerProcess = this;
	_jobData.rgb = (unsigned short *)rgba;
    _jobData.slices = threads;
	_jobData.countingWait.SetCount(threads);
	_jobData.jobNumber = 0;
    _jobData.fpX = faX;
	_jobData.fpY = faY;
	_jobData.ipCount = iaCount;
	_jobData.width = width;
	_jobData.height = height;

   vector<void *> bThreadStructs;
	for (int i = 0; i < threads; i++)
	{
		auto ts = BThreadSpawn(DisplaySliceThreadStartV4, &_jobData);
      MTIassert(ts != nullptr);
      bThreadStructs.push_back(ts);

		_jobData.jobNumber++;
	}

	// Now wait for it to finish
	_jobData.countingWait.Wait(bThreadStructs, __func__, __LINE__);

	return 0;
}

int CFlickerProcess::RenderV4(unsigned short *rgb, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
   float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], int iaCount[TL_CHAN_COUNT], const RECT &roi)
{
	int threads = SysInfo::AvailableProcessorCount() - 2;

	_jobData.flickerProcess = this;
	_jobData.rgb = rgb;
	_jobData.slices = threads;
	_jobData.countingWait.SetCount(threads);
	_jobData.jobNumber = 0;
	_jobData.fpX = faX;
	_jobData.fpY = faY;
	_jobData.ipCount = iaCount;
    _jobData.width = _width;
    _jobData.height = _height;

    _processingRoi = roi;
   vector<void *> bThreadStructs;
	for (int i = 0; i < threads; i++)
	{
		auto ts = BThreadSpawn(SliceThreadStartV4, &_jobData);
      MTIassert(ts != nullptr);
      bThreadStructs.push_back(ts);

		_jobData.jobNumber++;
	}

	// Now wait for it to finish
	_jobData.countingWait.Wait(bThreadStructs, __func__, __LINE__);

	return 0;
}
