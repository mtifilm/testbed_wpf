#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <Windows.h>
#include "IniFile.h"

#undef min  // defined in Windows.h
#undef max  // defined in Windows.h

#include "FlickerShot.h"

CFlickerSetting::CFlickerSetting()
{
   iReferenceFrame = -1;
   iReferenceType = FS_REFERENCE_TYPE_NONE;
   iaMovingAve[0] = 0;
   iaMovingAve[1] = 0;
   iaMovingAve[2] = 0;
}

CFlickerSetting::~CFlickerSetting()
{
}

CFlickerShot::CFlickerShot()
{
   iIn = -1;
   iOut = -1;
}

CFlickerShot::~CFlickerShot()
{
   listSetting.clear();
}

void CFlickerShot::setRange(int iInArg, int iOutArg)
{
   iIn = iInArg;
   iOut = iOutArg;

   // erase any settings which are outside the range
   vector<CFlickerSetting>::iterator it;
   for (it = listSetting.begin(); it != listSetting.end();)
   {
      if (it->iReferenceFrame < iIn || it->iReferenceFrame > iOut)
      {
         // outside the range.  Erase it
         it = listSetting.erase(it);
      }
      else
      {
         // move to element in list
         it ++;
      }
   }

}

vector <CFlickerSetting> &CFlickerShot::getSettingList()
{
   return listSetting;
}

bool CFlickerShot::AreSettingsTheSame(CFlickerShot &shot)
{
   if ((getIn() != shot.getIn()) || (getOut() != shot.getOut()))
   {
      return false;
   }

   if (getSettingCount() != shot.getSettingCount())
   {
      return false;
   }

   auto settings = getSettingList();
   auto shotSettings = shot.getSettingList();
   for (auto i=0; i < settings.size(); i++)
   {
       if (!settings[i].AreTheSame(shotSettings[i]))
       {
          return false;
       }
   }

   return true;
}

int CFlickerShot::getIn()
{
   return iIn;
}

int CFlickerShot::getOut()
{
   return iOut;
}

int CFlickerShot::getSettingCount()
{
   return (int)listSetting.size();
}

void CFlickerShot::getSetting(int iSettingIndex, int *ipReferenceFrame, int *ipReferenceType, int *ipMovingAve)
{
   *ipReferenceFrame = -1;
   *ipReferenceType = FS_REFERENCE_TYPE_NONE;
   if (ipMovingAve != nullptr)
   {
      ipMovingAve[0] = 0;
      ipMovingAve[1] = 0;
      ipMovingAve[2] = 0;
   }

   if (iSettingIndex < 0 || iSettingIndex >= listSetting.size())
   {
      return;
   }

   *ipReferenceFrame = listSetting.at(iSettingIndex).iReferenceFrame;
   *ipReferenceType = listSetting.at(iSettingIndex).iReferenceType;

   if (ipMovingAve != 0)
   {
      ipMovingAve[0] = listSetting.at(iSettingIndex).iaMovingAve[0];
      ipMovingAve[1] = listSetting.at(iSettingIndex).iaMovingAve[1];
      ipMovingAve[2] = listSetting.at(iSettingIndex).iaMovingAve[2];
   }
}

void CFlickerShot::getSetting(int iReferenceFrame, int *ipReferenceType, int *ipMovingAve)
{
   *ipReferenceType = FS_REFERENCE_TYPE_NONE;
   ipMovingAve[0] = 0;
   ipMovingAve[1] = 0;
   ipMovingAve[2] = 0;

   vector<CFlickerSetting>::iterator it;
   for (it = listSetting.begin(); it != listSetting.end(); it ++)
   {
      if (it->iReferenceFrame == iReferenceFrame)
      {
         *ipReferenceType = it->iReferenceType;
         ipMovingAve[0] = it->iaMovingAve[0];
         ipMovingAve[1] = it->iaMovingAve[1];
         ipMovingAve[2] = it->iaMovingAve[2];
      }
   }

}

int CFlickerShot::getReferenceFrame(int iSettingIndex)
{

   if (iSettingIndex < 0 || iSettingIndex >= listSetting.size())
   {
      return -1;
   }

   return listSetting.at(iSettingIndex).iReferenceFrame;
}

void CFlickerShot::addSetting(int iReferenceFrame, int iReferenceType, int *ipMovingAve)
   /*
    The addSetting function will add a new collection of settings.  If settings already
    exist at this frame, just return
    */
{

   CFlickerSetting setting;
   setting.iReferenceFrame = iReferenceFrame;
   setting.iReferenceType = iReferenceType;
   setting.iaMovingAve[0] = ipMovingAve[0];
   setting.iaMovingAve[1] = ipMovingAve[1];
   setting.iaMovingAve[2] = ipMovingAve[2];

   // determine where this setting belongs in the list
   vector<CFlickerSetting>::iterator it;
   bool bInserted = false;
   for (it = listSetting.begin(); it != listSetting.end() && bInserted == false; it ++)
   {
      if (it->iReferenceFrame == setting.iReferenceFrame)
      {
         // This reference frame already exists, just return
         return;
      }
      else if (it->iReferenceFrame > setting.iReferenceFrame)
      {
         // insert a new setting before current one
         it = listSetting.insert(it, setting);

         bInserted = true;
      }
   }

   if (bInserted == false)
   {
      // append to end
      listSetting.push_back(setting);
   }

}

void CFlickerShot::setSetting(int iReferenceFrame, int iReferenceType, int *ipMovingAve)
   /*
    setSetting will modify the settings.  If this reference frame does not exist,
    just return
    */
{
   CFlickerSetting setting;
   setting.iReferenceFrame = iReferenceFrame;
   setting.iReferenceType = iReferenceType;
   setting.iaMovingAve[0] = ipMovingAve[0];
   setting.iaMovingAve[1] = ipMovingAve[1];
   setting.iaMovingAve[2] = ipMovingAve[2];

   // determine where this setting belongs in the list
   vector<CFlickerSetting>::iterator it;
   bool bKeepLooking = true;
   for (it = listSetting.begin(); it != listSetting.end() && bKeepLooking == true; it ++)
   {
      if (it->iReferenceFrame == setting.iReferenceFrame)
      {
         // need to modify the existing entry
         *it = setting;

         bKeepLooking = false;
      }
   }

}

void CFlickerShot::delSetting(int iReferenceFrame)
{
   // determine where this reference frame is in the list
   vector<CFlickerSetting>::iterator it;
   for (it = listSetting.begin(); it != listSetting.end();)
   {
      if (it->iReferenceFrame == iReferenceFrame)
      {
         it = listSetting.erase(it);
      }
      else
      {
         it ++;
      }
   }
}

void CFlickerShot::clearSetting()
{
   listSetting.clear();
}

CFlickerShotList::CFlickerShotList()
{
}

CFlickerShotList::~CFlickerShotList()
{
   _flickerShotVector.clear();
}

int CFlickerShotList::getShotCount()
{
   return (int)_flickerShotVector.size();
}

int CFlickerShotList::getShotIndex(int iFrame)
{
   int iShotIndex = -1;

   for (int i = 0; i < _flickerShotVector.size(); i ++)
   {
      if (iFrame >= _flickerShotVector.at(i).getIn() && iFrame <= _flickerShotVector.at(i).getOut())
      {
         iShotIndex = i;
      }
   }

   return iShotIndex;
}

int CFlickerShotList::getShotIn(int iShotIndex)
{
   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return -1;
   }

   return _flickerShotVector.at(iShotIndex).getIn();
}

int CFlickerShotList::getShotOut(int iShotIndex)
{
   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return -1;
   }

   return _flickerShotVector.at(iShotIndex).getOut();
}

int CFlickerShotList::getSettingCount(int iShotIndex)
{
   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return -1;
   }

   return _flickerShotVector.at(iShotIndex).getSettingCount();
}

void CFlickerShotList::getSetting(int iShotIndex, int iSettingIndex, int *ipReferenceFrame, int *ipReferenceType,
   int *ipMovingAve)
{
   *ipReferenceFrame = -1;
   *ipReferenceType = FS_REFERENCE_TYPE_NONE;
   if (ipMovingAve != 0)
   {
      ipMovingAve[0] = 0;
      ipMovingAve[1] = 0;
      ipMovingAve[2] = 0;
   }

   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return;
   }

   _flickerShotVector.at(iShotIndex).getSetting(iSettingIndex, ipReferenceFrame, ipReferenceType, ipMovingAve);

   return;
}

void CFlickerShotList::getSetting(int iFrame, int *ipReferenceType, int *ipMovingAve)
{
   *ipReferenceType = FS_REFERENCE_TYPE_NONE;
   ipMovingAve[0] = 0;
   ipMovingAve[1] = 0;
   ipMovingAve[2] = 0;

   int iShotIndex = getShotIndex(iFrame);

   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return;
   }

   _flickerShotVector.at(iShotIndex).getSetting(iFrame, ipReferenceType, ipMovingAve);

   return;
}

//void CFlickerShotList::addSetting(int iReferenceFrame, int iReferenceType, int *ipMovingAve)
//{
//   int iShotIndex = getShotIndex(iReferenceFrame);
//
//   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
//   {
//      return;
//   }
//
//   _flickerShotVector.at(iShotIndex).addSetting(iReferenceFrame, iReferenceType, ipMovingAve);
//
//}

void CFlickerShotList::setSetting(int iReferenceFrame, int iReferenceType, int *ipMovingAve)
{
   int iShotIndex = getShotIndex(iReferenceFrame);

   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return;
   }

   _flickerShotVector.at(iShotIndex).setSetting(iReferenceFrame, iReferenceType, ipMovingAve);

}

int CFlickerShotList::getReferenceFrame(int iShotIndex, int iSettingIndex)
{

   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return -1;
   }

   return _flickerShotVector.at(iShotIndex).getReferenceFrame(iSettingIndex);
}

//void CFlickerShotList::delSetting(int iReferenceFrame)
//{
//   int iShotIndex = getShotIndex(iReferenceFrame);
//
//   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
//   {
//      return;
//   }
//
//   _flickerShotVector.at(iShotIndex).delSetting(iReferenceFrame);
//
//}

void CFlickerShotList::clearSetting(int iShotIndex)
{
   if (iShotIndex < 0 || iShotIndex >= _flickerShotVector.size())
   {
      return;
   }

   _flickerShotVector.at(iShotIndex).clearSetting();

}

void CFlickerShotList::clear()
{
   _flickerShotVector.clear();
}

void CFlickerShotList::loadShotList(const string &strDirName, int iFirst, int iLast)
{
   // clear previous list
   _flickerShotVector.clear();

   if (loadFlickerFile(strDirName, iFirst, iLast) != 0)
   {
      // failed to load the flicker instruction file.  Try the generic shot list file
      loadShotListFile(strDirName, iFirst, iLast);
   }

}


void CFlickerShotList::loadShotListFile(const string &strDirName, int iFirst, int iLast)
{
   string strFileName = strDirName + "shotlist.txt";

   // open the file
   errno_t err;
   FILE *fp;
   vector<int>fileEntry;

   // open the shot list and read it in
   err = fopen_s(&fp, strFileName.c_str(), "r");
   if (err == 0)
   {
      // opening the file was a success
      // read the contents

      int iFrame;
      int iFrameLast = -1;
      while (fscanf_s(fp, "%d", &iFrame) == 1)
      {
         while (getc(fp) != '\n');

         if (iFrame > iFrameLast)
         {
            // frame sequence is in the correct order.

            if (iFrame >= iFirst && iFrame <= iLast)
            {
               // this frame is within the sequence of frames
               fileEntry.push_back(iFrame);
            }

            iFrameLast = iFrame;
         }
      }

      fclose(fp);

      // if the first frame in the sequence is not in the list, add it here
      if (fileEntry.at(0) != iFirst)
      {
         fileEntry.insert(fileEntry.begin(), iFirst);
      }

      // add one additional element to close the list
      fileEntry.push_back(iLast + 1);
   }
   else
   {
      // could not open file.  Force the list to the entire range
      fileEntry.push_back(iFirst);
      fileEntry.push_back(iLast + 1);
   }

   for (int i = 0; i < fileEntry.size() - 1; i ++)
   {
      CFlickerShot shot;
      shot.setRange(fileEntry.at(i), fileEntry.at(i + 1) - 1);

      _flickerShotVector.push_back(shot);
   }

   fileEntry.clear();

}

bool CFlickerShotList::setShotList(vector<CFlickerShot> &newShotList)
{
   auto same = true;
   if (_flickerShotVector.size() ==  newShotList.size())
   {
      for (auto i=0; i < _flickerShotVector.size(); i++)
      {
         if (!_flickerShotVector[i].AreSettingsTheSame(newShotList[i]))
         {
            same = false;
            break;
         }
      }
   }
   else
   {
      same = false;
   }

   if (!same)
   {
      _flickerShotVector = newShotList;

//      auto ini = CreateIniFile("c:\\temp\\shots.ini");
//      int i = 0;
//      for (auto &shot : _flickerShotVector)
//      {
//          auto section = "Shot" + to_string(i);
//          ini->WriteInteger(section, "In", shot.getIn());
//          ini->WriteInteger(section, "Out", shot.getOut());
//          ini->WriteInteger(section, "Count", shot.getSettingCount());
//          i++;
//          int j = 0;
//          for (auto &s : shot.getSettingList())
//          {
//               auto id =  "." + to_string(j++);
//               ini->WriteInteger(section, "Frame" + id, s.iReferenceFrame);
//               ostringstream os;
//               os << hex << s.iReferenceType;
//               ini->WriteString(section, "Type" + id, os.str());
//               ini->WriteInteger(section, "MA0" + id, s.iaMovingAve[0]);
//               ini->WriteInteger(section, "MA1" + id, s.iaMovingAve[0]);
//               ini->WriteInteger(section, "MA2" + id, s.iaMovingAve[0]);
//          }
//      }
//
//      DeleteIniFile(ini);
   }

   return same;
}

int CFlickerShotList::loadFlickerFile(const string &strDirName, int iFirst, int iLast)
{

   string strFileName = strDirName + "FlickerInstruction.dat";

   // open the file
   errno_t err;
   FILE *fp;
   vector<int>fileEntry;

   // open the flicker file and read it in
   err = fopen_s(&fp, strFileName.c_str(), "rb");
   if (err != 0)
   {
      // error opening the file.  Return
      return -1;
   }

   // opening the file was a success
   // read the contents

   int iaTmp[5];

   // read the number of shots, first and last frames
   if (fread(iaTmp, sizeof(int), 3, fp) != 3)
   {
      fclose(fp);
      return -1;
   }

   int iNShot = iaTmp[0];
   if (iaTmp[1] != iFirst || iaTmp[2] != iLast)
   {
      fclose(fp);
      return -1;
   }

   for (int iShot = 0; iShot < iNShot; iShot ++)
   {
      CFlickerShot shot;

      // read the number of settings, the in and out
      if (fread(iaTmp, sizeof(int), 3, fp) != 3)
      {
         fclose(fp);
         return -1;
      }

      int iNSetting = iaTmp[0];
      shot.setRange(iaTmp[1], iaTmp[2]);

      for (int iSetting = 0; iSetting < iNSetting; iSetting ++)
      {
         if (fread(iaTmp, sizeof(int), 5, fp) != 5)
         {
            fclose(fp);
            return -1;
         }

         shot.addSetting(iaTmp[0], iaTmp[1], iaTmp + 2);
      }
      _flickerShotVector.push_back(shot);
   }

   fclose(fp);
   return 0;

}

int CFlickerShotList::saveFlickerFile(const string &strDirName)
{

   // open the file
   errno_t err;
   FILE *fp;
   vector<int>fileEntry;

   string strFileName = strDirName + "FlickerInstruction.dat";

   // open the flicker file and write it out
   err = fopen_s(&fp, strFileName.c_str(), "wb");
   if (err != 0)
   {
      // error opening the file.  Return
      return -1;
   }

   // opening the file was a success
   // read the contents

   int iaTmp[5];

   // write the number of shots, first and last frames
   int iNShot = (int)_flickerShotVector.size();
   iaTmp[0] = iNShot;
   if (iNShot > 0)
   {
      iaTmp[1] = getShotIn(0);
      iaTmp[2] = getShotOut(iNShot - 1);
   }
   else
   {
      iaTmp[1] = 0;
      iaTmp[2] = 0;
   }

   if (fwrite(iaTmp, sizeof(int), 3, fp) != 3)
   {
      fclose(fp);
      return -1;
   }

   for (int iShot = 0; iShot < iNShot; iShot ++)
   {
      // write the number of settings, the in and out
      int iNSetting = getSettingCount(iShot);
      iaTmp[0] = iNSetting;
      iaTmp[1] = getShotIn(iShot);
      iaTmp[2] = getShotOut(iShot);

      if (fwrite(iaTmp, sizeof(int), 3, fp) != 3)
      {
         fclose(fp);
         return -1;
      }

      for (int iSetting = 0; iSetting < iNSetting; iSetting ++)
      {

         getSetting(iShot, iSetting, iaTmp + 0, iaTmp + 1, iaTmp + 2);

         if (fwrite(iaTmp, sizeof(int), 5, fp) != 5)
         {
            fclose(fp);
            return -1;
         }

      }
   }

   fclose(fp);
   return 0;

}
