#ifndef FILE_IO_H
#define FILE_IO_H

#include <string>
using std::string;

#define FIO_NAME_LENGTH 1024
#define FIO_HEADER_LENGTH 1024
#define FIO_DPX_HEADER_SIZE 65536
#define FIO_ALIGN_COUNT 4

#define FIO_PROCESS_FLAG_NONE   0x0
#define FIO_PROCESS_FLAG_PROXY  0x1
#define FIO_PROCESS_FLAG_RENDER 0x2

struct SBox
{
	// the coordinates of the box are inclusive.  Both ULC and LRC are processed
	int iRowULC;
	int iColULC;
	int iRowLRC;
	int iColLRC;
};

class CFileIO
{
public:
  CFileIO();
  ~CFileIO();

  static int WriteProxy (const string &strFilePrototype, int iFrame, int iNRow, int iNCol, unsigned char *ucpRGB, 
                  unsigned char *ucpY, double *dpMean, SBox *bpBox);
  static int ReadProxy (const string &strFilePrototype, int iFrame, int iNRow, int iNCol, unsigned char *ucpRGB,
	              unsigned char *ucpY, double *dpMean, SBox *bpBox);
  static int WriteMotion (const string &strFilePrototype, int iFrameCur, int iFrameRef, int iNRow, int iNCol, 
	  int iRowULC, int iColULC, int iRowLRC, int iColLRC, int iRowOff, int iColOff, int iSatisfiedCount);
  static int ReadMotion (const string &strFilePrototype, int iFrameCur, int *ipFrameRef, int iNRow, int iNCol, 
	  int *ipRowULC, int *ipColULC, int *ipRowLRC, int *ipColLRC, int *ipRowOff, int *ipColOff, int *ipSatisfiedCount);

  static int getCount (const string &strFilePrototype, int iFirst, int iLast);
  static int getFileSize (const string &strFilePrototype, int iFrame);

  int getRowCount ();
  int getColCount ();

  int AllocImageIO (const string &strFilePrototype, int iFrame);
  void FreeImageIO ();
  int ReadImage (const string &strFilePrototype, int iFrame, unsigned short *uspRGB, unsigned char *ucpHeader);
  int WriteImage (const string &strFilePrototype, int iFrame, unsigned short *uspRGB, unsigned char *ucpHeader);
  int CopyHeader (unsigned char *ucpHeaderSrc, unsigned char *ucpHeaderDst);

  static int InitSummary (const string &strFileName, int iFirst, int iLast, SBox *bpBox);
  static int WriteSummary (const string &strFileName, int iFirst, int iLast, float *fpMean[3], unsigned char *ucpProcess);
  static int ReadSummary (const string &strFileName, int iFirst, int iLast, float *fpMean[3], unsigned char *ucpProcess);
  static int WriteSummaryInfo (const string &strFileName, int iMarkFirst, int iMarkLast, SBox *bpBox);
  static int ReadSummaryInfo (const string &strFileName, int *ipMarkFirst, int *ipMarkLast, SBox *bpBox);


private:

  int Unpack (unsigned char *ucpAlign, unsigned short *uspRGB, unsigned char *ucpHeader);
  int Repack (unsigned char *ucpAlign, unsigned short *uspRGB, unsigned char *ucpHeader);

  int ReadBytes (const string &strFilePrototype, int iFrame, unsigned char *ucpAlign);
  int WriteBytes (const string &strFilePrototype, int iFrame, unsigned char *ucpAlign);
  
  bool getByteSwap (unsigned char *header);
  int getHeaderSize (unsigned char *header);
  int getFileSize (unsigned char *header);
  int getRowCount (unsigned char *header);
  int getColCount (unsigned char *header);
  int getImageFormat (unsigned char *header);

  int iImageFileSize;
  unsigned char *ucpImageAlloc[FIO_ALIGN_COUNT];
  unsigned char *ucpImageAlign[FIO_ALIGN_COUNT];
  int iImageAlign;
  int iImageRowCount;
  int iImageColCount;
};

#endif
