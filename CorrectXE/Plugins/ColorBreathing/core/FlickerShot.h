//---------------------------------------------------------------------------

#ifndef FlickerShotH
#define FlickerShotH

#include <string>
using std::string;
#include <vector>
using std::vector;

#include "FlickerShotDefines.h"

// The flicker code implements two distinct algorithms:  
//   1.  one to correct LOW frequency 
//   2.  another to correct HIGH frequency
//

class CFlickerSetting
{
public:
  CFlickerSetting();
  ~CFlickerSetting();

  int iReferenceFrame;
  int iReferenceType;
  int iaMovingAve[3];

  bool AreTheSame(const CFlickerSetting setting)
  {
      return (iReferenceFrame == setting.iReferenceFrame) &&
             (iReferenceType == setting.iReferenceType) &&
             (iaMovingAve[0] == setting.iaMovingAve[0]) &&
             (iaMovingAve[1] == setting.iaMovingAve[1]) &&
             (iaMovingAve[2] == setting.iaMovingAve[2]);
  }
};

class CFlickerShot
{
public:
  CFlickerShot ();
  ~CFlickerShot ();

  void setRange (int iInArg, int OutArg);

  int getIn ();
  int getOut ();

  int getSettingCount ();
  void getSetting (int iSettingIndex, int *ipReferenceFrame, int *ipReferenceType, int *ipMovingAve);
  void getSetting (int iReferenceFrame, int *ipReferenceType, int *ipMovingAve);
  void addSetting (int iReferenceFrame, int iReferenceType, int *ipMovingAve);
  void setSetting (int iReferenceFrame, int iReferenceType, int *ipMovingAve);
  void delSetting (int iReferenceFrame);
  void clearSetting ();
  int getReferenceFrame (int iSettingIndex);

  bool AreSettingsTheSame(CFlickerShot &shot);

  vector <CFlickerSetting> &getSettingList();

private:

  int iIn;      // inclusive IN frame
  int iOut;     // inclusive OUT frame

  vector <CFlickerSetting> listSetting;
};

class CFlickerShotList
{
public:
  CFlickerShotList ();
  ~CFlickerShotList ();

  int getShotCount ();
  int getShotIndex (int iFrame);
  int getShotIn (int iShotIndex);
  int getShotOut (int iShotIndex);
  int getSettingCount (int iShotIndex);
  void getSetting (int iShotIndex, int iSettingIndex, int *ipReferenceFrame, int *ipReferenceType, int *ipMovingAve);
  void getSetting (int iFrame, int *ipReferenceType, int *ipMovingAve);
//  void addSetting (int iReferenceFrame, int iReferenceType, int *ipMovingAve);
  void setSetting (int iReferenceFrame, int iReferenceType, int *ipMovingAve);
//  void delSetting (int iReferenceFrame);
  void clearSetting (int iShotIndex);
  int getReferenceFrame (int iShotIndex, int iSettingIndex);

  void loadShotList (const string &strDirName, int iFirst, int iLast);
  int saveFlickerFile (const string &strDirName);

  bool setShotList(vector<CFlickerShot> &newShotList);
  void clear();
private:
  vector <CFlickerShot> _flickerShotVector;

  void loadShotListFile (const string &strDirName, int iFirst, int iLast);
  int loadFlickerFile (const string &strDirName, int iFirst, int iLast);
  
};

#endif
