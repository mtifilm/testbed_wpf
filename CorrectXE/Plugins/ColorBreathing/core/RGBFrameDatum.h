#ifndef RGBFrameDatumH
#define RGBFrameDatumH

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "PDL.h"
#include "MTImalloc.h"
#include "base64.h"

#define TL_CHAN_COUNT 3
#define TL_QUANTILE_COUNT 10
#define TL_QUANTILE_COUNT2 (TL_QUANTILE_COUNT+2)

const string RGBFrameDatumChild = "RGBDatum";
const string LeftKey = "L";
const string TopKey = "T";
const string RightKey = "R";
const string BottomKey = "B";

const string RedKey = "Red";
const string BlueKey = "Blue";
const string GreenKey = "Green";
const string QuantileKey = "Quantile";
const string ValidKey = "Valid";
const string FrameIndexKey = "FrameIndex";

struct RGBFrameDatum
{
   ShortROI ROI = {-1, -1, -1, -1};
   float Red;
   float Green;
   float Blue;
   short Valid;
   unsigned char Rendered;
   unsigned char WasRendered;

   double Quantile[TL_CHAN_COUNT][TL_QUANTILE_COUNT];

   RGBFrameDatum()
   {
      SetRGBInvalid();
      MTImemset(Quantile, 0, sizeof(Quantile));
   }

   RGBFrameDatum(double mean[3])
   {
      Red = mean[0];
      Green = mean[1];
      Blue = mean[2];
      Valid = true;
   }

   void SetRGBInvalid()
   {
      Red = -1;
      Green = -1;
      Blue = -1;
      Valid = false;
      Rendered = false;
		WasRendered = false;
   }

   bool DataSameAs(const RGBFrameDatum &rgbFrameDatum) const
   {
      if (!this->ROI.Equals(rgbFrameDatum.ROI))
      {
         return false;
      }

      for (auto i = 0; i < TL_CHAN_COUNT; i++)
      for (auto j=0; j < TL_QUANTILE_COUNT; j++)
      {
         if (this->Quantile[i][j] != rgbFrameDatum.Quantile[i][j])
         {
         	return false;
         }
      }

      return (this->Red == rgbFrameDatum.Red) && (this->Green == rgbFrameDatum.Green) &&
         (this->Blue == rgbFrameDatum.Blue);
   }

   bool Equals(const RGBFrameDatum &rgbFrameDatum) const
   {
      if (!this->DataSameAs(rgbFrameDatum))
      {
         return false;
      }

      return (this->Valid == rgbFrameDatum.Valid) && (this->Rendered == rgbFrameDatum.Rendered);
   }

    string QuantileToBase64()
    {
     	return base64_encode((unsigned char const *)Quantile , sizeof(Quantile));
    }

    void Base64ToQuantile(const string &base64String)
    {
       MTImemcpy(Quantile, base64_decode(base64String).c_str(), sizeof(Quantile));
    }

   void AddToPDLEntry(CPDLElement &parent, int frameIndex)
   {
      CPDLElement *pdlTP = parent.MakeNewChild(RGBFrameDatumChild);

      pdlTP->SetAttribInteger(LeftKey, this->ROI.Left);
      pdlTP->SetAttribInteger(RightKey, this->ROI.Right);
      pdlTP->SetAttribInteger(TopKey, this->ROI.Top);
      pdlTP->SetAttribInteger(BottomKey, this->ROI.Bottom);
      pdlTP->SetAttribDouble(RedKey, this->Red);
      pdlTP->SetAttribDouble(GreenKey, this->Green);
      pdlTP->SetAttribDouble(BlueKey, this->Blue);

      auto value = QuantileToBase64();
      pdlTP->SetAttribString(QuantileKey, value);

      pdlTP->SetAttribInteger(FrameIndexKey, frameIndex);
      pdlTP->SetAttribBool(ValidKey, this->Valid);
      }

   int ReadPDLEntry(CPDLElement *pdlTA, int &frameIndex)
   {
      this->ROI.Left = pdlTA->GetAttribInteger(LeftKey, -1);
      this->ROI.Right = pdlTA->GetAttribInteger(RightKey, 1);
      this->ROI.Top = pdlTA->GetAttribInteger(TopKey, 0);
      this->ROI.Bottom = pdlTA->GetAttribInteger(BottomKey, 1);

      this->Red = pdlTA->GetAttribDouble(RedKey, -1);
      this->Green = pdlTA->GetAttribDouble(GreenKey, -1);
      this->Blue = pdlTA->GetAttribDouble(BlueKey, -1);

      auto value = pdlTA->GetAttribString(QuantileKey, "");
      Base64ToQuantile(value);

      frameIndex = pdlTA->GetAttribInteger(FrameIndexKey, -1);

      // Valid should always be true, Rendered false
      this->Valid = pdlTA->GetAttribBool(ValidKey, true);

      // These are not useful in PDL, but perhaps should be
      this->Rendered = false;
      this->WasRendered = false;
      return 0;
   }
};

static_assert(sizeof(RGBFrameDatum) == 3*sizeof(float) +sizeof(int) + sizeof(ShortROI) + sizeof(double) * TL_CHAN_COUNT * TL_QUANTILE_COUNT, "RGBFrameDatum incorrect size");
static_assert(std::is_trivially_copyable<RGBFrameDatum>::value, "RGBFrameDatum must be able to be copied via mem copy");

#endif
