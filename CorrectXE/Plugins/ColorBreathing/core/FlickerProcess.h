#ifndef FlickerProcessH
#define FlickerProcessH

#include <string>
#include "mthread.h"
#include "ThreadLocker.h"
#include "RGBFrameDatum.h"
#include "Ippheaders.h"

using std::string;

class CFlickerProcess;

struct JobData
{
   CFlickerProcess *flickerProcess = nullptr;
   unsigned short *rgb;
   int slices;
   float ratioR;
   float ratioG;
   float ratioB;
   CCountingWait countingWait;
   float jobNumber;
   int width;
   int height;
   void *fpX;
   void *fpY;
   int *ipCount;
};

class CFlickerProcess
{
public:
  CFlickerProcess();
  ~CFlickerProcess();

  void setImageSize (int nRow, int nCol, int dataMax);

  int RenderSliceV4(unsigned short *rgb, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT], int rowIn, int rowOut);

  int RenderDisplaySliceV4(unsigned char *rgb, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT], int width, int rowIn, int rowOut);

  void JobThreadCallV4(JobData *jobData, int iJob);
  void DisplayJobThreadCallV4(JobData *jobData, int iJob);

  // These are different ways of rendering the same data to check on speed etc.
  int RenderV4(unsigned short *rgb, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT], const RECT &roi);

  int RenderDisplayV4(unsigned char *rgba, int width, int height, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
							 int iaCount[TL_CHAN_COUNT]);

  int getHeight() { return _height; }
  int getWidth() { return _width; }
  int getDataMax() { return _dataMax; }
private:
  int _height;
  int _width;
  int _dataMax;
  RECT _processingRoi;
  JobData _jobData;
};

#endif
