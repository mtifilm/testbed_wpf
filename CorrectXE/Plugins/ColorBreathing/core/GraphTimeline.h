// ---------------------------------------------------------------------------

#ifndef GraphTimelineH
#define GraphTimelineH
#include <string>
#include "RGBFrameDatum.h"

using std::string;

#include "TimelineDefines.h"
#include "FlickerShot.h"
#include "ippi.h"

#define TL_MOVING_AVERAGE_MAX_RADIUS 30

#define TL_PIXEL_PER_FRAME_DEFAULT 5
#define TL_PIXEL_PER_FRAME_MAX     20
#define TL_BORDER_PIX 1
#define TL_PROCESS_PIX 3

#define TL_COLOR_BORDER_R 75
#define TL_COLOR_BORDER_G 75
#define TL_COLOR_BORDER_B 75

#define TL_COLOR_DISPLAY_BG_R 82
#define TL_COLOR_DISPLAY_BG_G 82
#define TL_COLOR_DISPLAY_BG_B 82

#define TL_COLOR_DISPLAY_FG_R 116
#define TL_COLOR_DISPLAY_FG_G 116
#define TL_COLOR_DISPLAY_FG_B 116

#define TL_COLOR_CURSOR_R 255
#define TL_COLOR_CURSOR_G 0
#define TL_COLOR_CURSOR_B 0

#define TL_COLOR_PROCESSED_CURSOR_R 0
#define TL_COLOR_PROCESSED_CURSOR_G 255
#define TL_COLOR_PROCESSED_CURSOR_B 0

#define TL_COLOR_MARK_R 150
#define TL_COLOR_MARK_G 150
#define TL_COLOR_MARK_B 150

#define TL_COLOR_REFERENCE_R 150
#define TL_COLOR_REFERENCE_G 150
#define TL_COLOR_REFERENCE_B 150

#define TL_COLOR_REFERENCE_SELECT_R 175
#define TL_COLOR_REFERENCE_SELECT_G 175
#define TL_COLOR_REFERENCE_SELECT_B 175

#define TL_COLOR_SHOT_R 0
#define TL_COLOR_SHOT_G 0
#define TL_COLOR_SHOT_B 0

#define TL_COLOR_RED_R 255
#define TL_COLOR_RED_G 0
#define TL_COLOR_RED_B 0
#define TL_COLOR_GREEN_R 100
#define TL_COLOR_GREEN_G 255
#define TL_COLOR_GREEN_B 100
#define TL_COLOR_BLUE_R 0
#define TL_COLOR_BLUE_G 0
#define TL_COLOR_BLUE_B 255
#define TL_COLOR_BLACK_R 0
#define TL_COLOR_BLACK_G 0
#define TL_COLOR_BLACK_B 0

#define TL_COLOR_DISPLAY_PROXY_R 200
#define TL_COLOR_DISPLAY_PROXY_G 200
#define TL_COLOR_DISPLAY_PROXY_B 0

#define TL_COLOR_DISPLAY_RENDER_R 0
#define TL_COLOR_DISPLAY_RENDER_G 0
#define TL_COLOR_DISPLAY_RENDER_B 200

#define TL_COLOR_DISPLAY_BAD_RENDER_R 200
#define TL_COLOR_DISPLAY_BAD_RENDER_G 0
#define TL_COLOR_DISPLAY_BAD_RENDER_B 0

#define TL_COLOR_DISPLAY_WHITE_R 255
#define TL_COLOR_DISPLAY_WHITE_G 255
#define TL_COLOR_DISPLAY_WHITE_B 255

struct STimelineValues
{
   int iFirst; // the first frame
   int iLast; // the last frame
   int iWidth; // width of the bitmap
   int iHeight; // height of the bitmap

   // An equation to convert frame to pixel location:
   //
   // Pixel = PixelPerFrame * Frame + FirstPixel
   float fPixelPerFrame; // number of pixels corresponding to a single frame
   float fFirstPixel; // the pixel corresponding to iFirst
};

//#define TL_LIST_RGB_ORIG    0
//#define TL_LIST_RGB_PROC    1
//#define TL_LIST_COUNT       2

#define HIST_COUNT 1024


struct SMeanData
{
   // there are 2 lists of values:  RGB-original, RGB-corrected
   // These should be in own struct
   float *fpValOrig[TL_CHAN_COUNT];
   float *fpValProc[TL_CHAN_COUNT];

   float faMinOrig[TL_CHAN_COUNT];
   float faMaxOrig[TL_CHAN_COUNT];

   float faMinProc[TL_CHAN_COUNT];
   float faMaxProc[TL_CHAN_COUNT];

   // keep a quantile record for each R,G,B channel
   float *fpQuantile[TL_CHAN_COUNT][TL_QUANTILE_COUNT];

   // keep the repair value for each R,G,B channel
   float *fpRepair[TL_CHAN_COUNT][TL_QUANTILE_COUNT];
};

class CGraphTimeline
{
public:
   CGraphTimeline();
   ~CGraphTimeline();

 ////  void Clear();

   void setGraphDimension(int iWidth, int iHeight);

   int setFrames(int frames);
   void setMasterPixelxxx(int iPixel);
   void setGraphPixel(int iPixel);
   void setMarkRange(int iFirst, int iLast);

   void getSurroundingRange(int iFrameArg, int *ipIn, int *ipOut);
   int getShotIndex(int iFrameArg);

   void updateGraph(unsigned char *ucp);

   int getReferenceCount(int iShotIndex);

   void setFlickerChannel(int iArg);
   void SetAllRgbData(const vector<RGBFrameDatum> &rgbData);

   void MarkRendered(int iFrame);
   bool IsRendered(int iFrame);

   void SaveMeanData(const char *cpSummaryFileName);
   void SaveRenderFlag(const char *cpSummaryFileName);

   void setGraphType(int iType);
   void getDelta(int iFrameArg, float *fpDeltaR, float *fpDeltaG, float *fpDeltaB);
   void getRGB(int iFrameArg, float *fpR, float *fpG, float *fpB);

   void getQuantileCorrection (int iFrame, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
									   int iaCount[]);

   void CorrectMeanData();
   void CorrectQuantileData (int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast);

   void setShotList(vector<CFlickerShot> &newShotList);
   void setDisplayRange(int iDisplayFirst, int iDisplayLast);

   void setCursorPosition(int position);
   int getCursorPosition();

   int GetDisplayIn() const ;
   int GetDisplayOut() const ;

   // Logic is simple.  Graph data dirty means graph must be redrawn
   // for any reason. Is CursorPixelPosition dirty means cursor must be
   // redrawn.  SetGraphDirty does both
   bool IsGraphDataDirty() const ;
   bool IsGraphDirty() const ;
   bool IsCursorPixelPositionDirty() const ;

   void SetGraphDirty();
   void SetGraphRenderDirty();

   void ShowCursorActive(bool processed);

   void SetGraphCentered(bool centered);
   bool IsGraphCentered() const;

   void SetMinScale(double minScale);

   // Processing to make quantile data
   void SetQuantileMaximum(unsigned int dataMaximum);
   int MakeQuantiles(const Ipp16u* source, int stepInBytes, IppiSize roiSize, RGBFrameDatum &rgbFrameDatum);

   void DumpRGBData();
   void LoadRGBData();

private:
   STimelineValues tvMaster;
   STimelineValues tvGraph;

   float fPixelPerFrame;

   int iCurrentFrame; // the current frame
   int _totalFrames = 0; // the last frame in the sequence

   int iFlickerChannel = TL_FLICKER_CHANNELS_RGB;;

   // There is no need for two different SMeanData anymore because the Quantiles
   // Also, computation is quite fast enough even for many frames
   SMeanData mdProcess;

   unsigned char *ucpProcessFlag;

   void InitializeMeanData();
   void FreeMeanData();

   void SetFrameRgbData(int frame, const RGBFrameDatum &rgbDatum);
   void displayBackground(unsigned char *ucp, STimelineValues *tvp, int iFirstPixel, int iLastPixel);
   void displayCursor(unsigned char *ucp, STimelineValues *tvp);
   void displayMarks(unsigned char *ucp, STimelineValues *tvp);
   void displayReference(unsigned char *ucp, STimelineValues *tvp);
   void displayShot(unsigned char *ucp, STimelineValues *tvp);
   void displayValues(unsigned char *ucp, STimelineValues *tvp, int iList, int iChan);
   void displayBottomLine(unsigned char *ucpArg, STimelineValues *tvp, int frame, int c0, int c1);
   void displayRangeValue(unsigned char *ucpArg, STimelineValues *tvp, float fMax, float fMin);

   void updateGraphCursorOnly(unsigned char *ucpArg);
   void updateGraphData(unsigned char *ucpArg);

   void FindScaling(int iFirst, int iLast);
   void ConvertToCrYCb(float *fpSrc, float *fpDst);
   void ConvertFromCrYCb(float *fpSrc, float *fpDst);
   void MovingAverageRGB(int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast);

   void MovingAverage (int iChan, int iRefFirst, int iRefLast, int iMovingAveFirst,
       int iMovingAveLast, float *fp);

   void ApplyReference(int iFirstArg, int iLastArg);
   void getNew(int iRefType, float fOldR, float fOldG, float fOldB, float fRefR, float fRefG, float fRefB,
      float *fpNewR, float *fpNewG, float *fpNewB);

   void updateBottomLine(unsigned char *ucpArg, STimelineValues *tvp);

   void ApplyReferenceQuantile (int iFirstArg, int iLastArg);
   void MovingAverageQuantile (int iRefFirst, int iRefLast, int iMovingAveFirst, int iMovingAveLast);

   int FrameToGraphPixel(int frame);

   bool bAllocMeanData;

   int iMarkFirst = 0;
   int iMarkLast = 0;
   int _displayFirst = 0;
   int _displayLast = 0;
   bool _centered = false;

   int iGraphType;

   CFlickerShotList _flickerShotList;
   bool bShotListChanged; // indicates if user has has changed the shot list entries

   bool _isGraphDataDirty = true;
   bool _isCursorPixelPositionDirty = true;
   bool _isRenderDirty = true;

   int _graphSizeinBytes = 0;
   bool _cursorColorProcessed = false;
   unsigned char *_cleanDataGraph = nullptr;

   int _minScale = 4;

   // scratch space for computing moving average
   float *_fpMovingAveScratch = nullptr;

   // Quintile Data
   double _histogramDataMaximum;
   int _histogram[TL_CHAN_COUNT][HIST_COUNT];
   int _histogramSourceCount;
   int _histogramLevels[TL_CHAN_COUNT][HIST_COUNT+1];
   int _histogramLevelsCount[TL_CHAN_COUNT];
   int MakeQuantile (int iChannel, double daQuant[TL_QUANTILE_COUNT]);
};

// ---------------------------------------------------------------------------
#endif
