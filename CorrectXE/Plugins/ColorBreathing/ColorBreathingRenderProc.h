//---------------------------------------------------------------------------

#ifndef ColorBreathingRenderProcH
#define ColorBreathingRenderProcH

#include "ColorBreathingTool.h"
#include "ColorBreathingToolParameters.h"
#include "RGBFrameData.h"
#include "ToolObject.h"
#include "FlickerProcess.h"

class ColorBreathingRenderProc : public CToolProcessor
{
public:
   ColorBreathingRenderProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor);

   virtual ~ColorBreathingRenderProc();

   CHRTimer HRT;

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);
   int Render (unsigned short *rgb, float deltaR, float deltaG, float deltaB);

   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;
   int _processingThreadCount;
   CFlickerProcess *_flickerProcess = nullptr;

   ColorBreathingTool *_tool;
   ColorBreathingModel *_model;

};
//---------------------------------------------------------------------------
#endif
