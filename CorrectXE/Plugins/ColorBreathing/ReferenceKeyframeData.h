// ---------------------------------------------------------------------------

#ifndef ReferenceKeyframeDataH
#define ReferenceKeyframeDataH

#include "IniFile.h"
#include "IPresetParameters.h"
#include "PDL.h"

// Note, these are divided into to parts, the PresetParameters and other
// FrameNumber is the only other for now.
class ReferenceKeyframeData : public IPresetParameters
{
public:
    ReferenceKeyframeData();
    ReferenceKeyframeData(const ReferenceKeyframeData &r);

    bool operator == (const ReferenceKeyframeData &g) const ;
    bool operator != (const ReferenceKeyframeData &g) const ;
    ReferenceKeyframeData& operator = (const ReferenceKeyframeData & r);

    virtual void ReadParameters(CIniFile *ini, const string &iniSectionName);
    virtual void WriteParameters(CIniFile *ini, const string &iniSectionName) const ;
    virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const ;

    void AddToPDLEntry(CPDLElement &parent);
    int ReadPDLEntry(CPDLElement *pdlTA);

    // May need to change to getters and setters
    bool RedEnabled;
    bool BlueEnabled;
    bool GreenEnabled;

    bool ColorForwards;
    bool ColorBackwards;

    bool FlickerForwards;
    bool FlickerBackwards;
    int RedSmoothing;
    int GreenSmoothing;
    int BlueSmoothing;
    int FrameIndex;
};

// ---------------------------------------------------------------------------
#endif
