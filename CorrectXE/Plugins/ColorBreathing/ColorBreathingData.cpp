//---------------------------------------------------------------------------

#pragma hdrstop

#include "ColorBreathingData.h"
#include "GraphTimeline.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

ColorBreathingData::ColorBreathingData()
{
   this->RedEnabled = true;
   this->GreenEnabled = true;
   this->BlueEnabled = true;
}

int ColorBreathingData::GetGraphType()
{
   // Ugly;
   auto type = RedEnabled ? TL_GRAPH_TYPE_R : 0;
   type |= GreenEnabled ? TL_GRAPH_TYPE_G : 0;
   type |= BlueEnabled ? TL_GRAPH_TYPE_B : 0;

   return type;
}

