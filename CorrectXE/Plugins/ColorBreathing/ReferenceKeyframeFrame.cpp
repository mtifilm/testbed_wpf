// ---------------------------------------------------------------------------

#include <vcl.h>
#include "guid_mti.h"

#pragma hdrstop

#include "ReferenceKeyframeFrame.h"
#define SmoothingMinValue 0
#define SmoothingMaxValue 20

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "SpinEditFrameUnit"
#pragma link "SpinEditFrameUnit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"

extern TReferenceKeyframeFrame *referenceKeyframeFrame;

// NOTE: The framedata and the GUI are kept in lock step
// The GetData and SetData commands copies this data out

// ---------------------------------------------------------------------------
__fastcall TReferenceKeyframeFrame::TReferenceKeyframeFrame(TComponent* Owner) : TFrame(Owner)
{
   // This is not really ever used execpt for the designer.
   _referenceKeyframeData = new ReferenceKeyframeData();
   this->ConstructorInit();
}

__fastcall TReferenceKeyframeFrame::TReferenceKeyframeFrame(TComponent* Owner,
   ReferenceKeyframeData *referenceKeyframeData, const CTimecode &inTimecode) : TReferenceKeyframeFrame(Owner)
{
   delete _referenceKeyframeData;

   this->_referenceKeyframeData = referenceKeyframeData;
   this->_inTimecode = inTimecode;

   auto guidName = guid_to_string(guid_create());
   String name = "N" + (String)(guidName.c_str());

   this->Name = StringReplace(name, "-", "", TReplaceFlags() << rfReplaceAll);

   this->ConstructorInit();
}

void TReferenceKeyframeFrame::ConstructorInit()
{
   this->RedColorSpinEdit->SetRange(SmoothingMinValue, SmoothingMaxValue);
   this->GreenColorSpinEdit->SetRange(SmoothingMinValue, SmoothingMaxValue);
   this->BlueColorSpinEdit->SetRange(SmoothingMinValue, SmoothingMaxValue);
   this->DeleteButton->Tag = (NativeInt)this;
   this->UpdateGUI();

   SET_CBHOOK(RedSpinEditChange, this->RedColorSpinEdit->SpinEditValueChange);
   SET_CBHOOK(GreenSpinEditChange, this->GreenColorSpinEdit->SpinEditValueChange);
   SET_CBHOOK(BlueSpinEditChange, this->BlueColorSpinEdit->SpinEditValueChange);
}

// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::RedButtonClick(TObject *Sender)
{
   this->_referenceKeyframeData->RedEnabled = !this->_referenceKeyframeData->RedEnabled;
   this->UpdateGUI();
}

// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::GreenButtonClick(TObject *Sender)
{
   this->_referenceKeyframeData->GreenEnabled = !this->_referenceKeyframeData->GreenEnabled;
   this->UpdateGUI();
}

// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::BlueButtonClick(TObject *Sender)
{
   this->_referenceKeyframeData->BlueEnabled = !this->_referenceKeyframeData->BlueEnabled;
   this->UpdateGUI();
}

// ---------------------------------------------------------------------------

void TReferenceKeyframeFrame::UpdateGUI()
{
   // if (this->ReadFromGui() != this->_referenceKeyframeData)
   // {
   // return;
   // }

   // Enable it
   this->RedButton->Down = this->_referenceKeyframeData->RedEnabled;
   this->GreenButton->Down = this->_referenceKeyframeData->GreenEnabled;
   this->BlueButton->Down = this->_referenceKeyframeData->BlueEnabled;

   this->RedColorSpinEdit->Value = this->_referenceKeyframeData->RedSmoothing;
   this->GreenColorSpinEdit->Value = this->_referenceKeyframeData->GreenSmoothing;
   this->BlueColorSpinEdit->Value = this->_referenceKeyframeData->BlueSmoothing;

   this->ColorForwardsCheckBox->Checked = this->_referenceKeyframeData->ColorForwards;
   this->ColorBackwardsCheckBox->Checked = this->_referenceKeyframeData->ColorBackwards;
   this->SmoothingForwardsCheckBox->Checked = this->_referenceKeyframeData->FlickerForwards;
   this->SmoothingBackwardsCheckBox->Checked = this->_referenceKeyframeData->FlickerBackwards;

   // Should this be a property?
   string timecodeString;
   (_inTimecode +this->_referenceKeyframeData->FrameIndex).getTimeString(timecodeString);
   this->FrameLabel->Caption = StringTrimLeft(timecodeString).c_str();

   // We should REALLY see if the data has changed
   DataHasChanged.Notify();
}

const ReferenceKeyframeData TReferenceKeyframeFrame::ReadFromGui() const
{
   ReferenceKeyframeData referenceKeyFrameData;
   referenceKeyFrameData.RedEnabled = this->RedButton->Down;
   referenceKeyFrameData.GreenEnabled = this->GreenButton->Down;
   referenceKeyFrameData.BlueEnabled = this->BlueButton->Down;

   referenceKeyFrameData.RedSmoothing = this->RedColorSpinEdit->Value;
   referenceKeyFrameData.GreenSmoothing = this->GreenColorSpinEdit->Value;
   referenceKeyFrameData.BlueSmoothing = this->BlueColorSpinEdit->Value;

   referenceKeyFrameData.ColorForwards = this->ColorForwardsCheckBox->Checked;
   referenceKeyFrameData.ColorBackwards = this->ColorBackwardsCheckBox->Checked;
   referenceKeyFrameData.FlickerForwards = this->SmoothingForwardsCheckBox->Checked;
   referenceKeyFrameData.FlickerBackwards = this->SmoothingBackwardsCheckBox->Checked;

   return referenceKeyFrameData;
}

void TReferenceKeyframeFrame::RedSpinEditChange(void *Sender)
{
   if (this->_referenceKeyframeData->RedSmoothing == this->RedColorSpinEdit->Value)
   {
      return;
   }

   this->_referenceKeyframeData->RedSmoothing = this->RedColorSpinEdit->Value;
   DataHasChanged.Notify();
}

void TReferenceKeyframeFrame::GreenSpinEditChange(void *Sender)
{
   if (this->_referenceKeyframeData->GreenSmoothing == this->GreenColorSpinEdit->Value)
   {
      return;
   }

   this->_referenceKeyframeData->GreenSmoothing = this->GreenColorSpinEdit->Value;
   DataHasChanged.Notify();
}

void TReferenceKeyframeFrame::BlueSpinEditChange(void *Sender)
{
   if (this->_referenceKeyframeData->BlueSmoothing == this->BlueColorSpinEdit->Value)
   {
      return;
   }

   this->_referenceKeyframeData->BlueSmoothing = this->BlueColorSpinEdit->Value;
   DataHasChanged.Notify();
}

// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::ColorBackwardsCheckBoxClick(TObject *Sender)
{
   this->_referenceKeyframeData->ColorBackwards = this->ColorBackwardsCheckBox->Checked;
   DataHasChanged.Notify();
}

// ---------------------------------------------------------------------------

const ReferenceKeyframeData TReferenceKeyframeFrame::GetReferenceKeyFrameData() const
{
   return *_referenceKeyframeData;
}

ReferenceKeyframeData *TReferenceKeyframeFrame::GetReferenceKeyFramePointer() const
{
   return _referenceKeyframeData;
}

// ---------------------------------------------------------------------------

void TReferenceKeyframeFrame::CopyToReferenceKeyFrameData(const ReferenceKeyframeData &referenceKeyframeData)
{
   *_referenceKeyframeData = referenceKeyframeData;
   UpdateGUI();
}

void TReferenceKeyframeFrame::SetReferenceKeyFrameData(ReferenceKeyframeData *referenceKeyframeData)
{
   _referenceKeyframeData = referenceKeyframeData;
   UpdateGUI();
}

void __fastcall TReferenceKeyframeFrame::ColorForwardsCheckBoxClick(TObject *Sender)
{
   this->_referenceKeyframeData->ColorForwards = this->ColorForwardsCheckBox->Checked;
   DataHasChanged.Notify();
}
// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::SmoothingBackwardsCheckBoxClick(TObject *Sender)
{
   this->_referenceKeyframeData->FlickerBackwards = this->SmoothingBackwardsCheckBox->Checked;
   DataHasChanged.Notify();
}
// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::SmoothingForwardsCheckBoxClick(TObject *Sender)
{
   this->_referenceKeyframeData->FlickerForwards = this->SmoothingForwardsCheckBox->Checked;
   DataHasChanged.Notify();
}
// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::FramePanelClick(TObject *Sender)
{
   FrameClick.SetAllData(this->GetReferenceKeyFramePointer());
   FrameClick.Notify();
}
// ---------------------------------------------------------------------------

void DisableControls(TPanel *panel, bool enabled)
{
   if (panel == nullptr)
   {
      return;
   }

   for (auto i = 0; i < panel->ControlCount; i ++)
   {
      panel->Controls[i]->Enabled = enabled;
      DisableControls(dynamic_cast<TPanel*>(panel->Controls[i]), enabled);
   }
}

void TReferenceKeyframeFrame::SetMasterEnabled(bool enabled)
{
   DisableControls(BasePanel, enabled);
}

bool TReferenceKeyframeFrame::IsSelected() const
{
   return _isSelected;
}

void TReferenceKeyframeFrame::SetIsSelected(const bool selected)
{
   if (selected != _isSelected)
   {
      _isSelected = selected;
      Color = _isSelected ? TColor(0x00808080) : TColor(0x006A6A6A);
      BasePanel->Color = Color;
      FramePanel->Color = Color;
   }
}

void __fastcall TReferenceKeyframeFrame::ColorSpinEditUpButtonClick(TObject *Sender)
{
   auto speedButton = dynamic_cast<TSpeedButton *>(Sender);
   if (speedButton == nullptr)
   {
      return;
   }

   auto panel1 = dynamic_cast<TPanel *>(speedButton->Parent);
   if (panel1 == nullptr)
   {
      return;
   }

   auto panel2 = dynamic_cast<TPanel *>(panel1->Parent);
   if (panel2 == nullptr)
   {
      return;
   }

   auto spinEdit = dynamic_cast<TSpinEditFrame *>(panel2->Parent);
   if (spinEdit == nullptr)
   {
      return;
   }

   bool control = GetKeyState(VK_CONTROL) & 0x8000;
   if (control)
   {
      auto n = spinEdit->Value;
      auto r = RedColorSpinEdit->Value;
      auto g = GreenColorSpinEdit->Value;
      auto b = BlueColorSpinEdit->Value;

      if ((r == g) && (g == b))
      {
         n++;
      }

      RedColorSpinEdit->Value = n;
      GreenColorSpinEdit->Value = n;
      BlueColorSpinEdit->Value = n;
   }
   else
   {
      spinEdit->UpButtonClick(Sender);
   }
}
// ---------------------------------------------------------------------------

void __fastcall TReferenceKeyframeFrame::ColorSpinEditDownButtonClick(TObject *Sender)
{
   auto speedButton = dynamic_cast<TSpeedButton *>(Sender);
   if (speedButton == nullptr)
   {
      return;
   }

   auto panel1 = dynamic_cast<TPanel *>(speedButton->Parent);
   if (panel1 == nullptr)
   {
      return;
   }

   auto panel2 = dynamic_cast<TPanel *>(panel1->Parent);
   if (panel2 == nullptr)
   {
      return;
   }

   auto spinEdit = dynamic_cast<TSpinEditFrame *>(panel2->Parent);
   if (spinEdit == nullptr)
   {
      return;
   }

   bool control = GetKeyState(VK_CONTROL) & 0x8000;
   if (control)
   {
      auto n = spinEdit->Value;
      auto r = RedColorSpinEdit->Value;
      auto g = GreenColorSpinEdit->Value;
      auto b = BlueColorSpinEdit->Value;

      if ((r == g) && (g == b))
      {
         n--;
      }

      RedColorSpinEdit->Value = n;
      GreenColorSpinEdit->Value = n;
      BlueColorSpinEdit->Value = n;
   }
   else
   {
      spinEdit->DownButtonClick(Sender);
   }
}
// ---------------------------------------------------------------------------
