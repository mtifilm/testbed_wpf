// ColorBreathing.cpp
//
//  Created: Aug 12, 2014 by John Mertus
//
//  Interface that loads the ColorBreathing Plugin Tool.
//
/*
*/
//////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------


#include "machine.h"
#include "CPlugin.h"
#include "ColorBreathingTool.h"
#include "MTIstringstream.h"

//---------------------------Globals---------------------------------
//
//  Note:  The interface defines two ways of using the ToolNameTable
//  first is the number of tools PIN_RES_TOOL_NUMBER
//  the second is the ToolNameTable ENDS with a blank line
//

const char *ToolNameTable[] = {COLORBREATHING_TOOL_NAME , ""};
#define NAME_TABLE_SIZE ((sizeof(ToolNameTable)/sizeof(char*))-1)
const char *PluginName = COLORBREATHING_TOOL_NAME;

// Encryption for "CORRECT-COLORBREATHING"
MTI_UINT32 FEATURE_CORRECT_COLORBREATHING[] =
{0xcfc04a2d, 0xc5e5e9f4, 0xf4e3e5f2, 0x8be5e9ea, 0xe9f4e4f4, 0xe3e7f2ee, 0xefe8e1de, 0x27386d2f,
	0x7bdb2d7b, 0xcbb5f538, 0x65586854, 0xdb29ba51, 0xf16ba14b, 0xdd54b5ef, 0x52d24d2d, 0x4ace4acc};

MTI_UINT32 *ColorBreathingFeatureTable[] = {FEATURE_CORRECT_COLORBREATHING, 0};

ColorBreathingTool *GColorBreathingTool = nullptr;           // only allow one tool
extern "C" void ColorBreathingShutdown(void);   // Code when DLL or so terminates


//**************************WINDOWS CODE*****************************
#ifdef WIN32
#include <windows.h>
#include <vcl.h>
#include "DllSupport.h"
#define EXPORT __declspec(dllexport)

#pragma hdrstop

//
// Windows reads the version from the DLL itself
// So do all the necessary stuff here
MTI_INT32 Version[] = {-1, -1, -1, -1};
char *Copyright = nullptr;
char *TradeMark = nullptr;
char *Company = nullptr;

//
//  Will contain the global instance of the DLL
HINSTANCE hGInst;
//
//  This needs only be used if the DLL does not share the Application
// pointer.  Compiled under dynamic RTL and build using libraries
// the application pointers will be the same.
TApplication *GApp = NULL;         // Parent of DLL

//------------------ReadVersion-------------John Mertus--June 2001------

    void ReadVersion(void)

//  This returns the information about the plugin
//  The return is in the global Version. for example, "1.2.45.12"
//     V[0] - Major version number
//     V[1] - Minor version number
//     V[2] - Release number
//     V[3] - Build number
//
//***********************************************************************
{
    if (Copyright == NULL)
    {
       char PluginName[MAX_PATH];
	   GetModuleFileName(hGInst, PluginName, MAX_PATH);
       string Result = GetDLLVersion(PluginName);
       sscanf(Result.c_str(),"%d.%d.%d.%d",&Version[0],&Version[1],&Version[2],&Version[3]);

       string S = GetDLLInfo(PluginName, "LegalCopyright");
	   Copyright = MTIstrdup(S.c_str());

       S = GetDLLInfo(PluginName, "LegalTradeMarks");
       TradeMark = MTIstrdup(S.c_str());

       S = GetDLLInfo(PluginName, "CompanyName");
       Company = MTIstrdup(S.c_str());
    }
}

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{

  switch (reason)
    {
    case DLL_PROCESS_ATTACH:              // On Attach thread or process
    case DLL_THREAD_ATTACH:
      GApp = Application;
      hGInst = hinst;                     // save the instance for version and resources
      ReadVersion();
      break;

    case DLL_THREAD_DETACH:               // On release
      break;

    case DLL_PROCESS_DETACH:
      ColorBreathingShutdown();                     // Common destroy code
      if (GApp != NULL) Application = GApp;
      MTIfree(Copyright);
      Copyright = NULL;
      MTIfree(Company);
      Company = NULL;
      MTIfree(TradeMark);
      TradeMark = NULL;
      break;
    }

  return TRUE;
}

//------------------SetParent-----------------John Mertus--July 2001------

   int  SetParent(void *Msg)

// For windows, Msg is a pointer to the Application
// Only needed if Application pointer is not shared between DLL and main
//
//***********************************************************************
{
   if (Msg == NULL)
      return 0;   // do nothing if caller's argument in NULL

   if (GApp == NULL)
      GApp = Application;

   Application = (TApplication *)Msg;

   return 0;
}

#else
//*****************************UNIX/LINUX CODE*******************************

#define EXPORT    // No export in so's

MTI_INT32 Version[] = {0,9,0,1};
char Copyright[] = "Copyright 2002 by Mathematical Technologies Inc.";
char TradeMark[] = "IntelliDeck";
char Company[] = "Mathematical Technologies";

//------------------SetParent-----------------John Mertus--July 2001------

   int  SetParent(void *Msg)

// For Unix, Msg should be a parent widget
//
//***********************************************************************
{
   return 0;
}


#endif

//**************************SYSTEM INDEPENDENT CODE****************************

//---------------------------------------------------------------------------
//
//  Plugin Functions
//    Note:  All function are exported with "C"
//    Borland will append an _ before the function
//
//---------------------------------------------------------------------------

extern "C" EXPORT void *Create(string Name);
extern "C" EXPORT bool MTI_PluginVersion(int *V);
extern "C" EXPORT void *Resource(int n);
extern "C" EXPORT int Properties(void);
extern "C" EXPORT bool WriteProperties(string FName, string Section);
extern "C" EXPORT bool ReadProperties(string FName, string Section);
extern "C" EXPORT int SendTheMessage(int nMsg, void *Msg);
extern "C" EXPORT PluginStateStruct CurrentState(void);

//------------------MTI_PluginVersion-------------John Mertus--June 2001------

	bool MTI_PluginVersion(int *V)

//  This returns the information about the plugin
//  The return is the version string; for example, "1.2.45.12"
//  V is an array or null, if an array the return is
//     V[0] - Major version number
//     V[1] - Minor version number
//     V[2] - Release number
//     V[3] - Build number
//
// IT IS THE EXISTANCE OF FUNCTION THAT DETERMINES IF AN SO OR DLL
// IS A MTI PLUGIN
//
//***********************************************************************
{
   // See if parsing is wanted
   if (V != NULL)
	  {
	  V[0] = Version[0];
	  V[1] = Version[1];
	  V[2] = Version[2];
	  V[3] = Version[3];
	  }

   return true;
}

//------------------Create-------------

    void *Create(string Name)

//  The should return a specific version of a base class that is
//  associated with name.
//
//  A pointer to the class is returned if there is such a base class
//  a NULL otherwise.
//
//***********************************************************************
{
		if (Name == (string)ToolNameTable[0])
		{
			if (GColorBreathingTool != NULL)
			{
				return NULL;
			}

			GColorBreathingTool = new ColorBreathingTool(Name, ColorBreathingFeatureTable);

			return (void *)GColorBreathingTool;
		}

		return NULL;
}

//----------------Properties-------------------------John Mertus-----Jan 2001---

    int Properties(void)

//   This activates a form to change the properties of the plugin
//
//******************************************************************************
{
#ifdef _DEBUG
//  int Result = MessageBox(NULL, "Properties", "Properties form", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return true;
};

//----------------WriteProperties-----------------John Mertus-----Jan 2001---

    bool WriteProperties(string IniName, string Section)

//   This writes all the necessary properties to the ini file FName
//   under section Section
//
//******************************************************************************
{
#ifdef _DEBUG
//  MessageBox(NULL, (IniName + "\n" + Section + "\n" + PluginName).c_str(), "Writing properties", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return true;
};

//----------------CurrentState-----------------John Mertus-----Jan 2001---

    PluginStateStruct CurrentState(void)

//   This returns the current state of the tool.  State is determined by
// the licensing file
//
//******************************************************************************
{
   CPluginStateWrapper cps;

   cps.State(PLUGIN_STATE_ENABLED);
   cps.strReason("Licensed");

   return cps.getWrappedPluginState();
}


//----------------ReadProperties-----------------John Mertus-----Jan 2001---

    bool ReadProperties(string IniName, string Section)

//   This Reads all the necessary properties to the ini file FName
//   under section Section
//
//******************************************************************************
{
#ifdef _DEBUG
//  MessageBox(NULL, (IniName + "\n" + Section + "\n" + PluginName).c_str(), "Reading properties", MB_ICONINFORMATION | MB_OKCANCEL);
#endif
  return true;
};

//----------------Resource-----------------John Mertus-----Jan 2001---

    void *Resource(int N)

//  Non classe data is reported via the resource section
//  N is the index into the resource, will be abstracted later
//
//******************************************************************************
{
  void *Result;

  switch (N)
   {
      case PIN_RES_TOOL_NAMES:
         Result = (void *)ToolNameTable;
         break;

      case PIN_RES_TOOL_NUMBER:
         Result = (void *)NAME_TABLE_SIZE;
         break;

      case PIN_RES_PLUGIN_NAME:
         Result = (void *)PluginName;
         break;

      case PIN_RES_TRADEMARK:
         Result = (void *)TradeMark;
         break;

      case PIN_RES_COPYRIGHT:
         Result = (void *)Copyright;
         break;

      case PIN_RES_COMPANY:
         Result = (void *)Company;
         break;

      case PIN_RES_FEATURE:
         Result = (void *)ColorBreathingFeatureTable;
         break;

      default:
         Result = NULL;
   }

  return(Result);
};

//-------------SendTheMessage-----------------John Mertus--July 2001------

   int SendTheMessage(int nMsg, void *Msg)

// This processes messages, some are specific to the compiler and
// operating system
//
//***********************************************************************
{
   int Result;

   switch (nMsg)
	{
	  case PIN_MSG_SET_PARENT:
		Result = SetParent(Msg);
		break;

	  default:
		Result = 1;                    // Message not Processed

	}

   return(Result);
}


//------------------ColorBreathingShutdown-----------------------------------

  void ColorBreathingShutdown(void)

//  This is called whenever the SO is unloaded
//
//****************************************************************************
{
  delete GColorBreathingTool;
  GColorBreathingTool = NULL;
}
