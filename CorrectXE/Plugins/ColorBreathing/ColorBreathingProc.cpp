// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ColorBreathingProc.h"
#include "ColorBreathingModel.h"
#include "ToolProgressMonitor.h"
#include "ClipAPI.h"
#include "Ippheaders.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

ColorBreathingProc::ColorBreathingProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor)
   : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   StartProcessThread(); // Required by Tool Infrastructure
}

///////////////////////////////////////////////////////////////////////////////

ColorBreathingProc::~ColorBreathingProc()
{
}

///////////////////////////////////////////////////////////////////////////////

int ColorBreathingProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Make sure the number of the algorithm processing threads
   // is reasonable
   _processingThreadCount = toolIOConfig->processingThreadCount;
   if (_processingThreadCount < 1)
      _processingThreadCount = 1;
   else if (_processingThreadCount > CB_MAX_THREAD_COUNT)
      _processingThreadCount = CB_MAX_THREAD_COUNT;

   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // number of input and output frames. We read and write one frame
   // each iteration, and write output to a new buffer, not in place.

   SetInputMaxFrames(0, 1, 1); // 'In' port index,
   // One frame required for each iteration
   // One new frame needed every iteration

   SetOutputMaxFrames(0, 1); // 'Out' port index,
   // One frame written each iteration

 SetOutputNewFrame(0, 0); // 'Out' port index, 'In' port index
///SetOutputModifyInPlace(0, 0, false);

   return 0;
}

///////////////////////////////////////////////////////////////////////////////

int ColorBreathingProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   _inFrameIndex = frameRange.inFrameIndex;
   _outFrameIndex = frameRange.outFrameIndex;

   // Find if any frames have already been rendered
	 CBinManager binManager;

	 auto frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
    _listOfDirtyFrameIndexes.clear();

	 int progressPercent;
	 int modifiedFrameCount;

    CHRTimer hrt;
	 auto iErr = binManager.FindDirtyFrames(frameList, _inFrameIndex, _outFrameIndex, _listOfDirtyFrameIndexes, &progressPercent,
		  &modifiedFrameCount);

    if (iErr != 0)
    {
     	 TRACE_0(errout << "Warning, could not find rendered frame list, err: " << iErr);
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
int ColorBreathingProc::SetParameters(CToolParameters *toolParams)
{
   auto toolParameters = dynamic_cast<ColorBreathingToolParameters *>(toolParams);
   _model = toolParameters->Model;
   _tool = toolParameters->Tool;
   return 0;
}

int ColorBreathingProc::DoProcess(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("ColorBreathingProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   auto imageFormat = GetSrcImageFormat(0);
   auto pixelsCol = imageFormat->getPixelsPerLine();
   auto pixelsRow = imageFormat->getLinesPerFrame();

// INTERNAL FORMAT IS ALWAYS 3 COMPONENTS, tool shouldn't care what the source is!
//   // Return error if a monochrome image
//   if (imageFormat->getComponentCount() != 3)
//   {
//      autoErr.errorCode = theError.getError();
//      autoErr.msg << "Color Breathing only works on three component images, Image formate components = " <<
//         imageFormat->getComponentCount();
//
//      return -1;
//   }

   CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
   if (inToolFrameBufferB == nullptr)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "Input buffer is null ";
      return -1;
   }

   CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
   if (outToolFrameBuffer == nullptr)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "Output buffer is null ";
      return -1;
   }

   CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);

   auto baseOrigImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();
   if (baseOrigImage == NULL)
   {
      return -8006;
   }

   auto inIndex = outToolFrameBuffer->GetFrameIndex();
   auto &rgbFrameDatum = _model->RGBFrameData.GetRgbFrameDatum(inIndex);

   // MODEL should be used here
   if (_tool->getColorBreathingUseAnalysisRegion())
   {
         _boundingBox = rgbFrameDatum.ROI;
   }
   else
   {
      _boundingBox.Top = 0;
      _boundingBox.Left = 0;
      _boundingBox.Right = pixelsCol;
      _boundingBox.Bottom = pixelsRow;
   }

   if (((_boundingBox.Bottom - _boundingBox.Top) <= 0) || (_boundingBox.Right - _boundingBox.Left) < 0)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "ROI is incorrect set for frame index " << inIndex;
      return -1;
   }

   // Only works on 3 components, cannot work on monochrome
   auto mean(new double[3]);

   // Get the model from the parameters
   // set the ROI values here
   const int bytesPerPixel = 6;
   auto startPixelOfMask = _boundingBox.Top * pixelsCol + _boundingBox.Left;
   auto sourceStep = bytesPerPixel * pixelsCol;
   IppiSize sizeMask
   {
      (_boundingBox.Right - _boundingBox.Left), (_boundingBox.Bottom - _boundingBox.Top)
   };

   ippiMean_16u_C3R(baseOrigImage + 3 * startPixelOfMask, sourceStep, sizeMask, mean);

   // Kevin needs 8 bit data, we don't but this is for historical
   MTI_UINT16 componentValues[3];
   imageFormat->getComponentValueMax(componentValues);
   auto dataMaximum = componentValues[0];

   RGBFrameDatum rgb;
   rgb.Red = (mean[0] / dataMaximum) * 255;
   rgb.Green = (mean[1] / dataMaximum) * 255;
   rgb.Blue = (mean[2] / dataMaximum) * 255;
   rgb.Valid = true;
   rgb.Rendered = false;
   rgb.ROI = rgbFrameDatum.ROI;

   rgb.WasRendered = std::find(_listOfDirtyFrameIndexes.begin(), _listOfDirtyFrameIndexes.end(), inIndex) != _listOfDirtyFrameIndexes.end();
   _model->MakeQuantiles(baseOrigImage + 3 * startPixelOfMask, sourceStep, sizeMask, rgb);
   _model->SetRGBDatum(inIndex, rgb);

   return 0;
}

int ColorBreathingProc::BeginProcessing(SToolProcessingData & procData)
{
   GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
   GetToolProgressMonitor()->SetStatusMessage("Finding RGB Value");
   GetToolProgressMonitor()->StartProgress();

   MTI_UINT16 componentValues[3];

   auto *imageFormat = GetSrcImageFormat(0);
   imageFormat->getComponentValueMax(componentValues);
   _model->SetQuantileMaximum(componentValues[0]);
   return 0;
}

int ColorBreathingProc::EndProcessing(SToolProcessingData & procData)
{
   // Did render run to completion or was it aborted?
   if (procData.iteration == GetIterationCount(procData))
   {
      GetToolProgressMonitor()->SetStatusMessage("Graph Complete");
      GetToolProgressMonitor()->StopProgress(true);
   }
   else
   {
      GetToolProgressMonitor()->SetStatusMessage("Stopped");
      GetToolProgressMonitor()->StopProgress(false);
   }

   _model->RGBFrameData.NewProcessedData = true;
   return 0;
}

int ColorBreathingProc::BeginIteration(SToolProcessingData & procData)
{
   int inputFrameIndexList[2];
   inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
   SetInputFrames(0, 1, inputFrameIndexList);

   int outputFrameIndexList[2];
   outputFrameIndexList[0] = _inFrameIndex + procData.iteration;
   SetOutputFrames(0, 1, outputFrameIndexList);

   // When finished processing, release the oldest frame.
   SetFramesToRelease(0, 1, inputFrameIndexList);
   return 0;
}

int ColorBreathingProc::EndIteration(SToolProcessingData & procData)
{
   GetToolProgressMonitor()->BumpProgress();
   return 0;
}

int ColorBreathingProc::GetIterationCount(SToolProcessingData & procData)
{
   return _outFrameIndex - _inFrameIndex;
}
