// ---------------------------------------------------------------------------

#pragma hdrstop

#include "RGBFrameData.h"
#include <memory>
#include "MathFunctions.h"

#define WIN32_LEAN_AND_MEAN
#include "Windows.h"
#include "stdio.h"
#include "stdlib.h"

CSpinLock RGBFrameData::readWriteLock;

// ---------------------------------------------------------------------------


RGBFrameData::RGBFrameData()
{
}

void RGBFrameData::setDataValid(bool value)
{
   if (_isDataValid != value)
   {
      _isDataValid = value;
   }
}

void RGBFrameData::SetTotalFrames(int frameOut)
{
   CAutoSpinLocker lock(readWriteLock);
   // If range not valid, do nothing
   if (frameOut < 0)
   {
      _isRangeValid = false;
      return;
   }

   // This is a little dangerous as we have to invalidate the data if a
   // clip has changed.  Optimization should happen
   if (_frameOut == frameOut)
   {
      return;
   }

   _frameOut = frameOut;

   // If an initialization of data is not required, just
   // do a resize
   _rgbData.clear();
   _rgbData.resize(_frameOut);
   _isRangeValid = true;
   setDataValid(false);
}

void RGBFrameData::SetMarkRange(int markIn, int markOut)
{

   // If range not valid, do nothing
   if ((markIn < 0) || (markIn < 0))
   {
      _markIn = markIn;
      _markOut = markOut;
      _isRangeValid = false;
      return;
   }

   // This is a little dangerous as we have to invalidate the data if a
   // clip has changed.  Optimization should happen
   if ((_markIn == markIn) && (_markOut == markOut))
   {
      return;
   }

   _markIn = markIn;
   _markOut = markOut;
   CheckAndSetDataValid();
}

void RGBFrameData::Invalidate()
{
   CAutoSpinLocker lock(readWriteLock);
   for (auto &c : _rgbData)
    {
       c.Valid = false;
    }

   setDataValid(false);
}

void RGBFrameData::Invalidate(int markIn, int markOut)
{
   CAutoSpinLocker lock(readWriteLock);
   std::for_each(_rgbData.begin() + markIn, _rgbData.begin() + markOut, [](RGBFrameDatum &datum)
   	{
			datum.SetRGBInvalid();
   	}
   );
}

void RGBFrameData::DissassociateFilename()
{
   _loadedFileName = "";
   _guid_t defaultGuid;
   _clipGUID = defaultGuid;
}

RGBFrameData::~RGBFrameData()
{
}

bool RGBFrameData::IsRangeValid() const
{
   return _isRangeValid;
}

bool RGBFrameData::IsDataValid() const
{
   return _isDataValid;
}


RGBFrameDatum &RGBFrameData:: operator[](int index)
{
   CAutoSpinLocker lock(readWriteLock);

   // Debug trace code
   if ((index < 0) || (index >= _rgbData.size()))
   {
      TRACE_2(errout << "indexing error in " << __func__ << ", index " << index << ", size " << _rgbData.size());
      return _invalidRgbDatum;
   }

   return _rgbData[index];
}

vector<RGBFrameDatum> RGBFrameData::GetRGBFrameData()
{
   CAutoSpinLocker lock(readWriteLock);
   return _rgbData;
}

void RGBFrameData::SetRgbFrameDatum(int index, const RGBFrameDatum &rgbFrameDatum)
{
   CAutoSpinLocker lock(readWriteLock);
   if ((index < 0) || (index >= _rgbData.size()))
   {
      TRACE_2(errout << "indexing error in " << __func__ << ", index " << index << ", size " << _rgbData.size());
      return;
   }

   if ((_rgbData[index].DataSameAs(rgbFrameDatum) == false) || (_rgbData[index].Valid != rgbFrameDatum.Valid) ||
      (_rgbData[index].Rendered != rgbFrameDatum.Rendered))
   {
      _rgbData[index] = rgbFrameDatum;
      SetDataDirty(true);
   }
}

bool RGBFrameData::IsDataDirty() const
{
   return _rbgDataDirty;
}

void RGBFrameData::SetDataDirty(bool dirty)
{
   // Dangerous exposing this, only the timeline should set it back
   _rbgDataDirty = dirty;
}


RGBFrameDatum &RGBFrameData::GetRgbFrameDatum(int index)
{
   CAutoSpinLocker lock(readWriteLock);
   if ((index < 0) || (index >= _rgbData.size()))
   {
      TRACE_2(errout << "indexing error in " << __func__ << ", index " << index << ", size " << _rgbData.size());
      return _invalidRgbDatum;
   }

   return _rgbData[index];
}

RECT RGBFrameData::GetROI(int index) const
{
  CAutoSpinLocker lock(readWriteLock);
  ShortROI roi;

   if ((index < 0) || (index >= _rgbData.size()))
   {
      TRACE_2(errout << "indexing error in " << __func__ << ", index " << index << ", size " << _rgbData.size());
      roi = _invalidRgbDatum.ROI;
   }
   else
   {
      roi = _rgbData[index].ROI;
   }

   return {.left = roi.Left, .right = roi.Right, .top = roi.Top, .bottom = roi.Bottom };
}

bool RGBFrameData::CheckAndSetDataValid(const std::pair<int, int> &breakPair)
{
   _isDataValid = true;

   if ((breakPair.first < 0) || (breakPair.second < 0))
   {
      return false;
   }

   for (int i = breakPair.first; i <= breakPair.second; i ++)
   {
      if (!(*this)[i].Valid)
      {
         _isDataValid = false;
         return _isDataValid;
      }
   }

   return _isDataValid;
}

bool RGBFrameData::IsGraphValid(int markIn, int markOut)
{
   CAutoSpinLocker lock(readWriteLock);
   return std::all_of(_rgbData.cbegin() + markIn, _rgbData.cbegin() + markOut, [](RGBFrameDatum datum)
       {
          return datum.Valid;
       });
}

bool RGBFrameData::IsRenderValid(int markIn, int markOut)
{
    CAutoSpinLocker lock(readWriteLock);
    return std::all_of(_rgbData.cbegin() + markIn, _rgbData.cbegin() + markOut, [](RGBFrameDatum datum)
       {
          return datum.Rendered && datum.Valid;
       });
}

bool RGBFrameData::CheckAndSetDataValid()
{
   CAutoSpinLocker lock(readWriteLock);
   auto valid = false;

   if ((_markIn < 0) || (_markOut < 0))
   {
      valid = false;
   }
   else
   {
      valid = true;
      for (int i=_markIn; i < _markOut; i++)
      {
         if (!(*this)[i].Valid)
         {
            valid = false;
            break;
         }
      }
//      valid = std::all_of(_rgbData.cbegin()+_markIn, _rgbData.cbegin()+_markOut,
//                              [](RGBFrameDatum datum) {return datum.Valid;});
   }

   _isDataValid = valid;
   return _isDataValid;
}

bool RGBFrameData::CheckDataValid(const std::pair<int, int> &breakPair, const ShortROI &roi) const
{
   CAutoSpinLocker lock(readWriteLock);

     auto start = std::max(breakPair.first, 0);
     auto end = std::min(breakPair.second, _frameOut);
     for (int i = start; i <= end; i++)
     {
         auto &rgbDatum = _rgbData[i];
         if (rgbDatum.Valid == false)
         {
            return false;
         }

         if (roi.Equals(rgbDatum.ROI) == false)
         {
            return false;
         }
     }

     return true;
}

int RGBFrameData::size() const
{
   CAutoSpinLocker lock(readWriteLock);
   return _rgbData.size();
}

int RGBFrameData::GetFrames()
{
   CAutoSpinLocker lock(readWriteLock);
   return _frameOut;
}

int RGBFrameData::GetMarkIn()
{
   CAutoSpinLocker lock(readWriteLock);
   return _markIn;
}

int RGBFrameData::GetMarkOut()
{
   CAutoSpinLocker lock(readWriteLock);
   return _markOut;
}

   void RGBFrameData::SetROI(const RECT rect)
   {
      _roiRect = rect;
   }

   RECT RGBFrameData::GetROI() const
   {
      return _roiRect;
   }

int RGBFrameData::Save()
{
   if (_loadedFileName == "")
   {
      return -1;
   }

   return Save(_loadedFileName);
}

int RGBFrameData::Save(const string &fileName)
{
   CAutoSpinLocker lock(readWriteLock);
   // Structure of file is
   //  MagicNumber "RGB4"
   //  number of frames
   //  frame in
   //  frame out
   //  number of frames of RGBFrameDatusm
   int frames = _frameOut;
   if (frames <= 0)
   {
      return -1;
   }

   auto file = fopen(fileName.c_str(), "wb");
   if (file ==  nullptr)
   {
      return GetLastError();
   }

   if (fwrite(&MagicNumber, sizeof(MagicNumber), 1, file) != 1)
   {
      fclose(file);
      return -2;
   }

   if (fwrite(&frames, sizeof(frames), 1, file) != 1)
   {
      fclose(file);
      return -3;
   }

   if (fwrite(&_clipGUID, sizeof(_guid_t), 1, file) != 1)
   {
      fclose(file);
      return -4;
   }

   auto rgbDatumSize =  sizeof(RGBFrameDatum);
   auto n = _rgbData.size();
   auto nWritten = fwrite(_rgbData.data(), rgbDatumSize, n, file);
   if (nWritten != n)
   {
      fclose(file);
      return -5;
   }

   fclose(file);

   return 0;
}

bool RGBFrameData::SetROI(int inIndex, int outIndex, const RECT &newROI)
{
   CAutoSpinLocker lock(readWriteLock);
  // this take the ROI and the frame data and, for each frame, sees if the newROI matches
  // if it does and the frame data is valid, nothing is done, otherwise
  // the ROI is set to the newROI and the valid flag is set to 0.
  bool result = true;
  for (int i = inIndex; i < outIndex; i++)
  {
     auto &rgbDatum = _rgbData[i];
     if (!rgbDatum.ROI.Equals(newROI))
     {
        rgbDatum.SetRGBInvalid();
        rgbDatum.ROI.Set(newROI);
     }

     result = result && rgbDatum.Valid;
  }

  return result;
}

bool RGBFrameData::SetAllUnsetROI(const RECT &newROI, int frameIndex)
{
  // Larry wants an unset frame to use old ROI.  Unfortunally there are references
  // to the ROI without a getter or setter (UGH).  So in this case, if we are in
  // an invalid RGB data frame, reset all values around this.  This avoids having to
  // reset everything
  bool result = false;
  if (frameIndex < 0)
  {
    return false;
  }

  for (int i = frameIndex; i < _rgbData.size(); i++)
  {
     auto &rgbDatum = _rgbData[i];
     if (!rgbDatum.Valid)
     {
        rgbDatum.ROI.Set(newROI);
        result = true;
     }
     else
     {
       break;
     }
  }

  auto n = _rgbData.size();
  for (int i = frameIndex -1; i >= 0; i--)
  {
      if (i >= _rgbData.size())
      {
         continue;
      }

     auto &rgbDatum = _rgbData[i];
     if (!rgbDatum.Valid)
     {
        rgbDatum.ROI.Set(newROI);
        result = true;
     }
     else
     {
       break;
     }
  }

  return result;
}

RGBFrameDatum RGBFrameData::at(int index) const
{
   return _rgbData[index];
}

_guid_t RGBFrameData::GetGUID() const
{
   return  _clipGUID;
}

int RGBFrameData::Load(const string &fileName,  const _guid_t &expectedClipGUID)
{
   // Structure of file is
   //  MagicNumber "RGB1"
   //  number of frames
   //  _guid_t
   //  number of frames of RGBFrameDatusm

   _loadedFileName = fileName;
   auto file = fopen(fileName.c_str(), "rb");
   if (file ==  nullptr)
   {
      auto err = GetLastError();
      LoadMessage = GetSystemErrorMessage(err);
      return err;
   }

   DWORD magic;
   if (fread(&magic, sizeof(MagicNumber), 1, file) != 1)
   {
      fclose(file);
      LoadMessage = "Unexpected error reading magic number, RGB data must be recalculated";
      _clipGUID = expectedClipGUID;
      return -1;
   }

   if (magic != MagicNumber)
   {
      fclose(file);
      LoadMessage = "RGB Data comes from old version of tool, RGB data must be recalculated";
      _clipGUID = expectedClipGUID;
      return -2;
   }

   int frames;
   if (fread(&frames, sizeof(frames), 1, file) != 1)
   {
      fclose(file);
      LoadMessage = "Unexpected error reading number of frames, RGB data must be recalculated";
      _clipGUID = expectedClipGUID;
      return -3;
   }

   if (fread(&_clipGUID, sizeof(_guid_t), 1, file) != 1)
   {
      fclose(file);
      LoadMessage = "Unexpected error reading clip GUID, RGB data must be recalculated";
      _clipGUID = expectedClipGUID;
      return -4;
   }

   SetTotalFrames(frames);
   auto rgbDatumSize =  sizeof(RGBFrameDatum);
   auto n = _rgbData.size();
   auto nRead = fread(_rgbData.data(), rgbDatumSize, n, file);
   if (nRead != n)
   {
      fclose(file);
      LoadMessage = "Size of file is incorrect, RGB data must be recalculated";
      _clipGUID = expectedClipGUID;
      return -5;
   }

   // Now comes the fun part, see if the two guid agree, if no, we are assuming
   // they came from a version clip.  Throw out all rendered graphs
   if (guid_compare(_clipGUID, expectedClipGUID) != 0)
   {
      for (auto &rgbFrameDatum : _rgbData)
      {
         if (rgbFrameDatum.Rendered)
         {
            rgbFrameDatum.SetRGBInvalid();
         }
      }

      _clipGUID = expectedClipGUID;
   }

   fclose(file);

   return 0;
}

void RGBFrameData::SetLoadedFilename(const string fileName, const _guid_t &clipGUID)
{
   _loadedFileName = fileName;
   _clipGUID = clipGUID;
}

void RGBFrameData::MarkRendered(int frameIndex)
{
   _rgbData[frameIndex].Rendered = true;
}

void RGBFrameData::UpdateRenderFlag(const vector<int> &listOfCleanFrameIndexes)
{
   for (auto frameIndex : listOfCleanFrameIndexes)
   {
      // This might be called before we are ready, so do nothing
      if (frameIndex >= _rgbData.size())
      {
         return;
      }

      _rgbData[frameIndex].Rendered = false;
   }
}

bool RGBFrameData::AreAnyRenderFramesValid(int markIn, int markOut)
{
   CAutoSpinLocker lock(readWriteLock);
   return std::any_of(_rgbData.cbegin() + markIn, _rgbData.cbegin() + markOut, [](RGBFrameDatum datum)
       {
          return datum.Rendered && datum.Valid;
       });
}

void RGBFrameData::AddToPDLEntry(CPDLElement &parent)
{
    auto pdlRGBData = parent.MakeNewChild("RGBData");
    for (auto i = GetMarkIn(); i < GetMarkOut(); i++)
    {
       _rgbData[i].AddToPDLEntry(*pdlRGBData, i);
    }
}

int RGBFrameData::ReadRGBFrameDataFromPDLEntry(CPDLElement *pdlAttribute)
{
   // We are really in a slightly weird position here.  The
   // tool always creates all the RGBFrameData entries, so
   // we just read them in and assign them the correct one.
   RGBFrameDatum rgbFrameDatum;

   // The read doesn't read the validity
   rgbFrameDatum.Valid = true;

   int frameIndex;
   for (unsigned int i = 0; i < pdlAttribute->GetChildElementCount(); ++i)
   {
      rgbFrameDatum.ReadPDLEntry(pdlAttribute->GetChildElement(i), frameIndex);
      SetRgbFrameDatum(frameIndex, rgbFrameDatum);
   }

   return 0;
}
