// ---------------------------------------------------------------------------

#pragma hdrstop

#include "BreakFrameData.h"

void CBreakFrameData::ReadReferenceFrames(const string &referenceFrameFileName)
{
	ClearReferenceFrames();

	auto ini = CreateIniFile(referenceFrameFileName);
	StringList sections;
	ini->ReadSections(&sections);

	for (auto sectionName : sections)
	{
		// There is always an unnamed section, ignore it
		if (sectionName.empty())
		{
			continue;
		}

		ReferenceKeyframeData r;
		r.ReadParameters(ini, sectionName);
		Add(r);
	}

	DeleteIniFile(ini);

	_isDirty = true;

	SetFileName(referenceFrameFileName);
}

void CBreakFrameData::SetFileName(const string &referenceFrameFileName) {
	_referenceFrameFileName = referenceFrameFileName;}

void CBreakFrameData::WriteReferenceFrames()
{
	if (_referenceFrameFileName == "")
	{
		return;
	}

	DeleteFile(_referenceFrameFileName.c_str());
	auto ini = CreateIniFile(_referenceFrameFileName);
	for (auto m : _referenceKeyframes)
	{
		auto r = m.second;
		r->WriteParameters(ini, std::to_string(r->FrameIndex));
	}

	ini->WriteIntegerList("Breaks", "Internal", _breaks);

	DeleteIniFile(ini);
}

void CBreakFrameData::Add(const ReferenceKeyframeData &referenceKeyframeData)
{
	auto frameNumber = referenceKeyframeData.FrameIndex;

	// Delete if exists
	if (_referenceKeyframes.find(frameNumber) != _referenceKeyframes.end())
	{
		delete _referenceKeyframes[frameNumber];
	}

	// Now add it
	_referenceKeyframes[frameNumber] = new ReferenceKeyframeData(referenceKeyframeData);
	_isDirty = true;
	WriteReferenceFrames();
}

void CBreakFrameData::AddToPDLEntry(CPDLElement &parent)
{
	// Save the breaks
	auto pdlBreaks = parent.MakeNewChild("Breaks");
	pdlBreaks->SetAttribInteger("Count", _breaks.size());

	for (auto b : _breaks)
	{
		auto pldBreakList = pdlBreaks->MakeNewChild("Break");
		pldBreakList->SetAttribInteger("Frame", b);
	}

	// Create elements reference frames
	auto pdlReferenceList = parent.MakeNewChild("ReferenceKeyframe");
	for (auto referenceKeyframe : _referenceKeyframes)
	{
		referenceKeyframe.second->AddToPDLEntry(*pdlReferenceList);
	}
}

int CBreakFrameData::ReadBreaksFromPDLEntry(CPDLElement *pdlAttribute)
{
	// This is truely ugly, each PDL saves all the breaks, so we read them
	// back in.

	vector<int>newBreaks;
	for (unsigned int i = 0; i < pdlAttribute->GetChildElementCount(); ++i)
	{
		auto b = pdlAttribute->GetChildElement(i)->GetAttribInteger("Frame", -1);
		if (b >= 0)
		{
			newBreaks.push_back(b);
		}
	}

	_breaks = newBreaks;
	SetDirty(true);
	return 0;
}

int CBreakFrameData::ReadReferenceKeyframeFromPDLEntry(CPDLElement *pdlAttribute)
{
	// Create elements reference frames
	vector<ReferenceKeyframeData*>newRefrenceFrames;
	for (unsigned int i = 0; i < pdlAttribute->GetChildElementCount(); ++i)
	{
		auto *referenceKeyframeData = new ReferenceKeyframeData();
		referenceKeyframeData->ReadPDLEntry(pdlAttribute->GetChildElement(i));
		if (referenceKeyframeData->FrameIndex >= 0)
		{
			newRefrenceFrames.push_back(referenceKeyframeData);
		}
	}

	// Copy the frames, do this here in case of a failure in the reads
	ClearReferenceFrames();
	for (auto r : newRefrenceFrames)
	{
		_referenceKeyframes[r->FrameIndex] = r;
	}

	SetDirty(true);
	return 0;
}

void CBreakFrameData::Delete(const ReferenceKeyframeData &referenceKeyframeData) {
	Delete(referenceKeyframeData.FrameIndex);}

void CBreakFrameData::Delete(int frameIndex)
{
	delete _referenceKeyframes[frameIndex];
	_referenceKeyframes.erase(frameIndex);
	_isDirty = true;
	WriteReferenceFrames();
}

vector<ReferenceKeyframeData *>CBreakFrameData::GetAllReferenceKeyFrames()
{
	auto result = vector<ReferenceKeyframeData*>();
	for (auto ri : _referenceKeyframes)
	{
		result.push_back(ri.second);
	}

	return result;
}

vector<ReferenceKeyframeData *>CBreakFrameData::GetReferenceKeyFrames(int frame)
{
	auto result = vector<ReferenceKeyframeData*>();

	// If no breaks, return all
	if (_breaks.size() == 0)
	{
		for (auto ri : _referenceKeyframes)
		{
			result.push_back(ri.second);
		}

		return result;
	}

	// Find the [in, out) matches
	auto breakPair = GetBreakPair(frame);
	for (auto ri : _referenceKeyframes)
	{
		auto frameNumber = ri.first;
		if ((frameNumber >= breakPair.first) && (frameNumber <= breakPair.second))
		{
			result.push_back(ri.second);
		}
	}

	return result;
}

// BreakPair is a [n, m) half open pair of frame numbers.
std::pair<int, int>CBreakFrameData::GetBreakPair(int frame)
{
	auto result = std::pair<int, int>(0, INT_MAX);
	if (_breaks.size() == 0)
	{
		return result;
	}

	// This works because there is an implicit cut at
	// before the frames and after the frames.
	for (auto i : _breaks)
	{
		// We want the first i >= frame
		if (i <= frame)
		{
			result.first = i;
		}

		// Exit if we have exceeded end
		if (i > frame)
		{
			result.second = i - 1;
			break;
		}
	}

	return result;
}

void CBreakFrameData::ClearReferenceFrames()
{
	for (auto r : _referenceKeyframes)
	{
		delete r.second;
	}

	_referenceKeyframes.clear();
	_isDirty = true;
	DataHasChanged.Notify();
}

void CBreakFrameData::ClearReferenceFrames(int frameIn, int frameOut)
{
	vector<int>keysToDelete;
	for (auto r : _referenceKeyframes)
	{
		if ((r.first >= frameIn) && (r.first < frameOut))
		{
			keysToDelete.push_back(r.first);
		}
	}

	for (auto key : keysToDelete)
	{
		auto it = _referenceKeyframes.find(key);
		if (it != _referenceKeyframes.end())
		{
			delete it->second;
			_referenceKeyframes.erase(it);
		}
	}

	_isDirty = true;
	DataHasChanged.Notify();
}

void CBreakFrameData::ClearAll()
{
	ClearReferenceFrames();
	_breaks.clear();
}

void CBreakFrameData::SetBreaks(const vector<int> &breaks)
{
	auto changed = breaks.size() != _breaks.size();
	if (!changed)
	{
		// Just playing with c11,
		// Simple for loop is better
		auto boi = _breaks.begin();
		for (auto bn : breaks)
		{
			if (bn != *boi++)
			{
				changed = true;
				break;
			}
		}
	}

	_breaks = breaks;

	if (changed)
	{
		_isDirty = true;
		DataHasChanged.Notify();
	}
}

vector < std::pair < int, int >> CBreakFrameData::GetAllBreakPairs()
{
	auto result = vector < std::pair < int, int >> ();
	if (_breaks.size() == 0)
	{
		return result;
	}

	// This works because there is an implicit cut at
	// before the frames and after the frames.
	auto first = _breaks[0];
	for (auto i : _breaks)
	{
		// this just forces pairs to be built
		if (i > first)
		{
			result.push_back(std::pair<int, int>(first, i - 1));
			first = i;
		}
	}

	return result;
}

vector < std::pair < int, int >> CBreakFrameData::GetAllBreakPairs(int startFrame, int endFrame)
{
	// end is exclusive
	endFrame = endFrame - 1;

	// We have to add fake breaks at start and end frames
	vector<int>tempBreaks = {startFrame};

	vector < std::pair < int, int >> result;
	int last = startFrame;
	for (auto b : _breaks)
	{
		if ((b > last) && (b < endFrame))
		{
			result.push_back(std::pair<int, int> {last, b - 1});
			last = b;
		}
		else if ((b > last) && (b == endFrame))
		{
			// stupid kluge when break is last frame of selected
			// because kevin's is inclusive
			result.push_back(std::pair<int, int> {last, b});
			last = b;
		}

		// Now see if b is the last one
		if (b == endFrame)
		{
			break;
		}

		if (b >= endFrame)
		{
			result.push_back(std::pair<int, int> {b, endFrame});
			break;
		}
	}

	return result;
}

ReferenceKeyframeData *CBreakFrameData::FindReferenceframeDataAtFrameNumber(int frameNumber)
{
	auto b = _referenceKeyframes.find(frameNumber);
	if (b == _referenceKeyframes.end())
	{
		return nullptr;
	}

	return (*b).second;
}

bool CBreakFrameData::AreThereReferenceFramesHere(int frameIn, int frameOut)
{
	for (auto rf : _referenceKeyframes)
	{
		if ((rf.first >= frameIn) && (rf.first < frameOut))
		{
			return true;
		}
	}

	return false;
}

bool CBreakFrameData::IsDirty() const {return _isDirty;}

void CBreakFrameData::SetDirty(const bool dirty) {_isDirty = dirty;}

// ---------------------------------------------------------------------------
#pragma package(smart_init)
