//---------------------------------------------------------------------------

#ifndef ColorBreathingToolParametersH
#define ColorBreathingToolParametersH

#include "ColorBreathingModel.h"
#include "ColorBreathingTool.h"

//---------------------------------------------------------------------------

class ColorBreathingToolParameters : public CToolParameters
{
public:
   ColorBreathingToolParameters();
   virtual ~ColorBreathingToolParameters() { };

   ColorBreathingModel *Model = nullptr;
   ColorBreathingTool *Tool = nullptr;
};
#endif
