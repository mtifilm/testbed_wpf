// ---------------------------------------------------------------------------

#ifndef ColorBreathingProcH
#define ColorBreathingProcH

#include "ColorBreathingTool.h"
#include "ColorBreathingToolParameters.h"
#include "RGBFrameData.h"

#include "ToolObject.h"

class ColorBreathingProc : public CToolProcessor
{
public:
   ColorBreathingProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor);

   virtual ~ColorBreathingProc();

   CHRTimer HRT;

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;
   int _processingThreadCount;
   ShortROI _boundingBox;
   vector<int> _listOfDirtyFrameIndexes;

   ColorBreathingTool *_tool;
   ColorBreathingModel *_model;

};

// ---------------------------------------------------------------------------
#endif
