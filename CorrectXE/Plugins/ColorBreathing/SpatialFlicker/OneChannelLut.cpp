#include "OneChannelLut.h"
#include <stdlib.h>

namespace OneChannelLut
{

    static double InterpolateLinear(IppiPoint_32f p0, IppiPoint_32f p1, float v)
    {
         if (p1.x == p0.x)
         {
             return v;
         }

        return ((p1.y - p0.y) / (p1.x - p0.x)) * (v - p0.x) + p0.y;
    }

    static double InterpolateLut(vector<IppiPoint_32f>bendPoints, float v)
    {
        assert((v >= 0) && (v <= 1));

        // Clamp v
        v = std::min<float>(1, std::max<float>(0, v));

        // Start working up, this forces us to start at zero
        if (v < bendPoints[0].x)
        {
            IppiPoint_32f zeroPoint =
            {0, 0};
            return InterpolateLinear(zeroPoint, bendPoints[0], v);
        }

        for (auto i = 1; i < bendPoints.size(); i++)
        {
            if (v == bendPoints[i].x)
            {
               return bendPoints[i].y;
            }

            if (v < bendPoints[i].x)
            {
                return InterpolateLinear(bendPoints[i - 1], bendPoints[i], v);
            }
        }

        // This forces us to end at zero
        IppiPoint_32f onePoint =
        {1.0f, 1.0f};
        return InterpolateLinear(bendPoints[bendPoints.size() - 1], onePoint, v); ;
    }


    struct SplineSet
    {
        double a;
        double b;
        double c;
        double d;
        double x;
    };

    vector<SplineSet>spline(vector<double> &x, vector<double> &y)
    {
        int n = x.size() - 1;
        vector<double>a;
        a.insert(a.begin(), y.begin(), y.end());
        vector<double>b(n);
        vector<double>d(n);
        vector<double>h;

        for (int i = 0; i < n; ++i)
        {
            h.push_back(x[i + 1] - x[i]);
        }

        vector<double>alpha;
        for (int i = 0; i < n; ++i)
            alpha.push_back(3*(a[i + 1] - a[i]) / h[i] - 3*(a[i] - a[i - 1]) / h[i - 1]);

        vector<double>c(n + 1);
        vector<double>l(n + 1);
        vector<double>mu(n + 1);
        vector<double>z(n + 1);
        l[0] = 1;
        mu[0] = 0;
        z[0] = 0;

        for (int i = 1; i < n; ++i)
        {
            l[i] = 2 * (x[i + 1] - x[i - 1]) - h[i - 1] * mu[i - 1];
            mu[i] = h[i] / l[i];
            z[i] = (alpha[i - 1] - h[i - 1] * z[i - 1]) / l[i];
        }

        l[n] = 1;
        z[n] = 0;
        c[n] = 0;

        for (int j = n - 1; j >= 0; --j)
        {
            c[j] = z[j] - mu[j] * c[j + 1];
            b[j] = (a[j + 1] - a[j]) / h[j] - h[j] * (c[j + 1] + 2 * c[j]) / 3;
            d[j] = (c[j + 1] - c[j]) / 3 / h[j];
        }

        vector<SplineSet>output_set(n);
        for (int i = 0; i < n; ++i)
        {
            output_set[i].a = a[i];
            output_set[i].b = b[i];
            output_set[i].c = c[i];
            output_set[i].d = d[i];
            output_set[i].x = x[i];
        }

        return output_set;
    }

    vector<float>CreateLookupTable(vector<IppiPoint_32f>bendPoints, int levels)
    {
        vector<float>result;
        for (auto i = 0; i < levels; i++)
        {
            result.push_back(InterpolateLut(bendPoints, (double)i / (levels - 1)));
        }

        return result;
    }

    vector<float>CreateLookupTable(const vector<float> &lut, int levels)
    {
        // Early out
        if (lut.size() == levels)
        {
            return lut;
        }

        auto lutInc = 1.0f / (lut.size() - 1);

        vector<IppiPoint_32f>bendPoints;
        for (auto i=0; i < lut.size(); i++)
        {
            bendPoints.push_back({lutInc * i, lut[i]});
        }

        vector<float>result;
        for (auto i = 0; i < levels; i++)
        {
            result.push_back(InterpolateLut(bendPoints, (double)i / (levels - 1)));
        }

        return result;
    }

//    vector<float>CreateLookupTableCublicSpline(vector<IppiPoint_32f>bendPoints, int levels)
//    {
//        std::vector<double>xp;
//        std::vector<double>yp;
//
//        for (auto&p : bendPoints)
//        {
//            xp.push_back(p.x);
//            yp.push_back(p.y);
//        }
//
//        vector<float>result;
//        auto inc = xp.back() / (levels - 1);
//
//        auto splineSets = spline(xp, yp);
//
//        auto index = -1;
//        auto bendValue = -1.0;
//
//        for (auto i = 0; i < levels; i++)
//        {
//            auto v = inc * i;
//            if (v >= bendValue)
//            {
//                index++;
//                if (index == splineSets.size())
//                {
//                    result.push_back(yp.back());
//                    break;
//                }
//
//                bendValue = index == (splineSets.size() - 1) ? xp.back() : splineSets[index + 1].x;
//            }
//            double dx = v - splineSets[index].x;
//            double y = splineSets[index].a + splineSets[index].b * dx + splineSets[index].c * dx * dx +
//                splineSets[index].d * dx * dx * dx;
//
//            result.push_back(y);
//        }
//
//        return result;
//    }
//
//    vector<float>CreateLookupTableCublicSpline(const vector<float> &lut, int levels)
//    {
//        // Early out
//        if (lut.size() == levels)
//        {
//            return lut;
//        }
//        auto lutInc = 1.0 / (lut.size() - 1);
//
//        std::vector<double>xp;
//        std::vector<double>yp;
//
//        for (auto i=0; i < lut.size(); i++)
//        {
//            xp.push_back(lutInc * i);
//            yp.push_back(lut[i]);
//        }
//
//        vector<float>result;
//        auto inc = xp.back() / (levels - 1);
//
//        auto splineSets = spline(xp, yp);
//
//        auto index = -1;
//        auto bendValue = -1.0;
//
//        for (auto i = 0; i < levels; i++)
//        {
//            auto v = inc * i;
//            if (v >= bendValue)
//            {
//                index++;
//                if (index == splineSets.size())
//                {
//                    result.push_back(yp.back());
//                    break;
//                }
//
//                bendValue = index == (splineSets.size() - 1) ? xp.back() : splineSets[index + 1].x;
//            }
//            double dx = v - splineSets[index].x;
//            double y = splineSets[index].a + splineSets[index].b * dx + splineSets[index].c * dx * dx +
//                splineSets[index].d * dx * dx * dx;
//
//            result.push_back(y);
//        }
//
//        return result;
//    }


}
