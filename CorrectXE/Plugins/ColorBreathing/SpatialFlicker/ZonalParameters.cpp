//---------------------------------------------------------------------------

#pragma hdrstop

#include "ZonalParameters.h"
#include <cmath>
//---------------------------------------------------------------------------

#pragma package(smart_init)

#ifdef _DEBUGXX
#define FalseReturn(A, B) \
      if ((A)!=(B)) \
      {\
         TRACE_0(errout << #A << "!=" << #B << ", " << A << "!="<< B);\
         return false;  \
      }
#else
  #define FalseReturn(A, B) if ((A)!=(B)) return false;
#endif

bool ZonalParameters::doDeltaParametersMatch(const ZonalParameters &rhs) const
{
    // Base parameters are exactly those that are used to compute the deflicker
    // This does NOT include the range
    auto lhsRoi = getAnalysisRoi();
    auto rhsRoi = rhs.getAnalysisRoi();

    FalseReturn(getWidth(), rhs.getWidth());
    FalseReturn(getHeight(), rhs.getHeight());
    FalseReturn(lhsRoi.x, rhsRoi.x);
    FalseReturn(lhsRoi.y, rhsRoi.y);
    FalseReturn(lhsRoi.width, rhsRoi.width);
    FalseReturn(lhsRoi.height, rhsRoi.height);
    if (getZonalProcessType() != rhs.getZonalProcessType())
    {
    #ifdef _DEBUGXX
         TRACE_0(errout << "getZonalProcessType(), " << (int)getZonalProcessType() << "!="<< (int)rhs.getZonalProcessType());
    #endif
         return false;
    }

    FalseReturn(getHorizontalBlocks(), rhs.getHorizontalBlocks());
    FalseReturn(getVerticalBlocks(), rhs.getVerticalBlocks());
    FalseReturn(getOverlap(), rhs.getOverlap());
    FalseReturn(getNumberBins(), rhs.getNumberBins());
    FalseReturn(getNumIntensityLevels(), rhs.getNumIntensityLevels());
    if (std::fabs(getMotion() - rhs.getMotion()) > 1E-05)
    {
        FalseReturn(getMotion(), rhs.getMotion());
    }

    FalseReturn(getPdfSigma(), rhs.getPdfSigma());
    FalseReturn(getDownsample(), rhs.getDownsample());
    return true;
}

bool ZonalParameters::doSmoothingParametersMatch(const ZonalParameters &rhs) const
{
    FalseReturn(getSmoothingSigma(), rhs.getSmoothingSigma());
//    FalseReturn(getReferenceFrame(), rhs.getReferenceFrame());
//    FalseReturn(getHeadAnchorOffset(), rhs.getHeadAnchorOffset());
//    FalseReturn(getTailAnchorOffset(), rhs.getTailAnchorOffset());

    return true;
}

bool ZonalParameters::doProcessingParametersMatch(const ZonalParameters &rhs) const
{
    if (doSmoothingParametersMatch(rhs) == false)
    {
        return false;
    }

    return doDeltaParametersMatch(rhs);
}

bool ZonalParameters::doSegmentParametersMatch(const ZonalParameters &rhs) const
{
    if (getInFrame() != rhs.getInFrame())
        return false;
    if (getOutFrame() != rhs.getOutFrame())
        return false; ;

    return doProcessingParametersMatch(rhs);
}

MtiRect ZonalParameters::getAnalysisRoi() const
    {
        if (getUseAnalysisRegion())
        {
            // If the analysis is not defined, we need to use the full one
            if (_roi.isEmpty())
            {
            	return { 0, 0, getWidth(), getHeight()};
            }

            // make sure the ROI is in the image
            auto l = std::max<int>(_roi.x, 0);
            auto r = std::min<int>(getWidth(), _roi.x + _roi.width);
            auto w = r - l;

            auto t = std::max<int>(_roi.y, 0);
            auto b = std::min<int>(getHeight(), _roi.y + _roi.height);
            auto h = b - t;

            return {l, t, w, h};
        }

        return { 0, 0, getWidth(), getHeight()};
}

	  bool ZonalParameters:: operator == (const ZonalParameters &rhs) const {return doProcessingParametersMatch(rhs);}

	  int ZonalParameters::getVerticalBlocks() const
	  {
//		  if (_verticalBlocks > 0)
//		  {
//			  return _verticalBlocks;
//		  }

      // Because of roundoff, the number of boxes can change when reduced
      // Always use the reduced size to compute number of boxes.
      // WARNING, this means downsampling MUST be set before the first call to getVerticalBlocks
		 auto width = float(getAnalysisRoi().width);
       auto height = float(getAnalysisRoi().height);

       auto dsWidth = int(width / getDownsample());
       auto dsHeight = int(height / getDownsample());

		  MtiSize dsSize = {dsWidth, dsHeight};
        auto verticalBlocks =  verticalFromHorizontalBlocks(getHorizontalBlocks(), dsSize);
//        	DBTRACE2(getHorizontalBlocks(), verticalBlocks);
//	   DBTRACE2(dsSize, getDownsample());
        return verticalBlocks;
//		  auto ratio = 1;
//		  auto widthPerBox = ((float)roi.width + 0.5f) / _horizontalBlocks;
//		  return std::max<int>(2, int(float(roi.height) / (widthPerBox * ratio)));
	  }

///////////////////////////////////////////////////////////////////////////////////////////////////////////

   void ZonalParameters::setReferenceFrame(int value)
   {
     	_referenceFrame = value;
   }

   int ZonalParameters::getReferenceFrame() const
   {
      return _referenceFrame;
   }

   void ZonalParameters::setUseReferenceFrame(bool value)
   {
      if (getUseReferenceFrame() != value)
      {
        _referenceFrame = -(_referenceFrame + 1);
      }

      // Turn off others
      if (value)
      {
         setUseHeadAnchor(false);
         setUseTailAnchor(false);
      }
   }

   bool ZonalParameters::getUseReferenceFrame() const
   {
      return _referenceFrame >= 0;
   }

///////////////////////////////////////////////////////////////////////////////////////////////////////////

   void ZonalParameters::setHeadAnchorOffset(int value)
   {
   	if (value < 0)
      {
         setUseHeadAnchor(false);
         return;
      }

    	_headAnchor = value;
   }

   int ZonalParameters::getHeadAnchorOffset() const
   {
      return _headAnchor;
   }

   void ZonalParameters::setUseHeadAnchor(bool value)
   {
      if (getUseHeadAnchor() != value)
      {
        _headAnchor = -(_headAnchor + 1);
      }
   }

   bool ZonalParameters::getUseHeadAnchor() const
   {
      return _headAnchor >= 0;
   }

///////////////////////////////////////////////////////////////////////////////////////////////////////////

   void ZonalParameters::setTailAnchorOffset(int value)
   {
   	if (value < 0)
      {
         setUseTailAnchor(false);
         return;
      }

      _tailAnchor = value;
   }

   int ZonalParameters::getTailAnchorOffset() const
   {
      return _tailAnchor;
   }


   void ZonalParameters::setUseTailAnchor(bool value)
   {
      if (getUseTailAnchor() != value)
      {
         _tailAnchor = -(_tailAnchor + 1);
      }
   }

   bool ZonalParameters::getUseTailAnchor() const
   {
      return _tailAnchor >= 0;
   }

    void ZonalParameters::dumpDebugData() const
{
 		DBTRACE(getRoi());

		DBTRACE(getHorizontalBlocks());
		DBTRACE(_verticalBlocks);

		DBTRACE(getImageSize());
		DBTRACE(getInFrame());
		DBTRACE(getOutFrame());
		DBTRACE(getMaxValue());
		DBTRACE(getDownsample());
		DBTRACE(getProcessingChannels());

		DBTRACE((int)getZonalProcessType());
		DBTRACE(getUseProcessingRegion());
		DBTRACE(getUseAnalysisRegion());
		DBTRACE(getOverlap());
		DBTRACE(getNumberBins());
		DBTRACE(getFilterRadius());
		DBTRACE(getNumIntensityLevels());
		DBTRACE(getMotion());
		DBTRACE(getPdfSigma());

      // smoothing parameters
  		DBTRACE(getSmoothingSigma());
  		DBTRACE(getSmoothingType());
  		DBTRACE(getReferenceFrame());
  		DBTRACE(getHeadAnchorOffset());
  		DBTRACE(getTailAnchorOffset());
}
