// ---------------------------------------------------------------------------

#ifndef ZonalParametersH
#define ZonalParametersH
// ---------------------------------------------------------------------------

#ifdef __BORLANDC__
#include "IniFile.h"
//#include "pTimecode.h"
#include "MTIsleep.h"
#else
// This fakes some IniFile types
#include "MtiCore.h"
#endif

#include "Ippheaders.h"

// I hate manifest constants
#define CONTRAST_LUT_SIZE 1024
#define CONTRAST_FORWARD_LIMIT 65535
#define CONTRAST_LOG_OFFSET 0  // this turns it off, values are 50 high, 100 low
#define DEFAULT_DOWNSAMPLE 8

#define ZONAL_DB_UNINIT_VALUE LONG_MIN

enum class ChannelName
{
	Unknown = 0, Red = 1, Green = 2, Blue = 4
};

enum class ZonalProcessType
{
	Unknown = 0, Intensity = 1, SingleChannel = 2, ColorLinear = 3, ColorLog = 4
};

// These are the parameters for the preprocessing
// They are used 3 ways
// 1) Delta Parameters,  used to compute delta values for a fram
// 2) Processing Parameters, Delta + smoothing, all the values necessary for result
// 3) Segment parameters use for a segment of data, Processing + range.
// They could have been in three classes (maybe should have been) but evolution
// is a bitch
class ZonalParameters
{
public:

	// There has been made trivially copyable, so these aren't needed anymore
	// but left for historical reasons
	ZonalParameters & operator = (const ZonalParameters & rhs)
	{
		if (this == &rhs)
		{
			return *this;
		}

		setParameters(rhs);

		return *this;
	}

   bool operator == (const ZonalParameters &rhs) const;
   bool operator != (const ZonalParameters &rhs) const
   {
      return !(*this == rhs);
   }

	// We may want to make this virtual as = is over written, however old code
	// may blast the values in via memcpy.
	void setParameters(const ZonalParameters rhs)
	{
		setRoi(rhs.getRoi());

		setHorizontalBlocks(rhs.getHorizontalBlocks());
		setVerticalBlocks(rhs._verticalBlocks);

		setImageSize(rhs.getImageSize());
		setInFrame(rhs.getInFrame());
		setOutFrame(rhs.getOutFrame());
		setMaxValue(rhs.getMaxValue());
		setDownsample(rhs.getDownsample());
		setProcessingChannels(rhs.getProcessingChannels());

		setZonalProcessType(rhs.getZonalProcessType());
		setUseProcessingRegion(rhs.getUseProcessingRegion());
		setUseAnalysisRegion(rhs.getUseAnalysisRegion());
		setOverlap(rhs.getOverlap());
		setNumberBins(rhs.getNumberBins());
		setFilterRadius(rhs.getFilterRadius());
		setNumIntensityLevels(rhs.getNumIntensityLevels());
		setMotion(rhs.getMotion());
		setPdfSigma(rhs.getPdfSigma());

      //

      // smoothing parameters
  		setSmoothingSigma(rhs.getSmoothingSigma());
  		setSmoothingType(rhs.getSmoothingType());
  		setReferenceFrame(rhs.getReferenceFrame());
  		setHeadAnchorOffset(rhs.getHeadAnchorOffset());
  		setTailAnchorOffset(rhs.getTailAnchorOffset());
	}

   static int verticalFromHorizontalBlocks(int horizontalBlocks, const MtiSize &size)
   {
   	  auto widthPerBox = ((float)size.width + 0.5f) / horizontalBlocks;
		  return std::max<int>(2, int(float(size.height) / widthPerBox));
   }

   void setRoi(const IppiRect &roi)
   {
      _roi = roi;
   }

   void dumpDebugData() const;

   void setRoi(const RECT &roi)
   {
      _roi = convertToIppiRect(roi);
   }

   MtiRect getRoi() const
   {
      return _roi;
   }

   void setDownsample(int downSample)
   {
      _downSample = downSample;
   }

   int getDownsample() const
   {
      return _downSample;
   }

   void setUseProcessingRegion(const bool value)
   {
      _useProcessingRegion = value;
   }

   bool getUseProcessingRegion() const
   {
      return _useProcessingRegion;
   }

   void setUseAnalysisRegion(const bool value)
   {
      _useAnalysisRegion = value;
   }

   bool getUseAnalysisRegion() const
   {
      return _useAnalysisRegion;
   }

   void setImageSize(const IppiSize &size)
   {
      _imageSize = size;
   }

   MtiSize getImageSize() const
   {
      return _imageSize;
   }

   int getFramesToProcess() const
   {
      return _outFrame - _inFrame;
   }

   void setMaxValue(MTI_UINT16 maxValue)
   {
      _maxValue = maxValue;
   }

   MTI_UINT16 getMaxValue() const
   {
      return _maxValue;
   }

   int getWidth() const
   {
      return _imageSize.width;
   }

   int getHeight() const
   {
      return _imageSize.height;
   }

   void setInFrame(int value)
   {
      _inFrame = value;
   }

   int getInFrame() const
   {
      return _inFrame;
   }

   void setOutFrame(int value)
   {
      _outFrame = value;
   }

   int getOutFrame() const
   {
      return _outFrame;
   }

   void setOverlap(float value)
   {
      _overlap = value;
   }

   float getOverlap() const
   {
      return _overlap;
   }

   void setNumberBins(int value)
   {
      _numberBins = value;
   }

   int getNumberBins() const
   {
      return _numberBins;
   }

   void setFilterRadius(int value)
   {
      _filterRadius = value;
   }

   int getFilterRadius() const
   {
      return _filterRadius;
   }

   void setNumIntensityLevels(int value)
   {
      _numIntensityLevels = value;
   }

   int getNumIntensityLevels() const
   {
      return _numIntensityLevels;
   }

   void setMotion(float value)
   {
      _motion = value;
   }

   float getMotion() const
   {
      return _motion;
   }

   void setPdfSigma(float value)
   {
      _pdfSigma = value;
   }

   float getPdfSigma() const
   {
      return _pdfSigma;
   }

	// For now make vertical blocks a function of hor blocks and imagesize
	int getVerticalBlocks() const;
	void setVerticalBlocks(int value) { _verticalBlocks = value; }
	void setHorizontalBlocks(int value) { _horizontalBlocks = value; }

	int getHorizontalBlocks() const { return _horizontalBlocks; }

/////// Start Smoothing parameters

   void setSmoothingSigma(float value)
   {
      _smoothingSigma = value;
   }

   float getSmoothingSigma() const
   {
      return _smoothingSigma;
   }

   void setSmoothingType(int value)
   {
      _smoothingType = value;
   }

   int getSmoothingType() const
   {
      return _smoothingType;
   }

   void setReferenceFrame(int value);
   int getReferenceFrame() const;

   void setHeadAnchorOffset(int value);
   int getHeadAnchorOffset() const;

   void setTailAnchorOffset(int value);
   int getTailAnchorOffset() const;

 // Smoothing functions

   int getSmoothingRadius() const
   {
      return int(std::round<int>(3.5f * _smoothingSigma));
   }

   void setUseReferenceFrame(bool value);
   bool getUseReferenceFrame() const;

   void setUseHeadAnchor(bool value);
   bool getUseHeadAnchor() const;

   void setUseTailAnchor(bool value);
   bool getUseTailAnchor() const;

/////// End Smoothing parameters

	MtiRect getProcessingRoi() const
	{
		if (getUseProcessingRegion())
		{
			// make sure the ROI right, bottom doesn't exceed image
			auto w = _roi.width - std::max<int>(0, _roi.x + _roi.width - getWidth());
			auto h = _roi.height - std::max<int>(0, _roi.y + _roi.height - getHeight());

			return {_roi.x, _roi.y,  w, h };
		}

		return	{ 0, 0, getWidth(), getHeight() };
	}

	MtiRect getAnalysisRoi() const;

	bool doDeltaParametersMatch(const ZonalParameters &rhs) const;

	bool doSmoothingParametersMatch(const ZonalParameters &rhs) const;
	bool doProcessingParametersMatch(const ZonalParameters &rhs) const;
	bool doSegmentParametersMatch(const ZonalParameters &rhs) const;

   ZonalProcessType getZonalProcessType() const
   {
      return _flickerProcessType;
   }

   void setZonalProcessType(ZonalProcessType value)
   {
      _flickerProcessType = value;
   }

	//    int getAnalysisChannels() const { return _analysisChannels; }
	//    void setAnalysisChannels(int value) {_analysisChannels = value; }
	//
	//    // Helper functions
	//    void enableAnalysisChannel(ChannelName channel)
	//    {
	//        auto cs = getAnalysisChannels();
	//        setAnalysisChannels(cs | (int)channel);
	//    }
	//
	//    void disableAnalysisChannel(ChannelName channel)
	//    {
	//        auto cs = getAnalysisChannels();
	//        setAnalysisChannels(cs & ~(int)channel);
	//    }
	//
	//    bool isAnalysisChannelEnabled(ChannelName channel) { return ((int)channel & getAnalysisChannels()) != 0; }

   int getProcessingChannels() const
   {
      return _processingChannels;
   }

   void setProcessingChannels(int value)
   {
      _processingChannels = value;
   }

	// Helper functions
	void enableProcessingChannel(ChannelName channel)
	{
		auto cs = getProcessingChannels();
		setProcessingChannels(cs | (int)channel);
	}

	void disableProcessingChannel(ChannelName channel)
	{
		auto cs = getProcessingChannels();
		setProcessingChannels(cs & ~(int)channel);
	}

   bool isProcessingChannelEnabled(ChannelName channel)
   {
      return ((int)channel & getProcessingChannels()) != 0;
   }

private:
	MtiRect _roi {0, 0, 0, 0 };

	MtiSize _imageSize	{ -1, -1 };

	int _horizontalBlocks = 22;
	int _verticalBlocks = -1;
	bool _useProcessingRegion = false;
	bool _useAnalysisRegion = true;
	MTI_UINT16 _maxValue = 1023;
	int _inFrame = -1;
	int _outFrame = -1;

	int _downSample = DEFAULT_DOWNSAMPLE;

	float _overlap = 0.25;
	int _numberBins = 1024;
	int _filterRadius = 20;
	int _numIntensityLevels = 65536;
	float _motion = 0.0707f;
	float _pdfSigma = 20.0;

	ZonalProcessType _flickerProcessType = ZonalProcessType::Unknown;
	int _processingChannels = 7;
   int _smoothingType = 0;

	// Smoothing parameters
	float _smoothingSigma = 6.0;
   int _referenceFrame = ZONAL_DB_UNINIT_VALUE;
   int _headAnchor = -1;
   int _tailAnchor = -1;
};

// There is a bug is_trivially_copyable in VS2017, so just ignore for now
#ifndef _MSC_VER
///////static_assert(std::is_trivially_copyable<ZonalParameters>::value, "ZonalParameters must be able to be copied via mem copy");
#endif
#endif
