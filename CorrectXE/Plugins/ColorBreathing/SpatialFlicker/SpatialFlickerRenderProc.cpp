// ---------------------------------------------------------------------------

#pragma hdrstop

#include "SpatialFlickerRenderProc.h"
#include "ToolProgressMonitor.h"
#include "ClipAPI.h"
#include "Ippheaders.h"
#include "MTIsleep.h"
#include "err_cb.h"
#include "OneChannelLut.h"
#include <chrono>
#include <thread>
#include "GpuAccessor.h"

#pragma package(smart_init)

///////////////////////////////////////////////////////////////////////////////

SpatialFlickerRenderProc::SpatialFlickerRenderProc(int newToolNumber, const string &newToolName,
    CToolSystemInterface *newSystemAPI, const bool *newEmergencyStopFlagPtr,
    IToolProgressMonitor *newToolProgressMonitor)
    : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
    StartProcessThread(); // Required by Tool Infrastructure
}

///////////////////////////////////////////////////////////////////////////////

SpatialFlickerRenderProc::~SpatialFlickerRenderProc() {}

///////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------
#pragma package(smart_init)

// ****WARNING**** This will allow for multiple frames/iter but tool doesn't use it
int SpatialFlickerRenderProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
    // Make sure the number of the algorithm processing threads
    // is reasonable
    _processingThreadCount = toolIOConfig->processingThreadCount;
    if (_processingThreadCount < 1)
        _processingThreadCount = 1;
    else if (_processingThreadCount > RENDER_MAX_THREAD_COUNT)
        _processingThreadCount = RENDER_MAX_THREAD_COUNT;

    MTIassert(_processingThreadCount == 1);

    // Tell Tool Infrastructure, via CToolProcessor supertype, about the
    // number of input and output frames. We read and write one frame
    // each iteration, and write output to a new buffer, not in place.
    SetInputMaxFrames(0, _processingThreadCount, _processingThreadCount); // 'In' port index,

    // One frame required for each iteration
    // One new frame needed every iteration
    SetOutputMaxFrames(0, _processingThreadCount); // 'Out' port index,

    // One frame written each iteration
    SetOutputModifyInPlace(0, 0, false);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
    CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
    _inFrameIndex = frameRange.inFrameIndex;
    _renderNextIndex = _inFrameIndex;
    _outFrameIndex = frameRange.outFrameIndex;
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::SetParameters(CToolParameters *toolParams)
{
    auto toolParameters = dynamic_cast<ZonalFlickerToolParameters*>(toolParams);
    _spatialFlickerModel = toolParameters->Model;
    _tool = toolParameters->Tool;

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
 int SpatialFlickerRenderProc::BeginProcessing(SToolProcessingData &procData)
 {
	 GetToolProgressMonitor()->SetFrameCount(GetTotalFrames());
	 GetToolProgressMonitor()->SetStatusMessage("Render");
	 GetToolProgressMonitor()->StartProgress();

	 _tool->initializeMask();

	 GpuAccessor gpuAccessor;
	 if (gpuAccessor.useGpu())
	 {
       auto thinPlateSplineGpu = new CudaThinplateSpline16u(gpuAccessor.getDefaultGpu());
		 MTI_UINT16 maxValues[3], minValues[3];
       auto imageFormat = GetSrcImageFormat(0);
		 imageFormat->getComponentValueMax(&maxValues[0]);
		 imageFormat->getComponentValueMin(&minValues[0]);
       thinPlateSplineGpu->setMinMax(minValues[0], maxValues[0]);

       _spatialFlickerModel->setThinplateSplineSurfaceGpu(thinPlateSplineGpu);
	 }

	 return 0;
 }

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::EndProcessing(SToolProcessingData &procData)
{
    int result = 0;

    // Did render run to completion or was it aborted?
    if (procData.iteration == GetIterationCount(procData))
    {
    	_tool->setControlState(EMultiControlState::RENDERING_COMPLETE);
        GetToolProgressMonitor()->SetStatusMessage("Render Complete");
        GetToolProgressMonitor()->StopProgress(true);
    }
    else
    {
        _tool->setControlState(EMultiControlState::GOT_AN_ERROR);
        GetToolProgressMonitor()->SetStatusMessage("Render Stopped");
        GetToolProgressMonitor()->StopProgress(false);
    }

    // Update any render flags
    auto segments = _tool->getClipPreprocessData().getZonalDatabase().getSegmentRows(_inFrameIndex, _renderNextIndex);
    for (auto&segment : segments)
    {
        if ((segment.EndFrame <= _renderNextIndex) && (_tool->GetToolProcessingRenderFlag() == true))
        {
            segment.setRendered(true);
        }
    }

    _tool->getClipPreprocessData().getZonalDatabase().setSegmentRows(segments);
    return result;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::BeginIteration(SToolProcessingData &procData)
{
    int inputFrameIndexList[RENDER_MAX_THREAD_COUNT];

    _framesPerIter = _outFrameIndex - _processingThreadCount * procData.iteration;
    _framesPerIter = std::min(_framesPerIter, _processingThreadCount);
    if (_framesPerIter <= 0)
    {
        return CB_ERROR_INTERNAL_FRAMES_LESS_THAN_ZERO;
    }

    for (auto i = 0; i < _framesPerIter; i++)
    {
        inputFrameIndexList[i] = _inFrameIndex + _processingThreadCount * procData.iteration + i;
    }

    // Force
    _tool->findAnchorDeltaValuesIfNecessary(inputFrameIndexList[0]);
    SetInputFrames(0, _framesPerIter, inputFrameIndexList);

    // When finished processing, release the oldest frame.
    SetOutputFrames(0, _framesPerIter, inputFrameIndexList);

	 GpuAccessor gpuAccessor;
	 if (gpuAccessor.useGpu())
	 {
        auto frameIndex = _inFrameIndex + procData.iteration;
        auto imageSize = _spatialFlickerModel->getImageSize();
        auto rows = imageSize.height;
        auto cols = imageSize.width;
////        if (_tool->getMask(frameIndex) != nullptr)
////        {
////          Ipp8uArray maskArray(rows, cols, 1, (Ipp8u *)_tool->getMask(frameIndex));
////         _spatialFlickerModel->getThinplateSplineSurfaceGpu()->setMaskArray(maskArray);
////        }
	 }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::EndIteration(SToolProcessingData &procData)
{
    GetToolProgressMonitor()->BumpProgress();
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::GetIterationCount(SToolProcessingData &procData) {
    return (GetTotalFrames() + _processingThreadCount - 1) / _processingThreadCount;}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerRenderProc::DoProcess(SToolProcessingData &procData)
{
    CAutoErrorReporter autoErr("SpatialFlickerPreProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

    auto imageFormat = GetSrcImageFormat(0);

    // Return error if a monochrome image
    if (imageFormat->getComponentCount() != 3)
    {
        autoErr.errorCode = theError.getError();
        autoErr.msg << "Spatial flicker only works on three component images, Image formate components = " <<
            imageFormat->getComponentCount();

        return -1;
    }

    GpuAccessor gpuAccessor;
    if (gpuAccessor.useGpu())
	 {
  		return ProcessDeltaFrameGpu(0);
    }

    if (_spatialFlickerModel->getZonalProcessType() == ZonalProcessType::Intensity)
    {
        return ProcessDeltaFrame(0);
    }

    return ProcessColorDeltaFrame(0);
}

int SpatialFlickerRenderProc::ProcessDeltaFrame(int bufferIndex)
{
    CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, bufferIndex);
    if (inToolFrameBuffer == nullptr)
    {
        return CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL;
    }

    CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
    if (outToolFrameBuffer == nullptr)
    {

        return CB_ERROR_INTERNAL_OUTPUT_BUFFER_IS_NULL;
    }

    try
    {
        auto imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto outputImageData = outToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto frameIndex = inToolFrameBuffer->GetFrameIndex();

        _tool->loadZonalDataIntoModelIfNecessary(frameIndex);
        if (_spatialFlickerModel->isDataLoaded(frameIndex) == false)
        {
            return CB_REPAIR_DATA_IS_INVALID;
        }

        auto imageSize = _spatialFlickerModel->getImageSize();
        auto rows = imageSize.height;
        auto cols = imageSize.width;
        Ipp16uArray source({cols, rows, 3}, imageData);
        auto planes = source.copyToPlanar();

        _spatialFlickerModel->computeDeltaSurface(frameIndex, 0, _tool->getMask(frameIndex));
        _spatialFlickerModel->applyLutInPlace(frameIndex, planes[0]);
        _spatialFlickerModel->applyDeltaSurfaceInPlace(planes[0], -1);
        _spatialFlickerModel->applyInverseLutInPlace(frameIndex, planes[0]);
        planes[0].exportToInterleave3(source);

        outToolFrameBuffer->SetForceWrite(true);
        _renderNextIndex = frameIndex + 1;
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << ex.what());
        return -1;
    }
    catch (...)
    {
        TRACE_0(errout << "unknown error");
        return -1;
    }

    return 0;
}

int SpatialFlickerRenderProc::ProcessColorDeltaFrame(int bufferIndex)
{
    CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, bufferIndex);
    if (inToolFrameBuffer == nullptr)
    {
        return CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL;
    }

    CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
    if (outToolFrameBuffer == nullptr)
    {
        return CB_ERROR_INTERNAL_OUTPUT_BUFFER_IS_NULL;
    }

    try
    {
        auto start = std::chrono::system_clock::now();

        auto imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto outputImageData = outToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto frameIndex = inToolFrameBuffer->GetFrameIndex();
        _tool->loadZonalDataIntoModelIfNecessary(frameIndex);
        if (_spatialFlickerModel->isDataLoaded(frameIndex) == false)
        {
            return CB_REPAIR_DATA_IS_INVALID;
        }

        auto imageSize = _spatialFlickerModel->getImageSize();
        auto rows = imageSize.height;
        auto cols = imageSize.width;

        Ipp16uArray source({cols, rows, 3}, imageData);
        auto planes = source.copyToPlanar();

        if (_spatialFlickerModel->isProcessingChannelEnabled(ChannelName::Red))
        {
            _spatialFlickerModel->computeDeltaSurface(frameIndex, 0, _tool->getMask(frameIndex));
            _spatialFlickerModel->applyLutInPlace(frameIndex, planes[0]);
            _spatialFlickerModel->applyDeltaSurfaceInPlace(planes[0], -1);
            _spatialFlickerModel->applyInverseLutInPlace(frameIndex, planes[0]);
        }

        if (_spatialFlickerModel->isProcessingChannelEnabled(ChannelName::Green))
        {
            _spatialFlickerModel->computeDeltaSurface(frameIndex, 1, _tool->getMask(frameIndex));
            _spatialFlickerModel->applyLutInPlace(frameIndex, planes[1]);
            _spatialFlickerModel->applyDeltaSurfaceInPlace(planes[1], -1);
            _spatialFlickerModel->applyInverseLutInPlace(frameIndex, planes[1]);
        }

        if (_spatialFlickerModel->isProcessingChannelEnabled(ChannelName::Blue))
        {

            _spatialFlickerModel->computeDeltaSurface(frameIndex, 2, _tool->getMask(frameIndex));
             _spatialFlickerModel->applyLutInPlace(frameIndex, planes[2]);
            _spatialFlickerModel->applyDeltaSurfaceInPlace(planes[2], -1);
            _spatialFlickerModel->applyInverseLutInPlace(frameIndex, planes[2]);
        }

        source.copyFromPlanar(planes);

        auto end = std::chrono::system_clock::now();
        auto time = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
      ////  TRACE_0(errout << " Color Render Time " << time / 1000.0);
        outToolFrameBuffer->SetForceWrite(true);
        _renderNextIndex = frameIndex + 1;
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << "ERROR " << ex.what());
        return -1;
    }
    catch (...)
    {
        TRACE_0(errout << "Unexpected ERROR ");
        return -1;
    }

    return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////

int SpatialFlickerRenderProc::ProcessDeltaFrameGpu(int bufferIndex)
{
    CAutoErrorReporter autoErr(__func__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

    CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, bufferIndex);
    if (inToolFrameBuffer == nullptr)
    {
        return CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL;
    }

    CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
    if (outToolFrameBuffer == nullptr)
    {

        return CB_ERROR_INTERNAL_OUTPUT_BUFFER_IS_NULL;
    }

    try
    {
        auto imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto outputImageData = outToolFrameBuffer->GetVisibleFrameBufferPtr();
        auto frameIndex = inToolFrameBuffer->GetFrameIndex();
        _tool->loadZonalDataIntoModelIfNecessary(frameIndex);

        auto imageSize = _spatialFlickerModel->getImageSize();
        auto height = imageSize.height;
        auto width = imageSize.width;

        Ipp16uArray imageInRaw({width, height, 3}, imageData);
        Ipp16uArray imageOut({width, height, 3}, outputImageData);

        // We can speed up by having the LUT not inplace
        // In reality, we should move the luts up

        auto maskData = _tool->getMask(frameIndex);
        Ipp8uArray maskArray({width, height}, (Ipp8u *)maskData);

        auto imageIn = imageInRaw.duplicate();

 //       MatIO::saveListToMatFile("c:\\temp\\test.mat", imageInRaw, "imageBeforLut");
        _spatialFlickerModel->applyLutInPlace(frameIndex, imageIn);

        // applyDeltaSurfceGpu only does ROI and mask, so we must copy outside images before
        imageOut <<= imageIn;
        auto result = _spatialFlickerModel->applyDeltaSurfaceGpu(imageIn, imageOut, frameIndex, maskArray);
        if (result != 0)
        {
            TRACE_0(errout << result << "Impossible error GPU");
        }

//                 MatIO::appendListToMatFile("c:\\temp\\test.mat",imageIn, "imageAfterGPU");
        _spatialFlickerModel->applyInverseLutInPlace(frameIndex, imageOut);
//        MatIO::appendListToMatFile("c:\\temp\\test.mat",imageOut, "imageAfterInverseLut");
        outToolFrameBuffer->SetForceWrite(true);
        _renderNextIndex = frameIndex + 1;
    }
    catch (const std::exception &ex)
    {
        autoErr.errorCode = -1;
        autoErr.msg << ex.what();
        TRACE_3(errout << autoErr.msg.str());
        return -1;
    }
    catch (...)
    {
        autoErr.errorCode = CB_ERROR_UNKNOWN_APPLY_SURFACE_ERROR;
        autoErr.msg << "Unknown Error";
        TRACE_3(errout << autoErr.msg.str());
        return CB_ERROR_UNKNOWN_APPLY_SURFACE_ERROR;
    }

    return 0;
}
