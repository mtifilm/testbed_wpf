// ---------------------------------------------------------------------------

#ifndef AnchorDeltaValuesH
#define AnchorDeltaValuesH
// ---------------------------------------------------------------------------

#include "ThinPlateSplineSurface.h"

class AnchorDeltaValues
{
public:
	void clear();

	// Each index is the change in the deltaValues
	MtiPlanar<Ipp32fArray>headDeltaValues[3];
	MtiPlanar<Ipp32fArray>tailDeltaValues[3];
   vector<MtiPoint> controlPositions;

   // TO DO: Get rid of anchors, reframe as they are in the parameters now
	void set(const ZonalParameters &params, const std::pair<int, int> &anchors, int refFrame, const SceneCut &sceneCut);
	bool areValuesValid(const ZonalParameters &params, const std::pair<int, int> &anchors, int refFrame, const SceneCut &sceneCut) const;

	// Scene Index is 0 for first frame in cut
   // In reality, the control points the same for every clipIndex and channel
	SurfaceControlPoints findSurfaceControlPoints(int clipIndex, int channel);
   bool isFrameInAnchors(int clipIndex);

   bool operator == (const AnchorDeltaValues &rhs) const;
   bool operator != (const AnchorDeltaValues &rhs) const
   {
      return !(*this == rhs);
   }

   SceneCut getSceneCut() const { return _sceneCut; }
   void setSceneCut(const SceneCut &scene) { _sceneCut = scene; }

   // NOTE: we only need to check one value
   bool isEmpty() const { return (headDeltaValues[0].size() == 0) && (tailDeltaValues[0].size() == 0); }

private:
	ZonalParameters _params;

	std::pair<int, int> _anchors = {-1, -1};
	int _refFrame = -1;

	SceneCut _sceneCut;
};
#endif
