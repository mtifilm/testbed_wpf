#ifndef ThinPlateSplineChannelLutH
#define ThinPlateSplineChannelLutH

#include "ThinPlateSplineSurface.h"

// This is just the LUT at a given point, replaces TPS
struct ControlPointData
{
    int FrameIndex;
    IppiPoint Point;
    vector<float> Lut;
};

typedef vector<ControlPointData>  LutControlPoints;

class ThinPlateSplineChannelLut final
{
public:

    ThinPlateSplineChannelLut(SurfaceControlPoints &surfacePoints, int number, float dataMax,
        const IppiSize &imageSize);

    ThinPlateSplineChannelLut(const vector <ControlPointData> &surfaceLuts, int number, float dataMax,
        const IppiSize &imageSize);

    ~ThinPlateSplineChannelLut();

    int NumberOfLuts()
    {
        return _numberOfLuts;
    }

    float DataMax()
    {
        return _dataMax;
    }
    vector < vector < float >> &Weights()
    {
        return _weights;
    }

    Ipp32fArray applyLut(Ipp32fArray &mat);
    void applyLutInPlace(Ipp32fArray &mat, int channel = 0);

private:
    float interpolateLut(const IppiPoint &point, float v);

    ThinPlateSplineSurface _baseThinPlateSplineSurface;

    SurfaceControlPoints _surfacePointsOld;
    vector <ControlPointData> _surfacePoints;
    vector < vector < float >> _weights;
    int _numberOfLuts = 0;
    float _dataMax = 0;

};
#endif
