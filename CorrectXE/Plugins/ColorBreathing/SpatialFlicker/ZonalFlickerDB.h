// ---------------------------------------------------------------------------

#ifndef ZonalFlickerDBH
#define ZonalFlickerDBH
// ---------------------------------------------------------------------------

#include "SqliteDB.h"
#include "SqliteBlob.h"
#include "sqlite3.h"
#include "IniFile.h"
#include "FileSweeper.h"
#include "map"
#include "ZonalParameters.h"

struct FrameStatus
{
    bool rendered : 1;
    bool preprocessed : 1;
    bool prerendered : 1;

    static const int renderedValue = 1;
    static const int preprocessedValue = 2;
    static const int prerenderedValue = 4;

    FrameStatus() =

    default;

    FrameStatus(int32_t value)
    {
        rendered = value & renderedValue;
        preprocessed = value & preprocessedValue;
        prerendered = value & prerenderedValue;
    }

    explicit operator int() const {
        return (rendered * renderedValue) + (preprocessed * preprocessedValue) + (prerendered * prerenderedValue);}
};

static_assert(std::is_trivially_copyable<FrameStatus>::value, "FrameStatus must be trivially copyable");

struct FrameStatusRow
{
    int64_t Id;
    FrameStatus Status;

    bool isRendered() const {return Status.rendered;}

    bool isPreprocessed() const {return Status.preprocessed;}

    bool isPrerendered() const {return Status.prerendered;}

    void setRendered(bool value) {Status.rendered = value;}

    void setPreprocessed(bool value) {Status.preprocessed = value;}

    void setPrerendered(bool value) {Status.prerendered = value;}

    FrameStatusRow() = default;

    FrameStatusRow(int64_t id, int32_t status = 0)
    {
        Id = id;
        Status = status;
    }

    friend std::ostream& operator << (std::ostream &os, const FrameStatusRow &row) {
        return os << "(id=" << row.Id << ", Status=" << (int)row.Status << ")";}
};

static_assert(std::is_trivially_copyable<FrameStatusRow>::value, "FrameRow must be trivially copyable");

class SegmentRow
{
public:
    int64_t Id = -1;
    string Folder;
    string Uuid;
    int InFrame;         // Beginning and end of breaks [)
    int OutFrame;
    int StartFrame = -1;      // Start and end of processing [)
    int EndFrame = -1;
    int Status = 0;
    int ReferenceFrame;
    int HeadAnchorOffset;
    int TailAnchorOffset;
    int64_t ParamId;

    SegmentRow() = default;

    SegmentRow(int64_t id, const string &folder, const string &uuid, int inFrame, int outFrame, int startFrame, int endFrame, int status, int referenceFrame, int headAnchorOffset, int tailAnchorOffset, int64_t paramId)
    {
        Id = id;
        Folder = folder;
        Uuid = uuid;
        InFrame = inFrame;
        OutFrame = outFrame;
        StartFrame = startFrame;
        EndFrame = endFrame;
        Status = status;
	     ReferenceFrame = referenceFrame;
    	  HeadAnchorOffset = headAnchorOffset;
        TailAnchorOffset = tailAnchorOffset;
        ParamId = paramId;
    }

    // This should really default to database
    SegmentRow(int64_t id, const string &folder, const string &uuid, int inFrame, int outFrame, int64_t paramId)
       : SegmentRow(id, folder, uuid, inFrame, outFrame, -1, -1, false, ZONAL_DB_UNINIT_VALUE, -1, -1, paramId)
    {
    }

    int totalFrames() const {return OutFrame - InFrame; }

    // The status is as follows
    //   Preprocessed is set if there is preprocessed data (may not match current parameters);
    //   Rendered is set if the segment is rendered with the preprocessed data
    //   PreRender is set if the segment was rendered with a dirty frame, one already rendered
    bool isRendered() const;
    bool isPreprocessed() const;
    bool isPrerendered() const;
    void setRendered(bool value);
    void setPreprocessed(bool value);
    void setPrerendered(bool value);

    string getRepairFileName() const {return CombinePathNames({Folder, Uuid}, ".dat");}
    string getAnchorsFileName(const SceneCut &markCut) const;
    string getDeltaFileName() const {return CombinePathNames({Folder, Uuid}, ".delta");}

    bool Similar(const SegmentRow &rhs) const
    {
        return
            (Folder == rhs.Folder) &&
            (Uuid == rhs.Uuid) &&
            (InFrame == rhs.InFrame) &&
            (OutFrame == rhs.OutFrame) &&
            (ParamId == rhs.ParamId);
    }

    bool operator == (const SegmentRow &rhs) const
    {
        return Similar(rhs) &&
            (Id == rhs.Id) &&
            (StartFrame == rhs.StartFrame) &&
			(Status == rhs.Status) &&
            (EndFrame == rhs.EndFrame);
    }

    bool operator != (const SegmentRow & rhs) const {return !(*this == rhs);}
};

static_assert(std::is_copy_assignable<SegmentRow>::value, "SegmentRow must be trivially copyable");

class CZonalFlickerDB final: private CSqliteDB
{
public:
    CZonalFlickerDB() = default;

    // Expose some of the members from base class
    using CSqliteDB::getLastError;
    using CSqliteDB::execute;
    using CSqliteDB::isConnected;

    // Note, since this is final, we don't need virtual here
    ~CZonalFlickerDB();

    int open(const string &databasePath, int timeOut = 1000);
    void close();

    int64_t findMatchingParametersId(const ZonalParameters &params);
    int64_t insertParametersIfNecessary(const ZonalParameters &params);
    ZonalParameters getParameters(int id) const ;
    ZonalParameters getProcessedParameters(const SegmentRow &row) const;

    string getDatabaseName() const {return _databaseFilename;}

    string getDatabasePath() const {return _databasePath;}
    string getDatabaseFullFileName() const ;

    vector<FrameStatusRow>getFrameRows(int start, int end) const;
    vector<FrameStatusRow>getFrameRows(const SegmentRow &row) const;

    int getFrameRowsCount(int start, int end) const;

    void setFrameRow(const FrameStatusRow &row);
    void setFrameRows(const vector<FrameStatusRow> &rows, int count=-1);
    vector<FrameStatusRow>getFrameRows(const string &where) const;

    void setSegmentRow(const SegmentRow &row);
    void setSegmentRows(const vector<SegmentRow> &rows);
    vector<SegmentRow>getSegmentRows(int start, int end) const;
    vector<SegmentRow>getSegmentRows(const string &where) const ;

    vector<string> dumpAsBase64(int &fileSize) const;
    string dumpAsBase64InOneBigAssString(int &fileSize) const;

private:
    const string _databaseFilename = "ZonalFlicker3.db";

    string _databasePath;

    void createDatabase(const string &databasePath);
    int64_t insertParameters(const ZonalParameters &params);

    int cachedEncodedDatabaseFileSize = 0;
    MTI_UINT64 cachedEncodedDatabaseLastModifiedTime = 0;
    string cachedEncodedDatabasePath;
    string cachedEncodedDatabaseString;
};

#endif
