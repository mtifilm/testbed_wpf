//---------------------------------------------------------------------------
//  
//  Note: this interface wraps the new C++ ZonalFlickerAnalysis interface.  It replaces the old
//  C interface that was not muti-thread safe; hence, three copies of the same code.
//  However, old interface was debugged and it made more sense to emulate it rather than start
//  fresh.  A bad decision for coding but good for speed.
//  Although ZonalFlickerAnalysis could be this class, I preferred this method so I wouldn't throw up
//  looking that actual Zonal Flicker Interface
#ifndef ZonalFlickerBaseWrapperH
#define ZonalFlickerBaseWrapperH
#include "ZonalFlickerOneChannelAnalysis.h"

enum class DeflickerAction
{
	Init = 0,
	ProcessFrame = 1,
	ReturnDeltas = 2,
	Smooth = 3,
};

extern "C"
{
	// This is temporary until we figure out how to do it properly
	// WARNING DO NOT CHANGE
	struct DeflickerParamsStruct
	{
		double Width;
		double Height;
		double TotalFrames;
		double ProcessType;
		double crows;
		double ccols;
		double FrctOverlap;
		double nbins;
		double FiltRad;
		double MaxFlicker;
		double NumIntensityLevels;
		double TMotion;
		double PdfSig;
		double SmoothRad;
		double SmoothSig;
		double Downsample;

		// Params not defined by matlab
		int InFrame;
		int OutFrame;
		int bitShift = 0;
		IppiRect Roi;
	};
}

class ZonalFlickerBaseWrapper
{
public:
	void deflickerBaseStart();
	void deflickerBaseEnd();

	void deflickerBaseInit(const DeflickerParamsStruct &oneChannelParams);
	void deflickerBaseProcessOneFrame(const DeflickerParamsStruct &params, const Ipp16uArray &frameImage);
	void deflickerBaseProcessOneFrame(const DeflickerParamsStruct &params, const Ipp32fArray &frameImage);
	int deflickerBaseGetDeltaSize(const DeflickerParamsStruct& params) const;
	void deflickerBaseGetDeltaValues(const DeflickerParamsStruct &params, float *deltaValues) const;

	void deflickerBaseFindControlPosition(const DeflickerParamsStruct &params, int cOffset,
                                           std::vector<int> &cols, int rOffset, std::vector<int> &rows, IppiSize imageSize);

   void deflickerBaseSmoothAll(
            const DeflickerParamsStruct &params,
            int refFrameClipIndex,
            std::pair<int, int> anchorFrameOffsets,
            float *deltaValues,
            float *smoothvalues,
            int smoothingType
            );

   void deflickerBaseGetSmoothedValues(const DeflickerParamsStruct &params, const vector<int> &anchors, float *offsets, int smoothingType);
   void deflickerBaseGetSmoothedValues(const DeflickerParamsStruct &params, const vector<int> &anchors, float *deltaValues, float *offsets, int smoothingType);

   //ZonalFlickerOneChannelAnalysis *getZonalFlickerOneChannelAnalysis() const { return  _zonalFlickerOneChannelAnalysis;}
//   void deflickerBaseSmoothToReferenceFrame(
//            const DeflickerParamsStruct &params,
//            int refFrameClipIndex,
//            std::pair<int, int> anchorFrameOffsets,
//            float *deltaValues,
//            float *smoothvalues);

private:
	int _totalFrames = 0;
	ZonalFlickerOneChannelAnalysis *_zonalFlickerOneChannelAnalysis = nullptr;
	Ipp32fArray _previousFrame;
	bool _startOfProcessing = false;

};

//
//---------------------------------------------------------------------------
#endif
