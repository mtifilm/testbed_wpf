//---------------------------------------------------------------------------

#ifndef ThinPlateSplineDeltasH
#define ThinPlateSplineDeltasH

#include "ThinPlateSplineChannelLut.h"
#include "IniFile.h"
#include "MTIsleep.h"
#include "IppArrayConvert.h"
#include "ZonalParameters.h"
#include "AnchorDeltaValues.h"

//---------------------------------------------------------------------------

struct TpsDeltaRow
{
    int frame; // Not really needed
    IppiPoint Point;
    float delta;
};

class ThinPlateSplineDeltaData
{
public:
    SurfaceControlPoints getPreProcessedSurfaceControlPoints(int frameIndex, int channel=0);
    bool isDataValid(int frameIndex);

    ZonalParameters &getFlickerParameters() {return _parameters;}

    // This reads the parameters and preprocessing from data file
    bool loadSegmentData(const string &fileName);
    static ZonalParameters readParameters(const string &fileName);

    static AnchorDeltaValues readAnchorDeltaValues(const std::string &fileName);

    // Hack to get these values
    AnchorDeltaValues rawAnchorDeltaValues;
private:
    // The outside vector is the channesl
    // The inside is a 3D array, fixed.
    vector <vector<float>> _deltaValues;
    vector<IppiPoint> _boxes;
    ZonalParameters _parameters;

    static ZonalParameters readParameters(std::ifstream &inFile);
    static vector<IppiPoint> readSegmentData(std::ifstream &inFile, const ZonalParameters &zonalParameters, vector<vector<float>> &deltaValues);
};

class SegmentsDeltas : public vector<ThinPlateSplineDeltaData>
{

public:
    bool isDataLoaded(int frameIndex);
    bool getPreProcessedSurfaceControlPoints(int frameIndex, int channel, SurfaceControlPoints &surfaceControlPoints);

private:
    bool makeRelativeIndices(int index, int &segmentNumber, int &segmentIndex);
};

// ---------------- DELETE WHEN STU's code is in --------------------


#endif
