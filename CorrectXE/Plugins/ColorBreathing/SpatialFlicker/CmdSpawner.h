#ifndef CmdSpawnerH
#define CmdSpawnerH
#pragma once

#include <windows.h> 
#include <strsafe.h>
#include <string>
#include <queue>
#include <mutex>

enum class CmdSpawnerStatus
{
	Unknown,
	DataAvailable,
	NoDataAvailable,
	Terminated
};

class CmdSpawner
{
public:
	CmdSpawner();
	virtual ~CmdSpawner();
	void  spawnChildProcess(const std::string &exeName, const std::string &args);
	////void  SpawnChildProcess(const std::string &exeName, const std::vector<std::string> &argList);
	bool readLine(std::string &line);
	void startReading();
	bool getNextLine(std::string &line);
	bool isQueueEmpty();

	CmdSpawnerStatus wait(int milliseconds);

	void doneReading()
	{
		if (_readThread == nullptr)
		{
			return;
		}

		if (!_childTerminated)
		{
			_readThread->join();
			delete _readThread;
		}
	
		_readThread = nullptr;
	}

	int terminateChild()
	{
		TerminateProcess(_piProcInfo.hProcess, -1);
		return 1;
	}

private:
	void attachStandardIO();
	void detachStandardIO();
	static int launchRead(CmdSpawner *spawner);
	int runRead();

private:
	HANDLE _childStandardOutRead = nullptr;
	HANDLE _childStandardOutWrite = nullptr;
	HANDLE _childStandardInRead = nullptr;
	HANDLE _childStandardInWrite = nullptr;
	std::mutex _lockMutex;
	std::queue<std::string> _queue;

	bool _childTerminated = true;
	std::thread *_readThread = nullptr;

	PROCESS_INFORMATION _piProcInfo;
};

#endif
