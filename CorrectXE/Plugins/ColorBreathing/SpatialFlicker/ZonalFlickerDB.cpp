// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ZonalFlickerDB.h"
#include "FileSweeper.h"
#include "MTIsleep.h"
#include <mutex>
#include <chrono>
#include <stddef.h>    // for _threadid macro
#include <fstream>
#include "base64.h"
#include "HRTimer.h"

using std::chrono::system_clock;
std::mutex g_lock1;
std::mutex g_lock2;

// ---------------------------------------------------------------------------
#pragma package(smart_init)


string CZonalFlickerDB::getDatabaseFullFileName() const
{
    return CombinePathAndName(getDatabasePath(), getDatabaseName());
}

CZonalFlickerDB::~CZonalFlickerDB()
{
    close();
}

int CZonalFlickerDB::open(const string &databasePath, int timeOut)
{
   if (databasePath.empty())
   {
      TRACE_1(errout << "Color Flicker database path is empty.");
      return 0;
   }

    std::lock_guard<std::mutex> lock(g_lock1);
    closeConnection();
    _databasePath = databasePath;
    auto newFileName = getDatabaseFullFileName();
    if (DoesFileExist(newFileName) == false)
    {
        createDatabase(databasePath);
    }
    else
    {
        auto start = system_clock::now();
        auto elapsed = start - start;
        auto seconds = timeOut / 1000.0;
        while (openConnection(getDatabaseName(), databasePath) == false)
        {
            if ((getLastErrorCode() != SQLITE_BUSY) || (elapsed.count() >= seconds))
            {
               throw std::runtime_error(getLastError());
            }

            MTImillisleep(10);
            elapsed = system_clock::now() - start;
        }
    }

    return 0;
}

void CZonalFlickerDB::close()
{
    if (isConnected() == true)
    {
       closeConnection();;
    }
}

void CZonalFlickerDB::createDatabase(const string &databasePath)
{
    if (DoesWritableDirectoryExist(databasePath) == false)
    {
        throw std::runtime_error(string("No writable directory exists: '") + databasePath + "'");
    }

    // This isn't necessary until someone desides to make this
    // public
    std::lock_guard<std::mutex> lock(g_lock2);

    if (openConnection(getDatabaseName(), databasePath) == false)
    {
        // sqlite needs to close it even on failure!
        string errorMessage = getLastError();
        closeConnection();
        throw std::runtime_error(errorMessage);
    }

// Create parameter table (Comment is for when autoformat messes it up
//    sql = "create table parameters("
//          "ID                 INTEGER PRIMARY KEY ASC,          "
//          "NAME               TEXT             UNIQUE NOT NULL, "
//          "VALUE              TEXT             );               ";

    auto sql = "create table parameters("
          "ID                 INTEGER PRIMARY KEY ASC,          "
          "NAME               TEXT             UNIQUE NOT NULL, "
          "VALUE              TEXT                             "
          ");";
    execute(sql);

    // Insert version
    execute("INSERT INTO parameters (NAME, VALUE) VALUES('Version', '2.0');");

//    sql = "create table params("
//          "ID           INTEGER PRIMARY KEY ASC NOT NULL,               "
//          "WIDTH        INTEGER DEFAULT(0) NOT NULL,                    "
//          "HEIGHT       INTEGER DEFAULT(0) NOT NULL,                    "
//          "ROI_X        INTEGER DEFAULT(0) NOT NULL,                    "
//          "ROI_Y        INTEGER DEFAULT(0) NOT NULL,                    "
//          "ROI_W        INTEGER DEFAULT(0) NOT NULL,                    "
//          "ROI_H        INTEGER DEFAULT(0) NOT NULL,                    "
//          "PROCESS_TYPE INTEGER DEFAULT(1) NOT NULL,                    "
//          "USE_PROCESS_ROI  TINYINTEGER DEFAULT(0) NOT NULL,            "
//          "USE_ANALYSIS_ROI TINYINTEGER DEFAULT(1) NOT NULL,            "
//          "DOWNSAMPLE       INTEGER DEFAULT(2) NOT NULL,               "
//          "HORIZONTAL_BOXES INTEGER DEFAULT(22) NOT NULL,               "
//          "VERTICAL_BOXES   INTEGER DEFAULT(16) NOT NULL,               "
//          "OVERLAP          REAL DEFAULT(0.25) NOT NULL,                "
//          "NUMBER_BINS      INTEGER DEFAULT(1024) NOT NULL,             "
//          "FILTER_RADIUS    INTEGER DEFAULT(50) NOT NULL,               "
//          "INTENSITY_LEVELS INTEGER DEFAULT(65536) NOT NULL,            "
//          "TMOTION          REAL DEFAULT(0.0707) NOT NULL,              "
//          "PDF_SIGMA        REAL DEFAULT(20.0) NOT NULL,                "
//          "SMOOTH_SIGMA     REAL DEFAULT(6.0) NOT NULL                  "
//          ");";

    std::ostringstream os;
    os << "create table params(";
    os << "ID           INTEGER PRIMARY KEY ASC NOT NULL,   ";
    os << "WIDTH        INTEGER DEFAULT(0) NOT NULL,        ";
    os << "HEIGHT       INTEGER DEFAULT(0) NOT NULL,        ";
    os << "ROI_X        INTEGER DEFAULT(0) NOT NULL,        ";
    os << "ROI_Y        INTEGER DEFAULT(0) NOT NULL,        ";
    os << "ROI_W        INTEGER DEFAULT(0) NOT NULL,        ";
    os << "ROI_H        INTEGER DEFAULT(0) NOT NULL,        ";
    os << "PROCESS_TYPE INTEGER DEFAULT(1) NOT NULL,        ";
    os << "USE_PROCESS_ROI  TINYINTEGER DEFAULT(0) NOT NULL,";
    os << "DOWNSAMPLE       INTEGER DEFAULT(" << DEFAULT_DOWNSAMPLE << ") NOT NULL,    ";
    os << "HORIZONTAL_BOXES INTEGER DEFAULT(22) NOT NULL,   ";
    os << "VERTICAL_BOXES   INTEGER DEFAULT(16) NOT NULL,   ";
    os << "OVERLAP          REAL DEFAULT(0.25) NOT NULL,    ";
    os << "NUMBER_BINS      INTEGER DEFAULT(1024) NOT NULL, ";
    os << "FILTER_RADIUS    INTEGER DEFAULT(50) NOT NULL,   ";
    os << "INTENSITY_LEVELS INTEGER DEFAULT(65536) NOT NULL,";
    os << "TMOTION          REAL DEFAULT(0.0707) NOT NULL,  ";
    os << "PDF_SIGMA        REAL DEFAULT(20.0) NOT NULL,    ";
    os << "SMOOTH_SIGMA     REAL DEFAULT(6.0) NOT NULL      ";
    os << ")";

    execute(os.str());

//    sql = "create table segment("
//               "ID         INTEGER PRIMARY KEY ASC   NOT NULL,    "
//               "PATH_NAME       TEXT,                             "
//               "UUID            TEXT,                             "
//               "IN_FRAME        INTEGER DEFAULT(0) NOT NULL,      "
//               "OUT_FRAME       INTEGER DEFAULT(0) NOT NULL,      "
//               "START_FRAME     INTEGER DEFAULT(-1) NOT NULL,     "
//               "END_FRAME       INTEGER DEFAULT(-1) NOT NULL,     "
//               "STATUS          TINYINTEGER DEFAULT(0) NOT NULL,  "
// 				  "REF_FRAME       INTEGER DEFAULT(0) NOT NULL,      "
//					  "HEAD_OFFSET     INTEGER DEFAULT(0) NOT NULL,      "
//					  "TAIL_OFFSET     INTEGER DEFAULT(0) NOT NULL,      "
//               "PARAM_ID        INTEGER NOT NULL,                 "
//               "FOREIGN KEY(PARAM_ID) REFERENCES params(ID) "
//               ")";
   os.str("");
   os << "create table segment(                             ";
   os << "ID         INTEGER PRIMARY KEY ASC   NOT NULL,    ";
   os << "PATH_NAME       TEXT,                             ";
   os << "UUID            TEXT,                             ";
   os << "IN_FRAME        INTEGER DEFAULT(0) NOT NULL,      ";
   os << "OUT_FRAME       INTEGER DEFAULT(0) NOT NULL,      ";
   os << "START_FRAME     INTEGER DEFAULT(-1) NOT NULL,     ";
   os << "END_FRAME       INTEGER DEFAULT(-1) NOT NULL,     ";
   os << "STATUS          INTEGER DEFAULT(0) NOT NULL,      ";
   os << "REF_FRAME       INTEGER DEFAULT(" << ZONAL_DB_UNINIT_VALUE <<") NOT NULL,      ";
   os << "HEAD_OFFSET     INTEGER DEFAULT(-1) NOT NULL,     ";
   os << "TAIL_OFFSET     INTEGER DEFAULT(-1) NOT NULL,     ";
   os << "PARAM_ID        INTEGER NOT NULL,                 ";
   os << "FOREIGN KEY(PARAM_ID) REFERENCES params(ID)       ";
   os << ")";

    execute(os.str());

    sql = "create table frame_status("
                "ID         INTEGER PRIMARY KEY ASC   NOT NULL,    "
                "STATUS     INTEGER DEFAULT(0) NOT NULL,           "
                "PARAM_ID   INTEGER                                "
                ")";

    execute(sql);
}

int64_t CZonalFlickerDB::findMatchingParametersId(const ZonalParameters &params)
{
    std::ostringstream sql;
    sql << "SELECT id FROM params WHERE";
    sql << " WIDTH=" << params.getWidth();
    sql << " AND HEIGHT=" << params.getHeight();

    auto roi = params.getAnalysisRoi();
    sql << " AND ROI_X= " << roi.x;
    sql << " AND ROI_Y=" << roi.y;
    sql << " AND ROI_W=" << roi.width;
    sql << " AND ROI_H=" << roi.height;
    sql << " AND PROCESS_TYPE=" << (int)params.getZonalProcessType();
    sql << " AND DOWNSAMPLE=" << params.getDownsample();
    sql << " AND HORIZONTAL_BOXES=" << params.getHorizontalBlocks();
    sql << " AND VERTICAL_BOXES=" << params.getVerticalBlocks();
    sql << " AND OVERLAP=" << params.getOverlap();
    sql << " AND FILTER_RADIUS=" << params.getFilterRadius();
    sql << " AND INTENSITY_LEVELS=" << params.getNumIntensityLevels();
    sql << " AND TMOTION=" << params.getMotion();
    sql << " AND PDF_SIGMA=" << params.getPdfSigma();
    sql << " AND SMOOTH_SIGMA=" << params.getSmoothingSigma();
    sql << ";";

    auto result = executeSelect(sql.str());
    if (!result->next())
    {
        // No match
        result->release();
        return -1;
    }

    auto id = result->columnDataAsInteger(0);
    result->release();
    return id;
}

int64_t CZonalFlickerDB::insertParameters(const ZonalParameters &params)
{
    std::ostringstream os;
    auto roi = params.getAnalysisRoi();

    os << "INSERT INTO params (";
    os << "WIDTH,HEIGHT,";
    os << "ROI_X,ROI_Y,ROI_W,ROI_H,PROCESS_TYPE,DOWNSAMPLE,HORIZONTAL_BOXES,VERTICAL_BOXES,";
    os << "OVERLAP,FILTER_RADIUS,INTENSITY_LEVELS,TMOTION,PDF_SIGMA,SMOOTH_SIGMA";
    os << ") VALUES(";
    os << params.getWidth() << ",";
    os << params.getHeight() << ",";
    os << roi.x << ",";
    os << roi.y << ",";
    os << roi.width << ",";
    os << roi.height << ",";
    os << (int)params.getZonalProcessType() << ",";
    os << params.getDownsample() << ",";
    os << params.getHorizontalBlocks() << ",";
    os << params.getVerticalBlocks() << ",";
    os << params.getOverlap() << ",";
    os << params.getFilterRadius() << ",";
    os << params.getNumIntensityLevels() << ",";
    os << params.getMotion() << ",";
    os << params.getPdfSigma() << ",";
    os << params.getSmoothingSigma();
    os << ")";

    auto n = execute(os.str());
    MTIassert(n != 0);

    return getLastInsertRowID();
}

ZonalParameters CZonalFlickerDB::getProcessedParameters(const SegmentRow &row) const
{
    auto result = getParameters(row.ParamId);

    // Note the getParameters only gets the params row,
    // we must fill in the rest.
    result.setInFrame(row.StartFrame);
    result.setOutFrame(row.EndFrame);
    result.setReferenceFrame(row.ReferenceFrame);
    result.setHeadAnchorOffset(row.HeadAnchorOffset);
    result.setTailAnchorOffset(row.TailAnchorOffset);
    return result;
}

ZonalParameters CZonalFlickerDB::getParameters(int id) const
{
    std::ostringstream sql;
    sql << "SELECT ";
    sql << "WIDTH,HEIGHT,";
    sql << "ROI_X,ROI_Y,ROI_W,ROI_H,PROCESS_TYPE,DOWNSAMPLE,HORIZONTAL_BOXES,VERTICAL_BOXES,";
    sql << "OVERLAP,FILTER_RADIUS,INTENSITY_LEVELS,TMOTION,PDF_SIGMA,SMOOTH_SIGMA ";
    sql << "FROM params WHERE id=" << id;

    auto result = executeSelect(sql.str());
    if (!result->next())
    {
        // No match
        result->release();
        throw std::runtime_error(string("No paramenters corsponding to id=" + std::to_string(id)));
    }

    ZonalParameters zp;

    auto c=0;
    auto iw = result->columnDataAsInteger(c++);
    auto ih = result->columnDataAsInteger(c++);
    zp.setImageSize({.width=iw, .height=ih});

    auto x = result->columnDataAsInteger(c++);
    auto y = result->columnDataAsInteger(c++);
    auto w = result->columnDataAsInteger(c++);
    auto h = result->columnDataAsInteger(c++);
    zp.setRoi({.x=x, .y=y, .width=w, .height=h} );

    auto processType = result->columnDataAsInteger(c++);
    zp.setZonalProcessType(ZonalProcessType(processType));
    zp.setDownsample(result->columnDataAsInteger(c++));
    zp.setHorizontalBlocks(result->columnDataAsInteger(c++));
    zp.setVerticalBlocks(result->columnDataAsInteger(c++));
    zp.setOverlap(result->columnDataAsDouble(c++));
    zp.setFilterRadius(result->columnDataAsInteger(c++));
    zp.setNumIntensityLevels(result->columnDataAsInteger(c++));
    zp.setMotion(result->columnDataAsDouble(c++));
    zp.setPdfSigma(result->columnDataAsDouble(c++));
    zp.setSmoothingSigma(result->columnDataAsDouble(c++));

    result->release();
    return zp;
}

int64_t CZonalFlickerDB::insertParametersIfNecessary(const ZonalParameters &params)
{
    auto id = findMatchingParametersId(params);
    if (id > 0)
    {
        // matching parameters have been founnd
        return id;
    }

    // Not found, insert it
    return insertParameters(params);
}

vector<FrameStatusRow> CZonalFlickerDB::getFrameRows(int start, int end) const
{
    std::ostringstream sql;
    sql << "WHERE id >=" << start << " AND id <" << end << " ORDER BY id";

    return getFrameRows(sql.str());
}

int CZonalFlickerDB::getFrameRowsCount(int start, int end) const
{
    std::ostringstream sql;
    sql << "SELECT count(id) FROM frame_status ";
    sql << "WHERE id >=" << start << " AND id <" << end;
    auto rowSelect = executeSelect(sql.str());

    if  (rowSelect->next() == false)
    {
        rowSelect->release();
        throw std::runtime_error("No data for getFrameRowCount");
    }

    auto count = rowSelect->columnDataAsInteger(0);

    rowSelect->release();
    return count;
}

vector<FrameStatusRow> CZonalFlickerDB::getFrameRows(const SegmentRow &row) const
{
    return getFrameRows(row.InFrame, row.OutFrame);
}

vector<FrameStatusRow> CZonalFlickerDB::getFrameRows(const string &where) const
{
    std::ostringstream sql;
    sql << "SELECT id, status FROM frame_status " << where;

    auto rowSelect = executeSelect(sql.str());
    vector<FrameStatusRow> result;
    while (rowSelect->next())
    {
        result.emplace_back(rowSelect->columnDataAsInteger(0), rowSelect->columnDataAsInteger(1));
    }

    return result;
}

void CZonalFlickerDB::setFrameRows(const vector<FrameStatusRow> &rows, int count)
{
    if (count < 0)
    {
        count = rows.size();
    }

    if (count == 0)
    {
       return;
    }

    beginTransaction();
    try
    {
        auto i = 0;
        for (auto &v : rows)
        {
            setFrameRow(v);
            i++;
            if (i >= count)
            {
                break;
            }
        }
    }
    catch (const std::exception &ex)
    {
        rollbackTransaction();
        throw ex;
    }
    catch (...)
    {
        rollbackTransaction();
        throw std::runtime_error("Unknown exception in setFrameRow");
    }

    commitTransaction();
}

void CZonalFlickerDB::setFrameRow(const FrameStatusRow &row)
{
    std::ostringstream sql;
    sql << "REPLACE INTO frame_status (ID, STATUS) VALUES(" << row.Id << "," << int(row.Status) << ")";
    execute(sql.str());
}

void CZonalFlickerDB::setSegmentRows(const vector<SegmentRow> &rows)
{
    // Early out
    if (rows.size() == 0)
    {
        return;
    }

    beginTransaction();
    try
    {
        for (auto&v : rows)
        {
            setSegmentRow(v);
        }
    }
    catch (const std::exception &ex)
    {
        rollbackTransaction();
        throw ex;
    }
    catch (...)
    {
        rollbackTransaction();
        throw std::runtime_error("Unknown exception in setFrameRow");
    }

    commitTransaction();
}

vector<SegmentRow> CZonalFlickerDB::getSegmentRows(int start, int end) const
{
    std::ostringstream os;
    os << "WHERE OUT_FRAME>" << start;
    os << " AND IN_FRAME<" << end;
    return getSegmentRows(os.str());
}

vector<SegmentRow> CZonalFlickerDB::getSegmentRows(const string &where) const
{
    vector<SegmentRow> result;

    std::ostringstream sql;
    sql << "SELECT ID, PATH_NAME, UUID, IN_FRAME, OUT_FRAME, START_FRAME, END_FRAME, STATUS, REF_FRAME, HEAD_OFFSET, TAIL_OFFSET, PARAM_ID FROM segment " << where;
    sql << " ORDER BY IN_FRAME ASC";

    auto rowSelect = executeSelect(sql.str());
    if (rowSelect == nullptr)
    {
       if (getLastErrorCode() == SQLITE_OK)
       {
          throw std::runtime_error("SQL returned a null row, Database may not be opened");
       }

       ThrowOnSqlError(getLastErrorCode());
    }

    while (rowSelect->next())
    {
        int c = 0;
        auto id = rowSelect->columnDataAsInteger(c++);
        auto folder = rowSelect->columnDataAsString(c++);
        auto uuid = rowSelect->columnDataAsString(c++);
        auto inFrame = rowSelect->columnDataAsInteger(c++);
        auto outFrame = rowSelect->columnDataAsInteger(c++);
        auto startFrame = rowSelect->columnDataAsInteger(c++);
        auto endFrame = rowSelect->columnDataAsInteger(c++);
        auto status = rowSelect->columnDataAsInteger(c++);
        auto refFrame = rowSelect->columnDataAsInteger(c++);
        auto headOffset = rowSelect->columnDataAsInteger(c++);
        auto tailOffset = rowSelect->columnDataAsInteger(c++);
        auto paramId = rowSelect->columnDataAsInteger(c++);
        result.emplace_back(id, folder, uuid, inFrame, outFrame, startFrame, endFrame, status, refFrame, headOffset, tailOffset, paramId);
    }

    rowSelect->release();;
    return result;
}

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

void CZonalFlickerDB::setSegmentRow(const SegmentRow &row)
{
    std::ostringstream sql;

    // Escape an '
    auto dir = ReplaceAll(row.Folder, "'", "''");

    // Kludge, if row.ID <= 0; just do an INSERT, else a REPLACE
    if (row.Id <= 0)
    {
        sql << "INSERT INTO Segment (PATH_NAME, UUID, IN_FRAME, OUT_FRAME, START_FRAME, END_FRAME,";
        sql << "STATUS, REF_FRAME, HEAD_OFFSET, TAIL_OFFSET, PARAM_ID) VALUES(";
        sql << "'" << dir << "','" << row.Uuid << "'," << row.InFrame << "," << row.OutFrame << "," <<
            row.StartFrame << "," << row.EndFrame << "," << row.Status << "," <<
            row.ReferenceFrame << "," << row.HeadAnchorOffset << "," << row.TailAnchorOffset <<  "," << row.ParamId << ")";
    }
    else
    {
        sql << "REPLACE INTO Segment (ID, PATH_NAME, UUID, IN_FRAME, OUT_FRAME, START_FRAME, END_FRAME,";
        sql << "STATUS, REF_FRAME, HEAD_OFFSET, TAIL_OFFSET, PARAM_ID) VALUES(";
        sql << row.Id << ",";
        sql << "'" << dir << "','" << row.Uuid << "'," << row.InFrame << "," << row.OutFrame << "," <<
            row.StartFrame << "," << row.EndFrame << "," << row.Status << "," <<
            row.ReferenceFrame << "," << row.HeadAnchorOffset << "," << row.TailAnchorOffset <<  "," << row.ParamId << ")";
    }

    execute(sql.str());
}

bool SegmentRow::isRendered() const
{
   return  (FrameStatus::renderedValue & Status) == FrameStatus::renderedValue;
}

bool SegmentRow::isPreprocessed() const
{
   return  (FrameStatus::preprocessedValue & Status) == FrameStatus::preprocessedValue;
}

bool SegmentRow::isPrerendered() const
{
   return  (FrameStatus::prerenderedValue & Status) == FrameStatus::prerenderedValue;
}

void SegmentRow::setRendered(bool value)
{
    if (value)
    {
        Status |= FrameStatus::renderedValue;
    }
    else
    {
        Status &= ~FrameStatus::renderedValue;
    }
}

void SegmentRow::setPreprocessed(bool value)
{
    if (value)
    {
        Status |= FrameStatus::preprocessedValue;
    }
    else
    {
        Status &= ~FrameStatus::preprocessedValue;
    }
}

void SegmentRow::setPrerendered(bool value)
{
    if (value)
    {
        Status |= FrameStatus::prerenderedValue;
    }
    else
    {
        Status &= ~FrameStatus::prerenderedValue;
    }
}

vector <string> CZonalFlickerDB::dumpAsBase64(int &fileSize) const
{
    fileSize = GetFileLength(getDatabaseFullName());
    if (fileSize == 0)
    {
        throw std::runtime_error(string("Database is too small '") + getDatabaseFullName() + "'");
    }

    ifstream dbFile(getDatabaseFullName(), ios::in | ios::binary);
    if (dbFile.good() == false)
    {
        throw std::runtime_error(string("Unable to open file '") + getDatabaseFullName() + "'");
    }

    const int chunkSize = 1024;
    char chunk[chunkSize];
    vector <string> result;

    while (dbFile.eof() == false)
    {
        dbFile.read(chunk, chunkSize);
        auto c = reinterpret_cast<unsigned char *>(chunk);
        result.emplace_back(base64_encode(c, dbFile.gcount()));
    }

    return result;
}

string CZonalFlickerDB::dumpAsBase64InOneBigAssString(int &fileSize) const
{
   if (cachedEncodedDatabasePath == getDatabaseFullName()
   && cachedEncodedDatabaseLastModifiedTime == GetLastModifiedTime(cachedEncodedDatabasePath)
   && cachedEncodedDatabaseFileSize > 0)
   {
      fileSize = cachedEncodedDatabaseFileSize;
      return cachedEncodedDatabaseString;
   }

   auto nonConstThis = const_cast<CZonalFlickerDB *>(this);
   nonConstThis->cachedEncodedDatabaseFileSize = 0;
   nonConstThis->cachedEncodedDatabaseLastModifiedTime = 0;
   nonConstThis->cachedEncodedDatabasePath = "";
   nonConstThis->cachedEncodedDatabaseString = "";

   fileSize = GetFileLength(getDatabaseFullName());
   if (fileSize == 0)
   {
      throw std::runtime_error(string("Database is too small '") + getDatabaseFullName() + "'");
   }

   ifstream dbFile(getDatabaseFullName(), ios::in | ios::binary);
   if (dbFile.good() == false)
   {
      fileSize = 0;
      throw std::runtime_error(string("Unable to open database file '") + getDatabaseFullName() + "'");
   }

   unsigned char *buffer = new unsigned char[fileSize];
   dbFile.read((char *)buffer, fileSize);
   if (dbFile.good() == false)
   {
      delete[] buffer;
      fileSize = 0;
      throw std::runtime_error(string("Unable to read database file '") + getDatabaseFullName() + "'");
   }

   nonConstThis->cachedEncodedDatabaseString = base64_encode(buffer, fileSize);
   nonConstThis->cachedEncodedDatabasePath = getDatabaseFullName();
   nonConstThis->cachedEncodedDatabaseLastModifiedTime = GetLastModifiedTime(cachedEncodedDatabasePath);
   nonConstThis->cachedEncodedDatabaseFileSize = fileSize;
   delete[] buffer;

   return cachedEncodedDatabaseString;
}

string SegmentRow::getAnchorsFileName(const SceneCut &markCut) const
{
   MTIostringstream os;
   os << markCut.first << "_" << markCut.second;
	return CombinePathNames({Folder, os.str()}, ".anchors.mat");
}

