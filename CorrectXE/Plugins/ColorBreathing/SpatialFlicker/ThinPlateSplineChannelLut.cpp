#include "ThinPlateSplineChannelLut.h"
#include "OneChannelLut.h"
#include <math.h>
#include "MTIsleep.h"
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

ThinPlateSplineChannelLut::ThinPlateSplineChannelLut(SurfaceControlPoints &surfacePoints, int number,
    float dataMax, const IppiSize &imageSize)
{
    _surfacePointsOld = surfacePoints;
    _numberOfLuts = number;
    _dataMax = dataMax;

    // First create a lut for each control point
    // We are assuming a same number of points for each element
    auto numberControlPoints = surfacePoints.size();

    vector < vector < float >> bendPointList;

    for (auto&surfacePoint : _surfacePointsOld)
    {
        vector<IppiPoint_32f>bendPoints;
        for (int k = 0; k < surfacePoint.Original.size(); k++)
        {
            IppiPoint_32f cp;
            cp.x = surfacePoint.Original[k];
            cp.y = surfacePoint.Smoothed[k];
            bendPoints.push_back(cp);
            bendPointList.push_back(OneChannelLut::CreateLookupTable(bendPoints, number));
        }
    }

//    auto lut = bendPointList[5];
//    std::ofstream fs("c:\\junk\\d.txt");
//    for (auto v : lut)
//    {
//        fs << v << '\n';
//    }

    // Scale the control point positions
    vector<IppiPoint> controlPositions;
    for (auto &p : _surfacePointsOld)
    {
        IppiPoint cp = {.x = p.Point.x, .y = p.Point.y};
        controlPositions.push_back(cp);
    }

    _baseThinPlateSplineSurface.reinitialize(controlPositions, imageSize);

    auto heights = new float[numberControlPoints];
    for (auto i = 0; i < number; i++)
    {
        for (auto k = 0; k < numberControlPoints; k++)
        {
            heights[k] = (float)bendPointList[k][i];
        }

        vector<float>weights(numberControlPoints + 3, 0);
        _baseThinPlateSplineSurface.computeWeights(heights, weights.data(), numberControlPoints);

        _weights.push_back(weights);
    }

    delete[]heights;
}

ThinPlateSplineChannelLut::ThinPlateSplineChannelLut(const vector<ControlPointData> &surfacePoints, int number,
    float dataMax, const IppiSize &imageSize)
{
    _surfacePoints = surfacePoints;
    _numberOfLuts = number;
    _dataMax = dataMax;

    auto numberControlPoints = surfacePoints.size();

    vector < vector < float >> bendPointList;
    for (auto&surfacePoint : surfacePoints)
    {
        auto lut = OneChannelLut::CreateLookupTable(surfacePoint.Lut, number);
        bendPointList.push_back(lut);
    }

    // Scale the control point positions
    vector<IppiPoint>controlPoints;
    for (auto&p : surfacePoints)
    {
        controlPoints.push_back({ int(p.Point.x), int(p.Point.y)});
    }

    _baseThinPlateSplineSurface.reinitialize(controlPoints, imageSize);

    auto heights = new float[numberControlPoints];
    for (auto i = 0; i < number; i++)
    {
        for (auto k = 0; k < numberControlPoints; k++)
        {
            heights[k] = (float)bendPointList[k][i];
        }

        vector<float>weights(numberControlPoints + 3, 0);
        _baseThinPlateSplineSurface.computeWeights(heights, weights.data(), numberControlPoints);

        _weights.push_back(weights);
    }

    delete[]heights;
    TRACE_0(errout << "DONE with weights ");
}

ThinPlateSplineChannelLut::~ThinPlateSplineChannelLut()
{}

// public ThinPlateSplineChannelLut(List<List<TpsPoint>> controlPoints, Size imageSize, int number, int dataMax)
// : this(controlPoints, number, dataMax, imageSize, new Size2f(1, 1))
// {
// }

Ipp32fArray ThinPlateSplineChannelLut::applyLut(Ipp32fArray &mat)
{
    auto dataMax = DataMax();
    auto const depth = mat.getComponents();
    MTIassert(depth == 1);
    Ipp32fArray result(mat.getSize());

    // Ugly code to process rows.  This needs to be optimized
    for (auto r = 0; r < mat.getRows(); r++)
    {
        auto rowPointerIn = mat.getRowPointer(r);
        auto rowPointerOut = result.getRowPointer(r);
        for (auto c = 0; c < mat.getCols(); c++)
        {
            auto v = *rowPointerIn / dataMax;
            auto s = interpolateLut({c, r}, v);
            *rowPointerOut = std::round(s * dataMax);

            rowPointerIn += depth;
            rowPointerOut += depth;
        }
    }

    return result;
}

void ThinPlateSplineChannelLut::applyLutInPlace(Ipp32fArray &mat, int channel)
{
    auto dataMax = 1.0f;
    auto const depth = mat.getComponents();

    // Ugly code to process rows.  This needs to be optimized
    for (auto r = 0; r < mat.getRows(); r++)
    {
        auto rowPointer = mat.getRowPointer(r) + channel;
        for (auto c = 0; c < mat.getCols(); c++)
        {
            auto v = *rowPointer / dataMax;
            auto s = interpolateLut({c, r}, v);
            *rowPointer = s * dataMax /2;  // !!!! XXX DEBUG MODE REMOVE 2
            rowPointer += depth;
        }
    }

    return;
}

float ThinPlateSplineChannelLut::interpolateLut(const IppiPoint &point, float v)
{
    // Because number of input intensity values match the number of LUT values
    // we don't have to do a true interpolation.  This saves a computation at a give value
    int low = (int)std::round(v * (_numberOfLuts - 1));
    if ((low >= (_numberOfLuts - 1)) || (low == 0))
    {
        return v;
    }

    return _baseThinPlateSplineSurface.applyTransformationToPoint(point, _weights[low]);
}
