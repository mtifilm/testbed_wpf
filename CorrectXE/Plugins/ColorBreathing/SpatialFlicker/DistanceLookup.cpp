#include "DistanceLookup.h"

#include <memory>
#include <mutex>          // std::mutex
#include "SysInfo.h"      // for SysInfo::AvailableProcessorCount()
#include "MTIsleep.h"
#include "HRTimer.h"
#include "IpaStripeStream.h"

static vector < std::shared_ptr < DistanceLookup >> _flyweightLookups;
static std::mutex _lockMutex; // mutex for critical section

DistanceLookup::DistanceLookup(IppiSize imageSize)
{
    LookupTable = computeDistanceLookupTable(imageSize);
}

DistanceLookup::~DistanceLookup()
{
    delete[]LookupTable;
}

bool DistanceLookup::areParametersTheSame(const IppiSize &imageSize)
{
    if ((_imageSize.width != imageSize.width) || (_imageSize.height != imageSize.height))
    {
        return false;
    }

    return true;
}

std::shared_ptr<DistanceLookup>DistanceLookup::getDistanceLookup(const IppiSize &imageSize)
{
    std::lock_guard<std::mutex>lock(_lockMutex);

    for (auto distanceLookup : _flyweightLookups)
    {
        if (distanceLookup->areParametersTheSame(imageSize))
        {
            return distanceLookup;
        }
    }

    // We should limit the size
    auto d = std::make_shared<DistanceLookup>(imageSize);
    _flyweightLookups.push_back(d);

    return _flyweightLookups.back();
}

void DistanceLookup::computeInternalLookupTable(const IppiRect &roi, float result[])
{
    // We assume ROI.x = 0, ROI.Width = widthSize
    auto widthSize = int(std::pow(2, _widthBits));
    auto lowK = roi.y * widthSize;
    auto highK = (roi.y + roi.height) * widthSize;

    for (auto k=lowK; k < highK; k++)
    {
       auto dx = k % widthSize;
       auto dy = k / widthSize;
       result[k] = DistanceKernelTPS(dx, dy);
    }
}
//void DistanceLookup::computeInternalLookupTable(const IppiRect &roi, const vector<IppiPoint> &controlPositions, float result[])
//{
//    // This is lockless, which means sometimes there will be collisions
//    // however, the computation is the same so the loss of collisions is
//    // unimportant compared to locking cost
//    for (auto r = roi.y; r < roi.height+roi.y; r++)
//    {
//        for (auto c = roi.x; c < roi.width + roi.x; c++)
//        {
//            for (auto&p : controlPositions)
//            {
//                auto dx = abs(c - p.x);
//                auto dy = abs(r - p.y);
//                auto k = (dy << _widthBits) + dx;
//                ///					auto k = hashKey(dx, dy);
//                if (result[k] < 0)
//                {
//                    result[k] = DistanceKernelTPS(dx, dy);
//                }
//            }
//        }
//    }
//}

float *DistanceLookup::computeDistanceLookupTable(const IppiSize &imageSize)
{
    _imageSize = imageSize;
    _widthBits = (int)std::ceil(std::log2(_imageSize.width));
    auto heightBits = (int)std::ceil(std::log2(_imageSize.height));

    // Wasteful if small width and larger height;
    auto heightSize = (_widthBits >= heightBits) ? _imageSize.height : std::pow(2, heightBits);
    auto widthSize = int(std::pow(2, _widthBits));
    auto total = int(widthSize * heightSize);
    float *result = new float[total];

    int numberOfJobs = SysInfo::AvailableProcessorCount();

    numberOfJobs = std::min<int>(numberOfJobs, 64);
    auto minRowsPerJob = _imageSize.height / numberOfJobs;
    auto extraRows = _imageSize.height % numberOfJobs;
    vector<std::thread> strips;

    IpaStripeStream iss;
    auto f = [&](const MtiRect &jobRoi)
    {
             this->computeInternalLookupTable(jobRoi, result);
    };

    iss << MtiSize(_imageSize) << efu_roi(f);
    iss.stripe();
//    for (auto jobNumber = 0; jobNumber < numberOfJobs; jobNumber++)
//    {
//        // Evenly distribute n extra rows over the first n jobs.
//        auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
//        auto subRoiHeight = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);
//        IppiRect jobRoi =  {.x = 0, .y = jobY, .width = _imageSize.width, .height = subRoiHeight};
//        strips.emplace_back([jobRoi, result, this]()
//           {
//             this->computeInternalLookupTable(jobRoi, result);
//           }
//        );
//    }
//
//	 for (auto jobNumber=0; jobNumber < numberOfJobs; jobNumber++)
//    {
//        strips[jobNumber].joinxx();
//    }


	 return result;
}

//void RunDeltaJob(void *vp, int jobNumber)
//{
//
//
//
//
//    auto jobArray = srcArray.Extract(jobRoi);
//
//    // Do this job!
//    auto processRoi = model->getProcessingRoi();
//    model->getThinPlateSplineDeltaSurface()->applyDeltaSurfaceInPlace(jobArray, 0, processRoi);
//}
//        auto pRoi = _spatialFlickerModel->getProcessingRoi();

//        std:: thread t0([tps0, &fpRoi]() {tps0->applyDeltaSurfaceInPlace(fpRoi, 0);});
//        std:: thread t1([tps1, &fpRoi]() {tps1->applyDeltaSurfaceInPlace(fpRoi, 1);});
//        std:: thread t2([tps2, &fpRoi]() {tps2->applyDeltaSurfaceInPlace(fpRoi, 2);});



