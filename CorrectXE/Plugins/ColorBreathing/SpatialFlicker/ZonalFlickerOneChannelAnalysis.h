#ifndef ZonalFlickerOneChannelAnalysisH
#define ZonalFlickerOneChannelAnalysisH

#include "ZonalParameters.h"
#include "Ippheaders.h"
#include "Array2D.h"

// This processing class is designed to do ONE channel at the frame size; i.e., no proxy image
// However, this is confusing as the zonal parameters have a downsampling and multiple channels
// It is up to the calling programs to set channels to one and downsampling to 1.
//   Sorry for confusion of only a partial refactor 
//     (TO DO: base on deflicker class and no downsamping)
//   Time is more important right now.
//
class ZonalFlickerOneChannelAnalysis final : public ZonalParameters
{
public:
	ZonalFlickerOneChannelAnalysis();
	ZonalFlickerOneChannelAnalysis(const ZonalParameters &zonalParameters) : ZonalParameters(zonalParameters)
	{
		initialize(getFramesToProcess());
	}

	~ZonalFlickerOneChannelAnalysis();

	void setParameters(const ZonalParameters rhs)
	{
		static_cast<ZonalParameters *>(this)->setParameters(rhs);
		initialize(getFramesToProcess());
	}

	// Used for debugging
	void initialize(int totalFrames);

	void preprocess(const Ipp32fArray & frame0, const Ipp32fArray & frame1);

	MtiPlanar<Ipp32fArray> &getDeltaValues()
	{
		return _deltaValues;
	}

	MtiPlanar<Ipp32fArray> smoothing(MtiPlanar<Ipp32fArray>& deltaValues) const;
	MtiPlanar<Ipp32fArray> smoothWithAnchors(const MtiPlanar<Ipp32fArray>& deltaValues,
	                                         const std::vector<int>& anchors, int smoothType) const;
	static void smoothToReference(MtiPlanar<Ipp32fArray>& smoothedValues, int referenceIndex, int sceneIndex);
	void smoothToReference(MtiPlanar<Ipp32fArray>& smoothedValues, int refFrameClipIndex, std::pair<int, int> anchorFrameOffsets);
	MtiPlanar<Ipp32fArray> smoothAll(
         const MtiPlanar<Ipp32fArray> &rawDeltaValues,
         int refFrameClipIndex,
         std::pair<int, int> anchorOffsets,
         int smoothingType);

	std::pair<vector<int>, vector<int>> getControlPositionInReducedFrame() const { return {_ctrlCols, _ctrlRows}; }
   vector<MtiPoint> getControlPositionInOriginalFrame(int downsample, int cOffset, int rOffset);

private:
	Ipp32fArray createNormalFilter();
	void preProcessFrame(const Ipp32fArray & frame0, const Ipp32fArray & frame1);

private:
	int _rrows = 0;
	int _rcols = 0;
	vector<int> _ctrlRows;
	vector<int> _ctrlCols;
	MtiPlanar<Ipp32fArray> _deltaValues;
	MtiPlanar<Ipp32fArray> _helDists;
	Ipp32fArray _normalFilter;
	Array2D<MtiRect> _boxes;
	Array2D<Ipp32fArray> _sqrtd1;
};


#endif // ZonalFlickerAnalysisH
