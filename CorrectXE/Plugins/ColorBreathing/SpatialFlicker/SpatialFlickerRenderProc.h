//---------------------------------------------------------------------------

#ifndef SpatialFlickerRenderProcH
#define SpatialFlickerRenderProcH
//---------------------------------------------------------------------------


#include "SpatialFlickerModel.h"
#include "ZonalFlickerToolParameters.h"

#include "HRTimer.h"
#include "ToolObject.h"

// DO NOT CHANGE THIS UNTIL TOOL IS CHANGED
#define RENDER_MAX_THREAD_COUNT 1

class SpatialFlickerRenderProc : public CToolProcessor
{
public:
   SpatialFlickerRenderProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor);

   virtual ~SpatialFlickerRenderProc();

   CHRTimer HRT;

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

   int GetTotalFrames() {return _outFrameIndex - _inFrameIndex; }
   SpatialFlickerModel *GetSpatialFlickerModel() { return _spatialFlickerModel; }
   int ProcessOneFrame(int bufferIndex);
   int ProcessDeltaFrame(int bufferIndex);
   int ProcessBWDeltaFrame(int bufferIndex);
   int ProcessColorDeltaFrame(int bufferIndex);

   int ProcessDeltaFrameGpu(int bufferIndex);
   int ProcessColorDeltaFrameGpu(int bufferIndex);

private:
   int _inFrameIndex;
   int _outFrameIndex;
   int _renderNextIndex;
   int _iterationCount;
   int _processingThreadCount;
   int _framesPerIter;
   ShortROI _boundingBox;

   ColorBreathingTool *_tool = nullptr;
   SpatialFlickerModel *_spatialFlickerModel = nullptr;
};

#endif
