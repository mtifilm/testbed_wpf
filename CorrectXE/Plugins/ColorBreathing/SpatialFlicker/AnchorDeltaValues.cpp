//---------------------------------------------------------------------------

#pragma hdrstop

#include "AnchorDeltaValues.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void AnchorDeltaValues::clear()
{
   for (auto &deltaValue : headDeltaValues)
   {
   	deltaValue.clear();
   }

   for (auto &deltaValue : tailDeltaValues)
   {
   	deltaValue.clear();
   }
}

bool AnchorDeltaValues::areValuesValid(const ZonalParameters &params, const std::pair<int, int> &anchors, int refFrame, const SceneCut &sceneCut) const
{
	return (refFrame == _refFrame) &&
   		 (anchors == _anchors) &&
          (sceneCut == _sceneCut) &&
          (_params.doProcessingParametersMatch(params));
}

void AnchorDeltaValues::set(const ZonalParameters &params, const std::pair<int, int> &anchors, int refFrame, const SceneCut &sceneCut)
{
   _refFrame = refFrame;
   _anchors = anchors;
   _sceneCut = sceneCut;
   _params = params;
}

// Scene Index is 0 for first frame in cut
SurfaceControlPoints AnchorDeltaValues::findSurfaceControlPoints(int clipIndex, int channel)
{
   SurfaceControlPoints surfaceControlPoints;
   auto cutIndex = clipIndex - _sceneCut.first;

	if (cutIndex < _anchors.first)
	{
      auto av0 = headDeltaValues[channel][cutIndex];
		for (auto i : range(controlPositions.size()))
		{
			auto p = controlPositions[i];
			ControlPoint cp(p, {av0[i]}, {0});
         surfaceControlPoints.push_back(cp);
		}
	}

   // The analysis is left to right also,
   if (_anchors.second > 0)
   {
      auto validFrames = tailDeltaValues[0].size() - 1;
      auto fromEnd = clipIndex - _sceneCut.second;
      if ((fromEnd >= 0) && (fromEnd < _anchors.second))
      {
			auto av0 = tailDeltaValues[channel][validFrames-fromEnd];
			for (auto i : range(controlPositions.size()))
			{
				auto p = controlPositions[i];
				ControlPoint cp(p, {av0[i]}, {0});
				surfaceControlPoints.push_back(cp);
			}
      }
   }

   return surfaceControlPoints;
}

bool AnchorDeltaValues::isFrameInAnchors(int clipIndex)
{
   auto cutIndex = clipIndex - _sceneCut.first;

	if (cutIndex < _anchors.first)
	{
      return true;
	}

   // The analysis is left to right also,
   if (_anchors.second > 0)
   {
      auto fromEnd = clipIndex - _sceneCut.second;
      if ((fromEnd >= 0) && (fromEnd < _anchors.second))
      {
         return true;
      }
   }

   return false;
}

   bool AnchorDeltaValues::operator == (const AnchorDeltaValues &rhs) const
   {
      return areValuesValid(rhs._params, rhs._anchors, rhs._refFrame, rhs._sceneCut);
   }
