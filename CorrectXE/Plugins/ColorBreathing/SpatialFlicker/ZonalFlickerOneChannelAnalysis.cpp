#include "ZonalFlickerOneChannelAnalysis.h"
#include <numeric>
#include "IpaStripeStream.h"
#include "MtiMath.h"
#include "SysInfo.h"

ZonalFlickerOneChannelAnalysis::ZonalFlickerOneChannelAnalysis() {}

ZonalFlickerOneChannelAnalysis::~ZonalFlickerOneChannelAnalysis() {}

void ZonalFlickerOneChannelAnalysis::initialize(int totalFrames)
{
	auto ccols = getHorizontalBlocks();
	auto crows = getVerticalBlocks();
	auto frctOverlap = getOverlap();

	// Boxes will be centered on control points
	auto Rdist = getImageSize().height / float(crows);
	auto Cdist = getImageSize().width / float(ccols);
	auto f = 1 - frctOverlap;
	_rrows = int(std::round((Rdist - f) / (2 * f)));
	_rcols = int(std::round((Cdist - f) / (2 * f)));

	// Build array with the locations of the control points, i.e.the centers of
	// the boxes
	_ctrlRows = MtiMath::linspace(0, getImageSize().height, crows);
	_ctrlCols = MtiMath::linspace(0, getImageSize().width, ccols);

	MtiRect fullRoi =
	{0, 0, getImageSize().width, getImageSize().height};
	_boxes.resize({ccols, crows});
	for (auto r : range(crows))
	{
		auto rCenter = _ctrlRows[r];
		for (auto c : range(ccols))
		{
			auto cCenter = _ctrlCols[c];
			MtiRect box =
			{cCenter - _rcols, rCenter - _rrows, 2 * _rcols + 1, 2 * _rrows + 1, sub2ind(ccols, {c, r}), ccols};

			// & uses the first as tag, ignores the second
			_boxes(r, c) = box; // was (box & fullRoi);
		}
	}

	// "Rendering" will mean adding frame - byframe offsets, which are computed
	// by interpolating offsets located at each of the box centers(aka control
	// points).These offsets will be derived from

	_deltaValues.clear();
	_helDists.clear();

	_normalFilter = createNormalFilter();

	// Hellinger distance is based on the integral of the product of
	// the square roots of two densities :
	_sqrtd1.resize({ccols, crows});
}

void ZonalFlickerOneChannelAnalysis::preprocess(const Ipp32fArray &frame0, const Ipp32fArray &frame1)
{
	auto crows = int(_ctrlRows.size());
	auto ccols = int(_ctrlCols.size());
	if (frame0.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only mono frames can be analyized");
	}

	auto nframes = 2 - 1; // this is the number of NEW frames, since the blocks
	// of frames are overlapped - in particular, the first
	// frame in Frames is a repeat from the previous block
	// (or of the first frame, in the case of the first block)
	// The job is to compute 'DeltaValues'

	auto deltaValues = Ipp32fArray({ccols, crows});

	// Note that this is the LOCAL
	// 'DeltaValues', as in local to the current block of frames

	// Main loop will be over frames, and will be repeated two times.The first
	// ignores the possibility of motion and gives an initial set of
	// DeltaValues, but also measures the extent of the motions for each block
	// and pair of successive frames.
	// The second time through, DeltaValues is modified for those blocks and
	// frame pairs that showed evidence of motion.

	// Motion measurements will be based on the Hellinger distance between
	// the empirical distributions of intensities in blocks in successive frames

	// Initialize the array of Hellinger distances, one distance for
	// every control point and every frame :

	Ipp32fArray helDists({ccols, crows});

	// stuff for computing empirical distribution of intensities in a block :
	auto nbins = getNumberBins();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Remarks on Hellinger Distance

	// I really want the Hellinger distance INDEPENDENT of any
	// overall shifts - so, for example, a single flicker value
	// added to each intensity in one box to creat the other box
	// should give a distance of ZERO.This is accomplished by
	// minimizing distance over all shifts of one density versus
	// the other.In the case of Hellinger distance, this
	// minimization amounts to MAXIMIZING the convolution of the
	// square - root of one of the densities with respect to the
	// other, which is the principal reason for using Hellinger
	// distance in the first place(quick computation with fft's).
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// As usual with filtering, want to handle boundary effects.
	// Here I will use(symmetric) padding, with nbins / 2 on either
	// side:
	// start and end locations of original data in the padded vector

	auto numIntensityLevels = getNumIntensityLevels();

	// The first frame in Frames is a "boundary" frame.Will associate one
	// distance with every new frame(i.e.frames 2:(nframes + 1), meaning the
	// nframe new frames.

	// As initialization for distances, we will need to start with the
	// (estimated)density for the first frame :

	// PCR = int32(CtrlRows + rrows); // row coords in padded image
	// PCC = int32(CtrlCols + rcols); // col coords in padded image
	// density, and its square root, for all boxes in first frame :

	Ipp32fArray fullDensity1;
	Ipp8uArray scratchArray;

	// Each member of _sqrtd1 is an Ipp32fArray that is the SQRT of a histogram
	// for (auto &box : _boxes)
CHRTimer ppf;
//	for (auto r = 0; r < _boxes.rows(); r++)
   auto f = [&](int r)
	{
   	Ipp32fArray fullDensity1;
	   Ipp8uArray scratchArray;
		for (auto c = 0; c < _boxes.columns(); c++)
		{
			auto &box = _boxes(r, c);
			auto boxImage = frame0(box, MtiSelectMode::mirror);
			auto probBoxResult = FastHistogramIpp::computeProbability(boxImage, nbins, -0.5, numIntensityLevels - 0.5f)[0];
			Ipp32fArray probBoxArray({nbins, 1}, probBoxResult.data());

			auto density1 = probBoxArray.convFullReturnCenter(_normalFilter, fullDensity1, scratchArray);
			// (density1 should sum to one)
			_sqrtd1(r, c) = density1.sqrt();
		}
	};

   auto n = std::max<int>(2, SysInfo::AvailableProcessorCount()/3 + 1);
   n = std::min<int>(n, 8);

   IpaStripeStream iss;
   iss << _boxes.rows() << efu_job(f);
   iss.stripe(n);

//DBTRACE(ppf);
	preProcessFrame(frame0, frame1);
//DBTRACE(ppf);
}

Ipp32fArray ZonalFlickerOneChannelAnalysis::createNormalFilter()
{
	auto nbins = getNumberBins();
	auto sigma = getPdfSigma();
	auto filterRad = getFilterRadius();
	auto filterSize = 2 * filterRad + 1;
	auto normalFilter = Ipp32fArray::normalPdf(filterSize, 0, sigma);

	// easier to interpret results(e.g.ensures that
	// Hellinger distance is between 0 and 1)
	auto filtSum = std::accumulate(normalFilter.begin(), normalFilter.end(), 0.0f);
	normalFilter /= filtSum;

	return normalFilter;
}

void ZonalFlickerOneChannelAnalysis::preProcessFrame(const Ipp32fArray & frame0, const Ipp32fArray & frame1)
{
	auto crows = int(_ctrlRows.size());
	auto ccols = int(_ctrlCols.size());
	auto nbins = getNumberBins();

	// prealloc outputs for speed
	Ipp32fArray localDelta1({ccols, crows});
	Ipp32fArray localHelDists({ccols, crows});

	Array2D<Ipp32fArray>sqrtd2(MtiSize(ccols, crows));

	// Hellinger distance is based on the integral of the product of
	// the square roots of two densities :

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// Remarks on Hellinger Distance

	// I really want the Hellinger distance INDEPENDENT of any
	// overall shifts - so, for example, a single flicker value
	// added to each intensity in one box to creat the other box
	// should give a distance of ZERO.This is accomplished by
	// minimizing distance over all shifts of one density versus
	// the other.In the case of Hellinger distance, this
	// minimization amounts to MAXIMIZING the convolution of the
	// square - root of one of the densities with respect to the
	// other, which is the principal reason for using Hellinger
	// distance in the first place
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// As usual with filtering, want to handle boundary effects.
	// Here I will use(symmetric) padding, with nbins / 2 on either
	// side:

	Ipp32fArray padded1(2 * nbins); // padded location of square - root density inf.
	Ipp32fArray padded2(2 * nbins); // same...

	auto start = (nbins / 2) + 1;
	auto finish = start + nbins - 1;

	// start and end locations of original data in the padded vector
	auto numIntensityLevels = getNumIntensityLevels();

	// will estimate densities from a histogram
	// The first frame in Frames is a "boundary" frame.Will associate one
	// distance with every new frame(i.e.frames 2:(nframes + 1), meaning the
	// nframe new frames.

	// As initialization for distances, we will need to start with the
	// (estimated)density for the first frame :
	// (Will include boxes centered
	// on boundaries.Hence, need padding to keep the access to the
	// pixel data as clean as possible.)

	// difference at DIFF(:, : , frame), i.e.between frame
	// and previous frame.
	// The MEDIAN of the values in DIFF will be recorded in DeltaValues.
	// First, pad DIFF(by reflection) so that can get meaningful data at
	// and near the boundaries :

	auto frameDiffs = frame1 - frame0;

	// for (auto r = 0; r < _boxes.rows(); r++)
	auto f = [&](const int r)
	{
		// These must be declared here for multi-thread support.
		vector<Ipp32f>maxValues;
		vector<MtiPoint>indices;
		Ipp32fArray fullDensity1;
		Ipp8uArray scratchArray;

		for (auto c = 0; c < _boxes.columns(); c++)
		{
			auto &box = _boxes(r, c);
			// first, DeltaValues:
			auto boxF = frameDiffs(box, MtiSelectMode::mirror);

			auto median1 = boxF.medianQuick1D();

			localDelta1[box.linearIndex] = median1;
			auto boxG = frame1(box, MtiSelectMode::mirror);
			auto probBoxResult = FastHistogramIpp::computeProbability(boxG, nbins, -0.5, numIntensityLevels - 0.5f)[0];
			Ipp32fArray probBoxArray(nbins, probBoxResult.data());
			probBoxArray.convFullReturnCenter(_normalFilter, fullDensity1, scratchArray); // (density1 should sum to one)

			auto fd = fullDensity1.sqrt();
			sqrtd2(r, c) = fd;
			auto rd = fd.fliplr();

			// Find the maximum dot product
			Ipp32fArray horizontalConvolution;
			rd.convFullReturnCenter(_sqrtd1(r, c), horizontalConvolution, scratchArray);

			std::tie(maxValues, indices) = horizontalConvolution.maxAndIndex();
			localHelDists[box.linearIndex] = std::sqrt(1 - std::min<float>(1, maxValues[0]));
		}
	};

	IpaStripeStream iss;
	iss << _boxes.rows();
	iss << efu_job(f);
   auto n = std::max<int>(2, SysInfo::AvailableProcessorCount()/ 3 + 1);
   n = std::min<int>(n, 8);
	iss.stripe(n);

	// Blow up the boxes by 3; e.g, inflate by 1 in all directory
	for (auto r = 0; r < _boxes.rows(); r++)
	{
		for (auto c = 0; c < _boxes.columns(); c++)
		{
			auto index = MtiPoint(c, r);
			auto box = _boxes[index];
			if (localHelDists[index] > getMotion())
			{
				auto boxF = frameDiffs(box.inflate(_rcols, _rrows), MtiSelectMode::mirror);
				localDelta1[index] = boxF.medianQuick1D();
			}
		}
	}

	// Replace the density values
	_sqrtd1 = sqrtd2;

	// WARNING: the old interface only worked on 16 bits and assumed 16 bits correction
	if (numIntensityLevels != (1 << 16))
	{
		localDelta1 *= (1 << 16) / float(numIntensityLevels);
	}

	_deltaValues.emplace_back(localDelta1);
}

// ORIGINAL smoothing code
Ipp32fArray specSmoothAdaptive(const Ipp32fArray &segment, const float smoothSig, const int smoothRad)
{
	if ((segment.getSize().height != 1) && (segment.getSize().depth != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Invalid planar size");
	}

	auto kernel = Ipp32fArray::normalPdf(2 * smoothRad + 1, 0, smoothSig);
	Ipp32fArray medianSmoothed(segment.getSize());
	const auto n = segment.getSize().width;

	MTIassert(n > 0);
	if (n == 0)
	{
		return Ipp32fArray();
	}

	medianSmoothed[0] = segment[0];
	medianSmoothed[n - 1] = segment[n - 1];

	// Waste of time to do endpoints as they are as above
	for (auto i = 1; i < n - 1; i++)
	{
		auto r = std::min<int>(i, n - 1 - i);
		r = std::min<int>(r, smoothRad);
		medianSmoothed[i] = segment({i - r, 0, 2 * r + 1, 1}).medianQuick1D();
	}

	// A duplicate would probably be faster
	Ipp32fArray smoothed(medianSmoothed.getSize());
	smoothed.zero();
	smoothed[0] = medianSmoothed[0];
	smoothed[n - 1] = medianSmoothed[n - 1];

	for (auto i = 1; i < n - 1; i++)
	{
		auto r = std::min<int>(i, n - 1 - i);
		r = std::min<int>(r, smoothRad);
		MtiRect range =
		{smoothRad - r, 0, 2 * r + 1, 1};
		auto specialKernel = kernel(range).duplicate();
		specialKernel /= float(specialKernel.sum()[0]);
		MtiRect range2 =
		{i - r, 0, 2 * r + 1, 1};

		auto s = specialKernel.toVector()[0];
		auto m = medianSmoothed(range2).toVector()[0];

		smoothed[i] = float((medianSmoothed(range2) * specialKernel).sum()[0]);
	}

	// MatIO::saveListToMatFile("c:\\temp\\medianSmoothed.mat", medianSmoothed, "medianSmoothedC", segment, "segmentC", smoothed, "pdfSmoothed");

	return smoothed;
}

Ipp32fArray specSmoothMirror(const Ipp32fArray &segment, const float smoothSig, const int smoothRad)
{
	if ((segment.getSize().height != 1) && (segment.getSize().depth != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Invalid planar size");
	}

	auto kernel = Ipp32fArray::normalPdf(2 * smoothRad + 1, 0, smoothSig);
	const auto n = segment.getSize().width;

	MTIassert(n > 0);
	if (n == 0)
	{
		return Ipp32fArray();
	}

	// Extend the segment by smoothing radius
	auto indices = MtiPlanar<int>::findPaddingIndices(smoothRad, n, smoothRad);
	auto vectorSegment = segment.fromPixelIndices(indices);
	auto arraySegment = Ipp32fArray(int(vectorSegment.size()), vectorSegment.data());

	Ipp32fArray d;
	Ipp8uArray _scratch;
	auto s = arraySegment.convFullReturnCenter(kernel, d, _scratch);
	auto smoothed = s({smoothRad, 0, segment.getWidth(), 1});

	// auto smoothed = Ipp32fArray(n);
	//
	// for (auto i : range(segment.getWidth()))
	// {
	// MtiRect range2 =
	// {i, 0, 2 * smoothRad + 1, 1};
	// smoothed[i] = float((arraySegment(range2) * kernel).sum()[0]);
	// }

	return smoothed;
}

Ipp32fArray specSmoothReplicate(const Ipp32fArray &segment, const float smoothSig, const int smoothRad)
{
	if ((segment.getSize().height != 1) && (segment.getSize().depth != 1))
	{
		THROW_MTI_RUNTIME_ERROR("Invalid planar size");
	}

	auto kernel = Ipp32fArray::normalPdf(2 * smoothRad + 1, 0, smoothSig);
	const auto n = segment.getSize().width;

	MTIassert(n > 0);
	if (n == 0)
	{
		return Ipp32fArray();
	}

	// Extend the segment by smoothing radius
	// TODO: this is quick and dirty, should be replaced with efficient code of sets and copies
	auto indices = MtiPlanar<int>::findPaddingIndices(smoothRad, n, smoothRad);
	auto s0 = indices[smoothRad];
	auto len = int(indices.size());
	auto s1 = indices[len - 1 - smoothRad];
	for (auto i = 0; i < smoothRad; i++)
	{
		indices[i] = s0;
		indices[len - 1 - i] = s1;
	}

	auto vectorSegment = segment.fromPixelIndices(indices);
	auto arraySegment = Ipp32fArray(int(vectorSegment.size()), vectorSegment.data());

	Ipp32fArray d;
	Ipp8uArray _scratch;
	auto s = arraySegment.convFullReturnCenter(kernel, d, _scratch);
	auto smoothed = s({smoothRad, 0, segment.getWidth(), 1});

	return smoothed;
}

Ipp32fArray specSmooth(const Ipp32fArray &segment, const float smoothSig, const int smoothRad, const int smoothingType)
{
	switch (smoothingType)
	{
		case 0:
		return specSmoothMirror(segment, smoothSig, smoothRad);

		case 1:
		return specSmoothAdaptive(segment, smoothSig, smoothRad);

		case 2:
		return specSmoothReplicate(segment, smoothSig, smoothRad);

		default:
		THROW_MTI_RUNTIME_ERROR("No such smoothing type");
	}
}

MtiPlanar<Ipp32fArray>ZonalFlickerOneChannelAnalysis::smoothing(MtiPlanar<Ipp32fArray> &deltaValues) const
{
	// FreezeFrames, parameters)

	// Since we are assuming, at this point, that all DeltaValues are "real" -
	// all that needs to be done to compute the offsets is to smooth and
	// calculate residuals :

	// sigma for Gaussian smoothing of intensity
	auto smoothSig = getSmoothingSigma();

	// radius for smoothing intensity
	auto smoothRad = getSmoothingRadius();

	// trajectories, both Gaussian and median filters

	auto nFrames = int(deltaValues.size());

	// We are assuming all the same size
	auto crows = deltaValues[0].getHeight();
	auto ccols = deltaValues[0].getWidth();

	auto sumDeltaValues = deltaValues.cumulativeSum();
	// MatIO::saveListToMatFile("c:\\temp\\sumDeltaValues.mat", sumDeltaValues, "sumDeltaValues", extendedSumDeltaValues, "extendedSumDeltaValues");

	MtiPlanar<Ipp32fArray>offsets({ccols, crows, nFrames});

	// % FreezeFrames is a vector(possibly empty) of frame numbers(MATLAB
	// % INDEXING!!!) that will be untouched.Smoothing will only interpolate
	// % between these frames.Boundary effects, at the beginning and end, are
	// % handled by using a reflected padding('symmetric').
	// %
	// % First order of business is to prepare a list of start and finish
	// % locations, which define segments that will be smoothed.

	// Loop over blocks, smoothing the SDV trajectories
	for (auto r = 0; r < crows; r++)
	{
		for (auto c = 1; c < ccols; c++)
		{
			if (r == 9 && c == 9)
			{
				auto a = r * ccols + c;
			}

			auto segment = sumDeltaValues.trace({c, r});
			auto smoothed = specSmooth(segment, smoothSig, smoothRad, getSmoothingType());
			offsets.insertTrace({c, r}, smoothed - segment);
		}
	}
	// end

	// % Smoothing function
	return offsets;
}

MtiPlanar<Ipp32fArray>ZonalFlickerOneChannelAnalysis::smoothWithAnchors(const MtiPlanar<Ipp32fArray> &deltaValues,
	const std::vector<int> &anchors, int smoothType) const
{
	// sigma for Gaussian smoothing of intensity
	auto smoothSig = getSmoothingSigma();

	// radius for smoothing intensity
	auto smoothRad = getSmoothingRadius();

	// Build 'Left_and_Right':

	// nAnchors = numel(Anchors);
	auto nAnchors = int(anchors.size());
	auto nFrames = int(deltaValues.size());

	// TempAnchors = zeros(1, nAnchors + 2);
	vector<int>tempAnchors(nAnchors + 2);

	Ipp16uArray leftAndRight;

	if (!anchors.empty())
	{
		// if Anchors(1)~= 1 % if the first frame is not an anchor
		if (anchors[0] != 0)
		{
			// TempAnchors(1) = 1;   % first frame needs to start the first subseq
			tempAnchors[0] = 0;
			// TempAnchors(2:nAnchors + 1) = Anchors;
			std::copy(anchors.begin(), anchors.end(), tempAnchors.begin() + 1);

			// if Anchors(nAnchors)~= nframes % if the last frame is not an anchor
			if (anchors.back() != (nFrames - 1))
			{
				leftAndRight = Ipp16uArray({nAnchors + 1, 3});
				leftAndRight.zero();

				// TempAnchors(nAnchors + 2) = nframes; % last frame needs to end last
				tempAnchors.back() = nFrames - 1;

				// subsequence
				// Left_and_Right =
				// [TempAnchors(1:nAnchors + 1);
				std::copy(tempAnchors.begin(), tempAnchors.end() - 1, leftAndRight.getRowPointer(0));

				// TempAnchors(2:nAnchors + 2); ...
				std::copy(tempAnchors.begin() + 1, tempAnchors.end(), leftAndRight.getRowPointer(1));

				// [2, ones(1, nAnchors - 1), 3]];
				leftAndRight[{0, 2}] = 1;
				leftAndRight[{nAnchors, 2}] = 2;
				// else % last frame is already an anchor
			}
			else
			{
				leftAndRight = Ipp16uArray({nAnchors, 3});
				leftAndRight.zero();
				// Left_and_Right =
				// [TempAnchors(1:nAnchors); ...
				std::copy(tempAnchors.begin(), tempAnchors.end() - 2, leftAndRight.getRowPointer(0));

				// TempAnchors(2:nAnchors + 1); ...
				std::copy(tempAnchors.begin() + 1, tempAnchors.end() - 1, leftAndRight.getRowPointer(1));

				// [2, ones(1, nAnchors - 1)]];
				leftAndRight[{0, 2}] = 1;
				// end
			}

			// else % first frame is already an anchor
		}
		else // first frame is already an anchor
		{
			// TempAnchors(1:nAnchors) = Anchors;
			std::copy(anchors.begin(), anchors.end(), tempAnchors.begin());

			// if Anchors(nAnchors)~= nframes % if the last frame is not an anchor
			if (anchors.back() != (nFrames - 1))
			{
				// TempAnchors(nAnchors + 1) = nframes; % last frame needs to end last
				tempAnchors[nAnchors] = nFrames - 1;
				// % subsequence
				// Left_and_Right =
				leftAndRight = Ipp16uArray({nAnchors, 3});
				leftAndRight.zero();

				// [TempAnchors(1:nAnchors); ...
				std::copy(tempAnchors.begin(), tempAnchors.end() - 2, leftAndRight.getRowPointer(0));

				// TempAnchors(2:nAnchors + 1); ...
				std::copy(tempAnchors.begin() + 1, tempAnchors.end(), leftAndRight.getRowPointer(1));

				// [ones(1, nAnchors - 1), 3]];
				leftAndRight[{nAnchors - 1, 2}] = 2;

				// else % last frame is already an anchor
			}
			else
			{
				// Left_and_Right =
				leftAndRight = Ipp16uArray({nAnchors - 1, 3});
				leftAndRight.zero();
				// [TempAnchors(1:nAnchors - 1); ...
				std::copy(tempAnchors.begin(), tempAnchors.end() - 1, leftAndRight.getRowPointer(0));

				// TempAnchors(2:nAnchors); ...
				std::copy(tempAnchors.begin() + 1, tempAnchors.end(), leftAndRight.getRowPointer(1));

				// ones(1, nAnchors - 1)];

				// end
			}
			// end
		}
	}

	// % Now loop over blocks.In each block, first smooth the SDV trajectories,
	// % and then loop over the subsequence of 'smoothed', as defined by Anchors,
	// % and adjust them to fit the boundary conditions.

	// SDV = cumsum(DeltaValues, 3);
	auto SDV = deltaValues.cumulativeSum();
	MtiPlanar<Ipp32fArray>offsets(deltaValues.getPlanarSize());

	// segment = zeros(1, size(SDV, 3));
	// vector<float> segment(deltaValues.size());
	auto crows = deltaValues.getPlaneSize().height;
	auto ccols = deltaValues.getPlaneSize().width;

	// for r = 1:crows
	for (auto r : range(crows))
	{
		// for c = 1 : ccols
		for (auto c : range(ccols))
		{
			// segment(:) = SDV(r, c, :); % note that segment(1) = 0
			auto segment = SDV.trace({c, r});
			// smoothed = SpecSmooth(segment, SmoothSig, SmoothRad); % simple symmetric
			auto smoothed = specSmooth(segment, smoothSig, smoothRad, smoothType);
			// % smoothing

			// % Go through the list of subsequences, determine for each one
			// % whether one or both of its boundaries are anchor points, and act on
			// % the subsequence of 'smoothed' accordingly.
			if (!leftAndRight.isEmpty())
			{
				// Osmoothed = smoothed;  % Easier if hang onto Original values as you go.
				auto origSmoothed = smoothed.duplicate();
				// % This way, it doesn't matter if boundaries get reset twice, as
				// % part of the interval to the left and then as part of the one to
				// % the right.

				// nSubSequences = numel(Left_and_Right(1, :));
				auto nSubSequences = leftAndRight.getWidth();

				// for seq = 1:nSubSequences
				for (auto seq : range(nSubSequences))
				{
					// left = Left_and_Right(1, seq);
					auto left = leftAndRight[{seq, 0}];

					// right = Left_and_Right(2, seq);
					auto right = leftAndRight[{seq, 1}];

					// typeFlag = Left_and_Right(3, seq);
					auto typeFlag = leftAndRight[{seq, 2}];

					// if typeFlag == 1 % both left and right are anchors
					if (typeFlag == 0)
					{
						// LinTrend = linspace(0, 1, 1 + right - left) * ...
						// ((segment(right) - segment(left)) - (Osmoothed(right) - Osmoothed(left)));
						auto xValue = MtiMath::linspace(0.0f, 1.0f, 1.0f + right - left);
						auto yValue = (segment[right] - segment[left]) - (origSmoothed[right] - origSmoothed[left]);

						// smoothed(left:right) = Osmoothed(left:right) + ...
						// (segment(left) - Osmoothed(left)) + LinTrend;
						for (auto i = left; i <= right; i++)
						{
							smoothed[i] = origSmoothed[i] + segment[left] - origSmoothed[left] + yValue * xValue[i - left];
						}
						// elseif typeFlag == 2 % only right boundary is an anchor
					}
					else if (typeFlag == 1)
					{
						// smoothed(left:right) = ...
						// (Osmoothed(left:right) - Osmoothed(right)) + segment(right);
						for (auto i = left; i <= right; i++)
						{
							smoothed[i] = origSmoothed[i] - origSmoothed[right] + segment[right];
						}
						// elseif typeFlag == 3 % only left boundary is an anchor
					}
					else if (typeFlag == 2)
					{
						// smoothed(left:right) = ...
						// (Osmoothed(left:right) - Osmoothed(left)) + segment(left);
						for (auto i = left; i <= right; i++)
						{
							smoothed[i] = origSmoothed[i] - origSmoothed[left] + segment[left];
						}
					}
					// end
				}
			}

			offsets.insertTrace({c, r}, smoothed - segment);
		}
		// end
	}

	return offsets;
}

void ZonalFlickerOneChannelAnalysis::smoothToReference(MtiPlanar<Ipp32fArray> &smoothedValues, int referenceIndex,
	int sceneIndex)
{
	referenceIndex = std::max<int>(0, std::min<int>(int(smoothedValues.size()) - 1, referenceIndex));
	sceneIndex = std::max<int>(0, std::min<int>(int(smoothedValues.size()) - 1, sceneIndex));

	// if ((referenceIndex < 0) || (referenceIndex >= int(smoothedValues.size())))
	// {
	// return;
	// }
	//
	// if ((sceneIndex < 0) || (sceneIndex >= int(smoothedValues.size())))
	// {
	// return;
	// }

	// Should we use equality here?
	smoothedValues[sceneIndex] <<= smoothedValues[referenceIndex] - smoothedValues[sceneIndex];
}

void ZonalFlickerOneChannelAnalysis::smoothToReference(MtiPlanar<Ipp32fArray> &smoothedValues, int refFrameClipIndex,
	std::pair<int, int>anchorFrameOffsets)
{
	auto referenceIndex = refFrameClipIndex - getInFrame();
	auto headAnchorOffset = anchorFrameOffsets.first;
	auto tailAnchorOffset = anchorFrameOffsets.second;

	// Do the reference frame
	// NOTE: we assume if ref frame is set, then anchors are not set and vice versa.
	// If that's not true, here we give preference to the anchors, but elsewhere
	// precedence is given to the ref frame.
	if ((headAnchorOffset == -1) && (tailAnchorOffset == -1))
	{
		if ((referenceIndex < 0) || (referenceIndex >= int(smoothedValues.size())))
		{
			TRACE_0(errout << "Reference Index out of bounds, Reference Index: " << referenceIndex);
		}

		for (auto sceneIndex : range(smoothedValues.size()))
		{
			smoothToReference(smoothedValues, referenceIndex, sceneIndex);
		}

		return;
	}

	MTIassert(refFrameClipIndex == -1);

	// Do the reference frame for head
	auto lastSceneIndex = int(smoothedValues.size()) - 1;
	MTIassert(headAnchorOffset <= lastSceneIndex);
	for (auto sceneIndex = 0; sceneIndex < headAnchorOffset; sceneIndex++)
	{
		smoothToReference(smoothedValues, headAnchorOffset, sceneIndex);
	}

	// Do the reference frame for the tail, working backwards from the last scene frame.
	MTIassert(tailAnchorOffset <= lastSceneIndex);
	auto tailAnchorIndex = lastSceneIndex - tailAnchorOffset;
	for (auto sceneIndex = lastSceneIndex; sceneIndex > tailAnchorIndex; --sceneIndex)
	{
		smoothToReference(smoothedValues, tailAnchorIndex, sceneIndex);
	}
}

MtiPlanar<Ipp32fArray>ZonalFlickerOneChannelAnalysis::smoothAll(const MtiPlanar<Ipp32fArray> &rawDeltaValues,
	int refFrameClipIndex, std::pair<int, int>anchorOffsets, int smoothingType)
{
	auto headAnchorOffset = anchorOffsets.first;
	auto tailAnchorOffset = anchorOffsets.second;

	auto totalSceneFrames = getOutFrame() - getInFrame();
	if (totalSceneFrames <= 0)
	{
		throw std::runtime_error("Trying to smooth an empty scene");
	}

	// See if we want to do reference
	// NOTE: we assume if ref frame is set, then anchors are not set and vice versa.
	// If that's not true, here we give preference to the ref frame, but elsewhere
	// precedence is given to the anchors.
	if (refFrameClipIndex >= 0)
	{
		auto desiredReferenceIndex = refFrameClipIndex - getInFrame();
		auto referenceIndex = std::max<int>(0, std::min<int>(totalSceneFrames - 1, desiredReferenceIndex));
		if (referenceIndex != desiredReferenceIndex)
		{
			TRACE_0(errout << "Warning reference index outside of marks");
		}

		MTIassert(totalSceneFrames == rawDeltaValues.size());
		MTIassert(headAnchorOffset <= -1);
		MTIassert(tailAnchorOffset <= -1);

		// Smooth the hell out of it
		auto smoothSig = getSmoothingSigma();
		setSmoothingSigma(50);
		auto anchorSmoothing = smoothWithAnchors(rawDeltaValues, {referenceIndex}, 0);
		setSmoothingSigma(smoothSig);
		return anchorSmoothing;
	}

	// Now we just smooth via anchors
	vector<int>anchors;
	MTIassert(headAnchorOffset < totalSceneFrames);
	if (headAnchorOffset >= 0 && headAnchorOffset < totalSceneFrames)
	{
		anchors.push_back(headAnchorOffset);
	}
	else
	{
		// The header anchoer should NEVER be set after total SceneFrames
		// But we don't want a crash if it is
		headAnchorOffset = -1;
	}

	MTIassert(tailAnchorOffset < totalSceneFrames);
	if (tailAnchorOffset >= 0 && tailAnchorOffset < totalSceneFrames)
	{
		anchors.push_back(totalSceneFrames - 1 - tailAnchorOffset);
	}
	else
	{
		// The tail anchor should NEVER be set before 0
		// But we don't want a crash if it is
		tailAnchorOffset = -1;
	}

	// Larry wants to level out before and after anchors
	auto anchorSmoothed = smoothWithAnchors(rawDeltaValues, anchors, smoothingType);
	if (anchors.size() == 0)
	{
		return anchorSmoothed;
	}

	// save and restore smoothing values
	auto smoothSig = getSmoothingSigma();
	setSmoothingSigma(60);
	auto oversmooth = smoothWithAnchors(rawDeltaValues, anchors, smoothingType);
	setSmoothingSigma(smoothSig);

	for (auto sceneIndex : range(headAnchorOffset))
	{
		anchorSmoothed[sceneIndex] <<= oversmooth[sceneIndex];
	}

	for (auto sceneIndex : range(tailAnchorOffset))
	{
		auto sceneAnchor = totalSceneFrames - 1 - sceneIndex;
		anchorSmoothed[sceneAnchor] <<= oversmooth[sceneAnchor];
	}

	return anchorSmoothed;
}

vector<MtiPoint>ZonalFlickerOneChannelAnalysis::getControlPositionInOriginalFrame(int downsample, int cOffset,
	int rOffset)
{
	std::vector<int>cols;
	std::vector<int>rows;

	std::tie(cols, rows) = getControlPositionInReducedFrame();

	for (auto&c : cols)
	{
		c = int(c * downsample + cOffset);
	}

	for (auto&r : rows)
	{
		r = int(r * downsample + rOffset);
	}

	vector<MtiPoint>result;
	for (auto row : rows)
	{
		for (auto col : cols)
		{
			result.emplace_back(col, row);
		}
	}

	return result;
}
