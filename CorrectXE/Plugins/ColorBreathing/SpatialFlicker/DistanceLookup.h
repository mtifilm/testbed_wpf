// ---------------------------------------------------------------------------

#ifndef DistanceLookupH
#define DistanceLookupH

// ---------------------------------------------------------------------------
#pragma once

#include "IniFile.h"
#include "Ippheaders.h"
#include "Ipp32fArray.h"

// DO NOT CHANGE THIS, this MUST be the same AS GPU code
// and that computes weights
static inline float DistanceKernelTPS(int dx, int dy)
{
    auto r = std::sqrt(dx * dx + dy * dy);
    return r == 0 ? 0 : r * std::log(r);
}

class DistanceLookup
{
public:
    DistanceLookup(IppiSize imageSize);

    virtual ~DistanceLookup();

    static inline unsigned int hashKey(int x, int y, int bits) {return (y << bits) | x;}

    inline unsigned int hashKey(int x, int y) const {return hashKey(x, y, _widthBits);}

    bool areParametersTheSame(const IppiSize &imageSize);
    static std::shared_ptr<DistanceLookup>getDistanceLookup(const IppiSize &imageSize);

    float *LookupTable;

    int getWidthBits() {return _widthBits;}

    void computeInternalLookupTable(const IppiRect &roi, float result[]);
    float *computeDistanceLookupTable(const IppiSize &imageSize);

private:
    int _widthBits;
    IppiSize _imageSize;
};

#endif
