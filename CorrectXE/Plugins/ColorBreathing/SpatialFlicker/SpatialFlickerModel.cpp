// ---------------------------------------------------------------------------

#pragma hdrstop

#include "SpatialFlickerModel.h"
#include "IniFile.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include "MTImalloc.h"
#include <cmath>
#include "MTIsleep.h"
#include "mthread.h"
#include "SysInfo.h"
#include "chrono"
#include "OneChannelLut.h"
#include <thread>
#include <iterator>
#include "DllSupport.h"
#include "HRTimer.h"
#include "err_cb.h"
#include "GpuAccessor.h"

#pragma package(smart_init)

static const SIniAssociation ZonalFlickerProcessData[] =
{ {"FlickerProcessType_Unknown", (int)ZonalProcessType::Unknown},
	{"FlickerProcessType_Intensity", (int)ZonalProcessType::Intensity},
	{"FlickerProcessType_SingleChannel", (int)ZonalProcessType::SingleChannel},
	{"FlickerProcessType_ColorLinear", (int)ZonalProcessType::ColorLinear}, {"", -1}};

CIniAssociations ZonalProcessAssociation(ZonalFlickerProcessData, "FlickerProcessType");

// ---------------------------------------------------------------------------
SpatialFlickerModel::SpatialFlickerModel() {

}

// ---------------------------------------------------------------------------
SpatialFlickerModel::~SpatialFlickerModel()
{
	delete _thinPlateSplineSurface;
	delete _thinPlateSplineColorSurface[0];
	delete _thinPlateSplineColorSurface[1];
	delete _thinPlateSplineColorSurface[2];
	freeCommon();
}

int SpatialFlickerModel::freeCommon() {return 0;}

int SpatialFlickerModel::initCommon()
{
	///    throw runtime_error("not needed");
	// int result = freeCommon();
	// if (result != FLK_RET_SUCCESS)
	// {
	// return result;
	// }
	//
	// auto pitchInPixels = getCols(); // GET RID OF THE 3
	//
	// _ippArrayConvert = new IppArrayConvert(getRoi(), pitchInPixels, getMaxValue(), 1);
	//
	// auto engineArgs = makeEngineArgs();
	// _commonEngine = FlickerCommonAlloc(&engineArgs, &result);
	// if (result != FLK_RET_SUCCESS)
	// {
	// return result;
	// }
	//
	// _flickerMotion = FlickerMotionAlloc(_commonEngine, &result);
	// return result;

	return 0;
}

void SpatialFlickerModel::setPreviewMode(const bool mode) {_previewMode = mode;}

bool SpatialFlickerModel::getPreviewMode() const {return _previewMode;}

int SpatialFlickerModel::motionAnalyzeThreadsafe(int internalIndex) {return 0;}

vector<IppiRect>SpatialFlickerModel::makeTileBoxes(const IppiRect &roi, int horizontalBoxes, int verticalBoxes,
	 double overlap)
{
	vector<IppiRect>result;
	if ((roi.width <= 0) || (roi.height <= 0))
	{
		return result;
	}

	auto hf = (double)(roi.width - 0) / horizontalBoxes;
	auto vf = (double)(roi.height - 0) / verticalBoxes;
	overlap = 0;

	// These are the
	int hp[horizontalBoxes + 1];
	for (auto i = 0; i <= horizontalBoxes; i++)
	{
		hp[i] = (int)std::round(i * hf + roi.x);
	}

	int vp[verticalBoxes + 1];
	for (auto i = 0; i <= verticalBoxes; i++)
	{
		vp[i] = (int)std::round(i * vf + roi.y);
	}

	for (auto r = 0; r < verticalBoxes; r++)
	{
		for (auto c = 0; c < horizontalBoxes; c++)
		{
			auto wo = hp[c + 1] - hp[c];
			auto ho = vp[r + 1] - vp[r];
			auto w = (int)std::round((1 + overlap) * wo);
			auto h = (int)std::round((1 + overlap) * ho);

			if ((hp[c + 1] - w) < roi.x)
			{
				w = wo;
			}

			if ((vp[r + 1] - h) < roi.y)
			{
				h = ho;
			}

			if ((hp[c] + w) >= roi.width + roi.x)
			{
				w = roi.width - hp[c] + roi.x; ;
			}

			if ((vp[r] + h) >= roi.height + roi.y)
			{
				h = roi.height - vp[r] + roi.y;
			}

			result.push_back(IppiRect {.x = hp[c], .y = vp[r], .width = w, .height = h});
		}
	}

	for (auto r = 0; r < verticalBoxes; r++)
	{
		auto rowIndex = r * horizontalBoxes;
		auto rect = result[rowIndex];

		for (auto c = 1; c < horizontalBoxes; c++)
		{
			auto newRect = result[rowIndex + c];

			if (newRect.x != (rect.x + rect.width))
			{
				TRACE_0(errout << "Failure x0=" << rect.x << ", w = " << rect.width << ", x1= " << newRect.x);
			}

			rect = newRect;
		}
	}

   _lastMakeTileBoxes = {horizontalBoxes, verticalBoxes};
	return result;
}

vector<IppiRect>SpatialFlickerModel::makeTileBoxes()
{
	return makeTileBoxes(getAnalysisRoi(), getHorizontalBlocks(), getVerticalBlocks());
}

 bool SpatialFlickerModel::getSurfaceControlPoints(int frameIndex, int channel, SurfaceControlPoints &surfaceControlPoints)
 {
   auto result = getSegmentsDeltas().getPreProcessedSurfaceControlPoints(frameIndex, channel, surfaceControlPoints);
   if (!result)
   {
      DBCOMMENT(" NO DATA VALID");
      return false;
   }

   auto anchorControlPoints = _anchorDeltaValues.findSurfaceControlPoints(frameIndex, channel);

#ifdef _DEBUGXXX
   DBTRACE2(anchorControlPoints.size(), surfaceControlPoints.size());
#endif

	if (anchorControlPoints.size() > 0)
	{
      surfaceControlPoints = anchorControlPoints;
#ifdef _DEBUGXX
    DBCOMMENT("##########################################ANCHOR ")
#endif
	}
   else
   {
#ifdef _DEBUGXX
    DBCOMMENT("##########################################################CORRECT")
#endif
   }
#ifdef _DEBUGXX
   auto rr = 14;
   auto cc = 16;
   for (auto i = 32*rr + cc; i < 32*rr + cc + 10; i++)
   {
      DBTRACE2(i, surfaceControlPoints[i].Point);
      DBTRACE2(surfaceControlPoints[i].Smoothed[0], surfaceControlPoints[i].Original[0]);
   }

   DBTRACE2(frameIndex, surfaceControlPoints.size());
#endif
   return true;
 }

 bool SpatialFlickerModel::applyLutInPlace(int frameIndex, Ipp16uArray &image, ScratchArray &scratch)
 {
//    if (_anchorDeltaValues.isFrameInAnchors(frameIndex) == false)
//    {
//       image.applyLutInPlace({_forwardScaleLut}, _forwardAxisValues, scratch);
//       return false;
//    }
//
//    image.applyLutInPlace({_forwardLut}, _forwardAxisValues, scratch);
    return true;
 }

 bool SpatialFlickerModel::applyLutInPlace(int frameIndex, Ipp16uArray &image)
 {
	 ScratchArray scratch;
    return applyLutInPlace(frameIndex, image, scratch);
 }

 bool SpatialFlickerModel::applyInverseLutInPlace(int frameIndex, Ipp16uArray &image, ScratchArray &scratch)
 {
//    if (_anchorDeltaValues.isFrameInAnchors(frameIndex) == false)
//    {
//       image.applyLutInPlace({_forwardAxisValues}, _forwardScaleLut, scratch);
//       return false;
//    }
//
//    image.applyLutInPlace({_forwardAxisValues}, _forwardLut, scratch);
    return true;
 }

 bool SpatialFlickerModel::applyInverseLutInPlace(int frameIndex, Ipp16uArray &image)
 {
    ScratchArray scratch;
    return applyInverseLutInPlace(frameIndex, image, scratch);
 }

ThinPlateSplineSurface *SpatialFlickerModel::computeDeltaSurface(int frameIndex, int channel, const unsigned char *mask)
{
	delete _thinPlateSplineSurface;
	_thinPlateSplineSurface = nullptr;

	SurfaceControlPoints surfaceControlPoints;
	if (getSurfaceControlPoints(frameIndex, channel, surfaceControlPoints) == false)
	{
		throw std::runtime_error("No data available");
	}

	auto thinPlateSplineSurface = new ThinPlateSplineSurface();
	vector<IppiPoint>controlPoints;
	for (auto&p : surfaceControlPoints)
	{
		controlPoints.push_back(p.Point);
	}

	thinPlateSplineSurface->reinitialize(controlPoints, getImageSize());
	auto numberControlPoints = controlPoints.size();
	vector<float>heights(numberControlPoints);
	for (auto k = 0; k < numberControlPoints; k++)
	{
		heights[k] = surfaceControlPoints[k].Original[0];
	}

	thinPlateSplineSurface->computeWeightsAndSurface(heights, getImageSize(), getAnalysisRoi(), mask);

	_thinPlateSplineSurface = thinPlateSplineSurface;
	return thinPlateSplineSurface;
}

void SpatialFlickerModel::computeColorDeltaSurface(int frameIndex)
{
	for (auto i = 0; i < 3; i++)
	{
		delete _thinPlateSplineColorSurface[i];
		_thinPlateSplineColorSurface[i] = nullptr;

		SurfaceControlPoints surfaceControlPoints;
		if (getSurfaceControlPoints(frameIndex, i, surfaceControlPoints) == false)
		{
			throw std::runtime_error("No data available");
		}

		_thinPlateSplineColorSurface[i] = new ThinPlateSplineSurface();
		vector<IppiPoint>controlPoints;
		for (auto&p : surfaceControlPoints)
		{
			controlPoints.push_back(p.Point);
		}

		_thinPlateSplineColorSurface[i]->reinitialize(controlPoints, getImageSize());

		auto numberControlPoints = controlPoints.size();
		auto heights = new float[numberControlPoints];

		for (auto k = 0; k < numberControlPoints; k++)
		{
			heights[k] = surfaceControlPoints[k].Original[0];
		}

		_thinPlateSplineColorSurface[i]->computeWeights(heights, numberControlPoints);
		delete[]heights;
	}
}

struct ProcessTpsParams
{
	Ipp16uArray srcArray;
	Ipp16uArray dstArray; // Pointer to first destination element
	SpatialFlickerModel *model;
	Ipp16u maxValue; // The maximum source value: must be (power of 2) - 1
	int numberOfJobs; // The total number of jobs for segmentation calculation
};

void RunProcessorJob(void *vp, int jobNumber)
{
	if (vp == nullptr)
	{
		return;
	}

	// Image segmented by blocks of rows.
	auto params = static_cast<ProcessTpsParams*>(vp);
	if (jobNumber == params->numberOfJobs)
	{
		TRACE_0(errout << "Called with wrong jobNumber");
		return;
	}

	auto model = params->model;
	auto srcArray = params->srcArray;

	auto height = srcArray.getRoi().height;
	auto width = srcArray.getRoi().width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = height / params->numberOfJobs;
	auto extraRows = height % params->numberOfJobs;
	auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	auto subRoiHeight = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);
	IppiRect jobRoi =
	{.x = 0, .y = jobY, .width = width, .height = subRoiHeight};
	auto jobArray = srcArray(jobRoi);

	// Do this job!
	DBTRACE("NEVER SHOULD BE CALLED");
	///    model->getBaseThinPlateSplineChannelLut()->applyLutInPlace(jobArray, 0);
}

void RunDeltaJob(void *vp, int jobNumber)
{
	if (vp == nullptr)
	{
		return;
	}

	// Image segmented by blocks of rows.
	auto params = static_cast<ProcessTpsParams*>(vp);
	if (jobNumber >= params->numberOfJobs)
	{
		TRACE_0(errout << "Called with wrong jobNumber");
		return;
	}

	auto model = params->model;
	auto srcArray = params->srcArray;

	auto height = srcArray.getRoi().height;
	auto width = srcArray.getRoi().width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = height / params->numberOfJobs;
	auto extraRows = height % params->numberOfJobs;

	auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	auto subRoiHeight = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);
	IppiRect jobRoi =
	{.x = 0, .y = jobY, .width = width, .height = subRoiHeight};

	auto jobArray = srcArray(jobRoi);

	// Do this job!
	auto processRoi = model->getProcessingRoi();
	model->getThinPlateSplineDeltaSurface()->applyDeltaSurfaceInPlace(jobArray, 0, processRoi, params->maxValue);
}

void SpatialFlickerModel::applyColorDeltaSurfaceInPlace(Ipp16u *data, int jobs) {throw std::runtime_error("Not done");}

void SpatialFlickerModel::applyDeltaSurfaceInPlace(Ipp16uArray &yuvArray, int jobs)
{
	ProcessTpsParams params;

	// Dst is not used here.
	params.model = this;

	params.srcArray = yuvArray;
	params.maxValue = getMaxValue();
	params.numberOfJobs = jobs <= 0 ? SysInfo::AvailableProcessorCount() : jobs;
	if (params.numberOfJobs == 1)
	{
		RunDeltaJob(&params, 0);
		return;
	}

	SynchronousMthreadRunner mthreads(&params, RunDeltaJob);
	mthreads.Run();
}

void SpatialFlickerModel::PreviewFrame16Bit(int frameIndex, int width, int height, int maxPixel,
	 unsigned short *frameBits, const unsigned char *mask)
{
	if (isDataLoaded(frameIndex) == false)
	{
		TRACE_2(errout << "No corrections available");
		return;
	}

	auto imageSize = getImageSize();
	auto rows = imageSize.height;
	auto cols = imageSize.width;

	MTIassert((rows == height) && (cols == width));

   // There is a bug that the parameters are not set with min/max
   // force it here. TODO find out why parameters are wrong
   // MAYBE BECAUSE _processedParameters ISN'T ACTUALLY SET ANYWHERE!!???
  	_processedParameters.setMaxValue(maxPixel);

	// We should just move this into own routine
   Ipp8uArray maskImage;
	GpuAccessor gpuAccessor;
	if (gpuAccessor.useGpu())
	{
		Ipp16uArray imageIn({cols, rows, 3}, frameBits);
		if (mask != nullptr)
		{
			Ipp8uArray newMaskImage({cols, rows}, (Ipp8u *)mask);
			maskImage = newMaskImage;
		}

		try
		{
 //        MatIO::saveListToMatFile("c:\\temp\\test.mat",imageIn, "imageBeforeLut");
			applyLutInPlace(frameIndex, imageIn);
 //           MatIO::saveListToMatFile("c:\\temp\\test.mat",imageIn, "ImageAfterLut");
         applyDeltaSurfaceGpu(imageIn, imageIn, frameIndex, maskImage);
 //           MatIO::appendListToMatFile("c:\\temp\\test.mat",imageIn, "imageAfterDelta");
 		   applyInverseLutInPlace(frameIndex, imageIn);
 //           MatIO::appendListToMatFile("c:\\temp\\test.mat",imageIn, "imageAfterInverse");
		}
      catch (const std::exception ex)
		{
   		CAutoErrorReporter autoErr(__FILE__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
         autoErr.errorCode = -1;
      	autoErr.msg << "GPU ERROR: " << ex.what();
		}
		catch (...)
		{
   		CAutoErrorReporter autoErr(__FILE__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
         autoErr.errorCode = -1;
      	autoErr.msg << "Unknown exception using the GPU ";
		}

		return;
	}

	CHRTimer hrt;

	/// DO THIS PROPERLY
	if (getZonalProcessType() == ZonalProcessType::Intensity)
	{
		try
		{
			hrt.Start();
			Ipp16uArray source({cols, rows, 3}, frameBits);
			auto planes = source.copyToPlanar();
//                                       DBCOMMENT("getZonalProcessType() == ZonalProcessType::Intensity");
			// hrt.Start();
			computeDeltaSurface(frameIndex, 0, mask);
 //             MatIO::saveListToMatFile("c:\\temp\\test.mat",planes[0], "ImageAfterLut");
         applyLutInPlace(frameIndex, planes[0]);
 //            MatIO::appendListToMatFile("c:\\temp\\test.mat",planes[0], "ImageBeforeDelta");
			applyDeltaSurfaceInPlace(planes[0], -1);
 //            MatIO::appendListToMatFile("c:\\temp\\test.mat",planes[0], "ImageAfterDelta");
         MtiPlanar<Ipp16uArray> monoPlanes = {planes[0], planes[0], planes[0]};
         source <<= monoPlanes.toArray();

		}
		catch (const std::exception &ex)
		{
			TRACE_0(errout << "ERROR " << ex.what());
		}
		catch (...)
		{
			TRACE_0(errout << " UNKNOWN ERROR ");
		}

		return;
	}

	try
	{
		hrt.Start();
		Ipp16uArray source({cols, rows, 3}, frameBits);
		auto planes = source.copyToPlanar();

      CHRTimer hrt;

        	ScratchArray scratch;
		   if (isProcessingChannelEnabled(ChannelName::Red))
         {
		    	computeDeltaSurface(frameIndex, 0, mask);
            applyLutInPlace(frameIndex, planes[0], scratch);
		    	applyDeltaSurfaceInPlace(planes[0], -1);
            applyInverseLutInPlace(frameIndex, planes[0], scratch);
			}

		   if (isProcessingChannelEnabled(ChannelName::Green))
      	{
				computeDeltaSurface(frameIndex, 1, mask);
         	applyLutInPlace(frameIndex, planes[1], scratch);
				applyDeltaSurfaceInPlace(planes[1], -1);
         	applyInverseLutInPlace(frameIndex, planes[1], scratch);
			}

      	if (isProcessingChannelEnabled(ChannelName::Blue))
      	{
				computeDeltaSurface(frameIndex, 2, mask);
         	applyLutInPlace(frameIndex, planes[2], scratch);
				applyDeltaSurfaceInPlace(planes[2], -1);
      	   applyInverseLutInPlace(frameIndex, planes[2], scratch);
			}

		source.copyFromPlanar(planes);
	}
	catch (const std::exception &ex)
	{
		TRACE_2(errout << "ERROR " << ex.what());
	}
	catch (...)
	{
		TRACE_2(errout << "Unknown error ERROR ");
	}
}


bool SpatialFlickerModel::isDataLoaded(int frameIndex) {return getSegmentsDeltas().isDataLoaded(frameIndex);}

// -------------------------------------STU TEMP CODE ----------------

bool SpatialFlickerModel::isAnalysisRoiValid() const
{
	if ((getWidth() <= 0) || (getHeight() <= 0))
	{
		return false;
	}

	auto roi = getAnalysisRoi();
	if ((roi.x < 0) || (roi.y < 0))
	{
		return false;
	}

	// TBD: This is ad hoc, we need to figure out the values
	if ((roi.width < 100) || (roi.height < 100))
	{
		return false;
	}

	return true;
}

bool SpatialFlickerModel::needsPreprocessing(const ZonalParameters &rhs, int frameIndex)
{
	MTIassert(frameIndex >= 0);
	if (_preprocessingDone == false)
	{
		return true;
	}

	throw std::runtime_error("Not Implemented");
	///    return !_processedParameters.isPreprocessingValid(rhs);
}

int SpatialFlickerModel::applyDeltaSurfaceGpu(const Ipp16uArray &imageIn, Ipp16uArray &imageOut, int frameIndex, const Ipp8uArray &mask)
{

   // This is a leftover from MatLab, but not setting it gives an error
   _processedParameters.setMaxValue(getMaxValue());

	if (isDataLoaded(frameIndex) == false)
	{
		return CB_REPAIR_DATA_IS_INVALID;
	}

	SurfaceControlPoints surfaceControlPoints;
	if (getSurfaceControlPoints(frameIndex, 0, surfaceControlPoints) == false)
	{
		return CB_REPAIR_DATA_IS_INVALID;
	}

	// We need to get rid of thinPlateSplineSurface for getting weights
	auto thinPlateSplineSurface = new ThinPlateSplineSurface();

	vector<IppiPoint_32f>controlPoints_32f;
	vector<IppiPoint>controlPoints;
	for (auto&p : surfaceControlPoints)
	{
		controlPoints_32f.push_back({Ipp32f(p.Point.x), Ipp32f(p.Point.y)});
		controlPoints.push_back(p.Point);
	}

	try
	{
		thinPlateSplineSurface->reinitialize(controlPoints, getImageSize());
	}
	catch (...)
	{
		DBCOMMENT("GPU ERROR #1") throw;
	}

	std::vector < std::vector < float >> weights;
	for (auto i = 0; i < 3; i++)
	{
		try
		{
			if (getSurfaceControlPoints(frameIndex, i, surfaceControlPoints) == false)
			{
				return CB_REPAIR_DATA_IS_INVALID;
			}
		}
		catch (...)
		{
			DBCOMMENT("GPU ERROR #2") throw;
		}

		vector<float>heights;
		for (auto&p : surfaceControlPoints)
		{
			heights.push_back(p.Original[0]);
		}

		auto numberControlPoints = heights.size();

		// Now find the weights
		vector<float>channelWeights(numberControlPoints + 3);
		try
		{
			thinPlateSplineSurface->computeWeights(heights.data(), channelWeights.data(), numberControlPoints);
		}
		catch (...)
		{
			DBCOMMENT("GPU ERROR #3") throw;
		}

		weights.push_back(channelWeights);

		if (getZonalProcessType() == ZonalProcessType::Intensity)
		{
			// Because this is monochrome, each channel has same weights.
			// The GPU is so fast, it is as fast to recompute each channel than compute and copy
			// One already pushed back
			weights.push_back(channelWeights);
			weights.push_back(channelWeights);

         // We exit and don't do rest of loop
			break;
		}
	}

	// Sync processing for right now
	auto tps = getThinplateSplineSurfaceGpu();
	if (tps == nullptr)
	{
		GpuAccessor gpuAccessor;
		tps = new CudaThinplateSpline16u(gpuAccessor.getDefaultGpu());
		tps->setMinMax(0, _processedParameters.getMaxValue());

      // IF LUT IS APPLIED
    	//tps->setMinMax(0, CONTRAST_FORWARD_MAX);
		setThinplateSplineSurfaceGpu(tps);
	}

   tps->setMinMax(0, _processedParameters.getMaxValue());

   //tps->setMinMax(0, CONTRAST_FORWARD_MAX);
	auto roi = getProcessingRoi();
	// This interface sucks, need to redesign
	// Also the in and output really point to the same array
	tps->setInputRoi(roi);
	tps->setOutputRoi(roi);
   if (mask.isEmpty() == false)
	{
      tps->setMaskArray(mask);
   }
   else
   {
      tps->clearMaskArray();
   }

	auto processingChannels = getZonalProcessType() == ZonalProcessType::Intensity ? 7 : getProcessingChannels();
	try
	{
		tps->warpIntensity(imageIn, imageOut, weights, controlPoints_32f, processingChannels);
	}
	catch (...)
	{
		DBCOMMENT("GPU ERROR#5") throw;
	}

	return 0;
}

void SpatialFlickerModel::setAnchorDeltaValues(const AnchorDeltaValues &anchorDeltaValues, int maxValue)
{
   // Calling set anchor data values
   if (anchorDeltaValues == _anchorDeltaValues)
   {
   	return;
   }

    createContrastLuts(maxValue, CONTRAST_FORWARD_LIMIT, CONTRAST_LUT_SIZE);
	_anchorDeltaValues = anchorDeltaValues;
}

void SpatialFlickerModel::createContrastLuts(int axisMax, int rangeMax, int lutSize)
{
   _forwardLut = Ipp32sArray::logLut(lutSize, rangeMax, CONTRAST_LOG_OFFSET);
   _forwardScaleLut = Ipp32sArray::linspace(0.0f, rangeMax, lutSize);
   _forwardAxisValues = Ipp32sArray::linspace(0.0f, axisMax, lutSize);
}
