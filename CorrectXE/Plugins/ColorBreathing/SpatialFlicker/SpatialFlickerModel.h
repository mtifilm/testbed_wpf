// ---------------------------------------------------------------------------

#ifndef SpatialFlickerModelH
#define SpatialFlickerModelH
// ---------------------------------------------------------------------------

#include "ThinPlateSplineChannelLut.h"
#include "ThinPlateSplineDeltas.h"
#include "IniFile.h"
#include "MTIsleep.h"
#include "IppArrayConvert.h"
#include "PDL.h"
#include <algorithm>
#include "ZonalParameters.h"
#include "ZonalFlickerDB.h"
#include "MtiCudaCoreInterface.h"
#include "AnchorDeltaValues.h"

// This is ugly, as the parameters are slightly different between
// the matlab and us
struct ZonalParamsStruct : DeflickerParamsStruct
{
    SegmentRow segmentRow;
    IppiSize ImageSize;
};

extern CIniAssociations ZonalProcessAssociation;

// The spatial flicker model has two parts, the first is a preprocessor
// the second the actual rendering.

class SpatialFlickerModel : public ZonalParameters
{
public:
    SpatialFlickerModel();
    ~SpatialFlickerModel();

    int motionAnalyzeThreadsafe(int internalIndex);

    int initCommon();
    int freeCommon();

    bool getPreviewMode() const ;
    void setPreviewMode(const bool mode);

    vector<IppiRect>makeTileBoxes(const IppiRect &roi, int horizontalBoxes, int verticalBoxes, double overlap = 0.0);
    vector<IppiRect>makeTileBoxes();

    int getAnalysisChannel() const {return _analysisChannel;}

    void applyLutInPlace(Ipp16uArray &yuvArray, int jobs = -1);
    void PreviewFrame16Bit(int frameIndex, int width, int height, int maxPixel,
        unsigned short *frameBits, const unsigned char *mask);

    // These can be combined one call, not intensity and color
    ThinPlateSplineSurface *computeDeltaSurface(int frameIndex, int channel, const unsigned char *mask);
    void computeColorDeltaSurface(int internalIndex);
//    void applyDeltaSurfaceInPlace(Ipp16uArray &yuvArray, int maxValue, int jobs);
    void applyDeltaSurfaceInPlace(Ipp16uArray &yuvArray, int jobs);
    void applyColorDeltaSurfaceInPlace(Ipp16u *data, int jobs = -1);

    int applyDeltaSurfaceGpu(const Ipp16uArray &imageIn, Ipp16uArray &imageOut, int frameIndex, const Ipp8uArray &maskImage);
    bool applyLutInPlace(int frameIndex, Ipp16uArray &image);
    bool applyInverseLutInPlace(int frameIndex, Ipp16uArray &image);
    bool applyLutInPlace(int frameIndex, Ipp16uArray &image, ScratchArray &scratch);
    bool applyInverseLutInPlace(int frameIndex, Ipp16uArray &image, ScratchArray &scratch);

    // This is just temporary until we know where it goes
    ///ThinPlateSplineDeltaData thinPlateSplineDeltaData;
    SegmentsDeltas &getSegmentsDeltas() { return _segmentsDeltas; }

    ThinPlateSplineChannelLut *getBaseThinPlateSplineChannelLut() const
    {
       return _thinPlateSplineChannelLut;
    }

    ThinPlateSplineSurface *getThinPlateSplineDeltaSurface() const
    {
       return _thinPlateSplineSurface;
    }

    bool isDataLoaded(int frameIndex);

    bool needsPreprocessing(const ZonalParameters &rhs, int frameIndex);

    bool needsSmoothing(const ZonalParameters &rhs)
    {
        if (_preprocessingDone == false)
        {
            return true;
        }

        return !_processedParameters.doSmoothingParametersMatch(rhs);
    }

    bool isAnalysisRoiValid() const;

    ThinPlateSplineSurface *_thinPlateSplineColorSurface[3] =
    {nullptr, nullptr, nullptr};

    ZonalParameters &getModelParameters() const
    {
        auto cThis = const_cast<SpatialFlickerModel*>(this);
        return *reinterpret_cast<ZonalParameters*>(cThis);
    }

    CudaThinplateSpline16u *getThinplateSplineSurfaceGpu() const { return _thinplateSplineSurfaceGpu; }
    void setThinplateSplineSurfaceGpu(CudaThinplateSpline16u *thinplateSplineSurfaceGpu)
    {
       delete _thinplateSplineSurfaceGpu;
       _thinplateSplineSurfaceGpu = thinplateSplineSurfaceGpu;
    }

    AnchorDeltaValues &getAnchorDeltaValues() {return _anchorDeltaValues; }
    void setAnchorDeltaValues(const AnchorDeltaValues &anchorDeltaValues, int maxValue);

    bool needToComputeTileBoxes() const
    {
      return (_lastMakeTileBoxes.first != getHorizontalBlocks()) ||
             (_lastMakeTileBoxes.second != getVerticalBlocks());
    }

    // Ugly contrast luts
    void createContrastLuts(int axisMax, int rangeMax, int lutSize);
    std::pair<Ipp32sArray, Ipp32sArray> getContrastLuts() const { return {_forwardLut, _forwardAxisValues}; }

private:

    void *_commonEngine = nullptr;
    void *_flickerMotion = nullptr;
    std::pair<int, int> _lastMakeTileBoxes = {0,0};

        // I'm not happy with this here
    AnchorDeltaValues _anchorDeltaValues;
    SegmentsDeltas _segmentsDeltas;

    bool _previewMode = false;
    bool _preprocessingDone = false;
    int _analysisChannel = 0;
    IppArrayConvert *_ippArrayConvert = nullptr;

    ThinPlateSplineChannelLut *_thinPlateSplineChannelLut = nullptr;
    ThinPlateSplineSurface *_thinPlateSplineSurface = nullptr;

    ZonalParameters _processedParameters;

    CudaThinplateSpline16u *_thinplateSplineSurfaceGpu = nullptr;

    bool getSurfaceControlPoints(int frameIndex, int channel, SurfaceControlPoints &surfaceControlPoints);

    // Contrast luts
    Ipp32sArray _forwardLut;
    Ipp32sArray _forwardScaleLut;

    Ipp32sArray _forwardAxisValues;
    Ipp32sArray _inverseAxisValues;
};

#endif
