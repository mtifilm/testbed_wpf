// ---------------------------------------------------------------------------

#ifndef ThinPlateSplineSurfaceH
#define ThinPlateSplineSurfaceH
// ---------------------------------------------------------------------------

#include "DistanceLookup.h"
///#include "MatrixMath.h"
#include "UUIDSupport.h"
//#include "DeflickerBaseDll.h"
#include "ZonalFlickerBaseWrapper.h"

struct TpsPoint
{
    IppiPoint Point;

    double Original;
    double Smoothed;
    UUID Guid;

    TpsPoint(int x, int y, UUID uuid, double original, double smoothed)
    {
        Point = {.x = x, .y = y};
        ///IppiPoint_32f(x, y);
        Original = original;
        Smoothed = smoothed;
        Guid = uuid;
    }

    double Delta() {return Smoothed - Original;}

    TpsPoint(float x, float y, UUID uuid, double original) : TpsPoint(x, y, uuid, original, original) {}
};

struct ControlPoint
{
    IppiPoint Point;

    vector<float>Original;
    vector<float>Smoothed;
    UUID Guid;

    ControlPoint(const IppiPoint &point, vector<float>original, vector<float>smoothed)
    {
        MTIassert(original.size() == smoothed.size());
        Point = point;
        Original = original;
        Smoothed = smoothed;
        Guid = UuidSupport::NilUuid;
    }

    ControlPoint(const IppiPoint &point, vector<float>original)
    {
        Point = point;
        Original = original;
        Smoothed = vector<float>(original.size(), 0);
        Guid = UuidSupport::NilUuid;
    }

    vector<float>getDelta()
    {
        vector<float>result(Original.size());

        for (auto i = 0; i < Original.size(); i++)
        {
            result[i] = Smoothed[i] - Original[i];
        }

        return result;
    }
};

typedef vector<ControlPoint>SurfaceControlPoints;

class ThinPlateSplineSurface
{
public:
    /* Constructors */
    ThinPlateSplineSurface()
    {
        _regularizationParameter = 0;
        _tpsComputed = false;
    }

    ThinPlateSplineSurface(double regularizationParameter)
    {
        _regularizationParameter = regularizationParameter;
        _tpsComputed = false;
    }

    /* Destructor */
    virtual ~ThinPlateSplineSurface() {}

    // The main operators
    float applyTransformationToPoint(const IppiPoint &point, const vector<float> &weights);
    void applyDeltaSurfaceInPlace(Ipp16uArray &mat, int channel, const IppiRect &roi, Ipp16u maxValue);
    void applyDeltaSurfaceInPlace(Ipp32fArray &mat, int channel, const IppiRect &roi, Ipp16u maxValue);

    //    void applyDeltaSurfaceInPlaceWithMask(Ipp16uArray &mat, int channel, const Ipp8u *mask, Ipp16u maxValue);
    Ipp32fArray createSurface(const IppiSize &imageSize, const unsigned char *mask);
////    Ipp32fArray createSurfaceCuda(const IppiSize &imageSize);

    void computeWeights(const float *heights, float *weights, int n);
    void computeWeights(const float *heights, int n);
    void computeWeightsAndSurface(vector<float>heights, const IppiSize &imageSize, const IppiRect &roi, const unsigned char *mask);
    int reinitialize(const vector<IppiPoint> &controlPoints, const IppiSize &imageSize);
    void createRoiSurface(const Ipp32fArray &roiSurface);

    static vector<ZonalFlickerBaseWrapper> zonalFlickerBaseWrapper;
    vector <float> getWeights() { return _weights; }

private:
    inline float distance(const IppiPoint &p, const IppiPoint &q);

    float _applyTransformation(const vector<IppiPoint> &shapeRef, const IppiPoint point, const vector<float> &weights);

    bool _tpsComputed;
    double _regularizationParameter;
    vector<float>_weights;
    vector<IppiPoint>_downsampledPositions;
    vector<IppiPoint>_controlPositions;

    // TODO make a shared pointer
    float *_distanceLookupTable = nullptr;
    int _widthBits = 0;

    int _downsample = 16;
    Ipp32fArray _surface;

    vector<float> _LUArray;
    vector<int>_piviot;
};

std::shared_ptr<ThinPlateSplineSurface>createThinPlateSplineSurface(double regularizationParameter = 0);

#endif
