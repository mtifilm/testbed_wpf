// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ZonalFlickerBaseWrapper.h"

//#define DUMPTIMING(A) DBTRACE(A)
#define DUMPTIMING(A)

// ---------------------------------------------------------------------------

void ZonalFlickerBaseWrapper::deflickerBaseStart()
{

CHRTimer hrt;
	_zonalFlickerOneChannelAnalysis = new ZonalFlickerOneChannelAnalysis;
DUMPTIMING(hrt);
}

void ZonalFlickerBaseWrapper::deflickerBaseEnd()
{
CHRTimer hrt;
	delete _zonalFlickerOneChannelAnalysis;
	_zonalFlickerOneChannelAnalysis = nullptr;
DUMPTIMING(hrt);
}

void ZonalFlickerBaseWrapper::deflickerBaseInit(const DeflickerParamsStruct &params)
{
CHRTimer hrt;
	MTIassert(_zonalFlickerOneChannelAnalysis != nullptr);

	// The params reflect a down sample and multiple channels
	auto oneChannelParams = params;
	oneChannelParams.Downsample = 1;
	oneChannelParams.Width = int(oneChannelParams.Width / params.Downsample);
	oneChannelParams.Height = int(oneChannelParams.Height / params.Downsample);

	_zonalFlickerOneChannelAnalysis->setImageSize({int(oneChannelParams.Width), int(oneChannelParams.Height)});
	_totalFrames = int(oneChannelParams.TotalFrames);

	// This interface does one channel at a time.
	_zonalFlickerOneChannelAnalysis->setZonalProcessType(ZonalProcessType::Intensity);

	_zonalFlickerOneChannelAnalysis->setHorizontalBlocks(int(oneChannelParams.ccols));
	_zonalFlickerOneChannelAnalysis->setVerticalBlocks(int(oneChannelParams.crows));
	_zonalFlickerOneChannelAnalysis->setOverlap(float(oneChannelParams.FrctOverlap));
	_zonalFlickerOneChannelAnalysis->setNumberBins(int(oneChannelParams.nbins));
	_zonalFlickerOneChannelAnalysis->setFilterRadius(int(oneChannelParams.FiltRad));

	// MaxFlicker is not used
	_zonalFlickerOneChannelAnalysis->setNumIntensityLevels(int(oneChannelParams.NumIntensityLevels));
	_zonalFlickerOneChannelAnalysis->setMotion(float(oneChannelParams.TMotion));
	_zonalFlickerOneChannelAnalysis->setPdfSigma(float(oneChannelParams.PdfSig));
	_zonalFlickerOneChannelAnalysis->setSmoothingSigma(float(oneChannelParams.SmoothSig));

	_zonalFlickerOneChannelAnalysis->setInFrame(int(oneChannelParams.InFrame));
	_zonalFlickerOneChannelAnalysis->setOutFrame(int(oneChannelParams.OutFrame));

	// Smoothing radius is set to 3.5 * smoothing sigma
	_zonalFlickerOneChannelAnalysis->setDownsample(1);
	_zonalFlickerOneChannelAnalysis->initialize(_totalFrames);

//   _zonalFlickerOneChannelAnalysis->dumpDebugData();

   // We have two different computations for number of rows
   // In theory they are the same
   // Make sure they are the same UGLY
   if (params.crows != _zonalFlickerOneChannelAnalysis->getVerticalBlocks())
   {
      // setVerticalBlocks IS NOW A NOOP, so set them to the vertical blocks
      TRACE_0(errout << "Warning!! expected " << _zonalFlickerOneChannelAnalysis->getVerticalBlocks()
                     << " vertical blocks, asked for  " << params.crows);

      oneChannelParams.crows = _zonalFlickerOneChannelAnalysis->getVerticalBlocks();
   }

	_startOfProcessing = true;
DUMPTIMING(hrt);
}

void

    ZonalFlickerBaseWrapper::deflickerBaseProcessOneFrame(const DeflickerParamsStruct& params,
	 const Ipp16uArray &frameImage)
{
   // The width and height are already downsampled
	MtiSize downSampleSize(_zonalFlickerOneChannelAnalysis->getWidth(), _zonalFlickerOneChannelAnalysis->getHeight());

	auto currentFrame = Ipp32fArray(frameImage).resizeAntiAliasing(downSampleSize);
	auto factor = 1 << (16 - frameImage.getOriginalBits());
	currentFrame *= factor;
	// The first frame uses itself as the previous frame
	if (_startOfProcessing)
	{
		_previousFrame = currentFrame;
		_startOfProcessing = false;
	}

	// Initialize the first call, this initializes the start
  	_zonalFlickerOneChannelAnalysis->preprocess(_previousFrame, currentFrame);
	_previousFrame = currentFrame;
}

void ZonalFlickerBaseWrapper::deflickerBaseProcessOneFrame(const DeflickerParamsStruct& params,
	 const Ipp32fArray &frameImage)
{
CHRTimer hrt;
   // The width and height are already downsampled
	MtiSize downSampleSize(_zonalFlickerOneChannelAnalysis->getWidth(), _zonalFlickerOneChannelAnalysis->getHeight());

	auto currentFrame = frameImage.resizeAntiAliasing(downSampleSize);

	// The first frame uses itself as the previous frame
	if (_startOfProcessing)
	{
		_previousFrame = currentFrame;
		_startOfProcessing = false;
	}

	// Initialize the first call, this initializes the start
  	_zonalFlickerOneChannelAnalysis->preprocess(_previousFrame, currentFrame);
	_previousFrame = currentFrame;
DUMPTIMING(hrt);
}

int ZonalFlickerBaseWrapper::deflickerBaseGetDeltaSize(const DeflickerParamsStruct& params) const
{
CHRTimer hrt;
	MTIassert(_zonalFlickerOneChannelAnalysis != nullptr);

	// Should this return the desired size or the actual size.
	// Old code does the desired size but it really should be actual size
	auto desiredSize = _zonalFlickerOneChannelAnalysis->getHorizontalBlocks()
		 * _zonalFlickerOneChannelAnalysis->getVerticalBlocks() * _zonalFlickerOneChannelAnalysis->getFramesToProcess();
	auto actualSize = _zonalFlickerOneChannelAnalysis->getDeltaValues().getPlanarSize().volume();

//	MTIassert(desiredSize == actualSize);
 DUMPTIMING(hrt);
	return desiredSize;
}

void copyMtiPlanerToMatlabPointer(const MtiPlanar<Ipp32fArray> &planes, float *outputPointer)
{
CHRTimer hrt;
	auto tempPointer = outputPointer;
	for (auto&plane : planes)
	{
		// The old C code assumed row contiguous, IppArrays are column first
		for (auto c = 0; c < plane.getWidth(); c++)
		{
			for (auto r = 0; r < plane.getHeight(); r++)
			{
				*tempPointer++ = plane[{c, r}];
			}
		}
	}
DUMPTIMING(hrt);
}

MtiPlanar<Ipp32fArray>matlabPointerToMtiPlanar(ZonalFlickerOneChannelAnalysis *zonalFlickerOneChannelAnalysis, float *inputPointer)
{
CHRTimer hrt;
	auto rows = zonalFlickerOneChannelAnalysis->getVerticalBlocks();
	auto cols = zonalFlickerOneChannelAnalysis->getHorizontalBlocks();
	auto frames = zonalFlickerOneChannelAnalysis->getFramesToProcess();

	MtiPlanar<Ipp32fArray>planes({cols, rows, frames});

	auto tempPointer = inputPointer;
	for (auto&plane : planes)
	{
		// The old C code assumed row contiguous, IppArrays are column first
		for (auto c = 0; c < plane.getWidth(); c++)
		{
			for (auto r = 0; r < plane.getHeight(); r++)
			{
				plane[{c, r}] = *tempPointer++;
			}
		}
	}
 DUMPTIMING(hrt);
	return planes;
}

void ZonalFlickerBaseWrapper::deflickerBaseGetDeltaValues(const DeflickerParamsStruct& params, float* deltaValues) const
{
CHRTimer hrt;
	MTIassert(_zonalFlickerOneChannelAnalysis != nullptr);
	auto deltaValuePlanes = _zonalFlickerOneChannelAnalysis->getDeltaValues();
	copyMtiPlanerToMatlabPointer(deltaValuePlanes, deltaValues);
DUMPTIMING(hrt);
}

void ZonalFlickerBaseWrapper::deflickerBaseGetSmoothedValues(const DeflickerParamsStruct &params, const vector<int> &anchors, float *offsets, int smoothingType)
{
	auto deltaValues = _zonalFlickerOneChannelAnalysis->getDeltaValues();
	int n = deltaValues.getPlanarSize().volume();
	vector<float>tempDeltaValues(n);

	copyMtiPlanerToMatlabPointer(deltaValues, tempDeltaValues.data());
	deflickerBaseGetSmoothedValues(params, anchors, tempDeltaValues.data(), offsets, smoothingType);
}

void ZonalFlickerBaseWrapper::deflickerBaseGetSmoothedValues(const DeflickerParamsStruct &params, const vector<int> &anchors, float *deltaValues,
	 float *offsets, int smoothingType)
{
	auto localDeltaValues = matlabPointerToMtiPlanar(_zonalFlickerOneChannelAnalysis, deltaValues);
	auto smoothingValues = _zonalFlickerOneChannelAnalysis->smoothWithAnchors(localDeltaValues, anchors, smoothingType);
	copyMtiPlanerToMatlabPointer(smoothingValues, offsets);
}

void ZonalFlickerBaseWrapper::deflickerBaseFindControlPosition(const DeflickerParamsStruct& params, int cOffset,
	 std::vector<int> &cols, int rOffset, std::vector<int> &rows, IppiSize imageSize)
{
CHRTimer hrt;
	std::tie(cols, rows) = _zonalFlickerOneChannelAnalysis->getControlPositionInReducedFrame();

   // Because of roundoff, the position might move slightly larger than the frame
   for (auto &c : cols)
   {
      c = int(c * params.Downsample + cOffset);
      c = std::min<int>(std::max<int>(c, 0), imageSize.width - 1);
   }

   for (auto &r : rows)
   {
      r = int(r * params.Downsample + rOffset);
      r = std::min<int>(std::max<int>(r, 0), imageSize.height - 1);
   }
DUMPTIMING(hrt);
}

//void ZonalFlickerBaseWrapper::deflickerBaseSmoothToReferenceFrame(
//   const DeflickerParamsStruct &params,
//   int refFrameClipIndex,
//   std::pair<int, int> anchorFrameOffsets,
//   float *deltaValues,
//   float *smoothvalues)
//{
//	MtiPlanar<Ipp32fArray> localDeltaValues = matlabPointerToMtiPlanar(_zonalFlickerOneChannelAnalysis, smoothvalues);
//   _zonalFlickerOneChannelAnalysis->smoothToReference(localDeltaValues, refFrameClipIndex, anchorFrameOffsets);
//
//	copyMtiPlanerToMatlabPointer(localDeltaValues, smoothvalues);
//}


void ZonalFlickerBaseWrapper::deflickerBaseSmoothAll(
   const DeflickerParamsStruct &params,
   int refFrameClipIndex,
   std::pair<int, int> anchorFrameOffsets,
   float *deltaValues,
   float *smoothvalues,
   int smoothingType)
{
CHRTimer hrt;
	MtiPlanar<Ipp32fArray> localDeltaValues = matlabPointerToMtiPlanar(_zonalFlickerOneChannelAnalysis, deltaValues);
   auto smoothedDeltaValues = _zonalFlickerOneChannelAnalysis->smoothAll(localDeltaValues, refFrameClipIndex, anchorFrameOffsets, smoothingType);

	copyMtiPlanerToMatlabPointer(smoothedDeltaValues, smoothvalues);
DUMPTIMING(hrt);
}
