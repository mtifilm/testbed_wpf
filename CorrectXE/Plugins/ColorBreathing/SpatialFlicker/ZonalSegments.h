//---------------------------------------------------------------------------

#ifndef ZonalSegmentsH
#define ZonalSegmentsH
//---------------------------------------------------------------------------

#include "SpatialFlickerModel.h"
#include "ZonalFlickerDB.h"

class CZonalSegment
{
public:
    CZonalSegment(const SegmentRow &row);

private:
    SegmentRow _segmentRow;
};
#endif
