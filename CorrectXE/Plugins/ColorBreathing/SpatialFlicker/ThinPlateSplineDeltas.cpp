//---------------------------------------------------------------------------

#pragma hdrstop

#include "ThinPlateSplineDeltas.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include "MTImalloc.h"
#include <cmath>
#include "MTIsleep.h"
#include "mthread.h"
#include "SysInfo.h"
#include "chrono"
#include <iterator>
#include <algorithm>
#include "Ippheaders.h"
#include "DllSupport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//////////////////////////  STU's Data ///////////

SurfaceControlPoints ThinPlateSplineDeltaData::getPreProcessedSurfaceControlPoints(int frameIndex, int channel)
{
    SurfaceControlPoints result;

    auto ccols = _parameters.getHorizontalBlocks();
    auto crows = _parameters.getVerticalBlocks();
    auto boxesPerFrame = ccols * crows;
    auto &deltaValues = _deltaValues[channel];
    auto roi = _parameters.getAnalysisRoi();

    // Matlab dumps row, col, frame in Fortran indexing
    // we do box numbering in C++ indexing
    auto deltaIndex = [crows, ccols](int boxNumber, int f)
    {
        auto c = boxNumber % ccols;
        auto r = boxNumber / ccols;
        return (f * crows * ccols) + (crows * c) + r;
	 };

	 if (deltaValues.empty())
	 {
		throw std::runtime_error("EMPTY DELTAS!");
	 }

    auto mv = _parameters.getNumIntensityLevels() - 1;
    for (auto boxNumber = 0; boxNumber < boxesPerFrame; boxNumber++)
    {
        vector<float>v = {deltaValues[deltaIndex(boxNumber, frameIndex)]/mv};
        result.emplace_back(_boxes[boxNumber], v);
    }

    return result;
}

template<typename Out>
void split(const std::string &s, char delim, Out result)
{
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
        *(result++) = item;
    }
}


bool ThinPlateSplineDeltaData::isDataValid(int frameIndex)
{
    return (_parameters.getInFrame() <= frameIndex) && (frameIndex < _parameters.getOutFrame());
}

 bool ThinPlateSplineDeltaData::loadSegmentData(const string &fileName)
 {
	 ifstream inFile(fileName, ios::in | ios::binary);
	 if (inFile.good() == false)
	 {
		 return false;
	 }

	 try
	 {
		 _parameters = readParameters(inFile);
		 _boxes = readSegmentData(inFile, _parameters, _deltaValues);

		 if ((_parameters.getHeadAnchorOffset() < 0) && (_parameters.getTailAnchorOffset() < 0))
		 {
			 rawAnchorDeltaValues.clear();
			 return true;
		 }

		 // Now do the anchor frames
		 auto anchorFileName = ReplaceFileExt(fileName, ".anchors.mat");
		 rawAnchorDeltaValues = readAnchorDeltaValues(anchorFileName);
		 return true;
	 }
	 catch (const std::exception &ex)
	 {
    	CAutoErrorReporter autoErr("ThinPlateSplineDeltaData::loadSegmentData", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_TRACE_0;
		autoErr.errorCode = -1;
		autoErr.msg << string(ex.what());
		 TRACE_0(errout << "loadSegmentData: " << fileName);
		 return false;
	 }
	 catch (...)
	 {
		 TRACE_0(errout << "Unknown error");
		 return false;
	 }
 }

ZonalParameters ThinPlateSplineDeltaData::readParameters(const string &fileName)
{
    ifstream inFile(fileName, ios::in | ios::binary);
    if (inFile.good() == false)
    {
        throw std::runtime_error(GetSystemMessage(GetLastError()));
    }

    return readParameters(inFile);
}

vector<IppiPoint>ThinPlateSplineDeltaData::readSegmentData(std::ifstream &inFile,
    const ZonalParameters &zonalParameters, vector < vector < float >> &deltaValues)
{
    auto channels = int(zonalParameters.getZonalProcessType());
    deltaValues.clear();

    auto crows = zonalParameters.getVerticalBlocks();
    auto ccols = zonalParameters.getHorizontalBlocks();

    // Read row and column positions then make the control points
    vector<IppiPoint>positions(crows * ccols);
	if (!inFile.read(reinterpret_cast<char *>(positions.data()), crows * ccols*sizeof(IppiPoint)))
    {
        throw std::runtime_error("Could not read control positions");
    }

    // Just make sure we are in range.
    auto imageSize = zonalParameters.getImageSize();
    for (auto &position : positions)
    {
        if (position.y >= imageSize.height) position.y = imageSize.height - 1;
        position.y = std::max<int>(position.y, 0);

        if (position.x >= imageSize.width) position.x = imageSize.width - 1;
        position.x = std::max<int>(position.x, 0);
    }

    auto dataSize = positions.size() * zonalParameters.getFramesToProcess();
    auto dataSizeInBytes = dataSize*sizeof(float);

    for (auto channel = 0; channel < channels; channel++)
    {
        deltaValues.emplace_back(dataSize);
        auto &delta = deltaValues.back();
        auto p0 = inFile.tellg();
		if (!inFile.read(reinterpret_cast<char *>(delta.data()), dataSizeInBytes))
        {
            throw std::runtime_error("Could not read delta values");
        }
    }

    return positions;
}

ZonalParameters ThinPlateSplineDeltaData::readParameters(std::ifstream &inFile)
{
    int32_t version, id;
    ZonalParameters result;

    inFile.seekg(0, inFile.beg);
    inFile.read(reinterpret_cast<char *>(&version), sizeof(version));
    inFile.read(reinterpret_cast<char *>(&id), sizeof(id));

    // This data is fixed
    float data[21];
    if (!inFile.read(reinterpret_cast<char *>(&data), sizeof(data)))
    {
        throw std::runtime_error("Could not read preprocessor header");
    }

    // Create the parameters
    auto i = 0;
    auto InFrameIndex = int(data[i++]);
    auto OutFrameIndex = int(data[i++]);
    auto ProcessType = int(data[i++]);
    auto crows = int(data[i++]);
    auto ccols = int(data[i++]);
    auto FrctOverlap = data[i++];
    auto nbins = int(data[i++]);
    auto FiltRad = int(data[i++]);
    auto MaxFlicker = int(data[i++]);
    auto NumIntensityLevels = int(data[i++]);
    auto TMotion = data[i++];
    auto PdfSig = data[i++];
    auto SmoothRad = data[i++];
    auto SmoothSig = data[i++];
    auto Downsample = int(data[i++]);

    IppiSize ImageSize;
    ImageSize.width = int(data[i++]);
    ImageSize.height = int(data[i++]);

    // ***NO LONGER VALID FOR MATLAB GENERATED FILES because of indexing*****
    IppiRect roi;
    roi.x = int(data[i++]);
    roi.y = int(data[i++]);
    roi.width = int(data[i++]);
    roi.height = int(data[i++]);

    result.setInFrame(InFrameIndex);
    result.setOutFrame(OutFrameIndex);
    result.setZonalProcessType(ZonalProcessType(ProcessType));
    result.setVerticalBlocks(crows);
    result.setHorizontalBlocks(ccols);
    result.setOverlap(FrctOverlap);
    result.setNumberBins(nbins);
    result.setFilterRadius(FiltRad);
    result.setNumIntensityLevels(NumIntensityLevels);
    result.setMotion(TMotion);
    result.setPdfSigma(PdfSig);
    result.setSmoothingSigma(SmoothSig);
    // THIS NEEDS TO BE ADDED result.setSmoothingType(SmoothType);
    result.setDownsample(Downsample);

    result.setImageSize(ImageSize);
    result.setRoi(roi);

    return result;
}


//bool SegmentsDeltaData::isDataLoadedForFrame(int index)
//{
//    // index is absolute frame index, we need relative frame
//    int segmentNumber;
//    int segmentIndex;
//    return makeRelativeIndices(index, segmentNumber, segmentIndex);
//}

bool SegmentsDeltas::isDataLoaded(int index)
{
    // index is absolute frame index, we need relative frame
    int segmentNumber;
    int segmentIndex;
    return makeRelativeIndices(index, segmentNumber, segmentIndex);
}

bool SegmentsDeltas::makeRelativeIndices(int frameIndex, int &segmentNumber, int &segmentIndex)
{
    // find the segment match
    for (auto index = 0; index < this->size(); index++)
    {
        auto &deltaData = (*this)[index];
        auto params = deltaData.getFlickerParameters();
        if ((frameIndex >= params.getInFrame()) && (frameIndex < params.getOutFrame()))
        {
            segmentNumber = index;
            segmentIndex = frameIndex - params.getInFrame();
            return true;
        }
    }

    return false;
}

bool SegmentsDeltas::getPreProcessedSurfaceControlPoints(int frameIndex, int channel, SurfaceControlPoints &surfaceControlPoints)
{
    int segmentNumber;
    int segmentIndex;

    if (makeRelativeIndices(frameIndex, segmentNumber, segmentIndex) == false)
    {
        return false;
    }

    auto &segmentDeltas = at(segmentNumber);
    surfaceControlPoints = segmentDeltas.getPreProcessedSurfaceControlPoints(segmentIndex, channel);

    return true;
}

AnchorDeltaValues ThinPlateSplineDeltaData::readAnchorDeltaValues(const std::string &fileName)
{
	AnchorDeltaValues result;

   result.headDeltaValues[0] = MatIO::readIpp32fPlanar(fileName, "headDeltaValues0");
   result.headDeltaValues[1] = MatIO::readIpp32fPlanar(fileName, "headDeltaValues1");
   result.headDeltaValues[2] = MatIO::readIpp32fPlanar(fileName, "headDeltaValues2");
   result.tailDeltaValues[0] = MatIO::readIpp32fPlanar(fileName, "tailDeltaValues0");
   result.tailDeltaValues[1] = MatIO::readIpp32fPlanar(fileName, "tailDeltaValues1");
   result.tailDeltaValues[2] = MatIO::readIpp32fPlanar(fileName, "tailDeltaValues2");

   // The positions are written as a vector but can be read back as an Ipp32fArray

   auto positionVector = MatIO::readIpp32fArray(fileName,  "controlPositions");
   MTIassert(positionVector.getHeight() == 1);
   auto w = positionVector.getWidth()/2;

   auto p = positionVector.data();
   for (auto r : range(w))
   {
      auto x = int(*p++);
      auto y = int(*p++);
      result.controlPositions.emplace_back(x, y);
   }

	auto sceneCutVector = MatIO::readIpp32fArray(fileName, "sceneCut");
   result.setSceneCut({sceneCutVector[0], sceneCutVector[1]});

   return result;

}
