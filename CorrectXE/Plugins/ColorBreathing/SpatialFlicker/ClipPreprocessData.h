// ---------------------------------------------------------------------------

#ifndef ClipPreprocessDataH
#define ClipPreprocessDataH
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#include "IniFile.h"
#include "ClipAPI.h"
#include "NavigatorTool.h"
#include "ZonalFlickerDB.h"
#include "SpatialFlickerModel.h"
#include "ThinPlateSplineDeltas.h"
#include "UUIDSupport.h"
#include "AnchorDeltaValues.h"

// This is just a way of reducing the complexity of the
// tool.

class ClipPreprocessData
{
public:
    ClipPreprocessData() = default;

    CToolSystemInterface *getSystemAPI() const;
    string getPreprocessDataPath() const ;

    vector<int>getCutsFromClip() const;

    CZonalFlickerDB &getZonalDatabase() const;

    SegmentRow getSegmentRowFromFrameIndex(int frameIndex) const;

    void onNewClip();
    int onNewMarks();
    bool areMarksValid();
    int startFrame();
    int totalFrames();
    void updateSegmentsFromBreaks();
    bool updateRow(const SegmentRow &segmentRow);
    void updateFrameStatus();
    void updateFramesFromSegment(const SegmentRow &row);
    bool isPreprocessingValidForFrame(int frameIndex) const;
    bool isPreprocessingValidOverMarks() const { return isDeltaValidOverMarks() && isSmoothingValidOverMarks(); }
    bool isDeltaValidOverMarks() const { return _isProcessingValidOverMarks.first; }
    bool isSmoothingValidOverMarks() const { return _isProcessingValidOverMarks.second; }
    bool areMarksRendered() const;
    void resetRenderFlagsInMarks();
    bool isPreprocessingDoneForRow(const SegmentRow &row) const;
    bool isSegmentAtFramePreprocessed(int frameIndex) const;
    bool isSegmentAtMarksPreprocessed() const;

    void computeIsDefaultPreprocessingDone();

    string writeControlFile(const ZonalParameters &params);

    bool updatePreprocessDatabase(int markIn, int markOut);

    bool readPreprocessorData(int frameIndex, SegmentsDeltas &segmentDeltas);
    bool readPreprocessorData(int markIn, int markOut, SegmentsDeltas &segmentDeltas);

    void saveDeltaValues(const ZonalParamsStruct &params, const vector<vector<float>>&componentDeltas);
    void saveSmoothedValues(const ZonalParamsStruct &params, const vector<vector<float>>&componentSmoothed);

    bool resmoothPreprocessorData(const ZonalParameters &zonalParams, int refFrameClipIndex, std::pair<int, int> anchorOffsets, int smoothingType);
    vector<vector<float>> loadDeltaValues(const ZonalParamsStruct &zonalParamsStruct);
    vector<ZonalParamsStruct> getZonalParamsStruct(int markIn, int markOut);

    void saveAnchorDeltaValues(const SegmentRow &row, const AnchorDeltaValues &anchorValues, const SceneCut &markCut);

    // PDL
    void WritePDLEntry(CPDLElement &parent);
    int ReadPDLEntry(CPDLEntry &pdlEntry);
    void updateZonalRenderFlag(const vector<int> &cleanFrameIndices, const vector<int> &dirtyFrameIndices);
    void updatePrerenderedFlags();
    void updatePrerenderedFlags(int in, int out);
    void clearPreprocessorDataBetweenMarks(int markIn, int markOut);

    // Public for debugging
    static string generateIniString(const SegmentRow &row, const ZonalParameters &zonalParams);
    map<int64_t, bool>getCacheDataFileExistsIds() const {return _cacheDataFileExistsIds;};

    int reloadDefaultDatabase();
    void clearAllAnalysis();
    void setBetweenMarksToParameters(const ZonalParameters &zonalParams);

private:
    bool doRepairFilesExist(const SegmentRow &row) const;
    bool doRepairFilesExistCached(const SegmentRow &row) const;
    map<int64_t, bool> _cacheDataFileExistsIds;

    CZonalFlickerDB _db;
    string _dataPath;

    std::pair<bool, bool> _isProcessingValidOverMarks{false, false};

    void legalizeMarks(int &markIn, int &markOut) const;

    string getDataFolder() const;
    std::pair<bool, bool> computeIsPreprocessingDone(const ZonalParameters &params, const SegmentRow &row, int markIn, int markOut) const;
    std::pair<bool, bool> computeIsPreprocessingDone(const ZonalParameters &params, const vector<SegmentRow> &rows) const;

    string writeControlFile(const vector<SegmentRow> &rows, int markIn, int MarkOut) const;
    void cleanupFolder();
    static string getSegmentPrefix(const SegmentRow &row);

    // This finds the parameters for a given segment
    ZonalParameters generateParametersForSegment(const SegmentRow &row, int markIn, int markOut) const ;
    void createFrameStatusIfNecessary();
};

#endif
