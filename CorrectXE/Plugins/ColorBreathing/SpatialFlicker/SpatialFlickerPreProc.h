//---------------------------------------------------------------------------

#ifndef SpatialFlickerPreProcH
#define SpatialFlickerPreProcH
//---------------------------------------------------------------------------

#include "SpatialFlickerModel.h"
#include "ZonalFlickerToolParameters.h"

#include "HRTimer.h"
#include "ToolObject.h"

#define PREPROCESS_MAX_THREAD_COUNT 1

class SpatialFlickerPreProc : public CToolProcessor
{
public:
   SpatialFlickerPreProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
      const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor);

   virtual ~SpatialFlickerPreProc();

   CHRTimer HRT;

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   int GetTotalFrames() {return _markOut - _markIn; }
   SpatialFlickerModel *GetSpatialFlickerModel() { return _spatialFlickerModel; }

private:

   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);
   vector<ZonalParamsStruct> getDeflickerParametersSegments();

   int ProcessOneFrame(int bufferIndex);
   int ProcessColorFrame(int bufferIndex);

   int _markIn;
   int _markOut;
   int _iterationCount;
   int _processingThreadCount;
   int _framesPerIter;
   int _targetIndex;
   int _bitsPerPixelComponent = 0;
   ShortROI _boundingBox;
   vector<ZonalParamsStruct> _deflickerParamsStructs;
   ZonalParamsStruct _currentZonalParamsStruct;

   ColorBreathingTool *_tool = nullptr;
   SpatialFlickerModel *_spatialFlickerModel = nullptr;
   vector<int> _processingPlanes;
};
#endif
