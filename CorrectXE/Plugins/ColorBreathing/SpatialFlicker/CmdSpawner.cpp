#include "CmdSpawner.h"
#include <stdexcept>
#include <cstdarg>
#include <thread>
#include <chrono>
#include "IniFile.h"

CmdSpawner::CmdSpawner()
{
	attachStandardIO();
}

CmdSpawner::~CmdSpawner()
{
	doneReading();
	detachStandardIO();

	CloseHandle(_piProcInfo.hProcess);
	CloseHandle(_piProcInfo.hThread);
}

void CmdSpawner::attachStandardIO()
{
	SECURITY_ATTRIBUTES saAttr;

	// Set the bInheritHandle flag so pipe handles are inherited. 

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// Create a pipe for the child process's STDOUT. 

	if (!CreatePipe(&_childStandardOutRead, &_childStandardOutWrite, &saAttr, 0))
	{
		throw std::runtime_error("g_hChildStd_OUT_Rd SetHandleInformation");;
	}

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	if (!SetHandleInformation(_childStandardOutRead, HANDLE_FLAG_INHERIT, 0))
	{
		throw std::runtime_error("Stdout SetHandleInformation");
	}

	//// Create a pipe for the child process's STDIN. 
	//if (!CreatePipe(&g_hChildStd_IN_Rd, &g_hChildStd_IN_Wr, &saAttr, 0))
	//	ErrorExit(TEXT("Stdin CreatePipe"));

	//// Ensure the write handle to the pipe for STDIN is not inherited. 
	//if (!SetHandleInformation(g_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0))
	//	ErrorExit(TEXT("Stdin SetHandleInformation"));

	// Create the child process.
}

void CmdSpawner::detachStandardIO()
{
	CloseHandle(_childStandardOutRead);
	CloseHandle(_childStandardOutWrite);
	CloseHandle(_childStandardInRead);
	CloseHandle(_childStandardInWrite);
}

///void  CmdSpawner::SpawnChildProcess(const std::string &exeName, const std::vector<string> &args)


void  CmdSpawner::spawnChildProcess(const std::string &exeName, const std::string &args)
{
	auto cmdLine = exeName + " \"" + args + "\"";

	auto s = cmdLine.size();
	TCHAR szCmdline[4096];
	if (s >= 4096)
	{
		throw std::runtime_error("Command line too long");
	}

	for (auto i = 0; i < s; i++)
	{
		szCmdline[i] = cmdLine[i];
	}

	szCmdline[s] = 0;

	STARTUPINFO siStartInfo;
	BOOL bSuccess = FALSE;

	// Set up members of the PROCESS_INFORMATION structure. 

	ZeroMemory(&_piProcInfo, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure. 
	// This structure specifies the STDIN and STDOUT handles for redirection.

	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = _childStandardOutWrite;
	siStartInfo.hStdOutput = _childStandardOutWrite;
///	siStartInfo.hStdInput = _childStandardInRead;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

	// Create the child process. 

	bSuccess = CreateProcess(NULL,
		szCmdline,     // command line 
		NULL,          // process security attributes 
		NULL,          // primary thread security attributes 
		TRUE,          // handles are inherited 
		0,             // creation flags 
		NULL,          // use parent's environment 
		NULL,          // use parent's current directory 
		&siStartInfo,  // STARTUPINFO pointer 
		&_piProcInfo);  // receives PROCESS_INFORMATION 

					   // If an error occurs, exit the application. 
	if (!bSuccess)
	{
		throw std::runtime_error("CreateProcess");
	}
	else
	{
		// Close handles to the child process and its primary thread.
		// Some applications might keep these handles to monitor the status
		// of the child process, for example. 
//		CloseHandle(_piProcInfo.hProcess);
//		CloseHandle(_piProcInfo.hThread);
	}
}

void CmdSpawner::startReading()
{
	_readThread = new std::thread(&CmdSpawner::launchRead, this);
	_childTerminated = false;
}

int CmdSpawner::launchRead(CmdSpawner *spawner)
{
	return spawner->runRead();
}

bool CmdSpawner::isQueueEmpty()
{
	std::lock_guard<std::mutex> guard(_lockMutex);
	return _queue.empty();
}

CmdSpawnerStatus CmdSpawner::wait(int milliseconds)
{
	// if no data and child terminated
	if (isQueueEmpty())
	{
		if (_childTerminated)
		{
			return CmdSpawnerStatus::Terminated;
		}
	}

	// Do a stupid loop, we should do an event flag 
	auto startTime = std::chrono::system_clock::now();
	auto endTime = std::chrono::system_clock::now();
	auto time = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count();
	do
	{
		if (isQueueEmpty() == false)
		{
			return CmdSpawnerStatus::DataAvailable;
		}

		// Check if we have terminated
		if (WaitForSingleObject(_piProcInfo.hProcess, 0) == WAIT_OBJECT_0)
		{
			_childTerminated = true;
		}

		if (_childTerminated)
		{
			return CmdSpawnerStatus::Terminated;
		}

	  endTime = std::chrono::system_clock::now();
	  time = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count();
	  Sleep(100);

	} while (time <= milliseconds);
	
	return CmdSpawnerStatus::NoDataAvailable;
}

int CmdSpawner::runRead()
{
	std::string line;
	while (readLine(line))
	{
		_lockMutex.lock();
		_queue.push(line);
		_lockMutex.unlock();
	}

	_childTerminated = true;
	return 0;
}

bool CmdSpawner::getNextLine(std::string &line)
{
	std::lock_guard<std::mutex> guard(_lockMutex);
	if (_queue.empty())
	{
		return false;
	}

	line = _queue.front();
	_queue.pop();

	return true;
}

bool CmdSpawner::readLine(std::string &line)

// Read output from the child process's pipe for STDOUT
// and write to the parent process's pipe for STDOUT. 
// Stop when there is no more data. 
{
#define BUFSIZE 4096
#define EOT 4
#define EOM 25
	DWORD dwRead, dwWritten;
	CHAR chBuf[BUFSIZE];
	BOOL bSuccess = FALSE;

	auto i = 0;
	CHAR input;
	while (true)
	{
		PeekNamedPipe(_childStandardOutRead, nullptr, 0, nullptr, &dwRead, nullptr);
		
		// see if child process is still running
		if (WaitForSingleObject(_piProcInfo.hProcess, 0) == WAIT_OBJECT_0)
		{
			line = "";
			return false;
		}

		if (dwRead > 0)
		{
			bSuccess = ReadFile(_childStandardOutRead, &input, 1, &dwRead, nullptr);
			if (!bSuccess || (input == EOT) || (input == EOM))
			{
				line = "";
				return false;
			}

			if (input == 10)
			{
				break;
			}

			chBuf[i] = input;
			i++;
		}
	}

  chBuf[i] = 0;
  line = std::string(chBuf);
  return true;
}
