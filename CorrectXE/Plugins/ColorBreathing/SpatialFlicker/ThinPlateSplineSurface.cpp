#include "ThinPlateSplineSurface.h"
#include "MTIsleep.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "Ippheaders.h"
#include "OneChannelLut.h"
#include "HRTimer.h"
#include "MtiMkl.h"
#include "TpssDLL.h"
//#include "GpuAccessor.h"
#include "mthread.h"
#include "ThreadLocker.h"

// Since the matrix inverse is almost always the same, we don't have
// to recompute it.

static vector<IppiPoint> g_controlPositions;
static vector<float> g_LUArray;
static vector<int> g_piviot;
CSpinLock g_lock;

vector<ZonalFlickerBaseWrapper> ThinPlateSplineSurface::zonalFlickerBaseWrapper(3);

inline float ThinPlateSplineSurface::distance(const IppiPoint &p, const IppiPoint &q)
{
    int dx = abs(q.x - p.x);
    int dy = abs(q.y - p.y);
    int k = DistanceLookup::hashKey(dx, dy, _widthBits);
    return _distanceLookupTable[k];
}

inline bool inSide(int ar, int ac, const IppiRect &roi)
{
    if (ar < roi.y)
    {
        return false;
    }

    if (ac < roi.x)
    {
        return false;
    }

    if (ar >= (roi.y + roi.height))
    {
        return false;
    }

    if (ac >= (roi.x + roi.width))
    {
        return false;
    }

    return true;
}

void ThinPlateSplineSurface::applyDeltaSurfaceInPlace(Ipp16uArray &mat, int channel, const IppiRect &pRoi, Ipp16u maxValue)
{
	auto surface = _surface(mat.getRoi());
    auto const depth = mat.getComponents();

    // This is NOT the way to do it but for now we need to get it done
    // The surface and and mat, using the ROI should be put around
    auto roi = mat.getRoi();
    for (auto r = 0; r < mat.getRows(); r++)
    {
        auto rowPointer = mat.getRowPointer(r) + channel;
        auto surfacePointer = surface.getRowPointer(r);
          //             DBTRACE2(*rowPointer, int(*surfacePointer*maxValue));
        auto ar = r + roi.y;
        for (auto c = 0; c < mat.getCols(); c++)
        {
//            if (c == 1000 && r == 50)
//            {
//               DBTRACE2(ar, *surfacePointer);
//               DBTRACE2(ar, *rowPointer);
//               DBTRACE2(ar, *surfacePointer * maxValue);
//            }

            auto ac = c + roi.x;
            auto v = *rowPointer;
            auto s = *surfacePointer++;
            auto cs = (v + maxValue*s);
            if (cs < 0)
                cs = 0;
            if (cs > maxValue)
                cs = maxValue;
            if (inSide(ar, ac, pRoi))
            {
#ifdef _DEBUG
                *rowPointer = cs;// / 1.5f;   // DOES NOT OBEY MASK!!!!!!
#else
                *rowPointer = cs;
#endif
            }

            rowPointer += depth;
        }
    }
}

void ThinPlateSplineSurface::applyDeltaSurfaceInPlace(Ipp32fArray &mat, int channel, const IppiRect &pRoi, Ipp16u maxValue)
{
	auto surface = _surface(mat.getRoi());
    auto const depth = mat.getComponents();

    // This is NOT the way to do it but for now we need to get it done
    // The surface and and mat, using the ROI should be put around
    auto roi = mat.getRoi();
    for (auto r = 0; r < mat.getRows(); r++)
    {
        auto rowPointer = mat.getRowPointer(r) + channel;
        auto surfacePointer = surface.getRowPointer(r);
        auto ar = r + roi.y;
        for (auto c = 0; c < mat.getCols(); c++)
        {
            auto ac = c + roi.x;

//            if (c == 500 && r == 200)
//            {
//               DBTRACE2(*rowPointer, *surfacePointer);
//            }

            auto v = *rowPointer;
            auto s = *surfacePointer++;
            auto cs = (v + maxValue*s);
            if (cs < 0)
                cs = 0;
            if (cs > maxValue)
                cs = maxValue;
            if (inSide(ar, ac, pRoi))
            {
#ifdef _DEBUG
                *rowPointer = cs;// / 1.5f;   // DOES NOT OBEY MASK!!!!!!
#else
                *rowPointer = cs;
#endif
            }

            rowPointer += depth;
        }
    }
}

//void ThinPlateSplineSurface::applyDeltaSurfaceInPlaceWithMask(Ipp16uArray &mat, int channel, const Ipp8u *mask, Ipp16u maxValue)
//{
//	auto surface = _surface(mat.getRoi());
//	auto const depth = mat.getComponents();
//
//    // This is NOT the way to do it but for now we need to get it done
//    // The surface and and mat, using the ROI should be put around
//    auto roi = mat.getRoi();
//    for (auto r = 0; r < mat.getRows(); r++)
//    {
//        auto rowPointer = mat.getRowPointer(r) + channel;
//        auto surfacePointer = surface.getRowPointer(r);
//        auto ar = r + roi.y;
//        float m = 255.0f;
//        for (auto c = 0; c < mat.getCols(); c++)
//        {
//            auto ac = c + roi.x;
//            auto v = *rowPointer;
//            auto s = *surfacePointer++;
//
//            if (mask != nullptr)
//            {
//               m = mask[ar * roi.width + ac];
//            }
//
//			if (m != 0)
//			{
//				auto f = 1 - (m / 255.0f);
//				auto cs = (v + maxValue * s);
//				if (cs < 0)
//					cs = 0;
//				if (cs > maxValue)
//					cs = maxValue;
//
//				*rowPointer = f * cs;
//			}
//
//			rowPointer += depth;
//		}
//    }
//}


float ThinPlateSplineSurface::_applyTransformation(const vector<IppiPoint> &controlPositions, const IppiPoint point,
    const vector<float> &weights)
{
    float a1 = weights[weights.size() - 3];
    float ax = weights[weights.size() - 2];
    float ay = weights[weights.size() - 1];

    auto r = point.y;
    auto c = point.x;

    float affine = a1 + ax * c + ay * r;
    float nonrigid = 0;

    int len = controlPositions.size();
//    float dArray[len];  // BROKEN IN C++Builder 10.2 - NOT THREADSAFE!
    auto dArray = std::make_unique<float[]>(len);

    for (int j = 0; j < len; j++)
    {
        dArray[j] = distance(controlPositions[j], {c, r});
    }

    // Most of the cost is in the distance function, so this does little
    IppThrowOnError(ippsDotProd_32f(_weights.data(), dArray.get(), len, &nonrigid));

    return affine + nonrigid;
}

void ThinPlateSplineSurface::createRoiSurface(const Ipp32fArray &roiSurface)
{
    const int rowsDown = _downsample;
    const int colsDown = _downsample;

    float a1 = _weights[_weights.size() - 3];
    float ax = _weights[_weights.size() - 2];
    float ay = _weights[_weights.size() - 1];

    int len = _controlPositions.size();
//    float dArray[len];  // BROKEN IN C++Builder 10.2 - NOT THREADSAFE!
    auto dArray = std::make_unique<float[]>(len);

    // We are making an assumption that roiSurface is a subsurface
    auto roi = roiSurface.getRoi();
    // ar, ac are actual positions in the image
    for (auto r = 0; r < roiSurface.getRows(); r++)
    {
        auto ar = rowsDown * (r + roi.y);
        auto outPointer = roiSurface.getRowPointer(r);

        for (auto c = 0; c < roiSurface.getCols(); c++)
        {
            auto ac = colsDown * c;

            bool print = false; // (ar == 160) && (ac == 800);

            float affine = a1 + ax * ac + ay * ar;
            if (print)
            {
               TRACE_0(errout << "a: " << affine << "(" << ac << "," << ar << ")");
               TRACE_0(errout << a1 << ", " << ax << ", " << ay);
            }

            float nonrigid = 0;
            for (int j = 0; j < len; j++)
            {
                dArray[j] = distance(_controlPositions[j], {ac, ar});
            }

            // Most of the cost is in the distance function, so this does little
            IppThrowOnError(ippsDotProd_32f(_weights.data(), dArray.get(), len, &nonrigid));

            if (print)
            {
                   TRACE_0(errout << "Affine: " << affine << ", nr: " << nonrigid << "a+nr: " << affine + nonrigid << ", mv*s: " << 1023*(affine + nonrigid));
            }

            *outPointer++ = affine + nonrigid;
        }
    }
}

struct SurfaceParamsForCreate
{
    Ipp32fArray surface;
    ThinPlateSplineSurface *tps;
    int numberOfJobs;
};

void RunSurfaceJob(void *vp, int jobNumber)
{
    if (vp == nullptr)
    {
        return;
    }

   /// MTImillisleep(jobNumber *20);
    // Image segmented by blocks of rows.
    auto params = static_cast<SurfaceParamsForCreate*>(vp);

    auto surface = params->surface;
    auto height = surface.getRoi().height;
    auto width = surface.getRoi().width;

    // Evenly distribute n extra rows over the first n jobs.
    auto minRowsPerJob = height / params->numberOfJobs;
    auto extraRows = height % params->numberOfJobs;

    auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
    auto subRoiHeight = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);
    IppiRect jobRoi =
    {.x = 0, .y = jobY, .width = width, .height = subRoiHeight};

    auto jobArray = surface(jobRoi);
    params->tps->createRoiSurface(jobArray);
}

Ipp32fArray ThinPlateSplineSurface::createSurface(const IppiSize &imageSize, const unsigned char *mask)
{
	const int rowsDown = _downsample;
	const int colsDown = _downsample;

	Ipp32fArray surface({imageSize.width / colsDown, imageSize.height / rowsDown, 1});

    SurfaceParamsForCreate surfaceParamsForCreate =
    {surface, this, 0};

    surfaceParamsForCreate.numberOfJobs = 8;

    if (surfaceParamsForCreate.numberOfJobs == 1)
    {
        RunSurfaceJob(&surfaceParamsForCreate, 0);
    }
    else
    {
        SynchronousMthreadRunner mthreads(surfaceParamsForCreate.numberOfJobs, &surfaceParamsForCreate, RunSurfaceJob);
        auto error = mthreads.Run();
        if (error != 0)
        {
            TRACE_0(errout << "Error starting threads: " << error);
        }
    }

    if ((1 != rowsDown) || (colsDown != 1))
    {
        surface = surface.resize(imageSize);
    }

	 if (mask != nullptr)
	 {
		 for (auto r = 0; r < imageSize.height; r++)
		 {
			 auto rp = surface.getRowPointer(r);
			 auto mp = mask + r * imageSize.width;
			 for (auto c = 0; c < imageSize.width; c++)
			 {
				 auto m = *mp++;
				 if (m == 0)
				 {
					 *rp = 0;
				 }
				 else if (m != 255)
				 {
					 *rp = (*rp) * (m / 255.0f);
				 }

				 rp++;
			 }
		 }
	 }

    return surface;
}

//Ipp32fArray ThinPlateSplineSurface::createSurface(const IppiSize &imageSize)
//{
//    const int rowsDown = _downsample;
//    const int colsDown = _downsample;
//    Ipp32fArray surface(imageSize.height/rowsDown, imageSize.width/colsDown, 1);
//
//    float a1 = _weights[_weights.size() - 3];
//    float ax = _weights[_weights.size() - 2];
//    float ay = _weights[_weights.size() - 1];
//
//    int len = _controlPositions.size();
//    //float dArray[len];  // BROKEN IN C++Builder 10.2 - NOT THREADSAFE!
//    auto dArray = std::make_unique<float[]>(len);
//    auto owidth = imageSize.width;
//    auto oheight = imageSize.height;
//
//    // ar, ac are actual positions in the image
//    for (auto r = 0; r < surface.getRows(); r++)
//    {
//        auto ar = rowsDown * r;
//        auto outPointer = surface.getRowPointer(r);
//
//        for (auto c = 0; c < surface.getCols(); c++)
//        {
//            auto ac = colsDown * c;
//
//            float affine = a1 + ax * ac + ay * ar;
//            float nonrigid = 0;
//            for (int j = 0; j < len; j++)
//            {
//                dArray[j] = distance(_controlPositions[j], {ac, ar});
//            }
//
//            // Most of the cost is in the distance function, so this does little
//            IppThrowOnError(ippsDotProd_32f(_weights.data(), dArray, len, &nonrigid));
//
//            *outPointer++ = affine + nonrigid;
//        }
//    }
//
//    if ((1 == rowsDown) && (colsDown == 1))
//    {
//        return surface;
//    }
//
//    return surface.resize(imageSize);
//}

float ThinPlateSplineSurface::applyTransformationToPoint(const IppiPoint &point, const vector<float> &weights)
{
    MTIassert(_tpsComputed);

    // Apply transformation in the complete set of points
    // Assambling output
    return _applyTransformation(_downsampledPositions, point, weights);
}

// string dumpMatrix(string desc, int m, int n, double* a, int lda)
// {
// int i, j;
// ostringstream os;
// os << desc << endl;
// os << "{";
// for (i = 0; i < m; i++)
// {
// for (j = 0; j < n; j++)
// {
// os << a[i + j*lda] << ", ";
// }
// os << endl;
// }
// os << "}";
// }

int ThinPlateSplineSurface::reinitialize(const vector<IppiPoint> &controlPoints, const IppiSize &imageSize)
{
    CAutoSpinLocker lock(g_lock);

    auto distanceLookup = DistanceLookup::getDistanceLookup(imageSize);
    _distanceLookupTable = distanceLookup->LookupTable;
    _widthBits = distanceLookup->getWidthBits();

    // see if caching is done
    if (controlPoints.size() == g_controlPositions.size())
    {
        // We can copy, in reality we should have a singleton
        // but historical reason it was done this way
        _controlPositions = g_controlPositions;
        _LUArray = g_LUArray;
        _piviot = g_piviot;
        _tpsComputed = true;
        return 1;
    }

    // the caching is not done
    _controlPositions = controlPoints;

    // Building the matrices for solving the L*(w|a)=(v|0) problem with L={[K|P];[P'|0]}
    // Building K and P (Need to build L)
    auto n = (int)controlPoints.size();
	 auto lSize = n + 3;
	 vector<float>matL2(lSize*lSize);

	 auto sub2ind = [lSize](int i, int j) { return i*lSize + j; };

    for (auto i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == j)
				{
					 matL2[sub2ind(i,j)] = _regularizationParameter;
            }
            else
            {
					 auto d = distance(_controlPositions[i], _controlPositions[j]);
					 matL2[sub2ind(i,j)] = (float)d;
            }
		  }

		  matL2[sub2ind(i, n + 0)] = 1;
		  matL2[sub2ind(i, n + 1)] = _controlPositions[i].x;
		  matL2[sub2ind(i, n + 2)] = _controlPositions[i].y;

		  matL2[sub2ind(n + 0, i)] = matL2[sub2ind(i, n + 0)];
		  matL2[sub2ind(n + 1, i)] = matL2[sub2ind(i, n + 1)];
		  matL2[sub2ind(n + 2, i)] = matL2[sub2ind(i, n + 2)];
	 }

    for (auto i = 0; i < 3; i++)
	 {
		  matL2[sub2ind(n + 0, n + i)] = 0;
		  matL2[sub2ind(n + 1, n + i)] = 0;
		  matL2[sub2ind(n + 2, n + i)] = 0;
    }

	 vector<int>ipiv2(lSize);
	 auto info2 = MtiMkl::ludcmp(lSize, matL2.data(), lSize, ipiv2.data());
	 _LUArray = matL2;

	 _piviot = ipiv2;
    _tpsComputed = true;

    // Cache the data
	 g_LUArray = _LUArray;
    g_piviot = _piviot;
    g_controlPositions = _controlPositions;

    return 1;
}

void ThinPlateSplineSurface::computeWeights(const float *heights, int n)
{
    _weights.resize(n + 3);
    computeWeights(heights, _weights.data(), n);
}


void ThinPlateSplineSurface::computeWeightsAndSurface(vector<float> heights, const IppiSize &imageSize, const IppiRect &roi, const unsigned char *mask)
{
//CHRTimer hrt;
//    GpuAccessor gpuAccessor;
    computeWeights(heights.data(), heights.size());
    _surface = createSurface(imageSize, mask);
}

void ThinPlateSplineSurface::computeWeights(const float *heights, float *weights, int n)
{
    MTIassert(_tpsComputed);

    auto lSize = n + 3;
    float b[lSize];
    for (auto i = 0; i < n; i++)
    {
        b[i] = heights[i];
    }

    b[n + 0] = 0;
    b[n + 1] = 0;
    b[n + 2] = 0;
    MtiMkl::lubksb(_LUArray.data(), lSize, _piviot.data(), b);
    for (auto i = 0; i < lSize; i++)
    {
        weights[i] = b[i];
    }
}

std::shared_ptr<ThinPlateSplineSurface>createThinPlateSplineSurface(double regularizationParameter) {
    return std::shared_ptr<ThinPlateSplineSurface>(new ThinPlateSplineSurface(regularizationParameter));}
