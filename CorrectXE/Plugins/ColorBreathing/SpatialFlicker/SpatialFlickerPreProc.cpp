// ---------------------------------------------------------------------------

#pragma hdrstop

#include "SpatialFlickerPreProc.h"
#include "ZonalFlickerToolParameters.h"
#include "ToolProgressMonitor.h"
#include "ClipAPI.h"
#include "Ippheaders.h"
#include "MTIsleep.h"
#include "err_cb.h"
#include "IppArray.h"
#include "thread"
#include "IpaStripeStream.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

SpatialFlickerPreProc::SpatialFlickerPreProc(int newToolNumber, const string &newToolName,
	 CToolSystemInterface *newSystemAPI, const bool *newEmergencyStopFlagPtr,
	 IToolProgressMonitor *newToolProgressMonitor)
	 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
	StartProcessThread(); // Required by Tool Infrastructure
}

/////////////////////////////////////////////////////////////////////////////////

SpatialFlickerPreProc::~SpatialFlickerPreProc() {}

/////////////////////////////////////////////////////////////////////////////////

// ****WARNING**** This will allow for multiple frames/iter but tool doesn't use it
int SpatialFlickerPreProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	// Make sure the number of the algorithm processing threads
	// is reasonable
	_processingThreadCount = toolIOConfig->processingThreadCount;
	if (_processingThreadCount < 1)
		_processingThreadCount = 1;
	else if (_processingThreadCount > PREPROCESS_MAX_THREAD_COUNT)
		_processingThreadCount = PREPROCESS_MAX_THREAD_COUNT;

	// Tell Tool Infrastructure, via CToolProcessor supertype, about the
	// number of input and output frames. We read and write one frame
	// each iteration, and write output to a new buffer, not in place.
	SetInputMaxFrames(0, _processingThreadCount, _processingThreadCount); // 'In' port index,

	// One frame required for each iteration
	// One new frame needed every iteration
	SetOutputMaxFrames(0, _processingThreadCount); // 'Out' port index,

	// One frame written each iteration
	SetOutputModifyInPlace(0, 0, false);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
	_markIn = frameRange.inFrameIndex;
	_markOut = frameRange.outFrameIndex;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::SetParameters(CToolParameters *toolParams)
{
	auto toolParameters = dynamic_cast<ZonalFlickerToolParameters*>(toolParams);
	_spatialFlickerModel = toolParameters->Model;
	_tool = toolParameters->Tool;

   // Compute "Original Bits" for IppArray normalization.
	MTI_UINT16 componentValues[3];
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
	imageFormat->getComponentValueMax(componentValues);
	MTI_UINT16 maxPixel = std::max<MTI_UINT16>(componentValues[0], std::max<MTI_UINT16>(componentValues[1], componentValues[2]));
   _bitsPerPixelComponent = 0;
   for (auto mask = 1; mask < maxPixel; mask <<= 1)
   {
      ++_bitsPerPixelComponent;
   }

	return 0;
}

vector<ZonalParamsStruct>SpatialFlickerPreProc::getDeflickerParametersSegments()
{
	vector<ZonalParamsStruct>result;
	auto &zonalDatabase = _tool->getClipPreprocessData().getZonalDatabase(); ;
	auto rows = zonalDatabase.getSegmentRows(_markIn, _markOut);
	if (rows.size() == 0)
	{
		return result;
	}

	for (auto&row : rows)
	{
		ZonalParamsStruct zonalParamsStruct;
		auto zonalParams = zonalDatabase.getParameters(row.ParamId);

		zonalParamsStruct.ProcessType = (int)zonalParams.getZonalProcessType();
		zonalParamsStruct.crows = zonalParams.getVerticalBlocks();
		zonalParamsStruct.ccols = zonalParams.getHorizontalBlocks();
		zonalParamsStruct.FrctOverlap = zonalParams.getOverlap();
		zonalParamsStruct.nbins = zonalParams.getNumberBins();
		zonalParamsStruct.FiltRad = zonalParams.getFilterRadius();
		zonalParamsStruct.MaxFlicker = 10000; // TBD fix this
		zonalParamsStruct.NumIntensityLevels = zonalParams.getNumIntensityLevels();
		zonalParamsStruct.TMotion = zonalParams.getMotion();
		zonalParamsStruct.PdfSig = zonalParams.getPdfSigma();
		zonalParamsStruct.SmoothRad = zonalParams.getSmoothingRadius();
		zonalParamsStruct.SmoothSig = zonalParams.getSmoothingSigma();
		zonalParamsStruct.Downsample = zonalParams.getDownsample();

		auto maxDataValue = GetSpatialFlickerModel()->getMaxValue() + 1;
		zonalParamsStruct.bitShift = 16 - (int)std::ceil(std::log2(maxDataValue));

		// Non-stu parameters
		// Fudge the start frames because the marks don't have to cover a cut;
		zonalParamsStruct.InFrame = std::max<int>(_markIn, row.InFrame);
		zonalParamsStruct.OutFrame = std::min<int>(_markOut, row.OutFrame);
		zonalParamsStruct.TotalFrames = zonalParamsStruct.OutFrame - zonalParamsStruct.InFrame;

		zonalParamsStruct.Roi = zonalParams.getAnalysisRoi();
		zonalParamsStruct.Width = zonalParamsStruct.Roi.width;
		zonalParamsStruct.Height = zonalParamsStruct.Roi.height;

		zonalParamsStruct.segmentRow = row;
		zonalParamsStruct.ImageSize = zonalParams.getImageSize();

		result.push_back(zonalParamsStruct);
	}

	return result;
}


///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::BeginProcessing(SToolProcessingData &procData)
{
	_spatialFlickerModel->setParameters(_tool->getZonalFlickerParameters());
	_spatialFlickerModel->initCommon();

	_tool->setAbortPreprocessingFlag(false);
	_deflickerParamsStructs = getDeflickerParametersSegments();
	_currentZonalParamsStruct.InFrame = -1;
	_currentZonalParamsStruct.OutFrame = -1;

	if (_spatialFlickerModel->getZonalProcessType() == ZonalProcessType::Intensity)
	{
		_processingPlanes =
		{0};
	}
	else
	{
		_processingPlanes =
		{0, 1, 2};
	}

	for (auto p : _processingPlanes)
	{
	 //	ThinPlateSplineSurface::DeflickerBaseStart[p]();
 		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseStart();
	}

	GetToolProgressMonitor()->SetFrameCount(GetTotalFrames());
	GetToolProgressMonitor()->SetStatusMessage("Preprocessing");
	GetToolProgressMonitor()->StartProgress();

	return 0;
}

struct FlickerMotionJobsStruct
{
	SpatialFlickerPreProc *thisClass;
	int startFrame;
	int totalFrames;
	long totalFrameProcessed;

	void BumpProcessed() {InterlockedIncrement(&totalFrameProcessed);}
};

void RunConversionJob(void *vp, int jobNumber)
{
	auto flickerMotionJobsStruct = (FlickerMotionJobsStruct*)vp;

	// This is a trick, when we come to the end do nothing.
	auto internalIndex = flickerMotionJobsStruct->startFrame + jobNumber;
	if (internalIndex >= flickerMotionJobsStruct->totalFrames)
	{
		return;
	}

	auto model = flickerMotionJobsStruct->thisClass->GetSpatialFlickerModel();

	model->motionAnalyzeThreadsafe(internalIndex);
	flickerMotionJobsStruct->BumpProcessed();
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::EndProcessing(SToolProcessingData &procData)
{
	int result = 0;
	////   outFile.close();

	// Did render run to completion or was it aborted?
	if (procData.iteration == GetIterationCount(procData))
	{
		_tool->getClipPreprocessData().updatePreprocessDatabase(_markIn, _markOut);
		GetToolProgressMonitor()->SetStatusMessage("Preprocessing Complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		_tool->setControlState(EMultiControlState::GOT_AN_ERROR);
		GetToolProgressMonitor()->SetStatusMessage("Preprocessing Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	_tool->getClipPreprocessData().resetRenderFlagsInMarks();
	for (auto p : _processingPlanes)
	{
		ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseEnd();
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::BeginIteration(SToolProcessingData &procData)
{
	int inputFrameIndexList[PREPROCESS_MAX_THREAD_COUNT];
	auto currentIndex = _markIn + procData.iteration;

	if ((_currentZonalParamsStruct.InFrame > currentIndex) || (currentIndex >= _currentZonalParamsStruct.OutFrame))
	{
		auto iter = find_if(_deflickerParamsStructs.begin(), _deflickerParamsStructs.end(),
			 [currentIndex](const DeflickerParamsStruct & cp)
		{return (cp.InFrame <= currentIndex) && (currentIndex < cp.OutFrame);});

		if (iter == _deflickerParamsStructs.end())
		{
			return -1;
		}

		_currentZonalParamsStruct = *iter;

		// IMPORTANT change this so parms refect _inframe, _outFrame
		_targetIndex = std::min<int>(_markOut, _currentZonalParamsStruct.OutFrame) - 1;

      // This is so fast we don't have to thread it
		for (auto p : _processingPlanes)
		{
			ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseInit(_currentZonalParamsStruct);
		}

      // Now do the anchor analysis
     	auto row = _tool->getClipPreprocessData().getSegmentRowFromFrameIndex(currentIndex);
		if (row.Id <= 0)
		{
			TRACE_0(errout << "Warning: No row found ");
			return -1;
		}

//      SceneCut markCut = {row.InFrame, row.OutFrame-1};
      SceneCut markCut = {_markIn, _markOut};
		auto anchorValues = _tool->computeAnchorDeltaValues(markCut);

      // This saves nothing if nothing to save
   	_tool->getClipPreprocessData().saveAnchorDeltaValues(row, anchorValues, markCut);
	}

	_framesPerIter = _markOut - _processingThreadCount * procData.iteration;
	_framesPerIter = std::min(_framesPerIter, _processingThreadCount);
	if (_framesPerIter <= 0)
	{
		return CB_ERROR_INTERNAL_FRAMES_LESS_THAN_ZERO;
	}

	for (auto i = 0; i < _framesPerIter; i++)
	{
		inputFrameIndexList[i] = _markIn + _processingThreadCount * procData.iteration + i;
	}

	SetInputFrames(0, _framesPerIter, inputFrameIndexList);

	// When finished processing, release the oldest frame.
	SetOutputFrames(0, _framesPerIter, inputFrameIndexList);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::EndIteration(SToolProcessingData &procData)
{
	auto currentIndex = _markIn + procData.iteration;
	if (currentIndex == _targetIndex)
	{
      auto numPlanes = _processingPlanes.size();
		vector < vector < float >> componentDeltas(numPlanes);
		vector < vector < float >> componentSmoothed(numPlanes);

//		for (auto p : _processingPlanes)
		auto f = [&](int i)
		{
			int p = _processingPlanes[i];
			int n = ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseGetDeltaSize
				 (_currentZonalParamsStruct);
			vector<float>deltaValues(n);
			ThinPlateSplineSurface::zonalFlickerBaseWrapper[p].deflickerBaseGetDeltaValues(_currentZonalParamsStruct,
				 deltaValues.data());

			vector<float>smoothedDeltaValues(n);
			ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseSmoothAll(
            _currentZonalParamsStruct,
            _tool->getRefFrame(),
            _tool->getAnchorFrameOffsets(),
            deltaValues.data(),
            smoothedDeltaValues.data(),
            _tool->getZonalFlickerSmoothingType());

			componentDeltas[p] = deltaValues;
			componentSmoothed[p] = smoothedDeltaValues;
		};

      IpaStripeStream iss;
   	iss << _processingPlanes.size() << efu_job(f);
   	iss.stripe();

		_tool->getClipPreprocessData().saveDeltaValues(_currentZonalParamsStruct, componentDeltas);
		_tool->getClipPreprocessData().saveSmoothedValues(_currentZonalParamsStruct, componentSmoothed);
	}

	GetToolProgressMonitor()->BumpProgress();
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::GetIterationCount(SToolProcessingData &procData) {
	return (GetTotalFrames() + _processingThreadCount - 1) / _processingThreadCount;}

///////////////////////////////////////////////////////////////////////////////
int SpatialFlickerPreProc::DoProcess(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("SpatialFlickerPreProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	auto imageFormat = GetSrcImageFormat(0);

	// Return error if a monochrome image
// INTERNAL FORMAT IS ALWAYS 3 COMPONENTS, tool shouldn't care what the source is!
//	if (imageFormat->getComponentCount() != 3)
//	{
//		autoErr.errorCode = theError.getError();
//		autoErr.msg << "Color Breathing only works on three component images, Image formate components = " <<
//			 imageFormat->getComponentCount();
//
//		return -1;
//	}

	// Right now this is not multithreaded as it is fast
	// TODO:  if slow, have the tool infrastructer give us n frames and multprocess it
	try
	{
		if (_spatialFlickerModel->getZonalProcessType() == ZonalProcessType::Intensity)
		{
			return ProcessOneFrame(0);
		}

		return ProcessColorFrame(0);
	}
	catch (const std::exception &ex)
	{
      TRACE_0(errout << ex.what());
		autoErr.errorCode = theError.getError();
		autoErr.msg << "Unexpected error processing frame " << ex.what();
	}
	catch (...)
	{
   	TRACE_0(errout << "Unknow error processing frame ");
		autoErr.errorCode = theError.getError();
		autoErr.msg << "Unknown error processing frame ";
	}

	return -1;
}

int SpatialFlickerPreProc::ProcessOneFrame(int bufferIndex)
{
	CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, bufferIndex);
	if (inToolFrameBuffer == nullptr)
	{
		return CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL;
	}

	MTI_UINT16 *imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();
	auto frameIndex = inToolFrameBuffer->GetFrameIndex();

	auto imageFormat = GetSrcImageFormat(0);
	int width = imageFormat->getPixelsPerLine();
	int height = imageFormat->getLinesPerFrame();

	Ipp16uArray image({width, height, 3}, imageData);  \
   MTIassert(_bitsPerPixelComponent != 0);
   image.setOriginalBits(_bitsPerPixelComponent);

	auto imageRoi = image(_currentZonalParamsStruct.Roi);

   // KLUDGE, need to fix IPP later
   imageRoi.setOriginalBits(image.getOriginalBits());
	auto planes = imageRoi.copyToPlanar();

	if (_currentZonalParamsStruct.ProcessType == 1)
	{

		ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseProcessOneFrame(_currentZonalParamsStruct,
		planes[0]);
	}

	return 0;
}

int SpatialFlickerPreProc::ProcessColorFrame(int bufferIndex)
{
CHRTimer ProcessColorFramehrt;
	CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, bufferIndex);
	if (inToolFrameBuffer == nullptr)
	{
		return CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL;
	}

	MTI_UINT16 *imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();
	auto frameIndex = inToolFrameBuffer->GetFrameIndex();

	auto imageFormat = GetSrcImageFormat(0);
	int width = imageFormat->getPixelsPerLine();
	int height = imageFormat->getLinesPerFrame();

	Ipp16uArray image({width, height, 3}, imageData);
   MTIassert(_bitsPerPixelComponent != 0);
   image.setOriginalBits(_bitsPerPixelComponent);

	auto imageRoi = image(_currentZonalParamsStruct.Roi);

	// KLUDGE, need to fix IPP later
   imageRoi.setOriginalBits(image.getOriginalBits());
	auto planes = imageRoi.copyToPlanar();

   double t[3];
   IpaStripeStream iss;
   auto f = [&](int i)
   {
      CHRTimer hrt;
   	ThinPlateSplineSurface::zonalFlickerBaseWrapper[i].deflickerBaseProcessOneFrame(_currentZonalParamsStruct, planes[i]);
      t[i] = hrt.elapsedMilliseconds();
   };

   iss << 3 << efu_job(f);
   iss.stripe();

//   TRACE_0(errout << "[ " << t[0] << ", " << t[1] << ", " << t[2] << ", " << ProcessColorFramehrt << " ]");

//DBTRACE(ProcessColorFramehrt);
	return 0;
}
