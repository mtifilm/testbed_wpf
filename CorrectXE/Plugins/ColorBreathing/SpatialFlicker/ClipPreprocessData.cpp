
// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ClipPreprocessData.h"
#include "ColorBreathingTool.h"
#include "TimeLine.h"
#include "MTIsleep.h"
#include <filesystem>
#include <regex>
#include <numeric>
#include <functional>
#include "Ippheaders.h"
#include "IpaStripeStream.h"
#include "err_cb.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

string getTempPath()
{
    char dirBuf[MAX_PATH];
    GetTempPath(MAX_PATH, dirBuf);
    return string(dirBuf);
}

void ClipPreprocessData::onNewClip()
{
    auto clip = getSystemAPI()->getClip();
    if (clip == nullptr)
    {
        return;
    }

    auto clipPath = clip->getClipPathName();
    _dataPath = CombinePathAndName(clipPath, "ZonalData");

    // Create it if it doesn't exits
    if (DoesWritableDirectoryExist(_dataPath) == false)
    {
        if (MakeDirectory(_dataPath) == false)
        {
            CAutoErrorReporter autoErr("ZonalFlicker::OnNewClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
            autoErr.traceAction = AUTO_ERROR_NO_TRACE;
            autoErr.errorCode = -1;
            autoErr.msg << "Failed to create the necessary directory " << endl << _dataPath;
        }
    }

    auto imageFormat = getSystemAPI()->getVideoClipImageFormat();
    MTIassert(imageFormat != nullptr);
    auto pixelsCol = imageFormat->getPixelsPerLine();

//    MTI_UINT16 caMax[3];
//	 imageFormat->getComponentValueMax(caMax);
//    GColorBreathingTool->getZonalFlickerModel()->createContrastLuts(caMax[0], CONTRAST_FORWARD_MAX, CONTRAST_LUT_SIZE);

    auto downsample = (pixelsCol >= 3000) ? DEFAULT_DOWNSAMPLE : DEFAULT_DOWNSAMPLE / 2;
    getZonalDatabase().open(_dataPath);

    // Try to set it correctly
	 if (GColorBreathingTool->getZonalFlickerModel()->getZonalProcessType() == ZonalProcessType::Unknown)
	 {
		 if (imageFormat->getPixelComponents() == EPixelComponents::IF_PIXEL_COMPONENTS_YYY)
		 {
			 GColorBreathingTool->getZonalFlickerModel()->setZonalProcessType(ZonalProcessType::Intensity);
		 }
	 }

    GColorBreathingTool->getZonalFlickerModel()->setDownsample(downsample);
    updateSegmentsFromBreaks();

    onNewMarks();
}

string ClipPreprocessData::getPreprocessDataPath() const {return _dataPath;}

string ClipPreprocessData::getDataFolder() const
{
    auto path = getPreprocessDataPath();
    if (DoesDirectoryExist(path) == false)
    {
        if (MakeDirectory(path) == false)
        {
            throw std::runtime_error(string("Failed to make folder '") + path + "'");
        }
    }

    // char tempName[MAX_PATH];
    // auto err = GetTempFileName(path.c_str(), "ZF", 0, tempName);
    // if (err == 0)
    // {
    // throw std::runtime_error(GetLastSystemErrorMessage() + "\nCould not create temp name");
    // }
    // auto name = ReplaceFileExt(string(tempName), ".dat");
    // err = rename(tempName, name.c_str());
    // if (err != 0)
    // {
    // throw std::runtime_error(GetLastSystemErrorMessage() + "\nCould not rename to " + name);
    // }

    return path;
}

string inline ToUpper(const string &sin)
{
    auto s = sin;
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c)->unsigned char {return std::toupper(c);});
    return s;
}

bool endsWith(const string& a, const string& b)
{
    if (b.size() > a.size())
    {
        return false;
    }

    return std::equal(a.begin() + a.size() - b.size(), a.end(), b.begin());
}

void ClipPreprocessData::cleanupFolder()
{
    auto path = getPreprocessDataPath();
    auto segmentList = getZonalDatabase().getSegmentRows("");
    vector<string>files;

    // This matches a file name starting with a GUID and ending with a .*
    // Group 1 is the GUID.
    std::regex GuidId("^([[:alnum:]]{8}(-[[:alnum:]]{4}){4}[[:alnum:]]{8})\\..*");

#if __cplusplus <= 201103
	// <= 10.3.2
	for (auto&p : std::tr2::sys::directory_iterator(path))
#else
	// >= 10.3.3
	for (auto&p : std::filesystem::directory_iterator(path))
#endif
	{
        string s = GetFileNameWithExt(p.path().string());

        // If the file name is not a GUID quit;
        std::smatch cm;
        if (std::regex_match(s, cm, GuidId) == false)
        {
            continue;
        }

        // we really should do a case insensitve compare or convert to a true UUID
        // but this works for now
        s = cm[1];
        auto it = std::find_if(segmentList.begin(), segmentList.end(), [s](const SegmentRow & sr)->bool
        {return s == sr.Uuid;});

        if (it == segmentList.end())
        {
            auto fn = p.path().string();
            TRACE_0(errout << "Removed ZonalFlicker file: '" << fn << "'");
            remove(fn.c_str());
        }
    }
}

void ClipPreprocessData::updateSegmentsFromBreaks()
{
    vector<SegmentRow>segmentRows;
    auto cuts = getCutsFromClip();

    // Normally there must be two cuts, however the timeline can be
    // in flux, so the number of cuts are not determined, so do nothign
    // if this happens.
    if (cuts.size() == 0)
    {
        return;
    }

    auto frameIn = cuts.front();

    auto segmentList = getZonalDatabase().getSegmentRows("");
    string emptyString;
    vector<SegmentRow>alteredRows;
    vector<SegmentRow>oldValidRows;

    auto model = GColorBreathingTool->getZonalFlickerModel();
    for (auto j = 1; j < cuts.size(); j++)
    {
        auto frameOut = cuts[j];

        // See if that one already exists
        auto it = std::find_if(segmentList.begin(), segmentList.end(), [frameIn, frameOut](const SegmentRow & sr)
        {return (sr.InFrame == frameIn) && (sr.OutFrame == frameOut);});

        if (it == segmentList.end())
        {
            // Not found, create one
            auto id = getZonalDatabase().insertParametersIfNecessary(*model);
            alteredRows.emplace_back(-1, getDataFolder(), UuidSupport::CreateUuidString(), frameIn, frameOut, id);
        }
        else
        {
            oldValidRows.push_back(*it);
        }

        frameIn = frameOut;
    }

    // Early out
    if (alteredRows.size() == 0)
    {
        return;
    }

    // Delete all rows not used anymore
    if (oldValidRows.size() > 0)
    {
        auto concat = std::accumulate(oldValidRows.begin() + 1, oldValidRows.end(),
            string("ID!=") + std::to_string(oldValidRows.front().Id), [](std::string f, SegmentRow & r)
        {return f + " AND ID!= " + std::to_string(r.Id);});

        auto deleteSql = string("DELETE FROM segment WHERE ") + concat;
        getZonalDatabase().execute(deleteSql);
    }
    else
    {
        // Clear the table
        getZonalDatabase().execute("DELETE FROM segment");
    }

    getZonalDatabase().setSegmentRows(alteredRows);
    cleanupFolder();
}

void ClipPreprocessData::createFrameStatusIfNecessary()
{
    throw std::runtime_error("Should never be called");
    auto totalFrames = getSystemAPI()->getVideoFrameList()->getOutFrameIndex();
    auto frameRowCount = getZonalDatabase().getFrameRowsCount(0, totalFrames);

    // Early out, save time
    if (totalFrames == frameRowCount)
    {
        return;
    }

    // 100% of the time should be here
    if (frameRowCount == 0)
    {
        // Just to avoid very large chunks
        vector<FrameStatusRow>frameRows(100);
        auto chunkIndex = 0;
        auto chunkSize = frameRows.size();
        for (auto id = 0; id < totalFrames; id++)
        {
            frameRows[chunkIndex++].Id = id; ;
            if (chunkIndex >= chunkSize)
            {
                chunkIndex = 0;
                getZonalDatabase().setFrameRows(frameRows);
            }
        }

        getZonalDatabase().setFrameRows(frameRows, chunkIndex);

        return;
    }

    // Some frames are missing
    // See if the proper number of frames exist, if not create them
    // Do segment by segment.
    auto segmentList = getZonalDatabase().getSegmentRows("");
    for (auto&row : segmentList)
    {
        auto frameSegmentCount = getZonalDatabase().getFrameRowsCount(row.InFrame, row.OutFrame);
        if (frameSegmentCount < row.totalFrames())
        {
            auto frameRows = getZonalDatabase().getFrameRows(row.InFrame, row.OutFrame);
            vector<int>ids;
            for (auto&fr : frameRows)
            {
                ids.push_back(fr.Id);
            }

            // This is inefficient as we know the row IDs are increasing
            for (auto i = row.InFrame; i < row.OutFrame; i++)
            {
                auto it = std::find_if(ids.begin(), ids.end(), [i](int id)->bool {return i == id;});
                if (it == ids.end())
                {
                    frameRows.emplace_back(i);
                }
            }

            getZonalDatabase().setFrameRows(frameRows);
        }
    }
}

void ClipPreprocessData::updateFrameStatus()
{
    // NOT USED FOR NOW
    auto segmentList = getZonalDatabase().getSegmentRows("");
    for (auto&row : segmentList)
    {
        auto frameRows = getZonalDatabase().getFrameRows(row);

        // We need to create the rows if they don't exist
        auto dirtyFrames = false;
        if (frameRows.size() != row.totalFrames())
        {
            vector<int>ids;
            for (auto&fr : frameRows)
            {
                ids.push_back(fr.Id);
            }

            // This is inefficient as we know the row IDs are increasing
            for (auto i = row.InFrame; i < row.OutFrame; i++)
            {
                auto it = std::find_if(ids.begin(), ids.end(), [i](int id)->bool {return i == id;});
                if (it == ids.end())
                {
                    frameRows.emplace_back(i);
                }
            }

            getZonalDatabase().setFrameRows(frameRows);
        }

        // We now have a frameRow for each frame in row
        auto const &fileName = row.getRepairFileName();
        vector<bool>status(row.totalFrames(), false);
        if (DoesFileExist(fileName))
        {
            if (GetFileLength(fileName) != 0)
            {

            }
        }
    }
}

CToolSystemInterface *ClipPreprocessData::getSystemAPI() const {return GColorBreathingTool->getSystemAPI();}

vector<int>ClipPreprocessData::getCutsFromClip() const
{
    auto timeLine = getSystemAPI()->getClipTimeline();
    int iNext = -1;

    // Get all the cuts
    vector<int>cuts;

    // This could be called when timeline is in flux
    // So just ignore if out is zero
    auto out = timeLine->getTotalFrameCount();
    if (out == 0)
    {
        return cuts;
    }

    while (true)
    {
        auto next = timeLine->getNextEvent(iNext, EVENT_CUT_ADD | EVENT_CUT_AUTO);
        if (iNext == next)
        {
            break;
        }

        cuts.push_back(next);
        iNext = next;
    }

    if (cuts.size() == 0)
    {
        cuts.push_back(0);
        cuts.push_back(out);
    }
    else
    {
        if (cuts.front() != 0)
        {
            cuts.insert(cuts.cbegin(), 0);
        }

        if (cuts.back() != out)
        {
            cuts.push_back(out);
        }
    }

    return cuts;
}

 SegmentRow ClipPreprocessData::getSegmentRowFromFrameIndex(int frameIndex) const
 {
   // Just return a default row if index is negative
	 if ((frameIndex < 0) || !getZonalDatabase().isConnected())
	 {
		 return SegmentRow();
	 }

	 auto rows = getZonalDatabase().getSegmentRows(frameIndex, frameIndex + 1);
	 if (rows.size() == 1)
	 {
		 return rows[0];
	 }

    // This really isn't an exception but an error
	 return SegmentRow();
	/// THROW_MTI_RUNTIME_ERROR(string("Expecting exactly one segement for each frame\nFound: " + to_string(rows.size())));
 }

void ClipPreprocessData::updateFramesFromSegment(const SegmentRow &row)
{
    auto frameIn = row.InFrame;
    auto frameOut = row.OutFrame;

    auto frameList = getSystemAPI()->getVideoFrameList();
    if (frameList == nullptr)
    {
        return;
    }

    vector<int>dirtyFrameIndices;
    CBinManager binManager;
    int retVal = binManager.FindDirtyFrames(frameList, frameIn, frameOut - 1, dirtyFrameIndices, nullptr, nullptr);
    if (retVal)
    {
        // Because the clip at this point is not fully formed, just return
        return;
    }

    // We need the clean frames between frameIn and frame out
    vector<int>cleanFrameIndices;
    for (auto i = frameIn; i < frameOut; i++)
    {
        if (find(dirtyFrameIndices.begin(), dirtyFrameIndices.end(), i) == dirtyFrameIndices.end())
        {
            cleanFrameIndices.push_back(i);
        }
    }

    auto frameRows = getZonalDatabase().getFrameRows(frameIn, frameOut);

    // In almost every case, we get everything or empty
    if (frameRows.size() == 0)
    {
        // Empty means preprocessing was not done
        for (auto i : dirtyFrameIndices)
        {
            frameRows.push_back(i);
            frameRows.back().setPrerendered(true);
        }

        for (auto i : cleanFrameIndices)
        {
            frameRows.push_back(i);
        }

        getZonalDatabase().setFrameRows(frameRows);
        return;
    }
    else if (frameRows.size() < (frameOut - frameIn))
    {
        vector<FrameStatusRow>newFrameRows;
        for (auto i = frameIn; i < frameOut; i++)
        {
            auto it = std::find_if(frameRows.begin(), frameRows.end(), [i](const FrameStatusRow & r)
            {return r.Id == i;});

            if (it == frameRows.end())
            {
                // Add it
                newFrameRows.emplace_back(i);

                // See if dirty
                auto jt = std::find_if(dirtyFrameIndices.begin(), dirtyFrameIndices.end(), [i](const int &id)
                {return id == i;});
                if (jt != dirtyFrameIndices.end())
                {
                    newFrameRows.back().setPrerendered(true);
                }
            }
        }

        getZonalDatabase().setFrameRows(newFrameRows);
    }
}

string ClipPreprocessData::getSegmentPrefix(const SegmentRow &row)
{
    std::ostringstream os2;
    os2 << "SP" << row.Id;
    return os2.str();
}

void ClipPreprocessData::legalizeMarks(int &markIn, int &markOut) const
{
    if ((markOut > markIn) && (markIn >= 0))
    {
        return;
    }

    auto currentIndex = GColorBreathingTool->GetCurrentFrameIndex();

    auto cuts = getCutsFromClip();

    // Normally there must be two cuts, however the timeline can be
    // in flux, so the number of cuts are not determined, so do nothign
    // if this happens, do nothing
    if (cuts.size() == 0)
    {
        return;
    }

    auto lefti = 0;
    auto righti = 0;
    for (righti = 1; righti < cuts.size(); righti++)
    {
        if ((currentIndex >= cuts[lefti]) && (currentIndex < cuts[righti]))
        {
            break;
        }

        lefti = righti;
    }

    markIn = cuts[lefti];
    markOut = cuts[righti];
}

ZonalParameters ClipPreprocessData::generateParametersForSegment(const SegmentRow &row, int markIn, int markOut) const
{
    legalizeMarks(markIn, markOut);

    auto zonalParams = getZonalDatabase().getParameters(row.ParamId);
    zonalParams.setInFrame(std::max<int>(markIn, row.InFrame));
    zonalParams.setOutFrame(std::min<int>(markOut, row.OutFrame));
    zonalParams.setReferenceFrame(row.ReferenceFrame);
    zonalParams.setHeadAnchorOffset(row.HeadAnchorOffset);
    zonalParams.setTailAnchorOffset(row.TailAnchorOffset);

    return zonalParams;
}

string ClipPreprocessData::generateIniString(const SegmentRow &row, const ZonalParameters &zonalParams)
{
    auto sp = getSegmentPrefix(row) + ".";

    std::ostringstream os;
    os << sp << "Id=" << row.Id << endl;
    os << sp << "RepairDataFile='" << row.getRepairFileName() << "'" << endl;
    os << sp << "DeltaDataFile='" << row.getDeltaFileName() << "'" << endl;
//xxx092319    os << sp << "Images='" << row.getImageFileName() << "'" << endl;

    os << sp << "ImageSize=struct('width'," << zonalParams.getWidth();
    os << ",'height'," << zonalParams.getHeight() << ")" << endl;

    auto roi = zonalParams.getAnalysisRoi();
    os << sp << "ROI=struct('x', " << roi.x + 1 << ",'y'," << roi.y + 1 << ",'width'," << roi.width -
        1 << ",'height'," << roi.height - 1 << ")" << endl;

    // FYI parameters
    os << sp << "InFrameIndex=" << zonalParams.getInFrame() << endl;
    os << sp << "OutFrameIndex=" << zonalParams.getOutFrame() << endl;
    os << sp << "ProcessType=" << (int)zonalParams.getZonalProcessType() << endl;

    // Stu's parameters
    os << sp << "crows=" << zonalParams.getVerticalBlocks() << endl;
    os << sp << "ccols=" << zonalParams.getHorizontalBlocks() << endl;
    os << sp << "FrctOverlap=" << zonalParams.getOverlap() << endl;
    os << sp << "nbins=" << zonalParams.getNumberBins() << endl;
    os << sp << "FiltRad=" << zonalParams.getFilterRadius() << endl;
    os << sp << "MaxFlicker=10000" << endl;
    os << sp << "NumIntensityLevels=" << zonalParams.getNumIntensityLevels() << endl;
    os << sp << "TMotion=" << zonalParams.getMotion() << endl;
    os << sp << "PdfSig=" << zonalParams.getPdfSigma() << endl;
    os << sp << "SmoothRad=" << zonalParams.getSmoothingRadius() << endl;
    os << sp << "SmoothSig=" << zonalParams.getSmoothingSigma() << endl;
    os << sp << "Downsample=" << zonalParams.getDownsample() << endl;
    os << sp << "SequenceNumber=" << zonalParams.getInFrame() << endl;
    return os.str();
}

string ClipPreprocessData::writeControlFile(const vector<SegmentRow> &rows, int markIn, int markOut) const
{
    if (rows.size() == 0)
    {
        throw std::runtime_error(string(__func__) + ":: Number of segments must be greater than 0");
    }

    legalizeMarks(markIn, markOut);

    auto fileName = CombinePathAndName(getPreprocessDataPath(), "DeflickerControl.txt");
    ofstream fn(fileName, std::ofstream::out);

    // write parameters
    vector<string>segmentNames;
    for (auto&row : rows)
    {
        segmentNames.push_back(getSegmentPrefix(row));
        auto zonalParams = generateParametersForSegment(row, markIn, markOut);
        fn << generateIniString(row, zonalParams);

//xxx092319        auto imageFileName = row.getImageFileName();
//        ofstream imageFile(imageFileName, std::ofstream::out);
//        for (auto frameIndex = zonalParams.getInFrame(); frameIndex < zonalParams.getOutFrame(); frameIndex++)
//        {
//            imageFile << GColorBreathingTool->getImageFileName(frameIndex) << endl;
//        }
    }

    fn << "controlSegments=[";
    for (auto&segmentName : segmentNames)
    {
        fn << segmentName << " ";
    }

    fn << "]" << endl;

    return fileName;
}

string ClipPreprocessData::writeControlFile(const ZonalParameters &params)
{
    // In reality this should be the same as private data, but recompute for sure
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    vector<SegmentRow> segments;
    int64_t paramId;
    try
    {
       segments = getZonalDatabase().getSegmentRows(markIn, markOut);
       paramId = getZonalDatabase().insertParametersIfNecessary(params);
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << ex.what());
    }

    // Now update any information
    for (auto&segment : segments)
    {
        // TBD: Brute force for now, force everything to be redone
        segment.ParamId = paramId;
    }

    getZonalDatabase().setSegmentRows(segments);
    return writeControlFile(segments, markIn, markOut);
}

bool ClipPreprocessData::areMarksRendered() const
{
    try
    {
        if (getZonalDatabase().isConnected() == false)
        {
            return false;
        }

        auto markIn = getSystemAPI()->getMarkIn();
        auto markOut = getSystemAPI()->getMarkOut();
        legalizeMarks(markIn, markOut);

        auto segments = getZonalDatabase().getSegmentRows(markIn, markOut);
        if (segments.size() == 0)
        {
            TRACE_2(errout << "Size failed");
            return false;
        }

        for (auto&segment : segments)
        {
            if (segment.isRendered() == false)
            {
                TRACE_2(errout << "Segment " << segment.Id << "  is not rendered");
                return false;
            }
        }

        return true;
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << ex.what());
        return false;
    }
}

void ClipPreprocessData::resetRenderFlagsInMarks()
{
    if (getZonalDatabase().isConnected() == false)
    {
        return;
    }

    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    auto segments = getZonalDatabase().getSegmentRows(markIn, markOut);
    for (auto&segment : segments)
    {
        segment.setRendered(false);
    }

    getZonalDatabase().setSegmentRows(segments);
}

bool ClipPreprocessData::doRepairFilesExist(const SegmentRow &row) const
{
    // See if data file exists (it always should, unless an external event deleted it
    auto repairFileName = row.getRepairFileName();
    if (DoesFileExist(repairFileName) == false)
    {
        TRACE_1(errout << "no repair data file");
        return false;
    }

    auto deltaFileName = row.getDeltaFileName();
    if (DoesFileExist(deltaFileName) == false)
    {
        TRACE_1(errout << "no repair delta file");
        return false;
    }

    return true;
}

bool ClipPreprocessData::doRepairFilesExistCached(const SegmentRow &row) const
{
    auto cacheDataFileExistsIds = getCacheDataFileExistsIds();
    if (cacheDataFileExistsIds.find(row.Id) == cacheDataFileExistsIds.end())
    {
        cacheDataFileExistsIds[row.Id] = doRepairFilesExist(row);
    }

    return cacheDataFileExistsIds[row.Id];
}

std::pair<bool, bool>ClipPreprocessData::computeIsPreprocessingDone(const ZonalParameters &params, const SegmentRow &row,
    int in, int out) const
{
    // See if the preprocessing marks have been set
    if ((row.StartFrame < 0) || (row.EndFrame < 0))
    {
        TRACE_2(errout << "Row small");
        return {false, false};
    }

    // See if the frame range is correct
    if ((in < row.StartFrame) || (row.EndFrame < out))
    {
        return {
            false, false};
    }

    // See if data file exists (it always should, unless an external event deleted it
    if (doRepairFilesExistCached(row) == false)
    {
        TRACE_2(errout << "no repair files");
        return {false, false};
    }

    // Next check if parameters match row
    auto rowParams = getZonalDatabase().getProcessedParameters(row);
    if (rowParams.doDeltaParametersMatch(params) == false)
    {
        TRACE_2(errout << "match false");
        return {false, false};
    }

    auto smoothingMatch = rowParams.doSmoothingParametersMatch(params);
    return {
        true, smoothingMatch};
}

std::pair<bool, bool>ClipPreprocessData::computeIsPreprocessingDone(const ZonalParameters &params,
    const vector<SegmentRow> &rows) const
{
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    // This is brute force, we can optimize by caching the preprocessing
    // In reality there is always a segment
    if (rows.size() == 0)
    {
        return {
            false, false};
    }

    // See if all segments have been checked, if not clear it out
    auto cacheDataFileExistsIds = getCacheDataFileExistsIds();
    for (auto&row : rows)
    {
        if (cacheDataFileExistsIds.find(row.Id) == cacheDataFileExistsIds.end())
        {
            cacheDataFileExistsIds.clear();
            break;
        }
    }

    // Cleaner than find_if
    auto smoothing = true;
    for (auto&row : rows)
    {
        auto out = std::min<int>(row.OutFrame, markOut);
        auto in = std::max<int>(row.InFrame, markIn);
        auto done = computeIsPreprocessingDone(params, row, in, out);

        if (done.first == false)
        {
            return {false, false};
        }

        smoothing = smoothing && done.second;
    }

    return {true, smoothing};
}

void ClipPreprocessData::computeIsDefaultPreprocessingDone()
{
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    auto &model = *(GColorBreathingTool->getZonalFlickerModel());

    if (getZonalDatabase().isConnected() == false)
    {
        _isProcessingValidOverMarks = {false, false};
        TRACE_2(errout << "Warning: Zonal database not open");
        return;
    }

    // the newRows could be different than DB because markIn and markOut
    auto newRows = getZonalDatabase().getSegmentRows(markIn, markOut);
    _isProcessingValidOverMarks = computeIsPreprocessingDone(model, newRows);
}

int ClipPreprocessData::onNewMarks()
{
    computeIsDefaultPreprocessingDone();
    return 0;
}

bool ClipPreprocessData::updatePreprocessDatabase(int markIn, int markOut)
{
    legalizeMarks(markIn, markOut);

    // the newRows could be different than DB because markIn and markOut
    auto segments = getZonalDatabase().getSegmentRows(markIn, markOut);
    auto result = true;

    for (auto&segment : segments)
    {
        if (DoesFileExist(segment.getRepairFileName()) == false)
        {
            segment.StartFrame = -1;
            segment.EndFrame = -1;
            result = false;
        }
        else
        {
            auto params = ThinPlateSplineDeltaData::readParameters(segment.getRepairFileName());
            auto paramsId = getZonalDatabase().insertParametersIfNecessary(params);
            if (paramsId != segment.ParamId)
            {
                segment.ParamId = paramsId;
                result = false;
            }

            segment.StartFrame = params.getInFrame();
            segment.EndFrame = params.getOutFrame();
            segment.setPreprocessed(true);
        }
    }

    getZonalDatabase().setSegmentRows(segments);
    return result;
}

bool ClipPreprocessData::readPreprocessorData(int frameIndex, SegmentsDeltas &segmentDeltas) {
    return readPreprocessorData(frameIndex, frameIndex + 1, segmentDeltas);}

bool ClipPreprocessData::readPreprocessorData(int markIn, int markOut, SegmentsDeltas &segmentDeltas)
{
    legalizeMarks(markIn, markOut);

    try
    {
        auto segments = getZonalDatabase().getSegmentRows(markIn, markOut);
        if (segments.size() == 0)
        {
            return false;
        }

        segmentDeltas.clear();
        for (auto&segment : segments)
        {
            ThinPlateSplineDeltaData data;
            if (data.loadSegmentData(segment.getRepairFileName()) == false)
            {
                return false;
            }

            segmentDeltas.push_back(data);
        }

        computeIsDefaultPreprocessingDone();
        return true;
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << string(ex.what()));
    }

    return false;
}

bool ClipPreprocessData::isPreprocessingValidForFrame(int frameIndex) const
{
    try
    {
        auto row = getSegmentRowFromFrameIndex(frameIndex);
        auto &params = GColorBreathingTool->getZonalFlickerModel()->getModelParameters();
        auto done = computeIsPreprocessingDone(params, row, frameIndex, frameIndex);

        return done.first && done.second;
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << string(ex.what()));
    }
    catch (...)
    {
        TRACE_0(errout << "Unknown error");
    }

    return false;
}

bool ClipPreprocessData::areMarksValid()
{
    // See if set
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();

    if ((markIn < 0) && (markOut >= 0)) {
        return false;
    }

    if ((markOut < 0) && (markIn >= 0)) {
        return false;
    }

    if ((markIn >= 0) && (markOut > markIn))
    {
        return true;
    }

    auto cuts = getCutsFromClip();

    // We put in a cut at beginning and end
    return cuts.size() > 2;
}

int ClipPreprocessData::totalFrames()
{
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    return markOut - markIn;
}

int ClipPreprocessData::startFrame()
{
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);

    return markIn;
}

void ClipPreprocessData::WritePDLEntry(CPDLElement &parent)
{
    try
    {
        auto model = GColorBreathingTool->getZonalFlickerModel();
        auto markIn = getSystemAPI()->getMarkIn();
        auto markOut = getSystemAPI()->getMarkOut();
        legalizeMarks(markIn, markOut);

		  // Make tool type entry
		  auto pdlToolType = parent.MakeNewChild("ToolType");
		  pdlToolType->SetAttribString("ToolSubType", "Zonal");

         // Write Colorbreathing Parameters to a PDL Entry's Tool Attributes
        // We could pull them out of database
        auto pdlParameters = parent.MakeNewChild("Parameters");

        auto useProcessingRegion = GColorBreathingTool->getZonalFlickerUseProcessingRegion();
		  pdlParameters->SetAttribBool("UseProcessingRegion", useProcessingRegion);
		  pdlParameters->SetAttribRect("Roi", IppArrayConvert::convertToRect(model->getAnalysisRoi()));
        pdlParameters->SetAttribInteger("Boxes", model->getHorizontalBlocks());
        pdlParameters->SetAttribDouble("Smoothing", model->getSmoothingSigma());
        pdlParameters->SetAttribInteger("Channels", model->getProcessingChannels());
        auto anchorFrameOffsets = GColorBreathingTool->getAnchorFrameOffsets();
        pdlParameters->SetAttribInteger("HeadAnchorOffset", anchorFrameOffsets.first);
        pdlParameters->SetAttribInteger("TailAnchorOffset", anchorFrameOffsets.second);
        pdlParameters->SetAttribInteger("RefFrameClipIndex", GColorBreathingTool->getRefFrame());

        auto size = 0;
//        auto result = getZonalDatabase().dumpAsBase64(size);
        auto encodedDB = getZonalDatabase().dumpAsBase64InOneBigAssString(size);

        auto pdlData = parent.MakeNewChild("Database");
// THESE AREN'T USED because we encode the whole friggin' database a CDATA!
//        pdlData->SetAttribString("DatabaseName", getZonalDatabase().getDatabaseFullFileName());
//        pdlData->SetAttribString("DatabasePath", getZonalDatabase().getDatabasePath());
//        pdlData->SetAttribInteger("DatabaseSize", size);
        pdlData->AppendCdata(encodedDB);
      }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << string(ex.what()));
    }
    catch (...)
    {
        TRACE_0(errout << "Unknown error");
    }
}

int ClipPreprocessData::reloadDefaultDatabase()
{
	getZonalDatabase().close();
   return getZonalDatabase().open(_dataPath);
}

int ClipPreprocessData::ReadPDLEntry(CPDLEntry &pdlEntry)
{
    CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
    CPDLElement *toolAttribRoot = pdlEntryTool->parameters;
    auto retVal = 0;
    auto useProcessingRegion = false;

    auto model = GColorBreathingTool->getZonalFlickerModel();

    for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
    {
        CPDLElement *subElement = toolAttribRoot->GetChildElement(i);
        string elementName = subElement->GetElementName();

        if (elementName == "Database")
        {
// mbraca took these out because he thinks parsing attributes is slow!
// instead, hard-code temp database name to be "ZonalFlickerPDL.db"
//            auto name = subElement->GetAttribString("DatabaseName", "");
//            auto path = subElement->GetAttribString("DatabasePath", "");

            // Create a temp path
//            auto databaseName = GetFileName(name) + ".db";
            auto tempPath = getTempPath();
//            auto tempName = CombinePathAndName(tempPath, databaseName);
//            auto dataBaseSize = subElement->GetAttribInteger("DatabaseSize", -1);
            auto tempName = CombinePathAndName(tempPath, getZonalDatabase().getDatabaseName());

            getZonalDatabase().close();
            ofstream tempDatabaseFileOutputStream(tempName, ios::out | ios::binary);
            if (!tempDatabaseFileOutputStream.good())
            {
               return CB_ERROR_UNABLE_CREATE_TEMP_ZONAL_DATABASE;
            }

            if (subElement->GetAttribString("D0", "").empty() == false)
            {
               // LEGACY SUPPORT: DATABASE IS CHUNKED IN THE ATTRIBUTES!
               for (auto i = 0; true; i++)
               {
                   auto as = string("D") + std::to_string(i);
                   auto chunkString = subElement->GetAttribString(as, "");
                   if (chunkString.empty())
                   {
                       break;
                   }

                   auto chunk = base64_decode(chunkString);
                   if (chunk.size() == 0)
                       break;
                   tempDatabaseFileOutputStream << chunk;
               }
            }
            else
            {
               // NEW, much faster way: database is encoded to CDATA!
               auto cdata = subElement->GetCdata();
               if (cdata.empty())
               {
                  return CB_ERROR_PDL_IS_INVALID;
               }

               tempDatabaseFileOutputStream << base64_decode(cdata);
            }

            if (tempDatabaseFileOutputStream.fail())
            {
	              return CB_ERROR_UNABLE_WRITE_TEMP_ZONAL_DATABASE;
            }

            getZonalDatabase().open(tempPath);
        }

        else if (elementName == "Parameters")
        {
            useProcessingRegion = subElement->GetAttribBool("UseProcessingRegion", false);
            auto roi = subElement->GetAttribRect("Roi", {0, 0, 0, 0});
            auto ccols = subElement->GetAttribInteger("Boxes", model->getHorizontalBlocks());
            auto sigma = subElement->GetAttribDouble("Smoothing", model->getSmoothingSigma());
            auto channels = subElement->GetAttribDouble("Channels", model->getProcessingChannels());
            auto headAnchorOffset = subElement->GetAttribInteger("HeadAnchorOffset", -1);
            auto tailAnchorOffset = subElement->GetAttribInteger("TailAnchorOffset", -1);
            auto refFrameClipIndex = subElement->GetAttribInteger("RefFrameClipIndex", -1);

            model->setUseProcessingRegion(useProcessingRegion);
            model->setRoi(roi);
            model->setHorizontalBlocks(ccols);
            model->setSmoothingSigma(sigma);
            model->setProcessingChannels(channels);
            GColorBreathingTool->setRefFrame(refFrameClipIndex);
            GColorBreathingTool->setAnchorFrameOffsets({headAnchorOffset, tailAnchorOffset});
            GColorBreathingTool->makeTileBoxes();
        }
    }
    return 0;
}

void ClipPreprocessData::updateZonalRenderFlag(const vector<int> &cleanFrameIndices, const vector<int> &dirtyFrameIndices)
{
    if (dirtyFrameIndices.size() > 0)
    {
        // NOTE: dirtyFrameIndices must be sorted ascending
        auto in = dirtyFrameIndices.front();
        auto out = dirtyFrameIndices.back();
        auto segments = getZonalDatabase().getSegmentRows(in, out);
        auto i = 0;
        auto n = dirtyFrameIndices.size();
        for (auto&segment : segments)
        {
            if (i >= n)
            {
                break;
            }

            segment.setRendered(false);
            auto v = dirtyFrameIndices[i++];
            auto count = 0;
            if (segment.StartFrame == v)
            {
                while (v < segment.EndFrame)
                {
                    count++;
                    if (i >= n)
                    {
                        break;
                    }

                    v = dirtyFrameIndices[i++];

                }
            }

            segment.setRendered(count >= (segment.EndFrame - segment.StartFrame));
            // back up
            i--;
        }

        getZonalDatabase().setSegmentRows(segments);
    }


    // NOTE: cleanFrameIndices must be sorted ascending
    if (cleanFrameIndices.size() == 0)
    {
        return;
    }

    auto in = cleanFrameIndices.front();
    auto out = cleanFrameIndices.back();
    auto segments = getZonalDatabase().getSegmentRows(in, out);

    auto i = 0;
    auto n = cleanFrameIndices.size();

    for (auto&segment : segments)
    {
        if (i >= n)
        {
            break;
        }

        auto v = cleanFrameIndices[i++];
        if ((segment.StartFrame >= v) && (v < segment.EndFrame))
        {
            segment.setRendered(false);

            if (segment.isPrerendered())
            {
                segment.setPreprocessed(false);
                segment.StartFrame = -1;
                segment.EndFrame = -1;
            }

            segment.setPrerendered(false);

            // Skip the next ones
            while (v < segment.OutFrame)
            {
                v = cleanFrameIndices[i++];
            }

            // back up
            i--;
        }
    }

    getZonalDatabase().setSegmentRows(segments);
}

void ClipPreprocessData::updatePrerenderedFlags()
{
    auto model = GColorBreathingTool->getZonalFlickerModel();
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();
    legalizeMarks(markIn, markOut);
    updatePrerenderedFlags(markIn, markOut);
}

vector<ZonalParamsStruct>ClipPreprocessData::getZonalParamsStruct(int markIn, int markOut)
{
    vector<ZonalParamsStruct>result;
    auto &zonalDatabase = getZonalDatabase();

    legalizeMarks(markIn, markOut) ;
    auto rows = zonalDatabase.getSegmentRows(markIn, markOut);
    if (rows.size() == 0)
    {
        return result;
    }

    for (auto&row : rows)
    {
        ZonalParamsStruct zonalParamsStruct;
        auto zonalParams = zonalDatabase.getParameters(row.ParamId);

        zonalParamsStruct.ProcessType = (int)zonalParams.getZonalProcessType();
        zonalParamsStruct.crows = zonalParams.getVerticalBlocks();
        zonalParamsStruct.ccols = zonalParams.getHorizontalBlocks();
        zonalParamsStruct.FrctOverlap = zonalParams.getOverlap();
        zonalParamsStruct.nbins = zonalParams.getNumberBins();
        zonalParamsStruct.FiltRad = zonalParams.getFilterRadius();
        zonalParamsStruct.MaxFlicker = 10000; // TBD fix this
        zonalParamsStruct.NumIntensityLevels = zonalParams.getNumIntensityLevels();
        zonalParamsStruct.TMotion = zonalParams.getMotion();
        zonalParamsStruct.PdfSig = zonalParams.getPdfSigma();
        zonalParamsStruct.SmoothRad = zonalParams.getSmoothingRadius();
        zonalParamsStruct.SmoothSig = zonalParams.getSmoothingSigma();
        zonalParamsStruct.Downsample = zonalParams.getDownsample();

        auto maxDataValue = int(zonalParams.getMaxValue()) + 1;
        zonalParamsStruct.bitShift = 16 - (int)std::ceil(std::log2(maxDataValue));

        // Non-stu parameters
        // Fudge the start frames because the marks don't have to cover a cut;
        zonalParamsStruct.InFrame = std::max<int>(markIn, row.InFrame);
        zonalParamsStruct.OutFrame = std::min<int>(markOut, row.OutFrame);
        zonalParamsStruct.TotalFrames = zonalParamsStruct.OutFrame - zonalParamsStruct.InFrame;

        zonalParamsStruct.Roi = zonalParams.getAnalysisRoi();
        zonalParamsStruct.Width = zonalParamsStruct.Roi.width;
        zonalParamsStruct.Height = zonalParamsStruct.Roi.height;

        zonalParamsStruct.segmentRow = row;
        zonalParamsStruct.ImageSize = zonalParams.getImageSize();

        result.push_back(zonalParamsStruct);
    }

    return result;
}

void ClipPreprocessData::updatePrerenderedFlags(int in, int out)
{
    // Segments are sorted by IN_FRAME
    auto segments = getZonalDatabase().getSegmentRows(in, out);
    auto frameList = getSystemAPI()->getVideoFrameList();

    for (auto&segment : segments)
    {
        auto isPrerendered = false;
        for (auto frameIndex = segment.InFrame; frameIndex < segment.OutFrame; frameIndex++)
        {
            auto frame = frameList->getFrame(frameIndex);
            auto field = frame->getField(0);
            assert(field != nullptr);
            if (field->isDirtyMedia())
            {
                isPrerendered = true;
                break;
            }
        }

        segment.setPrerendered(isPrerendered);
    }

    getZonalDatabase().setSegmentRows(segments);
}

void ClipPreprocessData::saveDeltaValues(const ZonalParamsStruct &params, const vector<vector<float>> &componentDeltas)
{
    auto fileName = params.segmentRow.getDeltaFileName();

    auto localsegmentRow = params.segmentRow;

    // The in and out have the actually processed data
    // Set the processed part as that
    localsegmentRow.StartFrame = params.InFrame;
    localsegmentRow.EndFrame = params.OutFrame;

    std::ofstream oFile(fileName, std::ios::binary | std::ios::out);

    for (auto &deltaValues : componentDeltas)
    {
       oFile.write((char *)deltaValues.data(), deltaValues.size() * sizeof(float));
    }

    auto &delta = componentDeltas[0];
    auto bpf = int(params.crows*params.ccols);
    auto offset = 7;
    vector<int> test;
    for (auto i = 0; i < bpf; i++)
    {
        test.push_back(int(delta[i + bpf*offset]));
    }

    // Ugly, set the status to preprocessed only
    localsegmentRow.Status = int(FrameStatus::preprocessedValue);
    getZonalDatabase().setSegmentRow(localsegmentRow);
}

void ClipPreprocessData::saveSmoothedValues(const ZonalParamsStruct &params, const vector<vector<float>> &componentDeltas)
{
    // TBD, get rid of smoothed file
    auto fileName = params.segmentRow.getRepairFileName();
    auto localsegmentRow = params.segmentRow;

    // The in and out have the actually processed data
    // Set the processed part as that
    localsegmentRow.StartFrame = params.InFrame;
    localsegmentRow.EndFrame = params.OutFrame;

    std::ofstream oFile(fileName, std::ios::binary | std::ios::out);

    // Create a baby header
    int32_t version = 2;
    int32_t id = params.segmentRow.Id;
    ZonalParameters result;
    oFile.write(reinterpret_cast<char *>(&version), sizeof(version));
    oFile.write(reinterpret_cast<char *>(&id), sizeof(id));
    float data[21];

    // Create the parameters
    int i = 0;
    data[i++] = params.InFrame;
    data[i++] = params.OutFrame;
    data[i++] = params.ProcessType;
    data[i++] = params.crows;
    data[i++] = params.ccols;
    data[i++] = params.FrctOverlap;
    data[i++] = params.nbins;
    data[i++] = params.FiltRad;
    data[i++] = params.MaxFlicker;
    data[i++] = params.NumIntensityLevels;
    data[i++] = params.TMotion;
    data[i++] = params.PdfSig;
    data[i++] = params.SmoothRad;
    data[i++] = params.SmoothSig;
    data[i++] = params.Downsample;

    auto imageSize = params.ImageSize;
    data[i++] = imageSize.width;
    data[i++] = imageSize.height;

    // Fix matlab fuckup
    auto &roi = params.Roi;
    data[i++] = roi.x;
    data[i++] = roi.y;
    data[i++] = roi.width;
    data[i++] = roi.height;

    oFile.write((char *)data, 21 * sizeof(float));

    // Now write the control points
    std::vector<int>ctrlRows(int(params.crows));
    std::vector<int>ctrlCols(int(params.ccols));

    // TODO: THESE TWO ALMOST AGREE, THEY SHOULD BE PERFECT  Reason is one is computed at low res
    ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseFindControlPosition(params, params.Roi.x, ctrlCols, params.Roi.y, ctrlRows, imageSize);
///    DeflickerBaseFindControlPosition_0(params, params.Roi.x, ctrlCols.data(), params.Roi.y, ctrlRows.data());
    for (auto&ctrlRow : ctrlRows)
    {
        MTIassert((ctrlRow >= 0) && (ctrlRow < imageSize.height));

        for (auto&ctrlCol : ctrlCols)
        {
            MTIassert((ctrlCol >= 0) && (ctrlCol < imageSize.width));
            oFile.write((char *)&ctrlCol, sizeof(int32_t));
            oFile.write((char *)&ctrlRow, sizeof(int32_t));
        }
    }

    for (auto &smoothedValues : componentDeltas)
    {
       oFile.write((char *)smoothedValues.data(), smoothedValues.size() * sizeof(float));;
    }

    getZonalDatabase().setSegmentRow(localsegmentRow);
}

vector<vector<float>> ClipPreprocessData::loadDeltaValues(const ZonalParamsStruct &zonalParamsStruct)
{
    auto localsegmentRow = zonalParamsStruct.segmentRow;
    auto fileName = localsegmentRow.getDeltaFileName();
    auto in = localsegmentRow.StartFrame;
    auto out = localsegmentRow.EndFrame;

    auto channels = int(zonalParamsStruct.ProcessType);
    vector<vector<float>> componentDeltas;

    std::ifstream inFile(fileName, std::ios::binary | std::ios::in);
    if (inFile.good() == false)
    {
        throw std::runtime_error("Error opening " + fileName);
    }

    auto totalDeltaValues =  int(zonalParamsStruct.crows * zonalParamsStruct.ccols * (out - in));
    for (auto i : range(channels))
    {
       vector<float> deltaValues(totalDeltaValues);
       inFile.read((char *)deltaValues.data(), deltaValues.size() * sizeof(float));
       componentDeltas.push_back(deltaValues);
    }

    int floatsRead = inFile.tellg()/sizeof(float);
    if (floatsRead != (totalDeltaValues*channels))
    {
         throw std::runtime_error("Read error, file too small " + fileName);
    }

    return componentDeltas;
}

bool ClipPreprocessData::resmoothPreprocessorData(const ZonalParameters &zonalParams, int refFrameClipIndex, std::pair<int, int> anchorOffsets, int smoothingType)
{
    auto markIn = zonalParams.getInFrame();
    auto markOut = zonalParams.getOutFrame();
    try
    {
        auto zonalStructs = getZonalParamsStruct(markIn, markOut);

        for (auto&zonalStruct : zonalStructs)
        {
            ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseStart();
            ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseInit(zonalStruct);
            auto expectedSize = ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseGetDeltaSize(zonalStruct);
            auto deltaValuesComponents = loadDeltaValues(zonalStruct);

            // All deltaValues are same size, so just check first
            if (deltaValuesComponents[0].size() != expectedSize)
            {
                throw std::runtime_error("Number of preprocesser values read are different than expected");
            }

            // Change the smoothing
            zonalStruct.SmoothRad = zonalParams.getSmoothingRadius();
            zonalStruct.SmoothSig = zonalParams.getSmoothingSigma();

            auto numberComponents = deltaValuesComponents.size();
            vector < vector < float >> componentSmoothed(numberComponents);

//           for (auto i : range(numberComponents))
            auto f = [&](int i)
            {
                vector<float>smoothed(expectedSize);
                ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseSmoothAll(zonalStruct, refFrameClipIndex, anchorOffsets, deltaValuesComponents[i].data(), smoothed.data(), smoothingType);
                componentSmoothed[i] = smoothed;
            };

            IpaStripeStream iss;
            iss << numberComponents << efu_job(f);
            iss.stripe();

            saveSmoothedValues(zonalStruct, componentSmoothed);

            ThinPlateSplineSurface::zonalFlickerBaseWrapper[0].deflickerBaseEnd();
        }
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << string(ex.what()));
        return false;
    }

    return true;
}

CZonalFlickerDB &ClipPreprocessData::getZonalDatabase() const
{
    auto constThis = const_cast<ClipPreprocessData*>(this);
    return constThis->_db;
}

void ClipPreprocessData::clearPreprocessorDataBetweenMarks(int markIn, int markOut)
{
    legalizeMarks(markIn, markOut);
    auto segmentRows = getZonalDatabase().getSegmentRows(markIn, markOut);
    for (auto&row : segmentRows)
    {
        row.setPreprocessed(false);
        auto repairFileName = row.getRepairFileName();
        if (DoesFileExist(repairFileName) == true)
        {
           DeleteFile(repairFileName.c_str());
        }

        auto deltaFileName = row.getDeltaFileName();
        if (DoesFileExist(deltaFileName) == true)
        {
            DeleteFile(deltaFileName.c_str());
        }

        auto anchorsFileName = row.getAnchorsFileName({markIn, markOut});
        DBTRACE(anchorsFileName);
        if (DoesFileExist(anchorsFileName) == true)
        {
            DBTRACE(anchorsFileName);
            DeleteFile(anchorsFileName.c_str());
        }

        row.StartFrame = -1;
        row.EndFrame = -1;
    }

    getZonalDatabase().setSegmentRows(segmentRows);
    computeIsDefaultPreprocessingDone();
}

bool ClipPreprocessData::updateRow(const SegmentRow &segmentRow)
{
    getZonalDatabase().setSegmentRow(segmentRow);

    return true;
}

 bool ClipPreprocessData::isPreprocessingDoneForRow(const SegmentRow &row) const
 {
	 if (row.Id <= 0)
	 {
		 return false;
	 }

	 // See if row is defined
	 if ((row.InFrame < 0) || (row.OutFrame < 0))
	 {
		 return false;
	 }

	 if ((row.StartFrame != row.InFrame) || (row.EndFrame != row.OutFrame))
	 {
		 return false;
	 }

	 // Database seys it exists, so just check if the file does
	 // It always should, unless an external event deleted it
	 return doRepairFilesExist(row);
 }

 void ClipPreprocessData::saveAnchorDeltaValues(const SegmentRow &row, const AnchorDeltaValues &anchorValues, const SceneCut &markCut)
 {
   if (anchorValues.isEmpty())
   {
   	return;
   }

    auto fileName = row.getAnchorsFileName(markCut);

    // Massage control points to a vector so we can write it out
    // In the best of all worlds, we would do this in the matio library
    vector<Ipp32s> positionVector;
    for (auto &position : anchorValues.controlPositions)
    {
         positionVector.push_back(position.x);
         positionVector.push_back(position.y);
    }

    vector<Ipp32s> sceneCutVector =  { anchorValues.getSceneCut().first, anchorValues.getSceneCut().second};
    MatIO::saveListToMatFile(fileName, anchorValues.headDeltaValues[0], "headDeltaValues0",
												   anchorValues.headDeltaValues[1], "headDeltaValues1",
												   anchorValues.headDeltaValues[2], "headDeltaValues2",
                                       anchorValues.tailDeltaValues[0], "tailDeltaValues0",
                                       anchorValues.tailDeltaValues[1], "tailDeltaValues1",
                                       anchorValues.tailDeltaValues[2], "tailDeltaValues2",
                                       positionVector, "controlPositions",
    												sceneCutVector, "sceneCut");
 }


void ClipPreprocessData::clearAllAnalysis()
{
try
{
     auto segmentList = getZonalDatabase().getSegmentRows("");
     for (auto &segmentRow : segmentList)
     {
         segmentRow.StartFrame = -1;
         segmentRow.EndFrame = -1;
     }

     getZonalDatabase().setSegmentRows(segmentList);
}
catch (const std::exception &ex)
{
   TRACE_0(errout << ex.what());
}

}

void ClipPreprocessData::setBetweenMarksToParameters(const ZonalParameters &zonalParams)
{
   auto markIn = getSystemAPI()->getMarkIn();
   auto markOut = getSystemAPI()->getMarkOut();
   legalizeMarks(markIn, markOut);

   auto rows = getZonalDatabase().getSegmentRows("");

   auto id = getZonalDatabase().insertParametersIfNecessary(zonalParams);
   for (auto &row : rows)
   {
      row.ParamId = id;
	   row.ReferenceFrame = zonalParams.getReferenceFrame();
    	row.HeadAnchorOffset = zonalParams.getHeadAnchorOffset();
    	row.TailAnchorOffset = zonalParams.getTailAnchorOffset();
   }

   getZonalDatabase().setSegmentRows(rows);
}

bool ClipPreprocessData::isSegmentAtFramePreprocessed(int frameIndex) const
{
	 auto row = getSegmentRowFromFrameIndex(frameIndex);
    if (row.Id <= 0)
    {
       return false;
    }

    // See if the preprocessing marks have been set
    if ((row.StartFrame < 0) || (row.EndFrame < 0))
    {
        TRACE_2(errout << "Row small");
        return false;
    }

    // See if data file exists (it always should, unless an external event deleted it
    if (doRepairFilesExistCached(row) == false)
    {
        TRACE_2(errout << "no repair files");
        return false;
    }

    // Next check if row has been preeprocessed
    return row.isPreprocessed();
}

bool ClipPreprocessData::isSegmentAtMarksPreprocessed() const
{
    auto markIn = getSystemAPI()->getMarkIn();
    auto markOut = getSystemAPI()->getMarkOut();

    if ((markIn < 0) || (markOut < 0))
    {
       return false;
    }

    if (markIn >= markOut)
    {
       return false;
    }

    // Picking one frame only checks the segment at that frame.
    // Marks can extend over many segments but for now we will only care about the first
    // because the user only cares about the first
    return isSegmentAtFramePreprocessed(markIn);
}
