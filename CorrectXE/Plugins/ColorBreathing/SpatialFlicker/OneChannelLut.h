#ifndef OneChannelLutH
#define OneChannelLutH

#include "IniFile.h"
#include "Ippheaders.h"
#include "MTIsleep.h"
namespace OneChannelLut
{
    vector<float>CreateLookupTable(vector<IppiPoint_32f>bendPoints, int levels);
    vector<float>CreateLookupTable(const vector<Ipp32f> &lut, int levels);
    vector<float>CreateLookupTableCublicSpline(vector<IppiPoint_32f>bendPoints, int levels);
    vector<float>CreateLookupTableCublicSpline(const vector<Ipp32f> &lut, int levels);
}

#endif
