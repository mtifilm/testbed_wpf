/*
   File Name:  err_dewarp.h
   Created: June 18, 2017

   This contains all the error codes for all core_code functions
*/
#ifndef _ERR_CB_H
#define _ERR_CB_H

//-------------------------------------------------------------------------
//
//  Colorbreathing Erros  CB_ERROR__XXX
//

//  Internal error, this happens when the computed frames to process are < 0
#define CB_ERROR_INTERNAL_FRAMES_LESS_THAN_ZERO -9900

//  Internal error, this happens when the computed frames to process are < 0
#define CB_ERROR_INTERNAL_INPUT_BUFFER_IS_NULL -9901

//  Internal error, this happens when the computed frames to process are < 0
#define CB_ERROR_INTERNAL_OUTPUT_BUFFER_IS_NULL -9902

//  User error ROI too small
#define CB_ERROR_USER_ROI_TOO_SMALL -9903

#define CB_REPAIR_DATA_IS_INVALID -9904

#define CB_ERROR_INVALID_MARKS -9905

#define CB_ERROR_NO_ANALYSIS_DATA -9906

#define CB_ERROR_UNKNOWN_APPLY_SURFACE_ERROR -9907

#define CB_ERROR_UNABLE_OPEN_ZONALFLICKER_DATABASE -9908

#define CB_ERROR_UNABLE_WRITE_ZONALFLICKER_DATABASE -9909

#define CB_ERROR_UNABLE_CREATE_TEMP_ZONAL_DATABASE -9910

#define CB_ERROR_UNABLE_WRITE_TEMP_ZONAL_DATABASE -9911

#define CB_ERROR_PDL_IS_INVALID -9912

#define CB_REPAIR_DATA_WAS_NOT_LOADED -9913;

#define CB_REPAIR_CONTROL_POINTS_INVALID -9914;

#endif
