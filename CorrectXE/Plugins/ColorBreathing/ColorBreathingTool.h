// ---------------------------------------------------------------------------

#ifndef ColorBreathingToolH
#define ColorBreathingToolH

#include "UserInput.h"
#include "ToolObject.h"
#include "IniFile.h"
#include "HRTimer.h"
#include "ThreadLocker.h"
#include <fstream>
#include "ColorBreathingModel.h"
#include "SpatialFlickerModel.h"
#include "ColorPickTool.h"
#include "ClipPreprocessData.h"
#include "RegionOfInterest.h"
#include "AnchorDeltaValues.h"

#define COLORBREATHING_TOOL_NUMBER 22

#define CB_CMD_NO_OP                      0
#define CB_CMD_ADD_REF_FRAME              1
#define CB_CMD_DEL_REF_FRAME              2
#define CB_CMD_CLEAR_REF_GROUP            3
#define CB_CMD_GOTO_NEXT_REF_FRAME		   4
#define CB_CMD_GOTO_PREV_FRAME            5
#define CB_CMD_COLOR_ANALYZE	   	      6
#define CB_CMD_COLOR_BREAK                7
#define CB_CMD_START_SELECT               8
#define CB_CMD_END_SELECT                 9
#define CB_CMD_REJECT_FIXES              10
#define CB_CMD_TOGGLE_FIXES              11
#define CB_CMD_PRESET_1                  12
#define CB_CMD_PRESET_2                  13
#define CB_CMD_PRESET_3                  14
#define CB_CMD_PRESET_4                  15
#define CB_CMD_RENDER_GRAPH              16
#define CB_CMD_CENTER_SEGMENT            17
#define CB_CMD_USE_ROI                   18
#define CB_CMD_TOGGLE_ROI_USE            19
#define CB_CMD_LOCK_ROI                  20
#define CB_CMD_RENDER_MARKED             21
#define CB_CMD_ACCEPT_FIXES              22
#define CB_CMD_STOP                      23
#define CB_CMD_PAUSE_OR_RESUME           24
#define CB_CMD_SAVE_PRESET               25
#define CB_CMD_DEBUG_VIEW				 26
#define SF_CMD_TOGGLE_SHOW_BOXES         27
#define SF_CMD_TOGGLE_HIDE_ALL           28

#define SF_CMD_TURN_COLOR_PICKER_ON     100
#define SF_CMD_TURN_COLOR_PICKER_OFF    101


#define COLORBREATHING_TOOL_NAME "Deflicker"

#define CB_MAX_THREAD_COUNT 24

// Color breathing Tool Control States
//enum class EMultiControlState
//{
//    ALLOCATE = 0,
//    AQUIRE, // Start is a preprocessing aquire
//    RENDER, // Finally the render
//    COMPLETED
//};

// Stabilizer Tool Control States
enum class EMultiControlState
{
   IDLE = 0,
   GOT_AN_ERROR,
   NEED_TO_RUN_ACQUIRE,
   ACQURING,
   ACQURING_PAUSED,
   ACQURING_COMPLETE,
   NEED_TO_RUN_RENDERER,
   RENDERING,
   RENDERING_COMPLETE
};

#define CB_TOOL_GRAPH_CMD                   TOOL_PROCESSING_CMD_CUSTOM1
#define FLICKER_TOOL_PREPROCESS_CMD         TOOL_PROCESSING_CMD_CUSTOM2
#define FLICKER_TOOL_RENDER_CMD             TOOL_PROCESSING_CMD_CUSTOM3
#define FLICKER_TOOL_PREVIEW_CMD            TOOL_PROCESSING_CMD_CUSTOM4


enum class SubToolType
{
    Invalid, GlobalFlicker, ZonalFlicker, };

class ColorBreathingTool : public CToolObject
{
public:

    ColorBreathingTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
    virtual ~ColorBreathingTool();

    // Tool startup and shutdowm
    virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
    virtual int toolShutdown();
    virtual int toolActivate();
    virtual int toolDeactivate();
    virtual int onToolCommand(CToolCommand &toolCommand);
    virtual bool DoesToolPreserveHistory();
    virtual CToolProcessor* makeToolProcessorInstance(const bool *);
    virtual int RunFrameProcessing(int newResumeFrame);
    virtual int onUserInput(CUserInput &userInput);
    virtual int onTopPanelRedraw();

    virtual int toolHide();
    virtual bool toolHideQuery();
    virtual int toolShow();
    virtual bool IsShowing(void);
    virtual int onNewClip();
    virtual int onNewMarks();
    virtual int OnStopPDLRendering();
    virtual int onPDLExecutionComplete();
    virtual int onChangingClip();
    virtual int onDeletingOpenedClip();
    virtual int onRedraw(int frameIndex);
    virtual int onHeartbeat();
    virtual int onTimelineVisibleFrameRangeChange();
    virtual int onSwitchSubtool();
    virtual int onMouseMove(CUserInput &userInput);
    virtual void NotifyMaskChanged(const string &newMaskAsString);
    virtual void NotifyMaskVisibilityChanged();
    virtual void UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag);

    // Hacks for multi process tools
    virtual EToolControlState GetToolProcessingControlState();

    virtual int onTopPanelMouseDown(int x, int y);
    virtual int onTopPanelMouseDrag(int x, int y);
    virtual int ProcessSingleFrame(EToolSetupType toolSetupType);
    virtual int TryToResolveProvisional();
    ////virtual int onPreviewDisplayBufferDraw(int frameIndex, int width, int height, unsigned int *pixels);
    virtual int onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes);
    int onPreviewHackDrawCB(int frameIndex, unsigned short *frameBits);
	 int onPreviewHackDrawZonal(int frameIndex, unsigned short *frameBits);

	 virtual int MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
													 const CAutotoolStatus& newToolStatus,
													 int newResumeFrame);

    virtual int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
    int onCaptureGlobalFlickerPDLEntry(CPDLElement &pdlEntryToolParams);
    int onCaptureZonalFlickerPDLEntry(CPDLElement &pdlEntryToolParams);

    virtual int onGoToPDLEntry(CPDLEntry &pdlEntry);
    int onGoToGlobalFlickerPDLEntry(CPDLEntry &pdlEntry);
    int onGoToZonalFlickerPDLEntry(CPDLEntry &pdlEntry);

    virtual int onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag, const std::pair<int, int> &frameRange);
    virtual int onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message);
    int BeginUserSelect();
    int EndUserSelect();

    void UpdateBreaksFromClip();
    vector<int>getBreaksFromCurrentClip();
    void UpdateGuiReferenceFrameList();
    void SetRGBDataProcessing();
    void SetColorBreathingProcessing();
    void SetFlickerPreProcessing();
    void SetFlickerRender();
    void ButtonCommandHandler(EToolProcessingCommand buttonCommand);
    void ResetResumeFrameIndex(int resumeFrame);

    string IniFileName;

    void LoadAllSettingFromIni(const string &iniFileName);
    void SaveAllSettingFromIni(const string &iniFileName);

    ColorBreathingModel *getGlobalFlickerModel() const ;
    SpatialFlickerModel *getZonalFlickerModel() const ;

    // this is done wrong because it allows destructor to be called
    // should be fixed!!! this was a bad idea
    ZonalParameters &getZonalFlickerParameters()
    {
       return *dynamic_cast<ZonalParameters*>(getZonalFlickerModel());
    }

    void updateZonalFlickerParameters();

    CBHook ClipChanging; // Needed to call back on a change
    CBHook ClipChange; // Needed to call back on a change
    CBHook MarksChange; // Needed to call back on a change

    DEFINE_CBHOOK(ClipTimelineEventChanged, ColorBreathingTool);
    DEFINE_CBHOOK(DirtyMediaHasChanged, ColorBreathingTool);
    int GetPreviewFrameIndex(void);

    RECT getZonalFlickerRoi() const
    {
       return _zonalFlickerRoi;
    };

    void setZonalFlickerRoi(const RECT &rect);

    bool isZonalFlickerRoiDefined()
    {
       return (_zonalFlickerRoi.left < _zonalFlickerRoi.right) && (_zonalFlickerRoi.top < _zonalFlickerRoi.bottom);
    }

    // This is generic and call the other depending on tool subtype
    bool getUseProcessingRegion() const ;
    void setUseProcessingRegion(bool value);

    bool getGlobalFlickerUseProcessingRegion() const ;
    void setGlobalFlickerUseProcessingRegion(bool value);

    bool getZonalFlickerUseProcessingRegion() const ;
    void setZonalFlickerUseProcessingRegion(bool value);

    // This is generic and call the other depending on tool subtype
    bool GetIsRoiDisplayed() const ;
    void SetIsRoiDisplayed(const bool newDisp);

    void setIsGlobalFlickerRoiDisplayed(const bool newDisp);

    bool getIsGlobalFlickerRoiDisplayed() const {return _isGlobalFlickerRoiDisplayed;}

    void setIsZonalFlickerRoiDisplayed(const bool newDisp);

    bool getIsZonalFlickerRoiDisplayed() const {return _isZonalFlickerRoiDisplayed;}

    // This is generic and call the other depending on tool subtype
    void setUseAnalysisRegion(const bool value);
    bool getUseAnalysisRegion() const ;

    void setColorBreathingUseAnalysisRegion(const bool value);
    bool getColorBreathingUseAnalysisRegion() const ;

    void setZonalFlickerUseAnalysisRegion(const bool value);
    bool getZonalFlickerUseAnalysisRegion() const ;

    bool GetIsRoiLocked() const ;
    void SetIsRoiLocked(const bool newLock);

    bool GetInPreviewMode() const ;
    bool GetInGlobalFlickerPreviewMode() const ;
    bool GetInZonalFlickerPreviewMode() const ;

    void SetInPreviewMode(const bool mode);
    void SetInGlobalFlickerPreviewMode(const bool mode);
    void SetInZonalFlickerPreviewMode(const bool mode);

    void EnterColorPickMode(void);
    void ExitColorPickMode(void);
    bool IsInColorPickMode(void);

    bool areMarksValid();

    CBHook BoundBoxHasChanged;

    EMultiControlState getControlState(void);
    void setControlState(EMultiControlState newControlState);
    void AddReferenceFrameAtCurrentIndex(void);
    void DeleteReferenceFrameAtCurrentIndex(void);
    void DeleteReferenceFrame(const ReferenceKeyframeData &referenceKeyframeData);
    void DeleteReferenceFrameInCurrentBreak(void);
    void AcceptFix(void);
    int GetCurrentFrameIndex() const ;
    int getTotalFrames();

    bool CanWeToggle();
    bool CanWeToggleCB();
    bool CanWeToggleSF();

    bool CanWeAddReferenceFrame();
    void RenderedConsistencyCheck();
    bool CanWeRender();
    bool CanWeRenderColorBreathing();
    bool CanWeRenderZonalFlicker();

    string GetRgbFileNameFromClip();
    void InitializeDataFromClip();

    bool IsZonalFlickerSelected() const {return _subToolType == SubToolType::ZonalFlicker;}

    bool IsColorBreathingSelected() const {return _subToolType == SubToolType::GlobalFlicker;}
    void SetSubToolType(SubToolType subToolType);

    SubToolType getSubToolType() const {return _subToolType;}

    void makeTileBoxes();
    void hideDisplayOverlay(void);
    void showOverlays(void);
    void setOverlayDisplayList(const OverlayDisplayList &newList);

    // Kludge to kill last preprocessor stage
    bool getAbortPreprocessingFlag() const {return _abortPreprocessing;}

    void setAbortPreprocessingFlag(bool value) {_abortPreprocessing = value;}

    ////////////////  Blue Box code
    bool FindBoundingBox(int R = 25, int G = 25, int B = 25, int X = 0, int Y = 0, int slack = 50);

    ///   RECT getZonalFlickerRoi() const{ return IppArrayConvert::convertToRect(getZonalFlickerModel()->getRoi()); }

    // TBD: remove next two calls
    bool getZonalFlickerUseRoi() const {return true;}

    void setZonalFlickerUseRoi(bool value) {}

    bool getZonalFlickerShowBoxes() const {return _zonalFlickerShowBoxes;}
    void setZonalFlickerShowBoxes(bool value);

    bool getIsZonalFlickerRoiLocked() const {return _isZonalFlickerRoiLocked;}

    void setIsZonalFlickerRoiLocked(bool value) {_isZonalFlickerRoiLocked = value;}

    bool getIsGlobalFlickerRoiLocked() const {return _isGlobalFlickerRoiLocked;}

    void setIsGlobalFlickerRoiLocked(bool value) {_isGlobalFlickerRoiLocked = value;}

    int getZonalFlickerSmoothingType() const {return _zonalFlickerSmoothingType;}
    void setZonalFlickerSmoothingType(int value) {  _zonalFlickerSmoothingType = value;}

    /// KLUDGE IN UNTIL WE DECIDE WHICH MODE TO USE
    // bool getShowBoundingBox(void) const { return _showBoundingBox; }
    // void setShowBoundingBox(const bool newDisp);
    //
    bool getUseColorBreathingRoi(void) const {return _useColorBreathingRoi;}

    void setUseColorBreathingRoi(const bool value) {_useColorBreathingRoi = value;}

    void showBoundingBoxIfNeeded(void);
    int generateZonalPreprocessData();
    int generateZonalPreprocessDataIfNecessary();
    void resmoothZonalFlickerDataIfNecessary();

    int resmoothZonalFlickerData();
    void rereadZonalFlickerData();

    string getClipPath() const ;

    bool isToolActive() const {return _toolActive;}
    bool getRenderInProgress() const {return _renderInProgress; }
    void setRenderInProgress(bool value) { _renderInProgress = value; }

    ZonalProcessType getZonalProcessType() const {return getZonalFlickerModel()->getZonalProcessType();}
    void setZonalProcessType(ZonalProcessType value);

    string getDeflickerPreprocessorExeName();
    string getDeflickerSmoothingExeName();

    void setToolProcessingControlState(EToolControlState value) {_toolProcessingControlState = value;}

    EToolControlState getToolProcessingControlState() const {return _toolProcessingControlState;}

    RECT getColorBreathingAnaysisRoi() const ;
    RECT getFullImageRoi() const ;

    void resetZonalRoi();

    ClipPreprocessData &getClipPreprocessData() {return _clipPreprocessData;}
    string getImageFileName(int frame);
    bool loadZonalDataIntoModelIfNecessary(int frameIndex);

    void UnitTest();
    vector<DeflickerParamsStruct> getDeflickerParametersSegments();

    int getRenderState() const {return _renderState; }

    void onlyRunZonalPreprocessing();

    const unsigned char *getMask(int frameIndex);
    void initializeMask() {_mask = nullptr; }
    void roiToMask();

   void setRefFrame(int refFrameClipIndex);
   int getRefFrame() const;

   void loadZonalParametersIfNecessary();

   void setAnchorFrameOffsets(std::pair<int, int> anchorOffsets);
   std::pair<int, int> getAnchorFrameOffsets() const;
   void findAnchorDeltaValuesIfNecessary(int frameIndex);

   AnchorDeltaValues computeAnchorDeltaValues(const SceneCut &sceneCut);

	void updateDatabaseSegmentAtIndex(int index);

   // Special read of the analysis ROI
   Ipp16uArray readFrameNormalizeRoi(int frameIndex, Ipp16uArray &scratchArray);

protected:
    void DestroyAllToolSetups(bool notIfPaused);
    int BeginUserSelectColorBreathing();
    int EndUserSelectColorBreathing();

    OverlayDisplayList _overlayList;

    int BeginUserSelectZonalFlicker();
    int EndUserSelectZonalFlicker();

    // Methods
private:

    int startAcquire();
    int finishAcquire();
    int startRendering();
    int finishRendering();

    string getDataFilePath();

	 AnchorDeltaValues findAnchorDeltaValues(const SceneCut &sceneCut);

   /// string createPreprocessFile();
    int RunFrameFlickerPreProcessing(int newResumeFrame);
    int RunFrameFlickerRender(int newResumeFrame);

    void addSimpleBox(const IppiRect &rect, TColor color, int id = -1);

    void readParametersFromDesktopIni(ClipSharedPtr &clip);
    void writeParametersToDesktopIni(ClipSharedPtr &clip);
    void readClipBoundingBox(void);
    void writeClipBoundingBox(void);

    void setBoundingBoxInternal(const RECT &newBox);
    void DisplayRoi();
    void DisplayRoiColorBreathing();
    void DisplayRoiZonalFlicker();
    void PreviewFrame();
    void GotoNextReferenceFrame();
    void GotoPreviousReferenceFrame();
    void UpdateRenderedFramesThatWereDiscarded(int frameIn, int frameOut);
    bool CanWeRenderDisplayError();

    ClipPreprocessData _clipPreprocessData;

    // Variables
    CColorPickTool *ColorPickTool;

    SubToolType _subToolType = SubToolType::GlobalFlicker;

    DEFINE_CBHOOK(ColorPickerColorChanged, ColorBreathingTool);
    DEFINE_CBHOOK(ColorPickerColorPicked, ColorBreathingTool);
    bool CheckAndReportErrorIfGraphingIsLegal();
    bool CheckIfFramesAreRenderedBetweenMarks();
    bool CheckIfFlickerPreprocessingIsLegal();
    bool CheckIfFlickerRenderIsLegal();

    const unsigned char *_mask = nullptr;
    CRegionOfInterest _maskROI;

    int _resumeFrameIndex; // to resume after a pause
    int _previewFrameIndex;
    bool _abortPreprocessing;
    bool _renderInProgress = false;

    bool _needToTellGuiWeJustStoppedPlaying = false;
    bool _multiFrameRenderInProgress = false;
    bool _needToTellGuiThatFrameHasChanged = false;
    bool _needToTellGuiThatGuiReferenceFramesHaveChanged = false;
    bool _deferredClipChangeFlag = false;
    bool _initializeDataFromClipOnHeartBeat = false;

    bool _toolActive;
    int _currentFrameIndex;
    bool _doingOnRedraw;
    bool _isGpuUsed = false;

// ROI mode
    bool _previousMaskToolEnabled;
    // ROI mode stuff
    bool _maskIsInROIMode = false;
    bool _maskWasInROIMode = true;
    bool _needToClearTheMask = false;
    bool _lastActionWasGOV = false;
    bool _inhibitTildeKeyProcessing = false;
    bool _nowDrawingMask = false;
    int _autoRoiStartMouseX = -1000;
    int _autoRoiStartMouseY = -1000;
    int _mouseX = -1000;
    int _mouseY = -1000;
    bool _autoRoiModeActivationIsPending = false;

    int _graphToolSetupHandle = -1;
    int _renderToolSetupHandle = -1;
    int _flickerPreProcessingToolSetupHandle = -1;
    int _flickerRenderToolSetupHandle = -1;

    RECT _zonalFlickerRoi = {-1, -1, -1, -1};

    // This is used if there is not ROI baked into the RGBData
    RECT _globalFlickerRoi = {-1, -1, -1, -1};

    RECT _lastROI;

    bool _isGlobalFlickerRoiLocked = false;
    bool _isZonalFlickerRoiLocked = false;

    bool _isGlobalFlickerRoiDisplayed = true;
    bool _isGlobalFlickerRoiDisplayedOld = true; // Old is for user select

    bool _isZonalFlickerRoiDisplayed = true;
    bool _isZonalFlickerRoiDisplayedOld = true; // Old is for user select

    //// bool _isRoiProcessed = true;
    bool _deflickerStopFlag = true;

    bool _useColorBreathingRoi = false;

    // These really should be from the zonal flicker parameter structure

    bool _zonalFlickerShowBoxes = false;
    int _zonalFlickerSmoothingType = 0;

    EMultiControlState _multiControlState = EMultiControlState::IDLE;
    EMultiControlState _nextStateAfterAcquire = EMultiControlState::IDLE;
    int _oldBusyCursor;

    string _referenceKeyFrameFileName = "";

    string _rgbDataFileName;

    ColorBreathingModel *_colorBreathingModel = nullptr;
    SpatialFlickerModel *_zonalFlickerModel = nullptr;

    bool _turnOffHeartBeat = true;
    EToolControlState _toolProcessingControlState = TOOL_CONTROL_STATE_STOPPED;

    bool _runRenderAfterStop = false;

    // Ugly, just tell us what we are doing
#define PROC_TYPE_INVALID -1
#define PROC_TYPE_GENERATE_RGB_VALUES 0
#define PROC_TYPE_RENDER_PROCESS 1
#define PROC_TYPE_FLICKER_PREPROCESS 2
#define PROC_TYPE_FLICKER_RENDER 3

    int _toolProcessorTypeToMake = PROC_TYPE_INVALID;

    int singleFrameToolSetupHandle = -1;
    bool _inGlobalFlickerPreviewMode = false;
    bool _inZonalFlickerPreviewMode = false;
    bool _dirtyMediaHasChanged = false;

#define RENDER_STATE_ENDED           -2
#define RENDER_STATE_DISABLED        -1
#define RENDER_STATE_IDLE             0
#define RENDER_STATE_PAUSED           1
#define RENDER_STATE_AVERAGE          2
#define RENDER_STATE_RUNNING          3
    int _renderState = RENDER_STATE_IDLE;

    // This may seem to be redundent but this is used to check
    // if the new clip is a different size.  Set only in OnNewClip
    MtiSize _currentClipSize;

   // NOTE: Only set (one or both of head and tail anchors) OR (ref frame), NOT BOTH!
   // For all of these, "-1" means NOT SET.
//   int _headAnchorOffset = -1;
//   int _tailAnchorOffset = -1;
//   int _refFrameClipIndex = -1;

    std::pair<int, int> _currentCutRange = {-1, -1};
    int _oldFrameIndex = -1;
};

extern ColorBreathingTool *GColorBreathingTool; // Global from ColorBreathingTool.cpp

// ---------------------------------------------------------------------------
#endif
