//---------------------------------------------------------------------------

#pragma hdrstop

#include "ColorBreathingRenderProc.h"
//---------------------------------------------------------------------------
#include "ColorBreathingModel.h"
#include "ToolProgressMonitor.h"
#include "ClipAPI.h"
#include "Ippheaders.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

ColorBreathingRenderProc::ColorBreathingRenderProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor)
   : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   _flickerProcess = new CFlickerProcess();
   StartProcessThread(); // Required by Tool Infrastructure
}

///////////////////////////////////////////////////////////////////////////////

ColorBreathingRenderProc::~ColorBreathingRenderProc()
{
   delete _flickerProcess;
}

CHRTimer hrt2;

///////////////////////////////////////////////////////////////////////////////

int ColorBreathingRenderProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Make sure the number of the algorithm processing threads
   // is reasonable
   _processingThreadCount = toolIOConfig->processingThreadCount;
   if (_processingThreadCount < 1)
      _processingThreadCount = 1;
   else if (_processingThreadCount > CB_MAX_THREAD_COUNT)
      _processingThreadCount = CB_MAX_THREAD_COUNT;

//   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
//   // number of input and output frames. We read and write one frame
//   // each iteration, and write output to a new buffer, not in place.
//
//   SetInputMaxFrames(0, 1, 1); // 'In' port index,
//   // One frame required for each iteration
//   // One new frame needed every iteration
//
//   SetOutputMaxFrames(0, 1); // 'Out' port index,
//   // One frame written each iteration
//
// ////  SetOutputNewFrame(0,0);
//   SetOutputModifyInPlace(0, 0, false); // 'Out' port index, 'In' port index

   SetInputMaxFrames(0,2,2);
   SetOutputMaxFrames(0,1);
   SetOutputNewFrame(0,0);

   return 0;
}

///////////////////////////////////////////////////////////////////////////////

int ColorBreathingRenderProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   _inFrameIndex = frameRange.inFrameIndex;
   _outFrameIndex = frameRange.outFrameIndex;

   return 0;
}

///////////////////////////////////////////////////////////////////////////////
int ColorBreathingRenderProc::SetParameters(CToolParameters *toolParams)
{
   auto toolParameters = dynamic_cast<ColorBreathingToolParameters *>(toolParams);
   _model = toolParameters->Model;
   _tool = toolParameters->Tool;

   auto imageFormat = GetSrcImageFormat(0);
   auto pixelsCol = imageFormat->getPixelsPerLine();
   auto pixelsRow = imageFormat->getLinesPerFrame();
    MTI_UINT16 caMax[3];
   imageFormat->getComponentValueMax(caMax);

   _flickerProcess->setImageSize(pixelsRow, pixelsCol, caMax[0]);
   _model->CorrectMeanDataForRender();

   return 0;
}

int ColorBreathingRenderProc::DoProcess(SToolProcessingData &procData)
{
	CHRTimer hrt;

	CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
	auto baseOrigImage = inToolFrameBufferB->GetVisibleFrameBufferPtr();
	CToolFrameBuffer *outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
	auto outputImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();

	CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);

	auto frameIndex = inToolFrameBufferB->GetFrameIndex();

	float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
	float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
	int iaCount[TL_CHAN_COUNT];

	_model->GetQuantileCorrection(frameIndex, faX, faY, iaCount);

    auto roi = _model->getProcessingRoi(frameIndex);
	_flickerProcess->RenderV4(outputImage, faX, faY, iaCount, roi);

	_model->MarkRendered(frameIndex);
	outToolFrameBuffer->SetForceWrite(true);
	return 0;
}

int ColorBreathingRenderProc::BeginProcessing(SToolProcessingData & procData)
{
   CAutoErrorReporter autoErr("ColorBreathingRenderProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   auto imageFormat = GetSrcImageFormat(0);
   // Return error if a monochrome image
// INTERNAL FORMAT IS ALWAYS 3 COMPONENTS, tool shouldn't care what the source is!
//   if (imageFormat->getComponentCount() != 3)
//   {
//      autoErr.errorCode = theError.getError();
//      autoErr.msg << "Color Breathing only works on three component images, Image formate components = " <<
//         imageFormat->getComponentCount();
//
//      return -1;
//   }

   SetSaveToHistory(false);

   GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
   GetToolProgressMonitor()->SetStatusMessage("Rendering");
   GetToolProgressMonitor()->StartProgress();
   return 0;
}

int ColorBreathingRenderProc::EndProcessing(SToolProcessingData & procData)
{
   // Did render run to completion or was it aborted?
   if (procData.iteration == (_outFrameIndex - _inFrameIndex))
   {
      GetToolProgressMonitor()->SetStatusMessage("Rendering Complete");
      GetToolProgressMonitor()->StopProgress(true);
   }
   else
   {
      GetToolProgressMonitor()->SetStatusMessage("Stopped");
      GetToolProgressMonitor()->StopProgress(false);
   }

   _model->RGBFrameData.NewProcessedData = true;
   return 0;
}

int ColorBreathingRenderProc::BeginIteration(SToolProcessingData & procData)
{
//   int inputFrameIndexList[1];
//   inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
//   SetInputFrames(0, 1, inputFrameIndexList);
//
//   int outputFrameIndexList[1];
//   outputFrameIndexList[0] = _inFrameIndex + procData.iteration;
//   SetOutputFrames(0, 1, outputFrameIndexList);

   int inputFrameIndexList[2];
   inputFrameIndexList[0] = _inFrameIndex + procData.iteration;
   SetInputFrames(0, 1, inputFrameIndexList);

   // These are the frames that will be the output
   int outputFrameIndexList[1];
   outputFrameIndexList[0] = _inFrameIndex + procData.iteration;
   SetOutputFrames(0, 1, outputFrameIndexList);

   // When finished processing, release the oldest frame.
   SetFramesToRelease(0, 1, inputFrameIndexList);

   return 0;
}

int ColorBreathingRenderProc::EndIteration(SToolProcessingData & procData)
{
   GetToolProgressMonitor()->BumpProgress();
   return 0;
}

int ColorBreathingRenderProc::GetIterationCount(SToolProcessingData & procData)
{
   return _outFrameIndex - _inFrameIndex;
}
