// ---------------------------------------------------------------------------

#include "ReferenceKeyframeData.h"

const string ReferenceFrameChild = "RefFrame";

const string RedEnabledKey = "RE";
const string BlueEnabledKey = "BE";
const string GreenEnabledKey = "GE";

const string ColorForwardsKey = "CF";
const string ColorBackwardsKey = "CB";

const string FlickerForwardsKey = "FF";
const string FlickerBackwardsKey = "FB";
const string RedSmoothingKey = "RS";
const string GreenSmoothingKey = "GS";
const string BlueSmoothingKey = "BS";
const string FrameIndexKey = "FrameIndex";

// ---------------------------------------------------------------------------

#define FS_REFERENCE_TYPE_LOW_REV    0x1
#define FS_REFERENCE_TYPE_LOW_FWD    0x2
#define FS_REFERENCE_TYPE_LOW_R      0x4
#define FS_REFERENCE_TYPE_LOW_G      0x8
#define FS_REFERENCE_TYPE_LOW_B      0x10
#define FS_REFERENCE_TYPE_HIGH_REV    0x20
#define FS_REFERENCE_TYPE_HIGH_FWD    0x40

// Constructor, just initialize the variables
ReferenceKeyframeData::ReferenceKeyframeData()
{
   this->RedEnabled = true;
   this->GreenEnabled = true;
   this->BlueEnabled = true;

   this->ColorForwards = true;
   this->ColorBackwards = true;

   this->FlickerForwards = false;
   this->FlickerBackwards = false;

   this->RedSmoothing = 1;
   this->GreenSmoothing = 1;
   this->BlueSmoothing = 1;

   this->FrameIndex = -2;
}

ReferenceKeyframeData::ReferenceKeyframeData(const ReferenceKeyframeData &r)
{
   *this = r;
}

int ReferenceKeyframeData::ReadPDLEntry(CPDLElement *pdlTA)
{
   this->RedEnabled = pdlTA->GetAttribBool(RedEnabledKey, true);
   this->BlueEnabled = pdlTA->GetAttribBool(BlueEnabledKey, true);
   this->GreenEnabled = pdlTA->GetAttribBool(GreenEnabledKey, true);

   this->ColorForwards = pdlTA->GetAttribBool(ColorForwardsKey, true);
   this->ColorBackwards = pdlTA->GetAttribBool(ColorBackwardsKey, true);
   this->FlickerForwards = pdlTA->GetAttribBool(FlickerForwardsKey, true);
   this->FlickerBackwards = pdlTA->GetAttribBool(FlickerBackwardsKey, true);

   this->RedSmoothing = pdlTA->GetAttribInteger(RedSmoothingKey, 1);
   this->GreenSmoothing = pdlTA->GetAttribInteger(GreenSmoothingKey, 1);
   this->BlueSmoothing = pdlTA->GetAttribInteger(BlueSmoothingKey, 1);

   this->FrameIndex = pdlTA->GetAttribInteger(FrameIndexKey, -1);
   return this->FrameIndex;
}

void ReferenceKeyframeData::AddToPDLEntry(CPDLElement &parent)
{
   CPDLElement *pdlTP = parent.MakeNewChild(ReferenceFrameChild);

   pdlTP->SetAttribBool(RedEnabledKey, this->RedEnabled);
   pdlTP->SetAttribBool(BlueEnabledKey, this->BlueEnabled);
   pdlTP->SetAttribBool(GreenEnabledKey, this->GreenEnabled);

   pdlTP->SetAttribBool(ColorForwardsKey, this->ColorForwards);
   pdlTP->SetAttribBool(ColorBackwardsKey, this->ColorBackwards);
   pdlTP->SetAttribBool(FlickerForwardsKey, this->FlickerForwards);
   pdlTP->SetAttribBool(FlickerBackwardsKey, this->FlickerBackwards);

   pdlTP->SetAttribInteger(RedSmoothingKey, this->RedSmoothing);
   pdlTP->SetAttribInteger(GreenSmoothingKey, this->GreenSmoothing);
   pdlTP->SetAttribInteger(BlueSmoothingKey, this->BlueSmoothing);

   pdlTP->SetAttribInteger(FrameIndexKey, this->FrameIndex);
}

bool ReferenceKeyframeData:: operator == (const ReferenceKeyframeData &g) const
{
   return this->AreParametersTheSame(g) && (this->FrameIndex == g.FrameIndex);
}

bool ReferenceKeyframeData:: operator != (const ReferenceKeyframeData &g) const
{
   return !(*this == g);
}

ReferenceKeyframeData& ReferenceKeyframeData:: operator = (const ReferenceKeyframeData & r)
{
   if (&r == this)
   {
      return *this;
   }

   this->RedEnabled = r.RedEnabled;
   this->GreenEnabled = r.GreenEnabled;
   this->BlueEnabled = r.BlueEnabled;

   this->ColorForwards = r.ColorForwards;
   this->ColorBackwards = r.ColorBackwards;

   this->FlickerForwards = r.FlickerForwards;
   this->FlickerBackwards = r.FlickerBackwards;

   this->RedSmoothing = r.RedSmoothing;
   this->GreenSmoothing = r.GreenSmoothing;
   this->BlueSmoothing = r.BlueSmoothing;

   this->FrameIndex = r.FrameIndex;
	this->Tag = r.Tag;
   return *this;
}

void ReferenceKeyframeData::ReadParameters(CIniFile *ini, const string &iniSectionName)
{
   this->RedEnabled = ini->ReadBool(iniSectionName, "RedEnabled", true);
   this->GreenEnabled = ini->ReadBool(iniSectionName, "GreenEnabled", true);
   this->BlueEnabled = ini->ReadBool(iniSectionName, "BlueEnabled", true);

   this->ColorForwards = ini->ReadBool(iniSectionName, "ColorForwards", true);
   this->ColorBackwards = ini->ReadBool(iniSectionName, "ColorBackwards", true);
   this->FlickerForwards = ini->ReadBool(iniSectionName, "FlickerForwards", true);
   this->FlickerBackwards = ini->ReadBool(iniSectionName, "FlickerBackwards", true);

   this->RedSmoothing = ini->ReadInteger(iniSectionName, "RedSmoothing", 1);
   this->GreenSmoothing = ini->ReadInteger(iniSectionName, "GreenSmoothing", 1);
   this->BlueSmoothing = ini->ReadInteger(iniSectionName, "BlueSmoothing", 1);

   this->FrameIndex = ini->ReadInteger(iniSectionName, "FrameIndex", -1);
   // Fixup for old naming
   if (this->FrameIndex == -1)
   {
      this->FrameIndex = ini->ReadInteger(iniSectionName, "FrameNumber", -1);
   }
}

void ReferenceKeyframeData::WriteParameters(CIniFile *ini, const string &iniSectionName) const
{
   ini->WriteBool(iniSectionName, "RedEnabled", this->RedEnabled);
   ini->WriteBool(iniSectionName, "GreenEnabled", this->GreenEnabled);
   ini->WriteBool(iniSectionName, "BlueEnabled", this->BlueEnabled);

   ini->WriteBool(iniSectionName, "ColorForwards", this->ColorForwards);
   ini->WriteBool(iniSectionName, "ColorBackwards", this->ColorBackwards);
   ini->WriteBool(iniSectionName, "FlickerForwards", this->FlickerForwards);
   ini->WriteBool(iniSectionName, "FlickerBackwards", this->FlickerBackwards);

   ini->WriteInteger(iniSectionName, "RedSmoothing", this->RedSmoothing);
   ini->WriteInteger(iniSectionName, "GreenSmoothing", this->GreenSmoothing);
   ini->WriteInteger(iniSectionName, "BlueSmoothing", this->BlueSmoothing);

   ini->WriteInteger(iniSectionName, "FrameIndex", this->FrameIndex);
}

// Checks are values are equal except the labels
bool ReferenceKeyframeData::AreParametersTheSame(const IPresetParameters &presetParameter) const
{
   auto r1 = (ReferenceKeyframeData *)(&presetParameter);

   return (this->RedEnabled == r1->RedEnabled) && (this->GreenEnabled == r1->GreenEnabled) &&
      (this->BlueEnabled == r1->BlueEnabled) && (this->ColorForwards == r1->ColorForwards) &&
      (this->ColorBackwards == r1->ColorBackwards) && (this->FlickerForwards == r1->FlickerForwards) &&
      (this->FlickerBackwards == r1->FlickerBackwards) && (this->RedSmoothing == r1->RedSmoothing) &&
      (this->GreenSmoothing == r1->GreenSmoothing) && (this->BlueSmoothing == r1->BlueSmoothing);
}
