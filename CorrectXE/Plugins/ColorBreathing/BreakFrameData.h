// ---------------------------------------------------------------------------

#ifndef BreakFrameDataH
#define BreakFrameDataH

#include "ReferenceKeyframeData.h"
#include "PDL.h"
#include <algorithm>
#include <map>

// This class contains the actual key reference frame data.
// It exposes the points to the referenceKeyframe data so the Frame
// can use it in the gui,
class CBreakFrameData
{
public:
   void ReadReferenceFrames(const string &referenceFrameFileName);

   void SetFileName(const string &referenceFrameFileName);
   void WriteReferenceFrames();
   void Add(const ReferenceKeyframeData &referenceKeyframeData);
   void Delete(const ReferenceKeyframeData &referenceKeyframeData);
   void Delete(int frameNumber);
   vector<ReferenceKeyframeData *>GetReferenceKeyFrames(int frame);
   vector<ReferenceKeyframeData *>GetAllReferenceKeyFrames();

   std::pair<int, int>GetBreakPair(int frame);

   void ClearAll();
   void ClearReferenceFrames();
   void ClearReferenceFrames(int frameIn, int frameOut);
   void SetBreaks(const vector<int> &breaks);
   vector < std::pair < int, int >> GetAllBreakPairs(int startFrame, int endFrame);
   vector < std::pair < int, int >> GetAllBreakPairs();

   CBHook DataHasChanged;

   ReferenceKeyframeData *FindReferenceframeDataAtFrameNumber(int frameNumber);

   bool AreThereReferenceFramesHere(int frameIn, int frameOut);

   bool IsDirty() const ;
   void SetDirty(const bool dirty);

   void AddToPDLEntry(CPDLElement &parent);
   int ReadBreaksFromPDLEntry(CPDLElement *pdlAttribute);
   int ReadReferenceKeyframeFromPDLEntry(CPDLElement *pdlAttribute);

private:
   map<int, ReferenceKeyframeData *>_referenceKeyframes;
   vector<int>_breaks;

   bool _isDirty = true;
   string _referenceFrameFileName = "";
};

// ---------------------------------------------------------------------------
#endif
