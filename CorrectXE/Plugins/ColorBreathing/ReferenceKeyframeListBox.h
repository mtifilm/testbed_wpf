//---------------------------------------------------------------------------

#ifndef ReferenceKeyframeListBoxH
#define ReferenceKeyframeListBoxH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "ReferenceKeyframeFrame.h"
#include "RGBFrameData.h"

#include "IniFile.h"
#include "timecode.h"
#include <Vcl.ComCtrls.hpp>

// The idea is to have a display frame that is a panel with nothing but
// a list of ReferenceKeyframeFrames.
// The list of ReferenceKeyframeData is kept in this list, and functions
// are used to extract this data from the list.

//---------------------------------------------------------------------------

typedef void (__closure *TDeleteReferenceKeyframeEvent)(const ReferenceKeyframeData &referenceKeyframeData);
typedef void (__closure *TReferenceKeyframeEvent)(ReferenceKeyframeData *referenceKeyframeData);

class TReferenceKeyframeListBoxFrame : public TFrame
{
__published:	// IDE-managed Components
	TPanel *BasePanel;
	TScrollBox *DisplayScrollBox;
	TFlowPanel *DisplayFlowPanel;
	TReferenceKeyframeFrame *ReferenceKeyframeFrame3;
	TReferenceKeyframeFrame *ReferenceKeyframeFrame4;
	TReferenceKeyframeFrame *ReferenceKeyframeFrame5;
	TReferenceKeyframeFrame *ReferenceKeyframeFrame6;
	TTimer *OneShotDeleteTimer;
   void __fastcall DeleteSelf(TObject *Sender);
	void __fastcall OneShotDeleteTimerTimer(TObject *Sender);

private:	// User declarations
	DEFINE_CBHOOK(ReferenceDataHasChanged, TReferenceKeyframeListBoxFrame);
 	DEFINE_CBHOOK(FrameClick, TReferenceKeyframeListBoxFrame);
   TNotifyEvent _OnChange;
   TReferenceKeyframeEvent _OnFrameClick;
   CTimecode _inTimecode;

public:		// User declarations
	__fastcall TReferenceKeyframeListBoxFrame(TComponent* Owner);
	__property TNotifyEvent OnChange = {read = _OnChange, write=_OnChange};
 	__property TReferenceKeyframeEvent OnFrameClick = {read = _OnFrameClick, write=_OnFrameClick};

	void Clear();
	const ReferenceKeyframeData GetKeyframeData(int index) const;
  	ReferenceKeyframeData *GetKeyframePointer(int index) const;
	void DeleteKeyframeData(int index);
	void Add(ReferenceKeyframeData *referenceKeyframeData);
	void Insert(int index, ReferenceKeyframeData *referenceKeyframeData);
	const vector<ReferenceKeyframeData *> GetKeyframeDataList();
   void SetKeyframeDataList(vector<ReferenceKeyframeData *>);
   TDeleteReferenceKeyframeEvent OnDeleteReferenceKeyframe;
   void SetInTimecode(const CTimecode &inTimecode);
   void NoChangeAllowed(RGBFrameData &rgbFrameData);
   void SelectReferenceFrame(int frame);
};
//---------------------------------------------------------------------------
extern PACKAGE TReferenceKeyframeListBoxFrame *ReferenceKeyframeListBoxFrame;
//---------------------------------------------------------------------------
#endif
