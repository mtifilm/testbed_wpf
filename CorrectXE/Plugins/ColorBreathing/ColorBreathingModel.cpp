// ---------------------------------------------------------------------------

#pragma hdrstop

#include "ColorBreathingModel.h"
#include "FlickerShot.h"
#include "MathFunctions.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "SysUtils.hpp"

static CSpinLock recomputeLock;

#define RECOMPUTE_LOCK 	CAutoSpinLocker dataChangeLocker(recomputeLock);
////#define RECOMPUTE_LOCK

// ---------------------------------------------------------------------------
#pragma package(smart_init)

ColorBreathingModel::ColorBreathingModel()
{
	_userPresets = new PresetParameterModel<ReferenceKeyframeData>("ReferenceKeyFrameData", 4);
	_timeline = new CGraphTimeline();
	_flickerProcess = new CFlickerProcess();
}

ColorBreathingModel::~ColorBreathingModel()
{
	delete _userPresets;
	delete _timeline;
	delete _flickerProcess;
}

void ColorBreathingModel::LoadAllSettingsFromIni(const string &iniFileName)
{
	RECOMPUTE_LOCK
	_userPresets->ReadPresetsFromIni(iniFileName);
}

PresetParameterModel<ReferenceKeyframeData> * ColorBreathingModel::GetPresets()
{
	return _userPresets;
}

void ColorBreathingModel::SetGraphDirty()
{
	RECOMPUTE_LOCK

	if (_timeline != nullptr)
	{
		_timeline->SetGraphDirty();
		RGBFrameData.SetDataDirty(true);
	}
}

void ColorBreathingModel::SetGraphRenderDirty()
{
	RECOMPUTE_LOCK

	if (_timeline != nullptr)
	{
		_timeline->SetGraphDirty();
		RGBFrameData.SetDataDirty(true);
	}
}

CFlickerSetting ConvertToFlickerSetting(ReferenceKeyframeData *referenceKeyframeData)
{
	// Union just makes it simpler
	union
	{
		int Value = 0;
		struct
		{
			bool ColorBackwards : 1;
			bool ColorForwards : 1;
			bool RedEnabled : 1;
			bool GreenEnabled : 1;
			bool BlueEnabled : 1;
			bool FlickerBackwards : 1;
			bool FlickerForwards : 1;
                        bool Invisible : 1;
		};
	} localReferenceType;

	// Do conversions
	localReferenceType.ColorBackwards = referenceKeyframeData->ColorBackwards;
	localReferenceType.ColorForwards = referenceKeyframeData->ColorForwards;
	localReferenceType.RedEnabled = referenceKeyframeData->RedEnabled;
	localReferenceType.GreenEnabled = referenceKeyframeData->GreenEnabled;
	localReferenceType.BlueEnabled = referenceKeyframeData->BlueEnabled;
	localReferenceType.FlickerBackwards = referenceKeyframeData->FlickerBackwards;
	localReferenceType.FlickerForwards = referenceKeyframeData->FlickerForwards;
        localReferenceType.Invisible = false;

	// Set values
	CFlickerSetting result;
	result.iReferenceFrame = referenceKeyframeData->FrameIndex;
	result.iReferenceType = localReferenceType.Value;
	result.iaMovingAve[0] = referenceKeyframeData->RedSmoothing;
	result.iaMovingAve[1] = referenceKeyframeData->GreenSmoothing;
	result.iaMovingAve[2] = referenceKeyframeData->BlueSmoothing;

	return result;
}

void ColorBreathingModel::SetRGBDatum(int frameIndex, const RGBFrameDatum &rgbFrameDatum)
{
	RECOMPUTE_LOCK

	// This will set the data dirty if necessary
	RGBFrameData.SetRgbFrameDatum(frameIndex, rgbFrameDatum);
}

void ColorBreathingModel::LoadRGBDataInToTimeline()
{
	RECOMPUTE_LOCK

	if (_timeline->setFrames(RGBFrameData.GetFrames()) != 0)
	{
		CAutoErrorReporter autoErr("ColorBreathingTool::LoadRGBDataInToTimeline", AUTO_ERROR_NOTIFY_WAIT,
		   AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "Could not alloc memory for graph, we are hosed.";
	}

	// This corrects the mean data
	_timeline->SetAllRgbData(RGBFrameData.GetRGBFrameData());

	// We have loaded it so it is not dirty
	RGBFrameData.SetDataDirty(false);
}

void ColorBreathingModel::loadReferenceFramesInToTimeline(CGraphTimeline *timeline)
{
	RECOMPUTE_LOCK

	vector<CFlickerShot>shots;
	auto frameOut = RGBFrameData.GetFrames();
	auto breaks = BreakFrameData.GetAllBreakPairs();
        int zero[] = {0, 0, 0};

	for (auto b : breaks)
	{
		CFlickerShot shot;
		shot.setRange(b.first, b.second);

		// Now add all the reference frames
		auto referenceFrames = BreakFrameData.GetReferenceKeyFrames(b.first);

		for (auto r : referenceFrames)
		{
			// Programming like Kurt!!!!
			// The getReference key frames should actually have a break vector
			// as an arguement
			if (r->FrameIndex <= frameOut)
			{
				auto f = ConvertToFlickerSetting(r);
				shot.addSetting(f.iReferenceFrame, f.iReferenceType, f.iaMovingAve);
			}
		}

                // Kludge, add invisible reference if empty
                if (shot.getSettingCount() == 0)
                {
                     // Invisible, no forward, backward
                     shot.addSetting(b.first, 0x9C, zero);
                }

		shots.push_back(shot);
	}

	// We could directly push_back each shot
	timeline->setShotList(shots);
}

void ColorBreathingModel::SetGraphDimensions(int width, int height)
{
	RECOMPUTE_LOCK;

	_graphWidth = width;
	_graphHeight = height;

	// Set all the parameters for the timeline
	// This is cheap
	_timeline->setGraphDimension(_graphWidth, _graphHeight);
}

ColorBreathingData &ColorBreathingModel::GetGraphInfo()
{
	return _graphInfo;
}

void ColorBreathingModel::SetCenterState(bool enabled)
{
	RECOMPUTE_LOCK

	if (_centerState != enabled)
	{
		_centerState = enabled;
		SetGraphDirty();
	}
}

bool ColorBreathingModel::GetCenterState()
{
	return _centerState;
}

void ColorBreathingModel::SetDisplayIn(int displayIn)
{
	RECOMPUTE_LOCK

	if (_displayIn != displayIn)
	{
		_displayIn = displayIn;
		SetGraphDirty();
	}
}

void ColorBreathingModel::SetDisplayOut(int displayOut)
{
	RECOMPUTE_LOCK;

	if (_displayOut != displayOut)
	{
		_displayOut = displayOut;
		SetGraphDirty();
	}
}

int ColorBreathingModel::GetDisplayIn()
{
	RECOMPUTE_LOCK

	return _displayIn;
}

int ColorBreathingModel::GetDisplayOut()
{
	return _displayOut;
}

int ColorBreathingModel::ComputeBitmap(int width, int height, int position, unsigned char *bitmapData)
{
	RECOMPUTE_LOCK

	GetLastGraphBitmap(width, height, position, bitmapData);

	return 0;
}

void ColorBreathingModel::CorrectMeanDataForRender()
{
	RECOMPUTE_LOCK

	_timeline->CorrectMeanData();
}

void ColorBreathingModel::PreviewColorBreathingFrame16Bit(int frameIndex, int width, int height, int maxPixel,
    unsigned short *frameBits)
{
    RECOMPUTE_LOCK

    if (BreakFrameData.IsDirty())
    {
        CorrectMeanDataForRender();
        BreakFrameData.SetDirty(false);
    }

    float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
    float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
    int iaCount[TL_CHAN_COUNT];

    auto keyframes = GetAllReferenceFrameIndexes();
    GetQuantileCorrection(frameIndex, faX, faY, iaCount);

//    if (_dumpDebugInfo)
//    {
//        TRACE_0(errout << "TTT index " << frameIndex << ", total keyframes " << keyframes.size());
//
//        float fpDeltaR, fpDeltaG, fpDeltaB;
//        _timeline->getRGB(frameIndex, &fpDeltaR, &fpDeltaG, &fpDeltaB);
//        TRACE_0(errout << "Keyframe (RGB) " << fpDeltaR << ", " << fpDeltaG << ", " << fpDeltaB);
//
//        _timeline->getDelta(frameIndex, &fpDeltaR, &fpDeltaG, &fpDeltaB);
//        TRACE_0(errout << "Keyframe delta (RGB) " << fpDeltaR << ", " << fpDeltaG << ", " << fpDeltaB);
//
//        for (auto i = 0; i < iaCount[0]; i++)
//        {
//            TRACE_0(errout << i << " 0) X " << faX[0][i] << ", Y " << faY[0][i] << ", d " << faX[0][i] - faY[0][i]);
//        }
//
//        for (auto i = 0; i < iaCount[1]; i++)
//        {
//            TRACE_0(errout << i << " 1) X " << faX[1][i] << ", Y " << faY[1][i] << ", d " << faX[1][i] - faY[1][i]);
//        }
//    }

    _flickerProcess->setImageSize(height, width, maxPixel);
    auto roi = getProcessingRoi(frameIndex);

    _flickerProcess->RenderV4(frameBits, faX, faY, iaCount, roi);
}

RECT ColorBreathingModel::getProcessingRoi(int frameIndex) const
{
   if (getUseProcessingRegion() == false)
   {
      auto width = _flickerProcess->getWidth();
      auto height = _flickerProcess->getHeight();

      return { .left = 0, .right = width, .top = 0, .bottom = height};
   }

   return RGBFrameData.GetROI(frameIndex);
}

void ColorBreathingModel::PreviewColorBreathingFrame(int frameIndex, int width, int height, unsigned char *frameBits)
{
	RECOMPUTE_LOCK

	if (BreakFrameData.IsDirty())
	{
		_timeline->CorrectMeanData();
		BreakFrameData.SetDirty(false);
	}

	float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
	float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2];
	int iaCount[TL_CHAN_COUNT];

	auto keyframes = GetAllReferenceFrameIndexes();
	GetQuantileCorrection(frameIndex, faX, faY, iaCount);

//	if (_dumpDebugInfo)
//	{
//		TRACE_0(errout << "TTT index " << frameIndex << ", total keyframes " << keyframes.size());
//
//		float fpDeltaR, fpDeltaG, fpDeltaB;
//		_timeline->getRGB(frameIndex, &fpDeltaR, &fpDeltaG, &fpDeltaB);
//		TRACE_0(errout << "Keyframe (RGB) " << fpDeltaR << ", " << fpDeltaG << ", " << fpDeltaB);
//
//		_timeline->getDelta(frameIndex, &fpDeltaR, &fpDeltaG, &fpDeltaB);
//		TRACE_0(errout << "Keyframe delta (RGB) " << fpDeltaR << ", " << fpDeltaG << ", " << fpDeltaB);
//
//		for (auto i = 0; i < iaCount[0]; i++)
//		{
//			TRACE_0(errout << i << " 0) X " << faX[0][i] << ", Y " << faY[0][i] << ", d " << faX[0][i] - faY[0][i]);
//		}
//
//		for (auto i = 0; i < iaCount[1]; i++)
//		{
//			TRACE_0(errout << i << " 1) X " << faX[1][i] << ", Y " << faY[1][i] << ", d " << faX[1][i] - faY[1][i]);
//		}
//	}

	_flickerProcess->RenderDisplayV4(frameBits, width, height, faX, faY, iaCount);
}

void ColorBreathingModel::SetImageSize(int pixelsRow, int pixelsCol, int dataMax)
{
	_pixelsRow = pixelsRow;
	_pixelsCol = pixelsCol;
	_dataMax = dataMax;
}

int ColorBreathingModel::GetGraphDisplayIn() const
{
	return _timeline->GetDisplayIn();
}

int ColorBreathingModel::GetGraphDisplayOut() const
{
	return _timeline->GetDisplayOut();
}

void ColorBreathingModel::MarkRendered(int frameIndex)
{
	RECOMPUTE_LOCK

	RGBFrameData.MarkRendered(frameIndex);
	_timeline->MarkRendered(frameIndex);
}

void ColorBreathingModel::SetTotalFrames(int frames)
{
    RECOMPUTE_LOCK

	RGBFrameData.SetTotalFrames(frames);
	LoadRGBDataInToTimeline();
}

void ColorBreathingModel::SetPreviewMode(bool previewMode)
{
    RECOMPUTE_LOCK
	_previewMode = previewMode;
	_timeline->ShowCursorActive(_previewMode);
}

bool ColorBreathingModel::IsPreviewMode() const
{
	return _previewMode;
}

static void StaticRecomputeGraphThreadStart(void *vpAppData, void *vpReserved)
{
	if (BThreadBegin(vpReserved))
	{
		TRACE_0(errout << "StaticRecomputeGraphThreadStart: BThreadBegin failed");
		return;
	}

   try
   {
	   auto model = reinterpret_cast<ColorBreathingModel*>(vpAppData);
      model->RecomputeGraph();
   }
   catch (std::exception &exception)
   {
      if (exception.what() == nullptr)
      {
         TRACE_0(errout << "Impossible error in Recompute Graph");
      }
      else
      {
         TRACE_0(errout << "Error in Recompute Graph: "  << exception.what());
      }
   }
   catch (...)
   {
      TRACE_0(errout << "Unknown error in Recompute Graph");
   }
}

bool ColorBreathingModel::GraphIsOutOfSync()
{
	return _newGraphAvailable;
}

void ColorBreathingModel::RecomputeGraph()
{
	RECOMPUTE_LOCK

   try
	{
		// Set all the parameters for the timeline, these are no opts

		SetGraphDimensions(_graphWidth, _graphHeight);

		// Figure out the display range, in center mode it is the current break,
		if (_centerState)
		{
			auto breakPair = BreakFrameData.GetBreakPair(_position);
			_timeline->setDisplayRange(breakPair.first, breakPair.second);
		}
		else
		{
			auto displayIn = GetDisplayIn();
			auto displayOut = GetDisplayOut();
			if ((displayIn < 0) || (displayOut < 0))
			{
				_graphFinished = true;
				return;
			}

			_timeline->setDisplayRange(displayIn, displayOut);
		}

		_timeline->SetGraphCentered(_centerState);
		_timeline->setGraphType(_graphInfo.GetGraphType());
		auto in = RGBFrameData.GetMarkIn();
		auto out = RGBFrameData.GetMarkOut();
		_timeline->setMarkRange(RGBFrameData.GetMarkIn(), RGBFrameData.GetMarkOut());

		// TRACE_0(errout << "TTT after " << _timeline->IsGraphDataDirty()
		// << ", cursor " << _timeline->IsCursorPixelPositionDirty());
		CHRTimer hrt;
		hrt.Start();
		loadReferenceFramesInToTimeline(_timeline);
	}
	catch (...)
	{
      DBCOMMENT("Error before here");
	}

   try
   {
	if (RGBFrameData.IsDataDirty())
	{
		LoadRGBDataInToTimeline();
	}

	_timeline->setCursorPosition(_position);

	// We are done setting values, lets see if we have a change
	// TRACE_0(errout << "TTT GraphData dirty " << _timeline->IsGraphDataDirty()
	// << ", cursor " << _timeline->IsCursorPixelPositionDirty());
	if (_timeline->IsGraphDataDirty())
	{
		// TTT I don't think this is necessary
		//// _timeline->CorrectMeanData();
	}

	if (_timeline->IsGraphDirty())
	{
		_timeline->updateGraph(_currentGraph);
	}

   }
   catch (...)
   {
      DBCOMMENT("Error Here");
   }
	// This is thread safe because _graphFinished is only set here
	// and read in other thread. This thread is never running when _graphfinished
	// is true.  Thus never set in other thread
	_newGraphAvailable = true;
	_graphFinished = true;
}


void ColorBreathingModel::GetLastGraphBitmap(int width, int height, int position, unsigned char *bitmapData)
{
	RECOMPUTE_LOCK

	// Idea here is simple. We see if a bitmap has been created, if so it is copied
	// to the data.  If the graph is dirty, a new thread is fired up to compute it.
	// If another request comes in before completion, it is ignored,
	// assuming the incoming bitmap is in stale state.
	if (!_graphFinished)
	{
		return;
	}

	//// TRACE_0(errout << "TTT finished, dirty graph " << _timeline->IsGraphDirty() << ", dirty data " << RGBFrameData.IsDataDirty() );
	// If size has changed, don't copy
	if ((width != _graphWidth) || (height != _graphHeight))
	{
		_graphWidth = width;
		_graphHeight = height;
		MTIfree(_currentGraph);
		_currentGraph = (unsigned char *)MTImalloc(4 * _graphWidth * (_graphHeight + 40));
		_newGraphAvailable = false;
		_timeline->SetGraphDirty();
	}

	// Copy if necessary
	if (_newGraphAvailable == true)
	{
		MTImemcpy(bitmapData, _currentGraph, 4 * _graphWidth * (_graphHeight + 4));
		_newGraphAvailable = false;
	}

	// Recompute if necessary
	_position = position;
	_timeline->setCursorPosition(_position);

    auto gd = _timeline->IsGraphDirty();
    auto dd = RGBFrameData.IsDataDirty();
	if ((_timeline->IsGraphDirty() == false) && (RGBFrameData.IsDataDirty() == false))
	{
		return;
	}

	_graphFinished = false;
	_graphDisplayed = false;
	BThreadSpawn(StaticRecomputeGraphThreadStart, this);
}

void ColorBreathingModel::WritePDLEntry(CPDLElement &parent)
{
	// Make tool type entry
	auto pdlToolType = parent.MakeNewChild("ToolType");
	pdlToolType->SetAttribString("ToolSubType", "Global");

	// Write Colorbreathing Parameters to a PDL Entry's Tool Attributes
	auto pdlParameters = parent.MakeNewChild("Parameters");
	pdlParameters->SetAttribBool("UseProcessingRegion", getUseProcessingRegion());

	BreakFrameData.AddToPDLEntry(parent);
	RGBFrameData.AddToPDLEntry(parent);
   }

int ColorBreathingModel::ReadPDLEntry(CPDLEntry &pdlEntry)
{
	CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
	CPDLElement *toolAttribRoot = pdlEntryTool->parameters;
	auto retVal = 0;

	for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
	{
		CPDLElement *subElement = toolAttribRoot->GetChildElement(i);
		string elementName = subElement->GetElementName();

		if (elementName == "Breaks")
		{
			retVal = BreakFrameData.ReadBreaksFromPDLEntry(subElement);
			if (retVal != 0)
			{
				return retVal;
			}
		}
		else if (elementName == "ReferenceKeyframe")
		{
			retVal = BreakFrameData.ReadReferenceKeyframeFromPDLEntry(subElement);
			if (retVal != 0)
			{
				return retVal;
			}
		}
		else if (elementName == "RGBData")
		{
			retVal = RGBFrameData.ReadRGBFrameDataFromPDLEntry(subElement);
			if (retVal != 0)
			{
				return retVal;
			}
		}
		else if (elementName == "Parameters")
        {
            auto b = subElement->GetAttribBool("UseProcessingRegion", getUseProcessingRegion());
            setUseProcessingRegion(b);
        }
	}

	// Nothing like overkill
	LoadRGBDataInToTimeline();
	_timeline->CorrectMeanData();
	SetGraphDirty();
	RecomputeGraph();

	return 0;
}

void ColorBreathingModel::SetMinScale(double minScale)
{
	_timeline->SetMinScale(minScale);

	if (_timeline->IsGraphDirty())
	{
		RecomputeGraph();
	}
}

void ColorBreathingModel::DeleteReferenceFramesWhereThereIsNoGraph()
{
	for (auto keyFrame : BreakFrameData.GetAllReferenceKeyFrames())
	{
		if (!RGBFrameData[keyFrame->FrameIndex].Valid)
		{
			BreakFrameData.Delete(keyFrame->FrameIndex);
		}
	}
}

bool ColorBreathingModel::AddEndKeyframeToUnrenderedBreakArea(int frameIndex)
{
        return false;

	// Find the unrendred range
	auto unrenderedRange = FindUnrenderedRange(frameIndex);
	auto in = unrenderedRange.first;
	auto out = unrenderedRange.second;

	if (in == out)
	{
		return false;
	}

	// Now add reference frame if necessary
	// We don't need to add a reference frame if a break exists
	auto breakPair = BreakFrameData.GetBreakPair(in);
	if (breakPair.first != in)
	{
		// See if a keyframe exists at the in-1 point
		// All renders must be valid from break to in
		ReferenceKeyframeData bl;

		auto bf = BreakFrameData.FindReferenceframeDataAtFrameNumber(in - 1);
		if (bf != nullptr)
		{
			bl = *bf;
			bl.ColorForwards = false;
			bl.FlickerForwards = false;
		}
		else
		{
			bl.ColorForwards = false;
			bl.FlickerForwards = false;
			bl.ColorBackwards = false;
			bl.FlickerBackwards = false;
		}

		// Note: in cannot be 0 because there is aways a 'break' at 0
		bl.FrameIndex = in - 1;
		BreakFrameData.Add(bl);
	}

	// We don't need to add a reference frame if break on end
	// out is the first RENDERED frame, so we have to back up one
	breakPair = BreakFrameData.GetBreakPair(out - 1);
	if (breakPair.second != (out - 1))
	{
		// Add reference frame
		ReferenceKeyframeData br;
		auto bf = BreakFrameData.FindReferenceframeDataAtFrameNumber(out);
		if (bf != nullptr)
		{
			br = *bf;
			br.ColorBackwards = false;
			br.FlickerBackwards = false;
		}
		else
		{
			br.ColorForwards = false;
			br.FlickerForwards = false;
			br.ColorBackwards = false;
			br.FlickerBackwards = false;
			br.FrameIndex = out;
		}

		BreakFrameData.Add(br);
	}

	return true;
}

bool ColorBreathingModel::AddKeyframe(const ReferenceKeyframeData &r)
{
	// Find the unrendred range
	auto unrenderedRange = FindUnrenderedRange(r.FrameIndex);
	auto in = unrenderedRange.first;
	auto out = unrenderedRange.second;

	if (in == out)
	{
		return false;
	}

	BreakFrameData.Add(r);
	return true;
}

std::pair<int, int>ColorBreathingModel::FindUnrenderedRange(int frame)
{
	// This finds the range around the frame index frame that is terminated
	// by rendered frame. The result [in,out) frames are unrendered
	auto in = frame;
	while (in > 0)
	{
		if (RGBFrameData[in - 1].Rendered)
		{
			break;
		}

		in--;
	}

	auto out = frame;
	while (out < RGBFrameData.size())
	{
		if (RGBFrameData[out].Rendered)
		{
			break;
		}

		out++;
	}

//	std::pair<int, int>result(in, out);
	return {in, out};
}

void ColorBreathingModel::SetQuantileMaximum(unsigned int dataMaximum)
{
    _timeline->SetQuantileMaximum(dataMaximum);
}

void ColorBreathingModel::setUseProcessingRegion(const bool value)
{
    _useProcessingRegion = value;
}

bool ColorBreathingModel::getUseProcessingRegion() const
{
    return _useProcessingRegion;
}

void ColorBreathingModel::setUseAnalysisRegion(const bool value)
{
    _useAnalysisRegion = value;
}

bool ColorBreathingModel::getUseAnalysisRegion() const
{
    return _useAnalysisRegion;
}

int ColorBreathingModel::MakeQuantiles(const Ipp16u* source, int stepInBytes, IppiSize roiSize,
   RGBFrameDatum &rgbFrameDatum)
{
	return _timeline->MakeQuantiles(source, stepInBytes, roiSize, rgbFrameDatum);
}

void ColorBreathingModel::GetQuantileCorrection(int iFrame, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT2],
   float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT2], int iaCount[TL_CHAN_COUNT])
{
	auto keyframes = GetAllReferenceFrameIndexes();

   // This is an error
   if (keyframes.size() == 0)
   {
      DBCOMMENT("No keyframe, an error");
   }

	// See if we are on a keyframe
	if (find(keyframes.begin(), keyframes.end(), iFrame) != keyframes.end())
	{

		float fpDeltaR, fpDeltaG, fpDeltaB;
		_timeline->getDelta(iFrame, &fpDeltaR, &fpDeltaG, &fpDeltaB);

		if ((fpDeltaR != 0) || (fpDeltaG != 0) || (fpDeltaB != 0))
		{
			TRACE_0(errout << "Keyframe data error, frame " << iFrame);
			TRACE_0(errout << "Keyframe delta (RGB) " << fpDeltaR << ", " << fpDeltaG << ", " << fpDeltaB);
		}

		for (int iChan = 0; iChan < TL_CHAN_COUNT; iChan++)
		{
			iaCount[iChan] = 2;
			faX[iChan][0] = 0.f;
			faY[iChan][0] = 0.f;
			faX[iChan][1] = 1.f;
			faY[iChan][1] = 1.f;
		}

		return;
	}

	_timeline->getQuantileCorrection(iFrame, faX, faY, iaCount);
}

vector<int>ColorBreathingModel::GetAllReferenceFrameIndexes()
{
	vector<int>indexes;
	auto frameOut = RGBFrameData.GetFrames();
	auto breaks = BreakFrameData.GetAllBreakPairs();

	for (auto b : breaks)
	{
		CFlickerShot shot;
		shot.setRange(b.first, b.second);

		// Now add all the reference frames
		auto referenceFrames = BreakFrameData.GetReferenceKeyFrames(b.first);

		for (auto r : referenceFrames)
		{
			indexes.push_back(r->FrameIndex);
		}
	}

	// We could directly push_back each shot
	return indexes;
}

// DEBUGGING CODE for data from matlab
   void ColorBreathingModel::LoadRGBData()
{
   DBCOMMENT("Removed routine called");
//    ifstream file ("c:\\temp\\RGBData.txt");
//    auto totalFrames = RGBData.GetFrames();
//    while(!file.eof())
//    {
//    	RGBFrameDatum rgb;
//   	string str;
//   	std::getline(file, str);
//        if (str.size() < 2)
//        {
//            continue;
//        }
//
//        std::stringstream ss(str);
//
//        int iFrame;
//        float fVal;
//        char p;
//        ss >> iFrame;
//
//        if (iFrame >= totalFrames)
//        {
//            break;
//        }
//
//        ss >> p;
//        ss >> rgb.Red;
//        ss >> rgb.Green;
//        ss >> rgb.Blue;
//
//        std::getline(file, str);
//        ss >> iFrame;
//        ss >> p;
//        ss >> mdProcess.fpValProc[0][iFrame];
//        ss >> mdProcess.fpValProc[1][iFrame];
//        ss >> mdProcess.fpValProc[2][iFrame];
//    }
//
//    	SetGraphDirty();
}

