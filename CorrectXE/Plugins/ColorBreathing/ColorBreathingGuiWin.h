//---------------------------------------------------------------------------

#ifndef ColorBreathingGuiWinH
#define ColorBreathingGuiWinH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "ColorPanel.h"
#include "ExecButtonsFrameUnit.h"
#include "ToolSystemInterface.h"
#include "ExecStatusBarUnit.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include "MTIUNIT.h"
#include "cspin.h"
#include "ColorBreathingTool.h"
#include "ReferenceKeyframeFrame.h"
#include "ReferenceKeyframeListBox.h"
#include "ColorBreathingData.h"
#include <Vcl.ImgList.hpp>
#include <Vcl.Graphics.hpp>
#include "TrackEditFrameUnit.h"
#include <System.ImageList.hpp>
#include "ColorLabel.h"
#include "PresetsUnit.h"
#include <Vcl.Samples.Spin.hpp>
#include "SpinEditFrameUnit.h"
#include "VTimeCodeEdit.h"

//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateColorBreathingToolGUI(void);
extern void DestroyColorBreathingToolGUI(void);
extern bool ShowColorBreathingToolGUI(void);
extern bool HideColorBreathingToolGUI(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
								   bool processingRenderFlag);
bool RunExecButtonsCommand(EExecButtonId command);
extern bool IsToolVisible(void);
extern void ExternalGuiRefresh(int line);
extern void ExternalGuiReferenceFrameRefresh(int line);

extern void ToggleColorPicker(void);
extern void ToggleHideAllOverlays();
extern bool AreAllOverlaysHidden();

extern void NotifyMaskChanged(const string &newMaskAsString);
extern void NotifyMaskVisibilityChanged();

extern void SelectZonalPreset(int n);

class TColorBreathingForm : public TMTIForm
{
__published:	// IDE-managed Components
    TColorPanel *ColorBreathingControlPanel0;
	TPanel *Panel1;
	TLabel *FPSLabel;
	TLabel *MSLabel;
	TPanel *ToolbarContainerPanel;
	TToolBar *TBToolbar1;
	TToolButton *PreviewFrameButton;
	TToolButton *PreviewAllButton;
	TToolButton *RenderButton;
	TToolButton *StopButton;
	TToolButton *HighLightButton;
	TToolButton *PDLCaptureTBItem;
	TPanel *StatusBar1;
	TLabel *SBRightLabel;
	TLabel *SBLeftLabel;
	TExecButtonsFrame *ExecButtonsToolbar;
	TExecStatusBarFrame *ExecStatusBar;
	TGroupBox *GraphGroupBox;
	TReferenceKeyframeListBoxFrame *ReferenceKeyframeListBox;
	TPanel *SetupPanel;
	TReferenceKeyframeFrame *SetupReferenceKeyframeFrame;
	TLabel *ObscureColorLabel;
	TLabel *ObscureFlickerLabel;
	TImageList *ColorImageList;
	TButton *AllChannelsButton;
	TSpeedButton *RedGraphSpeedButton;
	TSpeedButton *GreenGraphSpeedButton;
	TSpeedButton *BlueGraphSpeedButton;
	TPanel *ObscureLabelPanel;
   TPanel *GLOBAL_PresetPanel;
   TLabel *GLOBAL_PresetLabel;
   TSpeedButton *GLOBAL_PresetButton1;
   TSpeedButton *GLOBAL_PresetButton2;
   TSpeedButton *GLOBAL_PresetButton3;
   TSpeedButton *GLOBAL_PresetButton4;
   TBitBtn *GLOBAL_SavePresetButton;
	TBitBtn *AddRefKeyFrameButton;
   TSpeedButton *CenterButton;
	TPanel *SegmentPanel;
	TLabel *Label3;
   TLabel *InLabel;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
   TLabel *DurationLabel;
   TLabel *OutLabel;
	TLabel *Label10;
	TPanel *ArrowPanel;
	TImage *Image1;
	TImage *Image2;
	TRadioGroup *DisplayRadioGroup;
   TBitBtn *GLOBAL_DisabledSaveButton;
   TBitBtn *GenerateGraphButton;
   TTimer *ColorPickerStateTimer;
   TGroupBox *GroupBox1;
   TCheckBox *ProcessingCBox;
	TButton *Fix1Button;
    TColorPanel *ColorBreathingControlPanel;
    TPageControl *ColorBreathingPageControl;
    TTabSheet *ColorBreathingTabSheet;
    TTabSheet *FlickerTabSheet;
    TBitBtn *ZonalAnalyzeButton;
    TGroupBox *AnalysisGroupBox;
    TTrackEditFrame *BoxesTrackEdit;
    TTrackEditFrame *SmoothingTrackEditFrame;
    TRadioGroup *ZonalDisplayRadioGroup;
    TGroupBox *ProcessingRegionGroupBox;
    TSpeedButton *TopUpButton;
    TSpeedButton *TopDownButton;
    TSpeedButton *BottomUpButton;
    TSpeedButton *BottomDownButton;
    TSpeedButton *LeftLeftButton;
    TSpeedButton *LeftRightButton;
    TSpeedButton *RightLeftButton;
    TSpeedButton *RightRightButton;
    TSpeedButton *ColorPickButton;
    TBitBtn *AllMatButton;
    TCheckBox *ShowMattingBoxCBox;
    TCheckBox *UseMattingCBox;
    TTimer *DownTimer;
    TBitBtn *ZonalSmoothingButton;
    TBitBtn *ReadAnalysisButton;
   TGroupBox *ZonalFlickerRoiGroupBox;
    TCheckBox *ZonalFlickerRoiProcessingCheckBox;
    TSpeedButton *ZonalFlickerIntensityTypeButton;
    TSpeedButton *ZonalFlickerColorTypeButton;
    TColorLabel *RoiWarningColorLabel;
    TCheckBox *GlobalFlickerRoiAnalysisCheckBox;
    TButton *DebugButton;
    TButton *ResetZonalRoiButton;
    TGroupBox *ZonalProcessChannelsGroupBox;
    TSpeedButton *RedZonalProcessingSpeedButton;
    TSpeedButton *GreenZonalProcessingSpeedButton;
    TSpeedButton *BlueZonalProcessingSpeedButton;
    TSpeedButton *AllZonalProcessSpeedButton;
    TCheckBox *fastCheckBox;
    TBitBtn *ZonalResetPreprocessingButton;
	TButton *RoiToMaskButton;
	TColorLabel *RoiOffWarningLabel;
	TColorLabel *Label1;
   TColorLabel *RoiLockedWarningLabel;
   TColorPanel *ZonalFlickerRoiLockHighlightPanel;
   TCheckBox *ZonalFlickerRoiLockCheckBox;
   TColorPanel *ZonalFlickerRoiShowHighlightPanel;
   TCheckBox *ZonalFlickerRoiShowCheckBox;
   TColorPanel *GlobalFlickerRoiShowHighlightPanel;
   TColorPanel *GlobalFlickerRoiLockHighlightPanel;
   TCheckBox *LockMattingCBox;
   TCheckBox *ShowMattingCBox;
   TPresetsFrame *ZonalPresetsFrame;
   TColorPanel *ZonalFlickerShowBoxesHighlightPanel;
   TCheckBox *ZonalFlickerShowBoxesCheckBox;
   TColorPanel *DisabledShowBoxesPanel;
   TCheckBox *DisabledShowBoxesCheckBox;
   TColorLabel *DisabledShowBoxesLabel;
   TColorPanel *HideAllOverlaysHighlightPanel;
   TCheckBox *HideAllOverlaysCheckBox;
   TColorPanel *DisabledShowRoiPanel;
   TColorLabel *DisabledShowRoiLabel;
   TCheckBox *DisabledShowRoiCheckBox;
   TPanel *ZonalFlickerSlidersPanel;
   TGroupBox *ZonalConstraintsGroupBox;
   TLabel *ZonalConstraintsAnchorsFirstFrameOffsetLabel;
   TSpeedButton *ZonalConstraintsAnchorsFirstFrameButton;
   TSpeedButton *ZonalConstraintsAnchorsLastFrameButton;
   TLabel *ZonalConstraintsAnchorsLastFrameOffsetLabel;
   TSpinEditFrame *ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit;
   TSpinEditFrame *ZonalConstraintsAnchorsLastFrameOffsetSpinEdit;
   TColorLabel *PleaseChooseBWorColorLabel;
   TRadioButton *ZonalConstraintsNoneRadioButton;
   TRadioButton *ZonalConstraintsAnchorsRadioButton;
   TRadioButton *ZonalConstraintsRefFrameRadioButton;
   TPanel *ZonalConstraintsAnchorsPanel;
   TPanel *ZonalConstraintsRefFramePanel;
   VTimeCodeEdit *ZonalConstraintsRefFrameTimecodeEdit;
   TSpeedButton *ZonalConstraintsSetRefFrameButton;
   TSpeedButton *ZonalConstraintsGoToRefframeButton;
	TPanel *InvalidIndexPanel;
	TColorLabel *ColorLabel1;
	TColorLabel *ColorLabel2;
	TComboBox *SmoothingTestComboBox;
	void __fastcall AllGraphColorsButtonClick(TObject *Sender);
	void __fastcall ReferenceFrameListBoxDrawItem(TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State);
	void __fastcall RedGraphButtonClick(TObject *Sender);
	void __fastcall GreenGraphButtonClick(TObject *Sender);
	void __fastcall BlueGraphButtonClick(TObject *Sender);
	void __fastcall GLOBAL_SavePresetButtonClick(TObject *Sender);
	void __fastcall GLOBAL_PresetButtonClick(TObject *Sender);
   void __fastcall AddRefKeyFrameButtonClick(TObject *Sender);
   void __fastcall GenerateGraphButtonClick(TObject *Sender);
   void __fastcall SetStartTCButtonClick(TObject *Sender);
   void __fastcall ProcessingRegionShowMattingCBoxClick(TObject *Sender);
   void __fastcall ProcessingRegionProcessingCBoxClick(TObject *Sender);
   void __fastcall CenterButtonClick(TObject *Sender);
   void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender);
   void __fastcall ProcessingRegionLockMattingCBoxClick(TObject *Sender);
   void __fastcall DisplayRadioGroupClick(TObject *Sender);
	void __fastcall DumpDebugInfoCheckBoxClick(TObject *Sender);
	void __fastcall GraphResetButtonClick(TObject *Sender);
	void __fastcall Fix1ButtonClick(TObject *Sender);
    void __fastcall ZonalAnalyzeButtonClick(TObject *Sender);
    void __fastcall ColorBreathingPageControlChange(TObject *Sender);
    void __fastcall ZonalFlickerSliderChanged(TObject *Sender);
    void __fastcall ZonalFlickerDisplayRadioGroupClick(TObject *Sender);
    void __fastcall TopDownButtonClick(TObject *Sender);
    void __fastcall RightLeftButtonClick(TObject *Sender);
    void __fastcall RightRightButtonClick(TObject *Sender);
    void __fastcall BottomDownButtonClick(TObject *Sender);
    void __fastcall BottomUpButtonClick(TObject *Sender);
    void __fastcall LeftRightButtonClick(TObject *Sender);
    void __fastcall LeftLeftButtonClick(TObject *Sender);
    void __fastcall TopUpButtonClick(TObject *Sender);
    void __fastcall UseMattingCBoxClick(TObject *Sender);
    void __fastcall ShowMattingBoxCBoxClick(TObject *Sender);
    void __fastcall SetMatButtonClick(TObject *Sender);
    void __fastcall AllMatButtonClick(TObject *Sender);
    void __fastcall ColorPickButtonClick(TObject *Sender);
    void __fastcall AutoMoveButtonMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall AutoMoveButtonMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall DownTimerTimer(TObject *Sender);
    void __fastcall ZonalFlickerShowBoxesCheckBoxClick(TObject *Sender);
    void __fastcall ColorPickerStateTimerTimer(TObject *Sender);
    void __fastcall ReadAnalysisButtonClick(TObject *Sender);
    void __fastcall ZonalFlickerColorTypeButtonClick(TObject *Sender);
    void __fastcall ZonalFlickerIntensityTypeButtonClick(TObject *Sender);
    void __fastcall ZonalFlickerRoiLockCheckBoxClick(TObject *Sender);
    void __fastcall ZonalFlickerRoiShowCheckBoxClick(TObject *Sender);
    void __fastcall ZonalFlickerRoiProcessingCheckBoxClick(TObject *Sender);
    void __fastcall ExecButtonsToolbarRenderAllButtonClick(TObject *Sender);
    void __fastcall ZonalSmoothingButtonClick(TObject *Sender);
    void __fastcall GlobalFlickerRoiAnalysisCheckBoxClick(TObject *Sender);
    void __fastcall DebugButtonClick(TObject *Sender);
    void __fastcall ResetZonalRoiButtonClick(TObject *Sender);
    void __fastcall AllZonalProcessSpeedButtonClick(TObject *Sender);
    void __fastcall ZonalAnalysisSpeedButtonClick(TObject *Sender);
    void __fastcall fastCheckBoxClick(TObject *Sender);
    void __fastcall ZonalResetPreprocessingButtonClick(TObject *Sender);
	void __fastcall RoiToMaskButtonClick(TObject *Sender);
   void __fastcall HideAllOverlaysCheckBoxClick(TObject *Sender);
   void __fastcall ZonalConstraintsGuiChanged(TObject *Sender);
   void __fastcall ExecButtonsToolbarCapturePDLButtonClick(TObject *Sender);
   void __fastcall ZonalConstraintsGoToRefframeButtonClick(TObject *Sender);
   void __fastcall ZonalConstraintsRefFrameTimeCodeEditEnterOrEscapeKey(TObject *Sender);
   void __fastcall ZonalConstraintsSetRefFrameButtonClick(TObject *Sender);
   void __fastcall ZonalConstraintsRefFrameTimecodeEditExit(TObject *Sender);
   void __fastcall ZonalConstraintsRadioButtonClick(TObject *Sender);
	void __fastcall SmoothingTestComboBoxChange(TObject *Sender);
   void __fastcall ZonalConstraintsNoneRadioButtonClick(TObject *Sender);



private:	// User declarations
   bool _TabStopDoNotDeletePanelVisible;
   TReferenceKeyframeFrame *TestFrame;
   void DeleteReferenceKeyframeEvent(const ReferenceKeyframeData &referenceKeyframeData);
   void EnablePreview1(bool On);
   void SetButtonCommand(TToolButton *button, EToolProcessingCommand newCmd);
   void __fastcall ReferenceDataChanged(TObject *Sender);
   void FrameLabelClicked(ReferenceKeyframeData *referenceFrameData);
   void setIdleCursor(void);

   TObject *GlobalSender = nullptr;

   // Exec buttons toolbar
   void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
   void ExecStop(void);
   void ExecCaptureToPDL(void);
   void ExecCaptureAllEventsToPDL(void);
   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToCurrent(void);
   void ExecSetResumeTimecodeToDefault(void);
   void ExecGoToResumeTimecode(void);
   void UpdateExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag);
   void UpdateColorBreathingExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag);
   void UpdateSpatialFlickerExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag);
   void ProcessPreviewOneFrameMode();

   TSpeedButton * _presetButtonArray[4];
   CTimecode _inTimecode;

   bool _updatingZonalFlickerGui = false;
   int _oldSmoothingType = 0;

   /////////////////////////////////////////////////////////////////////////////
   // CAREFUL: You must set the color of the highlight panels in the Designer //
   // to be the desired highlight colors!!                                    //
   /////////////////////////////////////////////////////////////////////////////
   const TColor _highlightPanelLockColor = ZonalFlickerRoiLockHighlightPanel->Color;
   const TColor _highlightPanelNoShowColor = ZonalFlickerRoiShowHighlightPanel->Color;
   const TColor _highlightPanelOffColor = ZonalFlickerRoiGroupBox->Color;

   class ZonalPresetParameters : public IPresetParameters
   {
   public:

      ZonalProcessType processType = ZonalProcessType::Unknown;
      bool processRed = true;
      bool processGreen = true;
      bool processBlue = true;

      bool processRoi = true;
      bool lockRoi = false;
      bool showRoi = true;
      RECT roi = {10000, 10000, 0, 0 };  // Illegal rect

      int boxesPerRow = 22;
      bool showBoxes = true;

      int temporalSmoothing = 10;

      bool hideAllOverlays = false;
      bool showMask = true;

      int headAnchorOffset = -1;
      int tailAnchorOffset = -1;
      int refFrameClipIndex = -1;

      string encodedMask;
      string maskFilename;

      virtual void ReadParameters(CIniFile *ini, const string &iniSectionName);
      virtual void WriteParameters(CIniFile *ini, const string &iniSectionName) const;
      virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const;

      bool operator==(const ZonalPresetParameters &rhs) const;

      static string readMask(const string &maskFilename);
      static void writeMask(const string &maskFilename, const string &maskAsString);
      static bool isMaskFileOk(const string &maskFilename);

      void dump() const;
   };

   PresetParameterModel<ZonalPresetParameters> _zonalPresets{"ZonalPresets", 4};
	DEFINE_CBHOOK(CurrentZonalPresetHasChanged, TColorBreathingForm);
public:
   void UpdateZonalPresetCurrentValues(string newMaskAsString = string(""));
private:
   void UpdateZonalGuiFromCurrentPreset();


public:		// User declarations

	void formCreate(void);
	void formShow(void);
	void formHide(void);

	void SavePresets();

	void updateGlobalGuiFromCurrentPreset(void);
	void updateZonalFlickerGui(void);

	void fullGuiUpdate();
	void referenceKeyFrameGuiUpdate();
	bool runExecButtonsCommand(EExecButtonId command);

	DEFINE_CBHOOK(CurrentGlobalPresetHasChanged, TColorBreathingForm);
	DEFINE_CBHOOK(CurrentDataHasChanged, TColorBreathingForm);
	DEFINE_CBHOOK(ClipHasChanged, TColorBreathingForm);
   DEFINE_CBHOOK(RefFrameOffsetHasChanged, TColorBreathingForm);

 	void GraphDataValidHasChanged();

	__fastcall TColorBreathingForm(TComponent* Owner);
  	__fastcall void OnBoundingBoxChange(TObject *sender);

    int getCurrentFrameIndex() const
    {
        return GColorBreathingTool->getSystemAPI()->getLastFrameIndex();
    }

	void UpdateStatusBarInfo(void);
   void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag);
   void ExternalUpdateProcessUnprocessed();
   void GuiDataRefresh();
   void GlobalGuiDataRefresh();
   void RefreshZonalFlickerAndExecButtonGuis();

	void selectZonalPreset(int n);

   bool _hideOverlaysPrevMaskVisibilityState = true;
   bool _hideOverlaysPrevRoiVisibilityState = true;
   bool _hideOverlaysPrevBoxesVisibilityState = true;

   // Storage for constrains as anchors are turned on or off
   bool _headAnchorWantedShadow = true;
   bool _tailAnchorWantedShadow = true;
   int _constraintsIndex = 0;
};
//---------------------------------------------------------------------------
extern PACKAGE TColorBreathingForm *ColorBreathingForm;
//---------------------------------------------------------------------------
#endif
