// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ColorBreathingGuiWin.h"
#include "ColorBreathingTool.h"

#include "ClipAPI.h"
#include "JobManager.h"
#include "MathFunctions.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "ShowModalDialog.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "PresetParameterModel.h"
#include "TimeLine.h"
#include <memory>
#include "ZonalFlickerDB.h"
#include "ColorBreathingTool.h"
#include "MTIsleep.h"
#include "MaskTool.h"

#define PLUGIN_TAB_INDEX 5
//#define ENABLE_PRESETS_TRACE

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
//#pragma link "CSPIN"
#pragma link "ExecButtonsFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "ReferenceKeyframeFrame"
#pragma link "ReferenceKeyframeListBox"
#pragma link "ProcessingRegionFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorLabel"
#pragma link "PresetsUnit"
#pragma link "SpinEditFrameUnit"
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TColorBreathingForm *ColorBreathingForm;

// ------------------CreateColorBreathingGUI---------------------

void CreateColorBreathingToolGUI(void)

      // This creates the ColorBreathing GUI if one does not already exist.
      // Note: only one can be created.
      //
      // ****************************************************************************
{
   if (ColorBreathingForm != nullptr)
   {
      return;
   } // Already created

   ColorBreathingForm = new TColorBreathingForm(Application); // Create it

   ColorBreathingForm->formCreate();
   ColorBreathingForm->RestoreProperties();

   // Reparent the controls to the UniTool
   extern const char *PluginName;

   GColorBreathingTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>(ColorBreathingForm->ColorBreathingControlPanel), nullptr);
}

// -----------------DestroyColorBreathingGUI------------------

void DestroyColorBreathingToolGUI(void)

      // This destroys then entire GUI interface
      //
      // ***************************************************************************
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   } // Already destroyed

   // Reparent the controls back to us before destroying them
   if (GColorBreathingTool != nullptr)
   {
      GColorBreathingTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
   }

   ColorBreathingForm->ColorBreathingControlPanel->Parent = ColorBreathingForm;

 //  ColorBreathingForm->Free();
   ColorBreathingForm = nullptr;
}

// -------------------ShowColorBreathingGUI-------------------

bool ShowColorBreathingToolGUI(void)

      // This creates the GUI if not already exists and then shows it
      //
      // ****************************************************************************
{
   CreateColorBreathingToolGUI(); // Create the gui if necessary

   if (ColorBreathingForm == nullptr || GColorBreathingTool == nullptr)
   {
      return false;
   }

   ColorBreathingForm->ColorBreathingControlPanel->Visible = true;
   ColorBreathingForm->formShow();

   return true;
}

// -------------------HideColorBreathingGUI-------------------

bool HideColorBreathingToolGUI(void)

      // This removes the tool from the screen
      //
      // ****************************************************************************
{
   if (ColorBreathingForm == nullptr || GColorBreathingTool == nullptr)
   {
      return false;
   }

   ColorBreathingForm->ColorBreathingControlPanel->Visible = false;
   ColorBreathingForm->formHide();

   return false; // controls are NOT visible !!?!
}

// ------------------IsToolVisible------------------John Mertus----Aug 2001----

bool IsToolVisible(void)

      // This returns the visual state of the gui
      //
      // ****************************************************************************
{
   if (ColorBreathingForm == nullptr)
   {
      return false; // Not created
   }

   return ColorBreathingForm->ColorBreathingControlPanel->Visible;
}

void ExternalGuiRefresh(int line)
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   }

   ColorBreathingForm->GuiDataRefresh();
}

// ---------------------------------------------------------------------------

void NotifyMaskChanged(const string &newMaskAsString)
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   }

   ColorBreathingForm->UpdateZonalPresetCurrentValues(newMaskAsString);
}

// ---------------------------------------------------------------------------

void NotifyMaskVisibilityChanged()
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   }

   ColorBreathingForm->UpdateZonalPresetCurrentValues();
}

// ---------------------------------------------------------------------------

void ToggleColorPicker(void)
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   }

   ColorBreathingForm->ColorPickButtonClick(nullptr);
}

// ---------------------------------------------------------------------------
void ToggleHideAllOverlays()
{
   ColorBreathingForm->HideAllOverlaysCheckBox->Checked =
         !ColorBreathingForm->HideAllOverlaysCheckBox->Checked;
   ExternalGuiRefresh(__LINE__);
}

bool AreAllOverlaysHidden()
{
   return ColorBreathingForm->HideAllOverlaysCheckBox->Checked;
}

// ---------------------------------------------------------------------------

void ExternalGuiReferenceFrameRefresh(int line)
{
   if (ColorBreathingForm == nullptr)
   {
      return;
   }

   ColorBreathingForm->GuiDataRefresh();
}

bool RunExecButtonsCommand(EExecButtonId command)
{
   if (ColorBreathingForm == NULL)
   {
      return false;
   }

   return ColorBreathingForm->runExecButtonsCommand(command);
}

void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag)

{
   if (ColorBreathingForm == 0)
   {
      return;
   }

   ColorBreathingForm->UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);
}

vector<TPanel*>cfl;

// ---------------------------------------------------------------------------
__fastcall TColorBreathingForm::TColorBreathingForm(TComponent* Owner) : TMTIForm(Owner)
{
   _presetButtonArray[0] = GLOBAL_PresetButton1;
   _presetButtonArray[1] = GLOBAL_PresetButton2;
   _presetButtonArray[2] = GLOBAL_PresetButton3;
   _presetButtonArray[3] = GLOBAL_PresetButton4;

   _TabStopDoNotDeletePanelVisible = false;

   ReferenceKeyframeListBox->Clear();
   ReferenceKeyframeListBox->OnDeleteReferenceKeyframe = DeleteReferenceKeyframeEvent;

   // Designer can't do this , not currently used
   AllChannelsButton->Tag = TL_GRAPH_TYPE_RGB;
   RedGraphSpeedButton->Tag = TL_GRAPH_TYPE_R;
   GreenGraphSpeedButton->Tag = TL_GRAPH_TYPE_B;
   BlueGraphSpeedButton->Tag = TL_GRAPH_TYPE_G;

   RedZonalProcessingSpeedButton->Tag = int(ChannelName::Red);
   GreenZonalProcessingSpeedButton->Tag = int(ChannelName::Green);
   BlueZonalProcessingSpeedButton->Tag = int(ChannelName::Blue);

   ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SetRange(0,5);
   ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SetRange(0,5);

   GlobalFlickerRoiLockHighlightPanel->Color = _highlightPanelOffColor;
   GlobalFlickerRoiShowHighlightPanel->Color = _highlightPanelOffColor;

#ifdef _DEBUG
   DebugButton->Visible = true;
   fastCheckBox->Visible = true;
#else
   DebugButton->Visible = false;
   fastCheckBox->Visible = false;
#endif
}

// Callback when a delete is issued.
void TColorBreathingForm::DeleteReferenceKeyframeEvent(const ReferenceKeyframeData &referenceKeyframeData)
{
   GColorBreathingTool->DeleteReferenceFrame(referenceKeyframeData);
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::formCreate(void)
{
   GColorBreathingTool->SetToolProgressMonitor(ExecStatusBar);
}

void TColorBreathingForm::formShow(void)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   // This should read tool and set it, but currently only user controls gui
   ColorBreathingPageControlChange(nullptr);

   auto globalPresets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();

   // Two directions for change
   SET_CBHOOK(CurrentGlobalPresetHasChanged, globalPresets->SelectedIndexChanged);
   SET_CBHOOK(CurrentDataHasChanged, SetupReferenceKeyframeFrame->DataHasChanged);
   SET_CBHOOK(ClipHasChanged, GColorBreathingTool->ClipChange);
   SET_CBHOOK(RefFrameOffsetHasChanged, ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SpinEditValueChange);
   SET_CBHOOK(RefFrameOffsetHasChanged, ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SpinEditValueChange);
   ReferenceKeyframeListBox->OnChange = ReferenceDataChanged;
   ReferenceKeyframeListBox->OnFrameClick = FrameLabelClicked;

   // This was moved to ColorBreathingPageControlChange()
   // UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
   this->_TabStopDoNotDeletePanelVisible = true;
   globalPresets->SetSelectedIndexAndCopyParametersToCurrent(0);

   // This appears not to be needed
   GColorBreathingTool->SetIsRoiDisplayed(ShowMattingCBox->Checked);
   GColorBreathingTool->getSystemAPI()->SetMainWindowTopPanelVisibility(true);

   // The actual values are set later but is the default
   BoxesTrackEdit->SetValue(22);
   BoxesTrackEdit->SetMinAndMax(2, 50);
   BoxesTrackEdit->Enable(true);

   SmoothingTrackEditFrame->SetValue(10);
   SmoothingTrackEditFrame->SetMinAndMax(1, 50);
   SmoothingTrackEditFrame->Visible = true;
   SmoothingTrackEditFrame->Enable(true);

   ZonalConstraintsAnchorsFirstFrameButton->Down = _headAnchorWantedShadow;
   ZonalConstraintsAnchorsLastFrameButton->Down = _tailAnchorWantedShadow;
//   ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SetValue(0);
//   ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SetValue(0);
   ZonalConstraintsSetRefFrameButtonClick(nullptr);
   ZonalConstraintsNoneRadioButton->Checked = true;
   ZonalConstraintsAnchorsPanel->Visible = false;
   ZonalConstraintsRefFramePanel->Visible = false;

   auto iniFileName = CPMPIniFileName("ColorBreathing");
   _zonalPresets.ReadPresetsFromIni(iniFileName);
// Don't try to load a preset - just let the tool do the desktop.ini thing at startup!
//   _zonalPresets.SetSelectedIndexAndCopyParametersToCurrent(0);
   SET_CBHOOK(CurrentZonalPresetHasChanged, _zonalPresets.SelectedIndexChanged);
   ZonalPresetsFrame->Initialize(&_zonalPresets, iniFileName);

   updateZonalFlickerGui();

   // Press the right button (UGH)
   switch (_constraintsIndex)
   {
      case 0:
        ZonalConstraintsNoneRadioButton->Checked = true;
        break;

      case 1:
        ZonalConstraintsAnchorsRadioButton->Checked = true;
        break;

      case 2:
        ZonalConstraintsRefFrameRadioButton->Checked = true;
        break;

   }
}

void TColorBreathingForm::formHide(void)
{
   // Save constraints
   if (ZonalConstraintsNoneRadioButton->Checked)
   {
      _constraintsIndex = 0;
   }
   else if (ZonalConstraintsAnchorsRadioButton->Checked)
   {
      _constraintsIndex = 1;
   }
   else if (ZonalConstraintsRefFrameRadioButton->Checked)
   {
      _constraintsIndex = 2;
   }

   auto presets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();
   REMOVE_CBHOOK(CurrentGlobalPresetHasChanged, presets->SelectedIndexChanged);
   REMOVE_CBHOOK(CurrentDataHasChanged, SetupReferenceKeyframeFrame->DataHasChanged);
   REMOVE_CBHOOK(ClipHasChanged, GColorBreathingTool->ClipChange);
   REMOVE_CBHOOK(RefFrameOffsetHasChanged, ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(RefFrameOffsetHasChanged, ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SpinEditValueChange);
   GColorBreathingTool->getSystemAPI()->SetMainWindowTopPanelVisibility(false);
}

void TColorBreathingForm::CurrentGlobalPresetHasChanged(void *Sender)
{
   updateGlobalGuiFromCurrentPreset();
}

void TColorBreathingForm::CurrentZonalPresetHasChanged(void *Sender)
{
   UpdateZonalGuiFromCurrentPreset();
}

void TColorBreathingForm::CurrentDataHasChanged(void *Sender)
{
   auto presets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();
   auto referenceKeyframeData = SetupReferenceKeyframeFrame->GetReferenceKeyFrameData();
   presets->SetCurrent(referenceKeyframeData);
   GLOBAL_SavePresetButton->Enabled = !presets->DoesCurrentMatchSelected();
   GLOBAL_DisabledSaveButton->Visible = !GLOBAL_SavePresetButton->Enabled;
}

void TColorBreathingForm::GraphDataValidHasChanged()
{
   auto rangeValid = GColorBreathingTool->getGlobalFlickerModel()->RGBFrameData.IsRangeValid();
   auto dataValid = GColorBreathingTool->getGlobalFlickerModel()->RGBFrameData.IsDataValid();

   // Enable/disable the changes
   AddRefKeyFrameButton->Enabled = GColorBreathingTool->CanWeAddReferenceFrame();

   AllChannelsButton->Enabled = dataValid;
   RedGraphSpeedButton->Enabled = dataValid;
   GreenGraphSpeedButton->Enabled = dataValid;
   BlueGraphSpeedButton->Enabled = dataValid;
   GraphGroupBox->Enabled = dataValid;
}

void TColorBreathingForm::SetButtonCommand(TToolButton *button, EToolProcessingCommand newCmd)
{
   // Set the tool processing command in the button's Tag and set the
   // the matching text label and icon for the button

   // Save the tool processing command in the button's Tag
   button->Tag = newCmd;

   string buttonString;
   System::Uitypes::TImageIndex iconIndex;
   switch (newCmd)
   {
   case TOOL_PROCESSING_CMD_PREVIEW:
      buttonString = "Preview";
      iconIndex = 2;
      break;
   case TOOL_PROCESSING_CMD_RENDER:
      buttonString = "Render";
      iconIndex = 1;
      break;
   case TOOL_PROCESSING_CMD_PAUSE:
      buttonString = "Pause";
      iconIndex = 7;
      break;
   case TOOL_PROCESSING_CMD_CONTINUE:
      buttonString = "Continue";
      iconIndex = 8;
      break;
   default:
      buttonString = "ERROR!"; // Shouldn't ever happen
      iconIndex = 3; // Stop sign
      break;
   }

   button->Caption = buttonString.c_str();
   button->ImageIndex = iconIndex;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ------------------------ EXEC BUTTONS TOOLBAR -----------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender)
{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
   default:
   case EXEC_BUTTON_NONE:
      // No-op
      break;

   case EXEC_BUTTON_PREVIEW_FRAME:
      //// Removed via larry
      //// ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW_1);
      break;

   case EXEC_BUTTON_PREVIEW_ALL:
      //// Removed via larry
      //// ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
      break;

   case EXEC_BUTTON_RENDER_FRAME:
      //// ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER_1);
      break;

   case EXEC_BUTTON_RENDER_ALL:
      ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
      break;

   case EXEC_BUTTON_PAUSE_OR_RESUME:
      break;

   case EXEC_BUTTON_PAUSE:
      /// ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
      break;

   case EXEC_BUTTON_RESUME:
      /// ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

   case EXEC_BUTTON_STOP:
      ExecStop();
      break;

   case EXEC_BUTTON_CAPTURE_PDL:
      ExecCaptureToPDL();
      break;

   case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      ExecCaptureAllEventsToPDL();
      break;

   case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
      ExecGoToResumeTimecode();
      break;

   case EXEC_BUTTON_SET_RESUME_TC:
      ExecSetResumeTimecodeToCurrent();
      break;
   }
}

void TColorBreathingForm::ExecCaptureAllEventsToPDL(void)
{
   if (GColorBreathingTool == NULL)
   {
      return;
   }

   // KLUDGE: reset database between marks
   auto model = GColorBreathingTool->getZonalFlickerModel();
   GColorBreathingTool->getClipPreprocessData().setBetweenMarksToParameters(*model);
   GColorBreathingTool->getSystemAPI()->CapturePDLEntry(true);
}

void TColorBreathingForm::ExecCaptureToPDL(void)
{
   if (GColorBreathingTool == NULL)
   {
      return;
   }

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   {
      all = true;
   }

   GColorBreathingTool->getSystemAPI()->CapturePDLEntry(all);
}

// ------------------ ExecRenderOrPreview ------------------------------------
//
// Call this when any render or preview button is clicked
// buttonCommand identifies the button that was pressed
//
void TColorBreathingForm::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
   bool flickerCommand = GColorBreathingTool->getSubToolType() == SubToolType::ZonalFlicker;
   EToolProcessingCommand command;

   switch (buttonCommand)
   {
   case TOOL_PROCESSING_CMD_RENDER:
      command = flickerCommand ? FLICKER_TOOL_RENDER_CMD : TOOL_PROCESSING_CMD_RENDER;
      GColorBreathingTool->ButtonCommandHandler(command);
      break;

   case TOOL_PROCESSING_CMD_PREVIEW_1:
      GColorBreathingTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_PREVIEW_1);
      break;

   case TOOL_PROCESSING_CMD_PREVIEW:
      command = flickerCommand ? FLICKER_TOOL_PREVIEW_CMD : TOOL_PROCESSING_CMD_PREVIEW;
      GColorBreathingTool->ButtonCommandHandler(command);
      break;

   case TOOL_PROCESSING_CMD_PAUSE:
      ///         GAutoFilter->PauseToolProcessing();
      break;

   case TOOL_PROCESSING_CMD_CONTINUE:
      // {
      // CVideoFrameList *frameList;
      // frameList = GAutoFilter->getSystemAPI()->getVideoFrameList();
      // int resumeFrame;
      // if (frameList == NULL)
      // resumeFrame = GAutoFilter->getSystemAPI()->getMarkIn();
      // else
      // {
      // PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
      // resumeFrame = frameList->getFrameIndex(resumeTimecode);
      // }
      //
      // GAutoFilter->ContinueToolProcessing(resumeFrame);
      // }
      break;

   default:
      break;
   }
}
// ---------------- Resume Timecode Functions -------------------------------------------

void __fastcall TColorBreathingForm::SetStartTCButtonClick(TObject *Sender)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   // This gets the current time code
   CVideoFrameList *frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
   if (frameList == nullptr)
   {
      return;
   }

   GColorBreathingTool->ResetResumeFrameIndex(getCurrentFrameIndex());
   //// ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(currentFrameIndex);
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   if (!GColorBreathingTool->IsActive())
   {
      return;
   }

   int previewFrameIndex = GColorBreathingTool->GetPreviewFrameIndex();
   bool bEnablePause = false;

   switch (toolProcessingControlState)
   {
   case TOOL_CONTROL_STATE_STOPPED:
      // Previewing or Rendering has stopped, either because processing
      // has completed or the user pressed the Stop button
      SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PREVIEW);
      SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_RENDER);
      GColorBreathingTool->onTopPanelRedraw();
      break;

   case TOOL_CONTROL_STATE_RUNNING:
      // Preview1, Preview or Render is now running
      if (previewFrameIndex < 0)
      {
         if (processingRenderFlag)
         {
            // Rendering
            PreviewAllButton->Enabled = false;
            if (bEnablePause)
            {
               // only allow pause during render
               RenderButton->Enabled = true;
               SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_PAUSE);
            }
            else
            {
               RenderButton->Enabled = false;
            }
         }
         else
         {
            // Previewing
            RenderButton->Enabled = false;
            if (bEnablePause)
            {
               // only allow pause during render
               PreviewAllButton->Enabled = true;
               SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_PAUSE);
            }
            else
            {
               PreviewAllButton->Enabled = false;
            }
         } // if (processingRenderFlag)
      } // if (previewFrameIndex)
      else
      {
         // Previewing One Frame
         PreviewAllButton->Enabled = false;
         RenderButton->Enabled = false;
      }
      break;

   case TOOL_CONTROL_STATE_PAUSED:
      // Previewing or Rendering is now Paused
      if (processingRenderFlag)
      {
         // Paused while Rendering
         PreviewAllButton->Enabled = false;

         SetButtonCommand(RenderButton, TOOL_PROCESSING_CMD_CONTINUE);
         RenderButton->Enabled = true;
      }
      else
      {
         // Paused while Previewing
         RenderButton->Enabled = false;

         SetButtonCommand(PreviewAllButton, TOOL_PROCESSING_CMD_CONTINUE);
         PreviewAllButton->Enabled = true;
      }

      // Now go load the current timecode
      SetStartTCButtonClick(nullptr);

      break;

   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
      // Previewing 1 frame, disable everything
      PreviewAllButton->Enabled = false;
      RenderButton->Enabled = false;
      break;

   case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
   case TOOL_CONTROL_STATE_WAITING_TO_STOP:
      // In transition to Pause or Stop, disable everything
      PreviewAllButton->Enabled = false;
      RenderButton->Enabled = false;
      break;

   default:
      MTIassert(false);
      break;
   }

   UpdateExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag);

} // UpdateExecutionButtons()

// ---------------------------------------------------------------------------

void TColorBreathingForm::UpdateExecutionButtonToolbar(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
   switch (GColorBreathingTool->getSubToolType())
   {
   case SubToolType::GlobalFlicker:
      UpdateColorBreathingExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag);
      break;

   case SubToolType::ZonalFlicker:
      UpdateSpatialFlickerExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag);
      break;

   default:
      MTIassert(false);
      break;
   }
}

void TColorBreathingForm::UpdateColorBreathingExecutionButtonToolbar(EToolControlState toolProcessingControlState,
      bool processingRenderFlag)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   int previewFrameIndex = GColorBreathingTool->GetPreviewFrameIndex();
   bool processingSingleFrameFlag = (previewFrameIndex >= 0);
   auto model = GColorBreathingTool->getGlobalFlickerModel();
   bool rangeValid = GColorBreathingTool->getGlobalFlickerModel()->RGBFrameData.IsRangeValid();

   auto cbState = GColorBreathingTool->getControlState();

   if (GColorBreathingTool->IsDisabled())
   {
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Can't render - tool is disabled");
      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't capture - tool is disabled");
      return;
   }

   // Check the validity of the RGB data
   auto rgbDataValid = model->RGBFrameData.CheckAndSetDataValid();
   GraphDataValidHasChanged();

   switch (toolProcessingControlState)
   {
   case TOOL_CONTROL_STATE_STOPPED:

      // Previewing or Rendering has stopped, either because processing
      // has completed or the user pressed the Stop button

      setIdleCursor();

      if (rgbDataValid)
      {
         GraphDataValidHasChanged();
         if (GColorBreathingTool->CanWeRender())
         {
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Render marked range (Shift+G)");
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Add to PDL (, key)");
         }
         else
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
         }
      }
      else
      {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
      }

      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

      ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

      ExecButtonsToolbar->EnableResumeWidget();
      ExecSetResumeTimecodeToDefault();

      GColorBreathingTool->RenderedConsistencyCheck();
      GColorBreathingTool->setRenderInProgress(false);
      break;

   case TOOL_CONTROL_STATE_RUNNING:

      // Preview1, Render1, Preview or Render is now running
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      if (processingSingleFrameFlag || (cbState == EMultiControlState::ACQURING))
      {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      }
      else
      {
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
      }

      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      // **** Per Larry, the preview buttons should NEVER be enabled!
		if (!processingRenderFlag)
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
		else
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

      break;

   case TOOL_CONTROL_STATE_PAUSED:
      // Previewing or Rendering is now Paused

      setIdleCursor();

      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      // Now go load the current timecode
      ExecSetResumeTimecodeToCurrent();

      break;

   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
      MTIassert(false);
      /* FALL THROUGH */

   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
   case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
   case TOOL_CONTROL_STATE_WAITING_TO_STOP:

      // Processing single frame or waiting to stop or pause --
      // disable everything
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE && toolProcessingControlState !=
            TOOL_CONTROL_STATE_WAITING_TO_STOP)
      {
         // **** Per Larry, the preview buttons should NEVER be enabled!
         MTIassert(processingRenderFlag);

         if (!processingRenderFlag)
         {
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
         }
         else
         {
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
         }
      }

      break;

   default:
      MTIassert(false);

      break;
   }

   // **** Per Larry, preview buttons should NEVER be enabled!
   ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
   ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
   ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_FRAME, "Use T key to preview fixed frames");
   ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Use T key to preview fixed frames");

   UpdateStatusBarInfo();

} // UpdateExecutionButtonToolbar()

void TColorBreathingForm::UpdateSpatialFlickerExecutionButtonToolbar(EToolControlState toolProcessingControlState,
      bool processingRenderFlag)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   auto model = GColorBreathingTool->getZonalFlickerModel();
   auto &zonalFlickerParameters = GColorBreathingTool->getZonalFlickerParameters();

   auto cbState = GColorBreathingTool->getControlState();

   int previewFrameIndex = GColorBreathingTool->GetPreviewFrameIndex();
   auto processingSingleFrameFlag = (previewFrameIndex >= 0);

   if (GColorBreathingTool->IsDisabled())
   {
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Can't render - tool is disabled");
      ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't capture - tool is disabled");
      return;
   }

   // Check the validity of the RGB data

   switch (toolProcessingControlState)
   {
   case TOOL_CONTROL_STATE_STOPPED:

      // Previewing or Rendering has stopped, either because processing
      // has completed or the user pressed the Stop button

      setIdleCursor();

      // We should be asking the model, not the tool
      if (true /* FIX THIS xxxxx */)
      {
         if (GColorBreathingTool->CanWeRender())
         {
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Render marked range (Shift+G)");
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Add to PDL (, key)");
         }
         else
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Add to PDL (, key)");
         }
      }
      else
      {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
      }

      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

      ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

      ExecButtonsToolbar->EnableResumeWidget();
      ExecSetResumeTimecodeToDefault();

      break;

   case TOOL_CONTROL_STATE_RUNNING:

      // Preview1, Render1, Preview or Render is now running
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);

      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      // **** Per Larry, the preview buttons should NEVER be enabled!
		if (!processingRenderFlag)
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
		else
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

      break;

   case TOOL_CONTROL_STATE_PAUSED:
      // Previewing or Rendering is now Paused

      setIdleCursor();

      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      // Now go load the current timecode
      ExecSetResumeTimecodeToCurrent();

      break;

   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
      MTIassert(false);
      /* FALL THROUGH */

   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
   case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
   case TOOL_CONTROL_STATE_WAITING_TO_STOP:

      // Processing single frame or waiting to stop or pause --
      // disable everything
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
      ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
      ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
      ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

      if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE && toolProcessingControlState !=
            TOOL_CONTROL_STATE_WAITING_TO_STOP)
      {
         // **** Per Larry, the preview buttons should NEVER be enabled!
			if (!processingRenderFlag)
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
			else
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
		}

      break;

   default:
      MTIassert(false);

      break;
   }

   // **** Per Larry, preview buttons should NEVER be enabled!
   ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
   ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
   ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_FRAME, "Use T key to preview fixed frames");
   ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Use T key to preview fixed frames");

   UpdateStatusBarInfo();

} // UpdateSpatialFlickerExecutionButtonToolbar()

void TColorBreathingForm::setIdleCursor(void)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   if (!GColorBreathingTool->IsInColorPickMode())
   {
      Screen->Cursor = crDefault;
   }
}

// ---------------------------------------------------------------------------
void TColorBreathingForm::UpdateStatusBarInfo(void)
{
   ////  ExecStatusBar->SetToolMessage(StringToStdString(sTmp + " Points").c_str());
}

void __fastcall TColorBreathingForm::AllGraphColorsButtonClick(TObject *Sender)
{
   // graphinfo should be setters and dirtyness set below
   auto &graphInfo = GColorBreathingTool->getGlobalFlickerModel()->GetGraphInfo();
   graphInfo.RedEnabled = true;
   graphInfo.GreenEnabled = true;
   graphInfo.BlueEnabled = true;
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphDirty();
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::updateGlobalGuiFromCurrentPreset()
{
   auto presets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();
   SetupReferenceKeyframeFrame->CopyToReferenceKeyFrameData(presets->GetCurrentPreset());
   GLOBAL_SavePresetButton->Enabled = !presets->DoesCurrentMatchSelected();
   GLOBAL_DisabledSaveButton->Visible = !GLOBAL_SavePresetButton->Enabled;

   _presetButtonArray[presets->GetSelectedIndex()]->Down = true;
}

void TColorBreathingForm::fullGuiUpdate()   // WTF is this????
{
   GuiDataRefresh();
   updateGlobalGuiFromCurrentPreset();
   updateZonalFlickerGui();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ReferenceFrameListBoxDrawItem(TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   // if (cfl[Index]->Top != Rect.Top)
   cfl[Index]->Top = Rect.Top;
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::RedGraphButtonClick(TObject *Sender)
{
   auto &graphInfo = GColorBreathingTool->getGlobalFlickerModel()->GetGraphInfo();
   graphInfo.RedEnabled = true;
   graphInfo.GreenEnabled = false;
   graphInfo.BlueEnabled = false;
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphDirty();
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::GreenGraphButtonClick(TObject *Sender)
{
   auto &graphInfo = GColorBreathingTool->getGlobalFlickerModel()->GetGraphInfo();
   graphInfo.RedEnabled = false;
   graphInfo.GreenEnabled = true;
   graphInfo.BlueEnabled = false;
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphDirty();
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::BlueGraphButtonClick(TObject *Sender)
{
   auto &graphInfo = GColorBreathingTool->getGlobalFlickerModel()->GetGraphInfo();
   graphInfo.RedEnabled = false;
   graphInfo.GreenEnabled = false;
   graphInfo.BlueEnabled = true;
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphDirty();
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::GLOBAL_SavePresetButtonClick(TObject *Sender)
{
   this->SavePresets();
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::SavePresets()
{
   auto presets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();

   // Store the changes to the selected preset
   presets->CopyCurrentParametersToSelectedPreset();

   // Todo  Decide where the ini file name should exist
   auto IniFileName = CPMPIniFileName("ColorBreathing");
   presets->WritePresetsToIni(IniFileName);
   updateGlobalGuiFromCurrentPreset();
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::GLOBAL_PresetButtonClick(TObject *Sender)
{
   auto speedButton = dynamic_cast<TSpeedButton*>(Sender);

   // No nothing if a not a speed button
   if (speedButton == nullptr)
   {
      return;
   }

   auto index = (int)(speedButton->Tag);
   auto presets = GColorBreathingTool->getGlobalFlickerModel()->GetPresets();

   // Call back will set gui
   presets->SetSelectedIndexAndCopyParametersToCurrent(index);
}

// ------------------ClipHasChanged----------------John Mertus----Nov 2002-----

void TColorBreathingForm::ClipHasChanged(void *Sender)

      // This is called when the clip is loaded into the main window
      //
      // ****************************************************************************
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   // The display must be in timecodes but everything else works in index, so get
   // the start timecode and store it in the gui for the display
   // Ugly as sin
   auto videoFrameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
   auto srcMinFrame = videoFrameList->getInFrameIndex();
   auto framePtr = videoFrameList->getFrame(srcMinFrame);
   framePtr->getTimecode(_inTimecode);
   ReferenceKeyframeListBox->SetInTimecode(_inTimecode);

   referenceKeyFrameGuiUpdate();

   auto imageFormat = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::AddRefKeyFrameButtonClick(TObject *Sender)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   GColorBreathingTool->AddReferenceFrameAtCurrentIndex();
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::referenceKeyFrameGuiUpdate()
{
   auto model = GColorBreathingTool->getGlobalFlickerModel();
   if (!model->IsDataInitialized)
   {
      return;
   }

   auto currentFrameIndex = getCurrentFrameIndex();

   // Set the text
   auto pair = model->BreakFrameData.GetBreakPair(currentFrameIndex);

   string timecodeString;
   (_inTimecode + pair.first).getTimeString(timecodeString);
   InLabel->Caption = timecodeString.c_str();

   (_inTimecode + pair.second).getTimeString(timecodeString);
   OutLabel->Caption = timecodeString.c_str();
   DurationLabel->Caption = IntToStr(pair.second - pair.first + 1);

   auto referenceKeyframeDataList = model->BreakFrameData.GetReferenceKeyFrames(currentFrameIndex);
   ReferenceKeyframeListBox->NoChangeAllowed(model->RGBFrameData);
   ReferenceKeyframeListBox->SetKeyframeDataList(referenceKeyframeDataList);
   ReferenceKeyframeListBox->SelectReferenceFrame(currentFrameIndex);

   // This is probably not needed
   model->RGBFrameData.CheckAndSetDataValid();
   GColorBreathingTool->onTopPanelRedraw();
   GraphDataValidHasChanged();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::GenerateGraphButtonClick(TObject *Sender)
{
   if (GColorBreathingTool != nullptr)
   {
      GColorBreathingTool->ButtonCommandHandler(CB_TOOL_GRAPH_CMD);
   }
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ProcessingRegionShowMattingCBoxClick(TObject *Sender)
{
   GColorBreathingTool->SetIsRoiDisplayed(ShowMattingCBox->Checked);
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ProcessingRegionProcessingCBoxClick(TObject *Sender)
{
   GColorBreathingTool->setUseProcessingRegion(ProcessingCBox->Checked);
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::FrameLabelClicked(ReferenceKeyframeData *referenceFrameData)
{
   if ((GColorBreathingTool == nullptr) || (referenceFrameData == nullptr))
   {
      return;
   }

   auto frameIndex = referenceFrameData->FrameIndex;
   GColorBreathingTool->getSystemAPI()->goToFrameSynchronous(frameIndex);
}

void __fastcall TColorBreathingForm::ReferenceDataChanged(TObject *Sender)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   GColorBreathingTool->getGlobalFlickerModel()->BreakFrameData.SetDirty(true);
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphDirty();
   GColorBreathingTool->onTopPanelRedraw();
   if (DisplayRadioGroup->ItemIndex == 1)
   {
      ProcessPreviewOneFrameMode();
   }
}

// ---------------- Resume Timecode Functions --------------------------------

void TColorBreathingForm::ExecSetResumeTimecodeToDefault(void)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   int frameIndex = GColorBreathingTool->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);
}

void TColorBreathingForm::ExecSetResumeTimecode(int frameIndex)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   CVideoFrameList *frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL)
   {
      return;
   }

   if (frameIndex < 0)
   {
      frameIndex = frameList->getInFrameIndex();
   }

   GColorBreathingTool->ResetResumeFrameIndex(frameIndex);
   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::ExecSetResumeTimecodeToCurrent(void)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   int frameIndex = getCurrentFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::ExecGoToResumeTimecode(void)
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   CVideoFrameList *frameList;
   frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL)
   {
      return;
   }

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GColorBreathingTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = getCurrentFrameIndex();
   GColorBreathingTool->ResetResumeFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameList->getTimecodeForFrameIndex(frameIndex));
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::ExecStop(void)
{
   if (GColorBreathingTool == NULL)
   {
      return;
   }

   GColorBreathingTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_STOP);
}

void __fastcall TColorBreathingForm::CenterButtonClick(TObject *Sender)
{
   auto model = GColorBreathingTool->getGlobalFlickerModel();
   model->SetCenterState(CenterButton->Down);
   referenceKeyFrameGuiUpdate();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ProcessingRegionLockMattingCBoxClick(TObject *Sender)
{
   GColorBreathingTool->SetIsRoiLocked(LockMattingCBox->Checked);
   GlobalGuiDataRefresh();
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::ProcessPreviewOneFrameMode()
{
   GColorBreathingTool->ButtonCommandHandler(TOOL_PROCESSING_CMD_PREVIEW_1);
}

void TColorBreathingForm::ExternalUpdateProcessUnprocessed()
{
   if (GColorBreathingTool->getSystemAPI()->IsProcessedVisible())
   {
      DisplayRadioGroup->ItemIndex = 1;
      ZonalDisplayRadioGroup->ItemIndex = 1;
   }
   else
   {
      DisplayRadioGroup->ItemIndex = 0;
      ZonalDisplayRadioGroup->ItemIndex = 0;
   }
}

void __fastcall TColorBreathingForm::DisplayRadioGroupClick(TObject *Sender)
{
   GColorBreathingTool->SetInPreviewMode(DisplayRadioGroup->ItemIndex == 1);
}
// ---------------------------------------------------------------------------

int _oldDisplayRadioGroupItemIndex = 0;

void TColorBreathingForm::GuiDataRefresh()
{
   switch (GColorBreathingTool->getSubToolType())
   {
   case SubToolType::GlobalFlicker:
      GlobalGuiDataRefresh();
      break;

   case SubToolType::ZonalFlicker:
      RefreshZonalFlickerAndExecButtonGuis();
      break;

   default:
      MTIassert(false);
      break;
   }
}

void TColorBreathingForm::GlobalGuiDataRefresh()
{
   // Hack to avoid slow playing -jam
   // We should update on shot boundary crossings
   if (!GColorBreathingTool->getSystemAPI()->IsPlayerReallyPlaying())
   {
      DisplayRadioGroup->ItemIndex = GColorBreathingTool->GetInGlobalFlickerPreviewMode() ? 1 : 0;
      auto model = GColorBreathingTool->getGlobalFlickerModel();
      auto &graphInfo = GColorBreathingTool->getGlobalFlickerModel()->GetGraphInfo();
      RedGraphSpeedButton->Down = graphInfo.RedEnabled;
      GreenGraphSpeedButton->Down = graphInfo.GreenEnabled;
      BlueGraphSpeedButton->Down = graphInfo.BlueEnabled;
      CenterButton->Down = model->GetCenterState();

      GlobalFlickerRoiAnalysisCheckBox->Checked = GColorBreathingTool->getUseAnalysisRegion();
      ProcessingCBox->Checked = GColorBreathingTool->getUseProcessingRegion();

      LockMattingCBox->Checked = GColorBreathingTool->GetIsRoiLocked();
      GlobalFlickerRoiLockHighlightPanel->Color = LockMattingCBox->Checked
                                                   ? _highlightPanelLockColor
                                                   : _highlightPanelOffColor;

      ShowMattingCBox->Checked = GColorBreathingTool->GetIsRoiDisplayed();
      GlobalFlickerRoiShowHighlightPanel->Color = ShowMattingCBox->Checked
                                                   ? _highlightPanelOffColor
                                                   : _highlightPanelNoShowColor;

      /* removed per larry */ GroupBox1->Height = GColorBreathingTool->GetIsRoiDisplayed() ? 41 : 41; // 60;
      referenceKeyFrameGuiUpdate();

      // See if the frames are rendered
      if (!GColorBreathingTool->CanWeToggle())
      {
         if (DisplayRadioGroup->Enabled)
         {
            _oldDisplayRadioGroupItemIndex = DisplayRadioGroup->ItemIndex;
            DisplayRadioGroup->Enabled = false;
            DisplayRadioGroup->ItemIndex = 0;
         }
      }
      else if (!DisplayRadioGroup->Enabled)
      {
         DisplayRadioGroup->Enabled = true;
         DisplayRadioGroup->ItemIndex = _oldDisplayRadioGroupItemIndex;
      }

      // This really should set a state
      // TODO: put in right place
      if (GColorBreathingTool->CanWeRender())
      {
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
      }
      else
      {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
      }
   }

   GColorBreathingTool->onTopPanelRedraw();
}

void TColorBreathingForm::RefreshZonalFlickerAndExecButtonGuis()
{
   // Hack to avoid slow playing -jam
   if (GColorBreathingTool->getSystemAPI()->IsPlayerReallyPlaying())
   {
      return;
   }

   updateZonalFlickerGui();

   auto controlState = GColorBreathingTool->getToolProcessingControlState();
   UpdateExecutionButtonToolbar(controlState, true);
   ExecStatusBar->updateReadouts();
}

bool TColorBreathingForm::runExecButtonsCommand(EExecButtonId command)
{
   return ExecButtonsToolbar->RunCommand(command);
}

void __fastcall TColorBreathingForm::DumpDebugInfoCheckBoxClick(TObject *Sender)
{
   ////   GColorBreathingTool->getGlobalFlickerModel()->SetDumpDebugInfo(DumpDebugInfoCheckBox->Checked);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::GraphResetButtonClick(TObject *Sender)
{
   GColorBreathingTool->getGlobalFlickerModel()->SetGraphFinished(true);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::Fix1ButtonClick(TObject *Sender)
{
   ///     GColorBreathingTool->getGlobalFlickerModel()->DumpRGBData();
   GColorBreathingTool->getGlobalFlickerModel()->LoadRGBData();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalAnalyzeButtonClick(TObject *Sender)
{
   // See if we are legal
   if (GColorBreathingTool->getZonalProcessType() == ZonalProcessType::Unknown)
   {
      _MTIErrorDialog(Handle, "Please choose B&W or Color");
      return;
   }

   if (GColorBreathingTool->areMarksValid() == false)
   {
      if (_MTIConfirmationDialog(Handle, "No breaks or marks defined\nYes to process entire clip") != MTI_DLG_OK)
      {
         return;
      }
   }

   try
   {
      updateZonalFlickerGui();
      GColorBreathingTool->onlyRunZonalPreprocessing();
   }
   catch (const std::exception &ex)
   {
      _MTIErrorDialog(Handle, ex.what());
   }

// ??? We just did this in the try/catch!!  QQQ
//	updateZonalFlickerGui();

// WTF - WHAT WAS THE INTENTION HERE??  QQQ
//	UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ColorBreathingPageControlChange(TObject *Sender)
{
   if (ColorBreathingPageControl->ActivePage == ColorBreathingTabSheet)
   {
      GColorBreathingTool->SetSubToolType(SubToolType::GlobalFlicker);
   }
   else if (ColorBreathingPageControl->ActivePage == FlickerTabSheet)
   {
      GColorBreathingTool->SetSubToolType(SubToolType::ZonalFlicker);
   }
   else
   {
      MTIassert(false);
   }

   GColorBreathingTool->makeTileBoxes();

   GuiDataRefresh();
   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
}

// ---------------------------------------------------------------------------

void TColorBreathingForm::updateZonalFlickerGui()
{
   if (GColorBreathingTool == nullptr)
   {
      return;
   }

   // DO NOT RE-ENTER!
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   try
   {
      // Ignore if the database is not yet initialized
      if (GColorBreathingTool->getClipPreprocessData().getZonalDatabase().isConnected() == false)
      {
         TRACE_3(errout << "Zonal database not yet initialized");
// WTF         return;
      }

      // REENTRY GUARD
      _updatingZonalFlickerGui = true;

      auto model = GColorBreathingTool->getZonalFlickerModel();
      auto &currentZonalParameters = GColorBreathingTool->getZonalFlickerParameters();

      auto isRoiLegal = model->isAnalysisRoiValid();
      auto globalEnable = (GColorBreathingTool->getRenderState() == RENDER_STATE_IDLE) ||
            (GColorBreathingTool->getRenderState() == RENDER_STATE_ENDED);

      auto markIn = GColorBreathingTool->getSystemAPI()->getMarkIn();
	   auto markOut = GColorBreathingTool->getSystemAPI()->getMarkOut();
      auto frameIndexInsideMarks = (markIn <= getCurrentFrameIndex()) && (markOut > getCurrentFrameIndex());
		globalEnable &= frameIndexInsideMarks;
      InvalidIndexPanel->Visible = !frameIndexInsideMarks;
      if (!globalEnable)
      {
         ///  GColorBreathingTool->SetInZonalFlickerPreviewMode(false);
      }

      HideAllOverlaysHighlightPanel->Color = HideAllOverlaysCheckBox->Checked
                                                   ? _highlightPanelNoShowColor
                                                   : _highlightPanelOffColor;

      // ROI
      RoiWarningColorLabel->Visible = !isRoiLegal;
      RoiWarningColorLabel->Caption = "Please choose an ROI";

      // removed per larry   ZonalFlickerRoiUseCheckBox->Checked = true; // REMOVE USE button
      ZonalFlickerRoiProcessingCheckBox->Checked = model->getUseProcessingRegion();
      /* removed per larry */ ZonalFlickerRoiGroupBox->Height = GColorBreathingTool->getIsZonalFlickerRoiDisplayed() ? 41 : 41; // 60;
      auto previewWanted = GColorBreathingTool->GetInZonalFlickerPreviewMode();
      ZonalDisplayRadioGroup->ItemIndex = previewWanted ? 1 : 0;

      ZonalFlickerRoiLockCheckBox->Checked = GColorBreathingTool->getIsZonalFlickerRoiLocked();
      ZonalFlickerRoiLockHighlightPanel->Color = ZonalFlickerRoiLockCheckBox->Checked
                                                   ? _highlightPanelLockColor
                                                   : _highlightPanelOffColor;

      ZonalFlickerRoiShowCheckBox->Checked = GColorBreathingTool->getIsZonalFlickerRoiDisplayed();
      ZonalFlickerRoiShowCheckBox->Enabled = !HideAllOverlaysCheckBox->Checked;
      DisabledShowRoiPanel->Visible = HideAllOverlaysCheckBox->Checked;
      ZonalFlickerRoiShowHighlightPanel->Color = ZonalFlickerRoiShowCheckBox->Checked
                                                   ? _highlightPanelOffColor
                                                   : _highlightPanelNoShowColor;

      ResetZonalRoiButton->Enabled = !GColorBreathingTool->getIsZonalFlickerRoiLocked();

      // CONSTRAINTS
      auto headAnchorOffset = GColorBreathingTool->getAnchorFrameOffsets().first;
      auto headAnchorWanted = GColorBreathingTool->getZonalFlickerModel()->getUseHeadAnchor();
      ZonalConstraintsAnchorsFirstFrameButton->Enabled = true;
      ZonalConstraintsAnchorsFirstFrameButton->Down = headAnchorWanted;
      ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->Activate(headAnchorWanted);
      ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->Enable(headAnchorWanted);
      if (headAnchorWanted)
      {
         ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SetValue(headAnchorOffset);
      }

      auto tailAnchorOffset = GColorBreathingTool->getAnchorFrameOffsets().second;
      auto tailAnchorWanted = GColorBreathingTool->getZonalFlickerModel()->getUseTailAnchor();
      ZonalConstraintsAnchorsLastFrameButton->Enabled = true;
      ZonalConstraintsAnchorsLastFrameButton->Down = tailAnchorWanted;
      ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->Activate(tailAnchorWanted);
      ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->Enable(tailAnchorWanted);
      if (tailAnchorWanted)
      {
         ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SetValue(tailAnchorOffset);
      }

      if (GColorBreathingTool->getRefFrame() >= 0)
      {
      	ZonalConstraintsRefFrameRadioButton->Checked = true;
      	ZonalConstraintsAnchorsPanel->Visible = false;
      	ZonalConstraintsRefFramePanel->Visible = true;
      }
      else if (headAnchorWanted || tailAnchorWanted)
      {
       	ZonalConstraintsAnchorsRadioButton->Checked = true;
      	ZonalConstraintsAnchorsPanel->Visible = true;
      	ZonalConstraintsRefFramePanel->Visible = false;
      }
      else
      {
       	ZonalConstraintsNoneRadioButton->Checked = true;
      	ZonalConstraintsAnchorsPanel->Visible = false;
      	ZonalConstraintsRefFramePanel->Visible = false;
      }

      ZonalConstraintsAnchorsPanel->Visible = ZonalConstraintsAnchorsRadioButton->Checked;
      ZonalConstraintsRefFramePanel->Visible = ZonalConstraintsRefFrameRadioButton->Checked;

      // BOXES
      BoxesTrackEdit->Enable(isRoiLegal && globalEnable);

      if (model->needToComputeTileBoxes())
      {
         GColorBreathingTool->makeTileBoxes();
      }

      ZonalFlickerShowBoxesCheckBox->Checked = GColorBreathingTool->getZonalFlickerShowBoxes();
      ZonalFlickerShowBoxesCheckBox->Enabled = !HideAllOverlaysCheckBox->Checked;
      DisabledShowBoxesPanel->Visible = HideAllOverlaysCheckBox->Checked;
      ZonalFlickerShowBoxesHighlightPanel->Color = ZonalFlickerShowBoxesCheckBox->Checked
                                                      ? _highlightPanelOffColor
                                                      : _highlightPanelNoShowColor;
      BoxesTrackEdit->SetValue(currentZonalParameters.getHorizontalBlocks());

      SmoothingTestComboBox->ItemIndex = GColorBreathingTool->getZonalFlickerSmoothingType();
      // SMOOTHING
      SmoothingTrackEditFrame->Visible = !ZonalConstraintsRefFrameRadioButton->Checked;
      SmoothingTrackEditFrame->Enable(SmoothingTrackEditFrame->Visible && isRoiLegal && globalEnable);
      SmoothingTrackEditFrame->SetValue(currentZonalParameters.getSmoothingSigma());

      // PROCESS TYPE
      // These are radio buttons, so set the down
      switch (GColorBreathingTool->getZonalProcessType())
      {
      case ZonalProcessType::ColorLinear:
         ZonalFlickerColorTypeButton->Down = true;
         break;

      case ZonalProcessType::Intensity:
         ZonalFlickerIntensityTypeButton->Down = true;
         break;

      case ZonalProcessType::Unknown:
         // RoiWarningColorLabel->Visible = true;
         // RoiWarningColorLabel->Caption = "Choose Intensity or Color Type";
         ZonalFlickerIntensityTypeButton->AllowAllUp = true;
         ZonalFlickerIntensityTypeButton->Down = false;
         ZonalFlickerColorTypeButton->Down = false;
         ZonalFlickerIntensityTypeButton->AllowAllUp = false;
         break;

      default:
         break;
      }

      int currentFrameIndex = getCurrentFrameIndex();
      bool analysisValid = false;
      bool rendered = false;
      bool isSmoothingValidOverMarks = false;
      bool didAnchorsChangeSinceSmoothing = _oldSmoothingType != GColorBreathingTool->getZonalFlickerSmoothingType();
      bool isPreprocessingValidForFrame = false;

      // May want to only do this when stuff changes because it's expensive! QQQ
      GColorBreathingTool->getClipPreprocessData().computeIsDefaultPreprocessingDone();

      // See if current frame is inside marks

      analysisValid = GColorBreathingTool->getClipPreprocessData().isDeltaValidOverMarks();
      rendered = GColorBreathingTool->getClipPreprocessData().areMarksRendered();
      isSmoothingValidOverMarks = GColorBreathingTool->getClipPreprocessData().isSmoothingValidOverMarks();
      isPreprocessingValidForFrame = GColorBreathingTool->getClipPreprocessData().isPreprocessingValidForFrame(currentFrameIndex);

      PleaseChooseBWorColorLabel->Visible = ZonalFlickerIntensityTypeButton->Down == false
                                             && ZonalFlickerColorTypeButton->Down == false;

      if (rendered)
      {
         ZonalAnalyzeButton->Enabled = true;
      }
      else
      {
         ZonalAnalyzeButton->Enabled = (!analysisValid && globalEnable);
         ZonalAnalyzeButton->Caption = "Analyze";
         if (!analysisValid && globalEnable)
         {
           GColorBreathingTool->SetInZonalFlickerPreviewMode(false);
           ZonalDisplayRadioGroup->Enabled = false;
         }
         else
         {
         	ZonalDisplayRadioGroup->Enabled = isPreprocessingValidForFrame && globalEnable && !rendered;
         }
      }



      ZonalResetPreprocessingButton->Enabled = GColorBreathingTool->getClipPreprocessData().getZonalDatabase().isConnected()
            && !ZonalAnalyzeButton->Enabled && globalEnable;
      ZonalFlickerRoiProcessingCheckBox->Enabled = globalEnable;
      ZonalFlickerRoiLockCheckBox->Enabled = globalEnable;
      ZonalSmoothingButton->Enabled = (!isSmoothingValidOverMarks || didAnchorsChangeSinceSmoothing) && analysisValid && globalEnable;

      auto inColorMode = currentZonalParameters.getZonalProcessType() == ZonalProcessType::ColorLinear;

      ZonalProcessChannelsGroupBox->Visible = inColorMode;
      AllZonalProcessSpeedButton->Enabled = inColorMode;
      RedZonalProcessingSpeedButton->Enabled = inColorMode;
      GreenZonalProcessingSpeedButton->Enabled = inColorMode;
      BlueZonalProcessingSpeedButton->Enabled = inColorMode;

      RedZonalProcessingSpeedButton->Down = model->isProcessingChannelEnabled(ChannelName::Red);
      GreenZonalProcessingSpeedButton->Down = model->isProcessingChannelEnabled(ChannelName::Green);
      BlueZonalProcessingSpeedButton->Down = model->isProcessingChannelEnabled(ChannelName::Blue);

      // TEMP EXPERIMENTAL
      fastCheckBox->Checked = model->getDownsample() == 8;

      auto analRoi = model->getAnalysisRoi();
      RECT newRoiRect =
      {analRoi.x, analRoi.y, analRoi.x + analRoi.width, analRoi.y + analRoi.height};
      RECT oldRoiRect;
      GColorBreathingTool->getSystemAPI()->GetROIShape(oldRoiRect, nullptr, nullptr);
      if (newRoiRect.left != oldRoiRect.left || newRoiRect.right != oldRoiRect.right || newRoiRect.top != oldRoiRect.top ||
            newRoiRect.bottom != oldRoiRect.bottom)
      {
         ESelectedRegionShape roiShape = isRoiLegal ? SELECTED_REGION_SHAPE_RECT : SELECTED_REGION_SHAPE_NONE;
         auto inRoiMode = GColorBreathingTool->getSystemAPI()->IsMaskInRoiMode();
         GColorBreathingTool->getSystemAPI()->SetMaskAllowed(true, true, isRoiLegal);
         auto monoModeWasNotChanged = inRoiMode == isRoiLegal;
         GColorBreathingTool->getSystemAPI()->SetROIShape(roiShape, newRoiRect, nullptr, nullptr);

         if (monoModeWasNotChanged == false)
         {
            GColorBreathingTool->getSystemAPI()->SetMaskAllowed(true, true, !isRoiLegal);
         }
      }

      oldRoiRect = GColorBreathingTool->getZonalFlickerRoi();
      if (newRoiRect.left != oldRoiRect.left || newRoiRect.right != oldRoiRect.right || newRoiRect.top != oldRoiRect.top ||
            newRoiRect.bottom != oldRoiRect.bottom)
      {
         GColorBreathingTool->setZonalFlickerRoi(newRoiRect);
      }

      UpdateZonalPresetCurrentValues();
   }
   catch (const std::exception &ex)
   {
      // There should be no errors, this was for debugging
      TRACE_0(errout << string(ex.what()));
   }
   catch (...)
   {
      TRACE_0(errout << "unknown error");
   }

   _updatingZonalFlickerGui = false;
   GColorBreathingTool->getSystemAPI()->refreshFrameCached();
}

void __fastcall TColorBreathingForm::ZonalFlickerSliderChanged(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   auto &currentZonalParameters = GColorBreathingTool->getZonalFlickerParameters();

   currentZonalParameters.setHorizontalBlocks(BoxesTrackEdit->GetValue());
   GColorBreathingTool->SetInPreviewMode(false);
   GColorBreathingTool->makeTileBoxes();

   currentZonalParameters.setSmoothingSigma(SmoothingTrackEditFrame->GetValue());

   updateZonalFlickerGui();
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::ZonalFlickerDisplayRadioGroupClick(TObject *Sender)
{
   // For now this is separate from the CB display radio group, we should make them the same
   GColorBreathingTool->SetInPreviewMode(ZonalDisplayRadioGroup->ItemIndex == 1);
}

////////////////////////////// START OF BOUINDING BOX

class BoolString
{
public:
   bool State;
   String Name;
};

typedef vector<BoolString>CStateMemory;

void DisableAllControls(TControl *panel, CStateMemory &StateMemory)
{
   TWinControl *twc;
   BoolString bs;

   // If already disabled, do  nothing
   if (!panel->Enabled)
   {
      return;
   }

   // Disable the component
   bs.Name = panel->Name;
   bs.State = panel->Enabled;
   StateMemory.push_back(bs);
   panel->Enabled = false;

   // Ignore some components
   if (panel->InheritsFrom(__classid(TRadioGroup)))
   {
      return;
   }

   // Special case: TrackEdits need to have a method called
   TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame*>(panel);
   if (trackEdit != NULL)
   {
      trackEdit->Enable(false);
      return; // Don't descend into the control - it's all set
   }

   // Window panels can have subcontrols
   if (!panel->InheritsFrom(__classid(TWinControl)))
   {
      return;
   }
   twc = dynamic_cast<TWinControl*>(panel);

   // Descend into the component
   for (int i = 0; i < twc->ControlCount; i++)
   {
      DisableAllControls(twc->Controls[i], StateMemory);
   }
}

void EnableAllControls(TControl *panel, CStateMemory &StateMemory)
{
   // Enable the controls
   for (unsigned j = 0; j < StateMemory.size(); j++)
   {
      if (panel->Name == StateMemory[j].Name)
      {
         panel->Enabled = StateMemory[j].State;
         StateMemory.erase(StateMemory.begin() + j);
         break;
      }
   }
   // Ignore some components
   if (panel->InheritsFrom(__classid(TRadioGroup)))
   {
      return;
   }

   // Special case: TrackEdits need to have a method called
   TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame*>(panel);
   if (trackEdit != NULL)
   {
      trackEdit->Enable(false);
      return; // Don't descend into the control - it's all set
   }

   if (!panel->InheritsFrom(__classid(TWinControl)))
   {
      return;
   }
   TWinControl *twc = dynamic_cast<TWinControl*>(panel);

   // Descend into the component
   for (int i = 0; i < twc->ControlCount; i++)
   {
      EnableAllControls(twc->Controls[i], StateMemory);
   }
}

void __fastcall TColorBreathingForm::UseMattingCBoxClick(TObject *Sender)
{
   // Disable/enable the matting box
   static CStateMemory StateMemory;
   if (UseMattingCBox->Checked)
   {
      EnableAllControls(ProcessingRegionGroupBox, StateMemory);
   }
   else
   {
      DisableAllControls(ProcessingRegionGroupBox, StateMemory);
      ProcessingRegionGroupBox->Enabled = true;
      UseMattingCBox->Enabled = true;
   }

   // Set the parameter
   ShowMattingBoxCBox->Enabled = UseMattingCBox->Checked;
   GColorBreathingTool->setUseColorBreathingRoi(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
   ////TTT UpdatePresetCurrentParameters();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::AllMatButtonClick(TObject *Sender)
{
   // const CImageFormat* imgFmt = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   // RECT bb;
   // bb.left = 0;
   // bb.top = 0;
   // bb.right = imgFmt->getPixelsPerLine() - 1;
   // bb.bottom = imgFmt->getLinesPerFrame() - 1;
   // GColorBreathingTool->setZonalFlickerRoi(bb);
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ShowMattingBoxCBoxClick(TObject *Sender)
{
   GColorBreathingTool->setIsGlobalFlickerRoiDisplayed(ShowMattingBoxCBox->Checked);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::SetMatButtonClick(TObject *Sender)
{
   GColorBreathingTool->FindBoundingBox();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ColorPickButtonClick(TObject *Sender)
{
   if (GColorBreathingTool == NULL)
   {
      return;
   }

   if (GColorBreathingTool->IsInColorPickMode())
   {
      GColorBreathingTool->ExitColorPickMode();
      ColorPickButton->Down = false;
      ColorPickerStateTimer->Enabled = false;
   }
   else
   {
      GColorBreathingTool->EnterColorPickMode();
      ColorPickButton->Down = true;
      ColorPickerStateTimer->Enabled = true;
   }
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::TopUpButtonClick(TObject *Sender)
{
   // // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.top = max<long>(0, bb.top - m);
   // GColorBreathingTool->setBoundingBox(bb);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::TopDownButtonClick(TObject *Sender)
{
   // // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // const CImageFormat* imgFmt = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.top = min<long>(imgFmt->getLinesPerFrame() - 1, bb.top + m);
   // GColorBreathingTool->setBoundingBox(bb);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::BottomDownButtonClick(TObject *Sender)
{
   // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // const CImageFormat* imgFmt = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.bottom = min<long>(imgFmt->getLinesPerFrame() - 1, bb.bottom + m);
   // GColorBreathingTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::BottomUpButtonClick(TObject *Sender)
{
   // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.bottom = max<long>(0, bb.bottom - m);
   // GColorBreathingTool->setBoundingBox(bb);
   //
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::LeftLeftButtonClick(TObject *Sender)
{
   // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.left = max<long>(0, bb.left - m);
   // GColorBreathingTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::RightLeftButtonClick(TObject *Sender)
{
   // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.right = max<long>(0, bb.right - m);
   // GColorBreathingTool->setBoundingBox(bb);
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::RightRightButtonClick(TObject *Sender)
{
   // // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // const CImageFormat* imgFmt = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.right = min<long>(imgFmt->getPixelsPerLine() - 1, bb.right + m);
   // GColorBreathingTool->setBoundingBox(bb);
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::LeftRightButtonClick(TObject *Sender)
{
   // // This avoids the button up causing a move
   // if (GlobalSender == nullptr)
   // return;
   //
   // int m = 1;
   // if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
   // m = 10 * m;
   //
   // const CImageFormat* imgFmt = GColorBreathingTool->getSystemAPI()->getVideoClipImageFormat();
   // RECT bb = GColorBreathingTool->getBoundingBox();
   // bb.left = min<long>(imgFmt->getPixelsPerLine() - 1, bb.left + m);
   // GColorBreathingTool->setBoundingBox(bb);

}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::AutoMoveButtonMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
   GlobalSender = Sender;
   TSpeedButton *sb = dynamic_cast<TSpeedButton*>(GlobalSender);
   sb->OnClick(sb);

   DownTimer->Interval = 250;
   DownTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::AutoMoveButtonMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
   GlobalSender = nullptr;
   DownTimer->Enabled = false;
}

void __fastcall TColorBreathingForm::DownTimerTimer(TObject *Sender)
{
   if (GlobalSender == nullptr)
   {
      DownTimer->Enabled = false;
      return;
   }

   TSpeedButton *sb = dynamic_cast<TSpeedButton*>(GlobalSender);
   sb->OnClick(sb);
   DownTimer->Interval = 75;
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::ZonalFlickerShowBoxesCheckBoxClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   auto model = GColorBreathingTool->getZonalFlickerModel();
   if ((model->isAnalysisRoiValid() == false)
   && ZonalFlickerShowBoxesCheckBox->Checked
   && GColorBreathingTool->isToolActive())
   {
      ZonalFlickerShowBoxesCheckBox->Checked = false;
      _MTIErrorDialog(Handle, "ROI is not valid!\nPlease define one or reset the ROI");
      return;
   }

   GColorBreathingTool->setZonalFlickerShowBoxes(ZonalFlickerShowBoxesCheckBox->Checked);
   updateZonalFlickerGui();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ColorPickerStateTimerTimer(TObject *Sender)
{
   if ((GColorBreathingTool == nullptr) || !GColorBreathingTool->IsInColorPickMode())
   {
      ColorPickButton->Down = false;
      ColorPickerStateTimer->Enabled = false;
   }
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::ReadAnalysisButtonClick(TObject *Sender)
{
   try
   {
      GColorBreathingTool->rereadZonalFlickerData();
   }
   catch (const std::exception &ex)
   {
      _MTIErrorDialog(Handle, ex.what());
   }
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalFlickerColorTypeButtonClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   GColorBreathingTool->setZonalProcessType(ZonalProcessType::ColorLinear);
   RefreshZonalFlickerAndExecButtonGuis();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalFlickerIntensityTypeButtonClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   GColorBreathingTool->setZonalProcessType(ZonalProcessType::Intensity);
   RefreshZonalFlickerAndExecButtonGuis();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalFlickerRoiLockCheckBoxClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   GColorBreathingTool->SetIsRoiLocked(ZonalFlickerRoiLockCheckBox->Checked);
   updateZonalFlickerGui();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalFlickerRoiShowCheckBoxClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   GColorBreathingTool->SetIsRoiDisplayed(ZonalFlickerRoiShowCheckBox->Checked);
   updateZonalFlickerGui();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalFlickerRoiProcessingCheckBoxClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   GColorBreathingTool->setUseProcessingRegion(ZonalFlickerRoiProcessingCheckBox->Checked);
   RefreshZonalFlickerAndExecButtonGuis();
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::ExecButtonsToolbarRenderAllButtonClick(TObject *Sender)
{
   ZonalDisplayRadioGroup->ItemIndex = 0;
   ExecButtonsToolbar->ExecButtonClick(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalSmoothingButtonClick(TObject *Sender)
{
   if (GColorBreathingTool->getClipPreprocessData().isDeltaValidOverMarks() == false)
   {
      //DBCOMMENT("------Delta Invalid---------");
      return;
   }

   GColorBreathingTool->resmoothZonalFlickerData();
   _oldSmoothingType = GColorBreathingTool->getZonalFlickerSmoothingType();
   ZonalSmoothingButton->Enabled = false;
   GColorBreathingTool->getSystemAPI()->refreshFrameCached();
}
// ---------------------------------------------------------------------------

class AutoInhibitClickCallback
{
public:
   AutoInhibitClickCallback(TObject *control)
   {
      // We might want to add other types later
      // E.g. use if (String(ClassRef->ClassName()) == "TCheckBox")
      //
      _control = dynamic_cast<TCheckBox*>(control);
      if (_control == nullptr)
      {
         return;
      }

      _notifyEvent = _control->OnClick;
      _control->OnClick = nullptr;
   }

   ~AutoInhibitClickCallback()
   {
      _control->OnClick = _notifyEvent;
   }

private:
   TCheckBox *_control = nullptr;
   TNotifyEvent _notifyEvent = nullptr;
};

void __fastcall TColorBreathingForm::GlobalFlickerRoiAnalysisCheckBoxClick(TObject *Sender)
{
   AutoInhibitClickCallback inhibitCallback(Sender);

   auto model = GColorBreathingTool->getGlobalFlickerModel();
   auto markIn = GColorBreathingTool->getSystemAPI()->getMarkIn();
   auto markOut = GColorBreathingTool->getSystemAPI()->getMarkOut();
   auto graphValid = model->RGBFrameData.IsGraphValid(markIn, markOut);
   if (graphValid)
   {
      if (_MTIConfirmationDialog(Handle,
            "Changing analysis ROI will delete the graph for the marked range\nYes to delete graph, No to cancel") != MTI_DLG_OK)
      {
         GlobalGuiDataRefresh();
         return;
      }
   }

   GColorBreathingTool->setUseAnalysisRegion(GlobalFlickerRoiAnalysisCheckBox->Checked);
}


// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::ResetZonalRoiButtonClick(TObject *Sender)
{
   GColorBreathingTool->resetZonalRoi();
   RefreshZonalFlickerAndExecButtonGuis();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::AllZonalProcessSpeedButtonClick(TObject *Sender)
{
   // graphinfo should be setters and dirtyness set below
   auto model = GColorBreathingTool->getZonalFlickerModel();
   model->enableProcessingChannel(ChannelName::Red);
   model->enableProcessingChannel(ChannelName::Green);
   model->enableProcessingChannel(ChannelName::Blue);

   if (GColorBreathingTool->GetInZonalFlickerPreviewMode())
   {
         GColorBreathingTool->getSystemAPI()->refreshFrameCached(); // goToFrameSynchronous(getCurrentFrameIndex());
   }

   RefreshZonalFlickerAndExecButtonGuis();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalAnalysisSpeedButtonClick(TObject *Sender)
{
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   auto button = dynamic_cast<TSpeedButton*>(Sender);
   if (button == nullptr)
   {
      return;
   }

   auto channelName = static_cast<ChannelName>(button->Tag);
   auto model = GColorBreathingTool->getZonalFlickerModel();
   if (button->Down)
   {
      model->enableProcessingChannel(channelName);
   }
   else
   {
      model->disableProcessingChannel(channelName);
   }

   if (GColorBreathingTool->GetInZonalFlickerPreviewMode())
   {
      GColorBreathingTool->getSystemAPI()->refreshFrameCached(); // goToFrameSynchronous(getCurrentFrameIndex());
   }

   RefreshZonalFlickerAndExecButtonGuis();
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::fastCheckBoxClick(TObject *Sender)
{
   auto model = GColorBreathingTool->getZonalFlickerModel();
   if (fastCheckBox->Checked)
   {
      model->setDownsample(8);
   }
   else
   {
      model->setDownsample(4);
   }

   GColorBreathingTool->getClipPreprocessData().computeIsDefaultPreprocessingDone();
   GuiDataRefresh();
}

// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalResetPreprocessingButtonClick(TObject *Sender)

{
   auto markIn = GColorBreathingTool->getSystemAPI()->getMarkIn();
   auto markOut = GColorBreathingTool->getSystemAPI()->getMarkOut();
   auto invalidMarks = (markIn < 0) || (markOut < 0) || (markOut <= markIn);
   string prompt = invalidMarks ? "Erase all preprocess data?\nYes to erase" : "Erase preprocess data between marks?\nYes to erase";

   if (_MTIConfirmationDialog(Handle, prompt) != MTI_DLG_OK)
   {
      GColorBreathingTool->getSystemAPI()->setMainWindowFocus();
      return;
   }

   try
   {
      GColorBreathingTool->getClipPreprocessData().clearPreprocessorDataBetweenMarks(markIn, markOut);
      GColorBreathingTool->SetInPreviewMode(false);
      RefreshZonalFlickerAndExecButtonGuis();
   }
   catch (const std::exception &ex)
   {
      _MTIErrorDialog(Handle, ex.what());
   }

   GColorBreathingTool->getSystemAPI()->setMainWindowFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::RoiToMaskButtonClick(TObject *Sender)
{
   GColorBreathingTool->roiToMask();
}
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void TColorBreathingForm::UpdateZonalPresetCurrentValues(string newMaskAsString)
{
   if (GColorBreathingTool == NULL)
   {
      return;
   }

   // This is a HACK! if the user hasn't chosen between B&W or Color, DO NOT
   // update the current values so the preset "Save" button never gets enabled!!
   // WHAT COULD POSSIBLY GO WRONG!!! hahaha
   if (GColorBreathingTool->getZonalProcessType() == ZonalProcessType::Unknown)
   {
      return;
   }

   ZonalPresetParameters presets;

   presets.processType = ZonalFlickerColorTypeButton->Down
                           ? ZonalProcessType::ColorLinear
                           : (ZonalFlickerIntensityTypeButton->Down
                              ? ZonalProcessType::Intensity
                              : ZonalProcessType::Unknown);
   presets.processRed = RedZonalProcessingSpeedButton->Down;
   presets.processGreen = GreenZonalProcessingSpeedButton->Down;
   presets.processBlue = BlueZonalProcessingSpeedButton->Down;

   presets.processRoi = ZonalFlickerRoiProcessingCheckBox->Checked;
   presets.lockRoi = ZonalFlickerRoiLockCheckBox->Checked;
   presets.showRoi = ZonalFlickerRoiShowCheckBox->Checked;

   RECT nullRECT = {0, 0, -1, -1};
   presets.roi = GColorBreathingTool
                  ? GColorBreathingTool->getZonalFlickerRoi()
                  : nullRECT;

   presets.boxesPerRow = BoxesTrackEdit->GetValue();
   presets.showBoxes = ZonalFlickerShowBoxesCheckBox->Checked;

   presets.temporalSmoothing = SmoothingTrackEditFrame->GetValue();

   presets.hideAllOverlays = HideAllOverlaysCheckBox->Checked;
   presets.showMask = GColorBreathingTool->getSystemAPI()->IsMaskVisible();

   // Note: if spin edits are inactive, they return -1 as value, and that's
   // exactly what we want!
   presets.headAnchorOffset = ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->GetValue();
   presets.tailAnchorOffset = ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->GetValue();
   presets.refFrameClipIndex = -1;
   if (ZonalConstraintsRefFrameRadioButton->Checked)
   {
      auto videoFrameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
      presets.refFrameClipIndex = videoFrameList
                                 ? ZonalConstraintsRefFrameTimecodeEdit->tcP - videoFrameList->getInTimecode()
                                 : -1;
   }

   // Turn off ROI mode before getting mask!
   GColorBreathingTool->getSystemAPI()->SetMaskAllowed(true, true, false);
   presets.encodedMask = newMaskAsString.empty()
                           ? GColorBreathingTool->getSystemAPI()->GetMaskAsString()
                           : newMaskAsString;

   char dirBuf[MAX_PATH];
   GetTempPath(MAX_PATH, dirBuf);
   presets.maskFilename = AddDirSeparator(dirBuf) + "ZonalFlickerCurrentMask.pdl";
   presets.writeMask(presets.maskFilename, presets.encodedMask);

   _zonalPresets.SetCurrent(presets);

#ifdef ENABLE_PRESETS_TRACE
   DBCOMMENT("");
   DBCOMMENT("UPDATE PRESET CURRENT VALUES:");
   presets.dump();
#endif
}
// ---------------------------------------------------------------------------

void SelectZonalPreset(int n)
{
	if (ColorBreathingForm == nullptr)
	{
		return;
	}

	ColorBreathingForm->selectZonalPreset(n);
}

void TColorBreathingForm::selectZonalPreset(int n)
{
	_zonalPresets.SetSelectedIndexAndCopyParametersToCurrent(n);
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::UpdateZonalGuiFromCurrentPreset()
{
   auto presets = dynamic_cast<ZonalPresetParameters*>(_zonalPresets.GetSelectedPreset());

   // NOTE: if the preset somehow got written with process type unknown, we
   // pretend it was set to color.
   if (presets->processType == ZonalProcessType::Intensity)
   {
      ZonalFlickerIntensityTypeButton->Down = true;
      ZonalFlickerIntensityTypeButtonClick(ZonalFlickerIntensityTypeButton);
   }
   else
   {
      ZonalFlickerColorTypeButton->Down = true;
      ZonalFlickerColorTypeButtonClick(ZonalFlickerColorTypeButton);
   }

   RedZonalProcessingSpeedButton->Down = presets->processRed;
   ZonalAnalysisSpeedButtonClick(RedZonalProcessingSpeedButton);
   GreenZonalProcessingSpeedButton->Down = presets->processGreen;
   ZonalAnalysisSpeedButtonClick(GreenZonalProcessingSpeedButton);
   BlueZonalProcessingSpeedButton->Down = presets->processBlue;
   ZonalAnalysisSpeedButtonClick(BlueZonalProcessingSpeedButton);

   ZonalFlickerRoiProcessingCheckBox->Checked = presets->processRoi;
   ZonalFlickerRoiLockCheckBox->Checked = presets->lockRoi;
   ZonalFlickerRoiShowCheckBox->Checked = presets->showRoi;
   if (GColorBreathingTool)
   {
      if (presets->roi.left >= 0
      && presets->roi.left < presets->roi.right
      && presets->roi.top >= 0
      && presets->roi.top < presets->roi.bottom)
      {
         GColorBreathingTool->setZonalFlickerRoi(presets->roi);
      }
      else
      {
         GColorBreathingTool->resetZonalRoi();
      }
   }

   // Do ShowBozes BEFORE BoxesTrackEdit!
   ZonalFlickerShowBoxesCheckBox->Checked = presets->showBoxes;

   // Stupid TrackEdit DOES NOT NOTIFY if we set the value, INCONSISTENT with
   // pretty much EVERY OTHER control.
   BoxesTrackEdit->SetValue(presets->boxesPerRow);
   SmoothingTrackEditFrame->SetValue(presets->temporalSmoothing);
   ZonalFlickerSliderChanged(nullptr);  // Uggh

   HideAllOverlaysCheckBox->Checked = presets->hideAllOverlays;

   ZonalConstraintsAnchorsFirstFrameButton->Down = presets->headAnchorOffset >= 0;
   ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->SetValue(presets->headAnchorOffset);
   ZonalConstraintsAnchorsLastFrameButton->Down = presets->tailAnchorOffset >= 0;
   ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->SetValue(presets->tailAnchorOffset);
   if (presets->headAnchorOffset >= 0 || presets->tailAnchorOffset >= 0)
   {
      ZonalConstraintsAnchorsRadioButton->Checked = true;
   }
   else if (presets->refFrameClipIndex >= 0)
   {
      ZonalConstraintsRefFrameRadioButton->Checked = true;
   }
   else
   {
      ZonalConstraintsNoneRadioButton->Checked = true;
   }

   GColorBreathingTool->getSystemAPI()->LockROI(true);

   // For some stupid reason LOAD MASK LAST!
   GColorBreathingTool->getSystemAPI()->SetMaskAllowed(true, true, false);

   if (presets->maskFilename.empty() || !ZonalPresetParameters::isMaskFileOk(presets->maskFilename))
   {
      presets->encodedMask = "";
      GColorBreathingTool->getSystemAPI()->ClearMask();
   }
   else
   {
      presets->encodedMask = ZonalPresetParameters::readMask(presets->maskFilename);
      GColorBreathingTool->getSystemAPI()->LoadMaskFromFile(presets->maskFilename);
      GColorBreathingTool->getSystemAPI()->EnableMaskOutlineDisplay(presets->showMask);
   }

   GColorBreathingTool->getSystemAPI()->LockROI(presets->lockRoi);


#ifdef ENABLE_PRESETS_TRACE
   DBCOMMENT("");
   DBCOMMENT("UPDATE ZONAL GUI FROM CURRENT PRESET:");
   presets->dump();
   DBCOMMENT("");
   DBCOMMENT("RESULT:");
   DBTRACE(ZonalFlickerColorTypeButton->Down);
   DBTRACE(ZonalFlickerIntensityTypeButton->Down);
   DBTRACE(RedZonalProcessingSpeedButton->Down);
   DBTRACE(GreenZonalProcessingSpeedButton->Down);
   DBTRACE(BlueZonalProcessingSpeedButton->Down);
   DBTRACE(ZonalFlickerRoiProcessingCheckBox->Checked);
   DBTRACE(ZonalFlickerRoiLockCheckBox->Checked);
   DBTRACE(ZonalFlickerRoiShowCheckBox->Checked);
   RECT nullRECT = {0, 0, -1, -1};
   auto roi = GColorBreathingTool ? GColorBreathingTool->getZonalFlickerRoi() : nullRECT;
   TRACE_0(errout << "roi = {" << roi.left << ", " << roi.top << ", " << roi.right << ", " << roi.bottom << "}");
   DBTRACE(BoxesTrackEdit->GetValue());
   DBTRACE(ZonalFlickerShowBoxesCheckBox->Checked);
   DBTRACE(SmoothingTrackEditFrame->GetValue());
   DBTRACE(HideAllOverlaysCheckBox->Checked);
   DBTRACE(ZonalConstraintsNoneRadioButton->Checked);
   DBTRACE(ZonalConstraintsAnchorsFirstFrameButton->Down);
   DBTRACE(ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->GetValue());
   DBTRACE(ZonalConstraintsAnchorsLastFrameButton->Down);
   DBTRACE(ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->GetValue());
   DBTRACE(ZonalConstraintsRefFrameTimecodeEdit->tcP.String());
   auto currentMaskString = GColorBreathingTool->getSystemAPI()->GetMaskAsString();
   DBTRACE((currentMaskString == presets->encodedMask));
   if (currentMaskString != presets->encodedMask)
   {
      DBTRACE(currentMaskString);
   }
#endif
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::ZonalPresetParameters::ReadParameters(CIniFile *iniFile, const string &iniSectionName)
{
   ZonalPresetParameters defaults;

   processType = static_cast<ZonalProcessType>(
                     iniFile->ReadAssociation(ZonalProcessAssociation, iniSectionName, "processType", int(defaults.processType))
                  );
   processRed = iniFile->ReadBool(iniSectionName, "processRed", defaults.processRed);
   processGreen = iniFile->ReadBool(iniSectionName, "processGreen", defaults.processGreen);
   processBlue = iniFile->ReadBool(iniSectionName, "processBlue", defaults.processBlue);

   processRoi = iniFile->ReadBool(iniSectionName, "processRoi", defaults.processRoi);
   lockRoi = iniFile->ReadBool(iniSectionName, "lockRoi", defaults.lockRoi);
   showRoi = iniFile->ReadBool(iniSectionName, "showRoi", defaults.showRoi);
   roi.left = iniFile->ReadInteger(iniSectionName, "roiLeft", defaults.roi.left);
   roi.top = iniFile->ReadInteger(iniSectionName, "roiTop", defaults.roi.top);
   roi.right = iniFile->ReadInteger(iniSectionName, "roiRight", defaults.roi.right);
   roi.bottom = iniFile->ReadInteger(iniSectionName, "roiBottom", defaults.roi.bottom);

   boxesPerRow = iniFile->ReadInteger(iniSectionName, "boxesPerRow", defaults.boxesPerRow);
   showBoxes = iniFile->ReadBool(iniSectionName, "showBoxes", defaults.showBoxes);
   temporalSmoothing = iniFile->ReadInteger(iniSectionName, "temporalSmoothing", defaults.temporalSmoothing);

   hideAllOverlays = iniFile->ReadBool(iniSectionName, "hideAllOverlays", defaults.hideAllOverlays);
   showMask = iniFile->ReadBool(iniSectionName, "showMask", defaults.showMask);

   headAnchorOffset = iniFile->ReadInteger(iniSectionName, "headAnchorOffset", defaults.headAnchorOffset);
   tailAnchorOffset = iniFile->ReadInteger(iniSectionName, "tailAnchorOffset", defaults.tailAnchorOffset);
   auto iniFileDir = GetFilePath(iniFile->FileName);
   maskFilename = AddDirSeparator(iniFileDir) + iniSectionName + "_Mask.pdl";
   encodedMask = readMask(maskFilename);

#ifdef ENABLE_PRESETS_TRACE
   DBCOMMENT("");
   DBCOMMENT("READ");
   DBTRACE2(iniFile->FileName, iniSectionName);
   dump();
#endif
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::ZonalPresetParameters::WriteParameters(CIniFile *iniFile, const string &iniSectionName) const
{
   iniFile->WriteAssociation(ZonalProcessAssociation, iniSectionName, "processType", int(processType));
   iniFile->WriteBool(iniSectionName, "processRed", processRed);
   iniFile->WriteBool(iniSectionName, "processGreen", processGreen);
   iniFile->WriteBool(iniSectionName, "processBlue", processBlue);

   iniFile->WriteInteger(iniSectionName, "roiLeft", roi.left);
   iniFile->WriteInteger(iniSectionName, "roiTop", roi.top);
   iniFile->WriteInteger(iniSectionName, "roiRight", roi.right);
   iniFile->WriteInteger(iniSectionName, "roiBottom", roi.bottom);
   iniFile->WriteBool(iniSectionName, "processRoi", processRoi);
   iniFile->WriteBool(iniSectionName, "lockRoi", lockRoi);
   iniFile->WriteBool(iniSectionName, "showRoi", showRoi);

   iniFile->WriteInteger(iniSectionName, "boxesPerRow", boxesPerRow);
   iniFile->WriteBool(iniSectionName, "showBoxes", showBoxes);

   iniFile->WriteInteger(iniSectionName, "temporalSmoothing", temporalSmoothing);

   iniFile->WriteBool(iniSectionName, "hideAllOverlays", hideAllOverlays);
   iniFile->WriteBool(iniSectionName, "showMask", showMask);

   iniFile->WriteInteger(iniSectionName, "headAnchorOffset", headAnchorOffset);
   iniFile->WriteInteger(iniSectionName, "tailAnchorOffset", tailAnchorOffset);

   // This isn't right but no time to fix!
   auto iniFileDir = GetFilePath(iniFile->FileName);
   auto nonConstThis = const_cast<ZonalPresetParameters *>(this);
//   nonConstThis->encodedMask = GColorBreathingTool->getSystemAPI()->GetMaskAsString();
   nonConstThis->maskFilename = AddDirSeparator(string(iniFileDir)) + iniSectionName + "_Mask.pdl";
   iniFile->WriteString(iniSectionName, "maskFilename", maskFilename);
   writeMask(maskFilename, encodedMask);

#ifdef ENABLE_PRESETS_TRACE
   DBCOMMENT("");
   DBTRACE("WRITE:");
   DBTRACE2(iniFile->FileName, iniSectionName);
   dump();
#endif
}
// ---------------------------------------------------------------------------

bool TColorBreathingForm::ZonalPresetParameters::AreParametersTheSame(const IPresetParameters &presetParameter) const
{
   auto rhs = static_cast< const ZonalPresetParameters*>(&presetParameter);
   return *this == *rhs;
}
// ---------------------------------------------------------------------------

bool TColorBreathingForm::ZonalPresetParameters:: operator == (const ZonalPresetParameters &rhs) const
{
#ifdef ENABLE_PRESETS_TRACE
   DBCOMMENT("OPERATOR ==");
   DBTRACE((processType == rhs.processType));
   DBTRACE((processRed == rhs.processRed));
   DBTRACE((processGreen == rhs.processGreen));
   DBTRACE((processBlue == rhs.processBlue));
   DBTRACE((processRoi == rhs.processRoi));
   DBTRACE((lockRoi == rhs.lockRoi));
   DBTRACE((showRoi == rhs.showRoi));
   DBTRACE((roi.left == rhs.roi.left));
   DBTRACE((roi.top == rhs.roi.top));
   DBTRACE((roi.right == rhs.roi.right));
   DBTRACE((roi.bottom == rhs.roi.bottom));
   DBTRACE((boxesPerRow == rhs.boxesPerRow));
   DBTRACE((showBoxes == rhs.showBoxes));
   DBTRACE((temporalSmoothing == rhs.temporalSmoothing));
   DBTRACE((hideAllOverlays == rhs.hideAllOverlays));
   DBTRACE((headAnchorOffset == rhs.headAnchorOffset));
   DBTRACE((tailAnchorOffset == rhs.tailAnchorOffset));
   DBTRACE((showMask == rhs.showMask));
   DBTRACE((encodedMask == rhs.encodedMask));
   DBCOMMENT("LHS:");
   dump();
   DBCOMMENT("RHS:");
   dump();
   DBCOMMENT("");
#endif

   return      processType == rhs.processType
            && processRed == rhs.processRed
            && processGreen == rhs.processGreen
            && processBlue == rhs.processBlue
            && processRoi == rhs.processRoi
            && lockRoi == rhs.lockRoi
            && showRoi == rhs.showRoi
            && roi.left == rhs.roi.left
            && roi.top == rhs.roi.top
            && roi.right == rhs.roi.right
            && roi.bottom == rhs.roi.bottom
            && boxesPerRow == rhs.boxesPerRow
            && showBoxes == rhs.showBoxes
            && temporalSmoothing == rhs.temporalSmoothing
            && hideAllOverlays == rhs.hideAllOverlays
            && showMask == rhs.showMask
            && headAnchorOffset == rhs.headAnchorOffset
            && tailAnchorOffset == rhs.tailAnchorOffset
            && refFrameClipIndex == rhs.refFrameClipIndex
            && encodedMask == rhs.encodedMask;
}
// ---------------------------------------------------------------------------

#include <fstream>
#include <sstream>
#include <streambuf>

/* static */
string TColorBreathingForm::ZonalPresetParameters::readMask(const string &maskFilename)
{
   std::ostringstream sstr;
   try
   {
      std::ifstream fstr(maskFilename);
      sstr << fstr.rdbuf();
   }
   catch(...)
   {
      TRACE_0(errout << "It seems that " << maskFilename << "does not exist!");
   }

   return sstr.str();
}
// ---------------------------------------------------------------------------

/* static */
void TColorBreathingForm::ZonalPresetParameters::writeMask(const string &maskFilename, const string &maskAsString)
{
   try
   {
      std::ofstream ofs;
      ofs.open(maskFilename, std::ofstream::out | std::ofstream::trunc);
      if (ofs.is_open())
      {
         ofs << maskAsString;
      }
   }
   catch(...)
   {
      TRACE_0(errout << "It seems that " << maskFilename << "cannot be written!");
   }
}
// ---------------------------------------------------------------------------

#include <sys/stat.h>
/* static */
bool TColorBreathingForm::ZonalPresetParameters::isMaskFileOk(const string &maskFilename)
{
   struct stat stat_buf;
   int rc = stat(maskFilename.c_str(), &stat_buf);
   return rc == 0 ? (stat_buf.st_size > 0) : false;
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::ZonalPresetParameters::dump() const
{
   DBTRACE((int)processType);
   DBTRACE(processRed);
   DBTRACE(processGreen);
   DBTRACE(processBlue);
   DBTRACE(processRoi);
   DBTRACE(lockRoi);
   DBTRACE(showRoi);
   TRACE_0(errout << "roi = {" << roi.left << ", " << roi.top << ", " << roi.right << ", " << roi.bottom << "}");
   DBTRACE(boxesPerRow);
   DBTRACE(showBoxes);
   DBTRACE(temporalSmoothing);
   DBTRACE(hideAllOverlays);
   DBTRACE(headAnchorOffset);
   DBTRACE(tailAnchorOffset);
   DBTRACE(refFrameClipIndex);
   DBTRACE(showMask);
   DBTRACE(encodedMask);
   DBTRACE(maskFilename);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::HideAllOverlaysCheckBoxClick(TObject *Sender)
{
   bool newRoiVisibility;
   bool newBoxesVisibility;

   if (HideAllOverlaysCheckBox->Checked)
   {
      _hideOverlaysPrevMaskVisibilityState = GColorBreathingTool->getSystemAPI()->IsMaskVisible();
      _hideOverlaysPrevRoiVisibilityState = GColorBreathingTool->GetIsRoiDisplayed();
      _hideOverlaysPrevBoxesVisibilityState =  GColorBreathingTool->getZonalFlickerShowBoxes();

      GColorBreathingTool->getSystemAPI()->setMaskVisibilityLockedOff(true);
      newRoiVisibility = false;
      newBoxesVisibility = false;
   }
   else
   {
      GColorBreathingTool->getSystemAPI()->setMaskVisibilityLockedOff(false);
      GColorBreathingTool->getSystemAPI()->EnableMaskOutlineDisplay(_hideOverlaysPrevMaskVisibilityState);
      newRoiVisibility = _hideOverlaysPrevRoiVisibilityState;
      newBoxesVisibility = _hideOverlaysPrevBoxesVisibilityState;
   }

   GColorBreathingTool->SetIsRoiDisplayed(newRoiVisibility);
   GColorBreathingTool->setZonalFlickerShowBoxes(newBoxesVisibility);

   updateZonalFlickerGui();
}
//---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsGuiChanged(TObject *Sender)
{
   // DO NOT RE-ENTER!
   if (_updatingZonalFlickerGui)
   {
      return;
   }

   auto anchorsChecked = ZonalConstraintsAnchorsRadioButton->Checked;
   ZonalConstraintsAnchorsPanel->Visible = anchorsChecked;

   ZonalConstraintsAnchorsFirstFrameButton->Enabled = anchorsChecked;
   ZonalConstraintsAnchorsFirstFrameOffsetLabel->Enabled = anchorsChecked;
   ZonalConstraintsAnchorsLastFrameButton->Enabled = anchorsChecked;
   ZonalConstraintsAnchorsLastFrameOffsetLabel->Enabled = anchorsChecked;

   bool wantHeadReferenceFrame = anchorsChecked && ZonalConstraintsAnchorsFirstFrameButton->Down;
   ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->Activate(wantHeadReferenceFrame);
   ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->Enable(wantHeadReferenceFrame);
   ZonalConstraintsAnchorsFirstFrameOffsetLabel->Enabled = wantHeadReferenceFrame;

   GColorBreathingTool->getZonalFlickerModel()->setUseHeadAnchor(wantHeadReferenceFrame);
//   auto currentHeadAnchorOffset = GColorBreathingTool->getAnchorFrameOffsets().first;
//   auto headAnchorOffset = wantHeadReferenceFrame
//                           ? ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->GetValue()
//                           : -1;

   bool wantTailReferenceFrame = anchorsChecked && ZonalConstraintsAnchorsLastFrameButton->Down;
   ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->Activate(wantTailReferenceFrame);
   ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->Enable(wantTailReferenceFrame);
   ZonalConstraintsAnchorsLastFrameOffsetLabel->Enabled = wantTailReferenceFrame;
//   auto tailAnchorOffset = wantTailReferenceFrame
//                           ? ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->GetValue()
//                           : -1;

   GColorBreathingTool->getZonalFlickerModel()->setUseTailAnchor(wantTailReferenceFrame);

   bool refFrameChecked = ZonalConstraintsRefFrameRadioButton->Checked;
   ZonalConstraintsRefFramePanel->Visible = refFrameChecked;
   SmoothingTrackEditFrame->Visible = !refFrameChecked;
   SmoothingTrackEditFrame->Enable(!refFrameChecked);
   ZonalSmoothingButton->Visible = SmoothingTrackEditFrame->Visible;

   auto refFrameClipIndex = -1;
   if (refFrameChecked)
   {
      auto videoFrameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
      if (videoFrameList)
      {
         refFrameClipIndex = ZonalConstraintsRefFrameTimecodeEdit->tcP - videoFrameList->getInTimecode();
      }
   }

   GColorBreathingTool->setRefFrame(refFrameClipIndex);

   // If we want anchors, set them
   if (wantHeadReferenceFrame)
   {
      GColorBreathingTool->getZonalFlickerModel()->setHeadAnchorOffset(ZonalConstraintsAnchorsFirstFrameOffsetSpinEdit->GetValue());
   }

   if (wantTailReferenceFrame)
   {
      GColorBreathingTool->getZonalFlickerModel()->setTailAnchorOffset(ZonalConstraintsAnchorsLastFrameOffsetSpinEdit->GetValue());
   }

   UpdateZonalPresetCurrentValues();
   updateZonalFlickerGui();

   // NEW - auto-resmoothing!
   GColorBreathingTool->resmoothZonalFlickerDataIfNecessary();
   ZonalSmoothingButtonClick(nullptr);
}
// ---------------------------------------------------------------------------

void TColorBreathingForm::RefFrameOffsetHasChanged(void *Sender)
{
   ZonalConstraintsGuiChanged(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ExecButtonsToolbarCapturePDLButtonClick(TObject *Sender)

{
   if (GColorBreathingTool->getSubToolType() == SubToolType::ZonalFlicker
   && GColorBreathingTool->getZonalProcessType() == ZonalProcessType::Unknown)
   {
      _MTIErrorDialog(Handle, "Please choose B&W or Color");
      return;
   }

   ExecButtonsToolbar->ExecButtonClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsSetRefFrameButtonClick(TObject *Sender)
{
	if (GColorBreathingTool == NULL)
		return;

	// This gets the current time code
	CVideoFrameList *frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;
	int currentFrameIndex = GColorBreathingTool->getSystemAPI()->getLastFrameIndex();
	PTimecode ptc(frameList->getTimecodeForFrameIndex(currentFrameIndex));
	if (ZonalConstraintsRefFrameTimecodeEdit->tcP != ptc)
	{
		ZonalConstraintsRefFrameTimecodeEdit->tcP = ptc;
		ZonalConstraintsRefFrameTimecodeEditExit(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsGoToRefframeButtonClick(TObject *Sender)
{
	if (GColorBreathingTool == NULL)
		return;

	ZonalConstraintsRefFrameTimecodeEdit->VExit(0);

	CVideoFrameList *frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;

	int nominalFrameIndex = frameList->getFrameIndex(ZonalConstraintsRefFrameTimecodeEdit->tcP);
	GColorBreathingTool->getSystemAPI()->goToFrameSynchronous(nominalFrameIndex);

	// wtf?? may go someplace else?
	int actualFrameIndex = GColorBreathingTool->getSystemAPI()->getLastFrameIndex();
	if (actualFrameIndex != nominalFrameIndex)
	{
		ZonalConstraintsRefFrameTimecodeEdit->tcP = frameList->getTimecodeForFrameIndex(actualFrameIndex);
		ZonalConstraintsRefFrameTimecodeEditExit(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsRefFrameTimecodeEditExit(TObject *Sender)
{
	if (ZonalConstraintsRefFrameRadioButton->Checked == false)
	{
      return;
	}

   ZonalConstraintsGuiChanged(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsRefFrameTimeCodeEditEnterOrEscapeKey(TObject *Sender)
{
	ZonalConstraintsRefFrameTimecodeEditExit(Sender);
	ZonalConstraintsGroupBox->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::ZonalConstraintsRadioButtonClick(TObject *Sender)
{
	// The first time a reference frame is set for a shot, set the boxes to 24, after that
   // don't change them
   if (ZonalConstraintsRefFrameRadioButton->Checked)
   {
   	// Parameters should already be loaded
      auto rf = GColorBreathingTool->getRefFrame();
//      if (rf == ZONAL_DB_UNINIT_VALUE)
      if (rf < 0)
      {
         GColorBreathingTool->getZonalFlickerModel()->setHorizontalBlocks(24);
         GColorBreathingTool->updateDatabaseSegmentAtIndex(getCurrentFrameIndex());
      }
   }

   if (ZonalConstraintsAnchorsRadioButton->Checked
   && ZonalConstraintsAnchorsFirstFrameButton->Down == false
   && ZonalConstraintsAnchorsLastFrameButton->Down == false)
   {
      auto useHead = _headAnchorWantedShadow;
      auto useTail = _tailAnchorWantedShadow;

      // In theory this should never happen
      if (!useHead && !useTail)
      {
         _headAnchorWantedShadow = true;
         _tailAnchorWantedShadow = true;
      }

      GColorBreathingTool->getZonalFlickerModel()->setUseHeadAnchor(_headAnchorWantedShadow);
      GColorBreathingTool->getZonalFlickerModel()->setUseTailAnchor(_tailAnchorWantedShadow);
      ZonalConstraintsAnchorsFirstFrameButton->Down = _headAnchorWantedShadow;
      ZonalConstraintsAnchorsLastFrameButton->Down = _tailAnchorWantedShadow;
   }

   if (GColorBreathingTool == NULL)
   {
      return;
   }

   if (ZonalConstraintsRefFrameRadioButton->Checked == false)
   {
	   ZonalConstraintsRefFrameTimecodeEdit->VExit(0);
   }

   ZonalConstraintsGuiChanged(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TColorBreathingForm::SmoothingTestComboBoxChange(TObject *Sender)
{
// TODO: push down where it belongs
    _oldSmoothingType = GColorBreathingTool->getZonalFlickerSmoothingType();
    GColorBreathingTool->setZonalFlickerSmoothingType(SmoothingTestComboBox->ItemIndex);
    DBTRACE2(_oldSmoothingType, GColorBreathingTool->getZonalFlickerSmoothingType());
    updateZonalFlickerGui();
}
//-----------------------------------------------------------------------


#include <windows.h>
#include <shobjidl.h>

int test()
{
	 CBinManager binManager;

	 auto frameList = GColorBreathingTool->getSystemAPI()->getVideoFrameList();
	 auto markIn = GColorBreathingTool->getSystemAPI()->getMarkIn();
	 auto markOut = GColorBreathingTool->getSystemAPI()->getMarkOut();
	 vector<int>listOfDirtyFrameIndexes;

	 int progressPercent;
	 int modifiedFrameCount;

    CHRTimer hrt;
	 auto i = binManager.FindDirtyFrames(frameList, markIn, markOut, listOfDirtyFrameIndexes, &progressPercent,
		  &modifiedFrameCount);

    DBTRACE(hrt);
    DBTRACE2(i, modifiedFrameCount);
    for (auto v : listOfDirtyFrameIndexes)
    {
      DBTRACE(v);
    }


   // THIS CODE does a better open file dialog
//	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
//		COINIT_DISABLE_OLE1DDE);
//	if (SUCCEEDED(hr))
//	{
//		IFileOpenDialog *pFileOpen;
//
//		// Create the FileOpenDialog object.
//		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
//			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));
//
//		if (SUCCEEDED(hr))
//		{
//			// Show the Open dialog box.
//			hr = pFileOpen->Show(NULL);
//
//			// Get the file name from the dialog box.
//			if (SUCCEEDED(hr))
//			{
//				IShellItem *pItem;
//				hr = pFileOpen->GetResult(&pItem);
//				if (SUCCEEDED(hr))
//				{
//					PWSTR pszFilePath;
//					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
//
//					// Display the file name to the user.
//					if (SUCCEEDED(hr))
//					{
//					//	MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
//						CoTaskMemFree(pszFilePath);
//					}
//					pItem->Release();
//				}
//			}
//			pFileOpen->Release();
//		}
//		CoUninitialize();
//	}
//	return 0;
   return 0;
}

// ---------------------------------------------------------------------------
void __fastcall TColorBreathingForm::DebugButtonClick(TObject *Sender)
{
   test();
}

void __fastcall TColorBreathingForm::ZonalConstraintsNoneRadioButtonClick(TObject *Sender)
{
   // If both are false, the previous was not on anchors, so we just switch
   if ((!GColorBreathingTool->getZonalFlickerModel()->getUseHeadAnchor()) &&
       (!GColorBreathingTool->getZonalFlickerModel()->getUseTailAnchor()))
   {
      ZonalConstraintsRadioButtonClick(Sender);
      return;
   }

   // push state of anchor buttons
   _headAnchorWantedShadow = GColorBreathingTool->getZonalFlickerModel()->getUseHeadAnchor();
   _tailAnchorWantedShadow = GColorBreathingTool->getZonalFlickerModel()->getUseTailAnchor();
   ZonalConstraintsRadioButtonClick(Sender);
}
//---------------------------------------------------------------------------
