// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ReferenceKeyframeListBox.h"
#include "ReferenceKeyframeFrame.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ReferenceKeyframeFrame"
#pragma resource "*.dfm"
TReferenceKeyframeListBoxFrame *ReferenceKeyframeListBoxFrame;

// ---------------------------------------------------------------------------
__fastcall TReferenceKeyframeListBoxFrame::TReferenceKeyframeListBoxFrame(TComponent* Owner) : TFrame(Owner)
{
}
// ---------------------------------------------------------------------------

void TReferenceKeyframeListBoxFrame::Clear()
{
    int n = this->DisplayFlowPanel->ControlCount;
    for (int i = n - 1; i >= 0; i--)
    {
	delete this->DisplayFlowPanel->Controls[i];
    }
}

const ReferenceKeyframeData TReferenceKeyframeListBoxFrame::GetKeyframeData(int index) const
{
    auto frame = (TReferenceKeyframeFrame*)(this->DisplayFlowPanel->Controls[index]);
    return frame->GetReferenceKeyFrameData();
}

ReferenceKeyframeData *TReferenceKeyframeListBoxFrame::GetKeyframePointer(int index) const
{
    auto frame = (TReferenceKeyframeFrame*)(this->DisplayFlowPanel->Controls[index]);
    return frame->GetReferenceKeyFramePointer();
}

void TReferenceKeyframeListBoxFrame::DeleteKeyframeData(int index)
{
    if ((index >= 0) && (index < this->DisplayFlowPanel->ControlCount))
    {
	auto referenceFrame = (TReferenceKeyframeFrame*)this->DisplayFlowPanel->Controls[index];
	REMOVE_CBHOOK(ReferenceDataHasChanged, referenceFrame->DataHasChanged);
	REMOVE_CBHOOK(FrameClick, referenceFrame->FrameClick);
	delete this->DisplayFlowPanel->Controls[index];
	ReferenceDataHasChanged(nullptr);
    }
}

void TReferenceKeyframeListBoxFrame::Add(ReferenceKeyframeData *referenceKeyframeData)
{
    auto newFrame = new TReferenceKeyframeFrame(this, referenceKeyframeData, _inTimecode);
    newFrame->DeleteButton->OnClick = DeleteSelf;
    newFrame->Parent = this->DisplayFlowPanel;
    SET_CBHOOK(ReferenceDataHasChanged, newFrame->DataHasChanged);
    SET_CBHOOK(FrameClick, newFrame->FrameClick);
    ReferenceDataHasChanged(newFrame);
}

void TReferenceKeyframeListBoxFrame::Insert(int index, ReferenceKeyframeData *referenceKeyframeData)
{
    auto newFrame = new TReferenceKeyframeFrame(this, referenceKeyframeData, _inTimecode);
    newFrame->Parent = this->DisplayFlowPanel;
    newFrame->DeleteButton->OnClick = DeleteSelf;
    this->DisplayFlowPanel->SetControlIndex(newFrame, index + 1);
    SET_CBHOOK(ReferenceDataHasChanged, newFrame->DataHasChanged);
    ReferenceDataHasChanged(newFrame);
}

const vector<ReferenceKeyframeData *>TReferenceKeyframeListBoxFrame::GetKeyframeDataList()
{
    auto result = vector<ReferenceKeyframeData*>();

    int n = this->DisplayFlowPanel->ControlCount;
    for (int i = n - 1; i >= 0; i--)
    {
	result.push_back(GetKeyframePointer(i));
    }

    return result;
}

void __fastcall TReferenceKeyframeListBoxFrame::DeleteSelf(TObject *Sender)
{
    OneShotDeleteTimer->Tag = ((TControl*)Sender)->Tag;
    auto n = OneShotDeleteTimer->Interval;
    auto e = OneShotDeleteTimer->Enabled;
    auto x = OneShotDeleteTimer->OnTimer;

    OneShotDeleteTimer->Enabled = true;
}

void __fastcall TReferenceKeyframeListBoxFrame::OneShotDeleteTimerTimer(TObject *Sender)
{
    OneShotDeleteTimer->Enabled = false;
    auto tag = ((TControl*)Sender)->Tag;
    auto frame = (TReferenceKeyframeFrame*)tag;
    if (OnDeleteReferenceKeyframe != nullptr)
    {
	OnDeleteReferenceKeyframe(frame->GetReferenceKeyFrameData());
    }

    delete frame;
}

bool Contains(const vector<ReferenceKeyframeData *> &referenceKeyframeDataList, ReferenceKeyframeData *rf)
{
    for (auto r : referenceKeyframeDataList)
    {
	if (r->FrameIndex == rf->FrameIndex)
	{
	    return true;
	}
    }

    return false;
}

void TReferenceKeyframeListBoxFrame::SetKeyframeDataList(const vector<ReferenceKeyframeData *>referenceKeyframeDataList)
{
    auto currentList = GetKeyframeDataList();
    if (currentList.size() == referenceKeyframeDataList.size())
    {
	bool noChange = true;
	for (auto rf : currentList)
	{
	    if (Contains(referenceKeyframeDataList, rf) == false)
	    {
		noChange = false;
		break;
	    }
	}

	if (noChange)
	{
	    return;
	}
    }

    Clear();
    for (auto r : referenceKeyframeDataList)
    {
	Add(r);
    }
}

void TReferenceKeyframeListBoxFrame::ReferenceDataHasChanged(void *Sender)
{
    if (OnChange != nullptr)
    {
	OnChange(this);
    }
}

void TReferenceKeyframeListBoxFrame::FrameClick(void *sender)
{
    if (OnFrameClick != nullptr)
    {
	OnFrameClick((ReferenceKeyframeData *)sender);
    }
}

void TReferenceKeyframeListBoxFrame::SetInTimecode(const CTimecode &inTimecode)
{
    _inTimecode = inTimecode;
}

// ---------------------------------------------------------------------------

void TReferenceKeyframeListBoxFrame::NoChangeAllowed(RGBFrameData &rgbFrameData)
{
    for (auto i = 0; i < DisplayFlowPanel->ControlCount; i++)
    {
	auto referenceKeyframeFrame = (TReferenceKeyframeFrame*)DisplayFlowPanel->Controls[i];
	auto frame = referenceKeyframeFrame->GetReferenceKeyFramePointer()->FrameIndex;
	referenceKeyframeFrame->SetMasterEnabled(!rgbFrameData[frame].Rendered);
    }
}

void TReferenceKeyframeListBoxFrame::SelectReferenceFrame(int frame)
{
    // Find the first reference frame to the left
    auto oldFrameIndex = -1;
    auto oldFrameSelected = false;
    for (int i = 0; i < DisplayFlowPanel->ControlCount; i++)
    {
	auto referenceKeyframeFrame = dynamic_cast<TReferenceKeyframeFrame*>(DisplayFlowPanel->Controls[i]);
	if (referenceKeyframeFrame == nullptr)
	{
	    continue;
	}

	auto frameNumberIndex = referenceKeyframeFrame->GetReferenceKeyFramePointer()->FrameIndex;
	if (frameNumberIndex > frame)
	{
	    // We have passed the point of no return
	    break;
	}

	oldFrameIndex = frameNumberIndex;
	oldFrameSelected = referenceKeyframeFrame->IsSelected();
    }

    // We are done if it was already selected
    if (oldFrameSelected)
    {
	return;
    }

    // Now turn off all frames but this one
    for (int i = 0; i < DisplayFlowPanel->ControlCount; i++)
    {
	auto referenceKeyframeFrame = dynamic_cast<TReferenceKeyframeFrame*>(DisplayFlowPanel->Controls[i]);

	auto currentFrameIndex = referenceKeyframeFrame->GetReferenceKeyFramePointer()->FrameIndex;
	if (currentFrameIndex != oldFrameIndex)
	{
	    referenceKeyframeFrame->SetIsSelected(false);
	}
	else
	{
	    referenceKeyframeFrame->SetIsSelected(true);
	    DisplayScrollBox->ScrollInView(referenceKeyframeFrame);
	}
    }
}
