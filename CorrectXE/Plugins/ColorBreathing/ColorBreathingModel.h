//---------------------------------------------------------------------------

#ifndef ColorBreathingModelH
#define ColorBreathingModelH
//---------------------------------------------------------------------------

#include "IniFile.h"
#include "PresetParameterModel.h"
#include "ReferenceKeyframeData.h"
#include "RGBFrameData.h"
#include "BreakFrameData.h"
#include "GraphTimeline.h"
#include "ColorBreathingData.h"
#include "FlickerProcess.h"

#define NUMBER_USER_PRESETS 4

// This class is the an interface into the C callable
// DLL for color breathing.  It also contains other things
// such as the presets
 class ColorBreathingModel
{
public:
   ColorBreathingModel();
   virtual ~ColorBreathingModel();

   void LoadAllSettingsFromIni(const string &iniFileName);
   int ComputeBitmap(int width, int height, int position, unsigned char *bitmapData);
   void GetLastGraphBitmap(int width, int height, int position, unsigned char *bitmapData);

   void RecomputeGraph();

   PresetParameterModel<ReferenceKeyframeData> *GetPresets();
   CBreakFrameData BreakFrameData;
   RGBFrameData RGBFrameData;
   void SetGraphDimensions(int width, int height);
   ColorBreathingData &GetGraphInfo();
   void SetCenterState(bool enabled);
   bool GetCenterState();
   void SetDisplayIn(int displayIn);
   void SetDisplayOut(int displayOut);

   int GetDisplayIn();
   int GetDisplayOut();
   int GetStartFrameNumber();
   void MarkRendered(int frameIndex);
   void DeleteReferenceFramesWhereThereIsNoGraph();

   void CorrectMeanDataForRender();
   void GetRenderDeltas(int iFrameArg, float *fpDeltaR, float *fpDeltaG, float *fpDeltaB);
   void GetQuantileCorrection(int iFrame, float faX[TL_CHAN_COUNT][TL_QUANTILE_COUNT + 2],
	   float faY[TL_CHAN_COUNT][TL_QUANTILE_COUNT + 2], int iaCount[TL_CHAN_COUNT]);
   void SetImageSize(int pixelsRow, int pixelsCol, int dataMax);
   void PreviewColorBreathingFrame(int frameIndex, int width, int height, unsigned char *frameBits);
   void PreviewColorBreathingFrame16Bit(int frameIndex, int width, int height, int maxPixel, unsigned short *frameBits);

   void SetTotalFrames(int frames);

   int GetGraphDisplayIn() const;
   int GetGraphDisplayOut() const;

   void SetGraphDirty();
   void SetGraphRenderDirty();
   void SetMinScale(double minScale);

   bool IsDataInitialized = false;
   void SetRGBDatum(int frameIndex, const RGBFrameDatum &rgbFrameDatum);
   void LoadRGBDataInToTimeline();

   bool IsPreviewMode() const;
   void SetPreviewMode(bool previewMode);
   bool GraphIsOutOfSync();
   void WritePDLEntry(CPDLElement &parent);
   int  ReadPDLEntry(CPDLEntry &pdlEntry);

   bool AddEndKeyframeToUnrenderedBreakArea(int frameIndex);
   bool AddKeyframe(const ReferenceKeyframeData &r);

   // Just redirects to CTimeline
   void SetQuantileMaximum(unsigned int dataMaximum);
   int MakeQuantiles(const Ipp16u* source, int stepInBytes, IppiSize roiSize, RGBFrameDatum &rgbFrameDatum);

   void SetDumpDebugInfo(bool value) { _dumpDebugInfo = value; }
   bool GetDumpDebugInfo() { return _dumpDebugInfo; }

   vector<int> GetAllReferenceFrameIndexes();

   void SetGraphFinished(bool value) { _graphFinished = value; }
   bool GetGraphFinished() { SetGraphDirty(); return _graphFinished; }

   void DumpRGBData() { _timeline->DumpRGBData(); }
   void LoadRGBData();

    void setUseProcessingRegion(const bool value);
    bool getUseProcessingRegion() const ;

    void setUseAnalysisRegion(const bool value);
    bool getUseAnalysisRegion() const ;

    RECT getProcessingRoi(int frameIndex) const;

private:
   void loadReferenceFramesInToTimeline(CGraphTimeline *timeline);
   std::pair<int, int> FindUnrenderedRange(int frame);

private:
   void *_colorManager = nullptr;
   PresetParameterModel<ReferenceKeyframeData> *_userPresets;
   CGraphTimeline *_timeline = nullptr;
   ColorBreathingData _graphInfo;

   // These default values are for debugging, production should set them to 0
   int _graphWidth = 0;
   int _graphHeight = 0;
   bool _centerState = false;
   int _displayIn = -1;
   int _displayOut = -1;
   int _pixelsRow = 0;
   int _pixelsCol = 0;
   int _dataMax = 0;
   bool _previewMode = false;
   bool _graphFinished = true;
   bool _graphDisplayed = false;
   bool _newGraphAvailable = false;
   bool _dumpDebugInfo = false;
   int _position = -1;
   unsigned char * _currentGraph = nullptr;

   CFlickerProcess *_flickerProcess = nullptr;

   bool _useProcessingRegion = false;
   bool _useAnalysisRegion = true;
};

#endif
